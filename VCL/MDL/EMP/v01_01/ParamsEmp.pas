unit ParamsEmp;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls, Db, (*DBTables,*) JPEG, ExtDlgs,
  ZCF2, ResIntStrings, UnDmkProcFunc, UnGOTOy, UnInternalConsts,
  UnMsgInt, UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa,
  dmkGeral, dmkPermissoes, dmkEdit, dmkLabel, dmkRadioGroup, dmkValUsu,
  dmkDBLookupComboBox, dmkEditCB, dmkDBEdit, ComCtrls, Grids, DBGrids, Menus,
  MyDBCheck, Variants, dmkCheckBox, dmkMemo, XMLDoc, xmldom, XMLIntf, Principal,
  dmkEditDateTimePicker,  dmkImage, UnDmkEnums, ShellAPI,
  {$IfNDef SemNFe_0000}
  CAPICOM_TLB,
  //NFeGeraXML_0310,
  NFeGeraXML_0400, {$EndIf}
  IdBaseComponent, IdComponent, IdGlobal, mySQLDirectQuery, UMySQLDB,
  UnProjGroup_Consts, Vcl.Samples.Spin,
  ACBrNFe, ACBrIntegrador, ACBrUtil,
  Vcl.OleCtrls, SHDocVw,
  UnitWin;

type
  TFmParamsEmp = class(TForm)
    PainelDados: TPanel;
    PainelEdita: TPanel;
    PnEditCab: TPanel;
    PnDataCab: TPanel;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    DBEdCodigo: TdmkDBEdit;
    DBEdit1: TDBEdit;
    DBEdit5: TDBEdit;
    DBEdit6: TDBEdit;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    VuCambioMda: TdmkValUsu;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    Panel4: TPanel;
    Label4: TLabel;
    DBEdit11: TDBEdit;
    DBEdit4: TDBEdit;
    Panel6: TPanel;
    PageControl2: TPageControl;
    TabSheet3: TTabSheet;
    Panel7: TPanel;
    Label5: TLabel;
    DBEdit7: TDBEdit;
    TabSheet4: TTabSheet;
    Panel8: TPanel;
    EdMoeda: TdmkEditCB;
    CBMoeda: TdmkDBLookupComboBox;
    SBMoeda: TSpeedButton;
    TabSheet5: TTabSheet;
    Panel9: TPanel;
    TabSheet6: TTabSheet;
    Panel10: TPanel;
    Label10: TLabel;
    EdBalQtdItem: TdmkEdit;
    Label11: TLabel;
    DBEdit9: TDBEdit;
    QrFiliais: TmySQLQuery;
    QrFiliaisFilial: TIntegerField;
    QrFiliaisCodigo: TIntegerField;
    QrFiliaisNOMEFILIAL: TWideStringField;
    DsFiliais: TDataSource;
    VuFilial: TdmkValUsu;
    QrParamsNFs: TMySQLQuery;
    DsParamsNFs: TDataSource;
    TabSheet7: TTabSheet;
    DBGrid1: TDBGrid;
    QrParamsNFsCodigo: TIntegerField;
    QrParamsNFsControle: TIntegerField;
    QrParamsNFsSequencial: TIntegerField;
    PMFilial: TPopupMenu;
    Adicionafilial1: TMenuItem;
    Alterafilialatual1: TMenuItem;
    Retirafilialatual1: TMenuItem;
    PMSerieNF: TPopupMenu;
    Incluinovasriedenotafiscal1: TMenuItem;
    Alterasriedenotafiscalatual1: TMenuItem;
    Excluisriedenotafiscal1: TMenuItem;
    QrParamsNFsIncSeqAuto: TIntegerField;
    QrParamsNFsNO_IncSeqAuto: TWideStringField;
    Panel11: TPanel;
    Label6: TLabel;
    DBEdit10: TDBEdit;
    DBRadioGroup1: TDBRadioGroup;
    DBRadioGroup2: TDBRadioGroup;
    DBRadioGroup3: TDBRadioGroup;
    Panel12: TPanel;
    Label23: TLabel;
    EdSituacao: TdmkEditCB;
    CBSituacao: TdmkDBLookupComboBox;
    RGFatSemEstq: TdmkRadioGroup;
    RGTipMediaDD: TdmkRadioGroup;
    RGFatSemPrcL: TdmkRadioGroup;
    GroupBox2: TGroupBox;
    CBAssociada: TdmkDBLookupComboBox;
    EdAssociada: TdmkEditCB;
    Label12: TLabel;
    EdAssocModNF: TdmkEditCB;
    CBAssocModNF: TdmkDBLookupComboBox;
    SBModeloNF: TSpeedButton;
    Label18: TLabel;
    QrImprime: TmySQLQuery;
    QrImprimeCodigo: TIntegerField;
    QrImprimeNome: TWideStringField;
    DsImprime: TDataSource;
    QrParamsNFsMaxSeqLib: TIntegerField;
    QrCtaProdVen: TmySQLQuery;
    DsCtaProdVen: TDataSource;
    QrCtaProdVenCodigo: TIntegerField;
    QrCtaProdVenNome: TWideStringField;
    Label17: TLabel;
    EdLogo3x1: TdmkEdit;
    SpeedButton5: TSpeedButton;
    DBEdit20: TDBEdit;
    Label25: TLabel;
    RGTipCalcJuro: TdmkRadioGroup;
    DBRadioGroup6: TDBRadioGroup;
    TabSheet8: TTabSheet;
    TabSheet9: TTabSheet;
    Panel13: TPanel;
    Panel14: TPanel;
    SpeedButton8: TSpeedButton;
    SpeedButton10: TSpeedButton;
    PageControl3: TPageControl;
    TabSheet10: TTabSheet;
    Panel16: TPanel;
    TabSheet11: TTabSheet;
    Panel20: TPanel;
    Label29: TLabel;
    EdDirEnvLot: TdmkEdit;
    Label33: TLabel;
    EdDirRec: TdmkEdit;
    Label35: TLabel;
    EdDirProRec: TdmkEdit;
    SBDirRec: TSpeedButton;
    SBDirEnvLot: TSpeedButton;
    SBDirProRec: TSpeedButton;
    QrUFs: TmySQLQuery;
    QrUFsCodigo: TIntegerField;
    QrUFsNome: TWideStringField;
    DsUFs: TDataSource;
    PageControl4: TPageControl;
    TabSheet12: TTabSheet;
    TabSheet13: TTabSheet;
    Panel21: TPanel;
    Panel22: TPanel;
    DBEdit21: TDBEdit;
    DBEdit22: TDBEdit;
    DBEdit23: TDBEdit;
    DBEdit24: TDBEdit;
    DBEdit25: TDBEdit;
    DBEdit26: TDBEdit;
    Label43: TLabel;
    EdDirNFeAss: TdmkEdit;
    SBDirNfeAss: TSpeedButton;
    Label44: TLabel;
    EdDirNFeGer: TdmkEdit;
    SBDirNFeGer: TSpeedButton;
    DBEdit28: TDBEdit;
    QrParamsNFsSerieNF: TIntegerField;
    Label47: TLabel;
    EdDirDen: TdmkEdit;
    SBDirDen: TSpeedButton;
    TabSheet14: TTabSheet;
    Panel23: TPanel;
    Label40: TLabel;
    Label49: TLabel;
    Label50: TLabel;
    Label51: TLabel;
    Label52: TLabel;
    EdDirPedSta: TdmkEdit;
    SBDirPedSta: TSpeedButton;
    Label53: TLabel;
    EdDirSta: TdmkEdit;
    SBDirSta: TSpeedButton;
    Label39: TLabel;
    Label48: TLabel;
    Label37: TLabel;
    EdDirPedRec: TdmkEdit;
    SBDirPedRec: TSpeedButton;
    Label30: TLabel;
    Label31: TLabel;
    Label32: TLabel;
    Label34: TLabel;
    Label36: TLabel;
    Label38: TLabel;
    Label45: TLabel;
    TabSheet15: TTabSheet;
    Panel24: TPanel;
    Label55: TLabel;
    Label56: TLabel;
    Label57: TLabel;
    Label58: TLabel;
    Label59: TLabel;
    Label60: TLabel;
    DBEdit29: TDBEdit;
    DBEdit30: TDBEdit;
    DBEdit31: TDBEdit;
    DBEdit32: TDBEdit;
    DBEdit33: TDBEdit;
    DBEdit34: TDBEdit;
    DBEdit35: TDBEdit;
    DBEdit36: TDBEdit;
    Label46: TLabel;
    Label54: TLabel;
    BtNFeDefaultDirs: TBitBtn;
    QrCtaServico: TmySQLQuery;
    DsCtaServico: TDataSource;
    QrCtaServicoCodigo: TIntegerField;
    QrCtaServicoNome: TWideStringField;
    DBRadioGroup7: TDBRadioGroup;
    DBRadioGroup8: TDBRadioGroup;
    RGPedVdaMudLista: TdmkRadioGroup;
    RGPedVdaMudPrazo: TdmkRadioGroup;
    Panel30: TPanel;
    Panel34: TPanel;
    QrPreMail0: TmySQLQuery;
    DsPreMail0: TDataSource;
    QrPreMail0Codigo: TIntegerField;
    QrPreMail0Nome: TWideStringField;
    QrPreMail1: TmySQLQuery;
    DsPreMail1: TDataSource;
    QrPreMail1Codigo: TIntegerField;
    QrPreMail1Nome: TWideStringField;
    TabSheet20: TTabSheet;
    VUEntiTipCt1: TdmkValUsu;
    DsEntiTipCto: TDataSource;
    QrEntiTipCto: TmySQLQuery;
    QrEntiTipCtoCodigo: TIntegerField;
    QrEntiTipCtoCodUsu: TIntegerField;
    QrEntiTipCtoNome: TWideStringField;
    TabSheet21: TTabSheet;
    Panel35: TPanel;
    Panel36: TPanel;
    GroupBox15: TGroupBox;
    GroupBox13: TGroupBox;
    Label100: TLabel;
    Label101: TLabel;
    EdVersao: TdmkEdit;
    Edide_mod: TdmkEdit;
    RGide_tpAmb: TdmkRadioGroup;
    RGAppCode: TdmkRadioGroup;
    RGAssDigMode: TdmkRadioGroup;
    GroupBox16: TGroupBox;
    RGCRT: TdmkRadioGroup;
    MeCSOSN_Edit: TMemo;
    GroupBox14: TGroupBox;
    GroupBox17: TGroupBox;
    Label104: TLabel;
    Label105: TLabel;
    DBRadioGroup11: TDBRadioGroup;
    DBEdit60: TDBEdit;
    DBEdit61: TDBEdit;
    DBRadioGroup10: TDBRadioGroup;
    DBRadioGroup9: TDBRadioGroup;
    GroupBox18: TGroupBox;
    Label108: TLabel;
    Label109: TLabel;
    RGDBCRT: TDBRadioGroup;
    MeCSOSN_Show: TMemo;
    EdCSOSN: TdmkEdit;
    EdDBCSOSN: TDBEdit;
    EdDirSchema: TdmkEdit;
    SpeedButton16: TSpeedButton;
    Label110: TLabel;
    Label112: TLabel;
    DBEdit63: TDBEdit;
    Label106: TLabel;
    Label107: TLabel;
    GroupBox19: TGroupBox;
    Label113: TLabel;
    EdpCredSNAlq: TdmkEdit;
    Label115: TLabel;
    EdpCredSNMez: TdmkEdit;
    GroupBox20: TGroupBox;
    Label114: TLabel;
    Label116: TLabel;
    DBEdit64: TDBEdit;
    DBEdit65: TDBEdit;
    CkEstq0UsoCons: TdmkCheckBox;
    DBCheckBox28: TDBCheckBox;
    Panel37: TPanel;
    TabSheet22: TTabSheet;
    Panel39: TPanel;
    GroupBox21: TGroupBox;
    EdSPED_EFD_IND_PERFIL: TdmkEdit;
    Label117: TLabel;
    EdSPED_EFD_IND_PERFIL_TXT: TdmkEdit;
    Label118: TLabel;
    EdSPED_EFD_IND_ATIV: TdmkEdit;
    EdSPED_EFD_IND_ATIV_TXT: TdmkEdit;
    TabSheet23: TTabSheet;
    QrContador4: TmySQLQuery;
    DsContador4: TDataSource;
    DsEscrCtb4: TDataSource;
    QrContador4Codigo: TIntegerField;
    QrContador4NO_ENT: TWideStringField;
    QrEscrCtb4: TmySQLQuery;
    IntegerField1: TIntegerField;
    StringField1: TWideStringField;
    TabSheet24: TTabSheet;
    Panel41: TPanel;
    DBRadioGroup15: TDBRadioGroup;
    DBRadioGroup12: TDBRadioGroup;
    GroupBox27: TGroupBox;
    Label129: TLabel;
    Label130: TLabel;
    Label131: TLabel;
    TabSheet25: TTabSheet;
    Panel42: TPanel;
    RGide_tpImp: TdmkRadioGroup;
    RGNFeItsLin: TdmkRadioGroup;
    GroupBox28: TGroupBox;
    Label132: TLabel;
    Label133: TLabel;
    Label134: TLabel;
    EdNFeFTRazao: TdmkEdit;
    EdNFeFTEnder: TdmkEdit;
    EdNFeFTFones: TdmkEdit;
    DBEdit77: TDBEdit;
    DBEdit78: TDBEdit;
    DBEdit79: TDBEdit;
    CkNFeMaiusc: TdmkCheckBox;
    EdNFeShowURL: TdmkEdit;
    Label135: TLabel;
    RGNFetpEmis: TdmkRadioGroup;
    GroupBox29: TGroupBox;
    Label88: TLabel;
    EdPreMailAut: TdmkEditCB;
    CBPreMailAut: TdmkDBLookupComboBox;
    Label89: TLabel;
    EdPreMailCan: TdmkEditCB;
    CBPreMailCan: TdmkDBLookupComboBox;
    Panel43: TPanel;
    GroupBox32: TGroupBox;
    Panel33: TPanel;
    Label86: TLabel;
    SpeedButton14: TSpeedButton;
    EdDirNFeProt: TdmkEdit;
    Label64: TLabel;
    EdPathLogoNF: TdmkEdit;
    SpeedButton11: TSpeedButton;
    Label84: TLabel;
    EdDirDANFEs: TdmkEdit;
    SpeedButton13: TSpeedButton;
    GroupBox31: TGroupBox;
    Panel44: TPanel;
    Label136: TLabel;
    EdSCAN_Ser: TdmkEdit;
    Label137: TLabel;
    EdSCAN_nNF: TdmkEdit;
    Label138: TLabel;
    EdDirNFeRWeb: TdmkEdit;
    SpeedButton18: TSpeedButton;
    TabSheet26: TTabSheet;
    Panel45: TPanel;
    Label139: TLabel;
    Label141: TLabel;
    Label142: TLabel;
    Label146: TLabel;
    Label147: TLabel;
    Label143: TLabel;
    DBEdit84: TDBEdit;
    DBEdit85: TDBEdit;
    DBEdit86: TDBEdit;
    DBEdit87: TDBEdit;
    DBEdit88: TDBEdit;
    DBEdit89: TDBEdit;
    TabSheet27: TTabSheet;
    Panel46: TPanel;
    Label144: TLabel;
    Label145: TLabel;
    Label148: TLabel;
    Label149: TLabel;
    Label150: TLabel;
    Label151: TLabel;
    EdDirPedCan: TdmkEdit;
    SBDirPedCan: TSpeedButton;
    EdDirCan: TdmkEdit;
    SBDirCan: TSpeedButton;
    EdDirPedInu: TdmkEdit;
    SBDirPedInu: TSpeedButton;
    EdDirInu: TdmkEdit;
    SBDirInu: TSpeedButton;
    EdDirPedSit: TdmkEdit;
    SBDirPedSit: TSpeedButton;
    EdDirSit: TdmkEdit;
    SBDirSit: TSpeedButton;
    EdDirEveEnvLot: TdmkEdit;
    SbDirEveEnvLot: TSpeedButton;
    EdDirEveRetLot: TdmkEdit;
    SbDirEveRetLot: TSpeedButton;
    EdDirEvePedCCe: TdmkEdit;
    SbDirEvePedCCe: TSpeedButton;
    EdDirEveRetCCe: TdmkEdit;
    SbDirEveCCe: TSpeedButton;
    EdDirEvePedCan: TdmkEdit;
    SbDirEvePedCan: TSpeedButton;
    EdDirEveRetCan: TdmkEdit;
    SbDirEveRetCan: TSpeedButton;
    QrEntiTipCt1: TmySQLQuery;
    DsEntiTipCt1: TDataSource;
    dmkValUsu1: TdmkValUsu;
    Label154: TLabel;
    EdDirEveProcCCe: TdmkEdit;
    SbDirProcCCe: TSpeedButton;
    Label155: TLabel;
    DBEdit80: TDBEdit;
    Label156: TLabel;
    EdPreMailEveCCe: TdmkEditCB;
    CBPreMailEveCCe: TdmkDBLookupComboBox;
    QrPreMail2: TmySQLQuery;
    DsPreMail2: TDataSource;
    QrPreMail2Codigo: TIntegerField;
    QrPreMail2Nome: TWideStringField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel47: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    BtFilial: TBitBtn;
    BtSerieNF: TBitBtn;
    TabSheet28: TTabSheet;
    QrMyNFSeMetodos: TmySQLQuery;
    DsMyNFSeMetodos: TDataSource;
    QrMyNFSeMetodosCodigo: TIntegerField;
    QrMyNFSeMetodosNome: TWideStringField;
    TabSheet29: TTabSheet;
    QrMyMetodo: TmySQLQuery;
    DsMyMetodo: TDataSource;
    QrMyMetodoNome: TWideStringField;
    QrEntiTipCt2: TmySQLQuery;
    DsEntiTipCt2: TDataSource;
    dmkValUsu2: TdmkValUsu;
    PageControl5: TPageControl;
    TabSheet30: TTabSheet;
    Panel49: TPanel;
    Label157: TLabel;
    Label158: TLabel;
    Label159: TLabel;
    Label164: TLabel;
    Label168: TLabel;
    Label170: TLabel;
    SbEdNFSeTipCtoMail: TSpeedButton;
    EdNFSeMetodo: TdmkEditCB;
    CBNFSeMetodo: TdmkDBLookupComboBox;
    EdNFSeVersao: TdmkEdit;
    EdNFSeSerieRps: TdmkEdit;
    EdNFSeWSProducao: TdmkEdit;
    RGNFSeAmbiente: TdmkRadioGroup;
    EdNFSeWSHomologa: TdmkEdit;
    CBNFSeTipCtoMail: TdmkDBLookupComboBox;
    EdNFSeTipCtoMail: TdmkEditCB;
    TabSheet31: TTabSheet;
    Panel51: TPanel;
    Label171: TLabel;
    Label172: TLabel;
    SbDirNFSeRPSRecLot: TSpeedButton;
    SbDirNFSeRPSEnvLot: TSpeedButton;
    Label174: TLabel;
    SbDirNFSeNFSAut: TSpeedButton;
    Label175: TLabel;
    SbDirNFSeDPSGer: TSpeedButton;
    EdDirNFSeRPSEnvLot: TdmkEdit;
    EdDirNFSeRPSRecLot: TdmkEdit;
    EdDirNFSeNFSAut: TdmkEdit;
    EdDirNFSeDPSGer: TdmkEdit;
    BtSugereDirNFSe: TBitBtn;
    PageControl6: TPageControl;
    TabSheet32: TTabSheet;
    Panel48: TPanel;
    Label166: TLabel;
    Label167: TLabel;
    DBEdit81: TDBEdit;
    DBEdit82: TDBEdit;
    DBEdit83: TDBEdit;
    DBEdit90: TDBEdit;
    DBEdit91: TDBEdit;
    DBEdit92: TDBEdit;
    DBEdit93: TDBEdit;
    DBRadioGroup16: TDBRadioGroup;
    DBEdit94: TDBEdit;
    DBEdit95: TDBEdit;
    TabSheet33: TTabSheet;
    Panel50: TPanel;
    Label181: TLabel;
    Label182: TLabel;
    DBEdit96: TDBEdit;
    DBEdit97: TDBEdit;
    GroupBox33: TGroupBox;
    Label178: TLabel;
    EdNFSeCertDigital: TdmkEdit;
    SbNFSeCertDigital: TSpeedButton;
    Label179: TLabel;
    TPNFSeCertValidad: TdmkEditDateTimePicker;
    Label180: TLabel;
    EdNFSeCertAviExpi: TdmkEdit;
    Label183: TLabel;
    GroupBox34: TGroupBox;
    Label184: TLabel;
    SpeedButton23: TSpeedButton;
    Label185: TLabel;
    Label186: TLabel;
    Label187: TLabel;
    DBEdit98: TDBEdit;
    DBEdit99: TDBEdit;
    DBEdit100: TDBEdit;
    Label194: TLabel;
    EdNFSePrefeitura1: TdmkEdit;
    Label195: TLabel;
    EdNFSeLogoFili: TdmkEdit;
    SbNFSeLogoFili: TSpeedButton;
    Label196: TLabel;
    EdNFSeLogoPref: TdmkEdit;
    SbNFSeLogoPref: TSpeedButton;
    Label197: TLabel;
    EdNFSePrefeitura2: TdmkEdit;
    Label198: TLabel;
    EdNFSePrefeitura3: TdmkEdit;
    RGNFSeMetodEnvRPS: TdmkRadioGroup;
    EdNFSeTipoRps: TdmkEdit;
    EdTXT_NFSeTipoRps1: TdmkEdit;
    Label199: TLabel;
    Label200: TLabel;
    Label160: TLabel;
    Label161: TLabel;
    EdTXT_NFSeTipoRps2: TdmkEdit;
    Label162: TLabel;
    Label169: TLabel;
    DBEdit101: TDBEdit;
    Label165: TLabel;
    Label201: TLabel;
    Label202: TLabel;
    Label203: TLabel;
    Label204: TLabel;
    Bevel2: TBevel;
    Label205: TLabel;
    Label206: TLabel;
    Label207: TLabel;
    Label208: TLabel;
    Label209: TLabel;
    Label210: TLabel;
    Label211: TLabel;
    DBEdit102: TDBEdit;
    DBEdit103: TDBEdit;
    DBEdit104: TDBEdit;
    DBEdit105: TDBEdit;
    DBCheckBox29: TDBCheckBox;
    DBEdit106: TDBEdit;
    DBEdit107: TDBEdit;
    DBEdit108: TDBEdit;
    DBEdit109: TDBEdit;
    DBEdit110: TDBEdit;
    DBEdit111: TDBEdit;
    DBEdit112: TDBEdit;
    TabSheet34: TTabSheet;
    Panel52: TPanel;
    QrPreEmailNFSeAut: TmySQLQuery;
    IntegerField2: TIntegerField;
    StringField2: TWideStringField;
    DsPreEmailNFSeAut: TDataSource;
    DsPreEmailNFSeCan: TDataSource;
    QrPreEmailNFSeCan: TmySQLQuery;
    IntegerField3: TIntegerField;
    StringField3: TWideStringField;
    TabSheet35: TTabSheet;
    Panel53: TPanel;
    Label214: TLabel;
    Label215: TLabel;
    Label217: TLabel;
    DBEdit113: TDBEdit;
    DBEdit114: TDBEdit;
    DBEdit115: TDBEdit;
    DBEdit116: TDBEdit;
    GroupBox36: TGroupBox;
    Label219: TLabel;
    Label221: TLabel;
    Label222: TLabel;
    Label223: TLabel;
    Label224: TLabel;
    Label225: TLabel;
    DBEdit120: TDBEdit;
    DBEdit121: TDBEdit;
    DBEdit122: TDBEdit;
    DBEdit123: TDBEdit;
    DBEdit124: TDBEdit;
    DBEdit125: TDBEdit;
    DBEdit126: TDBEdit;
    GroupBox37: TGroupBox;
    Label218: TLabel;
    Label226: TLabel;
    Label227: TLabel;
    Label228: TLabel;
    Label229: TLabel;
    Label230: TLabel;
    Label231: TLabel;
    EdNFeVerStaSer: TdmkEdit;
    EdNFeVerEnvLot: TdmkEdit;
    EdNFeVerConLot: TdmkEdit;
    EdNFeVerCanNFe: TdmkEdit;
    EdNFeVerInuNum: TdmkEdit;
    EdNFeVerConNFe: TdmkEdit;
    EdNFeVerLotEve: TdmkEdit;
    TabSheet36: TTabSheet;
    Panel54: TPanel;
    Label232: TLabel;
    DBEdit127: TDBEdit;
    TabSheet37: TTabSheet;
    Panel55: TPanel;
    Label233: TLabel;
    EdDirRetNfeDes: TdmkEdit;
    SBDirRetNfeDes: TSpeedButton;
    Label234: TLabel;
    DBEdit128: TDBEdit;
    Label235: TLabel;
    DBEdit129: TDBEdit;
    Label236: TLabel;
    Label237: TLabel;
    EdDirEvePedMDe: TdmkEdit;
    SbDirEvePedMDe: TSpeedButton;
    EdDirEveRetMDe: TdmkEdit;
    SbDirEveRetMDe: TSpeedButton;
    Panel32: TPanel;
    GroupBox30: TGroupBox;
    Panel57: TPanel;
    Label41: TLabel;
    Label61: TLabel;
    Label75: TLabel;
    EdUF_WebServ: TdmkEdit;
    EdSiglaCustm: TdmkEdit;
    EdUF_Servico: TdmkEdit;
    CkInfoPerCuz: TdmkCheckBox;
    GroupBox38: TGroupBox;
    Panel56: TPanel;
    Label238: TLabel;
    EdUF_MDeMDe: TdmkEdit;
    Label239: TLabel;
    EdUF_MDeDes: TdmkEdit;
    Label240: TLabel;
    EdUF_MDeNFe: TdmkEdit;
    Panel28: TPanel;
    GroupBox39: TGroupBox;
    GroupBox40: TGroupBox;
    Panel59: TPanel;
    Label241: TLabel;
    Label242: TLabel;
    Label243: TLabel;
    Panel58: TPanel;
    Label74: TLabel;
    Label42: TLabel;
    Label62: TLabel;
    Label77: TLabel;
    Label95: TLabel;
    Label96: TLabel;
    Label97: TLabel;
    DBEdit44: TDBEdit;
    DBEdit27: TDBEdit;
    DBEdit45: TDBEdit;
    DBCheckBox1: TDBCheckBox;
    DBEdit37: TDBEdit;
    DBEdit56: TDBEdit;
    DBEdit57: TDBEdit;
    DBEdit130: TDBEdit;
    DBEdit131: TDBEdit;
    DBEdit132: TDBEdit;
    Label244: TLabel;
    EdDirDowNFeDes: TdmkEdit;
    SbDirDowNFeDes: TSpeedButton;
    Label245: TLabel;
    DBEdit133: TDBEdit;
    Label246: TLabel;
    EdDirDowNFeNFe: TdmkEdit;
    SbDirDowNFeNFe: TSpeedButton;
    DBEdit134: TDBEdit;
    Label247: TLabel;
    Panel29: TPanel;
    PMCertDigital: TPopupMenu;
    Incluinovocertificadoaorepositriodosistemaoperacional1: TMenuItem;
    Removecertificadodorepositriodosistemaoperacional1: TMenuItem;
    Label250: TLabel;
    Label212: TLabel;
    EdNFSePreMailAut: TdmkEditCB;
    CBNFSePreMailAut: TdmkDBLookupComboBox;
    Label213: TLabel;
    EdNFSePreMailCan: TdmkEditCB;
    CBNFSePreMailCan: TdmkDBLookupComboBox;
    EdMyEmailNFSe: TdmkEdit;
    Label189: TLabel;
    EdDirNFSeLogs: TdmkEdit;
    SbDirNFSeLogs: TSpeedButton;
    EdDirNFSeSchema: TdmkEdit;
    Label190: TLabel;
    SbDirNFSeSchema: TSpeedButton;
    CkPediVdaNElertas: TdmkCheckBox;
    DBCheckBox30: TDBCheckBox;
    BtOpcoes: TBitBtn;
    QrNFeInfCpl: TmySQLQuery;
    QrNFeInfCplCodigo: TIntegerField;
    QrNFeInfCplCodUsu: TIntegerField;
    QrNFeInfCplNome: TWideStringField;
    QrNFeInfCplTexto: TWideMemoField;
    DsNFeInfCpl: TDataSource;
    GroupBox35: TGroupBox;
    Panel63: TPanel;
    DBEdit139: TDBEdit;
    DBEdit143: TDBEdit;
    DBEdit142: TDBEdit;
    DBEdit141: TDBEdit;
    DBEdit140: TDBEdit;
    Label254: TLabel;
    Label258: TLabel;
    GroupBox43: TGroupBox;
    Panel64: TPanel;
    Label259: TLabel;
    Label260: TLabel;
    Label261: TLabel;
    Label262: TLabel;
    Label263: TLabel;
    SBTZD_UTC: TSpeedButton;
    EdTZD_UTC: TdmkEdit;
    TPhVeraoAsk: TdmkEditDateTimePicker;
    TPhVeraoIni: TdmkEditDateTimePicker;
    TPhVeraoFim: TdmkEditDateTimePicker;
    Label253: TLabel;
    Label255: TLabel;
    Label256: TLabel;
    RGNFeNT2013_003LTT: TdmkRadioGroup;
    Label257: TLabel;
    EdNFeVerEnvLotSinc: TdmkEdit;
    Label220: TLabel;
    Label264: TLabel;
    DBEdit144: TDBEdit;
    Label265: TLabel;
    EdNFeVerConsCad: TdmkEdit;
    Label266: TLabel;
    DBEdit145: TDBEdit;
    RG_NFSe_indFinalCpl: TdmkRadioGroup;
    DBRadioGroup18: TDBRadioGroup;
    RG_NFe_indFinalCpl: TdmkRadioGroup;
    Label267: TLabel;
    EdNFeVerDistDFeInt: TdmkEdit;
    Label268: TLabel;
    DBEdit146: TDBEdit;
    Label269: TLabel;
    DBEdit147: TDBEdit;
    Label270: TLabel;
    DBEdit148: TDBEdit;
    Label271: TLabel;
    Label272: TLabel;
    EdDirDistDFeInt: TdmkEdit;
    EdDirRetDistDFeInt: TdmkEdit;
    SbDirDistDFeInt: TSpeedButton;
    SbDirRetDistDFeInt: TSpeedButton;
    Label273: TLabel;
    EdNFeVerDowNFe: TdmkEdit;
    Label274: TLabel;
    DBEdit149: TDBEdit;
    Label275: TLabel;
    EdDirDowNFeCnf: TdmkEdit;
    SbDirDowNFeCnf: TSpeedButton;
    Label276: TLabel;
    DBEdit150: TDBEdit;
    Label277: TLabel;
    CkTZD_UTC_Auto: TdmkCheckBox;
    DBCheckBox31: TDBCheckBox;
    QrCartEmisHonFun: TmySQLQuery;
    QrCartEmisHonFunCodigo: TIntegerField;
    QrCartEmisHonFunNome: TWideStringField;
    QrCartEmisHonFunTipo: TIntegerField;
    QrCartEmisHonFunForneceI: TIntegerField;
    DsCartEmisHonFun: TDataSource;
    LaCartEmisHonFun: TLabel;
    EdCartEmisHonFun: TdmkEditCB;
    CBCartEmisHonFun: TdmkDBLookupComboBox;
    SBCartEmisHonFun: TSpeedButton;
    DBLaCartEmisHonFun: TLabel;
    DBEdCartEmisHonFun: TDBEdit;
    DBCBCartEmisHonFun: TDBEdit;
    DBRadioGroup02: TDBRadioGroup;
    DsCtaProdCom: TDataSource;
    QrCtaProdCom: TmySQLQuery;
    IntegerField4: TIntegerField;
    StringField4: TWideStringField;
    QrCtaServicoPg: TmySQLQuery;
    IntegerField5: TIntegerField;
    StringField5: TWideStringField;
    DsCtaServicoPg: TDataSource;
    TabSheet38: TTabSheet;
    PageControl7: TPageControl;
    TabSheet39: TTabSheet;
    Panel65: TPanel;
    Panel66: TPanel;
    RGCTetpEmis: TdmkRadioGroup;
    GroupBox44: TGroupBox;
    Label290: TLabel;
    Label291: TLabel;
    Label292: TLabel;
    EdCTePreMailAut: TdmkEditCB;
    CBCTePreMailAut: TdmkDBLookupComboBox;
    EdCTePreMailCan: TdmkEditCB;
    CBCTePreMailCan: TdmkDBLookupComboBox;
    EdCTePreMailEveCCe: TdmkEditCB;
    CBCTePreMailEveCCe: TdmkDBLookupComboBox;
    Panel67: TPanel;
    GroupBox45: TGroupBox;
    Panel68: TPanel;
    Label293: TLabel;
    SpeedButton21: TSpeedButton;
    Label294: TLabel;
    SpeedButton24: TSpeedButton;
    EdDirCTeProt: TdmkEdit;
    EdDirCTeRWeb: TdmkEdit;
    GroupBox46: TGroupBox;
    Panel69: TPanel;
    Label295: TLabel;
    Label296: TLabel;
    EdCTeSCAN_Ser: TdmkEdit;
    EdCTeSCAN_nCT: TdmkEdit;
    Panel70: TPanel;
    GroupBox47: TGroupBox;
    Panel71: TPanel;
    Label297: TLabel;
    Label299: TLabel;
    Label300: TLabel;
    SpeedButton27: TSpeedButton;
    Label301: TLabel;
    Label302: TLabel;
    Label303: TLabel;
    EdCTeUF_WebServ: TdmkEdit;
    EdCTeUF_Servico: TdmkEdit;
    EdCTeSerNum: TdmkEdit;
    TPCTeSerVal: TdmkEditDateTimePicker;
    EdCTeSerAvi: TdmkEdit;
    GroupBox48: TGroupBox;
    Panel72: TPanel;
    Label304: TLabel;
    Label305: TLabel;
    Label306: TLabel;
    dmkEdit9: TdmkEdit;
    dmkEdit10: TdmkEdit;
    EdUF_MDeCTe: TdmkEdit;
    TabSheet40: TTabSheet;
    Panel73: TPanel;
    Label307: TLabel;
    Label308: TLabel;
    Label309: TLabel;
    SbDirCTePedRec: TSpeedButton;
    SbDirCTeEnvLot: TSpeedButton;
    SbDirCTeProRec: TSpeedButton;
    Label310: TLabel;
    SBDirCTeAss: TSpeedButton;
    Label311: TLabel;
    SBDirCTeGer: TSpeedButton;
    Label312: TLabel;
    SpeedButton31: TSpeedButton;
    Label313: TLabel;
    SpeedButton32: TSpeedButton;
    EdDirCTeEnvLot: TdmkEdit;
    EdDirCTeRec: TdmkEdit;
    EdDirCTeProRec: TdmkEdit;
    EdDirCTeAss: TdmkEdit;
    EdDirCTeGer: TdmkEdit;
    EdDirCTeDen: TdmkEdit;
    EdDirCTePedRec: TdmkEdit;
    BtCTeDefaultDirs: TBitBtn;
    TabSheet41: TTabSheet;
    Panel74: TPanel;
    Label314: TLabel;
    Label315: TLabel;
    Label316: TLabel;
    Label317: TLabel;
    Label318: TLabel;
    SbDirCTePedSta: TSpeedButton;
    Label319: TLabel;
    SbDirCTeSta: TSpeedButton;
    Label320: TLabel;
    Label321: TLabel;
    SBDirCTePedCan: TSpeedButton;
    SBDirCTeCan: TSpeedButton;
    SbDirCTePedInu: TSpeedButton;
    SbDirCTeInu: TSpeedButton;
    SbDirCTePedSit: TSpeedButton;
    SBDirCTeSit: TSpeedButton;
    EdDirCTePedSta: TdmkEdit;
    EdDirCTeSta: TdmkEdit;
    EdDirCTePedCan: TdmkEdit;
    EdDirCTeCan: TdmkEdit;
    EdDirCTePedInu: TdmkEdit;
    EdDirCTeInu: TdmkEdit;
    EdDirCTePedSit: TdmkEdit;
    EdDirCTeSit: TdmkEdit;
    TabSheet42: TTabSheet;
    Panel75: TPanel;
    Label322: TLabel;
    Label323: TLabel;
    Label324: TLabel;
    Label325: TLabel;
    Label326: TLabel;
    Label327: TLabel;
    SpeedButton41: TSpeedButton;
    SpeedButton42: TSpeedButton;
    SpeedButton43: TSpeedButton;
    SpeedButton44: TSpeedButton;
    SpeedButton45: TSpeedButton;
    SpeedButton46: TSpeedButton;
    Label328: TLabel;
    SpeedButton47: TSpeedButton;
    EdDirCTeEveEnvLot: TdmkEdit;
    EdDirCTeEveRetLot: TdmkEdit;
    EdDirCTeEvePedCCe: TdmkEdit;
    EdDirCTeEveRetCCe: TdmkEdit;
    EdDirCTeEvePedCan: TdmkEdit;
    EdDirCTeEveRetCan: TdmkEdit;
    EdDirCTEEveProcCCe: TdmkEdit;
    TabSheet43: TTabSheet;
    Panel76: TPanel;
    Label329: TLabel;
    SBDirRetCTeDes: TSpeedButton;
    Label330: TLabel;
    Label331: TLabel;
    SpeedButton48: TSpeedButton;
    SpeedButton49: TSpeedButton;
    Label332: TLabel;
    SbDirDowCTeDes: TSpeedButton;
    Label333: TLabel;
    SbDirDowCTeCTe: TSpeedButton;
    Label334: TLabel;
    Label335: TLabel;
    SbDirCTeEvePedEPEC: TSpeedButton;
    SbDirCTeEveEPEC: TSpeedButton;
    Label336: TLabel;
    SbDirDowCTeCnf: TSpeedButton;
    EdDirRetCTeDes: TdmkEdit;
    dmkEdit31: TdmkEdit;
    dmkEdit32: TdmkEdit;
    EdDirDowCTeDes: TdmkEdit;
    EdDirDowCTeCTe: TdmkEdit;
    EdDirCTeEvePedEPEC: TdmkEdit;
    EdDirCTeEveRetEPEC: TdmkEdit;
    EdDirDowCTeCnf: TdmkEdit;
    TabSheet44: TTabSheet;
    Panel77: TPanel;
    GroupBox49: TGroupBox;
    Label337: TLabel;
    Label338: TLabel;
    SpeedButton52: TSpeedButton;
    Label339: TLabel;
    SpeedButton53: TSpeedButton;
    Label340: TLabel;
    SpeedButton54: TSpeedButton;
    GroupBox50: TGroupBox;
    Label341: TLabel;
    Label342: TLabel;
    SpeedButton55: TSpeedButton;
    Label343: TLabel;
    EdCTeversao: TdmkEdit;
    EdCTeide_mod: TdmkEdit;
    RGCTeide_tpAmb: TdmkRadioGroup;
    EdDirCTeSchema: TdmkEdit;
    RGCTeAppCode: TdmkRadioGroup;
    RGCTeAssDigMode: TdmkRadioGroup;
    CBCTeEntiTipCto: TdmkDBLookupComboBox;
    EdMyEmailCTe: TdmkEdit;
    EdCTeEntiTipCto: TdmkEditCB;
    EdCTeEntiTipCt1: TdmkEditCB;
    CBCTeEntiTipCt1: TdmkDBLookupComboBox;
    EdCTeInfCpl: TdmkEditCB;
    CBCTeInfCpl: TdmkDBLookupComboBox;
    GroupBox53: TGroupBox;
    Label348: TLabel;
    Label349: TLabel;
    Label350: TLabel;
    Label351: TLabel;
    Label352: TLabel;
    Label353: TLabel;
    Label354: TLabel;
    Label355: TLabel;
    Label356: TLabel;
    Label357: TLabel;
    Label358: TLabel;
    EdCTeVerStaSer: TdmkEdit;
    EdCTeVerEnvLot: TdmkEdit;
    EdCTeVerConLot: TdmkEdit;
    EdCTeVerCanCTe: TdmkEdit;
    EdCTeVerInuNum: TdmkEdit;
    EdCTeVerConCTe: TdmkEdit;
    EdCTeVerLotEve: TdmkEdit;
    EdCTeVerEnvLotSinc: TdmkEdit;
    EdCTeVerConsCad: TdmkEdit;
    EdCTeVerDistDFeInt: TdmkEdit;
    EdCTeVerDowCTe: TdmkEdit;
    TabSheet45: TTabSheet;
    Panel78: TPanel;
    Label359: TLabel;
    Label360: TLabel;
    SpeedButton56: TSpeedButton;
    Label361: TLabel;
    SpeedButton57: TSpeedButton;
    RGCTeide_tpImp: TdmkRadioGroup;
    RGCTeItsLin: TdmkRadioGroup;
    GroupBox54: TGroupBox;
    Label362: TLabel;
    Label363: TLabel;
    Label364: TLabel;
    EdCTeFTRazao: TdmkEdit;
    EdCTeFTEnder: TdmkEdit;
    EdCTeFTFones: TdmkEdit;
    CkCTeMaiusc: TdmkCheckBox;
    EdCTeShowURL: TdmkEdit;
    EdPathLogoCTe: TdmkEdit;
    EdDirDACTes: TdmkEdit;
    RG_CTe_indFinalCpl: TdmkRadioGroup;
    VUCTeEntiTipCt1: TdmkValUsu;
    VUCTeEntiTipCto: TdmkValUsu;
    QrCtaMultiSV: TMySQLQuery;
    IntegerField6: TIntegerField;
    StringField6: TWideStringField;
    DsCtaMultiSV: TDataSource;
    QrCTeEntiTipCto: TmySQLQuery;
    IntegerField7: TIntegerField;
    IntegerField8: TIntegerField;
    StringField7: TWideStringField;
    QrCTeEntiTipCt1: TmySQLQuery;
    DsCTeEntiTipCto: TDataSource;
    DsCTeEntiTipCt1: TDataSource;
    PMSerieCTe: TPopupMenu;
    IncluinovasriedeCTe1: TMenuItem;
    AlterasriedeCTeatual1: TMenuItem;
    ExcluisriedeCTeatual1: TMenuItem;
    QrParamsCTe: TmySQLQuery;
    QrParamsCTeCodigo: TIntegerField;
    QrParamsCTeControle: TIntegerField;
    QrParamsCTeSequencial: TIntegerField;
    QrParamsCTeIncSeqAuto: TIntegerField;
    QrParamsCTeNO_IncSeqAuto: TWideStringField;
    QrParamsCTeMaxSeqLib: TIntegerField;
    QrParamsCTeSerieCTe: TIntegerField;
    DsParamsCTe: TDataSource;
    TabSheet46: TTabSheet;
    DBGrid2: TDBGrid;
    TabSheet47: TTabSheet;
    Panel79: TPanel;
    GroupBox3: TGroupBox;
    GroupBox5: TGroupBox;
    RGFaturaSeq: TdmkRadioGroup;
    Panel17: TPanel;
    Label15: TLabel;
    EdFaturaSep: TdmkEdit;
    Panel15: TPanel;
    SbProdVen: TSpeedButton;
    Label20: TLabel;
    SpeedButton9: TSpeedButton;
    Label66: TLabel;
    Label69: TLabel;
    Label70: TLabel;
    SBCtaProdCom: TSpeedButton;
    Label279: TLabel;
    Label280: TLabel;
    SBCtaServicoPg: TSpeedButton;
    Label282: TLabel;
    Label283: TLabel;
    SpeedButton28: TSpeedButton;
    Label344: TLabel;
    Label345: TLabel;
    RGFaturaDta: TdmkRadioGroup;
    CBCtaProdVen: TdmkDBLookupComboBox;
    EdCtaProdVen: TdmkEditCB;
    EdTxtProdVen: TdmkEdit;
    EdCtaServico: TdmkEditCB;
    CBCtaServico: TdmkDBLookupComboBox;
    EdTxtServico: TdmkEdit;
    EdDupProdVen: TdmkEdit;
    EdDupServico: TdmkEdit;
    RGFaturaNum: TdmkRadioGroup;
    RGUsaReferen: TdmkRadioGroup;
    EdCtaProdCom: TdmkEditCB;
    CBCtaProdCom: TdmkDBLookupComboBox;
    EdTxtProdCom: TdmkEdit;
    EdDupProdCom: TdmkEdit;
    EdCtaServicoPg: TdmkEditCB;
    CBCtaServicoPg: TdmkDBLookupComboBox;
    EdTxtServicoPg: TdmkEdit;
    EdDupServicoPg: TdmkEdit;
    EdCtaFretPrest: TdmkEditCB;
    CBCtaFretPrest: TdmkDBLookupComboBox;
    EdTxtFretPrest: TdmkEdit;
    EdDupFretPrest: TdmkEdit;
    TabSheet48: TTabSheet;
    PageControl8: TPageControl;
    TabSheet49: TTabSheet;
    Panel80: TPanel;
    Panel81: TPanel;
    RGMDFetpEmis: TdmkRadioGroup;
    GroupBox51: TGroupBox;
    Label346: TLabel;
    Label347: TLabel;
    Label365: TLabel;
    EdMDFePreMailAut: TdmkEditCB;
    CBMDFePreMailAut: TdmkDBLookupComboBox;
    EdMDFePreMailCan: TdmkEditCB;
    CBMDFePreMailCan: TdmkDBLookupComboBox;
    EdMDFePreMailEveCCe: TdmkEditCB;
    CBMDFePreMailEveCCe: TdmkDBLookupComboBox;
    Panel82: TPanel;
    GroupBox52: TGroupBox;
    Panel83: TPanel;
    Label366: TLabel;
    SpeedButton29: TSpeedButton;
    Label367: TLabel;
    SpeedButton30: TSpeedButton;
    EdDirMDFeProt: TdmkEdit;
    EdDirMDFeRWeb: TdmkEdit;
    GroupBox55: TGroupBox;
    Panel84: TPanel;
    Label368: TLabel;
    Label369: TLabel;
    EdMDFeSCAN_Ser: TdmkEdit;
    EdMDFeSCAN_nMDF: TdmkEdit;
    Panel85: TPanel;
    GroupBox56: TGroupBox;
    Panel86: TPanel;
    Label370: TLabel;
    Label371: TLabel;
    Label372: TLabel;
    SpeedButton33: TSpeedButton;
    Label373: TLabel;
    Label374: TLabel;
    Label375: TLabel;
    EdMDFeUF_WebServ: TdmkEdit;
    EdMDFeUF_Servico: TdmkEdit;
    EdMDFeSerNum: TdmkEdit;
    TPMDFeSerVal: TdmkEditDateTimePicker;
    EdMDFeSerAvi: TdmkEdit;
    GroupBox57: TGroupBox;
    Panel87: TPanel;
    Label376: TLabel;
    Label377: TLabel;
    Label378: TLabel;
    dmkEdit1: TdmkEdit;
    dmkEdit2: TdmkEdit;
    EdUF_MDeMDFe: TdmkEdit;
    TabSheet50: TTabSheet;
    Panel88: TPanel;
    Label379: TLabel;
    Label380: TLabel;
    Label381: TLabel;
    SbDirMDFePedRec: TSpeedButton;
    SbDirMDFeEnvLot: TSpeedButton;
    SbDirMDFeProRec: TSpeedButton;
    Label382: TLabel;
    SBDirMDFeAss: TSpeedButton;
    Label383: TLabel;
    SBDirMDFeGer: TSpeedButton;
    Label384: TLabel;
    SpeedButton34: TSpeedButton;
    Label385: TLabel;
    SpeedButton35: TSpeedButton;
    EdDirMDFeEnvLot: TdmkEdit;
    EdDirMDFeRec: TdmkEdit;
    EdDirMDFeProRec: TdmkEdit;
    EdDirMDFeAss: TdmkEdit;
    EdDirMDFeGer: TdmkEdit;
    EdDirMDFeDen: TdmkEdit;
    EdDirMDFePedRec: TdmkEdit;
    BtMDFeDefaultDirs: TBitBtn;
    TabSheet51: TTabSheet;
    Panel89: TPanel;
    Label386: TLabel;
    Label387: TLabel;
    Label388: TLabel;
    Label389: TLabel;
    Label390: TLabel;
    SbDirMDFePedSta: TSpeedButton;
    Label391: TLabel;
    SbDirMDFeSta: TSpeedButton;
    Label392: TLabel;
    Label393: TLabel;
    SBDirMDFePedCan: TSpeedButton;
    SBDirMDFeCan: TSpeedButton;
    SbDirMDFePedInu: TSpeedButton;
    SbDirMDFeInu: TSpeedButton;
    SbDirMDFePedSit: TSpeedButton;
    SBDirMDFeSit: TSpeedButton;
    EdDirMDFePedSta: TdmkEdit;
    EdDirMDFeSta: TdmkEdit;
    EdDirMDFePedCan: TdmkEdit;
    EdDirMDFeCan: TdmkEdit;
    EdDirMDFePedInu: TdmkEdit;
    EdDirMDFeInu: TdmkEdit;
    EdDirMDFePedSit: TdmkEdit;
    EdDirMDFeSit: TdmkEdit;
    TabSheet52: TTabSheet;
    Panel90: TPanel;
    Label394: TLabel;
    Label395: TLabel;
    Label396: TLabel;
    Label397: TLabel;
    Label398: TLabel;
    Label399: TLabel;
    SpeedButton36: TSpeedButton;
    SpeedButton37: TSpeedButton;
    SpeedButton38: TSpeedButton;
    SpeedButton39: TSpeedButton;
    SpeedButton40: TSpeedButton;
    SpeedButton58: TSpeedButton;
    EdDirMDFeEveEnvLot: TdmkEdit;
    EdDirMDFeEveRetLot: TdmkEdit;
    EdDirMDFeEvePedEnc: TdmkEdit;
    EdDirMDFeEveRetEnc: TdmkEdit;
    EdDirMDFeEvePedCan: TdmkEdit;
    EdDirMDFeEveRetCan: TdmkEdit;
    TabSheet53: TTabSheet;
    Panel91: TPanel;
    Label401: TLabel;
    SBDirRetMDFeDes: TSpeedButton;
    Label402: TLabel;
    Label403: TLabel;
    SpeedButton60: TSpeedButton;
    SpeedButton61: TSpeedButton;
    Label404: TLabel;
    SbDirDowMDFeDes: TSpeedButton;
    Label405: TLabel;
    SbDirDowMDFeMDFe: TSpeedButton;
    Label406: TLabel;
    Label407: TLabel;
    SpeedButton62: TSpeedButton;
    SpeedButton63: TSpeedButton;
    Label408: TLabel;
    SbDirDowMDFeCnf: TSpeedButton;
    EdDirRetMDFeDes: TdmkEdit;
    dmkEdit4: TdmkEdit;
    dmkEdit5: TdmkEdit;
    EdDirDowMDFeDes: TdmkEdit;
    EdDirDowMDFeMDFe: TdmkEdit;
    dmkEdit8: TdmkEdit;
    dmkEdit11: TdmkEdit;
    EdDirDowMDFeCnf: TdmkEdit;
    TabSheet54: TTabSheet;
    Panel92: TPanel;
    GroupBox58: TGroupBox;
    Label409: TLabel;
    Label410: TLabel;
    SpeedButton64: TSpeedButton;
    Label411: TLabel;
    SpeedButton65: TSpeedButton;
    Label412: TLabel;
    SpeedButton66: TSpeedButton;
    GroupBox59: TGroupBox;
    Label413: TLabel;
    Label414: TLabel;
    SpeedButton67: TSpeedButton;
    Label415: TLabel;
    EdMDFeversao: TdmkEdit;
    EdMDFeide_mod: TdmkEdit;
    RGMDFeide_tpAmb: TdmkRadioGroup;
    EdDirMDFeSchema: TdmkEdit;
    RGMDFeAppCode: TdmkRadioGroup;
    RGMDFeAssDigMode: TdmkRadioGroup;
    CBMDFeEntiTipCto: TdmkDBLookupComboBox;
    EdMyEmailMDFe: TdmkEdit;
    EdMDFeEntiTipCto: TdmkEditCB;
    EdMDFeEntiTipCt1: TdmkEditCB;
    CBMDFeEntiTipCt1: TdmkDBLookupComboBox;
    EdMDFeInfCpl: TdmkEditCB;
    CBMDFeInfCpl: TdmkDBLookupComboBox;
    GroupBox60: TGroupBox;
    Label416: TLabel;
    Label417: TLabel;
    Label418: TLabel;
    Label419: TLabel;
    Label420: TLabel;
    Label421: TLabel;
    Label422: TLabel;
    Label423: TLabel;
    Label424: TLabel;
    Label425: TLabel;
    Label426: TLabel;
    EdMDFeVerStaSer: TdmkEdit;
    EdMDFeVerEnvLot: TdmkEdit;
    EdMDFeVerConLot: TdmkEdit;
    EdMDFeVerCanMDFe: TdmkEdit;
    EdMDFeVerInuNum: TdmkEdit;
    EdMDFeVerConMDFe: TdmkEdit;
    EdMDFeVerLotEve: TdmkEdit;
    EdMDFeVerEnvLotSinc: TdmkEdit;
    EdMDFeVerConsCad: TdmkEdit;
    EdMDFeVerDistDFeInt: TdmkEdit;
    EdMDFeVerDowMDFe: TdmkEdit;
    TabSheet55: TTabSheet;
    Panel93: TPanel;
    Label427: TLabel;
    Label428: TLabel;
    SpeedButton68: TSpeedButton;
    Label429: TLabel;
    SpeedButton69: TSpeedButton;
    RGMDFeide_tpImp: TdmkRadioGroup;
    RGMDFeItsLin: TdmkRadioGroup;
    GroupBox61: TGroupBox;
    Label430: TLabel;
    Label431: TLabel;
    Label432: TLabel;
    EdMDFeFTRazao: TdmkEdit;
    EdMDFeFTEnder: TdmkEdit;
    EdMDFeFTFones: TdmkEdit;
    CkMDFeMaiusc: TdmkCheckBox;
    EdMDFeShowURL: TdmkEdit;
    EdPathLogoMDFe: TdmkEdit;
    EdDirDAMDFes: TdmkEdit;
    RG_MDFe_indFinalCpl: TdmkRadioGroup;
    PMSerieMDFe: TPopupMenu;
    IncluinovasriedeMDFe1: TMenuItem;
    AlterasriedeMDFeatual1: TMenuItem;
    ExcluisriedeMDFeatual1: TMenuItem;
    TabSheet56: TTabSheet;
    DBGrid3: TDBGrid;
    QrParamsMDFe: TmySQLQuery;
    DsParamsMDFe: TDataSource;
    QrParamsMDFeCodigo: TIntegerField;
    QrParamsMDFeControle: TIntegerField;
    QrParamsMDFeSequencial: TIntegerField;
    QrParamsMDFeIncSeqAuto: TIntegerField;
    QrParamsMDFeNO_IncSeqAuto: TWideStringField;
    QrParamsMDFeMaxSeqLib: TIntegerField;
    QrParamsMDFeSerieCTe: TIntegerField;
    Label400: TLabel;
    EdDirMDFeEvePedIdC: TdmkEdit;
    SpeedButton59: TSpeedButton;
    Label433: TLabel;
    EdDirMDFeEveRetIdC: TdmkEdit;
    SpeedButton70: TSpeedButton;
    N1: TMenuItem;
    N2: TMenuItem;
    Alteraentidadedafilialatual1: TMenuItem;
    Label434: TLabel;
    EdCTeVerEPEC: TdmkEdit;
    Label435: TLabel;
    EdCTeUF_EPEC: TdmkEdit;
    Label436: TLabel;
    EdNFeUF_EPEC: TdmkEdit;
    Label437: TLabel;
    EdCTeUF_Conting: TdmkEdit;
    GroupBox62: TGroupBox;
    Panel94: TPanel;
    RGSPED_EFD_DtaFiscalSaida: TdmkRadioGroup;
    RGNoDANFEMail: TdmkRadioGroup;
    RGSPED_EFD_ID_0150: TdmkRadioGroup;
    RGSPED_EFD_ID_0200: TdmkRadioGroup;
    Panel96: TPanel;
    Panel98: TPanel;
    Panel99: TPanel;
    RGSPED_EFD_Producao: TdmkRadioGroup;
    Panel97: TPanel;
    SpeedButton50: TSpeedButton;
    GroupBox64: TGroupBox;
    Panel102: TPanel;
    Label439: TLabel;
    Label442: TLabel;
    Label444: TLabel;
    EdFreteRpICMS: TdmkEdit;
    EdFreteRpPIS: TdmkEdit;
    EdFreteRpCOFINS: TdmkEdit;
    CkRetImpost: TdmkCheckBox;
    GroupBox67: TGroupBox;
    PninfRespTec_Usa: TPanel;
    Label440: TLabel;
    Label441: TLabel;
    EdinfRespTec_xContato: TdmkEdit;
    CkinfRespTec_Usa: TdmkCheckBox;
    EdinfRespTec_CNPJ: TdmkEdit;
    EdinfRespTec_fone: TdmkEdit;
    Label443: TLabel;
    EdinfRespTec_email: TdmkEdit;
    Label445: TLabel;
    Label446: TLabel;
    EdinfRespTec_idCSRT: TdmkEdit;
    EdinfRespTec_CSRT: TdmkEdit;
    SpeedButton71: TSpeedButton;
    Panel104: TPanel;
    CkSPED_EFD_MovSubPrd: TdmkCheckBox;
    Panel105: TPanel;
    Label98: TLabel;
    EdEntiTipCto: TdmkEditCB;
    CBEntiTipCto: TdmkDBLookupComboBox;
    SpeedButton15: TSpeedButton;
    Label152: TLabel;
    EdEntiTipCt1: TdmkEditCB;
    CBEntiTipCt1: TdmkDBLookupComboBox;
    SpeedButton19: TSpeedButton;
    Label99: TLabel;
    EdMyEmailNFe: TdmkEdit;
    Label251: TLabel;
    EdNFeInfCpl: TdmkEditCB;
    CBNFeInfCpl: TdmkDBLookupComboBox;
    SpeedButton20: TSpeedButton;
    CkNT2018_05v120: TdmkCheckBox;
    Panel106: TPanel;
    DBEdit62: TDBEdit;
    DBEdit138: TDBEdit;
    DBEdit137: TDBEdit;
    Label252: TLabel;
    DBEdit58: TDBEdit;
    Label103: TLabel;
    DBEdit76: TDBEdit;
    DBEdit74: TDBEdit;
    Label153: TLabel;
    DBEdit59: TDBEdit;
    Label102: TLabel;
    DBCheckBox32: TDBCheckBox;
    TabSheet57: TTabSheet;
    Panel18: TPanel;
    GroupBox6: TGroupBox;
    GroupBox7: TGroupBox;
    Panel19: TPanel;
    Label21: TLabel;
    DBEdit18: TDBEdit;
    DBRadioGroup4: TDBRadioGroup;
    Panel108: TPanel;
    Label22: TLabel;
    Label24: TLabel;
    Label67: TLabel;
    Label68: TLabel;
    Label71: TLabel;
    Label72: TLabel;
    Label284: TLabel;
    Label285: TLabel;
    Label286: TLabel;
    Label287: TLabel;
    Label288: TLabel;
    Label289: TLabel;
    DBRadioGroup5: TDBRadioGroup;
    DBEdit15: TDBEdit;
    DBEdit17: TDBEdit;
    DBEdit19: TDBEdit;
    DBEdit39: TDBEdit;
    DBEdit40: TDBEdit;
    DBEdit41: TDBEdit;
    DBEdit42: TDBEdit;
    DBEdit43: TDBEdit;
    DBRadioGroup13: TDBRadioGroup;
    DBRadioGroup14: TDBRadioGroup;
    DBEdit151: TDBEdit;
    DBEdit152: TDBEdit;
    DBEdit153: TDBEdit;
    DBEdit154: TDBEdit;
    DBEdit155: TDBEdit;
    DBEdit156: TDBEdit;
    DBEdit157: TDBEdit;
    DBEdit158: TDBEdit;
    GroupBox1: TGroupBox;
    Label13: TLabel;
    Label16: TLabel;
    DBEdit12: TDBEdit;
    DBEdit13: TDBEdit;
    DBEdit16: TDBEdit;
    DBEdit14: TDBEdit;
    Panel109: TPanel;
    GroupBox69: TGroupBox;
    Panel110: TPanel;
    Label452: TLabel;
    Label453: TLabel;
    Label454: TLabel;
    DBCheckBox34: TDBCheckBox;
    DBEdit165: TDBEdit;
    DBEdit166: TDBEdit;
    DBEdit167: TDBEdit;
    Label455: TLabel;
    DBCheckBox35: TDBCheckBox;
    DBEdit168: TDBEdit;
    Label63: TLabel;
    DBEdit38: TDBEdit;
    Label85: TLabel;
    DBEdit50: TDBEdit;
    DBRadioGroup19: TDBRadioGroup;
    DBRadioGroup17: TDBRadioGroup;
    Panel38: TPanel;
    GroupBox42: TGroupBox;
    Panel62: TPanel;
    Label248: TLabel;
    Label249: TLabel;
    DBEdit135: TDBEdit;
    DBEdit136: TDBEdit;
    Panel60: TPanel;
    Panel61: TPanel;
    Label87: TLabel;
    Label140: TLabel;
    DBEdit51: TDBEdit;
    DBEdit75: TDBEdit;
    GroupBox68: TGroupBox;
    PnDbinfRespTec_Usa: TPanel;
    Label447: TLabel;
    Label448: TLabel;
    Label449: TLabel;
    Label450: TLabel;
    Label451: TLabel;
    DBEdit159: TDBEdit;
    DBEdit160: TDBEdit;
    DBEdit161: TDBEdit;
    DBEdit162: TDBEdit;
    DBEdit163: TDBEdit;
    DBEdit164: TDBEdit;
    CkDBinfRespTec_Usa: TDBCheckBox;
    QrPrmsEmpNFe: TMySQLQuery;
    QrPrmsEmpNFePreMailAut: TIntegerField;
    QrPrmsEmpNFePreMailCan: TIntegerField;
    QrPrmsEmpNFePreMailEveCCe: TIntegerField;
    QrPrmsEmpNFeDirNFeProt: TWideStringField;
    QrPrmsEmpNFeDirNFeRWeb: TWideStringField;
    QrPrmsEmpNFeSCAN_Ser: TIntegerField;
    QrPrmsEmpNFeSCAN_nNF: TIntegerField;
    QrPrmsEmpNFeinfRespTec_xContato: TWideStringField;
    QrPrmsEmpNFeinfRespTec_CNPJ: TWideStringField;
    QrPrmsEmpNFeinfRespTec_fone: TWideStringField;
    QrPrmsEmpNFeinfRespTec_email: TWideStringField;
    QrPrmsEmpNFeinfRespTec_idCSRT: TWideStringField;
    QrPrmsEmpNFeinfRespTec_CSRT: TWideStringField;
    QrPrmsEmpNFeinfRespTec_Usa: TSmallintField;
    QrPrmsEmpNFeUF_WebServ: TWideStringField;
    QrPrmsEmpNFeSiglaCustm: TWideStringField;
    QrPrmsEmpNFeUF_Servico: TWideStringField;
    QrPrmsEmpNFeSimplesFed: TSmallintField;
    QrPrmsEmpNFeInfoPerCuz: TSmallintField;
    QrPrmsEmpNFeNFeSerNum: TWideStringField;
    QrPrmsEmpNFeNFeSerVal: TDateField;
    QrPrmsEmpNFeNFeSerAvi: TSmallintField;
    QrPrmsEmpNFeUF_MDeMDe: TWideStringField;
    QrPrmsEmpNFeUF_MDeDes: TWideStringField;
    QrPrmsEmpNFeUF_MDeNFe: TWideStringField;
    QrPrmsEmpNFeNFeUF_EPEC: TWideStringField;
    QrPrmsEmpNFeDirEnvLot: TWideStringField;
    QrPrmsEmpNFeDirRec: TWideStringField;
    QrPrmsEmpNFeDirProRec: TWideStringField;
    QrPrmsEmpNFeDirNFeAss: TWideStringField;
    QrPrmsEmpNFeDirNFeGer: TWideStringField;
    QrPrmsEmpNFeDirDen: TWideStringField;
    QrPrmsEmpNFeDirPedRec: TWideStringField;
    QrPrmsEmpNFeDirPedSta: TWideStringField;
    QrPrmsEmpNFeDirSta: TWideStringField;
    QrPrmsEmpNFeDirPedCan: TWideStringField;
    QrPrmsEmpNFeDirCan: TWideStringField;
    QrPrmsEmpNFeDirPedInu: TWideStringField;
    QrPrmsEmpNFeDirInu: TWideStringField;
    QrPrmsEmpNFeDirPedSit: TWideStringField;
    QrPrmsEmpNFeDirSit: TWideStringField;
    QrPrmsEmpNFeDirEveEnvLot: TWideStringField;
    QrPrmsEmpNFeDirEveRetLot: TWideStringField;
    QrPrmsEmpNFeDirEvePedCCe: TWideStringField;
    QrPrmsEmpNFeDirEveRetCCe: TWideStringField;
    QrPrmsEmpNFeDirEvePedCan: TWideStringField;
    QrPrmsEmpNFeDirEveRetCan: TWideStringField;
    QrPrmsEmpNFeDirEveProcCCe: TWideStringField;
    QrPrmsEmpNFeDirSchema: TWideStringField;
    QrPrmsEmpNFeversao: TFloatField;
    QrPrmsEmpNFeide_mod: TSmallintField;
    QrPrmsEmpNFeide_tpAmb: TSmallintField;
    QrPrmsEmpNFeNT2018_05v120: TSmallintField;
    QrPrmsEmpNFeAppCode: TSmallintField;
    QrPrmsEmpNFeAssDigMode: TSmallintField;
    QrPrmsEmpNFeMyEmailNFe: TWideStringField;
    QrPrmsEmpNFeCRT: TSmallintField;
    QrPrmsEmpNFeCSOSN: TIntegerField;
    QrPrmsEmpNFepCredSNAlq: TFloatField;
    QrPrmsEmpNFeNFeVerStaSer: TFloatField;
    QrPrmsEmpNFeNFeVerEnvLot: TFloatField;
    QrPrmsEmpNFeNFeVerConLot: TFloatField;
    QrPrmsEmpNFeNFeVerCanNFe: TFloatField;
    QrPrmsEmpNFeNFeVerInuNum: TFloatField;
    QrPrmsEmpNFeNFeVerConNFe: TFloatField;
    QrPrmsEmpNFeNFeVerLotEve: TFloatField;
    QrPrmsEmpNFeNFeVerConsCad: TFloatField;
    QrPrmsEmpNFeNFeVerDistDFeInt: TFloatField;
    QrPrmsEmpNFeNFeVerDowNFe: TFloatField;
    QrPrmsEmpNFeide_tpImp: TSmallintField;
    QrPrmsEmpNFeNFeItsLin: TSmallintField;
    QrPrmsEmpNFeNFeFTRazao: TSmallintField;
    QrPrmsEmpNFeNFeFTEnder: TSmallintField;
    QrPrmsEmpNFeNFeFTFones: TSmallintField;
    QrPrmsEmpNFePathLogoNF: TWideStringField;
    QrPrmsEmpNFeDirDANFEs: TWideStringField;
    QrPrmsEmpNFeNFe_indFinalCpl: TSmallintField;
    QrPrmsEmpNFeNoDANFEMail: TSmallintField;
    QrPrmsEmpNFeNFeMaiusc: TSmallintField;
    QrPrmsEmpNFeNFeShowURL: TWideStringField;
    QrPrmsEmpNFeNFeNT2013_003LTT: TSmallintField;
    DsParamsEmp: TDataSource;
    DsPrmsEmpNFe: TDataSource;
    QrPrmsEmpNFeDirRetNfeDes: TWideStringField;
    QrPrmsEmpNFeDirEvePedMDe: TWideStringField;
    QrPrmsEmpNFeDirEveRetMDe: TWideStringField;
    QrPrmsEmpNFeDirDowNFeDes: TWideStringField;
    QrPrmsEmpNFeDirDowNFeNFe: TWideStringField;
    QrPrmsEmpNFeDirDistDFeInt: TWideStringField;
    QrPrmsEmpNFeDirRetDistDFeInt: TWideStringField;
    QrPrmsEmpNFeDirDowNFeCnf: TWideStringField;
    QrPrmsEmpNFeNFeInfCpl: TIntegerField;
    QrParamsEmp: TMySQLQuery;
    QrPrmsEmpNFeNO_PREMAILAUT: TWideStringField;
    QrPrmsEmpNFeNO_PREMAILCAN: TWideStringField;
    QrPrmsEmpNFeNO_PREMAILEVECCE: TWideStringField;
    QrParamsEmpNO_AssocModNF: TWideStringField;
    QrParamsEmpNOMEMOEDA: TWideStringField;
    QrParamsEmpCODUSU_MDA: TFloatField;
    QrParamsEmpFI_ASSOCIADA: TIntegerField;
    QrParamsEmpNO_ASSOCIADA: TWideStringField;
    QrParamsEmpNOMEFILIAL: TWideStringField;
    QrParamsEmpCNPJCPF_FILIAL: TWideStringField;
    QrParamsEmpFilial: TIntegerField;
    QrParamsEmpEntidade: TIntegerField;
    QrParamsEmpNO_CtaProdVen: TWideStringField;
    QrParamsEmpNO_CtaServico: TWideStringField;
    QrParamsEmpNO_CtaProdCom: TWideStringField;
    QrParamsEmpNO_CtaServicoPg: TWideStringField;
    QrParamsEmpNO_CartEmisHonFun: TWideStringField;
    QrParamsEmpNO_NFSePreMailAut: TWideStringField;
    QrParamsEmpNO_NFSePreMailCan: TWideStringField;
    QrParamsEmpNO_EntiTipCto: TWideStringField;
    QrParamsEmpNO_EntiTipCt1: TWideStringField;
    QrParamsEmpNO_CTD: TWideStringField;
    QrParamsEmpNO_CTB: TWideStringField;
    QrParamsEmphVeraoAsk_TXT: TWideStringField;
    QrParamsEmphVeraoIni_TXT: TWideStringField;
    QrParamsEmphVeraoFim_TXT: TWideStringField;
    QrParamsEmpNFeInfCpl_TXT: TWideStringField;
    QrParamsEmpNO_NFSeTipCtoMail: TWideStringField;
    QrParamsEmpCodigo: TIntegerField;
    QrParamsEmpLogo3x1: TWideStringField;
    QrParamsEmpTZD_UTC: TFloatField;
    QrParamsEmphVeraoAsk: TDateField;
    QrParamsEmphVeraoIni: TDateField;
    QrParamsEmphVeraoFim: TDateField;
    QrParamsEmpTZD_UTC_Auto: TSmallintField;
    QrParamsEmpNFeNT2013_003LTT: TSmallintField;
    QrParamsEmpCartEmisHonFun: TIntegerField;
    QrParamsEmpSituacao: TIntegerField;
    QrParamsEmpFatSemEstq: TSmallintField;
    QrParamsEmpTipMediaDD: TSmallintField;
    QrParamsEmpFatSemPrcL: TSmallintField;
    QrParamsEmpTipCalcJuro: TSmallintField;
    QrParamsEmpPedVdaMudLista: TSmallintField;
    QrParamsEmpPedVdaMudPrazo: TSmallintField;
    QrParamsEmpPediVdaNElertas: TSmallintField;
    QrParamsEmpAssocModNF: TIntegerField;
    QrParamsEmpFaturaSeq: TSmallintField;
    QrParamsEmpFaturaSep: TWideStringField;
    QrParamsEmpFaturaDta: TSmallintField;
    QrParamsEmpCtaProdVen: TIntegerField;
    QrParamsEmpTxtProdVen: TWideStringField;
    QrParamsEmpCtaServico: TIntegerField;
    QrParamsEmpTxtServico: TWideStringField;
    QrParamsEmpDupProdVen: TWideStringField;
    QrParamsEmpDupServico: TWideStringField;
    QrParamsEmpFaturaNum: TSmallintField;
    QrParamsEmpUsaReferen: TSmallintField;
    QrParamsEmpCtaProdCom: TIntegerField;
    QrParamsEmpTxtProdCom: TWideStringField;
    QrParamsEmpDupProdCom: TWideStringField;
    QrParamsEmpCtaServicoPg: TIntegerField;
    QrParamsEmpTxtServicoPg: TWideStringField;
    QrParamsEmpDupServicoPg: TWideStringField;
    QrParamsEmpCtaFretPrest: TIntegerField;
    QrParamsEmpTxtFretPrest: TWideStringField;
    QrParamsEmpDupFretPrest: TWideStringField;
    QrParamsEmpFreteRpICMS: TFloatField;
    QrParamsEmpFreteRpPIS: TFloatField;
    QrParamsEmpFreteRpCOFINS: TFloatField;
    QrParamsEmpRetImpost: TSmallintField;
    QrParamsEmpBalQtdItem: TFloatField;
    QrParamsEmpEstq0UsoCons: TSmallintField;
    QrParamsEmpPreMailAut: TIntegerField;
    QrParamsEmpPreMailCan: TIntegerField;
    QrParamsEmpPreMailEveCCe: TIntegerField;
    QrParamsEmpDirNFeProt: TWideStringField;
    QrParamsEmpDirNFeRWeb: TWideStringField;
    QrParamsEmpSCAN_Ser: TIntegerField;
    QrParamsEmpSCAN_nNF: TIntegerField;
    QrParamsEmpinfRespTec_xContato: TWideStringField;
    QrParamsEmpinfRespTec_CNPJ: TWideStringField;
    QrParamsEmpinfRespTec_fone: TWideStringField;
    QrParamsEmpinfRespTec_email: TWideStringField;
    QrParamsEmpinfRespTec_idCSRT: TWideStringField;
    QrParamsEmpinfRespTec_CSRT: TWideStringField;
    QrParamsEmpinfRespTec_Usa: TSmallintField;
    QrParamsEmpUF_WebServ: TWideStringField;
    QrParamsEmpSiglaCustm: TWideStringField;
    QrParamsEmpUF_Servico: TWideStringField;
    QrParamsEmpSimplesFed: TSmallintField;
    QrParamsEmpInfoPerCuz: TSmallintField;
    QrParamsEmpNFeSerNum: TWideStringField;
    QrParamsEmpNFeSerVal: TDateField;
    QrParamsEmpNFeSerAvi: TSmallintField;
    QrParamsEmpUF_MDeMDe: TWideStringField;
    QrParamsEmpUF_MDeDes: TWideStringField;
    QrParamsEmpUF_MDeNFe: TWideStringField;
    QrParamsEmpNFeUF_EPEC: TWideStringField;
    QrParamsEmpDirEnvLot: TWideStringField;
    QrParamsEmpDirRec: TWideStringField;
    QrParamsEmpDirProRec: TWideStringField;
    QrParamsEmpDirNFeAss: TWideStringField;
    QrParamsEmpDirNFeGer: TWideStringField;
    QrParamsEmpDirDen: TWideStringField;
    QrParamsEmpDirPedRec: TWideStringField;
    QrParamsEmpDirPedSta: TWideStringField;
    QrParamsEmpDirSta: TWideStringField;
    QrParamsEmpDirPedCan: TWideStringField;
    QrParamsEmpDirCan: TWideStringField;
    QrParamsEmpDirPedInu: TWideStringField;
    QrParamsEmpDirInu: TWideStringField;
    QrParamsEmpDirPedSit: TWideStringField;
    QrParamsEmpDirSit: TWideStringField;
    QrParamsEmpDirEveEnvLot: TWideStringField;
    QrParamsEmpDirEveRetLot: TWideStringField;
    QrParamsEmpDirEvePedCCe: TWideStringField;
    QrParamsEmpDirEveRetCCe: TWideStringField;
    QrParamsEmpDirEvePedCan: TWideStringField;
    QrParamsEmpDirEveRetCan: TWideStringField;
    QrParamsEmpDirEveProcCCe: TWideStringField;
    QrParamsEmpDirRetNfeDes: TWideStringField;
    QrParamsEmpDirEvePedMDe: TWideStringField;
    QrParamsEmpDirEveRetMDe: TWideStringField;
    QrParamsEmpDirDowNFeDes: TWideStringField;
    QrParamsEmpDirDowNFeNFe: TWideStringField;
    QrParamsEmpDirDistDFeInt: TWideStringField;
    QrParamsEmpDirRetDistDFeInt: TWideStringField;
    QrParamsEmpDirDowNFeCnf: TWideStringField;
    QrParamsEmpversao: TFloatField;
    QrParamsEmpide_mod: TSmallintField;
    QrParamsEmpide_tpAmb: TSmallintField;
    QrParamsEmpDirSchema: TWideStringField;
    QrParamsEmpNT2018_05v120: TSmallintField;
    QrParamsEmpAppCode: TSmallintField;
    QrParamsEmpAssDigMode: TSmallintField;
    QrParamsEmpMyEmailNFe: TWideStringField;
    QrParamsEmpNFeInfCpl: TIntegerField;
    QrParamsEmpCRT: TSmallintField;
    QrParamsEmpCSOSN: TIntegerField;
    QrParamsEmppCredSNAlq: TFloatField;
    QrParamsEmppCredSNMez: TIntegerField;
    QrParamsEmpNFeVerStaSer: TFloatField;
    QrParamsEmpNFeVerEnvLot: TFloatField;
    QrParamsEmpNFeVerConLot: TFloatField;
    QrParamsEmpNFeVerCanNFe: TFloatField;
    QrParamsEmpNFeVerInuNum: TFloatField;
    QrParamsEmpNFeVerConNFe: TFloatField;
    QrParamsEmpNFeVerLotEve: TFloatField;
    QrParamsEmpNFeVerConsCad: TFloatField;
    QrParamsEmpNFeVerDistDFeInt: TFloatField;
    QrParamsEmpNFeVerDowNFe: TFloatField;
    QrParamsEmpide_tpImp: TSmallintField;
    QrParamsEmpNFeItsLin: TSmallintField;
    QrParamsEmpNFeFTRazao: TSmallintField;
    QrParamsEmpNFeFTEnder: TSmallintField;
    QrParamsEmpNFeFTFones: TSmallintField;
    QrParamsEmpNFeMaiusc: TSmallintField;
    QrParamsEmpNFeShowURL: TWideStringField;
    QrParamsEmpPathLogoNF: TWideStringField;
    QrParamsEmpDirDANFEs: TWideStringField;
    QrParamsEmpNFe_indFinalCpl: TSmallintField;
    QrParamsEmpNoDANFEMail: TSmallintField;
    QrParamsEmpSPED_EFD_IND_PERFIL: TWideStringField;
    QrParamsEmpSPED_EFD_IND_ATIV: TSmallintField;
    QrParamsEmpSPED_EFD_CadContador: TIntegerField;
    QrParamsEmpSPED_EFD_CRCContador: TWideStringField;
    QrParamsEmpSPED_EFD_EscriContab: TIntegerField;
    QrParamsEmpSPED_EFD_EnderContab: TSmallintField;
    QrParamsEmpSPED_EFD_ID_0150: TSmallintField;
    QrParamsEmpSPED_EFD_ID_0200: TSmallintField;
    QrParamsEmpSPED_EFD_Peri_K100: TSmallintField;
    QrParamsEmpSPED_EFD_Peri_E500: TSmallintField;
    QrParamsEmpSPED_EFD_Peri_E100: TSmallintField;
    QrParamsEmpSPED_EFD_Producao: TSmallintField;
    QrParamsEmpSPED_EFD_MovSubPrd: TSmallintField;
    QrParamsEmpSPED_EFD_Path: TWideStringField;
    QrParamsEmpSPED_EFD_ICMS_IPI_VersaoGuia: TWideStringField;
    QrParamsEmpNFSeMetodo: TIntegerField;
    QrParamsEmpNFSeVersao: TFloatField;
    QrParamsEmpNFSeSerieRps: TWideStringField;
    QrParamsEmpDpsNumero: TIntegerField;
    QrParamsEmpNFSeWSProducao: TWideStringField;
    QrParamsEmpNFSeAmbiente: TSmallintField;
    QrParamsEmpNFSeWSHomologa: TWideStringField;
    QrParamsEmpNFSeCertDigital: TWideStringField;
    QrParamsEmpNFSeCertValidad: TDateField;
    QrParamsEmpNFSeCertAviExpi: TSmallintField;
    QrParamsEmpRPSNumLote: TIntegerField;
    QrParamsEmpNFSeUserWeb: TWideStringField;
    QrParamsEmpNFSeSenhaWeb: TWideStringField;
    QrParamsEmpNFSeCodMunici: TIntegerField;
    QrParamsEmpNFSePrefeitura1: TWideStringField;
    QrParamsEmpNFSeMsgVisu: TSmallintField;
    QrParamsEmpNFSeLogoFili: TWideStringField;
    QrParamsEmpNFSeLogoPref: TWideStringField;
    QrParamsEmpNFSePrefeitura2: TWideStringField;
    QrParamsEmpNFSePrefeitura3: TWideStringField;
    QrParamsEmpNFSeMetodEnvRPS: TSmallintField;
    QrParamsEmpNFSeTipoRps: TSmallintField;
    QrParamsEmpNFSe_indFinalCpl: TSmallintField;
    QrParamsEmpDirNFSeRPSEnvLot: TWideStringField;
    QrParamsEmpDirNFSeRPSRecLot: TWideStringField;
    QrParamsEmpDirNFSeDPSAss: TWideStringField;
    QrParamsEmpDirNFSeDPSGer: TWideStringField;
    QrParamsEmpDirNFSeLogs: TWideStringField;
    QrParamsEmpDirNFSeSchema: TWideStringField;
    QrParamsEmpNFSePreMailAut: TIntegerField;
    QrParamsEmpNFSePreMailCan: TIntegerField;
    QrParamsEmpMyEmailNFSe: TWideStringField;
    QrParamsEmpCTetpEmis: TSmallintField;
    QrParamsEmpCTePreMailAut: TIntegerField;
    QrParamsEmpCTePreMailCan: TIntegerField;
    QrParamsEmpCTePreMailEveCCe: TIntegerField;
    QrParamsEmpDirCTeProt: TWideStringField;
    QrParamsEmpDirCTeRWeb: TWideStringField;
    QrParamsEmpCTeSCAN_Ser: TIntegerField;
    QrParamsEmpCTeSCAN_nCT: TIntegerField;
    QrParamsEmpCTeUF_WebServ: TWideStringField;
    QrParamsEmpCTeUF_Servico: TWideStringField;
    QrParamsEmpCTeSerNum: TWideStringField;
    QrParamsEmpCTeSerVal: TDateField;
    QrParamsEmpCTeSerAvi: TSmallintField;
    QrParamsEmpCTeUF_EPEC: TWideStringField;
    QrParamsEmpCTeUF_Conting: TWideStringField;
    QrParamsEmpDirCTeEnvLot: TWideStringField;
    QrParamsEmpDirCTeRec: TWideStringField;
    QrParamsEmpDirCTeProRec: TWideStringField;
    QrParamsEmpDirCTeAss: TWideStringField;
    QrParamsEmpDirCTeGer: TWideStringField;
    QrParamsEmpDirCTeDen: TWideStringField;
    QrParamsEmpDirCTePedRec: TWideStringField;
    QrParamsEmpDirCTePedSta: TWideStringField;
    QrParamsEmpDirCTeSta: TWideStringField;
    QrParamsEmpDirCTePedCan: TWideStringField;
    QrParamsEmpDirCTeCan: TWideStringField;
    QrParamsEmpDirCTePedInu: TWideStringField;
    QrParamsEmpDirCTeInu: TWideStringField;
    QrParamsEmpDirCTePedSit: TWideStringField;
    QrParamsEmpDirCTeSit: TWideStringField;
    QrParamsEmpDirCTeEveEnvLot: TWideStringField;
    QrParamsEmpDirCTeEveRetLot: TWideStringField;
    QrParamsEmpDirCTeEvePedCCe: TWideStringField;
    QrParamsEmpDirCTeEveRetCCe: TWideStringField;
    QrParamsEmpDirCTeEvePedCan: TWideStringField;
    QrParamsEmpDirCTeEveRetCan: TWideStringField;
    QrParamsEmpDirCTEEveProcCCe: TWideStringField;
    QrParamsEmpDirCTeEvePedEPEC: TWideStringField;
    QrParamsEmpDirCTeEveRetEPEC: TWideStringField;
    QrParamsEmpCTeversao: TFloatField;
    QrParamsEmpCTeide_mod: TSmallintField;
    QrParamsEmpCTeide_tpAmb: TSmallintField;
    QrParamsEmpDirCTeSchema: TWideStringField;
    QrParamsEmpCTeAppCode: TSmallintField;
    QrParamsEmpCTeAssDigMode: TSmallintField;
    QrParamsEmpMyEmailCTe: TWideStringField;
    QrParamsEmpCTeInfCpl: TIntegerField;
    QrParamsEmpCTeVerStaSer: TFloatField;
    QrParamsEmpCTeVerEnvLot: TFloatField;
    QrParamsEmpCTeVerConLot: TFloatField;
    QrParamsEmpCTeVerInuNum: TFloatField;
    QrParamsEmpCTeVerConCTe: TFloatField;
    QrParamsEmpCTeVerLotEve: TFloatField;
    QrParamsEmpCTeVerEPEC: TFloatField;
    QrParamsEmpCTeide_tpImp: TSmallintField;
    QrParamsEmpCTeItsLin: TSmallintField;
    QrParamsEmpCTeFTRazao: TSmallintField;
    QrParamsEmpCTeFTEnder: TSmallintField;
    QrParamsEmpCTeFTFones: TSmallintField;
    QrParamsEmpCTeMaiusc: TSmallintField;
    QrParamsEmpCTeShowURL: TWideStringField;
    QrParamsEmpPathLogoCTe: TWideStringField;
    QrParamsEmpDirDACTes: TWideStringField;
    QrParamsEmpCTe_indFinalCpl: TSmallintField;
    QrParamsEmpMDFePreMailAut: TIntegerField;
    QrParamsEmpMDFePreMailCan: TIntegerField;
    QrParamsEmpMDFePreMailEveCCe: TIntegerField;
    QrParamsEmpDirMDFeProt: TWideStringField;
    QrParamsEmpDirMDFeRWeb: TWideStringField;
    QrParamsEmpMDFeSCAN_Ser: TIntegerField;
    QrParamsEmpMDFeSCAN_nMDF: TIntegerField;
    QrParamsEmpMDFeUF_WebServ: TWideStringField;
    QrParamsEmpMDFeUF_Servico: TWideStringField;
    QrParamsEmpMDFeSerNum: TWideStringField;
    QrParamsEmpMDFeSerVal: TDateField;
    QrParamsEmpMDFeSerAvi: TSmallintField;
    QrParamsEmpDirMDFeEnvLot: TWideStringField;
    QrParamsEmpDirMDFeRec: TWideStringField;
    QrParamsEmpDirMDFeProRec: TWideStringField;
    QrParamsEmpDirMDFeAss: TWideStringField;
    QrParamsEmpDirMDFeGer: TWideStringField;
    QrParamsEmpDirMDFeDen: TWideStringField;
    QrParamsEmpDirMDFePedRec: TWideStringField;
    QrParamsEmpDirMDFePedSta: TWideStringField;
    QrParamsEmpDirMDFeSta: TWideStringField;
    QrParamsEmpDirMDFePedCan: TWideStringField;
    QrParamsEmpDirMDFeCan: TWideStringField;
    QrParamsEmpDirMDFePedInu: TWideStringField;
    QrParamsEmpDirMDFeInu: TWideStringField;
    QrParamsEmpDirMDFePedSit: TWideStringField;
    QrParamsEmpDirMDFeSit: TWideStringField;
    QrParamsEmpDirMDFeEveEnvLot: TWideStringField;
    QrParamsEmpDirMDFeEveRetLot: TWideStringField;
    QrParamsEmpDirMDFeEvePedEnc: TWideStringField;
    QrParamsEmpDirMDFeEveRetEnc: TWideStringField;
    QrParamsEmpDirMDFeEvePedCan: TWideStringField;
    QrParamsEmpDirMDFeEveRetCan: TWideStringField;
    QrParamsEmpDirMDFeEvePedIdC: TWideStringField;
    QrParamsEmpDirMDFeEveRetIdC: TWideStringField;
    QrParamsEmpMDFeversao: TFloatField;
    QrParamsEmpMDFeide_mod: TSmallintField;
    QrParamsEmpMDFeide_tpAmb: TSmallintField;
    QrParamsEmpDirMDFeSchema: TWideStringField;
    QrParamsEmpMDFeAppCode: TSmallintField;
    QrParamsEmpMDFeAssDigMode: TSmallintField;
    QrParamsEmpMyEmailMDFe: TWideStringField;
    QrParamsEmpMDFeInfCpl: TIntegerField;
    QrParamsEmpMDFeVerStaSer: TFloatField;
    QrParamsEmpMDFeVerEnvLot: TFloatField;
    QrParamsEmpMDFeVerConLot: TFloatField;
    QrParamsEmpMDFeVerInuNum: TFloatField;
    QrParamsEmpMDFeVerConMDFe: TFloatField;
    QrParamsEmpMDFeVerLotEve: TFloatField;
    QrParamsEmpMDFeide_tpImp: TSmallintField;
    QrParamsEmpMDFeItsLin: TSmallintField;
    QrParamsEmpMDFeFTRazao: TSmallintField;
    QrParamsEmpMDFeFTEnder: TSmallintField;
    QrParamsEmpMDFeFTFones: TSmallintField;
    QrParamsEmpMDFeMaiusc: TSmallintField;
    QrParamsEmpMDFeShowURL: TWideStringField;
    QrParamsEmpPathLogoMDFe: TWideStringField;
    QrParamsEmpDirDAMDFes: TWideStringField;
    QrParamsEmpMDFe_indFinalCpl: TSmallintField;
    QrParamsEmpMoeda: TIntegerField;
    QrParamsEmpAssociada: TIntegerField;
    QrParamsEmpEntiTipCt1: TIntegerField;
    QrParamsEmpEntiTipCto: TIntegerField;
    QrParamsEmpNFSeTipCtoMail: TIntegerField;
    QrParamsEmpCTeEntiTipCt1: TIntegerField;
    QrParamsEmpCTeEntiTipCto: TIntegerField;
    QrParamsEmppCredSNMez_TXT: TWideStringField;
    QrParamsEmpTZD_UTC_Str: TWideStringField;
    QrPrmsEmpMis: TMySQLQuery;
    QrPrmsEmpMisTZD_UTC_Auto_DataHoraAceite: TDateTimeField;
    QrPrmsEmpMisTZD_UTC_Auto_TermoAceite: TIntegerField;
    QrPrmsEmpMisTZD_UTC_Auto_UsuarioAceite: TIntegerField;
    QrPrmsEmpMisSituacao: TIntegerField;
    DsPrmsEmpMis: TDataSource;
    GroupBox4: TGroupBox;
    Panel25: TPanel;
    Label19: TLabel;
    EdCSC: TdmkEdit;
    GroupBox8: TGroupBox;
    Panel26: TPanel;
    Label26: TLabel;
    QrParamsEmpCSC: TWideStringField;
    DBEdit8: TDBEdit;
    BtSerieNFC: TBitBtn;
    BtSerieCTe: TBitBtn;
    BtSerieMDFe: TBitBtn;
    BtCertDigital: TBitBtn;
    PMSerieNFC: TPopupMenu;
    Incluinovasriedecupomfiscal1: TMenuItem;
    Alterasriedecupomfiscalatual1: TMenuItem;
    Excluisriedecupomfiscal1: TMenuItem;
    QrParamsNFCs: TMySQLQuery;
    QrParamsNFCsCodigo: TIntegerField;
    QrParamsNFCsControle: TIntegerField;
    QrParamsNFCsSequencial: TIntegerField;
    QrParamsNFCsIncSeqAuto: TIntegerField;
    QrParamsNFCsNO_IncSeqAuto: TWideStringField;
    QrParamsNFCsMaxSeqLib: TIntegerField;
    QrParamsNFCsSerieNF: TIntegerField;
    DsParamsNFCs: TDataSource;
    Label76: TLabel;
    EdCSCpos: TdmkEdit;
    Label78: TLabel;
    DBEdit46: TDBEdit;
    TabSheet16: TTabSheet;
    DBGrid4: TDBGrid;
    QrParamsEmpCSCpos: TWideStringField;
    Label79: TLabel;
    DBEdit47: TDBEdit;
    DBEdit48: TDBEdit;
    Label80: TLabel;
    DBEdit49: TDBEdit;
    DBEdit170: TDBEdit;
    Label81: TLabel;
    QrParamsEmpDupMultiSV: TWideStringField;
    QrParamsEmpCtaMultiSV: TIntegerField;
    QrParamsEmpTxtMultiSV: TWideStringField;
    QrParamsEmpNO_CtaMultiSV: TWideStringField;
    Label82: TLabel;
    EdCtaMultiSV: TdmkEditCB;
    CBCtaMultiSV: TdmkDBLookupComboBox;
    Label83: TLabel;
    SpeedButton6: TSpeedButton;
    EdTxtMultiSV: TdmkEdit;
    Label111: TLabel;
    EdDupMultiSV: TdmkEdit;
    Label457: TLabel;
    DBEdit171: TDBEdit;
    DBEdit172: TDBEdit;
    Label458: TLabel;
    DBEdit173: TDBEdit;
    QrCtaFretPrest: TMySQLQuery;
    IntegerField9: TIntegerField;
    WideStringField1: TWideStringField;
    DsCtaFretPrest: TDataSource;
    QrParamsEmpNO_CtaFretPrest: TWideStringField;
    TabSheet17: TTabSheet;
    Panel27: TPanel;
    Label459: TLabel;
    EdTipoPrintNFCe: TdmkEdit;
    Label460: TLabel;
    EdNomePrintNFCe: TdmkEdit;
    CkAutoCutNFce: TdmkCheckBox;
    QrPrmsEmpNFeTipoPrintNFCe: TWideStringField;
    QrPrmsEmpNFeNomePrintNFCe: TWideStringField;
    QrPrmsEmpNFeAutoCutNFCe: TSmallintField;
    TabSheet18: TTabSheet;
    Panel31: TPanel;
    Label461: TLabel;
    Label462: TLabel;
    DBEdit174: TDBEdit;
    DBEdit175: TDBEdit;
    DBCheckBox2: TDBCheckBox;
    EdIPPrintNFCe: TdmkEdit;
    Label463: TLabel;
    QrPrmsEmpNFeIPPrintNFCe: TWideStringField;
    Label464: TLabel;
    DBEdit176: TDBEdit;
    QrPrmsEmpNFeCertDigPfxCam: TWideStringField;
    QrPrmsEmpNFeCertDigPfxPwd: TWideStringField;
    QrPrmsEmpNFeCertDigPfxWay: TSmallintField;
    Label14: TLabel;
    Label65: TLabel;
    Label278: TLabel;
    Label281: TLabel;
    Label298: TLabel;
    RGNFeUsoCEST: TdmkRadioGroup;
    QrPrmsEmpNFeNFeUsoCEST: TSmallintField;
    DBRadioGroup21: TDBRadioGroup;
    DBCheckBox3: TDBCheckBox;
    CkSimplesFed: TdmkCheckBox;
    Label467: TLabel;
    Label468: TLabel;
    RG2CertDigPfxLoc: TdmkRadioGroup;
    Panel107: TPanel;
    dmkRadioGroup9: TdmkRadioGroup;
    GroupBox9: TGroupBox;
    Label90: TLabel;
    DBEdit52: TDBEdit;
    DBEdit53: TDBEdit;
    Label91: TLabel;
    DBEdit54: TDBEdit;
    DBEdit55: TDBEdit;
    Label216: TLabel;
    DBEdit117: TDBEdit;
    DBEdit118: TDBEdit;
    DBEdit177: TDBEdit;
    DBRadioGroup22: TDBRadioGroup;
    DFe: TTabSheet;
    Panel112: TPanel;
    PCACBr: TPageControl;
    TabSheet19: TTabSheet;
    TabSheet58: TTabSheet;
    GroupBox11: TGroupBox;
    sbtnPathSalvar: TSpeedButton;
    Label474: TLabel;
    Label479: TLabel;
    spPathSchemas: TSpeedButton;
    edtPathLogs: TEdit;
    ckSalvar: TCheckBox;
    cbxAtualizarXML: TCheckBox;
    cbxExibirErroSchema: TCheckBox;
    edtFormatoAlerta: TdmkEdit;
    cbxRetirarAcentos: TCheckBox;
    edtPathSchemas: TEdit;
    TabSheet59: TTabSheet;
    GroupBox12: TGroupBox;
    lTimeOut: TLabel;
    lSSLLib1: TLabel;
    cbxVisualizar: TCheckBox;
    cbxSalvarSOAP: TCheckBox;
    seTimeOut: TSpinEdit;
    cbSSLType: TComboBox;
    gbxRetornoEnvio: TGroupBox;
    Label485: TLabel;
    Label486: TLabel;
    Label487: TLabel;
    cbxAjustarAut: TCheckBox;
    edtTentativas: TdmkEdit;
    edtIntervalo: TdmkEdit;
    edtAguardar: TdmkEdit;
    TabSheet61: TTabSheet;
    cbxSalvarArqs: TCheckBox;
    cbxPastaMensal: TCheckBox;
    cbxAdicionaLiteral: TCheckBox;
    cbxEmissaoPathNFe: TCheckBox;
    cbxSalvaPathEvento: TCheckBox;
    cbxSepararPorCNPJ: TCheckBox;
    cbxSepararPorModelo: TCheckBox;
    gbProxy: TGroupBox;
    Label481: TLabel;
    Label482: TLabel;
    Label483: TLabel;
    Label484: TLabel;
    edtProxyHost: TEdit;
    edtProxyPorta: TEdit;
    edtProxyUser: TEdit;
    edtProxySenha: TEdit;
    Panel113: TPanel;
    Panel114: TPanel;
    btnLeituraX509: TButton;
    btnHTTPS: TButton;
    GroupBox10: TGroupBox;
    Edit1: TEdit;
    btnSha256: TButton;
    cbAssinar: TCheckBox;
    btnIssuerName: TButton;
    cbSSLLib: TComboBox;
    btVersao: TButton;
    cbXmlSignLib: TComboBox;
    cbHttpLib: TComboBox;
    cbCryptLib: TComboBox;
    lXmlSign: TLabel;
    lHttpLib: TLabel;
    lCryptLib: TLabel;
    lSSLLib: TLabel;
    gbCertificado: TGroupBox;
    Label469: TLabel;
    Label470: TLabel;
    sbtnCaminhoCert: TSpeedButton;
    Label471: TLabel;
    sbtnGetCert: TSpeedButton;
    sbtnNumSerie: TSpeedButton;
    Label472: TLabel;
    edtCaminho: TdmkEdit;
    edtSenha: TdmkEdit;
    edtNumSerie: TdmkEdit;
    edtURLPFX: TEdit;
    btnCNPJ: TButton;
    btnSubName: TButton;
    btnNumSerie: TButton;
    btnDataValidade: TButton;
    Panel115: TPanel;
    pgRespostas: TPageControl;
    TabSheet60: TTabSheet;
    MemoResp: TMemo;
    TabSheet62: TTabSheet;
    WBResposta: TWebBrowser;
    TabSheet63: TTabSheet;
    memoLog: TMemo;
    TabSheet64: TTabSheet;
    trvwDocumento: TTreeView;
    TabSheet65: TTabSheet;
    memoRespWS: TMemo;
    Dados: TTabSheet;
    MemoDados: TMemo;
    pgcBotoes: TPageControl;
    tsEnvios: TTabSheet;
    btnCriarEnviar: TButton;
    btnValidarRegrasNegocio: TButton;
    btnGerarTXT: TButton;
    btnGerarXML: TButton;
    btnImportarXML: TButton;
    btnGerarPDF: TButton;
    btnValidarXML: TButton;
    btnImprimir: TButton;
    btnEnviarEmail: TButton;
    btnAdicionarProtocolo: TButton;
    btnCarregarXMLEnviar: TButton;
    btnValidarAssinatura: TButton;
    btnImprimirDANFCE: TButton;
    btnImprimirDANFCEOffline: TButton;
    tsConsultas: TTabSheet;
    btnConsultar: TButton;
    btnConsultarChave: TButton;
    btnConsCad: TButton;
    btnConsultarRecibo: TButton;
    btnStatusServ: TButton;
    tsEventos: TTabSheet;
    btnCancelarXML: TButton;
    btnCancelarChave: TButton;
    btnCartadeCorrecao: TButton;
    btnImprimirEvento: TButton;
    btnEnviarEventoEmail: TButton;
    btnAtorInterNFeTransp: TButton;
    tsInutilizacao: TTabSheet;
    btnInutilizar: TButton;
    btnInutilizarImprimir: TButton;
    tsDistribuicao: TTabSheet;
    btnManifDestConfirmacao: TButton;
    btnDistrDFePorUltNSU: TButton;
    btnDistrDFePorNSU: TButton;
    btnDistrDFePorChave: TButton;
    btnManifDestDesconnhecimento: TButton;
    QrParamsEmpNO_CtbCadGruGer: TWideStringField;
    Label473: TLabel;
    DBEdit178: TDBEdit;
    DBEdit179: TDBEdit;
    Label475: TLabel;
    EdCtbCadGruGer: TdmkEditCB;
    CBCtbCadGruGer: TdmkDBLookupComboBox;
    SbCtbCadGruGer: TSpeedButton;
    DsCtbCadGruGer: TDataSource;
    QrCtbCadGruGer: TMySQLQuery;
    QrCtbCadGruGerCodigo: TIntegerField;
    QrCtbCadGruGerNome: TWideStringField;
    QrParamsEmpCtbCadGruGer: TFloatField;
    TabSheet66: TTabSheet;
    Panel116: TPanel;
    GroupBox41: TGroupBox;
    Panel117: TPanel;
    Label476: TLabel;
    EdUCTextoB: TdmkEdit;
    Label477: TLabel;
    EdUCTextoIPI_CST: TdmkEdit;
    Label478: TLabel;
    EdUCTextoPIS_CST: TdmkEdit;
    Label480: TLabel;
    EdUCTextoCOFINS_CST: TdmkEdit;
    EdUC_ICMS_CST_B: TdmkEdit;
    EdUC_IPI_CST: TdmkEdit;
    EdUC_PIS_CST: TdmkEdit;
    EdUC_COFINS_CST: TdmkEdit;
    QrPrmsEmpNFeUC_ICMS_CST_B: TWideStringField;
    QrPrmsEmpNFeUC_IPI_CST: TWideStringField;
    QrPrmsEmpNFeUC_PIS_CST: TWideStringField;
    QrPrmsEmpNFeUC_COFINS_CST: TWideStringField;
    Label488: TLabel;
    EdCLAS_ESTAB_IND: TdmkEdit;
    EdCLAS_ESTAB_IND_TXT: TdmkEdit;
    QrParamsEmpCLAS_ESTAB_IND: TSmallintField;
    EdSPED_EFD_IND_NAT_PJ_TXT: TdmkEdit;
    Label489: TLabel;
    EdSPED_EFD_IND_NAT_PJ: TdmkEdit;
    QrParamsEmpSPED_EFD_IND_NAT_PJ: TWideStringField;
    Panel118: TPanel;
    Panel119: TPanel;
    GroupBox24: TGroupBox;
    Label122: TLabel;
    Label123: TLabel;
    Label124: TLabel;
    EdSPED_EFD_CadContador: TdmkEditCB;
    CBSPED_EFD_CadContador: TdmkDBLookupComboBox;
    EdSPED_EFD_CRCContador: TdmkEdit;
    EdSPED_EFD_EscriContab: TdmkEditCB;
    CBSPED_EFD_EscriContab: TdmkDBLookupComboBox;
    RGSPED_EFD_EnderContab: TdmkRadioGroup;
    Panel103: TPanel;
    GroupBox26: TGroupBox;
    Label128: TLabel;
    SpeedButton17: TSpeedButton;
    EdSPED_EFD_Path: TdmkEdit;
    GroupBox66: TGroupBox;
    Label438: TLabel;
    SpeedButton51: TSpeedButton;
    EdSPED_EFD_ICMS_IPI_VersaoGuia: TdmkEdit;
    Panel120: TPanel;
    Label490: TLabel;
    EdSPED_COD_INC_TRIB: TdmkEdit;
    EdCOD_INC_TRIB_TXT: TdmkEdit;
    Label491: TLabel;
    EdSPED_IND_APRO_CRED: TdmkEdit;
    EdIND_APRO_CRED_TXT: TdmkEdit;
    EdCOD_TIPO_CONT_TXT: TdmkEdit;
    EdSPED_COD_TIPO_CONT: TdmkEdit;
    Label492: TLabel;
    EdIND_REG_CUM_TXT: TdmkEdit;
    EdSPED_IND_REG_CUM: TdmkEdit;
    Label493: TLabel;
    QrParamsEmpSPED_COD_INC_TRIB: TWideStringField;
    QrParamsEmpSPED_IND_APRO_CRED: TWideStringField;
    QrParamsEmpSPED_COD_TIPO_CONT: TWideStringField;
    QrParamsEmpSPED_IND_REG_CUM: TWideStringField;
    RGSPED_EFD_DtaFiscalEntrada: TdmkRadioGroup;
    QrParamsEmpSPED_EFD_DtaFiscalSaida: TSmallintField;
    QrParamsEmpSPED_EFD_DtaFiscalEntrada: TSmallintField;
    Panel121: TPanel;
    RGSPED_EFD_Peri_E100: TdmkRadioGroup;
    RGSPED_EFD_Peri_K100: TdmkRadioGroup;
    RGSPED_EFD_Peri_E500: TdmkRadioGroup;
    RGRegimTributar: TdmkRadioGroup;
    QrParamsEmpRegimTributar: TSmallintField;
    QrParamsEmpRegimPisCofins: TSmallintField;
    Panel122: TPanel;
    RGRegimPisCofins: TdmkRadioGroup;
    RGRegimCumulativ: TdmkRadioGroup;
    QrParamsEmpRegimCumulativ: TSmallintField;
    Panel40: TPanel;
    GroupBox22: TGroupBox;
    Label119: TLabel;
    Label120: TLabel;
    EdDBSPED_EFD_IND_PERFIL: TdmkEdit;
    EdDBSPED_EFD_IND_ATIV: TdmkEdit;
    DBEdit66: TDBEdit;
    DBEdit67: TDBEdit;
    GroupBox23: TGroupBox;
    Label121: TLabel;
    Label125: TLabel;
    Label126: TLabel;
    DBRadioGroup99: TDBRadioGroup;
    DBEdit68: TDBEdit;
    DBEdit69: TDBEdit;
    DBEdit70: TDBEdit;
    DBEdit71: TDBEdit;
    DBEdit72: TDBEdit;
    GroupBox63: TGroupBox;
    Panel95: TPanel;
    dmkRadioGroup2: TDBRadioGroup;
    dmkRadioGroup3: TDBRadioGroup;
    dmkRadioGroup4: TDBRadioGroup;
    Panel100: TPanel;
    Panel101: TPanel;
    dmkRadioGroup5: TDBRadioGroup;
    dmkRadioGroup6: TDBRadioGroup;
    dmkRadioGroup7: TDBRadioGroup;
    DBRadioGroup20: TDBRadioGroup;
    Panel111: TPanel;
    GroupBox25: TGroupBox;
    Label127: TLabel;
    DBEdit73: TDBEdit;
    GroupBox65: TGroupBox;
    Label456: TLabel;
    DBEdit169: TDBEdit;
    DBRadioGroup23: TDBRadioGroup;
    Panel123: TPanel;
    EdCodDctfPis: TdmkEdit;
    Label494: TLabel;
    EdCodDctfCofins: TdmkEdit;
    Label495: TLabel;
    QrParamsEmpCodDctfPis: TWideStringField;
    QrParamsEmpCodDctfCofins: TWideStringField;
    GroupBox70: TGroupBox;
    Panel124: TPanel;
    Label496: TLabel;
    Label497: TLabel;
    EdUCFisRegMesmaUF_TXT: TdmkEdit;
    EdUCFisRegOutraUF_TXT: TdmkEdit;
    EdUCFisRegMesmaUF: TdmkEdit;
    EdUCFisRegOutraUF: TdmkEdit;
    DBEdit180: TDBEdit;
    Label498: TLabel;
    EdDirEvePedEPEC: TdmkEdit;
    Label499: TLabel;
    EdDirEveRetEPEC: TdmkEdit;
    SbDirEvePedEPEC: TSpeedButton;
    SbDirEveRetEPEC: TSpeedButton;
    QrPrmsEmpNFeDirEvePedEPEC: TWideStringField;
    QrPrmsEmpNFeDirEveRetEPEC: TWideStringField;
    RGUCFisRegMadBy: TdmkRadioGroup;
    Label500: TLabel;
    EdUCCartPag: TdmkEdit;
    EdUCCartPag_TXT: TdmkEdit;
    Panel125: TPanel;
    CkForcaACBrNestePC: TCheckBox;
    TabSheet68: TTabSheet;
    Panel126: TPanel;
    Label73: TLabel;
    SbNFeSerNum: TSpeedButton;
    Label92: TLabel;
    Label93: TLabel;
    Label94: TLabel;
    Label465: TLabel;
    SbCertDigPfxCam: TSpeedButton;
    Label466: TLabel;
    EdNFeSerNum: TdmkEdit;
    TPNFeSerVal: TdmkEditDateTimePicker;
    EdNFeSerAvi: TdmkEdit;
    EdCertDigPfxCam: TdmkEdit;
    EdCertDigPfxPwd: TdmkEdit;
    RGCertDigPfxWay: TdmkRadioGroup;
    RGCertDigPfxLoc: TdmkRadioGroup;
    Label501: TLabel;
    EdDirNFSeIniFiles: TdmkEdit;
    SbDirNFSeIniFiles: TSpeedButton;
    GroupBox71: TGroupBox;
    Panel127: TPanel;
    GroupBox72: TGroupBox;
    Panel128: TPanel;
    GroupBox73: TGroupBox;
    Panel129: TPanel;
    EdDpsNumero: TdmkEdit;
    Label163: TLabel;
    EdRPSNumLote: TdmkEdit;
    Label188: TLabel;
    EdNFSeUserWeb: TdmkEdit;
    Label191: TLabel;
    Label192: TLabel;
    EdNFSeSenhaWeb: TdmkEdit;
    Label193: TLabel;
    EdNFSeCodMunici: TdmkEdit;
    CkNFSeMsgVisu: TdmkCheckBox;
    Label502: TLabel;
    EdDpsNumHom: TdmkEdit;
    Label503: TLabel;
    EdHomNumLote: TdmkEdit;
    QrParamsEmpDpsNumHom: TIntegerField;
    QrParamsEmpHomNumLote: TIntegerField;
    QrParamsEmpDirNFSeIniFiles: TWideStringField;
    EdNFSeWebFraseSecr: TdmkEdit;
    Label504: TLabel;
    QrParamsEmpNFSeWebFraseSecr: TWideStringField;
    Label173: TLabel;
    EdDirNFSeDPSAss: TdmkEdit;
    SbDirNFSeDPSAss: TSpeedButton;
    QrParamsEmpDirNFSeNFSAut: TWideStringField;
    Label176: TLabel;
    EdDirNFSeNFSCan: TdmkEdit;
    SbDirNFSeNFSCan: TSpeedButton;
    QrParamsEmpDirNFSeNFSCan: TWideStringField;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrParamsEmpAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrParamsEmpBeforeOpen(DataSet: TDataSet);
    procedure SbNovoClick(Sender: TObject);
    procedure BtFilialClick(Sender: TObject);
    procedure Adicionafilial1Click(Sender: TObject);
    procedure Alterafilialatual1Click(Sender: TObject);
    procedure BtSerieNFClick(Sender: TObject);
    procedure Incluinovasriedenotafiscal1Click(Sender: TObject);
    procedure Alterasriedenotafiscalatual1Click(Sender: TObject);
    procedure PMSerieNFPopup(Sender: TObject);
    procedure QrParamsEmpBeforeClose(DataSet: TDataSet);
    procedure QrParamsEmpAfterScroll(DataSet: TDataSet);
    procedure SBModeloNFClick(Sender: TObject);
    procedure EdAssocModNFChange(Sender: TObject);
    procedure EdAssociadaChange(Sender: TObject);
    procedure EdAssociadaEnter(Sender: TObject);
    procedure EdAssociadaExit(Sender: TObject);
    procedure EdAssocModNFEnter(Sender: TObject);
    procedure CBAssocModNFEnter(Sender: TObject);
    procedure SbProdVenClick(Sender: TObject);
    procedure SBDirDenClick(Sender: TObject);
    procedure SBDirNfeAssClick(Sender: TObject);
    procedure SBDirNFeGerClick(Sender: TObject);
    procedure SBDirEnvLotClick(Sender: TObject);
    procedure SBDirRecClick(Sender: TObject);
    procedure SBDirProRecClick(Sender: TObject);
    procedure SBDirPedCanClick(Sender: TObject);
    procedure SBDirCanClick(Sender: TObject);
    procedure SBDirPedInuClick(Sender: TObject);
    procedure SBDirInuClick(Sender: TObject);
    procedure SBDirPedSitClick(Sender: TObject);
    procedure SBDirSitClick(Sender: TObject);
    procedure SBDirPedStaClick(Sender: TObject);
    procedure SBDirStaClick(Sender: TObject);
    procedure SBDirPedRecClick(Sender: TObject);
    procedure SpeedButton11Click(Sender: TObject);
    procedure SBMoedaClick(Sender: TObject);
    procedure SpeedButton5Click(Sender: TObject);
    procedure BtNFeDefaultDirsClick(Sender: TObject);
    procedure SpeedButton9Click(Sender: TObject);
    procedure SbNFeSerNumClick(Sender: TObject);
    procedure EdUF_ServicoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure QrParamsEmpCalcFields(DataSet: TDataSet);
    procedure SpeedButton13Click(Sender: TObject);
    procedure SpeedButton14Click(Sender: TObject);
    procedure SpeedButton15Click(Sender: TObject);
    procedure PMFilialPopup(Sender: TObject);
    procedure EdCSOSNChange(Sender: TObject);
    procedure EdCSOSNKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure RGCRTClick(Sender: TObject);
    procedure SpeedButton16Click(Sender: TObject);
    procedure EdSPED_EFD_IND_PERFILChange(Sender: TObject);
    procedure EdSPED_EFD_IND_PERFILKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdSPED_EFD_IND_ATIVChange(Sender: TObject);
    procedure EdSPED_EFD_IND_ATIVKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SpeedButton17Click(Sender: TObject);
    procedure SpeedButton18Click(Sender: TObject);
    procedure SbDirEveEnvLotClick(Sender: TObject);
    procedure SbDirEveRetLotClick(Sender: TObject);
    procedure SbDirEvePedCCeClick(Sender: TObject);
    procedure SbDirEvePedCanClick(Sender: TObject);
    procedure SbDirEveRetCanClick(Sender: TObject);
    procedure SbDirEveCCeClick(Sender: TObject);
    procedure SpeedButton19Click(Sender: TObject);
    procedure SbDirProcCCeClick(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
    procedure SbEdNFSeTipCtoMailClick(Sender: TObject);
    procedure SbDirNFSeDPSGerClick(Sender: TObject);
    procedure SbDirNFSeNFSAutClick(Sender: TObject);
    procedure BtSugereDirNFSeClick(Sender: TObject);
    procedure SbNFSeCertDigitalClick(Sender: TObject);
    procedure SbDirNFSeRPSEnvLotClick(Sender: TObject);
    procedure SbDirNFSeRPSRecLotClick(Sender: TObject);
    procedure SbDirNFSeLogsClick(Sender: TObject);
    procedure SbDirNFSeSchemaClick(Sender: TObject);
    procedure SbNFSeLogoFiliClick(Sender: TObject);
    procedure SbNFSeLogoPrefClick(Sender: TObject);
    procedure EdNFSeTipoRpsChange(Sender: TObject);
    procedure DBEdit101Change(Sender: TObject);
    procedure SBDirRetNfeDesClick(Sender: TObject);
    procedure SbDirEvePedMDeClick(Sender: TObject);
    procedure SbDirEveRetMDeClick(Sender: TObject);
    procedure SbDirDowNFeDesClick(Sender: TObject);
    procedure SbDirDowNFeNFeClick(Sender: TObject);
    procedure BtCertDigitalClick(Sender: TObject);
    procedure Incluinovocertificadoaorepositriodosistemaoperacional1Click(
      Sender: TObject);
    procedure Removecertificadodorepositriodosistemaoperacional1Click(
      Sender: TObject);
    procedure BtOpcoesClick(Sender: TObject);
    procedure SpeedButton20Click(Sender: TObject);
    procedure SBTZD_UTCClick(Sender: TObject);
    procedure SbDirDistDFeIntClick(Sender: TObject);
    procedure SbDirRetDistDFeIntClick(Sender: TObject);
    procedure SbDirDowNFeCnfClick(Sender: TObject);
    procedure Label277Click(Sender: TObject);
    procedure EdVersaoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdNFeVerStaSerKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdNFeVerEnvLotKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdNFeVerConLotKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdNFeVerCanNFeKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdNFeVerInuNumKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdNFeVerConNFeKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdNFeVerLotEveKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdNFeVerConsCadKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdNFeVerDistDFeIntKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdNFeVerDowNFeKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure CkTZD_UTC_AutoClick(Sender: TObject);
    procedure SBCartEmisHonFunClick(Sender: TObject);
    procedure SBCtaProdComClick(Sender: TObject);
    procedure SBCtaServicoPgClick(Sender: TObject);
    procedure SpeedButton27Click(Sender: TObject);
    procedure SpeedButton21Click(Sender: TObject);
    procedure SpeedButton24Click(Sender: TObject);
    procedure SpeedButton52Click(Sender: TObject);
    procedure SpeedButton53Click(Sender: TObject);
    procedure SpeedButton54Click(Sender: TObject);
    procedure BtCTeDefaultDirsClick(Sender: TObject);
    procedure BtSerieCTeClick(Sender: TObject);
    procedure AlterasriedeCTeatual1Click(Sender: TObject);
    procedure IncluinovasriedeCTe1Click(Sender: TObject);
    procedure SpeedButton56Click(Sender: TObject);
    procedure EdCTeversaoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SpeedButton55Click(Sender: TObject);
    procedure SpeedButton67Click(Sender: TObject);
    procedure SpeedButton66Click(Sender: TObject);
    procedure SpeedButton68Click(Sender: TObject);
    procedure SpeedButton69Click(Sender: TObject);
    procedure SpeedButton57Click(Sender: TObject);
    procedure EdMDFeUF_ServicoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdCTeUF_ServicoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SpeedButton33Click(Sender: TObject);
    procedure SpeedButton29Click(Sender: TObject);
    procedure SpeedButton30Click(Sender: TObject);
    procedure BtMDFeDefaultDirsClick(Sender: TObject);
    procedure BtSerieMDFeClick(Sender: TObject);
    procedure IncluinovasriedeMDFe1Click(Sender: TObject);
    procedure AlterasriedeMDFeatual1Click(Sender: TObject);
    procedure SpeedButton28Click(Sender: TObject);
    procedure Alteraentidadedafilialatual1Click(Sender: TObject);
    procedure EdCTeUF_EPECKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdNFeUF_EPECKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SpeedButton50Click(Sender: TObject);
    procedure SpeedButton51Click(Sender: TObject);
    procedure CkinfRespTec_UsaClick(Sender: TObject);
    procedure BtSerieNFCClick(Sender: TObject);
    procedure Incluinovasriedecupomfiscal1Click(Sender: TObject);
    procedure Alterasriedecupomfiscalatual1Click(Sender: TObject);
    procedure PMSerieNFCPopup(Sender: TObject);
    procedure SbCertDigPfxCamClick(Sender: TObject);
    procedure sbtnNumSerieClick(Sender: TObject);
    procedure sbtnGetCertClick(Sender: TObject);
    procedure btnDataValidadeClick(Sender: TObject);
    procedure btnNumSerieClick(Sender: TObject);
    procedure btnIssuerNameClick(Sender: TObject);
    procedure btnSubNameClick(Sender: TObject);
    procedure btnCNPJClick(Sender: TObject);
    procedure btVersaoClick(Sender: TObject);
    procedure btnSha256Click(Sender: TObject);
    procedure btnHTTPSClick(Sender: TObject);
    procedure btnStatusServClick(Sender: TObject);
    procedure SbCtbCadGruGerClick(Sender: TObject);
    procedure EdUC_IPI_CSTChange(Sender: TObject);
    procedure EdUC_PIS_CSTChange(Sender: TObject);
    procedure EdUC_COFINS_CSTChange(Sender: TObject);
    procedure EdICMS_CSTChange(Sender: TObject);
    procedure EdUC_COFINS_CSTKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdUC_PIS_CSTKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdUC_IPI_CSTKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdUC_ICMS_CST_BKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdCLAS_ESTAB_INDChange(Sender: TObject);
    procedure EdCLAS_ESTAB_INDKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdSPED_EFD_IND_NAT_PJChange(Sender: TObject);
    procedure EdSPED_EFD_IND_NAT_PJKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdSPED_COD_INC_TRIBChange(Sender: TObject);
    procedure EdSPED_COD_INC_TRIBKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdSPED_IND_APRO_CREDChange(Sender: TObject);
    procedure EdSPED_IND_APRO_CREDKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdSPED_IND_REG_CUMChange(Sender: TObject);
    procedure EdSPED_IND_REG_CUMKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdSPED_COD_TIPO_CONTChange(Sender: TObject);
    procedure EdSPED_COD_TIPO_CONTKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure RGRegimPisCofinsClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure EdCodDctfPisRedefinido(Sender: TObject);
    procedure EdCodDctfCofinsRedefinido(Sender: TObject);
    procedure EdUCFisRegMesmaUFKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdUCFisRegMesmaUFChange(Sender: TObject);
    procedure EdUCFisRegOutraUFKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdUCFisRegOutraUFChange(Sender: TObject);
    procedure SbDirEvePedEPECClick(Sender: TObject);
    procedure SbDirEveRetEPECClick(Sender: TObject);
    procedure EdUCCartPagChange(Sender: TObject);
    procedure EdUCCartPagKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SbDirNFSeIniFilesClick(Sender: TObject);
    procedure SbDirNFSeDPSAssClick(Sender: TObject);
    procedure SbDirNFSeNFSCanClick(Sender: TObject);
  private
    FCriado: Boolean;
    FMostraTermo: Boolean;
    FSiglas_WS: MyArrayLista;
    FAssociada, FTZD_UTC_Auto_UsuarioAceite, FTZD_UTC_Auto_TermoAceite: Integer;
    FTZD_UTC_Auto_DataHoraAceite: TDateTime;
    F_CLAS_ESTAB_IND, F_IND_NAT_PJ,
    F_COD_INC_TRIB, F_IND_APRO_CRED, F_COD_TIPO_CONT, F_IND_REG_CUM: MyArrayLista;
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
   ////Procedures do form
    procedure MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo: Integer);
    procedure DefParams;
    procedure LocCod(Atual, Codigo: Integer);
    procedure Va(Para: TVaiPara);
    procedure ReopenImprime();
    function  DefinirCaminhoSeVazio_CNPJ(dmkEdit: TdmkEdit;
              PastaRaiz, PastaFinal: String): Boolean;
    procedure MostraFmContas(EdConta: TdmkEditCB; CBConta:
              TdmkDBLookupComboBox; Query: TmySQLQuery);
    procedure MostraFmEntiTipCto(EdEntTipCto: TdmkEditCB; CBEntTipCto:
              TdmkDBLookupComboBox; Query: TmySQLQuery);
    procedure GerenciaCertificadoDigital(Acao: TSQLType);
    {$IfNDef SemNFe_0000}
    procedure ObtemVersaoServico(Edit: TdmkEdit; Servico: String; MaxVersao: Double = 0);
    {$EndIf}
    procedure ConfiguraCamposTZ(Auto: Boolean);
    procedure PreencheCamposAlteraOpcoesMis();
    procedure PreencheCamposAlteraOpcoesNFe();
  public
    { Public declarations }
    ACBrNFe0: TACBrNFe;
    ACBrIntegrador1: TACBrIntegrador;
    //
    procedure ReopenParamsNFs(Controle: Integer);
    procedure ReopenParamsNFCs(Controle: Integer);
    procedure ReopenParamsCTe(Controle: Integer);
    procedure ReopenParamsMDFe(Controle: Integer);
  end;

var
  FmParamsEmp: TFmParamsEmp;
const
  FFormatFloat = '00000';

implementation

uses
{$IfNDef SemNFe_0000} ModuleNFe_0000, ParamsNFs, NfeInfCpl, NFe_PF, NFE_dll_TLB, {$EndIf}
{$IfNDef semNFCe_0000}
 ParamsNFCs,
{$EndIf}
{$IfNDef sNFSe}
  CapicomListas,
{$Else}
  {$IfNDef SemNFe_0000}
    CapicomListas,
  {$EndIf}
{$EndIf}
{$IfDef ComCTe_0000} ParamsCTe, {$EndIf}
{$IfDef ComMDFe_0000} ParamsMDFe, {$EndIf}
{$IfDef FmImprime} Imprime, {$EndIf}
{$IfNDef SemCotacoes} CambioMda, {$EndIf}
{$IfNDef NO_FINANCEIRO} UnFinanceiro,
{$IFDEF DEFINE_VARLCT}
{$IfNDef NO_FINANCEIRO} UnFinanceiroJan, {$EndIf}
{$EndIf}
{$IfDef sSPED} UnEfdIcmsIpi_Jan, {$EndIf}
Contas, {$EndIf}
  UnMyObjects, Module, ModuleGeral, MyListas, GetValor,
  {$IfNDef NoEntiAux}
  EntiTipCto,
  {$EndIf}
{$ifNDef sACBr} UnDmkACBr_ParamsEmp, (*LoadXML, 2023-12-21*) {$Endif}
  DmkDAC_PF, UnWAceites, UnDmkWeb(*, CapicomListas*);

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmParamsEmp.Label277Click(Sender: TObject);
begin
  DmkWeb.MostraWebBrowser('http://pcdsh01.on.br/verao1.html', True, False, 0, 0);
end;

procedure TFmParamsEmp.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmParamsEmp.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrParamsEmpCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmParamsEmp.DefParams;
begin
  VAR_GOTOTABELA := 'ParamsEmp';
  VAR_GOTOMYSQLTABLE := QrParamsEmp;
  VAR_GOTONEG := gotoAll;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

(*
  VAR_SQLx.Add('SELECT ');
{$IFDEF FmImprime}
  VAR_SQLx.Add('imp.Nome NO_AssocModNF,');
{$Else}
  VAR_SQLx.Add('"" NO_AssocModNF,');
{$EndIf}
{$IfNDef SemCotacoes}
  VAR_SQLx.Add('CONCAT(mda.Sigla," - ",mda.Nome) NOMEMOEDA,');
  VAR_SQLx.Add('mda.CodUsu + 0.000 CODUSU_MDA, ');
{$Else}
  VAR_SQLx.Add('"" NOMEMOEDA,');
  VAR_SQLx.Add('0.000 CODUSU_MDA, ');
{$EndIf}
  VAR_SQLx.Add('ass.Filial FI_ASSOCIADA,');
  VAR_SQLx.Add('IF(ass.Tipo=0,ass.RazaoSocial,ass.Nome) NO_ASSOCIADA,');
  VAR_SQLx.Add('IF(ent.Tipo=0,ent.RazaoSocial,ent.Nome) NOMEFILIAL,');
  VAR_SQLx.Add('IF(ent.Tipo=0,ent.CNPJ,ent.CPF) CNPJCPF_FILIAL,');
  VAR_SQLx.Add('ent.Filial, ent.Codigo Entidade, ');
{$IfNDef NO_FINANCEIRO}
  VAR_SQLx.Add('c01.Nome NO_CtaProdVen, c02.Nome NO_CtaServico, ');
  VAR_SQLx.Add('c03.Nome NO_CtaProdCom, c04.Nome NO_CtaServicoPg, ');
  VAR_SQLx.Add('chf.Nome NO_CartEmisHonFun, ');
{$Else}
  VAR_SQLx.Add('"" NO_CtaProdVen, "" NO_CtaServico, "" NO_CartEmisHonFun, ');
{$EndIf}
{$IfNDef NO_USE_EMAILDMK}
  VAR_SQLx.Add('pe1.Nome NO_PREMAILAUT, ');
  VAR_SQLx.Add('pe2.Nome NO_PREMAILCAN, ');
  VAR_SQLx.Add('pe3.Nome NO_PREMAILEVECCE, ');
  VAR_SQLx.Add('pe4.Nome NO_NFSePreMailAut, ');
  VAR_SQLx.Add('pe5.Nome NO_NFSePreMailCan, ');
{$Else}
  VAR_SQLx.Add('"" NO_PREMAILAUT, ');
  VAR_SQLx.Add('"" NO_PREMAILCAN, ');
  VAR_SQLx.Add('"" NO_PREMAILEVECCE, ');
  VAR_SQLx.Add('"" NO_NFSePreMailAut, ');
  VAR_SQLx.Add('"" NO_NFSePreMailCan, ');
{$EndIf}
  VAR_SQLx.Add('et0.Nome NO_EntiTipCto, et1.Nome NO_EntiTipCt1,');
  VAR_SQLx.Add('IF(ctd.Tipo=0, ctd.RazaoSocial, ctd.Nome) NO_CTD,');
  VAR_SQLx.Add('IF(ctb.Tipo=0, ctb.RazaoSocial, ctb.Nome) NO_CTB,');
  VAR_SQLx.Add('IF(pem.hVeraoAsk <= "1899-12-30", "", DATE_FORMAT(pem.hVeraoAsk, "%d/%m/%Y")) hVeraoAsk_TXT, ');
  VAR_SQLx.Add('IF(pem.hVeraoIni <= "1899-12-30", "", DATE_FORMAT(pem.hVeraoIni, "%d/%m/%Y")) hVeraoIni_TXT, ');
  VAR_SQLx.Add('IF(pem.hVeraoFim <= "1899-12-30", "", DATE_FORMAT(pem.hVeraoFim, "%d/%m/%Y")) hVeraoFim_TXT, ');
{$IfNDef SemNFe_0000}
  VAR_SQLx.Add('cpl.Nome NFeInfCpl_TXT, ');
{$Else}
  VAR_SQLx.Add('"" NFeInfCpl_TXT, ');
{$EndIf}
  VAR_SQLx.Add('et2.Nome NO_NFSeTipCtoMail, pem.*');
  VAR_SQLx.Add('FROM entidades ent');
  VAR_SQLx.Add('LEFT JOIN paramsemp pem ON pem.Codigo=ent.Codigo');
{$IfNDef SemNFe_0000}
  VAR_SQLx.Add('LEFT JOIN nfeinfcpl cpl ON cpl.Codigo=pem.NFeInfCpl');
{$EndIf}
{$IfNDef SemCotacoes}
  VAR_SQLx.Add('LEFT JOIN cambiomda mda ON mda.Codigo=pem.Moeda');
{$EndIf}
  VAR_SQLx.Add('LEFT JOIN entidades ass ON ass.Codigo=pem.Associada');
{$IFDEF FmImprime}
  VAR_SQLx.Add('LEFT JOIN imprime   imp ON imp.Codigo=pem.AssocModNF');
{$EndIf}
{$IfNDef NO_FINANCEIRO}
  VAR_SQLx.Add('LEFT JOIN contas c01 ON c01.Codigo=pem.CtaProdVen');
  VAR_SQLx.Add('LEFT JOIN contas c02 ON c02.Codigo=pem.CtaServico');
  VAR_SQLx.Add('LEFT JOIN contas c03 ON c03.Codigo=pem.CtaProdCom');
  VAR_SQLx.Add('LEFT JOIN contas c04 ON c04.Codigo=pem.CtaServicoPg');
  VAR_SQLx.Add('LEFT JOIN carteiras chf ON chf.Codigo=pem.CartEmisHonFun');
{$EndIf}
{$IfNDef NO_USE_EMAILDMK}
  VAR_SQLx.Add('LEFT JOIN preemail pe4 ON pe4.Codigo=pem.NFSePreMailAut');
  VAR_SQLx.Add('LEFT JOIN preemail pe5 ON pe5.Codigo=pem.NFSePreMailCan');
{$EndIf}
  VAR_SQLx.Add('LEFT JOIN entitipcto et0 ON et0.Codigo=pem.EntiTipCto');
  VAR_SQLx.Add('LEFT JOIN entitipcto et1 ON et1.Codigo=pem.EntiTipCt1');
  VAR_SQLx.Add('LEFT JOIN entitipcto et2 ON et2.Codigo=pem.NFSeTipCtoMail');
  VAR_SQLx.Add('LEFT JOIN entidades ctd ON ctd.Codigo=pem.SPED_EFD_CadContador');
  VAR_SQLx.Add('LEFT JOIN entidades ctb ON ctb.Codigo=pem.SPED_EFD_EscriContab');
  if CO_DMKID_APP IN [4, 5] then
    VAR_SQLx.Add('WHERE ent.CliInt <> 0')
  else
    VAR_SQLx.Add('WHERE ent.Codigo < -10');
  //
  VAR_SQL1.Add('AND pem.Codigo=:P0');
  //
  //VAR_SQL2.Add('AND CodUsu=:P0');
  //
  VAR_SQLa.Add('AND IF(ent.Tipo=0,ent.RazaoSocial,Nome) Like :P0');
  //
*)

  VAR_SQLx.Add('SELECT ');
{$IFDEF FmImprime}
  VAR_SQLx.Add('imp.Nome NO_AssocModNF,');
{$Else}
  VAR_SQLx.Add('"" NO_AssocModNF,');
{$EndIf}
{$IfNDef SemCotacoes}
  VAR_SQLx.Add('CONCAT(mda.Sigla," - ",mda.Nome) NOMEMOEDA,');
  VAR_SQLx.Add('mda.CodUsu + 0.000 CODUSU_MDA, ');
{$Else}
  VAR_SQLx.Add('"" NOMEMOEDA,');
  VAR_SQLx.Add('0.000 CODUSU_MDA, ');
{$EndIf}
  VAR_SQLx.Add('ass.Filial FI_ASSOCIADA,');
  VAR_SQLx.Add('IF(ass.Tipo=0,ass.RazaoSocial,ass.Nome) NO_ASSOCIADA,');
  VAR_SQLx.Add('IF(ent.Tipo=0,ent.RazaoSocial,ent.Nome) NOMEFILIAL,');
  VAR_SQLx.Add('IF(ent.Tipo=0,ent.CNPJ,ent.CPF) CNPJCPF_FILIAL,');
  VAR_SQLx.Add('ent.Filial, ent.Codigo Entidade, ');
{$IfNDef NO_FINANCEIRO}
  VAR_SQLx.Add('c01.Nome NO_CtaProdVen, c02.Nome NO_CtaServico, ');
  VAR_SQLx.Add('c03.Nome NO_CtaProdCom, c04.Nome NO_CtaServicoPg, ');
  VAR_SQLx.Add('c05.Nome NO_CtaFretPrest, c06.Nome NO_CtaMultiSV, ');
  VAR_SQLx.Add('chf.Nome NO_CartEmisHonFun, ccg.Nome NO_CtbCadGruGer, ');
  VAR_SQLx.Add('pem.CtbCadGruGer + 0.000 CtbCadGruGer, ');
{$Else}
  VAR_SQLx.Add('"" NO_CtaProdVen, "" NO_CtaServico, "" NO_CartEmisHonFun, ');
  VAR_SQLx.Add('"" NO_CtaProdCom, "" NO_CtaServicoPg, ');
  VAR_SQLx.Add('"" NO_CtaFretPrest, "" NO_CtaMultiSV, ');
  VAR_SQLx.Add('"" NO_CartEmisHonFun, "" NO_CtbCadGruGer, ');
  VAR_SQLx.Add('0.000 CtbCadGruGer, ');
{$EndIf}
{$IfNDef NO_USE_EMAILDMK}
  VAR_SQLx.Add('pe4.Nome NO_NFSePreMailAut, ');
  VAR_SQLx.Add('pe5.Nome NO_NFSePreMailCan, ');
{$Else}
  VAR_SQLx.Add('"" NO_NFSePreMailAut, ');
  VAR_SQLx.Add('"" NO_NFSePreMailCan, ');
{$EndIf}
  VAR_SQLx.Add('et0.Nome NO_EntiTipCto, et1.Nome NO_EntiTipCt1,');
  VAR_SQLx.Add('IF(ctd.Tipo=0, ctd.RazaoSocial, ctd.Nome) NO_CTD,');
  VAR_SQLx.Add('IF(ctb.Tipo=0, ctb.RazaoSocial, ctb.Nome) NO_CTB,');
  VAR_SQLx.Add('IF(pem.hVeraoAsk <= "1899-12-30", "", DATE_FORMAT(pem.hVeraoAsk, "%d/%m/%Y")) hVeraoAsk_TXT, ');
  VAR_SQLx.Add('IF(pem.hVeraoIni <= "1899-12-30", "", DATE_FORMAT(pem.hVeraoIni, "%d/%m/%Y")) hVeraoIni_TXT, ');
  VAR_SQLx.Add('IF(pem.hVeraoFim <= "1899-12-30", "", DATE_FORMAT(pem.hVeraoFim, "%d/%m/%Y")) hVeraoFim_TXT, ');
{$IfNDef SemNFe_0000}
  VAR_SQLx.Add('cpl.Nome NFeInfCpl_TXT, ');
{$Else}
  VAR_SQLx.Add('"" NFeInfCpl_TXT, ');
{$EndIf}
  VAR_SQLx.Add('et2.Nome NO_NFSeTipCtoMail, ');
  VAR_SQLx.Add('    pem.Codigo,');
  VAR_SQLx.Add('    pem.Logo3x1,');
  VAR_SQLx.Add('    pem.TZD_UTC,');
  VAR_SQLx.Add('    pem.hVeraoAsk,');
  VAR_SQLx.Add('    pem.hVeraoIni,');
  VAR_SQLx.Add('    pem.hVeraoFim,');
  VAR_SQLx.Add('    pem.TZD_UTC_Auto,');
  VAR_SQLx.Add('    pem.NFeNT2013_003LTT,');
  VAR_SQLx.Add('    pem.CartEmisHonFun,');
  VAR_SQLx.Add('    pem.Situacao,');
  VAR_SQLx.Add('    pem.FatSemEstq,');
  VAR_SQLx.Add('    pem.TipMediaDD,');
  VAR_SQLx.Add('    pem.FatSemPrcL,');
  VAR_SQLx.Add('    pem.TipCalcJuro,');
  VAR_SQLx.Add('    pem.PedVdaMudLista,');
  VAR_SQLx.Add('    pem.PedVdaMudPrazo,');
  VAR_SQLx.Add('    pem.PediVdaNElertas,');
  VAR_SQLx.Add('    pem.AssocModNF,');
  VAR_SQLx.Add('    pem.FaturaSeq,');
  VAR_SQLx.Add('    pem.FaturaSep,');
  VAR_SQLx.Add('    pem.FaturaDta,');
  VAR_SQLx.Add('    pem.CtaProdVen,');
  VAR_SQLx.Add('    pem.TxtProdVen,');
  VAR_SQLx.Add('    pem.CtaServico,');
  VAR_SQLx.Add('    pem.TxtServico,');
  VAR_SQLx.Add('    pem.DupProdVen,');
  VAR_SQLx.Add('    pem.DupServico,');
  VAR_SQLx.Add('    pem.FaturaNum,');
  VAR_SQLx.Add('    pem.UsaReferen,');
  VAR_SQLx.Add('    pem.CtaProdCom,');
  VAR_SQLx.Add('    pem.TxtProdCom,');
  VAR_SQLx.Add('    pem.DupProdCom,');
  VAR_SQLx.Add('    pem.CtaServicoPg,');
  VAR_SQLx.Add('    pem.TxtServicoPg,');
  VAR_SQLx.Add('    pem.DupServicoPg,');
  VAR_SQLx.Add('    pem.CtaFretPrest,');
  VAR_SQLx.Add('    pem.TxtFretPrest,');
  VAR_SQLx.Add('    pem.DupFretPrest,');
  VAR_SQLx.Add('    pem.FreteRpICMS,');
  VAR_SQLx.Add('    pem.FreteRpPIS,');
  VAR_SQLx.Add('    pem.FreteRpCOFINS,');
  VAR_SQLx.Add('    pem.RetImpost,');
  VAR_SQLx.Add('    pem.BalQtdItem,');
  VAR_SQLx.Add('    pem.Estq0UsoCons,');
  VAR_SQLx.Add('    pem.PreMailAut,');
  VAR_SQLx.Add('    pem.PreMailCan,');
  VAR_SQLx.Add('    pem.PreMailEveCCe,');
  VAR_SQLx.Add('    pem.DirNFeProt,');
  VAR_SQLx.Add('    pem.DirNFeRWeb,');
  VAR_SQLx.Add('    pem.SCAN_Ser,');
  VAR_SQLx.Add('    pem.SCAN_nNF,');
  VAR_SQLx.Add('    pem.infRespTec_xContato,');
  VAR_SQLx.Add('    pem.infRespTec_CNPJ,');
  VAR_SQLx.Add('    pem.infRespTec_fone,');
  VAR_SQLx.Add('    pem.infRespTec_email,');
  VAR_SQLx.Add('    pem.infRespTec_idCSRT,');
  VAR_SQLx.Add('    pem.infRespTec_CSRT,');
  VAR_SQLx.Add('    pem.infRespTec_Usa,');
  VAR_SQLx.Add('    pem.UF_WebServ,');
  VAR_SQLx.Add('    pem.SiglaCustm,');
  VAR_SQLx.Add('    pem.UF_Servico,');
  VAR_SQLx.Add('    pem.SimplesFed,');
  VAR_SQLx.Add('    pem.InfoPerCuz,');
  VAR_SQLx.Add('    pem.NFeSerNum,');
  VAR_SQLx.Add('    pem.NFeSerVal,');
  VAR_SQLx.Add('    pem.NFeSerAvi,');
  VAR_SQLx.Add('    pem.UF_MDeMDe,');
  VAR_SQLx.Add('    pem.UF_MDeDes,');
  VAR_SQLx.Add('    pem.UF_MDeNFe,');
  VAR_SQLx.Add('    pem.NFeUF_EPEC,');
  VAR_SQLx.Add('    pem.DirEnvLot,');
  VAR_SQLx.Add('    pem.DirRec,');
  VAR_SQLx.Add('    pem.DirProRec,');
  VAR_SQLx.Add('    pem.DirNFeAss,');
  VAR_SQLx.Add('    pem.DirNFeGer,');
  VAR_SQLx.Add('    pem.DirDen,');
  VAR_SQLx.Add('    pem.DirPedRec,');
  VAR_SQLx.Add('    pem.DirPedSta,');
  VAR_SQLx.Add('    pem.DirSta,');
  VAR_SQLx.Add('    pem.DirPedCan,');
  VAR_SQLx.Add('    pem.DirCan,');
  VAR_SQLx.Add('    pem.DirPedInu,');
  VAR_SQLx.Add('    pem.DirInu,');
  VAR_SQLx.Add('    pem.DirPedSit,');
  VAR_SQLx.Add('    pem.DirSit,');
  VAR_SQLx.Add('    pem.DirEveEnvLot,');
  VAR_SQLx.Add('    pem.DirEveRetLot,');
  VAR_SQLx.Add('    pem.DirEvePedCCe,');
  VAR_SQLx.Add('    pem.DirEveRetCCe,');
  VAR_SQLx.Add('    pem.DirEvePedCan,');
  VAR_SQLx.Add('    pem.DirEveRetCan,');
  VAR_SQLx.Add('    pem.DirEveProcCCe,');
  VAR_SQLx.Add('    pem.DirRetNfeDes,');
  VAR_SQLx.Add('    pem.DirEvePedMDe,');
  VAR_SQLx.Add('    pem.DirEveRetMDe,');
  VAR_SQLx.Add('    pem.DirDowNFeDes,');
  VAR_SQLx.Add('    pem.DirDowNFeNFe,');
  VAR_SQLx.Add('    pem.DirDistDFeInt,');
  VAR_SQLx.Add('    pem.DirRetDistDFeInt,');
  VAR_SQLx.Add('    pem.DirDowNFeCnf,');
  VAR_SQLx.Add('    pem.versao,');
  VAR_SQLx.Add('    pem.ide_mod,');
  VAR_SQLx.Add('    pem.ide_tpAmb,');
  VAR_SQLx.Add('    pem.DirSchema,');
  VAR_SQLx.Add('    pem.NT2018_05v120,');
  VAR_SQLx.Add('    pem.AppCode,');
  VAR_SQLx.Add('    pem.AssDigMode,');
  VAR_SQLx.Add('    pem.MyEmailNFe,');
  VAR_SQLx.Add('    pem.NFeInfCpl,');
  VAR_SQLx.Add('    pem.CRT,');
  VAR_SQLx.Add('    pem.CSOSN,');
  VAR_SQLx.Add('    pem.pCredSNAlq,');
  VAR_SQLx.Add('    pem.pCredSNMez,');
  VAR_SQLx.Add('    pem.NFeVerStaSer,');
  VAR_SQLx.Add('    pem.NFeVerEnvLot,');
  VAR_SQLx.Add('    pem.NFeVerConLot,');
  VAR_SQLx.Add('    pem.NFeVerCanNFe,');
  VAR_SQLx.Add('    pem.NFeVerInuNum,');
  VAR_SQLx.Add('    pem.NFeVerConNFe,');
  VAR_SQLx.Add('    pem.NFeVerLotEve,');
  VAR_SQLx.Add('    pem.NFeVerConsCad,');
  VAR_SQLx.Add('    pem.NFeVerDistDFeInt,');
  VAR_SQLx.Add('    pem.NFeVerDowNFe,');
  VAR_SQLx.Add('    pem.ide_tpImp,');
  VAR_SQLx.Add('    pem.NFeItsLin,');
  VAR_SQLx.Add('    pem.NFeFTRazao,');
  VAR_SQLx.Add('    pem.NFeFTEnder,');
  VAR_SQLx.Add('    pem.NFeFTFones,');
  VAR_SQLx.Add('    pem.NFeMaiusc,');
  VAR_SQLx.Add('    pem.NFeShowURL,');
  VAR_SQLx.Add('    pem.PathLogoNF,');
  VAR_SQLx.Add('    pem.DirDANFEs,');
  VAR_SQLx.Add('    pem.NFe_indFinalCpl,');
  VAR_SQLx.Add('    pem.NoDANFEMail,');
  VAR_SQLx.Add('    pem.SPED_EFD_IND_PERFIL,');
  VAR_SQLx.Add('    pem.SPED_EFD_IND_ATIV,');
  VAR_SQLx.Add('    pem.SPED_EFD_CadContador,');
  VAR_SQLx.Add('    pem.SPED_EFD_CRCContador,');
  VAR_SQLx.Add('    pem.SPED_EFD_EscriContab,');
  VAR_SQLx.Add('    pem.SPED_EFD_EnderContab,');
  VAR_SQLx.Add('    pem.SPED_EFD_DtaFiscalSaida,');
  VAR_SQLx.Add('    pem.SPED_EFD_DtaFiscalEntrada,');
  VAR_SQLx.Add('    pem.SPED_EFD_ID_0150,');
  VAR_SQLx.Add('    pem.SPED_EFD_ID_0200,');
  VAR_SQLx.Add('    pem.SPED_EFD_Peri_K100,');
  VAR_SQLx.Add('    pem.SPED_EFD_Peri_E500,');
  VAR_SQLx.Add('    pem.SPED_EFD_Peri_E100,');
  VAR_SQLx.Add('    pem.SPED_EFD_Producao,');
  VAR_SQLx.Add('    pem.SPED_EFD_MovSubPrd,');
  VAR_SQLx.Add('    pem.SPED_EFD_Path,');
  VAR_SQLx.Add('    pem.SPED_EFD_ICMS_IPI_VersaoGuia,');
  VAR_SQLx.Add('    pem.NFSeMetodo,');
  VAR_SQLx.Add('    pem.NFSeVersao,');
  VAR_SQLx.Add('    pem.NFSeSerieRps,');
  VAR_SQLx.Add('    pem.DpsNumero,');
  VAR_SQLx.Add('    pem.DpsNumHom,');
  VAR_SQLx.Add('    pem.NFSeWSProducao,');
  VAR_SQLx.Add('    pem.NFSeAmbiente,');
  VAR_SQLx.Add('    pem.NFSeWSHomologa,');
  VAR_SQLx.Add('    pem.NFSeCertDigital,');
  VAR_SQLx.Add('    pem.NFSeCertValidad,');
  VAR_SQLx.Add('    pem.NFSeCertAviExpi,');
  VAR_SQLx.Add('    pem.RPSNumLote,');
  VAR_SQLx.Add('    pem.HomNumLote,');
  VAR_SQLx.Add('    pem.NFSeUserWeb,');
  VAR_SQLx.Add('    pem.NFSeSenhaWeb,');
  VAR_SQLx.Add('    pem.NFSeWebFraseSecr,');
  VAR_SQLx.Add('    pem.NFSeCodMunici,');
  VAR_SQLx.Add('    pem.NFSePrefeitura1,');
  VAR_SQLx.Add('    pem.NFSeMsgVisu,');
  VAR_SQLx.Add('    pem.NFSeLogoFili,');
  VAR_SQLx.Add('    pem.NFSeLogoPref,');
  VAR_SQLx.Add('    pem.NFSePrefeitura2,');
  VAR_SQLx.Add('    pem.NFSePrefeitura3,');
  VAR_SQLx.Add('    pem.NFSeMetodEnvRPS,');
  VAR_SQLx.Add('    pem.NFSeTipoRps,');
  VAR_SQLx.Add('    pem.NFSe_indFinalCpl,');
  VAR_SQLx.Add('    pem.DirNFSeRPSEnvLot,');
  VAR_SQLx.Add('    pem.DirNFSeRPSRecLot,');
  VAR_SQLx.Add('    pem.DirNFSeIniFiles,');
  VAR_SQLx.Add('    pem.DirNFSeDPSGer,');
  VAR_SQLx.Add('    pem.DirNFSeDPSAss,');
  VAR_SQLx.Add('    pem.DirNFSeNFSAut,');
  VAR_SQLx.Add('    pem.DirNFSeNFSCan,');
  VAR_SQLx.Add('    pem.DirNFSeLogs,');
  VAR_SQLx.Add('    pem.DirNFSeSchema,');
  VAR_SQLx.Add('    pem.NFSePreMailAut,');
  VAR_SQLx.Add('    pem.NFSePreMailCan,');
  VAR_SQLx.Add('    pem.MyEmailNFSe,');
  VAR_SQLx.Add('    pem.CTetpEmis,');
  VAR_SQLx.Add('    pem.CTePreMailAut,');
  VAR_SQLx.Add('    pem.CTePreMailCan,');
  VAR_SQLx.Add('    pem.CTePreMailEveCCe,');
  VAR_SQLx.Add('    pem.DirCTeProt,');
  VAR_SQLx.Add('    pem.DirCTeRWeb,');
  VAR_SQLx.Add('    pem.CTeSCAN_Ser,');
  VAR_SQLx.Add('    pem.CTeSCAN_nCT,');
  VAR_SQLx.Add('    pem.CTeUF_WebServ,');
  VAR_SQLx.Add('    pem.CTeUF_Servico,');
  VAR_SQLx.Add('    pem.CTeSerNum,');
  VAR_SQLx.Add('    pem.CTeSerVal,');
  VAR_SQLx.Add('    pem.CTeSerAvi,');
  VAR_SQLx.Add('    pem.CTeUF_EPEC,');
  VAR_SQLx.Add('    pem.CTeUF_Conting,');
  VAR_SQLx.Add('    pem.DirCTeEnvLot,');
  VAR_SQLx.Add('    pem.DirCTeRec,');
  VAR_SQLx.Add('    pem.DirCTeProRec,');
  VAR_SQLx.Add('    pem.DirCTeAss,');
  VAR_SQLx.Add('    pem.DirCTeGer,');
  VAR_SQLx.Add('    pem.DirCTeDen,');
  VAR_SQLx.Add('    pem.DirCTePedRec,');
  VAR_SQLx.Add('    pem.DirCTePedSta,');
  VAR_SQLx.Add('    pem.DirCTeSta,');
  VAR_SQLx.Add('    pem.DirCTePedCan,');
  VAR_SQLx.Add('    pem.DirCTeCan,');
  VAR_SQLx.Add('    pem.DirCTePedInu,');
  VAR_SQLx.Add('    pem.DirCTeInu,');
  VAR_SQLx.Add('    pem.DirCTePedSit,');
  VAR_SQLx.Add('    pem.DirCTeSit,');
  VAR_SQLx.Add('    pem.DirCTeEveEnvLot,');
  VAR_SQLx.Add('    pem.DirCTeEveRetLot,');
  VAR_SQLx.Add('    pem.DirCTeEvePedCCe,');
  VAR_SQLx.Add('    pem.DirCTeEveRetCCe,');
  VAR_SQLx.Add('    pem.DirCTeEvePedCan,');
  VAR_SQLx.Add('    pem.DirCTeEveRetCan,');
  VAR_SQLx.Add('    pem.DirCTEEveProcCCe,');
  VAR_SQLx.Add('    pem.DirCTeEvePedEPEC,');
  VAR_SQLx.Add('    pem.DirCTeEveRetEPEC,');
  VAR_SQLx.Add('    pem.CTeversao,');
  VAR_SQLx.Add('    pem.CTeide_mod,');
  VAR_SQLx.Add('    pem.CTeide_tpAmb,');
  VAR_SQLx.Add('    pem.DirCTeSchema,');
  VAR_SQLx.Add('    pem.CTeAppCode,');
  VAR_SQLx.Add('    pem.CTeAssDigMode,');
  VAR_SQLx.Add('    pem.MyEmailCTe,');
  VAR_SQLx.Add('    pem.CTeInfCpl,');
  VAR_SQLx.Add('    pem.CTeVerStaSer,');
  VAR_SQLx.Add('    pem.CTeVerEnvLot,');
  VAR_SQLx.Add('    pem.CTeVerConLot,');
  VAR_SQLx.Add('    pem.CTeVerInuNum,');
  VAR_SQLx.Add('    pem.CTeVerConCTe,');
  VAR_SQLx.Add('    pem.CTeVerLotEve,');
  VAR_SQLx.Add('    pem.CTeVerEPEC,');
  VAR_SQLx.Add('    pem.CTeide_tpImp,');
  VAR_SQLx.Add('    pem.CTeItsLin,');
  VAR_SQLx.Add('    pem.CTeFTRazao,');
  VAR_SQLx.Add('    pem.CTeFTEnder,');
  VAR_SQLx.Add('    pem.CTeFTFones,');
  VAR_SQLx.Add('    pem.CTeMaiusc,');
  VAR_SQLx.Add('    pem.CTeShowURL,');
  VAR_SQLx.Add('    pem.PathLogoCTe,');
  VAR_SQLx.Add('    pem.DirDACTes,');
  VAR_SQLx.Add('    pem.CTe_indFinalCpl,');
  VAR_SQLx.Add('    pem.MDFePreMailAut,');
  VAR_SQLx.Add('    pem.MDFePreMailCan,');
  VAR_SQLx.Add('    pem.MDFePreMailEveCCe,');
  VAR_SQLx.Add('    pem.DirMDFeProt,');
  VAR_SQLx.Add('    pem.DirMDFeRWeb,');
  VAR_SQLx.Add('    pem.MDFeSCAN_Ser,');
  VAR_SQLx.Add('    pem.MDFeSCAN_nMDF,');
  VAR_SQLx.Add('    pem.MDFeUF_WebServ,');
  VAR_SQLx.Add('    pem.MDFeUF_Servico,');
  VAR_SQLx.Add('    pem.MDFeSerNum,');
  VAR_SQLx.Add('    pem.MDFeSerVal,');
  VAR_SQLx.Add('    pem.MDFeSerAvi,');
  VAR_SQLx.Add('    pem.DirMDFeEnvLot,');
  VAR_SQLx.Add('    pem.DirMDFeRec,');
  VAR_SQLx.Add('    pem.DirMDFeProRec,');
  VAR_SQLx.Add('    pem.DirMDFeAss,');
  VAR_SQLx.Add('    pem.DirMDFeGer,');
  VAR_SQLx.Add('    pem.DirMDFeDen,');
  VAR_SQLx.Add('    pem.DirMDFePedRec,');
  VAR_SQLx.Add('    pem.DirMDFePedSta,');
  VAR_SQLx.Add('    pem.DirMDFeSta,');
  VAR_SQLx.Add('    pem.DirMDFePedCan,');
  VAR_SQLx.Add('    pem.DirMDFeCan,');
  VAR_SQLx.Add('    pem.DirMDFePedInu,');
  VAR_SQLx.Add('    pem.DirMDFeInu,');
  VAR_SQLx.Add('    pem.DirMDFePedSit,');
  VAR_SQLx.Add('    pem.DirMDFeSit,');
  VAR_SQLx.Add('    pem.DirMDFeEveEnvLot,');
  VAR_SQLx.Add('    pem.DirMDFeEveRetLot,');
  VAR_SQLx.Add('    pem.DirMDFeEvePedEnc,');
  VAR_SQLx.Add('    pem.DirMDFeEveRetEnc,');
  VAR_SQLx.Add('    pem.DirMDFeEvePedCan,');
  VAR_SQLx.Add('    pem.DirMDFeEveRetCan,');
  VAR_SQLx.Add('    pem.DirMDFeEvePedIdC,');
  VAR_SQLx.Add('    pem.DirMDFeEveRetIdC,');
  VAR_SQLx.Add('    pem.MDFeversao,');
  VAR_SQLx.Add('    pem.MDFeide_mod,');
  VAR_SQLx.Add('    pem.MDFeide_tpAmb,');
  VAR_SQLx.Add('    pem.DirMDFeSchema,');
  VAR_SQLx.Add('    pem.MDFeAppCode,');
  VAR_SQLx.Add('    pem.MDFeAssDigMode,');
  VAR_SQLx.Add('    pem.MyEmailMDFe,');
  VAR_SQLx.Add('    pem.MDFeInfCpl,');
  VAR_SQLx.Add('    pem.MDFeVerStaSer,');
  VAR_SQLx.Add('    pem.MDFeVerEnvLot,');
  VAR_SQLx.Add('    pem.MDFeVerConLot,');
  VAR_SQLx.Add('    pem.MDFeVerInuNum,');
  VAR_SQLx.Add('    pem.MDFeVerConMDFe,');
  VAR_SQLx.Add('    pem.MDFeVerLotEve,');
  VAR_SQLx.Add('    pem.MDFeide_tpImp,');
  VAR_SQLx.Add('    pem.MDFeItsLin,');
  VAR_SQLx.Add('    pem.MDFeFTRazao,');
  VAR_SQLx.Add('    pem.MDFeFTEnder,');
  VAR_SQLx.Add('    pem.MDFeFTFones,');
  VAR_SQLx.Add('    pem.MDFeMaiusc,');
  VAR_SQLx.Add('    pem.MDFeShowURL,');
  VAR_SQLx.Add('    pem.PathLogoMDFe,');
  VAR_SQLx.Add('    pem.DirDAMDFes,');
  VAR_SQLx.Add('    pem.MDFe_indFinalCpl,');
  VAR_SQLx.Add('    pem.Moeda,');
  VAR_SQLx.Add('    pem.Associada,');
  VAR_SQLx.Add('    pem.EntiTipCt1,');
  VAR_SQLx.Add('    pem.EntiTipCto,');
  VAR_SQLx.Add('    pem.NFSeTipCtoMail,');
  VAR_SQLx.Add('    pem.CTeEntiTipCt1,');
  VAR_SQLx.Add('    pem.CTeEntiTipCto, ');
  VAR_SQLx.Add('    pem.CSC, ');
  VAR_SQLx.Add('    pem.CSCpos, ');
  VAR_SQLx.Add('    pem.DupMultiSV, ');
  VAR_SQLx.Add('    pem.CtaMultiSV, ');
  VAR_SQLx.Add('    pem.TxtMultiSV, ');
  VAR_SQLx.Add('    pem.CLAS_ESTAB_IND, ');
  VAR_SQLx.Add('    pem.SPED_EFD_IND_NAT_PJ, ');
  VAR_SQLx.Add('    pem.SPED_COD_INC_TRIB, ');
  VAR_SQLx.Add('    pem.SPED_IND_APRO_CRED, ');
  VAR_SQLx.Add('    pem.SPED_COD_TIPO_CONT, ');
  VAR_SQLx.Add('    pem.SPED_IND_REG_CUM, ');
  VAR_SQLx.Add('    pem.RegimTributar, ');
  VAR_SQLx.Add('    pem.RegimPisCofins, ');
  VAR_SQLx.Add('    pem.RegimCumulativ, ');
  VAR_SQLx.Add('    pem.CodDctfPis, ');
  VAR_SQLx.Add('    pem.CodDctfCofins ');
  //
  VAR_SQLx.Add('FROM entidades ent');
  VAR_SQLx.Add('LEFT JOIN paramsemp pem ON pem.Codigo=ent.Codigo');
{$IfNDef SemNFe_0000}
  VAR_SQLx.Add('LEFT JOIN nfeinfcpl cpl ON cpl.Codigo=pem.NFeInfCpl');
{$EndIf}
{$IfNDef SemCotacoes}
  VAR_SQLx.Add('LEFT JOIN cambiomda mda ON mda.Codigo=pem.Moeda');
{$EndIf}
  VAR_SQLx.Add('LEFT JOIN entidades ass ON ass.Codigo=pem.Associada');
{$IFDEF FmImprime}
  VAR_SQLx.Add('LEFT JOIN imprime   imp ON imp.Codigo=pem.AssocModNF');
{$EndIf}
{$IfNDef NO_FINANCEIRO}
  VAR_SQLx.Add('LEFT JOIN contas c01 ON c01.Codigo=pem.CtaProdVen');
  VAR_SQLx.Add('LEFT JOIN contas c02 ON c02.Codigo=pem.CtaServico');
  VAR_SQLx.Add('LEFT JOIN contas c03 ON c03.Codigo=pem.CtaProdCom');
  VAR_SQLx.Add('LEFT JOIN contas c04 ON c04.Codigo=pem.CtaServicoPg');
  VAR_SQLx.Add('LEFT JOIN contas c05 ON c05.Codigo=pem.CtaFretPrest');
  VAR_SQLx.Add('LEFT JOIN contas c06 ON c06.Codigo=pem.CtaMultiSV');
  VAR_SQLx.Add('LEFT JOIN carteiras chf ON chf.Codigo=pem.CartEmisHonFun');
  VAR_SQLx.Add('LEFT JOIN ctbcadgru ccg ON ccg.Codigo=pem.CtbCadGruGer');
{$EndIf}
{$IfNDef NO_USE_EMAILDMK}
  VAR_SQLx.Add('LEFT JOIN preemail pe1 ON pe1.Codigo=pem.PreMailAut');
  VAR_SQLx.Add('LEFT JOIN preemail pe2 ON pe2.Codigo=pem.PreMailCan');
  VAR_SQLx.Add('LEFT JOIN preemail pe3 ON pe3.Codigo=pem.PreMailEveCCe');
  VAR_SQLx.Add('LEFT JOIN preemail pe4 ON pe4.Codigo=pem.NFSePreMailAut');
  VAR_SQLx.Add('LEFT JOIN preemail pe5 ON pe5.Codigo=pem.NFSePreMailCan');
{$EndIf}
  VAR_SQLx.Add('LEFT JOIN entitipcto et0 ON et0.Codigo=pem.EntiTipCto');
  VAR_SQLx.Add('LEFT JOIN entitipcto et1 ON et1.Codigo=pem.EntiTipCt1');
  VAR_SQLx.Add('LEFT JOIN entitipcto et2 ON et2.Codigo=pem.NFSeTipCtoMail');
  VAR_SQLx.Add('LEFT JOIN entidades ctd ON ctd.Codigo=pem.SPED_EFD_CadContador');
  VAR_SQLx.Add('LEFT JOIN entidades ctb ON ctb.Codigo=pem.SPED_EFD_EscriContab');
  if CO_DMKID_APP IN [4, 5] then
    VAR_SQLx.Add('WHERE ent.CliInt <> 0')
  else
    VAR_SQLx.Add('WHERE ent.Codigo < -10');
  //
  VAR_SQL1.Add('AND pem.Codigo=:P0');
  //
  //VAR_SQL2.Add('AND CodUsu=:P0');
  //
  VAR_SQLa.Add('AND IF(ent.Tipo=0,ent.RazaoSocial,Nome) Like :P0');
  //

end;

procedure TFmParamsEmp.EdAssociadaChange(Sender: TObject);
begin
  if not EdAssociada.Focused then
    ReopenImprime();
end;

procedure TFmParamsEmp.EdAssociadaEnter(Sender: TObject);
begin
  FAssociada := Geral.IMV(EdAssociada.Text);
end;

procedure TFmParamsEmp.EdAssociadaExit(Sender: TObject);
begin
  if FAssociada <> Geral.IMV(EdAssociada.Text) then
    ReopenImprime();
end;

procedure TFmParamsEmp.EdAssocModNFChange(Sender: TObject);
begin
  ReopenImprime();
end;

procedure TFmParamsEmp.EdAssocModNFEnter(Sender: TObject);
begin
//  ReopenImprime();
end;

procedure TFmParamsEmp.EdUCCartPagChange(Sender: TObject);
var
  Codigo: Integer;
begin
{$IfNDef SemNFe_0000}
  Codigo := EdUCCartPag.ValueVariant;
  EdUCCartPag_TXT.Text := DModG.ObtemDescricao_TabApp(Codigo, 'Carteiras');
{$EndIf}
end;

procedure TFmParamsEmp.EdUCCartPagKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key=VK_F3 then
    CuringaLoc.Pesquisa('Codigo', 'Nome', 'Carteiras', Dmod.MyDB,
    ''(*Extra*), TdmkEdit(Sender), (*CBICMS_CST*)nil, dmktfInteger);
end;

procedure TFmParamsEmp.EdUCFisRegMesmaUFChange(Sender: TObject);
var
  Codigo: Integer;
begin
{$IfNDef SemNFe_0000}
  Codigo := EdUCFisRegMesmaUF.ValueVariant;
  EdUCFisRegMesmaUF_TXT.Text := DModG.ObtemDescricao_TabApp(Codigo, 'FisRegCad');
{$EndIf}
end;

procedure TFmParamsEmp.EdUCFisRegMesmaUFKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key=VK_F3 then
    CuringaLoc.Pesquisa('Codigo', 'Nome', 'FisRegCad', Dmod.MyDB,
    ''(*Extra*), TdmkEdit(Sender), (*CBICMS_CST*)nil, dmktfInteger);
end;

procedure TFmParamsEmp.EdUCFisRegOutraUFChange(Sender: TObject);
var
  Codigo: Integer;
begin
{$IfNDef SemNFe_0000}
  Codigo := EdUCFisRegOutraUF.ValueVariant;
  EdUCFisRegOutraUF_TXT.Text := DModG.ObtemDescricao_TabApp(Codigo, 'FisRegCad');
{$EndIf}

end;

procedure TFmParamsEmp.EdUCFisRegOutraUFKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key=VK_F3 then
    CuringaLoc.Pesquisa('Codigo', 'Nome', 'FisRegCad', Dmod.MyDB,
    ''(*Extra*), TdmkEdit(Sender), (*CBICMS_CST*)nil, dmktfInteger);
end;

procedure TFmParamsEmp.EdUC_COFINS_CSTChange(Sender: TObject);
begin
{$IfNDef NO_FINANCEIRO}
  EdUCTextoCOFINS_CST.Text := UFinanceiro.CST_COFINS_Get(EdUC_COFINS_CST.Text);
{$EndIf}
end;

procedure TFmParamsEmp.EdUC_COFINS_CSTKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F3 then
{$IfNDef NO_FINANCEIRO}
    EdUC_COFINS_CST.Text := UFinanceiro.ListaDeCST_COFINS();
{$EndIf}
end;

procedure TFmParamsEmp.EdCLAS_ESTAB_INDChange(Sender: TObject);
begin
  {$IfNDef NO_FINANCEIRO}
  {$IfNDef SemNFe_0000}
  EdCLAS_ESTAB_IND_TXT.Text := UnNFe_PF.TextoDeCodigoNFe(nfeCLAS_ESTAB_IND,  EdCLAS_ESTAB_IND.ValueVariant);
  {$EndIf}
  {$EndIf}
end;

procedure TFmParamsEmp.EdCLAS_ESTAB_INDKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  TitCols: array[0..1] of String;
begin
  {$IfNDef NO_FINANCEIRO}
  if Key = VK_F4 then
  begin
    TitCols[0] := 'C�d.';
    TitCols[1] := 'Descri��o';
    EdCLAS_ESTAB_IND.Text := Geral.SelecionaItem(F_CLAS_ESTAB_IND, 0,
      'SEL-LISTA-000 :: Classifica��o do estabelecimento industrial ou equiparado a industrial',
      TitCols, Screen.Width)
  end;
{$EndIf}
end;

procedure TFmParamsEmp.EdCodDctfPisRedefinido(Sender: TObject);
begin
  EdCodDctfPis.ValueVariant := Geral.SoNumero_TT(EdCodDctfPis.ValueVariant);
end;

procedure TFmParamsEmp.EdSPED_COD_INC_TRIBChange(Sender: TObject);
begin
{$IfNDef SemNFe_0000}
    EdCOD_INC_TRIB_TXT.Text := UnNFe_PF.TextoDeCodigoSPED_TXT(spedCOD_INC_TRIB,
    EdSPED_COD_INC_TRIB.Text);
{$EndIf}
  end;

procedure TFmParamsEmp.EdSPED_COD_INC_TRIBKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  TitCols: array[0..2] of String;
begin
  if Key = VK_F4 then
  begin
    TitCols[0] := 'UF';
    TitCols[1] := 'Sigla';
    TitCols[2] := 'Descri��o';
    //
    EdCOD_INC_TRIB_TXT.Text := Geral.SelecionaItem(F_COD_INC_TRIB, 1,
      'SEL-LISTA-000 :: C�digo indicador da incid�ncia tribut�ria noper�odo:', TitCols, Screen.Width)
  end;
end;

procedure TFmParamsEmp.EdSPED_COD_TIPO_CONTChange(Sender: TObject);
begin
{$IfNDef SemNFe_0000}
  EdCOD_TIPO_CONT_TXT.Text := UnNFe_PF.TextoDeCodigoSPED_TXT(spedCOD_TIPO_CONT,
    EdSPED_COD_TIPO_CONT.Text);
{$EndIf}
end;

procedure TFmParamsEmp.EdSPED_COD_TIPO_CONTKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  TitCols: array[0..2] of String;
begin
  if Key = VK_F4 then
  begin
    TitCols[0] := 'UF';
    TitCols[1] := 'Sigla';
    TitCols[2] := 'Descri��o';
    //
    EdCOD_TIPO_CONT_TXT.Text := Geral.SelecionaItem(F_COD_TIPO_CONT, 1,
      'SEL-LISTA-000 :: C�digo indicador do Tipo de Contribui��o Apurada no Per�odo', TitCols, Screen.Width)
  end;
end;

procedure TFmParamsEmp.EdCSOSNChange(Sender: TObject);
begin
  {$IfNDef NO_FINANCEIRO}
  MeCSOSN_Edit.Text := UFinanceiro.CSOSN_Get(RGCRT.ItemIndex, EdCSOSN.ValueVariant);
  {$EndIf}
end;

procedure TFmParamsEmp.EdCSOSNKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  {$IfNDef NO_FINANCEIRO}
  if Key = VK_F3 then
    EdCSOSN.Text := UFinanceiro.ListaDeCSOSN();
  {$EndIf}
end;

procedure TFmParamsEmp.EdCTeUF_EPECKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  TitCols: array[0..2] of String;
begin
  if Key = VK_F4 then
  begin
    TitCols[0] := 'UF';
    TitCols[1] := 'Sigla';
    TitCols[2] := 'Descri��o';
    //
    EdCTeUF_EPEC.Text := Geral.SelecionaItem(FSiglas_WS, 1,
      'SEL-LISTA-000 :: Sigla da Web Service', TitCols, Screen.Width)
  end;
end;

procedure TFmParamsEmp.EdCTeUF_ServicoKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  TitCols: array[0..2] of String;
begin
  if Key = VK_F4 then
  begin
    TitCols[0] := 'UF';
    TitCols[1] := 'Sigla';
    TitCols[2] := 'Descri��o';
    //
    EdCTeUF_Servico.Text := Geral.SelecionaItem(FSiglas_WS, 1,
      'SEL-LISTA-000 :: Sigla da Web Service', TitCols, Screen.Width)
  end;
end;

procedure TFmParamsEmp.EdCTeversaoKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
{$IfDef ComCTe_0000}
var
  Versao: Double;
{$EndIf}
begin
  {$IfDef ComCTe_0000}
(*
  if Key = VK_F4 then
  begin
    Versao := EdVersao.ValueVariant;
    //
    if MyObjects.FIC(Versao = 0, EdVersao, 'Informe a vers�o') then Exit;
    //
    if Versao = 2 then
    begin
      ObtemVersaoServico(EdCTeVerStaSer, FmCTeGeraXML_0200.NomeAcao(CTeGeraXML_0200.tcwsStatusServico), Versao);
      ObtemVersaoServico(EdCTeVerEnvLot, FmCTeGeraXML_0200.NomeAcao(CTeGeraXML_0200.tcwsEnviaLoteNF), Versao);
      ObtemVersaoServico(EdCTeVerConLot, FmCTeGeraXML_0200.NomeAcao(CTeGeraXML_0200.tcwsConsultaLote), Versao);
      ObtemVersaoServico(EdCTeVerCanCTe, FmCTeGeraXML_0200.NomeAcao(CTeGeraXML_0200.tcwsPedeCancelamento), Versao);
      ObtemVersaoServico(EdCTeVerInuNum, FmCTeGeraXML_0200.NomeAcao(CTeGeraXML_0200.tcwsPediInutilizacao), Versao);
      ObtemVersaoServico(EdCTeVerConCTe, FmCTeGeraXML_0200.NomeAcao(CTeGeraXML_0200.tcwsConsultaCTe), Versao);
      ObtemVersaoServico(EdCTeVerLotEve, FmCTeGeraXML_0200.NomeAcao(CTeGeraXML_0200.tcwsRecepcaoEvento), Versao);
      ObtemVersaoServico(EdCTeVerConsCad, FmCTeGeraXML_0200.NomeAcao(CTeGeraXML_0200.tcwsConsultaCadastro), Versao);
      ObtemVersaoServico(EdCTeVerDistDFeInt, FmCTeGeraXML_0200.NomeAcao(CTeGeraXML_0200.tcwsConsultaCTeDest), Versao);
      ObtemVersaoServico(EdCTeVerDowCTe, FmCTeGeraXML_0200.NomeAcao(CTeGeraXML_0200.tcwsDownloadCTeDest), Versao);
  end;
*)
  {$EndIf}
end;

procedure TFmParamsEmp.EdICMS_CSTChange(Sender: TObject);
begin
{$IfNDef NO_FINANCEIRO}
  EdUCTextoB.Text := UFinanceiro.CST_B_Get(Geral.IMV(EdUC_ICMS_CST_B.Text));
{$EndIf}
end;

procedure TFmParamsEmp.EdSPED_IND_APRO_CREDChange(Sender: TObject);
begin
{$IfNDef SemNFe_0000}
  EdIND_APRO_CRED_TXT.Text := UnNFe_PF.TextoDeCodigoSPED_TXT(spedIND_APRO_CRED,
    EdSPED_IND_APRO_CRED.Text);
{$EndIf}
end;

procedure TFmParamsEmp.EdSPED_IND_APRO_CREDKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  TitCols: array[0..2] of String;
begin
  if Key = VK_F4 then
  begin
    TitCols[0] := 'UF';
    TitCols[1] := 'Sigla';
    TitCols[2] := 'Descri��o';
    //
    EdIND_APRO_CRED_TXT.Text := Geral.SelecionaItem(F_IND_APRO_CRED, 1,
      'SEL-LISTA-000 :: C�digo indicador de m�todo de apropria��o de cr�ditos comuns, no caso de incid�ncia no regime n�o-cumulativo (COD_INC_TRIB = 1 ou 3):',
      TitCols, Screen.Width)
  end;
end;

procedure TFmParamsEmp.EdSPED_IND_REG_CUMChange(Sender: TObject);
begin
{$IfNDef SemNFe_0000}
  EdIND_REG_CUM_TXT.Text := UnNFe_PF.TextoDeCodigoSPED_TXT(spedIND_REG_CUM,
    EdSPED_IND_REG_CUM.Text);
{$EndIf}
end;

procedure TFmParamsEmp.EdSPED_IND_REG_CUMKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  TitCols: array[0..2] of String;
begin
  if Key = VK_F4 then
  begin
    TitCols[0] := 'UF';
    TitCols[1] := 'Sigla';
    TitCols[2] := 'Descri��o';
    //
    EdIND_REG_CUM_TXT.Text := Geral.SelecionaItem(F_IND_REG_CUM, 1,
      'SEL-LISTA-000 :: C�digo indicador do crit�rio de escritura��o e apura��o adotado, no caso de incid�ncia exclusivamente no regime cumulativo:',
      TitCols, Screen.Width)
  end;
end;

procedure TFmParamsEmp.EdUC_ICMS_CST_BKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F3 then
{$IfNDef NO_FINANCEIRO}
    EdUC_ICMS_CST_B.Text := UFinanceiro.ListaDeTributacaoPeloICMS();
(*if Key = VK_F3 then
    EdCSOSN.Text := UFinanceiro.ListaDeCSOSN();*)
{$EndIf}
end;

procedure TFmParamsEmp.EdUC_IPI_CSTChange(Sender: TObject);
begin
{$IfNDef NO_FINANCEIRO}
  EdUCTextoIPI_CST.Text := UFinanceiro.CST_IPI_Get(EdUC_IPI_CST.Text);
{$EndIf}
end;

procedure TFmParamsEmp.EdUC_IPI_CSTKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F3 then
{$IfNDef NO_FINANCEIRO}
    EdUC_IPI_CST.Text := UFinanceiro.ListaDeCST_IPI();
{$EndIf}
end;

procedure TFmParamsEmp.EdMDFeUF_ServicoKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  TitCols: array[0..2] of String;
begin
  if Key = VK_F4 then
  begin
    TitCols[0] := 'UF';
    TitCols[1] := 'Sigla';
    TitCols[2] := 'Descri��o';
    //
    EdMDFeUF_Servico.Text := Geral.SelecionaItem(FSiglas_WS, 1,
      'SEL-LISTA-000 :: Sigla da Web Service', TitCols, Screen.Width)
  end;
end;

procedure TFmParamsEmp.EdNFeUF_EPECKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  TitCols: array[0..2] of String;
begin
  if Key = VK_F4 then
  begin
    TitCols[0] := 'UF';
    TitCols[1] := 'Sigla';
    TitCols[2] := 'Descri��o';
    //
    EdNFeUF_EPEC.Text := Geral.SelecionaItem(FSiglas_WS, 1,
      'SEL-LISTA-000 :: Sigla da Web Service', TitCols, Screen.Width)
  end;
end;

procedure TFmParamsEmp.EdNFeVerCanNFeKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  {$IfNDef SemNFe_0000}
  if Key = VK_F4 then
  begin
(*
    {$IfNDef semNFe_v0200}
    if EdVersao.ValueVariant = 2 then
      ObtemVersaoServico(EdNFeVerCanNFe, FmNFeGeraXML_0200.NomeAcao(NFeGeraXML_0200.tcwsPedeCancelamento))
    else
    {$EndIf}
    if EdVersao.ValueVariant = 3.1 then
      ObtemVersaoServico(EdNFeVerCanNFe, FmNFeGeraXML_0310.NomeAcao(NFeGeraXML_0310.tcwsPedeCancelamento))
    else
*)
      EdNFeVerCanNFe.ValueVariant := 0; //N�o tem cancelamento na NF-e 4.0
  end;
  {$EndIf}
end;

procedure TFmParamsEmp.EdNFeVerConLotKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  {$IfNDef SemNFe_0000}
  if Key = VK_F4 then
  begin
(*
    {$IfNDef semNFe_v0200}
    if EdVersao.ValueVariant = 2 then
      ObtemVersaoServico(EdNFeVerConLot, FmNFeGeraXML_0200.NomeAcao(NFeGeraXML_0200.tcwsConsultaLote))
    else
    {$EndIf}
    if EdVersao.ValueVariant = 3.1 then
      ObtemVersaoServico(EdNFeVerConLot, FmNFeGeraXML_0310.NomeAcao(NFeGeraXML_0310.tcwsRetAutorizacao))
    else
*)
      ObtemVersaoServico(EdNFeVerConLot, FmNFeGeraXML_0400.NomeAcao(NFeGeraXML_0400.tcwsNFeRetAutorizacao))
  end;
  {$EndIf}
end;

procedure TFmParamsEmp.EdNFeVerConNFeKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  {$IfNDef SemNFe_0000}
  if Key = VK_F4 then
  begin
(*
    {$IfNDef semNFe_v0200}
    if EdVersao.ValueVariant = 2 then
      ObtemVersaoServico(EdNFeVerConNFe, FmNFeGeraXML_0200.NomeAcao(NFeGeraXML_0200.tcwsConsultaNFe))
    else
    {$EndIf}
    if EdVersao.ValueVariant = 3.1 then
      ObtemVersaoServico(EdNFeVerConNFe, FmNFeGeraXML_0310.NomeAcao(NFeGeraXML_0310.tcwsConsultaNFe))
    else
*)
      ObtemVersaoServico(EdNFeVerConNFe, FmNFeGeraXML_0400.NomeAcao(NFeGeraXML_0400.tcwsNfeConsultaProtocolo))
    end;
  {$EndIf}
end;

procedure TFmParamsEmp.EdNFeVerConsCadKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  {$IfNDef SemNFe_0000}
  if Key = VK_F4 then
  begin
(*
    {$IfNDef semNFe_v0200}
    if EdVersao.ValueVariant = 2 then
      ObtemVersaoServico(EdNFeVerConsCad, FmNFeGeraXML_0200.NomeAcao(NFeGeraXML_0200.tcwsConsultaCadastro))
    else
    {$EndIf}
    if EdVersao.ValueVariant = 3.1 then
      ObtemVersaoServico(EdNFeVerConsCad, FmNFeGeraXML_0310.NomeAcao(NFeGeraXML_0310.tcwsConsultaCadastro))
    else
*)
      ObtemVersaoServico(EdNFeVerConsCad, FmNFeGeraXML_0400.NomeAcao(NFeGeraXML_0400.tcwsNfeConsultaCadastro));
  end;
  {$EndIf}
end;

procedure TFmParamsEmp.EdNFeVerDistDFeIntKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  {$IfNDef SemNFe_0000}
  if Key = VK_F4 then
  begin
(*
    {$IfNDef semNFe_v0200}
    if EdVersao.ValueVariant = 2 then
      ObtemVersaoServico(EdNFeVerDistDFeInt, FmNFeGeraXML_0200.NomeAcao(NFeGeraXML_0200.tcwsConsultaNFeDest))
    else
    {$EndIf}
    if EdVersao.ValueVariant = 3.1 then
      ObtemVersaoServico(EdNFeVerDistDFeInt, FmNFeGeraXML_0310.NomeAcao(NFeGeraXML_0310.tcwsConsultaNFeDest))
    else
*)
      EdNFeVerDistDFeInt.ValueVariant := 0; //N�o tem cancelamento na NF-e 4.0
  end;
  {$EndIf}
end;

procedure TFmParamsEmp.EdNFeVerDowNFeKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  {$IfNDef SemNFe_0000}
  if Key = VK_F4 then
  begin
(*
    {$IfNDef semNFe_v0200}
    if EdVersao.ValueVariant = 2 then
      ObtemVersaoServico(EdNFeVerDowNFe, FmNFeGeraXML_0200.NomeAcao(NFeGeraXML_0200.tcwsDownloadNFeDest))
    else
    {$EndIf}
    if EdVersao.ValueVariant = 3.1 then
      ObtemVersaoServico(EdNFeVerDowNFe, FmNFeGeraXML_0310.NomeAcao(NFeGeraXML_0310.tcwsNFeDownloadNF))
    else
*)
      EdNFeVerDowNFe.ValueVariant := 0; //N�o tem cancelamento na NF-e 4.0
  end;
  {$EndIf}
end;

procedure TFmParamsEmp.EdNFeVerEnvLotKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  {$IfNDef SemNFe_0000}
  if Key = VK_F4 then
  begin
(*
    {$IfNDef semNFe_v0200}
    if EdVersao.ValueVariant = 2 then
      ObtemVersaoServico(EdNFeVerEnvLot, FmNFeGeraXML_0200.NomeAcao(tcwsEnviaLoteNF))
    else
    {$EndIf}
    if EdVersao.ValueVariant = 3.1 then
      ObtemVersaoServico(EdNFeVerEnvLot, FmNFeGeraXML_0310.NomeAcao(tcwsAutorizacao))
    else
*)
      ObtemVersaoServico(EdNFeVerEnvLot, FmNFeGeraXML_0400.NomeAcao(tcwsNFeAutorizacao));
  end;
  {$EndIf}
end;

procedure TFmParamsEmp.EdNFeVerInuNumKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  {$IfNDef SemNFe_0000}
  if Key = VK_F4 then
  begin
(*
    {$IfNDef semNFe_v0200}
    if EdVersao.ValueVariant = 2 then
      ObtemVersaoServico(EdNFeVerInuNum, FmNFeGeraXML_0200.NomeAcao(NFeGeraXML_0200.tcwsPediInutilizacao))
    else
    {$EndIf}
    if EdVersao.ValueVariant = 3.1 then
      ObtemVersaoServico(EdNFeVerInuNum, FmNFeGeraXML_0310.NomeAcao(NFeGeraXML_0310.tcwsPediInutilizacao))
    else
*)
      ObtemVersaoServico(EdNFeVerInuNum, FmNFeGeraXML_0400.NomeAcao(NFeGeraXML_0400.tcwsNfeInutilizacao));
  end;
  {$EndIf}
end;

procedure TFmParamsEmp.EdNFeVerLotEveKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  {$IfNDef SemNFe_0000}
  if Key = VK_F4 then
  begin
(*
    {$IfNDef semNFe_v0200}
    if EdVersao.ValueVariant = 2 then
      ObtemVersaoServico(EdNFeVerLotEve, FmNFeGeraXML_0200.NomeAcao(NFeGeraXML_0200.tcwsRecepcaoEvento))
    else
    {$EndIf}
    if EdVersao.ValueVariant = 3.1 then
      ObtemVersaoServico(EdNFeVerLotEve, FmNFeGeraXML_0310.NomeAcao(NFeGeraXML_0310.tcwsRecepcaoEvento))
    else
*)
      ObtemVersaoServico(EdNFeVerLotEve, FmNFeGeraXML_0400.NomeAcao(NFeGeraXML_0400.tcwsRecepcaoEvento));
  end;
  {$EndIf}
end;

procedure TFmParamsEmp.EdNFeVerStaSerKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  {$IfNDef SemNFe_0000}
  if Key = VK_F4 then
  begin
(*
    {$IfNDef semNFe_v0200}
    if EdVersao.ValueVariant = 2 then
      ObtemVersaoServico(EdNFeVerStaSer, FmNFeGeraXML_0200.NomeAcao(NFeGeraXML_0200.tcwsStatusServico))
    else
    {$EndIf}
    if EdVersao.ValueVariant = 3.10 then
      ObtemVersaoServico(EdNFeVerStaSer, FmNFeGeraXML_0310.NomeAcao(NFeGeraXML_0310.tcwsStatusServico))
    else
*)
      ObtemVersaoServico(EdNFeVerStaSer, FmNFeGeraXML_0400.NomeAcao(NFeGeraXML_0400.tcwsNfeStatusServico));
  end;
  {$EndIf}
end;

{$IfNDef SemNFe_0000}
procedure TFmParamsEmp.ObtemVersaoServico(Edit: TdmkEdit; Servico: String;
  MaxVersao: Double = 0);
var
  TitCols: array[0..0] of String;
  Versoes: MyArrayLista;
  UF: String;
  tpAmb: Integer;
begin
  TitCols[0] := 'Vers�o';
  //
  UF    := EdUF_Servico.ValueVariant;
  tpAmb := RGide_tpAmb.ItemIndex;
  //
  if UF = '' then
  begin
    PageControl2.ActivePageIndex := 4; //NF-e
    PageControl3.ActivePageIndex := 0; //Configura��es gerais
    //
    Geral.MB_Aviso('Informe o WebService!');
    EdUF_Servico.SetFocus;
  end;
  if tpAmb = 0 then
  begin
    PageControl2.ActivePageIndex := 4; //NF-e
    PageControl3.ActivePageIndex := 5; //Op��es
    //
    Geral.MB_Aviso('Informe a Identifica��o do Ambiente!');
    RGide_tpAmb.SetFocus;
  end;
  //
  if MaxVersao = 0 then
  begin
    Versoes := UnNFe_PF.ObtemVersoesServicoNFe(UF, Servico, tpAmb);
    //
    Edit.ValueVariant := Geral.SelecionaItem(Versoes, 0,
      'SEL-LISTA-000 :: Vers�o do servi�o', TitCols, Screen.Width);
  end else
    Edit.ValueVariant := UnNFe_PF.ObtemVersaoServicoNFe(UF, Servico, tpAmb, MaxVersao);
end;
{$EndIf}

procedure TFmParamsEmp.EdNFSeTipoRpsChange(Sender: TObject);
begin
  EdTXT_NFSeTipoRps1.Text := DModG.DefineTextoTipoRPS(EdNFSeTipoRps.ValueVariant);
end;

procedure TFmParamsEmp.EdUC_PIS_CSTChange(Sender: TObject);
begin
{$IfNDef NO_FINANCEIRO}
  EdUCTextoPIS_CST.Text := UFinanceiro.CST_PIS_Get(EdUC_PIS_CST.Text);
{$EndIf}end;

procedure TFmParamsEmp.EdUC_PIS_CSTKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F3 then
{$IfNDef NO_FINANCEIRO}
    EdUC_PIS_CST.Text := UFinanceiro.ListaDeCST_PIS();
{$EndIf}end;

procedure TFmParamsEmp.EdSPED_EFD_IND_ATIVChange(Sender: TObject);
begin
  {$IfNDef NO_FINANCEIRO}
  EdSPED_EFD_IND_ATIV_TXT.Text :=
    UFinanceiro.SPED_EFD_IND_ATIV_Get(EdSPED_EFD_IND_ATIV.ValueVariant);
{$EndIf}
end;

procedure TFmParamsEmp.EdSPED_EFD_IND_ATIVKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  {$IfNDef NO_FINANCEIRO}
  if Key = VK_F3 then
    EdSPED_EFD_IND_ATIV.Text := UFinanceiro.ListaDeSPED_EFD_IND_ATIV();
{$EndIf}
end;

procedure TFmParamsEmp.EdSPED_EFD_IND_NAT_PJChange(Sender: TObject);
begin
{$IfNDef SemNFe_0000}
  EdSPED_EFD_IND_NAT_PJ_TXT.Text := UnNFe_PF.TextoDeCodigoSPED_TXT(spedIND_NAT_PJ,
    EdSPED_EFD_IND_NAT_PJ.Text);
{$EndIf}
end;

procedure TFmParamsEmp.EdSPED_EFD_IND_NAT_PJKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
  TitCols: array[0..2] of String;
begin
  if Key = VK_F4 then
  begin
    TitCols[0] := 'UF';
    TitCols[1] := 'Sigla';
    TitCols[2] := 'Descri��o';
    //
    EdSPED_EFD_IND_NAT_PJ_TXT.Text := Geral.SelecionaItem(F_IND_NAT_PJ, 1,
      'SEL-LISTA-000 :: Indicador da natureza da pessoa jur�dica', TitCols, Screen.Width)
  end;
end;

procedure TFmParamsEmp.EdSPED_EFD_IND_PERFILChange(Sender: TObject);
begin
  {$IfNDef NO_FINANCEIRO}
  EdSPED_EFD_IND_PERFIL_TXT.Text :=
    UFinanceiro.SPED_EFD_IND_PERFIL_Get(EdSPED_EFD_IND_PERFIL.ValueVariant);
{$EndIf}
end;

procedure TFmParamsEmp.EdSPED_EFD_IND_PERFILKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  {$IfNDef NO_FINANCEIRO}
  if Key = VK_F3 then
    EdSPED_EFD_IND_PERFIL.Text := UFinanceiro.ListaDeSPED_EFD_IND_PERFIL();
{$EndIf}
end;

procedure TFmParamsEmp.EdUF_ServicoKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  TitCols: array[0..2] of String;
begin
  if Key = VK_F4 then
  begin
    TitCols[0] := 'UF';
    TitCols[1] := 'Sigla';
    TitCols[2] := 'Descri��o';
    //
    EdUF_Servico.Text := Geral.SelecionaItem(FSiglas_WS, 1,
      'SEL-LISTA-000 :: Sigla da Web Service', TitCols, Screen.Width)
  end;
end;

procedure TFmParamsEmp.EdVersaoKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
{$IfNDef SemNFe_0000}
var
  Versao: Double;
{$EndIf}
begin
  {$IfNDef SemNFe_0000}
  if Key = VK_F4 then
  begin
    Versao := EdVersao.ValueVariant;
    //
    if MyObjects.FIC(Versao = 0, EdVersao, 'Informe a vers�o') then Exit;
    //
(*
    if Versao = 2 then
    begin
    {$IfNDef semNFe_v0200}
      ObtemVersaoServico(EdNFeVerStaSer,     FmNFeGeraXML_0200.NomeAcao(NFeGeraXML_0200.tcwsStatusServico), Versao);
      ObtemVersaoServico(EdNFeVerEnvLot,     FmNFeGeraXML_0200.NomeAcao(NFeGeraXML_0200.tcwsEnviaLoteNF), Versao);
      ObtemVersaoServico(EdNFeVerConLot,     FmNFeGeraXML_0200.NomeAcao(NFeGeraXML_0200.tcwsConsultaLote), Versao);
      ObtemVersaoServico(EdNFeVerCanNFe,     FmNFeGeraXML_0200.NomeAcao(NFeGeraXML_0200.tcwsPedeCancelamento), Versao);
      ObtemVersaoServico(EdNFeVerInuNum,     FmNFeGeraXML_0200.NomeAcao(NFeGeraXML_0200.tcwsPediInutilizacao), Versao);
      ObtemVersaoServico(EdNFeVerConNFe,     FmNFeGeraXML_0200.NomeAcao(NFeGeraXML_0200.tcwsConsultaNFe), Versao);
      ObtemVersaoServico(EdNFeVerLotEve,     FmNFeGeraXML_0200.NomeAcao(NFeGeraXML_0200.tcwsRecepcaoEvento), Versao);
      ObtemVersaoServico(EdNFeVerConsCad,    FmNFeGeraXML_0200.NomeAcao(NFeGeraXML_0200.tcwsConsultaCadastro), Versao);
      ObtemVersaoServico(EdNFeVerDistDFeInt, FmNFeGeraXML_0200.NomeAcao(NFeGeraXML_0200.tcwsConsultaNFeDest), Versao);
      ObtemVersaoServico(EdNFeVerDowNFe,     FmNFeGeraXML_0200.NomeAcao(NFeGeraXML_0200.tcwsDownloadNFeDest), Versao);
    {$EndIf}
    end else
    if Versao = 3.10 then
    begin
      ObtemVersaoServico(EdNFeVerStaSer,     FmNFeGeraXML_0310.NomeAcao(NFeGeraXML_0310.tcwsStatusServico), Versao);
      ObtemVersaoServico(EdNFeVerEnvLot,     FmNFeGeraXML_0310.NomeAcao(NFeGeraXML_0310.tcwsAutorizacao), Versao);
      ObtemVersaoServico(EdNFeVerConLot,     FmNFeGeraXML_0310.NomeAcao(NFeGeraXML_0310.tcwsRetAutorizacao), Versao);
      ObtemVersaoServico(EdNFeVerCanNFe,     FmNFeGeraXML_0310.NomeAcao(NFeGeraXML_0310.tcwsPedeCancelamento), Versao);
      ObtemVersaoServico(EdNFeVerInuNum,     FmNFeGeraXML_0310.NomeAcao(NFeGeraXML_0310.tcwsPediInutilizacao), Versao);
      ObtemVersaoServico(EdNFeVerConNFe,     FmNFeGeraXML_0310.NomeAcao(NFeGeraXML_0310.tcwsConsultaNFe), Versao);
      ObtemVersaoServico(EdNFeVerLotEve,     FmNFeGeraXML_0310.NomeAcao(NFeGeraXML_0310.tcwsRecepcaoEvento), Versao);
      ObtemVersaoServico(EdNFeVerConsCad,    FmNFeGeraXML_0310.NomeAcao(NFeGeraXML_0310.tcwsConsultaCadastro), Versao);
      ObtemVersaoServico(EdNFeVerDistDFeInt, FmNFeGeraXML_0310.NomeAcao(NFeGeraXML_0310.tcwsConsultaNFeDest), Versao);
      ObtemVersaoServico(EdNFeVerDowNFe,     FmNFeGeraXML_0310.NomeAcao(NFeGeraXML_0310.tcwsNFeDownloadNF), Versao);
    end else
*)
    begin
      ObtemVersaoServico(EdNFeVerStaSer,  FmNFeGeraXML_0400.NomeAcao(NFeGeraXML_0400.tcwsNfeStatusServico), Versao);
      ObtemVersaoServico(EdNFeVerEnvLot,  FmNFeGeraXML_0400.NomeAcao(NFeGeraXML_0400.tcwsNFeAutorizacao), Versao);
      ObtemVersaoServico(EdNFeVerConLot,  FmNFeGeraXML_0400.NomeAcao(NFeGeraXML_0400.tcwsNFeRetAutorizacao), Versao);
      ObtemVersaoServico(EdNFeVerInuNum,  FmNFeGeraXML_0400.NomeAcao(NFeGeraXML_0400.tcwsNFeInutilizacao), Versao);
      ObtemVersaoServico(EdNFeVerConNFe,  FmNFeGeraXML_0400.NomeAcao(NFeGeraXML_0400.tcwsNfeConsultaProtocolo), Versao);
      ObtemVersaoServico(EdNFeVerLotEve,  FmNFeGeraXML_0400.NomeAcao(NFeGeraXML_0400.tcwsRecepcaoEvento), Versao);
      ObtemVersaoServico(EdNFeVerConsCad, FmNFeGeraXML_0400.NomeAcao(NFeGeraXML_0400.tcwsNfeConsultaCadastro), Versao);
      //
      //N�o tem cancelamento na NF-e 4.0
      EdNFeVerCanNFe.ValueVariant     := 0;
      EdNFeVerDistDFeInt.ValueVariant := 0;
      EdNFeVerDowNFe.ValueVariant     := 0;
    end;
  end;
  {$EndIf}
end;

procedure TFmParamsEmp.MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo: Integer);
begin
  case Mostra of
    0:
    begin
      PainelDados.Visible := True;
      PainelEdita.Visible := False;
    end;
    else
      Geral.MB_Aviso('A��o de Inclus�o/altera��o n�o definida!');
  end;
  ImgTipo.SQLType := SQLType;
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmParamsEmp.PMFilialPopup(Sender: TObject);
begin
  Alterafilialatual1.Enabled :=
    (QrParamsEmp.State <> dsInactive) and (QrParamsEmp.RecordCount > 0);
end;

procedure TFmParamsEmp.PMSerieNFCPopup(Sender: TObject);
begin
  Incluinovasriedenotafiscal1.Enabled :=
    (QrParamsEmp.State <> dsInactive)
  and
    (QrParamsEmp.RecordCount >0);
  //
  Alterasriedenotafiscalatual1.Enabled :=
    (QrParamsNFCs.State <> dsInactive)
  and
    (QrParamsNFCs.RecordCount >0);
end;

procedure TFmParamsEmp.PMSerieNFPopup(Sender: TObject);
begin
  Incluinovasriedenotafiscal1.Enabled :=
    (QrParamsEmp.State <> dsInactive)
  and
    (QrParamsEmp.RecordCount >0);
  //
  Alterasriedenotafiscalatual1.Enabled :=
    (QrParamsNFs.State <> dsInactive)
  and
    (QrParamsNFs.RecordCount >0);
end;

procedure TFmParamsEmp.PreencheCamposAlteraOpcoesMis();
begin
    //EdCodigo.ValueVariant :=                        QrParamsEmpCodigo.Value;
    EdLogo3x1.ValueVariant :=                       QrParamsEmpLogo3x1.Value;
    EdTZD_UTC.ValueVariant :=                       QrParamsEmpTZD_UTC.Value;
    TphVeraoAsk.Date :=                             QrParamsEmphVeraoAsk.Value;
    TPhVeraoIni.Date :=                             QrParamsEmphVeraoIni.Value;
    TPhVeraoFim.Date :=                             QrParamsEmphVeraoFim.Value;
    CkTZD_UTC_Auto.Checked :=                       Geral.IntToBool(QrParamsEmpTZD_UTC_Auto.Value);
    EdCartEmisHonFun.ValueVariant :=                QrParamsEmpCartEmisHonFun.Value;
    EdSituacao.ValueVariant :=                      QrParamsEmpSituacao.Value;
    RGFatSemEstq.ItemIndex :=                       QrParamsEmpFatSemEstq.Value;
    RGTipMediaDD.ItemIndex :=                       QrParamsEmpTipMediaDD.Value;
    RGFatSemPrcL.ItemIndex :=                       QrParamsEmpFatSemPrcL.Value;
    RGTipCalcJuro.ItemIndex :=                      QrParamsEmpTipCalcJuro.Value;
    RGPedVdaMudLista.ItemIndex :=                   QrParamsEmpPedVdaMudLista.Value;
    RGPedVdaMudPrazo.ItemIndex :=                   QrParamsEmpPedVdaMudPrazo.Value;
    CkPediVdaNElertas.Checked :=                    Geral.IntToBool(QrParamsEmpPediVdaNElertas.Value);
    EdAssocModNF.ValueVariant :=                    QrParamsEmpAssocModNF.Value;
    RGFaturaSeq.ItemIndex :=                        QrParamsEmpFaturaSeq.Value;
    EdFaturaSep.ValueVariant :=                     QrParamsEmpFaturaSep.Value;
    RGFaturaDta.ItemIndex :=                        QrParamsEmpFaturaDta.Value;
    EdCtaProdVen.ValueVariant :=                    QrParamsEmpCtaProdVen.Value;
    EdTxtProdVen.ValueVariant :=                    QrParamsEmpTxtProdVen.Value;
    EdCtaServico.ValueVariant :=                    QrParamsEmpCtaServico.Value;
    EdTxtServico.ValueVariant :=                    QrParamsEmpTxtServico.Value;
    EdDupProdVen.ValueVariant :=                    QrParamsEmpDupProdVen.Value;
    EdDupServico.ValueVariant :=                    QrParamsEmpDupServico.Value;
    RGFaturaNum.ItemIndex :=                        QrParamsEmpFaturaNum.Value;
    RGUsaReferen.ItemIndex :=                       QrParamsEmpUsaReferen.Value;
    EdCtaProdCom.ValueVariant :=                    QrParamsEmpCtaProdCom.Value;
    EdTxtProdCom.ValueVariant :=                    QrParamsEmpTxtProdCom.Value;
    EdDupProdCom.ValueVariant :=                    QrParamsEmpDupProdCom.Value;
    EdCtaServicoPg.ValueVariant :=                  QrParamsEmpCtaServicoPg.Value;
    EdTxtServicoPg.ValueVariant :=                  QrParamsEmpTxtServicoPg.Value;
    EdDupServicoPg.ValueVariant :=                  QrParamsEmpDupServicoPg.Value;
    EdCtaFretPrest.ValueVariant :=                  QrParamsEmpCtaFretPrest.Value;
    EdTxtFretPrest.ValueVariant :=                  QrParamsEmpTxtFretPrest.Value;
    EdDupFretPrest.ValueVariant :=                  QrParamsEmpDupFretPrest.Value;
    EdFreteRpICMS.ValueVariant :=                   QrParamsEmpFreteRpICMS.Value;
    EdFreteRpPIS.ValueVariant :=                    QrParamsEmpFreteRpPIS.Value;
    EdFreteRpCOFINS.ValueVariant :=                 QrParamsEmpFreteRpCOFINS.Value;
    CkRetImpost.Checked :=                          Geral.IntToBool(QrParamsEmpRetImpost.Value);
    EdBalQtdItem.ValueVariant :=                    QrParamsEmpBalQtdItem.Value;
    CkEstq0UsoCons.Checked :=                       Geral.IntToBool(QrParamsEmpEstq0UsoCons.Value);

    CkSimplesFed.Checked :=                         Geral.IntToBool(QrParamsEmpSimplesFed.Value);
    EdpCredSNMez.ValueVariant :=                    QrParamsEmppCredSNMez.Value;


    EdSPED_EFD_IND_PERFIL.ValueVariant :=           QrParamsEmpSPED_EFD_IND_PERFIL.Value;
    EdSPED_EFD_IND_ATIV.ValueVariant :=             QrParamsEmpSPED_EFD_IND_ATIV.Value;
    EdCLAS_ESTAB_IND.ValueVariant    :=             QrParamsEmpCLAS_ESTAB_IND.Value;
    EdSPED_EFD_CadContador.ValueVariant :=          QrParamsEmpSPED_EFD_CadContador.Value;
    EdSPED_EFD_CRCContador.ValueVariant :=          QrParamsEmpSPED_EFD_CRCContador.Value;
    EdSPED_EFD_EscriContab.ValueVariant :=          QrParamsEmpSPED_EFD_EscriContab.Value;
    RGSPED_EFD_EnderContab.ItemIndex :=             QrParamsEmpSPED_EFD_EnderContab.Value;
    RGSPED_EFD_DtaFiscalEntrada.ItemIndex :=        QrParamsEmpSPED_EFD_DtaFiscalEntrada.Value;
    RGSPED_EFD_DtaFiscalSaida.ItemIndex :=          QrParamsEmpSPED_EFD_DtaFiscalSaida.Value;
    RGSPED_EFD_ID_0150.ItemIndex   :=               QrParamsEmpSPED_EFD_ID_0150.Value;
    RGSPED_EFD_ID_0200.ItemIndex   :=               QrParamsEmpSPED_EFD_ID_0200.Value;
    RGSPED_EFD_Peri_K100.ItemIndex   :=             QrParamsEmpSPED_EFD_Peri_K100.Value;
    RGSPED_EFD_Peri_E500.ItemIndex   :=             QrParamsEmpSPED_EFD_Peri_E500.Value;
    RGSPED_EFD_Peri_E100.ItemIndex   :=             QrParamsEmpSPED_EFD_Peri_E100.Value;
    RGSPED_EFD_Producao.ItemIndex   :=              QrParamsEmpSPED_EFD_Producao.Value;
    CkSPED_EFD_MovSubPrd.Checked :=                 Geral.IntToBool(QrParamsEmpSPED_EFD_MovSubPrd.Value);
    EdSPED_EFD_Path.ValueVariant :=                 QrParamsEmpSPED_EFD_Path.Value;
    EdSPED_EFD_ICMS_IPI_VersaoGuia.ValueVariant :=  QrParamsEmpSPED_EFD_ICMS_IPI_VersaoGuia.Value;
    EdSPED_EFD_IND_NAT_PJ.ValueVariant :=           QrParamsEmpSPED_EFD_IND_NAT_PJ.Value;
    RGRegimTributar.ItemIndex :=                    QrParamsEmpRegimTributar.Value;
    RGRegimPisCofins.ItemIndex :=                   QrParamsEmpRegimPisCofins.Value;
    RGRegimCumulativ.ItemIndex :=                   QrParamsEmpRegimCumulativ.Value;
    EdCodDctfPis.ValueVariant :=                    QrParamsEmpCodDctfPis.Value;
    EdCodDctfCofins.ValueVariant :=                 QrParamsEmpCodDctfCofins.Value;


    EdSPED_COD_INC_TRIB.ValueVariant :=             QrParamsEmpSPED_COD_INC_TRIB.Value;
    EdSPED_IND_APRO_CRED.ValueVariant :=            QrParamsEmpSPED_IND_APRO_CRED.Value;
    EdSPED_COD_TIPO_CONT.ValueVariant :=            QrParamsEmpSPED_COD_TIPO_CONT.Value;
    EdSPED_IND_REG_CUM.ValueVariant :=              QrParamsEmpSPED_IND_REG_CUM.Value;
    EdNFSeMetodo.ValueVariant :=                    QrParamsEmpNFSeMetodo.Value;
    EdNFSeVersao.ValueVariant :=                    QrParamsEmpNFSeVersao.Value;
    EdNFSeSerieRps.ValueVariant :=                  QrParamsEmpNFSeSerieRps.Value;
    EdDpsNumero.ValueVariant :=                     QrParamsEmpDpsNumero.Value;
    EdDpsNumHom.ValueVariant :=                     QrParamsEmpDpsNumHom.Value;
    EdNFSeWSProducao.ValueVariant :=                QrParamsEmpNFSeWSProducao.Value;
    RGNFSeAmbiente.ItemIndex :=                     QrParamsEmpNFSeAmbiente.Value;
    EdNFSeWSHomologa.ValueVariant :=                QrParamsEmpNFSeWSHomologa.Value;
    EdNFSeCertDigital.ValueVariant :=               QrParamsEmpNFSeCertDigital.Value;
    TPNFSeCertValidad.Date :=                       QrParamsEmpNFSeCertValidad.Value;
    EdNFSeCertAviExpi.ValueVariant :=               QrParamsEmpNFSeCertAviExpi.Value;
    EdRPSNumLote.ValueVariant :=                    QrParamsEmpRPSNumLote.Value;
    EdHomNumLote.ValueVariant :=                    QrParamsEmpHomNumLote.Value;
    EdNFSeUserWeb.ValueVariant :=                   QrParamsEmpNFSeUserWeb.Value;
    EdNFSeSenhaWeb.ValueVariant :=                  QrParamsEmpNFSeSenhaWeb.Value;
    EdNFSeWebFraseSecr.ValueVariant :=              QrParamsEmpNFSeWebFraseSecr.Value;
    EdNFSeCodMunici.ValueVariant :=                 QrParamsEmpNFSeCodMunici.Value;
    EdNFSePrefeitura1.ValueVariant :=               QrParamsEmpNFSePrefeitura1.Value;
    CkNFSeMsgVisu.Checked :=                        Geral.IntToBool(QrParamsEmpNFSeMsgVisu.Value);
    EdNFSeLogoFili.ValueVariant :=                  QrParamsEmpNFSeLogoFili.Value;
    EdNFSeLogoPref.ValueVariant :=                  QrParamsEmpNFSeLogoPref.Value;
    EdNFSePrefeitura2.ValueVariant :=               QrParamsEmpNFSePrefeitura2.Value;
    EdNFSePrefeitura3.ValueVariant :=               QrParamsEmpNFSePrefeitura3.Value;
    RGNFSeMetodEnvRPS.ItemIndex :=                  QrParamsEmpNFSeMetodEnvRPS.Value;
    EdNFSeTipoRps.ValueVariant :=                   QrParamsEmpNFSeTipoRps.Value;
    RG_NFSe_indFinalCpl.ItemIndex :=                QrParamsEmpNFSe_indFinalCpl.Value;
    EdDirNFSeRPSEnvLot.ValueVariant :=              QrParamsEmpDirNFSeRPSEnvLot.Value;
    EdDirNFSeRPSRecLot.ValueVariant :=              QrParamsEmpDirNFSeRPSRecLot.Value;
    EdDirNFSeIniFiles.ValueVariant :=               QrParamsEmpDirNFSeIniFiles.Value;
    EdDirNFSeDPSAss.ValueVariant :=                 QrParamsEmpDirNFSeDPSAss.Value;
    EdDirNFSeDPSGer.ValueVariant :=                 QrParamsEmpDirNFSeDPSGer.Value;
    EdDirNFSeNFSAut.ValueVariant :=                 QrParamsEmpDirNFSeNFSAut.Value;
    EdDirNFSeNFSCan.ValueVariant :=                 QrParamsEmpDirNFSeNFSCan.Value;
    EdDirNFSeLogs.ValueVariant :=                   QrParamsEmpDirNFSeLogs.Value;
    EdDirNFSeSchema.ValueVariant :=                 QrParamsEmpDirNFSeSchema.Value;
    EdNFSePreMailAut.ValueVariant :=                QrParamsEmpNFSePreMailAut.Value;
    EdNFSePreMailCan.ValueVariant :=                QrParamsEmpNFSePreMailCan.Value;
    EdMyEmailNFSe.ValueVariant :=                   QrParamsEmpMyEmailNFSe.Value;
    RGCTetpEmis.ItemIndex :=                        QrParamsEmpCTetpEmis.Value;
    EdCTePreMailAut.ValueVariant :=                 QrParamsEmpCTePreMailAut.Value;
    EdCTePreMailCan.ValueVariant :=                 QrParamsEmpCTePreMailCan.Value;
    EdCTePreMailEveCCe.ValueVariant :=              QrParamsEmpCTePreMailEveCCe.Value;
    EdDirCTeProt.ValueVariant :=                    QrParamsEmpDirCTeProt.Value;
    EdDirCTeRWeb.ValueVariant :=                    QrParamsEmpDirCTeRWeb.Value;
    EdCTeSCAN_Ser.ValueVariant :=                   QrParamsEmpCTeSCAN_Ser.Value;
    EdCTeSCAN_nCT.ValueVariant :=                   QrParamsEmpCTeSCAN_nCT.Value;
    EdCTeUF_WebServ.ValueVariant :=                 QrParamsEmpCTeUF_WebServ.Value;
    EdCTeUF_Servico.ValueVariant :=                 QrParamsEmpCTeUF_Servico.Value;
    EdCTeSerNum.ValueVariant :=                     QrParamsEmpCTeSerNum.Value;
    TPCTeSerVal.Date :=                             QrParamsEmpCTeSerVal.Value;
    EdCTeSerAvi.ValueVariant :=                     QrParamsEmpCTeSerAvi.Value;
    EdCTeUF_EPEC.ValueVariant :=                    QrParamsEmpCTeUF_EPEC.Value;
    EdCTeUF_Conting.ValueVariant :=                 QrParamsEmpCTeUF_Conting.Value;
    EdDirCTeEnvLot.ValueVariant :=                  QrParamsEmpDirCTeEnvLot.Value;
    EdDirCTeRec.ValueVariant :=                     QrParamsEmpDirCTeRec.Value;
    EdDirCTeProRec.ValueVariant :=                  QrParamsEmpDirCTeProRec.Value;
    EdDirCTeAss.ValueVariant :=                     QrParamsEmpDirCTeAss.Value;
    EdDirCTeGer.ValueVariant :=                     QrParamsEmpDirCTeGer.Value;
    EdDirCTeDen.ValueVariant :=                     QrParamsEmpDirCTeDen.Value;
    EdDirCTePedRec.ValueVariant :=                  QrParamsEmpDirCTePedRec.Value;
    EdDirCTePedSta.ValueVariant :=                  QrParamsEmpDirCTePedSta.Value;
    EdDirCTeSta.ValueVariant :=                     QrParamsEmpDirCTeSta.Value;
    EdDirCTePedCan.ValueVariant :=                  QrParamsEmpDirCTePedCan.Value;
    EdDirCTeCan.ValueVariant :=                     QrParamsEmpDirCTeCan.Value;
    EdDirCTePedInu.ValueVariant :=                  QrParamsEmpDirCTePedInu.Value;
    EdDirCTeInu.ValueVariant :=                     QrParamsEmpDirCTeInu.Value;
    EdDirCTePedSit.ValueVariant :=                  QrParamsEmpDirCTePedSit.Value;
    EdDirCTeSit.ValueVariant :=                     QrParamsEmpDirCTeSit.Value;
    EdDirCTeEveEnvLot.ValueVariant :=               QrParamsEmpDirCTeEveEnvLot.Value;
    EdDirCTeEveRetLot.ValueVariant :=               QrParamsEmpDirCTeEveRetLot.Value;
    EdDirCTeEvePedCCe.ValueVariant :=               QrParamsEmpDirCTeEvePedCCe.Value;
    EdDirCTeEveRetCCe.ValueVariant :=               QrParamsEmpDirCTeEveRetCCe.Value;
    EdDirCTeEvePedCan.ValueVariant :=               QrParamsEmpDirCTeEvePedCan.Value;
    EdDirCTeEveRetCan.ValueVariant :=               QrParamsEmpDirCTeEveRetCan.Value;
    EdDirCTEEveProcCCe.ValueVariant :=              QrParamsEmpDirCTEEveProcCCe.Value;
    EdDirCTeEvePedEPEC.ValueVariant :=              QrParamsEmpDirCTeEvePedEPEC.Value;
    EdDirCTeEveRetEPEC.ValueVariant :=              QrParamsEmpDirCTeEveRetEPEC.Value;
    EdCTeversao.ValueVariant :=                     QrParamsEmpCTeversao.Value;
    EdCTeide_mod.ValueVariant :=                    QrParamsEmpCTeide_mod.Value;
    RGCTeide_tpAmb.ItemIndex :=                     QrParamsEmpCTeide_tpAmb.Value;
    EdDirCTeSchema.ValueVariant :=                  QrParamsEmpDirCTeSchema.Value;
    RGCTeAppCode.ItemIndex :=                       QrParamsEmpCTeAppCode.Value;
    RGCTeAssDigMode.ItemIndex :=                    QrParamsEmpCTeAssDigMode.Value;
    EdMyEmailCTe.ValueVariant :=                    QrParamsEmpMyEmailCTe.Value;
    EdCTeInfCpl.ValueVariant :=                     QrParamsEmpCTeInfCpl.Value;
    EdCTeVerStaSer.ValueVariant :=                  QrParamsEmpCTeVerStaSer.Value;
    EdCTeVerEnvLot.ValueVariant :=                  QrParamsEmpCTeVerEnvLot.Value;
    EdCTeVerConLot.ValueVariant :=                  QrParamsEmpCTeVerConLot.Value;
    EdCTeVerInuNum.ValueVariant :=                  QrParamsEmpCTeVerInuNum.Value;
    EdCTeVerConCTe.ValueVariant :=                  QrParamsEmpCTeVerConCTe.Value;
    EdCTeVerLotEve.ValueVariant :=                  QrParamsEmpCTeVerLotEve.Value;
    EdCTeVerEPEC.ValueVariant :=                    QrParamsEmpCTeVerEPEC.Value;
    RGCTeide_tpImp.ItemIndex :=                     QrParamsEmpCTeide_tpImp.Value;
    RGCTeItsLin.ItemIndex :=                        QrParamsEmpCTeItsLin.Value;
    EdCTeFTRazao.ValueVariant :=                    QrParamsEmpCTeFTRazao.Value;
    EdCTeFTEnder.ValueVariant :=                    QrParamsEmpCTeFTEnder.Value;
    EdCTeFTFones.ValueVariant :=                    QrParamsEmpCTeFTFones.Value;
    CkCTeMaiusc.Checked :=                          Geral.IntToBool(QrParamsEmpCTeMaiusc.Value);
    EdCTeShowURL.ValueVariant :=                    QrParamsEmpCTeShowURL.Value;
    EdPathLogoCTe.ValueVariant :=                   QrParamsEmpPathLogoCTe.Value;
    EdDirDACTes.ValueVariant :=                     QrParamsEmpDirDACTes.Value;
    RG_CTe_indFinalCpl.ItemIndex :=                 QrParamsEmpCTe_indFinalCpl.Value;
    EdMDFePreMailAut.ValueVariant :=                QrParamsEmpMDFePreMailAut.Value;
    EdMDFePreMailCan.ValueVariant :=                QrParamsEmpMDFePreMailCan.Value;
    EdMDFePreMailEveCCe.ValueVariant :=             QrParamsEmpMDFePreMailEveCCe.Value;
    EdDirMDFeProt.ValueVariant :=                   QrParamsEmpDirMDFeProt.Value;
    EdDirMDFeRWeb.ValueVariant :=                   QrParamsEmpDirMDFeRWeb.Value;
    EdMDFeSCAN_Ser.ValueVariant :=                  QrParamsEmpMDFeSCAN_Ser.Value;
    EdMDFeSCAN_nMDF.ValueVariant :=                 QrParamsEmpMDFeSCAN_nMDF.Value;
    EdMDFeUF_WebServ.ValueVariant :=                QrParamsEmpMDFeUF_WebServ.Value;
    EdMDFeUF_Servico.ValueVariant :=                QrParamsEmpMDFeUF_Servico.Value;
    EdMDFeSerNum.ValueVariant :=                    QrParamsEmpMDFeSerNum.Value;
    TPMDFeSerVal.Date :=                            QrParamsEmpMDFeSerVal.Value;
    EdMDFeSerAvi.ValueVariant :=                    QrParamsEmpMDFeSerAvi.Value;
    EdDirMDFeEnvLot.ValueVariant :=                 QrParamsEmpDirMDFeEnvLot.Value;
    EdDirMDFeRec.ValueVariant :=                    QrParamsEmpDirMDFeRec.Value;
    EdDirMDFeProRec.ValueVariant :=                 QrParamsEmpDirMDFeProRec.Value;
    EdDirMDFeAss.ValueVariant :=                    QrParamsEmpDirMDFeAss.Value;
    EdDirMDFeGer.ValueVariant :=                    QrParamsEmpDirMDFeGer.Value;
    EdDirMDFeDen.ValueVariant :=                    QrParamsEmpDirMDFeDen.Value;
    EdDirMDFePedRec.ValueVariant :=                 QrParamsEmpDirMDFePedRec.Value;
    EdDirMDFePedSta.ValueVariant :=                 QrParamsEmpDirMDFePedSta.Value;
    EdDirMDFeSta.ValueVariant :=                    QrParamsEmpDirMDFeSta.Value;
    EdDirMDFePedCan.ValueVariant :=                 QrParamsEmpDirMDFePedCan.Value;
    EdDirMDFeCan.ValueVariant :=                    QrParamsEmpDirMDFeCan.Value;
    EdDirMDFePedInu.ValueVariant :=                 QrParamsEmpDirMDFePedInu.Value;
    EdDirMDFeInu.ValueVariant :=                    QrParamsEmpDirMDFeInu.Value;
    EdDirMDFePedSit.ValueVariant :=                 QrParamsEmpDirMDFePedSit.Value;
    EdDirMDFeSit.ValueVariant :=                    QrParamsEmpDirMDFeSit.Value;
    EdDirMDFeEveEnvLot.ValueVariant :=              QrParamsEmpDirMDFeEveEnvLot.Value;
    EdDirMDFeEveRetLot.ValueVariant :=              QrParamsEmpDirMDFeEveRetLot.Value;
    EdDirMDFeEvePedEnc.ValueVariant :=              QrParamsEmpDirMDFeEvePedEnc.Value;
    EdDirMDFeEveRetEnc.ValueVariant :=              QrParamsEmpDirMDFeEveRetEnc.Value;
    EdDirMDFeEvePedCan.ValueVariant :=              QrParamsEmpDirMDFeEvePedCan.Value;
    EdDirMDFeEveRetCan.ValueVariant :=              QrParamsEmpDirMDFeEveRetCan.Value;
    EdDirMDFeEvePedIdC.ValueVariant :=              QrParamsEmpDirMDFeEvePedIdC.Value;
    EdDirMDFeEveRetIdC.ValueVariant :=              QrParamsEmpDirMDFeEveRetIdC.Value;
    EdMDFeversao.ValueVariant :=                    QrParamsEmpMDFeversao.Value;
    EdMDFeide_mod.ValueVariant :=                   QrParamsEmpMDFeide_mod.Value;
    RGMDFeide_tpAmb.ItemIndex :=                    QrParamsEmpMDFeide_tpAmb.Value;
    EdDirMDFeSchema.ValueVariant :=                 QrParamsEmpDirMDFeSchema.Value;
    RGMDFeAppCode.ItemIndex :=                      QrParamsEmpMDFeAppCode.Value;
    RGMDFeAssDigMode.ItemIndex :=                   QrParamsEmpMDFeAssDigMode.Value;
    EdMyEmailMDFe.ValueVariant :=                   QrParamsEmpMyEmailMDFe.Value;
    EdMDFeInfCpl.ValueVariant :=                    QrParamsEmpMDFeInfCpl.Value;
    EdMDFeVerStaSer.ValueVariant :=                 QrParamsEmpMDFeVerStaSer.Value;
    EdMDFeVerEnvLot.ValueVariant :=                 QrParamsEmpMDFeVerEnvLot.Value;
    EdMDFeVerConLot.ValueVariant :=                 QrParamsEmpMDFeVerConLot.Value;
    EdMDFeVerInuNum.ValueVariant :=                 QrParamsEmpMDFeVerInuNum.Value;
    EdMDFeVerConMDFe.ValueVariant :=                QrParamsEmpMDFeVerConMDFe.Value;
    EdMDFeVerLotEve.ValueVariant :=                 QrParamsEmpMDFeVerLotEve.Value;
    RGMDFeide_tpImp.ItemIndex :=                    QrParamsEmpMDFeide_tpImp.Value;
    RGMDFeItsLin.ItemIndex :=                       QrParamsEmpMDFeItsLin.Value;
    EdMDFeFTRazao.ValueVariant :=                   QrParamsEmpMDFeFTRazao.Value;
    EdMDFeFTEnder.ValueVariant :=                   QrParamsEmpMDFeFTEnder.Value;
    EdMDFeFTFones.ValueVariant :=                   QrParamsEmpMDFeFTFones.Value;
    CkMDFeMaiusc.Checked :=                         Geral.IntToBool(QrParamsEmpMDFeMaiusc.Value);
    EdMDFeShowURL.ValueVariant :=                   QrParamsEmpMDFeShowURL.Value;
    EdPathLogoMDFe.ValueVariant :=                  QrParamsEmpPathLogoMDFe.Value;
    EdDirDAMDFes.ValueVariant :=                    QrParamsEmpDirDAMDFes.Value;
    RG_MDFe_indFinalCpl.ItemIndex :=                QrParamsEmpMDFe_indFinalCpl.Value;
    EdMoeda.ValueVariant :=                         QrParamsEmpMoeda.Value;
    EdAssociada.ValueVariant :=                     QrParamsEmpAssociada.Value;
    EdEntiTipCt1.ValueVariant :=                    QrParamsEmpEntiTipCt1.Value;
    EdEntiTipCto.ValueVariant :=                    QrParamsEmpEntiTipCto.Value;
    EdNFSeTipCtoMail.ValueVariant :=                QrParamsEmpNFSeTipCtoMail.Value;
    EdCTeEntiTipCt1.ValueVariant :=                 QrParamsEmpCTeEntiTipCt1.Value;
    EdCTeEntiTipCto.ValueVariant :=                 QrParamsEmpCTeEntiTipCto.Value;



    RGNFeNT2013_003LTT.ItemIndex :=                 QrPrmsEmpNFeNFeNT2013_003LTT.Value;
    RGNFeUsoCEST.ItemIndex :=                       QrPrmsEmpNFeNFeUsoCEST.Value;
    EdPreMailAut.ValueVariant :=                    QrPrmsEmpNFePreMailAut.Value;
    EdPreMailCan.ValueVariant :=                    QrPrmsEmpNFePreMailCan.Value;
    EdPreMailEveCCe.ValueVariant :=                 QrPrmsEmpNFePreMailEveCCe.Value;
    EdDirNFeProt.ValueVariant :=                    QrPrmsEmpNFeDirNFeProt.Value;
    EdDirNFeRWeb.ValueVariant :=                    QrPrmsEmpNFeDirNFeRWeb.Value;
    EdSCAN_Ser.ValueVariant :=                      QrPrmsEmpNFeSCAN_Ser.Value;
    EdSCAN_nNF.ValueVariant :=                      QrPrmsEmpNFeSCAN_nNF.Value;
    EdinfRespTec_xContato.ValueVariant :=           QrPrmsEmpNFeinfRespTec_xContato.Value;
    EdinfRespTec_CNPJ.ValueVariant :=               QrPrmsEmpNFeinfRespTec_CNPJ.Value;
    EdinfRespTec_fone.ValueVariant :=               QrPrmsEmpNFeinfRespTec_fone.Value;
    EdinfRespTec_email.ValueVariant :=              QrPrmsEmpNFeinfRespTec_email.Value;
    EdinfRespTec_idCSRT.ValueVariant :=             QrPrmsEmpNFeinfRespTec_idCSRT.Value;
    EdinfRespTec_CSRT.ValueVariant :=               QrPrmsEmpNFeinfRespTec_CSRT.Value;
    CkinfRespTec_Usa.Checked :=                     Geral.IntToBool(QrPrmsEmpNFeinfRespTec_Usa.Value);
    EdUF_WebServ.ValueVariant :=                    QrPrmsEmpNFeUF_WebServ.Value;
    EdSiglaCustm.ValueVariant :=                    QrPrmsEmpNFeSiglaCustm.Value;
    EdUF_Servico.ValueVariant :=                    QrPrmsEmpNFeUF_Servico.Value;
    CkInfoPerCuz.Checked :=                         Geral.IntToBool(QrPrmsEmpNFeInfoPerCuz.Value);
    EdNFeSerNum.ValueVariant :=                     QrPrmsEmpNFeNFeSerNum.Value;
    TPNFeSerVal.Date :=                             QrPrmsEmpNFeNFeSerVal.Value;
    EdNFeSerAvi.ValueVariant :=                     QrPrmsEmpNFeNFeSerAvi.Value;
    EdUF_MDeMDe.ValueVariant :=                     QrPrmsEmpNFeUF_MDeMDe.Value;
    EdUF_MDeDes.ValueVariant :=                     QrPrmsEmpNFeUF_MDeDes.Value;
    EdUF_MDeNFe.ValueVariant :=                     QrPrmsEmpNFeUF_MDeNFe.Value;
    EdNFeUF_EPEC.ValueVariant :=                    QrPrmsEmpNFeNFeUF_EPEC.Value;
    EdDirEnvLot.ValueVariant :=                     QrPrmsEmpNFeDirEnvLot.Value;
    EdDirRec.ValueVariant :=                        QrPrmsEmpNFeDirRec.Value;
    EdDirProRec.ValueVariant :=                     QrPrmsEmpNFeDirProRec.Value;
    EdDirNFeAss.ValueVariant :=                     QrPrmsEmpNFeDirNFeAss.Value;
    EdDirNFeGer.ValueVariant :=                     QrPrmsEmpNFeDirNFeGer.Value;
    EdDirDen.ValueVariant :=                        QrPrmsEmpNFeDirDen.Value;
    EdDirPedRec.ValueVariant :=                     QrPrmsEmpNFeDirPedRec.Value;
    EdDirPedSta.ValueVariant :=                     QrPrmsEmpNFeDirPedSta.Value;
    EdDirSta.ValueVariant :=                        QrPrmsEmpNFeDirSta.Value;
    EdDirPedCan.ValueVariant :=                     QrPrmsEmpNFeDirPedCan.Value;
    EdDirCan.ValueVariant :=                        QrPrmsEmpNFeDirCan.Value;
    EdDirPedInu.ValueVariant :=                     QrPrmsEmpNFeDirPedInu.Value;
    EdDirInu.ValueVariant :=                        QrPrmsEmpNFeDirInu.Value;
    EdDirPedSit.ValueVariant :=                     QrPrmsEmpNFeDirPedSit.Value;
    EdDirSit.ValueVariant :=                        QrPrmsEmpNFeDirSit.Value;
    EdDirEveEnvLot.ValueVariant :=                  QrPrmsEmpNFeDirEveEnvLot.Value;
    EdDirEveRetLot.ValueVariant :=                  QrPrmsEmpNFeDirEveRetLot.Value;
    EdDirEvePedCCe.ValueVariant :=                  QrPrmsEmpNFeDirEvePedCCe.Value;
    EdDirEveRetCCe.ValueVariant :=                  QrPrmsEmpNFeDirEveRetCCe.Value;
    EdDirEvePedCan.ValueVariant :=                  QrPrmsEmpNFeDirEvePedCan.Value;
    EdDirEveRetCan.ValueVariant :=                  QrPrmsEmpNFeDirEveRetCan.Value;
    EdDirEvePedEPEC.ValueVariant :=                 QrPrmsEmpNFeDirEvePedEPEC.Value;
    EdDirEveRetEPEC.ValueVariant :=                 QrPrmsEmpNFeDirEveRetEPEC.Value;
    EdDirEveProcCCe.ValueVariant :=                 QrPrmsEmpNFeDirEveProcCCe.Value;
    EdDirRetNfeDes.ValueVariant :=                  QrPrmsEmpNFeDirRetNfeDes.Value;
    EdDirEvePedMDe.ValueVariant :=                  QrPrmsEmpNFeDirEvePedMDe.Value;
    EdDirEveRetMDe.ValueVariant :=                  QrPrmsEmpNFeDirEveRetMDe.Value;
    EdDirDowNFeDes.ValueVariant :=                  QrPrmsEmpNFeDirDowNFeDes.Value;
    EdDirDowNFeNFe.ValueVariant :=                  QrPrmsEmpNFeDirDowNFeNFe.Value;
    EdDirDistDFeInt.ValueVariant :=                 QrPrmsEmpNFeDirDistDFeInt.Value;
    EdDirRetDistDFeInt.ValueVariant :=              QrPrmsEmpNFeDirRetDistDFeInt.Value;
    EdDirDowNFeCnf.ValueVariant :=                  QrPrmsEmpNFeDirDowNFeCnf.Value;
    Edversao.ValueVariant :=                        QrPrmsEmpNFeversao.Value;
    Edide_mod.ValueVariant :=                       QrPrmsEmpNFeide_mod.Value;
    RGide_tpAmb.ItemIndex :=                        QrPrmsEmpNFeide_tpAmb.Value;
    EdDirSchema.ValueVariant :=                     QrPrmsEmpNFeDirSchema.Value;
    CkNT2018_05v120.Checked :=                      Geral.IntToBool(QrPrmsEmpNFeNT2018_05v120.Value);
    RGAppCode.ItemIndex :=                          QrPrmsEmpNFeAppCode.Value;
    RGAssDigMode.ItemIndex :=                       QrPrmsEmpNFeAssDigMode.Value;
    EdMyEmailNFe.ValueVariant :=                    QrPrmsEmpNFeMyEmailNFe.Value;
    EdNFeInfCpl.ValueVariant :=                     QrPrmsEmpNFeNFeInfCpl.Value;
    RGCRT.ItemIndex :=                              QrPrmsEmpNFeCRT.Value;
    EdCSOSN.ValueVariant :=                         QrPrmsEmpNFeCSOSN.Value;
    EdpCredSNAlq.ValueVariant :=                    QrPrmsEmpNFepCredSNAlq.Value;
    EdNFeVerStaSer.ValueVariant :=                  QrPrmsEmpNFeNFeVerStaSer.Value;
    EdNFeVerEnvLot.ValueVariant :=                  QrPrmsEmpNFeNFeVerEnvLot.Value;
    EdNFeVerConLot.ValueVariant :=                  QrPrmsEmpNFeNFeVerConLot.Value;
    EdNFeVerCanNFe.ValueVariant :=                  QrPrmsEmpNFeNFeVerCanNFe.Value;
    EdNFeVerInuNum.ValueVariant :=                  QrPrmsEmpNFeNFeVerInuNum.Value;
    EdNFeVerConNFe.ValueVariant :=                  QrPrmsEmpNFeNFeVerConNFe.Value;
    EdNFeVerLotEve.ValueVariant :=                  QrPrmsEmpNFeNFeVerLotEve.Value;
    EdNFeVerConsCad.ValueVariant :=                 QrPrmsEmpNFeNFeVerConsCad.Value;
    EdNFeVerDistDFeInt.ValueVariant :=              QrPrmsEmpNFeNFeVerDistDFeInt.Value;
    EdNFeVerDowNFe.ValueVariant :=                  QrPrmsEmpNFeNFeVerDowNFe.Value;
    RGide_tpImp.ItemIndex :=                        QrPrmsEmpNFeide_tpImp.Value;
    RGNFeItsLin.ItemIndex :=                        QrPrmsEmpNFeNFeItsLin.Value;
    EdNFeFTRazao.ValueVariant :=                    QrPrmsEmpNFeNFeFTRazao.Value;
    EdNFeFTEnder.ValueVariant :=                    QrPrmsEmpNFeNFeFTEnder.Value;
    EdNFeFTFones.ValueVariant :=                    QrPrmsEmpNFeNFeFTFones.Value;
    CkNFeMaiusc.Checked :=                          Geral.IntToBool(QrPrmsEmpNFeNFeMaiusc.Value);
    EdNFeShowURL.ValueVariant :=                    QrPrmsEmpNFeNFeShowURL.Value;
    EdPathLogoNF.ValueVariant :=                    QrPrmsEmpNFePathLogoNF.Value;
    EdDirDANFEs.ValueVariant :=                     QrPrmsEmpNFeDirDANFEs.Value;
    RG_NFe_indFinalCpl.ItemIndex :=                 QrPrmsEmpNFeNFe_indFinalCpl.Value;
    RGNoDANFEMail.ItemIndex :=                      QrPrmsEmpNFeNoDANFEMail.Value;


    EdCSC.ValueVariant :=                           QrParamsEmpCSC.Value;
    EdCSCpos.ValueVariant :=                        QrParamsEmpCSCpos.Value;

    EdTipoPrintNFCe.ValueVariant :=                 QrPrmsEmpNFeTipoPrintNFCe.Value;
    EdNomePrintNFCe.ValueVariant :=                 QrPrmsEmpNFeNomePrintNFCe.Value;
    EdIPPrintNFCe.ValueVariant :=                   QrPrmsEmpNFeIPPrintNFCe.Value;
    CkAutoCutNFCe.Checked :=                        Geral.IntToBool(QrPrmsEmpNFeAutoCutNFCe.Value);
    //
    EdCertDigPfxCam.Text :=                         QrPrmsEmpNFeCertDigPfxCam.Value;
    EdCertDigPfxPwd.Text :=                         QrPrmsEmpNFeCertDigPfxPwd.Value;
    RGCertDigPfxWay.ItemIndex :=                    QrPrmsEmpNFeCertDigPfxWay.Value;

(*
    EdFisContTel.ValueVariant :=                    QrParamsEmpFisContTel.Value;
    EdFisContNom.ValueVariant :=                    QrParamsEmpFisContNom.Value;
    EdFisFax.ValueVariant :=                        QrParamsEmpFisFax.Value;
    EdReg10.ValueVariant :=                         QrParamsEmpReg10.Value;
    EdReg11.ValueVariant :=                         QrParamsEmpReg11.Value;
    EdReg50.ValueVariant :=                         QrParamsEmpReg50.Value;
    EdReg51.ValueVariant :=                         QrParamsEmpReg51.Value;
    EdReg53.ValueVariant :=                         QrParamsEmpReg53.Value;
    EdReg54.ValueVariant :=                         QrParamsEmpReg54.Value;
    EdReg55.ValueVariant :=                         QrParamsEmpReg55.Value;
    EdReg56.ValueVariant :=                         QrParamsEmpReg56.Value;
    EdReg57.ValueVariant :=                         QrParamsEmpReg57.Value;
    EdReg60M.ValueVariant :=                        QrParamsEmpReg60M.Value;
    EdReg61.ValueVariant :=                         QrParamsEmpReg61.Value;
    EdReg70.ValueVariant :=                         QrParamsEmpReg70.Value;
    EdReg71.ValueVariant :=                         QrParamsEmpReg71.Value;
    EdReg74.ValueVariant :=                         QrParamsEmpReg74.Value;
    EdReg75.ValueVariant :=                         QrParamsEmpReg75.Value;
    EdReg76.ValueVariant :=                         QrParamsEmpReg76.Value;
    EdReg77.ValueVariant :=                         QrParamsEmpReg77.Value;
    EdReg85.ValueVariant :=                         QrParamsEmpReg85.Value;
    EdReg86.ValueVariant :=                         QrParamsEmpReg86.Value;
    EdReg90.ValueVariant :=                         QrParamsEmpReg90.Value;
    EdReg60A.ValueVariant :=                        QrParamsEmpReg60A.Value;
    EdReg60I.ValueVariant :=                        QrParamsEmpReg60I.Value;
    EdReg60D.ValueVariant :=                        QrParamsEmpReg60D.Value;
    EdReg60R.ValueVariant :=                        QrParamsEmpReg60R.Value;
    EdReg61R.ValueVariant :=                        QrParamsEmpReg61R.Value;
    EdSINTEGRA_Path.ValueVariant :=                 QrParamsEmpSINTEGRA_Path.Value;
*)

end;

procedure TFmParamsEmp.PreencheCamposAlteraOpcoesNFe();
begin
  EdPreMailAut.ValueVariant          := QrPrmsEmpNFePreMailAut.Value;
  CBPreMailAut.KeyValue              := QrPrmsEmpNFePreMailAut.Value;
  EdPreMailCan.ValueVariant          := QrPrmsEmpNFePreMailCan.Value;
  CBPreMailCan.KeyValue              := QrPrmsEmpNFePreMailCan.Value;
  EdPreMailEveCCe.ValueVariant       := QrPrmsEmpNFePreMailEveCCe.Value;
  CBPreMailEveCCe.KeyValue           := QrPrmsEmpNFePreMailEveCCe.Value;
  EdDirNFeProt.ValueVariant          := QrPrmsEmpNFeDirNFeProt.Value;
  EdDirNFeRWeb.ValueVariant          := QrPrmsEmpNFeDirNFeRWeb.Value;
  EdSCAN_Ser.ValueVariant            := QrPrmsEmpNFeSCAN_Ser.Value;
  EdSCAN_nNF.ValueVariant            := QrPrmsEmpNFeSCAN_nNF.Value;
  EdinfRespTec_xContato.ValueVariant := QrPrmsEmpNFeinfRespTec_xContato.Value;
  EdinfRespTec_CNPJ.ValueVariant     := QrPrmsEmpNFeinfRespTec_CNPJ.Value;
  EdinfRespTec_fone.ValueVariant     := QrPrmsEmpNFeinfRespTec_fone.Value;
  EdinfRespTec_email.ValueVariant    := QrPrmsEmpNFeinfRespTec_email.Value;
  EdinfRespTec_idCSRT.ValueVariant   := QrPrmsEmpNFeinfRespTec_idCSRT.Value;
  EdinfRespTec_CSRT.ValueVariant     := QrPrmsEmpNFeinfRespTec_CSRT.Value;
  CkinfRespTec_Usa.Checked           := Geral.IntToBool(QrPrmsEmpNFeinfRespTec_Usa.Value);
  EdUF_WebServ.ValueVariant          := QrPrmsEmpNFeUF_WebServ.Value;
  EdSiglaCustm.ValueVariant          := QrPrmsEmpNFeSiglaCustm.Value;
  EdUF_Servico.ValueVariant          := QrPrmsEmpNFeUF_Servico.Value;
  CkSimplesFed.Checked               := Geral.IntToBool(QrPrmsEmpNFeSimplesFed.Value);
  CkInfoPerCuz.Checked               := Geral.IntToBool(QrPrmsEmpNFeInfoPerCuz.Value);
  EdNFeSerNum.ValueVariant           := QrPrmsEmpNFeNFeSerNum.Value;
  TPNFeSerVal.Date                   := QrPrmsEmpNFeNFeSerVal.Value;
  EdNFeSerAvi.ValueVariant           := QrPrmsEmpNFeNFeSerAvi.Value;
  EdUF_MDeMDe.ValueVariant           := QrPrmsEmpNFeUF_MDeMDe.Value;
  EdUF_MDeDes.ValueVariant           := QrPrmsEmpNFeUF_MDeDes.Value;
  EdUF_MDeNFe.ValueVariant           := QrPrmsEmpNFeUF_MDeNFe.Value;
  EdNFeUF_EPEC.ValueVariant          := QrPrmsEmpNFeNFeUF_EPEC.Value;
  EdDirEnvLot.ValueVariant           := QrPrmsEmpNFeDirEnvLot.Value;
  EdDirRec.ValueVariant              := QrPrmsEmpNFeDirRec.Value;
  EdDirProRec.ValueVariant           := QrPrmsEmpNFeDirProRec.Value;
  EdDirNFeAss.ValueVariant           := QrPrmsEmpNFeDirNFeAss.Value;
  EdDirNFeGer.ValueVariant           := QrPrmsEmpNFeDirNFeGer.Value;
  EdDirDen.ValueVariant              := QrPrmsEmpNFeDirDen.Value;
  EdDirPedRec.ValueVariant           := QrPrmsEmpNFeDirPedRec.Value;
  EdDirPedSta.ValueVariant           := QrPrmsEmpNFeDirPedSta.Value;
  EdDirSta.ValueVariant              := QrPrmsEmpNFeDirSta.Value;
  EdDirPedCan.ValueVariant           := QrPrmsEmpNFeDirPedCan.Value;
  EdDirCan.ValueVariant              := QrPrmsEmpNFeDirCan.Value;
  EdDirPedInu.ValueVariant           := QrPrmsEmpNFeDirPedInu.Value;
  EdDirInu.ValueVariant              := QrPrmsEmpNFeDirInu.Value;
  EdDirPedSit.ValueVariant           := QrPrmsEmpNFeDirPedSit.Value;
  EdDirSit.ValueVariant              := QrPrmsEmpNFeDirSit.Value;
  EdDirEveEnvLot.ValueVariant        := QrPrmsEmpNFeDirEveEnvLot.Value;
  EdDirEveRetLot.ValueVariant        := QrPrmsEmpNFeDirEveRetLot.Value;
  EdDirEvePedCCe.ValueVariant        := QrPrmsEmpNFeDirEvePedCCe.Value;
  EdDirEveRetCCe.ValueVariant        := QrPrmsEmpNFeDirEveRetCCe.Value;
  EdDirEvePedCan.ValueVariant        := QrPrmsEmpNFeDirEvePedCan.Value;
  EdDirEveRetCan.ValueVariant        := QrPrmsEmpNFeDirEveRetCan.Value;
  EdDirEveProcCCe.ValueVariant       := QrPrmsEmpNFeDirEveProcCCe.Value;
  EdDirEvePedEPEC.ValueVariant       := QrPrmsEmpNFeDirEvePedEPEC.Value;
  EdDirEveRetEPEC.ValueVariant       := QrPrmsEmpNFeDirEveRetEPEC.Value;

  EdDirRetNfeDes.ValueVariant        := QrPrmsEmpNFeDirRetNfeDes.Value;
  EdDirEvePedMDe.ValueVariant        := QrPrmsEmpNFeDirEvePedMDe.Value;
  EdDirEveRetMDe.ValueVariant        := QrPrmsEmpNFeDirEveRetMDe.Value;
  EdDirDowNFeDes.ValueVariant        := QrPrmsEmpNFeDirDowNFeDes.Value;
  EdDirDowNFeNFe.ValueVariant        := QrPrmsEmpNFeDirDowNFeNFe.Value;
  EdDirDistDFeInt.ValueVariant       := QrPrmsEmpNFeDirDistDFeInt.Value;
  EdDirRetDistDFeInt.ValueVariant    := QrPrmsEmpNFeDirRetDistDFeInt.Value;
  EdDirDowNFeCnf.ValueVariant        := QrPrmsEmpNFeDirDowNFeCnf.Value;

  Edversao.ValueVariant              := QrPrmsEmpNFeversao.Value;
  Edide_mod.ValueVariant             := QrPrmsEmpNFeide_mod.Value;
  RGide_tpAmb.ItemIndex              := QrPrmsEmpNFeide_tpAmb.Value;
  EdDirSchema.ValueVariant           := QrPrmsEmpNFeDirSchema.Value;
  CkNT2018_05v120.Checked            := Geral.IntToBool(QrPrmsEmpNFeNT2018_05v120.Value);
  RGAppCode.ItemIndex                := QrPrmsEmpNFeAppCode.Value;
  RGAssDigMode.ItemIndex             := QrPrmsEmpNFeAssDigMode.Value;
  EdMyEmailNFe.ValueVariant          := QrPrmsEmpNFeMyEmailNFe.Value;
  EdNFeInfCpl.ValueVariant           := QrPrmsEmpNFeNFeInfCpl.Value;
  CBNFeInfCpl.KeyValue               := QrPrmsEmpNFeNFeInfCpl.Value;
  RGCRT.ItemIndex                    := QrPrmsEmpNFeCRT.Value;
  EdCSOSN.ValueVariant               := QrPrmsEmpNFeCSOSN.Value;
  EdpCredSNAlq.ValueVariant          := QrPrmsEmpNFepCredSNAlq.Value;
  EdpCredSNAlq.ValueVariant          := QrPrmsEmpNFepCredSNAlq.Value;
  EdNFeVerStaSer.ValueVariant        := QrPrmsEmpNFeNFeVerStaSer.Value;
  EdNFeVerEnvLot.ValueVariant        := QrPrmsEmpNFeNFeVerEnvLot.Value;
  EdNFeVerConLot.ValueVariant        := QrPrmsEmpNFeNFeVerConLot.Value;
  EdNFeVerCanNFe.ValueVariant        := QrPrmsEmpNFeNFeVerCanNFe.Value;
  EdNFeVerInuNum.ValueVariant        := QrPrmsEmpNFeNFeVerInuNum.Value;
  EdNFeVerConNFe.ValueVariant        := QrPrmsEmpNFeNFeVerConNFe.Value;
  EdNFeVerLotEve.ValueVariant        := QrPrmsEmpNFeNFeVerLotEve.Value;
  EdNFeVerConsCad.ValueVariant       := QrPrmsEmpNFeNFeVerConsCad.Value;
  EdNFeVerDistDFeInt.ValueVariant    := QrPrmsEmpNFeNFeVerDistDFeInt.Value;
  EdNFeVerDowNFe.ValueVariant        := QrPrmsEmpNFeNFeVerDowNFe.Value;
  RGide_tpImp.ItemIndex              := QrPrmsEmpNFeide_tpImp.Value;
  RGNFeItsLin.ItemIndex              := QrPrmsEmpNFeNFeItsLin.Value;
  EdNFeFTRazao.ValueVariant          := QrPrmsEmpNFeNFeFTRazao.Value;
  EdNFeFTEnder.ValueVariant          := QrPrmsEmpNFeNFeFTEnder.Value;
  EdNFeFTFones.ValueVariant          := QrPrmsEmpNFeNFeFTFones.Value;
  CkNFeMaiusc.Checked                := Geral.IntToBool(QrPrmsEmpNFeNFeMaiusc.Value);
  EdNFeShowURL.ValueVariant          := QrPrmsEmpNFeNFeShowURL.Value;
  EdPathLogoNF.ValueVariant          := QrPrmsEmpNFePathLogoNF.Value;
  EdDirDANFEs.ValueVariant           := QrPrmsEmpNFeDirDANFEs.Value;
  RG_NFe_indFinalCpl.ItemIndex       := QrPrmsEmpNFeNFe_indFinalCpl.Value;
  RGNoDANFEMail.ItemIndex            := QrPrmsEmpNFeNoDANFEMail.Value;
  RGNFeNT2013_003LTT.ItemIndex       := QrPrmsEmpNFeNFeNT2013_003LTT.Value;
  RGNFeUsoCEST.ItemIndex             := QrPrmsEmpNFeNFeUsoCEST.Value;
  //
  EdCertDigPfxCam.ValueVariant       := QrPrmsEmpNFeCertDigPfxCam.Value;
  EdCertDigPfxPwd.ValueVariant       := QrPrmsEmpNFeCertDigPfxPwd.Value;
  RGCertDigPfxWay.ItemIndex          := QrPrmsEmpNFeCertDigPfxWay.Value;
  //
  EdUC_ICMS_CST_B.ValueVariant       := QrPrmsEmpNFeUC_ICMS_CST_B.Value;
  EdUC_IPI_CST.ValueVariant          := QrPrmsEmpNFeUC_IPI_CST.Value;
  EdUC_PIS_CST.ValueVariant          := QrPrmsEmpNFeUC_PIS_CST.Value;
  EdUC_COFINS_CST.ValueVariant       := QrPrmsEmpNFeUC_COFINS_CST.Value;
end;

procedure TFmParamsEmp.CBAssocModNFEnter(Sender: TObject);
begin
// ReopenImprime();
end;

procedure TFmParamsEmp.CkinfRespTec_UsaClick(Sender: TObject);
begin
  PninfRespTec_Usa.Visible := CkinfRespTec_Usa.Checked;
end;

procedure TFmParamsEmp.CkTZD_UTC_AutoClick(Sender: TObject);
var
  DtaHora: TDateTime;
  Usuario, Termo: Integer;
begin
  ConfiguraCamposTZ(CkTZD_UTC_Auto.Checked);
  //
  if (CkTZD_UTC_Auto.Checked) and (FMostraTermo = True) then
  begin
    if not UWAceites.AceitaTermos(istTZD_UTC_Auto, Termo, Usuario, DtaHora) then
    begin
      FTZD_UTC_Auto_UsuarioAceite  := 0;
      FTZD_UTC_Auto_TermoAceite    := 0;
      FTZD_UTC_Auto_DataHoraAceite := 0;
      //
      CkTZD_UTC_Auto.Checked := False;
    end else
    begin
      FTZD_UTC_Auto_UsuarioAceite  := Usuario;
      FTZD_UTC_Auto_TermoAceite    := Termo;
      FTZD_UTC_Auto_DataHoraAceite := DtaHora;
      //
      EdTZD_UTC.ValueVariant := DModG.ObtemFusoHorarioServidor;
    end;
  end;
end;

procedure TFmParamsEmp.EdCodDctfCofinsRedefinido(Sender: TObject);
begin
  EdCodDctfCofins.ValueVariant := Geral.SoNumero_TT(EdCodDctfCofins.ValueVariant);
end;

procedure TFmParamsEmp.ConfiguraCamposTZ(Auto: Boolean);
begin
  if Auto then
  begin
    EdTZD_UTC.Enabled   := False;
    SBTZD_UTC.Enabled   := False;
    TPhVeraoAsk.Enabled := False;
    TPhVeraoIni.Enabled := False;
    TPhVeraoFim.Enabled := False;
  end else
  begin
    EdTZD_UTC.Enabled   := True;
    SBTZD_UTC.Enabled   := True;
    TPhVeraoAsk.Enabled := True;
    TPhVeraoIni.Enabled := True;
    TPhVeraoFim.Enabled := True;
  end;
  if (QrParamsEmp.State <> dsInactive) and (QrParamsEmp.RecordCount > 0) then
  begin
    FTZD_UTC_Auto_UsuarioAceite  := 0;
    FTZD_UTC_Auto_TermoAceite    := 0;
    FTZD_UTC_Auto_DataHoraAceite := 0;
  end else
  begin
    FTZD_UTC_Auto_UsuarioAceite  := QrPrmsEmpMisTZD_UTC_Auto_UsuarioAceite.Value;
    FTZD_UTC_Auto_TermoAceite    := QrPrmsEmpMisTZD_UTC_Auto_TermoAceite.Value;
    FTZD_UTC_Auto_DataHoraAceite := QrPrmsEmpMisTZD_UTC_Auto_DataHoraAceite.Value;
  end;
end;

procedure TFmParamsEmp.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmParamsEmp.QueryPrincipalAfterOpen;
begin
end;

procedure TFmParamsEmp.MostraFmContas(EdConta: TdmkEditCB;
  CBConta: TdmkDBLookupComboBox; Query: TmySQLQuery);
begin
  {$IfNDef NO_FINANCEIRO}
  VAR_CADASTRO := 0;
  if DBCheck.CriaFm(TFmContas, FmContas, afmoNegarComAviso) then
  begin
    FmContas.ShowModal;
    FmContas.Destroy;
    if VAR_CADASTRO > 0 then
    begin
      UMyMod.AbreQuery(QrCtaProdVen, Dmod.MyDB);
      UMyMod.AbreQuery(QrCtaProdCom, Dmod.MyDB);
      UMyMod.AbreQuery(QrCtaServico, Dmod.MyDB);
      UMyMod.AbreQuery(QrCtaServicoPg, Dmod.MyDB);
      //
      UMyMod.SetaCodigoPesquisado(EdConta, CBConta, Query, VAR_CADASTRO);
      EdConta.SetFocus;
    end;
  end;
  {$EndIf}
end;

procedure TFmParamsEmp.MostraFmEntiTipCto(EdEntTipCto: TdmkEditCB;
  CBEntTipCto: TdmkDBLookupComboBox; Query: TmySQLQuery);
begin
  {$IfNDef NoEntiAux}
  VAR_CADASTRO := 0;
  if DBCheck.CriaFm(TFmEntiTipCto, FmEntiTipCto, afmoNegarComAviso) then
  begin
    FmEntiTipCto.ShowModal;
    FmEntiTipCto.Destroy;
    if VAR_CADASTRO > 0 then
    begin
      UMyMod.AbreQuery(QrEntiTipCto, Dmod.MyDB);
      UMyMod.AbreQuery(QrEntiTipCt1, Dmod.MyDB);
      UMyMod.AbreQuery(QrEntiTipCt2, Dmod.MyDB);
      //
      UMyMod.SetaCodUsuDeCodigo(EdEntTipCto, CBEntTipCto, Query, VAR_CADASTRO);
      EdEntTipCto.SetFocus;
    end;
  end;
  {$EndIf}
end;

procedure TFmParamsEmp.Removecertificadodorepositriodosistemaoperacional1Click(
  Sender: TObject);
begin
  GerenciaCertificadoDigital(stDel);
end;

procedure TFmParamsEmp.ReopenImprime();
var
  AssocModNF, Associada, Empresa: Integer;
begin
  Associada := Geral.IMV(EdAssociada.Text);
  if Associada > 0 then
    Empresa := QrFiliaisCodigo.Value
  else Empresa := 0;
  AssocModNF := Geral.IMV(EdAssocModNF.Text);
  QrImprime.Close;
  if Empresa <> 0 then
  begin
    QrImprime.Params[0].AsInteger := Empresa;
    UMyMod.AbreQuery(QrImprime, Dmod.MyDB, 'TFmParamsEmp.ReopenImprime()');
    //
    if (AssocModNF = 0) or
    (not QrImprime.Locate('Codigo', AssocModNF, [])) then
    begin
      EdAssocModNF.ValueVariant := 0;
      CBAssocModNF.KeyValue     := Null;
    end;
  end;
end;

procedure TFmParamsEmp.ReopenParamsCTe(Controle: Integer);
begin
{$IfDef ComCTe_0000}
  UnDmkDAC_PF.AbreMySQLQuery0(QrParamsCTe, Dmod.MyDB, [
  'SELECT pnf.*, ELT(pnf.IncSeqAuto+1, "Manual", ',
  '"Autom�tico", "? ? ?") NO_IncSeqAuto ',
  'FROM paramscte pnf ',
  'WHERE pnf.Codigo=' + Geral.FF0(QrParamsEmpCodigo.Value),
  '']);
  QrParamsCTe.Locate('Controle', Controle, []);
{$EndIf}
end;

procedure TFmParamsEmp.ReopenParamsMDFe(Controle: Integer);
begin
{$IfDef ComMDFe_0000}
  UnDmkDAC_PF.AbreMySQLQuery0(QrParamsMDFe, Dmod.MyDB, [
  'SELECT pnf.*, ELT(pnf.IncSeqAuto+1, "Manual", ',
  '"Autom�tico", "? ? ?") NO_IncSeqAuto ',
  'FROM paramsmdfe pnf ',
  'WHERE pnf.Codigo=' + Geral.FF0(QrParamsEmpCodigo.Value),
  '']);
  QrParamsMDFe.Locate('Controle', Controle, []);
{$EndIf}
end;

procedure TFmParamsEmp.ReopenParamsNFs(Controle: Integer);
begin
  QrParamsNFs.Close;
  QrParamsNFs.Params[0].AsInteger := QrParamsEmpCodigo.Value;
  UMyMod.AbreQuery(QrParamsNFs, Dmod.MyDB, 'TFmParamsEmp.ReopenParamsNFs()');
  QrParamsNFs.Locate('Controle', Controle, []);
end;

procedure TFmParamsEmp.ReopenParamsNFCs(Controle: Integer);
begin
  QrParamsNFCs.Close;
  QrParamsNFCs.Params[0].AsInteger := QrParamsEmpCodigo.Value;
  UMyMod.AbreQuery(QrParamsNFCs, Dmod.MyDB, 'TFmParamsEmp.ReopenParamsNFCs()');
  QrParamsNFCs.Locate('Controle', Controle, []);
end;

procedure TFmParamsEmp.RGCRTClick(Sender: TObject);
begin
  {$IfNDef NO_FINANCEIRO}
  MeCSOSN_Edit.Text := UFinanceiro.CSOSN_Get(RGCRT.ItemIndex, EdCSOSN.ValueVariant);
  {$EndIf}
end;

procedure TFmParamsEmp.RGRegimPisCofinsClick(Sender: TObject);
begin
  if FCriado then
  begin
    if RGRegimPisCofins.ItemIndex = 2 then
    begin
      RGRegimCumulativ.Enabled := True;
    end else
    begin
      RGRegimCumulativ.ItemIndex := 0;
      RGRegimCumulativ.Enabled := False;
    end;
  end;
end;

procedure TFmParamsEmp.DBEdit101Change(Sender: TObject);
begin
  EdTXT_NFSeTipoRps2.Text := DModG.DefineTextoTipoRPS(QrParamsEmpNFSeTipoRps.Value);
end;

procedure TFmParamsEmp.DefineONomeDoForm;
begin
end;

function TFmParamsEmp.DefinirCaminhoSeVazio_CNPJ(dmkEdit: TdmkEdit;
  PastaRaiz, PastaFinal: String): Boolean;
var
  Empresa, Ini, Fim: String;
  P: Integer;
begin
  if Trim(dmkEdit.Text) = '' then
  begin
    Result := True;
    //
    if ImgTipo.SQLType = stIns then
    begin
      Geral.MB_Aviso('Esta a��o s� pode ser executada na altera��o do registro!');
      Exit;
    end;
    if Length(QrParamsEmpCNPJCPF_FILIAL.Value) > 1 then
      Empresa := 'CNPJ_' + Geral.SoNumero_TT(QrParamsEmpCNPJCPF_FILIAL.Value)
    else
    begin
      P := Pos(' ', QrParamsEmpNOMEFILIAL.Value);
      if P > 0 then
        Empresa := Copy(QrParamsEmpNOMEFILIAL.Value, 1, P)
      else
        Empresa := Copy(QrParamsEmpNOMEFILIAL.Value, 1);
      if Empresa = '' then
        Empresa := 'xxxxx';
      Empresa := dmkPF.ValidaPalavra(Empresa);
      Empresa := Trim(Copy(Empresa, 1, 30));
    end;
    //
    dmkPF.AjustaCaracterFinal('\', PastaRaiz, Ini);
    dmkPF.AjustaCaracterInicial('\', PastaFinal, Fim);
    dmkPF.AjustaCaracterFinal('\', Fim, Fim);
    dmkEdit.Text := Ini + Empresa + Fim;
  end else
    Result := False;
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmParamsEmp.SBDirDenClick(Sender: TObject);
begin
  MyObjects.DefineDiretorio(Self, EdDirDen);
end;

procedure TFmParamsEmp.SbDirDistDFeIntClick(Sender: TObject);
begin
  MyObjects.DefineDiretorio(Self, EdDirDistDFeInt);
end;

procedure TFmParamsEmp.SbDirDowNFeCnfClick(Sender: TObject);
begin
  MyObjects.DefineDiretorio(Self, EdDirDowNFeCnf);
end;

procedure TFmParamsEmp.SbDirDowNFeDesClick(Sender: TObject);
begin
  MyObjects.DefineDiretorio(Self, EdDirDowNFeDes);
end;

procedure TFmParamsEmp.SbDirDowNFeNFeClick(Sender: TObject);
begin
  MyObjects.DefineDiretorio(Self, EdDirDowNFeNFe);
end;

procedure TFmParamsEmp.SpeedButton11Click(Sender: TObject);
var
  IniDir, Arquivo: String;
begin
  IniDir := ExtractFileDir(EdPathLogoNF.Text);
  Arquivo := ExtractFileName(EdPathLogoNF.Text);
  if MyObjects.FileOpenDialog(FmParamsEmp, IniDir, Arquivo,
  'Selecione o arquivo do Logo', '', [], Arquivo) then
    EdPathLogoNF.Text := Arquivo;
end;

procedure TFmParamsEmp.SbNFeSerNumClick(Sender: TObject);
begin
  // ini 2023-12-21
//{$IfNDef SemNFe_or_NFSe0000}
{$IfNDef SemCertDig}
  // fim 2023-12-21
  if DBCheck.CriaFm(TFmCapicomListas, FmCapicomListas, afmoSoBoss) then
  begin
    FmCapicomListas.ShowModal;
    if FmCapicomListas.FSelected then
    begin
      EdNFeSerNum.Text := FmCapicomListas.FSerialNumber;
      TPNFeSerVal.Date := FmCapicomListas.FValidToDate;
      if EdNFeSerAvi.ValueVariant < 30 then
        EdNFeSerAvi.ValueVariant := 30;
    end;
    FmCapicomListas.Destroy;
{$IfNDef SemACBr}
    DmkACBr_ParamsEmp.FormCreate_ACBr(ACBrNFe0);
{$EndIf}
  end;
{$EndIf}
end;

procedure TFmParamsEmp.SpeedButton13Click(Sender: TObject);
begin
  MyObjects.DefineDiretorio(Self, EdDirDANFEs);
end;

procedure TFmParamsEmp.SpeedButton14Click(Sender: TObject);
begin
  MyObjects.DefineDiretorio(Self, EdDirNFeProt);
end;

procedure TFmParamsEmp.SpeedButton15Click(Sender: TObject);
begin
  MostraFmEntiTipCto(EdEntiTipCto, CBEntiTipCto, QrEntiTipCto);
end;

procedure TFmParamsEmp.SpeedButton16Click(Sender: TObject);
begin
  MyObjects.DefineDiretorio(Self, EdDirSchema);
end;

procedure TFmParamsEmp.SpeedButton17Click(Sender: TObject);
begin
  MyObjects.DefineDiretorio(Self, EdSPED_EFD_Path);
end;

procedure TFmParamsEmp.SpeedButton18Click(Sender: TObject);
begin
  MyObjects.DefineDiretorio(Self, EdDirNFeRWeb);
end;

procedure TFmParamsEmp.SpeedButton19Click(Sender: TObject);
begin
  MostraFmEntiTipCto(EdEntiTipCt1, CBEntiTipCt1, QrEntiTipCt1);
end;

procedure TFmParamsEmp.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmParamsEmp.SpeedButton20Click(Sender: TObject);
begin
  {$IfNDef SemNFe_0000}
  VAR_CADASTRO := 0;
  //
  if DBCheck.CriaFm(TFmNfeInfCpl, FmNfeInfCpl, afmoNegarComAviso) then
  begin
    FmNfeInfCpl.ShowModal;
    FmNfeInfCpl.Destroy;
  end;
  //
  if VAR_CADASTRO <> 0 then
  begin
    UMyMod.SetaCodigoPesquisado(EdNFeInfCpl, CBNFeInfCpl, QrNFeInfCpl, VAR_CADASTRO, 'Codigo');
    EdNFeInfCpl.SetFocus;
  end;
  {$EndIf}
end;

procedure TFmParamsEmp.SpeedButton21Click(Sender: TObject);
begin
  MyObjects.DefineDiretorio(Self, EdDirCTeProt);
end;

procedure TFmParamsEmp.SpeedButton24Click(Sender: TObject);
begin
  MyObjects.DefineDiretorio(Self, EdDirCTeRWeb);
end;

procedure TFmParamsEmp.SpeedButton27Click(Sender: TObject);
begin
    {$IfDef ComCTe_0000}
  if DBCheck.CriaFm(TFmCapicomListas, FmCapicomListas, afmoSoBoss) then
  begin
    FmCapicomListas.ShowModal;
    if FmCapicomListas.FSelected then
    begin
      EdCTeSerNum.Text := FmCapicomListas.FSerialNumber;
      TPCTeSerVal.Date := FmCapicomListas.FValidToDate;
      if EdCTeSerAvi.ValueVariant < 30 then
        EdCTeSerAvi.ValueVariant := 30;
    end;
    FmCapicomListas.Destroy;
  end;
    {$EndIf}
end;

procedure TFmParamsEmp.SpeedButton28Click(Sender: TObject);
begin
  MostraFmContas(EdCtaFretPrest, CBCtaFretPrest, QrCtaFretPrest);
end;

procedure TFmParamsEmp.SpeedButton29Click(Sender: TObject);
begin
  MyObjects.DefineDiretorio(Self, EdDirMDFeProt);
end;

procedure TFmParamsEmp.SBTZD_UTCClick(Sender: TObject);
begin
  dmkPF.TZD_UTC_InfoHelp();
end;

procedure TFmParamsEmp.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmParamsEmp.SpeedButton30Click(Sender: TObject);
begin
  MyObjects.DefineDiretorio(Self, EdDirMDFeRWeb);
end;

procedure TFmParamsEmp.SpeedButton33Click(Sender: TObject);
begin
    {$IfDef ComMDFe_0000}
  if DBCheck.CriaFm(TFmCapicomListas, FmCapicomListas, afmoSoBoss) then
  begin
    FmCapicomListas.ShowModal;
    if FmCapicomListas.FSelected then
    begin
      EdMDFeSerNum.Text := FmCapicomListas.FSerialNumber;
      TPMDFeSerVal.Date := FmCapicomListas.FValidToDate;
      if EdMDFeSerAvi.ValueVariant < 30 then
        EdMDFeSerAvi.ValueVariant := 30;
    end;
    FmCapicomListas.Destroy;
  end;
    {$EndIf}
end;

procedure TFmParamsEmp.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmParamsEmp.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmParamsEmp.SpeedButton50Click(Sender: TObject);
var
  Periodo: Integer;
begin
  Periodo := MyObjects.SelRadioGroup('Qual a periodicidade do IPI?',
  'Selecione a periodicidade do IPI: ',
  ['Mensal (mais comum)', 'Decendial (espec�ficos)'], -1);
  case Periodo of
    0:
    begin
      RGSPED_EFD_Peri_E100.ItemIndex := 5;
      RGSPED_EFD_Peri_E500.ItemIndex := 5;
      RGSPED_EFD_Peri_K100.ItemIndex := 5;
    end;
    1:
    begin
      RGSPED_EFD_Peri_E100.ItemIndex := 5;
      RGSPED_EFD_Peri_E500.ItemIndex := 3;
      RGSPED_EFD_Peri_K100.ItemIndex := 3;
    end;
  end;
end;

procedure TFmParamsEmp.SpeedButton51Click(Sender: TObject);
{$IfDef sSPED}
const
  Forca = True;
var
  Versao: String;
{$EndIf}
begin
{$IfDef sSPED}
  Versao := EfdIcmsIpi_Jan.VersaoTxtDeEfdIcmsIpi(Forca);
  if Versao <> '' then
    EdSPED_EFD_ICMS_IPI_VersaoGuia.Text := Versao;
{$EndIf}
end;

procedure TFmParamsEmp.SpeedButton52Click(Sender: TObject);
begin
  MostraFmEntiTipCto(EdCTeEntiTipCto, CBCTeEntiTipCto, QrCTeEntiTipCto);
end;

procedure TFmParamsEmp.SpeedButton53Click(Sender: TObject);
begin
  MostraFmEntiTipCto(EdCTeEntiTipCt1, CBCTeEntiTipCt1, QrCTeEntiTipCt1);
end;

procedure TFmParamsEmp.SpeedButton54Click(Sender: TObject);
begin
(*
  {$IfDef ComCTe_0000}
  VAR_CADASTRO := 0;
  //
  if DBCheck.CriaFm(TFmCTeInfCpl, FmCTeInfCpl, afmoNegarComAviso) then
  begin
    FmCTeInfCpl.ShowModal;
    FmCTeInfCpl.Destroy;
  end;
  //
  if VAR_CADASTRO <> 0 then
  begin
    UMyMod.SetaCodigoPesquisado(EdCTeInfCpl, CBCTeInfCpl, QrCTeInfCpl, VAR_CADASTRO, 'Codigo');
    EdCTeInfCpl.SetFocus;
  end;
  {$EndIf}
*)
end;

procedure TFmParamsEmp.SpeedButton55Click(Sender: TObject);
begin
  MyObjects.DefineDiretorio(Self, EdDirCTeSchema);
end;

procedure TFmParamsEmp.SpeedButton56Click(Sender: TObject);
var
  IniDir, Arquivo: String;
begin
  IniDir := ExtractFileDir(EdPathLogoCTe.Text);
  Arquivo := ExtractFileName(EdPathLogoCTe.Text);
  if MyObjects.FileOpenDialog(FmParamsEmp, IniDir, Arquivo,
  'Selecione o arquivo do Logo', '', [], Arquivo) then
    EdPathLogoCTe.Text := Arquivo;
end;

procedure TFmParamsEmp.SpeedButton57Click(Sender: TObject);
begin
  MyObjects.DefineDiretorio(Self, EdDirDACTEs);
end;

procedure TFmParamsEmp.SpeedButton5Click(Sender: TObject);
begin
  MyObjects.DefineArquivo1(Self, EdLogo3x1);
end;

procedure TFmParamsEmp.SpeedButton66Click(Sender: TObject);
begin
(*
  {$IfDef ComMDFe_0000}
  VAR_CADASTRO := 0;
  //
  if DBCheck.CriaFm(TFmMDFeInfCpl, FmMDFeInfCpl, afmoNegarComAviso) then
  begin
    FmMDFeInfCpl.ShowModal;
    FmMDFeInfCpl.Destroy;
  end;
  //
  if VAR_CADASTRO <> 0 then
  begin
    UMyMod.SetaCodigoPesquisado(EdMDFeInfCpl, CBMDFeInfCpl, QrMDFeInfCpl, VAR_CADASTRO, 'Codigo');
    EdMDFeInfCpl.SetFocus;
  end;
  {$EndIf}
*)
end;

procedure TFmParamsEmp.SpeedButton67Click(Sender: TObject);
begin
  MyObjects.DefineDiretorio(Self, EdDirMDFeSchema);
end;

procedure TFmParamsEmp.SpeedButton68Click(Sender: TObject);
var
  IniDir, Arquivo: String;
begin
  IniDir := ExtractFileDir(EdPathLogoMDFe.Text);
  Arquivo := ExtractFileName(EdPathLogoMDFe.Text);
  if MyObjects.FileOpenDialog(FmParamsEmp, IniDir, Arquivo,
  'Selecione o arquivo do Logo', '', [], Arquivo) then
    EdPathLogoMDFe.Text := Arquivo;
end;

procedure TFmParamsEmp.SpeedButton69Click(Sender: TObject);
begin
  MyObjects.DefineDiretorio(Self, EdDirDAMDFEs);
end;

procedure TFmParamsEmp.SpeedButton9Click(Sender: TObject);
begin
  MostraFmContas(EdCtaServico, CBCtaServico, QrCtaServico);
end;

procedure TFmParamsEmp.SBMoedaClick(Sender: TObject);
begin
{$IfNDef SemCotacoes}
  VAR_CADASTRO := 0;
  if DBCheck.CriaFm(TFmCambioMda, FmCambioMda, afmoNegarComAviso) then
  begin
    FmCambioMda.ShowModal;
    FmCambioMda.Destroy;
    if VAR_CADASTRO <> 0 then
    begin
      DModG.QrCambioMda.Close;
      UMyMod.AbreQuery(DModG.QrCambioMda, Dmod.MyDB, 'TFmParamsEmp.SBMoedaClick()');
      if DModG.QrCambioMda.Locate('Codigo', VAR_CADASTRO, []) then
      begin
        EdMoeda.ValueVariant := VAR_CADASTRO;
        CBMoeda.KeyValue     := VAR_CADASTRO;
      end;
    end;
  end;
{$EndIf}
end;

procedure TFmParamsEmp.Adicionafilial1Click(Sender: TObject);
var
  Novas: Integer;
begin
  BtNFeDefaultDirs.Enabled := False;
  BtCTeDefaultDirs.Enabled := False;
  BtMDFeDefaultDirs.Enabled := False;
  //
  DModG.QrFiliaisSP.Close;
  UMyMod.AbreQuery(DModG.QrFiliaisSP, Dmod.MyDB, 'TFmParamsEmp.Adicionafilial1Click()');
  if DModG.QrFiliaisSP.RecordCount > 0 then
  begin
    Novas := 0;
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('INSERT INTO paramsemp SET Codigo=:P0');
    while not DModG.QrFiliaisSP.Eof do
    begin
      if (DModG.QrFiliaisSPEmpresa.Value = 0) (*and (DModG.QrFiliaisSPEntidade.Value < 0)*)  then
      begin
        Novas := Novas + 1;
        Dmod.QrUpd.Params[0].AsInteger := DModG.QrFiliaisSPEntidade.Value;
        Dmod.QrUpd.ExecSQL;
      end;
      //
      DModG.QrFiliaisSP.Next;
    end;
    case Novas of
        0: Geral.MB_Aviso('N�o foi localizado nenhuma nova empresa!');
        1: Geral.MB_Aviso('Foi adicionada uma nova empresa!');
      else Geral.MB_Aviso('Foram adicionadas ' + Geral.FF0(Novas) + ' novas empresas!');
    end;
    if Novas > 0 then
      Va(vpLast);
  end else
    Geral.MB_Aviso('N�o foi localizado nenhuma empresa!');
end;

procedure TFmParamsEmp.Alteraentidadedafilialatual1Click(Sender: TObject);
begin
  DModG.CadastroDeEntidade(QrParamsEmpCodigo.Value, fmcadEntidade2, fmcadEntidade2, True);
  LocCod(QrParamsEmpCodigo.Value, QrParamsEmpCodigo.Value);
  DModG.AjustaFiliaisCliInt();
end;

procedure TFmParamsEmp.Alterafilialatual1Click(Sender: TObject);
var
  ForcaACBr: Integer;
begin
  FMostraTermo := False;
  //
  if PageControl1.ActivePageIndex < 7 then
    PageControl2.ActivePageIndex := PageControl1.ActivePageIndex
  else
    PageControl2.ActivePageIndex := 0;
  //
  //ReopenCartEmis(EmprEnti: Integer);
  {$IfNDef NO_FINANCEIRO}
  if CO_DMKID_APP = 28 then //Toolrent
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrCartEmisHonFun, Dmod.MyDB, [
      'SELECT car.Codigo, car.Nome, ',
      'car.Tipo, car.ForneceI ',
      'FROM carteiras car ',
      'WHERE car.Codigo<>0 ',
      //'AND car.Tipo=2 ',
      'AND car.ForneceI=' + Geral.FF0(QrParamsEmpEntidade.Value),
      'ORDER BY car.Nome ',
      '']);
  end else
    QrCartEmisHonFun.Close;
  {$EndIf}
  //
(*
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PainelEdita, QrParamsEmp, [PainelDados],
    [PainelEdita], nil, ImgTipo, 'ParamsEmp');
*)
  PreencheCamposAlteraOpcoesNFe();
  PreencheCamposAlteraOpcoesMis();
{$IfNDef SemNFe_0000}
  EdUCFisRegMesmaUF.ValueVariant := DModG.ObtemCodigo_TabApp('ParamsEmp', 'Codigo', 'UCFisRegMesmaUF', QrParamsEmpCodigo.Value);
  EdUCFisRegOutraUF.ValueVariant := DModG.ObtemCodigo_TabApp('ParamsEmp', 'Codigo', 'UCFisRegOutraUF', QrParamsEmpCodigo.Value);
  RGUCFisRegMadBy.Itemindex := DModG.ObtemCodigo_TabApp('ParamsEmp', 'Codigo', 'UCFisRegMadBy', QrParamsEmpCodigo.Value);
  EdUCCartPag.ValueVariant := DModG.ObtemCodigo_TabApp('ParamsEmp', 'Codigo', 'UCCartPag', QrParamsEmpCodigo.Value);
{$EndIf}
  ImgTipo.SQLType     := stUpd;
  PainelEdita.Visible := True;
  PainelDados.Visible := False;
  //
  case PageControl2.ActivePageIndex of
    0: EdMoeda.SetFocus;
    1: EdSituacao.SetFocus;
    2: EdFaturaSep.SetFocus;
    3: EdBalQtdItem.SetFocus;
  end;
  //
  PageControl3.ActivePageIndex := PageControl4.ActivePageIndex;
  //
  ConfiguraCamposTZ(Geral.IntToBool(QrParamsEmpTZD_UTC_Auto.Value));
  FMostraTermo := True;
  ForcaACBr := Geral.ReadAppKeyCU('ForcaACBr', Application.Title, ktInteger, 0);
  CkForcaACBrNestePC.Checked := Geral.IntToBool(ForcaACBr);
end;

procedure TFmParamsEmp.AlterasriedeCTeatual1Click(Sender: TObject);
begin
{$IfDef ComCTe_0000}
  UmyMod.FormInsUpd_Show(TFmParamsCTe, FmParamsCTe, afmoNegarComAviso,
    QrParamsCTe, stUpd);
{$EndIf}
end;

procedure TFmParamsEmp.Alterasriedecupomfiscalatual1Click(Sender: TObject);
begin
{$IfNDef semNFCe_0000}
  UmyMod.FormInsUpd_Show(TFmParamsNFCs, FmParamsNFCs, afmoNegarComAviso,
    QrParamsNFCs, stUpd);
{$EndIf}
end;

procedure TFmParamsEmp.AlterasriedeMDFeatual1Click(Sender: TObject);
begin
{$IfDef ComMDFe_0000}
  UmyMod.FormInsUpd_Show(TFmParamsMDFe, FmParamsMDFe, afmoNegarComAviso,
    QrParamsMDFe, stUpd);
{$EndIf}
end;

procedure TFmParamsEmp.Alterasriedenotafiscalatual1Click(Sender: TObject);
begin
{$IfNDef SemNFe_0000}
  UmyMod.FormInsUpd_Show(TFmParamsNFs, FmParamsNFs, afmoNegarComAviso,
    QrParamsNFs, stUpd);
{$EndIf}
end;

procedure TFmParamsEmp.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrParamsEmpCodigo.Value;
  Close;
end;

procedure TFmParamsEmp.BtSerieCTeClick(Sender: TObject);
begin
  PageControl1.ActivePageIndex := PageControl1.PageCount - 2;
  MyObjects.MostraPopUpDeBotao(PMSerieCTe, BtSerieCTe);
end;

procedure TFmParamsEmp.BtSerieMDFeClick(Sender: TObject);
begin
  PageControl1.ActivePageIndex := PageControl1.PageCount - 1;
  MyObjects.MostraPopUpDeBotao(PMSerieMDFe, BtSerieMDFe);
end;

procedure TFmParamsEmp.BtSerieNFCClick(Sender: TObject);
begin
  PageControl1.ActivePageIndex := PageControl1.PageCount - 3;
  MyObjects.MostraPopUpDeBotao(PMSerieNFC, BtSerieNFC);
end;

procedure TFmParamsEmp.BtSerieNFClick(Sender: TObject);
begin
  PageControl1.ActivePageIndex := PageControl1.PageCount - 3;
  MyObjects.MostraPopUpDeBotao(PMSerieNF, BtSerieNF);
end;

procedure TFmParamsEmp.BtSugereDirNFSeClick(Sender: TObject);
var
  Empresa, Dir, NFSeLogs: String;
  P: Integer;
begin
  NFSeLogs                   := EdDirNFSeLogs.ValueVariant;
  EdDirNFSeLogs.ValueVariant := 'C:\Dermatek\NFSe\';
  //
  Dir := MyObjects.DefineDiretorio(Self, EdDirNFSeLogs);
  //
  if Length(QrParamsEmpCNPJCPF_FILIAL.Value) > 1 then
    Empresa := 'CNPJ_' + Geral.SoNumero_TT(QrParamsEmpCNPJCPF_FILIAL.Value)
  else
  begin
    P := Pos(' ', QrParamsEmpNOMEFILIAL.Value);
    if P > 0 then
      Empresa := Copy(QrParamsEmpNOMEFILIAL.Value, 1, P)
    else
      Empresa := Copy(QrParamsEmpNOMEFILIAL.Value, 1);
    if Empresa = '' then
      Empresa := 'xxxxx';
    Empresa := dmkPF.ValidaPalavra(Empresa);
    Empresa := Trim(Copy(Empresa, 1, 30));
  end;
  //
  EdDirNFSeLogs.ValueVariant := NFSeLogs;
  //
  if Dir = '' then
    Dir := 'C:\Dermatek\NFSe\'
  else
    Dir := Dir + '\';
  //
  if EdDirNFSeLogs.Text = '' then
    EdDirNFSeLogs.Texto  := Dir; // + Empresa + '\XML';
  if EdDirNFSeSchema.Text = '' then
    EdDirNFSeSchema.Text := Dir + '\Schema'; // + Empresa + '\Schema';
  if EdDirNFSeDPSGer.Text = '' then
    EdDirNFSeDPSGer.Text := Dir; // + Empresa + '\';
  if EdDirNFSeDPSAss.Text = '' then
    EdDirNFSeDPSAss.Text  := Dir; // + Empresa + '\DPSAss';
  if EdDirNFSeNFSAut.Text = '' then
    EdDirNFSeNFSAut.Text  := Dir; // + Empresa + '\NFSAut';
  if EdDirNFSeRPSEnvLot.Text = '' then
    EdDirNFSeRPSEnvLot.Text  := Dir; // + Empresa + '\RPSEnvLot';
  if EdDirNFSeNFSCan.Text = '' then
    EdDirNFSeNFSCan.Text  := Dir; // + Empresa + '\NFSCant';
  if EdDirNFSeRPSRecLot.Text = '' then
    EdDirNFSeRPSRecLot.Text  := Dir; // + Empresa + '\RPSRecLot';
  if EdDirNFSeIniFiles.Text = '' then
    EdDirNFSeIniFiles.Text  := 'C:\Dermatek\ACBr\ACBrDFe\ACBrNFSe\Delphi\Ini'
end;

procedure TFmParamsEmp.btVersaoClick(Sender: TObject);
begin
  pgRespostas.ActivePageIndex := 0;
  MemoResp.Lines.Add(ACBrNFe0.SSL.SSLCryptClass.Versao);
end;

procedure TFmParamsEmp.BtNFeDefaultDirsClick(Sender: TObject);
var
  Empresa, Caminho, Dir: String;
  P, Ini: Integer;
begin
  Caminho := '';
  //
  Dir := MyObjects.DefineDiretorio(Self, EdDirNFeGer);
  //
  if Length(QrParamsEmpCNPJCPF_FILIAL.Value) > 1 then
    Empresa := 'CNPJ_' + Geral.SoNumero_TT(QrParamsEmpCNPJCPF_FILIAL.Value)
  else
  begin
    P := Pos(' ', QrParamsEmpNOMEFILIAL.Value);
    if P > 0 then
      Empresa := Copy(QrParamsEmpNOMEFILIAL.Value, 1, P)
    else
      Empresa := Copy(QrParamsEmpNOMEFILIAL.Value, 1);
    if Empresa = '' then
      Empresa := 'xxxxx';
    Empresa := dmkPF.ValidaPalavra(Empresa);
    Empresa := Trim(Copy(Empresa, 1, 30));
  end;
  if Dir = '' then
    Caminho := 'C:\Dermatek\NFe\' + Empresa
  else
    Caminho := Dir + '\' + Empresa;
  //
  if EdDirNFeGer.Text = '' then
    EdDirNFeGer.Text  := Caminho + '\NfeGer';
  if EdDirNFeAss.Text = '' then
    EdDirNFeAss.Text  := Caminho + '\NfeAss';
  if EdDirEnvLot.Text = '' then
    EdDirEnvLot.Text  := Caminho + '\EnvLot';
  if EdDirRec.Text = '' then
    EdDirRec.Text     := Caminho + '\Rec';
  if EdDirPedRec.Text = '' then
    EdDirPedRec.Text  := Caminho + '\PedRec';
  if EdDirProRec.Text = '' then
    EdDirProRec.Text  := Caminho + '\ProRec';
  if EdDirDen.Text = '' then
    EdDirDen.Text     := Caminho + '\Den';
  if EdDirPedCan.Text = '' then
    EdDirPedCan.Text  := Caminho + '\PedCan';
  if EdDirCan.Text = '' then
    EdDirCan.Text     := Caminho + '\Can';
  if EdDirPedInu.Text = '' then
    EdDirPedInu.Text  := Caminho + '\PedInu';
  if EdDirInu.Text = '' then
    EdDirInu.Text     := Caminho + '\Inu';
  if EdDirPedSit.Text = '' then
    EdDirPedSit.Text  := Caminho + '\PedSit';
  if EdDirSit.Text = '' then
    EdDirSit.Text     := Caminho + '\Sit';
  if EdDirPedSta.Text = '' then
    EdDirPedSta.Text  := Caminho + '\PedSta';
  if EdDirSta.Text = '' then
    EdDirSta.Text     := Caminho + '\Sta';
  //
  if EdDirNFeProt.Text = '' then
    EdDirNFeProt.Text := Caminho + '\NfeProt';
  if EdDirNFeRWeb.Text = '' then
    EdDirNFeRWeb.Text := Caminho + '\NfeWeb';
  //
  if EdDirEveEnvLot.Text = '' then
    EdDirEveEnvLot.Text := Caminho + '\EveEnvLot';
  if EdDirEveRetLot.Text = '' then
    EdDirEveRetLot.Text := Caminho + '\EveRetLot';
  if EdDirEvePedCCe.Text = '' then
    EdDirEvePedCCe.Text := Caminho + '\EvePedCCe';
  if EdDirEveRetCCe.Text = '' then
    EdDirEveRetCCe.Text := Caminho + '\EveRetCCe';
  if EdDirEveProcCCe.Text = '' then
    EdDirEveProcCCe.Text := Caminho + '\EveProcCCe';
  if EdDirEvePedCan.Text = '' then
    EdDirEvePedCan.Text := Caminho + '\EvePedCan';
  if EdDirEveRetCan.Text = '' then
    EdDirEveRetCan.Text := Caminho + '\EveRetCan';
  if EdDirEvePedEPEC.Text = '' then
    EdDirEvePedEPEC.Text := Caminho + '\EvePedEPEC';
  if EdDirEveRetEPEC.Text = '' then
    EdDirEveRetEPEC.Text := Caminho + '\EveRetEPEC';
  //
  if EdDirRetNfeDes.Text = '' then
    EdDirRetNfeDes.Text := Caminho + '\RetNfeDes';
  if EdDirEvePedMDe.Text = '' then
    EdDirEvePedMDe.Text := Caminho + '\EvePedMDe';
  if EdDirEveRetMDe.Text = '' then
    EdDirEveRetMDe.Text := Caminho + '\EveRetMDe';
  if EdDirDowNFeDes.Text = '' then
    EdDirDowNFeDes.Text := Caminho + '\DowNFeDes';
  if EdDirDowNFeNFe.Text = '' then
    EdDirDowNFeNFe.Text := Caminho + '\DowNFeNFe';
  if EdDirDistDFeInt.Text = '' then
    EdDirDistDFeInt.Text := Caminho + '\DistDFeInt';
  if EdDirRetDistDFeInt.Text = '' then
    EdDirRetDistDFeInt.Text := Caminho + '\RetDistDFeInt';
  if EdDirDowNFeCnf.Text = '' then
    EdDirDowNFeCnf.Text := Caminho + '\DowNFeCnf';
end;

procedure TFmParamsEmp.btnHTTPSClick(Sender: TObject);
begin
{$ifNDef sACBr}
  DmkACBr_ParamsEmp.TestarHTTPS_SemCertificado(ACBrNFe0);
{$EndIf}
end;

procedure TFmParamsEmp.btnIssuerNameClick(Sender: TObject);
begin
 ShowMessage(ACBrNFe0.SSL.CertIssuerName + sLineBreak + sLineBreak +
             'Certificadora: ' + ACBrNFe0.SSL.CertCertificadora);

end;

procedure TFmParamsEmp.btnNumSerieClick(Sender: TObject);
begin
  ShowMessage(ACBrNFe0.SSL.CertNumeroSerie);
end;

procedure TFmParamsEmp.btnSha256Click(Sender: TObject);
begin
{$ifNDef sACBr}
  DmkACBr_ParamsEmp.TestarSHA256_Mais_RSA(ACBrNFe0);
{$Endif}
end;

procedure TFmParamsEmp.btnStatusServClick(Sender: TObject);
var
  CodStatus: Integer;
  TxtStatus: String;
begin
{$ifNDef sACBr}
  DmkACBr_ParamsEmp.DFe_ConsultaSstatusServico(ACBrNFe0, CodStatus, TxtStatus);
{$Endif}
end;

procedure TFmParamsEmp.btnSubNameClick(Sender: TObject);
begin
  ShowMessage(ACBrNFe0.SSL.CertSubjectName + sLineBreak + sLineBreak +
              'Raz�o Social: ' + ACBrNFe0.SSL.CertRazaoSocial);
end;

procedure TFmParamsEmp.BtCertDigitalClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMCertDigital, BtCertDigital);
end;

procedure TFmParamsEmp.BtConfirmaClick(Sender: TObject);
var
  Codigo, Simples, IndicePC1, IndicePC3, CIB: Integer;
  Err1, Err2, Err3, Erro: Boolean;
begin
  IndicePC1 := PageControl2.ActivePageIndex;
  IndicePC3 := PageControl3.ActivePageIndex;
  //
  if MyObjects.FIC((RGCRT.ItemIndex = 1) and (CkSimplesFed.Checked = False), nil,
    '"Ativo no super simples (federal) deve estar selecionado para CRT=1"!') then Exit;
  if MyObjects.FIC((RGCRT.ItemIndex <> 1) and (CkSimplesFed.Checked = True), nil,
    '"CRT deve ser = 1 para super simples (federal)"!') then Exit;
  if MyObjects.FIC((EdNFSeCodMunici.ValueVariant <> 0) and
    (EdNFSeCodMunici.ValueVariant <> 4115200), nil,
      'O m�dulo NFS-e est� dispon�vel apenas para o municipio de Maring� PR!') then Exit;
  //
  PageControl2.ActivePageIndex := 5;
  //
  if MyObjects.NaoPermiteItemRadioGroup(RGSPED_EFD_Peri_E100, [3,5], '') then
    Exit;
  if MyObjects.NaoPermiteItemRadioGroup(RGSPED_EFD_Peri_E500, [3,5], '') then
    Exit;
  if MyObjects.NaoPermiteItemRadioGroup(RGSPED_EFD_Peri_K100, [3,5], '') then
    Exit;
  //
  if  DModG.QrCtrlGeralUsarEntraFiscal.Value = 1 then
  begin
    if MyObjects.FIC(RGRegimTributar.ItemIndex = 0, RGRegimTributar,
      '"Informe o Regime Tribut�rio"!') then Exit;
    if MyObjects.FIC(RGRegimPisCofins.ItemIndex = 0, RGRegimPisCofins,
      '"Informe o Regime PIS/COFINS"!') then Exit;
  end;
  //
  CIB := Geral.IMV(Geral.SoNumero_TT(EdSPED_COD_INC_TRIB.ValueVariant));
  Err1 := (CIB = 1) and (RGRegimPisCofins.ItemIndex <> 1);
  Err2 := (CIB = 2) and (RGRegimPisCofins.ItemIndex <> 2);
  Err3 := (CIB = 3) and (not (RGRegimPisCofins.ItemIndex in ([1,2])));
  Erro := (Err1=True) or (Err2=True) or (Err3=True);
  if MyObjects.FIC(Erro, EdSPED_COD_INC_TRIB,
  '"C�digo indicador da incid�ncia tribut�ria no per�odo" n�o combina com "Regime PIS/COFINS"!') then Exit;
  //
  if EdTZD_UTC.Text = '+00:01' then
  begin
    Geral.MB_Aviso('Informe um TZD UTC no campo TZD UTC');
    PageControl2.ActivePageIndex := 0;
    EdTZD_UTC.SetFocus;
  end;
{
  EdLogo3x1.Text       := (*dmkPF.DuplicaBarras(*)EdLogo3x1.Text(*)*);
  EdDirNFeGer.Text     := (*dmkPF.DuplicaBarras(*)EdDirNFeGer.Text(*)*);
  EdDirNFeAss.Text     := (*dmkPF.DuplicaBarras(*)EdDirNFeAss.Text(*)*);
  EdDirEnvLot.Text     := (*dmkPF.DuplicaBarras(*)EdDirEnvLot.Text(*)*);
  EdDirRec.Text        := (*dmkPF.DuplicaBarras(*)EdDirRec.Text(*)*);
  EdDirPedRec.Text     := (*dmkPF.DuplicaBarras(*)EdDirPedRec.Text(*)*);
  EdDirProRec.Text     := (*dmkPF.DuplicaBarras(*)EdDirProRec.Text(*)*);
  EdDirDen.Text        := (*dmkPF.DuplicaBarras(*)EdDirDen.Text(*)*);
  EdDirPedCan.Text     := (*dmkPF.DuplicaBarras(*)EdDirPedCan.Text(*)*);
  EdDirCan.Text        := (*dmkPF.DuplicaBarras(*)EdDirCan.Text(*)*);
  EdDirPedInu.Text     := (*dmkPF.DuplicaBarras(*)EdDirPedInu.Text(*)*);
  EdDirInu.Text        := (*dmkPF.DuplicaBarras(*)EdDirInu.Text(*)*);
  EdDirPedSit.Text     := (*dmkPF.DuplicaBarras(*)EdDirPedSit.Text(*)*);
  EdDirSit.Text        := (*dmkPF.DuplicaBarras(*)EdDirSit.Text(*)*);
  EdDirPedSta.Text     := (*dmkPF.DuplicaBarras(*)EdDirPedSta.Text(*)*);
  EdDirSta.Text        := (*dmkPF.DuplicaBarras(*)EdDirSta.Text(*)*);
  EdPathLogoNF.Text    := (*dmkPF.DuplicaBarras(*)EdPathLogoNF.Text(*)*);
  EdSINTEGRA_Path.Text := (*dmkPF.DuplicaBarras(*)EdSINTEGRA_Path.Text(*)*);
  EdDirDANFEs.Text     := (*dmkPF.DuplicaBarras(*)EdDirDANFEs.Text(*)*);
  EdDirNFeProt.Text    := (*dmkPF.DuplicaBarras(*)EdDirNFeProt.Text(*)*);
  //
}
  Codigo := QrParamsEmpCodigo.Value;
  if UMyMod.ExecSQLInsUpdPanel(ImgTipo.SQLType, FmParamsEmp, PainelEdita,
    'ParamsEmp', Codigo, Dmod.QrUpd, [PainelEdita], [PainelDados], ImgTipo, True) then
  begin
 {$ifNDef sACBr}
    DmkACBr_ParamsEmp.SalvarConfiguracao();
 {$Endif}
    Simples := dmkPF.EscolhaDe2Int(CkSimplesFed.Checked, 1, 0);
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'entidades', False, [
    'CRT', 'Simples'], ['Codigo'], [RGCRT.ItemIndex, Simples], [Codigo], True) then
    begin
      //Atualiza aceite
      if not UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'paramsemp', False,
        ['TZD_UTC_Auto_TermoAceite', 'TZD_UTC_Auto_DataHoraAceite',
        'TZD_UTC_Auto_UsuarioAceite'], ['Codigo'],
        [FTZD_UTC_Auto_TermoAceite, Geral.FDT(FTZD_UTC_Auto_DataHoraAceite, 109),
        FTZD_UTC_Auto_UsuarioAceite], [Codigo], True)
      then
        Geral.MB_Erro('Falha ao atualizar aceite!');
      //
      Geral.WriteAppKeyCU('CertDigPfxLoc', Application.Title, RGCertDigPfxLoc.ItemIndex, ktInteger);
      RG2CertDigPfxLoc.ItemIndex := RGCertDigPfxLoc.ItemIndex;
      //
      LocCod(Codigo, Codigo);
      PageControl1.ActivePageIndex := IndicePC1;
      PageControl4.ActivePageIndex := IndicePC3;
      DModG.QrParamsEmp.Close;
      // Se for apenas uma empresa! -> no caso dos Syn... � mais que uma!
      if pos(',', VAR_LIB_EMPRESAS) = 0 then
        DModG.ReopenParamsEmp(Geral.IMV(VAR_LIB_EMPRESAS));
    end;
  end;
  Geral.WriteAppKeyCU('ForcaACBr', Application.Title, Geral.BoolToInt(CkForcaACBrNestePC.Checked), ktInteger);
end;

procedure TFmParamsEmp.BtCTeDefaultDirsClick(Sender: TObject);
var
  Empresa, Caminho, Dir: String;
  P, Ini: Integer;
begin
  Caminho := '';
  //
  Dir := MyObjects.DefineDiretorio(Self, EdDirCTeGer);
  //
  if Length(QrParamsEmpCNPJCPF_FILIAL.Value) > 1 then
    Empresa := 'CNPJ_' + Geral.SoNumero_TT(QrParamsEmpCNPJCPF_FILIAL.Value)
  else
  begin
    P := Pos(' ', QrParamsEmpNOMEFILIAL.Value);
    if P > 0 then
      Empresa := Copy(QrParamsEmpNOMEFILIAL.Value, 1, P)
    else
      Empresa := Copy(QrParamsEmpNOMEFILIAL.Value, 1);
    if Empresa = '' then
      Empresa := 'xxxxx';
    Empresa := dmkPF.ValidaPalavra(Empresa);
    Empresa := Trim(Copy(Empresa, 1, 30));
  end;
  if Dir = '' then
    Caminho := 'C:\Dermatek\CTe\' + Empresa
  else
    Caminho := Dir + '\' + Empresa;
  //
  if EdDirCTeGer.Text = '' then
    EdDirCTeGer.Text  := Caminho + '\CTeGer';
  if EdDirCTeAss.Text = '' then
    EdDirCTeAss.Text  := Caminho + '\CTeAss';
  if EdDirCTeEnvLot.Text = '' then
    EdDirCTeEnvLot.Text  := Caminho + '\EnvLot';
  if EdDirCTeRec.Text = '' then
    EdDirCTeRec.Text     := Caminho + '\Rec';
  if EdDirCTePedRec.Text = '' then
    EdDirCTePedRec.Text  := Caminho + '\PedRec';
  if EdDirCTeProRec.Text = '' then
    EdDirCTeProRec.Text  := Caminho + '\ProRec';
  if EdDirCTeDen.Text = '' then
    EdDirCTeDen.Text     := Caminho + '\Den';
  if EdDirCTePedCan.Text = '' then
    EdDirCTePedCan.Text  := Caminho + '\PedCan';
  if EdDirCTeCan.Text = '' then
    EdDirCTeCan.Text     := Caminho + '\Can';
  if EdDirCTePedInu.Text = '' then
    EdDirCTePedInu.Text  := Caminho + '\PedInu';
  if EdDirCTeInu.Text = '' then
    EdDirCTeInu.Text     := Caminho + '\Inu';
  if EdDirCTePedSit.Text = '' then
    EdDirCTePedSit.Text  := Caminho + '\PedSit';
  if EdDirCTeSit.Text = '' then
    EdDirCTeSit.Text     := Caminho + '\Sit';
  if EdDirCTePedSta.Text = '' then
    EdDirCTePedSta.Text  := Caminho + '\PedSta';
  if EdDirCTeSta.Text = '' then
    EdDirCTeSta.Text     := Caminho + '\Sta';
  //
  if EdDirCTeProt.Text = '' then
    EdDirCTeProt.Text := Caminho + '\CTeProt';
  if EdDirCTeRWeb.Text = '' then
    EdDirCTeRWeb.Text := Caminho + '\CTeWeb';
  //
  if EdDirCTeEveEnvLot.Text = '' then
    EdDirCTeEveEnvLot.Text := Caminho + '\EveEnvLot';
  if EdDirCTeEveRetLot.Text = '' then
    EdDirCTeEveRetLot.Text := Caminho + '\EveRetLot';
  if EdDirCTeEvePedCCe.Text = '' then
    EdDirCTeEvePedCCe.Text := Caminho + '\EvePedCCe';
  if EdDirCTeEveRetCCe.Text = '' then
    EdDirCTeEveRetCCe.Text := Caminho + '\EveRetCCe';
  if EdDirCTeEveProcCCe.Text = '' then
    EdDirCTeEveProcCCe.Text := Caminho + '\EveProcCCe';
  if EdDirCTeEvePedCan.Text = '' then
    EdDirCTeEvePedCan.Text := Caminho + '\EvePedCan';
  if EdDirCTeEveRetCan.Text = '' then
    EdDirCTeEveRetCan.Text := Caminho + '\EveRetCan';
  if EdDirCTeEvePedEPEC.Text = '' then
    EdDirCTeEvePedEPEC.Text := Caminho + '\EvePedEPEC';
  if EdDirCTeEveRetEPEC.Text = '' then
    EdDirCTeEveRetEPEC.Text := Caminho + '\EveRetEPEC';
  //
(*
  if EdDirRetCTeDes.Text = '' then
    EdDirRetCTeDes.Text := Caminho + '\RetCTeDes';
  if EdDirCTeEvePedMDe.Text = '' then
    EdDirCTeEvePedMDe.Text := Caminho + '\EvePedMDe';
  if EdDirCTeEveRetMDe.Text = '' then
    EdDirCTeEveRetMDe.Text := Caminho + '\EveRetMDe';
*)
  if EdDirDowCTeDes.Text = '' then
    EdDirDowCTeDes.Text := Caminho + '\DowCTeDes';
  if EdDirDowCTeCTe.Text = '' then
    EdDirDowCTeCTe.Text := Caminho + '\DowCTeCTe';
  if EdDirDistDFeInt.Text = '' then
    EdDirDistDFeInt.Text := Caminho + '\DistDFeInt';
  if EdDirRetDistDFeInt.Text = '' then
    EdDirRetDistDFeInt.Text := Caminho + '\RetDistDFeInt';
  if EdDirDowCTeCnf.Text = '' then
    EdDirDowCTeCnf.Text := Caminho + '\DowCTeCnf';
end;

procedure TFmParamsEmp.BtDesisteClick(Sender: TObject);
var
  Codigo: Integer;
begin
  Codigo := QrParamsEmpCodigo.Value;
  MostraEdicao(0, stLok, Codigo);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'ParamsEmp', 'Codigo');
end;

procedure TFmParamsEmp.BtFilialClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMFilial, BtFilial);
end;

procedure TFmParamsEmp.BtMDFeDefaultDirsClick(Sender: TObject);
var
  Empresa, Caminho, Dir: String;
  P, Ini: Integer;
begin
  Caminho := '';
  //
  //Dir := MyObjects.DefineDiretorio(Self, EdDirMDFeGer);
  Dir := '';
  //
  if Length(QrParamsEmpCNPJCPF_FILIAL.Value) > 1 then
    Empresa := 'CNPJ_' + Geral.SoNumero_TT(QrParamsEmpCNPJCPF_FILIAL.Value)
  else
  begin
    P := Pos(' ', QrParamsEmpNOMEFILIAL.Value);
    if P > 0 then
      Empresa := Copy(QrParamsEmpNOMEFILIAL.Value, 1, P)
    else
      Empresa := Copy(QrParamsEmpNOMEFILIAL.Value, 1);
    if Empresa = '' then
      Empresa := 'xxxxx';
    Empresa := dmkPF.ValidaPalavra(Empresa);
    Empresa := Trim(Copy(Empresa, 1, 30));
  end;
  if Dir = '' then
    Caminho := 'C:\Dermatek\MDFe\' + Empresa
  else
    Caminho := Dir + '\' + Empresa;
  //
  if EdDirMDFeGer.Text = '' then
    EdDirMDFeGer.Text  := Caminho + '\MDFeGer';
  if EdDirMDFeAss.Text = '' then
    EdDirMDFeAss.Text  := Caminho + '\MDFeAss';
  if EdDirMDFeEnvLot.Text = '' then
    EdDirMDFeEnvLot.Text  := Caminho + '\EnvLot';
  if EdDirMDFeRec.Text = '' then
    EdDirMDFeRec.Text     := Caminho + '\Rec';
  if EdDirMDFePedRec.Text = '' then
    EdDirMDFePedRec.Text  := Caminho + '\PedRec';
  if EdDirMDFeProRec.Text = '' then
    EdDirMDFeProRec.Text  := Caminho + '\ProRec';
  if EdDirMDFeDen.Text = '' then
    EdDirMDFeDen.Text     := Caminho + '\Den';
  if EdDirMDFePedCan.Text = '' then
    EdDirMDFePedCan.Text  := Caminho + '\PedCan';
  if EdDirMDFeCan.Text = '' then
    EdDirMDFeCan.Text     := Caminho + '\Can';
  if EdDirMDFePedInu.Text = '' then
    EdDirMDFePedInu.Text  := Caminho + '\PedInu';
  if EdDirMDFeInu.Text = '' then
    EdDirMDFeInu.Text     := Caminho + '\Inu';
  if EdDirMDFePedSit.Text = '' then
    EdDirMDFePedSit.Text  := Caminho + '\PedSit';
  if EdDirMDFeSit.Text = '' then
    EdDirMDFeSit.Text     := Caminho + '\Sit';
  if EdDirMDFePedSta.Text = '' then
    EdDirMDFePedSta.Text  := Caminho + '\PedSta';
  if EdDirMDFeSta.Text = '' then
    EdDirMDFeSta.Text     := Caminho + '\Sta';
  //
  if EdDirMDFeProt.Text = '' then
    EdDirMDFeProt.Text := Caminho + '\MDFeProt';
  if EdDirMDFeRWeb.Text = '' then
    EdDirMDFeRWeb.Text := Caminho + '\MDFeWeb';
  //
  if EdDirMDFeEveEnvLot.Text = '' then
    EdDirMDFeEveEnvLot.Text := Caminho + '\EveEnvLot';
  if EdDirMDFeEveRetLot.Text = '' then
    EdDirMDFeEveRetLot.Text := Caminho + '\EveRetLot';
  if EdDirMDFeEvePedEnc.Text = '' then
    EdDirMDFeEvePedEnc.Text := Caminho + '\EvePedEnc';
  if EdDirMDFeEveRetEnc.Text = '' then
    EdDirMDFeEveRetEnc.Text := Caminho + '\EveRetEnc';
(*
  if EdDirMDFeEveProcCCe.Text = '' then
    EdDirMDFeEveProcCCe.Text := Caminho + '\EveProcCCe';
*)
  if EdDirMDFeEvePedCan.Text = '' then
    EdDirMDFeEvePedCan.Text := Caminho + '\EvePedCan';
  if EdDirMDFeEveRetCan.Text = '' then
    EdDirMDFeEveRetCan.Text := Caminho + '\EveRetCan';
  if EdDirMDFeEvePedIdC.Text = '' then
    EdDirMDFeEvePedIdC.Text := Caminho + '\EvePedIdC';
  if EdDirMDFeEveRetIdC.Text = '' then
    EdDirMDFeEveRetIdC.Text := Caminho + '\EveRetIdC';
  //
(*
  if EdDirRetMDFeDes.Text = '' then
    EdDirRetMDFeDes.Text := Caminho + '\RetMDFeDes';
  if EdDirMDFeEvePedMDe.Text = '' then
    EdDirMDFeEvePedMDe.Text := Caminho + '\EvePedMDe';
  if EdDirMDFeEveRetMDe.Text = '' then
    EdDirMDFeEveRetMDe.Text := Caminho + '\EveRetMDe';
*)
  if EdDirDowMDFeDes.Text = '' then
    EdDirDowMDFeDes.Text := Caminho + '\DowMDFeDes';
  if EdDirDowMDFeMDFe.Text = '' then
    EdDirDowMDFeMDFe.Text := Caminho + '\DowMDFeMDFe';
  if EdDirDistDFeInt.Text = '' then
    EdDirDistDFeInt.Text := Caminho + '\DistDFeInt';
  if EdDirRetDistDFeInt.Text = '' then
    EdDirRetDistDFeInt.Text := Caminho + '\RetDistDFeInt';
  if EdDirDowMDFeCnf.Text = '' then
    EdDirDowMDFeCnf.Text := Caminho + '\DowMDFeCnf';
end;

procedure TFmParamsEmp.btnCNPJClick(Sender: TObject);
begin
  ShowMessage(ACBrNFe0.SSL.CertCNPJ);
end;

procedure TFmParamsEmp.btnDataValidadeClick(Sender: TObject);
begin
  ShowMessage(FormatDateBr(ACBrNFe0.SSL.CertDataVenc));
end;

procedure TFmParamsEmp.BtOpcoesClick(Sender: TObject);
const
  PerguntaSobreTutorial = True;
  PerguntaSobreOpcoesInternet = True;
  PerguntaSobreSnapIn = True;
  E_Message = '';
begin
{
  2022-02-11 Movido para UnitWin procedure UnWin.OpcoesDaInternet(PerguntaSobreTutorial: Boolean);
}
{
  if Geral.MB_Pergunta('Deseja abrir um tutorial sobre configura��es conhecidas?') = ID_YES then
    DmkWeb.MostraWebBrowser('http://www.dermatek.net.br/index.php?page=wfaqview&id=172', True, False, 0, 0);
  try
    ShellExecute(HANDLE, 'open', 'control.exe',  'inetcpl.cpl', nil, SW_SHOWNORMAL);
  except
    try
      WinExec('control inetcpl.cpl', SW_SHOW);
    except
      ;
    end;
  end;
}
{$ifNDef sACBr}
UnWin.OpcoesDaInternet(Self, E_Message, PerguntaSobreTutorial,
  PerguntaSobreOpcoesInternet, PerguntaSobreSnapIn);
{$Endif}
end;

procedure TFmParamsEmp.FormCreate(Sender: TObject);
var
  Visi: Boolean;
begin
  FCriado := False;
  // For�ar retirar acentos. Erro de XML!
  cbxRetirarAcentos.Checked := True;
// ini dmkMLA
  ACBrNFe0 := TACBrNFe.Create(Self);
  ACBrIntegrador1 := TACBrIntegrador.Create(Self);
// fim dmkMLA
  ImgTipo.SQLType       := stLok;
  CBSituacao.ListSource := DModG.DsSituacao;
  CBMoeda.ListSource    := DModG.DsCambioMda;
  UMyMod.AbreQuery(QrFiliais, Dmod.MyDB, 'TFmParamsEmp.FormCreate()');
  //
  {$IfNDef NO_FINANCEIRO}
  UMyMod.AbreQuery(QrCtaProdVen, Dmod.MyDB, 'TFmParamsEmp.FormCreate()');
  UMyMod.AbreQuery(QrCtaProdCom, Dmod.MyDB, 'TFmParamsEmp.FormCreate()');
  UMyMod.AbreQuery(QrCtaServico, Dmod.MyDB, 'TFmParamsEmp.FormCreate()');
  UMyMod.AbreQuery(QrCtaServicoPg, Dmod.MyDB, 'TFmParamsEmp.FormCreate()');
  UMyMod.AbreQuery(QrCtaMultiSV, Dmod.MyDB, 'TFmParamsEmp.FormCreate()');
  UndmkDAC_PF.AbreQuery(QrCtaFretPrest, Dmod.MyDB);
  UndmkDAC_PF.AbreQuery(QrCTeEntiTipCt1, Dmod.MyDB);
  UndmkDAC_PF.AbreQuery(QrCTeEntiTipCto, Dmod.MyDB);
  UndmkDAC_PF.AbreQuery(QrCtbCadGruGer, Dmod.MyDB);
  {$EndIf}
  //
  UMyMod.AbreQuery(QrUFs, Dmod.MyDB, 'TFmParamsEmp.FormCreate()');
  UMyMod.AbreQuery(QrContador4, Dmod.MyDB, 'TFmParamsEmp.FormCreate()');
  UMyMod.AbreQuery(QrEscrCtb4, Dmod.MyDB, 'TFmParamsEmp.FormCreate()');
  //
  {$IfNDef SemNFe_0000}
  UMyMod.AbreQuery(QrNFeInfCpl, Dmod.MyDB, 'TFmParamsEmp.FormCreate()');
  //
  TabSheet8.TabVisible  := True; //Aba NF-e
  TabSheet9.TabVisible  := True; //Aba NF-e
  TabSheet2.TabVisible  := True; //Aba Pedidos
  TabSheet4.TabVisible  := True; //Aba Pedidos
  BtSerieNF.Visible     := True; //NF-e
  BtSerieNFC.Visible    := True; //NFC-e
  BtCertDigital.Visible := True; //NF-e e NFC-e
  {$Else}
  QrNFeInfCpl.Close;
  //
  TabSheet8.TabVisible  := False; //Aba NF-e
  TabSheet9.TabVisible  := False; //Aba NF-e
  TabSheet2.TabVisible  := False; //Aba Pedidos
  TabSheet4.TabVisible  := False; //Aba Pedidos
  BtSerieNF.Visible     := False; //NF-e
  BtSerieNFC.Visible    := False; //NFC-e
  BtCertDigital.Visible := False; //NF-e e NFC-e
  {$EndIf}
  //
  {$IfNDef sNFSe}
    TabSheet28.TabVisible := True; //Aba NFS-e
    TabSheet29.TabVisible := True; //Aba NFS-e
  {$Else}
    TabSheet28.TabVisible := False; //Aba NFS-e
    TabSheet29.TabVisible := False; //Aba NFS-e
  {$EndIf}
  // mudei para DModG > cambios
  ///DmPediVda.ReopenTabelasPedido();
  {$IfNDef SemCotacoes}
  UMyMod.AbreQuery(DmodG.QrCambioMda, Dmod.MyDB, 'TFmParamsEmp.FormCreate()');
  {$EndIf}
  //
  {$IfNDef NO_USE_EMAILDMK}
  UMyMod.AbreQuery(QrPreEmailNFSeAut, Dmod.MyDB, 'TFmParamsEmp.FormCreate()');
  UMyMod.AbreQuery(QrPreEmailNFSeCan, Dmod.MyDB, 'TFmParamsEmp.FormCreate()');
  {$EndIf}
  //
  PageControl1.Align           := alClient;
  PageControl2.Align           := alClient;
  PageControl1.ActivePageIndex := 0;
  PageControl2.ActivePageIndex := 0;
  PageControl3.ActivePageIndex := 0;
  PageControl4.ActivePageIndex := 0;
  PageControl5.ActivePageIndex := 0;
  PageControl6.ActivePageIndex := 0;
  PCACBr.ActivePageIndex := 0;
  FSiglas_WS                   := Geral.SiglasWebService();
  //
  (*&
  QrPrmsEmpMisNOMESITUACAO.LookupDataSet := DModG.QrSituacao;

  Guia pedidos - ao lado do DBEdit10 >> DsParamsEmp.Situacao

object DBEdit8: TDBEdit
  Left = 32
  Top = 20
  Width = 140
  Height = 21
  DataField = 'NOMESITUACAO'
  DataSource = DsPrmsEmpMis
  TabOrder = 1
end

//

object QrPrmsEmpMisNOMESITUACAO: TWideStringField
  FieldKind = fkLookup
  FieldName = 'NOMESITUACAO'
  LookupKeyFields = 'Codigo'
  LookupResultField = 'Nome'
  KeyFields = 'Situacao'
  Size = 30
  Lookup = True
end


  *)
  //
  {$IfNDef NO_USE_EMAILDMK}
  UMyMod.AbreQuery(QrPreMail0, Dmod.MyDB);
  UMyMod.AbreQuery(QrPreMail1, Dmod.MyDB);
  {$EndIf}
  UMyMod.AbreQuery(QrEntiTipCto, Dmod.MyDB);
  UMyMod.AbreQuery(QrEntiTipCt1, Dmod.MyDB);
  UMyMod.AbreQuery(QrEntiTipCt2, Dmod.MyDB);
  {$IfNDef sNFSe}
  UMyMod.AbreQuery(QrMyNFSeMetodos, DModG.AllID_DB);
  {$EndIf}
  //
  {$IFDEF DEFINE_VARLCT}
  SBCartEmisHonFun.Visible := True;
  {$Else}
  SBCartEmisHonFun.Visible := False;
  {$EndIf}
  Visi                       := CO_DMKID_APP = 28;
  LaCartEmisHonFun.Visible   := Visi;
  EdCartEmisHonFun.Visible   := Visi;
  CBCartEmisHonFun.Visible   := Visi;
  SBCartEmisHonFun.Visible   := Visi;
  DBLaCartEmisHonFun.Visible := Visi;
  DBEdCartEmisHonFun.Visible := Visi;
  DBCBCartEmisHonFun.Visible := Visi;
  //
  {$IfDef ComCTe_0000}
  BtSerieCTe.Visible         := True; //CT-e
  {$EndIf}
  {$IfDef ComMDFe_0000}
  BtSerieMDFe.Visible         := True; //CT-e
  {$EndIf}
  CriaOForm;
  RGCertDigPfxLoc.ItemIndex := Geral.ReadAppKeyCU('CertDigPfxLoc', Application.Title, ktInteger, 1);
  //
  // ACBr
{$ifNDef sACBr}
  DmkACBr_ParamsEmp.FormCreate_ACBr(ACBrNFe0);
{$Endif}
{$IfNDef SemNFe_0000}
  F_CLAS_ESTAB_IND := UnNFe_PF.ListaCLAS_ESTAB_IND();
  F_IND_NAT_PJ     := UnNFe_PF.ListaIND_NAT_PJ();
  F_COD_INC_TRIB   := UnNFe_PF.ListaCOD_INC_TRIB();
  F_IND_APRO_CRED  := UnNFe_PF.ListaIND_APRO_CRED();
  F_COD_TIPO_CONT  := UnNFe_PF.ListaCOD_TIPO_CONT();
  F_IND_REG_CUM    := UnNFe_PF.ListaIND_REG_CUM();
  //
{$EndIf}
end;


procedure TFmParamsEmp.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrParamsEmpCodigo.Value, LaRegistro.Caption);
end;

procedure TFmParamsEmp.SbProdVenClick(Sender: TObject);
begin
  MostraFmContas(EdCtaProdVen, CBCtaProdVen, QrCtaProdVen);
end;

procedure TFmParamsEmp.SBDirPedCanClick(Sender: TObject);
begin
  MyObjects.DefineDiretorio(Self, EdDirPedCan);
end;

procedure TFmParamsEmp.SBCartEmisHonFunClick(Sender: TObject);
begin
{$IfNDef NO_FINANCEIRO}
{$IFDEF DEFINE_VARLCT}
  FinanceiroJan.InsereEDefineCarteira(EdCartEmisHonFun.ValueVariant,
  EdCartEmisHonFun, CBCartEmisHonFun, QrCartEmisHonFun);
{$EndIf}
{$EndIf}
end;

procedure TFmParamsEmp.SbCertDigPfxCamClick(Sender: TObject);
begin
  MyObjects.DefineArquivo1(Self, EdCertDigPfxCam);
end;

procedure TFmParamsEmp.SBCtaProdComClick(Sender: TObject);
begin
  MostraFmContas(EdCtaProdCom, CBCtaProdCom, QrCtaProdCom);
end;

procedure TFmParamsEmp.SBCtaServicoPgClick(Sender: TObject);
begin
  MostraFmContas(EdCtaServicoPg, CBCtaServicoPg, QrCtaServicoPg);
end;

procedure TFmParamsEmp.SbCtbCadGruGerClick(Sender: TObject);
begin
  {$IfNDef NO_FINANCEIRO}
  VAR_CADASTRO := 0;
  FinanceiroJan.MostraFormCtbCadGru(EdCtbCadGruGer.ValueVariant);
  if VAR_CADASTRO <> 0 then
  begin
    UMyMod.AbreQuery(QrCtbCadGruGer, Dmod.MyDB);
    //
    UMyMod.SetaCodigoPesquisado(EdCtbCadGruGer, CBCtbCadGruGer, QrCtbCadGruGer, VAR_CADASTRO);
    EdCtbCadGruGer.SetFocus;
  end;
  {$EndIf}
end;

procedure TFmParamsEmp.SBDirCanClick(Sender: TObject);
begin
  MyObjects.DefineDiretorio(Self, EdDirCan);
end;

procedure TFmParamsEmp.SBDirPedInuClick(Sender: TObject);
begin
  MyObjects.DefineDiretorio(Self, EdDirPedInu);
end;

procedure TFmParamsEmp.SBDirInuClick(Sender: TObject);
begin
  MyObjects.DefineDiretorio(Self, EdDirInu);
end;

procedure TFmParamsEmp.SBDirRecClick(Sender: TObject);
begin
  MyObjects.DefineDiretorio(Self, EdDirRec);
end;

procedure TFmParamsEmp.SbDirRetDistDFeIntClick(Sender: TObject);
begin
  MyObjects.DefineDiretorio(Self, EdDirRetDistDFeInt);
end;

procedure TFmParamsEmp.SBDirRetNfeDesClick(Sender: TObject);
begin
  MyObjects.DefineDiretorio(Self, EdDirRetNfeDes);
end;

procedure TFmParamsEmp.SBDirEnvLotClick(Sender: TObject);
begin
  MyObjects.DefineDiretorio(Self, EdDirEnvLot);
end;

procedure TFmParamsEmp.SbDirEveRetCanClick(Sender: TObject);
begin
  MyObjects.DefineDiretorio(Self, EdDirEveRetCan);
end;

procedure TFmParamsEmp.SbDirEveRetEPECClick(Sender: TObject);
begin
  MyObjects.DefineDiretorio(Self, EdDirEveRetEPEC);
end;

procedure TFmParamsEmp.SbDirEveCCeClick(Sender: TObject);
begin
  MyObjects.DefineDiretorio(Self, EdDirEveRetCCe);
end;

procedure TFmParamsEmp.SbDirEveEnvLotClick(Sender: TObject);
begin
  MyObjects.DefineDiretorio(Self, EdDirEveEnvLot);
end;

procedure TFmParamsEmp.SbDirEvePedCanClick(Sender: TObject);
begin
  MyObjects.DefineDiretorio(Self, EdDirEvePedCan);
end;

procedure TFmParamsEmp.SbDirEvePedCCeClick(Sender: TObject);
begin
  MyObjects.DefineDiretorio(Self, EdDirEvePedCCe);
end;

procedure TFmParamsEmp.SbDirEvePedEPECClick(Sender: TObject);
begin
  MyObjects.DefineDiretorio(Self, EdDirEvePedEPEC);
end;

procedure TFmParamsEmp.SbDirEvePedMDeClick(Sender: TObject);
begin
  MyObjects.DefineDiretorio(Self, EdDirEvePedMDe);
end;

procedure TFmParamsEmp.SbDirEveRetLotClick(Sender: TObject);
begin
  MyObjects.DefineDiretorio(Self, EdDirEveRetLot);
end;

procedure TFmParamsEmp.SbDirEveRetMDeClick(Sender: TObject);
begin
  MyObjects.DefineDiretorio(Self, EdDirEveRetMDe);
end;

procedure TFmParamsEmp.SBDirPedRecClick(Sender: TObject);
begin
  MyObjects.DefineDiretorio(Self, EdDirPedRec);
end;

procedure TFmParamsEmp.SBDirProRecClick(Sender: TObject);
begin
  MyObjects.DefineDiretorio(Self, EdDirProRec);
end;

procedure TFmParamsEmp.SBDirNfeAssClick(Sender: TObject);
begin
  MyObjects.DefineDiretorio(Self, EdDirNFeAss);
end;

procedure TFmParamsEmp.SBDirNFeGerClick(Sender: TObject);
begin
  MyObjects.DefineDiretorio(Self, EdDirNFeGer);
end;

procedure TFmParamsEmp.SbDirNFSeNFSAutClick(Sender: TObject);
begin
  MyObjects.DefineDiretorio(Self, EdDirNFSeNFSAut);
end;

procedure TFmParamsEmp.SbDirNFSeNFSCanClick(Sender: TObject);
begin
  MyObjects.DefineDiretorio(Self, EdDirNFSeNFSCan);
end;

procedure TFmParamsEmp.SbDirNFSeDPSAssClick(Sender: TObject);
begin
  MyObjects.DefineDiretorio(Self, EdDirNFSeDPSAss);
end;

procedure TFmParamsEmp.SbDirNFSeDPSGerClick(Sender: TObject);
begin
  MyObjects.DefineDiretorio(Self, EdDirNFSeDPSGer);
end;

procedure TFmParamsEmp.SbDirNFSeIniFilesClick(Sender: TObject);
const
  ACBrIniFiles = 'C:\Dermatek\ACBr\ACBrDFe\ACBrNFSe\Delphi\Ini';
begin
  ForceDirectories(ACBrIniFiles);
  //
  if EdDirNFSeIniFiles.Text = '' then
    EdDirNFSeIniFiles.Text  := ACBrIniFiles;
  //
  MyObjects.DefineDiretorio(Self, EdDirNFSeIniFiles);
end;

procedure TFmParamsEmp.SbDirNFSeLogsClick(Sender: TObject);
begin
  if not DefinirCaminhoSeVazio_CNPJ(EdDirNFSeLogs, 'C:\Dermatek\NFSe', 'XML') then
    MyObjects.DefineDiretorio(Self, EdDirNFSeLogs);
end;

procedure TFmParamsEmp.SbDirNFSeRPSEnvLotClick(Sender: TObject);
begin
  MyObjects.DefineDiretorio(Self, EdDirNFSeRPSEnvLot);
end;

procedure TFmParamsEmp.SbDirNFSeRPSRecLotClick(Sender: TObject);
begin
  MyObjects.DefineDiretorio(Self, EdDirNFSeRPSRecLot);
end;

procedure TFmParamsEmp.SbDirNFSeSchemaClick(Sender: TObject);
begin
  if not DefinirCaminhoSeVazio_CNPJ(EdDirNFSeSchema, 'C:\Dermatek\NFSe', 'Schema') then
    MyObjects.DefineDiretorio(Self, EdDirNFSeSchema);
end;

procedure TFmParamsEmp.SBDirPedSitClick(Sender: TObject);
begin
  MyObjects.DefineDiretorio(Self, EdDirPedSit);
end;

procedure TFmParamsEmp.SBDirSitClick(Sender: TObject);
begin
  MyObjects.DefineDiretorio(Self, EdDirSit);
end;

procedure TFmParamsEmp.SBDirPedStaClick(Sender: TObject);
begin
  MyObjects.DefineDiretorio(Self, EdDirPedSta);
end;

procedure TFmParamsEmp.SbDirProcCCeClick(Sender: TObject);
begin
  MyObjects.DefineDiretorio(Self, EdDirEveProcCCe);
end;

procedure TFmParamsEmp.SBDirStaClick(Sender: TObject);
begin
  MyObjects.DefineDiretorio(Self, EdDirSta);
end;

procedure TFmParamsEmp.SbEdNFSeTipCtoMailClick(Sender: TObject);
begin
  MostraFmEntiTipCto(EdNFSeTipCtoMail, CBNFSeTipCtoMail, QrEntiTipCt2);
end;

procedure TFmParamsEmp.SbImprimeClick(Sender: TObject);
begin
//
end;

procedure TFmParamsEmp.SBModeloNFClick(Sender: TObject);
begin
{$IFDEF FmImprime}
  VAR_IMPRIMEFMT := 0;
  if DBCheck.CriaFm(TFmImprime, FmImprime, afmoNegarComAviso) then
  begin
    FmImprime.ShowModal;
    FmImprime.Destroy;
    if (VAR_IMPRIMEFMT <> 0) and (VAR_IMPRIMETYP = 1) then
    begin
      QrImprime.Close;
      UMyMod.AbreQuery(QrImprime, Dmod.MyDB, 'TFmParamsEmp.SBModeloNFClick()');
      //
      EdAssocModNF.ValueVariant := VAR_IMPRIMEFMT;
      CBAssocModNF.KeyValue := VAR_IMPRIMEFMT;
    end;
  end;
{$ELSE}
  Geral.MB_Info(
  'O formul�rio "FmImprime" n�o est� habilitado para este aplicativo.' +
  sLineBreak + 'AVISE A DERMATEK!');
{$ENDIF}
end;

procedure TFmParamsEmp.SbNFSeCertDigitalClick(Sender: TObject);
begin
{$IfNDef sNFSe}
  if DBCheck.CriaFm(TFmCapicomListas, FmCapicomListas, afmoSoBoss) then
  begin
    FmCapicomListas.ShowModal;
    if FmCapicomListas.FSelected then
    begin
      EdNFSeCertDigital.Text := FmCapicomListas.FSerialNumber;
      TPNFSeCertValidad.Date := FmCapicomListas.FValidToDate;
      if EdNFSeCertAviExpi.ValueVariant < 30 then
        EdNFSeCertAviExpi.ValueVariant := 30;
    end;
    FmCapicomListas.Destroy;
  end;
{$EndIf}
end;

procedure TFmParamsEmp.SbNFSeLogoFiliClick(Sender: TObject);
begin
  MyObjects.DefineArquivo1(Self, EdNFSeLogoFili);
end;

procedure TFmParamsEmp.SbNFSeLogoPrefClick(Sender: TObject);
begin
  MyObjects.DefineArquivo1(Self, EdNFSeLogoPref);
end;

procedure TFmParamsEmp.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmParamsEmp.SbNovoClick(Sender: TObject);
begin
  //LaRegistro.Caption := GOTOy.CodUsu(QrParamsEmpCodUsu.Value, LaRegistro.Caption);
end;

procedure TFmParamsEmp.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
  try
    FreeAndNil(ACBrNFe0);
    FreeAndNil(ACBrIntegrador1);
  except
    // Nada
  end;
end;

procedure TFmParamsEmp.QrParamsEmpAfterOpen(DataSet: TDataSet);
begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrPrmsEmpMis, Dmod.MyDB, [
    'SELECT Situacao, ',
    'TZD_UTC_Auto_UsuarioAceite, ',
    'TZD_UTC_Auto_TermoAceite, ',
    'TZD_UTC_Auto_DataHoraAceite ',
    'FROM paramsemp ',
    'WHERE Codigo=' + Geral.FF0(QrParamsEmpCodigo.Value),
    '']);
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrPrmsEmpNFe, Dmod.MyDB, [
    'SELECT ',
{$IfNDef NO_USE_EMAILDMK}
      'pe1.Nome NO_PREMAILAUT, ',
      'pe2.Nome NO_PREMAILCAN, ',
      'pe3.Nome NO_PREMAILEVECCE, ',
{$Else}
      '"" NO_PREMAILAUT, ',
      '"" NO_PREMAILCAN, ',
      '"" NO_PREMAILEVECCE, ',
{$EndIf}
      'pem.PreMailAut, ',
      'pem.PreMailCan, ',
      'pem.PreMailEveCCe, ',
      'pem.DirNFeProt, ',
      'pem.DirNFeRWeb, ',
      'pem.SCAN_Ser, ',
      'pem.SCAN_nNF, ',
      'pem.infRespTec_xContato, ',
      'pem.infRespTec_CNPJ, ',
      'pem.infRespTec_fone, ',
      'pem.infRespTec_email, ',
      'pem.infRespTec_idCSRT, ',
      'pem.infRespTec_CSRT, ',
      'pem.infRespTec_Usa, ',
      'pem.UF_WebServ, ',
      'pem.SiglaCustm, ',
      'pem.UF_Servico, ',
      'pem.SimplesFed, ',
      'pem.InfoPerCuz, ',
      'pem.NFeSerNum, ',
      'pem.NFeSerVal, ',
      'pem.NFeSerAvi, ',
      'pem.UF_MDeMDe, ',
      'pem.UF_MDeDes, ',
      'pem.UF_MDeNFe, ',
      'pem.NFeUF_EPEC, ',
      'pem.DirEnvLot, ',
      'pem.DirRec, ',
      'pem.DirProRec, ',
      'pem.DirNFeAss, ',
      'pem.DirNFeGer, ',
      'pem.DirDen, ',
      'pem.DirPedRec, ',
      'pem.DirPedSta, ',
      'pem.DirSta, ',
      'pem.DirPedCan, ',
      'pem.DirCan, ',
      'pem.DirPedInu, ',
      'pem.DirInu, ',
      'pem.DirPedSit, ',
      'pem.DirSit, ',
      'pem.DirEveEnvLot, ',
      'pem.DirEveRetLot, ',
      'pem.DirEvePedCCe, ',
      'pem.DirEveRetCCe, ',
      'pem.DirEvePedCan, ',
      'pem.DirEveRetCan, ',
      'pem.DirEvePedEPEC, ',
      'pem.DirEveRetEPEC, ',
      'pem.DirEveProcCCe, ',
      'pem.DirRetNfeDes, ',
      'pem.DirEvePedMDe, ',
      'pem.DirEveRetMDe, ',
      'pem.DirDowNFeDes, ',
      'pem.DirDowNFeNFe, ',
      'pem.DirDistDFeInt, ',
      'pem.DirRetDistDFeInt, ',
      'pem.DirDowNFeCnf, ',
      'pem.versao, ',
      'pem.ide_mod, ',
      'pem.ide_tpAmb, ',
      'pem.DirSchema, ',
      'pem.NT2018_05v120, ',
      'pem.AppCode, ',
      'pem.AssDigMode, ',
      'pem.MyEmailNFe, ',
      'pem.NFeInfCpl, ',
      'pem.CRT, ',
      'pem.CSOSN, ',
      'pem.pCredSNAlq, ',
      'pem.NFeVerStaSer, ',
      'pem.NFeVerEnvLot, ',
      'pem.NFeVerConLot, ',
      'pem.NFeVerCanNFe, ',
      'pem.NFeVerInuNum, ',
      'pem.NFeVerConNFe, ',
      'pem.NFeVerLotEve, ',
      'pem.NFeVerConsCad, ',
      'pem.NFeVerDistDFeInt, ',
      'pem.NFeVerDowNFe, ',
      'pem.ide_tpImp, ',
      'pem.NFeItsLin, ',
      'pem.NFeFTRazao, ',
      'pem.NFeFTEnder, ',
      'pem.NFeFTFones, ',
      'pem.NFeMaiusc, ',
      'pem.NFeShowURL, ',
      'pem.PathLogoNF, ',
      'pem.DirDANFEs, ',
      'pem.NFe_indFinalCpl, ',
      'pem.NoDANFEMail, ',
      'pem.DirRetNfeDes, ',
      'pem.DirEvePedMDe, ',
      'pem.DirEveRetMDe, ',
      'pem.DirDowNFeDes, ',
      'pem.DirDowNFeNFe, ',
      'pem.DirDistDFeInt, ',
      'pem.DirRetDistDFeInt, ',
      'pem.DirDowNFeCnf, ',
      'pem.DirSchema, ',
      'pem.TipoPrintNFCe, ',
      'pem.NomePrintNFCe, ',
      'pem.IPPrintNFCe, ',
      'pem.AutoCutNFCe,',
      'pem.CertDigPfxCam,',
      'pem.CertDigPfxPwd, ',
      'pem.CertDigPfxWay, ',
      'pem.NFeUsoCEST, ',
      //
      'pem.UC_ICMS_CST_B, ',
      'pem.UC_IPI_CST, ',
      'pem.UC_PIS_CST, ',
      'pem.UC_COFINS_CST, ',
      //
      'pem.NFeNT2013_003LTT ',
      'FROM paramsemp pem',
{$IfNDef NO_USE_EMAILDMK}
      'LEFT JOIN preemail pe1 ON pe1.Codigo=pem.PreMailAut ',
      'LEFT JOIN preemail pe2 ON pe2.Codigo=pem.PreMailCan ',
      'LEFT JOIN preemail pe3 ON pe3.Codigo=pem.PreMailEveCCe ',
{$EndIf}

      'WHERE pem.Codigo=' + Geral.FF0(QrParamsEmpCodigo.Value),
    '']);
  //
  QueryPrincipalAfterOpen;
  //BtFilial.Enabled          := QrParamsEmp.RecordCount > 0;
  BtSerieNF.Enabled         := QrParamsEmp.RecordCount > 0;
  BtSerieNFC.Enabled        := QrParamsEmp.RecordCount > 0;
  BtSerieCTe.Enabled        := QrParamsEmp.RecordCount > 0;
  BtSerieMDFe.Enabled       := QrParamsEmp.RecordCount > 0;
  BtNFeDefaultDirs.Enabled  := QrParamsEmp.RecordCount > 0;
  BtCTeDefaultDirs.Enabled  := QrParamsEmp.RecordCount > 0;
  BtMDFeDefaultDirs.Enabled := QrParamsEmp.RecordCount > 0;
  BtSugereDirNFSe.Enabled   := QrParamsEmp.RecordCount > 0;
end;

procedure TFmParamsEmp.QrParamsEmpAfterScroll(DataSet: TDataSet);
begin
  ReopenParamsNFs(0);
  ReopenParamsNFCs(0);
  ReopenParamsCTe(0);
  ReopenParamsMDFe(0);
{$IfNDef NO_FINANCEIRO}
  MeCSOSN_Show.Text := UFinanceiro.CSOSN_Get(QrPrmsEmpNFeCRT.Value, QrPrmsEmpNFeCSOSN.Value);
  EdDBSPED_EFD_IND_ATIV.Text := UFinanceiro.SPED_EFD_IND_ATIV_Get(QrParamsEmpSPED_EFD_IND_ATIV.Value);
  EdDBSPED_EFD_IND_PERFIL.Text := UFinanceiro.SPED_EFD_IND_PERFIL_Get(QrParamsEmpSPED_EFD_IND_PERFIL.Value);
  {$EndIf}
  {$IfNDef sNFSe}
  UnDmkDAC_PF.AbreMySQLQuery0(QrMyMetodo, DModG.AllID_DB, [
  'SELECT Nome ',
  'FROM mynfsemetodos ',
  'WHERE Codigo=' + Geral.FF0(QrParamsEmpNFSeMetodo.Value),
  '']);
  {$EndIf}
end;

procedure TFmParamsEmp.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmParamsEmp.SbQueryClick(Sender: TObject);
begin
  LocCod(QrParamsEmpCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'ParamsEmp', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmParamsEmp.sbtnGetCertClick(Sender: TObject);
begin
  edtNumSerie.Text := ACBrNFe0.SSL.SelecionarCertificado;
end;

procedure TFmParamsEmp.sbtnNumSerieClick(Sender: TObject);
begin
 // ini Marco
 // for�ar carregamento do certificado vigente
 ACBrNFe0.SSL.CertNumeroSerie;
 // fim marco
 {$ifNDef sACBr}
 DmkACBr_ParamsEmp.NumSerie(ACBrNFe0);
{$Endif}
end;

procedure TFmParamsEmp.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmParamsEmp.FormShow(Sender: TObject);
begin
  FCriado := True;
end;

procedure TFmParamsEmp.GerenciaCertificadoDigital(Acao: TSQLType);
(*  procedure AdicionaCertificado;
  var
    FStore : IStore3; //Store - Reposit�rio de Certificados
    FCert : ICertificate2; // Certificado.
  begin
    //instanciando os componentes.
    Fstore := CoStore.Create;
    Fcert := CoCertificate.Create;
    try
      //abrindo o reposit�rio de certificados do windows, no caso o usu�rio corrente.
      Fstore.Open(CAPICOM_CURRENT_USER_STORE, 'My', CAPICOM_STORE_OPEN_MAXIMUM_ALLOWED);

      //carregando o certificado
      FCert.Load('c:\certificado.pfx','senha',CAPICOM_KEY_STORAGE_DEFAULT,CAPICOM_CURRENT_USER_KEY);

      //adiciona o certificado carregado ao reposit�rio.
      FStore.Add(FCert);

    finally
    //fecha o reposit�rio.
    Fstore.Close;
    end;
  end;

 //a mesma coisa do adiciona, por�m ele usa remove ao inv�s de add, para remover o certificado do windows.
 procedure RemoveCertificado;
 var
 FStore : IStore3;
 FCert : ICertificate2;
 begin
 Fstore := CoStore.Create;
 Fcert := CoCertificate.Create;
 try
 Fstore.Open(CAPICOM_CURRENT_USER_STORE, 'My', CAPICOM_STORE_OPEN_MAXIMUM_ALLOWED);

FCert.Load('c:\certificado.pfx','senha',CAPICOM_KEY_STORAGE_DEFAULT,CAPICOM_CURRENT_USER_KEY);

FStore.Remove(FCert);

 finally
 FStore.Close;
 end;
 end;
*)
const
  Dir = 'C:\Dermatek\';
  //
  Casas = 2;
  LeftZeros = 0;
  ValMin = '';
  ValMax = '';
  Obrigatorio = False;
  FormCaption = 'Senha do certificado Digital';
  ValCaption = 'Informe a senha constante no arquivo:';
  WidthVal = 400;
var
  Res: Variant;
  Arquivo, Senha: String;
  {$IfNDef SemNFe_0000}
var
  FStore : IStore3; //Store - Reposit�rio de Certificados
  FCert : ICertificate2; // Certificado.
  {$EndIf}
begin
  {$IfNDef SemNFe_0000}
  Arquivo := '';
  if MyObjects.FileOpenDialog(Self, Dir, Arquivo,
  'Informe o arquivo que cont�m o certifiacado digital', '', [], Arquivo) then
  begin
    Senha := '';
    if MyObjects.GetValorDmk(TFmGetValor, FmGetValor, dmktfString, Senha,
    Casas, LeftZeros, ValMin, ValMax, Obrigatorio, FormCaption, ValCaption,
    WidthVal, Res) then
    begin
      Senha := String(Res);
      //
      //instanciando os componentes.
      Fstore := CoStore.Create;
      Fcert := CoCertificate.Create;
      try
        //abrindo o reposit�rio de certificados do windows, no caso o usu�rio corrente.
        Fstore.Open(CAPICOM_CURRENT_USER_STORE, 'My', CAPICOM_STORE_OPEN_MAXIMUM_ALLOWED);

        //carregando o certificado
        FCert.Load(Arquivo, Senha, CAPICOM_KEY_STORAGE_DEFAULT,CAPICOM_CURRENT_USER_KEY);

        //adiciona ou remove o certificado carregado ao reposit�rio.
        case Acao of
          stIns: FStore.Add(FCert);
          stDel: FStore.Add(FCert);
          else Geral.MB_ERRO(
          'A��o n�o definida para com o certificado selecionado!' + sLineBreak +
          'Informe a DERMATEK!');
        end;
      finally
      //fecha o reposit�rio.
      Fstore.Close;
      end;
    end;
  end;
  {$EndIf}
end;

procedure TFmParamsEmp.IncluinovasriedeCTe1Click(Sender: TObject);
begin
  {$IfDef ComCTe_0000}
  UmyMod.FormInsUpd_Cria(TFmParamsCTe, FmParamsCTe, afmoNegarComAviso,
    QrParamsCTe, stIns);
  FmParamsCTe.EdMaxSeqLib.ValueVariant := 999999999;
  FmParamsCTe.RGIncSeqAuto.ItemIndex   := 1;
  FmParamsCTe.ShowModal;
  FmParamsCTe.Destroy;
  {$EndIf}
end;

procedure TFmParamsEmp.Incluinovasriedecupomfiscal1Click(Sender: TObject);
begin
{$IfNDef semNFCe_0000}
  UmyMod.FormInsUpd_Show(TFmParamsNFCs, FmParamsNFCs, afmoNegarComAviso,
    QrParamsNFCs, stIns);
  {$EndIf}
end;

procedure TFmParamsEmp.IncluinovasriedeMDFe1Click(Sender: TObject);
begin
  {$IfDef ComMDFe_0000}
  UmyMod.FormInsUpd_Cria(TFmParamsMDFe, FmParamsMDFe, afmoNegarComAviso,
    QrParamsMDFe, stIns);
  FmParamsMDFe.EdMaxSeqLib.ValueVariant := 999999999;
  FmParamsMDFe.RGIncSeqAuto.ItemIndex   := 1;
  FmParamsMDFe.ShowModal;
  FmParamsMDFe.Destroy;
  {$EndIf}
end;

procedure TFmParamsEmp.Incluinovasriedenotafiscal1Click(Sender: TObject);
begin
  {$IfNDef SemNFe_0000}
  UmyMod.FormInsUpd_Show(TFmParamsNFs, FmParamsNFs, afmoNegarComAviso,
    QrParamsNFs, stIns);
  {$EndIf}
end;

procedure TFmParamsEmp.Incluinovocertificadoaorepositriodosistemaoperacional1Click(
  Sender: TObject);
begin
  GerenciaCertificadoDigital(stIns);
end;

procedure TFmParamsEmp.QrParamsEmpBeforeClose(DataSet: TDataSet);
begin
  QrPrmsEmpMis.Close;
  QrPrmsEmpNFe.Close;
  //
  QrParamsNFs.Close;
  QrParamsCTe.Close;
  //BtFilial.Enabled := False;
  BtSerieNF.Enabled := False;
  BtSerieNFC.Enabled := False;
  BtSerieCTe.Enabled := False;
  BtSerieMDFe.Enabled := False;
  {$IfNDef sNFSe}
  QrMyMetodo.Close;
  {$EndIf}
end;

procedure TFmParamsEmp.QrParamsEmpBeforeOpen(DataSet: TDataSet);
begin
  QrParamsEmpCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmParamsEmp.QrParamsEmpCalcFields(DataSet: TDataSet);
begin
  QrParamsEmppCredSNMez_TXT.Value := Geral.FDT_AAAAMM(QrParamsEmppCredSNMez.Value, 14);
  //
  QrParamsEmpTZD_UTC_Str.Value :=
    dmkPF.TZD_UTC_FloatToSignedStr(QrParamsEmpTZD_UTC.Value);
end;

 { TODO 4 -cCTe : Criar itens de indefinido nos radiogroup do faturamento}
 { TODO 5 -cCTe : Verificar se a guia faturamento esta configurada}
 { TODO 0 -cCTe : Rajustar visibles das guias NFe NFSe e CTe}

end.

