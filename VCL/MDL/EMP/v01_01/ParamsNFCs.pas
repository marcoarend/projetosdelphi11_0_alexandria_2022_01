unit ParamsNFCs;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, DBCtrls,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkLabel, Mask, dmkDBEdit, dmkGeral,
  Variants, dmkValUsu, dmkRadioGroup, dmkImage, UnDmkEnums;

type
  TFmParamsNFCs = class(TForm)
    Panel1: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    QrPesqPrc: TmySQLQuery;
    QrPesqPrcPreco: TFloatField;
    Panel3: TPanel;
    DBEdCodigo: TdmkDBEdit;
    DBEdit1: TDBEdit;
    DBEdNome: TDBEdit;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    EdSequencial: TdmkEdit;
    EdSerieNF: TdmkEdit;
    Label6: TLabel;
    EdControle: TdmkEdit;
    RGIncSeqAuto: TdmkRadioGroup;
    EdMaxSeqLib: TdmkEdit;
    Label7: TLabel;
    Panel4: TPanel;
    CkContinuar: TCheckBox;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel2: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel5: TPanel;
    BtOK: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    procedure AtualizaImprimeEmpr(Empresa, ParamsNFC: Integer);
  public
    { Public declarations }
  end;

  var
  FmParamsNFCs: TFmParamsNFCs;

implementation

uses UnMyObjects, Module, ParamsEmp, UMySQLModule, DmkDAC_PF;

{$R *.DFM}

procedure TFmParamsNFCs.AtualizaImprimeEmpr(Empresa, ParamsNFC: Integer);
var
  Qry, Qry2, QryUpd: TmySQLQuery;
  Codigo, Controle: Integer;
begin
  if MyObjects.FIC(Empresa = 0, nil, 'Empresa n�o definida na procedure "TFmParamsNFCs.AtualizaImprimeEmpr"!') then Exit;
  if MyObjects.FIC(ParamsNFC = 0, nil, 'ParamsNFC n�o definido na procedure "TFmParamsNFCs.AtualizaImprimeEmpr"!') then Exit;
  //
  Qry    := TmySQLQuery.Create(Dmod);
  Qry2   := TmySQLQuery.Create(Dmod);
  QryUpd := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SHOW tables LIKE "imprimeempr" ',
      '']);
    if Qry.RecordCount > 0 then
    begin
      //Apenas NF-e
      UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
        'SELECT Codigo ',
        'FROM imprime ',
        'WHERE TipoImpressao = 4 ',
        '']);
      if Qry.RecordCount > 0 then
      begin
        while not Qry.Eof do
        begin
          Codigo := Qry.FieldByName('Codigo').Value;
          //
          UnDmkDAC_PF.AbreMySQLQuery0(Qry2, Dmod.MyDB, [
            'SELECT Controle, ParamsNFCs ',
            'FROM imprimeempr ',
            'WHERE Empresa=' + Geral.FF0(Empresa),
            'AND ParamsNFCs=' + Geral.FF0(ParamsNFC),
            'OR ParamsNFCs=0', // 2020-10-26
            '']);
          if Qry2.RecordCount = 0 then
          begin
            //N�o achou inclui
            Controle := UMyMod.BuscaEmLivreY_Def('imprimeempr', 'Controle', stIns, 0);
            //
            UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'imprimeempr', False,
              ['Codigo', 'Empresa', 'ParamsNFCs'], ['Controle'],
              [Codigo, Empresa, ParamsNFC], [Controle], True);
          end
          else if Qry2.FieldByName('ParamsNFCs').AsInteger = 0 then
          begin
            //N�o achou inclui
            Controle := Qry2.FieldByName('Controle').AsInteger;
            //
            UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'imprimeempr', False,
              ['Codigo', 'Empresa', 'ParamsNFCs'], ['Controle'],
              [Codigo, Empresa, ParamsNFC], [Controle], True);
          end;
          //
          Qry.Next;
        end;
      end;
    end;
  finally
    Qry.Free;
    Qry2.Free;
    QryUpd.Free;
  end;
end;

procedure TFmParamsNFCs.BtOKClick(Sender: TObject);
var
  Controle, SerieNF: Integer;
begin

  SerieNF := EdSerieNF.ValueVariant;
  //
  if MyObjects.FIC(SerieNF = 0, EdSerieNF, 'Informe a s�rie da nota fiscal!') then Exit;
  if MyObjects.FIC(SerieNF >= 900, EdSerieNF, 'A serie deve ser inferior a 900!' +
    sLineBreak + 'Para cadastrar a s�rie para notas em conting�ncia utilize o campo correto!')
  then
    Exit;
  //
  Controle := EdControle.ValueVariant;
  //Controle := UMyMod.BuscaEmLivreY_Def('paramsnfs', 'Controle', ImgTipo.SQLType, Controle);
  Controle := UMyMod.BPGS1I32('paramsnfcs', 'Controle',  '', '', tsPos, ImgTipo.SQLType, Controle);
  //
  EdControle.ValueVariant := Controle;
  //
  if UMyMod.ExecSQLInsUpdFm(FmParamsNFCs, ImgTipo.SQLType, 'ParamsNFCs', Controle,
    Dmod.QrUpd) then
  begin
    AtualizaImprimeEmpr(FmParamsEmp.QrParamsEmpCodigo.Value, Controle);
    //
    FmParamsEmp.ReopenParamsNFCs(Controle);
    if CkContinuar.Checked then
    begin
      Geral.MB_Info(DmkEnums.NomeTipoSQL(ImgTipo.SQLType) +
        ' de S�rie de Nota Fiscal realizado com sucesso!');
      //
      ImgTipo.SQLType             := stIns;
      EdSerieNF.Text             := '';
      EdSequencial.ValueVariant  := 0;
      EdControle.ValueVariant    := 0;
      EdSerieNF.SetFocus;
    end else
      Close;
  end;
end;

procedure TFmParamsNFCs.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmParamsNFCs.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //
  DBEdCodigo.DataSource := FmParamsEmp.DsParamsEmp;
  DBEdit1.DataSource    := FmParamsEmp.DsParamsEmp;
  DBEdNome.DataSource   := FmParamsEmp.DsParamsEmp;
end;

procedure TFmParamsNFCs.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
end;

procedure TFmParamsNFCs.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
    [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmParamsNFCs.FormShow(Sender: TObject);
begin
  if ImgTipo.SQLType = stIns then
  begin
    EdMaxSeqLib.ValueVariant := 999999999;
    RGIncSeqAuto.ItemIndex   := 1;
    CkContinuar.Visible      := True;
    CkContinuar.Checked      := False;
  end else
  if ImgTipo.SQLType = stUpd then
  begin
    CkContinuar.Visible := False;
    CkContinuar.Checked := False;
  end;
end;

end.
