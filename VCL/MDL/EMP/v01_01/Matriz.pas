unit Matriz;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls, Db, (*DBTables,*) JPEG, ExtDlgs,
  ZCF2, ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, ComCtrls, mySQLDbTables, dmkGeral,
  Grids, DBGrids, dmkDBGrid, Menus, dmkEdit, dmkCheckBox, dmkImage, DmkDAC_PF,
  UnDmkEnums;

type
  TFmMatriz = class(TForm)
    QrEmpresa: TmySQLQuery;
    DsEmpresa: TDataSource;
    QrEmpresaCNPJ_TXT: TWideStringField;
    TbTerminais: TmySQLTable;
    DsTerminais: TDataSource;
    QrEmpresaMonitorar: TSmallintField;
    QrEmpresaMasLogin: TWideStringField;
    QrEmpresaMasSenha: TWideStringField;
    QrEmpresaMasAtivar: TWideStringField;
    QrEmpresaSolicitaSenha: TSmallintField;
    QrEmpresaLicenca: TWideStringField;
    QrEmpresaDistorcao: TIntegerField;
    QrEmpresaDataI: TDateField;
    QrEmpresaDataF: TDateField;
    QrEmpresaHoje: TDateField;
    QrEmpresaHora: TTimeField;
    QrEmpresaLimite: TSmallintField;
    QrFiliais: TmySQLQuery;
    DsFiliais: TDataSource;
    QrFiliaisCodigo: TIntegerField;
    QrFiliaisFilial: TIntegerField;
    QrFiliaisNOMEENT: TWideStringField;
    QrFiliaisCNPJCPF: TWideStringField;
    QrFiliaisIERG: TWideStringField;
    QrFiliaisFormaSociet: TWideStringField;
    QrFiliaisSimples: TSmallintField;
    QrFiliaisAtividade: TWideStringField;
    PMFilial: TPopupMenu;
    Incluinovafilial1: TMenuItem;
    Alterafilialatual1: TMenuItem;
    Excluifilial1: TMenuItem;
    QrFiliaisCNPJCPF_TXT: TWideStringField;
    QrErrFil: TmySQLQuery;
    QrErrFilFilial: TIntegerField;
    QrErrFilCodigo: TIntegerField;
    QrErrFilItens: TLargeintField;
    AJustaCodigodeclienteinterno1: TMenuItem;
    Alteraoutrsdadosdafilialatual1: TMenuItem;
    QrEmpresaUsaAccMngr: TSmallintField;
    QrEmpresaDECR_SNHA: TWideStringField;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    Panel3: TPanel;
    DBGrid1: TDBGrid;
    TabSheet3: TTabSheet;
    dmkDBGrid1: TdmkDBGrid;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBCadastroDados: TGroupBox;
    Panel7: TPanel;
    Label1: TLabel;
    Label9: TLabel;
    Label3: TLabel;
    Label7: TLabel;
    DBRazao: TDBEdit;
    DBCNPJ: TDBEdit;
    DBLogin: TDBEdit;
    DBSenha: TDBEdit;
    DBCheckBox1: TDBCheckBox;
    GBCadastroEdita: TGroupBox;
    PainelDados2: TPanel;
    Label13: TLabel;
    Label15: TLabel;
    Label19: TLabel;
    Label21: TLabel;
    EdRazao: TdmkEdit;
    EdCNPJ: TdmkEdit;
    EdLogin: TdmkEdit;
    CBUsaAccMngr: TdmkCheckBox;
    EdSenha: TEdit;
    GBAvisos1: TGroupBox;
    Panel14: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBConfirma: TGroupBox;
    PnSaiDesis: TPanel;
    BtDesiste: TBitBtn;
    Panel15: TPanel;
    BtConfirma: TBitBtn;
    GBControle: TGroupBox;
    Panel16: TPanel;
    Panel17: TPanel;
    BtSaida: TBitBtn;
    BtAltera: TBitBtn;
    BtDefine: TBitBtn;
    BtVerifiTerceiros: TBitBtn;
    GBFiliais: TGroupBox;
    Panel1: TPanel;
    Panel4: TPanel;
    BtFilial: TBitBtn;
    BtSaida2: TBitBtn;
    BtUsuario: TBitBtn;
    BtRelMtrz: TBitBtn;
    N1: TMenuItem;
    N2: TMenuItem;
    N3: TMenuItem;
    EntidadeA1: TMenuItem;
    EntidadeB1: TMenuItem;
    QrEmpresaentCNPJ: TWideStringField;
    QrEmpresaentEm: TWideStringField;
    EdSenhaAdmin: TEdit;
    LaSenhaAdmin: TLabel;
    SpeedButton1: TSpeedButton;
    QrEmpresaDECR_ADMIN: TWideStringField;
    DBEdit1: TDBEdit;
    Label2: TLabel;
    procedure BtAlteraClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormActivate(Sender: TObject);
    procedure QrEmpresaCalcFields(DataSet: TDataSet);
    procedure FormResize(Sender: TObject);
    procedure Incluinovafilial1Click(Sender: TObject);
    procedure QrFiliaisAfterScroll(DataSet: TDataSet);
    procedure QrFiliaisCalcFields(DataSet: TDataSet);
    procedure BtFilialClick(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure AJustaCodigodeclienteinterno1Click(Sender: TObject);
    procedure BtDefineClick(Sender: TObject);
    procedure BtVerifiTerceirosClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure PageControl1Change(Sender: TObject);
    procedure BtRelMtrzClick(Sender: TObject);
    procedure BtUsuarioClick(Sender: TObject);
    procedure PMFilialPopup(Sender: TObject);
    procedure EntidadeA1Click(Sender: TObject);
    procedure Alteraoutrsdadosdafilialatual1Click(Sender: TObject);
    procedure EntidadeB1Click(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
  private
    procedure CriaOForm;
   ////Procedures do form
    procedure MostraEdicao(Mostra: Boolean; SQLType: TSQLType; Codigo: Integer);
    procedure DefParams;
    function  ReopenFiliais(Codigo: Integer): Boolean;
    procedure ReopenEntidade2(Codigo: Integer);
  public
    { Public declarations }
  end;

var
  FmMatriz: TFmMatriz;

implementation

uses
  {$IFNDef SemEntidade}Entidade2, EntiCliInt, {$EndIf}
  {$IFNDef NAO_USA_TEXTOS}Textos, {$EndIf}
  {$IFNDef SemEntiCfgRel}EntiCfgRel, EntiCfgRel_01, {$EndIf}
  {$IFNDef SemBDTErceiros}VerifiDBTerceiros, {$EndIf}
  UnMyObjects, Module, SelCod, MyDBCheck, MyListas,
  ModuleGeral, FilialUsrs;

{$R *.DFM}

procedure TFmMatriz.DefParams;
begin
  VAR_GOTOTABELA := 'Master';
  VAR_GOTOmySQLTABLE := QrEmpresa;
  VAR_GOTONEG := gotoPiZ;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;
  VAR_SQLx.Add('SELECT AES_DECRYPT(m.MasSenha, "' + CO_USERSPNOW + '") DECR_SNHA, ');
  VAR_SQLx.Add('AES_DECRYPT(m.SenhaAdmin, "' + CO_USERSPNOW + '") DECR_ADMIN, m.*');
  VAR_SQLx.Add('FROM master m');
(*
  VAR_SQLx.Add('SELECT *');
  VAR_SQLx.Add('FROM empresa');
*)
  //
//  VAR_SQL1.Add('AND Codigo=:P0');
  //
//  VAR_SQLa.Add('AND Nome Like :P0');
  //
  QrEmpresa.Close;
  QrEmpresa.SQL.Clear;
  QrEmpresa.SQL.Add('SELECT m.*, AES_DECRYPT(m.MasSenha, "' + CO_USERSPNOW + '") DECR_SNHA, ');
  QrEmpresa.SQL.Add('AES_DECRYPT(m.SenhaAdmin, "' + CO_USERSPNOW + '") DECR_ADMIN, ');
  QrEmpresa.SQL.Add('IF(e.Tipo = 0, e.RazaoSocial, e.Nome) entEm, ');
  QrEmpresa.SQL.Add('IF(e.Tipo = 0, e.CNPJ, e.CPF) entCNPJ ');
  QrEmpresa.SQL.Add('FROM master m');
  QrEmpresa.SQL.Add('LEFT JOIN controle c ON c.CNPJ = m.CNPJ ');
  QrEmpresa.SQL.Add('LEFT JOIN entidades e ON e.Codigo = c.Dono ');
  UnDmkDAC_PF.AbreQuery(QrEmpresa, Dmod.MyDB);
  //Geral.MB_SQL(self, QrEmpresa);
end;

procedure TFmMatriz.EntidadeA1Click(Sender: TObject);
begin
  {$IfNDef SemEntidade}
  DModG.CadastroDeEntidade(QrFiliaisCodigo.Value, fmcadEntidades, fmcadEntidades, True);
  ReopenFiliais(QrFiliaisCodigo.Value);
  DModG.AjustaFiliaisCliInt();
  {$EndIf}
end;

procedure TFmMatriz.EntidadeB1Click(Sender: TObject);
begin
  {$IfNDef SemEntidade}
  DModG.CadastroDeEntidade(QrFiliaisCodigo.Value, fmcadEntidade2, fmcadEntidade2, True);
  ReopenFiliais(QrFiliaisCodigo.Value);
  DModG.AjustaFiliaisCliInt();
  {$EndIf}
end;

procedure TFmMatriz.MostraEdicao(Mostra: Boolean; SQLType: TSQLType; Codigo: Integer);
begin
  if Mostra then
  begin
    UnDmkDAC_PF.AbreQueryApenas(QrEmpresa);
    //
    GBControle.Visible   := False;
    TabSheet2.TabVisible := False; //Aba terminais
    TabSheet3.TabVisible := False; //Aba filiais
    GBConfirma.Visible   := True;
    //
    if GOTOy.Registros(QrEmpresa) > 0 then
    begin
      EdRazao.Text         := QrEmpresaentEm.Value;
      EdCNPJ.Text          := Geral.FormataCNPJ_TT(QrEmpresaentCNPJ.Value);
      EdLogin.Text         := QrEmpresaMasLogin.Value;
      EdSenha.Text         := QrEmpresaDECR_SNHA.Value;
      CBUsaAccMngr.Checked := QrEmpresaUsaAccMngr.Value = 1;
      EdSenhaAdmin.Text    := QrEmpresaDECR_ADMIN.Value;
    end else
    begin
      EdRazao.Text         := '';
      EdCNPJ.Text          := '';
      EdLogin.Text         := '';
      EdSenha.Text         := '';
      CBUsaAccMngr.Checked := False;
      EdSenhaAdmin.Text    := '';
    end;
    GBCadastroDados.Visible := False;
    GBCadastroEdita.Visible := True;
  end else
  begin
    GBControle.Visible      := True;
    TabSheet2.TabVisible    := True; //Aba terminais
    TabSheet3.TabVisible    := True; //Aba filiais
    GBConfirma.Visible      := False;
    GBCadastroEdita.Visible := False;
    GBCadastroDados.Visible := True;
  end;
  ImgTipo.SQLType := SQLType;
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmMatriz.PageControl1Change(Sender: TObject);
var
  Enab: Boolean;
begin
  Enab := (PageControl1.ActivePageIndex = 2) and (ImgTipo.SQLType = stLok);
  //
  GBFiliais.Visible  := Enab;
  GBControle.Visible := not Enab;
end;

procedure TFmMatriz.PMFilialPopup(Sender: TObject);
{$IFNDef SemEntidade}
var
  Enab: Boolean;
{$EndIf}
begin
{$IFNDef SemEntidade}
  Enab := (QrFiliais.State <> dsInactive) and (QrFiliais.RecordCount > 0);
  //
  Alterafilialatual1.Enabled             := Enab;
  Alteraoutrsdadosdafilialatual1.Enabled := Enab;
  AJustaCodigodeclienteinterno1.Enabled  := Enab;
  //
  EntidadeA1.Visible := False;//(CO_DMKID_APP in [3,22]); //Creditor e Credito2
{$Else}
  Alterafilialatual1.Enabled             := False;
  Alteraoutrsdadosdafilialatual1.Enabled := False;
{$EndIf}
end;

procedure TFmMatriz.CriaOForm;
begin
  DefParams;
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmMatriz.AJustaCodigodeclienteinterno1Click(Sender: TObject);
begin
  DModG.AjustaFiliaisCliInt();
end;

procedure TFmMatriz.Alteraoutrsdadosdafilialatual1Click(Sender: TObject);
begin
  {$IFNDef SemEntidade}
  if DBCheck.CriaFm(TFmEntiCliInt, FmEntiCliInt, afmoSoBoss) then
  begin
    FmEntiCliInt.FCodCliInt := QrFiliaisFilial.Value;
    //
    FmEntiCliInt.ShowModal;
    FmEntiCliInt.Destroy;
  end;
  {$EndIf}
end;

procedure TFmMatriz.BtUsuarioClick(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmFilialUsrs, FmFilialUsrs, afmoNegarComAviso) then
  begin
    FmFilialUsrs.ShowModal;
    FmFilialUsrs.Destroy;
  end;
end;

procedure TFmMatriz.BtAlteraClick(Sender: TObject);
begin
  if not DBCheck.LiberaPelaSenhaBoss then Exit;
  //
  MostraEdicao(True, stUpd, 0);
end;

procedure TFmMatriz.BtConfirmaClick(Sender: TObject);
var
  Em, CNPJ, MasLogin, MasSenha, SenhaAdmin: String;
  UsaAccMngr: Integer;
  StrType, SQL: String;
begin
  Dmod.QrUpdU.SQL.Clear;
  //
{
  if QrEmpresa.RecordCount = 0 then
    Dmod.QrUpdU.SQL.Add('INSERT INTO master SET ')
  else
    Dmod.QrUpdU.SQL.Add('UPDATE master SET ');
  //
  Dmod.QrUpdU.SQL.Add('Em=:P0, CNPJ=:P1, ');
  Dmod.QrUpdU.SQL.Add('MasLogin=:P2, MasSenha=AES_ENCRYPT(:P3, :P4), ');
  Dmod.QrUpdU.SQL.Add('UsaAccMngr=:P5');
  Dmod.QrUpdU.Params[00].AsString  := EdRazao.Text;
  Dmod.QrUpdU.Params[01].AsString  := Geral.SoNumero_TT(EdCNPJ.Text);
  Dmod.QrUpdU.Params[02].AsString  := EdLogin.Text;
  Dmod.QrUpdU.Params[03].AsString  := EdSenha.Text;
  Dmod.QrUpdU.Params[04].AsString  := CO_USERSPNOW;
  Dmod.QrUpdU.Params[05].AsInteger := Geral.BoolToInt(CBUsaAccMngr.Checked);
  Dmod.QrUpdU.ExecSQL;
}
  if QrEmpresa.RecordCount = 0 then
    StrType := 'INSERT INTO '
  else
    StrType := 'UPDATE';
  //
  Em             := EdRazao.Text;
  CNPJ           := Geral.SoNumero_TT(EdCNPJ.Text);
  MasLogin       := EdLogin.Text;
  MasSenha       := EdSenha.Text;
  UsaAccMngr     := Geral.BoolToInt(CBUsaAccMngr.Checked);
  SenhaAdmin     := EdSenhaAdmin.Text;
  //
  SQL := Geral.ATS([
  StrType + ' master SET ',
  'Em="' + Em + '", ',
  'CNPJ="' + CNPJ + '", ',
  'MasLogin="' + MasLogin + '", ',
  'MasSenha=AES_ENCRYPT("' + MasSenha + '", "' + CO_USERSPNOW + '"), ',
  'UsaAccMngr=' + Geral.FF0(Geral.BoolToInt(CBUsaAccMngr.Checked)) + ', ',
  'SenhaAdmin=AES_ENCRYPT("' + SenhaAdmin + '", "' + CO_USERSPNOW + '") ',
  '']);
  //
  Dmod.MyDB.Execute(SQL);
{
  Geral.ATS([
  StrType + ' master SET ',
  'Em="' + Em + '"',
  'CNPJ="' + CNPJ + '"',
  'MasLogin="' + MasLogin + '"',
  'MasSenha=AES_ENCRYPT("' + MasSenha + '", "' + CO_USERSPNOW + '"), ',
  'UsaAccMngr=' + Geral.BoolToInt(CBUsaAccMngr.Checked),
  'SenhaAdmin=AES_ENCRYPT("' + SenhaAdmin + '", "' + CO_USERSPNOW + '"), ',
}
//  '']));
  //
  QrEmpresa.Close;
  UnDmkDAC_PF.AbreQuery(QrEmpresa, Dmod.MyDB);
  MostraEdicao(False, stLok, 0);
end;

procedure TFmMatriz.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmMatriz.BtVerifiTerceirosClick(Sender: TObject);
begin
{$IFNDef SemBDTErceiros}
  if DBCheck.CriaFm(TFmVerifiDBTerceiros, FmVerifiDBTerceiros, afmoNegarComAviso) then
  begin
    FmVerifiDBTerceiros.ShowModal;
    FmVerifiDBTerceiros.Destroy;
  end;
{$EndIf}
end;

procedure TFmMatriz.BtDefineClick(Sender: TObject);
var
  Empresa, CNPJ, NO_EMP: String;
  SQLType: TSQLType;
begin
  Empresa := '-11';
  if InputQuery('Empresa', 'Informe o c�digo da entidade da empresa dona:',
  Empresa) then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(Dmod.QrAux, Dmod.MyDB, [
    'SELECT * ',
    'FROM master ',
    '']);
    if Dmod.QrAux.RecordCount = 0 then
      SQLType := stIns
    else
      SQLType := stUpd;
    //
    UnDmkDAC_PF.AbreMySQLQuery0(Dmod.QrAux, Dmod.MyDB, [
    'SELECT IF(Tipo=0, CNPJ, CPF) CNPJ_CPF, ',
    'IF(Tipo=0, RazaoSocial, Nome) NO_EMP ',
    'FROM entidades ',
    'WHERE Codigo=' + Empresa,
    '']);
    CNPJ   := DMod.QrAux.FieldByName('CNPJ_CPF').AsString;
    NO_EMP := DMod.QrAux.FieldByName('NO_EMP').AsString;
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'controle', False, [
    'CNPJ', 'Dono'], [], [CNPJ, Empresa], [], False) then
    begin
      if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'master', False, [
      'CNPJ', 'Em'], [], [CNPJ, NO_EMP], [], False) then
      begin
        DModG.ReopenControle();
        DModG.ReopenDono();
        DModG.ReopenMaster('TFmMatriz.460');
        UnDmkDAC_PF.AbreQueryApenas(QrEmpresa);
      end;
    end;
  end;
end;

procedure TFmMatriz.BtDesisteClick(Sender: TObject);
begin
  MostraEdicao(False, stLok, 0);
end;

procedure TFmMatriz.BtFilialClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMFilial, BtFilial);
end;

procedure TFmMatriz.BtRelMtrzClick(Sender: TObject);
begin
{$IFNDef SemEntiCfgRel}
  if DBCheck.CriaFm(TFmEntiCfgRel, FmEntiCfgRel, afmoNegarComAviso) then
  begin
    FmEntiCfgRel.ShowModal;
    FmEntiCfgRel.Destroy;
  end;
{$EndIf}
end;

procedure TFmMatriz.FormCreate(Sender: TObject);
var
  EhMaster: Boolean;
begin
  ImgTipo.SQLType := stLok;
  //
  EhMaster := ((Uppercase(VAR_LOGIN) = 'MASTER')
    and (Uppercase(VAR_SENHA) = Uppercase(CO_MASTER)));
  //
  EdRazao.Enabled  := False;
  EdRazao.ReadOnly := True;
  EdCNPJ.Enabled   := False;
  EdCNPJ.ReadOnly  := True;
  //
  {$IFNDef SemEntiCfgRel}
    BtRelMtrz.Visible := True;
  {$Else}
    BtRelMtrz.Visible := False;
  {$EndIf}
  {$IFNDef SemBDTErceiros}
    BtVerifiTerceiros.Visible := True;
  {$Else}
    BtVerifiTerceiros.Visible := False;
  {$EndIf}
  //
  BtDefine.Visible := EhMaster;
  //
  PageControl1.ActivePageIndex := 0;
  CriaOForm;
  if ReopenFiliais(0) then
  begin
    GBCadastroDados.Align := alClient;
    //
    try
      {$IFNDef SemBDTErceiros}
      UnDmkDAC_PF.AbreMySQLQuery0(DmodG.QrAllAux, DModG.AllID_DB, [
      'SELECT COUNT(Codigo) ITENS FROM dtb_munici',
      '']);
      if DmodG.QrAllAux.FieldByName('ITENS').AsInteger < 5566 then
        Geral.MensagemBox('Os dados da tabela de munic�pios (DTB) podem estar desatualizados!',
        'Aviso', MB_OK+MB_ICONWARNING);
      UnDmkDAC_PF.AbreMySQLQuery0(DmodG.QrAllAux, DModG.AllID_DB, [
      'SELECT COUNT(Codigo) ITENS FROM bacen_pais',
      '']);
      if DmodG.QrAllAux.FieldByName('ITENS').AsInteger < 241 then
        Geral.MensagemBox('Os dados da tabela de pa�ses (IBGE) podem estar desatualizados!',
        'Aviso', MB_OK+MB_ICONWARNING);
      {$EndIf}
    except
      ;
    end;
    try
      UnDmkDAC_PF.AbreTable(TbTerminais, Dmod.MyDB);
    except
      Geral.MensagemBox('N�o foi poss�vel listar os terminais!',
      'Aviso', MB_OK+MB_ICONWARNING);
    end;
  end;
end;

procedure TFmMatriz.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  DModG.AjustaFiliaisCliInt();
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmMatriz.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
  QrErrFil.Close;
  UnDmkDAC_PF.AbreQuery(QrErrFil, Dmod.MyDB);
  if QrErrFil.RecordCount = 0 then
  begin
    if (Uppercase(Application.Title) <> 'SYNDIC')
    and (Uppercase(Application.Title) <> 'SYNKER') then
    begin
      Geral.MensagemBox('Defina pelo menos uma filial!',
        'Aviso', MB_OK+MB_ICONWARNING);
      CanClose := False;
    end else CanClose := True;  
  end else if QrErrFilItens.Value > 1 then
  begin
    Geral.MensagemBox('Existe mais de uma empresa como o n�mero de ' +
      'filial igual a ' + IntToStr(QrErrFilFilial.Value) + '!', 'Aviso',
      MB_OK+MB_ICONWARNING);
    CanClose := False;
  end else if QrErrFilFilial.Value = 0 then
  begin
    Geral.MensagemBox('Defina o n�mero da filial para o cadastro '
    + IntToStr(QrErrFilCodigo.Value) + '!', 'Aviso', MB_OK+MB_ICONWARNING);
    CanClose := False;
  end else CanClose := True;
end;

procedure TFmMatriz.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmMatriz.QrEmpresaCalcFields(DataSet: TDataSet);
begin
  QrEmpresaCNPJ_TXT.Value := Geral.FormataCNPJ_TT(QrEmpresaentCNPJ.Value);
end;

procedure TFmMatriz.QrFiliaisAfterScroll(DataSet: TDataSet);
begin
  ReopenEntidade2(0);
end;

procedure TFmMatriz.QrFiliaisCalcFields(DataSet: TDataSet);
begin
  QrFiliaisCNPJCPF_TXT.Value := Geral.FormataCNPJ_TT(QrFiliaisCNPJCPF.Value);
end;

procedure TFmMatriz.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmMatriz.Incluinovafilial1Click(Sender: TObject);
var
  Codigo, Existe: Integer;
  Filial: String;
begin
  Screen.Cursor := crHourGlass;
  Codigo := 0;
  Existe := -11;
  while Codigo = 0 do
  begin
    Dmod.QrAux.Close;
    Dmod.QrAux.SQL.Clear;
    Dmod.QrAux.SQL.Add('SELECT Codigo FROM entidades');
    Dmod.QrAux.SQL.Add('WHERE Codigo=:P0');
    Dmod.QrAux.Params[0].AsInteger := Existe;
    UnDmkDAC_PF.AbreQuery(Dmod.QrAux, Dmod.MyDB);
    //
    if Dmod.QrAux.RecordCount = 0 then
      Codigo := Existe
    else Existe := Existe -1;  
  end;
  Screen.Cursor := crDefault;
  //
  if Codigo > -11 then Codigo := -11;
  Filial := FormatFloat('0', Codigo);
  //
  if InputQuery('Inclus�o de Nova Filial', 'Informe o c�digo da filial:',
  Filial) then
  begin
    Codigo := Geral.IMV(Filial);
    if Codigo = 0 then
    begin
      Geral.MensagemBox('O c�digo n�o pode ser zero!', 'Aviso',
      MB_OK+MB_ICONWARNING);
      Exit;
    end;
    Dmod.QrAux.Close;
    Dmod.QrAux.SQL.Clear;
    Dmod.QrAux.SQL.Add('SELECT Codigo FROM entidades');
    Dmod.QrAux.SQL.Add('WHERE Codigo=' + Filial);
    UnDmkDAC_PF.AbreQuery(Dmod.QrAux, Dmod.MyDB);
    //
    if Codigo > -11 then
    begin
      Geral.MensagemBox('O c�digo deve ser menor que -10', 'Aviso',
      MB_OK+MB_ICONWARNING);
      Exit;
    end;
    if Dmod.QrAux.RecordCount > 0 then
    begin
      Geral.MensagemBox('O c�digo ' + Filial + ' j� existe!', 'Aviso',
      MB_OK+MB_ICONWARNING);
      Exit;
    end;
    if Geral.MensagemBox('Confirma a inclus�o da filial ' + Filial +
    '?', 'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
    begin
      // UMyMod.SQLInsUpd(Dmod.QrUpd, 'entidades', False,)
      if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns,  'entidades', False, [
      'Tipo', 'CodUsu'],['Codigo'], [0, Codigo], [Codigo], True) then
        ReopenFiliais(Codigo);
    end;
  end;
  DModG.AjustaFiliaisCliInt();
end;

procedure TFmMatriz.ReopenEntidade2(Codigo: Integer);
begin
  {
  QrEntidade2.Close;
  QrEntidade2.Params[0].AsInteger := QrFiliaisCodigo.Value;
  UnDmkDAC_PF.AbreQuery(QrEntidade2, Dmod.MyDB);
  }
end;

function TFmMatriz.ReopenFiliais(Codigo: Integer): Boolean;
begin
  Result := False;
  try
    QrFiliais.Close;
    //
    if VAR_KIND_DEPTO in [kdUH, kdDpto] then
    begin
      QrFiliais.Close;
      QrFiliais.SQL.Clear;
      QrFiliais.SQL.Add('SELECT DISTINCT ent.Codigo, ent.CliInt Filial,');
      QrFiliais.SQL.Add('IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENT,');
      QrFiliais.SQL.Add('IF(ent.Tipo=0, ent.CNPJ, ent.CPF) CNPJCPF,');
      QrFiliais.SQL.Add('IF(ent.Tipo=0, ent.IE, ent.RG) IERG,');
      QrFiliais.SQL.Add('FormaSociet, Simples, Atividade');
      QrFiliais.SQL.Add('FROM entidades ent');
      QrFiliais.SQL.Add('WHERE ent.CliInt <> 0');
    end else begin
      QrFiliais.Close;
      QrFiliais.SQL.Clear;
      QrFiliais.SQL.Add('SELECT DISTINCT ent.Codigo, ent.Filial,');
      QrFiliais.SQL.Add('IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENT,');
      QrFiliais.SQL.Add('IF(ent.Tipo=0, ent.CNPJ, ent.CPF) CNPJCPF,');
      QrFiliais.SQL.Add('IF(ent.Tipo=0, ent.IE, ent.RG) IERG,');
      QrFiliais.SQL.Add('FormaSociet, Simples, Atividade');
      QrFiliais.SQL.Add('FROM entidades ent');
      QrFiliais.SQL.Add('WHERE ent.Codigo < -10');
    end;
    UnDmkDAC_PF.AbreQuery(QrFiliais, Dmod.MyDB);
    Result := true;
    QrFiliais.Locate('Codigo', Codigo, []);
  except
    Geral.MensagemBox('N�o foi poss�vel listar as filiais!',
    'Aviso', MB_OK+MB_ICONWARNING);
  end;
end;

procedure TFmMatriz.SpeedButton1Click(Sender: TObject);
begin
  if not DBCheck.LiberaPelaSenhaAdmin() then
    Exit;
  LaSenhaAdmin.Enabled := True;
  EdSenhaAdmin.Enabled := True;
end;

end.

