unit UnEmpresas;

interface

uses mySQLDbTables, UMySQLModule, dmkGeral, ModuleGeral, dmkImage, Forms,
  Controls, Windows, DmkDAC_PF, ComCtrls, StdCtrls, ExtCtrls, Messages,
  SysUtils, Classes, Graphics, Dialogs, Variants, MaskUtils, Registry, ShellAPI,
  UnMsgInt, DB,(* DbTables,*) mySQLExceptions, UnInternalConsts, UnDmkEnums;


type
  TUnEmpresas = class(TObject)
  private
    { Private declarations }
  public
    { Public declarations }
{
    procedure AtualizaEntidadesParaEntidade2(ProgreesBar: TProgressBar; DataBase, AllDataBase: TmySQLDatabase);
    function  ObtemCodBacenPais(Pais: String; AllDataBase: TmySQLDatabase): Integer;
    function  ObtemCodMunicipio(Cidade: String; AllDataBase: TmySQLDatabase): Integer;
    function  CriaContatoGenerico(Codigo: Integer; QueryUpd: Tmysqlquery): Integer;
    function  IncluiEntiMail(Codigo, Controle, EntiTipCto, Ordem,
                DiarioAdd: Integer; EMail: String; QueryUpd: Tmysqlquery): Integer;
    function  CNPJCPFDuplicado(Codigo, Tipo: Integer; CNPJ, CPF: String; SQLType: TSQLType): Boolean;
    function  CadastroDeEntidade(Entidade: Integer; TipoForm,
              Padrao: TFormCadEnti; EditingByMatriz: Boolean = False;
              AbrirEmAba: Boolean = False; InOwner: TWincontrol = nil;
              Pager: TWinControl = nil): Boolean;
}
    function  ForcaDefinicaodeEmpresa(var Empresa: Integer; HaltIfIndef: Boolean): Boolean;
{
    procedure MostraFormEntidadesImp();
    procedure MostraFormEntiImagens(Codigo, CodEnti, TipImg: Integer; MostraImagem: Boolean);
    function  TipoDeCadastroDefinido(CkCliente1, CkCliente2, CkCliente3,
              CkCliente4, CkFornece1, CkFornece2, CkFornece3, CkFornece4, CkFornece5,
              CkFornece6, CkFornece7, CkFornece8, CkTerceiro: TCheckBox;
              PageControl: TPageControl): Boolean;
    procedure MostraFormMatriz();
}
  end;

var
  UEmpresas: TUnEmpresas;

const
  CO_VERSAO_MODULO_ENT = 2.01;

implementation

uses
  Entidades, Entidade2, EntiImagens, Entidade2Imp, UnMyObjects, MyDBCheck,
  Matriz;

{ TUnEmpresas }

//var
  //FEntiToCad: Integer;
{

procedure TUnEmpresas.AtualizaEntidadesParaEntidade2(ProgreesBar: TProgressBar;
  DataBase, AllDataBase: TmySQLDatabase);

  function InsereEntiMail(Email: String; Codigo, Controle: Integer;
    Database: TmySQLDatabase): Integer;
  var
    QueryUpd: TmySQLQuery;
    Conta: Integer;
  begin
    Result   := 0;
    QueryUpd := TmySQLQuery.Create(DataBase);
    QueryUpd.Database := Database;
    try
      Conta := UMyMod.BuscaEmLivreY_Def('entimail', 'Conta', stIns, 0);
      //
      if UMyMod.SQLInsUpd(QueryUpd, stIns, 'entimail', False,
        ['EMail', 'Codigo', 'Controle'], ['Conta'],
        [Email, Codigo, Controle], [Conta], True) then
      begin
        Result := Conta;
      end;
    finally
      QueryUpd.Free;
    end;
  end;

var
  QueryUpd, QueryUpd2, QueryEnt: TmySQLQuery;
  Codigo, Controle, ECodiPais, PCodiPais, ECodMunici, PCodMunici,
  CCodMunici, LCodMunici: Integer;
  EEmail, PEmail, EPais, PPais, ECidade, PCidade, CCidade,
  LCidade: String;
begin
  QueryEnt  := TmySQLQuery.Create(DataBase);
  QueryUpd  := TmySQLQuery.Create(DataBase);
  QueryUpd2 := TmySQLQuery.Create(DataBase);
  //
  QueryEnt.Database  := DataBase;
  QueryUpd.Database  := DataBase;
  QueryUpd2.Database := DataBase;
  try
    Screen.Cursor := crHourGlass;
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QueryEnt, DataBase, [
      'SELECT Codigo, EEmail, PEmail, EPais, PPais, ',
      'ECidade, PCidade, CCidade, LCidade ',
      'FROM entidades ',
      '']);
    if QueryEnt.RecordCount > 0 then
    begin
      ProgreesBar.Visible  := True;
      ProgreesBar.Position := 0;
      ProgreesBar.Max      := QueryEnt.RecordCount;
      //
      QueryEnt.First;
      while not QueryEnt.Eof do
      begin
        ProgreesBar.Position := ProgreesBar.Position + 1;
        ProgreesBar.Update;
        Application.ProcessMessages;
        //
        Codigo := QueryEnt.FieldByName('Codigo').AsInteger;
        //
        EPais := QueryEnt.FieldByName('EPais').AsString;
        PPais := QueryEnt.FieldByName('PPais').AsString;
        //
        ECidade := QueryEnt.FieldByName('ECidade').AsString;
        PCidade := QueryEnt.FieldByName('PCidade').AsString;
        CCidade := QueryEnt.FieldByName('CCidade').AsString;
        LCidade := QueryEnt.FieldByName('LCidade').AsString;
        //
        EEmail := QueryEnt.FieldByName('EEmail').AsString;
        PEmail := QueryEnt.FieldByName('PEmail').AsString;
        //
        ECodiPais := ObtemCodBacenPais(EPais, AllDataBase);
        PCodiPais := ObtemCodBacenPais(PPais, AllDataBase);
        //
        ECodMunici := ObtemCodMunicipio(ECidade, AllDataBase);
        PCodMunici := ObtemCodMunicipio(PCidade, AllDataBase);
        CCodMunici := ObtemCodMunicipio(CCidade, AllDataBase);
        LCodMunici := ObtemCodMunicipio(LCidade, AllDataBase);
        //
        if UMyMod.SQLInsUpd(QueryUpd, stUpd, 'entidades', False,
          [
          'ECodiPais', 'PCodiPais', 'ECodMunici',
          'PCodMunici', 'CCodMunici', 'LCodMunici'
          ], ['Codigo'],
          [
          ECodiPais, PCodiPais, ECodMunici,
          PCodMunici, CCodMunici, LCodMunici
          ], [Codigo], True) then
        begin
          if (EEmail <> '') or (PEmail <> '') then
          begin
            Controle := CriaContatoGenerico(Codigo, QueryUpd2);
            //
            if (Controle <> 0) and (EEmail <> '') then
              InsereEntiMail(EEmail, Codigo, Controle, DataBase);
            if (Controle <> 0) and (PEmail <> '') then
              InsereEntiMail(PEmail, Codigo, Controle, DataBase);
          end;
        end;
        //
        QueryEnt.Next;
      end;
      UnDmkDAC_PF.ExecutaMySQLQuery0(QueryUpd, DataBase, [
        'UPDATE ctrlgeral SET AtualizouEntidades=1',
        '']);
    end;
  finally
    QueryEnt.Free;
    QueryUpd.Free;
    QueryUpd2.Free;
    //
    ProgreesBar.Visible := False;
    //
    Screen.Cursor := crDefault;
    //
    Geral.MB_Aviso('Atualiza��o de entidades finalizada!');
  end;
end;

function TUnEmpresas.CadastroDeEntidade(Entidade: Integer; TipoForm,
  Padrao: TFormCadEnti; EditingByMatriz, AbrirEmAba: Boolean;
  InOwner: TWincontrol; Pager: TWinControl): Boolean;
var
  Modelo: Integer;
  Form: TForm;
begin
  Result := False;
  //FEntiToCad := Entidade;
  if GERAL_MODELO_FORM_ENTIDADES <> fmcadSelecionar then
    Modelo := Integer(GERAL_MODELO_FORM_ENTIDADES) -1
  else
  begin
    case TipoForm of
      fmcadSelecionar:
      begin
        Modelo := MyObjects.SelRadioGroup('Selecione o modelo', '', ['Modelo A', 'Modelo B'], 2);
        if not Modelo in [0, 1] then
          Exit;
      end;
      //
      fmcadEntidades: Modelo := 0;
      fmcadEntidade2: Modelo := 1;
      else Modelo := -1;
    end;
  end;
  case Modelo of
    0:
    begin
      if DBCheck.CriaFm(TFmEntidades, FmEntidades, afmoNegarComAviso) then
      begin
        // Deve ser antes
        FmEntidades.FEditingByMatriz := EditingByMatriz;
        // somente depois...
        //if FEntiToCad <> 0 then
          //TFmEntidades(Form).LocCod(FEntiToCad, FEntiToCad);
        if Entidade <> 0 then
          FmEntidades.LocCod(Entidade, Entidade);
        FmEntidades.ShowModal;
        FmEntidades.Destroy;
        Result := True;
      end;
    end;
    1:
    begin
      if AbrirEmAba then
      begin
        if FmEntidade2 = nil then
        begin
          Form := MyObjects.FormTDICria(TFmEntidade2, InOwner, Pager, False, True);
          //
          TFmEntidade2(Form).FEditingByMatriz := EditingByMatriz;
          //
          //if FEntiToCad <> 0 then
            //TFmEntidade2(Form).LocCod(FEntiToCad, FEntiToCad);
          if Entidade <> 0 then
            TFmEntidade2(Form).LocCod(Entidade, Entidade);
        end;
      end else
      begin
        if DBCheck.CriaFm(TFmEntidade2, FmEntidade2, afmoNegarComAviso) then
        begin
          // Deve ser antes
          FmEntidade2.FEditingByMatriz := EditingByMatriz;
          // somente depois...
          //if FEntiToCad <> 0 then
            ///FmEntidade2.LocCod(FEntiToCad, FEntiToCad);
          if Entidade <> 0 then
            FmEntidade2.LocCod(Entidade, Entidade);
          FmEntidade2.ShowModal;
          FmEntidade2.Destroy;
          //
          FmEntidade2 := nil;
          //
          Result := True;
        end;
      end;
    end;
    else Geral.MB_Erro('Modelo de cadastro de entidade n�o implementado!');
  end;
end;

function TUnEmpresas.CNPJCPFDuplicado(Codigo, Tipo: Integer; CNPJ, CPF: String;
  SQLType: TSQLType): Boolean;
begin
  Result := False;
  //
  if SQLType <> stUpd then
  begin
    try
      Screen.Cursor := crHourGlass;
      //
      DModG.QrAux.Close;
      DModG.QrAux.SQL.Clear;
      if Tipo = 0 then
      begin
        DModG.QrAux.SQL.Add('SELECT Codigo FROM entidades');
        DModG.QrAux.SQL.Add('WHERE CNPJ="' + Geral.SoNumero_TT(CNPJ) + '"');
        DModG.QrAux.SQL.Add('AND Codigo <> ' + Geral.FF0(Codigo));
      end;
      if Tipo = 1 then
      begin
        DModG.QrAux.SQL.Add('SELECT Codigo FROM entidades');
        DModG.QrAux.SQL.Add('WHERE CPF="' + Geral.SoNumero_TT(CPF)+'"');
        DModG.QrAux.SQL.Add('AND Codigo <> ' + Geral.FF0(Codigo));
      end;
      UnDmkDAC_PF.AbreQueryApenas(DModG.QrAux);
    finally
      Screen.Cursor := crDefault;
    end;
    if DModG.QrAux.RecordCount > 0 then
    begin
      Result := True;
      //
      Geral.MB_Aviso('Esta entidade j� foi cadastrada com o c�digo n� ' +
        Geral.FF0(DModG.QrAux.FieldByName('Codigo').AsInteger) + '.');
      DModG.QrAux.Close;
      //
      Exit;
    end;
    DModG.QrAux.Close;
  end;
end;

function TUnEmpresas.CriaContatoGenerico(Codigo: Integer;
  QueryUpd: Tmysqlquery): Integer;
var
  Controle: Integer;
  Nome: String;
begin
  try
    Controle := UMyMod.BuscaEmLivreY_Def('enticontat', 'Controle', stIns, 0);
    Nome     := '[Contato]'; //Nome gen�rico para o contato
    //
    if UMyMod.SQLInsUpd(QueryUpd, stIns, 'enticontat', False,
      ['Nome', 'Codigo'], ['Controle'], [Nome, Codigo], [Controle], True) then
    begin
      DModG.AtualizaEntiConEnt();
      //
      Result := Controle;
    end else
      Result := 0;
  except
    Result := 0;
  end;
end;
}

function TUnEmpresas.ForcaDefinicaodeEmpresa(var Empresa: Integer;
  HaltIfIndef: Boolean): Boolean;
begin
  if VAR_LIB_EMPRESA_SEL = 0 then
  begin
    Geral.MB_Erro('Nenhuma empresa est� logada! O ERP ser� encerrado!');
    Halt(0);
  end else
    Empresa := VAR_LIB_EMPRESA_SEL;
end;

{
function TUnEmpresas.IncluiEntiMail(Codigo, Controle, EntiTipCto, Ordem,
  DiarioAdd: Integer; EMail: String; QueryUpd: Tmysqlquery): Integer;
var
  Conta: Integer;
begin
  try
    Conta := UMyMod.BuscaEmLivreY_Def('entimail', 'Conta', stIns, 0);
    //
    if UMyMod.SQLInsUpd(QueryUpd, stIns, 'entimail', False,
      ['EMail', 'EntiTipCto', 'Ordem', 'DiarioAdd', 'Codigo', 'Controle'],
      ['Conta'], [EMail, EntiTipCto, Ordem, DiarioAdd, Codigo, Controle],
      [Conta], True)
    then
      Result := Conta
    else
      Result := 0;
  except
    Result := 0;
  end;
end;

procedure TUnEmpresas.MostraFormEntiImagens(Codigo, CodEnti, TipImg: Integer;
  MostraImagem: Boolean);
begin
  if DBCheck.CriaFm(TFmEntiImagens, FmEntiImagens, afmoNegarComAviso) then
  begin
    FmEntiImagens.ReopenEntidades(Codigo, '');
    if MostraImagem then
    begin
      FmEntiImagens.ReopenEntiImgs(Codigo, CodEnti, TipImg);
      FmEntiImagens.VisualizaImagem();
    end;
    FmEntiImagens.ShowModal;
    FmEntiImagens.Destroy;
  end;
end;

procedure TUnEmpresas.MostraFormMatriz();
begin
  if DBCheck.CriaFm(TFmMatriz, FmMatriz, afmoSoBoss) then
  begin
    FmMatriz.ShowModal;
    FmMatriz.Destroy;
  end;
end;

procedure TUnEmpresas.MostraFormEntidadesImp();
begin
  if DBCheck.CriaFm(TFmEntidade2Imp, FmEntidade2Imp, afmoNegarComAviso) then
  begin
    FmEntidade2Imp.ShowModal;
    FmEntidade2Imp.Destroy;
  end;
end;

function TUnEmpresas.ObtemCodBacenPais(Pais: String;
  AllDataBase: TmySQLDatabase): Integer;
var
  QueryPais: TmySQLQuery;
begin
  QueryPais := TmySQLQuery.Create(AllDataBase);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(QueryPais, AllDataBase, [
      'SELECT Codigo ',
      'FROM bacen_pais ',
      'WHERE Nome LIKE "' + Pais + '" ',
      '']);
    if QueryPais.RecordCount > 0 then
      Result := QueryPais.FieldByName('Codigo').AsInteger
    else
      Result := 0;
  finally
    QueryPais.Free;
  end;
end;

function TUnEmpresas.ObtemCodMunicipio(Cidade: String;
  AllDataBase: TmySQLDatabase): Integer;
var
  QueryMuni: TmySQLQuery;
begin
  QueryMuni := TmySQLQuery.Create(AllDataBase);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(QueryMuni, AllDataBase, [
      'SELECT Codigo ',
      'FROM dtb_munici ',
      'WHERE Nome LIKE "' + Cidade + '" ',
      '']);
    if QueryMuni.RecordCount > 0 then
      Result := QueryMuni.FieldByName('Codigo').AsInteger
    else
      Result := 0;
  finally
    QueryMuni.Free;
  end;
end;

function TUnEmpresas.TipoDeCadastroDefinido(CkCliente1, CkCliente2, CkCliente3,
  CkCliente4, CkFornece1, CkFornece2, CkFornece3, CkFornece4, CkFornece5,
  CkFornece6, CkFornece7, CkFornece8, CkTerceiro: TCheckBox;
  PageControl: TPageControl): Boolean;
var
  Cliente1, Cliente2, Cliente3, Cliente4,
  Fornece1, Fornece2, Fornece3, Fornece4,
  Fornece5, Fornece6, Fornece7, Fornece8,
  Terceiro: String;
begin
  if CkCliente1.Checked then Cliente1 := 'V' else Cliente1 := 'F';
  if CkCliente2.Checked then Cliente2 := 'V' else Cliente2 := 'F';
  if CkCliente3.Checked then Cliente3 := 'V' else Cliente3 := 'F';
  if CkCliente4.Checked then Cliente4 := 'V' else Cliente4 := 'F';
  if CkFornece1.Checked then Fornece1 := 'V' else Fornece1 := 'F';
  if CkFornece2.Checked then Fornece2 := 'V' else Fornece2 := 'F';
  if CkFornece3.Checked then Fornece3 := 'V' else Fornece3 := 'F';
  if CkFornece4.Checked then Fornece4 := 'V' else Fornece4 := 'F';
  if CkFornece5.Checked then Fornece5 := 'V' else Fornece5 := 'F';
  if CkFornece6.Checked then Fornece6 := 'V' else Fornece6 := 'F';
  if CkFornece7.Checked then Fornece7 := 'V' else Fornece7 := 'F';
  if CkFornece8.Checked then Fornece8 := 'V' else Fornece8 := 'F';
  if CkTerceiro.Checked then Terceiro := 'V' else Terceiro := 'F';
  if
  (Cliente1 = 'F') and (Cliente2 = 'F') and (Cliente3 = 'F') and (Cliente4 = 'F') and
  (Fornece1 = 'F') and (Fornece2 = 'F') and (Fornece3 = 'F') and (Fornece4 = 'F') and
  (Fornece5 = 'F') and (Fornece6 = 'F') and (Fornece7 = 'F') and (Fornece8 = 'F') and
  (Terceiro = 'F') then
  begin
    Result := False;
    Screen.Cursor := crDefault;
    if PageControl <> nil then
      PageControl.ActivePageIndex := 1;
    Geral.MB_Aviso('Defina pelo menos um "tipo" de cadastro');
    //Exit;
  end else Result := True;
end;
}
end.
