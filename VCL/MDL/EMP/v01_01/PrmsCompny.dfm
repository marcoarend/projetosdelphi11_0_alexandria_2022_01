object FmPrmsCompny: TFmPrmsCompny
  Left = 368
  Top = 194
  Caption = 'PRM-CMPNY-001 :: Par'#226'metros de Companias'
  ClientHeight = 424
  ClientWidth = 784
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnDados: TPanel
    Left = 0
    Top = 96
    Width = 784
    Height = 328
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    object GBDados: TGroupBox
      Left = 0
      Top = 0
      Width = 784
      Height = 233
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 1
      object Label1: TLabel
        Left = 8
        Top = 12
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
        FocusControl = DBEdCodigo
      end
      object Label2: TLabel
        Left = 124
        Top = 12
        Width = 66
        Height = 13
        Caption = 'Raz'#227'o Social:'
      end
      object Label3: TLabel
        Left = 68
        Top = 12
        Width = 50
        Height = 13
        Caption = 'Compania:'
        FocusControl = DBEdit1
      end
      object Label4: TLabel
        Left = 8
        Top = 52
        Width = 261
        Height = 13
        Caption = 'Localiza'#231#227'o: Cadastro de Pa'#237's/regi'#227'o + L'#237'ngua/dialeto'
        FocusControl = DBEdit3
      end
      object Label11: TLabel
        Left = 8
        Top = 92
        Width = 50
        Height = 13
        Caption = 'Sigla pa'#237's:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label12: TLabel
        Left = 64
        Top = 92
        Width = 47
        Height = 13
        Caption = 'Company:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label17: TLabel
        Left = 120
        Top = 92
        Width = 111
        Height = 13
        Caption = 'Nome empresa no App:'
      end
      object Label18: TLabel
        Left = 8
        Top = 132
        Width = 221
        Height = 13
        Caption = 'Emails envio de PDF das despesas de viagem:'
      end
      object Label19: TLabel
        Left = 8
        Top = 172
        Width = 179
        Height = 13
        Caption = 'Emails envio de PDF do km particular:'
      end
      object DBEdCodigo: TdmkDBEdit
        Left = 8
        Top = 28
        Width = 56
        Height = 21
        TabStop = False
        DataField = 'Codigo'
        DataSource = DsPrmsCompny
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 0
        UpdType = utYes
        Alignment = taRightJustify
      end
      object DBEdit1: TDBEdit
        Left = 68
        Top = 28
        Width = 53
        Height = 21
        DataField = 'Filial'
        DataSource = DsPrmsCompny
        TabOrder = 1
      end
      object DBEdit2: TDBEdit
        Left = 124
        Top = 28
        Width = 600
        Height = 21
        DataField = 'NO_ENT'
        DataSource = DsPrmsCompny
        TabOrder = 2
      end
      object DBEdit3: TDBEdit
        Left = 8
        Top = 68
        Width = 717
        Height = 21
        DataField = 'NO_loclzl10ncab'
        DataSource = DsPrmsCompny
        TabOrder = 3
      end
      object DBEdit5: TDBEdit
        Left = 64
        Top = 108
        Width = 53
        Height = 21
        DataField = 'Company'
        DataSource = DsPrmsCompny
        TabOrder = 4
      end
      object DBEdit6: TDBEdit
        Left = 8
        Top = 108
        Width = 53
        Height = 21
        DataField = 'SiglaPais'
        DataSource = DsPrmsCompny
        TabOrder = 5
      end
      object DBEdit7: TDBEdit
        Left = 8
        Top = 148
        Width = 717
        Height = 21
        DataField = 'MailsDstn'
        DataSource = DsPrmsCompny
        TabOrder = 6
      end
      object DBEdit8: TDBEdit
        Left = 8
        Top = 188
        Width = 717
        Height = 21
        DataField = 'MailsDsKm'
        DataSource = DsPrmsCompny
        TabOrder = 7
      end
      object DBEdit9: TDBEdit
        Left = 120
        Top = 108
        Width = 605
        Height = 21
        DataField = 'NoEmpApp'
        DataSource = DsPrmsCompny
        TabOrder = 8
      end
    end
    object GBCntrl: TGroupBox
      Left = 0
      Top = 264
      Width = 784
      Height = 64
      Align = alBottom
      TabOrder = 0
      object Panel5: TPanel
        Left = 2
        Top = 15
        Width = 172
        Height = 47
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 174
        Top = 15
        Width = 88
        Height = 47
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel3: TPanel
        Left = 262
        Top = 15
        Width = 520
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object BtExclui: TBitBtn
          Tag = 12
          Left = 252
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Exclui'
          Enabled = False
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
        end
        object BtAltera: TBitBtn
          Tag = 11
          Left = 128
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Altera'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtAlteraClick
        end
        object BtInclui: TBitBtn
          Tag = 10
          Left = 4
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Inclui'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtIncluiClick
        end
        object Panel2: TPanel
          Left = 387
          Top = 0
          Width = 133
          Height = 47
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 3
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
      end
    end
  end
  object PnEdita: TPanel
    Left = 0
    Top = 96
    Width = 784
    Height = 328
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    Visible = False
    object GBEdita: TGroupBox
      Left = 0
      Top = 0
      Width = 784
      Height = 245
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 0
      object Label5: TLabel
        Left = 12
        Top = 16
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
        FocusControl = DBEdCodigo
      end
      object Label6: TLabel
        Left = 72
        Top = 16
        Width = 50
        Height = 13
        Caption = 'Compania:'
        FocusControl = DBEdit1
      end
      object Label7: TLabel
        Left = 128
        Top = 16
        Width = 66
        Height = 13
        Caption = 'Raz'#227'o Social:'
      end
      object Label8: TLabel
        Left = 12
        Top = 56
        Width = 261
        Height = 13
        Caption = 'Localiza'#231#227'o: Cadastro de Pa'#237's/regi'#227'o + L'#237'ngua/dialeto'
      end
      object SpeedButton5: TSpeedButton
        Left = 708
        Top = 71
        Width = 23
        Height = 23
        Caption = '...'
        OnClick = SpeedButton5Click
      end
      object Label9: TLabel
        Left = 708
        Top = 16
        Width = 15
        Height = 13
        Caption = 'Sit:'
        FocusControl = DBEdit4
      end
      object Label13: TLabel
        Left = 68
        Top = 96
        Width = 47
        Height = 13
        Caption = 'Company:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label10: TLabel
        Left = 12
        Top = 96
        Width = 50
        Height = 13
        Caption = 'Sigla pa'#237's:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label14: TLabel
        Left = 12
        Top = 136
        Width = 221
        Height = 13
        Caption = 'Emails envio de PDF das despesas de viagem:'
      end
      object Label15: TLabel
        Left = 12
        Top = 176
        Width = 179
        Height = 13
        Caption = 'Emails envio de PDF do km particular:'
      end
      object Label16: TLabel
        Left = 124
        Top = 96
        Width = 111
        Height = 13
        Caption = 'Nome empresa no App:'
      end
      object EdCodigo: TdmkEdit
        Left = 12
        Top = 32
        Width = 56
        Height = 21
        TabStop = False
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Codigo'
        UpdCampo = 'Codigo'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdNO_ENT: TdmkEdit
        Left = 128
        Top = 32
        Width = 577
        Height = 21
        TabStop = False
        ReadOnly = True
        TabOrder = 2
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'NO_ENT'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdFilial: TdmkEdit
        Left = 72
        Top = 32
        Width = 53
        Height = 21
        TabStop = False
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 1
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Filial'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdLoclzL10nCab: TdmkEditCB
        Left = 12
        Top = 72
        Width = 65
        Height = 21
        Alignment = taRightJustify
        TabOrder = 4
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'LoclzL10nCab'
        UpdCampo = 'LoclzL10nCab'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBLoclzL10nCab
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBLoclzL10nCab: TdmkDBLookupComboBox
        Left = 80
        Top = 72
        Width = 625
        Height = 21
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsLoclzL10nCab
        TabOrder = 5
        dmkEditCB = EdLoclzL10nCab
        QryCampo = 'LoclzL10nCab'
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object DBEdit4: TDBEdit
        Left = 708
        Top = 32
        Width = 25
        Height = 21
        DataField = 'SIT'
        DataSource = DsPrmsCompny
        TabOrder = 3
      end
      object EdCompany: TdmkEdit
        Left = 68
        Top = 112
        Width = 53
        Height = 21
        MaxLength = 4
        TabOrder = 7
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Company'
        UpdCampo = 'Company'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdSiglaPais: TdmkEdit
        Left = 12
        Top = 112
        Width = 49
        Height = 21
        MaxLength = 4
        TabOrder = 6
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'SiglaPais'
        UpdCampo = 'SiglaPais'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdMailsDstn: TdmkEdit
        Left = 12
        Top = 152
        Width = 721
        Height = 21
        TabStop = False
        TabOrder = 9
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'MailsDstn'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdMailsDsKm: TdmkEdit
        Left = 12
        Top = 192
        Width = 721
        Height = 21
        TabStop = False
        TabOrder = 10
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'MailsDsKm'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdNoEmpApp: TdmkEdit
        Left = 124
        Top = 112
        Width = 605
        Height = 21
        TabStop = False
        TabOrder = 8
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'NoEmpApp'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
    end
    object GBConfirma: TGroupBox
      Left = 0
      Top = 265
      Width = 784
      Height = 63
      Align = alBottom
      TabOrder = 1
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 12
        Top = 17
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object Panel1: TPanel
        Left = 645
        Top = 15
        Width = 137
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 7
          Top = 2
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 784
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 736
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 12
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 216
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 216
      Top = 0
      Width = 520
      Height = 52
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 322
        Height = 32
        Caption = 'Par'#226'metros de Companias'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 322
        Height = 32
        Caption = 'Par'#226'metros de Companias'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 322
        Height = 32
        Caption = 'Par'#226'metros de Companias'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 52
    Width = 784
    Height = 44
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 780
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 12
        Height = 17
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 12
        Height = 17
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object QrPrmsCompny: TMySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrPrmsCompnyBeforeOpen
    AfterOpen = QrPrmsCompnyAfterOpen
    SQL.Strings = (
      'SELECT ent.Codigo, ent.Filial,  '
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_ENT,'
      'llc.Nome NO_loclzl10ncab,'
      'IF(cny.Ativo IS NULL, "N", IF(cny.Ativo=0, "I", "A")) SIT,'
      'cny.MailsDstn, MailsDsKm, NoEmpApp'
      'FROM entidades ent'
      'LEFT JOIN prmscompny   cny ON cny.Entidade=ent.Codigo'
      'LEFT JOIN loclzl10ncab llc ON llc.Codigo=cny.LoclzL10nCab '
      'WHERE ent.Codigo < -10 '
      'AND ent.Filial > 0 ')
    Left = 100
    Top = 4
    object QrPrmsCompnyCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPrmsCompnyFilial: TIntegerField
      FieldName = 'Filial'
    end
    object QrPrmsCompnyNO_ENT: TWideStringField
      FieldName = 'NO_ENT'
      Size = 120
    end
    object QrPrmsCompnyNO_loclzl10ncab: TWideStringField
      FieldName = 'NO_loclzl10ncab'
      Size = 60
    end
    object QrPrmsCompnySIT: TWideStringField
      FieldName = 'SIT'
      Size = 1
    end
    object QrPrmsCompnySiglaPais: TWideStringField
      FieldName = 'SiglaPais'
      Size = 10
    end
    object QrPrmsCompnyLoclzL10nCab: TIntegerField
      FieldName = 'LoclzL10nCab'
    end
    object QrPrmsCompnyCompany: TWideStringField
      FieldName = 'Company'
      Size = 11
    end
    object QrPrmsCompnyMailsDstn: TWideStringField
      FieldName = 'MailsDstn'
      Size = 512
    end
    object QrPrmsCompnyMailsDsKm: TWideStringField
      FieldName = 'MailsDsKm'
      Size = 512
    end
    object QrPrmsCompnyNoEmpApp: TWideStringField
      FieldName = 'NoEmpApp'
      Size = 256
    end
  end
  object DsPrmsCompny: TDataSource
    DataSet = QrPrmsCompny
    Left = 100
    Top = 52
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = BtInclui
    CanUpd01 = BtAltera
    CanDel01 = BtExclui
    Left = 12
    Top = 8
  end
  object QrLoclzL10nCab: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM loclzl10ncab llc '
      'ORDER BY Nome')
    Left = 244
    Top = 68
    object QrLoclzL10nCabCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrLoclzL10nCabNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
  end
  object DsLoclzL10nCab: TDataSource
    DataSet = QrLoclzL10nCab
    Left = 272
    Top = 68
  end
end
