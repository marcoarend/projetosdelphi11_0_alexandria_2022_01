unit GruUsrEdtCab;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2, dmkGeral,
  ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt, UMySQLModule, Menus,
  UnInternalConsts2, mySQLDbTables, UnMySQLCuringa, dmkPermissoes, dmkEdit,
  dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup, Grids, DBGrids,
  UnDmkProcFunc, UnDmkENums;

type
  TFmGruUsrEdtCab = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    Label9: TLabel;
    EdNome: TdmkEdit;
    Label1: TLabel;
    DBEdCodigo: TdmkDBEdit;
    Label2: TLabel;
    DBEdNome: TdmkDBEdit;
    QrGruUsrEdtCab: TMySQLQuery;
    DsGruUsrEdtCab: TDataSource;
    QrGruUsrEdtIts: TMySQLQuery;
    DsGruUsrEdtIts: TDataSource;
    PMIts: TPopupMenu;
    ItsInclui1: TMenuItem;
    ItsExclui1: TMenuItem;
    ItsAltera1: TMenuItem;
    PMCab: TPopupMenu;
    CabInclui1: TMenuItem;
    CabAltera1: TMenuItem;
    CabExclui1: TMenuItem;
    BtCab: TBitBtn;
    BtIts: TBitBtn;
    QrGruUsrEdtCabCodigo: TIntegerField;
    QrGruUsrEdtCabNome: TWideStringField;
    QrGruUsrEdtItsCodigo: TIntegerField;
    QrGruUsrEdtItsControle: TIntegerField;
    QrGruUsrEdtItsUsuario: TIntegerField;
    QrGruUsrEdtItsLogin: TWideStringField;
    DGDados: TDBGrid;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrGruUsrEdtCabAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrGruUsrEdtCabBeforeOpen(DataSet: TDataSet);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure QrGruUsrEdtCabAfterScroll(DataSet: TDataSet);
    procedure CabInclui1Click(Sender: TObject);
    procedure CabAltera1Click(Sender: TObject);
    procedure CabExclui1Click(Sender: TObject);
    procedure BtCabClick(Sender: TObject);
    procedure BtItsClick(Sender: TObject);
    procedure ItsInclui1Click(Sender: TObject);
    procedure ItsExclui1Click(Sender: TObject);
    procedure ItsAltera1Click(Sender: TObject);
    procedure PMCabPopup(Sender: TObject);
    procedure PMItsPopup(Sender: TObject);
    procedure QrGruUsrEdtCabBeforeClose(DataSet: TDataSet);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure LocCod(Atual, Codigo: Integer);
    procedure Va(Para: TVaiPara);
    procedure MostraGruUsrEdtIts(SQLType: TSQLType);
  public
    { Public declarations }
    FSeq, FCabIni: Integer;
    FLocIni: Boolean;
    //
    procedure ReopenGruUsrEdtIts(Controle: Integer);
  end;

var
  FmGruUsrEdtCab: TFmGruUsrEdtCab;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, MyDBCheck, DmkDAC_PF, GruUsrEdtIts;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmGruUsrEdtCab.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmGruUsrEdtCab.MostraGruUsrEdtIts(SQLType: TSQLType);
begin
  if DBCheck.CriaFm(TFmGruUsrEdtIts, FmGruUsrEdtIts, afmoNegarComAviso) then
  begin
    FmGruUsrEdtIts.ImgTipo.SQLType := SQLType;
    FmGruUsrEdtIts.FQrCab := QrGruUsrEdtCab;
    FmGruUsrEdtIts.FDsCab := DsGruUsrEdtCab;
    FmGruUsrEdtIts.FQrIts := QrGruUsrEdtIts;
    FmGruUsrEdtIts.FCodigo := QrGruUsrEdtCabCodigo.Value;
    FmGruUsrEdtIts.EdCodigo.ValueVariant := QrGruUsrEdtCabCodigo.Value;
    FmGruUsrEdtIts.EdNome.ValueVariant := QrGruUsrEdtCabNome.Value;
    if SQLType = stIns then
      //
    else
    begin
      FmGruUsrEdtIts.EdControle.ValueVariant := QrGruUsrEdtItsControle.Value;
      //
      FmGruUsrEdtIts.EdUsuario.ValueVariant := QrGruUsrEdtItsUsuario.Value;
      FmGruUsrEdtIts.CBUsuario.KeyValue     := QrGruUsrEdtItsUsuario.Value;
    end;
    FmGruUsrEdtIts.ShowModal;
    FmGruUsrEdtIts.Destroy;
  end;
end;

procedure TFmGruUsrEdtCab.PMCabPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemCabUpd(CabAltera1, QrGruUsrEdtCab);
  MyObjects.HabilitaMenuItemCabDel(CabExclui1, QrGruUsrEdtCab, QrGruUsrEdtIts);
end;

procedure TFmGruUsrEdtCab.PMItsPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(ItsInclui1, QrGruUsrEdtCab);
  MyObjects.HabilitaMenuItemItsUpd(ItsAltera1, QrGruUsrEdtIts);
  MyObjects.HabilitaMenuItemItsDel(ItsExclui1, QrGruUsrEdtIts);
end;

procedure TFmGruUsrEdtCab.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrGruUsrEdtCabCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmGruUsrEdtCab.DefParams;
begin
  VAR_GOTOTABELA := 'gruusredtcab';
  VAR_GOTOMYSQLTABLE := QrGruUsrEdtCab;
  VAR_GOTONEG := gotoPiZ;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT *');
  VAR_SQLx.Add('FROM gruusredtcab');
  VAR_SQLx.Add('WHERE Codigo > 0');
  //
  VAR_SQL1.Add('AND Codigo=:P0');
  //
  //VAR_SQL2.Add('AND CodUsu=:P0');
  //
  VAR_SQLa.Add('AND Nome Like :P0');
  //
end;

procedure TFmGruUsrEdtCab.ItsAltera1Click(Sender: TObject);
begin
  MostraGruUsrEdtIts(stUpd);
end;

procedure TFmGruUsrEdtCab.CabExclui1Click(Sender: TObject);
begin
  Geral.MB_Info('A��o n�o implementada! Solicite � Dermatek:' + sLineBreak +
  Caption + sLineBreak + TMenuItem(Sender).Name);
end;

procedure TFmGruUsrEdtCab.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmGruUsrEdtCab.QueryPrincipalAfterOpen;
begin
end;

procedure TFmGruUsrEdtCab.ItsExclui1Click(Sender: TObject);
var
  Controle: Integer;
begin
  if UMyMod.ExcluiRegistroInt1('Confirma a exclus�o do item selecionado?',
  'GruUsrEdtIts', 'Controle', QrGruUsrEdtItsControle.Value, Dmod.MyDB) = ID_YES then
  begin
    Controle := GOTOy.LocalizaPriorNextIntQr(QrGruUsrEdtIts,
      QrGruUsrEdtItsControle, QrGruUsrEdtItsControle.Value);
    ReopenGruUsrEdtIts(Controle);
  end;
end;

procedure TFmGruUsrEdtCab.ReopenGruUsrEdtIts(Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrGruUsrEdtIts, Dmod.MyDB, [
  'SELECT gui.*, usu.Login ',
  'FROM gruusredtits gui ',
  'LEFT JOIN senhas usu ON usu.Numero=gui.Usuario ',
  'WHERE gui.Codigo=' + Geral.FF0(QrGruUsrEdtCabCodigo.Value),
  'ORDER BY usu.Login ',
  '']);
  //
  QrGruUsrEdtIts.Locate('Controle', Controle, []);
end;


procedure TFmGruUsrEdtCab.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmGruUsrEdtCab.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmGruUsrEdtCab.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmGruUsrEdtCab.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmGruUsrEdtCab.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmGruUsrEdtCab.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmGruUsrEdtCab.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrGruUsrEdtCabCodigo.Value;
  Close;
end;

procedure TFmGruUsrEdtCab.ItsInclui1Click(Sender: TObject);
begin
  MostraGruUsrEdtIts(stIns);
end;

procedure TFmGruUsrEdtCab.CabAltera1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrGruUsrEdtCab, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'gruusredtcab');
end;

procedure TFmGruUsrEdtCab.BtConfirmaClick(Sender: TObject);
var
  Nome: String;
  Codigo: Integer;
  SQLType: TSQLType;
begin
  SQLType        := ImgTipo.SQLType;
  Codigo         := EdCodigo.ValueVariant;
  Nome           := EdNome.ValueVariant;
  //
  if MyObjects.FIC(Length(Nome) = 0, EdNome, 'Defina uma descri��o!') then Exit;
  //
  Codigo := UMyMod.BPGS1I32('gruusredtcab', 'Codigo', '', '', tsPos, SQLType, Codigo);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'gruusredtcab', False, [
  'Nome'], [
  'Codigo'], [
  Nome], [
  Codigo], True) then
  begin
    ImgTipo.SQLType := stLok;
    PnDados.Visible := True;
    PnEdita.Visible := False;
    GOTOy.BotoesSb(ImgTipo.SQLType);
    //
    LocCod(Codigo, Codigo);
    if FSeq = 1 then Close;
  end;
end;

procedure TFmGruUsrEdtCab.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := EdCodigo.ValueVariant;
  if ImgTipo.SQLType = stIns then
    UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'gruusredtcab', Codigo);
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'gruusredtcab', 'Codigo');
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmGruUsrEdtCab.BtItsClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMIts, BtIts);
end;

procedure TFmGruUsrEdtCab.BtCabClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMCab, BtCab);
end;

procedure TFmGruUsrEdtCab.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  GBEdita.Align := alClient;
  DGDados.Align := alClient;
  CriaOForm;
  FSeq := 0;
end;

procedure TFmGruUsrEdtCab.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrGruUsrEdtCabCodigo.Value, LaRegistro.Caption);
end;

procedure TFmGruUsrEdtCab.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmGruUsrEdtCab.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrGruUsrEdtCabCodigo.Value, LaRegistro.Caption);
end;

procedure TFmGruUsrEdtCab.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmGruUsrEdtCab.QrGruUsrEdtCabAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmGruUsrEdtCab.QrGruUsrEdtCabAfterScroll(DataSet: TDataSet);
begin
  ReopenGruUsrEdtIts(0);
end;

procedure TFmGruUsrEdtCab.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //
  if FSeq = 1 then
    CabInclui1Click(Self)
  else
  if (not FLocIni) and (FCabIni <> 0)  then
  begin
    LocCod(FCabIni, FCabIni);
    if QrGruUsrEdtCabCodigo.Value <> FCabIni then
      Geral.MB_Aviso('Grupo n�o localizado!');
    FLocIni := True;
  end;
end;

procedure TFmGruUsrEdtCab.SbQueryClick(Sender: TObject);
begin
  LocCod(QrGruUsrEdtCabCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'gruusredtcab', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmGruUsrEdtCab.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmGruUsrEdtCab.CabInclui1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrGruUsrEdtCab, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'gruusredtcab');
end;

procedure TFmGruUsrEdtCab.QrGruUsrEdtCabBeforeClose(
  DataSet: TDataSet);
begin
  QrGruUsrEdtIts.Close;
end;

procedure TFmGruUsrEdtCab.QrGruUsrEdtCabBeforeOpen(DataSet: TDataSet);
begin
  QrGruUsrEdtCabCodigo.DisplayFormat := FFormatFloat;
end;

end.

