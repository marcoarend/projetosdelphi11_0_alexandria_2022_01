object FmMatriz: TFmMatriz
  Left = 394
  Top = 170
  Caption = 'SYS-PARAM-000 :: Configura'#231#227'o da Empresa'
  ClientHeight = 592
  ClientWidth = 784
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCloseQuery = FormCloseQuery
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PageControl1: TPageControl
    Left = 0
    Top = 92
    Width = 784
    Height = 360
    ActivePage = TabSheet1
    Align = alClient
    TabOrder = 0
    OnChange = PageControl1Change
    object TabSheet1: TTabSheet
      Caption = 'Cadastro'
      object GBCadastroDados: TGroupBox
        Left = 0
        Top = 0
        Width = 776
        Height = 149
        Align = alTop
        TabOrder = 0
        object Panel7: TPanel
          Left = 2
          Top = 15
          Width = 772
          Height = 132
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          object Label1: TLabel
            Left = 12
            Top = 8
            Width = 152
            Height = 13
            Caption = 'Raz'#227'o Social / Nome Completo:'
            FocusControl = DBRazao
          end
          object Label9: TLabel
            Left = 12
            Top = 48
            Width = 61
            Height = 13
            Caption = 'CNPJ / CPF:'
            FocusControl = DBCNPJ
          end
          object Label3: TLabel
            Left = 156
            Top = 48
            Width = 60
            Height = 13
            Caption = 'Master login:'
            FocusControl = DBLogin
          end
          object Label7: TLabel
            Left = 364
            Top = 48
            Width = 67
            Height = 13
            Caption = 'Master senha:'
            FocusControl = DBSenha
          end
          object Label2: TLabel
            Left = 556
            Top = 48
            Width = 66
            Height = 13
            Caption = 'Senha Admin:'
            FocusControl = DBEdit1
          end
          object DBRazao: TDBEdit
            Left = 12
            Top = 24
            Width = 752
            Height = 21
            Hint = 'N'#186' do banco'
            TabStop = False
            DataField = 'entEm'
            DataSource = DsEmpresa
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            MaxLength = 3
            ParentFont = False
            ParentShowHint = False
            ReadOnly = True
            ShowHint = True
            TabOrder = 0
          end
          object DBCNPJ: TDBEdit
            Left = 12
            Top = 64
            Width = 141
            Height = 21
            Hint = 'Nome do banco'
            DataField = 'CNPJ_TXT'
            DataSource = DsEmpresa
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            ParentShowHint = False
            ShowHint = True
            TabOrder = 1
          end
          object DBLogin: TDBEdit
            Left = 156
            Top = 64
            Width = 201
            Height = 21
            DataField = 'MasLogin'
            DataSource = DsEmpresa
            TabOrder = 2
          end
          object DBSenha: TDBEdit
            Left = 364
            Top = 64
            Width = 184
            Height = 22
            DataField = 'DECR_SNHA'
            DataSource = DsEmpresa
            Font.Charset = SYMBOL_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'Wingdings'
            Font.Style = []
            ParentFont = False
            PasswordChar = 'l'
            TabOrder = 3
          end
          object DBCheckBox1: TDBCheckBox
            Left = 12
            Top = 96
            Width = 393
            Height = 17
            Caption = 
              'Usa controle de acesso a dados de empresas por usu'#225'rios cadastra' +
              'dos.'
            DataField = 'UsaAccMngr'
            DataSource = DsEmpresa
            TabOrder = 4
            ValueChecked = '1'
            ValueUnchecked = '0'
          end
          object DBEdit1: TDBEdit
            Left = 556
            Top = 64
            Width = 184
            Height = 22
            DataField = 'DECR_ADMIN'
            DataSource = DsEmpresa
            Font.Charset = SYMBOL_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'Wingdings'
            Font.Style = []
            ParentFont = False
            PasswordChar = 'l'
            TabOrder = 5
          end
        end
      end
      object GBCadastroEdita: TGroupBox
        Left = 0
        Top = 149
        Width = 776
        Height = 183
        Align = alClient
        TabOrder = 1
        Visible = False
        object PainelDados2: TPanel
          Left = 2
          Top = 15
          Width = 772
          Height = 166
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          object Label13: TLabel
            Left = 12
            Top = 8
            Width = 152
            Height = 13
            Caption = 'Raz'#227'o Social / Nome Completo:'
            FocusControl = EdRazao
          end
          object Label15: TLabel
            Left = 156
            Top = 48
            Width = 60
            Height = 13
            Caption = 'Master login:'
          end
          object Label19: TLabel
            Left = 360
            Top = 48
            Width = 67
            Height = 13
            Caption = 'Master senha:'
          end
          object Label21: TLabel
            Left = 12
            Top = 48
            Width = 61
            Height = 13
            Caption = 'CNPJ / CPF:'
            FocusControl = EdCNPJ
          end
          object LaSenhaAdmin: TLabel
            Left = 552
            Top = 48
            Width = 66
            Height = 13
            Caption = 'Senha Admin:'
            Enabled = False
          end
          object SpeedButton1: TSpeedButton
            Left = 740
            Top = 64
            Width = 23
            Height = 22
            Caption = '...'
            OnClick = SpeedButton1Click
          end
          object EdRazao: TdmkEdit
            Left = 12
            Top = 24
            Width = 752
            Height = 21
            Hint = 'N'#186' do banco'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = 8281908
            Font.Height = -12
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            MaxLength = 100
            ParentFont = False
            ParentShowHint = False
            ShowHint = False
            TabOrder = 0
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdCNPJ: TdmkEdit
            Left = 12
            Top = 64
            Width = 141
            Height = 21
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            ParentShowHint = False
            ShowHint = True
            TabOrder = 1
            FormatType = dmktfString
            MskType = fmtCPFJ
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdLogin: TdmkEdit
            Left = 156
            Top = 64
            Width = 201
            Height = 21
            MaxLength = 30
            TabOrder = 2
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object CBUsaAccMngr: TdmkCheckBox
            Left = 12
            Top = 92
            Width = 365
            Height = 17
            Caption = 
              'Usa controle de acesso a dados de empresas por usu'#225'rios cadastra' +
              'dos.'
            TabOrder = 4
            UpdType = utYes
            ValCheck = #0
            ValUncheck = #0
            OldValor = #0
          end
          object EdSenha: TEdit
            Left = 360
            Top = 64
            Width = 184
            Height = 22
            Font.Charset = SYMBOL_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'Wingdings'
            Font.Style = []
            ParentFont = False
            PasswordChar = 'l'
            TabOrder = 3
          end
          object EdSenhaAdmin: TEdit
            Left = 552
            Top = 64
            Width = 184
            Height = 22
            Enabled = False
            Font.Charset = SYMBOL_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'Wingdings'
            Font.Style = []
            ParentFont = False
            PasswordChar = 'l'
            TabOrder = 5
          end
        end
      end
    end
    object TabSheet2: TTabSheet
      Caption = 'Terminais'
      ImageIndex = 1
      object Panel3: TPanel
        Left = 0
        Top = 0
        Width = 776
        Height = 332
        Align = alClient
        BevelOuter = bvNone
        ParentBackground = False
        TabOrder = 0
        object DBGrid1: TDBGrid
          Left = 0
          Top = 0
          Width = 217
          Height = 332
          Align = alLeft
          DataSource = DsTerminais
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'Terminal'
              Width = 48
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'IP'
              Width = 128
              Visible = True
            end>
        end
      end
    end
    object TabSheet3: TTabSheet
      Caption = 'Filiais'
      ImageIndex = 2
      object dmkDBGrid1: TdmkDBGrid
        Left = 0
        Top = 0
        Width = 776
        Height = 261
        Align = alClient
        Columns = <
          item
            Expanded = False
            FieldName = 'Codigo'
            Title.Caption = 'Entidade'
            Width = 51
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Filial'
            Width = 36
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NOMEENT'
            Title.Caption = 'Nome'
            Width = 272
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CNPJCPF_TXT'
            Title.Caption = 'CNPJ/CPF'
            Width = 112
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'IERG'
            Title.Caption = 'I.E./RG'
            Width = 112
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Atividade'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'FormaSociet'
            Title.Caption = 'Forma societ'#225'ria'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Simples'
            Title.Caption = 'Simples Estadual'
            Visible = True
          end>
        Color = clWindow
        DataSource = DsFiliais
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'Codigo'
            Title.Caption = 'Entidade'
            Width = 51
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Filial'
            Width = 36
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NOMEENT'
            Title.Caption = 'Nome'
            Width = 272
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CNPJCPF_TXT'
            Title.Caption = 'CNPJ/CPF'
            Width = 112
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'IERG'
            Title.Caption = 'I.E./RG'
            Width = 112
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Atividade'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'FormaSociet'
            Title.Caption = 'Forma societ'#225'ria'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Simples'
            Title.Caption = 'Simples Estadual'
            Visible = True
          end>
      end
      object GBFiliais: TGroupBox
        Left = 0
        Top = 261
        Width = 776
        Height = 71
        Align = alBottom
        Color = clBtnFace
        ParentBackground = False
        ParentColor = False
        TabOrder = 1
        object Panel1: TPanel
          Left = 629
          Top = 15
          Width = 145
          Height = 54
          Align = alRight
          BevelOuter = bvNone
          TabOrder = 1
          object BtSaida2: TBitBtn
            Tag = 13
            Left = 16
            Top = 3
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Hint = 'Sai da janela atual'
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
        object Panel4: TPanel
          Left = 2
          Top = 15
          Width = 627
          Height = 54
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          object BtFilial: TBitBtn
            Tag = 10049
            Left = 8
            Top = 3
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Caption = '&Filial'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtFilialClick
          end
          object BtUsuario: TBitBtn
            Tag = 10047
            Left = 132
            Top = 3
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Caption = '&Usu'#225'rio'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 1
            OnClick = BtUsuarioClick
          end
          object BtRelMtrz: TBitBtn
            Tag = 5
            Left = 256
            Top = 3
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Caption = '&Relat'#243'rios'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 2
            OnClick = BtRelMtrzClick
          end
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 784
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 736
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 688
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 315
        Height = 32
        Caption = 'Configura'#231#227'o da Empresa'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 315
        Height = 32
        Caption = 'Configura'#231#227'o da Empresa'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 315
        Height = 32
        Caption = 'Configura'#231#227'o da Empresa'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 48
    Width = 784
    Height = 44
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel14: TPanel
      Left = 2
      Top = 15
      Width = 780
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBConfirma: TGroupBox
    Left = 0
    Top = 452
    Width = 784
    Height = 70
    Align = alBottom
    TabOrder = 3
    Visible = False
    object PnSaiDesis: TPanel
      Left = 639
      Top = 15
      Width = 143
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtDesiste: TBitBtn
        Tag = 15
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtDesisteClick
      end
    end
    object Panel15: TPanel
      Left = 2
      Top = 15
      Width = 637
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
    end
  end
  object GBControle: TGroupBox
    Left = 0
    Top = 522
    Width = 784
    Height = 70
    Align = alBottom
    TabOrder = 4
    object Panel16: TPanel
      Left = 639
      Top = 15
      Width = 143
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel17: TPanel
      Left = 2
      Top = 15
      Width = 637
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtAltera: TBitBtn
        Tag = 11
        Left = 136
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Altera'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        OnClick = BtAlteraClick
      end
      object BtDefine: TBitBtn
        Tag = 294
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Define'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        Visible = False
        OnClick = BtDefineClick
      end
      object BtVerifiTerceiros: TBitBtn
        Tag = 10001
        Left = 260
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&DB P'#250'blicos'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 2
        OnClick = BtVerifiTerceirosClick
      end
    end
  end
  object QrEmpresa: TMySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrEmpresaCalcFields
    SQL.Strings = (
      
        'SELECT AES_DECRYPT(m.MasSenha, "'#39' + CO_USERSPNOW + '#39'") DECR_SNHA' +
        ', m.*'
      'FROM master m')
    Left = 12
    Top = 8
    object QrEmpresaCNPJ_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CNPJ_TXT'
      Calculated = True
    end
    object QrEmpresaMonitorar: TSmallintField
      FieldName = 'Monitorar'
      Origin = 'master.Monitorar'
    end
    object QrEmpresaMasLogin: TWideStringField
      FieldName = 'MasLogin'
      Origin = 'master.MasLogin'
      Size = 30
    end
    object QrEmpresaMasSenha: TWideStringField
      FieldName = 'MasSenha'
      Origin = 'master.MasSenha'
      Size = 30
    end
    object QrEmpresaMasAtivar: TWideStringField
      FieldName = 'MasAtivar'
      Origin = 'master.MasAtivar'
      Size = 1
    end
    object QrEmpresaSolicitaSenha: TSmallintField
      FieldName = 'SolicitaSenha'
      Origin = 'master.SolicitaSenha'
    end
    object QrEmpresaLicenca: TWideStringField
      FieldName = 'Licenca'
      Origin = 'master.Licenca'
      Size = 50
    end
    object QrEmpresaDistorcao: TIntegerField
      FieldName = 'Distorcao'
      Origin = 'master.Distorcao'
    end
    object QrEmpresaDataI: TDateField
      FieldName = 'DataI'
      Origin = 'master.DataI'
    end
    object QrEmpresaDataF: TDateField
      FieldName = 'DataF'
      Origin = 'master.DataF'
    end
    object QrEmpresaHoje: TDateField
      FieldName = 'Hoje'
      Origin = 'master.Hoje'
    end
    object QrEmpresaHora: TTimeField
      FieldName = 'Hora'
      Origin = 'master.Hora'
    end
    object QrEmpresaLimite: TSmallintField
      FieldName = 'Limite'
      Origin = 'master.Limite'
    end
    object QrEmpresaUsaAccMngr: TSmallintField
      FieldName = 'UsaAccMngr'
    end
    object QrEmpresaDECR_SNHA: TWideStringField
      FieldName = 'DECR_SNHA'
      Size = 30
    end
    object QrEmpresaentCNPJ: TWideStringField
      FieldName = 'entCNPJ'
      Size = 18
    end
    object QrEmpresaentEm: TWideStringField
      FieldName = 'entEm'
      Size = 100
    end
    object QrEmpresaDECR_ADMIN: TWideStringField
      FieldName = 'DECR_ADMIN'
      Size = 255
    end
  end
  object DsEmpresa: TDataSource
    DataSet = QrEmpresa
    Left = 40
    Top = 8
  end
  object TbTerminais: TMySQLTable
    Database = Dmod.MyDB
    TableName = 'terminais'
    Left = 72
    Top = 8
  end
  object DsTerminais: TDataSource
    DataSet = TbTerminais
    Left = 100
    Top = 8
  end
  object QrFiliais: TMySQLQuery
    Database = Dmod.MyDB
    AfterScroll = QrFiliaisAfterScroll
    OnCalcFields = QrFiliaisCalcFields
    SQL.Strings = (
      'SELECT ent.Codigo, ent.Filial,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENT,'
      'IF(ent.Tipo=0, ent.CNPJ, ent.CPF) CNPJCPF, '
      'IF(ent.Tipo=0, ent.IE, ent.RG) IERG,'
      'FormaSociet, Simples, Atividade  '
      'FROM entidades ent'
      'WHERE ent.Codigo < -10')
    Left = 500
    Top = 28
    object QrFiliaisCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrFiliaisFilial: TIntegerField
      FieldName = 'Filial'
      Required = True
    end
    object QrFiliaisNOMEENT: TWideStringField
      FieldName = 'NOMEENT'
      Required = True
      Size = 100
    end
    object QrFiliaisCNPJCPF: TWideStringField
      FieldName = 'CNPJCPF'
      Size = 18
    end
    object QrFiliaisIERG: TWideStringField
      FieldName = 'IERG'
    end
    object QrFiliaisFormaSociet: TWideStringField
      FieldName = 'FormaSociet'
    end
    object QrFiliaisSimples: TSmallintField
      FieldName = 'Simples'
      Required = True
    end
    object QrFiliaisAtividade: TWideStringField
      FieldName = 'Atividade'
      Size = 50
    end
    object QrFiliaisCNPJCPF_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CNPJCPF_TXT'
      Size = 40
      Calculated = True
    end
  end
  object DsFiliais: TDataSource
    DataSet = QrFiliais
    Left = 528
    Top = 28
  end
  object PMFilial: TPopupMenu
    OnPopup = PMFilialPopup
    Left = 136
    Top = 280
    object Incluinovafilial1: TMenuItem
      Caption = '&Inclui nova filial'
      OnClick = Incluinovafilial1Click
    end
    object N3: TMenuItem
      Caption = '-'
    end
    object Alterafilialatual1: TMenuItem
      Caption = '&Altera entidade da filial atual'
      object EntidadeB1: TMenuItem
        Caption = 'Entidade (&B)'
        OnClick = EntidadeB1Click
      end
      object EntidadeA1: TMenuItem
        Caption = 'Entidade (&A)'
        OnClick = EntidadeA1Click
      end
    end
    object Alteraoutrsdadosdafilialatual1: TMenuItem
      Caption = '&Altera outros dados da filial atual'
      OnClick = Alteraoutrsdadosdafilialatual1Click
    end
    object N2: TMenuItem
      Caption = '-'
    end
    object Excluifilial1: TMenuItem
      Caption = '&Exclui filial'
      Enabled = False
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object AJustaCodigodeclienteinterno1: TMenuItem
      Caption = 'A&Justa Codigo de cliente interno'
      OnClick = AJustaCodigodeclienteinterno1Click
    end
  end
  object QrErrFil: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Filial, Codigo, COUNT(*) Itens'
      'FROM entidades'
      'WHERE Codigo < -10'
      'GROUP BY Filial'
      'ORDER BY Itens DESC, Filial')
    Left = 20
    Top = 164
    object QrErrFilFilial: TIntegerField
      FieldName = 'Filial'
    end
    object QrErrFilCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrErrFilItens: TLargeintField
      FieldName = 'Itens'
      Required = True
    end
  end
end
