object FmParamsEmp: TFmParamsEmp
  Left = 368
  Top = 194
  Caption = 'SYS-PARAM-002 :: Par'#226'mentros das Filiais'
  ClientHeight = 850
  ClientWidth = 1054
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelDados: TPanel
    Left = 0
    Top = 96
    Width = 1054
    Height = 754
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    object PageControl1: TPageControl
      Left = 0
      Top = 48
      Width = 1054
      Height = 497
      ActivePage = TabSheet8
      Align = alTop
      TabOrder = 1
      object TabSheet1: TTabSheet
        Caption = 'Miscel'#226'nea '
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object Panel4: TPanel
          Left = 0
          Top = 0
          Width = 1046
          Height = 469
          Align = alClient
          ParentBackground = False
          TabOrder = 0
          object Label4: TLabel
            Left = 8
            Top = 4
            Width = 36
            Height = 13
            Caption = 'Moeda:'
          end
          object Label25: TLabel
            Left = 8
            Top = 44
            Width = 337
            Height = 13
            Caption = 
              'Caminho para logo da empresa  na propor'#231#227'o de 3 x 1 (largura x a' +
              'ltura):'
          end
          object DBLaCartEmisHonFun: TLabel
            Left = 12
            Top = 220
            Width = 253
            Height = 13
            Caption = 'Carteira gen'#233'rica (geral) de pagamento de honor'#225'rios:'
          end
          object DBEdit11: TDBEdit
            Left = 8
            Top = 20
            Width = 40
            Height = 21
            DataField = 'CODUSU_MDA'
            DataSource = DsParamsEmp
            TabOrder = 0
          end
          object DBEdit4: TDBEdit
            Left = 48
            Top = 20
            Width = 164
            Height = 21
            DataField = 'NOMEMOEDA'
            DataSource = DsParamsEmp
            TabOrder = 1
          end
          object DBEdit20: TDBEdit
            Left = 8
            Top = 60
            Width = 756
            Height = 21
            DataField = 'Logo3x1'
            DataSource = DsParamsEmp
            TabOrder = 2
          end
          object GroupBox35: TGroupBox
            Left = 8
            Top = 84
            Width = 757
            Height = 83
            Caption = ' TZD UTC e altera'#231#245'es de hor'#225'rio de ver'#227'o: '
            TabOrder = 3
            object Panel63: TPanel
              Left = 2
              Top = 15
              Width = 753
              Height = 66
              Align = alClient
              BevelOuter = bvNone
              TabOrder = 0
              object Label254: TLabel
                Left = 4
                Top = 4
                Width = 50
                Height = 13
                Caption = 'TZD UTC:'
                FocusControl = DBEdit139
              end
              object Label258: TLabel
                Left = 252
                Top = 24
                Width = 82
                Height = 13
                Caption = 'Hor'#225'rio de ver'#227'o:'
              end
              object Label253: TLabel
                Left = 340
                Top = 4
                Width = 144
                Height = 13
                Caption = 'Perguntar de novo  a partir de:'
              end
              object Label255: TLabel
                Left = 492
                Top = 4
                Width = 30
                Height = 13
                Caption = 'In'#237'cio:'
              end
              object Label256: TLabel
                Left = 608
                Top = 4
                Width = 41
                Height = 13
                Caption = 'T'#233'rmino:'
              end
              object DBEdit139: TDBEdit
                Left = 4
                Top = 20
                Width = 134
                Height = 21
                DataField = 'TZD_UTC'
                DataSource = DsParamsEmp
                TabOrder = 0
              end
              object DBEdit143: TDBEdit
                Left = 608
                Top = 20
                Width = 112
                Height = 21
                DataField = 'hVeraoFim_TXT'
                DataSource = DsParamsEmp
                TabOrder = 1
              end
              object DBEdit142: TDBEdit
                Left = 492
                Top = 20
                Width = 112
                Height = 21
                DataField = 'hVeraoIni_TXT'
                DataSource = DsParamsEmp
                TabOrder = 2
              end
              object DBEdit141: TDBEdit
                Left = 340
                Top = 20
                Width = 148
                Height = 21
                DataField = 'hVeraoAsk_TXT'
                DataSource = DsParamsEmp
                TabOrder = 3
              end
              object DBEdit140: TDBEdit
                Left = 140
                Top = 20
                Width = 82
                Height = 21
                DataField = 'TZD_UTC_Str'
                DataSource = DsPrmsEmpMis
                TabOrder = 4
              end
              object DBCheckBox31: TDBCheckBox
                Left = 6
                Top = 44
                Width = 170
                Height = 17
                Caption = 'Obter fuso hor'#225'rio do servidor'
                DataField = 'TZD_UTC_Auto'
                DataSource = DsParamsEmp
                TabOrder = 5
                ValueChecked = '1'
                ValueUnchecked = '0'
              end
            end
          end
          object DBEdCartEmisHonFun: TDBEdit
            Left = 12
            Top = 236
            Width = 56
            Height = 21
            DataField = 'CartEmisHonFun'
            DataSource = DsParamsEmp
            TabOrder = 4
          end
          object DBCBCartEmisHonFun: TDBEdit
            Left = 70
            Top = 236
            Width = 464
            Height = 21
            DataField = 'NO_CartEmisHonFun'
            DataSource = DsParamsEmp
            TabOrder = 5
          end
          object DBRadioGroup02: TDBRadioGroup
            Left = 9
            Top = 167
            Width = 756
            Height = 46
            Caption = ' Lei da Transpar'#234'ncia dos Tributos:'
            Columns = 4
            DataField = 'NFeNT2013_003LTT'
            DataSource = DsParamsEmp
            Items.Strings = (
              'Desabilitar'
              'Informar zerado'
              'Informar valor aproximado'
              'Informar e imprimir valor aproximado')
            TabOrder = 6
            Values.Strings = (
              '0'
              '1'
              '2'
              '3'
              '4'
              '5'
              '6'
              '7'
              '8'
              '9')
          end
        end
      end
      object TabSheet2: TTabSheet
        Caption = ' Pedidos '
        ImageIndex = 1
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object Panel6: TPanel
          Left = 0
          Top = 0
          Width = 1046
          Height = 469
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          object Panel11: TPanel
            Left = 0
            Top = 0
            Width = 1046
            Height = 125
            Align = alTop
            BevelOuter = bvNone
            TabOrder = 0
            object Label6: TLabel
              Left = 8
              Top = 4
              Width = 45
              Height = 13
              Caption = 'Situa'#231#227'o:'
            end
            object DBEdit10: TDBEdit
              Left = 8
              Top = 20
              Width = 24
              Height = 21
              DataField = 'Situacao'
              DataSource = DsParamsEmp
              TabOrder = 0
            end
            object DBEdit8: TDBEdit
              Left = 32
              Top = 20
              Width = 140
              Height = 21
              DataField = 'NOMESITUACAO'
              DataSource = DsPrmsEmpMis
              TabOrder = 1
            end
            object DBRadioGroup1: TDBRadioGroup
              Left = 180
              Top = 4
              Width = 277
              Height = 38
              Caption = ' C'#225'lculo de prazo m'#233'dio'
              Columns = 3
              DataField = 'TipMediaDD'
              DataSource = DsParamsEmp
              Items.Strings = (
                'Nenhum'
                'M'#233'dia simples'
                'M'#233'dia real')
              TabOrder = 2
              Values.Strings = (
                '0'
                '1'
                '2')
            end
            object DBRadioGroup2: TDBRadioGroup
              Left = 464
              Top = 44
              Width = 304
              Height = 38
              Caption = ' Permite inclus'#227'o de itens sem pre'#231'o de lista em pedido: '
              Columns = 2
              DataField = 'FatSemPrcL'
              DataSource = DsParamsEmp
              Items.Strings = (
                'N'#227'o permite'
                'Sim, permite')
              TabOrder = 3
              Values.Strings = (
                '0'
                '1')
            end
            object DBRadioGroup3: TDBRadioGroup
              Left = 8
              Top = 44
              Width = 449
              Height = 38
              Caption = ' Permite faturamento de item sem estoque suficiente: '
              Columns = 3
              DataField = 'FatSemEstq'
              DataSource = DsParamsEmp
              Items.Strings = (
                'N'#227'o permite'
                'Permite sem avisar'
                'Perguntar antes')
              TabOrder = 4
              Values.Strings = (
                '0'
                '1'
                '2')
            end
            object DBRadioGroup6: TDBRadioGroup
              Left = 464
              Top = 4
              Width = 304
              Height = 38
              Caption = ' C'#225'lculo de juros (lista de pre'#231'os de grupos de produtos): '
              Columns = 3
              DataField = 'TipCalcJuro'
              DataSource = DsParamsEmp
              Items.Strings = (
                'Nenhum'
                'Juro simples'
                'Juro composto')
              TabOrder = 5
              Values.Strings = (
                '0'
                '1'
                '2')
            end
            object DBRadioGroup7: TDBRadioGroup
              Left = 8
              Top = 84
              Width = 373
              Height = 38
              Caption = ' Permite alterar lista de pre'#231'os ap'#243's inclus'#227'o de itens: '
              Columns = 3
              DataField = 'PedVdaMudLista'
              DataSource = DsParamsEmp
              Items.Strings = (
                'N'#227'o permite'
                'Permite ao gerente'
                'Permite a todos')
              TabOrder = 6
              Values.Strings = (
                '0'
                '1'
                '2')
            end
            object DBRadioGroup8: TDBRadioGroup
              Left = 394
              Top = 84
              Width = 373
              Height = 38
              Caption = ' Permite alterar condi'#231#227'o de pagamento ap'#243's inclus'#227'o de itens: '
              Columns = 3
              DataField = 'PedVdaMudPrazo'
              DataSource = DsParamsEmp
              Items.Strings = (
                'N'#227'o permite'
                'Permite ao gerente'
                'Permite a todos')
              TabOrder = 7
              Values.Strings = (
                '0'
                '1'
                '2')
            end
            object DBCheckBox30: TDBCheckBox
              Left = 775
              Top = 20
              Width = 150
              Height = 17
              Caption = 'N'#227'o efetuar verifica'#231#245'es'
              DataField = 'PediVdaNElertas'
              DataSource = DsParamsEmp
              TabOrder = 8
              ValueChecked = '1'
              ValueUnchecked = '0'
            end
          end
        end
      end
      object TabSheet57: TTabSheet
        Caption = 'Faturamento'
        ImageIndex = 10
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object Panel18: TPanel
          Left = 0
          Top = 0
          Width = 1046
          Height = 469
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          object GroupBox6: TGroupBox
            Left = 0
            Top = 0
            Width = 1046
            Height = 185
            Align = alTop
            Caption = ' Parcelamento da fatura: '
            TabOrder = 0
            object GroupBox7: TGroupBox
              Left = 2
              Top = 15
              Width = 91
              Height = 168
              Align = alLeft
              Caption = ' Parcela da fatura: '
              TabOrder = 0
              object Panel19: TPanel
                Left = 2
                Top = 15
                Width = 87
                Height = 29
                Align = alTop
                BevelOuter = bvNone
                TabOrder = 0
                object Label21: TLabel
                  Left = 4
                  Top = 8
                  Width = 52
                  Height = 13
                  Caption = 'Separador:'
                end
                object DBEdit18: TDBEdit
                  Left = 60
                  Top = 4
                  Width = 24
                  Height = 21
                  DataField = 'FaturaSep'
                  DataSource = DsParamsEmp
                  TabOrder = 0
                end
              end
              object DBRadioGroup4: TDBRadioGroup
                Left = 2
                Top = 44
                Width = 87
                Height = 122
                Align = alClient
                Caption = ' Sequencial: '
                DataField = 'FaturaSeq'
                DataSource = DsParamsEmp
                Items.Strings = (
                  'N'#250'meros'
                  'Letras')
                TabOrder = 1
                Values.Strings = (
                  '0'
                  '1')
              end
            end
            object Panel108: TPanel
              Left = 93
              Top = 15
              Width = 951
              Height = 168
              Align = alClient
              BevelOuter = bvNone
              TabOrder = 1
              object Label22: TLabel
                Left = 4
                Top = 4
                Width = 147
                Height = 13
                Caption = 'Conta para venda de produtos:'
              end
              object Label24: TLabel
                Left = 428
                Top = 4
                Width = 176
                Height = 13
                Caption = 'Texto para descri'#231#227'o no lan'#231'amento:'
              end
              object Label67: TLabel
                Left = 4
                Top = 44
                Width = 152
                Height = 13
                Caption = 'Conta para compra de produtos:'
              end
              object Label68: TLabel
                Left = 428
                Top = 44
                Width = 176
                Height = 13
                Caption = 'Texto para descri'#231#227'o no lan'#231'amento:'
              end
              object Label71: TLabel
                Left = 612
                Top = 4
                Width = 47
                Height = 13
                Caption = 'ID Duplic.'
              end
              object Label72: TLabel
                Left = 612
                Top = 44
                Width = 47
                Height = 13
                Caption = 'ID Duplic.'
              end
              object Label284: TLabel
                Left = 4
                Top = 124
                Width = 228
                Height = 13
                Caption = 'Conta para pagamento de presta'#231#227'o de servi'#231'o:'
              end
              object Label285: TLabel
                Left = 4
                Top = 84
                Width = 147
                Height = 13
                Caption = 'Conta para venda de produtos:'
              end
              object Label286: TLabel
                Left = 428
                Top = 84
                Width = 176
                Height = 13
                Caption = 'Texto para descri'#231#227'o no lan'#231'amento:'
              end
              object Label287: TLabel
                Left = 612
                Top = 84
                Width = 47
                Height = 13
                Caption = 'ID Duplic.'
              end
              object Label288: TLabel
                Left = 612
                Top = 124
                Width = 47
                Height = 13
                Caption = 'ID Duplic.'
              end
              object Label289: TLabel
                Left = 428
                Top = 124
                Width = 176
                Height = 13
                Caption = 'Texto para descri'#231#227'o no lan'#231'amento:'
              end
              object DBRadioGroup5: TDBRadioGroup
                Left = 680
                Top = 85
                Width = 120
                Height = 75
                Caption = ' Data do faturamento: '
                DataField = 'FaturaDta'
                DataSource = DsParamsEmp
                Items.Strings = (
                  'Perguntar'
                  'Abertura'
                  'Encerramento')
                TabOrder = 0
                Values.Strings = (
                  '0'
                  '1'
                  '2')
              end
              object DBEdit15: TDBEdit
                Left = 61
                Top = 20
                Width = 364
                Height = 21
                DataField = 'NO_CtaProdVen'
                DataSource = DsParamsEmp
                TabOrder = 1
              end
              object DBEdit17: TDBEdit
                Left = 4
                Top = 20
                Width = 56
                Height = 21
                DataField = 'CtaProdVen'
                DataSource = DsParamsEmp
                TabOrder = 2
              end
              object DBEdit19: TDBEdit
                Left = 428
                Top = 20
                Width = 180
                Height = 21
                DataField = 'TxtProdVen'
                DataSource = DsParamsEmp
                TabOrder = 3
              end
              object DBEdit39: TDBEdit
                Left = 4
                Top = 60
                Width = 56
                Height = 21
                DataField = 'CtaProdCom'
                DataSource = DsParamsEmp
                TabOrder = 4
              end
              object DBEdit40: TDBEdit
                Left = 61
                Top = 60
                Width = 364
                Height = 21
                DataField = 'NO_CtaProdCom'
                DataSource = DsParamsEmp
                TabOrder = 5
              end
              object DBEdit41: TDBEdit
                Left = 428
                Top = 60
                Width = 180
                Height = 21
                DataField = 'TxtProdCom'
                DataSource = DsParamsEmp
                TabOrder = 6
              end
              object DBEdit42: TDBEdit
                Left = 612
                Top = 20
                Width = 48
                Height = 21
                DataField = 'DupProdVen'
                DataSource = DsParamsEmp
                TabOrder = 7
              end
              object DBEdit43: TDBEdit
                Left = 612
                Top = 60
                Width = 48
                Height = 21
                DataField = 'DupProdCom'
                DataSource = DsParamsEmp
                TabOrder = 8
              end
              object DBRadioGroup13: TDBRadioGroup
                Left = 680
                Top = 0
                Width = 225
                Height = 80
                Caption = ' N'#250'mero da Fatura:  '
                DataField = 'FaturaNum'
                DataSource = DsParamsEmp
                Items.Strings = (
                  'C'#243'digo do Faturamento'
                  'N'#250'mero da Nota Fiscal')
                TabOrder = 9
                Values.Strings = (
                  '0'
                  '1')
              end
              object DBRadioGroup14: TDBRadioGroup
                Left = 806
                Top = 86
                Width = 100
                Height = 75
                Caption = ' Itens de pedido: '
                DataField = 'UsaReferen'
                DataSource = DsParamsEmp
                Items.Strings = (
                  'Grade'
                  'Refer.')
                TabOrder = 10
                Values.Strings = (
                  '0'
                  '1')
              end
              object DBEdit151: TDBEdit
                Left = 4
                Top = 140
                Width = 56
                Height = 21
                DataField = 'CtaServicoPg'
                DataSource = DsParamsEmp
                TabOrder = 11
              end
              object DBEdit152: TDBEdit
                Left = 4
                Top = 100
                Width = 56
                Height = 21
                DataField = 'CtaServico'
                DataSource = DsParamsEmp
                TabOrder = 12
              end
              object DBEdit153: TDBEdit
                Left = 61
                Top = 100
                Width = 364
                Height = 21
                DataField = 'NO_CtaServico'
                DataSource = DsParamsEmp
                TabOrder = 13
              end
              object DBEdit154: TDBEdit
                Left = 428
                Top = 100
                Width = 180
                Height = 21
                DataField = 'TxtServico'
                DataSource = DsParamsEmp
                TabOrder = 14
              end
              object DBEdit155: TDBEdit
                Left = 612
                Top = 100
                Width = 48
                Height = 21
                DataField = 'DupServico'
                DataSource = DsParamsEmp
                TabOrder = 15
              end
              object DBEdit156: TDBEdit
                Left = 612
                Top = 140
                Width = 48
                Height = 21
                DataField = 'DupServicoPg'
                DataSource = DsParamsEmp
                TabOrder = 16
              end
              object DBEdit157: TDBEdit
                Left = 428
                Top = 140
                Width = 180
                Height = 21
                DataField = 'TxtServicoPg'
                DataSource = DsParamsEmp
                TabOrder = 17
              end
              object DBEdit158: TDBEdit
                Left = 61
                Top = 140
                Width = 364
                Height = 21
                DataField = 'NO_CtaServicoPg'
                DataSource = DsParamsEmp
                TabOrder = 18
              end
            end
          end
          object GroupBox1: TGroupBox
            Left = 0
            Top = 185
            Width = 1046
            Height = 100
            Align = alTop
            Caption = ' Associada: '
            TabOrder = 1
            Visible = False
            object Label13: TLabel
              Left = 8
              Top = 16
              Width = 44
              Height = 13
              Caption = 'Empresa:'
              FocusControl = DBEdit12
            end
            object Label16: TLabel
              Left = 8
              Top = 57
              Width = 70
              Height = 13
              Caption = 'Modelo de NF:'
            end
            object DBEdit12: TDBEdit
              Left = 64
              Top = 32
              Width = 701
              Height = 21
              DataField = 'NO_ASSOCIADA'
              DataSource = DsParamsEmp
              TabOrder = 0
            end
            object DBEdit13: TDBEdit
              Left = 8
              Top = 32
              Width = 56
              Height = 21
              DataField = 'FI_ASSOCIADA'
              DataSource = DsParamsEmp
              TabOrder = 1
            end
            object DBEdit16: TDBEdit
              Left = 64
              Top = 72
              Width = 701
              Height = 21
              DataField = 'NO_AssocModNF'
              DataSource = DsParamsEmp
              TabOrder = 2
            end
            object DBEdit14: TDBEdit
              Left = 8
              Top = 72
              Width = 56
              Height = 21
              DataField = 'AssocModNF'
              DataSource = DsParamsEmp
              TabOrder = 3
            end
          end
        end
      end
      object TabSheet5: TTabSheet
        Caption = ' Estoque '
        ImageIndex = 2
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object Panel9: TPanel
          Left = 0
          Top = 0
          Width = 1046
          Height = 65
          Align = alTop
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          object Label11: TLabel
            Left = 8
            Top = 12
            Width = 190
            Height = 13
            Caption = 'Quantidade padr'#227'o de itens no balan'#231'o:'
          end
          object DBEdit9: TDBEdit
            Left = 204
            Top = 8
            Width = 80
            Height = 21
            DataField = 'BalQtdItem'
            DataSource = DsParamsEmp
            TabOrder = 0
          end
          object DBCheckBox28: TDBCheckBox
            Left = 8
            Top = 40
            Width = 293
            Height = 17
            Caption = 'Estoque nulo para materiais no uso e consumo.'
            DataField = 'Estq0UsoCons'
            DataSource = DsParamsEmp
            TabOrder = 1
            ValueChecked = '1'
            ValueUnchecked = '0'
          end
        end
        object Panel109: TPanel
          Left = 0
          Top = 65
          Width = 1046
          Height = 404
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 1
          object GroupBox69: TGroupBox
            Left = 8
            Top = 16
            Width = 765
            Height = 57
            Caption = ' Frete: '
            TabOrder = 0
            object Panel110: TPanel
              Left = 2
              Top = 15
              Width = 761
              Height = 40
              Align = alClient
              BevelOuter = bvNone
              TabOrder = 0
              object Label452: TLabel
                Left = 120
                Top = 0
                Width = 66
                Height = 13
                Caption = '% Ret. ICMS :'
              end
              object Label453: TLabel
                Left = 296
                Top = 0
                Width = 54
                Height = 13
                Caption = '% Ret. PIS:'
              end
              object Label454: TLabel
                Left = 472
                Top = 0
                Width = 70
                Height = 13
                Caption = '% R. COFINS :'
              end
              object DBEdit165: TDBEdit
                Left = 120
                Top = 16
                Width = 68
                Height = 21
                DataField = 'FreteRpICMS'
                DataSource = DsParamsEmp
                TabOrder = 0
              end
              object DBEdit166: TDBEdit
                Left = 296
                Top = 16
                Width = 68
                Height = 21
                DataField = 'FreteRpPIS'
                DataSource = DsParamsEmp
                TabOrder = 1
              end
              object DBEdit167: TDBEdit
                Left = 472
                Top = 16
                Width = 68
                Height = 21
                DataField = 'FreteRpCOFINS'
                DataSource = DsParamsEmp
                TabOrder = 2
              end
            end
          end
          object DBCheckBox34: TDBCheckBox
            Left = 8
            Top = 0
            Width = 293
            Height = 17
            Caption = 'Se credita de impostos na compra e frete.'
            DataField = 'RetImpost'
            DataSource = DsParamsEmp
            TabOrder = 1
            ValueChecked = '1'
            ValueUnchecked = '0'
          end
        end
      end
      object TabSheet8: TTabSheet
        Caption = ' NF-e '
        ImageIndex = 4
        object Panel14: TPanel
          Left = 0
          Top = 0
          Width = 1046
          Height = 469
          Align = alClient
          ParentBackground = False
          TabOrder = 0
          object PageControl4: TPageControl
            Left = 1
            Top = 1
            Width = 1044
            Height = 467
            ActivePage = TabSheet20
            Align = alClient
            TabOrder = 0
            object TabSheet12: TTabSheet
              Caption = ' Configura'#231#245'es gerais '
              ExplicitLeft = 0
              ExplicitTop = 0
              ExplicitWidth = 0
              ExplicitHeight = 0
              object Panel21: TPanel
                Left = 0
                Top = 0
                Width = 1036
                Height = 439
                Align = alClient
                BevelOuter = bvNone
                ParentBackground = False
                TabOrder = 0
                object Panel30: TPanel
                  Left = 0
                  Top = 125
                  Width = 1036
                  Height = 88
                  Align = alTop
                  BevelOuter = bvNone
                  TabOrder = 0
                  object Label90: TLabel
                    Left = 8
                    Top = 4
                    Width = 192
                    Height = 13
                    Caption = 'Pr'#233'-email para envio de NF-e autorizada:'
                  end
                  object Label91: TLabel
                    Left = 8
                    Top = 44
                    Width = 193
                    Height = 13
                    Caption = 'Pr'#233'-email para envio de NF-e cancelada:'
                  end
                  object Label216: TLabel
                    Left = 522
                    Top = 4
                    Width = 250
                    Height = 13
                    Caption = 'Pr'#233'-email para envio de carta de corre'#231#227'o eletr'#244'nica:'
                  end
                  object DBEdit52: TDBEdit
                    Left = 8
                    Top = 20
                    Width = 56
                    Height = 21
                    DataField = 'PreMailAut'
                    DataSource = DsPrmsEmpNFe
                    TabOrder = 0
                  end
                  object DBEdit53: TDBEdit
                    Left = 64
                    Top = 20
                    Width = 450
                    Height = 21
                    DataField = 'NO_PREMAILAUT'
                    DataSource = DsPrmsEmpNFe
                    TabOrder = 1
                  end
                  object DBEdit54: TDBEdit
                    Left = 8
                    Top = 60
                    Width = 56
                    Height = 21
                    DataField = 'PreMailCan'
                    DataSource = DsPrmsEmpNFe
                    TabOrder = 2
                  end
                  object DBEdit55: TDBEdit
                    Left = 64
                    Top = 60
                    Width = 450
                    Height = 21
                    DataField = 'NO_PREMAILCAN'
                    DataSource = DsPrmsEmpNFe
                    TabOrder = 3
                  end
                  object DBEdit117: TDBEdit
                    Left = 522
                    Top = 20
                    Width = 56
                    Height = 21
                    DataField = 'PreMailEveCCe'
                    DataSource = DsPrmsEmpNFe
                    TabOrder = 4
                  end
                  object DBEdit118: TDBEdit
                    Left = 578
                    Top = 20
                    Width = 450
                    Height = 21
                    DataField = 'NO_PREMAILEVECCE'
                    DataSource = DsPrmsEmpNFe
                    TabOrder = 5
                  end
                end
                object Panel28: TPanel
                  Left = 0
                  Top = 0
                  Width = 1036
                  Height = 125
                  Align = alTop
                  BevelOuter = bvNone
                  TabOrder = 1
                  object GroupBox39: TGroupBox
                    Left = 0
                    Top = 0
                    Width = 769
                    Height = 125
                    Align = alLeft
                    TabOrder = 0
                    object Panel58: TPanel
                      Left = 2
                      Top = 15
                      Width = 765
                      Height = 105
                      Align = alTop
                      BevelOuter = bvNone
                      TabOrder = 0
                      object Label74: TLabel
                        Left = 7
                        Top = 60
                        Width = 223
                        Height = 13
                        Caption = 'N'#250'mero de S'#233'rie do Certificado Digital da NF-e:'
                      end
                      object Label42: TLabel
                        Left = 8
                        Top = 8
                        Width = 69
                        Height = 13
                        Caption = 'UF da SEFAZ:'
                      end
                      object Label62: TLabel
                        Left = 141
                        Top = 8
                        Width = 395
                        Height = 13
                        Caption = 
                          'Sigla para adicionar '#224's informa'#231#245'es de produtos customizados na ' +
                          ' NF-e (infAdProd):'
                      end
                      object Label77: TLabel
                        Left = 8
                        Top = 36
                        Width = 83
                        Height = 13
                        Caption = 'WebService [F4]:'
                      end
                      object Label95: TLabel
                        Left = 8
                        Top = 84
                        Width = 181
                        Height = 13
                        Caption = 'Data da validade do certificado digital:'
                      end
                      object Label96: TLabel
                        Left = 336
                        Top = 84
                        Width = 268
                        Height = 13
                        Caption = 'Avisar a expira'#231#227'o da validade do certificado digital com '
                      end
                      object Label97: TLabel
                        Left = 652
                        Top = 84
                        Width = 105
                        Height = 13
                        Caption = 'dias de anteced'#234'ncia.'
                      end
                      object DBEdit44: TDBEdit
                        Left = 240
                        Top = 56
                        Width = 517
                        Height = 21
                        DataField = 'NFeSerNum'
                        DataSource = DsPrmsEmpNFe
                        TabOrder = 0
                      end
                      object DBEdit27: TDBEdit
                        Left = 104
                        Top = 4
                        Width = 32
                        Height = 21
                        DataField = 'UF_WebServ'
                        DataSource = DsPrmsEmpNFe
                        TabOrder = 1
                      end
                      object DBEdit45: TDBEdit
                        Left = 96
                        Top = 32
                        Width = 40
                        Height = 21
                        DataField = 'UF_Servico'
                        DataSource = DsPrmsEmpNFe
                        TabOrder = 2
                      end
                      object DBCheckBox3: TDBCheckBox
                        Left = 141
                        Top = 32
                        Width = 173
                        Height = 17
                        Caption = 'Ativo no super simples (federal).'
                        DataField = 'SimplesFed'
                        DataSource = DsPrmsEmpNFe
                        TabOrder = 3
                        ValueChecked = '1'
                        ValueUnchecked = '0'
                      end
                      object DBCheckBox1: TDBCheckBox
                        Left = 314
                        Top = 32
                        Width = 453
                        Height = 17
                        Caption = 
                          'Informa percentual de customiza'#231#227'o nas informa'#231#245'es adicionais do' +
                          's produtos na NF-e.'
                        DataField = 'InfoPerCuz'
                        DataSource = DsPrmsEmpNFe
                        TabOrder = 4
                        ValueChecked = '1'
                        ValueUnchecked = '0'
                      end
                      object DBEdit37: TDBEdit
                        Left = 559
                        Top = 5
                        Width = 200
                        Height = 21
                        DataField = 'SiglaCustm'
                        DataSource = DsPrmsEmpNFe
                        TabOrder = 5
                      end
                      object DBEdit56: TDBEdit
                        Left = 200
                        Top = 80
                        Width = 112
                        Height = 21
                        DataField = 'NFeSerVal'
                        DataSource = DsPrmsEmpNFe
                        TabOrder = 6
                      end
                      object DBEdit57: TDBEdit
                        Left = 608
                        Top = 80
                        Width = 40
                        Height = 21
                        DataField = 'NFeSerAvi'
                        DataSource = DsParamsEmp
                        TabOrder = 7
                      end
                    end
                  end
                  object GroupBox40: TGroupBox
                    Left = 769
                    Top = 0
                    Width = 267
                    Height = 125
                    Align = alClient
                    Caption = ' WebServices espec'#237'ficos: '
                    TabOrder = 1
                    object Panel59: TPanel
                      Left = 2
                      Top = 15
                      Width = 263
                      Height = 108
                      Align = alClient
                      BevelOuter = bvNone
                      TabOrder = 0
                      object Label241: TLabel
                        Left = 8
                        Top = 4
                        Width = 139
                        Height = 13
                        Caption = 'Manifesta'#231#227'o do destinat'#225'rio:'
                      end
                      object Label242: TLabel
                        Left = 8
                        Top = 28
                        Width = 124
                        Height = 13
                        Caption = 'Consulta NF-e destinadas:'
                      end
                      object Label243: TLabel
                        Left = 8
                        Top = 52
                        Width = 77
                        Height = 13
                        Caption = 'Download NF-e:'
                      end
                      object DBEdit130: TDBEdit
                        Left = 152
                        Top = 0
                        Width = 40
                        Height = 21
                        DataField = 'UF_MDeMDe'
                        DataSource = DsPrmsEmpNFe
                        TabOrder = 0
                      end
                      object DBEdit131: TDBEdit
                        Left = 152
                        Top = 24
                        Width = 40
                        Height = 21
                        DataField = 'UF_MDeDes'
                        DataSource = DsPrmsEmpNFe
                        TabOrder = 1
                      end
                      object DBEdit132: TDBEdit
                        Left = 152
                        Top = 48
                        Width = 40
                        Height = 21
                        DataField = 'UF_MDeNFe'
                        DataSource = DsPrmsEmpNFe
                        TabOrder = 2
                      end
                    end
                  end
                end
                object Panel29: TPanel
                  Left = 0
                  Top = 213
                  Width = 1036
                  Height = 226
                  Align = alClient
                  BevelOuter = bvNone
                  TabOrder = 2
                  object Panel38: TPanel
                    Left = 0
                    Top = 0
                    Width = 1036
                    Height = 108
                    Align = alTop
                    BevelOuter = bvNone
                    TabOrder = 0
                    object GroupBox42: TGroupBox
                      Left = 0
                      Top = 0
                      Width = 121
                      Height = 108
                      Align = alLeft
                      Caption = ' Conting'#234'ncia SCAN: '
                      TabOrder = 0
                      object Panel62: TPanel
                        Left = 2
                        Top = 15
                        Width = 117
                        Height = 91
                        Align = alClient
                        BevelOuter = bvNone
                        TabOrder = 0
                        object Label248: TLabel
                          Left = 8
                          Top = 4
                          Width = 27
                          Height = 13
                          Caption = 'S'#233'rie:'
                        end
                        object Label249: TLabel
                          Left = 8
                          Top = 44
                          Width = 58
                          Height = 13
                          Caption = #218'ltima NF-e:'
                        end
                        object DBEdit135: TDBEdit
                          Left = 8
                          Top = 20
                          Width = 80
                          Height = 21
                          DataField = 'SCAN_Ser'
                          DataSource = DsPrmsEmpNFe
                          TabOrder = 0
                        end
                        object DBEdit136: TDBEdit
                          Left = 8
                          Top = 60
                          Width = 80
                          Height = 21
                          DataField = 'SCAN_nNF'
                          DataSource = DsPrmsEmpNFe
                          TabOrder = 1
                        end
                      end
                    end
                    object Panel60: TPanel
                      Left = 121
                      Top = 0
                      Width = 915
                      Height = 108
                      Align = alClient
                      BevelOuter = bvNone
                      TabOrder = 1
                      object Panel61: TPanel
                        Left = 468
                        Top = 0
                        Width = 447
                        Height = 108
                        Align = alClient
                        BevelOuter = bvNone
                        TabOrder = 0
                        object Label87: TLabel
                          Left = 8
                          Top = 8
                          Width = 282
                          Height = 13
                          Caption = 'Diret'#243'rio das NF-e protocoladas (autorizadas e canceladas):'
                        end
                        object Label140: TLabel
                          Left = 8
                          Top = 48
                          Width = 318
                          Height = 13
                          Caption = 
                            'Diret'#243'rio das NF-e recuperadas da web (autorizadas e canceladas)' +
                            ':'
                        end
                        object DBEdit51: TDBEdit
                          Left = 8
                          Top = 24
                          Width = 424
                          Height = 21
                          DataField = 'DirNFeProt'
                          DataSource = DsPrmsEmpNFe
                          TabOrder = 0
                        end
                        object DBEdit75: TDBEdit
                          Left = 8
                          Top = 64
                          Width = 424
                          Height = 21
                          DataField = 'DirNFeRWeb'
                          DataSource = DsPrmsEmpNFe
                          TabOrder = 1
                        end
                      end
                      object GroupBox68: TGroupBox
                        Left = 0
                        Top = 0
                        Width = 468
                        Height = 108
                        Align = alLeft
                        TabOrder = 1
                        object PnDbinfRespTec_Usa: TPanel
                          Left = 2
                          Top = 15
                          Width = 464
                          Height = 91
                          Align = alClient
                          BevelOuter = bvNone
                          TabOrder = 0
                          object Label447: TLabel
                            Left = 8
                            Top = 4
                            Width = 103
                            Height = 13
                            Caption = 'CNPJ desenvolvedor:'
                          end
                          object Label448: TLabel
                            Left = 148
                            Top = 4
                            Width = 143
                            Height = 13
                            Caption = 'Nome do contato na empresa:'
                          end
                          object Label449: TLabel
                            Left = 340
                            Top = 4
                            Width = 81
                            Height = 13
                            Caption = 'Fone do contato:'
                          end
                          object Label450: TLabel
                            Left = 8
                            Top = 44
                            Width = 142
                            Height = 13
                            Caption = 'e-mail do contato na empresa:'
                          end
                          object Label451: TLabel
                            Left = 188
                            Top = 44
                            Width = 94
                            Height = 13
                            Caption = 'Id e valor do CSRT:'
                          end
                          object DBEdit159: TDBEdit
                            Left = 8
                            Top = 20
                            Width = 137
                            Height = 21
                            DataField = 'infRespTec_CNPJ'
                            DataSource = DsPrmsEmpNFe
                            TabOrder = 0
                          end
                          object DBEdit160: TDBEdit
                            Left = 148
                            Top = 20
                            Width = 189
                            Height = 21
                            DataField = 'infRespTec_xContato'
                            DataSource = DsPrmsEmpNFe
                            TabOrder = 1
                          end
                          object DBEdit161: TDBEdit
                            Left = 340
                            Top = 20
                            Width = 117
                            Height = 21
                            DataField = 'infRespTec_fone'
                            DataSource = DsPrmsEmpNFe
                            TabOrder = 2
                          end
                          object DBEdit162: TDBEdit
                            Left = 8
                            Top = 60
                            Width = 177
                            Height = 21
                            DataField = 'infRespTec_email'
                            DataSource = DsPrmsEmpNFe
                            TabOrder = 3
                          end
                          object DBEdit163: TDBEdit
                            Left = 188
                            Top = 60
                            Width = 21
                            Height = 21
                            DataField = 'infRespTec_idCSRT'
                            DataSource = DsPrmsEmpNFe
                            TabOrder = 4
                          end
                          object DBEdit164: TDBEdit
                            Left = 212
                            Top = 60
                            Width = 245
                            Height = 21
                            DataField = 'infRespTec_CSRT'
                            DataSource = DsPrmsEmpNFe
                            TabOrder = 5
                          end
                        end
                        object CkDBinfRespTec_Usa: TDBCheckBox
                          Left = 12
                          Top = 2
                          Width = 245
                          Height = 17
                          Caption = ' Identifica'#231#227'o do Respons'#225'vel T'#233'cnico: '
                          DataField = 'infRespTec_Usa'
                          DataSource = DsPrmsEmpNFe
                          TabOrder = 1
                          ValueChecked = '1'
                          ValueUnchecked = '0'
                        end
                      end
                    end
                  end
                end
              end
            end
            object TabSheet13: TTabSheet
              Caption = ' Diret'#243'rios de arquivos XML [1] '
              ImageIndex = 1
              ExplicitLeft = 0
              ExplicitTop = 0
              ExplicitWidth = 0
              ExplicitHeight = 0
              object Panel22: TPanel
                Left = 0
                Top = 0
                Width = 1036
                Height = 439
                Align = alClient
                BevelOuter = bvNone
                ParentBackground = False
                TabOrder = 0
                object Label30: TLabel
                  Left = 8
                  Top = 4
                  Width = 174
                  Height = 13
                  Caption = 'Diret'#243'rio das NF-e geradas (-nfe.xml):'
                end
                object Label31: TLabel
                  Left = 8
                  Top = 44
                  Width = 183
                  Height = 13
                  Caption = 'Diret'#243'rio das NF-e assinadas (-nfe.xml):'
                end
                object Label32: TLabel
                  Left = 8
                  Top = 84
                  Width = 275
                  Height = 13
                  Caption = 'Diret'#243'rio dos lotes de envio de NF-e gerados (-env-lot.xml):'
                end
                object Label34: TLabel
                  Left = 8
                  Top = 124
                  Width = 287
                  Height = 13
                  Caption = 'Diret'#243'rio das respostas dos lotes de NF-e enviados (-rec.xml):'
                end
                object Label36: TLabel
                  Left = 8
                  Top = 164
                  Width = 400
                  Height = 13
                  Caption = 
                    'Diret'#243'rio dos pedidos de resultado de processamento de lotes de ' +
                    'NF-e (-ped-rec.xml):'
                end
                object Label38: TLabel
                  Left = 8
                  Top = 204
                  Width = 342
                  Height = 13
                  Caption = 
                    'Diret'#243'rio dos resultados de processamento de lote de NF-e (-pro-' +
                    'rec.xml):'
                end
                object Label45: TLabel
                  Left = 8
                  Top = 244
                  Width = 192
                  Height = 13
                  Caption = 'Diret'#243'rio das NF-e denegadas (-den.xml):'
                end
                object DBEdit21: TDBEdit
                  Left = 8
                  Top = 20
                  Width = 733
                  Height = 21
                  DataField = 'DirNFeGer'
                  DataSource = DsPrmsEmpNFe
                  TabOrder = 0
                end
                object DBEdit22: TDBEdit
                  Left = 8
                  Top = 60
                  Width = 733
                  Height = 21
                  DataField = 'DirNFeAss'
                  DataSource = DsPrmsEmpNFe
                  TabOrder = 1
                end
                object DBEdit23: TDBEdit
                  Left = 8
                  Top = 100
                  Width = 733
                  Height = 21
                  DataField = 'DirEnvLot'
                  DataSource = DsPrmsEmpNFe
                  TabOrder = 2
                end
                object DBEdit24: TDBEdit
                  Left = 8
                  Top = 140
                  Width = 733
                  Height = 21
                  DataField = 'DirRec'
                  DataSource = DsPrmsEmpNFe
                  TabOrder = 3
                end
                object DBEdit25: TDBEdit
                  Left = 8
                  Top = 180
                  Width = 733
                  Height = 21
                  DataField = 'DirPedRec'
                  DataSource = DsPrmsEmpNFe
                  TabOrder = 4
                end
                object DBEdit26: TDBEdit
                  Left = 8
                  Top = 220
                  Width = 733
                  Height = 21
                  DataField = 'DirProRec'
                  DataSource = DsPrmsEmpNFe
                  TabOrder = 5
                end
                object DBEdit28: TDBEdit
                  Left = 8
                  Top = 260
                  Width = 733
                  Height = 21
                  DataField = 'DirDen'
                  DataSource = DsPrmsEmpNFe
                  TabOrder = 6
                end
              end
            end
            object TabSheet15: TTabSheet
              Caption = ' Diret'#243'rios de arquivos XML [2] '
              ImageIndex = 2
              ExplicitLeft = 0
              ExplicitTop = 0
              ExplicitWidth = 0
              ExplicitHeight = 0
              object Panel24: TPanel
                Left = 0
                Top = 0
                Width = 1036
                Height = 439
                Align = alClient
                BevelOuter = bvNone
                ParentBackground = False
                TabOrder = 0
                object Label55: TLabel
                  Left = 8
                  Top = 84
                  Width = 336
                  Height = 13
                  Caption = 
                    'Diret'#243'rio dos pedidos de inutiliza'#231#227'o de n'#250'meros de NF-e (-ped-i' +
                    'nu.xml):'
                end
                object Label56: TLabel
                  Left = 8
                  Top = 124
                  Width = 323
                  Height = 13
                  Caption = 
                    'Diret'#243'rio das respostas de inutiliza'#231#227'o de n'#250'meros de NF-e (-inu' +
                    '.xml):'
                end
                object Label57: TLabel
                  Left = 8
                  Top = 164
                  Width = 346
                  Height = 13
                  Caption = 
                    'Diret'#243'rio dos pedidos de consulta da situa'#231#227'o atual da NF-e (-pe' +
                    'd-sit.xml):'
                end
                object Label58: TLabel
                  Left = 8
                  Top = 204
                  Width = 333
                  Height = 13
                  Caption = 
                    'Diret'#243'rio das respostas de consulta da situa'#231#227'o atual da NF-e (-' +
                    'sit.xml):'
                end
                object Label59: TLabel
                  Left = 8
                  Top = 244
                  Width = 323
                  Height = 13
                  Caption = 
                    'Diret'#243'rio dos pedidos de consulta do status do servi'#231'o (-ped-sta' +
                    '.xml):'
                end
                object Label60: TLabel
                  Left = 8
                  Top = 284
                  Width = 310
                  Height = 13
                  Caption = 
                    'Diret'#243'rio das respostas de consulta do status do servi'#231'o (-sta.x' +
                    'ml):'
                end
                object Label46: TLabel
                  Left = 8
                  Top = 4
                  Width = 297
                  Height = 13
                  Caption = 'Diret'#243'rio dos pedidos de cancelamento de NF-e (-ped-can.xml):'
                end
                object Label54: TLabel
                  Left = 8
                  Top = 44
                  Width = 284
                  Height = 13
                  Caption = 'Diret'#243'rio das respostas de cancelamento de NF-e (-can.xml):'
                end
                object DBEdit29: TDBEdit
                  Left = 8
                  Top = 20
                  Width = 733
                  Height = 21
                  DataField = 'DirPedCan'
                  DataSource = DsPrmsEmpNFe
                  TabOrder = 0
                end
                object DBEdit30: TDBEdit
                  Left = 8
                  Top = 60
                  Width = 733
                  Height = 21
                  DataField = 'DirCan'
                  DataSource = DsPrmsEmpNFe
                  TabOrder = 1
                end
                object DBEdit31: TDBEdit
                  Left = 8
                  Top = 100
                  Width = 733
                  Height = 21
                  DataField = 'DirPedInu'
                  DataSource = DsPrmsEmpNFe
                  TabOrder = 2
                end
                object DBEdit32: TDBEdit
                  Left = 8
                  Top = 140
                  Width = 733
                  Height = 21
                  DataField = 'DirInu'
                  DataSource = DsPrmsEmpNFe
                  TabOrder = 3
                end
                object DBEdit33: TDBEdit
                  Left = 8
                  Top = 180
                  Width = 733
                  Height = 21
                  DataField = 'DirPedSit'
                  DataSource = DsPrmsEmpNFe
                  TabOrder = 4
                end
                object DBEdit34: TDBEdit
                  Left = 8
                  Top = 220
                  Width = 733
                  Height = 21
                  DataField = 'DirSit'
                  DataSource = DsPrmsEmpNFe
                  TabOrder = 5
                end
                object DBEdit35: TDBEdit
                  Left = 8
                  Top = 260
                  Width = 733
                  Height = 21
                  DataField = 'DirPedSta'
                  DataSource = DsPrmsEmpNFe
                  TabOrder = 6
                end
                object DBEdit36: TDBEdit
                  Left = 8
                  Top = 300
                  Width = 733
                  Height = 21
                  DataField = 'DirSta'
                  DataSource = DsPrmsEmpNFe
                  TabOrder = 7
                end
              end
            end
            object TabSheet26: TTabSheet
              Caption = ' Diret'#243'rios de arquivos XML [3] '
              ImageIndex = 3
              ExplicitLeft = 0
              ExplicitTop = 0
              ExplicitWidth = 0
              ExplicitHeight = 0
              object Panel45: TPanel
                Left = 0
                Top = 0
                Width = 1036
                Height = 439
                Align = alClient
                BevelOuter = bvNone
                ParentBackground = False
                TabOrder = 0
                object Label139: TLabel
                  Left = 8
                  Top = 124
                  Width = 324
                  Height = 13
                  Caption = 
                    'Diret'#243'rio das respostas das cartas de corre'#231#227'o eletr'#244'nicas (-cce' +
                    '.xml):'
                end
                object Label141: TLabel
                  Left = 8
                  Top = 204
                  Width = 403
                  Height = 13
                  Caption = 
                    'Diret'#243'rio dos pedidos de cancelamento de NF-e como evento da NF-' +
                    'e (-eve-can.xml):'
                end
                object Label142: TLabel
                  Left = 8
                  Top = 244
                  Width = 390
                  Height = 13
                  Caption = 
                    'Diret'#243'rio das respostas de cancelamento de NF-e como evento da N' +
                    'F-e (-can.xml):'
                end
                object Label146: TLabel
                  Left = 8
                  Top = 4
                  Width = 355
                  Height = 13
                  Caption = 
                    'Diret'#243'rio do envio dos lotes de envio de eventos da NF-e (-eve-e' +
                    'nv-lot.xml):'
                end
                object Label147: TLabel
                  Left = 8
                  Top = 84
                  Width = 337
                  Height = 13
                  Caption = 
                    'Diret'#243'rio dos pedidos das cartas de corre'#231#227'o eletr'#244'nicas (-eve-c' +
                    'ce.xml):'
                end
                object Label143: TLabel
                  Left = 8
                  Top = 44
                  Width = 356
                  Height = 13
                  Caption = 
                    'Diret'#243'rio do retorno dos lotes de envio de eventos da NF-e (-eve' +
                    '-ret-lot.xml):'
                end
                object Label155: TLabel
                  Left = 8
                  Top = 164
                  Width = 435
                  Height = 13
                  Caption = 
                    'Diret'#243'rio de armazenamento e disposi'#231#227'o das cartas de corre'#231#227'o e' +
                    'letr'#244'nicas (-proc-cce.xml):'
                end
                object DBEdit84: TDBEdit
                  Left = 8
                  Top = 20
                  Width = 733
                  Height = 21
                  DataField = 'DirEveEnvLot'
                  DataSource = DsPrmsEmpNFe
                  TabOrder = 0
                end
                object DBEdit85: TDBEdit
                  Left = 8
                  Top = 60
                  Width = 733
                  Height = 21
                  DataField = 'DirEveRetLot'
                  DataSource = DsPrmsEmpNFe
                  TabOrder = 1
                end
                object DBEdit86: TDBEdit
                  Left = 8
                  Top = 100
                  Width = 733
                  Height = 21
                  DataField = 'DirEvePedCCe'
                  DataSource = DsPrmsEmpNFe
                  TabOrder = 2
                end
                object DBEdit87: TDBEdit
                  Left = 8
                  Top = 140
                  Width = 733
                  Height = 21
                  DataField = 'DirEveRetCCe'
                  DataSource = DsPrmsEmpNFe
                  TabOrder = 3
                end
                object DBEdit88: TDBEdit
                  Left = 8
                  Top = 220
                  Width = 733
                  Height = 21
                  DataField = 'DirEvePedCan'
                  DataSource = DsPrmsEmpNFe
                  TabOrder = 4
                end
                object DBEdit89: TDBEdit
                  Left = 8
                  Top = 260
                  Width = 733
                  Height = 21
                  DataField = 'DirEveRetCan'
                  DataSource = DsPrmsEmpNFe
                  TabOrder = 5
                end
                object DBEdit80: TDBEdit
                  Left = 8
                  Top = 180
                  Width = 733
                  Height = 21
                  DataField = 'DirEveProcCCe'
                  DataSource = DsPrmsEmpNFe
                  TabOrder = 6
                end
              end
            end
            object TabSheet36: TTabSheet
              Caption = ' Diret'#243'rios de arquivos XML [4] '
              ImageIndex = 4
              ExplicitLeft = 0
              ExplicitTop = 0
              ExplicitWidth = 0
              ExplicitHeight = 0
              object Panel54: TPanel
                Left = 0
                Top = 0
                Width = 1036
                Height = 439
                Align = alClient
                BevelOuter = bvNone
                ParentBackground = False
                TabOrder = 0
                object Label232: TLabel
                  Left = 8
                  Top = 4
                  Width = 348
                  Height = 13
                  Caption = 
                    'Diret'#243'rio das respostas de consulta de NF-es destinadas (-ret-nf' +
                    'e-des.xml):'
                end
                object Label234: TLabel
                  Left = 8
                  Top = 44
                  Width = 325
                  Height = 13
                  Caption = 
                    'Diret'#243'rio dos envios de manifesta'#231'oes do destinat'#225'rip (-eve-mde.' +
                    'xml):'
                end
                object Label235: TLabel
                  Left = 8
                  Top = 84
                  Width = 318
                  Height = 13
                  Caption = 
                    'Diret'#243'rio das respostas de manifesta'#231#245'es do destinat'#225'rio (-mde.x' +
                    'ml):'
                end
                object Label245: TLabel
                  Left = 8
                  Top = 124
                  Width = 236
                  Height = 13
                  Caption = 'Diret'#243'rio das NF-e baixadas (-ret-dow-nfe-des.xml):'
                end
                object Label247: TLabel
                  Left = 8
                  Top = 164
                  Width = 178
                  Height = 13
                  Caption = 'Diret'#243'rio das NF-e baixadas (-nfe.xml):'
                end
                object Label269: TLabel
                  Left = 8
                  Top = 204
                  Width = 364
                  Height = 13
                  Caption = 
                    'Diret'#243'rio dos envios de Distribui'#231#227'o de DFe de Interesse (-ped-d' +
                    'fe-dis-int.xml):'
                end
                object Label270: TLabel
                  Left = 8
                  Top = 244
                  Width = 365
                  Height = 13
                  Caption = 
                    'Diret'#243'rio dos retornos de Distribui'#231#227'o de DFe de Interesse (-ret' +
                    '-dfe-dis-int.xml):'
                end
                object Label276: TLabel
                  Left = 8
                  Top = 284
                  Width = 379
                  Height = 13
                  Caption = 
                    'Diret'#243'rio dos retornos de Download de NF-es confirmadas  (-ret-d' +
                    'ow-nfe-cnf.xml):'
                end
                object DBEdit127: TDBEdit
                  Left = 8
                  Top = 20
                  Width = 733
                  Height = 21
                  DataField = 'DirRetNfeDes'
                  TabOrder = 0
                end
                object DBEdit128: TDBEdit
                  Left = 8
                  Top = 60
                  Width = 733
                  Height = 21
                  DataField = 'DirEvePedMDe'
                  TabOrder = 1
                end
                object DBEdit129: TDBEdit
                  Left = 8
                  Top = 100
                  Width = 733
                  Height = 21
                  DataField = 'DirEveRetMDe'
                  TabOrder = 2
                end
                object DBEdit133: TDBEdit
                  Left = 8
                  Top = 140
                  Width = 733
                  Height = 21
                  DataField = 'DirDowNFeDes'
                  TabOrder = 3
                end
                object DBEdit134: TDBEdit
                  Left = 8
                  Top = 180
                  Width = 733
                  Height = 21
                  DataField = 'DirDowNFeNFe'
                  TabOrder = 4
                end
                object DBEdit147: TDBEdit
                  Left = 8
                  Top = 220
                  Width = 733
                  Height = 21
                  DataField = 'DirDistDFeInt'
                  TabOrder = 5
                end
                object DBEdit148: TDBEdit
                  Left = 8
                  Top = 260
                  Width = 733
                  Height = 21
                  DataField = 'DirRetDistDFeInt'
                  TabOrder = 6
                end
                object DBEdit150: TDBEdit
                  Left = 8
                  Top = 300
                  Width = 733
                  Height = 21
                  DataField = 'DirDowNFeCnf'
                  TabOrder = 7
                end
              end
            end
            object TabSheet20: TTabSheet
              Caption = ' Op'#231#245'es '
              ImageIndex = 4
              object Panel36: TPanel
                Left = 0
                Top = 0
                Width = 1036
                Height = 439
                Align = alClient
                ParentBackground = False
                TabOrder = 0
                object GroupBox14: TGroupBox
                  Left = 1
                  Top = 1
                  Width = 484
                  Height = 437
                  Align = alLeft
                  Caption = ' Op'#231#245'es at'#233' NF-e 1.10: '
                  TabOrder = 0
                  object GroupBox17: TGroupBox
                    Left = 2
                    Top = 15
                    Width = 480
                    Height = 138
                    Align = alTop
                    Caption = ' Dados e identifica'#231#227'o da NF-e: '
                    TabOrder = 0
                    object Label104: TLabel
                      Left = 12
                      Top = 16
                      Width = 31
                      Height = 13
                      Caption = 'Ver'#227'o:'
                    end
                    object Label105: TLabel
                      Left = 56
                      Top = 16
                      Width = 38
                      Height = 13
                      Caption = 'Modelo:'
                    end
                    object Label112: TLabel
                      Left = 100
                      Top = 16
                      Width = 214
                      Height = 13
                      Caption = 'Caminho dos arquivos do Schema xml (*.xsd):'
                    end
                    object DBRadioGroup11: TDBRadioGroup
                      Left = 12
                      Top = 76
                      Width = 461
                      Height = 45
                      Caption = ' Identifica'#231#227'o do Ambiente: '
                      Columns = 3
                      DataField = 'ide_tpAmb'
                      DataSource = DsPrmsEmpNFe
                      Items.Strings = (
                        '0 - Nenhum'
                        '1 - Produ'#231#227'o'
                        '2 - Homologa'#231#227'o')
                      TabOrder = 0
                      Values.Strings = (
                        '0'
                        '1'
                        '2')
                    end
                    object DBEdit60: TDBEdit
                      Left = 12
                      Top = 32
                      Width = 41
                      Height = 21
                      DataField = 'versao'
                      DataSource = DsPrmsEmpNFe
                      TabOrder = 1
                    end
                    object DBEdit61: TDBEdit
                      Left = 56
                      Top = 32
                      Width = 41
                      Height = 21
                      DataField = 'ide_mod'
                      DataSource = DsPrmsEmpNFe
                      TabOrder = 2
                    end
                    object DBEdit63: TDBEdit
                      Left = 100
                      Top = 32
                      Width = 372
                      Height = 21
                      DataField = 'DirSchema'
                      DataSource = DsPrmsEmpNFe
                      TabOrder = 3
                    end
                    object DBCheckBox32: TDBCheckBox
                      Left = 12
                      Top = 56
                      Width = 205
                      Height = 17
                      Caption = 'Nota t'#233'cnica 2018/005 vers'#227'o 1.20'
                      DataField = 'NT2018_05v120'
                      DataSource = DsPrmsEmpNFe
                      TabOrder = 4
                      ValueChecked = '1'
                      ValueUnchecked = '0'
                    end
                  end
                  object DBRadioGroup10: TDBRadioGroup
                    Left = 2
                    Top = 153
                    Width = 480
                    Height = 44
                    Align = alTop
                    Caption = ' Gerenciamento NF-e: '
                    Columns = 2
                    DataField = 'AppCode'
                    DataSource = DsPrmsEmpNFe
                    Items.Strings = (
                      'Pascal'
                      'C++')
                    TabOrder = 1
                    Values.Strings = (
                      '0'
                      '1')
                  end
                  object DBRadioGroup9: TDBRadioGroup
                    Left = 2
                    Top = 197
                    Width = 480
                    Height = 44
                    Align = alTop
                    Caption = ' Modo de assinar a NF-e no Pascal: '
                    Columns = 2
                    DataField = 'AssDigMode'
                    DataSource = DsPrmsEmpNFe
                    Items.Strings = (
                      'CAPICOM.DLL'
                      '.NET FRAMEWORK')
                    TabOrder = 2
                    Values.Strings = (
                      '0'
                      '1')
                  end
                  object Panel106: TPanel
                    Left = 2
                    Top = 241
                    Width = 480
                    Height = 194
                    Align = alClient
                    BevelOuter = bvNone
                    TabOrder = 3
                    object Label252: TLabel
                      Left = 4
                      Top = 124
                      Width = 66
                      Height = 13
                      Caption = 'Texto padr'#227'o:'
                    end
                    object Label103: TLabel
                      Left = 4
                      Top = 84
                      Width = 461
                      Height = 13
                      Caption = 
                        'Email pr'#243'prio para receber email enviado (mais de um separar por' +
                        ' ponte e v'#237'rgula) m'#225'x. 255 letras:'
                    end
                    object Label153: TLabel
                      Left = 4
                      Top = 44
                      Width = 386
                      Height = 13
                      Caption = 
                        'Tipo de email para envio espec'#237'fico de Carta de Corre'#231#227'o:  (Ex.:' +
                        ' Transportadoras)'
                    end
                    object Label102: TLabel
                      Left = 4
                      Top = 4
                      Width = 157
                      Height = 13
                      Caption = 'Tipo de email para envio de NFe:'
                    end
                    object DBEdit62: TDBEdit
                      Left = 64
                      Top = 20
                      Width = 405
                      Height = 21
                      DataField = 'NO_EntiTipCto'
                      DataSource = DsParamsEmp
                      TabOrder = 0
                    end
                    object DBEdit138: TDBEdit
                      Left = 64
                      Top = 140
                      Width = 405
                      Height = 21
                      DataField = 'NFeInfCpl_TXT'
                      DataSource = DsParamsEmp
                      TabOrder = 1
                    end
                    object DBEdit137: TDBEdit
                      Left = 4
                      Top = 140
                      Width = 56
                      Height = 21
                      DataField = 'NFeInfCpl'
                      DataSource = DsPrmsEmpNFe
                      TabOrder = 2
                    end
                    object DBEdit58: TDBEdit
                      Left = 4
                      Top = 100
                      Width = 465
                      Height = 21
                      DataField = 'MyEmailNFe'
                      DataSource = DsPrmsEmpNFe
                      TabOrder = 3
                    end
                    object DBEdit76: TDBEdit
                      Left = 64
                      Top = 60
                      Width = 405
                      Height = 21
                      DataField = 'NO_EntiTipCt1'
                      DataSource = DsParamsEmp
                      TabOrder = 4
                    end
                    object DBEdit74: TDBEdit
                      Left = 4
                      Top = 60
                      Width = 56
                      Height = 21
                      DataField = 'EntiTipCt1'
                      DataSource = DsParamsEmp
                      TabOrder = 5
                    end
                    object DBEdit59: TDBEdit
                      Left = 4
                      Top = 20
                      Width = 56
                      Height = 21
                      DataField = 'EntiTipCto'
                      DataSource = DsParamsEmp
                      TabOrder = 6
                    end
                  end
                end
                object GroupBox18: TGroupBox
                  Left = 725
                  Top = 1
                  Width = 310
                  Height = 437
                  Align = alClient
                  Caption = 'Op'#231#245'es at'#233' 2.00: '
                  TabOrder = 1
                  object Label108: TLabel
                    Left = 8
                    Top = 156
                    Width = 105
                    Height = 13
                    Caption = 'CSOSN* para CRT=1:'
                  end
                  object Label109: TLabel
                    Left = 4
                    Top = 176
                    Width = 264
                    Height = 13
                    Caption = '*C'#243'digo de Situa'#231#227'o da Opera'#231#227'o no Simples Nacional.'
                  end
                  object RGDBCRT: TDBRadioGroup
                    Left = 2
                    Top = 15
                    Width = 306
                    Height = 134
                    Align = alTop
                    Caption = ' CRT (C'#243'digo do Regime tribut'#225'rio): '
                    DataField = 'CRT'
                    DataSource = DsPrmsEmpNFe
                    Items.Strings = (
                      '0 - Nenhum'
                      '1 - Simples Nacional'
                      '2 - Simples Nacional - excesso de sublimite de receita bruta'
                      '3 - Regime Normal')
                    TabOrder = 0
                    Values.Strings = (
                      '0'
                      '1'
                      '2'
                      '3')
                  end
                  object EdDBCSOSN: TDBEdit
                    Left = 140
                    Top = 152
                    Width = 133
                    Height = 21
                    DataField = 'CSOSN'
                    DataSource = DsPrmsEmpNFe
                    TabOrder = 1
                  end
                  object MeCSOSN_Show: TMemo
                    Left = 8
                    Top = 192
                    Width = 265
                    Height = 45
                    ReadOnly = True
                    TabOrder = 2
                  end
                  object GroupBox20: TGroupBox
                    Left = 2
                    Top = 325
                    Width = 306
                    Height = 110
                    Align = alBottom
                    Caption = ' CSOSN = 101 ou 201: '
                    TabOrder = 3
                    object Label114: TLabel
                      Left = 12
                      Top = 16
                      Width = 140
                      Height = 26
                      Caption = 'Al'#237'quota aplic'#225'vel de c'#225'lculo'#13#10'do cr'#233'dito (Simples Nacional):'
                    end
                    object Label116: TLabel
                      Left = 12
                      Top = 56
                      Width = 107
                      Height = 13
                      Caption = 'AAAAMM de validade:'
                    end
                    object DBEdit64: TDBEdit
                      Left = 160
                      Top = 21
                      Width = 78
                      Height = 21
                      DataField = 'pCredSNAlq'
                      DataSource = DsPrmsEmpNFe
                      TabOrder = 0
                    end
                    object DBEdit65: TDBEdit
                      Left = 160
                      Top = 49
                      Width = 78
                      Height = 21
                      DataField = 'pCredSNMez_TXT'
                      DataSource = DsPrmsEmpMis
                      TabOrder = 1
                    end
                  end
                end
                object GroupBox36: TGroupBox
                  Left = 485
                  Top = 1
                  Width = 240
                  Height = 437
                  Align = alLeft
                  Caption = 'Vers'#227'o do servi'#231'o'
                  TabOrder = 2
                  object Label219: TLabel
                    Left = 5
                    Top = 17
                    Width = 85
                    Height = 13
                    Caption = 'Status do servi'#231'o:'
                  end
                  object Label221: TLabel
                    Left = 5
                    Top = 88
                    Width = 108
                    Height = 13
                    Caption = 'Consultar lote enviado:'
                  end
                  object Label222: TLabel
                    Left = 5
                    Top = 112
                    Width = 138
                    Height = 13
                    Caption = 'Pedir cancelamento de NF-e:'
                  end
                  object Label223: TLabel
                    Left = 5
                    Top = 136
                    Width = 187
                    Height = 13
                    Caption = 'Pedir inutiliza'#231#227'o de n'#250'mero(s) de NF-e:'
                  end
                  object Label224: TLabel
                    Left = 5
                    Top = 160
                    Width = 73
                    Height = 13
                    Caption = 'Consultar NF-e:'
                  end
                  object Label225: TLabel
                    Left = 5
                    Top = 184
                    Width = 147
                    Height = 13
                    Caption = 'Enviar lote de eventos da NFe:'
                  end
                  object Label220: TLabel
                    Left = 5
                    Top = 40
                    Width = 134
                    Height = 13
                    Caption = 'Enviar lote de NF-e ao fisco:'
                  end
                  object Label264: TLabel
                    Left = 5
                    Top = 64
                    Width = 177
                    Height = 13
                    Caption = 'Enviar lote de NF-e sincrono ao fisco:'
                    Enabled = False
                  end
                  object Label266: TLabel
                    Left = 5
                    Top = 208
                    Width = 165
                    Height = 13
                    Caption = 'Consultar cadastro de Contribuinte:'
                  end
                  object Label268: TLabel
                    Left = 5
                    Top = 232
                    Width = 99
                    Height = 13
                    Caption = 'Distribui'#231#227'o de DF-e:'
                  end
                  object Label274: TLabel
                    Left = 5
                    Top = 256
                    Width = 157
                    Height = 13
                    Caption = 'Download de NF-es confirmadas:'
                  end
                  object DBEdit120: TDBEdit
                    Left = 193
                    Top = 12
                    Width = 41
                    Height = 21
                    DataField = 'NFeVerStaSer'
                    DataSource = DsPrmsEmpNFe
                    TabOrder = 0
                  end
                  object DBEdit121: TDBEdit
                    Left = 193
                    Top = 36
                    Width = 41
                    Height = 21
                    DataField = 'NFeVerEnvLot'
                    DataSource = DsPrmsEmpNFe
                    TabOrder = 1
                  end
                  object DBEdit122: TDBEdit
                    Left = 193
                    Top = 84
                    Width = 41
                    Height = 21
                    DataField = 'NFeVerConLot'
                    DataSource = DsPrmsEmpNFe
                    TabOrder = 2
                  end
                  object DBEdit123: TDBEdit
                    Left = 193
                    Top = 108
                    Width = 41
                    Height = 21
                    DataField = 'NFeVerCanNFe'
                    DataSource = DsPrmsEmpNFe
                    TabOrder = 3
                  end
                  object DBEdit124: TDBEdit
                    Left = 193
                    Top = 132
                    Width = 41
                    Height = 21
                    DataField = 'NFeVerInuNum'
                    DataSource = DsPrmsEmpNFe
                    TabOrder = 4
                  end
                  object DBEdit125: TDBEdit
                    Left = 193
                    Top = 156
                    Width = 41
                    Height = 21
                    DataField = 'NFeVerConNFe'
                    DataSource = DsPrmsEmpNFe
                    TabOrder = 5
                  end
                  object DBEdit126: TDBEdit
                    Left = 193
                    Top = 180
                    Width = 41
                    Height = 21
                    DataField = 'NFeVerLotEve'
                    DataSource = DsPrmsEmpNFe
                    TabOrder = 6
                  end
                  object DBEdit144: TDBEdit
                    Left = 193
                    Top = 60
                    Width = 41
                    Height = 21
                    DataSource = DsPrmsEmpNFe
                    Enabled = False
                    TabOrder = 7
                  end
                  object DBEdit145: TDBEdit
                    Left = 193
                    Top = 204
                    Width = 41
                    Height = 21
                    DataField = 'NFeVerConsCad'
                    DataSource = DsPrmsEmpNFe
                    TabOrder = 8
                  end
                  object DBEdit146: TDBEdit
                    Left = 193
                    Top = 228
                    Width = 41
                    Height = 21
                    DataField = 'NFeVerDistDFeInt'
                    DataSource = DsPrmsEmpNFe
                    TabOrder = 9
                  end
                  object DBEdit149: TDBEdit
                    Left = 193
                    Top = 252
                    Width = 41
                    Height = 21
                    DataField = 'NFeVerDowNFe'
                    DataSource = DsPrmsEmpNFe
                    TabOrder = 10
                  end
                end
              end
            end
            object TabSheet24: TTabSheet
              Caption = ' DANFE '
              ImageIndex = 5
              ExplicitLeft = 0
              ExplicitTop = 0
              ExplicitWidth = 0
              ExplicitHeight = 0
              object Panel41: TPanel
                Left = 0
                Top = 0
                Width = 1036
                Height = 439
                Align = alClient
                BevelOuter = bvNone
                ParentBackground = False
                TabOrder = 0
                object Label455: TLabel
                  Left = 12
                  Top = 196
                  Width = 164
                  Height = 13
                  Caption = 'URL Emitente (n'#227'o recomendado):'
                end
                object Label63: TLabel
                  Left = 12
                  Top = 236
                  Width = 327
                  Height = 13
                  Caption = 
                    'Arquivo do logo da empresa (Impress'#227'o no DANFE) (propor'#231#227'o:  1:1' +
                    '):'
                end
                object Label85: TLabel
                  Left = 12
                  Top = 276
                  Width = 106
                  Height = 13
                  Caption = 'Diret'#243'rio das DANFEs:'
                end
                object DBRadioGroup15: TDBRadioGroup
                  Left = 8
                  Top = 52
                  Width = 460
                  Height = 45
                  Caption = ' Linhas por item na DANFE: '
                  Columns = 4
                  DataField = 'NFeItsLin'
                  DataSource = DsPrmsEmpNFe
                  Items.Strings = (
                    '3'
                    '1'
                    '2'
                    '3')
                  TabOrder = 0
                  Values.Strings = (
                    '0'
                    '1'
                    '2'
                    '3')
                end
                object DBRadioGroup12: TDBRadioGroup
                  Left = 8
                  Top = 4
                  Width = 460
                  Height = 45
                  Caption = ' Formato de impress'#227'o: '
                  Columns = 3
                  DataField = 'ide_tpImp'
                  DataSource = DsPrmsEmpNFe
                  Items.Strings = (
                    '0 - Nenhum'
                    '1 - Retrato'
                    '2 - Paisagem')
                  TabOrder = 1
                  Values.Strings = (
                    '0'
                    '1'
                    '2')
                end
                object GroupBox27: TGroupBox
                  Left = 8
                  Top = 100
                  Width = 460
                  Height = 65
                  Caption = ' Tamanho da Fonte: '
                  TabOrder = 2
                  object Label129: TLabel
                    Left = 12
                    Top = 20
                    Width = 66
                    Height = 13
                    Caption = 'Raz'#227'o Social:'
                  end
                  object Label130: TLabel
                    Left = 152
                    Top = 20
                    Width = 49
                    Height = 13
                    Caption = 'Endere'#231'o:'
                  end
                  object Label131: TLabel
                    Left = 292
                    Top = 20
                    Width = 45
                    Height = 13
                    Caption = 'Telefone:'
                  end
                  object DBEdit77: TDBEdit
                    Left = 12
                    Top = 36
                    Width = 134
                    Height = 21
                    DataField = 'NFeFTRazao'
                    DataSource = DsPrmsEmpNFe
                    TabOrder = 0
                  end
                  object DBEdit78: TDBEdit
                    Left = 152
                    Top = 36
                    Width = 134
                    Height = 21
                    DataField = 'NFeFTEnder'
                    DataSource = DsPrmsEmpNFe
                    TabOrder = 1
                  end
                  object DBEdit79: TDBEdit
                    Left = 292
                    Top = 36
                    Width = 134
                    Height = 21
                    DataField = 'NFeFTFones'
                    DataSource = DsPrmsEmpNFe
                    TabOrder = 2
                  end
                end
                object DBCheckBox35: TDBCheckBox
                  Left = 13
                  Top = 172
                  Width = 173
                  Height = 17
                  Caption = 'For'#231'ar texto a ficar mai'#250'sculo.'
                  DataField = 'NFeMaiusc'
                  DataSource = DsPrmsEmpNFe
                  TabOrder = 3
                  ValueChecked = '1'
                  ValueUnchecked = '0'
                end
                object DBEdit168: TDBEdit
                  Left = 12
                  Top = 212
                  Width = 600
                  Height = 21
                  DataField = 'NFeShowURL'
                  DataSource = DsPrmsEmpNFe
                  TabOrder = 4
                end
                object DBEdit38: TDBEdit
                  Left = 12
                  Top = 252
                  Width = 600
                  Height = 21
                  DataField = 'PathLogoNF'
                  DataSource = DsPrmsEmpNFe
                  TabOrder = 5
                end
                object DBEdit50: TDBEdit
                  Left = 12
                  Top = 292
                  Width = 600
                  Height = 21
                  DataField = 'DirDANFEs'
                  DataSource = DsPrmsEmpNFe
                  TabOrder = 6
                end
                object DBRadioGroup19: TDBRadioGroup
                  Left = 10
                  Top = 317
                  Width = 505
                  Height = 39
                  Caption = 
                    ' Informa'#231#227'o dos valores totais aproximados de impostos no campo ' +
                    '"Dados Adicionais" nas NF-es: '
                  Columns = 3
                  DataField = 'NFe_indFinalCpl'
                  DataSource = DsPrmsEmpNFe
                  Items.Strings = (
                    'Indefinido'
                    'Somente consumidor final'
                    'Todas NF-es')
                  TabOrder = 7
                  Values.Strings = (
                    '0'
                    '1'
                    '2'
                    '3'
                    '4'
                    '5'
                    '6'
                    '7'
                    '8'
                    '9')
                end
                object DBRadioGroup17: TDBRadioGroup
                  Left = 10
                  Top = 361
                  Width = 505
                  Height = 39
                  Caption = ' Descri'#231#227'o do DANFE PDF salvo para envio por e-mail'
                  Columns = 3
                  DataField = 'NoDANFEMail'
                  DataSource = DsPrmsEmpNFe
                  Items.Strings = (
                    'Chave de acesso da NF-e'
                    '"NF" + N'#250'mero NF-e + Nome do cliente')
                  TabOrder = 8
                  Values.Strings = (
                    '0'
                    '1'
                    '2'
                    '3'
                    '4'
                    '5'
                    '6'
                    '7'
                    '8'
                    '9')
                end
              end
            end
          end
        end
      end
      object TabSheet23: TTabSheet
        Caption = 'SPED EFD'
        ImageIndex = 6
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object Panel40: TPanel
          Left = 0
          Top = 0
          Width = 1046
          Height = 469
          Align = alClient
          ParentBackground = False
          TabOrder = 0
          object GroupBox22: TGroupBox
            Left = 1
            Top = 61
            Width = 1044
            Height = 64
            Align = alTop
            Caption = ' Registro 0000: '
            TabOrder = 0
            object Label119: TLabel
              Left = 12
              Top = 20
              Width = 189
              Height = 13
              Caption = 'Perfil de apresenta'#231#227'o do arquivo fiscal:'
            end
            object Label120: TLabel
              Left = 208
              Top = 20
              Width = 143
              Height = 13
              Caption = 'Indicador de tipo de atividade:'
            end
            object EdDBSPED_EFD_IND_PERFIL: TdmkEdit
              Left = 32
              Top = 36
              Width = 172
              Height = 21
              ReadOnly = True
              TabOrder = 0
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '?'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = '?'
              ValWarn = False
            end
            object EdDBSPED_EFD_IND_ATIV: TdmkEdit
              Left = 228
              Top = 36
              Width = 536
              Height = 21
              ReadOnly = True
              TabOrder = 1
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '?'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = '?'
              ValWarn = False
            end
            object DBEdit66: TDBEdit
              Left = 12
              Top = 36
              Width = 20
              Height = 21
              DataField = 'SPED_EFD_IND_PERFIL'
              DataSource = DsParamsEmp
              TabOrder = 2
            end
            object DBEdit67: TDBEdit
              Left = 208
              Top = 36
              Width = 20
              Height = 21
              DataField = 'SPED_EFD_IND_ATIV'
              DataSource = DsParamsEmp
              TabOrder = 3
            end
          end
          object GroupBox23: TGroupBox
            Left = 1
            Top = 125
            Width = 1044
            Height = 104
            Align = alTop
            Caption = ' Registro 0100: '
            TabOrder = 1
            object Label121: TLabel
              Left = 12
              Top = 20
              Width = 106
              Height = 13
              Caption = 'Contador respons'#225'vel:'
            end
            object Label125: TLabel
              Left = 440
              Top = 20
              Width = 146
              Height = 13
              Caption = 'CRC do Contador respons'#225'vel:'
            end
            object Label126: TLabel
              Left = 12
              Top = 60
              Width = 183
              Height = 13
              Caption = 'Escrit'#243'rio de contabilidade (se houver):'
            end
            object DBRadioGroup99: TDBRadioGroup
              Left = 604
              Top = 21
              Width = 161
              Height = 77
              Caption = ' Endere'#231'o a se informado: '
              DataField = 'SPED_EFD_EnderContab'
              DataSource = DsParamsEmp
              Items.Strings = (
                'Indefinido'
                'Do contador'
                'Do escrit'#243'rio')
              TabOrder = 0
              Values.Strings = (
                '0'
                '1'
                '2'
                '3'
                '4'
                '5'
                '6'
                '7'
                '8'
                '9')
            end
            object DBEdit68: TDBEdit
              Left = 12
              Top = 36
              Width = 56
              Height = 21
              DataField = 'SPED_EFD_CadContador'
              DataSource = DsParamsEmp
              TabOrder = 1
            end
            object DBEdit69: TDBEdit
              Left = 440
              Top = 36
              Width = 160
              Height = 21
              DataField = 'SPED_EFD_CRCContador'
              DataSource = DsParamsEmp
              TabOrder = 2
            end
            object DBEdit70: TDBEdit
              Left = 12
              Top = 76
              Width = 56
              Height = 21
              DataField = 'SPED_EFD_EscriContab'
              DataSource = DsParamsEmp
              TabOrder = 3
            end
            object DBEdit71: TDBEdit
              Left = 68
              Top = 36
              Width = 368
              Height = 21
              DataField = 'NO_CTD'
              DataSource = DsParamsEmp
              TabOrder = 4
            end
            object DBEdit72: TDBEdit
              Left = 68
              Top = 76
              Width = 532
              Height = 21
              DataField = 'NO_CTB'
              DataSource = DsParamsEmp
              TabOrder = 5
            end
          end
          object GroupBox63: TGroupBox
            Left = 1
            Top = 229
            Width = 1044
            Height = 96
            Align = alTop
            Caption = ' Geral:'
            TabOrder = 2
            object Panel95: TPanel
              Left = 2
              Top = 15
              Width = 1040
              Height = 79
              Align = alClient
              BevelOuter = bvNone
              TabOrder = 0
              object dmkRadioGroup2: TDBRadioGroup
                Left = 0
                Top = 0
                Width = 300
                Height = 79
                Align = alLeft
                Caption = ' Data de Escritura'#231#227'o de Emiss'#245'es:'
                Columns = 2
                DataField = 'SPED_EFD_DtFiscal'
                DataSource = DsParamsEmp
                Items.Strings = (
                  'Indefinido'
                  'Emiss'#227'o'
                  'Saida')
                TabOrder = 0
                Values.Strings = (
                  '0'
                  '1'
                  '2'
                  '3'
                  '4'
                  '5'
                  '6'
                  '7'
                  '8'
                  '9')
              end
              object dmkRadioGroup3: TDBRadioGroup
                Left = 300
                Top = 0
                Width = 300
                Height = 79
                Align = alLeft
                Caption = ' ID no arquivo exporta'#231#227'o - Entidade: '
                Columns = 2
                DataField = 'SPED_EFD_ID_0150'
                DataSource = DsParamsEmp
                Items.Strings = (
                  'Indefinido'
                  'C'#243'digo entidade no app'
                  'C'#243'd. participante SPED'
                  'CNPJ / CPF'
                  '"Dmk_" + C'#243'digo entid.')
                TabOrder = 1
                Values.Strings = (
                  '0'
                  '1'
                  '2'
                  '3'
                  '4'
                  '5'
                  '6'
                  '7'
                  '8'
                  '9')
              end
              object dmkRadioGroup4: TDBRadioGroup
                Left = 600
                Top = 0
                Width = 300
                Height = 79
                Align = alLeft
                Caption = ' ID no arquivo exporta'#231#227'o - Produto: '
                Columns = 2
                DataField = 'SPED_EFD_ID_0200'
                DataSource = DsParamsEmp
                Items.Strings = (
                  'Indefinido'
                  'Reduzido no app'
                  'C'#243'digo produto SPED'
                  'NCM'
                  '"Dmk_" + Reduzido ')
                TabOrder = 2
                Values.Strings = (
                  '0'
                  '1'
                  '2'
                  '3'
                  '4'
                  '5'
                  '6'
                  '7'
                  '8'
                  '9')
              end
            end
          end
          object Panel100: TPanel
            Left = 1
            Top = 325
            Width = 1044
            Height = 143
            Align = alBottom
            BevelOuter = bvNone
            TabOrder = 3
            object Panel101: TPanel
              Left = 129
              Top = 0
              Width = 915
              Height = 143
              Align = alClient
              BevelOuter = bvNone
              TabOrder = 0
              object dmkRadioGroup5: TDBRadioGroup
                Left = 0
                Top = 0
                Width = 915
                Height = 44
                Align = alTop
                Caption = '  Per'#237'odo de apura'#231#227'o do ICMS SPED EFD (Registro E100):'
                Columns = 6
                DataField = 'SPED_EFD_Peri_E100'
                DataSource = DsParamsEmp
                Items.Strings = (
                  'Indefinido'
                  'Di'#225'rio'
                  'Semanal'
                  'Decendial'
                  'Quinzenal'
                  'Mensal')
                TabOrder = 0
                Values.Strings = (
                  '0'
                  '1'
                  '2'
                  '3'
                  '4'
                  '5'
                  '6'
                  '7'
                  '8'
                  '9')
              end
              object dmkRadioGroup6: TDBRadioGroup
                Left = 0
                Top = 44
                Width = 915
                Height = 44
                Align = alTop
                Caption = '  Per'#237'odo de apura'#231#227'o do IPI SPED EFD (registro E500):'
                Columns = 6
                DataField = 'SPED_EFD_Peri_E500'
                DataSource = DsParamsEmp
                Items.Strings = (
                  'Indefinido'
                  'Di'#225'rio'
                  'Semanal'
                  'Decendial'
                  'Quinzenal'
                  'Mensal')
                TabOrder = 1
                Values.Strings = (
                  '0'
                  '1'
                  '2'
                  '3'
                  '4'
                  '5'
                  '6'
                  '7'
                  '8'
                  '9')
              end
              object dmkRadioGroup7: TDBRadioGroup
                Left = 0
                Top = 88
                Width = 915
                Height = 44
                Align = alTop
                Caption = '  Per'#237'odo de apura'#231#227'o da produ'#231#227'o SPED EFD (registro K100):'
                Columns = 6
                DataField = 'SPED_EFD_Peri_K100'
                DataSource = DsParamsEmp
                Items.Strings = (
                  'Indefinido'
                  'Di'#225'rio'
                  'Semanal'
                  'Decendial'
                  'Quinzenal'
                  'Mensal')
                TabOrder = 2
                Values.Strings = (
                  '0'
                  '1'
                  '2'
                  '3'
                  '4'
                  '5'
                  '6'
                  '7'
                  '8'
                  '9')
              end
            end
            object DBRadioGroup20: TDBRadioGroup
              Left = 0
              Top = 0
              Width = 129
              Height = 143
              Align = alLeft
              Caption = '  Produ'#231#227'o: '
              DataField = 'SPED_EFD_Producao'
              DataSource = DsParamsEmp
              Items.Strings = (
                'Singular'
                'Conjunta')
              TabOrder = 1
              Values.Strings = (
                '0'
                '1'
                '2'
                '3'
                '4'
                '5'
                '6'
                '7'
                '8'
                '9')
            end
          end
          object Panel111: TPanel
            Left = 1
            Top = 1
            Width = 1044
            Height = 60
            Align = alTop
            BevelOuter = bvNone
            TabOrder = 4
            object GroupBox25: TGroupBox
              Left = 160
              Top = 0
              Width = 884
              Height = 60
              Align = alClient
              Caption = ' PVA EFD:'
              TabOrder = 0
              object Label127: TLabel
                Left = 12
                Top = 16
                Width = 176
                Height = 13
                Caption = 'Caminho para salvar o arquivo digital:'
              end
              object DBEdit73: TDBEdit
                Left = 12
                Top = 32
                Width = 756
                Height = 21
                DataField = 'SPED_EFD_Path'
                DataSource = DsParamsEmp
                TabOrder = 0
              end
            end
            object GroupBox65: TGroupBox
              Left = 0
              Top = 0
              Width = 160
              Height = 60
              Align = alLeft
              Caption = 'Vers'#227'o:'
              TabOrder = 1
              object Label456: TLabel
                Left = 9
                Top = 16
                Width = 112
                Height = 13
                Caption = 'Vers'#227'o do Guia Pr'#225'tico:'
              end
              object DBEdit169: TDBEdit
                Left = 8
                Top = 32
                Width = 145
                Height = 21
                DataField = 'SPED_EFD_ICMS_IPI_VersaoGuia'
                DataSource = DsParamsEmp
                TabOrder = 0
              end
            end
          end
        end
      end
      object TabSheet28: TTabSheet
        Caption = 'NFS-e'
        ImageIndex = 6
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object PageControl6: TPageControl
          Left = 0
          Top = 0
          Width = 1046
          Height = 469
          ActivePage = TabSheet32
          Align = alClient
          TabOrder = 0
          object TabSheet32: TTabSheet
            Caption = ' Configura'#231#245'es Gerais '
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object Panel48: TPanel
              Left = 0
              Top = 0
              Width = 1038
              Height = 441
              Align = alClient
              ParentBackground = False
              TabOrder = 0
              object Label166: TLabel
                Left = 12
                Top = 60
                Width = 192
                Height = 13
                Caption = 'Web Service Municipal de PRODU'#199#195'O:'
              end
              object Label167: TLabel
                Left = 12
                Top = 100
                Width = 215
                Height = 13
                Caption = 'Web Service Municipal de HOMOLOGA'#199#195'O:'
              end
              object Label200: TLabel
                Left = 964
                Top = 20
                Width = 67
                Height = 13
                Caption = 'S'#233'rie do RPS:'
              end
              object Label160: TLabel
                Left = 716
                Top = 20
                Width = 36
                Height = 13
                Caption = 'Vers'#227'o:'
              end
              object Label161: TLabel
                Left = 760
                Top = 20
                Width = 64
                Height = 13
                Caption = 'Tipo de RPS:'
              end
              object Label162: TLabel
                Left = 12
                Top = 20
                Width = 165
                Height = 13
                Caption = 'M'#233'todo de integra'#231'ao de sistemas:'
              end
              object Label169: TLabel
                Left = 472
                Top = 20
                Width = 167
                Height = 13
                Caption = 'Tipo de email para envio de NFS-e:'
              end
              object Label165: TLabel
                Left = 12
                Top = 192
                Width = 93
                Height = 13
                Caption = #218'ltima DPS emitida:'
              end
              object Label201: TLabel
                Left = 116
                Top = 192
                Width = 81
                Height = 13
                Caption = #218'ltimo Lote RPS:'
              end
              object Label202: TLabel
                Left = 220
                Top = 192
                Width = 65
                Height = 13
                Caption = 'Usu'#225'rio Web:'
              end
              object Label203: TLabel
                Left = 352
                Top = 192
                Width = 60
                Height = 13
                Caption = 'Senha Web:'
              end
              object Label204: TLabel
                Left = 484
                Top = 192
                Width = 86
                Height = 13
                Caption = 'C'#243'digo Munic'#237'pio:'
              end
              object Bevel2: TBevel
                Left = 588
                Top = 208
                Width = 149
                Height = 21
              end
              object Label205: TLabel
                Left = 12
                Top = 232
                Width = 125
                Height = 13
                Caption = 'Cabe'#231'alho NFS-e Linha 1:'
              end
              object Label206: TLabel
                Left = 352
                Top = 232
                Width = 125
                Height = 13
                Caption = 'Cabe'#231'alho NFS-e Linha 2:'
              end
              object Label207: TLabel
                Left = 692
                Top = 232
                Width = 125
                Height = 13
                Caption = 'Cabe'#231'alho NFS-e Linha 3:'
              end
              object Label208: TLabel
                Left = 12
                Top = 272
                Width = 138
                Height = 13
                Caption = 'Diret'#243'rio dos arquivos NFS-e:'
              end
              object Label209: TLabel
                Left = 12
                Top = 312
                Width = 134
                Height = 13
                Caption = 'Diret'#243'rio dos Schemas XML:'
              end
              object Label210: TLabel
                Left = 520
                Top = 272
                Width = 65
                Height = 13
                Caption = 'Logo da Filial:'
              end
              object Label211: TLabel
                Left = 520
                Top = 312
                Width = 90
                Height = 13
                Caption = 'Logo da Prefeitura:'
              end
              object DBEdit81: TDBEdit
                Left = 12
                Top = 36
                Width = 56
                Height = 21
                DataField = 'NFSeMetodo'
                DataSource = DsParamsEmp
                TabOrder = 0
              end
              object DBEdit82: TDBEdit
                Left = 716
                Top = 36
                Width = 41
                Height = 21
                DataField = 'NFSeVersao'
                DataSource = DsParamsEmp
                TabOrder = 1
              end
              object DBEdit83: TDBEdit
                Left = 964
                Top = 36
                Width = 68
                Height = 21
                DataField = 'NFSeSerieRps'
                DataSource = DsParamsEmp
                TabOrder = 2
              end
              object DBEdit90: TDBEdit
                Left = 72
                Top = 36
                Width = 393
                Height = 21
                DataField = 'Nome'
                DataSource = DsMyMetodo
                TabOrder = 3
              end
              object DBEdit91: TDBEdit
                Left = 12
                Top = 208
                Width = 100
                Height = 21
                DataField = 'DpsNumero'
                DataSource = DsParamsEmp
                TabOrder = 4
              end
              object DBEdit92: TDBEdit
                Left = 12
                Top = 76
                Width = 868
                Height = 21
                DataField = 'NFSeWSProducao'
                DataSource = DsParamsEmp
                TabOrder = 5
              end
              object DBEdit93: TDBEdit
                Left = 12
                Top = 116
                Width = 868
                Height = 21
                DataField = 'NFSeWSHomologa'
                DataSource = DsParamsEmp
                TabOrder = 6
              end
              object DBRadioGroup16: TDBRadioGroup
                Left = 888
                Top = 76
                Width = 145
                Height = 61
                Caption = ' Identifica'#231#227'o do Ambiente: '
                DataField = 'NFSeAmbiente'
                DataSource = DsParamsEmp
                Items.Strings = (
                  '0 - Nenhum'
                  '1 - Produ'#231#227'o'
                  '2 - Homologa'#231#227'o')
                TabOrder = 7
                Values.Strings = (
                  '0'
                  '1'
                  '2')
              end
              object DBEdit94: TDBEdit
                Left = 472
                Top = 36
                Width = 28
                Height = 21
                DataField = 'NFSeTipCtoMail'
                DataSource = DsParamsEmp
                TabOrder = 8
              end
              object DBEdit95: TDBEdit
                Left = 500
                Top = 36
                Width = 209
                Height = 21
                DataField = 'NO_NFSeTipCtoMail'
                DataSource = DsParamsEmp
                TabOrder = 9
              end
              object GroupBox34: TGroupBox
                Left = 12
                Top = 140
                Width = 1025
                Height = 49
                Caption = ' Certificado Digital: '
                TabOrder = 10
                object Label184: TLabel
                  Left = 11
                  Top = 24
                  Width = 82
                  Height = 13
                  Caption = 'N'#250'mero de S'#233'rie:'
                end
                object SpeedButton23: TSpeedButton
                  Left = 372
                  Top = 20
                  Width = 21
                  Height = 21
                  Caption = '...'
                  OnClick = SbNFSeCertDigitalClick
                end
                object Label185: TLabel
                  Left = 396
                  Top = 24
                  Width = 84
                  Height = 13
                  Caption = 'Data da validade:'
                end
                object Label186: TLabel
                  Left = 600
                  Top = 24
                  Width = 268
                  Height = 13
                  Caption = 'Avisar a expira'#231#227'o da validade do certificado digital com '
                end
                object Label187: TLabel
                  Left = 912
                  Top = 24
                  Width = 105
                  Height = 13
                  Caption = 'dias de anteced'#234'ncia.'
                end
                object DBEdit98: TDBEdit
                  Left = 96
                  Top = 20
                  Width = 273
                  Height = 21
                  DataField = 'NFSeCertDigital'
                  DataSource = DsParamsEmp
                  TabOrder = 0
                end
                object DBEdit99: TDBEdit
                  Left = 484
                  Top = 20
                  Width = 112
                  Height = 21
                  DataField = 'NFSeCertValidad'
                  DataSource = DsParamsEmp
                  TabOrder = 1
                end
                object DBEdit100: TDBEdit
                  Left = 868
                  Top = 20
                  Width = 40
                  Height = 21
                  DataField = 'NFSeCertAviExpi'
                  DataSource = DsParamsEmp
                  TabOrder = 2
                end
              end
              object EdTXT_NFSeTipoRps2: TdmkEdit
                Left = 776
                Top = 36
                Width = 185
                Height = 21
                ReadOnly = True
                TabOrder = 11
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object DBEdit101: TDBEdit
                Left = 760
                Top = 36
                Width = 16
                Height = 21
                DataField = 'NFSeTipoRps'
                DataSource = DsParamsEmp
                TabOrder = 12
                OnChange = DBEdit101Change
              end
              object DBEdit102: TDBEdit
                Left = 116
                Top = 208
                Width = 101
                Height = 21
                DataField = 'RPSNumLote'
                DataSource = DsParamsEmp
                TabOrder = 13
              end
              object DBEdit103: TDBEdit
                Left = 12
                Top = 248
                Width = 337
                Height = 21
                DataField = 'NFSePrefeitura1'
                DataSource = DsParamsEmp
                TabOrder = 14
              end
              object DBEdit104: TDBEdit
                Left = 352
                Top = 248
                Width = 337
                Height = 21
                DataField = 'NFSePrefeitura2'
                DataSource = DsParamsEmp
                TabOrder = 15
              end
              object DBEdit105: TDBEdit
                Left = 692
                Top = 248
                Width = 337
                Height = 21
                DataField = 'NFSePrefeitura3'
                DataSource = DsParamsEmp
                TabOrder = 16
              end
              object DBCheckBox29: TDBCheckBox
                Left = 592
                Top = 210
                Width = 129
                Height = 17
                Caption = 'Visualizar mensagens.'
                DataField = 'NFSeMsgVisu'
                DataSource = DsParamsEmp
                TabOrder = 17
                ValueChecked = '1'
                ValueUnchecked = '0'
              end
              object DBEdit106: TDBEdit
                Left = 220
                Top = 208
                Width = 129
                Height = 21
                DataField = 'NFSeUserWeb'
                DataSource = DsParamsEmp
                TabOrder = 18
              end
              object DBEdit107: TDBEdit
                Left = 352
                Top = 208
                Width = 129
                Height = 21
                DataField = 'NFSeSenhaWeb'
                DataSource = DsParamsEmp
                TabOrder = 19
              end
              object DBEdit108: TDBEdit
                Left = 484
                Top = 208
                Width = 97
                Height = 21
                DataField = 'NFSeCodMunici'
                DataSource = DsParamsEmp
                TabOrder = 20
              end
              object DBEdit109: TDBEdit
                Left = 520
                Top = 328
                Width = 509
                Height = 21
                DataField = 'NFSeLogoPref'
                DataSource = DsParamsEmp
                TabOrder = 21
              end
              object DBEdit110: TDBEdit
                Left = 12
                Top = 328
                Width = 504
                Height = 21
                DataField = 'DirNFSeSchema'
                DataSource = DsParamsEmp
                TabOrder = 22
              end
              object DBEdit111: TDBEdit
                Left = 520
                Top = 288
                Width = 509
                Height = 21
                DataField = 'NFSeLogoFili'
                DataSource = DsParamsEmp
                TabOrder = 23
              end
              object DBEdit112: TDBEdit
                Left = 12
                Top = 288
                Width = 505
                Height = 21
                DataField = 'DirNFSeLogs'
                DataSource = DsParamsEmp
                TabOrder = 24
              end
            end
          end
          object TabSheet33: TTabSheet
            Caption = ' Diret'#243'rios dos arquivos XML '
            ImageIndex = 1
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object Panel50: TPanel
              Left = 0
              Top = 0
              Width = 1038
              Height = 441
              Align = alClient
              BevelOuter = bvNone
              ParentBackground = False
              TabOrder = 0
              object Label181: TLabel
                Left = 8
                Top = 44
                Width = 226
                Height = 13
                Caption = 'Diret'#243'rio das RPS assinadas (-ger-nfse-env.xml):'
              end
              object Label182: TLabel
                Left = 8
                Top = 4
                Width = 217
                Height = 13
                Caption = 'Diret'#243'rio das RPS geradas (-ger-nfse-env.xml):'
              end
              object DBEdit96: TDBEdit
                Left = 8
                Top = 20
                Width = 752
                Height = 21
                DataField = 'DirNFSeDPSGer'
                DataSource = DsParamsEmp
                TabOrder = 0
              end
              object DBEdit97: TDBEdit
                Left = 8
                Top = 60
                Width = 752
                Height = 21
                DataField = 'DirNFSeDPSAss'
                DataSource = DsParamsEmp
                TabOrder = 1
              end
            end
          end
          object TabSheet35: TTabSheet
            Caption = 'Op'#231#245'es'
            ImageIndex = 2
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object Panel53: TPanel
              Left = 0
              Top = 0
              Width = 1038
              Height = 441
              Align = alClient
              ParentBackground = False
              TabOrder = 0
              object Label214: TLabel
                Left = 8
                Top = 4
                Width = 192
                Height = 13
                Caption = 'Pr'#233'-email para envio de NF-e autorizada:'
              end
              object Label215: TLabel
                Left = 8
                Top = 44
                Width = 193
                Height = 13
                Caption = 'Pr'#233'-email para envio de NF-e cancelada:'
              end
              object Label217: TLabel
                Left = 8
                Top = 84
                Width = 461
                Height = 13
                Caption = 
                  'Email pr'#243'prio para receber email enviado (mais de um separar por' +
                  ' ponto e v'#237'rgula) m'#225'x. 255 letras:'
              end
              object DBEdit113: TDBEdit
                Left = 8
                Top = 20
                Width = 56
                Height = 21
                DataField = 'NFSePreMailAut'
                DataSource = DsParamsEmp
                TabOrder = 0
              end
              object DBEdit114: TDBEdit
                Left = 64
                Top = 20
                Width = 693
                Height = 21
                DataField = 'NO_NFSePreMailAut'
                DataSource = DsParamsEmp
                TabOrder = 1
              end
              object DBEdit115: TDBEdit
                Left = 8
                Top = 60
                Width = 56
                Height = 21
                DataField = 'NFSePreMailCan'
                DataSource = DsParamsEmp
                TabOrder = 2
              end
              object DBEdit116: TDBEdit
                Left = 64
                Top = 60
                Width = 693
                Height = 21
                DataField = 'NO_NFSePreMailCan'
                DataSource = DsParamsEmp
                TabOrder = 3
              end
              object DBEdit119: TDBEdit
                Left = 8
                Top = 100
                Width = 749
                Height = 21
                DataField = 'MyEmailNFSe'
                DataSource = DsParamsEmp
                TabOrder = 4
              end
              object DBRadioGroup18: TDBRadioGroup
                Left = 14
                Top = 317
                Width = 505
                Height = 39
                Caption = 
                  ' Informa'#231#227'o dos valores totais aproximados de impostos no campo ' +
                  'da descri'#231#227'o do servi'#231'o nas NFS-es: '
                Columns = 3
                DataField = 'NFSe_indFinalCpl'
                DataSource = DsParamsEmp
                Items.Strings = (
                  'Indefinido'
                  'Somente consumidor final'
                  'Todas NFS-es')
                TabOrder = 5
                Values.Strings = (
                  '0'
                  '1'
                  '2'
                  '3'
                  '4'
                  '5'
                  '6'
                  '7'
                  '8'
                  '9')
              end
            end
          end
        end
      end
      object TabSheet7: TTabSheet
        Caption = ' S'#233'ries de NF-es'
        ImageIndex = 3
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object DBGrid1: TDBGrid
          Left = 0
          Top = 0
          Width = 1046
          Height = 469
          Align = alClient
          DataSource = DsParamsNFs
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -12
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'Controle'
              Title.Caption = 'ID'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'SerieNF'
              Title.Caption = 'S'#233'rie da Nota Fiscal'
              Width = 109
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Sequencial'
              Title.Caption = 'N'#186' da '#250'ltima nota emitida '
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_IncSeqAuto'
              Title.Caption = 'Incremento'
              Width = 107
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'MaxSeqLib'
              Title.Caption = 'N'#186' m'#225'ximo liberado p/ emiss'#227'o '
              Visible = True
            end>
        end
      end
      object TabSheet46: TTabSheet
        Caption = ' S'#233'ries de CT-es '
        ImageIndex = 8
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object DBGrid2: TDBGrid
          Left = 0
          Top = 0
          Width = 1046
          Height = 469
          Align = alClient
          DataSource = DsParamsCTe
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -12
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'Controle'
              Title.Caption = 'ID'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'SerieCTe'
              Title.Caption = 'S'#233'rie do CT-e'
              Width = 109
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Sequencial'
              Title.Caption = 'N'#186' do '#250'ltimo CT-e emitido'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_IncSeqAuto'
              Title.Caption = 'Incremento'
              Width = 107
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'MaxSeqLib'
              Title.Caption = 'N'#186' m'#225'ximo liberado p/ emiss'#227'o '
              Visible = True
            end>
        end
      end
      object TabSheet56: TTabSheet
        Caption = ' S'#233'ries de MDF-es'
        ImageIndex = 9
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object DBGrid3: TDBGrid
          Left = 0
          Top = 0
          Width = 1046
          Height = 469
          Align = alClient
          DataSource = DsParamsMDFe
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -12
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'Controle'
              Title.Caption = 'ID'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'SerieMDFe'
              Title.Caption = 'S'#233'rie do MDF-e'
              Width = 109
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Sequencial'
              Title.Caption = 'N'#186' do '#250'ltimo MDF-e emitido'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_IncSeqAuto'
              Title.Caption = 'Incremento'
              Width = 107
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'MaxSeqLib'
              Title.Caption = 'N'#186' m'#225'ximo liberado p/ emiss'#227'o '
              Visible = True
            end>
        end
      end
    end
    object PnDataCab: TPanel
      Left = 0
      Top = 0
      Width = 1054
      Height = 48
      Align = alTop
      BevelOuter = bvNone
      Enabled = False
      ParentBackground = False
      TabOrder = 0
      object Label1: TLabel
        Left = 8
        Top = 4
        Width = 14
        Height = 13
        Caption = 'ID:'
      end
      object Label2: TLabel
        Left = 68
        Top = 4
        Width = 23
        Height = 13
        Caption = 'Filial:'
        FocusControl = DBEdit1
      end
      object Label3: TLabel
        Left = 128
        Top = 4
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
      end
      object DBEdit1: TDBEdit
        Left = 128
        Top = 20
        Width = 644
        Height = 21
        DataField = 'NOMEFILIAL'
        DataSource = DsParamsEmp
        TabOrder = 0
      end
      object DBEdit5: TDBEdit
        Left = 68
        Top = 20
        Width = 56
        Height = 21
        DataField = 'Filial'
        DataSource = DsParamsEmp
        TabOrder = 1
      end
      object DBEdit6: TDBEdit
        Left = 8
        Top = 20
        Width = 56
        Height = 21
        DataField = 'Codigo'
        DataSource = DsParamsEmp
        TabOrder = 2
      end
    end
    object GBCntrl: TGroupBox
      Left = 0
      Top = 690
      Width = 1054
      Height = 64
      Align = alBottom
      TabOrder = 2
      object Panel5: TPanel
        Left = 2
        Top = 15
        Width = 172
        Height = 47
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 174
        Top = 15
        Width = 218
        Height = 47
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel3: TPanel
        Left = 392
        Top = 15
        Width = 660
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object Panel2: TPanel
          Left = 551
          Top = 0
          Width = 109
          Height = 47
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 0
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 90
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
        object BtFilial: TBitBtn
          Tag = 408
          Left = 4
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Filial'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtFilialClick
        end
        object BtSerieNF: TBitBtn
          Tag = 441
          Left = 96
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = 'S'#233'ries &NF'
          Enabled = False
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = BtSerieNFClick
        end
        object BtCertDigital: TBitBtn
          Tag = 441
          Left = 372
          Top = 3
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = 'Cert. &Digital'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = BtCertDigitalClick
        end
        object BtSerieCTe: TBitBtn
          Tag = 441
          Left = 188
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = 'S'#233'ries &CT-e'
          Enabled = False
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 4
          OnClick = BtSerieCTeClick
        end
        object BtSerieMDFe: TBitBtn
          Tag = 441
          Left = 280
          Top = 3
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = 'S'#233'ries &MDF-e'
          Enabled = False
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 5
          OnClick = BtSerieMDFeClick
        end
      end
    end
  end
  object PainelEdita: TPanel
    Left = 0
    Top = 96
    Width = 1054
    Height = 754
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    Visible = False
    object GBConfirma: TGroupBox
      Left = 0
      Top = 691
      Width = 1054
      Height = 63
      Align = alBottom
      TabOrder = 2
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 12
        Top = 17
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object Panel1: TPanel
        Left = 920
        Top = 15
        Width = 132
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = -1
          Top = 2
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
      object BtOpcoes: TBitBtn
        Tag = 14
        Left = 135
        Top = 17
        Width = 150
        Height = 40
        Cursor = crHandPoint
        Caption = '&Op'#231#245'es Internet'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 2
        OnClick = BtOpcoesClick
      end
    end
    object PnEditCab: TPanel
      Left = 0
      Top = 0
      Width = 1054
      Height = 48
      Align = alTop
      BevelOuter = bvNone
      ParentBackground = False
      TabOrder = 0
      object Label7: TLabel
        Left = 8
        Top = 4
        Width = 14
        Height = 13
        Caption = 'ID:'
      end
      object Label8: TLabel
        Left = 68
        Top = 4
        Width = 23
        Height = 13
        Caption = 'Filial:'
      end
      object Label9: TLabel
        Left = 128
        Top = 4
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
      end
      object DBEdit2: TDBEdit
        Left = 128
        Top = 20
        Width = 644
        Height = 21
        DataField = 'NOMEFILIAL'
        DataSource = DsParamsEmp
        TabOrder = 2
      end
      object DBEdit3: TDBEdit
        Left = 68
        Top = 20
        Width = 56
        Height = 21
        DataField = 'Filial'
        DataSource = DsParamsEmp
        TabOrder = 1
      end
      object DBEdCodigo: TdmkDBEdit
        Left = 8
        Top = 20
        Width = 56
        Height = 21
        DataField = 'Codigo'
        DataSource = DsParamsEmp
        TabOrder = 0
        UpdCampo = 'Codigo'
        UpdType = utInc
        Alignment = taLeftJustify
      end
    end
    object PageControl2: TPageControl
      Left = 0
      Top = 48
      Width = 1054
      Height = 493
      ActivePage = TabSheet48
      Align = alTop
      TabOrder = 1
      object TabSheet3: TTabSheet
        Caption = 'Miscel'#226'nea '
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object Panel7: TPanel
          Left = 0
          Top = 0
          Width = 1046
          Height = 465
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          object Label5: TLabel
            Left = 8
            Top = 4
            Width = 36
            Height = 13
            Caption = 'Moeda:'
          end
          object SBMoeda: TSpeedButton
            Left = 190
            Top = 20
            Width = 21
            Height = 21
            OnClick = SBMoedaClick
          end
          object Label17: TLabel
            Left = 8
            Top = 44
            Width = 337
            Height = 13
            Caption = 
              'Caminho para logo da empresa  na propor'#231#227'o de 3 x 1 (largura x a' +
              'ltura):'
          end
          object SpeedButton5: TSpeedButton
            Left = 746
            Top = 60
            Width = 21
            Height = 21
            Caption = '...'
            OnClick = SpeedButton5Click
          end
          object LaCartEmisHonFun: TLabel
            Left = 12
            Top = 220
            Width = 253
            Height = 13
            Caption = 'Carteira gen'#233'rica (geral) de pagamento de honor'#225'rios:'
          end
          object SBCartEmisHonFun: TSpeedButton
            Left = 511
            Top = 238
            Width = 21
            Height = 21
            Caption = '...'
            OnClick = SBCartEmisHonFunClick
          end
          object DBEdit7: TDBEdit
            Left = 8
            Top = 20
            Width = 40
            Height = 21
            DataField = 'Moeda'
            TabOrder = 3
          end
          object EdMoeda: TdmkEditCB
            Left = 8
            Top = 20
            Width = 40
            Height = 21
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            DBLookupComboBox = CBMoeda
            IgnoraDBLookupComboBox = False
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
          object CBMoeda: TdmkDBLookupComboBox
            Left = 48
            Top = 20
            Width = 141
            Height = 21
            KeyField = 'CodUsu'
            ListField = 'Nome'
            TabOrder = 1
            dmkEditCB = EdMoeda
            UpdType = utNil
            LocF7SQLMasc = '$#'
          end
          object EdLogo3x1: TdmkEdit
            Left = 8
            Top = 60
            Width = 737
            Height = 21
            TabOrder = 2
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            QryCampo = 'Logo3x1'
            UpdCampo = 'Logo3x1'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object GroupBox43: TGroupBox
            Left = 8
            Top = 84
            Width = 757
            Height = 77
            Caption = ' TZD UTC e altera'#231#245'es de hor'#225'rio de ver'#227'o: '
            TabOrder = 4
            object Panel64: TPanel
              Left = 2
              Top = 15
              Width = 753
              Height = 60
              Align = alClient
              BevelOuter = bvNone
              TabOrder = 0
              object Label259: TLabel
                Left = 608
                Top = 1
                Width = 41
                Height = 13
                Caption = 'T'#233'rmino:'
              end
              object Label260: TLabel
                Left = 492
                Top = 1
                Width = 30
                Height = 13
                Caption = 'In'#237'cio:'
              end
              object Label261: TLabel
                Left = 340
                Top = 1
                Width = 144
                Height = 13
                Caption = 'Perguntar de novo  a partir de:'
              end
              object Label262: TLabel
                Left = 6
                Top = 1
                Width = 50
                Height = 13
                Caption = 'TZD UTC:'
              end
              object Label263: TLabel
                Left = 252
                Top = 20
                Width = 82
                Height = 13
                Caption = 'Hor'#225'rio de ver'#227'o:'
              end
              object SBTZD_UTC: TSpeedButton
                Left = 86
                Top = 17
                Width = 21
                Height = 21
                Caption = '?'
                OnClick = SBTZD_UTCClick
              end
              object Label277: TLabel
                Left = 340
                Top = 41
                Width = 204
                Height = 13
                Caption = 'Obter informa'#231#245'es sobre o hor'#225'rio de ver'#227'o'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlue
                Font.Height = -12
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsUnderline]
                ParentFont = False
                OnClick = Label277Click
              end
              object EdTZD_UTC: TdmkEdit
                Left = 6
                Top = 17
                Width = 75
                Height = 21
                TabOrder = 0
                FormatType = dmktfTime
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfTZD_UTC
                Texto = '+00:00'
                QryCampo = 'TZD_UTC'
                UpdCampo = 'TZD_UTC'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0.000000000000000000
                ValWarn = False
              end
              object TPhVeraoAsk: TdmkEditDateTimePicker
                Left = 340
                Top = 17
                Width = 148
                Height = 21
                Date = 41927.390190972220000000
                Time = 41927.390190972220000000
                TabOrder = 1
                ReadOnly = False
                DefaultEditMask = '!99/99/99;1;_'
                AutoApplyEditMask = True
                QryCampo = 'hVeraoAsk'
                UpdCampo = 'hVeraoAsk'
                UpdType = utYes
                DatePurpose = dmkdpNone
              end
              object TPhVeraoIni: TdmkEditDateTimePicker
                Left = 492
                Top = 17
                Width = 112
                Height = 21
                Date = 41927.390190972220000000
                Time = 41927.390190972220000000
                TabOrder = 2
                ReadOnly = False
                DefaultEditMask = '!99/99/99;1;_'
                AutoApplyEditMask = True
                QryCampo = 'hVeraoIni'
                UpdCampo = 'hVeraoIni'
                UpdType = utYes
                DatePurpose = dmkdpNone
              end
              object TPhVeraoFim: TdmkEditDateTimePicker
                Left = 608
                Top = 17
                Width = 112
                Height = 21
                Date = 41927.390190972220000000
                Time = 41927.390190972220000000
                TabOrder = 3
                ReadOnly = False
                DefaultEditMask = '!99/99/99;1;_'
                AutoApplyEditMask = True
                QryCampo = 'hVeraoFim'
                UpdCampo = 'hVeraoFim'
                UpdType = utYes
                DatePurpose = dmkdpNone
              end
              object CkTZD_UTC_Auto: TdmkCheckBox
                Left = 6
                Top = 40
                Width = 170
                Height = 17
                Caption = 'Obter fuso hor'#225'rio do servidor'
                TabOrder = 4
                OnClick = CkTZD_UTC_AutoClick
                QryCampo = 'TZD_UTC_Auto'
                UpdCampo = 'TZD_UTC_Auto'
                UpdType = utYes
                ValCheck = '1'
                ValUncheck = '0'
                OldValor = #0
              end
            end
          end
          object RGNFeNT2013_003LTT: TdmkRadioGroup
            Left = 9
            Top = 167
            Width = 756
            Height = 46
            Caption = ' Lei da Transpar'#234'ncia dos Tributos:'
            Columns = 4
            ItemIndex = 3
            Items.Strings = (
              'Desabilitar'
              'Informar zerado'
              'Informar valor aproximado'
              'Informar e imprimir valor aproximado')
            TabOrder = 5
            QryCampo = 'NFeNT2013_003LTT'
            UpdCampo = 'NFeNT2013_003LTT'
            UpdType = utYes
            OldValor = 0
          end
          object EdCartEmisHonFun: TdmkEditCB
            Left = 12
            Top = 238
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 6
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'CartEmisHonFun'
            UpdCampo = 'CartEmisHonFun'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            DBLookupComboBox = CBCartEmisHonFun
            IgnoraDBLookupComboBox = False
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
          object CBCartEmisHonFun: TdmkDBLookupComboBox
            Left = 68
            Top = 238
            Width = 440
            Height = 21
            KeyField = 'Codigo'
            ListField = 'Nome'
            ListSource = DsCartEmisHonFun
            TabOrder = 7
            dmkEditCB = EdCartEmisHonFun
            QryCampo = 'CartEmisHonFun'
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
        end
      end
      object TabSheet4: TTabSheet
        Caption = ' Pedidos '
        ImageIndex = 1
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object Panel8: TPanel
          Left = 0
          Top = 0
          Width = 1046
          Height = 465
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          object Panel12: TPanel
            Left = 0
            Top = 0
            Width = 1046
            Height = 125
            Align = alTop
            BevelOuter = bvNone
            TabOrder = 0
            object Label23: TLabel
              Left = 8
              Top = 4
              Width = 45
              Height = 13
              Caption = 'Situa'#231#227'o:'
            end
            object EdSituacao: TdmkEditCB
              Left = 8
              Top = 20
              Width = 24
              Height = 21
              Alignment = taRightJustify
              TabOrder = 0
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              QryCampo = 'Situacao'
              UpdCampo = 'Situacao'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              DBLookupComboBox = CBSituacao
              IgnoraDBLookupComboBox = False
              AutoSetIfOnlyOneReg = setregOnlyManual
            end
            object CBSituacao: TdmkDBLookupComboBox
              Left = 32
              Top = 20
              Width = 140
              Height = 21
              KeyField = 'Codigo'
              ListField = 'Nome'
              TabOrder = 1
              dmkEditCB = EdSituacao
              QryCampo = 'Situacao'
              UpdType = utNil
              LocF7SQLMasc = '$#'
            end
            object RGFatSemEstq: TdmkRadioGroup
              Left = 8
              Top = 44
              Width = 449
              Height = 38
              Caption = ' Permite faturamento de item sem estoque suficiente: '
              Columns = 3
              ItemIndex = 2
              Items.Strings = (
                'N'#227'o permite'
                'Permite sem avisar'
                'Perguntar antes')
              TabOrder = 4
              QryCampo = 'FatSemEstq'
              UpdCampo = 'FatSemEstq'
              UpdType = utYes
              OldValor = 0
            end
            object RGTipMediaDD: TdmkRadioGroup
              Left = 180
              Top = 4
              Width = 277
              Height = 38
              Caption = ' C'#225'lculo de prazo m'#233'dio: '
              Columns = 3
              ItemIndex = 0
              Items.Strings = (
                'Nenhum'
                'M'#233'dia simples'
                'M'#233'dia real')
              TabOrder = 2
              QryCampo = 'TipMediaDD'
              UpdCampo = 'TipMediaDD'
              UpdType = utYes
              OldValor = 0
            end
            object RGFatSemPrcL: TdmkRadioGroup
              Left = 460
              Top = 44
              Width = 304
              Height = 38
              Caption = ' Permite inclus'#227'o de itens sem pre'#231'o de lista em pedido: '
              Columns = 2
              ItemIndex = 0
              Items.Strings = (
                'N'#227'o permite'
                'Sim, permite')
              TabOrder = 5
              QryCampo = 'FatSemPrcL'
              UpdCampo = 'FatSemPrcL'
              UpdType = utYes
              OldValor = 0
            end
            object RGTipCalcJuro: TdmkRadioGroup
              Left = 460
              Top = 4
              Width = 304
              Height = 38
              Caption = ' C'#225'lculo de juros (lista de pre'#231'os de grupos de produtos): '
              Columns = 3
              ItemIndex = 0
              Items.Strings = (
                'Nenhum'
                'Juro simples'
                'Juro composto')
              TabOrder = 3
              QryCampo = 'TipCalcJuro'
              UpdCampo = 'TipCalcJuro'
              UpdType = utYes
              OldValor = 0
            end
            object RGPedVdaMudLista: TdmkRadioGroup
              Left = 8
              Top = 84
              Width = 373
              Height = 38
              Caption = ' Permite alterar lista de pre'#231'os ap'#243's inclus'#227'o de itens: '
              Columns = 3
              ItemIndex = 2
              Items.Strings = (
                'N'#227'o permite'
                'Permite ao gerente'
                'Permite a todos')
              TabOrder = 6
              QryCampo = 'PedVdaMudLista'
              UpdCampo = 'PedVdaMudLista'
              UpdType = utYes
              OldValor = 0
            end
            object RGPedVdaMudPrazo: TdmkRadioGroup
              Left = 391
              Top = 84
              Width = 373
              Height = 38
              Caption = ' Permite alterar condi'#231#227'o de pagamento ap'#243's inclus'#227'o de itens: '
              Columns = 3
              ItemIndex = 2
              Items.Strings = (
                'N'#227'o permite'
                'Permite ao gerente'
                'Permite a todos')
              TabOrder = 7
              QryCampo = 'PedVdaMudPrazo'
              UpdCampo = 'PedVdaMudPrazo'
              UpdType = utYes
              OldValor = 0
            end
            object CkPediVdaNElertas: TdmkCheckBox
              Left = 770
              Top = 20
              Width = 150
              Height = 17
              Caption = 'N'#227'o efetuar verifica'#231#245'es'
              TabOrder = 8
              QryCampo = 'PediVdaNElertas'
              UpdCampo = 'PediVdaNElertas'
              UpdType = utYes
              ValCheck = '1'
              ValUncheck = '0'
              OldValor = #0
            end
          end
          object GroupBox2: TGroupBox
            Left = 0
            Top = 125
            Width = 1046
            Height = 100
            Align = alTop
            Caption = ' Associada: '
            TabOrder = 1
            Visible = False
            object Label12: TLabel
              Left = 8
              Top = 16
              Width = 44
              Height = 13
              Caption = 'Empresa:'
            end
            object SBModeloNF: TSpeedButton
              Left = 746
              Top = 72
              Width = 21
              Height = 21
              Caption = '...'
              OnClick = SBModeloNFClick
            end
            object Label18: TLabel
              Left = 8
              Top = 57
              Width = 70
              Height = 13
              Caption = 'Modelo de NF:'
            end
            object CBAssociada: TdmkDBLookupComboBox
              Left = 68
              Top = 32
              Width = 697
              Height = 21
              KeyField = 'Filial'
              ListField = 'NOMEFILIAL'
              ListSource = DsFiliais
              TabOrder = 1
              dmkEditCB = EdAssociada
              UpdType = utNil
              LocF7SQLMasc = '$#'
            end
            object EdAssociada: TdmkEditCB
              Left = 8
              Top = 32
              Width = 56
              Height = 21
              Alignment = taRightJustify
              TabOrder = 0
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              OnChange = EdAssociadaChange
              OnEnter = EdAssociadaEnter
              OnExit = EdAssociadaExit
              DBLookupComboBox = CBAssociada
              IgnoraDBLookupComboBox = False
              AutoSetIfOnlyOneReg = setregOnlyManual
            end
            object EdAssocModNF: TdmkEditCB
              Left = 8
              Top = 72
              Width = 52
              Height = 21
              Alignment = taRightJustify
              TabOrder = 2
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              QryCampo = 'AssocModNF'
              UpdCampo = 'AssocModNF'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              OnChange = EdAssocModNFChange
              OnEnter = EdAssocModNFEnter
              DBLookupComboBox = CBAssocModNF
              IgnoraDBLookupComboBox = False
              AutoSetIfOnlyOneReg = setregOnlyManual
            end
            object CBAssocModNF: TdmkDBLookupComboBox
              Left = 64
              Top = 72
              Width = 681
              Height = 21
              KeyField = 'Codigo'
              ListField = 'Nome'
              ListSource = DsImprime
              TabOrder = 3
              OnEnter = CBAssocModNFEnter
              dmkEditCB = EdAssocModNF
              QryCampo = 'AssocModNF'
              UpdType = utNil
              LocF7SQLMasc = '$#'
            end
          end
        end
      end
      object TabSheet47: TTabSheet
        Caption = 'Faturamento'
        ImageIndex = 2
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object Panel79: TPanel
          Left = 0
          Top = 0
          Width = 1046
          Height = 465
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          object GroupBox3: TGroupBox
            Left = 0
            Top = 0
            Width = 1046
            Height = 224
            Align = alTop
            Caption = ' Parcelamento da fatura: '
            TabOrder = 0
            object GroupBox5: TGroupBox
              Left = 2
              Top = 15
              Width = 105
              Height = 207
              Align = alLeft
              Caption = ' Parcela da fatura: '
              TabOrder = 0
              object RGFaturaSeq: TdmkRadioGroup
                Left = 2
                Top = 44
                Width = 101
                Height = 161
                Align = alClient
                Caption = ' Sequencial: '
                Items.Strings = (
                  'N'#250'meros'
                  'Letras')
                TabOrder = 1
                QryCampo = 'FaturaSeq'
                UpdCampo = 'FaturaSeq'
                UpdType = utYes
                OldValor = 0
              end
              object Panel17: TPanel
                Left = 2
                Top = 15
                Width = 101
                Height = 29
                Align = alTop
                BevelOuter = bvNone
                TabOrder = 0
                object Label15: TLabel
                  Left = 4
                  Top = 8
                  Width = 52
                  Height = 13
                  Caption = 'Separador:'
                end
                object EdFaturaSep: TdmkEdit
                  Left = 60
                  Top = 4
                  Width = 25
                  Height = 21
                  MaxLength = 1
                  TabOrder = 0
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '/'
                  QryCampo = 'FaturaSep'
                  UpdCampo = 'FaturaSep'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = '/'
                  ValWarn = False
                end
              end
            end
            object Panel15: TPanel
              Left = 107
              Top = 15
              Width = 937
              Height = 207
              Align = alClient
              BevelOuter = bvNone
              TabOrder = 1
              object SbProdVen: TSpeedButton
                Left = 418
                Top = 20
                Width = 21
                Height = 21
                Caption = '...'
                OnClick = SbProdVenClick
              end
              object Label14: TLabel
                Left = 4
                Top = 4
                Width = 147
                Height = 13
                Caption = 'Conta para venda de produtos:'
              end
              object Label20: TLabel
                Left = 440
                Top = 4
                Width = 176
                Height = 13
                Caption = 'Texto para descri'#231#227'o no lan'#231'amento:'
              end
              object Label65: TLabel
                Left = 4
                Top = 84
                Width = 157
                Height = 13
                Caption = 'Conta para presta'#231#227'o de servi'#231'o:'
              end
              object SpeedButton9: TSpeedButton
                Left = 418
                Top = 100
                Width = 21
                Height = 21
                Caption = '...'
                OnClick = SpeedButton9Click
              end
              object Label66: TLabel
                Left = 440
                Top = 84
                Width = 176
                Height = 13
                Caption = 'Texto para descri'#231#227'o no lan'#231'amento:'
              end
              object Label69: TLabel
                Left = 624
                Top = 4
                Width = 47
                Height = 13
                Caption = 'ID Duplic.'
              end
              object Label70: TLabel
                Left = 624
                Top = 84
                Width = 47
                Height = 13
                Caption = 'ID Duplic.'
              end
              object Label278: TLabel
                Left = 4
                Top = 44
                Width = 152
                Height = 13
                Caption = 'Conta para compra de produtos:'
              end
              object SBCtaProdCom: TSpeedButton
                Left = 418
                Top = 60
                Width = 21
                Height = 21
                Caption = '...'
                OnClick = SBCtaProdComClick
              end
              object Label279: TLabel
                Left = 440
                Top = 44
                Width = 176
                Height = 13
                Caption = 'Texto para descri'#231#227'o no lan'#231'amento:'
              end
              object Label280: TLabel
                Left = 624
                Top = 44
                Width = 47
                Height = 13
                Caption = 'ID Duplic.'
              end
              object Label281: TLabel
                Left = 4
                Top = 124
                Width = 228
                Height = 13
                Caption = 'Conta para pagamento de presta'#231#227'o de servi'#231'o:'
              end
              object SBCtaServicoPg: TSpeedButton
                Left = 418
                Top = 140
                Width = 21
                Height = 21
                Caption = '...'
                OnClick = SBCtaServicoPgClick
              end
              object Label282: TLabel
                Left = 440
                Top = 124
                Width = 176
                Height = 13
                Caption = 'Texto para descri'#231#227'o no lan'#231'amento:'
              end
              object Label283: TLabel
                Left = 624
                Top = 124
                Width = 47
                Height = 13
                Caption = 'ID Duplic.'
              end
              object Label298: TLabel
                Left = 4
                Top = 162
                Width = 123
                Height = 13
                Caption = 'Conta para frete prestado:'
              end
              object SpeedButton28: TSpeedButton
                Left = 418
                Top = 178
                Width = 21
                Height = 21
                Caption = '...'
                OnClick = SpeedButton28Click
              end
              object Label344: TLabel
                Left = 440
                Top = 162
                Width = 176
                Height = 13
                Caption = 'Texto para descri'#231#227'o no lan'#231'amento:'
              end
              object Label345: TLabel
                Left = 624
                Top = 162
                Width = 47
                Height = 13
                Caption = 'ID Duplic.'
              end
              object RGFaturaDta: TdmkRadioGroup
                Left = 680
                Top = 85
                Width = 120
                Height = 75
                Caption = ' Data do faturamento: '
                Items.Strings = (
                  'Perguntar'
                  'Abertura'
                  'Encerramento')
                TabOrder = 17
                QryCampo = 'FaturaDta'
                UpdCampo = 'FaturaDta'
                UpdType = utYes
                OldValor = 0
              end
              object CBCtaProdVen: TdmkDBLookupComboBox
                Left = 60
                Top = 20
                Width = 356
                Height = 21
                KeyField = 'Codigo'
                ListField = 'Nome'
                ListSource = DsCtaProdVen
                TabOrder = 1
                OnEnter = CBAssocModNFEnter
                dmkEditCB = EdCtaProdVen
                QryCampo = 'CtaProdVen'
                UpdType = utNil
                LocF7SQLMasc = '$#'
              end
              object EdCtaProdVen: TdmkEditCB
                Left = 4
                Top = 20
                Width = 56
                Height = 21
                Alignment = taRightJustify
                TabOrder = 0
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                QryCampo = 'CtaProdVen'
                UpdCampo = 'CtaProdVen'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
                OnChange = EdAssocModNFChange
                OnEnter = EdAssocModNFEnter
                DBLookupComboBox = CBCtaProdVen
                IgnoraDBLookupComboBox = False
                AutoSetIfOnlyOneReg = setregOnlyManual
              end
              object EdTxtProdVen: TdmkEdit
                Left = 440
                Top = 20
                Width = 180
                Height = 21
                TabOrder = 2
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = 'VP'
                QryCampo = 'TxtProdVen'
                UpdCampo = 'TxtProdVen'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 'VP'
                ValWarn = False
              end
              object EdCtaServico: TdmkEditCB
                Left = 4
                Top = 100
                Width = 56
                Height = 21
                Alignment = taRightJustify
                TabOrder = 8
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                QryCampo = 'CtaServico'
                UpdCampo = 'CtaServico'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
                OnChange = EdAssocModNFChange
                OnEnter = EdAssocModNFEnter
                DBLookupComboBox = CBCtaServico
                IgnoraDBLookupComboBox = False
                AutoSetIfOnlyOneReg = setregOnlyManual
              end
              object CBCtaServico: TdmkDBLookupComboBox
                Left = 60
                Top = 100
                Width = 356
                Height = 21
                KeyField = 'Codigo'
                ListField = 'Nome'
                ListSource = DsCtaServico
                TabOrder = 9
                OnEnter = CBAssocModNFEnter
                dmkEditCB = EdCtaServico
                QryCampo = 'CtaServico'
                UpdType = utNil
                LocF7SQLMasc = '$#'
              end
              object EdTxtServico: TdmkEdit
                Left = 440
                Top = 100
                Width = 180
                Height = 21
                TabOrder = 10
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = 'MO'
                QryCampo = 'TxtServico'
                UpdCampo = 'TxtServico'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 'MO'
                ValWarn = False
              end
              object EdDupProdVen: TdmkEdit
                Left = 624
                Top = 20
                Width = 48
                Height = 21
                TabOrder = 3
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = 'VP-'
                QryCampo = 'DupProdVen'
                UpdCampo = 'DupProdVen'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 'VP-'
                ValWarn = False
              end
              object EdDupServico: TdmkEdit
                Left = 624
                Top = 100
                Width = 48
                Height = 21
                TabOrder = 11
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = 'MO-'
                QryCampo = 'DupServico'
                UpdCampo = 'DupServico'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 'MO-'
                ValWarn = False
              end
              object RGFaturaNum: TdmkRadioGroup
                Left = 680
                Top = 0
                Width = 225
                Height = 80
                Caption = ' N'#250'mero da Fatura:  '
                Items.Strings = (
                  'C'#243'digo do Faturamento'
                  'N'#250'mero da Nota Fiscal')
                TabOrder = 16
                QryCampo = 'FaturaNum'
                UpdCampo = 'FaturaNum'
                UpdType = utYes
                OldValor = 0
              end
              object RGUsaReferen: TdmkRadioGroup
                Left = 805
                Top = 85
                Width = 100
                Height = 75
                Caption = ' Itens de pedido: '
                ItemIndex = 0
                Items.Strings = (
                  'Grade'
                  'Refer.')
                TabOrder = 18
                QryCampo = 'UsaReferen'
                UpdCampo = 'UsaReferen'
                UpdType = utYes
                OldValor = 0
              end
              object EdCtaProdCom: TdmkEditCB
                Left = 4
                Top = 60
                Width = 56
                Height = 21
                Alignment = taRightJustify
                TabOrder = 4
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                QryCampo = 'CtaProdCom'
                UpdCampo = 'CtaProdCom'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
                OnChange = EdAssocModNFChange
                OnEnter = EdAssocModNFEnter
                DBLookupComboBox = CBCtaProdCom
                IgnoraDBLookupComboBox = False
                AutoSetIfOnlyOneReg = setregOnlyManual
              end
              object CBCtaProdCom: TdmkDBLookupComboBox
                Left = 60
                Top = 60
                Width = 356
                Height = 21
                KeyField = 'Codigo'
                ListField = 'Nome'
                ListSource = DsCtaProdCom
                TabOrder = 5
                OnEnter = CBAssocModNFEnter
                dmkEditCB = EdCtaProdCom
                QryCampo = 'CtaProdCom'
                UpdType = utNil
                LocF7SQLMasc = '$#'
              end
              object EdTxtProdCom: TdmkEdit
                Left = 440
                Top = 60
                Width = 180
                Height = 21
                TabOrder = 6
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = 'CP'
                QryCampo = 'TxtProdCom'
                UpdCampo = 'TxtProdCom'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 'CP'
                ValWarn = False
              end
              object EdDupProdCom: TdmkEdit
                Left = 624
                Top = 60
                Width = 48
                Height = 21
                TabOrder = 7
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = 'CP-'
                QryCampo = 'DupProdCom'
                UpdCampo = 'DupProdCom'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 'CP-'
                ValWarn = False
              end
              object EdCtaServicoPg: TdmkEditCB
                Left = 4
                Top = 140
                Width = 56
                Height = 21
                Alignment = taRightJustify
                TabOrder = 12
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                QryCampo = 'CtaServicoPg'
                UpdCampo = 'CtaServicoPg'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
                OnChange = EdAssocModNFChange
                OnEnter = EdAssocModNFEnter
                DBLookupComboBox = CBCtaServicoPg
                IgnoraDBLookupComboBox = False
                AutoSetIfOnlyOneReg = setregOnlyManual
              end
              object CBCtaServicoPg: TdmkDBLookupComboBox
                Left = 60
                Top = 140
                Width = 356
                Height = 21
                KeyField = 'Codigo'
                ListField = 'Nome'
                ListSource = DsCtaServicoPg
                TabOrder = 13
                OnEnter = CBAssocModNFEnter
                dmkEditCB = EdCtaServicoPg
                QryCampo = 'CtaServicoPg'
                UpdType = utNil
                LocF7SQLMasc = '$#'
              end
              object EdTxtServicoPg: TdmkEdit
                Left = 440
                Top = 140
                Width = 180
                Height = 21
                TabOrder = 14
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = 'PS'
                QryCampo = 'TxtServicoPg'
                UpdCampo = 'TxtServicoPg'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 'PS'
                ValWarn = False
              end
              object EdDupServicoPg: TdmkEdit
                Left = 624
                Top = 140
                Width = 48
                Height = 21
                TabOrder = 15
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = 'PS-'
                QryCampo = 'DupServicoPg'
                UpdCampo = 'DupServicoPg'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 'PS-'
                ValWarn = False
              end
              object EdCtaFretPrest: TdmkEditCB
                Left = 4
                Top = 178
                Width = 56
                Height = 21
                Alignment = taRightJustify
                TabOrder = 19
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                QryCampo = 'CtaFretPrest'
                UpdCampo = 'CtaFretPrest'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
                OnChange = EdAssocModNFChange
                OnEnter = EdAssocModNFEnter
                DBLookupComboBox = CBCtaFretPrest
                IgnoraDBLookupComboBox = False
                AutoSetIfOnlyOneReg = setregOnlyManual
              end
              object CBCtaFretPrest: TdmkDBLookupComboBox
                Left = 60
                Top = 178
                Width = 356
                Height = 21
                KeyField = 'Codigo'
                ListField = 'Nome'
                ListSource = DsCtaFretPrest
                TabOrder = 20
                OnEnter = CBAssocModNFEnter
                dmkEditCB = EdCtaFretPrest
                QryCampo = 'CtaFretPrest'
                UpdType = utNil
                LocF7SQLMasc = '$#'
              end
              object EdTxtFretPrest: TdmkEdit
                Left = 440
                Top = 178
                Width = 180
                Height = 21
                TabOrder = 21
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = 'FP'
                QryCampo = 'TxtFretPrest'
                UpdCampo = 'TxtFretPrest'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 'FP'
                ValWarn = False
              end
              object EdDupFretPrest: TdmkEdit
                Left = 624
                Top = 178
                Width = 48
                Height = 21
                TabOrder = 22
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = 'FP-'
                QryCampo = 'DupFretPrest'
                UpdCampo = 'DupFretPrest'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 'FP-'
                ValWarn = False
              end
            end
          end
        end
      end
      object TabSheet6: TTabSheet
        Caption = ' Estoque '
        ImageIndex = 3
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object Panel37: TPanel
          Left = 0
          Top = 65
          Width = 1046
          Height = 400
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 1
          object GroupBox64: TGroupBox
            Left = 8
            Top = 16
            Width = 765
            Height = 57
            Caption = ' Frete: '
            TabOrder = 0
            object Panel102: TPanel
              Left = 2
              Top = 15
              Width = 761
              Height = 40
              Align = alClient
              BevelOuter = bvNone
              TabOrder = 0
              object Label439: TLabel
                Left = 120
                Top = 0
                Width = 66
                Height = 13
                Caption = '% Ret. ICMS :'
              end
              object Label442: TLabel
                Left = 296
                Top = 0
                Width = 54
                Height = 13
                Caption = '% Ret. PIS:'
              end
              object Label444: TLabel
                Left = 472
                Top = 0
                Width = 70
                Height = 13
                Caption = '% R. COFINS :'
              end
              object EdFreteRpICMS: TdmkEdit
                Left = 120
                Top = 15
                Width = 68
                Height = 21
                Alignment = taRightJustify
                TabOrder = 0
                FormatType = dmktfDouble
                MskType = fmtNone
                DecimalSize = 4
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0,0000'
                QryCampo = 'FreteRpICMS'
                UpdCampo = 'FreteRpICMS'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0.000000000000000000
                ValWarn = False
              end
              object EdFreteRpPIS: TdmkEdit
                Left = 296
                Top = 15
                Width = 68
                Height = 21
                Alignment = taRightJustify
                TabOrder = 1
                FormatType = dmktfDouble
                MskType = fmtNone
                DecimalSize = 4
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0,0000'
                QryCampo = 'FreteRpPIS'
                UpdCampo = 'FreteRpPIS'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0.000000000000000000
                ValWarn = False
              end
              object EdFreteRpCOFINS: TdmkEdit
                Left = 472
                Top = 15
                Width = 68
                Height = 21
                Alignment = taRightJustify
                TabOrder = 2
                FormatType = dmktfDouble
                MskType = fmtNone
                DecimalSize = 4
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0,0000'
                QryCampo = 'FreteRpCOFINS'
                UpdCampo = 'FreteRpCOFINS'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0.000000000000000000
                ValWarn = False
              end
            end
          end
          object CkRetImpost: TdmkCheckBox
            Left = 8
            Top = 0
            Width = 257
            Height = 17
            Caption = 'Se credita de impostos na compra e frete.'
            TabOrder = 1
            QryCampo = 'RetImpost'
            UpdCampo = 'RetImpost'
            UpdType = utYes
            ValCheck = '1'
            ValUncheck = '0'
            OldValor = #0
          end
        end
        object Panel10: TPanel
          Left = 0
          Top = 0
          Width = 1046
          Height = 65
          Align = alTop
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          object Label10: TLabel
            Left = 8
            Top = 12
            Width = 190
            Height = 13
            Caption = 'Quantidade padr'#227'o de itens no balan'#231'o:'
          end
          object EdBalQtdItem: TdmkEdit
            Left = 200
            Top = 8
            Width = 80
            Height = 21
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 3
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,000'
            QryCampo = 'BalQtdItem'
            UpdCampo = 'BalQtdItem'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
          object CkEstq0UsoCons: TdmkCheckBox
            Left = 8
            Top = 40
            Width = 257
            Height = 17
            Caption = 'Estoque nulo para materiais no uso e consumo.'
            TabOrder = 1
            QryCampo = 'Estq0UsoCons'
            UpdCampo = 'Estq0UsoCons'
            UpdType = utYes
            ValCheck = '1'
            ValUncheck = '0'
            OldValor = #0
          end
        end
      end
      object TabSheet9: TTabSheet
        Caption = ' NF-e '
        ImageIndex = 4
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object Panel13: TPanel
          Left = 0
          Top = 0
          Width = 1046
          Height = 465
          Align = alClient
          ParentBackground = False
          TabOrder = 0
          object Label27: TLabel
            Left = 8
            Top = 92
            Width = 129
            Height = 13
            Caption = 'Diret'#243'rio das NF-e geradas:'
          end
          object SpeedButton8: TSpeedButton
            Left = 742
            Top = 108
            Width = 21
            Height = 21
          end
          object Label28: TLabel
            Left = 8
            Top = 132
            Width = 138
            Height = 13
            Caption = 'Diret'#243'rio das NF-e assinadas:'
          end
          object SpeedButton10: TSpeedButton
            Left = 742
            Top = 148
            Width = 21
            Height = 21
          end
          object PageControl3: TPageControl
            Left = 1
            Top = 1
            Width = 1044
            Height = 463
            ActivePage = TabSheet21
            Align = alClient
            TabOrder = 0
            object TabSheet10: TTabSheet
              Caption = ' Configura'#231#245'es gerais '
              ExplicitLeft = 0
              ExplicitTop = 0
              ExplicitWidth = 0
              ExplicitHeight = 0
              object Panel16: TPanel
                Left = 0
                Top = 0
                Width = 1036
                Height = 435
                Align = alClient
                BevelOuter = bvNone
                ParentBackground = False
                TabOrder = 0
                object Panel34: TPanel
                  Left = 0
                  Top = 125
                  Width = 1036
                  Height = 100
                  Align = alTop
                  BevelOuter = bvNone
                  TabOrder = 0
                  object RGNFetpEmis: TdmkRadioGroup
                    Left = 0
                    Top = 0
                    Width = 269
                    Height = 100
                    Align = alLeft
                    Caption = ' Tipo de emiss'#227'o da NF-e: (apenas informativo)'
                    Columns = 2
                    Enabled = False
                    ItemIndex = 1
                    Items.Strings = (
                      '0 - Nenhum'
                      '1 - Normal'
                      '2 - Contingencia FS'
                      '3 - Conting'#234'ncia SCAN'
                      '4 - Conting'#234'ncia DPEC'
                      '5 - Conting'#234'ncia FS-DA')
                    TabOrder = 0
                    QryCampo = 'NFetpEmis'
                    UpdType = utYes
                    OldValor = 0
                  end
                  object GroupBox29: TGroupBox
                    Left = 269
                    Top = 0
                    Width = 767
                    Height = 100
                    Align = alClient
                    Caption = ' Pr'#233'-emails: '
                    TabOrder = 1
                    object Label88: TLabel
                      Left = 8
                      Top = 16
                      Width = 192
                      Height = 13
                      Caption = 'Pr'#233'-email para envio de NF-e autorizada:'
                    end
                    object Label89: TLabel
                      Left = 8
                      Top = 56
                      Width = 193
                      Height = 13
                      Caption = 'Pr'#233'-email para envio de NF-e cancelada:'
                    end
                    object Label156: TLabel
                      Left = 380
                      Top = 16
                      Width = 250
                      Height = 13
                      Caption = 'Pr'#233'-email para envio de carta de corre'#231#227'o eletr'#244'nica:'
                    end
                    object EdPreMailAut: TdmkEditCB
                      Left = 8
                      Top = 32
                      Width = 44
                      Height = 21
                      Alignment = taRightJustify
                      TabOrder = 0
                      FormatType = dmktfInteger
                      MskType = fmtNone
                      DecimalSize = 0
                      LeftZeros = 0
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ValMin = '-2147483647'
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      Texto = '0'
                      QryName = 'QrPrmsEmpNFe'
                      UpdCampo = 'PreMailAut'
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = 0
                      ValWarn = False
                      DBLookupComboBox = CBPreMailAut
                      IgnoraDBLookupComboBox = False
                      AutoSetIfOnlyOneReg = setregOnlyManual
                    end
                    object CBPreMailAut: TdmkDBLookupComboBox
                      Left = 52
                      Top = 32
                      Width = 321
                      Height = 21
                      KeyField = 'Codigo'
                      ListField = 'Nome'
                      ListSource = DsPreMail0
                      TabOrder = 1
                      dmkEditCB = EdPreMailAut
                      QryName = 'QrPrmsEmpNFe'
                      QryCampo = 'PreMailAut'
                      UpdType = utYes
                      LocF7SQLMasc = '$#'
                    end
                    object EdPreMailCan: TdmkEditCB
                      Left = 8
                      Top = 72
                      Width = 44
                      Height = 21
                      Alignment = taRightJustify
                      TabOrder = 4
                      FormatType = dmktfInteger
                      MskType = fmtNone
                      DecimalSize = 0
                      LeftZeros = 0
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ValMin = '-2147483647'
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      Texto = '0'
                      QryName = 'QrPrmsEmpNFe'
                      UpdCampo = 'PreMailCan'
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = 0
                      ValWarn = False
                      DBLookupComboBox = CBPreMailCan
                      IgnoraDBLookupComboBox = False
                      AutoSetIfOnlyOneReg = setregOnlyManual
                    end
                    object CBPreMailCan: TdmkDBLookupComboBox
                      Left = 52
                      Top = 72
                      Width = 321
                      Height = 21
                      KeyField = 'Codigo'
                      ListField = 'Nome'
                      ListSource = DsPreMail1
                      TabOrder = 5
                      dmkEditCB = EdPreMailCan
                      QryName = 'QrPrmsEmpNFe'
                      QryCampo = 'PreMailCan'
                      UpdType = utYes
                      LocF7SQLMasc = '$#'
                    end
                    object EdPreMailEveCCe: TdmkEditCB
                      Left = 380
                      Top = 32
                      Width = 44
                      Height = 21
                      Alignment = taRightJustify
                      TabOrder = 2
                      FormatType = dmktfInteger
                      MskType = fmtNone
                      DecimalSize = 0
                      LeftZeros = 0
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ValMin = '-2147483647'
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      Texto = '0'
                      QryName = 'QrPrmsEmpNFe'
                      UpdCampo = 'PreMailEveCCe'
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = 0
                      ValWarn = False
                      DBLookupComboBox = CBPreMailEveCCe
                      IgnoraDBLookupComboBox = False
                      AutoSetIfOnlyOneReg = setregOnlyManual
                    end
                    object CBPreMailEveCCe: TdmkDBLookupComboBox
                      Left = 424
                      Top = 32
                      Width = 321
                      Height = 21
                      KeyField = 'Codigo'
                      ListField = 'Nome'
                      ListSource = DsPreMail0
                      TabOrder = 3
                      dmkEditCB = EdPreMailEveCCe
                      QryName = 'QrPrmsEmpNFe'
                      QryCampo = 'PreMailEveCCe'
                      UpdType = utYes
                      LocF7SQLMasc = '$#'
                    end
                  end
                end
                object Panel43: TPanel
                  Left = 0
                  Top = 225
                  Width = 1036
                  Height = 108
                  Align = alTop
                  BevelOuter = bvNone
                  TabOrder = 1
                  object GroupBox32: TGroupBox
                    Left = 589
                    Top = 0
                    Width = 447
                    Height = 108
                    Align = alClient
                    TabOrder = 1
                    object Panel33: TPanel
                      Left = 2
                      Top = 15
                      Width = 443
                      Height = 91
                      Align = alClient
                      BevelOuter = bvNone
                      TabOrder = 0
                      object Label86: TLabel
                        Left = 8
                        Top = 4
                        Width = 282
                        Height = 13
                        Caption = 'Diret'#243'rio das NF-e protocoladas (autorizadas e canceladas):'
                      end
                      object SpeedButton14: TSpeedButton
                        Left = 411
                        Top = 19
                        Width = 21
                        Height = 21
                        Caption = '...'
                        OnClick = SpeedButton14Click
                      end
                      object Label138: TLabel
                        Left = 8
                        Top = 44
                        Width = 318
                        Height = 13
                        Caption = 
                          'Diret'#243'rio das NF-e recuperadas da web (autorizadas e canceladas)' +
                          ':'
                      end
                      object SpeedButton18: TSpeedButton
                        Left = 411
                        Top = 59
                        Width = 21
                        Height = 21
                        Caption = '...'
                        OnClick = SpeedButton18Click
                      end
                      object EdDirNFeProt: TdmkEdit
                        Left = 8
                        Top = 19
                        Width = 400
                        Height = 21
                        TabOrder = 0
                        FormatType = dmktfString
                        MskType = fmtNone
                        DecimalSize = 0
                        LeftZeros = 0
                        NoEnterToTab = False
                        NoForceUppercase = False
                        ValMin = '-2147483647'
                        ForceNextYear = False
                        DataFormat = dmkdfShort
                        HoraFormat = dmkhfShort
                        QryName = 'QrPrmsEmpNFe'
                        UpdCampo = 'DirNFeProt'
                        UpdType = utYes
                        Obrigatorio = False
                        PermiteNulo = False
                        ValueVariant = ''
                        ValWarn = False
                      end
                      object EdDirNFeRWeb: TdmkEdit
                        Left = 8
                        Top = 59
                        Width = 400
                        Height = 21
                        TabOrder = 1
                        FormatType = dmktfString
                        MskType = fmtNone
                        DecimalSize = 0
                        LeftZeros = 0
                        NoEnterToTab = False
                        NoForceUppercase = False
                        ValMin = '-2147483647'
                        ForceNextYear = False
                        DataFormat = dmkdfShort
                        HoraFormat = dmkhfShort
                        QryName = 'QrPrmsEmpNFe'
                        UpdCampo = 'DirNFeRWeb'
                        UpdType = utYes
                        Obrigatorio = False
                        PermiteNulo = False
                        ValueVariant = ''
                        ValWarn = False
                      end
                    end
                  end
                  object GroupBox31: TGroupBox
                    Left = 0
                    Top = 0
                    Width = 121
                    Height = 108
                    Align = alLeft
                    Caption = ' Conting'#234'ncia SCAN: '
                    TabOrder = 0
                    object Panel44: TPanel
                      Left = 2
                      Top = 15
                      Width = 117
                      Height = 91
                      Align = alClient
                      BevelOuter = bvNone
                      TabOrder = 0
                      object Label136: TLabel
                        Left = 8
                        Top = 4
                        Width = 24
                        Height = 13
                        Caption = 'S'#233'rie'
                      end
                      object Label137: TLabel
                        Left = 8
                        Top = 44
                        Width = 55
                        Height = 13
                        Caption = #218'ltima NF-e'
                      end
                      object EdSCAN_Ser: TdmkEdit
                        Left = 8
                        Top = 20
                        Width = 80
                        Height = 21
                        Alignment = taRightJustify
                        TabOrder = 0
                        FormatType = dmktfInteger
                        MskType = fmtNone
                        DecimalSize = 0
                        LeftZeros = 0
                        NoEnterToTab = False
                        NoForceUppercase = False
                        ValMin = '900'
                        ValMax = '999'
                        ForceNextYear = False
                        DataFormat = dmkdfShort
                        HoraFormat = dmkhfShort
                        Texto = '900'
                        QryName = 'QrPrmsEmpNFe'
                        UpdCampo = 'SCAN_Ser'
                        UpdType = utYes
                        Obrigatorio = False
                        PermiteNulo = False
                        ValueVariant = 900
                        ValWarn = False
                      end
                      object EdSCAN_nNF: TdmkEdit
                        Left = 8
                        Top = 60
                        Width = 80
                        Height = 21
                        Alignment = taRightJustify
                        TabOrder = 1
                        FormatType = dmktfInteger
                        MskType = fmtNone
                        DecimalSize = 0
                        LeftZeros = 0
                        NoEnterToTab = False
                        NoForceUppercase = False
                        ValMax = '0'
                        ForceNextYear = False
                        DataFormat = dmkdfShort
                        HoraFormat = dmkhfShort
                        Texto = '0'
                        QryName = 'QrPrmsEmpNFe'
                        UpdCampo = 'SCAN_nNF'
                        UpdType = utYes
                        Obrigatorio = False
                        PermiteNulo = False
                        ValueVariant = 0
                        ValWarn = False
                      end
                    end
                  end
                  object GroupBox67: TGroupBox
                    Left = 121
                    Top = 0
                    Width = 468
                    Height = 108
                    Align = alLeft
                    TabOrder = 2
                    object PninfRespTec_Usa: TPanel
                      Left = 2
                      Top = 15
                      Width = 464
                      Height = 91
                      Align = alClient
                      BevelOuter = bvNone
                      TabOrder = 0
                      Visible = False
                      object Label440: TLabel
                        Left = 8
                        Top = 4
                        Width = 103
                        Height = 13
                        Caption = 'CNPJ desenvolvedor:'
                      end
                      object Label441: TLabel
                        Left = 148
                        Top = 4
                        Width = 143
                        Height = 13
                        Caption = 'Nome do contato na empresa:'
                      end
                      object Label443: TLabel
                        Left = 340
                        Top = 4
                        Width = 81
                        Height = 13
                        Caption = 'Fone do contato:'
                      end
                      object Label445: TLabel
                        Left = 8
                        Top = 44
                        Width = 142
                        Height = 13
                        Caption = 'e-mail do contato na empresa:'
                      end
                      object Label446: TLabel
                        Left = 188
                        Top = 44
                        Width = 94
                        Height = 13
                        Caption = 'Id e valor do CSRT:'
                      end
                      object SpeedButton71: TSpeedButton
                        Left = 8
                        Top = 20
                        Width = 21
                        Height = 21
                        Caption = 'V'
                      end
                      object EdinfRespTec_xContato: TdmkEdit
                        Left = 148
                        Top = 20
                        Width = 189
                        Height = 21
                        TabOrder = 0
                        FormatType = dmktfString
                        MskType = fmtNone
                        DecimalSize = 0
                        LeftZeros = 0
                        NoEnterToTab = False
                        NoForceUppercase = False
                        ValMax = '0'
                        ForceNextYear = False
                        DataFormat = dmkdfShort
                        HoraFormat = dmkhfShort
                        QryName = 'QrPrmsEmpNFe'
                        UpdCampo = 'infRespTec_xContato'
                        UpdType = utYes
                        Obrigatorio = False
                        PermiteNulo = False
                        ValueVariant = ''
                        ValWarn = False
                      end
                      object EdinfRespTec_CNPJ: TdmkEdit
                        Left = 32
                        Top = 20
                        Width = 113
                        Height = 21
                        Font.Charset = DEFAULT_CHARSET
                        Font.Color = clWindowText
                        Font.Height = -12
                        Font.Name = 'MS Sans Serif'
                        Font.Style = []
                        ParentFont = False
                        TabOrder = 1
                        FormatType = dmktfString
                        MskType = fmtCPFJ
                        DecimalSize = 0
                        LeftZeros = 0
                        NoEnterToTab = False
                        NoForceUppercase = False
                        ForceNextYear = False
                        DataFormat = dmkdfShort
                        HoraFormat = dmkhfShort
                        QryName = 'QrPrmsEmpNFe'
                        UpdCampo = 'infRespTec_CNPJ'
                        UpdType = utYes
                        Obrigatorio = False
                        PermiteNulo = False
                        ValueVariant = ''
                        ValWarn = False
                      end
                      object EdinfRespTec_fone: TdmkEdit
                        Left = 340
                        Top = 20
                        Width = 117
                        Height = 21
                        TabOrder = 2
                        FormatType = dmktfString
                        MskType = fmtNone
                        DecimalSize = 0
                        LeftZeros = 0
                        NoEnterToTab = False
                        NoForceUppercase = False
                        ValMax = '0'
                        ForceNextYear = False
                        DataFormat = dmkdfShort
                        HoraFormat = dmkhfShort
                        QryName = 'QrPrmsEmpNFe'
                        UpdCampo = 'infRespTec_fone'
                        UpdType = utYes
                        Obrigatorio = False
                        PermiteNulo = False
                        ValueVariant = ''
                        ValWarn = False
                      end
                      object EdinfRespTec_email: TdmkEdit
                        Left = 8
                        Top = 60
                        Width = 177
                        Height = 21
                        TabOrder = 3
                        FormatType = dmktfString
                        MskType = fmtNone
                        DecimalSize = 0
                        LeftZeros = 0
                        NoEnterToTab = False
                        NoForceUppercase = False
                        ValMax = '0'
                        ForceNextYear = False
                        DataFormat = dmkdfShort
                        HoraFormat = dmkhfShort
                        QryName = 'QrPrmsEmpNFe'
                        UpdCampo = 'infRespTec_email'
                        UpdType = utYes
                        Obrigatorio = False
                        PermiteNulo = False
                        ValueVariant = ''
                        ValWarn = False
                      end
                      object EdinfRespTec_idCSRT: TdmkEdit
                        Left = 188
                        Top = 60
                        Width = 21
                        Height = 21
                        TabOrder = 4
                        FormatType = dmktfString
                        MskType = fmtNone
                        DecimalSize = 0
                        LeftZeros = 0
                        NoEnterToTab = False
                        NoForceUppercase = False
                        ValMax = '0'
                        ForceNextYear = False
                        DataFormat = dmkdfShort
                        HoraFormat = dmkhfShort
                        QryName = 'QrPrmsEmpNFe'
                        UpdCampo = 'infRespTec_idCSRT'
                        UpdType = utYes
                        Obrigatorio = False
                        PermiteNulo = False
                        ValueVariant = ''
                        ValWarn = False
                      end
                      object EdinfRespTec_CSRT: TdmkEdit
                        Left = 208
                        Top = 60
                        Width = 249
                        Height = 21
                        TabOrder = 5
                        FormatType = dmktfString
                        MskType = fmtNone
                        DecimalSize = 0
                        LeftZeros = 0
                        NoEnterToTab = False
                        NoForceUppercase = False
                        ValMax = '0'
                        ForceNextYear = False
                        DataFormat = dmkdfShort
                        HoraFormat = dmkhfShort
                        QryName = 'QrPrmsEmpNFe'
                        UpdCampo = 'infRespTec_CSRT'
                        UpdType = utYes
                        Obrigatorio = False
                        PermiteNulo = False
                        ValueVariant = ''
                        ValWarn = False
                      end
                    end
                    object CkinfRespTec_Usa: TdmkCheckBox
                      Left = 16
                      Top = 0
                      Width = 213
                      Height = 17
                      Caption = ' Identifica'#231#227'o do Respons'#225'vel T'#233'cnico: '
                      TabOrder = 1
                      OnClick = CkinfRespTec_UsaClick
                      QryName = 'QrPrmsEmpNFe'
                      UpdCampo = 'infRespTec_Usa'
                      UpdType = utYes
                      ValCheck = '1'
                      ValUncheck = '0'
                      OldValor = #0
                    end
                  end
                end
                object Panel32: TPanel
                  Left = 0
                  Top = 0
                  Width = 1036
                  Height = 125
                  Align = alTop
                  BevelOuter = bvNone
                  TabOrder = 2
                  object GroupBox30: TGroupBox
                    Left = 0
                    Top = 0
                    Width = 769
                    Height = 125
                    Align = alLeft
                    TabOrder = 0
                    object Panel57: TPanel
                      Left = 2
                      Top = 15
                      Width = 765
                      Height = 108
                      Align = alClient
                      BevelOuter = bvNone
                      TabOrder = 0
                      object Label41: TLabel
                        Left = 8
                        Top = 4
                        Width = 69
                        Height = 13
                        Caption = 'UF da SEFAZ:'
                      end
                      object Label61: TLabel
                        Left = 155
                        Top = 4
                        Width = 395
                        Height = 13
                        Caption = 
                          'Sigla para adicionar '#224's informa'#231#245'es de produtos customizados na ' +
                          ' NF-e (infAdProd):'
                      end
                      object Label75: TLabel
                        Left = 8
                        Top = 28
                        Width = 83
                        Height = 13
                        Caption = 'WebService [F4]:'
                      end
                      object Label73: TLabel
                        Left = 7
                        Top = 56
                        Width = 223
                        Height = 13
                        Caption = 'N'#250'mero de S'#233'rie do Certificado Digital da NF-e:'
                      end
                      object SpeedButton12: TSpeedButton
                        Left = 736
                        Top = 52
                        Width = 21
                        Height = 21
                        Caption = '...'
                        OnClick = SpeedButton12Click
                      end
                      object Label92: TLabel
                        Left = 8
                        Top = 80
                        Width = 181
                        Height = 13
                        Caption = 'Data da validade do certificado digital:'
                      end
                      object Label93: TLabel
                        Left = 336
                        Top = 80
                        Width = 268
                        Height = 13
                        Caption = 'Avisar a expira'#231#227'o da validade do certificado digital com '
                      end
                      object Label94: TLabel
                        Left = 652
                        Top = 80
                        Width = 105
                        Height = 13
                        Caption = 'dias de anteced'#234'ncia.'
                      end
                      object EdUF_WebServ: TdmkEdit
                        Left = 108
                        Top = 0
                        Width = 40
                        Height = 21
                        CharCase = ecUpperCase
                        TabOrder = 0
                        FormatType = dmktfString
                        MskType = fmtNone
                        DecimalSize = 0
                        LeftZeros = 0
                        NoEnterToTab = False
                        NoForceUppercase = False
                        ForceNextYear = False
                        DataFormat = dmkdfShort
                        HoraFormat = dmkhfShort
                        Texto = 'PR'
                        QryName = 'QrPrmsEmpNFe'
                        UpdCampo = 'UF_WebServ'
                        UpdType = utYes
                        Obrigatorio = False
                        PermiteNulo = False
                        ValueVariant = 'PR'
                        ValWarn = False
                      end
                      object EdSiglaCustm: TdmkEdit
                        Left = 559
                        Top = 0
                        Width = 200
                        Height = 21
                        MaxLength = 15
                        TabOrder = 1
                        FormatType = dmktfString
                        MskType = fmtNone
                        DecimalSize = 0
                        LeftZeros = 0
                        NoEnterToTab = False
                        NoForceUppercase = False
                        ForceNextYear = False
                        DataFormat = dmkdfShort
                        HoraFormat = dmkhfShort
                        QryName = 'QrPrmsEmpNFe'
                        UpdCampo = 'SiglaCustm'
                        UpdType = utYes
                        Obrigatorio = False
                        PermiteNulo = False
                        ValueVariant = ''
                        ValWarn = False
                      end
                      object EdUF_Servico: TdmkEdit
                        Left = 108
                        Top = 24
                        Width = 40
                        Height = 21
                        CharCase = ecUpperCase
                        TabOrder = 2
                        FormatType = dmktfString
                        MskType = fmtNone
                        DecimalSize = 0
                        LeftZeros = 0
                        NoEnterToTab = False
                        NoForceUppercase = False
                        ForceNextYear = False
                        DataFormat = dmkdfShort
                        HoraFormat = dmkhfShort
                        Texto = 'PR'
                        QryName = 'QrPrmsEmpNFe'
                        UpdCampo = 'UF_Servico'
                        UpdType = utYes
                        Obrigatorio = False
                        PermiteNulo = False
                        ValueVariant = 'PR'
                        ValWarn = False
                        OnKeyDown = EdUF_ServicoKeyDown
                      end
                      object CkSimplesFed: TdmkCheckBox
                        Left = 156
                        Top = 28
                        Width = 169
                        Height = 17
                        Caption = 'Ativo no super simples (federal).'
                        TabOrder = 3
                        QryName = 'QrPrmsEmpNFe'
                        UpdCampo = 'SimplesFed'
                        UpdType = utYes
                        ValCheck = #0
                        ValUncheck = #0
                        OldValor = #0
                      end
                      object CkInfoPerCuz: TdmkCheckBox
                        Left = 330
                        Top = 28
                        Width = 429
                        Height = 17
                        Caption = 
                          'Informa percentual de customiza'#231#227'o nas informa'#231#245'es adicionais do' +
                          's produtos na NF-e.'
                        TabOrder = 4
                        QryName = 'QrPrmsEmpNFe'
                        UpdCampo = 'InfoPerCuz'
                        UpdType = utYes
                        ValCheck = #0
                        ValUncheck = #0
                        OldValor = #0
                      end
                      object EdNFeSerNum: TdmkEdit
                        Left = 240
                        Top = 52
                        Width = 493
                        Height = 21
                        MaxLength = 15
                        TabOrder = 5
                        FormatType = dmktfString
                        MskType = fmtNone
                        DecimalSize = 0
                        LeftZeros = 0
                        NoEnterToTab = False
                        NoForceUppercase = False
                        ForceNextYear = False
                        DataFormat = dmkdfShort
                        HoraFormat = dmkhfShort
                        QryName = 'QrPrmsEmpNFe'
                        UpdCampo = 'NFeSerNum'
                        UpdType = utYes
                        Obrigatorio = False
                        PermiteNulo = False
                        ValueVariant = ''
                        ValWarn = False
                      end
                      object TPNFeSerVal: TdmkEditDateTimePicker
                        Left = 200
                        Top = 76
                        Width = 112
                        Height = 21
                        Date = 0.669258657406317100
                        Time = 0.669258657406317100
                        TabOrder = 6
                        ReadOnly = False
                        DefaultEditMask = '!99/99/99;1;_'
                        AutoApplyEditMask = True
                        UpdCampo = 'NFeSerVal'
                        UpdType = utYes
                        DatePurpose = dmkdpNone
                      end
                      object EdNFeSerAvi: TdmkEdit
                        Left = 608
                        Top = 76
                        Width = 40
                        Height = 21
                        Alignment = taRightJustify
                        TabOrder = 7
                        FormatType = dmktfInteger
                        MskType = fmtNone
                        DecimalSize = 0
                        LeftZeros = 0
                        NoEnterToTab = False
                        NoForceUppercase = False
                        ForceNextYear = False
                        DataFormat = dmkdfShort
                        HoraFormat = dmkhfShort
                        Texto = '30'
                        QryName = 'QrPrmsEmpNFe'
                        UpdCampo = 'NFeSerAvi'
                        UpdType = utYes
                        Obrigatorio = False
                        PermiteNulo = False
                        ValueVariant = 30
                        ValWarn = False
                      end
                    end
                  end
                  object GroupBox38: TGroupBox
                    Left = 769
                    Top = 0
                    Width = 267
                    Height = 125
                    Align = alClient
                    Caption = ' WebServices espec'#237'ficos: '
                    TabOrder = 1
                    object Panel56: TPanel
                      Left = 2
                      Top = 15
                      Width = 263
                      Height = 108
                      Align = alClient
                      BevelOuter = bvNone
                      TabOrder = 0
                      object Label238: TLabel
                        Left = 8
                        Top = 4
                        Width = 139
                        Height = 13
                        Caption = 'Manifesta'#231#227'o do destinat'#225'rio:'
                      end
                      object Label239: TLabel
                        Left = 8
                        Top = 28
                        Width = 124
                        Height = 13
                        Caption = 'Consulta NF-e destinadas:'
                      end
                      object Label240: TLabel
                        Left = 8
                        Top = 52
                        Width = 77
                        Height = 13
                        Caption = 'Download NF-e:'
                      end
                      object Label436: TLabel
                        Left = 9
                        Top = 76
                        Width = 114
                        Height = 13
                        Caption = 'WebService EPEC [F4]:'
                      end
                      object EdUF_MDeMDe: TdmkEdit
                        Left = 152
                        Top = 0
                        Width = 40
                        Height = 21
                        CharCase = ecUpperCase
                        TabOrder = 0
                        FormatType = dmktfString
                        MskType = fmtNone
                        DecimalSize = 0
                        LeftZeros = 0
                        NoEnterToTab = False
                        NoForceUppercase = False
                        ForceNextYear = False
                        DataFormat = dmkdfShort
                        HoraFormat = dmkhfShort
                        Texto = 'PR'
                        QryName = 'QrPrmsEmpNFe'
                        UpdCampo = 'UF_MDeMDe'
                        UpdType = utYes
                        Obrigatorio = False
                        PermiteNulo = False
                        ValueVariant = 'PR'
                        ValWarn = False
                        OnKeyDown = EdUF_ServicoKeyDown
                      end
                      object EdUF_MDeDes: TdmkEdit
                        Left = 152
                        Top = 24
                        Width = 40
                        Height = 21
                        CharCase = ecUpperCase
                        TabOrder = 1
                        FormatType = dmktfString
                        MskType = fmtNone
                        DecimalSize = 0
                        LeftZeros = 0
                        NoEnterToTab = False
                        NoForceUppercase = False
                        ForceNextYear = False
                        DataFormat = dmkdfShort
                        HoraFormat = dmkhfShort
                        Texto = 'PR'
                        QryName = 'QrPrmsEmpNFe'
                        UpdCampo = 'UF_MDeDes'
                        UpdType = utYes
                        Obrigatorio = False
                        PermiteNulo = False
                        ValueVariant = 'PR'
                        ValWarn = False
                        OnKeyDown = EdUF_ServicoKeyDown
                      end
                      object EdUF_MDeNFe: TdmkEdit
                        Left = 152
                        Top = 48
                        Width = 40
                        Height = 21
                        CharCase = ecUpperCase
                        TabOrder = 2
                        FormatType = dmktfString
                        MskType = fmtNone
                        DecimalSize = 0
                        LeftZeros = 0
                        NoEnterToTab = False
                        NoForceUppercase = False
                        ForceNextYear = False
                        DataFormat = dmkdfShort
                        HoraFormat = dmkhfShort
                        Texto = 'PR'
                        QryName = 'QrPrmsEmpNFe'
                        UpdCampo = 'UF_MDeNFe'
                        UpdType = utYes
                        Obrigatorio = False
                        PermiteNulo = False
                        ValueVariant = 'PR'
                        ValWarn = False
                        OnKeyDown = EdUF_ServicoKeyDown
                      end
                      object EdNFeUF_EPEC: TdmkEdit
                        Left = 151
                        Top = 72
                        Width = 52
                        Height = 21
                        CharCase = ecUpperCase
                        TabOrder = 3
                        FormatType = dmktfString
                        MskType = fmtNone
                        DecimalSize = 0
                        LeftZeros = 0
                        NoEnterToTab = False
                        NoForceUppercase = False
                        ForceNextYear = False
                        DataFormat = dmkdfShort
                        HoraFormat = dmkhfShort
                        Texto = 'SVSP'
                        QryName = 'QrPrmsEmpNFe'
                        UpdCampo = 'NFeUF_EPEC'
                        UpdType = utYes
                        Obrigatorio = False
                        PermiteNulo = False
                        ValueVariant = 'SVSP'
                        ValWarn = False
                        OnKeyDown = EdNFeUF_EPECKeyDown
                      end
                    end
                  end
                end
              end
            end
            object TabSheet11: TTabSheet
              Caption = ' Diret'#243'rios de arquivos XML [1] '
              ImageIndex = 1
              ExplicitLeft = 0
              ExplicitTop = 0
              ExplicitWidth = 0
              ExplicitHeight = 0
              object Panel20: TPanel
                Left = 0
                Top = 0
                Width = 1036
                Height = 435
                Align = alClient
                BevelOuter = bvNone
                ParentBackground = False
                TabOrder = 0
                object Label29: TLabel
                  Left = 8
                  Top = 84
                  Width = 190
                  Height = 13
                  Caption = 'Diret'#243'rio dos lotes gerados (-env-lot.xml):'
                end
                object Label33: TLabel
                  Left = 8
                  Top = 124
                  Width = 246
                  Height = 13
                  Caption = 'Diret'#243'rio das respostas dos lotes enviados (-rec.xml):'
                end
                object Label35: TLabel
                  Left = 8
                  Top = 204
                  Width = 342
                  Height = 13
                  Caption = 
                    'Diret'#243'rio dos resultados de processamento de lote de NF-e (-pro-' +
                    'rec.xml):'
                end
                object SBDirRec: TSpeedButton
                  Left = 739
                  Top = 140
                  Width = 21
                  Height = 21
                  Caption = '...'
                  OnClick = SBDirRecClick
                end
                object SBDirEnvLot: TSpeedButton
                  Left = 739
                  Top = 100
                  Width = 21
                  Height = 21
                  Caption = '...'
                  OnClick = SBDirEnvLotClick
                end
                object SBDirProRec: TSpeedButton
                  Left = 739
                  Top = 220
                  Width = 21
                  Height = 21
                  Caption = '...'
                  OnClick = SBDirProRecClick
                end
                object Label43: TLabel
                  Left = 8
                  Top = 44
                  Width = 183
                  Height = 13
                  Caption = 'Diret'#243'rio das NF-e assinadas (-nfe.xml):'
                end
                object SBDirNfeAss: TSpeedButton
                  Left = 739
                  Top = 60
                  Width = 21
                  Height = 21
                  Caption = '...'
                  OnClick = SBDirNfeAssClick
                end
                object Label44: TLabel
                  Left = 8
                  Top = 4
                  Width = 174
                  Height = 13
                  Caption = 'Diret'#243'rio das NF-e geradas (-nfe.xml):'
                end
                object SBDirNFeGer: TSpeedButton
                  Left = 739
                  Top = 20
                  Width = 21
                  Height = 21
                  Caption = '...'
                  OnClick = SBDirNFeGerClick
                end
                object Label47: TLabel
                  Left = 8
                  Top = 244
                  Width = 192
                  Height = 13
                  Caption = 'Diret'#243'rio das NF-e denegadas (-den.xml):'
                end
                object SBDirDen: TSpeedButton
                  Left = 739
                  Top = 260
                  Width = 21
                  Height = 21
                  Caption = '...'
                  OnClick = SBDirDenClick
                end
                object Label37: TLabel
                  Left = 8
                  Top = 164
                  Width = 400
                  Height = 13
                  Caption = 
                    'Diret'#243'rio dos pedidos de resultado de processamento de lotes de ' +
                    'NF-e (-ped-rec.xml):'
                end
                object SBDirPedRec: TSpeedButton
                  Left = 739
                  Top = 180
                  Width = 21
                  Height = 21
                  Caption = '...'
                  OnClick = SBDirPedRecClick
                end
                object EdDirEnvLot: TdmkEdit
                  Left = 8
                  Top = 100
                  Width = 732
                  Height = 21
                  TabOrder = 2
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ValMin = '-2147483647'
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  QryName = 'QrPrmsEmpNFe'
                  UpdCampo = 'DirEnvLot'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = ''
                  ValWarn = False
                end
                object EdDirRec: TdmkEdit
                  Left = 8
                  Top = 140
                  Width = 732
                  Height = 21
                  TabOrder = 3
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ValMin = '-2147483647'
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  QryName = 'QrPrmsEmpNFe'
                  UpdCampo = 'DirRec'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = ''
                  ValWarn = False
                end
                object EdDirProRec: TdmkEdit
                  Left = 8
                  Top = 220
                  Width = 732
                  Height = 21
                  TabOrder = 5
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ValMin = '-2147483647'
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  QryName = 'QrPrmsEmpNFe'
                  UpdCampo = 'DirProRec'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = ''
                  ValWarn = False
                end
                object EdDirNFeAss: TdmkEdit
                  Left = 8
                  Top = 60
                  Width = 732
                  Height = 21
                  TabOrder = 1
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ValMin = '-2147483647'
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  QryName = 'QrPrmsEmpNFe'
                  UpdCampo = 'DirNFeAss'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = ''
                  ValWarn = False
                end
                object EdDirNFeGer: TdmkEdit
                  Left = 8
                  Top = 20
                  Width = 732
                  Height = 21
                  TabOrder = 0
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ValMin = '-2147483647'
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  QryName = 'QrPrmsEmpNFe'
                  UpdCampo = 'DirNFeGer'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = ''
                  ValWarn = False
                end
                object EdDirDen: TdmkEdit
                  Left = 8
                  Top = 260
                  Width = 732
                  Height = 21
                  TabOrder = 6
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ValMin = '-2147483647'
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  QryName = 'QrPrmsEmpNFe'
                  UpdCampo = 'DirDen'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = ''
                  ValWarn = False
                end
                object EdDirPedRec: TdmkEdit
                  Left = 8
                  Top = 180
                  Width = 732
                  Height = 21
                  TabOrder = 4
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ValMin = '-2147483647'
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  QryName = 'QrPrmsEmpNFe'
                  UpdCampo = 'DirPedRec'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = ''
                  ValWarn = False
                end
                object BtNFeDefaultDirs: TBitBtn
                  Left = 8
                  Top = 287
                  Width = 753
                  Height = 40
                  Cursor = crHandPoint
                  Caption = 
                    'Sugest'#227'o de diret'#243'rios para os arquivos XML [Geral], [1], [2] e ' +
                    '[3] (somente na altera'#231#227'o do registro, e somente para n'#227'o defini' +
                    'dos)'
                  Enabled = False
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -12
                  Font.Name = 'MS Sans Serif'
                  Font.Style = []
                  NumGlyphs = 2
                  ParentFont = False
                  ParentShowHint = False
                  ShowHint = True
                  TabOrder = 7
                  OnClick = BtNFeDefaultDirsClick
                end
              end
            end
            object TabSheet14: TTabSheet
              Caption = ' Diret'#243'rios de arquivos XML [2] '
              ImageIndex = 2
              ExplicitLeft = 0
              ExplicitTop = 0
              ExplicitWidth = 0
              ExplicitHeight = 0
              object Panel23: TPanel
                Left = 0
                Top = 0
                Width = 1036
                Height = 435
                Align = alClient
                BevelOuter = bvNone
                ParentBackground = False
                TabOrder = 0
                object Label40: TLabel
                  Left = 8
                  Top = 84
                  Width = 336
                  Height = 13
                  Caption = 
                    'Diret'#243'rio dos pedidos de inutiliza'#231#227'o de n'#250'meros de NF-e (-ped-i' +
                    'nu.xml):'
                end
                object Label49: TLabel
                  Left = 8
                  Top = 124
                  Width = 323
                  Height = 13
                  Caption = 
                    'Diret'#243'rio das respostas de inutiliza'#231#227'o de n'#250'meros de NF-e (-inu' +
                    '.xml):'
                end
                object Label50: TLabel
                  Left = 8
                  Top = 164
                  Width = 346
                  Height = 13
                  Caption = 
                    'Diret'#243'rio dos pedidos de consulta da situa'#231#227'o atual da NF-e (-pe' +
                    'd-sit.xml):'
                end
                object Label51: TLabel
                  Left = 8
                  Top = 204
                  Width = 333
                  Height = 13
                  Caption = 
                    'Diret'#243'rio das respostas de consulta da situa'#231#227'o atual da NF-e (-' +
                    'sit.xml):'
                end
                object Label52: TLabel
                  Left = 8
                  Top = 244
                  Width = 323
                  Height = 13
                  Caption = 
                    'Diret'#243'rio dos pedidos de consulta do status do servi'#231'o (-ped-sta' +
                    '.xml):'
                end
                object SBDirPedSta: TSpeedButton
                  Left = 739
                  Top = 260
                  Width = 21
                  Height = 21
                  Caption = '...'
                  OnClick = SBDirPedStaClick
                end
                object Label53: TLabel
                  Left = 8
                  Top = 284
                  Width = 310
                  Height = 13
                  Caption = 
                    'Diret'#243'rio das respostas de consulta do status do servi'#231'o (-sta.x' +
                    'ml):'
                end
                object SBDirSta: TSpeedButton
                  Left = 739
                  Top = 300
                  Width = 21
                  Height = 21
                  Caption = '...'
                  OnClick = SBDirStaClick
                end
                object Label39: TLabel
                  Left = 8
                  Top = 4
                  Width = 297
                  Height = 13
                  Caption = 'Diret'#243'rio dos pedidos de cancelamento de NF-e (-ped-can.xml):'
                end
                object Label48: TLabel
                  Left = 8
                  Top = 44
                  Width = 284
                  Height = 13
                  Caption = 'Diret'#243'rio das respostas de cancelamento de NF-e (-can.xml):'
                end
                object SBDirPedCan: TSpeedButton
                  Left = 739
                  Top = 20
                  Width = 21
                  Height = 21
                  Caption = '...'
                  OnClick = SBDirPedCanClick
                end
                object SBDirCan: TSpeedButton
                  Left = 739
                  Top = 60
                  Width = 21
                  Height = 21
                  Caption = '...'
                  OnClick = SBDirCanClick
                end
                object SBDirPedInu: TSpeedButton
                  Left = 739
                  Top = 100
                  Width = 21
                  Height = 21
                  Caption = '...'
                  OnClick = SBDirPedInuClick
                end
                object SBDirInu: TSpeedButton
                  Left = 739
                  Top = 140
                  Width = 21
                  Height = 21
                  Caption = '...'
                  OnClick = SBDirInuClick
                end
                object SBDirPedSit: TSpeedButton
                  Left = 739
                  Top = 180
                  Width = 21
                  Height = 21
                  Caption = '...'
                  OnClick = SBDirPedSitClick
                end
                object SBDirSit: TSpeedButton
                  Left = 739
                  Top = 220
                  Width = 21
                  Height = 21
                  Caption = '...'
                  OnClick = SBDirSitClick
                end
                object EdDirPedSta: TdmkEdit
                  Left = 8
                  Top = 260
                  Width = 732
                  Height = 21
                  TabOrder = 6
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ValMin = '-2147483647'
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  QryName = 'QrPrmsEmpNFe'
                  UpdCampo = 'DirPedSta'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = ''
                  ValWarn = False
                end
                object EdDirSta: TdmkEdit
                  Left = 8
                  Top = 300
                  Width = 732
                  Height = 21
                  TabOrder = 7
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ValMin = '-2147483647'
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  QryName = 'QrPrmsEmpNFe'
                  UpdCampo = 'DirSta'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = ''
                  ValWarn = False
                end
                object EdDirPedCan: TdmkEdit
                  Left = 8
                  Top = 20
                  Width = 732
                  Height = 21
                  TabOrder = 0
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ValMin = '-2147483647'
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  QryName = 'QrPrmsEmpNFe'
                  UpdCampo = 'DirPedCan'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = ''
                  ValWarn = False
                end
                object EdDirCan: TdmkEdit
                  Left = 8
                  Top = 60
                  Width = 732
                  Height = 21
                  TabOrder = 1
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ValMin = '-2147483647'
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  QryName = 'QrPrmsEmpNFe'
                  UpdCampo = 'DirCan'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = ''
                  ValWarn = False
                end
                object EdDirPedInu: TdmkEdit
                  Left = 8
                  Top = 100
                  Width = 732
                  Height = 21
                  TabOrder = 2
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ValMin = '-2147483647'
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  QryName = 'QrPrmsEmpNFe'
                  UpdCampo = 'DirPedInu'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = ''
                  ValWarn = False
                end
                object EdDirInu: TdmkEdit
                  Left = 8
                  Top = 140
                  Width = 732
                  Height = 21
                  TabOrder = 3
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ValMin = '-2147483647'
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  QryName = 'QrPrmsEmpNFe'
                  UpdCampo = 'DirInu'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = ''
                  ValWarn = False
                end
                object EdDirPedSit: TdmkEdit
                  Left = 8
                  Top = 180
                  Width = 732
                  Height = 21
                  TabOrder = 4
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ValMin = '-2147483647'
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  QryName = 'QrPrmsEmpNFe'
                  UpdCampo = 'DirPedSit'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = ''
                  ValWarn = False
                end
                object EdDirSit: TdmkEdit
                  Left = 8
                  Top = 220
                  Width = 732
                  Height = 21
                  TabOrder = 5
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ValMin = '-2147483647'
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  QryName = 'QrPrmsEmpNFe'
                  UpdCampo = 'DirSit'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = ''
                  ValWarn = False
                end
              end
            end
            object TabSheet27: TTabSheet
              Caption = ' Diret'#243'rios de arquivos XML [3] '
              ImageIndex = 3
              ExplicitLeft = 0
              ExplicitTop = 0
              ExplicitWidth = 0
              ExplicitHeight = 0
              object Panel46: TPanel
                Left = 0
                Top = 0
                Width = 1036
                Height = 435
                Align = alClient
                BevelOuter = bvNone
                ParentBackground = False
                TabOrder = 0
                object Label144: TLabel
                  Left = 8
                  Top = 124
                  Width = 324
                  Height = 13
                  Caption = 
                    'Diret'#243'rio das respostas das cartas de corre'#231#227'o eletr'#244'nicas (-cce' +
                    '.xml):'
                end
                object Label145: TLabel
                  Left = 8
                  Top = 204
                  Width = 403
                  Height = 13
                  Caption = 
                    'Diret'#243'rio dos pedidos de cancelamento de NF-e como evento da NF-' +
                    'e (-eve-can.xml):'
                end
                object Label148: TLabel
                  Left = 8
                  Top = 244
                  Width = 390
                  Height = 13
                  Caption = 
                    'Diret'#243'rio das respostas de cancelamento de NF-e como evento da N' +
                    'F-e (-can.xml):'
                end
                object Label149: TLabel
                  Left = 8
                  Top = 4
                  Width = 355
                  Height = 13
                  Caption = 
                    'Diret'#243'rio do envio dos lotes de envio de eventos da NF-e (-eve-e' +
                    'nv-lot.xml):'
                end
                object Label150: TLabel
                  Left = 8
                  Top = 84
                  Width = 337
                  Height = 13
                  Caption = 
                    'Diret'#243'rio dos pedidos das cartas de corre'#231#227'o eletr'#244'nicas (-eve-c' +
                    'ce.xml):'
                end
                object Label151: TLabel
                  Left = 8
                  Top = 44
                  Width = 356
                  Height = 13
                  Caption = 
                    'Diret'#243'rio do retorno dos lotes de envio de eventos da NF-e (-eve' +
                    '-ret-lot.xml):'
                end
                object SbDirEveEnvLot: TSpeedButton
                  Left = 739
                  Top = 20
                  Width = 21
                  Height = 21
                  Caption = '...'
                  OnClick = SbDirEveEnvLotClick
                end
                object SbDirEveRetLot: TSpeedButton
                  Left = 739
                  Top = 60
                  Width = 21
                  Height = 21
                  Caption = '...'
                  OnClick = SbDirEveRetLotClick
                end
                object SbDirEvePedCCe: TSpeedButton
                  Left = 739
                  Top = 100
                  Width = 21
                  Height = 21
                  Caption = '...'
                  OnClick = SbDirEvePedCCeClick
                end
                object SbDirEveCCe: TSpeedButton
                  Left = 739
                  Top = 140
                  Width = 21
                  Height = 21
                  Caption = '...'
                  OnClick = SbDirEveCCeClick
                end
                object SbDirEvePedCan: TSpeedButton
                  Left = 739
                  Top = 220
                  Width = 21
                  Height = 21
                  Caption = '...'
                  OnClick = SbDirEvePedCanClick
                end
                object SbDirEveRetCan: TSpeedButton
                  Left = 739
                  Top = 260
                  Width = 21
                  Height = 21
                  Caption = '...'
                  OnClick = SbDirEveRetCanClick
                end
                object Label154: TLabel
                  Left = 8
                  Top = 164
                  Width = 435
                  Height = 13
                  Caption = 
                    'Diret'#243'rio de armazenamento e disposi'#231#227'o das cartas de corre'#231#227'o e' +
                    'letr'#244'nicas (-proc-cce.xml):'
                end
                object SbDirProcCCe: TSpeedButton
                  Left = 739
                  Top = 180
                  Width = 21
                  Height = 21
                  Caption = '...'
                  OnClick = SbDirProcCCeClick
                end
                object EdDirEveEnvLot: TdmkEdit
                  Left = 8
                  Top = 20
                  Width = 732
                  Height = 21
                  TabOrder = 0
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ValMin = '-2147483647'
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  QryName = 'QrPrmsEmpNFe'
                  UpdCampo = 'DirEveEnvLot'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = ''
                  ValWarn = False
                end
                object EdDirEveRetLot: TdmkEdit
                  Left = 8
                  Top = 60
                  Width = 732
                  Height = 21
                  TabOrder = 1
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ValMin = '-2147483647'
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  QryName = 'QrPrmsEmpNFe'
                  UpdCampo = 'DirEveRetLot'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = ''
                  ValWarn = False
                end
                object EdDirEvePedCCe: TdmkEdit
                  Left = 8
                  Top = 100
                  Width = 732
                  Height = 21
                  TabOrder = 2
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ValMin = '-2147483647'
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  QryName = 'QrPrmsEmpNFe'
                  UpdCampo = 'DirEvePedCCe'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = ''
                  ValWarn = False
                end
                object EdDirEveRetCCe: TdmkEdit
                  Left = 8
                  Top = 140
                  Width = 732
                  Height = 21
                  TabOrder = 3
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ValMin = '-2147483647'
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  QryName = 'QrPrmsEmpNFe'
                  UpdCampo = 'DirEveRetCCe'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = ''
                  ValWarn = False
                end
                object EdDirEvePedCan: TdmkEdit
                  Left = 8
                  Top = 220
                  Width = 732
                  Height = 21
                  TabOrder = 5
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ValMin = '-2147483647'
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  QryName = 'QrPrmsEmpNFe'
                  UpdCampo = 'DirEvePedCan'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = ''
                  ValWarn = False
                end
                object EdDirEveRetCan: TdmkEdit
                  Left = 8
                  Top = 260
                  Width = 732
                  Height = 21
                  TabOrder = 6
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ValMin = '-2147483647'
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  QryName = 'QrPrmsEmpNFe'
                  UpdCampo = 'DirEveRetCan'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = ''
                  ValWarn = False
                end
                object EdDirEveProcCCe: TdmkEdit
                  Left = 8
                  Top = 180
                  Width = 732
                  Height = 21
                  TabOrder = 4
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ValMin = '-2147483647'
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  QryName = 'QrPrmsEmpNFe'
                  UpdCampo = 'DirEveProcCCe'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = ''
                  ValWarn = False
                end
              end
            end
            object TabSheet37: TTabSheet
              Caption = ' Diret'#243'rios de arquivos XML [4] '
              ImageIndex = 4
              ExplicitLeft = 0
              ExplicitTop = 0
              ExplicitWidth = 0
              ExplicitHeight = 0
              object Panel55: TPanel
                Left = 0
                Top = 0
                Width = 1036
                Height = 435
                Align = alClient
                BevelOuter = bvNone
                ParentBackground = False
                TabOrder = 0
                object Label233: TLabel
                  Left = 8
                  Top = 4
                  Width = 348
                  Height = 13
                  Caption = 
                    'Diret'#243'rio das respostas de consulta de NF-es destinadas (-ret-nf' +
                    'e-des.xml):'
                end
                object SBDirRetNfeDes: TSpeedButton
                  Left = 739
                  Top = 20
                  Width = 21
                  Height = 21
                  Caption = '...'
                  OnClick = SBDirRetNfeDesClick
                end
                object Label236: TLabel
                  Left = 8
                  Top = 44
                  Width = 325
                  Height = 13
                  Caption = 
                    'Diret'#243'rio dos envios de manifesta'#231'oes do destinat'#225'rip (-eve-mde.' +
                    'xml):'
                end
                object Label237: TLabel
                  Left = 8
                  Top = 84
                  Width = 318
                  Height = 13
                  Caption = 
                    'Diret'#243'rio das respostas de manifesta'#231#245'es do destinat'#225'rio (-mde.x' +
                    'ml):'
                end
                object SbDirEvePedMDe: TSpeedButton
                  Left = 739
                  Top = 60
                  Width = 21
                  Height = 21
                  Caption = '...'
                  OnClick = SbDirEvePedMDeClick
                end
                object SbDirEveRetMDe: TSpeedButton
                  Left = 739
                  Top = 100
                  Width = 21
                  Height = 21
                  Caption = '...'
                  OnClick = SbDirEveRetMDeClick
                end
                object Label244: TLabel
                  Left = 8
                  Top = 124
                  Width = 286
                  Height = 13
                  Caption = 'Diret'#243'rio dos grupos de NF-e baixadas (-ret-dow-nfe-des.xml):'
                end
                object SbDirDowNFeDes: TSpeedButton
                  Left = 739
                  Top = 140
                  Width = 21
                  Height = 21
                  Caption = '...'
                  OnClick = SbDirDowNFeDesClick
                end
                object Label246: TLabel
                  Left = 8
                  Top = 164
                  Width = 178
                  Height = 13
                  Caption = 'Diret'#243'rio das NF-e baixadas (-nfe.xml):'
                end
                object SbDirDowNFeNFe: TSpeedButton
                  Left = 739
                  Top = 180
                  Width = 21
                  Height = 21
                  Caption = '...'
                  OnClick = SbDirDowNFeNFeClick
                end
                object Label271: TLabel
                  Left = 8
                  Top = 204
                  Width = 364
                  Height = 13
                  Caption = 
                    'Diret'#243'rio dos envios de Distribui'#231#227'o de DFe de Interesse (-ped-d' +
                    'fe-dis-int.xml):'
                end
                object Label272: TLabel
                  Left = 8
                  Top = 244
                  Width = 365
                  Height = 13
                  Caption = 
                    'Diret'#243'rio dos retornos de Distribui'#231#227'o de DFe de Interesse (-ret' +
                    '-dfe-dis-int.xml):'
                end
                object SbDirDistDFeInt: TSpeedButton
                  Left = 739
                  Top = 220
                  Width = 21
                  Height = 21
                  Caption = '...'
                  OnClick = SbDirDistDFeIntClick
                end
                object SbDirRetDistDFeInt: TSpeedButton
                  Left = 739
                  Top = 260
                  Width = 21
                  Height = 21
                  Caption = '...'
                  OnClick = SbDirRetDistDFeIntClick
                end
                object Label275: TLabel
                  Left = 8
                  Top = 284
                  Width = 379
                  Height = 13
                  Caption = 
                    'Diret'#243'rio dos retornos de Download de NF-es confirmadas  (-ret-d' +
                    'ow-nfe-cnf.xml):'
                end
                object SbDirDowNFeCnf: TSpeedButton
                  Left = 739
                  Top = 300
                  Width = 21
                  Height = 21
                  Caption = '...'
                  OnClick = SbDirDowNFeCnfClick
                end
                object EdDirRetNfeDes: TdmkEdit
                  Left = 8
                  Top = 20
                  Width = 732
                  Height = 21
                  TabOrder = 0
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ValMin = '-2147483647'
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  QryName = 'QrPrmsEmpNFe'
                  UpdCampo = 'DirRetNfeDes'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = ''
                  ValWarn = False
                end
                object EdDirEvePedMDe: TdmkEdit
                  Left = 8
                  Top = 60
                  Width = 732
                  Height = 21
                  TabOrder = 1
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ValMin = '-2147483647'
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  QryName = 'QrPrmsEmpNFe'
                  UpdCampo = 'DirEvePedMDe'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = ''
                  ValWarn = False
                end
                object EdDirEveRetMDe: TdmkEdit
                  Left = 8
                  Top = 100
                  Width = 732
                  Height = 21
                  TabOrder = 2
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ValMin = '-2147483647'
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  QryName = 'QrPrmsEmpNFe'
                  UpdCampo = 'DirEveRetMDe'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = ''
                  ValWarn = False
                end
                object EdDirDowNFeDes: TdmkEdit
                  Left = 8
                  Top = 140
                  Width = 732
                  Height = 21
                  TabOrder = 3
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ValMin = '-2147483647'
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  QryName = 'QrPrmsEmpNFe'
                  UpdCampo = 'DirDowNFeDes'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = ''
                  ValWarn = False
                end
                object EdDirDowNFeNFe: TdmkEdit
                  Left = 8
                  Top = 180
                  Width = 732
                  Height = 21
                  TabOrder = 4
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ValMin = '-2147483647'
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  QryName = 'QrPrmsEmpNFe'
                  UpdCampo = 'DirDowNFeNFe'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = ''
                  ValWarn = False
                end
                object EdDirDistDFeInt: TdmkEdit
                  Left = 8
                  Top = 220
                  Width = 732
                  Height = 21
                  TabOrder = 5
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ValMin = '-2147483647'
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  QryName = 'QrPrmsEmpNFe'
                  UpdCampo = 'DirDistDFeInt'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = ''
                  ValWarn = False
                end
                object EdDirRetDistDFeInt: TdmkEdit
                  Left = 8
                  Top = 260
                  Width = 732
                  Height = 21
                  TabOrder = 6
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ValMin = '-2147483647'
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  QryName = 'QrPrmsEmpNFe'
                  UpdCampo = 'DirRetDistDFeInt'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = ''
                  ValWarn = False
                end
                object EdDirDowNFeCnf: TdmkEdit
                  Left = 8
                  Top = 300
                  Width = 732
                  Height = 21
                  TabOrder = 7
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ValMin = '-2147483647'
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  QryName = 'QrPrmsEmpNFe'
                  UpdCampo = 'DirDowNFeCnf'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = ''
                  ValWarn = False
                end
              end
            end
            object TabSheet21: TTabSheet
              Caption = ' Op'#231#245'es '
              ImageIndex = 4
              ExplicitLeft = 0
              ExplicitTop = 0
              ExplicitWidth = 0
              ExplicitHeight = 0
              object Panel35: TPanel
                Left = 0
                Top = 0
                Width = 1036
                Height = 435
                Align = alClient
                BevelOuter = bvNone
                ParentBackground = False
                TabOrder = 0
                object GroupBox15: TGroupBox
                  Left = 0
                  Top = 0
                  Width = 484
                  Height = 435
                  Align = alLeft
                  Caption = ' Op'#231#245'es at'#233' 1.10: '
                  TabOrder = 0
                  object GroupBox13: TGroupBox
                    Left = 2
                    Top = 15
                    Width = 480
                    Height = 138
                    Align = alTop
                    Caption = ' Dados e identifica'#231#227'o da NF-e: '
                    TabOrder = 0
                    object Label100: TLabel
                      Left = 12
                      Top = 16
                      Width = 57
                      Height = 13
                      Caption = 'Vers'#227'o: [F4]'
                    end
                    object Label101: TLabel
                      Left = 75
                      Top = 16
                      Width = 38
                      Height = 13
                      Caption = 'Modelo:'
                    end
                    object SpeedButton16: TSpeedButton
                      Left = 451
                      Top = 32
                      Width = 21
                      Height = 21
                      Caption = '...'
                      OnClick = SpeedButton16Click
                    end
                    object Label110: TLabel
                      Left = 120
                      Top = 16
                      Width = 214
                      Height = 13
                      Caption = 'Caminho dos arquivos do Schema xml (*.xsd):'
                    end
                    object EdVersao: TdmkEdit
                      Left = 12
                      Top = 32
                      Width = 57
                      Height = 21
                      Alignment = taRightJustify
                      TabOrder = 0
                      FormatType = dmktfDouble
                      MskType = fmtNone
                      DecimalSize = 2
                      LeftZeros = 0
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      Texto = '1,10'
                      QryName = 'QrPrmEmpNFe'
                      UpdCampo = 'versao'
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = 1.100000000000000000
                      ValWarn = False
                      OnKeyDown = EdVersaoKeyDown
                    end
                    object Edide_mod: TdmkEdit
                      Left = 75
                      Top = 32
                      Width = 41
                      Height = 21
                      Alignment = taRightJustify
                      TabOrder = 1
                      FormatType = dmktfInteger
                      MskType = fmtNone
                      DecimalSize = 2
                      LeftZeros = 0
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      Texto = '55'
                      QryName = 'QrPrmEmpNFe'
                      UpdCampo = 'ide_mod'
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = 55
                      ValWarn = False
                    end
                    object RGide_tpAmb: TdmkRadioGroup
                      Left = 12
                      Top = 84
                      Width = 461
                      Height = 45
                      Caption = ' Identifica'#231#227'o do Ambiente: '
                      Columns = 3
                      ItemIndex = 0
                      Items.Strings = (
                        '0 - Nenhum'
                        '1 - Produ'#231#227'o'
                        '2 - Homologa'#231#227'o')
                      TabOrder = 3
                      QryName = 'QrPrmEmpNFe'
                      UpdCampo = 'ide_tpAmb'
                      UpdType = utYes
                      OldValor = 0
                    end
                    object EdDirSchema: TdmkEdit
                      Left = 120
                      Top = 32
                      Width = 328
                      Height = 21
                      TabOrder = 2
                      FormatType = dmktfString
                      MskType = fmtNone
                      DecimalSize = 0
                      LeftZeros = 0
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ValMin = '-2147483647'
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      QryName = 'QrPrmEmpNFe'
                      UpdCampo = 'DirSchema'
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = ''
                      ValWarn = False
                    end
                    object CkNT2018_05v120: TdmkCheckBox
                      Left = 12
                      Top = 60
                      Width = 197
                      Height = 17
                      Caption = 'Nota t'#233'cnica 2018/005 vers'#227'o 1.20'
                      TabOrder = 4
                      QryName = 'QrPrmEmpNFe'
                      UpdCampo = 'NT2018_05v120'
                      UpdType = utYes
                      ValCheck = '1'
                      ValUncheck = '0'
                      OldValor = #0
                    end
                  end
                  object RGAppCode: TdmkRadioGroup
                    Left = 2
                    Top = 153
                    Width = 480
                    Height = 44
                    Align = alTop
                    Caption = ' Gerenciamento NF-e: '
                    Columns = 2
                    ItemIndex = 0
                    Items.Strings = (
                      'Pascal'
                      'C++')
                    TabOrder = 1
                    QryName = 'QrPrmEmpNFe'
                    UpdCampo = 'AppCode'
                    UpdType = utYes
                    OldValor = 0
                  end
                  object RGAssDigMode: TdmkRadioGroup
                    Left = 2
                    Top = 197
                    Width = 480
                    Height = 44
                    Align = alTop
                    Caption = ' Modo de assinar a NF-e no Pascal: '
                    Columns = 2
                    ItemIndex = 0
                    Items.Strings = (
                      'CAPICOM.DLL'
                      '.NET FRAMEWORK')
                    TabOrder = 2
                    QryName = 'QrPrmEmpNFe'
                    UpdCampo = 'AssDigMode'
                    UpdType = utYes
                    OldValor = 0
                  end
                  object Panel105: TPanel
                    Left = 2
                    Top = 241
                    Width = 480
                    Height = 192
                    Align = alClient
                    BevelOuter = bvNone
                    TabOrder = 3
                    object Label98: TLabel
                      Left = 4
                      Top = 4
                      Width = 255
                      Height = 13
                      Caption = 'Tipo de email para envio de NFe e Carta de Corre'#231#227'o:'
                    end
                    object SpeedButton15: TSpeedButton
                      Left = 448
                      Top = 20
                      Width = 21
                      Height = 21
                      Caption = '...'
                      OnClick = SpeedButton15Click
                    end
                    object Label152: TLabel
                      Left = 4
                      Top = 44
                      Width = 386
                      Height = 13
                      Caption = 
                        'Tipo de email para envio espec'#237'fico de Carta de Corre'#231#227'o:  (Ex.:' +
                        ' Transportadoras)'
                    end
                    object SpeedButton19: TSpeedButton
                      Left = 448
                      Top = 60
                      Width = 21
                      Height = 21
                      Caption = '...'
                      OnClick = SpeedButton19Click
                    end
                    object Label99: TLabel
                      Left = 4
                      Top = 88
                      Width = 461
                      Height = 13
                      Caption = 
                        'Email pr'#243'prio para receber email enviado (mais de um separar por' +
                        ' ponte e v'#237'rgula) m'#225'x. 255 letras:'
                    end
                    object Label251: TLabel
                      Left = 2
                      Top = 130
                      Width = 66
                      Height = 13
                      Caption = 'Texto padr'#227'o:'
                    end
                    object SpeedButton20: TSpeedButton
                      Left = 449
                      Top = 147
                      Width = 21
                      Height = 21
                      Caption = '...'
                      OnClick = SpeedButton20Click
                    end
                    object EdEntiTipCto: TdmkEditCB
                      Left = 4
                      Top = 20
                      Width = 56
                      Height = 21
                      Alignment = taRightJustify
                      TabOrder = 0
                      FormatType = dmktfInteger
                      MskType = fmtNone
                      DecimalSize = 0
                      LeftZeros = 0
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ValMin = '-2147483647'
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      Texto = '0'
                      QryName = 'QrPrmEmpNFe'
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = 0
                      ValWarn = False
                      DBLookupComboBox = CBEntiTipCto
                      IgnoraDBLookupComboBox = False
                      AutoSetIfOnlyOneReg = setregOnlyManual
                    end
                    object CBEntiTipCto: TdmkDBLookupComboBox
                      Left = 64
                      Top = 20
                      Width = 383
                      Height = 21
                      KeyField = 'CodUsu'
                      ListField = 'Nome'
                      ListSource = DsEntiTipCto
                      TabOrder = 1
                      dmkEditCB = EdEntiTipCto
                      QryName = 'QrPrmEmpNFe'
                      UpdType = utYes
                      LocF7SQLMasc = '$#'
                    end
                    object EdEntiTipCt1: TdmkEditCB
                      Left = 4
                      Top = 60
                      Width = 56
                      Height = 21
                      Alignment = taRightJustify
                      TabOrder = 2
                      FormatType = dmktfInteger
                      MskType = fmtNone
                      DecimalSize = 0
                      LeftZeros = 0
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ValMin = '-2147483647'
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      Texto = '0'
                      QryName = 'QrPrmEmpNFe'
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = 0
                      ValWarn = False
                      DBLookupComboBox = CBEntiTipCt1
                      IgnoraDBLookupComboBox = False
                      AutoSetIfOnlyOneReg = setregOnlyManual
                    end
                    object CBEntiTipCt1: TdmkDBLookupComboBox
                      Left = 64
                      Top = 60
                      Width = 383
                      Height = 21
                      KeyField = 'CodUsu'
                      ListField = 'Nome'
                      ListSource = DsEntiTipCt1
                      TabOrder = 3
                      dmkEditCB = EdEntiTipCt1
                      QryName = 'QrPrmEmpNFe'
                      UpdType = utYes
                      LocF7SQLMasc = '$#'
                    end
                    object EdMyEmailNFe: TdmkEdit
                      Left = 4
                      Top = 104
                      Width = 465
                      Height = 21
                      CharCase = ecLowerCase
                      MaxLength = 255
                      TabOrder = 4
                      FormatType = dmktfString
                      MskType = fmtNone
                      DecimalSize = 0
                      LeftZeros = 0
                      NoEnterToTab = False
                      NoForceUppercase = True
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      QryName = 'QrPrmEmpNFe'
                      UpdCampo = 'MyEmailNFe'
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = ''
                      ValWarn = False
                    end
                    object EdNFeInfCpl: TdmkEditCB
                      Left = 4
                      Top = 147
                      Width = 56
                      Height = 21
                      Alignment = taRightJustify
                      TabOrder = 5
                      FormatType = dmktfInteger
                      MskType = fmtNone
                      DecimalSize = 0
                      LeftZeros = 0
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ValMin = '-2147483647'
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      Texto = '0'
                      QryName = 'QrPrmEmpNFe'
                      UpdCampo = 'NFeInfCpl'
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = 0
                      ValWarn = False
                      DBLookupComboBox = CBNFeInfCpl
                      IgnoraDBLookupComboBox = False
                      AutoSetIfOnlyOneReg = setregOnlyManual
                    end
                    object CBNFeInfCpl: TdmkDBLookupComboBox
                      Left = 64
                      Top = 147
                      Width = 383
                      Height = 21
                      KeyField = 'Codigo'
                      ListField = 'Nome'
                      ListSource = DsNFeInfCpl
                      TabOrder = 6
                      dmkEditCB = EdNFeInfCpl
                      QryName = 'QrPrmEmpNFe'
                      UpdType = utYes
                      LocF7SQLMasc = '$#'
                    end
                  end
                end
                object GroupBox16: TGroupBox
                  Left = 724
                  Top = 0
                  Width = 312
                  Height = 435
                  Align = alClient
                  Caption = 'Op'#231#245'es at'#233' 2.00 (Simples Nacional): '
                  TabOrder = 1
                  object Label106: TLabel
                    Left = 8
                    Top = 156
                    Width = 105
                    Height = 13
                    Caption = 'CSOSN* para CRT=1:'
                  end
                  object Label107: TLabel
                    Left = 4
                    Top = 176
                    Width = 264
                    Height = 13
                    Caption = '*C'#243'digo de Situa'#231#227'o da Opera'#231#227'o no Simples Nacional.'
                  end
                  object RGCRT: TdmkRadioGroup
                    Left = 2
                    Top = 15
                    Width = 308
                    Height = 134
                    Align = alTop
                    Caption = ' CRT (C'#243'digo do Regime tribut'#225'rio): '
                    Items.Strings = (
                      '0 - Nenhum'
                      '1 - Simples Nacional'
                      '2 - Simples Nacional - excesso de sublimite de receita bruta'
                      '3 - Regime Normal')
                    TabOrder = 0
                    OnClick = RGCRTClick
                    QryName = 'QrPrmEmpNFe'
                    UpdCampo = 'CRT'
                    UpdType = utYes
                    OldValor = 0
                  end
                  object EdCSOSN: TdmkEdit
                    Left = 196
                    Top = 152
                    Width = 77
                    Height = 21
                    Alignment = taRightJustify
                    TabOrder = 1
                    FormatType = dmktfInteger
                    MskType = fmtNone
                    DecimalSize = 0
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '0'
                    QryName = 'QrPrmEmpNFe'
                    UpdCampo = 'CSOSN'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 0
                    ValWarn = False
                    OnChange = EdCSOSNChange
                    OnKeyDown = EdCSOSNKeyDown
                  end
                  object MeCSOSN_Edit: TMemo
                    Left = 8
                    Top = 192
                    Width = 265
                    Height = 45
                    TabStop = False
                    ReadOnly = True
                    TabOrder = 2
                  end
                  object GroupBox19: TGroupBox
                    Left = 2
                    Top = 324
                    Width = 308
                    Height = 109
                    Align = alBottom
                    Caption = ' CSOSN = 101 ou 201: '
                    TabOrder = 3
                    object Label113: TLabel
                      Left = 12
                      Top = 16
                      Width = 140
                      Height = 26
                      Caption = 'Al'#237'quota aplic'#225'vel de c'#225'lculo'#13#10'do cr'#233'dito (Simples Nacional):'
                    end
                    object Label115: TLabel
                      Left = 12
                      Top = 56
                      Width = 107
                      Height = 13
                      Caption = 'AAAAMM de validade:'
                    end
                    object EdpCredSNAlq: TdmkEdit
                      Left = 160
                      Top = 20
                      Width = 77
                      Height = 21
                      Alignment = taRightJustify
                      TabOrder = 0
                      FormatType = dmktfDouble
                      MskType = fmtNone
                      DecimalSize = 2
                      LeftZeros = 0
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      Texto = '0,00'
                      QryName = 'QrPrmEmpNFe'
                      UpdCampo = 'pCredSNAlq'
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = 0.000000000000000000
                      ValWarn = False
                    end
                    object EdpCredSNMez: TdmkEdit
                      Left = 160
                      Top = 52
                      Width = 77
                      Height = 21
                      Alignment = taCenter
                      TabOrder = 1
                      FormatType = dmktf_AAAAMM
                      MskType = fmtNone
                      DecimalSize = 2
                      LeftZeros = 6
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      QryName = 'QrPrmEmpNFe'
                      UpdCampo = 'pCredSNMez'
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = Null
                      ValWarn = False
                    end
                  end
                end
                object GroupBox37: TGroupBox
                  Left = 484
                  Top = 0
                  Width = 240
                  Height = 435
                  Align = alLeft
                  Caption = 'Vers'#227'o do servi'#231'o'
                  TabOrder = 2
                  object Label218: TLabel
                    Left = 5
                    Top = 17
                    Width = 106
                    Height = 13
                    Caption = 'Status do servi'#231'o: [F4]'
                  end
                  object Label226: TLabel
                    Left = 5
                    Top = 40
                    Width = 188
                    Height = 13
                    Caption = 'Enviar lote de NF-e assincrono ao fisco:'
                  end
                  object Label227: TLabel
                    Left = 5
                    Top = 88
                    Width = 129
                    Height = 13
                    Caption = 'Consultar lote enviado: [F4]'
                  end
                  object Label228: TLabel
                    Left = 5
                    Top = 112
                    Width = 159
                    Height = 13
                    Caption = 'Pedir cancelamento de NF-e: [F4]'
                  end
                  object Label229: TLabel
                    Left = 5
                    Top = 136
                    Width = 187
                    Height = 13
                    Caption = 'Pedir inutiliza'#231#227'o de n'#250'mero(s) de NF-e:'
                  end
                  object Label230: TLabel
                    Left = 5
                    Top = 160
                    Width = 94
                    Height = 13
                    Caption = 'Consultar NF-e: [F4]'
                  end
                  object Label231: TLabel
                    Left = 5
                    Top = 184
                    Width = 168
                    Height = 13
                    Caption = 'Enviar lote de eventos da NFe: [F4]'
                  end
                  object Label257: TLabel
                    Left = 5
                    Top = 64
                    Width = 177
                    Height = 13
                    Caption = 'Enviar lote de NF-e sincrono ao fisco:'
                    Enabled = False
                  end
                  object Label265: TLabel
                    Left = 5
                    Top = 208
                    Width = 186
                    Height = 13
                    Caption = 'Consultar cadastro de Contribuinte: [F4]'
                  end
                  object Label267: TLabel
                    Left = 5
                    Top = 232
                    Width = 120
                    Height = 13
                    Caption = 'Distribui'#231#227'o de DF-e: [F4]'
                  end
                  object Label273: TLabel
                    Left = 5
                    Top = 256
                    Width = 178
                    Height = 13
                    Caption = 'Download de NF-es confirmadas: [F4]'
                  end
                  object EdNFeVerStaSer: TdmkEdit
                    Left = 193
                    Top = 12
                    Width = 40
                    Height = 21
                    Alignment = taRightJustify
                    TabOrder = 0
                    FormatType = dmktfDouble
                    MskType = fmtNone
                    DecimalSize = 2
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '1,00'
                    QryName = 'QrPrmEmpNFe'
                    UpdCampo = 'NFeVerStaSer'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 1.000000000000000000
                    ValWarn = False
                    OnKeyUp = EdNFeVerStaSerKeyUp
                  end
                  object EdNFeVerEnvLot: TdmkEdit
                    Left = 193
                    Top = 36
                    Width = 40
                    Height = 21
                    Alignment = taRightJustify
                    TabOrder = 1
                    FormatType = dmktfDouble
                    MskType = fmtNone
                    DecimalSize = 2
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '1,10'
                    QryName = 'QrPrmEmpNFe'
                    UpdCampo = 'NFeVerEnvLot'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 1.100000000000000000
                    ValWarn = False
                    OnKeyDown = EdNFeVerEnvLotKeyDown
                  end
                  object EdNFeVerConLot: TdmkEdit
                    Left = 193
                    Top = 84
                    Width = 40
                    Height = 21
                    Alignment = taRightJustify
                    TabOrder = 3
                    FormatType = dmktfDouble
                    MskType = fmtNone
                    DecimalSize = 2
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '1,10'
                    QryName = 'QrPrmEmpNFe'
                    UpdCampo = 'NFeVerConLot'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 1.100000000000000000
                    ValWarn = False
                    OnKeyDown = EdNFeVerConLotKeyDown
                  end
                  object EdNFeVerCanNFe: TdmkEdit
                    Left = 193
                    Top = 108
                    Width = 40
                    Height = 21
                    Alignment = taRightJustify
                    TabOrder = 4
                    FormatType = dmktfDouble
                    MskType = fmtNone
                    DecimalSize = 2
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '1,10'
                    QryName = 'QrPrmEmpNFe'
                    UpdCampo = 'NFeVerCanNFe'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 1.100000000000000000
                    ValWarn = False
                    OnKeyDown = EdNFeVerCanNFeKeyDown
                  end
                  object EdNFeVerInuNum: TdmkEdit
                    Left = 193
                    Top = 132
                    Width = 40
                    Height = 21
                    Alignment = taRightJustify
                    TabOrder = 5
                    FormatType = dmktfDouble
                    MskType = fmtNone
                    DecimalSize = 2
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '1,10'
                    QryName = 'QrPrmEmpNFe'
                    UpdCampo = 'NFeVerInuNum'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 1.100000000000000000
                    ValWarn = False
                    OnKeyDown = EdNFeVerInuNumKeyDown
                  end
                  object EdNFeVerConNFe: TdmkEdit
                    Left = 193
                    Top = 156
                    Width = 40
                    Height = 21
                    Alignment = taRightJustify
                    TabOrder = 6
                    FormatType = dmktfDouble
                    MskType = fmtNone
                    DecimalSize = 2
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '1,10'
                    QryName = 'QrPrmEmpNFe'
                    UpdCampo = 'NFeVerConNFe'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 1.100000000000000000
                    ValWarn = False
                    OnKeyDown = EdNFeVerConNFeKeyDown
                  end
                  object EdNFeVerLotEve: TdmkEdit
                    Left = 193
                    Top = 180
                    Width = 40
                    Height = 21
                    Alignment = taRightJustify
                    TabOrder = 7
                    FormatType = dmktfDouble
                    MskType = fmtNone
                    DecimalSize = 2
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '1,10'
                    QryName = 'QrPrmEmpNFe'
                    UpdCampo = 'NFeVerLotEve'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 1.100000000000000000
                    ValWarn = False
                    OnKeyDown = EdNFeVerLotEveKeyDown
                  end
                  object EdNFeVerEnvLotSinc: TdmkEdit
                    Left = 193
                    Top = 60
                    Width = 40
                    Height = 21
                    Alignment = taRightJustify
                    Enabled = False
                    TabOrder = 2
                    FormatType = dmktfDouble
                    MskType = fmtNone
                    DecimalSize = 2
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '0,00'
                    QryName = 'QrPrmEmpNFe'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 0.000000000000000000
                    ValWarn = False
                  end
                  object EdNFeVerConsCad: TdmkEdit
                    Left = 193
                    Top = 204
                    Width = 40
                    Height = 21
                    Alignment = taRightJustify
                    TabOrder = 8
                    FormatType = dmktfDouble
                    MskType = fmtNone
                    DecimalSize = 2
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '2,00'
                    QryName = 'QrPrmEmpNFe'
                    UpdCampo = 'NFeVerConsCad'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 2.000000000000000000
                    ValWarn = False
                    OnKeyDown = EdNFeVerConsCadKeyDown
                  end
                  object EdNFeVerDistDFeInt: TdmkEdit
                    Left = 193
                    Top = 228
                    Width = 40
                    Height = 21
                    Alignment = taRightJustify
                    TabOrder = 9
                    FormatType = dmktfDouble
                    MskType = fmtNone
                    DecimalSize = 2
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '1,00'
                    QryName = 'QrPrmEmpNFe'
                    UpdCampo = 'NFeVerDistDFeInt'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 1.000000000000000000
                    ValWarn = False
                    OnKeyDown = EdNFeVerDistDFeIntKeyDown
                  end
                  object EdNFeVerDowNFe: TdmkEdit
                    Left = 193
                    Top = 252
                    Width = 40
                    Height = 21
                    Alignment = taRightJustify
                    TabOrder = 10
                    FormatType = dmktfDouble
                    MskType = fmtNone
                    DecimalSize = 2
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '3,10'
                    QryName = 'QrPrmEmpNFe'
                    UpdCampo = 'NFeVerDowNFe'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 3.100000000000000000
                    ValWarn = False
                    OnKeyDown = EdNFeVerDowNFeKeyDown
                  end
                end
              end
            end
            object TabSheet25: TTabSheet
              Caption = ' DANFE '
              ImageIndex = 5
              ExplicitLeft = 0
              ExplicitTop = 0
              ExplicitWidth = 0
              ExplicitHeight = 0
              object Panel42: TPanel
                Left = 0
                Top = 0
                Width = 1036
                Height = 435
                Align = alClient
                BevelOuter = bvNone
                ParentBackground = False
                TabOrder = 0
                object Label135: TLabel
                  Left = 12
                  Top = 196
                  Width = 164
                  Height = 13
                  Caption = 'URL Emitente (n'#227'o recomendado):'
                end
                object Label64: TLabel
                  Left = 12
                  Top = 236
                  Width = 246
                  Height = 13
                  Caption = 'Arquivo do logo da empresa (Impress'#227'o no DANFE):'
                end
                object SpeedButton11: TSpeedButton
                  Left = 743
                  Top = 252
                  Width = 21
                  Height = 21
                  Caption = '...'
                  OnClick = SpeedButton11Click
                end
                object Label84: TLabel
                  Left = 12
                  Top = 276
                  Width = 106
                  Height = 13
                  Caption = 'Diret'#243'rio das DANFEs:'
                end
                object SpeedButton13: TSpeedButton
                  Left = 743
                  Top = 292
                  Width = 21
                  Height = 21
                  Caption = '...'
                  OnClick = SpeedButton13Click
                end
                object RGide_tpImp: TdmkRadioGroup
                  Left = 8
                  Top = 4
                  Width = 460
                  Height = 45
                  Caption = ' Formato de impress'#227'o: '
                  Columns = 3
                  ItemIndex = 0
                  Items.Strings = (
                    '0 - Nenhum'
                    '1 - Retrato'
                    '2 - Paisagem')
                  TabOrder = 0
                  QryCampo = 'QrPrmsEmpNFe'
                  UpdCampo = 'ide_tpImp'
                  UpdType = utYes
                  OldValor = 0
                end
                object RGNFeItsLin: TdmkRadioGroup
                  Left = 8
                  Top = 52
                  Width = 460
                  Height = 45
                  Caption = ' Linhas por item na DANFE: '
                  Columns = 4
                  ItemIndex = 0
                  Items.Strings = (
                    '3'
                    '1'
                    '2'
                    '3')
                  TabOrder = 1
                  QryCampo = 'QrPrmsEmpNFe'
                  UpdCampo = 'NFeItsLin'
                  UpdType = utYes
                  OldValor = 0
                end
                object GroupBox28: TGroupBox
                  Left = 8
                  Top = 100
                  Width = 460
                  Height = 65
                  Caption = ' Tamanho da Fonte: '
                  TabOrder = 2
                  object Label132: TLabel
                    Left = 12
                    Top = 20
                    Width = 66
                    Height = 13
                    Caption = 'Raz'#227'o Social:'
                  end
                  object Label133: TLabel
                    Left = 152
                    Top = 20
                    Width = 49
                    Height = 13
                    Caption = 'Endere'#231'o:'
                  end
                  object Label134: TLabel
                    Left = 292
                    Top = 20
                    Width = 45
                    Height = 13
                    Caption = 'Telefone:'
                  end
                  object EdNFeFTRazao: TdmkEdit
                    Left = 12
                    Top = 36
                    Width = 134
                    Height = 21
                    Alignment = taRightJustify
                    TabOrder = 0
                    FormatType = dmktfLongint
                    MskType = fmtNone
                    DecimalSize = 0
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '0'
                    QryCampo = 'QrPrmsEmpNFe'
                    UpdCampo = 'NFeFTRazao'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 0
                    ValWarn = False
                  end
                  object EdNFeFTEnder: TdmkEdit
                    Left = 152
                    Top = 36
                    Width = 134
                    Height = 21
                    Alignment = taRightJustify
                    TabOrder = 1
                    FormatType = dmktfLongint
                    MskType = fmtNone
                    DecimalSize = 0
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '0'
                    QryCampo = 'QrPrmsEmpNFe'
                    UpdCampo = 'NFeFTEnder'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 0
                    ValWarn = False
                  end
                  object EdNFeFTFones: TdmkEdit
                    Left = 292
                    Top = 36
                    Width = 134
                    Height = 21
                    Alignment = taRightJustify
                    TabOrder = 2
                    FormatType = dmktfLongint
                    MskType = fmtNone
                    DecimalSize = 0
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '0'
                    QryCampo = 'QrPrmsEmpNFe'
                    UpdCampo = 'NFeFTFones'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 0
                    ValWarn = False
                  end
                end
                object CkNFeMaiusc: TdmkCheckBox
                  Left = 12
                  Top = 172
                  Width = 169
                  Height = 17
                  Caption = 'For'#231'ar texto a ficar mai'#250'sculo.'
                  TabOrder = 3
                  QryCampo = 'QrPrmsEmpNFe'
                  UpdCampo = 'NFeMaiusc'
                  UpdType = utYes
                  ValCheck = '1'
                  ValUncheck = '0'
                  OldValor = #0
                end
                object EdNFeShowURL: TdmkEdit
                  Left = 12
                  Top = 212
                  Width = 749
                  Height = 21
                  TabOrder = 4
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  QryCampo = 'QrPrmsEmpNFe'
                  UpdCampo = 'NFeShowURL'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = ''
                  ValWarn = False
                end
                object EdPathLogoNF: TdmkEdit
                  Left = 12
                  Top = 252
                  Width = 732
                  Height = 21
                  TabOrder = 5
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ValMin = '-2147483647'
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  QryCampo = 'QrPrmsEmpNFe'
                  UpdCampo = 'PathLogoNF'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = ''
                  ValWarn = False
                end
                object EdDirDANFEs: TdmkEdit
                  Left = 12
                  Top = 292
                  Width = 732
                  Height = 21
                  TabOrder = 6
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ValMin = '-2147483647'
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  QryCampo = 'QrPrmsEmpNFe'
                  UpdCampo = 'DirDANFEs'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = ''
                  ValWarn = False
                end
                object RG_NFe_indFinalCpl: TdmkRadioGroup
                  Left = 8
                  Top = 317
                  Width = 506
                  Height = 39
                  Caption = 
                    ' Informa'#231#227'o dos valores totais aproximados de impostos no campo ' +
                    '"Dados Adicionais" nas NF-es: '
                  Columns = 3
                  ItemIndex = 0
                  Items.Strings = (
                    'Indefinido'
                    'Somente consumidor final'
                    'Todas NF-es')
                  TabOrder = 7
                  QryCampo = 'QrPrmsEmpNFe'
                  UpdCampo = 'NFe_indFinalCpl'
                  UpdType = utYes
                  OldValor = 0
                end
                object RGNoDANFEMail: TdmkRadioGroup
                  Left = 7
                  Top = 362
                  Width = 507
                  Height = 39
                  Caption = ' Descri'#231#227'o do DANFE PDF salvo para envio por e-mail'
                  Columns = 2
                  ItemIndex = 0
                  Items.Strings = (
                    'Chave de acesso da NF-e'
                    '"NF" + N'#250'mero NF-e + Nome do cliente')
                  TabOrder = 8
                  QryCampo = 'QrPrmsEmpNFe'
                  UpdCampo = 'NoDANFEMail'
                  UpdType = utYes
                  OldValor = 0
                end
              end
            end
          end
        end
      end
      object TabSheet22: TTabSheet
        Caption = 'SPED EFD'
        ImageIndex = 5
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object Panel39: TPanel
          Left = 0
          Top = 0
          Width = 1046
          Height = 465
          Align = alClient
          BevelOuter = bvLowered
          ParentBackground = False
          TabOrder = 0
          object GroupBox21: TGroupBox
            Left = 1
            Top = 63
            Width = 1044
            Height = 64
            Align = alTop
            Caption = ' Registro 0000: '
            TabOrder = 0
            object Label117: TLabel
              Left = 12
              Top = 20
              Width = 189
              Height = 13
              Caption = 'Perfil de apresenta'#231#227'o do arquivo fiscal:'
            end
            object Label118: TLabel
              Left = 208
              Top = 20
              Width = 143
              Height = 13
              Caption = 'Indicador de tipo de atividade:'
            end
            object EdSPED_EFD_IND_PERFIL: TdmkEdit
              Left = 12
              Top = 36
              Width = 20
              Height = 21
              CharCase = ecUpperCase
              MaxLength = 1
              TabOrder = 0
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '?'
              QryCampo = 'SPED_EFD_IND_PERFIL'
              UpdCampo = 'SPED_EFD_IND_PERFIL'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = '?'
              ValWarn = False
              OnChange = EdSPED_EFD_IND_PERFILChange
              OnKeyDown = EdSPED_EFD_IND_PERFILKeyDown
            end
            object EdSPED_EFD_IND_PERFIL_TXT: TdmkEdit
              Left = 32
              Top = 36
              Width = 172
              Height = 21
              ReadOnly = True
              TabOrder = 1
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '?'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = '?'
              ValWarn = False
            end
            object EdSPED_EFD_IND_ATIV: TdmkEdit
              Left = 208
              Top = 36
              Width = 20
              Height = 21
              Alignment = taRightJustify
              MaxLength = 1
              TabOrder = 2
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '9'
              QryCampo = 'SPED_EFD_IND_ATIV'
              UpdCampo = 'SPED_EFD_IND_ATIV'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 9
              ValWarn = False
              OnChange = EdSPED_EFD_IND_ATIVChange
              OnKeyDown = EdSPED_EFD_IND_ATIVKeyDown
            end
            object EdSPED_EFD_IND_ATIV_TXT: TdmkEdit
              Left = 228
              Top = 36
              Width = 536
              Height = 21
              ReadOnly = True
              TabOrder = 3
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '?'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = '?'
              ValWarn = False
            end
          end
          object GroupBox24: TGroupBox
            Left = 1
            Top = 127
            Width = 1044
            Height = 104
            Align = alTop
            Caption = ' Registro 0100: '
            TabOrder = 1
            object Label122: TLabel
              Left = 12
              Top = 20
              Width = 106
              Height = 13
              Caption = 'Contador respons'#225'vel:'
            end
            object Label123: TLabel
              Left = 440
              Top = 20
              Width = 146
              Height = 13
              Caption = 'CRC do Contador respons'#225'vel:'
            end
            object Label124: TLabel
              Left = 12
              Top = 60
              Width = 183
              Height = 13
              Caption = 'Escrit'#243'rio de contabilidade (se houver):'
            end
            object EdSPED_EFD_CadContador: TdmkEditCB
              Left = 12
              Top = 36
              Width = 56
              Height = 21
              Alignment = taRightJustify
              TabOrder = 0
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              QryCampo = 'SPED_EFD_CadContador'
              UpdCampo = 'SPED_EFD_CadContador'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              DBLookupComboBox = CBSPED_EFD_CadContador
              IgnoraDBLookupComboBox = False
              AutoSetIfOnlyOneReg = setregOnlyManual
            end
            object CBSPED_EFD_CadContador: TdmkDBLookupComboBox
              Left = 68
              Top = 36
              Width = 368
              Height = 21
              KeyField = 'Codigo'
              ListField = 'NO_ENT'
              ListSource = DsContador4
              TabOrder = 1
              dmkEditCB = EdSPED_EFD_CadContador
              QryCampo = 'SPED_EFD_CadContador'
              UpdType = utYes
              LocF7SQLMasc = '$#'
            end
            object EdSPED_EFD_CRCContador: TdmkEdit
              Left = 440
              Top = 36
              Width = 160
              Height = 21
              TabOrder = 2
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              QryCampo = 'SPED_EFD_CRCContador'
              UpdCampo = 'SPED_EFD_CRCContador'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object EdSPED_EFD_EscriContab: TdmkEditCB
              Left = 12
              Top = 76
              Width = 56
              Height = 21
              Alignment = taRightJustify
              TabOrder = 3
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              QryCampo = 'SPED_EFD_EscriContab'
              UpdCampo = 'SPED_EFD_EscriContab'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              DBLookupComboBox = CBSPED_EFD_EscriContab
              IgnoraDBLookupComboBox = False
              AutoSetIfOnlyOneReg = setregOnlyManual
            end
            object CBSPED_EFD_EscriContab: TdmkDBLookupComboBox
              Left = 68
              Top = 76
              Width = 533
              Height = 21
              KeyField = 'Codigo'
              ListField = 'NO_ENT'
              ListSource = DsEscrCtb4
              TabOrder = 4
              dmkEditCB = EdSPED_EFD_EscriContab
              QryCampo = 'SPED_EFD_EscriContab'
              UpdType = utYes
              LocF7SQLMasc = '$#'
            end
            object RGSPED_EFD_EnderContab: TdmkRadioGroup
              Left = 604
              Top = 21
              Width = 161
              Height = 77
              Caption = ' Endere'#231'o a ser informado: '
              ItemIndex = 0
              Items.Strings = (
                'Indefinido'
                'Do contador'
                'Do escrit'#243'rio')
              TabOrder = 5
              QryCampo = 'SPED_EFD_EnderContab'
              UpdCampo = 'SPED_EFD_EnderContab'
              UpdType = utYes
              OldValor = 0
            end
          end
          object GroupBox62: TGroupBox
            Left = 1
            Top = 231
            Width = 1044
            Height = 96
            Align = alTop
            Caption = ' Geral:'
            TabOrder = 2
            object Panel94: TPanel
              Left = 2
              Top = 15
              Width = 1040
              Height = 79
              Align = alClient
              BevelOuter = bvNone
              TabOrder = 0
              object RGSPED_EFD_DtFiscal: TdmkRadioGroup
                Left = 0
                Top = 0
                Width = 300
                Height = 79
                Align = alLeft
                Caption = ' Data de Escritura'#231#227'o de Emiss'#245'es:'
                Columns = 2
                ItemIndex = 0
                Items.Strings = (
                  'Indefinido'
                  'Emiss'#227'o'
                  'Saida')
                TabOrder = 0
                QryCampo = 'SPED_EFD_DtFiscal'
                UpdCampo = 'SPED_EFD_DtFiscal'
                UpdType = utYes
                OldValor = 0
              end
              object RGSPED_EFD_ID_0150: TdmkRadioGroup
                Left = 300
                Top = 0
                Width = 300
                Height = 79
                Align = alLeft
                Caption = ' ID no arquivo exporta'#231#227'o - Entidade: '
                Columns = 2
                ItemIndex = 0
                Items.Strings = (
                  'Indefinido'
                  'C'#243'digo entidade no app'
                  'C'#243'd. participante SPED'
                  'CNPJ / CPF'
                  '"Dmk_" + C'#243'digo entid.')
                TabOrder = 1
                QryCampo = 'SPED_EFD_ID_0150'
                UpdCampo = 'SPED_EFD_ID_0150'
                UpdType = utYes
                OldValor = 0
              end
              object RGSPED_EFD_ID_0200: TdmkRadioGroup
                Left = 600
                Top = 0
                Width = 300
                Height = 79
                Align = alLeft
                Caption = ' ID no arquivo exporta'#231#227'o - Produto: '
                Columns = 2
                ItemIndex = 0
                Items.Strings = (
                  'Indefinido'
                  'Reduzido no app'
                  'C'#243'digo produto SPED'
                  'NCM'
                  '"Dmk_" + Reduzido ')
                TabOrder = 2
                QryCampo = 'SPED_EFD_ID_0200'
                UpdCampo = 'SPED_EFD_ID_0200'
                UpdType = utYes
                OldValor = 0
              end
            end
          end
          object Panel96: TPanel
            Left = 1
            Top = 327
            Width = 1044
            Height = 158
            Align = alTop
            BevelOuter = bvNone
            TabOrder = 3
            object Panel98: TPanel
              Left = 0
              Top = 0
              Width = 1044
              Height = 134
              Align = alClient
              BevelOuter = bvNone
              TabOrder = 0
              object Panel99: TPanel
                Left = 133
                Top = 0
                Width = 911
                Height = 134
                Align = alClient
                BevelOuter = bvNone
                TabOrder = 1
                object RGSPED_EFD_Peri_K100: TdmkRadioGroup
                  Left = 0
                  Top = 44
                  Width = 911
                  Height = 44
                  Align = alTop
                  Caption = '  Per'#237'odo de apura'#231#227'o da produ'#231#227'o SPED EFD (registro K100):'
                  Columns = 6
                  Items.Strings = (
                    'Indefinido'
                    'Di'#225'rio'
                    'Semanal'
                    'Decendial'
                    'Quinzenal'
                    'Mensal')
                  TabOrder = 0
                  QryCampo = 'SPED_EFD_Peri_K100'
                  UpdCampo = 'SPED_EFD_Peri_K100'
                  UpdType = utYes
                  OldValor = 0
                end
                object RGSPED_EFD_Peri_E500: TdmkRadioGroup
                  Left = 0
                  Top = 88
                  Width = 911
                  Height = 44
                  Align = alTop
                  Caption = '  Per'#237'odo de apura'#231#227'o do IPI SPED EFD (registro E500):'
                  Columns = 6
                  Items.Strings = (
                    'Indefinido'
                    'Di'#225'rio'
                    'Semanal'
                    'Decendial'
                    'Quinzenal'
                    'Mensal')
                  TabOrder = 1
                  QryCampo = 'SPED_EFD_Peri_E500'
                  UpdCampo = 'SPED_EFD_Peri_E500'
                  UpdType = utYes
                  OldValor = 0
                end
                object RGSPED_EFD_Peri_E100: TdmkRadioGroup
                  Left = 0
                  Top = 0
                  Width = 911
                  Height = 44
                  Align = alTop
                  Caption = '  Per'#237'odo de apura'#231#227'o do ICMS SPED EFD (Registro E100):'
                  Columns = 6
                  Items.Strings = (
                    'Indefinido'
                    'Di'#225'rio'
                    'Semanal'
                    'Decendial'
                    'Quinzenal'
                    'Mensal')
                  TabOrder = 2
                  QryCampo = 'SPED_EFD_Peri_E100'
                  UpdCampo = 'SPED_EFD_Peri_E100'
                  UpdType = utYes
                  OldValor = 0
                end
              end
              object RGSPED_EFD_Producao: TdmkRadioGroup
                Left = 0
                Top = 0
                Width = 85
                Height = 134
                Align = alLeft
                Caption = ' Produ'#231#227'o: '
                ItemIndex = 1
                Items.Strings = (
                  'Deprecado!Singular'
                  'Deprecado!Conjunta')
                TabOrder = 0
                QryCampo = 'SPED_EFD_Producao'
                UpdCampo = 'SPED_EFD_Producao'
                UpdType = utYes
                OldValor = 0
              end
              object Panel97: TPanel
                Left = 85
                Top = 0
                Width = 48
                Height = 134
                Align = alLeft
                BevelOuter = bvNone
                TabOrder = 2
                object SpeedButton50: TSpeedButton
                  Left = 12
                  Top = 44
                  Width = 23
                  Height = 22
                  Caption = '?'
                  OnClick = SpeedButton50Click
                end
              end
            end
            object Panel104: TPanel
              Left = 0
              Top = 134
              Width = 1044
              Height = 24
              Align = alBottom
              BevelOuter = bvNone
              TabOrder = 1
              object CkSPED_EFD_MovSubPrd: TdmkCheckBox
                Left = 4
                Top = 4
                Width = 553
                Height = 17
                Caption = 
                  'Informa movimenta'#231#227'o dos subprodutos na produ'#231#227'o (Bloco K regist' +
                  'ros 210 a 275 e 290 a 302).'
                TabOrder = 0
                QryCampo = 'SPED_EFD_MovSubPrd'
                UpdCampo = 'SPED_EFD_MovSubPrd'
                UpdType = utYes
                ValCheck = #0
                ValUncheck = #0
                OldValor = #0
              end
            end
          end
          object Panel103: TPanel
            Left = 1
            Top = 1
            Width = 1044
            Height = 62
            Align = alTop
            BevelOuter = bvNone
            Caption = 'Panel103'
            TabOrder = 4
            object GroupBox26: TGroupBox
              Left = 160
              Top = 0
              Width = 884
              Height = 62
              Align = alClient
              Caption = ' PVA EFD:'
              TabOrder = 1
              object Label128: TLabel
                Left = 12
                Top = 16
                Width = 176
                Height = 13
                Caption = 'Caminho para salvar o arquivo digital:'
              end
              object SpeedButton17: TSpeedButton
                Left = 743
                Top = 31
                Width = 21
                Height = 21
                Caption = '...'
                OnClick = SpeedButton17Click
              end
              object EdSPED_EFD_Path: TdmkEdit
                Left = 12
                Top = 31
                Width = 729
                Height = 21
                TabOrder = 0
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                QryCampo = 'SPED_EFD_Path'
                UpdCampo = 'SPED_EFD_Path'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
            end
            object GroupBox66: TGroupBox
              Left = 0
              Top = 0
              Width = 160
              Height = 62
              Align = alLeft
              Caption = 'Vers'#227'o:'
              TabOrder = 0
              object Label438: TLabel
                Left = 9
                Top = 16
                Width = 112
                Height = 13
                Caption = 'Vers'#227'o do Guia Pr'#225'tico:'
              end
              object SpeedButton51: TSpeedButton
                Left = 132
                Top = 30
                Width = 23
                Height = 22
                Caption = '?'
                OnClick = SpeedButton51Click
              end
              object EdSPED_EFD_ICMS_IPI_VersaoGuia: TdmkEdit
                Left = 8
                Top = 31
                Width = 120
                Height = 21
                TabOrder = 0
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                QryCampo = 'SPED_EFD_ICMS_IPI_VersaoGuia'
                UpdCampo = 'SPED_EFD_ICMS_IPI_VersaoGuia'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
            end
          end
        end
      end
      object TabSheet29: TTabSheet
        Caption = 'NFS-e'
        ImageIndex = 7
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object PageControl5: TPageControl
          Left = 0
          Top = 0
          Width = 1046
          Height = 465
          ActivePage = TabSheet34
          Align = alClient
          TabOrder = 0
          object TabSheet30: TTabSheet
            Caption = ' Configura'#231#245'es Gerais '
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object Panel49: TPanel
              Left = 0
              Top = 0
              Width = 1038
              Height = 437
              Align = alClient
              ParentBackground = False
              TabOrder = 0
              object Label157: TLabel
                Left = 12
                Top = 20
                Width = 165
                Height = 13
                Caption = 'M'#233'todo de integra'#231#227'o de sistemas:'
              end
              object Label158: TLabel
                Left = 716
                Top = 20
                Width = 36
                Height = 13
                Caption = 'Vers'#227'o:'
              end
              object Label159: TLabel
                Left = 964
                Top = 20
                Width = 67
                Height = 13
                Caption = 'S'#233'rie do RPS:'
              end
              object Label163: TLabel
                Left = 12
                Top = 192
                Width = 93
                Height = 13
                Caption = #218'ltima DPS emitida:'
              end
              object Label164: TLabel
                Left = 12
                Top = 60
                Width = 192
                Height = 13
                Caption = 'Web Service Municipal de PRODU'#199#195'O:'
              end
              object Label168: TLabel
                Left = 12
                Top = 100
                Width = 215
                Height = 13
                Caption = 'Web Service Municipal de HOMOLOGA'#199#195'O:'
              end
              object Label170: TLabel
                Left = 472
                Top = 20
                Width = 167
                Height = 13
                Caption = 'Tipo de email para envio de NFS-e:'
              end
              object SbEdNFSeTipCtoMail: TSpeedButton
                Left = 692
                Top = 36
                Width = 21
                Height = 21
                Caption = '...'
                OnClick = SbEdNFSeTipCtoMailClick
              end
              object Label188: TLabel
                Left = 116
                Top = 192
                Width = 81
                Height = 13
                Caption = #218'ltimo Lote RPS:'
              end
              object Label191: TLabel
                Left = 220
                Top = 192
                Width = 65
                Height = 13
                Caption = 'Usu'#225'rio Web:'
              end
              object Label192: TLabel
                Left = 352
                Top = 192
                Width = 60
                Height = 13
                Caption = 'Senha Web:'
              end
              object Label193: TLabel
                Left = 484
                Top = 192
                Width = 86
                Height = 13
                Caption = 'C'#243'digo Munic'#237'pio:'
              end
              object Label194: TLabel
                Left = 12
                Top = 232
                Width = 125
                Height = 13
                Caption = 'Cabe'#231'alho NFS-e Linha 1:'
              end
              object Bevel1: TBevel
                Left = 588
                Top = 208
                Width = 149
                Height = 21
              end
              object Label195: TLabel
                Left = 12
                Top = 272
                Width = 65
                Height = 13
                Caption = 'Logo da Filial:'
              end
              object SbNFSeLogoFili: TSpeedButton
                Left = 495
                Top = 288
                Width = 21
                Height = 21
                Caption = '...'
                OnClick = SbNFSeLogoFiliClick
              end
              object Label196: TLabel
                Left = 520
                Top = 272
                Width = 90
                Height = 13
                Caption = 'Logo da Prefeitura:'
              end
              object SbNFSeLogoPref: TSpeedButton
                Left = 1007
                Top = 288
                Width = 21
                Height = 21
                Caption = '...'
                OnClick = SbNFSeLogoPrefClick
              end
              object Label197: TLabel
                Left = 352
                Top = 232
                Width = 125
                Height = 13
                Caption = 'Cabe'#231'alho NFS-e Linha 2:'
              end
              object Label198: TLabel
                Left = 692
                Top = 232
                Width = 125
                Height = 13
                Caption = 'Cabe'#231'alho NFS-e Linha 3:'
              end
              object Label199: TLabel
                Left = 760
                Top = 20
                Width = 64
                Height = 13
                Caption = 'Tipo de RPS:'
              end
              object EdNFSeMetodo: TdmkEditCB
                Left = 12
                Top = 36
                Width = 56
                Height = 21
                Alignment = taRightJustify
                TabOrder = 0
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                QryCampo = 'NFSeMetodo'
                UpdCampo = 'NFSeMetodo'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
                DBLookupComboBox = CBNFSeMetodo
                IgnoraDBLookupComboBox = False
                AutoSetIfOnlyOneReg = setregOnlyManual
              end
              object CBNFSeMetodo: TdmkDBLookupComboBox
                Left = 68
                Top = 36
                Width = 397
                Height = 21
                KeyField = 'Codigo'
                ListField = 'Nome'
                ListSource = DsMyNFSeMetodos
                TabOrder = 1
                dmkEditCB = EdNFSeMetodo
                QryCampo = 'NFSeMetodo'
                UpdType = utYes
                LocF7SQLMasc = '$#'
              end
              object EdNFSeVersao: TdmkEdit
                Left = 716
                Top = 36
                Width = 41
                Height = 21
                Alignment = taRightJustify
                TabOrder = 4
                FormatType = dmktfDouble
                MskType = fmtNone
                DecimalSize = 2
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0,00'
                QryCampo = 'NFSeVersao'
                UpdCampo = 'NFSeVersao'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0.000000000000000000
                ValWarn = False
              end
              object EdNFSeSerieRps: TdmkEdit
                Left = 964
                Top = 36
                Width = 69
                Height = 21
                TabOrder = 7
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                QryCampo = 'NFSeSerieRps'
                UpdCampo = 'NFSeSerieRps'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object EdDpsNumero: TdmkEdit
                Left = 12
                Top = 208
                Width = 100
                Height = 21
                Alignment = taRightJustify
                TabOrder = 13
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                QryCampo = 'DpsNumero'
                UpdCampo = 'DpsNumero'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
              end
              object EdNFSeWSProducao: TdmkEdit
                Left = 12
                Top = 76
                Width = 724
                Height = 21
                TabOrder = 8
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                QryCampo = 'NFSeWSProducao'
                UpdCampo = 'NFSeWSProducao'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object RGNFSeAmbiente: TdmkRadioGroup
                Left = 888
                Top = 60
                Width = 145
                Height = 81
                Caption = ' Identifica'#231#227'o do Ambiente: '
                ItemIndex = 0
                Items.Strings = (
                  '0 - Nenhum'
                  '1 - Produ'#231#227'o'
                  '2 - Homologa'#231#227'o')
                TabOrder = 11
                QryCampo = 'NFSeAmbiente'
                UpdCampo = 'NFSeAmbiente'
                UpdType = utYes
                OldValor = 0
              end
              object EdNFSeWSHomologa: TdmkEdit
                Left = 12
                Top = 116
                Width = 724
                Height = 21
                TabOrder = 9
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                QryCampo = 'NFSeWSHomologa'
                UpdCampo = 'NFSeWSHomologa'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object CBNFSeTipCtoMail: TdmkDBLookupComboBox
                Left = 500
                Top = 36
                Width = 189
                Height = 21
                KeyField = 'Codigo'
                ListField = 'Nome'
                ListSource = DsEntiTipCt2
                TabOrder = 3
                dmkEditCB = EdNFSeTipCtoMail
                UpdType = utYes
                LocF7SQLMasc = '$#'
              end
              object EdNFSeTipCtoMail: TdmkEditCB
                Left = 472
                Top = 36
                Width = 28
                Height = 21
                Alignment = taRightJustify
                TabOrder = 2
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
                DBLookupComboBox = CBNFSeTipCtoMail
                IgnoraDBLookupComboBox = False
                AutoSetIfOnlyOneReg = setregOnlyManual
              end
              object GroupBox33: TGroupBox
                Left = 12
                Top = 140
                Width = 1025
                Height = 49
                Caption = ' Certificado Digital: '
                TabOrder = 12
                object Label178: TLabel
                  Left = 11
                  Top = 24
                  Width = 82
                  Height = 13
                  Caption = 'N'#250'mero de S'#233'rie:'
                end
                object SbNFSeCertDigital: TSpeedButton
                  Left = 372
                  Top = 20
                  Width = 21
                  Height = 21
                  Caption = '...'
                  OnClick = SbNFSeCertDigitalClick
                end
                object Label179: TLabel
                  Left = 396
                  Top = 24
                  Width = 84
                  Height = 13
                  Caption = 'Data da validade:'
                end
                object Label180: TLabel
                  Left = 600
                  Top = 24
                  Width = 268
                  Height = 13
                  Caption = 'Avisar a expira'#231#227'o da validade do certificado digital com '
                end
                object Label183: TLabel
                  Left = 912
                  Top = 24
                  Width = 105
                  Height = 13
                  Caption = 'dias de anteced'#234'ncia.'
                end
                object EdNFSeCertDigital: TdmkEdit
                  Left = 96
                  Top = 20
                  Width = 273
                  Height = 21
                  MaxLength = 15
                  TabOrder = 0
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  QryCampo = 'NFSeCertDigital'
                  UpdCampo = 'NFSeCertDigital'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = ''
                  ValWarn = False
                end
                object TPNFSeCertValidad: TdmkEditDateTimePicker
                  Left = 484
                  Top = 20
                  Width = 112
                  Height = 21
                  Date = 0.669258657406317100
                  Time = 0.669258657406317100
                  TabOrder = 1
                  ReadOnly = False
                  DefaultEditMask = '!99/99/99;1;_'
                  AutoApplyEditMask = True
                  QryCampo = 'NFSeCertValidad'
                  UpdCampo = 'NFSeCertValidad'
                  UpdType = utYes
                  DatePurpose = dmkdpNone
                end
                object EdNFSeCertAviExpi: TdmkEdit
                  Left = 868
                  Top = 20
                  Width = 40
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 2
                  FormatType = dmktfInteger
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '30'
                  QryCampo = 'NFSeCertAviExpi'
                  UpdCampo = 'NFSeCertAviExpi'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 30
                  ValWarn = False
                  OnKeyDown = EdUF_ServicoKeyDown
                end
              end
              object EdRPSNumLote: TdmkEdit
                Left = 116
                Top = 208
                Width = 100
                Height = 21
                Alignment = taRightJustify
                TabOrder = 14
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                QryCampo = 'RPSNumLote'
                UpdCampo = 'RPSNumLote'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
              end
              object EdNFSeUserWeb: TdmkEdit
                Left = 220
                Top = 208
                Width = 128
                Height = 21
                TabOrder = 15
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                QryCampo = 'NFSeUserWeb'
                UpdCampo = 'NFSeUserWeb'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object EdNFSeSenhaWeb: TdmkEdit
                Left = 352
                Top = 208
                Width = 128
                Height = 21
                TabOrder = 16
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                QryCampo = 'NFSeSenhaWeb'
                UpdCampo = 'NFSeSenhaWeb'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object EdNFSeCodMunici: TdmkEdit
                Left = 484
                Top = 208
                Width = 100
                Height = 21
                Alignment = taRightJustify
                TabOrder = 17
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                QryCampo = 'NFSeCodMunici'
                UpdCampo = 'NFSeCodMunici'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
              end
              object EdNFSePrefeitura1: TdmkEdit
                Left = 12
                Top = 248
                Width = 336
                Height = 21
                TabOrder = 19
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                QryCampo = 'NFSePrefeitura1'
                UpdCampo = 'NFSePrefeitura1'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object CkNFSeMsgVisu: TdmkCheckBox
                Left = 592
                Top = 210
                Width = 137
                Height = 17
                Caption = 'Visualizar mensagens.'
                TabOrder = 18
                QryCampo = 'NFSeMsgVisu'
                UpdCampo = 'NFSeMsgVisu'
                UpdType = utYes
                ValCheck = '1'
                ValUncheck = '0'
                OldValor = #0
              end
              object EdNFSeLogoFili: TdmkEdit
                Left = 12
                Top = 288
                Width = 480
                Height = 21
                TabOrder = 22
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                QryCampo = 'NFSeLogoFili'
                UpdCampo = 'NFSeLogoFili'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object EdNFSeLogoPref: TdmkEdit
                Left = 520
                Top = 288
                Width = 485
                Height = 21
                TabOrder = 23
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                QryCampo = 'NFSeLogoPref'
                UpdCampo = 'NFSeLogoPref'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object EdNFSePrefeitura2: TdmkEdit
                Left = 352
                Top = 248
                Width = 336
                Height = 21
                TabOrder = 20
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                QryCampo = 'NFSePrefeitura2'
                UpdCampo = 'NFSePrefeitura2'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object EdNFSePrefeitura3: TdmkEdit
                Left = 692
                Top = 248
                Width = 336
                Height = 21
                TabOrder = 21
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                QryCampo = 'NFSePrefeitura3'
                UpdCampo = 'NFSePrefeitura3'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object RGNFSeMetodEnvRPS: TdmkRadioGroup
                Left = 740
                Top = 60
                Width = 145
                Height = 81
                Caption = ' Envio RPS:  '
                ItemIndex = 0
                Items.Strings = (
                  '0 - Nenhum'
                  '1 - Ass'#237'ncrono'
                  '2 - S'#237'ncrono')
                TabOrder = 10
                QryCampo = 'NFSeMetodEnvRPS'
                UpdCampo = 'NFSeMetodEnvRPS'
                UpdType = utYes
                OldValor = 0
              end
              object EdNFSeTipoRps: TdmkEdit
                Left = 760
                Top = 36
                Width = 16
                Height = 21
                Alignment = taRightJustify
                TabOrder = 5
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '0'
                ValMax = '9'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                QryCampo = 'NFSeTipoRps'
                UpdCampo = 'NFSeTipoRps'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
                OnChange = EdNFSeTipoRpsChange
              end
              object EdTXT_NFSeTipoRps1: TdmkEdit
                Left = 776
                Top = 36
                Width = 185
                Height = 21
                ReadOnly = True
                TabOrder = 6
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object RG_NFSe_indFinalCpl: TdmkRadioGroup
                Left = 14
                Top = 317
                Width = 505
                Height = 39
                Caption = 
                  ' Informa'#231#227'o dos valores totais aproximados de impostos no campo ' +
                  'da descri'#231#227'o do servi'#231'o nas NFS-es: '
                Columns = 3
                ItemIndex = 0
                Items.Strings = (
                  'Indefinido'
                  'Somente consumidor final'
                  'Todas NFS-es')
                TabOrder = 24
                QryCampo = 'NFSe_indFinalCpl'
                UpdCampo = 'NFSe_indFinalCpl'
                UpdType = utYes
                OldValor = 0
              end
            end
          end
          object TabSheet31: TTabSheet
            Caption = ' Diret'#243'rios dos arquivos XML '
            ImageIndex = 1
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object Panel51: TPanel
              Left = 0
              Top = 0
              Width = 1038
              Height = 437
              Align = alClient
              BevelOuter = bvNone
              ParentBackground = False
              TabOrder = 0
              object Label171: TLabel
                Left = 8
                Top = 84
                Width = 247
                Height = 13
                Caption = 'Diret'#243'rio dos lotes de RPS gerados (-env-lot-rps.xml):'
              end
              object Label172: TLabel
                Left = 8
                Top = 124
                Width = 317
                Height = 13
                Caption = 
                  'Diret'#243'rio das respostas dos lotes de RPS enviados (-rec-lot-rps.' +
                  'xml):'
              end
              object Label173: TLabel
                Left = 8
                Top = 204
                Width = 342
                Height = 13
                Caption = 
                  'Diret'#243'rio dos resultados de processamento de lote de NF-e (-pro-' +
                  'rec.xml):'
                Visible = False
              end
              object SbDirNFSeRPSRecLot: TSpeedButton
                Left = 410
                Top = 140
                Width = 21
                Height = 21
                Caption = '...'
                OnClick = SbDirNFSeRPSRecLotClick
              end
              object SbDirNFSeRPSEnvLot: TSpeedButton
                Left = 410
                Top = 100
                Width = 21
                Height = 21
                Caption = '...'
                OnClick = SbDirNFSeRPSEnvLotClick
              end
              object SpeedButton22: TSpeedButton
                Left = 410
                Top = 220
                Width = 21
                Height = 21
                Caption = '...'
                Visible = False
              end
              object Label174: TLabel
                Left = 8
                Top = 44
                Width = 184
                Height = 13
                Caption = 'Diret'#243'rio das DPS assinadas (-dps.xml):'
              end
              object SbDirNFSeDPSAss: TSpeedButton
                Left = 410
                Top = 60
                Width = 21
                Height = 21
                Caption = '...'
                OnClick = SbDirNFSeDPSAssClick
              end
              object Label175: TLabel
                Left = 8
                Top = 4
                Width = 175
                Height = 13
                Caption = 'Diret'#243'rio das DPS geradas (-dps.xml):'
              end
              object SbDirNFSeDPSGer: TSpeedButton
                Left = 410
                Top = 20
                Width = 21
                Height = 21
                Caption = '...'
                OnClick = SbDirNFSeDPSGerClick
              end
              object Label176: TLabel
                Left = 8
                Top = 244
                Width = 192
                Height = 13
                Caption = 'Diret'#243'rio das NF-e denegadas (-den.xml):'
                Visible = False
              end
              object SpeedButton25: TSpeedButton
                Left = 410
                Top = 260
                Width = 21
                Height = 21
                Caption = '...'
                Visible = False
              end
              object Label177: TLabel
                Left = 8
                Top = 164
                Width = 400
                Height = 13
                Caption = 
                  'Diret'#243'rio dos pedidos de resultado de processamento de lotes de ' +
                  'NF-e (-ped-rec.xml):'
                Visible = False
              end
              object SpeedButton26: TSpeedButton
                Left = 410
                Top = 180
                Width = 21
                Height = 21
                Caption = '...'
                Visible = False
              end
              object Label189: TLabel
                Left = 440
                Top = 4
                Width = 138
                Height = 13
                Caption = 'Diret'#243'rio dos arquivos NFS-e:'
              end
              object SbDirNFSeLogs: TSpeedButton
                Left = 842
                Top = 20
                Width = 21
                Height = 21
                Caption = '...'
                OnClick = SbDirNFSeLogsClick
              end
              object Label190: TLabel
                Left = 440
                Top = 44
                Width = 134
                Height = 13
                Caption = 'Diret'#243'rio dos Schemas XML:'
              end
              object SbDirNFSeSchema: TSpeedButton
                Left = 842
                Top = 60
                Width = 21
                Height = 21
                Caption = '...'
                OnClick = SbDirNFSeSchemaClick
              end
              object EdDirNFSeRPSEnvLot: TdmkEdit
                Left = 8
                Top = 100
                Width = 400
                Height = 21
                TabOrder = 2
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                QryCampo = 'DirNFSeRPSEnvLot'
                UpdCampo = 'DirNFSeRPSEnvLot'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object EdDirNFSeRPSRecLot: TdmkEdit
                Left = 8
                Top = 140
                Width = 400
                Height = 21
                TabOrder = 3
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                QryCampo = 'DirNFSeRPSRecLot'
                UpdCampo = 'DirNFSeRPSRecLot'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object dmkEdit3: TdmkEdit
                Left = 8
                Top = 220
                Width = 400
                Height = 21
                TabOrder = 5
                Visible = False
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object EdDirNFSeDPSAss: TdmkEdit
                Left = 8
                Top = 60
                Width = 400
                Height = 21
                TabOrder = 1
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                QryCampo = 'DirNFSeDPSAss'
                UpdCampo = 'DirNFSeDPSAss'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object EdDirNFSeDPSGer: TdmkEdit
                Left = 8
                Top = 20
                Width = 400
                Height = 21
                TabOrder = 0
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                QryCampo = 'DirNFSeDPSGer'
                UpdCampo = 'DirNFSeDPSGer'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object dmkEdit6: TdmkEdit
                Left = 8
                Top = 260
                Width = 400
                Height = 21
                TabOrder = 6
                Visible = False
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object dmkEdit7: TdmkEdit
                Left = 8
                Top = 180
                Width = 400
                Height = 21
                TabOrder = 4
                Visible = False
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object BtSugereDirNFSe: TBitBtn
                Left = 8
                Top = 287
                Width = 580
                Height = 40
                Cursor = crHandPoint
                Caption = 
                  'Sugest'#227'o de diret'#243'rios para os arquivos XML (somente na altera'#231#227 +
                  'o do registro, e somente para n'#227'o definidos)'
                Enabled = False
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -12
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                NumGlyphs = 2
                ParentFont = False
                ParentShowHint = False
                ShowHint = True
                TabOrder = 9
                OnClick = BtSugereDirNFSeClick
              end
              object EdDirNFSeLogs: TdmkEdit
                Left = 440
                Top = 20
                Width = 400
                Height = 21
                TabOrder = 7
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                QryCampo = 'DirNFSeLogs'
                UpdCampo = 'DirNFSeLogs'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object EdDirNFSeSchema: TdmkEdit
                Left = 440
                Top = 60
                Width = 400
                Height = 21
                TabOrder = 8
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                QryCampo = 'DirNFSeSchema'
                UpdCampo = 'DirNFSeSchema'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
            end
          end
          object TabSheet34: TTabSheet
            Caption = 'Op'#231#245'es'
            ImageIndex = 2
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object Panel52: TPanel
              Left = 0
              Top = 0
              Width = 1038
              Height = 437
              Align = alClient
              ParentBackground = False
              TabOrder = 0
              object Label250: TLabel
                Left = 8
                Top = 84
                Width = 461
                Height = 13
                Caption = 
                  'Email pr'#243'prio para receber email enviado (mais de um separar por' +
                  ' ponto e v'#237'rgula) m'#225'x. 255 letras:'
              end
              object Label212: TLabel
                Left = 8
                Top = 4
                Width = 199
                Height = 13
                Caption = 'Pr'#233'-email para envio de NFS-e autorizada:'
              end
              object Label213: TLabel
                Left = 7
                Top = 44
                Width = 200
                Height = 13
                Caption = 'Pr'#233'-email para envio de NFS-e cancelada:'
              end
              object EdNFSePreMailAut: TdmkEditCB
                Left = 8
                Top = 20
                Width = 44
                Height = 21
                Alignment = taRightJustify
                TabOrder = 0
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                QryCampo = 'NFSePreMailAut'
                UpdCampo = 'NFSePreMailAut'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
                DBLookupComboBox = CBNFSePreMailAut
                IgnoraDBLookupComboBox = False
                AutoSetIfOnlyOneReg = setregOnlyManual
              end
              object CBNFSePreMailAut: TdmkDBLookupComboBox
                Left = 52
                Top = 20
                Width = 705
                Height = 21
                KeyField = 'Codigo'
                ListField = 'Nome'
                ListSource = DsPreEmailNFSeAut
                TabOrder = 1
                dmkEditCB = EdNFSePreMailAut
                QryCampo = 'NFSePreMailAut'
                UpdType = utYes
                LocF7SQLMasc = '$#'
              end
              object EdNFSePreMailCan: TdmkEditCB
                Left = 7
                Top = 60
                Width = 44
                Height = 21
                Alignment = taRightJustify
                TabOrder = 2
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                QryCampo = 'NFSePreMailCan'
                UpdCampo = 'NFSePreMailCan'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
                DBLookupComboBox = CBNFSePreMailCan
                IgnoraDBLookupComboBox = False
                AutoSetIfOnlyOneReg = setregOnlyManual
              end
              object CBNFSePreMailCan: TdmkDBLookupComboBox
                Left = 51
                Top = 60
                Width = 706
                Height = 21
                KeyField = 'Codigo'
                ListField = 'Nome'
                ListSource = DsPreEmailNFSeCan
                TabOrder = 3
                dmkEditCB = EdNFSePreMailCan
                QryCampo = 'NFSePreMailCan'
                UpdType = utYes
                LocF7SQLMasc = '$#'
              end
              object EdMyEmailNFSe: TdmkEdit
                Left = 8
                Top = 100
                Width = 749
                Height = 21
                TabOrder = 4
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = True
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                QryCampo = 'MyEmailNFSe'
                UpdCampo = 'MyEmailNFSe'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
            end
          end
        end
      end
      object TabSheet38: TTabSheet
        Caption = 'CT-e'
        ImageIndex = 8
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object PageControl7: TPageControl
          Left = 0
          Top = 0
          Width = 1046
          Height = 465
          ActivePage = TabSheet45
          Align = alClient
          TabOrder = 0
          object TabSheet39: TTabSheet
            Caption = ' Configura'#231#245'es gerais '
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object Panel65: TPanel
              Left = 0
              Top = 0
              Width = 1038
              Height = 437
              Align = alClient
              BevelOuter = bvNone
              ParentBackground = False
              TabOrder = 0
              object Panel66: TPanel
                Left = 0
                Top = 125
                Width = 1038
                Height = 136
                Align = alTop
                BevelOuter = bvNone
                TabOrder = 0
                object RGCTetpEmis: TdmkRadioGroup
                  Left = 0
                  Top = 0
                  Width = 425
                  Height = 136
                  Align = alLeft
                  Caption = ' Tipo de emiss'#227'o da CT-e: (apenas informativo)'
                  Columns = 2
                  ItemIndex = 1
                  Items.Strings = (
                    '0 - Nenhum'
                    '1 - Normal'
                    '2 - '
                    '3 - '
                    '4 - Conting'#234'ncia EPEC pela SVC'
                    '5 - Conting'#234'ncia FSDA'
                    '6-'
                    '7-Autoriza'#231#227'o pela SVC-RS'
                    '8-Autoriza'#231#227'o pela SVC-SP')
                  TabOrder = 0
                  QryCampo = 'CTetpEmis'
                  UpdCampo = 'CTetpEmis'
                  UpdType = utYes
                  OldValor = 0
                end
                object GroupBox44: TGroupBox
                  Left = 425
                  Top = 0
                  Width = 613
                  Height = 136
                  Align = alClient
                  Caption = ' Pr'#233'-emails: '
                  TabOrder = 1
                  object Label290: TLabel
                    Left = 8
                    Top = 16
                    Width = 192
                    Height = 13
                    Caption = 'Pr'#233'-email para envio de CT-e autorizada:'
                  end
                  object Label291: TLabel
                    Left = 8
                    Top = 56
                    Width = 193
                    Height = 13
                    Caption = 'Pr'#233'-email para envio de CT-e cancelada:'
                  end
                  object Label292: TLabel
                    Left = 8
                    Top = 96
                    Width = 250
                    Height = 13
                    Caption = 'Pr'#233'-email para envio de carta de corre'#231#227'o eletr'#244'nica:'
                  end
                  object EdCTePreMailAut: TdmkEditCB
                    Left = 8
                    Top = 32
                    Width = 44
                    Height = 21
                    Alignment = taRightJustify
                    TabOrder = 0
                    FormatType = dmktfInteger
                    MskType = fmtNone
                    DecimalSize = 0
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ValMin = '-2147483647'
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '0'
                    QryCampo = 'CTePreMailAut'
                    UpdCampo = 'CTePreMailAut'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 0
                    ValWarn = False
                    DBLookupComboBox = CBCTePreMailAut
                    IgnoraDBLookupComboBox = False
                    AutoSetIfOnlyOneReg = setregOnlyManual
                  end
                  object CBCTePreMailAut: TdmkDBLookupComboBox
                    Left = 52
                    Top = 32
                    Width = 520
                    Height = 21
                    KeyField = 'Codigo'
                    ListField = 'Nome'
                    ListSource = DsPreMail0
                    TabOrder = 1
                    dmkEditCB = EdCTePreMailAut
                    QryCampo = 'CTePreMailAut'
                    UpdType = utYes
                    LocF7SQLMasc = '$#'
                  end
                  object EdCTePreMailCan: TdmkEditCB
                    Left = 8
                    Top = 72
                    Width = 44
                    Height = 21
                    Alignment = taRightJustify
                    TabOrder = 4
                    FormatType = dmktfInteger
                    MskType = fmtNone
                    DecimalSize = 0
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ValMin = '-2147483647'
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '0'
                    QryCampo = 'CTePreMailCan'
                    UpdCampo = 'CTePreMailCan'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 0
                    ValWarn = False
                    DBLookupComboBox = CBCTePreMailCan
                    IgnoraDBLookupComboBox = False
                    AutoSetIfOnlyOneReg = setregOnlyManual
                  end
                  object CBCTePreMailCan: TdmkDBLookupComboBox
                    Left = 52
                    Top = 72
                    Width = 520
                    Height = 21
                    KeyField = 'Codigo'
                    ListField = 'Nome'
                    ListSource = DsPreMail1
                    TabOrder = 5
                    dmkEditCB = EdCTePreMailCan
                    QryCampo = 'CTePreMailCan'
                    UpdType = utYes
                    LocF7SQLMasc = '$#'
                  end
                  object EdCTePreMailEveCCe: TdmkEditCB
                    Left = 8
                    Top = 112
                    Width = 44
                    Height = 21
                    Alignment = taRightJustify
                    TabOrder = 2
                    FormatType = dmktfInteger
                    MskType = fmtNone
                    DecimalSize = 0
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ValMin = '-2147483647'
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '0'
                    QryCampo = 'CTePreMailEveCCe'
                    UpdCampo = 'CTePreMailEveCCe'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 0
                    ValWarn = False
                    DBLookupComboBox = CBCTePreMailEveCCe
                    IgnoraDBLookupComboBox = False
                    AutoSetIfOnlyOneReg = setregOnlyManual
                  end
                  object CBCTePreMailEveCCe: TdmkDBLookupComboBox
                    Left = 52
                    Top = 112
                    Width = 520
                    Height = 21
                    KeyField = 'Codigo'
                    ListField = 'Nome'
                    ListSource = DsPreMail0
                    TabOrder = 3
                    dmkEditCB = EdCTePreMailEveCCe
                    QryCampo = 'CTePreMailEveCCe'
                    UpdType = utYes
                    LocF7SQLMasc = '$#'
                  end
                end
              end
              object Panel67: TPanel
                Left = 0
                Top = 261
                Width = 1038
                Height = 152
                Align = alTop
                BevelOuter = bvNone
                TabOrder = 1
                object GroupBox45: TGroupBox
                  Left = 121
                  Top = 0
                  Width = 917
                  Height = 152
                  Align = alClient
                  TabOrder = 1
                  object Panel68: TPanel
                    Left = 2
                    Top = 15
                    Width = 643
                    Height = 135
                    Align = alLeft
                    BevelOuter = bvNone
                    TabOrder = 0
                    object Label293: TLabel
                      Left = 8
                      Top = 4
                      Width = 282
                      Height = 13
                      Caption = 'Diret'#243'rio das CT-e protocoladas (autorizadas e canceladas):'
                    end
                    object SpeedButton21: TSpeedButton
                      Left = 615
                      Top = 19
                      Width = 21
                      Height = 21
                      Caption = '...'
                      OnClick = SpeedButton21Click
                    end
                    object Label294: TLabel
                      Left = 8
                      Top = 44
                      Width = 318
                      Height = 13
                      Caption = 
                        'Diret'#243'rio das CT-e recuperadas da web (autorizadas e canceladas)' +
                        ':'
                    end
                    object SpeedButton24: TSpeedButton
                      Left = 615
                      Top = 59
                      Width = 21
                      Height = 21
                      Caption = '...'
                      OnClick = SpeedButton24Click
                    end
                    object EdDirCTeProt: TdmkEdit
                      Left = 8
                      Top = 19
                      Width = 605
                      Height = 21
                      TabOrder = 0
                      FormatType = dmktfString
                      MskType = fmtNone
                      DecimalSize = 0
                      LeftZeros = 0
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ValMin = '-2147483647'
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      QryCampo = 'DirCTeProt'
                      UpdCampo = 'DirCTeProt'
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = ''
                      ValWarn = False
                    end
                    object EdDirCTeRWeb: TdmkEdit
                      Left = 8
                      Top = 59
                      Width = 605
                      Height = 21
                      TabOrder = 1
                      FormatType = dmktfString
                      MskType = fmtNone
                      DecimalSize = 0
                      LeftZeros = 0
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ValMin = '-2147483647'
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      QryCampo = 'DirCTeRWeb'
                      UpdCampo = 'DirCTeRWeb'
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = ''
                      ValWarn = False
                    end
                  end
                end
                object GroupBox46: TGroupBox
                  Left = 0
                  Top = 0
                  Width = 121
                  Height = 152
                  Align = alLeft
                  Caption = ' Conting'#234'ncia SCAN: '
                  TabOrder = 0
                  object Panel69: TPanel
                    Left = 2
                    Top = 15
                    Width = 117
                    Height = 135
                    Align = alClient
                    BevelOuter = bvNone
                    TabOrder = 0
                    object Label295: TLabel
                      Left = 8
                      Top = 4
                      Width = 24
                      Height = 13
                      Caption = 'S'#233'rie'
                    end
                    object Label296: TLabel
                      Left = 8
                      Top = 44
                      Width = 55
                      Height = 13
                      Caption = #218'ltima CT-e'
                    end
                    object EdCTeSCAN_Ser: TdmkEdit
                      Left = 8
                      Top = 20
                      Width = 80
                      Height = 21
                      Alignment = taRightJustify
                      TabOrder = 0
                      FormatType = dmktfInteger
                      MskType = fmtNone
                      DecimalSize = 0
                      LeftZeros = 0
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ValMin = '900'
                      ValMax = '999'
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      Texto = '900'
                      QryCampo = 'CTeSCAN_Ser'
                      UpdCampo = 'CTeSCAN_Ser'
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = 900
                      ValWarn = False
                    end
                    object EdCTeSCAN_nCT: TdmkEdit
                      Left = 8
                      Top = 60
                      Width = 80
                      Height = 21
                      Alignment = taRightJustify
                      TabOrder = 1
                      FormatType = dmktfInteger
                      MskType = fmtNone
                      DecimalSize = 0
                      LeftZeros = 0
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ValMax = '0'
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      Texto = '0'
                      QryCampo = 'CTeSCAN_nCT'
                      UpdCampo = 'CTeSCAN_nCT'
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = 0
                      ValWarn = False
                    end
                  end
                end
              end
              object Panel70: TPanel
                Left = 0
                Top = 0
                Width = 1038
                Height = 125
                Align = alTop
                BevelOuter = bvNone
                TabOrder = 2
                object GroupBox47: TGroupBox
                  Left = 0
                  Top = 0
                  Width = 769
                  Height = 125
                  Align = alLeft
                  TabOrder = 0
                  object Panel71: TPanel
                    Left = 2
                    Top = 15
                    Width = 765
                    Height = 108
                    Align = alClient
                    BevelOuter = bvNone
                    TabOrder = 0
                    object Label297: TLabel
                      Left = 8
                      Top = 4
                      Width = 69
                      Height = 13
                      Caption = 'UF da SEFAZ:'
                    end
                    object Label299: TLabel
                      Left = 8
                      Top = 28
                      Width = 83
                      Height = 13
                      Caption = 'WebService [F4]:'
                    end
                    object Label300: TLabel
                      Left = 7
                      Top = 56
                      Width = 223
                      Height = 13
                      Caption = 'N'#250'mero de S'#233'rie do Certificado Digital da CT-e:'
                    end
                    object SpeedButton27: TSpeedButton
                      Left = 736
                      Top = 52
                      Width = 21
                      Height = 21
                      Caption = '...'
                      OnClick = SpeedButton27Click
                    end
                    object Label301: TLabel
                      Left = 8
                      Top = 80
                      Width = 181
                      Height = 13
                      Caption = 'Data da validade do certificado digital:'
                    end
                    object Label302: TLabel
                      Left = 336
                      Top = 80
                      Width = 268
                      Height = 13
                      Caption = 'Avisar a expira'#231#227'o da validade do certificado digital com '
                    end
                    object Label303: TLabel
                      Left = 652
                      Top = 80
                      Width = 105
                      Height = 13
                      Caption = 'dias de anteced'#234'ncia.'
                    end
                    object Label435: TLabel
                      Left = 160
                      Top = 4
                      Width = 114
                      Height = 13
                      Caption = 'WebService EPEC [F4]:'
                    end
                    object Label437: TLabel
                      Left = 160
                      Top = 28
                      Width = 81
                      Height = 13
                      Caption = 'UF conting'#234'ncia:'
                    end
                    object EdCTeUF_WebServ: TdmkEdit
                      Left = 108
                      Top = 0
                      Width = 40
                      Height = 21
                      CharCase = ecUpperCase
                      TabOrder = 0
                      FormatType = dmktfString
                      MskType = fmtNone
                      DecimalSize = 0
                      LeftZeros = 0
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      Texto = 'PR'
                      QryCampo = 'CTeUF_WebServ'
                      UpdCampo = 'CTeUF_WebServ'
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = 'PR'
                      ValWarn = False
                    end
                    object EdCTeUF_Servico: TdmkEdit
                      Left = 108
                      Top = 24
                      Width = 40
                      Height = 21
                      CharCase = ecUpperCase
                      TabOrder = 1
                      FormatType = dmktfString
                      MskType = fmtNone
                      DecimalSize = 0
                      LeftZeros = 0
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      Texto = 'PR'
                      QryCampo = 'CTeUF_Servico'
                      UpdCampo = 'CTeUF_Servico'
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = 'PR'
                      ValWarn = False
                      OnKeyDown = EdCTeUF_ServicoKeyDown
                    end
                    object EdCTeSerNum: TdmkEdit
                      Left = 240
                      Top = 52
                      Width = 493
                      Height = 21
                      MaxLength = 15
                      TabOrder = 2
                      FormatType = dmktfString
                      MskType = fmtNone
                      DecimalSize = 0
                      LeftZeros = 0
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      QryCampo = 'CTeSerNum'
                      UpdCampo = 'CTeSerNum'
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = ''
                      ValWarn = False
                    end
                    object TPCTeSerVal: TdmkEditDateTimePicker
                      Left = 200
                      Top = 76
                      Width = 112
                      Height = 21
                      Date = 0.669258657406317100
                      Time = 0.669258657406317100
                      TabOrder = 3
                      ReadOnly = False
                      DefaultEditMask = '!99/99/99;1;_'
                      AutoApplyEditMask = True
                      QryCampo = 'CTeSerVal'
                      UpdCampo = 'CTeSerVal'
                      UpdType = utYes
                      DatePurpose = dmkdpNone
                    end
                    object EdCTeSerAvi: TdmkEdit
                      Left = 608
                      Top = 76
                      Width = 40
                      Height = 21
                      Alignment = taRightJustify
                      TabOrder = 4
                      FormatType = dmktfInteger
                      MskType = fmtNone
                      DecimalSize = 0
                      LeftZeros = 0
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      Texto = '30'
                      QryCampo = 'CTeSerAvi'
                      UpdCampo = 'CTeSerAvi'
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = 30
                      ValWarn = False
                    end
                    object EdCTeUF_EPEC: TdmkEdit
                      Left = 280
                      Top = 0
                      Width = 52
                      Height = 21
                      CharCase = ecUpperCase
                      TabOrder = 5
                      FormatType = dmktfString
                      MskType = fmtNone
                      DecimalSize = 0
                      LeftZeros = 0
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      Texto = 'SVSP'
                      QryCampo = 'CTeUF_EPEC'
                      UpdCampo = 'CTeUF_EPEC'
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = 'SVSP'
                      ValWarn = False
                      OnKeyDown = EdCTeUF_EPECKeyDown
                    end
                    object EdCTeUF_Conting: TdmkEdit
                      Left = 280
                      Top = 24
                      Width = 52
                      Height = 21
                      CharCase = ecUpperCase
                      TabOrder = 6
                      FormatType = dmktfString
                      MskType = fmtNone
                      DecimalSize = 0
                      LeftZeros = 0
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      Texto = 'SP'
                      QryCampo = 'CTeUF_Conting'
                      UpdCampo = 'CTeUF_Conting'
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = 'SP'
                      ValWarn = False
                    end
                  end
                end
                object GroupBox48: TGroupBox
                  Left = 769
                  Top = 0
                  Width = 269
                  Height = 125
                  Align = alClient
                  Caption = ' WebServices espec'#237'ficos: '
                  TabOrder = 1
                  object Panel72: TPanel
                    Left = 2
                    Top = 15
                    Width = 265
                    Height = 108
                    Align = alClient
                    BevelOuter = bvNone
                    TabOrder = 0
                    object Label304: TLabel
                      Left = 8
                      Top = 4
                      Width = 139
                      Height = 13
                      Caption = 'Manifesta'#231#227'o do destinat'#225'rio:'
                      Enabled = False
                    end
                    object Label305: TLabel
                      Left = 8
                      Top = 28
                      Width = 124
                      Height = 13
                      Caption = 'Consulta CT-e destinadas:'
                      Enabled = False
                    end
                    object Label306: TLabel
                      Left = 8
                      Top = 52
                      Width = 77
                      Height = 13
                      Caption = 'Download CT-e:'
                      Enabled = False
                    end
                    object dmkEdit9: TdmkEdit
                      Left = 152
                      Top = 0
                      Width = 40
                      Height = 21
                      CharCase = ecUpperCase
                      Enabled = False
                      TabOrder = 0
                      FormatType = dmktfString
                      MskType = fmtNone
                      DecimalSize = 0
                      LeftZeros = 0
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      Texto = 'PR'
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = 'PR'
                      ValWarn = False
                    end
                    object dmkEdit10: TdmkEdit
                      Left = 152
                      Top = 24
                      Width = 40
                      Height = 21
                      CharCase = ecUpperCase
                      Enabled = False
                      TabOrder = 1
                      FormatType = dmktfString
                      MskType = fmtNone
                      DecimalSize = 0
                      LeftZeros = 0
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      Texto = 'PR'
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = 'PR'
                      ValWarn = False
                    end
                    object EdUF_MDeCTe: TdmkEdit
                      Left = 152
                      Top = 48
                      Width = 40
                      Height = 21
                      CharCase = ecUpperCase
                      Enabled = False
                      TabOrder = 2
                      FormatType = dmktfString
                      MskType = fmtNone
                      DecimalSize = 0
                      LeftZeros = 0
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      Texto = 'PR'
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = 'PR'
                      ValWarn = False
                    end
                  end
                end
              end
            end
          end
          object TabSheet40: TTabSheet
            Caption = ' Diret'#243'rios de arquivos XML [1] '
            ImageIndex = 1
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object Panel73: TPanel
              Left = 0
              Top = 0
              Width = 1041
              Height = 440
              Align = alClient
              BevelOuter = bvNone
              ParentBackground = False
              TabOrder = 0
              object Label307: TLabel
                Left = 8
                Top = 84
                Width = 190
                Height = 13
                Caption = 'Diret'#243'rio dos lotes gerados (-env-lot.xml):'
              end
              object Label308: TLabel
                Left = 8
                Top = 124
                Width = 246
                Height = 13
                Caption = 'Diret'#243'rio das respostas dos lotes enviados (-rec.xml):'
              end
              object Label309: TLabel
                Left = 8
                Top = 204
                Width = 342
                Height = 13
                Caption = 
                  'Diret'#243'rio dos resultados de processamento de lote de CT-e (-pro-' +
                  'rec.xml):'
              end
              object SbDirCTePedRec: TSpeedButton
                Left = 739
                Top = 140
                Width = 21
                Height = 21
                Caption = '...'
              end
              object SbDirCTeEnvLot: TSpeedButton
                Left = 739
                Top = 100
                Width = 21
                Height = 21
                Caption = '...'
              end
              object SbDirCTeProRec: TSpeedButton
                Left = 739
                Top = 220
                Width = 21
                Height = 21
                Caption = '...'
              end
              object Label310: TLabel
                Left = 8
                Top = 44
                Width = 188
                Height = 13
                Caption = 'Diret'#243'rio das CT-e assinadas (-CTe.xml):'
              end
              object SBDirCTeAss: TSpeedButton
                Left = 739
                Top = 60
                Width = 21
                Height = 21
                Caption = '...'
              end
              object Label311: TLabel
                Left = 8
                Top = 4
                Width = 179
                Height = 13
                Caption = 'Diret'#243'rio das CT-e geradas (-CTe.xml):'
              end
              object SBDirCTeGer: TSpeedButton
                Left = 739
                Top = 20
                Width = 21
                Height = 21
                Caption = '...'
              end
              object Label312: TLabel
                Left = 8
                Top = 244
                Width = 192
                Height = 13
                Caption = 'Diret'#243'rio das CT-e denegadas (-den.xml):'
              end
              object SpeedButton31: TSpeedButton
                Left = 739
                Top = 260
                Width = 21
                Height = 21
                Caption = '...'
              end
              object Label313: TLabel
                Left = 8
                Top = 164
                Width = 400
                Height = 13
                Caption = 
                  'Diret'#243'rio dos pedidos de resultado de processamento de lotes de ' +
                  'CT-e (-ped-rec.xml):'
              end
              object SpeedButton32: TSpeedButton
                Left = 739
                Top = 180
                Width = 21
                Height = 21
                Caption = '...'
              end
              object EdDirCTeEnvLot: TdmkEdit
                Left = 8
                Top = 100
                Width = 732
                Height = 21
                TabOrder = 2
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                QryCampo = 'DirCTeEnvLot'
                UpdCampo = 'DirCTeEnvLot'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object EdDirCTeRec: TdmkEdit
                Left = 8
                Top = 140
                Width = 732
                Height = 21
                TabOrder = 3
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                QryCampo = 'DirCTeRec'
                UpdCampo = 'DirCTeRec'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object EdDirCTeProRec: TdmkEdit
                Left = 8
                Top = 220
                Width = 732
                Height = 21
                TabOrder = 5
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                QryCampo = 'DirCTeProRec'
                UpdCampo = 'DirCTeProRec'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object EdDirCTeAss: TdmkEdit
                Left = 8
                Top = 60
                Width = 732
                Height = 21
                TabOrder = 1
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                QryCampo = 'DirCTeAss'
                UpdCampo = 'DirCTeAss'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object EdDirCTeGer: TdmkEdit
                Left = 8
                Top = 20
                Width = 732
                Height = 21
                TabOrder = 0
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                QryCampo = 'DirCTeGer'
                UpdCampo = 'DirCTeGer'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object EdDirCTeDen: TdmkEdit
                Left = 8
                Top = 260
                Width = 732
                Height = 21
                TabOrder = 6
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                QryCampo = 'DirCTeDen'
                UpdCampo = 'DirCTeDen'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object EdDirCTePedRec: TdmkEdit
                Left = 8
                Top = 180
                Width = 732
                Height = 21
                TabOrder = 4
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                QryCampo = 'DirCTePedRec'
                UpdCampo = 'DirCTePedRec'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object BtCTeDefaultDirs: TBitBtn
                Left = 8
                Top = 287
                Width = 753
                Height = 40
                Cursor = crHandPoint
                Caption = 
                  'Sugest'#227'o de diret'#243'rios para os arquivos XML [Geral], [1], [2] e ' +
                  '[3] (somente na altera'#231#227'o do registro, e somente para n'#227'o defini' +
                  'dos)'
                Enabled = False
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -12
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                NumGlyphs = 2
                ParentFont = False
                ParentShowHint = False
                ShowHint = True
                TabOrder = 7
                OnClick = BtCTeDefaultDirsClick
              end
            end
          end
          object TabSheet41: TTabSheet
            Caption = ' Diret'#243'rios de arquivos XML [2] '
            ImageIndex = 2
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object Panel74: TPanel
              Left = 0
              Top = 0
              Width = 1041
              Height = 440
              Align = alClient
              BevelOuter = bvNone
              ParentBackground = False
              TabOrder = 0
              object Label314: TLabel
                Left = 8
                Top = 84
                Width = 336
                Height = 13
                Caption = 
                  'Diret'#243'rio dos pedidos de inutiliza'#231#227'o de n'#250'meros de CT-e (-ped-i' +
                  'nu.xml):'
              end
              object Label315: TLabel
                Left = 8
                Top = 124
                Width = 323
                Height = 13
                Caption = 
                  'Diret'#243'rio das respostas de inutiliza'#231#227'o de n'#250'meros de CT-e (-inu' +
                  '.xml):'
              end
              object Label316: TLabel
                Left = 8
                Top = 164
                Width = 346
                Height = 13
                Caption = 
                  'Diret'#243'rio dos pedidos de consulta da situa'#231#227'o atual da CT-e (-pe' +
                  'd-sit.xml):'
              end
              object Label317: TLabel
                Left = 8
                Top = 204
                Width = 333
                Height = 13
                Caption = 
                  'Diret'#243'rio das respostas de consulta da situa'#231#227'o atual da CT-e (-' +
                  'sit.xml):'
              end
              object Label318: TLabel
                Left = 8
                Top = 244
                Width = 323
                Height = 13
                Caption = 
                  'Diret'#243'rio dos pedidos de consulta do status do servi'#231'o (-ped-sta' +
                  '.xml):'
              end
              object SbDirCTePedSta: TSpeedButton
                Left = 739
                Top = 260
                Width = 21
                Height = 21
                Caption = '...'
              end
              object Label319: TLabel
                Left = 8
                Top = 284
                Width = 310
                Height = 13
                Caption = 
                  'Diret'#243'rio das respostas de consulta do status do servi'#231'o (-sta.x' +
                  'ml):'
              end
              object SbDirCTeSta: TSpeedButton
                Left = 739
                Top = 300
                Width = 21
                Height = 21
                Caption = '...'
              end
              object Label320: TLabel
                Left = 8
                Top = 4
                Width = 297
                Height = 13
                Caption = 'Diret'#243'rio dos pedidos de cancelamento de CT-e (-ped-can.xml):'
              end
              object Label321: TLabel
                Left = 8
                Top = 44
                Width = 284
                Height = 13
                Caption = 'Diret'#243'rio das respostas de cancelamento de CT-e (-can.xml):'
              end
              object SBDirCTePedCan: TSpeedButton
                Left = 739
                Top = 20
                Width = 21
                Height = 21
                Caption = '...'
                Enabled = False
              end
              object SBDirCTeCan: TSpeedButton
                Left = 739
                Top = 60
                Width = 21
                Height = 21
                Caption = '...'
                Enabled = False
              end
              object SbDirCTePedInu: TSpeedButton
                Left = 739
                Top = 100
                Width = 21
                Height = 21
                Caption = '...'
              end
              object SbDirCTeInu: TSpeedButton
                Left = 739
                Top = 140
                Width = 21
                Height = 21
                Caption = '...'
              end
              object SbDirCTePedSit: TSpeedButton
                Left = 739
                Top = 180
                Width = 21
                Height = 21
                Caption = '...'
              end
              object SBDirCTeSit: TSpeedButton
                Left = 739
                Top = 220
                Width = 21
                Height = 21
                Caption = '...'
              end
              object EdDirCTePedSta: TdmkEdit
                Left = 8
                Top = 260
                Width = 732
                Height = 21
                TabOrder = 6
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                QryCampo = 'DirCTePedSta'
                UpdCampo = 'DirCTePedSta'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object EdDirCTeSta: TdmkEdit
                Left = 8
                Top = 300
                Width = 732
                Height = 21
                TabOrder = 7
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                QryCampo = 'DirCTeSta'
                UpdCampo = 'DirCTeSta'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object EdDirCTePedCan: TdmkEdit
                Left = 8
                Top = 20
                Width = 732
                Height = 21
                TabOrder = 0
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                QryCampo = 'DirCTePedCan'
                UpdCampo = 'DirCTePedCan'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object EdDirCTeCan: TdmkEdit
                Left = 8
                Top = 60
                Width = 732
                Height = 21
                TabOrder = 1
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                QryCampo = 'DirCTeCan'
                UpdCampo = 'DirCTeCan'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object EdDirCTePedInu: TdmkEdit
                Left = 8
                Top = 100
                Width = 732
                Height = 21
                TabOrder = 2
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                QryCampo = 'DirCTePedInu'
                UpdCampo = 'DirCTePedInu'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object EdDirCTeInu: TdmkEdit
                Left = 8
                Top = 140
                Width = 732
                Height = 21
                TabOrder = 3
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                QryCampo = 'DirCTeInu'
                UpdCampo = 'DirCTeInu'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object EdDirCTePedSit: TdmkEdit
                Left = 8
                Top = 180
                Width = 732
                Height = 21
                TabOrder = 4
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                QryCampo = 'DirCTePedSit'
                UpdCampo = 'DirCTePedSit'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object EdDirCTeSit: TdmkEdit
                Left = 8
                Top = 220
                Width = 732
                Height = 21
                TabOrder = 5
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                QryCampo = 'DirCTeSit'
                UpdCampo = 'DirCTeSit'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
            end
          end
          object TabSheet42: TTabSheet
            Caption = ' Diret'#243'rios de arquivos XML [3] '
            ImageIndex = 3
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object Panel75: TPanel
              Left = 0
              Top = 0
              Width = 1041
              Height = 440
              Align = alClient
              BevelOuter = bvNone
              ParentBackground = False
              TabOrder = 0
              object Label322: TLabel
                Left = 8
                Top = 124
                Width = 324
                Height = 13
                Caption = 
                  'Diret'#243'rio das respostas das cartas de corre'#231#227'o eletr'#244'nicas (-cce' +
                  '.xml):'
              end
              object Label323: TLabel
                Left = 8
                Top = 204
                Width = 403
                Height = 13
                Caption = 
                  'Diret'#243'rio dos pedidos de cancelamento de CT-e como evento da NF-' +
                  'e (-eve-can.xml):'
              end
              object Label324: TLabel
                Left = 8
                Top = 244
                Width = 390
                Height = 13
                Caption = 
                  'Diret'#243'rio das respostas de cancelamento de CT-e como evento da N' +
                  'F-e (-can.xml):'
              end
              object Label325: TLabel
                Left = 8
                Top = 4
                Width = 355
                Height = 13
                Caption = 
                  'Diret'#243'rio do envio dos lotes de envio de eventos da CT-e (-eve-e' +
                  'nv-lot.xml):'
              end
              object Label326: TLabel
                Left = 8
                Top = 84
                Width = 337
                Height = 13
                Caption = 
                  'Diret'#243'rio dos pedidos das cartas de corre'#231#227'o eletr'#244'nicas (-eve-c' +
                  'ce.xml):'
              end
              object Label327: TLabel
                Left = 8
                Top = 44
                Width = 356
                Height = 13
                Caption = 
                  'Diret'#243'rio do retorno dos lotes de envio de eventos da CT-e (-eve' +
                  '-ret-lot.xml):'
              end
              object SpeedButton41: TSpeedButton
                Left = 739
                Top = 20
                Width = 21
                Height = 21
                Caption = '...'
              end
              object SpeedButton42: TSpeedButton
                Left = 739
                Top = 60
                Width = 21
                Height = 21
                Caption = '...'
              end
              object SpeedButton43: TSpeedButton
                Left = 739
                Top = 100
                Width = 21
                Height = 21
                Caption = '...'
              end
              object SpeedButton44: TSpeedButton
                Left = 739
                Top = 140
                Width = 21
                Height = 21
                Caption = '...'
              end
              object SpeedButton45: TSpeedButton
                Left = 739
                Top = 220
                Width = 21
                Height = 21
                Caption = '...'
              end
              object SpeedButton46: TSpeedButton
                Left = 739
                Top = 260
                Width = 21
                Height = 21
                Caption = '...'
              end
              object Label328: TLabel
                Left = 8
                Top = 164
                Width = 435
                Height = 13
                Caption = 
                  'Diret'#243'rio de armazenamento e disposi'#231#227'o das cartas de corre'#231#227'o e' +
                  'letr'#244'nicas (-proc-cce.xml):'
              end
              object SpeedButton47: TSpeedButton
                Left = 739
                Top = 180
                Width = 21
                Height = 21
                Caption = '...'
              end
              object EdDirCTeEveEnvLot: TdmkEdit
                Left = 8
                Top = 20
                Width = 732
                Height = 21
                TabOrder = 0
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                QryCampo = 'DirCTeEveEnvLot'
                UpdCampo = 'DirCTeEveEnvLot'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object EdDirCTeEveRetLot: TdmkEdit
                Left = 8
                Top = 60
                Width = 732
                Height = 21
                TabOrder = 1
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                QryCampo = 'DirCTeEveRetLot'
                UpdCampo = 'DirCTeEveRetLot'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object EdDirCTeEvePedCCe: TdmkEdit
                Left = 8
                Top = 100
                Width = 732
                Height = 21
                TabOrder = 2
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                QryCampo = 'DirCTeEvePedCCe'
                UpdCampo = 'DirCTeEvePedCCe'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object EdDirCTeEveRetCCe: TdmkEdit
                Left = 8
                Top = 140
                Width = 732
                Height = 21
                TabOrder = 3
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                QryCampo = 'DirCTeEveRetCCe'
                UpdCampo = 'DirCTeEveRetCCe'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object EdDirCTeEvePedCan: TdmkEdit
                Left = 8
                Top = 220
                Width = 732
                Height = 21
                TabOrder = 5
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                QryCampo = 'DirCTeEvePedCan'
                UpdCampo = 'DirCTeEvePedCan'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object EdDirCTeEveRetCan: TdmkEdit
                Left = 8
                Top = 260
                Width = 732
                Height = 21
                TabOrder = 6
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                QryCampo = 'DirCTeEveRetCan'
                UpdCampo = 'DirCTeEveRetCan'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object EdDirCTEEveProcCCe: TdmkEdit
                Left = 8
                Top = 180
                Width = 732
                Height = 21
                TabOrder = 4
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                QryCampo = 'DirCTEEveProcCCe'
                UpdCampo = 'DirCTEEveProcCCe'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
            end
          end
          object TabSheet43: TTabSheet
            Caption = ' Diret'#243'rios de arquivos XML [4] '
            ImageIndex = 4
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object Panel76: TPanel
              Left = 0
              Top = 0
              Width = 1041
              Height = 440
              Align = alClient
              BevelOuter = bvNone
              ParentBackground = False
              TabOrder = 0
              object Label329: TLabel
                Left = 8
                Top = 4
                Width = 348
                Height = 13
                Caption = 
                  'Diret'#243'rio das respostas de consulta de CT-es destinadas (-ret-nf' +
                  'e-des.xml):'
              end
              object SBDirRetCTeDes: TSpeedButton
                Left = 739
                Top = 20
                Width = 21
                Height = 21
                Caption = '...'
              end
              object Label330: TLabel
                Left = 8
                Top = 44
                Width = 325
                Height = 13
                Caption = 
                  'Diret'#243'rio dos envios de manifesta'#231'oes do destinat'#225'rip (-eve-mde.' +
                  'xml):'
              end
              object Label331: TLabel
                Left = 8
                Top = 84
                Width = 318
                Height = 13
                Caption = 
                  'Diret'#243'rio das respostas de manifesta'#231#245'es do destinat'#225'rio (-mde.x' +
                  'ml):'
              end
              object SpeedButton48: TSpeedButton
                Left = 739
                Top = 60
                Width = 21
                Height = 21
                Caption = '...'
                OnClick = SbDirEvePedMDeClick
              end
              object SpeedButton49: TSpeedButton
                Left = 739
                Top = 100
                Width = 21
                Height = 21
                Caption = '...'
                OnClick = SbDirEveRetMDeClick
              end
              object Label332: TLabel
                Left = 8
                Top = 124
                Width = 291
                Height = 13
                Caption = 'Diret'#243'rio dos grupos de CT-e baixadas (-ret-dow-CTe-des.xml):'
              end
              object SbDirDowCTeDes: TSpeedButton
                Left = 739
                Top = 140
                Width = 21
                Height = 21
                Caption = '...'
              end
              object Label333: TLabel
                Left = 8
                Top = 164
                Width = 183
                Height = 13
                Caption = 'Diret'#243'rio das CT-e baixadas (-CTe.xml):'
              end
              object SbDirDowCTeCTe: TSpeedButton
                Left = 739
                Top = 180
                Width = 21
                Height = 21
                Caption = '...'
              end
              object Label334: TLabel
                Left = 8
                Top = 204
                Width = 346
                Height = 13
                Caption = 
                  'Diret'#243'rio dos envios de Emiss'#227'o em conting'#234'ncia - EPEC (-ped-epe' +
                  'c.xml):'
              end
              object Label335: TLabel
                Left = 8
                Top = 244
                Width = 347
                Height = 13
                Caption = 
                  'Diret'#243'rio dos retornos de Emiss'#227'o em conting'#234'ncia - EPEC (-ret-e' +
                  'pec.xml):'
              end
              object SbDirCTeEvePedEPEC: TSpeedButton
                Left = 739
                Top = 220
                Width = 21
                Height = 21
                Caption = '...'
                OnClick = SbDirDistDFeIntClick
              end
              object SbDirCTeEveEPEC: TSpeedButton
                Left = 739
                Top = 260
                Width = 21
                Height = 21
                Caption = '...'
                OnClick = SbDirRetDistDFeIntClick
              end
              object Label336: TLabel
                Left = 8
                Top = 284
                Width = 384
                Height = 13
                Caption = 
                  'Diret'#243'rio dos retornos de Download de CT-es confirmadas  (-ret-d' +
                  'ow-CTe-cnf.xml):'
              end
              object SbDirDowCTeCnf: TSpeedButton
                Left = 739
                Top = 300
                Width = 21
                Height = 21
                Caption = '...'
              end
              object EdDirRetCTeDes: TdmkEdit
                Left = 8
                Top = 20
                Width = 732
                Height = 21
                TabOrder = 0
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object dmkEdit31: TdmkEdit
                Left = 8
                Top = 60
                Width = 732
                Height = 21
                TabOrder = 1
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                QryCampo = 'DirEvePedMDe'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object dmkEdit32: TdmkEdit
                Left = 8
                Top = 100
                Width = 732
                Height = 21
                TabOrder = 2
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                QryCampo = 'DirEveRetMDe'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object EdDirDowCTeDes: TdmkEdit
                Left = 8
                Top = 140
                Width = 732
                Height = 21
                TabOrder = 3
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                QryCampo = 'DirDowCTeDes'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object EdDirDowCTeCTe: TdmkEdit
                Left = 8
                Top = 180
                Width = 732
                Height = 21
                TabOrder = 4
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                QryCampo = 'DirDowCTeCTe'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object EdDirCTeEvePedEPEC: TdmkEdit
                Left = 8
                Top = 220
                Width = 732
                Height = 21
                TabOrder = 5
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                QryCampo = 'DirCTeEvePedEPEC'
                UpdCampo = 'DirCTeEvePedEPEC'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object EdDirCTeEveRetEPEC: TdmkEdit
                Left = 8
                Top = 260
                Width = 732
                Height = 21
                TabOrder = 6
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                QryCampo = 'DirCTeEveRetEPEC'
                UpdCampo = 'DirCTeEveRetEPEC'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object EdDirDowCTeCnf: TdmkEdit
                Left = 8
                Top = 300
                Width = 732
                Height = 21
                TabOrder = 7
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                QryCampo = 'DirDowCTeCnf'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
            end
          end
          object TabSheet44: TTabSheet
            Caption = ' Op'#231#245'es '
            ImageIndex = 4
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object Panel77: TPanel
              Left = 0
              Top = 0
              Width = 1038
              Height = 437
              Align = alClient
              BevelOuter = bvNone
              ParentBackground = False
              TabOrder = 0
              object GroupBox49: TGroupBox
                Left = 0
                Top = 0
                Width = 484
                Height = 437
                Align = alLeft
                TabOrder = 0
                object Label337: TLabel
                  Left = 8
                  Top = 296
                  Width = 461
                  Height = 13
                  Caption = 
                    'Email pr'#243'prio para receber email enviado (mais de um separar por' +
                    ' ponte e v'#237'rgula) m'#225'x. 255 letras:'
                end
                object Label338: TLabel
                  Left = 8
                  Top = 212
                  Width = 255
                  Height = 13
                  Caption = 'Tipo de email para envio de CTe e Carta de Corre'#231#227'o:'
                end
                object SpeedButton52: TSpeedButton
                  Left = 452
                  Top = 228
                  Width = 21
                  Height = 21
                  Caption = '...'
                  OnClick = SpeedButton52Click
                end
                object Label339: TLabel
                  Left = 8
                  Top = 252
                  Width = 386
                  Height = 13
                  Caption = 
                    'Tipo de email para envio espec'#237'fico de Carta de Corre'#231#227'o:  (Ex.:' +
                    ' Transportadoras)'
                end
                object SpeedButton53: TSpeedButton
                  Left = 452
                  Top = 268
                  Width = 21
                  Height = 21
                  Caption = '...'
                  OnClick = SpeedButton53Click
                end
                object Label340: TLabel
                  Left = 6
                  Top = 338
                  Width = 66
                  Height = 13
                  Caption = 'Texto padr'#227'o:'
                end
                object SpeedButton54: TSpeedButton
                  Left = 453
                  Top = 355
                  Width = 21
                  Height = 21
                  Caption = '...'
                  OnClick = SpeedButton54Click
                end
                object GroupBox50: TGroupBox
                  Left = 2
                  Top = 15
                  Width = 480
                  Height = 106
                  Align = alTop
                  Caption = ' Dados e identifica'#231#227'o da CT-e: '
                  TabOrder = 0
                  object Label341: TLabel
                    Left = 12
                    Top = 16
                    Width = 57
                    Height = 13
                    Caption = 'Vers'#227'o: [F4]'
                  end
                  object Label342: TLabel
                    Left = 75
                    Top = 16
                    Width = 38
                    Height = 13
                    Caption = 'Modelo:'
                  end
                  object SpeedButton55: TSpeedButton
                    Left = 451
                    Top = 32
                    Width = 21
                    Height = 21
                    Caption = '...'
                    OnClick = SpeedButton55Click
                  end
                  object Label343: TLabel
                    Left = 120
                    Top = 16
                    Width = 214
                    Height = 13
                    Caption = 'Caminho dos arquivos do Schema xml (*.xsd):'
                  end
                  object EdCTeversao: TdmkEdit
                    Left = 12
                    Top = 32
                    Width = 57
                    Height = 21
                    Alignment = taRightJustify
                    TabOrder = 0
                    FormatType = dmktfDouble
                    MskType = fmtNone
                    DecimalSize = 2
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '2,00'
                    QryCampo = 'CTeversao'
                    UpdCampo = 'CTeversao'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 2.000000000000000000
                    ValWarn = False
                    OnKeyDown = EdCTeversaoKeyDown
                  end
                  object EdCTeide_mod: TdmkEdit
                    Left = 75
                    Top = 32
                    Width = 41
                    Height = 21
                    Alignment = taRightJustify
                    TabOrder = 1
                    FormatType = dmktfInteger
                    MskType = fmtNone
                    DecimalSize = 2
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '57'
                    QryCampo = 'CTeide_mod'
                    UpdCampo = 'CTeide_mod'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 57
                    ValWarn = False
                  end
                  object RGCTeide_tpAmb: TdmkRadioGroup
                    Left = 12
                    Top = 56
                    Width = 461
                    Height = 45
                    Caption = ' Identifica'#231#227'o do Ambiente: '
                    Columns = 3
                    ItemIndex = 0
                    Items.Strings = (
                      '0 - Nenhum'
                      '1 - Produ'#231#227'o'
                      '2 - Homologa'#231#227'o')
                    TabOrder = 3
                    QryCampo = 'CTeide_tpAmb'
                    UpdCampo = 'CTeide_tpAmb'
                    UpdType = utYes
                    OldValor = 0
                  end
                  object EdDirCTeSchema: TdmkEdit
                    Left = 120
                    Top = 32
                    Width = 328
                    Height = 21
                    TabOrder = 2
                    FormatType = dmktfString
                    MskType = fmtNone
                    DecimalSize = 0
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ValMin = '-2147483647'
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    QryCampo = 'DirCTeSchema'
                    UpdCampo = 'DirCTeSchema'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = ''
                    ValWarn = False
                  end
                end
                object RGCTeAppCode: TdmkRadioGroup
                  Left = 2
                  Top = 121
                  Width = 480
                  Height = 44
                  Align = alTop
                  Caption = ' Gerenciamento CT-e: '
                  Columns = 2
                  Enabled = False
                  ItemIndex = 0
                  Items.Strings = (
                    'Pascal'
                    'C++')
                  TabOrder = 1
                  QryCampo = 'CTeAppCode'
                  UpdCampo = 'CTeAppCode'
                  UpdType = utYes
                  OldValor = 0
                end
                object RGCTeAssDigMode: TdmkRadioGroup
                  Left = 2
                  Top = 165
                  Width = 480
                  Height = 44
                  Align = alTop
                  Caption = ' Modo de assinar a CT-e no Pascal: '
                  Columns = 2
                  Enabled = False
                  ItemIndex = 0
                  Items.Strings = (
                    'CAPICOM.DLL'
                    '.NET FRAMEWORK')
                  TabOrder = 2
                  QryCampo = 'CTeAssDigMode'
                  UpdCampo = 'CTeAssDigMode'
                  UpdType = utYes
                  OldValor = 0
                end
                object CBCTeEntiTipCto: TdmkDBLookupComboBox
                  Left = 68
                  Top = 228
                  Width = 383
                  Height = 21
                  KeyField = 'CodUsu'
                  ListField = 'Nome'
                  ListSource = DsCTeEntiTipCto
                  TabOrder = 4
                  dmkEditCB = EdCTeEntiTipCto
                  UpdType = utYes
                  LocF7SQLMasc = '$#'
                end
                object EdMyEmailCTe: TdmkEdit
                  Left = 8
                  Top = 312
                  Width = 465
                  Height = 21
                  CharCase = ecLowerCase
                  MaxLength = 255
                  TabOrder = 7
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = True
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  QryCampo = 'MyEmailCTe'
                  UpdCampo = 'MyEmailCTe'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = ''
                  ValWarn = False
                end
                object EdCTeEntiTipCto: TdmkEditCB
                  Left = 8
                  Top = 228
                  Width = 56
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 3
                  FormatType = dmktfInteger
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ValMin = '-2147483647'
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0
                  ValWarn = False
                  DBLookupComboBox = CBCTeEntiTipCto
                  IgnoraDBLookupComboBox = False
                  AutoSetIfOnlyOneReg = setregOnlyManual
                end
                object EdCTeEntiTipCt1: TdmkEditCB
                  Left = 8
                  Top = 268
                  Width = 56
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 5
                  FormatType = dmktfInteger
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ValMin = '-2147483647'
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0
                  ValWarn = False
                  DBLookupComboBox = CBCTeEntiTipCt1
                  IgnoraDBLookupComboBox = False
                  AutoSetIfOnlyOneReg = setregOnlyManual
                end
                object CBCTeEntiTipCt1: TdmkDBLookupComboBox
                  Left = 68
                  Top = 268
                  Width = 383
                  Height = 21
                  KeyField = 'CodUsu'
                  ListField = 'Nome'
                  ListSource = DsCTeEntiTipCt1
                  TabOrder = 6
                  dmkEditCB = EdCTeEntiTipCt1
                  UpdType = utYes
                  LocF7SQLMasc = '$#'
                end
                object EdCTeInfCpl: TdmkEditCB
                  Left = 8
                  Top = 355
                  Width = 56
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 8
                  FormatType = dmktfInteger
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ValMin = '-2147483647'
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0'
                  QryCampo = 'CTeInfCpl'
                  UpdCampo = 'CTeInfCpl'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0
                  ValWarn = False
                  DBLookupComboBox = CBCTeInfCpl
                  IgnoraDBLookupComboBox = False
                  AutoSetIfOnlyOneReg = setregOnlyManual
                end
                object CBCTeInfCpl: TdmkDBLookupComboBox
                  Left = 68
                  Top = 355
                  Width = 383
                  Height = 21
                  KeyField = 'Codigo'
                  ListField = 'Nome'
                  TabOrder = 9
                  dmkEditCB = EdCTeInfCpl
                  QryCampo = 'CTeInfCpl'
                  UpdType = utYes
                  LocF7SQLMasc = '$#'
                end
              end
              object GroupBox53: TGroupBox
                Left = 484
                Top = 0
                Width = 240
                Height = 437
                Align = alLeft
                Caption = 'Vers'#227'o do servi'#231'o'
                TabOrder = 1
                object Label348: TLabel
                  Left = 5
                  Top = 17
                  Width = 106
                  Height = 13
                  Caption = 'Status do servi'#231'o: [F4]'
                end
                object Label349: TLabel
                  Left = 5
                  Top = 40
                  Width = 188
                  Height = 13
                  Caption = 'Enviar lote de CT-e assincrono ao fisco:'
                end
                object Label350: TLabel
                  Left = 5
                  Top = 88
                  Width = 129
                  Height = 13
                  Caption = 'Consultar lote enviado: [F4]'
                end
                object Label351: TLabel
                  Left = 5
                  Top = 112
                  Width = 159
                  Height = 13
                  Caption = 'Pedir cancelamento de CT-e: [F4]'
                  Enabled = False
                end
                object Label352: TLabel
                  Left = 5
                  Top = 136
                  Width = 187
                  Height = 13
                  Caption = 'Pedir inutiliza'#231#227'o de n'#250'mero(s) de CT-e:'
                end
                object Label353: TLabel
                  Left = 5
                  Top = 160
                  Width = 94
                  Height = 13
                  Caption = 'Consultar CT-e: [F4]'
                end
                object Label354: TLabel
                  Left = 5
                  Top = 184
                  Width = 168
                  Height = 13
                  Caption = 'Enviar lote de eventos da CTe: [F4]'
                end
                object Label355: TLabel
                  Left = 5
                  Top = 64
                  Width = 177
                  Height = 13
                  Caption = 'Enviar lote de CT-e sincrono ao fisco:'
                  Enabled = False
                end
                object Label356: TLabel
                  Left = 5
                  Top = 208
                  Width = 186
                  Height = 13
                  Caption = 'Consultar cadastro de Contribuinte: [F4]'
                  Enabled = False
                end
                object Label357: TLabel
                  Left = 5
                  Top = 232
                  Width = 120
                  Height = 13
                  Caption = 'Distribui'#231#227'o de DF-e: [F4]'
                  Enabled = False
                end
                object Label358: TLabel
                  Left = 5
                  Top = 256
                  Width = 178
                  Height = 13
                  Caption = 'Download de CT-es confirmadas: [F4]'
                  Enabled = False
                end
                object Label434: TLabel
                  Left = 5
                  Top = 280
                  Width = 169
                  Height = 13
                  Caption = 'Emiss'#227'o em contig'#234'ncia EPEC: [F4]'
                end
                object EdCTeVerStaSer: TdmkEdit
                  Left = 193
                  Top = 12
                  Width = 40
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 0
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 2
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '2,00'
                  QryCampo = 'CTeVerStaSer'
                  UpdCampo = 'CTeVerStaSer'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 2.000000000000000000
                  ValWarn = False
                end
                object EdCTeVerEnvLot: TdmkEdit
                  Left = 193
                  Top = 36
                  Width = 40
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 1
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 2
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '2,00'
                  QryCampo = 'CTeVerEnvLot'
                  UpdCampo = 'CTeVerEnvLot'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 2.000000000000000000
                  ValWarn = False
                end
                object EdCTeVerConLot: TdmkEdit
                  Left = 193
                  Top = 84
                  Width = 40
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 3
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 2
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '2,00'
                  QryCampo = 'CTeVerConLot'
                  UpdCampo = 'CTeVerConLot'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 2.000000000000000000
                  ValWarn = False
                end
                object EdCTeVerCanCTe: TdmkEdit
                  Left = 193
                  Top = 108
                  Width = 40
                  Height = 21
                  Alignment = taRightJustify
                  Enabled = False
                  TabOrder = 4
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 2
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '2,00'
                  QryCampo = 'CTeVerCanCTe'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 2.000000000000000000
                  ValWarn = False
                end
                object EdCTeVerInuNum: TdmkEdit
                  Left = 193
                  Top = 132
                  Width = 40
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 5
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 2
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '2,00'
                  QryCampo = 'CTeVerInuNum'
                  UpdCampo = 'CTeVerInuNum'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 2.000000000000000000
                  ValWarn = False
                end
                object EdCTeVerConCTe: TdmkEdit
                  Left = 193
                  Top = 156
                  Width = 40
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 6
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 2
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '2,00'
                  QryCampo = 'CTeVerConCTe'
                  UpdCampo = 'CTeVerConCTe'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 2.000000000000000000
                  ValWarn = False
                end
                object EdCTeVerLotEve: TdmkEdit
                  Left = 193
                  Top = 180
                  Width = 40
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 7
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 2
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '2,00'
                  QryCampo = 'CTeVerLotEve'
                  UpdCampo = 'CTeVerLotEve'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 2.000000000000000000
                  ValWarn = False
                end
                object EdCTeVerEnvLotSinc: TdmkEdit
                  Left = 193
                  Top = 60
                  Width = 40
                  Height = 21
                  Alignment = taRightJustify
                  Enabled = False
                  TabOrder = 2
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 2
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0,00'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  ValWarn = False
                end
                object EdCTeVerConsCad: TdmkEdit
                  Left = 193
                  Top = 204
                  Width = 40
                  Height = 21
                  Alignment = taRightJustify
                  Enabled = False
                  TabOrder = 8
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 2
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '2,00'
                  QryCampo = 'CTeVerConsCad'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 2.000000000000000000
                  ValWarn = False
                end
                object EdCTeVerDistDFeInt: TdmkEdit
                  Left = 193
                  Top = 228
                  Width = 40
                  Height = 21
                  Alignment = taRightJustify
                  Enabled = False
                  TabOrder = 9
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 2
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '2,00'
                  QryCampo = 'CTeVerDistDFeInt'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 2.000000000000000000
                  ValWarn = False
                end
                object EdCTeVerDowCTe: TdmkEdit
                  Left = 193
                  Top = 252
                  Width = 40
                  Height = 21
                  Alignment = taRightJustify
                  Enabled = False
                  TabOrder = 10
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 2
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '3,10'
                  QryCampo = 'CTeVerDowCTe'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 3.100000000000000000
                  ValWarn = False
                end
                object EdCTeVerEPEC: TdmkEdit
                  Left = 193
                  Top = 276
                  Width = 40
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 11
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 2
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '2,00'
                  QryCampo = 'CTeVerEPEC'
                  UpdCampo = 'CTeVerEPEC'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 2.000000000000000000
                  ValWarn = False
                end
              end
            end
          end
          object TabSheet45: TTabSheet
            Caption = ' DACTe '
            ImageIndex = 5
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object Panel78: TPanel
              Left = 0
              Top = 0
              Width = 1038
              Height = 437
              Align = alClient
              BevelOuter = bvNone
              ParentBackground = False
              TabOrder = 0
              object Label359: TLabel
                Left = 12
                Top = 196
                Width = 164
                Height = 13
                Caption = 'URL Emitente (n'#227'o recomendado):'
              end
              object Label360: TLabel
                Left = 12
                Top = 236
                Width = 245
                Height = 13
                Caption = 'Arquivo do logo da empresa (Impress'#227'o no DACTe):'
              end
              object SpeedButton56: TSpeedButton
                Left = 743
                Top = 252
                Width = 21
                Height = 21
                Caption = '...'
                OnClick = SpeedButton56Click
              end
              object Label361: TLabel
                Left = 12
                Top = 276
                Width = 105
                Height = 13
                Caption = 'Diret'#243'rio das DACTes:'
              end
              object SpeedButton57: TSpeedButton
                Left = 743
                Top = 292
                Width = 21
                Height = 21
                Caption = '...'
                OnClick = SpeedButton57Click
              end
              object RGCTeide_tpImp: TdmkRadioGroup
                Left = 8
                Top = 4
                Width = 460
                Height = 45
                Caption = ' Formato de impress'#227'o: '
                Columns = 3
                ItemIndex = 0
                Items.Strings = (
                  '0 - Nenhum'
                  '1 - Retrato'
                  '2 - Paisagem')
                TabOrder = 0
                QryCampo = 'CTeide_tpImp'
                UpdCampo = 'CTeide_tpImp'
                UpdType = utYes
                OldValor = 0
              end
              object RGCTeItsLin: TdmkRadioGroup
                Left = 8
                Top = 52
                Width = 460
                Height = 45
                Caption = ' Linhas por item na DACTe: '
                Columns = 4
                ItemIndex = 0
                Items.Strings = (
                  '3'
                  '1'
                  '2'
                  '3')
                TabOrder = 1
                QryCampo = 'CTeItsLin'
                UpdCampo = 'CTeItsLin'
                UpdType = utYes
                OldValor = 0
              end
              object GroupBox54: TGroupBox
                Left = 8
                Top = 100
                Width = 460
                Height = 65
                Caption = ' Tamanho da Fonte: '
                TabOrder = 2
                object Label362: TLabel
                  Left = 12
                  Top = 20
                  Width = 66
                  Height = 13
                  Caption = 'Raz'#227'o Social:'
                end
                object Label363: TLabel
                  Left = 152
                  Top = 20
                  Width = 49
                  Height = 13
                  Caption = 'Endere'#231'o:'
                end
                object Label364: TLabel
                  Left = 292
                  Top = 20
                  Width = 45
                  Height = 13
                  Caption = 'Telefone:'
                end
                object EdCTeFTRazao: TdmkEdit
                  Left = 12
                  Top = 36
                  Width = 134
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 0
                  FormatType = dmktfLongint
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0'
                  QryCampo = 'CTeFTRazao'
                  UpdCampo = 'CTeFTRazao'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0
                  ValWarn = False
                end
                object EdCTeFTEnder: TdmkEdit
                  Left = 152
                  Top = 36
                  Width = 134
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 1
                  FormatType = dmktfLongint
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0'
                  QryCampo = 'CTeFTEnder'
                  UpdCampo = 'CTeFTEnder'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0
                  ValWarn = False
                end
                object EdCTeFTFones: TdmkEdit
                  Left = 292
                  Top = 36
                  Width = 134
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 2
                  FormatType = dmktfLongint
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0'
                  QryCampo = 'CTeFTFones'
                  UpdCampo = 'CTeFTFones'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0
                  ValWarn = False
                end
              end
              object CkCTeMaiusc: TdmkCheckBox
                Left = 12
                Top = 172
                Width = 169
                Height = 17
                Caption = 'For'#231'ar texto a ficar mai'#250'sculo.'
                TabOrder = 3
                QryCampo = 'CTeMaiusc'
                UpdCampo = 'CTeMaiusc'
                UpdType = utYes
                ValCheck = '1'
                ValUncheck = '0'
                OldValor = #0
              end
              object EdCTeShowURL: TdmkEdit
                Left = 12
                Top = 212
                Width = 749
                Height = 21
                TabOrder = 4
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                QryCampo = 'CTeShowURL'
                UpdCampo = 'CTeShowURL'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object EdPathLogoCTe: TdmkEdit
                Left = 12
                Top = 252
                Width = 732
                Height = 21
                TabOrder = 5
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                QryCampo = 'PathLogoCTe'
                UpdCampo = 'PathLogoCTe'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object EdDirDACTes: TdmkEdit
                Left = 12
                Top = 292
                Width = 732
                Height = 21
                TabOrder = 6
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                QryCampo = 'DirDACTes'
                UpdCampo = 'DirDACTes'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object RG_CTe_indFinalCpl: TdmkRadioGroup
                Left = 10
                Top = 317
                Width = 505
                Height = 39
                Caption = 
                  ' Informa'#231#227'o dos valores totais aproximados de impostos no campo ' +
                  '"Dados Adicionais" nas CT-es: '
                Columns = 3
                ItemIndex = 0
                Items.Strings = (
                  'Indefinido'
                  'Somente consumidor final'
                  'Todas CT-es')
                TabOrder = 7
                QryCampo = 'CTe_indFinalCpl'
                UpdCampo = 'CTe_indFinalCpl'
                UpdType = utYes
                OldValor = 0
              end
            end
          end
        end
      end
      object TabSheet48: TTabSheet
        Caption = 'MDF-e'
        ImageIndex = 9
        object PageControl8: TPageControl
          Left = 0
          Top = 0
          Width = 1046
          Height = 465
          ActivePage = TabSheet52
          Align = alClient
          TabOrder = 0
          object TabSheet49: TTabSheet
            Caption = ' Configura'#231#245'es gerais '
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object Panel80: TPanel
              Left = 0
              Top = 0
              Width = 1038
              Height = 437
              Align = alClient
              BevelOuter = bvNone
              ParentBackground = False
              TabOrder = 0
              ExplicitWidth = 1041
              ExplicitHeight = 440
              object Panel81: TPanel
                Left = 0
                Top = 125
                Width = 1038
                Height = 136
                Align = alTop
                BevelOuter = bvNone
                TabOrder = 0
                ExplicitWidth = 1041
                object RGMDFetpEmis: TdmkRadioGroup
                  Left = 0
                  Top = 0
                  Width = 425
                  Height = 136
                  Align = alLeft
                  Caption = ' Tipo de emiss'#227'o da MDF-e: (apenas informativo)'
                  Columns = 2
                  Enabled = False
                  ItemIndex = 1
                  Items.Strings = (
                    '0 - Nenhum'
                    '1 - Normal'
                    '2 - '
                    '3 - '
                    '4 - Conting'#234'ncia EPEC pela SVC'
                    '5 - Conting'#234'ncia FSDA'
                    '6-'
                    '7-Autoriza'#231#227'o pela SVC-RS'
                    '8-Autoriza'#231#227'o pela SVC-SP')
                  TabOrder = 0
                  QryCampo = 'MDFetpEmis'
                  UpdType = utYes
                  OldValor = 0
                end
                object GroupBox51: TGroupBox
                  Left = 425
                  Top = 0
                  Width = 613
                  Height = 136
                  Align = alClient
                  Caption = ' Pr'#233'-emails: '
                  TabOrder = 1
                  ExplicitWidth = 616
                  object Label346: TLabel
                    Left = 8
                    Top = 16
                    Width = 201
                    Height = 13
                    Caption = 'Pr'#233'-email para envio de MDF-e autorizada:'
                  end
                  object Label347: TLabel
                    Left = 8
                    Top = 56
                    Width = 202
                    Height = 13
                    Caption = 'Pr'#233'-email para envio de MDF-e cancelada:'
                  end
                  object Label365: TLabel
                    Left = 8
                    Top = 96
                    Width = 250
                    Height = 13
                    Caption = 'Pr'#233'-email para envio de carta de corre'#231#227'o eletr'#244'nica:'
                  end
                  object EdMDFePreMailAut: TdmkEditCB
                    Left = 8
                    Top = 32
                    Width = 44
                    Height = 21
                    Alignment = taRightJustify
                    TabOrder = 0
                    FormatType = dmktfInteger
                    MskType = fmtNone
                    DecimalSize = 0
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ValMin = '-2147483647'
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '0'
                    QryCampo = 'MDFePreMailAut'
                    UpdCampo = 'MDFePreMailAut'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 0
                    ValWarn = False
                    DBLookupComboBox = CBMDFePreMailAut
                    IgnoraDBLookupComboBox = False
                    AutoSetIfOnlyOneReg = setregOnlyManual
                  end
                  object CBMDFePreMailAut: TdmkDBLookupComboBox
                    Left = 52
                    Top = 32
                    Width = 520
                    Height = 21
                    KeyField = 'Codigo'
                    ListField = 'Nome'
                    ListSource = DsPreMail0
                    TabOrder = 1
                    dmkEditCB = EdMDFePreMailAut
                    QryCampo = 'MDFePreMailAut'
                    UpdType = utYes
                    LocF7SQLMasc = '$#'
                  end
                  object EdMDFePreMailCan: TdmkEditCB
                    Left = 8
                    Top = 72
                    Width = 44
                    Height = 21
                    Alignment = taRightJustify
                    TabOrder = 4
                    FormatType = dmktfInteger
                    MskType = fmtNone
                    DecimalSize = 0
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ValMin = '-2147483647'
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '0'
                    QryCampo = 'MDFePreMailCan'
                    UpdCampo = 'MDFePreMailCan'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 0
                    ValWarn = False
                    DBLookupComboBox = CBMDFePreMailCan
                    IgnoraDBLookupComboBox = False
                    AutoSetIfOnlyOneReg = setregOnlyManual
                  end
                  object CBMDFePreMailCan: TdmkDBLookupComboBox
                    Left = 52
                    Top = 72
                    Width = 520
                    Height = 21
                    KeyField = 'Codigo'
                    ListField = 'Nome'
                    ListSource = DsPreMail1
                    TabOrder = 5
                    dmkEditCB = EdMDFePreMailCan
                    QryCampo = 'MDFePreMailCan'
                    UpdType = utYes
                    LocF7SQLMasc = '$#'
                  end
                  object EdMDFePreMailEveCCe: TdmkEditCB
                    Left = 8
                    Top = 112
                    Width = 44
                    Height = 21
                    Alignment = taRightJustify
                    TabOrder = 2
                    FormatType = dmktfInteger
                    MskType = fmtNone
                    DecimalSize = 0
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ValMin = '-2147483647'
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '0'
                    QryCampo = 'MDFePreMailEveCCe'
                    UpdCampo = 'MDFePreMailEveCCe'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 0
                    ValWarn = False
                    DBLookupComboBox = CBMDFePreMailEveCCe
                    IgnoraDBLookupComboBox = False
                    AutoSetIfOnlyOneReg = setregOnlyManual
                  end
                  object CBMDFePreMailEveCCe: TdmkDBLookupComboBox
                    Left = 52
                    Top = 112
                    Width = 520
                    Height = 21
                    KeyField = 'Codigo'
                    ListField = 'Nome'
                    ListSource = DsPreMail0
                    TabOrder = 3
                    dmkEditCB = EdMDFePreMailEveCCe
                    QryCampo = 'MDFePreMailEveCCe'
                    UpdType = utYes
                    LocF7SQLMasc = '$#'
                  end
                end
              end
              object Panel82: TPanel
                Left = 0
                Top = 261
                Width = 1038
                Height = 152
                Align = alTop
                BevelOuter = bvNone
                TabOrder = 1
                ExplicitWidth = 1041
                object GroupBox52: TGroupBox
                  Left = 121
                  Top = 0
                  Width = 920
                  Height = 152
                  Align = alClient
                  TabOrder = 1
                  object Panel83: TPanel
                    Left = 2
                    Top = 15
                    Width = 643
                    Height = 135
                    Align = alLeft
                    BevelOuter = bvNone
                    TabOrder = 0
                    object Label366: TLabel
                      Left = 8
                      Top = 4
                      Width = 291
                      Height = 13
                      Caption = 'Diret'#243'rio das MDF-e protocoladas (autorizadas e canceladas):'
                    end
                    object SpeedButton29: TSpeedButton
                      Left = 615
                      Top = 19
                      Width = 21
                      Height = 21
                      Caption = '...'
                      OnClick = SpeedButton29Click
                    end
                    object Label367: TLabel
                      Left = 8
                      Top = 44
                      Width = 327
                      Height = 13
                      Caption = 
                        'Diret'#243'rio das MDF-e recuperadas da web (autorizadas e canceladas' +
                        '):'
                    end
                    object SpeedButton30: TSpeedButton
                      Left = 615
                      Top = 59
                      Width = 21
                      Height = 21
                      Caption = '...'
                      OnClick = SpeedButton30Click
                    end
                    object EdDirMDFeProt: TdmkEdit
                      Left = 8
                      Top = 19
                      Width = 605
                      Height = 21
                      TabOrder = 0
                      FormatType = dmktfString
                      MskType = fmtNone
                      DecimalSize = 0
                      LeftZeros = 0
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ValMin = '-2147483647'
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      QryCampo = 'DirMDFeProt'
                      UpdCampo = 'DirMDFeProt'
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = ''
                      ValWarn = False
                    end
                    object EdDirMDFeRWeb: TdmkEdit
                      Left = 8
                      Top = 59
                      Width = 605
                      Height = 21
                      TabOrder = 1
                      FormatType = dmktfString
                      MskType = fmtNone
                      DecimalSize = 0
                      LeftZeros = 0
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ValMin = '-2147483647'
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      QryCampo = 'DirMDFeRWeb'
                      UpdCampo = 'DirMDFeRWeb'
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = ''
                      ValWarn = False
                    end
                  end
                end
                object GroupBox55: TGroupBox
                  Left = 0
                  Top = 0
                  Width = 121
                  Height = 152
                  Align = alLeft
                  Caption = ' Conting'#234'ncia SCAN: '
                  TabOrder = 0
                  object Panel84: TPanel
                    Left = 2
                    Top = 15
                    Width = 117
                    Height = 135
                    Align = alClient
                    BevelOuter = bvNone
                    TabOrder = 0
                    object Label368: TLabel
                      Left = 8
                      Top = 4
                      Width = 24
                      Height = 13
                      Caption = 'S'#233'rie'
                    end
                    object Label369: TLabel
                      Left = 8
                      Top = 44
                      Width = 64
                      Height = 13
                      Caption = #218'ltima MDF-e'
                    end
                    object EdMDFeSCAN_Ser: TdmkEdit
                      Left = 8
                      Top = 20
                      Width = 80
                      Height = 21
                      Alignment = taRightJustify
                      TabOrder = 0
                      FormatType = dmktfInteger
                      MskType = fmtNone
                      DecimalSize = 0
                      LeftZeros = 0
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ValMin = '900'
                      ValMax = '999'
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      Texto = '900'
                      QryCampo = 'MDFeSCAN_Ser'
                      UpdCampo = 'MDFeSCAN_Ser'
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = 900
                      ValWarn = False
                    end
                    object EdMDFeSCAN_nMDF: TdmkEdit
                      Left = 8
                      Top = 60
                      Width = 80
                      Height = 21
                      Alignment = taRightJustify
                      TabOrder = 1
                      FormatType = dmktfInteger
                      MskType = fmtNone
                      DecimalSize = 0
                      LeftZeros = 0
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ValMax = '0'
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      Texto = '0'
                      QryCampo = 'MDFeSCAN_nMDF'
                      UpdCampo = 'MDFeSCAN_nMDF'
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = 0
                      ValWarn = False
                    end
                  end
                end
              end
              object Panel85: TPanel
                Left = 0
                Top = 0
                Width = 1038
                Height = 125
                Align = alTop
                BevelOuter = bvNone
                TabOrder = 2
                ExplicitWidth = 1041
                object GroupBox56: TGroupBox
                  Left = 0
                  Top = 0
                  Width = 769
                  Height = 125
                  Align = alLeft
                  TabOrder = 0
                  object Panel86: TPanel
                    Left = 2
                    Top = 15
                    Width = 765
                    Height = 108
                    Align = alClient
                    BevelOuter = bvNone
                    TabOrder = 0
                    ExplicitHeight = 109
                    object Label370: TLabel
                      Left = 8
                      Top = 4
                      Width = 69
                      Height = 13
                      Caption = 'UF da SEFAZ:'
                    end
                    object Label371: TLabel
                      Left = 8
                      Top = 28
                      Width = 83
                      Height = 13
                      Caption = 'WebService [F4]:'
                    end
                    object Label372: TLabel
                      Left = 7
                      Top = 56
                      Width = 232
                      Height = 13
                      Caption = 'N'#250'mero de S'#233'rie do Certificado Digital da MDF-e:'
                    end
                    object SpeedButton33: TSpeedButton
                      Left = 736
                      Top = 52
                      Width = 21
                      Height = 21
                      Caption = '...'
                      OnClick = SpeedButton33Click
                    end
                    object Label373: TLabel
                      Left = 8
                      Top = 80
                      Width = 181
                      Height = 13
                      Caption = 'Data da validade do certificado digital:'
                    end
                    object Label374: TLabel
                      Left = 336
                      Top = 80
                      Width = 268
                      Height = 13
                      Caption = 'Avisar a expira'#231#227'o da validade do certificado digital com '
                    end
                    object Label375: TLabel
                      Left = 652
                      Top = 80
                      Width = 105
                      Height = 13
                      Caption = 'dias de anteced'#234'ncia.'
                    end
                    object EdMDFeUF_WebServ: TdmkEdit
                      Left = 108
                      Top = 0
                      Width = 40
                      Height = 21
                      CharCase = ecUpperCase
                      TabOrder = 0
                      FormatType = dmktfString
                      MskType = fmtNone
                      DecimalSize = 0
                      LeftZeros = 0
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      Texto = 'PR'
                      QryCampo = 'MDFeUF_WebServ'
                      UpdCampo = 'MDFeUF_WebServ'
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = 'PR'
                      ValWarn = False
                    end
                    object EdMDFeUF_Servico: TdmkEdit
                      Left = 108
                      Top = 24
                      Width = 40
                      Height = 21
                      CharCase = ecUpperCase
                      TabOrder = 1
                      FormatType = dmktfString
                      MskType = fmtNone
                      DecimalSize = 0
                      LeftZeros = 0
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      Texto = 'PR'
                      QryCampo = 'MDFeUF_Servico'
                      UpdCampo = 'MDFeUF_Servico'
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = 'PR'
                      ValWarn = False
                      OnKeyDown = EdMDFeUF_ServicoKeyDown
                    end
                    object EdMDFeSerNum: TdmkEdit
                      Left = 240
                      Top = 52
                      Width = 493
                      Height = 21
                      MaxLength = 15
                      TabOrder = 2
                      FormatType = dmktfString
                      MskType = fmtNone
                      DecimalSize = 0
                      LeftZeros = 0
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      QryCampo = 'MDFeSerNum'
                      UpdCampo = 'MDFeSerNum'
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = ''
                      ValWarn = False
                    end
                    object TPMDFeSerVal: TdmkEditDateTimePicker
                      Left = 200
                      Top = 76
                      Width = 112
                      Height = 21
                      Date = 0.669258657406317100
                      Time = 0.669258657406317100
                      TabOrder = 3
                      ReadOnly = False
                      DefaultEditMask = '!99/99/99;1;_'
                      AutoApplyEditMask = True
                      QryCampo = 'MDFeSerVal'
                      UpdCampo = 'MDFeSerVal'
                      UpdType = utYes
                      DatePurpose = dmkdpNone
                    end
                    object EdMDFeSerAvi: TdmkEdit
                      Left = 608
                      Top = 76
                      Width = 40
                      Height = 21
                      Alignment = taRightJustify
                      TabOrder = 4
                      FormatType = dmktfInteger
                      MskType = fmtNone
                      DecimalSize = 0
                      LeftZeros = 0
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      Texto = '30'
                      QryCampo = 'MDFeSerAvi'
                      UpdCampo = 'MDFeSerAvi'
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = 30
                      ValWarn = False
                    end
                  end
                end
                object GroupBox57: TGroupBox
                  Left = 769
                  Top = 0
                  Width = 269
                  Height = 125
                  Align = alClient
                  Caption = ' WebServices espec'#237'ficos: '
                  TabOrder = 1
                  ExplicitWidth = 272
                  object Panel87: TPanel
                    Left = 2
                    Top = 15
                    Width = 269
                    Height = 109
                    Align = alClient
                    BevelOuter = bvNone
                    TabOrder = 0
                    object Label376: TLabel
                      Left = 8
                      Top = 4
                      Width = 139
                      Height = 13
                      Caption = 'Manifesta'#231#227'o do destinat'#225'rio:'
                      Enabled = False
                    end
                    object Label377: TLabel
                      Left = 8
                      Top = 28
                      Width = 133
                      Height = 13
                      Caption = 'Consulta MDF-e destinadas:'
                      Enabled = False
                    end
                    object Label378: TLabel
                      Left = 8
                      Top = 52
                      Width = 86
                      Height = 13
                      Caption = 'Download MDF-e:'
                      Enabled = False
                    end
                    object dmkEdit1: TdmkEdit
                      Left = 152
                      Top = 0
                      Width = 40
                      Height = 21
                      CharCase = ecUpperCase
                      Enabled = False
                      TabOrder = 0
                      FormatType = dmktfString
                      MskType = fmtNone
                      DecimalSize = 0
                      LeftZeros = 0
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      Texto = 'PR'
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = 'PR'
                      ValWarn = False
                    end
                    object dmkEdit2: TdmkEdit
                      Left = 152
                      Top = 24
                      Width = 40
                      Height = 21
                      CharCase = ecUpperCase
                      Enabled = False
                      TabOrder = 1
                      FormatType = dmktfString
                      MskType = fmtNone
                      DecimalSize = 0
                      LeftZeros = 0
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      Texto = 'PR'
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = 'PR'
                      ValWarn = False
                    end
                    object EdUF_MDeMDFe: TdmkEdit
                      Left = 152
                      Top = 48
                      Width = 40
                      Height = 21
                      CharCase = ecUpperCase
                      Enabled = False
                      TabOrder = 2
                      FormatType = dmktfString
                      MskType = fmtNone
                      DecimalSize = 0
                      LeftZeros = 0
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      Texto = 'PR'
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = 'PR'
                      ValWarn = False
                    end
                  end
                end
              end
            end
          end
          object TabSheet50: TTabSheet
            Caption = ' Diret'#243'rios de arquivos XML [1] '
            ImageIndex = 1
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object Panel88: TPanel
              Left = 0
              Top = 0
              Width = 1041
              Height = 440
              Align = alClient
              BevelOuter = bvNone
              ParentBackground = False
              TabOrder = 0
              object Label379: TLabel
                Left = 8
                Top = 84
                Width = 190
                Height = 13
                Caption = 'Diret'#243'rio dos lotes gerados (-env-lot.xml):'
              end
              object Label380: TLabel
                Left = 8
                Top = 124
                Width = 246
                Height = 13
                Caption = 'Diret'#243'rio das respostas dos lotes enviados (-rec.xml):'
              end
              object Label381: TLabel
                Left = 8
                Top = 204
                Width = 351
                Height = 13
                Caption = 
                  'Diret'#243'rio dos resultados de processamento de lote de MDF-e (-pro' +
                  '-rec.xml):'
              end
              object SbDirMDFePedRec: TSpeedButton
                Left = 739
                Top = 140
                Width = 21
                Height = 21
                Caption = '...'
              end
              object SbDirMDFeEnvLot: TSpeedButton
                Left = 739
                Top = 100
                Width = 21
                Height = 21
                Caption = '...'
              end
              object SbDirMDFeProRec: TSpeedButton
                Left = 739
                Top = 220
                Width = 21
                Height = 21
                Caption = '...'
              end
              object Label382: TLabel
                Left = 8
                Top = 44
                Width = 206
                Height = 13
                Caption = 'Diret'#243'rio das MDF-e assinadas (-MDFe.xml):'
              end
              object SBDirMDFeAss: TSpeedButton
                Left = 739
                Top = 60
                Width = 21
                Height = 21
                Caption = '...'
              end
              object Label383: TLabel
                Left = 8
                Top = 4
                Width = 197
                Height = 13
                Caption = 'Diret'#243'rio das MDF-e geradas (-MDFe.xml):'
              end
              object SBDirMDFeGer: TSpeedButton
                Left = 739
                Top = 20
                Width = 21
                Height = 21
                Caption = '...'
              end
              object Label384: TLabel
                Left = 8
                Top = 244
                Width = 201
                Height = 13
                Caption = 'Diret'#243'rio das MDF-e denegadas (-den.xml):'
              end
              object SpeedButton34: TSpeedButton
                Left = 739
                Top = 260
                Width = 21
                Height = 21
                Caption = '...'
              end
              object Label385: TLabel
                Left = 8
                Top = 164
                Width = 409
                Height = 13
                Caption = 
                  'Diret'#243'rio dos pedidos de resultado de processamento de lotes de ' +
                  'MDF-e (-ped-rec.xml):'
              end
              object SpeedButton35: TSpeedButton
                Left = 739
                Top = 180
                Width = 21
                Height = 21
                Caption = '...'
              end
              object EdDirMDFeEnvLot: TdmkEdit
                Left = 8
                Top = 100
                Width = 732
                Height = 21
                TabOrder = 2
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                QryCampo = 'DirMDFeEnvLot'
                UpdCampo = 'DirMDFeEnvLot'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object EdDirMDFeRec: TdmkEdit
                Left = 8
                Top = 140
                Width = 732
                Height = 21
                TabOrder = 3
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                QryCampo = 'DirMDFeRec'
                UpdCampo = 'DirMDFeRec'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object EdDirMDFeProRec: TdmkEdit
                Left = 8
                Top = 220
                Width = 732
                Height = 21
                TabOrder = 5
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                QryCampo = 'DirMDFeProRec'
                UpdCampo = 'DirMDFeProRec'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object EdDirMDFeAss: TdmkEdit
                Left = 8
                Top = 60
                Width = 732
                Height = 21
                TabOrder = 1
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                QryCampo = 'DirMDFeAss'
                UpdCampo = 'DirMDFeAss'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object EdDirMDFeGer: TdmkEdit
                Left = 8
                Top = 20
                Width = 732
                Height = 21
                TabOrder = 0
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                QryCampo = 'DirMDFeGer'
                UpdCampo = 'DirMDFeGer'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object EdDirMDFeDen: TdmkEdit
                Left = 8
                Top = 260
                Width = 732
                Height = 21
                TabOrder = 6
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                QryCampo = 'DirMDFeDen'
                UpdCampo = 'DirMDFeDen'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object EdDirMDFePedRec: TdmkEdit
                Left = 8
                Top = 180
                Width = 732
                Height = 21
                TabOrder = 4
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                QryCampo = 'DirMDFePedRec'
                UpdCampo = 'DirMDFePedRec'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object BtMDFeDefaultDirs: TBitBtn
                Left = 8
                Top = 287
                Width = 753
                Height = 40
                Cursor = crHandPoint
                Caption = 
                  'Sugest'#227'o de diret'#243'rios para os arquivos XML [Geral], [1], [2] e ' +
                  '[3] (somente na altera'#231#227'o do registro, e somente para n'#227'o defini' +
                  'dos)'
                Enabled = False
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -12
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                NumGlyphs = 2
                ParentFont = False
                ParentShowHint = False
                ShowHint = True
                TabOrder = 7
                OnClick = BtMDFeDefaultDirsClick
              end
            end
          end
          object TabSheet51: TTabSheet
            Caption = ' Diret'#243'rios de arquivos XML [2] '
            ImageIndex = 2
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object Panel89: TPanel
              Left = 0
              Top = 0
              Width = 1041
              Height = 440
              Align = alClient
              BevelOuter = bvNone
              ParentBackground = False
              TabOrder = 0
              object Label386: TLabel
                Left = 8
                Top = 84
                Width = 345
                Height = 13
                Caption = 
                  'Diret'#243'rio dos pedidos de inutiliza'#231#227'o de n'#250'meros de MDF-e (-ped-' +
                  'inu.xml):'
              end
              object Label387: TLabel
                Left = 8
                Top = 124
                Width = 332
                Height = 13
                Caption = 
                  'Diret'#243'rio das respostas de inutiliza'#231#227'o de n'#250'meros de MDF-e (-in' +
                  'u.xml):'
              end
              object Label388: TLabel
                Left = 8
                Top = 164
                Width = 355
                Height = 13
                Caption = 
                  'Diret'#243'rio dos pedidos de consulta da situa'#231#227'o atual da MDF-e (-p' +
                  'ed-sit.xml):'
              end
              object Label389: TLabel
                Left = 8
                Top = 204
                Width = 342
                Height = 13
                Caption = 
                  'Diret'#243'rio das respostas de consulta da situa'#231#227'o atual da MDF-e (' +
                  '-sit.xml):'
              end
              object Label390: TLabel
                Left = 8
                Top = 244
                Width = 323
                Height = 13
                Caption = 
                  'Diret'#243'rio dos pedidos de consulta do status do servi'#231'o (-ped-sta' +
                  '.xml):'
              end
              object SbDirMDFePedSta: TSpeedButton
                Left = 739
                Top = 260
                Width = 21
                Height = 21
                Caption = '...'
              end
              object Label391: TLabel
                Left = 8
                Top = 284
                Width = 310
                Height = 13
                Caption = 
                  'Diret'#243'rio das respostas de consulta do status do servi'#231'o (-sta.x' +
                  'ml):'
              end
              object SbDirMDFeSta: TSpeedButton
                Left = 739
                Top = 300
                Width = 21
                Height = 21
                Caption = '...'
              end
              object Label392: TLabel
                Left = 8
                Top = 4
                Width = 306
                Height = 13
                Caption = 'Diret'#243'rio dos pedidos de cancelamento de MDF-e (-ped-can.xml):'
              end
              object Label393: TLabel
                Left = 8
                Top = 44
                Width = 293
                Height = 13
                Caption = 'Diret'#243'rio das respostas de cancelamento de MDF-e (-can.xml):'
              end
              object SBDirMDFePedCan: TSpeedButton
                Left = 739
                Top = 20
                Width = 21
                Height = 21
                Caption = '...'
                Enabled = False
              end
              object SBDirMDFeCan: TSpeedButton
                Left = 739
                Top = 60
                Width = 21
                Height = 21
                Caption = '...'
                Enabled = False
              end
              object SbDirMDFePedInu: TSpeedButton
                Left = 739
                Top = 100
                Width = 21
                Height = 21
                Caption = '...'
              end
              object SbDirMDFeInu: TSpeedButton
                Left = 739
                Top = 140
                Width = 21
                Height = 21
                Caption = '...'
              end
              object SbDirMDFePedSit: TSpeedButton
                Left = 739
                Top = 180
                Width = 21
                Height = 21
                Caption = '...'
              end
              object SBDirMDFeSit: TSpeedButton
                Left = 739
                Top = 220
                Width = 21
                Height = 21
                Caption = '...'
              end
              object EdDirMDFePedSta: TdmkEdit
                Left = 8
                Top = 260
                Width = 732
                Height = 21
                TabOrder = 6
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                QryCampo = 'DirMDFePedSta'
                UpdCampo = 'DirMDFePedSta'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object EdDirMDFeSta: TdmkEdit
                Left = 8
                Top = 300
                Width = 732
                Height = 21
                TabOrder = 7
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                QryCampo = 'DirMDFeSta'
                UpdCampo = 'DirMDFeSta'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object EdDirMDFePedCan: TdmkEdit
                Left = 8
                Top = 20
                Width = 732
                Height = 21
                TabOrder = 0
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                QryCampo = 'DirMDFePedCan'
                UpdCampo = 'DirMDFePedCan'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object EdDirMDFeCan: TdmkEdit
                Left = 8
                Top = 60
                Width = 732
                Height = 21
                TabOrder = 1
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                QryCampo = 'DirMDFeCan'
                UpdCampo = 'DirMDFeCan'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object EdDirMDFePedInu: TdmkEdit
                Left = 8
                Top = 100
                Width = 732
                Height = 21
                TabOrder = 2
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                QryCampo = 'DirMDFePedInu'
                UpdCampo = 'DirMDFePedInu'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object EdDirMDFeInu: TdmkEdit
                Left = 8
                Top = 140
                Width = 732
                Height = 21
                TabOrder = 3
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                QryCampo = 'DirMDFeInu'
                UpdCampo = 'DirMDFeInu'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object EdDirMDFePedSit: TdmkEdit
                Left = 8
                Top = 180
                Width = 732
                Height = 21
                TabOrder = 4
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                QryCampo = 'DirMDFePedSit'
                UpdCampo = 'DirMDFePedSit'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object EdDirMDFeSit: TdmkEdit
                Left = 8
                Top = 220
                Width = 732
                Height = 21
                TabOrder = 5
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                QryCampo = 'DirMDFeSit'
                UpdCampo = 'DirMDFeSit'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
            end
          end
          object TabSheet52: TTabSheet
            Caption = ' Diret'#243'rios de arquivos XML [3] '
            ImageIndex = 3
            object Panel90: TPanel
              Left = 0
              Top = 0
              Width = 1038
              Height = 437
              Align = alClient
              BevelOuter = bvNone
              ParentBackground = False
              TabOrder = 0
              object Label394: TLabel
                Left = 8
                Top = 124
                Width = 354
                Height = 13
                Caption = 
                  'Diret'#243'rio das respostas dos pedidos de encerramento de DMF-e  (-' +
                  'enc.xml):'
              end
              object Label395: TLabel
                Left = 8
                Top = 164
                Width = 412
                Height = 13
                Caption = 
                  'Diret'#243'rio dos pedidos de cancelamento de MDF-e como evento da NF' +
                  '-e (-eve-can.xml):'
              end
              object Label396: TLabel
                Left = 8
                Top = 204
                Width = 399
                Height = 13
                Caption = 
                  'Diret'#243'rio das respostas de cancelamento de MDF-e como evento da ' +
                  'NF-e (-can.xml):'
              end
              object Label397: TLabel
                Left = 8
                Top = 4
                Width = 364
                Height = 13
                Caption = 
                  'Diret'#243'rio do envio dos lotes de envio de eventos da MDF-e (-eve-' +
                  'env-lot.xml):'
              end
              object Label398: TLabel
                Left = 8
                Top = 84
                Width = 304
                Height = 13
                Caption = 'Diret'#243'rio dos pedidos de encerramento de DMF-e (-eve-enc.xml):'
              end
              object Label399: TLabel
                Left = 8
                Top = 44
                Width = 365
                Height = 13
                Caption = 
                  'Diret'#243'rio do retorno dos lotes de envio de eventos da MDF-e (-ev' +
                  'e-ret-lot.xml):'
              end
              object SpeedButton36: TSpeedButton
                Left = 739
                Top = 20
                Width = 21
                Height = 21
                Caption = '...'
              end
              object SpeedButton37: TSpeedButton
                Left = 739
                Top = 60
                Width = 21
                Height = 21
                Caption = '...'
              end
              object SpeedButton38: TSpeedButton
                Left = 739
                Top = 100
                Width = 21
                Height = 21
                Caption = '...'
              end
              object SpeedButton39: TSpeedButton
                Left = 739
                Top = 140
                Width = 21
                Height = 21
                Caption = '...'
              end
              object SpeedButton40: TSpeedButton
                Left = 739
                Top = 180
                Width = 21
                Height = 21
                Caption = '...'
              end
              object SpeedButton58: TSpeedButton
                Left = 739
                Top = 220
                Width = 21
                Height = 21
                Caption = '...'
              end
              object Label400: TLabel
                Left = 8
                Top = 244
                Width = 435
                Height = 13
                Caption = 
                  'Diret'#243'rio de armazenamento e disposi'#231#227'o das cartas de corre'#231#227'o e' +
                  'letr'#244'nicas (-proc-cce.xml):'
              end
              object SpeedButton59: TSpeedButton
                Left = 739
                Top = 260
                Width = 21
                Height = 21
                Caption = '...'
              end
              object Label433: TLabel
                Left = 8
                Top = 284
                Width = 435
                Height = 13
                Caption = 
                  'Diret'#243'rio de armazenamento e disposi'#231#227'o das cartas de corre'#231#227'o e' +
                  'letr'#244'nicas (-proc-cce.xml):'
              end
              object SpeedButton70: TSpeedButton
                Left = 739
                Top = 300
                Width = 21
                Height = 21
                Caption = '...'
              end
              object EdDirMDFeEveEnvLot: TdmkEdit
                Left = 8
                Top = 20
                Width = 732
                Height = 21
                TabOrder = 0
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                QryCampo = 'DirMDFeEveEnvLot'
                UpdCampo = 'DirMDFeEveEnvLot'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object EdDirMDFeEveRetLot: TdmkEdit
                Left = 8
                Top = 60
                Width = 732
                Height = 21
                TabOrder = 1
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                QryCampo = 'DirMDFeEveRetLot'
                UpdCampo = 'DirMDFeEveRetLot'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object EdDirMDFeEvePedEnc: TdmkEdit
                Left = 8
                Top = 100
                Width = 732
                Height = 21
                TabOrder = 2
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                QryCampo = 'DirMDFeEvePedEnc'
                UpdCampo = 'DirMDFeEvePedEnc'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object EdDirMDFeEveRetEnc: TdmkEdit
                Left = 8
                Top = 140
                Width = 732
                Height = 21
                TabOrder = 3
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                QryCampo = 'DirMDFeEveRetEnc'
                UpdCampo = 'DirMDFeEveRetEnc'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object EdDirMDFeEvePedCan: TdmkEdit
                Left = 8
                Top = 180
                Width = 732
                Height = 21
                TabOrder = 4
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                QryCampo = 'DirMDFeEvePedCan'
                UpdCampo = 'DirMDFeEvePedCan'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object EdDirMDFeEveRetCan: TdmkEdit
                Left = 8
                Top = 220
                Width = 732
                Height = 21
                TabOrder = 5
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                QryCampo = 'DirMDFeEveRetCan'
                UpdCampo = 'DirMDFeEveRetCan'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object EdDirMDFeEvePedIdC: TdmkEdit
                Left = 8
                Top = 260
                Width = 732
                Height = 21
                TabOrder = 6
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                QryCampo = 'DirMDFeEvePedIdC'
                UpdCampo = 'DirMDFeEvePedIdC'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object EdDirMDFeEveRetIdC: TdmkEdit
                Left = 8
                Top = 300
                Width = 732
                Height = 21
                TabOrder = 7
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                QryCampo = 'DirMDFeEveRetIdC'
                UpdCampo = 'DirMDFeEveRetIdC'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
            end
          end
          object TabSheet53: TTabSheet
            Caption = ' Diret'#243'rios de arquivos XML [4] '
            ImageIndex = 4
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object Panel91: TPanel
              Left = 0
              Top = 0
              Width = 1041
              Height = 440
              Align = alClient
              BevelOuter = bvNone
              ParentBackground = False
              TabOrder = 0
              object Label401: TLabel
                Left = 8
                Top = 4
                Width = 357
                Height = 13
                Caption = 
                  'Diret'#243'rio das respostas de consulta de MDF-es destinadas (-ret-n' +
                  'fe-des.xml):'
              end
              object SBDirRetMDFeDes: TSpeedButton
                Left = 739
                Top = 20
                Width = 21
                Height = 21
                Caption = '...'
              end
              object Label402: TLabel
                Left = 8
                Top = 44
                Width = 325
                Height = 13
                Caption = 
                  'Diret'#243'rio dos envios de manifesta'#231'oes do destinat'#225'rip (-eve-mde.' +
                  'xml):'
              end
              object Label403: TLabel
                Left = 8
                Top = 84
                Width = 318
                Height = 13
                Caption = 
                  'Diret'#243'rio das respostas de manifesta'#231#245'es do destinat'#225'rio (-mde.x' +
                  'ml):'
              end
              object SpeedButton60: TSpeedButton
                Left = 739
                Top = 60
                Width = 21
                Height = 21
                Caption = '...'
                OnClick = SbDirEvePedMDeClick
              end
              object SpeedButton61: TSpeedButton
                Left = 739
                Top = 100
                Width = 21
                Height = 21
                Caption = '...'
                OnClick = SbDirEveRetMDeClick
              end
              object Label404: TLabel
                Left = 8
                Top = 124
                Width = 309
                Height = 13
                Caption = 'Diret'#243'rio dos grupos de MDF-e baixadas (-ret-dow-MDFe-des.xml):'
              end
              object SbDirDowMDFeDes: TSpeedButton
                Left = 739
                Top = 140
                Width = 21
                Height = 21
                Caption = '...'
              end
              object Label405: TLabel
                Left = 8
                Top = 164
                Width = 201
                Height = 13
                Caption = 'Diret'#243'rio das MDF-e baixadas (-MDFe.xml):'
              end
              object SbDirDowMDFeMDFe: TSpeedButton
                Left = 739
                Top = 180
                Width = 21
                Height = 21
                Caption = '...'
              end
              object Label406: TLabel
                Left = 8
                Top = 204
                Width = 364
                Height = 13
                Caption = 
                  'Diret'#243'rio dos envios de Distribui'#231#227'o de DFe de Interesse (-ped-d' +
                  'fe-dis-int.xml):'
              end
              object Label407: TLabel
                Left = 8
                Top = 244
                Width = 365
                Height = 13
                Caption = 
                  'Diret'#243'rio dos retornos de Distribui'#231#227'o de DFe de Interesse (-ret' +
                  '-dfe-dis-int.xml):'
              end
              object SpeedButton62: TSpeedButton
                Left = 739
                Top = 220
                Width = 21
                Height = 21
                Caption = '...'
                OnClick = SbDirDistDFeIntClick
              end
              object SpeedButton63: TSpeedButton
                Left = 739
                Top = 260
                Width = 21
                Height = 21
                Caption = '...'
                OnClick = SbDirRetDistDFeIntClick
              end
              object Label408: TLabel
                Left = 8
                Top = 284
                Width = 402
                Height = 13
                Caption = 
                  'Diret'#243'rio dos retornos de Download de MDF-es confirmadas  (-ret-' +
                  'dow-MDFe-cnf.xml):'
              end
              object SbDirDowMDFeCnf: TSpeedButton
                Left = 739
                Top = 300
                Width = 21
                Height = 21
                Caption = '...'
              end
              object EdDirRetMDFeDes: TdmkEdit
                Left = 8
                Top = 20
                Width = 732
                Height = 21
                TabOrder = 0
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object dmkEdit4: TdmkEdit
                Left = 8
                Top = 60
                Width = 732
                Height = 21
                TabOrder = 1
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                QryCampo = 'DirEvePedMDe'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object dmkEdit5: TdmkEdit
                Left = 8
                Top = 100
                Width = 732
                Height = 21
                TabOrder = 2
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                QryCampo = 'DirEveRetMDe'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object EdDirDowMDFeDes: TdmkEdit
                Left = 8
                Top = 140
                Width = 732
                Height = 21
                TabOrder = 3
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                QryCampo = 'DirDowMDFeDes'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object EdDirDowMDFeMDFe: TdmkEdit
                Left = 8
                Top = 180
                Width = 732
                Height = 21
                TabOrder = 4
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                QryCampo = 'DirDowMDFeMDFe'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object dmkEdit8: TdmkEdit
                Left = 8
                Top = 220
                Width = 732
                Height = 21
                TabOrder = 5
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                QryCampo = 'DirDistDFeInt'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object dmkEdit11: TdmkEdit
                Left = 8
                Top = 260
                Width = 732
                Height = 21
                TabOrder = 6
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                QryCampo = 'DirRetDistDFeInt'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object EdDirDowMDFeCnf: TdmkEdit
                Left = 8
                Top = 300
                Width = 732
                Height = 21
                TabOrder = 7
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                QryCampo = 'DirDowMDFeCnf'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
            end
          end
          object TabSheet54: TTabSheet
            Caption = ' Op'#231#245'es '
            ImageIndex = 4
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object Panel92: TPanel
              Left = 0
              Top = 0
              Width = 1038
              Height = 437
              Align = alClient
              BevelOuter = bvNone
              ParentBackground = False
              TabOrder = 0
              ExplicitWidth = 1041
              ExplicitHeight = 440
              object GroupBox58: TGroupBox
                Left = 0
                Top = 0
                Width = 484
                Height = 437
                Align = alLeft
                Caption = ': '
                TabOrder = 0
                ExplicitHeight = 440
                object Label409: TLabel
                  Left = 8
                  Top = 296
                  Width = 461
                  Height = 13
                  Caption = 
                    'Email pr'#243'prio para receber email enviado (mais de um separar por' +
                    ' ponte e v'#237'rgula) m'#225'x. 255 letras:'
                end
                object Label410: TLabel
                  Left = 8
                  Top = 212
                  Width = 264
                  Height = 13
                  Caption = 'Tipo de email para envio de MDFe e Carta de Corre'#231#227'o:'
                end
                object SpeedButton64: TSpeedButton
                  Left = 452
                  Top = 228
                  Width = 21
                  Height = 21
                  Caption = '...'
                  OnClick = SpeedButton52Click
                end
                object Label411: TLabel
                  Left = 8
                  Top = 252
                  Width = 386
                  Height = 13
                  Caption = 
                    'Tipo de email para envio espec'#237'fico de Carta de Corre'#231#227'o:  (Ex.:' +
                    ' Transportadoras)'
                end
                object SpeedButton65: TSpeedButton
                  Left = 452
                  Top = 268
                  Width = 21
                  Height = 21
                  Caption = '...'
                  OnClick = SpeedButton53Click
                end
                object Label412: TLabel
                  Left = 6
                  Top = 338
                  Width = 66
                  Height = 13
                  Caption = 'Texto padr'#227'o:'
                end
                object SpeedButton66: TSpeedButton
                  Left = 453
                  Top = 355
                  Width = 21
                  Height = 21
                  Caption = '...'
                  OnClick = SpeedButton66Click
                end
                object GroupBox59: TGroupBox
                  Left = 2
                  Top = 15
                  Width = 480
                  Height = 106
                  Align = alTop
                  Caption = ' Dados e identifica'#231#227'o da MDF-e: '
                  TabOrder = 0
                  ExplicitWidth = 481
                  object Label413: TLabel
                    Left = 12
                    Top = 16
                    Width = 57
                    Height = 13
                    Caption = 'Vers'#227'o: [F4]'
                  end
                  object Label414: TLabel
                    Left = 75
                    Top = 16
                    Width = 38
                    Height = 13
                    Caption = 'Modelo:'
                  end
                  object SpeedButton67: TSpeedButton
                    Left = 451
                    Top = 32
                    Width = 21
                    Height = 21
                    Caption = '...'
                    OnClick = SpeedButton67Click
                  end
                  object Label415: TLabel
                    Left = 120
                    Top = 16
                    Width = 214
                    Height = 13
                    Caption = 'Caminho dos arquivos do Schema xml (*.xsd):'
                  end
                  object EdMDFeversao: TdmkEdit
                    Left = 12
                    Top = 32
                    Width = 57
                    Height = 21
                    Alignment = taRightJustify
                    TabOrder = 0
                    FormatType = dmktfDouble
                    MskType = fmtNone
                    DecimalSize = 2
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '1,00'
                    QryCampo = 'MDFeversao'
                    UpdCampo = 'MDFeversao'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 1.000000000000000000
                    ValWarn = False
                  end
                  object EdMDFeide_mod: TdmkEdit
                    Left = 75
                    Top = 32
                    Width = 41
                    Height = 21
                    Alignment = taRightJustify
                    TabOrder = 1
                    FormatType = dmktfInteger
                    MskType = fmtNone
                    DecimalSize = 2
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '58'
                    QryCampo = 'MDFeide_mod'
                    UpdCampo = 'MDFeide_mod'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 58
                    ValWarn = False
                  end
                  object RGMDFeide_tpAmb: TdmkRadioGroup
                    Left = 12
                    Top = 56
                    Width = 461
                    Height = 45
                    Caption = ' Identifica'#231#227'o do Ambiente: '
                    Columns = 3
                    ItemIndex = 0
                    Items.Strings = (
                      '0 - Nenhum'
                      '1 - Produ'#231#227'o'
                      '2 - Homologa'#231#227'o')
                    TabOrder = 3
                    QryCampo = 'MDFeide_tpAmb'
                    UpdCampo = 'MDFeide_tpAmb'
                    UpdType = utYes
                    OldValor = 0
                  end
                  object EdDirMDFeSchema: TdmkEdit
                    Left = 120
                    Top = 32
                    Width = 328
                    Height = 21
                    TabOrder = 2
                    FormatType = dmktfString
                    MskType = fmtNone
                    DecimalSize = 0
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ValMin = '-2147483647'
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    QryCampo = 'DirMDFeSchema'
                    UpdCampo = 'DirMDFeSchema'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = ''
                    ValWarn = False
                  end
                end
                object RGMDFeAppCode: TdmkRadioGroup
                  Left = 2
                  Top = 121
                  Width = 480
                  Height = 44
                  Align = alTop
                  Caption = ' Gerenciamento MDF-e: '
                  Columns = 2
                  Enabled = False
                  ItemIndex = 0
                  Items.Strings = (
                    'Pascal'
                    'C++')
                  TabOrder = 1
                  QryCampo = 'MDFeAppCode'
                  UpdCampo = 'MDFeAppCode'
                  UpdType = utYes
                  OldValor = 0
                  ExplicitWidth = 481
                end
                object RGMDFeAssDigMode: TdmkRadioGroup
                  Left = 2
                  Top = 165
                  Width = 480
                  Height = 44
                  Align = alTop
                  Caption = ' Modo de assinar a MDF-e no Pascal: '
                  Columns = 2
                  Enabled = False
                  ItemIndex = 0
                  Items.Strings = (
                    'CAPICOM.DLL'
                    '.NET FRAMEWORK')
                  TabOrder = 2
                  QryCampo = 'MDFeAssDigMode'
                  UpdCampo = 'MDFeAssDigMode'
                  UpdType = utYes
                  OldValor = 0
                  ExplicitWidth = 481
                end
                object CBMDFeEntiTipCto: TdmkDBLookupComboBox
                  Left = 68
                  Top = 228
                  Width = 383
                  Height = 21
                  KeyField = 'CodUsu'
                  ListField = 'Nome'
                  TabOrder = 4
                  dmkEditCB = EdMDFeEntiTipCto
                  UpdType = utYes
                  LocF7SQLMasc = '$#'
                end
                object EdMyEmailMDFe: TdmkEdit
                  Left = 8
                  Top = 312
                  Width = 465
                  Height = 21
                  CharCase = ecLowerCase
                  MaxLength = 255
                  TabOrder = 7
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = True
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  QryCampo = 'MyEmailMDFe'
                  UpdCampo = 'MyEmailMDFe'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = ''
                  ValWarn = False
                end
                object EdMDFeEntiTipCto: TdmkEditCB
                  Left = 8
                  Top = 228
                  Width = 56
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 3
                  FormatType = dmktfInteger
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ValMin = '-2147483647'
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0
                  ValWarn = False
                  DBLookupComboBox = CBMDFeEntiTipCto
                  IgnoraDBLookupComboBox = False
                  AutoSetIfOnlyOneReg = setregOnlyManual
                end
                object EdMDFeEntiTipCt1: TdmkEditCB
                  Left = 8
                  Top = 268
                  Width = 56
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 5
                  FormatType = dmktfInteger
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ValMin = '-2147483647'
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0
                  ValWarn = False
                  DBLookupComboBox = CBMDFeEntiTipCt1
                  IgnoraDBLookupComboBox = False
                  AutoSetIfOnlyOneReg = setregOnlyManual
                end
                object CBMDFeEntiTipCt1: TdmkDBLookupComboBox
                  Left = 68
                  Top = 268
                  Width = 383
                  Height = 21
                  KeyField = 'CodUsu'
                  ListField = 'Nome'
                  TabOrder = 6
                  dmkEditCB = EdMDFeEntiTipCt1
                  UpdType = utYes
                  LocF7SQLMasc = '$#'
                end
                object EdMDFeInfCpl: TdmkEditCB
                  Left = 8
                  Top = 355
                  Width = 56
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 8
                  FormatType = dmktfInteger
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ValMin = '-2147483647'
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0'
                  QryCampo = 'MDFeInfCpl'
                  UpdCampo = 'MDFeInfCpl'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0
                  ValWarn = False
                  DBLookupComboBox = CBMDFeInfCpl
                  IgnoraDBLookupComboBox = False
                  AutoSetIfOnlyOneReg = setregOnlyManual
                end
                object CBMDFeInfCpl: TdmkDBLookupComboBox
                  Left = 68
                  Top = 355
                  Width = 383
                  Height = 21
                  KeyField = 'Codigo'
                  ListField = 'Nome'
                  TabOrder = 9
                  dmkEditCB = EdMDFeInfCpl
                  QryCampo = 'MDFeInfCpl'
                  UpdType = utYes
                  LocF7SQLMasc = '$#'
                end
              end
              object GroupBox60: TGroupBox
                Left = 484
                Top = 0
                Width = 240
                Height = 437
                Align = alLeft
                Caption = 'Vers'#227'o do servi'#231'o'
                TabOrder = 1
                ExplicitHeight = 440
                object Label416: TLabel
                  Left = 5
                  Top = 17
                  Width = 106
                  Height = 13
                  Caption = 'Status do servi'#231'o: [F4]'
                end
                object Label417: TLabel
                  Left = 5
                  Top = 40
                  Width = 197
                  Height = 13
                  Caption = 'Enviar lote de MDF-e assincrono ao fisco:'
                end
                object Label418: TLabel
                  Left = 5
                  Top = 88
                  Width = 129
                  Height = 13
                  Caption = 'Consultar lote enviado: [F4]'
                end
                object Label419: TLabel
                  Left = 5
                  Top = 112
                  Width = 168
                  Height = 13
                  Caption = 'Pedir cancelamento de MDF-e: [F4]'
                  Enabled = False
                end
                object Label420: TLabel
                  Left = 5
                  Top = 136
                  Width = 196
                  Height = 13
                  Caption = 'Pedir inutiliza'#231#227'o de n'#250'mero(s) de MDF-e:'
                end
                object Label421: TLabel
                  Left = 5
                  Top = 160
                  Width = 103
                  Height = 13
                  Caption = 'Consultar MDF-e: [F4]'
                end
                object Label422: TLabel
                  Left = 5
                  Top = 184
                  Width = 177
                  Height = 13
                  Caption = 'Enviar lote de eventos da MDFe: [F4]'
                end
                object Label423: TLabel
                  Left = 5
                  Top = 64
                  Width = 186
                  Height = 13
                  Caption = 'Enviar lote de MDF-e sincrono ao fisco:'
                  Enabled = False
                end
                object Label424: TLabel
                  Left = 5
                  Top = 208
                  Width = 186
                  Height = 13
                  Caption = 'Consultar cadastro de Contribuinte: [F4]'
                  Enabled = False
                end
                object Label425: TLabel
                  Left = 5
                  Top = 232
                  Width = 120
                  Height = 13
                  Caption = 'Distribui'#231#227'o de DF-e: [F4]'
                  Enabled = False
                end
                object Label426: TLabel
                  Left = 5
                  Top = 256
                  Width = 187
                  Height = 13
                  Caption = 'Download de MDF-es confirmadas: [F4]'
                  Enabled = False
                end
                object EdMDFeVerStaSer: TdmkEdit
                  Left = 193
                  Top = 12
                  Width = 40
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 0
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 2
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '1,00'
                  QryCampo = 'MDFeVerStaSer'
                  UpdCampo = 'MDFeVerStaSer'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 1.000000000000000000
                  ValWarn = False
                end
                object EdMDFeVerEnvLot: TdmkEdit
                  Left = 193
                  Top = 36
                  Width = 40
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 1
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 2
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '1,10'
                  QryCampo = 'MDFeVerEnvLot'
                  UpdCampo = 'MDFeVerEnvLot'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 1.100000000000000000
                  ValWarn = False
                end
                object EdMDFeVerConLot: TdmkEdit
                  Left = 193
                  Top = 84
                  Width = 40
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 3
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 2
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '1,10'
                  QryCampo = 'MDFeVerConLot'
                  UpdCampo = 'MDFeVerConLot'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 1.100000000000000000
                  ValWarn = False
                end
                object EdMDFeVerCanMDFe: TdmkEdit
                  Left = 193
                  Top = 108
                  Width = 40
                  Height = 21
                  Alignment = taRightJustify
                  Enabled = False
                  TabOrder = 4
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 2
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '1,10'
                  QryCampo = 'MDFeVerCanMDFe'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 1.100000000000000000
                  ValWarn = False
                end
                object EdMDFeVerInuNum: TdmkEdit
                  Left = 193
                  Top = 132
                  Width = 40
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 5
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 2
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '1,10'
                  QryCampo = 'MDFeVerInuNum'
                  UpdCampo = 'MDFeVerInuNum'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 1.100000000000000000
                  ValWarn = False
                end
                object EdMDFeVerConMDFe: TdmkEdit
                  Left = 193
                  Top = 156
                  Width = 40
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 6
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 2
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '1,10'
                  QryCampo = 'MDFeVerConMDFe'
                  UpdCampo = 'MDFeVerConMDFe'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 1.100000000000000000
                  ValWarn = False
                end
                object EdMDFeVerLotEve: TdmkEdit
                  Left = 193
                  Top = 180
                  Width = 40
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 7
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 2
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '1,10'
                  QryCampo = 'MDFeVerLotEve'
                  UpdCampo = 'MDFeVerLotEve'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 1.100000000000000000
                  ValWarn = False
                end
                object EdMDFeVerEnvLotSinc: TdmkEdit
                  Left = 193
                  Top = 60
                  Width = 40
                  Height = 21
                  Alignment = taRightJustify
                  Enabled = False
                  TabOrder = 2
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 2
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0,00'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  ValWarn = False
                end
                object EdMDFeVerConsCad: TdmkEdit
                  Left = 193
                  Top = 204
                  Width = 40
                  Height = 21
                  Alignment = taRightJustify
                  Enabled = False
                  TabOrder = 8
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 2
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '2,00'
                  QryCampo = 'MDFeVerConsCad'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 2.000000000000000000
                  ValWarn = False
                end
                object EdMDFeVerDistDFeInt: TdmkEdit
                  Left = 193
                  Top = 228
                  Width = 40
                  Height = 21
                  Alignment = taRightJustify
                  Enabled = False
                  TabOrder = 9
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 2
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '1,00'
                  QryCampo = 'MDFeVerDistDFeInt'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 1.000000000000000000
                  ValWarn = False
                end
                object EdMDFeVerDowMDFe: TdmkEdit
                  Left = 193
                  Top = 252
                  Width = 40
                  Height = 21
                  Alignment = taRightJustify
                  Enabled = False
                  TabOrder = 10
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 2
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '3,10'
                  QryCampo = 'MDFeVerDowMDFe'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 3.100000000000000000
                  ValWarn = False
                end
              end
            end
          end
          object TabSheet55: TTabSheet
            Caption = ' DAMDFe '
            ImageIndex = 5
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object Panel93: TPanel
              Left = 0
              Top = 0
              Width = 1041
              Height = 440
              Align = alClient
              BevelOuter = bvNone
              ParentBackground = False
              TabOrder = 0
              object Label427: TLabel
                Left = 12
                Top = 196
                Width = 164
                Height = 13
                Caption = 'URL Emitente (n'#227'o recomendado):'
              end
              object Label428: TLabel
                Left = 12
                Top = 236
                Width = 254
                Height = 13
                Caption = 'Arquivo do logo da empresa (Impress'#227'o no DAMDFe):'
              end
              object SpeedButton68: TSpeedButton
                Left = 743
                Top = 252
                Width = 21
                Height = 21
                Caption = '...'
                OnClick = SpeedButton68Click
              end
              object Label429: TLabel
                Left = 12
                Top = 276
                Width = 114
                Height = 13
                Caption = 'Diret'#243'rio das DAMDFes:'
              end
              object SpeedButton69: TSpeedButton
                Left = 743
                Top = 292
                Width = 21
                Height = 21
                Caption = '...'
                OnClick = SpeedButton69Click
              end
              object RGMDFeide_tpImp: TdmkRadioGroup
                Left = 8
                Top = 4
                Width = 460
                Height = 45
                Caption = ' Formato de impress'#227'o: '
                Columns = 3
                ItemIndex = 0
                Items.Strings = (
                  '0 - Nenhum'
                  '1 - Retrato'
                  '2 - Paisagem')
                TabOrder = 0
                QryCampo = 'MDFeide_tpImp'
                UpdCampo = 'MDFeide_tpImp'
                UpdType = utYes
                OldValor = 0
              end
              object RGMDFeItsLin: TdmkRadioGroup
                Left = 8
                Top = 52
                Width = 460
                Height = 45
                Caption = ' Linhas por item na DAMDFe: '
                Columns = 4
                ItemIndex = 0
                Items.Strings = (
                  '3'
                  '1'
                  '2'
                  '3')
                TabOrder = 1
                QryCampo = 'MDFeItsLin'
                UpdCampo = 'MDFeItsLin'
                UpdType = utYes
                OldValor = 0
              end
              object GroupBox61: TGroupBox
                Left = 8
                Top = 100
                Width = 460
                Height = 65
                Caption = ' Tamanho da Fonte: '
                TabOrder = 2
                object Label430: TLabel
                  Left = 12
                  Top = 20
                  Width = 66
                  Height = 13
                  Caption = 'Raz'#227'o Social:'
                end
                object Label431: TLabel
                  Left = 152
                  Top = 20
                  Width = 49
                  Height = 13
                  Caption = 'Endere'#231'o:'
                end
                object Label432: TLabel
                  Left = 292
                  Top = 20
                  Width = 45
                  Height = 13
                  Caption = 'Telefone:'
                end
                object EdMDFeFTRazao: TdmkEdit
                  Left = 12
                  Top = 36
                  Width = 134
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 0
                  FormatType = dmktfLongint
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0'
                  QryCampo = 'MDFeFTRazao'
                  UpdCampo = 'MDFeFTRazao'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0
                  ValWarn = False
                end
                object EdMDFeFTEnder: TdmkEdit
                  Left = 152
                  Top = 36
                  Width = 134
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 1
                  FormatType = dmktfLongint
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0'
                  QryCampo = 'MDFeFTEnder'
                  UpdCampo = 'MDFeFTEnder'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0
                  ValWarn = False
                end
                object EdMDFeFTFones: TdmkEdit
                  Left = 292
                  Top = 36
                  Width = 134
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 2
                  FormatType = dmktfLongint
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0'
                  QryCampo = 'MDFeFTFones'
                  UpdCampo = 'MDFeFTFones'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0
                  ValWarn = False
                end
              end
              object CkMDFeMaiusc: TdmkCheckBox
                Left = 12
                Top = 172
                Width = 169
                Height = 17
                Caption = 'For'#231'ar texto a ficar mai'#250'sculo.'
                TabOrder = 3
                QryCampo = 'MDFeMaiusc'
                UpdCampo = 'MDFeMaiusc'
                UpdType = utYes
                ValCheck = '1'
                ValUncheck = '0'
                OldValor = #0
              end
              object EdMDFeShowURL: TdmkEdit
                Left = 12
                Top = 212
                Width = 749
                Height = 21
                TabOrder = 4
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                QryCampo = 'MDFeShowURL'
                UpdCampo = 'MDFeShowURL'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object EdPathLogoMDFe: TdmkEdit
                Left = 12
                Top = 252
                Width = 732
                Height = 21
                TabOrder = 5
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                QryCampo = 'PathLogoMDFe'
                UpdCampo = 'PathLogoMDFe'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object EdDirDAMDFes: TdmkEdit
                Left = 12
                Top = 292
                Width = 732
                Height = 21
                TabOrder = 6
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                QryCampo = 'DirDAMDFes'
                UpdCampo = 'DirDAMDFes'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object RG_MDFe_indFinalCpl: TdmkRadioGroup
                Left = 10
                Top = 317
                Width = 505
                Height = 39
                Caption = 
                  ' Informa'#231#227'o dos valores totais aproximados de impostos no campo ' +
                  '"Dados Adicionais" nas MDF-es: '
                Columns = 3
                ItemIndex = 0
                Items.Strings = (
                  'Indefinido'
                  'Somente consumidor final'
                  'Todas MDF-es')
                TabOrder = 7
                QryCampo = 'MDFe_indFinalCpl'
                UpdCampo = 'MDFe_indFinalCpl'
                UpdType = utYes
                OldValor = 0
              end
            end
          end
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1054
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 1006
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 12
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 216
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
        OnClick = SbImprimeClick
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 216
      Top = 0
      Width = 790
      Height = 52
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 269
        Height = 32
        Caption = 'Par'#226'metros das Filiais'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 269
        Height = 32
        Caption = 'Par'#226'metros das Filiais'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 269
        Height = 32
        Caption = 'Par'#226'metros das Filiais'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 52
    Width = 1054
    Height = 44
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel47: TPanel
      Left = 2
      Top = 15
      Width = 1050
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 12
        Height = 17
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 12
        Height = 17
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = BtSaida
    CanUpd01 = Panel2
    Left = 52
    Top = 65520
  end
  object VuCambioMda: TdmkValUsu
    dmkEditCB = EdMoeda
    Panel = PainelEdita
    QryCampo = 'Moeda'
    UpdCampo = 'Moeda'
    RefCampo = 'Codigo'
    UpdType = utYes
    Left = 80
    Top = 65520
  end
  object QrFiliais: TMySQLQuery
    Database = Dmod.ZZDB
    SQL.Strings = (
      'SELECT Filial, Codigo, '
      'IF(Tipo=0, RazaoSocial, Nome) NOMEFILIAL'
      'FROM entidades'
      'WHERE (Codigo<-10 or Codigo=0)'
      'ORDER BY NOMEFILIAL')
    Left = 540
    Top = 12
    object QrFiliaisFilial: TIntegerField
      FieldName = 'Filial'
    end
    object QrFiliaisCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrFiliaisNOMEFILIAL: TWideStringField
      FieldName = 'NOMEFILIAL'
      Required = True
      Size = 100
    end
  end
  object DsFiliais: TDataSource
    DataSet = QrFiliais
    Left = 568
    Top = 12
  end
  object VuFilial: TdmkValUsu
    dmkEditCB = EdAssociada
    Panel = PainelEdita
    QryCampo = 'Associada'
    UpdCampo = 'Associada'
    RefCampo = 'Codigo'
    UpdType = utYes
    Left = 108
    Top = 65520
  end
  object QrParamsNFs: TMySQLQuery
    Database = Dmod.ZZDB
    SQL.Strings = (
      'SELECT pnf.*, ELT(pnf.IncSeqAuto+1, "Manual", '
      '"Autom'#225'tico", "? ? ?") NO_IncSeqAuto'
      'FROM paramsnfs pnf'
      'WHERE pnf.Codigo=:P0')
    Left = 372
    Top = 36
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrParamsNFsCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrParamsNFsControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrParamsNFsSequencial: TIntegerField
      FieldName = 'Sequencial'
    end
    object QrParamsNFsIncSeqAuto: TIntegerField
      FieldName = 'IncSeqAuto'
    end
    object QrParamsNFsNO_IncSeqAuto: TWideStringField
      FieldName = 'NO_IncSeqAuto'
      Size = 10
    end
    object QrParamsNFsMaxSeqLib: TIntegerField
      FieldName = 'MaxSeqLib'
      Required = True
    end
    object QrParamsNFsSerieNF: TIntegerField
      FieldName = 'SerieNF'
    end
  end
  object DsParamsNFs: TDataSource
    DataSet = QrParamsNFs
    Left = 400
    Top = 36
  end
  object PMFilial: TPopupMenu
    OnPopup = PMFilialPopup
    Left = 420
    Top = 760
    object Adicionafilial1: TMenuItem
      Caption = 'Ad&iciona filial(is)'
      OnClick = Adicionafilial1Click
    end
    object N2: TMenuItem
      Caption = '-'
    end
    object Alterafilialatual1: TMenuItem
      Caption = '&Altera filial atual'
      OnClick = Alterafilialatual1Click
    end
    object Alteraentidadedafilialatual1: TMenuItem
      Caption = 'Altera &entidade da filial atual'
      OnClick = Alteraentidadedafilialatual1Click
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object Retirafilialatual1: TMenuItem
      Caption = '&Retira filial atual'
      Enabled = False
    end
  end
  object PMSerieNF: TPopupMenu
    OnPopup = PMSerieNFPopup
    Left = 564
    Top = 176
    object Incluinovasriedenotafiscal1: TMenuItem
      Caption = '&Inclui nova s'#233'rie de nota fiscal'
      OnClick = Incluinovasriedenotafiscal1Click
    end
    object Alterasriedenotafiscalatual1: TMenuItem
      Caption = '&Altera s'#233'rie de nota fiscal atual'
      OnClick = Alterasriedenotafiscalatual1Click
    end
    object Excluisriedenotafiscal1: TMenuItem
      Caption = '&Exclui s'#233'rie de nota fiscal'
      Enabled = False
    end
  end
  object QrImprime: TMySQLQuery
    Database = Dmod.ZZDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM imprime'
      'WHERE Empresa=:P0'
      'ORDER BY Nome')
    Left = 600
    Top = 12
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrImprimeCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrImprimeNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
  object DsImprime: TDataSource
    DataSet = QrImprime
    Left = 628
    Top = 12
  end
  object QrCtaProdVen: TMySQLQuery
    Database = Dmod.ZZDB
    SQL.Strings = (
      'SELECT Codigo, Nome '
      'FROM contas'
      'WHERE Codigo>0'
      'AND Ativo = 1'
      'AND Credito = "V"'
      'ORDER BY Nome')
    Left = 536
    Top = 60
    object QrCtaProdVenCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCtaProdVenNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsCtaProdVen: TDataSource
    DataSet = QrCtaProdVen
    Left = 564
    Top = 60
  end
  object QrUFs: TMySQLQuery
    Database = Dmod.ZZDB
    SQL.Strings = (
      'SELECT DTB, Codigo, Nome '
      'FROM ufs'
      'ORDER BY Nome')
    Left = 661
    Top = 13
    object QrUFsCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrUFsNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 2
    end
  end
  object DsUFs: TDataSource
    DataSet = QrUFs
    Left = 689
    Top = 13
  end
  object QrCtaServico: TMySQLQuery
    Database = Dmod.ZZDB
    SQL.Strings = (
      'SELECT Codigo, Nome '
      'FROM contas'
      'WHERE Codigo>0'
      'AND Ativo = 1'
      'AND Credito = "V"'
      'ORDER BY Nome')
    Left = 592
    Top = 60
    object QrCtaServicoCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCtaServicoNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsCtaServico: TDataSource
    DataSet = QrCtaServico
    Left = 620
    Top = 60
  end
  object QrPreMail0: TMySQLQuery
    Database = Dmod.ZZDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM preemail'
      'ORDER BY Nome')
    Left = 656
    Top = 60
    object QrPreMail0Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPreMail0Nome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
  object DsPreMail0: TDataSource
    DataSet = QrPreMail0
    Left = 684
    Top = 60
  end
  object QrPreMail1: TMySQLQuery
    Database = Dmod.ZZDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM preemail'
      'ORDER BY Nome')
    Left = 656
    Top = 88
    object QrPreMail1Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPreMail1Nome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
  object DsPreMail1: TDataSource
    DataSet = QrPreMail1
    Left = 684
    Top = 88
  end
  object VUEntiTipCt1: TdmkValUsu
    dmkEditCB = EdEntiTipCt1
    Panel = PainelEdita
    QryCampo = 'EntiTipCt1'
    UpdCampo = 'EntiTipCt1'
    RefCampo = 'Codigo'
    UpdType = utYes
    Left = 484
    Top = 36
  end
  object DsEntiTipCto: TDataSource
    DataSet = QrEntiTipCto
    Left = 456
    Top = 36
  end
  object QrEntiTipCto: TMySQLQuery
    Database = Dmod.ZZDB
    SQL.Strings = (
      'SELECT * '
      'FROM entitipcto'
      'ORDER BY Nome')
    Left = 428
    Top = 36
    object QrEntiTipCtoCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEntiTipCtoCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrEntiTipCtoNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
  end
  object QrContador4: TMySQLQuery
    Database = Dmod.ZZDB
    SQL.Strings = (
      'SELECT Codigo, '
      'IF(Tipo=0, RazaoSocial, Nome) NO_ENT '
      'FROM entidades'
      'WHERE Fornece1="V"'
      'OR Fornece2="V"'
      'OR Fornece3="V"'
      'OR Fornece4="V"'
      'OR Fornece5="V"'
      'OR Fornece6="V"'
      'OR Fornece7="V"'
      'OR Fornece8="V"'
      'ORDER BY NO_ENT')
    Left = 800
    Top = 12
    object QrContador4Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrContador4NO_ENT: TWideStringField
      FieldName = 'NO_ENT'
      Size = 100
    end
  end
  object DsContador4: TDataSource
    DataSet = QrContador4
    Left = 828
    Top = 12
  end
  object DsEscrCtb4: TDataSource
    DataSet = QrEscrCtb4
    Left = 884
    Top = 12
  end
  object QrEscrCtb4: TMySQLQuery
    Database = Dmod.ZZDB
    SQL.Strings = (
      'SELECT Codigo, '
      'IF(Tipo=0, RazaoSocial, Nome) NO_ENT '
      'FROM entidades'
      'WHERE Fornece1="V"'
      'OR Fornece2="V"'
      'OR Fornece3="V"'
      'OR Fornece4="V"'
      'OR Fornece5="V"'
      'OR Fornece6="V"'
      'OR Fornece7="V"'
      'OR Fornece8="V"'
      'ORDER BY NO_ENT')
    Left = 856
    Top = 12
    object IntegerField1: TIntegerField
      FieldName = 'Codigo'
    end
    object StringField1: TWideStringField
      FieldName = 'NO_ENT'
      Size = 100
    end
  end
  object QrEntiTipCt1: TMySQLQuery
    Database = Dmod.ZZDB
    SQL.Strings = (
      'SELECT * '
      'FROM entitipcto'
      'ORDER BY Nome')
    Left = 428
    Top = 64
  end
  object DsEntiTipCt1: TDataSource
    DataSet = QrEntiTipCt1
    Left = 456
    Top = 64
  end
  object dmkValUsu1: TdmkValUsu
    dmkEditCB = EdEntiTipCto
    Panel = PainelEdita
    QryCampo = 'EntiTipCto'
    UpdCampo = 'EntiTipCto'
    RefCampo = 'Codigo'
    UpdType = utYes
    Left = 484
    Top = 64
  end
  object QrPreMail2: TMySQLQuery
    Database = Dmod.ZZDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM preemail'
      'ORDER BY Nome')
    Left = 712
    Top = 88
    object QrPreMail2Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPreMail2Nome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
  object DsPreMail2: TDataSource
    DataSet = QrPreMail2
    Left = 740
    Top = 88
  end
  object QrMyNFSeMetodos: TMySQLQuery
    Database = Dmod.ZZDB
    SQL.Strings = (
      'SELECT * '
      'FROM mynfsemetodos'
      'ORDER BY Nome')
    Left = 732
    Top = 12
    object QrMyNFSeMetodosCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrMyNFSeMetodosNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
  object DsMyNFSeMetodos: TDataSource
    DataSet = QrMyNFSeMetodos
    Left = 760
    Top = 12
  end
  object QrMyMetodo: TMySQLQuery
    Database = Dmod.ZZDB
    SQL.Strings = (
      'SELECT Nome '
      'FROM mynfsemetodos'
      'WHERE Codigo=:P0'
      '')
    Left = 732
    Top = 40
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrMyMetodoNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
  object DsMyMetodo: TDataSource
    DataSet = QrMyMetodo
    Left = 760
    Top = 40
  end
  object QrEntiTipCt2: TMySQLQuery
    Database = Dmod.ZZDB
    SQL.Strings = (
      'SELECT * '
      'FROM entitipcto'
      'ORDER BY Nome')
    Left = 428
    Top = 92
  end
  object DsEntiTipCt2: TDataSource
    DataSet = QrEntiTipCt2
    Left = 456
    Top = 92
  end
  object dmkValUsu2: TdmkValUsu
    dmkEditCB = EdNFSeTipCtoMail
    Panel = PainelEdita
    QryCampo = 'NFSeTipCtoMail'
    UpdCampo = 'NFSeTipCtoMail'
    RefCampo = 'Codigo'
    UpdType = utYes
    Left = 484
    Top = 92
  end
  object QrPreEmailNFSeAut: TMySQLQuery
    Database = Dmod.ZZDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM preemail'
      'ORDER BY Nome')
    Left = 832
    Top = 72
    object IntegerField2: TIntegerField
      FieldName = 'Codigo'
    end
    object StringField2: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
  object DsPreEmailNFSeAut: TDataSource
    DataSet = QrPreEmailNFSeAut
    Left = 860
    Top = 72
  end
  object DsPreEmailNFSeCan: TDataSource
    DataSet = QrPreEmailNFSeCan
    Left = 916
    Top = 72
  end
  object QrPreEmailNFSeCan: TMySQLQuery
    Database = Dmod.ZZDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM preemail'
      'ORDER BY Nome')
    Left = 888
    Top = 72
    object IntegerField3: TIntegerField
      FieldName = 'Codigo'
    end
    object StringField3: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
  object PMCertDigital: TPopupMenu
    Left = 832
    Top = 600
    object Incluinovocertificadoaorepositriodosistemaoperacional1: TMenuItem
      Caption = 'Inclui novo certificado ao reposit'#243'rio do sistema operacional'
      OnClick = Incluinovocertificadoaorepositriodosistemaoperacional1Click
    end
    object Removecertificadodorepositriodosistemaoperacional1: TMenuItem
      Caption = 'Remove certificado do reposit'#243'rio do sistema operacional'
      OnClick = Removecertificadodorepositriodosistemaoperacional1Click
    end
  end
  object QrNFeInfCpl: TMySQLQuery
    Database = Dmod.ZZDB
    SQL.Strings = (
      'SELECT *'
      'FROM nfeinfcpl')
    Left = 136
    Top = 8
    object QrNFeInfCplCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrNFeInfCplCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrNFeInfCplNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QrNFeInfCplTexto: TWideMemoField
      FieldName = 'Texto'
      BlobType = ftWideMemo
      Size = 4
    end
  end
  object DsNFeInfCpl: TDataSource
    DataSet = QrNFeInfCpl
    Left = 136
    Top = 56
  end
  object QrCartEmisHonFun: TMySQLQuery
    Database = Dmod.ZZDB
    SQL.Strings = (
      'SELECT car.Codigo, car.Nome, car.Tipo'
      'FROM carteiras car'
      'WHERE Tipo=2'
      'ORDER BY Nome')
    Left = 964
    Top = 4
    object QrCartEmisHonFunCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrCartEmisHonFunNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 100
    end
    object QrCartEmisHonFunTipo: TIntegerField
      FieldName = 'Tipo'
    end
    object QrCartEmisHonFunForneceI: TIntegerField
      FieldName = 'ForneceI'
    end
  end
  object DsCartEmisHonFun: TDataSource
    DataSet = QrCartEmisHonFun
    Left = 964
    Top = 52
  end
  object DsCtaProdCom: TDataSource
    DataSet = QrCtaProdCom
    Left = 564
    Top = 108
  end
  object QrCtaProdCom: TMySQLQuery
    Database = Dmod.ZZDB
    SQL.Strings = (
      'SELECT Codigo, Nome '
      'FROM contas'
      'WHERE Codigo>0'
      'AND Ativo = 1'
      'AND Debito = "V"'
      'ORDER BY Nome')
    Left = 536
    Top = 108
    object IntegerField4: TIntegerField
      FieldName = 'Codigo'
    end
    object StringField4: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object QrCtaServicoPg: TMySQLQuery
    Database = Dmod.ZZDB
    SQL.Strings = (
      'SELECT Codigo, Nome '
      'FROM contas'
      'WHERE Codigo>0'
      'AND Ativo = 1'
      'AND Debito = "V"'
      'ORDER BY Nome')
    Left = 592
    Top = 108
    object IntegerField5: TIntegerField
      FieldName = 'Codigo'
    end
    object StringField5: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsCtaServicoPg: TDataSource
    DataSet = QrCtaServicoPg
    Left = 620
    Top = 108
  end
  object VUCTeEntiTipCt1: TdmkValUsu
    dmkEditCB = EdEntiTipCt1
    Panel = PainelEdita
    QryCampo = 'CTeEntiTipCt1'
    UpdCampo = 'CTeEntiTipCt1'
    RefCampo = 'Codigo'
    UpdType = utYes
    Left = 512
    Top = 36
  end
  object VUCTeEntiTipCto: TdmkValUsu
    dmkEditCB = EdEntiTipCto
    Panel = PainelEdita
    QryCampo = 'CTeEntiTipCto'
    UpdCampo = 'CTeEntiTipCto'
    RefCampo = 'Codigo'
    UpdType = utYes
    Left = 512
    Top = 64
  end
  object QrCtaFretPrest: TMySQLQuery
    Database = Dmod.ZZDB
    SQL.Strings = (
      'SELECT Codigo, Nome '
      'FROM contas'
      'WHERE Codigo>0'
      'AND Ativo = 1'
      'AND Credito = "V"'
      'ORDER BY Nome')
    Left = 304
    Top = 32
    object IntegerField6: TIntegerField
      FieldName = 'Codigo'
    end
    object StringField6: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsCtaFretPrest: TDataSource
    DataSet = QrCtaFretPrest
    Left = 304
    Top = 76
  end
  object QrCTeEntiTipCto: TMySQLQuery
    Database = Dmod.ZZDB
    SQL.Strings = (
      'SELECT * '
      'FROM entitipcto'
      'ORDER BY Nome')
    Left = 192
    Top = 32
    object IntegerField7: TIntegerField
      FieldName = 'Codigo'
    end
    object IntegerField8: TIntegerField
      FieldName = 'CodUsu'
    end
    object StringField7: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
  end
  object QrCTeEntiTipCt1: TMySQLQuery
    Database = Dmod.ZZDB
    SQL.Strings = (
      'SELECT * '
      'FROM entitipcto'
      'ORDER BY Nome')
    Left = 252
    Top = 8
  end
  object DsCTeEntiTipCto: TDataSource
    DataSet = QrCTeEntiTipCto
    Left = 188
    Top = 76
  end
  object DsCTeEntiTipCt1: TDataSource
    DataSet = QrCTeEntiTipCt1
    Left = 248
    Top = 56
  end
  object PMSerieCTe: TPopupMenu
    Left = 624
    Top = 176
    object IncluinovasriedeCTe1: TMenuItem
      Caption = '&Inclui nova s'#233'rie de CT-e'
      OnClick = IncluinovasriedeCTe1Click
    end
    object AlterasriedeCTeatual1: TMenuItem
      Caption = '&Altera s'#233'rie de CT-e atual'
      OnClick = AlterasriedeCTeatual1Click
    end
    object ExcluisriedeCTeatual1: TMenuItem
      Caption = '&Exclui s'#233'rie de CT-e atual'
    end
  end
  object QrParamsCTe: TMySQLQuery
    Database = Dmod.ZZDB
    SQL.Strings = (
      'SELECT pnf.*, ELT(pnf.IncSeqAuto+1, "Manual", '
      '"Autom'#225'tico", "? ? ?") NO_IncSeqAuto'
      'FROM paramsnfs pnf'
      'WHERE pnf.Codigo=:P0')
    Left = 16
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrParamsCTeCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrParamsCTeControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrParamsCTeSequencial: TIntegerField
      FieldName = 'Sequencial'
    end
    object QrParamsCTeIncSeqAuto: TIntegerField
      FieldName = 'IncSeqAuto'
    end
    object QrParamsCTeNO_IncSeqAuto: TWideStringField
      FieldName = 'NO_IncSeqAuto'
      Size = 10
    end
    object QrParamsCTeMaxSeqLib: TIntegerField
      FieldName = 'MaxSeqLib'
      Required = True
    end
    object QrParamsCTeSerieCTe: TIntegerField
      FieldName = 'SerieCTe'
    end
  end
  object DsParamsCTe: TDataSource
    DataSet = QrParamsCTe
    Left = 16
    Top = 52
  end
  object PMSerieMDFe: TPopupMenu
    Left = 620
    Top = 228
    object IncluinovasriedeMDFe1: TMenuItem
      Caption = '&Inclui nova s'#233'rie de MDF-e'
      OnClick = IncluinovasriedeMDFe1Click
    end
    object AlterasriedeMDFeatual1: TMenuItem
      Caption = '&Altera s'#233'rie de MDF-e atual'
      OnClick = AlterasriedeMDFeatual1Click
    end
    object ExcluisriedeMDFeatual1: TMenuItem
      Caption = '&Exclui s'#233'rie de MDF-e atual'
    end
  end
  object QrParamsMDFe: TMySQLQuery
    Database = Dmod.ZZDB
    SQL.Strings = (
      'SELECT pnf.*, ELT(pnf.IncSeqAuto+1, "Manual", '
      '"Autom'#225'tico", "? ? ?") NO_IncSeqAuto'
      'FROM paramsnfs pnf'
      'WHERE pnf.Codigo=:P0')
    Left = 80
    Top = 32
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrParamsMDFeCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrParamsMDFeControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrParamsMDFeSequencial: TIntegerField
      FieldName = 'Sequencial'
    end
    object QrParamsMDFeIncSeqAuto: TIntegerField
      FieldName = 'IncSeqAuto'
    end
    object QrParamsMDFeNO_IncSeqAuto: TWideStringField
      FieldName = 'NO_IncSeqAuto'
      Size = 10
    end
    object QrParamsMDFeMaxSeqLib: TIntegerField
      FieldName = 'MaxSeqLib'
      Required = True
    end
    object QrParamsMDFeSerieCTe: TIntegerField
      FieldName = 'SerieMDFe'
    end
  end
  object DsParamsMDFe: TDataSource
    DataSet = QrParamsMDFe
    Left = 80
    Top = 72
  end
  object QrPrmsEmpNFe: TMySQLQuery
    Database = Dmod.ZZDB
    SQL.Strings = (
      'SELECT'
      'PreMailAut,'
      'PreMailCan,'
      'PreMailEveCCe,'
      'DirNFeProt,'
      'DirNFeRWeb,'
      'SCAN_Ser,'
      'SCAN_nNF,'
      'infRespTec_xContato,'
      'infRespTec_CNPJ,'
      'infRespTec_fone,'
      'infRespTec_email,'
      'infRespTec_idCSRT,'
      'infRespTec_CSRT,'
      'infRespTec_Usa,'
      'UF_WebServ,'
      'SiglaCustm,'
      'UF_Servico,'
      'SimplesFed,'
      'InfoPerCuz,'
      'NFeSerNum,'
      'NFeSerVal,'
      'NFeSerAvi,'
      'UF_MDeMDe,'
      'UF_MDeDes,'
      'UF_MDeNFe,'
      'NFeUF_EPEC,'
      'DirEnvLot,'
      'DirRec,'
      'DirProRec,'
      'DirNFeAss,'
      'DirNFeGer,'
      'DirDen,'
      'DirPedRec,'
      'DirPedSta,'
      'DirSta,'
      'DirPedCan,'
      'DirCan,'
      'DirPedInu,'
      'DirInu,'
      'DirPedSit,'
      'DirSit,'
      'DirEveEnvLot,'
      'DirEveRetLot,'
      'DirEvePedCCe,'
      'DirEveRetCCe,'
      'DirEvePedCan,'
      'DirEveRetCan,'
      'DirEveProcCCe,'
      'DirRetNfeDes,'
      'DirEvePedMDe,'
      'DirEveRetMDe,'
      'DirDowNFeDes,'
      'DirDowNFeNFe,'
      'DirDistDFeInt,'
      'DirRetDistDFeInt,'
      'DirDowNFeCnf,'
      'versao,'
      'ide_mod,'
      'ide_tpAmb,'
      'DirSchema,'
      'NT2018_05v120,'
      'AppCode,'
      'AssDigMode,'
      'MyEmailNFe,'
      'NFeInfCpl,'
      'CRT,'
      'CSOSN,'
      'pCredSNAlq,'
      'pCredSNAlq,'
      'NFeVerStaSer,'
      'NFeVerEnvLot,'
      'NFeVerConLot,'
      'NFeVerCanNFe,'
      'NFeVerInuNum,'
      'NFeVerConNFe,'
      'NFeVerLotEve,'
      'NFeVerConsCad,'
      'NFeVerDistDFeInt,'
      'NFeVerDowNFe,'
      'ide_tpImp,'
      'NFeItsLin,'
      'NFeFTRazao,'
      'NFeFTEnder,'
      'NFeFTFones,'
      'NFeMaiusc,'
      'NFeShowURL,'
      'PathLogoNF,'
      'DirDANFEs,'
      'NFe_indFinalCpl,'
      'NoDANFEMail,'
      'NFeNT2013_003LTT,'
      ''
      'CerDigital'
      'FROM paramsemp '
      'WHERE Codigo=0'
      '')
    Left = 392
    Top = 536
    object QrPrmsEmpNFeNO_PREMAILAUT: TWideStringField
      FieldName = 'NO_PREMAILAUT'
      Size = 100
    end
    object QrPrmsEmpNFeNO_PREMAILCAN: TWideStringField
      FieldName = 'NO_PREMAILCAN'
      Size = 100
    end
    object QrPrmsEmpNFeNO_PREMAILEVECCE: TWideStringField
      FieldName = 'NO_PREMAILEVECCE'
      Size = 100
    end
    object QrPrmsEmpNFePreMailAut: TIntegerField
      FieldName = 'PreMailAut'
      Origin = 'paramsemp.PreMailAut'
      Required = True
    end
    object QrPrmsEmpNFePreMailCan: TIntegerField
      FieldName = 'PreMailCan'
      Origin = 'paramsemp.PreMailCan'
      Required = True
    end
    object QrPrmsEmpNFePreMailEveCCe: TIntegerField
      FieldName = 'PreMailEveCCe'
      Origin = 'paramsemp.PreMailEveCCe'
      Required = True
    end
    object QrPrmsEmpNFeDirNFeProt: TWideStringField
      FieldName = 'DirNFeProt'
      Origin = 'paramsemp.DirNFeProt'
      Size = 255
    end
    object QrPrmsEmpNFeDirNFeRWeb: TWideStringField
      FieldName = 'DirNFeRWeb'
      Origin = 'paramsemp.DirNFeRWeb'
      Size = 255
    end
    object QrPrmsEmpNFeSCAN_Ser: TIntegerField
      FieldName = 'SCAN_Ser'
      Origin = 'paramsemp.SCAN_Ser'
      Required = True
    end
    object QrPrmsEmpNFeSCAN_nNF: TIntegerField
      FieldName = 'SCAN_nNF'
      Origin = 'paramsemp.SCAN_nNF'
      Required = True
    end
    object QrPrmsEmpNFeinfRespTec_xContato: TWideStringField
      FieldName = 'infRespTec_xContato'
      Origin = 'paramsemp.infRespTec_xContato'
      Required = True
      Size = 60
    end
    object QrPrmsEmpNFeinfRespTec_CNPJ: TWideStringField
      FieldName = 'infRespTec_CNPJ'
      Origin = 'paramsemp.infRespTec_CNPJ'
      Required = True
      Size = 14
    end
    object QrPrmsEmpNFeinfRespTec_fone: TWideStringField
      FieldName = 'infRespTec_fone'
      Origin = 'paramsemp.infRespTec_fone'
      Required = True
      Size = 14
    end
    object QrPrmsEmpNFeinfRespTec_email: TWideStringField
      FieldName = 'infRespTec_email'
      Origin = 'paramsemp.infRespTec_email'
      Required = True
      Size = 60
    end
    object QrPrmsEmpNFeinfRespTec_idCSRT: TWideStringField
      FieldName = 'infRespTec_idCSRT'
      Origin = 'paramsemp.infRespTec_idCSRT'
      Required = True
      Size = 2
    end
    object QrPrmsEmpNFeinfRespTec_CSRT: TWideStringField
      FieldName = 'infRespTec_CSRT'
      Origin = 'paramsemp.infRespTec_CSRT'
      Size = 120
    end
    object QrPrmsEmpNFeinfRespTec_Usa: TSmallintField
      FieldName = 'infRespTec_Usa'
      Origin = 'paramsemp.infRespTec_Usa'
      Required = True
    end
    object QrPrmsEmpNFeUF_WebServ: TWideStringField
      FieldName = 'UF_WebServ'
      Origin = 'paramsemp.UF_WebServ'
      Required = True
      Size = 2
    end
    object QrPrmsEmpNFeSiglaCustm: TWideStringField
      FieldName = 'SiglaCustm'
      Origin = 'paramsemp.SiglaCustm'
      Size = 15
    end
    object QrPrmsEmpNFeUF_Servico: TWideStringField
      FieldName = 'UF_Servico'
      Origin = 'paramsemp.UF_Servico'
      Required = True
      Size = 10
    end
    object QrPrmsEmpNFeSimplesFed: TSmallintField
      FieldName = 'SimplesFed'
      Origin = 'paramsemp.SimplesFed'
      Required = True
    end
    object QrPrmsEmpNFeInfoPerCuz: TSmallintField
      FieldName = 'InfoPerCuz'
      Origin = 'paramsemp.InfoPerCuz'
      Required = True
    end
    object QrPrmsEmpNFeNFeSerNum: TWideStringField
      FieldName = 'NFeSerNum'
      Origin = 'paramsemp.NFeSerNum'
      Size = 255
    end
    object QrPrmsEmpNFeNFeSerVal: TDateField
      FieldName = 'NFeSerVal'
      Origin = 'paramsemp.NFeSerVal'
      Required = True
    end
    object QrPrmsEmpNFeNFeSerAvi: TSmallintField
      FieldName = 'NFeSerAvi'
      Origin = 'paramsemp.NFeSerAvi'
      Required = True
    end
    object QrPrmsEmpNFeUF_MDeMDe: TWideStringField
      FieldName = 'UF_MDeMDe'
      Origin = 'paramsemp.UF_MDeMDe'
      Required = True
      Size = 10
    end
    object QrPrmsEmpNFeUF_MDeDes: TWideStringField
      FieldName = 'UF_MDeDes'
      Origin = 'paramsemp.UF_MDeDes'
      Required = True
      Size = 10
    end
    object QrPrmsEmpNFeUF_MDeNFe: TWideStringField
      FieldName = 'UF_MDeNFe'
      Origin = 'paramsemp.UF_MDeNFe'
      Required = True
      Size = 10
    end
    object QrPrmsEmpNFeNFeUF_EPEC: TWideStringField
      FieldName = 'NFeUF_EPEC'
      Origin = 'paramsemp.NFeUF_EPEC'
      Required = True
      Size = 10
    end
    object QrPrmsEmpNFeDirEnvLot: TWideStringField
      FieldName = 'DirEnvLot'
      Origin = 'paramsemp.DirEnvLot'
      Size = 255
    end
    object QrPrmsEmpNFeDirRec: TWideStringField
      FieldName = 'DirRec'
      Origin = 'paramsemp.DirRec'
      Size = 255
    end
    object QrPrmsEmpNFeDirProRec: TWideStringField
      FieldName = 'DirProRec'
      Origin = 'paramsemp.DirProRec'
      Size = 255
    end
    object QrPrmsEmpNFeDirNFeAss: TWideStringField
      FieldName = 'DirNFeAss'
      Origin = 'paramsemp.DirNFeAss'
      Size = 255
    end
    object QrPrmsEmpNFeDirNFeGer: TWideStringField
      FieldName = 'DirNFeGer'
      Origin = 'paramsemp.DirNFeGer'
      Size = 255
    end
    object QrPrmsEmpNFeDirDen: TWideStringField
      FieldName = 'DirDen'
      Origin = 'paramsemp.DirDen'
      Size = 255
    end
    object QrPrmsEmpNFeDirPedRec: TWideStringField
      FieldName = 'DirPedRec'
      Origin = 'paramsemp.DirPedRec'
      Size = 255
    end
    object QrPrmsEmpNFeDirPedSta: TWideStringField
      FieldName = 'DirPedSta'
      Origin = 'paramsemp.DirPedSta'
      Size = 255
    end
    object QrPrmsEmpNFeDirSta: TWideStringField
      FieldName = 'DirSta'
      Origin = 'paramsemp.DirSta'
      Size = 255
    end
    object QrPrmsEmpNFeDirPedCan: TWideStringField
      FieldName = 'DirPedCan'
      Origin = 'paramsemp.DirPedCan'
      Size = 255
    end
    object QrPrmsEmpNFeDirCan: TWideStringField
      FieldName = 'DirCan'
      Origin = 'paramsemp.DirCan'
      Size = 255
    end
    object QrPrmsEmpNFeDirPedInu: TWideStringField
      FieldName = 'DirPedInu'
      Origin = 'paramsemp.DirPedInu'
      Size = 255
    end
    object QrPrmsEmpNFeDirInu: TWideStringField
      FieldName = 'DirInu'
      Origin = 'paramsemp.DirInu'
      Size = 255
    end
    object QrPrmsEmpNFeDirPedSit: TWideStringField
      FieldName = 'DirPedSit'
      Origin = 'paramsemp.DirPedSit'
      Size = 255
    end
    object QrPrmsEmpNFeDirSit: TWideStringField
      FieldName = 'DirSit'
      Origin = 'paramsemp.DirSit'
      Size = 255
    end
    object QrPrmsEmpNFeDirEveEnvLot: TWideStringField
      FieldName = 'DirEveEnvLot'
      Origin = 'paramsemp.DirEveEnvLot'
      Size = 255
    end
    object QrPrmsEmpNFeDirEveRetLot: TWideStringField
      FieldName = 'DirEveRetLot'
      Origin = 'paramsemp.DirEveRetLot'
      Size = 255
    end
    object QrPrmsEmpNFeDirEvePedCCe: TWideStringField
      FieldName = 'DirEvePedCCe'
      Origin = 'paramsemp.DirEvePedCCe'
      Size = 255
    end
    object QrPrmsEmpNFeDirEveRetCCe: TWideStringField
      FieldName = 'DirEveRetCCe'
      Origin = 'paramsemp.DirEveRetCCe'
      Size = 255
    end
    object QrPrmsEmpNFeDirEvePedCan: TWideStringField
      FieldName = 'DirEvePedCan'
      Origin = 'paramsemp.DirEvePedCan'
      Size = 255
    end
    object QrPrmsEmpNFeDirEveRetCan: TWideStringField
      FieldName = 'DirEveRetCan'
      Origin = 'paramsemp.DirEveRetCan'
      Size = 255
    end
    object QrPrmsEmpNFeDirEveProcCCe: TWideStringField
      FieldName = 'DirEveProcCCe'
      Origin = 'paramsemp.DirEveProcCCe'
      Size = 255
    end
    object QrPrmsEmpNFeDirSchema: TWideStringField
      FieldName = 'DirSchema'
      Origin = 'paramsemp.DirSchema'
      Size = 255
    end
    object QrPrmsEmpNFeversao: TFloatField
      FieldName = 'versao'
      Origin = 'paramsemp.versao'
      Required = True
    end
    object QrPrmsEmpNFeide_mod: TSmallintField
      FieldName = 'ide_mod'
      Origin = 'paramsemp.ide_mod'
      Required = True
    end
    object QrPrmsEmpNFeide_tpAmb: TSmallintField
      FieldName = 'ide_tpAmb'
      Origin = 'paramsemp.ide_tpAmb'
      Required = True
    end
    object QrPrmsEmpNFeNT2018_05v120: TSmallintField
      FieldName = 'NT2018_05v120'
      Origin = 'paramsemp.NT2018_05v120'
      Required = True
    end
    object QrPrmsEmpNFeAppCode: TSmallintField
      FieldName = 'AppCode'
      Origin = 'paramsemp.AppCode'
      Required = True
    end
    object QrPrmsEmpNFeAssDigMode: TSmallintField
      FieldName = 'AssDigMode'
      Origin = 'paramsemp.AssDigMode'
      Required = True
    end
    object QrPrmsEmpNFeMyEmailNFe: TWideStringField
      FieldName = 'MyEmailNFe'
      Origin = 'paramsemp.MyEmailNFe'
      Size = 255
    end
    object QrPrmsEmpNFeCRT: TSmallintField
      FieldName = 'CRT'
      Origin = 'paramsemp.CRT'
      Required = True
    end
    object QrPrmsEmpNFeCSOSN: TIntegerField
      FieldName = 'CSOSN'
      Origin = 'paramsemp.CSOSN'
      Required = True
    end
    object QrPrmsEmpNFepCredSNAlq: TFloatField
      FieldName = 'pCredSNAlq'
      Origin = 'paramsemp.pCredSNAlq'
      Required = True
    end
    object QrPrmsEmpNFeNFeVerStaSer: TFloatField
      FieldName = 'NFeVerStaSer'
      Origin = 'paramsemp.NFeVerStaSer'
      Required = True
    end
    object QrPrmsEmpNFeNFeVerEnvLot: TFloatField
      FieldName = 'NFeVerEnvLot'
      Origin = 'paramsemp.NFeVerEnvLot'
      Required = True
    end
    object QrPrmsEmpNFeNFeVerConLot: TFloatField
      FieldName = 'NFeVerConLot'
      Origin = 'paramsemp.NFeVerConLot'
      Required = True
    end
    object QrPrmsEmpNFeNFeVerCanNFe: TFloatField
      FieldName = 'NFeVerCanNFe'
      Origin = 'paramsemp.NFeVerCanNFe'
      Required = True
    end
    object QrPrmsEmpNFeNFeVerInuNum: TFloatField
      FieldName = 'NFeVerInuNum'
      Origin = 'paramsemp.NFeVerInuNum'
      Required = True
    end
    object QrPrmsEmpNFeNFeVerConNFe: TFloatField
      FieldName = 'NFeVerConNFe'
      Origin = 'paramsemp.NFeVerConNFe'
      Required = True
    end
    object QrPrmsEmpNFeNFeVerLotEve: TFloatField
      FieldName = 'NFeVerLotEve'
      Origin = 'paramsemp.NFeVerLotEve'
      Required = True
    end
    object QrPrmsEmpNFeNFeVerConsCad: TFloatField
      FieldName = 'NFeVerConsCad'
      Origin = 'paramsemp.NFeVerConsCad'
      Required = True
    end
    object QrPrmsEmpNFeNFeVerDistDFeInt: TFloatField
      FieldName = 'NFeVerDistDFeInt'
      Origin = 'paramsemp.NFeVerDistDFeInt'
      Required = True
    end
    object QrPrmsEmpNFeNFeVerDowNFe: TFloatField
      FieldName = 'NFeVerDowNFe'
      Origin = 'paramsemp.NFeVerDowNFe'
      Required = True
    end
    object QrPrmsEmpNFeide_tpImp: TSmallintField
      FieldName = 'ide_tpImp'
      Origin = 'paramsemp.ide_tpImp'
      Required = True
    end
    object QrPrmsEmpNFeNFeItsLin: TSmallintField
      FieldName = 'NFeItsLin'
      Origin = 'paramsemp.NFeItsLin'
      Required = True
    end
    object QrPrmsEmpNFeNFeFTRazao: TSmallintField
      FieldName = 'NFeFTRazao'
      Origin = 'paramsemp.NFeFTRazao'
      Required = True
    end
    object QrPrmsEmpNFeNFeFTEnder: TSmallintField
      FieldName = 'NFeFTEnder'
      Origin = 'paramsemp.NFeFTEnder'
      Required = True
    end
    object QrPrmsEmpNFeNFeFTFones: TSmallintField
      FieldName = 'NFeFTFones'
      Origin = 'paramsemp.NFeFTFones'
      Required = True
    end
    object QrPrmsEmpNFePathLogoNF: TWideStringField
      FieldName = 'PathLogoNF'
      Origin = 'paramsemp.PathLogoNF'
      Size = 255
    end
    object QrPrmsEmpNFeDirDANFEs: TWideStringField
      FieldName = 'DirDANFEs'
      Origin = 'paramsemp.DirDANFEs'
      Size = 255
    end
    object QrPrmsEmpNFeNFe_indFinalCpl: TSmallintField
      FieldName = 'NFe_indFinalCpl'
      Origin = 'paramsemp.NFe_indFinalCpl'
      Required = True
    end
    object QrPrmsEmpNFeNoDANFEMail: TSmallintField
      FieldName = 'NoDANFEMail'
      Origin = 'paramsemp.NoDANFEMail'
      Required = True
    end
    object QrPrmsEmpNFeNFeMaiusc: TSmallintField
      FieldName = 'NFeMaiusc'
      Origin = 'paramsemp.NFeMaiusc'
      Required = True
    end
    object QrPrmsEmpNFeNFeShowURL: TWideStringField
      FieldName = 'NFeShowURL'
      Origin = 'paramsemp.NFeShowURL'
      Size = 255
    end
    object QrPrmsEmpNFeNFeNT2013_003LTT: TSmallintField
      FieldName = 'NFeNT2013_003LTT'
      Origin = 'paramsemp.NFeNT2013_003LTT'
      Required = True
    end
    object QrPrmsEmpNFeDirRetNfeDes: TWideStringField
      FieldName = 'DirRetNfeDes'
      Size = 255
    end
    object QrPrmsEmpNFeDirEvePedMDe: TWideStringField
      FieldName = 'DirEvePedMDe'
      Size = 255
    end
    object QrPrmsEmpNFeDirEveRetMDe: TWideStringField
      FieldName = 'DirEveRetMDe'
      Size = 255
    end
    object QrPrmsEmpNFeDirDowNFeDes: TWideStringField
      FieldName = 'DirDowNFeDes'
      Size = 255
    end
    object QrPrmsEmpNFeDirDowNFeNFe: TWideStringField
      FieldName = 'DirDowNFeNFe'
      Size = 255
    end
    object QrPrmsEmpNFeDirDistDFeInt: TWideStringField
      FieldName = 'DirDistDFeInt'
      Size = 255
    end
    object QrPrmsEmpNFeDirRetDistDFeInt: TWideStringField
      FieldName = 'DirRetDistDFeInt'
      Size = 255
    end
    object QrPrmsEmpNFeDirDowNFeCnf: TWideStringField
      FieldName = 'DirDowNFeCnf'
      Size = 255
    end
    object QrPrmsEmpNFeNFeInfCpl: TIntegerField
      FieldName = 'NFeInfCpl'
    end
  end
  object DsParamsEmp: TDataSource
    DataSet = QrParamsEmp
    Left = 289
    Top = 578
  end
  object DsPrmsEmpNFe: TDataSource
    DataSet = QrPrmsEmpNFe
    Left = 393
    Top = 586
  end
  object QrParamsEmp: TMySQLQuery
    Database = Dmod.ZZDB
    AfterOpen = QrParamsEmpAfterOpen
    BeforeClose = QrParamsEmpBeforeClose
    SQL.Strings = (
      'SELECT'
      'imp.Nome NO_AssocModNF,'
      'CONCAT(mda.Sigla," - ",mda.Nome) NOMEMOEDA,'
      'mda.CodUsu + 0.000 CODUSU_MDA,'
      'ass.Filial FI_ASSOCIADA,'
      'IF(ass.Tipo=0,ass.RazaoSocial,ass.Nome) NO_ASSOCIADA,'
      'IF(ent.Tipo=0,ent.RazaoSocial,ent.Nome) NOMEFILIAL,'
      'IF(ent.Tipo=0,ent.CNPJ,ent.CPF) CNPJCPF_FILIAL,'
      'ent.Filial, ent.Codigo Entidade,'
      'c01.Nome NO_CtaProdVen, c02.Nome NO_CtaServico,'
      'c03.Nome NO_CtaProdCom, c04.Nome NO_CtaServicoPg,'
      'chf.Nome NO_CartEmisHonFun,'
      'pe4.Nome NO_NFSePreMailAut,'
      'pe5.Nome NO_NFSePreMailCan,'
      'et0.Nome NO_EntiTipCto, et1.Nome NO_EntiTipCt1,'
      'IF(ctd.Tipo=0, ctd.RazaoSocial, ctd.Nome) NO_CTD,'
      'IF(ctb.Tipo=0, ctb.RazaoSocial, ctb.Nome) NO_CTB,'
      
        'IF(pem.hVeraoAsk <= "1899-12-30", "", DATE_FORMAT(pem.hVeraoAsk,' +
        ' "%d/%m/%Y")) hVeraoAsk_TXT,'
      
        'IF(pem.hVeraoIni <= "1899-12-30", "", DATE_FORMAT(pem.hVeraoIni,' +
        ' "%d/%m/%Y")) hVeraoIni_TXT,'
      
        'IF(pem.hVeraoFim <= "1899-12-30", "", DATE_FORMAT(pem.hVeraoFim,' +
        ' "%d/%m/%Y")) hVeraoFim_TXT,'
      'cpl.Nome NFeInfCpl_TXT,'
      'et2.Nome NO_NFSeTipCtoMail,'
      '    pem.Codigo,'
      '    pem.Logo3x1,'
      '    pem.TZD_UTC,'
      '    pem.hVeraoAsk,'
      '    pem.hVeraoIni,'
      '    pem.hVeraoFim,'
      '    pem.TZD_UTC_Auto,'
      '    pem.NFeNT2013_003LTT,'
      '    pem.CartEmisHonFun,'
      '    pem.Situacao,'
      '    pem.FatSemEstq,'
      '    pem.TipMediaDD,'
      '    pem.FatSemPrcL,'
      '    pem.TipCalcJuro,'
      '    pem.PedVdaMudLista,'
      '    pem.PedVdaMudPrazo,'
      '    pem.PediVdaNElertas,'
      '    pem.AssocModNF,'
      '    pem.FaturaSeq,'
      '    pem.FaturaSep,'
      '    pem.FaturaDta,'
      '    pem.CtaProdVen,'
      '    pem.TxtProdVen,'
      '    pem.CtaServico,'
      '    pem.TxtServico,'
      '    pem.DupProdVen,'
      '    pem.DupServico,'
      '    pem.FaturaNum,'
      '    pem.UsaReferen,'
      '    pem.CtaProdCom,'
      '    pem.TxtProdCom,'
      '    pem.DupProdCom,'
      '    pem.CtaServicoPg,'
      '    pem.TxtServicoPg,'
      '    pem.DupServicoPg,'
      '    pem.CtaFretPrest,'
      '    pem.TxtFretPrest,'
      '    pem.DupFretPrest,'
      '    pem.FreteRpICMS,'
      '    pem.FreteRpPIS,'
      '    pem.FreteRpCOFINS,'
      '    pem.RetImpost,'
      '    pem.BalQtdItem,'
      '    pem.Estq0UsoCons,'
      '    pem.PreMailAut,'
      '    pem.PreMailCan,'
      '    pem.PreMailEveCCe,'
      '    pem.DirNFeProt,'
      '    pem.DirNFeRWeb,'
      '    pem.SCAN_Ser,'
      '    pem.SCAN_nNF,'
      '    pem.infRespTec_xContato,'
      '    pem.infRespTec_CNPJ,'
      '    pem.infRespTec_fone,'
      '    pem.infRespTec_email,'
      '    pem.infRespTec_idCSRT,'
      '    pem.infRespTec_CSRT,'
      '    pem.infRespTec_Usa,'
      '    pem.UF_WebServ,'
      '    pem.SiglaCustm,'
      '    pem.UF_Servico,'
      '    pem.SimplesFed,'
      '    pem.InfoPerCuz,'
      '    pem.NFeSerNum,'
      '    pem.NFeSerVal,'
      '    pem.NFeSerAvi,'
      '    pem.UF_MDeMDe,'
      '    pem.UF_MDeDes,'
      '    pem.UF_MDeNFe,'
      '    pem.NFeUF_EPEC,'
      '    pem.DirEnvLot,'
      '    pem.DirRec,'
      '    pem.DirProRec,'
      '    pem.DirNFeAss,'
      '    pem.DirNFeGer,'
      '    pem.DirDen,'
      '    pem.DirPedRec,'
      '    pem.DirPedSta,'
      '    pem.DirSta,'
      '    pem.DirPedCan,'
      '    pem.DirCan,'
      '    pem.DirPedInu,'
      '    pem.DirInu,'
      '    pem.DirPedSit,'
      '    pem.DirSit,'
      '    pem.DirEveEnvLot,'
      '    pem.DirEveRetLot,'
      '    pem.DirEvePedCCe,'
      '    pem.DirEveRetCCe,'
      '    pem.DirEvePedCan,'
      '    pem.DirEveRetCan,'
      '    pem.DirEveProcCCe,'
      '    pem.DirRetNfeDes,'
      '    pem.DirEvePedMDe,'
      '    pem.DirEveRetMDe,'
      '    pem.DirDowNFeDes,'
      '    pem.DirDowNFeNFe,'
      '    pem.DirDistDFeInt,'
      '    pem.DirRetDistDFeInt,'
      '    pem.DirDowNFeCnf,'
      '    pem.versao,'
      '    pem.ide_mod,'
      '    pem.ide_tpAmb,'
      '    pem.DirSchema,'
      '    pem.NT2018_05v120,'
      '    pem.AppCode,'
      '    pem.AssDigMode,'
      '    pem.MyEmailNFe,'
      '    pem.NFeInfCpl,'
      '    pem.CRT,'
      '    pem.CSOSN,'
      '    pem.pCredSNAlq,'
      '    pem.pCredSNMez,'
      '    pem.NFeVerStaSer,'
      '    pem.NFeVerEnvLot,'
      '    pem.NFeVerConLot,'
      '    pem.NFeVerCanNFe,'
      '    pem.NFeVerInuNum,'
      '    pem.NFeVerConNFe,'
      '    pem.NFeVerLotEve,'
      '    pem.NFeVerConsCad,'
      '    pem.NFeVerDistDFeInt,'
      '    pem.NFeVerDowNFe,'
      '    pem.ide_tpImp,'
      '    pem.NFeItsLin,'
      '    pem.NFeFTRazao,'
      '    pem.NFeFTEnder,'
      '    pem.NFeFTFones,'
      '    pem.NFeMaiusc,'
      '    pem.NFeShowURL,'
      '    pem.PathLogoNF,'
      '    pem.DirDANFEs,'
      '    pem.NFe_indFinalCpl,'
      '    pem.NoDANFEMail,'
      '    pem.SPED_EFD_IND_PERFIL,'
      '    pem.SPED_EFD_IND_ATIV,'
      '    pem.SPED_EFD_CadContador,'
      '    pem.SPED_EFD_CRCContador,'
      '    pem.SPED_EFD_EscriContab,'
      '    pem.SPED_EFD_EnderContab,'
      '    pem.SPED_EFD_DtFiscal,'
      '    pem.SPED_EFD_ID_0150,'
      '    pem.SPED_EFD_ID_0200,'
      '    pem.SPED_EFD_Peri_K100,'
      '    pem.SPED_EFD_Peri_E500,'
      '    pem.SPED_EFD_Peri_E100,'
      '    pem.SPED_EFD_Producao,'
      '    pem.SPED_EFD_MovSubPrd,'
      '    pem.SPED_EFD_Path,'
      '    pem.SPED_EFD_ICMS_IPI_VersaoGuia,'
      '    pem.NFSeMetodo,'
      '    pem.NFSeVersao,'
      '    pem.NFSeSerieRps,'
      '    pem.DpsNumero,'
      '    pem.NFSeWSProducao,'
      '    pem.NFSeAmbiente,'
      '    pem.NFSeWSHomologa,'
      '    pem.NFSeCertDigital,'
      '    pem.NFSeCertValidad,'
      '    pem.NFSeCertAviExpi,'
      '    pem.RPSNumLote,'
      '    pem.NFSeUserWeb,'
      '    pem.NFSeSenhaWeb,'
      '    pem.NFSeCodMunici,'
      '    pem.NFSePrefeitura1,'
      '    pem.NFSeMsgVisu,'
      '    pem.NFSeLogoFili,'
      '    pem.NFSeLogoPref,'
      '    pem.NFSePrefeitura2,'
      '    pem.NFSePrefeitura3,'
      '    pem.NFSeMetodEnvRPS,'
      '    pem.NFSeTipoRps,'
      '    pem.NFSe_indFinalCpl,'
      '    pem.DirNFSeRPSEnvLot,'
      '    pem.DirNFSeRPSRecLot,'
      '    pem.DirNFSeDPSAss,'
      '    pem.DirNFSeDPSGer,'
      '    pem.DirNFSeLogs,'
      '    pem.DirNFSeSchema,'
      '    pem.NFSePreMailAut,'
      '    pem.NFSePreMailCan,'
      '    pem.MyEmailNFSe,'
      '    pem.CTetpEmis,'
      '    pem.CTePreMailAut,'
      '    pem.CTePreMailCan,'
      '    pem.CTePreMailEveCCe,'
      '    pem.DirCTeProt,'
      '    pem.DirCTeRWeb,'
      '    pem.CTeSCAN_Ser,'
      '    pem.CTeSCAN_nCT,'
      '    pem.CTeUF_WebServ,'
      '    pem.CTeUF_Servico,'
      '    pem.CTeSerNum,'
      '    pem.CTeSerVal,'
      '    pem.CTeSerAvi,'
      '    pem.CTeUF_EPEC,'
      '    pem.CTeUF_Conting,'
      '    pem.DirCTeEnvLot,'
      '    pem.DirCTeRec,'
      '    pem.DirCTeProRec,'
      '    pem.DirCTeAss,'
      '    pem.DirCTeGer,'
      '    pem.DirCTeDen,'
      '    pem.DirCTePedRec,'
      '    pem.DirCTePedSta,'
      '    pem.DirCTeSta,'
      '    pem.DirCTePedCan,'
      '    pem.DirCTeCan,'
      '    pem.DirCTePedInu,'
      '    pem.DirCTeInu,'
      '    pem.DirCTePedSit,'
      '    pem.DirCTeSit,'
      '    pem.DirCTeEveEnvLot,'
      '    pem.DirCTeEveRetLot,'
      '    pem.DirCTeEvePedCCe,'
      '    pem.DirCTeEveRetCCe,'
      '    pem.DirCTeEvePedCan,'
      '    pem.DirCTeEveRetCan,'
      '    pem.DirCTEEveProcCCe,'
      '    pem.DirCTeEvePedEPEC,'
      '    pem.DirCTeEveRetEPEC,'
      '    pem.CTeversao,'
      '    pem.CTeide_mod,'
      '    pem.CTeide_tpAmb,'
      '    pem.DirCTeSchema,'
      '    pem.CTeAppCode,'
      '    pem.CTeAssDigMode,'
      '    pem.MyEmailCTe,'
      '    pem.CTeInfCpl,'
      '    pem.CTeVerStaSer,'
      '    pem.CTeVerEnvLot,'
      '    pem.CTeVerConLot,'
      '    pem.CTeVerInuNum,'
      '    pem.CTeVerConCTe,'
      '    pem.CTeVerLotEve,'
      '    pem.CTeVerEPEC,'
      '    pem.CTeide_tpImp,'
      '    pem.CTeItsLin,'
      '    pem.CTeFTRazao,'
      '    pem.CTeFTEnder,'
      '    pem.CTeFTFones,'
      '    pem.CTeMaiusc,'
      '    pem.CTeShowURL,'
      '    pem.PathLogoCTe,'
      '    pem.DirDACTes,'
      '    pem.CTe_indFinalCpl,'
      '    pem.MDFePreMailAut,'
      '    pem.MDFePreMailCan,'
      '    pem.MDFePreMailEveCCe,'
      '    pem.DirMDFeProt,'
      '    pem.DirMDFeRWeb,'
      '    pem.MDFeSCAN_Ser,'
      '    pem.MDFeSCAN_nMDF,'
      '    pem.MDFeUF_WebServ,'
      '    pem.MDFeUF_Servico,'
      '    pem.MDFeSerNum,'
      '    pem.MDFeSerVal,'
      '    pem.MDFeSerAvi,'
      '    pem.DirMDFeEnvLot,'
      '    pem.DirMDFeRec,'
      '    pem.DirMDFeProRec,'
      '    pem.DirMDFeAss,'
      '    pem.DirMDFeGer,'
      '    pem.DirMDFeDen,'
      '    pem.DirMDFePedRec,'
      '    pem.DirMDFePedSta,'
      '    pem.DirMDFeSta,'
      '    pem.DirMDFePedCan,'
      '    pem.DirMDFeCan,'
      '    pem.DirMDFePedInu,'
      '    pem.DirMDFeInu,'
      '    pem.DirMDFePedSit,'
      '    pem.DirMDFeSit,'
      '    pem.DirMDFeEveEnvLot,'
      '    pem.DirMDFeEveRetLot,'
      '    pem.DirMDFeEvePedEnc,'
      '    pem.DirMDFeEveRetEnc,'
      '    pem.DirMDFeEvePedCan,'
      '    pem.DirMDFeEveRetCan,'
      '    pem.DirMDFeEvePedIdC,'
      '    pem.DirMDFeEveRetIdC,'
      '    pem.MDFeversao,'
      '    pem.MDFeide_mod,'
      '    pem.MDFeide_tpAmb,'
      '    pem.DirMDFeSchema,'
      '    pem.MDFeAppCode,'
      '    pem.MDFeAssDigMode,'
      '    pem.MyEmailMDFe,'
      '    pem.MDFeInfCpl,'
      '    pem.MDFeVerStaSer,'
      '    pem.MDFeVerEnvLot,'
      '    pem.MDFeVerConLot,'
      '    pem.MDFeVerInuNum,'
      '    pem.MDFeVerConMDFe,'
      '    pem.MDFeVerLotEve,'
      '    pem.MDFeide_tpImp,'
      '    pem.MDFeItsLin,'
      '    pem.MDFeFTRazao,'
      '    pem.MDFeFTEnder,'
      '    pem.MDFeFTFones,'
      '    pem.MDFeMaiusc,'
      '    pem.MDFeShowURL,'
      '    pem.PathLogoMDFe,'
      '    pem.DirDAMDFes,'
      '    pem.MDFe_indFinalCpl,'
      '    pem.Moeda,'
      '    pem.Associada,'
      '    pem.EntiTipCt1,'
      '    pem.EntiTipCto,'
      '    pem.NFSeTipCtoMail,'
      '    pem.CTeEntiTipCt1,'
      '    pem.CTeEntiTipCto'
      ''
      'FROM entidades ent'
      'LEFT JOIN paramsemp pem ON pem.Codigo=ent.Codigo'
      'LEFT JOIN nfeinfcpl cpl ON cpl.Codigo=pem.NFeInfCpl'
      'LEFT JOIN cambiomda mda ON mda.Codigo=pem.Moeda'
      'LEFT JOIN entidades ass ON ass.Codigo=pem.Associada'
      'LEFT JOIN imprime   imp ON imp.Codigo=pem.AssocModNF'
      'LEFT JOIN contas c01 ON c01.Codigo=pem.CtaProdVen'
      'LEFT JOIN contas c02 ON c02.Codigo=pem.CtaServico'
      'LEFT JOIN contas c03 ON c03.Codigo=pem.CtaProdCom'
      'LEFT JOIN contas c04 ON c04.Codigo=pem.CtaServicoPg'
      'LEFT JOIN carteiras chf ON chf.Codigo=pem.CartEmisHonFun'
      'LEFT JOIN preemail pe1 ON pe1.Codigo=pem.PreMailAut'
      'LEFT JOIN preemail pe2 ON pe2.Codigo=pem.PreMailCan'
      'LEFT JOIN preemail pe3 ON pe3.Codigo=pem.PreMailEveCCe'
      'LEFT JOIN preemail pe4 ON pe4.Codigo=pem.NFSePreMailAut'
      'LEFT JOIN preemail pe5 ON pe5.Codigo=pem.NFSePreMailCan'
      'LEFT JOIN entitipcto et0 ON et0.Codigo=pem.EntiTipCto'
      'LEFT JOIN entitipcto et1 ON et1.Codigo=pem.EntiTipCt1'
      'LEFT JOIN entitipcto et2 ON et2.Codigo=pem.NFSeTipCtoMail'
      'LEFT JOIN entidades ctd ON ctd.Codigo=pem.SPED_EFD_CadContador'
      'LEFT JOIN entidades ctb ON ctb.Codigo=pem.SPED_EFD_EscriContab'
      ''
      'WHERE ent.Codigo=-11')
    Left = 288
    Top = 532
    object QrParamsEmpNO_AssocModNF: TWideStringField
      FieldName = 'NO_AssocModNF'
      Size = 100
    end
    object QrParamsEmpNOMEMOEDA: TWideStringField
      FieldName = 'NOMEMOEDA'
      Size = 38
    end
    object QrParamsEmpCODUSU_MDA: TFloatField
      FieldName = 'CODUSU_MDA'
    end
    object QrParamsEmpFI_ASSOCIADA: TIntegerField
      FieldName = 'FI_ASSOCIADA'
      Required = True
    end
    object QrParamsEmpNO_ASSOCIADA: TWideStringField
      FieldName = 'NO_ASSOCIADA'
      Size = 100
    end
    object QrParamsEmpNOMEFILIAL: TWideStringField
      FieldName = 'NOMEFILIAL'
      Size = 100
    end
    object QrParamsEmpCNPJCPF_FILIAL: TWideStringField
      FieldName = 'CNPJCPF_FILIAL'
      Size = 18
    end
    object QrParamsEmpFilial: TIntegerField
      FieldName = 'Filial'
      Required = True
    end
    object QrParamsEmpEntidade: TIntegerField
      FieldName = 'Entidade'
      Required = True
    end
    object QrParamsEmpNO_CtaProdVen: TWideStringField
      FieldName = 'NO_CtaProdVen'
      Required = True
      Size = 50
    end
    object QrParamsEmpNO_CtaServico: TWideStringField
      FieldName = 'NO_CtaServico'
      Required = True
      Size = 50
    end
    object QrParamsEmpNO_CtaProdCom: TWideStringField
      FieldName = 'NO_CtaProdCom'
      Required = True
      Size = 50
    end
    object QrParamsEmpNO_CtaServicoPg: TWideStringField
      FieldName = 'NO_CtaServicoPg'
      Required = True
      Size = 50
    end
    object QrParamsEmpNO_CartEmisHonFun: TWideStringField
      FieldName = 'NO_CartEmisHonFun'
      Required = True
      Size = 100
    end
    object QrParamsEmpNO_NFSePreMailAut: TWideStringField
      FieldName = 'NO_NFSePreMailAut'
      Size = 100
    end
    object QrParamsEmpNO_NFSePreMailCan: TWideStringField
      FieldName = 'NO_NFSePreMailCan'
      Size = 100
    end
    object QrParamsEmpNO_EntiTipCto: TWideStringField
      FieldName = 'NO_EntiTipCto'
      Required = True
      Size = 30
    end
    object QrParamsEmpNO_EntiTipCt1: TWideStringField
      FieldName = 'NO_EntiTipCt1'
      Required = True
      Size = 30
    end
    object QrParamsEmpNO_CTD: TWideStringField
      FieldName = 'NO_CTD'
      Size = 100
    end
    object QrParamsEmpNO_CTB: TWideStringField
      FieldName = 'NO_CTB'
      Size = 100
    end
    object QrParamsEmphVeraoAsk_TXT: TWideStringField
      FieldName = 'hVeraoAsk_TXT'
      Size = 10
    end
    object QrParamsEmphVeraoIni_TXT: TWideStringField
      FieldName = 'hVeraoIni_TXT'
      Size = 10
    end
    object QrParamsEmphVeraoFim_TXT: TWideStringField
      FieldName = 'hVeraoFim_TXT'
      Size = 10
    end
    object QrParamsEmpNFeInfCpl_TXT: TWideStringField
      FieldName = 'NFeInfCpl_TXT'
      Size = 100
    end
    object QrParamsEmpNO_NFSeTipCtoMail: TWideStringField
      FieldName = 'NO_NFSeTipCtoMail'
      Required = True
      Size = 30
    end
    object QrParamsEmpCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrParamsEmpLogo3x1: TWideStringField
      FieldName = 'Logo3x1'
      Size = 255
    end
    object QrParamsEmpTZD_UTC: TFloatField
      FieldName = 'TZD_UTC'
      Required = True
    end
    object QrParamsEmphVeraoAsk: TDateField
      FieldName = 'hVeraoAsk'
      Required = True
    end
    object QrParamsEmphVeraoIni: TDateField
      FieldName = 'hVeraoIni'
      Required = True
    end
    object QrParamsEmphVeraoFim: TDateField
      FieldName = 'hVeraoFim'
      Required = True
    end
    object QrParamsEmpTZD_UTC_Auto: TSmallintField
      FieldName = 'TZD_UTC_Auto'
      Required = True
    end
    object QrParamsEmpNFeNT2013_003LTT: TSmallintField
      FieldName = 'NFeNT2013_003LTT'
      Required = True
    end
    object QrParamsEmpCartEmisHonFun: TIntegerField
      FieldName = 'CartEmisHonFun'
      Required = True
    end
    object QrParamsEmpSituacao: TIntegerField
      FieldName = 'Situacao'
      Required = True
    end
    object QrParamsEmpFatSemEstq: TSmallintField
      FieldName = 'FatSemEstq'
      Required = True
    end
    object QrParamsEmpTipMediaDD: TSmallintField
      FieldName = 'TipMediaDD'
      Required = True
    end
    object QrParamsEmpFatSemPrcL: TSmallintField
      FieldName = 'FatSemPrcL'
      Required = True
    end
    object QrParamsEmpTipCalcJuro: TSmallintField
      FieldName = 'TipCalcJuro'
      Required = True
    end
    object QrParamsEmpPedVdaMudLista: TSmallintField
      FieldName = 'PedVdaMudLista'
    end
    object QrParamsEmpPedVdaMudPrazo: TSmallintField
      FieldName = 'PedVdaMudPrazo'
    end
    object QrParamsEmpPediVdaNElertas: TSmallintField
      FieldName = 'PediVdaNElertas'
      Required = True
    end
    object QrParamsEmpAssocModNF: TIntegerField
      FieldName = 'AssocModNF'
      Required = True
    end
    object QrParamsEmpFaturaSeq: TSmallintField
      FieldName = 'FaturaSeq'
      Required = True
    end
    object QrParamsEmpFaturaSep: TWideStringField
      FieldName = 'FaturaSep'
      Required = True
      Size = 1
    end
    object QrParamsEmpFaturaDta: TSmallintField
      FieldName = 'FaturaDta'
      Required = True
    end
    object QrParamsEmpCtaProdVen: TIntegerField
      FieldName = 'CtaProdVen'
      Required = True
    end
    object QrParamsEmpTxtProdVen: TWideStringField
      FieldName = 'TxtProdVen'
      Required = True
      Size = 100
    end
    object QrParamsEmpCtaServico: TIntegerField
      FieldName = 'CtaServico'
      Required = True
    end
    object QrParamsEmpTxtServico: TWideStringField
      FieldName = 'TxtServico'
      Required = True
      Size = 100
    end
    object QrParamsEmpDupProdVen: TWideStringField
      FieldName = 'DupProdVen'
      Required = True
      Size = 3
    end
    object QrParamsEmpDupServico: TWideStringField
      FieldName = 'DupServico'
      Required = True
      Size = 3
    end
    object QrParamsEmpFaturaNum: TSmallintField
      FieldName = 'FaturaNum'
      Required = True
    end
    object QrParamsEmpUsaReferen: TSmallintField
      FieldName = 'UsaReferen'
      Required = True
    end
    object QrParamsEmpCtaProdCom: TIntegerField
      FieldName = 'CtaProdCom'
      Required = True
    end
    object QrParamsEmpTxtProdCom: TWideStringField
      FieldName = 'TxtProdCom'
      Required = True
      Size = 100
    end
    object QrParamsEmpDupProdCom: TWideStringField
      FieldName = 'DupProdCom'
      Required = True
      Size = 3
    end
    object QrParamsEmpCtaServicoPg: TIntegerField
      FieldName = 'CtaServicoPg'
      Required = True
    end
    object QrParamsEmpTxtServicoPg: TWideStringField
      FieldName = 'TxtServicoPg'
      Required = True
      Size = 100
    end
    object QrParamsEmpDupServicoPg: TWideStringField
      FieldName = 'DupServicoPg'
      Required = True
      Size = 3
    end
    object QrParamsEmpCtaFretPrest: TIntegerField
      FieldName = 'CtaFretPrest'
      Required = True
    end
    object QrParamsEmpTxtFretPrest: TWideStringField
      FieldName = 'TxtFretPrest'
      Required = True
      Size = 100
    end
    object QrParamsEmpDupFretPrest: TWideStringField
      FieldName = 'DupFretPrest'
      Required = True
      Size = 3
    end
    object QrParamsEmpFreteRpICMS: TFloatField
      FieldName = 'FreteRpICMS'
      Required = True
    end
    object QrParamsEmpFreteRpPIS: TFloatField
      FieldName = 'FreteRpPIS'
      Required = True
    end
    object QrParamsEmpFreteRpCOFINS: TFloatField
      FieldName = 'FreteRpCOFINS'
      Required = True
    end
    object QrParamsEmpRetImpost: TSmallintField
      FieldName = 'RetImpost'
      Required = True
    end
    object QrParamsEmpBalQtdItem: TFloatField
      FieldName = 'BalQtdItem'
      Required = True
    end
    object QrParamsEmpEstq0UsoCons: TSmallintField
      FieldName = 'Estq0UsoCons'
      Required = True
    end
    object QrParamsEmpPreMailAut: TIntegerField
      FieldName = 'PreMailAut'
      Required = True
    end
    object QrParamsEmpPreMailCan: TIntegerField
      FieldName = 'PreMailCan'
      Required = True
    end
    object QrParamsEmpPreMailEveCCe: TIntegerField
      FieldName = 'PreMailEveCCe'
      Required = True
    end
    object QrParamsEmpDirNFeProt: TWideStringField
      FieldName = 'DirNFeProt'
      Size = 255
    end
    object QrParamsEmpDirNFeRWeb: TWideStringField
      FieldName = 'DirNFeRWeb'
      Size = 255
    end
    object QrParamsEmpSCAN_Ser: TIntegerField
      FieldName = 'SCAN_Ser'
      Required = True
    end
    object QrParamsEmpSCAN_nNF: TIntegerField
      FieldName = 'SCAN_nNF'
      Required = True
    end
    object QrParamsEmpinfRespTec_xContato: TWideStringField
      FieldName = 'infRespTec_xContato'
      Required = True
      Size = 60
    end
    object QrParamsEmpinfRespTec_CNPJ: TWideStringField
      FieldName = 'infRespTec_CNPJ'
      Required = True
      Size = 14
    end
    object QrParamsEmpinfRespTec_fone: TWideStringField
      FieldName = 'infRespTec_fone'
      Required = True
      Size = 14
    end
    object QrParamsEmpinfRespTec_email: TWideStringField
      FieldName = 'infRespTec_email'
      Required = True
      Size = 60
    end
    object QrParamsEmpinfRespTec_idCSRT: TWideStringField
      FieldName = 'infRespTec_idCSRT'
      Required = True
      Size = 2
    end
    object QrParamsEmpinfRespTec_CSRT: TWideStringField
      FieldName = 'infRespTec_CSRT'
      Size = 120
    end
    object QrParamsEmpinfRespTec_Usa: TSmallintField
      FieldName = 'infRespTec_Usa'
      Required = True
    end
    object QrParamsEmpUF_WebServ: TWideStringField
      FieldName = 'UF_WebServ'
      Required = True
      Size = 2
    end
    object QrParamsEmpSiglaCustm: TWideStringField
      FieldName = 'SiglaCustm'
      Size = 15
    end
    object QrParamsEmpUF_Servico: TWideStringField
      FieldName = 'UF_Servico'
      Required = True
      Size = 10
    end
    object QrParamsEmpSimplesFed: TSmallintField
      FieldName = 'SimplesFed'
      Required = True
    end
    object QrParamsEmpInfoPerCuz: TSmallintField
      FieldName = 'InfoPerCuz'
      Required = True
    end
    object QrParamsEmpNFeSerNum: TWideStringField
      FieldName = 'NFeSerNum'
      Size = 255
    end
    object QrParamsEmpNFeSerVal: TDateField
      FieldName = 'NFeSerVal'
      Required = True
    end
    object QrParamsEmpNFeSerAvi: TSmallintField
      FieldName = 'NFeSerAvi'
      Required = True
    end
    object QrParamsEmpUF_MDeMDe: TWideStringField
      FieldName = 'UF_MDeMDe'
      Required = True
      Size = 10
    end
    object QrParamsEmpUF_MDeDes: TWideStringField
      FieldName = 'UF_MDeDes'
      Required = True
      Size = 10
    end
    object QrParamsEmpUF_MDeNFe: TWideStringField
      FieldName = 'UF_MDeNFe'
      Required = True
      Size = 10
    end
    object QrParamsEmpNFeUF_EPEC: TWideStringField
      FieldName = 'NFeUF_EPEC'
      Required = True
      Size = 10
    end
    object QrParamsEmpDirEnvLot: TWideStringField
      FieldName = 'DirEnvLot'
      Size = 255
    end
    object QrParamsEmpDirRec: TWideStringField
      FieldName = 'DirRec'
      Size = 255
    end
    object QrParamsEmpDirProRec: TWideStringField
      FieldName = 'DirProRec'
      Size = 255
    end
    object QrParamsEmpDirNFeAss: TWideStringField
      FieldName = 'DirNFeAss'
      Size = 255
    end
    object QrParamsEmpDirNFeGer: TWideStringField
      FieldName = 'DirNFeGer'
      Size = 255
    end
    object QrParamsEmpDirDen: TWideStringField
      FieldName = 'DirDen'
      Size = 255
    end
    object QrParamsEmpDirPedRec: TWideStringField
      FieldName = 'DirPedRec'
      Size = 255
    end
    object QrParamsEmpDirPedSta: TWideStringField
      FieldName = 'DirPedSta'
      Size = 255
    end
    object QrParamsEmpDirSta: TWideStringField
      FieldName = 'DirSta'
      Size = 255
    end
    object QrParamsEmpDirPedCan: TWideStringField
      FieldName = 'DirPedCan'
      Size = 255
    end
    object QrParamsEmpDirCan: TWideStringField
      FieldName = 'DirCan'
      Size = 255
    end
    object QrParamsEmpDirPedInu: TWideStringField
      FieldName = 'DirPedInu'
      Size = 255
    end
    object QrParamsEmpDirInu: TWideStringField
      FieldName = 'DirInu'
      Size = 255
    end
    object QrParamsEmpDirPedSit: TWideStringField
      FieldName = 'DirPedSit'
      Size = 255
    end
    object QrParamsEmpDirSit: TWideStringField
      FieldName = 'DirSit'
      Size = 255
    end
    object QrParamsEmpDirEveEnvLot: TWideStringField
      FieldName = 'DirEveEnvLot'
      Size = 255
    end
    object QrParamsEmpDirEveRetLot: TWideStringField
      FieldName = 'DirEveRetLot'
      Size = 255
    end
    object QrParamsEmpDirEvePedCCe: TWideStringField
      FieldName = 'DirEvePedCCe'
      Size = 255
    end
    object QrParamsEmpDirEveRetCCe: TWideStringField
      FieldName = 'DirEveRetCCe'
      Size = 255
    end
    object QrParamsEmpDirEvePedCan: TWideStringField
      FieldName = 'DirEvePedCan'
      Size = 255
    end
    object QrParamsEmpDirEveRetCan: TWideStringField
      FieldName = 'DirEveRetCan'
      Size = 255
    end
    object QrParamsEmpDirEveProcCCe: TWideStringField
      FieldName = 'DirEveProcCCe'
      Size = 255
    end
    object QrParamsEmpDirRetNfeDes: TWideStringField
      FieldName = 'DirRetNfeDes'
      Size = 255
    end
    object QrParamsEmpDirEvePedMDe: TWideStringField
      FieldName = 'DirEvePedMDe'
      Size = 255
    end
    object QrParamsEmpDirEveRetMDe: TWideStringField
      FieldName = 'DirEveRetMDe'
      Size = 255
    end
    object QrParamsEmpDirDowNFeDes: TWideStringField
      FieldName = 'DirDowNFeDes'
      Size = 255
    end
    object QrParamsEmpDirDowNFeNFe: TWideStringField
      FieldName = 'DirDowNFeNFe'
      Size = 255
    end
    object QrParamsEmpDirDistDFeInt: TWideStringField
      FieldName = 'DirDistDFeInt'
      Size = 255
    end
    object QrParamsEmpDirRetDistDFeInt: TWideStringField
      FieldName = 'DirRetDistDFeInt'
      Size = 255
    end
    object QrParamsEmpDirDowNFeCnf: TWideStringField
      FieldName = 'DirDowNFeCnf'
      Size = 255
    end
    object QrParamsEmpversao: TFloatField
      FieldName = 'versao'
      Required = True
    end
    object QrParamsEmpide_mod: TSmallintField
      FieldName = 'ide_mod'
      Required = True
    end
    object QrParamsEmpide_tpAmb: TSmallintField
      FieldName = 'ide_tpAmb'
      Required = True
    end
    object QrParamsEmpDirSchema: TWideStringField
      FieldName = 'DirSchema'
      Size = 255
    end
    object QrParamsEmpNT2018_05v120: TSmallintField
      FieldName = 'NT2018_05v120'
      Required = True
    end
    object QrParamsEmpAppCode: TSmallintField
      FieldName = 'AppCode'
      Required = True
    end
    object QrParamsEmpAssDigMode: TSmallintField
      FieldName = 'AssDigMode'
      Required = True
    end
    object QrParamsEmpMyEmailNFe: TWideStringField
      FieldName = 'MyEmailNFe'
      Size = 255
    end
    object QrParamsEmpNFeInfCpl: TIntegerField
      FieldName = 'NFeInfCpl'
      Required = True
    end
    object QrParamsEmpCRT: TSmallintField
      FieldName = 'CRT'
      Required = True
    end
    object QrParamsEmpCSOSN: TIntegerField
      FieldName = 'CSOSN'
      Required = True
    end
    object QrParamsEmppCredSNAlq: TFloatField
      FieldName = 'pCredSNAlq'
      Required = True
    end
    object QrParamsEmppCredSNMez: TIntegerField
      FieldName = 'pCredSNMez'
      Required = True
    end
    object QrParamsEmpNFeVerStaSer: TFloatField
      FieldName = 'NFeVerStaSer'
      Required = True
    end
    object QrParamsEmpNFeVerEnvLot: TFloatField
      FieldName = 'NFeVerEnvLot'
      Required = True
    end
    object QrParamsEmpNFeVerConLot: TFloatField
      FieldName = 'NFeVerConLot'
      Required = True
    end
    object QrParamsEmpNFeVerCanNFe: TFloatField
      FieldName = 'NFeVerCanNFe'
      Required = True
    end
    object QrParamsEmpNFeVerInuNum: TFloatField
      FieldName = 'NFeVerInuNum'
      Required = True
    end
    object QrParamsEmpNFeVerConNFe: TFloatField
      FieldName = 'NFeVerConNFe'
      Required = True
    end
    object QrParamsEmpNFeVerLotEve: TFloatField
      FieldName = 'NFeVerLotEve'
      Required = True
    end
    object QrParamsEmpNFeVerConsCad: TFloatField
      FieldName = 'NFeVerConsCad'
      Required = True
    end
    object QrParamsEmpNFeVerDistDFeInt: TFloatField
      FieldName = 'NFeVerDistDFeInt'
      Required = True
    end
    object QrParamsEmpNFeVerDowNFe: TFloatField
      FieldName = 'NFeVerDowNFe'
      Required = True
    end
    object QrParamsEmpide_tpImp: TSmallintField
      FieldName = 'ide_tpImp'
      Required = True
    end
    object QrParamsEmpNFeItsLin: TSmallintField
      FieldName = 'NFeItsLin'
      Required = True
    end
    object QrParamsEmpNFeFTRazao: TSmallintField
      FieldName = 'NFeFTRazao'
      Required = True
    end
    object QrParamsEmpNFeFTEnder: TSmallintField
      FieldName = 'NFeFTEnder'
      Required = True
    end
    object QrParamsEmpNFeFTFones: TSmallintField
      FieldName = 'NFeFTFones'
      Required = True
    end
    object QrParamsEmpNFeMaiusc: TSmallintField
      FieldName = 'NFeMaiusc'
      Required = True
    end
    object QrParamsEmpNFeShowURL: TWideStringField
      FieldName = 'NFeShowURL'
      Size = 255
    end
    object QrParamsEmpPathLogoNF: TWideStringField
      FieldName = 'PathLogoNF'
      Size = 255
    end
    object QrParamsEmpDirDANFEs: TWideStringField
      FieldName = 'DirDANFEs'
      Size = 255
    end
    object QrParamsEmpNFe_indFinalCpl: TSmallintField
      FieldName = 'NFe_indFinalCpl'
      Required = True
    end
    object QrParamsEmpNoDANFEMail: TSmallintField
      FieldName = 'NoDANFEMail'
      Required = True
    end
    object QrParamsEmpSPED_EFD_IND_PERFIL: TWideStringField
      FieldName = 'SPED_EFD_IND_PERFIL'
      Required = True
      Size = 1
    end
    object QrParamsEmpSPED_EFD_IND_ATIV: TSmallintField
      FieldName = 'SPED_EFD_IND_ATIV'
      Required = True
    end
    object QrParamsEmpSPED_EFD_CadContador: TIntegerField
      FieldName = 'SPED_EFD_CadContador'
      Required = True
    end
    object QrParamsEmpSPED_EFD_CRCContador: TWideStringField
      FieldName = 'SPED_EFD_CRCContador'
      Size = 15
    end
    object QrParamsEmpSPED_EFD_EscriContab: TIntegerField
      FieldName = 'SPED_EFD_EscriContab'
      Required = True
    end
    object QrParamsEmpSPED_EFD_EnderContab: TSmallintField
      FieldName = 'SPED_EFD_EnderContab'
      Required = True
    end
    object QrParamsEmpSPED_EFD_DtFiscal: TSmallintField
      FieldName = 'SPED_EFD_DtFiscal'
      Required = True
    end
    object QrParamsEmpSPED_EFD_ID_0150: TSmallintField
      FieldName = 'SPED_EFD_ID_0150'
      Required = True
    end
    object QrParamsEmpSPED_EFD_ID_0200: TSmallintField
      FieldName = 'SPED_EFD_ID_0200'
      Required = True
    end
    object QrParamsEmpSPED_EFD_Peri_K100: TSmallintField
      FieldName = 'SPED_EFD_Peri_K100'
      Required = True
    end
    object QrParamsEmpSPED_EFD_Peri_E500: TSmallintField
      FieldName = 'SPED_EFD_Peri_E500'
      Required = True
    end
    object QrParamsEmpSPED_EFD_Peri_E100: TSmallintField
      FieldName = 'SPED_EFD_Peri_E100'
      Required = True
    end
    object QrParamsEmpSPED_EFD_Producao: TSmallintField
      FieldName = 'SPED_EFD_Producao'
      Required = True
    end
    object QrParamsEmpSPED_EFD_MovSubPrd: TSmallintField
      FieldName = 'SPED_EFD_MovSubPrd'
      Required = True
    end
    object QrParamsEmpSPED_EFD_Path: TWideStringField
      FieldName = 'SPED_EFD_Path'
      Size = 255
    end
    object QrParamsEmpSPED_EFD_ICMS_IPI_VersaoGuia: TWideStringField
      FieldName = 'SPED_EFD_ICMS_IPI_VersaoGuia'
      Size = 15
    end
    object QrParamsEmpNFSeMetodo: TIntegerField
      FieldName = 'NFSeMetodo'
      Required = True
    end
    object QrParamsEmpNFSeVersao: TFloatField
      FieldName = 'NFSeVersao'
      Required = True
    end
    object QrParamsEmpNFSeSerieRps: TWideStringField
      FieldName = 'NFSeSerieRps'
      Size = 5
    end
    object QrParamsEmpDpsNumero: TIntegerField
      FieldName = 'DpsNumero'
      Required = True
    end
    object QrParamsEmpNFSeWSProducao: TWideStringField
      FieldName = 'NFSeWSProducao'
      Size = 255
    end
    object QrParamsEmpNFSeAmbiente: TSmallintField
      FieldName = 'NFSeAmbiente'
      Required = True
    end
    object QrParamsEmpNFSeWSHomologa: TWideStringField
      FieldName = 'NFSeWSHomologa'
      Size = 255
    end
    object QrParamsEmpNFSeCertDigital: TWideStringField
      FieldName = 'NFSeCertDigital'
      Size = 255
    end
    object QrParamsEmpNFSeCertValidad: TDateField
      FieldName = 'NFSeCertValidad'
      Required = True
    end
    object QrParamsEmpNFSeCertAviExpi: TSmallintField
      FieldName = 'NFSeCertAviExpi'
      Required = True
    end
    object QrParamsEmpRPSNumLote: TIntegerField
      FieldName = 'RPSNumLote'
      Required = True
    end
    object QrParamsEmpNFSeUserWeb: TWideStringField
      FieldName = 'NFSeUserWeb'
      Size = 60
    end
    object QrParamsEmpNFSeSenhaWeb: TWideStringField
      FieldName = 'NFSeSenhaWeb'
      Size = 60
    end
    object QrParamsEmpNFSeCodMunici: TIntegerField
      FieldName = 'NFSeCodMunici'
      Required = True
    end
    object QrParamsEmpNFSePrefeitura1: TWideStringField
      FieldName = 'NFSePrefeitura1'
      Size = 100
    end
    object QrParamsEmpNFSeMsgVisu: TSmallintField
      FieldName = 'NFSeMsgVisu'
      Required = True
    end
    object QrParamsEmpNFSeLogoFili: TWideStringField
      FieldName = 'NFSeLogoFili'
      Size = 255
    end
    object QrParamsEmpNFSeLogoPref: TWideStringField
      FieldName = 'NFSeLogoPref'
      Size = 255
    end
    object QrParamsEmpNFSePrefeitura2: TWideStringField
      FieldName = 'NFSePrefeitura2'
      Size = 100
    end
    object QrParamsEmpNFSePrefeitura3: TWideStringField
      FieldName = 'NFSePrefeitura3'
      Size = 100
    end
    object QrParamsEmpNFSeMetodEnvRPS: TSmallintField
      FieldName = 'NFSeMetodEnvRPS'
      Required = True
    end
    object QrParamsEmpNFSeTipoRps: TSmallintField
      FieldName = 'NFSeTipoRps'
      Required = True
    end
    object QrParamsEmpNFSe_indFinalCpl: TSmallintField
      FieldName = 'NFSe_indFinalCpl'
      Required = True
    end
    object QrParamsEmpDirNFSeRPSEnvLot: TWideStringField
      FieldName = 'DirNFSeRPSEnvLot'
      Size = 255
    end
    object QrParamsEmpDirNFSeRPSRecLot: TWideStringField
      FieldName = 'DirNFSeRPSRecLot'
      Size = 255
    end
    object QrParamsEmpDirNFSeDPSAss: TWideStringField
      FieldName = 'DirNFSeDPSAss'
      Size = 255
    end
    object QrParamsEmpDirNFSeDPSGer: TWideStringField
      FieldName = 'DirNFSeDPSGer'
      Size = 255
    end
    object QrParamsEmpDirNFSeLogs: TWideStringField
      FieldName = 'DirNFSeLogs'
      Size = 255
    end
    object QrParamsEmpDirNFSeSchema: TWideStringField
      FieldName = 'DirNFSeSchema'
      Size = 255
    end
    object QrParamsEmpNFSePreMailAut: TIntegerField
      FieldName = 'NFSePreMailAut'
      Required = True
    end
    object QrParamsEmpNFSePreMailCan: TIntegerField
      FieldName = 'NFSePreMailCan'
      Required = True
    end
    object QrParamsEmpMyEmailNFSe: TWideStringField
      FieldName = 'MyEmailNFSe'
      Size = 255
    end
    object QrParamsEmpCTetpEmis: TSmallintField
      FieldName = 'CTetpEmis'
      Required = True
    end
    object QrParamsEmpCTePreMailAut: TIntegerField
      FieldName = 'CTePreMailAut'
      Required = True
    end
    object QrParamsEmpCTePreMailCan: TIntegerField
      FieldName = 'CTePreMailCan'
      Required = True
    end
    object QrParamsEmpCTePreMailEveCCe: TIntegerField
      FieldName = 'CTePreMailEveCCe'
      Required = True
    end
    object QrParamsEmpDirCTeProt: TWideStringField
      FieldName = 'DirCTeProt'
      Size = 255
    end
    object QrParamsEmpDirCTeRWeb: TWideStringField
      FieldName = 'DirCTeRWeb'
      Size = 255
    end
    object QrParamsEmpCTeSCAN_Ser: TIntegerField
      FieldName = 'CTeSCAN_Ser'
      Required = True
    end
    object QrParamsEmpCTeSCAN_nCT: TIntegerField
      FieldName = 'CTeSCAN_nCT'
      Required = True
    end
    object QrParamsEmpCTeUF_WebServ: TWideStringField
      FieldName = 'CTeUF_WebServ'
      Required = True
      Size = 2
    end
    object QrParamsEmpCTeUF_Servico: TWideStringField
      FieldName = 'CTeUF_Servico'
      Required = True
      Size = 10
    end
    object QrParamsEmpCTeSerNum: TWideStringField
      FieldName = 'CTeSerNum'
      Size = 255
    end
    object QrParamsEmpCTeSerVal: TDateField
      FieldName = 'CTeSerVal'
      Required = True
    end
    object QrParamsEmpCTeSerAvi: TSmallintField
      FieldName = 'CTeSerAvi'
      Required = True
    end
    object QrParamsEmpCTeUF_EPEC: TWideStringField
      FieldName = 'CTeUF_EPEC'
      Required = True
      Size = 10
    end
    object QrParamsEmpCTeUF_Conting: TWideStringField
      FieldName = 'CTeUF_Conting'
      Required = True
      Size = 2
    end
    object QrParamsEmpDirCTeEnvLot: TWideStringField
      FieldName = 'DirCTeEnvLot'
      Size = 255
    end
    object QrParamsEmpDirCTeRec: TWideStringField
      FieldName = 'DirCTeRec'
      Size = 255
    end
    object QrParamsEmpDirCTeProRec: TWideStringField
      FieldName = 'DirCTeProRec'
      Size = 255
    end
    object QrParamsEmpDirCTeAss: TWideStringField
      FieldName = 'DirCTeAss'
      Size = 255
    end
    object QrParamsEmpDirCTeGer: TWideStringField
      FieldName = 'DirCTeGer'
      Size = 255
    end
    object QrParamsEmpDirCTeDen: TWideStringField
      FieldName = 'DirCTeDen'
      Size = 255
    end
    object QrParamsEmpDirCTePedRec: TWideStringField
      FieldName = 'DirCTePedRec'
      Size = 255
    end
    object QrParamsEmpDirCTePedSta: TWideStringField
      FieldName = 'DirCTePedSta'
      Size = 255
    end
    object QrParamsEmpDirCTeSta: TWideStringField
      FieldName = 'DirCTeSta'
      Size = 255
    end
    object QrParamsEmpDirCTePedCan: TWideStringField
      FieldName = 'DirCTePedCan'
      Size = 255
    end
    object QrParamsEmpDirCTeCan: TWideStringField
      FieldName = 'DirCTeCan'
      Size = 255
    end
    object QrParamsEmpDirCTePedInu: TWideStringField
      FieldName = 'DirCTePedInu'
      Size = 255
    end
    object QrParamsEmpDirCTeInu: TWideStringField
      FieldName = 'DirCTeInu'
      Size = 255
    end
    object QrParamsEmpDirCTePedSit: TWideStringField
      FieldName = 'DirCTePedSit'
      Size = 255
    end
    object QrParamsEmpDirCTeSit: TWideStringField
      FieldName = 'DirCTeSit'
      Size = 255
    end
    object QrParamsEmpDirCTeEveEnvLot: TWideStringField
      FieldName = 'DirCTeEveEnvLot'
      Size = 255
    end
    object QrParamsEmpDirCTeEveRetLot: TWideStringField
      FieldName = 'DirCTeEveRetLot'
      Size = 255
    end
    object QrParamsEmpDirCTeEvePedCCe: TWideStringField
      FieldName = 'DirCTeEvePedCCe'
      Size = 255
    end
    object QrParamsEmpDirCTeEveRetCCe: TWideStringField
      FieldName = 'DirCTeEveRetCCe'
      Size = 255
    end
    object QrParamsEmpDirCTeEvePedCan: TWideStringField
      FieldName = 'DirCTeEvePedCan'
      Size = 255
    end
    object QrParamsEmpDirCTeEveRetCan: TWideStringField
      FieldName = 'DirCTeEveRetCan'
      Size = 255
    end
    object QrParamsEmpDirCTEEveProcCCe: TWideStringField
      FieldName = 'DirCTEEveProcCCe'
      Size = 255
    end
    object QrParamsEmpDirCTeEvePedEPEC: TWideStringField
      FieldName = 'DirCTeEvePedEPEC'
      Size = 255
    end
    object QrParamsEmpDirCTeEveRetEPEC: TWideStringField
      FieldName = 'DirCTeEveRetEPEC'
      Size = 255
    end
    object QrParamsEmpCTeversao: TFloatField
      FieldName = 'CTeversao'
      Required = True
    end
    object QrParamsEmpCTeide_mod: TSmallintField
      FieldName = 'CTeide_mod'
      Required = True
    end
    object QrParamsEmpCTeide_tpAmb: TSmallintField
      FieldName = 'CTeide_tpAmb'
      Required = True
    end
    object QrParamsEmpDirCTeSchema: TWideStringField
      FieldName = 'DirCTeSchema'
      Size = 255
    end
    object QrParamsEmpCTeAppCode: TSmallintField
      FieldName = 'CTeAppCode'
      Required = True
    end
    object QrParamsEmpCTeAssDigMode: TSmallintField
      FieldName = 'CTeAssDigMode'
      Required = True
    end
    object QrParamsEmpMyEmailCTe: TWideStringField
      FieldName = 'MyEmailCTe'
      Size = 255
    end
    object QrParamsEmpCTeInfCpl: TIntegerField
      FieldName = 'CTeInfCpl'
      Required = True
    end
    object QrParamsEmpCTeVerStaSer: TFloatField
      FieldName = 'CTeVerStaSer'
      Required = True
    end
    object QrParamsEmpCTeVerEnvLot: TFloatField
      FieldName = 'CTeVerEnvLot'
      Required = True
    end
    object QrParamsEmpCTeVerConLot: TFloatField
      FieldName = 'CTeVerConLot'
      Required = True
    end
    object QrParamsEmpCTeVerInuNum: TFloatField
      FieldName = 'CTeVerInuNum'
      Required = True
    end
    object QrParamsEmpCTeVerConCTe: TFloatField
      FieldName = 'CTeVerConCTe'
      Required = True
    end
    object QrParamsEmpCTeVerLotEve: TFloatField
      FieldName = 'CTeVerLotEve'
      Required = True
    end
    object QrParamsEmpCTeVerEPEC: TFloatField
      FieldName = 'CTeVerEPEC'
      Required = True
    end
    object QrParamsEmpCTeide_tpImp: TSmallintField
      FieldName = 'CTeide_tpImp'
      Required = True
    end
    object QrParamsEmpCTeItsLin: TSmallintField
      FieldName = 'CTeItsLin'
      Required = True
    end
    object QrParamsEmpCTeFTRazao: TSmallintField
      FieldName = 'CTeFTRazao'
      Required = True
    end
    object QrParamsEmpCTeFTEnder: TSmallintField
      FieldName = 'CTeFTEnder'
      Required = True
    end
    object QrParamsEmpCTeFTFones: TSmallintField
      FieldName = 'CTeFTFones'
      Required = True
    end
    object QrParamsEmpCTeMaiusc: TSmallintField
      FieldName = 'CTeMaiusc'
      Required = True
    end
    object QrParamsEmpCTeShowURL: TWideStringField
      FieldName = 'CTeShowURL'
      Size = 255
    end
    object QrParamsEmpPathLogoCTe: TWideStringField
      FieldName = 'PathLogoCTe'
      Size = 255
    end
    object QrParamsEmpDirDACTes: TWideStringField
      FieldName = 'DirDACTes'
      Size = 255
    end
    object QrParamsEmpCTe_indFinalCpl: TSmallintField
      FieldName = 'CTe_indFinalCpl'
      Required = True
    end
    object QrParamsEmpMDFePreMailAut: TIntegerField
      FieldName = 'MDFePreMailAut'
      Required = True
    end
    object QrParamsEmpMDFePreMailCan: TIntegerField
      FieldName = 'MDFePreMailCan'
      Required = True
    end
    object QrParamsEmpMDFePreMailEveCCe: TIntegerField
      FieldName = 'MDFePreMailEveCCe'
      Required = True
    end
    object QrParamsEmpDirMDFeProt: TWideStringField
      FieldName = 'DirMDFeProt'
      Size = 255
    end
    object QrParamsEmpDirMDFeRWeb: TWideStringField
      FieldName = 'DirMDFeRWeb'
      Size = 255
    end
    object QrParamsEmpMDFeSCAN_Ser: TIntegerField
      FieldName = 'MDFeSCAN_Ser'
      Required = True
    end
    object QrParamsEmpMDFeSCAN_nMDF: TIntegerField
      FieldName = 'MDFeSCAN_nMDF'
      Required = True
    end
    object QrParamsEmpMDFeUF_WebServ: TWideStringField
      FieldName = 'MDFeUF_WebServ'
      Required = True
      Size = 2
    end
    object QrParamsEmpMDFeUF_Servico: TWideStringField
      FieldName = 'MDFeUF_Servico'
      Required = True
      Size = 10
    end
    object QrParamsEmpMDFeSerNum: TWideStringField
      FieldName = 'MDFeSerNum'
      Size = 255
    end
    object QrParamsEmpMDFeSerVal: TDateField
      FieldName = 'MDFeSerVal'
      Required = True
    end
    object QrParamsEmpMDFeSerAvi: TSmallintField
      FieldName = 'MDFeSerAvi'
      Required = True
    end
    object QrParamsEmpDirMDFeEnvLot: TWideStringField
      FieldName = 'DirMDFeEnvLot'
      Size = 255
    end
    object QrParamsEmpDirMDFeRec: TWideStringField
      FieldName = 'DirMDFeRec'
      Size = 255
    end
    object QrParamsEmpDirMDFeProRec: TWideStringField
      FieldName = 'DirMDFeProRec'
      Size = 255
    end
    object QrParamsEmpDirMDFeAss: TWideStringField
      FieldName = 'DirMDFeAss'
      Size = 255
    end
    object QrParamsEmpDirMDFeGer: TWideStringField
      FieldName = 'DirMDFeGer'
      Size = 255
    end
    object QrParamsEmpDirMDFeDen: TWideStringField
      FieldName = 'DirMDFeDen'
      Size = 255
    end
    object QrParamsEmpDirMDFePedRec: TWideStringField
      FieldName = 'DirMDFePedRec'
      Size = 255
    end
    object QrParamsEmpDirMDFePedSta: TWideStringField
      FieldName = 'DirMDFePedSta'
      Size = 255
    end
    object QrParamsEmpDirMDFeSta: TWideStringField
      FieldName = 'DirMDFeSta'
      Size = 255
    end
    object QrParamsEmpDirMDFePedCan: TWideStringField
      FieldName = 'DirMDFePedCan'
      Size = 255
    end
    object QrParamsEmpDirMDFeCan: TWideStringField
      FieldName = 'DirMDFeCan'
      Size = 255
    end
    object QrParamsEmpDirMDFePedInu: TWideStringField
      FieldName = 'DirMDFePedInu'
      Size = 255
    end
    object QrParamsEmpDirMDFeInu: TWideStringField
      FieldName = 'DirMDFeInu'
      Size = 255
    end
    object QrParamsEmpDirMDFePedSit: TWideStringField
      FieldName = 'DirMDFePedSit'
      Size = 255
    end
    object QrParamsEmpDirMDFeSit: TWideStringField
      FieldName = 'DirMDFeSit'
      Size = 255
    end
    object QrParamsEmpDirMDFeEveEnvLot: TWideStringField
      FieldName = 'DirMDFeEveEnvLot'
      Size = 255
    end
    object QrParamsEmpDirMDFeEveRetLot: TWideStringField
      FieldName = 'DirMDFeEveRetLot'
      Size = 255
    end
    object QrParamsEmpDirMDFeEvePedEnc: TWideStringField
      FieldName = 'DirMDFeEvePedEnc'
      Size = 255
    end
    object QrParamsEmpDirMDFeEveRetEnc: TWideStringField
      FieldName = 'DirMDFeEveRetEnc'
      Size = 255
    end
    object QrParamsEmpDirMDFeEvePedCan: TWideStringField
      FieldName = 'DirMDFeEvePedCan'
      Size = 255
    end
    object QrParamsEmpDirMDFeEveRetCan: TWideStringField
      FieldName = 'DirMDFeEveRetCan'
      Size = 255
    end
    object QrParamsEmpDirMDFeEvePedIdC: TWideStringField
      FieldName = 'DirMDFeEvePedIdC'
      Size = 255
    end
    object QrParamsEmpDirMDFeEveRetIdC: TWideStringField
      FieldName = 'DirMDFeEveRetIdC'
      Size = 255
    end
    object QrParamsEmpMDFeversao: TFloatField
      FieldName = 'MDFeversao'
      Required = True
    end
    object QrParamsEmpMDFeide_mod: TSmallintField
      FieldName = 'MDFeide_mod'
      Required = True
    end
    object QrParamsEmpMDFeide_tpAmb: TSmallintField
      FieldName = 'MDFeide_tpAmb'
      Required = True
    end
    object QrParamsEmpDirMDFeSchema: TWideStringField
      FieldName = 'DirMDFeSchema'
      Size = 255
    end
    object QrParamsEmpMDFeAppCode: TSmallintField
      FieldName = 'MDFeAppCode'
      Required = True
    end
    object QrParamsEmpMDFeAssDigMode: TSmallintField
      FieldName = 'MDFeAssDigMode'
      Required = True
    end
    object QrParamsEmpMyEmailMDFe: TWideStringField
      FieldName = 'MyEmailMDFe'
      Size = 255
    end
    object QrParamsEmpMDFeInfCpl: TIntegerField
      FieldName = 'MDFeInfCpl'
      Required = True
    end
    object QrParamsEmpMDFeVerStaSer: TFloatField
      FieldName = 'MDFeVerStaSer'
      Required = True
    end
    object QrParamsEmpMDFeVerEnvLot: TFloatField
      FieldName = 'MDFeVerEnvLot'
      Required = True
    end
    object QrParamsEmpMDFeVerConLot: TFloatField
      FieldName = 'MDFeVerConLot'
      Required = True
    end
    object QrParamsEmpMDFeVerInuNum: TFloatField
      FieldName = 'MDFeVerInuNum'
      Required = True
    end
    object QrParamsEmpMDFeVerConMDFe: TFloatField
      FieldName = 'MDFeVerConMDFe'
      Required = True
    end
    object QrParamsEmpMDFeVerLotEve: TFloatField
      FieldName = 'MDFeVerLotEve'
      Required = True
    end
    object QrParamsEmpMDFeide_tpImp: TSmallintField
      FieldName = 'MDFeide_tpImp'
      Required = True
    end
    object QrParamsEmpMDFeItsLin: TSmallintField
      FieldName = 'MDFeItsLin'
      Required = True
    end
    object QrParamsEmpMDFeFTRazao: TSmallintField
      FieldName = 'MDFeFTRazao'
      Required = True
    end
    object QrParamsEmpMDFeFTEnder: TSmallintField
      FieldName = 'MDFeFTEnder'
      Required = True
    end
    object QrParamsEmpMDFeFTFones: TSmallintField
      FieldName = 'MDFeFTFones'
      Required = True
    end
    object QrParamsEmpMDFeMaiusc: TSmallintField
      FieldName = 'MDFeMaiusc'
      Required = True
    end
    object QrParamsEmpMDFeShowURL: TWideStringField
      FieldName = 'MDFeShowURL'
      Size = 255
    end
    object QrParamsEmpPathLogoMDFe: TWideStringField
      FieldName = 'PathLogoMDFe'
      Size = 255
    end
    object QrParamsEmpDirDAMDFes: TWideStringField
      FieldName = 'DirDAMDFes'
      Size = 255
    end
    object QrParamsEmpMDFe_indFinalCpl: TSmallintField
      FieldName = 'MDFe_indFinalCpl'
      Required = True
    end
    object QrParamsEmpMoeda: TIntegerField
      FieldName = 'Moeda'
      Required = True
    end
    object QrParamsEmpAssociada: TIntegerField
      FieldName = 'Associada'
      Required = True
    end
    object QrParamsEmpEntiTipCt1: TIntegerField
      FieldName = 'EntiTipCt1'
      Required = True
    end
    object QrParamsEmpEntiTipCto: TIntegerField
      FieldName = 'EntiTipCto'
      Required = True
    end
    object QrParamsEmpNFSeTipCtoMail: TIntegerField
      FieldName = 'NFSeTipCtoMail'
      Required = True
    end
    object QrParamsEmpCTeEntiTipCt1: TIntegerField
      FieldName = 'CTeEntiTipCt1'
      Required = True
    end
    object QrParamsEmpCTeEntiTipCto: TIntegerField
      FieldName = 'CTeEntiTipCto'
      Required = True
    end
  end
  object QrPrmsEmpMis: TMySQLQuery
    Database = Dmod.ZZDB
    Left = 392
    Top = 636
    object QrPrmsEmpMisTZD_UTC_Auto_DataHoraAceite: TDateTimeField
      FieldName = 'TZD_UTC_Auto_DataHoraAceite'
    end
    object QrPrmsEmpMisTZD_UTC_Auto_TermoAceite: TIntegerField
      FieldName = 'TZD_UTC_Auto_TermoAceite'
    end
    object QrPrmsEmpMisTZD_UTC_Auto_UsuarioAceite: TIntegerField
      FieldName = 'TZD_UTC_Auto_UsuarioAceite'
    end
    object QrPrmsEmpMisSituacao: TIntegerField
      FieldName = 'Situacao'
    end
    object QrPrmsEmpMisNOMESITUACAO: TWideStringField
      FieldKind = fkLookup
      FieldName = 'NOMESITUACAO'
      LookupKeyFields = 'Codigo'
      LookupResultField = 'Nome'
      KeyFields = 'Situacao'
      Size = 30
      Lookup = True
    end
    object QrParamsEmpTZD_UTC_Str: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TZD_UTC_Str'
      Size = 6
      Calculated = True
    end
    object QrParamsEmppCredSNMez_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'pCredSNMez_TXT'
      Size = 7
      Calculated = True
    end
  end
  object DsPrmsEmpMis: TDataSource
    DataSet = QrPrmsEmpMis
    Left = 393
    Top = 682
  end
end
