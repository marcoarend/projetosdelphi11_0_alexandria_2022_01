unit MasterSelFilial;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, dmkGeral,
  dmkLabel, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkImage, UnDmkEnums;

type
  TFmMasterSelFilial = class(TForm)
    Panel1: TPanel;
    EdFilial: TdmkEditCB;
    CBFilial: TdmkDBLookupComboBox;
    dmkLabel1: TdmkLabel;
    QrFiliais: TmySQLQuery;
    DsFiliais: TDataSource;
    QrFiliaisFilial: TIntegerField;
    QrFiliaisCodigo: TIntegerField;
    QrFiliaisNOMEFILIAL: TWideStringField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    procedure BtDesisteClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    SQLExtra: String;
    FSelecionou: Boolean;
  end;

  var
  FmMasterSelFilial: TFmMasterSelFilial;

implementation

uses UnMyObjects, Module, UnInternalConsts;

{$R *.DFM}

procedure TFmMasterSelFilial.BtDesisteClick(Sender: TObject);
begin
  Close;
end;

procedure TFmMasterSelFilial.BtOKClick(Sender: TObject);
begin
  FSelecionou := True;
  Close;
end;

procedure TFmMasterSelFilial.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := EdFilial.ValueVariant;
  Close;
end;

procedure TFmMasterSelFilial.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmMasterSelFilial.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stIns;
  // QrFiliais deve ser setado e aberto pelo form que chama este form
  // UnDmkDAC_PF.AbreQuery(QrFiliais, Dmod.MyDB)
  //
  FSelecionou := False;
end;

procedure TFmMasterSelFilial.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

end.
