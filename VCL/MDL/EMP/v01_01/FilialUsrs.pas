unit FilialUsrs;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, dmkDBGrid, mySQLDbTables, Menus,
  MyDBCheck, UMySQLModule, Variants, UnDmkEnums;

type
  TFmFilialUsrs = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtUsuario: TBitBtn;
    LaTitulo1C: TLabel;
    dmkDBGrid1: TdmkDBGrid;
    Splitter1: TSplitter;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    dmkDBGrid2: TdmkDBGrid;
    QrFiliais: TmySQLQuery;
    QrFiliaisCodigo: TIntegerField;
    QrFiliaisFilial: TIntegerField;
    QrFiliaisNOMEENT: TWideStringField;
    QrFiliaisCNPJCPF: TWideStringField;
    QrFiliaisIERG: TWideStringField;
    QrFiliaisFormaSociet: TWideStringField;
    QrFiliaisSimples: TSmallintField;
    QrFiliaisAtividade: TWideStringField;
    QrFiliaisCNPJCPF_TXT: TWideStringField;
    DsFiliais: TDataSource;
    QrUsuarios: TmySQLQuery;
    QrUsuariosNumero: TIntegerField;
    QrUsuariosControle: TIntegerField;
    QrUsuariosEmpresa: TIntegerField;
    QrUsuariosLogin: TWideStringField;
    QrUsuariosNOMEFUNCIONARIO: TWideStringField;
    DsUsuarios: TDataSource;
    PMUsuario: TPopupMenu;
    Adicionausurio1: TMenuItem;
    Retirausurio1: TMenuItem;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure QrFiliaisCalcFields(DataSet: TDataSet);
    procedure QrFiliaisBeforeClose(DataSet: TDataSet);
    procedure QrFiliaisAfterScroll(DataSet: TDataSet);
    procedure BtUsuarioClick(Sender: TObject);
    procedure Adicionausurio1Click(Sender: TObject);
    procedure Retirausurio1Click(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenUsuarios(Controle: Integer);
    function  ReopenFiliais(Codigo: Integer): Boolean;
  public
    { Public declarations }
  end;

  var
  FmFilialUsrs: TFmFilialUsrs;

implementation

uses UnMyObjects, Module, MyListas, DmkDAC_PF;

{$R *.DFM}

procedure TFmFilialUsrs.Adicionausurio1Click(Sender: TObject);
const
  Aviso   = '...';
  Titulo  = 'Adiciona Usu�rio';
  Prompt  = 'Informe o usu�rio:';
  Campo   = 'Descricao';
var
  CodJan: Variant;
  Codigo, Controle: Integer;
begin
  if (QrFiliais.State = dsInactive) or (QrFiliais.RecordCount = 0) then Exit;
  //
  CodJan := DBCheck.EscolheCodigoUnico(Aviso, Titulo, Prompt, nil, nil, Campo, 0, [
    'SELECT Numero Codigo, Login ' + Campo,
    'FROM senhas ',
    'WHERE Numero > 0 ',
    'AND Numero NOT IN ( ',
    'SELECT Numero ',
    'FROM senhasits ',
    'WHERE Empresa=' + Geral.FF0(QrFiliaisCodigo.Value),
    ')',
    'ORDER BY ' + Campo,
    ''], Dmod.MyDB, False);
  //
  if CodJan <> Null then
    Codigo := Integer(CodJan)
  else
    Codigo := 0;
  //
  if Codigo <> 0 then
  begin
    Dmod.QrAux.Close;
    Dmod.QrAux.SQL.Clear;
    Dmod.QrAux.SQL.Add('SELECT Numero FROM senhasits ');
    Dmod.QrAux.SQL.Add('WHERE Numero=:P0 ');
    Dmod.QrAux.SQL.Add('AND Empresa=:P1');
    Dmod.QrAux.Params[00].AsInteger := Codigo;
    Dmod.QrAux.Params[01].AsInteger := QrFiliaisCodigo.Value;
    UnDmkDAC_PF.AbreQuery(Dmod.QrAux, Dmod.MyDB);
    if Dmod.QrAux.RecordCount > 0 then
    begin
      Geral.MB_Aviso('O usu�rio ' + FormatFloat('0', Codigo) +
        ' j� foi adicionado anteriormente!');
      Exit;
    end;
    Controle := UMyMod.BuscaEmLivreY_Def('senhasits', 'Controle', stIns, 0);
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'senhasits', False, [
      'Numero', 'Entidade', 'Empresa'], ['Controle'], [
    Codigo, 0, QrFiliaisCodigo.Value], [Controle], False) then
      ReopenUsuarios(Codigo);
  end;
end;

procedure TFmFilialUsrs.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmFilialUsrs.BtUsuarioClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMUsuario, BtUsuario);
end;

procedure TFmFilialUsrs.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmFilialUsrs.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  ReopenFiliais(0);
end;

procedure TFmFilialUsrs.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmFilialUsrs.QrFiliaisAfterScroll(DataSet: TDataSet);
begin
  ReopenUsuarios(0);
end;

procedure TFmFilialUsrs.QrFiliaisBeforeClose(DataSet: TDataSet);
begin
  QrUsuarios.Close;
end;

procedure TFmFilialUsrs.QrFiliaisCalcFields(DataSet: TDataSet);
begin
  QrFiliaisCNPJCPF_TXT.Value := Geral.FormataCNPJ_TT(QrFiliaisCNPJCPF.Value);
end;

function TFmFilialUsrs.ReopenFiliais(Codigo: Integer): Boolean;
begin
  Result := False;
  try
    QrFiliais.Close;
    if VAR_KIND_DEPTO = kdUH then
    begin
      QrFiliais.Close;
      QrFiliais.SQL.Clear;
      QrFiliais.SQL.Add('SELECT DISTINCT ent.Codigo, ent.CliInt Filial,');
      QrFiliais.SQL.Add('IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENT,');
      QrFiliais.SQL.Add('IF(ent.Tipo=0, ent.CNPJ, ent.CPF) CNPJCPF,');
      QrFiliais.SQL.Add('IF(ent.Tipo=0, ent.IE, ent.RG) IERG,');
      QrFiliais.SQL.Add('FormaSociet, Simples, Atividade');
      QrFiliais.SQL.Add('FROM entidades ent');
      QrFiliais.SQL.Add('WHERE ent.CliInt <> 0');
      QrFiliais.SQL.Add('ORDER BY ent.Codigo DESC ');
    end else
    begin
      QrFiliais.Close;
      QrFiliais.SQL.Clear;
      QrFiliais.SQL.Add('SELECT DISTINCT ent.Codigo, ent.Filial,');
      QrFiliais.SQL.Add('IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENT,');
      QrFiliais.SQL.Add('IF(ent.Tipo=0, ent.CNPJ, ent.CPF) CNPJCPF,');
      QrFiliais.SQL.Add('IF(ent.Tipo=0, ent.IE, ent.RG) IERG,');
      QrFiliais.SQL.Add('FormaSociet, Simples, Atividade');
      QrFiliais.SQL.Add('FROM entidades ent');
      QrFiliais.SQL.Add('WHERE ent.Codigo < -10');
      QrFiliais.SQL.Add('ORDER BY ent.Codigo DESC ');
    end;
    UnDmkDAC_PF.AbreQuery(QrFiliais, Dmod.MyDB);
    Result := true;
    QrFiliais.Locate('Codigo', Codigo, []);
  except
    Geral.MB_Aviso('N�o foi poss�vel listar as filiais!');
  end;
end;

procedure TFmFilialUsrs.ReopenUsuarios(Controle: Integer);
begin
  QrUsuarios.Close;
  QrUsuarios.Params[0].AsInteger := QrFiliaisCodigo.value;
  UnDmkDAC_PF.AbreQuery(QrUsuarios, Dmod.MyDB);
  QrUsuarios.Locate('Controle', Controle, []);
end;

procedure TFmFilialUsrs.Retirausurio1Click(Sender: TObject);
begin
  if (QrUsuarios.State = dsInactive) or (QrUsuarios.RecordCount = 0) then Exit;
  //
  UmyMod.ExcluiRegistroInt1('Confirma a retirada do usu�rio selecionado?',
  'senhasits', 'Controle', QrUsuariosControle.Value, DMod.MyDB);
  //
  ReopenUsuarios(QrUsuariosControle.Value);
end;

end.
