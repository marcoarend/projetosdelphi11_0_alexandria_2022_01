unit ParamsNFs;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, DBCtrls,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkLabel, Mask, dmkDBEdit, dmkGeral,
  Variants, dmkValUsu, dmkRadioGroup, dmkImage, UnDmkEnums;

type
  TFmParamsNFs = class(TForm)
    Panel1: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    QrPesqPrc: TmySQLQuery;
    QrPesqPrcPreco: TFloatField;
    Panel3: TPanel;
    DBEdCodigo: TdmkDBEdit;
    DBEdit1: TDBEdit;
    DBEdNome: TDBEdit;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    EdSequencial: TdmkEdit;
    EdSerieNF: TdmkEdit;
    Label6: TLabel;
    EdControle: TdmkEdit;
    RGIncSeqAuto: TdmkRadioGroup;
    EdMaxSeqLib: TdmkEdit;
    Label7: TLabel;
    Panel4: TPanel;
    CkContinuar: TCheckBox;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel2: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel5: TPanel;
    BtOK: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    procedure AtualizaImprimeEmpr(Empresa, ParamsNF: Integer);
  public
    { Public declarations }
  end;

  var
  FmParamsNFs: TFmParamsNFs;

implementation

uses UnMyObjects, Module, ParamsEmp, UMySQLModule, DmkDAC_PF;

{$R *.DFM}

procedure TFmParamsNFs.AtualizaImprimeEmpr(Empresa, ParamsNF: Integer);
var
  Qry, Qry2, QryUpd: TmySQLQuery;
  Codigo, Controle: Integer;
begin
  if MyObjects.FIC(Empresa = 0, nil, 'Empresa n�o definida na procedure "TFmParamsNFs.AtualizaImprimeEmpr"!') then Exit;
  if MyObjects.FIC(ParamsNF = 0, nil, 'ParamsNF n�o definido na procedure "TFmParamsNFs.AtualizaImprimeEmpr"!') then Exit;
  //
  Qry    := TmySQLQuery.Create(Dmod);
  Qry2   := TmySQLQuery.Create(Dmod);
  QryUpd := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SHOW tables LIKE "imprimeempr" ',
      '']);
    if Qry.RecordCount > 0 then
    begin
      //Apenas NF-e
      UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
        'SELECT Codigo ',
        'FROM imprime ',
        'WHERE TipoImpressao = 4 ',
        '']);
      if Qry.RecordCount > 0 then
      begin
        while not Qry.Eof do
        begin
          Codigo := Qry.FieldByName('Codigo').Value;
          //
          UnDmkDAC_PF.AbreMySQLQuery0(Qry2, Dmod.MyDB, [
            'SELECT Controle, ParamsNFs ',
            'FROM imprimeempr ',
            'WHERE Empresa=' + Geral.FF0(Empresa),
            'AND ParamsNFs=' + Geral.FF0(ParamsNF),
            'OR ParamsNFs=0 ', // 2020-10-26
            '']);
          if Qry2.RecordCount = 0 then
          begin
            //N�o achou inclui
            Controle := UMyMod.BuscaEmLivreY_Def('imprimeempr', 'Controle', stIns, 0);
            //
            UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'imprimeempr', False,
              ['Codigo', 'Empresa', 'ParamsNFs'], ['Controle'],
              [Codigo, Empresa, ParamsNF], [Controle], True);
          end
          else if Qry2.FieldByName('ParamsNFs').AsInteger = 0 then
          begin
            //N�o achou inclui
            Controle := Qry2.FieldByName('Controle').AsInteger;
            //
            UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'imprimeempr', False,
              ['Codigo', 'Empresa', 'ParamsNFs'], ['Controle'],
              [Codigo, Empresa, ParamsNF], [Controle], True);
          end;
          //
          Qry.Next;
        end;
      end;
    end;
  finally
    Qry.Free;
    Qry2.Free;
    QryUpd.Free;
  end;
end;

procedure TFmParamsNFs.BtOKClick(Sender: TObject);
var
  Controle, SerieNF: Integer;
begin
  SerieNF := EdSerieNF.ValueVariant;
  //
  if MyObjects.FIC(SerieNF = 0, EdSerieNF, 'Informe a s�rie da nota fiscal!') then Exit;
  if MyObjects.FIC(SerieNF >= 900, EdSerieNF, 'A serie deve ser inferior a 900!' +
    sLineBreak + 'Para cadastrar a s�rie para notas em conting�ncia utilize o campo correto!')
  then
    Exit;
  //
  Controle := EdControle.ValueVariant;
  Controle := UMyMod.BuscaEmLivreY_Def('paramsnfs', 'Controle', ImgTipo.SQLType, Controle);
  //
  EdControle.ValueVariant := Controle;
  //
  if UMyMod.ExecSQLInsUpdFm(FmParamsNFs, ImgTipo.SQLType, 'ParamsNFs', Controle,
    Dmod.QrUpd) then
  begin
    AtualizaImprimeEmpr(FmParamsEmp.QrParamsEmpCodigo.Value, Controle);
    //
    FmParamsEmp.ReopenParamsNFs(Controle);
    if CkContinuar.Checked then
    begin
      Geral.MB_Info(DmkEnums.NomeTipoSQL(ImgTipo.SQLType) +
        ' de S�rie de Nota Fiscal realizado com sucesso!');
      //
      ImgTipo.SQLType             := stIns;
      EdSerieNF.Text             := '';
      EdSequencial.ValueVariant  := 0;
      EdControle.ValueVariant    := 0;
      EdSerieNF.SetFocus;
    end else
      Close;
  end;
end;

procedure TFmParamsNFs.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmParamsNFs.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //
  DBEdCodigo.DataSource := FmParamsEmp.DsParamsEmp;
  DBEdit1.DataSource    := FmParamsEmp.DsParamsEmp;
  DBEdNome.DataSource   := FmParamsEmp.DsParamsEmp;
end;

procedure TFmParamsNFs.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
end;

procedure TFmParamsNFs.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
    [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmParamsNFs.FormShow(Sender: TObject);
begin
  if ImgTipo.SQLType = stIns then
  begin
    EdMaxSeqLib.ValueVariant := 999999999;
    RGIncSeqAuto.ItemIndex   := 1;
    CkContinuar.Visible      := True;
    CkContinuar.Checked      := False;
  end else
  if ImgTipo.SQLType = stUpd then
  begin
    CkContinuar.Visible := False;
    CkContinuar.Checked := False;
  end;
end;

end.
