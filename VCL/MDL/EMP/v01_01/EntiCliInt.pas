unit EntiCliInt;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnInternalConsts,
  dmkImage, UnDMkEnums;

type
  TFmEntiCliInt = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    QrCtaCfgCab: TmySQLQuery;
    QrCtaCfgCabCodigo: TIntegerField;
    QrCtaCfgCabNome: TWideStringField;
    DsCtaCfgCab: TDataSource;
    EdCtaCfgCab: TdmkEditCB;
    CBCtaCfgCab: TdmkDBLookupComboBox;
    Label1: TLabel;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    FCodCliInt: Integer;
  end;

  var
  FmEntiCliInt: TFmEntiCliInt;

implementation

uses UnMyObjects, Module, UMySQLModule, DmkDAC_PF;

{$R *.DFM}

procedure TFmEntiCliInt.BtOKClick(Sender: TObject);
var
  CtaCfgCab: Integer;
begin
  CtaCfgCab := EdCtaCfgCab.ValueVariant;
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'enticliint', False, [
  //'CodEnti', 'CodFilial', 'TipoTabLct', 'AccManager',
  'CtaCfgCab'], ['CodCliInt'], [
  //CodEnti, CodFilial, TipoTabLct, AccManager,
  CtaCfgCab], [FCodCliInt], True) then
    Close;
end;

procedure TFmEntiCliInt.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmEntiCliInt.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmEntiCliInt.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stUpd;
  UnDmkDAC_PF.AbreQuery(QrCtaCfgCab, Dmod.MyDB);
end;

procedure TFmEntiCliInt.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

end.
