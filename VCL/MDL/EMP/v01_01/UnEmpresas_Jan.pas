unit UnEmpresas_Jan;

interface

uses
  Windows, SysUtils, Classes, DB, mySQLDbTables, DmkGeral, DmkDAC_PF,
  UnInternalConsts, UnDmkProcFunc, frxClass, frxDBSet, Menus,
  dmkDBLookupComboBox, dmkEditCB, UnDmkEnums;

type
  TUnEmpresas_Jan = class(TObject)
  private
    { Private declarations }

  public
    { Public declarations }

    procedure MostraFormParamsEmp();
    procedure MostraFormMatriz();

  end;

var
  Empresas_Jan: TUnEmpresas_Jan;


implementation

uses MyDBCheck, ModuleGeral, ParamsEmp, Matriz;

{ TUnEmpresas_Jan }

procedure TUnEmpresas_Jan.MostraFormMatriz();
begin
  if DBCheck.CriaFm(TFmMatriz, FmMatriz, afmoSoAdmin) then
  begin
    FmMatriz.ShowModal;
    FmMatriz.Destroy;
  end;
end;

procedure TUnEmpresas_Jan.MostraFormParamsEmp();
begin
  if DBCheck.CriaFm(TFmParamsEmp, FmParamsEmp, afmoSoBoss) then
  begin
    FmParamsEmp.ShowModal;
    FmParamsEmp.Destroy;
  end;
end;

end.
