unit PrmsCompny;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, ExtDlgs, ZCF2, ResIntStrings,
  UnGOTOy, UnInternalConsts, UnMsgInt, UnInternalConsts2, UMySQLModule,
  mySQLDbTables, UnMySQLCuringa, dmkGeral, dmkPermissoes, dmkEdit, dmkLabel,
  dmkDBEdit, Mask, dmkImage, dmkRadioGroup, unDmkProcFunc, UnDmkEnums,
  dmkDBLookupComboBox, dmkEditCB;

type
  TFmPrmsCompny = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    BtExclui: TBitBtn;
    BtAltera: TBitBtn;
    BtInclui: TBitBtn;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    QrPrmsCompny: TMySQLQuery;
    DsPrmsCompny: TDataSource;
    dmkPermissoes1: TdmkPermissoes;
    EdCodigo: TdmkEdit;
    EdNO_ENT: TdmkEdit;
    Label1: TLabel;
    DBEdCodigo: TdmkDBEdit;
    Label2: TLabel;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    QrPrmsCompnyCodigo: TIntegerField;
    QrPrmsCompnyFilial: TIntegerField;
    QrPrmsCompnyNO_ENT: TWideStringField;
    QrPrmsCompnyNO_loclzl10ncab: TWideStringField;
    Label3: TLabel;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    Label4: TLabel;
    DBEdit3: TDBEdit;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    EdFilial: TdmkEdit;
    EdLoclzL10nCab: TdmkEditCB;
    CBLoclzL10nCab: TdmkDBLookupComboBox;
    Label8: TLabel;
    QrLoclzL10nCab: TMySQLQuery;
    QrLoclzL10nCabCodigo: TIntegerField;
    QrLoclzL10nCabNome: TWideStringField;
    DsLoclzL10nCab: TDataSource;
    SpeedButton5: TSpeedButton;
    QrPrmsCompnySIT: TWideStringField;
    Label9: TLabel;
    DBEdit4: TDBEdit;
    QrPrmsCompnyLoclzL10nCab: TIntegerField;
    Label13: TLabel;
    EdCompany: TdmkEdit;
    Label10: TLabel;
    EdSiglaPais: TdmkEdit;
    QrPrmsCompnySiglaPais: TWideStringField;
    QrPrmsCompnyCompany: TWideStringField;
    DBEdit5: TDBEdit;
    DBEdit6: TDBEdit;
    EdMailsDstn: TdmkEdit;
    Label14: TLabel;
    Label15: TLabel;
    EdMailsDsKm: TdmkEdit;
    Label16: TLabel;
    EdNoEmpApp: TdmkEdit;
    QrPrmsCompnyMailsDstn: TWideStringField;
    QrPrmsCompnyMailsDsKm: TWideStringField;
    QrPrmsCompnyNoEmpApp: TWideStringField;
    Label11: TLabel;
    Label12: TLabel;
    Label17: TLabel;
    Label18: TLabel;
    Label19: TLabel;
    DBEdit7: TDBEdit;
    DBEdit8: TDBEdit;
    DBEdit9: TDBEdit;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrPrmsCompnyAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrPrmsCompnyBeforeOpen(DataSet: TDataSet);
    procedure BtIncluiClick(Sender: TObject);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure SpeedButton5Click(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure Va(Para: TVaiPara);
  public
    { Public declarations }
    procedure LocCod(Atual, Codigo: Integer);
  end;

var
  FmPrmsCompny: TFmPrmsCompny;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, UnEmpresas_Jan, DmkDAC_PF, UnExesD_Jan;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmPrmsCompny.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmPrmsCompny.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrPrmsCompnyCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmPrmsCompny.DefParams;
begin
  VAR_GOTOTABELA := 'prmscompny';
  VAR_GOTOMYSQLTABLE := QrPrmsCompny;
  VAR_GOTONEG := gotoPos;
  VAR_GOTOCAMPO := 'Entidade'; //CO_CODIGO;
  VAR_GOTONOME := 'NO_ENT'; //CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT ent.Codigo, ent.Filial,   ');
  VAR_SQLx.Add('IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_ENT, ');
  VAR_SQLx.Add('cny.LoclzL10nCab, cny.SiglaPais, ');
  VAR_SQLx.Add('cny.Company, llc.Nome NO_loclzl10ncab, ');
  VAR_SQLx.Add('IF(cny.Ativo IS NULL, "N", IF(cny.Ativo=0, "I", "A")) SIT, ');
  VAR_SQLx.Add('cny.MailsDstn, MailsDsKm, NoEmpApp ');
  VAR_SQLx.Add('FROM entidades ent ');
  VAR_SQLx.Add('LEFT JOIN prmscompny   cny ON cny.Entidade=ent.Codigo ');
  VAR_SQLx.Add('LEFT JOIN loclzl10ncab llc ON llc.Codigo=cny.LoclzL10nCab  ');
  VAR_SQLx.Add('WHERE ent.Codigo <= -10  ');
  VAR_SQLx.Add('AND ent.Filial >= 0  ');
  VAR_SQLx.Add(' ');
  //
  VAR_SQL1.Add('AND ent.Codigo=:P0');
  //
  VAR_SQL2.Add('AND ent.Filial=:P0');
  //
  VAR_SQLa.Add('AND IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) Like :P0');
  //
end;

procedure TFmPrmsCompny.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmPrmsCompny.QueryPrincipalAfterOpen;
begin
end;

procedure TFmPrmsCompny.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARAÇÕES///////////////////

procedure TFmPrmsCompny.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmPrmsCompny.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmPrmsCompny.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmPrmsCompny.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmPrmsCompny.SpeedButton5Click(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  ExesD_Jan.MostraFormLoclzL10nCab(QrPrmsCompnyLoclzL10nCab.Value);
  UMyMod.SetaCodigoPesquisado(EdLoclzL10nCab, CBLoclzL10nCab, QrLoclzL10nCab, VAR_CADASTRO);
end;

procedure TFmPrmsCompny.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmPrmsCompny.BtAlteraClick(Sender: TObject);
begin
  if (QrPrmsCompny.State <> dsInactive) and (QrPrmsCompny.RecordCount > 0) then
  begin
    UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrPrmsCompny, [PnDados],
      [PnEdita], EdLoclzL10nCab, ImgTipo, 'prmscompny');
    if QrPrmsCompnySIT.Value = 'N' then
      ImgTipo.SQLType := stIns;
  end;
end;

procedure TFmPrmsCompny.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrPrmsCompnyCodigo.Value;
  Close;
end;

procedure TFmPrmsCompny.BtConfirmaClick(Sender: TObject);
var
  SiglaPais, Company, MailsDstn, MailsDsKm, NoEmpApp: String;
  Entidade, LoclzL10nCab: Integer;
  SQLType: TSQLType;
begin
  SQLType        := ImgTipo.SQLType;
  Entidade       := EdCodigo.ValueVariant;
  LoclzL10nCab   := EdLoclzL10nCab.ValueVariant;
  SiglaPais      := EdSiglaPais.ValueVariant;
  Company        := EdCompany.ValueVariant;
  MailsDstn      := EdMailsDstn.ValueVariant;
  MailsDsKm      := EdMailsDsKm.ValueVariant;
  NoEmpApp       := EdNoEmpApp.ValueVariant;
  //
  if MyObjects.FIC(LoclzL10nCab = 0, EdLoclzL10nCab, 'Defina a localização!') then Exit;
  //
  //ou > ? := UMyMod.BPGS1I32('prmscompny', 'Entidade', '', '', tsPosNeg?, stInsUpd?, CodAtual?);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'prmscompny', False, [
  'LoclzL10nCab', 'SiglaPais', 'Company',
  'MailsDstn', 'MailsDsKm', 'NoEmpApp'], [
  'Entidade'], [
  LoclzL10nCab, SiglaPais, Company,
  MailsDstn, MailsDsKm, NoEmpApp], [
  Entidade], True) then
  begin
    ImgTipo.SQLType := stLok;
    PnDados.Visible := True;
    PnEdita.Visible := False;
    GOTOy.BotoesSb(ImgTipo.SQLType);
    //
    LocCod(Entidade, Entidade);
  end;
end;

procedure TFmPrmsCompny.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := EdCodigo.ValueVariant;
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  //
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'prmscompny', 'Codigo');
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmPrmsCompny.BtIncluiClick(Sender: TObject);
begin
(*
  UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrPrmsCompny, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'prmscompny');
*)
  Empresas_Jan.MostraFormMatriz();
end;

procedure TFmPrmsCompny.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  GBEdita.Align := alClient;
  GBDados.Align := alClient;
  UnDmkDAC_PF.AbreQuery(QrLoclzL10nCab, Dmod.MyDB);
  CriaOForm;
end;

procedure TFmPrmsCompny.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrPrmsCompnyCodigo.Value, LaRegistro.Caption);
end;

procedure TFmPrmsCompny.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmPrmsCompny.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrPrmsCompnyCodigo.Value, LaRegistro.Caption);
end;

procedure TFmPrmsCompny.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmPrmsCompny.QrPrmsCompnyAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmPrmsCompny.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmPrmsCompny.SbQueryClick(Sender: TObject);
begin
  LocCod(QrPrmsCompnyCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'prmscompny', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmPrmsCompny.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmPrmsCompny.QrPrmsCompnyBeforeOpen(DataSet: TDataSet);
begin
  QrPrmsCompnyCodigo.DisplayFormat := FFormatFloat;
end;

end.

