object FmFilialUsrs: TFmFilialUsrs
  Left = 339
  Top = 185
  Caption = 'SYS-PARAM-005 :: Configura'#231#245'es de Usu'#225'rios por filial'
  ClientHeight = 629
  ClientWidth = 812
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 812
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 764
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 716
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 429
        Height = 32
        Caption = 'Configura'#231#245'es de Usu'#225'rios por filial'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 429
        Height = 32
        Caption = 'Configura'#231#245'es de Usu'#225'rios por filial'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 429
        Height = 32
        Caption = 'Configura'#231#245'es de Usu'#225'rios por filial'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 812
    Height = 467
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 812
      Height = 467
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 812
        Height = 467
        Align = alClient
        TabOrder = 0
        object Splitter1: TSplitter
          Left = 2
          Top = 262
          Width = 808
          Height = 10
          Cursor = crVSplit
          Align = alBottom
          ExplicitTop = 168
        end
        object dmkDBGrid1: TdmkDBGrid
          Left = 2
          Top = 15
          Width = 808
          Height = 247
          Align = alClient
          Columns = <
            item
              Expanded = False
              FieldName = 'Codigo'
              Title.Caption = 'Entidade'
              Width = 51
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Filial'
              Width = 36
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMEENT'
              Title.Caption = 'Nome'
              Width = 272
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'CNPJCPF_TXT'
              Title.Caption = 'CNPJ/CPF'
              Width = 112
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'IERG'
              Title.Caption = 'I.E./RG'
              Width = 112
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Atividade'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'FormaSociet'
              Title.Caption = 'Forma societ'#225'ria'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Simples'
              Title.Caption = 'Simples Estadual'
              Visible = True
            end>
          Color = clWindow
          DataSource = DsFiliais
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
          TabOrder = 0
          TitleFont.Charset = ANSI_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'Codigo'
              Title.Caption = 'Entidade'
              Width = 51
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Filial'
              Width = 36
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMEENT'
              Title.Caption = 'Nome'
              Width = 272
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'CNPJCPF_TXT'
              Title.Caption = 'CNPJ/CPF'
              Width = 112
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'IERG'
              Title.Caption = 'I.E./RG'
              Width = 112
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Atividade'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'FormaSociet'
              Title.Caption = 'Forma societ'#225'ria'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Simples'
              Title.Caption = 'Simples Estadual'
              Visible = True
            end>
        end
        object PageControl1: TPageControl
          Left = 2
          Top = 272
          Width = 808
          Height = 193
          ActivePage = TabSheet1
          Align = alBottom
          TabOrder = 1
          object TabSheet1: TTabSheet
            Caption = 'Usu'#225'rios'
            object dmkDBGrid2: TdmkDBGrid
              Left = 0
              Top = 0
              Width = 800
              Height = 165
              Align = alClient
              Columns = <
                item
                  Expanded = False
                  FieldName = 'Controle'
                  Width = 44
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Numero'
                  Title.Caption = 'Usu'#225'rio'
                  Width = 44
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Login'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NOMEFUNCIONARIO'
                  Title.Caption = 'Nome do funcion'#225'rio'
                  Width = 308
                  Visible = True
                end>
              Color = clWindow
              DataSource = DsUsuarios
              Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
              TabOrder = 0
              TitleFont.Charset = ANSI_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              Columns = <
                item
                  Expanded = False
                  FieldName = 'Controle'
                  Width = 44
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Numero'
                  Title.Caption = 'Usu'#225'rio'
                  Width = 44
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Login'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NOMEFUNCIONARIO'
                  Title.Caption = 'Nome do funcion'#225'rio'
                  Width = 308
                  Visible = True
                end>
            end
          end
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 515
    Width = 812
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 808
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 559
    Width = 812
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 666
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 664
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtUsuario: TBitBtn
        Tag = 10047
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Usu'#225'rio'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtUsuarioClick
      end
    end
  end
  object QrFiliais: TMySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrFiliaisBeforeClose
    AfterScroll = QrFiliaisAfterScroll
    OnCalcFields = QrFiliaisCalcFields
    SQL.Strings = (
      'SELECT ent.Codigo, ent.Filial,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENT,'
      'IF(ent.Tipo=0, ent.CNPJ, ent.CPF) CNPJCPF, '
      'IF(ent.Tipo=0, ent.IE, ent.RG) IERG,'
      'FormaSociet, Simples, Atividade  '
      'FROM entidades ent'
      'WHERE ent.Codigo < -10')
    Left = 204
    Top = 180
    object QrFiliaisCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrFiliaisFilial: TIntegerField
      FieldName = 'Filial'
      Required = True
    end
    object QrFiliaisNOMEENT: TWideStringField
      FieldName = 'NOMEENT'
      Required = True
      Size = 100
    end
    object QrFiliaisCNPJCPF: TWideStringField
      FieldName = 'CNPJCPF'
      Size = 18
    end
    object QrFiliaisIERG: TWideStringField
      FieldName = 'IERG'
    end
    object QrFiliaisFormaSociet: TWideStringField
      FieldName = 'FormaSociet'
    end
    object QrFiliaisSimples: TSmallintField
      FieldName = 'Simples'
      Required = True
    end
    object QrFiliaisAtividade: TWideStringField
      FieldName = 'Atividade'
      Size = 50
    end
    object QrFiliaisCNPJCPF_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CNPJCPF_TXT'
      Size = 40
      Calculated = True
    end
  end
  object DsFiliais: TDataSource
    DataSet = QrFiliais
    Left = 204
    Top = 228
  end
  object QrUsuarios: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT si.Numero, si.Controle, si.Empresa, '
      'se.Login, en.Nome NOMEFUNCIONARIO '
      'FROM senhasits si'
      'LEFT JOIN senhas se ON si.Numero=se.Numero'
      'LEFT JOIN entidades en ON en.Codigo=se.Funcionario'
      'WHERE si.Empresa=:P0')
    Left = 276
    Top = 180
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrUsuariosNumero: TIntegerField
      FieldName = 'Numero'
      Required = True
    end
    object QrUsuariosControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrUsuariosEmpresa: TIntegerField
      FieldName = 'Empresa'
      Required = True
    end
    object QrUsuariosLogin: TWideStringField
      FieldName = 'Login'
      Required = True
      Size = 30
    end
    object QrUsuariosNOMEFUNCIONARIO: TWideStringField
      FieldName = 'NOMEFUNCIONARIO'
      Size = 100
    end
  end
  object DsUsuarios: TDataSource
    DataSet = QrUsuarios
    Left = 276
    Top = 228
  end
  object PMUsuario: TPopupMenu
    Left = 152
    Top = 456
    object Adicionausurio1: TMenuItem
      Caption = '&Adiciona usu'#225'rio'
      OnClick = Adicionausurio1Click
    end
    object Retirausurio1: TMenuItem
      Caption = '&Retira usu'#225'rio'
      OnClick = Retirausurio1Click
    end
  end
end
