object FmWOrdSerGruIts: TFmWOrdSerGruIts
  Left = 339
  Top = 185
  Caption = 'WEB-ORSER-021 :: Itens do Grupo de Ordem de Servi'#231'o'
  ClientHeight = 524
  ClientWidth = 567
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 567
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 519
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 471
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 445
        Height = 32
        Caption = 'Itens do Grupo de Ordem de Servi'#231'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 445
        Height = 32
        Caption = 'Itens do Grupo de Ordem de Servi'#231'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 445
        Height = 32
        Caption = 'Itens do Grupo de Ordem de Servi'#231'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 567
    Height = 362
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 567
      Height = 362
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 567
        Height = 100
        Align = alTop
        TabOrder = 0
        object Label9: TLabel
          Left = 9
          Top = 11
          Width = 14
          Height = 13
          Caption = 'ID:'
        end
        object Label12: TLabel
          Left = 85
          Top = 11
          Width = 51
          Height = 13
          Caption = 'Descri'#231#227'o:'
        end
        object Label18: TLabel
          Left = 341
          Top = 10
          Width = 88
          Height = 13
          Caption = 'Previs'#227'o de in'#237'cio:'
        end
        object Label1: TLabel
          Left = 448
          Top = 10
          Width = 96
          Height = 13
          Caption = 'Previs'#227'o de t'#233'rmino:'
        end
        object LaRespons: TLabel
          Left = 9
          Top = 54
          Width = 65
          Height = 13
          Caption = 'Respons'#225'vel:'
        end
        object SBRespons: TSpeedButton
          Left = 527
          Top = 70
          Width = 21
          Height = 21
          Caption = '...'
          OnClick = SBResponsClick
        end
        object EdCodigo: TdmkEdit
          Left = 9
          Top = 26
          Width = 71
          Height = 21
          Alignment = taRightJustify
          Color = clInactiveCaption
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBackground
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          ReadOnly = True
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'Codigo'
          UpdCampo = 'Codigo'
          UpdType = utInc
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
        object EdNome: TdmkEdit
          Left = 85
          Top = 26
          Width = 250
          Height = 21
          MaxLength = 50
          TabOrder = 1
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object TPIniData: TdmkEditDateTimePicker
          Left = 341
          Top = 26
          Width = 100
          Height = 21
          Date = 39422.782497824100000000
          Time = 39422.782497824100000000
          TabOrder = 2
          ReadOnly = False
          DefaultEditMask = '!99/99/99;1;_'
          AutoApplyEditMask = True
          UpdType = utYes
        end
        object TPFimData: TdmkEditDateTimePicker
          Left = 448
          Top = 26
          Width = 100
          Height = 21
          Date = 39422.782497824100000000
          Time = 39422.782497824100000000
          TabOrder = 3
          ReadOnly = False
          DefaultEditMask = '!99/99/99;1;_'
          AutoApplyEditMask = True
          UpdType = utYes
        end
        object EdRespons: TdmkEditCB
          Left = 9
          Top = 70
          Width = 56
          Height = 21
          Alignment = taRightJustify
          TabOrder = 4
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'Respons'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          DBLookupComboBox = CBRespons
          IgnoraDBLookupComboBox = False
        end
        object CBRespons: TdmkDBLookupComboBox
          Left = 65
          Top = 70
          Width = 460
          Height = 21
          KeyField = 'Codigo'
          ListField = 'PersonalName'
          ListSource = DModWOrdSer.DsResp
          TabOrder = 5
          dmkEditCB = EdRespons
          QryCampo = 'StatOS'
          UpdType = utYes
          LocF7SQLMasc = '$#'
        end
      end
      object PageControl1: TPageControl
        Left = 0
        Top = 100
        Width = 567
        Height = 262
        ActivePage = TabSheet2
        Align = alClient
        TabOrder = 1
        object TabSheet1: TTabSheet
          Caption = 'Descri'#231#227'o:'
          object MeObserv: TMemo
            Left = 0
            Top = 0
            Width = 559
            Height = 234
            Align = alClient
            ScrollBars = ssVertical
            TabOrder = 0
          end
        end
        object TabSheet2: TTabSheet
          Caption = 'Etapas'
          ImageIndex = 1
          object GroupBox7: TGroupBox
            Left = 11
            Top = 5
            Width = 450
            Height = 220
            Caption = 'Etapas do grupo de solicita'#231#245'es'
            TabOrder = 0
            object Label10: TLabel
              Left = 12
              Top = 23
              Width = 39
              Height = 13
              Caption = 'Status1:'
            end
            object Label11: TLabel
              Left = 12
              Top = 71
              Width = 39
              Height = 13
              Caption = 'Status2:'
            end
            object Label2: TLabel
              Left = 12
              Top = 167
              Width = 39
              Height = 13
              Caption = 'Status4:'
            end
            object Label13: TLabel
              Left = 12
              Top = 119
              Width = 39
              Height = 13
              Caption = 'Status3:'
            end
            object EdGruStatus1: TdmkEditCB
              Left = 12
              Top = 42
              Width = 56
              Height = 21
              Alignment = taRightJustify
              TabOrder = 0
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              DBLookupComboBox = CBGruStatus1
              IgnoraDBLookupComboBox = False
            end
            object CBGruStatus1: TdmkDBLookupComboBox
              Left = 70
              Top = 42
              Width = 370
              Height = 21
              KeyField = 'Codigo'
              ListField = 'Nome'
              ListSource = DsGruStatus1
              TabOrder = 1
              dmkEditCB = EdGruStatus1
              UpdType = utYes
              LocF7SQLMasc = '$#'
            end
            object EdGruStatus2: TdmkEditCB
              Left = 12
              Top = 90
              Width = 56
              Height = 21
              Alignment = taRightJustify
              TabOrder = 2
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              DBLookupComboBox = CBGruStatus2
              IgnoraDBLookupComboBox = False
            end
            object CBGruStatus2: TdmkDBLookupComboBox
              Left = 74
              Top = 90
              Width = 370
              Height = 21
              KeyField = 'Codigo'
              ListField = 'Nome'
              ListSource = DsGruStatus2
              TabOrder = 3
              dmkEditCB = EdGruStatus2
              UpdType = utYes
              LocF7SQLMasc = '$#'
            end
            object EdGruStatus4: TdmkEditCB
              Left = 12
              Top = 186
              Width = 56
              Height = 21
              Alignment = taRightJustify
              TabOrder = 6
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              DBLookupComboBox = CBGruStatus4
              IgnoraDBLookupComboBox = False
            end
            object CBGruStatus4: TdmkDBLookupComboBox
              Left = 70
              Top = 186
              Width = 370
              Height = 21
              KeyField = 'Codigo'
              ListField = 'Nome'
              ListSource = DsGruStatus4
              TabOrder = 7
              dmkEditCB = EdGruStatus4
              UpdType = utYes
              LocF7SQLMasc = '$#'
            end
            object EdGruStatus3: TdmkEditCB
              Left = 12
              Top = 138
              Width = 56
              Height = 21
              Alignment = taRightJustify
              TabOrder = 4
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              DBLookupComboBox = CBGruStatus3
              IgnoraDBLookupComboBox = False
            end
            object CBGruStatus3: TdmkDBLookupComboBox
              Left = 70
              Top = 138
              Width = 370
              Height = 21
              KeyField = 'Codigo'
              ListField = 'Nome'
              ListSource = DsGruStatus3
              TabOrder = 5
              dmkEditCB = EdGruStatus3
              UpdType = utYes
              LocF7SQLMasc = '$#'
            end
          end
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 410
    Width = 567
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 563
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 454
    Width = 567
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 421
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 419
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object QrGruStatus4: TmySQLQuery
    Database = Dmod.MyDBn
    SQL.Strings = (
      'SELECT * '
      'FROM wordsersta'
      'WHERE WOrdSerEnc = 0'
      'ORDER BY Nome')
    Left = 460
    Top = 168
    object IntegerField5: TIntegerField
      FieldName = 'Codigo'
    end
    object QrGruStatus4Nome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsGruStatus4: TDataSource
    DataSet = QrGruStatus4
    Left = 488
    Top = 168
  end
  object DsGruStatus3: TDataSource
    DataSet = QrGruStatus3
    Left = 432
    Top = 168
  end
  object QrGruStatus3: TmySQLQuery
    Database = Dmod.MyDBn
    SQL.Strings = (
      'SELECT * '
      'FROM wordsersta'
      'WHERE WOrdSerEnc = 0'
      'ORDER BY Nome')
    Left = 404
    Top = 168
    object IntegerField6: TIntegerField
      FieldName = 'Codigo'
    end
    object QrGruStatus3Nome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object QrGruStatus2: TmySQLQuery
    Database = Dmod.MyDBn
    SQL.Strings = (
      'SELECT * '
      'FROM wordsersta'
      'WHERE WOrdSerEnc = 0 '
      'ORDER BY Nome')
    Left = 348
    Top = 168
    object IntegerField4: TIntegerField
      FieldName = 'Codigo'
    end
    object QrGruStatus2Nome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsGruStatus2: TDataSource
    DataSet = QrGruStatus2
    Left = 376
    Top = 168
  end
  object DsGruStatus1: TDataSource
    DataSet = QrGruStatus1
    Left = 320
    Top = 168
  end
  object QrGruStatus1: TmySQLQuery
    Database = Dmod.MyDBn
    SQL.Strings = (
      'SELECT * '
      'FROM wordsersta'
      'WHERE WOrdSerEnc = 0 '
      'ORDER BY Nome')
    Left = 292
    Top = 168
    object IntegerField3: TIntegerField
      FieldName = 'Codigo'
    end
    object QrGruStatus1Nome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
end
