object FmWOrdSerGru: TFmWOrdSerGru
  Left = 339
  Top = 185
  Caption = 'WEB-ORSER-020 :: Grupos de Ordem de Servi'#231'o'
  ClientHeight = 619
  ClientWidth = 993
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 993
    Height = 47
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 62
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alLeft
      TabOrder = 0
      object BtWOrdSerGru: TBitBtn
        Tag = 179
        Left = 5
        Top = 4
        Width = 49
        Height = 49
        Cursor = crHandPoint
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtWOrdSerGruClick
      end
    end
    object GB_M: TGroupBox
      Left = 62
      Top = 0
      Width = 1070
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      TabOrder = 1
      object LaTitulo1A: TLabel
        Left = 9
        Top = 11
        Width = 418
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Grupos de Ordem de Servi'#231'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 11
        Top = 14
        Width = 418
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Grupos de Ordem de Servi'#231'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 10
        Top = 12
        Width = 418
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Grupos de Ordem de Servi'#231'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
    object GB_R: TGroupBox
      Left = 1132
      Top = 0
      Width = 109
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      TabOrder = 2
      object ImgTipo: TdmkImage
        Left = 59
        Top = 14
        Width = 39
        Height = 39
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Transparent = True
        SQLType = stNil
      end
      object ImgWEB: TdmkImage
        Left = 10
        Top = 14
        Width = 39
        Height = 39
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Transparent = True
        SQLType = stNil
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 47
    Width = 993
    Height = 503
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 993
      Height = 503
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 1241
      ExplicitHeight = 629
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 993
        Height = 503
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        TabOrder = 0
        ExplicitWidth = 1241
        ExplicitHeight = 629
        object Splitter1: TSplitter
          Left = 378
          Top = 15
          Width = 12
          Height = 486
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          ExplicitTop = 18
          ExplicitHeight = 608
        end
        object Panel10: TPanel
          Left = 2
          Top = 15
          Width = 376
          Height = 486
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alLeft
          ParentBackground = False
          TabOrder = 0
          ExplicitTop = 18
          ExplicitHeight = 609
          object Splitter2: TSplitter
            Left = 1
            Top = 299
            Width = 374
            Height = 12
            Cursor = crVSplit
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alBottom
            ExplicitTop = 298
            ExplicitWidth = 373
          end
          object Panel12: TPanel
            Left = 1
            Top = 1
            Width = 374
            Height = 59
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alTop
            TabOrder = 0
            object BtIncluiGrupo: TBitBtn
              Tag = 10
              Left = 6
              Top = 6
              Width = 49
              Height = 49
              Cursor = crHandPoint
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              NumGlyphs = 2
              ParentShowHint = False
              ShowHint = True
              TabOrder = 0
              OnClick = BtIncluiGrupoClick
            end
            object BtAlteraGrupo: TBitBtn
              Tag = 11
              Left = 55
              Top = 6
              Width = 50
              Height = 49
              Cursor = crHandPoint
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              NumGlyphs = 2
              ParentShowHint = False
              ShowHint = True
              TabOrder = 1
              OnClick = BtAlteraGrupoClick
            end
            object BtExcluiGrupo: TBitBtn
              Tag = 12
              Left = 105
              Top = 6
              Width = 49
              Height = 49
              Cursor = crHandPoint
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              NumGlyphs = 2
              ParentShowHint = False
              ShowHint = True
              TabOrder = 2
              OnClick = BtExcluiGrupoClick
            end
            object BrReordena: TBitBtn
              Tag = 73
              Left = 154
              Top = 6
              Width = 49
              Height = 49
              Cursor = crHandPoint
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              NumGlyphs = 2
              ParentShowHint = False
              ShowHint = True
              TabOrder = 3
              OnClick = BrReordenaClick
            end
          end
          object DBGGrupo: TDBGrid
            Left = 1
            Top = 60
            Width = 374
            Height = 239
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alClient
            DataSource = DsWOrdSerGru
            TabOrder = 1
            TitleFont.Charset = ANSI_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            OnDragDrop = DBGGrupoDragDrop
            OnDragOver = DBGGrupoDragOver
            Columns = <
              item
                Expanded = False
                FieldName = 'Nome'
                Title.Caption = 'Descri'#231#227'o'
                Width = 184
                Visible = True
              end>
          end
          object Panel6: TPanel
            Left = 1
            Top = 311
            Width = 374
            Height = 297
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alBottom
            TabOrder = 2
            object Label4: TLabel
              Left = 1
              Top = 169
              Width = 66
              Height = 13
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alTop
              Caption = 'Observa'#231#245'es:'
              FocusControl = DBEdCodigo
            end
            object Panel7: TPanel
              Left = 1
              Top = 1
              Width = 372
              Height = 168
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alTop
              BevelOuter = bvNone
              TabOrder = 0
              object Label3: TLabel
                Left = 6
                Top = 108
                Width = 65
                Height = 13
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'Respons'#225'vel:'
                FocusControl = dmkDBEdit3
              end
              object Label1: TLabel
                Left = 6
                Top = 10
                Width = 14
                Height = 13
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'ID:'
                FocusControl = DBEdCodigo
              end
              object Label8: TLabel
                Left = 107
                Top = 59
                Width = 88
                Height = 13
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'Previs'#227'o de in'#237'cio:'
                FocusControl = dmkDBEdit2
              end
              object Label2: TLabel
                Left = 236
                Top = 59
                Width = 96
                Height = 13
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'Previs'#227'o de t'#233'rmino:'
                FocusControl = dmkDBEdit1
              end
              object Label6: TLabel
                Left = 6
                Top = 59
                Width = 76
                Height = 13
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'Dura'#231#227'o aprox.:'
                FocusControl = DBEdCodigo
              end
              object dmkDBEdit3: TdmkDBEdit
                Left = 6
                Top = 132
                Width = 353
                Height = 24
                Hint = 'N'#186' do banco'
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                TabStop = False
                DataField = 'Respons_TXT'
                DataSource = DsWOrdSerGru
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -15
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
                ParentShowHint = False
                ReadOnly = True
                ShowHint = True
                TabOrder = 4
                UpdType = utYes
                Alignment = taLeftJustify
              end
              object DBEdCodigo: TdmkDBEdit
                Left = 6
                Top = 30
                Width = 69
                Height = 24
                Hint = 'N'#186' do banco'
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                TabStop = False
                DataField = 'Codigo'
                DataSource = DsWOrdSerGru
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -15
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
                ParentShowHint = False
                ReadOnly = True
                ShowHint = True
                TabOrder = 0
                UpdType = utYes
                Alignment = taRightJustify
              end
              object dmkDBEdit2: TdmkDBEdit
                Left = 107
                Top = 79
                Width = 123
                Height = 24
                Hint = 'N'#186' do banco'
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                TabStop = False
                DataField = 'IniData_TXT'
                DataSource = DsWOrdSerGru
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -15
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
                ParentShowHint = False
                ReadOnly = True
                ShowHint = True
                TabOrder = 2
                UpdType = utYes
                Alignment = taCenter
              end
              object dmkDBEdit1: TdmkDBEdit
                Left = 236
                Top = 79
                Width = 123
                Height = 24
                Hint = 'N'#186' do banco'
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                TabStop = False
                DataField = 'FimData_TXT'
                DataSource = DsWOrdSerGru
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -15
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
                ParentShowHint = False
                ReadOnly = True
                ShowHint = True
                TabOrder = 3
                UpdType = utYes
                Alignment = taCenter
              end
              object EdDuracao: TdmkEdit
                Left = 5
                Top = 79
                Width = 97
                Height = 26
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Alignment = taRightJustify
                Font.Charset = ANSI_CHARSET
                Font.Color = clWindowText
                Font.Height = -15
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                ParentFont = False
                ReadOnly = True
                TabOrder = 1
                FormatType = dmktfDouble
                MskType = fmtNone
                DecimalSize = 2
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0,00'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0.000000000000000000
                ValWarn = False
              end
            end
            object DBMemo1: TDBMemo
              Left = 1
              Top = 185
              Width = 372
              Height = 111
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alClient
              DataField = 'Observ'
              DataSource = DsWOrdSerGru
              TabOrder = 1
            end
          end
        end
        object PnGrupo: TPanel
          Left = 390
          Top = 15
          Width = 601
          Height = 486
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 1
          ExplicitTop = 18
          ExplicitWidth = 849
          ExplicitHeight = 609
          object PCGrupo: TPageControl
            Left = 0
            Top = 59
            Width = 601
            Height = 427
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            ActivePage = TSGrade
            Align = alClient
            TabHeight = 25
            TabOrder = 0
            OnChange = PCGrupoChange
            ExplicitHeight = 424
            object TSChamados: TTabSheet
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Lista'
              ImageIndex = 2
              object LaOrderByGrupo: TLabel
                Left = 0
                Top = 495
                Width = 175
                Height = 20
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Align = alBottom
                Caption = 'Ordena'#231#227'o da grade: '
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clGreen
                Font.Height = -17
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                ParentFont = False
              end
              object Splitter4: TSplitter
                Left = 521
                Top = 0
                Width = 12
                Height = 475
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Align = alRight
                ExplicitLeft = 518
                ExplicitHeight = 466
              end
              object LaTotal0: TLabel
                Left = 0
                Top = 475
                Width = 6
                Height = 20
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Align = alBottom
                Font.Charset = ANSI_CHARSET
                Font.Color = clWindowText
                Font.Height = -17
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                ParentFont = False
              end
              object Panel19: TPanel
                Left = 533
                Top = 0
                Width = 308
                Height = 475
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Align = alRight
                TabOrder = 0
                object DBMemo5: TDBMemo
                  Left = 1
                  Top = 21
                  Width = 306
                  Height = 453
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Align = alClient
                  DataField = 'Descri'
                  DataSource = DsWOrdSer
                  TabOrder = 0
                end
                object StaticText8: TStaticText
                  Left = 1
                  Top = 1
                  Width = 75
                  Height = 20
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Align = alTop
                  Alignment = taCenter
                  BorderStyle = sbsSunken
                  Caption = 'Descri'#231#227'o'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -15
                  Font.Name = 'MS Sans Serif'
                  Font.Style = [fsBold]
                  ParentFont = False
                  TabOrder = 1
                end
              end
              object DBWOSGruIts: TdmkDBGrid
                Left = 0
                Top = 0
                Width = 521
                Height = 475
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Align = alClient
                Columns = <
                  item
                    Expanded = False
                    FieldName = 'Codigo'
                    Title.Caption = 'ID'
                    Width = 75
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'DuracaoApr'
                    Title.Caption = 'Dura'#231#227'o (h)'
                    Width = 100
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'NOMESTA'
                    Title.Caption = 'Status'
                    Width = 150
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'NOMEPRI'
                    Title.Caption = 'Prioridade'
                    Width = 100
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'AssuntoTXT'
                    Title.Caption = 'Assunto'
                    Width = 200
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'AssuntoGru'
                    Title.Caption = 'Tipo de assunto'
                    Width = 100
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'NOMEAPLIC'
                    Title.Caption = 'Aplicativo'
                    Width = 100
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'NOMERESP'
                    Title.Caption = 'Respons'#225'vel'
                    Width = 200
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'ABERTURA_TXT'
                    Title.Caption = 'Abertura'
                    Width = 130
                    Visible = True
                  end>
                Color = clWindow
                DataSource = DsWOrdSer
                Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgMultiSelect, dgTitleClick, dgTitleHotTrack]
                TabOrder = 1
                TitleFont.Charset = ANSI_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -11
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                FieldsCalcToOrder.Strings = (
                  'ABERTURA_TXT=Abertura'
                  'ENCERROU_TXT=Encerr')
                OnMouseMove = DBWOSGruItsMouseMove
                Columns = <
                  item
                    Expanded = False
                    FieldName = 'Codigo'
                    Title.Caption = 'ID'
                    Width = 75
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'DuracaoApr'
                    Title.Caption = 'Dura'#231#227'o (h)'
                    Width = 100
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'NOMESTA'
                    Title.Caption = 'Status'
                    Width = 150
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'NOMEPRI'
                    Title.Caption = 'Prioridade'
                    Width = 100
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'AssuntoTXT'
                    Title.Caption = 'Assunto'
                    Width = 200
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'AssuntoGru'
                    Title.Caption = 'Tipo de assunto'
                    Width = 100
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'NOMEAPLIC'
                    Title.Caption = 'Aplicativo'
                    Width = 100
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'NOMERESP'
                    Title.Caption = 'Respons'#225'vel'
                    Width = 200
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'ABERTURA_TXT'
                    Title.Caption = 'Abertura'
                    Width = 130
                    Visible = True
                  end>
              end
            end
            object TSGrade: TTabSheet
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Grade'
              ImageIndex = 1
              object PnGrade: TPanel
                Left = 0
                Top = 0
                Width = 841
                Height = 515
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Align = alClient
                BevelOuter = bvNone
                TabOrder = 0
                object LaTotal2: TLabel
                  Left = 0
                  Top = 495
                  Width = 6
                  Height = 20
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Align = alBottom
                  Font.Charset = ANSI_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -17
                  Font.Name = 'MS Sans Serif'
                  Font.Style = [fsBold]
                  ParentFont = False
                end
                object PnGrupoRight: TPanel
                  Left = 410
                  Top = 0
                  Width = 431
                  Height = 495
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Align = alRight
                  ParentBackground = False
                  TabOrder = 0
                  object StaticText5: TStaticText
                    Left = 1
                    Top = 1
                    Width = 82
                    Height = 20
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    Align = alTop
                    Alignment = taCenter
                    BorderStyle = sbsSunken
                    Caption = 'Solicita'#231#227'o'
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clWindowText
                    Font.Height = -15
                    Font.Name = 'MS Sans Serif'
                    Font.Style = [fsBold]
                    ParentFont = False
                    TabOrder = 0
                  end
                  object Panel14: TPanel
                    Left = 1
                    Top = 21
                    Width = 429
                    Height = 154
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    Align = alTop
                    BevelOuter = bvNone
                    TabOrder = 1
                    object Label22: TLabel
                      Left = 5
                      Top = 4
                      Width = 14
                      Height = 13
                      Margins.Left = 4
                      Margins.Top = 4
                      Margins.Right = 4
                      Margins.Bottom = 4
                      Caption = 'ID:'
                      FocusControl = dmkDBEdit4
                    end
                    object Label23: TLabel
                      Left = 5
                      Top = 53
                      Width = 65
                      Height = 13
                      Margins.Left = 4
                      Margins.Top = 4
                      Margins.Right = 4
                      Margins.Bottom = 4
                      Caption = 'Respons'#225'vel:'
                      FocusControl = dmkDBEdit5
                    end
                    object Label5: TLabel
                      Left = 5
                      Top = 102
                      Width = 50
                      Height = 13
                      Margins.Left = 4
                      Margins.Top = 4
                      Margins.Right = 4
                      Margins.Bottom = 4
                      Caption = 'Prioridade:'
                      FocusControl = dmkDBEdit6
                    end
                    object Label7: TLabel
                      Left = 160
                      Top = 27
                      Width = 153
                      Height = 13
                      Margins.Left = 4
                      Margins.Top = 4
                      Margins.Right = 4
                      Margins.Bottom = 4
                      Caption = 'Dura'#231#227'o (aproximada em horas):'
                    end
                    object dmkDBEdit4: TdmkDBEdit
                      Left = 5
                      Top = 23
                      Width = 69
                      Height = 24
                      Hint = 'N'#186' do banco'
                      Margins.Left = 4
                      Margins.Top = 4
                      Margins.Right = 4
                      Margins.Bottom = 4
                      TabStop = False
                      DataField = 'Codigo'
                      DataSource = DsWOrdSerGruIts
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clWindowText
                      Font.Height = -15
                      Font.Name = 'MS Sans Serif'
                      Font.Style = []
                      ParentFont = False
                      ParentShowHint = False
                      ReadOnly = True
                      ShowHint = True
                      TabOrder = 0
                      UpdType = utYes
                      Alignment = taRightJustify
                    end
                    object dmkDBEdit5: TdmkDBEdit
                      Left = 5
                      Top = 73
                      Width = 418
                      Height = 24
                      Hint = 'Nome do banco'
                      Margins.Left = 4
                      Margins.Top = 4
                      Margins.Right = 4
                      Margins.Bottom = 4
                      Color = clWhite
                      DataField = 'NOMERESP'
                      DataSource = DsWOrdSerGruIts
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clWindowText
                      Font.Height = -15
                      Font.Name = 'MS Sans Serif'
                      Font.Style = []
                      ParentFont = False
                      ParentShowHint = False
                      ShowHint = True
                      TabOrder = 1
                      UpdType = utYes
                      Alignment = taLeftJustify
                    end
                    object dmkDBEdit6: TdmkDBEdit
                      Left = 5
                      Top = 123
                      Width = 418
                      Height = 24
                      Hint = 'Nome do banco'
                      Margins.Left = 4
                      Margins.Top = 4
                      Margins.Right = 4
                      Margins.Bottom = 4
                      Color = clWhite
                      DataField = 'NOMEPRIOR'
                      DataSource = DsWOrdSerGruIts
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clWindowText
                      Font.Height = -15
                      Font.Name = 'MS Sans Serif'
                      Font.Style = []
                      ParentFont = False
                      ParentShowHint = False
                      ShowHint = True
                      TabOrder = 2
                      UpdType = utYes
                      Alignment = taLeftJustify
                    end
                    object dmkDBEdit7: TdmkDBEdit
                      Left = 354
                      Top = 23
                      Width = 69
                      Height = 24
                      Hint = 'N'#186' do banco'
                      Margins.Left = 4
                      Margins.Top = 4
                      Margins.Right = 4
                      Margins.Bottom = 4
                      TabStop = False
                      DataField = 'DuracaoApr'
                      DataSource = DsWOrdSerGruIts
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clWindowText
                      Font.Height = -15
                      Font.Name = 'MS Sans Serif'
                      Font.Style = [fsBold]
                      ParentFont = False
                      ParentShowHint = False
                      ReadOnly = True
                      ShowHint = True
                      TabOrder = 3
                      UpdType = utYes
                      Alignment = taRightJustify
                    end
                  end
                  object DBMemo3: TDBMemo
                    Left = 1
                    Top = 175
                    Width = 429
                    Height = 319
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    Align = alClient
                    DataField = 'Descri'
                    DataSource = DsWOrdSerGruIts
                    TabOrder = 2
                  end
                end
                object SGWOrdSerGru: TStringGrid
                  Left = 0
                  Top = 0
                  Width = 410
                  Height = 495
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Align = alClient
                  ColCount = 4
                  FixedCols = 0
                  Font.Charset = ANSI_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -17
                  Font.Name = 'MS Sans Serif'
                  Font.Style = []
                  Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goTabs, goThumbTracking]
                  ParentFont = False
                  TabOrder = 1
                  OnDragDrop = SGWOrdSerGruDragDrop
                  OnDragOver = SGWOrdSerGruDragOver
                  OnDrawCell = SGWOrdSerGruDrawCell
                  OnMouseDown = SGWOrdSerGruMouseDown
                  OnSelectCell = SGWOrdSerGruSelectCell
                  ColWidths = (
                    64
                    64
                    64
                    64)
                end
              end
            end
          end
          object Panel9: TPanel
            Left = 0
            Top = 0
            Width = 601
            Height = 59
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alTop
            Padding.Right = 6
            Padding.Bottom = 6
            ParentBackground = False
            TabOrder = 1
            object LaCodigo: TLabel
              Left = 6
              Top = 21
              Width = 14
              Height = 13
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'ID:'
            end
            object EdCodigo: TdmkEdit
              Left = 31
              Top = 17
              Width = 92
              Height = 26
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Alignment = taRightJustify
              TabOrder = 0
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
            end
            object BtReabre: TBitBtn
              Tag = 18
              Left = 130
              Top = 2
              Width = 50
              Height = 50
              Cursor = crHandPoint
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              NumGlyphs = 2
              ParentShowHint = False
              ShowHint = True
              TabOrder = 1
              OnClick = BtReabreClick
            end
            object RGMostraGrupoIts: TRadioGroup
              Left = 421
              Top = 1
              Width = 173
              Height = 51
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alRight
              Caption = 'Mostrar itens'
              Columns = 2
              ItemIndex = 0
              Items.Strings = (
                'Meus'
                'Todos')
              TabOrder = 2
              OnClick = RGMostraGrupoItsClick
            end
          end
        end
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 550
    Width = 993
    Height = 69
    Align = alBottom
    TabOrder = 2
    object PnSaiDesis: TPanel
      Left = 1032
      Top = 18
      Width = 207
      Height = 66
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 55
        Top = 5
        Width = 148
        Height = 49
        Cursor = crHandPoint
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
      object BtDesconectar: TBitBtn
        Tag = 495
        Left = 5
        Top = 5
        Width = 49
        Height = 49
        Cursor = crHandPoint
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        OnClick = BtDesconectarClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 18
      Width = 1030
      Height = 66
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GBAvisos1: TGroupBox
        Left = 0
        Top = 12
        Width = 1030
        Height = 54
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alBottom
        Caption = ' Avisos: '
        TabOrder = 0
        object Panel4: TPanel
          Left = 2
          Top = 18
          Width = 1026
          Height = 34
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          object LaAviso1: TLabel
            Left = 16
            Top = 2
            Width = 150
            Height = 19
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = '..............................'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clSilver
            Font.Height = -17
            Font.Name = 'Arial'
            Font.Style = []
            ParentFont = False
            Transparent = True
          end
          object LaAviso2: TLabel
            Left = 15
            Top = 1
            Width = 150
            Height = 19
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = '..............................'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clRed
            Font.Height = -17
            Font.Name = 'Arial'
            Font.Style = []
            ParentFont = False
            Transparent = True
          end
        end
      end
    end
  end
  object QrWOrdSerGru: TMySQLQuery
    Database = Dmod.MyDBn
    BeforeClose = QrWOrdSerGruBeforeClose
    AfterScroll = QrWOrdSerGruAfterScroll
    SQL.Strings = (
      'SELECT *'#11
      'FROM wordsergru')
    Left = 82
    Top = 199
    object QrWOrdSerGruCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrWOrdSerGruNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
    object QrWOrdSerGruIniData: TDateField
      FieldName = 'IniData'
    end
    object QrWOrdSerGruFimData: TDateField
      FieldName = 'FimData'
    end
    object QrWOrdSerGruObserv: TWideMemoField
      FieldName = 'Observ'
      BlobType = ftWideMemo
    end
    object QrWOrdSerGruRespons: TIntegerField
      FieldName = 'Respons'
    end
    object QrWOrdSerGruRespons_TXT: TWideStringField
      FieldName = 'Respons_TXT'
      Size = 32
    end
    object QrWOrdSerGruIniData_TXT: TWideStringField
      FieldName = 'IniData_TXT'
      Size = 12
    end
    object QrWOrdSerGruFimData_TXT: TWideStringField
      FieldName = 'FimData_TXT'
      Size = 12
    end
    object QrWOrdSerGruGruStatus1: TIntegerField
      FieldName = 'GruStatus1'
    end
    object QrWOrdSerGruGruStatus2: TIntegerField
      FieldName = 'GruStatus2'
    end
    object QrWOrdSerGruGruStatus3: TIntegerField
      FieldName = 'GruStatus3'
    end
    object QrWOrdSerGruGruStatus4: TIntegerField
      FieldName = 'GruStatus4'
    end
    object QrWOrdSerGruSTATUS42: TWideStringField
      FieldName = 'STATUS4'
    end
    object QrWOrdSerGruSTATUS32: TWideStringField
      FieldName = 'STATUS3'
    end
    object QrWOrdSerGruSTATUS22: TWideStringField
      FieldName = 'STATUS2'
    end
    object QrWOrdSerGruSTATUS12: TWideStringField
      FieldName = 'STATUS1'
    end
  end
  object DsWOrdSerGru: TDataSource
    DataSet = QrWOrdSerGru
    Left = 110
    Top = 199
  end
  object QrWOrdSer: TMySQLQuery
    AutoRefresh = True
    Database = Dmod.MyDBn
    BeforeClose = QrWOrdSerBeforeClose
    AfterScroll = QrWOrdSerAfterScroll
    SQL.Strings = (
      'SELECT wor.*, sta.Nome NOMESTA, srv.Nome NOMESERV,'
      'ass.Nome NOMEASS, pri.Nome NOMEPRI, wmo.Nome NOMEMOD,'
      'IF (ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMECLI,'
      'wur.PersonalName NOMERESP, wus.PersonalName, enc.Descri NOMESOL,'
      'his.DataHora DATAHORA_HIS,'
      '"" NOMEAPLIC, '
      'CASE his.Tarefa'
      'WHEN 0 THEN "Abertura"'
      'WHEN 1 THEN "Encerramento"'
      'WHEN 2 THEN "Comentado"'
      'WHEN 3 THEN "Reabertura"'
      'WHEN 4 THEN "Coment'#225'rio exclu'#237'do" END STATUS_TXT,'
      'IF (wor.Finaliz = 0, "O.S. em aberto", '
      'DATE_FORMAT(wor.Encerr, "%d/%m/%Y %H:%i:%s")) ENCERROU_TXT'
      'FROM wordser wor'
      'LEFT JOIN entidades ent ON ent.Codigo = wor.Cliente'
      'LEFT JOIN wordserass ass ON ass.Codigo = wor.Assunto'
      'LEFT JOIN wordsersta sta ON sta.Codigo = wor.Status'
      'LEFT JOIN wordserpri pri ON pri.Codigo = wor.Prioridade'
      'LEFT JOIN wusers wur ON wur.Codigo = wor.Respons'
      'LEFT JOIN wordsermod wmo ON wmo.Codigo = wor.Modo'
      'LEFT JOIN wusers wus ON wus.Codigo = wor.Solicit'
      'LEFT JOIN wordserenc enc ON enc.Codigo = wor.WOrdSerEnc'
      'LEFT JOIN wordserhis his ON his.Controle ='
      '('
      '  SELECT MAX(Controle)'
      '  FROM wordserhis'
      '  WHERE Codigo = wor.Codigo'
      ')'
      'WHERE wor.Codigo > 0')
    Left = 451
    Top = 219
    object QrWOrdSerCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'wordser.Codigo'
    end
    object QrWOrdSerABERTURA_TXT: TWideStringField
      FieldName = 'ABERTURA_TXT'
    end
    object QrWOrdSerNOMEPRI: TWideStringField
      FieldName = 'NOMEPRI'
      Origin = 'wordserpri.Nome'
    end
    object QrWOrdSerNOMEAPLIC: TWideStringField
      FieldName = 'NOMEAPLIC'
      Size = 50
    end
    object QrWOrdSerAssuntoTXT: TWideStringField
      FieldName = 'AssuntoTXT'
      Size = 150
    end
    object QrWOrdSerNOMERESP: TWideStringField
      FieldName = 'NOMERESP'
      Origin = 'wusers.PersonalName'
      Size = 32
    end
    object QrWOrdSerNOMESTA: TWideStringField
      FieldName = 'NOMESTA'
    end
    object QrWOrdSerDescri: TWideMemoField
      DisplayWidth = 10
      FieldName = 'Descri'
      Origin = 'wordser.Descri'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrWOrdSerGrupo: TIntegerField
      FieldName = 'Grupo'
    end
    object QrWOrdSerStatus: TIntegerField
      FieldName = 'Status'
    end
    object QrWOrdSerAplicativo: TIntegerField
      FieldName = 'Aplicativo'
    end
    object QrWOrdSerSolicit: TIntegerField
      FieldName = 'Solicit'
    end
    object QrWOrdSerCliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrWOrdSerAssunto: TIntegerField
      FieldName = 'Assunto'
    end
    object QrWOrdSerDuracaoApr: TFloatField
      FieldName = 'DuracaoApr'
    end
    object QrWOrdSerAssuntoGru: TWideStringField
      FieldName = 'AssuntoGru'
      Size = 50
    end
  end
  object DsWOrdSer: TDataSource
    DataSet = QrWOrdSer
    Left = 479
    Top = 219
  end
  object QrWOrdSerGruIts: TMySQLQuery
    Database = Dmod.MyDBn
    SQL.Strings = (
      'SELECT its.Codigo, its.Descri, wus.NOMEREP'
      'FROM wordser its'
      'LEFT JOIN wusers wus ON wus.Codigo = its.Respons'
      'WHERE Codigo=:P0')
    Left = 498
    Top = 354
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrWOrdSerGruItsCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'wordser.Codigo'
    end
    object QrWOrdSerGruItsNOMERESP: TWideStringField
      FieldName = 'NOMERESP'
      Origin = 'wusers.PersonalName'
      Size = 32
    end
    object QrWOrdSerGruItsDescri: TWideMemoField
      FieldName = 'Descri'
      Origin = 'wordser.Descri'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrWOrdSerGruItsNOMEPRIOR: TWideStringField
      FieldName = 'NOMEPRIOR'
    end
    object QrWOrdSerGruItsDuracaoApr: TFloatField
      FieldName = 'DuracaoApr'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
  end
  object DsWOrdSerGruIts: TDataSource
    DataSet = QrWOrdSerGruIts
    Left = 526
    Top = 354
  end
  object CSTabSheetChamou: TdmkCompoStore
    Left = 620
    Top = 354
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 648
    Top = 354
  end
  object PMMenu: TPopupMenu
    OnPopup = PMMenuPopup
    Left = 368
    Top = 256
    object Inclui1: TMenuItem
      Caption = '&Inclui'
      OnClick = Inclui1Click
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object Alterastatus1: TMenuItem
      Caption = 'Altera &status'
      OnClick = Alterastatus1Click
    end
    object Alteraprioridade1: TMenuItem
      Caption = 'Altera &prioridade'
      OnClick = Alteraprioridade1Click
    end
    object Alteraresponsvel1: TMenuItem
      Caption = 'Altera &respons'#225'vel'
      OnClick = Alteraresponsvel1Click
    end
    object Alteradurao1: TMenuItem
      Caption = 'Altera &dura'#231#227'o aprox.'
      OnClick = Alteradurao1Click
    end
  end
end
