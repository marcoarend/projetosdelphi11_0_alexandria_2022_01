object FmWOrdSerRes: TFmWOrdSerRes
  Left = 339
  Top = 185
  Caption = 'WEB-ORSER-018 :: Respons'#225'vel da Ordem de Servi'#231'o'
  ClientHeight = 237
  ClientWidth = 654
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 654
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 606
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 558
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 421
        Height = 32
        Caption = 'Respons'#225'vel da Ordem de Servi'#231'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 421
        Height = 32
        Caption = 'Respons'#225'vel da Ordem de Servi'#231'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 421
        Height = 32
        Caption = 'Respons'#225'vel da Ordem de Servi'#231'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 654
    Height = 75
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 654
      Height = 75
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 654
        Height = 75
        Align = alClient
        TabOrder = 0
        object Label3: TLabel
          Left = 9
          Top = 18
          Width = 32
          Height = 13
          Caption = 'Grupo:'
        end
        object LaSolicitante: TLabel
          Left = 320
          Top = 18
          Width = 65
          Height = 13
          Caption = 'Respons'#225'vel:'
        end
        object SBSolicitante: TSpeedButton
          Left = 614
          Top = 35
          Width = 21
          Height = 21
          Caption = '...'
          OnClick = SBSolicitanteClick
        end
        object EdGrupo: TdmkEditCB
          Left = 9
          Top = 35
          Width = 56
          Height = 21
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          OnChange = EdGrupoChange
          DBLookupComboBox = CBGrupo
          IgnoraDBLookupComboBox = False
        end
        object CBGrupo: TdmkDBLookupComboBox
          Left = 65
          Top = 35
          Width = 250
          Height = 21
          KeyField = 'Codigo'
          ListField = 'Nome'
          ListSource = DsGrupos
          TabOrder = 1
          dmkEditCB = EdGrupo
          UpdType = utYes
          LocF7SQLMasc = '$#'
        end
        object CBRespons: TdmkDBLookupComboBox
          Left = 376
          Top = 35
          Width = 235
          Height = 21
          KeyField = 'Codigo'
          ListField = 'PersonalName'
          ListSource = DsRespons
          TabOrder = 3
          dmkEditCB = EdRespons
          UpdType = utYes
          LocF7SQLMasc = '$#'
        end
        object EdRespons: TdmkEditCB
          Left = 320
          Top = 35
          Width = 56
          Height = 21
          Alignment = taRightJustify
          TabOrder = 2
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          DBLookupComboBox = CBRespons
          IgnoraDBLookupComboBox = False
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 123
    Width = 654
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 650
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 167
    Width = 654
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 508
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 506
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object QrGrupos: TmySQLQuery
    Database = Dmod.MyDBn
    SQL.Strings = (
      'SELECT Codigo, Nome'#11
      'FROM worseategr'
      'WHERE Ativo = 1'
      'ORDER BY Nome')
    Left = 256
    Top = 152
    object QrGruposCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrGruposNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsGrupos: TDataSource
    DataSet = QrGrupos
    Left = 284
    Top = 152
  end
  object QrRespons: TmySQLQuery
    Database = Dmod.MyDBn
    Left = 312
    Top = 152
    object IntegerField1: TIntegerField
      FieldName = 'Codigo'
    end
    object QrResponsPersonalName: TWideStringField
      FieldName = 'PersonalName'
      Size = 32
    end
  end
  object DsRespons: TDataSource
    DataSet = QrRespons
    Left = 340
    Top = 152
  end
end
