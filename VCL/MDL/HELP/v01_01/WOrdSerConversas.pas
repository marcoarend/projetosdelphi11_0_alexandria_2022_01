unit WOrdSerConversas;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, Tabs, DockTabSet, OleCtrls, SHDocVw, ImgList, Menus,
  dmkGeral, MSHTML;

type
  TFmWOrdSerConversas = class(TForm)
    WebBrowser1: TWebBrowser;
    TabSet1: TTabSet;
    PopupMenu1: TPopupMenu;
    Fecharaba1: TMenuItem;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure TabSet1Change(Sender: TObject; NewTab: Integer;
      var AllowChange: Boolean);
    procedure WebBrowser1DocumentComplete(ASender: TObject;
      const pDisp: IDispatch; var URL: OleVariant);
    procedure Fecharaba1Click(Sender: TObject);
    procedure PopupMenu1Popup(Sender: TObject);
    procedure TabSet1MouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
  private
    { Private declarations }
    FListaTabs, FListaCodigos: TStringList;
    procedure ConfiguraWebBrowser(Tab: Integer);
  public
    { Public declarations }
    procedure ConfiguraListas(Codigo: Integer; Solicitante: String);
  end;

var
  FmWOrdSerConversas: TFmWOrdSerConversas;

implementation

uses WOrdSer;

{$R *.dfm}

procedure TFmWOrdSerConversas.ConfiguraListas(Codigo: Integer; Solicitante: String);
var
  CodLista, Tab, i: Integer;
begin
  Tab := -1;
  for i := 0 to FListaCodigos.Count - 1 do
  begin
    if Tab = -1 then
    begin
      CodLista := Geral.IMV(FListaCodigos[i]);
      if Codigo = CodLista then
        Tab := i;
    end;
  end;
  if Tab = -1 then
  begin
    if FListaTabs <> nil then
      FListaTabs.Add(Geral.FF0(Codigo) + ' - ' + Solicitante);
    if FListaCodigos <> nil then
      FListaCodigos.Add(Geral.FF0(Codigo));
    //
    TabSet1.Tabs := FListaTabs;
    TabSet1.TabIndex := FListaTabs.Count - 1;
  end else
    TabSet1.TabIndex := Tab;
end;

procedure TFmWOrdSerConversas.ConfiguraWebBrowser(Tab: Integer);
var
  Codigo: Integer;
begin
  Codigo := Geral.IMV(FListaCodigos[Tab]);
  if Codigo <> 0 then
    WebBrowser1.Navigate('http://www.dermatek.net.br/dmkWOrdSerCom.php?codigo='
      + Geral.FF0(Codigo));
end;

procedure TFmWOrdSerConversas.Fecharaba1Click(Sender: TObject);
begin
  FListaTabs.Delete(TabSet1.TabIndex);
  FListaCodigos.Delete(TabSet1.TabIndex);
  //
  TabSet1.Tabs     := FListaTabs;
  TabSet1.TabIndex := FListaTabs.Count - 1;
  //
  ConfiguraWebBrowser(TabSet1.TabIndex);
end;

procedure TFmWOrdSerConversas.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  FListaTabs.Free;
  FListaCodigos.Free;
  WebBrowser1.Free;
  //
  FmWOrdSerConversas := nil;
end;

procedure TFmWOrdSerConversas.FormCreate(Sender: TObject);
begin
  FormStyle     := fsStayOnTop;
  FListaTabs    := TStringList.Create;
  FListaCodigos := TStringList.Create;
end;

procedure TFmWOrdSerConversas.PopupMenu1Popup(Sender: TObject);
begin
  Fecharaba1.Enabled := TabSet1.Tabs.Count > 1;
end;

procedure TFmWOrdSerConversas.TabSet1Change(Sender: TObject; NewTab: Integer;
  var AllowChange: Boolean);
begin
  ConfiguraWebBrowser(NewTab);
end;

procedure TFmWOrdSerConversas.TabSet1MouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var
  Tab: Integer;
begin
  if Button = mbRight then
  begin
    Tab := TabSet1.ItemAtPos(Point(X, Y));
    TabSet1.TabIndex := Tab;
  end;
end;

procedure TFmWOrdSerConversas.WebBrowser1DocumentComplete(ASender: TObject;
  const pDisp: IDispatch; var URL: OleVariant);
var
  Doc: IHTMLDocument2;
  Body: OleVariant;
begin
  Doc := IHTMLDocument2(WebBrowser1.Document);
  body := Doc.Body;
  {hide scrollbars}
  Body.Style.BorderStyle := 'none';
  Body.Scroll := 'no';
end;

end.
