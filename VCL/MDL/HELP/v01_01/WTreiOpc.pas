unit WTreiOpc;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkPermissoes, dmkEdit,
  dmkEditCB, dmkDBLookupComboBox, mySQLDbTables;

type
  TFmWTreiOpc = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    GroupBox4: TGroupBox;
    Label4: TLabel;
    Label6: TLabel;
    Label14: TLabel;
    CBEmailAut: TdmkDBLookupComboBox;
    EdEmailAut: TdmkEditCB;
    EdEmailPar: TdmkEditCB;
    CBEmailPar: TdmkDBLookupComboBox;
    EdEmailTrei: TdmkEditCB;
    CBEmailTrei: TdmkDBLookupComboBox;
    GroupBox2: TGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    CBTermoAut: TdmkDBLookupComboBox;
    EdTermoAut: TdmkEditCB;
    EdTermoPar: TdmkEditCB;
    CBTermoPar: TdmkDBLookupComboBox;
    Label8: TLabel;
    Label9: TLabel;
    EdHorasCfgAut: TdmkEdit;
    Label3: TLabel;
    EdHorasCfgPar: TdmkEdit;
    Label5: TLabel;
    QrWTreiOpc: TmySQLQuery;
    DsEmailAut: TDataSource;
    QrEmailAut: TmySQLQuery;
    QrEmailAutCodigo: TIntegerField;
    QrEmailAutNome: TWideStringField;
    QrEmailPar: TmySQLQuery;
    IntegerField1: TIntegerField;
    StringField1: TWideStringField;
    DsEmailPar: TDataSource;
    QrEmailTrei: TmySQLQuery;
    IntegerField2: TIntegerField;
    StringField2: TWideStringField;
    DsEmailTrei: TDataSource;
    QrTermoAut: TmySQLQuery;
    IntegerField3: TIntegerField;
    DsTermoAut: TDataSource;
    DsTermoPar: TDataSource;
    QrTermoPar: TmySQLQuery;
    IntegerField4: TIntegerField;
    StringField4: TWideStringField;
    QrTermoAutNome: TWideStringField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
    procedure MostraEdicao;
  public
    { Public declarations }
  end;

  var
  FmWTreiOpc: TFmWTreiOpc;

implementation

uses UnMyObjects, Module, UMySQLModule;

{$R *.DFM}

procedure TFmWTreiOpc.BtOKClick(Sender: TObject);
var
  HorasCfgAut, HorasCfgPar, EmailAut, EmailPar, EmailTrei, TermoAut,
  TermoPar: Integer;
begin
  HorasCfgAut := EdHorasCfgAut.ValueVariant;
  HorasCfgPar := EdHorasCfgPar.ValueVariant;
  EmailAut    := EdEmailAut.ValueVariant;
  EmailPar    := EdEmailPar.ValueVariant;
  EmailTrei   := EdEmailTrei.ValueVariant;
  TermoAut    := EdTermoAut.ValueVariant;
  TermoPar    := EdTermoPar.ValueVariant;
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpdN, stUpd, 'wtreiopc', False,
    ['HorasCfgAut', 'HorasCfgPar', 'EmailAut', 'EmailPar', 'EmailTrei',
    'TermoAut', 'TermoPar'], ['Codigo'],
    [HorasCfgAut, HorasCfgPar, EmailAut, EmailPar, EmailTrei,
    TermoAut, TermoPar], [1], True)
  then
    Close;
end;

procedure TFmWTreiOpc.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmWTreiOpc.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmWTreiOpc.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  MostraEdicao;
  //
  UMyMod.AbreQuery(QrEmailAut, DMod.MyDBn);
  UMyMod.AbreQuery(QrEmailPar, DMod.MyDBn);
  UMyMod.AbreQuery(QrEmailTrei, DMod.MyDBn);
  UMyMod.AbreQuery(QrTermoAut, DMod.MyDBn);
  UMyMod.AbreQuery(QrTermoPar, DMod.MyDBn);
end;

procedure TFmWTreiOpc.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
    [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmWTreiOpc.MostraEdicao;
begin
  UMyMod.AbreQuery(QrWTreiOpc, DMod.MyDBn);
  //
  EdHorasCfgAut.ValueVariant := QrWTreiOpc.FieldByName('HorasCfgAut').AsInteger;
  EdHorasCfgPar.ValueVariant := QrWTreiOpc.FieldByName('HorasCfgPar').AsInteger;
  //
  EdEmailAut.ValueVariant  := QrWTreiOpc.FieldByName('EmailAut').AsInteger;
  CBEmailAut.KeyValue      := QrWTreiOpc.FieldByName('EmailAut').AsInteger;
  EdEmailPar.ValueVariant  := QrWTreiOpc.FieldByName('EmailPar').AsInteger;
  CBEmailPar.KeyValue      := QrWTreiOpc.FieldByName('EmailPar').AsInteger;
  EdEmailTrei.ValueVariant := QrWTreiOpc.FieldByName('EmailTrei').AsInteger;
  CBEmailTrei.KeyValue     := QrWTreiOpc.FieldByName('EmailTrei').AsInteger;
  //
  EdTermoAut.ValueVariant := QrWTreiOpc.FieldByName('TermoAut').AsInteger;
  CBTermoAut.KeyValue     := QrWTreiOpc.FieldByName('TermoAut').AsInteger;
  EdTermoPar.ValueVariant := QrWTreiOpc.FieldByName('TermoPar').AsInteger;
  CBTermoPar.KeyValue     := QrWTreiOpc.FieldByName('TermoPar').AsInteger;
end;

end.
