unit WOrdSerAssGru;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2,
  ResIntStrings, UnMLAGeral, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, dmkGeral,
  dmkPermissoes, dmkEdit, dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup,
  unDmkProcFunc, dmkCheckBox, DmkDAC_PF, UnDmkEnums;

type
  TFmWOrdSerAssGru = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    BtExclui: TBitBtn;
    BtAltera: TBitBtn;
    BtInclui: TBitBtn;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    Label9: TLabel;
    EdNome: TdmkEdit;
    Label1: TLabel;
    DBEdCodigo: TdmkDBEdit;
    Label2: TLabel;
    DBEdNome: TdmkDBEdit;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    QrWOSAssGru: TmySQLQuery;
    QrWOSAssGruCodigo: TIntegerField;
    QrWOSAssGruNome: TWideStringField;
    QrWOSAssGruLk: TIntegerField;
    QrWOSAssGruDataCad: TDateField;
    QrWOSAssGruDataAlt: TDateField;
    QrWOSAssGruUserCad: TIntegerField;
    QrWOSAssGruUserAlt: TIntegerField;
    QrWOSAssGruAlterWeb: TSmallintField;
    QrWOSAssGruAtivo: TSmallintField;
    DsWOSAssGru: TDataSource;
    CbAtivo: TdmkCheckBox;
    DBCheckBox1: TDBCheckBox;
    QrLoc: TmySQLQuery;
    ImgWEB: TdmkImage;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrCadastro_SimplesAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrCadastro_SimplesBeforeOpen(DataSet: TDataSet);
    procedure BtIncluiClick(Sender: TObject);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure QrWOSAssGruAfterOpen(DataSet: TDataSet);
    procedure QrWOSAssGruBeforeOpen(DataSet: TDataSet);
    procedure SbImprimeClick(Sender: TObject);
    procedure BtExcluiClick(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure Va(Para: TVaiPara);
  public
    { Public declarations }
    procedure LocCod(Atual, Codigo: Integer);
  end;

var
  FmWOrdSerAssGru: TFmWOrdSerAssGru;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, UnDmkWeb;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmWOrdSerAssGru.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmWOrdSerAssGru.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrWOSAssGruCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmWOrdSerAssGru.DefParams;
begin
  VAR_GOTOTABELA := 'wosassgru';
  VAR_GOTOMYSQLTABLE := QrWOSAssGru;
  VAR_GOTONEG := gotoPos;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDBn;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT *');
  VAR_SQLx.Add('FROM wosassgru');
  VAR_SQLx.Add('WHERE Codigo > 0');
  //
  VAR_SQL1.Add('AND Codigo=:P0');
  //
  //VAR_SQL2.Add('AND CodUsu=:P0');
  //
  VAR_SQLa.Add('AND Nome Like :P0');
  //
end;

procedure TFmWOrdSerAssGru.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmWOrdSerAssGru.QueryPrincipalAfterOpen;
begin
end;

procedure TFmWOrdSerAssGru.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmWOrdSerAssGru.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmWOrdSerAssGru.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmWOrdSerAssGru.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmWOrdSerAssGru.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmWOrdSerAssGru.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmWOrdSerAssGru.BtAlteraClick(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrWOSAssGru, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'wosassgru');
end;

procedure TFmWOrdSerAssGru.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrWOSAssGruCodigo.Value;
  Close;
end;

procedure TFmWOrdSerAssGru.BtConfirmaClick(Sender: TObject);
var
  Codigo: Integer;
  Nome: String;
begin
  Nome := EdNome.ValueVariant;
  //
  if MyObjects.FIC(Length(Nome) = 0, EdNome, 'Defina um nome!') then Exit;
  //
  if ImgTipo.SQLType = stIns then
    Codigo := UMyMod.BuscaNovoCodigo_Int(Dmod.QrUpdN, 'wosassgru', 'Codigo',
      [], [], stIns, 0, siPositivo, nil)
  else
    Codigo := QrWOSAssGruCodigo.Value;
  //
  if UMyMod.ExecSQLInsUpdPanel(ImgTipo.SQLType, Self, PnEdita,
    'wosassgru', Codigo, Dmod.QrUpdN, [PnEdita], [PnDados], ImgTipo, True) then
  begin
    LocCod(Codigo, Codigo);
  end;
end;

procedure TFmWOrdSerAssGru.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo          := EdCodigo.ValueVariant;
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  //
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDBn, 'wosassgru', 'Codigo');
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmWOrdSerAssGru.BtExcluiClick(Sender: TObject);
var
  Codigo: Integer;
begin
  if (QrWOSAssGru.State <> dsInactive) and (QrWOSAssGru.RecordCount > 0) then
  begin
    Codigo := QrWOSAssGruCodigo.Value;
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrLoc, Dmod.MyDBn, [
    'SELECT * ',
    'FROM wordserass ',
    'WHERE Grupo=' + Geral.FF0(Codigo),
    '']);
    if QrLoc.RecordCount > 0 then
    begin
      Geral.MB_Aviso('Este grupo j� foi utilizado para algum assunto e por isso n�o poder� ser exclu�-do!');
      Exit;
    end;
    UMyMod.ExcluiRegistroInt1('Confirma a exclus�o deste item?',
      'wosassgru', 'Codigo', Codigo, DMod.MyDBn);
    //
    QrWOSAssGru.Close;
    QrWOSAssGru.Open;
    //
    Va(vpLast);
  end;
end;

procedure TFmWOrdSerAssGru.BtIncluiClick(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrWOSAssGru, [PnDados],
    [PnEdita], EdNome, ImgTipo, 'wosassgru');
end;

procedure TFmWOrdSerAssGru.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  GBEdita.Align   := alClient;
  GBDados.Align   := alClient;
  CriaOForm;
end;

procedure TFmWOrdSerAssGru.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrWOSAssGruCodigo.Value, LaRegistro.Caption);
end;

procedure TFmWOrdSerAssGru.SbImprimeClick(Sender: TObject);
begin
  Geral.MB_Info('Impress�o indispon�vel para esta janela!');
end;

procedure TFmWOrdSerAssGru.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmWOrdSerAssGru.SbNovoClick(Sender: TObject);
begin
  Geral.MB_Info('Tipo de pesquisa indispon�vel para esta janela!');
end;

procedure TFmWOrdSerAssGru.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmWOrdSerAssGru.QrCadastro_SimplesAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmWOrdSerAssGru.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  DmkWeb.CarregaImagemFormWEB(ImgWEB);
end;

procedure TFmWOrdSerAssGru.SbQueryClick(Sender: TObject);
begin
  LocCod(QrWOSAssGruCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'wosassgru', Dmod.MyDBn, CO_VAZIO));
end;

procedure TFmWOrdSerAssGru.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
    [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmWOrdSerAssGru.QrCadastro_SimplesBeforeOpen(DataSet: TDataSet);
begin
  QrWOSAssGruCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmWOrdSerAssGru.QrWOSAssGruAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmWOrdSerAssGru.QrWOSAssGruBeforeOpen(DataSet: TDataSet);
begin
  QrWOSAssGruCodigo.DisplayFormat := FFormatFloat;
end;

end.
