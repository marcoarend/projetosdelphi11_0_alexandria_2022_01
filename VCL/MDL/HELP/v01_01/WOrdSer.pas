unit WOrdSer;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, dmkDBLookupComboBox, dmkEdit, dmkEditCB,
  Mask, dmkDBGrid, Menus, dmkPermissoes, dmkValUsu, frxClass, mySQLDbTables,
  Variants, dmkCompoStore, frxDBSet, DmkDAC_PF, UnDmkProcFunc,
  OleCtrls, SHDocVw, Tabs, dmkDBEdit, UnDmkEnums, dmkEditDateTimePicker,
  frxDock, frxCtrls, (*AdvObj, BaseGrid, AdvGrid,*) UnDmkImg,
  UnProjGroup_Consts;

type
  THackDBGrid = class(TDBGrid);
  TFmWOrdSer = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    PnMenu: TPanel;
    BtInclui: TBitBtn;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbReabre: TBitBtn;
    BtDesfazOrdenacao: TBitBtn;
    BtBaseCon: TBitBtn;
    BtHisAlt: TBitBtn;
    BtEntidades: TBitBtn;
    BtDesconectar: TBitBtn;
    BtAltera: TBitBtn;
    BtExclui: TBitBtn;
    BtPesquisa: TBitBtn;
    BitBtn4: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    PMImprime: TPopupMenu;
    Atual1: TMenuItem;
    Pesquisados2: TMenuItem;
    Comcomentrios2: TMenuItem;
    Semcomentrios2: TMenuItem;
    Selecionados1: TMenuItem;
    Comcomentrios3: TMenuItem;
    Semcomentrios3: TMenuItem;
    N1: TMenuItem;
    Estatsticas1: TMenuItem;
    Timer1: TTimer;
    TrayIcon1: TTrayIcon;
    Timer2: TTimer;
    SaveFile: TSaveDialog;
    OpenDialog1: TOpenDialog;
    dmkPermissoes1: TdmkPermissoes;
    frxOSR_ORSER_001_001: TfrxReport;
    frxOSR_ORSER_001_002: TfrxReport;
    ImgWEB: TdmkImage;
    CSTabSheetChamou: TdmkCompoStore;
    QrWOrdSer: TmySQLQuery;
    QrWOrdSerCodigo: TIntegerField;
    QrWOrdSerCliente: TIntegerField;
    QrWOrdSerStatus: TIntegerField;
    QrWOrdSerAbertura: TDateTimeField;
    QrWOrdSerEncerr: TDateTimeField;
    QrWOrdSerLk: TIntegerField;
    QrWOrdSerDataCad: TDateField;
    QrWOrdSerDataAlt: TDateField;
    QrWOrdSerUserCad: TIntegerField;
    QrWOrdSerUserAlt: TIntegerField;
    QrWOrdSerAtivo: TSmallintField;
    QrWOrdSerAlterWeb: TSmallintField;
    QrWOrdSerFinaliz: TSmallintField;
    QrWOrdSerPrioridade: TIntegerField;
    QrWOrdSerRespons: TIntegerField;
    QrWOrdSerAplicativo: TIntegerField;
    QrWOrdSerJanela: TWideStringField;
    QrWOrdSerModo: TIntegerField;
    QrWOrdSerNOMESTA: TWideStringField;
    QrWOrdSerNOMEPRI: TWideStringField;
    QrWOrdSerNOMECLI: TWideStringField;
    QrWOrdSerNOMERESP: TWideStringField;
    QrWOrdSerNOMEMOD: TWideStringField;
    QrWOrdSerSolicit: TIntegerField;
    QrWOrdSerPersonalName: TWideStringField;
    QrWOrdSerWOrdSerEnc: TIntegerField;
    QrWOrdSerCadWeb: TSmallintField;
    QrWOrdSerAltWeb: TSmallintField;
    QrWOrdSerDescri: TWideMemoField;
    QrWOrdSerAssunto: TIntegerField;
    QrWOrdSerSTATUS_TXT: TWideStringField;
    QrWOrdSerENCERROU_TXT: TWideStringField;
    QrWOrdSerNOMESOL: TWideMemoField;
    QrWOrdSerEnvMailAbe: TSmallintField;
    QrWOrdSerEnvMailEnc: TSmallintField;
    QrWOrdSerNOMEAPLIC: TWideStringField;
    QrWOrdSerJanelaRel: TWideStringField;
    QrWOrdSerCompoRel: TWideStringField;
    QrWOrdSerAssuntoTXT: TWideStringField;
    QrWOrdSerABERTURA_TXT: TWideStringField;
    DsWOrdSer: TDataSource;
    frxDsWOrdSer: TfrxDBDataset;
    QrWOrdSerCom: TmySQLQuery;
    QrWOrdSerComCodigo: TIntegerField;
    QrWOrdSerComControle: TIntegerField;
    QrWOrdSerComNome: TWideStringField;
    QrWOrdSerComDataHora: TDateTimeField;
    QrWOrdSerComLk: TIntegerField;
    QrWOrdSerComDataCad: TDateField;
    QrWOrdSerComDataAlt: TDateField;
    QrWOrdSerComUserCad: TIntegerField;
    QrWOrdSerComUserAlt: TIntegerField;
    QrWOrdSerComAlterWeb: TSmallintField;
    QrWOrdSerComAtivo: TSmallintField;
    QrWOrdSerComEntidade: TIntegerField;
    QrWOrdSerComPersonalName: TWideStringField;
    QrWOrdSerComComRes: TSmallintField;
    QrWOrdSerComCadWeb: TSmallintField;
    QrWOrdSerComAltWeb: TSmallintField;
    QrWOrdSerComEnvMail: TSmallintField;
    DsWOrdSerCom: TDataSource;
    frxDsWOrdSerCom: TfrxDBDataset;
    QrWOrdSerArq: TmySQLQuery;
    QrWOrdSerArqControle: TIntegerField;
    QrWOrdSerArqNome: TWideStringField;
    QrWOrdSerArqFTPConfig: TIntegerField;
    QrWOrdSerArqCaminho: TWideStringField;
    QrWOrdSerArqArquivo: TIntegerField;
    QrWOrdSerArqOrigem: TIntegerField;
    QrWOrdSerArqARQNOME: TWideStringField;
    DsWOrdSerArq: TDataSource;
    QrOrdSerPgt: TmySQLQuery;
    QrOrdSerPgtSEQ: TIntegerField;
    QrOrdSerPgtData: TDateField;
    QrOrdSerPgtVencimento: TDateField;
    QrOrdSerPgtBanco: TIntegerField;
    QrOrdSerPgtFatID: TIntegerField;
    QrOrdSerPgtContaCorrente: TWideStringField;
    QrOrdSerPgtDocumento: TFloatField;
    QrOrdSerPgtDescricao: TWideStringField;
    QrOrdSerPgtFatParcela: TIntegerField;
    QrOrdSerPgtNOMECARTEIRA2: TWideStringField;
    QrOrdSerPgtBanco1: TIntegerField;
    QrOrdSerPgtAgencia1: TIntegerField;
    QrOrdSerPgtConta1: TWideStringField;
    QrOrdSerPgtTipoDoc: TSmallintField;
    QrOrdSerPgtControle: TIntegerField;
    QrOrdSerPgtNOMEFORNECEI: TWideStringField;
    QrOrdSerPgtCARTEIRATIPO: TIntegerField;
    QrOrdSerPgtFatNum: TFloatField;
    QrOrdSerPgtNOMECARTEIRA: TWideStringField;
    QrOrdSerPgtCredito: TFloatField;
    QrOrdSerPgtAgencia: TIntegerField;
    QrOrdSerPgtTipo: TSmallintField;
    QrOrdSerPgtCarteira: TIntegerField;
    QrOrdSerPgtSub: TSmallintField;
    DsOrdSerPgt: TDataSource;
    QrWOrdSerImp: TmySQLQuery;
    QrWOrdSerImpCodigo: TIntegerField;
    QrWOrdSerImpUserImp: TIntegerField;
    QrWOrdSerImpAbertura: TDateTimeField;
    QrWOrdSerImpEncerramento: TWideStringField;
    QrWOrdSerImpWOrdSerEnc: TWideMemoField;
    QrWOrdSerImpJanela: TWideStringField;
    QrWOrdSerImpJanelaRel: TWideStringField;
    QrWOrdSerImpCompoRel: TWideStringField;
    QrWOrdSerImpControle: TIntegerField;
    QrWOrdSerImpNomeCom: TWideStringField;
    QrWOrdSerImpEntidade: TWideStringField;
    QrWOrdSerImpDataHoraCom: TDateTimeField;
    frxDsWOrdSerImp: TfrxDBDataset;
    QrWOrdSerSistema: TWideStringField;
    QrWOrdSerImpacto: TWideStringField;
    QrWOrdSerVersaoApp: TWideStringField;
    QrWOrdSerGrupo: TIntegerField;
    QrWOrdSerComDataHora_TXT: TWideStringField;
    QrWOrdSerNOMEGRUPO: TWideStringField;
    QrWOrdSerImpVersaoApp: TWideStringField;
    BtOrdena: TBitBtn;
    QrWOrdSerDATAHORAHIS: TDateTimeField;
    QrWOrdSerImpNOMECLI: TWideStringField;
    QrWOrdSerImpPersonalName: TWideStringField;
    QrWOrdSerImpAssuntoTXT: TWideStringField;
    QrWOrdSerImpNOMEMOD: TWideStringField;
    QrWOrdSerImpNOMEPRI: TWideStringField;
    QrWOrdSerImpNOMESTA: TWideStringField;
    QrWOrdSerImpSTATUS_TXT: TWideStringField;
    QrWOrdSerImpNOMERESP: TWideStringField;
    QrWOrdSerImpDescri: TWideMemoField;
    QrWOrdSerImpNOMEAPLIC: TWideStringField;
    QrWOrdSerComPara_TXT: TWideStringField;
    QrWOrdSerComDataHoraLido_TXT: TWideStringField;
    PMWOrdSer: TPopupMenu;
    Visualizarsolicitao1: TMenuItem;
    AlteraStatusdasolicitaoatual1: TMenuItem;
    Alteraresponsveldasolicitaoatual1: TMenuItem;
    QrWOrdSerHis: TmySQLQuery;
    DsWOrdSerHis: TDataSource;
    QrWOrdSerHisRESPONS: TWideStringField;
    QrWOrdSerHisSTATUS_TXT: TWideStringField;
    QrWOrdSerHisCodigo: TIntegerField;
    QrWOrdSerHisControle: TIntegerField;
    QrWOrdSerHisDATAHORAHIS: TDateTimeField;
    N5: TMenuItem;
    QrWOrdSerHisMotivReopen: TWideStringField;
    QrWOrdSerHisTarefa: TIntegerField;
    QrWOrdSerArqArqTam_MB: TFloatField;
    BtWOrdSerGru: TBitBtn;
    Alteraprioridadedasolicitaoatual1: TMenuItem;
    Alterarassuntodasolicitaoatual1: TMenuItem;
    PMAltera: TPopupMenu;
    Alteraototal1: TMenuItem;
    N3: TMenuItem;
    Alterarstatus1: TMenuItem;
    Alteraprioridade1: TMenuItem;
    Alterarresponsvel1: TMenuItem;
    Alterarassunto1: TMenuItem;
    QrWOrdSerRem: TmySQLQuery;
    DsWOrdSerRem: TDataSource;
    QrWOrdSerRemControle: TIntegerField;
    QrWOrdSerRemSessionCode: TWideStringField;
    QrWOrdSerRemStartDate_TZ_TXT: TWideStringField;
    QrWOrdSerRemEndDate_TZ_TXT: TWideStringField;
    QrWOrdSerRemDeviceName: TWideStringField;
    QrWOrdSerRemDuracao: TTimeField;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    Panel6: TPanel;
    Splitter2: TSplitter;
    LaOrderBy: TLabel;
    StaticText1: TStaticText;
    DBGItens: TdmkDBGrid;
    PnDadosWEB: TPanel;
    Label18: TLabel;
    LaUsuarioWEB: TLabel;
    CBStatusWEB: TComboBox;
    Panel22: TPanel;
    Label16: TLabel;
    EdTotItens: TdmkEdit;
    CkAtualizar: TCheckBox;
    Panel7: TPanel;
    PageControl1: TPageControl;
    TabSheet3: TTabSheet;
    GridPanel1: TGridPanel;
    Panel24: TPanel;
    DBMemo2: TDBMemo;
    StaticText3: TStaticText;
    Panel15: TPanel;
    DBMemo4: TDBMemo;
    StaticText6: TStaticText;
    TabSheet4: TTabSheet;
    Splitter3: TSplitter;
    DBMemo1: TDBMemo;
    DBGComentarios: TdmkDBGrid;
    PnMenuCom: TPanel;
    Label15: TLabel;
    Panel9: TPanel;
    BtComInclui: TBitBtn;
    BtComAltera: TBitBtn;
    BtComExclui: TBitBtn;
    BtComConversa: TBitBtn;
    BtComReabre: TBitBtn;
    TabSheet5: TTabSheet;
    Panel16: TPanel;
    Panel17: TPanel;
    Panel18: TPanel;
    BtAneBaixar: TBitBtn;
    BtAneExclui: TBitBtn;
    BtAneInclui: TBitBtn;
    dmkDBGrid1: TdmkDBGrid;
    TabSheet6: TTabSheet;
    Panel11: TPanel;
    Panel23: TPanel;
    BtFatInclui: TBitBtn;
    BtFatExclui: TBitBtn;
    DBGPgto: TDBGrid;
    TabSheet2: TTabSheet;
    Splitter5: TSplitter;
    dmkDBGrid2: TdmkDBGrid;
    DBMeMotivReopen: TDBMemo;
    TabSheet1: TTabSheet;
    Panel1: TPanel;
    Panel8: TPanel;
    BtConexao: TBitBtn;
    BtExcluiConexao: TBitBtn;
    dmkDBGrid3: TdmkDBGrid;
    PnAviso: TPanel;
    Label9: TLabel;
    BitBtn8: TBitBtn;
    PMEntidades: TPopupMenu;
    EntidadesDadoscadastrais1: TMenuItem;
    ClientesDadosespecficosdocliente1: TMenuItem;
    ClienteDadosusurioWEB1: TMenuItem;
    N2: TMenuItem;
    ResponsvelDadosWEB1: TMenuItem;
    BtReabreConexao: TBitBtn;
    BtEncerraConexao: TBitBtn;
    PnPesquisa: TPanel;
    ScrollBox1: TScrollBox;
    RGStatus: TRadioGroup;
    GroupBox2: TGroupBox;
    CkEncIni: TCheckBox;
    TPEncIni: TDateTimePicker;
    CkEncFim: TCheckBox;
    TPEncFim: TDateTimePicker;
    GBEmissao: TGroupBox;
    CkAbeIni: TCheckBox;
    TPAbeIni: TDateTimePicker;
    CkAbeFim: TCheckBox;
    TPAbeFim: TDateTimePicker;
    Label8: TLabel;
    CkGrupo: TCheckBox;
    EdPesqGrupo: TdmkEditCB;
    CBPesqGrupo: TdmkDBLookupComboBox;
    CBAplicativo: TdmkDBLookupComboBox;
    EdAplicativo: TdmkEditCB;
    EdCodigo: TdmkEdit;
    Label14: TLabel;
    EdDescri: TdmkEdit;
    Label11: TLabel;
    Label10: TLabel;
    EdRequisitante: TdmkEditCB;
    CBRequisitante: TdmkDBLookupComboBox;
    CBRespons: TdmkDBLookupComboBox;
    EdRespons: TdmkEditCB;
    Label7: TLabel;
    Label6: TLabel;
    EdStatus: TdmkEditCB;
    CBStatus: TdmkDBLookupComboBox;
    CBPrioridade: TdmkDBLookupComboBox;
    Label5: TLabel;
    EdPrioridade: TdmkEditCB;
    EdModo: TdmkEditCB;
    CBModo: TdmkDBLookupComboBox;
    CBAssunto: TdmkDBLookupComboBox;
    EdAssunto: TdmkEditCB;
    Label2: TLabel;
    Label4: TLabel;
    Label1: TLabel;
    Label3: TLabel;
    EdCliente: TdmkEditCB;
    EdSolicit: TdmkEditCB;
    CBSolicit: TdmkDBLookupComboBox;
    CBCliente: TdmkDBLookupComboBox;
    Panel5: TPanel;
    BitBtn7: TBitBtn;
    BitBtn6: TBitBtn;
    BitBtn5: TBitBtn;
    N4: TMenuItem;
    Criartarefa1: TMenuItem;
    CkTarefa: TCheckBox;
    QrWOrdSerTAREFA: TSmallintField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
    procedure SbReabreClick(Sender: TObject);
    procedure BtDesfazOrdenacaoClick(Sender: TObject);
    procedure BtBaseConClick(Sender: TObject);
    procedure BtHisAltClick(Sender: TObject);
    procedure BtEntidadesClick(Sender: TObject);
    procedure BitBtn5Click(Sender: TObject);
    procedure BitBtn6Click(Sender: TObject);
    procedure BitBtn7Click(Sender: TObject);
    procedure BtDesconectarClick(Sender: TObject);
    procedure BitBtn4Click(Sender: TObject);
    procedure BtPesquisaClick(Sender: TObject);
    procedure BtExcluiClick(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtIncluiClick(Sender: TObject);
    procedure BtComIncluiClick(Sender: TObject);
    procedure BtComAlteraClick(Sender: TObject);
    procedure BtComExcluiClick(Sender: TObject);
    procedure BtAneIncluiClick(Sender: TObject);
    procedure BtAneExcluiClick(Sender: TObject);
    procedure BtAneBaixarClick(Sender: TObject);
    procedure BtFatIncluiClick(Sender: TObject);
    procedure BtFatExcluiClick(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure Timer2Timer(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure CBStatusWEBChange(Sender: TObject);
    procedure CkAtualizarClick(Sender: TObject);
    procedure DBGItensDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure EdClienteChange(Sender: TObject);
    procedure Atual1Click(Sender: TObject);
    procedure Comcomentrios2Click(Sender: TObject);
    procedure Semcomentrios2Click(Sender: TObject);
    procedure Comcomentrios3Click(Sender: TObject);
    procedure Semcomentrios3Click(Sender: TObject);
    procedure Estatsticas1Click(Sender: TObject);
    procedure PageControl1Change(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure QrWOrdSerAfterScroll(DataSet: TDataSet);
    procedure QrWOrdSerBeforeClose(DataSet: TDataSet);
    procedure BtComReabreClick(Sender: TObject);
    procedure BtOrdenaClick(Sender: TObject);
    procedure frxOSR_ORSER_001_002GetValue(const VarName: string;
      var Value: Variant);
    procedure BtComConversaClick(Sender: TObject);
    procedure Visualizarsolicitao1Click(Sender: TObject);
    procedure PMWOrdSerPopup(Sender: TObject);
    procedure Alteraresponsveldasolicitaoatual1Click(Sender: TObject);
    procedure QrWOrdSerHisAfterScroll(DataSet: TDataSet);
    procedure AlteraStatusdasolicitaoatual1Click(Sender: TObject);
    procedure BtWOrdSerGruClick(Sender: TObject);
    procedure Alteraprioridadedasolicitaoatual1Click(Sender: TObject);
    procedure Alterarassuntodasolicitaoatual1Click(Sender: TObject);
    procedure Alteraototal1Click(Sender: TObject);
    procedure Alterarstatus1Click(Sender: TObject);
    procedure Alterarresponsvel1Click(Sender: TObject);
    procedure Alteraprioridade1Click(Sender: TObject);
    procedure Alterarassunto1Click(Sender: TObject);
    procedure BtExcluiConexaoClick(Sender: TObject);
    procedure BtConexaoClick(Sender: TObject);
    procedure ClientesDadosespecficosdocliente1Click(Sender: TObject);
    procedure EntidadesDadoscadastrais1Click(Sender: TObject);
    procedure ClienteDadosusurioWEB1Click(Sender: TObject);
    procedure PMEntidadesPopup(Sender: TObject);
    procedure ResponsvelDadosWEB1Click(Sender: TObject);
    procedure BtReabreConexaoClick(Sender: TObject);
    procedure BtEncerraConexaoClick(Sender: TObject);
    procedure DBGItensDblClick(Sender: TObject);
    procedure Criartarefa1Click(Sender: TObject);
  private
    { Private declarations }
    FSourceCol, FSourceRow: Integer;
    FReabreGruIts: Boolean;
    FSQLWordSerImp: WideString;
    FOrderBy: String;
    procedure FechaJanela(Desconecta: Boolean);
    procedure LimpaCamposPesq(LimpaAtendente: Boolean = False);
    procedure ReopenQuerys();
    procedure ConfiguraPnDadosWeb();
    procedure ImprimePesquisados(MostraComent, Selecionados: Boolean);
    procedure ReopenWOrdSerArq(Controle: Integer);
    procedure ReopenOrdSerPgt();
    procedure AlteraRespons();
    procedure AlteraStatus();
    procedure AlteraAssunto();
    procedure AlteraPrior();
    procedure AlteraSolicit(ApenasVisualiza: Boolean);
    procedure ReopenWOrdSerHis();
    procedure ReopenWOrdSerRem();
  public
    { Public declarations }
    FModuleWOrdSer: TDataModule;
    FImpComen: Boolean;
    FDtEncer, FDtMorto: TDateTime;
    FTabLctA, FTabLctB, FTabLctD: String;
    FMaxHistorico: Integer;
    procedure ReopenWOrdSer(Codigo, Respons: Integer; NaoReabre: Boolean = False);
    procedure ReopenWOrdSerCom(Controle: Integer);
    function  GeraImpressaoWOrdSerPesq(UserImp: Integer; Codigos: String): Boolean;
    procedure ReopenWOrdSerImp(Codigo, UserImp: Integer);
  end;

  var
    FmWOrdSer: TFmWOrdSer;
  const
    MSG_AVISO_ENCERR = 'A��o abortada!' + sLineBreak + 'Prov�vel motivo: Item selecionado j� foi encerrado.';

implementation

uses UnMyObjects, Module, ModWOrdSer, ModuleGeral, WSuporte, Principal,
  {$IfnDef SVers}UnitVersoes_Jan, {$EndIf}
  {$IfnDef SWUsr}UnWUsersJan, {$EndIf}
  UnFinanceiro, MyDBCheck, UnDmkWeb, UMySQLModule, UnInternalConsts3, UnPagtos,
  UnMLAGeral, WOrdSerImp, MyGlyfs, MyListas, UnFTP, UnReordena, UnGrl_Vars,
  UnitWOrdSerJan, UnitWOrdSer;

{$R *.DFM}

procedure TFmWOrdSer.Alteraresponsveldasolicitaoatual1Click(Sender: TObject);
begin
  AlteraRespons();
end;

procedure TFmWOrdSer.Alterarresponsvel1Click(Sender: TObject);
begin
  AlteraRespons();
end;

procedure TFmWOrdSer.Alterarstatus1Click(Sender: TObject);
begin
  AlteraStatus();
end;

procedure TFmWOrdSer.AlteraPrior();
var
  Codigo: Integer;
begin
  if not DmkWeb.ConectarUsuarioWEB(True) then Exit;
  //
  if (QrWOrdSer.State <> dsInactive) and (QrWOrdSer.RecordCount > 0) then
  begin
    if QrWOrdSerFinaliz.Value <> 0 then
      if not DBCheck.LiberaPelaSenhaBoss then Exit;
    //
    Codigo := QrWOrdSerCodigo.Value;
    //
    if DModWOrdSer.AlteraPrioridade(Codigo) then
      ReopenWOrdSer(Codigo, 0);
  end;
end;

procedure TFmWOrdSer.Alteraprioridade1Click(Sender: TObject);
begin
  AlteraPrior;
end;

procedure TFmWOrdSer.Alteraprioridadedasolicitaoatual1Click(Sender: TObject);
begin
  AlteraPrior;
end;

procedure TFmWOrdSer.Alterarassunto1Click(Sender: TObject);
begin
  AlteraAssunto();
end;

procedure TFmWOrdSer.Alterarassuntodasolicitaoatual1Click(Sender: TObject);
begin
  AlteraAssunto();
end;

procedure TFmWOrdSer.AlteraRespons();
var
  Codigo: Integer;
begin
  if not DmkWeb.ConectarUsuarioWEB(True) then Exit;
  //
  if(QrWOrdSer.State <> dsInactive) and
    (QrWOrdSer.RecordCount > 0) then
  begin
    Screen.Cursor := crHourGlass;
    try
      if QrWOrdSerFinaliz.Value <> 0 then
        if not DBCheck.LiberaPelaSenhaBoss then Exit;
      //
      Codigo := QrWOrdSerCodigo.Value;
      //
      if DModWOrdSer.AlteraResponsavel(Codigo, QrWOrdSerAplicativo.Value,
        QrWOrdSerSolicit.Value, QrWOrdSerCliente.Value, QrWOrdSerAssunto.Value)
      then
        ReopenWOrdSer(Codigo, 0);
    finally
      Screen.Cursor := crDefault;
    end;
  end;
end;

procedure TFmWOrdSer.AlteraSolicit(ApenasVisualiza: Boolean);
var
  Codigo: Integer;
begin
  if not DmkWeb.ConectarUsuarioWEB(True) then Exit;
  //
  if ApenasVisualiza then
    DModWOrdSer.MostraWOrdSerIts(stUpd, QrWOrdSer, False)
  else
  begin
    if not DBCheck.LiberaPelaSenhaBoss then Exit;
    //
    if(QrWOrdSer.State <> dsInactive) and
      (QrWOrdSer.RecordCount > 0) and
      (QrWOrdSerFinaliz.Value <> 1) then
    begin
      Codigo := QrWOrdSerCodigo.Value;
      //
      DModWOrdSer.MostraWOrdSerIts(stUpd, QrWOrdSer, True);
      ReopenWOrdSer(Codigo, 0);
    end else
      Geral.MensagemBox('Exclus�o abortada!' + sLineBreak +
        'Prov�vel motivo: O item selecionado possui iten(s) atrelado(s) a ele. ' +
        sLineBreak + 'Ou j� foi encerrado!', 'Aviso', MB_OK+MB_ICONWARNING);
  end;
end;

procedure TFmWOrdSer.AlteraStatus();
var
  Codigo: Integer;
begin
  if not DmkWeb.ConectarUsuarioWEB(True) then Exit;
  //
  if(QrWOrdSer.State <> dsInactive) and
    (QrWOrdSer.RecordCount > 0) then
  begin
    if QrWOrdSerFinaliz.Value <> 0 then
      if not DBCheck.LiberaPelaSenhaBoss then Exit;
    //
    Codigo := QrWOrdSerCodigo.Value;
    //
    if DModWOrdSer.AlteraStatus(QrWOrdSerCodigo.Value, nil, nil) then
      ReopenWOrdSer(QrWOrdSerCodigo.Value, 0);
  end;
end;

procedure TFmWOrdSer.AlteraAssunto();
var
  Codigo: Integer;
begin
  if not DmkWeb.ConectarUsuarioWEB(True) then Exit;
  //
  if(QrWOrdSer.State <> dsInactive) and
    (QrWOrdSer.RecordCount > 0) then
  begin
    if QrWOrdSerFinaliz.Value <> 0 then
      if not DBCheck.LiberaPelaSenhaBoss then Exit;
    //
    Codigo := QrWOrdSerCodigo.Value;
    //
    if DModWOrdSer.AlteraAssunto(QrWOrdSerCodigo.Value, nil, nil) then
      ReopenWOrdSer(QrWOrdSerCodigo.Value, 0);
  end;
end;

procedure TFmWOrdSer.Alteraototal1Click(Sender: TObject);
begin
  AlteraSolicit(False);
end;

procedure TFmWOrdSer.AlteraStatusdasolicitaoatual1Click(Sender: TObject);
begin
  AlteraStatus();
end;

procedure TFmWOrdSer.Atual1Click(Sender: TObject);
var
  Codigo: Integer;
begin
  if (QrWOrdSer.State <> dsInactive) and (QrWOrdSer.RecordCount > 0) then
  begin
    Codigo := QrWOrdSerCodigo.Value;
    //
    EdCodigo.ValueVariant := Codigo;
    //
    MyObjects.frxDefineDataSets(frxOSR_ORSER_001_001, [
      DModWOrdSer.frxDsCli,
      DModG.frxDsEndereco,
      frxDsWOrdSer
      ]);
    ReopenWOrdSer(Codigo, 0, False);
    DModG.ReopenEndereco(Geral.IMV(VAR_LIB_EMPRESAS));
    DModWOrdSer.ReopenCli(QrWOrdSerCliente.Value);
    //
    MyObjects.frxMostra(frxOSR_ORSER_001_001, 'Ordem de Servi�o')
  end;
end;

procedure TFmWOrdSer.BtOrdenaClick(Sender: TObject);
var
  CamposTxt_Sel, CamposFields_Sel, CamposTxt_Default, CamposFields_Default: String;
begin
  if (QrWOrdSer.State <> dsInactive) and (QrWOrdSer.RecordCount > 0) and (VAR_WEB_USR_ID <> 0) then
  begin
    //Campos padr�o
    CamposTxt_Default    := DModWOrdSer.QrWOrdSerOpc.FieldByName('Ordem_CamposTxt').AsString;
    CamposFields_Default := DModWOrdSer.QrWOrdSerOpc.FieldByName('Ordem_CamposFields').AsString;
    //
    //Campos salvos
    CamposTxt_Sel    := DModWOrdSer.QrWOSOpcUsu.FieldByName('Ordem_CamposTxt').AsString;
    CamposFields_Sel := DModWOrdSer.QrWOSOpcUsu.FieldByName('Ordem_CamposFields').AsString;
    //
    if DModWOrdSer.ConfiguraOrdemCampos(CamposTxt_Default, CamposFields_Default,
      CamposTxt_Sel, CamposFields_Sel)
    then
      DModWOrdSer.AtualizaConfiguracoesUsuario(VAR_WEB_USR_ID, CamposFields_Sel,
        CamposTxt_Sel);
    //
    DModWOrdSer.ReopenWOSOpcUsu(VAR_WEB_USR_ID, istTodos);
    ReopenWOrdSer(QrWOrdSerCodigo.Value, 0);
  end;
end;

procedure TFmWOrdSer.BtFatExcluiClick(Sender: TObject);
begin
{
  if (QrWOrdSer.State <> dsInactive) and
    (QrWOrdSer.RecordCount > 0) and
    (QrOrdSerPgt.State <> dsInactive) and
    (QrOrdSerPgt.RecordCount > 0) and
    (QrWOrdSerFinaliz.Value = 0) then
  begin
    UFinanceiro.ExcluiLct_Unico(FTabLctA, Dmod.MyDB,
      QrOrdSerPgtData.Value, QrOrdSerPgtTipo.Value,
      QrOrdSerPgtCarteira.Value, QrOrdSerPgtControle.Value,
      QrOrdSerPgtSub.Value, UFinanceiro.MotivDel_ValidaCodigo(311), True);
    ReopenOrdSerPgt;
  end else
    Geral.MB_Aviso(MSG_AVISO_ENCERR);
}
end;

procedure TFmWOrdSer.BtAneIncluiClick(Sender: TObject);
var
  Codigo, Controle, FTPConfig, FTPWebDir, ArqWeb: Integer;
  Arquivo, Caminho, NomeArq, Msg: String;
  Arquivos: TStringList;
  SalvouAnexo: Boolean;
begin
  if (QrWOrdSer.State <> dsInactive) and
    (QrWOrdSer.RecordCount > 0) then
  begin
    if QrWOrdSerFinaliz.Value <> 0 then
      if not DBCheck.LiberaPelaSenhaBoss then Exit;
    //
    if OpenDialog1.Execute then
    begin
      DModWOrdSer.ReopenWOrdSerOpc;
      //
      FTPConfig := DModWOrdSer.QrWOrdSerOpc.FieldByName('FTPConfig').AsInteger;
      FTPWebDir := DModWOrdSer.QrWOrdSerOpc.FieldByName('DirFTPPad').AsInteger;
      Caminho   := DModWOrdSer.QrWOrdSerOpc.FieldByName('Caminho').AsString;
      Arquivo   := OpenDialog1.FileName;
      NomeArq   := FormatDateTime('yymmddhhnnss', Now);
      //
      try
        Arquivos := TStringList.Create;
        Arquivos.Add(Arquivo);
        //
        ArqWeb := UFTP.UploadFTP_DB(FmPrincipal.PageControl1,
                    FmPrincipal.AdvToolBarPagerNovo, stIns, ExtractFileName(Arquivo),
                    NomeArq, '', '', 0, CO_DMKID_APP, FTPConfig, 1, FTPWebDir,
                    0, 0, dtWOS, 0, 0, Arquivos, False, Dmod.QrUpdN, Dmod.QrAuxN,
                    Dmod.MyDBn, Caminho, False);
      finally
        Arquivos.Free;
      end;
      if ArqWeb <> 0 then
      begin
        Codigo   := QrWOrdSerCodigo.Value;
        Controle := DmkWeb.InsereWOrdSerArq(31, Codigo, ArqWeb, Msg);
        //
        if Controle > 0 then
        begin
          SalvouAnexo := True;
          ReopenWOrdSerArq(Controle);
        end else
          Geral.MB_Aviso('Falha ao inserir registro no banco de dados!');
      end else
        Geral.MB_Aviso('Falha ao realizar upload do arquivo!');
      if not SalvouAnexo then
        Geral.MB_Aviso('Falha ao salvar anexo!');
    end;
  end else
    Geral.MB_Aviso(MSG_AVISO_ENCERR);
end;

procedure TFmWOrdSer.BtAneExcluiClick(Sender: TObject);
var
  FTPConfig, ArqWeb, Controle: Integer;
  Caminho, Arquivo: String;
  Origem: TOrigFTP;
begin
  if (QrWOrdSerArq.State <> dsInactive) and (QrWOrdSerArq.RecordCount > 0) then
  begin
    if not DBCheck.LiberaPelaSenhaBoss then Exit;
    //
    if Geral.MB_Pergunta('Confirma a exclus�o do arquivo atual?' + sLineBreak +
      'ATEN��O: O arquivo tamb�m ser� removido do servidor!') = ID_YES then
    begin
      Controle  := QrWOrdSerArqControle.Value;
      FTPConfig := QrWOrdSerArqFTPConfig.Value;
      ArqWeb    := QrWOrdSerArqArquivo.Value;
      Caminho   := QrWOrdSerArqCaminho.Value;
      Arquivo   := QrWOrdSerArqARQNOME.Value;
      Origem    := UFTP.ConverteOrigem(QrWOrdSerArqOrigem.Value);
      //
      UFTP.ReopenFTPConfig(FTPConfig, CO_DMKID_APP, DModWOrdSer.QrLoc, Dmod.MyDBn);
      //
      if UFTP.DeletaArquvivoFTP(Arquivo, Caminho,
        DModWOrdSer.QrLoc.FieldByName('Web_FTPh').AsString,
        DModWOrdSer.QrLoc.FieldByName('Web_FTPu').AsString,
        DModWOrdSer.QrLoc.FieldByName('SENHA').AsString,
        DModWOrdSer.QrLoc.FieldByName('Passivo').AsInteger,
        ArqWeb, Origem, Dmod.MyDBn, True) then
      begin
        if UMyMod.ExcluiRegistroInt1('', 'wordserarq', 'Controle', Controle,
          Dmod.MyDBn) = ID_YES then
        begin
          ReopenWOrdSerArq(0);
        end else
          Geral.MB_Aviso('Falha ao remover arquivo do banco de dados!');
      end else
        Geral.MB_Aviso('Falha ao remover arquivo do servidor!');
    end;
  end;
end;

procedure TFmWOrdSer.BtComExcluiClick(Sender: TObject);
var
  Enab, Enab2: Boolean;
  Codigo, Controle: Integer;
  Comentario, DataHora: String;
begin
  if not DmkWeb.ConectarUsuarioWEB(True) then Exit;
  //
  Enab  := (QrWOrdSer.State <> dsInactive) and (QrWOrdSer.RecordCount > 0) and
            (QrWOrdSerFinaliz.Value = 0);
  Enab2 := (QrWOrdSerCom.State <>  dsInactive) and (QrWOrdSerCom.RecordCount > 0);
  //
  if Enab and Enab2 then
  begin
    if QrWOrdSerComEntidade.Value = VAR_WEB_USR_ID then
    begin
      Codigo     := QrWOrdSerComCodigo.Value;
      Controle   := QrWOrdSerComControle.Value;
      Comentario := QrWOrdSerComNome.Value;
      DataHora   := Geral.FDT(Now, 9);
      //
      if Geral.MensagemBox('Confirma a exclus�o do coment�rio ID ' +
        Geral.FF0(Controle) + '?', 'Exclus�o de registro',
        MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
      begin
        if DModWOrdSer.ExcluiWOrdSerCom(Codigo, Controle) then
          ReopenWOrdSerCom(0)
        else
          Geral.MB_Aviso('Falha ao excluir coment�rio!');
      end;
    end else
      Geral.MB_Aviso('Voc� pode excluir apenas seus coment�rios!');
  end else
    Geral.MB_Aviso(MSG_AVISO_ENCERR);
end;

procedure TFmWOrdSer.BtComIncluiClick(Sender: TObject);
var
  Enab: Boolean;
begin
  Enab := (QrWOrdSer.State <> dsInactive) and (QrWOrdSer.RecordCount > 0) and
            (QrWOrdSerFinaliz.Value = 0);
  if Enab then
  begin
    DModWOrdSer.MostraWOrdSerCom(stIns, QrWOrdSerCodigo.Value, 0, 0,
      QrWOrdSerSolicit.Value, 0, '');
    ReopenWOrdSerCom(0);
  end else
    Geral.MB_Aviso(MSG_AVISO_ENCERR);
end;

procedure TFmWOrdSer.BtComAlteraClick(Sender: TObject);
var
  Controle: Integer;
  Enab, Enab2: Boolean;
begin
  Enab  := (QrWOrdSer.State <> dsInactive) and (QrWOrdSer.RecordCount > 0) and
             (QrWOrdSerFinaliz.Value = 0);
  Enab2 := (QrWOrdSerCom.State <> dsInactive) and (QrWOrdSerCom.RecordCount > 0);
  //
  if Enab and Enab2 then
  begin
    Controle := QrWOrdSerComControle.Value;
    //
    DModWOrdSer.MostraWOrdSerCom(stUpd, QrWOrdSerCodigo.Value,
      QrWOrdSerComControle.Value, QrWOrdSerComEntidade.Value,
      QrWOrdSerSolicit.Value, QrWOrdSerComComRes.Value, QrWOrdSerComNome.Value);
    ReopenWOrdSerCom(Controle);
  end else
    Geral.MB_Aviso(MSG_AVISO_ENCERR);
end;

procedure TFmWOrdSer.BitBtn4Click(Sender: TObject);
var
  Enab, Finaliz: Boolean;
  Codigo, Assunto, Encerrou: Integer;
  Descri: String;
  Abertura: TDateTime;
begin
  if not DmkWeb.ConectarUsuarioWEB(True) then Exit;
  //
  Enab := (QrWOrdSer.State <> dsInactive) and (QrWOrdSer.RecordCount > 0);
  //
  if Enab then
  begin
    Codigo   := QrWOrdSerCodigo.Value;
    Encerrou := QrWOrdSerFinaliz.Value;
    Assunto  := QrWOrdSerAssunto.Value;
    Descri   := QrWOrdSerDescri.Value;
    Abertura := QrWOrdSerAbertura.Value;
    Finaliz  := Geral.IntToBool(QrWOrdSerFinaliz.Value);
    //
    DModWOrdSer.MostraWOrdSerEnc(Codigo, Encerrou, Assunto, Descri, Abertura,
      Finaliz);
    //
    Codigo := UMyMod.ProximoRegistro(QrWOrdSer, 'Codigo', Codigo);
    ReopenWOrdSer(Codigo, 0);
  end;
end;

procedure TFmWOrdSer.BitBtn5Click(Sender: TObject);
begin
  if not DmkWeb.ConectarUsuarioWEB(True) then Exit;
  //
  ReopenWOrdSer(0, 0);
end;

procedure TFmWOrdSer.BitBtn6Click(Sender: TObject);
begin
  LimpaCamposPesq;
end;

procedure TFmWOrdSer.BitBtn7Click(Sender: TObject);
begin
  if PnPesquisa.Visible then
  begin
    PnPesquisa.Visible := False;
    PnMenu.Visible     := True;
  end else
  begin
    ReopenQuerys;
    PnPesquisa.Visible := True;
    PnMenu.Visible     := False;
  end;
end;

procedure TFmWOrdSer.BtFatIncluiClick(Sender: TObject);
{
var
  Codigo, Empresa, Terceiro, Cod: Integer;
  Valor: Double;
  Descricao: String;
}
begin
  Geral.MB_Aviso('N�o implementado!');
  {
  if(DModWOrdSer.QrWOrdSer.State = dsInactive) or (DModWOrdSer.QrWOrdSer.RecordCount = 0) or
    (DModWOrdSer.QrWOrdSerFinaliz.Value = 1) or (DModWOrdSer.QrWOrdSerTotal.Value = 0) then
  begin
    Geral.MB_Aviso(MSG_AVISO_ENCERR);
    Exit;
  end;
  //
  Codigo        := DModWOrdSer.QrWOrdSerCodigo.Value;
  IC3_ED_FatNum := Codigo;
  IC3_ED_NF     := 0;
  IC3_ED_Data   := DModWOrdSer.QrWOrdSerAbertura.Value;
  IC3_ED_Vencto := DModWOrdSer.QrWOrdSerAbertura.Value;
  Cod           := Codigo;
  Terceiro      := DModWOrdSer.QrWOrdSerCliente.Value;
  Valor         := DModWOrdSer.QrWOrdSerTotal.Value;
  Descricao     := 'Recebimento da ordem de servi�o n� ' + IntToStr(DModWOrdSer.QrWOrdSerCodigo.Value);
  Empresa       := DmodG.QrFiliLogCodigo.Value;
  UPagtos.Pagto(DModWOrdSer.QrOrdSerPgt, tpCred, Cod, Terceiro, VAR_FATID_0901, 0, stIns,
    'Pagamento de Produtos', Valor, VAR_USUARIO, 0, Empresa,
    mmNenhum, 0, 0, True, True, 0, 0, 0, 0, FTabLctA);
  }
end;

procedure TFmWOrdSer.BtAlteraClick(Sender: TObject);
begin
  if not DmkWeb.ConectarUsuarioWEB(True) then Exit;
  //
  MyObjects.MostraPopUpDeBotao(PMAltera, BtAltera);
end;

procedure TFmWOrdSer.BtAneBaixarClick(Sender: TObject);
var
  DirSave, Host, Usuario, Senha: String;
  Passivo: Integer;
begin
  if (QrWOrdSerArq.State <> dsInactive) and
    (QrWOrdSerArq.RecordCount > 0) then
  begin
    UFTP.ReopenFTPConfig(QrWOrdSerArqFTPConfig.Value, CO_DMKID_APP,
      DModWOrdSer.QrLoc, Dmod.MyDBn);
    //
    Host    := DModWOrdSer.QrLoc.FieldByName('Web_FTPh').AsString;
    Usuario := DModWOrdSer.QrLoc.FieldByName('Web_FTPu').AsString;
    Senha   := DModWOrdSer.QrLoc.FieldByName('SENHA').AsString;
    Passivo := DModWOrdSer.QrLoc.FieldByName('Passivo').AsInteger;

    DirSave := UFTP.DownloadFileIndy(FmPrincipal.PageControl1,
                 FmPrincipal.AdvToolBarPagerNovo, SaveFile, QrWOrdSerArqCaminho.Value,
                 QrWOrdSerArqARQNOME.Value, '', Host, Usuario, Senha, Geral.IntToBool(Passivo));
    if Length(DirSave) > 0 then
    begin
      if Geral.MensagemBox('Deseja abrir o arquivo?', 'Pergunta',
        MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
      begin
        if FileExists(DirSave) then
          Geral.AbreArquivo(DirSave, True);
      end;
    end;
  end;
end;

procedure TFmWOrdSer.BtBaseConClick(Sender: TObject);
begin
  Application.CreateForm(TFmWSuporte, FmWSuporte);
  FmWSuporte.FOrigem := 'WOrdSer';
  FmWSuporte.ShowModal;
  FmWSuporte.Destroy;
end;

procedure TFmWOrdSer.BtComConversaClick(Sender: TObject);
var
  Enab: Boolean;
  Codigo: Integer;
  Link: String;
begin
  Enab := (QrWOrdSer.State <> dsInactive) and (QrWOrdSer.RecordCount > 0) and
            (QrWOrdSerFinaliz.Value = 0);
  if Enab then
  begin
    Codigo := QrWOrdSerCodigo.Value;
    Link   := 'http://www.dermatek.net.br/dmkWOrdSerCom.php?codigo='+ Geral.FF0(Codigo) +'&token=' + VAR_WEB_IDLOGIN;
    //
    DmkWeb.MostraWebBrowser(Link, True, False, 0, 0);
    //
    PageControl1.ActivePageIndex := 0;
  end else
    Geral.MB_Aviso(MSG_AVISO_ENCERR);
end;

procedure TFmWOrdSer.BtDesconectarClick(Sender: TObject);
begin
  FechaJanela(True);
end;

procedure TFmWOrdSer.BtDesfazOrdenacaoClick(Sender: TObject);
var
  Query: TmySQLQuery;
  Controle: Integer;
begin
  if not DmkWeb.ConectarUsuarioWEB(True) then Exit;
  //
  if DBGItens.DataSource.DataSet is TmySQLQuery then
    Query := TmySQLQuery(DBGItens.DataSource.DataSet)
  else
    Query := nil;
  if Query <> nil then
  begin
    Query.SortFieldNames := '';
    if Query.State <> dsInactive then
    begin
      Controle := Query.FieldByName('Codigo').AsInteger;
      Query.Close;
      Query.Open;
      Query.Locate('Codigo', Controle, []);
    end;
  end;
end;

procedure TFmWOrdSer.BtEncerraConexaoClick(Sender: TObject);
var
  Enab, Enab2: Boolean;
  Codigo: Integer;
  Link, Sessao: String;
begin
  Enab := (QrWOrdSer.State <> dsInactive) and (QrWOrdSer.RecordCount > 0) and
            (QrWOrdSerFinaliz.Value = 0);
  if Enab then
  begin
    Enab2 := (QrWOrdSerRem.State <> dsInactive) and (QrWOrdSerRem.RecordCount > 0);
    //
    if Enab2 then
    begin
      if QrWOrdSerRemDuracao.Value = 0 then
      begin
        Codigo := QrWOrdSerCodigo.Value;
        Sessao := QrWOrdSerRemSessionCode.Value;
        Link   := 'http://www.dermatek.net.br/?page=aremoto&id='+ Geral.FF0(Codigo) +
                  '&tipo=WOS&tvsessao=' + Sessao + '&token=' + VAR_WEB_IDLOGIN;
        //
        DmkWeb.MostraWebBrowser(Link, True, False, 0, 0);
      end else
        Geral.MB_Aviso('Conex�o j� encerrada!');
    end;
  end else
    Geral.MB_Aviso(MSG_AVISO_ENCERR);
end;

procedure TFmWOrdSer.BtEntidadesClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMEntidades, BtEntidades);
end;

procedure TFmWOrdSer.BtExcluiClick(Sender: TObject);
  procedure MostraMensagem;
  begin
    Geral.MensagemBox('Exclus�o abortada!' + sLineBreak +
      'Prov�vel motivo: O item selecionado possui iten(s) atrelado(s) a ele. ' +
      sLineBreak + 'Ou j� foi encerrado!', 'Aviso', MB_OK+MB_ICONWARNING);
  end;
var
  Codigo: Integer;
  Qry: TmySQLQuery;
begin
  if not DmkWeb.ConectarUsuarioWEB(True) then Exit;
  if not DBCheck.LiberaPelaSenhaBoss then Exit;
  //
  Codigo := QrWOrdSerCodigo.Value;
  //
  if(QrWOrdSer.State <> dsInactive) and
    (QrWOrdSer.RecordCount > 0) and
    (QrWOrdSerFinaliz.Value <> 1) then
  begin
    ReopenWOrdSerCom(0);
    if QrWOrdSerCom.State <> dsInactive then //Verifica coment�rios
    begin
      if QrWOrdSerCom.RecordCount > 0 then
      begin
        MostraMensagem;
        Exit;
      end;
    end;
    ReopenWOrdSerArq(0);
    if QrWOrdSerArq.State <> dsInactive then //Verifica arquivos
    begin
      if QrWOrdSerArq.RecordCount > 0 then
      begin
        MostraMensagem;
        Exit;
      end;
    end;
    ReopenOrdSerPgt;
    if QrOrdSerPgt.State <> dsInactive then //Verifica pagamentos
    begin
      if QrOrdSerPgt.RecordCount > 0 then
      begin
        MostraMensagem;
        Exit;
      end;
    end;
    Qry := TmySQLQuery.Create(Dmod.MyDB.Owner);
    try
      UnWOrdSer.ReabreWOrdSerTar(Qry, Codigo);
      if Qry.RecordCount > 0 then
      begin
        MostraMensagem;
        Exit;
      end;
    finally
      Qry.Free;
    end;
    //
    if Geral.MensagemBox('Confirma a exclus�o do item ID n�mero ' +
      Geral.FF0(Codigo) + '?', 'Exclus�o de registro',
      MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
    begin
      Dmod.QrUpdN.SQL.Clear;
      Dmod.QrUpdN.SQL.Add('DELETE FROM wordser WHERE Codigo=:P0');
      Dmod.QrUpdN.Params[0].AsInteger := Codigo;
      Dmod.QrUpdN.ExecSQL;
      //
      Dmod.QrUpdN.SQL.Clear;
      Dmod.QrUpdN.SQL.Add('DELETE FROM wordserhis WHERE Codigo=:P0');
      Dmod.QrUpdN.Params[0].AsInteger := Codigo;
      Dmod.QrUpdN.ExecSQL;
      //
      ReopenWOrdSer(0, 0);
    end;
  end else
    MostraMensagem;
end;

procedure TFmWOrdSer.BtExcluiConexaoClick(Sender: TObject);
var
  Enab, Enab2: Boolean;
  Codigo, Controle: Integer;
  Comentario, DataHora: String;
begin
  if not DmkWeb.ConectarUsuarioWEB(True) then Exit;
  if not DBCheck.LiberaPelaSenhaBoss then Exit;
  //
  Enab  := (QrWOrdSer.State <> dsInactive) and (QrWOrdSer.RecordCount > 0) and
            (QrWOrdSerFinaliz.Value = 0);
  Enab2 := (QrWOrdSerRem.State <>  dsInactive) and (QrWOrdSerRem.RecordCount > 0);
  //
  if Enab and Enab2 then
  begin
    Controle := QrWOrdSerRemControle.Value;
    //
    if Geral.MB_Pergunta('Confirma a exclus�o do relat�rio de conex�o ID ' +
      Geral.FF0(Controle) + '?') = ID_YES then
    begin
      if UMyMod.ExcluiRegistroInt1('', 'wordserrem', 'Controle', Controle, Dmod.MyDBn) = ID_YES then
        ReopenWOrdSerRem
      else
        Geral.MB_Aviso('Falha ao excluir relat�rio de conex�o!');
    end;
  end else
    Geral.MB_Aviso(MSG_AVISO_ENCERR);
end;

procedure TFmWOrdSer.BtHisAltClick(Sender: TObject);
begin
  {$IfnDef SVers}
  UnVersoes_Jan.MostraFormWHisAlt(True, FmPrincipal.PageControl1, FmPrincipal.AdvToolBarPagerNovo);
  {$EndIf}
end;

procedure TFmWOrdSer.BtIncluiClick(Sender: TObject);
begin
  DModWOrdSer.MostraWOrdSerIts(stIns, QrWOrdSer, True, 0, 0, True);
  //
  ReopenWOrdSer(0, 0);
end;

procedure TFmWOrdSer.BtPesquisaClick(Sender: TObject);
begin
  if not DmkWeb.ConectarUsuarioWEB(True) then Exit;
  //
  if PnPesquisa.Visible then
  begin
    PnPesquisa.Visible := False;
    PnMenu.Visible     := True;
  end else
  begin
    ReopenQuerys;
    PnPesquisa.Visible := True;
    PnMenu.Visible     := False;
  end;
end;

procedure TFmWOrdSer.BtReabreConexaoClick(Sender: TObject);
begin
  if not DmkWeb.ConectarUsuarioWEB(True) then Exit;
  //
  ReopenWOrdSerRem();
end;

procedure TFmWOrdSer.BtComReabreClick(Sender: TObject);
begin
  if not DmkWeb.ConectarUsuarioWEB(True) then Exit;
  //
  ReopenWOrdSerCom(0);
end;

procedure TFmWOrdSer.BtConexaoClick(Sender: TObject);
var
  Enab: Boolean;
  Codigo: Integer;
  Link: String;
begin
  Enab := (QrWOrdSer.State <> dsInactive) and (QrWOrdSer.RecordCount > 0) and
            (QrWOrdSerFinaliz.Value = 0);
  if Enab then
  begin
    Codigo := QrWOrdSerCodigo.Value;
    Link   := 'http://www.dermatek.net.br/?page=wordservis&id='+ Geral.FF0(Codigo) +'&token=' + VAR_WEB_IDLOGIN;
    //
    DmkWeb.MostraWebBrowser(Link, True, False, 0, 0);
  end else
    Geral.MB_Aviso(MSG_AVISO_ENCERR);
end;

procedure TFmWOrdSer.BtSaidaClick(Sender: TObject);
begin
  FechaJanela(False);
end;

procedure TFmWOrdSer.BtWOrdSerGruClick(Sender: TObject);
begin
  DModWOrdSer.MostraWOrdSerGru(True, FmPrincipal.PageControl1, FmPrincipal.AdvToolBarPagerNovo);
end;

procedure TFmWOrdSer.CBStatusWEBChange(Sender: TObject);
begin
  DModWOrdSer.AtualizaStatusUsuarioWEB(VAR_WEB_USR_ID, CBStatusWEB.ItemIndex);
end;

procedure TFmWOrdSer.CkAtualizarClick(Sender: TObject);
var
  Enab: Boolean;
begin
  Enab := not CkAtualizar.Checked;
  Timer1.Enabled := Enab;
end;

procedure TFmWOrdSer.ClienteDadosusurioWEB1Click(Sender: TObject);
begin
  {$IfnDef SWUsr}
  UWUsersJan.MostraWUsers(QrWOrdSerSolicit.Value);
  {$EndIf}
end;

procedure TFmWOrdSer.ClientesDadosespecficosdocliente1Click(Sender: TObject);
begin
  if CO_DMKID_APP = 17 then //DControl
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(DModWOrdSer.QrLoc, Dmod.MyDB, [
      'SELECT Codigo ',
      'FROM clientes ',
      'WHERE Cliente=' + Geral.FF0(QrWOrdSerCliente.Value),
      '',
      '']);
    if DModWOrdSer.QrLoc.RecordCount > 0 then
      FmPrincipal.CadastrodeClientes(True, DModWOrdSer.QrLoc.FieldByName('Codigo').AsInteger, 0)
    else
      FmPrincipal.CadastrodeClientes(True, 0, 0);
  end;
end;

procedure TFmWOrdSer.Comcomentrios2Click(Sender: TObject);
begin
  ImprimePesquisados(True, False);
end;

procedure TFmWOrdSer.Comcomentrios3Click(Sender: TObject);
begin
  ImprimePesquisados(True, True);
end;

procedure TFmWOrdSer.ConfiguraPnDadosWeb;
var
  Status: Integer;
  StatTxt: String;
begin
  if VAR_WEB_CONECTADO = 100 then
  begin
    DmkWeb.ObtemStatusUsuarioWEB(VAR_WEB_USR_ID, Status, StatTxt);
    //
    LaUsuarioWEB.Caption  := 'Bem-vindo ' + DmkWeb.ObtemNomeUsuarioWEB(VAR_WEB_USR_ID);
    CBStatusWEB.ItemIndex := Status;
    //
    PnDadosWEB.Visible := True;
  end else
    PnDadosWEB.Visible := False;
end;

procedure TFmWOrdSer.Criartarefa1Click(Sender: TObject);
begin
  UnWOrdSerJan.MostraFormWOrdSerTar(QrWOrdSerCodigo.Value,
    QrWOrdSerDescri.Value, QrWOrdSerPrioridade.Value, QrWOrdSerAbertura.Value);
  ReopenWOrdSer(QrWOrdSerCodigo.Value, 0);
end;

procedure TFmWOrdSer.DBGItensDblClick(Sender: TObject);
var
  Campo: String;
begin
  if (QrWOrdSer.State <> dsInactive) and (QrWOrdSer.RecordCount > 0) then
  begin
    Campo := UpperCase(TDBGrid(Sender).Columns[THackDBGrid(Sender).Col -1].FieldName);
    //
    if UpperCase(Campo) = UpperCase('Janela') then
    begin
      Geral.MB_Aviso(QrWOrdSerJanela.Value);
    end else
    if UpperCase(Campo) = UpperCase('JanelaRel') then
    begin
      Geral.MB_Aviso(QrWOrdSerJanelaRel.Value);
    end;
  end;
end;

procedure TFmWOrdSer.DBGItensDrawColumnCell(Sender: TObject; const Rect: TRect;
  DataCol: Integer; Column: TColumn; State: TGridDrawState);
var
  Cor: TColor;
  Bold: Boolean;
begin
  if (Column.FieldName = 'ENCERROU_TXT')then
  begin
    case QrWOrdSerFinaliz.Value of
      0:
      begin
        Cor  := clRed;
        Bold := True;
      end;
      else
      begin
        Cor  := clBlack;
        Bold := False;
      end;
    end;
    with DBGItens.Canvas do
    begin
      if Bold then Font.Style := [fsBold] else Font.Style := [];
      Font.Color := Cor;
      FillRect(Rect);
      TextOut(Rect.Left+2,rect.Top+2,Column.Field.DisplayText);
    end;
  end;
end;

procedure TFmWOrdSer.EdClienteChange(Sender: TObject);
var
  Tipo, Cliente: Integer;
begin
  Tipo := VAR_WEB_USR_TIPO;
  if EdCliente.ValueVariant <> 0 then
    Cliente := EdCliente.ValueVariant
  else
    Cliente := 0;
  if Cliente <> 0 then
  begin
    DModWOrdSer.ReopenWUsers(Cliente);
    //
    if CO_DMKID_APP = 17 then //DControl
    begin
      if Tipo in [0, 1] then
        DModWOrdSer.ReopenAplicativos(Cliente);
    end;
  end else
  begin
    DModWOrdSer.ReopenWUsers(0);
    //
    if CO_DMKID_APP = 17 then //DControl
    begin
      if Tipo in [0, 1] then
        DModWOrdSer.ReopenAplicativos(0);
    end;
  end;
end;

procedure TFmWOrdSer.EntidadesDadoscadastrais1Click(Sender: TObject);
begin
  DModG.CadastroDeEntidade(QrWOrdSerCliente.Value, fmcadEntidade2,
    fmcadEntidade2, False, True, FmPrincipal.PageControl1,
    FmPrincipal.AdvToolBarPagerNovo);
end;

procedure TFmWOrdSer.Estatsticas1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmWOrdSerImp, FmWOrdSerImp, afmoNegarComAviso) then
  begin
    FmWOrdSerImp.ShowModal;
    FmWOrdSerImp.Destroy;
  end;
end;

procedure TFmWOrdSer.FechaJanela(Desconecta: Boolean);
begin
  if Desconecta then
    DmkWeb.DesconectarUsuarioWEB;
  //
  if TFmWOrdSer(Self).Owner is TApplication then
    Close
  else
  begin
    Timer1.Enabled := False;
    MyObjects.FormTDIFecha(Self, TTabSheet(CSTabSheetChamou.Component));
  end;
end;

procedure TFmWOrdSer.FormActivate(Sender: TObject);
begin
  if TFmWOrdSer(Self).Owner is TApplication then
  begin
    MyObjects.CorIniComponente();
  end;
  DmkWeb.CarregaImagemFormWEB(ImgWEB);
end;

procedure TFmWOrdSer.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  if TFmWOrdSer(Self).Owner is TApplication then
    Timer1.Enabled := False;
end;

procedure TFmWOrdSer.FormCreate(Sender: TObject);
var
  SelecionouEmpresa, HabilitaAplicCols: Boolean;
  Entidade, CliInt, MaxControle, MaxUsuario: Integer;
begin
  ImgTipo.SQLType              := stLok;
  PageControl1.ActivePageIndex := 0;
  TabSheet6.TabVisible         := False;
  PnAviso.Visible              := False;
  FReabreGruIts                := True;
  //
  Entidade := 0;
  CliInt   := 0;
  //
  SelecionouEmpresa := DModG.SelecionaEmpresa(sllLivre);
  if SelecionouEmpresa then
  begin
    Entidade := DModG.EmpresaAtual_ObtemCodigo(tecEntidade);
    CliInt   := DModG.EmpresaAtual_ObtemCodigo(tecCliInt);
  end;
  if (Entidade = 0) or (CliInt = 0) then
  begin
    Geral.MB_Aviso('Falha ao definir tabela de lan�amentos!');
    Exit;
  end;
  DModG.Def_EM_ABD(TMeuDB, Entidade, CliInt, FDtEncer, FDtMorto, FTabLctA,
    FTabLctB, FTabLctD);
  //
  DModWOrdSer.ReopenWOrdSerOpc;
  DModWOrdSer.ReopenWOSOpcUsu(VAR_WEB_USR_ID, istTodos);
  //
  Timer1.Interval     := 1000 * DModWOrdSer.QrWOrdSerOpc.FieldByName('UpdLocInte').AsInteger;
  Timer1.Enabled      := True;
  CkAtualizar.Checked := False;
  FImpComen           := False;
  //
  if DModWOrdSer.VerificaMaxHistorico(EdRespons.ValueVariant, MaxControle,
    MaxUsuario)
  then
    FMaxHistorico := MaxControle
  else
    FMaxHistorico := 0;
  //
  ConfiguraPnDadosWeb;
  LimpaCamposPesq;
  //
  PnPesquisa.Visible           := False;
  PageControl1.ActivePageIndex := 0;
  ReopenWOrdSer(0, 0);
  QrWOrdSerImp.Close;
  //
  if CO_DMKID_APP = 17 then //DControl
    HabilitaAplicCols  := True
  else
    HabilitaAplicCols  := False;
  //
  DBGItens.Columns[14].Visible := HabilitaAplicCols;
  DBGItens.Columns[15].Visible := HabilitaAplicCols;
  DBGItens.Columns[16].Visible := HabilitaAplicCols;
  DBGItens.Columns[17].Visible := HabilitaAplicCols;
  DBGItens.Columns[18].Visible := HabilitaAplicCols;
  //
  DBGItens.PopupMenu := PMWOrdSer;
  //
  TabSheet1.TabVisible := DModWOrdSer.HabilitaAcessoremoto(VAR_WEB_USR_TIPO);
  //
  //Configura bot�es atalho
  {$IfnDef SVers}
  BtHisAlt.Enabled := True;
  {$Else}
  BtHisAlt.Enabled := False;
  {$EndIf}
end;

procedure TFmWOrdSer.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
    [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmWOrdSer.FormShow(Sender: TObject);
begin
{$IfNDef cSkinRank} //Berlin
{$IfNDef cAlphaSkin} //Berlin
  FmMyGlyfs.DefineGlyfs(TForm(Sender));
{$EndIf}
{$EndIf}
{$IfDef cSkinRank} //Berlin
  if FmPrincipal.Sd1.Active then
    FmMyGlyfs.DefineGlyfsTDI(FmPrincipal.sd1, Sender);
{$EndIf}
{$IfDef cAlphaSkin} //Berlin
  if FmPrincipal.sSkinManager1.Active then
    FmMyGlyfs.DefineGlyfsTDI2(FmPrincipal.sSkinManager1, Sender);
{$EndIf}
end;

procedure TFmWOrdSer.frxOSR_ORSER_001_002GetValue(const VarName: string;
  var Value: Variant);
begin
  if VarName = 'VAR_FImpComen' then
    Value := FImpComen
  else if VarName = 'VAR_PERIODO' then
  begin
    Value := dmkPF.PeriodoImp(TPAbeIni.Date, TPAbeFim.Date, TPEncIni.Date, TPEncFim.Date,
      CkAbeIni.Checked, CkAbeFim.Checked, CkEncIni.Checked, CkEncFim.Checked,
      'Abertura:', 'Encerramento:')
  end
end;

function TFmWOrdSer.GeraImpressaoWOrdSerPesq(UserImp: Integer;
  Codigos: String): Boolean;
var
  Ordem: String;
begin
  try
    Screen.Cursor := crHourGlass;
    FOrderBy      := QrWOrdSer.SortFieldNames;
    //
    DModWOrdSer.QrLoc.Close;
    DModWOrdSer.QrLoc.Database := Dmod.MyDBn;
    DModWOrdSer.QrLoc.SQL.Clear;
    DModWOrdSer.QrLoc.SQL.Add('DELETE FROM wordserimp WHERE UserImp=' + Geral.FF0(UserImp) + '; ');
    DModWOrdSer.QrLoc.SQL.Add('INSERT INTO wordserimp (Codigo, Controle, UserImp, Abertura, Encerr, ');
    DModWOrdSer.QrLoc.SQL.Add('Encerramento, NOMECLI, PersonalName, AssuntoTXT, NOMEMOD, NOMEPRI, ');
    DModWOrdSer.QrLoc.SQL.Add('NOMESTA, NOMERESP, Descri, WOrdSerEnc, NOMEAPLIC, VersaoApp, Janela, ');
    DModWOrdSer.QrLoc.SQL.Add('JanelaRel, CompoRel, NomeCom, Entidade, DataHoraCom, DataHoraHis) ');
    DModWOrdSer.QrLoc.SQL.Add('SELECT ');
    DModWOrdSer.QrLoc.SQL.Add('wor.Codigo, ');
    DModWOrdSer.QrLoc.SQL.Add('com.Controle, ');
    DModWOrdSer.QrLoc.SQL.Add(Geral.FF0(UserImp) + ', ');
    DModWOrdSer.QrLoc.SQL.Add('wor.Abertura, ');
    DModWOrdSer.QrLoc.SQL.Add('wor.Encerr, ');
    DModWOrdSer.QrLoc.SQL.Add('IF (wor.Finaliz = 0, "O.S. em aberto", DATE_FORMAT(wor.Encerr, "%d/%m/%Y %H:%i:%s")) ENCERROU_TXT, ');
    DModWOrdSer.QrLoc.SQL.Add('IF (ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMECLI, ');
    DModWOrdSer.QrLoc.SQL.Add('wus.PersonalName, ');
    DModWOrdSer.QrLoc.SQL.Add('CONCAT(LPAD(ass.Ordem, 3, 0), " - ", ass.Nome) AssuntoTXT, ');
    DModWOrdSer.QrLoc.SQL.Add('wmo.Nome NOMEMOD, ');
    DModWOrdSer.QrLoc.SQL.Add('CONCAT(LPAD(pri.Ordem, 3, 0), " - ", pri.Nome) NOMEPRI, ');
    DModWOrdSer.QrLoc.SQL.Add('sta.Nome NOMESTA, ');
    DModWOrdSer.QrLoc.SQL.Add('wur.PersonalName NOMERESP, ');
    DModWOrdSer.QrLoc.SQL.Add('wor.Descri, ');
    DModWOrdSer.QrLoc.SQL.Add('enc.Descri NOMESOL, ');
    DModWOrdSer.QrLoc.SQL.Add('apl.Nome NOMEAPLIC, ');
    DModWOrdSer.QrLoc.SQL.Add('wor.VersaoApp NOMEVER, ');
    DModWOrdSer.QrLoc.SQL.Add('wor.Janela, ');
    DModWOrdSer.QrLoc.SQL.Add('wor.JanelaRel, ');
    DModWOrdSer.QrLoc.SQL.Add('wor.CompoRel, ');
    DModWOrdSer.QrLoc.SQL.Add('com.Nome NomeCom, ');
    DModWOrdSer.QrLoc.SQL.Add('wuc.PersonalName Entidade, ');
    DModWOrdSer.QrLoc.SQL.Add('com.DataHora DataHoraCom, ');
    DModWOrdSer.QrLoc.SQL.Add('his.DataHora DATAHORAHIS ');
    DModWOrdSer.QrLoc.SQL.Add('FROM wordser wor ');
    DModWOrdSer.QrLoc.SQL.Add('LEFT JOIN wordsercom com ON com.Codigo = wor.Codigo ');
    DModWOrdSer.QrLoc.SQL.Add('LEFT JOIN wusers wuc ON wuc.Codigo = com.Entidade');
    DModWOrdSer.QrLoc.SQL.Add('LEFT JOIN entidades ent ON ent.Codigo = wor.Cliente ');
    DModWOrdSer.QrLoc.SQL.Add('LEFT JOIN wordserass ass ON ass.Codigo = wor.Assunto ');
    DModWOrdSer.QrLoc.SQL.Add('LEFT JOIN wordsersta sta ON sta.Codigo = wor.Status ');
    DModWOrdSer.QrLoc.SQL.Add('LEFT JOIN wordserpri pri ON pri.Codigo = wor.Prioridade ');
    DModWOrdSer.QrLoc.SQL.Add('LEFT JOIN wusers wur ON wur.Codigo = wor.Respons ');
    DModWOrdSer.QrLoc.SQL.Add('LEFT JOIN wordsermod wmo ON wmo.Codigo = wor.Modo ');
    DModWOrdSer.QrLoc.SQL.Add('LEFT JOIN wusers wus ON wus.Codigo = wor.Solicit ');
    DModWOrdSer.QrLoc.SQL.Add('LEFT JOIN wordserenc enc ON enc.Codigo = wor.WOrdSerEnc ');
    DModWOrdSer.QrLoc.SQL.Add('LEFT JOIN aplicativos apl ON apl.Codigo = wor.Aplicativo ');
    DModWOrdSer.QrLoc.SQL.Add('LEFT JOIN wordsertar tar ON tar.Codigo = wor.Codigo');
    DModWOrdSer.QrLoc.SQL.Add('LEFT JOIN wordserhis his ON his.Controle =');
    DModWOrdSer.QrLoc.SQL.Add('(');
    DModWOrdSer.QrLoc.SQL.Add('  SELECT MAX(Controle)');
    DModWOrdSer.QrLoc.SQL.Add('  FROM wordserhis');
    DModWOrdSer.QrLoc.SQL.Add('  WHERE Codigo = wor.Codigo');
    DModWOrdSer.QrLoc.SQL.Add(')');
    DModWOrdSer.QrLoc.SQL.Add(FSQLWordSerImp);
    if Codigos <> '' then
      DModWOrdSer.QrLoc.SQL.Add('AND wor.Codigo IN (' + Codigos + ')');
    //
    Ordem := DModWOrdSer.QrWOSOpcUsu.FieldByName('Ordem_CamposFields').AsString;
    //
    if Ordem = '' then
      Ordem := 'DATAHORAHIS DESC ';
    //
    DModWOrdSer.QrLoc.SQL.Add('ORDER BY ' + Ordem);
    DModWOrdSer.QrLoc.ExecSQL;
    //
    Result := True;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmWOrdSer.ImprimePesquisados(MostraComent,
  Selecionados: Boolean);
  function InsereCodigoSelecionados: String;
  var
    i: Integer;
    Codigos: String;
  begin
    Codigos := '';
    //
    if DBGItens.SelectedRows.Count > 1 then
    begin
      with DBGItens.DataSource.DataSet do
      for i:= 0 to DBGItens.SelectedRows.Count-1 do
      begin
        GotoBookmark(DBGItens.SelectedRows.Items[i]);
        //
        if i = 0 then
          Codigos := Geral.FF0(QrWOrdSerCodigo.Value)
        else
          Codigos := Codigos + ', ' + Geral.FF0(QrWOrdSerCodigo.Value);
      end;
    end else
      Codigos := Geral.FF0(QrWOrdSerCodigo.Value);
    Result := Codigos;
  end;
var
  Txt: String;
begin
  if Selecionados then
    Txt := InsereCodigoSelecionados
  else
    Txt := '';
  //
  FImpComen := MostraComent;
  //
  MyObjects.frxDefineDataSets(frxOSR_ORSER_001_002, [
    DModG.frxDsEndereco,
    frxDsWOrdSerImp
    ]);
  DModG.ReopenEndereco(Geral.IMV(VAR_LIB_EMPRESAS));
  GeraImpressaoWOrdSerPesq(VAR_WEB_USR_ID, Txt);
  ReopenWOrdSerImp(0, VAR_WEB_USR_ID);
  MyObjects.frxMostra(frxOSR_ORSER_001_002, 'Ordens de Servi�o');
  //
  PageControl1.ActivePageIndex := 0;
  FImpComen                    := False;
  //
  QrWOrdSerImp.Close;
end;

procedure TFmWOrdSer.LimpaCamposPesq(LimpaAtendente: Boolean = False);
begin
  EdCliente.ValueVariant    := 0;
  CBCliente.KeyValue        := Null;
  EdSolicit.ValueVariant    := 0;
  CBSolicit.KeyValue        := Null;
  EdAssunto.ValueVariant    := 0;
  CBAssunto.KeyValue        := Null;
  EdModo.ValueVariant       := 0;
  CBModo.KeyValue           := Null;
  EdPrioridade.ValueVariant := 0;
  CBPrioridade.KeyValue     := Null;
  EdStatus.ValueVariant     := 0;
  CBStatus.KeyValue         := Null;
  CkGrupo.Checked           := True;
  CkTarefa.Checked          := True;
  //
  if LimpaAtendente then
  begin
    EdRespons.ValueVariant := 0;
    CBRespons.KeyValue     := Null;
  end else
  begin
    EdRespons.ValueVariant := VAR_WEB_USR_ID;
    CBRespons.KeyValue     := VAR_WEB_USR_ID;
  end;
  EdRequisitante.ValueVariant := 0;
  CBRequisitante.KeyValue     := Null;
  EdDescri.ValueVariant       := '';
  CkAbeIni.Checked            := False;
  TPAbeIni.Date               := Date;
  CkAbeFim.Checked            := False;
  TPAbeFim.Date               := Date;
  CkEncIni.Checked            := False;
  TPEncIni.Date               := Date;
  CkEncFim.Checked            := False;
  TPEncFim.Date               := Date;
  EdCodigo.ValueVariant       := 0;
  RGStatus.ItemIndex          := 0;
  EdAplicativo.ValueVariant   := 0;
  CBAplicativo.KeyValue       := Null;
  EdPesqGrupo.ValueVariant    := 0;
  CBPesqGrupo.KeyValue        := Null;
  //
  if VAR_WEB_USR_TIPO = 1 then
  begin
    EdRespons.Enabled      := False;
    CBRespons.Enabled      := False;
    EdRequisitante.Enabled := False;
    CBRequisitante.Enabled := False;
  end else
  begin
    EdRespons.Enabled      := True;
    CBRespons.Enabled      := True;
    EdRequisitante.Enabled := True;
    CBRequisitante.Enabled := True;
  end;
end;

procedure TFmWOrdSer.PageControl1Change(Sender: TObject);
var
  PgIndx: Integer;
begin
  PgIndx := PageControl1.ActivePageIndex;
  //
  try
    Screen.Cursor := crHourGlass;
    //
    case PgIndx of
      1: ReopenWOrdSerCom(0);
      2: ReopenWOrdSerArq(0);
      3: ReopenOrdSerPgt;
      4: ReopenWOrdSerHis;
      5: ReopenWOrdSerRem;
    end;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmWOrdSer.PMEntidadesPopup(Sender: TObject);
var
  Enab, Enab2: Boolean;
begin
  Enab := (QrWOrdSer.State <> dsInactive) and (QrWOrdSer.RecordCount > 0);
  //
  {$IfnDef SWUsr}
  Enab2 := True;
  {$Else}
  Enab2 := False;
  {$EndIf}
  //
  EntidadesDadoscadastrais1.Enabled         := Enab;
  ClienteDadosusurioWEB1.Enabled            := Enab and Enab2;
  ClientesDadosespecficosdocliente1.Enabled := Enab and (CO_DMKID_APP = 17);
  ResponsvelDadosWEB1.Enabled               := Enab and Enab2;
end;

procedure TFmWOrdSer.PMWOrdSerPopup(Sender: TObject);
var
  Enab: Boolean;
begin
  if not DmkWeb.ConectarUsuarioWEB(True) then Exit;
  //
  Enab := (QrWOrdSer.State <> dsInactive) and (QrWOrdSer.RecordCount > 0);
  //
  Visualizarsolicitao1.Enabled              := Enab;
  AlteraStatusdasolicitaoatual1.Enabled     := Enab;
  Alteraresponsveldasolicitaoatual1.Enabled := Enab;
  Criartarefa1.Enabled                      := Enab;
end;

procedure TFmWOrdSer.QrWOrdSerAfterScroll(DataSet: TDataSet);
begin
  if FImpComen then
    ReopenWOrdSerCom(0)
  else
    PageControl1.ActivePageIndex := 0;
end;

procedure TFmWOrdSer.QrWOrdSerBeforeClose(DataSet: TDataSet);
begin
  QrWOrdSerArq.Close;
  QrWOrdSerCom.Close;
  QrOrdSerPgt.Close;
end;

procedure TFmWOrdSer.QrWOrdSerHisAfterScroll(DataSet: TDataSet);
var
  Visi: Boolean;
begin
  Visi := QrWOrdSerHisTarefa.Value = 3; //Reabertura
  //
  DBMeMotivReopen.Visible := Visi;
  Splitter5.Visible       := Visi;
end;

procedure TFmWOrdSer.ReopenOrdSerPgt;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrOrdSerPgt, Dmod.MyDB, [
  'SELECT la.Data, la.Vencimento, la.Credito, la.Banco, la.Agencia, la.FatID, ',
  'la.ContaCorrente, la.Documento, la.Descricao, la.FatParcela, la.FatNum, ',
  'ca.Nome NOMECARTEIRA, ca.Nome2 NOMECARTEIRA2, ca.Banco1, ',
  'ca.Agencia1, ca.Conta1, ca.TipoDoc, la.Controle, ',
  'CASE WHEN fo.Tipo=0 THEN fo.RazaoSocial ',
  'ELSE fo.Nome END NOMEFORNECEI, ',
  'ca.Tipo CARTEIRATIPO, la.Tipo, la.Carteira, la.Sub ',
  'FROM ' + FTabLctA + ' la ',
  'LEFT JOIN Carteiras ca ON ca.Codigo=la.Carteira ',
  'LEFT JOIN Entidades fo ON fo.Codigo=ca.ForneceI ',
  'WHERE FatID=' + Geral.FF0(VAR_FATID_0901),
  'AND FatNum=' + Geral.FF0(QrWOrdSerCodigo.Value),
  ' ',
  'UNION ',
  ' ',
  'SELECT la.Data, la.Vencimento, la.Credito, la.Banco, la.Agencia, la.FatID, ',
  'la.ContaCorrente, la.Documento, la.Descricao, la.FatParcela, la.FatNum, ',
  'ca.Nome NOMECARTEIRA, ca.Nome2 NOMECARTEIRA2, ca.Banco1, ',
  'ca.Agencia1, ca.Conta1, ca.TipoDoc, la.Controle, ',
  'CASE WHEN fo.Tipo=0 THEN fo.RazaoSocial ',
  'ELSE fo.Nome END NOMEFORNECEI, ',
  'ca.Tipo CARTEIRATIPO, la.Tipo, la.Carteira, la.Sub ',
  'FROM ' + FTabLctB + ' la ',
  'LEFT JOIN Carteiras ca ON ca.Codigo=la.Carteira ',
  'LEFT JOIN Entidades fo ON fo.Codigo=ca.ForneceI ',
  'WHERE FatID=' + Geral.FF0(VAR_FATID_0901),
  'AND FatNum=' + Geral.FF0(QrWOrdSerCodigo.Value),
  ' ',
  'UNION ',
  ' ',
  'SELECT la.Data, la.Vencimento, la.Credito, la.Banco, la.Agencia, la.FatID, ',
  'la.ContaCorrente, la.Documento, la.Descricao, la.FatParcela, la.FatNum, ',
  'ca.Nome NOMECARTEIRA, ca.Nome2 NOMECARTEIRA2, ca.Banco1, ',
  'ca.Agencia1, ca.Conta1, ca.TipoDoc, la.Controle, ',
  'CASE WHEN fo.Tipo=0 THEN fo.RazaoSocial ',
  'ELSE fo.Nome END NOMEFORNECEI, ',
  'ca.Tipo CARTEIRATIPO, la.Tipo, la.Carteira, la.Sub ',
  'FROM ' + FTabLctD + ' la ',
  'LEFT JOIN Carteiras ca ON ca.Codigo=la.Carteira ',
  'LEFT JOIN Entidades fo ON fo.Codigo=ca.ForneceI ',
  'WHERE FatID=' + Geral.FF0(VAR_FATID_0901),
  'AND FatNum=' + Geral.FF0(QrWOrdSerCodigo.Value),
  ' ',
  'ORDER BY FatParcela, Vencimento ',
  '']);
end;

procedure TFmWOrdSer.ReopenQuerys;
begin
  DModWOrdSer.ReopenClientes;
  DModWOrdSer.ReopenWOrdSerMod();
  DModWOrdSer.ReopenWOrdSerPri;
  DModWOrdSer.ReopenWOrdSerAss();
  DModWOrdSer.ReopenWOrdSerSta(-1, 0);
  DModWOrdSer.ReopenRespons;
  DModWOrdSer.ReopenRequisitante;
  DModWOrdSer.ReopenWOrdSerGruPes();
  //
  if CO_DMKID_APP = 17 then //DControl
    DModWOrdSer.ReopenAplicativos(0);
end;

procedure TFmWOrdSer.ReopenWOrdSer(Codigo, Respons: Integer;
  NaoReabre: Boolean = False);
var
  Cliente, Assunto, Modo, Prior, Status, Resp, Finaliz, Aplic, Solicit,
  WOrdSerGru, Requisitante, Tarefa: Integer;
  ID, Descri, SQL, TimeZoneDifUTC, Ordem: String;
begin
  if (NaoReabre = False) or ((QrWOrdSer.State = dsInactive) or (QrWOrdSer.RecordCount = 0)) then
  begin
    FSQLWordSerImp := '';
    SQL            := '';
    TimeZoneDifUTC := VAR_WEB_USR_TZDIFUTC;
    Cliente        := EdCliente.ValueVariant;
    Solicit        := EdSolicit.ValueVariant;
    Assunto        := EdAssunto.ValueVariant;
    Modo           := EdModo.ValueVariant;
    Prior          := EdPrioridade.ValueVariant;
    Status         := EdStatus.ValueVariant;
    Resp           := EdRespons.ValueVariant;
    Requisitante   := EdRequisitante.ValueVariant;
    Descri         := EdDescri.ValueVariant;
    Finaliz        := RGStatus.ItemIndex;
    Aplic          := EdAplicativo.ValueVariant;
    ID             := EdCodigo.ValueVariant;
    Ordem          := DModWOrdSer.QrWOSOpcUsu.FieldByName('Ordem_CamposFields').AsString;
    WOrdSerGru     := EdPesqGrupo.ValueVariant;
    Tarefa         := Geral.BoolToInt(CkTarefa.Checked);
    //
    if TimeZoneDifUTC = '' then
    begin
      Geral.MB_Aviso('Time zone n�o definido!');
      QrWOrdSer.Close;
      Exit;
    end;
    QrWOrdSer.Close;
    QrWOrdSer.SQL.Clear;
    QrWOrdSer.SQL.Add('SELECT wor.*, sta.Nome NOMESTA, gru.Nome NOMEGRUPO, ');
    QrWOrdSer.SQL.Add('IF(tar.Codigo IS NULL, 0, 1) TAREFA, ');
    QrWOrdSer.SQL.Add('CONCAT(LPAD(pri.Ordem, 3, 0), " - ", pri.Nome) NOMEPRI, wmo.Nome NOMEMOD,');
    QrWOrdSer.SQL.Add('IF (ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMECLI,');
    QrWOrdSer.SQL.Add('wur.PersonalName NOMERESP, wus.PersonalName, ');
    QrWOrdSer.SQL.Add('enc.Descri NOMESOL, CONCAT(LPAD(ass.Ordem, 3, 0), " - ", ass.Nome) AssuntoTXT,');
    QrWOrdSer.SQL.Add('CONVERT_TZ(his.DataHora, "+00:00", "'+ TimeZoneDifUTC +'") DATAHORAHIS,');
    //
    if CO_DMKID_APP = 17 then //DControl
      QrWOrdSer.SQL.Add('apl.Nome NOMEAPLIC, ')
    else
      QrWOrdSer.SQL.Add('"" NOMEAPLIC, ');
    //
    QrWOrdSer.SQL.Add('CASE his.Tarefa');
    QrWOrdSer.SQL.Add('WHEN 0 THEN "Abertura"');
    QrWOrdSer.SQL.Add('WHEN 1 THEN "Encerramento"');
    QrWOrdSer.SQL.Add('WHEN 2 THEN "Comentado"');
    QrWOrdSer.SQL.Add('WHEN 3 THEN "Reabertura"');
    QrWOrdSer.SQL.Add('WHEN 4 THEN "Mudan�a de Status" END STATUS_TXT, ');
    QrWOrdSer.SQL.Add('IF (wor.Finaliz = 0, "O.S. em aberto", ');
    QrWOrdSer.SQL.Add('DATE_FORMAT(CONVERT_TZ(wor.Encerr, "+00:00", "'+ TimeZoneDifUTC +'"), "%d/%m/%Y %H:%i:%s")) ENCERROU_TXT,');
    QrWOrdSer.SQL.Add('DATE_FORMAT(CONVERT_TZ(wor.Abertura, "+00:00", "'+ TimeZoneDifUTC +'"), "%d/%m/%Y %H:%i:%s") ABERTURA_TXT');
    QrWOrdSer.SQL.Add('FROM wordser wor');
    QrWOrdSer.SQL.Add('LEFT JOIN entidades ent ON ent.Codigo = wor.Cliente');
    QrWOrdSer.SQL.Add('LEFT JOIN wordserass ass ON ass.Codigo = wor.Assunto');
    QrWOrdSer.SQL.Add('LEFT JOIN wordsersta sta ON sta.Codigo = wor.Status');
    QrWOrdSer.SQL.Add('LEFT JOIN wordserpri pri ON pri.Codigo = wor.Prioridade');
    QrWOrdSer.SQL.Add('LEFT JOIN wusers wur ON wur.Codigo = wor.Respons');
    QrWOrdSer.SQL.Add('LEFT JOIN wordsermod wmo ON wmo.Codigo = wor.Modo');
    QrWOrdSer.SQL.Add('LEFT JOIN wusers wus ON wus.Codigo = wor.Solicit');
    QrWOrdSer.SQL.Add('LEFT JOIN wordserenc enc ON enc.Codigo = wor.WOrdSerEnc');
    QrWOrdSer.SQL.Add('LEFT JOIN wordsertar tar ON tar.Codigo = wor.Codigo');
    //
    if CO_DMKID_APP = 17 then //DControl
      QrWOrdSer.SQL.Add('LEFT JOIN aplicativos apl ON apl.Codigo = wor.Aplicativo');
    //
    QrWOrdSer.SQL.Add('LEFT JOIN wordsergru gru ON gru.Codigo = wor.Grupo');
    QrWOrdSer.SQL.Add('LEFT JOIN wordserhis his ON his.Controle = wor.WOrdSerHis');
    (*
    QrWOrdSer.SQL.Add('LEFT JOIN wordserhis his ON his.Controle =');
    QrWOrdSer.SQL.Add('(');
    QrWOrdSer.SQL.Add('  SELECT MAX(Controle)');
    QrWOrdSer.SQL.Add('  FROM wordserhis');
    QrWOrdSer.SQL.Add('  WHERE Codigo = wor.Codigo');
    QrWOrdSer.SQL.Add(')');
    *)
    QrWOrdSer.SQL.Add('WHERE wor.Codigo > 0');
    //
    FSQLWordSerImp := 'WHERE wor.Codigo > 0';
    //
    if (ID <> '') and (ID <> '0') then
    begin
      SQL := 'AND wor.Codigo IN (' + ID + ')';
      //
      QrWOrdSer.SQL.Add(SQL);
      //
      FSQLWordSerImp := FSQLWordSerImp + ' ' + SQL;
    end;
    if Cliente <> 0 then
    begin
      SQL := 'AND wor.Cliente = ' + Geral.FF0(EdCliente.ValueVariant);
      //
      QrWOrdSer.SQL.Add(SQL);
      //
      FSQLWordSerImp := FSQLWordSerImp + ' ' + SQL;
    end;
    if Solicit > 0 then
    begin
      SQL := 'AND wor.Solicit = ' + Geral.FF0(Solicit);
      //
      QrWOrdSer.SQL.Add(SQL);
      //
      FSQLWordSerImp := FSQLWordSerImp + ' ' + SQL;
    end;
    if Assunto > 0 then
    begin
      SQL := 'AND wor.Assunto = ' + Geral.FF0(Assunto);
      //
      QrWOrdSer.SQL.Add(SQL);
      //
      FSQLWordSerImp := FSQLWordSerImp + ' ' + SQL;
    end;
    if Modo > 0 then
    begin
      SQL := 'AND wor.Modo = ' + Geral.FF0(Modo);
      //
      QrWOrdSer.SQL.Add(SQL);
      //
      FSQLWordSerImp := FSQLWordSerImp + ' ' + SQL;
    end;
    if Prior > 0 then
    begin
      SQL := 'AND wor.Prior = ' + Geral.FF0(Prior);
      //
      QrWOrdSer.SQL.Add(SQL);
      //
      FSQLWordSerImp := FSQLWordSerImp + ' ' + SQL;
    end;
    if Status > 0 then
    begin
      SQL := 'AND wor.Status = ' + Geral.FF0(Status);
      //
      QrWOrdSer.SQL.Add(SQL);
      //
      FSQLWordSerImp := FSQLWordSerImp + ' ' + SQL;
    end;
    if (Resp > 0) and (Respons = 0) then
    begin
      SQL := 'AND wor.Respons = ' + Geral.FF0(EdRespons.ValueVariant);
      //
      QrWOrdSer.SQL.Add(SQL);
      //
      FSQLWordSerImp := FSQLWordSerImp + ' ' + SQL;
    end;
    if (Requisitante > 0) and (Respons = 0) then
    begin
      SQL := 'AND wor.UserCad = ' + Geral.FF0(EdRequisitante.ValueVariant);
      //
      QrWOrdSer.SQL.Add(SQL);
      //
      FSQLWordSerImp := FSQLWordSerImp + ' ' + SQL;
    end;
    if Respons <> 0 then
    begin
      SQL := 'AND (wor.Respons = ' +
        Geral.FF0(Respons) + ' OR wor.UserCad = '
        + Geral.FF0(Respons) + ') ';
      //
      QrWOrdSer.SQL.Add(SQL);
      //
      FSQLWordSerImp := FSQLWordSerImp + ' ' + SQL;
    end;
    if Length(Descri) > 0 then
    begin
      SQL := 'AND wor.Descri LIKE "%' + Descri + '%"';
      //
      QrWOrdSer.SQL.Add(SQL);
      //
      FSQLWordSerImp := FSQLWordSerImp + ' ' + SQL;
    end;
    if CkGrupo.Checked = True then
    begin
      SQL := 'AND wor.Grupo = ' + Geral.FF0(WOrdSerGru);
      //
      QrWOrdSer.SQL.Add(SQL);
      //
      FSQLWordSerImp := FSQLWordSerImp + ' ' + SQL;
    end;
    SQL := dmkPF.SQL_Periodo('AND DATE_FORMAT(Abertura, "%Y-%m-%d") ',
             TPAbeIni.Date, TPAbeFim.Date, CkAbeIni.Checked, CkAbeFim.Checked);
    //
    QrWOrdSer.SQL.Add(SQL);
    //
    FSQLWordSerImp := FSQLWordSerImp + ' ' + SQL;
    //
    SQL := dmkPF.SQL_Periodo('AND DATE_FORMAT(Encerr, "%Y-%m-%d") ',
             TPEncIni.Date, TPEncFim.Date, CkEncIni.Checked, CkEncFim.Checked);
    //
    QrWOrdSer.SQL.Add(SQL);
    //
    FSQLWordSerImp := FSQLWordSerImp + ' ' + SQL;
    //
    if Finaliz in [0, 1] then
    begin
      SQL := 'AND wor.Finaliz = ' + Geral.FF0(Finaliz);
      //
      QrWOrdSer.SQL.Add(SQL);
      //
      FSQLWordSerImp := FSQLWordSerImp + ' ' + SQL;
    end;
    if Aplic > 0 then
    begin
      SQL := 'AND wor.Aplicativo = ' + Geral.FF0(EdAplicativo.ValueVariant);
      //
      QrWOrdSer.SQL.Add(SQL);
      //
      FSQLWordSerImp := FSQLWordSerImp + ' ' + SQL;
    end;
    if Tarefa = 1 then
    begin
      SQL := 'AND tar.Codigo IS NULL';
      //
      QrWOrdSer.SQL.Add(SQL);
      //
      FSQLWordSerImp := FSQLWordSerImp + ' ' + SQL;
    end;
    if Ordem = '' then
      Ordem := 'DATAHORAHIS DESC ';
    //
    QrWOrdSer.SQL.Add('ORDER BY ' + Ordem);
    QrWOrdSer.Open;
    //
    if QrWOrdSer.RecordCount > 0 then
      EdTotItens.ValueVariant := QrWOrdSer.RecordCount
    else
      EdTotItens.ValueVariant := 0;
    //
    LaOrderBy.Caption := 'Ordena��o da grade: ' + DModWOrdSer.QrWOSOpcUsu.FieldByName('Ordem_CamposTxt').AsString;
  end;
  if Codigo > 0 then
  begin
    if not QrWOrdSer.Locate('Codigo', Codigo, []) then
      Geral.MB_Aviso('N�o foi poss�vel localizar o registro!' + sLineBreak +
        'Verifique se os filtros de pesquisa est�o configurados corretamente.');
  end;
end;

procedure TFmWOrdSer.ReopenWOrdSerArq(Controle: Integer);
begin
  QrWOrdSerArq.Close;
  QrWOrdSerArq.Params[0].AsInteger := QrWOrdSerCodigo.Value;
  UMyMod.AbreQuery(QrWOrdSerArq, DMod.MyDBn);
  //
  if Controle <> 0 then
    QrWOrdSerArq.Locate('Controle', Controle, []);
end;

procedure TFmWOrdSer.ReopenWOrdSerCom(Controle: Integer);
var
  TimeZoneDifUTC: String;
begin
  TimeZoneDifUTC := VAR_WEB_USR_TZDIFUTC;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrWOrdSerCom, Dmod.MyDBn, [
    'SELECT wus.PersonalName, wu2.PersonalName Para_TXT, ',
    'DATE_FORMAT(CONVERT_TZ(DataHora, "+00:00", "'+ TimeZoneDifUTC +'"), "%d/%m/%Y %H:%i:%s") DataHora_TXT, ',
    'IF(com.Lido=1, DATE_FORMAT(CONVERT_TZ(DataHoraLido, "+00:00", "'+ TimeZoneDifUTC +'"), "%d/%m/%Y %H:%i:%s"), "") DataHoraLido_TXT, ',
    'com.* ',
    'FROM wordsercom com ',
    'LEFT JOIN wusers wus ON wus.Codigo = com.Entidade ',
    'LEFT JOIN wusers wu2 ON wu2.Codigo = com.Para ',
    'WHERE com.Codigo=' + Geral.FF0(QrWOrdSerCodigo.Value),
    'ORDER BY DataHora ASC ',
    '']);
  if Controle <> 0 then
    QrWOrdSerCom.Locate('Controle', Controle, [])
  else
    QrWOrdSerCom.Last;
end;

procedure TFmWOrdSer.ReopenWOrdSerRem;
var
  Codigo: Integer;
  TimeZoneDifUTC: String;
begin
  Codigo         := QrWOrdSerCodigo.Value;
  TimeZoneDifUTC := VAR_WEB_USR_TZDIFUTC;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrWOrdSerRem, Dmod.MyDBn, [
    'SELECT *, ',
    'DATE_FORMAT(CONVERT_TZ(StartDate, "+00:00", "' + TimeZoneDifUTC + '"), "%d/%m/%Y %H:%i:%s") StartDate_TZ_TXT, ',
    'DATE_FORMAT(CONVERT_TZ(EndDate, "+00:00", "' + TimeZoneDifUTC + '"), "%d/%m/%Y %H:%i:%s") EndDate_TZ_TXT, ',
    'TIMEDIFF(EndDate, StartDate) Duracao ',
    'FROM wordserrem ',
    'WHERE Codigo=' + Geral.FF0(Codigo),
    'ORDER BY Controle ',
    '']);
end;

procedure TFmWOrdSer.ResponsvelDadosWEB1Click(Sender: TObject);
begin
  {$IfnDef SWUsr}
  UWUsersJan.MostraWUsers(QrWOrdSerRespons.Value);
  {$EndIf}
end;

procedure TFmWOrdSer.ReopenWOrdSerHis;
var
  Codigo: Integer;
  TimeZoneDifUTC: String;
begin
  Codigo         := QrWOrdSerCodigo.Value;
  TimeZoneDifUTC := VAR_WEB_USR_TZDIFUTC;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrWOrdSerHis, Dmod.MyDBn, [
    'SELECT CONVERT_TZ(his.DataHora, "+00:00", "' + TimeZoneDifUTC + '") DATAHORAHIS, ',
    'CASE his.Tarefa ',
    '  WHEN 0 THEN "Abertura" ',
    '  WHEN 1 THEN "Encerramento" ',
    '  WHEN 2 THEN "Comentado" ',
    '  WHEN 3 THEN "Reabertura" ',
    '  WHEN 4 THEN "Mudan�a de Status" END STATUS_TXT, ',
    'wus.PersonalName RESPONS, his.Codigo, his.Controle, his.MotivReopen, his.Tarefa ',
    'FROM wordserhis his ',
    'LEFT JOIN wusers wus ON wus.Codigo = his.Respons ',
    'WHERE his.Codigo=' + Geral.FF0(Codigo),
    'ORDER BY DATAHORAHIS ',
    '']);
end;

procedure TFmWOrdSer.ReopenWOrdSerImp(Codigo, UserImp: Integer);
var
  SQLCompl, Ordem: String;
begin
  Ordem := DModWOrdSer.QrWOSOpcUsu.FieldByName('Ordem_CamposFields').AsString;
  //
  if Ordem = '' then
    Ordem := 'DATAHORAHIS DESC ';
  //
  SQLCompl := 'ORDER BY ' + Ordem;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrWOrdSerImp, Dmod.MyDBn, [
    'SELECT wor.* ',
    'FROM wordserimp wor ',
    'WHERE wor.UserImp=' + Geral.FF0(UserImp),
    SQLCompl,
    '']);
  if Codigo > 0 then
    QrWOrdSerImp.Locate('Codigo', Codigo, []);
end;

procedure TFmWOrdSer.SbImprimeClick(Sender: TObject);
begin
  if not DmkWeb.ConectarUsuarioWEB(True) then Exit;
  //
  MyObjects.MostraPopUpDeBotao(PMImprime, SbImprime);
end;

procedure TFmWOrdSer.SbReabreClick(Sender: TObject);
begin
  if not DmkWeb.ConectarUsuarioWEB(True) then Exit;
  //
  ReopenWOrdSer(0, 0);
  //
  PnAviso.Visible := False;
end;

procedure TFmWOrdSer.Semcomentrios2Click(Sender: TObject);
begin
  ImprimePesquisados(False, False);
end;

procedure TFmWOrdSer.Semcomentrios3Click(Sender: TObject);
begin
  ImprimePesquisados(False, True);
end;

procedure TFmWOrdSer.Timer1Timer(Sender: TObject);
var
  MaxControle, MaxUsuario: Integer;
begin
  Timer1.Enabled := False;
  MaxControle    := 0;
  MaxUsuario     := 0;
  //
  if not DmkWeb.ConectarUsuarioWEB(True) then Exit;
  //
  DModWOrdSer.VerificaMaxHistorico(EdRespons.ValueVariant, MaxControle, MaxUsuario);
  //
  if (FMaxHistorico <> MaxControle) and (MaxUsuario <> VAR_WEB_USR_ID) then
  begin
    FMaxHistorico := MaxControle;
    //
    TrayIcon1.Visible := True;
    TrayIcon1.ShowBalloonHint;
    Timer2.Enabled := True;
    //
    PnAviso.Visible := True;
  end;
end;

procedure TFmWOrdSer.Timer2Timer(Sender: TObject);
begin
  TrayIcon1.Visible := False;
  Timer2.Enabled    := False;
end;

procedure TFmWOrdSer.Visualizarsolicitao1Click(Sender: TObject);
begin
  AlteraSolicit(True);
end;

end.
