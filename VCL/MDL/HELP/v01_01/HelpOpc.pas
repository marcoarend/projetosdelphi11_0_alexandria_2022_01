unit HelpOpc;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkPermissoes,
  dmkEditCB, dmkDBLookupComboBox, mySQLDbTables;

type
  TFmHelpOpc = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    LaTitulo1C: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    BtItens: TBitBtn;
    QrHelpOpc: TmySQLQuery;
    QrHelpOpcCodigo: TIntegerField;
    QrHelpOpcTexto: TWideMemoField;
    DsHelpOpc: TDataSource;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtItensClick(Sender: TObject);
  private
    { Private declarations }

  public
    { Public declarations }
  end;

  var
  FmHelpOpc: TFmHelpOpc;

implementation

uses UnMyObjects, UMySQLModule, UnTextos_Jan, Module, UnGrlHelp;

{$R *.DFM}

procedure TFmHelpOpc.BtItensClick(Sender: TObject);
begin
  if (QrHelpOpc.State <> dsInactive) and (QrHelpOpc.RecordCount > 0) then
  begin
    Textos_Jan.MostraTextosHTML(QrHelpOpcCodigo.Value, QrHelpOpcTexto.Value,
      'helpopc', 'Codigo', 'Texto', Dmod.MyDB, True);
    //
    GrlHelp.ReopenHelpOpc(Dmod.MyDB, QrHelpOpc);
  end;
end;

procedure TFmHelpOpc.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmHelpOpc.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmHelpOpc.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  GrlHelp.ReopenHelpOpc(Dmod.MyDB, QrHelpOpc);
end;

procedure TFmHelpOpc.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

end.
