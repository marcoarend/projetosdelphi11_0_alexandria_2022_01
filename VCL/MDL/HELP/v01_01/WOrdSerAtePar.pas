unit WOrdSerAtePar;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, Variants,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnMLAGeral, UnInternalConsts,
  dmkImage, DmkDAC_PF, dmkPermissoes, UnDmkEnums, UnProjGroup_Consts;

type
  TFmWOrdSerAtePar = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    LaAssunto: TLabel;
    EdAssunto: TdmkEditCB;
    CBAssunto: TdmkDBLookupComboBox;
    CkContinuar: TCheckBox;
    DsAplicativos: TDataSource;
    QrAplicativos: TmySQLQuery;
    QrAplicativosCodigo: TAutoIncField;
    dmkPermissoes1: TdmkPermissoes;
    ImgWEB: TdmkImage;
    LaCliente: TLabel;
    EdCliente: TdmkEditCB;
    CBCliente: TdmkDBLookupComboBox;
    CBSolicitante: TdmkDBLookupComboBox;
    EdSolicitante: TdmkEditCB;
    LaSolicitante: TLabel;
    CBAplicativo: TdmkDBLookupComboBox;
    EdAplicativo: TdmkEditCB;
    LaAplicativo: TLabel;
    QrWUsers: TmySQLQuery;
    AutoIncField1: TAutoIncField;
    DsWUsers: TDataSource;
    QrClientes: TmySQLQuery;
    AutoIncField2: TAutoIncField;
    DsClientes: TDataSource;
    QrWOrdSerAss: TmySQLQuery;
    AutoIncField3: TAutoIncField;
    StringField3: TWideStringField;
    DsWOrdSerAss: TDataSource;
    QrClientesNome: TWideStringField;
    QrWUsersPersonalName: TWideStringField;
    QrAplicativosNome: TWideStringField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure EdClienteChange(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenClientes();
    procedure ReopenWUsers();
    procedure ReopenAplicativos();
    procedure MostraEdicao();
  public
    { Public declarations }
    FCodigo: Integer;
  end;

  var
  FmWOrdSerAtePar: TFmWOrdSerAtePar;

implementation

uses UnMyObjects, Module, UMySQLModule, WOrdSerAte, UnDmkWeb, MyListas;

{$R *.DFM}

procedure TFmWOrdSerAtePar.BtOKClick(Sender: TObject);
var
  Controle, Assunto, Cliente, Solicit, Aplicativo: Integer;
begin
  Assunto    := EdAssunto.ValueVariant;
  Cliente    := EdCliente.ValueVariant;
  Solicit    := EdSolicitante.ValueVariant;
  Aplicativo := EdAplicativo.ValueVariant;
  //
  Controle := UMyMod.BuscaNovoCodigo_Int(Dmod.QrUpdN, 'worseateit', 'Controle',
                [], [], stIns, 0, siPositivo, nil);
  //
  if UMyMod.SQLInsUpd_Old(Dmod.QrUpdN, CO_INCLUSAO, 'worseateit', False,
  [
    'Assunto', 'Cliente', 'Solicit', 'Aplicativo', 'Codigo'
  ], ['Controle'],
  [
    Assunto, Cliente, Solicit, Aplicativo, FCodigo
  ], [Controle])
  then begin
    FmWOrdSerAte.ReopenWOrSeAteIt(FCodigo);
    //
    if CkContinuar.Checked then
    begin
      MostraEdicao;
    end else
      Close;
  end;
end;

procedure TFmWOrdSerAtePar.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmWOrdSerAtePar.EdClienteChange(Sender: TObject);
begin
  ReopenWUsers;
  ReopenAplicativos;
end;

procedure TFmWOrdSerAtePar.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  DmkWeb.CarregaImagemFormWEB(ImgWEB);
end;

procedure TFmWOrdSerAtePar.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  UMyMod.AbreQuery(QrWOrdSerAss, Dmod.MyDBn);
  ReopenClientes();
  ReopenWUsers();
  ReopenAplicativos;
  //
  CkContinuar.Checked := False;
end;

procedure TFmWOrdSerAtePar.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
    [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmWOrdSerAtePar.FormShow(Sender: TObject);
begin
  MostraEdicao;
end;

procedure TFmWOrdSerAtePar.MostraEdicao();
var
  Enab: Boolean;
begin
  Enab := CO_DMKID_APP = 17; //DControl
  //
  EdAssunto.ValueVariant     := 0;
  CBAssunto.KeyValue         := Null;
  EdCliente.ValueVariant     := 0;
  CBCliente.KeyValue         := Null;
  EdSolicitante.ValueVariant := 0;
  CBSolicitante.KeyValue     := Null;
  EdAplicativo.ValueVariant  := 0;
  CBAplicativo.KeyValue      := Null;
  //
  LaAplicativo.Visible := Enab;
  EdAplicativo.Visible := Enab;
  CBAplicativo.Visible := Enab;
  //
  EdAssunto.SetFocus;
end;

procedure TFmWOrdSerAtePar.ReopenAplicativos;
var
  Cliente: Integer;
begin
  if CO_DMKID_APP = 17 then //DControl
  begin
    Cliente := EdCliente.ValueVariant;
    //
    if Cliente = 0 then
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(QrAplicativos, Dmod.MyDBn, [
        'SELECT Codigo, Nome ',
        'FROM aplicativos ',
        'WHERE Ativo = 1 ',
        'ORDER BY Nome ',
        '']);
    end else
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(QrAplicativos, Dmod.MyDBn, [
        'SELECT apl.Codigo, apl.Nome ',
        'FROM cliaplic cap ',
        'LEFT JOIN clientes cli ON cli.Codigo=cap.Codigo ',
        'LEFT JOIN aplicativos apl ON apl.Codigo=cap.Aplicativo ',
        'WHERE apl.Ativo = 1 ',
        'GROUP BY apl.Codigo ',
        'ORDER BY apl.Nome ',
        '']);
    end;
  end;
end;

procedure TFmWOrdSerAtePar.ReopenClientes;
var
  SQL: String;
begin
  if CO_DMKID_APP = 17 then //DControl
    SQL := 'AND Cliente1 = "V"'
  else
    SQL := '';
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrClientes, Dmod.MyDBn, [
    'SELECT Codigo, IF(Tipo=0, RazaoSocial, Nome) Nome ',
    'FROM entidades ',
    'WHERE Ativo=1 ',
    SQL,
    'ORDER BY Nome ',
    '']);
end;

procedure TFmWOrdSerAtePar.ReopenWUsers;
var
  SQL: String;
begin
  if EdCliente.ValueVariant <> 0 then
    SQL := 'AND Entidade=' + Geral.FF0(EdCliente.ValueVariant)
  else
    SQL := '';
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrWUsers, Dmod.MyDBn, [
    'SELECT Codigo, PersonalName ',
    'FROM wusers ',
    'WHERE Ativo = 1 ',
    SQL,
    'ORDER BY PersonalName ',
    '']);
end;

end.
