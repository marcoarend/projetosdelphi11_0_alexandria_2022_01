unit WOrdSerMod;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2,
  ResIntStrings, UnMLAGeral, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, dmkGeral,
  dmkPermissoes, dmkEdit, dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup,
  unDmkProcFunc, dmkCheckBox, DmkDAC_PF, UnDmkEnums;

type
  TFmWOrdSerMod = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    BtExclui: TBitBtn;
    BtAltera: TBitBtn;
    BtInclui: TBitBtn;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    Label9: TLabel;
    EdNome: TdmkEdit;
    Label1: TLabel;
    DBEdCodigo: TdmkDBEdit;
    Label2: TLabel;
    DBEdNome: TdmkDBEdit;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    QrWOrdSerMod: TmySQLQuery;
    QrWOrdSerModCodigo: TIntegerField;
    QrWOrdSerModNome: TWideStringField;
    QrWOrdSerModLk: TIntegerField;
    QrWOrdSerModDataCad: TDateField;
    QrWOrdSerModDataAlt: TDateField;
    QrWOrdSerModUserCad: TIntegerField;
    QrWOrdSerModUserAlt: TIntegerField;
    QrWOrdSerModAlterWeb: TSmallintField;
    QrWOrdSerModAtivo: TSmallintField;
    DsWOrdSerMod: TDataSource;
    CbAtivo: TdmkCheckBox;
    DBCheckBox1: TDBCheckBox;
    QrLoc: TmySQLQuery;
    ImgWEB: TdmkImage;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrCadastro_SimplesAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrCadastro_SimplesBeforeOpen(DataSet: TDataSet);
    procedure BtIncluiClick(Sender: TObject);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure QrWOrdSerModAfterOpen(DataSet: TDataSet);
    procedure QrWOrdSerModBeforeOpen(DataSet: TDataSet);
    procedure SbImprimeClick(Sender: TObject);
    procedure BtExcluiClick(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure Va(Para: TVaiPara);
  public
    { Public declarations }
    procedure LocCod(Atual, Codigo: Integer);
  end;

var
  FmWOrdSerMod: TFmWOrdSerMod;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, UnDmkWeb;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmWOrdSerMod.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmWOrdSerMod.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrWOrdSerModCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmWOrdSerMod.DefParams;
begin
  VAR_GOTOTABELA := 'wordsermod';
  VAR_GOTOMYSQLTABLE := QrWOrdSerMod;
  VAR_GOTONEG := gotoPos;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDBn;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT *');
  VAR_SQLx.Add('FROM wordsermod');
  VAR_SQLx.Add('WHERE Codigo > 0');
  //
  VAR_SQL1.Add('AND Codigo=:P0');
  //
  //VAR_SQL2.Add('AND CodUsu=:P0');
  //
  VAR_SQLa.Add('AND Nome Like :P0');
  //
end;

procedure TFmWOrdSerMod.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmWOrdSerMod.QueryPrincipalAfterOpen;
begin
end;

procedure TFmWOrdSerMod.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmWOrdSerMod.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmWOrdSerMod.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmWOrdSerMod.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmWOrdSerMod.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmWOrdSerMod.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmWOrdSerMod.BtAlteraClick(Sender: TObject);
begin
  if (QrWOrdSerMod.State <> dsInactive) and (QrWOrdSerMod.RecordCount > 0) and
    (QrWOrdSerModCodigo.Value <> 0) then
  begin
    UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrWOrdSerMod, [PnDados],
      [PnEdita], EdNome, ImgTipo, 'wordsermod');
  end;
end;

procedure TFmWOrdSerMod.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrWOrdSerModCodigo.Value;
  Close;
end;

procedure TFmWOrdSerMod.BtConfirmaClick(Sender: TObject);
var
  Codigo: Integer;
  Nome: String;
begin
  Nome := EdNome.ValueVariant;
  //
  if MyObjects.FIC(Length(Nome) = 0, EdNome, 'Defina um nome!') then Exit;
  //
  if ImgTipo.SQLType = stIns then
    Codigo := UMyMod.BuscaNovoCodigo_Int(Dmod.QrUpdN, 'wordsermod', 'Codigo',
      [], [], stIns, 0, siPositivo, nil)
  else
    Codigo := QrWOrdSerModCodigo.Value;
  //
  if UMyMod.ExecSQLInsUpdPanel(ImgTipo.SQLType, Self, PnEdita,
    'wordsermod', Codigo, Dmod.QrUpdN, [PnEdita], [PnDados], ImgTipo, True) then
  begin
    LocCod(Codigo, Codigo);
  end;
end;

procedure TFmWOrdSerMod.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo          := EdCodigo.ValueVariant;
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  //
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDBn, 'wordsermod', 'Codigo');
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmWOrdSerMod.BtExcluiClick(Sender: TObject);
var
  Codigo: Integer;
begin
  if (QrWOrdSerMod.State <> dsInactive) and (QrWOrdSerMod.RecordCount > 0) then
  begin
    Codigo := QrWOrdSerModCodigo.Value;
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrLoc, Dmod.MyDBn, [
    'SELECT * ',
    'FROM wordser ',
    'WHERE Modo=' + Geral.FF0(Codigo),
    '']);
    if QrLoc.RecordCount > 0 then
    begin
      Geral.MB_Aviso('Este modo j� foi utilizado nas solicita��es e por isso n�o poder� ser exclu�-do!');
      Exit;
    end;
    UMyMod.ExcluiRegistroInt1('Confirma a exclus�o deste item?',
      'wordsermod', 'Codigo', Codigo, DMod.MyDBn);
    Va(vpLast);
  end;
end;

procedure TFmWOrdSerMod.BtIncluiClick(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrWOrdSerMod, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'wordsermod');
end;

procedure TFmWOrdSerMod.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  GBEdita.Align   := alClient;
  GBDados.Align   := alClient;
  CriaOForm;
end;

procedure TFmWOrdSerMod.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrWOrdSerModCodigo.Value, LaRegistro.Caption);
end;

procedure TFmWOrdSerMod.SbImprimeClick(Sender: TObject);
begin
  Geral.MB_Info('Impress�o indispon�vel para esta janela!');
end;

procedure TFmWOrdSerMod.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmWOrdSerMod.SbNovoClick(Sender: TObject);
begin
  Geral.MB_Info('Tipo de pesquisa indispon�vel para esta janela!');
end;

procedure TFmWOrdSerMod.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmWOrdSerMod.QrCadastro_SimplesAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmWOrdSerMod.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  DmkWeb.CarregaImagemFormWEB(ImgWEB);
end;

procedure TFmWOrdSerMod.SbQueryClick(Sender: TObject);
begin
  LocCod(QrWOrdSerModCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'wordsermod', Dmod.MyDBn, CO_VAZIO));
end;

procedure TFmWOrdSerMod.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmWOrdSerMod.QrCadastro_SimplesBeforeOpen(DataSet: TDataSet);
begin
  QrWOrdSerModCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmWOrdSerMod.QrWOrdSerModAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmWOrdSerMod.QrWOrdSerModBeforeOpen(DataSet: TDataSet);
begin
  QrWOrdSerModCodigo.DisplayFormat := FFormatFloat;
end;

end.
