unit HelpGer;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkPermissoes,
  dmkDBLookupComboBox, dmkEditCB, dmkCompoStore, mySQLDbTables, Vcl.Menus,
  Xml.XMLIntf, Xml.XMLDoc;

type
  TFmHelpGer = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    PnAgendas: TPanel;
    PnTopicos: TPanel;
    BtTopInclui: TBitBtn;
    StaticText1: TStaticText;
    PnPesquisa: TPanel;
    Label4: TLabel;
    EdHelpCab: TdmkEditCB;
    CBHelpCab: TdmkDBLookupComboBox;
    SBHelpCab: TSpeedButton;
    CSTabSheetChamou: TdmkCompoStore;
    QrHelpCab: TmySQLQuery;
    QrHelpCabCodigo: TIntegerField;
    QrHelpCabNome: TWideStringField;
    QrHelpCabAtivo: TSmallintField;
    QrHelpCabLiberado: TIntegerField;
    DsHelpCab: TDataSource;
    TVTopicos: TTreeView;
    PMTopicos: TPopupMenu;
    Incluisubtpico1: TMenuItem;
    Altera1: TMenuItem;
    Exclui1: TMenuItem;
    N1: TMenuItem;
    Mover1: TMenuItem;
    BtTopReordena: TBitBtn;
    QrHelpTopic: TmySQLQuery;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    Panel6: TPanel;
    Panel5: TPanel;
    Label13: TLabel;
    SBHelpFAQ: TSpeedButton;
    BitBtn1: TBitBtn;
    dmkEdDescricao: TdmkEdit;
    Memo1: TMemo;
    BtSincro: TBitBtn;
    BtOpcoes: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure BtTopIncluiClick(Sender: TObject);
    procedure Incluisubtpico1Click(Sender: TObject);
    procedure Altera1Click(Sender: TObject);
    procedure Exclui1Click(Sender: TObject);
    procedure TVTopicosMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure EdHelpCabChange(Sender: TObject);
    procedure BtTopReordenaClick(Sender: TObject);
    procedure Mover1Click(Sender: TObject);
    procedure BtSincroClick(Sender: TObject);
    procedure BtOpcoesClick(Sender: TObject);
  private
    { Private declarations }
    procedure CriaNode(Child: Boolean);
    procedure EditaNode();
    procedure ExcluiNode();
    procedure MostraEdicao(HelpCab: Integer; Mostra: Integer);
    procedure ConfiguraJanela();
    procedure ReordenaTopicos();
    procedure ReopenHelpTopic(Codigo: Integer);
    procedure SincronizaTabelas();
  public
    { Public declarations }
  end;

  var
  FmHelpGer: TFmHelpGer;

implementation

uses MyGlyfs, Principal, Module, UnMyObjects, UnGrlHelp, UnitHelpJan, MyListas,
  UnTreeView, DmkDAC_PF, UnVCL_Sincro, UMySQLModule, UnDmkWeb_Jan, ModuleGeral;

{$R *.DFM}

procedure TFmHelpGer.Altera1Click(Sender: TObject);
begin
  EditaNode();
end;

procedure TFmHelpGer.BtTopIncluiClick(Sender: TObject);
begin
  CriaNode(False);
end;

procedure TFmHelpGer.BtTopReordenaClick(Sender: TObject);
begin
  ReordenaTopicos;
end;

procedure TFmHelpGer.BtOpcoesClick(Sender: TObject);
begin
  UnHelpJan.MostraFormHelpOpc();
end;

procedure TFmHelpGer.BtSaidaClick(Sender: TObject);
begin
  if TFmHelpGer(Self).Owner is TApplication then
    Close
  else
    MyObjects.FormTDIFecha(Self, TTabSheet(CSTabSheetChamou.Component));
end;

procedure TFmHelpGer.BtSincroClick(Sender: TObject);
begin
  //SincronizaTabelas;
  DmkWeb_Jan.MostraWSincro(CO_DMKID_APP, CO_Versao,
    DModG.QrMasterHabilModulos.Value, DmodG.QrOpcoesGerl, Dmod.MyDBn,
    Dmod.MyDB, DModG.QrControle.FieldByName('DtaSincro').AsDateTime, TMeuDB,
    VAR_AllID_DB_NOME, nil, ['helpcab', 'helpopc', 'helptopic', 'helpfaq',
    'helprestr', 'helprestip'], VAR_VERIFI_DB_CANCEL);
end;

procedure TFmHelpGer.FormActivate(Sender: TObject);
begin
  if TFmHelpGer(Self).Owner is TApplication then
  begin
    MyObjects.CorIniComponente();
  end;
end;

procedure TFmHelpGer.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType  := stLok;
  //
  ConfiguraJanela;
  //
  MostraEdicao(0, 0);
end;

procedure TFmHelpGer.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
    [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmHelpGer.FormShow(Sender: TObject);
begin
  FmMyGlyfs.DefineGlyfsTDI(FmPrincipal.sd1, Sender);
end;

procedure TFmHelpGer.Incluisubtpico1Click(Sender: TObject);
begin
  CriaNode(True);
end;

procedure TFmHelpGer.MostraEdicao(HelpCab: Integer; Mostra: Integer);
begin
  if Mostra = 0 then
  begin
    PnTopicos.Visible  := True;
    //
    if HelpCab = 0 then
    begin
      BtTopInclui.Enabled   := False;
      BtTopReordena.Enabled := False;
      //
      QrHelpTopic.Close;
      TVTopicos.Items.Clear;
    end else
    begin
      BtTopInclui.Enabled   := True;
      BtTopReordena.Enabled := True;
      //
      ReopenHelpTopic(HelpCab);
    end;
  end;
end;

procedure TFmHelpGer.Mover1Click(Sender: TObject);
begin
  ReordenaTopicos;
end;

procedure TFmHelpGer.ReopenHelpTopic(Codigo: Integer);
begin
  GrlHelp.ReopenHelpTopic(Dmod.MyDB, QrHelpTopic, Codigo);
  UTreeView.CarregaDBItensTreeView(QrHelpTopic, 'Controle', 'Nome', 'Nivel',
    TVTopicos, True);
end;

procedure TFmHelpGer.ReordenaTopicos;
begin
  if UTreeView.ReordenaNodes(TVTopicos, True) then
    UTreeView.AtualizaDBNodesOrdem(Dmod.MyDB, Dmod.QrAux, TVTopicos,
      'helptopic', 'Nivel', 'Ordem', 'Controle');
end;

procedure TFmHelpGer.SincronizaTabelas;
var
  Msg: String;
  Res: Boolean;
begin
  Res := VCL_Sincro.SincronizaTabelas(Dmod.MyDB, ['helpcab', 'helpfaq',
           'helpfaqhis', 'helptopic'], LaAviso1, LaAviso2, Msg);
  //
  if Res = False then
    Geral.MB_Aviso(Msg);
end;

procedure TFmHelpGer.TVTopicosMouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
var
  Node: TTreeNode;
begin
  //Seleciona com o bot�o direito do mouse. O RightClickSelect n�o funciona
  if Button = mbRight then
  begin
    Node := TVTopicos.GetNodeAt(X, Y);
    //
    if Node <> nil then
      Node.Selected := True;
  end;
end;

procedure TFmHelpGer.ConfiguraJanela;
begin
  CBHelpCab.ListSource := DsHelpCab;
  TVTopicos.PopupMenu  := PMTopicos;
  TVTopicos.ReadOnly   := True;
  //
  GrlHelp.ReopenHelpCab(Dmod.MyDB, QrHelpCab);
end;

procedure TFmHelpGer.CriaNode(Child: Boolean);
var
  Codigo, Controle: Integer;
  Txt: String;
  Node: TTreeNode;
begin
  if InputQuery(Application.Title, 'Informe o t�pico', Txt) then
  begin
    Codigo   := EdHelpCab.ValueVariant;
    Controle := 0;
    //
    if Child then
      Node := TVTopicos.Selected
    else
      Node := nil;
    //
    if GrlHelp.InsUpdAjudaTopico(Dmod.MyDB, Dmod.QrUpd, stDesktop, Txt, Codigo,
      1, Controle) then
    begin
      Node := UTreeView.CriaNode(Node, Txt, Controle, TVTopicos, -1, Child);
      Node.Selected := True;
      //
      UTreeView.AtualizaDBNodesOrdem(Dmod.MyDB, Dmod.QrAux, TVTopicos,
        'helptopic', 'Nivel', 'Ordem', 'Controle');
    end;
  end;
end;

procedure TFmHelpGer.EdHelpCabChange(Sender: TObject);
begin
  MostraEdicao(EdHelpCab.ValueVariant, 0);
end;

procedure TFmHelpGer.EditaNode;

  function ObtemCodigo(Controle: Integer): Integer;
  begin
    if QrHelpTopic.Locate('Controle', Controle, []) then
      Result := QrHelpTopic.FieldByName('Codigo').AsInteger
    else
      Result := 0;
  end;

var
  Codigo, Controle: Integer;
  Txt: String;
  Node: TTreeNode;
begin
  Txt  := '';
  Node := TVTopicos.Selected;
  //
  if Node <> nil then
  begin
    Txt := Node.Text;
    //
    if InputQuery(Application.Title, 'Informe o t�pico', Txt) then
    begin
      Controle := Node.StateIndex;
      Codigo   := ObtemCodigo(Controle);
      //
      if GrlHelp.InsUpdAjudaTopico(Dmod.MyDB, Dmod.QrUpd, stDesktop, Txt, Codigo,
        1, Controle) then
      begin
        UTreeView.EditaNode(TVTopicos, Node, Txt, 0);
      end;
    end;
  end;
end;

procedure TFmHelpGer.Exclui1Click(Sender: TObject);
begin
  ExcluiNode();
end;

procedure TFmHelpGer.ExcluiNode();
var
  Controle: Integer;
  Nome, Msg: String;
  Node: TTreeNode;
begin
  Node := TVTopicos.Selected;
  //
  if Node <> nil then
  begin
    Controle := Node.StateIndex;
    Nome     := Node.Text;
    Msg      := 'Confirma a exclus�o do t�pico: "' + Nome + '"?';
    //
    if UTreeView.ExcluiNode(TVTopicos, Msg) then
      GrlHelp.ExcluiAjudaTopico(Dmod.MyDB, Dmod.QrAux, Controle, '');
  end;
end;

end.
