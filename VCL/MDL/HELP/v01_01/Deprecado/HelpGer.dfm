object FmHelpGer: TFmHelpGer
  Left = 339
  Top = 185
  ActiveControl = BtTopInclui
  Caption = 'HLP-GEREN-001 :: Gerenciamento de Ajudas'
  ClientHeight = 774
  ClientWidth = 999
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -14
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 120
  TextHeight = 16
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 999
    Height = 59
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 940
      Top = 0
      Width = 59
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 10
        Top = 14
        Width = 39
        Height = 39
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 110
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alLeft
      TabOrder = 1
      object BtSincro: TBitBtn
        Tag = 73
        Left = 56
        Top = 5
        Width = 49
        Height = 49
        Cursor = crHandPoint
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSincroClick
      end
      object BtOpcoes: TBitBtn
        Tag = 348
        Left = 5
        Top = 5
        Width = 49
        Height = 49
        Cursor = crHandPoint
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        OnClick = BtOpcoesClick
      end
    end
    object GB_M: TGroupBox
      Left = 110
      Top = 0
      Width = 830
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 9
        Top = 11
        Width = 368
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Gerenciamento de Ajudas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 11
        Top = 14
        Width = 368
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Gerenciamento de Ajudas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 10
        Top = 12
        Width = 368
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Gerenciamento de Ajudas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 59
    Width = 999
    Height = 575
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 999
      Height = 575
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 999
        Height = 575
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        TabOrder = 0
        object PnAgendas: TPanel
          Left = 2
          Top = 18
          Width = 350
          Height = 555
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alLeft
          BevelOuter = bvNone
          TabOrder = 0
          object PnTopicos: TPanel
            Left = 0
            Top = 75
            Width = 350
            Height = 59
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alTop
            TabOrder = 0
            object BtTopInclui: TBitBtn
              Tag = 10
              Left = 5
              Top = 5
              Width = 49
              Height = 49
              Cursor = crHandPoint
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              NumGlyphs = 2
              ParentShowHint = False
              ShowHint = True
              TabOrder = 0
              OnClick = BtTopIncluiClick
            end
            object BtTopReordena: TBitBtn
              Tag = 73
              Left = 56
              Top = 5
              Width = 49
              Height = 49
              Cursor = crHandPoint
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              NumGlyphs = 2
              ParentShowHint = False
              ShowHint = True
              TabOrder = 1
              OnClick = BtTopReordenaClick
            end
          end
          object StaticText1: TStaticText
            Left = 0
            Top = 55
            Width = 350
            Height = 20
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alTop
            Alignment = taCenter
            BorderStyle = sbsSunken
            Caption = 'T'#243'picos'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -15
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 1
          end
          object PnPesquisa: TPanel
            Left = 0
            Top = 0
            Width = 350
            Height = 55
            Align = alTop
            TabOrder = 2
            object Label4: TLabel
              Left = 5
              Top = 5
              Width = 38
              Height = 16
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Ajuda:'
            end
            object SBHelpCab: TSpeedButton
              Left = 318
              Top = 25
              Width = 26
              Height = 25
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = '...'
            end
            object EdHelpCab: TdmkEditCB
              Left = 5
              Top = 25
              Width = 69
              Height = 25
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Alignment = taRightJustify
              TabOrder = 1
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              OnChange = EdHelpCabChange
              DBLookupComboBox = CBHelpCab
              IgnoraDBLookupComboBox = False
            end
            object CBHelpCab: TdmkDBLookupComboBox
              Left = 75
              Top = 25
              Width = 240
              Height = 24
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              KeyField = 'Codigo'
              ListField = 'Nome'
              ListSource = DsHelpCab
              TabOrder = 0
              dmkEditCB = EdHelpCab
              UpdType = utYes
              LocF7SQLMasc = '$#'
            end
          end
          object TVTopicos: TTreeView
            Left = 0
            Top = 134
            Width = 350
            Height = 421
            Align = alClient
            Indent = 19
            RightClickSelect = True
            TabOrder = 3
            OnMouseDown = TVTopicosMouseDown
          end
        end
        object PageControl1: TPageControl
          Left = 352
          Top = 18
          Width = 645
          Height = 555
          ActivePage = TabSheet1
          Align = alClient
          TabHeight = 25
          TabOrder = 1
          object TabSheet1: TTabSheet
            Caption = 'T'#243'pico'
          end
          object TabSheet2: TTabSheet
            Caption = 'Tutoriais'
            ImageIndex = 1
            object Panel6: TPanel
              Left = 0
              Top = 0
              Width = 350
              Height = 520
              Align = alLeft
              TabOrder = 0
              object Panel5: TPanel
                Left = 1
                Top = 1
                Width = 348
                Height = 59
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Align = alTop
                TabOrder = 0
                object Label13: TLabel
                  Left = 5
                  Top = 6
                  Width = 60
                  Height = 16
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Caption = 'Pesquisa:'
                end
                object SBHelpFAQ: TSpeedButton
                  Left = 258
                  Top = 25
                  Width = 26
                  Height = 26
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Caption = '...'
                end
                object BitBtn1: TBitBtn
                  Tag = 10
                  Left = 292
                  Top = 3
                  Width = 49
                  Height = 49
                  Cursor = crHandPoint
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  NumGlyphs = 2
                  ParentShowHint = False
                  ShowHint = True
                  TabOrder = 0
                end
                object dmkEdDescricao: TdmkEdit
                  Left = 5
                  Top = 25
                  Width = 250
                  Height = 26
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  TabOrder = 1
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  QryCampo = 'Descricao'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = ''
                  ValWarn = False
                end
              end
              object Memo1: TMemo
                Left = 1
                Top = 60
                Width = 348
                Height = 459
                Align = alClient
                Lines.Strings = (
                  'Memo1')
                TabOrder = 1
              end
            end
          end
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 634
    Width = 999
    Height = 54
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 18
      Width = 995
      Height = 34
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 16
        Top = 2
        Width = 150
        Height = 19
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -17
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 15
        Top = 1
        Width = 150
        Height = 19
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -17
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 688
    Width = 999
    Height = 86
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 820
      Top = 18
      Width = 177
      Height = 66
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 16
        Top = 5
        Width = 147
        Height = 49
        Cursor = crHandPoint
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 18
      Width = 818
      Height = 66
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 15
        Top = 5
        Width = 147
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
      end
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 608
    Top = 203
  end
  object CSTabSheetChamou: TdmkCompoStore
    Left = 460
    Top = 443
  end
  object QrHelpCab: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *'
      'FROM cadastro_simples')
    Left = 48
    Top = 240
    object QrHelpCabCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrHelpCabNome: TWideStringField
      DisplayWidth = 60
      FieldName = 'Nome'
      Size = 60
    end
    object QrHelpCabAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrHelpCabLiberado: TIntegerField
      FieldName = 'Liberado'
    end
  end
  object DsHelpCab: TDataSource
    DataSet = QrHelpCab
    Left = 76
    Top = 240
  end
  object PMTopicos: TPopupMenu
    Left = 160
    Top = 312
    object Incluisubtpico1: TMenuItem
      Caption = '&Inclui subt'#243'pico'
      OnClick = Incluisubtpico1Click
    end
    object Altera1: TMenuItem
      Caption = '&Altera'
      OnClick = Altera1Click
    end
    object Exclui1: TMenuItem
      Caption = '&Exclui'
      OnClick = Exclui1Click
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object Mover1: TMenuItem
      Caption = '&Mover'
      OnClick = Mover1Click
    end
  end
  object QrHelpTopic: TmySQLQuery
    Database = Dmod.MyDB
    Left = 56
    Top = 312
  end
end
