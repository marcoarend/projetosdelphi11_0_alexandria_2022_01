object FmWSuporte: TFmWSuporte
  Left = 339
  Top = 185
  Caption = 'WEB-SUPOR-001 :: Suporte'
  ClientHeight = 562
  ClientWidth = 584
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = True
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 584
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 496
      Top = 0
      Width = 88
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 48
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
      object ImgWEB: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 448
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 95
        Height = 32
        Caption = 'Suporte'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 95
        Height = 32
        Caption = 'Suporte'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 95
        Height = 32
        Caption = 'Suporte'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 584
    Height = 514
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 584
      Height = 514
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 584
        Height = 514
        Align = alClient
        TabOrder = 0
        object TabControl1: TTabControl
          Left = 2
          Top = 137
          Width = 580
          Height = 311
          Align = alClient
          MultiLine = True
          TabHeight = 30
          TabOrder = 0
          Tabs.Strings = (
            'Ajudas'
            'Atendimento eletr'#244'nico')
          TabIndex = 0
          OnChange = TabControl1Change
          object WebBrowser1: TWebBrowser
            Left = 4
            Top = 36
            Width = 572
            Height = 271
            Align = alClient
            TabOrder = 0
            OnProgressChange = WebBrowser1ProgressChange
            OnBeforeNavigate2 = WebBrowser1BeforeNavigate2
            ExplicitWidth = 566
            ExplicitHeight = 274
            ControlData = {
              4C0000001E3B0000021C00000000000000000000000000000000000000000000
              000000004C000000000000000000000001000000E0D057007335CF11AE690800
              2B2E12620A000000000000004C0000000114020000000000C000000000000046
              8000000000000000000000000000000000000000000000000000000000000000
              00000000000000000100000000000000000000000000000000000000}
          end
        end
        object GBCntrl: TGroupBox
          Left = 2
          Top = 448
          Width = 580
          Height = 64
          Align = alBottom
          TabOrder = 1
          object Panel1: TPanel
            Left = 2
            Top = 15
            Width = 576
            Height = 47
            Align = alClient
            BevelOuter = bvNone
            TabOrder = 0
            object Panel4: TPanel
              Left = 447
              Top = 0
              Width = 129
              Height = 47
              Align = alRight
              BevelOuter = bvNone
              TabOrder = 0
              object BtSaida: TBitBtn
                Tag = 13
                Left = 4
                Top = 4
                Width = 120
                Height = 40
                Cursor = crHandPoint
                Caption = '&Sa'#237'da'
                NumGlyphs = 2
                ParentShowHint = False
                ShowHint = True
                TabOrder = 0
                OnClick = BtSaidaClick
              end
            end
            object BtMenu: TBitBtn
              Tag = 237
              Left = 140
              Top = 4
              Width = 130
              Height = 40
              Cursor = crHandPoint
              Caption = '&Menu'
              NumGlyphs = 2
              ParentShowHint = False
              ShowHint = True
              TabOrder = 1
              OnClick = BtMenuClick
            end
            object BtInclui: TBitBtn
              Tag = 10
              Left = 4
              Top = 4
              Width = 130
              Height = 40
              Cursor = crHandPoint
              Caption = '&Inclui solicita'#231#227'o'
              NumGlyphs = 2
              ParentShowHint = False
              ShowHint = True
              TabOrder = 2
              OnClick = BtIncluiClick
            end
            object CkNavegPad: TCheckBox
              Left = 298
              Top = 0
              Width = 149
              Height = 47
              Align = alRight
              BiDiMode = bdLeftToRight
              Caption = 'Abrir no navegador padr'#227'o'
              Checked = True
              ParentBiDiMode = False
              State = cbChecked
              TabOrder = 3
              Visible = False
              OnClick = CkNavegPadClick
            end
          end
        end
        object PnMenu: TPanel
          Left = 2
          Top = 15
          Width = 580
          Height = 122
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 2
          object LaPesquisa: TLabel
            Left = 0
            Top = 0
            Width = 580
            Height = 20
            Align = alTop
            Caption = 'Digite aqui o que voc'#234' deseja pesquisar:'
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -16
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            ExplicitWidth = 284
          end
          object ProgressBar1: TProgressBar
            Left = 0
            Top = 105
            Width = 580
            Height = 17
            Align = alBottom
            TabOrder = 1
          end
          object EdPesquisar: TdmkEdit
            Left = 0
            Top = 20
            Width = 580
            Height = 30
            Align = alTop
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -16
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 0
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            QryCampo = 'RazaoSocial'
            UpdCampo = 'RazaoSocial'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object BtConfirma: TBitBtn
            Tag = 14
            Left = 10
            Top = 57
            Width = 120
            Height = 40
            Caption = '&Pesquisar'
            NumGlyphs = 2
            TabOrder = 2
            OnClick = BtConfirmaClick
          end
        end
      end
    end
  end
  object PMMenu: TPopupMenu
    Left = 128
    Top = 304
    object Ajudas1: TMenuItem
      Caption = 'Ajudas'
      OnClick = Ajudas1Click
    end
    object utoriais1: TMenuItem
      Caption = 'Tutoriais'
      OnClick = utoriais1Click
    end
    object Gerenciarsolicitaes1: TMenuItem
      Caption = 'Gerenciar solicita'#231#245'es'
      OnClick = Gerenciarsolicitaes1Click
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object Histricodeatualizaes2: TMenuItem
      Caption = 'Hist'#243'rico de atualiza'#231#245'es'
      OnClick = Histricodeatualizaes2Click
    end
    object GerenciarusuriosWEB1: TMenuItem
      Caption = 'Gerenciar usu'#225'rios WEB'
      OnClick = GerenciarusuriosWEB1Click
    end
    object Gerenciarminhaconta2: TMenuItem
      Caption = 'Gerenciar minha conta'
      OnClick = Gerenciarminhaconta2Click
    end
    object Licenas1: TMenuItem
      Caption = 'Licen'#231'as'
      OnClick = Licenas1Click
    end
    object N2: TMenuItem
      Caption = '-'
    end
    object Boletosemaberto1: TMenuItem
      Caption = 'Boletos em aberto'
      OnClick = Boletosemaberto1Click
    end
    object Boletosvencidos1: TMenuItem
      Caption = 'Boletos vencidos'
      OnClick = Boletosvencidos1Click
    end
    object Notasfiscais1: TMenuItem
      Caption = 'Notas fiscais'
      OnClick = Notasfiscais1Click
    end
    object N3: TMenuItem
      Caption = '-'
    end
    object Desbloquearjanela1: TMenuItem
      Caption = 'Desbloquear janela'
      OnClick = Desbloquearjanela1Click
    end
    object N6: TMenuItem
      Caption = '-'
    end
    object Desconectareouconectarcomusuriodiferente1: TMenuItem
      Caption = 'Desconectar e/ou conectar com usu'#225'rio diferente'
      OnClick = Desconectareouconectarcomusuriodiferente1Click
    end
  end
end
