object FmWOrdSerIncRap: TFmWOrdSerIncRap
  Left = 339
  Top = 185
  Caption = 'WEB-ORSER-017 :: Inclui Solicita'#231#227'o'
  ClientHeight = 730
  ClientWidth = 843
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 843
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 755
      Top = 0
      Width = 88
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 48
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
      object ImgWEB: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 707
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 204
        Height = 32
        Caption = 'Inclui Solicita'#231#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 204
        Height = 32
        Caption = 'Inclui Solicita'#231#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 204
        Height = 32
        Caption = 'Inclui Solicita'#231#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 843
    Height = 612
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 2
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 843
      Height = 612
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 843
        Height = 612
        Align = alClient
        TabOrder = 0
        object PageControl1: TPageControl
          Left = 2
          Top = 15
          Width = 839
          Height = 595
          ActivePage = TabSheet1
          Align = alClient
          TabHeight = 25
          TabOrder = 0
          ExplicitWidth = 840
          object TabSheet1: TTabSheet
            Caption = 'Solicita'#231#245'es'
            object Splitter1: TSplitter
              Left = 638
              Top = 0
              Width = 10
              Height = 567
              Align = alRight
            end
            object Panel5: TPanel
              Left = 0
              Top = 0
              Width = 638
              Height = 567
              Align = alClient
              BevelOuter = bvNone
              ParentBackground = False
              TabOrder = 0
              object Panel7: TPanel
                Left = 0
                Top = 158
                Width = 638
                Height = 440
                Align = alTop
                BevelOuter = bvNone
                TabOrder = 2
                object Label4: TLabel
                  Left = 7
                  Top = 4
                  Width = 249
                  Height = 13
                  Caption = 'Informa'#231#245'es do computador: (Ex: Windows 8 64 bits)'
                end
                object Label5: TLabel
                  Left = 7
                  Top = 44
                  Width = 264
                  Height = 13
                  Caption = 'Impacto: (No que esta solicita'#231#227'o afeta no seu trabalho)'
                end
                object Label6: TLabel
                  Left = 7
                  Top = 84
                  Width = 57
                  Height = 13
                  Caption = '* Prioridade:'
                end
                object LaPrioridade: TLabel
                  Left = 113
                  Top = 103
                  Width = 256
                  Height = 13
                  Caption = 'Descri'#231#227'o referente a prioridade selecionada'
                  Font.Charset = ANSI_CHARSET
                  Font.Color = clGreen
                  Font.Height = -12
                  Font.Name = 'MS Sans Serif'
                  Font.Style = [fsBold]
                  ParentFont = False
                  Visible = False
                end
                object Label7: TLabel
                  Left = 7
                  Top = 124
                  Width = 56
                  Height = 13
                  Caption = '* Aplicativo:'
                end
                object Label8: TLabel
                  Left = 215
                  Top = 124
                  Width = 36
                  Height = 13
                  Caption = 'Vers'#227'o:'
                end
                object Label9: TLabel
                  Left = 423
                  Top = 124
                  Width = 34
                  Height = 13
                  Caption = 'Janela:'
                end
                object Label12: TLabel
                  Left = 7
                  Top = 164
                  Width = 92
                  Height = 13
                  Caption = 'Janela relacionada:'
                end
                object Label13: TLabel
                  Left = 216
                  Top = 162
                  Width = 127
                  Height = 13
                  Caption = 'Item da janela relacionada:'
                end
                object EdSistema: TdmkEdit
                  Left = 7
                  Top = 21
                  Width = 621
                  Height = 21
                  TabOrder = 0
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = ''
                  ValWarn = False
                end
                object EdImpacto: TdmkEdit
                  Left = 6
                  Top = 61
                  Width = 621
                  Height = 21
                  TabOrder = 1
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = ''
                  ValWarn = False
                end
                object CBPrioridade: TComboBox
                  Left = 7
                  Top = 100
                  Width = 102
                  Height = 24
                  Style = csDropDownList
                  TabOrder = 2
                  OnChange = CBPrioridadeChange
                end
                object CBAplicativo: TComboBox
                  Left = 7
                  Top = 140
                  Width = 200
                  Height = 24
                  Style = csDropDownList
                  TabOrder = 3
                  OnChange = CBAplicativoChange
                end
                object CBVersao: TComboBox
                  Left = 215
                  Top = 140
                  Width = 200
                  Height = 24
                  TabOrder = 4
                end
                object EdJanela: TdmkEdit
                  Left = 423
                  Top = 140
                  Width = 204
                  Height = 21
                  TabOrder = 5
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = ''
                  ValWarn = False
                end
                object EdJanelaRel: TdmkEdit
                  Left = 7
                  Top = 180
                  Width = 200
                  Height = 21
                  TabOrder = 6
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = ''
                  ValWarn = False
                end
                object EdCompoRel: TdmkEdit
                  Left = 215
                  Top = 180
                  Width = 412
                  Height = 21
                  TabOrder = 7
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = ''
                  ValWarn = False
                end
              end
              object Panel8: TPanel
                Left = 0
                Top = 0
                Width = 635
                Height = 50
                Align = alTop
                BevelOuter = bvNone
                TabOrder = 0
                ExplicitWidth = 638
                object LaSolicit: TLabel
                  Left = 6
                  Top = 4
                  Width = 48
                  Height = 13
                  Caption = '* Assunto:'
                end
                object CBAssunto: TComboBox
                  Left = 7
                  Top = 23
                  Width = 622
                  Height = 24
                  Style = csDropDownList
                  TabOrder = 0
                  OnChange = CBAssuntoChange
                end
              end
              object Panel11: TPanel
                Left = 0
                Top = 50
                Width = 635
                Height = 108
                Align = alTop
                BevelOuter = bvNone
                TabOrder = 1
                ExplicitWidth = 638
                object LaMsg: TLabel
                  Left = 0
                  Top = 0
                  Width = 58
                  Height = 13
                  Align = alTop
                  Caption = '* Descri'#231#227'o:'
                end
                object MeMsg: TdmkMemo
                  Left = 0
                  Top = 13
                  Width = 638
                  Height = 94
                  Align = alClient
                  ScrollBars = ssVertical
                  TabOrder = 0
                  UpdType = utYes
                end
              end
              object Panel6: TPanel
                Left = 0
                Top = 365
                Width = 635
                Height = 155
                Align = alBottom
                BevelOuter = bvNone
                TabOrder = 3
                ExplicitTop = 372
                ExplicitWidth = 638
                object Grade: TStringGrid
                  Left = 0
                  Top = 48
                  Width = 638
                  Height = 107
                  Align = alClient
                  ColCount = 2
                  DefaultRowHeight = 18
                  RowCount = 2
                  Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goColMoving]
                  TabOrder = 0
                  ColWidths = (
                    13
                    454)
                  RowHeights = (
                    18
                    18)
                end
                object Panel10: TPanel
                  Left = 0
                  Top = 0
                  Width = 638
                  Height = 48
                  Align = alTop
                  ParentBackground = False
                  TabOrder = 1
                  object BitBtn2: TBitBtn
                    Tag = 10
                    Left = 3
                    Top = 4
                    Width = 40
                    Height = 40
                    Cursor = crHandPoint
                    NumGlyphs = 2
                    ParentShowHint = False
                    ShowHint = True
                    TabOrder = 0
                    OnClick = BitBtn2Click
                  end
                  object BitBtn3: TBitBtn
                    Tag = 12
                    Left = 44
                    Top = 4
                    Width = 40
                    Height = 40
                    Cursor = crHandPoint
                    NumGlyphs = 2
                    ParentShowHint = False
                    ShowHint = True
                    TabOrder = 1
                    OnClick = BitBtn3Click
                  end
                  object CkPrint: TCheckBox
                    Left = 90
                    Top = 15
                    Width = 150
                    Height = 17
                    Caption = 'Enviar captura da tela'
                    TabOrder = 2
                    OnClick = CkPrintClick
                  end
                end
              end
              object GBAvisos1: TGroupBox
                Left = 0
                Top = 520
                Width = 635
                Height = 40
                Align = alBottom
                Caption = ' Avisos: '
                TabOrder = 4
                ExplicitTop = 527
                ExplicitWidth = 638
                object Panel4: TPanel
                  Left = 2
                  Top = 15
                  Width = 634
                  Height = 23
                  Align = alClient
                  BevelOuter = bvNone
                  TabOrder = 0
                  object LaAviso1: TLabel
                    Left = 13
                    Top = 2
                    Width = 120
                    Height = 17
                    Caption = '..............................'
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clSilver
                    Font.Height = -15
                    Font.Name = 'Arial'
                    Font.Style = []
                    ParentFont = False
                    Transparent = True
                  end
                  object LaAviso2: TLabel
                    Left = 12
                    Top = 1
                    Width = 120
                    Height = 17
                    Caption = '..............................'
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clRed
                    Font.Height = -15
                    Font.Name = 'Arial'
                    Font.Style = []
                    ParentFont = False
                    Transparent = True
                  end
                end
              end
            end
            object PnBrowser: TPanel
              Left = 645
              Top = 0
              Width = 186
              Height = 560
              Align = alRight
              BevelOuter = bvNone
              TabOrder = 1
              ExplicitLeft = 648
              ExplicitHeight = 567
              object WebBrowser1: TWebBrowser
                Left = 0
                Top = 16
                Width = 186
                Height = 404
                Align = alClient
                TabOrder = 0
                ControlData = {
                  4C000000EF120000182900000000000000000000000000000000000000000000
                  000000004C000000000000000000000001000000E0D057007335CF11AE690800
                  2B2E126208000000000000004C0000000114020000000000C000000000000046
                  8000000000000000000000000000000000000000000000000000000000000000
                  00000000000000000100000000000000000000000000000000000000}
              end
              object PnImg: TPanel
                Left = 0
                Top = 420
                Width = 186
                Height = 147
                Align = alBottom
                BevelOuter = bvNone
                TabOrder = 1
                object ImgPrint: TImage
                  Left = 0
                  Top = 16
                  Width = 186
                  Height = 131
                  Cursor = crHandPoint
                  Align = alClient
                  Center = True
                  Proportional = True
                  OnClick = ImgPrintClick
                end
                object StaticText1: TStaticText
                  Left = 0
                  Top = 0
                  Width = 91
                  Height = 17
                  Align = alTop
                  Alignment = taCenter
                  BorderStyle = sbsSunken
                  Caption = 'Captura da tela'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -12
                  Font.Name = 'MS Sans Serif'
                  Font.Style = [fsBold]
                  ParentFont = False
                  TabOrder = 0
                end
              end
              object StaticText8: TStaticText
                Left = 0
                Top = 0
                Width = 61
                Height = 17
                Align = alTop
                Alignment = taCenter
                BorderStyle = sbsSunken
                Caption = 'Descri'#231#227'o'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -12
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                ParentFont = False
                TabOrder = 2
              end
            end
          end
        end
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 660
    Width = 843
    Height = 70
    Align = alBottom
    TabOrder = 1
    object PnSaiDesis: TPanel
      Left = 698
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 696
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtEnviar: TBitBtn
        Tag = 14
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Caption = '&Enviar'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtEnviarClick
      end
      object BtRascunho: TBitBtn
        Tag = 10006
        Left = 138
        Top = 3
        Width = 120
        Height = 40
        Caption = '&Rascunho'
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtRascunhoClick
      end
      object CkAceito: TCheckBox
        Left = 264
        Top = 14
        Width = 250
        Height = 17
        Caption = 'Aceito os termos mencionados nesta janela'
        TabOrder = 2
      end
    end
  end
  object VUAplic: TdmkValUsu
    QryCampo = 'Aplicativo'
    RefCampo = 'Codigo'
    UpdType = utYes
    Left = 299
    Top = 11
  end
  object OpenDialog1: TOpenDialog
    Options = [ofHideReadOnly, ofAllowMultiSelect, ofEnableSizing]
    Left = 271
    Top = 11
  end
  object PMRascunho: TPopupMenu
    Left = 336
    Top = 216
    object Salvar1: TMenuItem
      Caption = '&Salva'
      OnClick = Salvar1Click
    end
    object Exclui1: TMenuItem
      Caption = '&Exclui'
      OnClick = Exclui1Click
    end
  end
  object XMLDocument1: TXMLDocument
    Left = 240
    Top = 160
    DOMVendorDesc = 'MSXML'
  end
end
