unit WOrdSerIncRap;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, ActiveX,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnInternalConsts,
  dmkImage, dmkValUsu, dmkMemo, Variants, OleCtrls, SHDocVw, DmkDAC_PF, Menus,
  xmldom, XMLIntf, msxmldom, XMLDoc, UnDmkEnums, UnProjGroup_Consts;

type
  TFmWOrdSerIncRap = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtEnviar: TBitBtn;
    LaTitulo1C: TLabel;
    VUAplic: TdmkValUsu;
    OpenDialog1: TOpenDialog;
    ImgWEB: TdmkImage;
    BtRascunho: TBitBtn;
    PMRascunho: TPopupMenu;
    Exclui1: TMenuItem;
    Salvar1: TMenuItem;
    XMLDocument1: TXMLDocument;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    Panel5: TPanel;
    Panel7: TPanel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    LaPrioridade: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    EdSistema: TdmkEdit;
    EdImpacto: TdmkEdit;
    CBPrioridade: TComboBox;
    CBAplicativo: TComboBox;
    CBVersao: TComboBox;
    EdJanela: TdmkEdit;
    EdJanelaRel: TdmkEdit;
    EdCompoRel: TdmkEdit;
    Panel8: TPanel;
    LaSolicit: TLabel;
    CBAssunto: TComboBox;
    Panel11: TPanel;
    LaMsg: TLabel;
    MeMsg: TdmkMemo;
    Panel6: TPanel;
    Grade: TStringGrid;
    Panel10: TPanel;
    BitBtn2: TBitBtn;
    BitBtn3: TBitBtn;
    CkPrint: TCheckBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    PnBrowser: TPanel;
    WebBrowser1: TWebBrowser;
    PnImg: TPanel;
    ImgPrint: TImage;
    StaticText1: TStaticText;
    StaticText8: TStaticText;
    Splitter1: TSplitter;
    CkAceito: TCheckBox;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure BitBtn3Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure BtEnviarClick(Sender: TObject);
    procedure CkPrintClick(Sender: TObject);
    procedure Salvar1Click(Sender: TObject);
    procedure Exclui1Click(Sender: TObject);
    procedure BtRascunhoClick(Sender: TObject);
    procedure CBAplicativoChange(Sender: TObject);
    procedure CBAssuntoChange(Sender: TObject);
    procedure CBPrioridadeChange(Sender: TObject);
    procedure ImgPrintClick(Sender: TObject);
  private
    { Private declarations }
    FWOrdSerAssCodigo, FWOrdSerAssNome, FWOrdSerPriCodigo, FWOrdSerPriNome,
    FWOrdSerPriDescri, FAplicCodigo, FAplicNome, FVersao: TStringList;
    //
    procedure ReopenWOrdSerAss_REST();
    procedure ReopenWOrdSerPri_REST();
    procedure ReopenWOrdSerApl_REST();
    procedure ReopenWOrdSerVer_REST(Aplicativo: Integer);
    //
    {$IfNDef NO_USE_MYSQLMODULE}
    function VerificasSeTabelaExiste(Tabela: String): Boolean;
    procedure ExcluiSalvo();
    {$EndIf}
    procedure MostraEdicao(Mostra: Boolean; SQLType: TSQLType);
    procedure AdicionaAnexo(Caminho: String);
  public
    { Public declarations }
    FPrntScrnImg, FJanela, FAncora, FSisInfo: String;
  end;

  var
    FmWOrdSerIncRap: TFmWOrdSerIncRap;
  const
    FURL = 'http://www.dermatek.net.br/dmkREST.php';
    FCodApl = 31;


implementation

uses UnMyObjects, UnDmkWeb, UMySQLModule, Module, MyListas, UnGOTOy,
  {$IfNDef NO_USE_MYSQLMODULE} ModuleGeral, UCreate, {$EndIf}
  UnFTP, UnGrl_Vars;

const
  FMaxAnexos = 3;

{$R *.DFM}

procedure TFmWOrdSerIncRap.ReopenWOrdSerApl_REST;
  function SQLReopenAplicativos(Tipo, Cliente: Integer): String;
  var
    SQL: String;
  begin
    if Cliente <> 0 then
    begin
      if Tipo in [2, 3] then
      begin
        SQL := 'SELECT apl.Codigo, apl.Nome ';
        SQL := SQL + 'FROM cliaplic cap ';
        SQL := SQL + 'LEFT JOIN aplicativos apl ON apl.Codigo = cap.Aplicativo ';
        SQL := SQL + 'WHERE cap.Codigo = "' + Geral.FF0(Cliente) + '"';
      end else
      begin
        SQL := 'SELECT apl.Codigo, apl.Nome ';
        SQL := SQL + 'FROM cliaplic cap ';
        SQL := SQL + 'LEFT JOIN clientes cli ON cli.Codigo = cap.Codigo ';
        SQL := SQL + 'LEFT JOIN aplicativos apl ON apl.Codigo = cap.Aplicativo ';
        SQL := SQL + 'WHERE cli.Cliente = "' + Geral.FF0(Cliente) + '"';
      end;
    end else
    begin
      SQL := 'SELECT Codigo, Nome ';
      SQL := SQL + 'FROM aplicativos ';
      SQL := SQL + 'WHERE Ativo = 1 ';
      SQL := SQL + 'ORDER BY Nome';
    end;
    Result := SQL;
  end;
var
  XML: TStringStream;
  i: Integer;
  Codigo, Nome: String;
begin
  XML    := TStringStream.Create('');
  Codigo := '';
  Nome   := '';
  try
    CBAplicativo.Items.Clear;
    //
    if DmkWeb.URLPost(FURL,
      [
      'method=REST_Dmk_ObtemResultadoSQL',
      'AplicativoSolicitante=' + Geral.FF0(FCodApl),
      'IDUsuarioSolicitante=' + Geral.FF0(VAR_WEB_USR_ID),
      'Token=' + VAR_WEB_IDAPI,
      'Titulo=aplicativos',
      'SQLType=' + Geral.FF0(6),
      'SQL=' + SQLReopenAplicativos(VAR_WEB_USR_TIPO, VAR_WEB_CLIUSER)
      ], XML) then
    begin
      XMLDocument1.LoadFromStream(XML);
      //
      for i := 0 to XMLDocument1.DocumentElement.ChildNodes.Count - 1 do
      begin
        with XMLDocument1.DocumentElement.ChildNodes[i] do
        begin
          Codigo := ChildNodes['Codigo'].Text;
          Nome   := ChildNodes['Nome'].Text;
          //
          FAplicCodigo.Add(Codigo);
          FAplicNome.Add(Nome);
          CBAplicativo.Items.Add(Nome);
        end;
      end;
    end;
  finally
    XML.Free;
  end;
end;

procedure TFmWOrdSerIncRap.ReopenWOrdSerAss_REST;
var
  XML: TStringStream;
  i: Integer;
  Codigo, Nome: String;
begin
  XML    := TStringStream.Create('');
  Codigo := '';
  Nome   := '';
  try
    CBAssunto.Items.Clear;
    //
    if DmkWeb.URLPost(FURL,
      [
      'method=REST_Dmk_SQLWordSerAss',
      'AplicativoSolicitante=' + Geral.FF0(FCodApl),
      'IDUsuarioSolicitante=' + Geral.FF0(VAR_WEB_USR_ID),
      'Token=' + VAR_WEB_IDAPI,
      'Titulo=wordserass',
      'SQLType=' + Geral.FF0(6)
      ], XML) then
    begin
      XMLDocument1.LoadFromStream(XML);
      //
      for i := 0 to XMLDocument1.DocumentElement.ChildNodes.Count - 1 do
      begin
        with XMLDocument1.DocumentElement.ChildNodes[i] do
        begin
          Codigo := ChildNodes['Codigo'].Text;
          Nome   := ChildNodes['Nome'].Text;
          //
          FWOrdSerAssCodigo.Add(Codigo);
          FWOrdSerAssNome.Add(Nome);
          CBAssunto.Items.Add(Nome);
        end;
      end;
    end;
  finally
    XML.Free;
  end;
end;

procedure TFmWOrdSerIncRap.ReopenWOrdSerPri_REST;
var
  XML: TStringStream;
  i: Integer;
  Codigo, Nome, Descri: String;
begin
  XML    := TStringStream.Create('');
  Codigo := '';
  Nome   := '';
  Descri := '';
  try
    CBPrioridade.Items.Clear;
    //
    if DmkWeb.URLPost(FURL,
      [
      'method=REST_Dmk_ObtemResultadoSQL',
      'AplicativoSolicitante=' + Geral.FF0(FCodApl),
      'IDUsuarioSolicitante=' + Geral.FF0(VAR_WEB_USR_ID),
      'Token=' + VAR_WEB_IDAPI,
      'Titulo=wordserpri',
      'SQLType=' + Geral.FF0(6),
      'SQL=SELECT Codigo, Nome, Descri FROM wordserpri WHERE Ativo = 1 ORDER BY Nome'
      ], XML) then
    begin
      XMLDocument1.LoadFromStream(XML);
      //
      for i := 0 to XMLDocument1.DocumentElement.ChildNodes.Count - 1 do
      begin
        with XMLDocument1.DocumentElement.ChildNodes[i] do
        begin
          Codigo := ChildNodes['Codigo'].Text;
          Nome   := ChildNodes['Nome'].Text;
          Descri := ChildNodes['Descri'].Text;
          //
          FWOrdSerPriCodigo.Add(Codigo);
          FWOrdSerPriDescri.Add(Descri);
          FWOrdSerPriNome.Add(Nome);
          CBPrioridade.Items.Add(Nome);
        end;
      end;
    end;
  finally
    XML.Free;
  end;
end;

procedure TFmWOrdSerIncRap.ReopenWOrdSerVer_REST(Aplicativo: Integer);
var
  XML: TStringStream;
  i: Integer;
  Versao: String;
begin
  XML    := TStringStream.Create('');
  Versao := '';
  try
    CBVersao.Items.Clear;
    //
    if Aplicativo <> 0 then
    begin
      if DmkWeb.URLPost(FURL,
        [
        'method=REST_Dmk_ObtemResultadoSQL',
        'AplicativoSolicitante=' + Geral.FF0(FCodApl),
        'IDUsuarioSolicitante=' + Geral.FF0(VAR_WEB_USR_ID),
        'Token=' + VAR_WEB_IDAPI,
        'Titulo=Versoes',
        'SQLType=' + Geral.FF0(6),
        'SQL=SELECT Versao FROM wversao WHERE Aplicativo=' + Geral.FF0(Aplicativo) + ' AND Versao <> "" GROUP BY Versao ORDER BY Versao DESC'
        ], XML) then
      begin
        XMLDocument1.LoadFromStream(XML);
        //
        for i := 0 to XMLDocument1.DocumentElement.ChildNodes.Count - 1 do
        begin
          with XMLDocument1.DocumentElement.ChildNodes[i] do
          begin
            Versao := ChildNodes['Versao'].Text;
            //
            FVersao.Add(Versao);
            CBVersao.Items.Add(Versao);
          end;
        end;
      end;
    end;
  finally
    XML.Free;
  end;
end;

procedure TFmWOrdSerIncRap.Salvar1Click(Sender: TObject);
{$IfNDef NO_USE_MYSQLMODULE}
var
  Assunto, Prioridade, Aplicativo: Integer;
  Descri, Sistema, Impacto, Versao, Janela, JanelaRel, CompoRel: String;
{$EndIf}
begin
{$IfNDef NO_USE_MYSQLMODULE}
  try
    Screen.Cursor := crHourGlass;
    //
    if CBAssunto.ItemIndex <> -1 then
      Assunto := Geral.IMV(FWOrdSerAssCodigo[CBAssunto.ItemIndex])
    else
      Assunto := 0;
    if CBPrioridade.ItemIndex <> -1 then
      Prioridade := Geral.IMV(FWOrdSerPriCodigo[CBPrioridade.ItemIndex])
    else
      Prioridade := 0;
    if CBAplicativo.ItemIndex <> -1 then
      Aplicativo := Geral.IMV(FAplicCodigo[CBAplicativo.ItemIndex])
    else
      Aplicativo := 0;
    //
    Descri     := MeMsg.Text;
    Sistema    := EdSistema.ValueVariant;
    Impacto    := EdImpacto.ValueVariant;
    Versao     := CBVersao.Text;
    Janela     := EdJanela.ValueVariant;
    JanelaRel  := EdJanelaRel.ValueVariant;
    CompoRel   := EdCompoRel.ValueVariant;
    //
    ExcluiSalvo();
    //
    DModG.QrUpdPID1.SQL.Clear;
    DModG.QrUpdPID1.SQL.Add('INSERT INTO _wordsertmp_ SET');
    DModG.QrUpdPID1.SQL.Add('WebUser=:P00, Aplic=:P01, Versao=:P02,');
    DModG.QrUpdPID1.SQL.Add('Assunto=:P03, Descri=:P04, Janela=:P05,');
    DModG.QrUpdPID1.SQL.Add('JanelaRel=:P06, CompoRel=:P07, Prioridade=:P08, ');
    DModG.QrUpdPID1.SQL.Add('Sistema=:P09, Impacto=:P10 ');
    DModG.QrUpdPID1.Params[00].AsInteger := VAR_WEB_USR_ID;
    DModG.QrUpdPID1.Params[01].AsInteger := Aplicativo;
    DModG.QrUpdPID1.Params[02].AsString  := Versao;
    DModG.QrUpdPID1.Params[03].AsInteger := Assunto;
    DModG.QrUpdPID1.Params[04].AsString  := Descri;
    DModG.QrUpdPID1.Params[05].AsString  := Janela;
    DModG.QrUpdPID1.Params[06].AsString  := JanelaRel;
    DModG.QrUpdPID1.Params[07].AsString  := CompoRel;
    DModG.QrUpdPID1.Params[08].AsInteger := Prioridade;
    DModG.QrUpdPID1.Params[09].AsString  := Sistema;
    DModG.QrUpdPID1.Params[10].AsString  := Impacto;
    DModG.QrUpdPID1.ExecSQL;
  finally
    Screen.Cursor := crDefault;
    Close;
  end;
{$EndIf}
end;

{$IfNDef NO_USE_MYSQLMODULE}
function TFmWOrdSerIncRap.VerificasSeTabelaExiste(Tabela: String): Boolean;
begin
  Result := False;
  //
  DModG.QrAuxPID1.Close;
  DModG.QrAuxPID1.Database := DModG.MyPID_DB;
  DModG.QrAuxPID1.SQL.Clear;
  DModG.QrAuxPID1.SQL.Add('SHOW TABLES LIKE "'+ Tabela +'"');
  UnDMkDAC_PF.AbreQuery(DModG.QrAuxPID1, DModG.MyPID_DB);
  //
  if DModG.QrAuxPID1.RecordCount > 0 then
  begin
    DModG.QrAuxPID1.Close;
    DModG.QrAuxPID1.Database := DModG.MyPID_DB;
    DModG.QrAuxPID1.SQL.Clear;
    DModG.QrAuxPID1.SQL.Add('SHOW FIELDS FROM '+ Tabela +' LIKE "Sistema"');
    UnDMkDAC_PF.AbreQuery(DModG.QrAuxPID1, DModG.MyPID_DB);
    //
    if DModG.QrAuxPID1.RecordCount > 0 then
      Result := True;
  end;
end;
{$EndIf}

{$IfNDef NO_USE_MYSQLMODULE}
procedure TFmWOrdSerIncRap.ExcluiSalvo;
begin
  DModG.QrUpdPID1.SQL.Clear;
  DModG.QrUpdPID1.SQL.Add(DELETE_FROM + ' _wordsertmp_ WHERE WebUser=' + Geral.FF0(VAR_WEB_USR_ID));
  DModG.QrUpdPID1.ExecSQL;
end;
{$EndIf}

procedure TFmWOrdSerIncRap.Exclui1Click(Sender: TObject);
begin
{$IfNDef NO_USE_MYSQLMODULE}
  try
    Screen.Cursor := crHourGlass;
    //
    if VerificasSeTabelaExiste('_wordsertmp_') then
      ExcluiSalvo;
  finally
    Screen.Cursor := crDefault;
    Close;
  end;
{$EndIf}
end;

procedure TFmWOrdSerIncRap.AdicionaAnexo(Caminho: String);
var
  Row: Integer;
begin
  if Length(Caminho) > 0 then
  begin
    if FileExists(Caminho) then
    begin
      if (FMaxAnexos + 1) > Grade.RowCount then
      begin
        if (Grade.RowCount = 2) and (Length(Grade.Cells[1, 1]) = 0) then
        begin
          Grade.Cells[1, 1] := Caminho;
        end else
        begin
          Row := Grade.RowCount + 1;
          //
          Grade.Cells[1, Row - 1] := Caminho;
          Grade.RowCount := Row;
        end;
      end else
      begin
        Geral.MB_Aviso('O limite m�ximo de anexos � de ' +
          FormatFloat('0', FMaxAnexos) + ' itens!');
      end;
    end;
  end;
end;

procedure TFmWOrdSerIncRap.BitBtn2Click(Sender: TObject);
var
  Arquivo: String;
begin
  if OpenDialog1.Execute then
  begin
    Arquivo := OpenDialog1.FileName;
    //
    AdicionaAnexo(Arquivo);
  end;
end;

procedure TFmWOrdSerIncRap.BitBtn3Click(Sender: TObject);
begin
  MyObjects.ExcluiLinhaStringGrid(Grade);
end;

procedure TFmWOrdSerIncRap.BtEnviarClick(Sender: TObject);
  function UploadAnexo(Caminho: String; CodIncl: Integer): Boolean;
  var
    CliInt, FTPConfig, FTPDir, FTPWebArq, Passivo: Integer;
    Tam: Double;
    PastaCam, Host, User, Pass, ArqNome, Desc, Ext, Msg: String;
    UplAnx: Boolean;
    Arquivos: TStringList;
  begin
    UplAnx := False;
    //
    //Obtem dados do diret�rio
    if DmkWeb.FTP_ObtemDadosFTPDir_SOAP(FCodApl, 0, CliInt, FTPConfig, FTPDir, PastaCam) then
    begin
      if DmkWeb.FTP_ObtemDadosFTPConf_SOAP(FCodApl, FTPConfig, Passivo, Host, User, Pass) then
      begin
        Tam := DmkWeb.TamanhoArquivoBytes(Caminho); // Tamanho do arquivo para upload (apenas informativo)
        Ext := Copy(LowerCase(ExtractFileExt(Caminho)), 2); // Extens�o do arquivo para upload (apenas informativo)
        //
        try
          Arquivos := TStringList.Create;
          Arquivos.Add(Caminho);
          //
          //Executa Upload
          ArqNome := UFTP.UploadFTP(PageControl1, nil, PastaCam, Host, User,
                       Pass, Geral.IntToBool(Passivo), False, Arquivos, True);
          //
          if ArqNome <> '' then
          begin
            //Salva no banco de dados WEB
            Desc := ChangeFileExt(ArqNome, '');
            //
            FTPWebArq := DmkWeb.FTP_InsUpdRegistroFTPArquivo(FCodApl, dtIns, CliInt, 0,
                           0, FTPDir, Geral.FFF(Tam, 3), Desc, ArqNome, 1, 1, 0, Msg);
            //
            if FTPWebArq > 0 then
            begin
              if DmkWeb.InsereWOrdSerArq(FCodApl, CodIncl, FTPWebArq, Msg) > 0 then
                UplAnx := True;
            end;
          end;
        finally
          Arquivos.Free;
        end;
      end;
    end;
    if not UplAnx then
      Geral.MB_Aviso('Falha ao enviar anexo!' + sLineBreak + 'O arquivo ' +
        ExtractFileName(Caminho) + ' n�o foi enviado!');
    Result := UplAnx;
  end;
var
  Assunto, Prioridade, Aplicativo, Resul, i, CodIncl: Integer;
  Descri, Sistema, Impacto, Versao, Janela, JanelaRel, CompoRel, Caminho, Mensagem: String;
begin
  if MyObjects.FIC(not CkAceito.Checked, CkAceito,
    'Para criar uma solicita��o voc� deve aceitar os termos!') then Exit;
  //
  try
    BtEnviar.Enabled := False;
    Descri           := MeMsg.Text;
    Sistema          := EdSistema.ValueVariant;
    Impacto          := EdImpacto.ValueVariant;
    Versao           := CBVersao.Text;
    Janela           := EdJanela.ValueVariant;
    JanelaRel        := EdJanelaRel.ValueVariant;
    CompoRel         := EdCompoRel.ValueVariant;
    Mensagem         := '';
    //
    if MyObjects.FIC(CBAssunto.ItemIndex < 0, CBAssunto, 'Assunto n�o definido!') then Exit;
    if MyObjects.FIC(Descri = '', MeMsg, 'Descri��o n�o definida!') then Exit;
    if MyObjects.FIC(CBPrioridade.ItemIndex < 0, CBPrioridade, 'Defina a prioridade!') then Exit;
    if MyObjects.FIC(CBAplicativo.ItemIndex < 0, CBAplicativo, 'Defina o aplicativo!') then Exit;
    //
    Assunto    := Geral.IMV(FWOrdSerAssCodigo[CBAssunto.ItemIndex]);
    Prioridade := Geral.IMV(FWOrdSerPriCodigo[CBPrioridade.ItemIndex]);
    Aplicativo := Geral.IMV(FAplicCodigo[CBAplicativo.ItemIndex]);
    try
      Screen.Cursor := crHourGlass;
      //
      Resul := DmkWeb.WOrdSerInclui(FCodApl, 0, VAR_WEB_USR_ENT, VAR_WEB_USR_ID,
        Assunto, Prioridade, 0, 0, Aplicativo, 0, 0, 0, '0000-00-00 00:00',
        Descri, Sistema, Impacto, Versao, Janela, JanelaRel, CompoRel, '',
        CodIncl, Mensagem);
      if Resul > 0 then
      begin
        if Resul = 103 then
          Geral.MB_Aviso(Mensagem);
        //
        //Executa Upload dos arquivos
        if (Grade.RowCount > 2) or ((Grade.RowCount = 2) and
          (Length(Grade.Cells[1, 1]) > 0)) then
        begin
          MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Realizando upload dos arquivos');
          //
          for i := 1 to Grade.RowCount - 1 do
          begin
            Caminho := Grade.Cells[1, i];
            //
            UploadAnexo(Caminho, CodIncl);
          end;
        end;
        if (CkPrint.Checked) and (Length(FPrntScrnImg) > 0) and (FileExists(FPrntScrnImg)) then
        begin
          MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Realizando upload dos arquivos');
          //
          UploadAnexo(FPrntScrnImg, CodIncl);
        end;
      end else
        Geral.MB_Aviso('Falha ao incluir solicita��o tente novamente mais tarde!');
    finally
      {$IfNDef NO_USE_MYSQLMODULE}
      ExcluiSalvo();
      {$EndIf}
      Screen.Cursor := crDefault;
    end;
    if CodIncl <> 0 then
    begin
      if Geral.MB_Pergunta(Mensagem + sLineBreak + 'Deseja continuar inserindo?') <> ID_YES
      then
        Close;
      MeMsg.Text := '';
    end else
      Geral.MB_Aviso(Mensagem);
  finally
    BtEnviar.Enabled := True;
  end;
end;

procedure TFmWOrdSerIncRap.BtRascunhoClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMRascunho, BtRascunho);
end;

procedure TFmWOrdSerIncRap.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmWOrdSerIncRap.CBAplicativoChange(Sender: TObject);
var
  Aplicativo: Integer;
begin
  if CBAplicativo.Text <> '' then
  begin
    Aplicativo := Geral.IMV(FAplicCodigo[CBAplicativo.ItemIndex]);
    //
    ReopenWOrdSerVer_REST(Aplicativo);
  end;
end;

procedure TFmWOrdSerIncRap.CBAssuntoChange(Sender: TObject);
const
  URL = 'http://www.dermatek.net.br/complementos/wordserassmsg.php';
var
  Assunto: Integer;
begin
  if CBAssunto.Text <> '' then
  begin
    Assunto := Geral.IMV(FWOrdSerAssCodigo[CBAssunto.ItemIndex]);
    //
    if Assunto <> 0 then
    begin
      PnBrowser.Visible := True;
      DmkWeb.WebBrowserPost(URL, WebBrowser1, ['id', 'wordserass'],
        [Geral.FF0(VAR_WEB_USR_ID), Geral.FF0(Assunto)]);
    end;
  end;
end;

procedure TFmWOrdSerIncRap.CBPrioridadeChange(Sender: TObject);
var
  Descri: String;
begin
  Descri := FWOrdSerPriDescri[CBPrioridade.ItemIndex];
  //
  LaPrioridade.Caption := Descri;
  LaPrioridade.Visible := True;
end;

procedure TFmWOrdSerIncRap.CkPrintClick(Sender: TObject);
begin
  if CkPrint.Checked then
    MyObjects.Informa2(LaAviso1, LaAviso2, False,
      'Ao marcar o campo Enviar captura da tela ser� enviado em anexo uma foto da janela!')
  else
    MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
end;

procedure TFmWOrdSerIncRap.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  DmkWeb.CarregaImagemFormWEB(ImgWEB);
end;

procedure TFmWOrdSerIncRap.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  try
    Screen.Cursor := crHourGlass;
    //
    FWOrdSerAssCodigo.Free;
    FWOrdSerAssNome.Free;
    FWOrdSerPriCodigo.Free;
    FWOrdSerPriNome.Free;
    FWOrdSerPriDescri.Free;
    FAplicCodigo.Free;
    FAplicNome.Free;
    FVersao.Free;
  finally
    Screen.Cursor := crDefault;
  end;
  if Length(FPrntScrnImg) > 0 then
    DeleteFile(FPrntScrnImg);
end;

procedure TFmWOrdSerIncRap.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  try
    Screen.Cursor := crHourGlass;
    //
    FWOrdSerAssCodigo := TStringList.Create;
    FWOrdSerAssNome   := TStringList.Create;
    FWOrdSerPriCodigo := TStringList.Create;
    FWOrdSerPriNome   := TStringList.Create;
    FWOrdSerPriDescri := TStringList.Create;
    FAplicCodigo      := TStringList.Create;
    FAplicNome        := TStringList.Create;
    FVersao           := TStringList.Create;
    //
    ReopenWOrdSerAss_REST();
    ReopenWOrdSerPri_REST();
    ReopenWOrdSerApl_REST();
    //
    FJanela      := '';
    FAncora      := '';
    FSisInfo     := '';
    FPrntScrnImg := '';
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmWOrdSerIncRap.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
    [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmWOrdSerIncRap.FormShow(Sender: TObject);
begin
  MostraEdicao(True, stIns);
end;

procedure TFmWOrdSerIncRap.ImgPrintClick(Sender: TObject);
begin
  if not FileExists(FPrntScrnImg) then Exit;
  //
  if Geral.MB_Pergunta('Deseja abrir o arquivo?') = ID_YES then
  begin
    Geral.AbreArquivo(FPrntScrnImg, True);
  end;
end;

procedure TFmWOrdSerIncRap.MostraEdicao(Mostra: Boolean; SQLType: TSQLType);
  procedure MontaStringGrid;
  begin
    MyObjects.LimpaGrade(Grade, 1, 1, True);
    //
    Grade.ColCount := 2;
    Grade.RowCount := 2;
    //
    Grade.Cells[1, 0] := 'Anexos';
  end;
var
  Assunto, Prioridade, Aplicativo: Integer;
  IncSalvo: Boolean;
begin
  BtRascunho.Visible := False;
  //
  if FileExists(FPrntScrnImg) then
  begin
    ImgPrint.Picture.LoadFromFile(FPrntScrnImg);
    PnImg.Visible := True;
  end else
    PnImg.Visible := False;
  //
  if Mostra then
  begin
    {$IfNDef NO_USE_MYSQLMODULE}
      if DModG.MyPID_DB.Connected then
      begin
        BtRascunho.Visible := True;
        //
        if not VerificasSeTabelaExiste('_wordsertmp_') then
          UCriar.RecriaTempTableNovo(ntrttWOrdSerTmp, DmodG.QrUpdPID1, False, 0, '_wordsertmp_');
        //
        DModG.QrAuxPID1.Close;
        DModG.QrAuxPID1.Database := DModG.MyPID_DB;
        DModG.QrAuxPID1.SQL.Clear;
        DModG.QrAuxPID1.SQL.Add('SELECT * FROM _wordsertmp_ WHERE WebUser=' + Geral.FF0(VAR_WEB_USR_ID));
        UnDMkDAC_PF.AbreQuery(DModG.QrAuxPID1, DModG.MyPID_DB);
        //
        if DModG.QrAuxPID1.RecordCount > 0 then
        begin
          Assunto    := DModG.QrAuxPID1.FieldByName('Assunto').AsInteger;
          Prioridade := DModG.QrAuxPID1.FieldByName('Prioridade').AsInteger;
          Aplicativo := DModG.QrAuxPID1.FieldByName('Aplic').AsInteger;
          //
          CBAssunto.ItemIndex      := FWOrdSerAssCodigo.IndexOf(Geral.FF0(Assunto));
          MeMsg.Text               := DModG.QrAuxPID1.FieldByName('Descri').AsString;
          EdSistema.ValueVariant   := DModG.QrAuxPID1.FieldByName('Sistema').AsString;
          EdImpacto.ValueVariant   := DModG.QrAuxPID1.FieldByName('Impacto').AsString;
          CBPrioridade.ItemIndex   := FWOrdSerPriCodigo.IndexOf(Geral.FF0(Prioridade));
          CBAplicativo.ItemIndex   := FAplicCodigo.IndexOf(Geral.FF0(Aplicativo));
          CBVersao.Text            := DModG.QrAuxPID1.FieldByName('Versao').AsString;
          EdJanela.ValueVariant    := DModG.QrAuxPID1.FieldByName('Janela').AsString;
          EdJanelaRel.ValueVariant := DModG.QrAuxPID1.FieldByName('JanelaRel').AsString;
          EdCompoRel.ValueVariant  := DModG.QrAuxPID1.FieldByName('CompoRel').AsString;
          CkPrint.Checked          := False;
          CkPrint.Visible          := True;
          //
          IncSalvo := True;
        end else
          IncSalvo := False;
      end else
      begin
        IncSalvo           := False;
        BtRascunho.Visible := False;
      end;
    {$Else}
      IncSalvo           := False;
      BtRascunho.Visible := False;
    {$EndIf}
    if not IncSalvo then
    begin
      CBAssunto.ItemIndex      := -1;
      MeMsg.Text               := '';
      EdSistema.ValueVariant   := FSisInfo;
      EdImpacto.ValueVariant   := '';
      CBPrioridade.ItemIndex   := -1;
      CBAplicativo.ItemIndex   := FAplicCodigo.IndexOf(Geral.FF0(CO_DMKID_APP));
      CBVersao.Text            :=  Geral.VersaoAplicTxt(CO_VERSAO);
      EdJanela.ValueVariant    := FJanela;
      if FAncora <> '' then
      begin
        EdJanelaRel.ValueVariant := FAncora;
        EdCompoRel.ValueVariant  := '';
      end else
      begin
        EdJanelaRel.ValueVariant := Copy(VAR_CaptionFormOrigem, 1, 13);
        EdCompoRel.ValueVariant  := VAR_NomeCompoF7;
      end;
      CkPrint.Checked := False;
      CkPrint.Visible := Length(FPrntScrnImg) > 0;
      //
      ReopenWOrdSerVer_REST(CO_DMKID_APP);
    end;
    MontaStringGrid;
  end;
  CBVersao.SetFocus;
end;

end.
