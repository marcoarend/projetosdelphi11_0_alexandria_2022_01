unit WSuporte;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnInternalConsts,
  dmkImage, OleCtrls, SHDocVw, StrUtils, dmkPageControl, Menus,
  UnDmkEnums, UnProjGroup_Consts;

type
  TFmWSuporte = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    LaTitulo1C: TLabel;
    ImgWEB: TdmkImage;
    TabControl1: TTabControl;
    WebBrowser1: TWebBrowser;
    GBCntrl: TGroupBox;
    PnMenu: TPanel;
    ProgressBar1: TProgressBar;
    EdPesquisar: TdmkEdit;
    LaPesquisa: TLabel;
    BtConfirma: TBitBtn;
    PMMenu: TPopupMenu;
    Gerenciarsolicitaes1: TMenuItem;
    N1: TMenuItem;
    Histricodeatualizaes2: TMenuItem;
    GerenciarusuriosWEB1: TMenuItem;
    Gerenciarminhaconta2: TMenuItem;
    Licenas1: TMenuItem;
    N2: TMenuItem;
    Boletosemaberto1: TMenuItem;
    Boletosvencidos1: TMenuItem;
    Notasfiscais1: TMenuItem;
    N6: TMenuItem;
    Desconectareouconectarcomusuriodiferente1: TMenuItem;
    Ajudas1: TMenuItem;
    N3: TMenuItem;
    Desbloquearjanela1: TMenuItem;
    Panel1: TPanel;
    Panel4: TPanel;
    BtSaida: TBitBtn;
    BtMenu: TBitBtn;
    BtInclui: TBitBtn;
    CkNavegPad: TCheckBox;
    utoriais1: TMenuItem;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure WebBrowser1ProgressChange(ASender: TObject; Progress,
      ProgressMax: Integer);
    procedure TabControl1Change(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure WebBrowser1BeforeNavigate2(ASender: TObject;
      const pDisp: IDispatch; const URL, Flags, TargetFrameName, PostData,
      Headers: OleVariant; var Cancel: WordBool);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtIncluiClick(Sender: TObject);
    procedure BtMenuClick(Sender: TObject);
    procedure Gerenciarsolicitaes1Click(Sender: TObject);
    procedure Histricodeatualizaes2Click(Sender: TObject);
    procedure GerenciarusuriosWEB1Click(Sender: TObject);
    procedure Gerenciarminhaconta2Click(Sender: TObject);
    procedure Licenas1Click(Sender: TObject);
    procedure Boletosemaberto1Click(Sender: TObject);
    procedure Boletosvencidos1Click(Sender: TObject);
    procedure Notasfiscais1Click(Sender: TObject);
    procedure Desconectareouconectarcomusuriodiferente1Click(Sender: TObject);
    procedure Ajudas1Click(Sender: TObject);
    procedure Desbloquearjanela1Click(Sender: TObject);
    procedure utoriais1Click(Sender: TObject);
    procedure CkNavegPadClick(Sender: TObject);
  private
    { Private declarations }
    FNaoPesquisa: Boolean;
    procedure trataEvento (AHtmlVars: TStringList);
    procedure ConfiguraURL(Tab: Integer);
    procedure MostraWFaq();
    procedure ExecutaPesquisa();
  public
    { Public declarations }
    FPrntScrnImg, FJanela, FAncora, FOrigem, FPesqTxt: String;
    FTamanho: Int64;
    FTabIndex, FNavePad: Integer;
  end;

  var
  FmWSuporte: TFmWSuporte;

implementation

uses UnMyObjects, Module, UnDmkWeb, MyListas, WOrdSerIncRap, UnDmkProcFunc,
  UnGrl_Vars;

{$R *.DFM}

procedure TFmWSuporte.ExecutaPesquisa();
var
  Txt: String;
begin
  Txt := EdPesquisar.ValueVariant;
  //
  try
    Screen.Cursor := crHourGlass;
    ConfiguraURL(TabControl1.TabIndex);
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmWSuporte.Ajudas1Click(Sender: TObject);
begin
  MostraWFaq();
  Close;
end;

procedure TFmWSuporte.Boletosemaberto1Click(Sender: TObject);
var
  Link: String;
begin
  if CkNavegPad.Checked then
  begin
    if VAR_WEB_USR_TIPO = 2 then
      Link := 'http://www.dermatek.net.br/?page=via2&token=' + VAR_WEB_IDLOGIN
    else
      Link := 'http://www.dermatek.net.br/?page=abertos&token=' + VAR_WEB_IDLOGIN;
  end else
  begin
    if VAR_WEB_USR_TIPO = 2 then
      Link := 'http://www.dermatek.net.br/?page=via2'
    else
      Link := 'http://www.dermatek.net.br/?page=abertos';
  end;
  //
  DmkWeb.MostraWebBrowser(Link, CkNavegPad.Checked, False, 0, 0);
  //
  Close;
end;

procedure TFmWSuporte.Boletosvencidos1Click(Sender: TObject);
var
  Link: String;
begin
  if CkNavegPad.Checked then
  begin
    if VAR_WEB_USR_TIPO = 2 then
      Link := 'http://www.dermatek.net.br/?page=inadimp&token=' + VAR_WEB_IDLOGIN
    else
      Link := 'http://www.dermatek.net.br/?page=inad&token=' + VAR_WEB_IDLOGIN;
  end else
  begin
    if VAR_WEB_USR_TIPO = 2 then
      Link := 'http://www.dermatek.net.br/?page=inadimp'
    else
      Link := 'http://www.dermatek.net.br/?page=inad';
  end;
  //
  DmkWeb.MostraWebBrowser(Link, CkNavegPad.Checked, False, 0, 0);
  //
  Close;
end;

procedure TFmWSuporte.BtConfirmaClick(Sender: TObject);
begin
  ExecutaPesquisa();
end;

procedure TFmWSuporte.BtIncluiClick(Sender: TObject);
begin
  if LowerCase(FOrigem) = 'wordser' then
  begin
    MostraWFaq();
  end else
  begin
    Application.CreateForm(TFmWOrdSerIncRap, FmWOrdSerIncRap);
    FmWOrdSerIncRap.FPrntScrnImg := FPrntScrnImg;
    FmWOrdSerIncRap.FJanela      := FJanela;
    FmWOrdSerIncRap.FAncora      := FAncora;
    FmWOrdSerIncRap.FSisInfo     := dmkPF.ObtemSysInfo();
    FmWOrdSerIncRap.ShowModal;
    FmWOrdSerIncRap.Destroy;
  end;
  Close;
end;

procedure TFmWSuporte.BtMenuClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMMenu, BtMenu);
end;

procedure TFmWSuporte.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmWSuporte.CkNavegPadClick(Sender: TObject);
begin
  Geral.WriteAppKeyLM('NavegPad', Application.Title, Geral.BoolToInt(CkNavegPad.Checked), ktInteger);
end;

procedure TFmWSuporte.ConfiguraURL(Tab: Integer);
var
  URL, Txt: String;
begin
  case Tab of
    0: //Perguntas frequentes
    begin
      LaPesquisa.Caption := 'Digite aqui o que voc� deseja pesquisar:';

      Txt := EdPesquisar.ValueVariant;
      URL := 'http://www.dermatek.net.br/complementos/widgets/wid_wfaq.php?desktop=true&limit=15&id=' +
        Geral.FF0(VAR_WEB_USR_ID) + '&texto=' + Txt;
    end;
    (* Desativado ser� fundido com o perguntas frequentes para formar o Ajudas
    1: //Tutoriais
    begin
      LaPesquisa.Caption := 'Digite aqui o que voc� deseja pesquisar:';

      Txt := EdPesquisar.ValueVariant;
      URL := 'http://www.dermatek.net.br/complementos/widgets/wid_whelp.php?desktop=true&id=' +
        Geral.FF0(VAR_WEB_CODUSER) + '&limit=15&texto=' + Txt + '&desktop=true&limit=10';
    end;
    *)
    1: //Solicita��es
    begin
      LaPesquisa.Caption := 'Digite o ID ou parte do texto da solicita��o:';

      if FNaoPesquisa then
      begin
        EdPesquisar.ValueVariant := '';
        Txt                      := '';
        FNaoPesquisa             := False;
      end else
      begin
        Txt := EdPesquisar.ValueVariant;
      end;
      URL := 'http://www.dermatek.net.br/complementos/widgets/wid_wordser.php?desktop=true&limit=15&id=' +
        Geral.FF0(VAR_WEB_USR_ID) + '&texto=' + Txt;
    end;
  end;
  webbrowser1.Navigate(URL);
end;

procedure TFmWSuporte.Desbloquearjanela1Click(Sender: TObject);
begin
  {$IFDef VER320}
    Geral.AbrirAppAuxiliar(12, Geral.FF0(VAR_PERFIL), FJanela);
  {$Else}
    Geral.AbrirAppAuxiliar(VAR_IP, 12, Geral.FF0(VAR_PERFIL), FJanela);
  {$EndIf}
end;

procedure TFmWSuporte.Desconectareouconectarcomusuriodiferente1Click(
  Sender: TObject);
begin
  DmkWeb.DesconectarUsuarioWEB;
  Close;
end;

procedure TFmWSuporte.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  DmkWeb.CarregaImagemFormWEB(ImgWEB);
  //
  EdPesquisar.SetFocus;
end;

procedure TFmWSuporte.FormCreate(Sender: TObject);
var
  Visi: Boolean;
begin
  ImgTipo.SQLType := stLok;
  FNaoPesquisa    := True;
  //
  //Configura PopupMenu fazer aqui para n�o dar erro de desenho
  Visi := VAR_WEB_USR_TIPO in [0,1,2];
  //
  GerenciarusuriosWEB1.Visible := Visi;
  Licenas1.Visible             := Visi;
  N1.Visible                   := Visi;
  Boletosemaberto1.Visible     := Visi;
  Boletosvencidos1.Visible     := Visi;
  Notasfiscais1.Visible        := Visi;
  N2.Visible                   := Visi;
end;

procedure TFmWSuporte.FormShow(Sender: TObject);
var
  NavegPad: Integer;
begin
  if LowerCase(FOrigem) = 'wordser' then
  begin
    BtInclui.Caption := 'Perguntas frequentes';
    BtMenu.Visible   := False;
  end else
  begin
    BtInclui.Caption := 'Inclui solicita��o';
    BtMenu.Visible   := True;
  end;
  if FPesqTxt <> '' then
    EdPesquisar.ValueVariant := FPesqTxt;
  //
  TabControl1.TabIndex := FTabIndex;
  ConfiguraURL(FTabIndex);
  //
  NavegPad := Geral.ReadAppKeyLM('NavegPad', Application.Title, ktInteger, 1);
  //
  CkNavegPad.Visible := True;
  CkNavegPad.Checked := Geral.IntToBool(NavegPad);
end;

procedure TFmWSuporte.Gerenciarminhaconta2Click(Sender: TObject);
var
  Link: String;
begin
  if CkNavegPad.Checked then
    Link := 'http://www.dermatek.net.br/?page=usercad&token=' + VAR_WEB_IDLOGIN
  else
    Link := 'http://www.dermatek.net.br/?page=usercad';
  //
  DmkWeb.MostraWebBrowser(Link, CkNavegPad.Checked, False, 0, 0);
  //
  Close;
end;

procedure TFmWSuporte.Gerenciarsolicitaes1Click(Sender: TObject);
var
  Link: String;
begin
  if CkNavegPad.Checked then
    Link := 'http://www.dermatek.net.br/?page=wordser&token=' + VAR_WEB_IDLOGIN
  else
    Link := 'http://www.dermatek.net.br/?page=wordser';
  //
  DmkWeb.MostraWebBrowser(Link, CkNavegPad.Checked, False, 0, 0);
  //
  Close;
end;

procedure TFmWSuporte.GerenciarusuriosWEB1Click(Sender: TObject);
var
  Link: String;
begin
  if CkNavegPad.Checked then
    Link := 'http://www.dermatek.net.br/?page=cadpass&token=' + VAR_WEB_IDLOGIN
  else
    Link := 'http://www.dermatek.net.br/?page=cadpass';
  //
  DmkWeb.MostraWebBrowser(Link, CkNavegPad.Checked, False, 0, 0);
  //
  Close;
end;

procedure TFmWSuporte.Histricodeatualizaes2Click(Sender: TObject);
var
  Link: String;
begin
  if CkNavegPad.Checked then
    Link := 'http://www.dermatek.net.br/?page=hisalt&token=' + VAR_WEB_IDLOGIN +
      '&aplicativo=' + Geral.FF0(CO_DMKID_APP) + '&versao=' + Geral.FF0(CO_VERSAO)
  else
    Link := 'http://www.dermatek.net.br/?page=hisalt&aplicativo=' +
      Geral.FF0(CO_DMKID_APP) + '&versao=' + Geral.FF0(CO_VERSAO);
  //
  DmkWeb.MostraWebBrowser(Link, CkNavegPad.Checked, False, 0, 0);
  //
  Close;
end;

procedure TFmWSuporte.Licenas1Click(Sender: TObject);
var
  Link: String;
begin
  if CkNavegPad.Checked then
    Link := 'http://www.dermatek.net.br/?page=licencas&token=' + VAR_WEB_IDLOGIN
  else
    Link := 'http://www.dermatek.net.br/?page=licencas';
  //
  DmkWeb.MostraWebBrowser(Link, CkNavegPad.Checked, False, 0, 0);
  //
  Close;
end;

procedure TFmWSuporte.MostraWFaq;
var
  Link: String;
begin
  if CkNavegPad.Checked then
    Link := 'http://www.dermatek.net.br/?page=wfaq&token=' + VAR_WEB_IDLOGIN +
      '&janela=' + FJanela
  else
    Link := 'http://www.dermatek.net.br/?page=wfaq&janela=' + FJanela;
  //
  DmkWeb.MostraWebBrowser(Link, CkNavegPad.Checked, False, 0, 0);
end;

procedure TFmWSuporte.Notasfiscais1Click(Sender: TObject);
var
  Link: String;
begin
  if CkNavegPad.Checked then
    Link := 'http://www.dermatek.net.br/?page=nfsevis&token=' + VAR_WEB_IDLOGIN
  else
    Link := 'http://www.dermatek.net.br/?page=nfsevis';
  //
  DmkWeb.MostraWebBrowser(Link, CkNavegPad.Checked, False, 0, 0);
  //
  Close;
end;

procedure TFmWSuporte.TabControl1Change(Sender: TObject);
begin
  ConfiguraURL(TabControl1.TabIndex);
end;

procedure TFmWSuporte.trataEvento(AHtmlVars: TStringList);
var
  Link, lStrExibir: String;
begin
  lStrExibir := AHtmlVars.Values ['url'];
  if Length(lStrExibir) > 0 then
  begin
    lStrExibir := Geral.Substitui(lStrExibir, '_', '&');
    //
    if CkNavegPad.Checked then
      Link := lStrExibir + '&token=' + VAR_WEB_IDLOGIN
    else
      Link := lStrExibir;
    //
    DmkWeb.MostraWebBrowser(Link, CkNavegPad.Checked, False, 0, 0);
    //
    Close;
  end;
end;

procedure TFmWSuporte.utoriais1Click(Sender: TObject);
var
  Link: String;
begin
  if CkNavegPad.Checked then
    Link := 'http://www.dermatek.net.br/?page=whelp&token=' + VAR_WEB_IDLOGIN
  else
    Link := 'http://www.dermatek.net.br/?page=whelp';
  //
  DmkWeb.MostraWebBrowser(Link, CkNavegPad.Checked, False, 0, 0);
  //
  Close;
end;

procedure TFmWSuporte.WebBrowser1BeforeNavigate2(ASender: TObject;
  const pDisp: IDispatch; const URL, Flags, TargetFrameName, PostData,
  Headers: OleVariant; var Cancel: WordBool);
var
  lHtmlVars : TStringList;
  WAddr: OleVariant;
begin
  WAddr     := URL;
  lHtmlVars := TStringList.Create;
  try
    lHtmlVars := Geral.Explode(WAddr, '&', 0);
  finally
    TrataEvento(lHtmlVars);
    FreeAndNil(lHtmlVars);
  end;
end;

procedure TFmWSuporte.WebBrowser1ProgressChange(ASender: TObject; Progress,
  ProgressMax: Integer);
begin
  if Progress > 0 then
  begin
    ProgressBar1.Max      := ProgressMax;
    ProgressBar1.Position := Progress
  end else
  begin
    ProgressBar1.Position := 0
  end;
end;

end.
