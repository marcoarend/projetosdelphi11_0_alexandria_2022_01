unit WOrdSerRes;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkDBLookupComboBox, dmkEdit,
  dmkEditCB, mySQLDbTables, DmkDAC_PF, Variants;

type
  TFmWOrdSerRes = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    Label3: TLabel;
    EdGrupo: TdmkEditCB;
    CBGrupo: TdmkDBLookupComboBox;
    CBRespons: TdmkDBLookupComboBox;
    EdRespons: TdmkEditCB;
    LaSolicitante: TLabel;
    SBSolicitante: TSpeedButton;
    QrGrupos: TmySQLQuery;
    DsGrupos: TDataSource;
    QrGruposCodigo: TIntegerField;
    QrGruposNome: TWideStringField;
    QrRespons: TmySQLQuery;
    IntegerField1: TIntegerField;
    DsRespons: TDataSource;
    QrResponsPersonalName: TWideStringField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure EdGrupoChange(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure SBSolicitanteClick(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenRespons(Grupo: Integer);
  public
    { Public declarations }
    FReabre, FEcerrado: Boolean;
    FCodigo, FAplicativo, FSolicitante, FCliente, FAssunto: Integer;
  end;

  var
  FmWOrdSerRes: TFmWOrdSerRes;

implementation

uses UnMyObjects, Module, UMySQLModule, ModWOrdSer, UnDmkWeb;

{$R *.DFM}

procedure TFmWOrdSerRes.BtOKClick(Sender: TObject);
var
  Resul, Respons: Integer;
  MensagemAviso: String;
begin
  Respons := EdRespons.ValueVariant;
  //
  if MyObjects.FIC(Respons = 0, EdRespons, 'Defina o respons�vel!') then Exit;
  if MyObjects.FIC(FCodigo = 0, nil, 'ID n�o definido!') then Exit;  
  //
  (* Mudado para o SOAP
  if UMyMod.SQLInsUpd(Dmod.QrUpdN, stUpd, 'wordser', False,
    ['Respons'], ['Codigo'], [Respons], [FCodigo], False) then
  begin
    FReabre := True;
    Close;
  end;
  *)
  Resul := DmkWeb.WOrdSerAtualizaRespons(31, FCodigo, Respons, MensagemAviso);
  //
  if Resul = 104 then
  begin
    FReabre := True;
    Close;
  end else
    Geral.MB_Aviso(MensagemAviso)
end;

procedure TFmWOrdSerRes.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmWOrdSerRes.EdGrupoChange(Sender: TObject);
var
  Grupo, Respons: Integer;
begin
  Grupo := EdGrupo.ValueVariant;
  //
  if Grupo <> 0 then
  begin
    Screen.Cursor := crHourGlass;
    try
      ReopenRespons(Grupo);
      //
      Respons := DModWOrdSer.ObtemAtendente3(FAplicativo, FSolicitante,
                   FCliente, FAssunto, Grupo);
      //
      EdRespons.ValueVariant := Respons;
      CBRespons.KeyValue     := Respons;
    finally
      Screen.Cursor := crDefault;
    end;
  end else
    QrRespons.Close;
end;

procedure TFmWOrdSerRes.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmWOrdSerRes.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  FReabre         := False;
  //
  UMyMod.AbreQuery(QrGrupos, DMod.MyDBn);
end;

procedure TFmWOrdSerRes.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
    [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmWOrdSerRes.ReopenRespons(Grupo: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrRespons, Dmod.MyDBn, [
    'SELECT wus.Codigo, wus.PersonalName ',
    'FROM worseateca wca ',
    'LEFT JOIN worseate ate ON ate.Codigo = wca.Codigo ',
    'LEFT JOIN wusers wus ON wus.Codigo = ate.Atendente ',
    'WHERE wca.Grupo=' + Geral.FF0(Grupo),
    'AND wus.Ativo = 1 ',
    'GROUP BY wus.Codigo ',
    'ORDER BY wus.PersonalName ',
    '']);
end;

procedure TFmWOrdSerRes.SBSolicitanteClick(Sender: TObject);
begin
  DModWOrdSer.MostraWOrdSerAte;
  //
  UMyMod.AbreQuery(QrGrupos, DMod.MyDBn);
  //
  EdGrupo.ValueVariant   := 0;
  CBGrupo.KeyValue       := Null;
  EdRespons.ValueVariant := 0;
  CBRespons.KeyValue     := Null;
  EdGrupo.SetFocus;
end;

end.
