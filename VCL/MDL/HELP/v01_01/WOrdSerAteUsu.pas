unit WOrdSerAteUsu;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, Grids, DBGrids, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, dmkDBGridDAC, dmkValUsu, dmkGeral;

type
  TFmWOrdSerAteUsu = class(TForm)
    PainelConfirma: TPanel;
    BtOK: TBitBtn;
    PainelTitulo: TPanel;
    Image1: TImage;
    Panel1: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    Panel3: TPanel;
    CBEntidade: TdmkDBLookupComboBox;
    EdEntidade: TdmkEditCB;
    Label9: TLabel;
    dmkDBGridDAC1: TdmkDBGridDAC;
    QrEntidade: TmySQLQuery;
    QrEntidadeCodigo: TIntegerField;
    QrEntidadeCodUsu: TIntegerField;
    QrEntidadeNome: TWideStringField;
    DsEntidade: TDataSource;
    VUEntidade: TdmkValUsu;
    QrWUsers: TmySQLQuery;
    DsWUsers: TDataSource;
    QrWUsersCodigo: TAutoIncField;
    QrWUsersPersonalName: TWideStringField;
    QrTmpWUsers: TmySQLQuery;
    DsTmpWUsers: TDataSource;
    QrTmpWUsersCodigo: TIntegerField;
    QrTmpWUsersAtivo: TSmallintField;
    QrTmpWUsersNome: TWideStringField;
    BtTodos: TBitBtn;
    BtNenhum: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure RadioGroup1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure EdEntidadeChange(Sender: TObject);
    procedure BtTodosClick(Sender: TObject);
    procedure BtNenhumClick(Sender: TObject);
    procedure dmkDBGridDAC1AfterSQLExec(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
    FWUserTmp: String;
    procedure MontaTmpTable(Entidade: Integer);
    procedure ReopenTmpWUsers;
    procedure AtualizaTodos(Status: Integer);
  public
    { Public declarations }
    FCodigo: Integer;
  end;

  var
  FmWOrdSerAteUsu: TFmWOrdSerAteUsu;

implementation

uses Module, ModuleGeral, UCreate, UMySQLModule, UnInternalConsts, WOrdSerAte,
UnMyObjects;

{$R *.DFM}

procedure TFmWOrdSerAteUsu.BtNenhumClick(Sender: TObject);
begin
  AtualizaTodos(0);
end;

procedure TFmWOrdSerAteUsu.BtOKClick(Sender: TObject);
var
  Codigo, Controle, Usuario: Integer;
begin
  Codigo := FCodigo;
  //
  QrTmpWUsers.First;
  while not QrTmpWUsers.Eof do
  begin
    Usuario := QrTmpWUsersCodigo.Value;
    //
    if QrTmpWUsersAtivo.Value = 1 then
    begin
      Controle := UMyMod.BuscaNovoCodigo_Int(Dmod.QrUpdN, 'worseateit', 'Controle', [], [], stIns, 0, siPositivo, nil);
      //
      if UMyMod.SQLInsUpd_Old(Dmod.QrUpdN, CO_INCLUSAO, 'worseateit', False,
      [
        'Usuario', 'Codigo'
      ], ['Controle'],
      [
        Usuario, Codigo
      ], [Controle])
      then begin
      end;
    end;
    //
    QrTmpWUsers.Next;
  end;
  FmWOrdSerAte.ReopenWOrSeAteItCli(FCodigo, VUEntidade.ValueVariant);
  Close;
end;

procedure TFmWOrdSerAteUsu.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmWOrdSerAteUsu.BtTodosClick(Sender: TObject);
begin
  AtualizaTodos(1);
end;

procedure TFmWOrdSerAteUsu.dmkDBGridDAC1AfterSQLExec(Sender: TObject);
begin
  ReopenTmpWUsers;
end;

procedure TFmWOrdSerAteUsu.EdEntidadeChange(Sender: TObject);
begin
  MontaTmpTable(VUEntidade.ValueVariant);
end;

procedure TFmWOrdSerAteUsu.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
end;

procedure TFmWOrdSerAteUsu.FormCreate(Sender: TObject);
begin
  QrEntidade.Close;
  QrEntidade.Open;
end;

procedure TFmWOrdSerAteUsu.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

procedure TFmWOrdSerAteUsu.RadioGroup1Click(Sender: TObject);
begin
 //Habilitar o BtOK;
end;

procedure TFmWOrdSerAteUsu.ReopenTmpWUsers;
begin
  QrTmpWUsers.Close;
  QrTmpWUsers.SQL.Clear;
  QrTmpWUsers.SQL.Add('SELECT * FROM ' + FWUserTmp);
  QrTmpWUsers.Open;
end;

procedure TFmWOrdSerAteUsu.MontaTmpTable(Entidade: Integer);
begin
  FWUserTmp := UCriar.RecriaTempTableNovo(ntrtt_CodNom, DmodG.QrUpdPID1, True, 1, 'MalaDirCon');
  dmkDBGridDAC1.SQLTable := FWUserTmp;
  //
  QrWUsers.Close;
  QrWUsers.Params[00].AsInteger := Entidade;
  QrWUsers.Open;
  //
  DmodG.QrUpdPID1.SQL.Clear;
  DmodG.QrUpdPID1.SQL.Add('INSERT INTO ' + FWUserTmp + ' SET ');
  DmodG.QrUpdPID1.SQL.Add('Codigo=:P0, Nome=:P1, Ativo=:P2');
  while not QrWUsers.Eof do
  begin
    DmodG.QrUpdPID1.Params[00].AsInteger := QrWUsersCodigo.Value;
    DmodG.QrUpdPID1.Params[01].AsString  := QrWUsersPersonalName.Value;
    DmodG.QrUpdPID1.Params[02].AsInteger := 0;
    DmodG.QrUpdPID1.ExecSQL;
    //
    QrWUsers.Next;
  end;
  QrWUsers.Close;
  ReopenTmpWUsers;
end;

procedure TFmWOrdSerAteUsu.AtualizaTodos(Status: Integer);
begin
  DmodG.QrUpdPID1.SQL.Clear;
  DmodG.QrUpdPID1.SQL.Add('UPDATE ' + FWUserTmp + ' SET Ativo=:P0');
  DmodG.QrUpdPID1.Params[00].AsInteger := Status;
  DmodG.QrUpdPID1.ExecSQL;
  //
  ReopenTmpWUsers;
end;

end.
