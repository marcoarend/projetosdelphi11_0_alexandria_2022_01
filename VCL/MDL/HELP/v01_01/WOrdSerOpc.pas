unit WOrdSerOpc;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, dmkEdit, dmkEditCB, dmkDBLookupComboBox,
  mySQLDbTables, dmkPermissoes, UnDmkEnums;

type
  TFmWOrdSerOpc = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    QrWOrdSerMod: TmySQLQuery;
    QrWOrdSerModCodigo: TIntegerField;
    QrWOrdSerModNome: TWideStringField;
    DsWOrdSerMod: TDataSource;
    DsWOrdSerSta: TDataSource;
    QrWOrdSerSta: TmySQLQuery;
    IntegerField1: TIntegerField;
    StringField1: TWideStringField;
    DsFTPWebDir: TDataSource;
    QrFTPWebDir: TmySQLQuery;
    QrFTPWebDirCodigo: TIntegerField;
    QrFTPWebDirNome: TWideStringField;
    QrEnvMailAbe: TmySQLQuery;
    DsEnvMailAbe: TDataSource;
    QrEnvMailCom: TmySQLQuery;
    DsEnvMailCom: TDataSource;
    QrEnvMailEnc: TmySQLQuery;
    DsEnvMailEnc: TDataSource;
    QrEnvMailRes: TmySQLQuery;
    DsEnvMailRes: TDataSource;
    QrWOrdSerModApl: TmySQLQuery;
    IntegerField2: TIntegerField;
    StringField2: TWideStringField;
    DsWOrdSerModApl: TDataSource;
    QrWOrdSerOpc: TmySQLQuery;
    DsWOrdSerOpc: TDataSource;
    dmkPermissoes1: TdmkPermissoes;
    ImgWEB: TdmkImage;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    GroupBox2: TGroupBox;
    Label98: TLabel;
    Label1: TLabel;
    CBWOrdSerMod: TdmkDBLookupComboBox;
    EdWOrdSerMod: TdmkEditCB;
    CBWOrdSerSta: TdmkDBLookupComboBox;
    EdWOrdSerSta: TdmkEditCB;
    GroupBox3: TGroupBox;
    Label2: TLabel;
    EdDirFTPPad: TdmkEditCB;
    CBDirFTPPad: TdmkDBLookupComboBox;
    GroupBox4: TGroupBox;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label3: TLabel;
    CBEnvMailAbe: TdmkDBLookupComboBox;
    EdEnvMailAbe: TdmkEditCB;
    CBEnvMailCom: TdmkDBLookupComboBox;
    EdEnvMailCom: TdmkEditCB;
    EdEnvMailEnc: TdmkEditCB;
    CBEnvMailEnc: TdmkDBLookupComboBox;
    EdEnvMailRes: TdmkEditCB;
    CBEnvMailRes: TdmkDBLookupComboBox;
    GroupBox5: TGroupBox;
    Label7: TLabel;
    CBWOrdSerModApl: TdmkDBLookupComboBox;
    EdWOrdSerModApl: TdmkEditCB;
    GroupBox6: TGroupBox;
    Label8: TLabel;
    Label9: TLabel;
    EdUpdLocInte: TdmkEdit;
    GroupBox7: TGroupBox;
    Label10: TLabel;
    EdGruStatus1: TdmkEditCB;
    CBGruStatus1: TdmkDBLookupComboBox;
    Label11: TLabel;
    EdGruStatus2: TdmkEditCB;
    CBGruStatus2: TdmkDBLookupComboBox;
    Label12: TLabel;
    EdGruStatus4: TdmkEditCB;
    CBGruStatus4: TdmkDBLookupComboBox;
    EdGruStatus3: TdmkEditCB;
    CBGruStatus3: TdmkDBLookupComboBox;
    Label13: TLabel;
    QrGruStatus1: TmySQLQuery;
    IntegerField3: TIntegerField;
    DsGruStatus1: TDataSource;
    QrGruStatus2: TmySQLQuery;
    IntegerField4: TIntegerField;
    DsGruStatus2: TDataSource;
    QrGruStatus4: TmySQLQuery;
    IntegerField5: TIntegerField;
    DsGruStatus4: TDataSource;
    QrGruStatus3: TmySQLQuery;
    IntegerField6: TIntegerField;
    DsGruStatus3: TDataSource;
    QrGruStatus1Nome: TWideStringField;
    QrGruStatus2Nome: TWideStringField;
    QrGruStatus3Nome: TWideStringField;
    QrGruStatus4Nome: TWideStringField;
    Label14: TLabel;
    EdEnvMailSta: TdmkEditCB;
    CBEnvMailSta: TdmkDBLookupComboBox;
    QrEnvMailSta: TmySQLQuery;
    DsEnvMailSta: TDataSource;
    QrEnvMailStaCodigo: TIntegerField;
    QrEnvMailStaNome: TWideStringField;
    QrEnvMailAbeCodigo: TIntegerField;
    QrEnvMailAbeNome: TWideStringField;
    QrEnvMailComCodigo: TIntegerField;
    QrEnvMailComNome: TWideStringField;
    QrEnvMailEncCodigo: TIntegerField;
    QrEnvMailEncNome: TWideStringField;
    QrEnvMailResCodigo: TIntegerField;
    QrEnvMailResNome: TWideStringField;
    EdOrdem_CamposTxt: TdmkEdit;
    EdOrdem_CamposFields: TdmkEdit;
    Label15: TLabel;
    SBOrdem_CamposFields: TSpeedButton;
    TabSheet3: TTabSheet;
    CkUsaTeamViewer: TCheckBox;
    LaGrupoTeamViewer: TLabel;
    CBGrupoTeamViewer: TComboBox;
    EdURLHelpTeamViewer: TdmkEdit;
    LaURLHelpTeamViewer: TLabel;
    SBGrupoTeamViewer: TSpeedButton;
    EdTokenTeamViewer: TdmkEdit;
    LaTokenTeamViewer: TLabel;
    SBTokenTeamViewer: TSpeedButton;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure SBOrdem_CamposFieldsClick(Sender: TObject);
    procedure CkUsaTeamViewerClick(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenWOrdSerOpc;
  public
    { Public declarations }
  end;

  var
  FmWOrdSerOpc: TFmWOrdSerOpc;

implementation

uses UnMyObjects, Module, UMySQLModule, UnDmkWeb, ModWOrdSer;

{$R *.DFM}

procedure TFmWOrdSerOpc.BtOKClick(Sender: TObject);
var
  Ordem_CamposTxt, Ordem_CamposFields, TokenTeamViewer, GrupoTeamViewer,
  URLHelpTeamViewer: String;
  Codigo, StatPad, ModPad, ModPadApl, DirFTPPad, EnvMailAbe, EnvMailCom,
  EnvMailEnc, EnvMailRes, EnvMailSta, UpdLocInte, GruStatus1, GruStatus2,
  GruStatus3, GruStatus4, UsaTeamViewer: Integer;
begin
  Codigo     := QrWOrdSerOpc.FieldByName('Codigo').AsInteger;
  StatPad    := EdWOrdSerSta.ValueVariant;
  ModPad     := EdWOrdSerMod.ValueVariant;
  ModPadApl  := EdWOrdSerModApl.ValueVariant;
  DirFTPPad  := EdDirFTPPad.ValueVariant;
  UpdLocInte := EdUpdLocInte.ValueVariant;
  //
  EnvMailAbe := EdEnvMailAbe.ValueVariant;
  EnvMailCom := EdEnvMailCom.ValueVariant;
  EnvMailEnc := EdEnvMailEnc.ValueVariant;
  EnvMailRes := EdEnvMailRes.ValueVariant;
  EnvMailSta := EdEnvMailSta.ValueVariant;
  //
  GruStatus1 := EdGruStatus1.ValueVariant;
  GruStatus2 := EdGruStatus2.ValueVariant;
  GruStatus3 := EdGruStatus3.ValueVariant;
  GruStatus4 := EdGruStatus4.ValueVariant;
  //
  UsaTeamViewer := Geral.BoolToInt(CkUsaTeamViewer.Checked);
  //
  if UsaTeamViewer = 1 then
  begin
    TokenTeamViewer   := EdTokenTeamViewer.ValueVariant;
    GrupoTeamViewer   := CBGrupoTeamViewer.Text;
    URLHelpTeamViewer := EdURLHelpTeamViewer.ValueVariant;

    if MyObjects.FIC(TokenTeamViewer = '', EdTokenTeamViewer, 'Defina o Token de script!') then Exit;
    if MyObjects.FIC(GrupoTeamViewer = '', CBGrupoTeamViewer, 'Defina o Grupo padr�o!') then Exit;
  end else
  begin
    TokenTeamViewer   := '';
    GrupoTeamViewer   := '';
    URLHelpTeamViewer := '';
  end;
  //
  Ordem_CamposTxt    := EdOrdem_CamposTxt.ValueVariant;
  Ordem_CamposFields := EdOrdem_CamposFields.ValueVariant;
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpdN, stUpd, 'wordseropc', False,
  [
    'ModPad', 'ModPadApl', 'StatPad', 'DirFTPPad', 'EnvMailAbe', 'EnvMailCom',
    'EnvMailEnc', 'EnvMailRes', 'EnvMailSta', 'UpdLocInte', 'GruStatus1',
    'GruStatus2', 'GruStatus3', 'GruStatus4',
    'UsaTeamViewer', 'TokenTeamViewer', 'GrupoTeamViewer', 'URLHelpTeamViewer',
    'Ordem_CamposTxt', 'Ordem_CamposFields'
  ], ['Codigo'],
  [
    ModPad, ModPadApl, StatPad, DirFTPPad, EnvMailAbe, EnvMailCom,
    EnvMailEnc, EnvMailRes, EnvMailSta, UpdLocInte, GruStatus1,
    GruStatus2, GruStatus3, GruStatus4,
    UsaTeamViewer, TokenTeamViewer, GrupoTeamViewer, URLHelpTeamViewer,
    Ordem_CamposTxt, Ordem_CamposFields
  ], [Codigo], True)
  then begin
    ReopenWOrdSerOpc;
    Close;
  end;
end;

procedure TFmWOrdSerOpc.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmWOrdSerOpc.CkUsaTeamViewerClick(Sender: TObject);
var
  Visi: Boolean;
begin
  Visi := CkUsaTeamViewer.Checked;
  //
  LaTokenTeamViewer.Visible   := Visi;
  EdTokenTeamViewer.Visible   := Visi;
  SBTokenTeamViewer.Visible   := Visi;
  LaGrupoTeamViewer.Visible   := Visi;
  CBGrupoTeamViewer.Visible   := Visi;
  SBGrupoTeamViewer.Visible   := Visi;
  LaURLHelpTeamViewer.Visible := Visi;
  EdURLHelpTeamViewer.Visible := Visi;
end;

procedure TFmWOrdSerOpc.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  DmkWeb.CarregaImagemFormWEB(ImgWEB);
end;

procedure TFmWOrdSerOpc.FormCreate(Sender: TObject);
var
  UsaTeamViewer: Boolean;
begin
  ImgTipo.SQLType := stLok;
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, False,
    'Para utilizar os status dos grupos todos os campos de status devem ser preenchidos!');
  //
  ReopenWOrdSerOpc;
  //
  UMyMod.AbreQuery(QrWOrdSerMod, DMod.MyDBn);
  UMyMod.AbreQuery(QrWOrdSerSta, DMod.MyDBn);
  UMyMod.AbreQuery(QrFTPWebDir, DMod.MyDBn);
  UMyMod.AbreQuery(QrWOrdSerModApl, DMod.MyDBn);
  //
  UMyMod.AbreQuery(QrEnvMailAbe, DMod.MyDBn);
  UMyMod.AbreQuery(QrEnvMailCom, DMod.MyDBn);
  UMyMod.AbreQuery(QrEnvMailEnc, DMod.MyDBn);
  UMyMod.AbreQuery(QrEnvMailRes, DMod.MyDBn);
  UMyMod.AbreQuery(QrEnvMailSta, DMod.MyDBn);
  //
  UMyMod.AbreQuery(QrGruStatus1, DMod.MyDBn);
  UMyMod.AbreQuery(QrGruStatus2, DMod.MyDBn);
  UMyMod.AbreQuery(QrGruStatus3, DMod.MyDBn);
  UMyMod.AbreQuery(QrGruStatus4, DMod.MyDBn);
  //
  EdWOrdSerMod.ValueVariant    := QrWOrdSerOpc.FieldByName('ModPad').AsInteger;
  CBWOrdSerMod.KeyValue        := QrWOrdSerOpc.FieldByName('ModPad').AsInteger;
  EdWOrdSerSta.ValueVariant    := QrWOrdSerOpc.FieldByName('StatPad').AsInteger;
  CBWOrdSerSta.KeyValue        := QrWOrdSerOpc.FieldByName('StatPad').AsInteger;
  EdDirFTPPad.ValueVariant     := QrWOrdSerOpc.FieldByName('DirFTPPad').AsInteger;
  CBDirFTPPad.KeyValue         := QrWOrdSerOpc.FieldByName('DirFTPPad').AsInteger;
  EdWOrdSerModApl.ValueVariant := QrWOrdSerOpc.FieldByName('ModPadApl').AsInteger;
  CBWOrdSerModApl.KeyValue     := QrWOrdSerOpc.FieldByName('ModPadApl').AsInteger;
  EdUpdLocInte.ValueVariant    := QrWOrdSerOpc.FieldByName('UpdLocInte').AsInteger;
  //
  EdEnvMailAbe.ValueVariant := QrWOrdSerOpc.FieldByName('EnvMailAbe').AsInteger;
  CBEnvMailAbe.KeyValue     := QrWOrdSerOpc.FieldByName('EnvMailAbe').AsInteger;
  EdEnvMailCom.ValueVariant := QrWOrdSerOpc.FieldByName('EnvMailCom').AsInteger;
  CBEnvMailCom.KeyValue     := QrWOrdSerOpc.FieldByName('EnvMailCom').AsInteger;
  EdEnvMailEnc.ValueVariant := QrWOrdSerOpc.FieldByName('EnvMailEnc').AsInteger;
  CBEnvMailEnc.KeyValue     := QrWOrdSerOpc.FieldByName('EnvMailEnc').AsInteger;
  EdEnvMailRes.ValueVariant := QrWOrdSerOpc.FieldByName('EnvMailRes').AsInteger;
  CBEnvMailRes.KeyValue     := QrWOrdSerOpc.FieldByName('EnvMailRes').AsInteger;
  EdEnvMailSta.ValueVariant := QrWOrdSerOpc.FieldByName('EnvMailSta').AsInteger;
  CBEnvMailSta.KeyValue     := QrWOrdSerOpc.FieldByName('EnvMailSta').AsInteger;
  //
  EdGruStatus1.ValueVariant := QrWOrdSerOpc.FieldByName('GruStatus1').AsInteger;
  CBGruStatus1.KeyValue     := QrWOrdSerOpc.FieldByName('GruStatus1').AsInteger;
  EdGruStatus2.ValueVariant := QrWOrdSerOpc.FieldByName('GruStatus2').AsInteger;
  CBGruStatus2.KeyValue     := QrWOrdSerOpc.FieldByName('GruStatus2').AsInteger;
  EdGruStatus3.ValueVariant := QrWOrdSerOpc.FieldByName('GruStatus3').AsInteger;
  CBGruStatus3.KeyValue     := QrWOrdSerOpc.FieldByName('GruStatus3').AsInteger;
  EdGruStatus4.ValueVariant := QrWOrdSerOpc.FieldByName('GruStatus4').AsInteger;
  CBGruStatus4.KeyValue     := QrWOrdSerOpc.FieldByName('GruStatus4').AsInteger;
  //
  EdOrdem_CamposTxt.ValueVariant    := QrWOrdSerOpc.FieldByName('Ordem_CamposTxt').AsString;
  EdOrdem_CamposFields.ValueVariant := QrWOrdSerOpc.FieldByName('Ordem_CamposFields').AsString;
  //
  UsaTeamViewer                    := Geral.IntToBool(QrWOrdSerOpc.FieldByName('UsaTeamViewer').AsInteger);
  CkUsaTeamViewer.Checked          := UsaTeamViewer;
  LaTokenTeamViewer.Visible        := UsaTeamViewer;
  EdTokenTeamViewer.Visible        := UsaTeamViewer;
  SBTokenTeamViewer.Visible        := UsaTeamViewer;
  EdTokenTeamViewer.ValueVariant   := QrWOrdSerOpc.FieldByName('TokenTeamViewer').AsString;;
  LaGrupoTeamViewer.Visible        := UsaTeamViewer;
  CBGrupoTeamViewer.Visible        := UsaTeamViewer;
  SBGrupoTeamViewer.Visible        := UsaTeamViewer;
  CBGrupoTeamViewer.Text           := QrWOrdSerOpc.FieldByName('GrupoTeamViewer').AsString;
  LaURLHelpTeamViewer.Visible      := UsaTeamViewer;
  EdURLHelpTeamViewer.Visible      := UsaTeamViewer;
  EdURLHelpTeamViewer.ValueVariant := QrWOrdSerOpc.FieldByName('URLHelpTeamViewer').AsString;
  //
  PageControl1.ActivePageIndex := 0;
end;

procedure TFmWOrdSerOpc.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmWOrdSerOpc.ReopenWOrdSerOpc;
begin
  QrWOrdSerOpc.Close;
  QrWOrdSerOpc.Open;
end;

procedure TFmWOrdSerOpc.SBOrdem_CamposFieldsClick(Sender: TObject);
var
  CamposTxt_Default, CamposFields_Default, CamposTxt_Sel, CamposFields_Sel: String;
begin
  DModWOrdSer.ReopenWOSOpcUsu(0, istApenasPadrao);
  //
  CamposTxt_Default    := DModWOrdSer.QrWOSOpcUsu.FieldByName('Ordem_CamposTxt').AsString;
  CamposFields_Default := DModWOrdSer.QrWOSOpcUsu.FieldByName('Ordem_CamposFields').AsString;
  CamposTxt_Sel        := EdOrdem_CamposTxt.ValueVariant;
  CamposFields_Sel     := EdOrdem_CamposFields.ValueVariant;
  //
  DModWOrdSer.ConfiguraOrdemCampos(CamposTxt_Default, CamposFields_Default,
    CamposTxt_Sel, CamposFields_Sel);
  //
  EdOrdem_CamposTxt.ValueVariant    := CamposTxt_Sel;
  EdOrdem_CamposFields.ValueVariant := CamposFields_Sel;
end;

end.
