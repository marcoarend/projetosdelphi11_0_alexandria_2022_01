object FmWTreiOpc: TFmWTreiOpc
  Left = 339
  Top = 185
  Caption = 'WEB-WTREI-001 :: Treinamentos - Op'#231#245'es'
  ClientHeight = 525
  ClientWidth = 494
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 494
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 446
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 398
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 283
        Height = 32
        Caption = 'Treinamentos - Op'#231#245'es'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 283
        Height = 32
        Caption = 'Treinamentos - Op'#231#245'es'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 283
        Height = 32
        Caption = 'Treinamentos - Op'#231#245'es'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 494
    Height = 363
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 494
      Height = 363
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 494
        Height = 363
        Align = alClient
        TabOrder = 0
        object Label8: TLabel
          Left = 15
          Top = 15
          Width = 180
          Height = 13
          Caption = 'Confirmar autoriza'#231#227'o com no m'#237'nimo '
        end
        object Label9: TLabel
          Left = 283
          Top = 15
          Width = 32
          Height = 13
          Caption = 'hora(s)'
        end
        object Label3: TLabel
          Left = 15
          Top = 41
          Width = 183
          Height = 13
          Caption = 'Confirmar participa'#231#227'o com no m'#237'nimo '
        end
        object Label5: TLabel
          Left = 283
          Top = 41
          Width = 32
          Height = 13
          Caption = 'hora(s)'
        end
        object GroupBox4: TGroupBox
          Left = 15
          Top = 62
          Width = 470
          Height = 160
          Caption = 'E-mails padr'#227'o'
          TabOrder = 2
          object Label4: TLabel
            Left = 12
            Top = 15
            Width = 222
            Height = 13
            Caption = 'E-mail padr'#227'o para autoriza'#231#227'o de treinamento:'
          end
          object Label6: TLabel
            Left = 12
            Top = 63
            Width = 301
            Height = 13
            Caption = 'E-mail padr'#227'o para confirma'#231#227'o de participa'#231#227'o de treinamento:'
          end
          object Label14: TLabel
            Left = 12
            Top = 111
            Width = 201
            Height = 13
            Caption = 'E-mail padr'#227'o para acesso ao treinamento:'
          end
          object CBEmailAut: TdmkDBLookupComboBox
            Left = 69
            Top = 34
            Width = 385
            Height = 21
            KeyField = 'Codigo'
            ListField = 'Nome'
            ListSource = DsEmailAut
            TabOrder = 1
            dmkEditCB = EdEmailAut
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
          object EdEmailAut: TdmkEditCB
            Left = 12
            Top = 34
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            DBLookupComboBox = CBEmailAut
            IgnoraDBLookupComboBox = False
          end
          object EdEmailPar: TdmkEditCB
            Left = 12
            Top = 82
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 2
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            DBLookupComboBox = CBEmailPar
            IgnoraDBLookupComboBox = False
          end
          object CBEmailPar: TdmkDBLookupComboBox
            Left = 69
            Top = 82
            Width = 385
            Height = 21
            KeyField = 'Codigo'
            ListField = 'Nome'
            ListSource = DsEmailPar
            TabOrder = 3
            dmkEditCB = EdEmailPar
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
          object EdEmailTrei: TdmkEditCB
            Left = 12
            Top = 130
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 4
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            DBLookupComboBox = CBEmailTrei
            IgnoraDBLookupComboBox = False
          end
          object CBEmailTrei: TdmkDBLookupComboBox
            Left = 69
            Top = 130
            Width = 385
            Height = 21
            KeyField = 'Codigo'
            ListField = 'Nome'
            ListSource = DsEmailTrei
            TabOrder = 5
            dmkEditCB = EdEmailTrei
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
        end
        object GroupBox2: TGroupBox
          Left = 15
          Top = 225
          Width = 470
          Height = 117
          Caption = 'Termos de uso'
          TabOrder = 3
          object Label1: TLabel
            Left = 12
            Top = 15
            Width = 224
            Height = 13
            Caption = 'Termo padr'#227'o para autoriza'#231#227'o de treinamento:'
          end
          object Label2: TLabel
            Left = 12
            Top = 63
            Width = 267
            Height = 13
            Caption = 'Termo para confirma'#231#227'o de participa'#231#227'o de treinamento:'
          end
          object CBTermoAut: TdmkDBLookupComboBox
            Left = 69
            Top = 34
            Width = 385
            Height = 21
            KeyField = 'Codigo'
            ListField = 'Nome'
            ListSource = DsTermoAut
            TabOrder = 1
            dmkEditCB = EdTermoAut
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
          object EdTermoAut: TdmkEditCB
            Left = 12
            Top = 34
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            DBLookupComboBox = CBTermoAut
            IgnoraDBLookupComboBox = False
          end
          object EdTermoPar: TdmkEditCB
            Left = 12
            Top = 82
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 2
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            DBLookupComboBox = CBTermoPar
            IgnoraDBLookupComboBox = False
          end
          object CBTermoPar: TdmkDBLookupComboBox
            Left = 69
            Top = 82
            Width = 385
            Height = 21
            KeyField = 'Codigo'
            ListField = 'Nome'
            ListSource = DsTermoPar
            TabOrder = 3
            dmkEditCB = EdTermoPar
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
        end
        object EdHorasCfgAut: TdmkEdit
          Left = 198
          Top = 10
          Width = 80
          Height = 21
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
        object EdHorasCfgPar: TdmkEdit
          Left = 198
          Top = 36
          Width = 80
          Height = 21
          Alignment = taRightJustify
          TabOrder = 1
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 411
    Width = 494
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 490
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 455
    Width = 494
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 348
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 346
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 400
    Top = 67
  end
  object QrWTreiOpc: TmySQLQuery
    Database = Dmod.MyDBn
    SQL.Strings = (
      'SELECT * '
      'FROM wtreiopc')
    Left = 348
    Top = 64
  end
  object DsEmailAut: TDataSource
    DataSet = QrEmailAut
    Left = 416
    Top = 136
  end
  object QrEmailAut: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM preemail'
      'WHERE Ativo = 1'
      'ORDER BY Nome')
    Left = 388
    Top = 136
    object QrEmailAutCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEmailAutNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
  object QrEmailPar: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM preemail'
      'WHERE Ativo = 1'
      'ORDER BY Nome')
    Left = 388
    Top = 192
    object IntegerField1: TIntegerField
      FieldName = 'Codigo'
    end
    object StringField1: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
  object DsEmailPar: TDataSource
    DataSet = QrEmailPar
    Left = 416
    Top = 192
  end
  object QrEmailTrei: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM preemail'
      'WHERE Ativo = 1'
      'ORDER BY Nome')
    Left = 388
    Top = 240
    object IntegerField2: TIntegerField
      FieldName = 'Codigo'
    end
    object StringField2: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
  object DsEmailTrei: TDataSource
    DataSet = QrEmailTrei
    Left = 416
    Top = 240
  end
  object QrTermoAut: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM wtextos'
      'WHERE Publicado = 1'
      'AND HashArq <> ""'
      'AND ArqWeb <> 0'
      'ORDER BY Nome')
    Left = 356
    Top = 296
    object IntegerField3: TIntegerField
      FieldName = 'Codigo'
    end
    object QrTermoAutNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
  object DsTermoAut: TDataSource
    DataSet = QrTermoAut
    Left = 384
    Top = 296
  end
  object DsTermoPar: TDataSource
    DataSet = QrTermoPar
    Left = 384
    Top = 344
  end
  object QrTermoPar: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM wtextos'
      'WHERE Publicado = 1'
      'AND HashArq <> ""'
      'AND ArqWeb <> 0'
      'ORDER BY Nome')
    Left = 356
    Top = 344
    object IntegerField4: TIntegerField
      FieldName = 'Codigo'
    end
    object StringField4: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
end
