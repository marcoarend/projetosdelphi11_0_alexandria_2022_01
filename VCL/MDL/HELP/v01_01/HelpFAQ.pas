unit HelpFAQ;

interface

uses
  Windows, Messages, SysUtils, ComCtrls, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2,
  ResIntStrings, UnMLAGeral, UnGOTOy, UnInternalConsts, UnMsgInt, Variants,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, dmkGeral,
  dmkPermissoes, dmkEdit, dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup,
  unDmkProcFunc, UnDmkEnums, dmkCheckBox, Vcl.OleCtrls, SHDocVw, Vcl.Menus,
  Vcl.Grids, Vcl.DBGrids, dmkDBGrid, dmkCompoStore, dmkCheckGroup, UnGrlHelp,
  dmkPageControl, UnProjGroup_Consts;

type
  TFmHelpFAQ = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    BtRestringe: TBitBtn;
    BtFAQ: TBitBtn;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    QrHelpFaq: TmySQLQuery;
    QrHelpFaqCodigo: TIntegerField;
    QrHelpFaqNome: TWideStringField;
    DsHelpFaq: TDataSource;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    Label9: TLabel;
    EdNome: TdmkEdit;
    Label1: TLabel;
    DBEdCodigo: TdmkDBEdit;
    Label2: TLabel;
    DBEdNome: TdmkDBEdit;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    EdTags: TdmkEdit;
    Label3: TLabel;
    RGStatus: TdmkRadioGroup;
    CkAtivo: TdmkCheckBox;
    QrHelpFaqTags: TWideStringField;
    QrHelpFaqStatus: TSmallintField;
    QrHelpFaqTexto: TWideMemoField;
    Label4: TLabel;
    dmkDBEdit1: TdmkDBEdit;
    DBCheckBox2: TDBCheckBox;
    DBRGStatus: TDBRadioGroup;
    QrHelpFaqAtivo: TSmallintField;
    BtTexto: TBitBtn;
    QrHelpOpc: TmySQLQuery;
    QrHelpOpcCodigo: TIntegerField;
    QrHelpOpcTexto: TWideMemoField;
    PMFAQ: TPopupMenu;
    Inclui1: TMenuItem;
    Altera1: TMenuItem;
    Exclui1: TMenuItem;
    PMRestringe: TPopupMenu;
    Inclui2: TMenuItem;
    Exclui2: TMenuItem;
    AplicativoLocal1: TMenuItem;
    AplicativoWeb1: TMenuItem;
    Mdulo1: TMenuItem;
    QrHelpRestr: TmySQLQuery;
    DsHelpRestr: TDataSource;
    QrHelpRestrCodigo: TIntegerField;
    QrHelpRestrItem: TIntegerField;
    QrHelpRestrHelpResTip: TIntegerField;
    QrHelpRestrNome: TWideStringField;
    QrHelpRestrHelpResTip_TXT: TWideStringField;
    CSTabSheetChamou: TdmkCompoStore;
    QrHelpRestrHelpTip: TSmallintField;
    QrHelpRestrHelp: TIntegerField;
    DBCGNivel: TdmkDBCheckGroup;
    CGNivel: TdmkCheckGroup;
    QrHelpFaqNivel: TIntegerField;
    DBGRestricao: TdmkDBGrid;
    BtVisualiza: TBitBtn;
    BtSincro: TBitBtn;
    dmkDBCheckGroup1: TdmkDBCheckGroup;
    QrHelpFaqTipo: TIntegerField;
    CGTipo: TdmkCheckGroup;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrHelpFaqAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrHelpFaqBeforeOpen(DataSet: TDataSet);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure BtTextoClick(Sender: TObject);
    procedure Inclui1Click(Sender: TObject);
    procedure BtFAQClick(Sender: TObject);
    procedure Altera1Click(Sender: TObject);
    procedure Exclui1Click(Sender: TObject);
    procedure PMFAQPopup(Sender: TObject);
    procedure BtRestringeClick(Sender: TObject);
    procedure QrHelpFaqAfterScroll(DataSet: TDataSet);
    procedure QrHelpFaqBeforeClose(DataSet: TDataSet);
    procedure AplicativoLocal1Click(Sender: TObject);
    procedure AplicativoWeb1Click(Sender: TObject);
    procedure Mdulo1Click(Sender: TObject);
    procedure Exclui2Click(Sender: TObject);
    procedure PMRestringePopup(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure BtVisualizaClick(Sender: TObject);
    procedure BtSincroClick(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    procedure MostraEdicao;
  public
    { Public declarations }
    procedure LocCod(Atual, Codigo: Integer);
  end;

var
  FmHelpFAQ: TFmHelpFAQ;
const
  FFormatFloat = '00000';

implementation

uses MyListas, UnMyObjects, Module, UnTextos_Jan, UnDmkHTML2, UnitHelp,
  DmkDAC_PF, MyDBCheck, MyGlyfs, Principal, UnitHelpJan, UnGrlUsuarios;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmHelpFAQ.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmHelpFAQ.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrHelpFaqCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmHelpFAQ.DefParams;
begin
  VAR_GOTOTABELA := 'helpfaq';
  VAR_GOTOMYSQLTABLE := QrHelpFaq;
  VAR_GOTONEG := gotoPos;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT *');
  VAR_SQLx.Add('FROM helpfaq');
  VAR_SQLx.Add('WHERE Codigo > 0');
  //
  VAR_SQL1.Add('AND Codigo=:P0');
  //
  //VAR_SQL2.Add('AND CodUsu=:P0');
  //
  VAR_SQLa.Add('AND Nome Like :P0');
  //
end;

procedure TFmHelpFAQ.Exclui1Click(Sender: TObject);
var
  Codigo: Integer;
begin
  Codigo := QrHelpFaqCodigo.Value;
  //
  GrlHelp.ExcluiHelpFaq(Dmod.MyDB, Dmod.QrAux, Codigo,
    'Confirma a exclus�o do item ID n�mero: ' + Geral.FF0(Codigo));
end;

procedure TFmHelpFAQ.Exclui2Click(Sender: TObject);
var
  Codigo: Integer;
begin
  Codigo := QrHelpRestrCodigo.Value;
  //
  if GrlHelp.ExcluiHelpRestr(Dmod.MyDB, Dmod.QrUpd, Codigo,
    'Confirma a exclus�o do item ID n�mero: ' + Geral.FF0(Codigo)) then
  begin
    GrlHelp.ReopenHelpRestr(Dmod.MyDB, QrHelpRestr, QrHelpFaqCodigo.Value,
      Integer(thtFAQ), 0);
  end;
end;

procedure TFmHelpFAQ.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmHelpFAQ.QueryPrincipalAfterOpen;
begin
end;

procedure TFmHelpFAQ.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmHelpFAQ.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmHelpFAQ.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmHelpFAQ.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmHelpFAQ.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmHelpFAQ.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmHelpFAQ.BtSincroClick(Sender: TObject);
begin
  UnHelpJan.MostraFormSincro();
end;

procedure TFmHelpFAQ.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrHelpFaqCodigo.Value;
  //
  if TFmHelpFAQ(Self).Owner is TApplication then
    Close
  else
    MyObjects.FormTDIFecha(Self, TTabSheet(CSTabSheetChamou.Component));
end;

procedure TFmHelpFAQ.Altera1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrHelpFaq, [PnDados],
    [PnEdita], EdNome, ImgTipo, 'helpfaq');
end;

procedure TFmHelpFAQ.AplicativoLocal1Click(Sender: TObject);
begin
  UnHelp.IncluiHelpRestr(thtFAQ, 1, QrHelpFaqCodigo.Value, QrHelpRestr);
end;

procedure TFmHelpFAQ.AplicativoWeb1Click(Sender: TObject);
begin
  UnHelp.IncluiHelpRestr(thtFAQ, 2, QrHelpFaqCodigo.Value, QrHelpRestr);
end;

procedure TFmHelpFAQ.BtConfirmaClick(Sender: TObject);
var
  Codigo, Status, Tipo, Nivel, Ativo: Integer;
  Nome, Tags: String;
begin
  if ImgTipo.SQLType = stIns then
    Codigo := 0
  else
    Codigo := QrHelpFaqCodigo.Value;
  //
  Nome   := EdNome.ValueVariant;
  Tags   := EdTags.ValueVariant;
  Tipo   := CGTipo.Value;
  Status := RGStatus.ItemIndex;
  Nivel  := CGNivel.Value;
  Ativo  := Geral.BoolToInt(CkAtivo.Checked);
  //
  if GrlHelp.InsUpd_FAQ(Dmod.MyDB, Dmod.QrUpd, stDesktop, Nome, Tags, Tipo,
    Nivel, Status, Ativo, Codigo, EdNome) then
  begin
    MostraEdicao;
    LocCod(Codigo, Codigo);
  end;
end;

procedure TFmHelpFAQ.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := EdCodigo.ValueVariant;
  //
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'helpfaq', 'Codigo');
  //
  MostraEdicao;
end;

procedure TFmHelpFAQ.BtFAQClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMFAQ, BtFAQ);
end;

procedure TFmHelpFAQ.Mdulo1Click(Sender: TObject);
begin
  UnHelp.IncluiHelpRestr(thtFAQ, 3, QrHelpFaqCodigo.Value, QrHelpRestr);
end;

procedure TFmHelpFAQ.MostraEdicao;
begin
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  //
  GOTOy.BotoesSb(ImgTipo.SQLType, TFmHelpFAQ(Self));
end;

procedure TFmHelpFAQ.PMFAQPopup(Sender: TObject);
var
  Enab, Enab2: Boolean;
begin
  Enab  := (QrHelpFaq.State <> dsInactive) and (QrHelpFaq.RecordCount > 0);
  Enab2 := (QrHelpRestr.State <> dsInactive) and (QrHelpRestr.RecordCount > 0);
  //
  Altera1.Enabled := Enab;
  Exclui1.Enabled := Enab and (not Enab2);
end;

procedure TFmHelpFAQ.PMRestringePopup(Sender: TObject);
var
  Enab, Enab2: Boolean;
begin
  Enab  := (QrHelpFaq.State <> dsInactive) and (QrHelpFaq.RecordCount > 0);
  Enab2 := (QrHelpRestr.State <> dsInactive) and (QrHelpRestr.RecordCount > 0);
  //
  Inclui2.Enabled := Enab;
  Exclui2.Enabled := Enab and Enab2;
end;

procedure TFmHelpFAQ.BtTextoClick(Sender: TObject);
var
  Texto: WideString;
begin
  if (QrHelpFaq.State <> dsInactive) and (QrHelpFaq.RecordCount > 0) then
  begin
    GrlHelp.ReopenHelpOpc(Dmod.MyDB, QrHelpOpc);
    //
    Texto := QrHelpFaqTexto.Value;
    //
    if Texto = '' then
      Texto := QrHelpOpcTexto.Value;
    //
    Textos_Jan.MostraTextosHTML(QrHelpFaqCodigo.Value, Texto, 'helpfaq',
      'Codigo', 'Texto', Dmod.MyDB, True);
    LocCod(QrHelpFaqCodigo.Value, QrHelpFaqCodigo.Value);
  end;
end;

procedure TFmHelpFAQ.BtVisualizaClick(Sender: TObject);
var
  Texto: String;
begin
  if (QrHelpFaq.State <> dsInactive) and (QrHelpFaq.RecordCount > 0) then
  begin
    Texto := QrHelpFaqTexto.Value;
    //
    if Texto <> '' then
      UnHelpJan.MostraFormWebBrowser(False, nil, nil, Texto);
  end;
end;

procedure TFmHelpFAQ.BtRestringeClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMRestringe, BtRestringe);
end;

procedure TFmHelpFAQ.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType         := stLok;
  GBEdita.Align           := alClient;
  DBGRestricao.Align      := alClient;
  DBGRestricao.DataSource := DsHelpRestr;
  //
  CriaOForm;
  //
  CGNivel.Items.Clear;
  DBCGNivel.Items.Clear;
  //
  CGNivel.Items.AddStrings(GrlUsuarios.ConfiguraNiveis(CO_DMKID_APP));
  DBCGNivel.Items.AddStrings(GrlUsuarios.ConfiguraNiveis(CO_DMKID_APP));
  //
  RGStatus.Items.Clear;
  RGStatus.Items.AddStrings(GrlHelp.ConfiguraStatus());
  //
  DBRGStatus.Items.Clear;
  DBRGStatus.Items.AddStrings(GrlHelp.ConfiguraStatus());
end;

procedure TFmHelpFAQ.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrHelpFaqCodigo.Value, LaRegistro.Caption);
end;

procedure TFmHelpFAQ.SbNomeClick(Sender: TObject);
begin
  //LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
  LocCod(QrHelpFaqCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'helpfaq', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmHelpFAQ.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrHelpFaqCodigo.Value, LaRegistro.Caption);
end;

procedure TFmHelpFAQ.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmHelpFAQ.QrHelpFaqAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmHelpFAQ.QrHelpFaqAfterScroll(DataSet: TDataSet);
begin
  GrlHelp.ReopenHelpRestr(Dmod.MyDB, QrHelpRestr, QrHelpFaqCodigo.Value,
    Integer(thtFAQ), 0);
end;

procedure TFmHelpFAQ.FormActivate(Sender: TObject);
begin
  if TFmHelpFAQ(Self).Owner is TApplication then
  begin
    MyObjects.CorIniComponente();
  end;
end;

procedure TFmHelpFAQ.SbQueryClick(Sender: TObject);
begin
  if TFmHelpFAQ(Self).Owner is TApplication then
    UnHelpJan.MostraFormHelpPsq(FmHelpFAQ, False, nil, nil)
  else
    UnHelpJan.MostraFormHelpPsq(FmHelpFAQ, True, FmPrincipal.PageControl1,
      FmPrincipal.AdvToolBarPagerNovo);
end;

procedure TFmHelpFAQ.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmHelpFAQ.FormShow(Sender: TObject);
begin
{$IfNDef cSkinRank} //Berlin
{$IfNDef cAlphaSkin} //Berlin
  FmMyGlyfs.DefineGlyfs(TForm(Sender));
{$EndIf}
{$EndIf}
{$IfDef cSkinRank} //Berlin
  if FmPrincipal.Sd1.Active then
    FmMyGlyfs.DefineGlyfsTDI(FmPrincipal.sd1, Sender);
{$EndIf}
{$IfDef cAlphaSkin} //Berlin
  if FmPrincipal.sSkinManager1.Active then
    FmMyGlyfs.DefineGlyfsTDI2(FmPrincipal.sSkinManager1, Sender);
{$EndIf}
end;

procedure TFmHelpFAQ.Inclui1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrHelpFaq, [PnDados],
    [PnEdita], EdNome, ImgTipo, 'helpfaq');
end;

procedure TFmHelpFAQ.QrHelpFaqBeforeClose(DataSet: TDataSet);
begin
  QrHelpRestr.Close;
end;

procedure TFmHelpFAQ.QrHelpFaqBeforeOpen(DataSet: TDataSet);
begin
  QrHelpFaqCodigo.DisplayFormat := FFormatFloat;
end;

end.

