unit UnitWOrdSer;

interface

uses
  StdCtrls, ExtCtrls, Windows, Messages, SysUtils, Classes, Graphics, Controls,
  ComCtrls, dmkGeral, Forms, Dialogs, Menus, Db, DbCtrls, Variants, mySQLDbTables,
  UnDmkEnums, UnDmkProcFunc;

type
  TUnitWOrdSer = class(TObject)
  private
    { Private declarations }
  public
    { Public declarations }
    function  ExcluiWOrdSerTar(AgenTarIts: Integer): Boolean;
    procedure InsereWOrdSerTar(Codigo, AgenTarIts: Integer);
    procedure ReabreWOrdSerTar(Query: TmySQLQuery; Codigo: Integer);
  end;

var
  UnWOrdSer: TUnitWOrdSer;

implementation

uses DmkDAC_PF, UnDmkImg, MyDBCheck, UMySQLModule, UnMyObjects, ModuleGeral,
  Module;

{ TUnitWOrdSer }

function TUnitWOrdSer.ExcluiWOrdSerTar(AgenTarIts: Integer): Boolean;
begin
  Result := UMyMod.ExcluiRegistroInt1('', 'wordsertar', 'AgenTarIts', AgenTarIts,
              Dmod.MyDBn) = ID_YES;
end;

procedure TUnitWOrdSer.InsereWOrdSerTar(Codigo, AgenTarIts: Integer);
begin
  UMyMod.SQLInsUpd(Dmod.QrUpdN, stIns, 'WOrdSerTar', False,
    [], ['Codigo', 'AgenTarIts'], [], [Codigo, AgenTarIts], True);
end;

procedure TUnitWOrdSer.ReabreWOrdSerTar(Query: TmySQLQuery; Codigo: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(Query, Dmod.MyDBn, [
    'SELECT * ',
    'FROM wordsertar ',
    'WHERE Codigo=' + Geral.FF0(Codigo),
    '']);
end;

end.