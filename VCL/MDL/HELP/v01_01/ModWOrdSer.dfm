object DModWOrdSer: TDModWOrdSer
  Height = 383
  Width = 644
  PixelsPerInch = 96
  object QrClientes: TMySQLQuery
    Database = Dmod.MyDBn
    SQL.Strings = (
      'SELECT Codigo, '
      'IF (Tipo=0, RazaoSocial, Nome) Nome'
      'FROM entidades'
      'WHERE Ativo = 1'
      'AND Cliente1 = "V"'
      'ORDER BY Nome')
    Left = 22
    Top = 66
    object QrClientesCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'entidades.Codigo'
    end
    object QrClientesNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 100
    end
  end
  object DsClientes: TDataSource
    DataSet = QrClientes
    Left = 50
    Top = 66
  end
  object QrWOrdSerPri: TMySQLQuery
    Database = Dmod.MyDBn
    SQL.Strings = (
      'SELECT Codigo, Nome, Descri'
      'FROM wordserpri'
      'WHERE Ativo = 1'
      'ORDER BY Nome')
    Left = 22
    Top = 231
    object IntegerField4: TIntegerField
      FieldName = 'Codigo'
      Origin = 'wordserpri.Codigo'
    end
    object StringField2: TWideStringField
      FieldName = 'Nome'
      Origin = 'wordserpri.Nome'
      Required = True
      Size = 100
    end
    object QrWOrdSerPriDescri: TWideStringField
      FieldName = 'Descri'
      Size = 100
    end
  end
  object DsWOrdSerPri: TDataSource
    DataSet = QrWOrdSerPri
    Left = 50
    Top = 231
  end
  object QrWOrdSerMod: TMySQLQuery
    Database = Dmod.MyDBn
    SQL.Strings = (
      'SELECT Codigo, Nome, Descri'
      'FROM wordsermod'
      'WHERE Ativo = 1'
      'ORDER BY Nome')
    Left = 23
    Top = 181
    object IntegerField6: TIntegerField
      FieldName = 'Codigo'
      Origin = 'wordserass.Codigo'
    end
    object StringField5: TWideStringField
      FieldName = 'Nome'
      Origin = 'wordserass.Nome'
      Size = 50
    end
  end
  object DsWOrdSerMod: TDataSource
    DataSet = QrWOrdSerMod
    Left = 51
    Top = 181
  end
  object QrWOrdSerSta: TMySQLQuery
    Database = Dmod.MyDBn
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM wordsersta'
      'WHERE Ativo = 1'
      'ORDER BY Nome')
    Left = 21
    Top = 282
    object IntegerField5: TIntegerField
      FieldName = 'Codigo'
      Origin = 'wordsersta.Codigo'
    end
    object StringField4: TWideStringField
      FieldName = 'Nome'
      Origin = 'wordsersta.Nome'
      Required = True
      Size = 100
    end
  end
  object DsWOrdSerSta: TDataSource
    DataSet = QrWOrdSerSta
    Left = 49
    Top = 282
  end
  object QrResp: TMySQLQuery
    Database = Dmod.MyDBn
    SQL.Strings = (
      'SELECT Codigo, PersonalName'
      'FROM wusers'
      'WHERE Ativo = 1'
      'AND Tipo IN(0, 1)'
      'ORDER BY PersonalName')
    Left = 20
    Top = 122
    object QrRespCodigo: TAutoIncField
      FieldName = 'Codigo'
    end
    object QrRespPersonalName: TWideStringField
      FieldName = 'PersonalName'
      Size = 32
    end
  end
  object DsResp: TDataSource
    DataSet = QrResp
    Left = 48
    Top = 122
  end
  object QrAplicativo: TMySQLQuery
    Database = Dmod.MyDBn
    SQL.Strings = (
      'SELECT apl.Codigo, apl.Nome'
      'FROM cliaplic cap'
      'LEFT JOIN clientes cli ON cli.Codigo = cap.Codigo'
      'LEFT JOIN aplicativos apl ON apl.Codigo = cap.Aplicativo'
      'WHERE cli.Cliente=:P0')
    Left = 21
    Top = 10
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object StringField7: TWideStringField
      FieldName = 'Nome'
      Origin = 'grasrv1.Nome'
      Size = 50
    end
    object QrAplicativoCodigo: TIntegerField
      FieldName = 'Codigo'
    end
  end
  object DsAplicativo: TDataSource
    DataSet = QrAplicativo
    Left = 49
    Top = 10
  end
  object QrCli: TMySQLQuery
    Database = Dmod.MyDBn
    OnCalcFields = QrCliCalcFields
    SQL.Strings = (
      'SELECT en.Codigo, Tipo, IE, '
      
        'CASE WHEN en.Tipo=0 THEN en.RazaoSocial ELSE en.Nome     END NOM' +
        'E_ENT, '
      
        'CASE WHEN en.Tipo=0 THEN en.CNPJ        ELSE en.CPF      END CNP' +
        'J_CPF, '
      
        'CASE WHEN en.Tipo=0 THEN en.Fantasia ELSE en.Apelido END NO2_ENT' +
        ', '
      
        'CASE WHEN en.Tipo=0 THEN en.IE          ELSE en.RG       END IE_' +
        'RG, '
      
        'CASE WHEN en.Tipo=0 THEN en.ERua        ELSE en.PRua     END RUA' +
        ', '
      
        'CASE WHEN en.Tipo=0 THEN en.ENumero     ELSE en.PNumero  END NUM' +
        'ERO, '
      
        'CASE WHEN en.Tipo=0 THEN en.ECompl      ELSE en.PCompl   END COM' +
        'PL, '
      
        'CASE WHEN en.Tipo=0 THEN en.EBairro     ELSE en.PBairro  END BAI' +
        'RRO, '
      
        'CASE WHEN en.Tipo=0 THEN en.ECidade     ELSE en.PCidade  END CID' +
        'ADE, '
      'CASE WHEN en.Tipo=0 THEN en.EUF     ELSE en.PUF  END UF, '
      
        'CASE WHEN en.Tipo=0 THEN lle.Nome       ELSE llp.Nome    END NOM' +
        'ELOGRAD, '
      
        'CASE WHEN en.Tipo=0 THEN ufe.Nome       ELSE ufp.Nome    END NOM' +
        'EUF, '
      
        'CASE WHEN en.Tipo=0 THEN en.EPais       ELSE en.PPais    END Pai' +
        's, '
      
        'CASE WHEN en.Tipo=0 THEN en.ELograd     ELSE en.PLograd  END Log' +
        'rad, '
      
        'CASE WHEN en.Tipo=0 THEN en.ECEP        ELSE en.PCEP     END CEP' +
        ', '
      
        'CASE WHEN en.Tipo=0 THEN en.EEndeRef    ELSE en.PEndeRef END END' +
        'EREF, '
      
        'CASE WHEN en.Tipo=0 THEN en.ETe1        ELSE en.PTe1     END TE1' +
        ', '
      
        'CASE WHEN en.Tipo=0 THEN en.EFax        ELSE en.PFax     END FAX' +
        ', '
      
        'IF(en.Tipo=0, "CNPJ", "CPF") CAD_FEDERAL, IF(en.Tipo=0, "I.E.", ' +
        '"RG") CAD_ESTADUAL'
      'FROM entidades en '
      'LEFT JOIN ufs ufe ON ufe.Codigo=en.EUF'
      'LEFT JOIN ufs ufp ON ufp.Codigo=en.PUF'
      'LEFT JOIN listalograd lle ON lle.Codigo=en.ELograd'
      'LEFT JOIN listalograd llp ON llp.Codigo=en.PLograd'
      'WHERE en.Codigo=:P0')
    Left = 556
    Top = 10
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrCliCNPJ_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CNPJ_TXT'
      Size = 30
      Calculated = True
    end
    object QrCliTE1_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TE1_TXT'
      Size = 40
      Calculated = True
    end
    object QrCliE_ALL: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'E_ALL'
      Size = 256
      Calculated = True
    end
    object QrCliFAX_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'FAX_TXT'
      Size = 40
      Calculated = True
    end
    object QrCliNUMERO_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NUMERO_TXT'
      Size = 30
      Calculated = True
    end
    object QrCliCEP_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CEP_TXT'
      Calculated = True
    end
    object QrCliIE_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'IE_TXT'
      Size = 50
      Calculated = True
    end
    object QrCliCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCliTipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrCliIE: TWideStringField
      FieldName = 'IE'
    end
    object QrCliNOME_ENT: TWideStringField
      FieldName = 'NOME_ENT'
      Size = 100
    end
    object QrCliCNPJ_CPF: TWideStringField
      FieldName = 'CNPJ_CPF'
      Size = 18
    end
    object QrCliNO2_ENT: TWideStringField
      FieldName = 'NO2_ENT'
      Size = 60
    end
    object QrCliIE_RG: TWideStringField
      FieldName = 'IE_RG'
    end
    object QrCliRUA: TWideStringField
      FieldName = 'RUA'
      Size = 30
    end
    object QrCliNUMERO: TIntegerField
      FieldName = 'NUMERO'
    end
    object QrCliCOMPL: TWideStringField
      FieldName = 'COMPL'
      Size = 30
    end
    object QrCliBAIRRO: TWideStringField
      FieldName = 'BAIRRO'
      Size = 30
    end
    object QrCliCIDADE: TWideStringField
      FieldName = 'CIDADE'
      Size = 25
    end
    object QrCliUF: TSmallintField
      FieldName = 'UF'
    end
    object QrCliNOMELOGRAD: TWideStringField
      FieldName = 'NOMELOGRAD'
      Size = 10
    end
    object QrCliNOMEUF: TWideStringField
      FieldName = 'NOMEUF'
      Size = 2
    end
    object QrCliPais: TWideStringField
      FieldName = 'Pais'
    end
    object QrCliLograd: TSmallintField
      FieldName = 'Lograd'
    end
    object QrCliCEP: TIntegerField
      FieldName = 'CEP'
    end
    object QrCliENDEREF: TWideStringField
      FieldName = 'ENDEREF'
      Size = 100
    end
    object QrCliTE1: TWideStringField
      FieldName = 'TE1'
    end
    object QrCliFAX: TWideStringField
      FieldName = 'FAX'
    end
    object QrCliCAD_FEDERAL: TWideStringField
      FieldName = 'CAD_FEDERAL'
      Required = True
      Size = 4
    end
    object QrCliCAD_ESTADUAL: TWideStringField
      FieldName = 'CAD_ESTADUAL'
      Required = True
      Size = 4
    end
  end
  object QrWUsers: TMySQLQuery
    Database = Dmod.MyDBn
    SQL.Strings = (
      'SELECT Codigo, PersonalName,'
      'CONCAT(PersonalName, " (", '
      'CASE Tipo'
      'WHEN 0 THEN "Administrador"'
      'WHEN 1 THEN "Boss"'
      'WHEN 2 THEN "Cliente"'
      'WHEN 3 THEN "Usu'#225'rio" END, ")") NOME_TXT'
      'FROM wusers'
      'WHERE Entidade=:P0'
      'AND Ativo = 1'
      'ORDER BY PersonalName')
    Left = 101
    Top = 10
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrWUsersCodigo: TAutoIncField
      FieldName = 'Codigo'
    end
    object QrWUsersPersonalName: TWideStringField
      FieldName = 'PersonalName'
      Size = 32
    end
    object QrWUsersNOME_TXT: TWideStringField
      FieldName = 'NOME_TXT'
      Size = 100
    end
  end
  object DsWUsers: TDataSource
    DataSet = QrWUsers
    Left = 129
    Top = 10
  end
  object frxDsCli: TfrxDBDataset
    UserName = 'frxDsCli'
    CloseDataSource = False
    FieldAliases.Strings = (
      'CNPJ_TXT=CNPJ_TXT'
      'TE1_TXT=TE1_TXT'
      'E_ALL=E_ALL'
      'FAX_TXT=FAX_TXT'
      'NUMERO_TXT=NUMERO_TXT'
      'CEP_TXT=CEP_TXT'
      'IE_TXT=IE_TXT'
      'Codigo=Codigo'
      'Tipo=Tipo'
      'IE=IE'
      'NOME_ENT=NOME_ENT'
      'CNPJ_CPF=CNPJ_CPF'
      'NO2_ENT=NO2_ENT'
      'IE_RG=IE_RG'
      'RUA=RUA'
      'NUMERO=NUMERO'
      'COMPL=COMPL'
      'BAIRRO=BAIRRO'
      'CIDADE=CIDADE'
      'UF=UF'
      'NOMELOGRAD=NOMELOGRAD'
      'NOMEUF=NOMEUF'
      'Pais=Pais'
      'Lograd=Lograd'
      'CEP=CEP'
      'ENDEREF=ENDEREF'
      'TE1=TE1'
      'FAX=FAX'
      'CAD_FEDERAL=CAD_FEDERAL'
      'CAD_ESTADUAL=CAD_ESTADUAL')
    DataSet = QrCli
    BCDToCurrency = False
    DataSetOptions = []
    Left = 584
    Top = 10
  end
  object QrAplic: TMySQLQuery
    Database = Dmod.MyDBn
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM aplicativos'
      'WHERE Ativo = 1'
      'AND Codigo=:P0')
    Left = 557
    Top = 58
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrAplicCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'aplicativos.Codigo'
    end
    object QrAplicNome: TWideStringField
      FieldName = 'Nome'
      Origin = 'aplicativos.Nome'
      Size = 50
    end
  end
  object DsAplic: TDataSource
    DataSet = QrAplic
    Left = 585
    Top = 58
  end
  object QrFTPDir: TMySQLQuery
    Database = Dmod.MyDBn
    SQL.Strings = (
      'SELECT dir.Codigo, dir.FTPConfig, dir.Caminho'
      'FROM ftpwebdir dir'
      'WHERE dir.Codigo=:P0')
    Left = 298
    Top = 13
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrFTPDirFTPConfig: TIntegerField
      FieldName = 'FTPConfig'
    end
    object QrFTPDirCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrFTPDirCaminho: TWideStringField
      FieldName = 'Caminho'
      Size = 255
    end
  end
  object QrWOrdSerOpc: TMySQLQuery
    Database = Dmod.MyDBn
    SQL.Strings = (
      'SELECT opc.*, '
      ' st1.Nome STATUS1, st2.Nome STATUS2, '
      'st3.Nome STATUS3, st4.Nome STATUS4,'
      'dir.FTPConfig, dir.Caminho'
      'FROM wordseropc opc'
      'LEFT JOIN wordsersta st1 ON st1.Codigo = opc.GruStatus1'
      'LEFT JOIN wordsersta st2 ON st2.Codigo = opc.GruStatus2'
      'LEFT JOIN wordsersta st3 ON st3.Codigo = opc.GruStatus3'
      'LEFT JOIN wordsersta st4 ON st4.Codigo = opc.GruStatus4'
      'LEFT JOIN ftpwebdir dir ON dir.Codigo =  opc.DirFTPPad ')
    Left = 354
    Top = 13
  end
  object QrWOrSeAteIt: TMySQLQuery
    Database = Dmod.MyDBn
    Left = 178
    Top = 285
  end
  object QrWOrdSerAss: TMySQLQuery
    Database = Dmod.MyDBn
    SQL.Strings = (
      'SELECT Codigo, Nome, Servico'
      'FROM wordserass'
      'WHERE Ativo = 1'
      'ORDER BY Nome')
    Left = 202
    Top = 122
    object QrWOrdSerAssCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrWOrdSerAssNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QrWOrdSerAssDescri: TWideMemoField
      FieldName = 'Descri'
      BlobType = ftWideMemo
      Size = 4
    end
  end
  object DsWOrdSerAss: TDataSource
    DataSet = QrWOrdSerAss
    Left = 230
    Top = 122
  end
  object QrLoc: TMySQLQuery
    Database = Dmod.MyDBn
    Left = 430
    Top = 181
  end
  object DsWOrdSerGruPes: TDataSource
    DataSet = QrWOrdSerGruPes
    Left = 350
    Top = 122
  end
  object QrWOrdSerGruPes: TMySQLQuery
    Database = Dmod.MyDBn
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM wordsergru'
      'WHERE Ativo = 1'
      'ORDER BY Nome')
    Left = 322
    Top = 122
    object IntegerField2: TIntegerField
      FieldName = 'Codigo'
    end
    object StringField1: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object QrWOSOpcUsu: TMySQLQuery
    Database = Dmod.MyDBn
    SQL.Strings = (
      'SELECT opc.*,'
      ' st1.Nome STATUS1, st2.Nome STATUS2, '
      'st3.Nome STATUS3, st4.Nome STATUS4'
      'FROM wordseropc opc'
      'LEFT JOIN wordsersta st1 ON st1.Codigo = opc.GruStatus1'
      'LEFT JOIN wordsersta st2 ON st2.Codigo = opc.GruStatus2'
      'LEFT JOIN wordsersta st3 ON st3.Codigo = opc.GruStatus3'
      'LEFT JOIN wordsersta st4 ON st4.Codigo = opc.GruStatus4')
    Left = 426
    Top = 13
  end
  object QrRequisitante: TMySQLQuery
    Database = Dmod.MyDBn
    SQL.Strings = (
      'SELECT Codigo, PersonalName'
      'FROM wusers'
      'WHERE Ativo = 1'
      'AND Tipo IN(0, 1)'
      'ORDER BY PersonalName')
    Left = 308
    Top = 250
    object AutoIncField1: TAutoIncField
      FieldName = 'Codigo'
    end
    object StringField3: TWideStringField
      FieldName = 'PersonalName'
      Size = 32
    end
  end
  object DsRequisitante: TDataSource
    DataSet = QrRequisitante
    Left = 336
    Top = 250
  end
end
