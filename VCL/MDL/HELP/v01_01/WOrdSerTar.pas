unit WOrdSerTar;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkPermissoes,
  dmkDBGrid, mySQLDbTables;

type
  TFmWOrdSerTar = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtInclui: TBitBtn;
    LaTitulo1C: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    DBGItens: TdmkDBGrid;
    QrWOrdSerTar: TmySQLQuery;
    QrWOrdSerTarCodigo: TIntegerField;
    DsWOrdSerTar: TDataSource;
    QrWOrdSerTarAgenTarIts: TIntegerField;
    BtLocalizar: TBitBtn;
    BtExclui: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure BtIncluiClick(Sender: TObject);
    procedure BtLocalizarClick(Sender: TObject);
    procedure DBGItensDblClick(Sender: TObject);
    procedure BtExcluiClick(Sender: TObject);
  private
    { Private declarations }
    procedure LocalizaItem();
  public
    { Public declarations }
    FCodigo, FPrioridade: Integer;
    FDescri: String;
    FDataCad: TDate;
  end;

  var
  FmWOrdSerTar: TFmWOrdSerTar;

implementation

uses Module, UnMyObjects, UnitWOrdSer, UnAppPF;

{$R *.DFM}

procedure TFmWOrdSerTar.BtLocalizarClick(Sender: TObject);
begin
  LocalizaItem();
end;

procedure TFmWOrdSerTar.BtExcluiClick(Sender: TObject);
begin
  if (QrWOrdSerTar.State <> dsInactive) and (QrWOrdSerTar.RecordCount > 0) then
  begin
    if AppPF.AGEN_ExcluiTarefasItem(Dmod.MyDB, Dmod.QrAux,
      QrWOrdSerTarAgenTarIts.Value, '', True) then
    begin
      UnWOrdSer.ExcluiWOrdSerTar(QrWOrdSerTarAgenTarIts.Value);
      UnWOrdSer.ReabreWOrdSerTar(QrWOrdSerTar, FCodigo);
    end;
  end;
end;

procedure TFmWOrdSerTar.BtIncluiClick(Sender: TObject);
var
  AgenTarIts: Integer;
begin
  AgenTarIts := AppPF.AGEN_CriaTarefa(1, Trunc(FCodigo), FDescri, FPrioridade,
                  FDataCad);
  //
  if (AgenTarIts <> 0) and (FCodigo <> 0) then
  begin
    UnWOrdSer.InsereWOrdSerTar(FCodigo, AgenTarIts);
    UnWOrdSer.ReabreWOrdSerTar(QrWOrdSerTar, FCodigo);
  end;
end;

procedure TFmWOrdSerTar.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmWOrdSerTar.DBGItensDblClick(Sender: TObject);
begin
  LocalizaItem();
end;

procedure TFmWOrdSerTar.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmWOrdSerTar.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
end;

procedure TFmWOrdSerTar.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmWOrdSerTar.FormShow(Sender: TObject);
begin
  UnWOrdSer.ReabreWOrdSerTar(QrWOrdSerTar, FCodigo);
end;

procedure TFmWOrdSerTar.LocalizaItem;
begin
  if (QrWOrdSerTar.State <> dsInactive) and (QrWOrdSerTar.RecordCount > 0) then
  begin
    AppPF.AGEN_MostraFormAgenTarGer(FmWOrdSerTar, False, nil, nil, QrWOrdSerTarAgenTarIts.Value);
    UnWOrdSer.ReabreWOrdSerTar(QrWOrdSerTar, FCodigo);
  end;
end;

end.