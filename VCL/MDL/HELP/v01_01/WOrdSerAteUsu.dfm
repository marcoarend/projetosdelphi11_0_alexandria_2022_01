object FmWOrdSerAteUsu: TFmWOrdSerAteUsu
  Left = 339
  Top = 185
  Caption = 'WEB-ORSER-013 :: Atendentes da Ordem de Servi'#231'o - Usu'#225'rios'
  ClientHeight = 414
  ClientWidth = 634
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelConfirma: TPanel
    Left = 0
    Top = 366
    Width = 634
    Height = 48
    Align = alBottom
    TabOrder = 1
    object BtOK: TBitBtn
      Tag = 14
      Left = 20
      Top = 4
      Width = 90
      Height = 40
      Caption = '&OK'
      TabOrder = 0
      OnClick = BtOKClick
      NumGlyphs = 2
    end
    object Panel2: TPanel
      Left = 522
      Top = 1
      Width = 111
      Height = 46
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 2
        Top = 3
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
        NumGlyphs = 2
      end
    end
    object BtTodos: TBitBtn
      Tag = 127
      Left = 228
      Top = 4
      Width = 90
      Height = 40
      Caption = '&Todos'
      TabOrder = 2
      OnClick = BtTodosClick
      NumGlyphs = 2
    end
    object BtNenhum: TBitBtn
      Tag = 128
      Left = 324
      Top = 4
      Width = 90
      Height = 40
      Caption = '&Nenhum'
      TabOrder = 3
      OnClick = BtNenhumClick
      NumGlyphs = 2
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 634
    Height = 48
    Align = alTop
    Caption = 'Atendentes da Ordem de Servi'#231'o - Usu'#225'rios'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 2
    object Image1: TImage
      Left = 1
      Top = 1
      Width = 632
      Height = 46
      Align = alClient
      Transparent = True
      ExplicitLeft = 2
      ExplicitTop = 2
      ExplicitWidth = 788
      ExplicitHeight = 44
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 634
    Height = 318
    Align = alClient
    TabOrder = 0
    object Panel3: TPanel
      Left = 1
      Top = 1
      Width = 632
      Height = 48
      Align = alTop
      TabOrder = 0
      object Label9: TLabel
        Left = 8
        Top = 4
        Width = 45
        Height = 13
        Caption = 'Entidade:'
      end
      object CBEntidade: TdmkDBLookupComboBox
        Left = 64
        Top = 20
        Width = 560
        Height = 21
        KeyField = 'CodUsu'
        ListField = 'Nome'
        ListSource = DsEntidade
        TabOrder = 1
        dmkEditCB = EdEntidade
        UpdType = utYes
      end
      object EdEntidade: TdmkEditCB
        Left = 8
        Top = 20
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        OnChange = EdEntidadeChange
        DBLookupComboBox = CBEntidade
      end
    end
    object dmkDBGridDAC1: TdmkDBGridDAC
      Left = 1
      Top = 49
      Width = 632
      Height = 268
      OnAfterSQLExec = dmkDBGridDAC1AfterSQLExec
      SQLFieldsToChange.Strings = (
        'Ativo')
      SQLIndexesOnUpdate.Strings = (
        'Codigo')
      Align = alClient
      Columns = <
        item
          Expanded = False
          FieldName = 'Ativo'
          Title.Caption = 'Ok'
          Width = 17
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Codigo'
          Title.Caption = 'ID'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Nome'
          Width = 250
          Visible = True
        end>
      Color = clWindow
      DataSource = DsTmpWUsers
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
      TabOrder = 1
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      EditForceNextYear = False
      Columns = <
        item
          Expanded = False
          FieldName = 'Ativo'
          Title.Caption = 'Ok'
          Width = 17
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Codigo'
          Title.Caption = 'ID'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Nome'
          Width = 250
          Visible = True
        end>
    end
  end
  object QrEntidade: TmySQLQuery
    Database = Dmod.MyDBn
    SQL.Strings = (
      'SELECT DISTINCT(ent.Codigo) Codigo, ent.CodUsu,'
      'IF (ent.Tipo=0, ent.RazaoSocial, ent.Nome) Nome'
      'FROM wusers wus'
      'LEFT JOIN entidades  ent ON ent.Codigo = wus.Entidade'
      'WHERE ent.Codigo > 0  '
      'AND ent.Ativo = 1'
      'ORDER BY Nome')
    Left = 58
    Top = 177
    object QrEntidadeCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEntidadeCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrEntidadeNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
  object DsEntidade: TDataSource
    DataSet = QrEntidade
    Left = 86
    Top = 177
  end
  object VUEntidade: TdmkValUsu
    dmkEditCB = EdEntidade
    QryCampo = 'Atendente'
    RefCampo = 'Codigo'
    UpdType = utYes
    Left = 114
    Top = 177
  end
  object QrWUsers: TmySQLQuery
    Database = Dmod.MyDBn
    SQL.Strings = (
      'SELECT Codigo, PersonalName'
      'FROM wusers'
      'WHERE Entidade=:P0'
      'AND Codigo NOT IN (SELECT Usuario FROM worseateit)'
      'AND Ativo = 1')
    Left = 142
    Top = 177
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrWUsersCodigo: TAutoIncField
      FieldName = 'Codigo'
    end
    object QrWUsersPersonalName: TWideStringField
      FieldName = 'PersonalName'
      Size = 32
    end
  end
  object DsWUsers: TDataSource
    DataSet = QrWUsers
    Left = 170
    Top = 177
  end
  object QrTmpWUsers: TmySQLQuery
    Database = DModG.MyPID_DB
    Left = 198
    Top = 177
    object QrTmpWUsersCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrTmpWUsersAtivo: TSmallintField
      FieldName = 'Ativo'
      MaxValue = 1
    end
    object QrTmpWUsersNome: TWideStringField
      FieldName = 'Nome'
      Size = 255
    end
  end
  object DsTmpWUsers: TDataSource
    DataSet = QrTmpWUsers
    Left = 226
    Top = 177
  end
end
