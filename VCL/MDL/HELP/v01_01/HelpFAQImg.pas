unit HelpFAQImg;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ExtCtrls, Vcl.StdCtrls, dmkEdit,
  dmkCheckBox, Vcl.Buttons;

type
  TFmHelpFAQImg = class(TForm)
    Panel1: TPanel;
    Label32: TLabel;
    EdImg: TdmkEdit;
    EdLegenda: TdmkEdit;
    Label1: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure EdImgKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
    FImg, FLegenda: String;
  end;

var
  FmHelpFAQImg: TFmHelpFAQImg;

implementation

uses UnMyObjects;

{$R *.dfm}

procedure TFmHelpFAQImg.BtOKClick(Sender: TObject);
begin
  FImg     := Trim(EdImg.ValueVariant);
  FLegenda := Trim(EdLegenda.ValueVariant);
  //
  if MyObjects.FIC(FImg = '', EdImg, 'Informe a imagem!') then
    Exit;
  //
  Close;
end;

procedure TFmHelpFAQImg.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmHelpFAQImg.EdImgKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  Arquivo: String;
begin
  if key = VK_F4 then
  begin
    Arquivo := ExtractFileName(EdImg.ValueVariant);
    //
    if MyObjects.FileOpenDialog(FmHelpFAQImg, '', Arquivo,
      'Selecione uma imagem', '', [], Arquivo)
    then
      EdImg.ValueVariant := Arquivo;
  end;
end;

procedure TFmHelpFAQImg.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmHelpFAQImg.FormCreate(Sender: TObject);
begin
  FImg     := '';
  FLegenda := '';
end;

procedure TFmHelpFAQImg.FormShow(Sender: TObject);
begin
  EdImg.SetFocus;
end;

end.
