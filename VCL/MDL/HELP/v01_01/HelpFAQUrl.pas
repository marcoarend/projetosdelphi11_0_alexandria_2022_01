unit HelpFAQUrl;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs,
  Vcl.ExtCtrls, Vcl.StdCtrls, dmkEdit, dmkCheckBox, Vcl.Buttons, Vcl.ComCtrls,
  Vcl.DBCtrls, dmkDBLookupComboBox, dmkEditCB, Data.DB, mySQLDbTables,
  UnGrlHelp;

type
  TFmHelpFAQUrl = class(TForm)
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    PageControl1: TPageControl;
    TSFaq: TTabSheet;
    TSLinkExt: TTabSheet;
    Panel1: TPanel;
    Label32: TLabel;
    Label1: TLabel;
    EdURL: TdmkEdit;
    EdDescricao: TdmkEdit;
    Label4: TLabel;
    EdHelpFAQ: TdmkEditCB;
    CBHelpFAQ: TdmkDBLookupComboBox;
    Label2: TLabel;
    EdHelpFAQDescri: TdmkEdit;
    QrHelpFAQ: TmySQLQuery;
    QrHelpFAQCodigo: TIntegerField;
    DsHelpFAQ: TDataSource;
    QrHelpFAQNome: TWideStringField;
    SBHelpFAQ: TSpeedButton;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure EdHelpFAQChange(Sender: TObject);
    procedure SBHelpFAQClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    FUrl, FDescri: String;
    FFAQ: Integer;
    FConfirmou, FLinkExt: Boolean;
    FHelpTipo: THelpTip;
  end;

var
  FmHelpFAQUrl: TFmHelpFAQUrl;

implementation

uses UnMyObjects, Module, UnInternalConsts, UnitHelpJan, UMySQLModule;

{$R *.dfm}

procedure TFmHelpFAQUrl.BtOKClick(Sender: TObject);
begin
  if PageControl1.ActivePageIndex = 0 then
  begin
    FDescri    := Trim(EdHelpFAQDescri.ValueVariant);
    FFAQ       := EdHelpFAQ.ValueVariant;
    FUrl       := '';
    FConfirmou := True;
    //
    if MyObjects.FIC((FFAQ = 0) and (TSLinkExt.TabVisible = True), EdHelpFAQ, 'Informe o tutorial!') then Exit;
    if MyObjects.FIC((FDescri = '') and (TSLinkExt.TabVisible = True), EdHelpFAQDescri, 'Informe uma descri��o!') then Exit;
  end else
  begin
    FUrl       := Trim(EdURL.ValueVariant);
    FDescri    := Trim(EdDescricao.ValueVariant);
    FFAQ       := 0;
    FConfirmou := True;
    //
    if MyObjects.FIC(FDescri = '', EdDescricao, 'Informe uma descri��o!') then Exit;
  end;
  Close;
end;

procedure TFmHelpFAQUrl.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmHelpFAQUrl.EdHelpFAQChange(Sender: TObject);
begin
  EdHelpFAQDescri.ValueVariant := CBHelpFAQ.Text;
end;

procedure TFmHelpFAQUrl.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmHelpFAQUrl.FormCreate(Sender: TObject);
begin
  CBHelpFAQ.ListSource := DsHelpFAQ;
  //
  FUrl       := '';
  FDescri    := '';
  FFAQ       := 0;
  FConfirmou := False;
  FLinkExt   := True;
end;

procedure TFmHelpFAQUrl.FormShow(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 0;
  TSLinkExt.TabVisible         := FLinkExt;
  //
  GrlHelp.ReopenHelpFAQ(Dmod.MyDB, QrHelpFAQ, 0, '', '', FHelpTipo);
  //
  EdHelpFAQ.SetFocus;
end;

procedure TFmHelpFAQUrl.SBHelpFAQClick(Sender: TObject);
var
  HelpFAQ: Integer;
begin
  VAR_CADASTRO := 0;
  HelpFAQ      := EdHelpFAQ.ValueVariant;
  //
  UnHelpJan.MostraFormHelpPsq(FmHelpFAQUrl, False, nil, nil, False);
  //
  if VAR_CADASTRO > 0 then
    UMyMod.SetaCodigoPesquisado(EdHelpFAQ, CBHelpFAQ, QrHelpFAQ, VAR_CADASTRO);
end;

end.
