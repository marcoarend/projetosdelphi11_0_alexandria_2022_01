unit WOrdSerEnc;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, dmkDBGrid, mySQLDbTables, Variants,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, DmkDAC_PF, dmkPermissoes, UnDmkEnums;

type
  TFmWOrdSerEnc = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    QrWOrdSerSta: TmySQLQuery;
    IntegerField5: TIntegerField;
    QrWOrdSerStaNome: TWideStringField;
    DsWOrdSerSta: TDataSource;
    QrWOrdSerEnc: TmySQLQuery;
    QrWOrdSerEncCodigo: TIntegerField;
    QrWOrdSerEncNome: TWideStringField;
    QrWOrdSerEncDescri: TWideMemoField;
    DsWOrdSerEnc: TDataSource;
    Panel5: TPanel;
    Label2: TLabel;
    SpeedButton7: TSpeedButton;
    LaData: TLabel;
    LaHora: TLabel;
    EdStatus: TdmkEditCB;
    CBStatus: TdmkDBLookupComboBox;
    TPData: TDateTimePicker;
    TPHora: TDateTimePicker;
    dmkPermissoes1: TdmkPermissoes;
    ImgWEB: TdmkImage;
    PageControl1: TPageControl;
    TSResol: TTabSheet;
    TSReab: TTabSheet;
    PnSolucao: TPanel;
    DBMemo1: TDBMemo;
    Panel6: TPanel;
    SpeedButton1: TSpeedButton;
    Label1: TLabel;
    EdWOrdSerEnc: TdmkEditCB;
    CBWOrdSerEnc: TdmkDBLookupComboBox;
    MeMotivReopen: TMemo;
    QrWOrdSerStaWOrdSerEnc: TIntegerField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SpeedButton7Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure EdStatusChange(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenWOrdSerEnc(Nome: String; Codigo: Integer);
    procedure ReopenWOrdSerSta(Encerrou: Boolean);
  public
    { Public declarations }
    FCodigo, FFinaliz, FAssunto: Integer;
    FDescri: String;
    FEncerrou: Boolean;
    FAbertura: TDateTime;
  end;

  var
  FmWOrdSerEnc: TFmWOrdSerEnc;

implementation

uses UnMyObjects, Module, WOrdSer, ModWOrdSer, UnMLAGeral, UnDmkWeb, UnGrl_Vars,
  UMySQLModule, ModuleGeral;

{$R *.DFM}

procedure TFmWOrdSerEnc.BtOKClick(Sender: TObject);
var
  Status, Encerrou, WOrdSerEnc, Resul: Integer;
  TimeZoneDifUTC, DataHora, DataHoraTZ, MotivReopen, MensagemAviso: String;
  DataHoraUTC: TDateTime;
begin
  if not DmkWeb.ConectarUsuarioWEB(True) then Exit;
  //
  Status   := EdStatus.ValueVariant;
  Encerrou := FFinaliz;
  //
  if MyObjects.FIC(Status = 0, EdStatus, 'Defina o status!') then Exit;
  //
  if Encerrou = 1 then //Reabertura
  begin
    WOrdSerEnc  := 0;
    Encerrou    := 0;
    DataHora    := '0000-00-00 00:00:00';
    MotivReopen := MeMotivReopen.Text;
    //
    if MyObjects.FIC(MotivReopen = '', MeMotivReopen, 'Defina o motivo da reabertura!') then Exit;    
    //
    Resul := DmkWeb.WOrdSerEnc(31, FCodigo, Status, DataHora, WOrdSerEnc, MotivReopen, MensagemAviso);
  end else
  begin
    TimeZoneDifUTC := VAR_WEB_USR_TZDIFUTC;
    DataHoraTZ     := Geral.FDT(Int(TPData.Date) + TPHora.Time - Int(TPHora.Time), 9);
    WOrdSerEnc     := EdWOrdSerEnc.ValueVariant;
    Encerrou       := 1;
    //
    if MyObjects.FIC(WOrdSerEnc = 0, EdWOrdSerEnc, 'Defina uma resolu��o!') then Exit;
    //
    DataHora    := Geral.FDT(Int(TPData.Date) + TPHora.Time - Int(TPHora.Time), 9);
    DataHoraUTC := DmkWeb.DataTimeZoneToNewTimeZone(Dmod.MyDBn, DModWOrdSer.QrLoc, DataHora, TimeZoneDifUTC, '+00:00');
    DataHora    := Geral.FDT(DataHoraUTC, 9);
    //
    if MyObjects.FIC(not DModWOrdSer.VerificaMaxData(FCodigo, 0, FAbertura, DataHoraUTC),
      TPData, 'Data selecionada � inferior a �ltima data cadastrada para este item!') then Exit;
    //
    Resul := DmkWeb.WOrdSerEnc(31, FCodigo, Status, DataHoraTZ, WOrdSerEnc, '', MensagemAviso);
  end;
  //
  if Resul > 0 then
  begin
    if Resul = 103 then
      Geral.MB_Aviso(MensagemAviso);
    Close;
  end else
    Geral.MB_Aviso('Falha ao encerrar solicita��o tente novamente mais tarde!');
end;

procedure TFmWOrdSerEnc.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmWOrdSerEnc.EdStatusChange(Sender: TObject);
begin
  if EdStatus.ValueVariant <> 0 then
  begin
    EdWOrdSerEnc.ValueVariant := QrWOrdSerStaWOrdSerEnc.Value;
    CBWOrdSerEnc.KeyValue     := QrWOrdSerStaWOrdSerEnc.Value;
  end;
end;

procedure TFmWOrdSerEnc.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  DmkWeb.CarregaImagemFormWEB(ImgWEB);
end;

procedure TFmWOrdSerEnc.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  TPData.Date               := DModG.ObtemAgora();
  TPHora.Time               := DModG.ObtemAgora();
  EdStatus.ValueVariant     := 0;
  CBStatus.KeyValue         := Null;
  EdWOrdSerEnc.ValueVariant := 0;
  CBWOrdSerEnc.KeyValue     := Null;
  //
  MeMotivReopen.Text        := '';
end;

procedure TFmWOrdSerEnc.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
    [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmWOrdSerEnc.FormShow(Sender: TObject);
begin
  ReopenWOrdSerSta(FEncerrou);
  ReopenWOrdSerEnc('', 0);
  //
  if not FEncerrou then
  begin
    TSResol.TabVisible := True;
    TSReab.TabVisible  := False;
  end else
  begin
    TSResol.TabVisible := False;
    TSReab.TabVisible  := True;
  end;
  EdStatus.SetFocus;
end;

procedure TFmWOrdSerEnc.ReopenWOrdSerEnc(Nome: String; Codigo: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrWOrdSerEnc, Dmod.MyDBn, [
    'SELECT enc.Codigo, enc.Nome, enc.Descri ',
    'FROM wordserenc enc ',
    'WHERE enc.Ativo = 1 ',
    'AND enc.Nome LIKE "%' + Nome + '%" ',
    'ORDER BY enc.Codigo ',
    '']);
  if Codigo <> 0 then
  begin
    QrWOrdSerEnc.Locate('Codigo', Codigo, []);
    //
    EdWOrdSerEnc.ValueVariant := Codigo;
    CBWOrdSerEnc.KeyValue     := Codigo;
  end;
end;

procedure TFmWOrdSerEnc.ReopenWOrdSerSta(Encerrou: Boolean);
var
  SQL, SQLNivel: String;
begin
  SQLNivel := 'AND (' + DmkWeb.VerificaSQLNiveis(VAR_WEB_USR_TIPO, 'Nivel') + ' OR Nivel & "1")';
  //
  if Encerrou then
    SQL := 'AND WOrdSerEnc = 0'
  else
    SQL := 'AND WOrdSerEnc > 0';
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrWOrdSerSta, Dmod.MyDBn, [
    'SELECT Codigo, Nome, WOrdSerEnc ',
    'FROM wordsersta ',
    'WHERE Ativo = 1 ',
    SQL,
    SQLNivel,
    'ORDER BY Nome ',
    '']);
end;

procedure TFmWOrdSerEnc.SpeedButton1Click(Sender: TObject);
begin
  DModWOrdSer.MostraWOrdSerSol(EdWOrdSerEnc.ValueVariant);
  if VAR_CADASTRO > 0 then
  begin
    ReopenWOrdSerEnc('', VAR_CADASTRO);
    //
    EdWOrdSerEnc.ValueVariant := VAR_CADASTRO;
    CBWOrdSerEnc.KeyValue     := VAR_CADASTRO;
    EdWOrdSerEnc.SetFocus;
  end;
end;

procedure TFmWOrdSerEnc.SpeedButton7Click(Sender: TObject);
begin
  DModWOrdSer.MostraWOrdSerSta(EdStatus.ValueVariant);
  if VAR_CADASTRO > 0 then
  begin
    DModWOrdSer.ReopenWOrdSerSta(VAR_WEB_USR_TIPO, 0);
    //
    EdStatus.ValueVariant := VAR_CADASTRO;
    CBStatus.KeyValue     := VAR_CADASTRO;
    EdStatus.SetFocus;
  end;
end;

end.
