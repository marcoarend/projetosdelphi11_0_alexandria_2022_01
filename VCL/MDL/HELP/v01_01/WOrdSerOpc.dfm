object FmWOrdSerOpc: TFmWOrdSerOpc
  Left = 339
  Top = 185
  Caption = 'WEB-ORSER-007 :: Op'#231#245'es Ordem de Servi'#231'o'
  ClientHeight = 790
  ClientWidth = 1162
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -14
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 120
  TextHeight = 16
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1162
    Height = 59
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 1054
      Top = 0
      Width = 108
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 59
        Top = 14
        Width = 39
        Height = 39
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Transparent = True
        SQLType = stNil
      end
      object ImgWEB: TdmkImage
        Left = 10
        Top = 14
        Width = 39
        Height = 39
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 59
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 59
      Top = 0
      Width = 995
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 9
        Top = 11
        Width = 379
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Op'#231#245'es Ordem de Servi'#231'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 11
        Top = 14
        Width = 379
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Op'#231#245'es Ordem de Servi'#231'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 10
        Top = 12
        Width = 379
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Op'#231#245'es Ordem de Servi'#231'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 59
    Width = 1162
    Height = 591
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 1162
      Height = 591
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 1162
        Height = 591
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        TabOrder = 0
        object PageControl1: TPageControl
          Left = 2
          Top = 18
          Width = 1158
          Height = 571
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          ActivePage = TabSheet3
          Align = alClient
          TabOrder = 0
          object TabSheet1: TTabSheet
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Atendimento eletr'#244'nico'
            object GroupBox2: TGroupBox
              Left = 14
              Top = 6
              Width = 1120
              Height = 84
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Op'#231#245'es Web'
              TabOrder = 0
              object Label98: TLabel
                Left = 15
                Top = 18
                Width = 172
                Height = 16
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'Modo de solicita'#231#227'o padr'#227'o:'
              end
              object Label1: TLabel
                Left = 566
                Top = 18
                Width = 87
                Height = 16
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'Status padr'#227'o:'
              end
              object CBWOrdSerMod: TdmkDBLookupComboBox
                Left = 85
                Top = 42
                Width = 474
                Height = 24
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                KeyField = 'Codigo'
                ListField = 'Nome'
                ListSource = DsWOrdSerMod
                TabOrder = 1
                dmkEditCB = EdWOrdSerMod
                UpdType = utYes
                LocF7SQLMasc = '$#'
              end
              object EdWOrdSerMod: TdmkEditCB
                Left = 15
                Top = 42
                Width = 69
                Height = 26
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Alignment = taRightJustify
                TabOrder = 0
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
                DBLookupComboBox = CBWOrdSerMod
                IgnoraDBLookupComboBox = False
              end
              object CBWOrdSerSta: TdmkDBLookupComboBox
                Left = 636
                Top = 42
                Width = 474
                Height = 24
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                KeyField = 'Codigo'
                ListField = 'Nome'
                ListSource = DsWOrdSerSta
                TabOrder = 3
                dmkEditCB = EdWOrdSerSta
                UpdType = utYes
                LocF7SQLMasc = '$#'
              end
              object EdWOrdSerSta: TdmkEditCB
                Left = 566
                Top = 42
                Width = 69
                Height = 26
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Alignment = taRightJustify
                TabOrder = 2
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
                DBLookupComboBox = CBWOrdSerSta
                IgnoraDBLookupComboBox = False
              end
            end
            object GroupBox3: TGroupBox
              Left = 14
              Top = 94
              Width = 553
              Height = 86
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Diret'#243'rios FTP'
              TabOrder = 1
              object Label2: TLabel
                Left = 15
                Top = 18
                Width = 192
                Height = 16
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'Diret'#243'rio para envio de arquivos'
              end
              object EdDirFTPPad: TdmkEditCB
                Left = 15
                Top = 42
                Width = 69
                Height = 26
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Alignment = taRightJustify
                TabOrder = 0
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
                DBLookupComboBox = CBDirFTPPad
                IgnoraDBLookupComboBox = False
              end
              object CBDirFTPPad: TdmkDBLookupComboBox
                Left = 86
                Top = 42
                Width = 456
                Height = 24
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                KeyField = 'Codigo'
                ListField = 'Nome'
                ListSource = DsFTPWebDir
                TabOrder = 1
                dmkEditCB = EdDirFTPPad
                UpdType = utYes
                LocF7SQLMasc = '$#'
              end
            end
            object GroupBox4: TGroupBox
              Left = 14
              Top = 185
              Width = 1120
              Height = 197
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'E-mails padr'#227'o'
              TabOrder = 2
              object Label4: TLabel
                Left = 15
                Top = 18
                Width = 222
                Height = 16
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'E-mail padr'#227'o para abertura de O. S.:'
              end
              object Label5: TLabel
                Left = 566
                Top = 18
                Width = 238
                Height = 16
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'E-mail padr'#227'o para coment'#225'rio na O. S.:'
              end
              object Label6: TLabel
                Left = 15
                Top = 78
                Width = 255
                Height = 16
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'E-mail padr'#227'o para encerramento de O. S.:'
              end
              object Label3: TLabel
                Left = 566
                Top = 78
                Width = 279
                Height = 16
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'E-mail padr'#227'o para informar a cria'#231#227'o de O. S.:'
              end
              object Label14: TLabel
                Left = 15
                Top = 137
                Width = 293
                Height = 16
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'E-mail padr'#227'o para altera'#231#245'es de status de O. S.:'
              end
              object CBEnvMailAbe: TdmkDBLookupComboBox
                Left = 85
                Top = 42
                Width = 474
                Height = 24
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                KeyField = 'Codigo'
                ListField = 'Nome'
                ListSource = DsEnvMailAbe
                TabOrder = 1
                dmkEditCB = EdEnvMailAbe
                UpdType = utYes
                LocF7SQLMasc = '$#'
              end
              object EdEnvMailAbe: TdmkEditCB
                Left = 15
                Top = 42
                Width = 69
                Height = 26
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Alignment = taRightJustify
                TabOrder = 0
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
                DBLookupComboBox = CBEnvMailAbe
                IgnoraDBLookupComboBox = False
              end
              object CBEnvMailCom: TdmkDBLookupComboBox
                Left = 636
                Top = 42
                Width = 474
                Height = 24
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                KeyField = 'Codigo'
                ListField = 'Nome'
                ListSource = DsEnvMailCom
                TabOrder = 3
                dmkEditCB = EdEnvMailCom
                UpdType = utYes
                LocF7SQLMasc = '$#'
              end
              object EdEnvMailCom: TdmkEditCB
                Left = 566
                Top = 42
                Width = 69
                Height = 26
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Alignment = taRightJustify
                TabOrder = 2
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
                DBLookupComboBox = CBEnvMailCom
                IgnoraDBLookupComboBox = False
              end
              object EdEnvMailEnc: TdmkEditCB
                Left = 15
                Top = 101
                Width = 69
                Height = 26
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Alignment = taRightJustify
                TabOrder = 4
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
                DBLookupComboBox = CBEnvMailEnc
                IgnoraDBLookupComboBox = False
              end
              object CBEnvMailEnc: TdmkDBLookupComboBox
                Left = 85
                Top = 101
                Width = 474
                Height = 24
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                KeyField = 'Codigo'
                ListField = 'Nome'
                ListSource = DsEnvMailEnc
                TabOrder = 5
                dmkEditCB = EdEnvMailEnc
                UpdType = utYes
                LocF7SQLMasc = '$#'
              end
              object EdEnvMailRes: TdmkEditCB
                Left = 566
                Top = 101
                Width = 69
                Height = 26
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Alignment = taRightJustify
                TabOrder = 6
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
                DBLookupComboBox = CBEnvMailRes
                IgnoraDBLookupComboBox = False
              end
              object CBEnvMailRes: TdmkDBLookupComboBox
                Left = 642
                Top = 101
                Width = 474
                Height = 24
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                KeyField = 'Codigo'
                ListField = 'Nome'
                ListSource = DsEnvMailRes
                TabOrder = 7
                dmkEditCB = EdEnvMailRes
                UpdType = utYes
                LocF7SQLMasc = '$#'
              end
              object EdEnvMailSta: TdmkEditCB
                Left = 15
                Top = 160
                Width = 69
                Height = 26
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Alignment = taRightJustify
                TabOrder = 8
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
                DBLookupComboBox = CBEnvMailSta
                IgnoraDBLookupComboBox = False
              end
              object CBEnvMailSta: TdmkDBLookupComboBox
                Left = 85
                Top = 160
                Width = 474
                Height = 24
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                KeyField = 'Codigo'
                ListField = 'Nome'
                ListSource = DsEnvMailSta
                TabOrder = 9
                dmkEditCB = EdEnvMailSta
                UpdType = utYes
                LocF7SQLMasc = '$#'
              end
            end
            object GroupBox5: TGroupBox
              Left = 562
              Top = 389
              Width = 573
              Height = 84
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Op'#231#245'es Aplicativos Dermatek'
              TabOrder = 4
              object Label7: TLabel
                Left = 15
                Top = 21
                Width = 172
                Height = 16
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'Modo de solicita'#231#227'o padr'#227'o:'
              end
              object CBWOrdSerModApl: TdmkDBLookupComboBox
                Left = 85
                Top = 42
                Width = 474
                Height = 24
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                KeyField = 'Codigo'
                ListField = 'Nome'
                ListSource = DsWOrdSerModApl
                TabOrder = 1
                dmkEditCB = EdWOrdSerModApl
                UpdType = utYes
                LocF7SQLMasc = '$#'
              end
              object EdWOrdSerModApl: TdmkEditCB
                Left = 15
                Top = 42
                Width = 69
                Height = 26
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Alignment = taRightJustify
                TabOrder = 0
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
                DBLookupComboBox = CBWOrdSerModApl
                IgnoraDBLookupComboBox = False
              end
            end
            object GroupBox6: TGroupBox
              Left = 14
              Top = 390
              Width = 541
              Height = 136
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Op'#231#245'es aplicativo'
              TabOrder = 3
              object Label8: TLabel
                Left = 10
                Top = 49
                Width = 174
                Height = 16
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'Atualizar solicita'#231#245'es a cada '
              end
              object Label9: TLabel
                Left = 289
                Top = 49
                Width = 60
                Height = 16
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'segundos'
              end
              object Label15: TLabel
                Left = 10
                Top = 73
                Width = 118
                Height = 16
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'Ordena'#231#227'o padr'#227'o:'
              end
              object SBOrdem_CamposFields: TSpeedButton
                Left = 505
                Top = 95
                Width = 25
                Height = 26
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = '...'
                OnClick = SBOrdem_CamposFieldsClick
              end
              object EdUpdLocInte: TdmkEdit
                Left = 185
                Top = 42
                Width = 98
                Height = 26
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Alignment = taRightJustify
                TabOrder = 0
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
              end
              object EdOrdem_CamposTxt: TdmkEdit
                Left = 10
                Top = 95
                Width = 492
                Height = 26
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                ReadOnly = True
                TabOrder = 1
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object EdOrdem_CamposFields: TdmkEdit
                Left = 249
                Top = 95
                Width = 252
                Height = 26
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                ReadOnly = True
                TabOrder = 2
                Visible = False
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
            end
          end
          object TabSheet2: TTabSheet
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Grupos'
            ImageIndex = 1
            object GroupBox7: TGroupBox
              Left = 14
              Top = 6
              Width = 553
              Height = 271
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Etapas do grupo de solicita'#231#245'es'
              TabOrder = 0
              object Label10: TLabel
                Left = 15
                Top = 28
                Width = 47
                Height = 16
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'Status1:'
              end
              object Label11: TLabel
                Left = 15
                Top = 87
                Width = 47
                Height = 16
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'Status2:'
              end
              object Label12: TLabel
                Left = 15
                Top = 206
                Width = 47
                Height = 16
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'Status4:'
              end
              object Label13: TLabel
                Left = 15
                Top = 146
                Width = 47
                Height = 16
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'Status3:'
              end
              object EdGruStatus1: TdmkEditCB
                Left = 15
                Top = 52
                Width = 69
                Height = 26
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Alignment = taRightJustify
                TabOrder = 0
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
                DBLookupComboBox = CBGruStatus1
                IgnoraDBLookupComboBox = False
              end
              object CBGruStatus1: TdmkDBLookupComboBox
                Left = 86
                Top = 52
                Width = 456
                Height = 24
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                KeyField = 'Codigo'
                ListField = 'Nome'
                ListSource = DsGruStatus1
                TabOrder = 1
                dmkEditCB = EdGruStatus1
                UpdType = utYes
                LocF7SQLMasc = '$#'
              end
              object EdGruStatus2: TdmkEditCB
                Left = 15
                Top = 111
                Width = 69
                Height = 26
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Alignment = taRightJustify
                TabOrder = 2
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
                DBLookupComboBox = CBGruStatus2
                IgnoraDBLookupComboBox = False
              end
              object CBGruStatus2: TdmkDBLookupComboBox
                Left = 86
                Top = 111
                Width = 456
                Height = 24
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                KeyField = 'Codigo'
                ListField = 'Nome'
                ListSource = DsGruStatus2
                TabOrder = 3
                dmkEditCB = EdGruStatus2
                UpdType = utYes
                LocF7SQLMasc = '$#'
              end
              object EdGruStatus4: TdmkEditCB
                Left = 15
                Top = 229
                Width = 69
                Height = 26
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Alignment = taRightJustify
                TabOrder = 6
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
                DBLookupComboBox = CBGruStatus4
                IgnoraDBLookupComboBox = False
              end
              object CBGruStatus4: TdmkDBLookupComboBox
                Left = 86
                Top = 229
                Width = 456
                Height = 24
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                KeyField = 'Codigo'
                ListField = 'Nome'
                ListSource = DsGruStatus4
                TabOrder = 7
                dmkEditCB = EdGruStatus4
                UpdType = utYes
                LocF7SQLMasc = '$#'
              end
              object EdGruStatus3: TdmkEditCB
                Left = 15
                Top = 170
                Width = 69
                Height = 26
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Alignment = taRightJustify
                TabOrder = 4
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
                DBLookupComboBox = CBGruStatus3
                IgnoraDBLookupComboBox = False
              end
              object CBGruStatus3: TdmkDBLookupComboBox
                Left = 86
                Top = 170
                Width = 456
                Height = 24
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                KeyField = 'Codigo'
                ListField = 'Nome'
                ListSource = DsGruStatus3
                TabOrder = 5
                dmkEditCB = EdGruStatus3
                UpdType = utYes
                LocF7SQLMasc = '$#'
              end
            end
          end
          object TabSheet3: TTabSheet
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Acesso remoto'
            ImageIndex = 2
            object LaGrupoTeamViewer: TLabel
              Left = 411
              Top = 38
              Width = 87
              Height = 16
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Grupo padr'#227'o:'
            end
            object LaURLHelpTeamViewer: TLabel
              Left = 10
              Top = 91
              Width = 98
              Height = 16
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'URL para ajuda:'
            end
            object SBGrupoTeamViewer: TSpeedButton
              Left = 599
              Top = 58
              Width = 26
              Height = 26
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = '...'
            end
            object LaTokenTeamViewer: TLabel
              Left = 10
              Top = 38
              Width = 96
              Height = 16
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Token de script:'
            end
            object SBTokenTeamViewer: TSpeedButton
              Left = 375
              Top = 58
              Width = 26
              Height = 26
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = '?'
              OnClick = SBOrdem_CamposFieldsClick
            end
            object CkUsaTeamViewer: TCheckBox
              Left = 10
              Top = 14
              Width = 369
              Height = 20
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Ativar integra'#231#227'o com o acesso remoto Team Viewer'
              TabOrder = 0
              OnClick = CkUsaTeamViewerClick
            end
            object CBGrupoTeamViewer: TComboBox
              Left = 411
              Top = 58
              Width = 185
              Height = 24
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              AutoDropDown = True
              TabOrder = 2
            end
            object EdURLHelpTeamViewer: TdmkEdit
              Left = 10
              Top = 110
              Width = 615
              Height = 25
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              TabOrder = 3
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object EdTokenTeamViewer: TdmkEdit
              Left = 10
              Top = 58
              Width = 363
              Height = 26
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              TabOrder = 1
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
          end
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 650
    Width = 1162
    Height = 54
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 18
      Width = 1158
      Height = 34
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 16
        Top = 2
        Width = 150
        Height = 19
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -17
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 15
        Top = 1
        Width = 150
        Height = 19
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -17
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 704
    Width = 1162
    Height = 86
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 983
      Top = 18
      Width = 177
      Height = 66
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 15
        Top = 4
        Width = 147
        Height = 49
        Cursor = crHandPoint
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 18
      Width = 981
      Height = 66
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 16
        Top = 4
        Width = 148
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object QrWOrdSerMod: TmySQLQuery
    Database = Dmod.MyDBn
    SQL.Strings = (
      'SELECT * '
      'FROM wordsermod'
      'WHERE Ativo = 1'
      'ORDER BY Nome')
    Left = 412
    Top = 16
    object QrWOrdSerModCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrWOrdSerModNome: TWideStringField
      FieldName = 'Nome'
    end
  end
  object DsWOrdSerMod: TDataSource
    DataSet = QrWOrdSerMod
    Left = 440
    Top = 16
  end
  object DsWOrdSerSta: TDataSource
    DataSet = QrWOrdSerSta
    Left = 496
    Top = 16
  end
  object QrWOrdSerSta: TmySQLQuery
    Database = Dmod.MyDBn
    SQL.Strings = (
      'SELECT * '
      'FROM wordsersta'
      'WHERE Ativo = 1'
      'ORDER BY Nome')
    Left = 468
    Top = 16
    object IntegerField1: TIntegerField
      FieldName = 'Codigo'
    end
    object StringField1: TWideStringField
      FieldName = 'Nome'
    end
  end
  object DsFTPWebDir: TDataSource
    DataSet = QrFTPWebDir
    Left = 552
    Top = 16
  end
  object QrFTPWebDir: TmySQLQuery
    Database = Dmod.MyDBn
    SQL.Strings = (
      'SELECT Codigo, Nome '
      'FROM ftpwebdir'
      'WHERE TipoDir = 1'
      'ORDER BY Nome')
    Left = 524
    Top = 16
    object QrFTPWebDirCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrFTPWebDirNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object QrEnvMailAbe: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM preemail'
      'ORDER BY Nome')
    Left = 580
    Top = 16
    object QrEnvMailAbeCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEnvMailAbeNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
  object DsEnvMailAbe: TDataSource
    DataSet = QrEnvMailAbe
    Left = 608
    Top = 16
  end
  object QrEnvMailCom: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM preemail'
      'ORDER BY Nome')
    Left = 636
    Top = 16
    object QrEnvMailComCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEnvMailComNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
  object DsEnvMailCom: TDataSource
    DataSet = QrEnvMailCom
    Left = 664
    Top = 16
  end
  object QrEnvMailEnc: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM preemail'
      'ORDER BY Nome')
    Left = 692
    Top = 16
    object QrEnvMailEncCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEnvMailEncNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
  object DsEnvMailEnc: TDataSource
    DataSet = QrEnvMailEnc
    Left = 720
    Top = 16
  end
  object QrEnvMailRes: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM preemail'
      'ORDER BY Nome')
    Left = 748
    Top = 16
    object QrEnvMailResCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEnvMailResNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
  object DsEnvMailRes: TDataSource
    DataSet = QrEnvMailRes
    Left = 776
    Top = 16
  end
  object QrWOrdSerModApl: TmySQLQuery
    Database = Dmod.MyDBn
    SQL.Strings = (
      'SELECT * '
      'FROM wordsermod'
      'WHERE Ativo = 1'
      'ORDER BY Nome')
    Left = 804
    Top = 16
    object IntegerField2: TIntegerField
      FieldName = 'Codigo'
    end
    object StringField2: TWideStringField
      FieldName = 'Nome'
    end
  end
  object DsWOrdSerModApl: TDataSource
    DataSet = QrWOrdSerModApl
    Left = 832
    Top = 16
  end
  object QrWOrdSerOpc: TmySQLQuery
    Database = Dmod.MyDBn
    SQL.Strings = (
      'SELECT * '
      'FROM wordseropc')
    Left = 684
    Top = 144
  end
  object DsWOrdSerOpc: TDataSource
    DataSet = QrWOrdSerOpc
    Left = 712
    Top = 144
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 740
    Top = 144
  end
  object QrGruStatus1: TmySQLQuery
    Database = Dmod.MyDBn
    SQL.Strings = (
      'SELECT * '
      'FROM wordsersta'
      'WHERE WOrdSerEnc = 0 '
      'ORDER BY Nome')
    Left = 548
    Top = 200
    object IntegerField3: TIntegerField
      FieldName = 'Codigo'
    end
    object QrGruStatus1Nome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsGruStatus1: TDataSource
    DataSet = QrGruStatus1
    Left = 576
    Top = 200
  end
  object QrGruStatus2: TmySQLQuery
    Database = Dmod.MyDBn
    SQL.Strings = (
      'SELECT * '
      'FROM wordsersta'
      'WHERE WOrdSerEnc = 0 '
      'ORDER BY Nome')
    Left = 604
    Top = 200
    object IntegerField4: TIntegerField
      FieldName = 'Codigo'
    end
    object QrGruStatus2Nome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsGruStatus2: TDataSource
    DataSet = QrGruStatus2
    Left = 632
    Top = 200
  end
  object QrGruStatus4: TmySQLQuery
    Database = Dmod.MyDBn
    SQL.Strings = (
      'SELECT * '
      'FROM wordsersta'
      'WHERE WOrdSerEnc = 0'
      'ORDER BY Nome')
    Left = 716
    Top = 200
    object IntegerField5: TIntegerField
      FieldName = 'Codigo'
    end
    object QrGruStatus4Nome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsGruStatus4: TDataSource
    DataSet = QrGruStatus4
    Left = 744
    Top = 200
  end
  object QrGruStatus3: TmySQLQuery
    Database = Dmod.MyDBn
    SQL.Strings = (
      'SELECT * '
      'FROM wordsersta'
      'WHERE WOrdSerEnc = 0'
      'ORDER BY Nome')
    Left = 660
    Top = 200
    object IntegerField6: TIntegerField
      FieldName = 'Codigo'
    end
    object QrGruStatus3Nome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsGruStatus3: TDataSource
    DataSet = QrGruStatus3
    Left = 688
    Top = 200
  end
  object QrEnvMailSta: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM preemail'
      'ORDER BY Nome')
    Left = 356
    Top = 16
    object QrEnvMailStaCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEnvMailStaNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
  object DsEnvMailSta: TDataSource
    DataSet = QrEnvMailSta
    Left = 384
    Top = 16
  end
end
