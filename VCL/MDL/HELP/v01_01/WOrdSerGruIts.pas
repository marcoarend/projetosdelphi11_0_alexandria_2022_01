unit WOrdSerGruIts;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkDBLookupComboBox,
  dmkEditCB, mySQLDbTables, Variants, dmkEditDateTimePicker;

type
  TFmWOrdSerGruIts = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    Label9: TLabel;
    EdCodigo: TdmkEdit;
    Label12: TLabel;
    EdNome: TdmkEdit;
    Label18: TLabel;
    TPIniData: TdmkEditDateTimePicker;
    TPFimData: TdmkEditDateTimePicker;
    Label1: TLabel;
    LaRespons: TLabel;
    EdRespons: TdmkEditCB;
    CBRespons: TdmkDBLookupComboBox;
    SBRespons: TSpeedButton;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    MeObserv: TMemo;
    GroupBox7: TGroupBox;
    Label10: TLabel;
    Label11: TLabel;
    Label2: TLabel;
    Label13: TLabel;
    EdGruStatus1: TdmkEditCB;
    CBGruStatus1: TdmkDBLookupComboBox;
    EdGruStatus2: TdmkEditCB;
    CBGruStatus2: TdmkDBLookupComboBox;
    EdGruStatus4: TdmkEditCB;
    CBGruStatus4: TdmkDBLookupComboBox;
    EdGruStatus3: TdmkEditCB;
    CBGruStatus3: TdmkDBLookupComboBox;
    QrGruStatus4: TmySQLQuery;
    IntegerField5: TIntegerField;
    QrGruStatus4Nome: TWideStringField;
    DsGruStatus4: TDataSource;
    DsGruStatus3: TDataSource;
    QrGruStatus3: TmySQLQuery;
    IntegerField6: TIntegerField;
    QrGruStatus3Nome: TWideStringField;
    QrGruStatus2: TmySQLQuery;
    IntegerField4: TIntegerField;
    QrGruStatus2Nome: TWideStringField;
    DsGruStatus2: TDataSource;
    DsGruStatus1: TDataSource;
    QrGruStatus1: TmySQLQuery;
    IntegerField3: TIntegerField;
    QrGruStatus1Nome: TWideStringField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure SBResponsClick(Sender: TObject);
  private
    { Private declarations }
    procedure MostraEdicao(SQLType: TSQLType; Codigo: Integer);
  public
    { Public declarations }
    FCodigo: Integer;
    FMostra: Boolean;
    FQrWOrdSerGru: TmySQLQuery;
  end;

  var
    FmWOrdSerGruIts: TFmWOrdSerGruIts;
  const
    FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, ModWOrdSer,
  {$IfnDef SWUsr}UnWUsersJan, {$EndIf}
  UMySQLModule;

{$R *.DFM}

procedure TFmWOrdSerGruIts.BtOKClick(Sender: TObject);
var
  Codigo, Respons, GruStatus1, GruStatus2, GruStatus3, GruStatus4: Integer;
  Nome, Observ, IniData, FimData: String;
  SQLTipo: TSQLType;
begin
  Nome    := EdNome.ValueVariant;
  IniData := Geral.FDT(TPIniData.Date, 1);
  FimData := Geral.FDT(TPFimData.Date, 1);
  Respons := EdRespons.ValueVariant;
  Observ  := MeObserv.Text;
  //
  GruStatus1 := EdGruStatus1.ValueVariant;
  GruStatus2 := EdGruStatus2.ValueVariant;
  GruStatus3 := EdGruStatus3.ValueVariant;
  GruStatus4 := EdGruStatus4.ValueVariant;
  //
  if MyObjects.FIC(Nome = '', EdNome, 'Defina uma descrição!') then Exit;
  if MyObjects.FIC(Respons = 0, EdRespons, 'Defina o responsável!') then Exit;
  if MyObjects.FIC(GruStatus1 = 0, EdGruStatus1, 'Defina o status 1!') then Exit;
  if MyObjects.FIC(GruStatus2 = 0, EdGruStatus2, 'Defina o status 2!') then Exit;
  if MyObjects.FIC(GruStatus3 = 0, EdGruStatus3, 'Defina o status 3!') then Exit;
  if MyObjects.FIC(GruStatus4 = 0, EdGruStatus4, 'Defina o status 4!') then Exit;
  //
  if ImgTipo.SQLType = stIns then
  begin
    SQLTipo := stIns;
    Codigo  := UMyMod.BuscaNovoCodigo_Int(Dmod.QrUpdN, 'wordsergru', 'Codigo', [],
                 [], stIns, 0, siPositivo, nil);
  end else
  begin
    SQLTipo := stUpd;
    Codigo  := FCodigo;
  end;
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpdN, SQLTipo, 'wordsergru', False,
    ['Nome', 'IniData', 'FimData', 'Respons', 'Observ',
    'GruStatus1', 'GruStatus2', 'GruStatus3', 'GruStatus4'], ['Codigo'],
    [Nome, IniData, FimData, Respons, Observ,
    GruStatus1, GruStatus2, GruStatus3, GruStatus4], [Codigo], True) then
  begin
    FCodigo := Codigo;
    //
    Close;
  end;
end;

procedure TFmWOrdSerGruIts.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmWOrdSerGruIts.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmWOrdSerGruIts.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  UMyMod.AbreQuery(QrGruStatus1, DMod.MyDBn);
  UMyMod.AbreQuery(QrGruStatus2, DMod.MyDBn);
  UMyMod.AbreQuery(QrGruStatus3, DMod.MyDBn);
  UMyMod.AbreQuery(QrGruStatus4, DMod.MyDBn);
end;

procedure TFmWOrdSerGruIts.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmWOrdSerGruIts.FormShow(Sender: TObject);
begin
  MostraEdicao(ImgTipo.SQLType, FCodigo);
end;

procedure TFmWOrdSerGruIts.MostraEdicao(SQLType: TSQLType; Codigo: Integer);
begin
  DModWOrdSer.ReopenRespons;
  DModWOrdSer.ReopenWOrdSerOpc;
  //
  PageControl1.ActivePageIndex := 0;
  //
  if SQLType = stIns then
  begin
    EdCodigo.ValueVariant  := FormatFloat(FFormatFloat, Codigo);
    EdNome.ValueVariant    := '';
    TPIniData.Date         := 0;
    TPFimData.Date         := 0;
    EdRespons.ValueVariant := 0;
    CBRespons.KeyValue     := Null;
    MeObserv.Text          := '';
    //
    EdGruStatus1.ValueVariant := DModWOrdSer.QrWOrdSerOpc.FieldByName('GruStatus1').AsInteger;
    CBGruStatus1.KeyValue     := DModWOrdSer.QrWOrdSerOpc.FieldByName('GruStatus1').AsInteger;
    EdGruStatus2.ValueVariant := DModWOrdSer.QrWOrdSerOpc.FieldByName('GruStatus2').AsInteger;
    CBGruStatus2.KeyValue     := DModWOrdSer.QrWOrdSerOpc.FieldByName('GruStatus2').AsInteger;
    EdGruStatus3.ValueVariant := DModWOrdSer.QrWOrdSerOpc.FieldByName('GruStatus3').AsInteger;
    CBGruStatus3.KeyValue     := DModWOrdSer.QrWOrdSerOpc.FieldByName('GruStatus3').AsInteger;
    EdGruStatus4.ValueVariant := DModWOrdSer.QrWOrdSerOpc.FieldByName('GruStatus4').AsInteger;
    CBGruStatus4.KeyValue     := DModWOrdSer.QrWOrdSerOpc.FieldByName('GruStatus4').AsInteger;
  end else begin
    try
      Screen.Cursor := crHourGlass;
      BtOK.Enabled  := False;
      //
      EdCodigo.ValueVariant  := FQrWOrdSerGru.FieldByName('Codigo').AsInteger;
      EdNome.ValueVariant    := FQrWOrdSerGru.FieldByName('Nome').AsString;
      TPIniData.Date         := FQrWOrdSerGru.FieldByName('IniData').AsDateTime;
      TPFimData.Date         := FQrWOrdSerGru.FieldByName('FimData').AsDateTime;
      EdRespons.ValueVariant := FQrWOrdSerGru.FieldByName('Respons').AsInteger;
      CBRespons.KeyValue     := FQrWOrdSerGru.FieldByName('Respons').AsInteger;
      MeObserv.Text          := FQrWOrdSerGru.FieldByName('Observ').AsString;
      //
      EdGruStatus1.ValueVariant := FQrWOrdSerGru.FieldByName('GruStatus1').AsInteger;
      CBGruStatus1.KeyValue     := FQrWOrdSerGru.FieldByName('GruStatus1').AsInteger;
      EdGruStatus2.ValueVariant := FQrWOrdSerGru.FieldByName('GruStatus2').AsInteger;
      CBGruStatus2.KeyValue     := FQrWOrdSerGru.FieldByName('GruStatus2').AsInteger;
      EdGruStatus3.ValueVariant := FQrWOrdSerGru.FieldByName('GruStatus3').AsInteger;
      CBGruStatus3.KeyValue     := FQrWOrdSerGru.FieldByName('GruStatus3').AsInteger;
      EdGruStatus4.ValueVariant := FQrWOrdSerGru.FieldByName('GruStatus4').AsInteger;
      CBGruStatus4.KeyValue     := FQrWOrdSerGru.FieldByName('GruStatus4').AsInteger;
    finally
      Screen.Cursor := crDefault;
      BtOK.Enabled  := True;
    end;
  end;
  EdNome.SetFocus;
end;

procedure TFmWOrdSerGruIts.SBResponsClick(Sender: TObject);
begin
  {$IfnDef SWUsr}
  VAR_CADASTRO := 0;
  //
  UWUsersJan.MostraWUsers(EdRespons.ValueVariant);
  //
  if VAR_CADASTRO > 0 then
  begin
    DModWOrdSer.ReopenWUsers(EdRespons.ValueVariant);
    //
    EdRespons.ValueVariant := VAR_CADASTRO;
    CBRespons.KeyValue     := VAR_CADASTRO;
    EdRespons.SetFocus;
  end;
  {$EndIf}
end;

end.
