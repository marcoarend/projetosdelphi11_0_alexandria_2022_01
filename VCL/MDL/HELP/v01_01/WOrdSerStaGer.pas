unit WOrdSerStaGer;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkDBLookupComboBox, dmkEdit,
  dmkEditCB, Variants, UnDmkWeb, DmkDAC_PF, mySQLDbTables;

type
  TFmWOrdSerStaGer = class(TForm)
    PnCabeca: TPanel;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    RGAcao: TRadioGroup;
    LaStatus: TLabel;
    EdStatus: TdmkEditCB;
    CBStatus: TdmkDBLookupComboBox;
    QrWOrdSerSta: TmySQLQuery;
    IntegerField5: TIntegerField;
    QrWOrdSerStaNome: TWideStringField;
    QrWOrdSerStaWOrdSerEnc: TIntegerField;
    DsWOrdSerSta: TDataSource;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    ImgWEB: TdmkImage;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure RGAcaoClick(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenWOrdSerSta(Encerrou: Boolean);
  public
    { Public declarations }
    FCodigo: Integer;
    FGrade: TDBGrid;
    FQuery: TmySQLQuery;
    FReabre: Boolean;
  end;

  var
  FmWOrdSerStaGer: TFmWOrdSerStaGer;

implementation

uses UnMyObjects, Module, ModuleGeral, UnGrl_Vars;

{$R *.DFM}

procedure TFmWOrdSerStaGer.BtOKClick(Sender: TObject);

  function AtualizaItensGrade(Encerra: Boolean; Status, WOrdSerEnc: Integer;
    DataHoraTZ: String; var MensagemAviso: String): Integer;
  var
    Resul, Codigo, Finaliz, i: Integer;
  begin
    if FGrade.SelectedRows.Count > 1 then
    begin
      Screen.Cursor := crHourGlass;
      Resul         := 1;
      try
        FGrade.Enabled := False;
        FQuery.DisableControls;
        //
        with FGrade.DataSource.DataSet do
        begin
          for i:= 0 to FGrade.SelectedRows.Count - 1 do
          begin
            GotoBookmark(FGrade.SelectedRows.Items[i]);
            //
            if Resul > 0 then
            begin
              Codigo  := FQuery.FieldByName('Codigo').AsInteger;
              Finaliz := FQuery.FieldByName('Finaliz').AsInteger;
              //
              if Finaliz = 0 then
              begin
                if Encerra then
                  Resul := DmkWeb.WOrdSerEnc(31, Codigo, Status, DataHoraTZ, WOrdSerEnc, '', MensagemAviso)
                else
                  Resul := DmkWeb.WOrdSerAtualizaStatus(31, Codigo, Status, MensagemAviso);
              end;
            end;
          end;
        end;
      finally
        FQuery.EnableControls;
        FGrade.Enabled := True;
        Screen.Cursor  := crDefault;
      end;
    end else
    begin
      Codigo := FQuery.FieldByName('Codigo').AsInteger;
      //
      if Encerra then
        Resul := DmkWeb.WOrdSerEnc(31, Codigo, Status, DataHoraTZ, WOrdSerEnc, '', MensagemAviso)
      else
        Resul := DmkWeb.WOrdSerAtualizaStatus(31, Codigo, Status, MensagemAviso);
    end;
    Result := Resul;
  end;
var
  Codigo, Status, UserTipo, UserCodigo, WOrdSerEnc, Resul: Integer;
  DataHoraTZ, MensagemAviso: String;
  Encerr: Boolean;
begin
  Codigo     := FCodigo;
  Status     := EdStatus.ValueVariant;
  Encerr     := RGAcao.ItemIndex = 0;
  UserCodigo := VAR_WEB_USR_ID;
  UserTipo   := VAR_WEB_USR_TIPO;
  //
  if MyObjects.FIC(Status = 0, EdStatus, 'Status n�o definido!') then Exit;
  //
  if Encerr then
  begin
    DataHoraTZ := Geral.FDT(DModG.ObtemAgora, 105);
    WOrdSerEnc := QrWOrdSerStaWOrdSerEnc.Value;
    //
    if Codigo = 0 then
      Resul := AtualizaItensGrade(Encerr, Status, WOrdSerEnc, DataHoraTZ, MensagemAviso)
    else
      Resul := DmkWeb.WOrdSerEnc(31, Codigo, Status, DataHoraTZ, WOrdSerEnc, '', MensagemAviso);
  end else
  begin
    if Codigo = 0 then
      Resul := AtualizaItensGrade(Encerr, Status, 0, '', MensagemAviso)
    else
      Resul := DmkWeb.WOrdSerAtualizaStatus(31, Codigo, Status, MensagemAviso);
  end;
  if Resul > 0 then
  begin
    if Resul <> 104 then
      Geral.MB_Aviso(MensagemAviso);
    FReabre := True;
    Close;
  end else
    Geral.MB_Aviso('Falha ao alterar o status da solicita��o tente novamente mais tarde!');
end;

procedure TFmWOrdSerStaGer.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmWOrdSerStaGer.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  DmkWeb.CarregaImagemFormWEB(ImgWEB);
end;

procedure TFmWOrdSerStaGer.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  FReabre          := False;
  RGAcao.ItemIndex := -1;
  LaStatus.Visible := False;
  EdStatus.Visible := False;
  CBStatus.Visible := False;
end;

procedure TFmWOrdSerStaGer.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
    [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmWOrdSerStaGer.ReopenWOrdSerSta(Encerrou: Boolean);
var
  SQL, SQLNivel: String;
begin
  SQLNivel := 'AND (' + DmkWeb.VerificaSQLNiveis(VAR_WEB_USR_TIPO, 'Nivel') + ' OR Nivel & "1")';
  //
  if Encerrou then
    SQL := 'AND WOrdSerEnc = 0'
  else
    SQL := 'AND WOrdSerEnc > 0';
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrWOrdSerSta, Dmod.MyDBn, [
    'SELECT Codigo, Nome, WOrdSerEnc ',
    'FROM wordsersta ',
    'WHERE Ativo = 1 ',
    SQL,
    SQLNivel,
    'ORDER BY Nome ',
    '']);
end;

procedure TFmWOrdSerStaGer.RGAcaoClick(Sender: TObject);
var
  Visi, Encerr: Boolean;
begin
  Visi   := RGAcao.ItemIndex > -1;
  Encerr := RGAcao.ItemIndex = 1;
  //
  if Visi then
  begin
    ReopenWOrdSerSta(Encerr);
    //
    EdStatus.ValueVariant := 0;
    CBStatus.KeyValue     := Null;
    //
    LaStatus.Visible := True;
    EdStatus.Visible := True;
    CBStatus.Visible := True;
    //
    EdStatus.SetFocus;
  end;
end;

end.
