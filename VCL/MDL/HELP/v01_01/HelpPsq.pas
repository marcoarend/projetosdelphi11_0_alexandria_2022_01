unit HelpPsq;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkPermissoes,
  dmkCompoStore, dmkCheckBox, dmkRadioGroup, dmkDBLookupComboBox, dmkEditCB,
  mySQLDbTables, dmkDBGrid, dmkPageControl, Vcl.OleCtrls, SHDocVw, Vcl.Menus;

type
  TFmHelpPsq = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    LaTitulo1C: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    CSTabSheetChamou: TdmkCompoStore;
    QrHelpFaq: TmySQLQuery;
    DsHelpFaq: TDataSource;
    QrHelpFaqCodigo: TIntegerField;
    QrHelpFaqNome: TWideStringField;
    QrHelpFaqAtivo: TSmallintField;
    Panel6: TPanel;
    Panel5: TPanel;
    Label9: TLabel;
    Label7: TLabel;
    Label3: TLabel;
    EdNome: TdmkEdit;
    EdCodigo: TdmkEdit;
    EdTags: TdmkEdit;
    BtLocaliza: TBitBtn;
    BtPesquisa: TBitBtn;
    DBGHelpFAQ: TdmkDBGrid;
    QrHelpFaqTexto: TWideMemoField;
    Splitter1: TSplitter;
    PMMenu: TPopupMenu;
    Abrirtutorial1: TMenuItem;
    BitBtn1: TBitBtn;
    WebBrowser1: TWebBrowser;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure BtPesquisaClick(Sender: TObject);
    procedure QrHelpFaqBeforeClose(DataSet: TDataSet);
    procedure QrHelpFaqAfterOpen(DataSet: TDataSet);
    procedure BtLocalizaClick(Sender: TObject);
    procedure QrHelpFaqAfterScroll(DataSet: TDataSet);
    procedure Abrirtutorial1Click(Sender: TObject);
    procedure PMMenuPopup(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
  private
    { Private declarations }
    procedure MostraFormHelpFAQ(Codigo: Integer);
  public
    { Public declarations }
    FLocaliza: Boolean;
  end;

  var
  FmHelpPsq: TFmHelpPsq;

implementation

uses MyGlyfs, Principal, UnMyObjects, UnGrlHelp, Module, UnDmkProcFunc,
  UnitHelpJan;

{$R *.DFM}

procedure TFmHelpPsq.Abrirtutorial1Click(Sender: TObject);
begin
  MostraFormHelpFAQ(QrHelpFaqCodigo.Value);
end;

procedure TFmHelpPsq.BitBtn1Click(Sender: TObject);
begin
  MostraFormHelpFAQ(QrHelpFaqCodigo.Value);
end;

procedure TFmHelpPsq.BtLocalizaClick(Sender: TObject);
var
  Codigo: Integer;
begin
  Codigo := QrHelpFaqCodigo.Value;
  //
  if Codigo <> 0 then
  begin
    if TFmHelpPsq(Self).Owner is TApplication then
    begin
      UnHelpJan.MostraFormHelpFAQ(FmHelpPsq, False, nil, nil, Codigo);
      Close
    end else
    begin
      UnHelpJan.MostraFormHelpFAQ(FmHelpPsq, True, FmPrincipal.PageControl1,
        FmPrincipal.AdvToolBarPagerNovo, Codigo);
      MyObjects.FormTDIFecha(Self, TTabSheet(CSTabSheetChamou.Component));
    end;
  end;
end;

procedure TFmHelpPsq.BtPesquisaClick(Sender: TObject);
var
  Codigo: Integer;
  Nome, Tags: String;
begin
  Codigo := EdCodigo.ValueVariant;
  Nome   := EdNome.ValueVariant;
  Tags   := EdTags.ValueVariant;
  //
  GrlHelp.ReopenHelpFAQ(Dmod.MyDB, QrHelpFaq, Codigo, Nome, Tags);
end;

procedure TFmHelpPsq.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrHelpFaqCodigo.Value;
  //
  if TFmHelpPsq(Self).Owner is TApplication then
    Close
  else
    MyObjects.FormTDIFecha(Self, TTabSheet(CSTabSheetChamou.Component));
end;

procedure TFmHelpPsq.FormActivate(Sender: TObject);
begin
  if TFmHelpPsq(Self).Owner is TApplication then
  begin
    MyObjects.CorIniComponente();
  end;
end;

procedure TFmHelpPsq.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType       := stLok;
  DBGHelpFAQ.DataSource := DsHelpFaq;
  DBGHelpFAQ.PopupMenu  := PMMenu;
  FLocaliza             := True;
  BtLocaliza.Visible    := False;
end;

procedure TFmHelpPsq.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
    [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmHelpPsq.FormShow(Sender: TObject);
begin
{$IfNDef cSkinRank} //Berlin
{$IfNDef cAlphaSkin} //Berlin
  FmMyGlyfs.DefineGlyfs(TForm(Sender));
{$EndIf}
{$EndIf}
{$IfDef cSkinRank} //Berlin
  if FmPrincipal.Sd1.Active then
    FmMyGlyfs.DefineGlyfsTDI(FmPrincipal.sd1, Sender);
{$EndIf}
{$IfDef cAlphaSkin} //Berlin
  if FmPrincipal.sSkinManager1.Active then
    FmMyGlyfs.DefineGlyfsTDI2(FmPrincipal.sSkinManager1, Sender);
{$EndIf}
end;

procedure TFmHelpPsq.MostraFormHelpFAQ(Codigo: Integer);
begin
  if TFmHelpPsq(Self).Owner is TApplication then
    UnHelpJan.MostraFormHelpFAQ(FmPrincipal, False, nil, nil, Codigo)
  else
    UnHelpJan.MostraFormHelpFAQ(FmPrincipal, True, FmPrincipal.PageControl1,
      FmPrincipal.AdvToolBarPagerNovo, Codigo);
end;

procedure TFmHelpPsq.PMMenuPopup(Sender: TObject);
var
  Enab: Boolean;
begin
  Enab := (QrHelpFaq.State <> dsInactive) and (QrHelpFaq.RecordCount > 0);
  //
  Abrirtutorial1.Enabled := Enab;
end;

procedure TFmHelpPsq.QrHelpFaqAfterOpen(DataSet: TDataSet);
begin
  BtLocaliza.Visible := (QrHelpFaq.RecordCount > 0) and (FLocaliza = True);
end;

procedure TFmHelpPsq.QrHelpFaqAfterScroll(DataSet: TDataSet);
begin
  UnHelpJan.MostraWebBrowser(WebBrowser1, QrHelpFaqTexto.Value);
end;

procedure TFmHelpPsq.QrHelpFaqBeforeClose(DataSet: TDataSet);
begin
  UnHelpJan.FechaWebBrowser(WebBrowser1);
  //
  BtLocaliza.Visible := False;
end;

end.
