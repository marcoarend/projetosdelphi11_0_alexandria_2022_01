unit WOrdSerAte;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2,
  ResIntStrings, UnMLAGeral, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, dmkGeral,
  dmkPermissoes, dmkEdit, dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup,
  unDmkProcFunc, DmkDAC_PF, Grids, DBGrids, dmkDBGrid, dmkCheckBox, dmkEditCB,
  dmkDBLookupComboBox, Menus, Variants, UnDmkEnums, Vcl.ComCtrls,
  UnProjGroup_Consts;

type
  TFmWOrdSerAte = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    BtParametros: TBitBtn;
    BtAtendente: TBitBtn;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    QrWOrdSerAte: TmySQLQuery;
    DsWOrdSerAte: TDataSource;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    Label1: TLabel;
    DBEdCodigo: TdmkDBEdit;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    QrWOrdSerAteCodigo: TIntegerField;
    QrWOrdSerAteAtendente: TIntegerField;
    QrWOrdSerAteLk: TIntegerField;
    QrWOrdSerAteDataCad: TDateField;
    QrWOrdSerAteDataAlt: TDateField;
    QrWOrdSerAteUserCad: TIntegerField;
    QrWOrdSerAteUserAlt: TIntegerField;
    QrWOrdSerAteAlterWeb: TSmallintField;
    QrWOrdSerAteAtivo: TSmallintField;
    QrWOrdSerAteNOMEATEN: TWideStringField;
    Label2: TLabel;
    DBEdNome: TdmkDBEdit;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    DBCheckBox2: TDBCheckBox;
    QrWOrSeAteIt: TmySQLQuery;
    DsWOrSeAteIt: TDataSource;
    DsAtendente: TDataSource;
    QrLoc: TmySQLQuery;
    QrAtendente: TmySQLQuery;
    QrAtendenteCodigo: TAutoIncField;
    QrAtendentePersonalName: TWideStringField;
    CBAtendente: TdmkDBLookupComboBox;
    Label9: TLabel;
    EdAtendente: TdmkEditCB;
    CkAtivo: TdmkCheckBox;
    CkContinuar: TCheckBox;
    PMAtendente: TPopupMenu;
    Inclui1: TMenuItem;
    Altera1: TMenuItem;
    Exclui1: TMenuItem;
    PMParametros: TPopupMenu;
    Inclui2: TMenuItem;
    Exclui2: TMenuItem;
    ImgWEB: TdmkImage;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    dmkDBGrid3: TdmkDBGrid;
    BtGrupos: TBitBtn;
    PMGrupos: TPopupMenu;
    Inclui3: TMenuItem;
    Exclui3: TMenuItem;
    dmkDBGrid2: TdmkDBGrid;
    QrWOrSeAteItCodigo: TIntegerField;
    QrWOrSeAteItControle: TIntegerField;
    QrWOrSeAteItAssunto_TXT: TWideStringField;
    QrWOrSeAteItCliente_TXT: TWideStringField;
    QrWOrSeAteItSolicit_TXT: TWideStringField;
    QrWOrSeAteItAplicativo_TXT: TWideStringField;
    QrWOrSeAteGr: TmySQLQuery;
    DsWOrSeAteGr: TDataSource;
    QrWOrSeAteGrCodigo: TIntegerField;
    QrWOrSeAteGrControle: TIntegerField;
    QrWOrSeAteGrGrupo_TXT: TWideStringField;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrWOrdSerAteAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrWOrdSerAteBeforeOpen(DataSet: TDataSet);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure QrWOrdSerAteBeforeClose(DataSet: TDataSet);
    procedure Inclui1Click(Sender: TObject);
    procedure Altera1Click(Sender: TObject);
    procedure PMAtendentePopup(Sender: TObject);
    procedure BtAtendenteClick(Sender: TObject);
    procedure Inclui2Click(Sender: TObject);
    procedure Exclui2Click(Sender: TObject);
    procedure BtParametrosClick(Sender: TObject);
    procedure BtGruposClick(Sender: TObject);
    procedure PMGruposPopup(Sender: TObject);
    procedure Inclui3Click(Sender: TObject);
    procedure Exclui3Click(Sender: TObject);
    procedure QrWOrdSerAteAfterScroll(DataSet: TDataSet);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure LocCod(Atual, Codigo: Integer);
    procedure Va(Para: TVaiPara);
    function VerificaSeAtendenteExiste(Atendente: Integer): Boolean;
  public
    { Public declarations }
    procedure ReopenWOrSeAteIt(Codigo: Integer);
    procedure ReopenWOrSeAteGr(Codigo: Integer);
  end;

var
  FmWOrdSerAte: TFmWOrdSerAte;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, ModWOrdSer, MyDBCheck, UnDmkWeb, MyListas;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmWOrdSerAte.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmWOrdSerAte.PMAtendentePopup(Sender: TObject);
var
  Enab: Boolean;
begin
  Enab := (QrWOrdSerAte.State <> dsInactive) and (QrWOrdSerAte.RecordCount > 0)
            and (QrWOrdSerAteCodigo.Value <> 0);
  //
  Altera1.Enabled := Enab;
  Exclui1.Enabled := Enab;
end;

procedure TFmWOrdSerAte.PMGruposPopup(Sender: TObject);
var
  Enab1, Enab2: Boolean;
begin
  Enab1 := (QrWOrdSerAte.State <> dsInactive) and (QrWOrdSerAte.RecordCount > 0);
  Enab2 := (QrWOrSeAteGr.State <> dsInactive) and (QrWOrSeAteGr.RecordCount > 0);
  //
  Inclui3.Enabled := Enab1;
  Exclui3.Enabled := Enab1 and Enab2;
end;

procedure TFmWOrdSerAte.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrWOrdSerAteCodigo.Value, LaRegistro.Caption[2]);
end;

function TFmWOrdSerAte.VerificaSeAtendenteExiste(Atendente: Integer): Boolean;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrLoc, Dmod.MyDBn, [
  'SELECT * ',
  'FROM worseate ',
  'WHERE Atendente=' + Geral.FF0(Atendente),
  '']);
  if QrLoc.RecordCount > 0 then
    Result := True
  else
    Result := False;
end;

/////////////////////////////////////////////////////////////////////////////////////

procedure TFmWOrdSerAte.DefParams;
begin
  VAR_GOTOTABELA := 'worseate';
  VAR_GOTOMYSQLTABLE := QrWOrdSerAte;
  VAR_GOTONEG := gotoPos;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDBn;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT ate.*, wus.PersonalName NOMEATEN');
  VAR_SQLx.Add('FROM worseate ate');
  VAR_SQLx.Add('LEFT JOIN wusers wus ON wus.Codigo = ate.Atendente');
  VAR_SQLx.Add('WHERE ate.Codigo > 0');
  //
  VAR_SQL1.Add('AND ate.Codigo=:P0');
  //
  VAR_SQLa.Add('AND ate.Nome Like :P0');
  //
end;

procedure TFmWOrdSerAte.Exclui2Click(Sender: TObject);
begin
  DBCheck.QuaisItens_Exclui(Dmod.QrUpdN, QrWOrSeAteIt, TDBGrid(dmkDBGrid2),
    'worseateit', ['Controle'], ['Controle'], istPergunta, '');
  //
  ReopenWOrSeAteIt(QrWOrdSerAteCodigo.Value);
end;

procedure TFmWOrdSerAte.Exclui3Click(Sender: TObject);
begin
  DBCheck.QuaisItens_Exclui(Dmod.QrUpdN, QrWOrSeAteIt, TDBGrid(dmkDBGrid3),
    'worseateca', ['Controle'], ['Controle'], istPergunta, '');
  //
  ReopenWOrSeAteGr(QrWOrdSerAteCodigo.Value);
end;

procedure TFmWOrdSerAte.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmWOrdSerAte.QueryPrincipalAfterOpen;
begin
end;

procedure TFmWOrdSerAte.ReopenWOrSeAteIt(Codigo: Integer);
var
  SQLField, SQLLeft: String;
begin
  if CO_DMKID_APP = 17 then //DControl
  begin
    SQLField := 'apl.Nome Aplicativo_TXT ';
    SQLLeft  := 'LEFT JOIN aplicativos apl ON apl.Codigo = wit.Aplicativo ';
  end else
  begin
    SQLField := '"" Aplicativo_TXT ';
    SQLLeft  := '';
  end;
  UnDmkDAC_PF.AbreMySQLQuery0(QrWOrSeAteIt, Dmod.MyDBn, [
    'SELECT wit.*, ass.Nome Assunto_TXT, ',
    'IF(ent.Tipo = 0, ent.RazaoSocial, ent.Nome) Cliente_TXT, ',
    'wus.PersonalName Solicit_TXT, ' + SQLField,
    'FROM worseateit wit ',
    'LEFT JOIN wordserass ass ON ass.Codigo = wit.Assunto ',
    'LEFT JOIN entidades ent ON ent.Codigo = wit.Cliente ',
    'LEFT JOIN wusers wus ON wus.Codigo = wit.Solicit ',
    'LEFT JOIN aplicativos apl ON apl.Codigo = wit.Aplicativo ',
    'WHERE wit.Codigo=' + Geral.FF0(Codigo),
    '']);
end;

procedure TFmWOrdSerAte.ReopenWOrSeAteGr(Codigo: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrWOrSeAteGr, Dmod.MyDBn, [
    'SELECT wca.*, wgu.Nome Grupo_TXT ',
    'FROM worseateca wca ',
    'LEFT JOIN worseategr wgu ON wgu.Codigo = wca.Grupo ',
    'WHERE wca.Codigo=' + Geral.FF0(Codigo),
    'ORDER BY Grupo_TXT ',
    '']);
end;

procedure TFmWOrdSerAte.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmWOrdSerAte.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmWOrdSerAte.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmWOrdSerAte.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmWOrdSerAte.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmWOrdSerAte.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmWOrdSerAte.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrWOrdSerAteCodigo.Value;
  Close;
end;

procedure TFmWOrdSerAte.Altera1Click(Sender: TObject);
begin
  CkContinuar.Visible := False;
  CkContinuar.Checked := False;
  //
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrWOrdSerAte, [PnDados],
  [PnEdita], EdAtendente, ImgTipo, 'worseate');
end;

procedure TFmWOrdSerAte.BtAtendenteClick(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 0;
  //
  MyObjects.MostraPopUpDeBotao(PMAtendente, BtAtendente);
end;

procedure TFmWOrdSerAte.BtConfirmaClick(Sender: TObject);
var
  Codigo, Atendente: Integer;
  PnShow, PnHide: TPanel;
begin
  Atendente := EdAtendente.ValueVariant;
  //
  if MyObjects.FIC(Atendente = 0, EdAtendente, 'Defina um atendente!') then Exit;
  //
  if ImgTipo.SQLType = stIns then
    if MyObjects.FIC(VerificaSeAtendenteExiste(Atendente), EdAtendente,
      'Este atendente j� foi cadastrado!') then Exit;
  //
  if ImgTipo.SQLType = stIns then
    Codigo := UMyMod.BuscaNovoCodigo_Int(Dmod.QrUpdN, 'worseate', 'Codigo', [], [], stIns, 0, siPositivo, nil)
  else
    Codigo := QrWOrdSerAteCodigo.Value;
  //
  if CkContinuar.Checked then
  begin
    PnShow := PnEdita;
    PnHide := PnDados;
  end else
  begin
    PnShow := PnDados;
    PnHide := PnEdita;
  end;
  //
  if UMyMod.ExecSQLInsUpdPanel(ImgTipo.SQLType, Self, PnEdita,
    'worseate', Codigo, Dmod.QrUpdN, [PnHide], [PnShow], ImgTipo, True) then
  begin
    MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
    LocCod(Codigo, Codigo);
    //
    if CkContinuar.Checked then
    begin
      Geral.MensagemBox('Dados salvos com sucesso!', 'Aviso', MB_OK+MB_ICONWARNING);
      //
      EdAtendente.ValueVariant := 0;
      CBAtendente.KeyValue     := Null;
      EdAtendente.SetFocus;
    end;
  end;
end;

procedure TFmWOrdSerAte.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo          := EdCodigo.ValueVariant;
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
  //
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDBn, 'worseate', 'Codigo');
end;

procedure TFmWOrdSerAte.BtGruposClick(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 1;
  //
  MyObjects.MostraPopUpDeBotao(PMGrupos, BtGrupos);
end;

procedure TFmWOrdSerAte.BtParametrosClick(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 0;
  //
  MyObjects.MostraPopUpDeBotao(PMParametros, BtParametros);
end;

procedure TFmWOrdSerAte.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType              := stLok;
  GBEdita.Align                := alClient;
  PageControl1.ActivePageIndex := 0;
  PageControl1.Align           := alClient;
  CriaOForm;
  //
  UMyMod.AbreQuery(QrAtendente, DMod.MyDBn);
end;

procedure TFmWOrdSerAte.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrWOrdSerAteCodigo.Value, LaRegistro.Caption);
end;

procedure TFmWOrdSerAte.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmWOrdSerAte.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrWOrdSerAteCodigo.Value, LaRegistro.Caption);
end;

procedure TFmWOrdSerAte.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmWOrdSerAte.QrWOrdSerAteAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmWOrdSerAte.QrWOrdSerAteAfterScroll(DataSet: TDataSet);
begin
  ReopenWOrSeAteIt(QrWOrdSerAteCodigo.Value);
  ReopenWOrSeAteGr(QrWOrdSerAteCodigo.Value);
end;

procedure TFmWOrdSerAte.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  DmkWeb.CarregaImagemFormWEB(ImgWEB);
end;

procedure TFmWOrdSerAte.SbQueryClick(Sender: TObject);
begin
  LocCod(QrWOrdSerAteCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'worseate', Dmod.MyDBn, CO_VAZIO));
end;

procedure TFmWOrdSerAte.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmWOrdSerAte.Inclui1Click(Sender: TObject);
begin
  CkContinuar.Visible := True;
  CkContinuar.Checked := False;
  //
  UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrWOrdSerAte, [PnDados],
    [PnEdita], EdAtendente, ImgTipo, 'worseate');
end;

procedure TFmWOrdSerAte.Inclui2Click(Sender: TObject);
begin
  DModWOrdSer.MostraWOrdSerAtePar(QrWOrdSerAteCodigo.Value);
end;

procedure TFmWOrdSerAte.Inclui3Click(Sender: TObject);
const
  Aviso  = '...';
  Titulo = 'Sele��o de grupo';
  Prompt = 'Informe o grupo: [F7 para pesquisar]';
  Campo  = 'Descricao';
var
  Codigo, Controle: Integer;
  Grupo: Variant;
begin
  Codigo := QrWOrdSerAteCodigo.Value;
  Grupo  := DBCheck.EscolheCodigoUnico(Aviso, Titulo, Prompt, nil, nil, Campo, 0, [
    'SELECT Codigo, Nome ' + Campo,
    'FROM worseategr ',
    'WHERE Codigo NOT IN ',
    '( ',
    'SELECT Grupo ',
    'FROM worseateca ',
    'WHERE Codigo=' + Geral.FF0(Codigo),
    ') ',
    'AND Ativo = 1 ',
    'ORDER BY ' + Campo,
    ''], Dmod.MyDBn, True);
  //
  if Grupo <> Null then
  begin
    Controle := UMyMod.BuscaNovoCodigo_Int(Dmod.QrUpdN, 'worseateca', 'Controle',
                  [], [], stIns, 0, siPositivo, nil);
    //
    UMyMod.SQLInsUpd(Dmod.QrUpdN, stIns, 'worseateca', False, ['Grupo',
      'Codigo'], ['Controle'], [Grupo, Codigo], [Controle], True);
  end;
end;

procedure TFmWOrdSerAte.QrWOrdSerAteBeforeClose(DataSet: TDataSet);
begin
  QrWOrSeAteIt.Close;
  QrWOrSeAteGr.Close;
end;

procedure TFmWOrdSerAte.QrWOrdSerAteBeforeOpen(DataSet: TDataSet);
begin
  QrWOrdSerAteCodigo.DisplayFormat := FFormatFloat;
end;

end.

