object FmWOrdSerImp: TFmWOrdSerImp
  Left = 339
  Top = 185
  ActiveControl = CkAbeIni
  Caption = 'WEB-ORSER-010 :: Estat'#237'sticas da Ordem de Servi'#231'o'
  ClientHeight = 326
  ClientWidth = 459
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelConfirma: TPanel
    Left = 0
    Top = 278
    Width = 459
    Height = 48
    Align = alBottom
    TabOrder = 1
    object BtOK: TBitBtn
      Tag = 14
      Left = 20
      Top = 4
      Width = 90
      Height = 40
      Caption = '&OK'
      NumGlyphs = 2
      TabOrder = 0
      OnClick = BtOKClick
    end
    object Panel2: TPanel
      Left = 347
      Top = 1
      Width = 111
      Height = 46
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 2
        Top = 3
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 459
    Height = 48
    Align = alTop
    Caption = 'Estat'#237'sticas da Ordem de Servi'#231'o'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 2
    object Image1: TImage
      Left = 1
      Top = 1
      Width = 457
      Height = 46
      Align = alClient
      Transparent = True
      ExplicitLeft = -18
      ExplicitTop = 2
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 459
    Height = 230
    Align = alClient
    TabOrder = 0
    object RGTipo: TdmkRadioGroup
      Left = 20
      Top = 6
      Width = 419
      Height = 120
      Caption = 'Tipos de relat'#243'rios'
      Columns = 2
      Items.Strings = (
        'Quantidade de insidentes'
        'Clientes mais atuantes'
        'Assuntos mais utilizados'
        'Modo de solicita'#231#227'o'
        'Quantidade de insidentes finalizados')
      TabOrder = 0
      QryCampo = 'Tipo'
      UpdCampo = 'Tipo'
      UpdType = utYes
      OldValor = 0
    end
    object GBEmissao: TGroupBox
      Left = 20
      Top = 132
      Width = 419
      Height = 65
      Caption = 'Per'#237'odo'
      TabOrder = 1
      object CkAbeIni: TCheckBox
        Left = 12
        Top = 16
        Width = 75
        Height = 17
        Caption = 'Data inicial:'
        TabOrder = 0
      end
      object CkAbeFim: TCheckBox
        Left = 213
        Top = 16
        Width = 68
        Height = 17
        Caption = 'Data final:'
        TabOrder = 2
      end
      object TPAbeIni: TdmkEditDateTimePicker
        Left = 12
        Top = 35
        Width = 186
        Height = 21
        Date = 40698.652010092590000000
        Time = 40698.652010092590000000
        TabOrder = 1
        ReadOnly = False
        DefaultEditMask = '!99/99/99;1;_'
        AutoApplyEditMask = True
        UpdType = utYes
      end
      object TPAbeFim: TdmkEditDateTimePicker
        Left = 213
        Top = 35
        Width = 186
        Height = 21
        Date = 40698.652010092590000000
        Time = 40698.652010092590000000
        TabOrder = 3
        ReadOnly = False
        DefaultEditMask = '!99/99/99;1;_'
        AutoApplyEditMask = True
        UpdType = utYes
      end
    end
  end
  object frxWEB_ORSER_010_001: TfrxReport
    Version = '4.14'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 40528.898746481500000000
    ReportOptions.LastChange = 40698.810517245370000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      
        '  MeVARF_CODI_FRX.Text := Copy(Report.Name, 4, Length(Report.Nam' +
        'e));'
      
        '  if <MeuLogoExiste> = True then Picture1.LoadFromFile(<MeuLogoC' +
        'aminho>);    '
      'end.')
    OnNewGetValue = frxWEB_ORSER_010_001NewGetValue
    Left = 16
    Top = 8
    Datasets = <
      item
        DataSet = frxDs1Gra
        DataSetName = 'frxDs1Gra'
      end
      item
        DataSet = frxDs1Its
        DataSetName = 'frxDs1Its'
      end
      item
        DataSet = frxDs2Tot
        DataSetName = 'frxDs2Tot'
      end
      item
        DataSet = DModG.frxDsEndereco
        DataSetName = 'frxDsEndereco'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 15.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object PageHeader1: TfrxPageHeader
        Height = 381.732334720000000000
        Top = 18.897650000000000000
        Width = 699.213050000000000000
        object Chart1: TfrxChartView
          Top = 79.370130000000000000
          Width = 699.212586220000000000
          Height = 302.362204720000000000
          ShowHint = False
          Chart = {
            5450463006544368617274054368617274044C656674020003546F7002000557
            696474680390010648656967687403FA00144261636B57616C6C2E50656E2E56
            697369626C6508104C6567656E642E416C69676E6D656E7407086C61426F7474
            6F6D124C6567656E642E466F6E742E48656967687402F80D4672616D652E5669
            7369626C6508165669657733444F7074696F6E732E526F746174696F6E02000A
            426576656C4F75746572070662764E6F6E6505436F6C6F720707636C57686974
            6511436F6C6F7250616C65747465496E646578020D000B544C696E6553657269
            65730753657269657331134D61726B732E4172726F772E56697369626C650819
            4D61726B732E43616C6C6F75742E42727573682E436F6C6F720707636C426C61
            636B154D61726B732E43616C6C6F75742E56697369626C65091B4D61726B732E
            43616C6C6F75742E4172726F772E56697369626C65080D4D61726B732E566973
            69626C650816506F696E7465722E496E666C6174654D617267696E73090D506F
            696E7465722E5374796C65070B707352656374616E676C650F506F696E746572
            2E56697369626C65080C5856616C7565732E4E616D650601580D5856616C7565
            732E4F72646572070B6C6F417363656E64696E670C5956616C7565732E4E616D
            650601590D5956616C7565732E4F7264657207066C6F4E6F6E65000000}
          ChartElevation = 345
          SeriesData = <
            item
              DataType = dtDBData
              DataSet = frxDs1Gra
              DataSetName = 'frxDs1Gra'
              SortOrder = soNone
              TopN = 0
              XType = xtText
              Source1 = 'frxDs1Gra."Abertura"'
              Source2 = 'frxDs1Gra."PORCENT"'
              Source3 = 'frxDs1Gra."Abertura"'
              XSource = 'frxDs1Gra."Abertura"'
              YSource = 'frxDs1Gra."PORCENT"'
            end>
        end
        object Memo6: TfrxMemoView
          Top = 56.692950000000000000
          Width = 699.213050000000000000
          Height = 22.677165350000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            'Quantidade de Insidentes no Per'#237'odo [PERIODO]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Shape1: TfrxShapeView
          Width = 699.213050000000000000
          Height = 56.692950000000000000
          ShowHint = False
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo13: TfrxMemoView
          Left = 154.960730000000000000
          Width = 393.071120000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsEndereco."NOME_ENT"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line1: TfrxLineView
          Left = 551.811380000000000000
          Top = 18.897650000000000000
          Width = 147.401670000000000000
          ShowHint = False
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo14: TfrxMemoView
          Left = 154.960730000000000000
          Top = 18.897650000000000000
          Width = 393.071120000000000000
          Height = 37.795275590000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsEndereco."E_ALL"]')
          ParentFont = False
        end
        object Memo15: TfrxMemoView
          Left = 548.031850000000000000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[Date]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo16: TfrxMemoView
          Left = 548.031850000000000000
          Top = 18.897650000000000000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Picture1: TfrxPictureView
          Left = 2.779530000000000000
          Top = 3.779530000000000000
          Width = 147.401574800000000000
          Height = 49.133890000000000000
          ShowHint = False
          HightQuality = False
          Transparent = False
          TransparentColor = clWhite
        end
        object frxDsEnderecoTE1: TfrxMemoView
          Left = 548.031850000000000000
          Top = 37.795300000000000000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsEndereco."TE1_TXT"]')
          ParentFont = False
        end
      end
    end
    object Page2: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Columns = 3
      ColumnWidth = 63.333333333333320000
      ColumnPositions.Strings = (
        '0'
        '63,3333333333333'
        '126,666666666667')
      PrintOnPreviousPage = True
      object MasterData1: TfrxMasterData
        Height = 15.118120000000000000
        Top = 98.267780000000000000
        Width = 239.370233333333300000
        DataSet = frxDs1Its
        DataSetName = 'frxDs1Its'
        RowCount = 0
        object Memo20: TfrxMemoView
          Width = 98.267750710000000000
          Height = 15.118120000000000000
          ShowHint = False
          DataSet = frxDs1Its
          DataSetName = 'frxDs1Its'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDs1Its."Abertura_TXT"]')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          Left = 98.267780000000000000
          Width = 64.251980710000000000
          Height = 15.118120000000000000
          ShowHint = False
          DataSet = frxDs1Its
          DataSetName = 'frxDs1Its'
          DisplayFormat.FormatStr = '#,###0'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDs1Its."Codigo"]')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          Left = 162.519790000000000000
          Width = 64.251980710000000000
          Height = 15.118120000000000000
          ShowHint = False
          DataSet = frxDs1Its
          DataSetName = 'frxDs1Its'
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDs1Its."PORCENT"]')
          ParentFont = False
        end
      end
      object GroupHeader1: TfrxGroupHeader
        Height = 34.015770000000000000
        Top = 41.574830000000000000
        Width = 239.370233333333300000
        Condition = 'frxDs1Its."MES"'
        object Memo1: TfrxMemoView
          Width = 226.771575430000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            'Per'#237'odo [frxDs1Its."MES"]')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          Top = 18.897650000000000000
          Width = 98.267750710000000000
          Height = 15.118120000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Data')
          ParentFont = False
        end
        object Memo40: TfrxMemoView
          Left = 98.267689690000000000
          Top = 18.897650000000000000
          Width = 64.251968500000000000
          Height = 15.118120000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Quantidade')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          Left = 162.519790000000000000
          Top = 18.897650000000000000
          Width = 64.251968500000000000
          Height = 15.118120000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '%')
          ParentFont = False
        end
      end
      object GroupFooter1: TfrxGroupFooter
        Height = 15.118120000000000000
        Top = 136.063080000000000000
        Width = 239.370233333333300000
        object Memo11: TfrxMemoView
          Width = 98.267716540000000000
          Height = 15.118120000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftTop]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Total')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          Left = 98.267780000000000000
          Width = 64.251946540000000000
          Height = 15.118120000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '#,###0'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftTop]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDs1Its."Codigo">)]')
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          Left = 162.519790000000000000
          Width = 64.251946540000000000
          Height = 15.118120000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftTop]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDs1Its."PORCENT">)]')
          ParentFont = False
        end
      end
      object Header1: TfrxHeader
        Top = 18.897650000000000000
        Width = 239.370233333333300000
      end
      object PageFooter1: TfrxPageFooter
        Height = 13.228346460000000000
        Top = 249.448980000000000000
        Width = 718.110700000000000000
        object Memo12: TfrxMemoView
          Width = 370.393940000000000000
          Height = 13.228346460000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - software customizado')
          ParentFont = False
          VAlign = vaCenter
        end
        object MeVARF_CODI_FRX: TfrxMemoView
          Left = 370.393940000000000000
          Width = 347.716760000000000000
          Height = 13.228346460000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'VARF_CODI_FRX')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object Footer1: TfrxFooter
        Height = 15.118120000000000000
        Top = 173.858380000000000000
        Width = 239.370233333333300000
        object Memo9: TfrxMemoView
          Width = 98.267716540000000000
          Height = 15.118120000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftTop]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Total')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          Left = 98.267780000000000000
          Width = 64.251946540000000000
          Height = 15.118120000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '#,###0'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftTop]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDs2Tot."Codigo"]')
          ParentFont = False
        end
        object Memo17: TfrxMemoView
          Left = 162.519790000000000000
          Width = 64.251946540000000000
          Height = 15.118120000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftTop]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDs1Its."PORCENT">)]')
          ParentFont = False
        end
      end
    end
  end
  object Qr1Gra: TmySQLQuery
    Database = Dmod.MyDBn
    OnCalcFields = Qr1GraCalcFields
    SQL.Strings = (
      'SELECT DATE_FORMAT(Abertura, "%m/%Y") Abertura,'
      'COUNT(Codigo) Codigo'
      'FROM wordser'
      'WHERE Codigo > 1'
      'GROUP BY DATE_FORMAT(Abertura, "%Y-%m")'
      'ORDER BY DATE_FORMAT(Abertura, "%Y-%m")')
    Left = 100
    Top = 8
    object Qr1GraCodigo: TLargeintField
      FieldName = 'Codigo'
      Origin = 'Codigo'
      Required = True
    end
    object Qr1GraAbertura: TWideStringField
      FieldName = 'Abertura'
      Origin = 'Abertura'
      Size = 7
    end
    object Qr1GraPORCENT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'PORCENT'
      Size = 10
      Calculated = True
    end
  end
  object Ds1Gra: TDataSource
    DataSet = Qr1Gra
    Left = 128
    Top = 8
  end
  object frxDs1Gra: TfrxDBDataset
    UserName = 'frxDs1Gra'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Codigo=Codigo'
      'Abertura=Abertura'
      'PORCENT=PORCENT')
    DataSet = Qr1Gra
    BCDToCurrency = False
    Left = 72
    Top = 8
  end
  object Ds1Its: TDataSource
    DataSet = Qr1Its
    Left = 128
    Top = 36
  end
  object Qr1Its: TmySQLQuery
    Database = Dmod.MyDBn
    OnCalcFields = Qr1ItsCalcFields
    SQL.Strings = (
      'SELECT DATE_FORMAT(Abertura, "%d/%m/%Y") Abertura_TXT,'
      'DATE_FORMAT(Abertura, "%m/%Y") MES, COUNT(Codigo) Codigo'
      'FROM wordser'
      'WHERE Abertura BETWEEN :P0 AND :P1'
      'GROUP BY Abertura_TXT'
      'ORDER BY Abertura ')
    Left = 100
    Top = 36
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object Qr1ItsAbertura_TXT: TWideStringField
      FieldName = 'Abertura_TXT'
      Size = 10
    end
    object Qr1ItsMES: TWideStringField
      FieldName = 'MES'
      Size = 7
    end
    object Qr1ItsCodigo: TLargeintField
      FieldName = 'Codigo'
      Required = True
    end
    object Qr1ItsPORCENT: TFloatField
      FieldKind = fkCalculated
      FieldName = 'PORCENT'
      DisplayFormat = '#,###,##0.00'
      Calculated = True
    end
  end
  object frxWEB_ORSER_010_002: TfrxReport
    Version = '4.14'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 40528.898746481500000000
    ReportOptions.LastChange = 40698.667057476850000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      
        '  MeVARF_CODI_FRX.Text := Copy(Report.Name, 4, Length(Report.Nam' +
        'e));'
      
        '  if <MeuLogoExiste> = True then Picture1.LoadFromFile(<MeuLogoC' +
        'aminho>);    '
      'end.')
    OnNewGetValue = frxWEB_ORSER_010_002NewGetValue
    Left = 44
    Top = 8
    Datasets = <
      item
        DataSet = frxDs2Gra
        DataSetName = 'frxDs2Gra'
      end
      item
        DataSet = frxDs2Its
        DataSetName = 'frxDs2Its'
      end
      item
        DataSet = frxDs2Tot
        DataSetName = 'frxDs2Tot'
      end
      item
        DataSet = DModG.frxDsEndereco
        DataSetName = 'frxDsEndereco'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 15.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object PageHeader1: TfrxPageHeader
        Height = 381.732334720000000000
        Top = 18.897650000000000000
        Width = 699.213050000000000000
        object Memo6: TfrxMemoView
          Top = 56.692950000000000000
          Width = 699.213050000000000000
          Height = 22.677165350000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            'Clientes Mais Atuantes no Per'#237'odo [PERIODO]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Shape1: TfrxShapeView
          Width = 699.213050000000000000
          Height = 56.692950000000000000
          ShowHint = False
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo13: TfrxMemoView
          Left = 154.960730000000000000
          Width = 393.071120000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsEndereco."NOME_ENT"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line1: TfrxLineView
          Left = 551.811380000000000000
          Top = 18.897650000000000000
          Width = 147.401670000000000000
          ShowHint = False
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo14: TfrxMemoView
          Left = 154.960730000000000000
          Top = 18.897650000000000000
          Width = 393.071120000000000000
          Height = 37.795275590000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsEndereco."E_ALL"]')
          ParentFont = False
        end
        object Memo15: TfrxMemoView
          Left = 548.031850000000000000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[Date]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo16: TfrxMemoView
          Left = 548.031850000000000000
          Top = 18.897650000000000000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Picture1: TfrxPictureView
          Left = 2.779530000000000000
          Top = 3.779530000000000000
          Width = 147.401574800000000000
          Height = 49.133890000000000000
          ShowHint = False
          HightQuality = False
          Transparent = False
          TransparentColor = clWhite
        end
        object frxDsEnderecoTE1: TfrxMemoView
          Left = 548.031850000000000000
          Top = 37.795300000000000000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsEndereco."TE1_TXT"]')
          ParentFont = False
        end
        object Chart3: TfrxChartView
          Top = 79.370130000000000000
          Width = 699.213050000000000000
          Height = 302.362204720000000000
          ShowHint = False
          Chart = {
            5450463006544368617274054368617274044C656674020003546F7002000557
            696474680390010648656967687403FA00144261636B57616C6C2E4272757368
            2E436F6C6F720707636C5768697465144261636B57616C6C2E50656E2E566973
            69626C6508104C6567656E642E416C69676E6D656E7407086C61426F74746F6D
            124C6567656E642E466F6E742E48656967687402F8105469746C652E466F6E74
            2E436F6C6F720707636C426C61636B115469746C652E466F6E742E4865696768
            7402F0105469746C652E466F6E742E5374796C650B066673426F6C640011426F
            74746F6D417869732E4C6162656C730817426F74746F6D417869732E4C616265
            6C734F6E41786973080D4672616D652E56697369626C6508175669657733444F
            7074696F6E732E456C65766174696F6E033B01195669657733444F7074696F6E
            732E50657273706563746976650200165669657733444F7074696F6E732E526F
            746174696F6E0368010A426576656C4F75746572070662764E6F6E6505436F6C
            6F720707636C576869746511436F6C6F7250616C65747465496E646578020D00
            0A54426172536572696573000E436F6C6F7245616368506F696E7409134D6172
            6B732E4172726F772E56697369626C6509194D61726B732E43616C6C6F75742E
            42727573682E436F6C6F720707636C426C61636B1B4D61726B732E43616C6C6F
            75742E4172726F772E56697369626C6509144D61726B732E43616C6C6F75742E
            4C656E67746802280A4D61726B732E436C6970090D4D61726B732E5669736962
            6C65080C5856616C7565732E4E616D650601580D5856616C7565732E4F726465
            72070B6C6F417363656E64696E670C5956616C7565732E4E616D650603426172
            0D5956616C7565732E4F7264657207066C6F4E6F6E65000000}
          ChartElevation = 315
          SeriesData = <
            item
              DataType = dtDBData
              DataSet = frxDs2Gra
              DataSetName = 'frxDs2Gra'
              SortOrder = soNone
              TopN = 0
              XType = xtText
              Source1 = 'frxDs2Gra."NOMECLI"'
              Source2 = 'frxDs2Gra."PORCENT"'
              XSource = 'frxDs2Gra."NOMECLI"'
              YSource = 'frxDs2Gra."PORCENT"'
            end>
        end
      end
    end
    object Page2: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Columns = 2
      ColumnWidth = 95.000000000000000000
      ColumnPositions.Strings = (
        '0'
        '95')
      PrintOnPreviousPage = True
      object PageFooter1: TfrxPageFooter
        Height = 13.228346460000000000
        Top = 249.448980000000000000
        Width = 718.110700000000000000
        object Memo12: TfrxMemoView
          Width = 370.393940000000000000
          Height = 13.228346460000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - software customizado')
          ParentFont = False
          VAlign = vaCenter
        end
        object MeVARF_CODI_FRX: TfrxMemoView
          Left = 370.393940000000000000
          Width = 347.716760000000000000
          Height = 13.228346460000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'VARF_CODI_FRX')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object Header1: TfrxHeader
        Top = 18.897650000000000000
        Width = 359.055350000000000000
      end
      object GroupHeader1: TfrxGroupHeader
        Height = 34.015770000000000000
        Top = 41.574830000000000000
        Width = 359.055350000000000000
        Condition = 'frxDs2Its."NOMECLI"'
        object Memo1: TfrxMemoView
          Width = 347.716528110000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            'Cliente [frxDs2Its."NOMECLI"]')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          Top = 18.897650000000000000
          Width = 219.212678980000000000
          Height = 15.118120000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Solicitante')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          Left = 219.212740000000000000
          Top = 18.897650000000000000
          Width = 64.251968500000000000
          Height = 15.118120000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Quantidade')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          Left = 283.464750000000000000
          Top = 18.897650000000000000
          Width = 64.251968500000000000
          Height = 15.118120000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '%')
          ParentFont = False
        end
      end
      object GroupFooter1: TfrxGroupFooter
        Height = 15.118120000000000000
        Top = 136.063080000000000000
        Width = 359.055350000000000000
        object Memo11: TfrxMemoView
          Width = 219.212378740000000000
          Height = 15.118120000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftTop]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Total')
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          Left = 219.212740000000000000
          Width = 64.251648740000000000
          Height = 15.118120000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '#,###0'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftTop]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDs2Its."Codigo">)]')
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          Left = 283.464750000000000000
          Width = 64.251648740000000000
          Height = 15.118120000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftTop]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDs2Its."PORCENT">)]')
          ParentFont = False
        end
      end
      object MasterData1: TfrxMasterData
        Height = 15.118120000000000000
        Top = 98.267780000000000000
        Width = 359.055350000000000000
        DataSet = frxDs2Its
        DataSetName = 'frxDs2Its'
        RowCount = 0
        object Memo20: TfrxMemoView
          Left = 219.212740000000000000
          Width = 64.251980710000000000
          Height = 15.118120000000000000
          ShowHint = False
          DataSet = frxDs2Its
          DataSetName = 'frxDs2Its'
          DisplayFormat.FormatStr = '#,###0'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDs2Its."Codigo"]')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          Left = 283.464750000000000000
          Width = 64.251980710000000000
          Height = 15.118120000000000000
          ShowHint = False
          DataSet = frxDs2Its
          DataSetName = 'frxDs2Its'
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDs2Its."PORCENT"]')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          Width = 219.212710710000000000
          Height = 15.118120000000000000
          ShowHint = False
          DataField = 'NOMESOLICIT'
          DataSet = frxDs2Its
          DataSetName = 'frxDs2Its'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDs2Its."NOMESOLICIT"]')
          ParentFont = False
        end
      end
      object Footer1: TfrxFooter
        Height = 15.118120000000000000
        Top = 173.858380000000000000
        Width = 359.055350000000000000
        object Memo10: TfrxMemoView
          Width = 219.212378740000000000
          Height = 15.118120000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftTop]
          HAlign = haRight
          Memo.UTF8W = (
            'Total')
          ParentFont = False
        end
        object Memo17: TfrxMemoView
          Left = 219.212740000000000000
          Width = 64.251648740000000000
          Height = 15.118120000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '#,###0'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftTop]
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDs2Tot."Codigo"]')
          ParentFont = False
        end
        object Memo18: TfrxMemoView
          Left = 283.464750000000000000
          Width = 64.251648740000000000
          Height = 15.118120000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftTop]
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDs2Its."PORCENT">)]')
          ParentFont = False
        end
      end
    end
  end
  object frxDs1Its: TfrxDBDataset
    UserName = 'frxDs1Its'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Abertura_TXT=Abertura_TXT'
      'MES=MES'
      'Codigo=Codigo'
      'PORCENT=PORCENT')
    DataSet = Qr1Its
    BCDToCurrency = False
    Left = 72
    Top = 36
  end
  object frxDs2Gra: TfrxDBDataset
    UserName = 'frxDs2Gra'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Codigo=Codigo'
      'NOMECLI=NOMECLI'
      'PORCENT=PORCENT')
    DataSet = Qr2Gra
    BCDToCurrency = False
    Left = 156
    Top = 8
  end
  object Qr2Gra: TmySQLQuery
    Database = Dmod.MyDBn
    OnCalcFields = Qr2GraCalcFields
    SQL.Strings = (
      'SELECT COUNT(wor.Codigo) Codigo,'
      'IF (ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMECLI'
      'FROM wordser wor'
      'LEFT JOIN entidades ent ON ent.Codigo = wor.Cliente'
      'WHERE wor.Codigo > 1'
      'GROUP BY wor.Cliente'
      'ORDER BY Codigo DESC, NOMECLI ASC')
    Left = 184
    Top = 8
    object Qr2GraCodigo: TLargeintField
      FieldName = 'Codigo'
      Origin = 'Codigo'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object Qr2GraNOMECLI: TWideStringField
      FieldName = 'NOMECLI'
      Origin = 'NOMECLI'
      Size = 100
    end
    object Qr2GraPORCENT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'PORCENT'
      Size = 10
      Calculated = True
    end
  end
  object Ds2Gra: TDataSource
    DataSet = Qr2Gra
    Left = 212
    Top = 8
  end
  object Qr2Its: TmySQLQuery
    Database = Dmod.MyDBn
    OnCalcFields = Qr2ItsCalcFields
    SQL.Strings = (
      'SELECT '
      '('
      'SELECT COUNT(Codigo) '
      'FROM wordser'
      'WHERE Cliente = wor.Cliente'
      'GROUP BY Cliente'
      ') CodCli, '
      'Count(wor.Codigo) Codigo, wor.Cliente,'
      'IF (ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMECLI,'
      'wus.PersonalName NOMESOLICIT'
      'FROM wordser wor'
      'LEFT JOIN wusers wus ON wus.Codigo = wor.Solicit'
      'LEFT JOIN entidades ent ON ent.Codigo = wor.Cliente'
      'GROUP BY wor.Solicit, wor.Cliente'
      'ORDER BY CodCli DESC, Codigo DESC')
    Left = 184
    Top = 36
    object Qr2ItsCodigo: TLargeintField
      FieldName = 'Codigo'
      Origin = 'Codigo'
      Required = True
    end
    object Qr2ItsCliente: TIntegerField
      FieldName = 'Cliente'
      Origin = 'wordser.Cliente'
    end
    object Qr2ItsNOMECLI: TWideStringField
      FieldName = 'NOMECLI'
      Origin = 'NOMECLI'
      Size = 100
    end
    object Qr2ItsNOMESOLICIT: TWideStringField
      FieldName = 'NOMESOLICIT'
      Origin = 'wusers.PersonalName'
      Size = 32
    end
    object Qr2ItsCodCli: TLargeintField
      FieldName = 'CodCli'
      Origin = 'CodCli'
    end
    object Qr2ItsPORCENT: TFloatField
      FieldKind = fkCalculated
      FieldName = 'PORCENT'
      DisplayFormat = '#,###,##0.00'
      Calculated = True
    end
  end
  object Ds2Its: TDataSource
    DataSet = Qr2Its
    Left = 212
    Top = 36
  end
  object frxDs2Its: TfrxDBDataset
    UserName = 'frxDs2Its'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Codigo=Codigo'
      'Cliente=Cliente'
      'NOMECLI=NOMECLI'
      'NOMESOLICIT=NOMESOLICIT'
      'CodCli=CodCli'
      'PORCENT=PORCENT')
    DataSet = Qr2Its
    BCDToCurrency = False
    Left = 156
    Top = 36
  end
  object frxWEB_ORSER_010_003: TfrxReport
    Version = '4.14'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 40528.898746481500000000
    ReportOptions.LastChange = 40699.478840625000000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      
        '  MeVARF_CODI_FRX.Text := Copy(Report.Name, 4, Length(Report.Nam' +
        'e));'
      
        '  if <MeuLogoExiste> = True then Picture1.LoadFromFile(<MeuLogoC' +
        'aminho>);    '
      'end.')
    OnNewGetValue = frxWEB_ORSER_010_002NewGetValue
    Left = 16
    Top = 36
    Datasets = <
      item
        DataSet = frxDs2Tot
        DataSetName = 'frxDs2Tot'
      end
      item
        DataSet = frxDs3Gra
        DataSetName = 'frxDs3Gra'
      end
      item
        DataSet = frxDs3Its
        DataSetName = 'frxDs3Its'
      end
      item
        DataSet = DModG.frxDsEndereco
        DataSetName = 'frxDsEndereco'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 15.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object PageHeader1: TfrxPageHeader
        Height = 381.732334720000000000
        Top = 18.897650000000000000
        Width = 699.213050000000000000
        object Memo6: TfrxMemoView
          Top = 56.692950000000000000
          Width = 699.213050000000000000
          Height = 22.677165350000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            'Assuntos Mais Utilizados no Per'#237'odo [PERIODO]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Shape1: TfrxShapeView
          Width = 699.213050000000000000
          Height = 56.692950000000000000
          ShowHint = False
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo13: TfrxMemoView
          Left = 154.960730000000000000
          Width = 393.071120000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsEndereco."NOME_ENT"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line1: TfrxLineView
          Left = 551.811380000000000000
          Top = 18.897650000000000000
          Width = 147.401670000000000000
          ShowHint = False
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo14: TfrxMemoView
          Left = 154.960730000000000000
          Top = 18.897650000000000000
          Width = 393.071120000000000000
          Height = 37.795275590000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsEndereco."E_ALL"]')
          ParentFont = False
        end
        object Memo15: TfrxMemoView
          Left = 548.031850000000000000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[Date]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo16: TfrxMemoView
          Left = 548.031850000000000000
          Top = 18.897650000000000000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Picture1: TfrxPictureView
          Left = 2.779530000000000000
          Top = 3.779530000000000000
          Width = 147.401574800000000000
          Height = 49.133890000000000000
          ShowHint = False
          HightQuality = False
          Transparent = False
          TransparentColor = clWhite
        end
        object frxDsEnderecoTE1: TfrxMemoView
          Left = 548.031850000000000000
          Top = 37.795300000000000000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsEndereco."TE1_TXT"]')
          ParentFont = False
        end
        object Chart3: TfrxChartView
          Top = 79.370130000000000000
          Width = 699.213050000000000000
          Height = 302.362204720000000000
          ShowHint = False
          Chart = {
            5450463006544368617274054368617274044C656674020003546F7002000557
            696474680390010648656967687403FA00144261636B57616C6C2E4272757368
            2E436F6C6F720707636C5768697465144261636B57616C6C2E50656E2E566973
            69626C6508104C6567656E642E416C69676E6D656E7407086C61426F74746F6D
            124C6567656E642E466F6E742E48656967687402F8105469746C652E466F6E74
            2E436F6C6F720707636C426C61636B115469746C652E466F6E742E4865696768
            7402F0105469746C652E466F6E742E5374796C650B066673426F6C640011426F
            74746F6D417869732E4C6162656C730817426F74746F6D417869732E4C616265
            6C734F6E41786973080D4672616D652E56697369626C6508175669657733444F
            7074696F6E732E456C65766174696F6E033B01195669657733444F7074696F6E
            732E50657273706563746976650200165669657733444F7074696F6E732E526F
            746174696F6E0368010A426576656C4F75746572070662764E6F6E6505436F6C
            6F720707636C576869746511436F6C6F7250616C65747465496E646578020D00
            0A54426172536572696573000E436F6C6F7245616368506F696E7409134D6172
            6B732E4172726F772E56697369626C6509194D61726B732E43616C6C6F75742E
            42727573682E436F6C6F720707636C426C61636B1B4D61726B732E43616C6C6F
            75742E4172726F772E56697369626C6509144D61726B732E43616C6C6F75742E
            4C656E67746802280A4D61726B732E436C6970090D4D61726B732E5669736962
            6C65080C5856616C7565732E4E616D650601580D5856616C7565732E4F726465
            72070B6C6F417363656E64696E670C5956616C7565732E4E616D650603426172
            0D5956616C7565732E4F7264657207066C6F4E6F6E65000000}
          ChartElevation = 315
          SeriesData = <
            item
              DataType = dtDBData
              DataSet = frxDs3Gra
              DataSetName = 'frxDs3Gra'
              SortOrder = soNone
              TopN = 0
              XType = xtText
              Source1 = 'frxDs3Gra."NOMEASS"'
              Source2 = 'frxDs3Gra."PORCENT"'
              XSource = 'frxDs3Gra."NOMEASS"'
              YSource = 'frxDs3Gra."PORCENT"'
            end>
        end
      end
    end
    object Page2: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Columns = 2
      ColumnWidth = 95.000000000000000000
      ColumnPositions.Strings = (
        '0'
        '95')
      PrintOnPreviousPage = True
      object PageFooter1: TfrxPageFooter
        Height = 13.228346460000000000
        Top = 249.448980000000000000
        Width = 718.110700000000000000
        object Memo12: TfrxMemoView
          Width = 370.393940000000000000
          Height = 13.228346460000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - software customizado')
          ParentFont = False
          VAlign = vaCenter
        end
        object MeVARF_CODI_FRX: TfrxMemoView
          Left = 370.393940000000000000
          Width = 347.716760000000000000
          Height = 13.228346460000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'VARF_CODI_FRX')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object Header1: TfrxHeader
        Top = 18.897650000000000000
        Width = 359.055350000000000000
      end
      object GroupHeader1: TfrxGroupHeader
        Height = 34.015770000000000000
        Top = 41.574830000000000000
        Width = 359.055350000000000000
        Condition = 'frxDs3Its."MES"'
        object Memo3: TfrxMemoView
          Left = 219.212740000000000000
          Top = 18.897650000000000000
          Width = 64.251968500000000000
          Height = 15.118120000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Quantidade')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          Left = 0.000163540000000000
          Top = 18.897650000000000000
          Width = 219.212576460000000000
          Height = 15.118120000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Assunto')
          ParentFont = False
        end
        object Memo1: TfrxMemoView
          Left = 0.000231890000000000
          Width = 347.716535433071000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            'Per'#237'odo de [frxDs3Its."MES"]')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          Left = 283.464750000000000000
          Top = 18.897650000000000000
          Width = 64.251968500000000000
          Height = 15.118120000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '%')
          ParentFont = False
        end
      end
      object GroupFooter1: TfrxGroupFooter
        Height = 15.118120000000000000
        Top = 136.063080000000000000
        Width = 359.055350000000000000
        object Memo11: TfrxMemoView
          Left = 0.000361260000000000
          Width = 219.212378740000000000
          Height = 15.118120000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftTop]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Total')
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          Left = 219.212740000000000000
          Width = 64.251648740000000000
          Height = 15.118120000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '#,###0'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftTop]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDs3Its."Codigo">)]')
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          Left = 283.464750000000000000
          Width = 64.251648740000000000
          Height = 15.118120000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftTop]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDs3Its."PORCENT">)]')
          ParentFont = False
        end
      end
      object MasterData1: TfrxMasterData
        Height = 15.118120000000000000
        Top = 98.267780000000000000
        Width = 359.055350000000000000
        Columns = 1
        ColumnWidth = 359.055118110236000000
        DataSet = frxDs3Its
        DataSetName = 'frxDs3Its'
        RowCount = 0
        object Memo20: TfrxMemoView
          Left = 219.212740000000000000
          Width = 64.251980710000000000
          Height = 15.118120000000000000
          ShowHint = False
          DataSet = frxDs3Its
          DataSetName = 'frxDs3Its'
          DisplayFormat.FormatStr = '#,###0'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDs3Its."Codigo"]')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          Width = 219.212588660000000000
          Height = 15.118120000000000000
          ShowHint = False
          DataField = 'NOMEASS'
          DataSet = frxDs3Its
          DataSetName = 'frxDs3Its'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDs3Its."NOMEASS"]')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          Left = 283.464750000000000000
          Width = 64.251980710000000000
          Height = 15.118120000000000000
          ShowHint = False
          DataSet = frxDs3Its
          DataSetName = 'frxDs3Its'
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDs3Its."PORCENT"]')
          ParentFont = False
        end
      end
      object Footer1: TfrxFooter
        Height = 15.118120000000000000
        Top = 173.858380000000000000
        Width = 359.055350000000000000
        object Memo10: TfrxMemoView
          Width = 219.212378740000000000
          Height = 15.118120000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftTop]
          HAlign = haRight
          Memo.UTF8W = (
            'Total')
          ParentFont = False
        end
        object Memo17: TfrxMemoView
          Left = 219.212378740000000000
          Width = 64.251648740000000000
          Height = 15.118120000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '#,###0'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftTop]
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDs2Tot."Codigo"]')
          ParentFont = False
        end
        object Memo18: TfrxMemoView
          Left = 283.464388740000000000
          Width = 64.251648740000000000
          Height = 15.118120000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftTop]
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDs3Its."PORCENT">)]')
          ParentFont = False
        end
      end
    end
  end
  object frxDs3Gra: TfrxDBDataset
    UserName = 'frxDs3Gra'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Codigo=Codigo'
      'PORCENT=PORCENT'
      'MES=MES'
      'NOMEASS=NOMEASS')
    DataSet = Qr3Gra
    BCDToCurrency = False
    Left = 240
    Top = 8
  end
  object Qr3Gra: TmySQLQuery
    Database = Dmod.MyDBn
    OnCalcFields = Qr3GraCalcFields
    SQL.Strings = (
      'SELECT COUNT(wor.Codigo) Codigo,'
      'DATE_FORMAT(wor.Abertura, "%m/%Y") MES,'
      'ass.Nome NOMEASS'
      'FROM wordser wor'
      'LEFT JOIN wordserass ass ON ass.Codigo = wor.Assunto'
      'WHERE wor.Codigo > 1'
      'GROUP BY wor.Assunto'
      'ORDER BY wor.Codigo DESC')
    Left = 268
    Top = 8
    object Qr3GraCodigo: TLargeintField
      FieldName = 'Codigo'
      Origin = 'Codigo'
      Required = True
    end
    object Qr3GraPORCENT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'PORCENT'
      Size = 10
      Calculated = True
    end
    object Qr3GraMES: TWideStringField
      FieldName = 'MES'
      Size = 7
    end
    object Qr3GraNOMEASS: TWideStringField
      FieldName = 'NOMEASS'
      Size = 50
    end
  end
  object Ds3Gra: TDataSource
    DataSet = Qr3Gra
    Left = 296
    Top = 8
  end
  object frxDs3Its: TfrxDBDataset
    UserName = 'frxDs3Its'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Codigo=Codigo'
      'MES=MES'
      'PORCENT=PORCENT'
      'NOMEASS=NOMEASS')
    DataSet = Qr3Its
    BCDToCurrency = False
    Left = 240
    Top = 36
  end
  object Ds3Its: TDataSource
    DataSet = Qr3Its
    Left = 296
    Top = 36
  end
  object Qr3Its: TmySQLQuery
    Database = Dmod.MyDBn
    OnCalcFields = Qr3ItsCalcFields
    SQL.Strings = (
      'SELECT COUNT(wor.Codigo) Codigo, ass.Nome NOMEASS, '
      'DATE_FORMAT(wor.Abertura, "%m/%Y") MES'
      'FROM wordser wor'
      'LEFT JOIN wordserass ass ON ass.Codigo = wor.Assunto'
      'GROUP BY wor.Assunto, MES'
      'ORDER BY wor.Codigo, NOMEASS')
    Left = 268
    Top = 36
    object Qr3ItsCodigo: TLargeintField
      FieldName = 'Codigo'
      Origin = 'Codigo'
      Required = True
    end
    object Qr3ItsMES: TWideStringField
      FieldName = 'MES'
      Origin = 'MES'
      Size = 7
    end
    object Qr3ItsPORCENT: TFloatField
      FieldKind = fkCalculated
      FieldName = 'PORCENT'
      DisplayFormat = '#,###,##0.00'
      Calculated = True
    end
    object Qr3ItsNOMEASS: TWideStringField
      FieldName = 'NOMEASS'
      Size = 50
    end
  end
  object frxWEB_ORSER_010_004: TfrxReport
    Version = '4.14'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 40528.898746481500000000
    ReportOptions.LastChange = 40531.624545243100000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      
        '  MeVARF_CODI_FRX.Text := Copy(Report.Name, 4, Length(Report.Nam' +
        'e));'
      
        '  if <MeuLogoExiste> = True then Picture1.LoadFromFile(<MeuLogoC' +
        'aminho>);    '
      'end.')
    OnNewGetValue = frxWEB_ORSER_010_002NewGetValue
    Left = 44
    Top = 36
    Datasets = <
      item
        DataSet = frxDs2Tot
        DataSetName = 'frxDs2Tot'
      end
      item
        DataSet = frxDs4Gra
        DataSetName = 'frxDs4Gra'
      end
      item
        DataSet = frxDs4Its
        DataSetName = 'frxDs4Its'
      end
      item
        DataSet = DModG.frxDsEndereco
        DataSetName = 'frxDsEndereco'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 15.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object PageHeader1: TfrxPageHeader
        Height = 381.732334720000000000
        Top = 18.897650000000000000
        Width = 699.213050000000000000
        object Memo6: TfrxMemoView
          Top = 56.692950000000000000
          Width = 699.213050000000000000
          Height = 22.677165350000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            'Modos de Solicita'#231#245'es Mais Utilizados no Per'#237'odo [PERIODO]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Shape1: TfrxShapeView
          Width = 699.213050000000000000
          Height = 56.692950000000000000
          ShowHint = False
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo13: TfrxMemoView
          Left = 154.960730000000000000
          Width = 393.071120000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsEndereco."NOME_ENT"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line1: TfrxLineView
          Left = 551.811380000000000000
          Top = 18.897650000000000000
          Width = 147.401670000000000000
          ShowHint = False
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo14: TfrxMemoView
          Left = 154.960730000000000000
          Top = 18.897650000000000000
          Width = 393.071120000000000000
          Height = 37.795275590000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsEndereco."E_ALL"]')
          ParentFont = False
        end
        object Memo15: TfrxMemoView
          Left = 548.031850000000000000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[Date]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo16: TfrxMemoView
          Left = 548.031850000000000000
          Top = 18.897650000000000000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Picture1: TfrxPictureView
          Left = 2.779530000000000000
          Top = 3.779530000000000000
          Width = 147.401574800000000000
          Height = 49.133890000000000000
          ShowHint = False
          HightQuality = False
          Transparent = False
          TransparentColor = clWhite
        end
        object frxDsEnderecoTE1: TfrxMemoView
          Left = 548.031850000000000000
          Top = 37.795300000000000000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsEndereco."TE1_TXT"]')
          ParentFont = False
        end
        object Chart3: TfrxChartView
          Top = 79.370130000000000000
          Width = 699.213050000000000000
          Height = 302.362204720000000000
          ShowHint = False
          Chart = {
            5450463006544368617274054368617274044C656674020003546F7002000557
            696474680390010648656967687403FA00144261636B57616C6C2E4272757368
            2E436F6C6F720707636C5768697465144261636B57616C6C2E50656E2E566973
            69626C6508104C6567656E642E416C69676E6D656E7407086C61426F74746F6D
            124C6567656E642E466F6E742E48656967687402F8105469746C652E466F6E74
            2E436F6C6F720707636C426C61636B115469746C652E466F6E742E4865696768
            7402F0105469746C652E466F6E742E5374796C650B066673426F6C640011426F
            74746F6D417869732E4C6162656C730817426F74746F6D417869732E4C616265
            6C734F6E41786973080D4672616D652E56697369626C6508175669657733444F
            7074696F6E732E456C65766174696F6E033B01195669657733444F7074696F6E
            732E50657273706563746976650200165669657733444F7074696F6E732E526F
            746174696F6E0368010A426576656C4F75746572070662764E6F6E6505436F6C
            6F720707636C576869746511436F6C6F7250616C65747465496E646578020D00
            0A54426172536572696573000E436F6C6F7245616368506F696E7409134D6172
            6B732E4172726F772E56697369626C6509194D61726B732E43616C6C6F75742E
            42727573682E436F6C6F720707636C426C61636B1B4D61726B732E43616C6C6F
            75742E4172726F772E56697369626C6509144D61726B732E43616C6C6F75742E
            4C656E67746802280A4D61726B732E436C6970090D4D61726B732E5669736962
            6C65080C5856616C7565732E4E616D650601580D5856616C7565732E4F726465
            72070B6C6F417363656E64696E670C5956616C7565732E4E616D650603426172
            0D5956616C7565732E4F7264657207066C6F4E6F6E65000000}
          ChartElevation = 315
          SeriesData = <
            item
              DataType = dtDBData
              DataSet = frxDs4Gra
              DataSetName = 'frxDs4Gra'
              SortOrder = soNone
              TopN = 0
              XType = xtText
              Source1 = 'frxDs4Gra."NOMEMOD"'
              Source2 = 'frxDs4Gra."PORCENT"'
              XSource = 'frxDs4Gra."NOMEMOD"'
              YSource = 'frxDs4Gra."PORCENT"'
            end>
        end
      end
    end
    object Page2: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Columns = 2
      ColumnWidth = 95.000000000000000000
      ColumnPositions.Strings = (
        '0'
        '95')
      PrintOnPreviousPage = True
      object PageFooter1: TfrxPageFooter
        Height = 13.228346460000000000
        Top = 257.008040000000000000
        Width = 718.110700000000000000
        object Memo12: TfrxMemoView
          Width = 370.393940000000000000
          Height = 13.228346460000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - software customizado')
          ParentFont = False
          VAlign = vaCenter
        end
        object MeVARF_CODI_FRX: TfrxMemoView
          Left = 370.393940000000000000
          Width = 347.716760000000000000
          Height = 13.228346460000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'VARF_CODI_FRX')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object Header1: TfrxHeader
        Top = 18.897650000000000000
        Width = 359.055350000000000000
      end
      object GroupHeader1: TfrxGroupHeader
        Height = 34.015770000000000000
        Top = 41.574830000000000000
        Width = 359.055350000000000000
        Condition = 'frxDs4Its."MES"'
        object Memo3: TfrxMemoView
          Left = 219.212740000000000000
          Top = 18.897650000000000000
          Width = 64.251968500000000000
          Height = 15.118120000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Quantidade')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          Left = 0.000163540000000000
          Top = 18.897650000000000000
          Width = 219.212576460000000000
          Height = 15.118120000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Modo')
          ParentFont = False
        end
        object Memo1: TfrxMemoView
          Width = 347.716535433070900000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            'Per'#237'odo de [frxDs4Its."MES"]')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          Left = 283.464750000000000000
          Top = 18.897650000000000000
          Width = 64.251968500000000000
          Height = 15.118120000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '%')
          ParentFont = False
        end
      end
      object GroupFooter1: TfrxGroupFooter
        Height = 15.118120000000000000
        Top = 136.063080000000000000
        Width = 359.055350000000000000
        object Memo11: TfrxMemoView
          Left = 0.000361260000000000
          Width = 219.212378740000000000
          Height = 15.118120000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftTop]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Total')
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          Left = 219.212740000000000000
          Width = 64.251968500000000000
          Height = 15.118120000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '#,###0'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftTop]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDs4Its."Codigo">)]')
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          Left = 283.464750000000000000
          Width = 64.251968500000000000
          Height = 15.118120000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftTop]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDs4Its."PORCENT">)]')
          ParentFont = False
        end
      end
      object MasterData1: TfrxMasterData
        Height = 15.118120000000000000
        Top = 98.267780000000000000
        Width = 359.055350000000000000
        ColumnWidth = 359.055118110236000000
        DataSet = frxDs4Its
        DataSetName = 'frxDs4Its'
        RowCount = 0
        object Memo20: TfrxMemoView
          Left = 219.212740000000000000
          Width = 64.251980710000000000
          Height = 15.118120000000000000
          ShowHint = False
          DataSet = frxDs4Its
          DataSetName = 'frxDs4Its'
          DisplayFormat.FormatStr = '#,###0'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDs4Its."Codigo"]')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          Width = 219.212588660000000000
          Height = 15.118120000000000000
          ShowHint = False
          DataField = 'NOMEMODO'
          DataSet = frxDs4Its
          DataSetName = 'frxDs4Its'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDs4Its."NOMEMODO"]')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          Left = 283.464750000000000000
          Width = 64.251980710000000000
          Height = 15.118120000000000000
          ShowHint = False
          DataSet = frxDs4Its
          DataSetName = 'frxDs4Its'
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDs4Its."PORCENT"]')
          ParentFont = False
        end
      end
      object Footer1: TfrxFooter
        Height = 22.677180000000000000
        Top = 173.858380000000000000
        Width = 359.055350000000000000
        object Memo10: TfrxMemoView
          Width = 219.212378740000000000
          Height = 15.118120000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftTop]
          HAlign = haRight
          Memo.UTF8W = (
            'Total')
          ParentFont = False
        end
        object Memo17: TfrxMemoView
          Left = 219.212378740000000000
          Width = 64.251968500000000000
          Height = 15.118120000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '#,###0'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftTop]
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDs2Tot."Codigo"]')
          ParentFont = False
        end
        object Memo18: TfrxMemoView
          Left = 283.464388740000000000
          Width = 64.251968500000000000
          Height = 15.118120000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftTop]
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDs4Its."PORCENT">)]')
          ParentFont = False
        end
      end
    end
  end
  object frxDs4Gra: TfrxDBDataset
    UserName = 'frxDs4Gra'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Codigo=Codigo'
      'NOMEMOD=NOMEMOD'
      'PORCENT=PORCENT')
    DataSet = Qr4Gra
    BCDToCurrency = False
    Left = 324
    Top = 8
  end
  object frxDs4Its: TfrxDBDataset
    UserName = 'frxDs4Its'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Codigo=Codigo'
      'NOMEMODO=NOMEMODO'
      'MES=MES'
      'PORCENT=PORCENT')
    DataSet = Qr4Its
    BCDToCurrency = False
    Left = 324
    Top = 36
  end
  object Qr4Its: TmySQLQuery
    Database = Dmod.MyDBn
    OnCalcFields = Qr4ItsCalcFields
    SQL.Strings = (
      'SELECT COUNT(wor.Codigo) Codigo, wmo.Nome NOMEMODO,  '
      'DATE_FORMAT(wor.Abertura, "%m/%Y") MES'
      'FROM wordser wor'
      'LEFT JOIN wordsermod wmo ON wmo.Codigo = wor.Modo'
      'GROUP BY wor.Modo, MES'
      'ORDER BY Codigo, NOMEMODO')
    Left = 352
    Top = 36
    object Qr4ItsNOMEMODO: TWideStringField
      FieldName = 'NOMEMODO'
      Origin = 'wordsermod.Nome'
    end
    object Qr4ItsMES: TWideStringField
      FieldName = 'MES'
      Origin = 'MES'
      Size = 7
    end
    object Qr4ItsPORCENT: TFloatField
      FieldKind = fkCalculated
      FieldName = 'PORCENT'
      DisplayFormat = '#,###,##0.00'
      Calculated = True
    end
    object Qr4ItsCodigo: TLargeintField
      FieldName = 'Codigo'
      Required = True
    end
  end
  object Ds4Its: TDataSource
    DataSet = Qr4Its
    Left = 380
    Top = 36
  end
  object Ds4Gra: TDataSource
    DataSet = Qr4Gra
    Left = 380
    Top = 8
  end
  object Qr4Gra: TmySQLQuery
    Database = Dmod.MyDBn
    OnCalcFields = Qr4GraCalcFields
    SQL.Strings = (
      'SELECT COUNT(wor.Codigo) Codigo,'
      'wmo.Nome NOMEMOD'
      'FROM wordser wor'
      'LEFT JOIN wordsermod wmo ON wmo.Codigo = wor.Modo'
      'WHERE wor.Codigo > 1'
      'GROUP BY wor.Modo'
      'ORDER BY Codigo DESC')
    Left = 352
    Top = 8
    object Qr4GraNOMEMOD: TWideStringField
      FieldName = 'NOMEMOD'
      Origin = 'wordsermod.Nome'
    end
    object Qr4GraPORCENT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'PORCENT'
      Size = 10
      Calculated = True
    end
    object Qr4GraCodigo: TLargeintField
      FieldName = 'Codigo'
      Required = True
    end
  end
  object Qr2Tot: TmySQLQuery
    Database = Dmod.MyDBn
    SQL.Strings = (
      'SELECT COUNT(Codigo) Codigo'
      'FROM wordser'
      'WHERE Abertura BETWEEN :P0 AND :P1')
    Left = 184
    Top = 64
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object Qr2TotCodigo: TLargeintField
      FieldName = 'Codigo'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
  end
  object Ds2Tot: TDataSource
    DataSet = Qr2Tot
    Left = 212
    Top = 64
  end
  object frxDs2Tot: TfrxDBDataset
    UserName = 'frxDs2Tot'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Codigo=Codigo')
    DataSet = Qr2Tot
    BCDToCurrency = False
    Left = 156
    Top = 64
  end
  object Qr5Its: TmySQLQuery
    Database = Dmod.MyDBn
    OnCalcFields = Qr5ItsCalcFields
    SQL.Strings = (
      'SELECT DATE_FORMAT(Encerr, "%d/%m/%Y") Encerr_TXT,'
      'DATE_FORMAT(Encerr, "%m/%Y") MES, COUNT(Codigo) Codigo'
      'FROM wordser'
      'WHERE Encerr BETWEEN :P0 AND :P1'
      'GROUP BY Encerr_TXT'
      'ORDER BY Encerr ')
    Left = 100
    Top = 92
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object StringField2: TWideStringField
      FieldName = 'MES'
      Size = 7
    end
    object Qr5ItsCodigo: TLargeintField
      FieldName = 'Codigo'
      Required = True
    end
    object Qr5ItsPORCENT: TFloatField
      FieldKind = fkCalculated
      FieldName = 'PORCENT'
      DisplayFormat = '#,###,##0.00'
      Calculated = True
    end
    object Qr5ItsEncerr_TXT: TWideStringField
      FieldName = 'Encerr_TXT'
      Size = 10
    end
  end
  object Ds5Its: TDataSource
    DataSet = Qr5Its
    Left = 128
    Top = 92
  end
  object Ds5Gra: TDataSource
    DataSet = Qr5Gra
    Left = 128
    Top = 64
  end
  object Qr5Gra: TmySQLQuery
    Database = Dmod.MyDBn
    OnCalcFields = Qr5GraCalcFields
    SQL.Strings = (
      'SELECT DATE_FORMAT(Encerr, "%m/%Y") Encerr,'
      'COUNT(Codigo) Codigo'
      'FROM wordser'
      'WHERE Codigo > 1'
      'GROUP BY DATE_FORMAT(Encerr, "%Y-%m")'
      'ORDER BY DATE_FORMAT(Encerr, "%Y-%m")')
    Left = 100
    Top = 64
    object Qr5GraCodigo: TLargeintField
      FieldName = 'Codigo'
      Origin = 'Codigo'
      Required = True
    end
    object Qr5GraPORCENT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'PORCENT'
      Size = 10
      Calculated = True
    end
    object Qr5GraEncerr: TWideStringField
      FieldName = 'Encerr'
      Size = 7
    end
  end
  object frxWEB_ORSER_010_005: TfrxReport
    Version = '4.14'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 40528.898746481500000000
    ReportOptions.LastChange = 41316.611579976850000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      
        '  MeVARF_CODI_FRX.Text := Copy(Report.Name, 4, Length(Report.Nam' +
        'e));'
      
        '  if <MeuLogoExiste> = True then Picture1.LoadFromFile(<MeuLogoC' +
        'aminho>);    '
      'end.')
    OnNewGetValue = frxWEB_ORSER_010_001NewGetValue
    Left = 16
    Top = 64
    Datasets = <
      item
        DataSet = frxDs5Gra
        DataSetName = 'frxDs5Gra'
      end
      item
        DataSet = frxDs5Its
        DataSetName = 'frxDs5Its'
      end
      item
        DataSet = frxDs5Tot
        DataSetName = 'frxDs5Tot'
      end
      item
        DataSet = DModG.frxDsEndereco
        DataSetName = 'frxDsEndereco'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 15.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object PageHeader1: TfrxPageHeader
        Height = 381.732334720000000000
        Top = 18.897650000000000000
        Width = 699.213050000000000000
        object Chart1: TfrxChartView
          Top = 79.370130000000000000
          Width = 699.212586220000000000
          Height = 302.362204720000000000
          ShowHint = False
          Chart = {
            5450463006544368617274054368617274044C656674020003546F7002000557
            696474680390010648656967687403FA00144261636B57616C6C2E50656E2E56
            697369626C6508104C6567656E642E416C69676E6D656E7407086C61426F7474
            6F6D124C6567656E642E466F6E742E48656967687402F80D4672616D652E5669
            7369626C6508165669657733444F7074696F6E732E526F746174696F6E02000A
            426576656C4F75746572070662764E6F6E6505436F6C6F720707636C57686974
            6511436F6C6F7250616C65747465496E646578020D000B544C696E6553657269
            65730753657269657331134D61726B732E4172726F772E56697369626C650819
            4D61726B732E43616C6C6F75742E42727573682E436F6C6F720707636C426C61
            636B154D61726B732E43616C6C6F75742E56697369626C65091B4D61726B732E
            43616C6C6F75742E4172726F772E56697369626C65080D4D61726B732E566973
            69626C650816506F696E7465722E496E666C6174654D617267696E73090D506F
            696E7465722E5374796C65070B707352656374616E676C650F506F696E746572
            2E56697369626C65080C5856616C7565732E4E616D650601580D5856616C7565
            732E4F72646572070B6C6F417363656E64696E670C5956616C7565732E4E616D
            650601590D5956616C7565732E4F7264657207066C6F4E6F6E65000000}
          ChartElevation = 345
          SeriesData = <
            item
              DataType = dtDBData
              DataSet = frxDs5Gra
              DataSetName = 'frxDs5Gra'
              SortOrder = soNone
              TopN = 0
              XType = xtText
              Source1 = 'frxDs5Gra."Encerr"'
              Source2 = 'frxDs5Gra."PORCENT"'
              Source3 = 'frxDs5Gra."Encerr"'
              XSource = 'frxDs5Gra."Encerr"'
              YSource = 'frxDs5Gra."PORCENT"'
            end>
        end
        object Memo6: TfrxMemoView
          Top = 56.692950000000000000
          Width = 699.213050000000000000
          Height = 22.677165350000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            'Quantidade de Insidentes Finalizados no Per'#237'odo [PERIODO]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Shape1: TfrxShapeView
          Width = 699.213050000000000000
          Height = 56.692950000000000000
          ShowHint = False
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo13: TfrxMemoView
          Left = 154.960730000000000000
          Width = 393.071120000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsEndereco."NOME_ENT"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line1: TfrxLineView
          Left = 551.811380000000000000
          Top = 18.897650000000000000
          Width = 147.401670000000000000
          ShowHint = False
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo14: TfrxMemoView
          Left = 154.960730000000000000
          Top = 18.897650000000000000
          Width = 393.071120000000000000
          Height = 37.795275590000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsEndereco."E_ALL"]')
          ParentFont = False
        end
        object Memo15: TfrxMemoView
          Left = 548.031850000000000000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[Date]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo16: TfrxMemoView
          Left = 548.031850000000000000
          Top = 18.897650000000000000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Picture1: TfrxPictureView
          Left = 2.779530000000000000
          Top = 3.779530000000000000
          Width = 147.401574800000000000
          Height = 49.133890000000000000
          ShowHint = False
          HightQuality = False
          Transparent = False
          TransparentColor = clWhite
        end
        object frxDsEnderecoTE1: TfrxMemoView
          Left = 548.031850000000000000
          Top = 37.795300000000000000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsEndereco."TE1_TXT"]')
          ParentFont = False
        end
      end
    end
    object Page2: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Columns = 3
      ColumnWidth = 63.333333333333320000
      ColumnPositions.Strings = (
        '0'
        '63,3333333333333'
        '126,666666666667')
      PrintOnPreviousPage = True
      object MasterData1: TfrxMasterData
        Height = 15.118120000000000000
        Top = 98.267780000000000000
        Width = 239.370233333333300000
        DataSet = frxDs5Its
        DataSetName = 'frxDs5Its'
        RowCount = 0
        object Memo20: TfrxMemoView
          Width = 98.267750710000000000
          Height = 15.118120000000000000
          ShowHint = False
          DataField = 'Encerr_TXT'
          DataSet = frxDs5Its
          DataSetName = 'frxDs5Its'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDs5Its."Encerr_TXT"]')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          Left = 98.267780000000000000
          Width = 64.251980710000000000
          Height = 15.118120000000000000
          ShowHint = False
          DataField = 'Codigo'
          DataSet = frxDs5Its
          DataSetName = 'frxDs5Its'
          DisplayFormat.FormatStr = '#,###0'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDs5Its."Codigo"]')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          Left = 162.519790000000000000
          Width = 64.251980710000000000
          Height = 15.118120000000000000
          ShowHint = False
          DataField = 'PORCENT'
          DataSet = frxDs5Its
          DataSetName = 'frxDs5Its'
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDs5Its."PORCENT"]')
          ParentFont = False
        end
      end
      object GroupHeader1: TfrxGroupHeader
        Height = 34.015770000000000000
        Top = 41.574830000000000000
        Width = 239.370233333333300000
        Condition = 'frxDs5Its."MES"'
        object Memo1: TfrxMemoView
          Width = 226.771575430000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            'Per'#237'odo [frxDs5Its."MES"]')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          Top = 18.897650000000000000
          Width = 98.267750710000000000
          Height = 15.118120000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Data')
          ParentFont = False
        end
        object Memo40: TfrxMemoView
          Left = 98.267689690000000000
          Top = 18.897650000000000000
          Width = 64.251968500000000000
          Height = 15.118120000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Quantidade')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          Left = 162.519790000000000000
          Top = 18.897650000000000000
          Width = 64.251968500000000000
          Height = 15.118120000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '%')
          ParentFont = False
        end
      end
      object GroupFooter1: TfrxGroupFooter
        Height = 15.118120000000000000
        Top = 136.063080000000000000
        Width = 239.370233333333300000
        object Memo11: TfrxMemoView
          Width = 98.267716540000000000
          Height = 15.118120000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftTop]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Total')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          Left = 98.267780000000000000
          Width = 64.251946540000000000
          Height = 15.118120000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '#,###0'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftTop]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDs5Its."Codigo">)]')
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          Left = 162.519790000000000000
          Width = 64.251946540000000000
          Height = 15.118120000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftTop]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDs5Its."PORCENT">)]')
          ParentFont = False
        end
      end
      object Header1: TfrxHeader
        Top = 18.897650000000000000
        Width = 239.370233333333300000
      end
      object PageFooter1: TfrxPageFooter
        Height = 13.228346460000000000
        Top = 249.448980000000000000
        Width = 718.110700000000000000
        object Memo12: TfrxMemoView
          Width = 370.393940000000000000
          Height = 13.228346460000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - software customizado')
          ParentFont = False
          VAlign = vaCenter
        end
        object MeVARF_CODI_FRX: TfrxMemoView
          Left = 370.393940000000000000
          Width = 347.716760000000000000
          Height = 13.228346460000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'VARF_CODI_FRX')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object Footer1: TfrxFooter
        Height = 15.118120000000000000
        Top = 173.858380000000000000
        Width = 239.370233333333300000
        object Memo9: TfrxMemoView
          Width = 98.267716540000000000
          Height = 15.118120000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftTop]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Total')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          Left = 98.267780000000000000
          Width = 64.251946540000000000
          Height = 15.118120000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '#,###0'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftTop]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDs5Tot."Codigo"]')
          ParentFont = False
        end
        object Memo17: TfrxMemoView
          Left = 162.519790000000000000
          Width = 64.251946540000000000
          Height = 15.118120000000000000
          ShowHint = False
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftTop]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDs5Its."PORCENT">)]')
          ParentFont = False
        end
      end
    end
  end
  object frxDs5Its: TfrxDBDataset
    UserName = 'frxDs5Its'
    CloseDataSource = False
    FieldAliases.Strings = (
      'MES=MES'
      'Codigo=Codigo'
      'PORCENT=PORCENT'
      'Encerr_TXT=Encerr_TXT')
    DataSet = Qr5Its
    BCDToCurrency = False
    Left = 72
    Top = 92
  end
  object frxDs5Gra: TfrxDBDataset
    UserName = 'frxDs5Gra'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Codigo=Codigo'
      'PORCENT=PORCENT'
      'Encerr=Encerr')
    DataSet = Qr5Gra
    BCDToCurrency = False
    Left = 72
    Top = 64
  end
  object Qr5Tot: TmySQLQuery
    Database = Dmod.MyDBn
    SQL.Strings = (
      'SELECT COUNT(Codigo) Codigo'
      'FROM wordser'
      'WHERE Encerr BETWEEN :P0 AND :P1')
    Left = 100
    Top = 120
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object Qr5TotCodigo: TLargeintField
      FieldName = 'Codigo'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
  end
  object Ds5Tot: TDataSource
    DataSet = Qr5Tot
    Left = 128
    Top = 120
  end
  object frxDs5Tot: TfrxDBDataset
    UserName = 'frxDs5Tot'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Codigo=Codigo')
    DataSet = Qr5Tot
    BCDToCurrency = False
    Left = 72
    Top = 120
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 324
    Top = 128
  end
end
