unit WOrdSerSta;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2,
  ResIntStrings, UnMLAGeral, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, dmkGeral,
  dmkPermissoes, dmkEdit, dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup,
  unDmkProcFunc, dmkCheckBox, DmkDAC_PF, dmkCheckGroup, dmkDBLookupComboBox,
  dmkEditCB, Variants, UnDmkEnums, UnProjGroup_Consts;

type
  TFmWOrdSerSta = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    BtExclui: TBitBtn;
    BtAltera: TBitBtn;
    BtInclui: TBitBtn;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    Label9: TLabel;
    EdNome: TdmkEdit;
    Label1: TLabel;
    DBEdCodigo: TdmkDBEdit;
    Label2: TLabel;
    DBEdNome: TdmkDBEdit;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    QrWOrdSerSta: TmySQLQuery;
    QrWOrdSerStaCodigo: TIntegerField;
    QrWOrdSerStaNome: TWideStringField;
    DsWOrdSerSta: TDataSource;
    QrWOrdSerStaAtivo: TSmallintField;
    QrLoc: TmySQLQuery;
    ImgWEB: TdmkImage;
    DBCGNivelArq: TdmkDBCheckGroup;
    DBMemo1: TDBMemo;
    Label3: TLabel;
    CGNiveis: TdmkCheckGroup;
    LaWOrdSerEnc: TLabel;
    EdWOrdSerEnc: TdmkEditCB;
    CBWOrdSerEnc: TdmkDBLookupComboBox;
    SBWOrdSerEnc: TSpeedButton;
    DBMeWOrdSerEnc: TDBMemo;
    CbAtivo: TdmkCheckBox;
    CkStatEncerr: TdmkCheckBox;
    DsWOrdSerEnc: TDataSource;
    QrWOrdSerEnc: TmySQLQuery;
    QrWOrdSerEncCodigo: TIntegerField;
    QrWOrdSerEncNome: TWideStringField;
    QrWOrdSerEncDescri: TWideMemoField;
    QrWOrdSerStaNivel: TIntegerField;
    QrWOrdSerStaWOrdSerEnc: TIntegerField;
    QrWOrdSerStaWOrdSerEnc_TXT: TWideMemoField;
    DBCheckBox1: TDBCheckBox;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtIncluiClick(Sender: TObject);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure QrWOrdSerStaAfterOpen(DataSet: TDataSet);
    procedure QrWOrdSerStaBeforeOpen(DataSet: TDataSet);
    procedure SbImprimeClick(Sender: TObject);
    procedure BtExcluiClick(Sender: TObject);
    procedure SBWOrdSerEncClick(Sender: TObject);
    procedure CkStatEncerrClick(Sender: TObject);
    procedure QrWOrdSerStaAfterScroll(DataSet: TDataSet);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    //procedure IncluiRegistro;
    //procedure AlteraRegistro;
   ////Procedures do form
    procedure MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo: Integer);
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    procedure ReopenWOrdSerEnc();
    procedure ConfiguraEncerramento(Encerramento: Boolean);
  public
    { Public declarations }
    procedure LocCod(Atual, Codigo: Integer);
  end;

var
  FmWOrdSerSta: TFmWOrdSerSta;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, UnDmkWeb, ModWOrdSer, MyListas, UnGrlUsuarios;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmWOrdSerSta.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmWOrdSerSta.MostraEdicao(Mostra: Integer; SQLType: TSQLType;
  Codigo: Integer);
begin

end;

procedure TFmWOrdSerSta.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrWOrdSerStaCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmWOrdSerSta.DefParams;
begin
  VAR_GOTOTABELA := 'wordsersta';
  VAR_GOTOMYSQLTABLE := QrWOrdSerSta;
  VAR_GOTONEG := gotoPos;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDBn;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT sta.*, enc.Descri WOrdSerEnc_TXT');
  VAR_SQLx.Add('FROM wordsersta sta');
  VAR_SQLx.Add('LEFT JOIN wordserenc enc ON enc.Codigo = sta.WOrdSerEnc');
  VAR_SQLx.Add('WHERE sta.Codigo > 0');
  //
  VAR_SQL1.Add('AND sta.Codigo=:P0');
  //
  //VAR_SQL2.Add('AND sta.CodUsu=:P0');
  //
  VAR_SQLa.Add('AND sta.Nome Like :P0');
  //
end;

procedure TFmWOrdSerSta.CkStatEncerrClick(Sender: TObject);
begin
  ConfiguraEncerramento(CkStatEncerr.Checked);
end;

procedure TFmWOrdSerSta.ConfiguraEncerramento(Encerramento: Boolean);
begin
  LaWOrdSerEnc.Visible   := Encerramento;
  EdWOrdSerEnc.Visible   := Encerramento;
  CBWOrdSerEnc.Visible   := Encerramento;
  SBWOrdSerEnc.Visible   := Encerramento;
  DBMeWOrdSerEnc.Visible := Encerramento;
  //
  if not Encerramento then
  begin
    EdWOrdSerEnc.ValueVariant := 0;
    CBWOrdSerEnc.KeyValue     := Null;
  end;
end;

procedure TFmWOrdSerSta.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmWOrdSerSta.QueryPrincipalAfterOpen;
begin
end;

procedure TFmWOrdSerSta.ReopenWOrdSerEnc;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrWOrdSerEnc, Dmod.MyDBn, [
  'SELECT enc.Codigo, enc.Nome, enc.Descri ',
  'FROM wordserenc enc ',
  'WHERE enc.Ativo = 1 ',
  'ORDER BY enc.Codigo ',
  '']);
end;

procedure TFmWOrdSerSta.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmWOrdSerSta.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmWOrdSerSta.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmWOrdSerSta.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmWOrdSerSta.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmWOrdSerSta.SBWOrdSerEncClick(Sender: TObject);
begin
  DModWOrdSer.MostraWOrdSerSol(EdWOrdSerEnc.ValueVariant);
  if VAR_CADASTRO > 0 then
  begin
    ReopenWOrdSerEnc();
    //
    EdWOrdSerEnc.ValueVariant := VAR_CADASTRO;
    CBWOrdSerEnc.KeyValue     := VAR_CADASTRO;
    EdWOrdSerEnc.SetFocus;
  end;
end;

procedure TFmWOrdSerSta.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmWOrdSerSta.BtAlteraClick(Sender: TObject);
var
  StaEncer: Boolean;
begin
  if (QrWOrdSerSta.State <> dsInactive) and (QrWOrdSerSta.RecordCount > 0) and
    (QrWOrdSerStaCodigo.Value <> 0) then
  begin
    UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrWOrdSerSta, [PnDados],
      [PnEdita], EdNome, ImgTipo, 'wordsersta');
    //
    StaEncer             := QrWOrdSerStaWOrdSerEnc.Value > 0;
    CkStatEncerr.Checked := StaEncer;
    ConfiguraEncerramento(StaEncer);
  end;
end;

procedure TFmWOrdSerSta.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrWOrdSerStaCodigo.Value;
  Close;
end;

procedure TFmWOrdSerSta.BtConfirmaClick(Sender: TObject);
var
  Codigo: Integer;
  Nome: String;
begin
  Nome := EdNome.ValueVariant;
  //
  if MyObjects.FIC(Length(Nome) = 0, EdNome, 'Defina uma descri��o!') then Exit;
  //
  if ImgTipo.SQLType = stIns then
    Codigo := UMyMod.BuscaNovoCodigo_Int(Dmod.QrUpdN, 'wordsersta', 'Codigo',
      [], [], stIns, 0, siPositivo, nil)
  else
    Codigo := QrWOrdSerStaCodigo.Value;
  //
  if UMyMod.ExecSQLInsUpdPanel(ImgTipo.SQLType, Self, PnEdita,
    'wordsersta', Codigo, Dmod.QrUpdN, [PnEdita], [PnDados], ImgTipo, True) then
  begin
    LocCod(Codigo, Codigo);
  end;
end;

procedure TFmWOrdSerSta.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo          := EdCodigo.ValueVariant;
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  //
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDBn, 'wordsersta', 'Codigo');
  GOTOy.BotoesSb(ImgTipo.SQLType);  
end;

procedure TFmWOrdSerSta.BtExcluiClick(Sender: TObject);
var
  Codigo: Integer;
begin
  if (QrWOrdSerSta.State <> dsInactive) and (QrWOrdSerSta.RecordCount > 0) then
  begin
    Codigo := QrWOrdSerStaCodigo.Value;
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrLoc, Dmod.MyDBn, [
    'SELECT * ',
    'FROM wordser ',
    'WHERE Status=' + Geral.FF0(Codigo),
    '']);
    if QrLoc.RecordCount > 0 then
    begin
      Geral.MB_Aviso('Este status j� foi utilizado nas solicita��es e por isso n�o poder� ser exclu�-do!');
      Exit;
    end;
    UMyMod.ExcluiRegistroInt1('Confirma a exclus�o deste item?',
      'wordsersta', 'Codigo', Codigo, DMod.MyDBn);
    Va(vpLast);
  end;
end;

procedure TFmWOrdSerSta.BtIncluiClick(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrWOrdSerSta, [PnDados],
    [PnEdita], EdNome, ImgTipo, 'wordsersta');
  //
  CkStatEncerr.Checked := False;
  ConfiguraEncerramento(False);
end;

procedure TFmWOrdSerSta.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  GBEdita.Align   := alClient;
  GBDados.Align   := alClient;
  CriaOForm;
  //
  ReopenWOrdSerEnc;
  //
  DBCGNivelArq.Items.Clear;
  DBCGNivelArq.Items.AddStrings(GrlUsuarios.ConfiguraNiveis(CO_DMKID_APP));
  //
  CGNiveis.Items.Clear;
  CGNiveis.Items.AddStrings(GrlUsuarios.ConfiguraNiveis(CO_DMKID_APP));
end;

procedure TFmWOrdSerSta.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrWOrdSerStaCodigo.Value, LaRegistro.Caption);
end;

procedure TFmWOrdSerSta.SbImprimeClick(Sender: TObject);
begin
  Geral.MB_Info('Impress�o indispon�vel para esta janela!');
end;

procedure TFmWOrdSerSta.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmWOrdSerSta.SbNovoClick(Sender: TObject);
begin
  Geral.MB_Info('Tipo de pesquisa indispon�vel para esta janela!');
end;

procedure TFmWOrdSerSta.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmWOrdSerSta.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  DmkWeb.CarregaImagemFormWEB(ImgWEB);
end;

procedure TFmWOrdSerSta.SbQueryClick(Sender: TObject);
begin
  LocCod(QrWOrdSerStaCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'wordsersta', Dmod.MyDBn, CO_VAZIO));
end;

procedure TFmWOrdSerSta.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmWOrdSerSta.QrWOrdSerStaAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmWOrdSerSta.QrWOrdSerStaAfterScroll(DataSet: TDataSet);
var
  Visi: Boolean;
begin
  Visi := QrWOrdSerStaWOrdSerEnc.Value <> 0;
  //
  Label3.Visible  := Visi;
  DBMemo1.Visible := Visi;
end;

procedure TFmWOrdSerSta.QrWOrdSerStaBeforeOpen(DataSet: TDataSet);
begin
  QrWOrdSerStaCodigo.DisplayFormat := FFormatFloat;
end;

end.
