unit UnitWOrdSerJan;

interface

uses
  StdCtrls, ExtCtrls, Windows, Messages, SysUtils, Classes, Graphics, Controls,
  ComCtrls, dmkGeral, Forms, Dialogs, Menus, Db, DbCtrls, Variants, mySQLDbTables,
  UnDmkEnums, UnDmkProcFunc;

type
  TUnitWOrdSerJan = class(TObject)
  private
    { Private declarations }
  public
    { Public declarations }
    procedure MostraWOrdSer(AbrirEmAba: Boolean; InOwner: TWincontrol;
              Pager: TWinControl; Codigo: Integer = 0;
              CkGrupo: Boolean = True; CkTarefa: Boolean = True);
    procedure MostraFormWOrdSerTar(Codigo: Integer; Descri: String;
              Prioridade: Integer; DataCad: TDate);
  end;

var
  UnWOrdSerJan: TUnitWOrdSerJan;

implementation

uses Module, ModuleGeral, UnDmkWeb, DmkDAC_PF, MyDBCheck, UMySQLModule,
  UnMyObjects, WOrdSer, WOrdSerTar;

{ TUnitWOrdSerJan }

procedure TUnitWOrdSerJan.MostraWOrdSer(AbrirEmAba: Boolean; InOwner: TWincontrol;
  Pager: TWinControl; Codigo: Integer = 0; CkGrupo: Boolean = True;
  CkTarefa: Boolean = True);
var
  Frm: TForm;
begin
  if DmkWeb.ConexaoRemota(Dmod.MyDBn, DmodG.QrWebParams, 1) and
    (DmkWeb.ConectarUsuarioWEB(True)) then
  begin
    if AbrirEmAba then
    begin
      Frm := MyObjects.FormTDICria(TFmWOrdSer, InOwner, Pager, True, True);
      //
      if Frm <> nil then
      begin
        TFmWOrdSer(Frm).CkGrupo.Checked  := CkGrupo;
        TFmWOrdSer(Frm).CkTarefa.Checked := CkTarefa;
        //
        if Codigo <> 0 then
          TFmWOrdSer(Frm).ReopenWOrdSer(Codigo, 0, False);
      end;
    end else
    begin
      if DBCheck.CriaFm(TFmWOrdSer, FmWOrdSer, afmoNegarComAviso) then
      begin
        FmWOrdSer.CkGrupo.Checked  := CkGrupo;
        FmWOrdSer.CkTarefa.Checked := CkTarefa;
        //
        if Codigo <> 0 then
          FmWOrdSer.ReopenWOrdSer(Codigo, 0, False);
        //
        FmWOrdSer.ShowModal;
        FmWOrdSer.Destroy;
      end;
    end;
  end;
end;

procedure TUnitWOrdSerJan.MostraFormWOrdSerTar(Codigo: Integer; Descri: String;
  Prioridade: Integer; DataCad: TDate);
begin
  if DBCheck.CriaFm(TFmWOrdSerTar, FmWOrdSerTar, afmoNegarComAviso) then
  begin
    FmWOrdSerTar.FCodigo     := Codigo;
    FmWOrdSerTar.FDescri     := Descri;
    FmWOrdSerTar.FPrioridade := Prioridade;
    FmWOrdSerTar.FDataCad    := DataCad;
    FmWOrdSerTar.ShowModal;
    FmWOrdSerTar.Destroy;
  end;
end;

end.