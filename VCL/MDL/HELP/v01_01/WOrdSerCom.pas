unit WOrdSerCom;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, Vcl.ComCtrls, UnInternalConsts, dmkPermissoes, dmkDBLookupComboBox,
  dmkEditCB, dmkEdit, dmkMemo, UnDmkEnums;

type
  TFmWOrdSerCom = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Panel5: TPanel;
    Label12: TLabel;
    Label13: TLabel;
    SpeedButton3: TSpeedButton;
    EdCodigo: TdmkEdit;
    CkContinuar: TCheckBox;
    EdEntidade: TdmkEditCB;
    CBEntidade: TdmkDBLookupComboBox;
    CkComRes: TCheckBox;
    ImgWEB: TdmkImage;
    LaTamanho: TLabel;
    RETexto: TRichEdit;
    CkEnvMail: TCheckBox;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure RETextoChange(Sender: TObject);
  private
    { Private declarations }
    procedure MostraEdicao(Codigo: Integer; SQLType: TSQLType);
  public
    { Public declarations }
    FCodigo, FControle, FEntidade, FSolicitante, FComRes: Integer;
    FNome: String;
  end;

  var
  FmWOrdSerCom: TFmWOrdSerCom;

implementation

uses UnMyObjects, Module, ModWOrdSer, UnDmkWeb, UMySQLModule, WOrdSer,
  {$IfnDef SWUsr}UnWUsersJan, {$EndIf}
  UnGrl_Vars, ModuleGeral;

{$R *.DFM}

procedure TFmWOrdSerCom.BtOKClick(Sender: TObject);
var
  Codigo, Controle, Entidade, Para, Resul: Integer;
  DataHora, Descri, CadAltWeb, Mensagem, MensagemAviso: String;
  DataHoraUTC: TDateTime;
  ComRes, EnvMail: Boolean;
begin
  if not DmkWeb.ConectarUsuarioWEB(True) then Exit;
  //
  Codigo   := FCodigo;
  Entidade := EdEntidade.ValueVariant;
  Para     := FSolicitante;
  Descri   := RETexto.Text;
  ComRes   := CkComRes.Checked;
  EnvMail  := CkEnvMail.Checked;
  Mensagem := '';
  //
  if MyObjects.FIC(Entidade = 0, EdEntidade, 'O campo Comentado por deve ser preenchido!') then Exit;
  if MyObjects.FIC(Length(Descri) = 0, RETexto, 'Descri��o n�o definida!') then Exit;
  //
  if ImgTipo.SQLType = stIns then
    Controle := 0
  else
    Controle := FControle;
  //
  Resul := DmkWeb.WOrdSerIncluiCom(31, Codigo, Controle, Entidade, Para,
             ComRes, EnvMail, Descri, MensagemAviso);
  //
  if Resul > 0 then
  begin
    if Resul = 103 then
      Geral.MB_Aviso(MensagemAviso);
    //
    if CkContinuar.Checked then
    begin
      Geral.MB_Aviso('Dados salvos com sucesso!');
      MostraEdicao(0, stIns);
    end else
      Close;
  end else
    Geral.MB_Aviso('Falha ao incluir solicita��o tente novamente mais tarde!');
end;

procedure TFmWOrdSerCom.BtSaidaClick(Sender: TObject);
begin
  VAR_COD := EdCodigo.ValueVariant;
  Close;
end;

procedure TFmWOrdSerCom.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  DmkWeb.CarregaImagemFormWEB(ImgWEB);
  if ImgTipo.SQLType = stIns then
    MostraEdicao(0, stIns)
  else
    MostraEdicao(0, stUpd);
end;

procedure TFmWOrdSerCom.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  DModWOrdSer.ReopenWUsers(Geral.IMV(VAR_LIB_EMPRESAS));
  //
  {$IfnDef SWUsr}
  SpeedButton3.Visible := True;
  {$Else}
  SpeedButton3.Visible := False;
  {$EndIf}
end;

procedure TFmWOrdSerCom.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmWOrdSerCom.MostraEdicao(Codigo: Integer; SQLType: TSQLType);
begin
  if SQLType = stIns then
  begin
    EdCodigo.ValueVariant   := FormatFloat('000', Codigo);
    EdEntidade.ValueVariant := VAR_WEB_USR_ID;
    CBEntidade.KeyValue     := VAR_WEB_USR_ID;
    RETexto.Text            := '';
    CkComRes.Checked        := False;
    CkEnvMail.Checked       := False;
    CkEnvMail.Visible       := True;
    CkContinuar.Checked     := False;
    CkContinuar.Visible     := True;
  end else begin
    EdCodigo.ValueVariant   := FControle;
    EdEntidade.ValueVariant := FEntidade;
    CBEntidade.KeyValue     := FEntidade;
    RETexto.Text            := FNome;
    CkComRes.Checked        := Geral.IntToBool(FComRes);
    CkEnvMail.Checked       := False;
    CkEnvMail.Visible       := False;
    CkContinuar.Checked     := False;
    CkContinuar.Visible     := False;
  end;
  if VAR_WEB_USR_TIPO = 1 then
  begin
    EdEntidade.Enabled := False;
    CBEntidade.Enabled := False;
  end else
  begin
    EdEntidade.Enabled := True;
    CBEntidade.Enabled := True;
  end;
  RETexto.SetFocus;
end;

procedure TFmWOrdSerCom.RETextoChange(Sender: TObject);
const
  TamMax = 255;
var
  Tam, Posi: Integer;
begin
  Tam               := Length(RETexto.Text);
  Posi              := TamMax - Tam;
  LaTamanho.Caption := Geral.FF0(Posi);
  //
  if Posi < 0 then
  begin
    BtOK.Enabled                := False;
    LaTamanho.Font.Color        := clRed;
    RETexto.SelAttributes.Color := clRed;
  end else
  begin
    if Posi = 0 then
    begin
      RETexto.SelAttributes.Color := clRed;
    end else
      RETexto.SelAttributes.Color := clWindowText;
    //
    BtOK.Enabled         := True;
    LaTamanho.Font.Color := clWindowText;
  end;
end;

procedure TFmWOrdSerCom.SpeedButton3Click(Sender: TObject);
begin
  {$IfnDef SWUsr}
  UWUsersJan.MostraWUsers(EdEntidade.ValueVariant);
  //
  if VAR_CADASTRO > 0 then
  begin
    DModWOrdSer.ReopenWUsers(Geral.IMV(VAR_LIB_EMPRESAS));
    //
    EdEntidade.ValueVariant := VAR_CADASTRO;
    CBEntidade.KeyValue     := VAR_CADASTRO;
    EdEntidade.SetFocus;
  end;
  {$EndIf}
end;

end.
