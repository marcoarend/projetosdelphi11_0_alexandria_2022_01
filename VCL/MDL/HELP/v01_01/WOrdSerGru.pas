unit WOrdSerGru;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkDBGrid, mySQLDbTables,
  Vcl.Mask, dmkDBEdit, dmkEdit, dmkPermissoes, dmkCompoStore, Vcl.OleCtrls,
  SHDocVw, Vcl.DBCGrids, Vcl.Menus, System.Variants, UnProjGroup_Consts;

type
  THackDBGrid = class(TDBGrid);
  TTabTipo = (istWOS, istTAR);
  TFmWOrdSerGru = class(TForm)
    PnCabeca: TPanel;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    LaTitulo1C: TLabel;
    QrWOrdSerGru: TmySQLQuery;
    QrWOrdSerGruCodigo: TIntegerField;
    QrWOrdSerGruNome: TWideStringField;
    DsWOrdSerGru: TDataSource;
    BtDesconectar: TBitBtn;
    QrWOrdSer: TmySQLQuery;
    QrWOrdSerCodigo: TIntegerField;
    QrWOrdSerNOMEPRI: TWideStringField;
    QrWOrdSerNOMERESP: TWideStringField;
    QrWOrdSerNOMEAPLIC: TWideStringField;
    QrWOrdSerAssuntoTXT: TWideStringField;
    QrWOrdSerABERTURA_TXT: TWideStringField;
    DsWOrdSer: TDataSource;
    QrWOrdSerGruIts: TmySQLQuery;
    QrWOrdSerGruItsCodigo: TIntegerField;
    QrWOrdSerGruItsNOMERESP: TWideStringField;
    QrWOrdSerGruItsDescri: TWideMemoField;
    DsWOrdSerGruIts: TDataSource;
    QrWOrdSerGrupo: TIntegerField;
    QrWOrdSerNOMESTA: TWideStringField;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    ImgWEB: TdmkImage;
    CSTabSheetChamou: TdmkCompoStore;
    dmkPermissoes1: TdmkPermissoes;
    QrWOrdSerDescri: TWideMemoField;
    QrWOrdSerGruIniData: TDateField;
    QrWOrdSerGruFimData: TDateField;
    QrWOrdSerGruObserv: TWideMemoField;
    QrWOrdSerGruRespons: TIntegerField;
    QrWOrdSerGruRespons_TXT: TWideStringField;
    QrWOrdSerGruIniData_TXT: TWideStringField;
    QrWOrdSerGruFimData_TXT: TWideStringField;
    QrWOrdSerStatus: TIntegerField;
    BtWOrdSerGru: TBitBtn;
    QrWOrdSerGruItsNOMEPRIOR: TWideStringField;
    PMMenu: TPopupMenu;
    Alterastatus1: TMenuItem;
    Alteraprioridade1: TMenuItem;
    Alteraresponsvel1: TMenuItem;
    Alteradurao1: TMenuItem;
    QrWOrdSerGruItsDuracaoApr: TFloatField;
    QrWOrdSerAplicativo: TIntegerField;
    QrWOrdSerSolicit: TIntegerField;
    QrWOrdSerCliente: TIntegerField;
    QrWOrdSerAssunto: TIntegerField;
    GroupBox1: TGroupBox;
    Splitter1: TSplitter;
    Panel10: TPanel;
    Splitter2: TSplitter;
    Panel12: TPanel;
    BtIncluiGrupo: TBitBtn;
    BtAlteraGrupo: TBitBtn;
    BtExcluiGrupo: TBitBtn;
    BrReordena: TBitBtn;
    DBGGrupo: TDBGrid;
    Panel6: TPanel;
    Label4: TLabel;
    Panel7: TPanel;
    Label3: TLabel;
    Label1: TLabel;
    Label8: TLabel;
    Label2: TLabel;
    dmkDBEdit3: TdmkDBEdit;
    DBEdCodigo: TdmkDBEdit;
    dmkDBEdit2: TdmkDBEdit;
    dmkDBEdit1: TdmkDBEdit;
    DBMemo1: TDBMemo;
    PnGrupo: TPanel;
    PCGrupo: TPageControl;
    TSChamados: TTabSheet;
    LaOrderByGrupo: TLabel;
    Splitter4: TSplitter;
    Panel19: TPanel;
    DBMemo5: TDBMemo;
    StaticText8: TStaticText;
    DBWOSGruIts: TdmkDBGrid;
    TSGrade: TTabSheet;
    PnGrade: TPanel;
    PnGrupoRight: TPanel;
    StaticText5: TStaticText;
    Panel14: TPanel;
    Label22: TLabel;
    Label23: TLabel;
    Label5: TLabel;
    Label7: TLabel;
    dmkDBEdit4: TdmkDBEdit;
    dmkDBEdit5: TdmkDBEdit;
    dmkDBEdit6: TdmkDBEdit;
    dmkDBEdit7: TdmkDBEdit;
    DBMemo3: TDBMemo;
    SGWOrdSerGru: TStringGrid;
    Panel9: TPanel;
    LaCodigo: TLabel;
    EdCodigo: TdmkEdit;
    BtReabre: TBitBtn;
    RGMostraGrupoIts: TRadioGroup;
    LaTotal0: TLabel;
    LaTotal2: TLabel;
    EdDuracao: TdmkEdit;
    Label6: TLabel;
    QrWOrdSerDuracaoApr: TFloatField;
    Inclui1: TMenuItem;
    N1: TMenuItem;
    QrWOrdSerAssuntoGru: TWideStringField;
    QrWOrdSerGruSTATUS12: TWideStringField;
    QrWOrdSerGruSTATUS22: TWideStringField;
    QrWOrdSerGruSTATUS32: TWideStringField;
    QrWOrdSerGruSTATUS42: TWideStringField;
    QrWOrdSerGruGruStatus4: TIntegerField;
    QrWOrdSerGruGruStatus3: TIntegerField;
    QrWOrdSerGruGruStatus2: TIntegerField;
    QrWOrdSerGruGruStatus1: TIntegerField;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtIncluiGrupoClick(Sender: TObject);
    procedure BtAlteraGrupoClick(Sender: TObject);
    procedure DBGGrupoDragDrop(Sender, Source: TObject; X, Y: Integer);
    procedure DBGGrupoDragOver(Sender, Source: TObject; X, Y: Integer;
      State: TDragState; var Accept: Boolean);
    procedure BtExcluiGrupoClick(Sender: TObject);
    procedure BrReordenaClick(Sender: TObject);
    procedure QrWOrdSerGruAfterScroll(DataSet: TDataSet);
    procedure QrWOrdSerGruBeforeClose(DataSet: TDataSet);
    procedure RGMostraGrupoItsClick(Sender: TObject);
    procedure DBWOSGruItsMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure SGWOrdSerGruDragDrop(Sender, Source: TObject; X, Y: Integer);
    procedure SGWOrdSerGruDragOver(Sender, Source: TObject; X, Y: Integer;
      State: TDragState; var Accept: Boolean);
    procedure SGWOrdSerGruDrawCell(Sender: TObject; ACol, ARow: Integer;
      Rect: TRect; State: TGridDrawState);
    procedure SGWOrdSerGruMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure SGWOrdSerGruSelectCell(Sender: TObject; ACol, ARow: Integer;
      var CanSelect: Boolean);
    procedure FormShow(Sender: TObject);
    procedure BtDesconectarClick(Sender: TObject);
    procedure PCGrupoChange(Sender: TObject);
    procedure QrWOrdSerAfterScroll(DataSet: TDataSet);
    procedure QrWOrdSerBeforeClose(DataSet: TDataSet);
    procedure BtWOrdSerGruClick(Sender: TObject);
    procedure BtReabreClick(Sender: TObject);
    procedure PMMenuPopup(Sender: TObject);
    procedure Alterastatus1Click(Sender: TObject);
    procedure Alteraprioridade1Click(Sender: TObject);
    procedure Alteraresponsvel1Click(Sender: TObject);
    procedure Alteradurao1Click(Sender: TObject);
    procedure Inclui1Click(Sender: TObject);
  private
    { Private declarations }
    FSourceCol, FSourceRow: Integer;
    procedure ConfiguraMenuGrupo(Habilita: Boolean; Grupo: Integer);
    procedure ReopenWOrdSer(Codigo: Integer);
    procedure ReopenWOrdSerGruIts(Codigo: Integer);
    procedure FechaJanela(Desconecta: Boolean);
    procedure ReopenWOrdSerGru(Query: TmySQLQuery; Codigo: Integer);
    procedure MostraWOrdSerGruIts(SQLType: TSQLType);
    procedure AtualizaDuracaoGrupo(Grupo: Integer);
  public
    { Public declarations }
  end;

  var
    FmWOrdSerGru: TFmWOrdSerGru;

implementation

uses UnMyObjects, Module, ModuleGeral, UnDmkWeb, UMySQLModule, ModWOrdSer,
  UnDmkImg, UnReordena, DmkDAC_PF, Principal, MyGlyfs, UnDmkProcFunc, MyDBCheck,
  WOrdSerGruIts, WOrdSer, MyListas, UnGrl_Vars, UnitWOrdSerJan;

{$R *.DFM}

procedure TFmWOrdSerGru.FechaJanela(Desconecta: Boolean);
begin
  if Desconecta then
    DmkWeb.DesconectarUsuarioWEB;
  //
  if TFmWOrdSerGru(Self).Owner is TApplication then
    Close
  else
  begin
    MyObjects.FormTDIFecha(Self, TTabSheet(CSTabSheetChamou.Component));
  end;
end;

procedure TFmWOrdSerGru.Alteradurao1Click(Sender: TObject);
var
  Codigo, Grupo: Integer;
  Duracao: Double;
  DuracaoTxt: String;
begin
  if not DmkWeb.ConectarUsuarioWEB(True) then Exit;
  //
  DuracaoTxt := FloatToStr(QrWOrdSerDuracaoApr.Value);
  //
  if not InputQuery('Dura��o (aproximada em horas)',
    'Dura��o (aproximada em horas):', DuracaoTxt)
  then
    Exit;
  //
  Grupo   := QrWOrdSerGruCodigo.Value;
  Duracao := Geral.DMV(DuracaoTxt);
  //
  Codigo := QrWOrdSerCodigo.Value;
  //
  if Codigo <> 0 then
  begin
    if UMyMod.SQLInsUpd(Dmod.QrUpdN, stUpd, 'wordser', False, ['DuracaoApr'],
      ['Codigo'], [Duracao], [Codigo], False) then
    begin
      ReopenWOrdSerGru(QrWOrdSerGru, Grupo);
    end;
  end;
end;

procedure TFmWOrdSerGru.Alteraprioridade1Click(Sender: TObject);
var
  Codigo: Integer;
begin
  if not DmkWeb.ConectarUsuarioWEB(True) then Exit;
  //
  if (QrWOrdSer.State <> dsInactive) and (QrWOrdSer.RecordCount > 0) then
  begin
    Codigo := QrWOrdSerCodigo.Value;
    //
    if DModWOrdSer.AlteraPrioridade(Codigo) then
      ReopenWOrdSer(Codigo);
  end;
end;

procedure TFmWOrdSerGru.Alteraresponsvel1Click(Sender: TObject);
var
  Codigo: Integer;
begin
  if not DmkWeb.ConectarUsuarioWEB(True) then Exit;
  //
  if(QrWOrdSer.State <> dsInactive) and (QrWOrdSer.RecordCount > 0) then
  begin
    Screen.Cursor := crHourGlass;
    try
      Codigo := QrWOrdSerCodigo.Value;
      //
      if DModWOrdSer.AlteraResponsavel(Codigo, QrWOrdSerAplicativo.Value,
        QrWOrdSerSolicit.Value, QrWOrdSerCliente.Value, QrWOrdSerAssunto.Value)
      then
        ReopenWOrdSer(Codigo);
    finally
      Screen.Cursor := crDefault;
    end;
  end;
end;

procedure TFmWOrdSerGru.Alterastatus1Click(Sender: TObject);
var
  Codigo: Integer;
begin
  if not DmkWeb.ConectarUsuarioWEB(True) then Exit;
  //
  if(QrWOrdSer.State <> dsInactive) and (QrWOrdSer.RecordCount > 0) then
  begin
    Codigo := QrWOrdSerCodigo.Value;
    //
    if DModWOrdSer.AlteraStatus(QrWOrdSerCodigo.Value, nil, nil) then
      ReopenWOrdSer(Codigo);
  end;
end;

procedure TFmWOrdSerGru.AtualizaDuracaoGrupo(Grupo: Integer);
var
  Qry: TmySQLQuery;
  SQL: String;
  Duracao: Double;
begin
  Duracao := 0;
  //
  Qry := TmySQLQuery.Create(TDataModule(Dmod.MyDBn.Owner));
  try
    if RGMostraGrupoIts.ItemIndex = 0 then
      SQL := 'AND Respons=' + Geral.FF0(VAR_WEB_USR_ID)
    else
      SQL := '';
    //
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDBn, [
      'SELECT SUM(DuracaoApr) Duracao ',
      'FROM wordser ',
      'WHERE Grupo=' + Geral.FF0(Grupo),
      SQL,
      '']);
    if Qry.RecordCount > 0 then
      Duracao := Duracao + Qry.FieldByName('Duracao').AsFloat;
  finally
    Qry.Free;
  end;
  EdDuracao.ValueVariant := Duracao;
end;

procedure TFmWOrdSerGru.BrReordenaClick(Sender: TObject);
var
  Codigo: Integer;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(DModWOrdSer.QrLoc, Dmod.MyDBn, [
    'SELECT * ',
    'FROM wordsergru ',
    'WHERE Codigo > 0 ',
    'ORDER BY Ordem ',
    '']);
  if DModWOrdSer.QrLoc.RecordCount > 0 then
  begin
    UReordena.ReordenaItens(DModWOrdSer.QrLoc, Dmod.QrUpdN, 'wordsergru',
      'Ordem', 'Codigo', 'Nome', '', '', '', nil);
    //
    Codigo := QrWOrdSerGruCodigo.Value;
    ReopenWOrdSerGru(QrWOrdSerGru, Codigo);
  end;
end;

procedure TFmWOrdSerGru.BtAlteraGrupoClick(Sender: TObject);
begin
  MostraWOrdSerGruIts(stUpd);
end;

procedure TFmWOrdSerGru.BtDesconectarClick(Sender: TObject);
begin
  FechaJanela(True);
end;

procedure TFmWOrdSerGru.BtExcluiGrupoClick(Sender: TObject);
var
  Codigo: Integer;
begin
  if (QrWOrdSerGru.State <> dsInactive) and (QrWOrdSerGru.RecordCount > 0) and
    (QrWOrdSerGruCodigo.Value > 0) then
  begin
    Codigo := QrWOrdSerGruCodigo.Value;
    //
    UnDmkDAC_PF.AbreMySQLQuery0(DModWOrdSer.QrLoc, Dmod.MyDBn, [
    'SELECT * ',
    'FROM wordser ',
    'WHERE Grupo=' + Geral.FF0(Codigo),
    '']);
    if DModWOrdSer.QrLoc.RecordCount > 0 then
    begin
      Geral.MB_Aviso('Este grupo possui itens e por isso n�o poder� ser exclu�-do!');
      Exit;
    end;
    if UMyMod.ExcluiRegistroInt1('Confirma a exclus�o deste item?',
      'wordsergru', 'Codigo', Codigo, DMod.MyDBn) = ID_YES then
    begin
      ReopenWOrdSerGru(QrWOrdSerGru, 0);
      ReopenWOrdSer(0);
    end;
  end;
end;

procedure TFmWOrdSerGru.BtIncluiGrupoClick(Sender: TObject);
begin
  MostraWOrdSerGruIts(stIns);
end;

procedure TFmWOrdSerGru.BtReabreClick(Sender: TObject);
var
  Achou: Boolean;
  Codigo, Grupo: Integer;
begin
  Codigo := EdCodigo.ValueVariant;
  //
  if Codigo <> 0 then
  begin
    Achou := False;
    //
    UnDmkDAC_PF.AbreMySQLQuery0(DModWOrdSer.QrLoc, Dmod.MyDBn, [
      'SELECT Codigo, Grupo ',
      'FROM wordser ',
      'WHERE Codigo=' + Geral.FF0(Codigo),
      '']);
    //
    if DModWOrdSer.QrLoc.RecordCount > 0 then
    begin
      Grupo := DModWOrdSer.QrLoc.FieldByName('Grupo').AsInteger;
      //
      Achou := QrWOrdSerGru.Locate('Codigo', Grupo, []);
      //
      if Achou then
        Achou := QrWOrdSer.Locate('Codigo', Codigo, []);
    end;
    if not Achou then
      Geral.MB_Aviso('ID n�o localizado!' + sLineBreak +
        'Prov�vel motivo: ID j� finalizado!');
  end else
    ReopenWOrdSer(0);
end;

procedure TFmWOrdSerGru.BtSaidaClick(Sender: TObject);
begin
  FechaJanela(False);
end;

procedure TFmWOrdSerGru.BtWOrdSerGruClick(Sender: TObject);
var
  Codigo: Integer;
begin
  case PCGrupo.ActivePageIndex of
    0:
    begin
      if (QrWOrdSer.State <> dsInactive) and (QrWOrdSer.RecordCount > 0) then
        Codigo := QrWOrdSerCodigo.Value
      else
        Codigo := 0;
    end;
    else
    begin
      if (QrWOrdSerGruIts.State <> dsInactive) and (QrWOrdSerGruIts.RecordCount > 0) then
        Codigo := QrWOrdSerGruItsCodigo.Value
      else
        Codigo := 0;
    end;
  end;
  UnWOrdSerJan.MostraWOrdSer(True, FmPrincipal.PageControl1,
    FmPrincipal.AdvToolBarPagerNovo, Codigo, False, False);
end;

procedure TFmWOrdSerGru.DBGGrupoDragDrop(Sender, Source: TObject; X,
  Y: Integer);
var
  Coordenadas: TGridCoord;
  i, Grupo, GrupoAtual: Integer;
  WOrdSer: String;
begin
  Coordenadas := THackDBGrid(DBGGrupo).MouseCoord(X, Y);
  //
  if (Coordenadas.X > 0) and (Coordenadas.Y > 0) then
  begin
    with THackDBGrid(DBGGrupo) do
    begin
      WOrdSer := '';
      //
      GrupoAtual := QrWOrdSerGrupo.Value;
      //
      if DBWOSGruIts.SelectedRows.Count > 1 then
      begin
        with DBWOSGruIts.DataSource.DataSet do
        for i:= 0 to DBWOSGruIts.SelectedRows.Count - 1 do
        begin
          GotoBookmark(DBWOSGruIts.SelectedRows.Items[i]);
          //
          if i = DBWOSGruIts.SelectedRows.Count - 1 then
            WOrdSer := WOrdSer + Geral.FF0(QrWOrdSerCodigo.Value)
          else
            WOrdSer := WOrdSer + Geral.FF0(QrWOrdSerCodigo.Value) + ',';
        end;
      end else
        WOrdSer := Geral.FF0(QrWOrdSerCodigo.Value);
      //
      DataSource.DataSet.MoveBy(Coordenadas.Y - Row);
      //
      Grupo := QrWOrdSerGruCodigo.Value;
      //
      if (Grupo <> GrupoAtual) and (Grupo <> -1) then
      begin
        Screen.Cursor := crHourGlass;
        try
          DBGGrupo.Enabled    := False;
          DBWOSGruIts.Enabled := False;
          //
          DModWOrdSer.AlteraGrupoStatus(WOrdSer, Grupo, GrupoAtual);
          //
          ReopenWOrdSer(0);
          QrWOrdSerGru.Locate('Codigo', GrupoAtual, []);
        finally
          DBGGrupo.Enabled    := True;
          DBWOSGruIts.Enabled := True;
          Screen.Cursor       := crDefault;
        end;
      end;
    end;
  end;
end;

procedure TFmWOrdSerGru.DBGGrupoDragOver(Sender, Source: TObject; X, Y: Integer;
  State: TDragState; var Accept: Boolean);
begin
  Accept := (Source is TdmkDBGrid) and (TdmkDBGrid(Source).Name = 'DBWOSGruIts');
end;

procedure TFmWOrdSerGru.DBWOSGruItsMouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
begin
  if QrWOrdSerGruCodigo.Value <> -1 then
  begin
    if (ssLeft in Shift) or ((ssLeft in Shift) and (ssShift in Shift)) or (ssShift in Shift) then
      DBWOSGruIts.BeginDrag(True, 4);
  end;
end;

procedure TFmWOrdSerGru.FormActivate(Sender: TObject);
begin
  if TFmWOrdSerGru(Self).Owner is TApplication then
  begin
    MyObjects.CorIniComponente();
  end;
  DmkWeb.CarregaImagemFormWEB(ImgWEB);
end;

procedure TFmWOrdSerGru.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  DModWOrdSer.ReopenWOrdSerOpc;
  ReopenWOrdSerGru(QrWOrdSerGru, 0);
  //
  DBWOSGruIts.PopupMenu := PMMenu;
end;

procedure TFmWOrdSerGru.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
   [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmWOrdSerGru.FormShow(Sender: TObject);
begin
{$IfNDef cSkinRank} //Berlin
{$IfNDef cAlphaSkin} //Berlin
  FmMyGlyfs.DefineGlyfs(TForm(Sender));
{$EndIf}
{$EndIf}
{$IfDef cSkinRank} //Berlin
  if FmPrincipal.Sd1.Active then
    FmMyGlyfs.DefineGlyfsTDI(FmPrincipal.sd1, Sender);
{$EndIf}
{$IfDef cAlphaSkin} //Berlin
  if FmPrincipal.sSkinManager1.Active then
    FmMyGlyfs.DefineGlyfsTDI2(FmPrincipal.sSkinManager1, Sender);
{$EndIf}
end;

procedure TFmWOrdSerGru.Inclui1Click(Sender: TObject);
var
  Status, Grupo: Integer;
begin
  Status := QrWOrdSerGru.FieldByName('GruStatus1').AsInteger;
  Grupo  := QrWOrdSerGruCodigo.Value;
  //
  DModWOrdSer.MostraWOrdSerIts(stIns, QrWOrdSer, True, Grupo, Status, True);
  //
  ReopenWOrdSer(0);
end;

procedure TFmWOrdSerGru.MostraWOrdSerGruIts(SQLType: TSQLType);
var
  Codigo: Integer;
begin
  if (DmkWeb.ConexaoRemota(Dmod.MyDBn, DmodG.QrWebParams, 1)) and
    (DmkWeb.ConectarUsuarioWEB(True)) then
  begin
    if SQLType = stIns then
      Codigo := 0
    else
      Codigo := QrWOrdSerGru.FieldByName('Codigo').AsInteger;
    //
    if DBCheck.CriaFm(TFmWOrdSerGruIts, FmWOrdSerGruIts, afmoNegarComAviso) then
    begin
      FmWOrdSerGruIts.ImgTipo.SQLType := SQLType;
      FmWOrdSerGruIts.FCodigo         := Codigo;
      FmWOrdSerGruIts.FQrWOrdSerGru   := QrWOrdSerGru;
      FmWOrdSerGruIts.ShowModal;
      //
      Codigo := FmWOrdSerGruIts.FCodigo;
      //
      FmWOrdSerGruIts.Destroy;
      //
      if Codigo <> 0 then
        ReopenWOrdSerGru(QrWOrdSerGru, Codigo);
    end;
  end;
end;

procedure TFmWOrdSerGru.PCGrupoChange(Sender: TObject);
var
  Visi: Boolean;
  Grupo, Codigo: Integer;
begin
  case PCGrupo.ActivePageIndex of
    0:
    begin
      if (QrWOrdSerGru.State <> dsInactive) and (QrWOrdSerGru.RecordCount > 0) then
        Grupo := QrWOrdSerGruCodigo.Value
      else
        Grupo := 0;
      //
      if (QrWOrdSer.State <> dsInactive) and (QrWOrdSer.RecordCount > 0) then
        Codigo := QrWOrdSerCodigo.Value
      else
        Codigo := 0;
      //
      ReopenWOrdSerGru(QrWOrdSerGru, Grupo);
      //
      if Codigo <> 0 then
      begin
        ReopenWOrdSer(Codigo);
      end;
    end else
      DModWOrdSer.MontaGradeGrupo(QrWOrdSerGruCodigo.Value, SGWOrdSerGru, QrWOrdSer, QrWOrdSerGru);
  end;
  Visi := PCGrupo.ActivePageIndex <> 2;
  //
  LaCodigo.Visible := Visi;
  EdCodigo.Visible := Visi;
  BtReabre.Visible := Visi;
end;

procedure TFmWOrdSerGru.PMMenuPopup(Sender: TObject);
var
  Enab, Enab2: Boolean;
begin
  Enab := (QrWOrdSerGru.State <> dsInactive) and (QrWOrdSerGru.RecordCount > 0);
  //
  Enab2 := (QrWOrdSer.State <> dsInactive) and (QrWOrdSer.RecordCount > 0);
  //
  Inclui1.Visible           := Enab;
  Alterastatus1.Visible     := Enab and Enab2;
  Alteraprioridade1.Visible := Enab and Enab2;
  Alteraresponsvel1.Visible := Enab and Enab2;
  Alteradurao1.Visible      := Enab and Enab2;
end;

procedure TFmWOrdSerGru.QrWOrdSerAfterScroll(DataSet: TDataSet);
begin
  LaTotal0.Caption := 'Total de itens: ' + Geral.FF0(QrWOrdSer.RecordCount);
end;

procedure TFmWOrdSerGru.QrWOrdSerBeforeClose(DataSet: TDataSet);
begin
  LaTotal0.Caption := '';
end;

procedure TFmWOrdSerGru.QrWOrdSerGruAfterScroll(DataSet: TDataSet);
var
  Codigo: Integer;
begin
  Codigo := QrWOrdSerGruCodigo.Value;
  //
  AtualizaDuracaoGrupo(Codigo);
  ConfiguraMenuGrupo(Codigo > 0, Codigo);
end;

procedure TFmWOrdSerGru.QrWOrdSerGruBeforeClose(DataSet: TDataSet);
begin
  QrWOrdSer.Close;
  //
  ConfiguraMenuGrupo(False, -99);
  //
  EdDuracao.ValueVariant := 0;
end;

procedure TFmWOrdSerGru.ReopenWOrdSer(Codigo: Integer);
var
  Resp, Finaliz: Integer;
  SQL, TimeZoneDifUTC, Ordem, WOrdSerGru_TXT: String;
begin
  SQL            := '';
  TimeZoneDifUTC := VAR_WEB_USR_TZDIFUTC;
  //
  if RGMostraGrupoIts.ItemIndex = 0 then
    Resp := VAR_WEB_USR_ID
  else
    Resp := 0;
  //
  Finaliz := 0;
  Ordem   := DModWOrdSer.QrWOrdSerOpc.FieldByName('Ordem_CamposFields').AsString;
  //
  case QrWOrdSerGruCodigo.Value of
     -1: WOrdSerGru_TXT := '';
      0: WOrdSerGru_TXT := 'AND wor.Grupo = 0';
    else WOrdSerGru_TXT := 'AND wor.Grupo = ' + Geral.FF0(QrWOrdSerGruCodigo.Value);
  end;
  //
  if TimeZoneDifUTC = '' then
  begin
    Geral.MB_Aviso('Time zone n�o definido!');
    QrWOrdSer.Close;
    Exit;
  end;
  QrWOrdSer.Close;
  QrWOrdSer.SQL.Clear;
  QrWOrdSer.SQL.Add('SELECT wor.*, sta.Nome NOMESTA, gru.Nome NOMEGRUPO, asg.Nome AssuntoGru, ');
  QrWOrdSer.SQL.Add('CONCAT(LPAD(pri.Ordem, 3, 0), " - ", pri.Nome) NOMEPRI, wmo.Nome NOMEMOD,');
  QrWOrdSer.SQL.Add('IF (ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMECLI,');
  QrWOrdSer.SQL.Add('wur.PersonalName NOMERESP, wus.PersonalName, wor.Aplicativo, ');
  QrWOrdSer.SQL.Add('enc.Descri NOMESOL, CONCAT(LPAD(ass.Ordem, 3, 0), " - ", ass.Nome) AssuntoTXT,');
  QrWOrdSer.SQL.Add('CONVERT_TZ(his.DataHora, "+00:00", "'+ TimeZoneDifUTC +'") DATAHORAHIS,');
  QrWOrdSer.SQL.Add('wor.Solicit, wor.Cliente, wor.Assunto, ');
  //
  if CO_DMKID_APP = 17 then //DControl
    QrWOrdSer.SQL.Add('apl.Nome NOMEAPLIC, ')
  else
    QrWOrdSer.SQL.Add('"" NOMEAPLIC, ');
  //
  QrWOrdSer.SQL.Add('CASE his.Tarefa');
  QrWOrdSer.SQL.Add('WHEN 0 THEN "Abertura"');
  QrWOrdSer.SQL.Add('WHEN 1 THEN "Encerramento"');
  QrWOrdSer.SQL.Add('WHEN 2 THEN "Comentado"');
  QrWOrdSer.SQL.Add('WHEN 3 THEN "Reabertura"');
  QrWOrdSer.SQL.Add('WHEN 4 THEN "Mudan�a de Status" END STATUS_TXT, ');
  QrWOrdSer.SQL.Add('IF (wor.Finaliz = 0, "O.S. em aberto", ');
  QrWOrdSer.SQL.Add('DATE_FORMAT(CONVERT_TZ(wor.Encerr, "+00:00", "'+ TimeZoneDifUTC +'"), "%d/%m/%Y %H:%i:%s")) ENCERROU_TXT,');
  QrWOrdSer.SQL.Add('DATE_FORMAT(CONVERT_TZ(wor.Abertura, "+00:00", "'+ TimeZoneDifUTC +'"), "%d/%m/%Y %H:%i:%s") ABERTURA_TXT');
  QrWOrdSer.SQL.Add('FROM wordser wor');
  QrWOrdSer.SQL.Add('LEFT JOIN entidades ent ON ent.Codigo = wor.Cliente');
  QrWOrdSer.SQL.Add('LEFT JOIN wordserass ass ON ass.Codigo = wor.Assunto');
  QrWOrdSer.SQL.Add('LEFT JOIN wordsersta sta ON sta.Codigo = wor.Status');
  QrWOrdSer.SQL.Add('LEFT JOIN wordserpri pri ON pri.Codigo = wor.Prioridade');
  QrWOrdSer.SQL.Add('LEFT JOIN wusers wur ON wur.Codigo = wor.Respons');
  QrWOrdSer.SQL.Add('LEFT JOIN wordsermod wmo ON wmo.Codigo = wor.Modo');
  QrWOrdSer.SQL.Add('LEFT JOIN wusers wus ON wus.Codigo = wor.Solicit');
  QrWOrdSer.SQL.Add('LEFT JOIN wordserenc enc ON enc.Codigo = wor.WOrdSerEnc');
  QrWOrdSer.SQL.Add('LEFT JOIN wosassgru asg ON asg.Codigo = ass.Grupo');
  //
  if CO_DMKID_APP = 17 then //DControl
    QrWOrdSer.SQL.Add('LEFT JOIN aplicativos apl ON apl.Codigo = wor.Aplicativo');
  //
  QrWOrdSer.SQL.Add('LEFT JOIN wordsergru gru ON gru.Codigo = wor.Grupo');
  QrWOrdSer.SQL.Add('LEFT JOIN wordserhis his ON his.Controle = wor.WOrdSerHis');
  QrWOrdSer.SQL.Add('WHERE wor.Finaliz = 0 ');
  //
  //Respons�vel
  if Resp > 0 then
  begin
    SQL := 'AND wor.Respons = ' + Geral.FF0(Resp);
    //
    QrWOrdSer.SQL.Add(SQL);
  end;
  //
  if WOrdSerGru_TXT <> '' then
  begin
    SQL := WOrdSerGru_TXT;
    //
    QrWOrdSer.SQL.Add(SQL);
  end;
  //
  if Finaliz in [0, 1] then
  begin
    SQL := 'AND wor.Finaliz = ' + Geral.FF0(Finaliz);
    //
    QrWOrdSer.SQL.Add(SQL);
  end;
  if Ordem = '' then
    Ordem := 'DATAHORAHIS DESC ';
  //
  QrWOrdSer.SQL.Add('ORDER BY ' + Ordem);
  QrWOrdSer.Open;
  //
  LaOrderByGrupo.Caption := 'Ordena��o da grade: ' +
                              DModWOrdSer.QrWOrdSerOpc.FieldByName('Ordem_CamposTxt').AsString;
  if Codigo > 0 then
  begin
    if not QrWOrdSer.Locate('Codigo', Codigo, []) then
      Geral.MB_Aviso('N�o foi poss�vel localizar o registro!');
  end;
end;

procedure TFmWOrdSerGru.RGMostraGrupoItsClick(Sender: TObject);
var
  Grupo: Integer;
begin
  if (QrWOrdSerGru.State <> dsInactive) and (QrWOrdSerGru.RecordCount > 0) then
  begin
    Grupo := QrWOrdSerGruCodigo.Value;
    //
    ReopenWOrdSerGru(QrWOrdSerGru, Grupo);
    //
    if Grupo > 0 then
    else
      ReopenWOrdSer(0);
  end;
end;

procedure TFmWOrdSerGru.SGWOrdSerGruDragDrop(Sender, Source: TObject; X,
  Y: Integer);

  function AtualizaStatus(Codigo, ColDest: Integer): Integer;
  var
    Status: Integer;
    MensagemAviso: String;
  begin
    if (QrWOrdSerGru.State <> dsInactive) and (QrWOrdSerGru.RecordCount > 0) then
    begin
      case ColDest of
          0: Status := QrWOrdSerGru.FieldByName('GruStatus1').AsInteger;
          1: Status := QrWOrdSerGru.FieldByName('GruStatus2').AsInteger;
          2: Status := QrWOrdSerGru.FieldByName('GruStatus3').AsInteger;
          3: Status := QrWOrdSerGru.FieldByName('GruStatus4').AsInteger;
        else Status := 0;
      end;
    end else
    begin
      Status := 0;
    end;
    if Status = 0 then
    begin
      Geral.MB_Aviso('Status n�o definido!');
      Result := 0;
    end else
      Result := DmkWeb.WOrdSerAtualizaStatus(31, Codigo, Status, MensagemAviso);
  end;

var
  Codigo, DestCol, DestRow: Integer;
begin
  SGWOrdSerGru.MouseToCell(X, Y, DestCol, DestRow);
  //
  Codigo := Geral.IMV(SGWOrdSerGru.Cells[FSourceCol, FSourceRow]);
  //
  if AtualizaStatus(Codigo, DestCol) > 0 then
  begin
    SGWOrdSerGru.Cells[DestCol, FSourceRow] := SGWOrdSerGru.Cells[FSourceCol, FSourceRow];
    //
    if (FSourceCol <> DestCol) or (FSourceRow <> DestRow) then
      SGWOrdSerGru.Cells[FSourceCol, FSourceRow] := '';
  end else
    Geral.MB_Aviso('Falha ao alterar o status da solicita��o tente novamente mais tarde!');
end;

procedure TFmWOrdSerGru.SGWOrdSerGruDragOver(Sender, Source: TObject; X,
  Y: Integer; State: TDragState; var Accept: Boolean);
var
  CurrentCol, CurrentRow: integer;
begin
  SGWOrdSerGru.MouseToCell(X, Y, CurrentCol, CurrentRow);
  //
  Accept := (Sender = Source) and (CurrentRow > 0);
end;

procedure TFmWOrdSerGru.SGWOrdSerGruDrawCell(Sender: TObject; ACol,
  ARow: Integer; Rect: TRect; State: TGridDrawState);
begin
  with SGWOrdSerGru do
  begin
    if (ARow > 0) then
    begin
      Canvas.Font.Color := clBlack;
      Canvas.Font.Style := [];
      case ACol of
        0: Canvas.Brush.Color := DmkImg.HexToTColor('FAEAE5');
        1: Canvas.Brush.Color := DmkImg.HexToTColor('D6ECF2');
        2: Canvas.Brush.Color := DmkImg.HexToTColor('F3F3F3');
        3: Canvas.Brush.Color := DmkImg.HexToTColor('EBF1DE');
      end;
    end else
    begin
      Canvas.Font.Color := clWhite;
      Canvas.Font.Style := [fsBold];
      case ACol of
        0: Canvas.Brush.Color := DmkImg.HexToTColor('CC293D');
        1: Canvas.Brush.Color := DmkImg.HexToTColor('009CCC');
        2: Canvas.Brush.Color := DmkImg.HexToTColor('C3C3C3');
        3: Canvas.Brush.Color := DmkImg.HexToTColor('B8CE8A');
      end;
    end;
    Canvas.FillRect(Rect);
    Canvas.TextOut(Rect.Left+2,Rect.Top+2,Cells[ACol, ARow]);
  end;
end;

procedure TFmWOrdSerGru.SGWOrdSerGruMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var
  Codigo: Integer;
begin
  SGWOrdSerGru.MouseToCell(X, Y, FSourceCol, FSourceRow);
  //
  if FSourceRow < 1 then
    Exit;
  //
  Codigo := Geral.IMV(SGWOrdSerGru.Cells[FSourceCol, FSourceRow]);
  //
  if (FSourceRow > 0) and (ssLeft in Shift) and (Codigo <> 0) then
    SGWOrdSerGru.BeginDrag(False, 4);
end;

procedure TFmWOrdSerGru.SGWOrdSerGruSelectCell(Sender: TObject; ACol,
  ARow: Integer; var CanSelect: Boolean);
var
  Codigo: Integer;
begin
  if ARow > 0 then
  begin
    Codigo := Geral.IMV(SGWOrdSerGru.Cells[ACol, ARow]);
    //
    if Codigo <> 0 then
      ReopenWOrdSerGruIts(Codigo);
  end;
end;

procedure TFmWOrdSerGru.ConfiguraMenuGrupo(Habilita: Boolean; Grupo: Integer);
begin
  BtAlteraGrupo.Enabled := Habilita;
  BtExcluiGrupo.Enabled := Habilita;
  TSGrade.TabVisible    := Habilita;
  //
  ReopenWOrdSer(0);
  //
  PCGrupo.ActivePageIndex := 0;
  TSChamados.TabVisible   := True;
end;

procedure TFmWOrdSerGru.ReopenWOrdSerGru(Query: TmySQLQuery; Codigo: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(Query, Dmod.MyDBn, [
    'SELECT gru.*, wus.PersonalName Respons_TXT, ',
    'IF(gru.IniData <= "1899-12-30", "", DATE_FORMAT(gru.IniData, "%d/%m/%Y")) IniData_TXT, ',
    'IF(gru.FimData <= "1899-12-30", "", DATE_FORMAT(gru.FimData, "%d/%m/%Y")) FimData_TXT, ',
    'st1.Nome STATUS1, st2.Nome STATUS2, st3.Nome STATUS3, st4.Nome STATUS4 ',
    'FROM wordsergru gru ',
    'LEFT JOIN wusers wus ON wus.Codigo = gru.Respons ',
    'LEFT JOIN wordsersta st1 ON st1.Codigo = gru.GruStatus1',
    'LEFT JOIN wordsersta st2 ON st2.Codigo = gru.GruStatus2',
    'LEFT JOIN wordsersta st3 ON st3.Codigo = gru.GruStatus3',
    'LEFT JOIN wordsersta st4 ON st4.Codigo = gru.GruStatus4',
    'ORDER BY gru.Ordem ',
    '']);
  //
  if Codigo <> 0 then
    Query.Locate('Codigo', Codigo, []);
end;

procedure TFmWOrdSerGru.ReopenWOrdSerGruIts(Codigo: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrWOrdSerGruIts, Dmod.MyDBn, [
    'SELECT its.Codigo, its.Descri, wus.PersonalName NOMERESP, ',
    'pri.Nome NOMEPRIOR, its.DuracaoApr ',
    'FROM wordser its ',
    'LEFT JOIN wusers wus ON wus.Codigo = its.Respons ',
    'LEFT JOIN wordserpri pri ON pri.Codigo = its.Prioridade ',
    'WHERE its.Codigo=' + Geral.FF0(Codigo),
    '']);
end;

end.

