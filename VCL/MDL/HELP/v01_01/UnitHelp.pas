unit UnitHelp;

interface

uses
  System.DateUtils, StdCtrls, ExtCtrls, Windows, Messages, SysUtils, Classes,
  Graphics, Controls, ComCtrls, dmkGeral, Forms, Dialogs, UnMsgInt, Db, DbCtrls,
  UnInternalConsts, Variants, mySQLDbTables, UnDmkEnums, SHDocVw, MSHTML,
  WBFuncs, Xml.XMLIntf, Xml.XMLDoc, Web.HTTPApp, UnGrlHelp;

type
  TUnitHelp = class(TObject)
  private
    { Private declarations }
  public
    { Public declarations }
    procedure InserirImagem(WebBrowser: TWebBrowser; Img, Legenda: String);
    procedure InserirVideo(WebBrowser: TWebBrowser; Url: String);
    procedure InserirUrl(WebBrowser: TWebBrowser; Url, Descri: String; FAQ: Integer);
    function  ObtemTextoHTML(Codigo: Integer): WideString;
    procedure IncluiHelpRestr(HelpTip: THelpTip; Tipo, Codigo: Integer;
              QueryRestr: TmySQLQuery);
  end;

var
  UnHelp: TUnitHelp;
  HTMLDocumento: IHTMLDocument2;

implementation

uses Module, ModuleGeral, UnMyObjects, DmkDAC_PF, UnDmkImg, MyDBCheck,
  UnDmkProcFunc, UnGrl_REST, UnGrlUsuarios;

{ TUnitHelp }

procedure TUnitHelp.InserirUrl(WebBrowser: TWebBrowser; Url, Descri: String;
  FAQ: Integer);
var
  Html, UrlStr: String;
  Sel: IHTMLSelectionObject;
  Range: IHTMLTxtRange;
begin
  if (Url <> '') or ((FAQ <> 0) and (Descri <> '')) then
  begin
    if Url <> '' then
    begin
      UrlStr := CO_HELP_Img_EXT + CO_HELP_Separador + Url + CO_HELP_Separador;
      //
      if Descri <> '' then
        UrlStr := UrlStr + Descri
      else
        UrlStr := UrlStr + Url;
    end else
      UrlStr := CO_HELP_Img_INT + CO_HELP_Separador + Geral.FF0(FAQ) +
                  CO_HELP_Separador + Descri;
    //
    Html := CO_HELP_UrlTagI + UrlStr + CO_HELP_UrlTagF;
    //
    HTMLDocumento := WebBrowser.Document as IHTMLDocument2;
    Sel := HTMLDocumento.selection;
    if Assigned(Sel) then
    begin
      if (Sel.type_ = 'None') or (Sel.type_ = 'Text') then
      begin
        Range := Sel.createRange as IHTMLTxtRange;
        Range.pasteHTML(Html);
      end;
    end;
  end;
end;

function TUnitHelp.ObtemTextoHTML(Codigo: Integer): WideString;
var
  Res, i, j, Tot: Integer;
  Resul, XMLNode, XMLVal: String;
  ParamCod: THttpParam;
  XMLArqRes: IXMLDocument;
begin
  ParamCod.Nome         := 'codigo';
  ParamCod.Valor        := Geral.FF0(Codigo);
  ParamCod.ACharse      := '';
  ParamCod.AContentType := '';
  //
  Res := Grl_REST.Rest_Dermatek_Post('textos', 'faq_item', trtXML, [ParamCod], Resul);
  //
  if Res = 200 then
  begin
    XMLArqRes := TXMLDocument.Create(nil);
    XMLArqRes.Active := False;
    try
      XMLArqRes.LoadFromXML(Resul);
      XMLArqRes.Encoding := 'ISO-10646-UCS-2';
      //
      for i := 0 to XMLArqRes.DocumentElement.ChildNodes.Count - 1 do
      begin
        with XMLArqRes.DocumentElement.ChildNodes[i] do
        begin
          Tot := ChildNodes.Count;
          //
          if Tot > 0 then
          begin
            for j := 0 to ChildNodes.Count - 1 do
            begin
              XMLNode := ChildNodes[j].NodeName;
              XMLVal  := ChildNodes[j].ChildNodes[0].Text;
              //
              if UpperCase(XMLNode) = 'TEXTO' then
                Result := XMLVal;
            end;
          end;
        end;
      end;
    finally
      ;
    end;
  end;
end;

procedure TUnitHelp.IncluiHelpRestr(HelpTip: THelpTip; Tipo, Codigo: Integer;
  QueryRestr: TmySQLQuery);
var
  Continua: Boolean;
  Cod: Integer;
  App: Variant;
  Qry: TmySQLQuery;
  SQLCompl, ListaIts, Item: String;
begin
  Continua := False;
  Cod      := 0;
  //
  if Tipo = 1 then
  begin
    Continua := DBCheck.EscolheCodigosMultiplos_0('...',
      'HLP-CADAS-002 :: Resti��o de Aplicativos - Local',
      'Seleciones os m�dulos desejados:', nil, 'Ativo', 'Nivel1', 'Nome', [
      'DELETE FROM _selcods_; ',
      'INSERT INTO _selcods_ ',
      'SELECT Codigo Nivel1, 0 Nivel2, ',
      '0 Nivel3, 0 Nivel4, 0 Nivel5, Nome, 0 Ativo ',
      'FROM ' + TMeuDB + '.aplicativos ',
      'WHERE Ativo = 1 ',
      'AND Categoria IN (0,1) ',
      'AND Codigo NOT IN ',
      '( ',
      'SELECT Item ',
      'FROM ' + TMeuDB + '.helprestr ',
      'WHERE HelpResTip = 1 ',
      'AND Help=' + Geral.FF0(Codigo),
      'AND HelpTip=' + Geral.FF0(Integer(HelpTip)),
      ') ',
      'ORDER BY Nome ',
      ''],[
      'SELECT * FROM _selcods_; ',
      ''], Dmod.QrUpd);
  end else
  if Tipo = 2 then
  begin
    Qry := TmySQLQuery.Create(Dmod);
    try
      ListaIts := '';
      SQLCompl := '';
      //
      UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
        'SELECT Item ',
        'FROM helprestr ',
        'WHERE HelpResTip = 2 ',
        'AND Help=' + Geral.FF0(Codigo),
        'AND HelpTip=' + Geral.FF0(Integer(HelpTip)),
        '']);
      if Qry.RecordCount > 0 then
      begin
        Qry.First;
        while not Qry.Eof do
        begin
          Item := Geral.FF0(Qry.FieldByName('Item').AsInteger);
          //
          if Qry.RecNo = Qry.RecordCount then
            ListaIts := ListaIts + Item
          else
            ListaIts := ListaIts + Item + ', ';
          //
          Qry.Next;
        end;
        if ListaIts <> '' then
          SQLCompl := 'AND Codigo NOT IN (' + ListaIts + ')';
      end;
      App := DBCheck.EscolheCodigoUnico('...',
        'HLP-CADAS-002 :: Sele��o de aplicativo',
        'Informe o aplicativo: [F7 para pesquisar]', nil, nil, 'Descricao', 0, [
        'SELECT Codigo, Nome Descricao ',
        'FROM appwebcab ',
        'WHERE Ativo = 1 ',
        SQLCompl,
        'ORDER BY Nome ',
        ''], Dmod.MyDBn, True);
      //
      if App <> Null then
      begin
          UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDBn, [
            'SELECT Codigo, Nome ',
            'FROM appwebcab ',
            'WHERE Codigo=' + Geral.FF0(App),
            '']);
          if Qry.RecordCount > 0 then
          begin
            GrlHelp.Ins_HelpRestr(Dmod.MyDB, Dmod.QrUpd, stDesktop,
              Qry.FieldByName('Nome').AsString, App, Tipo, Codigo,
              HelpTip, Cod);
            GrlHelp.ReopenHelpRestr(Dmod.MyDB, QueryRestr, Codigo,
              Integer(HelpTip), Cod);
          end;
      end;
    finally
      Qry.Free;
    end;
  end else
  if Tipo = 3 then
  begin
    Continua := DBCheck.EscolheCodigosMultiplos_0('...',
      'HLP-CADAS-002 :: Resti��o de Aplicativos - Web',
      'Seleciones os m�dulos desejados:', nil, 'Ativo', 'Nivel1', 'Nome', [
      'DELETE FROM _selcods_; ',
      'INSERT INTO _selcods_ ',
      'SELECT Codigo Nivel1, 0 Nivel2, ',
      '0 Nivel3, 0 Nivel4, 0 Nivel5, Nome, 0 Ativo ',
      'FROM ' + TMeuDB + '.modulos ',
      'WHERE Ativo = 1 ',
      'AND Codigo NOT IN ',
      '( ',
      'SELECT Item ',
      'FROM ' + TMeuDB + '.helprestr ',
      'WHERE HelpResTip = 3 ',
      'AND Help=' + Geral.FF0(Codigo),
      'AND HelpTip=' + Geral.FF0(Integer(HelpTip)),
      ') ',
      'ORDER BY Nome ',
      ''],[
      'SELECT * FROM _selcods_; ',
      ''], Dmod.QrUpd);
  end;
  if Continua then
  begin
    DModG.QrSelCods.First;
    //
    while not DModG.QrSelCods.Eof do
    begin
      if DModG.QrSelCodsAtivo.Value = 1 then
        GrlHelp.Ins_HelpRestr(Dmod.MyDB, Dmod.QrUpd, stDesktop,
          DModG.QrSelCodsNome.Value, DModG.QrSelCodsNivel1.Value, Tipo,
          Codigo, HelpTip, Cod);
      //
      DModG.QrSelCods.Next;
    end;
    GrlHelp.ReopenHelpRestr(Dmod.MyDB, QueryRestr, Codigo, Integer(HelpTip), Cod);
  end;
end;

procedure TUnitHelp.InserirVideo(WebBrowser: TWebBrowser; Url: String);
var
  Html: String;
  Sel: IHTMLSelectionObject;
  Range: IHTMLTxtRange;
begin
  if Url <> '' then
  begin
    Html := CO_HELP_VideoTagI + Url + CO_HELP_VideoTagF;
    //
    HTMLDocumento := WebBrowser.Document as IHTMLDocument2;
    Sel := HTMLDocumento.selection;
    if Assigned(Sel) then
    begin
      if (Sel.type_ = 'None') or (Sel.type_ = 'Text') then
      begin
        Range := Sel.createRange as IHTMLTxtRange;
        Range.pasteHTML(Html);
      end;
    end;
  end;
end;

procedure TUnitHelp.InserirImagem(WebBrowser: TWebBrowser; Img, Legenda: String);
var
  Html: String;
  Sel: IHTMLSelectionObject;
  Range: IHTMLTxtRange;
begin
  if Img <> '' then
  begin
    Html := CO_HELP_ImgTagI + Img + CO_HELP_Separador + Legenda + CO_HELP_ImgTagF;
    //
    HTMLDocumento := WebBrowser.Document as IHTMLDocument2;
    Sel := HTMLDocumento.selection;
    if Assigned(Sel) then
    begin
      if (Sel.type_ = 'None') or (Sel.type_ = 'Text') then
      begin
        Range := Sel.createRange as IHTMLTxtRange;
        Range.pasteHTML(Html);
      end;
    end;
  end;
end;

end.
