unit ModWOrdSer;

interface

uses
  SysUtils, Classes, DB, mySQLDbTables, Dialogs, Forms, Windows, ExtCtrls,
  UnMLAGeral, DBGrids, frxClass, dmkGeral, frxDBSet, Controls, Variants,
  Graphics, UnDmkProcFunc, DmkDAC_PF, dmkLabel, StdCtrls, ComCtrls, UnDmkEnums,
  Grids, dmkEdit, UnProjGroup_Consts;

type
  TOpcUser = (istApenasUsuario, istApenasPadrao, istTodos);
  TDModWOrdSer = class(TDataModule)
    QrClientes: TmySQLQuery;
    QrClientesCodigo: TIntegerField;
    QrClientesNome: TWideStringField;
    DsClientes: TDataSource;
    QrWOrdSerPri: TmySQLQuery;
    IntegerField4: TIntegerField;
    StringField2: TWideStringField;
    DsWOrdSerPri: TDataSource;
    QrWOrdSerMod: TmySQLQuery;
    IntegerField6: TIntegerField;
    StringField5: TWideStringField;
    DsWOrdSerMod: TDataSource;
    QrWOrdSerSta: TmySQLQuery;
    IntegerField5: TIntegerField;
    StringField4: TWideStringField;
    DsWOrdSerSta: TDataSource;
    QrResp: TmySQLQuery;
    DsResp: TDataSource;
    QrAplicativo: TmySQLQuery;
    StringField7: TWideStringField;
    DsAplicativo: TDataSource;
    QrCli: TmySQLQuery;
    QrCliCNPJ_TXT: TWideStringField;
    QrCliTE1_TXT: TWideStringField;
    QrCliFAX_TXT: TWideStringField;
    QrCliNUMERO_TXT: TWideStringField;
    QrCliCEP_TXT: TWideStringField;
    QrCliIE_TXT: TWideStringField;
    QrAplicativoCodigo: TIntegerField;
    QrWUsers: TmySQLQuery;
    DsWUsers: TDataSource;
    QrWUsersCodigo: TAutoIncField;
    QrWUsersPersonalName: TWideStringField;
    frxDsCli: TfrxDBDataset;
    QrAplic: TmySQLQuery;
    DsAplic: TDataSource;
    QrAplicCodigo: TIntegerField;
    QrAplicNome: TWideStringField;
    QrCliE_ALL: TWideStringField;
    QrCliCodigo: TIntegerField;
    QrCliTipo: TSmallintField;
    QrCliIE: TWideStringField;
    QrCliNOME_ENT: TWideStringField;
    QrCliCNPJ_CPF: TWideStringField;
    QrCliNO2_ENT: TWideStringField;
    QrCliIE_RG: TWideStringField;
    QrCliRUA: TWideStringField;
    QrCliNUMERO: TIntegerField;
    QrCliCOMPL: TWideStringField;
    QrCliBAIRRO: TWideStringField;
    QrCliCIDADE: TWideStringField;
    QrCliUF: TSmallintField;
    QrCliNOMELOGRAD: TWideStringField;
    QrCliNOMEUF: TWideStringField;
    QrCliPais: TWideStringField;
    QrCliLograd: TSmallintField;
    QrCliCEP: TIntegerField;
    QrCliENDEREF: TWideStringField;
    QrCliTE1: TWideStringField;
    QrCliFAX: TWideStringField;
    QrCliCAD_FEDERAL: TWideStringField;
    QrCliCAD_ESTADUAL: TWideStringField;
    QrFTPDir: TmySQLQuery;
    QrFTPDirFTPConfig: TIntegerField;
    QrFTPDirCodigo: TIntegerField;
    QrFTPDirCaminho: TWideStringField;
    QrWOrdSerOpc: TmySQLQuery;
    QrWOrSeAteIt: TmySQLQuery;
    QrRespCodigo: TAutoIncField;
    QrRespPersonalName: TWideStringField;
    QrWOrdSerAss: TmySQLQuery;
    DsWOrdSerAss: TDataSource;
    QrLoc: TmySQLQuery;
    QrWOrdSerAssCodigo: TIntegerField;
    QrWOrdSerAssNome: TWideStringField;
    QrWOrdSerAssDescri: TWideMemoField;
    QrWOrdSerPriDescri: TWideStringField;
    QrWUsersNOME_TXT: TWideStringField;
    DsWOrdSerGruPes: TDataSource;
    QrWOrdSerGruPes: TmySQLQuery;
    IntegerField2: TIntegerField;
    StringField1: TWideStringField;
    QrWOSOpcUsu: TmySQLQuery;
    QrRequisitante: TmySQLQuery;
    AutoIncField1: TAutoIncField;
    StringField3: TWideStringField;
    DsRequisitante: TDataSource;
    procedure QrCliCalcFields(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
    //Atualiza��es Ini
    procedure AtualizaWOrdSerHisDeWOrdSer(Progress: TProgressBar);
    //Atualiza��es Fim
    procedure MostraWOrdSerGru(AbrirEmAba: Boolean; InOwner: TWincontrol;
              Pager: TWinControl);
    procedure MostraWOrdSerOpc();
    procedure MostraWOrdSerMod(Codigo: Integer);
    procedure MostraWOrdSerAteParGr(Codigo: Integer);
    procedure MostraWOrdSerSta(Codigo: Integer);
    procedure MostraWOrdSerSol(Codigo: Integer);
    procedure MostraWOrdSerAss(Codigo: Integer);
    procedure MostraWOrdSerAssGru(Codigo: Integer);
    procedure MostraWOrdSerAte();
    procedure MostraWOrdSerAtePar(Codigo: Integer);
    procedure MostraWOrdSerCom(SQLType: TSQLType; Codigo, Controle, Entidade,
              Solicitante, ComRes: Integer; Nome: String);
    procedure MostraWOrdSerEnc(Codigo, Finaliz, Assunto: Integer; Descri: String;
              Abertura: TDateTime; Encerrou: Boolean);
    procedure MostraInformacoesDeRecebimentoDeEmail(Recebeu: Boolean; DataHora: TDateTime; IP: String);
    procedure ReopenAplicativos(Cliente: Integer);
    procedure ReopenClientes();
    procedure ReopenWOrdSerMod();
    procedure ReopenWOrdSerPri();
    procedure ReopenWOrdSerAss();
    procedure ReopenWOrdSerGruPes();
    procedure ReopenWOrdSerSta(Nivel, Grupo: Integer);
    procedure ReopenRespons();
    procedure ReopenRequisitante();
    procedure ReopenWUsers(Entidade: Integer);
    procedure ReopenFTPDir(Codigo: Integer);
    procedure ReopenWOrdSerOpc();
    procedure ReopenWOSOpcUsu(Usuario: Integer; Modo: TOpcUser);
    procedure ReopenCli(Cliente: Integer);
    procedure AtualizaStatusUsuarioWEB(Codigo, Status: Integer);
    procedure SetaVersoesAtuais(Aplicativo: Integer; Combo: TComboBox);
    procedure MostraWOrdSerIts(SQLType: TSQLType; QueryWOrdSer: TmySQLQuery;
              Mostra: Boolean; Grupo: Integer = 0; Status: Integer = 0;
              DadosUsuario: Boolean = False);
    procedure AlteraGrupoStatus(WOrdSer: String; Grupo, GrupoAtual: Integer);
    procedure ReordenaItens(Tabela, CampoOrdem, CampoChavePri: String;
              NovaOrdem, ChavePri: Integer);
    procedure MontaGradeGrupo(Grupo: Integer; StringGrid: TStringGrid; Query,
              QueryGru: TmySQLQuery);
    procedure AtualizaConfiguracoesUsuario(Usuario: Integer; Ordem_CamposFields,
              Ordem_CamposTxt: String);
    procedure AtualizaOrdemOSs(Usuario, Grupo: Integer);
    function  HabilitaAcessoremoto(TipoUser: Integer): Boolean;
    function  AlteraResponsavel(Codigo, Aplicativo, Solicitante, Cliente,
              Assunto: Integer): Boolean;
    function  AlteraStatus(Codigo: Integer; Grade: TDBGrid; Query: TmySQLQuery): Boolean;
    function  AlteraAssunto(Codigo: Integer; Grade: TDBGrid; Query: TmySQLQuery): Boolean;
    function  AlteraPrioridade(Codigo: Integer): Boolean;
    function  ConfiguraOrdemCampos(CamposTxt_Default, CamposFields_Default: String;
              var CamposTxt_Sel, CamposFields_Sel: String): Boolean;
    function  ExcluiWOrdSerCom(Codigo, Controle: Integer): Boolean;
    function  VerificaMaxData(Codigo, Controle: Integer; Abertura, Data: TDateTime): Boolean;
    function  VerificaMaxHistorico(const Responsavel: Integer; var Controle,
              Usuario: Integer): Boolean;
    //Antigo Usar o ObtemAtendente3 function  ObtemAtendente(Aplicativo, Solicitante, Cliente, Assunto: Integer): Integer;
    function  ObtemAtendente3(Aplicativo, Solicitante, Cliente, Assunto, Cargo: Integer): Integer;
    function  VerificaSeEnviaEmailAtendente(Atendente: Integer): Boolean;
    function  ObtemMaxOrdem(Tabela, CampoOrdem: String): Integer;
  end;

var
  DModWOrdSer: TDModWOrdSer;


implementation

uses Module, MyDBCheck, UnInternalConsts, ModuleGeral, WOrdSerMod,
  WOrdSerSta, WOrdSerCom, UMySQLModule, WOrdSerEnc, WOrdSerSol, MyListas,
  WOrdSerOpc, WOrdSerAte, WOrdSerAss, UnDmkWeb, WOrdSerAtePar, ModuleLct2,
  UnMyObjects, WOrdSerIts, WOrdSerStaGer, CfgCadLista, WOrdSerAteParGr,
  WOrdSerAssGru, WOrdSerRes, WOrdSerGru, UnGrl_Vars;

{$R *.dfm}

{ TDModWOrdSer }

function TDModWOrdSer.AlteraPrioridade(Codigo: Integer): Boolean;
const
  Aviso  = '...';
  Titulo = 'Sele��o de prioridade';
  Prompt = 'Informe a prioridade: [F7 para pesquisar]';
  Campo  = 'Descricao';
var
  Prioridade: Variant;
begin
  Result := False;
  //
  Prioridade := DBCheck.EscolheCodigoUnico(Aviso, Titulo, Prompt, nil, nil, Campo, 0, [
    'SELECT Codigo, Nome Descricao ',
    'FROM wordserpri ',
    'WHERE Ativo = 1 ',
    'ORDER BY Nome ',
    ''], Dmod.MyDBn, True);
  //
  if (Prioridade <> Null) and (Codigo <> 0) then
  begin
    if UMyMod.SQLInsUpd(Dmod.QrUpdN, stUpd, 'wordser', False, ['Prioridade'],
      ['Codigo'], [Prioridade], [Codigo], False)
    then
      Result := True;
  end;
end;

function TDModWOrdSer.HabilitaAcessoremoto(TipoUser: Integer): Boolean;
var
  TViewer: Integer;
  TViewe_Grupo: String;
begin
  ReopenWOrdSerOpc;

  TViewer      := QrWOrdSerOpc.FieldByName('UsaTeamViewer').AsInteger;
  TViewe_Grupo := QrWOrdSerOpc.FieldByName('GrupoTeamViewer').AsString;

  if((TViewer = 1) and (TViewe_Grupo <> '') and (TipoUser in [0,1])) then
    Result := True
  else
    Result := False;
end;


function TDModWOrdSer.AlteraResponsavel(Codigo, Aplicativo, Solicitante,
  Cliente, Assunto: Integer): Boolean;
begin
  Result := False;
  //
  if not DmkWeb.ConectarUsuarioWEB(True) then Exit;
  //
  if DmkWeb.ConexaoRemota(Dmod.MyDBn, DmodG.QrWebParams, 1) then
  begin
    if DBCheck.CriaFm(TFmWOrdSerRes, FmWOrdSerRes, afmoNegarComAviso) then
    begin
      FmWOrdSerRes.FCodigo      := Codigo;
      FmWOrdSerRes.FAplicativo  := Aplicativo;
      FmWOrdSerRes.FSolicitante := Solicitante;
      FmWOrdSerRes.FCliente     := Cliente;
      FmWOrdSerRes.FAssunto     := Assunto;
      FmWOrdSerRes.ShowModal;
      //
      Result := FmWOrdSerRes.FReabre;
      //
      FmWOrdSerRes.Destroy;
    end;
  end;
end;

procedure TDModWOrdSer.AtualizaConfiguracoesUsuario(Usuario: Integer;
  Ordem_CamposFields, Ordem_CamposTxt: String);
var
  User: Integer;
begin
  ReopenWOSOpcUsu(Usuario, istApenasUsuario);
  //
  User := QrWOSOpcUsu.FieldByName('Usuario').AsInteger;
  //
  if User <> 0 then
  begin
    UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpdN, Dmod.MyDBn, [
      'UPDATE wosopcusu SET Ordem_CamposFields="' + Ordem_CamposFields + '", Ordem_CamposTxt="' + Ordem_CamposTxt + '"',
      'WHERE Usuario=' + Geral.FF0(VAR_WEB_USR_ID),
      '']);
  end else
  begin
    UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpdN, Dmod.MyDBn, [
      'INSERT INTO wosopcusu SET Ordem_CamposFields="' + Ordem_CamposFields + '", Ordem_CamposTxt="' + Ordem_CamposTxt + '",',
      'Usuario=' + Geral.FF0(VAR_WEB_USR_ID),
      '']);
  end;
end;

procedure TDModWOrdSer.AtualizaStatusUsuarioWEB(Codigo, Status: Integer);
begin
  UMyMod.SQLInsUpd(Dmod.QrUpdN, stUpd, 'wusers', False,
    ['Status'], ['Codigo'], [Status], [Codigo], True);
end;

procedure TDModWOrdSer.AtualizaOrdemOSs(Usuario, Grupo: Integer);
var
  Codigo, Ordem: Integer;
  TimeZoneDifUTC, Order: String;
begin
  try
    ReopenWOSOpcUsu(Usuario, istTodos);
    //
    Ordem          := 0;
    Order          := QrWOSOpcUsu.FieldByName('Ordem_CamposFields').AsString;
    TimeZoneDifUTC := VAR_WEB_USR_TZDIFUTC;
    //
    QrLoc.Close;
    QrLoc.Database := Dmod.MyDBn;
    QrLoc.SQL.Clear;
    QrLoc.SQL.Add('SELECT wor.Codigo, wor.Abertura, ');
    QrLoc.SQL.Add('wor.Encerr, wor.VersaoApp, wor.Janela, ');
    QrLoc.SQL.Add('wor.JanelaRel, wor.CompoRel, wur.PersonalName NOMERESP, ');
    QrLoc.SQL.Add('CONVERT_TZ(his.DataHora, "+00:00", "' + TimeZoneDifUTC + '") DATAHORAHIS, ');
    QrLoc.SQL.Add('CONCAT(LPAD(ass.Ordem, 3, 0), " - ", ass.Nome) AssuntoTXT, ');
    QrLoc.SQL.Add('CONCAT(LPAD(pri.Ordem, 3, 0), " - ", pri.Nome) NOMEPRI, ');
    QrLoc.SQL.Add('sta.Nome NOMESTA, wus.PersonalName, wmo.Nome NOMEMOD, ');
    QrLoc.SQL.Add('IF (ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMECLI, ');
    QrLoc.SQL.Add('CASE his.Tarefa ');
    QrLoc.SQL.Add('  WHEN 0 THEN "Abertura" ');
    QrLoc.SQL.Add('  WHEN 1 THEN "Encerramento" ');
    QrLoc.SQL.Add('  WHEN 2 THEN "Comentado" ');
    QrLoc.SQL.Add('  WHEN 3 THEN "Reabertura" ');
    QrLoc.SQL.Add('  WHEN 4 THEN "Mudan�a de Status" END STATUS_TXT, ');
    //
    if CO_DMKID_APP = 17 then //DControl
      QrLoc.SQL.Add('apl.Nome NOMEAPLIC, ')
    else
      QrLoc.SQL.Add('"" NOMEAPLIC, ');
    //
    QrLoc.SQL.Add('FROM wordser wor ');
    QrLoc.SQL.Add('LEFT JOIN wordserhis his ON his.Controle = wor.WOrdSerHis ');
    QrLoc.SQL.Add('LEFT JOIN wordserass ass ON ass.Codigo = wor.Assunto ');
    QrLoc.SQL.Add('LEFT JOIN wordserpri pri ON pri.Codigo = wor.Prioridade ');
    QrLoc.SQL.Add('LEFT JOIN wordsersta sta ON sta.Codigo = wor.Status ');
    QrLoc.SQL.Add('LEFT JOIN wusers wus ON wus.Codigo = wor.Solicit ');
    QrLoc.SQL.Add('LEFT JOIN entidades ent ON ent.Codigo = wor.Cliente ');
    QrLoc.SQL.Add('LEFT JOIN wordsermod wmo ON wmo.Codigo = wor.Modo ');
    QrLoc.SQL.Add('LEFT JOIN wusers wur ON wur.Codigo = wor.Respons ');
    //
    if CO_DMKID_APP = 17 then //DControl
      QrLoc.SQL.Add('LEFT JOIN aplicativos apl ON apl.Codigo = wor.Aplicativo');
    //
    QrLoc.SQL.Add('WHERE wor.Respons=' + Geral.FF0(Usuario));
    QrLoc.SQL.Add('AND wor.Grupo=' + Geral.FF0(Grupo));
    QrLoc.SQL.Add('AND wor.Encerr = 0 ');
    QrLoc.SQL.Add('ORDER BY ' + Order);
    QrLoc.Open;
    //
    while not QrLoc.Eof do
    begin
      Codigo := QrLoc.FieldByName('Codigo').AsInteger;
      Ordem  := Ordem + 1;
      //
      UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpdN, Dmod.MyDBn, [
        'UPDATE wordser SET Grupo=' + Geral.FF0(Grupo),
        'WHERE Codigo=' + Geral.FF0(Codigo),
        '']);
      //
      QrLoc.Next;
    end;
  finally
    QrLoc.Close;
  end;
end;

function TDModWOrdSer.ObtemMaxOrdem(Tabela, CampoOrdem: String): Integer;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrLoc, Dmod.MyDBn, [
    'SELECT MAX(' + CampoOrdem + ') Ordem ',
    'FROM ' + Tabela,
    '']);
  if QrLoc.RecordCount > 0 then
    Result := QrLoc.FieldByName('Ordem').AsInteger + 1
  else
    Result := 1;
end;

procedure TDModWOrdSer.ReordenaItens(Tabela, CampoOrdem, CampoChavePri: String;
  NovaOrdem, ChavePri: Integer);
var
  Codigo, Ordem: Integer;
begin
  UMyMod.SQLInsUpd(Dmod.QrUpdN, stUpd, Tabela, False, [CampoOrdem],
    [CampoChavePri], [NovaOrdem], [ChavePri], True);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrLoc, Dmod.MyDBn, [
    'SELECT ' + CampoChavePri + ' Codigo, ' + CampoOrdem + ' Ordem ',
    'FROM ' + Tabela,
    'WHERE Ordem >= ' + Geral.FF0(NovaOrdem),
    'AND Codigo <> ' + Geral.FF0(ChavePri),
    '']);
  if QrLoc.RecordCount > 0 then
  begin
    while not QrLoc.Eof do
    begin
      Codigo := QrLoc.FieldByName('Codigo').AsInteger;
      Ordem  := QrLoc.FieldByName('Ordem').AsInteger + 1;
      //
      UMyMod.SQLInsUpd(Dmod.QrUpdN, stUpd, Tabela, False, [CampoOrdem],
        [CampoChavePri], [Ordem], [Codigo], True);
      //
      QrLoc.Next;
    end;
  end;
end;

procedure TDModWOrdSer.AlteraGrupoStatus(WOrdSer: String; Grupo, GrupoAtual: Integer);
var
  WOSer: String;
  Status1: Integer;
begin
  if WOrdSer = '' then Exit;
  //
  ReopenWOrdSerOpc;
  //
  Status1 := QrWOrdSerOpc.FieldByName('GruStatus1').AsInteger;
  //
  if Copy(WOrdSer, Length(WOrdSer), 1) = ',' then
    WOSer := Copy(WOrdSer, 1, Length(WOrdSer) - 1)
  else
    WOser := WOrdSer;
  //
  if GrupoAtual <> 0 then
  begin
    UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpdN, Dmod.MyDBn, [
      'UPDATE wordser SET Grupo=' + Geral.FF0(Grupo),
      'WHERE Codigo IN (' + WOSer + ')',
      '']);
  end else
  begin
    UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpdN, Dmod.MyDBn, [
      'UPDATE wordser SET Grupo=' + Geral.FF0(Grupo) + ', Status=' + Geral.FF0(Status1),
      'WHERE Codigo IN (' + WOSer + ')',
      '']);
  end;
end;

procedure TDModWOrdSer.AtualizaWOrdSerHisDeWOrdSer(Progress: TProgressBar);
var
  Codigo, WOrdSerHis: Integer;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(Dmod.QrWeb, Dmod.MyDBn, [
    'SELECT MAX(Controle) His, Codigo ',
    'FROM wordserhis ',
    'GROUP BY Codigo ',
    '']);
  if Dmod.QrWeb.RecordCount > 0 then
  begin
    Progress.Visible  := True;
    Progress.Max      := Dmod.QrWeb.RecordCount;
    Progress.Position := 0;
    //
    Dmod.QrWeb.First;
    while not Dmod.QrWeb.Eof do
    begin
      Codigo     := Dmod.QrWeb.FieldByName('Codigo').AsInteger;
      WOrdSerHis := Dmod.QrWeb.FieldByName('His').AsInteger;
      //
      UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpdN, Dmod.MyDBn, [
        'UPDATE wordser SET WOrdSerHis=' + Geral.FF0(WOrdSerHis),
        'WHERE Codigo=' + Geral.FF0(Codigo),
        '']);
      Progress.Position := Progress.Position + 1;
      Progress.Update;
      Application.ProcessMessages;
      //
      Dmod.QrWeb.Next;
    end;
    Progress.Visible := False;
  end;
end;

function TDModWOrdSer.ConfiguraOrdemCampos(CamposTxt_Default,
  CamposFields_Default: String; var CamposTxt_Sel, CamposFields_Sel: String): Boolean;
begin
  Result := DBCheck.ConfiguraOrdemCamposTabela(['Data da �ltima modifica��o', 'ID',
    'Abertura', 'Encerramento', 'Ordem / Assunto', 'Prioridade',
    'Status do servi�o', 'Solicitante', 'Cliente', 'Modo', 'Respons�vel',
    '�ltimo hist�rico', 'Aplicativo', 'Vers�o', 'Janela', 'Janela relac.',
    'Compo relac.'], ['DATAHORAHIS', 'Codigo', 'Abertura', 'Encerr',
    'AssuntoTXT', 'NOMEPRI', 'NOMESTA', 'PersonalName', 'NOMECLI', 'NOMEMOD',
    'NOMERESP', 'STATUS_TXT ', 'NOMEAPLIC', 'VersaoApp', 'Janela', 'JanelaRel',
    'CompoRel'], CamposTxt_Default, CamposFields_Default, ', ',
    CamposTxt_Sel, CamposFields_Sel);
end;

function TDModWOrdSer.ExcluiWOrdSerCom(Codigo, Controle: Integer): Boolean;
var
  Historico: Integer;
begin
  Result := False;
  //
  Dmod.QrUpdN.SQL.Clear;
  Dmod.QrUpdN.SQL.Add('DELETE FROM wordsercom WHERE Controle=:P0');
  Dmod.QrUpdN.Params[0].AsInteger := Controle;
  Dmod.QrUpdN.ExecSQL;
  //
  Dmod.QrUpdN.SQL.Clear;
  Dmod.QrUpdN.SQL.Add('DELETE FROM wordserhis WHERE Codigo=:P0 ');
  Dmod.QrUpdN.SQL.Add('AND Tarefa=:P1 AND CodOrig=:P2');
  Dmod.QrUpdN.Params[0].AsInteger := Codigo;
  Dmod.QrUpdN.Params[1].AsInteger := 2; //Coment�rio
  Dmod.QrUpdN.Params[2].AsInteger := Controle; //CodOrig = Controle do coment�rio
  Dmod.QrUpdN.ExecSQL;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrLoc, Dmod.MyDBn, [
    'SELECT MAX(Controle) Historico ',
    'FROM wordserhis ',
    'WHERE Codigo=' + Geral.FF0(Codigo),
    '']);
  Historico := QrLoc.FieldByName('Historico').AsInteger;
  //
  UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpdN, Dmod.MyDBn, [
    'UPDATE wordser SET WOrdSerHis=' + Geral.FF0(Historico),
    'WHERE Codigo=' + Geral.FF0(Codigo),
    '']);
  Result := True;
end;

procedure TDModWOrdSer.MontaGradeGrupo(Grupo: Integer; StringGrid: TStringGrid;
  Query, QueryGru: TmySQLQuery);
var
  ColWidth: Double;
  i, Status1, Status2, Status3, Status4, StatusOS: Integer;
begin
  MyObjects.LimpaGrade(StringGrid, 0, 0, True);
  //
  ColWidth               := (StringGrid.Width - 100) / 4;
  StringGrid.ColCount    := 4;
  StringGrid.Cells[0, 0] := QueryGru.FieldByName('STATUS1').AsString;
  StringGrid.Cells[1, 0] := QueryGru.FieldByName('STATUS2').AsString;
  StringGrid.Cells[2, 0] := QueryGru.FieldByName('STATUS3').AsString;
  StringGrid.Cells[3, 0] := QueryGru.FieldByName('STATUS4').AsString;
  //
  StringGrid.ColWidths[0] := Trunc(ColWidth);
  StringGrid.ColWidths[1] := Trunc(ColWidth);
  StringGrid.ColWidths[2] := Trunc(ColWidth);
  StringGrid.ColWidths[3] := Trunc(ColWidth);
  //
  StringGrid.ColWidths[0] := Trunc(ColWidth);
  StringGrid.ColWidths[1] := Trunc(ColWidth);
  StringGrid.ColWidths[2] := Trunc(ColWidth);
  StringGrid.ColWidths[3] := Trunc(ColWidth);
  //
  Status1 := QueryGru.FieldByName('GruStatus1').AsInteger;
  Status2 := QueryGru.FieldByName('GruStatus2').AsInteger;
  Status3 := QueryGru.FieldByName('GruStatus3').AsInteger;
  Status4 := QueryGru.FieldByName('GruStatus4').AsInteger;
  //
  if (Query.State <> dsInactive) and (Query.RecordCount > 0) then
  begin
    StringGrid.RowCount := Query.RecordCount + 1;
    Query.First;
    //
    for i := 1 to Query.RecordCount do
    begin
      StatusOS := Query.FieldByName('Status').AsInteger;
      //
      if StatusOS = Status1 then
      begin
        StringGrid.Cells[0, i] := Geral.FF0(Query.FieldByName('Codigo').AsInteger);
        StringGrid.Cells[1, i] := '';
        StringGrid.Cells[2, i] := '';
        StringGrid.Cells[3, i] := '';
      end;
      if StatusOS = Status2 then
      begin
        StringGrid.Cells[0, i] := '';
        StringGrid.Cells[1, i] := Geral.FF0(Query.FieldByName('Codigo').AsInteger);
        StringGrid.Cells[2, i] := '';
        StringGrid.Cells[3, i] := '';
      end;
      if StatusOS = Status3 then
      begin
        StringGrid.Cells[0, i] := '';
        StringGrid.Cells[1, i] := '';
        StringGrid.Cells[2, i] := Geral.FF0(Query.FieldByName('Codigo').AsInteger);
        StringGrid.Cells[3, i] := '';
      end;
      if StatusOS = Status4 then
      begin
        StringGrid.Cells[0, i] := '';
        StringGrid.Cells[1, i] := '';
        StringGrid.Cells[2, i] := '';
        StringGrid.Cells[3, i] := Geral.FF0(Query.FieldByName('Codigo').AsInteger);
      end;
      //
      Query.Next;
    end;
  end else
    StringGrid.RowCount := 1;
end;

procedure TDModWOrdSer.MostraInformacoesDeRecebimentoDeEmail(Recebeu: Boolean;
  DataHora: TDateTime; IP: String);
begin
  if Recebeu then
    Geral.MensagemBox('O e-mail foi recebido pelo destinat�rio!' + sLineBreak +
      sLineBreak + 'Data e hora do recebimento: ' + Geral.FDT(DataHora, 8) + sLineBreak
      + 'IP do computador que recebeu o e-mail: ' + IP,
      'Informa��es de recebimento do e-mail', MB_OK+MB_ICONINFORMATION)
  else
    Geral.MensagemBox('N�o foi poss�vel verificar se o e-mail foi recebido pelo destinat�rio!',
      'Informa��es de recebimento do e-mail', MB_OK+MB_ICONINFORMATION);
end;

procedure TDModWOrdSer.MostraWOrdSerGru(AbrirEmAba: Boolean; InOwner: TWincontrol;
  Pager: TWinControl);
begin
  if DmkWeb.ConexaoRemota(Dmod.MyDBn, DmodG.QrWebParams, 1) and
    (DmkWeb.ConectarUsuarioWEB(True)) then
  begin
    if AbrirEmAba then
    begin
      MyObjects.FormTDICria(TFmWOrdSerGru, InOwner, Pager, True, True);
    end else
    begin
      if DBCheck.CriaFm(TFmWOrdSerGru, FmWOrdSerGru, afmoNegarComAviso) then
      begin
        FmWOrdSerGru.ShowModal;
        FmWOrdSerGru.Destroy;
      end;
    end;
  end;
end;

procedure TDModWOrdSer.MostraWOrdSerAss(Codigo: Integer);
begin
  if DmkWeb.ConexaoRemota(Dmod.MyDBn, DmodG.QrWebParams, 1) then
  begin
    if DBCheck.CriaFm(TFmWOrdSerAss, FmWOrdSerAss, afmoNegarComAviso) then
    begin
      if Codigo > 0 then FmWOrdSerAss.LocCod(Codigo, Codigo);
      FmWOrdSerAss.ShowModal;
      FmWOrdSerAss.Destroy;
    end;
  end;
end;

procedure TDModWOrdSer.MostraWOrdSerAssGru(Codigo: Integer);
begin
  if DmkWeb.ConexaoRemota(Dmod.MyDBn, DmodG.QrWebParams, 1) then
  begin
    if DBCheck.CriaFm(TFmWOrdSerAssGru, FmWOrdSerAssGru, afmoNegarComAviso) then
    begin
      if Codigo <> 0 then
        FmWOrdSerAssGru.LocCod(Codigo, Codigo);
      FmWOrdSerAssGru.ShowModal;
      FmWOrdSerAssGru.Destroy;
    end;
  end;
end;

procedure TDModWOrdSer.MostraWOrdSerAte;
begin
  if DmkWeb.ConexaoRemota(Dmod.MyDBn, DmodG.QrWebParams, 1) then
  begin
    if DBCheck.CriaFm(TFmWOrdSerAte, FmWOrdSerAte, afmoNegarComAviso) then
    begin
      FmWOrdSerAte.ShowModal;
      FmWOrdSerAte.Destroy;
    end;
  end;
end;

procedure TDModWOrdSer.MostraWOrdSerAtePar(Codigo: Integer);
begin
  if DmkWeb.ConexaoRemota(Dmod.MyDBn, DmodG.QrWebParams, 1) then
  begin
    if DBCheck.CriaFm(TFmWOrdSerAtePar, FmWOrdSerAtePar, afmoNegarComAviso) then
    begin
      FmWOrdSerAtePar.FCodigo := Codigo;
      FmWOrdSerAtePar.ShowModal;
      FmWOrdSerAtePar.Destroy;
    end;
  end;
end;

procedure TDModWOrdSer.MostraWOrdSerOpc;
begin
  if DmkWeb.ConexaoRemota(Dmod.MyDBn, DmodG.QrWebParams, 1) then
  begin
    if DBCheck.CriaFm(TFmWOrdSerOpc, FmWOrdSerOpc, afmoNegarComAviso) then
    begin
      FmWOrdSerOpc.ShowModal;
      FmWOrdSerOpc.Destroy;
    end;
  end;
end;

procedure TDModWOrdSer.MostraWOrdSerCom(SQLType: TSQLType; Codigo,
  Controle, Entidade, Solicitante, ComRes: Integer; Nome: String);
begin
  if not DmkWeb.ConectarUsuarioWEB(True) then Exit;
  //
  if (Entidade <> VAR_WEB_USR_ID) and (SQLType = stUpd) then
  begin
    Geral.MB_Aviso('Voc� pode alterar apenas seus coment�rios!');
    Exit;
  end;
  //
  if DmkWeb.ConexaoRemota(Dmod.MyDBn, DmodG.QrWebParams, 1) then
  begin
    if DBCheck.CriaFm(TFmWOrdSerCom, FmWOrdSerCom, afmoNegarComAviso) then
    begin
      FmWOrdSerCom.ImgTipo.SQLType := SQLType;
      FmWOrdSerCom.FCodigo         := Codigo;
      FmWOrdSerCom.FControle       := Controle;
      FmWOrdSerCom.FSolicitante    := Solicitante;
      FmWOrdSerCom.FEntidade       := Entidade;
      FmWOrdSerCom.FNome           := Nome;
      FmWOrdSerCom.FComRes         := ComRes;
      FmWOrdSerCom.ShowModal;
      FmWOrdSerCom.Destroy;
    end;
  end;
end;

procedure TDModWOrdSer.MostraWOrdSerEnc(Codigo, Finaliz, Assunto: Integer;
  Descri: String; Abertura: TDateTime; Encerrou: Boolean);
begin
  if DmkWeb.ConexaoRemota(Dmod.MyDBn, DmodG.QrWebParams, 1) then
  begin
    if DBCheck.CriaFm(TFmWOrdSerEnc, FmWOrdSerEnc, afmoNegarComAviso) then
    begin
      FmWOrdSerEnc.FCodigo   := Codigo;
      FmWOrdSerEnc.FFinaliz  := Finaliz;
      FmWOrdSerEnc.FEncerrou := Encerrou;
      FmWOrdSerEnc.FAssunto  := Assunto;
      FmWOrdSerEnc.FDescri   := Descri;
      FmWOrdSerEnc.FAbertura := Abertura;
      FmWOrdSerEnc.ShowModal;
      FmWOrdSerEnc.Destroy;
    end;
  end;
end;

procedure TDModWOrdSer.MostraWOrdSerIts(SQLType: TSQLType;
  QueryWOrdSer: TmySQLQuery; Mostra: Boolean; Grupo: Integer = 0;
  Status: Integer = 0; DadosUsuario: Boolean = False);
var
  Codigo: Integer;
begin
  if (DmkWeb.ConexaoRemota(Dmod.MyDBn, DmodG.QrWebParams, 1)) and
    (DmkWeb.ConectarUsuarioWEB(True)) then
  begin
    if SQLType = stIns then
      Codigo := 0
    else
      Codigo := QueryWOrdSer.FieldByName('Codigo').AsInteger;
    //
    if DBCheck.CriaFm(TFmWOrdSerIts, FmWOrdSerIts, afmoNegarComAviso) then
    begin
      FmWOrdSerIts.ImgTipo.SQLType := SQLType;
      FmWOrdSerIts.FCodigo         := Codigo;
      FmWOrdSerIts.FDadosUsuario   := DadosUsuario;
      FmWOrdSerIts.FGrupo          := Grupo;
      FmWOrdSerIts.FStatus         := Status;
      FmWOrdSerIts.FMostra         := Mostra;
      FmWOrdSerIts.FQrWOrdSer      := QueryWOrdSer;
      FmWOrdSerIts.ShowModal;
      FmWOrdSerIts.Destroy;
    end;
  end;
end;

procedure TDModWOrdSer.MostraWOrdSerMod(Codigo: Integer);
begin
  VAR_CADASTRO := 0;
  //
  if DmkWeb.ConexaoRemota(Dmod.MyDBn, DmodG.QrWebParams, 1) then
  begin
    if DBCheck.CriaFm(TFmWOrdSerMod, FmWOrdSerMod, afmoNegarComAviso) then
    begin
      if Codigo > 0 then FmWOrdSerMod.LocCod(Codigo, Codigo);
      FmWOrdSerMod.ShowModal;
      FmWOrdSerMod.Destroy;
    end;
  end;
end;

procedure TDModWOrdSer.MostraWOrdSerSol(Codigo: Integer);
begin
  if DmkWeb.ConexaoRemota(Dmod.MyDBn, DmodG.QrWebParams, 1) then
  begin
    if DBCheck.CriaFm(TFmWOrdSerSol, FmWOrdSerSol, afmoNegarComAviso) then
    begin
      if Codigo <> 0 then
        FmWOrdSerSol.LocCod(Codigo, Codigo);
      FmWOrdSerSol.ShowModal;
      FmWOrdSerSol.Destroy;
    end;
  end;
end;

procedure TDModWOrdSer.MostraWOrdSerSta(Codigo: Integer);
begin
  if DmkWeb.ConexaoRemota(Dmod.MyDBn, DmodG.QrWebParams, 1) then
  begin
    if DBCheck.CriaFm(TFmWOrdSerSta, FmWOrdSerSta, afmoNegarComAviso) then
    begin
      if Codigo > 0 then FmWOrdSerSta.LocCod(Codigo, Codigo);
      FmWOrdSerSta.ShowModal;
      FmWOrdSerSta.Destroy;
    end;
  end;
end;

function TDModWOrdSer.AlteraStatus(Codigo: Integer; Grade: TDBGrid;
  Query: TmySQLQuery): Boolean;
begin
  Result := False;
  //
  if DmkWeb.ConexaoRemota(Dmod.MyDBn, DmodG.QrWebParams, 1) then
  begin
    if DBCheck.CriaFm(TFmWOrdSerStaGer, FmWOrdSerStaGer, afmoNegarComAviso) then
    begin
      FmWOrdSerStaGer.FCodigo := Codigo;
      FmWOrdSerStaGer.FGrade  := Grade;
      FmWOrdSerStaGer.FQuery  := Query;
      FmWOrdSerStaGer.ShowModal;
      //
      Result := FmWOrdSerStaGer.FReabre;
      //
      FmWOrdSerStaGer.Destroy;
    end;
  end;
end;

function TDModWOrdSer.AlteraAssunto(Codigo: Integer; Grade: TDBGrid;
  Query: TmySQLQuery): Boolean;
const
  Aviso  = '...';
  Titulo = 'Sele��o de assunto';
  Prompt = 'Informe o assunto: [F7 para pesquisar]';
  Campo  = 'Descricao';
var
  Assunto: Variant;
begin
  Result := False;
  //
  Assunto := DBCheck.EscolheCodigoUnico(Aviso, Titulo, Prompt, nil, nil, Campo, 0, [
    'SELECT Codigo, Nome Descricao ',
    'FROM wordserass ',
    'WHERE Ativo = 1 ',
    'ORDER BY Nome ',
    ''], Dmod.MyDBn, True);
  //
  if (Assunto <> Null) and (Codigo <> 0) then
  begin
    if UMyMod.SQLInsUpd(Dmod.QrUpdN, stUpd, 'wordser', False, ['Assunto'],
      ['Codigo'], [Assunto], [Codigo], False)
    then
      Result := True;
  end;
end;

procedure TDModWOrdSer.MostraWOrdSerAteParGr(Codigo: Integer);
begin
  if DmkWeb.ConexaoRemota(Dmod.MyDBn, DmodG.QrWebParams, 1) then
  begin
    if DBCheck.CriaFm(TFmWOrdSerAteParGr, FmWOrdSerAteParGr, afmoNegarComAviso) then
    begin
      if Codigo > 0 then
        FmWOrdSerAteParGr.LocCod(Codigo, Codigo);
      FmWOrdSerAteParGr.ShowModal;
      FmWOrdSerAteParGr.Destroy;
    end;
  end;
end;

(*
Antigo Usar o ObtemAtendente3
function TDModWOrdSer.ObtemAtendente(Aplicativo, Solicitante, Cliente, Assunto: Integer): Integer;
  function VerificaAtendentePeloParametro(TipParam, Parametro: Integer): Integer;
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrWOrSeAteIt, Dmod.MyDBn, [
    'SELECT ate.Atendente, ',
    '( ',
    'SELECT COUNT(Codigo) ',
    'FROM wordser ',
    'WHERE Finaliz = 0 ',
    'AND Respons = ate.Atendente ',
    ') OSAberta ',
    'FROM worseateit ati ',
    'LEFT JOIN worseate ate ON ate.Codigo = ati.Codigo ',
    'WHERE ati.TipParam=' + Geral.FF0(TipParam),
    'AND ati.Parametro=' + Geral.FF0(Parametro),
    'ORDER BY OSAberta ',
    'LIMIT 1 ',
    '']);
    if QrWOrSeAteIt.RecordCount > 0 then
      Result := QrWOrSeAteIt.FieldByName('Atendente').AsInteger
    else
      Result := 0;
  end;
var
  Atendente: Integer;
begin
  Atendente := 0;
  if Aplicativo <> 0 then //Aplicativo
  begin
    if CO_DMKID_APP = 17 then //DControl
      Atendente := VerificaAtendentePeloParametro(0, Aplicativo)
    else
      Atendente := 0;
  end;
  if (Atendente = 0) and (Solicitante <> 0) then //Solicitante
    Atendente := VerificaAtendentePeloParametro(1, Solicitante);
  if (Atendente = 0) and (Cliente <> 0) then //Cliente
    Atendente := VerificaAtendentePeloParametro(2, Cliente);
  if (Atendente = 0) and (Assunto <> 0) then //Assunto
    Atendente := VerificaAtendentePeloParametro(3, Assunto);
  //
  Result := Atendente;
end;
*)

procedure TDModWOrdSer.QrCliCalcFields(DataSet: TDataSet);
begin
  QrCliTE1_TXT.Value :=
    MLAGeral.FormataTelefone_TT_Curto(QrCliTe1.Value);
  QrCliFAX_TXT.Value :=
    MLAGeral.FormataTelefone_TT_Curto(QrCliFax.Value);
  QrCliCNPJ_TXT.Value :=
    Geral.FormataCNPJ_TT(QrCliCNPJ_CPF.Value);
  QrCliIE_TXT.Value :=
    Geral.Formata_IE(QrCliIE_RG.Value, QrCliUF.Value, '??', QrCliTipo.Value);
  QrCliNUMERO_TXT.Value :=
    MLAGeral.FormataNumeroDeRua(QrCliRUA.Value, QrCliNumero.Value, False);
  //
  QrCliE_ALL.Value := UpperCase(QrCliNOMELOGRAD.Value);
  if Trim(QrCliE_ALL.Value) <> '' then QrCliE_ALL.Value :=
    QrCliE_ALL.Value + ' ';
  QrCliE_ALL.Value := QrCliE_ALL.Value + Uppercase(QrCliRua.Value);
  if Trim(QrCliRua.Value) <> '' then QrCliE_ALL.Value :=
    QrCliE_ALL.Value + ', ' + QrCliNUMERO_TXT.Value;
  if Trim(QrCliCompl.Value) <>  '' then QrCliE_ALL.Value :=
    QrCliE_ALL.Value + ' ' + Uppercase(QrCliCompl.Value);
  if Trim(QrCliBairro.Value) <>  '' then QrCliE_ALL.Value :=
    QrCliE_ALL.Value + ' - ' + Uppercase(QrCliBairro.Value);
  if QrCliCEP.Value > 0 then QrCliE_ALL.Value :=
    QrCliE_ALL.Value + ' CEP ' + Geral.FormataCEP_NT(QrCliCEP.Value);
  if Trim(QrCliCidade.Value) <>  '' then QrCliE_ALL.Value :=
    QrCliE_ALL.Value + ' - ' + Uppercase(QrCliCidade.Value);
  if Trim(QrCliNOMEUF.Value) <>  '' then QrCliE_ALL.Value :=
    QrCliE_ALL.Value + ', ' + QrCliNOMEUF.Value;
  if Trim(QrCliPais.Value) <>  '' then QrCliE_ALL.Value :=
    QrCliE_ALL.Value + ' - ' + QrCliPais.Value;
end;

procedure TDModWOrdSer.ReopenAplicativos(Cliente: Integer);
  function SQLReopenAplicativos(Tipo, Cliente: Integer): String;
  var
    SQLTxt: String;
  begin
    if Cliente <> 0 then
    begin
      if Tipo in [2, 3] then
      begin
        SQLTxt := 'SELECT apl.Codigo, apl.Nome ' +
                  'FROM cliaplic cap ' +
                  'LEFT JOIN aplicativos apl ON apl.Codigo = cap.Aplicativo ' +
                  'WHERE cap.Codigo = "'+ Geral.FF0(Cliente) +'" ' +
                  'ORDER BY apl.Nome';
      end else
      begin
        SQLTxt := 'SELECT apl.Codigo, apl.Nome ' +
                  'FROM cliaplic cap ' +
                  'LEFT JOIN clientes cli ON cli.Codigo = cap.Codigo ' +
                  'LEFT JOIN aplicativos apl ON apl.Codigo = cap.Aplicativo ' +
                  'WHERE cli.Cliente = "'+ Geral.FF0(Cliente) +'" ' +
                  'ORDER BY apl.Nome';
      end
    end else
    begin
      SQLTxt := 'SELECT Codigo, Nome ' +
                'FROM aplicativos ' +
                'WHERE Ativo = 1 ' +
                'ORDER BY Nome';
    end;
    Result := SQLTxt;
  end;
var
  Tipo, Cli: Integer;
begin
  Tipo := VAR_WEB_USR_TIPO;
  Cli  := Cliente;
  //
  if Tipo in [2, 3] then
    Cli := VAR_WEB_CLIUSER;
  QrAplicativo.Close;
  QrAplicativo.SQL.Clear;
  QrAplicativo.SQL.Add(SQLReopenAplicativos(Tipo, Cli));
  QrAplicativo.Open;
  if Tipo in [0, 1] then
  begin
    if QrAplicativo.RecordCount = 0 then
    begin
      QrAplicativo.Close;
      QrAplicativo.SQL.Clear;
      QrAplicativo.SQL.Add(SQLReopenAplicativos(0, 0));
      QrAplicativo.Open;
    end;
  end;
end;

procedure TDModWOrdSer.ReopenCli(Cliente: Integer);
begin
  QrCli.Close;
  QrCli.Params[0].AsInteger := Cliente;
  UMyMod.AbreQuery(QrCli, DMod.MyDBn);
end;

procedure TDModWOrdSer.ReopenClientes;
var
  Tipo, Entidade: Integer;
begin
  Tipo := VAR_WEB_USR_TIPO;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrClientes, Dmod.MyDB, [
    'SELECT Codigo, ',
    'IF (Tipo=0, RazaoSocial, Nome) Nome ',
    'FROM entidades ',
    'WHERE Ativo = 1 ',
    'AND Cliente1 = "V" ',
    'ORDER BY Nome ',
    '']);
  //dmkPF.LeMeuTexto(QrClientes.SQL.Text);
end;

procedure TDModWOrdSer.ReopenFTPDir(Codigo: Integer);
begin
  QrFTPDir.Close;
  QrFTPDir.Params[0].AsInteger := Codigo;
  UMyMod.AbreQuery(QrFTPDir, DMod.MyDBn);
end;

procedure TDModWOrdSer.ReopenRequisitante;
begin
  UMyMod.AbreQuery(QrRequisitante, DMod.MyDBn);
end;

procedure TDModWOrdSer.ReopenRespons;
begin
  UMyMod.AbreQuery(QrResp, DMod.MyDBn);
end;

procedure TDModWOrdSer.ReopenWOrdSerAss();
begin            
  UnDmkDAC_PF.AbreMySQLQuery0(QrWOrdSerAss, Dmod.MyDBn, [
    'SELECT Codigo, Nome, Descri ',
    'FROM wordserass ',
    'WHERE Ativo = 1 ',
    'ORDER BY Nome ',
    '']);
end;

procedure TDModWOrdSer.ReopenWOrdSerGruPes;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrWOrdSerGruPes, Dmod.MyDBn, [
    'SELECT Codigo, Nome ',
    'FROM wordsergru ',
    'WHERE Ativo = 1 ',
    'ORDER BY Nome ',
    '']);
end;

procedure TDModWOrdSer.ReopenWOrdSerMod();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrWOrdSerMod, Dmod.MyDBn, [
    'SELECT Codigo, Nome ',
    'FROM wordsermod ',
    'WHERE Ativo = 1 ',
    'ORDER BY Nome ',
    '']);
end;

procedure TDModWOrdSer.ReopenWOrdSerOpc;
begin
  UMyMod.AbreQuery(QrWOrdSerOpc, DMod.MyDBn);
end;

procedure TDModWOrdSer.ReopenWOrdSerPri;
begin
  UMyMod.AbreQuery(QrWOrdSerPri, DMod.MyDBn);
end;

procedure TDModWOrdSer.ReopenWOrdSerSta(Nivel, Grupo: Integer);
var
  SQLNivel, SQLGrupo: String;
  Status1, Status2, Status3, Status4: Integer;
begin
  if Grupo <> 0 then
  begin
    ReopenWOrdSerOpc;
    //
    Status1 := QrWOrdSerOpc.FieldByName('GruStatus1').AsInteger;
    Status2 := QrWOrdSerOpc.FieldByName('GruStatus2').AsInteger;
    Status3 := QrWOrdSerOpc.FieldByName('GruStatus3').AsInteger;
    Status4 := QrWOrdSerOpc.FieldByName('GruStatus4').AsInteger;
    //
    SQLGrupo := 'AND Codigo IN (' + Geral.FF0(Status1) + ', ' +
                  Geral.FF0(Status2) + ', ' + Geral.FF0(Status3) + ', ' +
                  Geral.FF0(Status4) + ')';
  end else
    SQLGrupo := '';
  //
  if Nivel <> -1 then
  begin
    SQLNivel := 'AND (' + DmkWeb.VerificaSQLNiveis(Nivel, 'Nivel') +
                  ' OR Nivel & "1")';
  end else
    SQLNivel := '';
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrWOrdSerSta, Dmod.MyDBn, [
    'SELECT Codigo, Nome',
    'FROM wordsersta',
    'WHERE Ativo = 1',
    SQLNivel,
    SQLGrupo,
    'ORDER BY Nome',
    '']);
end;

procedure TDModWOrdSer.ReopenWOSOpcUsu(Usuario: Integer; Modo: TOpcUser);
begin
  case modo of
    istApenasUsuario:
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(QrWOSOpcUsu, Dmod.MyDBn, [
        'SELECT * ',
        'FROM wosopcusu ',
        'WHERE Usuario=' + Geral.FF0(Usuario),
        '']);
    end;
    istApenasPadrao:
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(QrWOSOpcUsu, Dmod.MyDBn, [
        'SELECT * ',
        'FROM wosopcusu ',
        'WHERE Usuario = 0',
        '']);
    end;
    istTodos:
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(QrWOSOpcUsu, Dmod.MyDBn, [
        'SELECT * ',
        'FROM wosopcusu ',
        'WHERE Usuario=' + Geral.FF0(Usuario),
        '']);
      if QrWOSOpcUsu.RecordCount = 0 then
      begin
        UnDmkDAC_PF.AbreMySQLQuery0(QrWOSOpcUsu, Dmod.MyDBn, [
          'SELECT * ',
          'FROM wosopcusu ',
          'WHERE Usuario = 0',
          '']);
      end;
    end;
  end;
end;

procedure TDModWOrdSer.ReopenWUsers(Entidade: Integer);
begin
  QrWUsers.Close;
  QrWUsers.Params[0].AsInteger := Entidade;
  UMyMod.AbreQuery(QrWUsers, DMod.MyDBn);
end;

procedure TDModWOrdSer.SetaVersoesAtuais(Aplicativo: Integer; Combo: TComboBox);
begin
  if Aplicativo <> 0 then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrLoc, Dmod.MyDBn, [
      'SELECT Versao ',
      'FROM wversao ',
      'WHERE Aplicativo=' + Geral.FF0(Aplicativo),
      'ORDER BY Versao DESC ',
      '']);
    Combo.Items.Clear;
    //
    if QrLoc.RecordCount > 0 then
    begin
      while not QrLoc.Eof do
      begin
        Combo.Items.Add(QrLoc.FieldByName('Versao').AsString);
        //
        QrLoc.Next;
      end;
    end;
    QrLoc.Close;
  end;
end;

function TDModWOrdSer.VerificaMaxData(Codigo, Controle: Integer; Abertura, Data: TDateTime): Boolean;
begin
  Dmod.QrUpdN.Close;
  Dmod.QrUpdN.SQL.Clear;
  Dmod.QrUpdN.SQL.Add('SELECT MAX(DataHora) Data');
  Dmod.QrUpdN.SQL.Add('FROM wordsercom');
  Dmod.QrUpdN.SQL.Add('WHERE Codigo=:P0');
  if Controle > 0 then
    Dmod.QrUpdN.SQL.Add('AND Controle<>:P1');
  Dmod.QrUpdN.Params[0].AsInteger := Codigo;
  if Controle > 0 then
    Dmod.QrUpdN.Params[1].AsInteger := Controle;
  Dmod.QrUpdN.Open;
  if Dmod.QrUpdN.FieldByName('Data').AsDateTime > 0 then
  begin
    if Dmod.QrUpdN.FieldByName('Data').AsDateTime > Data then
      Result := False
    else
      Result := True;
  end else
  begin
    if Abertura > Data then
      Result := False
    else
      Result := True;
  end;
end;

function TDModWOrdSer.VerificaMaxHistorico(const Responsavel: Integer; var Controle, Usuario: Integer): Boolean;
var
  SQL: String;
begin
  Result := False;
  try
    if Responsavel <> 0 then
      SQL := 'WHERE ser.Respons=' + Geral.FF0(Responsavel)
    else
      SQL := '';
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrLoc, Dmod.MyDBn, [
    'SELECT his.Controle MaxControle, ',
    'CASE his.Tarefa ',
    'WHEN 0 THEN ',
    '( ',
    'SELECT UserCad ',
    'FROM wordser ',
    'WHERE Codigo = his.Codigo ',
    ') ',
    'WHEN 1 THEN ',
    '( ',
    'SELECT UserAlt ',
    'FROM wordser ',
    'WHERE Codigo = his.Codigo ',
    ') ',
    'WHEN 2 THEN ',
    '( ',
    'SELECT UserCad ',
    'FROM wordsercom ',
    'WHERE Controle = his.CodOrig ',
    ') ',
    'WHEN 3 THEN ',
    '( ',
    'SELECT UserAlt ',
    'FROM wordser ',
    'WHERE Codigo = his.Codigo ',
    ') ',
    'END UsuarioEdit ',
    'FROM wordserhis his ',
    'WHERE his.Controle = ',
    '( ',
    'SELECT MAX(his.Controle) MaxControle ',
    'FROM wordserhis his ',
    'LEFT JOIN wordser ser ON ser.Codigo = his.Codigo ',
    SQL,
    ') ',
    '']);
    if QrLoc.RecordCount > 0 then
    begin
      Result   := True;
      Controle := QrLoc.FieldByName('MaxControle').AsInteger;
      Usuario  := QrLoc.FieldByName('UsuarioEdit').AsInteger;
    end else
    begin
      Result   := False;
      Controle := 0;
      Usuario  := 0;
    end;
  except
    Geral.MB_Aviso('Falha ao verificar solicita��es no site!' + sLineBreak +
      'Atualize o as solicita��es manualmente clicando no bot�o reabre.');
  end;
end;

function TDModWOrdSer.VerificaSeEnviaEmailAtendente(Atendente: Integer): Boolean;
begin
  Result := False;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrLoc, Dmod.MyDBn, [
  'SELECT ReceMail ',
  'FROM worseate ',
  'WHERE Atendente=' + Geral.FF0(Atendente),
  '']);
  if QrLoc.RecordCount > 0 then
    Result := Geral.IntToBool(QrLoc.FieldByName('ReceMail').AsInteger);
end;

function TDModWOrdSer.ObtemAtendente3(Aplicativo, Solicitante, Cliente, Assunto,
  Cargo: Integer): Integer;


  function VerificaAtendenteSemParametros(): Integer;
  begin
    if Cargo <> 0 then
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(QrWOrSeAteIt, Dmod.MyDBn, [
        'SELECT ate.Atendente, ',
        '( ',
        'SELECT COUNT(Codigo) ',
        'FROM wordser ',
        'WHERE Finaliz = 0 ',
        'AND Respons = ate.Atendente ',
        ') OSAberta ',
        'FROM worseate ate ',
        'LEFT JOIN worseateca wca ON wca.Codigo = ate.Codigo ',
        'WHERE wca.Grupo=' + Geral.FF0(Cargo),
        'GROUP BY ate.Codigo ',
        'ORDER BY OSAberta ASC ',
        'LIMIT 1',
        '']);
    end else
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(QrWOrSeAteIt, Dmod.MyDBn, [
        'SELECT ate.Atendente, ',
        '( ',
        'SELECT COUNT(Codigo) ',
        'FROM wordser ',
        'WHERE Finaliz = 0 ',
        'AND Respons = ate.Atendente ',
        ') OSAberta ',
        'FROM worseate ate ',
        'ORDER BY OSAberta ASC ',
        'LIMIT 1',
        '']);
    end;
    if QrWOrSeAteIt.RecordCount > 0 then
      Result := QrWOrSeAteIt.FieldByName('Atendente').AsInteger
    else
      Result := 0;
  end;

  function VerificaAtendentePeloParametro(SQLWHERE: String): Integer;
  begin
    if Cargo <> 0 then
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(QrWOrSeAteIt, Dmod.MyDBn, [
        'SELECT ate.Atendente, ',
        '( ',
        'SELECT COUNT(Codigo) ',
        'FROM wordser ',
        'WHERE Finaliz = 0 ',
        'AND Respons = ate.Atendente ',
        ') OSAberta ',
        'FROM worseateit ati ',
        'LEFT JOIN worseate ate ON ate.Codigo = ati.Codigo ',
        'LEFT JOIN worseateca wca ON wca.Codigo = ate.Codigo ',
        SQLWHERE,
        'AND wca.Grupo=' + Geral.FF0(Cargo),
        'GROUP BY ate.Codigo ',
        'ORDER BY OSAberta ASC ',
        'LIMIT 1 ',
        '']);
    end else
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(QrWOrSeAteIt, Dmod.MyDBn, [
        'SELECT ate.Atendente, ',
        '( ',
        'SELECT COUNT(Codigo) ',
        'FROM wordser ',
        'WHERE Finaliz = 0 ',
        'AND Respons = ate.Atendente ',
        ') OSAberta ',
        'FROM worseateit ati ',
        'LEFT JOIN worseate ate ON ate.Codigo = ati.Codigo ',
        SQLWHERE,
        'ORDER BY OSAberta ASC ',
        'LIMIT 1 ',
        '']);
    end;
    if QrWOrSeAteIt.RecordCount > 0 then
      Result := QrWOrSeAteIt.FieldByName('Atendente').AsInteger
    else
      Result := 0;
  end;

var
  Atendente: Integer;
  SQL: String;
begin
  Atendente := 0;
  SQL       := '';
  //
  //4 par�metros - Ini
  if (Aplicativo <> 0) and (Solicitante <> 0) and (Cliente <> 0) and (Assunto <> 0) then
  begin
    SQL := 'WHERE ati.Aplicativo=' + Geral.FF0(Aplicativo) + ' ';
    SQL := SQL + 'AND ati.Solicit=' + Geral.FF0(Solicitante) + ' ';
    SQL := SQL + 'AND ati.Cliente=' + Geral.FF0(Cliente) + ' ';
    SQL := SQL + 'AND ati.Assunto=' + Geral.FF0(Assunto) + ' ';
    //
    Atendente := VerificaAtendentePeloParametro(SQL);
  end;
  //4 par�metros - Fim
  //3 par�metros - Ini
  if (Atendente = 0) and (Solicitante <> 0) and (Cliente <> 0) and (Assunto <> 0) then
  begin
    SQL := 'WHERE ati.Solicit=' + Geral.FF0(Solicitante) + ' ';
    SQL := SQL + 'AND ati.Cliente=' + Geral.FF0(Cliente) + ' ';
    SQL := SQL + 'AND ati.Assunto=' + Geral.FF0(Assunto) + ' ';
    SQL := SQL + 'AND ati.Aplicativo=0 ';
    //
    Atendente := VerificaAtendentePeloParametro(SQL);
  end;
  if (Atendente = 0) and (Aplicativo <> 0) and (Cliente <> 0) and (Assunto <> 0) then
  begin
    SQL := 'WHERE ati.Aplicativo=' + Geral.FF0(Aplicativo) + ' ';
    SQL := SQL + 'AND ati.Cliente=' + Geral.FF0(Cliente) + ' ';
    SQL := SQL + 'AND ati.Assunto=' + Geral.FF0(Assunto) + ' ';
    SQL := SQL + 'AND ati.Solicit=0 ';
    //
    Atendente := VerificaAtendentePeloParametro(SQL);
  end;
  if (Atendente = 0) and (Aplicativo <> 0) and (Solicitante <> 0) and (Assunto <> 0) then
  begin
    SQL := 'WHERE ati.Aplicativo=' + Geral.FF0(Aplicativo) + ' ';
    SQL := SQL + 'AND ati.Solicit=' + Geral.FF0(Solicitante) + ' ';
    SQL := SQL + 'AND ati.Assunto=' + Geral.FF0(Assunto) + ' ';
    SQL := SQL + 'AND ati.Cliente=0 ';
    //
    Atendente := VerificaAtendentePeloParametro(SQL);
  end;
  if (Atendente = 0) and (Aplicativo <> 0) and (Solicitante <> 0) and (Cliente <> 0) then
  begin
    SQL := 'WHERE ati.Aplicativo=' + Geral.FF0(Aplicativo) + ' ';
    SQL := SQL + 'AND ati.Solicit=' + Geral.FF0(Solicitante) + ' ';
    SQL := SQL + 'AND ati.Cliente=' + Geral.FF0(Cliente) + ' ';
    SQL := SQL + 'AND ati.Assunto=0 ';
    //
    Atendente := VerificaAtendentePeloParametro(SQL);
  end;
  //3 par�metros - Fim
  //2 par�metros - Ini
  if (Atendente = 0) and (Cliente <> 0) and (Assunto <> 0) then
  begin
    SQL := 'WHERE ati.Cliente=' + Geral.FF0(Cliente) + ' ';
    SQL := SQL + 'AND ati.Assunto=' + Geral.FF0(Assunto) + ' ';
    SQL := SQL + 'AND ati.Solicit=0 ';
    SQL := SQL + 'AND ati.Aplicativo=0 ';
    //
    Atendente := VerificaAtendentePeloParametro(SQL);
  end;
  if (Atendente = 0) and (Solicitante <> 0) and (Assunto <> 0) then
  begin
    SQL := 'WHERE ati.Solicit=' + Geral.FF0(Solicitante) + ' ';
    SQL := SQL + 'AND ati.Assunto=' + Geral.FF0(Assunto) + ' ';
    SQL := SQL + 'AND ati.Cliente=0 ';
    SQL := SQL + 'AND ati.Aplicativo=0 ';
    //
    Atendente := VerificaAtendentePeloParametro(SQL);
  end;
  if (Atendente = 0) and (Aplicativo <> 0) and (Assunto <> 0) then
  begin
    SQL := 'WHERE ati.Aplicativo=' + Geral.FF0(Aplicativo) + ' ';
    SQL := SQL + 'AND ati.Assunto=' + Geral.FF0(Assunto) + ' ';
    SQL := SQL + 'AND ati.Cliente=0 ';
    SQL := SQL + 'AND ati.Solicit=0 ';
    //
    Atendente := VerificaAtendentePeloParametro(SQL);
  end;
  if (Atendente = 0) and (Solicitante <> 0) and (Cliente <> 0) then
  begin
    SQL := 'WHERE ati.Solicit=' + Geral.FF0(Solicitante) + ' ';
    SQL := SQL + 'AND ati.Cliente=' + Geral.FF0(Cliente) + ' ';
    SQL := SQL + 'AND ati.Assunto=0 ';
    SQL := SQL + 'AND ati.Aplicativo=0 ';
    //
    Atendente := VerificaAtendentePeloParametro(SQL);
  end;
  if (Atendente = 0) and (Aplicativo <> 0) and (Cliente <> 0) then
  begin
    SQL := 'WHERE ati.Aplicativo=' + Geral.FF0(Aplicativo) + ' ';
    SQL := SQL + 'AND ati.Cliente=' + Geral.FF0(Cliente) + ' ';
    SQL := SQL + 'AND ati.Solicit=0 ';
    SQL := SQL + 'AND ati.Assunto=0 ';
    //
    Atendente := VerificaAtendentePeloParametro(SQL);
  end;
  if (Atendente = 0) and (Aplicativo <> 0) and (Solicitante <> 0) then
  begin
    SQL := 'WHERE ati.Aplicativo=' + Geral.FF0(Aplicativo) + ' ';
    SQL := SQL + 'AND ati.Solicit=' + Geral.FF0(Solicitante) + ' ';
    SQL := SQL + 'AND ati.Assunto=0 ';
    SQL := SQL + 'AND ati.Cliente=0 ';
    //
    Atendente := VerificaAtendentePeloParametro(SQL);
  end;
  //2 par�metros - Ini
  //1 par�metros - Ini
  if (Atendente = 0) and (Solicitante <> 0) then
  begin
    SQL := 'WHERE ati.Solicit=' + Geral.FF0(Solicitante) + ' ';
    SQL := SQL + 'AND ati.Assunto=0 ';
    SQL := SQL + 'AND ati.Cliente=0 ';
    SQL := SQL + 'AND ati.Aplicativo=0 ';
    //
    Atendente := VerificaAtendentePeloParametro(SQL);
  end;
  if (Atendente = 0) and (Cliente <> 0) then
  begin
    SQL := 'WHERE ati.Cliente=' + Geral.FF0(Cliente) + ' ';
    SQL := SQL + 'AND ati.Solicit=0 ';
    SQL := SQL + 'AND ati.Assunto=0 ';
    SQL := SQL + 'AND ati.Aplicativo=0 ';
    //
    Atendente := VerificaAtendentePeloParametro(SQL);
  end;
  if (Atendente = 0) and (Aplicativo <> 0) then
  begin
    SQL := 'WHERE ati.Aplicativo=' + Geral.FF0(Aplicativo) + ' ';
    SQL := SQL + 'AND ati.Cliente=0 ';
    SQL := SQL + 'AND ati.Solicit=0 ';
    SQL := SQL + 'AND ati.Assunto=0 ';
    //
    Atendente := VerificaAtendentePeloParametro(SQL);
  end;
  if (Atendente = 0) and (Assunto <> 0) then
  begin
    SQL := 'WHERE ati.Assunto=' + Geral.FF0(Assunto) + ' ';
    SQL := SQL + 'AND ati.Cliente=0 ';
    SQL := SQL + 'AND ati.Solicit=0 ';
    SQL := SQL + 'AND ati.Aplicativo=0 ';
    //
    Atendente := VerificaAtendentePeloParametro(SQL);
  end;
  //1 par�metros - Fim
  //0 par�metros - Ini
  if (Atendente = 0) then
  begin
    Atendente := VerificaAtendenteSemParametros;
  end;
  //0 par�metros - Fim
  //
  Result := Atendente;
end;

end.
