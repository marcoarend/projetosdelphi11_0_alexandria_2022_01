unit WOrdSerIts;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, dmkDBLookupComboBox, dmkEditCB, dmkEdit,
  dmkValUsu, Variants, dmkLabelRotate, dmkPermissoes, OleCtrls, SHDocVw,
  mySQLDbTables, MSHTML, UnDmkEnums, UnProjGroup_Consts;

type
  TFmWOrdSerIts = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    Panel6: TPanel;
    Label8: TLabel;
    MeDescri: TMemo;
    PnAplic: TPanel;
    LaAplicativo: TLabel;
    Label7: TLabel;
    EdAplicativo: TdmkEditCB;
    CBAplicativo: TdmkDBLookupComboBox;
    EdJanela: TdmkEdit;
    Panel7: TPanel;
    Label2: TLabel;
    SpeedButton7: TSpeedButton;
    Label4: TLabel;
    LaRespons: TLabel;
    SBStatus: TSpeedButton;
    LaStatus: TLabel;
    EdModo: TdmkEditCB;
    CBModo: TdmkDBLookupComboBox;
    EdPrior: TdmkEditCB;
    CBPrior: TdmkDBLookupComboBox;
    EdRespons: TdmkEditCB;
    CBStatus: TdmkDBLookupComboBox;
    EdStatus: TdmkEditCB;
    CkContinuar: TCheckBox;
    CBRespons: TdmkDBLookupComboBox;
    SBRespons: TSpeedButton;
    dmkPermissoes1: TdmkPermissoes;
    ImgWEB: TdmkImage;
    Label12: TLabel;
    EdCompoRel: TdmkEdit;
    EdJanelaRel: TdmkEdit;
    Label1: TLabel;
    PnBrowser: TPanel;
    Panel5: TPanel;
    Label9: TLabel;
    Label3: TLabel;
    LaSolicitante: TLabel;
    SBSolicitante: TSpeedButton;
    Label19: TLabel;
    SpeedButton4: TSpeedButton;
    EdCodigo: TdmkEdit;
    GBDataHora: TGroupBox;
    Label18: TLabel;
    Label17: TLabel;
    TPData: TDateTimePicker;
    TPHora: TDateTimePicker;
    EdCliente: TdmkEditCB;
    CBCliente: TdmkDBLookupComboBox;
    EdSolicitante: TdmkEditCB;
    CBSolicitante: TdmkDBLookupComboBox;
    EdAssunto: TdmkEditCB;
    CBAssunto: TdmkDBLookupComboBox;
    Label15: TLabel;
    EdImpacto: TdmkEdit;
    EdSistema: TdmkEdit;
    Label14: TLabel;
    CBVersaoApp: TComboBox;
    SpeedButton6: TSpeedButton;
    Label11: TLabel;
    DBLaPrior: TDBText;
    Label10: TLabel;
    WebBrowser1: TWebBrowser;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SBSolicitanteClick(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure SpeedButton7Click(Sender: TObject);
    procedure SBStatusClick(Sender: TObject);
    procedure SBResponsClick(Sender: TObject);
    procedure EdAssuntoChange(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure EdClienteChange(Sender: TObject);
    procedure EdAplicativoChange(Sender: TObject);
    procedure EdPriorChange(Sender: TObject);
    procedure EdGrupoChange(Sender: TObject);
    procedure WebBrowser1DocumentComplete(ASender: TObject;
      const pDisp: IDispatch; const URL: OleVariant);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    FReabre: Boolean;
    FDataHora: String;
    procedure MostraEdicao(Mostra: Boolean; SQLType: TSQLType; Codigo: Integer);
    procedure ConfiguraSolcitante(Cliente: Integer);
    procedure ConfiguraAplicativo(Cliente: Integer);
    procedure ConfiguraPriorAtendente();
  public
    { Public declarations }
    FCodigo, FGrupo, FStatus: Integer;
    FMostra, FDadosUsuario: Boolean;
    FQrWOrdSer: TmySQLQuery;
  end;

var
  FmWOrdSerIts: TFmWOrdSerIts;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, MyListas, UnGrl_Vars,
  {$IfnDef SWUsr}UnWUsersJan, {$EndIf}
  ModWOrdSer, UnDmkWeb, UMySQLModule, MyDBCheck, ModuleGeral;

{$R *.DFM}

procedure TFmWOrdSerIts.BtOKClick(Sender: TObject);
var
  Codigo, Cliente, Solicit, Assunto, Aplic, Modo, Prioridade, Status, Respons,
  Grupo, Resul, CodigoReturn: Integer;
  DataHora, Descri, VersaoApp, Janela, JanelaRel, CompoRel,
  Sistema, Impacto, CadAltWeb, Mensagem: String;
begin
  if not DmkWeb.ConectarUsuarioWEB(True) then Exit;
  //
  //DataHora em TimeZone local
  //
  DataHora   := Geral.FDT(Int(TPData.Date) + TPHora.Time - Int(TPHora.Time), 9);
  Cliente    := EdCliente.ValueVariant;
  Solicit    := EdSolicitante.ValueVariant;
  Assunto    := EdAssunto.ValueVariant;
  Descri     := MeDescri.Text;
  Sistema    := EdSistema.ValueVariant;
  Impacto    := EdImpacto.ValueVariant;
  Modo       := EdModo.ValueVariant;
  Prioridade := EdPrior.ValueVariant;
  Status     := EdStatus.ValueVariant;
  Respons    := EdRespons.ValueVariant;
  Grupo      := FGrupo;
  Mensagem   := '';
  //
  if MyObjects.FIC(TPData.Date = 0, TPData, 'Data n�o definida!') then Exit;
  if MyObjects.FIC(TPHora.Time = 0, TPHora, 'Hora n�o definida!') then Exit;
  if MyObjects.FIC(Cliente = 0, EdCliente, 'Cliente n�o definido!') then Exit;
  if MyObjects.FIC(Solicit = 0, EdSolicitante, 'Solicitante n�o definido!') then Exit;
  if MyObjects.FIC(Assunto = 0, EdAssunto, 'Assunto n�o definido!') then Exit;
  if MyObjects.FIC(Length(Descri) = 0, MeDescri, 'Descri��o n�o definida!') then Exit;
  if MyObjects.FIC(Prioridade = 0, EdPrior, 'Prioridade n�o definida!') then Exit;
  if MyObjects.FIC(Modo = 0, EdModo, 'Modo n�o definido!') then Exit;
  if MyObjects.FIC(Status = 0, EdStatus, 'Status n�o definido!') then Exit;
  if MyObjects.FIC(Respons = 0, EdRespons, 'Respons�vel n�o definido!') then Exit;
  //
  if CO_DMKID_APP = 17 then //DControl
  begin
    Aplic     := EdAplicativo.ValueVariant;
    VersaoApp := CBVersaoApp.Text;
    Janela    := EdJanela.ValueVariant;
    JanelaRel := EdJanelaRel.ValueVariant;
    CompoRel  := EdCompoRel.ValueVariant;
    //
    if MyObjects.FIC(Aplic = 0, EdAplicativo, 'Aplicativo n�o definido!') then Exit;
  end else
  begin
    Aplic     := 0;
    VersaoApp := '';
    Janela    := '';
    JanelaRel := '';
    CompoRel  := '';
  end;
  //
  if ImgTipo.SQLType = stIns then
  begin
    Codigo := 0;
  end else
  begin
    Codigo := FCodigo;
  end;
  //
  Resul := DmkWeb.WOrdSerInclui(31, Codigo, Cliente, Solicit, Assunto, Prioridade,
    Modo, Status, Aplic, Respons, Grupo, 0, DataHora, Descri, Sistema, Impacto,
    VersaoApp, Janela, JanelaRel, CompoRel, '', CodigoReturn, Mensagem);
  if Resul > 0 then
  begin
    if Resul = 103 then
      Geral.MB_Aviso(Mensagem);
    //
    if CkContinuar.Checked then
    begin
      Geral.MensagemBox('Dados salvos com sucesso!', 'Aviso', MB_OK+MB_ICONWARNING);
      //
      TPData.Date   := DModG.ObtemAgora();
      TPHora.Time   := DModG.ObtemAgora();
      MeDescri.Text := '';
      EdCliente.SetFocus;
    end else
      Close;
  end else
    Geral.MB_Aviso('Falha ao incluir solicita��o tente novamente mais tarde!');
end;

procedure TFmWOrdSerIts.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmWOrdSerIts.ConfiguraAplicativo(Cliente: Integer);
begin
  if Cliente <> 0 then
  begin
    if CO_DMKID_APP = 17 then //DControl
      DModWOrdSer.ReopenAplicativos(Cliente)
    else
      DModWOrdSer.QrAplicativo.Close;
  end else
    DModWOrdSer.QrAplicativo.Close;
end;

procedure TFmWOrdSerIts.ConfiguraPriorAtendente;
begin
  EdRespons.ValueVariant := DModWOrdSer.ObtemAtendente3(EdAplicativo.ValueVariant,
    EdSolicitante.ValueVariant, EdCliente.ValueVariant, EdAssunto.ValueVariant, 0);
end;

procedure TFmWOrdSerIts.ConfiguraSolcitante(Cliente: Integer);
begin
  if Cliente <> 0 then
    DModWOrdSer.ReopenWUsers(Cliente)
  else
    DModWOrdSer.QrWUsers.Close;
end;

procedure TFmWOrdSerIts.EdAplicativoChange(Sender: TObject);
var
  Aplicativo: Integer;
begin
  if FReabre then
  begin
    Aplicativo := EdAplicativo.ValueVariant;
    //
    if EdRespons.ValueVariant = 0 then
      ConfiguraPriorAtendente;
    //
    if Aplicativo <> 0 then
      DModWOrdSer.SetaVersoesAtuais(Aplicativo, CBVersaoApp);
  end;
end;

procedure TFmWOrdSerIts.EdAssuntoChange(Sender: TObject);
const
  URL = 'http://www.dermatek.net.br/complementos/wordserassmsg.php';
begin
  if (EdAssunto.ValueVariant <> 0) and (FReabre) then
  begin
    if DModWOrdSer.QrWOrdSerAssDescri.Value <> '' then
    begin
      PnBrowser.Visible := True;
      DmkWeb.WebBrowserPost(URL, WebBrowser1, ['id', 'wordserass'],
        [Geral.FF0(VAR_WEB_USR_ID), Geral.FF0(EdAssunto.ValueVariant)]);
    end else
      PnBrowser.Visible := False;
    //
    EdAplicativo.ValueVariant := 0;
    CBAplicativo.KeyValue     := Null;
  end else
    PnBrowser.Visible := False;
end;

procedure TFmWOrdSerIts.EdClienteChange(Sender: TObject);
var
  Cliente: Integer;
begin
  Cliente := EdCliente.ValueVariant;
  //
  if (Cliente <> 0) and (FReabre) then
  begin
    ConfiguraSolcitante(Cliente);
    ConfiguraAplicativo(Cliente);
    //
    EdSolicitante.ValueVariant := 0;
    CBSolicitante.KeyValue     := Null;
    EdAplicativo.ValueVariant  := 0;
    CBAplicativo.KeyValue      := Null;
  end;
end;

procedure TFmWOrdSerIts.EdGrupoChange(Sender: TObject);
var
  Grupo: Integer;
begin
  Grupo := 0;
  //
  DModWOrdSer.ReopenWOrdSerSta(VAR_WEB_USR_TIPO, Grupo);
  //
  if Grupo <> 0 then
  begin
    EdStatus.ValueVariant := 0;
    CBStatus.KeyValue     := Null;
  end;
end;

procedure TFmWOrdSerIts.EdPriorChange(Sender: TObject);
begin
  DBLaPrior.Visible := EdPrior.ValueVariant > 0;
end;

procedure TFmWOrdSerIts.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  DmkWeb.CarregaImagemFormWEB(ImgWEB);
end;

procedure TFmWOrdSerIts.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  PnAplic.Visible := False;
  FGrupo          := 0;
  FStatus         := 0;
  //
  {$IfnDef SWUsr}
  SBSolicitante.Visible := True;
  SBRespons.Visible     := True;
  {$Else}
  SBSolicitante.Visible := False;
  SBRespons.Visible     := False;
  {$EndIf}
  //
  if CO_DMKID_APP = 17 then //DControl
    PnAplic.Visible := True
  else
    PnAplic.Visible := False;
end;

procedure TFmWOrdSerIts.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmWOrdSerIts.FormShow(Sender: TObject);
begin
  MostraEdicao(FMostra, ImgTipo.SQLType, FCodigo);
end;

procedure TFmWOrdSerIts.MostraEdicao(Mostra: Boolean; SQLType: TSQLType;
  Codigo: Integer);
begin
  PnBrowser.Visible := False;
  //
  DModWOrdSer.ReopenClientes;
  DModWOrdSer.ReopenWOrdSerMod();
  DModWOrdSer.ReopenWOrdSerAss();
  DModWOrdSer.ReopenWOrdSerPri;
  DModWOrdSer.ReopenRespons;
  DModWOrdSer.ReopenWOrdSerSta(VAR_WEB_USR_TIPO, 0);
  //
  if not Mostra then
  begin
    Panel5.Enabled      := False;
    Panel6.Enabled      := False;
    PnAplic.Enabled     := False;
    Panel3.Enabled      := False;
    BtOK.Visible        := False;
    CkContinuar.Checked := False;
  end  else
  begin
    Panel5.Enabled  := True;
    Panel6.Enabled  := True;
    PnAplic.Enabled := True;
    Panel3.Enabled  := True;
    BtOK.Visible    := True;
    //
    EdCliente.SetFocus;
  end;
  if SQLType = stIns then
  begin
    FReabre                    := True;
    EdCodigo.ValueVariant      := FormatFloat(FFormatFloat, Codigo);
    GBDataHora.Visible         := True;
    TPData.Date                := DModG.ObtemAgora();
    TPHora.Time                := DModG.ObtemAgora();
    FDataHora                  := '';
    EdAssunto.ValueVariant     := 0;
    CBAssunto.KeyValue         := Null;
    MeDescri.Text              := '';
    MeDescri.Enabled           := True;
    EdAplicativo.ValueVariant  := 0;
    CBAplicativo.KeyValue      := Null;
    CBVersaoApp.Text           := '';
    EdJanela.ValueVariant      := '';
    EdJanelaRel.ValueVariant   := '';
    EdCompoRel.ValueVariant    := '';
    EdModo.ValueVariant        := DModWOrdSer.QrWOrdSerOpc.FieldByName('ModPad').AsInteger;
    CBModo.KeyValue            := DModWOrdSer.QrWOrdSerOpc.FieldByName('ModPad').AsInteger;
    EdPrior.ValueVariant       := 0;
    CBPrior.KeyValue           := Null;
    //
    if FStatus <> 0 then
    begin
      EdStatus.ValueVariant := FStatus;
      CBStatus.KeyValue     := FStatus;
    end else
    begin
      EdStatus.ValueVariant := DModWOrdSer.QrWOrdSerOpc.FieldByName('StatPad').AsInteger;
      CBStatus.KeyValue     := DModWOrdSer.QrWOrdSerOpc.FieldByName('StatPad').AsInteger;
    end;
    //
    if FDadosUsuario = True then
    begin
      EdCliente.ValueVariant     := VAR_WEB_USR_ENT;
      CBCliente.KeyValue         := VAR_WEB_USR_ENT;
      EdSolicitante.ValueVariant := VAR_WEB_USR_ID;
      CBSolicitante.KeyValue     := VAR_WEB_USR_ID;
      EdRespons.ValueVariant     := VAR_WEB_USR_ID;
      CBRespons.KeyValue         := VAR_WEB_USR_ID;
    end;

    CkContinuar.Visible        := True;
    //
    if VAR_WEB_USR_TIPO = 1 then
    begin
      EdRespons.Enabled := False;
      CBRespons.Enabled := False;
      SBRespons.Enabled := False;
    end else
    begin
      EdRespons.Enabled := True;
      CBRespons.Enabled := True;
      SBRespons.Enabled := True;
    end;
  end else begin
    try
      FReabre                := False;
      Screen.Cursor          := crHourGlass;
      BtOK.Enabled           := False;
      GBDataHora.Visible     := False;
      EdCodigo.ValueVariant  := FQrWOrdSer.FieldByName('Codigo').AsInteger;
      FDataHora              := FQrWOrdSer.FieldByName('Abertura').AsString;
      EdCliente.ValueVariant := FQrWOrdSer.FieldByName('Cliente').AsInteger;
      //
      ConfiguraSolcitante(FQrWOrdSer.FieldByName('Cliente').AsInteger);
      //
      EdSolicitante.ValueVariant := FQrWOrdSer.FieldByName('Solicit').AsInteger;
      CBSolicitante.KeyValue     := FQrWOrdSer.FieldByName('Solicit').AsInteger;
      EdAssunto.ValueVariant     := FQrWOrdSer.FieldByName('Assunto').AsInteger;
      CBAssunto.KeyValue         := FQrWOrdSer.FieldByName('Assunto').AsInteger;
      MeDescri.Text              := FQrWOrdSer.FieldByName('Descri').AsString;
      MeDescri.Enabled           := (FQrWOrdSer.FieldByName('CadWeb').AsInteger = VAR_WEB_USR_ID) or (VAR_USUARIO < 0);
      //
      ConfiguraAplicativo(FQrWOrdSer.FieldByName('Cliente').AsInteger);
      //
      EdAplicativo.ValueVariant := FQrWOrdSer.FieldByName('Aplicativo').AsInteger;
      CBAplicativo.KeyValue     := FQrWOrdSer.FieldByName('Aplicativo').AsInteger;
      CBVersaoApp.Text          := FQrWOrdSer.FieldByName('VersaoApp').AsString;
      EdJanela.ValueVariant     := FQrWOrdSer.FieldByName('Janela').AsString;
      EdJanelaRel.ValueVariant  := FQrWOrdSer.FieldByName('JanelaRel').AsString;
      EdCompoRel.ValueVariant   := FQrWOrdSer.FieldByName('CompoRel').AsString;
      EdSistema.ValueVariant    := FQrWOrdSer.FieldByName('Sistema').AsString;
      EdImpacto.ValueVariant    := FQrWOrdSer.FieldByName('Impacto').AsString;
      EdModo.ValueVariant       := FQrWOrdSer.FieldByName('Modo').AsInteger;
      CBModo.KeyValue           := FQrWOrdSer.FieldByName('Modo').AsInteger;
      EdPrior.ValueVariant      := FQrWOrdSer.FieldByName('Prioridade').AsInteger;
      CBPrior.KeyValue          := FQrWOrdSer.FieldByName('Prioridade').AsInteger;
      EdStatus.ValueVariant     := FQrWOrdSer.FieldByName('Status').AsInteger;
      CBStatus.KeyValue         := FQrWOrdSer.FieldByName('Status').AsInteger;
      EdRespons.ValueVariant    := FQrWOrdSer.FieldByName('Respons').AsInteger;
      CBRespons.KeyValue        := FQrWOrdSer.FieldByName('Respons').AsInteger;
      FGrupo                    := FQrWOrdSer.FieldByName('Grupo').AsInteger;
      CkContinuar.Visible       := False;
      CkContinuar.Checked       := False;
    finally
      Screen.Cursor := crDefault;
      FReabre       := True;
      BtOK.Enabled  := True;
    end;
  end;
end;

procedure TFmWOrdSerIts.SBStatusClick(Sender: TObject);
var
  Grupo: Integer;
begin
  Grupo := 0;
  //
  DModWOrdSer.MostraWOrdSerSta(EdStatus.ValueVariant);
  //
  if VAR_CADASTRO > 0 then
  begin
    DModWOrdSer.ReopenWOrdSerSta(VAR_WEB_USR_TIPO, Grupo);
    //
    EdStatus.ValueVariant := VAR_CADASTRO;
    CBStatus.KeyValue     := VAR_CADASTRO;
    EdStatus.SetFocus;
  end;
end;

procedure TFmWOrdSerIts.SBSolicitanteClick(Sender: TObject);
begin
  {$IfnDef SWUsr}
  VAR_CADASTRO := 0;
  //
  UWUsersJan.MostraWUsers(EdSolicitante.ValueVariant);
  //
  if VAR_CADASTRO > 0 then
  begin
    DModWOrdSer.ReopenWUsers(EdCliente.ValueVariant);
    //
    EdSolicitante.ValueVariant := VAR_CADASTRO;
    CBSolicitante.KeyValue     := VAR_CADASTRO;
    EdSolicitante.SetFocus;
  end;
  {$EndIf}
end;

procedure TFmWOrdSerIts.SpeedButton4Click(Sender: TObject);
begin
  DModWOrdSer.MostraWOrdSerAss(EdAssunto.ValueVariant);
  if VAR_CADASTRO > 0 then
  begin
    DModWOrdSer.ReopenWOrdSerAss();
    //
    EdAssunto.ValueVariant := VAR_CADASTRO;
    CBAssunto.KeyValue     := VAR_CADASTRO;
    EdAssunto.SetFocus;
  end;
end;

procedure TFmWOrdSerIts.SBResponsClick(Sender: TObject);
begin
  {$IfnDef SWUsr}
  VAR_CADASTRO := 0;
  //
  UWUsersJan.MostraWUsers(EdRespons.ValueVariant);
  //
  if VAR_CADASTRO > 0 then
  begin
    DModWOrdSer.ReopenWUsers(EdRespons.ValueVariant);
    //
    EdRespons.ValueVariant := VAR_CADASTRO;
    CBRespons.KeyValue     := VAR_CADASTRO;
    EdRespons.SetFocus;
  end;
  {$EndIf}
end;

procedure TFmWOrdSerIts.SpeedButton7Click(Sender: TObject);
begin
  DModWOrdSer.MostraWOrdSerMod(EdModo.ValueVariant);
  if VAR_CADASTRO > 0 then
  begin
    DModWOrdSer.ReopenWOrdSerMod();
    //
    EdModo.ValueVariant := VAR_CADASTRO;
    CBModo.KeyValue     := VAR_CADASTRO;
    EdModo.SetFocus;
  end;
end;

procedure TFmWOrdSerIts.WebBrowser1DocumentComplete(ASender: TObject;
  const pDisp: IDispatch; const URL: OleVariant);
var
  Doc: IHTMLDocument2;
  Body: OleVariant;
begin
  Doc := IHTMLDocument2(WebBrowser1.Document);
  body := Doc.Body;
  {hide scrollbars}
  Body.Style.BorderStyle := 'none';
  Body.Scroll := 'no';
end;

end.
