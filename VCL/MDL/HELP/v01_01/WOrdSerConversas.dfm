object FmWOrdSerConversas: TFmWOrdSerConversas
  Left = 0
  Top = 0
  Caption = 'WEB-ORSER-018 :: Ordem de Servi'#231'o - Conversas'
  ClientHeight = 562
  ClientWidth = 784
  Color = clBtnFace
  Constraints.MinHeight = 460
  Constraints.MinWidth = 515
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object WebBrowser1: TWebBrowser
    Left = 0
    Top = 0
    Width = 784
    Height = 541
    Align = alClient
    TabOrder = 0
    OnDocumentComplete = WebBrowser1DocumentComplete
    ExplicitWidth = 1008
    ExplicitHeight = 600
    ControlData = {
      4C00000007510000EA3700000000000000000000000000000000000000000000
      000000004C000000000000000000000001000000E0D057007335CF11AE690800
      2B2E12620A000000000000004C0000000114020000000000C000000000000046
      8000000000000000000000000000000000000000000000000000000000000000
      00000000000000000100000000000000000000000000000000000000}
  end
  object TabSet1: TTabSet
    Left = 0
    Top = 541
    Width = 784
    Height = 21
    Align = alBottom
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    PopupMenu = PopupMenu1
    SoftTop = True
    Tabs.Strings = (
      'Teste'
      'Teste1')
    TabIndex = 0
    OnChange = TabSet1Change
    OnMouseDown = TabSet1MouseDown
  end
  object PopupMenu1: TPopupMenu
    OnPopup = PopupMenu1Popup
    Left = 152
    Top = 144
    object Fecharaba1: TMenuItem
      Caption = 'Fechar aba'
      OnClick = Fecharaba1Click
    end
  end
end
