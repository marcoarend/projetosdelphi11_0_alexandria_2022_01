object FmWOrdSerAtePar: TFmWOrdSerAtePar
  Left = 339
  Top = 185
  Caption = 'WEB-ORSER-014 :: Atendentes da Ordem de Servi'#231'o - Par'#226'metros'
  ClientHeight = 364
  ClientWidth = 719
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 719
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 631
      Top = 0
      Width = 88
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 48
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
      object ImgWEB: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 583
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 566
        Height = 32
        Caption = 'Atendentes da Ordem de Servi'#231'o - Par'#226'metros'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 566
        Height = 32
        Caption = 'Atendentes da Ordem de Servi'#231'o - Par'#226'metros'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 566
        Height = 32
        Caption = 'Atendentes da Ordem de Servi'#231'o - Par'#226'metros'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 719
    Height = 202
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 719
      Height = 202
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 719
        Height = 202
        Align = alClient
        TabOrder = 0
        object LaAplicativo: TLabel
          Left = 16
          Top = 94
          Width = 49
          Height = 13
          Caption = 'Aplicativo:'
        end
        object LaSolicitante: TLabel
          Left = 16
          Top = 12
          Width = 52
          Height = 13
          Caption = 'Solicitante:'
        end
        object LaAssunto: TLabel
          Left = 16
          Top = 134
          Width = 41
          Height = 13
          Caption = 'Assunto:'
        end
        object LaCliente: TLabel
          Left = 16
          Top = 52
          Width = 35
          Height = 13
          Caption = 'Cliente:'
        end
        object EdAplicativo: TdmkEditCB
          Left = 16
          Top = 110
          Width = 56
          Height = 21
          Alignment = taRightJustify
          TabOrder = 4
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          DBLookupComboBox = CBAplicativo
          IgnoraDBLookupComboBox = False
          AutoSetIfOnlyOneReg = setregOnlyManual
        end
        object EdSolicitante: TdmkEditCB
          Left = 16
          Top = 28
          Width = 56
          Height = 21
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          DBLookupComboBox = CBSolicitante
          IgnoraDBLookupComboBox = False
          AutoSetIfOnlyOneReg = setregOnlyManual
        end
        object EdAssunto: TdmkEditCB
          Left = 16
          Top = 150
          Width = 56
          Height = 21
          Alignment = taRightJustify
          TabOrder = 6
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          DBLookupComboBox = CBAssunto
          IgnoraDBLookupComboBox = False
          AutoSetIfOnlyOneReg = setregOnlyManual
        end
        object CBAssunto: TdmkDBLookupComboBox
          Left = 72
          Top = 150
          Width = 633
          Height = 21
          KeyField = 'Codigo'
          ListField = 'Nome'
          ListSource = DsWOrdSerAss
          TabOrder = 7
          dmkEditCB = EdAssunto
          UpdType = utYes
          LocF7SQLMasc = '$#'
          LocF7PreDefProc = f7pNone
        end
        object CkContinuar: TCheckBox
          Left = 16
          Top = 177
          Width = 112
          Height = 17
          Caption = 'Continuar inserindo.'
          TabOrder = 8
        end
        object EdCliente: TdmkEditCB
          Left = 16
          Top = 68
          Width = 56
          Height = 21
          Alignment = taRightJustify
          TabOrder = 2
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          OnChange = EdClienteChange
          DBLookupComboBox = CBCliente
          IgnoraDBLookupComboBox = False
          AutoSetIfOnlyOneReg = setregOnlyManual
        end
        object CBCliente: TdmkDBLookupComboBox
          Left = 72
          Top = 68
          Width = 633
          Height = 21
          KeyField = 'Codigo'
          ListField = 'Nome'
          ListSource = DsClientes
          TabOrder = 3
          dmkEditCB = EdCliente
          UpdType = utYes
          LocF7SQLMasc = '$#'
          LocF7PreDefProc = f7pNone
        end
        object CBSolicitante: TdmkDBLookupComboBox
          Left = 72
          Top = 28
          Width = 633
          Height = 21
          KeyField = 'Codigo'
          ListField = 'PersonalName'
          ListSource = DsWUsers
          TabOrder = 1
          dmkEditCB = EdSolicitante
          UpdType = utYes
          LocF7SQLMasc = '$#'
          LocF7PreDefProc = f7pNone
        end
        object CBAplicativo: TdmkDBLookupComboBox
          Left = 72
          Top = 110
          Width = 633
          Height = 21
          KeyField = 'Codigo'
          ListField = 'Nome'
          ListSource = DsAplicativos
          TabOrder = 5
          dmkEditCB = EdAplicativo
          UpdType = utYes
          LocF7SQLMasc = '$#'
          LocF7PreDefProc = f7pNone
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 250
    Width = 719
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 715
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 294
    Width = 719
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 573
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 571
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object DsAplicativos: TDataSource
    DataSet = QrAplicativos
    Left = 406
    Top = 265
  end
  object QrAplicativos: TMySQLQuery
    Database = Dmod.MyDBn
    Left = 378
    Top = 265
    object QrAplicativosCodigo: TAutoIncField
      FieldName = 'Codigo'
    end
    object QrAplicativosNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 340
    Top = 216
  end
  object QrWUsers: TMySQLQuery
    Database = Dmod.MyDBn
    Left = 322
    Top = 265
    object AutoIncField1: TAutoIncField
      FieldName = 'Codigo'
    end
    object QrWUsersPersonalName: TWideStringField
      FieldName = 'PersonalName'
      Size = 32
    end
  end
  object DsWUsers: TDataSource
    DataSet = QrWUsers
    Left = 350
    Top = 265
  end
  object QrClientes: TMySQLQuery
    Database = Dmod.MyDBn
    SQL.Strings = (
      'SELECT Codigo, IF(Tipo=0, RazaoSocial, Nome) Nome'#11
      'FROM entidades'
      'WHERE Cliente1 = "V"'
      'AND Ativo=1'
      'ORDER BY Nome')
    Left = 266
    Top = 265
    object AutoIncField2: TAutoIncField
      FieldName = 'Codigo'
    end
    object QrClientesNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
  object DsClientes: TDataSource
    DataSet = QrClientes
    Left = 294
    Top = 265
  end
  object QrWOrdSerAss: TMySQLQuery
    Database = Dmod.MyDBn
    SQL.Strings = (
      
        'SELECT ass.Codigo, CONCAT(ass.Nome, IF(ass.Grupo<>0, CONCAT(" ["' +
        ', gru.Nome, "]"), "")) Nome'#11
      'FROM wordserass ass'
      'LEFT JOIN wosassgru gru ON gru.Codigo = ass.Grupo '
      'WHERE ass.Ativo = 1'
      'ORDER BY ass.Nome')
    Left = 210
    Top = 265
    object AutoIncField3: TAutoIncField
      FieldName = 'Codigo'
    end
    object StringField3: TWideStringField
      FieldName = 'Nome'
      Size = 255
    end
  end
  object DsWOrdSerAss: TDataSource
    DataSet = QrWOrdSerAss
    Left = 238
    Top = 265
  end
end
