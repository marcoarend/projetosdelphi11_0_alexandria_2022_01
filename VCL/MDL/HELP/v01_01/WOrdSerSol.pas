unit WOrdSerSol;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2,
  ResIntStrings, UnMLAGeral, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, dmkGeral,
  dmkPermissoes, dmkEdit, dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup,
  unDmkProcFunc, dmkMemo, dmkCheckBox, DmkDAC_PF, UnDmkEnums;

type
  TFmWOrdSerSol = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    BtExclui: TBitBtn;
    BtAltera: TBitBtn;
    BtInclui: TBitBtn;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    QrWOrdSerEnc: TmySQLQuery;
    QrWOrdSerEncCodigo: TIntegerField;
    DsWOrdSerEnc: TDataSource;
    Panel6: TPanel;
    Label1: TLabel;
    DBEdCodigo: TdmkDBEdit;
    Panel7: TPanel;
    Label2: TLabel;
    DBMemo1: TDBMemo;
    DBCheckBox1: TDBCheckBox;
    QrWOrdSerEncAtivo: TSmallintField;
    Panel8: TPanel;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    Panel9: TPanel;
    Label9: TLabel;
    MeDescri: TdmkMemo;
    Label3: TLabel;
    DBEdNome: TdmkDBEdit;
    QrWOrdSerEncNome: TWideStringField;
    QrWOrdSerEncDescri: TWideMemoField;
    Label4: TLabel;
    EdNome: TdmkEdit;
    QrLoc: TmySQLQuery;
    ImgWEB: TdmkImage;
    CkAtivo: TdmkCheckBox;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtIncluiClick(Sender: TObject);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure QrWOrdSerEncAfterOpen(DataSet: TDataSet);
    procedure QrWOrdSerEncBeforeOpen(DataSet: TDataSet);
    procedure BtExcluiClick(Sender: TObject);
    procedure EdNomeEnter(Sender: TObject);
    procedure EdNomeExit(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    procedure ConfiguraTexto(MostraTexto: Boolean);
  public
    { Public declarations }
    procedure LocCod(Atual, Codigo: Integer);
  end;

var
  FmWOrdSerSol: TFmWOrdSerSol;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, UnDmkWeb;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmWOrdSerSol.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmWOrdSerSol.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrWOrdSerEncCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmWOrdSerSol.DefParams;
begin
  VAR_GOTOTABELA := 'wordserenc';
  VAR_GOTOMYSQLTABLE := QrWOrdSerEnc;
  VAR_GOTONEG := gotoPos;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDBn;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT *');
  VAR_SQLx.Add('FROM wordserenc');
  VAR_SQLx.Add('WHERE Codigo > 0');
  //
  VAR_SQL1.Add('AND Codigo=:P0');
  //
  //VAR_SQL2.Add('AND CodUsu=:P0');
  //
  VAR_SQLa.Add('AND Nome Like :P0');
  //
end;

procedure TFmWOrdSerSol.EdNomeEnter(Sender: TObject);
begin
  ConfiguraTexto(True);
end;

procedure TFmWOrdSerSol.EdNomeExit(Sender: TObject);
begin
  ConfiguraTexto(False);
  //
  if MeDescri.Text = '' then
  begin
    MeDescri.Text     := EdNome.ValueVariant;
    MeDescri.SelStart := Length(MeDescri.Text);
  end;
end;

procedure TFmWOrdSerSol.ConfiguraTexto(MostraTexto: Boolean);
begin
  if MostraTexto then
    MyObjects.Informa2(LaAviso1, LaAviso2, False,
      'Digite no campo Descri��o uma pr�via do que ser� digitado no campo Texto!')
  else
    MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
end;

procedure TFmWOrdSerSol.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmWOrdSerSol.QueryPrincipalAfterOpen;
begin
end;

procedure TFmWOrdSerSol.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmWOrdSerSol.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmWOrdSerSol.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmWOrdSerSol.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmWOrdSerSol.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmWOrdSerSol.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmWOrdSerSol.BtAlteraClick(Sender: TObject);
begin
  if (QrWOrdSerEnc.State <> dsInactive) and (QrWOrdSerEnc.RecordCount > 0) and
    (QrWOrdSerEncCodigo.Value <> 0) then
  begin
    UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrWOrdSerEnc, [PnDados],
      [PnEdita], EdNome, ImgTipo, 'wordserenc');
  end;
end;

procedure TFmWOrdSerSol.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrWOrdSerEncCodigo.Value;
  Close;
end;

procedure TFmWOrdSerSol.BtConfirmaClick(Sender: TObject);
var
  Codigo: Integer;
  Nome, Descri: String;
begin
  Nome   := EdNome.ValueVariant;
  Descri := MeDescri.Text;
  //
  if MyObjects.FIC(Length(Nome) = 0, EdNome, 'Defina uma descri��o!') then Exit;
  if MyObjects.FIC(Length(Descri) = 0, MeDescri, 'Defina um texto!') then Exit;
  //
  if ImgTipo.SQLType = stIns then
    Codigo := UMyMod.BuscaNovoCodigo_Int(Dmod.QrUpdN, 'wordserenc', 'Codigo', [], [], stIns, 0, siPositivo, nil)
  else
    Codigo := QrWOrdSerEncCodigo.Value;
  //
  if UMyMod.ExecSQLInsUpdPanel(ImgTipo.SQLType, Self, PnEdita,
    'wordserenc', Codigo, Dmod.QrUpdN, [PnEdita], [PnDados], ImgTipo, True) then
  begin
    ConfiguraTexto(False);
    LocCod(Codigo, Codigo);
  end;
end;

procedure TFmWOrdSerSol.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo          := EdCodigo.ValueVariant;
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  //
  ConfiguraTexto(False);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDBn, 'wordserenc', 'Codigo');
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmWOrdSerSol.BtExcluiClick(Sender: TObject);
var
  Codigo: Integer;
begin
  if (QrWOrdSerEnc.State <> dsInactive) and (QrWOrdSerEnc.RecordCount > 0) then
  begin
    Codigo := QrWOrdSerEncCodigo.Value;
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrLoc, Dmod.MyDBn, [
    'SELECT * ',
    'FROM wordser ',
    'WHERE WOrdSerEnc=' + Geral.FF0(Codigo),
    '']);
    if QrLoc.RecordCount > 0 then
    begin
      Geral.MB_Aviso('Esta resolu��o j� foi utilizada nas solicita��es e por isso n�o poder� ser exclu�-da!');
      Exit;
    end;
    UMyMod.ExcluiRegistroInt1('Confirma a exclus�o deste item?',
      'wordserenc', 'Codigo', Codigo, DMod.MyDBn);
    Va(vpLast);
  end;
end;

procedure TFmWOrdSerSol.BtIncluiClick(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrWOrdSerEnc, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'wordserenc');
end;

procedure TFmWOrdSerSol.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  GBEdita.Align   := alClient;
  GBDados.Align   := alClient;
  CriaOForm;
end;

procedure TFmWOrdSerSol.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrWOrdSerEncCodigo.Value, LaRegistro.Caption);
end;

procedure TFmWOrdSerSol.SbImprimeClick(Sender: TObject);
begin
  Geral.MB_Info('Impress�o indispon�vel para esta janela!');
end;

procedure TFmWOrdSerSol.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmWOrdSerSol.SbNovoClick(Sender: TObject);
begin
  Geral.MB_Info('Tipo de pesquisa indispon�vel para esta janela!');
end;

procedure TFmWOrdSerSol.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmWOrdSerSol.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  DmkWeb.CarregaImagemFormWEB(ImgWEB);
end;

procedure TFmWOrdSerSol.SbQueryClick(Sender: TObject);
begin
  LocCod(QrWOrdSerEncCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'wordserenc', Dmod.MyDBn, CO_VAZIO));
end;

procedure TFmWOrdSerSol.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmWOrdSerSol.QrWOrdSerEncAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmWOrdSerSol.QrWOrdSerEncBeforeOpen(DataSet: TDataSet);
begin
  QrWOrdSerEncCodigo.DisplayFormat := FFormatFloat;
end;

end.
