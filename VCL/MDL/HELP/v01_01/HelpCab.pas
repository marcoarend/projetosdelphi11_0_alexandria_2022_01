unit HelpCab;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2, ComCtrls,
  ResIntStrings, UnMLAGeral, UnGOTOy, UnInternalConsts, UnMsgInt, dmkCheckBox,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, dmkGeral,
  dmkPermissoes, dmkEdit, dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup,
  unDmkProcFunc, UnDmkEnums, dmkCompoStore, dmkDBLookupComboBox, dmkEditCB,
  dmkCheckGroup, Vcl.Menus, Vcl.Grids, Vcl.DBGrids, dmkDBGrid, dmkPageControl,
  Vcl.OleCtrls, SHDocVw, UnProjGroup_Consts;

type
  TFmHelpCab = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    BtExclui: TBitBtn;
    BtAltera: TBitBtn;
    BtInclui: TBitBtn;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    QrHelpCab: TmySQLQuery;
    QrHelpCabCodigo: TIntegerField;
    QrHelpCabNome: TWideStringField;
    DsHelpCab: TDataSource;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    Label9: TLabel;
    EdNome: TdmkEdit;
    Label1: TLabel;
    DBEdCodigo: TdmkDBEdit;
    Label2: TLabel;
    DBEdNome: TdmkDBEdit;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    QrHelpCabAtivo: TSmallintField;
    CkAtivo: TdmkCheckBox;
    DBCheckBox2: TDBCheckBox;
    CSTabSheetChamou: TdmkCompoStore;
    QrHelpCabNivel: TIntegerField;
    DBRGStatus: TDBRadioGroup;
    DBCGNivel: TdmkDBCheckGroup;
    RGStatus: TdmkRadioGroup;
    CGNivel: TdmkCheckGroup;
    PMTopicos: TPopupMenu;
    Incluisubtpico1: TMenuItem;
    Altera1: TMenuItem;
    Exclui1: TMenuItem;
    N1: TMenuItem;
    Mover1: TMenuItem;
    QrHelpTopic: TmySQLQuery;
    QrHelpCabStatus: TIntegerField;
    PCMenu: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    DBGRestricao: TdmkDBGrid;
    Splitter1: TSplitter;
    Panel6: TPanel;
    BtRestringe: TBitBtn;
    PMRestringe: TPopupMenu;
    Inclui2: TMenuItem;
    AplicativoLocal1: TMenuItem;
    AplicativoWeb1: TMenuItem;
    Mdulo1: TMenuItem;
    Exclui2: TMenuItem;
    QrHelpRestr: TmySQLQuery;
    QrHelpRestrCodigo: TIntegerField;
    QrHelpRestrItem: TIntegerField;
    QrHelpRestrHelpResTip: TIntegerField;
    QrHelpRestrNome: TWideStringField;
    QrHelpRestrHelpResTip_TXT: TWideStringField;
    QrHelpRestrHelpTip: TSmallintField;
    QrHelpRestrHelp: TIntegerField;
    DsHelpRestr: TDataSource;
    TVTopicos: TTreeView;
    PnTopicos: TPanel;
    BtTopInclui: TBitBtn;
    BtTopReordena: TBitBtn;
    N2: TMenuItem;
    Vinculartpicoaumtutorial1: TMenuItem;
    BtSincro: TBitBtn;
    Abrirtutorial1: TMenuItem;
    WebBrowser1: TWebBrowser;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrHelpCabAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrHelpCabBeforeOpen(DataSet: TDataSet);
    procedure BtIncluiClick(Sender: TObject);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure BtTopIncluiClick(Sender: TObject);
    procedure BtTopReordenaClick(Sender: TObject);
    procedure Incluisubtpico1Click(Sender: TObject);
    procedure Altera1Click(Sender: TObject);
    procedure Exclui1Click(Sender: TObject);
    procedure Mover1Click(Sender: TObject);
    procedure TVTopicosMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure QrHelpCabBeforeClose(DataSet: TDataSet);
    procedure QrHelpCabAfterScroll(DataSet: TDataSet);
    procedure BtRestringeClick(Sender: TObject);
    procedure PMRestringePopup(Sender: TObject);
    procedure AplicativoLocal1Click(Sender: TObject);
    procedure AplicativoWeb1Click(Sender: TObject);
    procedure Mdulo1Click(Sender: TObject);
    procedure Exclui2Click(Sender: TObject);
    procedure BtExcluiClick(Sender: TObject);
    procedure Vinculartpicoaumtutorial1Click(Sender: TObject);
    procedure TVTopicosClick(Sender: TObject);
    procedure BtSincroClick(Sender: TObject);
    procedure Abrirtutorial1Click(Sender: TObject);
    procedure PMTopicosPopup(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    procedure MostraEdicao;
    procedure CriaNode(Child: Boolean);
    procedure EditaNode();
    procedure ExcluiNode();
    procedure VincularTutorial();
    procedure CarregaTutorial();
    procedure ReordenaTopicos();
    procedure ReopenHelpTopic(Codigo: Integer);
    procedure AbrirTutorialSelecionado();
  public
    { Public declarations }
    procedure LocCod(Atual, Codigo: Integer);
  end;

var
  FmHelpCab: TFmHelpCab;
const
  FFormatFloat = '00000';

implementation

uses MyListas, UnMyObjects, Module, UnGrlHelp, Principal, MyGlyfs, UnGrlUsuarios,
  UnTreeView, UnitHelp, UnitHelpJan;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmHelpCab.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmHelpCab.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrHelpCabCodigo.Value, LaRegistro.Caption[2]);
end;

procedure TFmHelpCab.DefParams;
begin
  VAR_GOTOTABELA := 'helpcab';
  VAR_GOTOMYSQLTABLE := QrHelpCab;
  VAR_GOTONEG := gotoPos;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT *');
  VAR_SQLx.Add('FROM helpcab');
  VAR_SQLx.Add('WHERE Codigo > 0');
  //
  VAR_SQL1.Add('AND Codigo=:P0');
  //
  //VAR_SQL2.Add('AND CodUsu=:P0');
  //
  VAR_SQLa.Add('AND Nome Like :P0');
  //
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmHelpCab.Mdulo1Click(Sender: TObject);
begin
  UnHelp.IncluiHelpRestr(thtHelp, 3, QrHelpCabCodigo.Value, QrHelpRestr);
end;

procedure TFmHelpCab.Vinculartpicoaumtutorial1Click(Sender: TObject);
begin
  VincularTutorial();
end;

procedure TFmHelpCab.VincularTutorial;
var
  Confirmou: Boolean;
  HelpFaq, Controle: Integer;
  Node: TTreeNode;
begin
  Node := TVTopicos.Selected;
  //
  if Node <> nil then
  begin
    HelpFaq  := UnHelpJan.MostraFormHelpFAQUrl(thtHelp, Confirmou);
    Controle := Node.StateIndex;
    //
    if (Controle <> 0) and (Confirmou = True) then
    begin
      UnHelpJan.FechaWebBrowser(WebBrowser1);
      GrlHelp.UpdAjudaTopicoTutorial(Dmod.MyDB, Dmod.QrUpd, stDesktop, Controle,
        HelpFaq);
      ReopenHelpTopic(QrHelpCabCodigo.Value);
      CarregaTutorial;
    end;
  end;
end;

procedure TFmHelpCab.MostraEdicao;
begin
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  //
  GOTOy.BotoesSb(ImgTipo.SQLType, TFmHelpCab(Self));
end;

procedure TFmHelpCab.Mover1Click(Sender: TObject);
begin
  ReordenaTopicos;
end;

procedure TFmHelpCab.PMRestringePopup(Sender: TObject);
var
  Enab, Enab2: Boolean;
begin
  Enab  := (QrHelpCab.State <> dsInactive) and (QrHelpCab.RecordCount > 0) and
           (Geral.IntInConjunto(1, QrHelpCabNivel.Value));
  Enab2 := (QrHelpRestr.State <> dsInactive) and (QrHelpRestr.RecordCount > 0);
  //
  Inclui2.Enabled := Enab;
  Exclui2.Enabled := Enab and Enab2;
end;

procedure TFmHelpCab.PMTopicosPopup(Sender: TObject);
var
  Controle: Integer;
  Enab, Enab2: Boolean;
  Node: TTreeNode;
begin
  Enab  := False;
  Enab2 := False;
  //
  if TVTopicos.Items.Count > 0 then
  begin
    Node := TVTopicos.Selected;
    //
    if Node <> nil then
    begin
      Enab     := True;
      Controle := Node.StateIndex;
      //
      if Controle <> 0 then
        if QrHelpTopic.Locate('Controle', Controle, []) then
          if QrHelpTopic.FieldByName('HelpFaq').AsInteger <> 0 then
            Enab2 := True;
    end;
  end;
  Incluisubtpico1.Enabled           := Enab;
  Altera1.Enabled                   := Enab;
  Exclui1.Enabled                   := Enab;
  Vinculartpicoaumtutorial1.Enabled := Enab;
  Abrirtutorial1.Enabled            := Enab and Enab2;
end;

procedure TFmHelpCab.EditaNode;
var
  Codigo, Controle: Integer;
  Txt: String;
  Node: TTreeNode;
begin
  if (QrHelpCab.State <> dsInactive) and (QrHelpCab.RecordCount > 0) then
  begin
    Txt  := '';
    Node := TVTopicos.Selected;
    //
    if Node <> nil then
    begin
      Txt := Node.Text;
      //
      if InputQuery(Application.Title, 'Informe o t�pico', Txt) then
      begin
        Controle := Node.StateIndex;
        Codigo   := QrHelpCabCodigo.Value;
        //
        if GrlHelp.InsUpdAjudaTopico(Dmod.MyDB, Dmod.QrUpd, stDesktop, Txt, Codigo,
          1, Controle) then
        begin
          UTreeView.EditaNode(TVTopicos, Node, Txt, 0);
        end;
      end;
    end;
  end;
end;

procedure TFmHelpCab.Abrirtutorial1Click(Sender: TObject);
begin
  AbrirTutorialSelecionado;
end;

procedure TFmHelpCab.AbrirTutorialSelecionado;
var
  Controle, HelpFaq: Integer;
  Node: TTreeNode;
begin
  if (QrHelpCab.State <> dsInactive) and (QrHelpCab.RecordCount > 0) then
  begin
    Node := TVTopicos.Selected;
    //
    if Node <> nil then
    begin
      Controle := Node.StateIndex;
      //
      if Controle <> 0 then
        if QrHelpTopic.Locate('Controle', Controle, []) then
        begin
          HelpFaq := QrHelpTopic.FieldByName('HelpFaq').AsInteger;
          //
          if HelpFaq <> 0 then
          begin
            if TFmHelpCab(Self).Owner is TApplication then
              UnHelpJan.MostraFormHelpFAQ(FmPrincipal, False, nil, nil, HelpFaq)
            else
              UnHelpJan.MostraFormHelpFAQ(FmPrincipal, True, FmPrincipal.PageControl1,
                FmPrincipal.AdvToolBarPagerNovo, HelpFaq);
          end;
        end;
    end;
  end;
end;

procedure TFmHelpCab.Exclui1Click(Sender: TObject);
begin
  ExcluiNode();
end;

procedure TFmHelpCab.Exclui2Click(Sender: TObject);
var
  Codigo: Integer;
begin
  Codigo := QrHelpRestrCodigo.Value;
  //
  if GrlHelp.ExcluiHelpRestr(Dmod.MyDB, Dmod.QrUpd, Codigo,
    'Confirma a exclus�o do item ID n�mero: ' + Geral.FF0(Codigo)) then
  begin
    GrlHelp.ReopenHelpRestr(Dmod.MyDB, QrHelpRestr, QrHelpCabCodigo.Value,
      Integer(thtHelp), 0);
  end;
end;

procedure TFmHelpCab.ExcluiNode;
var
  Controle: Integer;
  Nome, Msg: String;
  Node: TTreeNode;
begin
  if (QrHelpCab.State <> dsInactive) and (QrHelpCab.RecordCount > 0) then
  begin
    Node := TVTopicos.Selected;
    //
    if Node <> nil then
    begin
      Controle := Node.StateIndex;
      Nome     := Node.Text;
      Msg      := 'Confirma a exclus�o do t�pico: "' + Nome + '"?';
      //
      if UTreeView.ExcluiNode(TVTopicos, Msg) then
        GrlHelp.ExcluiAjudaTopico(Dmod.MyDB, Dmod.QrAux, Controle, '');
    end;
  end;
end;

procedure TFmHelpCab.CriaNode(Child: Boolean);
var
  Codigo, Controle: Integer;
  Txt: String;
  Node: TTreeNode;
begin
  if (QrHelpCab.State <> dsInactive) and (QrHelpCab.RecordCount > 0) then
  begin
    if InputQuery(Application.Title, 'Informe o t�pico', Txt) then
    begin
      Codigo   := QrHelpCabCodigo.Value;
      Controle := 0;
      //
      if Child then
        Node := TVTopicos.Selected
      else
        Node := nil;
      //
      if GrlHelp.InsUpdAjudaTopico(Dmod.MyDB, Dmod.QrUpd, stDesktop, Txt, Codigo,
        1, Controle) then
      begin
        Node := UTreeView.CriaNode(Node, Txt, Controle, TVTopicos, -1, Child);
        Node.Selected := True;
        //
        UTreeView.AtualizaDBNodesOrdem(Dmod.MyDB, Dmod.QrAux, TVTopicos,
          'helptopic', 'Nivel', 'Ordem', 'Controle', 'ParentId');
      end;
    end;
  end;
end;

procedure TFmHelpCab.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmHelpCab.QueryPrincipalAfterOpen;
begin
end;

procedure TFmHelpCab.ReopenHelpTopic(Codigo: Integer);
begin
  GrlHelp.ReopenHelpTopic(Dmod.MyDB, QrHelpTopic, Codigo);
  UTreeView.CarregaDBItensTreeView(QrHelpTopic, 'Controle', 'Nome', 'Nivel',
    TVTopicos, True);
end;

procedure TFmHelpCab.ReordenaTopicos;
begin
  if UTreeView.ReordenaNodes(TVTopicos, True) then
  begin
    UTreeView.AtualizaDBNodesOrdem(Dmod.MyDB, Dmod.QrAux, TVTopicos,
      'helptopic', 'Nivel', 'Ordem', 'Controle', 'ParentId');
  end;
end;

procedure TFmHelpCab.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmHelpCab.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmHelpCab.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmHelpCab.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmHelpCab.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmHelpCab.CarregaTutorial();
var
  Controle: Integer;
  Node: TTreeNode;
  Carrega: Boolean;
begin
  TabSheet1.Enabled := False;
  try
    Carrega := False;
    //
    if (QrHelpCab.State <> dsInactive) and (QrHelpCab.RecordCount > 0) then
    begin
      Node := TVTopicos.Selected;
      //
      if Node <> nil then
      begin
        Controle := Node.StateIndex;
        //
        if Controle <> 0 then
          if QrHelpTopic.Locate('Controle', Controle, []) then
            if QrHelpTopic.FieldByName('Texto').AsString <> '' then
              Carrega := True;
      end;
    end;
    if Carrega then
      UnHelpJan.MostraWebBrowser(WebBrowser1, QrHelpTopic.FieldByName('Texto').AsString)
    else
      UnHelpJan.FechaWebBrowser(WebBrowser1);
  finally
    TabSheet1.Enabled := True;
  end;
end;

procedure TFmHelpCab.TVTopicosClick(Sender: TObject);
begin
  CarregaTutorial();
end;

procedure TFmHelpCab.TVTopicosMouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
var
  Node: TTreeNode;
begin
  //Seleciona com o bot�o direito do mouse. O RightClickSelect n�o funciona
  if Button = mbRight then
  begin
    Node := TVTopicos.GetNodeAt(X, Y);
    //
    if Node <> nil then
      Node.Selected := True;
  end;
end;

procedure TFmHelpCab.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmHelpCab.BtSincroClick(Sender: TObject);
begin
  UnHelpJan.MostraFormSincro();
end;

procedure TFmHelpCab.BtTopIncluiClick(Sender: TObject);
begin
  CriaNode(False);
end;

procedure TFmHelpCab.BtTopReordenaClick(Sender: TObject);
begin
  ReordenaTopicos;
end;

procedure TFmHelpCab.Altera1Click(Sender: TObject);
begin
  EditaNode();
end;

procedure TFmHelpCab.BtRestringeClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMRestringe, BtRestringe);
end;

procedure TFmHelpCab.AplicativoLocal1Click(Sender: TObject);
begin
  UnHelp.IncluiHelpRestr(thtHelp, 1, QrHelpCabCodigo.Value, QrHelpRestr);
end;

procedure TFmHelpCab.AplicativoWeb1Click(Sender: TObject);
begin
  UnHelp.IncluiHelpRestr(thtHelp, 2, QrHelpCabCodigo.Value, QrHelpRestr);
end;

procedure TFmHelpCab.BtAlteraClick(Sender: TObject);
begin
  if (QrHelpCab.State <> dsInactive) and (QrHelpCab.RecordCount > 0) then
  begin
    UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrHelpCab, [PnDados],
      [PnEdita], EdNome, ImgTipo, 'helpcab');
  end;
end;

procedure TFmHelpCab.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrHelpCabCodigo.Value;
  //
  if TFmHelpCab(Self).Owner is TApplication then
    Close
  else
    MyObjects.FormTDIFecha(Self, TTabSheet(CSTabSheetChamou.Component));
end;

procedure TFmHelpCab.BtConfirmaClick(Sender: TObject);
var
  Codigo, Nivel, Status, Ativo: Integer;
  Nome: String;
begin
  if ImgTipo.SQLType = stIns then
    Codigo := 0
  else
    Codigo := QrHelpCabCodigo.Value;
  //
  Nome   := EdNome.ValueVariant;
  Status := RGStatus.ItemIndex;
  Nivel  := CGNivel.Value;
  Ativo  := Geral.BoolToInt(CkAtivo.Checked);
  //
  if GrlHelp.InsUpdAjuda(Dmod.MyDB, Dmod.QrUpd, stDesktop, Nome, Status, Nivel,
    Ativo, Codigo) then
  begin
    MostraEdicao;
    LocCod(Codigo, Codigo);
  end;
end;

procedure TFmHelpCab.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := EdCodigo.ValueVariant;
  //
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'helpcab', 'Codigo');
  //
  MostraEdicao;
end;

procedure TFmHelpCab.BtExcluiClick(Sender: TObject);
var
  Codigo: Integer;
begin
  if (QrHelpCab.State <> dsInactive) and (QrHelpCab.RecordCount > 0) then
  begin
  Codigo := QrHelpCabCodigo.Value;
  //
  GrlHelp.ExcluiHelpFaq(Dmod.MyDB, Dmod.QrAux, Codigo,
    'Confirma a exclus�o do item ID n�mero: ' + Geral.FF0(Codigo));
  end;
end;

procedure TFmHelpCab.BtIncluiClick(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrHelpCab, [PnDados],
    [PnEdita], EdNome, ImgTipo, 'helpcab');
end;

procedure TFmHelpCab.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType         := stLok;
  GBEdita.Align           := alClient;
  TVTopicos.PopupMenu     := PMTopicos;
  PCMenu.ActivePageIndex  := 0;
  DBGRestricao.DataSource := DsHelpRestr;
  WebBrowser1.Align       := alClient;
  //
  CriaOForm;
  //
  CGNivel.Items.Clear;
  DBCGNivel.Items.Clear;
  //
  CGNivel.Items.AddStrings(GrlUsuarios.ConfiguraNiveis(CO_DMKID_APP));
  DBCGNivel.Items.AddStrings(GrlUsuarios.ConfiguraNiveis(CO_DMKID_APP));
  //
  RGStatus.Items.Clear;
  RGStatus.Items.AddStrings(GrlHelp.ConfiguraStatus());
  //
  DBRGStatus.Items.Clear;
  DBRGStatus.Items.AddStrings(GrlHelp.ConfiguraStatus());
end;

procedure TFmHelpCab.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrHelpCabCodigo.Value, LaRegistro.Caption);
end;

procedure TFmHelpCab.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmHelpCab.SbNovoClick(Sender: TObject);
begin
  Geral.MB_Aviso('Pesquisa n�o dispon�vel para esta janela!');
end;

procedure TFmHelpCab.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmHelpCab.QrHelpCabAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmHelpCab.QrHelpCabAfterScroll(DataSet: TDataSet);
begin
  ReopenHelpTopic(QrHelpCabCodigo.Value);
  GrlHelp.ReopenHelpRestr(Dmod.MyDB, QrHelpRestr, QrHelpCabCodigo.Value,
    Integer(thtHelp), 0);
end;

procedure TFmHelpCab.FormActivate(Sender: TObject);
begin
  if TFmHelpCab(Self).Owner is TApplication then
  begin
    MyObjects.CorIniComponente();
  end;
end;

procedure TFmHelpCab.SbQueryClick(Sender: TObject);
begin
  LocCod(QrHelpCabCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'helpcab', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmHelpCab.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmHelpCab.FormShow(Sender: TObject);
begin
{$IfNDef cSkinRank} //Berlin
{$IfNDef cAlphaSkin} //Berlin
  FmMyGlyfs.DefineGlyfs(TForm(Sender));
{$EndIf}
{$EndIf}
{$IfDef cSkinRank} //Berlin
  if FmPrincipal.Sd1.Active then
    FmMyGlyfs.DefineGlyfsTDI(FmPrincipal.sd1, Sender);
{$EndIf}
{$IfDef cAlphaSkin} //Berlin
  if FmPrincipal.sSkinManager1.Active then
    FmMyGlyfs.DefineGlyfsTDI2(FmPrincipal.sSkinManager1, Sender);
{$EndIf}
end;

procedure TFmHelpCab.Incluisubtpico1Click(Sender: TObject);
begin
  CriaNode(True);
end;

procedure TFmHelpCab.QrHelpCabBeforeClose(DataSet: TDataSet);
begin
  QrHelpRestr.Close;
  QrHelpTopic.Close;
  TVTopicos.Items.Clear;
end;

procedure TFmHelpCab.QrHelpCabBeforeOpen(DataSet: TDataSet);
begin
  QrHelpCabCodigo.DisplayFormat := FFormatFloat;
end;

end.

