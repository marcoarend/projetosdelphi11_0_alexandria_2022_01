unit UnitHelpJan;

interface

uses
  System.DateUtils, StdCtrls, ExtCtrls, Windows, Messages, SysUtils, Classes,
  Graphics, Controls, ComCtrls, dmkGeral, Forms, Dialogs, Menus, UnMsgInt,
  UnInternalConsts, Db, DbCtrls, Variants, mySQLDbTables, UnDmkEnums, SHDocVw,
  UnDmkProcFunc, dmkCompoStore, UnGrlHelp, UnProjGroup_Consts, UnGrl_Vars;

type
  TUnitHelpJan = class(TObject)
  private
    { Private declarations }
  public
    { Public declarations }
   procedure  MostraFormWebBrowser(AbrirEmAba: Boolean; InOwner: TWincontrol;
              Pager: TWinControl; Texto: String);
   procedure  MostraWebBrowser(WebBrowser: TWebBrowser; Texto: String);
    (* Deprecado
    procedure MostraFormHelpGer(Form: TForm; AbrirEmAba: Boolean;
              InOwner: TWincontrol; Pager: TWinControl);
    *)
    procedure MostraFormHelpCab(Form: TForm; AbrirEmAba: Boolean;
              InOwner: TWincontrol; Pager: TWinControl; Codigo: Integer);
    procedure MostraFormHelpFAQ(Form: TForm; AbrirEmAba: Boolean;
              InOwner: TWincontrol; Pager: TWinControl; Codigo: Integer);
    procedure MostraFormHelpPsq(Form: TForm; AbrirEmAba: Boolean;
              InOwner: TWincontrol; Pager: TWinControl; Localiza: Boolean = True);
    procedure MostraFormHelpFAQImg(WebBrowser: TWebBrowser);
    procedure MostraFormHelpFAQVideo(WebBrowser: TWebBrowser);
    function  MostraFormHelpFAQUrl(HelpTipo: THelpTip;
              var Confirmou: Boolean): Integer; overload;
    procedure MostraFormHelpFAQUrl(WebBrowser: TWebBrowser; HelpTipo: THelpTip;
              var Confirmou: Boolean); overload;
    procedure MostraFormHelpOpc();
    procedure FechaFormWebBrowser(PageControl: TPageControl;
              TabSheetChamou: TdmkCompoStore);
    procedure FechaWebBrowser(WebBrowser: TWebBrowser);
    procedure MostraFormSincro();
  end;

var
  UnHelpJan: TUnitHelpJan;

implementation

uses Module, ModuleGeral, MyListas, UnMyObjects, DmkDAC_PF, UnDmkImg, MyDBCheck,
  HelpCab, HelpFAQ, HelpFAQImg, HelpFAQUrl, HelpOpc, UnitHelp, HelpPsq, UnDmkWeb,
  UnGrlUsuarios, WebBrowser, UnDmkWeb2_Jan, UnDmkHTML2;

{ TUnitHelpJan }

procedure TUnitHelpJan.MostraFormHelpOpc();
begin
  if DBCheck.CriaFm(TFmHelpOpc, FmHelpOpc, afmoNegarComAviso) then
  begin
    FmHelpOpc.ShowModal;
    FmHelpOpc.Destroy;
  end;
end;

procedure TUnitHelpJan.MostraFormSincro();
begin
  //SincronizaTabelas;
  DmkWeb2_Jan.MostraWSincro(CO_DMKID_APP, CO_Versao,
    DModG.QrMasterHabilModulos.Value, DmodG.QrWebParams, Dmod.MyDB, Dmod.MyDBn,
    ['helpcab', 'helpopc', 'helptopic', 'helpfaq', 'helprestr', 'helprestip'],
    [], VAR_VERIFI_DB_CANCEL);
end;

procedure TUnitHelpJan.MostraFormHelpFAQImg(WebBrowser: TWebBrowser);
var
  Img, Legenda: String;
begin
  if DBCheck.CriaFm(TFmHelpFAQImg, FmHelpFAQImg, afmoNegarComAviso) then
  begin
    FmHelpFAQImg.ShowModal;
    //
    Img     := FmHelpFAQImg.FImg;
    Legenda := FmHelpFAQImg.FLegenda;
    //
    FmHelpFAQImg.Destroy;
    //
    if Img <> '' then
      UnHelp.InserirImagem(WebBrowser, Img, Legenda);
  end;
end;

procedure TUnitHelpJan.MostraFormHelpFAQVideo(WebBrowser: TWebBrowser);
var
  Url: String;
begin
  if not InputQuery('V�deo', 'Digite o ID do v�deo:', Url) then Exit;
  //
  if Url <> '' then
      UnHelp.InserirVideo(WebBrowser, Url);
end;

function TUnitHelpJan.MostraFormHelpFAQUrl(HelpTipo: THelpTip;
  var Confirmou: Boolean): Integer;
var
  FAQ: Integer;
begin
  //thtAll
  FAQ       := 0;
  Confirmou := False;
  //
  if DBCheck.CriaFm(TFmHelpFAQUrl, FmHelpFAQUrl, afmoNegarComAviso) then
  begin
    FmHelpFAQUrl.FHelpTipo := HelpTipo;
    FmHelpFAQUrl.FLinkExt  := False;
    FmHelpFAQUrl.ShowModal;
    //
    FAQ       := FmHelpFAQUrl.FFAQ;
    Confirmou := FmHelpFAQUrl.FConfirmou;
    //
    FmHelpFAQUrl.Destroy;
  end;
  Result := FAQ;
end;

procedure TUnitHelpJan.MostraFormHelpFAQUrl(WebBrowser: TWebBrowser;
  HelpTipo: THelpTip; var Confirmou: Boolean);
var
  Url, Descri: String;
  FAQ: Integer;
begin
  //thtAll
  if DBCheck.CriaFm(TFmHelpFAQUrl, FmHelpFAQUrl, afmoNegarComAviso) then
  begin
    FmHelpFAQUrl.FHelpTipo := HelpTipo;
    FmHelpFAQUrl.FLinkExt  := True;
    FmHelpFAQUrl.ShowModal;
    //
    Url       := FmHelpFAQUrl.FUrl;
    Descri    := FmHelpFAQUrl.FDescri;
    FAQ       := FmHelpFAQUrl.FFAQ;
    Confirmou := FmHelpFAQUrl.FConfirmou;
    //
    FmHelpFAQUrl.Destroy;
    //
    if (Url <> '') or ((FAQ <> 0) or (Descri <> '')) then
      UnHelp.InserirUrl(WebBrowser, Url, Descri, FAQ);
  end;
end;

procedure TUnitHelpJan.FechaFormWebBrowser(PageControl: TPageControl;
  TabSheetChamou: TdmkCompoStore);
var
  Frm: TForm;
  Idx: Integer;
begin
  Idx := MyObjects.VerificaSeTabFormExiste(PageControl, TFmWebBrowser);
  //
  if Idx > -1 then
  begin
    Frm := TForm(Pagecontrol.Pages[Idx].Components[0]);
    //
    if Frm <> nil then
      MyObjects.FormTDIFecha(Frm, TTabSheet(TabSheetChamou.Component));
  end;
end;

procedure TUnitHelpJan.FechaWebBrowser(WebBrowser: TWebBrowser);
begin
  dmkHTML2.DocumentoEmBranco(WebBrowser);
end;

procedure TUnitHelpJan.MostraFormHelpCab(Form: TForm; AbrirEmAba: Boolean;
  InOwner: TWincontrol; Pager: TWinControl; Codigo: Integer);
var
  Frm: TForm;
begin
  if GrlUsuarios.Efetuar_Login(Form, stDesktop, Dmod.MyDB, Dmod.QrUpd,
    Dmod.QrAux, VAR_USUARIO) then
  begin
    if AbrirEmAba then
    begin
      Frm := MyObjects.FormTDICria(TFmHelpCab, InOwner, Pager, True, True);
      //
      if (Frm <> nil) and (Codigo <> 0) then
        TFmHelpCab(Frm).LocCod(Codigo, Codigo);
    end else
    begin
      if DBCheck.CriaFm(TFmHelpCab, FmHelpCab, afmoNegarComAviso) then
      begin
        if Codigo <> 0 then
          FmHelpCab.LocCod(Codigo, Codigo);
        FmHelpCab.ShowModal;
        FmHelpCab.Destroy;
      end;
    end;
  end else
  begin
    Geral.MB_Aviso('Falha ao efetuar login!');
    Exit;
  end;
end;

procedure TUnitHelpJan.MostraFormHelpFAQ(Form: TForm; AbrirEmAba: Boolean;
  InOwner: TWincontrol; Pager: TWinControl; Codigo: Integer);
var
  Frm: TForm;
begin
  if GrlUsuarios.Efetuar_Login(Form, stDesktop, Dmod.MyDB, Dmod.QrUpd,
    Dmod.QrAux, VAR_USUARIO) then
  begin
    if AbrirEmAba then
    begin
      Frm := MyObjects.FormTDICria(TFmHelpFAQ, InOwner, Pager, True, True);
      //
      if (Frm <> nil) and (Codigo <> 0) then
        TFmHelpFAQ(Frm).LocCod(Codigo, Codigo);
    end else
    begin
      if DBCheck.CriaFm(TFmHelpFAQ, FmHelpFAQ, afmoNegarComAviso) then
      begin
        if Codigo <> 0 then
          FmHelpFAQ.LocCod(Codigo, Codigo);
        FmHelpFAQ.ShowModal;
        FmHelpFAQ.Destroy;
      end;
    end;
  end else
  begin
    Geral.MB_Aviso('Falha ao efetuar login!');
    Exit;
  end;
end;

(* Deprecado
procedure TUnitHelpJan.MostraFormHelpGer(Form: TForm; AbrirEmAba: Boolean;
  InOwner: TWincontrol; Pager: TWinControl);
begin
  if GrlUsuarios.Efetuar_Login(Form, stDesktop, Dmod.MyDB, Dmod.QrUpd,
    Dmod.QrAux, VAR_USUARIO) then
  begin
    if AbrirEmAba then
    begin
      MyObjects.FormTDICria(TFmHelpGer, InOwner, Pager, True, True);
    end else
    begin
      if DBCheck.CriaFm(TFmHelpGer, FmHelpGer, afmoNegarComAviso) then
      begin
        FmHelpGer.ShowModal;
        FmHelpGer.Destroy;
      end;
    end;
  end else
  begin
    Geral.MB_Aviso('Falha ao efetuar login!');
    Exit;
  end;
end;
*)

procedure TUnitHelpJan.MostraFormHelpPsq(Form: TForm; AbrirEmAba: Boolean;
  InOwner: TWincontrol; Pager: TWinControl; Localiza: Boolean = True);
var
  Frm: TForm;
begin
  if GrlUsuarios.Efetuar_Login(Form, stDesktop, Dmod.MyDB, Dmod.QrUpd,
    Dmod.QrAux, VAR_USUARIO) then
  begin
    if AbrirEmAba then
    begin
      Frm := MyObjects.FormTDICria(TFmHelpPsq, InOwner, Pager, True, True);
      //
      if Frm <> nil then
        TFmHelpPsq(Frm).FLocaliza := Localiza;
    end else
    begin
      if DBCheck.CriaFm(TFmHelpPsq, FmHelpPsq, afmoNegarComAviso) then
      begin
        FmHelpPsq.FLocaliza := Localiza;
        FmHelpPsq.ShowModal;
        FmHelpPsq.Destroy;
      end;
    end;
  end else
  begin
    Geral.MB_Aviso('Falha ao efetuar login!');
    Exit;
  end;
end;

procedure TUnitHelpJan.MostraFormWebBrowser(AbrirEmAba: Boolean;
  InOwner: TWincontrol; Pager: TWinControl; Texto: String);
var
  Frm: TForm;
  HtmlTxt: String;
begin
  if Texto <> '' then
    HtmlTxt := GrlHelp.SubstituiTags(Texto)
  else
    HtmlTxt := '';
  //
  if AbrirEmAba then
  begin
    Frm := MyObjects.FormTDICria(TFmWebBrowser, InOwner, Pager, True, True);
    //
    if (Frm <> nil) and (HtmlTxt <> '') then
      DmkWeb.CarregaTextNoWebBrowser(TFmWebBrowser(Frm).WebBrowser1, HtmlTxt);
  end else
  begin
    if DBCheck.CriaFm(TFmWebBrowser, FmWebBrowser, afmoNegarComAviso) then
    begin
      if HtmlTxt <> '' then
        DmkWeb.CarregaTextNoWebBrowser(FmWebBrowser.WebBrowser1, HtmlTxt);
      //
      FmWebBrowser.ShowModal;
      FmWebBrowser.Destroy;
    end;
  end;
end;

procedure TUnitHelpJan.MostraWebBrowser(WebBrowser: TWebBrowser; Texto: String);
var
  HtmlTxt: String;
begin
  if Texto <> '' then
    HtmlTxt := GrlHelp.SubstituiTags(Texto)
  else
    HtmlTxt := '';
  //
  if HtmlTxt <> '' then
    DmkWeb.CarregaTextNoWebBrowser(WebBrowser, HtmlTxt);
end;

end.
