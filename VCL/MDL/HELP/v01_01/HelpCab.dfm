object FmHelpCab: TFmHelpCab
  Left = 368
  Top = 194
  Caption = 'HLP-CADAS-001 :: Cadastro de Ajudas'
  ClientHeight = 577
  ClientWidth = 805
  Color = clBtnFace
  Constraints.MinHeight = 256
  Constraints.MinWidth = 630
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object PnDados: TPanel
    Left = 0
    Top = 94
    Width = 805
    Height = 483
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    object Splitter1: TSplitter
      Left = 350
      Top = 235
      Width = 10
      Height = 169
      ExplicitLeft = 523
      ExplicitTop = 266
      ExplicitHeight = 289
    end
    object GBDados: TGroupBox
      Left = 0
      Top = 0
      Width = 805
      Height = 235
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 1
      ExplicitWidth = 1006
      object Label1: TLabel
        Left = 20
        Top = 20
        Width = 14
        Height = 13
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'ID:'
        FocusControl = DBEdCodigo
      end
      object Label2: TLabel
        Left = 94
        Top = 20
        Width = 51
        Height = 13
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Descri'#231#227'o:'
        FocusControl = DBEdNome
      end
      object DBEdCodigo: TdmkDBEdit
        Left = 20
        Top = 39
        Width = 69
        Height = 24
        Hint = 'N'#186' do banco'
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        TabStop = False
        DataField = 'Codigo'
        DataSource = DsHelpCab
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 0
        UpdType = utYes
        Alignment = taRightJustify
      end
      object DBEdNome: TdmkDBEdit
        Left = 94
        Top = 39
        Width = 851
        Height = 24
        Hint = 'Nome do banco'
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Color = clWhite
        DataField = 'Nome'
        DataSource = DsHelpCab
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        UpdType = utYes
        Alignment = taLeftJustify
      end
      object DBCheckBox2: TDBCheckBox
        Left = 20
        Top = 208
        Width = 95
        Height = 17
        Caption = 'Ativo'
        DataField = 'Ativo'
        DataSource = DsHelpCab
        TabOrder = 4
        ValueChecked = '1'
        ValueUnchecked = '0'
      end
      object DBRGStatus: TDBRadioGroup
        Left = 20
        Top = 73
        Width = 925
        Height = 45
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '  Status: '
        Columns = 3
        DataField = 'Status'
        DataSource = DsHelpCab
        TabOrder = 2
        Values.Strings = (
          '0'
          '1'
          '2')
      end
      object DBCGNivel: TdmkDBCheckGroup
        Left = 20
        Top = 126
        Width = 925
        Height = 75
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'N'#237'veis de permiss'#227'o'
        Columns = 3
        DataField = 'Nivel'
        DataSource = DsHelpCab
        Items.Strings = (
          'N'#237'vel 0'
          'N'#237'vel 1'
          'N'#237'vel 2'
          'N'#237'vel 3'
          'N'#237'vel 4')
        ParentBackground = False
        TabOrder = 3
      end
    end
    object GBCntrl: TGroupBox
      Left = 0
      Top = 404
      Width = 805
      Height = 79
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alBottom
      TabOrder = 0
      ExplicitTop = 524
      ExplicitWidth = 1006
      object Panel5: TPanel
        Left = 2
        Top = 18
        Width = 212
        Height = 59
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 158
          Top = 5
          Width = 49
          Height = 49
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 108
          Top = 5
          Width = 50
          Height = 49
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 59
          Top = 5
          Width = 49
          Height = 49
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 10
          Top = 5
          Width = 49
          Height = 49
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 214
        Top = 18
        Width = 30
        Height = 17
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel3: TPanel
        Left = 363
        Top = 18
        Width = 641
        Height = 59
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object BtExclui: TBitBtn
          Tag = 12
          Left = 310
          Top = 5
          Width = 148
          Height = 49
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Exclui'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = BtExcluiClick
        end
        object BtAltera: TBitBtn
          Tag = 11
          Left = 158
          Top = 5
          Width = 147
          Height = 49
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Altera'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtAlteraClick
        end
        object BtInclui: TBitBtn
          Tag = 10
          Left = 5
          Top = 5
          Width = 148
          Height = 49
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Inclui'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtIncluiClick
        end
        object Panel2: TPanel
          Left = 478
          Top = 0
          Width = 163
          Height = 59
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 3
          object BtSaida: TBitBtn
            Tag = 13
            Left = 5
            Top = 5
            Width = 148
            Height = 49
            Cursor = crHandPoint
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
      end
    end
    object PCMenu: TPageControl
      Left = 0
      Top = 235
      Width = 350
      Height = 169
      ActivePage = TabSheet1
      Align = alLeft
      TabHeight = 25
      TabOrder = 2
      ExplicitHeight = 289
      object TabSheet1: TTabSheet
        Caption = 'T'#243'picos'
        object TVTopicos: TTreeView
          Left = 0
          Top = 59
          Width = 342
          Height = 75
          Align = alClient
          Indent = 19
          ReadOnly = True
          RightClickSelect = True
          TabOrder = 0
          OnClick = TVTopicosClick
          OnMouseDown = TVTopicosMouseDown
          ExplicitHeight = 195
        end
        object PnTopicos: TPanel
          Left = 0
          Top = 0
          Width = 342
          Height = 59
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alTop
          TabOrder = 1
          object BtTopInclui: TBitBtn
            Tag = 10
            Left = 5
            Top = 5
            Width = 49
            Height = 49
            Cursor = crHandPoint
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtTopIncluiClick
          end
          object BtTopReordena: TBitBtn
            Tag = 73
            Left = 56
            Top = 5
            Width = 49
            Height = 49
            Cursor = crHandPoint
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 1
            OnClick = BtTopReordenaClick
          end
        end
      end
      object TabSheet2: TTabSheet
        Caption = 'Restri'#231#245'es'
        ImageIndex = 1
        object DBGRestricao: TdmkDBGrid
          Left = 0
          Top = 59
          Width = 342
          Height = 195
          Align = alClient
          Columns = <
            item
              Expanded = False
              FieldName = 'Codigo'
              Title.Caption = 'ID'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'HelpResTip_TXT'
              Title.Caption = 'Tipo'
              Width = 120
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Nome'
              Title.Caption = 'Item'
              Width = 120
              Visible = True
            end>
          Color = clWindow
          DataSource = DsHelpRestr
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -12
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'Codigo'
              Title.Caption = 'ID'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'HelpResTip_TXT'
              Title.Caption = 'Tipo'
              Width = 120
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Nome'
              Title.Caption = 'Item'
              Width = 120
              Visible = True
            end>
        end
        object Panel6: TPanel
          Left = 0
          Top = 0
          Width = 342
          Height = 59
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alTop
          TabOrder = 1
          object BtRestringe: TBitBtn
            Tag = 629
            Left = 5
            Top = 5
            Width = 49
            Height = 49
            Cursor = crHandPoint
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtRestringeClick
          end
        end
      end
    end
    object WebBrowser1: TWebBrowser
      Left = 408
      Top = 296
      Width = 510
      Height = 171
      TabOrder = 3
      ControlData = {
        4C0000002B2A0000240E00000000000000000000000000000000000000000000
        000000004C000000000000000000000001000000E0D057007335CF11AE690800
        2B2E126208000000000000004C0000000114020000000000C000000000000046
        8000000000000000000000000000000000000000000000000000000000000000
        00000000000000000100000000000000000000000000000000000000}
    end
  end
  object PnEdita: TPanel
    Left = 0
    Top = 94
    Width = 805
    Height = 483
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    Visible = False
    object GBEdita: TGroupBox
      Left = 0
      Top = 0
      Width = 1006
      Height = 281
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 0
      object Label7: TLabel
        Left = 20
        Top = 20
        Width = 14
        Height = 13
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'ID:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label9: TLabel
        Left = 94
        Top = 20
        Width = 51
        Height = 13
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Descri'#231#227'o:'
        Color = clBtnFace
        ParentColor = False
      end
      object EdCodigo: TdmkEdit
        Left = 20
        Top = 39
        Width = 69
        Height = 26
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Codigo'
        UpdCampo = 'Codigo'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdNome: TdmkEdit
        Left = 94
        Top = 39
        Width = 851
        Height = 26
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        TabOrder = 1
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Nome'
        UpdCampo = 'Nome'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object CkAtivo: TdmkCheckBox
        Left = 20
        Top = 200
        Width = 95
        Height = 17
        Caption = 'Ativo'
        TabOrder = 4
        QryCampo = 'Ativo'
        UpdCampo = 'Ativo'
        UpdType = utYes
        ValCheck = '1'
        ValUncheck = '0'
        OldValor = #0
      end
      object RGStatus: TdmkRadioGroup
        Left = 20
        Top = 73
        Width = 930
        Height = 45
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '  Status: '
        Columns = 3
        TabOrder = 2
        QryCampo = 'Status'
        UpdCampo = 'Status'
        UpdType = utYes
        OldValor = 0
      end
      object CGNivel: TdmkCheckGroup
        Left = 20
        Top = 121
        Width = 930
        Height = 75
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'N'#237'veis de permiss'#227'o'
        Columns = 3
        Items.Strings = (
          'Nivel 0'
          'N'#237'vel 1'
          'N'#237'vel 2'
          'N'#237'vel 3'
          'N'#237'vel 4')
        TabOrder = 3
        QryCampo = 'Nivel'
        UpdCampo = 'Nivel'
        UpdType = utYes
        Value = 0
        OldValor = 0
      end
    end
    object GBConfirma: TGroupBox
      Left = 0
      Top = 525
      Width = 1006
      Height = 78
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alBottom
      TabOrder = 1
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 20
        Top = 20
        Width = 147
        Height = 50
        Cursor = crHandPoint
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object Panel1: TPanel
        Left = 835
        Top = 18
        Width = 169
        Height = 58
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 9
          Top = 2
          Width = 147
          Height = 50
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 805
    Height = 51
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 947
      Top = 0
      Width = 59
      Height = 64
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 10
        Top = 15
        Width = 39
        Height = 39
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 320
      Height = 64
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 5
        Top = 10
        Width = 49
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 0
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 57
        Top = 10
        Width = 49
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 108
        Top = 10
        Width = 50
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 160
        Top = 10
        Width = 49
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 212
        Top = 10
        Width = 49
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
      object BtSincro: TBitBtn
        Tag = 73
        Left = 264
        Top = 10
        Width = 49
        Height = 49
        Cursor = crHandPoint
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 5
        OnClick = BtSincroClick
      end
    end
    object GB_M: TGroupBox
      Left = 320
      Top = 0
      Width = 627
      Height = 64
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 9
        Top = 11
        Width = 280
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Cadastro de Ajudas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 11
        Top = 14
        Width = 280
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Cadastro de Ajudas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 10
        Top = 12
        Width = 280
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Cadastro de Ajudas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 51
    Width = 805
    Height = 43
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 18
      Width = 1002
      Height = 34
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 16
        Top = 2
        Width = 15
        Height = 19
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -17
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 15
        Top = 1
        Width = 15
        Height = 19
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -17
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object QrHelpCab: TMySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrHelpCabBeforeOpen
    AfterOpen = QrHelpCabAfterOpen
    BeforeClose = QrHelpCabBeforeClose
    AfterScroll = QrHelpCabAfterScroll
    SQL.Strings = (
      'SELECT *'
      'FROM cadastro_simples')
    Left = 272
    Top = 96
    object QrHelpCabCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrHelpCabNome: TWideStringField
      DisplayWidth = 60
      FieldName = 'Nome'
      Size = 60
    end
    object QrHelpCabAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrHelpCabNivel: TIntegerField
      FieldName = 'Nivel'
    end
    object QrHelpCabStatus: TIntegerField
      FieldName = 'Status'
    end
  end
  object DsHelpCab: TDataSource
    DataSet = QrHelpCab
    Left = 300
    Top = 96
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = BtInclui
    CanUpd01 = BtAltera
    CanDel01 = BtExclui
    Left = 328
    Top = 96
  end
  object CSTabSheetChamou: TdmkCompoStore
    Left = 580
    Top = 211
  end
  object PMTopicos: TPopupMenu
    OnPopup = PMTopicosPopup
    Left = 168
    Top = 392
    object Incluisubtpico1: TMenuItem
      Caption = '&Inclui subt'#243'pico'
      OnClick = Incluisubtpico1Click
    end
    object Altera1: TMenuItem
      Caption = '&Altera'
      OnClick = Altera1Click
    end
    object Exclui1: TMenuItem
      Caption = '&Exclui'
      OnClick = Exclui1Click
    end
    object N2: TMenuItem
      Caption = '-'
    end
    object Vinculartpicoaumtutorial1: TMenuItem
      Caption = '&Vincular t'#243'pico a um tutorial'
      OnClick = Vinculartpicoaumtutorial1Click
    end
    object Abrirtutorial1: TMenuItem
      Caption = 'Abrir &tutorial'
      OnClick = Abrirtutorial1Click
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object Mover1: TMenuItem
      Caption = '&Mover'
      OnClick = Mover1Click
    end
  end
  object QrHelpTopic: TMySQLQuery
    Database = Dmod.MyDB
    Left = 56
    Top = 480
  end
  object PMRestringe: TPopupMenu
    OnPopup = PMRestringePopup
    Left = 168
    Top = 536
    object Inclui2: TMenuItem
      Caption = '&Inclui'
      object AplicativoLocal1: TMenuItem
        Caption = 'Aplicativo - &Local'
        OnClick = AplicativoLocal1Click
      end
      object AplicativoWeb1: TMenuItem
        Caption = 'Aplicativo - &Web'
        OnClick = AplicativoWeb1Click
      end
      object Mdulo1: TMenuItem
        Caption = '&M'#243'dulo'
        OnClick = Mdulo1Click
      end
    end
    object Exclui2: TMenuItem
      Caption = '&Exclui'
      OnClick = Exclui2Click
    end
  end
  object QrHelpRestr: TMySQLQuery
    Database = Dmod.MyDB
    Left = 248
    Top = 472
    object QrHelpRestrCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrHelpRestrItem: TIntegerField
      FieldName = 'Item'
    end
    object QrHelpRestrHelpResTip: TIntegerField
      FieldName = 'HelpResTip'
    end
    object QrHelpRestrNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
    object QrHelpRestrHelpResTip_TXT: TWideStringField
      FieldName = 'HelpResTip_TXT'
    end
    object QrHelpRestrHelpTip: TSmallintField
      FieldName = 'HelpTip'
    end
    object QrHelpRestrHelp: TIntegerField
      FieldName = 'Help'
    end
  end
  object DsHelpRestr: TDataSource
    DataSet = QrHelpRestr
    Left = 276
    Top = 472
  end
end
