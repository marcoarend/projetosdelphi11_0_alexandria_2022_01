object FmWOrdSerAte: TFmWOrdSerAte
  Left = 368
  Top = 194
  Caption = 'WEB-ORSER-012 :: Atendentes da Ordem de Servi'#231'o'
  ClientHeight = 432
  ClientWidth = 864
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnEdita: TPanel
    Left = 0
    Top = 96
    Width = 864
    Height = 336
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    Visible = False
    object GBEdita: TGroupBox
      Left = 0
      Top = 0
      Width = 864
      Height = 89
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 0
      object Label7: TLabel
        Left = 16
        Top = 16
        Width = 14
        Height = 13
        Caption = 'ID:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label9: TLabel
        Left = 78
        Top = 16
        Width = 52
        Height = 13
        Caption = 'Atendente:'
        FocusControl = DBEdNome
      end
      object EdCodigo: TdmkEdit
        Left = 16
        Top = 32
        Width = 56
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Codigo'
        UpdCampo = 'Codigo'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object CBAtendente: TdmkDBLookupComboBox
        Left = 135
        Top = 32
        Width = 630
        Height = 21
        KeyField = 'Codigo'
        ListField = 'PersonalName'
        ListSource = DsAtendente
        TabOrder = 2
        dmkEditCB = EdAtendente
        QryCampo = 'Atendente'
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object EdAtendente: TdmkEditCB
        Left = 78
        Top = 32
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 1
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Atendente'
        UpdCampo = 'Atendente'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBAtendente
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CkAtivo: TdmkCheckBox
        Left = 16
        Top = 59
        Width = 45
        Height = 17
        Caption = 'Ativo'
        TabOrder = 3
        QryCampo = 'Ativo'
        UpdCampo = 'Ativo'
        UpdType = utYes
        ValCheck = '1'
        ValUncheck = '0'
        OldValor = #0
      end
      object CkContinuar: TCheckBox
        Left = 71
        Top = 59
        Width = 121
        Height = 17
        Caption = 'Continuar inserindo.'
        TabOrder = 4
      end
    end
    object GBConfirma: TGroupBox
      Left = 0
      Top = 273
      Width = 864
      Height = 63
      Align = alBottom
      TabOrder = 1
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 12
        Top = 17
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object Panel1: TPanel
        Left = 724
        Top = 15
        Width = 138
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 7
          Top = 2
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
    end
  end
  object PnDados: TPanel
    Left = 0
    Top = 96
    Width = 864
    Height = 336
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    object GBDados: TGroupBox
      Left = 0
      Top = 0
      Width = 864
      Height = 55
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 1
      object Label1: TLabel
        Left = 16
        Top = 8
        Width = 14
        Height = 13
        Caption = 'ID:'
        FocusControl = DBEdCodigo
      end
      object Label2: TLabel
        Left = 76
        Top = 8
        Width = 52
        Height = 13
        Caption = 'Atendente:'
        FocusControl = DBEdNome
      end
      object DBEdCodigo: TdmkDBEdit
        Left = 16
        Top = 24
        Width = 56
        Height = 21
        Hint = 'N'#186' do banco'
        TabStop = False
        DataField = 'Codigo'
        DataSource = DsWOrdSerAte
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 0
        UpdType = utYes
        Alignment = taRightJustify
      end
      object DBEdNome: TdmkDBEdit
        Left = 76
        Top = 24
        Width = 688
        Height = 21
        Color = clWhite
        DataField = 'NOMEATEN'
        DataSource = DsWOrdSerAte
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        UpdType = utYes
        Alignment = taLeftJustify
      end
      object DBCheckBox2: TDBCheckBox
        Left = 771
        Top = 26
        Width = 50
        Height = 17
        Caption = 'Ativo'
        DataField = 'Ativo'
        DataSource = DsWOrdSerAte
        TabOrder = 2
        ValueChecked = '1'
        ValueUnchecked = '0'
      end
    end
    object GBCntrl: TGroupBox
      Left = 0
      Top = 272
      Width = 864
      Height = 64
      Align = alBottom
      TabOrder = 0
      object Panel5: TPanel
        Left = 2
        Top = 15
        Width = 172
        Height = 47
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 174
        Top = 15
        Width = 30
        Height = 17
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel3: TPanel
        Left = 341
        Top = 15
        Width = 521
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object BtParametros: TBitBtn
          Left = 126
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Par'#226'metros'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtParametrosClick
        end
        object BtAtendente: TBitBtn
          Tag = 133
          Left = 4
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Atendente'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtAtendenteClick
        end
        object Panel2: TPanel
          Left = 388
          Top = 0
          Width = 133
          Height = 47
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 2
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
        object BtGrupos: TBitBtn
          Left = 248
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Grupos'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = BtGruposClick
        end
      end
    end
    object PageControl1: TPageControl
      Left = 0
      Top = 55
      Width = 864
      Height = 178
      ActivePage = TabSheet1
      Align = alTop
      TabHeight = 25
      TabOrder = 2
      object TabSheet1: TTabSheet
        Caption = 'Par'#226'metros'
        object dmkDBGrid2: TdmkDBGrid
          Left = 0
          Top = 0
          Width = 856
          Height = 143
          Align = alClient
          Columns = <
            item
              Expanded = False
              FieldName = 'Controle'
              Title.Caption = 'ID'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Solicit_TXT'
              Title.Caption = 'Solicitante'
              Width = 150
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Cliente_TXT'
              Title.Caption = 'Cliente'
              Width = 150
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Aplicativo_TXT'
              Title.Caption = 'Aplicativo'
              Width = 150
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Assunto_TXT'
              Title.Caption = 'Assunto'
              Width = 150
              Visible = True
            end>
          Color = clWindow
          DataSource = DsWOrSeAteIt
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'Controle'
              Title.Caption = 'ID'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Solicit_TXT'
              Title.Caption = 'Solicitante'
              Width = 150
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Cliente_TXT'
              Title.Caption = 'Cliente'
              Width = 150
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Aplicativo_TXT'
              Title.Caption = 'Aplicativo'
              Width = 150
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Assunto_TXT'
              Title.Caption = 'Assunto'
              Width = 150
              Visible = True
            end>
        end
      end
      object TabSheet2: TTabSheet
        Caption = 'Grupos'
        ImageIndex = 1
        object dmkDBGrid3: TdmkDBGrid
          Left = 0
          Top = 0
          Width = 856
          Height = 143
          Align = alClient
          Columns = <
            item
              Expanded = False
              FieldName = 'Controle'
              Title.Caption = 'ID'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Grupo_TXT'
              Title.Caption = 'Grupo'
              Width = 250
              Visible = True
            end>
          Color = clWindow
          DataSource = DsWOrSeAteGr
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'Controle'
              Title.Caption = 'ID'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Grupo_TXT'
              Title.Caption = 'Grupo'
              Width = 250
              Visible = True
            end>
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 864
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 776
      Top = 0
      Width = 88
      Height = 52
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 48
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
      object ImgWEB: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 216
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 216
      Top = 0
      Width = 560
      Height = 52
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 402
        Height = 32
        Caption = 'Atendentes da Ordem de Servi'#231'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 402
        Height = 32
        Caption = 'Atendentes da Ordem de Servi'#231'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 402
        Height = 32
        Caption = 'Atendentes da Ordem de Servi'#231'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 52
    Width = 864
    Height = 44
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 860
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object QrWOrdSerAte: TMySQLQuery
    Database = Dmod.MyDBn
    BeforeOpen = QrWOrdSerAteBeforeOpen
    AfterOpen = QrWOrdSerAteAfterOpen
    BeforeClose = QrWOrdSerAteBeforeClose
    AfterScroll = QrWOrdSerAteAfterScroll
    SQL.Strings = (
      'SELECT ate.*, wus.PersonalName NOMEATEN'
      'FROM worseate ate'
      'LEFT JOIN wusers wus ON wus.Codigo = ate.Atendente')
    Left = 64
    Top = 64
    object QrWOrdSerAteCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrWOrdSerAteAtendente: TIntegerField
      FieldName = 'Atendente'
    end
    object QrWOrdSerAteLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrWOrdSerAteDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrWOrdSerAteDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrWOrdSerAteUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrWOrdSerAteUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrWOrdSerAteAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrWOrdSerAteAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrWOrdSerAteNOMEATEN: TWideStringField
      FieldName = 'NOMEATEN'
      Size = 100
    end
  end
  object DsWOrdSerAte: TDataSource
    DataSet = QrWOrdSerAte
    Left = 92
    Top = 64
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = Inclui1
    CanIns02 = Inclui2
    CanUpd01 = Altera1
    CanDel01 = Exclui1
    CanDel02 = Exclui2
    Left = 120
    Top = 64
  end
  object QrWOrSeAteIt: TMySQLQuery
    Database = Dmod.MyDBn
    SQL.Strings = (
      'SELECT wit.*, ass.Nome Assunto_TXT, '
      'IF(ent.Tipo = 0, ent.RazaoSocial, ent.Nome) Cliente_TXT,'
      'wus.PersonalName Solicit_TXT, apl.Nome Aplicativo_TXT'#11
      'FROM worseateit wit'
      'LEFT JOIN wordserass ass ON ass.Codigo = wit.Assunto'
      'LEFT JOIN entidades ent ON ent.Codigo = wit.Cliente'
      'LEFT JOIN wusers wus ON wus.Codigo = wit.Solicit'
      'LEFT JOIN aplicativos apl ON apl.Codigo = wit.Aplicativo'
      'WHERE wit.Codigo=:P0')
    Left = 148
    Top = 64
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrWOrSeAteItCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrWOrSeAteItControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrWOrSeAteItAssunto_TXT: TWideStringField
      FieldName = 'Assunto_TXT'
      Size = 100
    end
    object QrWOrSeAteItCliente_TXT: TWideStringField
      FieldName = 'Cliente_TXT'
      Size = 100
    end
    object QrWOrSeAteItSolicit_TXT: TWideStringField
      FieldName = 'Solicit_TXT'
      Size = 32
    end
    object QrWOrSeAteItAplicativo_TXT: TWideStringField
      FieldName = 'Aplicativo_TXT'
      Size = 50
    end
  end
  object DsWOrSeAteIt: TDataSource
    DataSet = QrWOrSeAteIt
    Left = 176
    Top = 64
  end
  object DsAtendente: TDataSource
    DataSet = QrAtendente
    Left = 678
    Top = 121
  end
  object QrLoc: TMySQLQuery
    Database = Dmod.MyDBn
    Left = 706
    Top = 121
  end
  object QrAtendente: TMySQLQuery
    Database = Dmod.MyDBn
    SQL.Strings = (
      'SELECT Codigo, PersonalName'
      'FROM wusers'
      'WHERE Ativo = 1'
      'AND Tipo IN(0, 1)'
      'ORDER BY PersonalName')
    Left = 650
    Top = 121
    object QrAtendenteCodigo: TAutoIncField
      FieldName = 'Codigo'
    end
    object QrAtendentePersonalName: TWideStringField
      FieldName = 'PersonalName'
      Size = 32
    end
  end
  object PMAtendente: TPopupMenu
    OnPopup = PMAtendentePopup
    Left = 384
    Top = 342
    object Inclui1: TMenuItem
      Caption = '&Inclui'
      OnClick = Inclui1Click
    end
    object Altera1: TMenuItem
      Caption = '&Altera'
      OnClick = Altera1Click
    end
    object Exclui1: TMenuItem
      Caption = '&Exclui'
    end
  end
  object PMParametros: TPopupMenu
    Left = 520
    Top = 342
    object Inclui2: TMenuItem
      Caption = '&Inclui'
      OnClick = Inclui2Click
    end
    object Exclui2: TMenuItem
      Caption = '&Exclui'
      OnClick = Exclui2Click
    end
  end
  object PMGrupos: TPopupMenu
    OnPopup = PMGruposPopup
    Left = 640
    Top = 344
    object Inclui3: TMenuItem
      Caption = '&Inclui'
      OnClick = Inclui3Click
    end
    object Exclui3: TMenuItem
      Caption = '&Exclui'
      OnClick = Exclui3Click
    end
  end
  object QrWOrSeAteGr: TMySQLQuery
    Database = Dmod.MyDBn
    Left = 204
    Top = 64
    object QrWOrSeAteGrCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrWOrSeAteGrControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrWOrSeAteGrGrupo_TXT: TWideStringField
      FieldName = 'Grupo_TXT'
      Size = 50
    end
  end
  object DsWOrSeAteGr: TDataSource
    DataSet = QrWOrSeAteGr
    Left = 232
    Top = 64
  end
end
