unit WOrdSerAss;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2,
  ResIntStrings, UnMLAGeral, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, dmkGeral,
  dmkPermissoes, dmkEdit, dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup,
  unDmkProcFunc, dmkValUsu, dmkCheckBox, dmkDBLookupComboBox, dmkEditCB,
  DmkDAC_PF, dmkMemo, UnDmkEnums, dmkCheckGroup, Vcl.OleCtrls, SHDocVw,
  UnProjGroup_Consts;

type
  TFmWOrdSerAss = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    BtExclui: TBitBtn;
    BtAltera: TBitBtn;
    BtInclui: TBitBtn;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    Label9: TLabel;
    EdNome: TdmkEdit;
    Label1: TLabel;
    DBEdCodigo: TdmkDBEdit;
    Label2: TLabel;
    DBEdNome: TdmkDBEdit;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    QrWOrdSerAss: TmySQLQuery;
    QrWOrdSerAssCodigo: TIntegerField;
    QrWOrdSerAssLk: TIntegerField;
    QrWOrdSerAssDataCad: TDateField;
    QrWOrdSerAssDataAlt: TDateField;
    QrWOrdSerAssUserCad: TIntegerField;
    QrWOrdSerAssUserAlt: TIntegerField;
    QrWOrdSerAssAlterWeb: TSmallintField;
    QrWOrdSerAssAtivo: TSmallintField;
    QrWOrdSerAssNome: TWideStringField;
    DsWOrdSerAss: TDataSource;
    DBCheckBox1: TDBCheckBox;
    CkAtivo: TdmkCheckBox;
    QrLoc: TmySQLQuery;
    ImgWEB: TdmkImage;
    Label8: TLabel;
    dmkDBEdit2: TdmkDBEdit;
    Label10: TLabel;
    EdOrdem: TdmkEdit;
    QrWOrdSerAssOrdem: TIntegerField;
    QrWOrdSerAssGrupo_TXT: TWideStringField;
    LaStatus: TLabel;
    EdGrupo: TdmkEditCB;
    CBGrupo: TdmkDBLookupComboBox;
    SBStatus: TSpeedButton;
    Label3: TLabel;
    dmkDBEdit1: TdmkDBEdit;
    QrWOSAssGru: TmySQLQuery;
    DsWOSAssGru: TDataSource;
    QrWOSAssGruCodigo: TIntegerField;
    QrWOSAssGruNome: TWideStringField;
    QrWOrdSerAssGrupo: TIntegerField;
    DBCGNivelArq: TdmkDBCheckGroup;
    QrWOrdSerAssNivel: TIntegerField;
    CGNiveis: TdmkCheckGroup;
    QrWTextos: TmySQLQuery;
    DsWTextos: TDataSource;
    QrWTextosCodigo: TIntegerField;
    QrWTextosNome: TWideStringField;
    Label5: TLabel;
    EdTermo: TdmkEditCB;
    CBTermo: TdmkDBLookupComboBox;
    SBTermo: TSpeedButton;
    QrWOrdSerAssTermo: TIntegerField;
    DBCheckBox2: TDBCheckBox;
    CkSementeVersaoAtu: TdmkCheckBox;
    QrWOrdSerAssSomenteVersaoAtu: TSmallintField;
    Label6: TLabel;
    dmkDBEdit3: TdmkDBEdit;
    Label11: TLabel;
    dmkDBEdit4: TdmkDBEdit;
    QrWOrdSerAssTermo_TXT: TWideStringField;
    SBTermoView: TSpeedButton;
    Label4: TLabel;
    EdTarifa: TdmkEditCB;
    CBTarifa: TdmkDBLookupComboBox;
    SBTarifa: TSpeedButton;
    QrTarifas: TmySQLQuery;
    IntegerField1: TIntegerField;
    StringField1: TWideStringField;
    DsTarifas: TDataSource;
    QrWOrdSerAssSrvTarifa_TXT: TWideStringField;
    QrWOrdSerAssTarifa: TIntegerField;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtIncluiClick(Sender: TObject);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure QrWOrdSerAssAfterOpen(DataSet: TDataSet);
    procedure QrWOrdSerAssBeforeOpen(DataSet: TDataSet);
    procedure SbImprimeClick(Sender: TObject);
    procedure BtExcluiClick(Sender: TObject);
    procedure SBStatusClick(Sender: TObject);
    procedure SBTermoClick(Sender: TObject);
    procedure CBTermoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdTermoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SBTarifaClick(Sender: TObject);
    procedure SBTermoViewClick(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    procedure ReopenWTextos();
  public
    { Public declarations }
    procedure LocCod(Atual, Codigo: Integer);
  end;

var
  FmWOrdSerAss: TFmWOrdSerAss;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, ModuleGeral, UnDmkWeb, ModWOrdSer, MyListas,
  UnWTextos_Jan, ServTarifas, MyDBCheck, UnGrlUsuarios;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmWOrdSerAss.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmWOrdSerAss.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrWOrdSerAssCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmWOrdSerAss.DefParams;
begin
  VAR_GOTOTABELA := 'wordserass';
  VAR_GOTOMYSQLTABLE := QrWOrdSerAss;
  VAR_GOTONEG := gotoPos;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDBn;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT ass.*, wte.Nome Termo_TXT, gru.Nome Grupo_TXT, ');
  VAR_SQLx.Add('srv.Nome SrvTarifa_TXT ');
  VAR_SQLx.Add('FROM wordserass ass ');
  VAR_SQLx.Add('LEFT JOIN wosassgru gru ON gru.Codigo = ass.Grupo ');
  VAR_SQLx.Add('LEFT JOIN wtextos wte ON wte.Codigo = ass.Termo ');
  VAR_SQLx.Add('LEFT JOIN srvtarifa srv ON srv.Codigo = ass.Tarifa ');
  VAR_SQLx.Add('WHERE ass.Codigo > 0 ');
  //
  VAR_SQL1.Add('AND ass.Codigo=:P0');
  //
  //VAR_SQL2.Add('AND ass.CodUsu=:P0');
  //
  VAR_SQLa.Add('AND ass.Nome Like :P0');
  //
end;

procedure TFmWOrdSerAss.EdTermoKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F4 then ReopenWTextos;
end;

procedure TFmWOrdSerAss.CBTermoKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F4 then ReopenWTextos;
end;

procedure TFmWOrdSerAss.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmWOrdSerAss.QrWOrdSerAssAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmWOrdSerAss.QrWOrdSerAssBeforeOpen(DataSet: TDataSet);
begin
  QrWOrdSerAssCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmWOrdSerAss.QueryPrincipalAfterOpen;
begin
end;

procedure TFmWOrdSerAss.ReopenWTextos;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrWTextos, Dmod.MyDBn, [
    'SELECT Codigo, Nome ',
    'FROM wtextos ',
    'WHERE Ativo = 1 ',
    'AND Publicado = 1',
    'ORDER BY Nome ',
    '']);
end;

procedure TFmWOrdSerAss.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmWOrdSerAss.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmWOrdSerAss.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmWOrdSerAss.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmWOrdSerAss.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmWOrdSerAss.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmWOrdSerAss.BtAlteraClick(Sender: TObject);
begin
  if (QrWOrdSerAss.State <> dsInactive) and (QrWOrdSerAss.RecordCount > 0) and
    (QrWOrdSerAssCodigo.Value <> 0) then
  begin
    UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrWOrdSerAss, [PnDados],
      [PnEdita], EdNome, ImgTipo, 'wordserass');
  end;
end;

procedure TFmWOrdSerAss.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrWOrdSerAssCodigo.Value;
  Close;
end;

procedure TFmWOrdSerAss.BtConfirmaClick(Sender: TObject);
var
  Codigo: Integer;
  Nome: String;
begin
  Nome := EdNome.ValueVariant;
  //
  if MyObjects.FIC(Length(Nome) = 0, EdNome, 'Defina um nome!') then Exit;
  //
  if ImgTipo.SQLType = stIns then
    Codigo := UMyMod.BuscaNovoCodigo_Int(Dmod.QrUpdN, 'wordserass', 'Codigo',
      [], [], stIns, 0, siPositivo, nil)
  else
    Codigo := QrWOrdSerAssCodigo.Value;
  //
  if UMyMod.ExecSQLInsUpdPanel(ImgTipo.SQLType, Self, PnEdita,
    'wordserass', Codigo, Dmod.QrUpdN, [PnEdita], [PnDados], ImgTipo, True) then
  begin
    LocCod(Codigo, Codigo);
  end;
end;

procedure TFmWOrdSerAss.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo          := EdCodigo.ValueVariant;
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  //
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDBn, 'wordserass', 'Codigo');
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmWOrdSerAss.BtExcluiClick(Sender: TObject);
var
  Codigo: Integer;
begin
  if (QrWOrdSerAss.State <> dsInactive) and (QrWOrdSerAss.RecordCount > 0) then
  begin
    Codigo := QrWOrdSerAssCodigo.Value;
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrLoc, Dmod.MyDBn, [
    'SELECT * ',
    'FROM wordser ',
    'WHERE Assunto=' + Geral.FF0(Codigo),
    '']);
    if QrLoc.RecordCount > 0 then
    begin
      Geral.MB_Aviso('Este assunto j� foi utilizado nas solicita��es e por isso n�o poder� ser exclu�-do!');
      Exit;
    end;
    UnDmkDAC_PF.AbreMySQLQuery0(QrLoc, Dmod.MyDBn, [
    'SELECT Servico ',
    'FROM waplmodser ',
    'WHERE Servico=' + Geral.FF0(Codigo),
    '']);
    if QrLoc.RecordCount > 0 then
    begin
      Geral.MB_Aviso('Este assunto j� foi em uma lista de servi�os e por isso n�o poder� ser exclu�-do!');
      Exit;
    end;
    UMyMod.ExcluiRegistroInt1('Confirma a exclus�o deste item?',
      'wordserass', 'Codigo', Codigo, DMod.MyDBn);
    //
    QrWOrdSerAss.Close;
    QrWOrdSerAss.Open;
    //
    Va(vpLast);
  end;
end;

procedure TFmWOrdSerAss.BtIncluiClick(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrWOrdSerAss, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'wordserass');
end;

procedure TFmWOrdSerAss.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  GBEdita.Align   := alClient;
  GBDados.Align   := alClient;
  //
  UMyMod.AbreQuery(QrWOSAssGru, Dmod.MyDBn);
  UMyMod.AbreQuery(QrTarifas, Dmod.MyDB); //Par ficar mais r�pido
  //
  CriaOForm;
  //
  //
  DBCGNivelArq.Items.Clear;
  DBCGNivelArq.Items.AddStrings(GrlUsuarios.ConfiguraNiveis(CO_DMKID_APP));
  //
  CGNiveis.Items.Clear;
  CGNiveis.Items.AddStrings(GrlUsuarios.ConfiguraNiveis(CO_DMKID_APP));
  //
  ReopenWTextos;
end;

procedure TFmWOrdSerAss.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrWOrdSerAssCodigo.Value, LaRegistro.Caption);
end;

procedure TFmWOrdSerAss.SbImprimeClick(Sender: TObject);
begin
  Geral.MB_Info('Impress�o indispon�vel para esta janela!');
end;

procedure TFmWOrdSerAss.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmWOrdSerAss.SbNovoClick(Sender: TObject);
begin
  Geral.MB_Info('Tipo de pesquisa indispon�vel para esta janela!');
end;

procedure TFmWOrdSerAss.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmWOrdSerAss.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  DmkWeb.CarregaImagemFormWEB(ImgWEB);
end;

procedure TFmWOrdSerAss.SbQueryClick(Sender: TObject);
begin
  LocCod(QrWOrdSerAssCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'wordserass', Dmod.MyDBn, CO_VAZIO));
end;

procedure TFmWOrdSerAss.SBStatusClick(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  //
  DModWOrdSer.MostraWOrdSerAssGru(EdGrupo.ValueVariant);
  //
  if VAR_CADASTRO > 0 then
  begin
    UMyMod.AbreQuery(QrWOSAssGru, Dmod.MyDBn);
    //
    EdGrupo.ValueVariant := VAR_CADASTRO;
    CBGrupo.KeyValue     := VAR_CADASTRO;
    EdGrupo.SetFocus;
  end;
end;

procedure TFmWOrdSerAss.SBTarifaClick(Sender: TObject);
var
  Tarifa: Integer;
begin
  VAR_CADASTRO := 0;
  Tarifa       := EdTarifa.ValueVariant;
  //
  if DBCheck.CriaFm(TFmServTarifas, FmServTarifas, afmoNegarComAviso) then
  begin
    if Tarifa <> 0 then
      FmServTarifas.LocCod(Tarifa, Tarifa);
    FmServTarifas.ShowModal;
    FmServTarifas.Destroy;
    //
    if VAR_CADASTRO > 0 then
    begin
      UMyMod.AbreQuery(QrTarifas, Dmod.MyDB);
      //
      EdTarifa.ValueVariant := VAR_CADASTRO;
      CBTarifa.KeyValue     := VAR_CADASTRO;
      EdTarifa.SetFocus;
    end;
  end;
end;

procedure TFmWOrdSerAss.SBTermoClick(Sender: TObject);
var
  Termo: Integer;
begin
  VAR_CADASTRO := 0;
  Termo        := EdTermo.ValueVariant;
  //
  if DmkWeb.ConexaoRemota(Dmod.MyDBn, DModG.QrWebParams, 1) then
  begin
    WTextos_Jan.MostraFormWTermos(FmWOrdSerAss, Termo);
    //
    if VAR_CADASTRO > 0 then
    begin
      UMyMod.AbreQuery(QrWTextos, Dmod.MyDBn);
      //
      EdTermo.ValueVariant := VAR_CADASTRO;
      CBTermo.KeyValue     := VAR_CADASTRO;
      EdTermo.SetFocus;
    end;
  end;
end;

procedure TFmWOrdSerAss.SBTermoViewClick(Sender: TObject);
var
  Id: Integer;
begin
  if (QrWOrdSerAss.State <> dsInactive) and (QrWOrdSerAss.RecordCount > 0) and
    (QrWOrdSerAssTermo.Value <> 0) then
  begin
    Id := QrWOrdSerAssTermo.Value;
    //
    DmkWeb.MostraWebBrowser('http://www.dermatek.net.br/?page=termos&id=' +
      Geral.FF0(Id), True, False, 0, 0);
  end;
end;

procedure TFmWOrdSerAss.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

end.
