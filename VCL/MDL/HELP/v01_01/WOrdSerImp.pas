unit WOrdSerImp;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, Grids, DBGrids, dmkLabel,
  dmkRadioGroup, ComCtrls, frxClass, DB, mySQLDbTables, frxDBSet, dmkGeral,
  dmkEditDateTimePicker, UnDmkProcFunc, DmkDAC_PF, dmkPermissoes;

type
  TFmWOrdSerImp = class(TForm)
    PainelConfirma: TPanel;
    BtOK: TBitBtn;
    PainelTitulo: TPanel;
    Image1: TImage;
    Panel1: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    RGTipo: TdmkRadioGroup;
    GBEmissao: TGroupBox;
    CkAbeIni: TCheckBox;
    CkAbeFim: TCheckBox;
    frxWEB_ORSER_010_001: TfrxReport;
    Qr1Gra: TmySQLQuery;
    Ds1Gra: TDataSource;
    frxDs1Gra: TfrxDBDataset;
    Qr1GraCodigo: TLargeintField;
    Qr1GraAbertura: TWideStringField;
    Ds1Its: TDataSource;
    Qr1Its: TmySQLQuery;
    frxWEB_ORSER_010_002: TfrxReport;
    frxDs1Its: TfrxDBDataset;
    frxDs2Gra: TfrxDBDataset;
    Qr2Gra: TmySQLQuery;
    Ds2Gra: TDataSource;
    Qr2GraCodigo: TLargeintField;
    Qr2GraNOMECLI: TWideStringField;
    Qr2Its: TmySQLQuery;
    Ds2Its: TDataSource;
    frxDs2Its: TfrxDBDataset;
    Qr2ItsCodigo: TLargeintField;
    Qr2ItsCliente: TIntegerField;
    Qr2ItsNOMECLI: TWideStringField;
    Qr2ItsNOMESOLICIT: TWideStringField;
    frxWEB_ORSER_010_003: TfrxReport;
    frxDs3Gra: TfrxDBDataset;
    Qr3Gra: TmySQLQuery;
    Ds3Gra: TDataSource;
    Qr3GraCodigo: TLargeintField;
    frxDs3Its: TfrxDBDataset;
    Ds3Its: TDataSource;
    Qr3Its: TmySQLQuery;
    Qr3ItsCodigo: TLargeintField;
    Qr3ItsMES: TWideStringField;
    frxWEB_ORSER_010_004: TfrxReport;
    frxDs4Gra: TfrxDBDataset;
    frxDs4Its: TfrxDBDataset;
    Qr4Its: TmySQLQuery;
    Ds4Its: TDataSource;
    Ds4Gra: TDataSource;
    Qr4Gra: TmySQLQuery;
    Qr4GraNOMEMOD: TWideStringField;
    Qr4ItsNOMEMODO: TWideStringField;
    Qr4ItsMES: TWideStringField;
    Qr3GraMES: TWideStringField;
    Qr2Tot: TmySQLQuery;
    Ds2Tot: TDataSource;
    Qr2TotCodigo: TLargeintField;
    Qr2ItsCodCli: TLargeintField;
    frxDs2Tot: TfrxDBDataset;
    Qr2ItsPORCENT: TFloatField;
    Qr2GraPORCENT: TWideStringField;
    TPAbeIni: TdmkEditDateTimePicker;
    TPAbeFim: TdmkEditDateTimePicker;
    Qr1GraPORCENT: TWideStringField;
    Qr1ItsAbertura_TXT: TWideStringField;
    Qr1ItsMES: TWideStringField;
    Qr1ItsCodigo: TLargeintField;
    Qr1ItsPORCENT: TFloatField;
    Qr3GraPORCENT: TWideStringField;
    Qr3ItsPORCENT: TFloatField;
    Qr4ItsPORCENT: TFloatField;
    Qr4GraPORCENT: TWideStringField;
    Qr4GraCodigo: TLargeintField;
    Qr4ItsCodigo: TLargeintField;
    Qr3GraNOMEASS: TWideStringField;
    Qr3ItsNOMEASS: TWideStringField;
    Qr5Its: TmySQLQuery;
    StringField2: TWideStringField;
    Ds5Its: TDataSource;
    Ds5Gra: TDataSource;
    Qr5Gra: TmySQLQuery;
    frxWEB_ORSER_010_005: TfrxReport;
    frxDs5Its: TfrxDBDataset;
    frxDs5Gra: TfrxDBDataset;
    Qr5GraEncerr: TWideStringField;
    Qr5ItsEncerr_TXT: TWideStringField;
    Qr5GraCodigo: TLargeintField;
    Qr5GraPORCENT: TWideStringField;
    Qr5ItsCodigo: TLargeintField;
    Qr5ItsPORCENT: TFloatField;
    Qr5Tot: TmySQLQuery;
    Ds5Tot: TDataSource;
    frxDs5Tot: TfrxDBDataset;
    Qr5TotCodigo: TLargeintField;
    dmkPermissoes1: TdmkPermissoes;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure frxWEB_ORSER_010_001NewGetValue(Sender: TObject;
      const VarName: string; var Value: Variant);
    procedure frxWEB_ORSER_010_002NewGetValue(Sender: TObject;
      const VarName: string; var Value: Variant);
    procedure Qr2ItsCalcFields(DataSet: TDataSet);
    procedure Qr2GraCalcFields(DataSet: TDataSet);
    procedure Qr1GraCalcFields(DataSet: TDataSet);
    procedure Qr1ItsCalcFields(DataSet: TDataSet);
    procedure Qr3GraCalcFields(DataSet: TDataSet);
    procedure Qr3ItsCalcFields(DataSet: TDataSet);
    procedure Qr4GraCalcFields(DataSet: TDataSet);
    procedure Qr4ItsCalcFields(DataSet: TDataSet);
    procedure Qr5ItsCalcFields(DataSet: TDataSet);
    procedure Qr5GraCalcFields(DataSet: TDataSet);
  private
    { Private declarations }
    procedure Reopen1Gra();
    procedure Reopen1Its();
    procedure Reopen2Gra();
    procedure Reopen2Its();
    procedure Reopen2Tot();
    procedure Reopen3Gra();
    procedure Reopen3Its();
    procedure Reopen4Gra();
    procedure Reopen4Its();
    procedure Reopen5Gra();
    procedure Reopen5Its();
    procedure Reopen5Tot();
  public
    { Public declarations }
  end;

  var
  FmWOrdSerImp: TFmWOrdSerImp;

implementation

uses ModuleGeral, UnInternalConsts, UnMyObjects, Module;

{$R *.DFM}

procedure TFmWOrdSerImp.BtOKClick(Sender: TObject);
begin
  DModG.ReopenEndereco(Geral.IMV(VAR_LIB_EMPRESAS));
  case RGTipo.ItemIndex of
      0:
      begin
        Reopen1Gra;
        Reopen1Its;
        Reopen2Tot;
        //
        MyObjects.frxMostra(frxWEB_ORSER_010_001, 'O. S. - Insidentes no per�odo');
      end;
      1:
      begin
        Reopen2Gra;
        Reopen2Its;
        Reopen2Tot;
        //
        MyObjects.frxMostra(frxWEB_ORSER_010_002, 'O. S. - Clientes mais atuantes');
      end;
      2:
      begin
        Reopen3Gra;
        Reopen3Its;
        Reopen2Tot;
        //
        MyObjects.frxMostra(frxWEB_ORSER_010_003, 'O. S. - Servi�os mais utilizados');
      end;
      3:
      begin
        Reopen4Gra;
        Reopen4Its;
        Reopen2Tot;
        //
        MyObjects.frxMostra(frxWEB_ORSER_010_004, 'O. S. - Modos de solicita��o mais utilizados');
      end;
      4:
      begin
        Reopen5Gra;
        Reopen5Its;
        Reopen5Tot;
        //
        MyObjects.frxMostra(frxWEB_ORSER_010_005, 'O. S. - Insidentes finalizados no per�odo');
      end
    else
      if MyObjects.FIC(RGTipo.ItemIndex < 0, RGTipo, 'Defina o tipo de relat�rio') then Exit;
  end;
end;

procedure TFmWOrdSerImp.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmWOrdSerImp.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
end;

procedure TFmWOrdSerImp.FormCreate(Sender: TObject);
begin
  TPAbeIni.Date := Date;
  TPAbeFim.Date := Date;
end;

procedure TFmWOrdSerImp.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

procedure TFmWOrdSerImp.frxWEB_ORSER_010_001NewGetValue(Sender: TObject;
  const VarName: string; var Value: Variant);
begin
  if VarName = 'PERIODO' then
      Value := dmkPF.PeriodoImp1(TPAbeIni.Date, TPAbeFim.Date,
    CkAbeIni.Checked, CkAbeIni.Checked, '', '', ' ');
end;

procedure TFmWOrdSerImp.frxWEB_ORSER_010_002NewGetValue(Sender: TObject;
  const VarName: string; var Value: Variant);
begin
  if VarName = 'PERIODO' then
      Value := dmkPF.PeriodoImp1(TPAbeIni.Date, TPAbeFim.Date,
    CkAbeIni.Checked, CkAbeIni.Checked, '', '', ' ');
end;

procedure TFmWOrdSerImp.Qr1GraCalcFields(DataSet: TDataSet);
var
  Total: Integer;
  Percent: Double;
begin
  Total := Qr2TotCodigo.Value;
  if Total > 0 then
    Percent := (100 * Qr1GraCodigo.Value) / Total
  else
    Percent := 0;
  Qr1GraPORCENT.Value := FormatFloat('0.00', Percent);
end;

procedure TFmWOrdSerImp.Qr1ItsCalcFields(DataSet: TDataSet);
var
  Total: Integer;
  Percent: Double;
begin
  Total := Qr2TotCodigo.Value;
  if Total > 0 then
    Percent := (100 * Qr1ItsCodigo.Value) / Total
  else
    Percent := 0;
  Qr1ItsPORCENT.Value := Percent;
end;

procedure TFmWOrdSerImp.Qr2GraCalcFields(DataSet: TDataSet);
var
  Total: Integer;
  Percent: Double;
begin
  Total := Qr2TotCodigo.Value;
  if Total > 0 then
    Percent := (100 * Qr2GraCodigo.Value) / Total
  else
    Percent := 0;
  Qr2GraPORCENT.Value := FormatFloat('0.00', Percent);
end;

procedure TFmWOrdSerImp.Qr2ItsCalcFields(DataSet: TDataSet);
var
  Total: Integer;
  Percent: Double;
begin
  Total := Qr2TotCodigo.Value;
  if Total > 0 then
    Percent := (100 * Qr2ItsCodigo.Value) / Total
  else
    Percent := 0;
  Qr2ItsPORCENT.Value := Percent;
end;

procedure TFmWOrdSerImp.Qr3GraCalcFields(DataSet: TDataSet);
var
  Total: Integer;
  Percent: Double;
begin
  Total := Qr2TotCodigo.Value;
  if Total > 0 then
    Percent := (100 * Qr3GraCodigo.Value) / Total
  else
    Percent := 0;
  Qr3GraPORCENT.Value := FormatFloat('0.00', Percent);
end;

procedure TFmWOrdSerImp.Qr3ItsCalcFields(DataSet: TDataSet);
var
  Total: Integer;
  Percent: Double;
begin
  Total := Qr2TotCodigo.Value;
  if Total > 0 then
    Percent := (100 * Qr3ItsCodigo.Value) / Total
  else
    Percent := 0;
  Qr3ItsPORCENT.Value := Percent;
end;

procedure TFmWOrdSerImp.Qr4GraCalcFields(DataSet: TDataSet);
var
  Total: Integer;
  Percent: Double;
begin
  Total := Qr2TotCodigo.Value;
  if Total > 0 then
    Percent := (100 * Qr4GraCodigo.Value) / Total
  else
    Percent := 0;
  Qr4GraPORCENT.Value := FormatFloat('0.00', Percent);
end;

procedure TFmWOrdSerImp.Qr4ItsCalcFields(DataSet: TDataSet);
var
  Total: Integer;
  Percent: Double;
begin
  Total := Qr2TotCodigo.Value;
  if Total > 0 then
    Percent := (100 * Qr4ItsCodigo.Value) / Total
  else
    Percent := 0;
  Qr4ItsPORCENT.Value := Percent;
end;

procedure TFmWOrdSerImp.Qr5GraCalcFields(DataSet: TDataSet);
var
  Total: Integer;
  Percent: Double;
begin
  Total := Qr5TotCodigo.Value;
  if Total > 0 then
    Percent := (100 * Qr5GraCodigo.Value) / Total
  else
    Percent := 0;
  Qr5GraPORCENT.Value := FormatFloat('0.00', Percent);
end;

procedure TFmWOrdSerImp.Qr5ItsCalcFields(DataSet: TDataSet);
var
  Total: Integer;
  Percent: Double;
begin
  Total := Qr5TotCodigo.Value;
  if Total > 0 then
    Percent := (100 * Qr5ItsCodigo.Value) / Total
  else
    Percent := 0;
  Qr5ItsPORCENT.Value := Percent;
end;

procedure TFmWOrdSerImp.Reopen1Gra;
var
  Mask1, Mask2, MesI, MesF: string;
begin
  MesI := FormatDateTime(VAR_FORMATDATE22, TPAbeIni.Date);
  MesF := FormatDateTime(VAR_FORMATDATE22, TPAbeFim.Date);
  //
  if MesI = MesF then
  begin
    Mask1 := '%Y-%m-%d';
    Mask2 := '%d/%m/%Y';
  end else
  begin
    Mask1 := '%Y-%m';
    Mask2 := '%m/%Y';
  end;
  UnDmkDAC_PF.AbreMySQLQuery0(Qr1Gra, Dmod.MyDBn, [
  'SELECT DATE_FORMAT(Abertura, "'+ Mask2 +'") Abertura,',
  'COUNT(Codigo) Codigo',
  'FROM wordser',
  dmkPF.SQL_Periodo('WHERE Abertura ', TPAbeIni.Date, TPAbeFim.Date,
    CkAbeIni.Checked, CkAbeFim.Checked),
  'GROUP BY DATE_FORMAT(Abertura, "'+ Mask1 +'")',
  'ORDER BY DATE_FORMAT(Abertura, "%Y-%m-%d")',
  '']);
end;

procedure TFmWOrdSerImp.Reopen1Its();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(Qr1Its, Dmod.MyDBn, [
  'SELECT DATE_FORMAT(Abertura, "%d/%m/%Y") Abertura_TXT,',
  'DATE_FORMAT(Abertura, "%m/%Y") MES, COUNT(Codigo) Codigo',
  'FROM wordser',
  dmkPF.SQL_Periodo('WHERE Abertura ', TPAbeIni.Date, TPAbeFim.Date,
    CkAbeIni.Checked, CkAbeFim.Checked),
  'GROUP BY Abertura_TXT',
  'ORDER BY Abertura',
  '']);
end;

procedure TFmWOrdSerImp.Reopen2Gra;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(Qr2Gra, Dmod.MyDBn, [
  'SELECT COUNT(wor.Codigo) Codigo,',
  'IF (ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMECLI',
  'FROM wordser wor',
  'LEFT JOIN entidades ent ON ent.Codigo = wor.Cliente',
  dmkPF.SQL_Periodo('WHERE wor.Abertura ', TPAbeIni.Date, TPAbeFim.Date,
    CkAbeIni.Checked, CkAbeFim.Checked),
  'GROUP BY wor.Cliente',
  'ORDER BY Codigo DESC, NOMECLI ASC',
  '']);
end;

procedure TFmWOrdSerImp.Reopen2Its;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(Qr2Its, Dmod.MyDBn, [
  'SELECT',
  '(',    
  'SELECT COUNT(Codigo)',
  'FROM wordser',
  'WHERE Cliente = wor.Cliente',
  dmkPF.SQL_Periodo('AND Abertura ',
    TPAbeIni.Date, TPAbeFim.Date, CkAbeIni.Checked, CkAbeFim.Checked),
  'GROUP BY Cliente',
  ') CodCli,',
  'Count(wor.Codigo) Codigo, wor.Cliente,',
  'IF (ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMECLI,',
  'wus.PersonalName NOMESOLICIT',
  'FROM wordser wor',
  'LEFT JOIN wusers wus ON wus.Codigo = wor.Solicit',
  'LEFT JOIN entidades ent ON ent.Codigo = wor.Cliente',
  dmkPF.SQL_Periodo('WHERE wor.Abertura ',
    TPAbeIni.Date, TPAbeFim.Date, CkAbeIni.Checked, CkAbeFim.Checked),
  'GROUP BY wor.Solicit, wor.Cliente',
  'ORDER BY CodCli DESC, Codigo DESC, NOMECLI ASC',
  '']);
end;

procedure TFmWOrdSerImp.Reopen2Tot;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(Qr2Tot, Dmod.MyDBn, [
  'SELECT COUNT(Codigo) Codigo',
  'FROM wordser',
  dmkPF.SQL_Periodo('WHERE Abertura ',
    TPAbeIni.Date, TPAbeFim.Date, CkAbeIni.Checked, CkAbeFim.Checked),
  '']);
end;

procedure TFmWOrdSerImp.Reopen3Gra;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(Qr3Gra, Dmod.MyDBn, [
  'SELECT COUNT(wor.Codigo) Codigo,',
  'DATE_FORMAT(wor.Abertura, "%m/%Y") MES, ',
  'ass.Nome NOMEASS',
  'FROM wordser wor',
  'LEFT JOIN wordserass ass ON ass.Codigo = wor.Assunto',
  dmkPF.SQL_Periodo('WHERE wor.Abertura ',
    TPAbeIni.Date, TPAbeFim.Date, CkAbeIni.Checked, CkAbeFim.Checked),
  'GROUP BY wor.Assunto',
  'ORDER BY Codigo DESC',
  '']);
end;

procedure TFmWOrdSerImp.Reopen3Its;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(Qr3Its, Dmod.MyDBn, [
  'SELECT COUNT(wor.Codigo) Codigo, ass.Nome NOMEASS,',
  'DATE_FORMAT(wor.Abertura, "%m/%Y") MES',
  'FROM wordser wor',
  'LEFT JOIN wordserass ass ON ass.Codigo = wor.Assunto',
  dmkPF.SQL_Periodo('WHERE wor.Abertura ',
    TPAbeIni.Date, TPAbeFim.Date, CkAbeIni.Checked, CkAbeFim.Checked),
  'GROUP BY wor.Assunto, MES',
  'ORDER BY wor.Abertura',
  '']);
end;

procedure TFmWOrdSerImp.Reopen4Gra;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(Qr4Gra, Dmod.MyDBn, [
  'SELECT COUNT(wor.Codigo) Codigo,',
  'wmo.Nome NOMEMOD',
  'FROM wordser wor',
  'LEFT JOIN wordsermod wmo ON wmo.Codigo = wor.Modo',
  dmkPF.SQL_Periodo('WHERE wor.Abertura ',
    TPAbeIni.Date, TPAbeFim.Date, CkAbeIni.Checked, CkAbeFim.Checked),
  'GROUP BY wor.Modo',
  'ORDER BY Codigo DESC',
  '']);
end;

procedure TFmWOrdSerImp.Reopen4Its;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(Qr4Its, Dmod.MyDBn, [
  'SELECT COUNT(wor.Codigo) Codigo, wmo.Nome NOMEMODO,',
  'DATE_FORMAT(wor.Abertura, "%m/%Y") MES',
  'FROM wordser wor',
  'LEFT JOIN wordsermod wmo ON wmo.Codigo = wor.Modo',
  dmkPF.SQL_Periodo('WHERE wor.Abertura ',
    TPAbeIni.Date, TPAbeFim.Date, CkAbeIni.Checked, CkAbeFim.Checked),
  'GROUP BY wor.Modo, MES',
  'ORDER BY wor.Abertura',
  '']);
end;

procedure TFmWOrdSerImp.Reopen5Gra;
var
  Mask1, Mask2, MesI, MesF: string;
begin
  MesI := FormatDateTime(VAR_FORMATDATE22, TPAbeIni.Date);
  MesF := FormatDateTime(VAR_FORMATDATE22, TPAbeFim.Date);
  //
  if MesI = MesF then
  begin
    Mask1 := '%Y-%m-%d';
    Mask2 := '%d/%m/%Y';
  end else
  begin
    Mask1 := '%Y-%m';
    Mask2 := '%m/%Y';
  end;
  UnDmkDAC_PF.AbreMySQLQuery0(Qr5Gra, Dmod.MyDBn, [
  'SELECT DATE_FORMAT(Encerr, "'+ Mask2 +'") Encerr,',
  'COUNT(Codigo) Codigo',
  'FROM wordser',
  dmkPF.SQL_Periodo('WHERE Encerr ', TPAbeIni.Date, TPAbeFim.Date,
    CkAbeIni.Checked, CkAbeFim.Checked),
  'GROUP BY DATE_FORMAT(Encerr, "'+ Mask1 +'")',
  'ORDER BY DATE_FORMAT(Encerr, "%Y-%m-%d")',
  '']);
end;

procedure TFmWOrdSerImp.Reopen5Its();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(Qr5Its, Dmod.MyDBn, [
  'SELECT DATE_FORMAT(Encerr, "%d/%m/%Y") Encerr_TXT,',
  'DATE_FORMAT(Encerr, "%m/%Y") MES, COUNT(Codigo) Codigo',
  'FROM wordser',
  dmkPF.SQL_Periodo('WHERE Encerr ', TPAbeIni.Date, TPAbeFim.Date,
    CkAbeIni.Checked, CkAbeFim.Checked),
  'GROUP BY Encerr_TXT',
  'ORDER BY Encerr',
  '']);
end;

procedure TFmWOrdSerImp.Reopen5Tot();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(Qr5Tot, Dmod.MyDBn, [
  'SELECT COUNT(Codigo) Codigo',
  'FROM wordser',
  dmkPF.SQL_Periodo('WHERE Encerr ',
    TPAbeIni.Date, TPAbeFim.Date, CkAbeIni.Checked, CkAbeFim.Checked),
  '']);
end;

end.
