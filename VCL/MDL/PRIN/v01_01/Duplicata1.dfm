object FmDuplicata1: TFmDuplicata1
  Left = 332
  Top = 179
  Caption = 'IMP-DUPLC-001 :: Impress'#227'o de Duplicata [Modelo 1]'
  ClientHeight = 494
  ClientWidth = 718
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Panel2: TPanel
    Left = 0
    Top = 0
    Width = 718
    Height = 494
    Align = alClient
    ParentBackground = False
    TabOrder = 0
    object Panel4: TPanel
      Left = 1
      Top = 445
      Width = 716
      Height = 48
      Align = alBottom
      TabOrder = 1
      object BtImprime: TBitBtn
        Tag = 5
        Left = 11
        Top = 4
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Hint = 'Confima Inclus'#227'o / altera'#231#227'o'
        Caption = '&Imprime'
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtImprimeClick
      end
      object BtSaida: TBitBtn
        Tag = 13
        Left = 614
        Top = 4
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Hint = 'Cancela inclus'#227'o / altera'#231#227'o'
        Caption = '&Sa'#237'da'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        OnClick = BtSaidaClick
      end
      object CkTeste: TCheckBox
        Left = 284
        Top = 16
        Width = 65
        Height = 17
        Caption = 'Teste.'
        TabOrder = 2
      end
    end
    object PageControl1: TPageControl
      Left = 1
      Top = 1
      Width = 716
      Height = 444
      ActivePage = TabSheet1
      Align = alClient
      TabOrder = 0
      object TabSheet1: TTabSheet
        Caption = 'Dados'
        object Panel5: TPanel
          Left = 0
          Top = 52
          Width = 708
          Height = 364
          Align = alClient
          ParentBackground = False
          TabOrder = 1
          object Label1: TLabel
            Left = 12
            Top = 8
            Width = 82
            Height = 13
            Caption = 'Data da emiss'#227'o:'
          end
          object Label3: TLabel
            Left = 104
            Top = 7
            Width = 57
            Height = 13
            Caption = 'Valor fatura:'
          end
          object Label4: TLabel
            Left = 208
            Top = 7
            Width = 70
            Height = 13
            Caption = 'N'#250'mero fatura:'
          end
          object Label5: TLabel
            Left = 312
            Top = 7
            Width = 73
            Height = 13
            Caption = 'Valor duplicata:'
          end
          object Label6: TLabel
            Left = 416
            Top = 7
            Width = 87
            Height = 13
            Caption = 'N'#250'mero de ordem:'
          end
          object Label7: TLabel
            Left = 612
            Top = 52
            Width = 59
            Height = 13
            Caption = 'Vencimento:'
          end
          object GroupBox3: TGroupBox
            Left = 12
            Top = 236
            Width = 597
            Height = 77
            TabOrder = 8
            object MeExtenso: TMemo
              Left = 72
              Top = 8
              Width = 513
              Height = 65
              TabStop = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
              TabOrder = 0
            end
            object StaticText1: TStaticText
              Left = 24
              Top = 16
              Width = 28
              Height = 17
              Caption = 'Valor'
              TabOrder = 1
            end
            object StaticText2: TStaticText
              Left = 28
              Top = 32
              Width = 19
              Height = 17
              Caption = 'por'
              TabOrder = 2
            end
            object StaticText3: TStaticText
              Left = 16
              Top = 48
              Width = 41
              Height = 17
              Caption = 'extenso'
              TabOrder = 3
            end
          end
          object TPEmissao: TDateTimePicker
            Left = 12
            Top = 24
            Width = 89
            Height = 21
            Date = 38599.794259618100000000
            Time = 38599.794259618100000000
            TabOrder = 0
          end
          object EdValorF: TdmkEdit
            Left = 104
            Top = 23
            Width = 100
            Height = 21
            Alignment = taRightJustify
            TabOrder = 1
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
          object EdNumeroF: TdmkEdit
            Left = 208
            Top = 23
            Width = 100
            Height = 21
            Alignment = taCenter
            TabOrder = 2
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdValorD: TdmkEdit
            Left = 312
            Top = 23
            Width = 100
            Height = 21
            Alignment = taRightJustify
            TabOrder = 3
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
            OnChange = EdValorDChange
            OnExit = EdValorDExit
          end
          object EdNumeroD: TdmkEdit
            Left = 416
            Top = 23
            Width = 100
            Height = 21
            Alignment = taCenter
            TabOrder = 4
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object TPVencimento: TDateTimePicker
            Left = 612
            Top = 68
            Width = 89
            Height = 21
            Date = 38599.794259618100000000
            Time = 38599.794259618100000000
            TabOrder = 6
          end
          object GroupBox1: TGroupBox
            Left = 12
            Top = 104
            Width = 597
            Height = 137
            TabOrder = 7
            object Label8: TLabel
              Left = 8
              Top = 16
              Width = 40
              Height = 13
              Caption = 'Sacado:'
              FocusControl = EdNomeSacado
            end
            object Label9: TLabel
              Left = 8
              Top = 40
              Width = 49
              Height = 13
              Caption = 'Endere'#231'o:'
              FocusControl = EdNomeSacado
            end
            object Label10: TLabel
              Left = 8
              Top = 64
              Width = 50
              Height = 13
              Caption = 'Munic'#237'pio:'
              FocusControl = EdNomeSacado
            end
            object Label13: TLabel
              Left = 8
              Top = 88
              Width = 53
              Height = 13
              Caption = 'P'#231'a. Pgto.:'
              FocusControl = EdNomeSacado
            end
            object Label14: TLabel
              Left = 8
              Top = 112
              Width = 55
              Height = 13
              Caption = 'CNPJ/CPF:'
              FocusControl = EdNomeSacado
            end
            object Label16: TLabel
              Left = 408
              Top = 64
              Width = 36
              Height = 13
              Caption = 'Estado:'
            end
            object Label17: TLabel
              Left = 408
              Top = 88
              Width = 24
              Height = 13
              Caption = 'CEP:'
            end
            object Label18: TLabel
              Left = 408
              Top = 112
              Width = 19
              Height = 13
              Caption = 'I.E.:'
            end
            object EdNomeSacado: TDBEdit
              Left = 72
              Top = 12
              Width = 512
              Height = 21
              TabStop = False
              DataField = 'NOME'
              DataSource = DsSacado
              TabOrder = 1
            end
            object EdEndereco: TDBEdit
              Left = 72
              Top = 36
              Width = 512
              Height = 21
              TabStop = False
              DataField = 'ENDERECO'
              DataSource = DsSacado
              TabOrder = 2
            end
            object EdCidade: TDBEdit
              Left = 72
              Top = 60
              Width = 300
              Height = 21
              TabStop = False
              DataField = 'CIDADE'
              DataSource = DsSacado
              TabOrder = 3
              OnChange = EdCidadeChange
            end
            object EdCNPJ: TDBEdit
              Left = 72
              Top = 108
              Width = 300
              Height = 21
              TabStop = False
              DataField = 'CPF_TXT'
              DataSource = DsSacado
              TabOrder = 4
            end
            object EdUF: TDBEdit
              Left = 472
              Top = 60
              Width = 112
              Height = 21
              TabStop = False
              DataField = 'NOMEUF'
              DataSource = DsSacado
              TabOrder = 5
            end
            object EdCEP: TDBEdit
              Left = 472
              Top = 84
              Width = 112
              Height = 21
              TabStop = False
              DataField = 'CEP_TXT'
              DataSource = DsSacado
              TabOrder = 6
            end
            object EdIE: TDBEdit
              Left = 472
              Top = 108
              Width = 112
              Height = 21
              TabStop = False
              DataField = 'IE'
              DataSource = DsSacado
              TabOrder = 7
            end
            object EdPraca: TdmkEdit
              Left = 72
              Top = 84
              Width = 301
              Height = 21
              TabOrder = 0
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = 'EdPraca'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 'EdPraca'
              ValWarn = False
            end
          end
          object GroupBox2: TGroupBox
            Left = 12
            Top = 48
            Width = 597
            Height = 61
            TabOrder = 5
            object Label12: TLabel
              Left = 8
              Top = 16
              Width = 66
              Height = 13
              Caption = 'Descontos de'
              FocusControl = EdNomeSacado
            end
            object Label15: TLabel
              Left = 228
              Top = 16
              Width = 8
              Height = 13
              Caption = '%'
              FocusControl = EdNomeSacado
            end
            object Label20: TLabel
              Left = 8
              Top = 40
              Width = 97
              Height = 13
              Caption = 'Condi'#231#245'es especiais'
              FocusControl = EdNomeSacado
            end
            object Label19: TLabel
              Left = 244
              Top = 16
              Width = 49
              Height = 13
              Alignment = taRightJustify
              AutoSize = False
              Caption = '$:'
              FocusControl = EdNomeSacado
            end
            object EdDescoP: TdmkEdit
              Left = 112
              Top = 11
              Width = 113
              Height = 21
              Alignment = taRightJustify
              TabOrder = 0
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
              OnChange = EdDescoPChange
              OnExit = EdDescoPExit
            end
            object EdDescoV: TdmkEdit
              Left = 296
              Top = 11
              Width = 100
              Height = 21
              Alignment = taRightJustify
              TabOrder = 1
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
              OnChange = EdDescoVChange
              OnExit = EdDescoVExit
            end
            object TPAte: TDateTimePicker
              Left = 496
              Top = 12
              Width = 89
              Height = 21
              Date = 38599.794259618100000000
              Time = 38599.794259618100000000
              TabOrder = 3
            end
            object EdCondEsp: TdmkEdit
              Left = 112
              Top = 35
              Width = 472
              Height = 21
              TabOrder = 4
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object CkVenctoDesc: TCheckBox
              Left = 452
              Top = 14
              Width = 41
              Height = 17
              Caption = 'At'#233':'
              TabOrder = 2
            end
          end
          object RGVcto: TRadioGroup
            Left = 520
            Top = 4
            Width = 189
            Height = 45
            Caption = ' Vencimento: '
            Columns = 3
            Items.Strings = (
              #192' vista'
              'c/apres'
              'Prazo')
            TabOrder = 9
            OnClick = RGVctoClick
          end
        end
        object Panel3: TPanel
          Left = 0
          Top = 0
          Width = 708
          Height = 52
          Align = alTop
          ParentBackground = False
          TabOrder = 0
          object Label11: TLabel
            Left = 4
            Top = 4
            Width = 51
            Height = 13
            Caption = 'Impress'#227'o:'
          end
          object Label2: TLabel
            Left = 320
            Top = 4
            Width = 40
            Height = 13
            Caption = 'Sacado:'
          end
          object SpeedButton1: TSpeedButton
            Left = 680
            Top = 20
            Width = 21
            Height = 21
            Caption = '...'
            OnClick = SpeedButton1Click
          end
          object SpeedButton2: TSpeedButton
            Left = 296
            Top = 20
            Width = 21
            Height = 21
            Caption = '...'
            OnClick = SpeedButton2Click
          end
          object EdImprime: TdmkEditCB
            Left = 4
            Top = 20
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            DBLookupComboBox = CBImprime
            IgnoraDBLookupComboBox = False
          end
          object CBImprime: TdmkDBLookupComboBox
            Left = 60
            Top = 20
            Width = 233
            Height = 21
            Color = clWhite
            KeyField = 'Codigo'
            ListField = 'Nome'
            ListSource = DsImprime
            TabOrder = 1
            dmkEditCB = EdImprime
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
          object EdSacado: TdmkEditCB
            Left = 320
            Top = 20
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 2
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            DBLookupComboBox = CBSacado
            IgnoraDBLookupComboBox = False
          end
          object CBSacado: TdmkDBLookupComboBox
            Left = 376
            Top = 20
            Width = 304
            Height = 21
            Color = clWhite
            KeyField = 'Codigo'
            ListField = 'Nome'
            ListSource = DsSacado
            TabOrder = 3
            dmkEditCB = EdSacado
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
        end
      end
      object TabSheet2: TTabSheet
        Caption = 'Configura'#231#227'o'
        ImageIndex = 1
        object SG1: TStringGrid
          Left = 0
          Top = 0
          Width = 708
          Height = 416
          TabStop = False
          Align = alClient
          ColCount = 10
          DefaultRowHeight = 18
          RowCount = 2
          TabOrder = 0
          ColWidths = (
            64
            45
            44
            42
            42
            41
            33
            27
            26
            330)
        end
      end
    end
  end
  object QrImprime: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome, NomeFonte, MSupDOS, Pos_MSup2 , Pos_MEsq'
      'FROM imprime'
      'WHERE TipoImpressao=1')
    Left = 232
    Top = 36
    object QrImprimeCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrImprimeNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QrImprimeNomeFonte: TWideStringField
      FieldName = 'NomeFonte'
      Size = 255
    end
    object QrImprimeMSupDOS: TIntegerField
      FieldName = 'MSupDOS'
    end
    object QrImprimePos_MSup2: TIntegerField
      FieldName = 'Pos_MSup2'
    end
    object QrImprimePos_MEsq: TIntegerField
      FieldName = 'Pos_MEsq'
    end
  end
  object DsImprime: TDataSource
    DataSet = QrImprime
    Left = 260
    Top = 36
  end
  object QrItens: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrItensCalcFields
    SQL.Strings = (
      'SELECT imp.Pos_Topo TOPO_A, imp.Pos_MEsq MESQ_A, '
      'imb.Pos_Topo TOPO_B, imv.* '
      'FROM imprimeview imv'
      'LEFT JOIN imprime     imp ON imp.Codigo=imv.Codigo'
      'LEFT JOIN imprimeband imb ON imb.Codigo=imv.Codigo'
      'WHERE imv.Codigo=:P0')
    Left = 120
    Top = 36
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrItensTOPO_A: TIntegerField
      FieldName = 'TOPO_A'
    end
    object QrItensMESQ_A: TIntegerField
      FieldName = 'MESQ_A'
    end
    object QrItensTOPO_B: TIntegerField
      FieldName = 'TOPO_B'
    end
    object QrItensCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrItensControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrItensBand: TIntegerField
      FieldName = 'Band'
      Required = True
    end
    object QrItensNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 100
    end
    object QrItensTab_Tipo: TIntegerField
      FieldName = 'Tab_Tipo'
      Required = True
    end
    object QrItensTab_Campo: TIntegerField
      FieldName = 'Tab_Campo'
      Required = True
    end
    object QrItensPadrao: TWideStringField
      FieldName = 'Padrao'
      Size = 255
    end
    object QrItensPrefixo: TWideStringField
      FieldName = 'Prefixo'
      Size = 255
    end
    object QrItensSufixo: TWideStringField
      FieldName = 'Sufixo'
      Size = 255
    end
    object QrItensPos_Topo: TIntegerField
      FieldName = 'Pos_Topo'
      Required = True
    end
    object QrItensPos_Altu: TIntegerField
      FieldName = 'Pos_Altu'
      Required = True
    end
    object QrItensPos_Larg: TIntegerField
      FieldName = 'Pos_Larg'
      Required = True
    end
    object QrItensPos_MEsq: TIntegerField
      FieldName = 'Pos_MEsq'
      Required = True
    end
    object QrItensAlt_Linha: TIntegerField
      FieldName = 'Alt_Linha'
      Required = True
    end
    object QrItensSet_TAli: TIntegerField
      FieldName = 'Set_TAli'
      Required = True
    end
    object QrItensSet_BAli: TIntegerField
      FieldName = 'Set_BAli'
      Required = True
    end
    object QrItensSet_Negr: TIntegerField
      FieldName = 'Set_Negr'
      Required = True
    end
    object QrItensSet_Ital: TIntegerField
      FieldName = 'Set_Ital'
      Required = True
    end
    object QrItensSet_Unde: TIntegerField
      FieldName = 'Set_Unde'
      Required = True
    end
    object QrItensSet_Tam: TIntegerField
      FieldName = 'Set_Tam'
      Required = True
    end
    object QrItensFormato: TIntegerField
      FieldName = 'Formato'
      Required = True
    end
    object QrItensSubstitui: TSmallintField
      FieldName = 'Substitui'
      Required = True
    end
    object QrItensSubstituicao: TWideStringField
      FieldName = 'Substituicao'
      Size = 255
    end
    object QrItensSet_T_DOS: TSmallintField
      FieldName = 'Set_T_DOS'
      Required = True
    end
    object QrItensNulo: TSmallintField
      FieldName = 'Nulo'
      Required = True
    end
    object QrItensLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrItensDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrItensDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrItensUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrItensUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrItensTOPO: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'TOPO'
      Calculated = True
    end
    object QrItensMESQ: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'MESQ'
      Calculated = True
    end
  end
  object DsItens: TDataSource
    DataSet = QrItens
    Left = 148
    Top = 36
  end
  object DsSacado: TDataSource
    DataSet = QrSacado
    Left = 512
    Top = 12
  end
  object QrSacado: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrSacadoCalcFields
    SQL.Strings = (
      'SELECT fo.Codigo,'
      'IF( fo.Tipo=0, fo.RazaoSocial, fo.Nome) NOME,'
      'IF( fo.Tipo=0, fo.ERua, fo.PRua) RUA,'
      'IF( fo.Tipo=0, fo.ENumero, fo.PNumero) + 0.000 NUMERO,'
      'IF( fo.Tipo=0, fo.ECompl, fo.PCompl) COMPL,'
      'IF( fo.Tipo=0, fo.EBairro, fo.PBairro) BAIRRO,'
      'IF( fo.Tipo=0, fo.ECidade, fo.PCidade) CIDADE,'
      'IF( fo.Tipo=0, fo.EPais, fo.PPais) PAIS,'
      'IF( fo.Tipo=0, fo.ETe1, fo.PTe1) TELEFONE,'
      'IF( fo.Tipo=0, fo.EFax, fo.PPais) FAX,'
      'IF( fo.Tipo=0, fo.ECel, fo.PCel) Celular,'
      'IF( fo.Tipo=0, fo.CNPJ, fo.CPF) CNPJ,'
      'IF( fo.Tipo=0, fo.IE, fo.RG) IE,'
      'IF( fo.Tipo=0, fo.ECEP, fo.PCEP) + 0.000 CEP,'
      'IF( fo.Tipo=0, fo.EContato, fo.PContato) Contato,'
      ''
      'IF( fo.Tipo=0, fo.EUF, fo.PUF) + 0.000 UF,'
      'IF( fo.Tipo=0, uf1.Nome, uf2.Nome) NOMEUF,'
      'EEMail, PEmail,'
      ''
      'IF( fo.Tipo=0, ll1.Nome, ll2.Nome) LOGRAD'
      ''
      'FROM entidades fo'
      'LEFT JOIN ufs uf1 ON uf1.Codigo=fo.Euf'
      'LEFT JOIN ufs uf2 ON uf2.Codigo=fo.Puf'
      'LEFT JOIN listalograd ll1 ON ll1.Codigo=fo.ELograd'
      'LEFT JOIN listalograd ll2 ON ll2.Codigo=fo.PLograd'
      'ORDER BY NOME')
    Left = 484
    Top = 12
    object QrSacadoTEL_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TEL_TXT'
      Calculated = True
    end
    object QrSacadoCEL_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CEL_TXT'
      Calculated = True
    end
    object QrSacadoCPF_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CPF_TXT'
      Calculated = True
    end
    object QrSacadoFAX_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'FAX_TXT'
      Calculated = True
    end
    object QrSacadoCEP_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CEP_TXT'
      Calculated = True
    end
    object QrSacadoNUMEROTXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NUMEROTXT'
      Size = 40
      Calculated = True
    end
    object QrSacadoENDERECO: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'ENDERECO'
      Size = 255
      Calculated = True
    end
    object QrSacadoNOME: TWideStringField
      FieldName = 'NOME'
      Size = 100
    end
    object QrSacadoRUA: TWideStringField
      FieldName = 'RUA'
      Size = 30
    end
    object QrSacadoCOMPL: TWideStringField
      FieldName = 'COMPL'
      Size = 30
    end
    object QrSacadoBAIRRO: TWideStringField
      FieldName = 'BAIRRO'
      Size = 30
    end
    object QrSacadoCIDADE: TWideStringField
      FieldName = 'CIDADE'
      Size = 25
    end
    object QrSacadoPAIS: TWideStringField
      FieldName = 'PAIS'
    end
    object QrSacadoTELEFONE: TWideStringField
      FieldName = 'TELEFONE'
    end
    object QrSacadoFAX: TWideStringField
      FieldName = 'FAX'
    end
    object QrSacadoCelular: TWideStringField
      FieldName = 'Celular'
    end
    object QrSacadoCNPJ: TWideStringField
      FieldName = 'CNPJ'
      Size = 18
    end
    object QrSacadoIE: TWideStringField
      FieldName = 'IE'
    end
    object QrSacadoContato: TWideStringField
      FieldName = 'Contato'
      Size = 60
    end
    object QrSacadoNOMEUF: TWideStringField
      FieldName = 'NOMEUF'
      Size = 2
    end
    object QrSacadoEEMail: TWideStringField
      FieldName = 'EEMail'
      Size = 100
    end
    object QrSacadoPEmail: TWideStringField
      FieldName = 'PEmail'
      Size = 100
    end
    object QrSacadoLOGRAD: TWideStringField
      FieldName = 'LOGRAD'
      Size = 10
    end
    object QrSacadoCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrSacadoNUMERO: TFloatField
      FieldName = 'NUMERO'
    end
    object QrSacadoCEP: TFloatField
      FieldName = 'CEP'
    end
    object QrSacadoUF: TFloatField
      FieldName = 'UF'
      Required = True
    end
  end
  object PMImprime: TPopupMenu
    Left = 117
    Top = 425
    object TintaLaser1: TMenuItem
      Caption = '&Tinta / Laser'
      OnClick = TintaLaser1Click
    end
    object Matricial1: TMenuItem
      Caption = '&Matricial'
      OnClick = Matricial1Click
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object Completa1: TMenuItem
      Caption = '&Completa'
      object N1via1: TMenuItem
        Caption = '&1 via'
        OnClick = N1via1Click
      end
      object N2vias1: TMenuItem
        Caption = '&2 vias'
        OnClick = N2vias1Click
      end
    end
  end
  object QrPages: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Max(Pagin) PAGINAS'
      'FROM imprimir1')
    Left = 469
    Top = 209
    object QrPagesPAGINAS: TIntegerField
      FieldName = 'PAGINAS'
    end
  end
  object QrView1: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT* FROM imprimir1'
      'WHERE DTopo=:P0'
      'AND DBand=:P1'
      'AND Tipo =6'
      'ORDER BY DMEsq')
    Left = 640
    Top = 240
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrView1Campo: TIntegerField
      FieldName = 'Campo'
    end
    object QrView1Valor: TWideStringField
      FieldName = 'Valor'
      Size = 255
    end
    object QrView1Prefixo: TWideStringField
      FieldName = 'Prefixo'
      Size = 255
    end
    object QrView1Sufixo: TWideStringField
      FieldName = 'Sufixo'
      Size = 255
    end
    object QrView1Conta: TIntegerField
      FieldName = 'Conta'
    end
    object QrView1DBand: TIntegerField
      FieldName = 'DBand'
    end
    object QrView1DTopo: TIntegerField
      FieldName = 'DTopo'
    end
    object QrView1DMEsq: TIntegerField
      FieldName = 'DMEsq'
    end
    object QrView1DComp: TIntegerField
      FieldName = 'DComp'
    end
    object QrView1DAltu: TIntegerField
      FieldName = 'DAltu'
    end
    object QrView1Set_N: TIntegerField
      FieldName = 'Set_N'
    end
    object QrView1Set_I: TIntegerField
      FieldName = 'Set_I'
    end
    object QrView1Set_U: TIntegerField
      FieldName = 'Set_U'
    end
    object QrView1Set_T: TIntegerField
      FieldName = 'Set_T'
    end
    object QrView1Set_A: TIntegerField
      FieldName = 'Set_A'
    end
    object QrView1Tipo: TIntegerField
      FieldName = 'Tipo'
    end
    object QrView1Substituicao: TWideStringField
      FieldName = 'Substituicao'
      Size = 255
    end
    object QrView1Substitui: TSmallintField
      FieldName = 'Substitui'
    end
    object QrView1Nulo: TSmallintField
      FieldName = 'Nulo'
    end
    object QrView1Forma: TIntegerField
      FieldName = 'Forma'
    end
    object QrView1Linha: TIntegerField
      FieldName = 'Linha'
    end
    object QrView1Colun: TIntegerField
      FieldName = 'Colun'
    end
    object QrView1Pagin: TIntegerField
      FieldName = 'Pagin'
    end
  end
  object QrTopos1: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT DISTINCT DTopo, Tipo '
      'FROM imprimir1'
      'WHERE DBand=:P0'
      'AND Valor<>'#39#39
      'ORDER BY DTopo')
    Left = 664
    Top = 240
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrTopos1DTopo: TIntegerField
      FieldName = 'DTopo'
    end
    object QrTopos1Tipo: TIntegerField
      FieldName = 'Tipo'
    end
  end
  object QrImprimeBand: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM imprimeband'
      'WHERE Codigo=:P0'
      'ORDER BY Pos_Topo')
    Left = 632
    Top = 196
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrImprimeBandCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrImprimeBandControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrImprimeBandTipo: TIntegerField
      FieldName = 'Tipo'
    end
    object QrImprimeBandDataset: TIntegerField
      FieldName = 'Dataset'
    end
    object QrImprimeBandPos_Topo: TIntegerField
      FieldName = 'Pos_Topo'
    end
    object QrImprimeBandPos_Altu: TIntegerField
      FieldName = 'Pos_Altu'
    end
    object QrImprimeBandNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QrImprimeBandLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrImprimeBandDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrImprimeBandDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrImprimeBandUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrImprimeBandUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrImprimeBandCol_Qtd: TIntegerField
      FieldName = 'Col_Qtd'
    end
    object QrImprimeBandCol_Lar: TIntegerField
      FieldName = 'Col_Lar'
    end
    object QrImprimeBandCol_GAP: TIntegerField
      FieldName = 'Col_GAP'
    end
    object QrImprimeBandOrdem: TIntegerField
      FieldName = 'Ordem'
    end
    object QrImprimeBandLinhas: TIntegerField
      FieldName = 'Linhas'
    end
    object QrImprimeBandRepetencia: TSmallintField
      FieldName = 'Repetencia'
    end
  end
  object DsImprimeBand: TDataSource
    DataSet = QrImprimeBand
    Left = 660
    Top = 196
  end
  object QrItensA: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrItensACalcFields
    SQL.Strings = (
      'SELECT imp.Pos_Topo TOPO_A, imp.Pos_MEsq MESQ_A, '
      'imb.Pos_Topo TOPO_B, imv.* '
      'FROM imprimeview imv'
      'LEFT JOIN imprime     imp ON imp.Codigo=imv.Codigo'
      'LEFT JOIN imprimeband imb ON imb.Controle=imv.Controle'
      'WHERE imv.Codigo=:P0')
    Left = 484
    Top = 332
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrItensATOPO_A: TIntegerField
      FieldName = 'TOPO_A'
    end
    object QrItensAMESQ_A: TIntegerField
      FieldName = 'MESQ_A'
    end
    object QrItensATOPO_B: TIntegerField
      FieldName = 'TOPO_B'
    end
    object QrItensACodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrItensAControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrItensABand: TIntegerField
      FieldName = 'Band'
      Required = True
    end
    object QrItensANome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 100
    end
    object QrItensATab_Tipo: TIntegerField
      FieldName = 'Tab_Tipo'
      Required = True
    end
    object QrItensATab_Campo: TIntegerField
      FieldName = 'Tab_Campo'
      Required = True
    end
    object QrItensAPadrao: TWideStringField
      FieldName = 'Padrao'
      Size = 255
    end
    object QrItensAPrefixo: TWideStringField
      FieldName = 'Prefixo'
      Size = 255
    end
    object QrItensASufixo: TWideStringField
      FieldName = 'Sufixo'
      Size = 255
    end
    object QrItensAPos_Topo: TIntegerField
      FieldName = 'Pos_Topo'
      Required = True
    end
    object QrItensAPos_Altu: TIntegerField
      FieldName = 'Pos_Altu'
      Required = True
    end
    object QrItensAPos_Larg: TIntegerField
      FieldName = 'Pos_Larg'
      Required = True
    end
    object QrItensAPos_MEsq: TIntegerField
      FieldName = 'Pos_MEsq'
      Required = True
    end
    object QrItensAAlt_Linha: TIntegerField
      FieldName = 'Alt_Linha'
      Required = True
    end
    object QrItensASet_TAli: TIntegerField
      FieldName = 'Set_TAli'
      Required = True
    end
    object QrItensASet_BAli: TIntegerField
      FieldName = 'Set_BAli'
      Required = True
    end
    object QrItensASet_Negr: TIntegerField
      FieldName = 'Set_Negr'
      Required = True
    end
    object QrItensASet_Ital: TIntegerField
      FieldName = 'Set_Ital'
      Required = True
    end
    object QrItensASet_Unde: TIntegerField
      FieldName = 'Set_Unde'
      Required = True
    end
    object QrItensASet_Tam: TIntegerField
      FieldName = 'Set_Tam'
      Required = True
    end
    object QrItensAFormato: TIntegerField
      FieldName = 'Formato'
      Required = True
    end
    object QrItensASubstitui: TSmallintField
      FieldName = 'Substitui'
      Required = True
    end
    object QrItensASubstituicao: TWideStringField
      FieldName = 'Substituicao'
      Size = 255
    end
    object QrItensASet_T_DOS: TSmallintField
      FieldName = 'Set_T_DOS'
      Required = True
    end
    object QrItensANulo: TSmallintField
      FieldName = 'Nulo'
      Required = True
    end
    object QrItensALk: TIntegerField
      FieldName = 'Lk'
    end
    object QrItensADataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrItensADataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrItensAUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrItensAUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrItensATOPO: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'TOPO'
      Calculated = True
    end
    object QrItensAMESQ: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'MESQ'
      Calculated = True
    end
  end
  object frxCompleta1: TfrxReport
    Version = '4.14'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39364.415347395800000000
    ReportOptions.LastChange = 39364.415347395800000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      '  if <LogoDuplExiste> = True then'
      '  begin'
      '    Picture1.LoadFromFile(<LogoDuplCaminho>);'
      '  end;'
      'end.')
    OnGetValue = frxCompleta1GetValue
    Left = 229
    Top = 197
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = Dmod.frxDsMaster
        DataSetName = 'frxDsMaster'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      object ReportTitle1: TfrxReportTitle
        Height = 559.370078740000000000
        Top = 18.897650000000000000
        Width = 793.701300000000000000
        object Shape1: TfrxShapeView
          Left = 37.795300000000000000
          Top = 26.456710000000000000
          Width = 718.110700000000000000
          Height = 147.401670000000000000
          ShowHint = False
          Shape = skRoundRectangle
        end
        object Line1: TfrxLineView
          Left = 377.953000000000000000
          Top = 26.456710000000000000
          Height = 147.401670000000000000
          ShowHint = False
          Frame.Typ = [ftLeft]
        end
        object Memo1: TfrxMemoView
          Left = 381.732530000000000000
          Top = 30.236240000000000000
          Width = 362.834880000000000000
          Height = 68.031540000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftBottom]
          Memo.UTF8W = (
            '[frxDsDono."E_CUC"]')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          Left = 381.732530000000000000
          Top = 102.047310000000000000
          Width = 207.874150000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            'CNPJ: [frxDsDono."CNPJ_TXT"]')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          Left = 593.386210000000000000
          Top = 102.047310000000000000
          Width = 154.960730000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'I.E.: [frxDsDono."IE_RG"]')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          Left = 381.732530000000000000
          Top = 128.504020000000000000
          Width = 366.614410000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            'Data de emiss'#227'o: [VARF_EMISSAO]')
          ParentFont = False
        end
        object Shape2: TfrxShapeView
          Left = 37.795300000000000000
          Top = 181.417440000000000000
          Width = 585.827150000000000000
          Height = 75.590600000000000000
          ShowHint = False
          Shape = skRoundRectangle
        end
        object Line2: TfrxLineView
          Left = 37.795300000000000000
          Top = 200.315090000000000000
          Width = 483.779840000000000000
          ShowHint = False
          Frame.Typ = [ftTop]
        end
        object Line3: TfrxLineView
          Left = 521.575140000000000000
          Top = 181.417440000000000000
          Height = 75.590600000000000000
          ShowHint = False
          Frame.Typ = [ftLeft]
        end
        object Line4: TfrxLineView
          Left = 37.795300000000000000
          Top = 219.212740000000000000
          Width = 585.827150000000000000
          ShowHint = False
          Frame.Typ = [ftTop]
        end
        object Line5: TfrxLineView
          Left = 279.685220000000000000
          Top = 181.417440000000000000
          Height = 75.590600000000000000
          ShowHint = False
          Frame.Typ = [ftLeft]
        end
        object Line6: TfrxLineView
          Left = 170.078850000000000000
          Top = 200.315090000000000000
          Height = 56.692950000000000000
          ShowHint = False
          Frame.Typ = [ftLeft]
        end
        object Line7: TfrxLineView
          Left = 411.968770000000000000
          Top = 200.315090000000000000
          Height = 56.692950000000000000
          ShowHint = False
          Frame.Typ = [ftLeft]
        end
        object Memo5: TfrxMemoView
          Left = 45.354360000000000000
          Top = 181.417440000000000000
          Width = 226.771653540000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            'FATURA')
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          Left = 287.244280000000000000
          Top = 181.417440000000000000
          Width = 226.771653540000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            'DUPLICATA')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          Left = 45.354360000000000000
          Top = 200.315090000000000000
          Width = 120.944881890000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            'Valor R$')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          Left = 173.858380000000000000
          Top = 200.315090000000000000
          Width = 102.047244090000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            'N'#250'mero')
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          Left = 283.464750000000000000
          Top = 200.315090000000000000
          Width = 120.944881890000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            'Valor R$')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          Left = 415.748300000000000000
          Top = 200.315090000000000000
          Width = 102.047244090000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            'N'#250'mero')
          ParentFont = False
        end
        object Memo11: TfrxMemoView
          Left = 525.354670000000000000
          Top = 192.756030000000000000
          Width = 94.488188980000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            'VENCIMENTO')
          ParentFont = False
        end
        object Shape3: TfrxShapeView
          Left = 627.401980000000000000
          Top = 181.417440000000000000
          Width = 128.504020000000000000
          Height = 117.165430000000000000
          ShowHint = False
          Shape = skRoundRectangle
        end
        object Memo12: TfrxMemoView
          Left = 631.181510000000000000
          Top = 185.196970000000000000
          Width = 120.944960000000000000
          Height = 26.456710000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            'Para uso da institui'#231#227'o financeira')
          ParentFont = False
        end
        object Shape4: TfrxShapeView
          Left = 37.795300000000000000
          Top = 260.787570000000000000
          Width = 162.519790000000000000
          Height = 294.803340000000000000
          ShowHint = False
          Shape = skRoundRectangle
        end
        object Line8: TfrxLineView
          Left = 188.976500000000000000
          Top = 268.346630000000000000
          Height = 279.685220000000000000
          ShowHint = False
          Frame.Typ = [ftLeft]
        end
        object Memo13: TfrxMemoView
          Left = 173.858380000000000000
          Top = 268.346630000000000000
          Width = 18.897650000000000000
          Height = 279.685220000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            'ASSINATURA DO EMITENTE')
          ParentFont = False
          Rotation = 90
        end
        object Shape5: TfrxShapeView
          Left = 207.874150000000000000
          Top = 260.787570000000000000
          Width = 415.748300000000000000
          Height = 37.795300000000000000
          ShowHint = False
          Shape = skRoundRectangle
        end
        object Memo14: TfrxMemoView
          Left = 211.653680000000000000
          Top = 264.567100000000000000
          Width = 408.189240000000000000
          Height = 15.118120000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            
              'Descontos de [VARF_DESC_PERC] % - R$ [VARF_DESC_VAL] at'#233' [VARF_D' +
              'ESC_ATE]')
          ParentFont = False
        end
        object Memo15: TfrxMemoView
          Left = 211.653680000000000000
          Top = 279.685220000000000000
          Width = 408.189240000000000000
          Height = 15.118120000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            'Condi'#231#245'es especiais: [VARF_COND_ESPECIAL]')
          ParentFont = False
        end
        object Memo16: TfrxMemoView
          Left = 45.354360000000000000
          Top = 226.771800000000000000
          Width = 120.944881890000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_FAT_VAL]')
          ParentFont = False
        end
        object Memo17: TfrxMemoView
          Left = 173.858380000000000000
          Top = 226.771800000000000000
          Width = 102.047244090000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_FAT_NUM]')
          ParentFont = False
        end
        object Memo18: TfrxMemoView
          Left = 283.464750000000000000
          Top = 226.771800000000000000
          Width = 120.944881890000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_DUP_VAL]')
          ParentFont = False
        end
        object Memo19: TfrxMemoView
          Left = 415.748300000000000000
          Top = 226.771800000000000000
          Width = 102.047244090000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_DUP_NUM]')
          ParentFont = False
        end
        object Memo20: TfrxMemoView
          Left = 525.354670000000000000
          Top = 226.771800000000000000
          Width = 94.488188980000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_VENCTO]')
          ParentFont = False
        end
        object Shape6: TfrxShapeView
          Left = 207.874150000000000000
          Top = 302.362400000000000000
          Width = 548.031850000000000000
          Height = 109.606370000000000000
          ShowHint = False
          Shape = skRoundRectangle
        end
        object Memo21: TfrxMemoView
          Left = 215.433210000000000000
          Top = 306.141930000000000000
          Width = 532.913730000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            'Sacado: [VARF_SACADO]')
          ParentFont = False
          WordWrap = False
        end
        object Memo22: TfrxMemoView
          Left = 215.433210000000000000
          Top = 325.039580000000000000
          Width = 532.913730000000000000
          Height = 37.795280470000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            'Endere'#231'o: [VARF_ENDERECO]')
          ParentFont = False
        end
        object Shape7: TfrxShapeView
          Left = 207.874150000000000000
          Top = 411.968770000000000000
          Width = 548.031850000000000000
          Height = 56.692950000000000000
          ShowHint = False
          Shape = skRoundRectangle
        end
        object Memo23: TfrxMemoView
          Left = 272.126160000000000000
          Top = 415.748300000000000000
          Width = 476.220780000000000000
          Height = 49.133890000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[VARF_EXTENSO]')
          ParentFont = False
        end
        object Line9: TfrxLineView
          Left = 268.346630000000000000
          Top = 411.968770000000000000
          Height = 56.692950000000000000
          ShowHint = False
          Frame.Typ = [ftLeft]
        end
        object Memo24: TfrxMemoView
          Left = 211.653680000000000000
          Top = 415.748300000000000000
          Width = 56.692950000000000000
          Height = 49.133890000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            'VALOR POR EXTENSO')
          ParentFont = False
          VAlign = vaCenter
        end
        object Shape8: TfrxShapeView
          Left = 207.874150000000000000
          Top = 468.661720000000000000
          Width = 548.031850000000000000
          Height = 86.929190000000000000
          ShowHint = False
          Shape = skRoundRectangle
        end
        object Memo25: TfrxMemoView
          Left = 211.653680000000000000
          Top = 468.661720000000000000
          Width = 540.472790000000000000
          Height = 37.795300000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            
              'Reconhe'#231'o(cemos) a exatid'#227'o desta DUPLICATA de Venda Mercantil e' +
              '/ou Presta'#231#227'o de Servi'#231'os, na import'#226'ncia acima que pagarei(emos' +
              ') '#224' [frxDsDono."NOMEDONO"], ou '#224' sua ordem na pra'#231'a e vencimento' +
              ' acima indicados.')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo26: TfrxMemoView
          Left = 211.653680000000000000
          Top = 514.016080000000000000
          Width = 230.551330000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            'Em _______/_______/___________')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo27: TfrxMemoView
          Left = 445.984540000000000000
          Top = 514.016080000000000000
          Width = 306.141930000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '_________________________________________________')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo28: TfrxMemoView
          Left = 211.653680000000000000
          Top = 532.913730000000000000
          Width = 173.858380000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            'Data do aceite')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo29: TfrxMemoView
          Left = 445.984540000000000000
          Top = 532.913730000000000000
          Width = 306.141930000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            'Assinatura do sacado')
          ParentFont = False
          VAlign = vaCenter
        end
        object Picture1: TfrxPictureView
          Left = 45.354360000000000000
          Top = 34.015770000000000000
          Width = 325.039580000000000000
          Height = 113.385826770000000000
          ShowHint = False
          HightQuality = False
          Transparent = False
          TransparentColor = clWhite
        end
        object Memo30: TfrxMemoView
          Left = 215.433210000000000000
          Top = 362.834880000000000000
          Width = 370.393940000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            'Munic'#237'pio: [VARF_CIDADE]')
          ParentFont = False
        end
        object Memo31: TfrxMemoView
          Left = 215.433210000000000000
          Top = 377.953000000000000000
          Width = 370.393940000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            'P'#231'a. Pagto: [VARF_PRACAPGTO]')
          ParentFont = False
        end
        object Memo32: TfrxMemoView
          Left = 215.433210000000000000
          Top = 393.071120000000000000
          Width = 264.567100000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            'CNPJ / CPF: [VARF_DOCUM_SAC]')
          ParentFont = False
        end
        object Memo33: TfrxMemoView
          Left = 483.779840000000000000
          Top = 393.071120000000000000
          Width = 264.567100000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            'I.E. /  RG: [VARF_DOCUM2_SAC]')
          ParentFont = False
        end
        object Memo34: TfrxMemoView
          Left = 585.827150000000000000
          Top = 377.953000000000000000
          Width = 162.519790000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            'CEP: [VARF_CEP]')
          ParentFont = False
        end
        object Memo35: TfrxMemoView
          Left = 585.827150000000000000
          Top = 362.834880000000000000
          Width = 162.519790000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            'Estado: [VARF_UF]')
          ParentFont = False
        end
        object Memo36: TfrxMemoView
          Left = 45.354360000000000000
          Top = 153.070871020000000000
          Width = 325.039580000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          WordWrap = False
        end
        object Line10: TfrxLineView
          Left = 37.795300000000000000
          Top = 151.181200000000000000
          Width = 340.157700000000000000
          ShowHint = False
          Frame.Typ = [ftTop]
        end
      end
    end
  end
  object frxCompleta2: TfrxReport
    Version = '4.14'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39364.415347395800000000
    ReportOptions.LastChange = 39364.415347395800000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      '  if <LogoDuplExiste> = True then'
      '  begin'
      '    Picture1.LoadFromFile(<LogoDuplCaminho>);'
      '    Picture2.LoadFromFile(<LogoDuplCaminho>);'
      '  end;'
      'end.')
    OnGetValue = frxCompleta1GetValue
    Left = 257
    Top = 197
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = Dmod.frxDsMaster
        DataSetName = 'frxDsMaster'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      object ReportTitle1: TfrxReportTitle
        Height = 1099.843230000000000000
        Top = 18.897650000000000000
        Width = 793.701300000000000000
        object Line21: TfrxLineView
          Top = 555.590910000000000000
          Width = 797.480830000000000000
          ShowHint = False
          Frame.Style = fsDot
          Frame.Typ = [ftTop]
        end
        object Memo73: TfrxMemoView
          Left = 680.315400000000000000
          Top = 544.252320000000000000
          Width = 94.488250000000000000
          Height = 11.338590000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '1'#170' via')
          ParentFont = False
        end
        object Memo74: TfrxMemoView
          Left = 680.315400000000000000
          Top = 555.590910000000000000
          Width = 94.488250000000000000
          Height = 11.338590000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '2'#170' via')
          ParentFont = False
        end
        object Shape1: TfrxShapeView
          Left = 37.795300000000000000
          Top = 18.897650000000000000
          Width = 718.110700000000000000
          Height = 147.401670000000000000
          ShowHint = False
          Shape = skRoundRectangle
        end
        object Line1: TfrxLineView
          Left = 377.953000000000000000
          Top = 18.897650000000000000
          Height = 147.401670000000000000
          ShowHint = False
          Frame.Typ = [ftLeft]
        end
        object Memo1: TfrxMemoView
          Left = 381.732530000000000000
          Top = 22.677180000000000000
          Width = 362.834880000000000000
          Height = 68.031540000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftBottom]
          Memo.UTF8W = (
            '[frxDsDono."E_CUC"]')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          Left = 381.732530000000000000
          Top = 94.488250000000000000
          Width = 207.874150000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            'CNPJ: [frxDsDono."CNPJ_TXT"]')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          Left = 593.386210000000000000
          Top = 94.488250000000000000
          Width = 154.960730000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'I.E.: [frxDsDono."IE_RG"]')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          Left = 381.732530000000000000
          Top = 120.944960000000000000
          Width = 366.614410000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            'Data de emiss'#227'o: [VARF_EMISSAO]')
          ParentFont = False
        end
        object Shape2: TfrxShapeView
          Left = 37.795300000000000000
          Top = 173.858380000000000000
          Width = 585.827150000000000000
          Height = 75.590600000000000000
          ShowHint = False
          Shape = skRoundRectangle
        end
        object Line2: TfrxLineView
          Left = 37.795300000000000000
          Top = 192.756030000000000000
          Width = 483.779840000000000000
          ShowHint = False
          Frame.Typ = [ftTop]
        end
        object Line3: TfrxLineView
          Left = 521.575140000000000000
          Top = 173.858380000000000000
          Height = 75.590600000000000000
          ShowHint = False
          Frame.Typ = [ftLeft]
        end
        object Line4: TfrxLineView
          Left = 37.795300000000000000
          Top = 211.653680000000000000
          Width = 585.827150000000000000
          ShowHint = False
          Frame.Typ = [ftTop]
        end
        object Line5: TfrxLineView
          Left = 279.685220000000000000
          Top = 173.858380000000000000
          Height = 75.590600000000000000
          ShowHint = False
          Frame.Typ = [ftLeft]
        end
        object Line6: TfrxLineView
          Left = 170.078850000000000000
          Top = 192.756030000000000000
          Height = 56.692950000000000000
          ShowHint = False
          Frame.Typ = [ftLeft]
        end
        object Line7: TfrxLineView
          Left = 411.968770000000000000
          Top = 192.756030000000000000
          Height = 56.692950000000000000
          ShowHint = False
          Frame.Typ = [ftLeft]
        end
        object Memo5: TfrxMemoView
          Left = 45.354360000000000000
          Top = 173.858380000000000000
          Width = 226.771653540000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            'FATURA')
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          Left = 287.244280000000000000
          Top = 173.858380000000000000
          Width = 226.771653540000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            'DUPLICATA')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          Left = 45.354360000000000000
          Top = 192.756030000000000000
          Width = 120.944881890000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            'Valor R$')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          Left = 173.858380000000000000
          Top = 192.756030000000000000
          Width = 102.047244090000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            'N'#250'mero')
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          Left = 283.464750000000000000
          Top = 192.756030000000000000
          Width = 120.944881890000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            'Valor R$')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          Left = 415.748300000000000000
          Top = 192.756030000000000000
          Width = 102.047244090000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            'N'#250'mero')
          ParentFont = False
        end
        object Memo11: TfrxMemoView
          Left = 525.354670000000000000
          Top = 185.196970000000000000
          Width = 94.488188980000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            'VENCIMENTO')
          ParentFont = False
        end
        object Shape3: TfrxShapeView
          Left = 627.401980000000000000
          Top = 173.858380000000000000
          Width = 128.504020000000000000
          Height = 117.165430000000000000
          ShowHint = False
          Shape = skRoundRectangle
        end
        object Memo12: TfrxMemoView
          Left = 631.181510000000000000
          Top = 177.637910000000000000
          Width = 120.944960000000000000
          Height = 26.456710000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            'Para uso da institui'#231#227'o financeira')
          ParentFont = False
        end
        object Shape4: TfrxShapeView
          Left = 37.795300000000000000
          Top = 253.228510000000000000
          Width = 162.519790000000000000
          Height = 294.803340000000000000
          ShowHint = False
          Shape = skRoundRectangle
        end
        object Line8: TfrxLineView
          Left = 188.976500000000000000
          Top = 260.787570000000000000
          Height = 279.685220000000000000
          ShowHint = False
          Frame.Typ = [ftLeft]
        end
        object Memo13: TfrxMemoView
          Left = 173.858380000000000000
          Top = 260.787570000000000000
          Width = 18.897650000000000000
          Height = 279.685220000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            'ASSINATURA DO EMITENTE')
          ParentFont = False
          Rotation = 90
        end
        object Shape5: TfrxShapeView
          Left = 207.874150000000000000
          Top = 253.228510000000000000
          Width = 415.748300000000000000
          Height = 37.795300000000000000
          ShowHint = False
          Shape = skRoundRectangle
        end
        object Memo14: TfrxMemoView
          Left = 211.653680000000000000
          Top = 257.008040000000000000
          Width = 408.189240000000000000
          Height = 15.118120000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            
              'Descontos de [VARF_DESC_PERC] % - R$ [VARF_DESC_VAL] at'#233' [VARF_D' +
              'ESC_ATE]')
          ParentFont = False
        end
        object Memo15: TfrxMemoView
          Left = 211.653680000000000000
          Top = 272.126160000000000000
          Width = 408.189240000000000000
          Height = 15.118120000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            'Condi'#231#245'es especiais: [VARF_COND_ESPECIAL]')
          ParentFont = False
        end
        object Memo16: TfrxMemoView
          Left = 45.354360000000000000
          Top = 219.212740000000000000
          Width = 120.944881890000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_FAT_VAL]')
          ParentFont = False
        end
        object Memo17: TfrxMemoView
          Left = 173.858380000000000000
          Top = 219.212740000000000000
          Width = 102.047244090000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_FAT_NUM]')
          ParentFont = False
        end
        object Memo18: TfrxMemoView
          Left = 283.464750000000000000
          Top = 219.212740000000000000
          Width = 120.944881890000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_DUP_VAL]')
          ParentFont = False
        end
        object Memo19: TfrxMemoView
          Left = 415.748300000000000000
          Top = 219.212740000000000000
          Width = 102.047244090000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_DUP_NUM]')
          ParentFont = False
        end
        object Memo20: TfrxMemoView
          Left = 525.354670000000000000
          Top = 219.212740000000000000
          Width = 94.488188980000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_VENCTO]')
          ParentFont = False
        end
        object Shape6: TfrxShapeView
          Left = 207.874150000000000000
          Top = 294.803340000000000000
          Width = 548.031850000000000000
          Height = 109.606370000000000000
          ShowHint = False
          Shape = skRoundRectangle
        end
        object Memo21: TfrxMemoView
          Left = 215.433210000000000000
          Top = 298.582870000000000000
          Width = 532.913730000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            'Sacado: [VARF_SACADO]')
          ParentFont = False
          WordWrap = False
        end
        object Memo22: TfrxMemoView
          Left = 215.433210000000000000
          Top = 317.480520000000000000
          Width = 532.913730000000000000
          Height = 37.795280470000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            'Endere'#231'o: [VARF_ENDERECO]')
          ParentFont = False
        end
        object Shape7: TfrxShapeView
          Left = 207.874150000000000000
          Top = 404.409710000000000000
          Width = 548.031850000000000000
          Height = 56.692950000000000000
          ShowHint = False
          Shape = skRoundRectangle
        end
        object Memo23: TfrxMemoView
          Left = 272.126160000000000000
          Top = 408.189240000000000000
          Width = 476.220780000000000000
          Height = 49.133890000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[VARF_EXTENSO]')
          ParentFont = False
        end
        object Line9: TfrxLineView
          Left = 268.346630000000000000
          Top = 404.409710000000000000
          Height = 56.692950000000000000
          ShowHint = False
          Frame.Typ = [ftLeft]
        end
        object Memo24: TfrxMemoView
          Left = 211.653680000000000000
          Top = 408.189240000000000000
          Width = 56.692950000000000000
          Height = 49.133890000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            'VALOR POR EXTENSO')
          ParentFont = False
          VAlign = vaCenter
        end
        object Shape8: TfrxShapeView
          Left = 207.874150000000000000
          Top = 461.102660000000000000
          Width = 548.031850000000000000
          Height = 86.929190000000000000
          ShowHint = False
          Shape = skRoundRectangle
        end
        object Memo25: TfrxMemoView
          Left = 211.653680000000000000
          Top = 461.102660000000000000
          Width = 540.472790000000000000
          Height = 37.795300000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            
              'Reconhe'#231'o(cemos) a exatid'#227'o desta DUPLICATA de Venda Mercantil e' +
              '/ou Presta'#231#227'o de Servi'#231'os, na import'#226'ncia acima que pagarei(emos' +
              ') '#224' [frxDsDono."NOMEDONO"], ou '#224' sua ordem na pra'#231'a e vencimento' +
              ' acima indicados.')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo26: TfrxMemoView
          Left = 211.653680000000000000
          Top = 506.457020000000000000
          Width = 230.551330000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            'Em _______/_______/___________')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo27: TfrxMemoView
          Left = 445.984540000000000000
          Top = 506.457020000000000000
          Width = 306.141930000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '_________________________________________________')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo28: TfrxMemoView
          Left = 211.653680000000000000
          Top = 525.354670000000000000
          Width = 173.858380000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            'Data do aceite')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo29: TfrxMemoView
          Left = 445.984540000000000000
          Top = 525.354670000000000000
          Width = 306.141930000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            'Assinatura do sacado')
          ParentFont = False
          VAlign = vaCenter
        end
        object Picture1: TfrxPictureView
          Left = 45.354360000000000000
          Top = 26.456710000000000000
          Width = 325.039580000000000000
          Height = 113.385826770000000000
          ShowHint = False
          HightQuality = False
          Transparent = False
          TransparentColor = clWhite
        end
        object Memo30: TfrxMemoView
          Left = 215.433210000000000000
          Top = 355.275820000000000000
          Width = 370.393940000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            'Munic'#237'pio: [VARF_CIDADE]')
          ParentFont = False
        end
        object Memo31: TfrxMemoView
          Left = 215.433210000000000000
          Top = 370.393940000000000000
          Width = 370.393940000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            'P'#231'a. Pagto: [VARF_PRACAPGTO]')
          ParentFont = False
        end
        object Memo32: TfrxMemoView
          Left = 215.433210000000000000
          Top = 385.512060000000000000
          Width = 264.567100000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            'CNPJ / CPF: [VARF_DOCUM_SAC]')
          ParentFont = False
        end
        object Memo33: TfrxMemoView
          Left = 483.779840000000000000
          Top = 385.512060000000000000
          Width = 264.567100000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            'I.E. /  RG: [VARF_DOCUM2_SAC]')
          ParentFont = False
        end
        object Memo34: TfrxMemoView
          Left = 585.827150000000000000
          Top = 370.393940000000000000
          Width = 162.519790000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            'CEP: [VARF_CEP]')
          ParentFont = False
        end
        object Memo35: TfrxMemoView
          Left = 585.827150000000000000
          Top = 355.275820000000000000
          Width = 162.519790000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            'Estado: [VARF_UF]')
          ParentFont = False
        end
        object Memo36: TfrxMemoView
          Left = 45.354360000000000000
          Top = 145.511811020000000000
          Width = 325.039580000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          WordWrap = False
        end
        object Line10: TfrxLineView
          Left = 37.795300000000000000
          Top = 143.622140000000000000
          Width = 340.157700000000000000
          ShowHint = False
          Frame.Typ = [ftTop]
        end
        object Shape9: TfrxShapeView
          Left = 37.795300000000000000
          Top = 563.149970000000000000
          Width = 718.110700000000000000
          Height = 147.401670000000000000
          ShowHint = False
          Shape = skRoundRectangle
        end
        object Line11: TfrxLineView
          Left = 377.953000000000000000
          Top = 563.149970000000000000
          Height = 147.401670000000000000
          ShowHint = False
          Frame.Typ = [ftLeft]
        end
        object Memo37: TfrxMemoView
          Left = 381.732530000000000000
          Top = 566.929500000000000000
          Width = 362.834880000000000000
          Height = 68.031540000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftBottom]
          Memo.UTF8W = (
            '[frxDsDono."E_CUC"]')
          ParentFont = False
        end
        object Memo38: TfrxMemoView
          Left = 381.732530000000000000
          Top = 638.740570000000000000
          Width = 207.874150000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            'CNPJ: [frxDsDono."CNPJ_TXT"]')
          ParentFont = False
        end
        object Memo39: TfrxMemoView
          Left = 593.386210000000000000
          Top = 638.740570000000000000
          Width = 154.960730000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'I.E.: [frxDsDono."IE_RG"]')
          ParentFont = False
        end
        object Memo40: TfrxMemoView
          Left = 381.732530000000000000
          Top = 665.197280000000000000
          Width = 366.614410000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            'Data de emiss'#227'o: [VARF_EMISSAO]')
          ParentFont = False
        end
        object Shape10: TfrxShapeView
          Left = 37.795300000000000000
          Top = 718.110700000000000000
          Width = 585.827150000000000000
          Height = 75.590600000000000000
          ShowHint = False
          Shape = skRoundRectangle
        end
        object Line12: TfrxLineView
          Left = 37.795300000000000000
          Top = 737.008350000000000000
          Width = 483.779840000000000000
          ShowHint = False
          Frame.Typ = [ftTop]
        end
        object Line13: TfrxLineView
          Left = 521.575140000000000000
          Top = 718.110700000000000000
          Height = 75.590600000000000000
          ShowHint = False
          Frame.Typ = [ftLeft]
        end
        object Line14: TfrxLineView
          Left = 37.795300000000000000
          Top = 755.906000000000000000
          Width = 585.827150000000000000
          ShowHint = False
          Frame.Typ = [ftTop]
        end
        object Line15: TfrxLineView
          Left = 279.685220000000000000
          Top = 718.110700000000000000
          Height = 75.590600000000000000
          ShowHint = False
          Frame.Typ = [ftLeft]
        end
        object Line16: TfrxLineView
          Left = 170.078850000000000000
          Top = 737.008350000000000000
          Height = 56.692950000000000000
          ShowHint = False
          Frame.Typ = [ftLeft]
        end
        object Line17: TfrxLineView
          Left = 411.968770000000000000
          Top = 737.008350000000000000
          Height = 56.692950000000000000
          ShowHint = False
          Frame.Typ = [ftLeft]
        end
        object Memo41: TfrxMemoView
          Left = 45.354360000000000000
          Top = 718.110700000000000000
          Width = 226.771653540000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            'FATURA')
          ParentFont = False
        end
        object Memo42: TfrxMemoView
          Left = 287.244280000000000000
          Top = 718.110700000000000000
          Width = 226.771653540000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            'DUPLICATA')
          ParentFont = False
        end
        object Memo43: TfrxMemoView
          Left = 45.354360000000000000
          Top = 737.008350000000000000
          Width = 120.944881890000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            'Valor R$')
          ParentFont = False
        end
        object Memo44: TfrxMemoView
          Left = 173.858380000000000000
          Top = 737.008350000000000000
          Width = 102.047244090000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            'N'#250'mero')
          ParentFont = False
        end
        object Memo45: TfrxMemoView
          Left = 283.464750000000000000
          Top = 737.008350000000000000
          Width = 120.944881890000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            'Valor R$')
          ParentFont = False
        end
        object Memo46: TfrxMemoView
          Left = 415.748300000000000000
          Top = 737.008350000000000000
          Width = 102.047244090000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            'N'#250'mero')
          ParentFont = False
        end
        object Memo47: TfrxMemoView
          Left = 525.354670000000000000
          Top = 729.449290000000000000
          Width = 94.488188980000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            'VENCIMENTO')
          ParentFont = False
        end
        object Shape11: TfrxShapeView
          Left = 627.401980000000000000
          Top = 718.110700000000000000
          Width = 128.504020000000000000
          Height = 117.165430000000000000
          ShowHint = False
          Shape = skRoundRectangle
        end
        object Memo48: TfrxMemoView
          Left = 631.181510000000000000
          Top = 721.890230000000000000
          Width = 120.944960000000000000
          Height = 26.456710000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            'Para uso da institui'#231#227'o financeira')
          ParentFont = False
        end
        object Shape12: TfrxShapeView
          Left = 37.795300000000000000
          Top = 797.480830000000000000
          Width = 162.519790000000000000
          Height = 294.803340000000000000
          ShowHint = False
          Shape = skRoundRectangle
        end
        object Line18: TfrxLineView
          Left = 188.976500000000000000
          Top = 805.039890000000000000
          Height = 279.685220000000000000
          ShowHint = False
          Frame.Typ = [ftLeft]
        end
        object Memo49: TfrxMemoView
          Left = 173.858380000000000000
          Top = 805.039890000000000000
          Width = 18.897650000000000000
          Height = 279.685220000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            'ASSINATURA DO EMITENTE')
          ParentFont = False
          Rotation = 90
        end
        object Shape13: TfrxShapeView
          Left = 207.874150000000000000
          Top = 797.480830000000000000
          Width = 415.748300000000000000
          Height = 37.795300000000000000
          ShowHint = False
          Shape = skRoundRectangle
        end
        object Memo50: TfrxMemoView
          Left = 211.653680000000000000
          Top = 801.260360000000000000
          Width = 408.189240000000000000
          Height = 15.118120000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            
              'Descontos de [VARF_DESC_PERC] % - R$ [VARF_DESC_VAL] at'#233' [VARF_D' +
              'ESC_ATE]')
          ParentFont = False
        end
        object Memo51: TfrxMemoView
          Left = 211.653680000000000000
          Top = 816.378480000000000000
          Width = 408.189240000000000000
          Height = 15.118120000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            'Condi'#231#245'es especiais: [VARF_COND_ESPECIAL]')
          ParentFont = False
        end
        object Memo52: TfrxMemoView
          Left = 45.354360000000000000
          Top = 763.465060000000000000
          Width = 120.944881890000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_FAT_VAL]')
          ParentFont = False
        end
        object Memo53: TfrxMemoView
          Left = 173.858380000000000000
          Top = 763.465060000000000000
          Width = 102.047244090000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_FAT_NUM]')
          ParentFont = False
        end
        object Memo54: TfrxMemoView
          Left = 283.464750000000000000
          Top = 763.465060000000000000
          Width = 120.944881890000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_DUP_VAL]')
          ParentFont = False
        end
        object Memo55: TfrxMemoView
          Left = 415.748300000000000000
          Top = 763.465060000000000000
          Width = 102.047244090000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_DUP_NUM]')
          ParentFont = False
        end
        object Memo56: TfrxMemoView
          Left = 525.354670000000000000
          Top = 763.465060000000000000
          Width = 94.488188980000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_VENCTO]')
          ParentFont = False
        end
        object Shape14: TfrxShapeView
          Left = 207.874150000000000000
          Top = 839.055660000000000000
          Width = 548.031850000000000000
          Height = 109.606370000000000000
          ShowHint = False
          Shape = skRoundRectangle
        end
        object Memo57: TfrxMemoView
          Left = 215.433210000000000000
          Top = 842.835190000000000000
          Width = 532.913730000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            'Sacado: [VARF_SACADO]')
          ParentFont = False
          WordWrap = False
        end
        object Memo58: TfrxMemoView
          Left = 215.433210000000000000
          Top = 861.732840000000000000
          Width = 532.913730000000000000
          Height = 37.795280470000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            'Endere'#231'o: [VARF_ENDERECO]')
          ParentFont = False
        end
        object Shape15: TfrxShapeView
          Left = 207.874150000000000000
          Top = 948.662030000000000000
          Width = 548.031850000000000000
          Height = 56.692950000000000000
          ShowHint = False
          Shape = skRoundRectangle
        end
        object Memo59: TfrxMemoView
          Left = 272.126160000000000000
          Top = 952.441560000000000000
          Width = 476.220780000000000000
          Height = 49.133890000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[VARF_EXTENSO]')
          ParentFont = False
        end
        object Line19: TfrxLineView
          Left = 268.346630000000000000
          Top = 948.662030000000000000
          Height = 56.692950000000000000
          ShowHint = False
          Frame.Typ = [ftLeft]
        end
        object Memo60: TfrxMemoView
          Left = 211.653680000000000000
          Top = 952.441560000000000000
          Width = 56.692950000000000000
          Height = 49.133890000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            'VALOR POR EXTENSO')
          ParentFont = False
          VAlign = vaCenter
        end
        object Shape16: TfrxShapeView
          Left = 207.874150000000000000
          Top = 1005.354980000000000000
          Width = 548.031850000000000000
          Height = 86.929190000000000000
          ShowHint = False
          Shape = skRoundRectangle
        end
        object Memo61: TfrxMemoView
          Left = 211.653680000000000000
          Top = 1005.354980000000000000
          Width = 540.472790000000000000
          Height = 37.795300000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            
              'Reconhe'#231'o(cemos) a exatid'#227'o desta DUPLICATA de Venda Mercantil e' +
              '/ou Presta'#231#227'o de Servi'#231'os, na import'#226'ncia acima que pagarei(emos' +
              ') '#224' [frxDsDono."NOMEDONO"], ou '#224' sua ordem na pra'#231'a e vencimento' +
              ' acima indicados.')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo62: TfrxMemoView
          Left = 211.653680000000000000
          Top = 1050.709340000000000000
          Width = 230.551330000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            'Em _______/_______/___________')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo63: TfrxMemoView
          Left = 445.984540000000000000
          Top = 1050.709340000000000000
          Width = 306.141930000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '_________________________________________________')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo64: TfrxMemoView
          Left = 211.653680000000000000
          Top = 1069.606990000000000000
          Width = 173.858380000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            'Data do aceite')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo65: TfrxMemoView
          Left = 445.984540000000000000
          Top = 1069.606990000000000000
          Width = 306.141930000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            'Assinatura do sacado')
          ParentFont = False
          VAlign = vaCenter
        end
        object Picture2: TfrxPictureView
          Left = 45.354360000000000000
          Top = 570.709030000000000000
          Width = 325.039580000000000000
          Height = 113.385826770000000000
          ShowHint = False
          HightQuality = False
          Transparent = False
          TransparentColor = clWhite
        end
        object Memo66: TfrxMemoView
          Left = 215.433210000000000000
          Top = 899.528140000000000000
          Width = 370.393940000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            'Munic'#237'pio: [VARF_CIDADE]')
          ParentFont = False
        end
        object Memo67: TfrxMemoView
          Left = 215.433210000000000000
          Top = 914.646260000000000000
          Width = 370.393940000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            'P'#231'a. Pagto: [VARF_PRACAPGTO]')
          ParentFont = False
        end
        object Memo68: TfrxMemoView
          Left = 215.433210000000000000
          Top = 929.764380000000000000
          Width = 264.567100000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            'CNPJ / CPF: [VARF_DOCUM_SAC]')
          ParentFont = False
        end
        object Memo69: TfrxMemoView
          Left = 483.779840000000000000
          Top = 929.764380000000000000
          Width = 264.567100000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            'I.E. /  RG: [VARF_DOCUM2_SAC]')
          ParentFont = False
        end
        object Memo70: TfrxMemoView
          Left = 585.827150000000000000
          Top = 914.646260000000000000
          Width = 162.519790000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            'CEP: [VARF_CEP]')
          ParentFont = False
        end
        object Memo71: TfrxMemoView
          Left = 585.827150000000000000
          Top = 899.528140000000000000
          Width = 162.519790000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            'Estado: [VARF_UF]')
          ParentFont = False
        end
        object Memo72: TfrxMemoView
          Left = 45.354360000000000000
          Top = 689.764131020000000000
          Width = 325.039580000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          WordWrap = False
        end
        object Line20: TfrxLineView
          Left = 37.795300000000000000
          Top = 687.874460000000000000
          Width = 340.157700000000000000
          ShowHint = False
          Frame.Typ = [ftTop]
        end
      end
    end
  end
end
