object FmQRCodePrint: TFmQRCodePrint
  Left = 339
  Top = 185
  Caption = 'IMP-QRCOD-001 :: Impress'#227'o de QR Code'
  ClientHeight = 495
  ClientWidth = 701
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 701
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 653
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 605
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 287
        Height = 32
        Caption = 'Impress'#227'o de QR Code'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 287
        Height = 32
        Caption = 'Impress'#227'o de QR Code'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 287
        Height = 32
        Caption = 'Impress'#227'o de QR Code'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 701
    Height = 333
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 701
      Height = 333
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 701
        Height = 333
        Align = alClient
        TabOrder = 0
        object Image1: TImage
          Left = 8
          Top = 8
          Width = 317
          Height = 321
        end
        object Label1: TLabel
          Left = 335
          Top = 8
          Width = 34
          Height = 13
          Caption = 'Dados:'
        end
        object Label2: TLabel
          Left = 335
          Top = 135
          Width = 102
          Height = 13
          Caption = 'Tamanho da imagem:'
        end
        object Label3: TLabel
          Left = 335
          Top = 181
          Width = 130
          Height = 13
          Caption = 'N'#237'vel de corre'#231'ao de erros:'
        end
        object MemoData: TMemo
          Left = 335
          Top = 24
          Width = 350
          Height = 105
          Lines.Strings = (
            'Sample Data')
          MaxLength = 2000
          ScrollBars = ssBoth
          TabOrder = 0
        end
        object EditWidth: TMaskEdit
          Left = 335
          Top = 150
          Width = 67
          Height = 21
          EditMask = '!999;1; '
          MaxLength = 3
          TabOrder = 1
          Text = '300'
        end
        object EditHeight: TMaskEdit
          Left = 412
          Top = 150
          Width = 69
          Height = 21
          EditMask = '!999;1; '
          MaxLength = 3
          TabOrder = 2
          Text = '300'
        end
        object ComboBoxErrCorrLevel: TComboBox
          Left = 337
          Top = 196
          Width = 348
          Height = 21
          Style = csDropDownList
          ItemHeight = 13
          ItemIndex = 0
          TabOrder = 3
          Text = 'L  - [Padr'#227'o] Permite a recupera'#231#227'o de at'#233' 7% de perda de dados'
          Items.Strings = (
            'L  - [Padr'#227'o] Permite a recupera'#231#227'o de at'#233' 7% de perda de dados'
            'M - Permite a recupera'#231#227'o de at'#233' 15% de perda de dados'
            'Q - Permite a recupera'#231#227'o de at'#233' 25% de perda de dados'
            'H - Permite a recupera'#231#227'o de at'#233' 30% de perda de dados')
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 381
    Width = 701
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 697
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 425
    Width = 701
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 555
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
        NumGlyphs = 2
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 553
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        TabOrder = 0
        OnClick = BtOKClick
        NumGlyphs = 2
      end
    end
  end
end
