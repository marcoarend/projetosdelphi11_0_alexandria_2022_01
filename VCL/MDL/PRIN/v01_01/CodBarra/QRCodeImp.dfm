�
 TFMQRCODEIMP 0�  TPF0TFmQrCodeImpFmQrCodeImpLeft� Top� Caption&   IMP-QRCOD-001 :: Impressão de QR CodeClientHeightClientWidthColor	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style OldCreateOrderPositionpoScreenCenter
OnActivateFormActivateOnCreate
FormCreateOnResize
FormResizePixelsPerInch`
TextHeight TPanelPanel1Left Top4WidthHeightqAlignalClient
BevelOuterbvNoneTabOrder  TImageImage1LeftTopWidth�Heighti  TLabelLabel1Left�Top� WidthHeightCaptionValor:  TPageControlPageControl1Left�TopWidth�Height� 
ActivePage	TabSheet1TabOrder  	TTabSheet	TabSheet1Caption   Código asteca:ExplicitLeft ExplicitTop ExplicitWidth ExplicitHeight  TLabelLabel2LeftTopWidth;HeightCaption   Codificação:  TLabelLabel3LeftTop8WidthHeightCaptionFNC1  TLabelLabel4LeftTopPWidthdHeightCaption   Tamanho do módulo:  TLabelMarginLeft� Top8Width)HeightCaptionMargem:  TLabelLabel6Left� TopWidthYHeightCaption   Nível de correção:  TLabelLabel8LeftTop Width$HeightCaption   Versão:  TLabelLabel9Left� Top WidthSHeightCaptionIdentificador App:  TLabelLabel5Left� TopPWidth� HeightCaption!   Tamanho do módulo na impressão:  	TComboBox
CBEncodingLeftLTopWidthaHeightTabOrder TextAUTOItems.StringsALPHABYTENUMERICKANJIAUTO   	TComboBoxCBFNC1LeftLTop4WidthaHeightTabOrderTextNOItems.StringsNOFIRSTSECOND   TEditEdSizeLefttTopLWidth!HeightTabOrderText4  TEditEdMarginLeft4Top4WidthAHeightTabOrderText10  	TComboBox	CBVersionLeftLTopWidthaHeightTabOrderText1Items.Strings12345678910111213141516171819202122232425262728293031323334353637383940   TEditEdAppIdentifierLeft4TopWidthAHeightTabOrderText-1  	TComboBoxComboECLeft4TopWidthAHeightTabOrderTextLItems.StringsLMQH   	TCheckBoxCkAutoConfigurateLeftTophWidth� Height	AlignmenttaLeftJustifyCaption   Auto configuração:Checked	State	cbCheckedTabOrder  TEditEdPrinterSizeLeftLTopLWidth)HeightTabOrderText40   	TTabSheet	TabSheet2CaptionEstrutura anexada:
ImageIndexExplicitLeft ExplicitTop ExplicitWidth ExplicitHeight  TLabelLabel10LeftTop4Width HeightCaption   Índice:  TLabelLabel7LeftTopPWidthHeightCaptionTotal:  TEditEdIndexLeft0Top,Width9HeightTabOrder Text0  TEditEdTotalLeft0TopLWidth9HeightTabOrderText0  	TCheckBoxCkStructuredAppendLeftTopWidth� HeightCaptionAtivar estrutura anexadaTabOrder    TMemoMeTextoLeft�Top� Width�Height� Lines.Strings  TabOrderWantReturnsWantTabs	WordWrap   TPanelPnCabecaLeft Top WidthHeight4AlignalTop
BevelOuterbvNoneTabOrder 	TGroupBoxGB_RLeft�Top Width0Height4AlignalRightTabOrder  	TdmkImageImgTipoLeftTopWidth Height Transparent	SQLTypestNil   	TGroupBoxGB_LLeft Top Width0Height4AlignalLeftTabOrder TBitBtn	SbImprimeTagLeftTopWidth(Height(	NumGlyphsTabOrder OnClickSbImprimeClick   	TGroupBoxGB_MLeft0Top Width�Height4AlignalClientTabOrder TLabel
LaTitulo1ALeftTop	WidthHeight Caption   Impressão de QR CodeColor	clBtnFaceFont.CharsetANSI_CHARSET
Font.ColorclGradientActiveCaptionFont.Height�	Font.NameArial
Font.Style ParentColor
ParentFontVisible  TLabel
LaTitulo1BLeft	TopWidthHeight Caption   Impressão de QR CodeColor	clBtnFaceFont.CharsetANSI_CHARSET
Font.ColorclSilverFont.Height�	Font.NameArial
Font.Style ParentColor
ParentFont  TLabel
LaTitulo1CLeftTop
WidthHeight Caption   Impressão de QR CodeColor	clBtnFaceFont.CharsetANSI_CHARSET
Font.Color
clHotLightFont.Height�	Font.NameArial
Font.Style ParentColor
ParentFont    	TGroupBox	GBAvisos1Left Top�WidthHeight,AlignalBottomCaption	 Avisos: TabOrder TPanelPanel4LeftTopWidthHeightAlignalClient
BevelOuterbvNoneTabOrder  TLabelLaAviso1LeftTopWidthxHeightCaption..............................Font.CharsetDEFAULT_CHARSET
Font.ColorclSilverFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparent	  TLabelLaAviso2LeftTopWidthxHeightCaption..............................Font.CharsetDEFAULT_CHARSET
Font.ColorclRedFont.Height�	Font.NameArial
Font.Style 
ParentFontTransparent	    	TGroupBoxGBRodaPeLeft Top�WidthHeightFAlignalBottomTabOrder TPanel
PnSaiDesisLeft�TopWidth� Height5AlignalRight
BevelOuterbvNoneTabOrder TBitBtnBtSaidaTagLeftTopWidthxHeight(CursorcrHandPointCaption&Desiste	NumGlyphsParentShowHintShowHint	TabOrder OnClickBtSaidaClick   TPanelPanel2LeftTopWidth�Height5AlignalClient
BevelOuterbvNoneTabOrder  TButton	BtRefreshTagLeftTopWidthxHeight(CaptionRefreshTabOrder OnClickBtRefreshClick  TButtonBtPrintTagLeft� TopWidthxHeight(CaptionImprimeTabOrderOnClickBtPrintClick  TButtonBtSalvaTagLeftTopWidthxHeight(Caption	Salva BMPTabOrderOnClickBtSalvaClick    
TfrxReport
frxReport1Version4.14DotMatrixReportIniFile\Software\Fast ReportsPreviewOptions.ButtonspbPrintpbLoadpbSavepbExportpbZoompbFind	pbOutlinepbPageSetuppbToolspbEditpbNavigatorpbExportQuick PreviewOptions.Zoom       ��?PrintOptions.PrinterDefaultPrintOptions.PrintOnSheet ReportOptions.CreateDate ���bm�@ReportOptions.LastChange ���bm�@ScriptLanguagePascalScriptScriptText.Stringsbegin    end. Left� Top� Datasets 	Variables Style  TfrxDataPageDataHeight       �@Width       �@  TfrxReportPagePage1
PaperWidth       �@PaperHeight      ��@	PaperSize	
LeftMargin       �@RightMargin       �@	TopMargin       �@BottomMargin       �@ TfrxPictureViewPicture1Width  �&�@Height �-�R\��@ShowHintHightQualityTransparentTransparentColorclWhite     