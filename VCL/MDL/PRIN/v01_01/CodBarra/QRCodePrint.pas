unit QRCodePrint;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnMLAGeral, UnInternalConsts,
  dmkImage, Mask, frxpngimage;

type
  //
  TQrImage_ErrCorrLevel=(L,M,Q,H);
  //
  TFmQRCodePrint = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    Image1: TImage;
    Label1: TLabel;
    MemoData: TMemo;
    Label2: TLabel;
    EditWidth: TMaskEdit;
    EditHeight: TMaskEdit;
    Label3: TLabel;
    ComboBoxErrCorrLevel: TComboBox;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  var
  FmQRCodePrint: TFmQRCodePrint;

implementation

uses UnMyObjects, Module, HTTPApp, WinInet;

{$R *.DFM}

const
  UrlGoogleQrCode='http://chart.apis.google.com/chart?chs=%dx%d&cht=qr&chld=%s&chl=%s';
  QrImgCorrStr   : array [TQrImage_ErrCorrLevel] of string=('L','M','Q','H');

procedure WinInet_HttpGet(const Url: string;Stream:TStream);
const
BuffSize = 1024*1024;
var
  hInter   : HINTERNET;
  UrlHandle: HINTERNET;
  BytesRead: DWORD;
  Buffer   : Pointer;
begin
  hInter := InternetOpen('', INTERNET_OPEN_TYPE_PRECONFIG, nil, nil, 0);
  if Assigned(hInter) then
  begin
    Stream.Seek(0,0);
    GetMem(Buffer,BuffSize);
    try
        UrlHandle := InternetOpenUrl(hInter, PChar(Url), nil, 0, INTERNET_FLAG_RELOAD, 0);
        if Assigned(UrlHandle) then
        begin
          repeat
            InternetReadFile(UrlHandle, Buffer, BuffSize, BytesRead);
            if BytesRead>0 then
             Stream.WriteBuffer(Buffer^,BytesRead);
          until BytesRead = 0;
          InternetCloseHandle(UrlHandle);
        end;
    finally
      FreeMem(Buffer);
    end;
    InternetCloseHandle(hInter);
  end
end;

procedure GetQrCode(Width,Height:Word;Correction_Level:TQrImage_ErrCorrLevel;const Data:string;StreamImage : TMemoryStream);
Var
 EncodedURL  : string;
begin
  EncodedURL:=Format(UrlGoogleQrCode,[Width,Height,QrImgCorrStr[Correction_Level],HTTPEncode(Data)]);
  WinInet_HttpGet(EncodedURL,StreamImage);
end;



procedure TFmQRCodePrint.BtOKClick(Sender: TObject);
var
  ImageStream : TMemoryStream;
  PngImage    : TPngObject;
begin
  Image1.Picture:=nil;
  ImageStream:=TMemoryStream.Create;
  PngImage   :=TPngObject.Create;
  try
    try
      GetQrCode(StrToInt(Trim(EditWidth.Text)),StrToInt(Trim(EditHeight.Text)),TQrImage_ErrCorrLevel(ComboBoxErrCorrLevel.ItemIndex),MemoData.Lines.Text,ImageStream);
      if ImageStream.Size>0 then
      begin
        ImageStream.Position:=0;
        PngImage.LoadFromStream(ImageStream);
        Image1.Picture.Assign(PngImage);
      end;
    except
      on E: exception do
      ShowMessage(E.Message);
    end;
  finally
    ImageStream.Free;
    PngImage.Free;
  end;
end;


{
procedure TFmQRCodePrint.BtOKClick(Sender: TObject);
var
  ImageStream : TMemoryStream;
  //PngImage    : TPngImage;
  PngImage: TBitmap;
begin
  Image1.Picture:=nil;
  ImageStream:=TMemoryStream.Create;
  PngImage   := TBitmap.Create;
  try
    try
      GetQrCode(StrToInt(Trim(EditWidth.Text)),StrToInt(Trim(EditHeight.Text)),TQrImage_ErrCorrLevel(ComboBoxErrCorrLevel.ItemIndex),MemoData.Lines.Text,ImageStream);
      if ImageStream.Size>0 then
      begin
        ImageStream.Position:=0;
        PngImage.LoadFromStream(ImageStream);
        Image1.Picture.Assign(PngImage);
      end;
    except
      on E: exception do
      ShowMessage(E.Message);
    end;
  finally
    ImageStream.Free;
    PngImage.Free;
  end;
end;
}
procedure TFmQRCodePrint.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmQRCodePrint.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  ImgTipo.SQLType := stPsq;
end;

procedure TFmQRCodePrint.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  BtOKClick(Sender);
end;

procedure TFmQRCodePrint.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

end.
