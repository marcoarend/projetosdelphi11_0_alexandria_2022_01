unit QRCodeImp;

interface

uses
  QRCode, Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ComCtrls, ExtCtrls,printers, Buttons, dmkImage, dmkGeral,
  UnMyObjects, UnDmkEnums, frxClass;

type
  TFmQrCodeImp = class(TForm)
    Panel1: TPanel;
    Image1: TImage;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Margin: TLabel;
    Label6: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label5: TLabel;
    CBEncoding: TComboBox;
    CBFNC1: TComboBox;
    EdSize: TEdit;
    EdMargin: TEdit;
    CBVersion: TComboBox;
    EdAppIdentifier: TEdit;
    ComboEC: TComboBox;
    CkAutoConfigurate: TCheckBox;
    EdPrinterSize: TEdit;
    TabSheet2: TTabSheet;
    Label10: TLabel;
    Label7: TLabel;
    EdIndex: TEdit;
    EdTotal: TEdit;
    CkStructuredAppend: TCheckBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtRefresh: TButton;
    BtPrint: TButton;
    BtSalva: TButton;
    Label1: TLabel;
    MeTexto: TMemo;
    SbImprime: TBitBtn;
    frxReport1: TfrxReport;
    procedure BtRefreshClick(Sender: TObject);
    procedure BtPrintClick(Sender: TObject);
    procedure BtSalvaClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
  private
    { Private declarations }
    QRCode: TQRCode;
  public
    { Public declarations }
  end;

var
  FmQrCodeImp: TFmQrCodeImp;


implementation

{$R *.DFM}

procedure TFmQrCodeImp.BtRefreshClick(Sender: TObject);
var
  Dummy: Integer;
  Valor: Integer;
begin

  {create new barcode}
  {encoding}
  if (CBEncoding.itemIndex>=0) then
    QRCode.encoding:=TQRCodeEncoding(CBEncoding.itemIndex);

  {value}
  QRCode.code := MeTexto.Text;
  QRCode.processTilde:=true;
  {format}
  if (CBVersion.itemIndex>=0) then QRCode.preferredVersion:=CBVersion.itemIndex+1;

  if (CBFNC1.itemIndex>=0) then QRCode.fnc1mode:=TQRCodeFNC1Mode(CBFNC1.itemIndex);

  if (ComboEC.itemIndex>=0) then QRCode.correctionLevel:=TQRCodeECLevel(ComboEC.itemIndex);

  {dots size}
  Val(EdSize.Text, Valor, Dummy);
  QRCode.moduleWidth := Valor;
  {margin}
  Val(EdMargin.text, Valor, Dummy);
  QRCode.marginPixels := Valor;
  {rune}
  if (EdAppIdentifier.Text = '') then EdAppIdentifier.Text := '-1';
  Val(EdAppIdentifier.Text, Valor, Dummy);
  QRCode.applicationIndicator := Valor;
  {correction level}


  QRCode.width:=Image1.width;
  QRCode.height:=Image1.height;

  {structured append}
  QRCode.structuredAppend:=false;
  if (CkStructuredAppend.Checked) then
  begin
    QRCode.structuredAppend := True;
  end;

  QRCode.structuredAppendCounter:=1;
  Val(EdTotal.Text, Valor, Dummy);
  QRCode.structuredAppendCounter := Valor;

  QRCode.structuredAppendIndex := 1;
  Val(EdIndex.Text, valor, Dummy);
  QRCode.structuredAppendindex := Valor;

  QRCode.autoconfigurate := CkAutoConfigurate.Checked;
  QRCode.paintBarcode(Image1.Canvas);
end;

procedure TFmQrCodeImp.BtPrintClick(Sender: TObject);
var
  Valor: Integer;
  Dummy: Integer;
begin
  with Printer do
  begin
		BeginDoc;

    {dots size}
    Val(EdPrinterSize.Text, Valor, Dummy);
    QRCode.moduleWidth := Valor;

    QRCode.offsetX := Printer.PageWidth div 10;
		QRCode.paintBarcode(Canvas);

		EndDoc;
  end;
end;

procedure TFmQrCodeImp.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmQrCodeImp.BtSalvaClick(Sender: TObject);
var
  BMP: TBitmap;
begin
  BMP := TBitmap.Create();
  BMP.Height := 400;
  BMP.Width := 400;
  QRCode.paintBarcode(bmp.Canvas);
  BMP.SaveToFile('QRCode.bmp');
  Geral.MB_Info('Imagem salva como "QRCode.bmp"');
end;

procedure TFmQrCodeImp.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmQrCodeImp.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  QRCode := TQRCode.Create(nil) ;
  QRCode.autoConfigurate := True;
  {refresh}
  CBEncoding.itemIndex := 4;
  ComboEC.ItemIndex := 0;
  CBVersion.itemIndex := 0;
  BtRefreshClick(nil);
end;

procedure TFmQrCodeImp.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmQrCodeImp.SbImprimeClick(Sender: TObject);
(*
      TempStream := TMemoryStream.Create;
      FmMyGlyfs.LogosBancos.Items[ID].Bitmap.SaveToStream(TempStream);
      TempStream.Position := 0;
      Image2.Picture.Bitmap.LoadFromStream(TempStream);
      Picture2.Picture.Bitmap.LoadFromStream(TempStream);

*)
var
  frxBmp: TfrxPictureView;
begin
  frxBmp := frxReport1.FindObject('Picture1') as TfrxPictureView;
  frxBmp.Picture.Bitmap.Assign(Image1.Picture.Bitmap);
  MyObjects.frxMostra(frxReport1, 'QrCode');
end;

end.
