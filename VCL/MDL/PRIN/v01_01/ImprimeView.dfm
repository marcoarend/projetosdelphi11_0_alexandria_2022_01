object FmImprimeView: TFmImprimeView
  Left = 339
  Top = 167
  Caption = 'Edi'#231#227'o de Campos de Impress'#227'o'
  ClientHeight = 730
  ClientWidth = 1241
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -14
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 120
  TextHeight = 16
  object PainelDados: TPanel
    Left = 0
    Top = 59
    Width = 1241
    Height = 531
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object PCTabelas: TPageControl
      Left = 0
      Top = 213
      Width = 1241
      Height = 318
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      ActivePage = TabSheet10
      Align = alClient
      TabOrder = 0
      object TabSheet1: TTabSheet
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = ' NF Formato A '
        object Panel2: TPanel
          Left = 0
          Top = 0
          Width = 1233
          Height = 287
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alClient
          TabOrder = 0
          object RGNF_A: TRadioGroup
            Left = 1
            Top = 1
            Width = 1231
            Height = 285
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alClient
            Caption = '  Tipo de campo: '
            Columns = 5
            ItemIndex = 0
            Items.Strings = (
              'NENHUM'
              'Sa'#237'da [X]'
              'Entrada [X]'
              'Data emiss'#227'o'
              'Data entra/sai'
              'C'#243'digo CFOP'
              'Descri'#231#227'o CFOP'
              'Base c'#225'lculo ICMS'
              'Valor ICMS'
              'Base c'#225'lc. ICMS subst.'
              'Valor ICMS subst.'
              'Valor frete'
              'Valor seguro'
              'Outras desp. aces.'
              'Valor total IPI'
              'Valor total produtos'
              'Valor total servicos'
              'Valor total nota'
              'Placa ve'#237'culo'
              'UF placa ve'#237'culo'
              'Vol. Quantidade'
              'Vol. Esp'#233'cie'
              'Vol. Marca '
              'Vol. N'#250'mero'
              'Vol. kg bruto'
              'Vol. kg l'#237'quido'
              'Dados adicionais'
              'Nota Fiscal'
              'Frete por conta de ...'
              'Desconto especial')
            TabOrder = 0
          end
        end
      end
      object TabSheet2: TTabSheet
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Entidades'
        ImageIndex = 1
        object RGEntidades: TRadioGroup
          Left = 0
          Top = 0
          Width = 1233
          Height = 287
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alClient
          Caption = ' Campo: '
          Columns = 3
          ItemIndex = 0
          Items.Strings = (
            'Nome ou Raz'#227'o Social'
            'CNPJ ou CPF'
            'Endere'#231'o'
            'Bairro'
            'CEP'
            'Cidade'
            'Telefone'
            'UF'
            'I.E. ou RG'
            'I.E.S.T.')
          TabOrder = 0
        end
      end
      object TabSheet3: TTabSheet
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Vendas'
        ImageIndex = 2
        object RGProdutos: TRadioGroup
          Left = 0
          Top = 0
          Width = 1233
          Height = 287
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alClient
          Caption = ' Item de faturamento: '
          Columns = 4
          ItemIndex = 0
          Items.Strings = (
            'C'#243'digo'
            'Descri'#231#227'o'
            'Classifica'#231#227'o Fiscal'
            'Situa'#231#227'o Tribut'#225'ria'
            'Unidade'
            'Quantidade'
            'Valor unit'#225'rio'
            'Valor total'
            'Aliquota ICMS'
            'Aliquota IPI'
            'Valor IPI'
            'CFOP'
            'Refer'#234'ncia')
          TabOrder = 0
        end
      end
      object TabSheet4: TTabSheet
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Servi'#231'os'
        ImageIndex = 3
        object RGServicos: TRadioGroup
          Left = 0
          Top = 0
          Width = 1233
          Height = 287
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alClient
          Caption = ' Item de faturamento: '
          Columns = 4
          ItemIndex = 0
          Items.Strings = (
            'C'#243'digo'
            'Descri'#231#227'o'
            'Classifica'#231#227'o Fiscal'
            'Situa'#231#227'o Tribut'#225'ria'
            'Unidade'
            'Quantidade'
            'Valor unit'#225'rio'
            'Valor total'
            'Aliquota ICMS'
            'Aliquota IPI'
            'Valor IPI'
            'CFOP'
            'Refer'#234'ncia')
          TabOrder = 0
        end
      end
      object TabSheet5: TTabSheet
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Faturamento'
        ImageIndex = 4
        object RGFaturamento: TRadioGroup
          Left = 0
          Top = 0
          Width = 1233
          Height = 287
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alClient
          Caption = ' Item de faturamento: '
          Columns = 3
          ItemIndex = 0
          Items.Strings = (
            'Parcela'
            'Valor'
            'Vencimento')
          TabOrder = 0
        end
      end
      object TabSheet6: TTabSheet
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Transportadora'
        ImageIndex = 5
        object RGTransportadora: TRadioGroup
          Left = 0
          Top = 0
          Width = 1233
          Height = 287
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alClient
          Caption = ' Campo: '
          Columns = 3
          ItemIndex = 0
          Items.Strings = (
            'Nome ou Raz'#227'o Social'
            'CNPJ ou CPF'
            'Endere'#231'o'
            'Bairro'
            'CEP'
            'Cidade'
            'Telefone'
            'UF'
            'I.E. ou RG'
            'I.E.S.T.')
          TabOrder = 0
        end
      end
      object TabSheet7: TTabSheet
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Duplicata'
        ImageIndex = 6
        object RGDuplicata: TRadioGroup
          Left = 0
          Top = 0
          Width = 1233
          Height = 287
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alClient
          Caption = ' Item de duplicata: '
          Columns = 4
          Items.Strings = (
            'Dia emiss'#227'o'
            'M'#234's emiss'#227'o'
            'Ano emiss'#227'o'
            'Valor fatura'
            'N'#250'mero fatura'
            'Valor duplicata'
            'N'#250'mero de ordem'
            'Vencimento'
            'Desconto de %'
            'Desconto de $'
            'Desconto at'#233
            'Condi'#231#245'es especiais'
            'Sacado'
            'Endere'#231'o'
            'Munic'#237'pio'
            'Estado'
            'P'#231'a. Pgto'
            'CEP'
            'CNPJ/CPF'
            'I.E.'
            'Valor extenso')
          TabOrder = 0
          OnClick = RGDuplicataClick
        end
      end
      object TabSheet8: TTabSheet
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = ' NF Formato B '
        ImageIndex = 7
        object RGNF_B: TRadioGroup
          Left = 0
          Top = 0
          Width = 1233
          Height = 287
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alClient
          Caption = '  Tipo de campo: '
          Columns = 6
          ItemIndex = 0
          Items.Strings = (
            'NENHUM'
            'Sa'#237'da [X]'
            'Entrada [X]'
            'Data emiss'#227'o'
            'Data entra/sai'
            'C'#243'd CFOP '#250'nico'
            'C'#243'd CFOP 1'
            'C'#243'd CFOP 2'
            'Descr. CFOP '#250'nico'
            'Descr. CFOP 1'
            'Descr. CFOP 2'
            'I.E. Subst. Trib.'
            'Cliente Nome'
            'Cliente CNPJ/CPF'
            'Cliente Endere'#231'o'
            'Cliente Bairro'
            'Cliente CEP'
            'Cliente Munic'#237'pio'
            'Cliente Fone/Fax'
            'Cliente UF'
            'Cliente I.E.'
            'Fat. Num. col. 1'
            'Fat. Valor col. 1'
            'Fat. Data col. 1'
            'Fat. Num. col. 2'
            'Fat. Valor col. 2'
            'Fat. Data col. 2'
            'Fat. Num. col. 3'
            'Fat. Valor col. 3'
            'Fat. Data col. 3'
            'Desconto de ...'
            'Desconto at'#233' ...'
            'Pra'#231'a pagamento'
            'Prod. C'#243'digo'
            'Prod. Descri'#231#227'o'
            'Prod. Sit.Trib.'
            'Prod. Unidade'
            'Prod. Quantidade'
            'Prod. Pre'#231'o'
            'Prod. Valor total'
            'Prod. % ICMS'
            'Base c'#225'lculo ICMS'
            'Valor ICMS'
            'Base c'#225'lc. ICMS sub.'
            'Valor ICMS subst.'
            'Valor total produtos'
            'Valor frete'
            'Valor seguro'
            'Outras desp. aces.'
            '?'
            'Valor total nota'
            'Transp. Nome'
            'Transp. FPC'
            'Placa Ve'#237'culo'
            'UF placa ve'#237'culo'
            'Transp. CNPJ/CPF'
            'Transp. Endere'#231'o'
            'Transp. Munic'#237'pio'
            'Transp. UF'
            'Transp. I.E.'
            'Vol. Quantidade'
            'Vol. Esp'#233'cie'
            'Vol. Marca '
            'Vol. N'#250'mero'
            'Vol. kg bruto'
            'Vol. kg l'#237'quido'
            'Dados adicionais'
            'Valor extenso'
            'Prod. Class. Fisc.'
            'Dados corpo NF')
          TabOrder = 0
          OnClick = RGNF_BClick
        end
      end
      object TabSheet9: TTabSheet
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = ' Lista de NCMs '
        ImageIndex = 8
        object RGNCMs: TdmkRadioGroup
          Left = 0
          Top = 0
          Width = 1233
          Height = 287
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alClient
          Caption = ' Tipo de campo: '
          Items.Strings = (
            '[NENHUM]'
            'Letra Coluna 1'
            'NCM Coluna 1'
            'Letra Coluna 2'
            'NCM Coluna 2'
            'Letra Coluna 3'
            'NCM Coluna 3'
            'Letra Coluna 4'
            'NCM Coluna 4'
            'Letra Coluna 5'
            'NCM Coluna 5'
            'Letra Coluna 6'
            'NCM Coluna 6')
          TabOrder = 0
          OnClick = RGNF_BClick
          UpdType = utYes
          OldValor = 0
        end
      end
      object TabSheet10: TTabSheet
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = ' NF Formato C '
        ImageIndex = 9
        object RGNF_C: TRadioGroup
          Left = 0
          Top = 0
          Width = 1233
          Height = 287
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alClient
          Caption = '  Tipo de campo: '
          Columns = 8
          ItemIndex = 0
          Items.Strings = (
            '[NENHUM]'
            'Sa'#237'da [X]'
            'Entrada [X]'
            'Data emiss'#227'o'
            'Data entra/sai'
            'C'#243'd CFOP '#250'nico'
            'C'#243'd CFOP 1'
            'C'#243'd CFOP 2'
            'Descr. CFOP '#250'nico'
            'Descr. CFOP 1'
            'Descr. CFOP 2'
            'I.E. Subst. Trib.'
            'Cliente Nome'
            'Cliente CNPJ/CPF'
            'Cliente Endere'#231'o'
            'Cliente Bairro'
            'Cliente CEP'
            'Cliente Munic'#237'pio'
            'Cliente Fone/Fax'
            'Cliente UF'
            'Cliente I.E.'
            'Fat. Seq. col. 1'
            'Fat. Dupl. col. 1'
            'Fat. Valor col. 1'
            'Fat. Data col. 1'
            'Fat. Seq. col. 2'
            'Fat. Dupl. col. 2'
            'Fat. Valor col. 2'
            'Fat. Data col. 2'
            'Fat. Seq. col. 3'
            'Fat. Dupl. col. 3'
            'Fat. Valor col. 3'
            'Fat. Data col. 3'
            'Fat. Seq. col. 4'
            'Fat. Dupl. col. 4'
            'Fat. Valor col. 4'
            'Fat. Data col. 4'
            'Fat. Seq. col. 5'
            'Fat. Dupl. col. 5'
            'Fat. Valor col. 5'
            'Fat. Data col. 5'
            'Fat. Seq. col. 6'
            'Fat. Dupl. col. 6'
            'Fat. Valor col. 6'
            'Fat. Data col. 6'
            'Desconto de ...'
            'Desconto at'#233' ...'
            'Pra'#231'a pagamento'
            'Prod. C'#243'digo'
            'Prod. Descri'#231#227'o'
            'Prod. Sit.Trib.'
            'Prod. Class. Fisc.'
            'Prod. Letra da NCM'
            'Prod. Unidade'
            'Prod. Quantidade'
            'Prod. Pre'#231'o'
            'Prod. Valor total'
            'Prod. % ICMS'
            'Prod. % IPI'
            'Prod. Valor IPI'
            'Base c'#225'lculo ICMS'
            'Valor ICMS'
            'Base c'#225'lc. ICMS sub.'
            'Valor ICMS subst.'
            'Valor total produtos'
            'Valor frete'
            'Valor seguro'
            'Outras desp. aces.'
            'Valor total nota'
            'Transp. Nome'
            'Transp. FPC'
            'Placa Ve'#237'culo'
            'UF placa ve'#237'culo'
            'Transp. CNPJ/CPF'
            'Transp. Endere'#231'o'
            'Transp. Munic'#237'pio'
            'Transp. UF'
            'Transp. I.E.'
            'Vol. Quantidade'
            'Vol. Esp'#233'cie'
            'Vol. Marca'
            'Vol. N'#250'mero'
            'Vol. kg bruto'
            'Vol. kg l'#237'quido'
            'Dados adicionais'
            'Valor extenso'
            'Dados corpo NF'
            'NCM col. 1 Letra'
            'NCM col. 1 NCM'
            'NCM col. 2 Letra'
            'NCM col. 2 NCM'
            'NCM col. 3 Letra'
            'NCM col. 3 NCM'
            'NCM col. 4 Letra'
            'NCM col. 4 NCM'
            'NCM col. 5 Letra'
            'NCM col. 5 NCM'
            'NCM col. 6 Letra'
            'NCM col. 6 NCM'
            'C'#243'd. representante'
            'N'#186' Pedido'
            'Condi'#231#227'o de pagto')
          TabOrder = 0
          OnClick = RGNF_BClick
        end
      end
    end
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 1241
      Height = 182
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 1
      object Label2: TLabel
        Left = 15
        Top = 2
        Width = 65
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Descri'#231#227'o:'
      end
      object Label9: TLabel
        Left = 332
        Top = 2
        Width = 36
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Topo:'
      end
      object Label8: TLabel
        Left = 411
        Top = 2
        Width = 78
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Topo banda:'
      end
      object Label1: TLabel
        Left = 490
        Top = 2
        Width = 62
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Topo real:'
      end
      object Label3: TLabel
        Left = 574
        Top = 2
        Width = 37
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Altura:'
      end
      object Label4: TLabel
        Left = 650
        Top = 2
        Width = 79
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Margem esq.'
      end
      object Label5: TLabel
        Left = 731
        Top = 2
        Width = 75
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Largura [F4]:'
      end
      object Label12: TLabel
        Left = 810
        Top = 2
        Width = 72
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Altura Linha:'
      end
      object EdNome: TdmkEdit
        Left = 15
        Top = 22
        Width = 315
        Height = 26
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Color = clWhite
        MaxLength = 100
        TabOrder = 0
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdTopoReal: TdmkEdit
        Left = 335
        Top = 22
        Width = 74
        Height = 26
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        TabOrder = 1
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = EdTopoRealChange
        OnExit = EdTopoRealExit
      end
      object EdTopoBanda: TdmkEdit
        Left = 414
        Top = 22
        Width = 73
        Height = 26
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        TabStop = False
        Alignment = taRightJustify
        Color = clBtnFace
        ReadOnly = True
        TabOrder = 2
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = EdTopoBandaChange
      end
      object EdTopo: TdmkEdit
        Left = 492
        Top = 22
        Width = 74
        Height = 26
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        TabStop = False
        Alignment = taRightJustify
        Color = clBtnFace
        ReadOnly = True
        TabOrder = 3
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdAltu: TdmkEdit
        Left = 571
        Top = 22
        Width = 74
        Height = 26
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        TabOrder = 4
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdMEsq: TdmkEdit
        Left = 650
        Top = 22
        Width = 74
        Height = 26
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        TabOrder = 5
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdLarg: TdmkEdit
        Left = 729
        Top = 22
        Width = 73
        Height = 26
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        TabOrder = 6
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnKeyDown = EdLargKeyDown
      end
      object EdAlt_Linha: TdmkEdit
        Left = 807
        Top = 22
        Width = 74
        Height = 26
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        TabOrder = 7
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object GroupBox1: TGroupBox
        Left = 15
        Top = 49
        Width = 405
        Height = 46
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = ' Formata'#231#227'o: '
        TabOrder = 8
        object Label6: TLabel
          Left = 249
          Top = 21
          Width = 112
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Tamanho da fonte:'
        end
        object CkNegr: TCheckBox
          Left = 12
          Top = 20
          Width = 73
          Height = 21
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Negrito'
          TabOrder = 0
        end
        object CkItal: TCheckBox
          Left = 86
          Top = 20
          Width = 63
          Height = 21
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'It'#225'lico'
          TabOrder = 1
        end
        object CkSubl: TCheckBox
          Left = 153
          Top = 20
          Width = 93
          Height = 21
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Sublinhado'
          TabOrder = 2
        end
        object EdFont: TdmkEdit
          Left = 364
          Top = 16
          Width = 31
          Height = 26
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Alignment = taRightJustify
          TabOrder = 3
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '8'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 8
          ValWarn = False
        end
      end
      object RGAlign1: TRadioGroup
        Left = 420
        Top = 49
        Width = 262
        Height = 46
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = ' Alinhamento do texto: '
        Columns = 3
        ItemIndex = 0
        Items.Strings = (
          'Esquerda'
          'Centro'
          'Direita')
        TabOrder = 9
      end
      object RGSet_T_DOS: TRadioGroup
        Left = 886
        Top = 10
        Width = 139
        Height = 85
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = ' Fonte imp. matricial: '
        ItemIndex = 0
        Items.Strings = (
          'Comprimido'
          'Normal'
          'Expandido')
        TabOrder = 10
      end
      object CkNulo: TCheckBox
        Left = 1029
        Top = 21
        Width = 198
        Height = 23
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Nulo quando valor = zero'
        TabOrder = 11
      end
      object CkSubstitui: TCheckBox
        Left = 1029
        Top = 46
        Width = 198
        Height = 23
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Substitui at'#233' a pen'#250'lt. p'#225'gina:'
        TabOrder = 12
      end
      object EdSubstituicao: TdmkEdit
        Left = 1029
        Top = 69
        Width = 198
        Height = 26
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        MaxLength = 255
        TabOrder = 13
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object RGTabela: TRadioGroup
        Left = 15
        Top = 97
        Width = 474
        Height = 85
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = ' Tabela relacionada: '
        Columns = 4
        ItemIndex = 0
        Items.Strings = (
          'NF Formato A'
          'Entidades'
          'Vendas'
          'Servi'#231'os'
          'Faturam.'
          'Transport.'
          'Duplicata'
          'NF Formato B'
          'Lista de NCMs'
          'NF Formato C')
        TabOrder = 14
      end
      object RGFormato: TRadioGroup
        Left = 492
        Top = 97
        Width = 735
        Height = 85
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = ' Formata'#231#227'o: '
        Columns = 5
        ItemIndex = 0
        Items.Strings = (
          'Texto'
          'Inteiro'
          'N'#250'mero 0,0'
          'N'#250'mero 0,00'
          'N'#250'mero 0,000'
          'N'#250'mero 0,0000'
          'Data "dd/mm/aa"'
          'Data "dd/mm/aaaa"'
          'Hora "hh:nn"'
          'Hora "hh:nn:ss"')
        TabOrder = 15
      end
      object RGAlign2: TRadioGroup
        Left = 665
        Top = 57
        Width = 217
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Columns = 3
        ItemIndex = 0
        Items.Strings = (
          'Topo'
          'Centro'
          'Abaixo')
        TabOrder = 16
      end
    end
    object Panel1: TPanel
      Left = 0
      Top = 182
      Width = 1241
      Height = 31
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 2
      object Label7: TLabel
        Left = 10
        Top = 10
        Width = 84
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Texto padr'#227'o:'
      end
      object Label10: TLabel
        Left = 345
        Top = 10
        Width = 44
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Prefixo:'
      end
      object Label11: TLabel
        Left = 645
        Top = 10
        Width = 39
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Sufixo:'
      end
      object EdPadrao: TdmkEdit
        Left = 98
        Top = 2
        Width = 238
        Height = 26
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        MaxLength = 255
        TabOrder = 0
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdPrefixo: TdmkEdit
        Left = 389
        Top = 2
        Width = 237
        Height = 26
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        MaxLength = 255
        TabOrder = 1
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdSufixo: TdmkEdit
        Left = 689
        Top = 2
        Width = 238
        Height = 26
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        MaxLength = 255
        TabOrder = 2
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1241
    Height = 59
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 1182
      Top = 0
      Width = 59
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 10
        Top = 14
        Width = 39
        Height = 39
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 59
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 59
      Top = 0
      Width = 1123
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 9
        Top = 11
        Width = 474
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Edi'#231#227'o de Campos de Impress'#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 11
        Top = 14
        Width = 474
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Edi'#231#227'o de Campos de Impress'#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 10
        Top = 12
        Width = 474
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Edi'#231#227'o de Campos de Impress'#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 590
    Width = 1241
    Height = 54
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 18
      Width = 1237
      Height = 34
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 16
        Top = 2
        Width = 467
        Height = 19
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Antes de confirmar selecione a orelha correta do item desejado!'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -17
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 15
        Top = 1
        Width = 467
        Height = 19
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Antes de confirmar selecione a orelha correta do item desejado!'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -17
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 644
    Width = 1241
    Height = 86
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 1062
      Top = 18
      Width = 177
      Height = 66
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 15
        Top = 4
        Width = 147
        Height = 49
        Cursor = crHandPoint
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
      end
    end
    object Panel5: TPanel
      Left = 2
      Top = 18
      Width = 1060
      Height = 66
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 15
        Top = 4
        Width = 147
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
    end
  end
  object QrInvalidoM: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM imprimeview'
      'WHERE Codigo=:P0'
      'AND Band=:P1'
      'AND Pos_Topo=:P2'
      'AND Pos_Mesq < :P3'
      'AND (Pos_Mesq+Pos_Larg)>=:P4'
      'AND Controle<>:P5')
    Left = 508
    Top = 328
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P4'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P5'
        ParamType = ptUnknown
      end>
    object QrInvalidoMCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrInvalidoMControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrInvalidoMBand: TIntegerField
      FieldName = 'Band'
    end
    object QrInvalidoMNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QrInvalidoMTab_Tipo: TIntegerField
      FieldName = 'Tab_Tipo'
    end
    object QrInvalidoMTab_Campo: TIntegerField
      FieldName = 'Tab_Campo'
    end
    object QrInvalidoMPadrao: TWideStringField
      FieldName = 'Padrao'
      Size = 255
    end
    object QrInvalidoMPos_Topo: TIntegerField
      FieldName = 'Pos_Topo'
    end
    object QrInvalidoMPos_Altu: TIntegerField
      FieldName = 'Pos_Altu'
    end
    object QrInvalidoMPos_Larg: TIntegerField
      FieldName = 'Pos_Larg'
    end
    object QrInvalidoMPos_MEsq: TIntegerField
      FieldName = 'Pos_MEsq'
    end
    object QrInvalidoMSet_TAli: TIntegerField
      FieldName = 'Set_TAli'
    end
    object QrInvalidoMSet_BAli: TIntegerField
      FieldName = 'Set_BAli'
    end
    object QrInvalidoMSet_Negr: TIntegerField
      FieldName = 'Set_Negr'
    end
    object QrInvalidoMSet_Ital: TIntegerField
      FieldName = 'Set_Ital'
    end
    object QrInvalidoMSet_Unde: TIntegerField
      FieldName = 'Set_Unde'
    end
    object QrInvalidoMSet_Tam: TIntegerField
      FieldName = 'Set_Tam'
    end
    object QrInvalidoMFormato: TIntegerField
      FieldName = 'Formato'
    end
    object QrInvalidoMLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrInvalidoMDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrInvalidoMDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrInvalidoMUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrInvalidoMUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrInvalidoMPrefixo: TWideStringField
      FieldName = 'Prefixo'
      Size = 255
    end
    object QrInvalidoMSufixo: TWideStringField
      FieldName = 'Sufixo'
      Size = 255
    end
    object QrInvalidoMSubstitui: TSmallintField
      FieldName = 'Substitui'
    end
    object QrInvalidoMSubstituicao: TWideStringField
      FieldName = 'Substituicao'
      Size = 255
    end
    object QrInvalidoMSet_T_DOS: TSmallintField
      FieldName = 'Set_T_DOS'
    end
    object QrInvalidoMAlt_Linha: TIntegerField
      FieldName = 'Alt_Linha'
    end
    object QrInvalidoMNulo: TSmallintField
      FieldName = 'Nulo'
    end
  end
  object QrInvalidoL: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM imprimeview'
      'WHERE Codigo=:P0'
      'AND Band=:P1'
      'AND Pos_Topo=:P2'
      'AND Pos_Mesq >= :P3'
      'AND Pos_Mesq <= :P4'
      'AND Controle<>:P5'
      ' ')
    Left = 536
    Top = 328
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P4'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P5'
        ParamType = ptUnknown
      end>
    object QrInvalidoLCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrInvalidoLControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrInvalidoLBand: TIntegerField
      FieldName = 'Band'
    end
    object QrInvalidoLNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QrInvalidoLTab_Tipo: TIntegerField
      FieldName = 'Tab_Tipo'
    end
    object QrInvalidoLTab_Campo: TIntegerField
      FieldName = 'Tab_Campo'
    end
    object QrInvalidoLPadrao: TWideStringField
      FieldName = 'Padrao'
      Size = 255
    end
    object QrInvalidoLPos_Topo: TIntegerField
      FieldName = 'Pos_Topo'
    end
    object QrInvalidoLPos_Altu: TIntegerField
      FieldName = 'Pos_Altu'
    end
    object QrInvalidoLPos_Larg: TIntegerField
      FieldName = 'Pos_Larg'
    end
    object QrInvalidoLPos_MEsq: TIntegerField
      FieldName = 'Pos_MEsq'
    end
    object QrInvalidoLSet_TAli: TIntegerField
      FieldName = 'Set_TAli'
    end
    object QrInvalidoLSet_BAli: TIntegerField
      FieldName = 'Set_BAli'
    end
    object QrInvalidoLSet_Negr: TIntegerField
      FieldName = 'Set_Negr'
    end
    object QrInvalidoLSet_Ital: TIntegerField
      FieldName = 'Set_Ital'
    end
    object QrInvalidoLSet_Unde: TIntegerField
      FieldName = 'Set_Unde'
    end
    object QrInvalidoLSet_Tam: TIntegerField
      FieldName = 'Set_Tam'
    end
    object QrInvalidoLFormato: TIntegerField
      FieldName = 'Formato'
    end
    object QrInvalidoLLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrInvalidoLDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrInvalidoLDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrInvalidoLUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrInvalidoLUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrInvalidoLPrefixo: TWideStringField
      FieldName = 'Prefixo'
      Size = 255
    end
    object QrInvalidoLSufixo: TWideStringField
      FieldName = 'Sufixo'
      Size = 255
    end
    object QrInvalidoLSubstitui: TSmallintField
      FieldName = 'Substitui'
    end
    object QrInvalidoLSubstituicao: TWideStringField
      FieldName = 'Substituicao'
      Size = 255
    end
    object QrInvalidoLSet_T_DOS: TSmallintField
      FieldName = 'Set_T_DOS'
    end
    object QrInvalidoLAlt_Linha: TIntegerField
      FieldName = 'Alt_Linha'
    end
    object QrInvalidoLNulo: TSmallintField
      FieldName = 'Nulo'
    end
  end
end
