object FmEmiteCheque_4: TFmEmiteCheque_4
  Left = 339
  Top = 185
  Caption = 'CHS-PRINT-004 :: Impress'#227'o de Cheques na LX 300'
  ClientHeight = 661
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PageControl1: TPageControl
    Left = 0
    Top = 102
    Width = 1008
    Height = 489
    ActivePage = TabSheet1
    Align = alClient
    TabOrder = 0
    OnChange = PageControl1Change
    object TabSheet1: TTabSheet
      Caption = ' Gera'#231#227'o '
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object Panel1: TPanel
        Left = 0
        Top = 0
        Width = 1000
        Height = 461
        Align = alClient
        ParentBackground = False
        TabOrder = 0
        object Panel10: TPanel
          Left = 1
          Top = 1
          Width = 998
          Height = 168
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 0
          object Label8: TLabel
            Left = 12
            Top = 4
            Width = 131
            Height = 13
            Caption = 'Configura'#231#227'o de impress'#227'o:'
          end
          object SpeedButton1: TSpeedButton
            Left = 600
            Top = 20
            Width = 21
            Height = 21
            Caption = '...'
            OnClick = SpeedButton1Click
          end
          object LaCidade: TLabel
            Left = 624
            Top = 4
            Width = 36
            Height = 13
            Caption = 'Cidade:'
          end
          object CBRegerar: TGroupBox
            Left = 456
            Top = 50
            Width = 249
            Height = 91
            Caption = '       '
            TabOrder = 7
            object Label6: TLabel
              Left = 22
              Top = 40
              Width = 27
              Height = 13
              Caption = 'S'#233'rie:'
            end
            object LaDoc: TLabel
              Left = 123
              Top = 40
              Width = 44
              Height = 13
              Caption = 'N'#186' inicial:'
            end
            object EdSerieCH: TdmkEdit
              Left = 21
              Top = 56
              Width = 78
              Height = 21
              CharCase = ecUpperCase
              TabOrder = 0
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object EdDocumento: TdmkEdit
              Left = 121
              Top = 56
              Width = 92
              Height = 21
              Alignment = taRightJustify
              TabOrder = 1
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
            end
          end
          object EdChConfig: TdmkEditCB
            Left = 12
            Top = 20
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            OnChange = EdChConfigChange
            DBLookupComboBox = CBChConfig
            IgnoraDBLookupComboBox = False
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
          object CBChConfig: TdmkDBLookupComboBox
            Left = 68
            Top = 20
            Width = 532
            Height = 21
            KeyField = 'Codigo'
            ListField = 'Nome'
            ListSource = DsChConfigCab
            TabOrder = 1
            dmkEditCB = EdChConfig
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
          object EdCidade: TdmkEdit
            Left = 624
            Top = 20
            Width = 180
            Height = 21
            TabOrder = 2
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object RGLct: TRadioGroup
            Left = 12
            Top = 50
            Width = 141
            Height = 113
            Caption = ' Lan'#231'amentos: '
            ItemIndex = 0
            Items.Strings = (
              'Nenhum'
              'Selecionados'
              'Indefinidos')
            TabOrder = 3
            OnClick = RGLctClick
          end
          object RGData: TRadioGroup
            Left = 156
            Top = 50
            Width = 161
            Height = 113
            Caption = ' Data de emiss'#227'o do cheque: '
            ItemIndex = 1
            Items.Strings = (
              'Do lan'#231'amento'
              'Do vencimento'
              'O dia de hoje')
            TabOrder = 4
          end
          object RGBomPara: TRadioGroup
            Left = 320
            Top = 50
            Width = 133
            Height = 113
            Caption = ' Usar "BOM PARA": '
            ItemIndex = 1
            Items.Strings = (
              'Em nenhum cheque'
              'Nos pr'#233'-datados'
              'Em todos cheques')
            TabOrder = 5
          end
          object CkReGerar: TCheckBox
            Left = 468
            Top = 46
            Width = 217
            Height = 17
            Caption = ' Gerar / Regerar n'#250'mero do(s) cheque(s): '
            Checked = True
            State = cbChecked
            TabOrder = 6
            OnClick = CkReGerarClick
          end
          object GroupBox1: TGroupBox
            Left = 708
            Top = 50
            Width = 97
            Height = 113
            Caption = ' Impressora: '
            TabOrder = 8
            object Label1: TLabel
              Left = 10
              Top = 40
              Width = 28
              Height = 13
              Caption = 'Porta:'
            end
            object EdPorta: TdmkEdit
              Left = 9
              Top = 56
              Width = 80
              Height = 21
              CharCase = ecUpperCase
              TabOrder = 0
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = 'LPT1:'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 'LPT1:'
              ValWarn = False
            end
          end
          object CkNominal: TCheckBox
            Left = 468
            Top = 146
            Width = 61
            Height = 17
            Caption = 'Nominal'
            Checked = True
            State = cbChecked
            TabOrder = 9
          end
          object CkForca: TCheckBox
            Left = 530
            Top = 146
            Width = 175
            Height = 17
            Caption = 'For'#231'a sele'#231#227'o da tabela BRASCII'
            Checked = True
            State = cbChecked
            TabOrder = 10
          end
          object RGLctsCfg: TGroupBox
            Left = 809
            Top = 4
            Width = 180
            Height = 159
            TabOrder = 11
            Visible = False
            object Label9: TLabel
              Left = 3
              Top = 107
              Width = 55
              Height = 13
              Caption = 'Data inicial:'
            end
            object Label10: TLabel
              Left = 10
              Top = 136
              Width = 48
              Height = 13
              Caption = 'Data final:'
            end
            object RGTipoData: TRadioGroup
              Left = 10
              Top = 19
              Width = 111
              Height = 77
              Caption = ' Per'#237'odo: '
              Items.Strings = (
                'Emiss'#227'o'
                'Vencimento'
                'Compensado')
              TabOrder = 0
            end
            object TPDataIni: TdmkEditDateTimePicker
              Left = 63
              Top = 106
              Width = 106
              Height = 21
              Date = 39328.122587314800000000
              Time = 39328.122587314800000000
              TabOrder = 1
              ReadOnly = False
              DefaultEditMask = '!99/99/99;1;_'
              AutoApplyEditMask = True
              UpdType = utYes
              DatePurpose = dmkdpNone
            end
            object TPDataFim: TdmkEditDateTimePicker
              Left = 63
              Top = 132
              Width = 106
              Height = 21
              Date = 39328.122587314800000000
              Time = 39328.122587314800000000
              TabOrder = 2
              ReadOnly = False
              DefaultEditMask = '!99/99/99;1;_'
              AutoApplyEditMask = True
              UpdType = utYes
              DatePurpose = dmkdpNone
            end
            object CkFiltroLct: TCheckBox
              Left = 10
              Top = 0
              Width = 110
              Height = 17
              Caption = 'Filtrar lan'#231'amentos'
              Checked = True
              State = cbChecked
              TabOrder = 3
              Visible = False
              OnClick = CkFiltroLctClick
            end
            object BtRefresh: TBitBtn
              Tag = 18
              Left = 129
              Top = 19
              Width = 40
              Height = 40
              Cursor = crHandPoint
              NumGlyphs = 2
              TabOrder = 4
              OnClick = BtRefreshClick
            end
          end
        end
        object PCLct: TPageControl
          Left = 1
          Top = 169
          Width = 998
          Height = 291
          ActivePage = TabSheet6
          Align = alClient
          TabOrder = 1
          Visible = False
          object TabSheet6: TTabSheet
            Caption = 'Lan'#231'ametos'
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object DBGLct: TdmkDBGridDAC
              Left = 0
              Top = 0
              Width = 990
              Height = 263
              Align = alClient
              Columns = <
                item
                  Expanded = False
                  FieldName = 'Ativo'
                  Title.Caption = 'ok'
                  Width = 17
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Data'
                  Width = 56
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Genero'
                  Title.Caption = 'Conta'
                  Width = 32
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NO_CONTA'
                  Title.Caption = 'Descri'#231#227'o da Conta do Plano de contas'
                  Width = 200
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Descricao'
                  Title.Caption = 'Hist'#243'rico do lan'#231'amento'
                  Width = 140
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Debito'
                  Title.Caption = 'D'#233'bito'
                  Width = 68
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Vencimento'
                  Title.Caption = 'Vencto'
                  Width = 56
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NO_ENT'
                  Title.Caption = 'Fornecedor'
                  Width = 140
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Controle'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'AGRUPADO_TXT'
                  Title.Caption = 'Agrupado'
                  Visible = True
                end>
              Color = clWindow
              DataSource = DsChequesLct
              Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
              TabOrder = 0
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              OnCellClick = DBGLctCellClick
              EditForceNextYear = False
              Columns = <
                item
                  Expanded = False
                  FieldName = 'Ativo'
                  Title.Caption = 'ok'
                  Width = 17
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Data'
                  Width = 56
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Genero'
                  Title.Caption = 'Conta'
                  Width = 32
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NO_CONTA'
                  Title.Caption = 'Descri'#231#227'o da Conta do Plano de contas'
                  Width = 200
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Descricao'
                  Title.Caption = 'Hist'#243'rico do lan'#231'amento'
                  Width = 140
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Debito'
                  Title.Caption = 'D'#233'bito'
                  Width = 68
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Vencimento'
                  Title.Caption = 'Vencto'
                  Width = 56
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NO_ENT'
                  Title.Caption = 'Fornecedor'
                  Width = 140
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Controle'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'AGRUPADO_TXT'
                  Title.Caption = 'Agrupado'
                  Visible = True
                end>
            end
          end
          object TabSheet7: TTabSheet
            Caption = 'Todos Lan'#231'amentos Inclusive agrupados'
            ImageIndex = 1
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object DBGLct2: TdmkDBGridDAC
              Left = 0
              Top = 0
              Width = 990
              Height = 263
              Align = alClient
              Columns = <
                item
                  Expanded = False
                  FieldName = 'Ativo'
                  Title.Caption = 'ok'
                  Width = 17
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Data'
                  Width = 56
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Genero'
                  Title.Caption = 'Conta'
                  Width = 32
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NO_CONTA'
                  Title.Caption = 'Descri'#231#227'o da Conta do Plano de contas'
                  Width = 200
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Descricao'
                  Title.Caption = 'Hist'#243'rico do lan'#231'amento'
                  Width = 140
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Debito'
                  Title.Caption = 'D'#233'bito'
                  Width = 68
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Vencimento'
                  Title.Caption = 'Vencto'
                  Width = 56
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NO_ENT'
                  Title.Caption = 'Fornecedor'
                  Width = 140
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Controle'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'AGRUPADO_TXT'
                  Title.Caption = 'Agrupado'
                  Visible = True
                end>
              Color = clWindow
              DataSource = DsChequesLct2
              Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
              TabOrder = 0
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              EditForceNextYear = False
              Columns = <
                item
                  Expanded = False
                  FieldName = 'Ativo'
                  Title.Caption = 'ok'
                  Width = 17
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Data'
                  Width = 56
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Genero'
                  Title.Caption = 'Conta'
                  Width = 32
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NO_CONTA'
                  Title.Caption = 'Descri'#231#227'o da Conta do Plano de contas'
                  Width = 200
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Descricao'
                  Title.Caption = 'Hist'#243'rico do lan'#231'amento'
                  Width = 140
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Debito'
                  Title.Caption = 'D'#233'bito'
                  Width = 68
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Vencimento'
                  Title.Caption = 'Vencto'
                  Width = 56
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NO_ENT'
                  Title.Caption = 'Fornecedor'
                  Width = 140
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Controle'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'AGRUPADO_TXT'
                  Title.Caption = 'Agrupado'
                  Visible = True
                end>
            end
          end
        end
      end
    end
    object TabSheet2: TTabSheet
      Caption = ' Impress'#227'o '
      ImageIndex = 1
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object Panel5: TPanel
        Left = 0
        Top = 0
        Width = 1000
        Height = 461
        Align = alClient
        ParentBackground = False
        TabOrder = 0
        object DBGrid1: TDBGrid
          Left = 1
          Top = 1
          Width = 998
          Height = 368
          Align = alClient
          DataSource = DsChequesImp
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'Seq'
              Width = 24
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ChSerie'
              Title.Caption = 'S'#233'rie Ch.'
              Width = 48
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ChNumero'
              Title.Caption = 'N'#186' Cheque'
              Width = 60
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Favorecido'
              Width = 140
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Valor'
              Width = 80
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Dia'
              Width = 24
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Mes'
              Title.Caption = 'M'#234's'
              Width = 80
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Ano'
              Width = 32
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Cidade'
              Width = 90
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'BomPara'
              Title.Caption = 'Bom para'
              Width = 62
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Observacao'
              Title.Caption = 'Observa'#231#227'o'
              Width = 400
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Controle'
              Title.Caption = 'ID'
              Visible = True
            end>
        end
        object Panel6: TPanel
          Left = 1
          Top = 369
          Width = 998
          Height = 43
          Align = alBottom
          BevelOuter = bvNone
          TabOrder = 1
          object DBEdit2: TDBEdit
            Left = 0
            Top = 20
            Width = 813
            Height = 22
            DataField = 'Extenso2'
            DataSource = DsChequesImp
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Courier New'
            Font.Style = []
            ParentFont = False
            TabOrder = 0
          end
          object DBEdit1: TDBEdit
            Left = 0
            Top = 0
            Width = 813
            Height = 22
            DataField = 'Extenso1'
            DataSource = DsChequesImp
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Courier New'
            Font.Style = []
            ParentFont = False
            TabOrder = 1
          end
        end
        object Panel3: TPanel
          Left = 1
          Top = 412
          Width = 998
          Height = 48
          Align = alBottom
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 2
          object BitBtn1: TBitBtn
            Tag = 5
            Left = 11
            Top = 4
            Width = 120
            Height = 40
            Caption = '&Imprime'
            NumGlyphs = 2
            TabOrder = 0
            OnClick = BitBtn1Click
          end
          object Panel4: TPanel
            Left = 857
            Top = 0
            Width = 141
            Height = 48
            Align = alRight
            BevelOuter = bvNone
            TabOrder = 1
            object BitBtn2: TBitBtn
              Tag = 13
              Left = 12
              Top = 4
              Width = 120
              Height = 40
              Cursor = crHandPoint
              Caption = '&Sa'#237'da'
              NumGlyphs = 2
              ParentShowHint = False
              ShowHint = True
              TabOrder = 0
              OnClick = BtSaidaClick
            end
          end
          object BitBtn3: TBitBtn
            Tag = 11
            Left = 262
            Top = 4
            Width = 120
            Height = 40
            Caption = '&Observa'#231#227'o'
            NumGlyphs = 2
            TabOrder = 2
            OnClick = BitBtn3Click
          end
          object BtCopiaDoc: TBitBtn
            Tag = 10019
            Left = 137
            Top = 4
            Width = 120
            Height = 40
            Caption = '&C'#243'pia cheque'
            NumGlyphs = 2
            TabOrder = 3
            OnClick = BtCopiaDocClick
          end
        end
      end
    end
    object TabSheet3: TTabSheet
      Caption = ' Testes '
      ImageIndex = 2
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object Panel7: TPanel
        Left = 0
        Top = 0
        Width = 105
        Height = 461
        Align = alLeft
        BevelOuter = bvNone
        ParentBackground = False
        TabOrder = 0
        object Label2: TLabel
          Left = 44
          Top = 168
          Width = 15
          Height = 13
          Caption = 'at'#233
        end
        object Label3: TLabel
          Left = 8
          Top = 148
          Width = 54
          Height = 13
          Caption = 'Caracteres:'
        end
        object Label4: TLabel
          Left = 20
          Top = 40
          Width = 43
          Height = 13
          Caption = 'Reverso:'
        end
        object Button1: TButton
          Left = 12
          Top = 120
          Width = 75
          Height = 25
          Caption = 'Caracteres'
          TabOrder = 0
          OnClick = Button1Click
        end
        object Button2: TButton
          Left = 12
          Top = 12
          Width = 75
          Height = 25
          Caption = 'Margens'
          TabOrder = 1
          OnClick = Button2Click
        end
        object EdCarIni: TdmkEdit
          Left = 8
          Top = 164
          Width = 33
          Height = 21
          Alignment = taRightJustify
          TabOrder = 2
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '33'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 33
          ValWarn = False
        end
        object EdCarFim: TdmkEdit
          Left = 64
          Top = 164
          Width = 33
          Height = 21
          Alignment = taRightJustify
          TabOrder = 3
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '126'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 126
          ValWarn = False
        end
        object EdReverso: TdmkEdit
          Left = 20
          Top = 56
          Width = 61
          Height = 21
          Alignment = taRightJustify
          TabOrder = 4
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '0'
          ValMax = '127'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
      end
      object Panel8: TPanel
        Left = 105
        Top = 0
        Width = 895
        Height = 461
        Align = alClient
        BevelOuter = bvNone
        ParentBackground = False
        TabOrder = 1
        object Panel9: TPanel
          Left = 0
          Top = 0
          Width = 895
          Height = 41
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 0
          object Label5: TLabel
            Left = 8
            Top = 12
            Width = 41
            Height = 13
            Caption = 'Colunas:'
          end
          object Label7: TLabel
            Left = 108
            Top = 12
            Width = 34
            Height = 13
            Caption = 'Linhas:'
          end
          object EdCol: TdmkEdit
            Left = 52
            Top = 8
            Width = 45
            Height = 21
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '10'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 10
            ValWarn = False
          end
          object EdLin: TdmkEdit
            Left = 148
            Top = 8
            Width = 45
            Height = 21
            Alignment = taRightJustify
            TabOrder = 1
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '6'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 6
            ValWarn = False
          end
          object Button3: TButton
            Left = 212
            Top = 4
            Width = 75
            Height = 25
            Caption = 'Executa'
            TabOrder = 2
            OnClick = Button3Click
          end
        end
        object PageControl2: TPageControl
          Left = 0
          Top = 41
          Width = 895
          Height = 420
          ActivePage = TabSheet4
          Align = alClient
          TabOrder = 1
          object TabSheet4: TTabSheet
            Caption = ' Execu'#231#227'o de comandos '
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object Grade: TStringGrid
              Left = 0
              Top = 0
              Width = 887
              Height = 392
              Align = alClient
              ColCount = 10
              DefaultColWidth = 32
              DefaultRowHeight = 18
              RowCount = 6
              Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goEditing]
              TabOrder = 0
            end
          end
        end
      end
    end
    object TabSheet5: TTabSheet
      Caption = ' Avan'#231'o carro impressora '
      ImageIndex = 3
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object Memo1: TMemo
        Left = 0
        Top = 0
        Width = 1000
        Height = 461
        Align = alClient
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Courier New'
        Font.Style = []
        Lines.Strings = (
          '')
        ParentFont = False
        TabOrder = 0
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 912
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 413
        Height = 32
        Caption = 'Impress'#227'o de Cheques na LX 300'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 413
        Height = 32
        Caption = 'Impress'#227'o de Cheques na LX 300'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 413
        Height = 32
        Caption = 'Impress'#227'o de Cheques na LX 300'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 48
    Width = 1008
    Height = 54
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel11: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 37
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object PB1: TProgressBar
        Left = 0
        Top = 20
        Width = 1004
        Height = 17
        Align = alBottom
        TabOrder = 0
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 591
    Width = 1008
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 862
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel13: TPanel
      Left = 2
      Top = 15
      Width = 860
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object Panel12: TPanel
        Left = 0
        Top = 0
        Width = 138
        Height = 53
        Align = alLeft
        BevelOuter = bvNone
        Caption = 'PnGera'
        TabOrder = 0
        object BtGera: TBitBtn
          Tag = 163
          Left = 14
          Top = 3
          Width = 120
          Height = 40
          Caption = '&Gera'
          Enabled = False
          NumGlyphs = 2
          TabOrder = 0
          OnClick = BtGeraClick
        end
      end
      object PnDefAll: TPanel
        Left = 138
        Top = 0
        Width = 603
        Height = 53
        Align = alLeft
        BevelOuter = bvNone
        TabOrder = 1
        object BtAgrupar: TBitBtn
          Left = 2
          Top = 3
          Width = 120
          Height = 40
          Caption = '&Agrupar'
          Enabled = False
          NumGlyphs = 2
          TabOrder = 0
          OnClick = BtAgruparClick
        end
        object BtDesagrupar: TBitBtn
          Left = 127
          Top = 3
          Width = 120
          Height = 40
          Caption = '&Desagrupar'
          Enabled = False
          NumGlyphs = 2
          TabOrder = 1
          OnClick = BtDesagruparClick
        end
        object BtTodos: TBitBtn
          Tag = 127
          Left = 252
          Top = 3
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Hint = 'Sai da janela atual'
          Caption = '&Todos'
          Enabled = False
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = BtTodosClick
        end
        object BtNenhum: TBitBtn
          Tag = 128
          Left = 376
          Top = 3
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Hint = 'Sai da janela atual'
          Caption = '&Nenhum'
          Enabled = False
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = BtNenhumClick
        end
      end
    end
  end
  object QrChConfigCab: TMySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrChConfigCabAfterOpen
    SQL.Strings = (
      'SELECT * FROM chconfcab'
      'ORDER BY Nome')
    Left = 168
    Top = 452
    object QrChConfigCabCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrChConfigCabNome: TWideStringField
      FieldName = 'Nome'
      Size = 255
    end
    object QrChConfigCabAltura: TIntegerField
      FieldName = 'Altura'
    end
    object QrChConfigCabTopoIni: TIntegerField
      FieldName = 'TopoIni'
    end
    object QrChConfigCabCPI: TSmallintField
      FieldName = 'CPI'
    end
    object QrChConfigCabMEsq: TSmallintField
      FieldName = 'MEsq'
    end
  end
  object DsChConfigCab: TDataSource
    DataSet = QrChConfigCab
    Left = 196
    Top = 452
  end
  object QrIni: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM chconfval'
      'WHERE Codigo=:P0')
    Left = 452
    Top = 444
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrIniCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrIniCampo: TIntegerField
      FieldName = 'Campo'
    end
    object QrIniTopo: TIntegerField
      FieldName = 'Topo'
    end
    object QrIniMEsq: TIntegerField
      FieldName = 'MEsq'
    end
    object QrIniLarg: TIntegerField
      FieldName = 'Larg'
    end
    object QrIniFTam: TIntegerField
      FieldName = 'FTam'
    end
    object QrIniNegr: TIntegerField
      FieldName = 'Negr'
    end
    object QrIniItal: TIntegerField
      FieldName = 'Ital'
    end
    object QrIniLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrIniDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrIniDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrIniUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrIniUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrIniAltu: TIntegerField
      FieldName = 'Altu'
    end
    object QrIniPref: TWideStringField
      FieldName = 'Pref'
      Size = 255
    end
    object QrIniSufi: TWideStringField
      FieldName = 'Sufi'
      Size = 255
    end
    object QrIniPadr: TWideStringField
      FieldName = 'Padr'
      Size = 255
    end
    object QrIniAliV: TIntegerField
      FieldName = 'AliV'
    end
    object QrIniAliH: TIntegerField
      FieldName = 'AliH'
    end
    object QrIniTopR: TIntegerField
      FieldName = 'TopR'
    end
  end
  object QrChConfVal: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM chconfval'
      'WHERE Codigo=:P0'
      'AND Campo in (2,3)')
    Left = 224
    Top = 452
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrChConfValCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrChConfValCampo: TIntegerField
      FieldName = 'Campo'
    end
    object QrChConfValTopo: TIntegerField
      FieldName = 'Topo'
    end
    object QrChConfValMEsq: TIntegerField
      FieldName = 'MEsq'
    end
    object QrChConfValLarg: TIntegerField
      FieldName = 'Larg'
    end
    object QrChConfValFTam: TIntegerField
      FieldName = 'FTam'
    end
    object QrChConfValNegr: TIntegerField
      FieldName = 'Negr'
    end
    object QrChConfValItal: TIntegerField
      FieldName = 'Ital'
    end
    object QrChConfValLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrChConfValDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrChConfValDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrChConfValUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrChConfValUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrChConfValAltu: TIntegerField
      FieldName = 'Altu'
    end
    object QrChConfValPref: TWideStringField
      FieldName = 'Pref'
      Size = 255
    end
    object QrChConfValSufi: TWideStringField
      FieldName = 'Sufi'
      Size = 255
    end
    object QrChConfValPadr: TWideStringField
      FieldName = 'Padr'
      Size = 255
    end
    object QrChConfValAliV: TIntegerField
      FieldName = 'AliV'
    end
    object QrChConfValAliH: TIntegerField
      FieldName = 'AliH'
    end
    object QrChConfValTopR: TIntegerField
      FieldName = 'TopR'
    end
  end
  object QrChequesImp: TMySQLQuery
   
    SQL.Strings = (
      'SELECT * '
      'FROM chequesimp'
      'ORDER BY Seq')
    Left = 484
    Top = 444
    object QrChequesImpValor: TWideStringField
      FieldName = 'Valor'
      Size = 255
    end
    object QrChequesImpExtenso1: TWideStringField
      FieldName = 'Extenso1'
      Size = 255
    end
    object QrChequesImpExtenso2: TWideStringField
      FieldName = 'Extenso2'
      Size = 255
    end
    object QrChequesImpFavorecido: TWideStringField
      FieldName = 'Favorecido'
      Size = 255
    end
    object QrChequesImpCidade: TWideStringField
      FieldName = 'Cidade'
      Size = 255
    end
    object QrChequesImpDia: TWideStringField
      FieldName = 'Dia'
      Size = 255
    end
    object QrChequesImpMes: TWideStringField
      FieldName = 'Mes'
      Size = 255
    end
    object QrChequesImpAno: TWideStringField
      FieldName = 'Ano'
      Size = 255
    end
    object QrChequesImpBomPara: TWideStringField
      FieldName = 'BomPara'
      Size = 255
    end
    object QrChequesImpObservacao: TWideStringField
      FieldName = 'Observacao'
      Size = 255
    end
    object QrChequesImpSeq: TIntegerField
      FieldName = 'Seq'
    end
    object QrChequesImpCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrChequesImpCPI: TIntegerField
      FieldName = 'CPI'
    end
    object QrChequesImpTopoIni: TIntegerField
      FieldName = 'TopoIni'
    end
    object QrChequesImpAltura: TIntegerField
      FieldName = 'Altura'
    end
    object QrChequesImpMEsq: TIntegerField
      FieldName = 'MEsq'
    end
    object QrChequesImpControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrChequesImpSub: TIntegerField
      FieldName = 'Sub'
    end
    object QrChequesImpChNumero: TFloatField
      FieldName = 'ChNumero'
    end
    object QrChequesImpChSerie: TWideStringField
      FieldName = 'ChSerie'
      Size = 10
    end
    object QrChequesImpChGrava: TSmallintField
      FieldName = 'ChGrava'
    end
  end
  object DsChequesImp: TDataSource
    DataSet = QrChequesImp
    Left = 512
    Top = 444
  end
  object QrView1: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *'
      'FROM chconfval'
      'WHERE Codigo=:P0'
      'AND TopR=:P1'
      'ORDER BY MEsq')
    Left = 572
    Top = 444
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrView1Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrView1Campo: TIntegerField
      FieldName = 'Campo'
    end
    object QrView1Topo: TIntegerField
      FieldName = 'Topo'
    end
    object QrView1MEsq: TIntegerField
      FieldName = 'MEsq'
    end
    object QrView1Larg: TIntegerField
      FieldName = 'Larg'
    end
    object QrView1FTam: TIntegerField
      FieldName = 'FTam'
    end
    object QrView1Negr: TIntegerField
      FieldName = 'Negr'
    end
    object QrView1Ital: TIntegerField
      FieldName = 'Ital'
    end
    object QrView1Lk: TIntegerField
      FieldName = 'Lk'
    end
    object QrView1DataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrView1DataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrView1UserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrView1UserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrView1Altu: TIntegerField
      FieldName = 'Altu'
    end
    object QrView1Pref: TWideStringField
      FieldName = 'Pref'
      Size = 255
    end
    object QrView1Sufi: TWideStringField
      FieldName = 'Sufi'
      Size = 255
    end
    object QrView1Padr: TWideStringField
      FieldName = 'Padr'
      Size = 255
    end
    object QrView1AliV: TIntegerField
      FieldName = 'AliV'
    end
    object QrView1AliH: TIntegerField
      FieldName = 'AliH'
    end
    object QrView1TopR: TIntegerField
      FieldName = 'TopR'
    end
    object QrView1PrEs: TIntegerField
      FieldName = 'PrEs'
    end
  end
  object QrTopos1: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT DISTINCT TopR'
      'FROM chconfval'
      'WHERE Codigo=:P0'
      'ORDER BY Topo')
    Left = 544
    Top = 444
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrTopos1TopR: TIntegerField
      FieldName = 'TopR'
    end
  end
  object QrCart: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Tipo, TipoDoc '
      'FROM carteiras'
      'WHERE Codigo=:P0')
    Left = 600
    Top = 444
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrCartTipo: TIntegerField
      FieldName = 'Tipo'
    end
    object QrCartTipoDoc: TSmallintField
      FieldName = 'TipoDoc'
    end
  end
  object QrChequesLct: TMySQLQuery
   
    BeforeClose = QrChequesLctBeforeClose
    AfterScroll = QrChequesLctAfterScroll
    SQL.Strings = (
      'SELECT *'
      'FROM chequeslct'
      'WHERE Agrupar = 0;')
    Left = 636
    Top = 444
    object QrChequesLctAtivo: TSmallintField
      FieldName = 'Ativo'
      MaxValue = 1
    end
    object QrChequesLctData: TDateField
      FieldName = 'Data'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrChequesLctControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrChequesLctGenero: TIntegerField
      FieldName = 'Genero'
    end
    object QrChequesLctNO_CONTA: TWideStringField
      FieldName = 'NO_CONTA'
      Size = 50
    end
    object QrChequesLctDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 100
    end
    object QrChequesLctVencimento: TDateField
      FieldName = 'Vencimento'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrChequesLctFornecedor: TIntegerField
      FieldName = 'Fornecedor'
    end
    object QrChequesLctNO_ENT: TWideStringField
      FieldName = 'NO_ENT'
      Size = 100
    end
    object QrChequesLctSub: TSmallintField
      FieldName = 'Sub'
    end
    object QrChequesLctDocumento: TFloatField
      FieldName = 'Documento'
    end
    object QrChequesLctSerieCH: TWideStringField
      FieldName = 'SerieCH'
      Size = 10
    end
    object QrChequesLctDebito: TFloatField
      FieldName = 'Debito'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrChequesLctAgrupar: TIntegerField
      FieldName = 'Agrupar'
    end
    object QrChequesLctAGRUPADO_TXT: TWideStringField
      FieldName = 'AGRUPADO_TXT'
      Size = 3
    end
  end
  object DsChequesLct: TDataSource
    DataSet = QrChequesLct
    Left = 664
    Top = 444
  end
  object QrLoc: TMySQLQuery
    Database = Dmod.MyDB
    Left = 304
    Top = 336
  end
  object QrChequesLct2: TMySQLQuery
   
    BeforeClose = QrChequesLctBeforeClose
    AfterScroll = QrChequesLctAfterScroll
    SQL.Strings = (
      'SELECT *'
      'FROM chequeslct'
      'WHERE Agrupar = 0;')
    Left = 636
    Top = 472
    object QrChequesLct2Ativo: TSmallintField
      FieldName = 'Ativo'
      MaxValue = 1
    end
    object QrChequesLct2Data: TDateField
      FieldName = 'Data'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrChequesLct2Controle: TIntegerField
      FieldName = 'Controle'
    end
    object QrChequesLct2Genero: TIntegerField
      FieldName = 'Genero'
    end
    object QrChequesLct2NO_CONTA: TWideStringField
      FieldName = 'NO_CONTA'
      Size = 50
    end
    object QrChequesLct2Descricao: TWideStringField
      FieldName = 'Descricao'
      Size = 100
    end
    object QrChequesLct2Vencimento: TDateField
      FieldName = 'Vencimento'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrChequesLct2Fornecedor: TIntegerField
      FieldName = 'Fornecedor'
    end
    object QrChequesLct2NO_ENT: TWideStringField
      FieldName = 'NO_ENT'
      Size = 100
    end
    object QrChequesLct2Sub: TSmallintField
      FieldName = 'Sub'
    end
    object QrChequesLct2Documento: TFloatField
      FieldName = 'Documento'
    end
    object QrChequesLct2SerieCH: TWideStringField
      FieldName = 'SerieCH'
      Size = 10
    end
    object QrChequesLct2Debito: TFloatField
      FieldName = 'Debito'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrChequesLct2Agrupar: TIntegerField
      FieldName = 'Agrupar'
    end
    object QrChequesLct2AGRUPADO_TXT: TWideStringField
      FieldName = 'AGRUPADO_TXT'
      Size = 3
    end
  end
  object DsChequesLct2: TDataSource
    DataSet = QrChequesLct2
    Left = 664
    Top = 472
  end
end
