object FmEmiteCheque_0: TFmEmiteCheque_0
  Left = 367
  Top = 177
  Caption = 'CHS-PRINT-000 :: Emite Cheque Remoto'
  ClientHeight = 445
  ClientWidth = 778
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelDados: TPanel
    Left = 0
    Top = 48
    Width = 778
    Height = 93
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object Panel2: TPanel
      Left = 0
      Top = 0
      Width = 778
      Height = 44
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object Label1: TLabel
        Left = 12
        Top = 4
        Width = 26
        Height = 13
        Caption = 'Data:'
      end
      object LaValor: TLabel
        Left = 112
        Top = 4
        Width = 27
        Height = 13
        Caption = 'Valor:'
      end
      object Label5: TLabel
        Left = 208
        Top = 4
        Width = 58
        Height = 13
        Caption = 'Benefici'#225'rio:'
      end
      object Label2: TLabel
        Left = 616
        Top = 4
        Width = 36
        Height = 13
        Caption = 'Cidade:'
      end
      object TPData: TDateTimePicker
        Left = 12
        Top = 20
        Width = 97
        Height = 21
        Date = 38741.724412615700000000
        Time = 38741.724412615700000000
        TabOrder = 0
      end
      object EdValor: TdmkEdit
        Left = 112
        Top = 20
        Width = 93
        Height = 21
        Alignment = taRightJustify
        TabOrder = 1
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdBenef: TdmkEdit
        Left = 208
        Top = 20
        Width = 405
        Height = 21
        CharCase = ecUpperCase
        TabOrder = 2
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdCidade: TdmkEdit
        Left = 616
        Top = 20
        Width = 153
        Height = 21
        CharCase = ecUpperCase
        TabOrder = 3
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
    end
    object Panel4: TPanel
      Left = 0
      Top = 44
      Width = 778
      Height = 49
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 1
      object Label3: TLabel
        Left = 12
        Top = 4
        Width = 34
        Height = 13
        Caption = 'Banco:'
      end
      object Label4: TLabel
        Left = 616
        Top = 4
        Width = 51
        Height = 13
        Caption = 'Bom para :'
      end
      object EdBomPara: TdmkEdit
        Left = 616
        Top = 20
        Width = 153
        Height = 21
        CharCase = ecUpperCase
        TabOrder = 0
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdBanco: TdmkEdit
        Left = 12
        Top = 20
        Width = 56
        Height = 21
        TabOrder = 1
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = '0'
        ValWarn = False
      end
      object dmkEdit2: TdmkEdit
        Left = 72
        Top = 20
        Width = 541
        Height = 21
        CharCase = ecUpperCase
        TabOrder = 2
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
    end
  end
  object Panel3: TPanel
    Left = 0
    Top = 141
    Width = 778
    Height = 190
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel1: TPanel
      Left = 0
      Top = 0
      Width = 264
      Height = 190
      Align = alLeft
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 264
        Height = 56
        Align = alTop
        Caption = ' Dados de conex'#227'o: '
        TabOrder = 0
        object Label11: TLabel
          Left = 8
          Top = 16
          Width = 68
          Height = 13
          Caption = 'IP do servidor:'
        end
        object Label12: TLabel
          Left = 100
          Top = 16
          Width = 28
          Height = 13
          Caption = 'Porta:'
        end
        object Label6: TLabel
          Left = 180
          Top = 16
          Width = 73
          Height = 13
          Caption = 'ID da conex'#227'o:'
        end
        object EdServer_IP: TdmkEdit
          Left = 8
          Top = 32
          Width = 90
          Height = 21
          TabOrder = 0
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '127.0.0.1'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = '127.0.0.1'
          ValWarn = False
        end
        object dmkEdit1: TdmkEdit
          Left = 100
          Top = 32
          Width = 76
          Height = 21
          Alignment = taRightJustify
          TabOrder = 1
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '9520'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 9520
          ValWarn = False
        end
        object EdID_ConexaoVai: TEdit
          Left = 180
          Top = 32
          Width = 76
          Height = 21
          ReadOnly = True
          TabOrder = 2
        end
      end
      object GroupBox2: TGroupBox
        Left = 0
        Top = 56
        Width = 264
        Height = 134
        Align = alClient
        Caption = ' Dados do Servidor: '
        TabOrder = 1
        object Label7: TLabel
          Left = 12
          Top = 68
          Width = 134
          Height = 13
          Caption = 'Retorno do ID da atribui'#231#227'o:'
        end
        object Label8: TLabel
          Left = 12
          Top = 20
          Width = 114
          Height = 13
          Caption = 'Mensagem de conex'#227'o:'
        end
        object Label9: TLabel
          Left = 12
          Top = 44
          Width = 119
          Height = 13
          Caption = 'Mensagem de atribui'#231#227'o:'
        end
        object Label10: TLabel
          Left = 12
          Top = 88
          Width = 126
          Height = 13
          Caption = #218'ltima mensagem de fluxo:'
        end
        object Ed3: TEdit
          Left = 136
          Top = 39
          Width = 120
          Height = 21
          ReadOnly = True
          TabOrder = 0
        end
        object Ed2: TEdit
          Left = 136
          Top = 15
          Width = 120
          Height = 21
          ReadOnly = True
          TabOrder = 1
        end
        object EdID_ConexaoVem: TEdit
          Left = 180
          Top = 64
          Width = 76
          Height = 21
          ReadOnly = True
          TabOrder = 2
        end
        object EdMsgA: TEdit
          Left = 12
          Top = 104
          Width = 244
          Height = 21
          ReadOnly = True
          TabOrder = 3
        end
      end
    end
    object Memo: TMemo
      Left = 264
      Top = 0
      Width = 514
      Height = 190
      Align = alClient
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 1
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 778
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 730
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 682
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 273
        Height = 32
        Caption = 'Emite Cheque Remoto'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 273
        Height = 32
        Caption = 'Emite Cheque Remoto'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 273
        Height = 32
        Caption = 'Emite Cheque Remoto'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 331
    Width = 778
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel5: TPanel
      Left = 2
      Top = 15
      Width = 774
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 375
    Width = 778
    Height = 70
    Align = alBottom
    TabOrder = 4
    object PnSaiDesis: TPanel
      Left = 632
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel6: TPanel
      Left = 2
      Top = 15
      Width = 630
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BitBtn1: TBitBtn
        Tag = 14
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Caption = '&Emite'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BitBtn1Click
      end
      object BtInfo: TBitBtn
        Tag = 118
        Left = 212
        Top = 3
        Width = 120
        Height = 40
        Caption = '&L'#234
        NumGlyphs = 2
        TabOrder = 1
        Visible = False
      end
    end
  end
  object IdTCPClient1: TIdTCPClient
    OnStatus = IdTCPClient1Status
    OnDisconnected = IdTCPClient1Disconnected
    OnConnected = IdTCPClient1Connected
    ConnectTimeout = 0
    IPVersion = Id_IPv4
    Port = 9520
    ReadTimeout = 100
    Left = 616
    Top = 12
  end
  object Timer1: TTimer
    Enabled = False
    OnTimer = Timer1Timer
    Left = 644
    Top = 12
  end
end
