unit DotPrint;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, ComCtrls, DBCtrls, UnInternalConsts,
  Db, (*DBTables,*) mySQLDbTables, ClipBrd, RichEdit, Variants, dmkGeral, dmkImage,
  UnDmkEnums, UnMyPrinters;

type
  //
  TFormatDOSPrint = (fdpNormalSize, fdpCompress, fdpDoubleSize, fdpAvanceSize);
  TFormatDOSPrints = set of TFormatDOSPrint;
  TMyTextos_136 = array [1..136] of String;
  TMyAlinha_136 = array [1..136] of TAlignment;
  TMyTamCol_136 = array [1..136] of Integer;
  //
  TFmDotPrint = class(TForm)
    Panel2: TPanel;
    Panel3: TPanel;
    RE1: TRichEdit;
    Memo1: TMemo;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel1: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel5: TPanel;
    StatusBar1: TStatusBar;
    BtImprime: TBitBtn;
    BtItens: TBitBtn;
    PainelMaster: TPanel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    EdCalcs: TEdit;
    EdWin: TEdit;
    Button3: TButton;
    Edit1: TEdit;
    EdPas: TEdit;
    RGCPI: TRadioGroup;
    RGAltura: TRadioGroup;
    EdFator: TEdit;
    Button2: TButton;
    CkInicio: TCheckBox;
    EdLinha: TEdit;
    EdTamanho: TEdit;
    Button1: TButton;
    CkEjeta: TCheckBox;
    EdPartes: TEdit;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure BtImprimeClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure RE1SelectionChange(Sender: TObject);
    procedure BtItensClick(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
  private
    { Private declarations }
    //
    //FMemoryStream: TMemoryStream;
    //procedure CopiaDeRichEdit(Source, Dest: TdmkRichEdit);
    procedure UpdateCursorPos;
    function MyDefLength: Int64;
    function LinhasErradas: Double;
    //
    //function GetCaretPos: TPoint;
    //nction GetSelLength: Integer;
    //function GetSelTextBuf(Buffer: PChar; BufSize: Integer): Integer;
    //function GetSelText: string;
    //function GetSelStart: Integer;

  public
    { Public declarations }
    FEntidade: Integer;
    FCPI,
    FLin,
    FCol: Integer;
    FCalcPos: Boolean;
    FTipoLinhas: Boolean;
    FQtdeLinhas: Integer;
    function LarguraLinha(Linha: Integer): Integer;
    function LocalizaLinha(Linha:Integer): Integer ;
    procedure AdicionaLinhaTexto(Texto: String;
              FormatoI, FormatoF: TFormatDOSPrints);
    procedure AdicionaLinhaTexto2(Texto: String; Fonte, Negrito,
              Italico, Sublinhado, Riscado: Integer);
    procedure AdicionaLinhaValor(Texto, Valor: String; ColunasValor, MargemD:
              Integer; FormatoI, FormatoF: TFormatDOSPrints);
    procedure AdicionaLinhaColunas(Colunas: Integer; Textos: TMyTextos_136;
              Alinha: TMyAlinha_136; Tamanhos: TMyTamCol_136;
              Formato: TFormatDOSPrints; Complemento: String);
    procedure AdicionaLinhaColunas2(Colunas: Integer; Textos: TMyTextos_136;
              Alinha: TMyAlinha_136; Tamanhos: TMyTamCol_136;
              Complemento: String; Fonte, Negrito: Integer);
    procedure ImprimeTeste;
  end;

var
  FmDotPrint: TFmDotPrint;

implementation

uses UnMyObjects, Textos, Module;

{$R *.DFM}

procedure TFmDotPrint.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmDotPrint.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  FCalcPos := True;
  UpdateCursorPos;
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmDotPrint.BtImprimeClick(Sender: TObject);
begin
  ImprimeTeste;
end;

procedure TFmDotPrint.ImprimeTeste;
var
  i, Linha: Integer;
  ArqPrn: TextFile;
  FormatI, Texto, FormatA, FormatNI, FormatNF, FormatF, Partes,
  NegrI, NegrF, ItalI, ItalF, Sublinhado, Riscado: String;
  NAnt, NIni, NFim, NSit, ArqPrnLength, TamI, TamF, CMod: Integer;
  MyCPI: String;
begin
  Linha := 0;
  ArqPrnLength := MyDefLength;
  AssignFile(ArqPrn, 'LPT1:');
  ReWrite(ArqPrn);
  RE1.SelStart := 0;
  //Write(ArqPrn, #27+'R'+#0);
  Write(ArqPrn, #13);
  case RGAltura.ItemIndex of
    0: Partes := #27+'0';
    1: Partes := #27+'1';
    2: Partes := #27+'2';
    3: Partes := MyPrinters.DefAlturaDotImp(3, Geral.IMV(EdFator.Text), '');
    4: Partes := MyPrinters.DefAlturaDotImp(4, Geral.IMV(EdFator.Text), '');
  end;
  //
  Write(ArqPrn, Partes);
  case RGCPI.ItemIndex of
    0: if FCPI = 12 then MyCPI := 'M' else MyCPI := 'P';
    1: MyCPI := 'P';
    2: MyCPI := 'M';
  end;
  Write(ArqPrn, #27+MyCPI);
  FormatA    := #15;
  FormatI    := #15;
  FormatNI   := #15;
  FormatNF   := #15;
  NegrI      := '';
  NegrF      := '';
  ItalI      := '';
  ItalF      := '';
  //
  Sublinhado := '';
  Riscado    := '';
  for i := 0 to ArqPrnLength do
  begin
    RE1.SelStart := i-2;
    if (fsBold in RE1.SelAttributes.Style) then NAnt := 1 else NAnt := 0;
    RE1.SelStart := i;
    if (fsBold in RE1.SelAttributes.Style) then NFim := 1 else NFim := 0;
    ////////////////////////////////////////////////////////////////////////////
    RE1.SelStart := i-1;
    TamI := RE1.SelAttributes.Size;
    if (fsBold in RE1.SelAttributes.Style) then NIni := 1 else NIni := 0;
    NSit := NAnt*100+NIni*10+NFim;
    case NSit of
      000: begin NegrI := '';      NegrF := '';      end;
      001: begin NegrI := '';      NegrF := '';      end;
      010: begin NegrI := #27+'E'; NegrF := #27+'F'; end;
      011: begin NegrI := #27+'E'; NegrF := '';      end;
      100: begin NegrI := '';      NegrF := '';      end;
      110: begin NegrI := '';      NegrF := #27+'F'; end;
      111: begin NegrI := '';      NegrF := '';      end;
    end;
    //if NegrI <> '' then
    (*if (NAnt = 0) and (NIni=0) and (NFim=0) then begin
      NegrI := '';
      NegrF := '';
    end else
    if (NAnt = 0) and (NIni=1) and (NFim=1) then begin
      NegrI := #27+'E';
      NegrF := '';
    end else
    if (NAnt = 1) and (NIni=1) and (NFim=1) then begin
      NegrI := '';
      NegrF := '';
    end else
    if (NAnt = 1) and (NIni=1) and (NFim=0) then begin
      NegrI := '';
      NegrF := #27+'F';
    end else
    if (NAnt = 0) and (NIni=1) and (NFim=0) then begin
      NegrI := #27+'E';
      NegrF := #27+'F';
    end;*)
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    (*case TamI of
      VAR_IMPSizeCompri: FormatNI := #15;
      VAR_IMPSizeNormal: FormatNI := '';
      VAR_IMPSizeExpand: FormatNI := #18#14;
    end;*)
    case TamI of
      VAR_IMPSizeCompri: FormatNI := #15;
      VAR_IMPSizeNormal: FormatNI := #18;
      VAR_IMPSizeExpand: FormatNI := #18#14;
    end;
    if FormatNI <> FormatA then
      FormatI := FormatNI
    else
      FormatI := '';
    ////////////////////////////////////////////////////////////////////////////
    RE1.SelStart := i;
    TamF := RE1.SelAttributes.Size;
    if TamF <> TamI then
    begin
      case TamI of
        VAR_IMPSizeCompri: FormatNF := #18;
        VAR_IMPSizeNormal: FormatNF := #18;
        VAR_IMPSizeExpand: FormatNF := #87;
      end;
    end;
    ////////////////////////////////////////////////////////////////////////////
    if NegrI <> '' then
      Write(ArqPrn, NegrI+#15);
    RE1.SelStart := i-1;
    if (FCol > 0) and (FCol <= Length(RE1.Lines[FLin])) then
    begin
      Texto := Copy(RE1.Lines[FLin], FCol, 1);
      Write(ArqPrn, FormatI+ItalI+Sublinhado+Riscado+Texto+FormatF+ItalF);
      if NegrF<> '' then
        Write(ArqPrn, NegrF+#15);
      FormatA := FormatNI;
    end else begin
      Write(ArqPrn, sLineBreak);
      Linha := Linha + 1;
      if FTipoLinhas then
      begin
        case FQtdeLinhas of
          0: if (Linha/63) = Int(Linha/63) then Write(ArqPrn, #12);
          1:
          begin
            if (Linha mod 63) = 31 then Write(ArqPrn, sLineBreak);
            if (Linha/63) = Int(Linha/63) then Write(ArqPrn, #12);
          end;
        end;
      end else if (Linha/63) = Int(Linha/63) then Write(ArqPrn, #12)
    end;
  end;
  Write(ArqPrn, #13);
  WriteLn(ArqPrn, #27+'0');
  if CkEjeta.Checked then
  begin
    case FQtdeLinhas of
      0: Write(ArqPrn, #12);
      1:
      begin
        CMod := (Linha mod 63);
        if CMod < 34 then
        begin
          while CMod < 34 do
          begin
            Write(ArqPrn, sLineBreak);
            Linha := Linha + 1;
            CMod := (Linha mod 63);
          end;
        end else Write(ArqPrn, #12);
      end;
    end;
  end;
  for i := 1 to Dmod.QrControle.FieldByName('OrcaLFeed').AsInteger do
    Write(ArqPrn, sLineBreak);
  CloseFile(ArqPrn);
  FCalcPos := True;
end;

procedure TFmDotPrint.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmDotPrint.AdicionaLinhaTexto(Texto: String;
  FormatoI, FormatoF: TFormatDOSPrints);
begin
  if fdpAvanceSize in FormatoI then RE1.SelAttributes.Size := VAR_IMPSizeAvance;
  if fdpCompress   in FormatoI then RE1.SelAttributes.Size := VAR_IMPSizeCompri;
  if fdpNormalSize in FormatoI then RE1.SelAttributes.Size := VAR_IMPSizeNormal;
  if fdpDoubleSize in FormatoI then RE1.SelAttributes.Size := VAR_IMPSizeExpand;
  RE1.Lines.Add(Texto);
end;

procedure TFmDotPrint.AdicionaLinhaTexto2(Texto: String; Fonte, Negrito,
Italico, Sublinhado, Riscado: Integer);
begin
  if Negrito > 0
    then RE1.SelAttributes.Style := RE1.SelAttributes.Style + [fsBold]
    else RE1.SelAttributes.Style := RE1.SelAttributes.Style - [fsBold];
  if Italico > 0
    then RE1.SelAttributes.Style := RE1.SelAttributes.Style + [fsItalic]
    else RE1.SelAttributes.Style := RE1.SelAttributes.Style - [fsItalic];
  if Sublinhado > 0
    then RE1.SelAttributes.Style := RE1.SelAttributes.Style + [fsUnderline]
    else RE1.SelAttributes.Style := RE1.SelAttributes.Style - [fsUnderline];
  if Riscado > 0
    then RE1.SelAttributes.Style := RE1.SelAttributes.Style + [fsStrikeOut]
    else RE1.SelAttributes.Style := RE1.SelAttributes.Style - [fsStrikeOut];
  case Fonte of
    -1: RE1.SelAttributes.Size := VAR_IMPSizeAvance;
     0: RE1.SelAttributes.Size := VAR_IMPSizeCompri;
     1: RE1.SelAttributes.Size := VAR_IMPSizeNormal;
     2: RE1.SelAttributes.Size := VAR_IMPSizeExpand;
   end;
  RE1.Lines.Add(Texto);
end;

procedure TFmDotPrint.AdicionaLinhaValor(Texto, Valor: String;
  ColunasValor, MargemD: Integer; FormatoI, FormatoF: TFormatDOSPrints);
var
  LargTxt, ColsImp: Integer;
  MyTexto: String;

begin
  if fdpNormalSize in FormatoI then ColsImp := VAR_LOCCOLUNASIMP else
  ColsImp := Trunc(VAR_LOCCOLUNASIMP * 17 / 10);
  MyTexto := Texto;
  LargTxt := ColsImp-ColunasValor-MargemD;
  if Length(MyTexto) > LargTxt then
  MyTexto := Copy(MyTexto, 1, LargTxt);
  while Length(MyTexto) < LargTxt do MyTexto := MyTexto+' ';
  while Length(Valor) < ColunasValor do Valor := ' ' + Valor;
  AdicionaLinhaTexto(MyTexto+Valor, FormatoI, FormatoF);
end;

procedure TFmDotPrint.AdicionaLinhaColunas(Colunas: Integer; Textos: TMyTextos_136;
              Alinha: TMyAlinha_136; Tamanhos: TMyTamCol_136;
              Formato: TFormatDOSPrints; Complemento: String);
var
  Str, Texto: String;
  i: Integer;
begin
  Texto := '';
  for i := 1 to Colunas do
  begin
    Str := Geral.CompletaString2(Textos[i], ' ', Tamanhos[i], Alinha[i],
      True, Complemento);
    Texto := Texto + Str;
  end;
  AdicionaLinhaTexto(Texto, Formato, Formato);
end;

procedure TFmDotPrint.AdicionaLinhaColunas2(Colunas: Integer; Textos: TMyTextos_136;
              Alinha: TMyAlinha_136; Tamanhos: TMyTamCol_136;
              Complemento: String; Fonte, Negrito: Integer);
var
  Str, Texto: String;
  i: Integer;
begin
  Texto := '';
  for i := 1 to Colunas do
  begin
    Str := Geral.CompletaString2(Textos[i], ' ', Tamanhos[i], Alinha[i],
      True, Complemento);
    Texto := Texto + Str;
  end;
  AdicionaLinhaTexto2(Texto, Fonte, Negrito, 0, 0, 0);
end;

procedure TFmDotPrint.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  UpdateCursorPos;
  //
  Exit;
  //MeFonteT.Lines.Clear;
  //MeTitulo.Lines.Clear;
  //MeCabeca.Lines.Clear;
  //MeRodaPe.Lines.Clear;
  //
  RE1.Lines.Clear;
  //RE2.Lines.Clear;
  if (VAR_SENHA = CO_MASTER)
  or ((Uppercase(VAR_SENHA) = Uppercase(VAR_ADMIN)) and (VAR_ADMIN <> '')) then
    PainelMaster.Visible := True;
end;

(*procedure TFmDotPrint.CopiaDeRichEdit(Source, Dest: TdmkRichEdit);
begin
  FMemoryStream := TMemoryStream.Create;
  Source.Lines.SaveToStream(FMemoryStream);
  // reset to the beginning of the stream
  FMemoryStream.Position := 0;
  Dest.PasteFromStream(FMemoryStream);
  FMemoryStream.Free;
end;*)

procedure TFmDotPrint.UpdateCursorPos;
var
  CharPos: TPoint;
begin
  if not FCalcPos then Exit;
  CharPos.Y := SendMessage(RE1.Handle, EM_EXLINEFROMCHAR, 0,
    RE1.SelStart);
  CharPos.X := (RE1.SelStart -
    SendMessage(RE1.Handle, EM_LINEINDEX, CharPos.Y, 0));
  FLin := CharPos.Y;
  FCol := CharPos.X;
  Inc(CharPos.Y);
  Inc(CharPos.X);
  StatusBar1.Panels[1].Text := IntToStr(CharPos.Y);
  StatusBar1.Panels[3].Text := IntToStr(CharPos.X);
  StatusBar1.Panels[5].Text := IntToStr(RE1.SelStart);
  StatusBar1.Panels[7].Text := IntToStr(RE1.SelAttributes.Size);
  StatusBar1.Panels[9].Text := IntToStr(Length(RE1.Lines[FLin]));
  StatusBar1.Panels[11].Text := RE1.Lines[FLin];
end;

procedure TFmDotPrint.RE1SelectionChange(Sender: TObject);
begin
  UpdateCursorPos;
end;

function TFmDotPrint.MyDefLength: Int64;
var
  i: Int64;
begin
  i := RE1.SelStart-1;
  while i <> RE1.SelStart do
  begin
    i := RE1.SelStart;
    RE1.SelStart := RE1.SelStart + 1;
  end;
  Result := i;
end;

procedure TFmDotPrint.BtItensClick(Sender: TObject);
var
  MemoryStream: TMemoryStream;
begin
  Application.CreateForm(TFmTextos, FmTextos);
  FmTextos.FAtributesSize := 8;
  FmTextos.FAtributesName := 'Arial';
  FmTextos.FSaveSit    := 2;
  FmTextos.FRichEdit   := RE1;
  MemoryStream := TMemoryStream.Create;
  RE1.Lines.SaveToStream(MemoryStream);
  MemoryStream.Position := 0;
  FmTextos.Editor.Lines.LoadFromStream(MemoryStream);
  MemoryStream.Free;
  //
  FmTextos.Splitter1.Visible := False;
  FmTextos.LbItensMD.Visible := False;
  FmTextos.ShowModal;
  FmTextos.Destroy;
end;

function TFmDotPrint.LinhasErradas: Double;
var
  i: Integer;
  FormatI, FormatA, FormatN: String;
  ArqPrnLength, Erros: Integer;
  //
  Tam: Double;
begin
  ArqPrnLength := MyDefLength;
  RE1.SelStart := 0;
  FormatA := '';
  FormatI := '';
  FormatN := '';
  i := -1;
  Tam := 0;
  Erros := 0;
  while i < ArqPrnLength do
  begin
    i := i + 1;
    RE1.SelStart := i;
    case RE1.SelAttributes.Size of
      VAR_IMPSizeCompri: FormatN := #15;
      VAR_IMPSizeNormal: FormatN := #18;
      VAR_IMPSizeExpand: FormatN := #14;
    end;
    if FormatN <> FormatA then FormatI := FormatN else FormatI := '';
    if (FCol > 0) and (FCol <= Length(RE1.Lines[FLin])) then
    begin
      case RE1.SelAttributes.Size of
        VAR_IMPSizeCompri: Tam := Tam + (1200/136);
        VAR_IMPSizeNormal: Tam := Tam + 15;
        VAR_IMPSizeExpand: Tam := Tam + 30;
      end;
    end;
    if (FCol = Length(RE1.Lines[FLin])) and (FCol>0) then
    begin
      Memo1.Lines.Add(FormatFloat('000.000', Tam));
      if Tam > 1200.5 then Erros := Erros + 1;
      Tam := 0;
    end;
  end;
  if Erros > 0 then
  begin
    if Geral.MensagemBox('H� '+IntToStr(Erros)+
    ' linha(s) com erro(s)! (Soma maior que 1200). Deseja imprimir assim mesmo?',
    'Erro', MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then Erros := 0;
  end;
  LinhasErradas := Erros;
end;

function TFmDotPrint.LocalizaLinha(Linha:Integer): Integer ;
begin
  RE1.SelStart := SendMessage(RE1.Handle, EM_LINEINDEX, Linha, 0);
  Result := RE1.SelStart;
end;

function TFmDotPrint.LarguraLinha(Linha: Integer): Integer;
var
  i, Ini, Fim: Integer;
  Tam: Double;
begin
  Tam := 0;
  Ini := LocalizaLinha(Linha);
  Fim := Ini + SendMessage(RE1.Handle, EM_LINELENGTH, RE1.SelStart, 0);
  for i := Ini to Fim-2 do
  begin
    FmDotPrint.RE1.SelStart := i;
    case FmDotPrint.RE1.SelAttributes.Size of
      VAR_IMPSizeAvance: Tam := Tam + 0;
      VAR_IMPSizeCompri: Tam := Tam + 1/136;
      VAR_IMPSizeNormal: Tam := Tam + 1/80;
      VAR_IMPSizeExpand: Tam := Tam + 1/40;
    end;
  end;
  if FCPI = 12 then Tam := Tam / 1.2;
  Tam := Tam * 800 * CO_POLEGADA;
  Result := Trunc(Tam);
end;

procedure TFmDotPrint.Button2Click(Sender: TObject);
begin
  LocalizaLinha(Geral.IMV(EdLinha.Text));
  EdTamanho.Text := IntToStr(LarguraLinha(Geral.IMV(EdLinha.Text)));
end;

(*function TFmDotPrint.GetCaretPos: TPoint;
var
  CharRange: TCharRange;
begin
  SendMessage(Handle, EM_EXGETSEL, 0, LongInt(@CharRange));
  Result.X := CharRange.cpMax;
  Result.Y := SendMessage(Handle, EM_EXLINEFROMCHAR, 0, Result.X);
  Result.X := Result.X - SendMessage(Handle, EM_LINEINDEX, -1, 0);
end;*)

(*nction TFmDotPrint.GetSelLength: Integer;
var
  CharRange: TCharRange;
begin
  SendMessage(Handle, EM_EXGETSEL, 0, Longint(@CharRange));
  Result := CharRange.cpMax - CharRange.cpMin;
end;*)

(*function TFmDotPrint.GetSelStart: Integer;
var
  CharRange: TCharRange;
begin
  SendMessage(Handle, EM_EXGETSEL, 0, Longint(@CharRange));
  Result := CharRange.cpMin;
end;*)

(*function TFmDotPrint.GetSelTextBuf(Buffer: PChar; BufSize: Integer): Integer;
var
  S: string;
begin
  S := GetSelText;
  Result := Length(S);
  if BufSize < Length(S) then Result := BufSize;
  StrPLCopy(Buffer, S, Result);
end;*)

(*function TFmDotPrint.GetSelText: string;
var
  Length: Integer;
begin
  SetLength(Result, GetSelLength + 1);
  Length := SendMessage(Handle, EM_GETSELTEXT, 0, Longint(PChar(Result)));
  SetLength(Result, Length);
end;*)

procedure TFmDotPrint.Button1Click(Sender: TObject);
var
 L: Integer;
begin
  L := SendMessage(RE1.Handle, EM_LINELENGTH, RE1.SelStart, 0);
  EdTamanho.Text := IntToStr(L);
end;

procedure TFmDotPrint.Button3Click(Sender: TObject);
var
  i, Calcs: Integer;
  Tempo, win, pas: TTime;
  x: Double;
begin
  Calcs := Geral.IMV(EdCalcs.Text);
  x := 0;
  Tempo := Now;
  for i := 1 to Calcs do
    x := x + (SendMessage(RE1.Handle, EM_LINELENGTH, RE1.SelStart, 0) / 1000000000);
  Win := Now - Tempo;
  Edit1.Text := FloatToStr(x);
  ///////////////////
  Tempo := Now;
  for i := 1 to Calcs do
    x := x + Length(RE1.Lines[0]);
  pas := Now - Tempo;
  Edit1.Text := FloatToStr(x);
  ///////////////////
  Edwin.Text := FloatToStr(Win*86400000);
  EdPas.Text := FloatToStr(Pas*86400000);
  // o windows leva apenas 9% do tempo do pascal
end;

end.


