object FmEmiteCheque: TFmEmiteCheque
  Left = 367
  Top = 177
  Caption = 'Emite Cheque'
  ClientHeight = 509
  ClientWidth = 778
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelDados: TPanel
    Left = 0
    Top = 48
    Width = 778
    Height = 413
    Align = alClient
    TabOrder = 0
    object Label10: TLabel
      Left = 16
      Top = 344
      Width = 55
      Height = 13
      Caption = 'Mensagem:'
    end
    object Edit1: TEdit
      Left = 348
      Top = 12
      Width = 61
      Height = 21
      TabOrder = 1
      Text = '.C409288263000B5893809900A46300993'
      OnChange = Edit1Change
    end
    object Edit2: TEdit
      Left = 436
      Top = 12
      Width = 129
      Height = 21
      TabOrder = 2
      Text = 'Edit2'
      OnChange = Edit2Change
    end
    object Panel1: TPanel
      Left = 1
      Top = 233
      Width = 790
      Height = 108
      BevelOuter = bvNone
      TabOrder = 0
      Visible = False
      object Label3: TLabel
        Left = 14
        Top = 60
        Width = 34
        Height = 13
        Caption = 'Banco:'
      end
      object Label4: TLabel
        Left = 98
        Top = 60
        Width = 42
        Height = 13
        Caption = 'Ag'#234'ncia:'
      end
      object Label6: TLabel
        Left = 204
        Top = 60
        Width = 31
        Height = 13
        Caption = 'Conta:'
      end
      object Label7: TLabel
        Left = 434
        Top = 60
        Width = 40
        Height = 13
        Caption = 'Cheque:'
      end
      object Label8: TLabel
        Left = 580
        Top = 60
        Width = 33
        Height = 13
        Caption = 'Comp.:'
      end
      object Label9: TLabel
        Left = 16
        Top = 8
        Width = 35
        Height = 13
        Caption = 'CMC-7:'
      end
      object ST_Banco: TStaticText
        Left = 14
        Top = 76
        Width = 67
        Height = 30
        Caption = '399'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -28
        Font.Name = 'CMC7'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
      end
      object ST_Agencia: TStaticText
        Left = 98
        Top = 76
        Width = 88
        Height = 30
        Caption = '0036'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -28
        Font.Name = 'CMC7'
        Font.Style = []
        ParentFont = False
        TabOrder = 1
      end
      object ST_Conta: TStaticText
        Left = 204
        Top = 76
        Width = 214
        Height = 30
        Caption = '0036288290'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -28
        Font.Name = 'CMC7'
        Font.Style = []
        ParentFont = False
        TabOrder = 2
      end
      object ST_CMC7: TStaticText
        Left = 14
        Top = 24
        Width = 634
        Height = 30
        Caption = '399 0036 0036288290 908398 009'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -28
        Font.Name = 'CMC7'
        Font.Style = []
        ParentFont = False
        TabOrder = 3
      end
      object ST_Cheque: TStaticText
        Left = 434
        Top = 76
        Width = 130
        Height = 30
        Caption = '908398'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -28
        Font.Name = 'CMC7'
        Font.Style = []
        ParentFont = False
        TabOrder = 4
      end
      object ST_Praca: TStaticText
        Left = 580
        Top = 76
        Width = 67
        Height = 30
        Caption = '009'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -28
        Font.Name = 'CMC7'
        Font.Style = []
        ParentFont = False
        TabOrder = 5
      end
    end
    object Panel2: TPanel
      Left = 1
      Top = 1
      Width = 776
      Height = 232
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 3
      object Label1: TLabel
        Left = 12
        Top = 8
        Width = 26
        Height = 13
        Caption = 'Data:'
      end
      object LaValor: TLabel
        Left = 112
        Top = 8
        Width = 27
        Height = 13
        Caption = 'Valor:'
      end
      object Label5: TLabel
        Left = 208
        Top = 8
        Width = 58
        Height = 13
        Caption = 'Benefici'#225'rio:'
      end
      object Label2: TLabel
        Left = 616
        Top = 8
        Width = 36
        Height = 13
        Caption = 'Cidade:'
      end
      object TPData: TDateTimePicker
        Left = 12
        Top = 24
        Width = 97
        Height = 21
        Date = 38741.724412615700000000
        Time = 38741.724412615700000000
        TabOrder = 0
      end
      object EdValor: TLMDEdit
        Left = 112
        Top = 24
        Width = 93
        Height = 21
        
        Caret.BlinkRate = 530
        TabOrder = 1
        OnExit = EdValorExit
        AutoSelect = True
        Alignment = taRightJustify
        CustomButtons = <>
        PasswordChar = #0
        Text = '0,00'
      end
      object EdBenef: TLMDEdit
        Left = 208
        Top = 24
        Width = 405
        Height = 21
        
        Caret.BlinkRate = 530
        TabOrder = 2
        CharCase = ecUpperCase
        AutoSelect = True
        CustomButtons = <>
        PasswordChar = #0
      end
      object EdCidade: TLMDEdit
        Left = 616
        Top = 24
        Width = 153
        Height = 21
        
        Caret.BlinkRate = 530
        TabOrder = 3
        CharCase = ecUpperCase
        AutoSelect = True
        CustomButtons = <>
        PasswordChar = #0
      end
      object RGForma: TRadioGroup
        Left = 12
        Top = 52
        Width = 757
        Height = 177
        Caption = ' Forma de preenchimento: '
        Columns = 2
        ItemIndex = 2
        Items.Strings = (
          '0 - Somente preenchimento'
          '1 - Preenchimento e chancela'
          '2 - Preenchimento e leitura de caracteres magnetiz'#225'veis CMC7'
          '3 - Preenchimento, chancela e leitura'
          '4 - Preenchimento com ano de 4 d'#237'gitos'
          '5 - Preenchimento, chancela e ano com 4 d'#237'gitos'
          '6 - Preenchimento, leitura e ano com 4 d'#237'gitos'
          '7 - Preenchimento, chancela, leitura e ano com 4 d'#237'gitos'
          '8 - Preenchimento e cruzamento'
          '9 - Preenchimento, cruzamento e chancela'
          'A - Preenchimento, cruzamento e leitura'
          'B - Preenchimento, cruzamento, chancela e leitura'
          'C - Preenchimento, cruzamento e ano com 4 d'#237'gitos'
          'D - Preenchimento, cruzamento, chancela e ano com 4 d'#237'gitos'
          'E - Preenchimento, cruzamento, leitura e ano com 4 d'#237'gitos'
          
            'F - Preenchimento, cruzamento, chancela, leitura e ano com 4 d'#237'g' +
            'itos')
        TabOrder = 4
      end
    end
    object EdMens: TLMDEdit
      Left = 16
      Top = 360
      Width = 757
      Height = 21
      
      Caret.BlinkRate = 530
      TabOrder = 4
      CharCase = ecUpperCase
      AutoSelect = True
      CustomButtons = <>
      PasswordChar = #0
    end
    object ST1: TStaticText
      Left = 1
      Top = 395
      Width = 776
      Height = 17
      Align = alBottom
      Alignment = taCenter
      Caption = '...'
      TabOrder = 5
    end
  end
  object PainelConfirma: TPanel
    Left = 0
    Top = 461
    Width = 778
    Height = 48
    Align = alBottom
    TabOrder = 1
    object BtOK: TBitBtn
      Tag = 14
      Left = 20
      Top = 4
      Width = 90
      Height = 40
      Caption = '&Emite'
      TabOrder = 0
      OnClick = BtOKClick
      NumGlyphs = 2
    end
    object BtSaida: TBitBtn
      Tag = 13
      Left = 682
      Top = 4
      Width = 90
      Height = 40
      Cursor = crHandPoint
      Hint = 'Sai da janela atual'
      Caption = '&Sa'#237'da'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      OnClick = BtSaidaClick
      NumGlyphs = 2
    end
    object BtInfo: TBitBtn
      Tag = 118
      Left = 212
      Top = 4
      Width = 90
      Height = 40
      Caption = '&L'#234
      TabOrder = 2
      OnClick = BtInfoClick
      NumGlyphs = 2
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 778
    Height = 48
    Align = alTop
    
    
    
    
    Caption = 'Emite Cheque'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 2

    object Image1: TImage
      Left = 2
      Top = 2
      Width = 774
      Height = 44
      Align = alClient
      Transparent = True
      ExplicitWidth = 782
    end
  end
end
