unit ImprimeEmpr;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Variants, DBCtrls,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB, mySQLDbTables, dmkGeral,
  dmkImage, UnDmkEnums;

type
  TFmImprimeEmpr = class(TForm)
    Panel1: TPanel;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    Label1: TLabel;
    QrParamsNFs: TmySQLQuery;
    DsParamsNFs: TDataSource;
    Label2: TLabel;
    EdParamsNFs: TdmkEditCB;
    CBParamsNFs: TdmkDBLookupComboBox;
    QrParamsNFsCodigo: TIntegerField;
    QrParamsNFsControle: TIntegerField;
    QrParamsNFsSerieNF: TIntegerField;
    QrParamsNFsSequencial: TIntegerField;
    QrParamsNFsMaxSeqLib: TIntegerField;
    QrParamsNFsIncSeqAuto: TSmallintField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    QrParamsNFsSerieNFSeq_TXT: TWideStringField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure EdEmpresaChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  var
  FmImprimeEmpr: TFmImprimeEmpr;

implementation

uses UnMyObjects, Module, UMySQLModule, ModuleGeral, Imprime, UnInternalConsts,
  DmkDAC_PF;

{$R *.DFM}

procedure TFmImprimeEmpr.BtOKClick(Sender: TObject);
var
  Codigo, Controle, Filial, Empresa, ParamsNFs: Integer;
begin
  Filial := Geral.IMV(EdEmpresa.Text);
  if Filial = 0 then
  begin
    Geral.MB_Aviso('Informe a filial!');
    EdEmpresa.SetFocus;
    Exit;
  end;
  Empresa := DModG.QrEmpresasCodigo.Value;
  ParamsNFs := Geral.IMV(EdParamsNFs.Text);
  if ParamsNFs = 0 then
  begin
    Geral.MB_Aviso('Informe a S�rie da Nota Fiscal!');
    EdParamsNFs.SetFocus;
    Exit;
  end;
  Screen.Cursor := crHourGlass;
  try
    Codigo   := FmImprime.QrImprimeCodigo.Value;
    Controle := UMyMod.BuscaEmLivreY_Def('imprimeempr', 'Controle', stIns, 0);
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'imprimeempr', False, [
    'Codigo', 'Empresa', 'ParamsNFs'], ['Controle'], [
    Codigo, Empresa, ParamsNFs], [Controle], True) then
    begin
      FmImprime.ReopenImprimeEmpr(Controle);
      Screen.Cursor := crDefault;
      Close;
    end;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmImprimeEmpr.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmImprimeEmpr.EdEmpresaChange(Sender: TObject);
begin
  QrParamsNFs.Close;
  QrParamsNFs.Params[0].AsInteger := DModG.QrEmpresasCodigo.Value;
  UnDmkDAC_PF.AbreQuery(QrParamsNFs, Dmod.MyDB);
  //
  if not QrParamsNFs.Locate('Controle', EdParamsNFs.ValueVariant, []) then
  begin
    EdParamsNFs.ValueVariant := 0;
    CBParamsNFs.KeyValue     := Null;
  end;  
end;

procedure TFmImprimeEmpr.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmImprimeEmpr.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  DModG.ReopenEmpresas(VAR_USUARIO, 0, EdEmpresa, CBEmpresa);
  CBEmpresa.ListSource := DModG.DsEmpresas;
end;

procedure TFmImprimeEmpr.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

end.

