unit EmiteCheque_4;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkLabel,
  mySQLDbTables, dmkGeral, ComCtrls, DBCtrls, dmkDBLookupComboBox, dmkEdit,
  dmkEditCB, Mask, dmkDBGrid, dmkDBGridDAC, DB, UnDmkProcFunc, dmkImage,
  DmkDAC_PF, dmkEditDateTimePicker, UnDmkEnums, UnMyPrinters;

type
  TFmEmiteCheque_4 = class(TForm)
    QrChConfigCab: TmySQLQuery;
    QrChConfigCabCodigo: TIntegerField;
    QrChConfigCabNome: TWideStringField;
    QrChConfigCabAltura: TIntegerField;
    QrChConfigCabTopoIni: TIntegerField;
    QrChConfigCabCPI: TSmallintField;
    QrChConfigCabMEsq: TSmallintField;
    DsChConfigCab: TDataSource;
    QrIni: TmySQLQuery;
    QrIniCodigo: TIntegerField;
    QrIniCampo: TIntegerField;
    QrIniTopo: TIntegerField;
    QrIniMEsq: TIntegerField;
    QrIniLarg: TIntegerField;
    QrIniFTam: TIntegerField;
    QrIniNegr: TIntegerField;
    QrIniItal: TIntegerField;
    QrIniLk: TIntegerField;
    QrIniDataCad: TDateField;
    QrIniDataAlt: TDateField;
    QrIniUserCad: TIntegerField;
    QrIniUserAlt: TIntegerField;
    QrIniAltu: TIntegerField;
    QrIniPref: TWideStringField;
    QrIniSufi: TWideStringField;
    QrIniPadr: TWideStringField;
    QrIniAliV: TIntegerField;
    QrIniAliH: TIntegerField;
    QrIniTopR: TIntegerField;
    QrChConfVal: TmySQLQuery;
    QrChConfValCodigo: TIntegerField;
    QrChConfValCampo: TIntegerField;
    QrChConfValTopo: TIntegerField;
    QrChConfValMEsq: TIntegerField;
    QrChConfValLarg: TIntegerField;
    QrChConfValFTam: TIntegerField;
    QrChConfValNegr: TIntegerField;
    QrChConfValItal: TIntegerField;
    QrChConfValLk: TIntegerField;
    QrChConfValDataCad: TDateField;
    QrChConfValDataAlt: TDateField;
    QrChConfValUserCad: TIntegerField;
    QrChConfValUserAlt: TIntegerField;
    QrChConfValAltu: TIntegerField;
    QrChConfValPref: TWideStringField;
    QrChConfValSufi: TWideStringField;
    QrChConfValPadr: TWideStringField;
    QrChConfValAliV: TIntegerField;
    QrChConfValAliH: TIntegerField;
    QrChConfValTopR: TIntegerField;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    Panel1: TPanel;
    QrChequesImp: TmySQLQuery;
    QrChequesImpValor: TWideStringField;
    QrChequesImpExtenso1: TWideStringField;
    QrChequesImpExtenso2: TWideStringField;
    QrChequesImpFavorecido: TWideStringField;
    QrChequesImpCidade: TWideStringField;
    QrChequesImpDia: TWideStringField;
    QrChequesImpMes: TWideStringField;
    QrChequesImpAno: TWideStringField;
    QrChequesImpBomPara: TWideStringField;
    QrChequesImpObservacao: TWideStringField;
    QrChequesImpSeq: TIntegerField;
    QrChequesImpCodigo: TIntegerField;
    QrChequesImpCPI: TIntegerField;
    QrChequesImpTopoIni: TIntegerField;
    QrChequesImpAltura: TIntegerField;
    QrChequesImpMEsq: TIntegerField;
    DsChequesImp: TDataSource;
    Panel5: TPanel;
    DBGrid1: TDBGrid;
    Panel6: TPanel;
    DBEdit2: TDBEdit;
    DBEdit1: TDBEdit;
    QrChequesImpControle: TIntegerField;
    QrChequesImpSub: TIntegerField;
    QrChequesImpChNumero: TFloatField;
    QrChequesImpChSerie: TWideStringField;
    QrChequesImpChGrava: TSmallintField;
    QrView1: TmySQLQuery;
    QrView1Codigo: TIntegerField;
    QrView1Campo: TIntegerField;
    QrView1Topo: TIntegerField;
    QrView1MEsq: TIntegerField;
    QrView1Larg: TIntegerField;
    QrView1FTam: TIntegerField;
    QrView1Negr: TIntegerField;
    QrView1Ital: TIntegerField;
    QrView1Lk: TIntegerField;
    QrView1DataCad: TDateField;
    QrView1DataAlt: TDateField;
    QrView1UserCad: TIntegerField;
    QrView1UserAlt: TIntegerField;
    QrView1Altu: TIntegerField;
    QrView1Pref: TWideStringField;
    QrView1Sufi: TWideStringField;
    QrView1Padr: TWideStringField;
    QrView1AliV: TIntegerField;
    QrView1AliH: TIntegerField;
    QrView1TopR: TIntegerField;
    QrView1PrEs: TIntegerField;
    QrTopos1: TmySQLQuery;
    QrTopos1TopR: TIntegerField;
    Panel3: TPanel;
    BitBtn1: TBitBtn;
    Panel4: TPanel;
    BitBtn2: TBitBtn;
    TabSheet3: TTabSheet;
    Panel7: TPanel;
    Button1: TButton;
    Button2: TButton;
    EdCarIni: TdmkEdit;
    EdCarFim: TdmkEdit;
    Label2: TLabel;
    Label3: TLabel;
    EdReverso: TdmkEdit;
    Label4: TLabel;
    Panel8: TPanel;
    Panel9: TPanel;
    PageControl2: TPageControl;
    TabSheet4: TTabSheet;
    Grade: TStringGrid;
    EdCol: TdmkEdit;
    Label5: TLabel;
    Label7: TLabel;
    EdLin: TdmkEdit;
    Button3: TButton;
    BitBtn3: TBitBtn;
    QrCart: TmySQLQuery;
    QrCartTipo: TIntegerField;
    QrCartTipoDoc: TSmallintField;
    QrChequesLct: TmySQLQuery;
    QrChequesLctData: TDateField;
    QrChequesLctControle: TIntegerField;
    QrChequesLctGenero: TIntegerField;
    QrChequesLctDescricao: TWideStringField;
    QrChequesLctVencimento: TDateField;
    QrChequesLctFornecedor: TIntegerField;
    QrChequesLctNO_ENT: TWideStringField;
    QrChequesLctAtivo: TSmallintField;
    Panel10: TPanel;
    Label8: TLabel;
    EdChConfig: TdmkEditCB;
    CBChConfig: TdmkDBLookupComboBox;
    SpeedButton1: TSpeedButton;
    LaCidade: TLabel;
    EdCidade: TdmkEdit;
    RGLct: TRadioGroup;
    RGData: TRadioGroup;
    RGBomPara: TRadioGroup;
    CkReGerar: TCheckBox;
    CBRegerar: TGroupBox;
    Label6: TLabel;
    LaDoc: TLabel;
    EdSerieCH: TdmkEdit;
    EdDocumento: TdmkEdit;
    GroupBox1: TGroupBox;
    Label1: TLabel;
    EdPorta: TdmkEdit;
    DsChequesLct: TDataSource;
    QrChequesLctNO_CONTA: TWideStringField;
    QrChequesLctSub: TSmallintField;
    QrChequesLctDocumento: TFloatField;
    QrChequesLctSerieCH: TWideStringField;
    QrChequesLctDebito: TFloatField;
    TabSheet5: TTabSheet;
    Memo1: TMemo;
    BtCopiaDoc: TBitBtn;
    CkNominal: TCheckBox;
    CkForca: TCheckBox;
    QrLoc: TmySQLQuery;
    QrChequesLctAgrupar: TIntegerField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel11: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel13: TPanel;
    Panel12: TPanel;
    BtGera: TBitBtn;
    PnDefAll: TPanel;
    BtAgrupar: TBitBtn;
    BtDesagrupar: TBitBtn;
    BtTodos: TBitBtn;
    BtNenhum: TBitBtn;
    PB1: TProgressBar;
    QrChequesLctAGRUPADO_TXT: TWideStringField;
    PCLct: TPageControl;
    TabSheet6: TTabSheet;
    TabSheet7: TTabSheet;
    DBGLct: TdmkDBGridDAC;
    DBGLct2: TdmkDBGridDAC;
    QrChequesLct2: TmySQLQuery;
    DsChequesLct2: TDataSource;
    QrChequesLct2Ativo: TSmallintField;
    QrChequesLct2Data: TDateField;
    QrChequesLct2Controle: TIntegerField;
    QrChequesLct2Genero: TIntegerField;
    QrChequesLct2NO_CONTA: TWideStringField;
    QrChequesLct2Descricao: TWideStringField;
    QrChequesLct2Vencimento: TDateField;
    QrChequesLct2Fornecedor: TIntegerField;
    QrChequesLct2NO_ENT: TWideStringField;
    QrChequesLct2Sub: TSmallintField;
    QrChequesLct2Documento: TFloatField;
    QrChequesLct2SerieCH: TWideStringField;
    QrChequesLct2Debito: TFloatField;
    QrChequesLct2Agrupar: TIntegerField;
    QrChequesLct2AGRUPADO_TXT: TWideStringField;
    RGLctsCfg: TGroupBox;
    RGTipoData: TRadioGroup;
    Label9: TLabel;
    TPDataIni: TdmkEditDateTimePicker;
    TPDataFim: TdmkEditDateTimePicker;
    Label10: TLabel;
    CkFiltroLct: TCheckBox;
    BtRefresh: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure EdChConfigChange(Sender: TObject);
    procedure QrChConfigCabAfterOpen(DataSet: TDataSet);
    procedure BtGeraClick(Sender: TObject);
    procedure CkReGerarClick(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure BitBtn3Click(Sender: TObject);
    procedure QrChequesLctBeforeClose(DataSet: TDataSet);
    procedure BtTodosClick(Sender: TObject);
    procedure BtNenhumClick(Sender: TObject);
    procedure RGLctClick(Sender: TObject);
    procedure BtCopiaDocClick(Sender: TObject);
    procedure BtAgruparClick(Sender: TObject);
    procedure BtDesagruparClick(Sender: TObject);
    procedure PageControl1Change(Sender: TObject);
    procedure QrChequesLctAfterScroll(DataSet: TDataSet);
    procedure DBGLctCellClick(Column: TColumn);
    procedure CkFiltroLctClick(Sender: TObject);
    procedure BtRefreshClick(Sender: TObject);
  private
    { Private declarations }
    FLinAtu, FPosIni, FLetras1, FLetras2: Integer;
    FPosAtu: Double;
    //
    FArqPrn: TextFile;
    FTamLinha, FFoiExpand: Integer;
    FPrintedLine: Boolean;
    FChequesLct, FNameFldFrn: String;
    //function AvancaCarro(PosicaoAtual, NovaPosicao: Integer): Integer;
    function AvancaCarro2(NovaPosicao: Integer): Boolean;
    function EspacoEntreTextos(QrView: TmySQLQuery; CPI: Integer): String;
    function Formata(Texto, Prefixo, Sufixo: String; EspacosPrefixo, Formato:
             Integer; Nulo: Boolean): String;
    function FormataTexto(Texto, Prefixo, Sufixo, Substituicao: String;
             EspacosPrefixo, Substitui, Alinhamento, Comprimento, Fonte,
             Formatacao, CPI: Integer; Nulo: Boolean): String;
    //
    procedure DefinePadroes();
    procedure ConfiguraExtenso(const Valor: String; var Linha1, Linha2: String);
    procedure ConfiguraTamEdits();
    procedure ReopenChConfVal();
    procedure ReopenChequesImp();
    procedure ReopenChequesLct(Controle: Integer);
    procedure AtivarTodos(Ativo: Integer);
    procedure InfoAvanco(Tipo: Char; LinRun, NeedPos: Integer);
  public
    { Public declarations }
    FCarteira: Integer;
    FGrade: TDBGrid;
    FTabLct, FCampoNomeFornecedor: String;

    //
    function PreparaDados(): Boolean;
    procedure SelecionaItens();
    procedure ImprimeChequeAtual(ChAtu, ChMax, ChConfCab, CPI, TopoIni,
              Altura, MEsq: Integer; Dia, Mes, Ano, Vcto, Valor, Favorecido, Extenso1,
              Extenso2, Obs, Cidade: String);
    //
  end;

  var
  FmEmiteCheque_4: TFmEmiteCheque_4;


implementation

uses UnMyObjects, UCreate, ModuleGeral, UnInternalConsts, UMySQLModule,
  ChConfigCab, MyDBCheck, Module, UnFinanceiro, ModuleFin, UnFinanceiroJan;

  const
  FAltLin = CO_POLEGADA / 2.16;

{$R *.DFM}

procedure TFmEmiteCheque_4.BitBtn1Click(Sender: TObject);
  procedure AtualizaLancto(SerieCH: String; Documento: Double; Controle: Integer);
  var
    Lct: Integer;
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(DModG.QrAuxPID1, DModG.MyPID_DB, [
    'SELECT Controle',
    'FROM chequeslct',
    'WHERE Agrupar=' + Geral.FF0(Controle),
    'AND Controle<>' + Geral.FF0(Controle),
    '']);
    if DModG.QrAuxPID1.RecordCount > 0 then
    begin
      DModG.QrAuxPID1.First;
      while not DModG.QrAuxPID1.Eof do
      begin
        Lct := DModG.QrAuxPID1.FieldByName('Controle').AsInteger;
        //
        UFinanceiro.SQLInsUpd_Lct(DMod.QrUpd, stUpd, False, ['SerieCH', 'Documento'],
          ['Controle'],  [SerieCH, Documento], [Lct], True, '', FTabLct);
        //
        DModG.QrAuxPID1.Next;
      end;
    end else
    begin
      UFinanceiro.SQLInsUpd_Lct(DMod.QrUpd, stUpd, False, ['SerieCH', 'Documento'],
        ['Controle'],  [SerieCH, Documento], [Controle], True, '', FTabLct);
    end;
  end;
var
  ChAtu, ChMax, ChConfCab, CPI, TopoIni, Altura, MEsq, Controle: Integer;
  Dia, Mes, Ano, Vcto, Valor, Favorecido, Extenso1, Extenso2, Obs, Cidade,
  SerieCH: String;
  Documento: Double;
begin
  Memo1.Lines.Clear;
  FLinAtu := 0;
  FPosAtu := 0;
  //
  QrChequesImp.DisableControls;
  Screen.Cursor := crHourGlass;
  try
    SerieCH   := EdSerieCH.Text;
    Documento := EdDocumento.ValueVariant;
    //
    QrChequesImp.First;
    while not QrChequesImp.Eof do
    begin
      ChAtu      := QrChequesImp.RecNo;
      ChMax      := QrChequesImp.RecordCount;
      ChConfCab  := QrChequesImpCodigo.Value;
      CPI        := QrChequesImpCPI.Value;
      TopoIni    := QrChequesImpTopoIni.Value;
      Altura     := QrChequesImpAltura.Value;
      MEsq       := QrChequesImpMEsq.Value;
      Dia        := QrChequesImpDia.Value;
      Mes        := QrChequesImpMes.Value;
      Ano        := QrChequesImpAno.Value;
      Vcto       := QrChequesImpBomPara.Value;
      Valor      := QrChequesImpValor.Value;
      Favorecido := QrChequesImpFavorecido.Value;
      Extenso1   := QrChequesImpExtenso1.Value;
      Extenso2   := QrChequesImpExtenso2.Value;
      Obs        := QrChequesImpObservacao.Value;
      Cidade     := QrChequesImpCidade.Value;
      //
      Controle   := QrChequesImpControle.Value;
      //
      ImprimeChequeAtual(ChAtu, ChMax, ChConfCab, CPI, TopoIni, Altura, MEsq,
      Dia, Mes, Ano, Vcto, Valor, Favorecido, Extenso1, Extenso2, Obs, Cidade);
      //
      if CkReGerar.Checked then
      begin
        AtualizaLancto(SerieCH, Documento, Controle);
        //
        Documento := Documento + 1;
      end;
      //
      QrChequesImp.Next;
    end;
  finally
    QrChequesImp.EnableControls;
    Screen.Cursor := crDefault;
  end;
  BtCopiaDoc.Enabled := True;
end;

procedure TFmEmiteCheque_4.BitBtn3Click(Sender: TObject);
var
  Observacao: String;
  Controle: Integer;
begin
  Controle   := QrChequesImpControle.Value;
  Observacao := QrChequesImpObservacao.Value;
  if InputQuery('Observa��o', 'Informe a nova observa��o', Observacao) then
  begin
    UMyMod.SQLInsUpd(DmodG.QrUpdPID1, stUpd, 'chequesimp', True, [
    'Observacao'], ['Controle'], [Observacao], [Controle], False);
  end;
  QrChequesImp.Close;
  UnDmkDAC_PF.AbreQuery(QrChequesImp, DModG.MyPID_DB);
  QrChequesImp.Locate('Controle', Controle, []);
end;

procedure TFmEmiteCheque_4.BtAgruparClick(Sender: TObject);
  function InsereTmpLct(Debito: Double; Fornecedor, Data: String): Integer;
  var
    Controle: Integer;
  begin
    Controle := UMyMod.BuscaNovoCodigo_Int(DModG.QrUpdPID1, 'chequeslct',
                  'Controle', [], [], stIns, 0, siNegativo, nil);
    //
    if UMyMod.SQLInsUpd(DModG.QrUpdPID1, stIns, 'chequeslct', False,
      ['Data', 'Vencimento', 'Debito', 'NO_ENT', 'Agrupar'], ['Controle'],
      [Data, Data, Debito, Fornecedor, Controle], [Controle], False)
    then
      Result := Controle
    else
      Result := 0;
  end;
  procedure AtualizaTmpLct(Controle, CodAgrupa: Integer);
  begin
    UMyMod.SQLInsUpd(DModG.QrUpdPID1, stUpd, 'chequeslct', False, ['Ativo', 'Agrupar'],
      ['Controle'], [0, CodAgrupa], [Controle], False);
  end;

  function VerificaSeSelNaoEstaAgrupado(): Boolean;
  var
    Agrupar: Integer;
  begin
    //True  = J� est� agrupado
    //False = N�o tem agrupamento
    Result := False;
    //
    QrLoc.Close;
    QrLoc.SQL.Clear;
    QrLoc.SQL.Add('SELECT Agrupar');
    QrLoc.SQL.Add('FROM chequeslct');
    QrLoc.SQL.Add('WHERE Ativo = 1');
    UnDmkDAC_PF.AbreQuery(QrLoc, DModG.MyPID_DB);
    QrLoc.First;
    while not QrLoc.Eof do
    begin
      Agrupar := QrLoc.FieldByName('Agrupar').AsInteger;
      if Agrupar <> 0 then
      begin
        Result := True;
        Exit;
      end;
      QrLoc.Next;
    end;
  end;
  function ReabreChequesLctSel(): Integer;
  begin
    QrLoc.Close;
    QrLoc.SQL.Clear;
    QrLoc.SQL.Add('SELECT Controle');
    QrLoc.SQL.Add('FROM chequeslct');
    QrLoc.SQL.Add('WHERE Ativo = 1');
    UnDmkDAC_PF.AbreQuery(QrLoc, DModG.MyPID_DB);
    //
    Result := QrLoc.RecordCount;
    //
    QrLoc.Close;
  end;
  function VerificaFornecedor(Fornecedores, Fornecedor: String): String;
  begin
    if Pos(Fornecedor, Fornecedores) > 0 then
      Result := Fornecedores
    else
      Result := Fornecedores + '; ' + Fornecedor;
  end;
var
  Fornece: String;
  Debito: Double;
  Data: String;
  //i,
  CodAgrupa: Integer;
  DtEmiCh: TDate;
begin
  Fornece := '';
  Debito  := 0;
  //
  if ReabreChequesLctSel > 1 then
  begin
    if not VerificaSeSelNaoEstaAgrupado then
    begin
      //Soma os D�bitos e agrupa os fornecedores
      QrLoc.Close;
      QrLoc.SQL.Clear;
      QrLoc.SQL.Add('SELECT *');
      QrLoc.SQL.Add('FROM chequeslct');
      QrLoc.SQL.Add('WHERE Ativo = 1');
      UnDmkDAC_PF.AbreQuery(QrLoc, DModG.MyPID_DB);
      QrLoc.First;
      while not QrLoc.Eof do
      begin
        if Length(Fornece) > 0 then
        begin
          Fornece := VerificaFornecedor(Fornece, QrLoc.FieldByName('NO_ENT').AsString);
        end else
          Fornece := QrChequesLctNO_ENT.Value;
        //
        Debito := Debito + QrLoc.FieldByName('Debito').AsFloat;
        //
        QrLoc.Next;
      end;
      //Verifica as datas e insere o lan�amento com os totais
      case RGData.ItemIndex of
        0:
          Data := Geral.FDT(QrChequesLctData.Value, 02);
        1:
          Data := Geral.FDT(QrChequesLctVencimento.Value, 02);
        else
          Data := '';
      end;

      //Fazer para caso n�o preencher a data n�o fazer nada

      if InputQuery('Data de emiss�o do cheque', 'Defina a data de emiss�o do cheque:', Data) then
      begin
        if StrToDate(Data) < 2 then
        begin
          Geral.MB_Aviso('Data n�o definida!');
          Exit;
        end else
          DtEmiCh := StrToDate(Data);
          Data := Geral.FDT(DtEmiCh, 01);
          //
          CodAgrupa := InsereTmpLct(Debito, Fornece, Data);
      end else
        Exit;
      //
      if CodAgrupa <> 0 then
      begin
        //Atualiza o campo Agrupar com o controle do lan�amento principal
        QrLoc.Close;
        QrLoc.SQL.Clear;
        QrLoc.SQL.Add('SELECT Controle');
        QrLoc.SQL.Add('FROM chequeslct');
        QrLoc.SQL.Add('WHERE Ativo = 1');
        UnDmkDAC_PF.AbreQuery(QrLoc, DModG.MyPID_DB);
        QrLoc.First;
        while not QrLoc.Eof do
        begin
          AtualizaTmpLct(QrLoc.FieldByName('Controle').AsInteger, CodAgrupa);
          //
          QrLoc.Next;
        end;
        ReopenChequesLct(CodAgrupa);
      end else
        Geral.MB_Aviso('N�o foi poss�vel agrupar os lan�amentos');
    end else
      Geral.MB_Aviso(
      'Voc� deve selecionar somente lan�amentos que ainda n�o tenham sido agrupados!');
  end else
    Geral.MB_Aviso(
    'Voc� deve selecionar mais de um lan�amento para agrupar');
end;

procedure TFmEmiteCheque_4.BtCopiaDocClick(Sender: TObject);
var
  DBG: TDBGrid;
  QrLct: TmySQLQuery;
begin
  case RGLct.ItemIndex of
    1:
    begin
      DBG := FGrade;
      //QrLct := DModFin.QrLcts;
(*   O delphi resolveu n�o obedecer a diretiva de compila��o abaixo!
     Portanto ter� que ser mudado a cada necessidade de compila��o!*)
      {$IFDEF DEFINE_VARLCT}
        QrLct := DModFin.QrLcts;
      {$ELSE}
        QrLct := DmodFin.QrLctos;
      {$ENDIF}
    end;
    2:
    begin
      if QrChequesLct2.RecordCount > 0 then
      begin
        DBGLct2.DataSource.DataSet.First;
        while not DBGLct2.DataSource.DataSet.eof do
        begin
          if QrChequesLct2Ativo.Value = 1 then
            DBGLct2.SelectedRows.CurrentRowSelected := True;
          DBGLct2.DataSource.DataSet.next;
        end;
      end;
      DBG   := TDBGrid(DBGLct2);
      QrLct := QrChequesLct2;
    end;
    else
      Exit;
  end;
  FinanceiroJan.MostraCopiaDoc(QrLct, DBG, FTabLct);
end;

procedure TFmEmiteCheque_4.BtDesagruparClick(Sender: TObject);
var
  Controle, Agrupar: Integer;
begin
  Agrupar := QrChequesLctAgrupar.Value;
  //
  if MyObjects.FIC(Agrupar = 0, nil, 'Este lan�amento n�o est� agrupado!') then Exit;
  //
  QrLoc.Close;
  QrLoc.SQL.Clear;
  QrLoc.SQL.Add('SELECT *');
  QrLoc.SQL.Add('FROM chequeslct');
  QrLoc.SQL.Add('WHERE Agrupar=:P0');
  QrLoc.Params[0].AsInteger := Agrupar;
  UnDmkDAC_PF.AbreQuery(QrLoc, DModG.MyPID_DB);
  QrLoc.First;
  while not QrLoc.Eof do
  begin
    Controle := QrLoc.FieldByName('Controle').AsInteger;
    //
    UMyMod.SQLInsUpd(DModG.QrUpdPID1, stUpd, 'chequeslct', False, ['Ativo', 'Agrupar'],
      ['Controle'], [0, 0], [Controle], False);
    //
    QrLoc.Next;
  end;
  UMyMod.ExcluiRegistros('', DModG.QrUpdPID1, 'chequeslct', ['Controle'],
    ['='], [Agrupar], '');
  //
  ReopenChequesLct(0);
end;

procedure TFmEmiteCheque_4.BtGeraClick(Sender: TObject);
begin
  if MyObjects.FIC(RGLct.ItemIndex < 1, RGLct,
  'Informe o item "Lan�amentos"!') then Exit;
  //
  if MyObjects.FIC(CkRegerar.Checked and (EdDocumento.ValueVariant = 0), nil,
  'Informe o n�mero do primeiro cheque!') then Exit;
  //
  if MyObjects.FIC(EdChConfig.ValueVariant = 0, EdChConfig,
  'Informe a configura��o de impress�o!') then Exit;
  //
{
  case RGLct.itemIndex of
    1: SelecionaItens();
    2: PreparaDados();
  end;
}
    PreparaDados();
end;

procedure TFmEmiteCheque_4.BtNenhumClick(Sender: TObject);
begin
  AtivarTodos(0);
end;

procedure TFmEmiteCheque_4.BtRefreshClick(Sender: TObject);
begin
  SelecionaItens();
end;

procedure TFmEmiteCheque_4.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmEmiteCheque_4.BtTodosClick(Sender: TObject);
begin
  AtivarTodos(1);
end;

procedure TFmEmiteCheque_4.Button1Click(Sender: TObject);
var
  Texto: string;
  F: TextFile;
  I, J, K: Integer;
begin
  AssignFile(F, EdPorta.Text);
  Rewrite(F);
  try
    I := EdCarIni.ValueVariant;
    while I <= EdCarFim.ValueVariant do
    begin
      Texto := '';
      J := I;
      K := 1;
      while (J < EdCarFim.ValueVariant) and (K < 11) do
      begin
        Texto := Texto + FormatFloat('000', J) + ' - ' + Char(J) + ' ';
        //
        K := K + 1;
        J := J + 1;
      end;
      WriteLn(F, Texto);
      I := J;
    end;
  finally
    CloseFile(F);
  end;
end;

procedure TFmEmiteCheque_4.Button2Click(Sender: TObject);
var
  F: TextFile;
  K, I: Integer;
begin
  AssignFile(F, EdPorta.Text);
  Rewrite(F);
  try
    K :=  Trunc(EdReverso.ValueVariant / FAltLin);
    //  Reverso: N�o funciona na LX 300
    Write(F, #27+#106+ Char(K));
    //
    for I := 1 to 5 do
    begin
      WriteLn(F, '/' + #8 + '\' + '/' + #8 + '\' + '/' + #8 + '\' + '/' + #8 + '\' + '/' + #8 + '\');
    end;
    WriteLn(F, #12);
  finally
    CloseFile(F);
  end;
end;

procedure TFmEmiteCheque_4.Button3Click(Sender: TObject);
var
  Texto: string;
  F: TextFile;
  I, J: Integer;
begin
  AssignFile(F, EdPorta.Text);
  Rewrite(F);
  try
    for I := 1 to Grade.RowCount - 1 do
    begin
      Texto := '';
      for J := 01to Grade.ColCount - 1 do
      begin
        if Grade.Cells[J,I]  <> '' then
          Texto := Texto + Char(Geral.IMV(Grade.Cells[J,I]));
      end;
      if Texto <> '' then
        WriteLn(F, Texto);
    end;
  finally
    CloseFile(F);
  end;
end;

procedure TFmEmiteCheque_4.CkFiltroLctClick(Sender: TObject);
begin
  if CkFiltroLct.Visible = True then
    SelecionaItens();
end;

procedure TFmEmiteCheque_4.CkReGerarClick(Sender: TObject);
begin
  CBRegerar.Visible := CkRegerar.Checked;
end;

procedure TFmEmiteCheque_4.ConfiguraExtenso(const Valor: String; var Linha1, Linha2: String);
var
  Texto: String;
  Letras1, Letras2: Integer;
begin
  Letras1 := FLetras1;
  Letras2 := FLetras2;
  QrChConfVal.First;
  Texto := dmkPF.ExtensoMoney(Geral.TFT(Valor, 2, siPositivo));
  ////////////////////////////////////////////////////////////////////////////
  if Length(Texto) > Letras1 then
  begin
    while (Texto[Letras1] <> ' ') and (Letras1 > 1) do Letras1 := Letras1 -1;
    Linha1 := Copy(Texto, 1, Letras1);
    Linha2 := Copy(Texto, Letras1+1, Length(Texto));
  end else begin
    Linha1 := Texto;
    Linha2 := '';
  end;
  if (Length(Linha1) < Letras1) and (Length(Linha2) = 0) then
  begin
    Linha1 := Linha1 + ' ';
    while Length(Linha1) < Letras1 do Linha1 := Linha1 + 'x';
  end;
  if Length(Linha2) < Letras2  then Linha2 := Linha2 + ' ';
  while Length(Linha2) < Letras2 do Linha2 := Linha2 + 'x';
end;

procedure TFmEmiteCheque_4.ConfiguraTamEdits();
var
  CPI: Integer;
begin
  FLetras1 := 0;
  FLetras2 := 0;
  QrChConfVal.First;
  ////////////////////////////////////////////////////////////////////////////
  CPI := (QrChConfigCabCPI.Value * 10) + QrChConfValFTam.Value;
  case CPI of
    100: FLetras1 := 17;
    101: FLetras1 := 10;
    102: FLetras1 := 05;
    120: FLetras1 := 20;
    121: FLetras1 := 12;
    122: FLetras1 := 06;
    else FLetras1 := 0;
  end;
  FLetras1 := Trunc(FLetras1 * (QrChConfValLarg.Value /(100 * CO_POLEGADA)));
  {
  case QrChConfValFTam.Value of
    0: EdExtenso1.Width := (FLetras1 * VAR_WIDSizeCompri) + VAR_MESSizeCompri;
    1: EdExtenso1.Width := (FLetras1 * VAR_WIDSizeNormal) + VAR_MESSizeNormal;
    2: EdExtenso1.Width := (FLetras1 * VAR_WIDSizeExpand) + VAR_MESSizeExpand;
  end;
  case QrChConfValFTam.Value of
    0: EdExtenso1.Height := VAR_HEISizeCompri;
    1: EdExtenso1.Height := VAR_HEISizeNormal;
    2: EdExtenso1.Height := VAR_HEISizeExpand;
  end;
  case QrChConfValFTam.Value of
    0: EdExtenso1.Font.Size := VAR_IMPSizeCompri;
    1: EdExtenso1.Font.Size := VAR_IMPSizeNormal;
    2: EdExtenso1.Font.Size := VAR_IMPSizeExpand;
  end;
  }
  ////////////////////////////////////////////////////////////////////////////
  QrChConfVal.Next;
  CPI := (QrChConfigCabCPI.Value * 10) + QrChConfValFTam.Value;
  case CPI of
    100: FLetras2 := 17;
    101: FLetras2 := 10;
    102: FLetras2 := 05;
    120: FLetras2 := 20;
    121: FLetras2 := 12;
    122: FLetras2 := 06;
    else FLetras2 := 0;
  end;
  FLetras2 := Trunc(FLetras2 * (QrChConfValLarg.Value /(100 * CO_POLEGADA)));
  {
  case QrChConfValFTam.Value of
    0: EdExtenso2.Width := (FLetras2 * VAR_WIDSizeCompri) + VAR_MESSizeCompri;
    1: EdExtenso2.Width := (FLetras2 * VAR_WIDSizeNormal) + VAR_MESSizeNormal;
    2: EdExtenso2.Width := (FLetras2 * VAR_WIDSizeExpand) + VAR_MESSizeExpand;
  end;
  case QrChConfValFTam.Value of
    0: EdExtenso2.Height := VAR_HEISizeCompri;
    1: EdExtenso2.Height := VAR_HEISizeNormal;
    2: EdExtenso2.Height := VAR_HEISizeExpand;
  end;
  case QrChConfValFTam.Value of
    0: EdExtenso2.Font.Size := VAR_IMPSizeCompri;
    1: EdExtenso2.Font.Size := VAR_IMPSizeNormal;
    2: EdExtenso2.Font.Size := VAR_IMPSizeExpand;
  end;
  }
  ////////////////////////////////////////////////////////////////////////////
end;

procedure TFmEmiteCheque_4.DBGLctCellClick(Column: TColumn);
var
  Ativo, Controle, Agrupar: Integer;
begin
  if Column.FieldName = 'Ativo' then
  begin
    Ativo := QrChequesLctAtivo.Value;
    if Ativo = 0 then
      Ativo := 1
    else
      Ativo := 0;
    //
    Controle := QrChequesLctControle.Value;
    Agrupar  := QrChequesLctAgrupar.Value;
    //
    DmodG.QrUpdPID1.SQL.Clear;
    DmodG.QrUpdPID1.SQL.Add('UPDATE chequeslct SET Ativo=:P0');
    DmodG.QrUpdPID1.SQL.Add('WHERE Controle=:P1');
    DmodG.QrUpdPID1.Params[00].AsInteger := Ativo;
    DmodG.QrUpdPID1.Params[01].AsInteger := Controle;
    DmodG.QrUpdPID1.ExecSQL;
    //
    if Agrupar <> 0 then
    begin
      DmodG.QrUpdPID1.SQL.Clear;
      DmodG.QrUpdPID1.SQL.Add('UPDATE chequeslct SET Ativo=:P0');
      DmodG.QrUpdPID1.SQL.Add('WHERE Agrupar=:P1 AND Agrupar <> Controle');
      DmodG.QrUpdPID1.Params[00].AsInteger := Ativo;
      DmodG.QrUpdPID1.Params[01].AsInteger := Agrupar;
      DmodG.QrUpdPID1.ExecSQL;
    end;
    ReopenChequesLct(Controle);
  end;
end;

procedure TFmEmiteCheque_4.DefinePadroes();
begin
  QrIni.Close;
  QrIni.Params[0].AsInteger := QrChConfigCabCodigo.Value;
  UnDmkDAC_PF.AbreQuery(QrIni, Dmod.MyDB);
  //
  while not QrIni.Eof do
  begin
    case QrIniCampo.Value of
      01: ;//EdValor.Text := QrIniPadr.Value;
      02: ;//EdExtenso1.Text := QrIniPadr.Value;
      03: ;//EdExtenso2.Text := QrIniPadr.Value;
      04: ;//EdFavorecido.Text := QrIniPadr.Value;
      05: ;//dia := QrIniPadr.Value;
      06: ;//mes := QrIniPadr.Value;
      07: ;//ano := QrIniPadr.Value;
      08: ;//BomPara.Text := QrIniPadr.Value;
      09: ;//EdObs.Text := QrIniPadr.Value;
      10: EdCidade.Text := QrIniPadr.Value;
    end;
    QrIni.Next;
  end;
  if EdCidade.Text = '' then EdCidade.Text := VAR_CIDADEPADRAO;
end;

procedure TFmEmiteCheque_4.EdChConfigChange(Sender: TObject);
begin
  DefinePadroes();
end;

procedure TFmEmiteCheque_4.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmEmiteCheque_4.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  QrChequesLct.Database := DModG.MyPID_DB;
  QrChequesImp.Database := DModG.MyPID_DB;
  QrLoc.Database        := DModG.MyPID_DB;
  //
  ReopenChequesImp();
  PageControl1.ActivePageIndex := 0;
  BtCopiaDoc.Enabled           := False;
{
  if QrChequesImp.RecordCount = 0 then
    PageControl1.ActivePageIndex := 0
  else
    PageControl1.ActivePageIndex := 1;
}
  PCLct.Visible        := False;
  TabSheet3.TabVisible := False;
  TabSheet5.TabVisible := False;
  TabSheet7.TabVisible := False;
  RGLctsCfg.Visible := False;
  CkFiltroLct.Visible  := False;
  //
  UnDmkDAC_PF.AbreQuery(QrChConfigCab, Dmod.MyDB);
  if QrChConfigCab.RecordCount = 1 then
  begin
    EdChConfig.ValueVariant := QrChConfigCabCodigo.Value;
    CBChConfig.KeyValue     := QrChConfigCabCodigo.Value;
    //
    DefinePadroes();
  end;
end;

procedure TFmEmiteCheque_4.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmEmiteCheque_4.PageControl1Change(Sender: TObject);
begin
  GBRodaPe.Visible := PageControl1.ActivePageIndex <> 1;
end;

function TFmEmiteCheque_4.PreparaDados(): Boolean;
  procedure IncluiCheque(Qry: TmySQLQuery; Seq: Integer);
  var
    ChSerie, Valor, Extenso1, Extenso2, Favorecido, Cidade,
    Dia, Mes, Ano, BomPara, Observacao: String;
    Codigo, CPI, TopoIni, Altura, MEsq, Controle, Sub, ChGrava: Integer;
    Debito, ChNumero: Double;
    Data, Vcto: TDateTime;
    D, M, A: Word;
  begin
    PB1.Position := PB1.Position + 1;
    Controle     := Qry.FieldByName('Controle' ).AsInteger;
    Sub          := Qry.FieldByName('Sub'      ).AsInteger;
    if CkReGerar.Checked then
    begin
      //ChNumero   := Seq + EdDocumento.ValueVariant; Estava pulando 1 => Alterado em: 13/06/2013
      ChNumero   := Seq + EdDocumento.ValueVariant - 1;
      ChSerie    := EdSerieCh.Text;
      ChGrava    := 1;
    end else begin
      ChNumero   := Qry.FieldByName('Documento').AsFloat;
      ChSerie    := Qry.FieldByName('SerieCh'  ).AsString;
      ChGrava    := 0;
    end;
    Debito     := Qry.FieldByName('Debito'   ).AsFloat;
    Valor      := FormatFloat('#,###,###,###,##0.00', Debito);
    ConfiguraExtenso(Valor, Extenso1, Extenso2);
    //Favorecido := Qry.FieldByName(FNameFldFrn).AsString;
    if CkNominal.Checked then
      Favorecido := Qry.FieldByName(FNameFldFrn).AsString
    else
      Favorecido := '';
    Cidade     := EdCidade.Text;
    case RGData.ItemIndex of
      0: Data     := Qry.FieldByName('Data').AsDateTime;
      1: Data     := Qry.FieldByName('Vencimento').AsDateTime;
      else Data  := Int(Date);
    end;

    DecodeDate(Data, A, M, D);
    Dia        := FormatFloat('0', D);
    Mes        := dmkPF.VerificaMes(M, False);
    Ano        := FormatFloat('0', A);
    Vcto       := Qry.FieldByName('Vencimento').AsDateTime;
    case RGBomPara.ItemIndex of
      0: BomPara := '';
      1: if Vcto > Int(Date) then BomPara := Geral.FDT(Vcto, 2);
      2: BomPara := Geral.FDT(Vcto, 2);
    end;
    // n�o precisa! j� tem no sufixo e prefixo
    //if BomPara <> '' then BomPara := 'Bom para: ' + BomPara;
    Observacao := '';
    Codigo     := QrChConfigCabCodigo.Value;
    CPI        := QrChConfigCabCPI.Value;
    TopoIni    := QrChConfigCabTopoIni.Value;
    Altura     := QrChConfigCabAltura.Value;
    MEsq       := QrChConfigCabMEsq.Value;
    //
    if Favorecido = '[ND]' then Favorecido := '';
    //
    UMyMod.SQLInsUpd(DmodG.QrUpdPID1, stIns, 'chequesimp', False, [
    'Controle', 'Sub', 'ChNumero',
    'ChSerie', 'Valor', 'Extenso1',
    'Extenso2', 'Favorecido', 'Cidade',
    'Dia', 'Mes', 'Ano',
    'BomPara', 'Observacao', 'Seq',
    'Codigo', 'CPI', 'TopoIni',
    'Altura', 'MEsq', 'ChGrava'], [
    ], [
    Controle, Sub, ChNumero,
    ChSerie, Valor, Extenso1,
    Extenso2, Favorecido, Cidade,
    Dia, Mes, Ano,
    BomPara, Observacao, Seq + 1,
    Codigo, CPI, TopoIni,
    Altura, MEsq, ChGrava], [
    ], False);
  end;
var
  I: Integer;
  Qry: TmySQLQuery;
begin
  Screen.Cursor := crHourGlass;
  PB1.Position := 0;
  PB1.Max      := 1;
  try
    UCriar.RecriaTempTable('ChequesImp', DModG.QrUpdPID1, False);
    case RGLct.ItemIndex of
      1:
      begin
        FNameFldFrn := FCampoNomeFornecedor;
        if FGrade <> nil then
        begin
          if FGrade.DataSource.DataSet <> nil then
          begin
            Qry := TmySQLQuery(FGrade.DataSource.DataSet);
            if FGrade.SelectedRows.Count > 1 then
            begin
              PB1.Max := FGrade.SelectedRows.Count;
              with FGrade.DataSource.DataSet do
              for i:= 0 to FGrade.SelectedRows.Count-1 do
              begin
                //GotoBookmark(FGrade.SelectedRows.Items[i]);
                GotoBookmark(FGrade.SelectedRows.Items[i]);
                IncluiCheque(Qry, I);
              end;
            end else IncluiCheque(Qry, 0);
            ReopenChequesImp();
            PageControl1.ActivePageIndex := 1;
            GBRodaPe.Visible             := False;
            Result := True;
          end else Geral.MB_Erro('A query da grade n�o est� definida!');
        end else Geral.MB_Erro('A grade n�o est� definida!');
      end;
      2:
      begin
        FNameFldFrn := 'NO_ENT';
        I := 0;
        PB1.Max := QrChequesLct.RecordCount;
        QrChequesLct.DisableControls;
        try
          QrChequesLct.First;
          while not QrChequesLct.Eof do
          begin
            if QrChequesLctAtivo.Value = 1 then
            begin
              I := I + 1;
              IncluiCheque(QrChequesLct, I);
            end else PB1.Position := PB1.Position + 1;
            //
            QrChequesLct.Next;
          end;
          if I > 0 then
          begin
            ReopenChequesImp();
            PageControl1.ActivePageIndex := 1;
            GBRodaPe.Visible             := False;
            Result := True;
          end else Geral.MB_Aviso('Nenhum lan�amento foi selecionado!');
        finally
          QrChequesLct.EnableControls;
        end;
      end;
    end;
    Result := True;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmEmiteCheque_4.QrChConfigCabAfterOpen(DataSet: TDataSet);
begin
  ReopenChConfVal();
end;

procedure TFmEmiteCheque_4.QrChequesLctAfterScroll(DataSet: TDataSet);
var
  Agrupado: Boolean;
begin
  Agrupado := QrChequesLctAgrupar.Value > 0;
  //
  BtAgrupar.Enabled    := not Agrupado;
  BtDesagrupar.Enabled := Agrupado;
end;

procedure TFmEmiteCheque_4.QrChequesLctBeforeClose(DataSet: TDataSet);
begin
  BtAgrupar.Enabled    := False;
  BtDesagrupar.Enabled := False;
{
  DBGLct.Visible := False;
  PnDefAll.Visible := False;
}
end;

procedure TFmEmiteCheque_4.ReopenChConfVal;
begin
  QrChConfVal.Close;
  QrChConfVal.Params[0].AsInteger := QrChConfigCabCodigo.Value;
  UnDmkDAC_PF.AbreQuery(QrChConfVal, Dmod.MyDB);
  //
  ConfiguraTamEdits();
end;

procedure TFmEmiteCheque_4.ReopenChequesImp();
begin
  try
    QrChequesImp.Close;
    UnDmkDAC_PF.AbreQuery(QrChequesImp, DModG.MyPID_DB);
  except
    UCriar.RecriaTempTable('ChequesImp', DModG.QrUpdPID1, False);
    raise;
  end;
end;

procedure TFmEmiteCheque_4.ReopenChequesLct(Controle: Integer);
begin
  try
    UnDmkDAC_PF.AbreMySQLQuery0(QrChequesLct, DModG.MyPID_DB, [
      'SELECT *, IF(Agrupar > 0, "SIM", "N�O") AGRUPADO_TXT ',
      'FROM chequeslct ',
      'WHERE Agrupar = 0 OR Controle = Agrupar',
    '']);
    if Controle <> 0 then
      QrChequesLct.Locate('Controle', Controle, []);
    //
    //Por causa da c�pia cheque
    UnDmkDAC_PF.AbreMySQLQuery0(QrChequesLct2, DModG.MyPID_DB, [
      'SELECT *, IF(Agrupar > 0, "SIM", "N�O") AGRUPADO_TXT ',
      'FROM chequeslct ',
      'WHERE Controle <> Agrupar',
    '']);
  except
    UCriar.RecriaTempTable('ChequesLct', DModG.QrUpdPID1, False);
    raise;
  end;
end;

procedure TFmEmiteCheque_4.RGLctClick(Sender: TObject);
begin
  PCLct.Visible    := False;
  PnDefAll.Visible := False;
  //
  BtAgrupar.Enabled    := False;
  BtDesagrupar.Enabled := False;
  BtNenhum.Enabled     := False;
  BtTodos.Enabled      := False;
  RGLctsCfg.Visible := False;
  CkFiltroLct.Visible  := False;
  CkFiltroLct.Checked  := False;
  //
  case RGLct.ItemIndex of
    1:
    begin
      BtGera.Enabled := True;
    end;
    2:
    begin
      RGLctsCfg.Visible := True;
      CkFiltroLct.Visible  := True;
      CkFiltroLct.Checked  := True;
      SelecionaItens();
    end
    else
    begin
      BtGera.Enabled := False;
    end;
  end;
end;

procedure TFmEmiteCheque_4.SelecionaItens();
var
  Ini, Fim, Campo, TabLct: String;
begin
  TabLct := DmodG.ObtemLctA_Para_Web(FTabLct); //Remove o nome do banco de dados se tiver
  //
  QrCart.Close;
  QrCart.Params[0].AsInteger := FCarteira;
  UnDmkDAC_PF.AbreQuery(QrCart, Dmod.MyDB);
  if MyObjects.FIC((QrCartTipo.Value <> 2) or (QrCartTipoDoc.Value <> 1), nil,
    'Para imprimir cheques "indefinidos" a carteira deve ser uma "Emiss�o" de "Cheques".') then Exit;
  //
  FChequesLct := UCriar.RecriaTempTable('ChequesLct', DModG.QrUpdPID1, False);
  //
  QrChequesLct.Close;
  QrChequesLct.SQL.Clear;
  QrChequesLct.SQL.Add('INSERT INTO chequeslct');
  QrChequesLct.SQL.Add('SELECT 1 Ativo, lan.Data, lan.Controle, lan.Genero, ');
  QrChequesLct.SQL.Add('gen.Nome NO_CONTA, lan.Descricao, lan.Debito, ');
  QrChequesLct.SQL.Add('lan.Vencimento, lan.Fornecedor, IF(ent.Codigo=0, "",');
  QrChequesLct.SQL.Add('IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome)) NO_ENT, ');
  QrChequesLct.SQL.Add('lan.Sub, lan.Documento, lan.SerieCH, 0 Agrupar ');
  QrChequesLct.SQL.Add('FROM ' + TMeuDB + '.' + TabLct + ' lan');
  QrChequesLct.SQL.Add('LEFT JOIN '+TMeuDB+'.entidades ent ON ent.Codigo=lan.Fornecedor');
  QrChequesLct.SQL.Add('LEFT JOIN '+TMeuDB+'.contas    gen ON gen.Codigo=lan.Genero');
  QrChequesLct.SQL.Add('WHERE Carteira=:P0');
  //
  if (CkFiltroLct.Visible) and (CkFiltroLct.Checked) then
  begin
    Ini := FormatDateTime(VAR_FORMATDATE, TPDataIni.Date);
    Fim := FormatDateTime(VAR_FORMATDATE, TPDataFim.Date);
    //  
    case RGTipoData.ItemIndex of
        1: Campo := 'lan.Vencimento';
        2: Campo := 'lan.Compensado';
      else Campo := 'lan.Data';
    end;
    QrChequesLct.SQL.Add('AND ' + Campo + ' BETWEEN "' + Ini + '" AND "' + Fim + '"');
  end;
  QrChequesLct.SQL.Add('AND lan.Documento=0');
  QrChequesLct.SQL.Add('ORDER BY lan.Data, lan.Controle;');
  //QrChequesLct.SQL.Add('SELECT * FROM chequeslct;');
  QrChequesLct.Params[0].AsInteger := FCarteira;
  QrChequesLct.ExecSQL;
  ReopenChequesLct(0);
  //
  if QrChequesLct.RecordCount > 0 then
  begin
    PCLct.Visible    := True;
    PnDefAll.Visible := True;
    BtGera.Enabled   := True;
    //
    BtAgrupar.Enabled    := True;
    BtDesagrupar.Enabled := True;
    BtTodos.Enabled      := True;
    BtNenhum.Enabled     := True;
  end else
    Geral.MB_Aviso('N�o foi localizado cheque a ser impresso!');
end;

procedure TFmEmiteCheque_4.SpeedButton1Click(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  if DBCheck.CriaFm(TFmChConfigCab, FmChConfigCab, afmoNegarComAviso) then
  begin
    FmChConfigCab.ShowModal;
    FmChConfigCab.Destroy;
    //
    UMyMod.SetaCodigoPesquisado(EdChConfig, CBChConfig, QrChConfigCab, VAR_CADASTRO);
  end;
end;

procedure TFmEmiteCheque_4.ImprimeChequeAtual(ChAtu, ChMax, ChConfCab, CPI, TopoIni,
  Altura, MEsq: Integer; Dia, Mes, Ano, Vcto, Valor, Favorecido, Extenso1,
  Extenso2, Obs, Cidade: String);
var
  {
      Anulado 2010-07-11
  LinhaAtual: Integer;
  }
  MyCPI, Texto, Espaco, Linha, FormataA, FormataI, FormataNI, FormataNF: String;
  PA: Integer;
begin
  Screen.Cursor := crHourGlass;
  try
    PA := Altura * (ChAtu - 1);
  {
      Anulado 2010-07-11
    LinhaAtual := 0;
}
    if ChAtu = 1 then
    begin
      //AssignFile(FArqPrn, 'LPT1:');
      AssignFile(FArqPrn, EdPorta.Text);
      ReWrite(FArqPrn);
      // Configura��o padrao (inicializa)
      Write(FArqPrn, #27+#64);
      if CkForca.Checked then
      begin
        //Seleciona modo de caracteres ao inv�s de modo gr�fico
        Write(FArqPrn, #27+#116+#48); //ESC t 0
        //Seleciona a tabela de caracteres BRASCII (~�`�,...)
        Write(FArqPrn, #27+#40+#116+#3+#0+#48+#25+#0); //ESC ( t 3 0 48 25 0 BRASCII
        //N�o serve
        //Write(FArqPrn, #27+#40+#116+#3+#0+#48+#19+#26); //ESC ( t 3 0 48 19 26 ISO8859-1 (Latin 1)
      end;
      // Fonte Draft
      Write(FArqPrn, #27+#120+#48);
      // Posi��o inicial do carro
      Write(FArqPrn, #13);
      if CPI = 12 then MyCPI := 'M' else MyCPI := 'P';
      Write(FArqPrn, #27+MyCPI);
      FormataA  := #15;
      FormataI  := #15;
      FormataNI := #15;
      FormataNF := #15;
      //
      //
      {
      Anulado 2010-07-11
      if TopoIni < 0 then LinhaAtual := LinhaAtual - TopoIni else
        LinhaAtual := AvancaCarro(LinhaAtual, TopoIni);
      }
      FPosIni := TopoIni;
      //AvancaCarro2(TopoIni + PA);
    end;
    //////////////////////////////////////////////////////////////////////////
    QrTopos1.Close;
    QrTopos1.Params[0].AsInteger := ChConfCab;
    UnDmkDAC_PF.AbreQuery(QrTopos1, Dmod.MyDB);
    QrTopos1.First;
    while not QrTopos1.Eof do
    begin
      {
      Anulado 2010-07-11
      LinhaAtual := AvancaCarro(LinhaAtual, QrTopos1TopR.Value);
      }
      AvancaCarro2(QrTopos1TopR.Value + PA);
      if not FPrintedLine then
      begin
        QrView1.Close;
        QrView1.Params[0].AsInteger := ChConfCab;
        QrView1.Params[1].AsInteger := QrTopos1TopR.Value;
        UnDmkDAC_PF.AbreQuery(QrView1, Dmod.MyDB);
        QrView1.First;
        Linha := '';
        FTamLinha  := MEsq;
        FFoiExpand := 0;
        while not QrView1.Eof do
          begin
          case QrView1FTam.Value of
            0: FormataA := #15;
            1: FormataA := #18;
            2: FormataA := #18#14;
          end;
          Espaco := EspacoEntreTextos(QrView1, CPI);
          //if QrView1Nulo.Value = 1 then Nulo := True else Nulo := False;
          //
          case QrView1Campo.Value of
            01: Texto := Valor;
            02: Texto := Extenso1;
            03: Texto := Extenso2;
            04: Texto := Favorecido;
            05: Texto := Dia;
            06: Texto := Mes;
            07: Texto := Ano;
            08: Texto := Vcto;
            09: Texto := Obs;
            10: Texto := Cidade;
            else Texto := '';
          end;
          Texto := FormataTexto(Texto, QrView1Pref.Value, QrView1Sufi.Value,
          '', QrView1PrEs.Value, 0, QrView1AliH.Value, QrView1Larg.Value,
          QrView1FTam.Value, 0(*Texto*) , CPI, True(*Nulo*));
          Linha := Linha + Espaco + FormataA + Texto;
          // J� usou expandido na mesma linha
          if QrView1FTam.Value = 2 then FFoiExpand := 100;
          QrView1.Next;
        end;
        Write(FarqPrn, #27+'3'+#0);
        //Write(FarqPrn, #27+'F');
        WriteLn(FArqPrn, Linha);
        FPrintedLine := True;
      end;
      QrTopos1.Next;
    end;
    {
    Anulado 2010-07-11
    AvancaCarro(LinhaAtual, Altura);
    }
    AvancaCarro2(Altura + PA);
    Memo1.Lines.Add('------------------------------------------------------------');
    if ChAtu = ChMax then
    begin
      Write(FArqPrn, #13);
      WriteLn(FArqPrn, #27+'0');
      //if CkEjeta.Checked then
      //Write(FArqPrn, #12);
      // Configura��o padrao (inicializa)
      Write(FArqPrn, #27+#64);
      CloseFile(FArqPrn);
    end;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmEmiteCheque_4.InfoAvanco(Tipo: Char; LinRun, NeedPos: Integer);
  function FI(I: Integer): String;
  begin
    Result := Geral.TFD(FormatFloat('0', I), 11, siPositivo);
  end;
  function FF(D: Double): String;
  begin
    Result := Geral.TFD(Geral.FFT(D, 3, siNegativo), 11, siPositivo);
  end;
var
  LA: Integer;
  PA: Double;
begin
  if Tipo = 'I' then
  begin
    Memo1.Lines.Add('-' + ' ' + '    Lin Ini' + ' ' + '    Pos Ini' + ' ' +
      '    Lin Atu' + ' ' + '    Pos Atu' + ' ' + '   Need Pos');
    Memo1.Lines.Add('------------------------------------------------------------');
  end;
  LA := FLinAtu;
  PA := FPosAtu;
  FLinAtu := FLinAtu + LinRun;
  FPosAtu := FPosAtu + (LinRun / 2.16 * CO_POLEGADA);

  //

  Memo1.Lines.Add(Tipo + ' ' +
    FI(LA) + ' ' +
    FF(PA) + ' ' +
    FF(FLinAtu) + ' ' +
    FF(FPosAtu) + ' ' +
    FI(NeedPos)
    );
end;

procedure TFmEmiteCheque_4.AtivarTodos(Ativo: Integer);
var
  Controle: Integer;
begin
  Screen.Cursor := crHourGlass;
  try
    Controle := QrChequesLctControle.Value;
    DmodG.QrUpdPID1.Close;
    DmodG.QrUpdPID1.SQL.Clear;
    DmodG.QrUpdPID1.SQL.Add('UPDATE ' + FChequesLct + ' SET ');
    DmodG.QrUpdPID1.SQL.Add('Ativo=:P0');
    DmodG.QrUpdPID1.Params[00].AsInteger := Ativo;
    DmodG.QrUpdPID1.ExecSQL;
    //
    ReopenChequesLct(Controle);
  finally
    Screen.Cursor := crDefault;
  end;
end;

{
function TFmEmiteCheque_4.AvancaCarro(PosicaoAtual, NovaPosicao: Integer): Integer;
var
  LinAtu: Integer;
  Tipo: Char;
begin
  Tipo := 'T';
  if PosicaoAtual < NovaPosicao then
  begin
    LinAtu := Trunc((NovaPosicao - PosicaoAtual) / FAltLin);
    while LinAtu > 0 do
    begin
      if LinAtu > 216 then
      begin
        Tipo := 'F';
        Write(FArqPrn, #27+'3'+#216);
        LinAtu := LinAtu - 216;
        WriteLn(FArqPrn,' ');
      end else begin
        Write(FArqPrn, MyPrinters.DefAlturaDotImp(3, LinAtu, ''));
        WriteLn(FArqPrn,' ');
        LinAtu := 0;
      end;
    end;
  end;
  Result := NovaPosicao;
  FPrintedLine := False;
end;
}

function TFmEmiteCheque_4.AvancaCarro2(NovaPosicao: Integer): Boolean;
var
  LinAtu: Integer;
  Tipo: Char;
begin
  Tipo := 'T';
  if FPosAtu = 0 then
  begin
    LinAtu  := Trunc(FPosIni / FAltLin);
    InfoAvanco('I', LinAtu, NovaPosicao);
  end;
  if FPosAtu < NovaPosicao then
  begin
    LinAtu := Trunc((NovaPosicao - FPosAtu) / FAltLin);
    while LinAtu > 0 do
    begin
      if LinAtu > 216 then
      begin
        InfoAvanco('P', 216, NovaPosicao);
        Tipo := 'F';
        Write(FArqPrn, #27+'3'+#216);
        LinAtu := LinAtu - 216;
        WriteLn(FArqPrn,' ');
      end else begin
        InfoAvanco(Tipo, LinAtu, NovaPosicao);
        Write(FArqPrn, MyPrinters.DefAlturaDotImp(3, LinAtu, ''));
        WriteLn(FArqPrn,' ');
        LinAtu := 0;
      end;
    end;
  end;
  Result := True;
  FPrintedLine := False;
end;

function TFmEmiteCheque_4.FormataTexto(Texto, Prefixo, Sufixo, Substituicao: String;
  EspacosPrefixo, Substitui, Alinhamento, Comprimento, Fonte, Formatacao, CPI:
  Integer; Nulo: Boolean): String;
var
  i, Tam, Letras, MyCPI, Vazios: Integer;
  Esq: Boolean;
  Txt, Vaz: String;
  Increm: Double;
begin
  if Substitui = 1 then Txt := Substituicao else
  begin
    if Texto = '' then Txt := ''
    else Txt := Formata(Texto, Prefixo, Sufixo, EspacosPrefixo, Formatacao, Nulo);
  end;
  if Txt = '' then
  Txt := ' '; // evitar erro de Margem Esquerda ao imprimir ??
  if CPI = 0 then MyCPI := 10 else MyCPI := CPI;
  Tam := Fonte + (MyCPI*10);
  Letras := 0;
  case Tam of
    100: Letras := 17;
    101: Letras := 10;
    102: Letras := 05;
    120: Letras := 20;
    121: Letras := 12;
    122: Letras := 06;
    else Geral.MB_Aviso('CPI indefinido!');
  end;
  Increm := (CO_POLEGADA * 100) / Letras;
  Letras := Trunc(((Comprimento / 100) / CO_POLEGADA) * Letras);
  FTamLinha := FTamLinha + Trunc(Increm * Letras);
  Vazios := Letras - Length(Txt);
  Esq := True;
  // [0: Esq] [1: Centro] [2: Direita]
  if Alinhamento = 1 then
  begin
    for i := 1 to Vazios do
    begin
      if Esq then
      begin
        Txt := Txt + ' ';
        Esq := False;
      end else begin
        Txt := ' ' + Txt;
        Esq := True;
      end;
    end;
  end else begin
    for i := 1 to Vazios do Vaz := Vaz + ' ';
    if Alinhamento = 0 then Txt := Txt + Vaz else Txt := Vaz + Txt;
  end;
  Result := Txt;
end;

function TFmEmiteCheque_4.EspacoEntreTextos(QrView: TmySQLQuery; CPI: Integer): String;
var
  Texto: String;
  i, MyCPI, Letras: Integer;
  Soma: Double;
begin
  Texto := '';
  if FTamLinha > QrView.FieldByName('MEsq').AsInteger then
  begin
    // Erro !!!
  end;
  if FTamLinha < QrView.FieldByName('MEsq').AsInteger then
  begin
    Texto := #15;
    if CPI = 0 then MyCPI := 10 else MyCPI := CPI;
    MyCPI := MyCPI + FFoiExpand;
    case MyCPI of
     010: MyCPI := 17;
     012: MyCPI := 20;
     110: MyCPI := 10;
     112: MyCPI := 10;
    end;
    Soma := (CO_POLEGADA * 100)/ MyCPI;
    Letras := Trunc((QrView.FieldByName('MEsq').AsInteger - FTamLinha) /
      (CO_POLEGADA * 100) * MyCPI);
    for i := 1 to Letras do Texto := Texto + ' ';
    FTamLinha := FTamLinha + Trunc(Soma * Letras);
  end;
  Result := Texto;
end;

function TFmEmiteCheque_4.Formata(Texto, Prefixo, Sufixo: String; EspacosPrefixo,
 Formato: Integer; Nulo: Boolean): String;
var
  MeuTexto, MeuPrefixo: String;
  i: Integer;
begin
  if Trim(Texto) = '' then MeuTexto := Texto else
  begin
    case Formato of
      1: MeuTexto := Geral.TFT(Texto, 0, siNegativo);
      2: MeuTexto := Geral.TFT(Texto, 1, siNegativo);
      3: MeuTexto := Geral.TFT(Texto, 2, siNegativo);
      4: MeuTexto := Geral.TFT(Texto, 3, siNegativo);
      5: MeuTexto := Geral.TFT(Texto, 4, siNegativo);
      6: MeuTexto := FormatDateTime(VAR_FORMATDATE3, StrToDate(Texto));
      7: MeuTexto := FormatDateTime(VAR_FORMATDATE2, StrToDate(Texto));
      8: MeuTexto := FormatDateTime(VAR_FORMATTIME2, StrToTime(Texto));
      9: MeuTexto := FormatDateTime(VAR_FORMATTIME , StrToTime(Texto));
      else MeuTexto := Texto;
    end;
  end;
  if (Formato in ([1,2,3,4,5])) and Nulo then
  begin
    if Geral.DMV(MeuTexto) < 0.0001 then MeuTexto := '';
  end;
  if MeuTexto = '' then Result := '' else
  begin
    MeuPrefixo := '';
    if Length(Prefixo) > 0 then
      for i := 0 to EspacosPrefixo-1 do MeuPrefixo := MeuPrefixo + ' ';
    Result := Prefixo+MeuPrefixo+MeuTexto+Sufixo;
  end;
end;

end.
