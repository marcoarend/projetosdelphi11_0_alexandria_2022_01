unit EmiteCheque_0;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, ComCtrls, dmkGeral, IdBaseComponent,
  IdComponent, IdTCPConnection, IdTCPClient, dmkEdit, DB, mySQLDbTables,
  DBCtrls, dmkDBLookupComboBox, dmkEditCB, dmkImage, UnDmkEnums;

type
  TFmEmiteCheque_0 = class(TForm)
    PainelDados: TPanel;
    Panel2: TPanel;
    Label1: TLabel;
    LaValor: TLabel;
    Label5: TLabel;
    Label2: TLabel;
    TPData: TDateTimePicker;
    EdValor: TdmkEdit;
    EdBenef: TdmkEdit;
    EdCidade: TdmkEdit;
    IdTCPClient1: TIdTCPClient;
    Panel3: TPanel;
    Timer1: TTimer;
    Panel1: TPanel;
    Memo: TMemo;
    Panel4: TPanel;
    Label3: TLabel;
    Label4: TLabel;
    EdBomPara: TdmkEdit;
    GroupBox1: TGroupBox;
    Label11: TLabel;
    EdServer_IP: TdmkEdit;
    Label12: TLabel;
    dmkEdit1: TdmkEdit;
    Label6: TLabel;
    EdID_ConexaoVai: TEdit;
    GroupBox2: TGroupBox;
    Ed3: TEdit;
    Ed2: TEdit;
    EdID_ConexaoVem: TEdit;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    EdMsgA: TEdit;
    Label10: TLabel;
    EdBanco: TdmkEdit;
    dmkEdit2: TdmkEdit;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel5: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel6: TPanel;
    BitBtn1: TBitBtn;
    BtInfo: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure IdTCPClient1Connected(Sender: TObject);
    procedure IdTCPClient1Disconnected(Sender: TObject);
    procedure IdTCPClient1Status(ASender: TObject; const AStatus: TIdStatus;
      const AStatusText: string);
    procedure Timer1Timer(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
    function ConectaAoServidor(): Boolean;
    function DesconectaDoServidor(): Boolean;
  public
    { Public declarations }
  end;

  var
  FmEmiteCheque_0: TFmEmiteCheque_0;

implementation

uses UnMyObjects;

{$R *.DFM}

procedure TFmEmiteCheque_0.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

function TFmEmiteCheque_0.ConectaAoServidor(): Boolean;
var
  ID_CONEXAO, ID_RETORNO: String;
begin
  //Result := False;
  IdTCPClient1.Host := EdServer_IP.ValueVariant;
  if not IdTCPClient1.Connected then
  begin
    try
      IdTCPClient1.Connect;
      Ed2.Text := IdTCPClient1.IOHandler.ReadLnWait(3000);
      Randomize();
      ID_RETORNO := '';
      //
      ID_CONEXAO := '0';
      while ID_CONEXAO = '0' do
        ID_CONEXAO := FormatFloat('0', Random(1000000000));
      //  
      EdID_ConexaoVai.Text := ID_CONEXAO;
      IdTCPClient1.IOHandler.WriteLn('ID_CONEXAO SET|' + ID_CONEXAO);
      Ed3.Text := IdTCPClient1.IOHandler.ReadLnWait(3000);
      IdTCPClient1.IOHandler.WriteLn('ID_CONEXAO GET');
      ID_RETORNO := IdTCPClient1.IOHandler.ReadLnWait(3000);
      EdID_ConexaoVem.Text := ID_RETORNO;
      //
      Result := ID_RETORNO = ID_CONEXAO;
    except
      raise;
    end;
  end else begin
    IdTCPClient1.IOHandler.WriteLn('ID_CONEXAO GET');
    ID_RETORNO := IdTCPClient1.IOHandler.ReadLnWait(3000);
    EdID_ConexaoVem.Text := ID_RETORNO;
    //
    Result := ID_RETORNO = ID_CONEXAO;
  end;
  //IdTCPClient1.IOHandler.WriteLn('CIDADE GET');
  //IdTCPClient1.IOHandler.WriteLn('VALOR GET');
  //EdMsgA.Text := IdTCPClient1.IOHandler.ReadLnWait(3000));
  Timer1.Enabled := Result;
  if not Result then
    Memo.Lines.Add('Servidor ocupado com impress�o de outro cheque!');
end;

function TFmEmiteCheque_0.DesconectaDoServidor(): Boolean;
begin
  Result := False;
  if IdTCPClient1.Connected then
  begin
    IdTCPClient1.Disconnect;
    IdTCPClient1.IOHandler.CloseGracefully;
    IdTCPClient1.IOHandler.Free;
    Screen.Cursor := crHourGlass;
    Timer1.Enabled := False;
    Screen.Cursor := crDefault;
    Result := True;
    Timer1.Enabled := not Result;
  end;  
end;

procedure TFmEmiteCheque_0.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmEmiteCheque_0.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmEmiteCheque_0.IdTCPClient1Connected(Sender: TObject);
begin
  EdMsgA.Text := 'Connectado ao servidor.';
end;

procedure TFmEmiteCheque_0.IdTCPClient1Disconnected(Sender: TObject);
begin
  EdMsgA.Text := 'Desconectado do servidor.';
end;

procedure TFmEmiteCheque_0.IdTCPClient1Status(ASender: TObject;
  const AStatus: TIdStatus; const AStatusText: string);
begin
  EdMsgA.Text := 'Status: ' + AStatusText;
end;

procedure TFmEmiteCheque_0.Timer1Timer(Sender: TObject);
var
  I: Integer;
  Texto: String;
begin
  Timer1.Enabled := False;
  try
    if IdTCPClient1.Connected then
    begin
      IdTCPClient1.Socket.WriteLn('HEARTBEAT');
      try
        for I := 1 to 2 do
        begin
          Texto := IdTCPClient1.IOHandler.ReadLnWait(10);
          if Texto <> '' then
          begin
            if Texto <>  '200 Ok' then
              Memo.Lines.Add(Texto + ' �s ' + FormatDateTime('hh:nn:ss:zzz', Now()));
          end;
        end;
      except
      end;
    end;
  finally
    Timer1.Enabled := True;
  end;
end;

procedure TFmEmiteCheque_0.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  try
    DesconectaDoServidor();
  except
   // Nada
  end;
end;

procedure TFmEmiteCheque_0.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  TPData.Date := Date;
  //UnDmkDAC_PF.AbreQuery(QrBancos, Dmod.MyDB);
end;

procedure TFmEmiteCheque_0.BitBtn1Click(Sender: TObject);
begin
  Screen.Cursor := crHourGlass;
  try
    Memo.Lines.Clear;
    if ConectaAoServidor() then
    begin
      IdTCPClient1.IOHandler.WriteLn('STEP SET|' + '1');
      IdTCPClient1.IOHandler.WriteLn('DATA SET|' + FormatFloat('0', TPData.Date));
      IdTCPClient1.IOHandler.WriteLn('VALOR SET|' + EdValor.Text);
      IdTCPClient1.IOHandler.WriteLn('BENEF SET|' + EdBenef.Text);
      IdTCPClient1.IOHandler.WriteLn('CIDADE SET|' + EdCidade.Text);
      IdTCPClient1.IOHandler.WriteLn('BANCO SET|' + EdBanco.Text);
      IdTCPClient1.IOHandler.WriteLn('BOMPARA SET|' + EdBomPara.Text);
      IdTCPClient1.IOHandler.WriteLn('STEP SET|' + '90');
    end;
  finally
    Screen.Cursor := crDefault;
  end;
end;

end.
