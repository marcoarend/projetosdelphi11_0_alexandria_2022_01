unit PertoChek;

interface

uses
  StdCtrls, ExtCtrls, Windows, Messages, SysUtils, Classes, Graphics, Controls,
  Forms, dmkGeral, dmkEdit;

type
  TPertoChek = class(TObject)
  private
    { Private declarations }

  public
    { Public declarations }
    function HabilitaPertoChek(var Retorno: String): Boolean;
    procedure DesabilitaPertoCheck;
    procedure Verifica_PertoChek(var Retorno: String);
    function EnviaPertoChek2(Texto: String; var Retorno: String): String;
    function EnviaPertoChek3(const Texto: String; var CodigoRetorno,
             Mensagem: TLabel): Boolean;
    function ErroPertoChek(Codigo: String; ShowSeOK, ShowErro: Boolean): String;
  end;

function habilita_paralela(si :PChar) :boolean; far; stdcall external 'PertoChekPar.dll';
function desabilita_paralela:boolean; far; stdcall external 'PertoChekPar.dll';
function transmite(si :PChar) :integer; far; stdcall external 'PertoChekPar.dll';
function recebe(t :integer; bufrx :PChar) :integer; far; stdcall external 'PertoChekPar.dll';

function IniComm(si :PChar) :boolean; far; stdcall external 'pertochekser.dll';
function EndComm:boolean; far; stdcall external 'pertochekser.dll';
function EnvComm(si :PChar) :integer; far; stdcall external 'pertochekser.dll';
function RecComm(t :integer; bufrx :PChar) :integer; far; stdcall external 'pertochekser.dll';

var
  PertoChekP: TPertoChek;

implementation

uses UnInternalConsts;

function TPertoChek.HabilitaPertoChek(var Retorno: String): Boolean;
var
 //conf: array[0..255] of char;
 Res: Boolean;
begin
  Res := True;
  //strpcopy (conf, 'LPT1');
  if VAR_IMPCHEQUE = 1 then
  begin
    Res := habilita_paralela(PChar('LPT1'));
    if Res = False then Retorno := 'Porta paralela n�o pode ser inicializada !';
  end else if VAR_IMPCHEQUE = 2 then
  begin
    Res :=  IniComm(PChar('COM1:4800,N,8,1'));
    if Res = False then Retorno := 'Porta serial n�o pode ser inicializada !';
  end;
  Result := Res;
end;

procedure TPertoChek.DesabilitaPertoCheck;
begin
  if VAR_IMPCHEQUE = 1 then desabilita_paralela
  else if VAR_IMPCHEQUE = 2 then EndComm();
end;

function TPertoChek.ErroPertoChek(Codigo: String; ShowSeOK, ShowErro: Boolean): String;
var
  Erro: Integer;
  Acao: String;
begin
  Erro := 0;
  Result := Codigo;
  if Length(Codigo) = 4 then
  begin
    Acao := Codigo[1];
    Erro := StrToInt(Copy(Codigo, 2, 3));
    case Erro of
      000: Result := 'Sucesso na execu��o do comando.';
      001: Result := 'Mensagem com dados inv�lidos.';
      002: Result := 'Tamanho de mensagem inv�lido.';
      005: Result := 'Leitura dos caracteres magn�ticos inv�lida.';
      006: Result := 'Problemas no acionamento do motor 1.';
      008: Result := 'Problemas no acionamento do motor 2.';
      009: Result := 'Banco diferente do solicitado.';
      011: Result := 'Sensor 1 obstru�do.';
      012: Result := 'Sensor 2 obstru�do.';
      013: Result := 'Sensor 4 obstru�do.';
      014: Result := 'Erro o posicionamento da cabe�a de impress�o (relativo a S4).';
      015: Result := 'Erro o posicionamento na p�s-marca��o.';
      016: Result := 'D�gito verificador do cheque n�o confere.';
      017: Result := 'Aus�ncia de caracteres magn�ticos ou cheque na posi��o errada.';
      018: Result := 'Tempo esgotado.';
      019: Result := 'Documento mal inserido.';
      020: Result := 'Cheque preso durante o alinhamento (S1 e S2 desobstru�dos).';
      021: Result := 'Cheque preso durante o alinhamento (S1 obstru�do e S2 desobstru�do).';
      022: Result := 'Cheque preso durante o alinhamento (S1 desobstru�do e S2 obstru�do).';
      023: Result := 'Cheque preso durante o alinhamento (S1 e S2 obstru�dos).';
      024: Result := 'Cheque preso durante o preenchimento (S1 e S2 desobstru�dos).';
      025: Result := 'Cheque preso durante o preenchimento (S1 obstru�do e S2 desobstru�do).';
      026: Result := 'Cheque preso durante o preenchimento (S1 desobstru�do e S2 obstru�do).';
      027: Result := 'Cheque preso durante o preenchimento (S1 e S2 obstru�dos).';
      028: Result := 'Caracter inexistente.';
      030: Result := 'N�o h� cheques na mem�ria.';
      031: Result := 'Lista negra interna cheia.';
      042: Result := 'Cheque ausente.';
      043: Result := 'Pin pad ou teclado ausente.';
      050: Result := 'Erro de transmiss�o.';
      051: Result := 'Erro de transmiss�o: Impressora off line, desconectada ou ocupada.';
      052: Result := 'Erro no pin pad.';
      060: Result := 'Cheque na lista negra.';
      073: Result := 'Cheque n�o encontrado na lista negra.';
      074: Result := 'Comando cancelado.';
      084: Result := 'Arquivo de lay out�s cheio.';
      085: Result := 'Lay out inexistente na mem�ria.';
      091: Result := 'Leitura de cart�o inv�lida.';
      097: Result := 'Cheque na posi��o errada.';
      111: Result := 'Pin pad n�o retornou EOT.';
      150: Result := 'Pin pad n�o retornou NAK.';
      155: Result := 'Pin pad n�o responde.';
      171: Result := 'Tempo esgotado na resposta do pin pad.';
      255: Result := 'Comando inexistente.';
    end;  
  end;
  if (Erro = 0) and (ShowSeOK=False) then Exit;
  if (Erro <> 0) and (ShowErro=False) then Exit;
  //
  if Erro <> 0 then
    Geral.MB_Erro(Result)
  else
    Geral.MB_Info(Result);
end;

function TPertoChek.EnviaPertoChek2(Texto: String; var Retorno: String): String;
var
  ticks: integer;
  resp :array[0..255] of char;
  r: integer;
begin
  Screen.Cursor := crHourGlass;
  ticks := 500;
  r := 0;
  if VAR_IMPCHEQUE = 1 then r := transmite(PChar(Texto))
  else EnvComm(PChar(Texto));
  if r = 0 then
  begin
    Retorno := 'Erro na transmiss�o do comando para a Pertocheck!';
    //VAR_IMPRECHEQUE := 0;
  end else begin
    if VAR_IMPCHEQUE = 1 then r := recebe(ticks, resp) else RecComm(ticks, resp);
    if r = 0 then
    begin
       Result := resp;
       Retorno := 'Erro na recep��o da resposta da pertocheck!';
    end else
    begin
      VAR_IMPRECHEQUE := 1;
      Result := resp;
      Retorno := ErroPertoChek(resp, False, False);
    end;
  end;
  Screen.Cursor := crDefault;
end;

function TPertoChek.EnviaPertoChek3(const Texto: String; var CodigoRetorno,
Mensagem: TLabel): Boolean;
var
  ticks: integer;
  resp :array[0..255] of char;
  r: integer;
begin
  Result := False;
  if VAR_IMPRECHEQUE = 0 then
  begin
    CodigoRetorno.Caption := '0';
    Mensagem.Caption := 'Erro na transmiss�o do comando para a Pertocheck!';
    Exit;
  end;
  Screen.Cursor := crHourGlass;
  ticks := 500;
  r := 0;
  if VAR_IMPCHEQUE = 1 then r := transmite(PChar(Texto))
  else EnvComm(PChar(Texto));
  if r = 0 then
  begin
    //VAR_IMPRECHEQUE := 0;
    CodigoRetorno.Caption := '0';
    Mensagem.Caption := 'Erro na transmiss�o do comando para a Pertocheck!';
    //Result := False;
  end else begin
    if VAR_IMPCHEQUE = 1 then r := recebe(ticks, resp) else RecComm(ticks, resp);
    if r = 0 then
    begin
       CodigoRetorno.Caption := resp;
       Mensagem.Caption := 'Erro na recep��o da resposta da pertocheck!';
    end else
    begin
      VAR_IMPRECHEQUE := 1;
      CodigoRetorno.Caption := resp;
      Mensagem.Caption := ErroPertoChek(resp, False, False);
    end;
  end;
  Result := VAR_IMPCHEQUE = 1;
  Screen.Cursor := crDefault;
end;

procedure TPertoChek.Verifica_PertoChek(var Retorno: String);
var
  Caminho: String;
begin
  if VAR_IMPCHEQUE = 1 then
  begin
    Caminho := ExtractFilePath(Application.ExeName)+'\PertoChekPar.dll';
    if not FileExists(Caminho) then Retorno := 'A DLL da Pertochek'+
    ' (paralela) n�o foi localizada no diret�rio do execut�vel!'
    else begin
      HabilitaPertoChek(Retorno);
      Retorno := EnviaPertoChek2('V', Retorno);
      if Length(Retorno) > 4 then
        if VAR_STIMPCHEQUE <> nil then
          VAR_STIMPCHEQUE.Text := 'Pertochek ' + Retorno;
    end;
  end;
  if VAR_IMPCHEQUE = 2 then
  begin
    Caminho := ExtractFilePath(Application.ExeName)+'\PertoChekSer.dll';
    if not FileExists(Caminho) then Retorno := 'A DLL da Pertochek'+
    ' (serial) n�o foi localizada no diret�rio do execut�vel!'
    else begin
      HabilitaPertoChek(Retorno);
      Retorno := EnviaPertoChek2('V', Retorno);
      if Length(Retorno) > 4 then
        if VAR_STIMPCHEQUE <> nil then
          VAR_STIMPCHEQUE.Text := 'Pertochek ' + Retorno;
    end;
  end;
end;

end.




