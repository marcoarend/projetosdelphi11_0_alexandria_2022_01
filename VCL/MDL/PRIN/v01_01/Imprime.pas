unit Imprime;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls, Db, (*DBTables,*) JPEG, ExtDlgs,
  ZCF2, ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, iniFiles,
  Grids, DBGrids, Menus, dmkGeral, dmkDBLookupComboBox, dmkEdit, dmkEditCB,
  dmkLabel, Variants, dmkPopOutFntCBox, UnDmkProcFunc, dmkImage, UnDmkEnums;

type
  TFmImprime = class(TForm)
    PainelDados: TPanel;
    DsImprime: TDataSource;
    QrImprime: TmySQLQuery;
    PainelEdita: TPanel;
    QrImprimeCodigo: TIntegerField;
    QrImprimeNome: TWideStringField;
    QrImprimeTitulo: TWideStringField;
    QrImprimeTipo: TSmallintField;
    QrImprimeOrientacao: TSmallintField;
    QrImprimePos_Topo: TIntegerField;
    QrImprimePos_Altu: TIntegerField;
    QrImprimePos_Larg: TIntegerField;
    QrImprimePos_MEsq: TIntegerField;
    QrImprimeLk: TIntegerField;
    QrImprimeDataCad: TDateField;
    QrImprimeDataAlt: TDateField;
    QrImprimeUserCad: TIntegerField;
    QrImprimeUserAlt: TIntegerField;
    Panel1: TPanel;
    Panel6: TPanel;
    GradeBand: TDBGrid;
    GradeView: TDBGrid;
    QrImprimeBand: TmySQLQuery;
    QrImprimeBandCodigo: TIntegerField;
    QrImprimeBandControle: TIntegerField;
    QrImprimeBandTipo: TIntegerField;
    QrImprimeBandPos_Topo: TIntegerField;
    QrImprimeBandPos_Altu: TIntegerField;
    DsImprimeBand: TDataSource;
    QrImprimeBandNome: TWideStringField;
    QrImprimeView: TmySQLQuery;
    DsImprimeView: TDataSource;
    QrImprimeViewCodigo: TIntegerField;
    QrImprimeViewControle: TIntegerField;
    QrImprimeViewPos_Topo: TIntegerField;
    QrImprimeViewPos_Altu: TIntegerField;
    QrImprimeViewPos_Larg: TIntegerField;
    QrImprimeViewPos_MEsq: TIntegerField;
    QrImprimeViewSet_Negr: TIntegerField;
    QrImprimeViewSet_Ital: TIntegerField;
    QrImprimeViewSet_Unde: TIntegerField;
    QrImprimeViewSet_Tam: TIntegerField;
    QrImprimeViewNome: TWideStringField;
    PMInclui: TPopupMenu;
    NovoFormato1: TMenuItem;
    N1: TMenuItem;
    IncluiBanda1: TMenuItem;
    IncluiCampo1: TMenuItem;
    QrImprimeBandNOMETIPO: TWideStringField;
    QrImprimeBandNOMEDATASET: TWideStringField;
    QrImprimeBandDataset: TIntegerField;
    PMAltera: TPopupMenu;
    MenuItem1: TMenuItem;
    MenuItem2: TMenuItem;
    MenuItem3: TMenuItem;
    MenuItem4: TMenuItem;
    PMExclui: TPopupMenu;
    MenuItem5: TMenuItem;
    MenuItem6: TMenuItem;
    ExcluiBanda1: TMenuItem;
    ExcluiCampo1: TMenuItem;
    QrImprimeViewNOMEALIGN: TWideStringField;
    QrImprimeViewNOMENEGR: TWideStringField;
    QrImprimeViewNOMEITAL: TWideStringField;
    QrImprimeViewNOMESUBL: TWideStringField;
    QrImprimeViewSet_TAli: TIntegerField;
    QrImprimeViewSet_BAli: TIntegerField;
    QrImprimeBandCol_Qtd: TIntegerField;
    QrImprimeBandCol_Lar: TIntegerField;
    QrImprimeBandCol_GAP: TIntegerField;
    QrImprimeViewTab_Tipo: TIntegerField;
    QrImprimeViewTab_Campo: TIntegerField;
    QrImprimeViewPadrao: TWideStringField;
    QrImprimeViewNOMETABELA: TWideStringField;
    QrImprimeViewNOMECAMPO: TWideStringField;
    QrImprimeViewFormato: TIntegerField;
    QrImprimeViewNOMEFORMATO: TWideStringField;
    QrImprimeViewTOPOBANDA: TIntegerField;
    QrImprimeViewTOPOREAL: TIntegerField;
    QrImprimePos_MSup: TIntegerField;
    Duplicaformatoatual1: TMenuItem;
    QrImprimeViewPrefixo: TWideStringField;
    QrImprimeViewSufixo: TWideStringField;
    QrImprimeNomeFonte: TWideStringField;
    QrImprimeBandLinhas: TIntegerField;
    QrImprimeBandRepetencia: TSmallintField;
    QrImprimeBandNOMEREPETENCIA: TWideStringField;
    QrImprimeViewSubstitui: TSmallintField;
    QrImprimeViewSubstituicao: TWideStringField;
    Somenteselecionado1: TMenuItem;
    Umaum1: TMenuItem;
    QrImprimeCPI: TSmallintField;
    QrImprimeMSupDOS: TIntegerField;
    QrImprimeViewBand: TIntegerField;
    QrImprimeViewSet_T_DOS: TSmallintField;
    QrImprimeViewLk: TIntegerField;
    QrImprimeViewDataCad: TDateField;
    QrImprimeViewDataAlt: TDateField;
    QrImprimeViewUserCad: TIntegerField;
    QrImprimeViewUserAlt: TIntegerField;
    QrImprimeViewAlt_Linha: TIntegerField;
    QrImprimeViewNulo: TSmallintField;
    QrImprimeView2: TmySQLQuery;
    QrImprimeView2TOPOBANDA: TIntegerField;
    QrImprimeView2Codigo: TIntegerField;
    QrImprimeView2Controle: TIntegerField;
    QrImprimeView2Band: TIntegerField;
    QrImprimeView2Nome: TWideStringField;
    QrImprimeView2Tab_Tipo: TIntegerField;
    QrImprimeView2Tab_Campo: TIntegerField;
    QrImprimeView2Padrao: TWideStringField;
    QrImprimeView2Pos_Topo: TIntegerField;
    QrImprimeView2Pos_Altu: TIntegerField;
    QrImprimeView2Pos_Larg: TIntegerField;
    QrImprimeView2Pos_MEsq: TIntegerField;
    QrImprimeView2Set_TAli: TIntegerField;
    QrImprimeView2Set_BAli: TIntegerField;
    QrImprimeView2Set_Negr: TIntegerField;
    QrImprimeView2Set_Ital: TIntegerField;
    QrImprimeView2Set_Unde: TIntegerField;
    QrImprimeView2Set_Tam: TIntegerField;
    QrImprimeView2Formato: TIntegerField;
    QrImprimeView2Lk: TIntegerField;
    QrImprimeView2DataCad: TDateField;
    QrImprimeView2DataAlt: TDateField;
    QrImprimeView2UserCad: TIntegerField;
    QrImprimeView2UserAlt: TIntegerField;
    QrImprimeView2Prefixo: TWideStringField;
    QrImprimeView2Sufixo: TWideStringField;
    QrImprimeView2Substitui: TSmallintField;
    QrImprimeView2Substituicao: TWideStringField;
    QrImprimeView2Set_T_DOS: TSmallintField;
    QrImprimeView2Alt_Linha: TIntegerField;
    QrImprimeView2Nulo: TSmallintField;
    QrImprimeView2TOPOREAL: TIntegerField;
    QrImprimePos_MSup2: TIntegerField;
    QrImprimeTipoImpressao: TIntegerField;
    IncluiCampo2: TMenuItem;
    QrCampos: TmySQLQuery;
    QrImprimeBandOrdem: TIntegerField;
    QrImprimeBandLk: TIntegerField;
    QrImprimeBandDataCad: TDateField;
    QrImprimeBandDataAlt: TDateField;
    QrImprimeBandUserCad: TIntegerField;
    QrImprimeBandUserAlt: TIntegerField;
    QrDados: TmySQLQuery;
    QrDadosIts: TmySQLQuery;
    QrCamposIts: TmySQLQuery;
    PMReciclo: TPopupMenu;
    Exporta1: TMenuItem;
    Importa1: TMenuItem;
    OpenDialog1: TOpenDialog;
    ListBox1: TListBox;
    ListBox3: TListBox;
    RGTipoImpressao: TRadioGroup;
    QrImprimeNO_TipoImpressao: TWideStringField;
    QrImprimeEmpr: TmySQLQuery;
    DsImprimeEmpr: TDataSource;
    QrImprimeEmprCodigo: TIntegerField;
    QrImprimeEmprControle: TIntegerField;
    QrImprimeEmprEmpresa: TIntegerField;
    QrImprimeEmprSerieNF: TIntegerField;
    QrImprimeEmprNO_EMPR: TWideStringField;
    QrImprimeEmprSequencial: TIntegerField;
    QrImprimeEmprParamsNFs: TIntegerField;
    N2: TMenuItem;
    SerieNFFilial1: TMenuItem;
    N3: TMenuItem;
    N4: TMenuItem;
    SerieNFFilial3: TMenuItem;
    SerieNFFilial2: TMenuItem;
    Panel16: TPanel;
    Panel7: TPanel;
    Panel8: TPanel;
    Label9: TLabel;
    Label10: TLabel;
    Label8: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Label17: TLabel;
    Label20: TLabel;
    Label19: TLabel;
    Label28: TLabel;
    EdCodigo: TdmkEdit;
    EdNome: TEdit;
    EdTitulo: TEdit;
    EdTopo: TdmkEdit;
    EdAltu: TdmkEdit;
    EdLarg: TdmkEdit;
    EdMEsq: TdmkEdit;
    EdMSup: TdmkEdit;
    EdMSup2: TdmkEdit;
    Panel9: TPanel;
    Bevel2: TBevel;
    Land2: TImage;
    Port2: TImage;
    RGOrient2: TRadioGroup;
    Panel10: TPanel;
    GroupBox1: TGroupBox;
    Label23: TLabel;
    RGCPI: TRadioGroup;
    EdMSupDOS: TdmkEdit;
    QrImprimeEmprFilial: TIntegerField;
    Panel17: TPanel;
    Panel2: TPanel;
    Panel4: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label3: TLabel;
    Label18: TLabel;
    Label21: TLabel;
    Label27: TLabel;
    DBEdCodigo: TDBEdit;
    EdDBNome: TDBEdit;
    EdDBTopo: TDBEdit;
    EdDBAltu: TDBEdit;
    EdDBLarg: TDBEdit;
    EdDBMEsq: TDBEdit;
    EdDBTitulo: TDBEdit;
    EdDBMSup: TDBEdit;
    DBEdit1: TDBEdit;
    EdDBMSup2: TDBEdit;
    Panel11: TPanel;
    Bevel1: TBevel;
    Land1: TImage;
    Port1: TImage;
    RGOrient1: TDBRadioGroup;
    Panel12: TPanel;
    STImport: TStaticText;
    Panel14: TPanel;
    GroupBox2: TGroupBox;
    Label14: TLabel;
    DBRadioGroup1: TDBRadioGroup;
    EdDBMSupDOS: TDBEdit;
    Panel18: TPanel;
    DBGImprimeEmpr: TDBGrid;
    CBFoNome: TdmkPopOutFntCBox;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel15: TPanel;
    BtDesiste: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel19: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel13: TPanel;
    BtSaida: TBitBtn;
    BtInclui: TBitBtn;
    BtAltera: TBitBtn;
    BtExclui: TBitBtn;
    BitBtn1: TBitBtn;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtIncluiClick(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrImprimeAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure QrImprimeAfterScroll(DataSet: TDataSet);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrImprimeBeforeOpen(DataSet: TDataSet);
    procedure RGOrient2Change(Sender: TObject; ButtonIndex: Integer);
    procedure RGOrient2Click(Sender: TObject);
    procedure RGOrient1Change(Sender: TObject);
    procedure RGOrient1Click(Sender: TObject);
    procedure NovoFormato1Click(Sender: TObject);
    procedure IncluiBanda1Click(Sender: TObject);
    procedure QrImprimeBandCalcFields(DataSet: TDataSet);
    procedure MenuItem1Click(Sender: TObject);
    procedure MenuItem3Click(Sender: TObject);
    procedure ExcluiBanda1Click(Sender: TObject);
    procedure BtExcluiClick(Sender: TObject);
    procedure IncluiCampo1Click(Sender: TObject);
    procedure QrImprimeViewCalcFields(DataSet: TDataSet);
    procedure ExcluiCampo1Click(Sender: TObject);
    procedure PMIncluiPopup(Sender: TObject);
    procedure QrImprimeBandAfterScroll(DataSet: TDataSet);
    procedure PMExcluiPopup(Sender: TObject);
    procedure Duplicaformatoatual1Click(Sender: TObject);
    procedure Somenteselecionado1Click(Sender: TObject);
    procedure Umaum1Click(Sender: TObject);
    procedure QrImprimeView2CalcFields(DataSet: TDataSet);
    procedure QrImprimeBandBeforeClose(DataSet: TDataSet);
    procedure QrImprimeBeforeClose(DataSet: TDataSet);
    procedure IncluiCampo2Click(Sender: TObject);
    procedure Exporta1Click(Sender: TObject);
    procedure Importa1Click(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure GradeViewDblClick(Sender: TObject);
    procedure EdFilialChange(Sender: TObject);
    procedure EdFilialEnter(Sender: TObject);
    procedure EdFilialExit(Sender: TObject);
    procedure SerieNFFilial3Click(Sender: TObject);
    procedure SerieNFFilial1Click(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
    procedure SbNovoClick(Sender: TObject);
    procedure PMAlteraPopup(Sender: TObject);
  private
    FExecuted: Boolean;
    AppIni: TIniFile;
    //FFilial: Integer;
    //
    function DefineInsertCampo(Field, Campo, Valor: String): String;
    procedure CriaOForm;
    procedure SubQueriesReopen;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure IncluiRegistro;
    procedure AlteraRegistro;
   ////Procedures do form
    procedure MostraEdicao(Mostra: Boolean; SQLType: TSQLType; Codigo: Integer);
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    procedure VerificaOrientacao;
    procedure AtualizaOrdemBandas;
    procedure DuplicaRegistro;
    procedure AlteraCampoSelecionado(Query: TmySQLQuery; UmAUm: Boolean);
    procedure ConfiguraDadosView(Query: TmySQLQuery);
    //procedure ReopenParamsNFs(Controle: Integer);
  public
    { Public declarations }
    FView,
    FBand: Integer;
    FDesiste: Boolean;
    procedure ReopenImprimeView(Controle: Integer);
    procedure ReopenImprimeEmpr(Controle: Integer);
    procedure LocCod(Atual, Codigo: Integer);
  end;

var
  FmImprime: TFmImprime;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, ImprimeBand, ImprimeView, UCreate, (*NF1,*) MyDBCheck,
  ImprimeEmpr, DmkDAC_PF;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmImprime.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmImprime.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrImprimeCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////
procedure TFmImprime.DefParams;
begin
  VAR_GOTOTABELA := 'Imprime';
  VAR_GOTOMYSQLTABLE := QrImprime;
  VAR_GOTONEG := gotoPiZ;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;
  VAR_SQLx.Add('SELECT imp.*, ');
  {
  VAR_SQLx.Add('nfs.SerieNF NO_SerieNF, nfs.Sequencial,');
  VAR_SQLx.Add('IF(ent.Tipo=0,ent.RazaoSocial,ent.Nome) NO_EMPRESA,');
  VAR_SQLx.Add('ent.Filial, ');
  }
  VAR_SQLx.Add('ELT(imp.TipoImpressao+1, "NF Formato A",');
  VAR_SQLx.Add('"Duplicata", "NF Formato B", "NF Formato C", ');
  VAR_SQLx.Add('"? ? ?") NO_TipoImpressao');
  VAR_SQLx.Add('FROM imprime imp');
  {
  VAR_SQLx.Add('LEFT JOIN entidades ent ON ent.Codigo=imp.Empresa');
  VAR_SQLx.Add('LEFT JOIN paramsnfs nfs ON nfs.Controle=imp.SerieNF');
  }
  VAR_SQLx.Add('WHERE imp.Codigo > 0');
  VAR_SQLx.Add('');
  //
  VAR_SQL1.Add('AND imp.Codigo=:P0');
  //
  VAR_SQLa.Add('AND imp.Nome Like :P0');
  //
end;

procedure TFmImprime.MostraEdicao(Mostra: Boolean; SQLType: TSQLType; Codigo: Integer);
var
  i: Integer;
begin
  if Mostra then
  begin
    PainelEdita.Visible := True;
    PainelDados.Visible := False;
    EdNome.Text := CO_VAZIO;
    EdNome.Visible := True;
    if SQLType = stIns then
    begin
      EdCodigo.Text             := FormatFloat(FFormatFloat, Codigo);
      EdNome.Text               := '';
      EdTitulo.Text             := 'Relat�rio '+Application.Title;
      EdTopo.Text               := '';
      EdAltu.Text               := '';
      EdLarg.Text               := '';
      EdMEsq.Text               := '';
      EdMSup.Text               := '';
      EdMSup2.Text              := '';
      EdMSupDOS.Text            := '';
      //RGOrient1.ItemIndex     := 0;
      RGCPI.ItemIndex           := 0;
      {
      EdFilial.ValueVariant     := Null;
      CBFilial.KeyValue         := Null;
      EdSerieNF.ValueVariant    := Null;
      CBSerieNF.KeyValue        := Null;
      }
    end else begin
      EdCodigo.Text             := DBEdCodigo.Text;
      EdNome.Text               := EdDBNome.Text;
      EdTitulo.Text             := EdDBTitulo.Text;
      EdMEsq.Text               := EdDBMEsq.Text;
      EdMSup.Text               := EdDBMSup.Text;
      EdMSup2.Text              := EdDBMSup2.Text;
      EdMSupDOS.Text            := EdDBMSupDOS.Text;
      EdLarg.Text               := EdDBLarg.Text;
      EdAltu.Text               := EdDBAltu.Text;
      EdTopo.Text               := EdDBTopo.Text;
      RGOrient2.ItemIndex       := RGOrient1.ItemIndex;
      RGTipoImpressao.ItemIndex := QrImprimeTipoImpressao.Value;
      {
      EdFilial.ValueVariant     := QrImprimeFilial.Value;
      CBFilial.KeyValue         := QrImprimeFilial.Value;
      // Se n�o reabrir n�o mostra nada!
      ReopenParamsNFs(0);
      EdSerieNF.ValueVariant    := QrImprimeSerieNF.Value;
      CBSerieNF.KeyValue        := QrImprimeSerieNF.Value;
      }
      if QrImprimeCPI.Value = 12 then
        RGCPI.ItemIndex := 1
      else
        RGCPI.ItemIndex := 0;
      for i := 0 to CBFoNome.Items.Count-1 do
      begin
        if CBFoNome.Items[i] = QrImprimeNomeFonte.Value then
        begin
          CBFoNome.ItemIndex := i;
          Break;
        end;
      end;
    end;
    EdNome.SetFocus;
  end else begin
    PainelDados.Visible := True;
    PainelEdita.Visible := False;
  end;
  ImgTipo.SQLType := SQLType;
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmImprime.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmImprime.AlteraRegistro;
var
  Imprime : Integer;
begin
  Imprime := QrImprimeCodigo.Value;
  if QrImprimeCodigo.Value = 0 then
  begin
    BtAltera.Enabled := False;
    Exit;
  end;
  if not UMyMod.SelLockY(Imprime, Dmod.MyDB, 'Imprime', 'Codigo') then
  begin
    try
      UMyMod.UpdLockY(Imprime, Dmod.MyDB, 'Imprime', 'Codigo');
      MostraEdicao(True, stUpd, 0);
    finally
      Screen.Cursor := Cursor;
    end;
  end;
end;

procedure TFmImprime.IncluiRegistro;
var
  Cursor : TCursor;
  Imprime : Integer;
begin
  Cursor := Screen.Cursor;
  Screen.Cursor := crHourglass;
  Refresh;
  try
    Imprime := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
    'Imprime', 'Imprime', 'Codigo');
    if Length(FormatFloat(FFormatFloat, Imprime))>Length(FFormatFloat) then
    begin
      Geral.MB_Erro(
      'Inclus�o cancelada. Limite de cadastros extrapolado');
      Screen.Cursor := Cursor;
      Exit;
    end;
    MostraEdicao(True, stIns, Imprime);
  finally
    Screen.Cursor := Cursor;
  end;
end;

procedure TFmImprime.QueryPrincipalAfterOpen;
begin
end;

procedure TFmImprime.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmImprime.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmImprime.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmImprime.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmImprime.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmImprime.BtIncluiClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMInclui, BtInclui);
end;

procedure TFmImprime.BtAlteraClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMAltera, BtAltera);
end;

procedure TFmImprime.BtSaidaClick(Sender: TObject);
begin
  VAR_IMPRIMEFMT := QrImprimeCodigo.Value;
  VAR_IMPRIMETYP := 1;
  Close;
end;

procedure TFmImprime.BtConfirmaClick(Sender: TObject);
var
  CPI, MSupDOS, Codigo, {Empresa, SerieNF,} Tipo, Orientacao,
  Pos_Topo, Pos_Altu, Pos_Larg, Pos_MEsq, Pos_MSup, Pos_MSup2,
  TipoImpressao(*, Filial*): Integer;
  Nome, NomeFonte, Titulo: String;
begin
  Nome := EdNome.Text;
  if Length(Nome) = 0 then
  begin
    Geral.MB_Aviso('Defina uma descri��o.');
    EdNome.SetFocus;
    Exit;
  end;
  CPI           := dmkPF.EscolhaDe2Int(RGCPI.ItemIndex = 1, 12, 10);
  MSupDOS       := Geral.IMV(EdMSupDOS.Text);
  Tipo          := 0; // ???
  Orientacao    := RGOrient2.ItemIndex;
  Pos_Topo      := Geral.IMV(EdTopo.Text);
  Pos_Altu      := Geral.IMV(EdAltu.Text);
  Pos_Larg      := Geral.IMV(EdLarg.Text);
  Pos_MEsq      := Geral.IMV(EdMEsq.Text);
  Pos_MSup      := Geral.IMV(EdMSup.Text);
  Pos_MSup2     := Geral.IMV(EdMSup2.Text);
  TipoImpressao := RGTipoImpressao.ItemIndex;
  NomeFonte     := CBFoNome.FonteNome;
  Titulo        := EdTitulo.Text;

  {
  SerieNF       := Geral.IMV(EdSerieNF.Text);
  Filial        := Geral.IMV(EdFilial.Text);
  if Filial > 0 then
    Empresa     := QrFiliaisCodigo.Value
  else
    Empresa     := 0;
  }
  //SerieNF := 0;
  //Filial  := 0;
  //Empresa := 0;
  //
  //Codigo := UMyMod.BuscaEmLivreY_Def('imprime', 'Codigo', ImgTipo.SQLType, CodAtual);
  Codigo := Geral.IMV(EdCodigo.Text);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'imprime', False, [
  'Nome', 'Titulo', 'Tipo',
  'Orientacao', 'Pos_Topo', 'Pos_Altu',
  'Pos_Larg', 'Pos_MEsq', 'Pos_MSup',
  'Pos_MSup2', 'NomeFonte', 'CPI',
  'MSupDOS', 'TipoImpressao'], [
  'Codigo'], [
  Nome, Titulo, Tipo,
  Orientacao, Pos_Topo, Pos_Altu,
  Pos_Larg, Pos_MEsq, Pos_MSup,
  Pos_MSup2, NomeFonte, CPI,
  MSupDOS, TipoImpressao], [
  Codigo], True) then
  begin
    UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'Imprime', 'Codigo');
    MostraEdicao(False, stLok, 0);
    LocCod(Codigo,Codigo);
  end;
end;

procedure TFmImprime.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := Geral.IMV(EdCodigo.Text);
  if ImgTipo.SQLType = stIns then
    UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'Imprime', Codigo);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'Imprime', 'Codigo');
  MostraEdicao(False, stLok, 0);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'Imprime', 'Codigo');
end;

procedure TFmImprime.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //UnDmkDAC_PF.AbreQuery(QrFiliais, Dmod.MyDB);
  StImport.Caption := '';
  Height := Screen.Height - 70;                                       
  Width  := Screen.Width;
  PainelEdita.Align := alClient;
  PainelDados.Align := alClient;
  GradeView.Align := alClient;
  CriaOForm;
end;

procedure TFmImprime.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrImprimeCodigo.Value,LaRegistro.Caption);
end;

procedure TFmImprime.SbImprimeClick(Sender: TObject);
begin
//
end;

procedure TFmImprime.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmImprime.SbNovoClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMReciclo, SbNovo);
end;

procedure TFmImprime.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmImprime.QrImprimeAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmImprime.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //if UMyMod.NegaInclusaoY(Dmod.MyDB, 'Controle', 'Imprime', 'Livres', 99) then
  //BtInclui.Enabled := False;
end;

procedure TFmImprime.QrImprimeAfterScroll(DataSet: TDataSet);
begin
  STImport.Caption := 'Tipo de documento: ' + QrImprimeNO_TipoImpressao.Value;
  BtAltera.Enabled := GOTOy.BtEnabled(QrImprimeCodigo.Value, False);
  SubQueriesReopen;
  VerificaOrientacao;
  ReopenImprimeEmpr(0);
end;

procedure TFmImprime.SbQueryClick(Sender: TObject);
begin
  LocCod(QrImprimeCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'Imprime', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmImprime.SerieNFFilial1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmImprimeEmpr, FmImprimeEmpr, afmoNegarComAviso) then
  begin
    FmImprimeEmpr.ShowModal;
    FmImprimeEmpr.Destroy;
  end;
end;

procedure TFmImprime.SerieNFFilial3Click(Sender: TObject);
begin
  DBCheck.QuaisItens_Exclui(Dmod.QrUpd, QrImprimeEmpr, DBGImprimeEmpr, 'imprimeempr',
    ['Controle'], ['Controle'], istPergunta, '');
end;

procedure TFmImprime.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmImprime.GradeViewDblClick(Sender: TObject);
begin
  AlteraCampoSelecionado(QrImprimeView, False);
end;

procedure TFmImprime.QrImprimeBeforeOpen(DataSet: TDataSet);
begin
  QrImprimeCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmImprime.EdFilialChange(Sender: TObject);
begin
{
  if not EdFilial.Focused then
    ReopenParamsNFs(0);
}
end;

procedure TFmImprime.EdFilialEnter(Sender: TObject);
begin
//  FFilial := EdFilial.ValueVariant;
end;

procedure TFmImprime.EdFilialExit(Sender: TObject);
begin
{
  if EdFilial.ValueVariant <> FFilial then
    ReopenParamsNFs(0);
}
end;

procedure TFmImprime.RGOrient2Change(Sender: TObject;
  ButtonIndex: Integer);
begin
  VerificaOrientacao;
end;

procedure TFmImprime.RGOrient2Click(Sender: TObject);
begin
  VerificaOrientacao;
end;

procedure TFmImprime.VerificaOrientacao;
begin
  case RGOrient1.ItemIndex of
    0: Land1.Visible := True;
    1: Port1.Visible := True;
  end;
  case RGOrient1.ItemIndex of
    0: Port1.Visible := False;
    1: Land1.Visible := False;
  end;
  //
  case RGOrient2.ItemIndex of
    0: Land2.Visible := True;
    1: Port2.Visible := True;
  end;
  case RGOrient2.ItemIndex of
    0: Port2.Visible := False;
    1: Land2.Visible := False;
  end;
end;

procedure TFmImprime.RGOrient1Change(Sender: TObject);
begin
  VerificaOrientacao;
end;

procedure TFmImprime.RGOrient1Click(Sender: TObject);
begin
  VerificaOrientacao;
end;

procedure TFmImprime.SubQueriesReopen;
begin
  QrImprimeBand.Close;
  QrImprimeBand.Params[0].AsInteger := QrImprimeCodigo.Value;
  UnDmkDAC_PF.AbreQuery(QrImprimeBand, Dmod.MyDB);
  //
  if FBand > 0 then QrImprimeBand.Locate('Controle', FBand, []);
end;

procedure TFmImprime.NovoFormato1Click(Sender: TObject);
begin
  IncluiRegistro;
end;

procedure TFmImprime.QrImprimeBandCalcFields(DataSet: TDataSet);
begin
  case QrImprimeBandTipo.Value of
    -1: QrImprimeBandNOMETIPO.Value := '[ND]';
    00: QrImprimeBandNOMETIPO.Value := 'T�tulo do relat�rio';
    01: QrImprimeBandNOMETIPO.Value := 'Sum�rio do relat�rio';
    02: QrImprimeBandNOMETIPO.Value := 'Cabe�alho de p�gina';
    03: QrImprimeBandNOMETIPO.Value := 'Rodap� de p�gina';
    04: QrImprimeBandNOMETIPO.Value := 'Cabe�alho master';
    05: QrImprimeBandNOMETIPO.Value := 'Dados master';
    06: QrImprimeBandNOMETIPO.Value := 'Rodap� master';
    07: QrImprimeBandNOMETIPO.Value := 'Cabe�alho de detalhes';
    08: QrImprimeBandNOMETIPO.Value := 'Dados de detalhes';
    09: QrImprimeBandNOMETIPO.Value := 'Rodap� de detalhes';
    10: QrImprimeBandNOMETIPO.Value := 'Cabe�alho de sub-detalhes';
    11: QrImprimeBandNOMETIPO.Value := 'Dados de sub-detalhes';
    12: QrImprimeBandNOMETIPO.Value := 'Rodap� de sub-detalhes';
    13: QrImprimeBandNOMETIPO.Value := 'Revestimento';
    14: QrImprimeBandNOMETIPO.Value := 'Cabe�alho de coluna';
    15: QrImprimeBandNOMETIPO.Value := 'Rodap� de coluna';
    16: QrImprimeBandNOMETIPO.Value := 'Cabe�alho de grupo';
    17: QrImprimeBandNOMETIPO.Value := 'Rodap� de grupo';
    18: QrImprimeBandNOMETIPO.Value := 'Cabe�alho de dados cruzados';
    19: QrImprimeBandNOMETIPO.Value := 'Dados dos dados cruzados';
    20: QrImprimeBandNOMETIPO.Value := 'Rodap� de dados cruzados';
    21: QrImprimeBandNOMETIPO.Value := 'Filho';
  end;
  case QrImprimeBandDataset.Value of
    00: QrImprimeBandNOMEDATASET.Value := '[ND]';
    01: QrImprimeBandNOMEDATASET.Value := 'Produtos';
    02: QrImprimeBandNOMEDATASET.Value := 'Servi�os';
    03: QrImprimeBandNOMEDATASET.Value := 'Faturamento';
  end;
  case QrImprimeBandRepetencia.Value of
    -1: QrImprimeBandNOMEREPETENCIA.Value := '[ND]';
    00: QrImprimeBandNOMEREPETENCIA.Value := 'Repetir dados';
    01: QrImprimeBandNOMEREPETENCIA.Value := 'Preencher anteriores com "X"';
    02: QrImprimeBandNOMEREPETENCIA.Value := 'N�o se aplica';
    else QrImprimeBandNOMEREPETENCIA.Value := '(N�O IMPLEMENTADO)';
  end;
end;

procedure TFmImprime.MenuItem1Click(Sender: TObject);
begin
  AlteraRegistro;
end;

procedure TFmImprime.BtExcluiClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMExclui, BtExclui);
end;

procedure TFmImprime.IncluiBanda1Click(Sender: TObject);
begin
  MyObjects.CriaForm_AcessoTotal(TFmImprimeBand, FmImprimeBand);
  FmImprimeBand.ImgTipo.SQLType := stIns;
  FmImprimeBand.FCodigo        := QrImprimeCodigo.Value;
  FmImprimeBand.FControle      := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres',
                                  'Controle', 'ImprimeBand', 'ImprimeBand',
                                  'Codigo');
  FmImprimeBand.ShowModal;
  FmImprimeBand.Destroy;
  SubQueriesReopen;
  AtualizaOrdemBandas;
end;

procedure TFmImprime.IncluiCampo1Click(Sender: TObject);
begin
  MyObjects.CriaForm_AcessoTotal(TFmImprimeView, FmImprimeView);
  FmImprimeView.ImgTipo.SQLType := stIns;
  FmImprimeView.FBand          := QrImprimeBandControle.Value;
  FmImprimeView.FCodigo        := QrImprimeCodigo.Value;
  FmImprimeView.FControle      := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres',
                                  'Controle', 'ImprimeView', 'ImprimeView',
                                  'Codigo');
  FmImprimeView.EdTopoBanda.Text := IntToStr(QrImprimeViewTOPOBANDA.Value);
  FmImprimeView.FPageMEsq := QrImprimePos_MEsq.Value;
  FmImprimeView.ShowModal;
  FmImprimeView.Destroy;
  SubQueriesReopen;
end;

procedure TFmImprime.MenuItem3Click(Sender: TObject);
begin
  MyObjects.CriaForm_AcessoTotal(TFmImprimeBand, FmImprimeBand);
  FmImprimeBand.ImgTipo.SQLType      := stUpd;
  FmImprimeBand.EdNome.Text         := QrImprimeBandNome.Value;
  FmImprimeBand.EdTopo.Text         := IntToStr(QrImprimeBandPos_Topo.Value);
  FmImprimeBand.EdAltura.Text       := IntToStr(QrImprimeBandPos_Altu.Value);
  FmImprimeBand.EdColQtd.Text       := IntToStr(QrImprimeBandCol_Qtd.Value);
  FmImprimeBand.EdColLar.Text       := IntToStr(QrImprimeBandCol_Lar.Value);
  FmImprimeBand.EdColGAP.Text       := IntToStr(QrImprimeBandCol_GAP.Value);
  FmImprimeBand.EdLinhas.Text       := IntToStr(QrImprimeBandLinhas.Value);
  FmImprimeBand.RGTipo.ItemIndex    := QrImprimeBandTipo.Value;
  FmImprimeBand.RGDataset.ItemIndex := QrImprimeBandDataset.Value;
  FmImprimeBand.RGMultipaginas.ItemIndex := QrImprimeBandRepetencia.Value;
  FmImprimeBand.FAntTopo            := QrImprimeBandPos_Topo.Value;
  FmImprimeBand.FCodigo             := QrImprimeBandCodigo.Value;
  FmImprimeBand.FControle           := QrImprimeBandControle.Value;
  FmImprimeBand.ShowModal;
  FmImprimeBand.Destroy;
  SubQueriesReopen;
  AtualizaOrdemBandas;
end;

procedure TFmImprime.QrImprimeViewCalcFields(DataSet: TDataSet);
var
  Align: Integer;
begin
  Align := (QrImprimeViewSet_TAli.Value * 100) + QrImprimeViewSet_BAli.Value;
  case Align of
    000: QrImprimeViewNOMEALIGN.Value := 'Esquerda no topo';
    001: QrImprimeViewNOMEALIGN.Value := 'Esquerda no centro';
    002: QrImprimeViewNOMEALIGN.Value := 'Esquerda em baixo';
    100: QrImprimeViewNOMEALIGN.Value := 'Centro no topo';
    101: QrImprimeViewNOMEALIGN.Value := 'Centralizado';
    102: QrImprimeViewNOMEALIGN.Value := 'Centro em baixo';
    200: QrImprimeViewNOMEALIGN.Value := 'Direita no topo';
    201: QrImprimeViewNOMEALIGN.Value := 'Direita no centro';
    202: QrImprimeViewNOMEALIGN.Value := 'Direita em baixo';
  end;
  if QrImprimeViewSet_Ital.Value = 0 then
  QrImprimeViewNOMEITAL.Value := 'N' else QrImprimeViewNOMEITAL.Value := 'S';
  if QrImprimeViewSet_Negr.Value = 0 then
  QrImprimeViewNOMENEGR.Value := 'N' else QrImprimeViewNOMENEGR.Value := 'S';
  if QrImprimeViewSet_Unde.Value = 0 then
  QrImprimeViewNOMESUBL.Value := 'N' else QrImprimeViewNOMESUBL.Value := 'S';
  ///
  case QrImprimeViewTab_Tipo.Value of
    0: QrImprimeViewNOMETABELA.Value := '---';
    1: QrImprimeViewNOMETABELA.Value := 'Entidades';
    2: QrImprimeViewNOMETABELA.Value := 'Produtoss';
    3: QrImprimeViewNOMETABELA.Value := 'Servicos';
    4: QrImprimeViewNOMETABELA.Value := 'Faturamento';
    5: QrImprimeViewNOMETABELA.Value := 'Transportador';
    6: QrImprimeViewNOMETABELA.Value := 'Duplicata';
    7: QrImprimeViewNOMETABELA.Value := 'NF Formato B';
    8: QrImprimeViewNOMETABELA.Value := 'Lista de NCMs';
    9: QrImprimeViewNOMETABELA.Value := 'NF Formato C';
    else QrImprimeViewNOMETABELA.Value := '?';
  end;
  case QrImprimeViewTab_Tipo.Value of
    0:
    begin
      case QrImprimeViewTab_Campo.Value of
        00: QrImprimeViewNOMECAMPO.Value := '- - - - -';
        01: QrImprimeViewNOMECAMPO.Value := 'Sa�da [X]';
        02: QrImprimeViewNOMECAMPO.Value := 'Entrada [X]';
        03: QrImprimeViewNOMECAMPO.Value := 'Data emiss�o';
        04: QrImprimeViewNOMECAMPO.Value := 'Data entra/sai';
        05: QrImprimeViewNOMECAMPO.Value := 'C�digo CFOP';
        06: QrImprimeViewNOMECAMPO.Value := 'Descri��o CFOP';
        07: QrImprimeViewNOMECAMPO.Value := 'Base c�lculo ICMS';
        08: QrImprimeViewNOMECAMPO.Value := 'Valor ICMS';
        09: QrImprimeViewNOMECAMPO.Value := 'Base c�lc. ICMS subst.';
        10: QrImprimeViewNOMECAMPO.Value := 'Valor ICMS subst.';
        11: QrImprimeViewNOMECAMPO.Value := 'Valor frete';
        12: QrImprimeViewNOMECAMPO.Value := 'Valor seguro';
        13: QrImprimeViewNOMECAMPO.Value := 'Outras desp. aces.';
        14: QrImprimeViewNOMECAMPO.Value := 'Valor total IPI';
        15: QrImprimeViewNOMECAMPO.Value := 'Valor total produtos';
        16: QrImprimeViewNOMECAMPO.Value := 'Valor total servicos';
        17: QrImprimeViewNOMECAMPO.Value := 'Valor total nota';
        18: QrImprimeViewNOMECAMPO.Value := 'Placa ve�culo';
        19: QrImprimeViewNOMECAMPO.Value := 'UF placa ve�culo';
        20: QrImprimeViewNOMECAMPO.Value := 'Vol. Quantidade';
        21: QrImprimeViewNOMECAMPO.Value := 'Vol. Esp�cie';
        22: QrImprimeViewNOMECAMPO.Value := 'Vol. Marca';
        23: QrImprimeViewNOMECAMPO.Value := 'Vol. N�mero';
        24: QrImprimeViewNOMECAMPO.Value := 'Vol. kg bruto';
        25: QrImprimeViewNOMECAMPO.Value := 'Vol. kg l�quido';
        26: QrImprimeViewNOMECAMPO.Value := 'Dados adicionais';
        27: QrImprimeViewNOMECAMPO.Value := 'Nota Fiscal';
        28: QrImprimeViewNOMECAMPO.Value := 'Frete por conta de ...';
        29: QrImprimeViewNOMECAMPO.Value := 'Desconto especial';
        else QrImprimeViewNOMECAMPO.Value := '?';
      end;
    end;
    1:
    begin
      case QrImprimeViewTab_Campo.Value of
        0: QrImprimeViewNOMECAMPO.Value := 'Nome ou Raz�o Social';
        1: QrImprimeViewNOMECAMPO.Value := 'CNPJ ou CPF';
        2: QrImprimeViewNOMECAMPO.Value := 'Endere�o';
        3: QrImprimeViewNOMECAMPO.Value := 'Bairro';
        4: QrImprimeViewNOMECAMPO.Value := 'CEP';
        5: QrImprimeViewNOMECAMPO.Value := 'Cidade';
        6: QrImprimeViewNOMECAMPO.Value := 'Telefone';
        7: QrImprimeViewNOMECAMPO.Value := 'UF';
        8: QrImprimeViewNOMECAMPO.Value := 'I.E. ou RG';
        9: QrImprimeViewNOMECAMPO.Value := 'I.E.S.T.';
        else QrImprimeViewNOMECAMPO.Value := '?';
      end;
    end;
    2:
    begin
      case QrImprimeViewTab_Campo.Value of
        0: QrImprimeViewNOMECAMPO.Value := 'C�digo';
        1: QrImprimeViewNOMECAMPO.Value := 'Descri��o';
        2: QrImprimeViewNOMECAMPO.Value := 'Classifica��o Fiscal';
        3: QrImprimeViewNOMECAMPO.Value := 'Situa��o tribut�ria';
        4: QrImprimeViewNOMECAMPO.Value := 'Unidade';
        5: QrImprimeViewNOMECAMPO.Value := 'Quantidade';
        6: QrImprimeViewNOMECAMPO.Value := 'Valor unit�rio';
        7: QrImprimeViewNOMECAMPO.Value := 'Valor Total';
        8: QrImprimeViewNOMECAMPO.Value := 'Al�quota de ICMS';
        9: QrImprimeViewNOMECAMPO.Value := 'Al�quota de IPI';
        10: QrImprimeViewNOMECAMPO.Value := 'Valor do IPI';
        11: QrImprimeViewNOMECAMPO.Value := 'CFOP';
        12: QrImprimeViewNOMECAMPO.Value := 'Refer�ncia';
        else QrImprimeViewNOMECAMPO.Value := '?';
      end;
    end;
    3:
    begin
      case QrImprimeViewTab_Campo.Value of
        0: QrImprimeViewNOMECAMPO.Value := 'C�digo';
        1: QrImprimeViewNOMECAMPO.Value := 'Descri��o';
        2: QrImprimeViewNOMECAMPO.Value := 'Classifica��o Fiscal';
        3: QrImprimeViewNOMECAMPO.Value := 'Situa��o tribut�ria';
        4: QrImprimeViewNOMECAMPO.Value := 'Unidade';
        5: QrImprimeViewNOMECAMPO.Value := 'Quantidade';
        6: QrImprimeViewNOMECAMPO.Value := 'Valor unit�rio';
        7: QrImprimeViewNOMECAMPO.Value := 'Valor Total';
        8: QrImprimeViewNOMECAMPO.Value := 'Al�quota de ICMS';
        9: QrImprimeViewNOMECAMPO.Value := 'Al�quota de IPI';
        10: QrImprimeViewNOMECAMPO.Value := 'Valor do IPI';
        11: QrImprimeViewNOMECAMPO.Value := 'CFOP';
        12: QrImprimeViewNOMECAMPO.Value := 'Refer�ncia';
        else QrImprimeViewNOMECAMPO.Value := '?';
      end;
    end;
    4:
    begin
      case QrImprimeViewTab_Campo.Value of
        0: QrImprimeViewNOMECAMPO.Value := 'Parcela';
        1: QrImprimeViewNOMECAMPO.Value := 'Valor';
        2: QrImprimeViewNOMECAMPO.Value := 'Vencimento';
        else QrImprimeViewNOMECAMPO.Value := '?';
      end;
    end;
    5:
    begin
      case QrImprimeViewTab_Campo.Value of
        0: QrImprimeViewNOMECAMPO.Value := 'Nome ou Raz�o Social';
        1: QrImprimeViewNOMECAMPO.Value := 'CNPJ ou CPF';
        2: QrImprimeViewNOMECAMPO.Value := 'Endere�o';
        3: QrImprimeViewNOMECAMPO.Value := 'Bairro';
        4: QrImprimeViewNOMECAMPO.Value := 'CEP';
        5: QrImprimeViewNOMECAMPO.Value := 'Cidade';
        6: QrImprimeViewNOMECAMPO.Value := 'Telefone';
        7: QrImprimeViewNOMECAMPO.Value := 'UF';
        8: QrImprimeViewNOMECAMPO.Value := 'I.E. ou RG';
        9: QrImprimeViewNOMECAMPO.Value := 'I.E.S.T.';
        else QrImprimeViewNOMECAMPO.Value := '?';
      end;
    end;
    6:
    begin
      case QrImprimeViewTab_Campo.Value of
        00: QrImprimeViewNOMECAMPO.Value := 'Dia emiss�o';
        01: QrImprimeViewNOMECAMPO.Value := 'M�s emiss�o';
        02: QrImprimeViewNOMECAMPO.Value := 'Ano emiss�o';
        03: QrImprimeViewNOMECAMPO.Value := 'Valor fatura';
        04: QrImprimeViewNOMECAMPO.Value := 'N�mero fatura';
        05: QrImprimeViewNOMECAMPO.Value := 'Valor duplicata';
        06: QrImprimeViewNOMECAMPO.Value := 'N�mero de ordem';
        07: QrImprimeViewNOMECAMPO.Value := 'Vencimento';
        08: QrImprimeViewNOMECAMPO.Value := 'Desconto de %';
        09: QrImprimeViewNOMECAMPO.Value := 'Desconto de $';
        10: QrImprimeViewNOMECAMPO.Value := 'Desconto at�';
        11: QrImprimeViewNOMECAMPO.Value := 'Condi��es especiais';
        12: QrImprimeViewNOMECAMPO.Value := 'Sacado';
        13: QrImprimeViewNOMECAMPO.Value := 'Endere�o';
        14: QrImprimeViewNOMECAMPO.Value := 'Munic�pio';
        15: QrImprimeViewNOMECAMPO.Value := 'Estado';
        16: QrImprimeViewNOMECAMPO.Value := 'P�a. Pgto';
        17: QrImprimeViewNOMECAMPO.Value := 'CEP';
        18: QrImprimeViewNOMECAMPO.Value := 'CNPJ/CPF';
        19: QrImprimeViewNOMECAMPO.Value := 'I.E.';
        20: QrImprimeViewNOMECAMPO.Value := 'Valor extenso';
        else QrImprimeViewNOMECAMPO.Value := '?';
      end;
    end;
    7:
    begin
      case QrImprimeViewTab_Campo.Value of
        00: QrImprimeViewNOMECAMPO.Value := '- - - - -';
        01: QrImprimeViewNOMECAMPO.Value := 'Sa�da [X]';
        02: QrImprimeViewNOMECAMPO.Value := 'Entrada [X]';
        03: QrImprimeViewNOMECAMPO.Value := 'Data emiss�o';
        04: QrImprimeViewNOMECAMPO.Value := 'Data entra/sai';
        05: QrImprimeViewNOMECAMPO.Value := 'C�d CFOP �nico';
        06: QrImprimeViewNOMECAMPO.Value := 'C�d CFOP 1';
        07: QrImprimeViewNOMECAMPO.Value := 'C�d CFOP 2';
        08: QrImprimeViewNOMECAMPO.Value := 'Descr. CFOP �nico';
        09: QrImprimeViewNOMECAMPO.Value := 'Descr. CFOP 1';
        10: QrImprimeViewNOMECAMPO.Value := 'Descr. CFOP 2';
        11: QrImprimeViewNOMECAMPO.Value := 'I.E. Subst. Trib.';
        12: QrImprimeViewNOMECAMPO.Value := 'Cliente Nome';
        13: QrImprimeViewNOMECAMPO.Value := 'Cliente CNPJ/CPF';
        14: QrImprimeViewNOMECAMPO.Value := 'Cliente Endere�o';
        15: QrImprimeViewNOMECAMPO.Value := 'Cliente Bairro';
        16: QrImprimeViewNOMECAMPO.Value := 'Cliente CEP';
        17: QrImprimeViewNOMECAMPO.Value := 'Cliente Munic�pio';
        18: QrImprimeViewNOMECAMPO.Value := 'Cliente Fone/Fax';
        19: QrImprimeViewNOMECAMPO.Value := 'Cliente UF';
        20: QrImprimeViewNOMECAMPO.Value := 'Cliente I.E.';
        21: QrImprimeViewNOMECAMPO.Value := 'Fat. Num. col. 1';
        22: QrImprimeViewNOMECAMPO.Value := 'Fat. Valor col. 1';
        23: QrImprimeViewNOMECAMPO.Value := 'Fat. Data col. 1';
        24: QrImprimeViewNOMECAMPO.Value := 'Fat. Num. col. 2';
        25: QrImprimeViewNOMECAMPO.Value := 'Fat. Valor col. 2';
        26: QrImprimeViewNOMECAMPO.Value := 'Fat. Data col. 2';
        27: QrImprimeViewNOMECAMPO.Value := 'Fat. Num. col. 3';
        28: QrImprimeViewNOMECAMPO.Value := 'Fat. Valor col. 3';
        29: QrImprimeViewNOMECAMPO.Value := 'Fat. Data col. 3';
        30: QrImprimeViewNOMECAMPO.Value := 'Desconto de ...';
        31: QrImprimeViewNOMECAMPO.Value := 'Desconto at� ...';
        32: QrImprimeViewNOMECAMPO.Value := 'Pra�a pagamento';
        33: QrImprimeViewNOMECAMPO.Value := 'Prod. C�digo';
        34: QrImprimeViewNOMECAMPO.Value := 'Prod. Descri��o';
        35: QrImprimeViewNOMECAMPO.Value := 'Prod. Sit.Trib.';
        36: QrImprimeViewNOMECAMPO.Value := 'Prod. Unidade';
        37: QrImprimeViewNOMECAMPO.Value := 'Prod. Quantidade';
        38: QrImprimeViewNOMECAMPO.Value := 'Prod. Pre�o';
        39: QrImprimeViewNOMECAMPO.Value := 'Prod. Valor total';
        40: QrImprimeViewNOMECAMPO.Value := 'Prod. % ICMS';
        41: QrImprimeViewNOMECAMPO.Value := 'Base c�lculo ICMS';
        42: QrImprimeViewNOMECAMPO.Value := 'Valor ICMS';
        43: QrImprimeViewNOMECAMPO.Value := 'Base c�lc. ICMS sub.';
        44: QrImprimeViewNOMECAMPO.Value := 'Valor ICMS subst.';
        45: QrImprimeViewNOMECAMPO.Value := 'Valor total produtos';
        46: QrImprimeViewNOMECAMPO.Value := 'Valor frete';
        47: QrImprimeViewNOMECAMPO.Value := 'Valor seguro';
        48: QrImprimeViewNOMECAMPO.Value := 'Outras desp. aces.';
        49: QrImprimeViewNOMECAMPO.Value := '?';
        50: QrImprimeViewNOMECAMPO.Value := 'Valor total nota';
        51: QrImprimeViewNOMECAMPO.Value := 'Transp. Nome';
        52: QrImprimeViewNOMECAMPO.Value := 'Transp. FPC';
        53: QrImprimeViewNOMECAMPO.Value := 'Placa Ve�culo';
        54: QrImprimeViewNOMECAMPO.Value := 'UF placa ve�culo';
        55: QrImprimeViewNOMECAMPO.Value := 'Transp. CNPJ/CPF';
        56: QrImprimeViewNOMECAMPO.Value := 'Transp. Endere�o';
        57: QrImprimeViewNOMECAMPO.Value := 'Transp. Munic�pio';
        58: QrImprimeViewNOMECAMPO.Value := 'Transp. UF';
        59: QrImprimeViewNOMECAMPO.Value := 'Transp. I.E.';
        60: QrImprimeViewNOMECAMPO.Value := 'Vol. Quantidade';
        61: QrImprimeViewNOMECAMPO.Value := 'Vol. Esp�cie';
        62: QrImprimeViewNOMECAMPO.Value := 'Vol. Marca';
        63: QrImprimeViewNOMECAMPO.Value := 'Vol. N�mero';
        64: QrImprimeViewNOMECAMPO.Value := 'Vol. kg bruto';
        65: QrImprimeViewNOMECAMPO.Value := 'Vol. kg l�quido';
        66: QrImprimeViewNOMECAMPO.Value := 'Dados adicionais';
        67: QrImprimeViewNOMECAMPO.Value := 'Valor extenso';
        else QrImprimeViewNOMECAMPO.Value := '?';
      end;
    end;
    8:
    begin
      case QrImprimeViewTab_Campo.Value of
        00: QrImprimeViewNOMECAMPO.Value := '- - - - -';
        01: QrImprimeViewNOMECAMPO.Value := 'Letra coluna 1';
        02: QrImprimeViewNOMECAMPO.Value := 'NCM coluna 2';
        03: QrImprimeViewNOMECAMPO.Value := 'Letra coluna 2';
        04: QrImprimeViewNOMECAMPO.Value := 'NCM coluna 2';
        05: QrImprimeViewNOMECAMPO.Value := 'Letra coluna 3';
        06: QrImprimeViewNOMECAMPO.Value := 'NCM coluna 3';
        07: QrImprimeViewNOMECAMPO.Value := 'Letra coluna 4';
        08: QrImprimeViewNOMECAMPO.Value := 'NCM coluna 4';
        09: QrImprimeViewNOMECAMPO.Value := 'Letra coluna 5';
        10: QrImprimeViewNOMECAMPO.Value := 'NCM coluna 5';
        11: QrImprimeViewNOMECAMPO.Value := 'Letra coluna 6';
        12: QrImprimeViewNOMECAMPO.Value := 'NCM coluna 6';
        else QrImprimeViewNOMECAMPO.Value := '?';
      end;
    end;
    9:
    begin
      case QrImprimeViewTab_Campo.Value of
        00: QrImprimeViewNOMECAMPO.Value := '- - - - -';
        01: QrImprimeViewNOMECAMPO.Value := 'Sa�da [X]';
        02: QrImprimeViewNOMECAMPO.Value := 'Entrada [X]';
        03: QrImprimeViewNOMECAMPO.Value := 'Data emiss�o';
        04: QrImprimeViewNOMECAMPO.Value := 'Data entra/sai';
        05: QrImprimeViewNOMECAMPO.Value := 'C�d CFOP �nico';
        06: QrImprimeViewNOMECAMPO.Value := 'C�d CFOP 1';
        07: QrImprimeViewNOMECAMPO.Value := 'C�d CFOP 2';
        08: QrImprimeViewNOMECAMPO.Value := 'Descr. CFOP �nico';
        09: QrImprimeViewNOMECAMPO.Value := 'Descr. CFOP 1';
        10: QrImprimeViewNOMECAMPO.Value := 'Descr. CFOP 2';
        11: QrImprimeViewNOMECAMPO.Value := 'I.E. Subst. Trib.';
        12: QrImprimeViewNOMECAMPO.Value := 'Cliente Nome';
        13: QrImprimeViewNOMECAMPO.Value := 'Cliente CNPJ/CPF';
        14: QrImprimeViewNOMECAMPO.Value := 'Cliente Endere�o';
        15: QrImprimeViewNOMECAMPO.Value := 'Cliente Bairro';
        16: QrImprimeViewNOMECAMPO.Value := 'Cliente CEP';
        17: QrImprimeViewNOMECAMPO.Value := 'Cliente Munic�pio';
        18: QrImprimeViewNOMECAMPO.Value := 'Cliente Fone/Fax';
        19: QrImprimeViewNOMECAMPO.Value := 'Cliente UF';
        20: QrImprimeViewNOMECAMPO.Value := 'Cliente I.E.';
        21: QrImprimeViewNOMECAMPO.Value := 'Fat. Seq. col. 1';
        22: QrImprimeViewNOMECAMPO.Value := 'Fat. Dupl. col. 1';
        23: QrImprimeViewNOMECAMPO.Value := 'Fat. Valor col. 1';
        24: QrImprimeViewNOMECAMPO.Value := 'Fat. Data col. 1';
        25: QrImprimeViewNOMECAMPO.Value := 'Fat. Seq. col. 2';
        26: QrImprimeViewNOMECAMPO.Value := 'Fat. Dupl. col. 2';
        27: QrImprimeViewNOMECAMPO.Value := 'Fat. Valor col. 2';
        28: QrImprimeViewNOMECAMPO.Value := 'Fat. Data col. 2';
        29: QrImprimeViewNOMECAMPO.Value := 'Fat. Seq. col. 3';
        30: QrImprimeViewNOMECAMPO.Value := 'Fat. Dupl. col. 3';
        31: QrImprimeViewNOMECAMPO.Value := 'Fat. Valor col. 3';
        32: QrImprimeViewNOMECAMPO.Value := 'Fat. Data col. 3';
        33: QrImprimeViewNOMECAMPO.Value := 'Fat. Seq. col. 4';
        34: QrImprimeViewNOMECAMPO.Value := 'Fat. Dupl. col. 4';
        35: QrImprimeViewNOMECAMPO.Value := 'Fat. Valor col. 4';
        36: QrImprimeViewNOMECAMPO.Value := 'Fat. Data col. 4';
        37: QrImprimeViewNOMECAMPO.Value := 'Fat. Seq. col. 5';
        38: QrImprimeViewNOMECAMPO.Value := 'Fat. Dupl. col. 5';
        39: QrImprimeViewNOMECAMPO.Value := 'Fat. Valor col. 5';
        40: QrImprimeViewNOMECAMPO.Value := 'Fat. Data col. 5';
        41: QrImprimeViewNOMECAMPO.Value := 'Fat. Seq. col. 6';
        42: QrImprimeViewNOMECAMPO.Value := 'Fat. Dupl. col. 6';
        43: QrImprimeViewNOMECAMPO.Value := 'Fat. Valor col. 6';
        44: QrImprimeViewNOMECAMPO.Value := 'Fat. Data col. 6';
        45: QrImprimeViewNOMECAMPO.Value := 'Desconto de ...';
        46: QrImprimeViewNOMECAMPO.Value := 'Desconto at� ...';
        47: QrImprimeViewNOMECAMPO.Value := 'Pra�a pagamento';
        48: QrImprimeViewNOMECAMPO.Value := 'Prod. C�digo';
        49: QrImprimeViewNOMECAMPO.Value := 'Prod. Descri��o';
        50: QrImprimeViewNOMECAMPO.Value := 'Prod. Sit.Trib.';
        51: QrImprimeViewNOMECAMPO.Value := 'Prod. Class. Fisc.';
        52: QrImprimeViewNOMECAMPO.Value := 'Prod. Letra da NCM';
        53: QrImprimeViewNOMECAMPO.Value := 'Prod. Unidade';
        54: QrImprimeViewNOMECAMPO.Value := 'Prod. Quantidade';
        55: QrImprimeViewNOMECAMPO.Value := 'Prod. Pre�o';
        56: QrImprimeViewNOMECAMPO.Value := 'Prod. Valor total';
        57: QrImprimeViewNOMECAMPO.Value := 'Prod. % ICMS';
        58: QrImprimeViewNOMECAMPO.Value := 'Prod. % IPI';
        59: QrImprimeViewNOMECAMPO.Value := 'Prod. Valor IPI';
        60: QrImprimeViewNOMECAMPO.Value := 'Base c�lculo ICMS';
        61: QrImprimeViewNOMECAMPO.Value := 'Valor ICMS';
        62: QrImprimeViewNOMECAMPO.Value := 'Base c�lc. ICMS sub.';
        63: QrImprimeViewNOMECAMPO.Value := 'Valor ICMS subst.';
        64: QrImprimeViewNOMECAMPO.Value := 'Valor total produtos';
        65: QrImprimeViewNOMECAMPO.Value := 'Valor frete';
        66: QrImprimeViewNOMECAMPO.Value := 'Valor seguro';
        67: QrImprimeViewNOMECAMPO.Value := 'Outras desp. aces.';
        68: QrImprimeViewNOMECAMPO.Value := 'Valor total nota';
        69: QrImprimeViewNOMECAMPO.Value := 'Transp. Nome';
        70: QrImprimeViewNOMECAMPO.Value := 'Transp. FPC';
        71: QrImprimeViewNOMECAMPO.Value := 'Placa Ve�culo';
        72: QrImprimeViewNOMECAMPO.Value := 'UF placa ve�culo';
        73: QrImprimeViewNOMECAMPO.Value := 'Transp. CNPJ/CPF';
        74: QrImprimeViewNOMECAMPO.Value := 'Transp. Endere�o';
        75: QrImprimeViewNOMECAMPO.Value := 'Transp. Munic�pio';
        76: QrImprimeViewNOMECAMPO.Value := 'Transp. UF';
        77: QrImprimeViewNOMECAMPO.Value := 'Transp. I.E.';
        78: QrImprimeViewNOMECAMPO.Value := 'Vol. Quantidade';
        79: QrImprimeViewNOMECAMPO.Value := 'Vol. Esp�cie';
        80: QrImprimeViewNOMECAMPO.Value := 'Vol. Marca';
        81: QrImprimeViewNOMECAMPO.Value := 'Vol. N�mero';
        82: QrImprimeViewNOMECAMPO.Value := 'Vol. kg bruto';
        83: QrImprimeViewNOMECAMPO.Value := 'Vol. kg l�quido';
        84: QrImprimeViewNOMECAMPO.Value := 'Dados adicionais';
        85: QrImprimeViewNOMECAMPO.Value := 'Valor extenso';
        86: QrImprimeViewNOMECAMPO.Value := 'Dados corpo NF';
        87: QrImprimeViewNOMECAMPO.Value := 'NCM col. 1 Letra';
        88: QrImprimeViewNOMECAMPO.Value := 'NCM col. 1 NCM';
        89: QrImprimeViewNOMECAMPO.Value := 'NCM col. 2 Letra';
        90: QrImprimeViewNOMECAMPO.Value := 'NCM col. 2 NCM';
        91: QrImprimeViewNOMECAMPO.Value := 'NCM col. 3 Letra';
        92: QrImprimeViewNOMECAMPO.Value := 'NCM col. 3 NCM';
        93: QrImprimeViewNOMECAMPO.Value := 'NCM col. 4 Letra';
        94: QrImprimeViewNOMECAMPO.Value := 'NCM col. 4 NCM';
        95: QrImprimeViewNOMECAMPO.Value := 'NCM col. 5 Letra';
        96: QrImprimeViewNOMECAMPO.Value := 'NCM col. 5 NCM';
        97: QrImprimeViewNOMECAMPO.Value := 'NCM col. 6 Letra';
        98: QrImprimeViewNOMECAMPO.Value := 'NCM col. 6 NCM';
        99: QrImprimeViewNOMECAMPO.Value := 'C�d. representante';
        100: QrImprimeViewNOMECAMPO.Value := 'N� Pedido';
        101: QrImprimeViewNOMECAMPO.Value := 'Condi��o de pagto';
        else QrImprimeViewNOMECAMPO.Value := '?';
      end;
    end;
  end;
  case QrImprimeViewFormato.Value of
    0: QrImprimeViewNOMEFORMATO.Value := 'Texto';
    1: QrImprimeViewNOMEFORMATO.Value := 'Inteiro';
    2: QrImprimeViewNOMEFORMATO.Value := 'N�mero 0,0';
    3: QrImprimeViewNOMEFORMATO.Value := 'N�mero 0,00';
    4: QrImprimeViewNOMEFORMATO.Value := 'N�mero 0,000';
    5: QrImprimeViewNOMEFORMATO.Value := 'N�mero 0,0000';
    6: QrImprimeViewNOMEFORMATO.Value := 'Data "dd/mm/aa"';
    7: QrImprimeViewNOMEFORMATO.Value := 'Data "dd/mm/aaaa"';
    8: QrImprimeViewNOMEFORMATO.Value := 'Hora "hh:nn"';
    9: QrImprimeViewNOMEFORMATO.Value := 'Hora "hh:nn:ss"';
    else QrImprimeViewNOMEFORMATO.Value := '?';
  end;
  //
  QrImprimeViewTOPOREAL.Value :=
  QrImprimeViewPos_Topo.Value + QrImprimeViewTOPOBANDA.Value;
end;

procedure TFmImprime.ExcluiBanda1Click(Sender: TObject);
begin
  if Geral.MB_Pergunta('Confirma a exclus�o da bada selecionada?') = ID_YES then
  begin
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('DELETE FROM imprimeband WHERE Codigo=:P0 AND Controle=:P1');
    Dmod.QrUpd.Params[0].AsInteger := QrImprimeBandCodigo.Value;
    Dmod.QrUpd.Params[1].AsInteger := QrImprimeBandControle.Value;
    Dmod.QrUpd.ExecSQL;
    SubqueriesReopen;
  end;
end;

procedure TFmImprime.ExcluiCampo1Click(Sender: TObject);
begin
  DBCheck.QuaisItens_Exclui(Dmod.QrUpd, QrImprimeView, GradeView,
    'imprimeview', ['Codigo', 'Controle'], ['Codigo', 'Controle'], istPergunta, '');
  {
  if Geral.MB_Pergunta(
  'Confirma a exclus�o do campo selecionado?') = ID_YES then
  begin
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('DELETE FROM imprimeview WHERE Codigo=:P0 AND Controle=:P1');
    Dmod.QrUpd.Params[0].AsInteger := QrImprimeViewCodigo.Value;
    Dmod.QrUpd.Params[1].AsInteger := QrImprimeViewControle.Value;
    Dmod.QrUpd.ExecSQL;
    SubqueriesReopen;
  end;
}
end;

procedure TFmImprime.PMIncluiPopup(Sender: TObject);
begin
  if QrImprimeBand.State in [dsBrowse] then
  begin
    if QrImprimeBand.RecordCount = 0 then
      IncluiCampo1.Enabled := False
    else
      IncluiCampo1.Enabled := True;
    //
    if QrImprimeBand.RecordCount = 0 then
      IncluiCampo2.Enabled := False
    else
      IncluiCampo2.Enabled := True;
  end else
    IncluiCampo2.Enabled := False;
  if QrImprimeView.State in [dsBrowse] then
  begin
    if QrImprimeView.RecordCount = 0 then
      IncluiCampo2.Enabled := False
    else
      IncluiCampo2.Enabled := True;
  end else
    IncluiCampo2.Enabled := False;
end;

procedure TFmImprime.QrImprimeBandAfterScroll(DataSet: TDataSet);
begin
  ReopenImprimeView(0);
end;

procedure TFmImprime.ReopenImprimeEmpr(Controle: Integer);
begin
  QrImprimeEmpr.Close;
  QrImprimeEmpr.Params[0].AsInteger := QrImprimeCodigo.Value;
  UnDmkDAC_PF.AbreQuery(QrImprimeEmpr, Dmod.MyDB);
  //
  QrImprimeEmpr.Locate('Controle', Controle, []);
end;

procedure TFmImprime.ReopenImprimeView(Controle: Integer);
begin
  QrImprimeView.Close;
  QrImprimeView.Params[0].AsInteger := QrImprimeCodigo.Value;
  QrImprimeView.Params[1].AsInteger := QrImprimeBandControle.Value;
  UnDmkDAC_PF.AbreQuery(QrImprimeView, Dmod.MyDB);
  //
  if Controle > 0 then
    QrImprimeView.Locate('Controle', Controle, [])
  else
  if FView > 0 then
    QrImprimeView.Locate('Controle', FView, []);
end;

{
procedure TFmImprime.ReopenParamsNFs(Controle: Integer);
begin
  QrParamsNFs.Close;
  QrParamsNFs.Params[0].AsInteger := QrFiliaisCodigo.Value;
  UnDmkDAC_PF.AbreQuery(QrParamsNFs, Dmod.MyDB);
  QrParamsNFs.Locate('Controle', Controle, []);
  //
  EdSerieNF.ValueVariant := Null;
  CBSerieNF.KeyValue     := Null;
end;
}

procedure TFmImprime.PMAlteraPopup(Sender: TObject);
var
  Enab, Enab2, Enab3: Boolean;
begin
  Enab  := (QrImprime.State <> dsInactive) and (QrImprime.RecordCount > 0);
  Enab2 := (QrImprimeBand.State <> dsInactive) and (QrImprimeBand.RecordCount >0);
  Enab3 := (QrImprimeView.State <> dsInactive) and (QrImprimeView.RecordCount > 0);
  //
  MenuItem1.Enabled      := Enab;
  MenuItem3.Enabled      := Enab2;
  MenuItem4.Enabled      := Enab3;
  SerieNFFilial2.Enabled := False;
end;

procedure TFmImprime.PMExcluiPopup(Sender: TObject);
var
  Enab, Enab2, Enab3: Boolean;
begin
  Enab  := (QrImprimeBand.State <> dsInactive) and (QrImprimeBand.RecordCount >0);
  Enab2 := (QrImprimeView.State <> dsInactive) and (QrImprimeView.RecordCount >0);
  Enab3 := (QrImprimeEmpr.State <> dsInactive) and (QrImprimeEmpr.RecordCount >0);
  //
  MenuItem5.Enabled      := False;
  ExcluiBanda1.Enabled   := Enab;
  ExcluiCampo1.Enabled   := Enab2;
  SerieNFFilial3.Enabled := Enab3;
end;

procedure TFmImprime.AtualizaOrdemBandas;
var
  Ordem: Integer;
begin
  Ordem := 0;
  QrImprimeBand.DisableControls;
  FBand := QrImprimeBandControle.Value;
  QrImprimeBand.First;
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('UPDATE imprimeband SET ');
  Dmod.QrUpd.SQL.Add('Ordem=:P0 WHERE Codigo=:P1 AND Controle=:P2');
  while not QrImprimeBand.Eof do
  begin
    Ordem := Ordem + 1;
    Dmod.QrUpd.Params[0].AsInteger := Ordem;
    Dmod.QrUpd.Params[1].AsInteger := QrImprimeBandCodigo.Value;
    Dmod.QrUpd.Params[2].AsInteger := QrImprimeBandControle.Value;
    Dmod.QrUpd.ExecSQL;
    QrImprimeBand.Next;
  end;
  QrImprimeBand.EnableControls;
  SubQueriesReopen;
end;

procedure TFmImprime.DuplicaRegistro;
var
  Codigo, BandControle, ViewControle : Integer;
begin
  Codigo := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
    'Imprime', 'Imprime', 'Codigo');
  Dmod.QrUpdU.SQL.Clear;
  Dmod.QrUpdU.SQL.Add('INSERT INTO imprime SET ');
  Dmod.QrUpdU.SQL.Add('Nome=:P0, Titulo=:P1, Pos_Topo=:P2, Pos_Altu=:P3,');
  Dmod.QrUpdU.SQL.Add('Pos_Larg=:P4, Pos_MEsq=:P5, Pos_MSup=:P6, Orientacao=:P7, ');
  Dmod.QrUpdU.SQL.Add('DataCad=:Px, UserCad=:Py, Codigo=:Pz');
  Dmod.QrUpdU.Params[00].AsString  := 'C�pia de '+QrImprimeNome.Value;
  Dmod.QrUpdU.Params[01].AsString  := QrImprimeTitulo.Value;
  Dmod.QrUpdU.Params[02].AsInteger := QrImprimePos_Topo.Value;
  Dmod.QrUpdU.Params[03].AsInteger := QrImprimePos_Altu.Value;
  Dmod.QrUpdU.Params[04].AsInteger := QrImprimePos_Larg.Value;
  Dmod.QrUpdU.Params[05].AsInteger := QrImprimePos_MEsq.Value;
  Dmod.QrUpdU.Params[06].AsInteger := QrImprimePos_MSup.Value;
  Dmod.QrUpdU.Params[07].AsInteger := QrImprimeOrientacao.Value;
  Dmod.QrUpdU.Params[08].AsString  := FormatDateTime(VAR_FORMATDATE, Date);
  Dmod.QrUpdU.Params[09].AsInteger := VAR_USUARIO;
  Dmod.QrUpdU.Params[10].AsInteger := Codigo;
  Dmod.QrUpdU.ExecSQL;
  //
  QrImprimeBand.First;
  while not QrImprimeBand.Eof do
  begin
    BandControle := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
    'ImprimeBand', 'ImprimeBand', 'Codigo');
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('INSERT INTO imprimeband SET ');
    Dmod.QrUpd.SQL.Add('Tipo=:P0, Dataset=:P1, Pos_Topo=:P2, Pos_Altu=:P3, ');
    Dmod.QrUpd.SQL.Add('Nome=:P4, Col_Qtd=:P5, Col_Lar=:P6, Col_GAP=:P7, ');
    Dmod.QrUpd.SQL.Add('DataCad=:P8, UserCad=:P9, Codigo=:P10, Controle=:P11');
    Dmod.QrUpd.Params[00].AsInteger := QrImprimeBandTipo.Value;
    Dmod.QrUpd.Params[01].AsInteger := QrImprimeBandDataset.Value;
    Dmod.QrUpd.Params[02].AsInteger := QrImprimeBandPos_Topo.Value;
    Dmod.QrUpd.Params[03].AsInteger := QrImprimeBandPos_Altu.Value;
    Dmod.QrUpd.Params[04].AsString  := QrImprimeBandNome.Value;
    Dmod.QrUpd.Params[05].AsInteger := QrImprimeBandCol_Qtd.Value;
    Dmod.QrUpd.Params[06].AsInteger := QrImprimeBandCol_Lar.Value;
    Dmod.QrUpd.Params[07].AsInteger := QrImprimeBandCol_GAP.Value;
    Dmod.QrUpd.Params[08].AsString  := FormatDateTime(VAR_FORMATDATE, Date);
    Dmod.QrUpd.Params[09].AsInteger := VAR_USUARIO;
    Dmod.QrUpd.Params[10].AsInteger := Codigo;
    Dmod.QrUpd.Params[11].AsInteger := BandControle;
    Dmod.QrUpd.ExecSQL;
    //
    QrImprimeView.First;
    while not QrImprimeView.Eof do
    begin
      ViewControle := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
      'ImprimeView', 'ImprimeView', 'Codigo');
      Dmod.QrUpdU.SQL.Clear;
      Dmod.QrUpdU.SQL.Add('INSERT INTO imprimeview SET ');
      Dmod.QrUpdU.SQL.Add('Nome=:P0, Pos_Topo=:P1, Pos_Altu=:P2, Pos_MEsq=:P3,');
      Dmod.QrUpdU.SQL.Add('Pos_Larg=:P4, Set_TAli=:P5, Set_BAli=:P6, ');
      Dmod.QrUpdU.SQL.Add('Set_Negr=:P7, Set_Ital=:P8, Set_Unde=:P9, ');
      Dmod.QrUpdU.SQL.Add('Set_Tam=:P10, Tab_Tipo=:P11, Tab_Campo=:P12, ');
      Dmod.QrUpdU.SQL.Add('Padrao=:P13, Formato=:P14, Band=:P15, ');
      Dmod.QrUpdU.SQL.Add('DataCad=:Px, UserCad=:Py, Codigo=:Pz, Controle=:Pw');
      Dmod.QrUpdU.Params[00].AsString  := QrImprimeViewNome.Value;
      Dmod.QrUpdU.Params[01].AsInteger := QrImprimeViewPos_Topo.Value;
      Dmod.QrUpdU.Params[02].AsInteger := QrImprimeViewPos_Altu.Value;
      Dmod.QrUpdU.Params[03].AsInteger := QrImprimeViewPos_MEsq.Value;
      Dmod.QrUpdU.Params[04].AsInteger := QrImprimeViewPos_Larg.Value;
      Dmod.QrUpdU.Params[05].AsInteger := QrImprimeViewSet_TAli.Value;
      Dmod.QrUpdU.Params[06].AsInteger := QrImprimeViewSet_BAli.Value;
      Dmod.QrUpdU.Params[07].AsInteger := QrImprimeViewSet_Negr.Value;
      Dmod.QrUpdU.Params[08].AsInteger := QrImprimeViewSet_Ital.Value;
      Dmod.QrUpdU.Params[09].AsInteger := QrImprimeViewSet_Unde.Value;
      Dmod.QrUpdU.Params[10].AsInteger := QrImprimeViewSet_Tam.Value;
      Dmod.QrUpdU.Params[11].AsInteger := QrImprimeViewTab_Tipo.Value;
      Dmod.QrUpdU.Params[12].AsInteger := QrImprimeViewTab_Campo.Value;
      Dmod.QrUpdU.Params[13].AsString  := QrImprimeViewPadrao.Value;
      Dmod.QrUpdU.Params[14].AsInteger := QrImprimeViewFormato.Value;
      Dmod.QrUpdU.Params[15].AsInteger := BandControle;
      Dmod.QrUpdU.Params[16].AsString  := FormatDateTime(VAR_FORMATDATE, Date);
      Dmod.QrUpdU.Params[17].AsInteger := VAR_USUARIO;
      Dmod.QrUpdU.Params[18].AsInteger := Codigo;
      Dmod.QrUpdU.Params[19].AsInteger := ViewControle;
      Dmod.QrUpdU.ExecSQL;
      //
      QrImprimeView.Next;
    end;
    QrImprimeBand.Next;
  end;
  //

  MostraEdicao(False, stLok, 0);
  LocCod(Codigo,Codigo);
end;

procedure TFmImprime.Duplicaformatoatual1Click(Sender: TObject);
begin
  DuplicaRegistro;
end;

procedure TFmImprime.Somenteselecionado1Click(Sender: TObject);
begin
  AlteraCampoSelecionado(QrImprimeView, False);
end;

procedure TFmImprime.Umaum1Click(Sender: TObject);
begin
  QrImprimeView2.Close;
  QrImprimeView2.Params[0].AsInteger := QrImprimeCodigo.Value;
  QrImprimeView2.Params[1].AsInteger := QrImprimeBandControle.Value;
  UnDmkDAC_PF.AbreQuery(QrImprimeView2, Dmod.MyDB);
  //
  if QrImprimeView2.Locate('Controle', QrImprimeViewControle.Value, []) then
  begin
    while not QrImprimeView2.Eof do
    begin
      AlteraCampoSelecionado(QrImprimeView2, True);
      if FDesiste then Exit;
      QrImprimeView2.Next;
    end;
  end;
end;

procedure TFmImprime.AlteraCampoSelecionado(Query: TmySQLQuery; UmAUm: Boolean);
begin
  MyObjects.CriaForm_AcessoTotal(TFmImprimeView, FmImprimeView);
  FmImprimeView.ImgTipo.SQLType      := stUpd;
  FBand := QrImprimeBandControle.Value;
  FmImprimeView.FUmAUm := True;
  FmImprimeView.FPageMEsq := QrImprimePos_MEsq.Value;
  ConfiguraDadosView(Query);
  FmImprimeView.FControle := Query.FieldByName('Controle').AsInteger;
  FmImprimeView.ShowModal;
  FmImprimeView.Destroy;
  SubQueriesReopen;
end;

procedure TFmImprime.ConfiguraDadosView(Query: TmySQLQuery);
begin
  FmImprimeView.EdNome.Text         := Query.FieldByName('Nome').AsString;
  FmImprimeView.EdTopoBanda.Text    := IntToStr(Query.FieldByName('TOPOBANDA').AsInteger);
  FmImprimeView.EdTopoReal.Text     := IntToStr(Query.FieldByName('TOPOREAL').AsInteger);
  FmImprimeView.EdAltu.Text         := IntToStr(Query.FieldByName('Pos_Altu').AsInteger);
  FmImprimeView.EdMEsq.Text         := IntToStr(Query.FieldByName('Pos_MEsq').AsInteger);
  FmImprimeView.EdLarg.Text         := IntToStr(Query.FieldByName('Pos_Larg').AsInteger);
  if QrImprimeBandDataset.Value > 0 then
  begin
    FmImprimeView.EdAlt_Linha.Text    := '0';
    FmImprimeView.EdAlt_Linha.Enabled := False;
  end else
    FmImprimeView.EdAlt_Linha.Text  := IntToStr(Query.FieldByName('Alt_Linha').AsInteger);
  FmImprimeView.RGAlign1.ItemIndex  := Query.FieldByName('Set_TAli').AsInteger;
  FmImprimeView.RGAlign2.ItemIndex  := Query.FieldByName('Set_BAli').AsInteger;
  FmImprimeView.RGTabela.ItemIndex  := Query.FieldByName('Tab_Tipo').AsInteger;
  FmImprimeView.RGFormato.ItemIndex := Query.FieldByName('Formato').AsInteger;
  FmImprimeView.RGSet_T_DOS.ItemIndex := Query.FieldByName('Set_T_DOS').AsInteger;
  case Query.FieldByName('Tab_Tipo').AsInteger of
    0: FmImprimeView.RGNF_A.ItemIndex           := Query.FieldByName('Tab_Campo').AsInteger;
    1: FmImprimeView.RGEntidades.ItemIndex      := Query.FieldByName('Tab_Campo').AsInteger;
    2: FmImprimeView.RGProdutos.ItemIndex       := Query.FieldByName('Tab_Campo').AsInteger;
    3: FmImprimeView.RGServicos.ItemIndex       := Query.FieldByName('Tab_Campo').AsInteger;
    4: FmImprimeView.RGFaturamento.ItemIndex    := Query.FieldByName('Tab_Campo').AsInteger;
    5: FmImprimeView.RGTransportadora.ItemIndex := Query.FieldByName('Tab_Campo').AsInteger;
    6: FmImprimeView.RGDuplicata.ItemIndex      := Query.FieldByName('Tab_Campo').AsInteger;
    7: FmImprimeView.RGNF_B.ItemIndex           := Query.FieldByName('Tab_Campo').AsInteger;
    8: FmImprimeView.RGNCMs.ItemIndex           := Query.FieldByName('Tab_Campo').AsInteger;
    9: FmImprimeView.RGNF_C.ItemIndex           := Query.FieldByName('Tab_Campo').AsInteger;
  end;
  if Query.FieldByName('Set_Negr').AsInteger  = 1 then FmImprimeView.CkNegr.Checked := True;
  if Query.FieldByName('Set_Unde').AsInteger  = 1 then FmImprimeView.CkSubl.Checked := True;
  if Query.FieldByName('Set_Ital').AsInteger  = 1 then FmImprimeView.CkItal.Checked := True;
  if Query.FieldByName('Substitui').AsInteger = 1 then FmImprimeView.CkSubstitui.Checked := True;
  if Query.FieldByName('Nulo').AsInteger      = 1 then FmImprimeView.CkNulo.Checked := True;
  FmImprimeView.EdFont.Text         := IntToStr(Query.FieldByName('Set_Tam').AsInteger);
  FmImprimeView.FCodigo             := Query.FieldByName('Codigo').AsInteger;
  FmImprimeView.FBand               := QrImprimeBandControle.Value;
  FmImprimeView.EdPadrao.Text       := Query.FieldByName('Padrao').AsString;
  FmImprimeView.EdPrefixo.Text      := Query.FieldByName('Prefixo').AsString;
  FmImprimeView.EdSufixo.Text       := Query.FieldByName('Sufixo').AsString;
  FmImprimeView.EdSubstituicao.Text := Query.FieldByName('Substituicao').AsString;
  //
  FmImprimeView.PCTabelas.ActivePageIndex := Query.FieldByName('Tab_Tipo').AsInteger;
end;

procedure TFmImprime.QrImprimeView2CalcFields(DataSet: TDataSet);
begin
  QrImprimeView2TOPOREAL.Value :=
  QrImprimeView2Pos_Topo.Value + QrImprimeView2TOPOBANDA.Value;
end;

procedure TFmImprime.QrImprimeBandBeforeClose(DataSet: TDataSet);
begin
  QrImprimeView.Close;
end;

procedure TFmImprime.QrImprimeBeforeClose(DataSet: TDataSet);
begin
  STImport.Caption := '';
  QrImprimeBand.Close;
  QrImprimeEmpr.Close;
end;

procedure TFmImprime.IncluiCampo2Click(Sender: TObject);
begin
  MyObjects.CriaForm_AcessoTotal(TFmImprimeView, FmImprimeView);
  FmImprimeView.ImgTipo.SQLType := stIns;
  FmImprimeView.FBand          := QrImprimeBandControle.Value;
  FmImprimeView.FCodigo        := QrImprimeCodigo.Value;
  FmImprimeView.FControle      := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres',
                                  'Controle', 'ImprimeView', 'ImprimeView',
                                  'Codigo');
  FmImprimeView.EdTopoBanda.Text := IntToStr(QrImprimeViewTOPOBANDA.Value);
  FmImprimeView.FPageMEsq := QrImprimePos_MEsq.Value;
  ConfiguraDadosView(QrImprimeView);
  FmImprimeView.ShowModal;
  FmImprimeView.Destroy;
  SubQueriesReopen;
end;

procedure TFmImprime.Exporta1Click(Sender: TObject);
var
  Arq: TextFile;
  Name, Diretorio: String;
  TipoCampo, SimbCampo, NomeCampo, ValrCampo: String;
  TamField: Integer;
begin
  try
    Diretorio := 'C:\Dermatek';
    if not DirectoryExists(Diretorio) then ForceDirectories(Diretorio);
    Name := Diretorio + '\Cadastro_Imprime_Num_'+IntToStr(QrImprimeCodigo.Value)+'.txt';
    if FileExists(Name) then DeleteFile(Name);
    AssignFile(Arq, Name);
    ReWrite(Arq);
    //////////////////////////////////////////////////////////////////////////////
    WriteLn(Arq, '[Imprime]');
    //
    QrCampos.Close;
    QrCampos.SQL.Clear;
    QrCampos.SQL.Add('SHOW FIELDS FROM imprime');
    UnDmkDAC_PF.AbreQuery(QrCampos, Dmod.MyDB);
    //
    QrDados.Close;
    QrDados.SQL.Clear;
    QrDados.SQL.Add('SELECT * FROM imprime WHERE Codigo='+IntToStr(QrImprimeCodigo.Value));
    UnDmkDAC_PF.AbreQuery(QrDados, Dmod.MyDB);
    //
    while not QrCampos.Eof do
    begin
      NomeCampo := QrCampos.FieldByName('Field').AsString;
      TamField := pos('(', QrCampos.FieldByName('Type').AsString)-1;
      if TamField <= 0 then
        TamField := Length(QrCampos.FieldByName('Type').AsString);
      if TamField > 0 then
      begin
        TipoCampo := Uppercase(Copy(QrCampos.FieldByName('Type').AsString, 1, TamField));
        if TipoCampo = 'INT'        then SimbCampo := 'I' else
        if TipoCampo = 'TINYINT'    then SimbCampo := 'I' else
        if TipoCampo = 'MEDIUMINT'  then SimbCampo := 'I' else
        if TipoCampo = 'LONGINT'    then SimbCampo := 'I' else
        if TipoCampo = 'DOUBLE'     then SimbCampo := 'N' else
        if TipoCampo = 'FLOAT'      then SimbCampo := 'N' else
        if TipoCampo = 'CHAR'       then SimbCampo := 'A' else
        if TipoCampo = 'VARCHAR'    then SimbCampo := 'A' else
        if TipoCampo = 'DATE'       then SimbCampo := 'D' else
        if TipoCampo = 'TIME'       then SimbCampo := 'H' else
        if TipoCampo = 'DATETIME'   then SimbCampo := 'T' else
        if TipoCampo = 'BLOB'       then SimbCampo := 'B' else
        if TipoCampo = 'MEDIUMBLOB' then SimbCampo := 'B' else
        if TipoCampo = 'LONGBLOB'   then SimbCampo := 'B' else
        if TipoCampo = 'TYNYBLOB'   then SimbCampo := 'B' else
        if TipoCampo = 'TEXT'       then SimbCampo := 'X' else
        if TipoCampo = 'MEDIUMTEXT' then SimbCampo := 'X' else
        if TipoCampo = 'LONGTEXT'   then SimbCampo := 'X' else
        if TipoCampo = 'TINYTEXT'   then SimbCampo := 'X' else
                                         SimbCampo := '?';
        if TipoCampo = 'DATE'       then
          ValrCampo := FormatDateTime(VAR_FORMATDATE, QrDados.FieldByName(
          NomeCampo).AsDateTime) else
            ValrCampo := QrDados.FieldByName(NomeCampo).AsString;

        WriteLn(Arq, SimbCampo+'_'+NomeCampo+'='+ValrCampo);
      end;
      QrCampos.Next;
    end;
    //
    //////////////////////////////////////////////////////////////////////////////
    QrCampos.Close;
    QrCampos.SQL.Clear;
    QrCampos.SQL.Add('SHOW FIELDS FROM imprimeband');
    UnDmkDAC_PF.AbreQuery(QrCampos, Dmod.MyDB);
    //
    QrDados.Close;
    QrDados.SQL.Clear;
    QrDados.SQL.Add('SELECT * FROM imprimeband WHERE Codigo='+IntToStr(QrImprimeCodigo.Value));
    UnDmkDAC_PF.AbreQuery(QrDados, Dmod.MyDB);
    //
    WriteLn(Arq, '[Band'+IntToStr(QrDados.FieldByName('Controle').AsInteger)+']');
    while not QrDados.Eof do
    begin
      QrCampos.First;
      while not QrCampos.Eof do
      begin
        NomeCampo := QrCampos.FieldByName('Field').AsString;
        TamField := pos('(', QrCampos.FieldByName('Type').AsString)-1;
        if TamField <= 0 then
          TamField := Length(QrCampos.FieldByName('Type').AsString);
        if TamField > 0 then
        begin
          TipoCampo := Uppercase(Copy(QrCampos.FieldByName('Type').AsString, 1, TamField));
          if TipoCampo = 'INT'        then SimbCampo := 'I' else
          if TipoCampo = 'TINYINT'    then SimbCampo := 'I' else
          if TipoCampo = 'MEDIUMINT'  then SimbCampo := 'I' else
          if TipoCampo = 'LONGINT'    then SimbCampo := 'I' else
          if TipoCampo = 'DOUBLE'     then SimbCampo := 'N' else
          if TipoCampo = 'FLOAT'      then SimbCampo := 'N' else
          if TipoCampo = 'CHAR'       then SimbCampo := 'A' else
          if TipoCampo = 'VARCHAR'    then SimbCampo := 'A' else
          if TipoCampo = 'DATE'       then SimbCampo := 'D' else
          if TipoCampo = 'TIME'       then SimbCampo := 'H' else
          if TipoCampo = 'DATETIME'   then SimbCampo := 'T' else
          if TipoCampo = 'BLOB'       then SimbCampo := 'B' else
          if TipoCampo = 'MEDIUMBLOB' then SimbCampo := 'B' else
          if TipoCampo = 'LONGBLOB'   then SimbCampo := 'B' else
          if TipoCampo = 'TYNYBLOB'   then SimbCampo := 'B' else
          if TipoCampo = 'TEXT'       then SimbCampo := 'X' else
          if TipoCampo = 'MEDIUMTEXT' then SimbCampo := 'X' else
          if TipoCampo = 'LONGTEXT'   then SimbCampo := 'X' else
          if TipoCampo = 'TINYTEXT'   then SimbCampo := 'X' else
                                           SimbCampo := '?';
          if TipoCampo = 'DATE'       then
            ValrCampo := FormatDateTime(VAR_FORMATDATE, QrDados.FieldByName(
            NomeCampo).AsDateTime) else
              ValrCampo := QrDados.FieldByName(NomeCampo).AsString;

          WriteLn(Arq, SimbCampo+'_'+NomeCampo+'='+ValrCampo);
        end;
        QrCampos.Next;
      end;
      QrDados.Next;
    end;
    //////////////////////////////////////////////////////////////////////////////
    QrCamposIts.Close;
    QrCamposIts.SQL.Clear;
    QrCamposIts.SQL.Add('SHOW FIELDS FROM imprimeview');
    UnDmkDAC_PF.AbreQuery(QrCamposIts, Dmod.MyDB);
    //
    QrDados.First;
    while not QrDados.Eof do
    begin
      QrDadosIts.Close;
      QrDadosIts.SQL.Clear;
      QrDadosIts.SQL.Add('SELECT * FROM imprimeview WHERE Band='+
      QrDados.FieldByName('Controle').AsString);
      UnDmkDAC_PF.AbreQuery(QrDadosIts, Dmod.MyDB);
      //
      while not QrDadosIts.Eof do
      begin
        WriteLn(Arq, '[View'+IntToStr(QrDadosIts.FieldByName('Band').AsInteger)+
        '.'+IntToStr(QrDadosIts.FieldByName('Controle').AsInteger)+']');
        QrCamposIts.First;
        while not QrCamposIts.Eof do
        begin
          NomeCampo := QrCamposIts.FieldByName('Field').AsString;
          TamField := pos('(', QrCamposIts.FieldByName('Type').AsString)-1;
          if TamField <= 0 then
            TamField := Length(QrCamposIts.FieldByName('Type').AsString);
          if TamField > 0 then
          begin
            TipoCampo := Uppercase(Copy(QrCamposIts.FieldByName('Type').AsString, 1, TamField));
            if TipoCampo = 'INT'        then SimbCampo := 'I' else
            if TipoCampo = 'TINYINT'    then SimbCampo := 'I' else
            if TipoCampo = 'MEDIUMINT'  then SimbCampo := 'I' else
            if TipoCampo = 'LONGINT'    then SimbCampo := 'I' else
            if TipoCampo = 'DOUBLE'     then SimbCampo := 'N' else
            if TipoCampo = 'FLOAT'      then SimbCampo := 'N' else
            if TipoCampo = 'CHAR'       then SimbCampo := 'A' else
            if TipoCampo = 'VARCHAR'    then SimbCampo := 'A' else
            if TipoCampo = 'DATE'       then SimbCampo := 'D' else
            if TipoCampo = 'TIME'       then SimbCampo := 'H' else
            if TipoCampo = 'DATETIME'   then SimbCampo := 'T' else
            if TipoCampo = 'BLOB'       then SimbCampo := 'B' else
            if TipoCampo = 'MEDIUMBLOB' then SimbCampo := 'B' else
            if TipoCampo = 'LONGBLOB'   then SimbCampo := 'B' else
            if TipoCampo = 'TYNYBLOB'   then SimbCampo := 'B' else
            if TipoCampo = 'TEXT'       then SimbCampo := 'X' else
            if TipoCampo = 'MEDIUMTEXT' then SimbCampo := 'X' else
            if TipoCampo = 'LONGTEXT'   then SimbCampo := 'X' else
            if TipoCampo = 'TINYTEXT'   then SimbCampo := 'X' else
                                             SimbCampo := '?';
            if TipoCampo = 'DATE'       then
              ValrCampo := FormatDateTime(VAR_FORMATDATE, QrDadosIts.FieldByName(
              NomeCampo).AsDateTime) else
                ValrCampo := QrDadosIts.FieldByName(NomeCampo).AsString;

            WriteLn(Arq, SimbCampo+'_'+NomeCampo+'='+ValrCampo);
          end;
          QrCamposIts.Next;
        end;
        QrDadosIts.Next;
      end;
      QrDados.Next;
    end;
    WriteLn(Arq, '[FIM]');
    CloseFile(Arq);
    Geral.MB_Aviso('Dados exportados com sucesso para o arquivo'+
    ' "'+Name+'"');
  except
    ;
  end;
end;

procedure TFmImprime.Importa1Click(Sender: TObject);
var
  i, k, j, Conta, Codigo, Banda, Controle: Integer;
  Field, Campo, Valor, NomeView: String;
begin
  if OpenDialog1.Execute then
  begin
    FExecuted := True;
    AppIni := TIniFile.Create(OpenDialog1.FileName);
    AppIni.ReadSections(ListBox1.Items);
    AppIni.ReadSection('Imprime',Listbox3.Items);
    if Geral.MB_Pergunta('Confirma a importac�o do formato de '+
    'impress�o "'+AppIni.ReadString('Imprime', 'A_Nome', '')+'"?')=ID_YES then
    begin
      Screen.Cursor := crHourGlass;
      try
        Codigo := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
        'Imprime', 'Imprime', 'Codigo');
        STImport.Visible := True;
        STImport.Caption := 'Incluindo Formato de Impress�o n� '+IntToStr(Codigo);
        STImport.refresh;
        Update;
        Conta := 0;
        for i := 0 to ListBox1.Items.Count-1 do
          if Uppercase(ListBox1.Items[i]) = 'IMPRIME' then Conta := Conta+1;
        if Conta <> 1 then
        begin
          Geral.MB_Aviso('O arquivo deve ter um registro "[Imprime]"!'+
          ' Mas foi encontrado '+IntToStr(Conta));
          Exit;
        end;
        //////
        AppIni.ReadSection('Imprime',Listbox3.Items);
        Dmod.QrUpd.SQL.Clear;
        Dmod.QrUpd.SQL.Add('INSERT INTO imprime SET ');
        Dmod.QrUpd.SQL.Add('Codigo='+IntToStr(codigo));
        for i := 0 to ListBox3.Items.Count-1 do
        begin
          Field := ListBox3.Items[i];
          Campo := Copy(Field, 3, Length(Field)-2);
          Field := Field[1];
          if Uppercase(Campo) <> 'CODIGO' then
          begin
            Valor := AppIni.ReadString('Imprime', ListBox3.Items[i], '');
            Dmod.QrUpd.SQL.Add(', '+DefineInsertCampo(Field, Campo, Valor));
          end;
        end;
        Dmod.QrUpd.SQL.Add('');
        Dmod.QrUpd.ExecSQL;
        //
    ////////////////////////////////////////////////////////////////////////////////
        //
        for k := 0 to ListBox1.Items.Count-1 do
        begin
          if Length(ListBox1.Items[k]) > 3 then
          if Uppercase(Copy(ListBox1.Items[k], 1, 4)) = 'BAND' then
          begin
            Banda := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
              'ImprimeBand', 'ImprimeBand', 'Codigo');
            STImport.Caption := 'Incluindo Banda n� '+IntToStr(Banda);
            STImport.refresh;
            Update;
            AppIni.ReadSection(ListBox1.Items[k],Listbox3.Items);
            Dmod.QrUpd.SQL.Clear;
            Dmod.QrUpd.SQL.Add('INSERT INTO imprimeband SET ');
            Dmod.QrUpd.SQL.Add('  Codigo='+IntToStr(Codigo));
            Dmod.QrUpd.SQL.Add(', Controle='+IntToStr(Banda));
            for i := 0 to ListBox3.Items.Count-1 do
            begin
              Field := ListBox3.Items[i];
              Campo := Copy(Field, 3, Length(Field)-2);
              Field := Field[1];
              if Uppercase(Campo) <> 'CODIGO' then
              if Uppercase(Campo) <> 'CONTROLE' then
              begin
                Valor := AppIni.ReadString(ListBox1.Items[k], ListBox3.Items[i], '');
                Dmod.QrUpd.SQL.Add(', '+DefineInsertCampo(Field, Campo, Valor));
              end;
            end;
            Dmod.QrUpd.SQL.Add('');
            Dmod.QrUpd.ExecSQL;
            NomeView := 'VIEW'+Copy(ListBox1.Items[k], 5, Length(ListBox1.Items[k])-4);
            for j := 0 to ListBox1.Items.Count-1 do
            begin
              if Length(ListBox1.Items[j]) > 3 then
              if Uppercase(Copy(ListBox1.Items[j], 1, Length(NomeView))) = NomeView then
              begin
                Controle := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
                  'ImprimeView', 'ImprimeView', 'Codigo');
                STImport.Caption := 'Incluindo Campo n� '+IntToStr(Controle);
                STImport.refresh;
                Update;
                AppIni.ReadSection(ListBox1.Items[j],Listbox3.Items);
                Dmod.QrUpd.SQL.Clear;
                Dmod.QrUpd.SQL.Add('INSERT INTO imprimeview SET ');
                Dmod.QrUpd.SQL.Add('  Codigo='+IntToStr(Codigo));
                Dmod.QrUpd.SQL.Add(', Band='+IntToStr(Banda));
                Dmod.QrUpd.SQL.Add(', Controle='+IntToStr(Controle));
                for i := 0 to ListBox3.Items.Count-1 do
                begin
                  Field := ListBox3.Items[i];
                  Campo := Copy(Field, 3, Length(Field)-2);
                  Field := Field[1];
                  if Uppercase(Campo) <> 'CODIGO' then
                  if Uppercase(Campo) <> 'BAND' then
                  if Uppercase(Campo) <> 'CONTROLE' then
                  begin
                    Valor := AppIni.ReadString(ListBox1.Items[j], ListBox3.Items[i], '');
                    Dmod.QrUpd.SQL.Add(', '+DefineInsertCampo(Field, Campo, Valor));
                  end;
                end;
                Dmod.QrUpd.SQL.Add('');
                Dmod.QrUpd.ExecSQL;
              end;
            end;
          end;
        end;
        Screen.Cursor := crDefault;
        LocCod(Codigo, Codigo);
      except
        Screen.Cursor := crDefault;
        raise;
      end;
    end;
    AppIni.Free;
    STImport.Visible := False;
  end;
end;

function TFmImprime.DefineInsertCampo(Field, Campo, Valor: String): String;
var
  Fmt: Integer;
begin
  if Field = 'I' then Fmt := 0 else
  if Field = 'N' then Fmt := 0 else
  if Field = 'A' then Fmt := 1 else
  if Field = 'D' then Fmt := 1 else
  if Field = 'H' then Fmt := 1 else
  if Field = 'T' then Fmt := 1 else
  if Field = 'B' then Fmt := 0 else
  if Field = 'X' then Fmt := 1 else
  if Field = '?' then Fmt := -1
                     else Fmt := -1;
  case Fmt of
     0: Result := Valor;
     1: Result := '"'+Valor+'"';
    else begin
      Geral.MB_Aviso('Tipo de campo indefinido!');
      Result := Valor;
    end;
  end;
  Result := Campo+'='+Result;
end;

procedure TFmImprime.BitBtn1Click(Sender: TObject);
begin
(*
  case QrImprimeTipoImpressao.Value of
    2:
    begin
      UCriar.GerenciaTabelaLocal('NF1Fat', acCreate);
      UCriar.GerenciaTabelaLocal('NF1Pro', acCreate);
      UCriar.GerenciaTabelaLocal('Imprimir1', acCreate);
      MyObjects.CriaForm_AcessoTotal(TFmNF1, FmNF1);
      FmNF1.TbNF1Pro.Close;
      UnDmkDAC_PF.AbreQuery(FmNF1.TbNF1Pro, Dmod.MyDB);

      FmNF1.RecalculaTotalNF;

      FmNF1.EdImprime.Text               := IntToStr(QrImprimeCodigo.Value);
      FmNF1.CBImprime.KeyValue           := QrImprimeCodigo.Value;
      FmNF1.CkTeste.Checked              := True;
      FmNF1.PageControl1.ActivePageIndex := 3;
      FmNF1.ShowModal;
      FmNF1.Destroy;
    end;
    else
*)
    Geral.MB_Info('Modelo sem teste implementado!');
(*
  end;
*)
end;

end.

