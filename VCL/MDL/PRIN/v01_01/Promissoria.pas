unit Promissoria;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Db, mySQLDbTables, ComCtrls, Grids, DBGrids,
  Mask, DBCtrls, frxClass, dmkGeral, dmkEdit, dmkEditDateTimePicker,
  dmkDBLookupComboBox, dmkEditCB, Menus, UnDmkProcFunc, dmkImage, UnDmkWeb,
  UnDmkEnums, DMkDAC_PF;

type
  TFmPromissoria = class(TForm)
    Panel1: TPanel;
    QrCliente: TmySQLQuery;
    QrClienteCNPJ_TXT: TWideStringField;
    QrClienteTE1_TXT: TWideStringField;
    QrClienteCEP_TXT: TWideStringField;
    Panel2: TPanel;
    Panel3: TPanel;
    Panel4: TPanel;
    DBGrid1: TDBGrid;
    DsPromissoria: TDataSource;
    QrPromissoria: TmySQLQuery;
    GroupBox1: TGroupBox;
    TPEmissao: TdmkEditDateTimePicker;
    Label1: TLabel;
    GroupBox2: TGroupBox;
    Label2: TLabel;
    EdDupVal: TdmkEdit;
    EdDupNum: TdmkEdit;
    Label10: TLabel;
    Label3: TLabel;
    TPVencto: TdmkEditDateTimePicker;
    GroupBox3: TGroupBox;
    Label4: TLabel;
    Label5: TLabel;
    EdFatVal: TdmkEdit;
    EdFatNum: TdmkEdit;
    GroupBox4: TGroupBox;
    Label6: TLabel;
    EdDesc: TdmkEdit;
    Label7: TLabel;
    RGMoeda: TRadioGroup;
    Memo1: TMemo;
    EdDDAntes: TdmkEdit;
    Label8: TLabel;
    QrClienteTipo: TSmallintField;
    QrClienteRespons1: TWideStringField;
    QrClienteRespons2: TWideStringField;
    QrClienteRUA: TWideStringField;
    QrClienteCOMPL: TWideStringField;
    QrClienteBAIRRO: TWideStringField;
    QrClienteTE1: TWideStringField;
    QrClienteFAX: TWideStringField;
    QrClienteDOC1: TWideStringField;
    QrClienteDOC2: TWideStringField;
    QrClienteCIDADE: TWideStringField;
    QrClienteNOMEUF: TWideStringField;
    QrClienteNOMEENTIDADE: TWideStringField;
    QrClienteNUM_TXT: TWideStringField;
    QrClienteDOC_TXT: TWideStringField;
    GroupBox5: TGroupBox;
    DsCliente: TDataSource;
    QrClienteCodigo: TIntegerField;
    frxPromissoria: TfrxReport;
    QrPromissoriaData: TDateField;
    QrPromissoriaTipo: TSmallintField;
    QrPromissoriaCarteira: TIntegerField;
    QrPromissoriaControle: TIntegerField;
    QrPromissoriaSub: TSmallintField;
    QrPromissoriaAutorizacao: TIntegerField;
    QrPromissoriaGenero: TIntegerField;
    QrPromissoriaQtde: TFloatField;
    QrPromissoriaDescricao: TWideStringField;
    QrPromissoriaNotaFiscal: TIntegerField;
    QrPromissoriaDebito: TFloatField;
    QrPromissoriaCredito: TFloatField;
    QrPromissoriaCompensado: TDateField;
    QrPromissoriaSerieCH: TWideStringField;
    QrPromissoriaDocumento: TFloatField;
    QrPromissoriaSit: TIntegerField;
    QrPromissoriaVencimento: TDateField;
    QrPromissoriaFatID: TIntegerField;
    QrPromissoriaFatID_Sub: TIntegerField;
    QrPromissoriaFatNum: TFloatField;
    QrPromissoriaFatParcela: TIntegerField;
    QrPromissoriaID_Pgto: TIntegerField;
    QrPromissoriaID_Quit: TIntegerField;
    QrPromissoriaID_Sub: TSmallintField;
    QrPromissoriaFatura: TWideStringField;
    QrPromissoriaEmitente: TWideStringField;
    QrPromissoriaBanco: TIntegerField;
    QrPromissoriaContaCorrente: TWideStringField;
    QrPromissoriaCNPJCPF: TWideStringField;
    QrPromissoriaLocal: TIntegerField;
    QrPromissoriaCartao: TIntegerField;
    QrPromissoriaLinha: TIntegerField;
    QrPromissoriaOperCount: TIntegerField;
    QrPromissoriaLancto: TIntegerField;
    QrPromissoriaPago: TFloatField;
    QrPromissoriaMez: TIntegerField;
    QrPromissoriaFornecedor: TIntegerField;
    QrPromissoriaCliente: TIntegerField;
    QrPromissoriaCliInt: TIntegerField;
    QrPromissoriaForneceI: TIntegerField;
    QrPromissoriaMoraDia: TFloatField;
    QrPromissoriaMulta: TFloatField;
    QrPromissoriaMoraVal: TFloatField;
    QrPromissoriaMultaVal: TFloatField;
    QrPromissoriaProtesto: TDateField;
    QrPromissoriaDataDoc: TDateField;
    QrPromissoriaCtrlIni: TIntegerField;
    QrPromissoriaNivel: TIntegerField;
    QrPromissoriaVendedor: TIntegerField;
    QrPromissoriaAccount: TIntegerField;
    QrPromissoriaICMS_P: TFloatField;
    QrPromissoriaICMS_V: TFloatField;
    QrPromissoriaDuplicata: TWideStringField;
    QrPromissoriaDepto: TIntegerField;
    QrPromissoriaDescoPor: TIntegerField;
    QrPromissoriaDescoVal: TFloatField;
    QrPromissoriaDescoControle: TIntegerField;
    QrPromissoriaUnidade: TIntegerField;
    QrPromissoriaNFVal: TFloatField;
    QrPromissoriaAntigo: TWideStringField;
    QrPromissoriaExcelGru: TIntegerField;
    QrPromissoriaDoc2: TWideStringField;
    QrPromissoriaCNAB_Sit: TSmallintField;
    QrPromissoriaTipoCH: TSmallintField;
    QrPromissoriaReparcel: TIntegerField;
    QrPromissoriaLk: TIntegerField;
    QrPromissoriaDataCad: TDateField;
    QrPromissoriaDataAlt: TDateField;
    QrPromissoriaUserCad: TIntegerField;
    QrPromissoriaUserAlt: TIntegerField;
    QrPromissoriaAlterWeb: TSmallintField;
    QrPromissoriaAtivo: TSmallintField;
    QrPromissoriaAtrelado: TIntegerField;
    QrPromissoriaPagMul: TFloatField;
    QrPromissoriaPagJur: TFloatField;
    QrPromissoriaSubPgto1: TIntegerField;
    QrPromissoriaMultiPgto: TIntegerField;
    QrPromissoriaNOMECARTEIRA: TWideStringField;
    EdCliente: TdmkEditCB;
    CBCliente: TdmkDBLookupComboBox;
    SpeedButton1: TSpeedButton;
    frxPromissoria2: TfrxReport;
    QrClienteE_ALL: TWideStringField;
    QrClienteNO_LOGRAD: TWideStringField;
    QrClienteE_MIN: TWideStringField;
    Panel5: TPanel;
    EdAvalista1: TdmkEditCB;
    CBAvalista1: TdmkDBLookupComboBox;
    SpeedButton2: TSpeedButton;
    Label11: TLabel;
    EdAvalista2: TdmkEditCB;
    CBAvalista2: TdmkDBLookupComboBox;
    Label12: TLabel;
    SpeedButton3: TSpeedButton;
    QrAvalista1: TmySQLQuery;
    DsAvalista1: TDataSource;
    QrAvalista1NO_ENT: TWideStringField;
    QrAvalista1CNPJCPF: TWideStringField;
    QrAvalista1Te1: TWideStringField;
    QrAvalista2: TmySQLQuery;
    DsAvalista2: TDataSource;
    QrAvalista1Codigo: TIntegerField;
    QrAvalista2NO_ENT: TWideStringField;
    QrAvalista2CNPJCPF: TWideStringField;
    QrAvalista2Te1: TWideStringField;
    QrAvalista2Codigo: TIntegerField;
    Label9: TLabel;
    EdPracaPagto: TdmkEdit;
    CkFundo: TCheckBox;
    frxPromissoria1: TfrxReport;
    QrPromissoriaAgencia: TIntegerField;
    QrClienteNUMERO: TFloatField;
    QrClienteCEP: TFloatField;
    QrClienteLograd: TFloatField;
    QrClienteUF: TFloatField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel6: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel7: TPanel;
    PnSaiDesis: TPanel;
    BtImprime: TBitBtn;
    BtSalvar: TBitBtn;
    BtBxaNP_BMP: TBitBtn;
    BtSaida: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure BtImprimeClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure QrPromissoriaAfterScroll(DataSet: TDataSet);
    procedure BtSalvarClick(Sender: TObject);
    procedure QrClienteCalcFields(DataSet: TDataSet);
    procedure frxPromissoriaGetValue(const VarName: String;
      var Value: Variant);
    procedure FormCreate(Sender: TObject);
    procedure BtBxaNP_BMPClick(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure frxPromissoria2GetValue(const VarName: string;
      var Value: Variant);
    procedure FormResize(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    FFatTipo, FFatID, FControle: Integer;
    FFatNum: Double;
  end;

var
  FmPromissoria: TFmPromissoria;
const
  FPath = 'Dermatek\Promissoria';

implementation

uses UnMyObjects, Module, UnInternalConsts, Principal, ModuleGeral,
  UMySQLModule;

{$R *.DFM}

const
  FNotaPromissoriaBMP = 'C:\Dermatek\Imagens\NotaPromissoria.bmp';

procedure TFmPromissoria.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmPromissoria.BtImprimeClick(Sender: TObject);
begin
  if CkFundo.Checked then
    MyObjects.frxMostra(frxPromissoria2, 'Nota Promissória')
  else
    MyObjects.frxMostra(frxPromissoria1, 'Nota Promissória');
end;

procedure TFmPromissoria.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  QrPromissoria.Close;
  QrPromissoria.DataBase := VAR_GOTOMySQLDBNAME;
  QrPromissoria.SQL.Clear;
  QrPromissoria.SQL.Add('SELECT la.*, ca.Nome NOMECARTEIRA');
  QrPromissoria.SQL.Add('FROM ' + VAR_LCT + ' la, carteiras ca');
  QrPromissoria.SQL.Add('WHERE ca.Codigo=la.Carteira');
  if FFatTipo = 0 then
  begin
    QrPromissoria.SQL.Add('AND Controle='+ FormatFloat('0', FControle));
  end else begin
    QrPromissoria.SQL.Add('AND FatNum = '+ FormatFloat('0', FFatNum));
    QrPromissoria.SQL.Add('AND FatID = '+ FormatFloat('0', FFatID));
  end;
  QrPromissoria.SQL.Add('ORDER BY Vencimento, FatParcela');
  UnDmkDAC_PF.AbreQuery(QrPromissoria, Dmod.MyDB);
  /////////////////
  EdDesc.Text := Geral.ReadAppKeyCU('Desconto',  FPath, ktString, '0,00');
  RGMoeda.ItemIndex := StrToInt(Geral.ReadAppKeyCU('Moeda',  FPath, ktString, '0'));
  EdDDAntes.Text := Geral.ReadAppKeyCU('Dias',  FPath, ktString, '0');
  Memo1.Text := Geral.ReadAppKeyCU('Memo',  FPath, ktString, '');
  EdPracaPagto.Text := Geral.ReadAppKeyCU('Praca_Pagto',  FPath, ktString, '');
  CkFundo.Checked := Geral.ReadAppKeyCU('Imp_Fundo',  FPath, ktBoolean, False);
end;

procedure TFmPromissoria.QrPromissoriaAfterScroll(DataSet: TDataSet);
begin
  EdDupVal.ValueVariant := QrPromissoriaCredito.Value;
  EdDupNum.Text := IntToStr(QrPromissoriaFatParcela.Value);
  TPVencto.Date := QrPromissoriaVencimento.Value;
  TPVencto.Date := QrPromissoriaVencimento.Value;
end;

procedure TFmPromissoria.SpeedButton1Click(Sender: TObject);
begin
  QrCliente.Close;
  UnDmkDAC_PF.AbreQuery(QrCliente, Dmod.MyDB);
  VAR_CADASTRO := 0;
  if DModG.CadastroDeEntidade(QrClienteCodigo.Value, fmcadEntidade2, fmcadEntidade2) then
  UMyMod.SetaCodigoPesquisado(EdCliente, CBCliente, QrCliente, VAR_CADASTRO);
end;

procedure TFmPromissoria.BtSalvarClick(Sender: TObject);
var
 Moeda: String;
begin
  Moeda := IntToStr(RGMoeda.ItemIndex);
  Geral.WriteAppKeyCU('Desconto',  FPath, EdDesc.Text,  ktString);
  Geral.WriteAppKeyCU('Moeda',  FPath, Moeda,  ktString);
  Geral.WriteAppKeyCU('Dias',  FPath, EdDDAntes.Text,  ktString);
  Geral.WriteAppKeyCU('Memo',  FPath, Memo1.Text,  ktString);
  Geral.WriteAppKeyCU('Praca_Pagto',  FPath, EdPracaPagto.Text,  ktString);
  Geral.WriteAppKeyCU('Imp_Fundo',  FPath, CkFundo.Checked,  ktBoolean);
end;

procedure TFmPromissoria.QrClienteCalcFields(DataSet: TDataSet);
begin
  QrClienteTE1_TXT.Value := Geral.FormataTelefone_TT(QrClienteTe1.Value);
  QrClienteCEP_TXT.Value :=Geral.FormataCEP_NT(QrClienteCEP.Value);
  QrClienteDOC_TXT.Value := Geral.FormataCNPJ_TT(QrClienteDOC1.Value);
  if QrClienteNUMERO.Value <> 0 then
    QrClienteNUM_TXT.Value := FormatFloat('#,##0', QrClienteNUMERO.Value)
  else QrClienteNUM_TXT.Value := 'S/N';
  //
  QrClienteE_ALL.Value := QrClienteNO_LOGRAD.Value;
  if Trim(QrClienteE_ALL.Value) <> '' then QrClienteE_ALL.Value :=
    QrClienteE_ALL.Value + ' ';
  QrClienteE_ALL.Value := QrClienteE_ALL.Value + QrClienteRua.Value;
  if Trim(QrClienteRua.Value) <> '' then QrClienteE_ALL.Value :=
    QrClienteE_ALL.Value + ', ' + QrClienteNUM_TXT.Value;
  if Trim(QrClienteCompl.Value) <>  '' then QrClienteE_ALL.Value :=
    QrClienteE_ALL.Value + ' ' + QrClienteCompl.Value;
    //
  QrClienteE_MIN.Value := QrClienteE_ALL.Value;
    //
  if Trim(QrClienteBairro.Value) <>  '' then QrClienteE_ALL.Value :=
    QrClienteE_ALL.Value + ', Bairro ' + QrClienteBairro.Value;
  if QrClienteCEP.Value > 0 then QrClienteE_ALL.Value :=
    QrClienteE_ALL.Value + ' - CEP ' +Geral.FormataCEP_NT(QrClienteCEP.Value);
  if Trim(QrClienteCidade.Value) <>  '' then QrClienteE_ALL.Value :=
    QrClienteE_ALL.Value + ' - ' + QrClienteCidade.Value;
  if Trim(QrClienteNOMEUF.Value) <>  '' then QrClienteE_ALL.Value :=
    QrClienteE_ALL.Value + ', ' + QrClienteNOMEUF.Value;
  //
end;

procedure TFmPromissoria.BtBxaNP_BMPClick(Sender: TObject);
var
  Versao: Int64;
  ArqNome: String;
begin
  DmkWeb.VerificaAtualizacaoVersao2(True, True, CO_WEBID_PROMISSORIA,
    'IMAGEM FUNDO NOTA PROMISSÓRIA', Geral.SoNumero_TT(DmodG.QrMasterCNPJ.Value),
    0, 0, DmodG.ObtemAgora(), nil, dtImag, Versao, ArqNome, False);
  //
  BtBxaNP_BMP.Enabled := not FileExists(FNotaPromissoriaBMP);
end;

procedure TFmPromissoria.frxPromissoriaGetValue(const VarName: String;
  var Value: Variant);
var
  FoneFax: String;
  i : Integer;
begin
  if VarName = 'VALOR_EXTENSO' then Value := 'TESTE EXTENSO';
  if VarName = 'EMPRESA' then Value := DModG.QrDonoNOMEDONO.Value;
  if VarName = 'FATVAL' then Value := EdFatVal.Text;
  if VarName = 'FATNUM' then Value := EdFatNum.Text;
  if VarName = 'DUPVAL' then Value := EdDupVal.Text;
  if VarName = 'DUPNUM' then Value := EdDupNum.Text;

  if VarName = 'DESCONTO' then
  begin
    if Geral.DMV(EdDesc.Text) > 0 then
    begin
      if RGMoeda.ItemIndex = 1 then
        Value := EdDesc.Text + '' + RGMoeda.Items[RGMoeda.ItemIndex]
      else Value := RGMoeda.Items[RGMoeda.ItemIndex] + '' + EdDesc.Text;
    end else Value := ' ';
  end;
  if VarName = 'CONDICOES_ESPECIAIS' then Value := Memo1.Text;
  if VarName = 'ENDERECO' then
  begin
    Value := DModG.QrDonoRUA.Value + ', ' + DModG.QrDonoNUMERO_TXT.Value;
    if Trim(DModG.QrDonoCOMPL.Value) <> '' then
       Value := Value + '  ' + DModG.QrDonoCOMPL.Value;
    if Trim(DModG.QrDonoBairro.Value) <> '' then
       Value := Value + ' - ' + DModG.QrDonoBairro.Value;
  end;
  if VarName = 'FONE_FAX' then
  begin
    Value := Geral.FormataTelefone_TT(DModG.QrDonoTE1.Value);
    if Trim(DModG.QrDonoFAX.Value) <> '' then
    begin
       FoneFax := 'Fone/Fax: ';
       if (Trim(DModG.QrDonoFAX.Value) <> Trim(DModG.QrDonoTE1.Value)) then
         Value := Value + ' - ' +
         Geral.FormataTelefone_TT(DModG.QrDonoFAX.Value);
    end else FoneFax := 'Fone: ';
    Value := FoneFax + Value;
  end;
  if VarName = 'CEP_CIDADE_UF' then
    Value := 'CEP ' + DModG.QrDonoECEP_TXT.Value + ' - ' +
    DModG.QrDonoCIDADE.Value + '   ' + DModG.QrDonoNOMEUF.Value;
  if VarName = 'CNPJ' then
  begin
    Value := DModG.QrDonoCNPJ_TXT.Value;
    if DModG.QrDonoTipo.Value = 0 then
       Value := 'CNPJ: '+Value else Value := 'CPF: '+ Value;
  end;
  if VarName = 'IE' then
  begin
    Value := DModG.QrDonoIE_RG.Value;
    if DModG.QrDonoTipo.Value = 0 then
       Value := 'INSCRIÇÃO ESTADUAL: '+Value
    else Value := 'RG: '+ Value;
  end;
  if VarName = 'DATA_EMISSAO' then
     Value := FormatDateTime(VAR_FORMATDATE3, TPEmissao.Date);
  if VarName = 'VENCTO' then
     Value := FormatDateTime(VAR_FORMATDATE3, TPVencto.Date);
  if VarName = 'ATE' then
  begin
    if Geral.DMV(EdDesc.Text) > 0 then
      Value := FormatDateTime(VAR_FORMATDATE3, TPVencto.Date -
      Geral.IMV(EdDDAntes.Text))
    else Value := ' ';
  end;
  //////////////////////////////////////////////////////
  if VarName = 'SAC_NOME' then Value := QrClienteNOMEENTIDADE.Value;
  if VarName = 'SAC_ENDERECO' then
  begin
    Value := QrClienteRUA.Value + ', ' + QrClienteNUM_TXT.Value;
    if Trim(QrClienteCOMPL.Value) <> '' then
       Value := Value + '  ' + QrClienteCOMPL.Value;
    if Trim(QrClienteBairro.Value) <> '' then
       Value := Value + ' - ' + QrClienteBairro.Value;
  end;
  if VarName = 'SAC_CIDADE' then Value := QrClienteCIDADE.Value;
  if VarName = 'SAC_PRACA_PAGTO' then Value := EdPracaPagto.Text;
  if VarName = 'SAC_CNPJ_CPF' then Value := QrClienteDOC_TXT.Value;
  if VarName = 'SAC_CEP_TXT' then Value := QrClienteCEP_TXT.Value;
  if VarName = 'SAC_NOMEUF' then Value := QrClienteNOMEUF.Value;
  if VarName = 'SAC_IE' then
    if QrClienteTipo.Value = 0 then Value := QrClienteDOC2.Value
    else Value := ' ';
  if VarName = 'VALOR_EXTENSO' then
  begin
    Value := dmkPF.ExtensoMoney(EdDupVal.Text) + ' ';
    for i := 1 to Trunc((210-Length(Value))/2) do
      Value := Value + 'X ';
  end;

  // user function

  if VarName = 'VARF_FUNDO' then Value := CkFundo.Checked;
end;

procedure TFmPromissoria.frxPromissoria2GetValue(const VarName: string;
  var Value: Variant);
var
  i: Integer;
begin
  if VarName = 'VARF_DIA' then Value := FormatDateTime('dd', TPVencto.Date) else
  if VarName = 'VARF_DIA_TXT' then
    Value := dmkPF.ExtensoMil('0' + FormatDateTime('dd', TPVencto.Date)) else
  if VarName = 'VARF_MES_TXT' then Value := FormatDateTime('mmmm', TPVencto.Date) else
  if VarName = 'VARF_ANO' then Value := FormatDateTime('yyyy', TPVencto.Date) else
  if VarName = 'VARF_EMPRESA_NOME' then Value := DmodG.QrDonoNOMEDONO.Value else
  if VarName = 'VARF_EMPRESA_CNPJ' then Value := Geral.FormataCNPJ_TT(DmodG.QrDonoCNPJ_CPF.Value) else
  if VarName = 'VARF_NUM_PROMISSOR' then Value := EdDupNum.Text else
  if VarName = 'VARF_VALOR' then Value := EdDupVal.Text else
  if VarName = 'VARF_LOCAL_PAGTO' then Value := EdPracaPagto.Text else
  if VarName = 'VARF_DIA_EMI' then Value := FormatDateTime('dd', TPEmissao.Date) else
  if VarName = 'VARF_MES_EMI' then Value := FormatDateTime('mm', TPEmissao.Date) else
  if VarName = 'VARF_ANO_EMI' then Value := FormatDateTime('yyyy', TPEmissao.Date) else
  if VarName = 'VARF_CLIENTE_NOME' then Value := CBCliente.Text else
  if VarName = 'VARF_CLIENTE_CNPJCPF' then Value := QrClienteDOC_TXT.Value else
  if VarName = 'VARF_CLIENTE_ENDERECO' then Value := QrClienteE_ALL.Value else
  //
  if VarName = 'VARF_AVALISTA_1_NOME' then
  begin
    if EdAvalista1.ValueVariant = 0 then
      Value := ''
    else
      Value := QrAvalista1NO_ENT.Value;
  end else
  if VarName = 'VARF_AVALISTA_1_CNPJ' then Value := Geral.FormataCNPJ_TT(QrAvalista1CNPJCPF.Value) else
  if VarName = 'VARF_AVALISTA_1_TEL1' then Value := Geral.FormataTelefone_TT_Curto(QrAvalista1Te1.Value) else
  //
  if VarName = 'VARF_AVALISTA_2_NOME' then
  begin
    if EdAvalista2.ValueVariant = 0 then
      Value := ''
    else
      Value := QrAvalista2NO_ENT.Value;
  end;
  if VarName = 'VARF_AVALISTA_2_CNPJ' then Value := Geral.FormataCNPJ_TT(QrAvalista2CNPJCPF.Value) else
  if VarName = 'VARF_AVALISTA_2_TEL1' then Value := Geral.FormataTelefone_TT_Curto(QrAvalista2Te1.Value) else
  //
  if VarName = 'VARF_VALOR_EXTENSO' then
  begin
    Value := Geral.Maiusculas(dmkPF.ExtensoMoney(EdDupVal.Text), False) + ' ';
    for i := 1 to Trunc((70 - Length(Value)/2)) do
      Value := Value + 'X ';
  end;
  if VarName = 'FundoPromissoriaExiste' then Value := FileExists(FNotaPromissoriaBMP) else
  if VarName = 'FundoPromissoriaArquivo' then Value := FNotaPromissoriaBMP else
end;

procedure TFmPromissoria.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  QrCliente.Close;
  UnDmkDAC_PF.AbreQuery(QrCliente, Dmod.MyDB);
  /////////////////
  QrAvalista1.Close;
  UnDmkDAC_PF.AbreQuery(QrAvalista1, Dmod.MyDB);
  /////////////////
  QrAvalista2.Close;
  UnDmkDAC_PF.AbreQuery(QrAvalista2, Dmod.MyDB);
  /////////////////
  BtBxaNP_BMP.Enabled := not FileExists(FNotaPromissoriaBMP);
  //
  TPEmissao.Date := Date;
  TPVencto.Date  := Date;
end;

procedure TFmPromissoria.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);

end;

end.

