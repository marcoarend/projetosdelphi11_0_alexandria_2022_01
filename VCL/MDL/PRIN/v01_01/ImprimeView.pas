unit ImprimeView;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, UnInternalConsts, Buttons, DBCtrls, Db, (*DBTables,*)
  UMySQLModule, mySQLDbTables, ComCtrls, dmkGeral, dmkRadioGroup,
  dmkEdit, dmkImage, UnDmkEnums;

type
  TFmImprimeView = class(TForm)
    PainelDados: TPanel;
    PCTabelas: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    TabSheet3: TTabSheet;
    TabSheet4: TTabSheet;
    RGEntidades: TRadioGroup;
    TabSheet5: TTabSheet;
    RGFaturamento: TRadioGroup;
    RGProdutos: TRadioGroup;
    RGServicos: TRadioGroup;
    Panel2: TPanel;
    RGNF_A: TRadioGroup;
    TabSheet6: TTabSheet;
    RGTransportadora: TRadioGroup;
    QrInvalidoM: TmySQLQuery;
    QrInvalidoL: TmySQLQuery;
    QrInvalidoMCodigo: TIntegerField;
    QrInvalidoMControle: TIntegerField;
    QrInvalidoMBand: TIntegerField;
    QrInvalidoMNome: TWideStringField;
    QrInvalidoMTab_Tipo: TIntegerField;
    QrInvalidoMTab_Campo: TIntegerField;
    QrInvalidoMPadrao: TWideStringField;
    QrInvalidoMPos_Topo: TIntegerField;
    QrInvalidoMPos_Altu: TIntegerField;
    QrInvalidoMPos_Larg: TIntegerField;
    QrInvalidoMPos_MEsq: TIntegerField;
    QrInvalidoMSet_TAli: TIntegerField;
    QrInvalidoMSet_BAli: TIntegerField;
    QrInvalidoMSet_Negr: TIntegerField;
    QrInvalidoMSet_Ital: TIntegerField;
    QrInvalidoMSet_Unde: TIntegerField;
    QrInvalidoMSet_Tam: TIntegerField;
    QrInvalidoMFormato: TIntegerField;
    QrInvalidoMLk: TIntegerField;
    QrInvalidoMDataCad: TDateField;
    QrInvalidoMDataAlt: TDateField;
    QrInvalidoMUserCad: TIntegerField;
    QrInvalidoMUserAlt: TIntegerField;
    QrInvalidoMPrefixo: TWideStringField;
    QrInvalidoMSufixo: TWideStringField;
    QrInvalidoMSubstitui: TSmallintField;
    QrInvalidoMSubstituicao: TWideStringField;
    QrInvalidoMSet_T_DOS: TSmallintField;
    QrInvalidoMAlt_Linha: TIntegerField;
    QrInvalidoMNulo: TSmallintField;
    QrInvalidoLCodigo: TIntegerField;
    QrInvalidoLControle: TIntegerField;
    QrInvalidoLBand: TIntegerField;
    QrInvalidoLNome: TWideStringField;
    QrInvalidoLTab_Tipo: TIntegerField;
    QrInvalidoLTab_Campo: TIntegerField;
    QrInvalidoLPadrao: TWideStringField;
    QrInvalidoLPos_Topo: TIntegerField;
    QrInvalidoLPos_Altu: TIntegerField;
    QrInvalidoLPos_Larg: TIntegerField;
    QrInvalidoLPos_MEsq: TIntegerField;
    QrInvalidoLSet_TAli: TIntegerField;
    QrInvalidoLSet_BAli: TIntegerField;
    QrInvalidoLSet_Negr: TIntegerField;
    QrInvalidoLSet_Ital: TIntegerField;
    QrInvalidoLSet_Unde: TIntegerField;
    QrInvalidoLSet_Tam: TIntegerField;
    QrInvalidoLFormato: TIntegerField;
    QrInvalidoLLk: TIntegerField;
    QrInvalidoLDataCad: TDateField;
    QrInvalidoLDataAlt: TDateField;
    QrInvalidoLUserCad: TIntegerField;
    QrInvalidoLUserAlt: TIntegerField;
    QrInvalidoLPrefixo: TWideStringField;
    QrInvalidoLSufixo: TWideStringField;
    QrInvalidoLSubstitui: TSmallintField;
    QrInvalidoLSubstituicao: TWideStringField;
    QrInvalidoLSet_T_DOS: TSmallintField;
    QrInvalidoLAlt_Linha: TIntegerField;
    QrInvalidoLNulo: TSmallintField;
    TabSheet7: TTabSheet;
    RGDuplicata: TRadioGroup;
    TabSheet8: TTabSheet;
    RGNF_B: TRadioGroup;
    TabSheet9: TTabSheet;
    RGNCMs: TdmkRadioGroup;
    TabSheet10: TTabSheet;
    RGNF_C: TRadioGroup;
    Panel3: TPanel;
    Label2: TLabel;
    EdNome: TdmkEdit;
    Label9: TLabel;
    EdTopoReal: TdmkEdit;
    Label8: TLabel;
    EdTopoBanda: TdmkEdit;
    Label1: TLabel;
    EdTopo: TdmkEdit;
    Label3: TLabel;
    EdAltu: TdmkEdit;
    Label4: TLabel;
    EdMEsq: TdmkEdit;
    Label5: TLabel;
    EdLarg: TdmkEdit;
    Label12: TLabel;
    EdAlt_Linha: TdmkEdit;
    GroupBox1: TGroupBox;
    Label6: TLabel;
    CkNegr: TCheckBox;
    CkItal: TCheckBox;
    CkSubl: TCheckBox;
    EdFont: TdmkEdit;
    RGAlign1: TRadioGroup;
    RGSet_T_DOS: TRadioGroup;
    CkNulo: TCheckBox;
    CkSubstitui: TCheckBox;
    EdSubstituicao: TdmkEdit;
    RGTabela: TRadioGroup;
    RGFormato: TRadioGroup;
    RGAlign2: TRadioGroup;
    Panel1: TPanel;
    Label7: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    EdPadrao: TdmkEdit;
    EdPrefixo: TdmkEdit;
    EdSufixo: TdmkEdit;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel5: TPanel;
    BtConfirma: TBitBtn;
    procedure FormActivate(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure EdTopoRealExit(Sender: TObject);
    procedure EdTopoRealChange(Sender: TObject);
    procedure EdTopoBandaChange(Sender: TObject);
    procedure EdLargKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure RGDuplicataClick(Sender: TObject);
    procedure RGNF_BClick(Sender: TObject);
  private
    { Private declarations }
    procedure CalculaTopo;
  public
    { Public declarations }
    FUmAUm: Boolean;
    FCodigo,
    FControle,
    FPageMEsq, FBand: Integer;
  end;

var
  FmImprimeView: TFmImprimeView;

implementation

uses UnMyObjects, Module, Imprime, ModuleGeral, DmkDAC_PF;

{$R *.DFM}

procedure TFmImprimeView.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmImprimeView.BtDesisteClick(Sender: TObject);
begin
  if ImgTipo.SQLType = stIns then
  UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'ImprimeBand', FControle);
  FmImprime.FDesiste := True;
  Close;
end;

procedure TFmImprimeView.BtConfirmaClick(Sender: TObject);
var
  Excluso, Larg, Topo, MEsq, Negr, Ital, Subl, Campo, Substitui, Linha, Nulo: Integer;
begin
  Topo := Geral.IMV(EdTopo.Text);
  MEsq := Geral.IMV(EdMEsq.Text);
  Larg := Geral.IMV(EdLarg.Text);
  if ImgTipo.SQLType = stUpd then Excluso := FControle
  else Excluso := -1000000;
  //
  QrInvalidoM.Close;
  QrInvalidoM.Params[0].AsInteger := FCodigo;
  QrInvalidoM.Params[1].AsInteger := FBand;
  QrInvalidoM.Params[2].AsInteger := Topo;
  QrInvalidoM.Params[3].AsInteger := MEsq;
  QrInvalidoM.Params[4].AsInteger := MEsq;
  QrInvalidoM.Params[5].AsInteger := Excluso;
  UnDmkDAC_PF.AbreQuery(QrInvalidoM, Dmod.MyDB);
  if QrInvalidoM.RecordCount > 0 then
  begin
    if Geral.MB_Pergunta('A margem esquerda '+
    'informada � menor que esperado. O campo "'+QrInvalidoMNome.Value+
    '" permite a margem m�nima de '+intToStr(QrInvalidoMPos_MEsq.Value+
    QrInvalidoMPos_Larg.Value+1)+', em fun��o do seu comprimento!'+
    ' Deseja continuar assim mesmo?') <> ID_YES then Exit;
  end;
  //
  QrInvalidoL.Close;
  QrInvalidoL.Params[0].AsInteger := FCodigo;
  QrInvalidoL.Params[1].AsInteger := FBand;
  QrInvalidoL.Params[2].AsInteger := Topo;
  QrInvalidoL.Params[3].AsInteger := MEsq;
  QrInvalidoL.Params[4].AsInteger := MEsq+Larg;
  QrInvalidoL.Params[5].AsInteger := Excluso;
  UnDmkDAC_PF.AbreQuery(QrInvalidoL, Dmod.MyDB);
  if QrInvalidoL.RecordCount > 0 then
  begin
    if Geral.MB_Pergunta('O comprimento � maior '+
    'que o esperado. O campo "'+QrInvalidoLNome.Value+
    '" permite o tamanho m�ximo de '+intToStr(QrInvalidoLPos_MEsq.Value-
    MEsq-1)+', em fun��o da sua margem esquerda. ' + sLineBreak +
    'Deseja continuar assim mesmo?') <> ID_YES then Exit;
  end;
  //
  Linha := Geral.IMV(EdMEsq.Text) + Geral.IMV(EdLarg.Text)+FPageMEsq;
  if Linha > 2032 then
  begin
    if Geral.MB_Pergunta('O comprimento total da linha n�o pode '+
    'ser superior a '+FloatToStr(203.2)+' mm, e esta linha ter� '+
    FormatFloat('0.0', Linha/10)+' mm. ' + sLineBreak +
    'Deseja continuar assim mesmo?') <> ID_YES then Exit;
  end;
  if Trim(EdNome.Text) = '' then begin
    ShowMessage('Defina a descri��o do relat�rio!');
    EdNome.SetFocus;
    Exit;
  end;
  try
    if CkNegr.Checked then Negr := 1 else Negr := 0;
    if CkItal.Checked then Ital := 1 else Ital := 0;
    if CkSubl.Checked then Subl := 1 else Subl := 0;
    Campo := 0;
    case RGTabela.ItemIndex of
      0: Campo := RGNF_A.ItemIndex;
      1: Campo := RGEntidades.ItemIndex;
      2: Campo := RGProdutos.ItemIndex;
      3: Campo := RGServicos.ItemIndex;
      4: Campo := RGFaturamento.ItemIndex;
      5: Campo := RGTransportadora.ItemIndex;
      6: Campo := RGDuplicata.ItemIndex;
      7: Campo := RGNF_B.ItemIndex;
      8: Campo := RGNCMs.ItemIndex;
      9: Campo := RGNF_C.ItemIndex;
    end;
    if CkSubstitui.Checked then Substitui := 1 else Substitui := 0;
    if CkNulo.Checked then Nulo := 1 else Nulo := 0;
    Dmod.QrUpd.SQL.Clear;
    if ImgTipo.SQLType = stIns then begin
      Dmod.QrUpd.SQL.Add('INSERT INTO imprimeview SET ');
      Dmod.QrUpd.SQL.Add('Nome=:P0, Pos_Topo=:P1, Pos_Altu=:P2, Pos_MEsq=:P3,');
      Dmod.QrUpd.SQL.Add('Pos_Larg=:P4, Set_TAli=:P5, Set_BAli=:P6, ');
      Dmod.QrUpd.SQL.Add('Set_Negr=:P7, Set_Ital=:P8, Set_Unde=:P9, ');
      Dmod.QrUpd.SQL.Add('Set_Tam=:P10, Tab_Tipo=:P11, Tab_Campo=:P12, ');
      Dmod.QrUpd.SQL.Add('Padrao=:P13, Formato=:P14, Band=:P15, ');
      Dmod.QrUpd.SQL.Add('Prefixo=:P16, Sufixo=:P17, Substitui=:P18,');
      Dmod.QrUpd.SQL.Add('Substituicao=:P19, Set_T_DOS=:P20, Alt_Linha=:P21, ');
      Dmod.QrUpd.SQL.Add('Nulo=:P22, ');
      Dmod.QrUpd.SQL.Add('DataCad=:Px, UserCad=:Py, Codigo=:Pz, Controle=:Pw');
    end else begin
      Dmod.QrUpd.SQL.Add('UPDATE imprimeview SET ');
      Dmod.QrUpd.SQL.Add('Nome=:P0, Pos_Topo=:P1, Pos_Altu=:P2, Pos_MEsq=:P3,');
      Dmod.QrUpd.SQL.Add('Pos_Larg=:P4, Set_TAli=:P5, Set_BAli=:P6, ');
      Dmod.QrUpd.SQL.Add('Set_Negr=:P7, Set_Ital=:P8, Set_Unde=:P9, ');
      Dmod.QrUpd.SQL.Add('Set_Tam=:P10, Tab_Tipo=:P11, Tab_Campo=:P12, ');
      Dmod.QrUpd.SQL.Add('Padrao=:P13, Formato=:P14, Band=:P15, ');
      Dmod.QrUpd.SQL.Add('Prefixo=:P16, Sufixo=:P17, Substitui=:P18,');
      Dmod.QrUpd.SQL.Add('Substituicao=:P19, Set_T_DOS=:P20, Alt_Linha=:P21, ');
      Dmod.QrUpd.SQL.Add('Nulo=:P22, ');
      Dmod.QrUpd.SQL.Add('DataCad=:Px, UserCad=:Py WHERE Codigo=:Pz AND Controle=:Pw');
    end;
    Dmod.QrUpd.Params[00].AsString  := EdNome.Text;
    Dmod.QrUpd.Params[01].AsInteger := Topo;
    Dmod.QrUpd.Params[02].AsInteger := Geral.IMV(EdAltu.Text);
    Dmod.QrUpd.Params[03].AsInteger := MEsq;
    Dmod.QrUpd.Params[04].AsInteger := Larg;
    Dmod.QrUpd.Params[05].AsInteger := RGAlign1.ItemIndex;
    Dmod.QrUpd.Params[06].AsInteger := RGAlign2.ItemIndex;
    Dmod.QrUpd.Params[07].AsInteger := Negr;
    Dmod.QrUpd.Params[08].AsInteger := Ital;
    Dmod.QrUpd.Params[09].AsInteger := Subl;
    Dmod.QrUpd.Params[10].AsInteger := Geral.IMV(EdFont.Text);
    Dmod.QrUpd.Params[11].AsInteger := RGTabela.ItemIndex;
    Dmod.QrUpd.Params[12].AsInteger := Campo;
    Dmod.QrUpd.Params[13].AsString  := EdPadrao.Text;
    Dmod.QrUpd.Params[14].AsInteger := RGFormato.ItemIndex;
    Dmod.QrUpd.Params[15].AsInteger := FBand;
    Dmod.QrUpd.Params[16].AsString  := EdPrefixo.Text;
    Dmod.QrUpd.Params[17].AsString  := EdSufixo.Text;
    Dmod.QrUpd.Params[18].AsInteger := Substitui;
    Dmod.QrUpd.Params[19].AsString  := EdSubstituicao.Text;
    Dmod.QrUpd.Params[20].AsInteger := RGSet_T_DOS.ItemIndex;
    Dmod.QrUpd.Params[21].AsInteger := Geral.IMV(EdAlt_Linha.Text);
    Dmod.QrUpd.Params[22].AsInteger := Nulo;
    //
    Dmod.QrUpd.Params[23].AsString  := FormatDateTime(VAR_FORMATDATE, Date);
    Dmod.QrUpd.Params[24].AsInteger := VAR_USUARIO;
    Dmod.QrUpd.Params[25].AsInteger := FCodigo;
    Dmod.QrUpd.Params[26].AsInteger := FControle;
    Dmod.QrUpd.ExecSQL;
    FmImprime.FView := FControle;
    if FUmAUm then
    begin
      FmImprime.FDesiste := False;
      Close;
    end else begin
      FmImprime.FView := FControle;
      FControle := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
      'ImprimeView', 'ImprimeView', 'Codigo');
      Geral.MB_Info('Registro incluido com sucesso');
      EdNome.SetFocus;
      ImgTipo.SQLType := stIns;
      if RGTabela.ItemIndex = 7 then
      begin
        EdNome.Text := '';
        if RGNF_B.ItemIndex +1 < RGNF_B.Items.Count then
        RGNF_B.ItemIndex := RGNF_B.ItemIndex + 1;
        //RGNF_BClick(Self);
        EdNome.SetFocus;
      end;
      FmImprime.ReopenImprimeView(FControle);
    end;
  except;
    raise;
  end;
end;

procedure TFmImprimeView.FormResize(Sender: TObject);
begin
   MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmImprimeView.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  PCTabelas.ActivePage := TabSheet1;
end;

procedure TFmImprimeView.EdTopoRealExit(Sender: TObject);
begin
  CalculaTopo;
end;

procedure TFmImprimeView.CalculaTopo;
var
  Banda, Real: Integer;
begin
  Real  := Geral.IMV(EdTopoReal.Text);
  Banda := Geral.IMV(EdTopoBanda.Text);
  EdTopo.Text := IntToStr(Real-Banda);
end;

procedure TFmImprimeView.EdTopoRealChange(Sender: TObject);
begin
  CalculaTopo;
end;

procedure TFmImprimeView.EdTopoBandaChange(Sender: TObject);
begin
  CalculaTopo;
end;

procedure TFmImprimeView.EdLargKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  Resp: String;
  Valor: Integer;
begin
  if Key= VK_F4 then
  begin
    Resp := IntToStr(Geral.IMV(EdMEsq.Text)+Geral.IMV(EdLarg.Text));
    if InputQuery('Largura', 'Informe a margem direita do campo:', Resp) then
    begin
      Valor := (Geral.IMV(Resp) - Geral.IMV(EdMEsq.Text));
      if Valor > 0 then EdLarg.Text := IntToStr(Valor);
    end;
  end;  
end;

procedure TFmImprimeView.RGDuplicataClick(Sender: TObject);
begin
  EdNome.Text := RGDuplicata.Items[RGDuplicata.ItemIndex];
end;

procedure TFmImprimeView.RGNF_BClick(Sender: TObject);
begin
  if EdNome.Text = '' then
  begin
    case RGTabela.ItemIndex of
      7: EdNome.Text := RGNF_B.Items[RGNF_B.ItemIndex];
      8: EdNome.Text := RGNCMs.Items[RGNCMS.ItemIndex];
      9: EdNome.Text := RGNF_C.Items[RGNF_C.ItemIndex];
    end;
  end;
end;

end.
