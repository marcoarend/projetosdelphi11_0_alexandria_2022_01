object FmEmiteCheque_3: TFmEmiteCheque_3
  Left = 367
  Top = 177
  Caption = 'CHS-PRINT-003 :: Emite Cheque Bematech DP-20'
  ClientHeight = 254
  ClientWidth = 778
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelDados: TPanel
    Left = 0
    Top = 48
    Width = 778
    Height = 92
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object Label10: TLabel
      Left = 512
      Top = 48
      Width = 252
      Height = 13
      Caption = 'Mensagem a ser impressa (m'#225'ximo de 25 caracteres):'
    end
    object Label3: TLabel
      Left = 12
      Top = 48
      Width = 34
      Height = 13
      Caption = 'Banco:'
    end
    object Label1: TLabel
      Left = 12
      Top = 8
      Width = 26
      Height = 13
      Caption = 'Data:'
    end
    object LaValor: TLabel
      Left = 112
      Top = 8
      Width = 27
      Height = 13
      Caption = 'Valor:'
    end
    object Label5: TLabel
      Left = 208
      Top = 8
      Width = 58
      Height = 13
      Caption = 'Benefici'#225'rio:'
    end
    object Label2: TLabel
      Left = 616
      Top = 8
      Width = 36
      Height = 13
      Caption = 'Cidade:'
    end
    object EdMens: TdmkEdit
      Left = 512
      Top = 64
      Width = 257
      Height = 21
      CharCase = ecUpperCase
      TabOrder = 6
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      ValWarn = False
    end
    object TPData: TdmkEditDateTimePicker
      Left = 12
      Top = 24
      Width = 97
      Height = 21
      Date = 40236.599490879630000000
      Time = 40236.599490879630000000
      TabOrder = 0
      ReadOnly = False
      DefaultEditMask = '!99/99/99;1;_'
      AutoApplyEditMask = True
      UpdType = utYes
    end
    object EdValor: TdmkEdit
      Left = 112
      Top = 24
      Width = 93
      Height = 21
      Alignment = taRightJustify
      TabOrder = 1
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 2
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,00'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
    end
    object EdBenef: TdmkEdit
      Left = 208
      Top = 24
      Width = 405
      Height = 21
      CharCase = ecUpperCase
      TabOrder = 2
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      ValWarn = False
    end
    object EdCidade: TdmkEdit
      Left = 616
      Top = 24
      Width = 153
      Height = 21
      CharCase = ecUpperCase
      TabOrder = 3
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      ValWarn = False
    end
    object EdBanco: TdmkEditCB
      Left = 12
      Top = 64
      Width = 50
      Height = 21
      Alignment = taRightJustify
      TabOrder = 4
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 3
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '000'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      DBLookupComboBox = CBBanco
      IgnoraDBLookupComboBox = False
    end
    object CBBanco: TdmkDBLookupComboBox
      Left = 65
      Top = 64
      Width = 440
      Height = 21
      KeyField = 'Codigo'
      ListField = 'Nome'
      ListSource = DsBancos
      TabOrder = 5
      dmkEditCB = EdBanco
      UpdType = utYes
      LocF7SQLMasc = '$#'
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 778
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 730
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 682
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 384
        Height = 32
        Caption = 'Emite Cheque Bematech DP-20'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 384
        Height = 32
        Caption = 'Emite Cheque Bematech DP-20'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 384
        Height = 32
        Caption = 'Emite Cheque Bematech DP-20'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 140
    Width = 778
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 774
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 184
    Width = 778
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 632
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 630
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaSerie2: TLabel
        Left = 240
        Top = 4
        Width = 102
        Height = 37
        Alignment = taRightJustify
        AutoSize = False
        Caption = '???'
        Font.Charset = ANSI_CHARSET
        Font.Color = 16764159
        Font.Height = -32
        Font.Name = 'Arial Rounded MT Bold'
        Font.Style = []
        ParentFont = False
      end
      object LaCheque2: TLabel
        Left = 352
        Top = 4
        Width = 145
        Height = 37
        AutoSize = False
        Caption = '000000'
        Font.Charset = ANSI_CHARSET
        Font.Color = 16764159
        Font.Height = -32
        Font.Name = 'Arial Rounded MT Bold'
        Font.Style = []
        ParentFont = False
      end
      object LaCheque1: TLabel
        Left = 351
        Top = 3
        Width = 145
        Height = 37
        AutoSize = False
        Caption = '000000'
        Font.Charset = ANSI_CHARSET
        Font.Color = clPurple
        Font.Height = -32
        Font.Name = 'Arial Rounded MT Bold'
        Font.Style = []
        ParentFont = False
      end
      object LaSerie1: TLabel
        Left = 239
        Top = 3
        Width = 102
        Height = 37
        Alignment = taRightJustify
        AutoSize = False
        Caption = '???'
        Font.Charset = ANSI_CHARSET
        Font.Color = clPurple
        Font.Height = -32
        Font.Name = 'Arial Rounded MT Bold'
        Font.Style = []
        ParentFont = False
      end
      object BtOK: TBitBtn
        Tag = 14
        Left = 20
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Emite'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object QrBancos: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *'
      'FROM bancos'
      'ORDER BY Nome')
    Left = 12
    Top = 12
    object QrBancosCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrBancosNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QrBancosXlsLinha: TIntegerField
      FieldName = 'XlsLinha'
    end
    object QrBancosXlsData: TWideStringField
      FieldName = 'XlsData'
      Size = 2
    end
    object QrBancosXlsHist: TWideStringField
      FieldName = 'XlsHist'
      Size = 2
    end
    object QrBancosXlsDocu: TWideStringField
      FieldName = 'XlsDocu'
      Size = 2
    end
    object QrBancosXlsHiDo: TWideStringField
      FieldName = 'XlsHiDo'
      Size = 2
    end
    object QrBancosXlsCred: TWideStringField
      FieldName = 'XlsCred'
      Size = 2
    end
    object QrBancosXlsDebi: TWideStringField
      FieldName = 'XlsDebi'
      Size = 2
    end
    object QrBancosXlsCrDb: TWideStringField
      FieldName = 'XlsCrDb'
      Size = 2
    end
    object QrBancosXlsDouC: TWideStringField
      FieldName = 'XlsDouC'
      Size = 2
    end
    object QrBancosXlsTCDB: TSmallintField
      FieldName = 'XlsTCDB'
    end
    object QrBancosXlsComp: TWideStringField
      FieldName = 'XlsComp'
      Size = 2
    end
    object QrBancosXlsCPMF: TWideStringField
      FieldName = 'XlsCPMF'
      Size = 2
    end
    object QrBancosXlsSldo: TWideStringField
      FieldName = 'XlsSldo'
      Size = 2
    end
  end
  object DsBancos: TDataSource
    DataSet = QrBancos
    Left = 40
    Top = 12
  end
end
