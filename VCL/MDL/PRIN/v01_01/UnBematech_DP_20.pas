unit UnBematech_DP_20;

interface

uses
  StdCtrls, ExtCtrls, Windows, Messages, SysUtils, Classes, Graphics, Controls,
  Forms, dmkGeral, dmkEdit, Dialogs, SHFolder, UnDmkEnums;

type
  TUnBematech_DP_20 = class(TObject)
  private
    { Private declarations }
  public
    { Public declarations }
    function ImpressoraHabilita(var Retorno: String): Boolean;
    function ImpressoraDesabilita(var Retorno: String): Boolean;
    procedure ImpressoraVerifica(var Retorno: String);
    function ChequeImprime(Banco: Integer; Valor: Double; Beneficiario, Cidade:
             String; Data: TDateTime; Mensagem: String; var Retorno: String):
             Boolean;
    function FolhaTrava(var Retorno: String): Boolean;
    function FolhaDestrava(var Retorno: String): Boolean;
  end;

// Fun��o para abrir a porta de comunica��o.
function Bematech_DP_IniciaPorta(Porta: AnsiString): integer; stdcall; far; external 'BemaDP32.dll';
// Fun��o para fechar a porta de comunica��o.
function Bematech_DP_FechaPorta: integer; stdcall; far; external 'BemaDP32.dll';
// Fun��o para imprimir o cheque normal.
function Bematech_DP_ImprimeCheque (Banco: AnsiString; Valor: AnsiString; Favorecido: AnsiString; Cidade: AnsiString; Data: AnsiString; Mensagem: AnsiString): integer; stdcall; far; external 'BemaDP32.dll';
// Fun��o para imprimir o cheque transfer�ncia.
function Bematech_DP_ImprimeChequeTransferencia (Banco: AnsiString; Valor: AnsiString; Cidade: AnsiString; Data: AnsiString; Agencia: AnsiString; Conta: AnsiString; Mensagem: AnsiString): integer; stdcall; far; external 'BemaDP32.dll';
// Fun��o para imprimir a c�pia do cheque.
function Bematech_DP_ImprimeCopiaCheque: integer; stdcall; far; external 'BemaDP32.dll';
// Fun��o para travar o documento.
function Bematech_DP_TravaDocumento (Trava: integer): integer; stdcall; far; external 'BemaDP32.dll';
// Fun��o para imprimir texto comum, sem formata��o.
function Bematech_DP_ImprimeTexto (Texto: AnsiString; AvancaLinha: integer): integer; stdcall; far; external 'BemaDP32.dll';
// Fun��o para imprimir texto formatado: it�lico, expandido, negrito.
function Bematech_DP_FormataTexto (Texto: AnsiString; Letra: AnsiString; Italico: integer; Expandido: integer; Negrito: integer): integer; stdcall; far; external 'BemaDP32.dll';
// Fun��o para incluir o nome da cidade e do favorecido no arquivo BEMADP32.INI.
function Bematech_DP_IncluiCidadeFavorecido (Cidade: AnsiString; Favorecido: AnsiString): integer; stdcall; far; external 'BemaDP32.dll';
// Fun��o para incluir e alterar o banco e suas coordenadas para impress�o de cheque
// normal no arquivo BEMADP32.INI.
function Bematech_DP_IncluiAlteraBanco (Banco: AnsiString; Coordenadas: AnsiString): integer; stdcall; far; external 'BemaDP32.dll';
// Fun��o para incluir e alterar o banco e suas coordenadas para impress�o de cheque
// transfer�ncia no arquivo BEMADP32.INI.
function Bematech_DP_IncluiAlteraBancoTransferencia (Banco: AnsiString; Coordenadas: AnsiString): integer; stdcall; far; external 'BemaDP32.dll';
// Fun��o para programar moeda na impressora com teclado
function Bematech_DP_ProgramaMoeda (MoedaSingular: AnsiString; MoedaPlural: AnsiString): integer; stdcall; far; external 'BemaDP32.dll';
// Fun��o para programar favorecido na impressora com teclado.
function Bematech_DP_ProgramaFavorecido (CodiFavorecido: AnsiString; NomeFavorecido: AnsiString): integer; stdcall; far; external 'BemaDP32.dll';
// Fun��o para desprogramar favorecido na impressora com teclado.
function Bematech_DP_DesprogramaFavorecido (CodiFavorecido: AnsiString): integer; stdcall; far; external 'BemaDP32.dll';
// Fun��o para programa o banco e suas coordenadas na impressora com teclado.
function Bematech_DP_ProgramaBanco (Banco: AnsiString; Coordenadas: AnsiString): integer; stdcall; far; external 'BemaDP32.dll';
// Fun��o para excluir um banco usado para impress�o de cheque normal do arquivo BEMADP32.INI.
function Bematech_DP_ExcluiBanco (Banco: AnsiString): integer; stdcall; far; external 'BemaDP32.dll';
// Fun��o para excluir um banco usado para impress�o de cheque transfer�ncia do arquivo
// BEMADP32.INI.
function Bematech_DP_ExcluiBancoTransferencia (Banco: AnsiString): integer; stdcall; far; external 'BemaDP32.dll';
// Fun��o para enviar comandos para a impressora
function Bematech_DP_EnviaComando (Comando: AnsiString; Tamanho: integer): integer;  stdcall; far; external 'BemaDP32.dll';
// Fun��o para configura��es especiais na impressora com teclado.
function Bematech_DP_ConfiguraImpressora (LinhaChancela: integer; Preenchimento: integer; Velocidade: integer; NumeroBits: integer; Paridade: integer; Centavos: integer): integer; stdcall; far; external 'BemaDP32.dll';
// Fun��o para cancelar o relat�rio de cheques emitidos na impressora.
function Bematech_DP_CancelaRelatorio (Opcao: AnsiString): integer; stdcall; far; external 'BemaDP32.dll';
// Fun��o para configurar o modelo da impressora.
function Bematech_DP_ConfiguraModeloImpressora (ModeloImpressora: integer): integer; stdcall; far; external 'BemaDP32.dll';
// Fun��o para reinicializar a configura��o de mem�ria da impressora com teclado.
function Bematech_DP_ReinicializaConfiguracao: integer; stdcall; far; external 'BemaDP32.dll';

var
  Bematech_DP_20: TUnBematech_DP_20;


implementation

uses UnInternalConsts;

const
  FImpressora = 'Bematech DP-20';

function TUnBematech_DP_20.ImpressoraDesabilita(var Retorno: String): Boolean;
var
  Res: Integer;
begin
  Result := False;
  Res := 0;
  Retorno := '';
  if VAR_IMPCHEQUE = 3 then
  begin
    Res := Bematech_DP_FechaPorta();
    case Res of
      0:
      begin
        Retorno := FImpressora + '. ' + 'Erro ao tentar fechar a porta de comunica��o.';
        Exit;
      end;  
      //1 (um): Sucesso.
    end;
  end;
  Result := Res = 1;
end;

function TUnBematech_DP_20.ImpressoraHabilita(var Retorno: String): Boolean;
var
  Res: Integer;
begin
  Result := False;
  Res := 0;
  Retorno := '';
  if VAR_IMPCHEQUE = 3 then
  begin
    Res := Bematech_DP_IniciaPorta(PChar(VAR_IMPCHEQUE_PORTA));
    case Res of
      0:
      begin
        Retorno := FImpressora + '. ' + 'Erro ao tentar abrir a porta de comunica��o: ' +
         VAR_IMPCHEQUE_PORTA + '.';
        Exit;
      end;
      //1 (um): Sucesso.
    end;
  end;
  Result := Res = 1;
end;

procedure TUnBematech_DP_20.ImpressoraVerifica(var Retorno: String);
var
  //Caminho, Resposta: AnsiString;
  AchouDLL, AchouINI: Boolean;
  PathDLL, PathINI, Dir, Texto: AnsiString;
begin
  Retorno := '';
  if VAR_IMPCHEQUE = 3 then
  begin
    Dir := Geral.PastaEspecial(CSIDL_WINDOWS);
    //  DLL e INI
    PathDLL := Dir + '\BEMADP32.DLL';
    AchouDLL := FileExists(PathDLL);
    PathINI := Dir + '\BEMADP32.INI';
    AchouINI := FileExists(PathINI);
    //
    Texto := '';
    if not AchouINI then
      Texto := Texto + sLineBreak + PathINI;
    if not AchouDLL then
      Texto := Texto + sLineBreak + PathDLL;

    if Texto <> '' then
    begin
      Retorno :=
      'Os seguintes arquivos da Bematech n�o foram localizados:' +
      Texto;
    end else begin
      if ImpressoraHabilita(Retorno) then
        if VAR_STIMPCHEQUE <> nil then
          VAR_STIMPCHEQUE.Text := FImpressora;
      ImpressoraDesabilita(Retorno);
    end;
  end;
end;

function TUnBematech_DP_20.ChequeImprime(Banco: Integer; Valor: Double;
  Beneficiario, Cidade: String; Data: TDateTime; Mensagem: String;
  var Retorno: String): Boolean;
var
  Res: Integer;
  sBanco, sValor, sData, sBenef, sCidade, sMsg: AnsiString;
begin
  Result := False;
  Res := 0;
  Retorno := '';
  sBanco := FormatFloat('000', Banco);
  if Banco = 0 then
  begin
    Retorno := FImpressora + '. ' + 'Banco n�o informado!';
    Exit;
  end;
  if Valor < 0.01 then
  begin
    Retorno := FImpressora + '. ' + 'Informe um valor v�lido!';
    Exit;
  end;
  sValor := Geral.FFT(Valor, 2, siPositivo);
  if Data < 2 then
  begin
    Retorno := FImpressora + '. ' + 'Informe uma data v�lida!';
    Exit;
  end;
  sData  := FormatDateTime('ddmmyyyy', Data);
  sBenef := Copy(Beneficiario, 1, 80);
  //if Geral.FIV(sBenef = '', 'Informe um benefici�rio!') then Exit;
  sCidade:= Copy(Cidade, 1, 30);
  if sCidade = '' then
  begin
    Retorno := FImpressora + '. ' + 'Informe uma cidade!';
    Exit;
  end;
  //
  sMsg := Copy(Mensagem, 1, 25);
  if ImpressoraHabilita(Retorno) then
  begin
    if FolhaTrava(Retorno) then
    begin
      Res := Bematech_DP_ImprimeCheque(sBanco, sValor, Beneficiario, Cidade, sData, sMsg);
      case Res of
        -3:
        begin
          Retorno := FImpressora + '. ' + 'Banco n�o localizado!';
          Exit;
        end;
        -2:
        begin
          Retorno := FImpressora + '. ' + 'Par�metro inv�lido!';
          Exit;
        end;
        0:
        begin
          Retorno := FImpressora + '. ' + 'Erro de comunica��o com a impressora ao imprimir o cheque!';
          Exit;
        end;
        //1 (um): Sucesso.
      end;
      //
      FolhaDestrava(Retorno);
    end;
    ImpressoraDesabilita(Retorno);
  end;
  Result := Res = 1;
end;

function TUnBematech_DP_20.FolhaDestrava(var Retorno: String): Boolean;
var
  Res: Integer;
begin
  Result := False;
  Retorno := '';
  Res := 0;
  if VAR_IMPCHEQUE = 3 then
  begin
    Res := Bematech_DP_TravaDocumento(0);
    case Res of
      0:
      begin
        Retorno := FImpressora + '. ' + 'Erro ao tentar destravar a folha.';
        Exit;
      end;
      //1 (um): Sucesso.
    end;
  end;
  Result := Res = 1;
end;

function TUnBematech_DP_20.FolhaTrava(var Retorno: String): Boolean;
var
  Res: Integer;
begin
  Result := False;
  Retorno := '';
  Res := 0;
  if VAR_IMPCHEQUE = 3 then
  begin
    Res := Bematech_DP_TravaDocumento(1);
    case Res of
      0:
      begin
        Retorno := FImpressora + '. ' + 'Erro ao tentar ravar a folha.';
        Exit;
      end;
      //1 (um): Sucesso.
    end;
  end;
  Result := Res = 1;
end;

end.




