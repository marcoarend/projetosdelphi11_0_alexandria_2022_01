unit EmiteCheque_3;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, ComCtrls, dmkGeral,
  dmkEditDateTimePicker, dmkEdit, DBCtrls, dmkDBLookupComboBox, dmkEditCB, DB,
  mySQLDbTables, dmkImage, UnDmkEnums;

type
  TFmEmiteCheque_3 = class(TForm)
    PainelDados: TPanel;
    Label10: TLabel;
    EdMens: TdmkEdit;
    Label3: TLabel;
    Label1: TLabel;
    TPData: TdmkEditDateTimePicker;
    LaValor: TLabel;
    EdValor: TdmkEdit;
    Label5: TLabel;
    EdBenef: TdmkEdit;
    Label2: TLabel;
    EdCidade: TdmkEdit;
    QrBancos: TmySQLQuery;
    QrBancosCodigo: TIntegerField;
    QrBancosNome: TWideStringField;
    QrBancosXlsLinha: TIntegerField;
    QrBancosXlsData: TWideStringField;
    QrBancosXlsHist: TWideStringField;
    QrBancosXlsDocu: TWideStringField;
    QrBancosXlsHiDo: TWideStringField;
    QrBancosXlsCred: TWideStringField;
    QrBancosXlsDebi: TWideStringField;
    QrBancosXlsCrDb: TWideStringField;
    QrBancosXlsDouC: TWideStringField;
    QrBancosXlsTCDB: TSmallintField;
    QrBancosXlsComp: TWideStringField;
    QrBancosXlsCPMF: TWideStringField;
    QrBancosXlsSldo: TWideStringField;
    DsBancos: TDataSource;
    EdBanco: TdmkEditCB;
    CBBanco: TdmkDBLookupComboBox;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaCheque1: TLabel;
    LaCheque2: TLabel;
    LaSerie1: TLabel;
    LaSerie2: TLabel;
    procedure BtOKClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  var
    FmEmiteCheque_3: TFmEmiteCheque_3;

implementation

uses UnMyObjects, UnBematech_DP_20, DmkDAC_PF, Module;

{$R *.DFM}

procedure TFmEmiteCheque_3.BtOKClick(Sender: TObject);
var
  Cidade, Nome, Msg, Retorno: String;
begin
  Cidade := Geral.SemAcento(Geral.Maiusculas(EdCidade.Text, False));
  Nome   := Geral.SemAcento(Geral.Maiusculas(EdBenef.Text, False));
  Msg    := Geral.SemAcento(Geral.Maiusculas(EdMens.Text, False));
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Enviando dados');
  Bematech_DP_20.ChequeImprime(EdBanco.ValueVariant, EdValor.ValueVariant,
  Nome, Cidade, TPData.Date, Msg, Retorno);
  if Trim(Retorno) = '' then
    MyObjects.Informa2(LaAviso1, LaAviso2, False, 'Impress�o do cheque enviado com sucesso!.')
  else
    Geral.MB_Aviso(Retorno);
  //
end;

procedure TFmEmiteCheque_3.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmEmiteCheque_3.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmEmiteCheque_3.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmEmiteCheque_3.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  TPData.Date := Date;
  UnDmkDAC_PF.AbreQuery(QrBancos, Dmod.MyDB);
end;

end.
