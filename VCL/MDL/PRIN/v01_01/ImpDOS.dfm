object FmImpDOS: TFmImpDOS
  Left = 355
  Top = 174
  Caption = 'Cadastro de Impress'#245'es Matriciais'
  ClientHeight = 614
  ClientWidth = 784
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelItens: TPanel
    Left = 0
    Top = 92
    Width = 784
    Height = 522
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 2
    Visible = False
    ExplicitLeft = 348
    ExplicitTop = 132
    ExplicitHeight = 460
    object Label3: TLabel
      Left = 12
      Top = 48
      Width = 29
      Height = 13
      Caption = 'Linha:'
    end
    object Label4: TLabel
      Left = 56
      Top = 48
      Width = 36
      Height = 13
      Caption = 'Coluna:'
    end
    object Label5: TLabel
      Left = 100
      Top = 48
      Width = 32
      Height = 13
      Caption = 'Letras:'
    end
    object Label6: TLabel
      Left = 12
      Top = 92
      Width = 37
      Height = 13
      Caption = 'Padr'#227'o:'
    end
    object Label7: TLabel
      Left = 204
      Top = 92
      Width = 35
      Height = 13
      Caption = 'Prefixo:'
    end
    object Label8: TLabel
      Left = 396
      Top = 92
      Width = 32
      Height = 13
      Caption = 'Sufixo:'
    end
    object Label11: TLabel
      Left = 588
      Top = 92
      Width = 50
      Height = 13
      Caption = 'Substituto:'
    end
    object Label12: TLabel
      Left = 12
      Top = 4
      Width = 36
      Height = 13
      Caption = 'Tabela:'
    end
    object Label13: TLabel
      Left = 248
      Top = 4
      Width = 88
      Height = 13
      Caption = 'Descri'#231#227'o do item:'
      FocusControl = DBEdit2
    end
    object RGFormato: TRadioGroup
      Left = 12
      Top = 132
      Width = 777
      Height = 141
      Caption = ' Formato: '
      Columns = 3
      ItemIndex = 0
      Items.Strings = (
        ''
        'dd/mm/yy'
        'dd/mm/yyyy'
        'hh:nn'
        'hh:nn:ss'
        '#,###,##0'
        '#,###,##0.0'
        '#,###,##0.00'
        '#,###,##0.000'
        '#,###,##0.0000'
        '#,###,##0.00000'
        '#,###,##0.000000'
        '#,###,##0;-#,###,##0; '
        '#,###,##0.0;-#,###,##0.0; '
        '#,###,##0.00;-#,###,##0.00; '
        '#,###,##0.000;-#,###,##0.000; '
        '#,###,##0.0000;-#,###,##0.0000; '
        '#,###,##0.00000;-#,###,##0.00000; '
        '#,###,##0.000000;-#,###,##0.000000; ')
      TabOrder = 12
    end
    object EdLinha: TdmkEdit
      Left = 12
      Top = 64
      Width = 40
      Height = 21
      Alignment = taRightJustify
      TabOrder = 0
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
    end
    object EdColuna: TdmkEdit
      Left = 56
      Top = 64
      Width = 40
      Height = 21
      Alignment = taRightJustify
      TabOrder = 1
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
    end
    object EdLetras: TdmkEdit
      Left = 100
      Top = 64
      Width = 40
      Height = 21
      Alignment = taRightJustify
      TabOrder = 2
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
    end
    object RGFonte: TRadioGroup
      Left = 144
      Top = 48
      Width = 261
      Height = 37
      Caption = ' Fonte: '
      Columns = 3
      ItemIndex = 1
      Items.Strings = (
        'Normal'
        'Comprimido'
        'Duplo')
      TabOrder = 3
    end
    object CkNegrito: TCheckBox
      Left = 408
      Top = 52
      Width = 61
      Height = 17
      Caption = 'Negrito'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 4
    end
    object CkItalico: TCheckBox
      Left = 408
      Top = 68
      Width = 57
      Height = 17
      Caption = 'It'#225'lico'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsItalic]
      ParentFont = False
      TabOrder = 5
    end
    object CkSubstit: TCheckBox
      Left = 472
      Top = 68
      Width = 61
      Height = 17
      Caption = 'Substituir'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 6
    end
    object EdPadrao: TEdit
      Left = 12
      Top = 108
      Width = 188
      Height = 21
      TabOrder = 8
    end
    object EdPrefixo: TEdit
      Left = 204
      Top = 108
      Width = 188
      Height = 21
      TabOrder = 9
    end
    object EdSufixo: TEdit
      Left = 396
      Top = 108
      Width = 188
      Height = 21
      TabOrder = 10
    end
    object EdSubstituto: TEdit
      Left = 588
      Top = 108
      Width = 188
      Height = 21
      TabOrder = 11
    end
    object DBEdit2: TDBEdit
      Left = 248
      Top = 20
      Width = 529
      Height = 21
      TabStop = False
      Color = clBtnFace
      DataField = 'Descricao'
      DataSource = DsImpDOSImp
      ReadOnly = True
      TabOrder = 13
    end
    object RGAlinhamento: TRadioGroup
      Left = 536
      Top = 48
      Width = 241
      Height = 37
      Caption = ' Alinhamento: '
      Columns = 3
      ItemIndex = 0
      Items.Strings = (
        'Esquerda'
        'Centro'
        'Direita')
      TabOrder = 7
    end
    object EdTabela: TEdit
      Left = 12
      Top = 20
      Width = 233
      Height = 21
      TabStop = False
      Color = clBtnFace
      ReadOnly = True
      TabOrder = 14
    end
    object GroupBox1: TGroupBox
      Left = 0
      Top = 459
      Width = 784
      Height = 63
      Align = alBottom
      TabOrder = 15
      ExplicitTop = 162
      object BtConfirma2: TBitBtn
        Tag = 14
        Left = 12
        Top = 17
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirma2Click
      end
      object Panel4: TPanel
        Left = 674
        Top = 15
        Width = 108
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        ExplicitLeft = 672
        object BtDesiste2: TBitBtn
          Tag = 15
          Left = 7
          Top = 2
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesiste2Click
        end
      end
    end
  end
  object PainelEdita: TPanel
    Left = 0
    Top = 92
    Width = 784
    Height = 522
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
    Visible = False
    ExplicitLeft = 236
    ExplicitTop = 152
    ExplicitHeight = 460
    object Label9: TLabel
      Left = 16
      Top = 8
      Width = 36
      Height = 13
      Caption = 'C'#243'digo:'
    end
    object Label10: TLabel
      Left = 124
      Top = 8
      Width = 51
      Height = 13
      Caption = 'Descri'#231#227'o:'
    end
    object Label16: TLabel
      Left = 408
      Top = 8
      Width = 62
      Height = 13
      Caption = 'Linhas Prod.:'
    end
    object Label17: TLabel
      Left = 480
      Top = 8
      Width = 62
      Height = 13
      Caption = 'Linhas Serv.:'
    end
    object Label18: TLabel
      Left = 552
      Top = 8
      Width = 67
      Height = 13
      Caption = 'Linhas Fatura:'
    end
    object Label19: TLabel
      Left = 624
      Top = 8
      Width = 56
      Height = 13
      Caption = 'Cols Fatura:'
    end
    object Label20: TLabel
      Left = 696
      Top = 8
      Width = 58
      Height = 13
      Caption = 'GAP Fatura:'
    end
    object EdCodigo: TdmkEdit
      Left = 16
      Top = 24
      Width = 100
      Height = 21
      Alignment = taRightJustify
      Font.Charset = DEFAULT_CHARSET
      Font.Color = 8281908
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      ReadOnly = True
      TabOrder = 0
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
    end
    object EdNome: TEdit
      Left = 124
      Top = 24
      Width = 280
      Height = 21
      MaxLength = 20
      TabOrder = 1
    end
    object EdLProd: TdmkEdit
      Left = 408
      Top = 24
      Width = 68
      Height = 21
      Alignment = taRightJustify
      TabOrder = 2
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
    end
    object EdLServ: TdmkEdit
      Left = 480
      Top = 24
      Width = 68
      Height = 21
      Alignment = taRightJustify
      TabOrder = 3
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
    end
    object EdLFatu: TdmkEdit
      Left = 552
      Top = 24
      Width = 68
      Height = 21
      Alignment = taRightJustify
      TabOrder = 4
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
    end
    object EdCFatu: TdmkEdit
      Left = 624
      Top = 24
      Width = 68
      Height = 21
      Alignment = taRightJustify
      TabOrder = 5
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
    end
    object EdEFatu: TdmkEdit
      Left = 696
      Top = 24
      Width = 68
      Height = 21
      Alignment = taRightJustify
      TabOrder = 6
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
    end
    object GBConfirma: TGroupBox
      Left = 0
      Top = 459
      Width = 784
      Height = 63
      Align = alBottom
      TabOrder = 7
      ExplicitTop = 162
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 12
        Top = 17
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object Panel3: TPanel
        Left = 674
        Top = 15
        Width = 108
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        ExplicitLeft = 672
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 7
          Top = 2
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
    end
  end
  object PainelDados: TPanel
    Left = 0
    Top = 92
    Width = 784
    Height = 522
    Align = alClient
    BevelOuter = bvNone
    Color = clAppWorkSpace
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    ExplicitLeft = 96
    ExplicitTop = 28
    ExplicitHeight = 460
    object Panel1: TPanel
      Left = 0
      Top = 0
      Width = 784
      Height = 53
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object Label1: TLabel
        Left = 16
        Top = 8
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
        FocusControl = DBEdCodigo
      end
      object Label2: TLabel
        Left = 120
        Top = 8
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
        FocusControl = DBEdNome
      end
      object Label21: TLabel
        Left = 408
        Top = 8
        Width = 62
        Height = 13
        Caption = 'Linhas Prod.:'
      end
      object Label22: TLabel
        Left = 480
        Top = 8
        Width = 62
        Height = 13
        Caption = 'Linhas Serv.:'
      end
      object Label23: TLabel
        Left = 552
        Top = 8
        Width = 67
        Height = 13
        Caption = 'Linhas Fatura:'
      end
      object Label24: TLabel
        Left = 624
        Top = 8
        Width = 56
        Height = 13
        Caption = 'Cols Fatura:'
      end
      object Label25: TLabel
        Left = 696
        Top = 8
        Width = 58
        Height = 13
        Caption = 'GAP Fatura:'
      end
      object DBEdCodigo: TDBEdit
        Left = 16
        Top = 24
        Width = 100
        Height = 21
        Hint = 'N'#186' do banco'
        TabStop = False
        DataField = 'Codigo'
        DataSource = DsImpDOS
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = 8281908
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        MaxLength = 3
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 0
      end
      object DBEdNome: TDBEdit
        Left = 120
        Top = 24
        Width = 285
        Height = 21
        Hint = 'Nome do banco'
        Color = clWhite
        DataField = 'Descricao'
        DataSource = DsImpDOS
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
      end
      object DBEdit3: TDBEdit
        Left = 408
        Top = 24
        Width = 68
        Height = 21
        DataField = 'LProd'
        DataSource = DsImpDOS
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 2
      end
      object DBEdit4: TDBEdit
        Left = 480
        Top = 24
        Width = 68
        Height = 21
        DataField = 'LServ'
        DataSource = DsImpDOS
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 3
      end
      object DBEdit5: TDBEdit
        Left = 552
        Top = 24
        Width = 68
        Height = 21
        DataField = 'LFatu'
        DataSource = DsImpDOS
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 4
      end
      object DBEdit6: TDBEdit
        Left = 624
        Top = 24
        Width = 68
        Height = 21
        DataField = 'CFatu'
        DataSource = DsImpDOS
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 5
      end
      object DBEdit7: TDBEdit
        Left = 696
        Top = 24
        Width = 68
        Height = 21
        DataField = 'EFatu'
        DataSource = DsImpDOS
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 6
      end
    end
    object Grade: TPageControl
      Left = 120
      Top = 180
      Width = 665
      Height = 193
      ActivePage = TabSheet6
      TabOrder = 1
      object TabSheet1: TTabSheet
        Caption = 'Textos Avulsos'
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object GradeA: TDBGrid
          Left = 0
          Top = 0
          Width = 657
          Height = 165
          Align = alClient
          DataSource = DsA
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          OnDrawColumnCell = GradeADrawColumnCell
          Columns = <
            item
              Expanded = False
              FieldName = 'Descricao'
              Title.Caption = 'Descri'#231#227'o'
              Width = 119
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Linha'
              Width = 40
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Coluna'
              Width = 40
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Letras'
              Width = 40
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'FoTam'
              Title.Caption = 'Fonte'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Negrito'
              Title.Caption = 'N'
              Title.Font.Charset = DEFAULT_CHARSET
              Title.Font.Color = clWindowText
              Title.Font.Height = -11
              Title.Font.Name = 'MS Sans Serif'
              Title.Font.Style = [fsBold]
              Width = 18
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Italico'
              Title.Font.Charset = DEFAULT_CHARSET
              Title.Font.Color = clWindowText
              Title.Font.Height = -11
              Title.Font.Name = 'MS Sans Serif'
              Title.Font.Style = [fsItalic]
              Width = 18
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Padrao'
              Title.Caption = 'Padr'#227'o'
              Width = 60
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Substituir'
              Title.Caption = 'XX'
              Width = 18
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Substituto'
              Width = 60
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Prefixo'
              Width = 60
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Sufixo'
              Width = 60
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Formato'
              Width = 100
              Visible = True
            end>
        end
      end
      object TabSheet2: TTabSheet
        Caption = 'Entidade'
        ImageIndex = 1
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object GradeE: TDBGrid
          Left = 0
          Top = 0
          Width = 657
          Height = 165
          Align = alClient
          DataSource = DsE
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          OnDrawColumnCell = GradeEDrawColumnCell
          Columns = <
            item
              Expanded = False
              FieldName = 'Descricao'
              Title.Caption = 'Descri'#231#227'o'
              Width = 119
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Linha'
              Width = 40
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Coluna'
              Width = 40
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Letras'
              Width = 40
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'FoTam'
              Title.Caption = 'Fonte'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Negrito'
              Title.Caption = 'N'
              Title.Font.Charset = DEFAULT_CHARSET
              Title.Font.Color = clWindowText
              Title.Font.Height = -11
              Title.Font.Name = 'MS Sans Serif'
              Title.Font.Style = [fsBold]
              Width = 18
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Italico'
              Title.Font.Charset = DEFAULT_CHARSET
              Title.Font.Color = clWindowText
              Title.Font.Height = -11
              Title.Font.Name = 'MS Sans Serif'
              Title.Font.Style = [fsItalic]
              Width = 18
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Padrao'
              Title.Caption = 'Padr'#227'o'
              Width = 60
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Substituir'
              Title.Caption = 'XX'
              Width = 18
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Substituto'
              Width = 60
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Prefixo'
              Width = 60
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Sufixo'
              Width = 60
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Formato'
              Width = 100
              Visible = True
            end>
        end
      end
      object TabSheet3: TTabSheet
        Caption = 'Produtos'
        ImageIndex = 2
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object GradeP: TDBGrid
          Left = 0
          Top = 0
          Width = 657
          Height = 165
          Align = alClient
          DataSource = DsP
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          OnDrawColumnCell = GradePDrawColumnCell
          Columns = <
            item
              Expanded = False
              FieldName = 'Descricao'
              Title.Caption = 'Descri'#231#227'o'
              Width = 119
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Linha'
              Width = 40
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Coluna'
              Width = 40
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Letras'
              Width = 40
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'FoTam'
              Title.Caption = 'Fonte'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Negrito'
              Title.Caption = 'N'
              Title.Font.Charset = DEFAULT_CHARSET
              Title.Font.Color = clWindowText
              Title.Font.Height = -11
              Title.Font.Name = 'MS Sans Serif'
              Title.Font.Style = [fsBold]
              Width = 18
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Italico'
              Title.Font.Charset = DEFAULT_CHARSET
              Title.Font.Color = clWindowText
              Title.Font.Height = -11
              Title.Font.Name = 'MS Sans Serif'
              Title.Font.Style = [fsItalic]
              Width = 18
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Padrao'
              Title.Caption = 'Padr'#227'o'
              Width = 60
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Substituir'
              Title.Caption = 'XX'
              Width = 18
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Substituto'
              Width = 60
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Prefixo'
              Width = 60
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Sufixo'
              Width = 60
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Formato'
              Width = 100
              Visible = True
            end>
        end
      end
      object TabSheet4: TTabSheet
        Caption = 'Sevi'#231'os'
        ImageIndex = 3
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object GradeS: TDBGrid
          Left = 0
          Top = 0
          Width = 657
          Height = 165
          Align = alClient
          DataSource = DsS
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          OnDrawColumnCell = GradeSDrawColumnCell
          Columns = <
            item
              Expanded = False
              FieldName = 'Descricao'
              Title.Caption = 'Descri'#231#227'o'
              Width = 119
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Linha'
              Width = 40
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Coluna'
              Width = 40
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Letras'
              Width = 40
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'FoTam'
              Title.Caption = 'Fonte'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Negrito'
              Title.Caption = 'N'
              Title.Font.Charset = DEFAULT_CHARSET
              Title.Font.Color = clWindowText
              Title.Font.Height = -11
              Title.Font.Name = 'MS Sans Serif'
              Title.Font.Style = [fsBold]
              Width = 18
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Italico'
              Title.Font.Charset = DEFAULT_CHARSET
              Title.Font.Color = clWindowText
              Title.Font.Height = -11
              Title.Font.Name = 'MS Sans Serif'
              Title.Font.Style = [fsItalic]
              Width = 18
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Padrao'
              Title.Caption = 'Padr'#227'o'
              Width = 60
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Substituir'
              Title.Caption = 'XX'
              Width = 18
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Substituto'
              Width = 60
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Prefixo'
              Width = 60
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Sufixo'
              Width = 60
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Formato'
              Width = 100
              Visible = True
            end>
        end
      end
      object TabSheet5: TTabSheet
        Caption = 'Faturamento'
        ImageIndex = 4
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object GradeF: TDBGrid
          Left = 0
          Top = 0
          Width = 657
          Height = 165
          Align = alClient
          DataSource = DsF
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          OnDrawColumnCell = GradeFDrawColumnCell
          Columns = <
            item
              Expanded = False
              FieldName = 'Descricao'
              Title.Caption = 'Descri'#231#227'o'
              Width = 119
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Linha'
              Width = 40
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Coluna'
              Width = 40
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Letras'
              Width = 40
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'FoTam'
              Title.Caption = 'Fonte'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Negrito'
              Title.Caption = 'N'
              Title.Font.Charset = DEFAULT_CHARSET
              Title.Font.Color = clWindowText
              Title.Font.Height = -11
              Title.Font.Name = 'MS Sans Serif'
              Title.Font.Style = [fsBold]
              Width = 18
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Italico'
              Title.Font.Charset = DEFAULT_CHARSET
              Title.Font.Color = clWindowText
              Title.Font.Height = -11
              Title.Font.Name = 'MS Sans Serif'
              Title.Font.Style = [fsItalic]
              Width = 18
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Padrao'
              Title.Caption = 'Padr'#227'o'
              Width = 60
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Substituir'
              Title.Caption = 'XX'
              Width = 18
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Substituto'
              Width = 60
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Prefixo'
              Width = 60
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Sufixo'
              Width = 60
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Formato'
              Width = 100
              Visible = True
            end>
        end
      end
      object TabSheet6: TTabSheet
        Caption = 'Transportadora'
        ImageIndex = 5
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object GradeT: TDBGrid
          Left = 0
          Top = 0
          Width = 657
          Height = 165
          Align = alClient
          DataSource = DsT
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          OnDrawColumnCell = GradeTDrawColumnCell
          Columns = <
            item
              Expanded = False
              FieldName = 'Descricao'
              Title.Caption = 'Descri'#231#227'o'
              Width = 119
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Linha'
              Width = 40
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Coluna'
              Width = 40
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Letras'
              Width = 40
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'FoTam'
              Title.Caption = 'Fonte'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Negrito'
              Title.Caption = 'N'
              Title.Font.Charset = DEFAULT_CHARSET
              Title.Font.Color = clWindowText
              Title.Font.Height = -11
              Title.Font.Name = 'MS Sans Serif'
              Title.Font.Style = [fsBold]
              Width = 18
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Italico'
              Title.Font.Charset = DEFAULT_CHARSET
              Title.Font.Color = clWindowText
              Title.Font.Height = -11
              Title.Font.Name = 'MS Sans Serif'
              Title.Font.Style = [fsItalic]
              Width = 18
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Padrao'
              Title.Caption = 'Padr'#227'o'
              Width = 60
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Substituir'
              Title.Caption = 'XX'
              Width = 18
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Substituto'
              Width = 60
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Prefixo'
              Width = 60
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Sufixo'
              Width = 60
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Formato'
              Width = 100
              Visible = True
            end>
        end
      end
    end
    object GBCntrl: TGroupBox
      Left = 0
      Top = 458
      Width = 784
      Height = 64
      Align = alBottom
      TabOrder = 2
      ExplicitTop = 161
      object Panel5: TPanel
        Left = 2
        Top = 15
        Width = 172
        Height = 47
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 174
        Top = 15
        Width = 30
        Height = 17
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel6: TPanel
        Left = 261
        Top = 15
        Width = 521
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object BtExclui: TBitBtn
          Tag = 12
          Left = 188
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Exclui'
          Enabled = False
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = BtExcluiClick
        end
        object BtAltera: TBitBtn
          Tag = 11
          Left = 96
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Altera'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtAlteraClick
        end
        object BtInclui: TBitBtn
          Tag = 10
          Left = 4
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Inclui'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtIncluiClick
        end
        object Panel7: TPanel
          Left = 412
          Top = 0
          Width = 109
          Height = 47
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 3
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 90
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 784
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 3
    object GB_R: TGroupBox
      Left = 736
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 688
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 421
        Height = 32
        Caption = 'Cadastro de Impress'#245'es Matriciais'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 421
        Height = 32
        Caption = 'Cadastro de Impress'#245'es Matriciais'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 421
        Height = 32
        Caption = 'Cadastro de Impress'#245'es Matriciais'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 48
    Width = 784
    Height = 44
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 4
    ExplicitTop = 378
    object Panel2: TPanel
      Left = 2
      Top = 15
      Width = 780
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object DsImpDOS: TDataSource
    DataSet = QrImpDOS
    Left = 268
    Top = 265
  end
  object QrImpDOS: TmySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrImpDOSBeforeOpen
    AfterOpen = QrImpDOSAfterOpen
    AfterClose = QrImpDOSAfterClose
    AfterScroll = QrImpDOSAfterScroll
    SQL.Strings = (
      'SELECT *'
      'FROM impdos'
      'WHERE Codigo > 0')
    Left = 240
    Top = 265
    object QrImpDOSCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrImpDOSDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 50
    end
    object QrImpDOSLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrImpDOSDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrImpDOSDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrImpDOSUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrImpDOSUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrImpDOSLProd: TIntegerField
      FieldName = 'LProd'
    end
    object QrImpDOSLServ: TIntegerField
      FieldName = 'LServ'
    end
    object QrImpDOSLFatu: TIntegerField
      FieldName = 'LFatu'
    end
    object QrImpDOSCFatu: TIntegerField
      FieldName = 'CFatu'
    end
    object QrImpDOSEFatu: TIntegerField
      FieldName = 'EFatu'
    end
  end
  object QrImpDOSImp: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrImpDOSImpCalcFields
    SQL.Strings = (
      'SELECT pit.Tabela, pit.Descricao, '
      'pit.Codigo CODIGOITEM, pip.*'
      'FROM impdosits pit'
      
        'LEFT JOIN impdosimp pip ON pip.Item=pit.Codigo AND pip.Codigo=:P' +
        '0')
    Left = 240
    Top = 293
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrImpDOSImpTabela: TIntegerField
      DisplayWidth = 12
      FieldName = 'Tabela'
      Required = True
    end
    object QrImpDOSImpDescricao: TWideStringField
      DisplayWidth = 30
      FieldName = 'Descricao'
      Required = True
      Size = 255
    end
    object QrImpDOSImpCodigo: TIntegerField
      DisplayWidth = 12
      FieldName = 'Codigo'
    end
    object QrImpDOSImpItem: TIntegerField
      DisplayWidth = 12
      FieldName = 'Item'
    end
    object QrImpDOSImpLinha: TSmallintField
      DisplayWidth = 12
      FieldName = 'Linha'
    end
    object QrImpDOSImpColuna: TSmallintField
      DisplayWidth = 12
      FieldName = 'Coluna'
    end
    object QrImpDOSImpLetras: TSmallintField
      DisplayWidth = 12
      FieldName = 'Letras'
    end
    object QrImpDOSImpFoTam: TSmallintField
      DisplayWidth = 12
      FieldName = 'FoTam'
    end
    object QrImpDOSImpNegrito: TSmallintField
      DisplayWidth = 12
      FieldName = 'Negrito'
    end
    object QrImpDOSImpItalico: TSmallintField
      DisplayWidth = 12
      FieldName = 'Italico'
    end
    object QrImpDOSImpPrefixo: TWideStringField
      DisplayWidth = 120
      FieldName = 'Prefixo'
      Size = 100
    end
    object QrImpDOSImpSufixo: TWideStringField
      DisplayWidth = 120
      FieldName = 'Sufixo'
      Size = 100
    end
    object QrImpDOSImpPadrao: TWideStringField
      DisplayWidth = 120
      FieldName = 'Padrao'
      Size = 100
    end
    object QrImpDOSImpSubstituto: TWideStringField
      DisplayWidth = 120
      FieldName = 'Substituto'
      Size = 100
    end
    object QrImpDOSImpSubstituir: TSmallintField
      DisplayWidth = 12
      FieldName = 'Substituir'
    end
    object QrImpDOSImpNOMETABELA: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMETABELA'
      Size = 100
      Calculated = True
    end
    object QrImpDOSImpCODIGOITEM: TIntegerField
      FieldName = 'CODIGOITEM'
      Required = True
    end
    object QrImpDOSImpFormato: TWideStringField
      FieldName = 'Formato'
      Size = 100
    end
    object QrImpDOSImpAlinhamento: TSmallintField
      FieldName = 'Alinhamento'
    end
  end
  object DsImpDOSImp: TDataSource
    DataSet = QrImpDOSImp
    Left = 268
    Top = 293
  end
  object QrLoc: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT pip.*'
      'FROM impdosits pit'
      
        'LEFT JOIN impdosimp pip ON pip.Item=pit.Codigo AND pip.Codigo=:P' +
        '0'
      'WHERE pit.Codigo=:P1'
      'AND pip.Codigo>0')
    Left = 224
    Top = 184
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrLocCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrLocItem: TIntegerField
      FieldName = 'Item'
    end
    object QrLocLinha: TSmallintField
      FieldName = 'Linha'
    end
    object QrLocColuna: TSmallintField
      FieldName = 'Coluna'
    end
    object QrLocLetras: TSmallintField
      FieldName = 'Letras'
    end
    object QrLocFoTam: TSmallintField
      FieldName = 'FoTam'
    end
    object QrLocNegrito: TSmallintField
      FieldName = 'Negrito'
    end
    object QrLocItalico: TSmallintField
      FieldName = 'Italico'
    end
    object QrLocPrefixo: TWideStringField
      FieldName = 'Prefixo'
      Size = 100
    end
    object QrLocSufixo: TWideStringField
      FieldName = 'Sufixo'
      Size = 100
    end
    object QrLocPadrao: TWideStringField
      FieldName = 'Padrao'
      Size = 100
    end
    object QrLocSubstituto: TWideStringField
      FieldName = 'Substituto'
      Size = 100
    end
    object QrLocSubstituir: TSmallintField
      FieldName = 'Substituir'
    end
  end
  object PMAltera: TPopupMenu
    Left = 408
    Top = 192
    object AlteraaImpresso1: TMenuItem
      Caption = 'Altera a Im&press'#227'o'
      OnClick = AlteraaImpresso1Click
    end
    object AlteraItem1: TMenuItem
      Caption = 'Altera &Item'
      object Selecionado1: TMenuItem
        Caption = 'Se&lecionado'
        OnClick = Selecionado1Click
      end
      object Seqncia1: TMenuItem
        Caption = 'Se&q'#252#234'ncia'
        OnClick = Seqncia1Click
      end
    end
  end
  object QrA: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT pit.Tabela, pit.Descricao, '
      'pit.Codigo CODIGOITEM, pip.*'
      'FROM impdosits pit'
      
        'LEFT JOIN impdosimp pip ON pip.Item=pit.Codigo AND pip.Codigo=:P' +
        '0'
      'WHERE Tabela=1'
      'ORDER BY Linha, Coluna'
      '')
    Left = 428
    Top = 277
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrATabela: TIntegerField
      FieldName = 'Tabela'
      Required = True
    end
    object QrADescricao: TWideStringField
      FieldName = 'Descricao'
      Required = True
      Size = 255
    end
    object QrACODIGOITEM: TIntegerField
      FieldName = 'CODIGOITEM'
      Required = True
    end
    object QrACodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrAItem: TIntegerField
      FieldName = 'Item'
    end
    object QrALinha: TIntegerField
      FieldName = 'Linha'
    end
    object QrAColuna: TIntegerField
      FieldName = 'Coluna'
    end
    object QrALetras: TIntegerField
      FieldName = 'Letras'
    end
    object QrAFoTam: TSmallintField
      FieldName = 'FoTam'
    end
    object QrANegrito: TSmallintField
      FieldName = 'Negrito'
    end
    object QrAItalico: TSmallintField
      FieldName = 'Italico'
    end
    object QrAPrefixo: TWideStringField
      FieldName = 'Prefixo'
      Size = 100
    end
    object QrASufixo: TWideStringField
      FieldName = 'Sufixo'
      Size = 100
    end
    object QrAPadrao: TWideStringField
      FieldName = 'Padrao'
      Size = 100
    end
    object QrASubstituto: TWideStringField
      FieldName = 'Substituto'
      Size = 100
    end
    object QrASubstituir: TSmallintField
      FieldName = 'Substituir'
    end
    object QrAFormato: TWideStringField
      FieldName = 'Formato'
      Size = 100
    end
    object QrAAlinhamento: TSmallintField
      FieldName = 'Alinhamento'
    end
  end
  object DsA: TDataSource
    DataSet = QrA
    Left = 456
    Top = 277
  end
  object QrE: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT pit.Tabela, pit.Descricao, '
      'pit.Codigo CODIGOITEM, pip.*'
      'FROM impdosits pit'
      
        'LEFT JOIN impdosimp pip ON pip.Item=pit.Codigo AND pip.Codigo=:P' +
        '0'
      'WHERE Tabela=2'
      'ORDER BY Linha, Coluna'
      '')
    Left = 428
    Top = 305
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrETabela: TIntegerField
      FieldName = 'Tabela'
      Required = True
    end
    object QrEDescricao: TWideStringField
      FieldName = 'Descricao'
      Required = True
      Size = 255
    end
    object QrECODIGOITEM: TIntegerField
      FieldName = 'CODIGOITEM'
      Required = True
    end
    object QrECodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEItem: TIntegerField
      FieldName = 'Item'
    end
    object QrELinha: TIntegerField
      FieldName = 'Linha'
    end
    object QrEColuna: TIntegerField
      FieldName = 'Coluna'
    end
    object QrELetras: TIntegerField
      FieldName = 'Letras'
    end
    object QrEFoTam: TSmallintField
      FieldName = 'FoTam'
    end
    object QrENegrito: TSmallintField
      FieldName = 'Negrito'
    end
    object QrEItalico: TSmallintField
      FieldName = 'Italico'
    end
    object QrEPrefixo: TWideStringField
      FieldName = 'Prefixo'
      Size = 100
    end
    object QrESufixo: TWideStringField
      FieldName = 'Sufixo'
      Size = 100
    end
    object QrEPadrao: TWideStringField
      FieldName = 'Padrao'
      Size = 100
    end
    object QrESubstituto: TWideStringField
      FieldName = 'Substituto'
      Size = 100
    end
    object QrESubstituir: TSmallintField
      FieldName = 'Substituir'
    end
    object QrEFormato: TWideStringField
      FieldName = 'Formato'
      Size = 100
    end
    object QrEAlinhamento: TSmallintField
      FieldName = 'Alinhamento'
    end
  end
  object DsE: TDataSource
    DataSet = QrE
    Left = 456
    Top = 305
  end
  object QrP: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT pit.Tabela, pit.Descricao, '
      'pit.Codigo CODIGOITEM, pip.*'
      'FROM impdosits pit'
      
        'LEFT JOIN impdosimp pip ON pip.Item=pit.Codigo AND pip.Codigo=:P' +
        '0'
      'WHERE Tabela=3'
      'ORDER BY Linha, Coluna'
      '')
    Left = 428
    Top = 333
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPTabela: TIntegerField
      FieldName = 'Tabela'
      Required = True
    end
    object QrPDescricao: TWideStringField
      FieldName = 'Descricao'
      Required = True
      Size = 255
    end
    object QrPCODIGOITEM: TIntegerField
      FieldName = 'CODIGOITEM'
      Required = True
    end
    object QrPCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPItem: TIntegerField
      FieldName = 'Item'
    end
    object QrPLinha: TIntegerField
      FieldName = 'Linha'
    end
    object QrPColuna: TIntegerField
      FieldName = 'Coluna'
    end
    object QrPLetras: TIntegerField
      FieldName = 'Letras'
    end
    object QrPFoTam: TSmallintField
      FieldName = 'FoTam'
    end
    object QrPNegrito: TSmallintField
      FieldName = 'Negrito'
    end
    object QrPItalico: TSmallintField
      FieldName = 'Italico'
    end
    object QrPPrefixo: TWideStringField
      FieldName = 'Prefixo'
      Size = 100
    end
    object QrPSufixo: TWideStringField
      FieldName = 'Sufixo'
      Size = 100
    end
    object QrPPadrao: TWideStringField
      FieldName = 'Padrao'
      Size = 100
    end
    object QrPSubstituto: TWideStringField
      FieldName = 'Substituto'
      Size = 100
    end
    object QrPSubstituir: TSmallintField
      FieldName = 'Substituir'
    end
    object QrPFormato: TWideStringField
      FieldName = 'Formato'
      Size = 100
    end
    object QrPAlinhamento: TSmallintField
      FieldName = 'Alinhamento'
    end
  end
  object DsP: TDataSource
    DataSet = QrP
    Left = 456
    Top = 333
  end
  object QrS: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT pit.Tabela, pit.Descricao, '
      'pit.Codigo CODIGOITEM, pip.*'
      'FROM impdosits pit'
      
        'LEFT JOIN impdosimp pip ON pip.Item=pit.Codigo AND pip.Codigo=:P' +
        '0'
      'WHERE Tabela=4'
      'ORDER BY Linha, Coluna')
    Left = 484
    Top = 277
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSTabela: TIntegerField
      FieldName = 'Tabela'
      Required = True
    end
    object QrSDescricao: TWideStringField
      FieldName = 'Descricao'
      Required = True
      Size = 255
    end
    object QrSCODIGOITEM: TIntegerField
      FieldName = 'CODIGOITEM'
      Required = True
    end
    object QrSCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrSItem: TIntegerField
      FieldName = 'Item'
    end
    object QrSLinha: TIntegerField
      FieldName = 'Linha'
    end
    object QrSColuna: TIntegerField
      FieldName = 'Coluna'
    end
    object QrSLetras: TIntegerField
      FieldName = 'Letras'
    end
    object QrSFoTam: TSmallintField
      FieldName = 'FoTam'
    end
    object QrSNegrito: TSmallintField
      FieldName = 'Negrito'
    end
    object QrSItalico: TSmallintField
      FieldName = 'Italico'
    end
    object QrSPrefixo: TWideStringField
      FieldName = 'Prefixo'
      Size = 100
    end
    object QrSSufixo: TWideStringField
      FieldName = 'Sufixo'
      Size = 100
    end
    object QrSPadrao: TWideStringField
      FieldName = 'Padrao'
      Size = 100
    end
    object QrSSubstituto: TWideStringField
      FieldName = 'Substituto'
      Size = 100
    end
    object QrSSubstituir: TSmallintField
      FieldName = 'Substituir'
    end
    object QrSFormato: TWideStringField
      FieldName = 'Formato'
      Size = 100
    end
    object QrSAlinhamento: TSmallintField
      FieldName = 'Alinhamento'
    end
  end
  object DsS: TDataSource
    DataSet = QrS
    Left = 512
    Top = 277
  end
  object QrF: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT pit.Tabela, pit.Descricao, '
      'pit.Codigo CODIGOITEM, pip.*'
      'FROM impdosits pit'
      
        'LEFT JOIN impdosimp pip ON pip.Item=pit.Codigo AND pip.Codigo=:P' +
        '0'
      'WHERE Tabela=5'
      'ORDER BY Linha, Coluna'
      '')
    Left = 484
    Top = 305
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrFTabela: TIntegerField
      FieldName = 'Tabela'
      Required = True
    end
    object QrFDescricao: TWideStringField
      FieldName = 'Descricao'
      Required = True
      Size = 255
    end
    object QrFCODIGOITEM: TIntegerField
      FieldName = 'CODIGOITEM'
      Required = True
    end
    object QrFCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrFItem: TIntegerField
      FieldName = 'Item'
    end
    object QrFLinha: TIntegerField
      FieldName = 'Linha'
    end
    object QrFColuna: TIntegerField
      FieldName = 'Coluna'
    end
    object QrFLetras: TIntegerField
      FieldName = 'Letras'
    end
    object QrFFoTam: TSmallintField
      FieldName = 'FoTam'
    end
    object QrFNegrito: TSmallintField
      FieldName = 'Negrito'
    end
    object QrFItalico: TSmallintField
      FieldName = 'Italico'
    end
    object QrFPrefixo: TWideStringField
      FieldName = 'Prefixo'
      Size = 100
    end
    object QrFSufixo: TWideStringField
      FieldName = 'Sufixo'
      Size = 100
    end
    object QrFPadrao: TWideStringField
      FieldName = 'Padrao'
      Size = 100
    end
    object QrFSubstituto: TWideStringField
      FieldName = 'Substituto'
      Size = 100
    end
    object QrFSubstituir: TSmallintField
      FieldName = 'Substituir'
    end
    object QrFFormato: TWideStringField
      FieldName = 'Formato'
      Size = 100
    end
    object QrFAlinhamento: TSmallintField
      FieldName = 'Alinhamento'
    end
  end
  object DsF: TDataSource
    DataSet = QrF
    Left = 512
    Top = 305
  end
  object QrT: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT pit.Tabela, pit.Descricao, '
      'pit.Codigo CODIGOITEM, pip.*'
      'FROM impdosits pit'
      
        'LEFT JOIN impdosimp pip ON pip.Item=pit.Codigo AND pip.Codigo=:P' +
        '0'
      'WHERE Tabela=6'
      'ORDER BY Linha, Coluna'
      '')
    Left = 484
    Top = 333
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrTTabela: TIntegerField
      FieldName = 'Tabela'
      Required = True
    end
    object QrTDescricao: TWideStringField
      FieldName = 'Descricao'
      Required = True
      Size = 255
    end
    object QrTCODIGOITEM: TIntegerField
      FieldName = 'CODIGOITEM'
      Required = True
    end
    object QrTCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrTItem: TIntegerField
      FieldName = 'Item'
    end
    object QrTLinha: TIntegerField
      FieldName = 'Linha'
    end
    object QrTColuna: TIntegerField
      FieldName = 'Coluna'
    end
    object QrTLetras: TIntegerField
      FieldName = 'Letras'
    end
    object QrTFoTam: TSmallintField
      FieldName = 'FoTam'
    end
    object QrTNegrito: TSmallintField
      FieldName = 'Negrito'
    end
    object QrTItalico: TSmallintField
      FieldName = 'Italico'
    end
    object QrTPrefixo: TWideStringField
      FieldName = 'Prefixo'
      Size = 100
    end
    object QrTSufixo: TWideStringField
      FieldName = 'Sufixo'
      Size = 100
    end
    object QrTPadrao: TWideStringField
      FieldName = 'Padrao'
      Size = 100
    end
    object QrTSubstituto: TWideStringField
      FieldName = 'Substituto'
      Size = 100
    end
    object QrTSubstituir: TSmallintField
      FieldName = 'Substituir'
    end
    object QrTFormato: TWideStringField
      FieldName = 'Formato'
      Size = 100
    end
    object QrTAlinhamento: TSmallintField
      FieldName = 'Alinhamento'
    end
  end
  object DsT: TDataSource
    DataSet = QrT
    Left = 512
    Top = 333
  end
end
