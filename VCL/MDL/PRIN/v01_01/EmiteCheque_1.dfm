object FmEmiteCheque_1: TFmEmiteCheque_1
  Left = 367
  Top = 177
  Caption = 'CHS-PRINT-001 :: Emite Cheque Pertochek'
  ClientHeight = 506
  ClientWidth = 778
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelDados: TPanel
    Left = 0
    Top = 48
    Width = 778
    Height = 344
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    ExplicitLeft = 100
    ExplicitHeight = 363
    object Label10: TLabel
      Left = 16
      Top = 300
      Width = 55
      Height = 13
      Caption = 'Mensagem:'
    end
    object Edit1: TEdit
      Left = 348
      Top = 12
      Width = 61
      Height = 21
      TabOrder = 1
      Text = '.C409288263000B5893809900A46300993'
      OnChange = Edit1Change
    end
    object Edit2: TEdit
      Left = 436
      Top = 12
      Width = 129
      Height = 21
      TabOrder = 2
      Text = 'Edit2'
      OnChange = Edit2Change
    end
    object Panel1: TPanel
      Left = 0
      Top = 200
      Width = 778
      Height = 92
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      Visible = False
      ExplicitLeft = 109
      ExplicitTop = 193
      ExplicitWidth = 776
      object Label3: TLabel
        Left = 14
        Top = 48
        Width = 34
        Height = 13
        Caption = 'Banco:'
      end
      object Label4: TLabel
        Left = 98
        Top = 48
        Width = 42
        Height = 13
        Caption = 'Ag'#234'ncia:'
      end
      object Label6: TLabel
        Left = 204
        Top = 48
        Width = 31
        Height = 13
        Caption = 'Conta:'
      end
      object Label7: TLabel
        Left = 434
        Top = 48
        Width = 40
        Height = 13
        Caption = 'Cheque:'
      end
      object Label8: TLabel
        Left = 580
        Top = 48
        Width = 33
        Height = 13
        Caption = 'Comp.:'
      end
      object Label9: TLabel
        Left = 16
        Top = 0
        Width = 35
        Height = 13
        Caption = 'CMC-7:'
      end
      object ST_Banco: TStaticText
        Left = 14
        Top = 64
        Width = 67
        Height = 30
        Caption = '399'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -28
        Font.Name = 'CMC7'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
      end
      object ST_Agencia: TStaticText
        Left = 98
        Top = 64
        Width = 88
        Height = 30
        Caption = '0036'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -28
        Font.Name = 'CMC7'
        Font.Style = []
        ParentFont = False
        TabOrder = 1
      end
      object ST_Conta: TStaticText
        Left = 204
        Top = 64
        Width = 214
        Height = 30
        Caption = '0036288290'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -28
        Font.Name = 'CMC7'
        Font.Style = []
        ParentFont = False
        TabOrder = 2
      end
      object ST_CMC7: TStaticText
        Left = 14
        Top = 12
        Width = 634
        Height = 30
        Caption = '399 0036 0036288290 908398 009'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -28
        Font.Name = 'CMC7'
        Font.Style = []
        ParentFont = False
        TabOrder = 3
      end
      object ST_Cheque: TStaticText
        Left = 434
        Top = 64
        Width = 130
        Height = 30
        Caption = '908398'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -28
        Font.Name = 'CMC7'
        Font.Style = []
        ParentFont = False
        TabOrder = 4
      end
      object ST_Praca: TStaticText
        Left = 580
        Top = 64
        Width = 67
        Height = 30
        Caption = '009'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -28
        Font.Name = 'CMC7'
        Font.Style = []
        ParentFont = False
        TabOrder = 5
      end
    end
    object Panel2: TPanel
      Left = 0
      Top = 0
      Width = 778
      Height = 200
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 3
      ExplicitLeft = 1
      ExplicitTop = 1
      ExplicitWidth = 776
      object Label1: TLabel
        Left = 12
        Top = 4
        Width = 26
        Height = 13
        Caption = 'Data:'
      end
      object LaValor: TLabel
        Left = 112
        Top = 4
        Width = 27
        Height = 13
        Caption = 'Valor:'
      end
      object Label5: TLabel
        Left = 208
        Top = 4
        Width = 58
        Height = 13
        Caption = 'Benefici'#225'rio:'
      end
      object Label2: TLabel
        Left = 572
        Top = 4
        Width = 36
        Height = 13
        Caption = 'Cidade:'
      end
      object Label11: TLabel
        Left = 728
        Top = 4
        Width = 34
        Height = 13
        Caption = 'Banco:'
      end
      object TPData: TDateTimePicker
        Left = 12
        Top = 20
        Width = 97
        Height = 21
        Date = 38741.724412615700000000
        Time = 38741.724412615700000000
        TabOrder = 0
      end
      object EdValor: TdmkEdit
        Left = 112
        Top = 20
        Width = 93
        Height = 21
        Alignment = taRightJustify
        TabOrder = 1
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdBenef: TdmkEdit
        Left = 208
        Top = 20
        Width = 357
        Height = 21
        CharCase = ecUpperCase
        TabOrder = 2
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdCidade: TdmkEdit
        Left = 572
        Top = 20
        Width = 153
        Height = 21
        CharCase = ecUpperCase
        TabOrder = 3
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object RGForma: TRadioGroup
        Left = 12
        Top = 44
        Width = 757
        Height = 153
        Caption = ' Forma de preenchimento: '
        Columns = 2
        ItemIndex = 2
        Items.Strings = (
          '0 - Somente preenchimento'
          '1 - Preenchimento e chancela'
          '2 - Preenchimento e leitura de caracteres magnetiz'#225'veis CMC7'
          '3 - Preenchimento, chancela e leitura'
          '4 - Preenchimento com ano de 4 d'#237'gitos'
          '5 - Preenchimento, chancela e ano com 4 d'#237'gitos'
          '6 - Preenchimento, leitura e ano com 4 d'#237'gitos'
          '7 - Preenchimento, chancela, leitura e ano com 4 d'#237'gitos'
          '8 - Preenchimento e cruzamento'
          '9 - Preenchimento, cruzamento e chancela'
          'A - Preenchimento, cruzamento e leitura'
          'B - Preenchimento, cruzamento, chancela e leitura'
          'C - Preenchimento, cruzamento e ano com 4 d'#237'gitos'
          'D - Preenchimento, cruzamento, chancela e ano com 4 d'#237'gitos'
          'E - Preenchimento, cruzamento, leitura e ano com 4 d'#237'gitos'
          
            'F - Preenchimento, cruzamento, chancela, leitura e ano com 4 d'#237'g' +
            'itos')
        TabOrder = 4
      end
      object EdBanco: TdmkEdit
        Left = 728
        Top = 20
        Width = 40
        Height = 21
        Alignment = taRightJustify
        CharCase = ecUpperCase
        TabOrder = 5
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 3
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '000'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
    end
    object EdMens: TdmkEdit
      Left = 16
      Top = 316
      Width = 757
      Height = 21
      CharCase = ecUpperCase
      TabOrder = 4
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      ValWarn = False
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 778
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    ExplicitWidth = 635
    object GB_R: TGroupBox
      Left = 730
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      ExplicitLeft = 587
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 682
      Height = 48
      Align = alClient
      TabOrder = 2
      ExplicitWidth = 539
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 302
        Height = 32
        Caption = 'Emite Cheque Pertochek'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 302
        Height = 32
        Caption = 'Emite Cheque Pertochek'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 302
        Height = 32
        Caption = 'Emite Cheque Pertochek'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 392
    Width = 778
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    ExplicitTop = 186
    ExplicitWidth = 635
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 774
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 631
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 436
    Width = 778
    Height = 70
    Align = alBottom
    TabOrder = 3
    ExplicitTop = 230
    ExplicitWidth = 635
    object PnSaiDesis: TPanel
      Left = 632
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      ExplicitLeft = 489
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel3: TPanel
      Left = 2
      Top = 15
      Width = 630
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 487
      object LaSerie2: TLabel
        Left = 316
        Top = 6
        Width = 102
        Height = 37
        Alignment = taRightJustify
        AutoSize = False
        Caption = '???'
        Font.Charset = ANSI_CHARSET
        Font.Color = 16764159
        Font.Height = -32
        Font.Name = 'Arial Rounded MT Bold'
        Font.Style = []
        ParentFont = False
      end
      object LaCheque2: TLabel
        Left = 428
        Top = 6
        Width = 145
        Height = 37
        AutoSize = False
        Caption = '000000'
        Font.Charset = ANSI_CHARSET
        Font.Color = 16764159
        Font.Height = -32
        Font.Name = 'Arial Rounded MT Bold'
        Font.Style = []
        ParentFont = False
      end
      object LaSerie1: TLabel
        Left = 315
        Top = 5
        Width = 102
        Height = 37
        Alignment = taRightJustify
        AutoSize = False
        Caption = '???'
        Font.Charset = ANSI_CHARSET
        Font.Color = clPurple
        Font.Height = -32
        Font.Name = 'Arial Rounded MT Bold'
        Font.Style = []
        ParentFont = False
      end
      object LaCheque1: TLabel
        Left = 427
        Top = 5
        Width = 145
        Height = 37
        AutoSize = False
        Caption = '000000'
        Font.Charset = ANSI_CHARSET
        Font.Color = clPurple
        Font.Height = -32
        Font.Name = 'Arial Rounded MT Bold'
        Font.Style = []
        ParentFont = False
      end
      object BtOK: TBitBtn
        Tag = 14
        Left = 20
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Emite'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
      object BtInfo: TBitBtn
        Tag = 118
        Left = 180
        Top = 4
        Width = 120
        Height = 40
        Caption = '&L'#234
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtInfoClick
      end
    end
  end
end
