object FmImprimeBand: TFmImprimeBand
  Left = 337
  Top = 177
  Caption = 'Edi'#231#227'o de Bandas de Impress'#227'o'
  ClientHeight = 495
  ClientWidth = 587
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelDados: TPanel
    Left = 0
    Top = 48
    Width = 587
    Height = 333
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object Label2: TLabel
      Left = 12
      Top = 8
      Width = 51
      Height = 13
      Caption = 'Descri'#231#227'o:'
    end
    object Label1: TLabel
      Left = 418
      Top = 8
      Width = 28
      Height = 13
      Caption = 'Topo:'
    end
    object Label3: TLabel
      Left = 498
      Top = 8
      Width = 30
      Height = 13
      Caption = 'Altura:'
    end
    object Label4: TLabel
      Left = 368
      Top = 292
      Width = 41
      Height = 13
      Caption = 'Colunas:'
    end
    object Label5: TLabel
      Left = 420
      Top = 292
      Width = 48
      Height = 13
      Caption = 'Larg. Col.:'
    end
    object Label6: TLabel
      Left = 472
      Top = 292
      Width = 50
      Height = 13
      Caption = 'Espa'#231'am.:'
    end
    object Label7: TLabel
      Left = 524
      Top = 292
      Width = 34
      Height = 13
      Caption = 'Linhas:'
    end
    object EdNome: TEdit
      Left = 12
      Top = 24
      Width = 405
      Height = 21
      Color = clWhite
      MaxLength = 100
      TabOrder = 0
    end
    object EdTopo: TdmkEdit
      Left = 420
      Top = 24
      Width = 77
      Height = 21
      Alignment = taRightJustify
      TabOrder = 1
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object EdAltura: TdmkEdit
      Left = 500
      Top = 24
      Width = 77
      Height = 21
      Alignment = taRightJustify
      TabOrder = 2
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object RGTipo: TRadioGroup
      Left = 12
      Top = 52
      Width = 353
      Height = 277
      Caption = ' Tipo de banda: '
      Columns = 2
      ItemIndex = 0
      Items.Strings = (
        'T'#237'tulo do relat'#243'rio'
        'Sum'#225'rio do relat'#243'rio'
        'Cabe'#231'alho de p'#225'gina'
        'Rodap'#233' de p'#225'gina'
        'Cabe'#231'alho master'
        'Dados master'
        'Rodap'#233' master'
        'Cabe'#231'alho de detalhes'
        'Dados de detalhes'
        'Rodap'#233' de detalhes'
        'Cabe'#231'alho de sub-detalhes'
        'Dados de sub-detalhes'
        'Rodap'#233' de sub-detalhes'
        'Revestimento'
        'Cabe'#231'alho de coluna'
        'Rodap'#233' de coluna'
        'Cabe'#231'alho de grupo'
        'Rodap'#233' de grupo'
        'Cabe'#231'alho de dados cruzados'
        'Dados dos dados cruzados'
        'Rodap'#233' de dados cruzados'
        'Filho')
      TabOrder = 3
    end
    object RGDataset: TRadioGroup
      Left = 368
      Top = 52
      Width = 209
      Height = 125
      Caption = ' Tabela relacionada: '
      ItemIndex = 0
      Items.Strings = (
        'Nenhuma tabela'
        'Dados de Produtos'
        'Dados de Servi'#231'os'
        'Dados de Faturamento')
      TabOrder = 4
    end
    object EdColQtd: TdmkEdit
      Left = 368
      Top = 308
      Width = 50
      Height = 21
      Alignment = taRightJustify
      TabOrder = 6
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '1'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 1
      ValWarn = False
    end
    object EdColLar: TdmkEdit
      Left = 420
      Top = 308
      Width = 50
      Height = 21
      Alignment = taRightJustify
      TabOrder = 7
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '2100'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 2100
      ValWarn = False
    end
    object EdColGAP: TdmkEdit
      Left = 472
      Top = 308
      Width = 50
      Height = 21
      Alignment = taRightJustify
      TabOrder = 8
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '10'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 10
      ValWarn = False
    end
    object EdLinhas: TdmkEdit
      Left = 524
      Top = 308
      Width = 50
      Height = 21
      Alignment = taRightJustify
      TabOrder = 9
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '1'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 1
      ValWarn = False
    end
    object RGMultipaginas: TRadioGroup
      Left = 368
      Top = 180
      Width = 209
      Height = 105
      Caption = ' Multi-p'#225'ginas: '
      ItemIndex = 0
      Items.Strings = (
        'Repetir dados'
        'Preencher anteriores com '#39'X'#39
        'N'#227'o se aplica')
      TabOrder = 5
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 587
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 539
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 491
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 389
        Height = 32
        Caption = 'Edi'#231#227'o de bandas de impress'#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 389
        Height = 32
        Caption = 'Edi'#231#227'o de bandas de impress'#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 389
        Height = 32
        Caption = 'Edi'#231#227'o de bandas de impress'#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 381
    Width = 587
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 583
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 425
    Width = 587
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 441
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 439
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
      end
    end
  end
end
