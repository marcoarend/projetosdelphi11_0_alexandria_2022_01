object FmDotPrint: TFmDotPrint
  Left = 349
  Top = 189
  Caption = 'IMP-MATRI-001 :: Impress'#227'o Matricial (Direta)'
  ClientHeight = 521
  ClientWidth = 792
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 792
    Height = 340
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    ExplicitLeft = 100
    ExplicitHeight = 377
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 792
      Height = 340
      Align = alClient
      BevelOuter = bvNone
      Caption = 'Panel3'
      TabOrder = 0
      ExplicitLeft = 1
      ExplicitTop = 1
      ExplicitWidth = 790
      ExplicitHeight = 187
      object RE1: TRichEdit
        Left = 0
        Top = 0
        Width = 704
        Height = 212
        Align = alClient
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Courier New'
        Font.Style = []
        HideScrollBars = False
        ParentFont = False
        ScrollBars = ssBoth
        TabOrder = 0
        WordWrap = False
        OnSelectionChange = RE1SelectionChange
      end
      object Memo1: TMemo
        Left = 704
        Top = 0
        Width = 88
        Height = 212
        Align = alRight
        TabOrder = 1
        Visible = False
        ExplicitLeft = 701
        ExplicitTop = 1
        ExplicitHeight = 185
      end
      object PainelMaster: TPanel
        Left = 0
        Top = 212
        Width = 792
        Height = 128
        Align = alBottom
        BevelOuter = bvNone
        TabOrder = 2
        ExplicitLeft = 108
        ExplicitTop = 256
        object Label4: TLabel
          Left = 4
          Top = 4
          Width = 43
          Height = 13
          Caption = 'C'#225'lculos:'
        end
        object Label5: TLabel
          Left = 92
          Top = 4
          Width = 22
          Height = 13
          Caption = 'Win:'
        end
        object Label6: TLabel
          Left = 180
          Top = 4
          Width = 21
          Height = 13
          Caption = 'Pas:'
        end
        object Label1: TLabel
          Left = 240
          Top = 76
          Width = 27
          Height = 13
          Caption = 'Fator:'
        end
        object Label2: TLabel
          Left = 370
          Top = 76
          Width = 26
          Height = 13
          Caption = 'Linha'
        end
        object Label3: TLabel
          Left = 404
          Top = 76
          Width = 48
          Height = 13
          Caption = 'Tamanho:'
        end
        object EdCalcs: TEdit
          Left = 4
          Top = 16
          Width = 85
          Height = 21
          TabOrder = 0
        end
        object EdWin: TEdit
          Left = 92
          Top = 16
          Width = 85
          Height = 21
          TabOrder = 1
        end
        object Button3: TButton
          Left = 264
          Top = 12
          Width = 75
          Height = 25
          Caption = 'Executa'
          TabOrder = 2
          OnClick = Button3Click
        end
        object Edit1: TEdit
          Left = 344
          Top = 12
          Width = 121
          Height = 21
          TabOrder = 3
          Text = 'Edit1'
        end
        object EdPas: TEdit
          Left = 180
          Top = 16
          Width = 85
          Height = 21
          TabOrder = 4
        end
        object RGCPI: TRadioGroup
          Left = 0
          Top = 69
          Width = 97
          Height = 47
          Caption = ' CPI: '
          Columns = 2
          ItemIndex = 0
          Items.Strings = (
            'Def'
            '10'
            '12')
          TabOrder = 5
        end
        object RGAltura: TRadioGroup
          Left = 100
          Top = 69
          Width = 137
          Height = 47
          Caption = ' Altura da linha: '
          Columns = 5
          ItemIndex = 2
          Items.Strings = (
            '0'
            '1'
            '2'
            '3'
            'A')
          TabOrder = 6
        end
        object EdFator: TEdit
          Left = 240
          Top = 92
          Width = 49
          Height = 21
          TabOrder = 7
          Text = '30'
        end
        object Button2: TButton
          Left = 293
          Top = 88
          Width = 75
          Height = 25
          Caption = 'AchaLin'
          TabOrder = 8
          OnClick = Button2Click
        end
        object CkInicio: TCheckBox
          Left = 291
          Top = 69
          Width = 62
          Height = 17
          Caption = 'In'#237'cio'
          TabOrder = 9
        end
        object EdLinha: TEdit
          Left = 371
          Top = 92
          Width = 30
          Height = 21
          TabOrder = 10
          Text = '1'
        end
        object EdTamanho: TEdit
          Left = 404
          Top = 92
          Width = 109
          Height = 21
          TabOrder = 11
        end
        object Button1: TButton
          Left = 456
          Top = 72
          Width = 57
          Height = 17
          Caption = 'Tamanho'
          TabOrder = 12
          OnClick = Button1Click
        end
        object CkEjeta: TCheckBox
          Left = 540
          Top = 76
          Width = 45
          Height = 13
          Caption = 'Ejeta'
          Checked = True
          State = cbChecked
          TabOrder = 13
        end
        object EdPartes: TEdit
          Left = 540
          Top = 92
          Width = 49
          Height = 21
          TabOrder = 14
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 792
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    ExplicitWidth = 784
    object GB_R: TGroupBox
      Left = 744
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      ExplicitLeft = 736
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 696
      Height = 48
      Align = alClient
      TabOrder = 2
      ExplicitWidth = 688
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 335
        Height = 32
        Caption = 'Impress'#227'o Matricial (Direta)'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 335
        Height = 32
        Caption = 'Impress'#227'o Matricial (Direta)'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 335
        Height = 32
        Caption = 'Impress'#227'o Matricial (Direta)'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 388
    Width = 792
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    ExplicitTop = 378
    ExplicitWidth = 784
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 788
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 780
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 432
    Width = 792
    Height = 70
    Align = alBottom
    TabOrder = 3
    ExplicitTop = 422
    ExplicitWidth = 784
    object PnSaiDesis: TPanel
      Left = 646
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      ExplicitLeft = 638
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel5: TPanel
      Left = 2
      Top = 15
      Width = 644
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 636
      object BtImprime: TBitBtn
        Left = 9
        Top = 4
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Hint = 'Sai da janela atual'
        Caption = '&Imprime'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtImprimeClick
      end
      object BtItens: TBitBtn
        Left = 336
        Top = 4
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Hint = 'Edita texto'
        Caption = '&Texto'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        OnClick = BtItensClick
      end
    end
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 502
    Width = 792
    Height = 19
    Panels = <
      item
        Text = ' Linha'
        Width = 50
      end
      item
        Width = 50
      end
      item
        Text = ' Coluna'
        Width = 50
      end
      item
        Width = 50
      end
      item
        Text = ' Posi'#231#227'o'
        Width = 50
      end
      item
        Width = 70
      end
      item
        Text = ' Fonte.Tamanho'
        Width = 90
      end
      item
        Width = 50
      end
      item
        Text = ' Carct.Linha '
        Width = 68
      end
      item
        Width = 50
      end
      item
        Text = ' Texto '
        Width = 40
      end
      item
        Width = 50
      end>
    ExplicitLeft = -8
    ExplicitTop = 484
  end
end
