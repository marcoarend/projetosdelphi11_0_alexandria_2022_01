unit BarComands;
interface

uses
  StdCtrls, ExtCtrls, Windows, Messages, SysUtils, Classes, Graphics, Controls,
  Forms, Dialogs, Menus, UnMsgInt, UnInternalConsts;

type
  TBarComandsType = (bctIni, bctFim);
  TPrnProgLang = (pplNone, pplArgoxA, pplArgoxB);
  TPrnMeasure = (pmNone, pmMetric, pmInches);
  TUBarComands = class(TObject)
  private
    { Private declarations }
  public
    { Public declarations }
    function ComandosAutomaticos(BarComandsType: TBarComandsType;
             PrnProgLang: TPrnProgLang; Measure: TPrnMeasure; Memo: TMemo): Boolean;
  end;

var
  UBarComands: TUBarComands;

implementation

uses UnMyObjects, Module, Bancos, Opcoes, MyDBCheck;

function TUBarComands.ComandosAutomaticos(BarComandsType: TBarComandsType;
  PrnProgLang: TPrnProgLang; Measure: TPrnMeasure; Memo: TMemo): Boolean;
begin
  case PrnProgLang of
    pplArgoxA:
    begin
      case BarComandsType of
        bctIni:
        begin
          case Measure of
            pmMetric:
            begin
              Memo.Lines.Add(Char(2) + 'm'); // p�g 23
              Memo.Lines.Add(Char(2) + 'f840'); // back feed (p�g. 18)
            end;
            pmInches:
            begin
              Memo.Lines.Add(Char(2) + 'n'); // p�g 23
              Memo.Lines.Add(Char(2) + 'f320'); // back feed (p�g. 18)
            end;
          end;
          Memo.Lines.Add(Char(2) + 'e'); // ver gaps (p�g. 17)
          Memo.Lines.Add(Char(2) + 'O0220'); // start position (p�g. 24)
          Memo.Lines.Add(Char(2) + 'L'); // FORMATA��O DO LABEL (p�g 22)
          Memo.Lines.Add('C0000'); // Margem esquerda (p�g 34)
          //Memo.Lines.Add('H10'); // Intensidade do calor [H02 a H20] (p�g 38)
          //Memo.Lines.Add('PE'); // Velocidade - varia cfe modelo! (p�g 39)
          Memo.Lines.Add('D11'); // Tamanho m�nimo do pixel (p�g 35)
        end;
        bctFim:
        begin
          Memo.Lines.Add('Q0001');// Qtde de c�pias do label (p�g 40)
          Memo.Lines.Add('E');// FIMFORMATA��O LABEL (p�g 36)
        end;
      end;
    end;
  end;
  Result := True;
end;

end.
