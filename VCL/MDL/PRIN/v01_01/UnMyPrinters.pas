unit UnMyPrinters;

interface

uses
  StdCtrls, ExtCtrls, Windows, Messages, SysUtils, Classes, Graphics, Controls,
  Forms, dmkGeral, dmkEdit, UnInternalConsts, DBGrids, UnDmkEnums;

type
  TUnMyPrinters = class(TObject)
  private
    { Private declarations }

  public
    { Public declarations }
    // Mudei 2010-06-07 > coloquei op��o de S�rie, Cheque, Data, Carteira e Banco1
    // Mudei 2010-07-04 > coloquei op��o grade lcts, NomeCamnpoNomeFornecedor
    //procedure EmiteCheque(Lancto, Sub: Integer; Debito: Double;
    //          Fornecedor: String; Credito: Double; Cliente: String);
    procedure MostraEmiteCheque_0();
    procedure MostraCfgImpressao();
    procedure EmiteCheque(Lancto, Sub, Banco1: Integer; Debito: Double;
              Fornecedor: String; Credito: Double; Cliente: String;
              Serie: String; Cheque: Double; Data: TDateTime; Carteira: Integer;
              CampoNomeFornecedor: String; Grade: TDBGrid; TabLct: String;
              TipoData: Integer; DataIni, DataFim: TDate);
    procedure VerificaImpressoraDeCheque(var Retorno: String);
    function  DefAlturaDotImp(Altura, Fator: Integer; Partes: String): String;
  end;


var
  MyPrinters: TUnMyPrinters;

implementation

uses PertoChek, UnBematech_DP_20, ModuleGeral, Module, UnMyObjects,
EmiteCheque_0, EmiteCheque_1, EmiteCheque_3, EmiteCheque_4, MyDBCheck, Imprime,
DmkDAC_PF;

// Mudei 2010-06-07 > coloquei op��o de S�rie, Cheque, Data, Carteira e Banco1
// Mudei 2010-07-04 > coloquei op��o grade lcts, NomeCamnpoNomeFornecedor
function TUnMyPrinters.DefAlturaDotImp(Altura, Fator: Integer;
  Partes: String): String;
begin
  if Altura > 2 then
  begin
    if Partes = '' then
    begin
      case Fator of
        00: Partes := #0;
        01: Partes := #1;
        02: Partes := #2;
        03: Partes := #3;
        04: Partes := #4;
        05: Partes := #5;
        06: Partes := #6;
        07: Partes := #7;
        08: Partes := #8;
        09: Partes := #9;
        10: Partes := #10;
        11: Partes := #11;
        12: Partes := #12;
        13: Partes := #13;
        14: Partes := #14;
        15: Partes := #15;
        16: Partes := #16;
        17: Partes := #17;
        18: Partes := #18;
        19: Partes := #19;
        20: Partes := #20;
        21: Partes := #21;
        22: Partes := #22;
        23: Partes := #23;
        24: Partes := #24;
        25: Partes := #25;
        26: Partes := #26;
        27: Partes := #27;
        28: Partes := #28;
        29: Partes := #29;
        30: Partes := #30;
        31: Partes := #31;
        32: Partes := #32;
        33: Partes := #33;
        34: Partes := #34;
        35: Partes := #35;
        36: Partes := #36;
        37: Partes := #37;
        38: Partes := #38;
        39: Partes := #39;
        40: Partes := #40;
        41: Partes := #41;
        42: Partes := #42;
        43: Partes := #43;
        44: Partes := #44;
        45: Partes := #45;
        46: Partes := #46;
        47: Partes := #47;
        48: Partes := #48;
        49: Partes := #49;
        50: Partes := #50;
        51: Partes := #51;
        52: Partes := #52;
        53: Partes := #53;
        54: Partes := #54;
        55: Partes := #55;
        56: Partes := #56;
        57: Partes := #57;
        58: Partes := #58;
        59: Partes := #59;
        60: Partes := #60;
        61: Partes := #61;
        62: Partes := #62;
        63: Partes := #63;
        64: Partes := #64;
        65: Partes := #65;
        66: Partes := #66;
        67: Partes := #67;
        68: Partes := #68;
        69: Partes := #69;
        70: Partes := #70;
        71: Partes := #71;
        72: Partes := #72;
        73: Partes := #73;
        74: Partes := #74;
        75: Partes := #75;
        76: Partes := #76;
        77: Partes := #77;
        78: Partes := #78;
        79: Partes := #79;
        80: Partes := #80;
        81: Partes := #81;
        82: Partes := #82;
        83: Partes := #83;
        84: Partes := #84;
        85: Partes := #85;
        86: Partes := #86;
        87: Partes := #87;
        88: Partes := #88;
        89: Partes := #89;
        90: Partes := #90;
        91: Partes := #91;
        92: Partes := #92;
        93: Partes := #93;
        94: Partes := #94;
        95: Partes := #95;
        96: Partes := #96;
        97: Partes := #97;
        98: Partes := #98;
        99: Partes := #99;
        100: Partes := #100;
        101: Partes := #101;
        102: Partes := #102;
        103: Partes := #103;
        104: Partes := #104;
        105: Partes := #105;
        106: Partes := #106;
        107: Partes := #107;
        108: Partes := #108;
        109: Partes := #109;
        110: Partes := #110;
        111: Partes := #111;
        112: Partes := #112;
        113: Partes := #113;
        114: Partes := #114;
        115: Partes := #115;
        116: Partes := #116;
        117: Partes := #117;
        118: Partes := #118;
        119: Partes := #119;
        120: Partes := #120;
        121: Partes := #121;
        122: Partes := #122;
        123: Partes := #123;
        124: Partes := #124;
        125: Partes := #125;
        126: Partes := #126;
        127: Partes := #127;
        128: Partes := #128;
        129: Partes := #129;
        130: Partes := #130;
        131: Partes := #131;
        132: Partes := #132;
        133: Partes := #133;
        134: Partes := #134;
        135: Partes := #135;
        136: Partes := #136;
        137: Partes := #137;
        138: Partes := #138;
        139: Partes := #139;
        140: Partes := #140;
        141: Partes := #141;
        142: Partes := #142;
        143: Partes := #143;
        144: Partes := #144;
        145: Partes := #145;
        146: Partes := #146;
        147: Partes := #147;
        148: Partes := #148;
        149: Partes := #149;
        150: Partes := #150;
        151: Partes := #151;
        152: Partes := #152;
        153: Partes := #153;
        154: Partes := #154;
        155: Partes := #155;
        156: Partes := #156;
        157: Partes := #157;
        158: Partes := #158;
        159: Partes := #159;
        160: Partes := #160;
        161: Partes := #161;
        162: Partes := #162;
        163: Partes := #163;
        164: Partes := #164;
        165: Partes := #165;
        166: Partes := #166;
        167: Partes := #167;
        168: Partes := #168;
        169: Partes := #169;
        170: Partes := #170;
        171: Partes := #171;
        172: Partes := #172;
        173: Partes := #173;
        174: Partes := #174;
        175: Partes := #175;
        176: Partes := #176;
        177: Partes := #177;
        178: Partes := #178;
        179: Partes := #179;
        180: Partes := #180;
        181: Partes := #181;
        182: Partes := #182;
        183: Partes := #183;
        184: Partes := #184;
        185: Partes := #185;
        186: Partes := #186;
        187: Partes := #187;
        188: Partes := #188;
        189: Partes := #189;
        190: Partes := #190;
        191: Partes := #191;
        192: Partes := #192;
        193: Partes := #193;
        194: Partes := #194;
        195: Partes := #195;
        196: Partes := #196;
        197: Partes := #197;
        198: Partes := #198;
        199: Partes := #199;
        200: Partes := #200;
        201: Partes := #201;
        202: Partes := #202;
        203: Partes := #203;
        204: Partes := #204;
        205: Partes := #205;
        206: Partes := #206;
        207: Partes := #207;
        208: Partes := #208;
        209: Partes := #209;
        210: Partes := #210;
        211: Partes := #211;
        212: Partes := #212;
        213: Partes := #213;
        214: Partes := #214;
        215: Partes := #215;
        216: Partes := #216;
      end;
    end;
  end else Partes := '';
  case Altura of
    3: Partes := '3'+Partes;
    4: Partes := 'A'+Partes;
  end;
  Result := #27+Partes;
end;

procedure TUnMyPrinters.EmiteCheque(Lancto, Sub, Banco1: Integer; Debito: Double;
  Fornecedor: String; Credito: Double; Cliente: String;
  Serie: String; Cheque: Double; Data: TDateTime; Carteira: Integer;
  CampoNomeFornecedor: String; Grade: TDBGrid; TabLct: String;
  TipoData: Integer; DataIni, DataFim: TDate);
var
  Valor, Benef, Cidade: String;
  Banco: Integer;
  Emiss: TDateTime;
begin
  Banco := 0;
  Emiss := Date;
  if VAR_IMPCHEQUE <> 4 then (* m�ltiplos cheques! Matricial > EPSON LX 300 *)
  begin
    if Cheque <> 0 then
    begin
      DModG.QrCheque.Close;
      DModG.QrCheque.SQL.Clear;
      DModG.QrCheque.SQL.Add('SELECT ca.Banco1, SUM(la.Debito) Debito,');
      DModG.QrCheque.SQL.Add('SUM(la.Credito) Credito, la.Fornecedor, la.Cliente,');
      DModG.QrCheque.SQL.Add('IF(cl.Tipo=0, cl.RazaoSocial, cl.Nome) NOMECLIENTE,');
      DModG.QrCheque.SQL.Add('IF(fo.Tipo=0, fo.RazaoSocial, fo.Nome) NOMEFORNECEDOR,');
      DModG.QrCheque.SQL.Add('la.Data, la.Vencimento');
      DModG.QrCheque.SQL.Add('FROM ' + TabLct + ' la');
      DModG.QrCheque.SQL.Add('LEFT JOIN entidades cl ON cl.Codigo=la.Cliente');
      DModG.QrCheque.SQL.Add('LEFT JOIN entidades fo ON fo.Codigo=la.Fornecedor');
      DModG.QrCheque.SQL.Add('LEFT JOIN carteiras ca ON ca.Codigo=la.Carteira');
      DModG.QrCheque.SQL.Add('WHERE la.SerieCH=:P0');
      DModG.QrCheque.SQL.Add('AND la.Documento=:P1');
      DModG.QrCheque.SQL.Add('AND la.Data=:P2');
      DModG.QrCheque.SQL.Add('AND la.Carteira=:P3');
      DModG.QrCheque.SQL.Add('GROUP BY la.SerieCH, la.Documento, la.Data, la.Carteira');
      DModG.QrCheque.Params[00].AsString  := Serie;
      DModG.QrCheque.Params[01].AsFloat   := Cheque;
      DModG.QrCheque.Params[02].AsString  := Geral.FDT(Data, 1);
      DModG.QrCheque.Params[03].AsInteger := Carteira;
      UnDmkDAC_PF.AbreQuery(DModG.QrCheque, Dmod.MyDB);
      //
      if DModG.QrChequeDebito.Value <> 0 then
      begin
        Valor := Geral.FFT(DModG.QrChequeDebito.Value, 2, siPositivo);
        Benef := DModG.QrChequeNOMEFORNECEDOR.Value;
      end else begin
        Valor := Geral.FFT(DModG.QrChequeCredito.Value, 2, siPositivo);
        Benef := DModG.QrChequeNOMECLIENTE.Value;
      end;
      Banco := DModG.QrChequeBanco1.Value;
      Emiss := DModG.QrChequeData.Value;
    end else
    if Lancto <> 0 then
    begin
      DModG.QrLancto.Close;
      DModG.QrLancto.SQL.Clear;
      DModG.QrLancto.SQL.Add('SELECT ca.Banco1, la.Debito, la.Credito, la.Fornecedor, la.Cliente,');
      DModG.QrLancto.SQL.Add('IF(cl.Tipo=0, cl.RazaoSocial, cl.Nome) NOMECLIENTE,');
      DModG.QrLancto.SQL.Add('IF(fo.Tipo=0, fo.RazaoSocial, fo.Nome) NOMEFORNECEDOR,');
      DModG.QrLancto.SQL.Add('la.Data, la.Vencimento');
      DModG.QrLancto.SQL.Add('FROM ' + TabLct + ' la');
      DModG.QrLancto.SQL.Add('LEFT JOIN entidades cl ON cl.Codigo=la.Cliente');
      DModG.QrLancto.SQL.Add('LEFT JOIN entidades fo ON fo.Codigo=la.Fornecedor');
      DModG.QrLancto.SQL.Add('LEFT JOIN carteiras ca ON ca.Codigo=la.Carteira');
      DModG.QrLancto.SQL.Add('WHERE la.Controle=:P0');
      DModG.QrLancto.SQL.Add('AND Sub=:P1');
      DModG.QrLancto.Params[00].AsInteger := Lancto;
      DModG.QrLancto.Params[01].AsInteger := Sub;
      UnDmkDAC_PF.AbreQuery(DModG.QrLancto, Dmod.MyDB);
      //
      if DModG.QrLanctoDebito.Value <> 0 then
      begin
        Valor := Geral.FFT(DModG.QrLanctoDebito.Value, 2, siPositivo);
        Benef := DModG.QrLanctoNOMEFORNECEDOR.Value;
      end else begin
        Valor := Geral.FFT(DModG.QrLanctoCredito.Value, 2, siPositivo);
        Benef := DModG.QrLanctoNOMECLIENTE.Value;
      end;
      Banco := DModG.QrLanctoBanco1.Value;
      Emiss := DModG.QrLanctoData.Value;
    end else
    begin
      if Debito <> 0 then
      begin
        Valor := Geral.FFT(Debito, 2, siPositivo);
        Benef := Fornecedor;
      end else begin
        Valor := Geral.FFT(Credito, 2, siPositivo);
        Benef := Cliente;
      end;
      Banco := Banco1;
      Emiss := Data;
    end;
    Cidade := DModG.ObtemNomeCidade(Trunc(DmodG.QrDonoCODMUNICI.Value));
    //
    if VAR_IMPCHEQUE in ([1,2]) then
    begin
      Benef := Geral.SemAcento(Geral.Maiusculas(Benef, False));
      Cidade := Geral.SemAcento(Geral.Maiusculas(Cidade, False));
    end;
  end;
  //
  case VAR_IMPCHEQUE of
    0: // Servidor
    begin
      Application.CreateForm(TFmEmiteCheque_0, FmEmiteCheque_0);
      FmEmiteCheque_0.TPData.Date   := Emiss;
      FmEmiteCheque_0.EdValor.Text  := Valor;
      FmEmiteCheque_0.EdBenef.Text  := Benef;
      FmEmiteCheque_0.EdCidade.Text := Cidade;
      FmEmiteCheque_0.EdBanco.ValueVariant  := Banco;
      //
      FmEmiteCheque_0.ShowModal;
      FmEmiteCheque_0.Destroy;
    end;
    1,2: // Perto (serial e paralela)
    begin
      Application.CreateForm(TFmEmiteCheque_1, FmEmiteCheque_1);
      FmEmiteCheque_1.TPData.Date   := Emiss;
      FmEmiteCheque_1.EdValor.Text  := Valor;
      FmEmiteCheque_1.EdBenef.Text  := Benef;
      FmEmiteCheque_1.EdCidade.Text := Cidade;
      FmEmiteCheque_1.EdBanco.ValueVariant  := Banco;
      //
      MyObjects.Informa2(FmEmiteCheque_1.LaSerie1, FmEmiteCheque_1.LaSerie2,
        False, Serie);
      MyObjects.Informa2(FmEmiteCheque_1.LaCheque1, FmEmiteCheque_1.LaCheque2,
        False, Geral.FFN(Trunc(Cheque), 6));
      //
      FmEmiteCheque_1.ShowModal;
      FmEmiteCheque_1.Destroy;
    end;
    3: // Bematech DP 20
    begin
      Application.CreateForm(TFmEmiteCheque_3, FmEmiteCheque_3);
      FmEmiteCheque_3.TPData.Date           := Emiss;
      FmEmiteCheque_3.EdValor.Text          := Valor;
      FmEmiteCheque_3.EdBenef.Text          := Benef;
      FmEmiteCheque_3.EdCidade.Text         := Cidade;
      FmEmiteCheque_3.EdBanco.ValueVariant  := Banco;
      if DModG.QrLanctoVencimento.Value > Date then
        FmEmiteCheque_3.EdMens.Text  := 'BOM PARA ' +
        FormatDateTime('dd/mm/yyyy', DModG.QrLanctoVencimento.Value);
      //
      MyObjects.Informa2(FmEmiteCheque_3.LaSerie1, FmEmiteCheque_3.LaSerie2,
        False, Serie);
      MyObjects.Informa2(FmEmiteCheque_3.LaCheque1, FmEmiteCheque_3.LaCheque2,
        False, Geral.FFN(Trunc(Cheque), 6));
      //
      FmEmiteCheque_3.ShowModal;
      FmEmiteCheque_3.Destroy;
    end;
    4: // EPSON LX 300
    begin
      if DBCheck.CriaFm(TFmEmiteCheque_4, FmEmiteCheque_4, afmoNegarComAviso) then
      begin
        FmEmiteCheque_4.FCampoNomeFornecedor := CampoNomeFornecedor;
        FmEmiteCheque_4.FTabLct              := TabLct;
        FmEmiteCheque_4.FGrade               := Grade;
        FmEmiteCheque_4.FCarteira            := Carteira;
        //
        if TipoData >= 0 then
          FmEmiteCheque_4.RGTipoData.ItemIndex := TipoData;
        if DataIni > 0 then
          FmEmiteCheque_4.TPDataIni.Date := DataIni
        else
          FmEmiteCheque_4.TPDataIni.Date := Date;
        if DataFim > 0 then
          FmEmiteCheque_4.TPDataFim.Date := DataFim
        else
          FmEmiteCheque_4.TPDataFim.Date := Date;
        //
        FmEmiteCheque_4.ShowModal;
        FmEmiteCheque_4.Destroy;
        //
      end;
    //  DModFin.ImpressaoEDefinicaoDeChequesDeLctos(TDBGrid(DBGLct), 'NOMEFORNECEDOR');
    end;
  end;
end;

procedure TUnMyPrinters.MostraCfgImpressao;
begin
  if DBCheck.CriaFm(TFmImprime, FmImprime, afmoNegarComAviso) then
  begin
    FmImprime.ShowModal;
    FmImprime.Destroy;
  end;
end;

procedure TUnMyPrinters.MostraEmiteCheque_0;
begin
  if DBCheck.CriaFm(TFmEmiteCheque_0, FmEmiteCheque_0, afmoNegarComAviso) then
  begin
    FmEmiteCheque_0.ShowModal;
    FmEmiteCheque_0.Destroy;
  end;
end;

procedure TUnMyPrinters.VerificaImpressoraDeCheque(var Retorno: String);
begin
  Retorno := '';
  case VAR_IMPCHEQUE of
    1,2: PertoChekP.Verifica_Pertochek(Retorno);
    3: Bematech_DP_20.ImpressoraVerifica(Retorno);
  end;
  if Trim(Retorno) <> '' then
    Geral.MB_Aviso(Retorno);
end;

end.
