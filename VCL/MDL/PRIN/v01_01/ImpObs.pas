unit ImpObs;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls, Db, (*DBTables,*) JPEG, ExtDlgs,
  ZCF2, ResIntStrings, UnDmkProcFunc, dmkGeral, UnGOTOy, UnInternalConsts,
  UnMsgInt, UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa,
  dmkEdit, dmkImage, UnDmkEnums;

type
  TFmImpObs = class(TForm)
    PainelDados: TPanel;
    DsImpObs: TDataSource;
    QrImpObs: TmySQLQuery;
    PainelEdita: TPanel;
    PainelEdit: TPanel;
    Label9: TLabel;
    EdCodigo: TdmkEdit;
    Label10: TLabel;
    PainelData: TPanel;
    Label1: TLabel;
    DBEdCodigo: TDBEdit;
    DBEdNome: TDBEdit;
    Label2: TLabel;
    EdNome: TdmkEdit;
    StaticText1: TStaticText;
    MeTexto: TMemo;
    QrImpObsCodigo: TIntegerField;
    QrImpObsNome: TWideStringField;
    QrImpObsTexto: TWideMemoField;
    QrImpObsAlterWeb: TSmallintField;
    QrImpObsAtivo: TSmallintField;
    MeDBTxt: TDBMemo;
    StaticText2: TStaticText;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel6: TPanel;
    PnSaiDesis: TPanel;
    BtConfirma: TBitBtn;
    BtDesiste: TBitBtn;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    BtInclui: TBitBtn;
    BtAltera: TBitBtn;
    BtExclui: TBitBtn;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtIncluiClick(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrImpObsAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrImpObsBeforeOpen(DataSet: TDataSet);
    procedure SbImprimeClick(Sender: TObject);
    procedure SbNovoClick(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure IncluiRegistro;
    procedure AlteraRegistro;
   ////Procedures do form
    procedure MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo: Integer);
    procedure DefParams;
    procedure LocCod(Atual, Codigo: Integer);
    procedure Va(Para: TVaiPara);
  public
    { Public declarations }
  end;

var
  FmImpObs: TFmImpObs;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmImpObs.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmImpObs.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrImpObsCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmImpObs.DefParams;
begin
  VAR_GOTOTABELA := 'ImpObs';
  VAR_GOTOMYSQLTABLE := QrImpObs;
  VAR_GOTONEG := gotoAll;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;
  VAR_SQLx.Add('SELECT *');
  VAR_SQLx.Add('FROM impobs');
  VAR_SQLx.Add('WHERE Codigo <> 0');
  //
  VAR_SQL1.Add('AND Codigo=:P0');
  //
  VAR_SQLa.Add('AND Nome Like :P0');
  //
end;

procedure TFmImpObs.MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo: Integer);
begin
  case Mostra of
    0:
    begin
      PainelDados.Visible := True;
      PainelEdita.Visible := False;
    end;
    1:
    begin
      PainelEdita.Visible := True;
      PainelDados.Visible := False;
      if SQLType = stIns then
      begin
        EdCodigo.Text := FormatFloat(FFormatFloat, Codigo);
        EdNome.Text := '';
        MeTexto.Text := '';
      end else begin
        EdCodigo.Text := DBEdCodigo.Text;
        EdNome.Text := DBEdNome.Text;
        MeTexto.Text := QrImpObsTexto.Value;
      end;
      EdNome.SetFocus;
    end;
    else Application.MessageBox('A��o de Inclus�o/altera��o n�o definida!',
    'Aviso', MB_OK+MB_ICONWARNING);
  end;
  ImgTipo.SQLType := SQLType;
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmImpObs.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmImpObs.AlteraRegistro;
var
  ImpObs : Integer;
begin
  ImpObs := QrImpObsCodigo.Value;
  if QrImpObsCodigo.Value = 0 then
  begin
    BtAltera.Enabled := False;
    Exit;
  end;
  if not UMyMod.SelLockY(ImpObs, Dmod.MyDB, 'ImpObs', 'Codigo') then
  begin
    try
      UMyMod.UpdLockY(ImpObs, Dmod.MyDB, 'ImpObs', 'Codigo');
      MostraEdicao(1, stLok, 0);
    finally
      Screen.Cursor := Cursor;
    end;
  end;
end;

procedure TFmImpObs.IncluiRegistro;
var
  Cursor : TCursor;
  ImpObs : Integer;
begin
  Cursor := Screen.Cursor;
  Screen.Cursor := crHourglass;
  Refresh;
  try
    ImpObs := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
    'ImpObs', 'ImpObs', 'Codigo');
    if Length(FormatFloat(FFormatFloat, ImpObs))>Length(FFormatFloat) then
    begin
      Application.MessageBox(
      'Inclus�o cancelada. Limite de cadastros extrapolado', 'Erro',
      MB_OK+MB_ICONERROR);
      Screen.Cursor := Cursor;
      Exit;
    end;
    MostraEdicao(1, stIns, ImpObs);
  finally
    Screen.Cursor := Cursor;
  end;
end;

procedure TFmImpObs.QueryPrincipalAfterOpen;
begin
end;

procedure TFmImpObs.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmImpObs.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmImpObs.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmImpObs.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmImpObs.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmImpObs.BtIncluiClick(Sender: TObject);
begin
  IncluiRegistro;
end;

procedure TFmImpObs.BtAlteraClick(Sender: TObject);
begin
  AlteraRegistro;
end;

procedure TFmImpObs.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrImpObsCodigo.Value;
  Close;
end;

procedure TFmImpObs.BtConfirmaClick(Sender: TObject);
var
  Codigo: Integer;
  Nome: String;
begin
  Nome := EdNome.Text;
  if Length(Nome) = 0 then
  begin
    Application.MessageBox('Defina uma descri��o.', 'Erro', MB_OK+MB_ICONERROR);
    Exit;
  end;
  {
  Codigo := UMyMod.BuscaEmLivreY_Def('MPP', 'Codigo', ImgTipo.SQLType, QrMPPCodigo.Value);
  }
  Codigo := Geral.IMV(EdCodigo.Text);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'impobs', False,
  [
    'Nome', 'Texto'
  ], ['Codigo'], [
    Nome, MeTexto.Text
  ], [Codigo], True) then
  begin
    UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'ImpObs', 'Codigo');
    MostraEdicao(0, stLok, 0);
    LocCod(Codigo,Codigo);
  end;
end;

procedure TFmImpObs.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := Geral.IMV(EdCodigo.Text);
  if ImgTipo.SQLType = stIns then UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'ImpObs', Codigo);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'ImpObs', 'Codigo');
  MostraEdicao(0, stLok, 0);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'ImpObs', 'Codigo');
end;

procedure TFmImpObs.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  MeTexto.Align     := alClient;
  MeDBTxt.Align     := alClient;
  CriaOForm;
end;

procedure TFmImpObs.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrImpObsCodigo.Value,LaRegistro.Caption);
end;

procedure TFmImpObs.SbImprimeClick(Sender: TObject);
begin
//
end;

procedure TFmImpObs.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmImpObs.SbNovoClick(Sender: TObject);
begin
//
end;

procedure TFmImpObs.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmImpObs.QrImpObsAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmImpObs.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmImpObs.SbQueryClick(Sender: TObject);
begin
  LocCod(QrImpObsCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'ImpObs', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmImpObs.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmImpObs.QrImpObsBeforeOpen(DataSet: TDataSet);
begin
  QrImpObsCodigo.DisplayFormat := FFormatFloat;
end;

end.

