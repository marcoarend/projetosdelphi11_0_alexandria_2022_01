unit ImpDOS;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls, Db, (*DBTables,*) JPEG, ExtDlgs,
  ZCF2, ResIntStrings, UnMLAGeral, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, Grids, DBGrids, Menus,
  ComCtrls, dmkGeral, dmkEdit, UnDmkProcFunc, dmkImage, UnDmkEnums;

type
  TFmImpDOS = class(TForm)
    PainelDados: TPanel;
    DsImpDOS: TDataSource;
    QrImpDOS: TmySQLQuery;
    PainelEdita: TPanel;
    Label9: TLabel;
    Label10: TLabel;
    EdCodigo: TdmkEdit;
    EdNome: TEdit;
    Panel1: TPanel;
    Label1: TLabel;
    DBEdCodigo: TDBEdit;
    DBEdNome: TDBEdit;
    Label2: TLabel;
    QrImpDOSImp: TmySQLQuery;
    DsImpDOSImp: TDataSource;
    QrLoc: TmySQLQuery;
    QrImpDOSCodigo: TIntegerField;
    QrImpDOSDescricao: TWideStringField;
    QrImpDOSImpTabela: TIntegerField;
    QrImpDOSImpDescricao: TWideStringField;
    QrImpDOSImpCodigo: TIntegerField;
    QrImpDOSImpItem: TIntegerField;
    QrImpDOSImpLinha: TSmallintField;
    QrImpDOSImpColuna: TSmallintField;
    QrImpDOSImpLetras: TSmallintField;
    QrImpDOSImpFoTam: TSmallintField;
    QrImpDOSImpNegrito: TSmallintField;
    QrImpDOSImpItalico: TSmallintField;
    QrImpDOSImpPrefixo: TWideStringField;
    QrImpDOSImpSufixo: TWideStringField;
    QrImpDOSImpPadrao: TWideStringField;
    QrImpDOSImpSubstituto: TWideStringField;
    QrImpDOSImpSubstituir: TSmallintField;
    QrImpDOSImpNOMETABELA: TWideStringField;
    PMAltera: TPopupMenu;
    AlteraaImpresso1: TMenuItem;
    AlteraItem1: TMenuItem;
    Selecionado1: TMenuItem;
    Seqncia1: TMenuItem;
    PainelItens: TPanel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label11: TLabel;
    EdLinha: TdmkEdit;
    EdColuna: TdmkEdit;
    EdLetras: TdmkEdit;
    RGFonte: TRadioGroup;
    CkNegrito: TCheckBox;
    CkItalico: TCheckBox;
    CkSubstit: TCheckBox;
    EdPadrao: TEdit;
    EdPrefixo: TEdit;
    EdSufixo: TEdit;
    EdSubstituto: TEdit;
    RGFormato: TRadioGroup;
    QrImpDOSImpCODIGOITEM: TIntegerField;
    QrLocCodigo: TIntegerField;
    QrLocItem: TIntegerField;
    QrLocLinha: TSmallintField;
    QrLocColuna: TSmallintField;
    QrLocLetras: TSmallintField;
    QrLocFoTam: TSmallintField;
    QrLocNegrito: TSmallintField;
    QrLocItalico: TSmallintField;
    QrLocPrefixo: TWideStringField;
    QrLocSufixo: TWideStringField;
    QrLocPadrao: TWideStringField;
    QrLocSubstituto: TWideStringField;
    QrLocSubstituir: TSmallintField;
    QrImpDOSImpFormato: TWideStringField;
    Label12: TLabel;
    Label13: TLabel;
    DBEdit2: TDBEdit;
    Label16: TLabel;
    EdLProd: TdmkEdit;
    Label17: TLabel;
    EdLServ: TdmkEdit;
    EdLFatu: TdmkEdit;
    Label18: TLabel;
    Label19: TLabel;
    EdCFatu: TdmkEdit;
    Label20: TLabel;
    EdEFatu: TdmkEdit;
    QrImpDOSLk: TIntegerField;
    QrImpDOSDataCad: TDateField;
    QrImpDOSDataAlt: TDateField;
    QrImpDOSUserCad: TIntegerField;
    QrImpDOSUserAlt: TIntegerField;
    QrImpDOSLProd: TIntegerField;
    QrImpDOSLServ: TIntegerField;
    QrImpDOSLFatu: TIntegerField;
    QrImpDOSCFatu: TIntegerField;
    QrImpDOSEFatu: TIntegerField;
    Label21: TLabel;
    Label22: TLabel;
    Label23: TLabel;
    Label24: TLabel;
    Label25: TLabel;
    DBEdit3: TDBEdit;
    DBEdit4: TDBEdit;
    DBEdit5: TDBEdit;
    DBEdit6: TDBEdit;
    DBEdit7: TDBEdit;
    RGAlinhamento: TRadioGroup;
    QrImpDOSImpAlinhamento: TSmallintField;
    Grade: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    GradeA: TDBGrid;
    GradeE: TDBGrid;
    TabSheet3: TTabSheet;
    GradeP: TDBGrid;
    TabSheet4: TTabSheet;
    TabSheet5: TTabSheet;
    GradeS: TDBGrid;
    GradeF: TDBGrid;
    TabSheet6: TTabSheet;
    GradeT: TDBGrid;
    QrA: TmySQLQuery;
    DsA: TDataSource;
    QrE: TmySQLQuery;
    DsE: TDataSource;
    QrP: TmySQLQuery;
    DsP: TDataSource;
    QrS: TmySQLQuery;
    DsS: TDataSource;
    QrF: TmySQLQuery;
    DsF: TDataSource;
    QrT: TmySQLQuery;
    DsT: TDataSource;
    QrATabela: TIntegerField;
    QrADescricao: TWideStringField;
    QrACODIGOITEM: TIntegerField;
    QrACodigo: TIntegerField;
    QrAItem: TIntegerField;
    QrALinha: TIntegerField;
    QrAColuna: TIntegerField;
    QrALetras: TIntegerField;
    QrAFoTam: TSmallintField;
    QrANegrito: TSmallintField;
    QrAItalico: TSmallintField;
    QrAPrefixo: TWideStringField;
    QrASufixo: TWideStringField;
    QrAPadrao: TWideStringField;
    QrASubstituto: TWideStringField;
    QrASubstituir: TSmallintField;
    QrAFormato: TWideStringField;
    QrAAlinhamento: TSmallintField;
    QrETabela: TIntegerField;
    QrEDescricao: TWideStringField;
    QrECODIGOITEM: TIntegerField;
    QrECodigo: TIntegerField;
    QrEItem: TIntegerField;
    QrELinha: TIntegerField;
    QrEColuna: TIntegerField;
    QrELetras: TIntegerField;
    QrEFoTam: TSmallintField;
    QrENegrito: TSmallintField;
    QrEItalico: TSmallintField;
    QrEPrefixo: TWideStringField;
    QrESufixo: TWideStringField;
    QrEPadrao: TWideStringField;
    QrESubstituto: TWideStringField;
    QrESubstituir: TSmallintField;
    QrEFormato: TWideStringField;
    QrEAlinhamento: TSmallintField;
    QrPTabela: TIntegerField;
    QrPDescricao: TWideStringField;
    QrPCODIGOITEM: TIntegerField;
    QrPCodigo: TIntegerField;
    QrPItem: TIntegerField;
    QrPLinha: TIntegerField;
    QrPColuna: TIntegerField;
    QrPLetras: TIntegerField;
    QrPFoTam: TSmallintField;
    QrPNegrito: TSmallintField;
    QrPItalico: TSmallintField;
    QrPPrefixo: TWideStringField;
    QrPSufixo: TWideStringField;
    QrPPadrao: TWideStringField;
    QrPSubstituto: TWideStringField;
    QrPSubstituir: TSmallintField;
    QrPFormato: TWideStringField;
    QrPAlinhamento: TSmallintField;
    QrSTabela: TIntegerField;
    QrSDescricao: TWideStringField;
    QrSCODIGOITEM: TIntegerField;
    QrSCodigo: TIntegerField;
    QrSItem: TIntegerField;
    QrSLinha: TIntegerField;
    QrSColuna: TIntegerField;
    QrSLetras: TIntegerField;
    QrSFoTam: TSmallintField;
    QrSNegrito: TSmallintField;
    QrSItalico: TSmallintField;
    QrSPrefixo: TWideStringField;
    QrSSufixo: TWideStringField;
    QrSPadrao: TWideStringField;
    QrSSubstituto: TWideStringField;
    QrSSubstituir: TSmallintField;
    QrSFormato: TWideStringField;
    QrSAlinhamento: TSmallintField;
    QrFTabela: TIntegerField;
    QrFDescricao: TWideStringField;
    QrFCODIGOITEM: TIntegerField;
    QrFCodigo: TIntegerField;
    QrFItem: TIntegerField;
    QrFLinha: TIntegerField;
    QrFColuna: TIntegerField;
    QrFLetras: TIntegerField;
    QrFFoTam: TSmallintField;
    QrFNegrito: TSmallintField;
    QrFItalico: TSmallintField;
    QrFPrefixo: TWideStringField;
    QrFSufixo: TWideStringField;
    QrFPadrao: TWideStringField;
    QrFSubstituto: TWideStringField;
    QrFSubstituir: TSmallintField;
    QrFFormato: TWideStringField;
    QrFAlinhamento: TSmallintField;
    QrTTabela: TIntegerField;
    QrTDescricao: TWideStringField;
    QrTCODIGOITEM: TIntegerField;
    QrTCodigo: TIntegerField;
    QrTItem: TIntegerField;
    QrTLinha: TIntegerField;
    QrTColuna: TIntegerField;
    QrTLetras: TIntegerField;
    QrTFoTam: TSmallintField;
    QrTNegrito: TSmallintField;
    QrTItalico: TSmallintField;
    QrTPrefixo: TWideStringField;
    QrTSufixo: TWideStringField;
    QrTPadrao: TWideStringField;
    QrTSubstituto: TWideStringField;
    QrTSubstituir: TSmallintField;
    QrTFormato: TWideStringField;
    QrTAlinhamento: TSmallintField;
    EdTabela: TEdit;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel2: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel3: TPanel;
    BtDesiste: TBitBtn;
    GroupBox1: TGroupBox;
    BtConfirma2: TBitBtn;
    Panel4: TPanel;
    BtDesiste2: TBitBtn;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel6: TPanel;
    BtExclui: TBitBtn;
    BtAltera: TBitBtn;
    BtInclui: TBitBtn;
    Panel7: TPanel;
    BtSaida: TBitBtn;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtIncluiClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrImpDOSAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure QrImpDOSAfterScroll(DataSet: TDataSet);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrImpDOSBeforeOpen(DataSet: TDataSet);
    procedure QrImpDOSAfterClose(DataSet: TDataSet);
    procedure BtAlteraClick(Sender: TObject);
    procedure AlteraaImpresso1Click(Sender: TObject);
    procedure Selecionado1Click(Sender: TObject);
    procedure Seqncia1Click(Sender: TObject);
    procedure BtDesiste2Click(Sender: TObject);
    procedure BtConfirma2Click(Sender: TObject);
    procedure QrImpDOSImpCalcFields(DataSet: TDataSet);
    procedure SbImprimeClick(Sender: TObject);
    procedure GradeADrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure GradeEDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure GradePDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure GradeSDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure GradeFDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure GradeTDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure SbNovoClick(Sender: TObject);
    procedure BtExcluiClick(Sender: TObject);
  private
    Query: TmySQLQuery;
    FItemA, FItemE, FItemP, FItemS, FItemF, FItemT: Integer;
    FSequencia: Boolean;
    procedure DefineNomeTabela(Tabela: Integer);
    procedure CriaOForm;
    procedure SubQuery1Reopen;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure IncluiRegistro;
    procedure AlteraRegistro;
   ////Procedures do form
    procedure MostraEdicao(Mostra: Boolean; SQLType: TSQLType; Codigo: Integer);
    procedure DefParams;
    procedure LocCod(Atual, Codigo: Integer);
    procedure Va(Para: TVaiPara);
    //ocedure GradeClica(Checked: Integer);
    procedure AlteraSubRegistro;
    procedure MostraPainelItens;
    procedure EscondePainelItens;

  public
    { Public declarations }
  end;

var
  FmImpDOS: TFmImpDOS;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, MyVCLSkin, DotPrint;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmImpDOS.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmImpDOS.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrImpDOSCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmImpDOS.DefParams;
begin
  VAR_GOTOTABELA := 'ImpDOS';
  VAR_GOTOMYSQLTABLE := QrImpDOS;
  VAR_GOTONEG := gotoPiZ;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_DESCRICAO;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;
  VAR_SQLx.Add('SELECT *');
  VAR_SQLx.Add('FROM impdos');
  VAR_SQLx.Add('WHERE Codigo > 0');
  //
  VAR_SQL1.Add('AND Codigo=:P0');
  //
  VAR_SQLa.Add('AND Descricao Like :P0');
  //
end;

procedure TFmImpDOS.MostraEdicao(Mostra: Boolean; SQLType: TSQLType; Codigo: Integer);
begin
  if Mostra then
  begin
    PainelEdita.Visible := True;
    PainelDados.Visible := False;
    EdNome.Text := CO_VAZIO;
    EdNome.Visible := True;
    if SQLType = stIns then
    begin
      EdCodigo.Text := FormatFloat(FFormatFloat, Codigo);
      EdLProd.Text := '0';
      EdLServ.Text := '0';
      EdLFatu.Text := '0';
      EdCFatu.Text := '0';
      EdEFatu.Text := '0';
    end else begin
      EdCodigo.Text := DBEdCodigo.Text;
      EdNome.Text := DBEdNome.Text;
      EdLProd.Text := Geral.FFT(QrImpDOSLProd.Value, 0, siPositivo);
      EdLServ.Text := Geral.FFT(QrImpDOSLServ.Value, 0, siPositivo);
      EdLFatu.Text := Geral.FFT(QrImpDOSLFatu.Value, 0, siPositivo);
      EdCFatu.Text := Geral.FFT(QrImpDOSCFatu.Value, 0, siPositivo);
      EdEFatu.Text := Geral.FFT(QrImpDOSEFatu.Value, 0, siPositivo);
    end;
    EdNome.SetFocus;
  end else begin
    PainelDados.Visible := True;
    PainelEdita.Visible := False;
  end;
  ImgTipo.SQLType := SQLType;
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmImpDOS.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  va(vpLast);
end;

procedure TFmImpDOS.AlteraRegistro;
var
  ImpDOS : Integer;
begin
  ImpDOS := QrImpDOSCodigo.Value;
  if QrImpDOSCodigo.Value = 0 then
  begin
    BtAltera.Enabled := False;
    Exit;
  end;
  if not UMyMod.SelLockY(ImpDOS, Dmod.MyDB, 'ImpDOS', 'Codigo') then
  begin
    try
      UMyMod.UpdLockY(ImpDOS, Dmod.MyDB, 'ImpDOS', 'Codigo');
      MostraEdicao(True, stUpd, 0);
    finally
      Screen.Cursor := Cursor;
    end;
  end;
end;

procedure TFmImpDOS.IncluiRegistro;
var
  Cursor : TCursor;
  ImpDOS : Integer;
begin
  Cursor := Screen.Cursor;
  Screen.Cursor := crHourglass;
  Refresh;
  try
    ImpDOS := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
    'ImpDOS', 'ImpDOS', 'Codigo');
    if Length(FormatFloat(FFormatFloat, ImpDOS))>Length(FFormatFloat) then
    begin
      Geral.MensagemBox(
      'Inclus�o cancelada. Limite de cadastros extrapolado', 'Erro',
      MB_OK+MB_ICONERROR);
      Screen.Cursor := Cursor;
      Exit;
    end;
    MostraEdicao(True, stIns, ImpDOS);
  finally
    Screen.Cursor := Cursor;
  end;
end;

procedure TFmImpDOS.QueryPrincipalAfterOpen;
begin
end;

procedure TFmImpDOS.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmImpDOS.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmImpDOS.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmImpDOS.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmImpDOS.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmImpDOS.BtIncluiClick(Sender: TObject);
begin
  IncluiRegistro;
end;

procedure TFmImpDOS.BtSaidaClick(Sender: TObject);
begin
  VAR_IMPRIMEFMT := QrImpDOSCodigo.Value;
  VAR_IMPRIMETYP := 0;
  Close;
end;

procedure TFmImpDOS.BtConfirmaClick(Sender: TObject);
var
  Codigo : Integer;
  Nome : String;
begin
  Nome := EdNome.Text;
  if Length(Nome) = 0 then
  begin
    Geral.MensagemBox('Defina uma descri��o.', 'Erro', MB_OK+MB_ICONERROR);
    Exit;
  end;
  Codigo := Geral.IMV(EdCodigo.Text);
  Dmod.QrUpdU.SQL.Clear;
  if ImgTipo.SQLType = stIns then
    Dmod.QrUpdU.SQL.Add('INSERT INTO impdos SET ')
  else Dmod.QrUpdU.SQL.Add('UPDATE impdos SET ');
  Dmod.QrUpdU.SQL.Add('Descricao=:P0, LProd=:P1, LServ=:P2, LFatu=:P3, CFatu=:P4, EFatu=:P5,');
  //
  if ImgTipo.SQLType = stIns then
    Dmod.QrUpdU.SQL.Add('DataCad=:Px, UserCad=:Py, Codigo=:Pz')
  else Dmod.QrUpdU.SQL.Add('DataAlt=:Px, UserAlt=:Py WHERE Codigo=:Pz');
  Dmod.QrUpdU.Params[00].AsString  := Nome;
  Dmod.QrUpdU.Params[01].AsInteger := Geral.IMV(EdLProd.Text);
  Dmod.QrUpdU.Params[02].AsInteger := Geral.IMV(EdLServ.Text);
  Dmod.QrUpdU.Params[03].AsInteger := Geral.IMV(EdLFatu.Text);
  Dmod.QrUpdU.Params[04].AsInteger := Geral.IMV(EdCFatu.Text);
  Dmod.QrUpdU.Params[05].AsInteger := Geral.IMV(EdEFatu.Text);
  //
  Dmod.QrUpdU.Params[06].AsString  := FormatDateTime(VAR_FORMATDATE, Date);
  Dmod.QrUpdU.Params[07].AsInteger := VAR_USUARIO;
  Dmod.QrUpdU.Params[08].AsInteger := Codigo;
  Dmod.QrUpdU.ExecSQL;
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'ImpDOS', 'Codigo');
  MostraEdicao(False, stLok, 0);
  LocCod(Codigo,Codigo);
end;

procedure TFmImpDOS.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := Geral.IMV(EdCodigo.Text);
  if ImgTipo.SQLType = stIns then UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'ImpDOS', Codigo);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'ImpDOS', 'Codigo');
  MostraEdicao(False, stLok, 0);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'ImpDOS', 'Codigo');
end;

procedure TFmImpDOS.BtExcluiClick(Sender: TObject);
begin
//
end;

procedure TFmImpDOS.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  PainelEdita.Align := alClient;
  PainelDados.Align := alClient;
  PainelItens.Align := alClient;
  Grade.Align       := alClient;
  CriaOForm;
end;

procedure TFmImpDOS.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrImpDOSCodigo.Value,LaRegistro.Caption);
end;

procedure TFmImpDOS.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmImpDOS.SbNovoClick(Sender: TObject);
begin
//
end;

procedure TFmImpDOS.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmImpDOS.QrImpDOSAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmImpDOS.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmImpDOS.QrImpDOSAfterScroll(DataSet: TDataSet);
begin
  BtAltera.Enabled := GOTOy.BtEnabled(QrImpDOSCodigo.Value, False);
  SubQuery1Reopen;
end;

procedure TFmImpDOS.SbQueryClick(Sender: TObject);
begin
  //LocCod(QrImpDOSCodigo.Value,
  //CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'ImpDOS', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmImpDOS.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmImpDOS.QrImpDOSBeforeOpen(DataSet: TDataSet);
begin
  QrImpDOSCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmImpDOS.SubQuery1Reopen;
begin
  QrImpDOSImp.Close;
  QrA.Close;
  QrE.Close;
  QrP.Close;
  QrS.Close;
  QrF.Close;
  QrT.Close;
  if (QrImpDOS.State in ([dsBrowse])) then
    if (QrImpDOS.RecordCount>0) then
    begin
      QrImpDOSImp.Params[0].AsInteger := QrImpDOSCodigo.Value;
      QrA.Params[0].AsInteger := QrImpDOSCodigo.Value;
      QrE.Params[0].AsInteger := QrImpDOSCodigo.Value;
      QrP.Params[0].AsInteger := QrImpDOSCodigo.Value;
      QrS.Params[0].AsInteger := QrImpDOSCodigo.Value;
      QrF.Params[0].AsInteger := QrImpDOSCodigo.Value;
      QrT.Params[0].AsInteger := QrImpDOSCodigo.Value;
      QrA.Open;
      QrE.Open;
      QrP.Open;
      QrS.Open;
      QrF.Open;
      QrT.Open;
      QrImpDOSImp.Open;
      QrA.Locate('Item', FItemA, []);
      QrE.Locate('Item', FItemE, []);
      QrP.Locate('Item', FItemP, []);
      QrS.Locate('Item', FItemS, []);
      QrF.Locate('Item', FItemF, []);
      QrT.Locate('Item', FItemT, []);
    end;
end;

procedure TFmImpDOS.QrImpDOSAfterClose(DataSet: TDataSet);
begin
  QrImpDOSImp.Close;
end;

procedure TFmImpDOS.BtAlteraClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMAltera, BtAltera);
end;

procedure TFmImpDOS.AlteraaImpresso1Click(Sender: TObject);
begin
  AlteraRegistro;
end;

procedure TFmImpDOS.Selecionado1Click(Sender: TObject);
begin
  FSequencia := False;
  AlteraSubRegistro;
end;

procedure TFmImpDOS.Seqncia1Click(Sender: TObject);
begin
  FSequencia := True;
  AlteraSubRegistro;
end;

procedure TFmImpDOS.AlteraSubRegistro;
var
  i: Integer;
begin
  DefineNomeTabela(Grade.ActivePageIndex+1);
  case Grade.ActivePageIndex of
    0: Query := QrA;
    1: Query := QrE;
    2: Query := QrP;
    3: Query := QrS;
    4: Query := QrF;
    5: Query := QrT;
  end;
  case Grade.ActivePageIndex of
    0: DBEdit2.DataSource := DsA;
    1: DBEdit2.DataSource := DsE;
    2: DBEdit2.DataSource := DsP;
    3: DBEdit2.DataSource := DsS;
    4: DBEdit2.DataSource := DsF;
    5: DBEdit2.DataSource := DsT;
  end;
  //
  EdLinha.Text := IntToStr(Query.FieldByName('Linha').AsInteger);
  EdColuna.Text := IntToStr(Query.FieldByName('Coluna').AsInteger);
  EdLetras.Text := IntToStr(Query.FieldByName('Letras').AsInteger);
  //
  RGFonte.ItemIndex := Query.FieldByName('FoTam').AsInteger;
  RGAlinhamento.ItemIndex := Query.FieldByName('Alinhamento').AsInteger;
  RGFormato.ItemIndex := 0;
  for i := 0 to RGFormato.Items.Count-1 do
  begin
    if RGFormato.Items[i] = Query.FieldByName('Formato').AsString
    then RGFormato.ItemIndex := i;
  end;
  //
  EdPadrao.Text     := Query.FieldByName('Padrao').AsString;
  EdPrefixo.Text    := Query.FieldByName('Prefixo').AsString;
  EdSufixo.Text     := Query.FieldByName('Sufixo').AsString;
  EdSubstituto.Text := Query.FieldByName('Substituto').AsString;
  //
  if Query.FieldByName('Negrito').AsInteger = 1 then
    CkNegrito.Checked := True else CkNegrito.Checked := False;
  if Query.FieldByName('Italico').AsInteger = 1 then
    CkItalico.Checked := True else CkItalico.Checked := False;
  if Query.FieldByName('Substituir').AsInteger = 1 then
    CkSubstit.Checked := True else CkSubstit.Checked := False;
    //
  MostraPainelItens;
  EdLinha.SetFocus;
end;

procedure TFmImpDOS.BtDesiste2Click(Sender: TObject);
begin
  EscondePainelItens;
end;

procedure TFmImpDOS.BtConfirma2Click(Sender: TObject);
var
  Italico, Negrito, Substit, Item: Integer;
begin
  if CkNegrito.Checked then Negrito := 1 else Negrito := 0;
  if CkItalico.Checked then Italico := 1 else Italico := 0;
  if CkSubstit.Checked then Substit := 1 else Substit := 0;
  QrLoc.Close;
  QrLoc.Params[0].AsInteger := QrImpDOSCodigo.Value;
  QrLoc.Params[1].AsInteger := Query.FieldByName('CODIGOITEM').AsInteger;
  QrLoc.Open;
  //
  Dmod.QrUpd.SQL.Clear;
  if QrLoc.RecordCount > 0 then
    Dmod.QrUpd.SQL.Add('UPDATE impdosimp SET ')
  else
    Dmod.QrUpd.SQL.Add('INSERT INTO impdosimp SET ');
  Dmod.QrUpd.SQL.Add('Linha         =:P00');
  Dmod.QrUpd.SQL.Add(', Coluna      =:P01');
  Dmod.QrUpd.SQL.Add(', Letras      =:P02');
  Dmod.QrUpd.SQL.Add(', FoTam       =:P03');
  Dmod.QrUpd.SQL.Add(', Negrito     =:P04');
  Dmod.QrUpd.SQL.Add(', Italico     =:P05');
  Dmod.QrUpd.SQL.Add(', Prefixo     =:P06');
  Dmod.QrUpd.SQL.Add(', Sufixo      =:P07');
  Dmod.QrUpd.SQL.Add(', Padrao      =:P08');
  Dmod.QrUpd.SQL.Add(', Substituto  =:P09');
  Dmod.QrUpd.SQL.Add(', Substituir  =:P10');
  Dmod.QrUpd.SQL.Add(', Formato     =:P11');
  Dmod.QrUpd.SQL.Add(', Alinhamento =:P12');
  if QrLoc.RecordCount > 0 then
    Dmod.QrUpd.SQL.Add('WHERE Codigo=:Px AND Item=:Py')
  else
    Dmod.QrUpd.SQL.Add(', Codigo=:Px, Item=:Py');
  //
  Dmod.QrUpd.Params[00].AsInteger := Geral.IMV(EdLinha.Text);
  Dmod.QrUpd.Params[01].AsInteger := Geral.IMV(EdColuna.Text);
  Dmod.QrUpd.Params[02].AsInteger := Geral.IMV(EdLetras.Text);
  Dmod.QrUpd.Params[03].AsInteger := RGFonte.ItemIndex;
  Dmod.QrUpd.Params[04].AsInteger := Negrito;
  Dmod.QrUpd.Params[05].AsInteger := Italico;
  Dmod.QrUpd.Params[06].AsString  := EdPrefixo.Text;
  Dmod.QrUpd.Params[07].AsString  := EdSufixo.Text;
  Dmod.QrUpd.Params[08].AsString  := EdPadrao.Text;
  Dmod.QrUpd.Params[09].AsString  := EdSubstituto.Text;
  Dmod.QrUpd.Params[10].AsInteger := Substit;
  Dmod.QrUpd.Params[11].AsString  := RGFormato.Items[RGFormato.ItemIndex];
  Dmod.QrUpd.Params[12].AsInteger := RGAlinhamento.ItemIndex;
  //
  Dmod.QrUpd.Params[13].AsInteger := QrImpDOSCodigo.Value;
  Dmod.QrUpd.Params[14].AsInteger := Query.FieldByName('CODIGOITEM').AsInteger;
  Dmod.QrUpd.ExecSQL;
  //
  SubQuery1Reopen;
  if not FSequencia then EscondePainelItens
  else begin
    Item := Query.FieldByName('CODIGOITEM').AsInteger;
    Query.Next;
    if Item = Query.FieldByName('CODIGOITEM').AsInteger then FSequencia := False
    else AlteraSubRegistro;
  end;
end;

procedure TFmImpDOS.MostraPainelItens;
begin
  PainelItens.Visible := True;
  PainelDados.Visible := False;
end;

procedure TFmImpDOS.EscondePainelItens;
begin
  PainelDados.Visible := True;
  PainelItens.Visible := False;
end;

procedure TFmImpDOS.QrImpDOSImpCalcFields(DataSet: TDataSet);
begin
  case QrImpDOSImpTabela.Value of
    1: QrImpDOSImpNOMETABELA.Value := 'Geral';
    2: QrImpDOSImpNOMETABELA.Value := 'Cliente';
    3: QrImpDOSImpNOMETABELA.Value := 'Produtos';
    4: QrImpDOSImpNOMETABELA.Value := 'Servi�os';
    5: QrImpDOSImpNOMETABELA.Value := 'Faturamento';
    6: QrImpDOSImpNOMETABELA.Value := 'Transportadora';
    else QrImpDOSImpNOMETABELA.Value := '(N�O DEFINIDA)';
  end;
end;

procedure TFmImpDOS.SbImprimeClick(Sender: TObject);
var
  Texto, Linha, Num: String;
  i: Integer;
begin
  MyObjects.CriaForm_AcessoTotal(TFmDotPrint, FmDotPrint);
  FmDotPrint.RE1.Lines.Clear;
  //
  for i := 3 to 136 do
  begin
    Num := IntToStr(i);
    Linha := Linha + Copy(Num, Length(Num)-1, 1);
  end;
  for i := 1 to 63 do
  begin
    Texto := FormatFloat('00', i) + Linha;
    FmDotPrint.AdicionaLinhaTexto(Texto, [fdpCompress], []);
  end;
  FmDotPrint.ShowModal;
  FmDotPrint.Destroy;
end;

procedure TFmImpDOS.GradeADrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
begin
  if Column.FieldName = 'Negrito' then
    MeuVCLSkin.DrawGrid(GradeA, Rect, 1, QrANegrito.Value);
  if Column.FieldName = 'Italico' then
    MeuVCLSkin.DrawGrid(GradeA, Rect, 1, QrAItalico.Value);
  if Column.FieldName = 'Substituir' then
    MeuVCLSkin.DrawGrid(GradeA, Rect, 1, QrASubstituir.Value);
end;

procedure TFmImpDOS.GradeEDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
begin
  if Column.FieldName = 'Negrito' then
    MeuVCLSkin.DrawGrid(GradeE, Rect, 1, QrENegrito.Value);
  if Column.FieldName = 'Italico' then
    MeuVCLSkin.DrawGrid(GradeE, Rect, 1, QrEItalico.Value);
  if Column.FieldName = 'Substituir' then
    MeuVCLSkin.DrawGrid(GradeE, Rect, 1, QrESubstituir.Value);
end;

procedure TFmImpDOS.GradePDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
begin
  if Column.FieldName = 'Negrito' then
    MeuVCLSkin.DrawGrid(GradeP, Rect, 1, QrPNegrito.Value);
  if Column.FieldName = 'Italico' then
    MeuVCLSkin.DrawGrid(GradeP, Rect, 1, QrPItalico.Value);
  if Column.FieldName = 'Substituir' then
    MeuVCLSkin.DrawGrid(GradeP, Rect, 1, QrPSubstituir.Value);
end;

procedure TFmImpDOS.GradeSDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
begin
  if Column.FieldName = 'Negrito' then
    MeuVCLSkin.DrawGrid(GradeS, Rect, 1, QrSNegrito.Value);
  if Column.FieldName = 'Italico' then
    MeuVCLSkin.DrawGrid(GradeS, Rect, 1, QrSItalico.Value);
  if Column.FieldName = 'Substituir' then
    MeuVCLSkin.DrawGrid(GradeS, Rect, 1, QrSSubstituir.Value);
end;

procedure TFmImpDOS.GradeFDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
begin
  if Column.FieldName = 'Negrito' then
    MeuVCLSkin.DrawGrid(GradeF, Rect, 1, QrFNegrito.Value);
  if Column.FieldName = 'Italico' then
    MeuVCLSkin.DrawGrid(GradeF, Rect, 1, QrFItalico.Value);
  if Column.FieldName = 'Substituir' then
    MeuVCLSkin.DrawGrid(GradeF, Rect, 1, QrFSubstituir.Value);
end;

procedure TFmImpDOS.GradeTDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
begin
  if Column.FieldName = 'Negrito' then
    MeuVCLSkin.DrawGrid(GradeT, Rect, 1, QrTNegrito.Value);
  if Column.FieldName = 'Italico' then
    MeuVCLSkin.DrawGrid(GradeT, Rect, 1, QrTItalico.Value);
  if Column.FieldName = 'Substituir' then
    MeuVCLSkin.DrawGrid(GradeT, Rect, 1, QrTSubstituir.Value);
end;

procedure TFmImpDOS.DefineNomeTabela(Tabela: Integer);
begin
  case Tabela of
    1: EdTabela.Text := 'Geral';
    2: EdTabela.Text := 'Cliente';
    3: EdTabela.Text := 'Produtos';
    4: EdTabela.Text := 'Servi�os';
    5: EdTabela.Text := 'Faturamento';
    6: EdTabela.Text := 'Transportadora';
    else EdTabela.Text := '(N�O DEFINIDA)';
  end;
end;

end.

