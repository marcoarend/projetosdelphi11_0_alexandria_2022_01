unit Duplicata1;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ComCtrls, ExtCtrls, DBCtrls, Db, mySQLDbTables, Buttons, Mask,
  Menus, Grids, frxClass, dmkGeral, dmkDBLookupComboBox, dmkEdit, dmkEditCB,
  UnDmkProcFunc, UnDmkEnums, UnMyPrinters, DmkDAC_PF;

type
  TFmDuplicata1 = class(TForm)
    QrImprime: TmySQLQuery;
    DsImprime: TDataSource;
    QrImprimeCodigo: TIntegerField;
    QrImprimeNome: TWideStringField;
    QrItens: TmySQLQuery;
    DsItens: TDataSource;
    QrItensTOPO_A: TIntegerField;
    QrItensMESQ_A: TIntegerField;
    QrItensTOPO_B: TIntegerField;
    QrItensCodigo: TIntegerField;
    QrItensControle: TIntegerField;
    QrItensBand: TIntegerField;
    QrItensNome: TWideStringField;
    QrItensTab_Tipo: TIntegerField;
    QrItensTab_Campo: TIntegerField;
    QrItensPadrao: TWideStringField;
    QrItensPrefixo: TWideStringField;
    QrItensSufixo: TWideStringField;
    QrItensPos_Topo: TIntegerField;
    QrItensPos_Altu: TIntegerField;
    QrItensPos_Larg: TIntegerField;
    QrItensPos_MEsq: TIntegerField;
    QrItensAlt_Linha: TIntegerField;
    QrItensSet_TAli: TIntegerField;
    QrItensSet_BAli: TIntegerField;
    QrItensSet_Negr: TIntegerField;
    QrItensSet_Ital: TIntegerField;
    QrItensSet_Unde: TIntegerField;
    QrItensSet_Tam: TIntegerField;
    QrItensFormato: TIntegerField;
    QrItensSubstitui: TSmallintField;
    QrItensSubstituicao: TWideStringField;
    QrItensSet_T_DOS: TSmallintField;
    QrItensNulo: TSmallintField;
    QrItensLk: TIntegerField;
    QrItensDataCad: TDateField;
    QrItensDataAlt: TDateField;
    QrItensUserCad: TIntegerField;
    QrItensUserAlt: TIntegerField;
    QrItensTOPO: TIntegerField;
    QrItensMESQ: TIntegerField;
    QrImprimeNomeFonte: TWideStringField;
    DsSacado: TDataSource;
    QrSacado: TmySQLQuery;
    QrSacadoTEL_TXT: TWideStringField;
    QrSacadoCEL_TXT: TWideStringField;
    QrSacadoCPF_TXT: TWideStringField;
    QrSacadoFAX_TXT: TWideStringField;
    QrSacadoCEP_TXT: TWideStringField;
    QrSacadoNUMEROTXT: TWideStringField;
    QrSacadoENDERECO: TWideStringField;
    QrSacadoNOME: TWideStringField;
    QrSacadoRUA: TWideStringField;
    QrSacadoCOMPL: TWideStringField;
    QrSacadoBAIRRO: TWideStringField;
    QrSacadoCIDADE: TWideStringField;
    QrSacadoPAIS: TWideStringField;
    QrSacadoTELEFONE: TWideStringField;
    QrSacadoFAX: TWideStringField;
    QrSacadoCelular: TWideStringField;
    QrSacadoCNPJ: TWideStringField;
    QrSacadoIE: TWideStringField;
    QrSacadoContato: TWideStringField;
    QrSacadoNOMEUF: TWideStringField;
    QrSacadoEEMail: TWideStringField;
    QrSacadoPEmail: TWideStringField;
    QrSacadoLOGRAD: TWideStringField;
    QrSacadoCodigo: TIntegerField;
    Panel2: TPanel;
    Panel4: TPanel;
    BtImprime: TBitBtn;
    BtSaida: TBitBtn;
    CkTeste: TCheckBox;
    PMImprime: TPopupMenu;
    TintaLaser1: TMenuItem;
    Matricial1: TMenuItem;
    QrPages: TmySQLQuery;
    QrPagesPAGINAS: TIntegerField;
    QrImprimeMSupDOS: TIntegerField;
    QrImprimePos_MSup2: TIntegerField;
    QrImprimePos_MEsq: TIntegerField;
    QrView1: TmySQLQuery;
    QrView1Campo: TIntegerField;
    QrView1Valor: TWideStringField;
    QrView1Prefixo: TWideStringField;
    QrView1Sufixo: TWideStringField;
    QrView1Conta: TIntegerField;
    QrView1DBand: TIntegerField;
    QrView1DTopo: TIntegerField;
    QrView1DMEsq: TIntegerField;
    QrView1DComp: TIntegerField;
    QrView1DAltu: TIntegerField;
    QrView1Set_N: TIntegerField;
    QrView1Set_I: TIntegerField;
    QrView1Set_U: TIntegerField;
    QrView1Set_T: TIntegerField;
    QrView1Set_A: TIntegerField;
    QrView1Tipo: TIntegerField;
    QrView1Substituicao: TWideStringField;
    QrView1Substitui: TSmallintField;
    QrView1Nulo: TSmallintField;
    QrView1Forma: TIntegerField;
    QrView1Linha: TIntegerField;
    QrView1Colun: TIntegerField;
    QrView1Pagin: TIntegerField;
    QrTopos1: TmySQLQuery;
    QrTopos1DTopo: TIntegerField;
    QrTopos1Tipo: TIntegerField;
    QrImprimeBand: TmySQLQuery;
    QrImprimeBandCodigo: TIntegerField;
    QrImprimeBandControle: TIntegerField;
    QrImprimeBandTipo: TIntegerField;
    QrImprimeBandDataset: TIntegerField;
    QrImprimeBandPos_Topo: TIntegerField;
    QrImprimeBandPos_Altu: TIntegerField;
    QrImprimeBandNome: TWideStringField;
    QrImprimeBandLk: TIntegerField;
    QrImprimeBandDataCad: TDateField;
    QrImprimeBandDataAlt: TDateField;
    QrImprimeBandUserCad: TIntegerField;
    QrImprimeBandUserAlt: TIntegerField;
    QrImprimeBandCol_Qtd: TIntegerField;
    QrImprimeBandCol_Lar: TIntegerField;
    QrImprimeBandCol_GAP: TIntegerField;
    QrImprimeBandOrdem: TIntegerField;
    QrImprimeBandLinhas: TIntegerField;
    QrImprimeBandRepetencia: TSmallintField;
    DsImprimeBand: TDataSource;
    QrItensA: TmySQLQuery;
    QrItensATOPO_A: TIntegerField;
    QrItensAMESQ_A: TIntegerField;
    QrItensATOPO_B: TIntegerField;
    QrItensACodigo: TIntegerField;
    QrItensAControle: TIntegerField;
    QrItensABand: TIntegerField;
    QrItensANome: TWideStringField;
    QrItensATab_Tipo: TIntegerField;
    QrItensATab_Campo: TIntegerField;
    QrItensAPadrao: TWideStringField;
    QrItensAPrefixo: TWideStringField;
    QrItensASufixo: TWideStringField;
    QrItensAPos_Topo: TIntegerField;
    QrItensAPos_Altu: TIntegerField;
    QrItensAPos_Larg: TIntegerField;
    QrItensAPos_MEsq: TIntegerField;
    QrItensAAlt_Linha: TIntegerField;
    QrItensASet_TAli: TIntegerField;
    QrItensASet_BAli: TIntegerField;
    QrItensASet_Negr: TIntegerField;
    QrItensASet_Ital: TIntegerField;
    QrItensASet_Unde: TIntegerField;
    QrItensASet_Tam: TIntegerField;
    QrItensAFormato: TIntegerField;
    QrItensASubstitui: TSmallintField;
    QrItensASubstituicao: TWideStringField;
    QrItensASet_T_DOS: TSmallintField;
    QrItensANulo: TSmallintField;
    QrItensALk: TIntegerField;
    QrItensADataCad: TDateField;
    QrItensADataAlt: TDateField;
    QrItensAUserCad: TIntegerField;
    QrItensAUserAlt: TIntegerField;
    QrItensATOPO: TIntegerField;
    QrItensAMESQ: TIntegerField;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    Panel5: TPanel;
    Label1: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    GroupBox3: TGroupBox;
    MeExtenso: TMemo;
    StaticText1: TStaticText;
    StaticText2: TStaticText;
    StaticText3: TStaticText;
    TPEmissao: TDateTimePicker;
    EdValorF: TdmkEdit;
    EdNumeroF: TdmkEdit;
    EdValorD: TdmkEdit;
    EdNumeroD: TdmkEdit;
    TPVencimento: TDateTimePicker;
    GroupBox1: TGroupBox;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    Label18: TLabel;
    EdNomeSacado: TDBEdit;
    EdEndereco: TDBEdit;
    EdCidade: TDBEdit;
    EdCNPJ: TDBEdit;
    EdUF: TDBEdit;
    EdCEP: TDBEdit;
    EdIE: TDBEdit;
    GroupBox2: TGroupBox;
    Label12: TLabel;
    Label15: TLabel;
    Label20: TLabel;
    EdDescoP: TdmkEdit;
    EdDescoV: TdmkEdit;
    TPAte: TDateTimePicker;
    EdCondEsp: TdmkEdit;
    CkVenctoDesc: TCheckBox;
    Panel3: TPanel;
    Label11: TLabel;
    Label2: TLabel;
    EdImprime: TdmkEditCB;
    CBImprime: TdmkDBLookupComboBox;
    EdSacado: TdmkEditCB;
    CBSacado: TdmkDBLookupComboBox;
    SG1: TStringGrid;
    RGVcto: TRadioGroup;
    Label7: TLabel;
    frxCompleta1: TfrxReport;
    N1: TMenuItem;
    Completa1: TMenuItem;
    frxCompleta2: TfrxReport;
    N1via1: TMenuItem;
    N2vias1: TMenuItem;
    SpeedButton1: TSpeedButton;
    EdPraca: TdmkEdit;
    Label19: TLabel;
    SpeedButton2: TSpeedButton;
    QrSacadoNUMERO: TFloatField;
    QrSacadoCEP: TFloatField;
    QrSacadoUF: TFloatField;
    procedure FormCreate(Sender: TObject);
    procedure BtImprimeClick(Sender: TObject);
    procedure QrItensCalcFields(DataSet: TDataSet);
    procedure BtSaidaClick(Sender: TObject);
    procedure QrSacadoCalcFields(DataSet: TDataSet);
    procedure EdValorDExit(Sender: TObject);
    procedure EdValorDChange(Sender: TObject);
    procedure EdDescoPExit(Sender: TObject);
    procedure EdDescoVExit(Sender: TObject);
    procedure EdDescoPChange(Sender: TObject);
    procedure EdDescoVChange(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure TintaLaser1Click(Sender: TObject);
    procedure Matricial1Click(Sender: TObject);
    procedure QrItensACalcFields(DataSet: TDataSet);
    procedure frxCompleta1GetValue(const VarName: String;
      var Value: Variant);
    procedure RGVctoClick(Sender: TObject);
    procedure N1via1Click(Sender: TObject);
    procedure N2vias1Click(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure EdCidadeChange(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
  private
    { Private declarations }
    FDiaEmissao, FMesEmissao, FAnoEmissao: Word;
    FCalc: Integer;
    FVencimento, FDesconto, FVenctoDesc: Double;
    FArqPrn: TextFile;
    FTamLinha: Integer;
    FPrintedLine: Boolean;
    FPagina: Integer;
    procedure CalculaDesconto;
    function TextoAImprimir(Campo: Integer): String;
    function DataTexto(Data: TDateTime; Calcula, UsaData: Double; Texto:
              String): String;
    //procedure ImprimeTintaLaser;
    procedure ImprimeMatricial;
    function AvancaCarro(PosicaoAtual, NovaPosicao: Integer): Integer;
    function EspacoEntreTextos(QrView: TmySQLQuery): String;
    function Formata(Texto, Prefixo, Sufixo: String; Formato: Integer; Nulo:
             Boolean): String;
    function FormataTexto(Texto, Prefixo, Sufixo, Substituicao: String;
             Substitui, Alinhamento, Comprimento, Fonte, Formatacao: Integer;
             Nulo: Boolean): String;
    procedure CalculaItensA;
  public
    { Public declarations }
    FFoiExpand,
    FCPI: Integer;
  end;

var
  FmDuplicata1: TFmDuplicata1;

implementation

{$R *.DFM}

uses UnMyObjects, UnInternalConsts, Module, ModuleGeral, Imprime, MyDBCheck,
  UCreate;

const
  FAltLin = CO_POLEGADA / 2.16;

function TFmDuplicata1.Formata(Texto, Prefixo, Sufixo: String; Formato: Integer;
 Nulo: Boolean): String;
var
  MeuTexto: String;
begin
  if Trim(Texto) = '' then MeuTexto := Texto else
  begin
    case Formato of
      1: MeuTexto := Geral.TFT(Texto, 0, siNegativo);
      2: MeuTexto := Geral.TFT(Texto, 1, siNegativo);
      3: MeuTexto := Geral.TFT(Texto, 2, siNegativo);
      4: MeuTexto := Geral.TFT(Texto, 3, siNegativo);
      5: MeuTexto := Geral.TFT(Texto, 4, siNegativo);
      6: MeuTexto := FormatDateTime(VAR_FORMATDATE3, StrToDate(Texto));
      7: MeuTexto := FormatDateTime(VAR_FORMATDATE2, StrToDate(Texto));
      8: MeuTexto := FormatDateTime(VAR_FORMATTIME2, StrToTime(Texto));
      9: MeuTexto := FormatDateTime(VAR_FORMATTIME , StrToTime(Texto));
      else MeuTexto := Texto;
    end;
  end;
  if (Formato in ([1,2,3,4,5])) and Nulo then
  begin
    if Geral.DMV(MeuTexto) < 0.0001 then MeuTexto := '';
  end;
  if MeuTexto = '' then Result := '' else
  begin
    if Prefixo<> '' then Prefixo := Prefixo + ' ';
    if Sufixo<> '' then Sufixo := Sufixo + ' ';
    Result := Prefixo+MeuTexto+Sufixo;
  end;
end;

function TFmDuplicata1.FormataTexto(Texto, Prefixo, Sufixo, Substituicao: String;
  Substitui, Alinhamento, Comprimento, Fonte, Formatacao: Integer; Nulo:
  Boolean): String;
var
  i, Tam, Letras, CPI, Vazios: Integer;
  Esq: Boolean;
  Txt, Vaz: String;
  Increm: Double;
begin
  if Substitui = 1 then Txt := Substituicao else
  begin
    if Texto = '' then Txt := ''
    else Txt := Formata(Texto, Prefixo, Sufixo, Formatacao, Nulo);
  end;
  if Txt = '' then
  Txt := ' '; // evitar erro de Margem Esquerda ao imprimir ??
  if FCPI = 0 then CPI := 10 else CPI := FCPI;
  Tam := Fonte + (CPI*10);
  Letras := 0;
  case Tam of
    100: Letras := 17;
    101: Letras := 10;
    102: Letras := 05;
    120: Letras := 20;
    121: Letras := 12;
    122: Letras := 06;
    else Geral.MensagemBox('CPI indefinido!', 'Erro', MB_OK+MB_ICONERROR);
  end;
  Increm := (CO_POLEGADA * 100) / Letras;
  Letras := Trunc(((Comprimento / 100) / CO_POLEGADA) * Letras);
  FTamLinha := FTamLinha + Trunc(Increm * Letras);
  Vazios := Letras - Length(Txt);
  Esq := True;
  // [0: Esq] [1: Centro] [2: Direita]
  if Alinhamento = 1 then
  begin
    for i := 1 to Vazios do
    begin
      if Esq then
      begin
        Txt := Txt + ' ';
        Esq := False;
      end else begin
        Txt := ' ' + Txt;
        Esq := True;
      end;
    end;
  end else begin
    for i := 1 to Vazios do Vaz := Vaz + ' ';
    if Alinhamento = 0 then Txt := Txt + Vaz else Txt := Vaz + Txt;
  end;
  Result := Txt;
end;

procedure TFmDuplicata1.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmDuplicata1.FormCreate(Sender: TObject);
begin
  UnDmkDAC_PF.AbreQuery(QrImprime, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrSacado, Dmod.MyDB);
  TPEmissao.Date := Date;
  TPVencimento.Date := Date;
  TPAte.Date := Date;
  PageControl1.ActivePageIndex := 0;
end;

procedure TFmDuplicata1.BtImprimeClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMImprime, BtImprime);
end;

procedure TFmDuplicata1.QrItensCalcFields(DataSet: TDataSet);
begin
  QrItensTOPO.Value := QrItensTOPO_A.Value+QrItensTOPO_B.Value+QrItensPos_Topo.Value;
  QrItensMESQ.Value := QrItensMESQ_A.Value+                    QrItensPos_MEsq.Value;
  //
end;

procedure TFmDuplicata1.QrSacadoCalcFields(DataSet: TDataSet);
var
  Endereco: String;
begin
  QrSacadoTEL_TXT.Value := Geral.FormataTelefone_TT(QrSacadoTelefone.Value);
  QrSacadoFAX_TXT.Value := Geral.FormataTelefone_TT(QrSacadoFax.Value);
  QrSacadoCEL_TXT.Value := Geral.FormataTelefone_TT(QrSacadoCelular.Value);
  QrSacadoCPF_TXT.Value := Geral.FormataCNPJ_TT(QrSacadoCNPJ.Value);
  QrSacadoCEP_TXT.Value := Geral.FormataCEP_TT(FloatToStr(QrSacadoCEP.Value));
  //
  Endereco := QrSacadoRUA.Value;
  if Trim(QrSacadoLOGRAD.Value) <> '' then
  begin
    if Uppercase(Copy(QrSacadoRUA.Value, 1, Length(QrSacadoLOGRAD.Value))) <>
      Uppercase(QrSacadoLOGRAD.Value) then
    Endereco := QrSacadoLOGRAD.Value + ' ' + Endereco;
  end;
  //
  if QrSacadoNUMERO.Value = 0 then
    QrSacadoNUMEROTXT.Value := 'S/N' else
    QrSacadoNUMEROTXT.Value :=
    FloatToStr(QrSacadoNUMERO.Value);
  QrSacadoENDERECO.Value :=
    Endereco + ', '+
    QrSacadoNUMEROTXT.Value + '  '+
    QrSacadoCOMPL.Value;
end;

procedure TFmDuplicata1.EdValorDExit(Sender: TObject);
begin
  MeExtenso.Text := dmkPF.ExtensoMoney(EdValorD.Text);
  CalculaDesconto;
end;

procedure TFmDuplicata1.EdValorDChange(Sender: TObject);
begin
  MeExtenso.Text := dmkPF.ExtensoMoney(Geral.TFT(EdValorD.Text, 2, siPositivo));
end;

procedure TFmDuplicata1.EdDescoPExit(Sender: TObject);
begin
  CalculaDesconto;
end;

procedure TFmDuplicata1.EdDescoVExit(Sender: TObject);
begin
  CalculaDesconto;
end;

procedure TFmDuplicata1.EdDescoPChange(Sender: TObject);
begin
  if EdDescoP.Focused then FCalc := 0;
end;

procedure TFmDuplicata1.EdDescoVChange(Sender: TObject);
begin
  if EdDescoV.Focused then FCalc := 1;
end;

procedure TFmDuplicata1.CalculaDesconto;
var
  Va, DP, DV: Double;
  TP, TV: String;
begin
  Va := Geral.DMV(EdValorD.Text);
  DP := Geral.DMV(EdDescoP.Text);
  DV := Geral.DMV(EdDescoV.Text);
  //
  if FCalc = 0 then DV := VA * DP / 100
  else if Va = 0 then DP := 0
  else DP := DV / Va * 100;
  if DV > 0.009999 then TV := Geral.FFT(DV, 2, siPositivo) else TV := '';
  if DP > 0.009999 then TP := Geral.FFT(DP, 2, siPositivo) else TP := '';
  EdDescoV.Text := TV;
  EdDescoP.Text := TP;
end;

procedure TFmDuplicata1.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
end;

function TFmDuplicata1.DataTexto(Data: TDateTime; Calcula, UsaData: Double;
  Texto: String): String;
begin
  if Calcula > 0.00009 then
  begin
    case Trunc(UsaData) of
      0: Result := '� vista';
      1: Result := 'c/apres';
      else  Result := FormatDateTime(VAR_FORMATDATE2, Data);
    end;
  end else Result := '';
end;

function TFmDuplicata1.TextoAImprimir(Campo: Integer): String;
begin
  case Campo of
    00: Result := FormatFloat('00', FDiaEmissao);
    01: Result := dmkPF.VerificaMes(FMesEmissao, False);
    02: Result := Copy(FormatFloat('0000', FAnoEmissao), 3, 2);
    03: Result := EdValorF.Text;
    04: Result := EdNumeroF.Text;
    05: Result := EdValorD.Text;
    06: Result := EdNumeroD.Text;
    07: Result := DataTexto(TPVencimento.Date, 1, FVencimento, 'c/apres');
    08: Result := EdDescoP.Text;
    09: Result := EdDescoV.Text;
    10: Result := DataTexto(TPAte.Date, FDesconto, FVenctoDesc, 'c/apres');
    11: Result := EdCondEsp.Text;
    12: Result := QrSacadoNOME.Value;
    13: Result := QrSacadoENDERECO.Value;
    14: Result := QrSacadoCIDADE.Value;
    15: Result := QrSacadoNOMEUF.Value;
    16: Result := ''; // P�a. Pgto.
    17: Result := QrSacadoCEP_TXT.Value;
    18: Result := QrSacadoCPF_TXT.Value;
    19: Result := QrSacadoIE.Value;
    20: Result := MeExtenso.Text;
    else Result := '***'
  end;
end;

procedure TFmDuplicata1.TintaLaser1Click(Sender: TObject);
begin
  //ImprimeTintaLaser;
end;

{
procedure TFmDuplicata1.ImprimeTintaLaser;
var
  v: TfrView;
  //b: TfrBandView;
  //p: TfrPictureView;
  //r: TfrRichView;
  Page: TfrPage;
  Align, L, T, W, H: Integer;
begin
  Decodedate(TPEmissao.Date, FAnoEmissao, FMesEmissao, FDiaEmissao);
  FVencimento := RGVcto.ItemIndex;
  FVenctoDesc := Geral.BoolToInt(CkVenctoDesc.Checked)+1;
  FDesconto   := Geral.DMV(EdDescoP.Text)+Geral.DMV(EdDescoV.Text);
  //
  frCarta.Pages.Clear;
  frCarta.Pages.Add;
  Page := frCarta.Pages[0];
  //////////////////////////////////////////////////////////////////////////////
  QrItens.Close;
  QrItens.Params[0].AsInteger := QrImprimeCodigo.Value;
  UnDmkDAC_PF.AbreQuery(QrItens, Dmod.MyDB);
  //
  while not QrItens.Eof do
  begin
    L := Trunc(QrItensMESQ.Value     / VAR_DOTIMP);
    T := Trunc(QrItensTOPO.Value     / VAR_DOTIMP);
    W := Trunc(QrItensPos_Larg.Value / VAR_DOTIMP);
    H := Trunc(QrItensPos_Altu.Value / VAR_DOTIMP);
    //
    v := TfrMemoView.Create;
    v.SetBounds(L, T, W, H); // (Left, Top, Width, Height: Integer);
    //
      Align := (QrItensSet_TAli.Value * 100) + QrItensSet_BAli.Value;
      case Align of
        000: v.Prop['Alignment'] := frtaLeft;   //'Esquerda no topo';
        001: v.Prop['Alignment'] := frtaLeft + frtaMiddle;     //'Esquerda no centro';
        002: v.Prop['Alignment'] := frtaLeft + frtaDown;       //'Esquerda em baixo';
        100: v.Prop['Alignment'] := frtaCenter; //'Centro no topo';
        101: v.Prop['Alignment'] := frtaCenter + frtaMiddle;   //'Centralizado';
        102: v.Prop['Alignment'] := frtaCenter + frtaDown;     //'Centro em baixo';
        200: v.Prop['Alignment'] := frtaRight;   //'Direita no topo';
        201: v.Prop['Alignment'] := frtaRight + frtaMiddle;    //'Direita no centro';
        202: v.Prop['Alignment'] := frtaRight + frtaDown;      //'Direita em baixo';
      end;
    //
    v.Prop['Font.Name'] := QrImprimeNomeFonte.Value;
    v.Prop['Font.Size'] := QrItensSet_Tam.Value;
    v.Memo.Add(TextoAImprimir(QrItensTab_Campo.Value));
    if CkTeste.Checked then v.Prop['FrameTyp'] := 15;
    Page.Objects.Add(v);
    QrItens.Next;
  end;
  frCarta.PrepareReport;
  frCarta.ShowPreparedReport;
end;
}

function TFmDuplicata1.AvancaCarro(PosicaoAtual, NovaPosicao: Integer): Integer;
var
  LinAtu: Integer;
begin
  NovaPosicao := Trunc(NovaPosicao * 250 / 90);
  if PosicaoAtual < NovaPosicao then
  begin
    LinAtu := Trunc((NovaPosicao - PosicaoAtual) / FAltLin);
    while LinAtu > 0 do
    begin
      if LinAtu > 216 then
      begin
        Write(FArqPrn, #27+'3'+#216);
        LinAtu := LinAtu - 216;
        WriteLn(FArqPrn,' ');
      end else begin
        Write(FArqPrn, MyPrinters.DefAlturaDotImp(3, LinAtu, ''));
        WriteLn(FArqPrn,' ');
        LinAtu := 0;
      end;
    end;
  end;
  Result := NovaPosicao;
  FPrintedLine := False;
end;

function TFmDuplicata1.EspacoEntreTextos(QrView: TmySQLQuery): String;
var
  Texto: String;
  i, CPI, Letras: Integer;
  Soma: Double;
begin
  Texto := '';
  if FTamLinha > QrView.FieldByName('DMEsq').AsInteger then
  begin
    // Erro !!!
  end;
  if FTamLinha < QrView.FieldByName('DMEsq').AsInteger then
  begin
    Texto := #15;
    if FCPI = 0 then CPI := 10 else CPI := FCPI;
    CPI := CPI + FFoiExpand;
    case CPI of
     010: CPI := 17;
     012: CPI := 20;
     110: CPI := 10;
     112: CPI := 10;
    end;
    Soma := (CO_POLEGADA * 100)/ CPI;
    Letras := Trunc((QrView.FieldByName('DMEsq').AsInteger - FTamLinha) / (CO_POLEGADA * 100) * CPI);
    for i := 1 to Letras do Texto := Texto + ' ';
    FTamLinha := FTamLinha + Trunc(Soma * Letras);
  end;
  Result := Texto;
end;

procedure TFmDuplicata1.Matricial1Click(Sender: TObject);
begin
  Decodedate(TPEmissao.Date, FAnoEmissao, FMesEmissao, FDiaEmissao);
  FVencimento := RGVcto.ItemIndex;
  FVenctoDesc := Geral.BoolToInt(CkVenctoDesc.Checked);
  FDesconto   := Geral.DMV(EdDescoP.Text)+Geral.DMV(EdDescoV.Text);
  //
  FPagina := 1;

  UCriar.RecriaTempTable('imprimir1', DModG.QrUpdPID1, False);
  DModG.QrUpdPID1.ExecSQL;
  //
  DModG.QrUpdPID1.SQL.Clear;
  DModG.QrUpdPID1.SQL.Add('INSERT INTO imprimir1 SET ');
  DModG.QrUpdPID1.SQL.Add('Valor=:P0, Prefixo=:P1, Sufixo=:P2, ');
  DModG.QrUpdPID1.SQL.Add('DTopo=:P3, DMEsq=:P4, DComp=:P5, DAltu=:P6, ');
  DModG.QrUpdPID1.SQL.Add('DBand=:P7, Set_N=:P8, Set_I=:P9, ');
  DModG.QrUpdPID1.SQL.Add('Set_U=:P10, Set_T=:P11, Set_A=:P12, ');
  DModG.QrUpdPID1.SQL.Add('Forma=:P13, Substitui=:P14, ');
  DModG.QrUpdPID1.SQL.Add('Substituicao=:P15, Nulo=:P16, ');
  DModG.QrUpdPID1.SQL.Add('Campo=:P17, Tipo=:P18, Conta=:P19, Pagin=:P20');
  CalculaItensA;
  //
  ImprimeMatricial;
end;

procedure TFmDuplicata1.ImprimeMatricial;
var
  i, BTopo, LinhaAtual, MSupDOS, Substitui: Integer;
  Nulo: Boolean;
  Espaco, MyCPI, Linha, FormataA, FormataI, FormataNI, FormataNF, Texto: String;
begin
  Screen.Cursor := crHourGlass;
  try
    AssignFile(FArqPrn, 'LPT1:');
    ReWrite(FArqPrn);
    Write(FArqPrn, #13);
    //Partes := #27+'3'+#1;
    //Write(FArqPrn, Partes);
    if FCPI = 12 then MyCPI := 'M' else MyCPI := 'P';
    Write(FArqPrn, #27+MyCPI);
    FormataA  := #15;
    FormataI  := #15;
    FormataNI := #15;
    FormataNF := #15;
    //
    QrPages.Close;
    UnDmkDAC_PF.AbreQuery(QrPages, Dmod.MyDB);
    //TamVazio := FCPI;
    MSupDOS := QrImprimeMSupDOS.Value;
    for i := 0 to QrPagesPAGINAS.Value-1 do
    begin
      if (i < QrPagesPAGINAS.Value-1) then Substitui := 1 else Substitui := 0;
      LinhaAtual := 0;
      if i = 0 then
      begin
        if MSupDos < 0 then LinhaAtual := LinhaAtual - MSupDos else
          LinhaAtual := AvancaCarro(LinhaAtual, MSupDos);
      end;
      QrImprimeBand.Close;
      QrImprimeBand.Params[0].AsInteger := QrImprimeCodigo.Value;
      UnDmkDAC_PF.AbreQuery(QrImprimeBand, Dmod.MyDB);
      QrImprimeBand.First;
      while not QrImprimeBand.Eof do
      begin
        BTopo := QrImprimeBandPos_Topo.Value;
        if i > 0 then BTopo := BTopo + QrImprimePos_MSup2.Value;
           //Altura da banda
        //+ (QrImprimePos_Altu.Value * i);
        QrTopos1.Close;
        QrTopos1.Params[0].AsInteger := QrImprimeBandControle.Value;
        UnDmkDAC_PF.AbreQuery(QrTopos1, Dmod.MyDB);
        QrTopos1.First;
        //if QrTopos1Tipo.Value in ([0,1,5]) then
        //begin
          while not QrTopos1.Eof do
          begin
            LinhaAtual := AvancaCarro(LinhaAtual, QrTopos1DTopo.Value+BTopo);
            if not FPrintedLine then
            begin
              QrView1.Close;
              QrView1.Params[0].AsInteger := QrTopos1DTopo.Value;
              QrView1.Params[1].AsInteger := QrImprimeBandControle.Value;
              UnDmkDAC_PF.AbreQuery(QrView1, Dmod.MyDB);
              QrView1.First;
              Linha := '';
              FTamLinha  := 0;
              FFoiExpand := 0;
              while not QrView1.Eof do
                begin
                case QrView1Set_T.Value of
                  0: FormataA := #15;
                  1: FormataA := #18;
                  2: FormataA := #18#14;
                end;
                Espaco := EspacoEntreTextos(QrView1);
                if QrView1Nulo.Value = 1 then Nulo := True else Nulo := False;
                //if QrView1Valor.Value = '' then Texto := '' else
                  //Texto := Formata(QrView1Valor.Value, QrView1Prefixo.Value,
                  //QrView1Sufixo.Value, QrView1Forma.Value, Nulo);
                  Texto := FormataTexto(QrView1Valor.Value, QrView1Prefixo.Value,
                  QrView1Sufixo.Value, QrView1Substituicao.Value, Substitui *
                  QrView1Substitui.Value, QrView1Set_A.Value, QrView1DComp.Value,
                  QrView1Set_T.Value, QrView1Forma.Value, Nulo);
                //
                Linha := Linha + Espaco + FormataA + Texto;
                // J� usou expandido na mesma linha
                if QrView1Set_T.Value = 2 then FFoiExpand := 100;
                QrView1.Next;
              end;
              Write(FarqPrn, #27+'3'+#0);
              //Write(FarqPrn, #27+'F');
              WriteLn(FArqPrn, Linha);
              FPrintedLine := True;
            end;
            QrTopos1.Next;
          end;
        QrImprimeBand.Next;
      end;
      //LinhaAtual := AvancaCarro(LinhaAtual, QrImprimePos_Altu.Value);
      //Write(FArqPrn, '_____________-------------------_________________---------------____________');
    end;
    Write(FArqPrn, #13);
    WriteLn(FArqPrn, #27+'0');
    //if CkEjeta.Checked then
    Write(FArqPrn, #12);
    // n�o fazer para imprimir v�rios???
    //for i := 1 to 13 do Write(FArqPrn, sLineBreak);
    CloseFile(FArqPrn);
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmDuplicata1.EdCidadeChange(Sender: TObject);
begin
  EdPraca.Text := EdCidade.Text;
end;

procedure TFmDuplicata1.CalculaItensA;
var
  L, T, W, H, A, M, R: Integer;
  //
  Texto: String;
  i, j, LinIni, Maxlin, LinAtu, Conta: Integer;
begin
  M := 0;
  LinAtu := 0;
  QrItensA.Close;
  QrItensA.Params[0].AsInteger := QrImprimeCodigo.Value;
  UnDmkDAC_PF.AbreQuery(QrItensA, Dmod.MyDB);
  //
  //SG1.RowCount := QrItensA.RecordCount+1;
  while not QrItensA.Eof do
  begin
    L := Trunc(QrItensAMESQ.Value      / VAR_DOTIMP);
    T := Trunc(QrItensATOPO.Value      / VAR_DOTIMP);
    W := Trunc(QrItensAPos_Larg.Value  / VAR_DOTIMP);
    H := Trunc(QrItensAPos_Altu.Value  / VAR_DOTIMP);
    R := Trunc(QrItensAAlt_Linha.Value / VAR_DOTIMP);
    //
    if QrItensATab_Campo.Value = 20 then // Valor por Extenso
    begin
      case QrItensASet_BAli.Value of
        0: M := 0;
        1: M := Trunc((QrItensAAlt_Linha.Value - CO_AltLetraDOS) / 2);
        2: M := QrItensAAlt_Linha.Value - CO_AltLetraDOS;
      end;
      if M < 0 then M := 0;
      M := Trunc(M / VAR_DOTIMP);

      MaxLin := Trunc(Int(QrItensAPos_Altu.Value / QrItensAAlt_Linha.Value));
      if MeExtenso.Lines.Count > MaxLin then
        Geral.MensagemBox('Valor por extenso excedeu o limite de '+
        'linhas!', 'Aviso', MB_OK+MB_ICONWARNING);
      LinIni := ((MaxLin - MeExtenso.Lines.Count) div 2) + 1;
      if LinIni < 1 then LinIni := 1;
      Conta := 0;
      for i := LinIni to LinIni+MeExtenso.Lines.Count do
      begin
        LinAtu := LinAtu +1;
        if LinAtu > 2 then SG1.RowCount := LinAtu;
        SG1.Cells[0,LinAtu] := IntToStr(LinAtu);
        SG1.Cells[1,LinAtu] := IntToStr(L);
        SG1.Cells[2,LinAtu] := IntToStr(T);
        SG1.Cells[3,LinAtu] := IntToStr(W);
        SG1.Cells[4,LinAtu] := IntToStr(H);
        //
        A := T + M + (R *(LinIni+Conta));
        SG1.Cells[5,LinAtu] := IntToStr(A+M);
        SG1.Cells[6,LinAtu] := IntToStr(QrItensASet_TAli.Value);
        SG1.Cells[7,LinAtu] := IntToStr(QrItensASet_BAli.Value);
        SG1.Cells[8,LinAtu] := IntToStr(QrItensASet_Tam.Value);
        SG1.Cells[9,LinAtu] := MeExtenso.Lines[i-LinIni];
        //
        Texto := TextoAImprimir(QrItensATab_Campo.Value);
        for j := 1 to FPagina do
        begin
          if Trim(Texto) <> '' then
          begin
            DModG.QrUpdPID1.Params[00].AsString  := MeExtenso.Lines[i-LinIni];
            DModG.QrUpdPID1.Params[01].AsString  := QrItensAPrefixo.Value;
            DModG.QrUpdPID1.Params[02].AsString  := QrItensASufixo.Value;
            DModG.QrUpdPID1.Params[03].AsInteger := A;
            DModG.QrUpdPID1.Params[04].AsInteger := QrItensAPos_MEsq.Value+QrImprimePos_MEsq.Value;
            DModG.QrUpdPID1.Params[05].AsInteger := QrItensAPos_Larg.Value;
            DModG.QrUpdPID1.Params[06].AsInteger := QrItensAPos_Altu.Value;
            DModG.QrUpdPID1.Params[07].AsInteger := QrItensABand.Value;
            DModG.QrUpdPID1.Params[08].AsInteger := QrItensASet_Negr.Value;
            DModG.QrUpdPID1.Params[09].AsInteger := QrItensASet_Ital.Value;
            DModG.QrUpdPID1.Params[10].AsInteger := QrItensASet_Unde.Value;
            DModG.QrUpdPID1.Params[11].AsInteger := QrItensASet_T_DOS.Value;
            DModG.QrUpdPID1.Params[12].AsInteger := QrItensASet_TAli.Value;
            DModG.QrUpdPID1.Params[13].AsInteger := QrItensAFormato.Value;
            DModG.QrUpdPID1.Params[14].AsInteger := QrItensASubstitui.Value;
            DModG.QrUpdPID1.Params[15].AsString  := QrItensASubstituicao.Value;
            DModG.QrUpdPID1.Params[16].AsInteger := QrItensANulo.Value;
            DModG.QrUpdPID1.Params[17].AsInteger := QrItensAControle.Value;
            DModG.QrUpdPID1.Params[18].AsInteger := QrItensATab_Tipo.Value;
            DModG.QrUpdPID1.Params[19].AsInteger := LinAtu;
            DModG.QrUpdPID1.Params[20].AsInteger := j;
            DModG.QrUpdPID1.ExecSQL;
          end;
          //
          Conta := Conta + 1;
        end;
      end;
    end else begin
      case QrItensASet_BAli.Value of
        0: M := 0;
        1: M := Trunc((QrItensAPos_Altu.Value - CO_AltLetraDOS) / 2);
        2: M := QrItensAPos_Altu.Value - CO_AltLetraDOS;
      end;
      if M < 0 then M := 0;
      M := Trunc(M / VAR_DOTIMP);
      LinAtu := LinAtu +1;
      if LinAtu > 2 then SG1.RowCount := LinAtu;
      SG1.Cells[0,LinAtu] := IntToStr(LinAtu);
      SG1.Cells[1,LinAtu] := IntToStr(L);
      SG1.Cells[2,LinAtu] := IntToStr(T);
      SG1.Cells[3,LinAtu] := IntToStr(W);
      SG1.Cells[4,LinAtu] := IntToStr(H);
      //
      A := T + M;
      SG1.Cells[5,LinAtu] := IntToStr(A);
      SG1.Cells[6,LinAtu] := IntToStr(QrItensASet_TAli.Value);
      SG1.Cells[7,LinAtu] := IntToStr(QrItensASet_BAli.Value);
      SG1.Cells[8,LinAtu] := IntToStr(QrItensASet_Tam.Value);
      SG1.Cells[9,LinAtu] := TextoAImprimir(QrItensATab_Campo.Value);
      //
      Texto := TextoAImprimir(QrItensATab_Campo.Value);
      for j := 1 to FPagina do
      begin
        if Trim(Texto) <> '' then
        begin
          DModG.QrUpdPID1.Params[00].AsString  := TextoAImprimir(QrItensATab_Campo.Value);
          DModG.QrUpdPID1.Params[01].AsString  := QrItensAPrefixo.Value;
          DModG.QrUpdPID1.Params[02].AsString  := QrItensASufixo.Value;
          DModG.QrUpdPID1.Params[03].AsInteger := A;
          DModG.QrUpdPID1.Params[04].AsInteger := QrItensAPos_MEsq.Value+QrImprimePos_MEsq.Value;
          DModG.QrUpdPID1.Params[05].AsInteger := QrItensAPos_Larg.Value;
          DModG.QrUpdPID1.Params[06].AsInteger := QrItensAPos_Altu.Value;
          DModG.QrUpdPID1.Params[07].AsInteger := QrItensABand.Value;
          DModG.QrUpdPID1.Params[08].AsInteger := QrItensASet_Negr.Value;
          DModG.QrUpdPID1.Params[09].AsInteger := QrItensASet_Ital.Value;
          DModG.QrUpdPID1.Params[10].AsInteger := QrItensASet_Unde.Value;
          DModG.QrUpdPID1.Params[11].AsInteger := QrItensASet_T_DOS.Value;
          DModG.QrUpdPID1.Params[12].AsInteger := QrItensASet_TAli.Value;
          DModG.QrUpdPID1.Params[13].AsInteger := QrItensAFormato.Value;
          DModG.QrUpdPID1.Params[14].AsInteger := QrItensASubstitui.Value;
          DModG.QrUpdPID1.Params[15].AsString  := QrItensASubstituicao.Value;
          DModG.QrUpdPID1.Params[16].AsInteger := QrItensANulo.Value;
          DModG.QrUpdPID1.Params[17].AsInteger := QrItensAControle.Value;
          DModG.QrUpdPID1.Params[18].AsInteger := QrItensATab_Tipo.Value;
          DModG.QrUpdPID1.Params[19].AsInteger := LinAtu;
          DModG.QrUpdPID1.Params[20].AsInteger := FPagina;
          DModG.QrUpdPID1.ExecSQL;
        end;
      end;
      //
    end;
    QrItensA.Next;
  end;
end;

procedure TFmDuplicata1.QrItensACalcFields(DataSet: TDataSet);
begin
  QrItensATOPO.Value := QrItensATOPO_A.Value+QrItensATOPO_B.Value+QrItensAPos_Topo.Value;
  QrItensAMESQ.Value := QrItensAMESQ_A.Value+                    QrItensAPos_MEsq.Value;
end;

procedure TFmDuplicata1.frxCompleta1GetValue(const VarName: String;
  var Value: Variant);
begin
  if AnsiCompareText(VarName, 'VARF_EMISSAO') = 0 then Value :=
    Geral.FDT(TPEmissao.Date, 2)
  else if AnsiCompareText(VarName, 'VARF_DESC_PERC') = 0 then Value :=
    EdDescoP.Text
  else if AnsiCompareText(VarName, 'VARF_DESC_VAL' ) = 0 then Value :=
    EdDescoV.Text
  else if AnsiCompareText(VarName, 'VARF_DESC_ATE' ) = 0 then
  begin
    if CkVenctoDesc.Checked then Value := Geral.FDT(TPAte.Date, 2)
    else Value := ' ';
  end
  else if AnsiCompareText(VarName, 'VARF_COND_ESPECIAL') = 0 then Value :=
    EdCondEsp.Text
  else if AnsiCompareText(VarName, 'VARF_FAT_VAL') = 0 then Value :=
    EdValorF.Text
  else if AnsiCompareText(VarName, 'VARF_FAT_NUM') = 0 then Value :=
    EdNumeroF.Text
  else if AnsiCompareText(VarName, 'VARF_DUP_VAL') = 0 then Value :=
    EdValorD.Text
  else if AnsiCompareText(VarName, 'VARF_DUP_NUM') = 0 then Value :=
    EdNumeroD.Text
  else if AnsiCompareText(VarName, 'VARF_VENCTO') = 0 then
  begin
    if RGVcto.ItemIndex = 2 then Value := Geral.FDT(TPVencimento.Date, 2)
    else Value := RGVcto.Items[RGVcto.ItemIndex];
  end
  else if AnsiCompareText(VarName, 'VARF_SACADO') = 0 then Value :=
    EdNomeSacado.Text
  else if AnsiCompareText(VarName, 'VARF_ENDERECO') = 0 then Value :=
    EdEndereco.Text
  else if AnsiCompareText(VarName, 'VARF_EXTENSO') = 0 then Value :=
    MeExtenso.Text
  else if AnsiCompareText(VarName, 'VARF_CIDADE') = 0 then Value :=
    EdCidade.Text
  else if AnsiCompareText(VarName, 'VARF_UF') = 0 then Value :=
    EdUF.Text
  else if AnsiCompareText(VarName, 'VARF_CEP') = 0 then Value :=
    EdCEP.Text
  else if AnsiCompareText(VarName, 'VARF_DOCUM_SAC') = 0 then Value :=
    EdCNPJ.Text
  else if AnsiCompareText(VarName, 'VARF_DOCUM2_SAC') = 0 then Value :=
    EdIE.Text
  else if AnsiCompareText(VarName, 'VARF_PRACAPGTO') = 0 then Value :=
    EdPraca.Text
end;

procedure TFmDuplicata1.RGVctoClick(Sender: TObject);
begin
  BtImprime.Enabled := RGVcto.ItemIndex > -1;
end;

procedure TFmDuplicata1.SpeedButton1Click(Sender: TObject);
begin
  DModG.CadastroDeEntidade(EdSacado.ValueVariant, fmcadEntidade2, fmcadEntidade2);
end;

procedure TFmDuplicata1.SpeedButton2Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmImprime, FmImprime, afmoNegarComAviso) then
  begin
    FmImprime.ShowModal;
    FmImprime.Destroy;
  end;
end;

procedure TFmDuplicata1.N1via1Click(Sender: TObject);
begin
  MyObjects.frxMostra(frxCompleta1, 'Duplicata em 1 via');
end;

procedure TFmDuplicata1.N2vias1Click(Sender: TObject);
begin
  MyObjects.frxMostra(frxCompleta2, 'Duplicata em 2 vias');
end;

end.

