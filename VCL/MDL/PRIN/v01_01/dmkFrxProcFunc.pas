unit dmkFrxProcFunc;

interface

uses
  Forms, StdCtrls, ComCtrls, ExtCtrls, Windows, Messages, SysUtils, Classes,
  Graphics, Controls, Dialogs, Variants, MaskUtils, Registry, UnMsgInt,
  dmkGeral, UnInternalConsts, frxClass;

type
  TListaBMPs = array of TBitmap;
  TdmkFrxProcFunc = class(TObject)
  private
    {private declaration}
  public
    {public declaration}
    procedure CarregaListImageEmFrxReport(var frxReport: TfrxReport;
              var Dest: TListaBMPs; const Source: TImageList);
    function  UniqueNameListaImgsFastReport(ListaImg: TImageList;
              ImageIndex: Integer): String;
  end;

var
  dmkFrxPF: TdmkFrxProcFunc;

implementation

{ TdmkFrxProcFunc }

procedure TdmkFrxProcFunc.CarregaListImageEmFrxReport(var frxReport: TfrxReport;
  var Dest: TListaBMPs; const Source: TImageList);
var
  I: Integer;
  UniqueName: String;
begin
  SetLength(Dest, Source.Count);
  for I := 0 to Source.Count - 1 do
  begin
    Dest[I] := TBitmap.Create;
    Source.GetBitmap(I, Dest[I]);
    //
    UniqueName := UniqueNameListaImgsFastReport(Source, I);
    frxReport.Script.Variables[UniqueName] := Integer(Dest[I]);
  end;
(* Como usar:
No script do report
procedure Picture1OnBeforePrint(Sender: TfrxComponent);
var P: TPicture;
begin
     P:=TPicture.Create;
     P:=TPicture(bmp);  onde bmp = ao valor de UniqueName
     Picture1.Picture:=P;
end;
*)
end;

function TdmkFrxProcFunc.UniqueNameListaImgsFastReport(
  ListaImg: TImageList; ImageIndex: Integer): String;
begin
  Result := 'p' + Trim(ListaImg.Name) + '_' + Geral.FFN(ImageIndex, 4);
end;

end.
