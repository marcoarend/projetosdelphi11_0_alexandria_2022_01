unit EmiteCheque_1;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, ComCtrls, dmkGeral, IdBaseComponent,
  IdComponent, IdTCPConnection, IdTCPClient, dmkEdit, dmkImage, UnDmkEnums;

type
  TFmEmiteCheque_1 = class(TForm)
    PainelDados: TPanel;
    Edit1: TEdit;
    Edit2: TEdit;
    Label3: TLabel;
    ST_Banco: TStaticText;
    Label4: TLabel;
    ST_Agencia: TStaticText;
    ST_Conta: TStaticText;
    Label6: TLabel;
    ST_CMC7: TStaticText;
    Label7: TLabel;
    ST_Cheque: TStaticText;
    ST_Praca: TStaticText;
    Label8: TLabel;
    Label9: TLabel;
    Panel1: TPanel;
    Panel2: TPanel;
    Label1: TLabel;
    LaValor: TLabel;
    Label5: TLabel;
    Label2: TLabel;
    TPData: TDateTimePicker;
    EdValor: TdmkEdit;
    EdBenef: TdmkEdit;
    EdCidade: TdmkEdit;
    RGForma: TRadioGroup;
    Label10: TLabel;
    EdMens: TdmkEdit;
    EdBanco: TdmkEdit;
    Label11: TLabel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel3: TPanel;
    BtOK: TBitBtn;
    BtInfo: TBitBtn;
    LaSerie2: TLabel;
    LaCheque2: TLabel;
    LaSerie1: TLabel;
    LaCheque1: TLabel;
    procedure BtOKClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure Edit1Change(Sender: TObject);
    procedure Edit2Change(Sender: TObject);
    procedure BtInfoClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
    function EnviaComandoPertoChek(Comando: String): String;
  public
    { Public declarations }
  end;

  var
  FmEmiteCheque_1: TFmEmiteCheque_1;

implementation

uses UnMyObjects, PertoChek;

{$R *.DFM}

procedure TFmEmiteCheque_1.BtOKClick(Sender: TObject);
var
  Valor, Cidade, Nome, Data, Banco: String;
begin
  Panel1.Visible := False;
  ST_Banco.Caption   := '';
  ST_Agencia.Caption := '';
  ST_Conta.Caption   := '';
  ST_Cheque.Caption  := '';
  ST_Praca.Caption   := '';
  ST_CMC7.Caption    := '';
  Valor := Geral.CompletaString(Geral.SoNumero_TT(EdValor.Text),
    '0', 12, taRightJustify, False);
  Valor := RGForma.Items[RGForma.ItemIndex][1]+Valor;
  Cidade := Geral.SemAcento(Geral.Maiusculas(EdCidade.Text, False));
  Nome := Geral.SemAcento(Geral.Maiusculas(EdBenef.Text, False));
  Data := FormatDateTime('ddmmyy', TPData.Date);
  //
  if Nome = '' then Nome := '     ';
  if Geral.DMV(EdValor.Text) <= 0 then
  begin
    Geral.MB_Aviso('Informe um valor v�lido!');
    EdValor.SetFocus;
    Exit;
  end;
  if EdCidade.Text = '' then
  begin
    Geral.MB_Aviso('Informe a cidade!');
    EdCidade.SetFocus;
    Exit;
  end;
  //PertoChekP.HabilitaPertoCheck;
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Enviando data');
  EnviaComandoPertoChek('!' + Data);
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Data enviada com sucesso! Enviando cidade');
  EnviaComandoPertoChek('#' + Cidade);
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Cidade enviada com sucesso! Enviando nome');
  EnviaComandoPertoChek('%' + Nome);
  if RGForma.ItemIndex in ([2,3,6,7,10,11,14,15]) then
  begin
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Nome enviado com sucesso! Enviando "="');
    Edit2.Text := EnviaComandoPertoChek('=');
    MyObjects.Informa2(LaAviso1, LaAviso2, True, '"=" enviado com sucesso! Enviando "."');
    Edit1.Text := EnviaComandoPertoChek('.');
    Panel1.Visible := True;
    EnviaComandoPertoChek(';' + Valor);
  end else begin
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Nome enviado com sucesso! Enviando c�digo do banco');
    {
    Banco := InputBox('Impress�o de cheque - Pertochek',
    'Informe os tr�s digitos do banco.', '001');
    }
    Banco := EdBanco.Text;
    EnviaComandoPertoChek('$' + Valor + Banco);
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'C�digo do banco enviado com sucesso.');
   //PertoChekP.DesabilitaPertoCheck;
  end;
  MyObjects.Informa2(LaAviso1, LaAviso2, False,
    'Impress�o do cheque enviado com sucesso! (dados: ' + Valor + Banco + ').');
  //
end;

procedure TFmEmiteCheque_1.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmEmiteCheque_1.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmEmiteCheque_1.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmEmiteCheque_1.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  TPData.Date        := Date;
  ST_CMC7.Caption    := '';
  ST_Banco.Caption   := '';
  ST_Agencia.Caption := '';
  ST_Conta.Caption   := '';
  ST_Cheque.Caption  := '';
  ST_Praca.Caption   := '';
  Edit1.Text         := '';
  RGForma.ItemIndex  := Geral.ReadAppKeyLM('Cheque\Pertochek\', Application.Title, ktInteger, 0);
end;

function TFmEmiteCheque_1.EnviaComandoPertoChek(Comando: String): String;
var
  Ret: String;
begin
  Result := PertoChekP.EnviaPertoChek2(Comando, Ret);
  EdMens.Text := Ret;
end;

procedure TFmEmiteCheque_1.Edit1Change(Sender: TObject);
var
  i: Integer;
  CMC7: String;
begin
  i := Length(Edit1.Text);
  if i >= 34 then
  begin
    CMC7 := Char(123);
    for i := 34 downto 3 do
    begin
      case i of
        26: CMC7 := CMC7 + Char(123) + ' ';
        15: CMC7 := CMC7 + Char(125) + ' ';
        else CMC7 := CMC7 + Edit1.Text[i];
      end;
    end;
    CMC7 := CMC7 + Char(91);
    ST_CMC7.Caption := CMC7;
  end;
end;

procedure TFmEmiteCheque_1.Edit2Change(Sender: TObject);
var
  Erro: Integer;
begin
  if Length(Edit2.Text) = 30 then
  begin
    Erro := StrToInt(Copy(Edit2.Text, 2,3));
    if Erro = 0 then
    begin
      ST_Banco.Caption   := Copy(Edit2.Text, 05,03);
      ST_Agencia.Caption := Copy(Edit2.Text, 08,04);
      ST_Conta.Caption   := Copy(Edit2.Text, 12,10);
      ST_Cheque.Caption  := Copy(Edit2.Text, 22,06);
      ST_Praca.Caption   := Copy(Edit2.Text, 28,03);
    end else begin
      ST_Banco.Caption   := '';
      ST_Agencia.Caption := '';
      ST_Conta.Caption   := '';
      ST_Cheque.Caption  := '';
      ST_Praca.Caption   := '';
    end;
  end;
  //
end;

procedure TFmEmiteCheque_1.BtInfoClick(Sender: TObject);
begin
  Panel1.Visible := False;
  ST_Banco.Caption   := '';
  ST_Agencia.Caption := '';
  ST_Conta.Caption   := '';
  ST_Cheque.Caption  := '';
  ST_Praca.Caption   := '';
  ST_CMC7.Caption    := '';
  //
  //PertoChekP.HabilitaPertoCheck;
  Edit2.Text := EnviaComandoPertoChek('=');
  Edit1.Text := EnviaComandoPertoChek('.');
  EnviaComandoPertoChek('>');
  Panel1.Visible := True;
  //PertoChekP.DesabilitaPertoCheck;
end;

procedure TFmEmiteCheque_1.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Geral.WriteAppKeyLM2('Cheque\Pertochek\', Application.Title, RGForma.ItemIndex, ktInteger);
end;

end.
