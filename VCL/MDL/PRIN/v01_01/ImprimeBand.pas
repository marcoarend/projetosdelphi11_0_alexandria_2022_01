unit ImprimeBand;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, UnInternalConsts, Buttons, DBCtrls, Db, (*DBTables,*)
  UMySQLModule, mySQLDbTables, dmkGeral, dmkEdit, dmkImage,
  UnDmkEnums;

type
  TFmImprimeBand = class(TForm)
    PainelDados: TPanel;
    Label2: TLabel;
    EdNome: TEdit;
    Label1: TLabel;
    EdTopo: TdmkEdit;
    Label3: TLabel;
    EdAltura: TdmkEdit;
    RGTipo: TRadioGroup;
    RGDataset: TRadioGroup;
    Label4: TLabel;
    EdColQtd: TdmkEdit;
    Label5: TLabel;
    EdColLar: TdmkEdit;
    EdColGAP: TdmkEdit;
    Label6: TLabel;
    Label7: TLabel;
    EdLinhas: TdmkEdit;
    RGMultipaginas: TRadioGroup;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    procedure FormActivate(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    FCodigo,
    FControle,
    FAntTopo: Integer;
  end;

var
  FmImprimeBand: TFmImprimeBand;

implementation

uses UnMyObjects, Module, Imprime;

{$R *.DFM}

procedure TFmImprimeBand.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmImprimeBand.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
end;

procedure TFmImprimeBand.BtDesisteClick(Sender: TObject);
begin
  if ImgTipo.SQLType = stIns then
  UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'ImprimeBand', FControle);
  Close;
end;

procedure TFmImprimeBand.BtConfirmaClick(Sender: TObject);
var
  DifTopo: Integer;
begin
  if Trim(EdNome.Text) = '' then begin
    ShowMessage('Defina a descri��o do relat�rio!');
    EdNome.SetFocus;
    Exit;
  end;
  try
    Dmod.QrUpd.SQL.Clear;
    if ImgTipo.SQLType = stIns then begin
      Dmod.QrUpd.SQL.Add('INSERT INTO imprimeband SET ');
      Dmod.QrUpd.SQL.Add('Tipo=:P0, Dataset=:P1, Pos_Topo=:P2, Pos_Altu=:P3, ');
      Dmod.QrUpd.SQL.Add('Nome=:P4, Col_Qtd=:P5, Col_Lar=:P6, Col_GAP=:P7, ');
      Dmod.QrUpd.SQL.Add('Linhas=:P8, Repetencia=:P9, ');
      Dmod.QrUpd.SQL.Add('DataCad=:Pw, UserCad=:Px, Codigo=:Py, Controle=:Pz');
    end else begin
      Dmod.QrUpd.SQL.Add('UPDATE imprimeband SET ');
      Dmod.QrUpd.SQL.Add('Tipo=:P0, Dataset=:P1, Pos_Topo=:P2, Pos_Altu=:P3, ');
      Dmod.QrUpd.SQL.Add('Nome=:P4, Col_Qtd=:P5, Col_Lar=:P6, Col_GAP=:P7, ');
      Dmod.QrUpd.SQL.Add('Linhas=:P8, Repetencia=:P9, ');
      Dmod.QrUpd.SQL.Add('DataAlt=:Pw, UserAlt=:Px WHERE Codigo=:Py AND Controle=:Pz');
    end;
    Dmod.QrUpd.Params[00].AsInteger := RGTipo.ItemIndex;
    Dmod.QrUpd.Params[01].AsInteger := RGDataset.ItemIndex;
    Dmod.QrUpd.Params[02].AsInteger := Geral.IMV(EdTopo.Text);
    Dmod.QrUpd.Params[03].AsInteger := Geral.IMV(EdAltura.Text);
    Dmod.QrUpd.Params[04].AsString  := EdNome.Text;
    Dmod.QrUpd.Params[05].AsInteger := Geral.IMV(EdColQtd.Text);
    Dmod.QrUpd.Params[06].AsInteger := Geral.IMV(EdColLar.Text);
    Dmod.QrUpd.Params[07].AsInteger := Geral.IMV(EdColGAP.Text);
    Dmod.QrUpd.Params[08].AsInteger := Geral.IMV(EdLinhas.Text);
    Dmod.QrUpd.Params[09].AsInteger := RGMultipaginas.ItemIndex;
    //
    Dmod.QrUpd.Params[10].AsString  := FormatDateTime(VAR_FORMATDATE, Date);
    Dmod.QrUpd.Params[11].AsInteger := VAR_USUARIO;
    Dmod.QrUpd.Params[12].AsInteger := FCodigo;
    Dmod.QrUpd.Params[13].AsInteger := FControle;
    Dmod.QrUpd.ExecSQL;
    DifTopo := Geral.IMV(EdTopo.Text) - FAntTopo;
    if (ImgTipo.SQLType = stUpd)
    and (DifTopo <> 0)
    and (FmImprime.QrImprimeView.RecordCount>0) then
    begin
      if Geral.MB_Pergunta('A altera��o do topo desta banda ir� '+
      'alterar o topo de todos seus campos. Caso queira que isso ocorra '+
      'confirme. Deseja alterar a altura real dos campos desta banda?') = ID_NO then
      begin
        Dmod.QrUpd.SQL.Clear;
        Dmod.QrUpd.SQL.Add('UPDATE ImprimeView SET Pos_Topo=Pos_Topo+:P0');
        Dmod.QrUpd.SQL.Add('WHERE Band=:P1');
        Dmod.QrUpd.Params[0].AsInteger := -DifTopo;
        Dmod.QrUpd.Params[1].AsInteger := FControle;
        Dmod.QrUpd.ExecSQL;
      end;
    end;
    FmImprime.FBand := FControle;
    FControle := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
    'ImprimeBand', 'ImprimeBand', 'Codigo');
    Geral.MB_Info('Registro incluido com sucesso');
    EdNome.SetFocus;
    ImgTipo.SQLType := stIns;
  except
    raise;
  end;  
end;

procedure TFmImprimeBand.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

end.
