object FmPromissoria: TFmPromissoria
  Left = 314
  Top = 200
  Caption = 'IMP-PROMS-001 :: Impress'#227'o de Promiss'#243'ria'
  ClientHeight = 648
  ClientWidth = 714
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 714
    Height = 492
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object Panel2: TPanel
      Left = 0
      Top = 0
      Width = 714
      Height = 121
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 224
        Top = 0
        Width = 266
        Height = 65
        Align = alClient
        Caption = ' Data: '
        TabOrder = 1
        object Label1: TLabel
          Left = 8
          Top = 20
          Width = 42
          Height = 13
          Caption = 'Emiss'#227'o:'
        end
        object Label3: TLabel
          Left = 136
          Top = 20
          Width = 59
          Height = 13
          Caption = 'Vencimento:'
        end
        object TPEmissao: TdmkEditDateTimePicker
          Left = 8
          Top = 36
          Width = 125
          Height = 21
          Time = 0.919161041696497700
          TabOrder = 0
          ReadOnly = False
          DefaultEditMask = '!99/99/99;1;_'
          AutoApplyEditMask = True
          UpdType = utYes
          DatePurpose = dmkdpNone
        end
        object TPVencto: TdmkEditDateTimePicker
          Left = 136
          Top = 36
          Width = 125
          Height = 21
          Time = 0.919161041696497700
          TabOrder = 1
          ReadOnly = False
          DefaultEditMask = '!99/99/99;1;_'
          AutoApplyEditMask = True
          UpdType = utYes
          DatePurpose = dmkdpNone
        end
      end
      object GroupBox2: TGroupBox
        Left = 0
        Top = 0
        Width = 224
        Height = 65
        Align = alLeft
        Caption = ' Promiss'#243'ria: '
        TabOrder = 0
        object Label2: TLabel
          Left = 8
          Top = 20
          Width = 27
          Height = 13
          Caption = 'Valor:'
        end
        object Label10: TLabel
          Left = 128
          Top = 20
          Width = 40
          Height = 13
          Caption = 'N'#250'mero:'
        end
        object EdDupVal: TdmkEdit
          Left = 8
          Top = 36
          Width = 117
          Height = 21
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object EdDupNum: TdmkEdit
          Left = 128
          Top = 36
          Width = 85
          Height = 21
          Alignment = taRightJustify
          TabOrder = 1
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
      end
      object GroupBox3: TGroupBox
        Left = 490
        Top = 0
        Width = 224
        Height = 65
        Align = alRight
        Caption = ' Fatura: '
        Enabled = False
        TabOrder = 2
        object Label4: TLabel
          Left = 8
          Top = 20
          Width = 27
          Height = 13
          Caption = 'Valor:'
        end
        object Label5: TLabel
          Left = 128
          Top = 20
          Width = 40
          Height = 13
          Caption = 'N'#250'mero:'
        end
        object EdFatVal: TdmkEdit
          Left = 8
          Top = 36
          Width = 117
          Height = 21
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object EdFatNum: TdmkEdit
          Left = 128
          Top = 36
          Width = 85
          Height = 21
          Alignment = taRightJustify
          TabOrder = 1
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
      end
      object GroupBox5: TGroupBox
        Left = 0
        Top = 65
        Width = 714
        Height = 56
        Align = alBottom
        Caption = ' Sacado: '
        TabOrder = 3
        object SpeedButton1: TSpeedButton
          Left = 684
          Top = 20
          Width = 21
          Height = 21
          Caption = '...'
          OnClick = SpeedButton1Click
        end
        object EdCliente: TdmkEditCB
          Left = 8
          Top = 20
          Width = 56
          Height = 21
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          DBLookupComboBox = CBCliente
          IgnoraDBLookupComboBox = False
          AutoSetIfOnlyOneReg = setregOnlyManual
        end
        object CBCliente: TdmkDBLookupComboBox
          Left = 64
          Top = 20
          Width = 620
          Height = 21
          KeyField = 'Codigo'
          ListField = 'NOMEENTIDADE'
          ListSource = DsCliente
          TabOrder = 1
          dmkEditCB = EdCliente
          UpdType = utYes
          LocF7SQLMasc = '$#'
          LocF7PreDefProc = f7pNone
        end
      end
    end
    object Panel3: TPanel
      Left = 0
      Top = 121
      Width = 714
      Height = 291
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 1
      object DBGrid1: TDBGrid
        Left = 0
        Top = 136
        Width = 714
        Height = 155
        Align = alClient
        DataSource = DsPromissoria
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'FatParcela'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Data'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Credito'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Vencimento'
            Visible = True
          end>
      end
      object Panel5: TPanel
        Left = 0
        Top = 0
        Width = 714
        Height = 136
        Align = alTop
        TabOrder = 0
        object SpeedButton2: TSpeedButton
          Left = 684
          Top = 20
          Width = 21
          Height = 21
          Caption = '...'
          OnClick = SpeedButton1Click
        end
        object Label11: TLabel
          Left = 8
          Top = 4
          Width = 49
          Height = 13
          Caption = 'Avalista 1:'
        end
        object Label12: TLabel
          Left = 8
          Top = 44
          Width = 49
          Height = 13
          Caption = 'Avalista 2:'
        end
        object SpeedButton3: TSpeedButton
          Left = 684
          Top = 60
          Width = 21
          Height = 21
          Caption = '...'
          OnClick = SpeedButton1Click
        end
        object Label9: TLabel
          Left = 8
          Top = 88
          Width = 103
          Height = 13
          Caption = 'Pra'#231'a de Pagamento:'
        end
        object EdAvalista1: TdmkEditCB
          Left = 8
          Top = 20
          Width = 56
          Height = 21
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          DBLookupComboBox = CBAvalista1
          IgnoraDBLookupComboBox = False
          AutoSetIfOnlyOneReg = setregOnlyManual
        end
        object CBAvalista1: TdmkDBLookupComboBox
          Left = 64
          Top = 20
          Width = 620
          Height = 21
          KeyField = 'Codigo'
          ListField = 'NO_ENT'
          ListSource = DsAvalista1
          TabOrder = 1
          dmkEditCB = EdAvalista1
          UpdType = utYes
          LocF7SQLMasc = '$#'
          LocF7PreDefProc = f7pNone
        end
        object EdAvalista2: TdmkEditCB
          Left = 8
          Top = 60
          Width = 56
          Height = 21
          Alignment = taRightJustify
          TabOrder = 2
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          DBLookupComboBox = CBAvalista2
          IgnoraDBLookupComboBox = False
          AutoSetIfOnlyOneReg = setregOnlyManual
        end
        object CBAvalista2: TdmkDBLookupComboBox
          Left = 64
          Top = 60
          Width = 620
          Height = 21
          KeyField = 'Codigo'
          ListField = 'NO_ENT'
          ListSource = DsAvalista2
          TabOrder = 3
          dmkEditCB = EdAvalista2
          UpdType = utYes
          LocF7SQLMasc = '$#'
          LocF7PreDefProc = f7pNone
        end
        object EdPracaPagto: TdmkEdit
          Left = 8
          Top = 104
          Width = 589
          Height = 21
          TabOrder = 4
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object CkFundo: TCheckBox
          Left = 604
          Top = 108
          Width = 97
          Height = 17
          Caption = 'Imprime fundo.'
          Checked = True
          State = cbChecked
          TabOrder = 5
        end
      end
    end
    object Panel4: TPanel
      Left = 0
      Top = 412
      Width = 714
      Height = 80
      Align = alBottom
      TabOrder = 2
      Visible = False
      object GroupBox4: TGroupBox
        Left = 1
        Top = 1
        Width = 712
        Height = 78
        Align = alClient
        Caption = ' Condi'#231#245'es especiais: '
        TabOrder = 0
        object Label6: TLabel
          Left = 12
          Top = 32
          Width = 64
          Height = 13
          Caption = 'Desconto de:'
        end
        object Label7: TLabel
          Left = 188
          Top = 32
          Width = 53
          Height = 13
          Caption = 'Dias antes:'
        end
        object Label8: TLabel
          Left = 244
          Top = 12
          Width = 33
          Height = 13
          Caption = 'Texto: '
        end
        object EdDesc: TdmkEdit
          Left = 12
          Top = 48
          Width = 77
          Height = 21
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object RGMoeda: TRadioGroup
          Left = 92
          Top = 28
          Width = 93
          Height = 41
          Caption = ' Esp'#233'cie: '
          Columns = 2
          ItemIndex = 0
          Items.Strings = (
            'R$'
            '%')
          TabOrder = 1
        end
        object Memo1: TMemo
          Left = 244
          Top = 28
          Width = 357
          Height = 41
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Lucida Console'
          Font.Style = []
          ParentFont = False
          TabOrder = 2
        end
        object EdDDAntes: TdmkEdit
          Left = 188
          Top = 48
          Width = 53
          Height = 21
          Alignment = taRightJustify
          TabOrder = 3
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 714
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 666
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 618
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 315
        Height = 32
        Caption = 'Impress'#227'o de Promiss'#243'ria'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 315
        Height = 32
        Caption = 'Impress'#227'o de Promiss'#243'ria'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 315
        Height = 32
        Caption = 'Impress'#227'o de Promiss'#243'ria'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 540
    Width = 714
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel6: TPanel
      Left = 2
      Top = 15
      Width = 710
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 584
    Width = 714
    Height = 64
    Align = alBottom
    TabOrder = 3
    object Panel7: TPanel
      Left = 2
      Top = 15
      Width = 710
      Height = 47
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object PnSaiDesis: TPanel
        Left = 566
        Top = 0
        Width = 144
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 0
        object BtSaida: TBitBtn
          Tag = 13
          Left = 6
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Sa'#237'da'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
        end
      end
      object BtImprime: TBitBtn
        Tag = 5
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Imprime'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        OnClick = BtImprimeClick
      end
      object BtSalvar: TBitBtn
        Tag = 24
        Left = 132
        Top = 4
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = 'S&alvar'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 2
        OnClick = BtSalvarClick
      end
      object BtBxaNP_BMP: TBitBtn
        Left = 256
        Top = 4
        Width = 181
        Height = 40
        Caption = 'Download NotaPromissoria.BMP'
        TabOrder = 3
        OnClick = BtBxaNP_BMPClick
      end
    end
  end
  object QrCliente: TMySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrClienteCalcFields
    SQL.Strings = (
      'SELECT te.Codigo, te.Tipo, te.Respons1, te.Respons2,'
      'IF( te.Tipo=0, te.RazaoSocial , te.Nome) NOMEENTIDADE,'
      'IF( te.Tipo=0, te.ERua , te.PRua) RUA,'
      'IF( te.Tipo=0, te.ENumero , te.PNumero) + 0.000 NUMERO,'
      'IF( te.Tipo=0, te.ECompl , te.PCompl) COMPL,'
      'IF( te.Tipo=0, te.EBairro , te.PBairro) BAIRRO,'
      'IF( te.Tipo=0, te.ETe1 , te.PTe1) TE1,'
      'IF( te.Tipo=0, te.EFax , te.PFax) FAX,'
      'IF( te.Tipo=0, te.CNPJ , te.CPF) DOC1,'
      'IF( te.Tipo=0, te.IE , te.RG) DOC2,'
      'IF( te.Tipo=0, te.ECidade , te.PCidade) CIDADE,'
      'IF( te.Tipo=0, te.ECEP , te.PCEP) + 0.000 CEP,'
      'IF( te.Tipo=0, te.ELograd , te.PLograd) + 0.000 Lograd,'
      'IF( te.Tipo=0, te.EUF , te.PUF) + 0.000 UF,'
      'uf.Nome NOMEUF, ll.Nome NO_LOGRAD'
      'FROM entidades te, UFs uf, ListaLograd ll'
      'WHERE uf.Codigo=IF(te.Tipo=0, te.EUF , te.PUF)'
      'AND ll.Codigo=IF(te.Tipo=0,ELograd,PLograd)'
      'ORDER BY te.Codigo'
      '')
    Left = 400
    Top = 211
    object QrClienteCNPJ_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CNPJ_TXT'
      Size = 30
      Calculated = True
    end
    object QrClienteTE1_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TE1_TXT'
      Size = 30
      Calculated = True
    end
    object QrClienteCEP_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CEP_TXT'
      Size = 15
      Calculated = True
    end
    object QrClienteNOMEENTIDADE: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
    object QrClienteTipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrClienteRespons1: TWideStringField
      FieldName = 'Respons1'
      Required = True
      Size = 60
    end
    object QrClienteRespons2: TWideStringField
      FieldName = 'Respons2'
      Required = True
      Size = 60
    end
    object QrClienteRUA: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'RUA'
      Size = 30
    end
    object QrClienteCOMPL: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'COMPL'
      Size = 30
    end
    object QrClienteBAIRRO: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'BAIRRO'
      Size = 30
    end
    object QrClienteTE1: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'TE1'
    end
    object QrClienteFAX: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'FAX'
    end
    object QrClienteDOC1: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'DOC1'
      Size = 18
    end
    object QrClienteDOC2: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'DOC2'
    end
    object QrClienteCIDADE: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'CIDADE'
      Size = 25
    end
    object QrClienteNOMEUF: TWideStringField
      FieldName = 'NOMEUF'
      Required = True
      Size = 2
    end
    object QrClienteNUM_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NUM_TXT'
      Size = 30
      Calculated = True
    end
    object QrClienteDOC_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DOC_TXT'
      Size = 30
      Calculated = True
    end
    object QrClienteCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrClienteNO_LOGRAD: TWideStringField
      FieldName = 'NO_LOGRAD'
      Size = 10
    end
    object QrClienteE_ALL: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'E_ALL'
      Size = 255
      Calculated = True
    end
    object QrClienteE_MIN: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'E_MIN'
      Size = 255
      Calculated = True
    end
    object QrClienteNUMERO: TFloatField
      FieldName = 'NUMERO'
    end
    object QrClienteCEP: TFloatField
      FieldName = 'CEP'
    end
    object QrClienteLograd: TFloatField
      FieldName = 'Lograd'
      Required = True
    end
    object QrClienteUF: TFloatField
      FieldName = 'UF'
      Required = True
    end
  end
  object DsPromissoria: TDataSource
    DataSet = QrPromissoria
    Left = 428
    Top = 184
  end
  object QrPromissoria: TMySQLQuery
    Database = Dmod.MyDB
    AfterScroll = QrPromissoriaAfterScroll
    SQL.Strings = (
      'SELECT la.*, ca.Nome NOMECARTEIRA'
      'FROM lanctos la, Carteiras ca'
      'WHERE ca.Codigo=la.Carteira'
      'AND FatNum=:P0'
      'AND FatID=:P1'
      'ORDER BY Vencimento, FatParcela')
    Left = 400
    Top = 184
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrPromissoriaData: TDateField
      FieldName = 'Data'
      Required = True
    end
    object QrPromissoriaTipo: TSmallintField
      FieldName = 'Tipo'
      Required = True
    end
    object QrPromissoriaCarteira: TIntegerField
      FieldName = 'Carteira'
      Required = True
    end
    object QrPromissoriaControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrPromissoriaSub: TSmallintField
      FieldName = 'Sub'
      Required = True
    end
    object QrPromissoriaAutorizacao: TIntegerField
      FieldName = 'Autorizacao'
    end
    object QrPromissoriaGenero: TIntegerField
      FieldName = 'Genero'
    end
    object QrPromissoriaQtde: TFloatField
      FieldName = 'Qtde'
    end
    object QrPromissoriaDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 100
    end
    object QrPromissoriaNotaFiscal: TIntegerField
      FieldName = 'NotaFiscal'
    end
    object QrPromissoriaDebito: TFloatField
      FieldName = 'Debito'
    end
    object QrPromissoriaCredito: TFloatField
      FieldName = 'Credito'
    end
    object QrPromissoriaCompensado: TDateField
      FieldName = 'Compensado'
    end
    object QrPromissoriaSerieCH: TWideStringField
      FieldName = 'SerieCH'
      Size = 10
    end
    object QrPromissoriaDocumento: TFloatField
      FieldName = 'Documento'
    end
    object QrPromissoriaSit: TIntegerField
      FieldName = 'Sit'
    end
    object QrPromissoriaVencimento: TDateField
      FieldName = 'Vencimento'
      Required = True
    end
    object QrPromissoriaFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrPromissoriaFatID_Sub: TIntegerField
      FieldName = 'FatID_Sub'
    end
    object QrPromissoriaFatNum: TFloatField
      FieldName = 'FatNum'
    end
    object QrPromissoriaFatParcela: TIntegerField
      FieldName = 'FatParcela'
    end
    object QrPromissoriaID_Pgto: TIntegerField
      FieldName = 'ID_Pgto'
      Required = True
    end
    object QrPromissoriaID_Quit: TIntegerField
      FieldName = 'ID_Quit'
      Required = True
    end
    object QrPromissoriaID_Sub: TSmallintField
      FieldName = 'ID_Sub'
    end
    object QrPromissoriaFatura: TWideStringField
      FieldName = 'Fatura'
      Size = 1
    end
    object QrPromissoriaEmitente: TWideStringField
      FieldName = 'Emitente'
      Size = 30
    end
    object QrPromissoriaBanco: TIntegerField
      FieldName = 'Banco'
    end
    object QrPromissoriaAgencia: TIntegerField
      FieldName = 'Agencia'
      DisplayFormat = '0000'
    end
    object QrPromissoriaContaCorrente: TWideStringField
      FieldName = 'ContaCorrente'
      Size = 15
    end
    object QrPromissoriaCNPJCPF: TWideStringField
      FieldName = 'CNPJCPF'
      Size = 15
    end
    object QrPromissoriaLocal: TIntegerField
      FieldName = 'Local'
    end
    object QrPromissoriaCartao: TIntegerField
      FieldName = 'Cartao'
    end
    object QrPromissoriaLinha: TIntegerField
      FieldName = 'Linha'
    end
    object QrPromissoriaOperCount: TIntegerField
      FieldName = 'OperCount'
    end
    object QrPromissoriaLancto: TIntegerField
      FieldName = 'Lancto'
    end
    object QrPromissoriaPago: TFloatField
      FieldName = 'Pago'
    end
    object QrPromissoriaMez: TIntegerField
      FieldName = 'Mez'
      Required = True
    end
    object QrPromissoriaFornecedor: TIntegerField
      FieldName = 'Fornecedor'
    end
    object QrPromissoriaCliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrPromissoriaCliInt: TIntegerField
      FieldName = 'CliInt'
    end
    object QrPromissoriaForneceI: TIntegerField
      FieldName = 'ForneceI'
    end
    object QrPromissoriaMoraDia: TFloatField
      FieldName = 'MoraDia'
    end
    object QrPromissoriaMulta: TFloatField
      FieldName = 'Multa'
    end
    object QrPromissoriaMoraVal: TFloatField
      FieldName = 'MoraVal'
    end
    object QrPromissoriaMultaVal: TFloatField
      FieldName = 'MultaVal'
    end
    object QrPromissoriaProtesto: TDateField
      FieldName = 'Protesto'
    end
    object QrPromissoriaDataDoc: TDateField
      FieldName = 'DataDoc'
    end
    object QrPromissoriaCtrlIni: TIntegerField
      FieldName = 'CtrlIni'
    end
    object QrPromissoriaNivel: TIntegerField
      FieldName = 'Nivel'
    end
    object QrPromissoriaVendedor: TIntegerField
      FieldName = 'Vendedor'
    end
    object QrPromissoriaAccount: TIntegerField
      FieldName = 'Account'
    end
    object QrPromissoriaICMS_P: TFloatField
      FieldName = 'ICMS_P'
    end
    object QrPromissoriaICMS_V: TFloatField
      FieldName = 'ICMS_V'
    end
    object QrPromissoriaDuplicata: TWideStringField
      FieldName = 'Duplicata'
      Size = 13
    end
    object QrPromissoriaDepto: TIntegerField
      FieldName = 'Depto'
    end
    object QrPromissoriaDescoPor: TIntegerField
      FieldName = 'DescoPor'
    end
    object QrPromissoriaDescoVal: TFloatField
      FieldName = 'DescoVal'
    end
    object QrPromissoriaDescoControle: TIntegerField
      FieldName = 'DescoControle'
      Required = True
    end
    object QrPromissoriaUnidade: TIntegerField
      FieldName = 'Unidade'
      Required = True
    end
    object QrPromissoriaNFVal: TFloatField
      FieldName = 'NFVal'
      Required = True
    end
    object QrPromissoriaAntigo: TWideStringField
      FieldName = 'Antigo'
    end
    object QrPromissoriaExcelGru: TIntegerField
      FieldName = 'ExcelGru'
    end
    object QrPromissoriaDoc2: TWideStringField
      FieldName = 'Doc2'
    end
    object QrPromissoriaCNAB_Sit: TSmallintField
      FieldName = 'CNAB_Sit'
    end
    object QrPromissoriaTipoCH: TSmallintField
      FieldName = 'TipoCH'
      Required = True
    end
    object QrPromissoriaReparcel: TIntegerField
      FieldName = 'Reparcel'
      Required = True
    end
    object QrPromissoriaLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrPromissoriaDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrPromissoriaDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrPromissoriaUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrPromissoriaUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrPromissoriaAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Required = True
    end
    object QrPromissoriaAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
    object QrPromissoriaAtrelado: TIntegerField
      FieldName = 'Atrelado'
      Required = True
    end
    object QrPromissoriaPagMul: TFloatField
      FieldName = 'PagMul'
      Required = True
    end
    object QrPromissoriaPagJur: TFloatField
      FieldName = 'PagJur'
      Required = True
    end
    object QrPromissoriaSubPgto1: TIntegerField
      FieldName = 'SubPgto1'
      Required = True
    end
    object QrPromissoriaMultiPgto: TIntegerField
      FieldName = 'MultiPgto'
    end
    object QrPromissoriaNOMECARTEIRA: TWideStringField
      FieldName = 'NOMECARTEIRA'
      Required = True
      Size = 100
    end
  end
  object DsCliente: TDataSource
    DataSet = QrCliente
    Left = 372
    Top = 211
  end
  object frxPromissoria: TfrxReport
    Version = '2022.1'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39719.818514895800000000
    ReportOptions.LastChange = 39719.818514895800000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    OnGetValue = frxPromissoriaGetValue
    Left = 432
    Top = 212
    Datasets = <>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
      MirrorMode = []
      object ReportTitle1: TfrxReportTitle
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 524.000000000000000000
        Top = 18.897650000000000000
        Width = 718.110700000000000000
        object Shape1: TfrxShapeView
          AllowVectorExport = True
          Left = -1.000000000000000000
          Top = 17.102350000000000000
          Width = 360.000000000000000000
          Height = 105.000000000000000000
          OnBeforePrint = 'Shape1OnBeforePrint'
          Frame.Typ = []
          Shape = skRoundRectangle
        end
        object Shape2: TfrxShapeView
          AllowVectorExport = True
          Left = 370.000000000000000000
          Top = 17.102350000000000000
          Width = 349.000000000000000000
          Height = 105.000000000000000000
          OnBeforePrint = 'Shape2OnBeforePrint'
          Frame.Typ = []
          Shape = skRoundRectangle
        end
        object Shape3: TfrxShapeView
          AllowVectorExport = True
          Left = -1.000000000000000000
          Top = 137.102350000000000000
          Width = 561.000000000000000000
          Height = 47.000000000000000000
          OnBeforePrint = 'Shape3OnBeforePrint'
          Frame.Typ = []
          Shape = skRoundRectangle
        end
        object Shape4: TfrxShapeView
          AllowVectorExport = True
          Left = 572.000000000000000000
          Top = 137.102350000000000000
          Width = 147.000000000000000000
          Height = 122.000000000000000000
          OnBeforePrint = 'Shape4OnBeforePrint'
          Frame.Typ = []
          Shape = skRoundRectangle
        end
        object Shape5: TfrxShapeView
          AllowVectorExport = True
          Left = -1.000000000000000000
          Top = 197.102350000000000000
          Width = 108.000000000000000000
          Height = 313.000000000000000000
          OnBeforePrint = 'Shape5OnBeforePrint'
          Frame.Typ = []
          Shape = skRoundRectangle
        end
        object Picture1: TfrxPictureView
          AllowVectorExport = True
          Left = 4.000000000000000000
          Top = 21.102350000000000000
          Width = 350.000000000000000000
          Height = 96.000000000000000000
          OnBeforePrint = 'Picture1OnBeforePrint'
          Center = True
          Frame.Typ = []
          Stretched = False
          HightQuality = False
          Transparent = False
          TransparentColor = clWhite
        end
        object Shape6: TfrxShapeView
          AllowVectorExport = True
          Left = 118.000000000000000000
          Top = 197.102350000000000000
          Width = 441.000000000000000000
          Height = 61.000000000000000000
          OnBeforePrint = 'Shape6OnBeforePrint'
          Frame.Typ = []
          Shape = skRoundRectangle
        end
        object Shape7: TfrxShapeView
          AllowVectorExport = True
          Left = 118.000000000000000000
          Top = 273.102350000000000000
          Width = 601.000000000000000000
          Height = 91.000000000000000000
          OnBeforePrint = 'Shape7OnBeforePrint'
          Frame.Typ = []
          Shape = skRoundRectangle
        end
        object Shape8: TfrxShapeView
          AllowVectorExport = True
          Left = 118.000000000000000000
          Top = 380.102350000000000000
          Width = 601.000000000000000000
          Height = 45.000000000000000000
          OnBeforePrint = 'Shape8OnBeforePrint'
          Frame.Typ = []
          Shape = skRoundRectangle
        end
        object Shape9: TfrxShapeView
          AllowVectorExport = True
          Left = 118.000000000000000000
          Top = 441.102350000000000000
          Width = 601.000000000000000000
          Height = 70.000000000000000000
          OnBeforePrint = 'Shape9OnBeforePrint'
          Frame.Typ = []
          Shape = skRoundRectangle
        end
        object Line1: TfrxLineView
          AllowVectorExport = True
          Left = 229.000000000000000000
          Top = 137.102350000000000000
          Height = 47.000000000000000000
          OnBeforePrint = 'Line1OnBeforePrint'
          Color = clBlack
          Frame.Typ = [ftLeft]
        end
        object Line2: TfrxLineView
          AllowVectorExport = True
          Left = 440.000000000000000000
          Top = 137.102350000000000000
          Height = 47.000000000000000000
          OnBeforePrint = 'Line2OnBeforePrint'
          Color = clBlack
          Frame.Typ = [ftLeft]
        end
        object Line3: TfrxLineView
          AllowVectorExport = True
          Left = -1.000000000000000000
          Top = 146.102350000000000000
          Width = 441.000000000000000000
          OnBeforePrint = 'Line3OnBeforePrint'
          Color = clBlack
          Frame.Typ = [ftTop]
        end
        object Line4: TfrxLineView
          AllowVectorExport = True
          Left = -1.000000000000000000
          Top = 155.102350000000000000
          Width = 562.000000000000000000
          OnBeforePrint = 'Line4OnBeforePrint'
          Color = clBlack
          Frame.Typ = [ftTop]
        end
        object Line5: TfrxLineView
          AllowVectorExport = True
          Left = 130.000000000000000000
          Top = 146.102350000000000000
          Height = 39.000000000000000000
          OnBeforePrint = 'Line5OnBeforePrint'
          Color = clBlack
          Frame.Typ = [ftLeft]
        end
        object Line6: TfrxLineView
          AllowVectorExport = True
          Left = 354.000000000000000000
          Top = 146.102350000000000000
          Height = 39.000000000000000000
          OnBeforePrint = 'Line6OnBeforePrint'
          Color = clBlack
          Frame.Typ = [ftLeft]
        end
        object Memo1: TfrxMemoView
          AllowVectorExport = True
          Top = 137.196850393701000000
          Width = 232.000000000000000000
          Height = 9.779530000000000000
          OnBeforePrint = 'Memo1OnBeforePrint'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Lucida Console'
          Font.Style = []
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'FATURA')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          AllowVectorExport = True
          Left = 230.000000000000000000
          Top = 137.196850393701000000
          Width = 208.000000000000000000
          Height = 9.779530000000000000
          OnBeforePrint = 'Memo2OnBeforePrint'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Lucida Console'
          Font.Style = []
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'DUPLICATA')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          AllowVectorExport = True
          Left = -2.000000000000000000
          Top = 145.511811023622000000
          Width = 132.000000000000000000
          Height = 9.070866141732280000
          OnBeforePrint = 'Memo3OnBeforePrint'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Lucida Console'
          Font.Style = []
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'VALOR R$')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          AllowVectorExport = True
          Left = 130.000000000000000000
          Top = 145.511811023622000000
          Width = 96.000000000000000000
          Height = 9.070866141732280000
          OnBeforePrint = 'Memo4OnBeforePrint'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Lucida Console'
          Font.Style = []
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'N'#218'MERO')
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          AllowVectorExport = True
          Left = 230.000000000000000000
          Top = 145.511811023622000000
          Width = 124.000000000000000000
          Height = 9.070866141732280000
          OnBeforePrint = 'Memo5OnBeforePrint'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Lucida Console'
          Font.Style = []
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'VALOR R$')
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          AllowVectorExport = True
          Left = 354.000000000000000000
          Top = 145.511811023622000000
          Width = 84.000000000000000000
          Height = 9.070866141732280000
          OnBeforePrint = 'Memo6OnBeforePrint'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Lucida Console'
          Font.Style = []
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'N'#218'MERO')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          AllowVectorExport = True
          Left = 442.000000000000000000
          Top = 137.543290000000000000
          Width = 116.000000000000000000
          Height = 17.338590000000000000
          OnBeforePrint = 'Memo7OnBeforePrint'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Lucida Console'
          Font.Style = []
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'VENCIMENTO')
          ParentFont = False
          VAlign = vaBottom
        end
        object Memo8: TfrxMemoView
          AllowVectorExport = True
          Left = 574.000000000000000000
          Top = 141.102350000000000000
          Width = 144.000000000000000000
          Height = 30.000000000000000000
          OnBeforePrint = 'Memo8OnBeforePrint'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Lucida Console'
          Font.Style = []
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'PARA USO DA INSTITUI'#199#195'O FINANCEIRA')
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          AllowVectorExport = True
          Left = 122.000000000000000000
          Top = 201.102350000000000000
          Width = 64.000000000000000000
          Height = 16.000000000000000000
          OnBeforePrint = 'Memo9OnBeforePrint'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Lucida Console'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'DESCONTO DE:')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo10: TfrxMemoView
          AllowVectorExport = True
          Left = 122.000000000000000000
          Top = 217.102350000000000000
          Width = 64.000000000000000000
          Height = 32.000000000000000000
          OnBeforePrint = 'Memo10OnBeforePrint'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Lucida Console'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'COND. ESPECIAIS:')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo11: TfrxMemoView
          AllowVectorExport = True
          Left = 414.000000000000000000
          Top = 201.102350000000000000
          Width = 24.000000000000000000
          Height = 16.000000000000000000
          OnBeforePrint = 'Memo11OnBeforePrint'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Lucida Console'
          Font.Style = []
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'AT'#201':')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo12: TfrxMemoView
          AllowVectorExport = True
          Left = 90.000000000000000000
          Top = 209.102350000000000000
          Width = 16.000000000000000000
          Height = 290.000000000000000000
          OnBeforePrint = 'Memo12OnBeforePrint'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Lucida Console'
          Font.Style = []
          Frame.Typ = [ftLeft]
          HAlign = haCenter
          Memo.UTF8W = (
            '[EMPRESA]')
          ParentFont = False
          Rotation = 90
          VAlign = vaCenter
        end
        object Memo13: TfrxMemoView
          AllowVectorExport = True
          Left = 122.000000000000000000
          Top = 277.102350000000000000
          Width = 80.000000000000000000
          Height = 16.000000000000000000
          OnBeforePrint = 'Memo13OnBeforePrint'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Lucida Console'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'NOME DO SACADO:')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo14: TfrxMemoView
          AllowVectorExport = True
          Left = 122.000000000000000000
          Top = 293.102350000000000000
          Width = 80.000000000000000000
          Height = 16.000000000000000000
          OnBeforePrint = 'Memo14OnBeforePrint'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Lucida Console'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'ENDERE'#199'O:')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo15: TfrxMemoView
          AllowVectorExport = True
          Left = 122.000000000000000000
          Top = 325.102350000000000000
          Width = 80.000000000000000000
          Height = 16.000000000000000000
          OnBeforePrint = 'Memo15OnBeforePrint'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Lucida Console'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'P'#199'A DE PAGTO:')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo16: TfrxMemoView
          AllowVectorExport = True
          Left = 122.000000000000000000
          Top = 309.102350000000000000
          Width = 80.000000000000000000
          Height = 16.000000000000000000
          OnBeforePrint = 'Memo16OnBeforePrint'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Lucida Console'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'MUNIC'#205'PIO:')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo17: TfrxMemoView
          AllowVectorExport = True
          Left = 122.000000000000000000
          Top = 341.102350000000000000
          Width = 80.000000000000000000
          Height = 16.000000000000000000
          OnBeforePrint = 'Memo17OnBeforePrint'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Lucida Console'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'CNPJ / CPF N'#186':')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo18: TfrxMemoView
          AllowVectorExport = True
          Left = 510.000000000000000000
          Top = 309.102350000000000000
          Width = 40.000000000000000000
          Height = 16.000000000000000000
          OnBeforePrint = 'Memo18OnBeforePrint'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Lucida Console'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'ESTADO:')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo19: TfrxMemoView
          AllowVectorExport = True
          Left = 450.000000000000000000
          Top = 341.102350000000000000
          Width = 100.000000000000000000
          Height = 16.000000000000000000
          OnBeforePrint = 'Memo19OnBeforePrint'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Lucida Console'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'INSCRI'#199#195'O ESTADUAL:')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo20: TfrxMemoView
          AllowVectorExport = True
          Left = 550.000000000000000000
          Top = 293.102350000000000000
          Width = 24.000000000000000000
          Height = 16.000000000000000000
          OnBeforePrint = 'Memo20OnBeforePrint'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Lucida Console'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'CEP:')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo21: TfrxMemoView
          AllowVectorExport = True
          Left = 122.000000000000000000
          Top = 383.102350000000000000
          Width = 96.000000000000000000
          Height = 40.000000000000000000
          OnBeforePrint = 'Memo21OnBeforePrint'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'VALOR POR EXTENSO')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line7: TfrxLineView
          AllowVectorExport = True
          Left = 221.000000000000000000
          Top = 381.102350000000000000
          Height = 43.000000000000000000
          OnBeforePrint = 'Line7OnBeforePrint'
          Color = clBlack
          Frame.Typ = [ftLeft]
        end
        object Memo22: TfrxMemoView
          AllowVectorExport = True
          Left = 226.000000000000000000
          Top = 382.102350000000000000
          Width = 488.000000000000000000
          Height = 42.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Lucida Console'
          Font.Style = []
          Frame.Typ = []
          HAlign = haBlock
          Memo.UTF8W = (
            '[VALOR_EXTENSO]')
          ParentFont = False
        end
        object Memo23: TfrxMemoView
          AllowVectorExport = True
          Left = 126.000000000000000000
          Top = 445.102350000000000000
          Width = 588.000000000000000000
          Height = 32.000000000000000000
          OnBeforePrint = 'Memo23OnBeforePrint'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Lucida Console'
          Font.Style = []
          Frame.Typ = []
          HAlign = haBlock
          Memo.UTF8W = (
            
              'RECONHE'#199'O(EMOS) A EXATID'#195'O DESTA DUPLICATA DE VENDA MERCANTIL, N' +
              'A IMPORT'#194'NCIA ACIMA, QUE PAGAREI(EMOS) '#192' [EMPRESA], OU '#192' SUA ORD' +
              'EM NA PRA'#199'A E VENCIMENTO INDICADOS.')
          ParentFont = False
        end
        object Memo24: TfrxMemoView
          AllowVectorExport = True
          Left = 126.000000000000000000
          Top = 483.102350000000000000
          Width = 108.000000000000000000
          Height = 16.000000000000000000
          OnBeforePrint = 'Memo24OnBeforePrint'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Lucida Console'
          Font.Style = []
          Frame.Typ = []
          HAlign = haBlock
          Memo.UTF8W = (
            'EM ____/_____/_____')
          ParentFont = False
        end
        object Memo25: TfrxMemoView
          AllowVectorExport = True
          Left = 154.000000000000000000
          Top = 497.102350000000000000
          Width = 80.000000000000000000
          Height = 12.000000000000000000
          OnBeforePrint = 'Memo25OnBeforePrint'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Lucida Console'
          Font.Style = []
          Frame.Typ = []
          HAlign = haBlock
          Memo.UTF8W = (
            'DATA DO ACEITE')
          ParentFont = False
        end
        object Memo26: TfrxMemoView
          AllowVectorExport = True
          Left = 398.000000000000000000
          Top = 497.102350000000000000
          Width = 304.000000000000000000
          Height = 12.000000000000000000
          OnBeforePrint = 'Memo26OnBeforePrint'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Lucida Console'
          Font.Style = []
          Frame.Typ = [ftTop]
          HAlign = haCenter
          Memo.UTF8W = (
            'ASSINATURA DO SACADO')
          ParentFont = False
        end
        object Memo27: TfrxMemoView
          AllowVectorExport = True
          Left = 374.000000000000000000
          Top = 25.102350000000000000
          Width = 340.000000000000000000
          Height = 12.000000000000000000
          OnBeforePrint = 'Memo27OnBeforePrint'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Lucida Console'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[ENDERECO]')
          ParentFont = False
        end
        object Memo28: TfrxMemoView
          AllowVectorExport = True
          Left = 374.000000000000000000
          Top = 39.102350000000000000
          Width = 340.000000000000000000
          Height = 12.000000000000000000
          OnBeforePrint = 'Memo28OnBeforePrint'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Lucida Console'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[FONE_FAX]')
          ParentFont = False
        end
        object Memo29: TfrxMemoView
          AllowVectorExport = True
          Left = 374.000000000000000000
          Top = 53.102350000000000000
          Width = 340.000000000000000000
          Height = 12.000000000000000000
          OnBeforePrint = 'Memo29OnBeforePrint'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Lucida Console'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[CEP_CIDADE_UF]')
          ParentFont = False
        end
        object Memo30: TfrxMemoView
          AllowVectorExport = True
          Left = 374.000000000000000000
          Top = 67.102350000000000000
          Width = 340.000000000000000000
          Height = 12.000000000000000000
          OnBeforePrint = 'Memo30OnBeforePrint'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Lucida Console'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[CNPJ]')
          ParentFont = False
        end
        object Memo31: TfrxMemoView
          AllowVectorExport = True
          Left = 374.000000000000000000
          Top = 81.102350000000000000
          Width = 340.000000000000000000
          Height = 12.000000000000000000
          OnBeforePrint = 'Memo31OnBeforePrint'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Lucida Console'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[IE]')
          ParentFont = False
        end
        object Memo32: TfrxMemoView
          AllowVectorExport = True
          Left = 374.000000000000000000
          Top = 101.102350000000000000
          Width = 120.000000000000000000
          Height = 12.000000000000000000
          OnBeforePrint = 'Memo32OnBeforePrint'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Lucida Console'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'DATA DA EMISS'#195'O:')
          ParentFont = False
        end
        object Memo33: TfrxMemoView
          AllowVectorExport = True
          Left = 2.000000000000000000
          Top = 161.102350000000000000
          Width = 124.000000000000000000
          Height = 16.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Lucida Console'
          Font.Style = []
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[FATVAL]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo34: TfrxMemoView
          AllowVectorExport = True
          Left = 134.000000000000000000
          Top = 161.102350000000000000
          Width = 92.000000000000000000
          Height = 16.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Lucida Console'
          Font.Style = []
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[FATNUM]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo35: TfrxMemoView
          AllowVectorExport = True
          Left = 232.000000000000000000
          Top = 161.102350000000000000
          Width = 120.000000000000000000
          Height = 16.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Lucida Console'
          Font.Style = []
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[DUPVAL]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo36: TfrxMemoView
          AllowVectorExport = True
          Left = 358.000000000000000000
          Top = 161.102350000000000000
          Width = 80.000000000000000000
          Height = 16.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Lucida Console'
          Font.Style = []
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[DUPNUM]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo37: TfrxMemoView
          AllowVectorExport = True
          Left = 444.000000000000000000
          Top = 161.102350000000000000
          Width = 112.000000000000000000
          Height = 16.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Lucida Console'
          Font.Style = []
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[VENCTO]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo38: TfrxMemoView
          AllowVectorExport = True
          Left = 190.000000000000000000
          Top = 201.102350000000000000
          Width = 220.000000000000000000
          Height = 16.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Lucida Console'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[DESCONTO]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo39: TfrxMemoView
          AllowVectorExport = True
          Left = 190.000000000000000000
          Top = 217.102350000000000000
          Width = 364.000000000000000000
          Height = 36.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Lucida Console'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[CONDICOES_ESPECIAIS]')
          ParentFont = False
        end
        object Memo40: TfrxMemoView
          AllowVectorExport = True
          Left = 442.000000000000000000
          Top = 201.102350000000000000
          Width = 112.000000000000000000
          Height = 16.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Lucida Console'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[ATE]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo41: TfrxMemoView
          AllowVectorExport = True
          Left = 202.000000000000000000
          Top = 277.102350000000000000
          Width = 512.000000000000000000
          Height = 16.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Lucida Console'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[SAC_NOME]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo42: TfrxMemoView
          AllowVectorExport = True
          Left = 574.000000000000000000
          Top = 293.102350000000000000
          Width = 140.000000000000000000
          Height = 16.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Lucida Console'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[SAC_CEP_TXT]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo43: TfrxMemoView
          AllowVectorExport = True
          Left = 202.000000000000000000
          Top = 341.102350000000000000
          Width = 244.000000000000000000
          Height = 16.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Lucida Console'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[SAC_CNPJ_CPF]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo44: TfrxMemoView
          AllowVectorExport = True
          Left = 202.000000000000000000
          Top = 325.102350000000000000
          Width = 512.000000000000000000
          Height = 16.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Lucida Console'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[SAC_PRACA_PAGTO]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo45: TfrxMemoView
          AllowVectorExport = True
          Left = 202.000000000000000000
          Top = 309.102350000000000000
          Width = 304.000000000000000000
          Height = 16.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Lucida Console'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[SAC_CIDADE]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo46: TfrxMemoView
          AllowVectorExport = True
          Left = 202.000000000000000000
          Top = 293.102350000000000000
          Width = 344.000000000000000000
          Height = 16.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Lucida Console'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[SAC_ENDERECO]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo47: TfrxMemoView
          AllowVectorExport = True
          Left = 550.000000000000000000
          Top = 341.102350000000000000
          Width = 164.000000000000000000
          Height = 16.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Lucida Console'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[SAC_IE]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo48: TfrxMemoView
          AllowVectorExport = True
          Left = 550.000000000000000000
          Top = 309.102350000000000000
          Width = 164.000000000000000000
          Height = 16.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Lucida Console'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[SAC_NOMEUF]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo49: TfrxMemoView
          AllowVectorExport = True
          Left = 494.000000000000000000
          Top = 101.102350000000000000
          Width = 220.000000000000000000
          Height = 12.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Lucida Console'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[DATA_EMISSAO]')
          ParentFont = False
        end
      end
    end
  end
  object frxPromissoria2: TfrxReport
    Version = '2022.1'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 40374.776172615700000000
    ReportOptions.LastChange = 40374.776172615700000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      '  if <FundoPromissoriaExiste> = True then '
      '    Picture1.LoadFromFile(<FundoPromissoriaArquivo>);  '
      'end.')
    OnGetValue = frxPromissoria2GetValue
    Left = 488
    Top = 212
    Datasets = <>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 12.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 12.000000000000000000
      BottomMargin = 12.000000000000000000
      Frame.Typ = []
      MirrorMode = []
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 311.811023620000000000
        Top = 18.897650000000000000
        Width = 710.551640000000000000
        RowCount = 1
        object Shape1: TfrxShapeView
          AllowVectorExport = True
          Width = 711.685039370000000000
          Height = 311.811023620000000000
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
        end
        object Picture1: TfrxPictureView
          AllowVectorExport = True
          Left = 15.118110240000000000
          Top = 15.118110240000000000
          Width = 681.600000000000000000
          Height = 281.574803150000000000
          Frame.Typ = []
          HightQuality = True
          Transparent = False
          TransparentColor = clWhite
        end
        object Memo1: TfrxMemoView
          AllowVectorExport = True
          Left = 434.645950000000000000
          Top = 22.677165350000000000
          Width = 34.015770000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_DIA]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo2: TfrxMemoView
          AllowVectorExport = True
          Left = 487.559370000000000000
          Top = 22.677165350000000000
          Width = 124.724490000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_MES_TXT]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo3: TfrxMemoView
          AllowVectorExport = True
          Left = 631.181510000000000000
          Top = 22.677165350000000000
          Width = 60.472480000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_ANO]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo4: TfrxMemoView
          AllowVectorExport = True
          Left = 166.299320000000000000
          Top = 71.811070000000000000
          Width = 158.740260000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_DIA_TXT]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo5: TfrxMemoView
          AllowVectorExport = True
          Left = 340.157700000000000000
          Top = 71.811070000000000000
          Width = 173.858380000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_MES_TXT]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo6: TfrxMemoView
          AllowVectorExport = True
          Left = 529.134200000000000000
          Top = 71.811070000000000000
          Width = 162.519790000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_ANO]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo7: TfrxMemoView
          AllowVectorExport = True
          Left = 442.205010000000000000
          Top = 98.267780000000000000
          Width = 34.015770000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'ei')
          ParentFont = False
          WordWrap = False
        end
        object Memo8: TfrxMemoView
          AllowVectorExport = True
          Left = 151.181200000000000000
          Top = 119.055118110000000000
          Width = 332.598640000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[VARF_EMPRESA_NOME]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo9: TfrxMemoView
          AllowVectorExport = True
          Left = 529.134200000000000000
          Top = 119.055118110000000000
          Width = 158.740260000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_EMPRESA_CNPJ]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo10: TfrxMemoView
          AllowVectorExport = True
          Left = 143.622140000000000000
          Top = 143.622140000000000000
          Width = 544.252320000000000000
          Height = 41.574820240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          LineSpacing = 14.000000000000000000
          Memo.UTF8W = (
            '                       [VARF_VALOR_EXTENSO] ')
          ParentFont = False
        end
        object Memo11: TfrxMemoView
          AllowVectorExport = True
          Left = 532.913730000000000000
          Top = 45.354360000000000000
          Width = 154.960730000000000000
          Height = 18.897640240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_VALOR]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo12: TfrxMemoView
          AllowVectorExport = True
          Left = 204.094620000000000000
          Top = 45.354360000000000000
          Width = 294.803340000000000000
          Height = 18.897640240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[VARF_NUM_PROMISSOR]')
          ParentFont = False
          WordWrap = False
        end
        object Memo13: TfrxMemoView
          AllowVectorExport = True
          Left = 222.992270000000000000
          Top = 196.535560000000000000
          Width = 275.905690000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[VARF_LOCAL_PAGTO]')
          ParentFont = False
          WordWrap = False
        end
        object Memo14: TfrxMemoView
          AllowVectorExport = True
          Left = 570.709030000000000000
          Top = 196.535560000000000000
          Width = 30.236240000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_DIA_EMI]')
          ParentFont = False
          WordWrap = False
        end
        object Memo15: TfrxMemoView
          AllowVectorExport = True
          Left = 604.724800000000000000
          Top = 196.535560000000000000
          Width = 30.236240000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_MES_EMI]')
          ParentFont = False
          WordWrap = False
        end
        object Memo16: TfrxMemoView
          AllowVectorExport = True
          Left = 638.740570000000000000
          Top = 196.535560000000000000
          Width = 49.133890000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_ANO_EMI]')
          ParentFont = False
          WordWrap = False
        end
        object Memo17: TfrxMemoView
          AllowVectorExport = True
          Left = 219.212740000000000000
          Top = 219.212740000000000000
          Width = 468.661720000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[VARF_CLIENTE_NOME]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo18: TfrxMemoView
          AllowVectorExport = True
          Left = 181.417440000000000000
          Top = 241.889920000000000000
          Width = 158.740260000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[VARF_CLIENTE_CNPJCPF]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo19: TfrxMemoView
          AllowVectorExport = True
          Left = 384.755905511811000000
          Top = 233.330860000000000000
          Width = 306.897637795275600000
          Height = 22.677170240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[VARF_CLIENTE_ENDERECO]')
          ParentFont = False
          VAlign = vaBottom
        end
        object Memo20: TfrxMemoView
          AllowVectorExport = True
          Left = 136.063080000000000000
          Top = 283.464750000000000000
          Width = 551.811380000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_CLIENTE_NOME]')
          ParentFont = False
          WordWrap = False
        end
        object Memo21: TfrxMemoView
          AllowVectorExport = True
          Left = 46.110236220472440000
          Top = 18.897650000000000000
          Width = 11.338590000000000000
          Height = 275.905680240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_AVALISTA_1_NOME]')
          ParentFont = False
          Rotation = 90
          WordWrap = False
        end
        object Memo22: TfrxMemoView
          AllowVectorExport = True
          Left = 94.110236220472440000
          Top = 18.897650000000000000
          Width = 11.338590000000000000
          Height = 275.905680240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_AVALISTA_2_NOME]')
          ParentFont = False
          Rotation = 90
          WordWrap = False
        end
        object Memo23: TfrxMemoView
          AllowVectorExport = True
          Left = 56.692913390000000000
          Top = 105.826840000000000000
          Width = 11.338590000000000000
          Height = 154.960720240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_AVALISTA_1_CNPJ]')
          ParentFont = False
          Rotation = 90
          WordWrap = False
        end
        object Memo24: TfrxMemoView
          AllowVectorExport = True
          Left = 56.692913390000000000
          Top = 18.897650000000000000
          Width = 11.338590000000000000
          Height = 68.031530240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_AVALISTA_1_TEL1]')
          ParentFont = False
          Rotation = 90
          WordWrap = False
        end
        object Memo25: TfrxMemoView
          AllowVectorExport = True
          Left = 105.448818900000000000
          Top = 105.826840000000000000
          Width = 11.338590000000000000
          Height = 154.960720240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_AVALISTA_2_CNPJ]')
          ParentFont = False
          Rotation = 90
          WordWrap = False
        end
        object Memo26: TfrxMemoView
          AllowVectorExport = True
          Left = 105.448818900000000000
          Top = 18.897650000000000000
          Width = 11.338590000000000000
          Height = 68.031530240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_AVALISTA_2_TEL1]')
          ParentFont = False
          Rotation = 90
          WordWrap = False
        end
      end
    end
  end
  object QrAvalista1: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT IF(Tipo=0, RazaoSocial, Nome) NO_ENT,'
      'IF(Tipo=0, CNPJ, CPF) CNPJCPF,'
      'IF(Tipo=0, ETe1, PTe1) Te1, Codigo'
      'FROM entidades'
      'WHERE Codigo > -1'
      'ORDER BY NO_ENT')
    Left = 452
    Top = 136
    object QrAvalista1NO_ENT: TWideStringField
      FieldName = 'NO_ENT'
      Required = True
      Size = 100
    end
    object QrAvalista1CNPJCPF: TWideStringField
      FieldName = 'CNPJCPF'
      Size = 18
    end
    object QrAvalista1Te1: TWideStringField
      FieldName = 'Te1'
    end
    object QrAvalista1Codigo: TIntegerField
      FieldName = 'Codigo'
    end
  end
  object DsAvalista1: TDataSource
    DataSet = QrAvalista1
    Left = 480
    Top = 136
  end
  object QrAvalista2: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT IF(Tipo=0, RazaoSocial, Nome) NO_ENT,'
      'IF(Tipo=0, CNPJ, CPF) CNPJCPF,'
      'IF(Tipo=0, ETe1, PTe1) Te1, Codigo'
      'FROM entidades'
      'WHERE Codigo > -1'
      'ORDER BY NO_ENT')
    Left = 508
    Top = 136
    object QrAvalista2NO_ENT: TWideStringField
      FieldName = 'NO_ENT'
      Required = True
      Size = 100
    end
    object QrAvalista2CNPJCPF: TWideStringField
      FieldName = 'CNPJCPF'
      Size = 18
    end
    object QrAvalista2Te1: TWideStringField
      FieldName = 'Te1'
    end
    object QrAvalista2Codigo: TIntegerField
      FieldName = 'Codigo'
    end
  end
  object DsAvalista2: TDataSource
    DataSet = QrAvalista2
    Left = 536
    Top = 136
  end
  object frxPromissoria1: TfrxReport
    Version = '2022.1'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 40374.776172615700000000
    ReportOptions.LastChange = 40374.776172615700000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      'end.')
    OnGetValue = frxPromissoria2GetValue
    Left = 460
    Top = 212
    Datasets = <>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 12.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 12.000000000000000000
      BottomMargin = 12.000000000000000000
      Frame.Typ = []
      MirrorMode = []
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 311.811023620000000000
        Top = 18.897650000000000000
        Width = 710.551640000000000000
        RowCount = 1
        object Memo58: TfrxMemoView
          AllowVectorExport = True
          Left = 215.433210000000000000
          Top = 143.622140000000000000
          Width = 472.441250000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Fill.BackColor = 14803425
          ParentFont = False
        end
        object Memo59: TfrxMemoView
          AllowVectorExport = True
          Left = 143.622140000000000000
          Top = 166.299320000000000000
          Width = 464.882190000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Fill.BackColor = 14803425
          ParentFont = False
        end
        object Shape1: TfrxShapeView
          AllowVectorExport = True
          Width = 711.685039370000000000
          Height = 311.811023620000000000
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
        end
        object frxMemoView1: TfrxMemoView
          AllowVectorExport = True
          Left = 434.645950000000000000
          Top = 22.677165350000000000
          Width = 34.015770000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_DIA]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo2: TfrxMemoView
          AllowVectorExport = True
          Left = 487.559370000000000000
          Top = 22.677165350000000000
          Width = 124.724490000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_MES_TXT]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo3: TfrxMemoView
          AllowVectorExport = True
          Left = 631.181510000000000000
          Top = 22.677165350000000000
          Width = 60.472480000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_ANO]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo4: TfrxMemoView
          AllowVectorExport = True
          Left = 166.299320000000000000
          Top = 71.811070000000000000
          Width = 158.740260000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_DIA_TXT]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo5: TfrxMemoView
          AllowVectorExport = True
          Left = 340.157700000000000000
          Top = 71.811070000000000000
          Width = 173.858380000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_MES_TXT]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo6: TfrxMemoView
          AllowVectorExport = True
          Left = 529.134200000000000000
          Top = 71.811070000000000000
          Width = 162.519790000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_ANO]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo7: TfrxMemoView
          AllowVectorExport = True
          Left = 442.205010000000000000
          Top = 97.267780000000000000
          Width = 45.354360000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'ei')
          ParentFont = False
          WordWrap = False
        end
        object Memo8: TfrxMemoView
          AllowVectorExport = True
          Left = 151.181200000000000000
          Top = 119.055118110000000000
          Width = 332.598640000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[VARF_EMPRESA_NOME]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo9: TfrxMemoView
          AllowVectorExport = True
          Left = 529.134200000000000000
          Top = 119.055118110000000000
          Width = 158.740260000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_EMPRESA_CNPJ]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo10: TfrxMemoView
          AllowVectorExport = True
          Left = 143.622140000000000000
          Top = 143.622140000000000000
          Width = 544.252320000000000000
          Height = 41.574820240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          LineSpacing = 14.000000000000000000
          Memo.UTF8W = (
            '                       [VARF_VALOR_EXTENSO]')
          ParentFont = False
        end
        object Memo11: TfrxMemoView
          AllowVectorExport = True
          Left = 532.913730000000000000
          Top = 45.354360000000000000
          Width = 154.960730000000000000
          Height = 18.897640240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Fill.BackColor = 14803425
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_VALOR]')
          ParentFont = False
          WordWrap = False
        end
        object Memo12: TfrxMemoView
          AllowVectorExport = True
          Left = 204.094620000000000000
          Top = 45.354360000000000000
          Width = 154.960730000000000000
          Height = 18.897640240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Fill.BackColor = 14803425
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_NUM_PROMISSOR]')
          ParentFont = False
          WordWrap = False
        end
        object Memo13: TfrxMemoView
          AllowVectorExport = True
          Left = 222.992270000000000000
          Top = 196.535560000000000000
          Width = 275.905690000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[VARF_LOCAL_PAGTO]')
          ParentFont = False
          WordWrap = False
        end
        object Memo14: TfrxMemoView
          AllowVectorExport = True
          Left = 570.709030000000000000
          Top = 196.535560000000000000
          Width = 30.236240000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_DIA_EMI]')
          ParentFont = False
          WordWrap = False
        end
        object Memo15: TfrxMemoView
          AllowVectorExport = True
          Left = 604.724800000000000000
          Top = 196.535560000000000000
          Width = 30.236240000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_MES_EMI]')
          ParentFont = False
          WordWrap = False
        end
        object Memo16: TfrxMemoView
          AllowVectorExport = True
          Left = 638.740570000000000000
          Top = 196.535560000000000000
          Width = 49.133890000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_ANO_EMI]')
          ParentFont = False
          WordWrap = False
        end
        object Memo17: TfrxMemoView
          AllowVectorExport = True
          Left = 219.212740000000000000
          Top = 219.212740000000000000
          Width = 468.661720000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[VARF_CLIENTE_NOME]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo18: TfrxMemoView
          AllowVectorExport = True
          Left = 185.196970000000000000
          Top = 241.889920000000000000
          Width = 154.960730000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[VARF_CLIENTE_CNPJCPF]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo19: TfrxMemoView
          AllowVectorExport = True
          Left = 384.755905510000000000
          Top = 233.330860000000000000
          Width = 306.897637800000000000
          Height = 22.677170240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[VARF_CLIENTE_ENDERECO]')
          ParentFont = False
          VAlign = vaBottom
        end
        object Memo20: TfrxMemoView
          AllowVectorExport = True
          Left = 136.063080000000000000
          Top = 283.464750000000000000
          Width = 551.811380000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_CLIENTE_NOME]')
          ParentFont = False
          WordWrap = False
        end
        object Memo21: TfrxMemoView
          AllowVectorExport = True
          Left = 46.110236220472400000
          Top = 18.897650000000000000
          Width = 11.338590000000000000
          Height = 275.905680240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_AVALISTA_1_NOME]')
          ParentFont = False
          Rotation = 90
          WordWrap = False
        end
        object Memo22: TfrxMemoView
          AllowVectorExport = True
          Left = 94.110236220472400000
          Top = 18.897650000000000000
          Width = 11.338590000000000000
          Height = 275.905680240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_AVALISTA_2_NOME]')
          ParentFont = False
          Rotation = 90
          WordWrap = False
        end
        object Memo23: TfrxMemoView
          AllowVectorExport = True
          Left = 56.692913390000000000
          Top = 105.826840000000000000
          Width = 11.338590000000000000
          Height = 143.622130240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_AVALISTA_1_CNPJ]')
          ParentFont = False
          Rotation = 90
          WordWrap = False
        end
        object Memo24: TfrxMemoView
          AllowVectorExport = True
          Left = 56.692913390000000000
          Top = 18.897650000000000000
          Width = 11.338590000000000000
          Height = 64.252000240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_AVALISTA_1_TEL1]')
          ParentFont = False
          Rotation = 90
          WordWrap = False
        end
        object Memo25: TfrxMemoView
          AllowVectorExport = True
          Left = 105.448818900000000000
          Top = 105.826840000000000000
          Width = 11.338590000000000000
          Height = 143.622130240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_AVALISTA_2_CNPJ]')
          ParentFont = False
          Rotation = 90
          WordWrap = False
        end
        object Memo26: TfrxMemoView
          AllowVectorExport = True
          Left = 105.448818900000000000
          Top = 18.897650000000000000
          Width = 11.338590000000000000
          Height = 64.252000240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_AVALISTA_2_TEL1]')
          ParentFont = False
          Rotation = 90
          WordWrap = False
        end
        object Memo1: TfrxMemoView
          AllowVectorExport = True
          Left = 381.732530000000000000
          Top = 28.236240000000000000
          Width = 49.133890000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Vencimento:')
          ParentFont = False
        end
        object Memo27: TfrxMemoView
          AllowVectorExport = True
          Left = 614.063390000000000000
          Top = 28.456710000000000000
          Width = 15.118120000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'de')
          ParentFont = False
        end
        object Memo28: TfrxMemoView
          AllowVectorExport = True
          Left = 470.661720000000000000
          Top = 28.456710000000000000
          Width = 15.118120000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'de')
          ParentFont = False
        end
        object Memo29: TfrxMemoView
          AllowVectorExport = True
          Left = 326.039580000000000000
          Top = 78.370130000000000000
          Width = 15.118120000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'de')
          ParentFont = False
        end
        object Memo30: TfrxMemoView
          AllowVectorExport = True
          Left = 515.795610000000000000
          Top = 77.370130000000000000
          Width = 15.118120000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'de')
          ParentFont = False
        end
        object Memo31: TfrxMemoView
          AllowVectorExport = True
          Left = 413.748300000000000000
          Top = 100.267780000000000000
          Width = 26.456710000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'pagar')
          ParentFont = False
        end
        object Memo32: TfrxMemoView
          AllowVectorExport = True
          Left = 139.842610000000000000
          Top = 77.590600000000000000
          Width = 30.236240000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'No dia')
          ParentFont = False
        end
        object Memo33: TfrxMemoView
          AllowVectorExport = True
          Left = 138.063080000000000000
          Top = 124.724490000000000000
          Width = 11.338590000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'a')
          ParentFont = False
        end
        object Memo34: TfrxMemoView
          AllowVectorExport = True
          Left = 490.338900000000000000
          Top = 100.047310000000000000
          Width = 79.370130000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'por esta '#250'nica via de')
          ParentFont = False
        end
        object Memo35: TfrxMemoView
          AllowVectorExport = True
          Left = 574.488560000000000000
          Top = 98.267780000000000000
          Width = 113.385900000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            'NOTA PROMISS'#211'RIA')
          ParentFont = False
        end
        object Memo36: TfrxMemoView
          AllowVectorExport = True
          Left = 128.504020000000000000
          Top = 141.842610000000000000
          Width = 83.149660000000000000
          Height = 30.236240000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'OU '#192' SUA ORDEM,'
            'A QUANTIA DE')
          ParentFont = False
        end
        object Memo37: TfrxMemoView
          AllowVectorExport = True
          Left = 483.779840000000000000
          Top = 123.944960000000000000
          Width = 45.354360000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'CPF/CNPJ:')
          ParentFont = False
        end
        object Memo38: TfrxMemoView
          AllowVectorExport = True
          Left = 139.842610000000000000
          Top = 204.094620000000000000
          Width = 86.929190000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Local de pagamento:')
          ParentFont = False
        end
        object Memo39: TfrxMemoView
          AllowVectorExport = True
          Left = 502.677490000000000000
          Top = 202.315090000000000000
          Width = 68.031540000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Data de Emiss'#227'o:')
          ParentFont = False
        end
        object Memo40: TfrxMemoView
          AllowVectorExport = True
          Left = 610.504330000000000000
          Top = 166.299320000000000000
          Width = 83.149660000000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'Em moeda corrente'
            'deste pa'#237's.')
          ParentFont = False
        end
        object Memo41: TfrxMemoView
          AllowVectorExport = True
          Left = 139.842610000000000000
          Top = 225.771800000000000000
          Width = 71.811070000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Nome do emitente:')
          ParentFont = False
        end
        object Memo42: TfrxMemoView
          AllowVectorExport = True
          Left = 632.961040000000000000
          Top = 198.535560000000000000
          Width = 7.559060000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '/')
          ParentFont = False
        end
        object Memo43: TfrxMemoView
          AllowVectorExport = True
          Left = 342.937230000000000000
          Top = 248.448980000000000000
          Width = 41.574830000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Endere'#231'o:')
          ParentFont = False
        end
        object Memo44: TfrxMemoView
          AllowVectorExport = True
          Left = 598.945270000000000000
          Top = 198.535560000000000000
          Width = 7.559060000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '/')
          ParentFont = False
        end
        object Memo45: TfrxMemoView
          AllowVectorExport = True
          Left = 17.118120000000000000
          Top = 234.330860000000000000
          Width = 15.118120000000000000
          Height = 60.472480000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            'AVALISTAS:')
          ParentFont = False
          Rotation = 90
        end
        object Memo47: TfrxMemoView
          AllowVectorExport = True
          Left = 36.795300000000000000
          Top = 268.346630000000000000
          Width = 15.118120000000000000
          Height = 26.456710000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            'Nome:')
          ParentFont = False
          Rotation = 90
        end
        object Memo48: TfrxMemoView
          AllowVectorExport = True
          Left = 56.692950000000000000
          Top = 249.448980000000000000
          Width = 15.118120000000000000
          Height = 45.354360000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            'CPF/CNPJ:')
          ParentFont = False
          Rotation = 90
        end
        object Memo49: TfrxMemoView
          AllowVectorExport = True
          Left = 85.929190000000000000
          Top = 268.346630000000000000
          Width = 15.118120000000000000
          Height = 26.456710000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            'Nome:')
          ParentFont = False
          Rotation = 90
        end
        object Memo50: TfrxMemoView
          AllowVectorExport = True
          Left = 105.826840000000000000
          Top = 249.448980000000000000
          Width = 15.118120000000000000
          Height = 45.354360000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            'CPF/CNPJ:')
          ParentFont = False
          Rotation = 90
        end
        object Memo51: TfrxMemoView
          AllowVectorExport = True
          Left = 56.692950000000000000
          Top = 83.149660000000000000
          Width = 15.118120000000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            'Fone:')
          ParentFont = False
          Rotation = 90
        end
        object Memo52: TfrxMemoView
          AllowVectorExport = True
          Left = 105.826840000000000000
          Top = 83.149660000000000000
          Width = 15.118120000000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            'Fone:')
          ParentFont = False
          Rotation = 90
        end
        object Memo53: TfrxMemoView
          AllowVectorExport = True
          Left = 32.236240000000000000
          Top = 18.897650000000000000
          Width = 15.118120000000000000
          Height = 249.448980000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftRight]
          Frame.Width = 0.100000000000000000
          ParentFont = False
          Rotation = 90
        end
        object Memo54: TfrxMemoView
          AllowVectorExport = True
          Left = 80.149660000000000000
          Top = 18.897650000000000000
          Width = 15.118120000000000000
          Height = 249.448980000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftRight]
          Frame.Width = 0.100000000000000000
          ParentFont = False
          Rotation = 90
        end
        object Memo46: TfrxMemoView
          AllowVectorExport = True
          Left = 139.842610000000000000
          Top = 100.267780000000000000
          Width = 272.126160000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '')
          ParentFont = False
        end
        object Memo55: TfrxMemoView
          AllowVectorExport = True
          Left = 173.858267720000000000
          Top = 45.354330710000000000
          Width = 30.236240000000000000
          Height = 18.897640240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            'N'#186)
          ParentFont = False
          WordWrap = False
        end
        object Memo56: TfrxMemoView
          AllowVectorExport = True
          Left = 139.842610000000000000
          Top = 245.669450000000000000
          Width = 45.354360000000000000
          Height = 11.338590000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'CPF/CNPJ:')
          ParentFont = False
        end
        object Memo57: TfrxMemoView
          AllowVectorExport = True
          Left = 506.457020000000000000
          Top = 45.354360000000000000
          Width = 26.456710000000000000
          Height = 18.897640240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            'R$')
          ParentFont = False
          WordWrap = False
        end
      end
    end
  end
end
