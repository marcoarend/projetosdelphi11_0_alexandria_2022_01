unit SugVPedItsAtrProd;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DB,
  DBGrids, dmkDBGrid, DBCtrls, mySQLDbTables, Menus, ComCtrls, UnDmkProcFunc;

type
  THackDBGrid = class(TDBGrid);
  TFmSugVPedItsAtrProd = class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    BtNenhum: TBitBtn;
    BtTodos: TBitBtn;
    BtPesq: TBitBtn;
    PMPesq: TPopupMenu;
    Ativa1: TMenuItem;
    Desativa1: TMenuItem;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    Panel3: TPanel;
    Label1: TLabel;
    EdPesq: TEdit;
    DBGrid1: TdmkDBGrid;
    Panel4: TPanel;
    DBGrid2: TdmkDBGrid;
    RGSelecao: TRadioGroup;
    procedure FormCreate(Sender: TObject);
    procedure DBGrid1CellClick(Column: TColumn);
    procedure FormStartDock(Sender: TObject; var DragObject: TDragDockObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure EdPesqChange(Sender: TObject);
    procedure FormUnDock(Sender: TObject; Client: TControl;
      NewTarget: TWinControl; var Allow: Boolean);
    procedure BtNenhumClick(Sender: TObject);
    procedure BtTodosClick(Sender: TObject);
    procedure BtPesqClick(Sender: TObject);
    procedure Ativa1Click(Sender: TObject);
    procedure Desativa1Click(Sender: TObject);
    procedure PageControl1Change(Sender: TObject);
    procedure FormDeactivate(Sender: TObject);
    procedure RGSelecaoClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    FSVPAtrProd: String;
    procedure Pesquisar();
    procedure AtivaItens(Status: Integer);
    procedure AtivaSelecionados(Status: Integer);
    procedure HabilitaItemUnico();
  public
    { Public declarations }
    class function CreateDockForm(const aColor: TColor): TCustomForm;
    //class procedure FiltaAtivos();
    procedure LimpaPesquisa();
  end;

var
  FmSugVPedItsAtrProd: TFmSugVPedItsAtrProd;

implementation

uses UnMyObjects, Module, UMySQLModule, SugVPedIts, ModSugVda, ModuleGeral, UCreate,
UnInternalConsts, MyGlyfs;

{$R *.DFM}

///// PROCEDURES E FUNCTIONS DO DOCKFORM

procedure TFmSugVPedItsAtrProd.Ativa1Click(Sender: TObject);
begin
  AtivaSelecionados(1);
end;

procedure TFmSugVPedItsAtrProd.AtivaItens(Status: Integer);
var
  Codigo: Integer;
begin
  Screen.Cursor := crHourGlass;
  Codigo := DmSugVda.TbSVPAtrProdItsCodigo.Value;
  DModG.QrUpdPID1.SQL.Clear;
  DModG.QrUpdPID1.SQL.Add('UPDATE ' + FSVPAtrProd + ' SET Ativo=' + dmkPF.FFP(Status, 0) + ';');
  UMyMod.ExecutaQuery(DmodG.QrUpdPID1);
  DmSugVda.TbSVPAtrProdIts.Close;
  DmSugVda.TbSVPAtrProdIts.Open;
  DmSugVda.TbSVPAtrProdIts.Locate('Codigo', Codigo, []);
  HabilitaItemUnico();
  Screen.Cursor := crDefault;
end;

procedure TFmSugVPedItsAtrProd.AtivaSelecionados(Status: Integer);
begin
  Screen.Cursor := crHourGlass;
  DmSugVda.TbSVPAtrProdIts.First;
  while not DmSugVda.TbSVPAtrProdIts.Eof do
  begin
    if DmSugVda.TbSVPAtrProdItsAtivo.Value <> Status then
    begin
      DmSugVda.TbSVPAtrProdIts.Edit;
      DmSugVda.TbSVPAtrProdItsAtivo.Value := Status;
      DmSugVda.TbSVPAtrProdIts.Post;
    end;
    DmSugVda.TbSVPAtrProdIts.Next;
  end;
  HabilitaItemUnico();
  Screen.Cursor := crDefault;
end;

procedure TFmSugVPedItsAtrProd.BtPesqClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMPesq, BtPesq);
end;

procedure TFmSugVPedItsAtrProd.BtNenhumClick(Sender: TObject);
begin
  AtivaItens(0);
end;

procedure TFmSugVPedItsAtrProd.BtTodosClick(Sender: TObject);
begin
  AtivaItens(1);
end;

class function TFmSugVPedItsAtrProd.CreateDockForm(const aColor: TColor): TCustomForm;
begin
  result := TFmSugVPedItsAtrProd.Create(Application);
  //result.Color := aColor;
  //result.Caption := ColorToString(aColor);
  FmMyGlyfs.ConfiguraFormDock(result);
  result.Show;
end;

procedure TFmSugVPedItsAtrProd.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  DmodG.QrUpdPID1.Close;
  DmodG.QrUpdPID1.SQL.Clear;
  DmodG.QrUpdPID1.SQL.Add('DROP TABLE ' + FSVPAtrProd + '; ');
  UMyMod.ExecutaQuery(DmodG.QrUpdPID1);
  //
  ManualFloat(Rect(0, 0, 0, 0));
  Action := caFree;
end;

procedure TFmSugVPedItsAtrProd.FormShow(Sender: TObject);
begin
  FmSugVPedIts.FechaPesquisa();
end;

procedure TFmSugVPedItsAtrProd.FormStartDock(Sender: TObject;
  var DragObject: TDragDockObject);
begin
  DragObject := TDragDockObjectEx.Create(Self);
  DragObject.Brush.Color := clAqua; // this will display a red outline
end;


procedure TFmSugVPedItsAtrProd.FormUnDock(Sender: TObject; Client: TControl;
  NewTarget: TWinControl; var Allow: Boolean);
begin
end;

///// FIM DAS PROCEDURES E FUNCTIONS DO DOCKFORM

procedure TFmSugVPedItsAtrProd.PageControl1Change(Sender: TObject);
begin
  BtPesq.Enabled := PageControl1.ActivePageIndex = 0;
  case PageControl1.ActivePageIndex of
    0: Pesquisar();
    1: RGSelecaoClick(Self);
  end;
end;

procedure TFmSugVPedItsAtrProd.Pesquisar();
begin
  Screen.Cursor := crHourGlass;
  //
  DmSugVda.TbSVPAtrProdIts.Filter   := 'Nome LIKE ''%' + EdPesq.Text + '%''';
  DmSugVda.TbSVPAtrProdIts.Filtered := True;
  //
  Screen.Cursor := crDefault;
end;

procedure TFmSugVPedItsAtrProd.RGSelecaoClick(Sender: TObject);
begin
  DmSugVda.TbSVPAtrProdIts.Filtered := False;
  case RGSelecao.ItemIndex of
    0: DmSugVda.TbSVPAtrProdIts.Filter := 'Ativo=0';
    1: DmSugVda.TbSVPAtrProdIts.Filter := 'Ativo=1';
    2: DmSugVda.TbSVPAtrProdIts.Filter := '';
  end;
  if RGSelecao.ItemIndex < 2 then
    DmSugVda.TbSVPAtrProdIts.Filtered := True;
end;

procedure TFmSugVPedItsAtrProd.DBGrid1CellClick(Column: TColumn);
var
  Controle, Status: Integer;
begin
  if Column.FieldName = 'Ativo' then
  begin
    Controle := DmSugVda.TbSVPAtrProdItsControle.Value;
    if DmSugVda.TbSVPAtrProdItsAtivo.Value = 1 then
    begin
      Status := 0;
      DmSugVda.TbSVPAtrProdIts.Edit;
      DmSugVda.TbSVPAtrProdItsAtivo.Value := Status;
      DmSugVda.TbSVPAtrProdIts.Post;
    end else begin
      DmodG.QrUpdPID1.SQL.Clear;
      DmodG.QrUpdPID1.SQL.Add('UPDATE ' + FSVPAtrProd + ' SET Ativo=0');
      DmodG.QrUpdPID1.SQL.Add('WHERE Codigo=:P0');
      DmodG.QrUpdPID1.Params[0].AsInteger := DmSugVda.TbSVPAtrProdItsCodigo.Value;
      UMyMod.ExecutaQuery(DmodG.QrUpdPID1);
      //
      DmSugVda.TbSVPAtrProdIts.Close;
      DmSugVda.TbSVPAtrProdIts.Open;
      if DmSugVda.TbSVPAtrProdIts.Locate('Controle', Controle, []) then
      begin
        DmSugVda.TbSVPAtrProdIts.Edit;
        DmSugVda.TbSVPAtrProdItsAtivo.Value := 1;
        DmSugVda.TbSVPAtrProdIts.Post;
      end;
    end;
    //
  end;
  HabilitaItemUnico();
end;

procedure TFmSugVPedItsAtrProd.HabilitaItemUnico();
var
  Habilita: Boolean;
begin
  DmSugVda.QrSVPAtrProdAti.Close;
  DmSugVda.QrSVPAtrProdAti.Open;
  Habilita :=  DmSugVda.QrSVPAtrProdAtiItens.Value = 0;
  FmSugVPedIts.LaGraAtrCad.Enabled := Habilita;
  FmSugVPedIts.EdGraAtrCad.Enabled := Habilita;
  FmSugVPedIts.CBGraAtrCad.Enabled := Habilita;
  FmSugVPedIts.LaGraAtrIts.Enabled := Habilita;
  FmSugVPedIts.EdGraAtrIts.Enabled := Habilita;
  FmSugVPedIts.CBGraAtrIts.Enabled := Habilita;
  if not Habilita then
  begin
    FmSugVPedIts.EdGraAtrCad.ValueVariant := 0;
    FmSugVPedIts.CBGraAtrCad.KeyValue     := 0;
  end;
end;

procedure TFmSugVPedItsAtrProd.LimpaPesquisa;
var
  K, I: Integer;
begin
  for K := 0 to Screen.FormCount - 1 do
  begin
    if Screen.Forms[K] is TFmSugVPedItsAtrProd then
    begin
      for I := 0 to Screen.Forms[K].ComponentCount -1 do
      if Screen.Forms[K].Components[I] is TEdit then
        TEdit(Screen.Forms[K].Components[I]).Text := '';
    end;
  end;
  AtivaItens(0);
end;

procedure TFmSugVPedItsAtrProd.Desativa1Click(Sender: TObject);
begin
  AtivaSelecionados(0);
end;

procedure TFmSugVPedItsAtrProd.EdPesqChange(Sender: TObject);
begin
  Pesquisar();
end;

procedure TFmSugVPedItsAtrProd.FormCreate(Sender: TObject);
begin
  DmSugVda.TbSVPAtrProdIts.Close;
  FSVPAtrProd := UCriar.RecriaTempTable('SVPAtrProd', DmodG.QrUpdPID1, False);
  DmodG.QrUpdPID1.SQL.Clear;
  DmodG.QrUpdPID1.SQL.Add('INSERT INTO ' + FSVPAtrProd);
  DmodG.QrUpdPID1.SQL.Add('SELECT gai.Codigo, gai.Controle, gai.CodUsu,');
  DmodG.QrUpdPID1.SQL.Add('CONCAT(gac.Nome, ": " , gai.Nome) Nome, 0');
  DmodG.QrUpdPID1.SQL.Add('FROM ' + TMeuDB + '.graatrits gai');
  DmodG.QrUpdPID1.SQL.Add('LEFT JOIN ' + TMeuDB + '.graatrcad gac ON gac.Codigo=gai.Codigo');
  DmodG.QrUpdPID1.SQL.Add('ORDER BY Nome');
  UMyMod.ExecutaQuery(DmodG.QrUpdPID1);
  DmSugVda.TbSVPAtrProdIts.Open;
  PageControl1.ActivePageIndex := 0;
end;

procedure TFmSugVPedItsAtrProd.FormDeactivate(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 1;
  RGSelecao.ItemIndex := 1;
  //
  if FindWindow('TFmSugVPedIts', nil) > 0 then
    FmSugVPedIts.AtivaBtConfirma();
end;

end.

