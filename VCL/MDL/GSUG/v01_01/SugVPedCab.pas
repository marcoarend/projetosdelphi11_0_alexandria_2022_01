unit SugVPedCab;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls, Db, (*DBTables,*) JPEG, ExtDlgs,
  ZCF2, ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt, UnInternalConsts2,
  UMySQLModule, mySQLDbTables, UnMySQLCuringa, dmkGeral, dmkPermissoes, dmkEdit,
  dmkLabel, dmkRadioGroup, Grids, DBGrids, dmkDBGrid, Menus, dmkCheckGroup,
  UnDmkProcFunc, dmkImage, UnDmkEnums;

type
  TFmSugVPedCab = class(TForm)
    PainelDados: TPanel;
    DsSugVPedCab: TDataSource;
    QrSugVPedCab: TmySQLQuery;
    PainelEdita: TPanel;
    PainelEdit: TPanel;
    EdCodigo: TdmkEdit;
    PnCabeca: TPanel;
    Label1: TLabel;
    DBEdCodigo: TDBEdit;
    dmkPermissoes1: TdmkPermissoes;
    Label3: TLabel;
    DBEdit1: TDBEdit;
    Label7: TLabel;
    Label8: TLabel;
    EdCodUsu: TdmkEdit;
    QrSugVPedCabCodigo: TIntegerField;
    QrSugVPedCabCodUsu: TIntegerField;
    QrSugVPedCabLk: TIntegerField;
    QrSugVPedCabDataCad: TDateField;
    QrSugVPedCabDataAlt: TDateField;
    QrSugVPedCabUserCad: TIntegerField;
    QrSugVPedCabUserAlt: TIntegerField;
    QrSugVPedCabAlterWeb: TSmallintField;
    QrSugVPedCabAtivo: TSmallintField;
    Label9: TLabel;
    Label10: TLabel;
    EdNome: TdmkEdit;
    QrSugVPedCabNome: TWideStringField;
    DBEdit6: TDBEdit;
    QrSugVPedCabLOGIN_CAD: TWideStringField;
    QrSugVPedCabLOGIN_ALT: TWideStringField;
    DBEdit2: TDBEdit;
    Label2: TLabel;
    DBEdit3: TDBEdit;
    DBEdit4: TDBEdit;
    DBEdit7: TDBEdit;
    DBEdit8: TDBEdit;
    DBEdit5: TDBEdit;
    Label4: TLabel;
    Panel6: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel7: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel8: TPanel;
    PnSaiDesis: TPanel;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    BtInclui: TBitBtn;
    BtCalcula: TBitBtn;
    BtExclui: TBitBtn;
    BtConfirma: TBitBtn;
    BtDesiste: TBitBtn;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrSugVPedCabAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrSugVPedCabBeforeOpen(DataSet: TDataSet);
    procedure EdCodUsuKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SbNovoClick(Sender: TObject);
    procedure BtIncluiClick(Sender: TObject);
    procedure BtCalculaClick(Sender: TObject);
    procedure QrSugVPedCabAfterScroll(DataSet: TDataSet);
    procedure QrSugVPedCabBeforeClose(DataSet: TDataSet);
    procedure SbImprimeClick(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
   ////Procedures do form
    procedure MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo: Integer);
    procedure DefParams;
    procedure Va(Para: TVaiPara);
  public
    { Public declarations }
    procedure LocCod(Atual, Codigo: Integer);
  end;

var
  FmSugVPedCab: TFmSugVPedCab;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, MyDBCheck, SugVPedIts;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmSugVPedCab.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmSugVPedCab.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrSugVPedCabCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmSugVPedCab.DefParams;
begin
  VAR_GOTOTABELA := 'SugVPedCab';
  VAR_GOTOMYSQLTABLE := QrSugVPedCab;
  VAR_GOTONEG := gotoAll;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;
  VAR_SQLx.Add('SELECT sec.Login LOGIN_CAD, sea.Login LOGIN_ALT, svp.*');
  VAR_SQLx.Add('FROM sugvpedcab svp');
  VAR_SQLx.Add('LEFT JOIN senhas sec ON sec.Numero=svp.UserCad');
  VAR_SQLx.Add('LEFT JOIN senhas sea ON sea.Numero=svp.UserCad');
  VAR_SQLx.Add('WHERE svp.Codigo > -1000');
  //
  VAR_SQL1.Add('AND svp.Codigo=:P0');
  //
  VAR_SQL2.Add('AND svp.CodUsu=:P0');
  //
  VAR_SQLa.Add('AND svp.Nome Like :P0');
  //
end;

procedure TFmSugVPedCab.EdCodUsuKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key = VK_F4) and (ImgTipo.SQLType = stIns) then
    UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, 'SugVPedCab', 'CodUsu', [], [],
    stIns, 0, siPositivo, EdCodUsu);
end;

procedure TFmSugVPedCab.MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo: Integer);
begin
  case Mostra of
    0:
    begin
      PainelDados.Visible := True;
      PainelEdita.Visible := False;
    end;
    else Application.MessageBox('A��o de Inclus�o/altera��o n�o definida!',
    'Aviso', MB_OK+MB_ICONWARNING);
  end;
  ImgTipo.SQLType := SQLType;
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmSugVPedCab.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmSugVPedCab.QueryPrincipalAfterOpen;
begin
end;

procedure TFmSugVPedCab.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmSugVPedCab.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmSugVPedCab.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmSugVPedCab.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmSugVPedCab.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmSugVPedCab.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrSugVPedCabCodigo.Value;
  Close;
end;

procedure TFmSugVPedCab.BtIncluiClick(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PainelEdit, QrSugVPedCab, [PainelDados],
  [PainelEdita], EdCodUsu, ImgTipo, 'SugVPedCab');
end;

procedure TFmSugVPedCab.BtConfirmaClick(Sender: TObject);
var
  Codigo: Integer;
  Nome: String;
begin
  Nome := EdNome.Text;
  if Length(Nome) = 0 then
  begin
    Application.MessageBox('Defina uma descri��o.', 'Erro', MB_OK+MB_ICONERROR);
    Exit;
  end;
  Codigo := UMyMod.BuscaEmLivreY_Def('SugVPedCab', 'Codigo', ImgTipo.SQLType,
    QrSugVPedCabCodigo.Value);
  if UMyMod.ExecSQLInsUpdPanel(ImgTipo.SQLType, FmSugVPedCab, PainelEdit,
    'SugVPedCab', Codigo, Dmod.QrUpd, [PainelEdita], [PainelDados], ImgTipo, True) then
  begin
    LocCod(Codigo, Codigo);
  end;
end;

procedure TFmSugVPedCab.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := Geral.IMV(EdCodigo.Text);
  if ImgTipo.SQLType = stIns then UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'SugVPedCab', Codigo);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'SugVPedCab', 'Codigo');
  MostraEdicao(0, stLok, 0);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'SugVPedCab', 'Codigo');
end;

procedure TFmSugVPedCab.BtCalculaClick(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmSugVPedIts, FmSugVPedIts, afmoNegarComAviso) then
  begin
    FmSugVPedIts.ShowModal;
    FmSugVPedIts.Destroy;
  end;
end;

procedure TFmSugVPedCab.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  PainelEdit.Align  := alClient;
  PnCabeca.Align    := alClient;
  CriaOForm;
end;

procedure TFmSugVPedCab.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrSugVPedCabCodigo.Value, LaRegistro.Caption);
end;

procedure TFmSugVPedCab.SbImprimeClick(Sender: TObject);
begin
//
end;

procedure TFmSugVPedCab.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmSugVPedCab.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrSugVPedCabCodUsu.Value, LaRegistro.Caption);
end;

procedure TFmSugVPedCab.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmSugVPedCab.QrSugVPedCabAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmSugVPedCab.QrSugVPedCabAfterScroll(DataSet: TDataSet);
var
  Habilita: Boolean;
begin
  Habilita := (QrSugVPedCab.State <> dsInactive) and (QrSugVPedCab.RecordCount > 0);
  BtCalcula.Enabled := Habilita;
  BtExclui.Enabled := Habilita;
end;

procedure TFmSugVPedCab.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmSugVPedCab.SbQueryClick(Sender: TObject);
begin
  LocCod(QrSugVPedCabCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'SugVPedCab', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmSugVPedCab.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmSugVPedCab.QrSugVPedCabBeforeClose(DataSet: TDataSet);
begin
  BtCalcula.Enabled := False;
  BtExclui.Enabled := False;
end;

procedure TFmSugVPedCab.QrSugVPedCabBeforeOpen(DataSet: TDataSet);
begin
  QrSugVPedCabCodigo.DisplayFormat := FFormatFloat;
end;

end.

