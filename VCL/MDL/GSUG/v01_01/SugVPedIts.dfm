object FmSugVPedIts: TFmSugVPedIts
  Left = 0
  Top = 0
  Caption = 'SUG-PEDVD-002 :: C'#225'lculo de Sugest'#227'o de Venda por Pedidos'
  ClientHeight = 561
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object pDockLeft: TPanel
    Left = 1008
    Top = 48
    Width = 0
    Height = 465
    Align = alRight
    BevelOuter = bvNone
    DockSite = True
    TabOrder = 0
    OnDockDrop = pDockLeftDockDrop
    OnDockOver = pDockLeftDockOver
    OnUnDock = pDockLeftUnDock
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    Caption = 'C'#225'lculo de Sugest'#227'o de Venda por Pedidos'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 1
    object Image1: TImage
      Left = 1
      Top = 1
      Width = 1006
      Height = 46
      Align = alClient
      Transparent = True
      ExplicitLeft = 2
      ExplicitTop = 2
      ExplicitWidth = 788
      ExplicitHeight = 44
    end
  end
  object Panel3: TPanel
    Left = 0
    Top = 513
    Width = 1008
    Height = 48
    Align = alBottom
    TabOrder = 2
    object Label1: TLabel
      Left = 508
      Top = 4
      Width = 36
      Height = 13
      Caption = 'Tempo:'
    end
    object BtConfirma: TBitBtn
      Tag = 20
      Left = 20
      Top = 4
      Width = 90
      Height = 40
      Caption = '&Pesquisa'
      NumGlyphs = 2
      TabOrder = 0
      OnClick = BtConfirmaClick
    end
    object Panel4: TPanel
      Left = 896
      Top = 1
      Width = 111
      Height = 46
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtDesiste: TBitBtn
        Tag = 13
        Left = 2
        Top = 3
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sair'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtDesisteClick
      end
    end
    object BtImprime: TBitBtn
      Tag = 5
      Left = 112
      Top = 4
      Width = 90
      Height = 40
      Caption = '&Imprime'
      Enabled = False
      NumGlyphs = 2
      TabOrder = 2
      OnClick = BtImprimeClick
    end
    object BtLimpa: TBitBtn
      Tag = 20
      Left = 204
      Top = 4
      Width = 90
      Height = 40
      Caption = '&Limpa'
      NumGlyphs = 2
      TabOrder = 3
      OnClick = BtLimpaClick
    end
    object CkRolar: TCheckBox
      Left = 304
      Top = 8
      Width = 165
      Height = 17
      Caption = 'Rolar itens ao gerar relat'#243'rio.'
      TabOrder = 4
    end
    object EdTempo: TdmkEdit
      Left = 508
      Top = 20
      Width = 97
      Height = 21
      Enabled = False
      TabOrder = 5
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfLong
      Texto = '00:00:00:000'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = '00:00:00:000'
      ValWarn = False
    end
  end
  object PageControl1: TPageControl
    Left = 0
    Top = 48
    Width = 1008
    Height = 465
    ActivePage = TabSheet1
    Align = alClient
    TabOrder = 3
    object TabSheet1: TTabSheet
      Caption = ' Configura'#231#227'o '
      object Splitter1: TSplitter
        Left = 973
        Top = 0
        Height = 437
        Align = alRight
        ExplicitLeft = 272
        ExplicitTop = 160
        ExplicitHeight = 100
      end
      object Panel1: TPanel
        Left = 0
        Top = 0
        Width = 973
        Height = 437
        Align = alClient
        BevelOuter = bvNone
        ParentBackground = False
        TabOrder = 0
        object Panel2: TPanel
          Left = 0
          Top = 0
          Width = 973
          Height = 420
          Align = alTop
          BevelOuter = bvLowered
          TabOrder = 0
          object Panel6: TPanel
            Left = 1
            Top = 1
            Width = 971
            Height = 256
            Align = alTop
            BevelOuter = bvNone
            TabOrder = 0
            object GroupBox5: TGroupBox
              Left = 477
              Top = 0
              Width = 494
              Height = 256
              Align = alClient
              Caption = ' Pedidos de Venda: '
              TabOrder = 0
              object GroupBox1: TGroupBox
                Left = 4
                Top = 16
                Width = 244
                Height = 65
                Caption = ' Per'#237'odo de inclus'#227'o: '
                TabOrder = 0
                object TPIncluIni: TdmkEditDateTimePicker
                  Left = 8
                  Top = 36
                  Width = 112
                  Height = 21
                  Date = 39892.420627939810000000
                  Time = 39892.420627939810000000
                  TabOrder = 1
                  OnClick = CkIncluIniClick
                  ReadOnly = False
                  DefaultEditMask = '!99/99/99;1;_'
                  AutoApplyEditMask = True
                  UpdType = utYes
                  DatePurpose = dmkdpNone
                end
                object TPIncluFim: TdmkEditDateTimePicker
                  Left = 124
                  Top = 36
                  Width = 112
                  Height = 21
                  Date = 39892.420627939810000000
                  Time = 39892.420627939810000000
                  TabOrder = 3
                  OnClick = CkIncluIniClick
                  ReadOnly = False
                  DefaultEditMask = '!99/99/99;1;_'
                  AutoApplyEditMask = True
                  UpdType = utYes
                  DatePurpose = dmkdpNone
                end
                object CkIncluIni: TCheckBox
                  Left = 8
                  Top = 16
                  Width = 77
                  Height = 17
                  Caption = 'Data inicial:'
                  Checked = True
                  Enabled = False
                  State = cbChecked
                  TabOrder = 0
                  OnClick = CkIncluIniClick
                end
                object CkIncluFim: TCheckBox
                  Left = 124
                  Top = 16
                  Width = 77
                  Height = 17
                  Caption = 'Data final:'
                  Checked = True
                  Enabled = False
                  State = cbChecked
                  TabOrder = 2
                  OnClick = CkIncluIniClick
                end
              end
              object GroupBox2: TGroupBox
                Left = 252
                Top = 16
                Width = 244
                Height = 65
                Caption = ' Per'#237'odo de emiss'#227'o: '
                TabOrder = 1
                object TPEmissIni: TdmkEditDateTimePicker
                  Left = 8
                  Top = 36
                  Width = 112
                  Height = 21
                  Date = 39892.420627939810000000
                  Time = 39892.420627939810000000
                  TabOrder = 1
                  OnClick = CkIncluIniClick
                  ReadOnly = False
                  DefaultEditMask = '!99/99/99;1;_'
                  AutoApplyEditMask = True
                  UpdType = utYes
                  DatePurpose = dmkdpNone
                end
                object TPEmissFim: TdmkEditDateTimePicker
                  Left = 124
                  Top = 36
                  Width = 112
                  Height = 21
                  Date = 39892.420627939810000000
                  Time = 39892.420627939810000000
                  TabOrder = 3
                  OnClick = CkIncluIniClick
                  ReadOnly = False
                  DefaultEditMask = '!99/99/99;1;_'
                  AutoApplyEditMask = True
                  UpdType = utYes
                  DatePurpose = dmkdpNone
                end
                object CkEmissIni: TCheckBox
                  Left = 8
                  Top = 16
                  Width = 77
                  Height = 17
                  Caption = 'Data inicial:'
                  Checked = True
                  Enabled = False
                  State = cbChecked
                  TabOrder = 0
                  OnClick = CkIncluIniClick
                end
                object CkEmissFim: TCheckBox
                  Left = 124
                  Top = 16
                  Width = 77
                  Height = 17
                  Caption = 'Data final:'
                  Checked = True
                  Enabled = False
                  State = cbChecked
                  TabOrder = 2
                  OnClick = CkIncluIniClick
                end
              end
              object GroupBox3: TGroupBox
                Left = 4
                Top = 84
                Width = 244
                Height = 65
                Caption = ' Per'#237'odo de chegada: '
                TabOrder = 2
                object TPEntraIni: TdmkEditDateTimePicker
                  Left = 8
                  Top = 36
                  Width = 112
                  Height = 21
                  Date = 39892.420627939810000000
                  Time = 39892.420627939810000000
                  TabOrder = 1
                  OnClick = CkIncluIniClick
                  ReadOnly = False
                  DefaultEditMask = '!99/99/99;1;_'
                  AutoApplyEditMask = True
                  UpdType = utYes
                  DatePurpose = dmkdpNone
                end
                object TPEntraFim: TdmkEditDateTimePicker
                  Left = 124
                  Top = 36
                  Width = 112
                  Height = 21
                  Date = 39892.420627939810000000
                  Time = 39892.420627939810000000
                  TabOrder = 3
                  OnClick = CkIncluIniClick
                  ReadOnly = False
                  DefaultEditMask = '!99/99/99;1;_'
                  AutoApplyEditMask = True
                  UpdType = utYes
                  DatePurpose = dmkdpNone
                end
                object CkEntraIni: TCheckBox
                  Left = 8
                  Top = 16
                  Width = 77
                  Height = 17
                  Caption = 'Data inicial:'
                  Checked = True
                  Enabled = False
                  State = cbChecked
                  TabOrder = 0
                  OnClick = CkIncluIniClick
                end
                object CkEntraFim: TCheckBox
                  Left = 124
                  Top = 16
                  Width = 77
                  Height = 17
                  Caption = 'Data final:'
                  Checked = True
                  Enabled = False
                  State = cbChecked
                  TabOrder = 2
                  OnClick = CkIncluIniClick
                end
              end
              object GroupBox4: TGroupBox
                Left = 252
                Top = 84
                Width = 244
                Height = 65
                Caption = ' Per'#237'odo de previs'#227'o de entrega: '
                TabOrder = 3
                object TPPreviIni: TdmkEditDateTimePicker
                  Left = 8
                  Top = 36
                  Width = 112
                  Height = 21
                  Date = 39892.420627939810000000
                  Time = 39892.420627939810000000
                  TabOrder = 1
                  OnClick = CkIncluIniClick
                  ReadOnly = False
                  DefaultEditMask = '!99/99/99;1;_'
                  AutoApplyEditMask = True
                  UpdType = utYes
                  DatePurpose = dmkdpNone
                end
                object TPPreviFim: TdmkEditDateTimePicker
                  Left = 124
                  Top = 36
                  Width = 112
                  Height = 21
                  Date = 39892.420627939810000000
                  Time = 39892.420627939810000000
                  TabOrder = 3
                  OnClick = CkIncluIniClick
                  ReadOnly = False
                  DefaultEditMask = '!99/99/99;1;_'
                  AutoApplyEditMask = True
                  UpdType = utYes
                  DatePurpose = dmkdpNone
                end
                object CkPreviIni: TCheckBox
                  Left = 8
                  Top = 16
                  Width = 77
                  Height = 17
                  Caption = 'Data inicial:'
                  Checked = True
                  Enabled = False
                  State = cbChecked
                  TabOrder = 0
                  OnClick = CkIncluIniClick
                end
                object CkPreviFim: TCheckBox
                  Left = 124
                  Top = 16
                  Width = 77
                  Height = 17
                  Caption = 'Data final:'
                  Checked = True
                  Enabled = False
                  State = cbChecked
                  TabOrder = 2
                  OnClick = CkIncluIniClick
                end
              end
              object GroupBox6: TGroupBox
                Left = 4
                Top = 152
                Width = 244
                Height = 45
                Caption = ' Prioridade: '
                TabOrder = 4
                object Label2: TLabel
                  Left = 112
                  Top = 20
                  Width = 24
                  Height = 13
                  Caption = '   '#224'   '
                end
                object dmkEdit1: TdmkEdit
                  Left = 8
                  Top = 16
                  Width = 80
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 0
                  FormatType = dmktfInteger
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0
                  ValWarn = False
                end
                object dmkEdit2: TdmkEdit
                  Left = 156
                  Top = 16
                  Width = 80
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 1
                  FormatType = dmktfInteger
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '99'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 99
                  ValWarn = False
                end
              end
              object GroupBox7: TGroupBox
                Left = 252
                Top = 152
                Width = 244
                Height = 45
                Caption = ' Prazo m'#233'dio: '
                TabOrder = 5
                object Label3: TLabel
                  Left = 112
                  Top = 20
                  Width = 24
                  Height = 13
                  Caption = '   '#224'   '
                end
                object dmkEdit3: TdmkEdit
                  Left = 8
                  Top = 16
                  Width = 80
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 0
                  FormatType = dmktfInteger
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0
                  ValWarn = False
                end
                object dmkEdit4: TdmkEdit
                  Left = 156
                  Top = 16
                  Width = 80
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 1
                  FormatType = dmktfInteger
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '180'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 180
                  ValWarn = False
                end
              end
            end
            object Panel10: TPanel
              Left = 0
              Top = 0
              Width = 477
              Height = 256
              Align = alLeft
              BevelOuter = bvLowered
              TabOrder = 1
              object LaSVPEmpr: TLabel
                Left = 8
                Top = 8
                Width = 45
                Height = 13
                Caption = 'Empresa:'
              end
              object LaSVPCent: TLabel
                Left = 8
                Top = 32
                Width = 80
                Height = 13
                Caption = 'Centro de estq.:'
              end
              object LaSVPProd: TLabel
                Left = 8
                Top = 152
                Width = 77
                Height = 13
                Caption = 'Grupo de prod.:'
              end
              object LaSVPClie: TLabel
                Left = 8
                Top = 56
                Width = 37
                Height = 13
                Caption = 'Cliente:'
              end
              object LaSVPRepr: TLabel
                Left = 8
                Top = 80
                Width = 76
                Height = 13
                Caption = 'Representante:'
              end
              object LaSVPRegi: TLabel
                Left = 8
                Top = 104
                Width = 37
                Height = 13
                Caption = 'Regi'#227'o:'
              end
              object LaSVPRegr: TLabel
                Left = 8
                Top = 128
                Width = 60
                Height = 13
                Caption = 'Regra fiscal:'
              end
              object LaGraAtrCad: TLabel
                Left = 8
                Top = 200
                Width = 87
                Height = 13
                Caption = 'Atributo de prod.:'
              end
              object LaSVPSitu: TLabel
                Left = 8
                Top = 176
                Width = 85
                Height = 13
                Caption = 'Situa'#231#227'o do ped.:'
              end
              object LaGraAtrIts: TLabel
                Left = 8
                Top = 224
                Width = 79
                Height = 13
                Caption = 'Item Atri. prod.:'
              end
              object EdSVPEmpr: TdmkEditCB
                Left = 96
                Top = 4
                Width = 48
                Height = 21
                Alignment = taRightJustify
                TabOrder = 0
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
                OnChange = EdSVPEmprChange
                DBLookupComboBox = CBSVPEmpr
                IgnoraDBLookupComboBox = False
                AutoSetIfOnlyOneReg = setregOnlyManual
              end
              object EdSVPCent: TdmkEditCB
                Left = 96
                Top = 28
                Width = 48
                Height = 21
                Alignment = taRightJustify
                TabOrder = 2
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                UpdCampo = 'Cliente'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
                OnChange = EdSVPCentChange
                DBLookupComboBox = CBSVPCent
                IgnoraDBLookupComboBox = False
                AutoSetIfOnlyOneReg = setregOnlyManual
              end
              object EdSVPProd: TdmkEditCB
                Left = 96
                Top = 148
                Width = 48
                Height = 21
                Alignment = taRightJustify
                TabOrder = 12
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
                OnChange = EdSVPProdChange
                DBLookupComboBox = CBSVPProd
                IgnoraDBLookupComboBox = False
                AutoSetIfOnlyOneReg = setregOnlyManual
              end
              object CBSVPProd: TdmkDBLookupComboBox
                Left = 144
                Top = 148
                Width = 328
                Height = 21
                KeyField = 'CodUsu'
                ListField = 'Nome'
                TabOrder = 13
                dmkEditCB = EdSVPProd
                UpdType = utYes
                LocF7SQLMasc = '$#'
                LocF7PreDefProc = f7pNone
              end
              object CBSVPCent: TdmkDBLookupComboBox
                Left = 144
                Top = 28
                Width = 328
                Height = 21
                KeyField = 'CodUsu'
                ListField = 'Nome'
                ListSource = DModG.DsStqCenCad
                TabOrder = 3
                dmkEditCB = EdSVPCent
                UpdType = utNil
                LocF7SQLMasc = '$#'
                LocF7PreDefProc = f7pNone
              end
              object CBSVPEmpr: TdmkDBLookupComboBox
                Left = 144
                Top = 4
                Width = 328
                Height = 21
                KeyField = 'Filial'
                ListField = 'NOMEFILIAL'
                ListSource = DModG.DsEmpresas
                TabOrder = 1
                dmkEditCB = EdSVPEmpr
                UpdType = utYes
                LocF7SQLMasc = '$#'
                LocF7PreDefProc = f7pNone
              end
              object EdSVPClie: TdmkEditCB
                Left = 96
                Top = 52
                Width = 48
                Height = 21
                Alignment = taRightJustify
                TabOrder = 4
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                UpdCampo = 'Cliente'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
                OnChange = EdSVPCentChange
                DBLookupComboBox = CBSVPClie
                IgnoraDBLookupComboBox = False
                AutoSetIfOnlyOneReg = setregOnlyManual
              end
              object CBSVPClie: TdmkDBLookupComboBox
                Left = 144
                Top = 52
                Width = 328
                Height = 21
                KeyField = 'CodUsu'
                ListField = 'NOMEENT'
                TabOrder = 5
                dmkEditCB = EdSVPClie
                UpdType = utNil
                LocF7SQLMasc = '$#'
                LocF7PreDefProc = f7pNone
              end
              object EdSVPRegi: TdmkEditCB
                Left = 96
                Top = 100
                Width = 48
                Height = 21
                Alignment = taRightJustify
                TabOrder = 8
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
                OnChange = EdSVPProdChange
                DBLookupComboBox = CBSVPRegi
                IgnoraDBLookupComboBox = False
                AutoSetIfOnlyOneReg = setregOnlyManual
              end
              object EdSVPRepr: TdmkEditCB
                Left = 96
                Top = 76
                Width = 48
                Height = 21
                Alignment = taRightJustify
                TabOrder = 6
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                UpdCampo = 'Cliente'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
                OnChange = EdSVPCentChange
                DBLookupComboBox = CBSVPRepr
                IgnoraDBLookupComboBox = False
                AutoSetIfOnlyOneReg = setregOnlyManual
              end
              object CBSVPRepr: TdmkDBLookupComboBox
                Left = 144
                Top = 76
                Width = 328
                Height = 21
                KeyField = 'CodUsu'
                ListField = 'NOMEENT'
                TabOrder = 7
                dmkEditCB = EdSVPRepr
                UpdType = utNil
                LocF7SQLMasc = '$#'
                LocF7PreDefProc = f7pNone
              end
              object CBSVPRegi: TdmkDBLookupComboBox
                Left = 144
                Top = 100
                Width = 328
                Height = 21
                KeyField = 'CodUsu'
                ListField = 'Nome'
                TabOrder = 9
                dmkEditCB = EdSVPRegi
                UpdType = utYes
                LocF7SQLMasc = '$#'
                LocF7PreDefProc = f7pNone
              end
              object EdSVPRegr: TdmkEditCB
                Left = 96
                Top = 124
                Width = 48
                Height = 21
                Alignment = taRightJustify
                TabOrder = 10
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                UpdCampo = 'Cliente'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
                OnChange = EdSVPCentChange
                DBLookupComboBox = CBSVPRegr
                IgnoraDBLookupComboBox = False
                AutoSetIfOnlyOneReg = setregOnlyManual
              end
              object CBSVPRegr: TdmkDBLookupComboBox
                Left = 144
                Top = 124
                Width = 328
                Height = 21
                KeyField = 'CodUsu'
                ListField = 'Nome'
                TabOrder = 11
                dmkEditCB = EdSVPRegr
                UpdType = utNil
                LocF7SQLMasc = '$#'
                LocF7PreDefProc = f7pNone
              end
              object EdGraAtrCad: TdmkEditCB
                Left = 96
                Top = 196
                Width = 48
                Height = 21
                Alignment = taRightJustify
                TabOrder = 16
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
                OnChange = EdGraAtrCadChange
                OnExit = EdGraAtrCadExit
                DBLookupComboBox = CBGraAtrCad
                IgnoraDBLookupComboBox = False
                AutoSetIfOnlyOneReg = setregOnlyManual
              end
              object CBGraAtrCad: TdmkDBLookupComboBox
                Left = 144
                Top = 196
                Width = 328
                Height = 21
                KeyField = 'CodUsu'
                ListField = 'Nome'
                TabOrder = 17
                dmkEditCB = EdGraAtrCad
                UpdType = utYes
                LocF7SQLMasc = '$#'
                LocF7PreDefProc = f7pNone
              end
              object EdSVPSitu: TdmkEditCB
                Left = 96
                Top = 172
                Width = 48
                Height = 21
                Alignment = taRightJustify
                TabOrder = 14
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
                OnChange = EdSVPProdChange
                DBLookupComboBox = CBSVPSitu
                IgnoraDBLookupComboBox = False
                AutoSetIfOnlyOneReg = setregOnlyManual
              end
              object CBSVPSitu: TdmkDBLookupComboBox
                Left = 144
                Top = 172
                Width = 328
                Height = 21
                KeyField = 'Codigo'
                ListField = 'Nome'
                TabOrder = 15
                dmkEditCB = EdSVPSitu
                UpdType = utYes
                LocF7SQLMasc = '$#'
                LocF7PreDefProc = f7pNone
              end
              object EdGraAtrIts: TdmkEditCB
                Left = 96
                Top = 220
                Width = 48
                Height = 21
                Alignment = taRightJustify
                TabOrder = 18
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
                OnChange = EdSVPProdChange
                DBLookupComboBox = CBGraAtrIts
                IgnoraDBLookupComboBox = False
                AutoSetIfOnlyOneReg = setregOnlyManual
              end
              object CBGraAtrIts: TdmkDBLookupComboBox
                Left = 144
                Top = 220
                Width = 328
                Height = 21
                KeyField = 'CodUsu'
                ListField = 'Nome'
                TabOrder = 19
                dmkEditCB = EdGraAtrIts
                UpdType = utYes
                LocF7SQLMasc = '$#'
                LocF7PreDefProc = f7pNone
              end
            end
          end
          object Panel7: TPanel
            Left = 1
            Top = 257
            Width = 971
            Height = 116
            Align = alTop
            BevelOuter = bvNone
            TabOrder = 1
            object GroupBox8: TGroupBox
              Left = 0
              Top = 0
              Width = 580
              Height = 116
              Align = alLeft
              Caption = ' Crit'#233'rios: '
              TabOrder = 0
              object GroupBox9: TGroupBox
                Left = 2
                Top = 15
                Width = 111
                Height = 99
                Align = alLeft
                Caption = ' Produto (%): '
                TabOrder = 0
                object Label4: TLabel
                  Left = 12
                  Top = 24
                  Width = 33
                  Height = 13
                  Caption = 'Grupo:'
                end
                object Label5: TLabel
                  Left = 12
                  Top = 48
                  Width = 42
                  Height = 13
                  Caption = 'Produto:'
                end
                object Label6: TLabel
                  Left = 12
                  Top = 72
                  Width = 21
                  Height = 13
                  Caption = 'Cor:'
                end
                object dmkEditCB1: TdmkEditCB
                  Left = 56
                  Top = 20
                  Width = 48
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 0
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 2
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ValMin = '-2147483647'
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0,00'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  ValWarn = False
                  OnChange = EdSVPEmprChange
                  DBLookupComboBox = CBSVPEmpr
                  IgnoraDBLookupComboBox = False
                  AutoSetIfOnlyOneReg = setregOnlyManual
                end
                object dmkEditCB2: TdmkEditCB
                  Left = 56
                  Top = 44
                  Width = 48
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 1
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 2
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ValMin = '-2147483647'
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0,00'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  ValWarn = False
                  OnChange = EdSVPEmprChange
                  DBLookupComboBox = CBSVPEmpr
                  IgnoraDBLookupComboBox = False
                  AutoSetIfOnlyOneReg = setregOnlyManual
                end
                object dmkEditCB3: TdmkEditCB
                  Left = 56
                  Top = 68
                  Width = 48
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 2
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 2
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ValMin = '-2147483647'
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0,00'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  ValWarn = False
                  OnChange = EdSVPEmprChange
                  DBLookupComboBox = CBSVPEmpr
                  IgnoraDBLookupComboBox = False
                  AutoSetIfOnlyOneReg = setregOnlyManual
                end
              end
              object GroupBox10: TGroupBox
                Left = 113
                Top = 15
                Width = 224
                Height = 99
                Align = alLeft
                Caption = ' Quantidade: '
                TabOrder = 1
                object Label7: TLabel
                  Left = 12
                  Top = 24
                  Width = 42
                  Height = 13
                  Caption = 'Vendida:'
                end
                object Label8: TLabel
                  Left = 12
                  Top = 48
                  Width = 50
                  Height = 13
                  Caption = 'Pendente:'
                end
                object Label9: TLabel
                  Left = 12
                  Top = 72
                  Width = 46
                  Height = 13
                  Caption = 'Sugerida:'
                end
                object Label13: TLabel
                  Left = 136
                  Top = 24
                  Width = 12
                  Height = 13
                  Caption = ' '#224' '
                end
                object Label14: TLabel
                  Left = 136
                  Top = 48
                  Width = 12
                  Height = 13
                  Caption = ' '#224' '
                end
                object Label15: TLabel
                  Left = 136
                  Top = 72
                  Width = 12
                  Height = 13
                  Caption = ' '#224' '
                end
                object dmkEditCB4: TdmkEditCB
                  Left = 68
                  Top = 20
                  Width = 64
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 0
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 3
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ValMin = '-2147483647'
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0,000'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  ValWarn = False
                  OnChange = EdSVPEmprChange
                  DBLookupComboBox = CBSVPEmpr
                  IgnoraDBLookupComboBox = False
                  AutoSetIfOnlyOneReg = setregOnlyManual
                end
                object dmkEditCB5: TdmkEditCB
                  Left = 68
                  Top = 44
                  Width = 64
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 1
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 3
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ValMin = '-2147483647'
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0,000'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  ValWarn = False
                  OnChange = EdSVPEmprChange
                  DBLookupComboBox = CBSVPEmpr
                  IgnoraDBLookupComboBox = False
                  AutoSetIfOnlyOneReg = setregOnlyManual
                end
                object dmkEditCB6: TdmkEditCB
                  Left = 68
                  Top = 68
                  Width = 64
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 2
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 3
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ValMin = '-2147483647'
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0,000'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  ValWarn = False
                  OnChange = EdSVPEmprChange
                  DBLookupComboBox = CBSVPEmpr
                  IgnoraDBLookupComboBox = False
                  AutoSetIfOnlyOneReg = setregOnlyManual
                end
                object dmkEditCB10: TdmkEditCB
                  Left = 152
                  Top = 20
                  Width = 64
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 3
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 3
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ValMin = '-2147483647'
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0,000'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  ValWarn = False
                  OnChange = EdSVPEmprChange
                  DBLookupComboBox = CBSVPEmpr
                  IgnoraDBLookupComboBox = False
                  AutoSetIfOnlyOneReg = setregOnlyManual
                end
                object dmkEditCB11: TdmkEditCB
                  Left = 152
                  Top = 44
                  Width = 64
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 4
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 3
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ValMin = '-2147483647'
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0,000'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  ValWarn = False
                  OnChange = EdSVPEmprChange
                  DBLookupComboBox = CBSVPEmpr
                  IgnoraDBLookupComboBox = False
                  AutoSetIfOnlyOneReg = setregOnlyManual
                end
                object dmkEditCB12: TdmkEditCB
                  Left = 152
                  Top = 68
                  Width = 64
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 5
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 3
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ValMin = '-2147483647'
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0,000'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  ValWarn = False
                  OnChange = EdSVPEmprChange
                  DBLookupComboBox = CBSVPEmpr
                  IgnoraDBLookupComboBox = False
                  AutoSetIfOnlyOneReg = setregOnlyManual
                end
              end
              object GroupBox11: TGroupBox
                Left = 337
                Top = 15
                Width = 240
                Height = 99
                Align = alLeft
                Caption = ' Valor ($): '
                TabOrder = 2
                object Label10: TLabel
                  Left = 12
                  Top = 24
                  Width = 42
                  Height = 13
                  Caption = 'Vendida:'
                end
                object Label11: TLabel
                  Left = 12
                  Top = 48
                  Width = 50
                  Height = 13
                  Caption = 'Pendente:'
                end
                object Label12: TLabel
                  Left = 12
                  Top = 72
                  Width = 46
                  Height = 13
                  Caption = 'Sugerida:'
                end
                object Label16: TLabel
                  Left = 144
                  Top = 24
                  Width = 12
                  Height = 13
                  Caption = ' '#224' '
                end
                object Label17: TLabel
                  Left = 144
                  Top = 48
                  Width = 12
                  Height = 13
                  Caption = ' '#224' '
                end
                object Label18: TLabel
                  Left = 144
                  Top = 72
                  Width = 12
                  Height = 13
                  Caption = ' '#224' '
                end
                object dmkEditCB7: TdmkEditCB
                  Left = 68
                  Top = 68
                  Width = 72
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 0
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 2
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ValMin = '-2147483647'
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0,00'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  ValWarn = False
                  OnChange = EdSVPEmprChange
                  DBLookupComboBox = CBSVPEmpr
                  IgnoraDBLookupComboBox = False
                  AutoSetIfOnlyOneReg = setregOnlyManual
                end
                object dmkEditCB8: TdmkEditCB
                  Left = 68
                  Top = 44
                  Width = 72
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 1
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 2
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ValMin = '-2147483647'
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0,00'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  ValWarn = False
                  OnChange = EdSVPEmprChange
                  DBLookupComboBox = CBSVPEmpr
                  IgnoraDBLookupComboBox = False
                  AutoSetIfOnlyOneReg = setregOnlyManual
                end
                object dmkEditCB9: TdmkEditCB
                  Left = 68
                  Top = 20
                  Width = 72
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 2
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 2
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ValMin = '-2147483647'
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0,00'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  ValWarn = False
                  OnChange = EdSVPEmprChange
                  DBLookupComboBox = CBSVPEmpr
                  IgnoraDBLookupComboBox = False
                  AutoSetIfOnlyOneReg = setregOnlyManual
                end
                object dmkEditCB13: TdmkEditCB
                  Left = 160
                  Top = 68
                  Width = 72
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 3
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 2
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ValMin = '-2147483647'
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0,00'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  ValWarn = False
                  OnChange = EdSVPEmprChange
                  DBLookupComboBox = CBSVPEmpr
                  IgnoraDBLookupComboBox = False
                  AutoSetIfOnlyOneReg = setregOnlyManual
                end
                object dmkEditCB14: TdmkEditCB
                  Left = 160
                  Top = 44
                  Width = 72
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 4
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 2
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ValMin = '-2147483647'
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0,00'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  ValWarn = False
                  OnChange = EdSVPEmprChange
                  DBLookupComboBox = CBSVPEmpr
                  IgnoraDBLookupComboBox = False
                  AutoSetIfOnlyOneReg = setregOnlyManual
                end
                object dmkEditCB15: TdmkEditCB
                  Left = 160
                  Top = 20
                  Width = 72
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 5
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 2
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ValMin = '-2147483647'
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0,00'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  ValWarn = False
                  OnChange = EdSVPEmprChange
                  DBLookupComboBox = CBSVPEmpr
                  IgnoraDBLookupComboBox = False
                  AutoSetIfOnlyOneReg = setregOnlyManual
                end
              end
            end
            object GroupBox12: TGroupBox
              Left = 580
              Top = 0
              Width = 391
              Height = 116
              Align = alClient
              Caption = ' Ordena'#231#227'o: '
              TabOrder = 1
              object RadioGroup1: TRadioGroup
                Left = 2
                Top = 15
                Width = 95
                Height = 99
                Align = alLeft
                Caption = ' Ordem 1: '
                ItemIndex = 0
                Items.Strings = (
                  'Emiss'#227'o'#9
                  'Prioridade'#9
                  'Prazo m'#233'dio'#9
                  'Entrega'#9)
                TabOrder = 0
              end
              object RadioGroup2: TRadioGroup
                Left = 97
                Top = 15
                Width = 95
                Height = 99
                Align = alLeft
                Caption = ' Ordem 2: '
                ItemIndex = 1
                Items.Strings = (
                  'Emiss'#227'o'#9
                  'Prioridade'#9
                  'Prazo m'#233'dio'#9
                  'Entrega'#9)
                TabOrder = 1
              end
              object RadioGroup3: TRadioGroup
                Left = 192
                Top = 15
                Width = 95
                Height = 99
                Align = alLeft
                Caption = ' Ordem 3: '
                ItemIndex = 2
                Items.Strings = (
                  'Emiss'#227'o'#9
                  'Prioridade'#9
                  'Prazo m'#233'dio'#9
                  'Entrega'#9)
                TabOrder = 2
              end
              object RadioGroup4: TRadioGroup
                Left = 287
                Top = 15
                Width = 95
                Height = 99
                Align = alLeft
                Caption = ' Ordem 4: '
                ItemIndex = 3
                Items.Strings = (
                  'Emiss'#227'o'#9
                  'Prioridade'#9
                  'Prazo m'#233'dio'#9
                  'Entrega'#9)
                TabOrder = 3
              end
            end
          end
        end
        object Panel8: TPanel
          Left = 0
          Top = 420
          Width = 973
          Height = 17
          Align = alBottom
          BevelOuter = bvLowered
          TabOrder = 1
          object LaAviso1: TLabel
            Left = 1
            Top = 3
            Width = 971
            Height = 13
            Align = alBottom
            Alignment = taCenter
            Caption = '...'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clRed
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
            ExplicitWidth = 9
          end
        end
      end
      object DockTabSet1: TTabSet
        Left = 976
        Top = 0
        Width = 20
        Height = 437
        Align = alRight
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ShrinkToFit = True
        Style = tsModernPopout
        TabPosition = tpLeft
        DockSite = False
        DestinationDockSite = pDockLeft
        OnDockDrop = DockTabSet1DockDrop
        OnTabAdded = DockTabSet1TabAdded
        OnTabRemoved = DockTabSet1TabRemoved
      end
      object Panel5: TPanel
        Left = 996
        Top = 0
        Width = 4
        Height = 437
        Align = alRight
        BevelOuter = bvNone
        ParentBackground = False
        TabOrder = 2
      end
    end
    object TabSheet2: TTabSheet
      Caption = ' Resultado '
      ImageIndex = 1
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
    end
  end
  object frxPED_INPRI_000_01: TfrxReport
    Version = '5.6.18'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39596.679376979200000000
    ReportOptions.LastChange = 39596.679376979200000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'var'
      '  Fmt: String;                                  '
      'begin'
      
        '  MeVARF_CODI_FRX.Text := Copy(Report.Name, 4, Length(Report.Nam' +
        'e));'
      '  //        '
      '  Fmt := <VARF_FMT>;                        '
      
        '  Memo17.Memo.Text := '#39'[FormatFloat('#39#39#39' + Fmt + '#39#39#39',<frxDsGgx."E' +
        'stq">)]'#39';'
      
        '  Memo10.Memo.Text := '#39'[FormatFloat('#39#39#39' + Fmt + '#39#39#39',<frxDsGgx."P' +
        'edv">)]'#39';'
      
        '  Memo9.Memo.Text  := '#39'[FormatFloat('#39#39#39' + Fmt + '#39#39#39',<frxDsGgx."N' +
        'ec1">)]'#39';'
      
        '  Memo12.Memo.Text := '#39'[FormatFloat('#39#39#39' + Fmt + '#39#39#39',<frxDsGgx."C' +
        'omp">)]'#39';'
      
        '  Memo8.Memo.Text  := '#39'[FormatFloat('#39#39#39' + Fmt + '#39#39#39',<frxDsGgx."P' +
        'rdz">)]'#39';'
      
        '  Memo42.Memo.Text := '#39'[FormatFloat('#39#39#39' + Fmt + '#39#39#39',<frxDsGgx."N' +
        'ec2">)]'#39';'
      
        '  Memo23.Memo.Text := '#39'[FormatFloat('#39#39#39' + Fmt + '#39#39#39',<frxDsGgx."S' +
        'obr">)]'#39';'
      '  //'
      
        '  Memo40.Memo.Text := '#39'[FormatFloat('#39#39#39' + Fmt +'#39#39#39',SUM(<frxDsGgx' +
        '."Estq">))]'#39';'
      
        '  Memo38.Memo.Text := '#39'[FormatFloat('#39#39#39' + Fmt +'#39#39#39',SUM(<frxDsGgx' +
        '."Pedv">))]'#39';'
      
        '  Memo37.Memo.Text := '#39'[FormatFloat('#39#39#39' + Fmt +'#39#39#39',SUM(<frxDsGgx' +
        '."Nec1">))]'#39';'
      
        '  Memo39.Memo.Text := '#39'[FormatFloat('#39#39#39' + Fmt +'#39#39#39',SUM(<frxDsGgx' +
        '."Comp">))]'#39';'
      
        '  Memo25.Memo.Text := '#39'[FormatFloat('#39#39#39' + Fmt +'#39#39#39',SUM(<frxDsGgx' +
        '."Prdz">))]'#39';'
      
        '  Memo43.Memo.Text := '#39'[FormatFloat('#39#39#39' + Fmt +'#39#39#39',SUM(<frxDsGgx' +
        '."Nec2">))]'#39';'
      
        '  Memo24.Memo.Text := '#39'[FormatFloat('#39#39#39' + Fmt +'#39#39#39',SUM(<frxDsGgx' +
        '."Sobr">))]'#39';'
      '  //        '
      'end.')
    OnGetValue = frxPED_INPRI_000_01GetValue
    Left = 692
    Top = 8
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsGgx
        DataSetName = 'frxDsGgx'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 15.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        Height = 41.574830000000000000
        Top = 56.692950000000000000
        Width = 699.213050000000000000
        object Shape1: TfrxShapeView
          Width = 699.213050000000000000
          Height = 37.795300000000000000
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo13: TfrxMemoView
          Left = 7.559060000000000000
          Width = 684.094930000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line1: TfrxLineView
          Top = 18.897650000000000000
          Width = 699.213050000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo14: TfrxMemoView
          Left = 120.944960000000000000
          Top = 18.897650000000000000
          Width = 457.323130000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'CORTE SIMPLES')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo15: TfrxMemoView
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 113.385900000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'impresso em [Date], [Time]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo16: TfrxMemoView
          Left = 578.267716540000000000
          Top = 18.897650000000000000
          Width = 113.385826770000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object ReportTitle1: TfrxReportTitle
        FillType = ftBrush
        Height = 13.228346460000000000
        Top = 18.897650000000000000
        Width = 699.213050000000000000
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        Height = 15.118110236220470000
        Top = 306.141930000000000000
        Width = 699.213050000000000000
        DataSet = frxDsGgx
        DataSetName = 'frxDsGgx'
        RowCount = 0
        object Memo3: TfrxMemoView
          Left = 45.354360000000000000
          Width = 60.472480000000000000
          Height = 15.118110236220470000
          DataSet = frxDsGgx
          DataSetName = 'frxDsGgx'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsGgx."NO_TAM"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo23: TfrxMemoView
          Left = 604.724800000000000000
          Width = 94.488196300000000000
          Height = 15.118110236220470000
          DataSet = frxDsGgx
          DataSetName = 'frxDsGgx'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo8: TfrxMemoView
          Left = 438.425480000000000000
          Width = 83.149606300000000000
          Height = 15.118110236220470000
          DataSet = frxDsGgx
          DataSetName = 'frxDsGgx'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo9: TfrxMemoView
          Left = 272.126160000000000000
          Width = 83.149606300000000000
          Height = 15.118110236220470000
          DataSet = frxDsGgx
          DataSetName = 'frxDsGgx'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo10: TfrxMemoView
          Left = 188.976500000000000000
          Width = 83.149606300000000000
          Height = 15.118110236220470000
          DataSet = frxDsGgx
          DataSetName = 'frxDsGgx'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo12: TfrxMemoView
          Left = 355.275820000000000000
          Width = 83.149606300000000000
          Height = 15.118110236220470000
          DataSet = frxDsGgx
          DataSetName = 'frxDsGgx'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo17: TfrxMemoView
          Left = 105.826840000000000000
          Width = 83.149606300000000000
          Height = 15.118110236220470000
          DataSet = frxDsGgx
          DataSetName = 'frxDsGgx'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo42: TfrxMemoView
          Left = 521.575140000000000000
          Width = 83.149606300000000000
          Height = 15.118110236220470000
          DataSet = frxDsGgx
          DataSetName = 'frxDsGgx'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        Height = 13.228346460000000000
        Top = 449.764070000000000000
        Width = 699.213050000000000000
        object Memo1: TfrxMemoView
          Left = 7.559060000000000000
          Width = 362.834880000000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'www.dermatek.com.br - software customizado')
          ParentFont = False
          VAlign = vaCenter
        end
        object MeVARF_CODI_FRX: TfrxMemoView
          Left = 370.393940000000000000
          Width = 321.260050000000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'VARF_CODI_FRX')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object MasterData3: TfrxMasterData
        FillType = ftBrush
        Height = 30.125996460000000000
        Top = 158.740260000000000000
        Width = 699.213050000000000000
        RowCount = 1
        object Memo29: TfrxMemoView
          Width = 177.637795280000000000
          Height = 13.228346460000000000
          DataSetName = 'frxDsPCI'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Data de inclus'#227'o')
          ParentFont = False
        end
        object Memo30: TfrxMemoView
          Top = 13.118120000000000000
          Width = 177.637795280000000000
          Height = 13.228346460000000000
          DataSetName = 'frxDsPCI'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_PERIODO_INCLU]')
          ParentFont = False
        end
        object Memo31: TfrxMemoView
          Left = 177.637910000000000000
          Width = 173.858267720000000000
          Height = 13.228346460000000000
          DataSetName = 'frxDsPCI'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Data de emiss'#227'o')
          ParentFont = False
        end
        object Memo32: TfrxMemoView
          Left = 177.637910000000000000
          Top = 13.118120000000000000
          Width = 173.858267720000000000
          Height = 13.228346460000000000
          DataSetName = 'frxDsPCI'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_PERIODO_EMISS]')
          ParentFont = False
        end
        object Memo33: TfrxMemoView
          Left = 351.496290000000000000
          Width = 173.858267720000000000
          Height = 13.228346460000000000
          DataSetName = 'frxDsPCI'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Data de chegada')
          ParentFont = False
        end
        object Memo34: TfrxMemoView
          Left = 351.496290000000000000
          Top = 13.118120000000000000
          Width = 173.858267720000000000
          Height = 13.228346460000000000
          DataSetName = 'frxDsPCI'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_PERIODO_ENTRA]')
          ParentFont = False
        end
        object Memo35: TfrxMemoView
          Left = 525.354670000000000000
          Width = 173.858267720000000000
          Height = 13.228346460000000000
          DataSetName = 'frxDsPCI'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Previs'#227'o de entrega')
          ParentFont = False
        end
        object Memo36: TfrxMemoView
          Left = 525.354670000000000000
          Top = 13.118120000000000000
          Width = 173.858267720000000000
          Height = 13.228346460000000000
          DataSetName = 'frxDsPCI'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_PERIODO_PREVI]')
          ParentFont = False
        end
      end
      object GroupHeader1: TfrxGroupHeader
        FillType = ftBrush
        Height = 20.787404020000000000
        Top = 211.653680000000000000
        Width = 699.213050000000000000
        Condition = 'frxDsGgx."CU_NIVEL1"'
        object Memo2: TfrxMemoView
          Top = 3.779530000000000000
          Width = 37.795300000000000000
          Height = 15.118110240000000000
          DataSet = FmPediVdaImp.frxDs1
          DataSetName = 'frxDs1'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Grupo:')
          ParentFont = False
          WordWrap = False
        end
        object Memo4: TfrxMemoView
          Left = 37.795300000000000000
          Top = 3.779530000000000000
          Width = 94.488250000000000000
          Height = 15.118110240000000000
          DataSet = FmPediVdaImp.frxDs1
          DataSetName = 'frxDs1'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsGgx."CU_NIVEL1"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo5: TfrxMemoView
          Left = 132.283550000000000000
          Top = 3.779530000000000000
          Width = 510.236550000000000000
          Height = 15.118110240000000000
          DataSet = FmPediVdaImp.frxDs1
          DataSetName = 'frxDs1'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsGgx."NO_NIVEL1"]')
          ParentFont = False
          WordWrap = False
        end
      end
      object GroupFooter1: TfrxGroupFooter
        FillType = ftBrush
        Height = 7.559060000000000000
        Top = 381.732530000000000000
        Width = 699.213050000000000000
        object Memo20: TfrxMemoView
          Width = 699.213050000000000000
          Height = 7.559060000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GroupFooter2: TfrxGroupFooter
        FillType = ftBrush
        Height = 15.118110236220470000
        Top = 343.937230000000000000
        Width = 699.213050000000000000
        object Memo22: TfrxMemoView
          Left = 45.354360000000000000
          Width = 60.472480000000000000
          Height = 15.118110236220470000
          DataSet = frxDsGgx
          DataSetName = 'frxDsGgx'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'T O T A L')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo24: TfrxMemoView
          Left = 604.724800000000000000
          Width = 94.488196300000000000
          Height = 15.118110236220470000
          DataSet = frxDsGgx
          DataSetName = 'frxDsGgx'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo25: TfrxMemoView
          Left = 438.425480000000000000
          Width = 83.149606300000000000
          Height = 15.118110236220470000
          DataSet = frxDsGgx
          DataSetName = 'frxDsGgx'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo37: TfrxMemoView
          Left = 272.126160000000000000
          Width = 83.149606300000000000
          Height = 15.118110236220470000
          DataSet = frxDsGgx
          DataSetName = 'frxDsGgx'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo38: TfrxMemoView
          Left = 188.976500000000000000
          Width = 83.149606300000000000
          Height = 15.118110236220470000
          DataSet = frxDsGgx
          DataSetName = 'frxDsGgx'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo39: TfrxMemoView
          Left = 355.275820000000000000
          Width = 83.149606300000000000
          Height = 15.118110236220470000
          DataSet = frxDsGgx
          DataSetName = 'frxDsGgx'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo40: TfrxMemoView
          Left = 105.826840000000000000
          Width = 83.149606300000000000
          Height = 15.118110236220470000
          DataSet = frxDsGgx
          DataSetName = 'frxDsGgx'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo43: TfrxMemoView
          Left = 521.575140000000000000
          Width = 83.149606300000000000
          Height = 15.118110236220470000
          DataSet = frxDsGgx
          DataSetName = 'frxDsGgx'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GroupHeader2: TfrxGroupHeader
        FillType = ftBrush
        Height = 26.456690480000000000
        Top = 257.008040000000000000
        Width = 699.213050000000000000
        Condition = 'frxDsGgx."CU_COR"'
        object Memo11: TfrxMemoView
          Left = 45.354360000000000000
          Top = 13.228344020000000000
          Width = 60.472480000000000000
          Height = 13.228346460000000000
          DataSet = FmPediVdaImp.frxDs1
          DataSetName = 'frxDs1'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Tamanho')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo26: TfrxMemoView
          Left = 272.126160000000000000
          Top = 13.228344020000000000
          Width = 83.149606300000000000
          Height = 13.228346460000000000
          DataSet = FmPediVdaImp.frxDs1
          DataSetName = 'frxDs1'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Saldo 1')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo27: TfrxMemoView
          Left = 438.425480000000000000
          Top = 13.228344020000000000
          Width = 83.149606300000000000
          Height = 13.228346460000000000
          DataSet = FmPediVdaImp.frxDs1
          DataSetName = 'frxDs1'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Produ'#231#227'o')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo28: TfrxMemoView
          Left = 604.724800000000000000
          Top = 13.228344020000000000
          Width = 94.488196300000000000
          Height = 13.228346460000000000
          DataSet = FmPediVdaImp.frxDs1
          DataSetName = 'frxDs1'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Sobras')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo6: TfrxMemoView
          Left = 37.795300000000000000
          Width = 56.692950000000000000
          Height = 13.228346460000000000
          DataSet = FmPediVdaImp.frxDs1
          DataSetName = 'frxDs1'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsGgx."CU_COR"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo7: TfrxMemoView
          Left = 94.488250000000000000
          Width = 548.031850000000000000
          Height = 13.228346460000000000
          DataSet = FmPediVdaImp.frxDs1
          DataSetName = 'frxDs1'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsGgx."NO_COR"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo18: TfrxMemoView
          Left = 105.826840000000000000
          Top = 13.228344020000000000
          Width = 83.149606300000000000
          Height = 13.228346460000000000
          DataSet = FmPediVdaImp.frxDs1
          DataSetName = 'frxDs1'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Estoque')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo19: TfrxMemoView
          Left = 355.275820000000000000
          Top = 13.228344020000000000
          Width = 83.149606300000000000
          Height = 13.228346460000000000
          DataSet = FmPediVdaImp.frxDs1
          DataSetName = 'frxDs1'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Compras')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo21: TfrxMemoView
          Left = 188.976500000000000000
          Top = 13.228344020000000000
          Width = 83.149606300000000000
          Height = 13.228346460000000000
          DataSet = FmPediVdaImp.frxDs1
          DataSetName = 'frxDs1'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Pedido pendente')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo41: TfrxMemoView
          Left = 521.575140000000000000
          Top = 13.228346460000000000
          Width = 83.149606300000000000
          Height = 13.228346460000000000
          DataSet = FmPediVdaImp.frxDs1
          DataSetName = 'frxDs1'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'A produzir')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo44: TfrxMemoView
          Left = 15.118120000000000000
          Width = 22.677180000000000000
          Height = 13.228346460000000000
          DataSet = FmPediVdaImp.frxDs1
          DataSetName = 'frxDs1'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Cor:')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
    end
  end
  object QrGgx: TMySQLQuery
    Database = Dmod.ZZDB
    AfterOpen = QrGgxAfterOpen
    BeforeClose = QrGgxBeforeClose
    SQL.Strings = (
      'SELECT gg1.CodUsu CU_NIVEL1, gg1.Nome NO_NIVEL1,'
      'gti.Controle CO_TAM, gti.Nome NO_TAM,'
      'gcc.CodUsu CU_COR, gcc.Nome NO_COR, pci.*'
      'FROM pci1 pci'
      'LEFT JOIN planning.gragru1 gg1 ON gg1.Nivel1=pci.GraGru1'
      'LEFT JOIN planning.gratamits gti ON gti.Controle=pci.GraTamI'
      'LEFT JOIN planning.gratamcad gtc ON gtc.Codigo=gti.Codigo'
      'LEFT JOIN planning.gragruc   ggc ON ggc.Controle=pci.GraGruC'
      'LEFT JOIN planning.gracorcad gcc ON gcc.Codigo=ggc.GraCorCad'
      'ORDER BY NO_NIVEL1, NO_NIVEL1, CO_TAM')
    Left = 720
    Top = 8
    object QrGgxCU_NIVEL1: TIntegerField
      FieldName = 'CU_NIVEL1'
    end
    object QrGgxNO_NIVEL1: TWideStringField
      FieldName = 'NO_NIVEL1'
      Size = 30
    end
    object QrGgxCO_TAM: TIntegerField
      FieldName = 'CO_TAM'
    end
    object QrGgxNO_TAM: TWideStringField
      FieldName = 'NO_TAM'
      Size = 5
    end
    object QrGgxCU_COR: TIntegerField
      FieldName = 'CU_COR'
    end
    object QrGgxNO_COR: TWideStringField
      FieldName = 'NO_COR'
      Size = 30
    end
    object QrGgxGraGruC: TIntegerField
      FieldName = 'GraGruC'
      Required = True
    end
    object QrGgxGraGru1: TIntegerField
      FieldName = 'GraGru1'
      Required = True
    end
    object QrGgxGraTamI: TIntegerField
      FieldName = 'GraTamI'
      Required = True
    end
    object QrGgxControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrGgxEstq: TFloatField
      FieldName = 'Estq'
      Required = True
    end
    object QrGgxPedv: TFloatField
      FieldName = 'Pedv'
      Required = True
    end
    object QrGgxNec1: TFloatField
      FieldName = 'Nec1'
      Required = True
    end
    object QrGgxComp: TFloatField
      FieldName = 'Comp'
      Required = True
    end
    object QrGgxPrdz: TFloatField
      FieldName = 'Prdz'
      Required = True
    end
    object QrGgxNec2: TFloatField
      FieldName = 'Nec2'
      Required = True
    end
    object QrGgxSobr: TFloatField
      FieldName = 'Sobr'
      Required = True
    end
    object QrGgxAtivo: TIntegerField
      FieldName = 'Ativo'
      Required = True
    end
  end
  object DsGgx: TDataSource
    DataSet = QrGgx
    Left = 748
    Top = 8
  end
  object frxDsGgx: TfrxDBDataset
    UserName = 'frxDsGgx'
    CloseDataSource = False
    DataSet = QrGgx
    BCDToCurrency = False
    Left = 776
    Top = 8
  end
end
