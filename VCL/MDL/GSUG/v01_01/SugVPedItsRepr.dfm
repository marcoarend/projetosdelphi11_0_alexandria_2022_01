object FmSugVPedItsRepr: TFmSugVPedItsRepr
  Left = 339
  Top = 185
  BorderIcons = [biMinimize, biMaximize]
  Caption = 'Representantes'
  ClientHeight = 375
  ClientWidth = 372
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnCreate = FormCreate
  OnDeactivate = FormDeactivate
  OnShow = FormShow
  OnStartDock = FormStartDock
  OnUnDock = FormUnDock
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 372
    Height = 375
    Align = alClient
    TabOrder = 0
    object Panel2: TPanel
      Left = 1
      Top = 326
      Width = 370
      Height = 48
      Align = alBottom
      TabOrder = 0
      object BtNenhum: TBitBtn
        Tag = 128
        Left = 271
        Top = 4
        Width = 90
        Height = 40
        Hint = 'Desmarca todos itens'
        Caption = '&Nenhum'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtNenhumClick
      end
      object BtTodos: TBitBtn
        Tag = 127
        Left = 7
        Top = 4
        Width = 90
        Height = 40
        Hint = 'Marca todos itens'
        Caption = '&Todos'
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtTodosClick
      end
      object BtPesq: TBitBtn
        Tag = 241
        Left = 123
        Top = 4
        Width = 120
        Height = 40
        Hint = 'Marca todos itens'
        Caption = '&Pesquisados'
        NumGlyphs = 2
        TabOrder = 2
        OnClick = BtPesqClick
      end
    end
    object PageControl1: TPageControl
      Left = 1
      Top = 1
      Width = 370
      Height = 325
      ActivePage = TabSheet2
      Align = alClient
      TabOrder = 1
      OnChange = PageControl1Change
      object TabSheet1: TTabSheet
        Caption = ' Pesquisa '
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object Panel3: TPanel
          Left = 0
          Top = 0
          Width = 362
          Height = 48
          Align = alTop
          ParentBackground = False
          TabOrder = 0
          object Label1: TLabel
            Left = 16
            Top = 4
            Width = 201
            Height = 13
            Caption = 'Informe parte da descri'#231#227'o para pesquisar:'
          end
          object EdPesq: TEdit
            Left = 16
            Top = 20
            Width = 325
            Height = 21
            TabOrder = 0
            OnChange = EdPesqChange
          end
        end
        object DBGrid1: TdmkDBGrid
          Left = 0
          Top = 48
          Width = 362
          Height = 249
          Align = alClient
          Columns = <
            item
              Expanded = False
              FieldName = 'Ativo'
              Title.Caption = 'OK'
              Width = 20
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'CodUsu'
              Title.Caption = 'C'#243'digo'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Nome'
              Title.Caption = 'Descri'#231#227'o'
              Width = 240
              Visible = True
            end>
          Color = clWindow
          DataSource = DmSugVda.DsSVPReprIts
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
          TabOrder = 1
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          OnCellClick = DBGrid1CellClick
          Columns = <
            item
              Expanded = False
              FieldName = 'Ativo'
              Title.Caption = 'OK'
              Width = 20
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'CodUsu'
              Title.Caption = 'C'#243'digo'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Nome'
              Title.Caption = 'Descri'#231#227'o'
              Width = 240
              Visible = True
            end>
        end
      end
      object TabSheet2: TTabSheet
        Caption = ' Selecionados '
        ImageIndex = 1
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object Panel4: TPanel
          Left = 0
          Top = 0
          Width = 362
          Height = 48
          Align = alTop
          ParentBackground = False
          TabOrder = 0
          object RGSelecao: TRadioGroup
            Left = 1
            Top = 1
            Width = 360
            Height = 46
            Align = alClient
            Caption = ' Status dos itens: '
            Columns = 3
            Items.Strings = (
              'N'#227'o selecionados'
              'Selecinodos'
              'Todos')
            TabOrder = 0
            OnClick = RGSelecaoClick
          end
        end
        object DBGrid2: TdmkDBGrid
          Left = 0
          Top = 48
          Width = 362
          Height = 249
          Align = alClient
          Columns = <
            item
              Expanded = False
              FieldName = 'Ativo'
              Title.Caption = 'OK'
              Width = 20
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'CodUsu'
              Title.Caption = 'C'#243'digo'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Nome'
              Title.Caption = 'Descri'#231#227'o'
              Width = 240
              Visible = True
            end>
          Color = clWindow
          DataSource = DmSugVda.DsSVPReprIts
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
          TabOrder = 1
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          OnCellClick = DBGrid1CellClick
          Columns = <
            item
              Expanded = False
              FieldName = 'Ativo'
              Title.Caption = 'OK'
              Width = 20
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'CodUsu'
              Title.Caption = 'C'#243'digo'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Nome'
              Title.Caption = 'Descri'#231#227'o'
              Width = 240
              Visible = True
            end>
        end
      end
    end
  end
  object PMPesq: TPopupMenu
    Left = 44
    Top = 116
    object Ativa1: TMenuItem
      Caption = '&Ativa'
      OnClick = Ativa1Click
    end
    object Desativa1: TMenuItem
      Caption = '&Desativa'
      OnClick = Desativa1Click
    end
  end
end
