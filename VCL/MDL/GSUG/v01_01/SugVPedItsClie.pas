unit SugVPedItsClie;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DB,
  DBGrids, dmkDBGrid, DBCtrls, mySQLDbTables, Menus, ComCtrls, UnDmkProcFunc;

type
  THackDBGrid = class(TDBGrid);
  TFmSugVPedItsClie = class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    BtNenhum: TBitBtn;
    BtTodos: TBitBtn;
    BtPesq: TBitBtn;
    PMPesq: TPopupMenu;
    Ativa1: TMenuItem;
    Desativa1: TMenuItem;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    Panel3: TPanel;
    Label1: TLabel;
    EdPesq: TEdit;
    DBGrid1: TdmkDBGrid;
    Panel4: TPanel;
    DBGrid2: TdmkDBGrid;
    RGSelecao: TRadioGroup;
    procedure FormCreate(Sender: TObject);
    procedure DBGrid1CellClick(Column: TColumn);
    procedure FormStartDock(Sender: TObject; var DragObject: TDragDockObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure EdPesqChange(Sender: TObject);
    procedure FormUnDock(Sender: TObject; Client: TControl;
      NewTarget: TWinControl; var Allow: Boolean);
    procedure BtNenhumClick(Sender: TObject);
    procedure BtTodosClick(Sender: TObject);
    procedure BtPesqClick(Sender: TObject);
    procedure Ativa1Click(Sender: TObject);
    procedure Desativa1Click(Sender: TObject);
    procedure PageControl1Change(Sender: TObject);
    procedure FormDeactivate(Sender: TObject);
    procedure RGSelecaoClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    FSVPClie: String;
    procedure Pesquisar();
    procedure AtivaItens(Status: Integer);
    procedure AtivaSelecionados(Status: Integer);
    procedure HabilitaItemUnico();
  public
    { Public declarations }
    class function CreateDockForm(const aColor: TColor): TCustomForm;
    //class procedure FiltaAtivos();
    procedure LimpaPesquisa();
  end;

var
  FmSugVPedItsClie: TFmSugVPedItsClie;

implementation

uses UnMyObjects, Module, UMySQLModule, SugVPedIts, ModSugVda, ModuleGeral, UCreate,
UnInternalConsts, MyGlyfs;

{$R *.DFM}

///// PROCEDURES E FUNCTIONS DO DOCKFORM

procedure TFmSugVPedItsClie.Ativa1Click(Sender: TObject);
begin
  AtivaSelecionados(1);
end;

procedure TFmSugVPedItsClie.AtivaItens(Status: Integer);
var
  Codigo: Integer;
begin
  Screen.Cursor := crHourGlass;
  Codigo := DmSugVda.TbSVPClieItsCodigo.Value;
  DModG.QrUpdPID1.SQL.Clear;
  DModG.QrUpdPID1.SQL.Add('UPDATE ' + FSVPClie + ' SET Ativo=' + dmkPF.FFP(Status, 0) + ';');
  UMyMod.ExecutaQuery(DmodG.QrUpdPID1);
  DmSugVda.TbSVPClieIts.Close;
  DmSugVda.TbSVPClieIts.Open;
  DmSugVda.TbSVPClieIts.Locate('Codigo', Codigo, []);
  HabilitaItemUnico();
  Screen.Cursor := crDefault;
end;

procedure TFmSugVPedItsClie.AtivaSelecionados(Status: Integer);
begin
  Screen.Cursor := crHourGlass;
  DmSugVda.TbSVPClieIts.First;
  while not DmSugVda.TbSVPClieIts.Eof do
  begin
    if DmSugVda.TbSVPClieItsAtivo.Value <> Status then
    begin
      DmSugVda.TbSVPClieIts.Edit;
      DmSugVda.TbSVPClieItsAtivo.Value := Status;
      DmSugVda.TbSVPClieIts.Post;
    end;
    DmSugVda.TbSVPClieIts.Next;
  end;
  HabilitaItemUnico();
  Screen.Cursor := crDefault;
end;

procedure TFmSugVPedItsClie.BtPesqClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMPesq, BtPesq);
end;

procedure TFmSugVPedItsClie.BtNenhumClick(Sender: TObject);
begin
  AtivaItens(0);
end;

procedure TFmSugVPedItsClie.BtTodosClick(Sender: TObject);
begin
  AtivaItens(1);
end;

class function TFmSugVPedItsClie.CreateDockForm(const aColor: TColor): TCustomForm;
begin
  result := TFmSugVPedItsClie.Create(Application);
  //result.Color := aColor;
  //result.Caption := ColorToString(aColor);
  FmMyGlyfs.ConfiguraFormDock(result);
  result.Show;
end;

procedure TFmSugVPedItsClie.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  DmodG.QrUpdPID1.Close;
  DmodG.QrUpdPID1.SQL.Clear;
  DmodG.QrUpdPID1.SQL.Add('DROP TABLE ' + FSVPClie + '; ');
  UMyMod.ExecutaQuery(DmodG.QrUpdPID1);
  //
  ManualFloat(Rect(0, 0, 0, 0));
  Action := caFree;
end;

procedure TFmSugVPedItsClie.FormShow(Sender: TObject);
begin
  FmSugVPedIts.FechaPesquisa();
end;

procedure TFmSugVPedItsClie.FormStartDock(Sender: TObject;
  var DragObject: TDragDockObject);
begin
  DragObject := TDragDockObjectEx.Create(Self);
  DragObject.Brush.Color := clAqua; // this will display a red outline
end;


procedure TFmSugVPedItsClie.FormUnDock(Sender: TObject; Client: TControl;
  NewTarget: TWinControl; var Allow: Boolean);
begin
end;

///// FIM DAS PROCEDURES E FUNCTIONS DO DOCKFORM

procedure TFmSugVPedItsClie.PageControl1Change(Sender: TObject);
begin
  BtPesq.Enabled := PageControl1.ActivePageIndex = 0;
  case PageControl1.ActivePageIndex of
    0: Pesquisar();
    1: RGSelecaoClick(Self);
  end;
end;

procedure TFmSugVPedItsClie.Pesquisar();
begin
  Screen.Cursor := crHourGlass;
  //
  DmSugVda.TbSVPClieIts.Filter   := 'Nome LIKE ''%' + EdPesq.Text + '%''';
  DmSugVda.TbSVPClieIts.Filtered := True;
  //
  Screen.Cursor := crDefault;
end;

procedure TFmSugVPedItsClie.RGSelecaoClick(Sender: TObject);
begin
  DmSugVda.TbSVPClieIts.Filtered := False;
  case RGSelecao.ItemIndex of
    0: DmSugVda.TbSVPClieIts.Filter := 'Ativo=0';
    1: DmSugVda.TbSVPClieIts.Filter := 'Ativo=1';
    2: DmSugVda.TbSVPClieIts.Filter := '';
  end;
  if RGSelecao.ItemIndex < 2 then
    DmSugVda.TbSVPClieIts.Filtered := True;
end;

procedure TFmSugVPedItsClie.DBGrid1CellClick(Column: TColumn);
var
  Status: Integer;
begin
  if Column.FieldName = 'Ativo' then
  begin
    if DmSugVda.TbSVPClieItsAtivo.Value = 1 then
      Status := 0
    else
      Status := 1;
    //
    DmSugVda.TbSVPClieIts.Edit;
    DmSugVda.TbSVPClieItsAtivo.Value := Status;
    DmSugVda.TbSVPClieIts.Post;
  end;
  HabilitaItemUnico();
end;

procedure TFmSugVPedItsClie.HabilitaItemUnico();
var
  Habilita: Boolean;
begin
  DmSugVda.QrSVPClieAti.Close;
  DmSugVda.QrSVPClieAti.Open;
  Habilita :=  DmSugVda.QrSVPClieAtiItens.Value = 0;
  FmSugVPedIts.LaSVPClie.Enabled := Habilita;
  FmSugVPedIts.EdSVPClie.Enabled := Habilita;
  FmSugVPedIts.CBSVPClie.Enabled := Habilita;
  if not Habilita then
  begin
    FmSugVPedIts.EdSVPClie.ValueVariant := 0;
    FmSugVPedIts.CBSVPClie.KeyValue     := 0;
  end;
end;

procedure TFmSugVPedItsClie.LimpaPesquisa;
var
  K, I: Integer;
begin
  for K := 0 to Screen.FormCount - 1 do
  begin
    if Screen.Forms[K] is TFmSugVPedItsClie then
    begin
      for I := 0 to Screen.Forms[K].ComponentCount -1 do
      if Screen.Forms[K].Components[I] is TEdit then
        TEdit(Screen.Forms[K].Components[I]).Text := '';
    end;
  end;
  AtivaItens(0);
end;

procedure TFmSugVPedItsClie.Desativa1Click(Sender: TObject);
begin
  AtivaSelecionados(0);
end;

procedure TFmSugVPedItsClie.EdPesqChange(Sender: TObject);
begin
  Pesquisar();
end;

procedure TFmSugVPedItsClie.FormCreate(Sender: TObject);
begin
  DmSugVda.TbSVPClieIts.Close;
  FSVPClie := UCriar.RecriaTempTable('SVPClie', DmodG.QrUpdPID1, False);
  DmodG.QrUpdPID1.SQL.Clear;
  DmodG.QrUpdPID1.SQL.Add('INSERT INTO ' + FSVPClie);
  DmodG.QrUpdPID1.SQL.Add('SELECT Codigo, CodUsu,');
  DmodG.QrUpdPID1.SQL.Add('IF(Tipo=0,RazaoSocial,Nome) Nome, 0');
  DmodG.QrUpdPID1.SQL.Add('FROM ' + TMeuDB + '.entidades');
  DmodG.QrUpdPID1.SQL.Add('WHERE Cliente1="V"');
  UMyMod.ExecutaQuery(DmodG.QrUpdPID1);
  DmSugVda.TbSVPClieIts.Open;
  PageControl1.ActivePageIndex := 0;
end;

procedure TFmSugVPedItsClie.FormDeactivate(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 1;
  RGSelecao.ItemIndex := 1;
  //
  if FindWindow('TFmSugVPedIts', nil) > 0 then
    FmSugVPedIts.AtivaBtConfirma();
end;

end.

