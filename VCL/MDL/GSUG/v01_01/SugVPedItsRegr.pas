unit SugVPedItsRegr;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DB, DBGrids,
  dmkDBGrid, DBCtrls, mySQLDbTables, Menus, ComCtrls, UnDmkProcFunc;

type
  THackDBGrid = class(TDBGrid);
  TFmSugVPedItsRegr = class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    BtNenhum: TBitBtn;
    BtTodos: TBitBtn;
    BtPesq: TBitBtn;
    PMPesq: TPopupMenu;
    Ativa1: TMenuItem;
    Desativa1: TMenuItem;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    Panel3: TPanel;
    Label1: TLabel;
    EdPesq: TEdit;
    DBGrid1: TdmkDBGrid;
    Panel4: TPanel;
    DBGrid2: TdmkDBGrid;
    RGSelecao: TRadioGroup;
    procedure FormCreate(Sender: TObject);
    procedure DBGrid1CellClick(Column: TColumn);
    procedure FormStartDock(Sender: TObject; var DragObject: TDragDockObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure EdPesqChange(Sender: TObject);
    procedure FormUnDock(Sender: TObject; Client: TControl;
      NewTarget: TWinControl; var Allow: Boolean);
    procedure BtNenhumClick(Sender: TObject);
    procedure BtTodosClick(Sender: TObject);
    procedure BtPesqClick(Sender: TObject);
    procedure Ativa1Click(Sender: TObject);
    procedure Desativa1Click(Sender: TObject);
    procedure PageControl1Change(Sender: TObject);
    procedure FormDeactivate(Sender: TObject);
    procedure RGSelecaoClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    FSVPRegr: String;
    procedure Pesquisar();
    procedure AtivaItens(Status: Integer);
    procedure AtivaSelecionados(Status: Integer);
    procedure HabilitaItemUnico();
  public
    { Public declarations }
    class function CreateDockForm(const aColor: TColor): TCustomForm;
    //class procedure FiltaAtivos();
    procedure LimpaPesquisa();
  end;

var
  FmSugVPedItsRegr: TFmSugVPedItsRegr;

implementation

uses UnMyObjects, Module, UMySQLModule, SugVPedIts, ModSugVda, ModuleGeral, UCreate,
UnInternalConsts, MyGlyfs;

{$R *.DFM}

///// PROCEDURES E FUNCTIONS DO DOCKFORM

procedure TFmSugVPedItsRegr.Ativa1Click(Sender: TObject);
begin
  AtivaSelecionados(1);
end;

procedure TFmSugVPedItsRegr.AtivaItens(Status: Integer);
var
  Codigo: Integer;
begin
  Screen.Cursor := crHourGlass;
  Codigo := DmSugVda.TbSVPRegrItsCodigo.Value;
  DModG.QrUpdPID1.SQL.Clear;
  DModG.QrUpdPID1.SQL.Add('UPDATE ' + FSVPRegr + ' SET Ativo=' + dmkPF.FFP(Status, 0) + ';');
  UMyMod.ExecutaQuery(DmodG.QrUpdPID1);
  DmSugVda.TbSVPRegrIts.Close;
  DmSugVda.TbSVPRegrIts.Open;
  DmSugVda.TbSVPRegrIts.Locate('Codigo', Codigo, []);
  HabilitaItemUnico();
  Screen.Cursor := crDefault;
end;

procedure TFmSugVPedItsRegr.AtivaSelecionados(Status: Integer);
begin
  Screen.Cursor := crHourGlass;
  DmSugVda.TbSVPRegrIts.First;
  while not DmSugVda.TbSVPRegrIts.Eof do
  begin
    if DmSugVda.TbSVPRegrItsAtivo.Value <> Status then
    begin
      DmSugVda.TbSVPRegrIts.Edit;
      DmSugVda.TbSVPRegrItsAtivo.Value := Status;
      DmSugVda.TbSVPRegrIts.Post;
    end;
    DmSugVda.TbSVPRegrIts.Next;
  end;
  HabilitaItemUnico();
  Screen.Cursor := crDefault;
end;

procedure TFmSugVPedItsRegr.BtPesqClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMPesq, BtPesq);
end;

procedure TFmSugVPedItsRegr.BtNenhumClick(Sender: TObject);
begin
  AtivaItens(0);
end;

procedure TFmSugVPedItsRegr.BtTodosClick(Sender: TObject);
begin
  AtivaItens(1);
end;

class function TFmSugVPedItsRegr.CreateDockForm(const aColor: TColor): TCustomForm;
begin
  result := TFmSugVPedItsRegr.Create(Application);
  //result.Color := aColor;
  //result.Caption := ColorToString(aColor);
  FmMyGlyfs.ConfiguraFormDock(result);
  result.Show;
end;

procedure TFmSugVPedItsRegr.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  DmodG.QrUpdPID1.Close;
  DmodG.QrUpdPID1.SQL.Clear;
  DmodG.QrUpdPID1.SQL.Add('DROP TABLE ' + FSVPRegr + '; ');
  UMyMod.ExecutaQuery(DmodG.QrUpdPID1);
  //
  ManualFloat(Rect(0, 0, 0, 0));
  Action := caFree;
end;

procedure TFmSugVPedItsRegr.FormShow(Sender: TObject);
begin
  FmSugVPedIts.FechaPesquisa();
end;

procedure TFmSugVPedItsRegr.FormStartDock(Sender: TObject;
  var DragObject: TDragDockObject);
begin
  DragObject := TDragDockObjectEx.Create(Self);
  DragObject.Brush.Color := clAqua; // this will display a red outline
end;


procedure TFmSugVPedItsRegr.FormUnDock(Sender: TObject; Client: TControl;
  NewTarget: TWinControl; var Allow: Boolean);
begin
end;

///// FIM DAS PROCEDURES E FUNCTIONS DO DOCKFORM

procedure TFmSugVPedItsRegr.PageControl1Change(Sender: TObject);
begin
  BtPesq.Enabled := PageControl1.ActivePageIndex = 0;
  case PageControl1.ActivePageIndex of
    0: Pesquisar();
    1: RGSelecaoClick(Self);
  end;
end;

procedure TFmSugVPedItsRegr.Pesquisar();
begin
  Screen.Cursor := crHourGlass;
  //
  DmSugVda.TbSVPRegrIts.Filter   := 'Nome LIKE ''%' + EdPesq.Text + '%''';
  DmSugVda.TbSVPRegrIts.Filtered := True;
  //
  Screen.Cursor := crDefault;
end;

procedure TFmSugVPedItsRegr.RGSelecaoClick(Sender: TObject);
begin
  DmSugVda.TbSVPRegrIts.Filtered := False;
  case RGSelecao.ItemIndex of
    0: DmSugVda.TbSVPRegrIts.Filter := 'Ativo=0';
    1: DmSugVda.TbSVPRegrIts.Filter := 'Ativo=1';
    2: DmSugVda.TbSVPRegrIts.Filter := '';
  end;
  if RGSelecao.ItemIndex < 2 then
    DmSugVda.TbSVPRegrIts.Filtered := True;
end;

procedure TFmSugVPedItsRegr.DBGrid1CellClick(Column: TColumn);
var
  Status: Integer;
begin
  if Column.FieldName = 'Ativo' then
  begin
    if DmSugVda.TbSVPRegrItsAtivo.Value = 1 then
      Status := 0
    else
      Status := 1;
    //
    DmSugVda.TbSVPRegrIts.Edit;
    DmSugVda.TbSVPRegrItsAtivo.Value := Status;
    DmSugVda.TbSVPRegrIts.Post;
  end;
  HabilitaItemUnico();
end;

procedure TFmSugVPedItsRegr.HabilitaItemUnico();
var
  Habilita: Boolean;
begin
  DmSugVda.QrSVPRegrAti.Close;
  DmSugVda.QrSVPRegrAti.Open;
  Habilita :=  DmSugVda.QrSVPRegrAtiItens.Value = 0;
  FmSugVPedIts.LaSVPRegr.Enabled := Habilita;
  FmSugVPedIts.EdSVPRegr.Enabled := Habilita;
  FmSugVPedIts.CBSVPRegr.Enabled := Habilita;
  if not Habilita then
  begin
    FmSugVPedIts.EdSVPRegr.ValueVariant := 0;
    FmSugVPedIts.CBSVPRegr.KeyValue     := 0;
  end;
end;

procedure TFmSugVPedItsRegr.LimpaPesquisa;
var
  K, I: Integer;
begin
  for K := 0 to Screen.FormCount - 1 do
  begin
    if Screen.Forms[K] is TFmSugVPedItsRegr then
    begin
      for I := 0 to Screen.Forms[K].ComponentCount -1 do
      if Screen.Forms[K].Components[I] is TEdit then
        TEdit(Screen.Forms[K].Components[I]).Text := '';
    end;
  end;
  AtivaItens(0);
end;

procedure TFmSugVPedItsRegr.Desativa1Click(Sender: TObject);
begin
  AtivaSelecionados(0);
end;

procedure TFmSugVPedItsRegr.EdPesqChange(Sender: TObject);
begin
  Pesquisar();
end;

procedure TFmSugVPedItsRegr.FormCreate(Sender: TObject);
begin
  DmSugVda.TbSVPRegrIts.Close;
  FSVPRegr := UCriar.RecriaTempTable('SVPRegr', DmodG.QrUpdPID1, False);
  DmodG.QrUpdPID1.SQL.Clear;
  DmodG.QrUpdPID1.SQL.Add('INSERT INTO ' + FSVPRegr);
  DmodG.QrUpdPID1.SQL.Add('SELECT Codigo, CodUsu, Nome, 0');
  DmodG.QrUpdPID1.SQL.Add('FROM ' + TMeuDB + '.fisregcad');
  DmodG.QrUpdPID1.SQL.Add('WHERE Codigo<>0');
  UMyMod.ExecutaQuery(DmodG.QrUpdPID1);
  DmSugVda.TbSVPRegrIts.Open;
  PageControl1.ActivePageIndex := 0;
end;

procedure TFmSugVPedItsRegr.FormDeactivate(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 1;
  RGSelecao.ItemIndex := 1;
  //
  if FindWindow('TFmSugVPedIts', nil) > 0 then
    FmSugVPedIts.AtivaBtConfirma();
end;

end.

