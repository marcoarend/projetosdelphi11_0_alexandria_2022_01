unit ModSugVda;

interface

uses
  SysUtils, Classes, DB, mySQLDbTables;

type
  TDmSugVda = class(TDataModule)
    QrSVPClieCad: TmySQLQuery;
    DsSVPClieCad: TDataSource;
    DsSVPClieIts: TDataSource;
    QrSVPClieAti: TmySQLQuery;
    QrGraAtrCad: TmySQLQuery;
    DsGraAtrCad: TDataSource;
    QrSVPClieCadCodigo: TIntegerField;
    QrSVPClieCadCodUsu: TIntegerField;
    QrSVPClieCadNOMEENT: TWideStringField;
    TbSVPClieIts: TmySQLTable;
    TbSVPClieItsCodigo: TIntegerField;
    TbSVPClieItsCodUsu: TIntegerField;
    TbSVPClieItsNome: TWideStringField;
    QrSVPClieAtiItens: TLargeintField;
    TbSVPClieItsAtivo: TSmallintField;
    QrSVPReprCad: TmySQLQuery;
    IntegerField1: TIntegerField;
    IntegerField2: TIntegerField;
    StringField1: TWideStringField;
    DsSVPReprCad: TDataSource;
    TbSVPReprIts: TmySQLTable;
    DsSVPReprIts: TDataSource;
    QrSVPReprAti: TmySQLQuery;
    QrSVPRegiCad: TmySQLQuery;
    DsSVPRegiCad: TDataSource;
    TbSVPRegiIts: TmySQLTable;
    DsSVPRegiIts: TDataSource;
    QrSVPRegiAti: TmySQLQuery;
    QrSVPRegrCad: TmySQLQuery;
    DsSVPRegrCad: TDataSource;
    TbSVPRegrIts: TmySQLTable;
    DsSVPRegrIts: TDataSource;
    QrSVPRegrAti: TmySQLQuery;
    QrSVPProdCad: TmySQLQuery;
    DsSVPProdCad: TDataSource;
    TbSVPProdIts: TmySQLTable;
    DsSVPProdIts: TDataSource;
    QrSVPProdAti: TmySQLQuery;
    TbSVPSituIts: TmySQLTable;
    DsSVPSituIts: TDataSource;
    QrSVPSituAti: TmySQLQuery;
    QrSVPRegiCadCodigo: TIntegerField;
    QrSVPRegiCadCodUsu: TIntegerField;
    QrSVPRegiCadNome: TWideStringField;
    QrSVPRegrCadCodigo: TIntegerField;
    QrSVPRegrCadCodUsu: TIntegerField;
    QrSVPRegrCadNome: TWideStringField;
    QrSVPProdCadNivel1: TIntegerField;
    QrSVPProdCadCodUsu: TIntegerField;
    QrSVPProdCadNome: TWideStringField;
    QrGraAtrIts: TmySQLQuery;
    DsGraAtrIts: TDataSource;
    QrGraAtrItsControle: TIntegerField;
    QrGraAtrItsCodUsu: TIntegerField;
    QrGraAtrItsNome: TWideStringField;
    QrGraAtrCadCodigo: TIntegerField;
    QrGraAtrCadCodUsu: TIntegerField;
    QrGraAtrCadNome: TWideStringField;
    TbSVPReprItsCodigo: TIntegerField;
    TbSVPReprItsCodUsu: TIntegerField;
    TbSVPReprItsNome: TWideStringField;
    TbSVPReprItsAtivo: TSmallintField;
    QrSVPReprAtiItens: TLargeintField;
    TbSVPRegiItsCodigo: TIntegerField;
    TbSVPRegiItsCodUsu: TIntegerField;
    TbSVPRegiItsNome: TWideStringField;
    TbSVPRegiItsAtivo: TSmallintField;
    TbSVPRegrItsCodigo: TIntegerField;
    TbSVPRegrItsCodUsu: TIntegerField;
    TbSVPRegrItsNome: TWideStringField;
    TbSVPRegrItsAtivo: TSmallintField;
    TbSVPProdItsCodigo: TIntegerField;
    TbSVPProdItsCodUsu: TIntegerField;
    TbSVPProdItsNome: TWideStringField;
    TbSVPProdItsAtivo: TSmallintField;
    TbSVPSituItsCodigo: TIntegerField;
    TbSVPSituItsCodUsu: TIntegerField;
    TbSVPSituItsNome: TWideStringField;
    TbSVPSituItsAtivo: TSmallintField;
    QrSVPRegiAtiItens: TLargeintField;
    QrSVPRegrAtiItens: TLargeintField;
    QrSVPProdAtiItens: TLargeintField;
    QrSVPSituAtiItens: TLargeintField;
    TbSVPAtrProdIts: TmySQLTable;
    DsSVPAtrProdIts: TDataSource;
    QrSVPAtrProdAti: TmySQLQuery;
    TbSVPAtrProdItsCodigo: TIntegerField;
    TbSVPAtrProdItsCodUsu: TIntegerField;
    TbSVPAtrProdItsNome: TWideStringField;
    TbSVPAtrProdItsAtivo: TSmallintField;
    QrSVPAtrProdAtiItens: TLargeintField;
    TbSVPAtrProdItsControle: TIntegerField;
  private
    { Private declarations }
  public
    { Public declarations }
    procedure ReopenAll();
    procedure ReopenGraAtrIts(Controle: Integer);
  end;

var
  DmSugVda: TDmSugVda;

implementation

uses UnMyObjects, ModuleGeral, UnInternalConsts, ModPediVda;

{$R *.dfm}

{ TDmSugVda }

procedure TDmSugVda.ReopenAll;
begin
  DModG.ReopenEmpresas(VAR_USUARIO, 0);
  DModG.ReopenStqCenCad(0);
  //
  QrSVPClieCad.Close;
  QrSVPClieCad.Open;
  //
  QrSVPReprCad.Close;
  QrSVPReprCad.Open;
  //
  QrSVPRegiCad.Close;
  QrSVPRegiCad.Open;
  //
  QrSVPRegrCad.Close;
  QrSVPRegrCad.Open;
  //
  QrSVPProdCad.Close;
  QrSVPProdCad.Open;
  //
  QrGraAtrCad.Close;
  QrGraAtrCad.Open;
  //
end;

procedure TDmSugVda.ReopenGraAtrIts(Controle: Integer);
begin
  QrGraAtrIts.Close;
  QrGraAtrIts.Params[0].AsInteger := QrGraAtrCadCodigo.Value;
  QrGraAtrIts.Open;
  //
  if Controle > 0 then
    QrGraAtrIts.Locate('Controle', Controle, []);
end;

end.
