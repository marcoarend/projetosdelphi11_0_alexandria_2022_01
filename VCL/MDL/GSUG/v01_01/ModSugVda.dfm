object DmSugVda: TDmSugVda
  OldCreateOrder = False
  Height = 518
  Width = 749
  object QrSVPClieCad: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, CodUsu,'
      'IF(Tipo=0,RazaoSocial,Nome) NOMEENT'
      'FROM entidades'
      'WHERE Cliente1="V"'
      'ORDER BY NOMEENT')
    Left = 28
    object QrSVPClieCadCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrSVPClieCadCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrSVPClieCadNOMEENT: TWideStringField
      FieldName = 'NOMEENT'
      Required = True
      Size = 100
    end
  end
  object DsSVPClieCad: TDataSource
    DataSet = QrSVPClieCad
    Left = 100
  end
  object DsSVPClieIts: TDataSource
    DataSet = TbSVPClieIts
    Left = 248
  end
  object QrSVPClieAti: TMySQLQuery
    Database = DModG.MyPID_DB
    RequestLive = True
    SQL.Strings = (
      'SELECT Count(*)  Itens'
      'FROM svpclie'
      'WHERE Ativo=1'
      '')
    Left = 316
    object QrSVPClieAtiItens: TLargeintField
      FieldName = 'Itens'
      Required = True
    end
  end
  object QrGraAtrCad: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, CodUsu, Nome'
      'FROM graatrcad'
      'ORDER BY Nome')
    Left = 28
    Top = 296
    object QrGraAtrCadCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrGraAtrCadCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrGraAtrCadNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
  end
  object DsGraAtrCad: TDataSource
    DataSet = QrGraAtrCad
    Left = 100
    Top = 296
  end
  object TbSVPClieIts: TMySQLTable
    Database = DModG.MyPID_DB
    TableName = 'svpclie'
    Left = 172
    object TbSVPClieItsCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'svpclie.Codigo'
    end
    object TbSVPClieItsCodUsu: TIntegerField
      FieldName = 'CodUsu'
      Origin = 'svpclie.CodUsu'
    end
    object TbSVPClieItsNome: TWideStringField
      FieldName = 'Nome'
      Origin = 'svpclie.Nome'
      Size = 100
    end
    object TbSVPClieItsAtivo: TSmallintField
      FieldName = 'Ativo'
      Origin = 'svpclie.Ativo'
      MaxValue = 1
    end
  end
  object QrSVPReprCad: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, CodUsu,'
      'IF(Tipo=0,RazaoSocial,Nome) NOMEENT'
      'FROM entidades'
      'WHERE Fornece3="V"'
      'OR        Fornece4="V"'
      'OR        Fornece5="V"'
      'OR        Fornece6="V"'
      'OR        Fornece7="V"'
      'ORDER BY NOMEENT')
    Left = 28
    Top = 48
    object IntegerField1: TIntegerField
      FieldName = 'Codigo'
    end
    object IntegerField2: TIntegerField
      FieldName = 'CodUsu'
    end
    object StringField1: TWideStringField
      FieldName = 'NOMEENT'
      Required = True
      Size = 100
    end
  end
  object DsSVPReprCad: TDataSource
    DataSet = QrSVPReprCad
    Left = 100
    Top = 48
  end
  object TbSVPReprIts: TMySQLTable
    Database = DModG.MyPID_DB
    TableName = 'svprepr'
    Left = 172
    Top = 48
    object TbSVPReprItsCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'svpclie.Codigo'
    end
    object TbSVPReprItsCodUsu: TIntegerField
      FieldName = 'CodUsu'
      Origin = 'svpclie.CodUsu'
    end
    object TbSVPReprItsNome: TWideStringField
      FieldName = 'Nome'
      Origin = 'svpclie.Nome'
      Size = 100
    end
    object TbSVPReprItsAtivo: TSmallintField
      FieldName = 'Ativo'
      Origin = 'svpclie.Ativo'
      MaxValue = 1
    end
  end
  object DsSVPReprIts: TDataSource
    DataSet = TbSVPReprIts
    Left = 248
    Top = 48
  end
  object QrSVPReprAti: TMySQLQuery
    Database = DModG.MyPID_DB
    RequestLive = True
    SQL.Strings = (
      'SELECT Count(*)  Itens'
      'FROM svprepr'
      'WHERE Ativo=1'
      '')
    Left = 316
    Top = 48
    object QrSVPReprAtiItens: TLargeintField
      FieldName = 'Itens'
      Required = True
    end
  end
  object QrSVPRegiCad: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, CodUsu, Nome'
      'FROM regioes'
      'ORDER BY Nome'
      '')
    Left = 28
    Top = 96
    object QrSVPRegiCadCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrSVPRegiCadCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrSVPRegiCadNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
  end
  object DsSVPRegiCad: TDataSource
    DataSet = QrSVPRegiCad
    Left = 100
    Top = 96
  end
  object TbSVPRegiIts: TMySQLTable
    Database = DModG.MyPID_DB
    TableName = 'svpregi'
    Left = 172
    Top = 96
    object TbSVPRegiItsCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'svpclie.Codigo'
    end
    object TbSVPRegiItsCodUsu: TIntegerField
      FieldName = 'CodUsu'
      Origin = 'svpclie.CodUsu'
    end
    object TbSVPRegiItsNome: TWideStringField
      FieldName = 'Nome'
      Origin = 'svpclie.Nome'
      Size = 100
    end
    object TbSVPRegiItsAtivo: TSmallintField
      FieldName = 'Ativo'
      Origin = 'svpclie.Ativo'
      MaxValue = 1
    end
  end
  object DsSVPRegiIts: TDataSource
    DataSet = TbSVPRegiIts
    Left = 248
    Top = 96
  end
  object QrSVPRegiAti: TMySQLQuery
    Database = DModG.MyPID_DB
    RequestLive = True
    SQL.Strings = (
      'SELECT Count(*)  Itens'
      'FROM svpregi'
      'WHERE Ativo=1'
      '')
    Left = 316
    Top = 96
    object QrSVPRegiAtiItens: TLargeintField
      FieldName = 'Itens'
      Required = True
    end
  end
  object QrSVPRegrCad: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, CodUsu, Nome'
      'FROM fisregcad'
      'ORDER BY Nome')
    Left = 28
    Top = 148
    object QrSVPRegrCadCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrSVPRegrCadCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrSVPRegrCadNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsSVPRegrCad: TDataSource
    DataSet = QrSVPRegrCad
    Left = 100
    Top = 148
  end
  object TbSVPRegrIts: TMySQLTable
    Database = DModG.MyPID_DB
    TableName = 'svpregr'
    Left = 172
    Top = 148
    object TbSVPRegrItsCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'svpclie.Codigo'
    end
    object TbSVPRegrItsCodUsu: TIntegerField
      FieldName = 'CodUsu'
      Origin = 'svpclie.CodUsu'
    end
    object TbSVPRegrItsNome: TWideStringField
      FieldName = 'Nome'
      Origin = 'svpclie.Nome'
      Size = 100
    end
    object TbSVPRegrItsAtivo: TSmallintField
      FieldName = 'Ativo'
      Origin = 'svpclie.Ativo'
      MaxValue = 1
    end
  end
  object DsSVPRegrIts: TDataSource
    DataSet = TbSVPRegrIts
    Left = 248
    Top = 148
  end
  object QrSVPRegrAti: TMySQLQuery
    Database = DModG.MyPID_DB
    RequestLive = True
    SQL.Strings = (
      'SELECT Count(*)  Itens'
      'FROM svpregr'
      'WHERE Ativo=1'
      '')
    Left = 316
    Top = 148
    object QrSVPRegrAtiItens: TLargeintField
      FieldName = 'Itens'
      Required = True
    end
  end
  object QrSVPProdCad: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Nivel1, CodUsu, Nome'
      'FROM gragru1'
      'ORDER BY Nome')
    Left = 28
    Top = 196
    object QrSVPProdCadNivel1: TIntegerField
      FieldName = 'Nivel1'
    end
    object QrSVPProdCadCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrSVPProdCadNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
  end
  object DsSVPProdCad: TDataSource
    DataSet = QrSVPProdCad
    Left = 100
    Top = 196
  end
  object TbSVPProdIts: TMySQLTable
    Database = DModG.MyPID_DB
    TableName = 'svpprod'
    Left = 172
    Top = 196
    object TbSVPProdItsCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'svpclie.Codigo'
    end
    object TbSVPProdItsCodUsu: TIntegerField
      FieldName = 'CodUsu'
      Origin = 'svpclie.CodUsu'
    end
    object TbSVPProdItsNome: TWideStringField
      FieldName = 'Nome'
      Origin = 'svpclie.Nome'
      Size = 100
    end
    object TbSVPProdItsAtivo: TSmallintField
      FieldName = 'Ativo'
      Origin = 'svpclie.Ativo'
      MaxValue = 1
    end
  end
  object DsSVPProdIts: TDataSource
    DataSet = TbSVPProdIts
    Left = 248
    Top = 196
  end
  object QrSVPProdAti: TMySQLQuery
    Database = DModG.MyPID_DB
    RequestLive = True
    SQL.Strings = (
      'SELECT Count(*)  Itens'
      'FROM svpprod'
      'WHERE Ativo=1'
      '')
    Left = 316
    Top = 196
    object QrSVPProdAtiItens: TLargeintField
      FieldName = 'Itens'
      Required = True
    end
  end
  object TbSVPSituIts: TMySQLTable
    Database = DModG.MyPID_DB
    TableName = 'svpsitu'
    Left = 172
    Top = 244
    object TbSVPSituItsCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'svpclie.Codigo'
    end
    object TbSVPSituItsCodUsu: TIntegerField
      FieldName = 'CodUsu'
      Origin = 'svpclie.CodUsu'
    end
    object TbSVPSituItsNome: TWideStringField
      FieldName = 'Nome'
      Origin = 'svpclie.Nome'
      Size = 100
    end
    object TbSVPSituItsAtivo: TSmallintField
      FieldName = 'Ativo'
      Origin = 'svpclie.Ativo'
      MaxValue = 1
    end
  end
  object DsSVPSituIts: TDataSource
    DataSet = TbSVPSituIts
    Left = 248
    Top = 244
  end
  object QrSVPSituAti: TMySQLQuery
    Database = DModG.MyPID_DB
    RequestLive = True
    SQL.Strings = (
      'SELECT Count(*)  Itens'
      'FROM svpsitu'
      'WHERE Ativo=1'
      '')
    Left = 316
    Top = 244
    object QrSVPSituAtiItens: TLargeintField
      FieldName = 'Itens'
      Required = True
    end
  end
  object QrGraAtrIts: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle, CodUsu, Nome'
      'FROM graatrits'
      'WHERE Codigo=:P0'
      'ORDER BY Nome')
    Left = 28
    Top = 248
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrGraAtrItsControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrGraAtrItsCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrGraAtrItsNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsGraAtrIts: TDataSource
    DataSet = QrGraAtrIts
    Left = 100
    Top = 248
  end
  object TbSVPAtrProdIts: TMySQLTable
    Database = DModG.MyPID_DB
    TableName = 'svpatrprod'
    Left = 172
    Top = 296
    object TbSVPAtrProdItsCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'svpclie.Codigo'
    end
    object TbSVPAtrProdItsControle: TIntegerField
      FieldName = 'Controle'
    end
    object TbSVPAtrProdItsCodUsu: TIntegerField
      FieldName = 'CodUsu'
      Origin = 'svpclie.CodUsu'
    end
    object TbSVPAtrProdItsNome: TWideStringField
      FieldName = 'Nome'
      Origin = 'svpclie.Nome'
      Size = 100
    end
    object TbSVPAtrProdItsAtivo: TSmallintField
      FieldName = 'Ativo'
      Origin = 'svpclie.Ativo'
      MaxValue = 1
    end
  end
  object DsSVPAtrProdIts: TDataSource
    DataSet = TbSVPAtrProdIts
    Left = 248
    Top = 296
  end
  object QrSVPAtrProdAti: TMySQLQuery
    Database = DModG.MyPID_DB
    RequestLive = True
    SQL.Strings = (
      'SELECT Count(*)  Itens'
      'FROM svpatrprod'
      'WHERE Ativo=1'
      '')
    Left = 316
    Top = 296
    object QrSVPAtrProdAtiItens: TLargeintField
      FieldName = 'Itens'
      Required = True
    end
  end
end
