unit SugVPedItsSitu;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DB,
  DBGrids, dmkDBGrid, DBCtrls, mySQLDbTables, Menus, ComCtrls, UnDmkProcFunc;

type
  THackDBGrid = class(TDBGrid);
  TFmSugVPedItsSitu = class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    BtNenhum: TBitBtn;
    BtTodos: TBitBtn;
    BtPesq: TBitBtn;
    PMPesq: TPopupMenu;
    Ativa1: TMenuItem;
    Desativa1: TMenuItem;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    Panel3: TPanel;
    Label1: TLabel;
    EdPesq: TEdit;
    DBGrid1: TdmkDBGrid;
    Panel4: TPanel;
    DBGrid2: TdmkDBGrid;
    RGSelecao: TRadioGroup;
    procedure FormCreate(Sender: TObject);
    procedure DBGrid1CellClick(Column: TColumn);
    procedure FormStartDock(Sender: TObject; var DragObject: TDragDockObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure EdPesqChange(Sender: TObject);
    procedure FormUnDock(Sender: TObject; Client: TControl;
      NewTarget: TWinControl; var Allow: Boolean);
    procedure BtNenhumClick(Sender: TObject);
    procedure BtTodosClick(Sender: TObject);
    procedure BtPesqClick(Sender: TObject);
    procedure Ativa1Click(Sender: TObject);
    procedure Desativa1Click(Sender: TObject);
    procedure PageControl1Change(Sender: TObject);
    procedure FormDeactivate(Sender: TObject);
    procedure RGSelecaoClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    FSVPSitu: String;
    procedure Pesquisar();
    procedure AtivaItens(Status: Integer);
    procedure AtivaSelecionados(Status: Integer);
    procedure HabilitaItemUnico();
  public
    { Public declarations }
    class function CreateDockForm(const aColor: TColor): TCustomForm;
    //class procedure FiltaAtivos();
    procedure LimpaPesquisa();
  end;

var
  FmSugVPedItsSitu: TFmSugVPedItsSitu;

implementation

uses UnMyObjects, Module, UMySQLModule, SugVPedIts, ModSugVda, ModuleGeral, UCreate,
UnInternalConsts, MyGlyfs;

{$R *.DFM}

///// PROCEDURES E FUNCTIONS DO DOCKFORM

procedure TFmSugVPedItsSitu.Ativa1Click(Sender: TObject);
begin
  AtivaSelecionados(1);
end;

procedure TFmSugVPedItsSitu.AtivaItens(Status: Integer);
var
  Codigo: Integer;
begin
  Screen.Cursor := crHourGlass;
  Codigo := DmSugVda.TbSVPSituItsCodigo.Value;
  DModG.QrUpdPID1.SQL.Clear;
  DModG.QrUpdPID1.SQL.Add('UPDATE ' + FSVPSitu + ' SET Ativo=' + dmkPF.FFP(Status, 0) + ';');
  UMyMod.ExecutaQuery(DmodG.QrUpdPID1);
  DmSugVda.TbSVPSituIts.Close;
  DmSugVda.TbSVPSituIts.Open;
  DmSugVda.TbSVPSituIts.Locate('Codigo', Codigo, []);
  HabilitaItemUnico();
  Screen.Cursor := crDefault;
end;

procedure TFmSugVPedItsSitu.AtivaSelecionados(Status: Integer);
begin
  Screen.Cursor := crHourGlass;
  DmSugVda.TbSVPSituIts.First;
  while not DmSugVda.TbSVPSituIts.Eof do
  begin
    if DmSugVda.TbSVPSituItsAtivo.Value <> Status then
    begin
      DmSugVda.TbSVPSituIts.Edit;
      DmSugVda.TbSVPSituItsAtivo.Value := Status;
      DmSugVda.TbSVPSituIts.Post;
    end;
    DmSugVda.TbSVPSituIts.Next;
  end;
  HabilitaItemUnico();
  Screen.Cursor := crDefault;
end;

procedure TFmSugVPedItsSitu.BtPesqClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMPesq, BtPesq);
end;

procedure TFmSugVPedItsSitu.BtNenhumClick(Sender: TObject);
begin
  AtivaItens(0);
end;

procedure TFmSugVPedItsSitu.BtTodosClick(Sender: TObject);
begin
  AtivaItens(1);
end;

class function TFmSugVPedItsSitu.CreateDockForm(const aColor: TColor): TCustomForm;
begin
  result := TFmSugVPedItsSitu.Create(Application);
  //result.Color := aColor;
  //result.Caption := ColorToString(aColor);
  FmMyGlyfs.ConfiguraFormDock(result);
  result.Show;
end;

procedure TFmSugVPedItsSitu.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  DmodG.QrUpdPID1.Close;
  DmodG.QrUpdPID1.SQL.Clear;
  DmodG.QrUpdPID1.SQL.Add('DROP TABLE ' + FSVPSitu + '; ');
  UMyMod.ExecutaQuery(DmodG.QrUpdPID1);
  //
  ManualFloat(Rect(0, 0, 0, 0));
  Action := caFree;
end;

procedure TFmSugVPedItsSitu.FormShow(Sender: TObject);
begin
  FmSugVPedIts.FechaPesquisa();
end;

procedure TFmSugVPedItsSitu.FormStartDock(Sender: TObject;
  var DragObject: TDragDockObject);
begin
  DragObject := TDragDockObjectEx.Create(Self);
  DragObject.Brush.Color := clAqua; // this will display a red outline
end;


procedure TFmSugVPedItsSitu.FormUnDock(Sender: TObject; Client: TControl;
  NewTarget: TWinControl; var Allow: Boolean);
begin
end;

///// FIM DAS PROCEDURES E FUNCTIONS DO DOCKFORM

procedure TFmSugVPedItsSitu.PageControl1Change(Sender: TObject);
begin
  BtPesq.Enabled := PageControl1.ActivePageIndex = 0;
  case PageControl1.ActivePageIndex of
    0: Pesquisar();
    1: RGSelecaoClick(Self);
  end;
end;

procedure TFmSugVPedItsSitu.Pesquisar();
begin
  Screen.Cursor := crHourGlass;
  //
  DmSugVda.TbSVPSituIts.Filter   := 'Nome LIKE ''%' + EdPesq.Text + '%''';
  DmSugVda.TbSVPSituIts.Filtered := True;
  //
  Screen.Cursor := crDefault;
end;

procedure TFmSugVPedItsSitu.RGSelecaoClick(Sender: TObject);
begin
  DmSugVda.TbSVPSituIts.Filtered := False;
  case RGSelecao.ItemIndex of
    0: DmSugVda.TbSVPSituIts.Filter := 'Ativo=0';
    1: DmSugVda.TbSVPSituIts.Filter := 'Ativo=1';
    2: DmSugVda.TbSVPSituIts.Filter := '';
  end;
  if RGSelecao.ItemIndex < 2 then
    DmSugVda.TbSVPSituIts.Filtered := True;
end;

procedure TFmSugVPedItsSitu.DBGrid1CellClick(Column: TColumn);
var
  Status: Integer;
begin
  if Column.FieldName = 'Ativo' then
  begin
    if DmSugVda.TbSVPSituItsAtivo.Value = 1 then
      Status := 0
    else
      Status := 1;
    //
    DmSugVda.TbSVPSituIts.Edit;
    DmSugVda.TbSVPSituItsAtivo.Value := Status;
    DmSugVda.TbSVPSituIts.Post;
  end;
  HabilitaItemUnico();
end;

procedure TFmSugVPedItsSitu.HabilitaItemUnico();
var
  Habilita: Boolean;
begin
  DmSugVda.QrSVPSituAti.Close;
  DmSugVda.QrSVPSituAti.Open;
  Habilita :=  DmSugVda.QrSVPSituAtiItens.Value = 0;
  FmSugVPedIts.LaSVPSitu.Enabled := Habilita;
  FmSugVPedIts.EdSVPSitu.Enabled := Habilita;
  FmSugVPedIts.CBSVPSitu.Enabled := Habilita;
  if not Habilita then
  begin
    FmSugVPedIts.EdSVPSitu.ValueVariant := 0;
    FmSugVPedIts.CBSVPSitu.KeyValue     := 0;
  end;
end;

procedure TFmSugVPedItsSitu.LimpaPesquisa;
var
  K, I: Integer;
begin
  for K := 0 to Screen.FormCount - 1 do
  begin
    if Screen.Forms[K] is TFmSugVPedItsSitu then
    begin
      for I := 0 to Screen.Forms[K].ComponentCount -1 do
      if Screen.Forms[K].Components[I] is TEdit then
        TEdit(Screen.Forms[K].Components[I]).Text := '';
    end;
  end;
  AtivaItens(0);
end;

procedure TFmSugVPedItsSitu.Desativa1Click(Sender: TObject);
begin
  AtivaSelecionados(0);
end;

procedure TFmSugVPedItsSitu.EdPesqChange(Sender: TObject);
begin
  Pesquisar();
end;

procedure TFmSugVPedItsSitu.FormCreate(Sender: TObject);
begin
  DmSugVda.TbSVPSituIts.Close;
  FSVPSitu := UCriar.RecriaTempTable('SVPSitu', DmodG.QrUpdPID1, False);
  {
  DmodG.QrUpdPID1.SQL.Clear;
  DmodG.QrUpdPID1.SQL.Add('INSERT INTO ' + FSVPSitu);
  DmodG.QrUpdPID1.SQL.Add('SELECT Codigo, CodUsu, Nome, 0');
  DmodG.QrUpdPID1.SQL.Add('FROM ' + TMeuDB + '.Situoes');
  UMyMod.ExecutaQuery(DmodG.QrUpdPID1);
  }
  DmSugVda.TbSVPSituIts.Open;
  PageControl1.ActivePageIndex := 0;
end;

procedure TFmSugVPedItsSitu.FormDeactivate(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 1;
  RGSelecao.ItemIndex := 1;
  //
  if FindWindow('TFmSugVPedIts', nil) > 0 then
    FmSugVPedIts.AtivaBtConfirma();
end;

end.

