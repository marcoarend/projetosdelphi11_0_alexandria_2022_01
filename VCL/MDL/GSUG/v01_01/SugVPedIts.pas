unit SugVPedIts;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Tabs, DockTabSet, ExtCtrls, StdCtrls, Buttons, DBCtrls,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, ComCtrls, dmkEditDateTimePicker, DB,
  mySQLDbTables, Grids, DBGrids, frxClass, frxDBSet, dmkDBGrid,
  UnDmkProcFunc;

type
  TFmSugVPedIts = class(TForm)
    pDockLeft: TPanel;
    PainelTitulo: TPanel;
    Image1: TImage;
    Panel3: TPanel;
    BtConfirma: TBitBtn;
    Panel4: TPanel;
    BtDesiste: TBitBtn;
    frxPED_INPRI_000_01: TfrxReport;
    BtImprime: TBitBtn;
    BtLimpa: TBitBtn;
    QrGgx: TmySQLQuery;
    DsGgx: TDataSource;
    frxDsGgx: TfrxDBDataset;
    QrGgxCU_NIVEL1: TIntegerField;
    QrGgxNO_NIVEL1: TWideStringField;
    QrGgxCO_TAM: TIntegerField;
    QrGgxNO_TAM: TWideStringField;
    QrGgxCU_COR: TIntegerField;
    QrGgxNO_COR: TWideStringField;
    QrGgxGraGruC: TIntegerField;
    QrGgxGraGru1: TIntegerField;
    QrGgxGraTamI: TIntegerField;
    QrGgxControle: TIntegerField;
    QrGgxEstq: TFloatField;
    QrGgxPedv: TFloatField;
    QrGgxNec1: TFloatField;
    QrGgxComp: TFloatField;
    QrGgxPrdz: TFloatField;
    QrGgxNec2: TFloatField;
    QrGgxSobr: TFloatField;
    QrGgxAtivo: TIntegerField;
    CkRolar: TCheckBox;
    EdTempo: TdmkEdit;
    Label1: TLabel;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    Panel1: TPanel;
    Panel2: TPanel;
    Splitter1: TSplitter;
    //DockTabSet1: TDockTabSet;
    DockTabSet1: TTabSet;
    TabSheet2: TTabSheet;
    Panel8: TPanel;
    LaAviso1: TLabel;
    Panel6: TPanel;
    GroupBox5: TGroupBox;
    GroupBox1: TGroupBox;
    TPIncluIni: TdmkEditDateTimePicker;
    TPIncluFim: TdmkEditDateTimePicker;
    CkIncluIni: TCheckBox;
    CkIncluFim: TCheckBox;
    GroupBox2: TGroupBox;
    TPEmissIni: TdmkEditDateTimePicker;
    TPEmissFim: TdmkEditDateTimePicker;
    CkEmissIni: TCheckBox;
    CkEmissFim: TCheckBox;
    GroupBox3: TGroupBox;
    TPEntraIni: TdmkEditDateTimePicker;
    TPEntraFim: TdmkEditDateTimePicker;
    CkEntraIni: TCheckBox;
    CkEntraFim: TCheckBox;
    GroupBox4: TGroupBox;
    TPPreviIni: TdmkEditDateTimePicker;
    TPPreviFim: TdmkEditDateTimePicker;
    CkPreviIni: TCheckBox;
    CkPreviFim: TCheckBox;
    GroupBox6: TGroupBox;
    Label2: TLabel;
    dmkEdit1: TdmkEdit;
    dmkEdit2: TdmkEdit;
    GroupBox7: TGroupBox;
    Label3: TLabel;
    dmkEdit3: TdmkEdit;
    dmkEdit4: TdmkEdit;
    Panel10: TPanel;
    LaSVPEmpr: TLabel;
    LaSVPCent: TLabel;
    LaSVPProd: TLabel;
    LaSVPClie: TLabel;
    LaSVPRepr: TLabel;
    LaSVPRegi: TLabel;
    LaSVPRegr: TLabel;
    LaGraAtrCad: TLabel;
    LaSVPSitu: TLabel;
    EdSVPEmpr: TdmkEditCB;
    EdSVPCent: TdmkEditCB;
    EdSVPProd: TdmkEditCB;
    CBSVPProd: TdmkDBLookupComboBox;
    CBSVPCent: TdmkDBLookupComboBox;
    CBSVPEmpr: TdmkDBLookupComboBox;
    EdSVPClie: TdmkEditCB;
    CBSVPClie: TdmkDBLookupComboBox;
    EdSVPRegi: TdmkEditCB;
    EdSVPRepr: TdmkEditCB;
    CBSVPRepr: TdmkDBLookupComboBox;
    CBSVPRegi: TdmkDBLookupComboBox;
    EdSVPRegr: TdmkEditCB;
    CBSVPRegr: TdmkDBLookupComboBox;
    EdGraAtrCad: TdmkEditCB;
    CBGraAtrCad: TdmkDBLookupComboBox;
    EdSVPSitu: TdmkEditCB;
    CBSVPSitu: TdmkDBLookupComboBox;
    Panel5: TPanel;
    LaGraAtrIts: TLabel;
    EdGraAtrIts: TdmkEditCB;
    CBGraAtrIts: TdmkDBLookupComboBox;
    Panel7: TPanel;
    GroupBox8: TGroupBox;
    GroupBox9: TGroupBox;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    dmkEditCB1: TdmkEditCB;
    dmkEditCB2: TdmkEditCB;
    dmkEditCB3: TdmkEditCB;
    GroupBox10: TGroupBox;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    dmkEditCB4: TdmkEditCB;
    dmkEditCB5: TdmkEditCB;
    dmkEditCB6: TdmkEditCB;
    dmkEditCB10: TdmkEditCB;
    dmkEditCB11: TdmkEditCB;
    dmkEditCB12: TdmkEditCB;
    GroupBox11: TGroupBox;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    Label18: TLabel;
    dmkEditCB7: TdmkEditCB;
    dmkEditCB8: TdmkEditCB;
    dmkEditCB9: TdmkEditCB;
    dmkEditCB13: TdmkEditCB;
    dmkEditCB14: TdmkEditCB;
    dmkEditCB15: TdmkEditCB;
    GroupBox12: TGroupBox;
    RadioGroup1: TRadioGroup;
    RadioGroup2: TRadioGroup;
    RadioGroup3: TRadioGroup;
    RadioGroup4: TRadioGroup;
    procedure DockTabSet1TabRemoved(Sender: TObject);
    procedure DockTabSet1DockDrop(Sender: TObject; Source: TDragDockObject; X, Y: Integer);
    procedure pDockLeftDockOver(Sender: TObject; Source: TDragDockObject; X, Y: Integer; State: TDragState;
      var Accept: Boolean);
    procedure pDockLeftUnDock(Sender: TObject; Client: TControl; NewTarget: TWinControl; var Allow: Boolean);
    procedure pDockLeftDockDrop(Sender: TObject; Source: TDragDockObject; X, Y: Integer);
    procedure FormCreate(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure RGOrdemGruClick(Sender: TObject);
    procedure BtImprimeClick(Sender: TObject);
    procedure frxPED_INPRI_000_01GetValue(const VarName: string;
      var Value: Variant);
    procedure FormActivate(Sender: TObject);
    procedure DockTabSet1TabAdded(Sender: TObject);
    procedure BtLimpaClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure QrGgxAfterOpen(DataSet: TDataSet);
    procedure QrGgxBeforeClose(DataSet: TDataSet);
    procedure EdSVPEmprChange(Sender: TObject);
    procedure EdSVPCentChange(Sender: TObject);
    procedure EdSVPProdChange(Sender: TObject);
    procedure RGOrdemCorClick(Sender: TObject);
    procedure CkSemNegativosClick(Sender: TObject);
    procedure CkHideSemValClick(Sender: TObject);
    procedure CkIncluIniClick(Sender: TObject);
    procedure DBGrid1DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure RGMostrarClick(Sender: TObject);
    procedure EdGraAtrCadExit(Sender: TObject);
    procedure EdGraAtrCadChange(Sender: TObject);
  private
    { Private declarations }
    FInRevise: Boolean;
    FLcoTb001: String;
    //
    procedure RecriaDockForms();
    procedure RevisaDockForms();
    //procedure CriaQrSVP();
    //procedure ListaQrSVP();
    //
    //procedure VerificaSQLClie(Liga, Campo: String; var SQL: String);
    //procedure VerificaSQLAtr(var SQL: String);
    //procedure MostraAviso(LaTipo: TLabel; var Seq: Integer; Msg: String);
  public
    { Public declarations }
    //FPCIAtrCli_Txt,
    //FPCIAtrRep_Txt,
    FPCIAtrPrd_Txt: String;
    //FPCIAtrCli_Cod,
    //FPCIAtrRep_Cod,
    FPCIAtrPrd_Cod: Integer;
    //
    procedure AtivaBtConfirma();
    procedure FechaPesquisa();
  end;

var
  FmSugVPedIts: TFmSugVPedIts;

implementation

uses UnMyObjects, UMySQLModule,
// DockForms
SugVPedItsClie, 
// FIM DockForms
Module, ModuleGeral, UCreate, ModSugVda, ModPediVda, SugVPedItsRepr,
  SugVPedItsRegi, SugVPedItsRegr, SugVPedItsProd, SugVPedItsSitu,
  SugVPedItsAtrProd;
  //SugVPedItsRegi, SugVPedItsRegr, SugVPedItsProd, SugVPedItsSitu,
  //SugVPedItsAtrProd;

{$R *.dfm}

procedure TFmSugVPedIts.AtivaBtConfirma();
begin
  BtConfirma.Enabled := True;
end;

procedure TFmSugVPedIts.BtImprimeClick(Sender: TObject);
var
  Tempo: TDateTime;
begin
  Tempo := Now();
  LaAviso1.Caption := 'AGUARDE. Gerando visualiza��o...';
  if CkRolar.Checked = False then
    QrGgx.DisableControls;
  MyObjects.frxMostra(frxPED_INPRI_000_01, Caption);
  QrGgx.EnableControls;
  LaAviso1.Caption := '...';
  EdTempo.ValueVariant := FormatDateTime('hh:nn:ss:zzz',Now() - Tempo);
end;

procedure TFmSugVPedIts.BtLimpaClick(Sender: TObject);
begin
  RecriaDockForms();
  //
  EdSVPEmpr.ValueVariant := 0;
  CBSVPEmpr.KeyValue     := Null;
  EdSVPCent.ValueVariant := 0;
  CBSVPCent.KeyValue     := Null;
  EdSVPProd.ValueVariant := 0;
  CBSVPProd.KeyValue     := Null;
  //
  CkEmissFim.Checked    := True;
  CkEmissIni.Checked    := True;
  CkEntraFim.Checked    := True;
  CkEntraIni.Checked    := True;
  CkIncluFim.Checked    := True;
  CkIncluIni.Checked    := True;
  CkPreviFim.Checked    := True;
  CkPreviIni.Checked    := True;
  //
  TPIncluIni.Date := 0;
  TPIncluFim.Date := Date;
  TPEmissIni.Date := 0;
  TPEmissFim.Date := Date;
  TPEntraIni.Date := 0;
  TPEntraFim.Date := Date;
  TPPreviIni.Date := 0;
  TPPreviFim.Date := Date + 180;
  //
  FmSugVPedItsClie.LimpaPesquisa();
  FmSugVPedItsRepr.LimpaPesquisa();
  FmSugVPedItsRegi.LimpaPesquisa();
  FmSugVPedItsRegr.LimpaPesquisa();
  FmSugVPedItsProd.LimpaPesquisa();
  FmSugVPedItsSitu.LimpaPesquisa();
  FmSugVPedItsAtrProd.LimpaPesquisa();
  // ...
  //
end;

procedure TFmSugVPedIts.BtConfirmaClick(Sender: TObject);
{
var
  SQL1, SQL2, SQL2a, SQL2b, SQL3, SQL4, SQL_Temp, Ordem: String;
  Seq: Integer;
  Liga: String;
  Tempo: TDateTime;
}
begin
  {
  Screen.Cursor := crHourGlass;
  //
  Tempo := Now();
  Seq := 0;
  MostraAviso(LaAviso1, Seq, 'Pesquisando reduzidos conforme filtros da pesquisa...');
  CriaQrSVP();
  ListaQrSVP();
  UMyMod.ExecutaDB(DmodG.MyPID_DB, DEL+ETE FROM  + FLcoTb001);
  //
  SQL1 := SQL1 + 'INSERT INTO ' + FLcoTb001 + sLineBreak;
  SQL1 := SQL1 + 'SELECT ggx.GraGruC, ggx.GraGru1, ggx.GraTamI,' + sLineBreak;
  SQL1 := SQL1 + 'ggx.Controle, 0 Estq, 0 Comp, 0 Pedv, 0 Prdz,' + sLineBreak;
  SQL1 := SQL1 + '0 Nec1, 0 Nec2, 0 Sobr, 1 Ativo' + sLineBreak;
  SQL1 := SQL1 + 'FROM planning.gragrux ggx' + sLineBreak;
  if (DmSugVda.TSVPAtrProdIts.RecordCount > 0) and (FPCIAtrPrd_Cod <> 0) then
  begin
    SQL1 := SQL1 + 'LEFT JOIN planning.gragruatr gga ' +
      'ON gga.Nivel1=ggx.GraGru1 AND gga.GraAtrCad=' +
      FormatFloat('0', FPCIAtrPrd_Cod) + sLineBreak;
    // n�o precisa
    //SQL1 := SQL1 +'LEFT JOIN graatrits gai ON gai.Controle=gga.GraAtrIts ');
  end;
  SQL1 := SQL1 +'WHERE ggx.Controle > -1000000' + sLineBreak;
  //
  VerificaSQLPrd('ggx.GraGru1', SQL1);
  VerificaSQLAtr(SQL1);
  //
  UMyMod.ExecutaDB(DmodG.MyPID_DB, SQL1);
  /

  //
  MostraAviso(LaAviso1, Seq, 'Verificando estoques conforme filtros da pesquisa...');
  DmodG.QrUpdPID1.SQL.Clear;
  SQL2a := '';
  VerificaSQLEmp('  WHERE', 'Empresa', SQL2a);
  SQL2b := '';
  if Trim(SQL2a) <> '' then
    VerificaSQLCen('  AND', 'StqCenCad', SQL2b)
  else
    VerificaSQLCen('  WHERE', 'StqCenCad', SQL2b);
  SQL2 :=
    'UPDATE ' + FLcoTb001 + ','sLineBreak+
    '('sLineBreak +
    '  SELECT GraGruX, SUM(Qtde) AS QtdeSum'sLineBreak +
    '  FROM planning.stqmovitsa'sLineBreak +
    SQL2a +
    SQL2b +
    '  GROUP BY GraGruX'sLineBreak +
    ') AS T'sLineBreak +
    'SET ' + FLcoTb001 + '.Estq=QtdeSum WHERE ' + FLcoTb001 + '.Controle=T.GraGrux';
  UMyMod.ExecutaDB(DmodG.MyPID_DB, SQL2);
  //
  (*
  if CkSemNegativos.Checked then
  begin
    MostraAviso(LaAviso1, Seq, 'Zerando estoques negativos na pesquisa...');
    UMyMod.ExecutaDB(DmodG.MyPID_DB, 'UPDATE ' + FLcoTb001 + ' SET Estq=0 WHERE Estq<0');
  end;
  *)
  //
  MostraAviso(LaAviso1, Seq, 'Verificando pedidos conforme filtros da pesquisa...');
  SQL3 :=
    'UPDATE ' + FLcoTb001 + ','sLineBreak+
    '('sLineBreak+
    '  SELECT pvi.GraGruX, SUM(pvi.QuantP-pvi.QuantC-pvi.QuantV) AS QtdeSum'sLineBreak+
    '  FROM planning.pedivdaits pvi'sLineBreak+
    '  LEFT JOIN planning.pedivda pvd ON pvd.Codigo=pvi.Codigo'sLineBreak+
    '  WHERE pvi.QuantP > pvi.QuantC+pvi.QuantV'sLineBreak;
    VerificaSQLEmp('  AND', 'Empresa', SQL3);


    // Data da inclus�o
    SQL_Temp := dmkPF.SQL_Periodo('  AND pvd.DtaInclu ',
      TPIncluIni.Date, TPIncluFim.Date, CkIncluIni.Checked, CkIncluFim.Checked);
    if SQL_Temp <> '' then
      SQL3 := SQL3 + SQL_Temp + sLineBreak;
    //
    // Data da emiss�o
    SQL_Temp := dmkPF.SQL_Periodo('  AND pvd.DtaEmiss ',
      TPEmissIni.Date, TPEmissFim.Date, CkEmissIni.Checked, CkEmissFim.Checked);
    if SQL_Temp <> '' then
      SQL3 := SQL3 + SQL_Temp + sLineBreak;
    //
    // Data da entrada
    SQL_Temp := dmkPF.SQL_Periodo('  AND pvd.DtaEntra ',
      TPEntraIni.Date, TPEntraFim.Date, CkEntraIni.Checked, CkEntraFim.Checked);
    if SQL_Temp <> '' then
      SQL3 := SQL3 + SQL_Temp + sLineBreak;
    //
    // Data da previs�o
    SQL_Temp := dmkPF.SQL_Periodo('  AND pvd.DtaPrevi ',
      TPPreviIni.Date, TPPreviFim.Date, CkPreviIni.Checked, CkPreviFim.Checked);
    if SQL_Temp <> '' then
      SQL3 := SQL3 + SQL_Temp + sLineBreak;
    //
    SQL3 := SQL3 +
    '  GROUP BY pvi.GraGruX'sLineBreak+
    ') AS T'sLineBreak+
    'SET ' + FLcoTb001 + '.Pedv=QtdeSum WHERE ' + FLcoTb001 + '.Controle=T.GraGrux'sLineBreak;
  //
  UMyMod.ExecutaDB(DmodG.MyPID_DB, SQL3);


  //
  MostraAviso(LaAviso1, Seq, 'Definindo necessidades...');
  SQL4 :=
    'UPDATE ' + FLcoTb001 + ' SET Nec1=Estq-Pedv,' +
    'Nec2=IF(Estq-Pedv+Comp+Prdz<0,Estq-Pedv+Comp+Prdz,0),' +
    'Sobr=IF(Estq-Pedv+Comp+Prdz>0,Estq-Pedv+Comp+Prdz,0)';
  UMyMod.ExecutaDB(DmodG.MyPID_DB, SQL4);



  //
  MostraAviso(LaAviso1, Seq, 'Abrindo tabela com resultados da pesquisa...');
  QrGgx.Close;
  QrGgx.SQL.Clear;
  //QrGgx.SQL.Add('SELECT * FROM ' + FLcoTb001);
  QrGgx.SQL.Add('SELECT gg1.CodUsu CU_NIVEL1, gg1.Nome NO_NIVEL1,');
  QrGgx.SQL.Add('gti.Controle CO_TAM, gti.Nome NO_TAM,');
  //QrGgx.SQL.Add('gtc.CodUsu CU_GRADE, gtc.Nome NO_GRADE,');
  QrGgx.SQL.Add('gcc.CodUsu CU_COR, gcc.Nome NO_COR, pci.*');
  QrGgx.SQL.Add('FROM ' + FLcoTb001 + ' pci');
  QrGgx.SQL.Add('LEFT JOIN planning.gragru1 gg1 ON gg1.Nivel1=pci.GraGru1');
  QrGgx.SQL.Add('LEFT JOIN planning.gratamits gti ON gti.Controle=pci.GraTamI');
  QrGgx.SQL.Add('LEFT JOIN planning.gratamcad gtc ON gtc.Codigo=gti.Codigo');
  QrGgx.SQL.Add('LEFT JOIN planning.gragruc   ggc ON ggc.Controle=pci.GraGruC');
  QrGgx.SQL.Add('LEFT JOIN planning.gracorcad gcc ON gcc.Codigo=ggc.GraCorCad');
  (*
  case RGMostrar.ItemIndex of
    0: ;//nada
    1: QrGgx.SQL.Add('WHERE pci.Estq <> 0 OR pci.Comp <> 0 OR pci.Pedv <> 0 OR pci.Prdz <> 0');
    2: QrGgx.SQL.Add('WHERE pci.Nec2 < 0');
  end;
  QrGgx.SQL.Add('');
  Ordem := 'ORDER BY ';
  case RGOrdemGru.ItemIndex of
    0: Ordem := Ordem + 'CU_NIVEL1, ';
    1: Ordem := Ordem + 'NO_NIVEL1, ';
  end;
  case RGOrdemCor.ItemIndex of
    0: Ordem := Ordem + 'CU_COR, ';
    1: Ordem := Ordem + 'NO_COR, ';
  end;
  Ordem := Ordem + 'CO_TAM';
  QrGgx.SQL.Add(Ordem);
  *)
  //
  UMyMod.AbreQuery(QrGgx, '??????????????');
  LaAviso1.Caption := 'Pesquisa conclu�da!';
  //
  EdTempo.ValueVariant := FormatDateTime('hh:nn:ss:zzz',Now() - Tempo);
  Screen.Cursor := crDefault;
  }
end;

procedure TFmSugVPedIts.BtDesisteClick(Sender: TObject);
begin
  Close;
end;

procedure TFmSugVPedIts.DBGrid1DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
{
var
  CorTexto, CorFundo: TColor;
}
begin
{
  if QrGgxNec2.Value < 0 then
  begin
    CorTexto := clRed;
    CorFundo := $00E4E4E4;
  end else begin
    CorTexto := clNavy;
    CorFundo := clWhite;
  end;
  MyObjects.DesenhaTextoEmDBGrid(DBGrid1, Rect, CorTexto, CorFundo,
    Column.Alignment, Column.Field.DisplayText);
}
end;

procedure TFmSugVPedIts.DockTabSet1DockDrop(Sender: TObject; Source: TDragDockObject; X, Y: Integer);
begin
  DockTabSet1.Visible := True;
end;

procedure TFmSugVPedIts.DockTabSet1TabAdded(Sender: TObject);
begin
  DockTabSet1.Visible := DockTabSet1.Tabs.Count > 0;
end;

procedure TFmSugVPedIts.DockTabSet1TabRemoved(Sender: TObject);
begin
  DockTabSet1.Visible := DockTabSet1.Tabs.Count > 0;
  if FInRevise then Exit;
  RevisaDockForms();
end;

procedure TFmSugVPedIts.EdGraAtrCadChange(Sender: TObject);
begin
  FechaPesquisa();
  if not EdGraAtrCad.Focused then
    DmSugVda.ReopenGraAtrIts(0);
end;

procedure TFmSugVPedIts.EdGraAtrCadExit(Sender: TObject);
begin
  DmSugVda.ReopenGraAtrIts(0);
end;

procedure TFmSugVPedIts.EdSVPCentChange(Sender: TObject);
begin
  FechaPesquisa();
end;

procedure TFmSugVPedIts.EdSVPEmprChange(Sender: TObject);
begin
  FechaPesquisa();
end;

procedure TFmSugVPedIts.EdSVPProdChange(Sender: TObject);
begin
  FechaPesquisa();
end;

procedure TFmSugVPedIts.FechaPesquisa;
begin
  LaAviso1.Caption := '...';
  QrGgx.Close;
end;

procedure TFmSugVPedIts.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmSugVPedIts.FormCreate(Sender: TObject);
var
  FmtQtde: String;
begin
  CBSVPEmpr.ListSource := DModG.DsEmpresas;
  DmSugVda.ReopenAll();
  //
  FmtQtde := dmkPF.FormataCasas(Dmod.QrControleCasasProd.Value);
  FLcoTb001 := UCriar.RecriaTempTable('svi1', DmodG.QrUpdPID1, False);
  TFmSugVPedItsClie.CreateDockForm(clWhite).ManualDock(DockTabSet1);
  TFmSugVPedItsRepr.CreateDockForm(clWhite).ManualDock(DockTabSet1);
  TFmSugVPedItsRegi.CreateDockForm(clWhite).ManualDock(DockTabSet1);
  TFmSugVPedItsRegr.CreateDockForm(clWhite).ManualDock(DockTabSet1);
  TFmSugVPedItsProd.CreateDockForm(clWhite).ManualDock(DockTabSet1);
  TFmSugVPedItsSitu.CreateDockForm(clWhite).ManualDock(DockTabSet1);
  TFmSugVPedItsAtrProd.CreateDockForm(clWhite).ManualDock(DockTabSet1);
  // ...
  TPIncluIni.Date := 0;
  TPIncluFim.Date := Date;
  TPEmissIni.Date := 0;
  TPEmissFim.Date := Date;
  TPEntraIni.Date := 0;
  TPEntraFim.Date := Date;
  TPPreviIni.Date := 0;
  TPPreviFim.Date := Date + 180;
  //
  QrGgxEstq.DisplayFormat := FmtQtde;
  QrGgxComp.DisplayFormat := FmtQtde;
  QrGgxNec1.DisplayFormat := FmtQtde;
  QrGgxPedv.DisplayFormat := FmtQtde;
  QrGgxPrdz.DisplayFormat := FmtQtde;
  QrGgxNec2.DisplayFormat := FmtQtde;
  QrGgxSobr.DisplayFormat := FmtQtde;
end;

procedure TFmSugVPedIts.FormDestroy(Sender: TObject);
begin
  //DModG.ExcluiTempTable(FLcoTb001);
end;

procedure TFmSugVPedIts.frxPED_INPRI_000_01GetValue(const VarName: string;
  var Value: Variant);
begin
  if VarName = 'VARF_SUB_TITULO' then
    Value := '? ? ? ? ? '
  else
  if VarName = 'VARF_PERIODO_INCLU' then
    Value := dmkPF.PeriodoImp2(TPIncluIni.Date, TPIncluFim.Date,
      CkIncluIni.Checked, CkIncluFim.Checked, '', '', '')
  else
  if VarName = 'VARF_PERIODO_EMISS' then
    Value := dmkPF.PeriodoImp2(TPEmissIni.Date, TPEmissFim.Date,
      CkEmissIni.Checked, CkEmissFim.Checked, '', '', '')
  else
  if VarName = 'VARF_PERIODO_ENTRA' then
    Value := dmkPF.PeriodoImp2(TPEntraIni.Date, TPEntraFim.Date,
      CkEntraIni.Checked, CkEntraFim.Checked, '', '', '')
  else
  if VarName = 'VARF_PERIODO_PREVI' then
    Value := dmkPF.PeriodoImp2(TPPreviIni.Date, TPPreviFim.Date,
      CkPreviIni.Checked, CkPreviFim.Checked, '', '', '')
  else
  if VarName = 'VARF_FMT' then
    Value := dmkPF.FormataCasas(Dmod.QrControleCasasProd.Value)
end;

procedure TFmSugVPedIts.pDockLeftDockDrop(Sender: TObject; Source: TDragDockObject; X, Y: Integer);
begin
  if pDockLeft.Width = 0 then
    pDockLeft.Width := 150;
  Splitter1.Visible := True;
  Splitter1.Left := pDockLeft.Width;
end;

procedure TFmSugVPedIts.pDockLeftDockOver(Sender: TObject; Source: TDragDockObject; X, Y: Integer; State: TDragState;
  var Accept: Boolean);
var
  lRect: TRect;
begin
  Accept := Source.Control is TFmSugVPedItsClie;
  if Accept then
  begin
    lRect.TopLeft := pDockLeft.ClientToScreen(Point(0, 0));
    lRect.BottomRight := pDockLeft.ClientToScreen(Point(150, pDockLeft.Height));
    Source.DockRect := lRect;
  end;
end;

procedure TFmSugVPedIts.pDockLeftUnDock(Sender: TObject; Client: TControl; NewTarget: TWinControl; var Allow: Boolean);
begin
  if pDockLeft.DockClientCount = 1 then
  begin
    pDockLeft.Width := 0;
    Splitter1.Visible := False;
  end;
end;

procedure TFmSugVPedIts.QrGgxAfterOpen(DataSet: TDataSet);
begin
  BtImprime.Enabled := QrGgx.RecordCount > 0;
end;

procedure TFmSugVPedIts.QrGgxBeforeClose(DataSet: TDataSet);
begin
  BtImprime.Enabled := False;
end;

procedure TFmSugVPedIts.RecriaDockForms();
{
var
  i: Integer;
}
begin
  {
  for i := 0 to Screen.FormCount - 1 do
  begin
    ShowMessage(Screen.Forms[i].Name + sLineBreak + Screen.Forms[i].Caption);
    if Screen.Forms[i] is TFmSugVPedItsClie then
    begin
      Screen.Forms[i].Close;
      TFmSugVPedItsClie.CreateDockForm(clWhite).ManualDock(DockTabSet1);
    end;
    if Screen.Forms[i] is TFmSugVPedItsCen then
    begin
      Screen.Forms[i].Close;
      TFmSugVPedItsCen.CreateDockForm(clWhite).ManualDock(DockTabSet1);
    end;
    if Screen.Forms[i] is TFmSugVPedItsPrd then
    begin
      Screen.Forms[i].Close;
      TFmSugVPedItsPrd.CreateDockForm(clWhite).ManualDock(DockTabSet1);
    end;
    if Screen.Forms[i] is TFmSugVPedItsAtrPrd then
    begin
      Screen.Forms[i].Close;
      TFmSugVPedItsAtrPrd.CreateDockForm(clWhite).ManualDock(DockTabSet1);
    end;
  end;
  }
end;

procedure TFmSugVPedIts.RevisaDockForms();
const
  MaxJan = 6;
  Janelas: array [0..MaxJan] of String = ('Clientes', 'Representantes',
  'Regi�es', 'Regras Fiscais', 'Grupos de Produtos', 'Situa��es',
  'Atributos de Produtos');
var
  I, J, K: Integer;
  Achou: Boolean;
  //CustomForm: TCustomForm;
begin
  FInRevise := True;
  //for J := Low(Janelas) to High(Janelas) do
  for J := 0 to MaxJan do
  begin
    Achou := False;
    for I  := 0 to DockTabSet1.Tabs.Count - 1 do
    begin
      if Uppercase(Janelas[J]) = Uppercase(DockTabSet1.Tabs[I]) then
      begin
        Achou := True;
        Break;
      end;
    end;
    if not Achou then
    begin
      case J of
        0:
        begin
          for K := 0 to Screen.FormCount - 1 do
          begin
            if Screen.Forms[K] is TFmSugVPedItsClie then
            begin
              Screen.Forms[K].Close;
              TFmSugVPedItsClie.CreateDockForm(clWhite).ManualDock(DockTabSet1);
            end;
          end;
        end;
        1:
        begin
          for K := 0 to Screen.FormCount - 1 do
          begin
            if Screen.Forms[K] is TFmSugVPedItsRepr then
            begin
              Screen.Forms[K].Close;
              TFmSugVPedItsRepr.CreateDockForm(clWhite).ManualDock(DockTabSet1);
            end;
          end;
        end;
        2:
        begin
          for K := 0 to Screen.FormCount - 1 do
          begin
            if Screen.Forms[K] is TFmSugVPedItsRegi then
            begin
              Screen.Forms[K].Close;
              TFmSugVPedItsRegi.CreateDockForm(clWhite).ManualDock(DockTabSet1);
            end;
          end;
        end;
        3:
        begin
          for K := 0 to Screen.FormCount - 1 do
          begin
            if Screen.Forms[K] is TFmSugVPedItsRegr then
            begin
              Screen.Forms[K].Close;
              TFmSugVPedItsRegr.CreateDockForm(clWhite).ManualDock(DockTabSet1);
            end;
          end;
        end;
        4:
        begin
          for K := 0 to Screen.FormCount - 1 do
          begin
            if Screen.Forms[K] is TFmSugVPedItsProd then
            begin
              Screen.Forms[K].Close;
              TFmSugVPedItsProd.CreateDockForm(clWhite).ManualDock(DockTabSet1);
            end;
          end;
        end;
        5:
        begin
          for K := 0 to Screen.FormCount - 1 do
          begin
            if Screen.Forms[K] is TFmSugVPedItsSitu then
            begin
              Screen.Forms[K].Close;
              TFmSugVPedItsSitu.CreateDockForm(clWhite).ManualDock(DockTabSet1);
            end;
          end;
        end;
       6:
        begin
          for K := 0 to Screen.FormCount - 1 do
          begin
            if Screen.Forms[K] is TFmSugVPedItsAtrProd then
            begin
              Screen.Forms[K].Close;
              TFmSugVPedItsAtrProd.CreateDockForm(clWhite).ManualDock(DockTabSet1);
            end;
          end;
        end;
        //
  // ...
      end;
    end;
  end;
  FInRevise := False;
end;

procedure TFmSugVPedIts.RGMostrarClick(Sender: TObject);
begin
  FechaPesquisa();
end;

procedure TFmSugVPedIts.RGOrdemCorClick(Sender: TObject);
begin
  FechaPesquisa();
end;

procedure TFmSugVPedIts.RGOrdemGruClick(Sender: TObject);
begin
  AtivaBtConfirma();
  FechaPesquisa();
end;

{
procedure TFmSugVPedIts.VerificaSQLClie(Liga, Campo: String; var SQL: String);
var
  Sep, Aux: String;
begin
  if EdSVPClie.Enabled  then
  begin
    if EdSVPClie.ValueVariant <> 0 then
    begin
      SQL := SQL + Liga + ' ' + Campo + ' = ' + FormatFloat('0',
        DmSugVda.QrSVPClieCadCodigo.Value) + sLineBreak
    end;
  end else begin
    if DmSugVda.TbSVPClieIts.RecordCount > 0 then
    begin
      Sep := '';
      Aux := Liga + ' ' + Campo + ' IN (';
      DmSugVda.TbSVPClieIts.First;
      while not DmSugVda.TbSVPClieIts.Eof do
      begin
        if DmSugVda.TbSVPClieItsAtivo.Value = 1 then
        begin
          Aux := Aux + Sep + FormatFloat('0', DmSugVda.TbSVPClieItsCodigo.Value);
          Sep := ',';
        end;
        DmSugVda.TbSVPClieIts.Next;
      end;
      SQL := SQL + Aux + ')' + sLineBreak;
    end;
  end;
end;
}
{
procedure TFmSugVPedIts.VerificaSQLAtr(var SQL: String);
var
  Sep, Aux: String;
  QrABS: TABSQuery;
begin
  QrABS := DmSugVda.QrSVPAtrPrdIts;
  if QrABS.RecordCount > 0 then
  begin
    Sep := '';
    Aux := 'AND gga.GraAtrIts IN (';
    //
    QrABS.First;
    while not QrABS.Eof do
    begin
      if QrABS.FieldByName('Ativo').AsInteger = 1 then
      begin
        Aux := Aux + Sep + FormatFloat('0', QrABS.FieldByName('Codigo').AsInteger);
        Sep := ',';
      end;
      //
      QrABS.Next;
    end;
    SQL := SQL + Aux + ')' + sLineBreak;
  end;
end;
}

procedure TFmSugVPedIts.CkHideSemValClick(Sender: TObject);
begin
  FechaPesquisa();
end;

procedure TFmSugVPedIts.CkIncluIniClick(Sender: TObject);
begin
  FechaPesquisa();
end;

procedure TFmSugVPedIts.CkSemNegativosClick(Sender: TObject);
begin
  FechaPesquisa();
end;

{
procedure TFmSugVPedIts.CriaQrSVP();
const
  Txt1 = 'INSERT INTO pci (Codigo,CodUsu,Nome,Ativo) VALUES(';
begin
  QrSVP.Close;
  QrSVP.SQL.Clear;
  QrSVP.SQL.Add('DROP TABLE PCI;         ');
  QrSVP.SQL.Add('CREATE TABLE PCI (      ');
  QrSVP.SQL.Add('  Tabela  varchar(30)  ,');
  QrSVP.SQL.Add('  CodUsu1 integer      ,');
  QrSVP.SQL.Add('  Nome1   varchar(50)  ,');
  QrSVP.SQL.Add('  CodUsu2 integer      ,');
  QrSVP.SQL.Add('  Nome2   varchar(50)  ,');
  QrSVP.SQL.Add('  CodUsu3 integer      ,');
  QrSVP.SQL.Add('  Nome3   varchar(50)  ,');
  QrSVP.SQL.Add('  Ativo   smallint      ');
  QrSVP.SQL.Add(');                      ');
  //
  QrSVP.SQL.Add('SELECT * FROM pci;');
  QrSVP.Open;
  //
  FCriouPCI := True;
end;
}

{
procedure TFmSugVPedIts.ListaQrSVP();
  procedure AdicionaItensTabela(Tabela: String; Qry: TABSQuery);
  const
    Txt1 = 'INSERT INTO pci (Tabela,' +
    'CodUsu1,Nome1,CodUsu2,Nome2,CodUsu3,Nome3,Ativo) VALUES(';
  var
    Txt2: String;
    j: Integer;
    Str1, Str2, Str3: String;
  begin
    j := 0;
    Qry.First;
    while not Qry.Eof do
    begin
      if Qry.FieldByName('Ativo').AsInteger = 1 then
      begin
        if j = 0 then
        begin
          Txt2 := '"' + Tabela + '",';
          Str1 := '0," ",';
          Str2 := '0," ",';
          Str3 := '0," ",1);';
          j := 1;
        end;
        case j of
          1:
          begin
            Str1 :=
            dmkPF.FFP(Qry.FieldByName('CodUsu').AsInteger, 0) + ',' +
            '"' + Qry.FieldByName('Nome').AsString + '",';
            j := 2;
          end;
          2:
          begin
            Str2 :=
            dmkPF.FFP(Qry.FieldByName('CodUsu').AsInteger, 0) + ',' +
            '"' + Qry.FieldByName('Nome').AsString + '",';
            j := 3;
          end;
          3:
          begin
            Str3 :=
            dmkPF.FFP(Qry.FieldByName('CodUsu').AsInteger, 0) + ',' +
            '"' + Qry.FieldByName('Nome').AsString + '",1);';
            //
            // ? QrSVP.SQL.Add(Txt1 + Txt2 + Str1 + Str2 + Str3);
            j := 0;
          end;
        end;
      end;
      Qry.Next;
    end;
    if j > 0 then
      // ?QrSVP.SQL.Add(Txt1 + Txt2 + Str1 + Str2 + Str3);
  end;
  var
    AtribPrd, AtribCli, AtribRep: String;
begin
  QrSVP.Close;
  QrSVP.SQL.Clear;
  QrSVP.SQL.Add('DELETE FROM pci; ');
  //
  AtribPrd := 'Atributo de produto: ' + FPCIAtrPrd_Txt;
  //
  AdicionaItensTabela('Filial',                    DmSugVda.QrSVPEmprIts);
  AdicionaItensTabela('Centro de estoque',         DmSugVda.QrSVPCentIts);
  AdicionaItensTabela('Grupo de produto',          DmSugVda.QrSVPProdIts);
  AdicionaItensTabela(AtribPrd,                    DmSugVda.QrSVPAtrPrdIts);
  //
  QrSVP.SQL.Add('SELECT * FROM pci;');
  UMyMod.AbreABSQuery(QrSVP);
end;
  }

{
procedure TFmSugVPedIts.MostraAviso(LaTipo: TLabel; var Seq: Integer;
  Msg: String);
begin
  Seq := Seq + 1;
  if LaTipo <> nil then
  begin
    LaTipo.Caption := FormatFloat('000', Seq) + '. ' + Msg;
    LaTipo.Update;
    Application.ProcessMessages;
  end;
end;
}

(*
object DockTabSet1: TDockTabSet
  Left = 976
  Top = 0
  Width = 20
  Height = 437
  Align = alRight
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = [fsBold]
  ShrinkToFit = True
  Style = tsModernPopout
  TabPosition = tpLeft
  DockSite = False
  DestinationDockSite = pDockLeft
  OnDockDrop = DockTabSet1DockDrop
  OnTabAdded = DockTabSet1TabAdded
  OnTabRemoved = DockTabSet1TabRemoved
end

*)
end.

