unit SugVPedItsRepr;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DB,
  DBGrids, dmkDBGrid, DBCtrls, mySQLDbTables, Menus, ComCtrls, UnDmkProcFunc;

type
  THackDBGrid = class(TDBGrid);
  TFmSugVPedItsRepr = class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    BtNenhum: TBitBtn;
    BtTodos: TBitBtn;
    BtPesq: TBitBtn;
    PMPesq: TPopupMenu;
    Ativa1: TMenuItem;
    Desativa1: TMenuItem;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    Panel3: TPanel;
    Label1: TLabel;
    EdPesq: TEdit;
    DBGrid1: TdmkDBGrid;
    Panel4: TPanel;
    DBGrid2: TdmkDBGrid;
    RGSelecao: TRadioGroup;
    procedure FormCreate(Sender: TObject);
    procedure DBGrid1CellClick(Column: TColumn);
    procedure FormStartDock(Sender: TObject; var DragObject: TDragDockObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure EdPesqChange(Sender: TObject);
    procedure FormUnDock(Sender: TObject; Client: TControl;
      NewTarget: TWinControl; var Allow: Boolean);
    procedure BtNenhumClick(Sender: TObject);
    procedure BtTodosClick(Sender: TObject);
    procedure BtPesqClick(Sender: TObject);
    procedure Ativa1Click(Sender: TObject);
    procedure Desativa1Click(Sender: TObject);
    procedure PageControl1Change(Sender: TObject);
    procedure FormDeactivate(Sender: TObject);
    procedure RGSelecaoClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    FSVPRepr: String;
    procedure Pesquisar();
    procedure AtivaItens(Status: Integer);
    procedure AtivaSelecionados(Status: Integer);
    procedure HabilitaItemUnico();
  public
    { Public declarations }
    class function CreateDockForm(const aColor: TColor): TCustomForm;
    //class procedure FiltaAtivos();
    procedure LimpaPesquisa();
  end;

var
  FmSugVPedItsRepr: TFmSugVPedItsRepr;

implementation

uses UnMyObjects, Module, UMySQLModule, SugVPedIts, ModSugVda, ModuleGeral, UCreate,
UnInternalConsts, MyGlyfs;

{$R *.DFM}

///// PROCEDURES E FUNCTIONS DO DOCKFORM

procedure TFmSugVPedItsRepr.Ativa1Click(Sender: TObject);
begin
  AtivaSelecionados(1);
end;

procedure TFmSugVPedItsRepr.AtivaItens(Status: Integer);
var
  Codigo: Integer;
begin
  Screen.Cursor := crHourGlass;
  Codigo := DmSugVda.TbSVPReprItsCodigo.Value;
  DModG.QrUpdPID1.SQL.Clear;
  DModG.QrUpdPID1.SQL.Add('UPDATE ' + FSVPRepr + ' SET Ativo=' + dmkPF.FFP(Status, 0) + ';');
  UMyMod.ExecutaQuery(DmodG.QrUpdPID1);
  DmSugVda.TbSVPReprIts.Close;
  DmSugVda.TbSVPReprIts.Open;
  DmSugVda.TbSVPReprIts.Locate('Codigo', Codigo, []);
  HabilitaItemUnico();
  Screen.Cursor := crDefault;
end;

procedure TFmSugVPedItsRepr.AtivaSelecionados(Status: Integer);
begin
  Screen.Cursor := crHourGlass;
  DmSugVda.TbSVPReprIts.First;
  while not DmSugVda.TbSVPReprIts.Eof do
  begin
    if DmSugVda.TbSVPReprItsAtivo.Value <> Status then
    begin
      DmSugVda.TbSVPReprIts.Edit;
      DmSugVda.TbSVPReprItsAtivo.Value := Status;
      DmSugVda.TbSVPReprIts.Post;
    end;
    DmSugVda.TbSVPReprIts.Next;
  end;
  HabilitaItemUnico();
  Screen.Cursor := crDefault;
end;

procedure TFmSugVPedItsRepr.BtPesqClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMPesq, BtPesq);
end;

procedure TFmSugVPedItsRepr.BtNenhumClick(Sender: TObject);
begin
  AtivaItens(0);
end;

procedure TFmSugVPedItsRepr.BtTodosClick(Sender: TObject);
begin
  AtivaItens(1);
end;

class function TFmSugVPedItsRepr.CreateDockForm(const aColor: TColor): TCustomForm;
begin
  result := TFmSugVPedItsRepr.Create(Application);
  //result.Color := aColor;
  //result.Caption := ColorToString(aColor);
  FmMyGlyfs.ConfiguraFormDock(result);
  result.Show;
end;

procedure TFmSugVPedItsRepr.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  DmodG.QrUpdPID1.Close;
  DmodG.QrUpdPID1.SQL.Clear;
  DmodG.QrUpdPID1.SQL.Add('DROP TABLE ' + FSVPRepr + '; ');
  UMyMod.ExecutaQuery(DmodG.QrUpdPID1);
  //
  ManualFloat(Rect(0, 0, 0, 0));
  Action := caFree;
end;

procedure TFmSugVPedItsRepr.FormShow(Sender: TObject);
begin
  FmSugVPedIts.FechaPesquisa();
end;

procedure TFmSugVPedItsRepr.FormStartDock(Sender: TObject;
  var DragObject: TDragDockObject);
begin
  DragObject := TDragDockObjectEx.Create(Self);
  DragObject.Brush.Color := clAqua; // this will display a red outline
end;


procedure TFmSugVPedItsRepr.FormUnDock(Sender: TObject; Client: TControl;
  NewTarget: TWinControl; var Allow: Boolean);
begin
end;

///// FIM DAS PROCEDURES E FUNCTIONS DO DOCKFORM

procedure TFmSugVPedItsRepr.PageControl1Change(Sender: TObject);
begin
  BtPesq.Enabled := PageControl1.ActivePageIndex = 0;
  case PageControl1.ActivePageIndex of
    0: Pesquisar();
    1: RGSelecaoClick(Self);
  end;
end;

procedure TFmSugVPedItsRepr.Pesquisar();
begin
  Screen.Cursor := crHourGlass;
  //
  DmSugVda.TbSVPReprIts.Filter   := 'Nome LIKE ''%' + EdPesq.Text + '%''';
  DmSugVda.TbSVPReprIts.Filtered := True;
  //
  Screen.Cursor := crDefault;
end;

procedure TFmSugVPedItsRepr.RGSelecaoClick(Sender: TObject);
begin
  DmSugVda.TbSVPReprIts.Filtered := False;
  case RGSelecao.ItemIndex of
    0: DmSugVda.TbSVPReprIts.Filter := 'Ativo=0';
    1: DmSugVda.TbSVPReprIts.Filter := 'Ativo=1';
    2: DmSugVda.TbSVPReprIts.Filter := '';
  end;
  if RGSelecao.ItemIndex < 2 then
    DmSugVda.TbSVPReprIts.Filtered := True;
end;

procedure TFmSugVPedItsRepr.DBGrid1CellClick(Column: TColumn);
var
  Status: Integer;
begin
  if Column.FieldName = 'Ativo' then
  begin
    if DmSugVda.TbSVPReprItsAtivo.Value = 1 then
      Status := 0
    else
      Status := 1;
    //
    DmSugVda.TbSVPReprIts.Edit;
    DmSugVda.TbSVPReprItsAtivo.Value := Status;
    DmSugVda.TbSVPReprIts.Post;
  end;
  HabilitaItemUnico();
end;

procedure TFmSugVPedItsRepr.HabilitaItemUnico();
var
  Habilita: Boolean;
begin
  DmSugVda.QrSVPReprAti.Close;
  DmSugVda.QrSVPReprAti.Open;
  Habilita :=  DmSugVda.QrSVPReprAtiItens.Value = 0;
  FmSugVPedIts.LaSVPRepr.Enabled := Habilita;
  FmSugVPedIts.EdSVPRepr.Enabled := Habilita;
  FmSugVPedIts.CBSVPRepr.Enabled := Habilita;
  if not Habilita then
  begin
    FmSugVPedIts.EdSVPRepr.ValueVariant := 0;
    FmSugVPedIts.CBSVPRepr.KeyValue     := 0;
  end;
end;

procedure TFmSugVPedItsRepr.LimpaPesquisa;
var
  K, I: Integer;
begin
  for K := 0 to Screen.FormCount - 1 do
  begin
    if Screen.Forms[K] is TFmSugVPedItsRepr then
    begin
      for I := 0 to Screen.Forms[K].ComponentCount -1 do
      if Screen.Forms[K].Components[I] is TEdit then
        TEdit(Screen.Forms[K].Components[I]).Text := '';
    end;
  end;
  AtivaItens(0);
end;

procedure TFmSugVPedItsRepr.Desativa1Click(Sender: TObject);
begin
  AtivaSelecionados(0);
end;

procedure TFmSugVPedItsRepr.EdPesqChange(Sender: TObject);
begin
  Pesquisar();
end;

procedure TFmSugVPedItsRepr.FormCreate(Sender: TObject);
begin
  DmSugVda.TbSVPReprIts.Close;
  FSVPRepr := UCriar.RecriaTempTable('SVPRepr', DmodG.QrUpdPID1, False);
  DmodG.QrUpdPID1.SQL.Clear;
  DmodG.QrUpdPID1.SQL.Add('INSERT INTO ' + FSVPRepr);
  DmodG.QrUpdPID1.SQL.Add('SELECT Codigo, CodUsu,');
  DmodG.QrUpdPID1.SQL.Add('IF(Tipo=0,RazaoSocial,Nome) Nome, 0');
  DmodG.QrUpdPID1.SQL.Add('FROM ' + TMeuDB + '.entidades');
  DmodG.QrUpdPID1.SQL.Add('WHERE Fornece3="V"');
  DmodG.QrUpdPID1.SQL.Add('OR Fornece4="V"');
  DmodG.QrUpdPID1.SQL.Add('OR Fornece5="V"');
  DmodG.QrUpdPID1.SQL.Add('OR Fornece6="V"');
  DmodG.QrUpdPID1.SQL.Add('OR Fornece7="V"');
  UMyMod.ExecutaQuery(DmodG.QrUpdPID1);
  DmSugVda.TbSVPReprIts.Open;
  PageControl1.ActivePageIndex := 0;
end;

procedure TFmSugVPedItsRepr.FormDeactivate(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 1;
  RGSelecao.ItemIndex := 1;
  //
  if FindWindow('TFmSugVPedIts', nil) > 0 then
    FmSugVPedIts.AtivaBtConfirma();
end;

end.

