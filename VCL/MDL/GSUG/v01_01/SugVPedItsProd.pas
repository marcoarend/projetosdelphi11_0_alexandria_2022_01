unit SugVPedItsProd;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DB,
  DBGrids, dmkDBGrid, DBCtrls, mySQLDbTables, Menus, ComCtrls, UnDmkProcFunc;

type
  THackDBGrid = class(TDBGrid);
  TFmSugVPedItsProd = class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    BtNenhum: TBitBtn;
    BtTodos: TBitBtn;
    BtPesq: TBitBtn;
    PMPesq: TPopupMenu;
    Ativa1: TMenuItem;
    Desativa1: TMenuItem;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    Panel3: TPanel;
    Label1: TLabel;
    EdPesq: TEdit;
    DBGrid1: TdmkDBGrid;
    Panel4: TPanel;
    DBGrid2: TdmkDBGrid;
    RGSelecao: TRadioGroup;
    procedure FormCreate(Sender: TObject);
    procedure DBGrid1CellClick(Column: TColumn);
    procedure FormStartDock(Sender: TObject; var DragObject: TDragDockObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure EdPesqChange(Sender: TObject);
    procedure FormUnDock(Sender: TObject; Client: TControl;
      NewTarget: TWinControl; var Allow: Boolean);
    procedure BtNenhumClick(Sender: TObject);
    procedure BtTodosClick(Sender: TObject);
    procedure BtPesqClick(Sender: TObject);
    procedure Ativa1Click(Sender: TObject);
    procedure Desativa1Click(Sender: TObject);
    procedure PageControl1Change(Sender: TObject);
    procedure FormDeactivate(Sender: TObject);
    procedure RGSelecaoClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    FSVPProd: String;
    procedure Pesquisar();
    procedure AtivaItens(Status: Integer);
    procedure AtivaSelecionados(Status: Integer);
    procedure HabilitaItemUnico();
  public
    { Public declarations }
    class function CreateDockForm(const aColor: TColor): TCustomForm;
    //class procedure FiltaAtivos();
    procedure LimpaPesquisa();
  end;

var
  FmSugVPedItsProd: TFmSugVPedItsProd;

implementation

uses UnMyObjects, Module, UMySQLModule, SugVPedIts, ModSugVda, ModuleGeral, UCreate,
UnInternalConsts, MyGlyfs;

{$R *.DFM}

///// PROCEDURES E FUNCTIONS DO DOCKFORM

procedure TFmSugVPedItsProd.Ativa1Click(Sender: TObject);
begin
  AtivaSelecionados(1);
end;

procedure TFmSugVPedItsProd.AtivaItens(Status: Integer);
var
  Codigo: Integer;
begin
  Screen.Cursor := crHourGlass;
  Codigo := DmSugVda.TbSVPProdItsCodigo.Value;
  DModG.QrUpdPID1.SQL.Clear;
  DModG.QrUpdPID1.SQL.Add('UPDATE ' + FSVPProd + ' SET Ativo=' + dmkPF.FFP(Status, 0) + ';');
  UMyMod.ExecutaQuery(DmodG.QrUpdPID1);
  DmSugVda.TbSVPProdIts.Close;
  DmSugVda.TbSVPProdIts.Open;
  DmSugVda.TbSVPProdIts.Locate('Codigo', Codigo, []);
  HabilitaItemUnico();
  Screen.Cursor := crDefault;
end;

procedure TFmSugVPedItsProd.AtivaSelecionados(Status: Integer);
begin
  Screen.Cursor := crHourGlass;
  DmSugVda.TbSVPProdIts.First;
  while not DmSugVda.TbSVPProdIts.Eof do
  begin
    if DmSugVda.TbSVPProdItsAtivo.Value <> Status then
    begin
      DmSugVda.TbSVPProdIts.Edit;
      DmSugVda.TbSVPProdItsAtivo.Value := Status;
      DmSugVda.TbSVPProdIts.Post;
    end;
    DmSugVda.TbSVPProdIts.Next;
  end;
  HabilitaItemUnico();
  Screen.Cursor := crDefault;
end;

procedure TFmSugVPedItsProd.BtPesqClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMPesq, BtPesq);
end;

procedure TFmSugVPedItsProd.BtNenhumClick(Sender: TObject);
begin
  AtivaItens(0);
end;

procedure TFmSugVPedItsProd.BtTodosClick(Sender: TObject);
begin
  AtivaItens(1);
end;

class function TFmSugVPedItsProd.CreateDockForm(const aColor: TColor): TCustomForm;
begin
  result := TFmSugVPedItsProd.Create(Application);
  //result.Color := aColor;
  //result.Caption := ColorToString(aColor);
  FmMyGlyfs.ConfiguraFormDock(result);
  result.Show;
end;

procedure TFmSugVPedItsProd.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  DmodG.QrUpdPID1.Close;
  DmodG.QrUpdPID1.SQL.Clear;
  DmodG.QrUpdPID1.SQL.Add('DROP TABLE ' + FSVPProd + '; ');
  UMyMod.ExecutaQuery(DmodG.QrUpdPID1);
  //
  ManualFloat(Rect(0, 0, 0, 0));
  Action := caFree;
end;

procedure TFmSugVPedItsProd.FormShow(Sender: TObject);
begin
  FmSugVPedIts.FechaPesquisa();
end;

procedure TFmSugVPedItsProd.FormStartDock(Sender: TObject;
  var DragObject: TDragDockObject);
begin
  DragObject := TDragDockObjectEx.Create(Self);
  DragObject.Brush.Color := clAqua; // this will display a red outline
end;


procedure TFmSugVPedItsProd.FormUnDock(Sender: TObject; Client: TControl;
  NewTarget: TWinControl; var Allow: Boolean);
begin
end;

///// FIM DAS PROCEDURES E FUNCTIONS DO DOCKFORM

procedure TFmSugVPedItsProd.PageControl1Change(Sender: TObject);
begin
  BtPesq.Enabled := PageControl1.ActivePageIndex = 0;
  case PageControl1.ActivePageIndex of
    0: Pesquisar();
    1: RGSelecaoClick(Self);
  end;
end;

procedure TFmSugVPedItsProd.Pesquisar();
begin
  Screen.Cursor := crHourGlass;
  //
  DmSugVda.TbSVPProdIts.Filter   := 'Nome LIKE ''%' + EdPesq.Text + '%''';
  DmSugVda.TbSVPProdIts.Filtered := True;
  //
  Screen.Cursor := crDefault;
end;

procedure TFmSugVPedItsProd.RGSelecaoClick(Sender: TObject);
begin
  DmSugVda.TbSVPProdIts.Filtered := False;
  case RGSelecao.ItemIndex of
    0: DmSugVda.TbSVPProdIts.Filter := 'Ativo=0';
    1: DmSugVda.TbSVPProdIts.Filter := 'Ativo=1';
    2: DmSugVda.TbSVPProdIts.Filter := '';
  end;
  if RGSelecao.ItemIndex < 2 then
    DmSugVda.TbSVPProdIts.Filtered := True;
end;

procedure TFmSugVPedItsProd.DBGrid1CellClick(Column: TColumn);
var
  Status: Integer;
begin
  if Column.FieldName = 'Ativo' then
  begin
    if DmSugVda.TbSVPProdItsAtivo.Value = 1 then
      Status := 0
    else
      Status := 1;
    //
    DmSugVda.TbSVPProdIts.Edit;
    DmSugVda.TbSVPProdItsAtivo.Value := Status;
    DmSugVda.TbSVPProdIts.Post;
  end;
  HabilitaItemUnico();
end;

procedure TFmSugVPedItsProd.HabilitaItemUnico();
var
  Habilita: Boolean;
begin
  DmSugVda.QrSVPProdAti.Close;
  DmSugVda.QrSVPProdAti.Open;
  Habilita :=  DmSugVda.QrSVPProdAtiItens.Value = 0;
  FmSugVPedIts.LaSVPProd.Enabled := Habilita;
  FmSugVPedIts.EdSVPProd.Enabled := Habilita;
  FmSugVPedIts.CBSVPProd.Enabled := Habilita;
  if not Habilita then
  begin
    FmSugVPedIts.EdSVPProd.ValueVariant := 0;
    FmSugVPedIts.CBSVPProd.KeyValue     := 0;
  end;
end;

procedure TFmSugVPedItsProd.LimpaPesquisa;
var
  K, I: Integer;
begin
  for K := 0 to Screen.FormCount - 1 do
  begin
    if Screen.Forms[K] is TFmSugVPedItsProd then
    begin
      for I := 0 to Screen.Forms[K].ComponentCount -1 do
      if Screen.Forms[K].Components[I] is TEdit then
        TEdit(Screen.Forms[K].Components[I]).Text := '';
    end;
  end;
  AtivaItens(0);
end;

procedure TFmSugVPedItsProd.Desativa1Click(Sender: TObject);
begin
  AtivaSelecionados(0);
end;

procedure TFmSugVPedItsProd.EdPesqChange(Sender: TObject);
begin
  Pesquisar();
end;

procedure TFmSugVPedItsProd.FormCreate(Sender: TObject);
begin
  DmSugVda.TbSVPProdIts.Close;
  FSVPProd := UCriar.RecriaTempTable('SVPProd', DmodG.QrUpdPID1, False);
  DmodG.QrUpdPID1.SQL.Clear;
  DmodG.QrUpdPID1.SQL.Add('INSERT INTO ' + FSVPProd);
  DmodG.QrUpdPID1.SQL.Add('SELECT Nivel1, CodUsu, Nome, 0');
  DmodG.QrUpdPID1.SQL.Add('FROM ' + TMeuDB + '.gragru1');
  DmodG.QrUpdPID1.SQL.Add('WHERE Nivel1<>0');
  UMyMod.ExecutaQuery(DmodG.QrUpdPID1);
  DmSugVda.TbSVPProdIts.Open;
  PageControl1.ActivePageIndex := 0;
end;

procedure TFmSugVPedItsProd.FormDeactivate(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 1;
  RGSelecao.ItemIndex := 1;
  //
  if FindWindow('TFmSugVPedIts', nil) > 0 then
    FmSugVPedIts.AtivaBtConfirma();
end;

end.

