unit UnOVS_Jan;

interface

uses
  Windows, SysUtils, Classes, DB, Menus, UnDmkEnums, mySQLDbTables, Forms,
  Grids, DBGrids, UnInternalConsts;

type
  TUnOVS_Jan = class(TObject)
  private
    { Private declarations }

  public
    { Public declarations }
    procedure MostraFormImportaCSV_ERP_01();
  end;

var
  OVS_Jan: TUnOVS_Jan;


implementation

uses
  MyDBCheck, Module, ImportaCSV_ERP_01;

{ TUnOVS_Jan }

procedure TUnOVS_Jan.MostraFormImportaCSV_ERP_01();
begin
  if DBCheck.CriaFm(TFmImportaCSV_ERP_01, FmImportaCSV_ERP_01, afmoSoBoss) then
  begin
    FmImportaCSV_ERP_01.ShowModal;
    FmImportaCSV_ERP_01.Destroy;
  end;
end;

end.
