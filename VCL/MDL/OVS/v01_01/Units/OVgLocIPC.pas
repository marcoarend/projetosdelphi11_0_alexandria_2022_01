unit OVgLocIPC;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkPermissoes,
  dmkDBLookupComboBox, dmkEditCB, mySQLDbTables, dmkDBGridZTO;

type
  TFmOVgLocIPC = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    GroupBox2: TGroupBox;
    Panel5: TPanel;
    Label1: TLabel;
    EdArtigo: TdmkEditCB;
    CBArtigo: TdmkDBLookupComboBox;
    QrOVdReferencia: TMySQLQuery;
    DsOVdReferencia: TDataSource;
    EdReferencia: TdmkEdit;
    Label2: TLabel;
    QrOVdReferenciaCodigo: TIntegerField;
    QrOVdReferenciaNome: TWideStringField;
    QrOVgIspPrfCab: TMySQLQuery;
    QrOVgIspPrfCabCodigo: TIntegerField;
    QrOVgIspPrfCabNome: TWideStringField;
    QrOVgIspPrfCabSeqGrupo: TIntegerField;
    QrOVgIspPrfCabOVcYnsMed: TIntegerField;
    QrOVgIspPrfCabOVcYnsChk: TIntegerField;
    QrOVgIspPrfCabLimiteChk: TIntegerField;
    QrOVgIspPrfCabLimiteMed: TIntegerField;
    QrOVgIspPrfCabAtivo: TSmallintField;
    QrOVgIspPrfCabNO_OVcYnsMed: TWideStringField;
    QrOVgIspPrfCabNO_OVcYnsChk: TWideStringField;
    QrOVgIspPrfCabOVcYnsARQ: TIntegerField;
    DsOVgIspPrfCab: TDataSource;
    DBGrid1: TdmkDBGridZTO;
    RGAtivo: TRadioGroup;
    QrOVgIspPrfCabNO_Referencia: TWideStringField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure EdArtigoChange(Sender: TObject);
    procedure EdArtigoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdReferenciaChange(Sender: TObject);
    procedure EdReferenciaExit(Sender: TObject);
    procedure EdReferenciaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBGrid1DblClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    FCodigo: Integer;
  end;

  var
  FmOVgLocIPC: TFmOVgLocIPC;

implementation

uses UnMyObjects, Module, UnMySQLCuringa, DmkDAC_PF, ModOVS;

{$R *.DFM}

procedure TFmOVgLocIPC.BtOKClick(Sender: TObject);
var
  Artigo, Ativo: Integer;
  SQL_Artigo, SQL_Ativo: String;
begin
  Artigo           := EdArtigo.ValueVariant;
  Ativo            := RGAtivo.ItemIndex;
  //
  SQL_Artigo       := '';
  SQL_Ativo        := '';
  //
  if Artigo <> 0 then
    SQL_Artigo := 'AND igc.SeqGrupo=' + Geral.FF0(Artigo);
  if Ativo <> 2 then
    SQL_Ativo := 'AND igc.Ativo=' + Geral.FF0(Ativo);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrOVgIspPrfCab, Dmod.MyDB, [
(*
  'SELECT igc.*, ymc.Nome NO_OVcYnsMed, ycc.Nome NO_OVcYnsChk, ',
  'dlo.Nome NO_Local, ref.Nome NO_Referencia,  ',
  'isc.Nome NO_ZtatusIsp, yac.Nome NO_OVcYnsARQ,  ',
  'IF(igc.DtHrAbert  <= "1899-12-30", "",   ',
  '  DATE_FORMAT(igc.DtHrAbert, "%d/%m/%Y %H:%i:%s")) DtHrAbert_TXT,   ',
  'IF(igc.DtHrFecha  <= "1899-12-30", "",   ',
  '  DATE_FORMAT(igc.DtHrFecha, "%d/%m/%Y %H:%i:%s")) DtHrFecha_TXT, ',
  'IF(Ativo=0, "Sim", "N�o") NO_Ativo ',
  'FROM ovgispprfcab igc ',
  'LEFT JOIN ovdlocal dlo ON dlo.Codigo=igc.Local   ',
  'LEFT JOIN ovdreferencia ref ON ref.Codigo=igc.SeqGrupo  ',
  'LEFT JOIN ovcynsmedcad ymc ON ymc.Codigo=igc.OVcYnsMed ',
  'LEFT JOIN ovcynschkcad ycc ON ycc.Codigo=igc.OVcYnsChk ',
  'LEFT JOIN OVgIspStaCad isc ON isc.Codigo=igc.ZtatusIsp ',
  'LEFT JOIN OVcYnsARQCad yac ON yac.Codigo=igc.OVcYnsARQ ',
  'WHERE igc.Codigo > 0 ',
*)
  'SELECT igc.*, ymc.Nome NO_OVcYnsMed, ycc.Nome NO_OVcYnsChk, ',
  '/*dlo.Nome NO_Local,*/ ref.Nome NO_Referencia  ',
  '/*isc.Nome NO_ZtatusIsp,*/ /*yac.Nome NO_OVcYnsARQ*/',
  'FROM ovgispprfcab igc ',
  '/*LEFT JOIN ovdlocal dlo ON dlo.Codigo=igc.Local,*/   ',
  '/*',
  'IF(igc.DtHrAbert  <= "1899-12-30", "",   ',
  '  DATE_FORMAT(igc.DtHrAbert, "%d/%m/%Y %H:%i:%s")) DtHrAbert_TXT,   ',
  'IF(igc.DtHrFecha  <= "1899-12-30", "",   ',
  '  DATE_FORMAT(igc.DtHrFecha, "%d/%m/%Y %H:%i:%s")) DtHrFecha_TXT, ',
  'IF(Ativo=0, "Sim", "N�o") NO_Ativo ',
  '*/',
  'LEFT JOIN ovdreferencia ref ON ref.Codigo=igc.SeqGrupo  ',
  'LEFT JOIN ovcynsmedcad ymc ON ymc.Codigo=igc.OVcYnsMed ',
  'LEFT JOIN ovcynschkcad ycc ON ycc.Codigo=igc.OVcYnsChk ',
  '/*LEFT JOIN OVgIspStaCad isc ON isc.Codigo=igc.ZtatusIsp */',
  'LEFT JOIN OVcYnsARQCad yac ON yac.Codigo=igc.OVcYnsARQ ',
  'WHERE igc.Codigo > 0 ',
  SQL_Artigo,
  SQL_Ativo,
  EmptyStr]);
(*
object QrOVgIspPrfCabNO_Referencia: TWideStringField
  FieldName = 'NO_Referencia'
  Size = 100
end
*)
end;

procedure TFmOVgLocIPC.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmOVgLocIPC.DBGrid1DblClick(Sender: TObject);
begin
  if (QrOVgIspPrfCab.State <> dsInactive) and (QrOVgIspPrfCab.RecordCount > 0) then
  begin
    FCodigo := QrOVgIspPrfCabCodigo.Value;
    Close;
  end;

end;

procedure TFmOVgLocIPC.EdArtigoChange(Sender: TObject);
begin
  if not EdReferencia.Focused then
    DmOVS.PesquisaPorCodigo(EdArtigo.ValueVariant, EdReferencia);
end;

procedure TFmOVgLocIPC.EdArtigoKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key=VK_F3 then
    CuringaLoc.Pesquisa('Codigo', 'Nome', 'Referencia', Dmod.MyDB,
    ''(*Extra*), EdArtigo, CBArtigo, dmktfInteger);
end;

procedure TFmOVgLocIPC.EdReferenciaChange(Sender: TObject);
begin
  if EdReferencia.Focused then
    DmOVS.PesquisaPorReferencia(False, EdReferencia, EdArtigo, CBArtigo);
end;

procedure TFmOVgLocIPC.EdReferenciaExit(Sender: TObject);
begin
  DmOVS.PesquisaPorReferencia(True, EdReferencia, EdArtigo, CBArtigo);
end;

procedure TFmOVgLocIPC.EdReferenciaKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key=VK_F3 then
    CuringaLoc.Pesquisa('Codigo', 'Nome', 'Referencia', Dmod.MyDB,
    ''(*Extra*), EdArtigo, CBArtigo, dmktfInteger)
end;

procedure TFmOVgLocIPC.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmOVgLocIPC.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  FCodigo := 0;
  UnDmkDAC_PF.AbreQuery(QrOVdReferencia, Dmod.MyDB);
end;

procedure TFmOVgLocIPC.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

end.
