object FmOVcYnsExgCad: TFmOVcYnsExgCad
  Left = 368
  Top = 194
  Caption = 'OVS-EXGCS-003 :: Cadastros de Tabelas de Exa'#231#245'es'
  ClientHeight = 592
  ClientWidth = 1008
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnEdita: TPanel
    Left = 0
    Top = 105
    Width = 1008
    Height = 487
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    Visible = False
    object GBEdita: TGroupBox
      Left = 0
      Top = 0
      Width = 1008
      Height = 185
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 0
      object Label7: TLabel
        Left = 16
        Top = 16
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label9: TLabel
        Left = 76
        Top = 16
        Width = 269
        Height = 13
        Caption = 'Descri'#231#227'o ([F4] copia nome do artigo referrencial abaixo):'
        Color = clBtnFace
        ParentColor = False
      end
      object Label3: TLabel
        Left = 16
        Top = 56
        Width = 30
        Height = 13
        Caption = 'Artigo:'
      end
      object Label4: TLabel
        Left = 16
        Top = 96
        Width = 87
        Height = 13
        Caption = 'Cliente referencial:'
      end
      object SbArtigRef: TSpeedButton
        Left = 736
        Top = 71
        Width = 23
        Height = 23
        Caption = '...'
        OnClick = SbArtigRefClick
      end
      object SbClientRef: TSpeedButton
        Left = 736
        Top = 111
        Width = 23
        Height = 23
        Caption = '...'
        OnClick = SbClientRefClick
      end
      object Label8: TLabel
        Left = 680
        Top = 16
        Width = 75
        Height = 13
        Caption = 'Pontos (cr'#237'tico):'
        Color = clBtnFace
        ParentColor = False
      end
      object Label12: TLabel
        Left = 596
        Top = 16
        Width = 79
        Height = 13
        Caption = 'Pontos (resalva):'
        Color = clBtnFace
        ParentColor = False
      end
      object EdCodigo: TdmkEdit
        Left = 16
        Top = 32
        Width = 56
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Codigo'
        UpdCampo = 'Codigo'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnKeyDown = EdCodigoKeyDown
      end
      object EdNome: TdmkEdit
        Left = 76
        Top = 32
        Width = 517
        Height = 21
        MaxLength = 60
        TabOrder = 1
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Nome'
        UpdCampo = 'Nome'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
        OnKeyDown = EdNomeKeyDown
      end
      object EdArtigRef: TdmkEditCB
        Left = 16
        Top = 72
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 4
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'ArtigRef'
        UpdCampo = 'ArtigRef'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBArtigRef
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBArtigRef: TdmkDBLookupComboBox
        Left = 74
        Top = 72
        Width = 659
        Height = 21
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsOVdReferencia
        TabOrder = 5
        dmkEditCB = EdArtigRef
        QryCampo = 'ArtigRef'
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdClientRef: TdmkEditCB
        Left = 16
        Top = 112
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 6
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'ClientRef'
        UpdCampo = 'ClientRef'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBClientRef
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBClientRef: TdmkDBLookupComboBox
        Left = 74
        Top = 112
        Width = 659
        Height = 21
        KeyField = 'Codigo'
        ListField = 'NO_ENT'
        ListSource = DsEntidades
        TabOrder = 7
        dmkEditCB = EdClientRef
        QryCampo = 'ClientRef'
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdPontosCrit: TdmkEdit
        Left = 680
        Top = 32
        Width = 80
        Height = 21
        Alignment = taRightJustify
        TabOrder = 3
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'PontosCrit'
        UpdCampo = 'PontosCrit'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdPontosRsv: TdmkEdit
        Left = 596
        Top = 32
        Width = 80
        Height = 21
        Alignment = taRightJustify
        TabOrder = 2
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'PontosRsv'
        UpdCampo = 'PontosRsv'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
    end
    object GBConfirma: TGroupBox
      Left = 0
      Top = 424
      Width = 1008
      Height = 63
      Align = alBottom
      TabOrder = 1
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 12
        Top = 17
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object Panel1: TPanel
        Left = 868
        Top = 15
        Width = 138
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 7
          Top = 2
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
    end
  end
  object PnDados: TPanel
    Left = 0
    Top = 105
    Width = 1008
    Height = 487
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    object GBDados: TGroupBox
      Left = 0
      Top = 0
      Width = 1008
      Height = 141
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 1
      object Label1: TLabel
        Left = 16
        Top = 16
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
        FocusControl = DBEdCodigo
      end
      object Label2: TLabel
        Left = 76
        Top = 16
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
        FocusControl = DBEdNome
      end
      object Label5: TLabel
        Left = 16
        Top = 56
        Width = 82
        Height = 13
        Caption = 'Artigo referencial:'
      end
      object Label6: TLabel
        Left = 16
        Top = 96
        Width = 87
        Height = 13
        Caption = 'Cliente referencial:'
      end
      object Label10: TLabel
        Left = 680
        Top = 16
        Width = 79
        Height = 13
        Caption = 'Pontos (cr'#237'tico)*:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label11: TLabel
        Left = 592
        Top = 16
        Width = 83
        Height = 13
        Caption = 'Pontos (resalva)*:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label13: TLabel
        Left = 776
        Top = 36
        Width = 120
        Height = 13
        Caption = '* V'#225'lido para mensur'#225'vel.'
      end
      object DBEdCodigo: TdmkDBEdit
        Left = 16
        Top = 32
        Width = 56
        Height = 21
        TabStop = False
        DataField = 'Codigo'
        DataSource = DsOVcYnsExgCad
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 0
        UpdType = utYes
        Alignment = taRightJustify
      end
      object DBEdNome: TdmkDBEdit
        Left = 76
        Top = 32
        Width = 513
        Height = 21
        Color = clWhite
        DataField = 'Nome'
        DataSource = DsOVcYnsExgCad
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        UpdType = utYes
        Alignment = taLeftJustify
      end
      object DBEdit1: TDBEdit
        Left = 16
        Top = 72
        Width = 56
        Height = 21
        DataField = 'ArtigRef'
        DataSource = DsOVcYnsExgCad
        TabOrder = 2
      end
      object DBEdit2: TDBEdit
        Left = 76
        Top = 72
        Width = 689
        Height = 21
        DataField = 'NO_ArtigRef'
        DataSource = DsOVcYnsExgCad
        TabOrder = 3
      end
      object DBEdit3: TDBEdit
        Left = 16
        Top = 112
        Width = 56
        Height = 21
        DataField = 'ClientRef'
        DataSource = DsOVcYnsExgCad
        TabOrder = 4
      end
      object DBEdit4: TDBEdit
        Left = 76
        Top = 112
        Width = 689
        Height = 21
        DataField = 'NO_ClientRef'
        DataSource = DsOVcYnsExgCad
        TabOrder = 5
      end
      object DBEdit5: TDBEdit
        Left = 680
        Top = 32
        Width = 85
        Height = 21
        DataField = 'PontosCrit'
        DataSource = DsOVcYnsExgCad
        TabOrder = 6
      end
      object DBEdit6: TDBEdit
        Left = 592
        Top = 32
        Width = 85
        Height = 21
        DataField = 'PontosRsv'
        DataSource = DsOVcYnsExgCad
        TabOrder = 7
      end
    end
    object GBCntrl: TGroupBox
      Left = 0
      Top = 423
      Width = 1008
      Height = 64
      Align = alBottom
      TabOrder = 0
      object Panel5: TPanel
        Left = 2
        Top = 15
        Width = 172
        Height = 47
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 174
        Top = 15
        Width = 154
        Height = 47
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel3: TPanel
        Left = 328
        Top = 15
        Width = 678
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object Panel2: TPanel
          Left = 545
          Top = 0
          Width = 133
          Height = 47
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 0
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
        object BtTab: TBitBtn
          Tag = 1000384
          Left = 4
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Tabela'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtTabClick
        end
        object BtTop: TBitBtn
          Tag = 1000494
          Left = 128
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = 'T'#243'&pico'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = BtTopClick
        end
      end
    end
    object PnGrids: TPanel
      Left = 0
      Top = 141
      Width = 1008
      Height = 112
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 2
      object DBGExgTop: TDBGrid
        Left = 0
        Top = 0
        Width = 1008
        Height = 112
        Align = alClient
        DataSource = DsOVcYnsExgTop
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'Controle'
            Title.Caption = 'ID'
            Width = 56
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Topyko'
            Title.Caption = 'T'#243'pico'
            Width = 56
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NO_TOPYKO'
            Title.Caption = 'Descri'#231#227'o do T'#243'pico'
            Width = 335
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NO_Mensuravl'
            Title.Caption = 'Medir?'
            Width = 37
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'MedidCer'
            Title.Caption = 'Medida certa'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'MedidMin'
            Title.Caption = 'Medida m'#237'n.'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'MedidMax'
            Title.Caption = 'Medida m'#225'x.'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NO_TolerBasCalc'
            Title.Caption = 'Bas.Calc.Toler.'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'TolerSglUnMdid'
            Title.Caption = 'Unidade'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NO_TlrRndUsaMin'
            Title.Caption = 'M'#237'nimo?'
            Width = 46
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'TlrRndPrcMin'
            Title.Caption = 'Marg m'#237'n. med/ %:'
            Width = 71
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NO_TlrRndUsaMax'
            Title.Caption = 'M'#225'ximo?'
            Width = 48
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'TlrRndPrcMax'
            Title.Caption = 'Marg m'#225'x. med/ %:'
            Visible = True
          end>
      end
    end
    object DBGMixOpc: TDBGrid
      Left = 420
      Top = 253
      Width = 588
      Height = 170
      Align = alRight
      DataSource = DsOVcYnsMixOpc
      TabOrder = 3
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'Controle'
          Width = 48
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Nome'
          Width = 338
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_Magnitude'
          Title.Caption = 'Magnitude'
          Width = 100
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'PontNeg'
          Title.Caption = 'Pontos neg.'
          Visible = True
        end>
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 12
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 216
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 216
      Top = 0
      Width = 744
      Height = 52
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 413
        Height = 32
        Caption = 'Cadastros de Tabelas de Exa'#231#245'es'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 413
        Height = 32
        Caption = 'Cadastros de Tabelas de Exa'#231#245'es'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 413
        Height = 32
        Caption = 'Cadastros de Tabelas de Exa'#231#245'es'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 52
    Width = 1008
    Height = 53
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 36
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object PB1: TProgressBar
        Left = 0
        Top = 19
        Width = 1004
        Height = 17
        Align = alBottom
        TabOrder = 0
      end
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = BtTab
    Left = 120
    Top = 64
  end
  object QrOVcYnsExgCad: TMySQLQuery
    Database = DModG.MyCompressDB
    BeforeOpen = QrOVcYnsExgCadBeforeOpen
    AfterOpen = QrOVcYnsExgCadAfterOpen
    BeforeClose = QrOVcYnsExgCadBeforeClose
    AfterScroll = QrOVcYnsExgCadAfterScroll
    SQL.Strings = (
      'SELECT IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_ClientRef, '
      'ycc.Nome NO_ArtigRef, ycc.* '
      'FROM ovcynsexgcad ycc'
      'LEFT JOIN ovdreferencia art ON art.Codigo=ycc.ArtigRef '
      'LEFT JOIN entidades ent ON ent.Codigo=ycc.ClientRef '
      '')
    Left = 92
    Top = 233
    object QrOVcYnsExgCadNO_ClientRef: TWideStringField
      FieldName = 'NO_ClientRef'
      Size = 100
    end
    object QrOVcYnsExgCadNO_ArtigRef: TWideStringField
      FieldName = 'NO_ArtigRef'
      Size = 60
    end
    object QrOVcYnsExgCadCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrOVcYnsExgCadNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
    object QrOVcYnsExgCadArtigRef: TIntegerField
      FieldName = 'ArtigRef'
      Required = True
    end
    object QrOVcYnsExgCadClientRef: TIntegerField
      FieldName = 'ClientRef'
      Required = True
    end
    object QrOVcYnsExgCadPontosCrit: TIntegerField
      FieldName = 'PontosCrit'
      Required = True
    end
    object QrOVcYnsExgCadPontosRsv: TIntegerField
      FieldName = 'PontosRsv'
      Required = True
    end
    object QrOVcYnsExgCadLk: TIntegerField
      FieldName = 'Lk'
      Required = True
    end
    object QrOVcYnsExgCadDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrOVcYnsExgCadDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrOVcYnsExgCadUserCad: TIntegerField
      FieldName = 'UserCad'
      Required = True
    end
    object QrOVcYnsExgCadUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Required = True
    end
    object QrOVcYnsExgCadAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Required = True
    end
    object QrOVcYnsExgCadAWServerID: TIntegerField
      FieldName = 'AWServerID'
      Required = True
    end
    object QrOVcYnsExgCadAWStatSinc: TSmallintField
      FieldName = 'AWStatSinc'
      Required = True
    end
    object QrOVcYnsExgCadAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
  end
  object DsOVcYnsExgCad: TDataSource
    DataSet = QrOVcYnsExgCad
    Left = 92
    Top = 281
  end
  object QrOVcYnsExgTop: TMySQLQuery
    Database = DModG.MyCompressDB
    BeforeClose = QrOVcYnsExgTopBeforeClose
    AfterScroll = QrOVcYnsExgTopAfterScroll
    SQL.Strings = (
      
        'SELECT IF(ymt.Mensuravl=0,"", ELT(ymt.TolerBasCalc+1, "Medida", ' +
        '"Percentual")) NO_TolerBasCalc, '
      'IF(ymt.Mensuravl=1, "SIM", "N'#195'O") NO_Mensuravl,'
      'IF(ymt.TlrRndUsaMin=1, "SIM", "N'#195'O") NO_TlrRndUsaMin,'
      'IF(ymt.TlrRndUsaMax=1, "SIM", "N'#195'O") NO_TlrRndUsaMax,'
      'ygt.Nome NO_TOPYKO, ymt.* '
      'FROM ovcynsexgtop ymt '
      'LEFT JOIN ovcynsmixtop ygt ON ygt.Codigo=ymt.Topyko '
      'WHERE ymt.Codigo>0')
    Left = 188
    Top = 233
    object QrOVcYnsExgTopNO_TolerBasCalc: TWideStringField
      FieldName = 'NO_TolerBasCalc'
      Size = 10
    end
    object QrOVcYnsExgTopNO_TOPYKO: TWideStringField
      FieldName = 'NO_TOPYKO'
      Size = 60
    end
    object QrOVcYnsExgTopNO_Mensuravl: TWideStringField
      FieldName = 'NO_Mensuravl'
      Required = True
      Size = 3
    end
    object QrOVcYnsExgTopNO_TlrRndUsaMin: TWideStringField
      FieldName = 'NO_TlrRndUsaMin'
      Required = True
      Size = 3
    end
    object QrOVcYnsExgTopNO_TlrRndUsaMax: TWideStringField
      FieldName = 'NO_TlrRndUsaMax'
      Required = True
      Size = 3
    end
    object QrOVcYnsExgTopCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrOVcYnsExgTopControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrOVcYnsExgTopTopyko: TIntegerField
      FieldName = 'Topyko'
      Required = True
    end
    object QrOVcYnsExgTopTolerBasCalc: TSmallintField
      FieldName = 'TolerBasCalc'
      Required = True
    end
    object QrOVcYnsExgTopTolerSglUnMdid: TWideStringField
      FieldName = 'TolerSglUnMdid'
      Size = 15
    end
    object QrOVcYnsExgTopTlrRndUsaMin: TSmallintField
      FieldName = 'TlrRndUsaMin'
      Required = True
    end
    object QrOVcYnsExgTopTlrRndPrcMin: TFloatField
      FieldName = 'TlrRndPrcMin'
      Required = True
      DisplayFormat = '#,###,##0.000;-#,###,##0.000; '
    end
    object QrOVcYnsExgTopTlrRndUsaMax: TSmallintField
      FieldName = 'TlrRndUsaMax'
      Required = True
    end
    object QrOVcYnsExgTopTlrRndPrcMax: TFloatField
      FieldName = 'TlrRndPrcMax'
      Required = True
      DisplayFormat = '#,###,##0.000;-#,###,##0.000; '
    end
    object QrOVcYnsExgTopLk: TIntegerField
      FieldName = 'Lk'
      Required = True
    end
    object QrOVcYnsExgTopDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrOVcYnsExgTopDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrOVcYnsExgTopUserCad: TIntegerField
      FieldName = 'UserCad'
      Required = True
    end
    object QrOVcYnsExgTopUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Required = True
    end
    object QrOVcYnsExgTopAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Required = True
    end
    object QrOVcYnsExgTopAWServerID: TIntegerField
      FieldName = 'AWServerID'
      Required = True
    end
    object QrOVcYnsExgTopAWStatSinc: TSmallintField
      FieldName = 'AWStatSinc'
      Required = True
    end
    object QrOVcYnsExgTopAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
    object QrOVcYnsExgTopMedidCer: TFloatField
      FieldName = 'MedidCer'
      Required = True
      DisplayFormat = '#,###,##0.000;-#,###,##0.000; '
    end
    object QrOVcYnsExgTopMedidMin: TFloatField
      FieldName = 'MedidMin'
      Required = True
      DisplayFormat = '#,###,##0.000;-#,###,##0.000; '
    end
    object QrOVcYnsExgTopMedidMax: TFloatField
      FieldName = 'MedidMax'
      Required = True
      DisplayFormat = '#,###,##0.000;-#,###,##0.000; '
    end
    object QrOVcYnsExgTopMensuravl: TSmallintField
      FieldName = 'Mensuravl'
    end
  end
  object DsOVcYnsExgTop: TDataSource
    DataSet = QrOVcYnsExgTop
    Left = 188
    Top = 281
  end
  object PMTop: TPopupMenu
    OnPopup = PMTopPopup
    Left = 500
    Top = 404
    object ItsInclui1: TMenuItem
      Caption = '&Adiciona'
      Enabled = False
      OnClick = ItsInclui1Click
    end
    object ItsAltera1: TMenuItem
      Caption = '&Edita'
      Enabled = False
      OnClick = ItsAltera1Click
    end
    object ItsExclui1: TMenuItem
      Caption = '&Remove'
      Enabled = False
      OnClick = ItsExclui1Click
    end
  end
  object PMTab: TPopupMenu
    OnPopup = PMTabPopup
    Left = 300
    Top = 372
    object CabInclui1: TMenuItem
      Caption = '&Inclui'
      OnClick = CabInclui1Click
    end
    object CabAltera1: TMenuItem
      Caption = '&Altera'
      Enabled = False
      OnClick = CabAltera1Click
    end
    object CabExclui1: TMenuItem
      Caption = '&Exclui'
      Enabled = False
      OnClick = CabExclui1Click
    end
  end
  object QrEntidades: TMySQLQuery
    Database = DModG.MyCompressDB
    SQL.Strings = (
      'SELECT Codigo, IF(Tipo=0, RazaoSocial, Nome) NO_ENT'
      'FROM entidades'
      'ORDER BY NO_ENT')
    Left = 932
    Top = 121
    object QrEntidadesCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrEntidadesNO_ENT: TWideStringField
      FieldName = 'NO_ENT'
      Size = 100
    end
  end
  object DsEntidades: TDataSource
    DataSet = QrEntidades
    Left = 932
    Top = 169
  end
  object QrOVdReferencia: TMySQLQuery
    Database = DModG.MyCompressDB
    SQL.Strings = (
      'SELECT Codigo, Nome '
      'FROM ovdreferencia'
      'ORDER BY Nome')
    Left = 848
    Top = 129
    object QrOVdReferenciaCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrOVdReferenciaNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
  end
  object DsOVdReferencia: TDataSource
    DataSet = QrOVdReferencia
    Left = 848
    Top = 177
  end
  object QrGT: TMySQLQuery
    Database = DModG.MyCompressDB
    Left = 468
    Top = 277
    object QrGTCodGrade: TIntegerField
      FieldName = 'CodGrade'
    end
    object QrGTCodTam: TWideStringField
      FieldName = 'CodTam'
      Size = 30
    end
  end
  object QrTop: TMySQLQuery
    Database = DModG.MyCompressDB
    SQL.Strings = (
      'SELECT Codigo, Controle'
      'FROM ovcynsmedtop ymt'
      'WHERE ygt.Codigo=:P0')
    Left = 468
    Top = 325
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrTopCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrTopControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
  end
  object DqAux: TMySQLDirectQuery
    Database = DModG.MyCompressDB
    Left = 564
    Top = 240
  end
  object QrOVcYnsMixOpc: TMySQLQuery
    Database = Dmod.ZZDB
    SQL.Strings = (
      'SELECT mag.Nome NO_Magnitude, opc.* '
      'FROM ovcynsmixopc opc'
      'LEFT JOIN ovcynsqstmag mag ON  mag.Codigo=opc.Magnitude'
      'WHERE opc.Codigo=0')
    Left = 364
    Top = 261
    object QrOVcYnsMixOpcNO_Magnitude: TWideStringField
      FieldName = 'NO_Magnitude'
      Size = 60
    end
    object QrOVcYnsMixOpcCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrOVcYnsMixOpcControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrOVcYnsMixOpcNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
    object QrOVcYnsMixOpcMagnitude: TIntegerField
      FieldName = 'Magnitude'
      Required = True
    end
    object QrOVcYnsMixOpcPontNeg: TIntegerField
      FieldName = 'PontNeg'
      Required = True
    end
    object QrOVcYnsMixOpcLk: TIntegerField
      FieldName = 'Lk'
      Required = True
    end
    object QrOVcYnsMixOpcDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrOVcYnsMixOpcDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrOVcYnsMixOpcUserCad: TIntegerField
      FieldName = 'UserCad'
      Required = True
    end
    object QrOVcYnsMixOpcUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Required = True
    end
    object QrOVcYnsMixOpcAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Required = True
    end
    object QrOVcYnsMixOpcAWServerID: TIntegerField
      FieldName = 'AWServerID'
      Required = True
    end
    object QrOVcYnsMixOpcAWStatSinc: TSmallintField
      FieldName = 'AWStatSinc'
      Required = True
    end
    object QrOVcYnsMixOpcAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
  end
  object DsOVcYnsMixOpc: TDataSource
    DataSet = QrOVcYnsMixOpc
    Left = 364
    Top = 309
  end
end
