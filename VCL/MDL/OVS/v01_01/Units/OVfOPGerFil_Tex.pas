unit OVfOPGerFil_Tex;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkPermissoes,
  mySQLDbTables, dmkDBGridZTO, Variants;

type
  TFmOVfOPGerFil_Tex = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtConfigura: TBitBtn;
    LaTitulo1C: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    PnNrReduzidoOP: TPanel;
    DBGNrReduzidoOP: TdmkDBGridZTO;
    Panel12: TPanel;
    Label16: TLabel;
    SpeedButton8: TSpeedButton;
    dmkEdit3: TdmkEdit;
    QrOVfOrdemProducao: TMySQLQuery;
    QrOVfOrdemProducaoNrReduzidoOP: TIntegerField;
    QrOVfOrdemProducaoEmpresa: TIntegerField;
    QrOVfOrdemProducaoCiclo: TIntegerField;
    QrOVfOrdemProducaoNrOP: TIntegerField;
    QrOVfOrdemProducaoLocal: TIntegerField;
    QrOVfOrdemProducaoProduto: TIntegerField;
    QrOVfOrdemProducaoSeqGrupo: TIntegerField;
    QrOVfOrdemProducaoTipoOP: TIntegerField;
    QrOVfOrdemProducaoPrioridade: TIntegerField;
    QrOVfOrdemProducaoNrSituacaoOP: TIntegerField;
    QrOVfOrdemProducaoDtInclusao: TDateField;
    QrOVfOrdemProducaoDtPrevisao: TDateField;
    QrOVfOrdemProducaoTipoLocalizacao: TIntegerField;
    QrOVfOrdemProducaoDtEntrada: TDateField;
    QrOVfOrdemProducaoQtReal: TFloatField;
    QrOVfOrdemProducaoQtLocal: TFloatField;
    QrOVfOrdemProducaoCodCategoria: TIntegerField;
    QrOVfOrdemProducaoNrLote: TIntegerField;
    QrOVfOrdemProducaoNrTipoProducaoOP: TWideStringField;
    QrOVfOrdemProducaoDtPrevRet: TDateField;
    QrOVfOrdemProducaoCodPessoa: TIntegerField;
    QrOVfOrdemProducaoReInsrt: TIntegerField;
    QrOVfOrdemProducaoDestino: TWideStringField;
    QrOVfOrdemProducaoLk: TIntegerField;
    QrOVfOrdemProducaoDataCad: TDateField;
    QrOVfOrdemProducaoDataAlt: TDateField;
    QrOVfOrdemProducaoUserCad: TIntegerField;
    QrOVfOrdemProducaoUserAlt: TIntegerField;
    QrOVfOrdemProducaoAlterWeb: TSmallintField;
    QrOVfOrdemProducaoAWServerID: TIntegerField;
    QrOVfOrdemProducaoAWStatSinc: TSmallintField;
    QrOVfOrdemProducaoAtivo: TSmallintField;
    QrOVfOrdemProducaoLastInsrt: TDateTimeField;
    QrOVfOrdemProducaoNO_Local: TWideStringField;
    QrOVfOrdemProducaoDLO_Externo: TWideStringField;
    QrOVfOrdemProducaoNO_CodCategoria: TWideStringField;
    QrOVfOrdemProducaoNO_Referencia: TWideStringField;
    QrOVfOrdemProducaoCodGrade: TIntegerField;
    QrOVfOrdemProducaoCodTam: TWideStringField;
    QrOVfOrdemProducaoNO_Lote: TWideStringField;
    QrOVfOrdemProducaoNO_NrSituacaOP: TWideStringField;
    QrOVfOrdemProducaoNO_TipoLocalizacao: TWideStringField;
    QrOVfOrdemProducaoNO_TipoOP: TWideStringField;
    QrOVfOrdemProducaoNO_NrTipoProducaoOP: TWideStringField;
    DsOVfOrdemProducao: TDataSource;
    DGDados: TdmkDBGridZTO;
    QrOVgIspLasSta: TMySQLQuery;
    DsOVgIspLasSta: TDataSource;
    QrOVgIspLasStaLocal: TIntegerField;
    QrOVgIspLasStaNrOP: TIntegerField;
    QrOVgIspLasStaSeqGrupo: TIntegerField;
    QrOVgIspLasStaNrReduzidoOP: TIntegerField;
    QrOVgIspLasStaDataHora: TDateTimeField;
    QrOVgIspLasStaNO_Local: TWideStringField;
    QrOVgIspLasStaNO_Referencia: TWideStringField;
    Splitter1: TSplitter;
    BtDesobriga: TBitBtn;
    BtOVgItxPrfCad: TBitBtn;
    QrOVgIspLasStaReferencia: TWideStringField;
    BtSoCriaPerfil: TBitBtn;
    Panel5: TPanel;
    RGSeccao: TRadioGroup;
    BtExclui: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure QrNovasReduzOPs_AfterScroll(DataSet: TDataSet);
    procedure QrOVgIspLasStaAfterScroll(DataSet: TDataSet);
    procedure QrOVgIspLasStaAfterOpen(DataSet: TDataSet);
    procedure BtDesobrigaClick(Sender: TObject);
    procedure BtConfiguraClick(Sender: TObject);
    procedure BtOVgItxPrfCadClick(Sender: TObject);
    procedure BtSoCriaPerfilClick(Sender: TObject);
    procedure RGSeccaoClick(Sender: TObject);
    procedure BtExcluiClick(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenQrOVfOrdemProducao();
  public
    { Public declarations }
    FCriaGerenciamentoAuomaticamente: Boolean;
    procedure ReopenNovasReduzOPs();
  end;

  var
  FmOVfOPGerFil_Tex: TFmOVfOPGerFil_Tex;

implementation

uses UnMyObjects, Module, DmkDAC_PF, UnOVS_PF, UnOVS_Consts, MyDBCheck,
  UnOVS_Jan, UnProjGroup_Jan, UnOVS_LoadCSV, ModuleGeral, UnProjGroup_PF;

{$R *.DFM}

procedure TFmOVfOPGerFil_Tex.BtDesobrigaClick(Sender: TObject);
var
  Motivo: Integer;
  //
  function ObtemMotivo(): Boolean;
  const
    Aviso  = '...';
    Titulo = 'Sele��o de Motido de Desobriga��o';
    Prompt = 'Informe Motivo: [F7 para pesquisar]';
    Campo  = 'Descricao';
  var
    Controle, Codigo: Variant;
    PesqSQL: String;
  begin
    Result := False;
    Motivo := 0;
    PesqSQL := Geral.ATS([
    'SELECT Codigo, Nome ' + Campo,
    'FROM ovgispmotsta ',
    //'WHERE Codigo > 0', Aqui Permite -1  (s� aqui !!!??)
    'ORDER BY ' + Campo,
    '']);
    Codigo :=
      DBCheck.EscolheCodigoUnico(Aviso, Titulo, Prompt, nil, nil, Campo, 0, [
      PesqSQL], Dmod.MyDB, True);
    if Codigo <> Null then
    begin
      Motivo   := Codigo;
      Result   := True;
      //
    end;
  end;
  //
  procedure DesobrigaItemAtual();
  begin
    OVS_PF.AlteraZtatusOVgIspAllSta(CO_OVS_IMPORT_ALHEIO_2048_DESOBRIGADO,
      Motivo, QrOVgIspLasStaLocal.Value, QrOVgIspLasStaNrOP.Value,
      QrOVgIspLasStaSeqGrupo.Value, QrOVgIspLasStaNrReduzidoOP.Value,
      DModG.ObtemAgora());
  end;
var
  I, N: Integer;
  Continua: Boolean;
begin
  Continua := False;
  N := DBGNrReduzidoOP.SelectedRows.Count;
  if QrOVgIspLasSta.RecordCount > 0 then
  begin
    if N > 1 then
      Continua := Geral.MB_Pergunta('Confirma a desobriga��o de inspe��o dos ' +
      Geral.FF0(N) + ' reduzidos de OP selecionados?') = ID_YES
    else
      Continua := Geral.MB_Pergunta(
      'Confirma a desobriga��o de inspe��o do reduzido de OP selecionado?') =
      ID_YES;
  end else
    Geral.MB_Info('Nenhum item foi selecionado!');
  //
  if Continua then
  begin
    if ObtemMotivo() then
    begin
      if not DBCheck.LiberaPelaSenhaBoss() then Exit;
      if N > 0 then
      begin
        for I := 0 to N - 1 do
        begin
          QrOVgIspLasSta.GotoBookmark(DBGNrReduzidoOP.SelectedRows.Items[I]);
          DesobrigaItemAtual();
        end;
      end else
        DesobrigaItemAtual();
      //
      ReopenNovasReduzOPs();
      // erro desenho! (Tokyo?)
      DBGNrReduzidoOP.Invalidate;
    end;
  end;
end;

procedure TFmOVfOPGerFil_Tex.BtExcluiClick(Sender: TObject);
  procedure ExcluiItemAtual();
  begin
    OVS_PF.ExcluiZtatusOVgIspAllSta(CO_OVS_IMPORT_ALHEIO_0512_EXCLUIDO,
      0(*Motivo*), QrOVgIspLasStaLocal.Value, QrOVgIspLasStaNrOP.Value,
      QrOVgIspLasStaSeqGrupo.Value, QrOVgIspLasStaNrReduzidoOP.Value,
      DModG.ObtemAgora());
  end;
var
  I, N: Integer;
  Continua: Boolean;
begin
  Continua := False;
  N := DBGNrReduzidoOP.SelectedRows.Count;
  if QrOVgIspLasSta.RecordCount > 0 then
  begin
    if N > 1 then
      Continua := Geral.MB_Pergunta('Confirma a EXCLUS�O dos ' +
      Geral.FF0(N) + ' reduzidos de OP selecionados?') = ID_YES
    else
      Continua := Geral.MB_Pergunta(
      'Confirma a EXCLUS�O do reduzido de OP selecionado?') =
      ID_YES;
  end else
    Geral.MB_Info('Nenhum item foi selecionado!');
  //
  if Continua then
  begin
    if not DBCheck.LiberaPelaSenhaBoss() then Exit;
    if N > 0 then
    begin
      for I := 0 to N - 1 do
      begin
        QrOVgIspLasSta.GotoBookmark(DBGNrReduzidoOP.SelectedRows.Items[I]);
        ExcluiItemAtual();
      end;
    end else
      ExcluiItemAtual();
    //
    ReopenNovasReduzOPs();
    // erro desenho! (Tokyo?)
    DBGNrReduzidoOP.Invalidate;
  end;
end;

procedure TFmOVfOPGerFil_Tex.BtConfiguraClick(Sender: TObject);
const
  OVcYnsExg = 0;
  PermiFinHow = 0;
(*
  OVcYnsChk = 0;
  LimiteMed = 0;
  LimiteChk = 0;
*)
var
  Codigo: Integer;
begin
  Codigo := 0;
  Codigo := OVS_Jan.MostraFormOVgItxGerCad(stIns, Codigo,
    QrOVgIspLasStaLocal.Value, QrOVgIspLasStaNO_Local.Value,
    QrOVgIspLasStaNrOP.Value, QrOVgIspLasStaSeqGrupo.Value,
    QrOVgIspLasStaNO_Referencia.Value, QrOVgIspLasStaNrReduzidoOP.Value,
    OVcYnsExg, {OVcYnsChk, (*OVcYnsARQ*)1, LimiteMed, LimiteChk,*}
    PermiFinHow, CO_OVS_IMPORT_ALHEIO_5120_APTO_DOWNMOBILE);
  if Codigo > 0 then
  begin
    OVS_Jan.MostraFormOVgItxGerCab(Codigo);
    ReopenNovasReduzOPs()
  end;
end;

procedure TFmOVfOPGerFil_Tex.BtOVgItxPrfCadClick(Sender: TObject);
var
  Codigo, SeqGrupo, OVcYnsExg, (*OVcYnsChk, OVcYnsARQ, LimiteMed, LimiteChk,*)
  PermiFinHow, Ativo: Integer;
  NO_Artigo, Referencia, Nome: String;
begin
  SeqGrupo   := QrOVgIspLasStaSeqGrupo.Value;
  NO_Artigo  := QrOVgIspLasStaNO_Referencia.Value;
  Referencia := QrOVgIspLasStaReferencia.Value;
  //
  if SeqGrupo <> 0 then
  begin
    Codigo        := 0;
    OVcYnsExg     := 0;
(*
    OVcYnsChk     := 0;
    OVcYnsARQ     := 0;
    LimiteMed     := 0;
    LimiteChk     := 0;
*)
    PermiFinHow   := 0;
    Ativo         := 0;
    Nome          := '';
    //
    if OVS_Jan.MostraFormOVgItxPrfCad(stIns, Codigo, SeqGrupo, NO_Artigo,
    Referencia, OVcYnsExg, (*OVcYnsChk, OVcYnsARQ, LimiteMed, LimiteChk,*)
    PermiFinHow, Ativo, Nome)  <> 0 then
    begin
      MyObjects.Informa2(LaAviso1, LaAviso2, True,
        'Configurando itens automaticamente pelo seu perfil');
      if ProjGroup_Jan.MostraFormOVgItxPrfCfg(True(*Automatico*), LaAviso1,
      LaAviso2) then
      begin
        MyObjects.Informa2(LaAviso1, LaAviso2, True,
          'Reabrindo itens aptos ainda n�o configurados');
        ReopenNovasReduzOPs();
      end;
    end;
  end;
  //MyObjects.Informa2(LaAviso1, LaAviso2, False, '');
end;

procedure TFmOVfOPGerFil_Tex.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmOVfOPGerFil_Tex.BtSoCriaPerfilClick(Sender: TObject);
var
  Codigo, SeqGrupo, OVcYnsExg,
  //OVcYnsChk, OVcYnsARQ, LimiteMed, LimiteChk,
  PermiFinHow, Ativo: Integer;
  NO_Artigo, Referencia, Nome: String;
begin
  SeqGrupo   := QrOVgIspLasStaSeqGrupo.Value;
  NO_Artigo  := QrOVgIspLasStaNO_Referencia.Value;
  Referencia := QrOVgIspLasStaReferencia.Value;
  //
  if SeqGrupo <> 0 then
  begin
    Codigo        := 0;
    OVcYnsExg     := 0;
(*
    OVcYnsChk     := 0;
    OVcYnsARQ     := 0;
    LimiteMed     := 0;
    LimiteChk     := 0;
*)
    PermiFinHow   := 0;
    Ativo         := 0;
    Nome          := '';
    //
    if OVS_Jan.MostraFormOVgItxPrfCad(stIns, Codigo, SeqGrupo, NO_Artigo,
    Referencia, OVcYnsExg, (*OVcYnsChk, OVcYnsARQ, LimiteMed, LimiteChk,*)
    PermiFinHow, Ativo, Nome)  <> 0 then
    begin
     //if ProjGroup_Jan.MostraFormOVgItxPrfCfg(True(*Automatico*)) then
       //ReopenNovasReduzOPs();
    end;
  end;
end;

procedure TFmOVfOPGerFil_Tex.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmOVfOPGerFil_Tex.FormCreate(Sender: TObject);
begin
  FCriaGerenciamentoAuomaticamente := False;
  ImgTipo.SQLType := stLok;
end;

procedure TFmOVfOPGerFil_Tex.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmOVfOPGerFil_Tex.QrNovasReduzOPs_AfterScroll(DataSet: TDataSet);
begin
  ReopenQrOVfOrdemProducao();
end;

procedure TFmOVfOPGerFil_Tex.QrOVgIspLasStaAfterOpen(DataSet: TDataSet);
begin
  MyObjects.Informa2(LaAviso1, LaAviso2, False, Geral.FF0(
  QrOVgIspLasSta.RecordCount) + ' novos itens a serem configurarados!');
end;

procedure TFmOVfOPGerFil_Tex.QrOVgIspLasStaAfterScroll(DataSet: TDataSet);
begin
  ReopenQrOVfOrdemProducao();
end;

procedure TFmOVfOPGerFil_Tex.ReopenNovasReduzOPs();
const
  sProcName = 'FmOVfOPGerFil_Tex.ReopenNovasReduzOPs()';
var
  sLocal,
  SQL_OVdClasLocal: String;
begin
  case RGSeccao.ItemIndex of
    // Todos
    0: SQL_OVdClasLocal := '';
    // Tecelagem
    1: SQL_OVdClasLocal := 'AND dcl.CodValr=' + Geral.FF0(Dmod.QrOpcoesAppCdScTecelagem.Value);
    // Tinturaria
    2: SQL_OVdClasLocal := 'AND dcl.CodValr=' + Geral.FF0(Dmod.QrOpcoesAppCdScTinturaria.Value);
    else Geral.MB_Erro('"dcl.CodValr" n�o implementado em ' + sProcName);
  end;
  UnDmkDAC_PF.AbreMySQLQuery0(QrOVgIspLasSta, Dmod.MyDB, [
  'SELECT ils.Local, ils.NrOP, ils.SeqGrupo,  ',
  'ils.NrReduzidoOP, ils.DataHora, ',
  'dlo.Nome NO_Local, ref.Nome NO_Referencia, ref.Referencia ',
  'FROM ovgisplassta ils ',
  'LEFT JOIN ovdlocal dlo ON dlo.Codigo=ils.Local  ',
  'LEFT JOIN ovdclaslocal dcl ON dcl.Codigo=dlo.Codigo ',
  'LEFT JOIN ovdreferencia ref ON ref.Codigo=ils.SeqGrupo ',
  'WHERE ils.IspZtatus=' + Geral.FF0(CO_OVS_IMPORT_ALHEIO_1024_IMPORTADO),
  'AND ils.GrupSgmt=' + Geral.FF0(CO_SGMT_INSP_2048_TEXTIL),
  'AND dcl.CodNome=' +  Geral.FF0(Dmod.QrOpcoesAppLoadCSVCodNomLocal.Value),
  SQL_OVdClasLocal,
  'ORDER BY NrReduzidoOP ',
  '']);
  //Geral.MB_SQL(Self, QrOVgIspLasSta);
  MyObjects.Informa2(LaAviso1, LaAviso2, False,
    Geral.FF0(QrOVgIspLasSta.RecordCount) + ' novos itens a configurar!');
end;

procedure TFmOVfOPGerFil_Tex.ReopenQrOVfOrdemProducao();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrOVfOrdemProducao, Dmod.MyDB, [
  'SELECT fop.*, dlo.Nome NO_Local, dlo.Externo DLO_Externo, ',
  'dco.Nome NO_CodCategoria, ref.Nome NO_Referencia, ',
  'prd.CodGrade, prd.CodTam, lot.Nome NO_Lote,  ',
  'nso.Nome NO_NrSituacaOP, tlo.Nome NO_TipoLocalizacao, ',
  'top.Nome NO_TipoOP, tpo.Nome NO_NrTipoProducaoOP ',
  'FROM ovfordemproducao fop ',
  'LEFT JOIN ovdlocal dlo ON dlo.Codigo=fop.Local ',
  'LEFT JOIN ovdclaslocal dcl ON dcl.Codigo=fop.Local ',
  'LEFT JOIN ovdcodcategoria dco ON dco.Codigo=fop.CodCategoria ',
  'LEFT JOIN ovdproduto prd ON prd.Controle=fop.Produto ',
  'LEFT JOIN ovdreferencia ref ON ref.Codigo=prd.Codigo ',
  'LEFT JOIN ovdlote lot ON lot.Codigo=fop.NrLote ',
  'LEFT JOIN ovdnrsituacaoop nso ON nso.Codigo=fop.NrSituacaoOP ',
  'LEFT JOIN ovdtipolocalizacao tlo ON tlo.Codigo=fop.TipoLocalizacao ',
  'LEFT JOIN ovdtipoop top ON top.Codigo=fop.TipoOP ',
  'LEFT JOIN ovdtipoproducaoop tpo ON tpo.CodTxt=fop.NrTipoProducaoOP ',
  ProjGroup_PF.FiltroDadosReleventesAlheios_Texteis(),
  'AND NrReduzidoOP=' + Geral.FF0(QrOVgIspLasStaNrReduzidoOP.Value),
  'AND Local=' + Geral.FF0(QrOVgIspLasStaLocal.Value),
  'AND NrOP=' + Geral.FF0(QrOVgIspLasStaNrOP.Value),
  'AND SeqGrupo=' + Geral.FF0(QrOVgIspLasStaSeqGrupo.Value),
  '']);
  //
end;

procedure TFmOVfOPGerFil_Tex.RGSeccaoClick(Sender: TObject);
begin
   ReopenNovasReduzOPs();
end;

end.
