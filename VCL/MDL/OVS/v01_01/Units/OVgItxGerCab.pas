unit OVgItxGerCab;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2, dmkGeral,
  ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt, UMySQLModule, Menus,
  UnInternalConsts2, mySQLDbTables, UnMySQLCuringa, dmkPermissoes, dmkEdit,
  dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup, Grids, DBGrids,
  UnDmkProcFunc, UnDmkENums, Vcl.ComCtrls, dmkDBGridZTO;

type
  TFmOVgItxGerCab = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    Label9: TLabel;
    EdNome: TdmkEdit;
    QrOVgItxGerCab: TMySQLQuery;
    DsOVgItxGerCab: TDataSource;
    PMBtl: TPopupMenu;
    ItsInclui1: TMenuItem;
    ItsExclui1: TMenuItem;
    ItsAltera1: TMenuItem;
    PMCab: TPopupMenu;
    CabInclui1: TMenuItem;
    CabAltera1: TMenuItem;
    CabExclui1: TMenuItem;
    BtCab: TBitBtn;
    BtBtl: TBitBtn;
    QrOVgItxGerCabCodigo: TIntegerField;
    QrOVgItxGerCabLocal: TIntegerField;
    QrOVgItxGerCabNrOP: TIntegerField;
    QrOVgItxGerCabSeqGrupo: TIntegerField;
    QrOVgItxGerCabNrReduzidoOP: TIntegerField;
    QrOVgItxGerCabDtHrAbert: TDateTimeField;
    QrOVgItxGerCabDtHrFecha: TDateTimeField;
    QrOVgItxGerCabOVcYnsExg: TIntegerField;
    QrOVgItxGerCabZtatusIsp: TIntegerField;
    QrOVgItxGerCabZtatusDtH: TDateTimeField;
    QrOVgItxGerCabZtatusMot: TIntegerField;
    QrOVgItxGerCabLk: TIntegerField;
    QrOVgItxGerCabDataCad: TDateField;
    QrOVgItxGerCabDataAlt: TDateField;
    QrOVgItxGerCabUserCad: TIntegerField;
    QrOVgItxGerCabUserAlt: TIntegerField;
    QrOVgItxGerCabAlterWeb: TSmallintField;
    QrOVgItxGerCabAWServerID: TIntegerField;
    QrOVgItxGerCabAWStatSinc: TSmallintField;
    QrOVgItxGerCabAtivo: TSmallintField;
    QrOVgItxGerCabNO_Local: TWideStringField;
    QrOVgItxGerCabNO_Referencia: TWideStringField;
    QrOVgItxGerCabNome: TWideStringField;
    QrOVgItxGerCabNO_OVcYnsExg: TWideStringField;
    QrOVgItxGerCabDtHrAbert_TXT: TWideStringField;
    QrOVgItxGerCabDtHrFecha_TXT: TWideStringField;
    LiberarParaInspecao1: TMenuItem;
    QrOVgItxGerCabNO_ZtatusIsp: TWideStringField;
    QrSumZtatusIsp: TMySQLQuery;
    DsSumZtatusIsp: TDataSource;
    QrSumZtatusIspZtatusIsp: TIntegerField;
    QrSumZtatusIspNO_ZTATUS: TWideStringField;
    QrSumZtatusIspITENS: TLargeintField;
    QrOVcYnsExgTop: TMySQLQuery;
    QrOVcYnsExgTopNO_TolerBasCalc: TWideStringField;
    QrOVcYnsExgTopNO_TOPYKO: TWideStringField;
    QrOVcYnsExgTopNO_Mensuravl: TWideStringField;
    QrOVcYnsExgTopNO_TlrRndUsaMin: TWideStringField;
    QrOVcYnsExgTopNO_TlrRndUsaMax: TWideStringField;
    QrOVcYnsExgTopCodigo: TIntegerField;
    QrOVcYnsExgTopControle: TIntegerField;
    QrOVcYnsExgTopTopyko: TIntegerField;
    QrOVcYnsExgTopTolerBasCalc: TSmallintField;
    QrOVcYnsExgTopTolerSglUnMdid: TWideStringField;
    QrOVcYnsExgTopTlrRndUsaMin: TSmallintField;
    QrOVcYnsExgTopTlrRndPrcMin: TFloatField;
    QrOVcYnsExgTopTlrRndUsaMax: TSmallintField;
    QrOVcYnsExgTopTlrRndPrcMax: TFloatField;
    QrOVcYnsExgTopLk: TIntegerField;
    QrOVcYnsExgTopDataCad: TDateField;
    QrOVcYnsExgTopDataAlt: TDateField;
    QrOVcYnsExgTopUserCad: TIntegerField;
    QrOVcYnsExgTopUserAlt: TIntegerField;
    QrOVcYnsExgTopAlterWeb: TSmallintField;
    QrOVcYnsExgTopAWServerID: TIntegerField;
    QrOVcYnsExgTopAWStatSinc: TSmallintField;
    QrOVcYnsExgTopAtivo: TSmallintField;
    QrOVcYnsExgTopMedidCer: TFloatField;
    QrOVcYnsExgTopMedidMin: TFloatField;
    QrOVcYnsExgTopMedidMax: TFloatField;
    QrOVcYnsExgTopMensuravl: TSmallintField;
    DsOVcYnsExgTop: TDataSource;
    QrOVcYnsMixOpc: TMySQLQuery;
    QrOVcYnsMixOpcNO_Magnitude: TWideStringField;
    QrOVcYnsMixOpcCodigo: TIntegerField;
    QrOVcYnsMixOpcControle: TIntegerField;
    QrOVcYnsMixOpcNome: TWideStringField;
    QrOVcYnsMixOpcMagnitude: TIntegerField;
    QrOVcYnsMixOpcPontNeg: TIntegerField;
    QrOVcYnsMixOpcLk: TIntegerField;
    QrOVcYnsMixOpcDataCad: TDateField;
    QrOVcYnsMixOpcDataAlt: TDateField;
    QrOVcYnsMixOpcUserCad: TIntegerField;
    QrOVcYnsMixOpcUserAlt: TIntegerField;
    QrOVcYnsMixOpcAlterWeb: TSmallintField;
    QrOVcYnsMixOpcAWServerID: TIntegerField;
    QrOVcYnsMixOpcAWStatSinc: TSmallintField;
    QrOVcYnsMixOpcAtivo: TSmallintField;
    DsOVcYnsMixOpc: TDataSource;
    QrOVgItxGerCabPermiFinHow: TIntegerField;
    Panel6: TPanel;
    Panel8: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Label17: TLabel;
    Label18: TLabel;
    Label19: TLabel;
    Label13: TLabel;
    Label15: TLabel;
    Label20: TLabel;
    DBEdCodigo: TdmkDBEdit;
    DBEdNome: TdmkDBEdit;
    dmkDBEdit1: TdmkDBEdit;
    dmkDBEdit2: TdmkDBEdit;
    dmkDBEdit5: TdmkDBEdit;
    dmkDBEdit6: TdmkDBEdit;
    dmkDBEdit3: TdmkDBEdit;
    dmkDBEdit4: TdmkDBEdit;
    dmkDBEdit7: TdmkDBEdit;
    dmkDBEdit8: TdmkDBEdit;
    PnDadosOri: TPanel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label8: TLabel;
    Label10: TLabel;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    DBEdit4: TDBEdit;
    DBEdit5: TDBEdit;
    DBEdit6: TDBEdit;
    Panel7: TPanel;
    DBRadioGroup1: TDBRadioGroup;
    PCItens: TPageControl;
    TabSheet1: TTabSheet;
    PnGrids: TPanel;
    DBGMixOpc: TDBGrid;
    PnGrids2: TPanel;
    DBGExgTop: TDBGrid;
    Panel9: TPanel;
    Label11: TLabel;
    DBEdit7: TDBEdit;
    DBEdit9: TDBEdit;
    Splitter1: TSplitter;
    QrOVgItxGerBtl: TMySQLQuery;
    QrOVgItxGerBtlCodigo: TIntegerField;
    QrOVgItxGerBtlControle: TIntegerField;
    QrOVgItxGerBtlOrdem: TIntegerField;
    QrOVgItxGerBtlSeccaoOP: TWideStringField;
    QrOVgItxGerBtlSeccaoMaq: TWideStringField;
    QrOVgItxGerBtlQtReal: TFloatField;
    QrOVgItxGerBtlLk: TIntegerField;
    QrOVgItxGerBtlDataCad: TDateField;
    QrOVgItxGerBtlDataAlt: TDateField;
    QrOVgItxGerBtlUserCad: TIntegerField;
    QrOVgItxGerBtlUserAlt: TIntegerField;
    QrOVgItxGerBtlAlterWeb: TSmallintField;
    QrOVgItxGerBtlAWServerID: TIntegerField;
    QrOVgItxGerBtlAWStatSinc: TSmallintField;
    QrOVgItxGerBtlAtivo: TSmallintField;
    DsOVgItxGerBtl: TDataSource;
    DBGrid1: TDBGrid;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrOVgItxGerCabAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrOVgItxGerCabBeforeOpen(DataSet: TDataSet);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure QrOVgItxGerCabAfterScroll(DataSet: TDataSet);
    procedure CabInclui1Click(Sender: TObject);
    procedure CabAltera1Click(Sender: TObject);
    procedure CabExclui1Click(Sender: TObject);
    procedure BtCabClick(Sender: TObject);
    procedure BtBtlClick(Sender: TObject);
    procedure PMCabPopup(Sender: TObject);
    procedure QrOVgItxGerCabBeforeClose(DataSet: TDataSet);
    procedure LiberarParaInspecao1Click(Sender: TObject);
    procedure QrOVcYnsExgTopAfterScroll(DataSet: TDataSet);
    procedure QrOVcYnsExgTopBeforeClose(DataSet: TDataSet);
    procedure ItsInclui1Click(Sender: TObject);
    procedure ItsAltera1Click(Sender: TObject);
    procedure PMBtlPopup(Sender: TObject);
    procedure ItsExclui1Click(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    //
    function  AlteraStatus(NovoStatus, Motivo: Integer): Boolean;
    procedure ReopenOVcYnsExgTop(Controle: Integer);
    procedure ReopenOVcYnsMixOpc(Controle: Integer);
    procedure ReopenOVgItxGerBtl(Controle: Integer);


  public
    { Public declarations }
    FSeq, FCabIni: Integer;
    FLocIni: Boolean;
    //
    procedure LocCod(Atual, Codigo: Integer);

  end;

var
  FmOVgItxGerCab: TFmOVgItxGerCab;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, MyDBCheck, DmkDAC_PF, UnOVS_Jan, UnOVS_PF,
  UnOVS_Consts, ModuleGeral;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmOVgItxGerCab.LiberarParaInspecao1Click(Sender: TObject);
begin
  if Geral.MB_Pergunta('Confirma a libera��o desta inspe��o para download mobile?') =
  ID_YES then
  begin
    AlteraStatus(CO_OVS_IMPORT_ALHEIO_5120_APTO_DOWNMOBILE, 0);
    LocCod(QrOVgItxGerCabCodigo.Value, QrOVgItxGerCabCodigo.Value);
  end;
end;

procedure TFmOVgItxGerCab.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmOVgItxGerCab.PMBtlPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(ItsInclui1, QrOVgItxGerCab);
  MyObjects.HabilitaMenuItemItsUpd(ItsAltera1, QrOVgItxGerBtl);
  MyObjects.HabilitaMenuItemItsDel(ItsExclui1, QrOVgItxGerBtl);
end;

procedure TFmOVgItxGerCab.PMCabPopup(Sender: TObject);
var
  HI: Boolean;
  ZI: Integer;
begin
  MyObjects.HabilitaMenuItemCabUpd(CabAltera1, QrOVgItxGerCab);
  //
  ZI := QrOVgItxGerCabZtatusIsp.Value;
  HI := (ZI >= CO_OVS_IMPORT_ALHEIO_3972_CONFIGURADO) and
         (ZI <> CO_OVS_IMPORT_ALHEIO_4096_APTO_UPWEBSERVR);
  LiberarParaInspecao1.Enabled := HI;
{
  CO_OVS_IMPORT_ALHEIO_3972_CONFIGURADO = 3072;
  // 4096 - Apto ao Upload (n�o usado >> direto)
  CO_OVS_IMPORT_ALHEIO_4096_APTO_UPWEBSERVR = 4096;
  // 5120 - Apto ao download mobile
  CO_OVS_IMPORT_ALHEIO_5120_APTO_DOWNMOBILE = 5120;
}
end;

procedure TFmOVgItxGerCab.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrOVgItxGerCabCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmOVgItxGerCab.DefParams;
begin
  VAR_GOTOTABELA := 'ovgitxgercab';
  VAR_GOTOMYSQLTABLE := QrOVgItxGerCab;
  VAR_GOTONEG := gotoPiZ;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT igc.*, ymc.Nome NO_OVcYnsExg,');
  VAR_SQLx.Add('dlo.Nome NO_Local, ref.Nome NO_Referencia, ');
  VAR_SQLx.Add('isc.Nome NO_ZtatusIsp, ');
  VAR_SQLx.Add('IF(igc.DtHrAbert  <= "1899-12-30", "",  ');
  VAR_SQLx.Add('  DATE_FORMAT(igc.DtHrAbert, "%d/%m/%Y %H:%i:%s")) DtHrAbert_TXT,  ');
  VAR_SQLx.Add('IF(igc.DtHrFecha  <= "1899-12-30", "",  ');
  VAR_SQLx.Add('  DATE_FORMAT(igc.DtHrFecha, "%d/%m/%Y %H:%i:%s")) DtHrFecha_TXT');
  VAR_SQLx.Add('FROM ovgitxgercab igc');
  VAR_SQLx.Add('LEFT JOIN ovdlocal dlo ON dlo.Codigo=igc.Local  ');
  VAR_SQLx.Add('LEFT JOIN ovdreferencia ref ON ref.Codigo=igc.SeqGrupo ');
  VAR_SQLx.Add('LEFT JOIN ovcynsexgcad ymc ON ymc.Codigo=igc.OVcYnsExg');
  VAR_SQLx.Add('LEFT JOIN OVgIspStaCad isc ON isc.Codigo=igc.ZtatusIsp');
  VAR_SQLx.Add('WHERE igc.Codigo > 0');
  //
  VAR_SQL1.Add('AND igc.Codigo=:P0');
  //
  //VAR_SQL2.Add('AND CodUsu=:P0');
  //
  VAR_SQLa.Add('AND igc.Nome Like :P0');
  //
end;

procedure TFmOVgItxGerCab.CabExclui1Click(Sender: TObject);
begin
  Geral.MB_Info('A��o n�o implementada! Solicite � Dermatek:' + sLineBreak +
  Caption + sLineBreak + TMenuItem(Sender).Name);
end;

procedure TFmOVgItxGerCab.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmOVgItxGerCab.QueryPrincipalAfterOpen;
begin
end;

procedure TFmOVgItxGerCab.ReopenOVcYnsExgTop(Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrOVcYnsExgTop, Dmod.MyDB, [
  'SELECT IF(ymt.Mensuravl=0,"", ELT(ymt.TolerBasCalc+1, "Medida", "Percentual")) NO_TolerBasCalc, ',
  'IF(ymt.Mensuravl=1, "SIM", "N�O") NO_Mensuravl,',
  'IF(ymt.TlrRndUsaMin=1, "SIM", "N�O") NO_TlrRndUsaMin,',
  'IF(ymt.TlrRndUsaMax=1, "SIM", "N�O") NO_TlrRndUsaMax,',
  'ygt.Nome NO_TOPYKO, ymt.* ',
  'FROM ovcynsexgtop ymt ',
  'LEFT JOIN ovcynsmixtop ygt ON ygt.Codigo=ymt.Topyko ',
  'WHERE ymt.Codigo=' + Geral.FF0(QrOVgItxGerCabOVcYnsExg.Value),
  '']);
  //
  QrOVcYnsExgTop.Locate('Controle', Controle, []);
end;

procedure TFmOVgItxGerCab.ReopenOVcYnsMixOpc(Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrOVcYnsMixOpc, Dmod.MyDB, [
  'SELECT mag.Nome NO_Magnitude, opc.*  ',
  'FROM ovcynsmixopc opc ',
  'LEFT JOIN ovcynsqstmag mag ON  mag.Codigo=opc.Magnitude ',
  'WHERE opc.Codigo=' + Geral.FF0(QrOVcYnsExgTopTopyko.Value),
  '']);
  //
  if Controle <> 0 then
    QrOVcYnsMixOpc.Locate('Controle', Controle, []);
end;

procedure TFmOVgItxGerCab.ReopenOVgItxGerBtl(Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrOVgItxGerBtl, Dmod.MyDB, [
  'SELECT * ',
  'FROM ovgitxgerbtl ',
  'WHERE Codigo=' + Geral.FF0(QrOVgItxGerCabCodigo.Value),
  EmptyStr]);
  //
  if Controle <> 0 then
    QrOVgItxGerBtl.Locate('Controle', Controle, []);
end;

procedure TFmOVgItxGerCab.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmOVgItxGerCab.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmOVgItxGerCab.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmOVgItxGerCab.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmOVgItxGerCab.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmOVgItxGerCab.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmOVgItxGerCab.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrOVgItxGerCabCodigo.Value;
  Close;
end;

procedure TFmOVgItxGerCab.CabAltera1Click(Sender: TObject);
begin
  OVS_Jan.MostraFormOVgItxGerCad(stUpd, QrOVgItxGerCabCodigo.Value,
    QrOVgItxGerCabLocal.Value, QrOVgItxGerCabNO_Local.Value,
    QrOVgItxGerCabNrOP.Value, QrOVgItxGerCabSeqGrupo.Value,
    QrOVgItxGerCabNO_Referencia.Value, QrOVgItxGerCabNrReduzidoOP.Value,
    QrOVgItxGerCabOVcYnsExg.Value, (*QrOVgItxGerCabOVcYnsChk.Value,
    QrOVgItxGerCabOVcYnsARQ.Value,
    QrOVgItxGerCabLimiteMed.Value, QrOVgItxGerCabLimiteChk.Value,*)
    QrOVgItxGerCabPermiFinHow.Value, QrOVgItxGerCabZtatusIsp.Value);
  //
  LocCod(QrOVgItxGerCabCodigo.Value, QrOVgItxGerCabCodigo.Value);
end;

procedure TFmOVgItxGerCab.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := EdCodigo.ValueVariant;
  if ImgTipo.SQLType = stIns then
    UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'ovgitxgercab', Codigo);
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'ovgitxgercab', 'Codigo');
end;

procedure TFmOVgItxGerCab.BtBtlClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMBtl, BtBtl);
end;

function  TFmOVgItxGerCab.AlteraStatus(NovoStatus, Motivo: Integer): Boolean;
var
  //Nome, DtHrAbert, DtHrFecha,
  ZtatusDtH: String;
  //OVcYnsExg, OVcYnsChk, LimiteChk, LimiteMed,
  Codigo, Local, NrOP, SeqGrupo, NrReduzidoOP, ZtatusIsp, ZtatusMot: Integer;
  SQLType: TSQLType;
  //
  Agora: TDateTime;
begin
  Result         := False;
  //
  Agora          := DModG.ObtemAgora();
  SQLType        := stUpd;
  Codigo         := QrOVgItxGerCabCodigo.Value;
  Local          := QrOVgItxGerCabLocal.Value;
  NrOP           := QrOVgItxGerCabNrOP.Value;
  SeqGrupo       := QrOVgItxGerCabSeqGrupo.Value;
  NrReduzidoOP   := QrOVgItxGerCabNrReduzidoOP.Value;
  //DtHrAbert      := ;
  //DtHrFecha      := ;
  //OVcYnsExg      := ;
  //OVcYnsChk      := ;
  //LimiteChk      := ;
  //LimiteMed      := ;
  ZtatusIsp      := NovoStatus;
  ZtatusDtH      := Geral.FDT(Agora, 109);
  ZtatusMot      := Motivo;
  //Nome           := ;
  //
  //Codigo := UMyMod.BPGS1I32('ovgitxgercab', 'Codigo', '', '', tsPosNeg?, stInsUpd?, CodAtual?);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'ovgitxgercab', False, [
  'Local', 'NrOP', 'SeqGrupo',
  'NrReduzidoOP', (*'DtHrAbert', 'DtHrFecha',
  'OVcYnsExg', 'OVcYnsChk', 'LimiteChk',
  'LimiteMed',*) 'ZtatusIsp', 'ZtatusDtH',
  'ZtatusMot'(*, 'Nome'*)], [
  'Codigo'], [
  Local, NrOP, SeqGrupo,
  NrReduzidoOP, (*DtHrAbert, DtHrFecha,
  OVcYnsExg, OVcYnsChk, LimiteChk,
  LimiteMed,*) ZtatusIsp, ZtatusDtH,
  ZtatusMot(*, Nome*)], [
  Codigo], True) then
  begin
    if OVS_PF.AlteraZtatusOVgIspAllSta(NovoStatus, ZtatusMot, Local, NrOP,
    SeqGrupo, NrReduzidoOP, Agora) then
      Result := True;
  end;
end;

procedure TFmOVgItxGerCab.BtCabClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMCab, BtCab);
end;

procedure TFmOVgItxGerCab.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  GBEdita.Align   := alClient;
  PnGrids.Align   := alClient;
  PnGrids2.Align  := alClient;
  PCItens.Align   := alClient;
  PCItens.ActivePageIndex := 0;
  CriaOForm;
  FSeq := 0;
end;

procedure TFmOVgItxGerCab.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrOVgItxGerCabCodigo.Value, LaRegistro.Caption);
end;

procedure TFmOVgItxGerCab.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmOVgItxGerCab.SbNovoClick(Sender: TObject);
const
  MultiSelect = False;
  Destroi     = True;
begin
//  LaRegistro.Caption := GOTOy.CodUsu(QrOVgItxGerCabCodigo.Value, LaRegistro.Caption);
(*
  'SELECT igc.ZtatusIsp, ist.Nome NO_ZTATUS,  ',
  'COUNT(igc.Codigo) ITENS ',
  'FROM ovgitxgercab igc ',
  'LEFT JOIN ovgispstacad ist ON ist.Codigo=igc.ZtatusIsp ',
  'WHERE igc.Codigo <> 0 ',
  'GROUP BY igc.ZtatusIsp ',
*)
  UnDmkDAC_PF.AbreQuery(QrSumZtatusIsp, Dmod.MyDB);
  DBCheck.EscolheCodigoUniGrid('Aviso', 'Itens por Status', 'Prompt',
  DsSumZtatusIsp, MultiSelect, Destroi);
end;

procedure TFmOVgItxGerCab.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmOVgItxGerCab.QrOVcYnsExgTopAfterScroll(DataSet: TDataSet);
begin
  ReopenOVcYnsMixOpc(0);
end;

procedure TFmOVgItxGerCab.QrOVcYnsExgTopBeforeClose(DataSet: TDataSet);
begin
  QrOVcYnsMixOpc.Close;
end;

procedure TFmOVgItxGerCab.QrOVgItxGerCabAfterOpen(DataSet: TDataSet);
begin
  //Geral.MB_Info(QrOVgItxGerCab.SQL.Text);
  QueryPrincipalAfterOpen;
end;

procedure TFmOVgItxGerCab.QrOVgItxGerCabAfterScroll(DataSet: TDataSet);
begin
  ReopenOVcYnsExgTop(0);
  ReopenOVgItxGerBtl(0);
end;

procedure TFmOVgItxGerCab.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //
  if FSeq = 1 then
    CabInclui1Click(Self)
  else
  if (not FLocIni) and (FCabIni <> 0)  then
  begin
    LocCod(FCabIni, FCabIni);
    if QrOVgItxGerCabCodigo.Value <> FCabIni then
      Geral.MB_Aviso('Grupo n�o localizado!');
    FLocIni := True;
  end;
end;

procedure TFmOVgItxGerCab.SbQueryClick(Sender: TObject);
var
  Codigo: Integer;
begin
  if OVS_Jan.LocalizaOVgItxGerCab(Codigo) then
    LocCod(Codigo, Codigo);
end;

procedure TFmOVgItxGerCab.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmOVgItxGerCab.ItsAltera1Click(Sender: TObject);
begin
  OVS_Jan.MostraFormOVgItxGerBtl(stUpd, DsOVgItxGerCab, QrOVgItxGerBtl);
end;

procedure TFmOVgItxGerCab.ItsExclui1Click(Sender: TObject);
var
  Codigo, Controle: Integer;
begin
  if UMyMod.ExcluiRegistroInt1('Confirma a exclus�o do item selecionado?',
  'ovgitxgerbtl', 'Controle', QrOVgItxGerBtlControle.Value, Dmod.MyDB) = ID_YES then
  begin
    Codigo   := QrOVgItxGerCabCodigo.Value;
    Controle := GOTOy.LocalizaPriorNextIntQr(QrOVgItxGerBtl,
      QrOVgItxGerBtlControle, QrOVgItxGerBtlControle.Value);
    Dmod.AtualizaQtBtl(Codigo);
    ReopenOVgItxGerBtl(Controle);
  end;
end;

procedure TFmOVgItxGerCab.ItsInclui1Click(Sender: TObject);
begin
  OVS_Jan.MostraFormOVgItxGerBtl(stIns, DsOVgItxGerCab, QrOVgItxGerBtl);
end;

procedure TFmOVgItxGerCab.CabInclui1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrOVgItxGerCab, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'ovgitxgercab');
end;

procedure TFmOVgItxGerCab.QrOVgItxGerCabBeforeClose(
  DataSet: TDataSet);
begin
  QrOVcYnsExgTop.Close;
  QrOVgItxGerBtl.Close;
end;

procedure TFmOVgItxGerCab.QrOVgItxGerCabBeforeOpen(DataSet: TDataSet);
begin
  QrOVgItxGerCabCodigo.DisplayFormat := FFormatFloat;
end;

end.

