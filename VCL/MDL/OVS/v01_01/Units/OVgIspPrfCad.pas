unit OVgIspPrfCad;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkPermissoes,
  dmkDBLookupComboBox, dmkEditCB, mySQLDbTables, dmkRadioGroup, dmkCheckBox;

type
  TFmOVgIspPrfCad = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    GroupBox2: TGroupBox;
    GroupBox3: TGroupBox;
    Panel5: TPanel;
    QrOVcYnsMedCad: TMySQLQuery;
    QrOVcYnsMedCadCodigo: TIntegerField;
    QrOVcYnsMedCadNome: TWideStringField;
    DsOVcYnsMedCad: TDataSource;
    QrOVcYnsChkCad: TMySQLQuery;
    DsOVcYnsChkCad: TDataSource;
    Label7: TLabel;
    EdOVcYnsMed: TdmkEditCB;
    CBOVcYnsMed: TdmkDBLookupComboBox;
    Label8: TLabel;
    EdOVcYnsChk: TdmkEditCB;
    CBOVcYnsChk: TdmkDBLookupComboBox;
    SbOVcYnsChk: TSpeedButton;
    SbOVcYnsMed: TSpeedButton;
    QrOVcYnsChkCadCodigo: TIntegerField;
    QrOVcYnsChkCadNome: TWideStringField;
    GroupBox4: TGroupBox;
    Panel6: TPanel;
    EdLimiteChk: TdmkEdit;
    Label9: TLabel;
    GroupBox5: TGroupBox;
    Panel7: TPanel;
    Label10: TLabel;
    EdLimiteMed: TdmkEdit;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    EdCodigo: TdmkEdit;
    Label14: TLabel;
    EdOVcYnsARQ: TdmkEditCB;
    CBOVcYnsARQ: TdmkDBLookupComboBox;
    SBOVcYnsARQ: TSpeedButton;
    QrOVcYnsARQCad: TMySQLQuery;
    DsOVcYnsARQCad: TDataSource;
    QrOVcYnsARQCadCodigo: TIntegerField;
    QrOVcYnsARQCadNome: TWideStringField;
    RGPermiFinHow: TdmkRadioGroup;
    PnDadosOri: TPanel;
    Label4: TLabel;
    Label5: TLabel;
    EdSeqGrupo: TdmkEdit;
    EdNO_SeqGrupo: TdmkEdit;
    Label1: TLabel;
    EdReferencia: TdmkEdit;
    CkAtivo: TdmkCheckBox;
    Label15: TLabel;
    EdNome: TdmkEdit;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure SbOVcYnsMedClick(Sender: TObject);
    procedure SbOVcYnsChkClick(Sender: TObject);
    procedure SBOVcYnsARQClick(Sender: TObject);
    procedure EdNomeKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
    FCodigo, FSeqGrupo: Integer;
    //
    procedure ReopenOVcYnsMedCad(ArtigRef: Integer);
  end;

  var
  FmOVgIspPrfCad: TFmOVgIspPrfCad;

implementation

uses UnMyObjects, UnOVS_Jan, Module, DmkDAC_PF, UMySQLModule, ModuleGeral,
  UnOVS_Consts, UnOVS_PF;

{$R *.DFM}

procedure TFmOVgIspPrfCad.BtOKClick(Sender: TObject);
var
  Nome: String;
  Codigo, SeqGrupo, OVcYnsMed, OVcYnsChk, LimiteChk, LimiteMed, OVcYnsARQ,
  PermiFinHow, Ativo: Integer;
  SQLType: TSQLType;
  Agora: TDateTime;
begin
  SQLType        := ImgTipo.SQLType;
  Agora          := DModG.ObtemAgora();
  Codigo         := EdCodigo.ValueVariant;
  SeqGrupo       := EdSeqGrupo.ValueVariant;
  OVcYnsMed      := EdOVcYnsMed.ValueVariant;
  OVcYnsChk      := EdOVcYnsChk.ValueVariant;
  LimiteChk      := EdLimiteChk.ValueVariant;
  LimiteMed      := EdLimiteMed.ValueVariant;
  OVcYnsARQ      := EdOVcYnsARQ.ValueVariant;
  PermiFinHow    := RGPermiFinHow.ItemIndex;
  Ativo          := Geral.BoolToInt(CkAtivo.Checked);
  Nome           := EdNome.ValueVariant;
  //
  if MyObjects.FIC(Trim(Nome) = EmptyStr, EdNome,
    '"Informe uma descri��o para o perfil"') then Exit;
  //
  if MyObjects.FIC(OVcYnsMed = 0, EdOVcYnsMed,
    '"Informe a tabela de medidas"') then Exit;
  //
  if MyObjects.FIC(OVcYnsChk = 0, EdOVcYnsChk,
    '"Informe o check list de inconformidades"') then Exit;
  //
  if MyObjects.FIC(OVcYnsARQ = 0, EdOVcYnsARQ,
    '"Plano de Amostragem e Regime de Qualidade"') then Exit;
  //
  if MyObjects.FIC(PermiFinHow < 1, RGPermiFinHow,
    'Informe a permiss�o de finaliza��o da inspe��o"') then Exit;
  //
  Codigo := UMyMod.BPGS1I32('ovgispprfcab', 'Codigo', '', '', tsPos, SQLType, Codigo);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'ovgispprfcab', False, [
  'Nome', 'SeqGrupo',
  'OVcYnsMed', 'OVcYnsChk', 'LimiteChk',
  'LimiteMed', 'OVcYnsARQ', 'PermiFinHow',
  'Ativo'], [
  'Codigo'], [
  Nome, SeqGrupo,
  OVcYnsMed, OVcYnsChk, LimiteChk,
  LimiteMed, OVcYnsARQ, PermiFinHow,
  Ativo], [
  Codigo], True) then
  begin
    FCodigo := Codigo;
    Close;
  end;
end;

procedure TFmOVgIspPrfCad.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmOVgIspPrfCad.EdNomeKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F3 then
    EdNome.Text := EdReferencia.Text;
  if Key = VK_F4 then
    EdNome.Text := EdNO_SeqGrupo.Text;
end;

procedure TFmOVgIspPrfCad.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  if EdNome.Text = EMptyStr then
  try
    EdNome.SetFocus;
  except
    // nada
  end;
end;

procedure TFmOVgIspPrfCad.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  FCodigo   := 0;
  FSeqGrupo := 0;
  //UnDmkDAC_PF.AbreQuery(QrOVcYnsMedCad, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrOVcYnsChkCad, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrOVcYnsARQCad, Dmod.MyDB);
end;

procedure TFmOVgIspPrfCad.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmOVgIspPrfCad.ReopenOVcYnsMedCad(ArtigRef: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrOVcYnsMedCad, Dmod.MyDB, [
  'SELECT Codigo, Nome',
  'FROM ovcynsmedcad',
  'WHERE ArtigRef=' + Geral.FF0(ArtigRef),
  'ORDER BY Nome',
  '']);
end;

procedure TFmOVgIspPrfCad.SBOVcYnsARQClick(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  OVS_Jan.MostraFormOVcYnsARQCad();
  UMyMod.SetaCodigoPesquisado(EdOVcYnsARQ, CBOVcYnsARQ, QrOVcYnsARQCad, VAR_CADASTRO);
end;

procedure TFmOVgIspPrfCad.SbOVcYnsChkClick(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  OVS_Jan.MostraFormOVcYnsChkCad();
  UMyMod.SetaCodigoPesquisado(EdOVcYnsChk, CBOVcYnsChk, QrOVcYnsChkCad, VAR_CADASTRO);
end;

procedure TFmOVgIspPrfCad.SbOVcYnsMedClick(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  OVS_Jan.MostraFormOVcYnsMedCad(FSeqGrupo);
  UMyMod.SetaCodigoPesquisado(EdOVcYnsMed, CBOVcYnsMed, QrOVcYnsMedCad,
    VAR_CADASTRO);
end;

end.
