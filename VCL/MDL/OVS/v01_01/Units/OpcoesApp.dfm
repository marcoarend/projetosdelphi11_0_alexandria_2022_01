object FmOpcoesApp: TFmOpcoesApp
  Left = 339
  Top = 185
  Caption = 'APP-OPCAO-000 :: Op'#231#245'es Espec'#237'ficas do Sistema'
  ClientHeight = 657
  ClientWidth = 767
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 767
    Height = 501
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object PCGeral: TPageControl
      Left = 0
      Top = 0
      Width = 767
      Height = 501
      ActivePage = TabSheet2
      Align = alClient
      TabOrder = 0
      object TabSheet2: TTabSheet
        Caption = 'Importa'#231#227'o de dados'
        ImageIndex = 1
        object Panel6: TPanel
          Left = 0
          Top = 0
          Width = 759
          Height = 473
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          object Label2: TLabel
            Left = 188
            Top = 4
            Width = 175
            Height = 13
            Caption = 'Caminho da pasta dos arquivos CSV:'
          end
          object Panel2: TPanel
            Left = 0
            Top = 0
            Width = 759
            Height = 89
            Align = alTop
            TabOrder = 0
            object Label1: TLabel
              Left = 4
              Top = 4
              Width = 80
              Height = 13
              Caption = 'IP arquivos CSV:'
            end
            object Label3: TLabel
              Left = 4
              Top = 44
              Width = 163
              Height = 13
              Caption = 'Esquema de layout de importa'#231#227'o:'
            end
            object SbOVpLayEsq: TSpeedButton
              Left = 596
              Top = 59
              Width = 23
              Height = 23
              Caption = '...'
              OnClick = SbOVpLayEsqClick
            end
            object SpeedButton2: TSpeedButton
              Left = 728
              Top = 20
              Width = 23
              Height = 22
              OnClick = SpeedButton2Click
            end
            object Label11: TLabel
              Left = 624
              Top = 44
              Width = 115
              Height = 13
              Caption = 'Intervalo scan (minutos):'
            end
            object EdLoadCSVOthIP: TEdit
              Left = 4
              Top = 20
              Width = 181
              Height = 21
              TabOrder = 0
            end
            object EdLoadCSVOthDir: TEdit
              Left = 188
              Top = 20
              Width = 537
              Height = 21
              TabOrder = 1
            end
            object EdOVpLayEsq: TdmkEditCB
              Left = 4
              Top = 60
              Width = 56
              Height = 21
              Alignment = taRightJustify
              TabOrder = 2
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              DBLookupComboBox = CBOVpLayEsq
              IgnoraDBLookupComboBox = False
              AutoSetIfOnlyOneReg = setregOnlyManual
            end
            object CBOVpLayEsq: TdmkDBLookupComboBox
              Left = 62
              Top = 60
              Width = 531
              Height = 21
              KeyField = 'Codigo'
              ListField = 'Nome'
              ListSource = DsOVpLayEsq
              TabOrder = 3
              dmkEditCB = EdOVpLayEsq
              UpdType = utYes
              LocF7SQLMasc = '$#'
              LocF7PreDefProc = f7pNone
            end
            object EdLoadCSVIntrv: TdmkEdit
              Left = 624
              Top = 60
              Width = 125
              Height = 21
              Alignment = taRightJustify
              TabOrder = 4
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
            end
          end
          object PCFiltros: TPageControl
            Left = 0
            Top = 89
            Width = 759
            Height = 384
            ActivePage = TabSheet4
            Align = alClient
            TabOrder = 1
            object TabSheet4: TTabSheet
              Caption = ' Filtros de dados importados de fac'#231#245'es:  '
              object PanelFaccao: TPanel
                Left = 0
                Top = 0
                Width = 751
                Height = 356
                Align = alClient
                BevelOuter = bvNone
                ParentBackground = False
                TabOrder = 0
                object RGLoadCSVIntExt: TdmkRadioGroup
                  Left = 0
                  Top = 0
                  Width = 751
                  Height = 42
                  Align = alTop
                  Caption = ' Locais: '
                  Columns = 4
                  ItemIndex = 0
                  Items.Strings = (
                    'N/D'
                    'Internos'
                    'Externos'
                    'Ambos')
                  TabOrder = 0
                  UpdType = utYes
                  OldValor = 0
                end
                object RGLoadCSVSoLote: TdmkRadioGroup
                  Left = 0
                  Top = 42
                  Width = 751
                  Height = 42
                  Align = alTop
                  Caption = ' Lotes: '
                  Columns = 4
                  ItemIndex = 0
                  Items.Strings = (
                    'N/D'
                    'Com n'#250'mero de lote'
                    'Sem n'#250'mero de lote'
                    'Qualquer situa'#231#227'o')
                  TabOrder = 1
                  UpdType = utYes
                  OldValor = 0
                end
                object Panel3: TPanel
                  Left = 0
                  Top = 84
                  Width = 751
                  Height = 272
                  Align = alClient
                  TabOrder = 2
                  object GroupBox2: TGroupBox
                    Left = 1
                    Top = 1
                    Width = 370
                    Height = 270
                    Align = alLeft
                    Caption = ' Fitros b'#225'sicos de importa'#231#227'o de ordens de produ'#231#227'o: '
                    TabOrder = 0
                    object Panel4: TPanel
                      Left = 2
                      Top = 15
                      Width = 366
                      Height = 50
                      Align = alTop
                      TabOrder = 0
                      object Label4: TLabel
                        Left = 4
                        Top = 4
                        Width = 246
                        Height = 13
                        Caption = 'Empresa(s): (Texto. separar por v'#237'rgula! Ex,: 1,2,10):'
                      end
                      object EdLoadCSVEmpresas: TdmkEdit
                        Left = 4
                        Top = 20
                        Width = 352
                        Height = 21
                        TabOrder = 0
                        FormatType = dmktfString
                        MskType = fmtNone
                        DecimalSize = 0
                        LeftZeros = 0
                        NoEnterToTab = False
                        NoForceUppercase = False
                        ForceNextYear = False
                        DataFormat = dmkdfShort
                        HoraFormat = dmkhfShort
                        UpdType = utYes
                        Obrigatorio = False
                        PermiteNulo = False
                        ValueVariant = ''
                        ValWarn = False
                      end
                    end
                    object Panel5: TPanel
                      Left = 2
                      Top = 65
                      Width = 366
                      Height = 50
                      Align = alTop
                      TabOrder = 1
                      object Label5: TLabel
                        Left = 4
                        Top = 4
                        Width = 253
                        Height = 13
                        Caption = 'Tipos de OP: (Texto. separar por v'#237'rgula! Ex,: 1,2,10):'
                      end
                      object EdLoadCSVTipOP: TdmkEdit
                        Left = 4
                        Top = 20
                        Width = 352
                        Height = 21
                        TabOrder = 0
                        FormatType = dmktfString
                        MskType = fmtNone
                        DecimalSize = 0
                        LeftZeros = 0
                        NoEnterToTab = False
                        NoForceUppercase = False
                        ForceNextYear = False
                        DataFormat = dmkdfShort
                        HoraFormat = dmkhfShort
                        UpdType = utYes
                        Obrigatorio = False
                        PermiteNulo = False
                        ValueVariant = ''
                        ValWarn = False
                      end
                    end
                    object Panel7: TPanel
                      Left = 2
                      Top = 115
                      Width = 366
                      Height = 50
                      Align = alTop
                      TabOrder = 2
                      object Label6: TLabel
                        Left = 4
                        Top = 4
                        Width = 279
                        Height = 13
                        Caption = 'Situa'#231#245'es de OPs: (Texto. separar por v'#237'rgula! Ex,: 1,2,10):'
                      end
                      object EdLoadCSVSitOP: TdmkEdit
                        Left = 4
                        Top = 20
                        Width = 352
                        Height = 21
                        TabOrder = 0
                        FormatType = dmktfString
                        MskType = fmtNone
                        DecimalSize = 0
                        LeftZeros = 0
                        NoEnterToTab = False
                        NoForceUppercase = False
                        ForceNextYear = False
                        DataFormat = dmkdfShort
                        HoraFormat = dmkhfShort
                        UpdType = utYes
                        Obrigatorio = False
                        PermiteNulo = False
                        ValueVariant = ''
                        ValWarn = False
                      end
                    end
                    object Panel14: TPanel
                      Left = 2
                      Top = 165
                      Width = 366
                      Height = 50
                      Align = alTop
                      TabOrder = 3
                      object Label7: TLabel
                        Left = 4
                        Top = 4
                        Width = 329
                        Height = 13
                        Caption = 
                          'Tipos de localiza'#231#227'o de OPs: (Texto. separar por v'#237'rgula! Ex,: 1' +
                          ',2,10):'
                      end
                      object EdLoadCSVTipLoclz: TdmkEdit
                        Left = 4
                        Top = 20
                        Width = 352
                        Height = 21
                        TabOrder = 0
                        FormatType = dmktfString
                        MskType = fmtNone
                        DecimalSize = 0
                        LeftZeros = 0
                        NoEnterToTab = False
                        NoForceUppercase = False
                        ForceNextYear = False
                        DataFormat = dmkdfShort
                        HoraFormat = dmkhfShort
                        UpdType = utYes
                        Obrigatorio = False
                        PermiteNulo = False
                        ValueVariant = ''
                        ValWarn = False
                      end
                    end
                    object Panel15: TPanel
                      Left = 2
                      Top = 215
                      Width = 366
                      Height = 50
                      Align = alTop
                      TabOrder = 4
                      object Label8: TLabel
                        Left = 4
                        Top = 4
                        Width = 347
                        Height = 13
                        Caption = 
                          'Tipos de produ'#231#227'o de OPs: (Texto. separar por v'#237'rgula! Ex,: "A",' +
                          '"P","F"):'
                      end
                      object EdLoadCSVTipProdOP: TdmkEdit
                        Left = 4
                        Top = 20
                        Width = 352
                        Height = 21
                        TabOrder = 0
                        FormatType = dmktfString
                        MskType = fmtNone
                        DecimalSize = 0
                        LeftZeros = 0
                        NoEnterToTab = False
                        NoForceUppercase = False
                        ForceNextYear = False
                        DataFormat = dmkdfShort
                        HoraFormat = dmkhfShort
                        UpdType = utYes
                        Obrigatorio = False
                        PermiteNulo = False
                        ValueVariant = ''
                        ValWarn = False
                      end
                    end
                  end
                  object GroupBox3: TGroupBox
                    Left = 371
                    Top = 1
                    Width = 379
                    Height = 270
                    Align = alClient
                    Caption = ' Defini'#231#245'es de cadastros de segmentos: '
                    TabOrder = 1
                    object Label31: TLabel
                      Left = 8
                      Top = 120
                      Width = 116
                      Height = 13
                      Caption = 'Senha de desobriga'#231#227'o:'
                    end
                    object Panel17: TPanel
                      Left = 2
                      Top = 15
                      Width = 375
                      Height = 50
                      Align = alTop
                      TabOrder = 0
                      object Label15: TLabel
                        Left = 4
                        Top = 4
                        Width = 340
                        Height = 13
                        Caption = 
                          'Defini'#231#227'o de "FACCOES" em valor de atributo de classificac'#227'o de ' +
                          'local:'
                      end
                      object EdLoadCSVAtbValr1: TdmkEdit
                        Left = 4
                        Top = 20
                        Width = 352
                        Height = 21
                        TabOrder = 0
                        FormatType = dmktfString
                        MskType = fmtNone
                        DecimalSize = 0
                        LeftZeros = 0
                        NoEnterToTab = False
                        NoForceUppercase = False
                        ForceNextYear = False
                        DataFormat = dmkdfShort
                        HoraFormat = dmkhfShort
                        UpdType = utYes
                        Obrigatorio = False
                        PermiteNulo = False
                        ValueVariant = ''
                        ValWarn = False
                      end
                    end
                    object Panel28: TPanel
                      Left = 2
                      Top = 65
                      Width = 375
                      Height = 50
                      Align = alTop
                      TabOrder = 1
                      object Label27: TLabel
                        Left = 4
                        Top = 4
                        Width = 284
                        Height = 13
                        Caption = 'C'#243'digos (2) de fac'#231#245'es de atributo de classificac'#227'o de local:'
                      end
                      object EdLoadCSVCodValr1: TdmkEdit
                        Left = 4
                        Top = 20
                        Width = 352
                        Height = 21
                        TabOrder = 0
                        FormatType = dmktfString
                        MskType = fmtNone
                        DecimalSize = 0
                        LeftZeros = 0
                        NoEnterToTab = False
                        NoForceUppercase = False
                        ForceNextYear = False
                        DataFormat = dmkdfShort
                        HoraFormat = dmkhfShort
                        UpdType = utYes
                        Obrigatorio = False
                        PermiteNulo = False
                        ValueVariant = ''
                        ValWarn = False
                      end
                    end
                    object EdPwdDesobriga: TEdit
                      Left = 8
                      Top = 136
                      Width = 349
                      Height = 21
                      TabOrder = 2
                    end
                  end
                end
              end
            end
            object TabSheet5: TTabSheet
              Caption = ' Filtros de dados importados de t'#234'xteis:  '
              ImageIndex = 1
              ExplicitLeft = 0
              ExplicitTop = 0
              ExplicitWidth = 0
              ExplicitHeight = 0
              object PnTexteis: TPanel
                Left = 0
                Top = 0
                Width = 751
                Height = 356
                Align = alClient
                BevelOuter = bvNone
                ParentBackground = False
                TabOrder = 0
                object RGLoadCSVIntEx2: TdmkRadioGroup
                  Left = 0
                  Top = 0
                  Width = 751
                  Height = 42
                  Align = alTop
                  Caption = ' Locais: '
                  Columns = 4
                  ItemIndex = 0
                  Items.Strings = (
                    'N/D'
                    'Internos'
                    'Externos'
                    'Ambos')
                  TabOrder = 0
                  UpdType = utYes
                  OldValor = 0
                end
                object RGLoadCSVSoLot2: TdmkRadioGroup
                  Left = 0
                  Top = 42
                  Width = 751
                  Height = 42
                  Align = alTop
                  Caption = ' Lotes: '
                  Columns = 4
                  ItemIndex = 0
                  Items.Strings = (
                    'N/D'
                    'Com n'#250'mero de lote'
                    'Sem n'#250'mero de lote'
                    'Qualquer situa'#231#227'o')
                  TabOrder = 1
                  UpdType = utYes
                  OldValor = 0
                end
                object Panel20: TPanel
                  Left = 0
                  Top = 84
                  Width = 751
                  Height = 272
                  Align = alClient
                  TabOrder = 2
                  object GroupBox1: TGroupBox
                    Left = 1
                    Top = 1
                    Width = 370
                    Height = 270
                    Align = alLeft
                    Caption = ' Fitros b'#225'sicos de importa'#231#227'o de ordens de produ'#231#227'o: '
                    TabOrder = 0
                    object Panel21: TPanel
                      Left = 2
                      Top = 15
                      Width = 366
                      Height = 50
                      Align = alTop
                      TabOrder = 0
                      object Label19: TLabel
                        Left = 4
                        Top = 4
                        Width = 246
                        Height = 13
                        Caption = 'Empresa(s): (Texto. separar por v'#237'rgula! Ex,: 1,2,10):'
                      end
                      object EdLoadCSVEmpresa2: TdmkEdit
                        Left = 4
                        Top = 20
                        Width = 352
                        Height = 21
                        TabOrder = 0
                        FormatType = dmktfString
                        MskType = fmtNone
                        DecimalSize = 0
                        LeftZeros = 0
                        NoEnterToTab = False
                        NoForceUppercase = False
                        ForceNextYear = False
                        DataFormat = dmkdfShort
                        HoraFormat = dmkhfShort
                        UpdType = utYes
                        Obrigatorio = False
                        PermiteNulo = False
                        ValueVariant = ''
                        ValWarn = False
                      end
                    end
                    object Panel22: TPanel
                      Left = 2
                      Top = 65
                      Width = 366
                      Height = 50
                      Align = alTop
                      TabOrder = 1
                      object Label20: TLabel
                        Left = 4
                        Top = 4
                        Width = 253
                        Height = 13
                        Caption = 'Tipos de OP: (Texto. separar por v'#237'rgula! Ex,: 1,2,10):'
                      end
                      object EdLoadCSVTipO2: TdmkEdit
                        Left = 4
                        Top = 20
                        Width = 352
                        Height = 21
                        TabOrder = 0
                        FormatType = dmktfString
                        MskType = fmtNone
                        DecimalSize = 0
                        LeftZeros = 0
                        NoEnterToTab = False
                        NoForceUppercase = False
                        ForceNextYear = False
                        DataFormat = dmkdfShort
                        HoraFormat = dmkhfShort
                        UpdType = utYes
                        Obrigatorio = False
                        PermiteNulo = False
                        ValueVariant = ''
                        ValWarn = False
                      end
                    end
                    object Panel23: TPanel
                      Left = 2
                      Top = 115
                      Width = 366
                      Height = 50
                      Align = alTop
                      TabOrder = 2
                      object Label21: TLabel
                        Left = 4
                        Top = 4
                        Width = 279
                        Height = 13
                        Caption = 'Situa'#231#245'es de OPs: (Texto. separar por v'#237'rgula! Ex,: 1,2,10):'
                      end
                      object EdLoadCSVSitO2: TdmkEdit
                        Left = 4
                        Top = 20
                        Width = 352
                        Height = 21
                        TabOrder = 0
                        FormatType = dmktfString
                        MskType = fmtNone
                        DecimalSize = 0
                        LeftZeros = 0
                        NoEnterToTab = False
                        NoForceUppercase = False
                        ForceNextYear = False
                        DataFormat = dmkdfShort
                        HoraFormat = dmkhfShort
                        UpdType = utYes
                        Obrigatorio = False
                        PermiteNulo = False
                        ValueVariant = ''
                        ValWarn = False
                      end
                    end
                    object Panel24: TPanel
                      Left = 2
                      Top = 165
                      Width = 366
                      Height = 50
                      Align = alTop
                      TabOrder = 3
                      object Label22: TLabel
                        Left = 4
                        Top = 4
                        Width = 329
                        Height = 13
                        Caption = 
                          'Tipos de localiza'#231#227'o de OPs: (Texto. separar por v'#237'rgula! Ex,: 1' +
                          ',2,10):'
                      end
                      object EdLoadCSVTipLocl2: TdmkEdit
                        Left = 4
                        Top = 20
                        Width = 352
                        Height = 21
                        TabOrder = 0
                        FormatType = dmktfString
                        MskType = fmtNone
                        DecimalSize = 0
                        LeftZeros = 0
                        NoEnterToTab = False
                        NoForceUppercase = False
                        ForceNextYear = False
                        DataFormat = dmkdfShort
                        HoraFormat = dmkhfShort
                        UpdType = utYes
                        Obrigatorio = False
                        PermiteNulo = False
                        ValueVariant = ''
                        ValWarn = False
                      end
                    end
                    object Panel25: TPanel
                      Left = 2
                      Top = 215
                      Width = 366
                      Height = 50
                      Align = alTop
                      TabOrder = 4
                      object Label23: TLabel
                        Left = 4
                        Top = 4
                        Width = 347
                        Height = 13
                        Caption = 
                          'Tipos de produ'#231#227'o de OPs: (Texto. separar por v'#237'rgula! Ex,: "A",' +
                          '"P","F"):'
                      end
                      object EdLoadCSVTipProdO2: TdmkEdit
                        Left = 4
                        Top = 20
                        Width = 352
                        Height = 21
                        TabOrder = 0
                        FormatType = dmktfString
                        MskType = fmtNone
                        DecimalSize = 0
                        LeftZeros = 0
                        NoEnterToTab = False
                        NoForceUppercase = False
                        ForceNextYear = False
                        DataFormat = dmkdfShort
                        HoraFormat = dmkhfShort
                        UpdType = utYes
                        Obrigatorio = False
                        PermiteNulo = False
                        ValueVariant = ''
                        ValWarn = False
                      end
                    end
                  end
                  object GroupBox4: TGroupBox
                    Left = 371
                    Top = 1
                    Width = 379
                    Height = 270
                    Align = alClient
                    Caption = ' Defini'#231#245'es de cadastros de segmentos: '
                    TabOrder = 1
                    object Panel18: TPanel
                      Left = 2
                      Top = 15
                      Width = 375
                      Height = 50
                      Align = alTop
                      TabOrder = 0
                      object Label16: TLabel
                        Left = 4
                        Top = 4
                        Width = 351
                        Height = 13
                        Caption = 
                          'Outros segmentos* em valor de atributo de classif. de local insp' +
                          'ecion'#225'veis:'
                      end
                      object EdLoadCSVAtbValr2: TdmkEdit
                        Left = 4
                        Top = 20
                        Width = 352
                        Height = 21
                        TabOrder = 0
                        FormatType = dmktfString
                        MskType = fmtNone
                        DecimalSize = 0
                        LeftZeros = 0
                        NoEnterToTab = False
                        NoForceUppercase = False
                        ForceNextYear = False
                        DataFormat = dmkdfShort
                        HoraFormat = dmkhfShort
                        UpdType = utYes
                        Obrigatorio = False
                        PermiteNulo = False
                        ValueVariant = ''
                        ValWarn = False
                      end
                    end
                    object Panel29: TPanel
                      Left = 2
                      Top = 65
                      Width = 375
                      Height = 50
                      Align = alTop
                      TabOrder = 1
                      object Label28: TLabel
                        Left = 4
                        Top = 4
                        Width = 343
                        Height = 13
                        Caption = 
                          'Outros c'#243'digos* em segmentos de atributo de classif. de local in' +
                          'specion.:'
                      end
                      object EdLoadCSVCodValr2: TdmkEdit
                        Left = 4
                        Top = 20
                        Width = 352
                        Height = 21
                        TabOrder = 0
                        FormatType = dmktfString
                        MskType = fmtNone
                        DecimalSize = 0
                        LeftZeros = 0
                        NoEnterToTab = False
                        NoForceUppercase = False
                        ForceNextYear = False
                        DataFormat = dmkdfShort
                        HoraFormat = dmkhfShort
                        UpdType = utYes
                        Obrigatorio = False
                        PermiteNulo = False
                        ValueVariant = ''
                        ValWarn = False
                      end
                    end
                  end
                end
              end
            end
            object TabSheet7: TTabSheet
              Caption = 'Filtros de se'#231#245'es de produ'#231#227'o:'
              ImageIndex = 2
              ExplicitLeft = 0
              ExplicitTop = 0
              ExplicitWidth = 0
              ExplicitHeight = 0
              object GroupBox5: TGroupBox
                Left = 0
                Top = 0
                Width = 370
                Height = 356
                Align = alLeft
                Caption = ' Fitros b'#225'sicos de importa'#231#227'o de ordens de produ'#231#227'o: '
                Color = clBtnFace
                ParentBackground = False
                ParentColor = False
                TabOrder = 0
                object Panel19: TPanel
                  Left = 2
                  Top = 15
                  Width = 366
                  Height = 50
                  Align = alTop
                  TabOrder = 0
                  object Label24: TLabel
                    Left = 4
                    Top = 4
                    Width = 346
                    Height = 13
                    Caption = 
                      'Defini'#231#227'o de confec'#231#227'o ("FACCOES") em valor de atributo de clas.' +
                      ' local:'
                  end
                  object EdSecConfeccao: TdmkEdit
                    Left = 60
                    Top = 20
                    Width = 296
                    Height = 21
                    TabOrder = 1
                    FormatType = dmktfString
                    MskType = fmtNone
                    DecimalSize = 0
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = ''
                    ValWarn = False
                  end
                  object EdCdScConfeccao: TdmkEdit
                    Left = 4
                    Top = 20
                    Width = 56
                    Height = 21
                    Alignment = taRightJustify
                    TabOrder = 0
                    FormatType = dmktfInteger
                    MskType = fmtNone
                    DecimalSize = 0
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '0'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 0
                    ValWarn = False
                  end
                end
                object Panel26: TPanel
                  Left = 2
                  Top = 65
                  Width = 366
                  Height = 50
                  Align = alTop
                  TabOrder = 1
                  object Label25: TLabel
                    Left = 4
                    Top = 4
                    Width = 360
                    Height = 13
                    Caption = 
                      'Defini'#231#227'o de tecelagem ("TECELAGEM") em valor de atributo de cla' +
                      's. local:'
                  end
                  object EdSecTecelagem: TdmkEdit
                    Left = 60
                    Top = 20
                    Width = 296
                    Height = 21
                    TabOrder = 1
                    FormatType = dmktfString
                    MskType = fmtNone
                    DecimalSize = 0
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = ''
                    ValWarn = False
                  end
                  object EdCdScTecelagem: TdmkEdit
                    Left = 4
                    Top = 20
                    Width = 56
                    Height = 21
                    Alignment = taRightJustify
                    TabOrder = 0
                    FormatType = dmktfInteger
                    MskType = fmtNone
                    DecimalSize = 0
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '0'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 0
                    ValWarn = False
                  end
                end
                object Panel27: TPanel
                  Left = 2
                  Top = 115
                  Width = 366
                  Height = 50
                  Align = alTop
                  TabOrder = 2
                  object Label26: TLabel
                    Left = 4
                    Top = 4
                    Width = 352
                    Height = 13
                    Caption = 
                      'Defini'#231#227'o de tinturaria ("TINTURARIA") em valor de atributo de c' +
                      'las. local:'
                  end
                  object EdSecTinturaria: TdmkEdit
                    Left = 60
                    Top = 20
                    Width = 296
                    Height = 21
                    TabOrder = 1
                    FormatType = dmktfString
                    MskType = fmtNone
                    DecimalSize = 0
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = ''
                    ValWarn = False
                  end
                  object EdCdScTinturaria: TdmkEdit
                    Left = 4
                    Top = 20
                    Width = 56
                    Height = 21
                    Alignment = taRightJustify
                    TabOrder = 0
                    FormatType = dmktfInteger
                    MskType = fmtNone
                    DecimalSize = 0
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '0'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 0
                    ValWarn = False
                  end
                end
              end
            end
          end
        end
      end
      object TabSheet3: TTabSheet
        Caption = 'Outras pastas'
        ImageIndex = 1
        object Panel12: TPanel
          Left = 0
          Top = 0
          Width = 759
          Height = 473
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          object Panel13: TPanel
            Left = 0
            Top = 424
            Width = 759
            Height = 49
            Align = alBottom
            BevelOuter = bvNone
            TabOrder = 0
            object BtIncluiPasta: TBitBtn
              Tag = 14
              Left = 8
              Top = 4
              Width = 120
              Height = 40
              Caption = '&Incluir pasta'
              NumGlyphs = 2
              TabOrder = 0
              OnClick = BtIncluiPastaClick
            end
            object BtExcluiPasta: TBitBtn
              Tag = 13
              Left = 132
              Top = 4
              Width = 120
              Height = 40
              Cursor = crHandPoint
              Caption = '&Excluir pasta'
              NumGlyphs = 2
              ParentShowHint = False
              ShowHint = True
              TabOrder = 1
              OnClick = BtExcluiPastaClick
            end
          end
          object DBGrid1: TDBGrid
            Left = 0
            Top = 0
            Width = 759
            Height = 424
            Align = alClient
            DataSource = DsOVpDirXtr
            TabOrder = 1
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            Columns = <
              item
                Expanded = False
                FieldName = 'Nome'
                Visible = True
              end>
          end
        end
      end
      object TabSheet1: TTabSheet
        Caption = 'Miscel'#226'nea'
        ImageIndex = 2
        object Panel10: TPanel
          Left = 0
          Top = 0
          Width = 759
          Height = 473
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          object Label10: TLabel
            Left = 15
            Top = 69
            Width = 69
            Height = 13
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Tipo de e-mail:'
          end
          object SpeedButton1: TSpeedButton
            Left = 635
            Top = 89
            Width = 26
            Height = 25
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = '...'
            OnClick = SpeedButton2Click
          end
          object Panel11: TPanel
            Left = 0
            Top = 0
            Width = 759
            Height = 317
            Align = alTop
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 0
            object Label9: TLabel
              Left = 8
              Top = 8
              Width = 129
              Height = 13
              Caption = 'Nome amig'#225'vel do sistema:'
            end
            object Label12: TLabel
              Left = 8
              Top = 48
              Width = 235
              Height = 13
              Caption = 'Tipo de email de envio de resultado de inspe'#231#227'o: '
            end
            object SbEntiTipCto_Inspecao: TSpeedButton
              Left = 732
              Top = 64
              Width = 21
              Height = 21
              Caption = '...'
              OnClick = SbEntiTipCto_InspecaoClick
            end
            object Label13: TLabel
              Left = 8
              Top = 88
              Width = 315
              Height = 13
              Caption = 
                'Email (grupo) para envio dos resultados das inspe'#231#245'es de fac'#231#245'es' +
                ':'
            end
            object Label18: TLabel
              Left = 8
              Top = 128
              Width = 307
              Height = 13
              Caption = 
                'Email (grupo) para envio dos resultados das inspe'#231#245'es de t'#234'xteis' +
                ':'
            end
            object Label14: TLabel
              Left = 220
              Top = 8
              Width = 371
              Height = 13
              Caption = 
                'C'#243'digo e defini'#231#227'o de "LOCAL" em nome de atributo de classificac' +
                #227'o de local:'
            end
            object EdERPNameByCli: TdmkEdit
              Left = 8
              Top = 24
              Width = 205
              Height = 21
              TabOrder = 0
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              QryCampo = 'ERPNameByCli'
              UpdCampo = 'ERPNameByCli'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object EdEntiTipCto_Inspecao: TdmkEditCB
              Left = 8
              Top = 64
              Width = 56
              Height = 21
              Alignment = taRightJustify
              TabOrder = 1
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              QryCampo = 'EntiTipCto_Inspecao'
              UpdCampo = 'EntiTipCto_Inspecao'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              DBLookupComboBox = CBEntiTipCto_Inspecao
              IgnoraDBLookupComboBox = False
              AutoSetIfOnlyOneReg = setregOnlyManual
            end
            object CBEntiTipCto_Inspecao: TdmkDBLookupComboBox
              Left = 64
              Top = 64
              Width = 669
              Height = 21
              KeyField = 'CodUsu'
              ListField = 'Nome'
              ListSource = DsEntiTipCto
              TabOrder = 2
              dmkEditCB = EdEntiTipCto_Inspecao
              QryCampo = 'EntiTipCto_Inspecao'
              UpdType = utYes
              LocF7SQLMasc = '$#'
              LocF7PreDefProc = f7pNone
            end
            object EdMailResInspResul: TdmkEdit
              Left = 8
              Top = 104
              Width = 745
              Height = 21
              TabOrder = 3
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              QryCampo = 'MailResInspResul'
              UpdCampo = 'MailResInspResul'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object CkForcaReabCfgInsp: TdmkCheckBox
              Left = 8
              Top = 168
              Width = 745
              Height = 17
              Caption = 
                'For'#231'a a reabertura de configura'#231#245'es de inspe'#231#245'es de todos Locais' +
                '/Artigos de Reduzidos de OPs ativos.'
              TabOrder = 4
              UpdType = utYes
              ValCheck = #0
              ValUncheck = #0
              OldValor = #0
            end
            object EdMailResInspResu2: TdmkEdit
              Left = 8
              Top = 144
              Width = 745
              Height = 21
              TabOrder = 5
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              QryCampo = 'MailResInspResu2'
              UpdCampo = 'MailResInspResu2'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object EdLoadCSVAtbNomLocal: TdmkEdit
              Left = 276
              Top = 24
              Width = 477
              Height = 21
              TabOrder = 6
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object EdLoadCSVCodNomLocal: TdmkEdit
              Left = 220
              Top = 24
              Width = 56
              Height = 21
              Alignment = taRightJustify
              TabOrder = 7
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
            end
          end
        end
      end
      object TabSheet6: TTabSheet
        Caption = 'Usu'#225'rios'
        ImageIndex = 3
        object Panel16: TPanel
          Left = 0
          Top = 425
          Width = 759
          Height = 48
          Align = alBottom
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          object BtExclui: TBitBtn
            Tag = 12
            Left = 252
            Top = 4
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Caption = '&Exclui'
            Enabled = False
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 2
            OnClick = BtExcluiClick
          end
          object BtAltera: TBitBtn
            Tag = 11
            Left = 128
            Top = 4
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Caption = '&Altera'
            Enabled = False
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 1
            OnClick = BtAlteraClick
          end
          object BtInclui: TBitBtn
            Tag = 10
            Left = 4
            Top = 4
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Caption = '&Inclui'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtIncluiClick
          end
        end
        object DBGOpcoesApU: TdmkDBGridZTO
          Left = 0
          Top = 0
          Width = 759
          Height = 425
          Align = alClient
          DataSource = DsOpcoesApU
          Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
          TabOrder = 1
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          RowColors = <>
          Columns = <
            item
              Expanded = False
              FieldName = 'Codigo'
              Title.Caption = 'ID'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Login'
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'NO_HabFaccao'
              Title.Caption = 'Fac'#231#227'o'
              Width = 44
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_HabFacConfecaocao'
              Title.Caption = 'Confec'#231#227'o'
              Width = 44
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'NO_HabTextil'
              Title.Caption = 'T'#234'xtil'
              Width = 44
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_HabTexTecelagem'
              Title.Caption = 'Tecelagem'
              Width = 44
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_HabTexTinturaria'
              Title.Caption = 'Tinturaria'
              Width = 44
              Visible = True
            end>
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 767
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 719
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 671
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 384
        Height = 32
        Caption = 'Op'#231#245'es Espec'#237'ficas do Sistema'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 384
        Height = 32
        Caption = 'Op'#231#245'es Espec'#237'ficas do Sistema'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 384
        Height = 32
        Caption = 'Op'#231#245'es Espec'#237'ficas do Sistema'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 549
    Width = 767
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel8: TPanel
      Left = 2
      Top = 15
      Width = 763
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 593
    Width = 767
    Height = 64
    Align = alBottom
    TabOrder = 3
    object Panel9: TPanel
      Left = 2
      Top = 15
      Width = 763
      Height = 47
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaLoadCSVAtbValr2: TLabel
        Left = 264
        Top = 8
        Width = 311
        Height = 13
        Caption = '"TECELAGEM", "TINTURARIA", "BORDADO", "SILK"'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        OnDblClick = LaLoadCSVAtbValr2DblClick
      end
      object Label29: TLabel
        Left = 140
        Top = 24
        Width = 99
        Height = 13
        Caption = 'Outros c'#243'digos*: '
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label17: TLabel
        Left = 140
        Top = 8
        Width = 115
        Height = 13
        Caption = 'Outros segmentos*: '
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        OnDblClick = LaLoadCSVAtbValr2DblClick
      end
      object Label30: TLabel
        Left = 264
        Top = 24
        Width = 41
        Height = 13
        Caption = '6,4,3,9'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object PnSaiDesis: TPanel
        Left = 619
        Top = 0
        Width = 144
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtSaida: TBitBtn
          Tag = 15
          Left = 6
          Top = 3
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Hint = 'Sai da janela atual'
          Caption = '&Desiste'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
        end
      end
      object BtOK: TBitBtn
        Tag = 14
        Left = 14
        Top = 3
        Width = 119
        Height = 40
        Caption = '&Salva'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object QrOVpLayEsq: TMySQLQuery
    Database = FmVerificaConexoes.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome '
      'FROM ovplayesq'
      'ORDER BY Nome')
    Left = 538
    Top = 24
    object QrOVpLayEsqCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrOVpLayEsqNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
  end
  object DsOVpLayEsq: TDataSource
    DataSet = QrOVpLayEsq
    Left = 538
    Top = 72
  end
  object QrEntiTipCto: TMySQLQuery
    Database = FmVerificaConexoes.MyDB
    SQL.Strings = (
      'SELECT Codigo,CodUsu, Nome '
      'FROM entitipcto'
      'ORDER BY Nome')
    Left = 630
    Top = 28
    object QrEntiTipCtoCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrEntiTipCtoCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEntiTipCtoNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
  end
  object DsEntiTipCto: TDataSource
    DataSet = QrEntiTipCto
    Left = 630
    Top = 76
  end
  object dmkValUsu1: TdmkValUsu
    QryCampo = 'EntiTipCto'
    UpdCampo = 'EntiTipCto'
    RefCampo = 'Codigo'
    UpdType = utYes
    Left = 64
    Top = 12
  end
  object QrOVpDirXtr: TMySQLQuery
    Database = FmVerificaConexoes.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM ovpdirxtr'
      'ORDER BU Nome')
    Left = 392
    Top = 108
    object QrOVpDirXtrCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrOVpDirXtrNome: TWideStringField
      FieldName = 'Nome'
      Size = 255
    end
  end
  object DsOVpDirXtr: TDataSource
    DataSet = QrOVpDirXtr
    Left = 392
    Top = 156
  end
  object QrOpcoesApU: TMySQLQuery
    Database = FmVerificaConexoes.MyDB
    AfterOpen = QrOpcoesApUAfterOpen
    BeforeClose = QrOpcoesApUBeforeClose
    SQL.Strings = (
      'SELECT apu.Codigo, apu.HabFaccao, apu.HabTextil, '
      'apu.HabFacConfeccao, '
      'apu.HabTexTecelagem, apu.HabTexTinturaria, '
      'IF(apu.HabFaccao=1, "SIM", "N'#195'O") NO_HabFaccao, '
      'IF(apu.HabFacConfeccao=1, "SIM", "N'#195'O") NO_HabFacConfecaocao, '
      'IF(apu.HabTextil=1, "SIM", "N'#195'O") NO_HabTextil, '
      'IF(apu.HabTexTecelagem=1, "SIM", "N'#195'O") NO_HabTexTecelagem, '
      'IF(apu.HabTexTinturaria=1, "SIM", "N'#195'O") NO_HabTexTinturaria, '
      'pwd.Login '
      'FROM opcoesapu apu '
      'LEFT JOIN senhas pwd ON pwd.Numero=apu.Codigo ')
    Left = 146
    Top = 216
    object QrOpcoesApUCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrOpcoesApUHabFaccao: TSmallintField
      FieldName = 'HabFaccao'
      Required = True
    end
    object QrOpcoesApUHabTextil: TSmallintField
      FieldName = 'HabTextil'
      Required = True
    end
    object QrOpcoesApUNO_HabFaccao: TWideStringField
      FieldName = 'NO_HabFaccao'
      Required = True
      Size = 3
    end
    object QrOpcoesApUNO_HabTextil: TWideStringField
      FieldName = 'NO_HabTextil'
      Required = True
      Size = 3
    end
    object QrOpcoesApULogin: TWideStringField
      FieldName = 'Login'
      Size = 30
    end
    object QrOpcoesApUHabFacConfeccao: TSmallintField
      FieldName = 'HabFacConfeccao'
      Required = True
    end
    object QrOpcoesApUHabTexTecelagem: TSmallintField
      FieldName = 'HabTexTecelagem'
      Required = True
    end
    object QrOpcoesApUHabTexTinturaria: TSmallintField
      FieldName = 'HabTexTinturaria'
      Required = True
    end
    object QrOpcoesApUNO_HabFacConfecaocao: TWideStringField
      FieldName = 'NO_HabFacConfecaocao'
      Required = True
      Size = 3
    end
    object QrOpcoesApUNO_HabTexTecelagem: TWideStringField
      FieldName = 'NO_HabTexTecelagem'
      Required = True
      Size = 3
    end
    object QrOpcoesApUNO_HabTexTinturaria: TWideStringField
      FieldName = 'NO_HabTexTinturaria'
      Required = True
      Size = 3
    end
  end
  object DsOpcoesApU: TDataSource
    DataSet = QrOpcoesApU
    Left = 146
    Top = 264
  end
end
