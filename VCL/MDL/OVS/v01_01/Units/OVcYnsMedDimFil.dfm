object FmOVcYnsMedDimFil: TFmOVcYnsMedDimFil
  Left = 339
  Top = 185
  Caption = 'OVS-TAMAN-005 :: Preenchimento de Medidas de Tabela de Medidas'
  ClientHeight = 629
  ClientWidth = 812
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 812
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 764
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 716
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 604
        Height = 32
        Caption = 'Preenchimento de Medidas de Tabela de Medidas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 604
        Height = 32
        Caption = 'Preenchimento de Medidas de Tabela de Medidas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 604
        Height = 32
        Caption = 'Preenchimento de Medidas de Tabela de Medidas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 812
    Height = 467
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 812
      Height = 467
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 812
        Height = 467
        Align = alClient
        TabOrder = 0
        object Panel6: TPanel
          Left = 2
          Top = 15
          Width = 808
          Height = 450
          Align = alClient
          TabOrder = 0
          object ScrollBox1: TScrollBox
            Left = 1
            Top = 1
            Width = 806
            Height = 448
            Align = alClient
            TabOrder = 0
            object GPBoxes: TGridPanel
              Left = 0
              Top = 0
              Width = 500
              Height = 229
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = '...'
              ColumnCollection = <
                item
                  SizeStyle = ssAbsolute
                  Value = 30.000000000000000000
                end
                item
                  SizeStyle = ssAbsolute
                  Value = 100.000000000000000000
                end
                item
                  SizeStyle = ssAbsolute
                  Value = 100.000000000000000000
                end
                item
                  SizeStyle = ssAbsolute
                  Value = 100.000000000000000000
                end>
              ControlCollection = <>
              RowCollection = <
                item
                  SizeStyle = ssAbsolute
                  Value = 24.000000000000000000
                end
                item
                  SizeStyle = ssAbsolute
                  Value = 24.000000000000000000
                end
                item
                  SizeStyle = ssAbsolute
                  Value = 24.000000000000000000
                end>
              TabOrder = 0
            end
          end
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 515
    Width = 812
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 808
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 559
    Width = 812
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 666
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 664
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
      end
      object BitBtn1: TBitBtn
        Left = 356
        Top = 8
        Width = 75
        Height = 25
        Caption = 'BitBtn1'
        TabOrder = 1
        OnClick = BitBtn1Click
      end
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 352
    Top = 171
  end
  object QrTop: TMySQLQuery
    Database = FmPrincipal.TempDB
    SQL.Strings = (
      
        'SELECT ELT(ymt.TolerBasCalc+1, "Medida", "Percentual") NO_TolerB' +
        'asCalc,'
      'ELT(ymt.TolerUnMdida+1, "cm", "inch") NO_TolerUnMdida, '
      'ygt.Nome NO_TOBIKO, ymt.*'
      'FROM ovcynsmedtop ymt'
      'LEFT JOIN ovcynsgratop ygt ON ygt.Codigo=ymt.Tobiko'
      'WHERE ygt.Codigo=:P0'
      '')
    Left = 340
    Top = 229
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrTopNO_TolerBasCalc: TWideStringField
      FieldName = 'NO_TolerBasCalc'
      Size = 10
    end
    object QrTopNO_TolerUnMdida: TWideStringField
      FieldName = 'NO_TolerUnMdida'
      Size = 4
    end
    object QrTopNO_TOBIKO: TWideStringField
      FieldName = 'NO_TOBIKO'
      Size = 60
    end
    object QrTopCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrTopControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrTopTobiko: TIntegerField
      FieldName = 'Tobiko'
      Required = True
    end
    object QrTopTolerBasCalc: TSmallintField
      FieldName = 'TolerBasCalc'
      Required = True
    end
    object QrTopTolerUnMdida: TSmallintField
      FieldName = 'TolerUnMdida'
      Required = True
    end
    object QrTopTolerRndPerc: TFloatField
      FieldName = 'TolerRndPerc'
      Required = True
      DisplayFormat = '0.000'
    end
    object QrTopLk: TIntegerField
      FieldName = 'Lk'
      Required = True
    end
    object QrTopDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrTopDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrTopUserCad: TIntegerField
      FieldName = 'UserCad'
      Required = True
    end
    object QrTopUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Required = True
    end
    object QrTopAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Required = True
    end
    object QrTopAWServerID: TIntegerField
      FieldName = 'AWServerID'
      Required = True
    end
    object QrTopAWStatSinc: TSmallintField
      FieldName = 'AWStatSinc'
      Required = True
    end
    object QrTopAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
  end
  object DqAux: TMySQLDirectQuery
    Database = FmPrincipal.TempDB
    Left = 352
    Top = 372
  end
  object QrRow: TMySQLQuery
    Database = FmPrincipal.TempDB
    SQL.Strings = (
      'SELECT DISTINCT ymd.CodGrade, ymd.CodTam, '
      'LPAD(CodTam, 30, " ")  OrdTam'
      'FROM ovcynsmeddim ymd '
      'LEFT JOIN ovdgradetam ogt ON  '
      '  ogt.Codigo=ymd.CodGrade '
      '  AND '
      '  ogt.Nome=ymd.CodTam '
      'WHERE ymd.Codigo=:P0'
      'ORDER BY ogt.Ordem, OrdTam, ogt.Nome ')
    Left = 340
    Top = 280
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrRowCodGrade: TIntegerField
      FieldName = 'CodGrade'
      Required = True
    end
    object QrRowCodTam: TWideStringField
      FieldName = 'CodTam'
      Size = 30
    end
    object QrRowOrdTam: TWideStringField
      FieldName = 'OrdTam'
      Size = 30
    end
  end
end
