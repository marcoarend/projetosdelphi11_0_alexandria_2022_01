unit OVcYnsQstTop;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2, dmkGeral,
  ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt, UMySQLModule, Menus,
  UnInternalConsts2, mySQLDbTables, UnMySQLCuringa, dmkPermissoes, dmkEdit,
  dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup, Grids, DBGrids,
  UnDmkProcFunc, UnDmkENums, dmkDBLookupComboBox, dmkEditCB;

type
  TFmOVcYnsQstTop = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    Label9: TLabel;
    EdNome: TdmkEdit;
    Label1: TLabel;
    DBEdCodigo: TdmkDBEdit;
    Label2: TLabel;
    DBEdNome: TdmkDBEdit;
    QrOVcYnsQstTop: TMySQLQuery;
    DsOVcYnsQstTop: TDataSource;
    QrUsouEm: TMySQLQuery;
    DsUsouEm: TDataSource;
    PMIts: TPopupMenu;
    ItsInclui1: TMenuItem;
    ItsExclui1: TMenuItem;
    ItsAltera1: TMenuItem;
    PMCab: TPopupMenu;
    CabInclui1: TMenuItem;
    CabAltera1: TMenuItem;
    CabExclui1: TMenuItem;
    BtCab: TBitBtn;
    BtIts: TBitBtn;
    DGDados: TDBGrid;
    QrOVcYnsQstTopNO_ContexRef: TWideStringField;
    QrOVcYnsQstTopNO_MagnitRef: TWideStringField;
    QrOVcYnsQstTopCodigo: TIntegerField;
    QrOVcYnsQstTopNome: TWideStringField;
    QrOVcYnsQstTopContexRef: TIntegerField;
    QrOVcYnsQstTopMagnitRef: TIntegerField;
    QrOVcYnsQstTopLk: TIntegerField;
    QrOVcYnsQstTopDataCad: TDateField;
    QrOVcYnsQstTopDataAlt: TDateField;
    QrOVcYnsQstTopUserCad: TIntegerField;
    QrOVcYnsQstTopUserAlt: TIntegerField;
    QrOVcYnsQstTopAlterWeb: TSmallintField;
    QrOVcYnsQstTopAWServerID: TIntegerField;
    QrOVcYnsQstTopAWStatSinc: TSmallintField;
    QrOVcYnsQstTopAtivo: TSmallintField;
    QrOVcYnsQstMag: TMySQLQuery;
    DsOVcYnsQstMag: TDataSource;
    QrOVcYnsQstCtx: TMySQLQuery;
    DsOVcYnsQstCtx: TDataSource;
    QrOVcYnsQstMagCodigo: TIntegerField;
    QrOVcYnsQstMagNome: TWideStringField;
    QrOVcYnsQstCtxCodigo: TIntegerField;
    QrOVcYnsQstCtxNome: TWideStringField;
    Label3: TLabel;
    EdContexRef: TdmkEditCB;
    CBContexRef: TdmkDBLookupComboBox;
    Label4: TLabel;
    EdMagnitRef: TdmkEditCB;
    CBMagnitRef: TdmkDBLookupComboBox;
    SbOVcYnsQstCtx: TSpeedButton;
    SbOVcYnsQstMag: TSpeedButton;
    Label5: TLabel;
    Label6: TLabel;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    DBEdit4: TDBEdit;
    QrUsouEmCO_CheckList: TIntegerField;
    QrUsouEmNO_CheckList: TWideStringField;
    QrUsouEmCO_Contexto: TIntegerField;
    QrUsouEmNO_Contexto: TWideStringField;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrOVcYnsQstTopAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrOVcYnsQstTopBeforeOpen(DataSet: TDataSet);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure QrOVcYnsQstTopAfterScroll(DataSet: TDataSet);
    procedure CabInclui1Click(Sender: TObject);
    procedure CabAltera1Click(Sender: TObject);
    procedure CabExclui1Click(Sender: TObject);
    procedure BtCabClick(Sender: TObject);
    procedure BtItsClick(Sender: TObject);
    procedure ItsInclui1Click(Sender: TObject);
    procedure ItsAltera1Click(Sender: TObject);
    procedure PMCabPopup(Sender: TObject);
    procedure PMItsPopup(Sender: TObject);
    procedure QrOVcYnsQstTopBeforeClose(DataSet: TDataSet);
    procedure SbOVcYnsQstCtxClick(Sender: TObject);
    procedure SbOVcYnsQstMagClick(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure LocCod(Atual, Codigo: Integer);
    procedure Va(Para: TVaiPara);
    procedure MostraUsouEm(SQLType: TSQLType);

  public
    { Public declarations }
    FSeq, FCabIni: Integer;
    FLocIni: Boolean;
    //
    procedure ReopenUsouEm();
    procedure ReopenOVcYnsQstCtx();
    procedure ReopenOVcYnsQstMag();

  end;

var
  FmOVcYnsQstTop: TFmOVcYnsQstTop;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, MyDBCheck, DmkDAC_PF, UnOVS_Jan;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmOVcYnsQstTop.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmOVcYnsQstTop.MostraUsouEm(SQLType: TSQLType);
begin
  //
end;

procedure TFmOVcYnsQstTop.PMCabPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemCabUpd(CabAltera1, QrOVcYnsQstTop);
  MyObjects.HabilitaMenuItemCabDel(CabExclui1, QrOVcYnsQstTop, QrUsouEm);
end;

procedure TFmOVcYnsQstTop.PMItsPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(ItsInclui1, QrOVcYnsQstTop);
  MyObjects.HabilitaMenuItemItsUpd(ItsAltera1, QrUsouEm);
  MyObjects.HabilitaMenuItemItsDel(ItsExclui1, QrUsouEm);
end;

procedure TFmOVcYnsQstTop.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrOVcYnsQstTopCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmOVcYnsQstTop.DefParams;
begin
  VAR_GOTOTABELA := 'OVcYnsQstTop';
  VAR_GOTOMYSQLTABLE := QrOVcYnsQstTop;
  VAR_GOTONEG := gotoPiZ;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT yqc.Nome NO_ContexRef, yqm.Nome NO_MagnitRef, yqt.* ');
  VAR_SQLx.Add('FROM ovcynsqsttop yqt ');
  VAR_SQLx.Add('LEFT JOIN ovcynsqstctx yqc ON yqc.Codigo=yqt.ContexRef ');
  VAR_SQLx.Add('LEFT JOIN ovcynsqstmag yqm ON yqm.Codigo=yqt.MagnitRef ');
  //
  VAR_SQL1.Add('WHERE yqt.Codigo>0');
  VAR_SQL1.Add('AND yqt.Codigo=:P0');
  //
  //VAR_SQL2.Add('AND CodUsu=:P0');
  //
  VAR_SQLa.Add('AND yqt.Nome Like :P0');
  //
end;

procedure TFmOVcYnsQstTop.ItsAltera1Click(Sender: TObject);
begin
  MostraUsouEm(stUpd);
end;

procedure TFmOVcYnsQstTop.CabExclui1Click(Sender: TObject);
begin
  Geral.MB_Info('A��o n�o implementada! Solicite � Dermatek:' + sLineBreak +
  Caption + sLineBreak + TMenuItem(Sender).Name);
end;

procedure TFmOVcYnsQstTop.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmOVcYnsQstTop.QueryPrincipalAfterOpen;
begin
end;

procedure TFmOVcYnsQstTop.ReopenOVcYnsQstCtx();
begin
  UnDMkDAC_PF.AbreQuery(QrOVcYnsQstCtx, DMod.MyDB);
end;

procedure TFmOVcYnsQstTop.ReopenOVcYnsQstMag();
begin
  UnDMkDAC_PF.AbreQuery(QrOVcYnsQstMag, DMod.MyDB);
end;

procedure TFmOVcYnsQstTop.ReopenUsouEm();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrUsouEm, Dmod.MyDB, [
  'SELECT ycc.Codigo CO_CheckList, ycc.Nome NO_CheckList,  ',
  'yqc.Codigo CO_Contexto, yqc.Nome NO_Contexto ',
  'FROM ovcynschktop yct ',
  'LEFT JOIN ovcynschkctx yck ON yck.Controle=yct.Controle ',
  'LEFT JOIN ovcynschkcad ycc ON ycc.Codigo=yct.Codigo ',
  'LEFT JOIN ovcynsqstctx yqc ON yqc.Codigo=yck.Contexto ',
  'WHERE yct.Topico=' + Geral.FF0(QrOVcYnsQstTopCodigo.Value),
  'ORDER BY NO_CheckList, NO_Contexto ',
  '']);
end;


procedure TFmOVcYnsQstTop.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmOVcYnsQstTop.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmOVcYnsQstTop.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmOVcYnsQstTop.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmOVcYnsQstTop.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmOVcYnsQstTop.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmOVcYnsQstTop.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrOVcYnsQstTopCodigo.Value;
  Close;
end;

procedure TFmOVcYnsQstTop.ItsInclui1Click(Sender: TObject);
begin
  MostraUsouEm(stIns);
end;

procedure TFmOVcYnsQstTop.CabAltera1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrOVcYnsQstTop, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'OVcYnsQstTop');
end;

procedure TFmOVcYnsQstTop.BtConfirmaClick(Sender: TObject);
var
  Nome: String;
  Codigo, ContexRef, MagnitRef: Integer;
  SQLType: TSQLType;
begin
  SQLType        := ImgTipo.SQLType;
  Codigo         := EdCodigo.ValueVariant;
  Nome           := EdNome.ValueVariant;
  ContexRef      := EdContexRef.ValueVariant;
  MagnitRef      := EdMagnitRef.ValueVariant;
  //
  if MyObjects.FIC(Length(Nome) = 0, EdNome, 'Defina uma descri��o!') then Exit;
  //
  Codigo := UMyMod.BPGS1I32('ovcynsqsttop', 'Codigo', '', '', tsPos, SQLType, Codigo);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'ovcynsqsttop', False, [
  'Nome', 'ContexRef', 'MagnitRef'], [
  'Codigo'], [
  Nome, ContexRef, MagnitRef], [
  Codigo], True) then
  begin
    ImgTipo.SQLType := stLok;
    PnDados.Visible := True;
    PnEdita.Visible := False;
    GOTOy.BotoesSb(ImgTipo.SQLType);
    //
    LocCod(Codigo, Codigo);
    if FSeq = 1 then Close;
  end;
end;

procedure TFmOVcYnsQstTop.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := EdCodigo.ValueVariant;
  if ImgTipo.SQLType = stIns then
    UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'OVcYnsQstTop', Codigo);
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'OVcYnsQstTop', 'Codigo');
end;

procedure TFmOVcYnsQstTop.BtItsClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMIts, BtIts);
end;

procedure TFmOVcYnsQstTop.BtCabClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMCab, BtCab);
end;

procedure TFmOVcYnsQstTop.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  GBEdita.Align := alClient;
  DGDados.Align := alClient;
  CriaOForm;
  FSeq := 0;
  QrOVcYnsQstTop.Database := Dmod.MyDB;
  ReopenOVcYnsQstCtx();
  ReopenOVcYnsQstMag();
end;

procedure TFmOVcYnsQstTop.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrOVcYnsQstTopCodigo.Value, LaRegistro.Caption);
end;

procedure TFmOVcYnsQstTop.SbOVcYnsQstCtxClick(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  OVS_Jan.MostraFormOVcYnsQstCtx();
  if VAR_CADASTRO <> 0 then
  begin
      UMyMod.SetaCodigoPesquisado(EdContexRef, CBContexRef, QrOVcYnsQstCtx, VAR_CADASTRO);
      EdContexRef.SetFocus;
  end else
    ReopenOVcYnsQstCtx();
end;

procedure TFmOVcYnsQstTop.SbOVcYnsQstMagClick(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  OVS_Jan.MostraFormOVcYnsQstMag();
  if VAR_CADASTRO <> 0 then
  begin
      UMyMod.SetaCodigoPesquisado(EdMagnitRef, CBMagnitRef, QrOVcYnsQstMag, VAR_CADASTRO);
      EdMagnitRef.SetFocus;
  end else
    ReopenOVcYnsQstMag();
end;

procedure TFmOVcYnsQstTop.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmOVcYnsQstTop.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrOVcYnsQstTopCodigo.Value, LaRegistro.Caption);
end;

procedure TFmOVcYnsQstTop.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmOVcYnsQstTop.QrOVcYnsQstTopAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmOVcYnsQstTop.QrOVcYnsQstTopAfterScroll(DataSet: TDataSet);
begin
  ReopenUsouEm();
end;

procedure TFmOVcYnsQstTop.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //
  if FSeq = 1 then
    CabInclui1Click(Self)
  else
  if (not FLocIni) and (FCabIni <> 0)  then
  begin
    LocCod(FCabIni, FCabIni);
    if QrOVcYnsQstTopCodigo.Value <> FCabIni then
      Geral.MB_Aviso('Grupo n�o localizado!');
    FLocIni := True;
  end;
end;

procedure TFmOVcYnsQstTop.SbQueryClick(Sender: TObject);
begin
  LocCod(QrOVcYnsQstTopCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'OVcYnsQstTop', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmOVcYnsQstTop.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmOVcYnsQstTop.CabInclui1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrOVcYnsQstTop, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'OVcYnsQstTop');
end;

procedure TFmOVcYnsQstTop.QrOVcYnsQstTopBeforeClose(
  DataSet: TDataSet);
begin
  QrUsouEm.Close;
end;

procedure TFmOVcYnsQstTop.QrOVcYnsQstTopBeforeOpen(DataSet: TDataSet);
begin
  QrOVcYnsQstTopCodigo.DisplayFormat := FFormatFloat;
end;

end.

