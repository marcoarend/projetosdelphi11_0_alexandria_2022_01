unit OVmItxDevCab;
interface

uses
  Windows, Messages, SysUtils, Classes, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2,
  ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, dmkGeral,
  dmkPermissoes, dmkEdit, dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup,
  unDmkProcFunc, UnDmkEnums, Vcl.ComCtrls, Vcl.Grids, Vcl.DBGrids,
  System.NetEncoding, System.IOUtils, Vcl.Imaging.jpeg,
  FMX.Graphics, Vcl.Graphics, System.UITypes,
  Soap.EncdDecd;

type
  TFmOVmItxDevCab = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    BtExclui: TBitBtn;
    BtAltera: TBitBtn;
    BtEmail: TBitBtn;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    QrOVmItxDevCab: TMySQLQuery;
    DsOVmItxDevCab: TDataSource;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    Label9: TLabel;
    EdNome: TdmkEdit;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    QrOVmItxDevCabCodigo: TIntegerField;
    QrOVmItxDevCabCodInMob: TIntegerField;
    QrOVmItxDevCabDeviceSI: TIntegerField;
    QrOVmItxDevCabDeviceID: TWideStringField;
    QrOVmItxDevCabDeviceCU: TIntegerField;
    QrOVmItxDevCabNome: TWideStringField;
    QrOVmItxDevCabOVgItxGer: TIntegerField;
    QrOVmItxDevCabLocal: TIntegerField;
    QrOVmItxDevCabNrOP: TIntegerField;
    QrOVmItxDevCabSeqGrupo: TIntegerField;
    QrOVmItxDevCabNrReduzidoOP: TIntegerField;
    QrOVmItxDevCabDtHrAbert: TDateTimeField;
    QrOVmItxDevCabDtHrFecha: TDateTimeField;
    QrOVmItxDevCabOVcYnsExg: TIntegerField;
    QrOVmItxDevCabPecasItx: TIntegerField;
    QrOVmItxDevCabPecaAtual: TIntegerField;
    QrOVmItxDevCabLk: TIntegerField;
    QrOVmItxDevCabDataCad: TDateField;
    QrOVmItxDevCabDataAlt: TDateField;
    QrOVmItxDevCabUserCad: TIntegerField;
    QrOVmItxDevCabUserAlt: TIntegerField;
    QrOVmItxDevCabAlterWeb: TSmallintField;
    QrOVmItxDevCabAWServerID: TIntegerField;
    QrOVmItxDevCabAWStatSinc: TSmallintField;
    QrOVmItxDevCabAtivo: TSmallintField;
    QrOVmItxDevCabQtReal: TFloatField;
    QrOVmItxDevCabQtLocal: TFloatField;
    QrOVmItxDevCabProduto: TIntegerField;
    QrOVmItxDevCabCodGrade: TIntegerField;
    QrOVmItxDevCabCodTam: TWideStringField;
    QrOVmItxDevCabPontosTot: TIntegerField;
    QrOVmItxDevCabInspResul: TIntegerField;
    QrOVmItxDevCabInspeSeq: TIntegerField;
    QrOVmItxDevCabRandmStr: TWideStringField;
    QrOVmItxDevCabDtHrUpIni: TDateTimeField;
    QrOVmItxDevCabDtHrUpFim: TDateTimeField;
    QrOVmItxDevCabEmpresa: TIntegerField;
    QrOVmItxDevCabNO_OVcYnsExg: TWideStringField;
    QrOVmItxDevCabNO_Local: TWideStringField;
    QrOVmItxDevCabNO_Referencia: TWideStringField;
    QrOVmItxDevCabNO_InspResul: TWideStringField;
    QrOVmItxDevCabDtHrAbert_TXT: TWideStringField;
    QrOVmItxDevCabDtHrFecha_TXT: TWideStringField;
    GBDados: TGroupBox;
    Panel6: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Label17: TLabel;
    Label18: TLabel;
    Label19: TLabel;
    DBEdCodigo: TdmkDBEdit;
    DBEdNome: TdmkDBEdit;
    dmkDBEdit1: TdmkDBEdit;
    dmkDBEdit2: TdmkDBEdit;
    dmkDBEdit5: TdmkDBEdit;
    dmkDBEdit6: TdmkDBEdit;
    Label3: TLabel;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    Label4: TLabel;
    Label6: TLabel;
    DBEdit4: TDBEdit;
    DBEdit5: TDBEdit;
    Label8: TLabel;
    Label5: TLabel;
    DBEdit3: TDBEdit;
    DBEdit6: TDBEdit;
    Label10: TLabel;
    Label11: TLabel;
    DBEdit7: TDBEdit;
    DBEdit9: TDBEdit;
    PGDados: TPageControl;
    TabSheet1: TTabSheet;
    QrPontosNeg: TMySQLQuery;
    QrPontosNegPecaSeq: TIntegerField;
    QrPontosNegNO_Topico: TWideStringField;
    QrPontosNegPontNeg: TIntegerField;
    DsPontosNeg: TDataSource;
    DBGrid1: TDBGrid;
    Label13: TLabel;
    DBEdit13: TDBEdit;
    DBEdit14: TDBEdit;
    Label15: TLabel;
    QrPontosNegIdTabela: TWideStringField;
    QrPontosNegTabela: TWideStringField;
    Label21: TLabel;
    DBEdit15: TDBEdit;
    DBEdit16: TDBEdit;
    Label23: TLabel;
    DBEdit17: TDBEdit;
    Label22: TLabel;
    DBEdit18: TDBEdit;
    Label24: TLabel;
    DBEdit19: TDBEdit;
    Label25: TLabel;
    DBEdit20: TDBEdit;
    Label26: TLabel;
    DBEdit21: TDBEdit;
    Label27: TLabel;
    DBEdit22: TDBEdit;
    Label28: TLabel;
    DBEdit23: TDBEdit;
    Label29: TLabel;
    DBEdit24: TDBEdit;
    Label30: TLabel;
    DBEdit25: TDBEdit;
    QrPontosNegQtdFotos: TIntegerField;
    Panel7: TPanel;
    DBGrid2: TDBGrid;
    Label31: TLabel;
    QrPontosNegCtrlInMob: TIntegerField;
    QrOVmItxDevFts: TMySQLQuery;
    DsOVmItxDevFts: TDataSource;
    Panel8: TPanel;
    ImgFoto: TImage;
    LaNomeFoto: TLabel;
    QrOVmItxDevBmp: TMySQLQuery;
    QrOVmItxDevBmpTxtBmp: TWideMemoField;
    QrOVmItxDevCabUserNmePdr: TWideStringField;
    QrOVmItxDevCabPerExgReal: TFloatField;
    QrOVmItxDevCabDtHrMailSnt: TFloatField;
    Label32: TLabel;
    DBEdit26: TDBEdit;
    Label33: TLabel;
    DBEdit27: TDBEdit;
    QrOVmItxDevFtsCodigo: TIntegerField;
    QrOVmItxDevFtsIdTabela: TSmallintField;
    QrOVmItxDevFtsCodInMob: TIntegerField;
    QrOVmItxDevFtsCtrlInMob: TIntegerField;
    QrOVmItxDevFtsDataHora: TDateTimeField;
    QrOVmItxDevFtsNomeArq: TWideStringField;
    QrOVmItxDevFtsNoArqSvr: TWideStringField;
    QrOVmItxDevFtsLk: TIntegerField;
    QrOVmItxDevFtsDataCad: TDateField;
    QrOVmItxDevFtsDataAlt: TDateField;
    QrOVmItxDevFtsUserCad: TIntegerField;
    QrOVmItxDevFtsUserAlt: TIntegerField;
    QrOVmItxDevFtsAlterWeb: TSmallintField;
    QrOVmItxDevFtsAWServerID: TIntegerField;
    QrOVmItxDevFtsAWStatSinc: TSmallintField;
    QrOVmItxDevFtsAtivo: TSmallintField;
    QrOVmItxDevCabSeccaoOP: TWideStringField;
    QrOVmItxDevCabSeccaoMaq: TWideStringField;
    QrOVmItxDevCabSegmntInsp: TIntegerField;
    QrOVmItxDevCabSeccaoInsp: TIntegerField;
    QrOVmItxDevCabBatelada: TIntegerField;
    Label12: TLabel;
    DBEdit8: TDBEdit;
    Label14: TLabel;
    DBEdit10: TDBEdit;
    Label16: TLabel;
    DBEdit11: TDBEdit;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrOVmItxDevCabAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrOVmItxDevCabBeforeOpen(DataSet: TDataSet);
    procedure BtEmailClick(Sender: TObject);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure QrOVmItxDevCabAfterScroll(DataSet: TDataSet);
    procedure QrOVmItxDevCabBeforeClose(DataSet: TDataSet);
    procedure QrPontosNegAfterScroll(DataSet: TDataSet);
    procedure QrOVmItxDevFtsAfterScroll(DataSet: TDataSet);
    procedure Button1Click(Sender: TObject);
    procedure QrPontosNegBeforeClose(DataSet: TDataSet);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    //
    procedure ReopenPontosNeg();
    procedure ReopenOVmItxDevFts();
    //procedure BitmapFromBase64(Base64: string; Bitmap: TBitmap);
    function ConvertFmxBitmapToVclBitmap(b: FMX.Graphics.TBitmap): Vcl.Graphics.TBitmap;
  public
    { Public declarations }
    procedure LocCod(Atual, Codigo: Integer);

  end;

var
  FmOVmItxDevCab: TFmOVmItxDevCab;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, DmkDAC_PF, UnOVS_Consts, UnREST_App, UnProjGroupEnums,
  MyDBCheck, OVmItxDevPsq, UnOVS_ProjGroupVars, UnOVS_PF;

{$R *.DFM}

function BitmapFromBase64(const base64: string):FMX.Graphics.TBitmap;
var
  Input: TStringStream;
  Output: TBytesStream;
begin
  Input := TStringStream.Create(base64, TEncoding.ASCII);
  try
    Output := TBytesStream.Create;
    try
      Soap.EncdDecd.DecodeStream(Input, Output);
      Output.Position := 0;
      Result := FMX.Graphics.TBitmap.Create;
      try
        Result.LoadFromStream(Output);
      except
        Result.Free;
        raise;
      end;
    finally
      Output.Free;
    end;
  finally
    Input.Free;
  end;
end;


/////////////////////////////////////////////////////////////////////////////////////
procedure TFmOVmItxDevCab.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmOVmItxDevCab.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrOVmItxDevCabCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmOVmItxDevCab.DefParams;
begin
  VAR_GOTOTABELA := 'ovmitxdevcab';
  VAR_GOTOMYSQLTABLE := QrOVmItxDevCab;
  VAR_GOTONEG := gotoPos;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT igc.*, ymc.Nome NO_OVcYnsExg, ');
  VAR_SQLx.Add('dlo.Nome NO_Local, ref.Nome NO_Referencia,');
  VAR_SQLx.Add('isc.Nome NO_InspResul, ');
  VAR_SQLx.Add('IF(igc.DtHrAbert  <= "1899-12-30", "",');
  VAR_SQLx.Add('  DATE_FORMAT(igc.DtHrAbert, "%d/%m/%Y %H:%i:%s")) DtHrAbert_TXT,');
  VAR_SQLx.Add('IF(igc.DtHrFecha  <= "1899-12-30", "",');
  VAR_SQLx.Add('  DATE_FORMAT(igc.DtHrFecha, "%d/%m/%Y %H:%i:%s")) DtHrFecha_TXT,');
  VAR_SQLx.Add('mdc.UserNmePdr ');
  VAR_SQLx.Add('FROM ovmitxdevcab igc');
  VAR_SQLx.Add('LEFT JOIN ovdlocal dlo ON dlo.Codigo=igc.Local');
  VAR_SQLx.Add('LEFT JOIN ovdreferencia ref ON ref.Codigo=igc.SeqGrupo');
  VAR_SQLx.Add('LEFT JOIN ovcynsexgcad ymc ON ymc.Codigo=igc.OVcYnsExg');
  VAR_SQLx.Add('LEFT JOIN ovgisprescad isc ON isc.Codigo=igc.InspResul');
  VAR_SQLx.Add('LEFT JOIN ovcmobdevcad mdc ON mdc.Codigo=igc.DeviceSI');
  VAR_SQLx.Add('WHERE igc.Codigo > 0');
  //
  VAR_SQL1.Add('AND igc.Codigo=:P0');
  //
  //VAR_SQL2.Add('AND igc.CodUsu=:P0');
  //
  VAR_SQLa.Add('AND igc.Nome Like :P0');
  //

  VAR_SQLx.Add('');
end;

function TFmOVmItxDevCab.ConvertFmxBitmapToVclBitmap(
  b: FMX.Graphics.TBitmap): Vcl.Graphics.TBitmap;
var
  data:FMX.Graphics.TBitmapData;
  i,j:Integer;
  AlphaColor: TAlphaColor;
begin
  Result := VCL.Graphics.TBitmap.Create;
  Result.SetSize(b.Width, b.Height);
  if(b.Map(TMapAccess.Readwrite, data)) then
  try
    for i := 0 to data.Height-1 do
    begin
      for j := 0 to data.Width-1 do
      begin
        AlphaColor:=data.GetPixel(j,i);
        Result.Canvas.Pixels[j,i]:=
          RGB(
            TAlphaColorRec(AlphaColor).R,
            TAlphaColorRec(AlphaColor).G,
            TAlphaColorRec(AlphaColor).B
          );
      end;
    end;
  finally
    b.Unmap(data);
  end;
end;

procedure TFmOVmItxDevCab.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmOVmItxDevCab.QueryPrincipalAfterOpen;
begin
end;

procedure TFmOVmItxDevCab.ReopenOVmItxDevFts();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrOVmItxDevFts, Dmod.MyDB, [
  'SELECT * ',
  'FROM ovmitxdevfts ',
  'WHERE Codigo=' + Geral.FF0(QrOVmItxDevCabCodigo.Value),
  'AND CtrlInMob=' + Geral.FF0(QrPontosNegCtrlInMob.Value),
  'AND IdTabela="' + QrPontosNegIdTabela.Value + '"',
  EmptyStr]);
end;

procedure TFmOVmItxDevCab.ReopenPontosNeg();
var
  sCodigo: String;
begin
  sCodigo := Geral.FF0(QrOVmItxDevCabCodigo.Value);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrPontosNeg, Dmod.MyDB, [
  'SELECT  "3" IdTabela, "' + CO_MEM_FLD_TAB_3_Textil_Exigencia + '" Tabela,  ',
  'mmc.CtrlInMob AS CtrlInMob, mmc.PecaSeq AS PecaSeq, ',
  'yqt.Nome NO_Topico, mmc.PontNeg AS PontNeg, ',
  'mmc.QtdFotos AS QtdFotos, ',
  'mmc.Magnitude AS Magnitude ',
  'FROM ovmitxdevexg mmc ',
  'LEFT JOIN ovcynsmixtop yqt ON yqt.Codigo=mmc.Topyko ',
  'WHERE mmc.Codigo=' + sCodigo,
  ' ',
  'UNION ',
  ' ',
  'SELECT  "4" IdTabela, "' + CO_MEM_FLD_TAB_4_Textil_LivreTexto + '" Tabela,   ',
  'mml.CtrlInMob AS CtrlInMob, mml.PecaSeq AS PecaSeq,  ',
  'mml.Descricao NO_Topico, mml.PontNeg AS PontNeg, ',
  'mml.QtdFotos AS QtdFotos, ',
  'mml.Magnitude AS Magnitude',
  'FROM ovmitxdevlvr mml  ',
  'WHERE mml.Codigo=' + sCodigo,
  '  ',
  'ORDER BY PecaSeq DESC, IdTabela, CtrlInMob ',
  EmptyStr]);
  //
  //Geral.MB_SQL(Self, QrPontosNeg);
end;

procedure TFmOVmItxDevCab.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARAÇÕES///////////////////

procedure TFmOVmItxDevCab.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmOVmItxDevCab.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmOVmItxDevCab.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmOVmItxDevCab.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmOVmItxDevCab.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmOVmItxDevCab.Button1Click(Sender: TObject);

{
function Base64FromBitmap(Bitmap: TBitmap): string;
var
  Stream: TBytesStream;
  Encoding: TBase64Encoding;
begin
  Stream := TBytesStream.Create;
  try
    Bitmap.SaveToStream(Stream);
    Encoding := TBase64Encoding.Create(0);
    try
      Result := Encoding.EncodeBytesToString(Copy(Stream.Bytes, 0, Stream.Size));
    finally
      Encoding.Free;
    end;
  finally
    Stream.Free;
  end;
end;
}
{
  procedure BitmapFromBase64(Base64: string; Bitmap: TBitmap);
  var
    Input: TStringStream;
    Output: TBytesStream;
    Encoding: TBase64Encoding;
  begin
    Input := TStringStream.Create(Base64, TEncoding.ASCII);
    try
      Output := TBytesStream.Create;
      try
        Encoding := TBase64Encoding.Create(0);
        try
          Encoding.Decode(Input, Output);
          Output.Position := 0;
          Bitmap.LoadFromStream(Output);
        finally
          Encoding.Free;
        end;
      finally
        Output.Free;
      end;
    finally
      Input.Free;
    end;
  end;
}

function Base64FromBitmap(Bitmap: TBitmap): string;
var
  Input: TBytesStream;
  Output: TStringStream;
begin
  Input := TBytesStream.Create;
  try
    Bitmap.SaveToStream(Input);
    Input.Position := 0;
    Output := TStringStream.Create('', TEncoding.ASCII);
    try
      Soap.EncdDecd.EncodeStream(Input, Output);
      Result := Output.DataString;
    finally
      Output.Free;
    end;
  finally
    Input.Free;
  end;
end;

(*
  function Base64FromBitmap(Bitmap: TBitmap): string;
  var
    Input: TBytesStream;
    Output: TStringStream;
    Encoding: TBase64Encoding;
  begin
    Input := TBytesStream.Create;
    try
      Bitmap.SaveToStream(Input);
      Input.Position := 0;
      Output := TStringStream.Create('', TEncoding.ASCII);
      try
        Encoding := TBase64Encoding.Create(0);
        try
          Encoding.Encode(Input, Output);
          Result := Output.DataString;
        finally
          Encoding.Free;
        end;
      finally
        Output.Free;
      end;
    finally
      Input.Free;
    end;
  end;
*)

{
  procedure DuplicaImagem();
  var
    Bitmap: TBitmap;
    sBase64, Str1, str2: String;
  begin
    Bitmap := TBitmap.Create;
    try
      //BitMap := Image1.Picture.Bitmap;


    Bitmap.Width  := Image1.Width;
    Bitmap.Height := Image1.Height;
    Bitmap.PixelFormat := pf32bit;
    Bitmap.Assign(Image1.Picture.Bitmap);
    Bitmap.PixelFormat := TPixelFormat.pf32bit;


      sBase64 := Base64FromBitmap(Bitmap);
      ShowMessage(IntToStr(Length(sBase64)));
  //    ShowMessage(sBase64);
      Str1 := Copy(sBase64, 1, 1000);
      Str2 := Copy(sBase64, 1001);
     // BitmapFromBase64(str1 + str2, Image1.Picture.Bitmap);
     Image2.Picture.Bitmap := BitmapFromBase64(str1 + str2);
    finally
      //Bitmap.Free;
    end;
  end;
}
begin
  //DuplicaImagem();
end;

(*
procedure TFmOVmItxDevCab.BitmapFromBase64(Base64: string; Bitmap: TBitmap);
{
var
  Input: TStringStream;
  Output: TBytesStream;
  Encoding: TBase64Encoding;
begin
  //Input := TStringStream.Create(Base64, TEncoding.ASCII);
  Input := TStringStream.Create(Base64, TEncoding.UTF8);
  try
    Output := TBytesStream.Create;
    try
      Encoding := TBase64Encoding.Create(0);
      try
        Encoding.Decode(Input, Output);
        Output.Position := 0;
        Bitmap.LoadFromStream(Output);
      finally
        Encoding.Free;
      end;
    finally
      Output.Free;
    end;
  finally
    Input.Free;
  end;
}
var
  Stream: TBytesStream;
  Bytes: TBytes;
  Encoding: TBase64Encoding;
begin
  Stream := TBytesStream.Create;
  try
    Encoding := TBase64Encoding.Create(0);
    try
      Bytes := Encoding.DecodeStringToBytes(Base64);
      Stream.WriteData(Bytes, Length(Bytes));
      Stream.Position := 0;
      Bitmap.LoadFromStream(Stream);
    finally
      Encoding.Free;
    end;
  finally
    Stream.Free;
  end;
end;
*)

procedure TFmOVmItxDevCab.BtAlteraClick(Sender: TObject);
begin
  if (QrOVmItxDevCab.State <> dsInactive) and (QrOVmItxDevCab.RecordCount > 0) then
  begin
    UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrOVmItxDevCab, [PnDados],
      [PnEdita], EdNome, ImgTipo, 'ovmtxdevcab');
  end;
end;

procedure TFmOVmItxDevCab.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrOVmItxDevCabCodigo.Value;
  Close;
end;

procedure TFmOVmItxDevCab.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := EdCodigo.ValueVariant;
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  //
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'ovmitxdevcab', 'Codigo');
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmOVmItxDevCab.BtEmailClick(Sender: TObject);
(*
var
  Inspecao: Integer;
  Retorno: String;
*)
begin
(*
  Inspecao := QrOVmItxDevCabCodigo.Value;
  REST_App.EnviaEmailResultadoInspecao(Inspecao,
  'marco@dermatek.com.br;arendemilly@gmail.com', TTipoEnvioEmailInspecao.smerAll,
  Retorno);
  //'marco@dermatek.com.br;arendemilly@gmail.com');
  //'marco@dermatek.com.br');
  //'');
*)
end;

procedure TFmOVmItxDevCab.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  GBEdita.Align := alClient;
  PGDados.Align := alClient;
  CriaOForm;
end;

procedure TFmOVmItxDevCab.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrOVmItxDevCabCodigo.Value, LaRegistro.Caption);
end;

procedure TFmOVmItxDevCab.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmOVmItxDevCab.SbNovoClick(Sender: TObject);
var
  Codigo: Integer;
begin
  //LaRegistro.Caption := GOTOy.CodUsu(QrOVmItxDevCabCodigo.Value, LaRegistro.Caption);
  Codigo := 0;
  if DBCheck.CriaFm(TFmOVmItxDevPsq, FmOVmItxDevPsq, afmoNegarComAviso) then
  begin
    FmOVmItxDevPsq.ShowModal;
    if FmOVmItxDevPsq.FOCmItxDevCab <> 0 then
      Codigo := FmOVmItxDevPsq.FOCmItxDevCab;
    FmOVmItxDevPsq.Destroy;
    //
    if Codigo <> 0 then
      LocCod(Codigo, Codigo);
  end;
end;

procedure TFmOVmItxDevCab.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmOVmItxDevCab.QrOVmItxDevCabAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmOVmItxDevCab.QrOVmItxDevCabAfterScroll(DataSet: TDataSet);
begin
  ReopenPontosNeg();
end;

procedure TFmOVmItxDevCab.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmOVmItxDevCab.SbQueryClick(Sender: TObject);
begin
  LocCod(QrOVmItxDevCabCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'ovmitxdevcab', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmOVmItxDevCab.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmOVmItxDevCab.QrOVmItxDevCabBeforeClose(DataSet: TDataSet);
begin
  QrPontosNeg.Close;
end;

procedure TFmOVmItxDevCab.QrOVmItxDevCabBeforeOpen(DataSet: TDataSet);
begin
  QrOVmItxDevCabCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmOVmItxDevCab.QrOVmItxDevFtsAfterScroll(DataSet: TDataSet);
(*
function Decode(const Input: AnsiString): string;
var
  bytes: TBytes;
  utf8: UTF8String;
begin
  bytes := EncdDecd.DecodeBase64(Input);
  SetLength(utf8, Length(bytes));
  Move(Pointer(bytes)^, Pointer(utf8)^, Length(bytes));
  Result := string(utf8);
end;

function Base64ToBitmap(const S: string): TBitmap;
var
SS: TStringStream;
V: string;
begin
V := Decode(S);
SS := TStringStream.Create(V);
try
Result := TBitmap.Create;
Result.LoadFromStream(SS);
finally
SS.Free;
end;
end;
*)

var
  NomeArq, NoArqSvr, Base64: AnsiString;
  //Bitmap: TBitmap;
  FMX_Bitmap: FMX.Graphics.TBitmap;
  VCL_Bitmap: VCL.Graphics.TBitmap;
  Codigo, IdTabela, CtrlInMob: Integer;
  DataHora: String;
  CriarArqLocal: Boolean;
begin
//EXIT;
  CriarArqLocal := False;
  NomeArq := CO_DIR_FOTOS_INSPECOES + QrOVmItxDevFtsNoArqSvr.Value;
  if QrOVmItxDevFtsNoArqSvr.Value <> EmptyStr then
  begin
    if FileExists(NomeArq) then
      ImgFoto.Picture.Bitmap.LoadFromFile(NomeArq)
    else
      CriarArqLocal := True;
  end else
    CriarArqLocal := True;
  if CriarArqLocal then
  begin
    if not DirectoryExists(CO_DIR_FOTOS_INSPECOES) then
      ForceDirectories(CO_DIR_FOTOS_INSPECOES);
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrOVmItxDevBmp, Dmod.MyDB, [
    'SELECT TxtBmp ',
    'FROM ovmitxdevbmp ',
    'WHERE Codigo=' + Geral.FF0(QrOVmItxDevFtsCodigo.Value),
    'AND IdTabela=' + Geral.FF0(QrOVmItxDevFtsIdTabela.Value),
    'AND CtrlInMob=' + Geral.FF0(QrOVmItxDevFtsCtrlInMob.Value),
    'AND DataHora="' + Geral.FDT(QrOVmItxDevFtsDataHora.Value, 109) + '"',
    'ORDER BY Pedaco',
    EmptyStr]);
    //
    Base64 := '';
    QrOVmItxDevBmp.First;
    while not QrOVmItxDevBmp.Eof do
    begin
      Base64 := Base64 + QrOVmItxDevBmpTxtBmp.Value;
      //
      QrOVmItxDevBmp.Next;
    end;
    if Base64 <> EmptyStr then
    begin
      FMX_BitMap := FMX.Graphics.TBitmap.Create;
      VCL_BitMap := VCL.Graphics.TBitmap.Create;
      //
      FMX_Bitmap := BitmapFromBase64(Base64);
      //Converter aqui de FMX para VCL!
      VCL_BitMap := ConvertFmxBitmapToVclBitmap(FMX_Bitmap);
      ImgFoto.Picture.Bitmap.Assign(VCL_Bitmap);
      Codigo    := QrOVmItxDevFtsCodigo.Value;
      IdTabela  := QrOVmItxDevFtsIDTabela.Value;
      CtrlInMob := QrOVmItxDevFtsCtrlInMob.Value;
      DataHora  := Geral.FDT(QrOVmItxDevFtsDataHora.Value, 109);
      NoArqSvr  := OVS_PF.DefineNomeArquivoFotoInconformidade(Codigo, CtrlInMob,
        IDTabela, QrOVmItxDevFtsDataHora.Value);
      NomeArq := CO_DIR_FOTOS_INSPECOES + NoArqSvr;
      ImgFoto.Picture.Bitmap.SaveToFile(NomeArq);
      if FileExists(NomeArq) and (QrOVmItxDevFtsNoArqSvr.Value = EmptyStr) then
      begin
        if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'ovmitxdevfts', False, [
        'NoArqSvr'], [
        'Codigo', 'IdTabela', 'CtrlInMob', 'DataHora'], [
        NoArqSvr], [
        Codigo, IdTabela, CtrlInMob, DataHora], True) then
        begin
          // ???? Excluir dados da tabela de ovmitxdevbmp?????
        end;
      end;
    end;
  end;
end;

procedure TFmOVmItxDevCab.QrPontosNegAfterScroll(DataSet: TDataSet);
begin
  if ImgFoto.Picture <> nil then
    ImgFoto.Picture := nil;
  ReopenOVmItxDevFts();
end;
procedure TFmOVmItxDevCab.QrPontosNegBeforeClose(DataSet: TDataSet);
begin
  if ImgFoto.Picture <> nil then
    ImgFoto.Picture := nil;
  QrOVmItxDevFts.Close;
end;

{
/  Blob Field
https://www.devmedia.com.br/armazenando-imagens-no-mysql/32104
//
/ FMX.Bitmap to Vcl.Bitmap
https://stackoverflow.com/questions/36823042/how-can-convert-fmx-graphics-tbitmap-to-vcl-graphics-tbitmap-or-vcl-imaging-pngi
function
    ConvertFmxBitmapToVclBitmap(b:FMX.Graphics.TBitmap):Vcl.Graphics.TBitmap;
var
  data:FMX.Graphics.TBitmapData;
  i,j:Integer;
  AlphaColor:TAlphaColor;
begin
  Result:=VCL.Graphics.TBitmap.Create;
  Result.SetSize(b.Width,b.Height);
  if(b.Map(TMapAccess.Readwrite,data))then
  try
    for i := 0 to data.Height-1 do begin
      for j := 0 to data.Width-1 do begin
        AlphaColor:=data.GetPixel(j,i);
        Result.Canvas.Pixels[j,i]:=
          RGB(
            TAlphaColorRec(AlphaColor).R,
            TAlphaColorRec(AlphaColor).G,
            TAlphaColorRec(AlphaColor).B);
      end;
    end;
  finally
    b.Unmap(data);
  end;
end;
}
end.

