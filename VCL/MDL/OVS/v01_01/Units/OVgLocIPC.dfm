object FmOVgLocIPC: TFmOVgLocIPC
  Left = 339
  Top = 185
  Caption = 'OVS-INSPE-008 :: Localizar Perfil de Inspe'#231#227'o de Fac'#231#227'o'
  ClientHeight = 629
  ClientWidth = 784
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 784
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 736
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 688
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 469
        Height = 32
        Caption = 'Localizar Perfil de Inspe'#231#227'o de Fac'#231#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 469
        Height = 32
        Caption = 'Localizar Perfil de Inspe'#231#227'o de Fac'#231#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 469
        Height = 32
        Caption = 'Localizar Perfil de Inspe'#231#227'o de Fac'#231#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 784
    Height = 467
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 784
      Height = 467
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 784
        Height = 467
        Align = alClient
        TabOrder = 0
        object GroupBox2: TGroupBox
          Left = 2
          Top = 15
          Width = 780
          Height = 126
          Align = alTop
          Caption = ' Filtros de pesquisa: '
          TabOrder = 0
          object Panel5: TPanel
            Left = 2
            Top = 15
            Width = 776
            Height = 109
            Align = alClient
            BevelOuter = bvNone
            TabOrder = 0
            object Label1: TLabel
              Left = 4
              Top = 4
              Width = 30
              Height = 13
              Caption = 'Artigo:'
            end
            object Label2: TLabel
              Left = 64
              Top = 4
              Width = 55
              Height = 13
              Caption = 'Refer'#234'ncia:'
            end
            object EdArtigo: TdmkEditCB
              Left = 4
              Top = 20
              Width = 56
              Height = 21
              Alignment = taRightJustify
              TabOrder = 0
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              QryCampo = 'Topico'
              UpdCampo = 'Topico'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              OnChange = EdArtigoChange
              OnKeyDown = EdArtigoKeyDown
              DBLookupComboBox = CBArtigo
              IgnoraDBLookupComboBox = False
              AutoSetIfOnlyOneReg = setregOnlyManual
            end
            object CBArtigo: TdmkDBLookupComboBox
              Left = 168
              Top = 20
              Width = 605
              Height = 21
              KeyField = 'Codigo'
              ListField = 'Nome'
              ListSource = DsOVdReferencia
              TabOrder = 1
              OnKeyDown = EdArtigoKeyDown
              dmkEditCB = EdArtigo
              UpdType = utYes
              LocF7SQLMasc = '$#'
            end
            object EdReferencia: TdmkEdit
              Left = 64
              Top = 20
              Width = 101
              Height = 21
              TabOrder = 2
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
              OnChange = EdReferenciaChange
              OnExit = EdReferenciaExit
              OnKeyDown = EdReferenciaKeyDown
            end
            object RGAtivo: TRadioGroup
              Left = 4
              Top = 44
              Width = 769
              Height = 57
              Caption = ' Status do perfil: '
              Columns = 3
              ItemIndex = 1
              Items.Strings = (
                'Inativo'
                'Ativo'
                'Ambos')
              TabOrder = 3
            end
          end
        end
        object DBGrid1: TdmkDBGridZTO
          Left = 2
          Top = 141
          Width = 780
          Height = 324
          Align = alClient
          DataSource = DsOVgIspPrfCab
          Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
          TabOrder = 1
          TitleFont.Charset = ANSI_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          RowColors = <>
          OnDblClick = DBGrid1DblClick
          Columns = <
            item
              Expanded = False
              FieldName = 'Codigo'
              Title.Caption = 'C'#243'digo'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'SeqGrupo'
              Title.Caption = 'Artigo'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Referencia'
              Title.Caption = 'Refer'#234'ncia'
              Width = 253
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_Referencia'
              Title.Caption = 'Nome artigo'
              Width = 340
              Visible = True
            end>
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 515
    Width = 784
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 780
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 559
    Width = 784
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 638
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 636
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 432
    Top = 11
  end
  object QrOVdReferencia: TMySQLQuery
    SQL.Strings = (
      'SELECT Codigo, Referencia, Nome '
      'FROM ovdreferencia '
      'ORDER BY Nome ')
    Left = 148
    Top = 246
    object QrOVdReferenciaCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrOVdReferenciaNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
  object DsOVdReferencia: TDataSource
    DataSet = QrOVdReferencia
    Left = 148
    Top = 294
  end
  object QrOVgIspPrfCab: TMySQLQuery
    SQL.Strings = (
      'SELECT igc.*, ymc.Nome NO_OVcYnsMed, ycc.Nome NO_OVcYnsChk,'
      'dlo.Nome NO_Local, ref.Nome NO_Referencia, '
      'isc.Nome NO_ZtatusIsp, '
      'IF(igc.DtHrAbert  <= "1899-12-30", "",  '
      '  DATE_FORMAT(igc.DtHrAbert, "%d/%m/%Y %h:%i")) DtHrAbert_TXT,  '
      'IF(igc.DtHrFecha  <= "1899-12-30", "",  '
      '  DATE_FORMAT(igc.DtHrFecha, "%d/%m/%Y %h:%i")) DtHrFecha_TXT'
      'FROM ovgispgercab igc'
      'LEFT JOIN ovdlocal dlo ON dlo.Codigo=igc.Local  '
      'LEFT JOIN ovdreferencia ref ON ref.Codigo=igc.SeqGrupo '
      'LEFT JOIN ovcynsmedcad ymc ON ymc.Codigo=igc.OVcYnsMed'
      'LEFT JOIN ovcynschkcad ycc ON ycc.Codigo=igc.OVcYnsChk'
      'LEFT JOIN OVgIspStaCad isc ON isc.Codigo=igc.ZtatusIsp'
      ''
      'WHERE igc.Codigo > 0')
    Left = 44
    Top = 245
    object QrOVgIspPrfCabCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrOVgIspPrfCabNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QrOVgIspPrfCabSeqGrupo: TIntegerField
      FieldName = 'SeqGrupo'
      Required = True
    end
    object QrOVgIspPrfCabOVcYnsMed: TIntegerField
      FieldName = 'OVcYnsMed'
      Required = True
    end
    object QrOVgIspPrfCabOVcYnsChk: TIntegerField
      FieldName = 'OVcYnsChk'
      Required = True
    end
    object QrOVgIspPrfCabLimiteChk: TIntegerField
      FieldName = 'LimiteChk'
      Required = True
    end
    object QrOVgIspPrfCabLimiteMed: TIntegerField
      FieldName = 'LimiteMed'
      Required = True
    end
    object QrOVgIspPrfCabAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
    object QrOVgIspPrfCabNO_Referencia: TWideStringField
      FieldName = 'NO_Referencia'
      Size = 100
    end
    object QrOVgIspPrfCabNO_OVcYnsMed: TWideStringField
      FieldName = 'NO_OVcYnsMed'
      Size = 60
    end
    object QrOVgIspPrfCabNO_OVcYnsChk: TWideStringField
      FieldName = 'NO_OVcYnsChk'
      Size = 60
    end
    object QrOVgIspPrfCabOVcYnsARQ: TIntegerField
      FieldName = 'OVcYnsARQ'
    end
  end
  object DsOVgIspPrfCab: TDataSource
    DataSet = QrOVgIspPrfCab
    Left = 44
    Top = 293
  end
end
