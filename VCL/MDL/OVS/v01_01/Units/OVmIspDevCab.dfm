object FmOVmIspDevCab: TFmOVmIspDevCab
  Left = 368
  Top = 194
  Caption = 'OVS-INSPE-004 :: Inspe'#231#245'es Realizadas em Fac'#231#245'es'
  ClientHeight = 659
  ClientWidth = 1007
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnEdita: TPanel
    Left = 0
    Top = 96
    Width = 1007
    Height = 563
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    Visible = False
    object GBEdita: TGroupBox
      Left = 0
      Top = 0
      Width = 1007
      Height = 125
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 0
      object Label7: TLabel
        Left = 16
        Top = 16
        Width = 14
        Height = 13
        Caption = 'ID:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label9: TLabel
        Left = 76
        Top = 16
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
        Color = clBtnFace
        ParentColor = False
      end
      object EdCodigo: TdmkEdit
        Left = 16
        Top = 32
        Width = 56
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Codigo'
        UpdCampo = 'Codigo'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdNome: TdmkEdit
        Left = 76
        Top = 32
        Width = 692
        Height = 21
        TabOrder = 1
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Nome'
        UpdCampo = 'Nome'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
    end
    object GBConfirma: TGroupBox
      Left = 0
      Top = 500
      Width = 1007
      Height = 63
      Align = alBottom
      TabOrder = 1
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 12
        Top = 17
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
      end
      object Panel1: TPanel
        Left = 868
        Top = 15
        Width = 137
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 7
          Top = 2
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
    end
  end
  object PnDados: TPanel
    Left = 0
    Top = 96
    Width = 1007
    Height = 563
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    object GBCntrl: TGroupBox
      Left = 0
      Top = 499
      Width = 1007
      Height = 64
      Align = alBottom
      TabOrder = 0
      object Panel5: TPanel
        Left = 2
        Top = 15
        Width = 172
        Height = 47
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 174
        Top = 15
        Width = 311
        Height = 47
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel3: TPanel
        Left = 485
        Top = 15
        Width = 520
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object BtExclui: TBitBtn
          Tag = 12
          Left = 252
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Exclui'
          Enabled = False
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          Visible = False
        end
        object BtAltera: TBitBtn
          Tag = 11
          Left = 128
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Altera'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          Visible = False
          OnClick = BtAlteraClick
        end
        object BtEmail: TBitBtn
          Tag = 10
          Left = 4
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Email'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          Visible = False
          OnClick = BtEmailClick
        end
        object Panel2: TPanel
          Left = 387
          Top = 0
          Width = 133
          Height = 47
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 3
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
      end
    end
    object GBDados: TGroupBox
      Left = 0
      Top = 0
      Width = 1007
      Height = 221
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 1
      object Panel6: TPanel
        Left = 2
        Top = 15
        Width = 1003
        Height = 204
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object Label1: TLabel
          Left = 8
          Top = 0
          Width = 36
          Height = 13
          Caption = 'C'#243'digo:'
          FocusControl = DBEdCodigo
        end
        object Label2: TLabel
          Left = 68
          Top = 0
          Width = 51
          Height = 13
          Caption = 'Descri'#231#227'o:'
          FocusControl = DBEdNome
        end
        object Label17: TLabel
          Left = 768
          Top = 40
          Width = 43
          Height = 13
          Caption = 'Abertura:'
          FocusControl = DBEdCodigo
        end
        object Label18: TLabel
          Left = 884
          Top = 40
          Width = 69
          Height = 13
          Caption = 'Encerramento:'
          FocusControl = DBEdNome
        end
        object Label19: TLabel
          Left = 612
          Top = 0
          Width = 62
          Height = 13
          Caption = 'Resultado:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label20: TLabel
          Left = 468
          Top = 40
          Width = 223
          Height = 13
          Caption = 'Plano de Amostragem e Regime de Qualidade :'
        end
        object Label3: TLabel
          Left = 8
          Top = 40
          Width = 29
          Height = 13
          Caption = 'Local:'
        end
        object Label4: TLabel
          Left = 64
          Top = 40
          Width = 91
          Height = 13
          Caption = 'Descri'#231#227'o do local:'
        end
        object Label6: TLabel
          Left = 8
          Top = 80
          Width = 30
          Height = 13
          Caption = 'Artigo:'
        end
        object Label8: TLabel
          Left = 64
          Top = 80
          Width = 95
          Height = 13
          Caption = 'Descri'#231#227'o do artigo:'
        end
        object Label5: TLabel
          Left = 772
          Top = 80
          Width = 18
          Height = 13
          Caption = 'OP:'
        end
        object Label10: TLabel
          Left = 848
          Top = 80
          Width = 52
          Height = 13
          Caption = 'Reduz.OP:'
        end
        object Label11: TLabel
          Left = 8
          Top = 120
          Width = 93
          Height = 13
          Caption = 'Tabela de medidas:'
        end
        object Label12: TLabel
          Left = 476
          Top = 120
          Width = 147
          Height = 13
          Caption = 'Check list de inconformidades: '
        end
        object Label14: TLabel
          Left = 848
          Top = 120
          Width = 68
          Height = 13
          Caption = 'L.p'#231'.tab.med: '
        end
        object Label16: TLabel
          Left = 924
          Top = 120
          Width = 60
          Height = 13
          Caption = 'L.p'#231'.chklist: '
        end
        object Label13: TLabel
          Left = 676
          Top = 80
          Width = 48
          Height = 13
          Caption = 'Tamanho:'
        end
        object Label15: TLabel
          Left = 924
          Top = 80
          Width = 32
          Height = 13
          Caption = 'Grade:'
        end
        object Label21: TLabel
          Left = 8
          Top = 160
          Width = 54
          Height = 13
          Caption = 'Dispositivo:'
          FocusControl = DBEdit15
        end
        object Label23: TLabel
          Left = 428
          Top = 0
          Width = 60
          Height = 13
          Caption = 'Cofigura'#231#227'o:'
          FocusControl = DBEdit17
        end
        object Label22: TLabel
          Left = 760
          Top = 160
          Width = 47
          Height = 13
          Caption = 'P'#231' a insp.'
          FocusControl = DBEdit18
        end
        object Label24: TLabel
          Left = 820
          Top = 160
          Width = 53
          Height = 13
          Caption = 'P'#231'. inspec.'
          FocusControl = DBEdit19
        end
        object Label25: TLabel
          Left = 880
          Top = 160
          Width = 37
          Height = 13
          Caption = 'Qt. real:'
          FocusControl = DBEdit20
        end
        object Label26: TLabel
          Left = 940
          Top = 160
          Width = 42
          Height = 13
          Caption = 'Qt. local:'
          FocusControl = DBEdit21
        end
        object Label27: TLabel
          Left = 616
          Top = 80
          Width = 40
          Height = 13
          Caption = 'Produto:'
          FocusControl = DBEdit22
        end
        object Label28: TLabel
          Left = 552
          Top = 0
          Width = 48
          Height = 13
          Caption = 'Pts neg.'
          FocusControl = DBEdit23
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label29: TLabel
          Left = 496
          Top = 0
          Width = 31
          Height = 13
          Caption = 'Seq.:'
          FocusControl = DBEdit24
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label30: TLabel
          Left = 280
          Top = 160
          Width = 66
          Height = 13
          Caption = 'Autentica'#231#227'o:'
          FocusControl = DBEdit25
        end
        object Label32: TLabel
          Left = 820
          Top = 0
          Width = 177
          Height = 13
          Caption = 'Usu'#225'rio padr'#227'o do disposotivo:'
          FocusControl = DBEdit26
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label33: TLabel
          Left = 624
          Top = 160
          Width = 49
          Height = 13
          Caption = '% Medido:'
          FocusControl = DBEdit27
        end
        object Label34: TLabel
          Left = 692
          Top = 160
          Width = 37
          Height = 13
          Caption = '% Insp.:'
          FocusControl = DBEdit28
        end
        object DBEdCodigo: TdmkDBEdit
          Left = 8
          Top = 16
          Width = 56
          Height = 21
          TabStop = False
          DataField = 'Codigo'
          DataSource = DsOVmIspDevCab
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ReadOnly = True
          ShowHint = True
          TabOrder = 0
          UpdType = utYes
          Alignment = taRightJustify
        end
        object DBEdNome: TdmkDBEdit
          Left = 68
          Top = 16
          Width = 357
          Height = 21
          Color = clWhite
          DataField = 'Nome'
          DataSource = DsOVmIspDevCab
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          UpdType = utYes
          Alignment = taLeftJustify
        end
        object dmkDBEdit1: TdmkDBEdit
          Left = 768
          Top = 56
          Width = 112
          Height = 21
          Color = clWhite
          DataField = 'DtHrAbert_TXT'
          DataSource = DsOVmIspDevCab
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          UpdType = utYes
          Alignment = taLeftJustify
        end
        object dmkDBEdit2: TdmkDBEdit
          Left = 883
          Top = 56
          Width = 112
          Height = 21
          Color = clWhite
          DataField = 'DtHrFecha_TXT'
          DataSource = DsOVmIspDevCab
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          UpdType = utYes
          Alignment = taLeftJustify
        end
        object dmkDBEdit5: TdmkDBEdit
          Left = 612
          Top = 16
          Width = 49
          Height = 21
          Color = clWhite
          DataField = 'InspResul'
          DataSource = DsOVmIspDevCab
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 4
          UpdType = utYes
          Alignment = taLeftJustify
        end
        object dmkDBEdit6: TdmkDBEdit
          Left = 664
          Top = 16
          Width = 153
          Height = 21
          Color = clWhite
          DataField = 'NO_InspResul'
          DataSource = DsOVmIspDevCab
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 5
          UpdType = utYes
          Alignment = taLeftJustify
        end
        object dmkDBEdit7: TdmkDBEdit
          Left = 468
          Top = 56
          Width = 49
          Height = 21
          Color = clWhite
          DataField = 'OVcYnsARQ'
          DataSource = DsOVmIspDevCab
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 6
          UpdType = utYes
          Alignment = taLeftJustify
        end
        object dmkDBEdit8: TdmkDBEdit
          Left = 520
          Top = 56
          Width = 245
          Height = 21
          Color = clWhite
          DataField = 'NO_OVcYnsARQ'
          DataSource = DsOVmIspDevCab
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 7
          UpdType = utYes
          Alignment = taLeftJustify
        end
        object DBEdit1: TDBEdit
          Left = 8
          Top = 56
          Width = 56
          Height = 21
          DataField = 'Local'
          DataSource = DsOVmIspDevCab
          TabOrder = 8
        end
        object DBEdit2: TDBEdit
          Left = 64
          Top = 56
          Width = 401
          Height = 21
          DataField = 'NO_Local'
          DataSource = DsOVmIspDevCab
          TabOrder = 9
        end
        object DBEdit4: TDBEdit
          Left = 8
          Top = 96
          Width = 56
          Height = 21
          DataField = 'SeqGrupo'
          DataSource = DsOVmIspDevCab
          TabOrder = 10
        end
        object DBEdit5: TDBEdit
          Left = 64
          Top = 96
          Width = 545
          Height = 21
          DataField = 'NO_Referencia'
          DataSource = DsOVmIspDevCab
          TabOrder = 11
        end
        object DBEdit3: TDBEdit
          Left = 772
          Top = 96
          Width = 72
          Height = 21
          DataField = 'NrOP'
          DataSource = DsOVmIspDevCab
          TabOrder = 12
        end
        object DBEdit6: TDBEdit
          Left = 848
          Top = 96
          Width = 72
          Height = 21
          DataField = 'NrReduzidoOP'
          DataSource = DsOVmIspDevCab
          TabOrder = 13
        end
        object DBEdit7: TDBEdit
          Left = 8
          Top = 136
          Width = 56
          Height = 21
          DataField = 'OVcYnsMed'
          DataSource = DsOVmIspDevCab
          TabOrder = 14
        end
        object DBEdit9: TDBEdit
          Left = 64
          Top = 136
          Width = 409
          Height = 21
          DataField = 'NO_OVcYnsMed'
          DataSource = DsOVmIspDevCab
          TabOrder = 15
        end
        object DBEdit8: TDBEdit
          Left = 476
          Top = 136
          Width = 56
          Height = 21
          DataField = 'OVcYnsChk'
          DataSource = DsOVmIspDevCab
          TabOrder = 16
        end
        object DBEdit10: TDBEdit
          Left = 532
          Top = 136
          Width = 313
          Height = 21
          DataField = 'NO_OVcYnsChk'
          DataSource = DsOVmIspDevCab
          TabOrder = 17
        end
        object DBEdit12: TDBEdit
          Left = 848
          Top = 136
          Width = 72
          Height = 21
          DataField = 'LimiteMed'
          DataSource = DsOVmIspDevCab
          TabOrder = 18
        end
        object DBEdit11: TDBEdit
          Left = 924
          Top = 136
          Width = 72
          Height = 21
          DataField = 'LimiteChk'
          DataSource = DsOVmIspDevCab
          TabOrder = 19
        end
        object DBEdit13: TDBEdit
          Left = 676
          Top = 96
          Width = 93
          Height = 21
          DataField = 'CodTam'
          DataSource = DsOVmIspDevCab
          TabOrder = 20
        end
        object DBEdit14: TDBEdit
          Left = 924
          Top = 96
          Width = 72
          Height = 21
          DataField = 'CodGrade'
          DataSource = DsOVmIspDevCab
          TabOrder = 21
        end
        object DBEdit15: TDBEdit
          Left = 8
          Top = 176
          Width = 56
          Height = 21
          DataField = 'DeviceSI'
          DataSource = DsOVmIspDevCab
          TabOrder = 22
        end
        object DBEdit16: TDBEdit
          Left = 64
          Top = 176
          Width = 213
          Height = 21
          DataField = 'DeviceID'
          DataSource = DsOVmIspDevCab
          TabOrder = 23
        end
        object DBEdit17: TDBEdit
          Left = 428
          Top = 16
          Width = 65
          Height = 21
          DataField = 'OVgIspGer'
          DataSource = DsOVmIspDevCab
          TabOrder = 24
        end
        object DBEdit18: TDBEdit
          Left = 760
          Top = 176
          Width = 56
          Height = 21
          DataField = 'PecasIsp'
          DataSource = DsOVmIspDevCab
          TabOrder = 25
        end
        object DBEdit19: TDBEdit
          Left = 820
          Top = 176
          Width = 56
          Height = 21
          DataField = 'PecaAtual'
          DataSource = DsOVmIspDevCab
          TabOrder = 26
        end
        object DBEdit20: TDBEdit
          Left = 880
          Top = 176
          Width = 56
          Height = 21
          DataField = 'QtReal'
          DataSource = DsOVmIspDevCab
          TabOrder = 27
        end
        object DBEdit21: TDBEdit
          Left = 940
          Top = 176
          Width = 56
          Height = 21
          DataField = 'QtLocal'
          DataSource = DsOVmIspDevCab
          TabOrder = 28
        end
        object DBEdit22: TDBEdit
          Left = 616
          Top = 96
          Width = 56
          Height = 21
          DataField = 'Produto'
          DataSource = DsOVmIspDevCab
          TabOrder = 29
        end
        object DBEdit23: TDBEdit
          Left = 552
          Top = 16
          Width = 57
          Height = 21
          DataField = 'PontosTot'
          DataSource = DsOVmIspDevCab
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 30
        end
        object DBEdit24: TDBEdit
          Left = 496
          Top = 16
          Width = 53
          Height = 21
          DataField = 'InspeSeq'
          DataSource = DsOVmIspDevCab
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 31
        end
        object DBEdit25: TDBEdit
          Left = 280
          Top = 176
          Width = 341
          Height = 21
          DataField = 'RandmStr'
          DataSource = DsOVmIspDevCab
          TabOrder = 32
        end
        object DBEdit26: TDBEdit
          Left = 820
          Top = 16
          Width = 177
          Height = 21
          DataField = 'UserNmePdr'
          DataSource = DsOVmIspDevCab
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 33
        end
        object DBEdit27: TDBEdit
          Left = 624
          Top = 176
          Width = 64
          Height = 21
          DataField = 'PerMedReal'
          DataSource = DsOVmIspDevCab
          TabOrder = 34
        end
        object DBEdit28: TDBEdit
          Left = 692
          Top = 176
          Width = 64
          Height = 21
          DataField = 'PerChkReal'
          DataSource = DsOVmIspDevCab
          TabOrder = 35
        end
      end
    end
    object PGDados: TPageControl
      Left = 0
      Top = 221
      Width = 1007
      Height = 212
      ActivePage = TabSheet1
      Align = alTop
      TabOrder = 2
      object TabSheet1: TTabSheet
        Caption = ' T'#243'picos negativados '
        object DBGrid1: TDBGrid
          Left = 0
          Top = 0
          Width = 457
          Height = 184
          Align = alLeft
          DataSource = DsPontosNeg
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -12
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'PecaSeq'
              Title.Caption = 'P'#231' seq.'
              Width = 36
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Tabela'
              Width = 102
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'QtdFotos'
              Title.Caption = 'Q.Fotos'
              Width = 27
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_Topico'
              Title.Caption = 'T'#243'pico'
              Width = 210
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'PontNeg'
              Title.Caption = 'Ps neg.'
              Width = 41
              Visible = True
            end>
        end
        object Panel7: TPanel
          Left = 457
          Top = 0
          Width = 148
          Height = 184
          Align = alLeft
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 1
          object Label31: TLabel
            Left = 0
            Top = 0
            Width = 148
            Height = 13
            Align = alTop
            Alignment = taCenter
            Caption = 'Fotos do item selecionado'
            ExplicitWidth = 123
          end
          object DBGrid2: TDBGrid
            Left = 0
            Top = 13
            Width = 148
            Height = 171
            Align = alClient
            DataSource = DsOVmIspDevFts
            TabOrder = 0
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -12
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            Columns = <
              item
                Expanded = False
                FieldName = 'DataHora'
                Visible = True
              end>
          end
        end
        object Panel8: TPanel
          Left = 605
          Top = 0
          Width = 394
          Height = 184
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 2
          object ImgFoto: TImage
            Left = 0
            Top = 13
            Width = 394
            Height = 171
            Align = alClient
            Picture.Data = {
              0A544A504547496D61676577020000FFD8FFE000104A46494600010101006000
              600000FFDB004300020101020101020202020202020203050303030303060404
              0305070607070706070708090B0908080A0807070A0D0A0A0B0C0C0C0C07090E
              0F0D0C0E0B0C0C0CFFDB004301020202030303060303060C0807080C0C0C0C0C
              0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C
              0C0C0C0C0C0C0C0C0C0C0C0C0CFFC00011080002000203012200021101031101
              FFC4001F0000010501010101010100000000000000000102030405060708090A
              0BFFC400B5100002010303020403050504040000017D01020300041105122131
              410613516107227114328191A1082342B1C11552D1F02433627282090A161718
              191A25262728292A3435363738393A434445464748494A535455565758595A63
              6465666768696A737475767778797A838485868788898A92939495969798999A
              A2A3A4A5A6A7A8A9AAB2B3B4B5B6B7B8B9BAC2C3C4C5C6C7C8C9CAD2D3D4D5D6
              D7D8D9DAE1E2E3E4E5E6E7E8E9EAF1F2F3F4F5F6F7F8F9FAFFC4001F01000301
              01010101010101010000000000000102030405060708090A0BFFC400B5110002
              0102040403040705040400010277000102031104052131061241510761711322
              328108144291A1B1C109233352F0156272D10A162434E125F11718191A262728
              292A35363738393A434445464748494A535455565758595A636465666768696A
              737475767778797A82838485868788898A92939495969798999AA2A3A4A5A6A7
              A8A9AAB2B3B4B5B6B7B8B9BAC2C3C4C5C6C7C8C9CAD2D3D4D5D6D7D8D9DAE2E3
              E4E5E6E7E8E9EAF2F3F4F5F6F7F8F9FAFFDA000C03010002110311003F00FE7F
              E8A28A00FFD9}
            Proportional = True
            ExplicitLeft = 32
            ExplicitTop = 25
            ExplicitHeight = 152
          end
          object LaNomeFoto: TLabel
            Left = 0
            Top = 0
            Width = 394
            Height = 13
            Align = alTop
            Caption = '...'
            ExplicitWidth = 9
          end
        end
      end
      object TabSheet2: TTabSheet
        Caption = ' Medidas '
        ImageIndex = 1
        object Panel9: TPanel
          Left = 0
          Top = 0
          Width = 999
          Height = 184
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          object DBGMedPecas: TdmkDBGridZTO
            Left = 0
            Top = 0
            Width = 378
            Height = 184
            Align = alLeft
            DataSource = DsMedPecas
            DefaultDrawing = False
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
            ParentFont = False
            TabOrder = 0
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -12
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            RowColors = <>
            OnDrawColumnCell = DBGMedPecasDrawColumnCell
            Columns = <
              item
                Expanded = False
                FieldName = 'PecaSeq'
                Title.Caption = 'Pe'#231'a'
                Width = 40
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Itens'
                Title.Caption = 'Medidas'
                Width = 40
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Exatas'
                Width = 40
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Dentro'
                Width = 40
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Fora'
                Width = 40
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'PtsDesvioMed'
                Title.Caption = 'Pt.Desv.M'#233'd'
                Width = 45
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'MaxPtsDesvio'
                Title.Caption = 'Max Pt. Desvio'
                Width = 45
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'PercDesvio'
                Title.Caption = '% Desvio'
                Width = 40
                Visible = True
              end>
          end
          object DBGMedNPeca: TdmkDBGridZTO
            Left = 378
            Top = 0
            Width = 436
            Height = 184
            Align = alClient
            DataSource = DsMedNPeca
            DefaultDrawing = False
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
            ParentFont = False
            TabOrder = 1
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -12
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            RowColors = <>
            OnDrawColumnCell = DBGMedNPecaDrawColumnCell
            Columns = <
              item
                Expanded = False
                FieldName = 'CtrlInMob'
                Title.Caption = 'ID'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'PtsDesvio'
                Title.Caption = 'Pts.Desvio'
                Width = 40
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'PercDesvio'
                Title.Caption = '% Desvio'
                Width = 40
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'CodTam'
                Title.Caption = 'Tamanho'
                Width = 49
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NO_TOBIKO'
                Title.Caption = 'T'#243'pico'
                Width = 233
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'MedidFei'
                Title.Caption = 'Med.Feita'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NO_TolerUnMdida'
                Title.Caption = 'Un.medida'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NO_MedidOky'
                Title.Caption = 'OK?'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'PontNeg'
                Title.Caption = 'Pt.Neg.'
                Width = 41
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'MedidCer'
                Title.Caption = 'Med.Certa'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'MedidMin'
                Title.Caption = 'Med. M'#237'nima'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'MedidMax'
                Title.Caption = 'Med. M'#225'xima'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NO_TolerBasCalc'
                Title.Caption = 'Toler.Tipo'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'TolerRndPerc'
                Title.Caption = 'Toler.Valor'
                Visible = True
              end>
          end
          object Memo1: TMemo
            Left = 814
            Top = 0
            Width = 185
            Height = 184
            Align = alRight
            TabOrder = 2
          end
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1007
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 959
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 12
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 216
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 216
      Top = 0
      Width = 743
      Height = 52
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 424
        Height = 32
        Caption = 'Inspe'#231#245'es Realizadas em Fac'#231#245'es'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 424
        Height = 32
        Caption = 'Inspe'#231#245'es Realizadas em Fac'#231#245'es'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 424
        Height = 32
        Caption = 'Inspe'#231#245'es Realizadas em Fac'#231#245'es'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 52
    Width = 1007
    Height = 44
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1003
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 12
        Height = 17
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 12
        Height = 17
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object QrOVmIspDevCab: TMySQLQuery
    Database = FmTempDB.DBTeste
    BeforeOpen = QrOVmIspDevCabBeforeOpen
    AfterOpen = QrOVmIspDevCabAfterOpen
    BeforeClose = QrOVmIspDevCabBeforeClose
    AfterScroll = QrOVmIspDevCabAfterScroll
    SQL.Strings = (
      
        'SELECT  mdc.UserNmePdr, igc.*, ymc.Nome NO_OVcYnsMed, ycc.Nome N' +
        'O_OVcYnsChk,'
      'dlo.Nome NO_Local, ref.Nome NO_Referencia,'
      'isc.Nome NO_InspResul, yac.Nome NO_OVcYnsARQ,'
      'IF(igc.DtHrAbert  <= "1899-12-30", "",'
      '  DATE_FORMAT(igc.DtHrAbert, "%d/%m/%Y %H:%i")) DtHrAbert_TXT,'
      'IF(igc.DtHrFecha  <= "1899-12-30", "",'
      '  DATE_FORMAT(igc.DtHrFecha, "%d/%m/%Y %H:%i")) DtHrFecha_TXT'
      'FROM ovmispdevcab igc'
      'LEFT JOIN ovdlocal dlo ON dlo.Codigo=igc.Local'
      'LEFT JOIN ovdreferencia ref ON ref.Codigo=igc.SeqGrupo'
      'LEFT JOIN ovcynsmedcad ymc ON ymc.Codigo=igc.OVcYnsMed'
      'LEFT JOIN ovcynschkcad ycc ON ycc.Codigo=igc.OVcYnsChk'
      'LEFT JOIN ovgisprescad isc ON isc.Codigo=igc.InspResul'
      'LEFT JOIN OVcYnsARQCad yac ON yac.Codigo=igc.OVcYnsARQ'
      'LEFT JOIN ovcmobdevcad mdc ON mdc.Codigo=igc.DeviceSI'
      'WHERE igc.Codigo > 0')
    Left = 60
    Top = 64
    object QrOVmIspDevCabCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrOVmIspDevCabCodInMob: TIntegerField
      FieldName = 'CodInMob'
      Required = True
    end
    object QrOVmIspDevCabDeviceSI: TIntegerField
      FieldName = 'DeviceSI'
      Required = True
    end
    object QrOVmIspDevCabDeviceID: TWideStringField
      FieldName = 'DeviceID'
      Size = 60
    end
    object QrOVmIspDevCabDeviceCU: TIntegerField
      FieldName = 'DeviceCU'
      Required = True
    end
    object QrOVmIspDevCabNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QrOVmIspDevCabOVgIspGer: TIntegerField
      FieldName = 'OVgIspGer'
      Required = True
    end
    object QrOVmIspDevCabLocal: TIntegerField
      FieldName = 'Local'
      Required = True
    end
    object QrOVmIspDevCabNrOP: TIntegerField
      FieldName = 'NrOP'
      Required = True
    end
    object QrOVmIspDevCabSeqGrupo: TIntegerField
      FieldName = 'SeqGrupo'
      Required = True
    end
    object QrOVmIspDevCabNrReduzidoOP: TIntegerField
      FieldName = 'NrReduzidoOP'
      Required = True
    end
    object QrOVmIspDevCabDtHrAbert: TDateTimeField
      FieldName = 'DtHrAbert'
      Required = True
    end
    object QrOVmIspDevCabDtHrFecha: TDateTimeField
      FieldName = 'DtHrFecha'
      Required = True
    end
    object QrOVmIspDevCabOVcYnsMed: TIntegerField
      FieldName = 'OVcYnsMed'
      Required = True
    end
    object QrOVmIspDevCabOVcYnsChk: TIntegerField
      FieldName = 'OVcYnsChk'
      Required = True
    end
    object QrOVmIspDevCabOVcYnsARQ: TIntegerField
      FieldName = 'OVcYnsARQ'
      Required = True
    end
    object QrOVmIspDevCabLimiteChk: TIntegerField
      FieldName = 'LimiteChk'
      Required = True
    end
    object QrOVmIspDevCabLimiteMed: TIntegerField
      FieldName = 'LimiteMed'
      Required = True
    end
    object QrOVmIspDevCabPecasIsp: TIntegerField
      FieldName = 'PecasIsp'
      Required = True
    end
    object QrOVmIspDevCabPecaAtual: TIntegerField
      FieldName = 'PecaAtual'
      Required = True
    end
    object QrOVmIspDevCabLk: TIntegerField
      FieldName = 'Lk'
      Required = True
    end
    object QrOVmIspDevCabDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrOVmIspDevCabDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrOVmIspDevCabUserCad: TIntegerField
      FieldName = 'UserCad'
      Required = True
    end
    object QrOVmIspDevCabUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Required = True
    end
    object QrOVmIspDevCabAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Required = True
    end
    object QrOVmIspDevCabAWServerID: TIntegerField
      FieldName = 'AWServerID'
      Required = True
    end
    object QrOVmIspDevCabAWStatSinc: TSmallintField
      FieldName = 'AWStatSinc'
      Required = True
    end
    object QrOVmIspDevCabAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
    object QrOVmIspDevCabQtReal: TFloatField
      FieldName = 'QtReal'
      Required = True
    end
    object QrOVmIspDevCabQtLocal: TFloatField
      FieldName = 'QtLocal'
      Required = True
    end
    object QrOVmIspDevCabProduto: TIntegerField
      FieldName = 'Produto'
      Required = True
    end
    object QrOVmIspDevCabCodGrade: TIntegerField
      FieldName = 'CodGrade'
      Required = True
    end
    object QrOVmIspDevCabCodTam: TWideStringField
      FieldName = 'CodTam'
      Size = 30
    end
    object QrOVmIspDevCabPontosTot: TIntegerField
      FieldName = 'PontosTot'
      Required = True
    end
    object QrOVmIspDevCabInspResul: TIntegerField
      FieldName = 'InspResul'
      Required = True
    end
    object QrOVmIspDevCabInspeSeq: TIntegerField
      FieldName = 'InspeSeq'
      Required = True
    end
    object QrOVmIspDevCabRandmStr: TWideStringField
      FieldName = 'RandmStr'
      Required = True
      Size = 32
    end
    object QrOVmIspDevCabDtHrUpIni: TDateTimeField
      FieldName = 'DtHrUpIni'
      Required = True
    end
    object QrOVmIspDevCabDtHrUpFim: TDateTimeField
      FieldName = 'DtHrUpFim'
      Required = True
    end
    object QrOVmIspDevCabEmpresa: TIntegerField
      FieldName = 'Empresa'
      Required = True
    end
    object QrOVmIspDevCabNO_OVcYnsMed: TWideStringField
      FieldName = 'NO_OVcYnsMed'
      Size = 60
    end
    object QrOVmIspDevCabNO_OVcYnsChk: TWideStringField
      FieldName = 'NO_OVcYnsChk'
      Size = 60
    end
    object QrOVmIspDevCabNO_Local: TWideStringField
      FieldName = 'NO_Local'
      Size = 100
    end
    object QrOVmIspDevCabNO_Referencia: TWideStringField
      FieldName = 'NO_Referencia'
      Size = 100
    end
    object QrOVmIspDevCabNO_InspResul: TWideStringField
      FieldName = 'NO_InspResul'
      Size = 60
    end
    object QrOVmIspDevCabNO_OVcYnsARQ: TWideStringField
      FieldName = 'NO_OVcYnsARQ'
      Size = 60
    end
    object QrOVmIspDevCabDtHrAbert_TXT: TWideStringField
      DisplayWidth = 19
      FieldName = 'DtHrAbert_TXT'
      Size = 19
    end
    object QrOVmIspDevCabDtHrFecha_TXT: TWideStringField
      DisplayWidth = 19
      FieldName = 'DtHrFecha_TXT'
      Size = 19
    end
    object QrOVmIspDevCabUserNmePdr: TWideStringField
      FieldName = 'UserNmePdr'
      Size = 60
    end
    object QrOVmIspDevCabPerMedReal: TFloatField
      FieldName = 'PerMedReal'
      Required = True
      DisplayFormat = '0.00'
    end
    object QrOVmIspDevCabPerChkReal: TFloatField
      FieldName = 'PerChkReal'
      Required = True
      DisplayFormat = '0.00'
    end
    object QrOVmIspDevCabDtHrMailSnt: TFloatField
      FieldName = 'DtHrMailSnt'
      Required = True
    end
  end
  object DsOVmIspDevCab: TDataSource
    DataSet = QrOVmIspDevCab
    Left = 92
    Top = 64
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = BtEmail
    CanUpd01 = BtAltera
    CanDel01 = BtExclui
    Left = 120
    Top = 64
  end
  object QrPontosNeg: TMySQLQuery
    Database = FmTempDB.DBTeste
    BeforeClose = QrPontosNegBeforeClose
    AfterScroll = QrPontosNegAfterScroll
    SQL.Strings = (
      'SELECT 0 Tabela,  mmc.PecaSeq, mmc.CtrlInMob, '
      'yqt.Nome NO_Topico, mmc.PontNeg, mmc.QtdFotos'
      'FROM ovmispdevinc mmc'
      'LEFT JOIN ovcynsqsttop yqt ON yqt.Codigo=mmc.Topico'
      'WHERE mmc.Codigo=161'
      ''
      'UNION'
      ''
      'SELECT 1 tabela,  mmm.PecaSeq, mmm.CtrlInMob, '
      'ygt.Nome NO_Topico, mmm.PontNeg, mmm.QtdFotos'
      'FROM ovmispdevmed mmm'
      'LEFT JOIN ovcynsmeddim ymd ON ymd.Conta=mmm.ovcynsmeddim'
      'LEFT JOIN ovcynsmedtop ymt ON ymt.Controle=ymd.Controle'
      'LEFT JOIN ovcynsgratop ygt ON ygt.Codigo=ymt.Tobiko'
      'WHERE mmm.MedidOky=0'
      'AND mmm.Codigo=161'
      ''
      'UNION'
      ''
      'SELECT 2 Tabela, mml.PecaSeq, mml.CtrlInMob, '
      'mml.Descricao NO_Topico, mml.PontNeg, mml.QtdFotos'
      'FROM ovmispdevlvr mml'
      'WHERE mml.Codigo=161'
      ''
      'ORDER BY PecaSeq, NO_Topico')
    Left = 512
    Top = 9
    object QrPontosNegPecaSeq: TIntegerField
      FieldName = 'PecaSeq'
      Required = True
    end
    object QrPontosNegNO_Topico: TWideStringField
      FieldName = 'NO_Topico'
      Size = 60
    end
    object QrPontosNegPontNeg: TIntegerField
      FieldName = 'PontNeg'
      Required = True
    end
    object QrPontosNegIdTabela: TWideStringField
      FieldName = 'IdTabela'
      Size = 1
    end
    object QrPontosNegTabela: TWideStringField
      FieldName = 'Tabela'
    end
    object QrPontosNegQtdFotos: TIntegerField
      FieldName = 'QtdFotos'
    end
    object QrPontosNegCtrlInMob: TIntegerField
      FieldName = 'CtrlInMob'
      Required = True
    end
  end
  object DsPontosNeg: TDataSource
    DataSet = QrPontosNeg
    Left = 512
    Top = 57
  end
  object QrOVmIspDevFts: TMySQLQuery
    Database = FmTempDB.DBTeste
    AfterScroll = QrOVmIspDevFtsAfterScroll
    SQL.Strings = (
      'SELECT * FROM ovmispdevfts')
    Left = 604
    Top = 9
    object QrOVmIspDevFtsCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrOVmIspDevFtsIdTabela: TSmallintField
      FieldName = 'IdTabela'
      Required = True
    end
    object QrOVmIspDevFtsCodInMob: TIntegerField
      FieldName = 'CodInMob'
      Required = True
    end
    object QrOVmIspDevFtsCtrlInMob: TIntegerField
      FieldName = 'CtrlInMob'
      Required = True
    end
    object QrOVmIspDevFtsDataHora: TDateTimeField
      FieldName = 'DataHora'
      Required = True
    end
    object QrOVmIspDevFtsNomeArq: TWideStringField
      FieldName = 'NomeArq'
      Size = 255
    end
    object QrOVmIspDevFtsLk: TIntegerField
      FieldName = 'Lk'
      Required = True
    end
    object QrOVmIspDevFtsDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrOVmIspDevFtsDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrOVmIspDevFtsUserCad: TIntegerField
      FieldName = 'UserCad'
      Required = True
    end
    object QrOVmIspDevFtsUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Required = True
    end
    object QrOVmIspDevFtsAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Required = True
    end
    object QrOVmIspDevFtsAWServerID: TIntegerField
      FieldName = 'AWServerID'
      Required = True
    end
    object QrOVmIspDevFtsAWStatSinc: TSmallintField
      FieldName = 'AWStatSinc'
      Required = True
    end
    object QrOVmIspDevFtsAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
    object QrOVmIspDevFtsNoArqSvr: TWideStringField
      FieldName = 'NoArqSvr'
      Size = 255
    end
  end
  object DsOVmIspDevFts: TDataSource
    DataSet = QrOVmIspDevFts
    Left = 604
    Top = 57
  end
  object QrOVmIspDevBmp: TMySQLQuery
    Database = FmTempDB.DBTeste
    Left = 584
    Top = 413
    object QrOVmIspDevBmpTxtBmp: TWideMemoField
      FieldName = 'TxtBmp'
      BlobType = ftWideMemo
    end
  end
  object QrMedPecas: TMySQLQuery
    Database = FmTempDB.DBTeste
    BeforeClose = QrMedPecasBeforeClose
    AfterScroll = QrMedPecasAfterScroll
    SQL.Strings = (
      'SELECT PecaSeq, COUNT(PecaSeq) Itens,'
      'SUM(IF(MedidFei=MedidCer, 1, 0)) Exatas,'
      'SUM(IF(MedidFei BETWEEN MedidMin AND MedidMax, 1, 0)) -'
      'SUM(IF(MedidFei=MedidCer, 1, 0)) Dentro,'
      'SUM(IF(MedidFei BETWEEN MedidMin AND MedidMax, 0, 1)) Fora,'
      'SUM('
      '  CASE '
      '    WHEN MedidFei=MedidCer THEN 0'
      
        '    WHEN MedidFei<MedidCer THEN POW((MedidCer - MedidFei) / (Med' +
        'idCer-MedidMin) * 4, 2)'
      
        '    WHEN MedidFei>MedidCer THEN POW((MedidFei - MedidCer) / (Med' +
        'idMax-MedidCer) * 4, 2)'
      '  END)'
      'PtsDesvioMed, COUNT(PecaSeq) * POW(4, 2) MaxPtsDesvio,'
      'SUM('
      '  CASE '
      '    WHEN MedidFei=MedidCer THEN 0'
      
        '    WHEN MedidFei<MedidCer THEN POW((MedidCer - MedidFei) / (Med' +
        'idCer-MedidMin) * 4, 2)'
      
        '    WHEN MedidFei>MedidCer THEN POW((MedidFei - MedidCer) / (Med' +
        'idMax-MedidCer) * 4, 2)'
      '  END)'
      '/ (COUNT(PecaSeq) * POW(4, 2)) * 100 PercDesvio'
      ''
      'FROM ovmispdevmed'
      'WHERE Codigo=4'
      'GROUP BY PecaSeq'
      'ORDER BY PecaSeq')
    Left = 248
    Top = 393
    object QrMedPecasPecaSeq: TIntegerField
      FieldName = 'PecaSeq'
      Required = True
      DisplayFormat = '0;-0; '
    end
    object QrMedPecasItens: TLargeintField
      FieldName = 'Itens'
      Required = True
      DisplayFormat = '0;-0; '
    end
    object QrMedPecasExatas: TFloatField
      FieldName = 'Exatas'
      DisplayFormat = '0;-0; '
    end
    object QrMedPecasDentro: TFloatField
      FieldName = 'Dentro'
      DisplayFormat = '0;-0; '
    end
    object QrMedPecasFora: TFloatField
      FieldName = 'Fora'
      DisplayFormat = '0;-0; '
    end
    object QrMedPecasPtsDesvioMed: TFloatField
      FieldName = 'PtsDesvioMed'
      DisplayFormat = '0;-0; '
    end
    object QrMedPecasMaxPtsDesvio: TFloatField
      FieldName = 'MaxPtsDesvio'
      DisplayFormat = '0;-0; '
    end
    object QrMedPecasPercDesvio: TFloatField
      FieldName = 'PercDesvio'
      DisplayFormat = '0;-0; '
    end
  end
  object DsMedPecas: TDataSource
    DataSet = QrMedPecas
    Left = 248
    Top = 437
  end
  object QrMedNPeca: TMySQLQuery
    Database = FmTempDB.DBTeste
    SQL.Strings = (
      'SELECT  '
      ' '
      'CASE  '
      '  WHEN MedidFei=MedidCer THEN 0 '
      
        '  WHEN MedidFei<MedidCer THEN Pow((MedidCer - MedidFei) / (Medid' +
        'Cer-MedidMin) * 4, 2) '
      
        '  WHEN MedidFei>MedidCer THEN Pow((MedidFei - MedidCer) / (Medid' +
        'Max-MedidCer) * 4, 2) '
      'END PtsDesvio, '
      '( '
      '  CASE  '
      '    WHEN MedidFei=MedidCer THEN 0 '
      
        '    WHEN MedidFei<MedidCer THEN Pow((MedidCer - MedidFei) / (Med' +
        'idCer-MedidMin) * 4, 2) '
      
        '    WHEN MedidFei>MedidCer THEN Pow((MedidFei - MedidCer) / (Med' +
        'idMax-MedidCer) * 4, 2) '
      '  END) / POW(4, 2) * 100 PercDesvio, '
      ' '
      
        'ELT(ygt.TolerBasCalc+1, "Medida", "Percentual") NO_TolerBasCalc,' +
        ' '
      'ELT(ygt.TolerUnMdida+1, "cm", "inch") NO_TolerUnMdida,  '
      'ygt.Nome NO_TOBIKO,  '
      'ygt.TolerBasCalc, ygt.TolerUnMdida, '
      'ygt.TolerRndPerc,  '
      'IF(idm.MedidOky=0, "N'#227'o", "Sim") NO_MedidOky,  '
      'idm.* '
      'FROM ovmispdevmed idm '
      'LEFT JOIN ovcynsgratop ygt ON ygt.Codigo=idm.OVcYnsMedTop '
      'WHERE idm.Codigo=7 ')
    Left = 332
    Top = 393
    object QrMedNPecaNO_TolerBasCalc: TWideStringField
      FieldName = 'NO_TolerBasCalc'
      Size = 10
    end
    object QrMedNPecaNO_TolerUnMdida: TWideStringField
      FieldName = 'NO_TolerUnMdida'
      Size = 4
    end
    object QrMedNPecaNO_TOBIKO: TWideStringField
      FieldName = 'NO_TOBIKO'
      Size = 60
    end
    object QrMedNPecaTolerBasCalc: TSmallintField
      FieldName = 'TolerBasCalc'
    end
    object QrMedNPecaTolerUnMdida: TSmallintField
      FieldName = 'TolerUnMdida'
    end
    object QrMedNPecaTolerRndPerc: TFloatField
      FieldName = 'TolerRndPerc'
    end
    object QrMedNPecaNO_MedidOky: TWideStringField
      FieldName = 'NO_MedidOky'
      Required = True
      Size = 3
    end
    object QrMedNPecaCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrMedNPecaCodInMob: TIntegerField
      FieldName = 'CodInMob'
      Required = True
    end
    object QrMedNPecaCtrlInMob: TIntegerField
      FieldName = 'CtrlInMob'
      Required = True
    end
    object QrMedNPecaPecaSeq: TIntegerField
      FieldName = 'PecaSeq'
      Required = True
    end
    object QrMedNPecaOVcYnsMedDim: TIntegerField
      FieldName = 'OVcYnsMedDim'
      Required = True
    end
    object QrMedNPecaOVcYnsMedTop: TIntegerField
      FieldName = 'OVcYnsMedTop'
      Required = True
    end
    object QrMedNPecaTobiko: TIntegerField
      FieldName = 'Tobiko'
      Required = True
    end
    object QrMedNPecaCodGrade: TIntegerField
      FieldName = 'CodGrade'
      Required = True
    end
    object QrMedNPecaCodTam: TWideStringField
      FieldName = 'CodTam'
      Size = 30
    end
    object QrMedNPecaMedidFei: TFloatField
      FieldName = 'MedidFei'
      Required = True
      DisplayFormat = '#,##0.00'
    end
    object QrMedNPecaMedidOky: TSmallintField
      FieldName = 'MedidOky'
      Required = True
    end
    object QrMedNPecaPontNeg: TIntegerField
      FieldName = 'PontNeg'
      Required = True
    end
    object QrMedNPecaMedidCer: TFloatField
      FieldName = 'MedidCer'
      Required = True
      DisplayFormat = '#,##0.00'
    end
    object QrMedNPecaMedidMin: TFloatField
      FieldName = 'MedidMin'
      Required = True
      DisplayFormat = '#,##0.00'
    end
    object QrMedNPecaMedidMax: TFloatField
      FieldName = 'MedidMax'
      Required = True
      DisplayFormat = '#,##0.00'
    end
    object QrMedNPecaUlWayInz: TSmallintField
      FieldName = 'UlWayInz'
      Required = True
    end
    object QrMedNPecaQtdFotos: TIntegerField
      FieldName = 'QtdFotos'
      Required = True
    end
    object QrMedNPecaPtsDesvio: TFloatField
      FieldName = 'PtsDesvio'
      DisplayFormat = '0'
    end
    object QrMedNPecaPercDesvio: TFloatField
      FieldName = 'PercDesvio'
      DisplayFormat = '0'
    end
  end
  object DsMedNPeca: TDataSource
    DataSet = QrMedNPeca
    Left = 332
    Top = 441
  end
end
