object FmSeccTexTinturaria: TFmSeccTexTinturaria
  Left = 339
  Top = 185
  Caption = 'OVS-PSCFG-003 :: P'#243's-configura'#231#227'o de Itens de Tinturaria'
  ClientHeight = 629
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 912
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 483
        Height = 32
        Caption = ' P'#243's-configura'#231#227'o de Itens de Tinturaria'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 483
        Height = 32
        Caption = ' P'#243's-configura'#231#227'o de Itens de Tinturaria'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 483
        Height = 32
        Caption = ' P'#243's-configura'#231#227'o de Itens de Tinturaria'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 1008
    Height = 467
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 1008
      Height = 467
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 1008
        Height = 467
        Align = alClient
        TabOrder = 0
        object Splitter1: TSplitter
          Left = 2
          Top = 273
          Width = 1004
          Height = 5
          Cursor = crVSplit
          Align = alTop
          ExplicitWidth = 192
        end
        object Panel5: TPanel
          Left = 2
          Top = 278
          Width = 1004
          Height = 187
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          object PnEdita: TPanel
            Left = 0
            Top = 138
            Width = 1004
            Height = 49
            Align = alBottom
            BevelOuter = bvNone
            TabOrder = 0
            object BtAlteraBtl: TBitBtn
              Tag = 11
              Left = 96
              Top = 4
              Width = 90
              Height = 40
              Cursor = crHandPoint
              Caption = '&Altera'
              Enabled = False
              NumGlyphs = 2
              ParentShowHint = False
              ShowHint = True
              TabOrder = 0
              OnClick = BtAlteraBtlClick
            end
            object BtExcluiBtl: TBitBtn
              Tag = 12
              Left = 188
              Top = 4
              Width = 90
              Height = 40
              Cursor = crHandPoint
              Caption = '&Exclui'
              Enabled = False
              NumGlyphs = 2
              ParentShowHint = False
              ShowHint = True
              TabOrder = 1
              OnClick = BtExcluiBtlClick
            end
            object BtIncluiBtl: TBitBtn
              Tag = 10
              Left = 4
              Top = 4
              Width = 90
              Height = 40
              Cursor = crHandPoint
              Caption = '&Inclui'
              Enabled = False
              NumGlyphs = 2
              ParentShowHint = False
              ShowHint = True
              TabOrder = 2
              OnClick = BtIncluiBtlClick
            end
          end
          object DBGrid1: TDBGrid
            Left = 0
            Top = 0
            Width = 1004
            Height = 138
            Align = alClient
            DataSource = DsOVgItxGerBtl
            TabOrder = 1
            TitleFont.Charset = ANSI_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            Columns = <
              item
                Expanded = False
                FieldName = 'Controle'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'SeccaoOP'
                Title.Caption = 'OP tingimento'
                Width = 140
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'QtReal'
                Title.Caption = 'Qtde. real'
                Width = 72
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'SeccaoMaq'
                Title.Caption = 'Identifica'#231#227'o da m'#225'quina'
                Width = 140
                Visible = True
              end>
          end
        end
        object Panel6: TPanel
          Left = 2
          Top = 15
          Width = 1004
          Height = 258
          Align = alTop
          BevelOuter = bvNone
          Caption = 'Panel6'
          TabOrder = 1
          object DBGridZTO: TdmkDBGridZTO
            Left = 0
            Top = 0
            Width = 1004
            Height = 209
            Align = alClient
            DataSource = DsAptos
            Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
            TabOrder = 0
            TitleFont.Charset = ANSI_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            RowColors = <>
            Columns = <
              item
                Expanded = False
                FieldName = 'Codigo'
                Title.Caption = 'ID Conf.'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Ciclo'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Local'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NO_LOCAL'
                Title.Caption = 'Local'
                Width = 190
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'SeqGrupo'
                Title.Caption = 'Artigo'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NO_REFERENCIA'
                Title.Caption = 'Nome do Artigo'
                Width = 190
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NrOP'
                Title.Caption = 'N'#250'mero OP'
                Width = 62
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NrReduzidoOP'
                Title.Caption = 'Reduz. OP'
                Width = 62
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'QtLocal'
                Title.Caption = 'Qt. Local'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'QtReal'
                Title.Caption = 'Qt. Real'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'QtBtl'
                Title.Caption = 'Soma qtde.'
                Width = 72
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'CfgBtlOk'
                Title.Caption = 'OK'
                Width = 18
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Local'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NrReduzidoOP'
                Visible = True
              end>
          end
          object Panel7: TPanel
            Left = 0
            Top = 209
            Width = 1004
            Height = 49
            Align = alBottom
            BevelOuter = bvNone
            TabOrder = 1
            object BtAlteraCab: TBitBtn
              Tag = 11
              Left = 96
              Top = 4
              Width = 90
              Height = 40
              Cursor = crHandPoint
              Caption = '&Altera'
              Enabled = False
              NumGlyphs = 2
              ParentShowHint = False
              ShowHint = True
              TabOrder = 0
              OnClick = BtAlteraCabClick
            end
          end
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 515
    Width = 1008
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 559
    Width = 1008
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 862
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 860
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtRefresh: TBitBtn
        Tag = 18
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Refresh'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtRefreshClick
      end
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 352
    Top = 171
  end
  object QrAptos: TMySQLQuery
    Database = Dmod.ZZDB
    AfterOpen = QrAptosAfterOpen
    BeforeClose = QrAptosBeforeClose
    AfterScroll = QrAptosAfterScroll
    SQL.Strings = (
      'SELECT fop.Ciclo, fop.QtReal, fop.QtLocal, '
      'loc.Nome NO_LOCAL, art.Nome NO_Referencia, itx.* '
      'FROM ovgitxgercab itx '
      'LEFT JOIN ovdlocal loc ON loc.Codigo=itx.Local '
      'LEFT JOIN ovdreferencia art ON art.Codigo=itx.SeqGrupo '
      'LEFT JOIN ovfordemproducao fop ON '
      '  fop.NrReduzidoOP=itx.NrReduzidoOP '
      '  AND fop.NrOP=itx.NrOP'
      '  AND fop.Local=itx.Local'
      '  AND fop.SeqGrupo=itx.SeqGrupo'
      'WHERE itx.SeccaoInsp=2048 '
      'AND itx.CfgBtlOk=0 ')
    Left = 220
    Top = 160
    object QrAptosNO_LOCAL: TWideStringField
      FieldName = 'NO_LOCAL'
      Size = 100
    end
    object QrAptosNO_REFERENCIA: TWideStringField
      FieldName = 'NO_REFERENCIA'
      Size = 100
    end
    object QrAptosCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrAptosNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QrAptosLocal: TIntegerField
      FieldName = 'Local'
      Required = True
    end
    object QrAptosNrOP: TIntegerField
      FieldName = 'NrOP'
      Required = True
    end
    object QrAptosSeqGrupo: TIntegerField
      FieldName = 'SeqGrupo'
      Required = True
    end
    object QrAptosNrReduzidoOP: TIntegerField
      FieldName = 'NrReduzidoOP'
      Required = True
    end
    object QrAptosDtHrAbert: TDateTimeField
      FieldName = 'DtHrAbert'
      Required = True
    end
    object QrAptosDtHrFecha: TDateTimeField
      FieldName = 'DtHrFecha'
      Required = True
    end
    object QrAptosOVcYnsExg: TIntegerField
      FieldName = 'OVcYnsExg'
      Required = True
    end
    object QrAptosZtatusIsp: TIntegerField
      FieldName = 'ZtatusIsp'
      Required = True
    end
    object QrAptosZtatusDtH: TDateTimeField
      FieldName = 'ZtatusDtH'
      Required = True
    end
    object QrAptosZtatusMot: TIntegerField
      FieldName = 'ZtatusMot'
      Required = True
    end
    object QrAptosPermiFinHow: TIntegerField
      FieldName = 'PermiFinHow'
      Required = True
    end
    object QrAptosOVgItxPrfCab: TIntegerField
      FieldName = 'OVgItxPrfCab'
      Required = True
    end
    object QrAptosAtivMan: TSmallintField
      FieldName = 'AtivMan'
      Required = True
    end
    object QrAptosAtivAut: TSmallintField
      FieldName = 'AtivAut'
      Required = True
    end
    object QrAptosLk: TIntegerField
      FieldName = 'Lk'
      Required = True
    end
    object QrAptosDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrAptosDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrAptosUserCad: TIntegerField
      FieldName = 'UserCad'
      Required = True
    end
    object QrAptosUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Required = True
    end
    object QrAptosAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Required = True
    end
    object QrAptosAWServerID: TIntegerField
      FieldName = 'AWServerID'
      Required = True
    end
    object QrAptosAWStatSinc: TSmallintField
      FieldName = 'AWStatSinc'
      Required = True
    end
    object QrAptosAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
    object QrAptosSegmntInsp: TIntegerField
      FieldName = 'SegmntInsp'
      Required = True
    end
    object QrAptosSeccaoInsp: TIntegerField
      FieldName = 'SeccaoInsp'
      Required = True
    end
    object QrAptosQtBtl: TFloatField
      FieldName = 'QtBtl'
    end
    object QrAptosCiclo: TIntegerField
      FieldName = 'Ciclo'
    end
    object QrAptosQtReal: TFloatField
      FieldName = 'QtReal'
    end
    object QrAptosQtLocal: TFloatField
      FieldName = 'QtLocal'
    end
    object QrAptosCfgBtlOk: TSmallintField
      FieldName = 'CfgBtlOk'
      Required = True
    end
  end
  object DsAptos: TDataSource
    DataSet = QrAptos
    Left = 220
    Top = 208
  end
  object QrOVgItxGerBtl: TMySQLQuery
    Database = Dmod.ZZDB
    AfterOpen = QrOVgItxGerBtlAfterOpen
    BeforeClose = QrOVgItxGerBtlBeforeClose
    SQL.Strings = (
      'SELECT * '
      'FROM ovgitxgerbtl'
      'WHERE Codigo=0')
    Left = 396
    Top = 356
    object QrOVgItxGerBtlCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrOVgItxGerBtlControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrOVgItxGerBtlOrdem: TIntegerField
      FieldName = 'Ordem'
      Required = True
    end
    object QrOVgItxGerBtlSeccaoOP: TWideStringField
      FieldName = 'SeccaoOP'
      Size = 60
    end
    object QrOVgItxGerBtlSeccaoMaq: TWideStringField
      FieldName = 'SeccaoMaq'
      Size = 60
    end
    object QrOVgItxGerBtlQtReal: TFloatField
      FieldName = 'QtReal'
      Required = True
    end
    object QrOVgItxGerBtlLk: TIntegerField
      FieldName = 'Lk'
      Required = True
    end
    object QrOVgItxGerBtlDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrOVgItxGerBtlDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrOVgItxGerBtlUserCad: TIntegerField
      FieldName = 'UserCad'
      Required = True
    end
    object QrOVgItxGerBtlUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Required = True
    end
    object QrOVgItxGerBtlAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Required = True
    end
    object QrOVgItxGerBtlAWServerID: TIntegerField
      FieldName = 'AWServerID'
      Required = True
    end
    object QrOVgItxGerBtlAWStatSinc: TSmallintField
      FieldName = 'AWStatSinc'
      Required = True
    end
    object QrOVgItxGerBtlAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
  end
  object DsOVgItxGerBtl: TDataSource
    DataSet = QrOVgItxGerBtl
    Left = 396
    Top = 404
  end
  object PMAptos: TPopupMenu
    Left = 34
    Top = 271
    object Encerraitemdefinecomopsconfigurado1: TMenuItem
      Caption = 'Encerra item (define como p'#243's-configurado)'
      OnClick = Encerraitemdefinecomopsconfigurado1Click
    end
  end
  object QrCorda: TMySQLQuery
    Database = Dmod.ZZDB
    SQL.Strings = (
      'SELECT loc.Nome NO_LOCAL, art.Nome NO_Referencia, itx.*'
      'FROM ovgitxgercab itx'
      'LEFT JOIN ovdlocal loc ON loc.Codigo=itx.Local'
      'LEFT JOIN ovdreferencia art ON art.Codigo=itx.SeqGrupo'
      ''
      'WHERE itx.SeccaoInsp=2048'
      'AND ('
      '  SeccaoOP IS NULL'
      '  OR '
      '  SeccaoOP = ""'
      ')')
    Left = 36
    Top = 168
    object QrCordaCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
  end
end
