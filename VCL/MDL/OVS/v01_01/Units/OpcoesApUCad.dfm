object FmOpcoesApUCad: TFmOpcoesApUCad
  Left = 339
  Top = 185
  Caption = 'OVS-USUAR-001 :: Opc'#245'es de Usu'#225'rio'
  ClientHeight = 411
  ClientWidth = 408
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 408
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    ExplicitWidth = 364
    object GB_R: TGroupBox
      Left = 360
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      ExplicitLeft = 316
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 312
      Height = 48
      Align = alClient
      TabOrder = 2
      ExplicitWidth = 268
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 233
        Height = 32
        Caption = 'Opc'#245'es de Usu'#225'rio'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 233
        Height = 32
        Caption = 'Opc'#245'es de Usu'#225'rio'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 233
        Height = 32
        Caption = 'Opc'#245'es de Usu'#225'rio'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 408
    Height = 249
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    ExplicitWidth = 364
    ExplicitHeight = 111
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 408
      Height = 249
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 364
      ExplicitHeight = 111
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 408
        Height = 249
        Align = alClient
        TabOrder = 0
        ExplicitWidth = 364
        ExplicitHeight = 111
        object Panel5: TPanel
          Left = 2
          Top = 15
          Width = 404
          Height = 46
          Align = alTop
          BevelOuter = bvNone
          Enabled = False
          TabOrder = 0
          ExplicitWidth = 360
          object Label1: TLabel
            Left = 8
            Top = 4
            Width = 39
            Height = 13
            Caption = 'Usu'#225'rio:'
          end
          object EdCodigo: TdmkEdit
            Left = 8
            Top = 20
            Width = 80
            Height = 21
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
          object EdLogin: TdmkEdit
            Left = 92
            Top = 20
            Width = 297
            Height = 21
            TabOrder = 1
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
        end
        object Panel6: TPanel
          Left = 2
          Top = 61
          Width = 404
          Height = 186
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 1
          ExplicitWidth = 360
          ExplicitHeight = 48
          object Panel8: TPanel
            Left = 0
            Top = 0
            Width = 404
            Height = 81
            Align = alTop
            BevelOuter = bvNone
            TabOrder = 0
            ExplicitWidth = 531
            object CkHabFaccao: TdmkCheckBox
              Left = 8
              Top = 0
              Width = 465
              Height = 17
              Caption = 'Usu'#225'rio com habilita'#231#227'o gen'#233'rica para janelas de FAC'#199#213'ES.'
              TabOrder = 0
              OnClick = CkHabFaccaoClick
              UpdType = utYes
              ValCheck = #0
              ValUncheck = #0
              OldValor = #0
            end
            object GroupBox2: TGroupBox
              Left = 8
              Top = 20
              Width = 381
              Height = 53
              TabOrder = 1
              object PnFaccoes: TPanel
                Left = 2
                Top = 15
                Width = 377
                Height = 36
                Align = alClient
                BevelOuter = bvNone
                TabOrder = 0
                ExplicitLeft = -2
                ExplicitWidth = 181
                ExplicitHeight = 56
                object CkHabFacConfeccao: TdmkCheckBox
                  Left = 8
                  Top = 4
                  Width = 360
                  Height = 17
                  Caption = 'Usu'#225'rio com habilita'#231#227'o gen'#233'rica para janelas de Confec'#231#227'o.'
                  TabOrder = 0
                  UpdType = utYes
                  ValCheck = #0
                  ValUncheck = #0
                  OldValor = #0
                end
              end
            end
          end
          object Panel9: TPanel
            Left = 0
            Top = 81
            Width = 404
            Height = 100
            Align = alTop
            BevelOuter = bvNone
            TabOrder = 1
            ExplicitWidth = 531
            object GroupBox3: TGroupBox
              Left = 8
              Top = 20
              Width = 381
              Height = 73
              TabOrder = 0
              object PnTexteis: TPanel
                Left = 2
                Top = 15
                Width = 377
                Height = 56
                Align = alClient
                BevelOuter = bvNone
                TabOrder = 0
                ExplicitWidth = 505
                ExplicitHeight = 40
                object CkHabTexTecelagem: TdmkCheckBox
                  Left = 8
                  Top = 4
                  Width = 360
                  Height = 17
                  Caption = 'Usu'#225'rio com habilita'#231#227'o gen'#233'rica para janelas de Tecelagem.'
                  TabOrder = 0
                  UpdType = utYes
                  ValCheck = #0
                  ValUncheck = #0
                  OldValor = #0
                end
                object CkHabTexTinturaria: TdmkCheckBox
                  Left = 8
                  Top = 24
                  Width = 360
                  Height = 17
                  Caption = 'Usu'#225'rio com habilita'#231#227'o gen'#233'rica para janelas de Tinturaria.'
                  TabOrder = 1
                  UpdType = utYes
                  ValCheck = #0
                  ValUncheck = #0
                  OldValor = #0
                end
              end
            end
            object CkHabTextil: TdmkCheckBox
              Left = 8
              Top = 0
              Width = 465
              Height = 17
              Caption = 'Usu'#225'rio com habilita'#231#227'o gen'#233'rica para janelas de T'#202'XTEIS.'
              TabOrder = 1
              OnClick = CkHabTextilClick
              UpdType = utYes
              ValCheck = #0
              ValUncheck = #0
              OldValor = #0
            end
          end
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 297
    Width = 408
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    ExplicitTop = 159
    ExplicitWidth = 364
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 404
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 360
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 341
    Width = 408
    Height = 70
    Align = alBottom
    TabOrder = 3
    ExplicitTop = 203
    ExplicitWidth = 364
    object PnSaiDesis: TPanel
      Left = 262
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      ExplicitLeft = 218
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 260
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 216
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 12
    Top = 11
  end
end
