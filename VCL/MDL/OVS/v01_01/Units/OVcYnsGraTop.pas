unit OVcYnsGraTop;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2, dmkGeral,
  ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt, UMySQLModule, Menus,
  UnInternalConsts2, mySQLDbTables, UnMySQLCuringa, dmkPermissoes, dmkEdit,
  dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup, Grids, DBGrids,
  UnDmkProcFunc, UnDmkENums, dmkDBLookupComboBox, dmkEditCB;

type
  TFmOVcYnsGraTop = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    Label9: TLabel;
    EdNome: TdmkEdit;
    Label1: TLabel;
    DBEdCodigo: TdmkDBEdit;
    Label2: TLabel;
    DBEdNome: TdmkDBEdit;
    QrOVcYnsGraTop: TMySQLQuery;
    DsOVcYnsGraTop: TDataSource;
    QrUsouEm: TMySQLQuery;
    DsUsouEm: TDataSource;
    PMIts: TPopupMenu;
    ItsInclui1: TMenuItem;
    ItsExclui1: TMenuItem;
    ItsAltera1: TMenuItem;
    PMCab: TPopupMenu;
    CabInclui1: TMenuItem;
    CabAltera1: TMenuItem;
    CabExclui1: TMenuItem;
    BtCab: TBitBtn;
    BtIts: TBitBtn;
    DGDados: TDBGrid;
    QrOVcYnsGraTopCodigo: TIntegerField;
    QrOVcYnsGraTopNome: TWideStringField;
    QrOVcYnsGraTopLk: TIntegerField;
    QrOVcYnsGraTopDataCad: TDateField;
    QrOVcYnsGraTopDataAlt: TDateField;
    QrOVcYnsGraTopUserCad: TIntegerField;
    QrOVcYnsGraTopUserAlt: TIntegerField;
    QrOVcYnsGraTopAlterWeb: TSmallintField;
    QrOVcYnsGraTopAWServerID: TIntegerField;
    QrOVcYnsGraTopAWStatSinc: TSmallintField;
    QrOVcYnsGraTopAtivo: TSmallintField;
    Label5: TLabel;
    DBEdit1: TDBEdit;
    QrOVcYnsGraTopSigla: TWideStringField;
    Label3: TLabel;
    EdSigla: TdmkEdit;
    RGTolerBasCalc: TdmkRadioGroup;
    RGTolerUnMdida: TdmkRadioGroup;
    EdTolerRndPerc: TdmkEdit;
    Label11: TLabel;
    QrOVcYnsGraTopNO_TolerBasCalc: TWideStringField;
    QrOVcYnsGraTopNO_TolerUnMdida: TWideStringField;
    QrOVcYnsGraTopTolerBasCalc: TSmallintField;
    QrOVcYnsGraTopTolerUnMdida: TSmallintField;
    QrOVcYnsGraTopTolerRndPerc: TFloatField;
    DBRadioGroup1: TDBRadioGroup;
    DBRadioGroup2: TDBRadioGroup;
    DBEdit2: TDBEdit;
    Label4: TLabel;
    QrUsouEmCodigo: TIntegerField;
    QrUsouEmNome: TWideStringField;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrOVcYnsGraTopAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrOVcYnsGraTopBeforeOpen(DataSet: TDataSet);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure QrOVcYnsGraTopAfterScroll(DataSet: TDataSet);
    procedure CabInclui1Click(Sender: TObject);
    procedure CabAltera1Click(Sender: TObject);
    procedure CabExclui1Click(Sender: TObject);
    procedure BtCabClick(Sender: TObject);
    procedure BtItsClick(Sender: TObject);
    procedure ItsInclui1Click(Sender: TObject);
    procedure ItsAltera1Click(Sender: TObject);
    procedure PMCabPopup(Sender: TObject);
    procedure PMItsPopup(Sender: TObject);
    procedure QrOVcYnsGraTopBeforeClose(DataSet: TDataSet);
    procedure EdNomeRedefinido(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure LocCod(Atual, Codigo: Integer);
    procedure Va(Para: TVaiPara);
    procedure MostraUsouEm(SQLType: TSQLType);

  public
    { Public declarations }
    FSeq, FCabIni: Integer;
    FLocIni: Boolean;
    //
    procedure ReopenUsouEm();

  end;

var
  FmOVcYnsGraTop: TFmOVcYnsGraTop;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, MyDBCheck, DmkDAC_PF, UnOVS_Jan;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmOVcYnsGraTop.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmOVcYnsGraTop.MostraUsouEm(SQLType: TSQLType);
begin
  //
end;

procedure TFmOVcYnsGraTop.PMCabPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemCabUpd(CabAltera1, QrOVcYnsGraTop);
  MyObjects.HabilitaMenuItemCabDel(CabExclui1, QrOVcYnsGraTop, QrUsouEm);
end;

procedure TFmOVcYnsGraTop.PMItsPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(ItsInclui1, QrOVcYnsGraTop);
  MyObjects.HabilitaMenuItemItsUpd(ItsAltera1, QrUsouEm);
  MyObjects.HabilitaMenuItemItsDel(ItsExclui1, QrUsouEm);
end;

procedure TFmOVcYnsGraTop.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrOVcYnsGraTopCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmOVcYnsGraTop.DefParams;
begin
  VAR_GOTOTABELA := 'ovcynsgratop';
  VAR_GOTOMYSQLTABLE := QrOVcYnsGraTop;
  VAR_GOTONEG := gotoPiZ;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT ');
  VAR_SQLx.Add('ELT(ygt.TolerBasCalc+1, "Medida", "Percentual") NO_TolerBasCalc, ');
  VAR_SQLx.Add('ELT(ygt.TolerUnMdida+1, "cm", "inch") NO_TolerUnMdida, ygt.* ');
  VAR_SQLx.Add('FROM ovcynsgratop ygt ');
  //
  VAR_SQL1.Add('WHERE ygt.Codigo>0');
  VAR_SQL1.Add('AND ygt.Codigo=:P0');
  //
  //VAR_SQL2.Add('AND CodUsu=:P0');
  //
  VAR_SQLa.Add('AND ygt.Nome Like :P0');
  //
end;

procedure TFmOVcYnsGraTop.EdNomeRedefinido(Sender: TObject);
begin
  if EdSigla.Text = '' then
    EdSigla.Text := Copy(EdNome.Text, 1, EdSigla.MaxLength);
end;

procedure TFmOVcYnsGraTop.ItsAltera1Click(Sender: TObject);
begin
  MostraUsouEm(stUpd);
end;

procedure TFmOVcYnsGraTop.CabExclui1Click(Sender: TObject);
begin
  Geral.MB_Info('A��o n�o implementada! Solicite � Dermatek:' + sLineBreak +
  Caption + sLineBreak + TMenuItem(Sender).Name);
end;

procedure TFmOVcYnsGraTop.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmOVcYnsGraTop.QueryPrincipalAfterOpen;
begin
end;

procedure TFmOVcYnsGraTop.ReopenUsouEm();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrUsouEm, Dmod.MyDB, [
  'SELECT ymc.Codigo, ymc.Nome ',
  'FROM ovcynsmedtop ymt ',
  'LEFT JOIN ovcynsmedcad ymc ON ymc.Codigo=ymt.Tobiko ',
  'WHERE ymt.Tobiko=' + Geral.FF0(QrOVcYnsGraTopCodigo.Value),
  '']);
end;


procedure TFmOVcYnsGraTop.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmOVcYnsGraTop.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmOVcYnsGraTop.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmOVcYnsGraTop.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmOVcYnsGraTop.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmOVcYnsGraTop.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmOVcYnsGraTop.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrOVcYnsGraTopCodigo.Value;
  Close;
end;

procedure TFmOVcYnsGraTop.ItsInclui1Click(Sender: TObject);
begin
  MostraUsouEm(stIns);
end;

procedure TFmOVcYnsGraTop.CabAltera1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrOVcYnsGraTop, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'ovcynsgratop');
end;

procedure TFmOVcYnsGraTop.BtConfirmaClick(Sender: TObject);
var
  Nome, Sigla: String;
  Codigo, TolerBasCalc, TolerUnMdida: Integer;
  TolerRndPerc: Double;
  SQLType: TSQLType;
begin
  SQLType        := ImgTipo.SQLType;
  Codigo         := EdCodigo.ValueVariant;
  Nome           := EdNome.ValueVariant;
  Sigla          := EdSigla.ValueVariant;
  TolerBasCalc   := RGTolerBasCalc.ItemIndex;
  TolerUnMdida   := RGTolerUnMdida.ItemIndex;
  TolerRndPerc   := EDTolerRndPerc.ValueVariant;
  //
  if MyObjects.FIC(Length(Nome) = 0, EdNome, 'Defina uma descri��o!') then Exit;
  //
  Codigo := UMyMod.BPGS1I32('ovcynsgratop', 'Codigo', '', '', tsPos, SQLType, Codigo);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'ovcynsgratop', False, [
  'Nome', 'Sigla', 'TolerBasCalc',
  'TolerUnMdida', 'TolerRndPerc'], [
  'Codigo'], [
  Nome, Sigla, TolerBasCalc,
  TolerUnMdida, TolerRndPerc], [
  Codigo], True) then
  begin
    ImgTipo.SQLType := stLok;
    PnDados.Visible := True;
    PnEdita.Visible := False;
    GOTOy.BotoesSb(ImgTipo.SQLType);
    //
    LocCod(Codigo, Codigo);
    if FSeq = 1 then Close;
  end;
end;

procedure TFmOVcYnsGraTop.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := EdCodigo.ValueVariant;
  if ImgTipo.SQLType = stIns then
    UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'ovcynsgratop', Codigo);
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'ovcynsgratop', 'Codigo');
end;

procedure TFmOVcYnsGraTop.BtItsClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMIts, BtIts);
end;

procedure TFmOVcYnsGraTop.BtCabClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMCab, BtCab);
end;

procedure TFmOVcYnsGraTop.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  GBEdita.Align := alClient;
  DGDados.Align := alClient;
  CriaOForm;
  FSeq := 0;
  QrOVcYnsGraTop.Database := Dmod.MyDB;
end;

procedure TFmOVcYnsGraTop.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrOVcYnsGraTopCodigo.Value, LaRegistro.Caption);
end;

procedure TFmOVcYnsGraTop.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmOVcYnsGraTop.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrOVcYnsGraTopCodigo.Value, LaRegistro.Caption);
end;

procedure TFmOVcYnsGraTop.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmOVcYnsGraTop.QrOVcYnsGraTopAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmOVcYnsGraTop.QrOVcYnsGraTopAfterScroll(DataSet: TDataSet);
begin
  ReopenUsouEm();
end;

procedure TFmOVcYnsGraTop.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //
  if FSeq = 1 then
    CabInclui1Click(Self)
  else
  if (not FLocIni) and (FCabIni <> 0)  then
  begin
    LocCod(FCabIni, FCabIni);
    if QrOVcYnsGraTopCodigo.Value <> FCabIni then
      Geral.MB_Aviso('Grupo n�o localizado!');
    FLocIni := True;
  end;
end;

procedure TFmOVcYnsGraTop.SbQueryClick(Sender: TObject);
begin
  LocCod(QrOVcYnsGraTopCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'ovcynsgratop', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmOVcYnsGraTop.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmOVcYnsGraTop.CabInclui1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrOVcYnsGraTop, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'ovcynsgratop');
end;

procedure TFmOVcYnsGraTop.QrOVcYnsGraTopBeforeClose(
  DataSet: TDataSet);
begin
  QrUsouEm.Close;
end;

procedure TFmOVcYnsGraTop.QrOVcYnsGraTopBeforeOpen(DataSet: TDataSet);
begin
  QrOVcYnsGraTopCodigo.DisplayFormat := FFormatFloat;
end;

end.

