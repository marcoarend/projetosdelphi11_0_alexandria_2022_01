unit OVpLayFld;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, DBCtrls, dmkLabel, Mask,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkDBEdit, dmkGeral, Variants,
  dmkValUsu, dmkImage, UnDmkEnums, dmkRadioGroup;

type
  TFmOVpLayFld = class(TForm)
    Panel1: TPanel;
    CkContinuar: TCheckBox;
    GroupBox1: TGroupBox;
    DBEdCodigo: TdmkDBEdit;
    Label5: TLabel;
    DBEdNome: TDBEdit;
    Label3: TLabel;
    GroupBox2: TGroupBox;
    EdConta: TdmkEdit;
    Label6: TLabel;
    EdNome: TdmkEdit;
    Label7: TLabel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    QrEntidades: TmySQLQuery;
    QrEntidadesCodigo: TIntegerField;
    QrEntidadesCodUsu: TIntegerField;
    QrEntidadesNome: TWideStringField;
    DsEntidades: TDataSource;
    DBEdControle: TdmkDBEdit;
    Label2: TLabel;
    EdCampo: TdmkEdit;
    Label1: TLabel;
    EdColuna: TdmkEdit;
    Label4: TLabel;
    RGDataType: TdmkRadioGroup;
    LaTamanho: TLabel;
    EdTamanho: TdmkEdit;
    Label8: TLabel;
    EdFldType: TdmkEdit;
    Label9: TLabel;
    EdFldNull: TdmkEdit;
    Label10: TLabel;
    EdFldDefault: TdmkEdit;
    EdFldKey: TdmkEdit;
    Label11: TLabel;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure RGDataTypeClick(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenCadastro_Com_Itens_ITS(Conta: Integer);
  public
    { Public declarations }
    FQrCab, FQrIts: TmySQLQuery;
    FDsCab: TDataSource;
  end;

  var
  FmOVpLayFld: TFmOVpLayFld;

implementation

uses UnMyObjects, Module, UMySQLModule, UnInternalConsts, MyDBCheck, DmkDAC_PF;

{$R *.DFM}

procedure TFmOVpLayFld.BtOKClick(Sender: TObject);
const
  sProcName = 'TFmOVpLayFld.BtOKClick()';
var
  Campo, Nome, DataType, FldType, FldNull, FldDefault, FldKey, Tamanho: String;
  Codigo, Controle, Conta, Coluna: Integer;
  SQLType: TSQLType;
begin
  if MyObjects.FIC(Trim(EdNome.Text) = '', EdNome,
  'Informe uma descri��o!') then
    Exit;
  if MyObjects.FIC(EdColuna.ValueVariant < 1, EdColuna,
  'Informe a coluna!') then
    Exit;
  if MyObjects.FIC(Trim(EdCampo.Text) = '', EdCampo,
  'Informe o nome do campo!') then
    Exit;
  if MyObjects.FIC(Trim(EdFldType.Text) = '', EdFldType,
  'Informe o nome do campo!') then
    Exit;
  if MyObjects.FIC(RGDataType.ItemIndex < 1, RGDataType,
  'Informe o tipo de dados da coluna!') then
    Exit;
  SQLType        := ImgTipo.SQLType;
  Codigo         := Geral.IMV(DBEdCodigo.Text);
  Controle       := Geral.IMV(DBEdControle.Text);
  Conta          := EdConta.ValueVariant;
  Coluna         := EdColuna.ValueVariant;
  Campo          := EdCampo.ValueVariant;
  Nome           := EdNome.ValueVariant;
  FldType        := EdFldType.ValueVariant;
  FldNull        := EdFldNull.ValueVariant;
  FldDefault     := EdFldDefault.ValueVariant;
  FldKey         := EdFldKey.ValueVariant;
  //
{
  if EdTamanho.Enabled then
  begin
    Tamanho      := EdTamanho.ValueVariant;
    if MyObjects.FIC(Tamanho < 1, EdTamanho,
    'Informe o tamanho do campo!') then
      Exit;
  end else
    Tamanho      := 0;
}
  Tamanho := EdTamanho.ValueVariant;
  //DataType       := ;
  case RGDataType.ItemIndex of
    1: DataType  := 'C'; // Character
    2: DataType  := 'D'; // DateTime
    3: DataType  := 'I'; // Integer
    4: DataType  := 'N'; // Float
    else
    begin
      DataType   := '?';
      Geral.MB_Erro('Tipo de dados n�o implementado em ' + sProcName);
    end;
  end;
  //
  Conta := UMyMod.BPGS1I32('ovplayfld', 'Conta', '', '', tsPos, SQLType, Conta);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'ovplayfld', False, [
  'Codigo', 'Controle', 'Coluna',
  'Campo', 'Nome', 'DataType',
  'Tamanho', 'FldType', 'FldNull',
  'FldDefault', 'FldKey'], [
  'Conta'], [
  Codigo, Controle, Coluna,
  Campo, Nome, DataType,
  Tamanho, FldType, FldNull,
  FldDefault, FldKey], [
  Conta], True) then
  begin
    ReopenCadastro_Com_Itens_ITS(Controle);
    if CkContinuar.Checked then
    begin
      ImgTipo.SQLType          := stIns;
      EdConta.ValueVariant     := 0;
      EdNome.ValueVariant      := '';
      EdCampo.ValueVariant     := '';
      EdNome.SetFocus;
    end else Close;
  end;
end;

procedure TFmOVpLayFld.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmOVpLayFld.FormActivate(Sender: TObject);
begin
  DBEdCodigo.DataSource := FDsCab;
  DBEdControle.DataSource := FDsCab;
  DBEdNome.DataSource := FDsCab;
  MyObjects.CorIniComponente();
end;

procedure TFmOVpLayFld.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
end;

procedure TFmOVpLayFld.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmOVpLayFld.ReopenCadastro_Com_Itens_ITS(Conta: Integer);
begin
  if FQrIts <> nil then
  begin
    UnDmkDAC_PF.AbreQuery(FQrIts, FQrIts.Database);
    //
    if Conta <> 0 then
      FQrIts.Locate('Conta', Conta, []);
  end;
end;

procedure TFmOVpLayFld.RGDataTypeClick(Sender: TObject);
var
  Habilitado: Boolean;
begin
  Habilitado := RGDataType.ItemIndex = 1;
  //LaTamanho.Enabled := Habilitado;
  //EdTamanho.Enabled := Habilitado;
  case RGDataType.ItemIndex of
    1: EdFldType.ValueVariant := 'varchar(60)';
    2: EdFldType.ValueVariant := 'datetime';
    3: EdFldType.ValueVariant := 'int(11)';
    4: EdFldType.ValueVariant := 'double(15,3)';
  end;
  case RGDataType.ItemIndex of
    1: EdTamanho.ValueVariant := '60';
    2: EdTamanho.ValueVariant := '';
    3: EdTamanho.ValueVariant := '11';
    4: EdTamanho.ValueVariant := '15,3';
  end;
  case RGDataType.ItemIndex of
    1: EdFldNull.ValueVariant := 'YES';
    2: EdFldNull.ValueVariant := 'NO';
    3: EdFldNull.ValueVariant := 'NO';
    4: EdFldNull.ValueVariant := 'NO';
  end;
  case RGDataType.ItemIndex of
    1: EdFldDefault.ValueVariant := '';
    2: EdFldDefault.ValueVariant := '0000-00-00 00:00:00';
    3: EdFldDefault.ValueVariant := '0';
    4: EdFldDefault.ValueVariant := '0.000';
  end;
end;

end.
