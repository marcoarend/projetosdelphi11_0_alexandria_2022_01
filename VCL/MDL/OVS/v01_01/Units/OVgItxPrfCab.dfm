object FmOVgItxPrfCab: TFmOVgItxPrfCab
  Left = 368
  Top = 194
  Caption = 'OVS-EXCAO-006 :: Perfis de Inspe'#231#245'es de T'#234'xteis'
  ClientHeight = 639
  ClientWidth = 1008
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnEdita: TPanel
    Left = 0
    Top = 96
    Width = 1008
    Height = 543
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    Visible = False
    object GBEdita: TGroupBox
      Left = 0
      Top = 0
      Width = 1008
      Height = 125
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 0
      object Label7: TLabel
        Left = 16
        Top = 16
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label9: TLabel
        Left = 76
        Top = 16
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
        Color = clBtnFace
        ParentColor = False
      end
      object EdCodigo: TdmkEdit
        Left = 16
        Top = 32
        Width = 56
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Codigo'
        UpdCampo = 'Codigo'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdNome: TdmkEdit
        Left = 76
        Top = 32
        Width = 633
        Height = 21
        TabOrder = 1
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Nome'
        UpdCampo = 'Nome'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
    end
    object GBConfirma: TGroupBox
      Left = 0
      Top = 480
      Width = 1008
      Height = 63
      Align = alBottom
      TabOrder = 1
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 12
        Top = 17
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
      end
      object Panel1: TPanel
        Left = 868
        Top = 15
        Width = 138
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 7
          Top = 2
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
    end
  end
  object PnDados: TPanel
    Left = 0
    Top = 96
    Width = 1008
    Height = 543
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    object GBDados: TGroupBox
      Left = 0
      Top = 0
      Width = 1008
      Height = 213
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 1
      object Panel6: TPanel
        Left = 2
        Top = 15
        Width = 1004
        Height = 78
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 0
        object Label1: TLabel
          Left = 8
          Top = 0
          Width = 36
          Height = 13
          Caption = 'C'#243'digo:'
          FocusControl = DBEdCodigo
        end
        object Label2: TLabel
          Left = 68
          Top = 0
          Width = 51
          Height = 13
          Caption = 'Descri'#231#227'o:'
          FocusControl = DBEdNome
        end
        object Label20: TLabel
          Left = 752
          Top = 40
          Width = 223
          Height = 13
          Caption = 'Plano de Amostragem e Regime de Qualidade :'
        end
        object Label6: TLabel
          Left = 8
          Top = 40
          Width = 30
          Height = 13
          Caption = 'Artigo:'
        end
        object Label8: TLabel
          Left = 68
          Top = 40
          Width = 95
          Height = 13
          Caption = 'Descri'#231#227'o do artigo:'
        end
        object Label3: TLabel
          Left = 804
          Top = 0
          Width = 55
          Height = 13
          Caption = 'Refer'#234'ncia:'
          FocusControl = DBEdit1
        end
        object DBEdCodigo: TdmkDBEdit
          Left = 8
          Top = 16
          Width = 56
          Height = 21
          TabStop = False
          DataField = 'Codigo'
          DataSource = DsOVgItxPrfCab
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ReadOnly = True
          ShowHint = True
          TabOrder = 0
          UpdType = utYes
          Alignment = taRightJustify
        end
        object DBEdNome: TdmkDBEdit
          Left = 68
          Top = 16
          Width = 681
          Height = 21
          Color = clWhite
          DataField = 'Nome'
          DataSource = DsOVgItxPrfCab
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          UpdType = utYes
          Alignment = taLeftJustify
        end
        object dmkDBEdit7: TdmkDBEdit
          Left = 752
          Top = 56
          Width = 49
          Height = 21
          Color = clWhite
          DataSource = DsOVgItxPrfCab
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          UpdType = utYes
          Alignment = taLeftJustify
        end
        object dmkDBEdit8: TdmkDBEdit
          Left = 804
          Top = 56
          Width = 193
          Height = 21
          Color = clWhite
          DataSource = DsOVgItxPrfCab
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          UpdType = utYes
          Alignment = taLeftJustify
        end
        object DBEdit4: TDBEdit
          Left = 8
          Top = 56
          Width = 56
          Height = 21
          DataField = 'SeqGrupo'
          DataSource = DsOVgItxPrfCab
          TabOrder = 4
        end
        object DBEdit5: TDBEdit
          Left = 68
          Top = 56
          Width = 681
          Height = 21
          DataField = 'NO_Referencia'
          DataSource = DsOVgItxPrfCab
          TabOrder = 5
        end
        object DBEdit1: TDBEdit
          Left = 804
          Top = 16
          Width = 189
          Height = 21
          DataField = 'Referencia'
          DataSource = DsOVgItxPrfCab
          TabOrder = 6
        end
        object DBCheckBox1: TDBCheckBox
          Left = 752
          Top = 20
          Width = 49
          Height = 17
          Caption = 'Ativo'
          DataField = 'Ativo'
          DataSource = DsOVgItxPrfCab
          TabOrder = 7
          ValueChecked = '1'
          ValueUnchecked = '0'
        end
      end
      object GroupBox3: TGroupBox
        Left = 2
        Top = 93
        Width = 1004
        Height = 118
        Align = alClient
        Caption = ' Configura'#231#245'es de Inspe'#231#227'o: '
        TabOrder = 1
        TabStop = True
        object Panel7: TPanel
          Left = 2
          Top = 15
          Width = 1000
          Height = 101
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          TabStop = True
          object Label11: TLabel
            Left = 8
            Top = 4
            Width = 94
            Height = 13
            Caption = 'Tabela de exa'#231#245'es:'
          end
          object DBEdit7: TDBEdit
            Left = 8
            Top = 20
            Width = 56
            Height = 21
            DataField = 'OVcYnsExg'
            DataSource = DsOVgItxPrfCab
            TabOrder = 0
          end
          object DBEdit9: TDBEdit
            Left = 68
            Top = 20
            Width = 925
            Height = 21
            DataField = 'NO_OVcYnsExg'
            DataSource = DsOVgItxPrfCab
            TabOrder = 1
          end
          object DBRadioGroup1: TDBRadioGroup
            Left = 8
            Top = 44
            Width = 985
            Height = 45
            Caption = ' Permiss'#227'o de finaliza'#231#227'o da inspe'#231#227'o: '
            Columns = 4
            DataField = 'PermiFinHow'
            DataSource = DsOVgItxPrfCab
            Items.Strings = (
              '0 - Indefinido'
              '1 - Somente ao finalizar toda inspe'#231#227'o'
              '2 - Ao atingir a pontua'#231#227'o de reprova'#231#227'o'
              '3 - A qualquer momento')
            TabOrder = 2
            Values.Strings = (
              '0'
              '1'
              '2'
              '3'
              '4'
              '5'
              '6'
              '7'
              '8'
              '9')
          end
        end
      end
    end
    object GBCntrl: TGroupBox
      Left = 0
      Top = 479
      Width = 1008
      Height = 64
      Align = alBottom
      TabOrder = 0
      object Panel5: TPanel
        Left = 2
        Top = 15
        Width = 172
        Height = 47
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 174
        Top = 15
        Width = 311
        Height = 47
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel3: TPanel
        Left = 485
        Top = 15
        Width = 521
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object Panel2: TPanel
          Left = 388
          Top = 0
          Width = 133
          Height = 47
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 0
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
        object BtCab: TBitBtn
          Tag = 1000076
          Left = 4
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Perfil'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtCabClick
        end
        object BtIts: TBitBtn
          Tag = 110
          Left = 128
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&???'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          Visible = False
          OnClick = BtItsClick
        end
      end
    end
    object PCItens: TPageControl
      Left = 0
      Top = 213
      Width = 1008
      Height = 244
      ActivePage = TabSheet1
      Align = alTop
      TabOrder = 2
      object TabSheet1: TTabSheet
        Caption = ' Tabela de Exa'#231#245'es'
        object PnGrids: TPanel
          Left = 0
          Top = 0
          Width = 1000
          Height = 216
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          object DBGMixOpc: TDBGrid
            Left = 412
            Top = 117
            Width = 588
            Height = 99
            Align = alRight
            DataSource = DsOVcYnsMixOpc
            TabOrder = 0
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            Columns = <
              item
                Expanded = False
                FieldName = 'Controle'
                Width = 48
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Nome'
                Width = 338
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NO_Magnitude'
                Title.Caption = 'Magnitude'
                Width = 100
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'PontNeg'
                Title.Caption = 'Pontos neg.'
                Visible = True
              end>
          end
          object PnGrids2: TPanel
            Left = 0
            Top = 0
            Width = 1000
            Height = 117
            Align = alTop
            BevelOuter = bvNone
            TabOrder = 1
            object DBGExgTop: TDBGrid
              Left = 0
              Top = 0
              Width = 1000
              Height = 117
              Align = alClient
              DataSource = DsOVcYnsExgTop
              TabOrder = 0
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              Columns = <
                item
                  Expanded = False
                  FieldName = 'Controle'
                  Title.Caption = 'ID'
                  Width = 56
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Topyko'
                  Title.Caption = 'T'#243'pico'
                  Width = 56
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NO_TOPYKO'
                  Title.Caption = 'Descri'#231#227'o do T'#243'pico'
                  Width = 335
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NO_Mensuravl'
                  Title.Caption = 'Medir?'
                  Width = 37
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'MedidCer'
                  Title.Caption = 'Medida certa'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'MedidMin'
                  Title.Caption = 'Medida m'#237'n.'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'MedidMax'
                  Title.Caption = 'Medida m'#225'x.'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NO_TolerBasCalc'
                  Title.Caption = 'Bas.Calc.Toler.'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'TolerSglUnMdid'
                  Title.Caption = 'Unidade'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NO_TlrRndUsaMin'
                  Title.Caption = 'M'#237'nimo?'
                  Width = 46
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'TlrRndPrcMin'
                  Title.Caption = 'Marg m'#237'n. med/ %:'
                  Width = 71
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NO_TlrRndUsaMax'
                  Title.Caption = 'M'#225'ximo?'
                  Width = 48
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'TlrRndPrcMax'
                  Title.Caption = 'Marg m'#225'x. med/ %:'
                  Visible = True
                end>
            end
          end
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 12
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 216
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 216
      Top = 0
      Width = 744
      Height = 52
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 371
        Height = 32
        Caption = 'Perfis de Inspe'#231#245'es de T'#234'xteis'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 371
        Height = 32
        Caption = 'Perfis de Inspe'#231#245'es de T'#234'xteis'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 371
        Height = 32
        Caption = 'Perfis de Inspe'#231#245'es de T'#234'xteis'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 52
    Width = 1008
    Height = 44
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = BtCab
    Left = 120
    Top = 64
  end
  object QrOVgItxPrfCab: TMySQLQuery
    Database = Dmod.ZZDB
    BeforeOpen = QrOVgItxPrfCabBeforeOpen
    AfterOpen = QrOVgItxPrfCabAfterOpen
    BeforeClose = QrOVgItxPrfCabBeforeClose
    AfterScroll = QrOVgItxPrfCabAfterScroll
    SQL.Strings = (
      'SELECT igc.*, ymc.Nome NO_OVcYnsMed, ycc.Nome NO_OVcYnsChk,'
      'dlo.Nome NO_Local, ref.Nome NO_Referencia, ref.Referencia,'
      'isc.Nome NO_ZtatusIsp, '
      'IF(igc.DtHrAbert  <= "1899-12-30", "",  '
      '  DATE_FORMAT(igc.DtHrAbert, "%d/%m/%Y %h:%i")) DtHrAbert_TXT,  '
      'IF(igc.DtHrFecha  <= "1899-12-30", "",  '
      '  DATE_FORMAT(igc.DtHrFecha, "%d/%m/%Y %h:%i")) DtHrFecha_TXT'
      'FROM ovgispgercab igc'
      'LEFT JOIN ovdlocal dlo ON dlo.Codigo=igc.Local  '
      'LEFT JOIN ovdreferencia ref ON ref.Codigo=igc.SeqGrupo '
      'LEFT JOIN ovcynsmedcad ymc ON ymc.Codigo=igc.OVcYnsMed'
      'LEFT JOIN ovcynschkcad ycc ON ycc.Codigo=igc.OVcYnsChk'
      'LEFT JOIN OVgIspStaCad isc ON isc.Codigo=igc.ZtatusIsp'
      ''
      'WHERE igc.Codigo > 0')
    Left = 620
    Top = 5
    object QrOVgItxPrfCabCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrOVgItxPrfCabNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QrOVgItxPrfCabSeqGrupo: TIntegerField
      FieldName = 'SeqGrupo'
      Required = True
    end
    object QrOVgItxPrfCabOVcYnsExg: TIntegerField
      FieldName = 'OVcYnsExg'
      Required = True
    end
    object QrOVgItxPrfCabLk: TIntegerField
      FieldName = 'Lk'
      Required = True
    end
    object QrOVgItxPrfCabDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrOVgItxPrfCabDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrOVgItxPrfCabUserCad: TIntegerField
      FieldName = 'UserCad'
      Required = True
    end
    object QrOVgItxPrfCabUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Required = True
    end
    object QrOVgItxPrfCabAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Required = True
    end
    object QrOVgItxPrfCabAWServerID: TIntegerField
      FieldName = 'AWServerID'
      Required = True
    end
    object QrOVgItxPrfCabAWStatSinc: TSmallintField
      FieldName = 'AWStatSinc'
      Required = True
    end
    object QrOVgItxPrfCabAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
    object QrOVgItxPrfCabNO_Referencia: TWideStringField
      FieldName = 'NO_Referencia'
      Size = 100
    end
    object QrOVgItxPrfCabNO_OVcYnsExg: TWideStringField
      FieldName = 'NO_OVcYnsExg'
      Size = 60
    end
    object QrOVgItxPrfCabReferencia: TWideStringField
      FieldName = 'Referencia'
      Size = 100
    end
    object QrOVgItxPrfCabPermiFinHow: TSmallintField
      FieldName = 'PermiFinHow'
    end
  end
  object DsOVgItxPrfCab: TDataSource
    DataSet = QrOVgItxPrfCab
    Left = 620
    Top = 53
  end
  object PMIts: TPopupMenu
    Left = 668
    Top = 544
    object ItsInclui1: TMenuItem
      Caption = '&Adiciona'
      Enabled = False
    end
    object ItsAltera1: TMenuItem
      Caption = '&Edita'
      Enabled = False
    end
    object ItsExclui1: TMenuItem
      Caption = '&Remove'
      Enabled = False
    end
  end
  object PMCab: TPopupMenu
    OnPopup = PMCabPopup
    Left = 544
    Top = 544
    object CabInclui1: TMenuItem
      Caption = '&Inclui'
      OnClick = CabInclui1Click
    end
    object CabAltera1: TMenuItem
      Caption = '&Altera'
      Enabled = False
      OnClick = CabAltera1Click
    end
    object CabExclui1: TMenuItem
      Caption = '&Exclui'
      Enabled = False
      OnClick = CabExclui1Click
    end
  end
  object QrOVdReferencia: TMySQLQuery
    Database = Dmod.ZZDB
    SQL.Strings = (
      'SELECT Codigo, Referencia, Nome '
      'FROM ovdreferencia '
      'ORDER BY Nome ')
    Left = 676
    Top = 370
    object QrOVdReferenciaCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrOVdReferenciaNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QrOVdReferenciaReferencia: TWideStringField
      FieldName = 'Referencia'
      Size = 60
    end
  end
  object DsOVdReferencia: TDataSource
    DataSet = QrOVdReferencia
    Left = 676
    Top = 418
  end
  object QrOVcYnsExgTop: TMySQLQuery
    Database = Dmod.ZZDB
    BeforeClose = QrOVcYnsExgTopBeforeClose
    AfterScroll = QrOVcYnsExgTopAfterScroll
    SQL.Strings = (
      
        'SELECT IF(ymt.Mensuravl=0,"", ELT(ymt.TolerBasCalc+1, "Medida", ' +
        '"Percentual")) NO_TolerBasCalc, '
      'IF(ymt.Mensuravl=1, "SIM", "N'#195'O") NO_Mensuravl,'
      'IF(ymt.TlrRndUsaMin=1, "SIM", "N'#195'O") NO_TlrRndUsaMin,'
      'IF(ymt.TlrRndUsaMax=1, "SIM", "N'#195'O") NO_TlrRndUsaMax,'
      'ygt.Nome NO_TOPYKO, ymt.* '
      'FROM ovcynsexgtop ymt '
      'LEFT JOIN ovcynsmixtop ygt ON ygt.Codigo=ymt.Topyko '
      'WHERE ymt.Codigo>0')
    Left = 280
    Top = 345
    object QrOVcYnsExgTopNO_TolerBasCalc: TWideStringField
      FieldName = 'NO_TolerBasCalc'
      Size = 10
    end
    object QrOVcYnsExgTopNO_TOPYKO: TWideStringField
      FieldName = 'NO_TOPYKO'
      Size = 60
    end
    object QrOVcYnsExgTopNO_Mensuravl: TWideStringField
      FieldName = 'NO_Mensuravl'
      Required = True
      Size = 3
    end
    object QrOVcYnsExgTopNO_TlrRndUsaMin: TWideStringField
      FieldName = 'NO_TlrRndUsaMin'
      Required = True
      Size = 3
    end
    object QrOVcYnsExgTopNO_TlrRndUsaMax: TWideStringField
      FieldName = 'NO_TlrRndUsaMax'
      Required = True
      Size = 3
    end
    object QrOVcYnsExgTopCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrOVcYnsExgTopControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrOVcYnsExgTopTopyko: TIntegerField
      FieldName = 'Topyko'
      Required = True
    end
    object QrOVcYnsExgTopTolerBasCalc: TSmallintField
      FieldName = 'TolerBasCalc'
      Required = True
    end
    object QrOVcYnsExgTopTolerSglUnMdid: TWideStringField
      FieldName = 'TolerSglUnMdid'
      Size = 15
    end
    object QrOVcYnsExgTopTlrRndUsaMin: TSmallintField
      FieldName = 'TlrRndUsaMin'
      Required = True
    end
    object QrOVcYnsExgTopTlrRndPrcMin: TFloatField
      FieldName = 'TlrRndPrcMin'
      Required = True
      DisplayFormat = '#,###,##0.000;-#,###,##0.000; '
    end
    object QrOVcYnsExgTopTlrRndUsaMax: TSmallintField
      FieldName = 'TlrRndUsaMax'
      Required = True
    end
    object QrOVcYnsExgTopTlrRndPrcMax: TFloatField
      FieldName = 'TlrRndPrcMax'
      Required = True
      DisplayFormat = '#,###,##0.000;-#,###,##0.000; '
    end
    object QrOVcYnsExgTopLk: TIntegerField
      FieldName = 'Lk'
      Required = True
    end
    object QrOVcYnsExgTopDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrOVcYnsExgTopDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrOVcYnsExgTopUserCad: TIntegerField
      FieldName = 'UserCad'
      Required = True
    end
    object QrOVcYnsExgTopUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Required = True
    end
    object QrOVcYnsExgTopAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Required = True
    end
    object QrOVcYnsExgTopAWServerID: TIntegerField
      FieldName = 'AWServerID'
      Required = True
    end
    object QrOVcYnsExgTopAWStatSinc: TSmallintField
      FieldName = 'AWStatSinc'
      Required = True
    end
    object QrOVcYnsExgTopAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
    object QrOVcYnsExgTopMedidCer: TFloatField
      FieldName = 'MedidCer'
      Required = True
      DisplayFormat = '#,###,##0.000;-#,###,##0.000; '
    end
    object QrOVcYnsExgTopMedidMin: TFloatField
      FieldName = 'MedidMin'
      Required = True
      DisplayFormat = '#,###,##0.000;-#,###,##0.000; '
    end
    object QrOVcYnsExgTopMedidMax: TFloatField
      FieldName = 'MedidMax'
      Required = True
      DisplayFormat = '#,###,##0.000;-#,###,##0.000; '
    end
    object QrOVcYnsExgTopMensuravl: TSmallintField
      FieldName = 'Mensuravl'
    end
  end
  object DsOVcYnsExgTop: TDataSource
    DataSet = QrOVcYnsExgTop
    Left = 280
    Top = 393
  end
  object QrOVcYnsMixOpc: TMySQLQuery
    Database = Dmod.ZZDB
    SQL.Strings = (
      'SELECT mag.Nome NO_Magnitude, opc.* '
      'FROM ovcynsmixopc opc'
      'LEFT JOIN ovcynsqstmag mag ON  mag.Codigo=opc.Magnitude'
      'WHERE opc.Codigo=0')
    Left = 376
    Top = 345
    object QrOVcYnsMixOpcNO_Magnitude: TWideStringField
      FieldName = 'NO_Magnitude'
      Size = 60
    end
    object QrOVcYnsMixOpcCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrOVcYnsMixOpcControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrOVcYnsMixOpcNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
    object QrOVcYnsMixOpcMagnitude: TIntegerField
      FieldName = 'Magnitude'
      Required = True
    end
    object QrOVcYnsMixOpcPontNeg: TIntegerField
      FieldName = 'PontNeg'
      Required = True
    end
    object QrOVcYnsMixOpcLk: TIntegerField
      FieldName = 'Lk'
      Required = True
    end
    object QrOVcYnsMixOpcDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrOVcYnsMixOpcDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrOVcYnsMixOpcUserCad: TIntegerField
      FieldName = 'UserCad'
      Required = True
    end
    object QrOVcYnsMixOpcUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Required = True
    end
    object QrOVcYnsMixOpcAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Required = True
    end
    object QrOVcYnsMixOpcAWServerID: TIntegerField
      FieldName = 'AWServerID'
      Required = True
    end
    object QrOVcYnsMixOpcAWStatSinc: TSmallintField
      FieldName = 'AWStatSinc'
      Required = True
    end
    object QrOVcYnsMixOpcAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
  end
  object DsOVcYnsMixOpc: TDataSource
    DataSet = QrOVcYnsMixOpc
    Left = 376
    Top = 393
  end
end
