unit OVgIspGerCab;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2, dmkGeral,
  ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt, UMySQLModule, Menus,
  UnInternalConsts2, mySQLDbTables, UnMySQLCuringa, dmkPermissoes, dmkEdit,
  dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup, Grids, DBGrids,
  UnDmkProcFunc, UnDmkENums, Vcl.ComCtrls;

type
  TFmOVgIspGerCab = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    Label9: TLabel;
    EdNome: TdmkEdit;
    QrOVgIspGerCab: TMySQLQuery;
    DsOVgIspGerCab: TDataSource;
    PMIts: TPopupMenu;
    ItsInclui1: TMenuItem;
    ItsExclui1: TMenuItem;
    ItsAltera1: TMenuItem;
    PMCab: TPopupMenu;
    CabInclui1: TMenuItem;
    CabAltera1: TMenuItem;
    CabExclui1: TMenuItem;
    BtCab: TBitBtn;
    BtIts: TBitBtn;
    QrOVgIspGerCabCodigo: TIntegerField;
    QrOVgIspGerCabLocal: TIntegerField;
    QrOVgIspGerCabNrOP: TIntegerField;
    QrOVgIspGerCabSeqGrupo: TIntegerField;
    QrOVgIspGerCabNrReduzidoOP: TIntegerField;
    QrOVgIspGerCabDtHrAbert: TDateTimeField;
    QrOVgIspGerCabDtHrFecha: TDateTimeField;
    QrOVgIspGerCabOVcYnsMed: TIntegerField;
    QrOVgIspGerCabOVcYnsChk: TIntegerField;
    QrOVgIspGerCabLimiteChk: TIntegerField;
    QrOVgIspGerCabLimiteMed: TIntegerField;
    QrOVgIspGerCabZtatusIsp: TIntegerField;
    QrOVgIspGerCabZtatusDtH: TDateTimeField;
    QrOVgIspGerCabZtatusMot: TIntegerField;
    QrOVgIspGerCabLk: TIntegerField;
    QrOVgIspGerCabDataCad: TDateField;
    QrOVgIspGerCabDataAlt: TDateField;
    QrOVgIspGerCabUserCad: TIntegerField;
    QrOVgIspGerCabUserAlt: TIntegerField;
    QrOVgIspGerCabAlterWeb: TSmallintField;
    QrOVgIspGerCabAWServerID: TIntegerField;
    QrOVgIspGerCabAWStatSinc: TSmallintField;
    QrOVgIspGerCabAtivo: TSmallintField;
    QrOVgIspGerCabNO_Local: TWideStringField;
    QrOVgIspGerCabNO_Referencia: TWideStringField;
    QrOVgIspGerCabNome: TWideStringField;
    Panel6: TPanel;
    Label1: TLabel;
    DBEdCodigo: TdmkDBEdit;
    DBEdNome: TdmkDBEdit;
    Label2: TLabel;
    GroupBox2: TGroupBox;
    PnDadosOri: TPanel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label8: TLabel;
    Label10: TLabel;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    DBEdit4: TDBEdit;
    DBEdit5: TDBEdit;
    DBEdit6: TDBEdit;
    GroupBox3: TGroupBox;
    Panel7: TPanel;
    Label11: TLabel;
    Label12: TLabel;
    DBEdit7: TDBEdit;
    DBEdit8: TDBEdit;
    QrOVgIspGerCabNO_OVcYnsMed: TWideStringField;
    QrOVgIspGerCabNO_OVcYnsChk: TWideStringField;
    DBEdit9: TDBEdit;
    DBEdit10: TDBEdit;
    Label16: TLabel;
    DBEdit11: TDBEdit;
    DBEdit12: TDBEdit;
    dmkDBEdit1: TdmkDBEdit;
    Label17: TLabel;
    Label18: TLabel;
    dmkDBEdit2: TdmkDBEdit;
    QrOVgIspGerCabDtHrAbert_TXT: TWideStringField;
    QrOVgIspGerCabDtHrFecha_TXT: TWideStringField;
    PCItens: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    QrOVcYnsMedTop: TMySQLQuery;
    QrOVcYnsMedTopNO_TolerBasCalc: TWideStringField;
    QrOVcYnsMedTopNO_TolerUnMdida: TWideStringField;
    QrOVcYnsMedTopNO_TOBIKO: TWideStringField;
    QrOVcYnsMedTopCodigo: TIntegerField;
    QrOVcYnsMedTopControle: TIntegerField;
    QrOVcYnsMedTopTobiko: TIntegerField;
    QrOVcYnsMedTopTolerBasCalc: TSmallintField;
    QrOVcYnsMedTopTolerUnMdida: TSmallintField;
    QrOVcYnsMedTopTolerRndPerc: TFloatField;
    QrOVcYnsMedTopLk: TIntegerField;
    QrOVcYnsMedTopDataCad: TDateField;
    QrOVcYnsMedTopDataAlt: TDateField;
    QrOVcYnsMedTopUserCad: TIntegerField;
    QrOVcYnsMedTopUserAlt: TIntegerField;
    QrOVcYnsMedTopAlterWeb: TSmallintField;
    QrOVcYnsMedTopAWServerID: TIntegerField;
    QrOVcYnsMedTopAWStatSinc: TSmallintField;
    QrOVcYnsMedTopAtivo: TSmallintField;
    DsOVcYnsMedTop: TDataSource;
    QrOVcYnsMedDim: TMySQLQuery;
    QrOVcYnsMedDimCodigo: TIntegerField;
    QrOVcYnsMedDimControle: TIntegerField;
    QrOVcYnsMedDimConta: TIntegerField;
    QrOVcYnsMedDimCodGrade: TIntegerField;
    QrOVcYnsMedDimCodTam: TWideStringField;
    QrOVcYnsMedDimMedidCer: TFloatField;
    QrOVcYnsMedDimMedidMin: TFloatField;
    QrOVcYnsMedDimMedidMax: TFloatField;
    QrOVcYnsMedDimUlWayInz: TSmallintField;
    QrOVcYnsMedDimLk: TIntegerField;
    QrOVcYnsMedDimDataCad: TDateField;
    QrOVcYnsMedDimDataAlt: TDateField;
    QrOVcYnsMedDimUserCad: TIntegerField;
    QrOVcYnsMedDimUserAlt: TIntegerField;
    QrOVcYnsMedDimAlterWeb: TSmallintField;
    QrOVcYnsMedDimAWServerID: TIntegerField;
    QrOVcYnsMedDimAWStatSinc: TSmallintField;
    QrOVcYnsMedDimAtivo: TSmallintField;
    DsOVcYnsMedDim: TDataSource;
    PnGrids: TPanel;
    DBGTopico: TDBGrid;
    DBGContexto: TDBGrid;
    QrOVcYnsChkCtx: TMySQLQuery;
    QrOVcYnsChkCtxNO_CONTEXTO: TWideStringField;
    QrOVcYnsChkCtxCodigo: TIntegerField;
    QrOVcYnsChkCtxControle: TIntegerField;
    QrOVcYnsChkCtxContexto: TIntegerField;
    DsOVcYnsChkCtx: TDataSource;
    QrOVcYnsChkTop: TMySQLQuery;
    QrOVcYnsChkTopCodigo: TIntegerField;
    QrOVcYnsChkTopControle: TIntegerField;
    QrOVcYnsChkTopConta: TIntegerField;
    QrOVcYnsChkTopTopico: TIntegerField;
    QrOVcYnsChkTopMagnitude: TIntegerField;
    QrOVcYnsChkTopLk: TIntegerField;
    QrOVcYnsChkTopDataCad: TDateField;
    QrOVcYnsChkTopDataAlt: TDateField;
    QrOVcYnsChkTopUserCad: TIntegerField;
    QrOVcYnsChkTopUserAlt: TIntegerField;
    QrOVcYnsChkTopAlterWeb: TSmallintField;
    QrOVcYnsChkTopAWServerID: TIntegerField;
    QrOVcYnsChkTopAWStatSinc: TSmallintField;
    QrOVcYnsChkTopAtivo: TSmallintField;
    QrOVcYnsChkTopNO_Topico: TWideStringField;
    QrOVcYnsChkTopNO_Magnitude: TWideStringField;
    DsOVcYnsChkTop: TDataSource;
    Panel8: TPanel;
    DBGrid1: TDBGrid;
    DBGrid2: TDBGrid;
    LiberarParaInspecao1: TMenuItem;
    dmkDBEdit5: TdmkDBEdit;
    Label19: TLabel;
    dmkDBEdit6: TdmkDBEdit;
    dmkDBEdit3: TdmkDBEdit;
    Label13: TLabel;
    dmkDBEdit4: TdmkDBEdit;
    Label15: TLabel;
    QrOVgIspGerCabNO_ZtatusIsp: TWideStringField;
    QrOVgIspGerCabOVcYnsARQ: TIntegerField;
    Label20: TLabel;
    dmkDBEdit7: TdmkDBEdit;
    dmkDBEdit8: TdmkDBEdit;
    QrOVgIspGerCabNO_OVcYnsARQ: TWideStringField;
    DBRadioGroup1: TDBRadioGroup;
    QrOVgIspGerCabPermiFinHow: TIntegerField;
    QrSumZtatusIsp: TMySQLQuery;
    DsSumZtatusIsp: TDataSource;
    QrSumZtatusIspZtatusIsp: TIntegerField;
    QrSumZtatusIspNO_ZTATUS: TWideStringField;
    QrSumZtatusIspITENS: TLargeintField;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrOVgIspGerCabAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrOVgIspGerCabBeforeOpen(DataSet: TDataSet);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure QrOVgIspGerCabAfterScroll(DataSet: TDataSet);
    procedure CabInclui1Click(Sender: TObject);
    procedure CabAltera1Click(Sender: TObject);
    procedure CabExclui1Click(Sender: TObject);
    procedure BtCabClick(Sender: TObject);
    procedure BtItsClick(Sender: TObject);
    procedure PMCabPopup(Sender: TObject);
    procedure QrOVgIspGerCabBeforeClose(DataSet: TDataSet);
    procedure QrOVcYnsMedTopAfterScroll(DataSet: TDataSet);
    procedure QrOVcYnsMedTopBeforeClose(DataSet: TDataSet);
    procedure QrOVcYnsChkCtxAfterScroll(DataSet: TDataSet);
    procedure QrOVcYnsChkCtxBeforeClose(DataSet: TDataSet);
    procedure LiberarParaInspecao1Click(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    //
    function  AlteraStatus(NovoStatus, Motivo: Integer): Boolean;
    procedure ReopenOVcYnsMedTop(Controle: Integer);
    procedure ReopenOVcYnsMedDim(Conta: Integer);
    procedure ReopenOVcYnsChkCtx(Controle: Integer);
    procedure ReopenOVcYnsChkTop(Conta: Integer);

  public
    { Public declarations }
    FSeq, FCabIni: Integer;
    FLocIni: Boolean;
    //
    procedure LocCod(Atual, Codigo: Integer);

  end;

var
  FmOVgIspGerCab: TFmOVgIspGerCab;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, MyDBCheck, DmkDAC_PF, UnOVS_Jan, UnOVS_PF,
  UnOVS_Consts, ModuleGeral;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmOVgIspGerCab.LiberarParaInspecao1Click(Sender: TObject);
begin
  if Geral.MB_Pergunta('Confirma a libera��o desta inspe��o para download mobile?') =
  ID_YES then
  begin
    AlteraStatus(CO_OVS_IMPORT_ALHEIO_5120_APTO_DOWNMOBILE, 0);
    LocCod(QrOVgIspGerCabCodigo.Value, QrOVgIspGerCabCodigo.Value);
  end;
end;

procedure TFmOVgIspGerCab.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmOVgIspGerCab.PMCabPopup(Sender: TObject);
var
  HI: Boolean;
  ZI: Integer;
begin
  MyObjects.HabilitaMenuItemCabUpd(CabAltera1, QrOVgIspGerCab);
  //
  ZI := QrOVgIspGerCabZtatusIsp.Value;
  HI := (ZI >= CO_OVS_IMPORT_ALHEIO_3972_CONFIGURADO) and
         (ZI <> CO_OVS_IMPORT_ALHEIO_4096_APTO_UPWEBSERVR);
  LiberarParaInspecao1.Enabled := HI;
{
  CO_OVS_IMPORT_ALHEIO_3972_CONFIGURADO = 3072;
  // 4096 - Apto ao Upload (n�o usado >> direto)
  CO_OVS_IMPORT_ALHEIO_4096_APTO_UPWEBSERVR = 4096;
  // 5120 - Apto ao download mobile
  CO_OVS_IMPORT_ALHEIO_5120_APTO_DOWNMOBILE = 5120;
}
end;

procedure TFmOVgIspGerCab.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrOVgIspGerCabCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmOVgIspGerCab.DefParams;
begin
  VAR_GOTOTABELA := 'ovgispgercab';
  VAR_GOTOMYSQLTABLE := QrOVgIspGerCab;
  VAR_GOTONEG := gotoPiZ;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT igc.*, ymc.Nome NO_OVcYnsMed, ycc.Nome NO_OVcYnsChk,');
  VAR_SQLx.Add('dlo.Nome NO_Local, ref.Nome NO_Referencia, ');
  VAR_SQLx.Add('isc.Nome NO_ZtatusIsp, yac.Nome NO_OVcYnsARQ, ');
  VAR_SQLx.Add('IF(igc.DtHrAbert  <= "1899-12-30", "",  ');
  VAR_SQLx.Add('  DATE_FORMAT(igc.DtHrAbert, "%d/%m/%Y %H:%i:%s")) DtHrAbert_TXT,  ');
  VAR_SQLx.Add('IF(igc.DtHrFecha  <= "1899-12-30", "",  ');
  VAR_SQLx.Add('  DATE_FORMAT(igc.DtHrFecha, "%d/%m/%Y %H:%i:%s")) DtHrFecha_TXT');
  VAR_SQLx.Add('FROM ovgispgercab igc');
  VAR_SQLx.Add('LEFT JOIN ovdlocal dlo ON dlo.Codigo=igc.Local  ');
  VAR_SQLx.Add('LEFT JOIN ovdreferencia ref ON ref.Codigo=igc.SeqGrupo ');
  VAR_SQLx.Add('LEFT JOIN ovcynsmedcad ymc ON ymc.Codigo=igc.OVcYnsMed');
  VAR_SQLx.Add('LEFT JOIN ovcynschkcad ycc ON ycc.Codigo=igc.OVcYnsChk');
  VAR_SQLx.Add('LEFT JOIN OVgIspStaCad isc ON isc.Codigo=igc.ZtatusIsp');
  VAR_SQLx.Add('LEFT JOIN OVcYnsARQCad yac ON yac.Codigo=igc.OVcYnsARQ');
  VAR_SQLx.Add('WHERE igc.Codigo > 0');
  //
  VAR_SQL1.Add('AND igc.Codigo=:P0');
  //
  //VAR_SQL2.Add('AND CodUsu=:P0');
  //
  VAR_SQLa.Add('AND igc.Nome Like :P0');
  //
end;

procedure TFmOVgIspGerCab.CabExclui1Click(Sender: TObject);
begin
  Geral.MB_Info('A��o n�o implementada! Solicite � Dermatek:' + sLineBreak +
  Caption + sLineBreak + TMenuItem(Sender).Name);
end;

procedure TFmOVgIspGerCab.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmOVgIspGerCab.QueryPrincipalAfterOpen;
begin
end;

procedure TFmOVgIspGerCab.ReopenOVcYnsChkCtx(Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrOVcYnsChkCtx, Dmod.MyDB, [
  'SELECT yqt.Nome NO_CONTEXTO, ycc.*  ',
  'FROM ovcynschkctx ycc ',
  'LEFT JOIN ovcynsqstctx yqt ON yqt.Codigo=ycc.Contexto ',
  'WHERE ycc.Codigo=' + Geral.FF0(QrOVgIspGerCabOVcYnsChk.Value),
  '']);
  //
  QrOVcYnsChkCtx.Locate('Controle', Controle, []);
end;

procedure TFmOVgIspGerCab.ReopenOVcYnsChkTop(Conta: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrOVcYnsChkTop, Dmod.MyDB, [
  'SELECT yqc.Nome NO_Topico, yqm.Nome NO_Magnitude, yct.* ',
  'FROM ovcynschktop yct ',
  'LEFT JOIN ovcynsqsttop yqc ON yqc.Codigo=yct.Topico ',
  'LEFT JOIN ovcynsqstmag yqm ON yqm.Codigo=yct.Magnitude ',
  'WHERE yct.Controle=' + Geral.FF0(QrOVcYnsChkCtxControle.Value),
  '']);
  //
  QrOVcYnsChkTop.Locate('Conta', Conta, []);
end;

procedure TFmOVgIspGerCab.ReopenOVcYnsMedDim(Conta: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrOVcYnsMedDim, Dmod.MyDB, [
  'SELECT ymd.* ',
  'FROM ovcynsmeddim ymd ',
  'WHERE ymd.Controle=' + Geral.FF0(QrOVcYnsMedTopControle.Value),
  '']);
  //
  QrOVcYnsMedDim.Locate('Conta', Conta, []);
end;

procedure TFmOVgIspGerCab.ReopenOVcYnsMedTop(Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrOVcYnsMedTop, Dmod.MyDB, [
  'SELECT ELT(ymt.TolerBasCalc+1, "Medida", "Percentual") NO_TolerBasCalc, ',
  'ELT(ymt.TolerUnMdida+1, "cm", "inch") NO_TolerUnMdida,  ',
  'ygt.Nome NO_TOBIKO, ymt.* ',
  'FROM ovcynsmedtop ymt ',
  'LEFT JOIN ovcynsgratop ygt ON ygt.Codigo=ymt.Tobiko ',
  'WHERE ymt.Codigo=' + Geral.FF0(QrOVgIspGerCabOVcYnsMed.Value),
  '']);
  //
  QrOVcYnsMedTop.Locate('Controle', Controle, []);
end;

procedure TFmOVgIspGerCab.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmOVgIspGerCab.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmOVgIspGerCab.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmOVgIspGerCab.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmOVgIspGerCab.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmOVgIspGerCab.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmOVgIspGerCab.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrOVgIspGerCabCodigo.Value;
  Close;
end;

procedure TFmOVgIspGerCab.CabAltera1Click(Sender: TObject);
begin
  OVS_Jan.MostraFormOVgIspGerCad(stUpd, QrOVgIspGerCabCodigo.Value,
    QrOVgIspGerCabLocal.Value, QrOVgIspGerCabNO_Local.Value,
    QrOVgIspGerCabNrOP.Value, QrOVgIspGerCabSeqGrupo.Value,
    QrOVgIspGerCabNO_Referencia.Value, QrOVgIspGerCabNrReduzidoOP.Value,
    QrOVgIspGerCabOVcYnsMed.Value, QrOVgIspGerCabOVcYnsChk.Value,
    QrOVgIspGerCabOVcYnsARQ.Value,
    QrOVgIspGerCabLimiteMed.Value, QrOVgIspGerCabLimiteChk.Value,
    QrOVgIspGerCabPermiFinHow.Value,
    QrOVgIspGerCabZtatusIsp.Value);
  //
  LocCod(QrOVgIspGerCabCodigo.Value, QrOVgIspGerCabCodigo.Value);
end;

procedure TFmOVgIspGerCab.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := EdCodigo.ValueVariant;
  if ImgTipo.SQLType = stIns then
    UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'ovgispgercab', Codigo);
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'ovgispgercab', 'Codigo');
end;

procedure TFmOVgIspGerCab.BtItsClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMIts, BtIts);
end;

function  TFmOVgIspGerCab.AlteraStatus(NovoStatus, Motivo: Integer): Boolean;
var
  //Nome, DtHrAbert, DtHrFecha,
  ZtatusDtH: String;
  //OVcYnsMed, OVcYnsChk, LimiteChk, LimiteMed,
  Codigo, Local, NrOP, SeqGrupo, NrReduzidoOP, ZtatusIsp, ZtatusMot: Integer;
  SQLType: TSQLType;
  //
  Agora: TDateTime;
begin
  Result         := False;
  //
  Agora          := DModG.ObtemAgora();
  SQLType        := stUpd;
  Codigo         := QrOVgIspGerCabCodigo.Value;
  Local          := QrOVgIspGerCabLocal.Value;
  NrOP           := QrOVgIspGerCabNrOP.Value;
  SeqGrupo       := QrOVgIspGerCabSeqGrupo.Value;
  NrReduzidoOP   := QrOVgIspGerCabNrReduzidoOP.Value;
  //DtHrAbert      := ;
  //DtHrFecha      := ;
  //OVcYnsMed      := ;
  //OVcYnsChk      := ;
  //LimiteChk      := ;
  //LimiteMed      := ;
  ZtatusIsp      := NovoStatus;
  ZtatusDtH      := Geral.FDT(Agora, 109);
  ZtatusMot      := Motivo;
  //Nome           := ;
  //
  //Codigo := UMyMod.BPGS1I32('ovgispgercab', 'Codigo', '', '', tsPosNeg?, stInsUpd?, CodAtual?);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'ovgispgercab', False, [
  'Local', 'NrOP', 'SeqGrupo',
  'NrReduzidoOP', (*'DtHrAbert', 'DtHrFecha',
  'OVcYnsMed', 'OVcYnsChk', 'LimiteChk',
  'LimiteMed',*) 'ZtatusIsp', 'ZtatusDtH',
  'ZtatusMot'(*, 'Nome'*)], [
  'Codigo'], [
  Local, NrOP, SeqGrupo,
  NrReduzidoOP, (*DtHrAbert, DtHrFecha,
  OVcYnsMed, OVcYnsChk, LimiteChk,
  LimiteMed,*) ZtatusIsp, ZtatusDtH,
  ZtatusMot(*, Nome*)], [
  Codigo], True) then
  begin
    if OVS_PF.AlteraZtatusOVgIspAllSta(NovoStatus, ZtatusMot, Local, NrOP,
    SeqGrupo, NrReduzidoOP, Agora) then
      Result := True;
  end;
end;

procedure TFmOVgIspGerCab.BtCabClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMCab, BtCab);
end;

procedure TFmOVgIspGerCab.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  GBEdita.Align := alClient;
  PCItens.Align := alClient;
  PCItens.ActivePageIndex := 0;
  CriaOForm;
  FSeq := 0;
end;

procedure TFmOVgIspGerCab.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrOVgIspGerCabCodigo.Value, LaRegistro.Caption);
end;

procedure TFmOVgIspGerCab.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmOVgIspGerCab.SbNovoClick(Sender: TObject);
const
  MultiSelect = False;
  Destroi     = True;
begin
//  LaRegistro.Caption := GOTOy.CodUsu(QrOVgIspGerCabCodigo.Value, LaRegistro.Caption);
(*
  'SELECT igc.ZtatusIsp, ist.Nome NO_ZTATUS,  ',
  'COUNT(igc.Codigo) ITENS ',
  'FROM ovgispgercab igc ',
  'LEFT JOIN ovgispstacad ist ON ist.Codigo=igc.ZtatusIsp ',
  'WHERE igc.Codigo <> 0 ',
  'GROUP BY igc.ZtatusIsp ',
*)
  UnDmkDAC_PF.AbreQuery(QrSumZtatusIsp, Dmod.MyDB);
  DBCheck.EscolheCodigoUniGrid('Aviso', 'Itens por Status', 'Prompt',
  DsSumZtatusIsp, MultiSelect, Destroi);
end;

procedure TFmOVgIspGerCab.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmOVgIspGerCab.QrOVcYnsChkCtxAfterScroll(DataSet: TDataSet);
begin
  ReopenOVcYnsChkTop(0);
end;

procedure TFmOVgIspGerCab.QrOVcYnsChkCtxBeforeClose(DataSet: TDataSet);
begin
  QrOVcYnsChkTop.Close;
end;

procedure TFmOVgIspGerCab.QrOVcYnsMedTopAfterScroll(DataSet: TDataSet);
begin
  ReopenOVcYnsMedDim(0);
end;

procedure TFmOVgIspGerCab.QrOVcYnsMedTopBeforeClose(DataSet: TDataSet);
begin
  QrOVcYnsMedDim.Close;
end;

procedure TFmOVgIspGerCab.QrOVgIspGerCabAfterOpen(DataSet: TDataSet);
begin
  //Geral.MB_Info(QrOVgIspGerCab.SQL.Text);
  QueryPrincipalAfterOpen;
end;

procedure TFmOVgIspGerCab.QrOVgIspGerCabAfterScroll(DataSet: TDataSet);
begin
  ReopenOVcYnsMedTop(0);
  ReopenOVcYnsChkCtx(0);
end;

procedure TFmOVgIspGerCab.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //
  if FSeq = 1 then
    CabInclui1Click(Self)
  else
  if (not FLocIni) and (FCabIni <> 0)  then
  begin
    LocCod(FCabIni, FCabIni);
    if QrOVgIspGerCabCodigo.Value <> FCabIni then
      Geral.MB_Aviso('Grupo n�o localizado!');
    FLocIni := True;
  end;
end;

procedure TFmOVgIspGerCab.SbQueryClick(Sender: TObject);
var
  Codigo: Integer;
begin
  if OVS_Jan.LocalizaOVgIspGerCab(Codigo) then
    LocCod(Codigo, Codigo);
end;

procedure TFmOVgIspGerCab.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmOVgIspGerCab.CabInclui1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrOVgIspGerCab, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'ovgispgercab');
end;

procedure TFmOVgIspGerCab.QrOVgIspGerCabBeforeClose(
  DataSet: TDataSet);
begin
  QrOVcYnsMedTop.Close;
end;

procedure TFmOVgIspGerCab.QrOVgIspGerCabBeforeOpen(DataSet: TDataSet);
begin
  QrOVgIspGerCabCodigo.DisplayFormat := FFormatFloat;
end;

end.

