object FmOVcYnsMixCad: TFmOVcYnsMixCad
  Left = 368
  Top = 194
  Caption = 'OVS-TAMAN-002 :: Cadastros de Tabelas de Medidas'
  ClientHeight = 691
  ClientWidth = 1008
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnEdita: TPanel
    Left = 0
    Top = 105
    Width = 1008
    Height = 586
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    Visible = False
    object GBEdita: TGroupBox
      Left = 0
      Top = 0
      Width = 1008
      Height = 185
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 0
      object Label7: TLabel
        Left = 16
        Top = 16
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label9: TLabel
        Left = 76
        Top = 16
        Width = 269
        Height = 13
        Caption = 'Descri'#231#227'o ([F4] copia nome do artigo referrencial abaixo):'
        Color = clBtnFace
        ParentColor = False
      end
      object Label3: TLabel
        Left = 16
        Top = 56
        Width = 82
        Height = 13
        Caption = 'Artigo referencial:'
      end
      object Label4: TLabel
        Left = 16
        Top = 96
        Width = 87
        Height = 13
        Caption = 'Cliente referencial:'
      end
      object SbArtigRef: TSpeedButton
        Left = 736
        Top = 71
        Width = 23
        Height = 23
        Caption = '...'
        OnClick = SbArtigRefClick
      end
      object SbClientRef: TSpeedButton
        Left = 736
        Top = 111
        Width = 23
        Height = 23
        Caption = '...'
        OnClick = SbClientRefClick
      end
      object Label8: TLabel
        Left = 680
        Top = 16
        Width = 75
        Height = 13
        Caption = 'Pontos (cr'#237'tico):'
        Color = clBtnFace
        ParentColor = False
      end
      object EdCodigo: TdmkEdit
        Left = 16
        Top = 32
        Width = 56
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Codigo'
        UpdCampo = 'Codigo'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnKeyDown = EdCodigoKeyDown
      end
      object EdNome: TdmkEdit
        Left = 76
        Top = 32
        Width = 601
        Height = 21
        MaxLength = 60
        TabOrder = 1
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Nome'
        UpdCampo = 'Nome'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
        OnKeyDown = EdNomeKeyDown
      end
      object EdArtigRef: TdmkEditCB
        Left = 16
        Top = 72
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 3
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'ArtigRef'
        UpdCampo = 'ArtigRef'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBArtigRef
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBArtigRef: TdmkDBLookupComboBox
        Left = 74
        Top = 72
        Width = 659
        Height = 21
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsOVdReferencia
        TabOrder = 4
        dmkEditCB = EdArtigRef
        QryCampo = 'ArtigRef'
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdClientRef: TdmkEditCB
        Left = 16
        Top = 112
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 5
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'ClientRef'
        UpdCampo = 'ClientRef'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBClientRef
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBClientRef: TdmkDBLookupComboBox
        Left = 74
        Top = 112
        Width = 659
        Height = 21
        KeyField = 'Codigo'
        ListField = 'NO_ENT'
        ListSource = DsEntidades
        TabOrder = 6
        dmkEditCB = EdClientRef
        QryCampo = 'ClientRef'
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdPontosCrit: TdmkEdit
        Left = 680
        Top = 32
        Width = 80
        Height = 21
        Alignment = taRightJustify
        TabOrder = 2
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'PontosCrit'
        UpdCampo = 'PontosCrit'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
    end
    object GBConfirma: TGroupBox
      Left = 0
      Top = 523
      Width = 1008
      Height = 63
      Align = alBottom
      TabOrder = 1
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 12
        Top = 17
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object Panel1: TPanel
        Left = 868
        Top = 15
        Width = 138
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 7
          Top = 2
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
    end
  end
  object PnDados: TPanel
    Left = 0
    Top = 105
    Width = 1008
    Height = 586
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    object GBDados: TGroupBox
      Left = 0
      Top = 0
      Width = 1008
      Height = 141
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 1
      object Label1: TLabel
        Left = 16
        Top = 16
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
        FocusControl = DBEdCodigo
      end
      object Label2: TLabel
        Left = 76
        Top = 16
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
        FocusControl = DBEdNome
      end
      object Label5: TLabel
        Left = 16
        Top = 56
        Width = 82
        Height = 13
        Caption = 'Artigo referencial:'
      end
      object Label6: TLabel
        Left = 16
        Top = 96
        Width = 87
        Height = 13
        Caption = 'Cliente referencial:'
      end
      object Label10: TLabel
        Left = 680
        Top = 16
        Width = 75
        Height = 13
        Caption = 'Pontos (cr'#237'tico):'
        Color = clBtnFace
        ParentColor = False
      end
      object DBEdCodigo: TdmkDBEdit
        Left = 16
        Top = 32
        Width = 56
        Height = 21
        TabStop = False
        DataField = 'Codigo'
        DataSource = DsOVcYnsMedCad
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 0
        UpdType = utYes
        Alignment = taRightJustify
      end
      object DBEdNome: TdmkDBEdit
        Left = 76
        Top = 32
        Width = 601
        Height = 21
        Color = clWhite
        DataField = 'Nome'
        DataSource = DsOVcYnsMedCad
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        UpdType = utYes
        Alignment = taLeftJustify
      end
      object DBEdit1: TDBEdit
        Left = 16
        Top = 72
        Width = 56
        Height = 21
        DataField = 'ArtigRef'
        DataSource = DsOVcYnsMedCad
        TabOrder = 2
      end
      object DBEdit2: TDBEdit
        Left = 76
        Top = 72
        Width = 689
        Height = 21
        DataField = 'NO_ArtigRef'
        DataSource = DsOVcYnsMedCad
        TabOrder = 3
      end
      object DBEdit3: TDBEdit
        Left = 16
        Top = 112
        Width = 56
        Height = 21
        DataField = 'ClientRef'
        DataSource = DsOVcYnsMedCad
        TabOrder = 4
      end
      object DBEdit4: TDBEdit
        Left = 76
        Top = 112
        Width = 689
        Height = 21
        DataField = 'NO_ClientRef'
        DataSource = DsOVcYnsMedCad
        TabOrder = 5
      end
      object DBEdit5: TDBEdit
        Left = 680
        Top = 32
        Width = 85
        Height = 21
        DataField = 'PontosCrit'
        DataSource = DsOVcYnsMedCad
        TabOrder = 6
      end
    end
    object GBCntrl: TGroupBox
      Left = 0
      Top = 522
      Width = 1008
      Height = 64
      Align = alBottom
      TabOrder = 0
      object Panel5: TPanel
        Left = 2
        Top = 15
        Width = 172
        Height = 47
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 174
        Top = 15
        Width = 154
        Height = 47
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel3: TPanel
        Left = 328
        Top = 15
        Width = 678
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object Panel2: TPanel
          Left = 545
          Top = 0
          Width = 133
          Height = 47
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 0
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
        object BtTab: TBitBtn
          Tag = 1000384
          Left = 4
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Tabela'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtTabClick
        end
        object BtTop: TBitBtn
          Tag = 1000494
          Left = 128
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = 'T'#243'&pico'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = BtTopClick
        end
        object BtGra: TBitBtn
          Tag = 1000058
          Left = 252
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Grade'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = BtGraClick
        end
        object BtFil: TBitBtn
          Tag = 1000300
          Left = 376
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Medidas'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 4
          OnClick = BtFilClick
        end
      end
    end
    object PnGrids: TPanel
      Left = 0
      Top = 141
      Width = 1008
      Height = 112
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 2
      object DBGTopico: TDBGrid
        Left = 625
        Top = 0
        Width = 383
        Height = 112
        Align = alClient
        DataSource = DsOVcYnsMedDim
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'Conta'
            Title.Caption = 'ID'
            Width = 56
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CodGrade'
            Title.Caption = 'C'#243'd. grade'
            Width = 48
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CodTam'
            Title.Caption = 'C'#243'd. tam'
            Width = 68
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'MedidCer'
            Title.Caption = 'Med. certa'
            Width = 55
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'MedidMin'
            Title.Caption = 'Med. m'#237'nima'
            Width = 55
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'MedidMax'
            Title.Caption = 'Med. m'#225'xima'
            Width = 55
            Visible = True
          end>
      end
      object DBGContexto: TDBGrid
        Left = 0
        Top = 0
        Width = 625
        Height = 112
        Align = alLeft
        DataSource = DsOVcYnsMedTop
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'Controle'
            Title.Caption = 'ID'
            Width = 56
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Tobiko'
            Width = 56
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NO_TOBIKO'
            Title.Caption = 'Descri'#231#227'o do Contexto'
            Width = 261
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NO_TolerBasCalc'
            Title.Caption = 'Bas.Calc.Toler.'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NO_TolerUnMdida'
            Title.Caption = 'Un.Med.Tol.'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'TolerRndPerc'
            Title.Caption = 'Marg. med/ %:'
            Width = 71
            Visible = True
          end>
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 12
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 216
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 216
      Top = 0
      Width = 744
      Height = 52
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 413
        Height = 32
        Caption = 'Cadastros de Tabelas de Medidas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 413
        Height = 32
        Caption = 'Cadastros de Tabelas de Medidas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 413
        Height = 32
        Caption = 'Cadastros de Tabelas de Medidas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 52
    Width = 1008
    Height = 53
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 36
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object PB1: TProgressBar
        Left = 0
        Top = 19
        Width = 1004
        Height = 17
        Align = alBottom
        TabOrder = 0
      end
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = BtTab
    Left = 120
    Top = 64
  end
  object QrOVcYnsMedCad: TMySQLQuery
    Database = Dmod.ZZDB
    BeforeOpen = QrOVcYnsMedCadBeforeOpen
    AfterOpen = QrOVcYnsMedCadAfterOpen
    BeforeClose = QrOVcYnsMedCadBeforeClose
    AfterScroll = QrOVcYnsMedCadAfterScroll
    SQL.Strings = (
      'SELECT IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_ClientRef, '
      'ycc.Nome NO_ArtigRef, ycc.* '
      'FROM ovcynschkcad ycc'
      'LEFT JOIN ovdreferencia art ON art.Codigo=ycc.ArtigRef '
      'LEFT JOIN entidades ent ON ent.Codigo=ycc.ClientRef '
      '')
    Left = 92
    Top = 233
    object QrOVcYnsMedCadNO_ClientRef: TWideStringField
      FieldName = 'NO_ClientRef'
      Size = 100
    end
    object QrOVcYnsMedCadNO_ArtigRef: TWideStringField
      FieldName = 'NO_ArtigRef'
      Size = 60
    end
    object QrOVcYnsMedCadCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrOVcYnsMedCadNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
    object QrOVcYnsMedCadArtigRef: TIntegerField
      FieldName = 'ArtigRef'
      Required = True
    end
    object QrOVcYnsMedCadClientRef: TIntegerField
      FieldName = 'ClientRef'
      Required = True
    end
    object QrOVcYnsMedCadPontosCrit: TIntegerField
      FieldName = 'PontosCrit'
    end
  end
  object DsOVcYnsMedCad: TDataSource
    DataSet = QrOVcYnsMedCad
    Left = 92
    Top = 281
  end
  object QrOVcYnsMedTop: TMySQLQuery
    Database = Dmod.ZZDB
    BeforeClose = QrOVcYnsMedTopBeforeClose
    AfterScroll = QrOVcYnsMedTopAfterScroll
    SQL.Strings = (
      
        'SELECT ELT(ymt.TolerBasCalc+1, "Medida", "Percentual") NO_TolerB' +
        'asCalc,'
      'ELT(ymt.TolerUnMdida+1, "cm", "inch") NO_TolerUnMdida, '
      'ygt.Nome NO_TOBIKO, ymt.*'
      'FROM ovcynsmedtop ymt'
      'LEFT JOIN ovcynsgratop ygt ON ygt.Codigo=ymt.Tobiko'
      'WHERE ygt.Codigo=1')
    Left = 188
    Top = 233
    object QrOVcYnsMedTopNO_TolerBasCalc: TWideStringField
      FieldName = 'NO_TolerBasCalc'
      Size = 10
    end
    object QrOVcYnsMedTopNO_TolerUnMdida: TWideStringField
      FieldName = 'NO_TolerUnMdida'
      Size = 4
    end
    object QrOVcYnsMedTopNO_TOBIKO: TWideStringField
      FieldName = 'NO_TOBIKO'
      Size = 60
    end
    object QrOVcYnsMedTopCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrOVcYnsMedTopControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrOVcYnsMedTopTobiko: TIntegerField
      FieldName = 'Tobiko'
      Required = True
    end
    object QrOVcYnsMedTopTolerBasCalc: TSmallintField
      FieldName = 'TolerBasCalc'
      Required = True
    end
    object QrOVcYnsMedTopTolerUnMdida: TSmallintField
      FieldName = 'TolerUnMdida'
      Required = True
    end
    object QrOVcYnsMedTopTolerRndPerc: TFloatField
      FieldName = 'TolerRndPerc'
      Required = True
      DisplayFormat = '0.000'
    end
    object QrOVcYnsMedTopLk: TIntegerField
      FieldName = 'Lk'
      Required = True
    end
    object QrOVcYnsMedTopDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrOVcYnsMedTopDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrOVcYnsMedTopUserCad: TIntegerField
      FieldName = 'UserCad'
      Required = True
    end
    object QrOVcYnsMedTopUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Required = True
    end
    object QrOVcYnsMedTopAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Required = True
    end
    object QrOVcYnsMedTopAWServerID: TIntegerField
      FieldName = 'AWServerID'
      Required = True
    end
    object QrOVcYnsMedTopAWStatSinc: TSmallintField
      FieldName = 'AWStatSinc'
      Required = True
    end
    object QrOVcYnsMedTopAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
  end
  object DsOVcYnsMedTop: TDataSource
    DataSet = QrOVcYnsMedTop
    Left = 188
    Top = 281
  end
  object PMTop: TPopupMenu
    OnPopup = PMTopPopup
    Left = 500
    Top = 404
    object ItsInclui1: TMenuItem
      Caption = '&Adiciona'
      Enabled = False
      OnClick = ItsInclui1Click
    end
    object ItsAltera1: TMenuItem
      Caption = '&Edita'
      Enabled = False
      OnClick = ItsAltera1Click
    end
    object ItsExclui1: TMenuItem
      Caption = '&Remove'
      Enabled = False
      OnClick = ItsExclui1Click
    end
  end
  object PMTab: TPopupMenu
    OnPopup = PMTabPopup
    Left = 300
    Top = 372
    object CabInclui1: TMenuItem
      Caption = '&Inclui'
      OnClick = CabInclui1Click
    end
    object CabAltera1: TMenuItem
      Caption = '&Altera'
      Enabled = False
      OnClick = CabAltera1Click
    end
    object CabExclui1: TMenuItem
      Caption = '&Exclui'
      Enabled = False
      OnClick = CabExclui1Click
    end
  end
  object QrEntidades: TMySQLQuery
    Database = Dmod.ZZDB
    SQL.Strings = (
      'SELECT Codigo, IF(Tipo=0, RazaoSocial, Nome) NO_ENT'
      'FROM entidades'
      'ORDER BY NO_ENT')
    Left = 932
    Top = 121
    object QrEntidadesCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrEntidadesNO_ENT: TWideStringField
      FieldName = 'NO_ENT'
      Size = 100
    end
  end
  object DsEntidades: TDataSource
    DataSet = QrEntidades
    Left = 932
    Top = 169
  end
  object QrOVdReferencia: TMySQLQuery
    Database = Dmod.ZZDB
    SQL.Strings = (
      'SELECT Codigo, Nome '
      'FROM ovdreferencia'
      'ORDER BY Nome')
    Left = 848
    Top = 129
    object QrOVdReferenciaCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrOVdReferenciaNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
  end
  object DsOVdReferencia: TDataSource
    DataSet = QrOVdReferencia
    Left = 848
    Top = 177
  end
  object PMGra: TPopupMenu
    OnPopup = PMGraPopup
    Left = 612
    Top = 380
    object Adicionatamanhos1: TMenuItem
      Caption = '&Adiciona tamanho(s)'
      Enabled = False
      OnClick = Adicionatamanhos1Click
    end
    object Removetamanho1: TMenuItem
      Caption = '&Remove tamanho'
      Enabled = False
      OnClick = Removetamanho1Click
    end
  end
  object QrOVcYnsMedDim: TMySQLQuery
    Database = Dmod.ZZDB
    SQL.Strings = (
      'SELECT ymd.*  '
      'FROM ovcynsmeddim ymd  '
      'WHERE ymd.Controle=1 ')
    Left = 292
    Top = 237
    object QrOVcYnsMedDimCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrOVcYnsMedDimControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrOVcYnsMedDimConta: TIntegerField
      FieldName = 'Conta'
      Required = True
    end
    object QrOVcYnsMedDimCodGrade: TIntegerField
      FieldName = 'CodGrade'
      Required = True
    end
    object QrOVcYnsMedDimCodTam: TWideStringField
      FieldName = 'CodTam'
      Size = 30
    end
    object QrOVcYnsMedDimMedidCer: TFloatField
      FieldName = 'MedidCer'
      Required = True
      DisplayFormat = '0.000'
    end
    object QrOVcYnsMedDimMedidMin: TFloatField
      FieldName = 'MedidMin'
      Required = True
      DisplayFormat = '0.000'
    end
    object QrOVcYnsMedDimMedidMax: TFloatField
      FieldName = 'MedidMax'
      Required = True
      DisplayFormat = '0.000'
    end
    object QrOVcYnsMedDimUlWayInz: TSmallintField
      FieldName = 'UlWayInz'
      Required = True
    end
    object QrOVcYnsMedDimLk: TIntegerField
      FieldName = 'Lk'
      Required = True
    end
    object QrOVcYnsMedDimDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrOVcYnsMedDimDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrOVcYnsMedDimUserCad: TIntegerField
      FieldName = 'UserCad'
      Required = True
    end
    object QrOVcYnsMedDimUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Required = True
    end
    object QrOVcYnsMedDimAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Required = True
    end
    object QrOVcYnsMedDimAWServerID: TIntegerField
      FieldName = 'AWServerID'
      Required = True
    end
    object QrOVcYnsMedDimAWStatSinc: TSmallintField
      FieldName = 'AWStatSinc'
      Required = True
    end
    object QrOVcYnsMedDimAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
  end
  object DsOVcYnsMedDim: TDataSource
    DataSet = QrOVcYnsMedDim
    Left = 292
    Top = 285
  end
  object QrGT: TMySQLQuery
    Database = Dmod.ZZDB
    Left = 468
    Top = 277
    object QrGTCodGrade: TIntegerField
      FieldName = 'CodGrade'
    end
    object QrGTCodTam: TWideStringField
      FieldName = 'CodTam'
      Size = 30
    end
  end
  object QrTop: TMySQLQuery
    Database = Dmod.ZZDB
    BeforeClose = QrOVcYnsMedTopBeforeClose
    AfterScroll = QrOVcYnsMedTopAfterScroll
    SQL.Strings = (
      'SELECT Codigo, Controle'
      'FROM ovcynsmedtop ymt'
      'WHERE ygt.Codigo=:P0')
    Left = 468
    Top = 325
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrTopCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrTopControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
  end
  object DqAux: TMySQLDirectQuery
    Database = Dmod.ZZDB
    Left = 564
    Top = 240
  end
  object PMFil: TPopupMenu
    Left = 744
    Top = 397
    object FormatoPanormico1: TMenuItem
      Caption = 'Formato &Grade'
      Enabled = False
      OnClick = FormatoPanormico1Click
    end
    object Itemaitem1: TMenuItem
      Caption = 'Formato &Lista'
      OnClick = Itemaitem1Click
    end
  end
end
