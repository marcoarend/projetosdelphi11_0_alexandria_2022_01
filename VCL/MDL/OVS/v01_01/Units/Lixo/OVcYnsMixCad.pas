unit OVcYnsMixCad;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2, dmkGeral,
  ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt, UMySQLModule, Menus,
  UnInternalConsts2, mySQLDbTables, UnMySQLCuringa, dmkPermissoes, dmkEdit,
  dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup, Grids, DBGrids,
  UnDmkProcFunc, UnDmkENums, dmkDBLookupComboBox, dmkEditCB, Variants,
  mySQLDirectQuery, Vcl.ComCtrls;

type
  TFmOVcYnsMixCad = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    Label9: TLabel;
    EdNome: TdmkEdit;
    Label1: TLabel;
    DBEdCodigo: TdmkDBEdit;
    Label2: TLabel;
    DBEdNome: TdmkDBEdit;
    QrOVcYnsMedCad: TMySQLQuery;
    DsOVcYnsMedCad: TDataSource;
    QrOVcYnsMedTop: TMySQLQuery;
    DsOVcYnsMedTop: TDataSource;
    PMTop: TPopupMenu;
    ItsInclui1: TMenuItem;
    ItsExclui1: TMenuItem;
    ItsAltera1: TMenuItem;
    PMTab: TPopupMenu;
    CabInclui1: TMenuItem;
    CabAltera1: TMenuItem;
    CabExclui1: TMenuItem;
    BtTab: TBitBtn;
    BtTop: TBitBtn;
    QrEntidades: TMySQLQuery;
    DsEntidades: TDataSource;
    QrOVdReferencia: TMySQLQuery;
    DsOVdReferencia: TDataSource;
    QrEntidadesCodigo: TIntegerField;
    QrOVdReferenciaCodigo: TIntegerField;
    QrOVdReferenciaNome: TWideStringField;
    Label3: TLabel;
    EdArtigRef: TdmkEditCB;
    CBArtigRef: TdmkDBLookupComboBox;
    Label4: TLabel;
    EdClientRef: TdmkEditCB;
    CBClientRef: TdmkDBLookupComboBox;
    SbArtigRef: TSpeedButton;
    SbClientRef: TSpeedButton;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    DBEdit4: TDBEdit;
    QrEntidadesNO_ENT: TWideStringField;
    QrOVcYnsMedCadNO_ClientRef: TWideStringField;
    QrOVcYnsMedCadNO_ArtigRef: TWideStringField;
    QrOVcYnsMedCadCodigo: TIntegerField;
    QrOVcYnsMedCadNome: TWideStringField;
    QrOVcYnsMedCadArtigRef: TIntegerField;
    QrOVcYnsMedCadClientRef: TIntegerField;
    Label5: TLabel;
    Label6: TLabel;
    BtGra: TBitBtn;
    PMGra: TPopupMenu;
    QrOVcYnsMedDim: TMySQLQuery;
    DsOVcYnsMedDim: TDataSource;
    PnGrids: TPanel;
    DBGTopico: TDBGrid;
    DBGContexto: TDBGrid;
    QrOVcYnsMedTopNO_TolerBasCalc: TWideStringField;
    QrOVcYnsMedTopNO_TolerUnMdida: TWideStringField;
    QrOVcYnsMedTopNO_TOBIKO: TWideStringField;
    QrOVcYnsMedTopCodigo: TIntegerField;
    QrOVcYnsMedTopControle: TIntegerField;
    QrOVcYnsMedTopTobiko: TIntegerField;
    QrOVcYnsMedTopTolerBasCalc: TSmallintField;
    QrOVcYnsMedTopTolerUnMdida: TSmallintField;
    QrOVcYnsMedTopTolerRndPerc: TFloatField;
    QrOVcYnsMedTopLk: TIntegerField;
    QrOVcYnsMedTopDataCad: TDateField;
    QrOVcYnsMedTopDataAlt: TDateField;
    QrOVcYnsMedTopUserCad: TIntegerField;
    QrOVcYnsMedTopUserAlt: TIntegerField;
    QrOVcYnsMedTopAlterWeb: TSmallintField;
    QrOVcYnsMedTopAWServerID: TIntegerField;
    QrOVcYnsMedTopAWStatSinc: TSmallintField;
    QrOVcYnsMedTopAtivo: TSmallintField;
    QrOVcYnsMedDimCodigo: TIntegerField;
    QrOVcYnsMedDimControle: TIntegerField;
    QrOVcYnsMedDimConta: TIntegerField;
    QrOVcYnsMedDimCodGrade: TIntegerField;
    QrOVcYnsMedDimCodTam: TWideStringField;
    QrOVcYnsMedDimMedidCer: TFloatField;
    QrOVcYnsMedDimMedidMin: TFloatField;
    QrOVcYnsMedDimMedidMax: TFloatField;
    QrOVcYnsMedDimUlWayInz: TSmallintField;
    QrOVcYnsMedDimLk: TIntegerField;
    QrOVcYnsMedDimDataCad: TDateField;
    QrOVcYnsMedDimDataAlt: TDateField;
    QrOVcYnsMedDimUserCad: TIntegerField;
    QrOVcYnsMedDimUserAlt: TIntegerField;
    QrOVcYnsMedDimAlterWeb: TSmallintField;
    QrOVcYnsMedDimAWServerID: TIntegerField;
    QrOVcYnsMedDimAWStatSinc: TSmallintField;
    QrOVcYnsMedDimAtivo: TSmallintField;
    Adicionatamanhos1: TMenuItem;
    QrGT: TMySQLQuery;
    QrTop: TMySQLQuery;
    QrTopCodigo: TIntegerField;
    QrTopControle: TIntegerField;
    QrGTCodGrade: TIntegerField;
    QrGTCodTam: TWideStringField;
    DqAux: TMySQLDirectQuery;
    PB1: TProgressBar;
    Removetamanho1: TMenuItem;
    BtFil: TBitBtn;
    PMFil: TPopupMenu;
    FormatoPanormico1: TMenuItem;
    Itemaitem1: TMenuItem;
    EdPontosCrit: TdmkEdit;
    Label8: TLabel;
    QrOVcYnsMedCadPontosCrit: TIntegerField;
    DBEdit5: TDBEdit;
    Label10: TLabel;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrOVcYnsMedCadAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrOVcYnsMedCadBeforeOpen(DataSet: TDataSet);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure QrOVcYnsMedCadAfterScroll(DataSet: TDataSet);
    procedure CabInclui1Click(Sender: TObject);
    procedure CabAltera1Click(Sender: TObject);
    procedure CabExclui1Click(Sender: TObject);
    procedure BtTabClick(Sender: TObject);
    procedure BtTopClick(Sender: TObject);
    procedure ItsInclui1Click(Sender: TObject);
    procedure ItsExclui1Click(Sender: TObject);
    procedure ItsAltera1Click(Sender: TObject);
    procedure PMTabPopup(Sender: TObject);
    procedure PMTopPopup(Sender: TObject);
    procedure QrOVcYnsMedCadBeforeClose(DataSet: TDataSet);
    procedure SbClientRefClick(Sender: TObject);
    procedure SbArtigRefClick(Sender: TObject);
    procedure BtGraClick(Sender: TObject);
    procedure PMGraPopup(Sender: TObject);
    procedure QrOVcYnsMedTopBeforeClose(DataSet: TDataSet);
    procedure QrOVcYnsMedTopAfterScroll(DataSet: TDataSet);
    procedure Adicionatamanhos1Click(Sender: TObject);
    procedure Removetamanho1Click(Sender: TObject);
    procedure BtFilClick(Sender: TObject);
    procedure FormatoPanormico1Click(Sender: TObject);
    procedure Itemaitem1Click(Sender: TObject);
    procedure EdCodigoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdNomeKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
  private
    FRegCadSel, FRegistrosAdicionados: Integer;
    //
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure LocCod(Atual, Codigo: Integer);
    procedure Va(Para: TVaiPara);
    //
    procedure CopiaNomeArtigo();
    procedure MostraFormOVcYnsMedTop(SQLType: TSQLType);
    procedure MostraFormOVcYnsMedDimAdd(SQLType: TSQLType);

  public
    { Public declarations }
    FSeq, FCabIni, FArtigo: Integer;
    FLocIni: Boolean;
    //
    procedure ReopenOVcYnsMedTop(Controle: Integer);
    procedure ReopenOVcYnsMedDim(Conta: Integer);
    procedure ReopenEntidades();
    procedure ReopenOVdReferencia();
    procedure VerificaEAdicionaNovosItensGrade(
              var GradesToAdd: array of Integer;
              var TamsToAdd: array of String);

  end;

var
  FmOVcYnsMixCad: TFmOVcYnsMixCad;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, MyDBCheck, DmkDAC_PF, UnOVS_Jan, UMySQLDB,
  UnEntities,
  OVcYnsMedTop, OVcYnsMedDimAdd, OVcYnsMedDimFil, OVcYnsMedDimIts;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmOVcYnsMixCad.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmOVcYnsMixCad.MostraFormOVcYnsMedTop(SQLType: TSQLType);
begin
  if DBCheck.CriaFm(TFmOVcYnsMedTop, FmOVcYnsMedTop, afmoNegarComAviso) then
  begin
    FmOVcYnsMedTop.ImgTipo.SQLType := SQLType;
    FmOVcYnsMedTop.FDsCad := DsOVcYnsMedCad;
    FmOVcYnsMedTop.FDsIts := DsOVcYnsMedTop;
    FmOVcYnsMedTop.FQrIts := QrOVcYnsMedTop;
    //
    if SQLType = stIns then
    begin
      //FmOVcYnsMedTop.EdCPF1.ReadOnly := False
    end else
    begin
      //
      FmOVcYnsMedTop.EdControle.ValueVariant := QrOVcYnsMedTopControle.Value;
      //
      FmOVcYnsMedTop.EdTobiko.ValueVariant       := QrOVcYnsMedTopTobiko.Value;
      FmOVcYnsMedTop.CBTobiko.KeyValue           := QrOVcYnsMedTopTobiko.Value;

      //  Devem ser os �ltimos? Na ordem abaixo?
      FmOVcYnsMedTop.RGTolerBasCalc.ItemIndex    := QrOVcYnsMedTopTolerBasCalc.Value;
      FmOVcYnsMedTop.RGTolerUnMdida.ItemIndex    := QrOVcYnsMedTopTolerUnMdida.Value;
      FmOVcYnsMedTop.EdTolerRndPerc.ValueVariant := QrOVcYnsMedTopTolerRndPerc.Value;
    end;
    FmOVcYnsMedTop.ShowModal;
    FmOVcYnsMedTop.Destroy;
  end;
end;

procedure TFmOVcYnsMixCad.MostraFormOVcYnsMedDimAdd(SQLType: TSQLType);
begin
  if UmyMod.FormInsUpd_Cria(TFmOVcYnsMedDimAdd, FmOVcYnsMedDimAdd, afmoNegarComAviso,
    QrOVcYnsMedDim, SQLType) then
  begin
    FmOVcYnsMedDimAdd.FDsCad := DsOVcYnsMedCad;
    FmOVcYnsMedDimAdd.FDsCtx := DsOVcYnsMedTop;
    FmOVcYnsMedDimAdd.FQrTop := QrOVcYnsMedDim;
    FmOVcYnsMedDimAdd.FProduto := QrOVcYnsMedCadArtigRef.Value;
    //
    FmOVcYnsMedDimAdd.DBEdCodigo.DataSource := DsOVcYnsMedCad;
    FmOVcYnsMedDimAdd.DBEdNome.DataSource   := DsOVcYnsMedCad;
    //
    //FmOVcYnsMedDimAdd.DBEdControle.DataSource := DsOVcYnsMedTop;
    //FmOVcYnsMedDimAdd.DBEdCtrlNome.DataSource := DsOVcYnsMedTop;
    //
    FmOVcYnsMedDimAdd.ReopenGrade(FmOVcYnsMedDimAdd.FProduto);
    FmOVcYnsMedDimAdd.EdGrade.ValueVariant := FmOVcYnsMedDimAdd.QrGradeCodGrade.Value;
    FmOVcYnsMedDimAdd.CBGrade.KeyValue     := FmOVcYnsMedDimAdd.QrGradeCodGrade.Value;
    //
    FmOVcYnsMedDimAdd.ShowModal;
    FmOVcYnsMedDimAdd.Destroy;
    //
  end;
end;

procedure TFmOVcYnsMixCad.PMTabPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemCabUpd(CabAltera1, QrOVcYnsMedCad);
  MyObjects.HabilitaMenuItemCabDel(CabExclui1, QrOVcYnsMedCad, QrOVcYnsMedTop);
end;

procedure TFmOVcYnsMixCad.PMTopPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(ItsInclui1, QrOVcYnsMedCad);
  MyObjects.HabilitaMenuItemItsUpd(ItsAltera1, QrOVcYnsMedTop);
  MyObjects.HabilitaMenuItemItsUpd(ItsExclui1, QrOVcYnsMedTop); //
end;

procedure TFmOVcYnsMixCad.PMGraPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(AdicionaTamanhos1, QrOVcYnsMedTop);
  //MyObjects.HabilitaMenuItemItsUpd(AlteraTamanho1, QrOVcYnsMedDim);
  MyObjects.HabilitaMenuItemItsDel(RemoveTamanho1, QrOVcYnsMedDim);
end;

procedure TFmOVcYnsMixCad.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrOVcYnsMedCadCodigo.Value, LaRegistro.Caption[2]);
end;

procedure TFmOVcYnsMixCad.VerificaEAdicionaNovosItensGrade(
  var GradesToAdd: array of Integer;
  var TamsToAdd: array of String);
const
  sProcName = 'TFmOVcYnsMedCad.VerificaEAdicionaNovosItensGrade()';
var
  I, K, M, N, Codigo, Controle, Conta, CodGrade: Integer;
  SQLType: TSQLType;

  CodTam, sCodigo, sControle: String;
  //
  function ExecutaSQL(): Boolean;
  begin
    //
    UnDmkDAC_PF.AbreMySQLDirectQuery0(DqAux, Dmod.MyDB, [
    'SELECT Conta ',
    'FROM ovcynsmeddim ',
    'WHERE Codigo=' + sCodigo,
    'AND Controle=' + sControle,
    'AND CodGrade=' + Geral.FF0(CodGrade),
    'AND CodTam = "' + CodTam + '" ',
    '']);
    Conta := USQLDB.Int(DqAux, 'Conta');
    if Conta = 0 then
    begin
      Conta := UMyMod.BPGS1I32('ovcynsmeddim', 'Conta', '', '', tsPos, SQLType, Conta);
      //
      if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'ovcynsmeddim', False, [
      'Codigo', 'Controle', 'CodGrade',
      'CodTam'(*, 'MedidCer', 'MedidMin',
      'MedidMax', 'UlWayInz'*)], [
      'Conta'], [
      Codigo, Controle, CodGrade,
      CodTam(*, MedidCer, MedidMin,
      MedidMax, UlWayInz*)], [
      Conta], True) then
      begin
        M := M + 1;
        MyObjects.Informa2(LaAviso1, LaAviso2, False,
          'Adicionando registro ' + Geral.FF0(M) + '!');
      end;
    end;
    Result := True;
  end;
begin
  Screen.Cursor := crHourGlass;
  try
    SQLType := stIns;
    Codigo         := QrOVcYnsMedCadCodigo.Value;
    //Controle       := Todos!
    Conta          := 0;
    //CodGrade       := 0;  // Abaixo. Item a item
    //CodTam         := ''; // Abaixo. Item a item
    //MedidCer       := 0; //  Depois na janela OVcYnsMesDimFil !!!
    //MedidMin       := 0; //  Depois na janela OVcYnsMesDimFil !!!
    //MedidMax       := 0; //  Depois na janela OVcYnsMesDimFil !!!
    //UlWayInz       := iAutomatico;
    //
    M         := 0;
    sCodigo   := Geral.FF0(Codigo);
    //sControle := Geral.FF0(Controle);
    PB1.Position := 0;
    UnDmkDAC_PF.AbreMySQLQuery0(QrGT, Dmod.MyDB, [
    'SELECT DISTINCT CodGrade, CodTam ',
    'FROM ovcynsmeddim ',
    'WHERE Codigo=' + Geral.FF0(QrOVcYnsMedCadCodigo.Value),
    '']);
    //
    if (Length(GradesToAdd) <> Length(TamsToAdd)) then
    begin
      Geral.MB_Erro('ERRO! quantidade de itens em arrays divergem!' +
      sLineBreak + sProcName);
      Exit;
    end;
    N := QrGT.RecordCount + Length(GradesToAdd);
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrTop, Dmod.MyDB, [
    'SELECT Codigo, Controle ',
    'FROM ovcynsmedtop ',
    'WHERE Codigo=' + Geral.FF0(QrOVcYnsMedCadCodigo.Value),
    '']);
    K := QrTop.RecordCount * N;
    PB1.Max := K;
    //
    QrTop.First;
    while not QrTop.Eof do
    begin
      Controle  := QrTopControle.Value;
      sControle := Geral.FF0(Controle);
      //
      for I := 0 to Length(GradesToAdd) -1 do
      begin
        PB1.Position := PB1.Position + 1;
        //
        CodGrade := GradesToAdd[I];
        CodTam   := TamsToAdd[I];
        //
        ExecutaSQL();
      end;
      //
      QrGT.First;
      while not QrGT.Eof do
      begin
        PB1.Position := PB1.Position + 1;
        //
        CodGrade := QrGTCodGrade.Value;
        CodTam   := QrGTCodTam.Value;
        //
        ExecutaSQL();
        //
        QrGT.Next;
      end;
      //
      QrTop.Next;
    end;
    ReopenOVcYnsMedDim(0);
  finally
    Screen.Cursor := crDefault;
  end;
  FRegistrosAdicionados := FRegistrosAdicionados + M;
  MyObjects.Informa2(LaAviso1, LaAviso2, False,
    Geral.FF0(FRegistrosAdicionados) + ' registros foram adicionados!');
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmOVcYnsMixCad.DefParams;
begin
  VAR_GOTOTABELA := 'ovcynsmedcad';
  VAR_GOTOMYSQLTABLE := QrOVcYnsMedCad;
  VAR_GOTONEG := gotoPiZ;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_ClientRef, ');
  VAR_SQLx.Add('art.Nome NO_ArtigRef, ycc.* ');
  VAR_SQLx.Add('FROM ovcynsmedcad ycc');
  VAR_SQLx.Add('LEFT JOIN ovdreferencia art ON art.Codigo=ycc.ArtigRef ');
  VAR_SQLx.Add('LEFT JOIN entidades ent ON ent.Codigo=ycc.ClientRef ');
  //
  VAR_SQL1.Add('WHERE ycc.Codigo>0');
  VAR_SQL1.Add('AND ycc.Codigo=:P0');
  //
  //VAR_SQL2.Add('AND CodUsu=:P0');
  //
  VAR_SQLa.Add('AND ycc.Nome Like :P0');
  //
end;

procedure TFmOVcYnsMixCad.EdCodigoKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F4 then
    CopiaNomeArtigo();
end;

procedure TFmOVcYnsMixCad.EdNomeKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F4 then
    CopiaNomeArtigo();
end;

procedure TFmOVcYnsMixCad.Itemaitem1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmOVcYnsMedDimIts, FmOVcYnsMedDimIts, afmoNegarComAviso) then
  begin
    FmOVcYnsMedDimIts.FCodigo := QrOVcYnsMedCadCodigo.Value;
    FmOVcYnsMedDimIts.ReopenTam();
    FmOVcYnsMedDimIts.PreeencheGrade();
    FmOVcYnsMedDimIts.ShowModal;
    FmOVcYnsMedDimIts.Destroy;
    LocCod(QrOVcYnsMedCadCodigo.Value, QrOVcYnsMedCadCodigo.Value);
  end;
end;

procedure TFmOVcYnsMixCad.ItsAltera1Click(Sender: TObject);
begin
  MostraFormOVcYnsMedTop(stUpd);
end;

procedure TFmOVcYnsMixCad.CabExclui1Click(Sender: TObject);
begin
  Geral.MB_Info('A��o n�o implementada! Solicite � Dermatek:' + sLineBreak +
  Caption + sLineBreak + TMenuItem(Sender).Name);
end;

procedure TFmOVcYnsMixCad.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmOVcYnsMixCad.QueryPrincipalAfterOpen;
begin
end;

procedure TFmOVcYnsMixCad.ItsExclui1Click(Sender: TObject);
var
  Controle: Integer;
  Pergunta: String;
begin
  Pergunta := 'Deseja remover o T�pico "' + QrOVcYnsMedTopNO_TOBIKO.Value +
    '" e seus tamanhos?';
  //
  if Geral.MB_Pergunta(Pergunta) = ID_YES then
  begin
    UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
    'DELETE FROM ovcynsmeddim  ',
    'WHERE Controle=' + Geral.FF0(QrOVcYnsMedTopControle.Value),
    '']);
    //
    UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
    'DELETE FROM ovcynsmedtop  ',
    'WHERE Controle=' + Geral.FF0(QrOVcYnsMedTopControle.Value),
    '']);
    //
    Controle := GOTOy.LocalizaPriorNextIntQr(QrOVcYnsMedTop,
      QrOVcYnsMedTopControle, QrOVcYnsMedTopControle.Value);
    ReopenOVcYnsMedTop(Controle);
  end;
end;

procedure TFmOVcYnsMixCad.Removetamanho1Click(Sender: TObject);
var
  Conta: Integer;
  Pergunta: String;
begin
  Pergunta := 'Deseja remover o tamanho "' + QrOVcYnsMedDimCodTam.Value +
    '" de todos t�picos desta tabela?';
  //
  if Geral.MB_Pergunta(Pergunta) = ID_YES then
  begin
    UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
    'DELETE FROM ovcynsmeddim  ',
    'WHERE Codigo=' + Geral.FF0(QrOVcYnsMedCadCodigo.Value),
    'AND CodTam="' + QrOVcYnsMedDimCodTam.Value + '" ',
    '']);
    //
    Conta := GOTOy.LocalizaPriorNextIntQr(QrOVcYnsMedDim,
      QrOVcYnsMedDimConta, QrOVcYnsMedDimConta.Value);
    ReopenOVcYnsMedDim(Conta);
  end;
end;

procedure TFmOVcYnsMixCad.ReopenEntidades();
begin
  UnDMkDAC_PF.AbreQuery(QrEntidades, DMod.MyDB);
end;

procedure TFmOVcYnsMixCad.ReopenOVdReferencia;
begin
  UnDMkDAC_PF.AbreQuery(QrOVdReferencia, DMod.MyDB);
end;

procedure TFmOVcYnsMixCad.ReopenOVcYnsMedTop(Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrOVcYnsMedTop, Dmod.MyDB, [
  'SELECT ELT(ymt.TolerBasCalc+1, "Medida", "Percentual") NO_TolerBasCalc, ',
  'ELT(ymt.TolerUnMdida+1, "cm", "inch") NO_TolerUnMdida,  ',
  'ygt.Nome NO_TOBIKO, ymt.* ',
  'FROM ovcynsmedtop ymt ',
  'LEFT JOIN ovcynsgratop ygt ON ygt.Codigo=ymt.Tobiko ',
  'WHERE ymt.Codigo=' + Geral.FF0(QrOVcYnsMedCadCodigo.Value),
  '']);
  //
  QrOVcYnsMedTop.Locate('Controle', Controle, []);
end;

procedure TFmOVcYnsMixCad.ReopenOVcYnsMedDim(Conta: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrOVcYnsMedDim, Dmod.MyDB, [
  'SELECT ymd.* ',
  'FROM ovcynsmeddim ymd ',
  'WHERE ymd.Controle=' + Geral.FF0(QrOVcYnsMedTopControle.Value),
  '']);
  //
  QrOVcYnsMedDim.Locate('Conta', Conta, []);
end;

procedure TFmOVcYnsMixCad.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmOVcYnsMixCad.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmOVcYnsMixCad.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmOVcYnsMixCad.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmOVcYnsMixCad.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmOVcYnsMixCad.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmOVcYnsMixCad.BtGraClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMGra, BtGra);
end;

procedure TFmOVcYnsMixCad.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrOVcYnsMedCadCodigo.Value;
  Close;
end;

procedure TFmOVcYnsMixCad.ItsInclui1Click(Sender: TObject);
begin
  MostraFormOVcYnsMedTop(stIns);
end;

procedure TFmOVcYnsMixCad.CabAltera1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrOVcYnsMedCad, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'ovcynsmedcad');
end;

procedure TFmOVcYnsMixCad.BtConfirmaClick(Sender: TObject);
var
  Nome: String;
  Codigo, ArtigRef, ClientRef, PontosCrit: Integer;
  SQLType: TSQLType;
begin
  SQLType        := ImgTipo.SQLType;
  Codigo         := EdCodigo.ValueVariant;
  Nome           := EdNome.ValueVariant;
  ArtigRef       := EdArtigRef.ValueVariant;
  ClientRef      := EdClientRef.ValueVariant;
  PontosCrit     := EdPontosCrit.ValueVariant;
  //
  if MyObjects.FIC(Length(Nome) = 0, EdNome, 'Defina uma descri��o!') then Exit;
  if MyObjects.FIC(EdPontosCrit.ValueVariant = 0, EdPontosCrit,
    'ATEN��O: os pontos (cr�tico) n�o foram informados!') then (*Exit*);
  //
  Codigo := UMyMod.BPGS1I32('ovcynsmedcad', 'Codigo', '', '', tsPos, SQLType, Codigo);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'ovcynsmedcad', False, [
  'Nome', 'ArtigRef', 'ClientRef',
  'PontosCrit'], [
  'Codigo'], [
  Nome, ArtigRef, ClientRef,
  PontosCrit], [
  Codigo], True) then
  begin
    ImgTipo.SQLType := stLok;
    PnDados.Visible := True;
    PnEdita.Visible := False;
    GOTOy.BotoesSb(ImgTipo.SQLType);
    //
    LocCod(Codigo, Codigo);
    if FSeq = 1 then Close;
  end;
end;

procedure TFmOVcYnsMixCad.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := EdCodigo.ValueVariant;
  if ImgTipo.SQLType = stIns then
    UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'ovcynsmedcad', Codigo);
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'ovcynsmedcad', 'Codigo');
end;

procedure TFmOVcYnsMixCad.BtFilClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMFil, BtFil);
end;

procedure TFmOVcYnsMixCad.BtTopClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMTop, BtTop);
end;

procedure TFmOVcYnsMixCad.Adicionatamanhos1Click(Sender: TObject);
begin
  MostraFormOVcYnsMedDimAdd(stIns);
end;

procedure TFmOVcYnsMixCad.BtTabClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMTab, BtTab);
end;

procedure TFmOVcYnsMixCad.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  GBEdita.Align := alClient;
  PnGrids.Align := alClient;
  CriaOForm;
  FSeq := 0;
  FRegCadSel := 0;
  FRegistrosAdicionados := 0;
  QrOVcYnsMedCad.Database := Dmod.MyDB;
  ReopenOVdReferencia();
  ReopenEntidades();
end;

procedure TFmOVcYnsMixCad.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrOVcYnsMedCadCodigo.Value, LaRegistro.Caption);
end;

procedure TFmOVcYnsMixCad.SbArtigRefClick(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  OVS_Jan.MostraFormOVdReferencia();
  if VAR_CADASTRO <> 0 then
  begin
      UMyMod.SetaCodigoPesquisado(EdArtigRef, CBArtigRef, QrOVdReferencia, VAR_CADASTRO);
      EdArtigRef.SetFocus;
  end else
    ReopenOVdReferencia();
end;

procedure TFmOVcYnsMixCad.SbClientRefClick(Sender: TObject);
begin
  Entities.CadastroDeEntidade(0, fmCadSelecionar, fmCadSelecionar, False);
  VAR_CADASTRO := 0;
  if VAR_CADASTRO <> 0 then
  begin
      UMyMod.SetaCodigoPesquisado(EdClientRef, CBClientRef, QrENtidades, VAR_CADASTRO);
      EdClientRef.SetFocus;
  end else
    ReopenEntidades();
end;

procedure TFmOVcYnsMixCad.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmOVcYnsMixCad.SbNovoClick(Sender: TObject);
begin
  // LStyleService.GetSystemColor(clHighlightText);
  LaTitulo1C.Font.Color := MyObjects.StyleServiceObtemCor(clBtnFace);
  LaTitulo1B.Font.Color := MyObjects.StyleServiceObtemCor(clHighlightText);
  LaTitulo1A.Font.Color := MyObjects.StyleServiceObtemCor(clHighlight);
  //EXIT;
  LaRegistro.Caption := GOTOy.CodUsu(QrOVcYnsMedCadCodigo.Value, LaRegistro.Caption);
end;

procedure TFmOVcYnsMixCad.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmOVcYnsMixCad.QrOVcYnsMedCadAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmOVcYnsMixCad.QrOVcYnsMedCadAfterScroll(DataSet: TDataSet);
begin
  if FRegCadSel <> QrOVcYnsMedCadCodigo.Value then
    FRegistrosAdicionados := 0;
  FRegCadSel := QrOVcYnsMedCadCodigo.Value;
  //
  ReopenOVcYnsMedTop(0);
end;

procedure TFmOVcYnsMixCad.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //
  if FSeq = 1 then
    CabInclui1Click(Self)
  else
  if (not FLocIni) and (FCabIni <> 0)  then
  begin
    LocCod(FCabIni, FCabIni);
    if QrOVcYnsMedCadCodigo.Value <> FCabIni then
      Geral.MB_Aviso('Grupo n�o localizado!');
    FLocIni := True;
  end;
end;

procedure TFmOVcYnsMixCad.FormatoPanormico1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmOVcYnsMedDimFil, FmOVcYnsMedDimFil, afmoNegarComAviso) then
  begin
    FmOVcYnsMedDimFil.FCodigo := QrOVcYnsMedCadCodigo.Value;
    FmOVcYnsMedDimFil.DefineGradeAddingOwns(TExpandStyle.emFixedSize);
    FmOVcYnsMedDimFil.ShowModal;
    FmOVcYnsMedDimFil.Destroy;
  end;
end;

procedure TFmOVcYnsMixCad.SbQueryClick(Sender: TObject);
begin
  LocCod(QrOVcYnsMedCadCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'ovcynsmedcad', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmOVcYnsMixCad.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmOVcYnsMixCad.CabInclui1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrOVcYnsMedCad, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'ovcynsmedcad');
  EdPontosCrit.ValueVariant := 4;
  EdArtigRef.ValueVariant   := FArtigo;
  CBArtigRef.KeyValue       := FArtigo;
end;

procedure TFmOVcYnsMixCad.CopiaNomeArtigo();
begin
  EdNome.ValueVariant := CBArtigRef.Text;
end;

procedure TFmOVcYnsMixCad.QrOVcYnsMedCadBeforeClose(
  DataSet: TDataSet);
begin
  QrOVcYnsMedTop.Close;
end;

procedure TFmOVcYnsMixCad.QrOVcYnsMedCadBeforeOpen(DataSet: TDataSet);
begin
  QrOVcYnsMedCadCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmOVcYnsMixCad.QrOVcYnsMedTopAfterScroll(DataSet: TDataSet);
begin
  ReopenOVcYnsMedDim(0);
end;

procedure TFmOVcYnsMixCad.QrOVcYnsMedTopBeforeClose(DataSet: TDataSet);
begin
  QrOVcYnsMedDim.Close;
end;

end.

