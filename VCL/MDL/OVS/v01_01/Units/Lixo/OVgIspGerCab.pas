unit OVgIspGerCab;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkPermissoes;

type
  TFmOVgIspGerCab = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    GroupBox2: TGroupBox;
    PnDadosOri: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    EdLocal: TdmkEdit;
    EdNO_Local: TdmkEdit;
    EdNrOP: TdmkEdit;
    EdSeqGrupo: TdmkEdit;
    EdNO_SeqGrupo: TdmkEdit;
    EdNrReduzidoOP: TdmkEdit;
    GroupBox3: TGroupBox;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }

  public
    { Public declarations }
  end;

  var
  FmOVgIspGerCab: TFmOVgIspGerCab;

implementation

uses UnMyObjects;

{$R *.DFM}

procedure TFmOVgIspGerCab.BtOKClick(Sender: TObject);
var
  DtHrAbert, DtHrFecha, ZtatusDtH: String;
  Codigo, Local, NrOP, SeqGrupo, NrReduzidoOP, OVcYnsMed, OVcYnsChk, LimiteChk,
  LimiteMed, ZtatusIsp, ZtatusMot: Integer;
  SQLType: TSQLType;
begin
{
  SQLType := ImgTipo.SQLType?;
  Codigo         := ;
  Local          := ;
  NrOP           := ;
  SeqGrupo       := ;
  NrReduzidoOP   := ;
  DtHrAbert      := ;
  DtHrFecha      := ;
  OVcYnsMed      := ;
  OVcYnsChk      := ;
  LimiteChk      := ;
  LimiteMed      := ;
  ZtatusIsp      := ;
  ZtatusDtH      := ;
  ZtatusMot      := ;

  //
? := UMyMod.BuscaEmLivreY_Def('ovgispgercab', 'Codigo', SQLType, CodAtual?);
ou > ? := UMyMod.BPGS1I32('ovgispgercab', 'Codigo', '', '', tsPosNeg?, stInsUpd?, CodAtual?);
if UMyMod.SQLInsUpd_IGNORE?(Dmod.QrUpd?, SQLType, 'ovgispgercab', auto_increment?[
'Local', 'NrOP', 'SeqGrupo',
'NrReduzidoOP', 'DtHrAbert', 'DtHrFecha',
'OVcYnsMed', 'OVcYnsChk', 'LimiteChk',
'LimiteMed', 'ZtatusIsp', 'ZtatusDtH',
'ZtatusMot'], [
'Codigo'], [
Local, NrOP, SeqGrupo,
NrReduzidoOP, DtHrAbert, DtHrFecha,
OVcYnsMed, OVcYnsChk, LimiteChk,
LimiteMed, ZtatusIsp, ZtatusDtH,
ZtatusMot], [
Codigo], UserDataAlterweb?, IGNORE?
}
end;

procedure TFmOVgIspGerCab.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmOVgIspGerCab.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmOVgIspGerCab.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
end;

procedure TFmOVgIspGerCab.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

end.
