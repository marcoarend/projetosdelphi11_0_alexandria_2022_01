unit OVcYnsMedDimFil;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkPermissoes,
  mySQLDbTables, UnOVS_Vars, mySQLDirectQuery;

type
  THackWinControl = class(TWinControl);
  TFmOVcYnsMedDimFil = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Panel5: TPanel;
    Panel6: TPanel;
    QrTop: TMySQLQuery;
    QrTopNO_TolerBasCalc: TWideStringField;
    QrTopNO_TolerUnMdida: TWideStringField;
    QrTopNO_TOBIKO: TWideStringField;
    QrTopCodigo: TIntegerField;
    QrTopControle: TIntegerField;
    QrTopTobiko: TIntegerField;
    QrTopTolerBasCalc: TSmallintField;
    QrTopTolerUnMdida: TSmallintField;
    QrTopTolerRndPerc: TFloatField;
    QrTopLk: TIntegerField;
    QrTopDataCad: TDateField;
    QrTopDataAlt: TDateField;
    QrTopUserCad: TIntegerField;
    QrTopUserAlt: TIntegerField;
    QrTopAlterWeb: TSmallintField;
    QrTopAWServerID: TIntegerField;
    QrTopAWStatSinc: TSmallintField;
    QrTopAtivo: TSmallintField;
    ScrollBox1: TScrollBox;
    GPBoxes: TGridPanel;
    DqAux: TMySQLDirectQuery;
    QrRow: TMySQLQuery;
    QrRowCodGrade: TIntegerField;
    QrRowCodTam: TWideStringField;
    QrRowOrdTam: TWideStringField;
    StringGrid1: TStringGrid;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    //
    procedure EditKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    //
  private
    { Private declarations }
    FGradesToAdd: TGradesToAdd;
    FTamsToAdd: TTamsToAdd;
    Panels: array of array of TPanel;
    //
    //
    procedure ReopenTop();
    //procedure ReopenCol();
    procedure ReopenRow();
  public
    { Public declarations }
    FCodigo: Integer;
    //
(*
    procedure DefineGradeAddingRows();
    procedure DefineGradeAddingCols();
*)
    procedure DefineGradeAddingOwns(Style: TExpandStyle);
  end;

  var
  FmOVcYnsMedDimFil: TFmOVcYnsMedDimFil;

implementation

uses UnMyObjects, DmkDAC_PF, Module, UMySQLDB;

{$R *.DFM}

const
  cWEdTam = 112;
  cWEdVal = 56 * 3;

procedure TFmOVcYnsMedDimFil.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

{
procedure TFmOVcYnsMedDimFil.DefineGradeAddingCols();
var
  teste, J, Col, Row: Integer;
  AllWid, AllHei: Extended;
  Pn: TPanel;
  Ed: TdmkEdit;
begin
  GPBoxes.ExpandStyle := emAddColumns;
  //
  Col := 0;
  Row := 0;
  AllWid := 0;
  AllHei := 0;
  // Limpal GridPanel
  GPBoxes.RowCollection.BeginUpdate;
  GPBoxes.ColumnCollection.BeginUpdate;
  for J := 0 to -1 + GPBoxes.ControlCount do
    GPBoxes.Controls[0].Free;
  GPBoxes.RowCollection.Clear;
  GPBoxes.ColumnCollection.Clear;
  //
  // Definir Quantidade e dados de colunas
  ReopenCol();
  // Dados de Topicos
  //  COLUNA DOS T�PICOS
  GPBoxes.ColumnCollection.Add;
  GPBoxes.ColumnCollection[Col].SizeStyle := ssAbsolute;
  GPBoxes.ColumnCollection[Col].Value     := cWEdTam;
  AllWid := AllWid + GPBoxes.ColumnCollection[Col].Value;
  ReopenTop();
  // TITULO DOS T�PICOS
  GPBoxes.RowCollection.Add;
  GPBoxes.RowCollection[Row].SizeStyle    := ssAbsolute;
  GPBoxes.RowCollection[Row].Value        := 24;
  AllHei := AllHei + GPBoxes.RowCollection[Row].Value;
  Pn := TPanel.Create(FmOVcYnsMedDimFil);
  Pn.Parent := GPBoxes; // Adiciona na pr�xima c�lula dispon�vel!
  Pn.Name   := 'PnFundamental';
  Pn.Tag    := 0;
  Pn.Align := alClient;
  Pn.Caption := 'V>';
  QrCol.First;
  while not QrCol.Eof do
  begin
    Col := Col + 1;
    //
    GPBoxes.ColumnCollection.Add;
    GPBoxes.ColumnCollection[Col].SizeStyle := ssAbsolute;
    GPBoxes.ColumnCollection[Col].Value     := cWEdVal;
    AllWid := AllWid + GPBoxes.ColumnCollection[Col].Value;
    //
    Pn := TPanel.Create(FmOVcYnsMedDimFil);
    Pn.Parent := GPBoxes; // Adiciona na pr�xima c�lula dispon�vel!
    //Pn.Name   := 'PnTit_' + QrColCodTam.Value + '_' + Geral.FF0(QrColCodGrade.Value);
    Pn.Tag    := 0;
    Pn.Align := alClient;
    Pn.Caption := QrColCodTam.Value;
    //
    QrCol.Next;
  end;
  while not QrTop.Eof do
  begin
    Row := Row + 1;
    //
    GPBoxes.RowCollection.Add;
    GPBoxes.RowCollection[Row].SizeStyle    := ssAbsolute;
    GPBoxes.RowCollection[Row].Value        := 24;
    AllHei := AllHei + GPBoxes.RowCollection[Row].Value;
    //
    Pn := TPanel.Create(FmOVcYnsMedDimFil);
    Pn.Parent := GPBoxes; // Adiciona na pr�xima c�lula dispon�vel!
    Pn.Name   := 'Topico_' + Geral.FF0(QrTopControle.Value);
    Pn.Tag    := QrTopControle.Value;
    Pn.Align := alClient;
    Pn.Alignment := taLeftJustify;
    Pn.Caption := QrTopNO_TOBIKO.Value;
    //
    QrCol.First;
    while not QrCol.Eof do
    begin
      Col := Col + 1;
      //
      UnDmkDAC_PF.AbreMySQLDirectQuery0(DqAux, Dmod.MyDB, [
      'SELECT Conta  ',
      'FROM ovcynsmeddim ',
      'WHERE Codigo=' + Geral.FF0(FCodigo),
      'AND Controle=' + Geral.FF0(QrTopControle.Value),
      'AND CodGrade=' + Geral.FF0(QrColCodGrade.Value),
      'AND CodTam="' + QrColCodTam.Value + '"',
      '']);
(*
      GPBoxes.ColumnCollection.Add;
      GPBoxes.ColumnCollection[Col].SizeStyle := ssAbsolute;
      GPBoxes.ColumnCollection[Col].Value     := 56;
      //
*)
      Pn := TPanel.Create(FmOVcYnsMedDimFil);
      Pn.Parent := GPBoxes; // Adiciona na pr�xima c�lula dispon�vel!
      //Pn.Name   := 'PnTam_' + QrColCodTam.Value + '_' + Geral.FF0(QrColCodGrade.Value);
      Pn.Tag    := USQLDB.Int(DqAux, 'Conta');
      Pn.Align := alClient;
      Pn.Caption := Geral.FF0(Pn.Tag);
      //
      Ed := TdmkEdit.Create(FmOVcYnsMedDimFil);
      Ed.Parent := Pn;
      //Ed.Name := 'DBEdPalletMedia' + IdTXT;
      Ed.Align := alRight;
      Ed.Alignment := taRightJustify;
      Ed.Font.Charset := DEFAULT_CHARSET;
      Ed.Font.Color := clWindowText;
//      Ed.Font.Height := GetFontHeight(-19); //-19(*14*));
      Ed.Font.Name := 'Tahoma';
      Ed.Font.Style := [fsBold];
      Ed.ParentFont := False;
      //Ed.ReadOnly := True;
      Ed.TabOrder := 0;
      Ed.Text := '0.000';
      Ed.FormatType := dmktfDouble;
      Ed.MskType := fmtNone;
      Ed.DecimalSize := 3;
      Ed.LeftZeros := 0;
      //Ed.NoEnterToTab := False;
      Ed.NoEnterToTab := True;
      Ed.NoForceUppercase := False;
      Ed.ForceNextYear := False;
      Ed.DataFormat := dmkdfShort;
      Ed.HoraFormat := dmkhfShort;
      Ed.Texto := '0,000';
      Ed.UpdType := utYes;
      Ed.Obrigatorio := False;
      Ed.PermiteNulo := False;
      Ed.ValueVariant := 0;
      Ed.ValWarn := False;
      Ed.Width := 56;
      //
      QrCol.Next;
    end;
    //
    QrTop.Next;
  end;
  GPBoxes.Width := Trunc(AllWid + 16);
  GPBoxes.Height := Trunc(AllHei + 16);
  //
end;
}

procedure TFmOVcYnsMedDimFil.DefineGradeAddingOwns(Style: TExpandStyle);
var
  I, J, W, Col, Row, Cols, Rows, PanelsCount: Integer;
  AllWid, AllHei: Extended;
  Pn: TPanel;
  Ed: TdmkEdit;
  _Controle, _CodGrade, _CodTam: String;
begin
  GPBoxes.ExpandStyle := Style; //TExpandStyle.emFixedSize;
  //
  Col := 0;
  Row := 0;
  AllWid := 0;
  AllHei := 0;
  // Limpal GridPanel
  GPBoxes.RowCollection.BeginUpdate;
  GPBoxes.ColumnCollection.BeginUpdate;
  for J := 0 to -1 + GPBoxes.ControlCount do
    GPBoxes.Controls[0].Free;
  GPBoxes.RowCollection.Clear;
  GPBoxes.ColumnCollection.Clear;
  //
  // Definir Quantidade e dados de colunas e linhas
  ReopenRow();
  ReopenTop();
  Cols := QrTop.RecordCount + 1;
  Rows := QrRow.RecordCount + 1;
  PanelsCount := Cols * Rows;
  SetLength(Panels, Rows);
  for I := 0 to Rows - 1 do
    SetLength(Panels[I], Cols);
  //
  //GridPanel1.ControlCollection.Controls[2,3]
  W := cWEdTam;
  for I := 0 to Cols - 1 do
  begin
    GPBoxes.ColumnCollection.Add;
    GPBoxes.ColumnCollection[Col].SizeStyle := ssAbsolute;
    GPBoxes.ColumnCollection[Col].Value     := W;
    AllWid := AllWid + GPBoxes.ColumnCollection[Col].Value;
    W := cWEdVal;
  end;
  //
  for I := 0 to Rows - 1 do
  begin
    GPBoxes.RowCollection.Add;
    GPBoxes.RowCollection[Row].SizeStyle    := ssAbsolute;
    GPBoxes.RowCollection[Row].Value        := 24;
    AllHei := AllHei + GPBoxes.RowCollection[Row].Value;
  end;
  //
  //C := 0;
  //R := 1;
  QrTop.First;
  QrRow.First;
  _Controle := '';
  _CodGrade := '';
  _CodTam   := '';
  for I := 0 to PanelsCount - 1 do
  begin
    Pn := TPanel.Create(FmOVcYnsMedDimFil);
    Pn.Parent := GPBoxes; // Adiciona na pr�xima c�lula dispon�vel!
    Pn.Name   := 'Panel_' + Geral.FF0(I);
    //Pn.Tag    := QrTopControle.Value;
    Pn.Align := alClient;
    Pn.Alignment := taLeftJustify;
    Pn.Caption := Pn.Name;
    //
    Col := (I + Cols) mod Cols;
    Row := (I (*+ Rows*)) div Cols;
    Panels[Col, Row] := Pn;
    //
    Pn.Caption := '';
    if Col = 0 then
    begin
      QrTop.First;
    end;
    if Row = 0 then
    begin
      if Col > 0 then
      begin
        Pn.Caption := QrTopNO_TOBIKO.Value;
        if Col < Cols then
          QrTop.Next;
      end;
    end else
    begin
      if Col = 0 then
        Pn.Caption := QrRowCodTam.Value;
    end;
    //
    if (Row > 0) and (Col > 0) then
    begin
      Pn.Tag    := QrTopControle.Value;
      _Controle := Geral.FF0(Pn.Tag);
      //
      if Col < Cols then
        QrTop.Next;
      //
      Pn.Caption := QrRowCodTam.Value;
      Pn.Hint    := Geral.FF0(QrRowCodGrade.Value);
      //Pn.Name := '_' + Geral.FF0(Pn.Tag) + '_' + Pn.Caption + '_' + Pn.Hint;
      _CodGrade := Pn.Hint;
      _CodTam   := Pn.Caption;
      //
      if Col = Cols - 1 then
        QrRow.Next;
      //
      Pn.Caption := '_' + _Controle + '_' + _CodGrade + '_' + _CodTam;
      //
      UnDmkDAC_PF.AbreMySQLDirectQuery0(DqAux, Dmod.MyDB, [
      'SELECT Conta, MedidCer, MedidMin, MedidMax ',
      'FROM ovcynsmeddim ',
      'WHERE Codigo=' + Geral.FF0(FCodigo),
      'AND Controle=' + _Controle,
      'AND CodGrade=' + _CodGrade,
      'AND CodTam="' + _CodTam + '"',
      '']);
      //  Medida certa -> alLeft
      Ed := TdmkEdit.Create(FmOVcYnsMedDimFil);
      Ed.Parent := Pn;
      //Ed.Name := 'DBEdPalletMedia' + IdTXT;
      Ed.Align := alLeft;
      Ed.Alignment := taRightJustify;
      Ed.Font.Charset := DEFAULT_CHARSET;
      Ed.Font.Color := clWindowText;
//      Ed.Font.Height := GetFontHeight(-19); //-19(*14*));
      Ed.Font.Name := 'Tahoma';
      //Ed.Font.Style := [fsBold];
      Ed.ParentFont := False;
      //Ed.ReadOnly := True;
      Ed.TabOrder := 0;
      Ed.Text := '0.000';
      Ed.FormatType := dmktfDouble;
      Ed.MskType := fmtNone;
      Ed.DecimalSize := 3;
      Ed.LeftZeros := 0;
      //Ed.NoEnterToTab := False;
      Ed.NoEnterToTab := True;
      Ed.NoForceUppercase := False;
      Ed.ForceNextYear := False;
      Ed.DataFormat := dmkdfShort;
      Ed.HoraFormat := dmkhfShort;
      Ed.Texto := '0,000';
      Ed.UpdType := utYes;
      Ed.Obrigatorio := False;
      Ed.PermiteNulo := False;
      Ed.ValueVariant := 0;
      Ed.ValWarn := False;
      Ed.Width := 56;
      Ed.OnKeyUp := EditKeyUp;
      Ed.Tag := USQLDB.Int(DqAux, 'Conta');
      Ed.Hint := 'MedidCer';
      Ed.ValueVariant := USQLDB.Flu(DqAux, 'MedidCer');
      //
      //  Medida m�xima -> alRigth
      Ed := TdmkEdit.Create(FmOVcYnsMedDimFil);
      Ed.Parent := Pn;
      //Ed.Name := 'DBEdPalletMedia' + IdTXT;
      Ed.Align := alRight;
      Ed.Alignment := taRightJustify;
      Ed.Font.Charset := DEFAULT_CHARSET;
      Ed.Font.Color := clWindowText;
//      Ed.Font.Height := GetFontHeight(-19); //-19(*14*));
      Ed.Font.Name := 'Tahoma';
      //Ed.Font.Style := [fsBold];
      Ed.ParentFont := False;
      //Ed.ReadOnly := True;
      //Ed.TabOrder := 0;  Vai ser 2!!
      Ed.Text := '0.000';
      Ed.FormatType := dmktfDouble;
      Ed.MskType := fmtNone;
      Ed.DecimalSize := 3;
      Ed.LeftZeros := 0;
      //Ed.NoEnterToTab := False;
      Ed.NoEnterToTab := True;
      Ed.NoForceUppercase := False;
      Ed.ForceNextYear := False;
      Ed.DataFormat := dmkdfShort;
      Ed.HoraFormat := dmkhfShort;
      Ed.Texto := '0,000';
      Ed.UpdType := utYes;
      Ed.Obrigatorio := False;
      Ed.PermiteNulo := False;
      Ed.ValueVariant := 0;
      Ed.ValWarn := False;
      Ed.Width := 56;
      Ed.OnKeyUp := EditKeyUp;
      Ed.Tag := USQLDB.Int(DqAux, 'Conta');
      Ed.Hint := 'MedidMax';
      Ed.ValueVariant := USQLDB.Flu(DqAux, 'MedidMax');
      //
      //  Medida m�nima -> alClient
      Ed := TdmkEdit.Create(FmOVcYnsMedDimFil);
      Ed.Parent := Pn;
      //Ed.Name := 'DBEdPalletMedia' + IdTXT;
      Ed.Align := alClient;
      Ed.Alignment := taRightJustify;
      Ed.Font.Charset := DEFAULT_CHARSET;
      Ed.Font.Color := clWindowText;
//      Ed.Font.Height := GetFontHeight(-19); //-19(*14*));
      Ed.Font.Name := 'Tahoma';
      //Ed.Font.Style := [fsBold];
      Ed.ParentFont := False;
      //Ed.ReadOnly := True;
      Ed.TabOrder := 1;
      Ed.Text := '0.000';
      Ed.FormatType := dmktfDouble;
      Ed.MskType := fmtNone;
      Ed.DecimalSize := 3;
      Ed.LeftZeros := 0;
      //Ed.NoEnterToTab := False;
      Ed.NoEnterToTab := True;
      Ed.NoForceUppercase := False;
      Ed.ForceNextYear := False;
      Ed.DataFormat := dmkdfShort;
      Ed.HoraFormat := dmkhfShort;
      Ed.Texto := '0,000';
      Ed.UpdType := utYes;
      Ed.Obrigatorio := False;
      Ed.PermiteNulo := False;
      Ed.ValueVariant := 0;
      Ed.ValWarn := False;
      Ed.Width := 56;
      Ed.OnKeyUp := EditKeyUp;
      Ed.Tag := USQLDB.Int(DqAux, 'Conta');
      Ed.Hint := 'MedidMin';
      Ed.ValueVariant := USQLDB.Flu(DqAux, 'MedidMin');
    end;
  end;
  //GridPanel1.ControlCollection.Controls[2,3]
  GPBoxes.Width  := Trunc(AllWid + 16);
  GPBoxes.Height := Trunc(AllHei + 16);
end;

{
procedure TFmOVcYnsMedDimFil.DefineGradeAddingRows();
var
  teste, J, Col, Row: Integer;
  AllWid, AllHei: Extended;
  Pn: TPanel;
  Ed: TdmkEdit;
begin
  GPBoxes.ExpandStyle := emAddRows;
  //
  Col := 0;
  Row := 0;
  AllWid := 0;
  AllHei := 0;
  // Limpal GridPanel
  GPBoxes.RowCollection.BeginUpdate;
  GPBoxes.ColumnCollection.BeginUpdate;
  for J := 0 to -1 + GPBoxes.ControlCount do
    GPBoxes.Controls[0].Free;
  GPBoxes.RowCollection.Clear;
  GPBoxes.ColumnCollection.Clear;
  //
  // Definir Quantidade e dados de colunas
  ReopenCol();
  // Dados de Topicos
  //  COLUNA DOS T�PICOS
  GPBoxes.ColumnCollection.Add;
  GPBoxes.ColumnCollection[Col].SizeStyle := ssAbsolute;
  GPBoxes.ColumnCollection[Col].Value     := cWEdTam;
  AllWid := AllWid + GPBoxes.ColumnCollection[Col].Value;
  ReopenTop();
  // TITULO DOS T�PICOS
  GPBoxes.RowCollection.Add;
  GPBoxes.RowCollection[Row].SizeStyle    := ssAbsolute;
  GPBoxes.RowCollection[Row].Value        := 24;
  AllHei := AllHei + GPBoxes.RowCollection[Row].Value;
  Pn := TPanel.Create(FmOVcYnsMedDimFil);
  Pn.Parent := GPBoxes; // Adiciona na pr�xima c�lula dispon�vel!
  Pn.Name   := 'PnFundamental';
  Pn.Tag    := 0;
  Pn.Align := alClient;
  Pn.Caption := 'V>';
  QrCol.First;
  while not QrCol.Eof do
  begin
    Col := Col + 1;
    //
    GPBoxes.ColumnCollection.Add;
    GPBoxes.ColumnCollection[Col].SizeStyle := ssAbsolute;
    GPBoxes.ColumnCollection[Col].Value     := cWEdVal;
    AllWid := AllWid + GPBoxes.ColumnCollection[Col].Value;
    //
    Pn := TPanel.Create(FmOVcYnsMedDimFil);
    Pn.Parent := GPBoxes; // Adiciona na pr�xima c�lula dispon�vel!
    //Pn.Name   := 'PnTit_' + QrColCodTam.Value + '_' + Geral.FF0(QrColCodGrade.Value);
    Pn.Tag    := 0;
    Pn.Align := alClient;
    Pn.Caption := QrColCodTam.Value;
    //
    QrCol.Next;
  end;
  while not QrTop.Eof do
  begin
    Row := Row + 1;
    //
    GPBoxes.RowCollection.Add;
    GPBoxes.RowCollection[Row].SizeStyle    := ssAbsolute;
    GPBoxes.RowCollection[Row].Value        := 24;
    AllHei := AllHei + GPBoxes.RowCollection[Row].Value;
    //
    Pn := TPanel.Create(FmOVcYnsMedDimFil);
    Pn.Parent := GPBoxes; // Adiciona na pr�xima c�lula dispon�vel!
    Pn.Name   := 'Topico_' + Geral.FF0(QrTopControle.Value);
    Pn.Tag    := QrTopControle.Value;
    Pn.Align := alClient;
    Pn.Alignment := taLeftJustify;
    Pn.Caption := QrTopNO_TOBIKO.Value;
    //
    QrCol.First;
    while not QrCol.Eof do
    begin
      Col := Col + 1;
      //
      UnDmkDAC_PF.AbreMySQLDirectQuery0(DqAux, Dmod.MyDB, [
      'SELECT Conta  ',
      'FROM ovcynsmeddim ',
      'WHERE Codigo=' + Geral.FF0(FCodigo),
      'AND Controle=' + Geral.FF0(QrTopControle.Value),
      'AND CodGrade=' + Geral.FF0(QrColCodGrade.Value),
      'AND CodTam="' + QrColCodTam.Value + '"',
      '']);
(*
      GPBoxes.ColumnCollection.Add;
      GPBoxes.ColumnCollection[Col].SizeStyle := ssAbsolute;
      GPBoxes.ColumnCollection[Col].Value     := 56;
      //
*)
      Pn := TPanel.Create(FmOVcYnsMedDimFil);
      Pn.Parent := GPBoxes; // Adiciona na pr�xima c�lula dispon�vel!
      //Pn.Name   := 'PnTam_' + QrColCodTam.Value + '_' + Geral.FF0(QrColCodGrade.Value);
      Pn.Tag    := USQLDB.Int(DqAux, 'Conta');
      Pn.Align := alClient;
      Pn.Caption := Geral.FF0(Pn.Tag);
      //
      Ed := TdmkEdit.Create(FmOVcYnsMedDimFil);
      Ed.Parent := Pn;
      //Ed.Name := 'DBEdPalletMedia' + IdTXT;
      Ed.Align := alRight;
      Ed.Alignment := taRightJustify;
      Ed.Font.Charset := DEFAULT_CHARSET;
      Ed.Font.Color := clWindowText;
//      Ed.Font.Height := GetFontHeight(-19); //-19(*14*));
      Ed.Font.Name := 'Tahoma';
      Ed.Font.Style := [fsBold];
      Ed.ParentFont := False;
      //Ed.ReadOnly := True;
      Ed.TabOrder := 0;
      Ed.Text := '0.000';
      Ed.FormatType := dmktfDouble;
      Ed.MskType := fmtNone;
      Ed.DecimalSize := 3;
      Ed.LeftZeros := 0;
      //Ed.NoEnterToTab := False;
      Ed.NoEnterToTab := True;
      Ed.NoForceUppercase := False;
      Ed.ForceNextYear := False;
      Ed.DataFormat := dmkdfShort;
      Ed.HoraFormat := dmkhfShort;
      Ed.Texto := '0,000';
      Ed.UpdType := utYes;
      Ed.Obrigatorio := False;
      Ed.PermiteNulo := False;
      Ed.ValueVariant := 0;
      Ed.ValWarn := False;
      Ed.Width := 56;
      Ed.OnKeyUp := EditKeyUp;
      //
      QrCol.Next;
    end;
    //
    QrTop.Next;
  end;
  GPBoxes.Width := Trunc(AllWid + 16);
  GPBoxes.Height := Trunc(AllHei + 16);
  //
end;
}

procedure TFmOVcYnsMedDimFil.EditKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_RETURN then
  begin
    //Key := VK_TAB;
    PostMessage(GetParentForm(Self).Handle, wm_NextDlgCtl, Ord((ssShift in Shift)), 0);
    Key := 0;
  end;
end;

procedure TFmOVcYnsMedDimFil.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmOVcYnsMedDimFil.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  DqAux.Close;
  QrTop.Close;
  QrRow.Close;
end;

procedure TFmOVcYnsMedDimFil.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  GPBoxes.Align := alNone;
  GPBoxes.Top := 0;
  GPBoxes.Left := 0;
end;

procedure TFmOVcYnsMedDimFil.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

{
procedure TFmOVcYnsMedDimFil.ReopenCol();
var
  I: Integer;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrCol, Dmod.MyDB, [
  'SELECT DISTINCT ymd.CodGrade, ymd.CodTam, ',
  'LPAD(CodTam, 30, " ")  OrdTam',
  'FROM ovcynsmeddim ymd ',
  'LEFT JOIN ovdgradetam ogt ON  ',
  '  ogt.Codigo=ymd.CodGrade ',
  '  AND ',
  '  ogt.Nome=ymd.CodTam ',
  'WHERE ymd.Codigo=' + Geral.FF0(FCodigo),
  'ORDER BY ogt.Ordem, OrdTam, ogt.Nome ',
  '']);
  SetLength(FGradesToAdd, QrCol.RecordCount);
  SetLength(FTamsToAdd, QrCol.RecordCount);
  QrCol.First;
  while not QrCol.Eof do
  begin
    I := QrCol.RecNo -1;
    FGradesToAdd[I] := QrColCodGrade.Value;
    FTamsToAdd[I] := QrColCodTam.Value;
    QrCol.Next;
  end;
end;
}

procedure TFmOVcYnsMedDimFil.ReopenRow;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrRow, Dmod.MyDB, [
  'SELECT DISTINCT ymd.CodGrade, ymd.CodTam, ',
  'LPAD(CodTam, 30, " ")  OrdTam',
  'FROM ovcynsmeddim ymd ',
  'LEFT JOIN ovdgradetam ogt ON  ',
  '  ogt.Codigo=ymd.CodGrade ',
  '  AND ',
  '  ogt.Nome=ymd.CodTam ',
  'WHERE ymd.Codigo=' + Geral.FF0(FCodigo),
  'ORDER BY ogt.Ordem, OrdTam, ogt.Nome ',
  '']);
end;

procedure TFmOVcYnsMedDimFil.ReopenTop();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrTop, Dmod.MyDB, [
  'SELECT ELT(ymt.TolerBasCalc+1, "Medida", "Percentual") NO_TolerBasCalc, ',
  'ELT(ymt.TolerUnMdida+1, "cm", "inch") NO_TolerUnMdida,  ',
  'ygt.Nome NO_TOBIKO, ymt.* ',
  'FROM ovcynsmedtop ymt ',
  'LEFT JOIN ovcynsgratop ygt ON ygt.Codigo=ymt.Tobiko ',
  'WHERE ymt.Codigo=' + Geral.FF0(FCodigo),
  '']);
  //
  //QrTop.Locate('Controle', Controle, []);
end;

{
procedure TFmVSClassifOneNw3.DefineBoxes(Max: Integer);
var
  I, Col, Row: Integer;
begin
  GPBoxes.Visible := False;
  try
    if FBoxMax < Max then
      FBoxMax := Max;
    //
    GPBoxes.ColumnCollection.BeginUpdate;
    GPBoxes.ColumnCollection.Clear;
    GPBoxes.RowCollection.BeginUpdate;
    GPBoxes.RowCollection.Clear;
    case FBoxMax of
      00..06: begin Col := 3; Row := 2; end;
      07..09: begin Col := 3; Row := 3; end;
      10..12: begin Col := 4; Row := 3; end;
      13..15: begin Col := 5; Row := 3; end;
    end;
    for I := 0 to Col - 1 do
      GPBoxes.ColumnCollection.Add;
    for I := 0 to Row - 1 do
      GPBoxes.RowCollection.Add;
    //
    GPBoxes.ColumnCollection.EndUpdate;
    GPBoxes.RowCollection.EndUpdate;
    for I := 1 to FBoxMax do
    begin
      VS_CRC_PF.CriaBoxEmGridPanel(I, Self, GPBoxes, FPnTxLx, FPnBox, FPnArt1N,    // *&�
      FPnArt2N, FPnArt3N, FPnNumBx, FPnIDBox, FPnSubXx, FPnBxTot, FPnIMEIImei,
      FPnIMEICtrl, FPnPalletMedia, FPnPalletArea, FPnPalletPecas, FPnPalletID,
      FPnIntMei, FLaNumBx, FLaIMEIImei, FLaIMEICtrl, FLaPalletMedia, FLaPalletArea,
      FLaPalletPecas, FLaPalletID, FCkSubClass, FDBEdArtNome, FDBEdArtNoCli,
      FDBEdArtNoSta, FDBEdIMEIImei, FDBEdIMEICtrl, FDBEdPalletID, FGBIMEI,
      FGBPallet, FEdPalletMedia, FEdPalletArea, FEdPalletPecas, FSGItens,
      FPMItens);
      FEdPalletPecas[I].OnChange := EdPecasXXChange;
      //
      FBoxes[I] := False;
      FErrosBox[I] := False;
      //
      FQrVSPallet[I] := TmySQLQuery.Create(Dmod);
      FDsVSPallet[I] := TDataSource.Create(Self);
      FDsVSPallet[I].DataSet := FQrVSPallet[I];
      FDBEdPalletID[I].DataSource := FDsVSPallet[I];
      FDBEdArtNome[I].DataSource  := FDsVSPallet[I];
      FDBEdArtNoCli[I].DataSource := FDsVSPallet[I];
      FDBEdArtNoSta[I].DataSource := FDsVSPallet[I];
      //
      FQrVSPalClaIts[I] := TmySQLQuery.Create(Dmod);
      FDsVSPalClaIts[I] := TDataSource.Create(Self);
      FDsVSPalClaIts[I].DataSet := FQrVSPalClaIts[I];
      FDBEdIMEICtrl[I].DataSource := FDsVSPalClaIts[I];
      FDBEdIMEIImei[I].DataSource := FDsVSPalClaIts[I];
      //
    end;
    //for I := 1 to VAR_CLA_ART_RIB_MAX_BOX_15
    FPalletDoBox[01] := PalletDoBox01;
    FPalletDoBox[02] := PalletDoBox02;
    FPalletDoBox[03] := PalletDoBox03;
    FPalletDoBox[04] := PalletDoBox04;
    FPalletDoBox[05] := PalletDoBox05;
    FPalletDoBox[06] := PalletDoBox06;
    FPalletDoBox[07] := PalletDoBox07;
    FPalletDoBox[08] := PalletDoBox08;
    FPalletDoBox[09] := PalletDoBox09;
    FPalletDoBox[10] := PalletDoBox10;
    FPalletDoBox[11] := PalletDoBox11;
    FPalletDoBox[12] := PalletDoBox12;
    FPalletDoBox[13] := PalletDoBox13;
    FPalletDoBox[14] := PalletDoBox14;
    FPalletDoBox[15] := PalletDoBox15;
  finally
    GPBoxes.Visible := True;
  end;
  //
end;

}




















{
procedure TUnVS_CRC_PF.CriaBoxEmGridPanel(const Box: Integer; const Form: TForm;
  const GridPanel: TGridPanel; var FPnTxLx, FPnBox, FPnArt1N, FPnArt2N,
  FPnArt3N, FPnNumBx, FPnIDBox, FPnSubXx, FPnBxTot, FPnIMEIImei, FPnIMEICtrl,
  FPnPalletMedia, FPnPalletArea, FPnPalletPecas, FPnPalletID,
  FPnIntMei: array of TPanel; var FLaNumBx, FLaIMEIImei, FLaIMEICtrl,
  FLaPalletMedia, FLaPalletArea, FLaPalletPecas, FLaPalletID: array of TLabel;
  var FCkSubClass: array of TCheckBox; var FDBEdArtNome, FDBEdArtNoCli,
  FDBEdArtNoSta, FDBEdIMEIImei, FDBEdIMEICtrl, FDBEdPalletID: array of TDBEdit;
  var FGBIMEI, FGBPallet: array of TGroupBox; var FEdPalletMedia, FEdPalletArea,
  FEdPalletPecas: array of TdmkEdit; var FSGItens: array of TStringGrid;
  FPMItens: array of TPopupMenu);
var
  FatorFonte: Double;
  //
  function GetFontHeight(Base: Integer): Integer;
  var
    Tam: Integer;
  begin
    Tam := Trunc(-Base * (3 / GridPanel.ColumnCollection.Count));
    //Result := -Trunc(Tam * (Screen.Width / 1920));
    Result := Trunc(Tam * FatorFonte);
  end;

  function ObtemHeigth(Base: Integer): Integer;
  var
    Tam: Integer;
    H1, H2: Integer;
  begin
    Tam := Trunc(Base * (3 / GridPanel.ColumnCollection.Count));
    H1 := Trunc(Tam * (Screen.Height / 1080));
    //H2 := Trunc(Tam * (Screen.Width * 1920));
    //if H1 < H2 then
      Result := H1
    //else
      //Result := H2;
  end;

  function ObtemWidth(Base: Integer): Integer;
  var
    Tam: Integer;
  begin
    Tam := Trunc(Base * (3 / GridPanel.ColumnCollection.Count));
    Result := Trunc(Tam * (Screen.Width / 1920));
  end;

var
  IdTXT: String;
  Painel: TPanel;
begin
  if (Screen.Width / 1920) > (Screen.Height / 1080) then
    FatorFonte := Screen.Height / 1080
  else
    FatorFonte := Screen.Width / 1920;
  //
  IdTXT := Geral.FFF(Box, 2);
(*
object PnBoxT1L1: TPanel
  Align = alClient
*)
  FPnTxLx[Box] := TPanel.Create(Form);
  FPnTxLx[Box].Parent := GridPanel;
  FPnTxLx[Box].Name := 'PnTxLx' + IdTXT;
  FPnTxLx[Box].Align := alClient;
  //FPnTxLx[Box].BevelOuter := bvNone;
  FPnTxLx[Box].Caption := '';
(*
  object PnBox01: TPanel
    Align = alClient
    BevelOuter = bvNone
*)
  FPnBox[Box] := TPanel.Create(Form);
  FPnBox[Box].Parent := FPnTxLx[Box];
  FPnBox[Box].Name := 'PnBox' + IdTXT;
  FPnBox[Box].Align := alClient;
  FPnBox[Box].BevelOuter := bvNone;
  FPnBox[Box].Caption := '';
(*
    object Panel4: TPanel
      Height = 64
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 1
*)
    FPnArt1N[Box] := TPanel.Create(Form);
    FPnArt1N[Box].Parent := FPnBox[Box];
    FPnArt1N[Box].Name := 'PnArt1N' + IdTXT;
    //FPnArt1N[Box].Height := ObtemHeigth(72) + 4;
    FPnArt1N[Box].Height := ObtemHeigth(56) + 4;
    FPnArt1N[Box].Align := alTop;
    FPnArt1N[Box].BevelOuter := bvNone;
    FPnArt1N[Box].Caption := '';
(*
      object Panel50: TPanel
        Left = 0
        Top = 0
        Width = 41
        Height = 64
        Align = alLeft
        BevelOuter = bvNone
        TabOrder = 1
*)
      FPnNumBx[Box] := TPanel.Create(Form);
      FPnNumBx[Box].Parent := FPnArt1N[Box];
      FPnNumBx[Box].Name := 'PnNumBx' + IdTXT;
      FPnNumBx[Box].Width := ObtemWidth(41);
      FPnNumBx[Box].Align := alLeft;
      FPnNumBx[Box].BevelOuter := bvNone;
      FPnNumBx[Box].Caption := '';
(*
        object Label79: TLabel
          Align = alTop
          Alignment = taCenter
          Caption = 'Box'
        end
*)
        FLaNumBx[Box] := TLabel.Create(Form);
        FLaNumBx[Box].Parent := FPnNumBx[Box];
        FLaNumBx[Box].Name := 'LaNumBx' + IdTXT;
        //FLaNumBx[Box].Width := ObtemWidth(41);
        FLaNumBx[Box].Align := alTop;
        FLaNumBx[Box].Font.Height := GetFontHeight(-19(*14*));
        FLaNumBx[Box].Caption := ' Box';
        FLaNumBx[Box].Alignment := taCenter;
(*
        object Panel51: TPanel
          Align = alClient
          BevelOuter = bvNone
          Caption = '1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -37
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 0
        end
      end
*)
        FPnIDBox[Box] := TPanel.Create(Form);
        FPnIDBox[Box].Parent := FPnNumBx[Box];
        FPnIDBox[Box].Name := 'PnIDBox' + IdTXT;
        FPnIDBox[Box].Width := ObtemWidth(41);
        FPnIDBox[Box].Align := alClient;
        FPnIDBox[Box].BevelOuter := bvNone;
        FPnIDBox[Box].Font.Charset := DEFAULT_CHARSET;
        FPnIDBox[Box].Font.Color := clWindowText;
        FPnIDBox[Box].Font.Height := GetFontHeight(-32(*24*));
        FPnIDBox[Box].Font.Name := 'Tahoma';
        FPnIDBox[Box].Font.Style := [fsBold];
        FPnIDBox[Box].ParentFont := False;
        FPnIDBox[Box].Caption := IdTXT;
(*
      object Panel64: TPanel
        Width = 122
        Align = alRight
        BevelOuter = bvNone
*)
      FPnSubXx[Box] := TPanel.Create(Form);
      FPnSubXx[Box].Parent := FPnArt1N[Box];
      FPnSubXx[Box].Name := 'PnSubXx' + IdTXT;
      FPnSubXx[Box].Width := ObtemWidth(122);
      FPnSubXx[Box].Align := alRight;
      FPnSubXx[Box].BevelOuter := bvNone;
      FPnSubXx[Box].Caption := '';
(*
        object CkSubClass1: TCheckBox
          Left = 4
          Top = 0
          Width = 113
          Height = 25
          Caption = 'Sub classe'
          TabOrder = 0
        end
      end
*)
        FCkSubClass[Box] := TCheckBox.Create(Form);
        FCkSubClass[Box].Parent := FPnSubXx[Box];
        FCkSubClass[Box].Name := 'CkSubClass' + IdTXT;
        FCkSubClass[Box].Width := ObtemWidth(118);
        FCkSubClass[Box].Font.Height := GetFontHeight(-19);
        FCkSubClass[Box].Top := 0;
        //FCkSubClass[Box].Left := ObtemWidth(4);
        //if FCkSubClass[Box].Left > 4 then
          FCkSubClass[Box].Left := 4;
        FCkSubClass[Box].Align := alRight;
        FCkSubClass[Box].Caption := 'Sub classe';
(*
      object Panel8: TPanel
        Left = 41
        Top = 0
        Width = 469
        Height = 64
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
*)
      FPnArt2N[Box] := TPanel.Create(Form);
      FPnArt2N[Box].Parent := FPnArt1N[Box];
      FPnArt2N[Box].Name := 'PnArt2N' + IdTXT;
      FPnArt2N[Box].Width := ObtemWidth(122);
      FPnArt2N[Box].Align := alClient;
      FPnArt2N[Box].BevelOuter := bvNone;
      FPnArt2N[Box].Caption := '';
(*
        object DBEdit1: TDBEdit
          Align = alTop
          DataField = 'NO_PRD_TAM_COR'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -19
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
*)
        FDBEdArtNome[Box] := TDBEdit.Create(Form);
        FDBEdArtNome[Box].Parent := FPnArt2N[Box];
        FDBEdArtNome[Box].Name := 'EdDBArtNome' + IdTXT;
        FDBEdArtNome[Box].Align := alTop;
        FDBEdArtNome[Box].Font.Charset := DEFAULT_CHARSET;
        FDBEdArtNome[Box].Font.Color := clWindowText;
        FDBEdArtNome[Box].Font.Height := GetFontHeight(-19(*14**));
        FDBEdArtNome[Box].Font.Name := 'Tahoma';
        FDBEdArtNome[Box].Font.Style := [fsBold];
        FDBEdArtNome[Box].ParentFont := False;
        FDBEdArtNome[Box].TabOrder := 0;
        FDBEdArtNome[Box].DataField := 'NO_PRD_TAM_COR';
        FDBEdArtNome[Box].Text := '';
(*
        object Panel11: TPanel
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
*)
      FPnArt3N[Box] := TPanel.Create(Form);
      FPnArt3N[Box].Parent := FPnArt2N[Box];
      FPnArt3N[Box].Name := 'PnArt3N' + IdTXT;
      FPnArt3N[Box].Align := alClient;
      FPnArt3N[Box].BevelOuter := bvNone;
      FPnArt3N[Box].Caption := '';
(*
          object DBEdit8: TDBEdit
            Left = 0
            Top = 0
            Width = 100
            Height = 31
            Align = alLeft
            DataField = 'NO_STATUS'
            TabOrder = 1
            ExplicitHeight = 21
          end
*)
          FDBEdArtNoCli[Box] := TDBEdit.Create(Form);
          FDBEdArtNoCli[Box].Parent := FPnArt3N[Box];
          FDBEdArtNoCli[Box].Name := 'EdDBArtNoCli' + IdTXT;
          FDBEdArtNoCli[Box].Align := alLeft;
          FDBEdArtNoCli[Box].Width := ObtemWidth(100);
          FDBEdArtNoCli[Box].Font.Charset := DEFAULT_CHARSET;
          FDBEdArtNoCli[Box].Font.Color := clWindowText;
          FDBEdArtNoCli[Box].Font.Height := GetFontHeight(-19(*14**));
          FDBEdArtNoCli[Box].Font.Name := 'Tahoma';
          FDBEdArtNoCli[Box].Font.Style := [fsBold];
          FDBEdArtNoCli[Box].ParentFont := False;
          FDBEdArtNoCli[Box].TabOrder := 0;
          FDBEdArtNoCli[Box].DataField := 'NO_STATUS';
          FDBEdArtNoCli[Box].Text := '';
(*
          object DBEdit3: TDBEdit
            Left = 100
            Top = 0
            Width = 369
            Height = 31
            Align = alClient
            DataField = 'NO_CLISTAT'
            TabOrder = 0
            ExplicitHeight = 21
          end
        end
      end
    end
*)
          FDBEdArtNoSta[Box] := TDBEdit.Create(Form);
          FDBEdArtNoSta[Box].Parent := FPnArt3N[Box];
          FDBEdArtNoSta[Box].Name := 'EdDBArtNoSta' + IdTXT;
          FDBEdArtNoSta[Box].Align := alClient;
          FDBEdArtNoSta[Box].Font.Charset := DEFAULT_CHARSET;
          FDBEdArtNoSta[Box].Font.Color := clWindowText;
          FDBEdArtNoSta[Box].Font.Height := GetFontHeight(-19(*14**));
          FDBEdArtNoSta[Box].Font.Name := 'Tahoma';
          FDBEdArtNoSta[Box].Font.Style := [fsBold];
          FDBEdArtNoSta[Box].ParentFont := False;
          FDBEdArtNoSta[Box].TabOrder := 0;
          FDBEdArtNoSta[Box].DataField := 'NO_CLISTAT';
          FDBEdArtNoSta[Box].Text := '';
(*
    object Panel5: TPanel
      Top = 64
      Width = 298
      Height = 252
      Align = alLeft
      BevelOuter = bvNone
      TabOrder = 0
*)
    FPnBxTot[Box] := TPanel.Create(Form);
    FPnBxTot[Box].Parent := FPnBox[Box];
    FPnBxTot[Box].Name := 'PnBxTot' + IdTXT;
    FPnBxTot[Box].Align := alClient;
    FPnBxTot[Box].BevelOuter := bvNone;
    FPnBxTot[Box].Align := alLeft;
    FPnBxTot[Box].Width := ObtemWidth(298);
    FPnBxTot[Box].TabOrder := 0;
    FPnBxTot[Box].Caption := '';
(*
      object GroupBox1: TGroupBox
        Height = 80
        Align = alTop
        Caption = ' Do artigo na classe: '
        TabOrder = 0
*)
      FGBIMEI[Box] := TGroupBox.Create(Form);
      FGBIMEI[Box].Parent := FPnBxTot[Box];
      FGBIMEI[Box].Name := 'GBIMEI' + IdTXT;
      FGBIMEI[Box].Align := alTop;
      //FGBIMEI[Box].Height := ObtemHeigth(80) + 12;
      FGBIMEI[Box].Height := ObtemHeigth(56) + 16;
      FGBIMEI[Box].Caption := ' Do artigo na classe: ';
      FGBIMEI[Box].TabOrder := 0;
(*
        object Panel6: TPanel
          Height = 63
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
*)
        FPnIMEIImei[Box] := TPanel.Create(Form);
        FPnIMEIImei[Box].Parent := FGBIMEI[Box];
        FPnIMEIImei[Box].Name := 'PnIMEIImei' + IdTXT;
        FPnIMEIImei[Box].Align := alTop;
        FPnIMEIImei[Box].BevelOuter := bvNone;
        //FPnIMEIImei[Box].Height := ObtemHeigth(36);
        FPnIMEIImei[Box].Height := ObtemHeigth(28);
        FPnIMEIImei[Box].TabOrder := 0;
        FPnIMEIImei[Box].Caption := '';
(*
          object Label54: TLabel
            Left = 6
            Top = 32
            Width = 30
            Height = 13
            Caption = 'IME-I:'
            FocusControl = DBEdControle01
          end
*)
          FLaIMEIImei[Box] := TLabel.Create(Form);
          FLaIMEIImei[Box].Parent := FPnIMEIImei[Box];
          FLaIMEIImei[Box].Name := 'LaIMEIImei' + IdTXT;
          //FLaIMEIImei[Box].Width := ObtemWidth(41);
          FLaIMEIImei[Box].Align := alLeft;
          //FLaIMEIImei[Box].Top := ObtemHeigth(4);
          //FLaIMEIImei[Box].Font.Height := GetFontHeight(-11(*8*));
          FLaIMEIImei[Box].Font.Height := GetFontHeight(-19(*14*));
          FLaIMEIImei[Box].Caption := ' IME-I:';
          //FLaIMEIImei[Box].Alignment := taCenter;
(*
          object DBEdIMEI01: TDBEdit
            Left = 112
            Top = 28
            Width = 180
            Height = 21
            DataField = 'VMI_Dest'
            TabOrder = 1
          end
        end
      end
*)
          FDBEdIMEIImei[Box] := TDBEdit.Create(Form);
          FDBEdIMEIImei[Box].Parent := FPnIMEIImei[Box];
          FDBEdIMEIImei[Box].Name := 'DBEdIMEIImei' + IdTXT;
          FDBEdIMEIImei[Box].Align := alRight;
          FDBEdIMEIImei[Box].Font.Charset := DEFAULT_CHARSET;
          FDBEdIMEIImei[Box].Font.Color := clWindowText;
          FDBEdIMEIImei[Box].Font.Height := GetFontHeight(-21(*16*));
          FDBEdIMEIImei[Box].Font.Name := 'Tahoma';
          FDBEdIMEIImei[Box].Font.Style := [fsBold];
          FDBEdIMEIImei[Box].ParentFont := False;
          FDBEdIMEIImei[Box].TabOrder := 0;
          FDBEdIMEIImei[Box].DataField := 'VMI_Dest';
          FDBEdIMEIImei[Box].Width := ObtemWidth(120);
          FDBEdIMEIImei[Box].Text := '';
(*
        object Panel?: TPanel
          Height = 63
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
*)
        FPnIMEICtrl[Box] := TPanel.Create(Form);
        FPnIMEICtrl[Box].Parent := FGBIMEI[Box];
        FPnIMEICtrl[Box].Name := 'PnIMEICtrl' + IdTXT;
        FPnIMEICtrl[Box].Align := alTop;
        FPnIMEICtrl[Box].BevelOuter := bvNone;
        //FPnIMEICtrl[Box].Height := ObtemHeigth(36);
        FPnIMEICtrl[Box].Height := ObtemHeigth(28);
        FPnIMEICtrl[Box].TabOrder := 0;
        FPnIMEICtrl[Box].Caption := '';
    ///
(*
          object Label4: TLabel
            Height = 13
            Caption = 'Controle:'
            FocusControl = DBEdControle01
          end
*)
          FLaIMEICtrl[Box] := TLabel.Create(Form);
          FLaIMEICtrl[Box].Parent := FPnIMEICtrl[Box];
          FLaIMEICtrl[Box].Name := 'LaIMEICtrl' + IdTXT;
          //FLaIMEICtrl[Box].Width := ObtemWidth(41);
          FLaIMEICtrl[Box].Align := alLeft;
          //FLaIMEICtrl[Box].Top := ObtemHeigth(4);
          //FLaIMEICtrl[Box].Font.Height := GetFontHeight(-11(*8*));
          FLaIMEICtrl[Box].Font.Height := GetFontHeight(-21(*16*));
          FLaIMEICtrl[Box].Caption := ' Controle:';
          //FLaIMEICtrl[Box].Alignment := taCenter;

(*
          object DBEdControle01: TDBEdit
            Height = 21
            DataField = 'Controle'
            TabOrder = 0
          end
*)
          FDBEdIMEICtrl[Box] := TDBEdit.Create(Form);
          FDBEdIMEICtrl[Box].Parent := FPnIMEICtrl[Box];
          FDBEdIMEICtrl[Box].Name := 'DBEdIMEICtrl' + IdTXT;
          FDBEdIMEICtrl[Box].Align := alRight;
          FDBEdIMEICtrl[Box].Font.Charset := DEFAULT_CHARSET;
          FDBEdIMEICtrl[Box].Font.Color := clWindowText;
          FDBEdIMEICtrl[Box].Font.Height := GetFontHeight(-19(*14*));
          FDBEdIMEICtrl[Box].Font.Name := 'Tahoma';
          FDBEdIMEICtrl[Box].Font.Style := [fsBold];
          FDBEdIMEICtrl[Box].ParentFont := False;
          FDBEdIMEICtrl[Box].TabOrder := 0;
          FDBEdIMEICtrl[Box].DataField := 'Controle';
          FDBEdIMEICtrl[Box].Width := ObtemWidth(120);
          FDBEdIMEICtrl[Box].Text := '';
          //
          //
          //
          //
(*
      object GroupBox2: TGroupBox
        Top = 80
        Height = 172
        Align = alClient
        Caption = ' Do Pallet: '
        TabOrder = 1
*)
      FGBPallet[Box] := TGroupBox.Create(Form);
      FGBPallet[Box].Parent := FPnBxTot[Box];
      FGBPallet[Box].Name := 'GBPallet' + IdTXT;
      FGBPallet[Box].Align := alClient;
      //FGBPallet[Box].Height := ObtemHeigth(172);
      FGBPallet[Box].Caption := ' Do Pallet: ';
      FGBPallet[Box].TabOrder := 1;
(*
        object Panel16: TPanel
          Left = 2
          Top = 15
          Width = 294
          Height = 155
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
*)
        FPnPalletMedia[Box] := TPanel.Create(Form);
        FPnPalletMedia[Box].Parent := FGBPallet[Box];
        FPnPalletMedia[Box].Name := 'PnPalletMedia' + IdTXT;
        FPnPalletMedia[Box].Align := alTop;
        FPnPalletMedia[Box].BevelOuter := bvNone;
        //FPnPalletMedia[Box].Height := ObtemHeigth(36);
        FPnPalletMedia[Box].Height := ObtemHeigth(28);
        FPnPalletMedia[Box].TabOrder := 0;
        FPnPalletMedia[Box].Caption := '';
(*
          object Label73: TLabel
            Height = 13
            Caption = 'm'#178' / Pe'#231'a:'
          end
*)
          FLaPalletMedia[Box] := TLabel.Create(Form);
          FLaPalletMedia[Box].Parent := FPnPalletMedia[Box];
          FLaPalletMedia[Box].Name := 'LaPalletMedia' + IdTXT;
          //FLaPalletMedia[Box].Width := ObtemWidth(41);
          FLaPalletMedia[Box].Align := alLeft;
          //FLaPalletMedia[Box].Top := ObtemHeigth(4);
          //FLaPalletMedia[Box].Font.Height := GetFontHeight(-11(*8*));
          FLaPalletMedia[Box].Font.Height := GetFontHeight(-21(*16*));
          FLaPalletMedia[Box].Caption := ' m� / Pe�a:';
          //FLaPalletMedia[Box].Alignment := taCenter;
(*
          object EdMedia01: TdmkEdit
            Width = 180
            Height = 27
            Alignment = taRightJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -16
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
            ReadOnly = True
            TabOrder = 2
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
*)
          FEdPalletMedia[Box] := TdmkEdit.Create(Form);
          FEdPalletMedia[Box].Parent := FPnPalletMedia[Box];
          FEdPalletMedia[Box].Name := 'DBEdPalletMedia' + IdTXT;
          FEdPalletMedia[Box].Align := alRight;
          FEdPalletMedia[Box].Alignment := taRightJustify;
          FEdPalletMedia[Box].Font.Charset := DEFAULT_CHARSET;
          FEdPalletMedia[Box].Font.Color := clWindowText;
          FEdPalletMedia[Box].Font.Height := GetFontHeight(-19); //-19(*14*));
          FEdPalletMedia[Box].Font.Name := 'Tahoma';
          FEdPalletMedia[Box].Font.Style := [fsBold];
          FEdPalletMedia[Box].ParentFont := False;
          FEdPalletMedia[Box].ReadOnly := True;
          FEdPalletMedia[Box].TabOrder := 0;
          FEdPalletMedia[Box].Text := '0.00';
          FEdPalletMedia[Box].FormatType := dmktfDouble;
          FEdPalletMedia[Box].MskType := fmtNone;
          FEdPalletMedia[Box].DecimalSize := 2;
          FEdPalletMedia[Box].LeftZeros := 0;
          FEdPalletMedia[Box].NoEnterToTab := False;
          FEdPalletMedia[Box].NoForceUppercase := False;
          FEdPalletMedia[Box].ForceNextYear := False;
          FEdPalletMedia[Box].DataFormat := dmkdfShort;
          FEdPalletMedia[Box].HoraFormat := dmkhfShort;
          FEdPalletMedia[Box].Texto := '0,00';
          FEdPalletMedia[Box].UpdType := utYes;
          FEdPalletMedia[Box].Obrigatorio := False;
          FEdPalletMedia[Box].PermiteNulo := False;
          FEdPalletMedia[Box].ValueVariant := 0;
          FEdPalletMedia[Box].ValWarn := False;
          FEdPalletMedia[Box].Width := ObtemWidth(120);
(*
        object Panel16: TPanel
          Left = 2
          Top = 15
          Width = 294
          Height = 155
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
*)
        FPnPalletArea[Box] := TPanel.Create(Form);
        FPnPalletArea[Box].Parent := FGBPallet[Box];
        FPnPalletArea[Box].Name := 'PnPalletArea' + IdTXT;
        FPnPalletArea[Box].Align := alTop;
        FPnPalletArea[Box].BevelOuter := bvNone;
        //FPnPalletArea[Box].Height := ObtemHeigth(36);
        FPnPalletArea[Box].Height := ObtemHeigth(28);
        FPnPalletArea[Box].TabOrder := 0;
        FPnPalletArea[Box].Caption := '';
(*
          object Label53: TLabel
            Height = 13
            Caption = #193'rea:'
          end
*)
          FLaPalletArea[Box] := TLabel.Create(Form);
          FLaPalletArea[Box].Parent := FPnPalletArea[Box];
          FLaPalletArea[Box].Name := 'LaPalletArea' + IdTXT;
          //FLaPalletArea[Box].Width := ObtemWidth(41);
          FLaPalletArea[Box].Align := alLeft;
          //FLaPalletArea[Box].Top := ObtemHeigth(4);
          //FLaPalletArea[Box].Font.Height := GetFontHeight(-11(*8*));
          FLaPalletArea[Box].Font.Height := GetFontHeight(-21(*16*));
          FLaPalletArea[Box].Caption := ' �rea:';
          //FLaPalletArea[Box].Alignment := taCenter;
(*
          object EdArea01: TdmkEdit
            Height = 27
            Alignment = taRightJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlue
            Font.Height = -16
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
            ReadOnly = True
            TabOrder = 1
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
*)
          FEdPalletArea[Box] := TdmkEdit.Create(Form);
          FEdPalletArea[Box].Parent := FPnPalletArea[Box];
          FEdPalletArea[Box].Name := 'DBEdPalletArea' + IdTXT;
          FEdPalletArea[Box].Align := alRight;
          FEdPalletArea[Box].Alignment := taRightJustify;
          FEdPalletArea[Box].Font.Charset := DEFAULT_CHARSET;
          FEdPalletArea[Box].Font.Color := clWindowText;
          FEdPalletArea[Box].Font.Height := GetFontHeight(-19(*14*));
          FEdPalletArea[Box].Font.Name := 'Tahoma';
          FEdPalletArea[Box].Font.Style := [fsBold];
          FEdPalletArea[Box].ParentFont := False;
          FEdPalletArea[Box].ReadOnly := True;
          FEdPalletArea[Box].TabOrder := 0;
          FEdPalletArea[Box].Text := '0.00';
          FEdPalletArea[Box].FormatType := dmktfDouble;
          FEdPalletArea[Box].MskType := fmtNone;
          FEdPalletArea[Box].DecimalSize := 2;
          FEdPalletArea[Box].LeftZeros := 0;
          FEdPalletArea[Box].NoEnterToTab := False;
          FEdPalletArea[Box].NoForceUppercase := False;
          FEdPalletArea[Box].ForceNextYear := False;
          FEdPalletArea[Box].DataFormat := dmkdfShort;
          FEdPalletArea[Box].HoraFormat := dmkhfShort;
          FEdPalletArea[Box].Texto := '0,00';
          FEdPalletArea[Box].UpdType := utYes;
          FEdPalletArea[Box].Obrigatorio := False;
          FEdPalletArea[Box].PermiteNulo := False;
          FEdPalletArea[Box].ValueVariant := 0;
          FEdPalletArea[Box].ValWarn := False;
          FEdPalletArea[Box].Width := ObtemWidth(120);
(*
        object Panel16: TPanel
          Left = 2
          Top = 15
          Width = 294
          Height = 155
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
*)
        FPnPalletPecas[Box] := TPanel.Create(Form);
        FPnPalletPecas[Box].Parent := FGBPallet[Box];
        FPnPalletPecas[Box].Name := 'PnPalletPecas' + IdTXT;
        FPnPalletPecas[Box].Align := alTop;
        FPnPalletPecas[Box].BevelOuter := bvNone;
        //FPnPalletPecas[Box].Height := ObtemHeigth(36);
        FPnPalletPecas[Box].Height := ObtemHeigth(28);
        FPnPalletPecas[Box].TabOrder := 0;
        FPnPalletPecas[Box].Caption := '';
(*
          object Label52: TLabel
            Height = 13
            Caption = 'Pe'#231'as:'
          end
*)
          FLaPalletPecas[Box] := TLabel.Create(Form);
          FLaPalletPecas[Box].Parent := FPnPalletPecas[Box];
          FLaPalletPecas[Box].Name := 'LaPalletPecas' + IdTXT;
          //FLaPalletPecas[Box].Width := ObtemWidth(41);
          FLaPalletPecas[Box].Align := alLeft;
          //FLaPalletPecas[Box].Top := ObtemHeigth(4);
          //FLaPalletPecas[Box].Font.Height := GetFontHeight(-11(*8*));
          FLaPalletPecas[Box].Font.Height := GetFontHeight(-21(*16*));
          FLaPalletPecas[Box].Caption := ' Pe�as:';
          //FLaPalletPecas[Box].Alignment := taCenter;
(*
          object EdPecas01: TdmkEdit
            Height = 27
            Alignment = taRightJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlue
            Font.Height = -16
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
            ReadOnly = True
            TabOrder = 0
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 3
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,000'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
*)
          FEdPalletPecas[Box] := TdmkEdit.Create(Form);
          FEdPalletPecas[Box].Parent := FPnPalletPecas[Box];
          FEdPalletPecas[Box].Name := 'DBEdPalletPecas' + IdTXT;
          FEdPalletPecas[Box].Align := alRight;
          FEdPalletPecas[Box].Alignment := taRightJustify;
          FEdPalletPecas[Box].Font.Charset := DEFAULT_CHARSET;
          FEdPalletPecas[Box].Font.Color := clWindowText;
          FEdPalletPecas[Box].Font.Height := GetFontHeight(-19); //-19(*14*));
          FEdPalletPecas[Box].Font.Name := 'Tahoma';
          FEdPalletPecas[Box].Font.Style := [fsBold];
          FEdPalletPecas[Box].ParentFont := False;
          FEdPalletPecas[Box].ReadOnly := True;
          FEdPalletPecas[Box].TabOrder := 0;
          FEdPalletPecas[Box].Text := '0.00';
          FEdPalletPecas[Box].FormatType := dmktfDouble;
          FEdPalletPecas[Box].MskType := fmtNone;
          FEdPalletPecas[Box].DecimalSize := 1;
          FEdPalletPecas[Box].LeftZeros := 0;
          FEdPalletPecas[Box].NoEnterToTab := False;
          FEdPalletPecas[Box].NoForceUppercase := False;
          FEdPalletPecas[Box].ForceNextYear := False;
          FEdPalletPecas[Box].DataFormat := dmkdfShort;
          FEdPalletPecas[Box].HoraFormat := dmkhfShort;
          FEdPalletPecas[Box].Texto := '0,00';
          FEdPalletPecas[Box].UpdType := utYes;
          FEdPalletPecas[Box].Obrigatorio := False;
          FEdPalletPecas[Box].PermiteNulo := False;
          FEdPalletPecas[Box].ValueVariant := 0;
          FEdPalletPecas[Box].ValWarn := False;
          FEdPalletPecas[Box].Width := ObtemWidth(120);
(*
        object Panel16: TPanel
          Left = 2
          Top = 15
          Width = 294
          Height = 155
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
*)
        FPnPalletID[Box] := TPanel.Create(Form);
        FPnPalletID[Box].Parent := FGBPallet[Box];
        FPnPalletID[Box].Name := 'PnPalletID' + IdTXT;
        FPnPalletID[Box].Align := alTop;
        FPnPalletID[Box].BevelOuter := bvNone;
        //FPnPalletID[Box].Height := ObtemHeigth(36);
        FPnPalletID[Box].Height := ObtemHeigth(28);
        FPnPalletID[Box].TabOrder := 0;
        FPnPalletID[Box].Caption := '';
(*
          object Label55: TLabel
            Left = 4
            Top = 8
            Width = 15
            Height = 13
            Caption = 'ID:'
          end
*)
          FLaPalletID[Box] := TLabel.Create(Form);
          FLaPalletID[Box].Parent := FPnPalletID[Box];
          FLaPalletID[Box].Name := 'LaPalletID' + IdTXT;
          //FLaPalletID[Box].Width := ObtemWidth(41);
          FLaPalletID[Box].Align := alLeft;
          //FLaPalletID[Box].Top := ObtemHeigth(4);
          //FLaPalletID[Box].Font.Height := GetFontHeight(-11(*8*));
          FLaPalletID[Box].Font.Height := GetFontHeight(-21(*16*));
          FLaPalletID[Box].Caption := ' ID:';
          //FLaPalletID[Box].Alignment := taCenter;
(*
          object DBEdPallet01: TDBEdit
            Height = 27
            DataField = 'Codigo'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clPurple
            Font.Height = -16
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 3
          end
*)
          FDBEdPalletID[Box] := TDBEdit.Create(Form);
          FDBEdPalletID[Box].Parent := FPnPalletID[Box];
          FDBEdPalletID[Box].Name := 'EdDBPalletID' + IdTXT;
          FDBEdPalletID[Box].Align := alRight;
          FDBEdPalletID[Box].Font.Charset := DEFAULT_CHARSET;
          FDBEdPalletID[Box].Font.Color := clPurple;
          FDBEdPalletID[Box].Font.Height := GetFontHeight(-19(*14*));
          FDBEdPalletID[Box].Font.Name := 'Tahoma';
          FDBEdPalletID[Box].Font.Style := [fsBold];
          FDBEdPalletID[Box].ParentFont := False;
          FDBEdPalletID[Box].TabOrder := 0;
          FDBEdPalletID[Box].DataField := 'Codigo';
          FDBEdPalletID[Box].Width := ObtemWidth(120);
          FDBEdPalletID[Box].Text := '';
(*
        end
      end
    end
*)
(*
    object PnIntMei01: TPanel
      Height = 28
      Align = alBottom
      Caption = 'ATEN'#199#195'O!!! Couro deve ser cortado!!! '
      UseDockManager = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clRed
      Font.Height = -19
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 2
      Visible = False
    end
*)
    // 2016-02-25
    Painel := TPanel.Create(Form);
    Painel.Parent := FPnBox[Box];
    Painel.Name := 'PnAAAA' + IdTXT;
    Painel.Align := alClient;
    Painel.BevelOuter := bvNone;
    //Painel.Height := ObtemHeigth(36);
    Painel.TabOrder := 0;
    Painel.Caption := '';
    // Fim 2016-02-25

    FPnIntMei[Box] := TPanel.Create(Form);
    //FPnIntMei[Box].Parent := FPnBox[Box];
    FPnIntMei[Box].Parent := Painel;
    FPnIntMei[Box].Name := 'PnIntMei' + IdTXT;
    FPnIntMei[Box].Align := alBottom;
    FPnIntMei[Box].BevelOuter := bvNone;
    //FPnIntMei[Box].Font.Height := GetFontHeight(-19(*14*));
    FPnIntMei[Box].Font.Height := GetFontHeight(-15(*11*));
    //FPnIntMei[Box].Caption := ' ATEN��O!!! Couro deve ser cortado!!! ';
    FPnIntMei[Box].Caption := ' ATEN��O! CORTAR! ';
    FPnIntMei[Box].UseDockManager := False;
    FPnIntMei[Box].Font.Charset := DEFAULT_CHARSET;
    FPnIntMei[Box].Font.Color := clRed;
    //FPnIntMei[Box].Height := ObtemHeigth(28);
    FPnIntMei[Box].Height := ObtemHeigth(16);
    FPnIntMei[Box].Font.Name := 'Tahoma';
    FPnIntMei[Box].Font.Style := [fsBold];
    FPnIntMei[Box].ParentFont := False;
    FPnIntMei[Box].TabOrder := 2;
    FPnIntMei[Box].Visible := False;
(*
    object SGItens01: TStringGrid
      Align = alClient
      TabOrder = 3
    end
  end
end
*)
    FSGItens[Box] := TStringGrid.Create(Form);
    //FSGItens[Box].Parent := FPnBox[Box];
    FSGItens[Box].Parent := Painel;
    FSGItens[Box].Name := 'SGItens' + IdTXT;
    FSGItens[Box].Align := alClient;
    FSGItens[Box].DefaultRowHeight := ObtemWidth(24);
    FSGItens[Box].Font.Height := GetFontHeight(-19(*14*));
    FSGItens[Box].Font.Charset := DEFAULT_CHARSET;
    //FSGItens[Box].Font.Color := clRed;
    FSGItens[Box].Height := ObtemHeigth(28);
    //FSGItens[Box].Height := ObtemHeigth(24);
    FSGItens[Box].Font.Name := 'Tahoma';
    //FSGItens[Box].Font.Style := [fsBold];
    FSGItens[Box].ParentFont := False;
    FSGItens[Box].TabOrder := 3;
    FSGItens[Box].ColCount := 2;
    FSGItens[Box].FixedCols := 1;
    FSGItens[Box].Cells[0, 0]  := 'Item';
    FSGItens[Box].Cells[1, 0]  := '�rea m�';
    //
    FSGItens[Box].ColWidths[0] := ObtemWidth(46);
    FSGItens[Box].ColWidths[1] := ObtemWidth(78);
    //
    FSGItens[Box].PopupMenu := FPMItens[Box];
end;
}


{
object QrCol: TMySQLQuery
  SQL.Strings = (
    'SELECT DISTINCT ymd.CodGrade, ymd.CodTam, '
    'LPAD(CodTam, 30, " ")  OrdTam'
    'FROM ovcynsmeddim ymd '
    'LEFT JOIN ovdgradetam ogt ON  '
    '  ogt.Codigo=ymd.CodGrade '
    '  AND '
    '  ogt.Nome=ymd.CodTam '
    'WHERE ymd.Codigo=:P0'
    'ORDER BY ogt.Ordem, OrdTam, ogt.Nome ')
  Left = 500
  Top = 256
  ParamData = <
    item
      DataType = ftUnknown
      Name = 'P0'
      ParamType = ptUnknown
    end>
  object QrColCodGrade: TIntegerField
    FieldName = 'CodGrade'
    Required = True
  end
  object QrColCodTam: TWideStringField
    FieldName = 'CodTam'
    Size = 30
  end
  object QrColOrdTam: TWideStringField
    FieldName = 'OrdTam'
    Size = 30
  end
end
}



{
object QrDim: TMySQLQuery
  Database = Dmod.ZZDB
  SQL.Strings = (
    'SELECT ymd.*  '
    'FROM ovcynsmeddim ymd  '
    'WHERE ymd.Controle=:P0')
  Left = 340
  Top = 277
  ParamData = <
    item
      DataType = ftUnknown
      Name = 'P0'
      ParamType = ptUnknown
    end>
  object QrDimCodigo: TIntegerField
    FieldName = 'Codigo'
    Required = True
  end
  object QrDimControle: TIntegerField
    FieldName = 'Controle'
    Required = True
  end
  object QrDimConta: TIntegerField
    FieldName = 'Conta'
    Required = True
  end
  object QrDimCodGrade: TIntegerField
    FieldName = 'CodGrade'
    Required = True
  end
  object QrDimCodTam: TWideStringField
    FieldName = 'CodTam'
    Size = 30
  end
  object QrDimMedidCer: TFloatField
    FieldName = 'MedidCer'
    Required = True
  end
  object QrDimMedidMin: TFloatField
    FieldName = 'MedidMin'
    Required = True
  end
  object QrDimMedidMax: TFloatField
    FieldName = 'MedidMax'
    Required = True
  end
  object QrDimUlWayInz: TSmallintField
    FieldName = 'UlWayInz'
    Required = True
  end
  object QrDimLk: TIntegerField
    FieldName = 'Lk'
    Required = True
  end
  object QrDimDataCad: TDateField
    FieldName = 'DataCad'
  end
  object QrDimDataAlt: TDateField
    FieldName = 'DataAlt'
  end
  object QrDimUserCad: TIntegerField
    FieldName = 'UserCad'
    Required = True
  end
  object QrDimUserAlt: TIntegerField
    FieldName = 'UserAlt'
    Required = True
  end
  object QrDimAlterWeb: TSmallintField
    FieldName = 'AlterWeb'
    Required = True
  end
  object QrDimAWServerID: TIntegerField
    FieldName = 'AWServerID'
    Required = True
  end
  object QrDimAWStatSinc: TSmallintField
    FieldName = 'AWStatSinc'
    Required = True
  end
  object QrDimAtivo: TSmallintField
    FieldName = 'Ativo'
    Required = True
  end
end
}

end.
