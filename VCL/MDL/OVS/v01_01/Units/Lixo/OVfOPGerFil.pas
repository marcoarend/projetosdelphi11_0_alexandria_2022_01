unit OVfOPGerFil;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkPermissoes,
  mySQLDbTables, dmkDBGridZTO, Variants;

type
  TFmOVfOPGerFil = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtConfigura: TBitBtn;
    LaTitulo1C: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    PnNrReduzidoOP: TPanel;
    DBGNrReduzidoOP: TdmkDBGridZTO;
    Panel12: TPanel;
    Label16: TLabel;
    SpeedButton8: TSpeedButton;
    dmkEdit3: TdmkEdit;
    QrOVfOrdemProducao: TMySQLQuery;
    QrOVfOrdemProducaoNrReduzidoOP: TIntegerField;
    QrOVfOrdemProducaoEmpresa: TIntegerField;
    QrOVfOrdemProducaoCiclo: TIntegerField;
    QrOVfOrdemProducaoNrOP: TIntegerField;
    QrOVfOrdemProducaoLocal: TIntegerField;
    QrOVfOrdemProducaoProduto: TIntegerField;
    QrOVfOrdemProducaoSeqGrupo: TIntegerField;
    QrOVfOrdemProducaoTipoOP: TIntegerField;
    QrOVfOrdemProducaoPrioridade: TIntegerField;
    QrOVfOrdemProducaoNrSituacaoOP: TIntegerField;
    QrOVfOrdemProducaoDtInclusao: TDateField;
    QrOVfOrdemProducaoDtPrevisao: TDateField;
    QrOVfOrdemProducaoTipoLocalizacao: TIntegerField;
    QrOVfOrdemProducaoDtEntrada: TDateField;
    QrOVfOrdemProducaoQtReal: TFloatField;
    QrOVfOrdemProducaoQtLocal: TFloatField;
    QrOVfOrdemProducaoCodCategoria: TIntegerField;
    QrOVfOrdemProducaoNrLote: TIntegerField;
    QrOVfOrdemProducaoNrTipoProducaoOP: TWideStringField;
    QrOVfOrdemProducaoDtPrevRet: TDateField;
    QrOVfOrdemProducaoCodPessoa: TIntegerField;
    QrOVfOrdemProducaoReInsrt: TIntegerField;
    QrOVfOrdemProducaoDestino: TWideStringField;
    QrOVfOrdemProducaoLk: TIntegerField;
    QrOVfOrdemProducaoDataCad: TDateField;
    QrOVfOrdemProducaoDataAlt: TDateField;
    QrOVfOrdemProducaoUserCad: TIntegerField;
    QrOVfOrdemProducaoUserAlt: TIntegerField;
    QrOVfOrdemProducaoAlterWeb: TSmallintField;
    QrOVfOrdemProducaoAWServerID: TIntegerField;
    QrOVfOrdemProducaoAWStatSinc: TSmallintField;
    QrOVfOrdemProducaoAtivo: TSmallintField;
    QrOVfOrdemProducaoLastInsrt: TDateTimeField;
    QrOVfOrdemProducaoNO_Local: TWideStringField;
    QrOVfOrdemProducaoDLO_Externo: TWideStringField;
    QrOVfOrdemProducaoNO_CodCategoria: TWideStringField;
    QrOVfOrdemProducaoNO_Referencia: TWideStringField;
    QrOVfOrdemProducaoCodGrade: TIntegerField;
    QrOVfOrdemProducaoCodTam: TWideStringField;
    QrOVfOrdemProducaoNO_Lote: TWideStringField;
    QrOVfOrdemProducaoNO_NrSituacaOP: TWideStringField;
    QrOVfOrdemProducaoNO_TipoLocalizacao: TWideStringField;
    QrOVfOrdemProducaoNO_TipoOP: TWideStringField;
    QrOVfOrdemProducaoNO_NrTipoProducaoOP: TWideStringField;
    DsOVfOrdemProducao: TDataSource;
    DGDados: TdmkDBGridZTO;
    QrOVgIspLasSta: TMySQLQuery;
    DsOVgIspLasSta: TDataSource;
    QrOVgIspLasStaLocal: TIntegerField;
    QrOVgIspLasStaNrOP: TIntegerField;
    QrOVgIspLasStaSeqGrupo: TIntegerField;
    QrOVgIspLasStaNrReduzidoOP: TIntegerField;
    QrOVgIspLasStaDataHora: TDateTimeField;
    QrOVgIspLasStaNO_Local: TWideStringField;
    QrOVgIspLasStaNO_Referencia: TWideStringField;
    Splitter1: TSplitter;
    BitBtn1: TBitBtn;
    BtOVgIspPrfCad: TBitBtn;
    QrOVgIspLasStaReferencia: TWideStringField;
    BtSoCriaPerfil: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure QrNovasReduzOPs_AfterScroll(DataSet: TDataSet);
    procedure QrOVgIspLasStaAfterScroll(DataSet: TDataSet);
    procedure QrOVgIspLasStaAfterOpen(DataSet: TDataSet);
    procedure BitBtn1Click(Sender: TObject);
    procedure BtConfiguraClick(Sender: TObject);
    procedure BtOVgIspPrfCadClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure BtSoCriaPerfilClick(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenQrOVfOrdemProducao();
  public
    { Public declarations }
    FCriaGerenciamentoAuomaticamente: Boolean;
    procedure ReopenNovasReduzOPs();
  end;

  var
  FmOVfOPGerFil: TFmOVfOPGerFil;

implementation

uses UnMyObjects, Module, DmkDAC_PF, UnOVS_PF, UnOVS_Consts, MyDBCheck,
  UnOVS_Jan, UnProjGroup_Jan, UnOVS_LoadCSV, ModuleGeral, UnProjGroup_PF;

{$R *.DFM}

procedure TFmOVfOPGerFil.BitBtn1Click(Sender: TObject);
var
  Motivo: Integer;
  //
  function ObtemMotivo(): Boolean;
  const
    Aviso  = '...';
    Titulo = 'Sele��o de Motido de Desobriga��o';
    Prompt = 'Informe Motivo: [F7 para pesquisar]';
    Campo  = 'Descricao';
  var
    Controle, Codigo: Variant;
    PesqSQL: String;
  begin
    Result := False;
    Motivo := 0;
    PesqSQL := Geral.ATS([
    'SELECT Codigo, Nome ' + Campo,
    'FROM ovgispmotsta ',
    //'WHERE Codigo > 0', Aqui Permite -1  (s� aqui !!!??)
    'ORDER BY ' + Campo,
    '']);
    Codigo :=
      DBCheck.EscolheCodigoUnico(Aviso, Titulo, Prompt, nil, nil, Campo, 0, [
      PesqSQL], Dmod.MyDB, True);
    if Codigo <> Null then
    begin
      Motivo   := Codigo;
      Result   := True;
      //
    end;
  end;
  //
  procedure DesobrigaItemAtual();
  begin
    OVS_PF.AlteraZtatusOVgIspAllSta(CO_OVS_IMPORT_ALHEIO_2048_DESOBRIGADO,
      Motivo, QrOVgIspLasStaLocal.Value, QrOVgIspLasStaNrOP.Value,
      QrOVgIspLasStaSeqGrupo.Value, QrOVgIspLasStaNrReduzidoOP.Value,
      DModG.ObtemAgora());
  end;
var
  I, N: Integer;
  Continua: Boolean;
begin
  Continua := False;
  N := DBGNrReduzidoOP.SelectedRows.Count;
  if QrOVgIspLasSta.RecordCount > 0 then
  begin
    if N > 1 then
      Continua := Geral.MB_Pergunta('Confirma a desobriga��o de inspe��o dos ' +
      Geral.FF0(N) + ' reduzidos de OP selecionados?') = ID_YES
    else
      Continua := Geral.MB_Pergunta(
      'Confirma a desobriga��o de inspe��o do reduzido de OP selecionado?') =
      ID_YES;
  end else
    Geral.MB_Info('Nenhum item foi selecionado!');
  //
  if Continua then
  begin
    if ObtemMotivo() then
    begin
      if not DBCheck.LiberaPelaSenhaBoss() then Exit;
      if N > 0 then
      begin
        for I := 0 to N - 1 do
        begin
          QrOVgIspLasSta.GotoBookmark(DBGNrReduzidoOP.SelectedRows.Items[I]);
          DesobrigaItemAtual();
        end;
      end else
        DesobrigaItemAtual();
      //
      ReopenNovasReduzOPs();
      // erro desenho! (Tokyo?)
      DBGNrReduzidoOP.Invalidate;
    end;
  end;
end;

procedure TFmOVfOPGerFil.BtConfiguraClick(Sender: TObject);
const
  OVcYnsMed = 0;
  OVcYnsChk = 0;
  LimiteMed = 0;
  LimiteChk = 0;
var
  Codigo: Integer;
begin
  Codigo := 0;
  Codigo := OVS_Jan.MostraFormOVgIspGerCad(stIns, Codigo,
    QrOVgIspLasStaLocal.Value, QrOVgIspLasStaNO_Local.Value,
    QrOVgIspLasStaNrOP.Value, QrOVgIspLasStaSeqGrupo.Value,
    QrOVgIspLasStaNO_Referencia.Value, QrOVgIspLasStaNrReduzidoOP.Value,
    OVcYnsMed, OVcYnsChk, (*OVcYnsARQ*)1, LimiteMed, LimiteChk,
    0(*PermiFinHow*), CO_OVS_IMPORT_ALHEIO_5120_APTO_DOWNMOBILE);
  if Codigo > 0 then
  begin
    OVS_Jan.MostraFormOVgIspGerCab(Codigo);
    ReopenNovasReduzOPs()
  end;
end;

procedure TFmOVfOPGerFil.BtOVgIspPrfCadClick(Sender: TObject);
var
  Codigo, SeqGrupo, OVcYnsMed, OVcYnsChk, OVcYnsARQ, LimiteMed, LimiteChk,
  PermiFinHow, Ativo: Integer;
  NO_Artigo, Referencia, Nome: String;
begin
  SeqGrupo   := QrOVgIspLasStaSeqGrupo.Value;
  NO_Artigo  := QrOVgIspLasStaNO_Referencia.Value;
  Referencia := QrOVgIspLasStaReferencia.Value;
  //
  if SeqGrupo <> 0 then
  begin
    Codigo        := 0;
    OVcYnsMed     := 0;
    OVcYnsChk     := 0;
    OVcYnsARQ     := 0;
    LimiteMed     := 0;
    LimiteChk     := 0;
    PermiFinHow   := 0;
    Ativo         := 0;
    Nome          := '';
    //
    if OVS_Jan.MostraFormOVgIspPrfCad(stIns, Codigo, SeqGrupo, NO_Artigo,
    Referencia, OVcYnsMed, OVcYnsChk, OVcYnsARQ, LimiteMed, LimiteChk,
    PermiFinHow, Ativo, Nome)  <> 0 then
    begin
      MyObjects.Informa2(LaAviso1, LaAviso2, True,
        'Configurando itens automaticamente pelo seu perfil');
      if ProjGroup_Jan.MostraFormOVgIspPrfCfg(True(*Automatico*), LaAviso1,
      LaAviso2) then
      begin
        MyObjects.Informa2(LaAviso1, LaAviso2, True,
          'Reabrindo itens aptos ainda n�o configurados');
        ReopenNovasReduzOPs();
      end;
    end;
  end;
  //MyObjects.Informa2(LaAviso1, LaAviso2, False, '');
end;

procedure TFmOVfOPGerFil.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmOVfOPGerFil.BtSoCriaPerfilClick(Sender: TObject);
var
  Codigo, SeqGrupo, OVcYnsMed, OVcYnsChk, OVcYnsARQ, LimiteMed, LimiteChk,
  PermiFinHow, Ativo: Integer;
  NO_Artigo, Referencia, Nome: String;
begin
  SeqGrupo   := QrOVgIspLasStaSeqGrupo.Value;
  NO_Artigo  := QrOVgIspLasStaNO_Referencia.Value;
  Referencia := QrOVgIspLasStaReferencia.Value;
  //
  if SeqGrupo <> 0 then
  begin
    Codigo        := 0;
    OVcYnsMed     := 0;
    OVcYnsChk     := 0;
    OVcYnsARQ     := 0;
    LimiteMed     := 0;
    LimiteChk     := 0;
    PermiFinHow   := 0;
    Ativo         := 0;
    Nome          := '';
    //
    if OVS_Jan.MostraFormOVgIspPrfCad(stIns, Codigo, SeqGrupo, NO_Artigo,
    Referencia, OVcYnsMed, OVcYnsChk, OVcYnsARQ, LimiteMed, LimiteChk,
    PermiFinHow, Ativo, Nome)  <> 0 then
    begin
     //if ProjGroup_Jan.MostraFormOVgIspPrfCfg(True(*Automatico*)) then
       //ReopenNovasReduzOPs();
    end;
  end;
end;

procedure TFmOVfOPGerFil.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmOVfOPGerFil.FormCreate(Sender: TObject);
begin
  FCriaGerenciamentoAuomaticamente := False;
  ImgTipo.SQLType := stLok;
end;

procedure TFmOVfOPGerFil.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmOVfOPGerFil.FormShow(Sender: TObject);
begin
  // Configura��o autom�tica de Inspe��es via OVgIspPrfCab
  // desmarcar aqui!
  //ProjGroup_Jan.MostraFormOVgIspPrfCfg(True(*Automatico*));
 {
  if not FCriaGerenciamentoAuomaticamente then Exit;
  Screen.Cursor := crHourGlass;
  try
    Screen.Cursor := crHourGlass;
(*
    QrOVgIspLasSta.DisableControls;
    QrOVgIspLasSta.First;
    while not QrOVgIspLasSta.Eof do
    begin

      //
      QrOVgIspLasSta.Next;
    end;
*)
/  'SELECT DiSTINCT ils.Local, ils.NrOP, ils.SeqGrupo,  ',
  'ils.NrReduzidoOP ',
  'FROM ovgisplassta ils ',
  'LEFT JOIN ovdlocal dlo ON dlo.Codigo=ils.Local  ',
  'LEFT JOIN ovdreferencia ref ON ref.Codigo=ils.SeqGrupo ',
  'LEFT JOIN ovgispprfcab prf ON prf.SeqGrupo=ils.SeqGrupo',
  'WHERE ils.IspZtatus=1024',
  'AND prf.Ativo=1',
  'ORDER BY NrReduzidoOP ',

    finally
    Screen.Cursor := crDefault;
    //QrOVgIspLasSta.EnableControls;
  end;
}
end;

procedure TFmOVfOPGerFil.QrNovasReduzOPs_AfterScroll(DataSet: TDataSet);
begin
  ReopenQrOVfOrdemProducao();
end;

procedure TFmOVfOPGerFil.QrOVgIspLasStaAfterOpen(DataSet: TDataSet);
begin
  MyObjects.Informa2(LaAviso1, LaAviso2, False, Geral.FF0(
  QrOVgIspLasSta.RecordCount) + ' novos itens a serem configurarados!');
end;

procedure TFmOVfOPGerFil.QrOVgIspLasStaAfterScroll(DataSet: TDataSet);
begin
  ReopenQrOVfOrdemProducao();
end;

procedure TFmOVfOPGerFil.ReopenNovasReduzOPs();
begin
{
  UnDmkDAC_PF.AbreMySQLQuery0(QrNovasReduzOPs, Dmod.MyDB, [
  'SELECT Local, NrOP, SeqGrupo, NrReduzidoOP, ',
  //'lot.Nome NO_Lote,  ',
  'dlo.Nome NO_Local, ref.Nome NO_Referencia ',
  'FROM ovfordemproducao fop ',
  'LEFT JOIN ovdlocal dlo ON dlo.Codigo=fop.Local ',
  'LEFT JOIN ovdproduto prd ON prd.Controle=fop.Produto ',
  'LEFT JOIN ovdreferencia ref ON ref.Codigo=prd.Codigo ',
  //'LEFT JOIN ovdlote lot ON lot.Codigo=fop.NrLote ',
  OVS_PF.FiltroDadosRelevAlheios(),
  'GROUP BY Local, NrOP, SeqGrupo, NrReduzidoOP ',
  '']);
  //
  //Geral.MB_SQL(self, QrNovasReduzOPs);
}
  UnDmkDAC_PF.AbreMySQLQuery0(QrOVgIspLasSta, Dmod.MyDB, [
  'SELECT ils.Local, ils.NrOP, ils.SeqGrupo,  ',
  'ils.NrReduzidoOP, ils.DataHora, ',
  'dlo.Nome NO_Local, ref.Nome NO_Referencia, ref.Referencia ',
  'FROM ovgisplassta ils ',
  'LEFT JOIN ovdlocal dlo ON dlo.Codigo=ils.Local  ',
  'LEFT JOIN ovdreferencia ref ON ref.Codigo=ils.SeqGrupo ',
  'WHERE ils.IspZtatus=' + Geral.FF0(CO_OVS_IMPORT_ALHEIO_1024_IMPORTADO),
  'ORDER BY NrReduzidoOP ',
  '']);
  MyObjects.Informa2(LaAviso1, LaAviso2, False,
    Geral.FF0(QrOVgIspLasSta.RecordCount) + ' novos itens a configurar!');
end;

procedure TFmOVfOPGerFil.ReopenQrOVfOrdemProducao();
{
  UnDmkDAC_PF.AbreMySQLQuery0(QrOVfOrdemProducao, Dmod.MyDB, [
  'SELECT fop.*, dlo.Nome NO_Local, dlo.Externo DLO_Externo, ',
  'dco.Nome NO_CodCategoria, ref.Nome NO_Referencia, ',
  'prd.CodGrade, prd.CodTam, lot.Nome NO_Lote,  ',
  'nso.Nome NO_NrSituacaOP, tlo.Nome NO_TipoLocalizacao, ',
  'top.Nome NO_TipoOP, tpo.Nome NO_NrTipoProducaoOP ',
  'FROM ovfordemproducao fop ',
  'LEFT JOIN ovdlocal dlo ON dlo.Codigo=fop.Local ',
  'LEFT JOIN ovdclaslocal dcl ON dcl.Codigo=fop.Local ',
  'LEFT JOIN ovdcodcategoria dco ON dco.Codigo=fop.CodCategoria ',
  'LEFT JOIN ovdproduto prd ON prd.Controle=fop.Produto ',
  'LEFT JOIN ovdreferencia ref ON ref.Codigo=prd.Codigo ',
  'LEFT JOIN ovdlote lot ON lot.Codigo=fop.NrLote ',
  'LEFT JOIN ovdnrsituacaoop nso ON nso.Codigo=fop.NrSituacaoOP ',
  'LEFT JOIN ovdtipolocalizacao tlo ON tlo.Codigo=fop.TipoLocalizacao ',
  'LEFT JOIN ovdtipoop top ON top.Codigo=fop.TipoOP ',
  'LEFT JOIN ovdtipoproducaoop tpo ON tpo.CodTxt=fop.NrTipoProducaoOP ',
  OVS_PF.FiltroDadosRelevAlheios(),
  'AND NrReduzidoOP=' + Geral.FF0(QrNovasReduzOPsNrReduzidoOP.Value),
  'AND Local=' + Geral.FF0(QrNovasReduzOPsLocal.Value),
  'AND NrOP=' + Geral.FF0(QrNovasReduzOPsNrOP.Value),
  'AND SeqGrupo=' + Geral.FF0(QrNovasReduzOPsSeqGrupo.Value),
  '']);
  //
}
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrOVfOrdemProducao, Dmod.MyDB, [
  'SELECT fop.*, dlo.Nome NO_Local, dlo.Externo DLO_Externo, ',
  'dco.Nome NO_CodCategoria, ref.Nome NO_Referencia, ',
  'prd.CodGrade, prd.CodTam, lot.Nome NO_Lote,  ',
  'nso.Nome NO_NrSituacaOP, tlo.Nome NO_TipoLocalizacao, ',
  'top.Nome NO_TipoOP, tpo.Nome NO_NrTipoProducaoOP ',
  'FROM ovfordemproducao fop ',
  'LEFT JOIN ovdlocal dlo ON dlo.Codigo=fop.Local ',
  'LEFT JOIN ovdclaslocal dcl ON dcl.Codigo=fop.Local ',
  'LEFT JOIN ovdcodcategoria dco ON dco.Codigo=fop.CodCategoria ',
  'LEFT JOIN ovdproduto prd ON prd.Controle=fop.Produto ',
  'LEFT JOIN ovdreferencia ref ON ref.Codigo=prd.Codigo ',
  'LEFT JOIN ovdlote lot ON lot.Codigo=fop.NrLote ',
  'LEFT JOIN ovdnrsituacaoop nso ON nso.Codigo=fop.NrSituacaoOP ',
  'LEFT JOIN ovdtipolocalizacao tlo ON tlo.Codigo=fop.TipoLocalizacao ',
  'LEFT JOIN ovdtipoop top ON top.Codigo=fop.TipoOP ',
  'LEFT JOIN ovdtipoproducaoop tpo ON tpo.CodTxt=fop.NrTipoProducaoOP ',
  ProjGroup_PF.FiltroDadosReleventesAlheios_Faccoes(),
  'AND NrReduzidoOP=' + Geral.FF0(QrOVgIspLasStaNrReduzidoOP.Value),
  'AND Local=' + Geral.FF0(QrOVgIspLasStaLocal.Value),
  'AND NrOP=' + Geral.FF0(QrOVgIspLasStaNrOP.Value),
  'AND SeqGrupo=' + Geral.FF0(QrOVgIspLasStaSeqGrupo.Value),
  '']);
  //
end;

end.
