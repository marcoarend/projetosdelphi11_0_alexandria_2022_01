unit ImportaCSV_ERP_01;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkPermissoes,
  mySQLDbTables, dmkDBLookupComboBox, dmkEditCB, mySQLDirectQuery,
  UnProjGroup_PF;

type
  TAcaoCvsErpLaituraArq = (acelaNenhum=0, acelaLayout=1, acelaDados=2);
  TFmImportaCSV_ERP_01 = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    LaTitulo1C: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Panel5: TPanel;
    Panel7: TPanel;
    GradeA: TStringGrid;
    Panel10: TPanel;
    MeAvisos: TMemo;
    QrOVpLayEsq: TMySQLQuery;
    DsOVpLayEsq: TDataSource;
    QrOVpLayEsqCodigo: TIntegerField;
    QrOVpLayEsqNome: TWideStringField;
    QrFld: TMySQLQuery;
    QrFldCampo: TWideStringField;
    QrFldTabela: TWideStringField;
    QrFldOrdDataType: TLargeintField;
    PCAcao: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    Panel9: TPanel;
    Label1: TLabel;
    SbLoadCSVOthDir: TSpeedButton;
    Label95: TLabel;
    Label2: TLabel;
    EdLoadCSVOthDir: TEdit;
    EdOVpLayEsq: TdmkEditCB;
    CBOVpLayEsq: TdmkDBLookupComboBox;
    Panel11: TPanel;
    Label3: TLabel;
    SbLoadLayOutDir: TSpeedButton;
    Label4: TLabel;
    EdLoadLayOutDir: TEdit;
    TabSheet3: TTabSheet;
    Panel6: TPanel;
    LBFound: TListBox;
    Panel8: TPanel;
    Panel12: TPanel;
    BtCarregaDados: TBitBtn;
    BtCriaLayouy: TBitBtn;
    Splitter1: TSplitter;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    PB: TProgressBar;
    DqAux: TMySQLDirectQuery;
    EdTempoCarrega: TdmkEdit;
    Label5: TLabel;
    QrLL: TMySQLQuery;
    CkForcaRecarga: TCheckBox;
    QrOVpDirXtr: TMySQLQuery;
    QrOVpDirXtrCodigo: TIntegerField;
    QrOVpDirXtrNome: TWideStringField;
    TabSheet4: TTabSheet;
    TabSheet5: TTabSheet;
    LstLoaded: TListBox;
    LstNaoLoad: TListBox;
    QrTab: TMySQLQuery;
    QrFldNome: TWideStringField;
    QrEncerrados: TMySQLQuery;
    QrEncerradosenc_NrReduzidoOP: TIntegerField;
    QrEncerradosNrReduzidoOP: TIntegerField;
    QrEncerradosLocal: TIntegerField;
    QrEncerradosSeqGrupo: TIntegerField;
    QrAux: TMySQLQuery;
    CkForcaEncerramento: TCheckBox;
    CkForcaReabertura: TCheckBox;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbLoadCSVOthDirClick(Sender: TObject);
    procedure BtCriaLayouyClick(Sender: TObject);
    procedure BtCarregaDadosClick(Sender: TObject);
    procedure SbLoadLayOutDirClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
  private
    { Private declarations }
    FIniTick, FFimTick, FDifTick: DWORD;
    FCodLayout, FCodLog: Integer;
    FDataHora_TXT: String;
    FDataHora_DTH: TDateTime;
    // provisorio! Mudar para dmkGeral!
    function FileTimeToDTime(FTime: TFileTime): TDateTime;
    function  ObtemDatasArquivo(const Arquivo: String; var CreateDT, AccessDT,
              ModifyDT, MaiorDt: TDateTime): Boolean;
    // fim provisorio
    procedure CarregaArquivoParaLayout(var CodLayout: Integer; const Arquivo:
              String; var Tabela: String);
    procedure CarregaArquivoParaMovimento(Arquivo: String; MaiorDt: TDateTime);
    procedure Informa(Texto: String);
    function  InsereTabelaOVPLayEsq(var Nome: String; var Codigo: Integer): Boolean;
    function  InsereTabelaOVPLayTab(const Codigo: Integer; var Controle:
              Integer; const Nome: String; var Tabela: String): Boolean;
    procedure InsereTabelaOVPLayFld(const Codigo, Controle: Integer; var
              Conta: Integer; const Campo, Nome, DataType: String; var Coluna: Integer);
    procedure LeDiretorio(Dir: String; Acao: TAcaoCvsErpLaituraArq);
    function  LeLinhaArq(const Linha: String; var LstLin: TStringList): Boolean;
    procedure ReopenOvpLayEsq();
    procedure AdicionaExtraidosDeCarregados();
    procedure MostraTempoTranscorrido();
    procedure AtualizaLog(Status: Integer; Nome, Campo: String; ValInt: Integer);
    function  DefineNovosItensRelavantes(): Integer;
    function  DefineNovosItensEncerrados(): Integer;
    procedure Mensagem(SvcMsgKind: TSvcMsgKind; Msg: String);
    procedure ReaberturaDeItensEncerrados();
    function  PossivelRastreioEmAndamento(): Boolean;

  public
    { Public declarations }
    FNovasOrdens, FScanAborted: Boolean;
    //
    procedure CarregaDados();
  end;

  var
  FmImportaCSV_ERP_01: TFmImportaCSV_ERP_01;

implementation

uses UnMyObjects, Module, UnDmkProcFunc,
{$IfNDef sDefLayCSV}
MyDBCheck, ModuleGeral,
{$Else}
ModuleGeral,
{$EndIf}
  UMySQLModule,
  UnOVS_PF, DmkDAC_PF, UnGrl_Consts, UnOVS_Consts, UnOVS_ProjGroupVars;

{$R *.DFM}

const
  _STATUS_000 =    0; // In�cio da importa��o
  _STATUS_100 =  100; // Importa��o de Layout
  _STATUS_200 =  200; // Importa��o de movimento
  _STATUS_300 =  300; // Extraindo dados secund�rios
  _STATUS_400 =  400; // Extra��o finalizada
  _STATUS_500 =  500; // Definindo itens relevantes
  _STATUS_600 =  600; // Define itens encerrados
  _STATUS_700 =  700; // Reabrindo itens encerrados
  _STATUS_999 =  999; // Importa��o finalizada

procedure TFmImportaCSV_ERP_01.AdicionaExtraidosDeCarregados();
var
  AllSQL, Codigo, CodUsu, RazaoSocial, Nome, xTip, Tipo: String;
  Item, Count: Integer;
begin
  AtualizaLog(_STATUS_300, 'extraindo para ovdgradecad', EmptyStr, 0);
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Carregando dados de cabe�alhos de grade');
  UnDMkDAC_PF.AbreMySQLDirectQuery0(DqAux, Dmod.MyDB, [
  'SELECT CodGrade',
  'FROM ovdproduto ',
  'GROUP BY CodGrade',
  'ORDER BY CodGrade',
  '']);
  Count := DqAux.RecordCount;
  if Count > 0 then
  begin
    AllSQL := 'INSERT INTO ovdgradecad (Codigo,Nome,Extraido) VALUES ';
    Item := 0;
    while not DqAux.Eof do
    begin
      Codigo := DqAux.FieldValueByFieldName('CodGrade');
      if Codigo = '' then
        Codigo := '0';
      //
      Item := Item + 1;
      if Item = Count then
        AllSQL := AllSQL + '(' + Codigo + ', "Grade ' + Codigo + '", 1)' + sLineBreak
      else
        AllSQL := AllSQL + '(' + Codigo + ', "Grade ' + Codigo + '", 1),' + sLineBreak;
      //
      DqAux.Next;
    end;
    AllSQL := AllSQL + ' ON DUPLICATE KEY UPDATE ReInsrt=ReInsrt+1, LastInsrt="' + FDataHora_TXT + '";';
    DMod.MyDB.Execute(AllSQL);
  end;
  //
  //
  //
  AtualizaLog(_STATUS_300, 'extraindo para ovdgradetam', EmptyStr, 0);
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Carregando dados de tamanhos de grade');
   UnDMkDAC_PF.AbreMySQLDirectQuery0(DqAux, Dmod.MyDB, [
  'SELECT CodGrade, CodTam ',
  'FROM ovdproduto ',
  'GROUP BY CodGrade, CodTam ',
  'ORDER BY CodGrade, CodTam ',
  '']);
  Count := DqAux.RecordCount;
  if Count > 0 then
  begin
    AllSQL := 'INSERT ovdgradetam (Codigo,Nome,Extraido) VALUES ';
    Item := 0;
    while not DqAux.Eof do
    begin
      Codigo := DqAux.FieldValueByFieldName('CodGrade');
      Nome   := Geral.VariavelToString(DqAux.FieldValueByFieldName('CodTam'));
      //
      Item := Item + 1;
      if Item = Count then
        AllSQL := AllSQL + '(' + Codigo + ', ' + Nome +  ', 1)' + sLineBreak
      else
        AllSQL := AllSQL + '(' + Codigo + ', ' + Nome +  ', 1),' + sLineBreak;
      //
      DqAux.Next;
    end;
    AllSQL := AllSQL + ' ON DUPLICATE KEY UPDATE ReInsrt=ReInsrt+1, LastInsrt="' + FDataHora_TXT + '";';
    DMod.MyDB.Execute(AllSQL);
  end;
  //
  //
  //
  AtualizaLog(_STATUS_300, 'extraindo para entidades', EmptyStr, 0);
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Carregando dados de entidades de pessoas');
   UnDMkDAC_PF.AbreMySQLDirectQuery0(DqAux, Dmod.MyDB, [
  'SELECT pes.*',
  'FROM ovdpessoas pes',
  'LEFT JOIN entidades ent ON ent.Codigo=pes.Codigo',
  'WHERE ent.Codigo IS NULL ',
  'ORDER BY pes.Codigo',
  '']);
  Count := DqAux.RecordCount;
  if Count > 0 then
  begin
    AllSQL := 'INSERT INTO entidades (Codigo,CodUsu,Tipo,RazaoSocial,Nome) VALUES ';
    Item := 0;
    while not DqAux.Eof do
    begin
      Codigo := DqAux.FieldValueByFieldName('Codigo');
      CodUsu := Codigo;
      XTip   := DqAux.FieldValueByFieldName('TpPessoa');
      if XTip = 'F' then
      begin
        Nome        := Geral.VariavelToString(DqAux.FieldValueByFieldName('Nome'));
        RazaoSocial := '""';
        Tipo        := '1';
      end else
      begin
        Nome        := '""';
        RazaoSocial := Geral.VariavelToString(DqAux.FieldValueByFieldName('Nome'));
        Tipo        := '0';
      end;
      //
      Item := Item + 1;
      if Item = Count then
        AllSQL := AllSQL + '(' + Codigo + ', ' + CodUsu + ', ' + Tipo +', ' + RazaoSocial + ', ' + Nome +  ')' + sLineBreak
      else
        AllSQL := AllSQL + '(' + Codigo + ', ' + CodUsu + ', ' + Tipo +', ' + RazaoSocial + ', ' + Nome +  '),' + sLineBreak;
      //
      DqAux.Next;
    end;
    AllSQL := AllSQL + ' ON DUPLICATE KEY UPDATE Ativo=1;';
    DMod.MyDB.Execute(AllSQL);
  end;
  //
  //
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, True, '...');
end;

procedure TFmImportaCSV_ERP_01.AtualizaLog(Status: Integer; Nome, Campo: String;
ValInt: Integer);
var
  DataHora: String;
  Codigo: Integer;
  SQLType: TSQLType;
begin
  if FCodLog = 0 then
  begin
    SQLType := stIns;
    Codigo := UMyMod.BPGS1I32('ovpimplog', 'Codigo', '', '', tsPos, stIns, 0);
    FCodLog := Codigo;
  end else
  begin
    Codigo  := FCodLog;
    SQLType := stUpd;
  end;
  //Codigo         := ;
  //Nome           := ;
  //Status         := ;
  DataHora       := FDataHora_TXT;
  //
  //if
  if Campo <> EmptyStr then
  begin
    UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'ovpimplog', False, [
    'Nome', 'Status', 'DataHora',
    Campo], [
    'Codigo'], [
    Nome, Status, DataHora,
    ValInt], [
    Codigo], True);
  end else
  begin
    UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'ovpimplog', False, [
    'Nome', 'Status', 'DataHora'], ['Codigo'], [
    Nome, Status, DataHora], [Codigo], True);
  end;
end;

procedure TFmImportaCSV_ERP_01.BtCarregaDadosClick(Sender: TObject);
begin
  CarregaDados();
end;

procedure TFmImportaCSV_ERP_01.BtCriaLayouyClick(Sender: TObject);
var
  Dir: String;
begin
{$IfNDef sDefLayCSV}
  if not DBCheck.LiberaPelaSenhaAdmin() then Exit;
  //
  Dir := EdLoadLayOutDir.Text;
  //
  MeAvisos.Text := '';
  VAR_LOAD_CSV_Memo := MeAvisos;
  //
  LeDiretorio(Dir, TAcaoCvsErpLaituraArq.acelaLayout);
  MostraTempoTranscorrido();
  ReopenOvpLayEsq();
{$Else}
  Mensagem(TSvcMsgKind.smkRunOK,
    'Aplicativo sem defini��o de Layout de importa��o CSV!');
{$EndIf}
end;

procedure TFmImportaCSV_ERP_01.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmImportaCSV_ERP_01.CarregaArquivoParaLayout(var CodLayout: Integer; const Arquivo:
 String; var Tabela: String);
var
  I, Controle, Conta, Coluna: Integer;
  Linha, CodTxt, Nome, DataIni, DataFim, NomeEsq, NomeArq, NomeTab, NomeFld,
  Campo, DataType: String;
  LstArq, LstLin1, LstLin2, LstLin3: TStringList;
  Continua: Boolean;
begin
  AtualizaLog(_STATUS_100, '', EmptyStr, 0);
  //FItens := 0;
  if FileExists(Arquivo) then
  begin
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Abrindo arquivo: ' + Arquivo + '.');
    LstArq := TStringList.Create;
    try
      LstLin1 := TStringList.Create;
      try
        LstLin2 := TStringList.Create;
        try
          LstLin3 := TStringList.Create;
          try
            LstArq.LoadFromFile(Arquivo);
            if lstArq.Count > 0 then
            begin
              Linha := ';' + LstArq[0] + ';';
              //
              LstLin1 := Geral.Explode3(Linha, ';');
              if LstLin1.Count > 0 then
              begin
                Linha := ';' + LstArq[1] + ';';
                LstLin2 := Geral.Explode3(Linha, ';');
                if LstLin2.Count > 0 then
                begin
                  if LstLin1.Count <> LstLin2.Count then
                  begin
                    Informa('[CAPL] Quantidade de Colunas difere (2): ' +
                      sLineBreak + Arquivo);
                    Exit;
                  end;
                  Linha := ';' + LstArq[2] + ';';
                  LstLin3 := Geral.Explode3(Linha, ';');
                  if LstLin3.Count > 0 then
                  begin
                    if LstLin1.Count <> LstLin3.Count then
                    begin
                      Informa('[CAPL] Quantidade de Colunas difere (3): ' +
                        sLineBreak + Arquivo);
                      Exit;
                    end;
                    NomeEsq  := '';
                    Controle := 0;
                    NomeArq  := ExtractFileName(Arquivo);
                    NomeTab  := '';
                    if CodLayout = 0 then
                      Continua := InsereTabelaOVPLayEsq(NomeEsq, CodLayout)
                    else
                      Continua := True;
                    if Continua then
                    begin
                      if InsereTabelaOVPLayTab(CodLayout, Controle, NomeArq, NomeTab) then
                      begin
                        for I := 0 to LstLin1.Count - 1 do
                        begin
                          Conta    := 0;
                          Coluna   := I + 1;
                          NomeFld  := LstLin1[I];
                          //NomeFld  := UTF8ToWIdeString(LstLin1[I]);
                          Campo    := LstLin2[I];
                          DataType := LstLin3[I];
                          InsereTabelaOVPLayFld(CodLayout, Controle, Conta, Campo,
                            NomeFld, DataType, Coluna);
                        end;
                      end;
                    end;
                  end;
                end;
              end;
            end;
            //
            MyObjects.Informa2(LaAviso1, LaAviso2, False, Geral.FDT(
              Dmod.DModG_ObtemAgora(), 2) + ' Obten��o layout finalizado do arquivo: ' +
              Arquivo);
            PB.Position := 0;
          finally
            if LstLin3 <> nil then
              LstLin3.Free;
          end;
        finally
          if LstLin2 <> nil then
            LstLin2.Free;
        end;
      finally
        if LstLin1 <> nil then
          LstLin1.Free;
      end;
    finally
      if LstArq <> nil then
        LstArq.Free;
    end;
  end;
end;

procedure TFmImportaCSV_ERP_01.CarregaArquivoParaMovimento(Arquivo: String;
  MaiorDt: TDateTime);
var
  I, N, K, L: Integer;
  Linha, CodTxt, Nome, DataIni, DataFim, Texto, NomeEExtArq, Tabela, TabXtr, Campos,
  Valrs, BlocoSQL, SQLtoExec, xDt, xHr, AllTxt: String;
  LstArq, LstLin: TStringList;
  ArrColunas: array of Integer;
  ArrDataTyp: array of Integer;
  ArrValores: array of String;
  J, EofArq: Integer;
  //Continua: Boolean;
  NomeArq, TitCol, s1, s2, Msg: String;
  LastLoad: String;
begin
  TabXtr := EmptyStr;
  //FItens := 0;
  if FileExists(Arquivo) then
  begin
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Abrindo arquivo: ' + Arquivo + '.');
    // 2019-11-04
    NomeEExtArq := ExtractFileName(Arquivo);
    UnDmkDAC_PF.AbreMySQLQuery0(QrTab, Dmod.MyDB, [
    'SELECT tab.Tabela ',
    'FROM ovplaytab tab ',
    'WHERE UPPER(tab.Nome)=UPPER("' + NomeEExtArq + '") ',
    ' ']);
    if QrTab.RecordCount = 0 then
    begin
      LstNaoLoad.Items.Add(Arquivo);
      Exit;
    end else begin
      //LstLoaded.Items.Add(Arquivo);
      // Fim 2019-11-04
    end;
    lstArq := TStringList.Create;
    try
      LstLin := TStringList.Create;
      try
        try
          lstArq.LoadFromFile(Arquivo);
        except
          on E: Exception do
          begin
            Mensagem(TSvcMsgKind.smkGetErr, NomeEExtArq + '[dmk 481] ' + E.Message);
            Exit;
          end;
        end;
        if lstArq.Count > 1 then
        begin
          Linha := ';' + lstArq[0] + ';';
          LstLin := Geral.Explode3(Linha, ';');
          N := -1;
          SetLength(ArrColunas, 0);
          //SetLength(ArrValores, N);
          AtualizaLog(_STATUS_200, NomeEExtArq, EmptyStr, 0);
          Tabela := '';
          Campos := '';
          for I := 0 to LstLin.Count - 1 do
          begin
            UnDmkDAC_PF.AbreMySQLQuery0(QrFld, Dmod.MyDB, [
            'SELECT tab.Tabela, fld.Nome, fld.Campo, ',
            'ORD(fld.DataType) OrdDataType ',
            'FROM ovplayfld fld ',
            'LEFT JOIN ovplaytab tab ON tab.Controle=fld.Controle ',
            'WHERE fld.Codigo=' + Geral.FF0(FCodLayout),
            'AND UPPER(tab.Nome)=UPPER("' + NomeEExtArq + '") ',
            'AND fld.Coluna=' + Geral.FF0(I + 1),
            ' ']);
            //TitCol := Trim(UTF8ToWIdeString(LstLin[I]));
            TitCol := Trim(LstLin[I]);
            //TitCol := DmkPF.ConverteMinuscEmMaiusc(TitCol);
            s1 := QrFldNome.Value;
            s1 := DmkPF.LimpaTexto(s1);
            s2 := TitCol;
            s2 := DmkPF.LimpaTexto(s2);
            if AnsiCompareStr(Uppercase(s1), Uppercase(s2)) <> 0 then
            begin
             Mensagem(TSvcMsgKind.smkGetErr,
              'A coluna esperada "' + s2 + //LstLin[I] +
              '" n�o confere com a econtrada "' + s1 + //QrFldNome.Value +
              '" no arquivo:' + sLineBreak + Arquivo);
              LstNaoLoad.Items.Add(Arquivo);
{              /
[2019-12-04 12:39:33][qT2Vrutg8MKxiFS7KYb9qYnE8In3XyAT]:OverSeer_RST: A coluna esperada "CodLocal" n�o confere com a econtrada "Local" no arquivo:
C:\pBI\pDATA\FORDENSPRODUCAO.CSV
}
              Exit;
            end;
            if QrFld.RecordCount > 0 then
            begin
              if Tabela = '' then
              begin
                Tabela := QrFldTabela.Value;
                if Uppercase(Tabela) = Uppercase('ovFOrdemProducao') then
                begin
                  FNovasOrdens := True;
                  TabXtr       := LowerCase('ovMOrdemProducao');
                end;
              end;
               N := N + 1;
              if N > 0 then
                Campos := Campos + ',';
              Campos := Campos + QrFldCampo.Value;
              SetLength(ArrColunas, N + 1);
              SetLength(ArrDataTyp, N + 1);
              //SetLength(ArrValores, N + 1);
              ArrColunas[N] := I;
              ArrDataTyp[N] := QrFldOrdDataType.Value;
            end;
          end;
////////////////////////////////////////////////////////////////////////////////
          LstLoaded.Items.Add(Arquivo);
////////////////////////////////////////////////////////////////////////////////
          SetLength(ArrValores, N + 1);
          //
          PB.Position := 0;
          PB.Max := lstArq.Count;
          BlocoSQL := '';
          //
          EofArq := lstArq.Count -1;
          L := 0;
          for I := 1 to EofArq do
          begin
            L := L + 1;
            PB.Position := PB.Position + 1;
            //
            Linha := StringReplace(lstArq[I], ';', '', [rfReplaceAll, rfIgnoreCase]);
            if Length(Linha) > 0 then
            begin
              Linha := ';' + lstArq[I] + ';';
              LstLin := Geral.Explode3(Linha, ';');
              //
              Valrs := '';
              for J := 0 to Length(ArrColunas) - 1 do
              begin
                Texto := LstLin[ArrColunas[J]];
                if J = 0 then
                begin
                  if Length(BlocoSQL) > 0 then
                    BlocoSQL := BlocoSQL + ', ';
                  //Continua := Length(Trim(Texto)) > 0;
                  Valrs := Valrs + ' ('
                end else
                  Valrs := Valrs + ',';
                //if Continua then
                begin
                  case ArrDataTyp[J] of
                    (*c,C*)099, 067: Valrs := Valrs + Geral.VariavelToString(Texto);
                    (*d,D*)100, 068:
                    begin
                      if Texto = '' then
                        Valrs := Valrs + '"0000-00-00"'
                      else
                      try
                        K := pos(' ', Texto);
                        if K > 0  then
                        begin
                          xDt := Copy(Texto, 1, K-1);
                          xHr := Copy(Texto, K + 1);
                          Valrs := Valrs + Geral.VariavelToString(Geral.FDT(Geral.ValidaDataBr(xDt, True, False), 1) + ' ' + xHr);
                        end  else
                         Valrs := Valrs + Geral.VariavelToString(Geral.ValidaDataBr(Texto, True, False));
                      except
                        Mensagem(TSvcMsgKind.smkGetErr, 'Erro ao converter data: ' + Texto);
                      end;
                    end;
                    (*i,I*)105, 073:
                    begin
                      if Texto = '' then
                        Valrs := Valrs + '0'
                      else
                        Valrs := Valrs + Texto;
                    end;
                    (*n,N*)110, 078:
                    begin
                      if Texto = '' then
                        Valrs := Valrs + '0'
                      else
                        Valrs := Valrs + StringReplace(Texto, ',', '', [rfReplaceAll, rfIgnoreCase]);
                    end;
                    (*???*)else
                    begin
                      Informa('DataType no csv layout n�o implementado: ' + Geral.FF0(ArrDataTyp[J]));
                      Valrs := Valrs + '"' + Texto + '"';
                    end;
                  end;
                end;
              end;
              if L >= VAR_LoadCSVRowsBlc then
              begin
                Valrs := Valrs + ') ';
                BlocoSQL := BlocoSQL + Valrs;
                //
                SQLtoExec := 'INSERT INTO ' + Tabela + ' (' + Campos + ') VALUES ' +
                BlocoSQL + sLineBreak + ' ON DUPLICATE KEY UPDATE ReInsrt=ReInsrt+1, LastInsrt="' + FDataHora_TXT + '";';
                try
                  Application.ProcessMessages;
                  Dmod.MyDB.Execute(SQLToExec);
                except
                  on E: Exception do
                  begin
                    Mensagem(TSvcMsgKind.smkGetErr, 'Tabela: ' + Tabela +
                    '[dmk 629] ' + E.Message + sLineBreak + SQLToExec);
                    Exit;
                  end;
                end;
////////////////////////////////////////////////////////////////////////////////
                // Espec�fico para: TabXtr := LowerCase('ovMOrdemProducao');
                if TabXtr <> EmptyStr then
                begin
                  SQLtoExec := 'INSERT INTO ' + TabXtr + ' (' + Campos + ') VALUES ' +
                  BlocoSQL + sLineBreak + ' ON DUPLICATE KEY UPDATE ReInsrt=ReInsrt+1, LastInsrt="' + FDataHora_TXT + '";';
                  try
                    Application.ProcessMessages;
                    Dmod.MyDB.Execute(SQLToExec);
                  except
                    on E: Exception do
                    begin
                      Mensagem(TSvcMsgKind.smkGetErr, 'TabXtr: ' + TabXtr +
                      '[dmk 629(2)] ' + E.Message + sLineBreak + SQLToExec);
                      Exit;
                    end;
                  end;
                end;
                //
////////////////////////////////////////////////////////////////////////////////
                BlocoSQL := '';
                Valrs := Valrs + ' (';
                L := 0;
              end else
              begin
(*
                if I < EofArq then
                  Valrs := Valrs + '), ' + sLineBreak
                else
                  Valrs := Valrs + ') ';
*)
                  Valrs := Valrs + ')';

                //
                BlocoSQL := BlocoSQL + Valrs;

              end;
            end;
          end;
          if Length(BlocoSQL) > 0 then
          begin
            //
            SQLtoExec := 'INSERT INTO ' + Tabela + ' (' + Campos + ') VALUES ' +
            BlocoSQL + sLineBreak + ' ON DUPLICATE KEY UPDATE ReInsrt=ReInsrt+1, LastInsrt="' + FDataHora_TXT + '";';
            //
            try
              Application.ProcessMessages;
              Dmod.MyDB.Execute(SQLToExec);
            except
              on E: Exception do
              begin
                Mensagem(TSvcMsgKind.smkGetErr, 'Tabela: ' + Tabela +
                '[dmk 664] ' + E.Message + sLineBreak + SQLToExec);
                Exit;
              end;
            end;
            // Colocar data em novos utens
            SQLtoExec := 'UPDATE ' + Tabela + ' SET LastInsrt="' + FDataHora_TXT + '" WHERE LastInsrt <= "1900-01-01";';
            try
              Application.ProcessMessages;
              Dmod.MyDB.Execute(SQLToExec);
            except
              on E: Exception do
              begin
                Mensagem(TSvcMsgKind.smkGetErr, 'Tabela: ' + Tabela +
                '[dmk 664] ' + E.Message + sLineBreak + SQLToExec);
                Exit;
              end;
            end;
////////////////////////////////////////////////////////////////////////////////
            // Espec�fico para: TabXtr := LowerCase('ovMOrdemProducao');
            if TabXtr <> EmptyStr then
            begin
              SQLtoExec := 'INSERT INTO ' + TabXtr + ' (' + Campos + ') VALUES ' +
              BlocoSQL + sLineBreak + ' ON DUPLICATE KEY UPDATE ReInsrt=ReInsrt+1, LastInsrt="' + FDataHora_TXT + '";';
              //
              try
                Application.ProcessMessages;
                Dmod.MyDB.Execute(SQLToExec);
              except
                on E: Exception do
                begin
                  Mensagem(TSvcMsgKind.smkGetErr, 'TabXtr: ' + TabXtr +
                  '[dmk 664] ' + E.Message + sLineBreak + SQLToExec);
                  Exit;
                end;
              end;
              // Colocar data em novos utens
              SQLtoExec := 'UPDATE ' + TabXtr + ' SET LastInsrt="' + FDataHora_TXT + '" WHERE LastInsrt <= "1900-01-01";';
              try
                Application.ProcessMessages;
                Dmod.MyDB.Execute(SQLToExec);
              except
                on E: Exception do
                begin
                  Mensagem(TSvcMsgKind.smkGetErr, 'TabXtr: ' + TabXtr +
                  '[dmk 664] ' + E.Message + sLineBreak + SQLToExec);
                  Exit;
                end;
              end;
              // Excluir antigos
              SQLtoExec := 'DELETE FROM ' + TabXtr + ' WHERE LastInsrt<>"' + FDataHora_TXT + '" ;';
              try
                Application.ProcessMessages;
                Dmod.MyDB.Execute(SQLToExec);
              except
                on E: Exception do
                begin
                  Mensagem(TSvcMsgKind.smkGetErr, 'TabXtr: ' + TabXtr +
                  '[dmk 664] ' + E.Message + sLineBreak + SQLToExec);
                  Exit;
                end;
              end;
            end;
            //
////////////////////////////////////////////////////////////////////////////////
          end;
        end;
        //if OVpLayTab_Controle > 0 then
        begin
          LastLoad := Geral.FDT(MaiorDt, 109);
          NomeArq  := ExtractFileName(Arquivo);

          UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'ovplaytab', False, [
          'LastLoad'], ['Codigo', 'Nome'], [LastLoad], [FCodLayout, NomeArq], True);
        end;
        //
        LstLoaded.Items.Add(Arquivo);
        //
        MyObjects.Informa2(LaAviso1, LaAviso2, True, Geral.FDT(
          Dmod.DModG_ObtemAgora(), 2) + ' Carregamento finalizado do arquivo: ' +
          NomeEExtArq);
        //
        Msg := ' Arquivo carregado: ' + NomeEExtArq + ' :: ' + LastLoad;
        if VAR_RUN_AS_SVC then
          Mensagem(TSvcMsgKind.smkRunOK, Msg)
        else
          Informa(Msg);
        PB.Position := 0;
      finally
        //if LstLin <> nil then
          //LstLin.Free;
         FreeAndNil(lstLin);
      end;
    finally
      //if lstArq <> nil then
        //lstArq.Free;
      FreeAndNil(lstArq);
    end;
  end;
end;


procedure TFmImportaCSV_ERP_01.CarregaDados;
var
  Dir, Fim_TXT: String;
  Inclusoes, Encerrados: Integer;
begin
  Informa('OverSeerRst2.CarregaDados em iniciado!');
  //
  FNovasOrdens := False;
  LstLoaded.Items.Clear;
  LstNaoLoad.Items.Clear;
  //
  Screen.Cursor := crHourGlass;
  try
    MeAvisos.Text := '';
    VAR_LOAD_CSV_Memo := MeAvisos;
    //
    FDataHora_DTH  := Dmod.DModG_ObtemAgora();
    FDataHora_TXT  := Geral.FDT(FDataHora_DTH, 109);
    FCodLayout := EdOVpLayEsq.ValueVariant;
    //
    if FCodLayout = 0 then
    begin
      Mensagem(TSvcMsgKind.smkGetErr, 'Esquema de Layout n�o definido!');
      Exit;
    end;
    //
    if PossivelRastreioEmAndamento() then
    begin
      Mensagem(TSvcMsgKind.smkGetErr, 'Poss�vel rastreio j� em andamento!');
      Exit;
    end else
    try
      //
      Dir := EdLoadCSVOthDir.Text;
      //
      AtualizaLog(_STATUS_000, '', EmptyStr, 0);
      LeDiretorio(Dir, TAcaoCvsErpLaituraArq.acelaDados);
      //
      AdicionaExtraidosDeCarregados();
      AtualizaLog(_STATUS_400, 'OK', EmptyStr, 0);
      //
      if FNovasOrdens  then
      begin
        Inclusoes := DefineNovosItensRelavantes();
        AtualizaLog(_STATUS_500, 'OK', 'Inclusoes', Inclusoes);
        //
        Encerrados := DefineNovosItensEncerrados();
        AtualizaLog(_STATUS_600, 'OK', 'Encerrados', Encerrados);
        //
        //Reabertos :=
        Dmod.ReopenOpcoesApp();
        if Dmod.QrOpcoesAppForcaReabCfgInsp.Value = 1 then
        begin
          ReaberturaDeItensEncerrados();
          AtualizaLog(_STATUS_600, 'OK', EmptyStr, 0);//'Reabertos', Encerrados);
        end;
        //
      end;
      //DefineNovosItensEncerrados();
      AtualizaLog(_STATUS_999, 'OK', EmptyStr, 0);
      //
      MostraTempoTranscorrido();
    finally
      Fim_TXT := Geral.FDT(DModG.ObtemAgora(), 109);
      //
      UnDMkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
      'UPDATE opcoesapp ',
      'SET DtHrFimRastreio="' + Fim_TXT + '"',
      'WHERE Codigo=1 ',
      EmptyStr]);
    end;
  finally
    Screen.Cursor := crDefault;
  end;
end;

function TFmImportaCSV_ERP_01.DefineNovosItensEncerrados(): Integer;
var
  NrReduzidoOP, Local, SeqGrupo, Encerrados, Reativados: Integer;
  DataHora: String;
begin
  if (not VAR_RUN_AS_SVC) and (CkForcaEncerramento.Checked = False) then
    Exit;
////////////////////////////////////////////////////////////////////////////////
  Result := 0;
  //
(*  FDataHora_TXT:*)
  UnDmkDAC_PF.AbreMySQLQuery0(QrAux, Dmod.MyDB, [
  'SELECT MAX(LastInsrt), LastInsrt ',
  'FROM ovfordemproducao ',
  EmptyStr]);
  //
  DataHora := Geral.FDT(QrAux.FieldByName('LastInsrt').AsDateTime, 109);
  if (DataHora = FDataHora_TXT) and (FDataHora_DTH > 2) then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrEncerrados, Dmod.MyDB, [
    'DROP TABLE IF EXISTS _Alheio_Encerrado_;',
    'CREATE TABLE _Alheio_Encerrado_',
    'SELECT DISTINCT NrReduzidoOP, Local, SeqGrupo',
    'FROM ovfordemproducao',
    'WHERE LastInsrt="' + DataHora + '"',
    ';',
    '',
    'DROP TABLE IF EXISTS _Alheio_Configurado_;',
    'CREATE TABLE _Alheio_Configurado_',
    'SELECT DISTINCT NrReduzidoOP, Local, SeqGrupo',
    'FROM ovgispgercab',
    'WHERE ZtatusIsp=' + Geral.FF0(CO_OVS_IMPORT_ALHEIO_5120_APTO_DOWNMOBILE) + ';',
    '',
    'SELECT enc.NrReduzidoOP enc_NrReduzidoOP,',
    'cfg.*',
    'FROM _Alheio_Configurado_ cfg',
    'LEFT JOIN _Alheio_Encerrado_ enc',
    '  ON enc.NrReduzidoOP=cfg.NrReduzidoOP',
    'AND enc.Local=cfg.Local',
    'AND enc.SeqGrupo=cfg.SeqGrupo',
    'WHERE enc.NrReduzidoOP IS NULL',
    EmptyStr]);
    //Geral.MB_SQL(Self, QrEncerrados);
    //
    QrEncerrados.First;
    while not QrEncerrados.Eof do
    begin
      NrReduzidoOP := QrEncerradosNrReduzidoOP.Value;
      Local        := QrEncerradosLocal.Value;
      SeqGrupo     := QrEncerradosSeqGrupo.Value;
      //
      if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'ovgispgercab', False, [
      'DtHrFecha', 'ZtatusIsp'], [
      'NrReduzidoOP', 'Local', 'SeqGrupo'], [
      DataHora, CO_OVS_IMPORT_ALHEIO_6144_ENCERRADO], [
      NrReduzidoOP, Local, SeqGrupo], True) then
      begin
        Result := Result + 1;
      end;
      //
      QrEncerrados.Next;
    end;
  end;
end;

function  TFmImportaCSV_ERP_01.DefineNovosItensRelavantes(): Integer;
var
  SQL: String;
begin
  Result := 0;
  UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
  'DROP TABLE IF EXISTS _Alheio_Relevnt_1024_;',
  '',
  'CREATE TABLE _Alheio_Relevnt_1024_',
  'SELECT DISTINCT fop.Local, fop.NrOP, fop.SeqGrupo, fop.NrReduzidoOP, ',
  'fop.Lk, fop.DataCad, fop.DataAlt, fop.UserCad, fop.UserAlt, ',
  'fop.AlterWeb, fop.AWServerID, fop.AWStatSinc, fop.Ativo, ',
  '0 GrupCnfg, 1024 GrupSgmt ',  // CO_SGMT_INSP_1024_FACCAO
  'FROM ovfordemproducao fop ',
  'LEFT JOIN ovdlocal dlo ON dlo.Codigo=fop.Local ',
  'LEFT JOIN ovdproduto prd ON prd.Controle=fop.Produto ',
  'LEFT JOIN ovdreferencia ref ON ref.Codigo=prd.Codigo ',
  ProjGroup_PF.FiltroDadosReleventesAlheios(),
  'GROUP BY Local, NrOP, SeqGrupo, NrReduzidoOP ',
  '']);
  Geral.MB_SQL(Self, Dmod.QrUpd);
  //
  SQL := UMyMod.ObtemCamposDeTabelaIdentica(Dmod.MyDB, 'ovgisplassta', '_ar.');
  SQL := Geral.Substitui(SQL, ', _ar.IspZtatus', ', ' +
    Geral.FF0(CO_OVS_IMPORT_ALHEIO_1024_IMPORTADO) + ' IspZtatus');
  SQL := Geral.Substitui(SQL, ', _ar.DataHora', ', "' + FDataHora_TXT + '" DataHora');
  SQL := Geral.Substitui(SQL, ', _ar.Motivo', ', 0 Motivo');
  // Atualiza tabela do �ltimo status
  UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
  'INSERT INTO ovgisplassta ',
  'SELECT DISTINCT ',
  SQL,
  'FROM _Alheio_Relevnt_1024_ _ar',
  'LEFT JOIN ovgisplassta ilp ON',
  '   ilp.Local   = _ar.Local',
  '  AND ',
  '   ilp.NrOP    = _ar.NrOP',
  '  AND   ',
  '  ilp.SeqGrupo = _ar.SeqGrupo',
  '  AND',
  '   ilp.NrReduzidoOP  = _ar.NrReduzidoOP',
  //'WHERE ilp.DataHora IS NULL',
  'WHERE ilp.Local IS NULL',
  //'ON DUPLICATE KEY UPDATE Ativo=1 ',
  EmptyStr]);
  // deve ser aqui!
  Result := Dmod.QrUpd.RowsAffected;
  //
  // Atualiza Log de todos status
  UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
  'INSERT INTO ovgispallsta',
  'SELECT * ',
  'FROM ovgisplassta',
  'WHERE DataHora = "' + FDataHora_TXT + '"',
  'AND IspZtatus=' + Geral.FF0(CO_OVS_IMPORT_ALHEIO_1024_IMPORTADO),
  '']);
  //
end;

function TFmImportaCSV_ERP_01.FileTimeToDTime(FTime: TFileTime): TDateTime;
var
  LocalFTime: TFileTime;
  STime: TSystemTime;
begin
  FileTimeToLocalFileTime(FTime, LocalFTime);
  FileTimeToSystemTime(LocalFTime, STime);
  Result := SystemTimeToDateTime(STime);
end;

procedure TFmImportaCSV_ERP_01.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmImportaCSV_ERP_01.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  PCAcao.ActivePageIndex := 0;
  //
  Dmod.ReopenOpcoesApp();
  EdLoadCSVOthDir.Text := Dmod.QrOpcoesAppLoadCSVOthDir.Value;
  ReopenOvpLayEsq();
  //
  EdOVpLayEsq.ValueVariant := Dmod.QrOpcoesAppOVpLayEsq.Value;
  CBOVpLayEsq.KeyValue     := Dmod.QrOpcoesAppOVpLayEsq.Value;
  //
  FDataHora_DTH  := Dmod.DModG_ObtemAgora();
  FDataHora_TXT  := Geral.FDT(FDataHora_DTH, 109);
  FCodLog   := 0;
end;

procedure TFmImportaCSV_ERP_01.FormDestroy(Sender: TObject);
begin
  DmkPF.TrimAppMemorySize();
end;

procedure TFmImportaCSV_ERP_01.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmImportaCSV_ERP_01.Informa(Texto: String);
begin
  MeAvisos.Text := Geral.FDT(Dmod.DModG_ObtemAgora(), 2) + ' ' + Texto +
    sLineBreak + MeAvisos.Text;
end;

function TFmImportaCSV_ERP_01.InsereTabelaOVPLayEsq(var Nome: String; var Codigo: Integer): Boolean;
var
  SQLType: TSQLType;
begin
  Result := False;
  if Trim(Nome) = '' then
    InputQuery('Nome do Esquema de Layout',
    'Informe o nome do esquema do layout:', Nome);
  if Codigo = 0 then
    SQLType := TSQLType.stIns
  else
    SQLType := TSQLType.stUpd;
  //
  Codigo := UMyMod.BPGS1I32('ovplayesq', 'Codigo', '', '', tsPos, SQLType, Codigo);
  if Trim(Nome) = '' then
    Nome := 'Esquema de layout ' + Geral.FF0(Codigo);
  //
  Result :=  UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'ovplayesq', False, [
    'Nome'], ['Codigo'], [
    Nome], [Codigo], True);
end;

procedure TFmImportaCSV_ERP_01.InsereTabelaOVPLayFld(const Codigo,
  Controle: Integer; var Conta: Integer; const Campo, Nome, DataType: String;
  var Coluna: Integer);
var
  SQLType: TSQLType;
begin
  if Conta = 0 then
    SQLType := TSQLType.stIns
  else
    SQLType := TSQLType.stUpd;
  //Codigo         := ;
  //Controle       := ;
  //Conta          := ;
  //Coluna         := ;
  //Campo          := ;
  //Nome           := ;
  Conta := UMyMod.BPGS1I32('ovplayfld', 'Conta', '', '', tsPos, SQLType, Conta);
  //if
  UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'ovplayfld', False, [
  'Codigo', 'Controle', 'Coluna',
  'Campo', 'Nome', 'DataType'], [
  'Conta'], [
  Codigo, Controle, Coluna,
  Campo, Nome, DataType], [
  Conta], True);
end;

function TFmImportaCSV_ERP_01.InsereTabelaOVPLayTab(const Codigo: Integer; var
  Controle: Integer; const Nome: String; var Tabela: String): Boolean;
const
  cArquivos: array[0..9] of String = ('',
  'DPESSOAS.csv',
  'DCICLO.csv',
  'DCLASLOCAL.csv',
  'DEMPRESA.csv',
  'DLOCAL.csv',
  //'DLOTE.csv',
  'DLOTEPRODUCAO.csv',
  'DPRODUTO.csv',
  'DREFERENCIA.csv',
  //'FORDEMPRODUCAO.csv');
  'FORDENSPRODUCAO.csv');
  cTabelas: array[0..9] of String = ('',
  'ovDPessoas',
  'ovDCiclo',
  'ovDClasLocal',
  'ovDEmpresa',
  'ovDLocal',
  'ovDLote',
  'ovDProduto',
  'ovDReferencia',
  'ovFOrdemProducao');
var
  P: Integer;
  SQLType: TSQLType;
begin
  if Controle = 0 then
    SQLType := TSQLType.stIns
  else
    SQLType := TSQLType.stUpd;
  //
  if Trim(Nome) = '' then
  begin
    Informa('[ITTab] Nome tabela indefinido');
    Exit;
  end;
  //
  if Trim(Tabela) = '' then
  begin
    //P := pos(Nome, cTabelas);
    for P := 0 to Length(cArquivos) - 1 do
    begin
      if Lowercase(cArquivos[P]) = Lowercase(Nome) then
      begin
        Tabela := cTabelas[P];
        Break;
      end;
    end;
  end;
(*
  DPESSOAS.csv
  DCICLO.csv
  DCLASLOCAL.csv
  DEMPRESA.csv
  DLOCAL.csv
  DLOTE.csv
  DPRODUTO.csv
  DREFERENCIA.csv
  FORDEMPRODUCAO.csv
*)
  //Codigo         := ;
  //Controle       := ;
  //Nome           := ;
  //Tabela         := ;

  //
  Controle := UMyMod.BPGS1I32('ovplaytab', 'Controle', '', '', tsPos, SQLType, Controle);
  Result := UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'ovplaytab', False, [
  'Codigo', 'Nome', 'Tabela'], [
  'Controle'], [
  Codigo, Nome, Tabela], [
  Controle], True);
end;

procedure TFmImportaCSV_ERP_01.LeDiretorio(Dir: String; Acao: TAcaoCvsErpLaituraArq);
  //
const
  SubDir   = False;
  Extensao = '*.csv';
  LimpaAnterior = True;
  NaoLimpaAnterior = False;
var
  Arq, Tabela, Tempo, Caminho: String;
  I, Itens, AntIt, ic, n, CodLayout: Integer;
  DataGerou, DataAlter, DataAcess, MaiorDt: TDateTime;
begin
  FIniTick := GetTickCount;
  Screen.Cursor := crHourGlass;
  try
    FScanAborted  := False;
    MeAvisos.Text := '';
    //
    if not DirectoryExists(Dir) then
    begin
      Informa('N�o foi poss�vel localizar o diret�rio:' + sLineBreak +
      Dir);
      if Acao = TAcaoCvsErpLaituraArq.acelaLayout then
        Exit;
    end;
    Itens := 0;
    LBFound.Clear;
    MyObjects.LimpaGrade(GradeA, 1, 1, True);
    try
      dmkPF.GetAllFiles(SubDir, Dir + '\' + Extensao, LBFound, LimpaAnterior);
      Itens := LBFound.Items.Count;
      if Itens = 0 then
        Informa('N�o foi localizado nenhum arquivo v�lido no diret�rio:' +
        sLineBreak + Dir);
    except
      on E: Exception do
      begin
        Mensagem(TSvcMsgKind.smkGetErr, '[dmk 998] ' + E.Message);
        Exit;
      end;
    end;
////////////////////////////////////////////////////////////////////////////////
    if Acao = TAcaoCvsErpLaituraArq.acelaDados then
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(QrOVpDirXtr, Dmod.MyDB, [
      'SELECT Codigo, Nome ',
      'FROM ovpdirxtr ',
      'ORDER BY Nome ',
      '']);
      while not QrOVpDirXtr.Eof do
      begin
        Caminho := QrOVpDirXtrNome.Value;
        if not DirectoryExists(Caminho) then
        begin
          Informa('N�o foi poss�vel localizar o diret�rio:' + sLineBreak +
          Caminho);
          //Exit;
        end;
        AntIt := Itens;
        try
          dmkPF.GetAllFiles(SubDir, Caminho + '\' + Extensao, LBFound, NaoLimpaAnterior);
        except
          on E: Exception do
          begin
            Mensagem(TSvcMsgKind.smkGetErr, '[dmk 1025] ' + E.Message);
            Exit;
          end;
        end;
        Itens := LBFound.Items.Count - AntIt;
        if Itens = 0 then
          Informa('N�o foi localizado nenhum arquivo v�lido no diret�rio:' +
          sLineBreak + Caminho);
        //
        QrOVpDirXtr.Next;
      end;
    end;
////////////////////////////////////////////////////////////////////////////////
    if LBFound.Items.Count = 0 then
      Exit;
    ic := 0;
    for I := 0 to LBFound.Items.Count -1 do
    begin
      if FScanAborted then
      begin
         Screen.Cursor := crDefault;
         Exit;
      end;
      Arq := LBFound.Items[I];
      if ObtemDatasArquivo(Arq, DataGerou, DataAlter, DataAcess, MaiorDT) then
      begin
        ic := ic + 1;
        GradeA.RowCount := ic + 1;
        GradeA.Cells[0, ic] := IntToStr(ic);
        GradeA.Cells[1, ic] := ExtractFileName(Arq);
        GradeA.Cells[2, ic] := Geral.FDT(DataGerou, 109);
        GradeA.Cells[3, ic] := Geral.FDT(DataAlter, 109);
        GradeA.Cells[4, ic] := Geral.FDT(DataAcess, 109);
      end;
    end;

    ic := 0;
    CodLayout := 0;  // n�o tirar daqui! usa valor no loop apos setar <> de 0!!
    for I := 0 to LBFound.Items.Count -1 do
    begin
      if FScanAborted then
      begin
         Screen.Cursor := crDefault;
         Exit;
      end;
      Arq := LBFound.Items[I];
      case Acao of
        TAcaoCvsErpLaituraArq.acelaLayout:
        begin
          Tabela    := '';
          CarregaArquivoParaLayout(CodLayout, Arq, Tabela);
        end;
        TAcaoCvsErpLaituraArq.acelaDados:
        begin
          Tabela    := '';
          if ObtemDatasArquivo(Arq, DataGerou, DataAlter, DataAcess, MaiorDT) then
            CarregaArquivoParaMovimento(Arq, MaiorDt);
        end;
      end;
    end;
  finally
    Screen.Cursor := crDefault;
  end;
end;

function TFmImportaCSV_ERP_01.LeLinhaArq(const Linha: String; var LstLin:
  TStringList): Boolean;
var
  P, N: Integer;
begin
  Result := False;
  N := 0;
  P := pos(';', Linha);
  if P > 0 then
  begin
    lstLin := TStringList.Create;
    try
      //N := Geral.MyExtractStrings([';'], [' '], PChar(Linha + ';'), lstLin);
      LstLin := Geral.Explode3(Linha, ';');
      Result := LstLin.Count > 1;
      (*
      CodTxt  := Copy(Linha, 1, P-1);
      if lstLin.Count > 0 then
        Nome    := lstLin[00];
      if lstLin.Count > 1 then
        DataIni := lstLin[01];
      if lstLin.Count > 2 then
        DataFim := lstLin[02];
      *)
    finally
      //if lstLin <> nil then
        FreeAndNil(lstLin);//.Free;
    end;
  end;
end;

procedure TFmImportaCSV_ERP_01.Mensagem(SvcMsgKind: TSvcMsgKind; Msg: String);
begin
  if VAR_RUN_AS_SVC then
    ProjGroup_PF.SalvaEmArquivoLog(TSvcMsgKind.smkGetErr, Msg)
  else
    Geral.MB_Aviso(Msg);
end;

procedure TFmImportaCSV_ERP_01.MostraTempoTranscorrido;
var
  Tempo: String;
begin
  FFimTick := GetTickCount;
  FDifTick := FFimTick - FIniTick;
  Tempo := FormatDateTime('hh:nn:ss:zzz', FDifTick / 86400000);
  EdTempoCarrega.Text := Tempo;
  MyObjects.Informa2(LaAviso1, LaAviso2, False, Tempo);
end;


function TFmImportaCSV_ERP_01.ObtemDatasArquivo(const Arquivo: String; var
  CreateDT, AccessDT, ModifyDT, MaiorDt: TDateTime): Boolean;
var
  SR: TSearchRec;
  NomeArq: String;
  Ultima: TDateTime;
begin
  Result := False;
  if FindFirst(Arquivo, faAnyFile, SR) = 0 then
  begin
    Result   := CkForcaRecarga.Checked;
    CreateDT := FileTimeToDTime(SR.FindData.ftCreationTime);
    AccessDT := FileTimeToDTime(SR.FindData.ftLastAccessTime);
    ModifyDT := FileTimeToDTime(SR.FindData.ftLastWriteTime);
    //
    MaiorDt  := CreateDT;
    if AccessDT > MaiorDt  then
      MaiorDt  := AccessDT;
    if ModifyDT > MaiorDt  then
      MaiorDt  := ModifyDT;

    NomeArq  := ExtractFileName(Arquivo);
    //
    UnDMkDAC_PF.AbreMySQLQuery0(QrLL, Dmod.MyDB, [
    'SELECT Controle, Nome, Tabela, LastLoad  ',
    'FROM ovplaytab ',
    'WHERE Codigo=' + Geral.FF0(FCodLayout),
    'AND Nome="' + NomeArq + '" ',
    '']);
    //
    Ultima := QrLL.FieldByName('LastLoad').AsDateTime;
    Ultima := Ultima + 0.0007; // 1,008 minutos
    if (MaiorDT > Ultima) then
      Result := True;
(*
    G e r a l . M B _ I n f o('Created: ' + DateTimeToStr(CreateDT) +
      ' Accessed: ' + DateTimeToStr(AccessDT) +
      ' Modified: ' + DateTimeToStr(ModifyDT));
*)
  end
  else
    Informa('[ODA] Arquivo n�o localizado: ' + sLineBreak +
    Arquivo);
  //
  FindClose(SR);
end;

function TFmImportaCSV_ERP_01.PossivelRastreioEmAndamento(): Boolean;
var
  Agora, Ini, Fim: TDateTime;
  OK: Boolean;
  Ini_TXT: String;
begin
  Result := False;
  UnDMkDAC_PF.AbreMySQLQuery0(QrAux, Dmod.MyDB, [
  'SELECT DtHrIniRastreio, DtHrFimRastreio',
  'FROM opcoesapp ',
  'WHERE Codigo=1 ',
  EmptyStr]);
  Ini := QrAux.FieldByName('DtHrIniRastreio').AsDateTime;
  Fim := QrAux.FieldByName('DtHrFimRastreio').AsDateTime;
  //
  Agora := DModG.ObtemAgora();
  //
  if Fim > 2 then
    OK := True
  else if Agora - Ini > 0.0416666 (* 1 hora *) then
    OK := True
  else if (Ini < 2) and (Fim < 2) (* primeira vez *) then
    OK := True
  else
    OK := False;
  //
  if OK then
  begin
    Ini_TXT := Geral.FDT(Agora, 109);
    UnDMkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
    'UPDATE opcoesapp ',
    'SET DtHrIniRastreio="' + Ini_TXT +
    '", DtHrFimRastreio="0000-00-00 00:00:00"',
    'WHERE Codigo=1 ',
    EmptyStr]);
  end;
  Result := not OK;
end;

procedure TFmImportaCSV_ERP_01.ReaberturaDeItensEncerrados();
var
  FldAtivXxx, Corda, CampoBase, MarcadorOld, MarcadorNew: String;
  L, AtvAntes, AtvDepois, AtvAfRw: Integer;
begin
  if (not VAR_RUN_AS_SVC) and (CkForcaReabertura.Checked = False) then
    Exit;
////////////////////////////////////////////////////////////////////////////////
  //Result := 0;
  //
  UnDMkDAC_PF.AbreMySQLQuery0(QrAux, Dmod.MyDB, [
  'SELECT COUNT(*) Itens',
  'FROM ovgispgercab ',
  'WHERE ZtatusIsp=' + Geral.FF0(CO_OVS_IMPORT_ALHEIO_5120_APTO_DOWNMOBILE),
  EmptyStr]);
  AtvAntes := QrAux.FieldByName('Itens').AsInteger;
  //
  //
  if VAR_RUN_AS_SVC then
    FldAtivXxx := 'AtivAut'
  else
    FldAtivXxx := 'AtivMan';
  //
  MarcadorNew := '0';
  Dmod.MyDB.Execute('UPDATE ovgispgercab SET ' + FldAtivXxx + '=' + MarcadorNew);
  //
////////////////////////////////////////////////////////////////////////////////
///   Marcar os reduzidos de OP (NrReduzidoOP) ativos.
////////////////////////////////////////////////////////////////////////////////
///
  CampoBase := 'NrReduzidoOP';
  MarcadorOld  := '0';
  MarcadorNew  := '1';
  UnDMkDAC_PF.AbreMySQLDirectQuery0(DqAux, Dmod.MyDB, [
  'SELECT DISTINCT ' + CampoBase,
  'FROM ovmordemproducao ',
  '']);
  if DqAux.RecordCount > 0 then
  begin
    L := 0;
    Corda := '';
    while not DqAux.Eof do
    begin
      L := L + 1;
      Corda := Corda + DqAux.FieldValueByFieldName(CampoBase) + ', ';
      if L >= VAR_LoadCSVRowsBlc then
      begin
        Corda := Copy(Corda, 1, Length(Corda) - 2);
        Dmod.MyDB.Execute('UPDATE ovgispgercab SET ' + FldAtivXxx + '=' +
          MarcadorNew + ' WHERE ' + CampoBase + ' IN (' + Corda + ')');
        L := 0;
        Corda := '';
      end;
      //
      DqAux.Next;
    end;
    if Length(Corda) > 2 then
    begin
      Corda := Copy(Corda, 1, Length(Corda) - 2);
      Dmod.MyDB.Execute('UPDATE ovgispgercab SET ' + FldAtivXxx + '=' +
        MarcadorNew + ' WHERE ' + CampoBase + ' IN (' + Corda + ')');
      L := 0;
      Corda := '';
    end;
  end;
////////////////////////////////////////////////////////////////////////////////
///   Marcar os artigos ativos nos reduzidos de OP marcados como ativos.
////////////////////////////////////////////////////////////////////////////////
  CampoBase := 'SeqGrupo';
  MarcadorOld  := '1';
  MarcadorNew  := '3';
  UnDMkDAC_PF.AbreMySQLDirectQuery0(DqAux, Dmod.MyDB, [
  'SELECT DISTINCT ' + CampoBase,
  'FROM ovmordemproducao ',
  '']);
  if DqAux.RecordCount > 0 then
  begin
    L := 0;
    Corda := '';
    while not DqAux.Eof do
    begin
      L := L + 1;
      Corda := Corda + DqAux.FieldValueByFieldName(CampoBase) + ', ';
      if L >= VAR_LoadCSVRowsBlc then
      begin
        Corda := Copy(Corda, 1, Length(Corda) - 2);
        Dmod.MyDB.Execute('UPDATE ovgispgercab SET ' + FldAtivXxx + '=' +
          MarcadorNew + ' WHERE ' + CampoBase + ' IN (' + Corda + ') AND ' +
          FldAtivXxx + '=' + MarcadorOld);
        L := 0;
        Corda := '';
      end;
      //
      DqAux.Next;
    end;
    if Length(Corda) > 2 then
    begin
      Corda := Copy(Corda, 1, Length(Corda) - 2);
        Dmod.MyDB.Execute('UPDATE ovgispgercab SET ' + FldAtivXxx + '=' +
          MarcadorNew + ' WHERE ' + CampoBase + ' IN (' + Corda + ') AND ' +
          FldAtivXxx + '=' + MarcadorOld);
      L := 0;
      Corda := '';
    end;
  end;
////////////////////////////////////////////////////////////////////////////////
///   Marcar os Locais ativos nos artigos/reduzidos de OP marcados como ativos.
////////////////////////////////////////////////////////////////////////////////
  CampoBase := 'Local';
  MarcadorOld  := '3';
  MarcadorNew  := '7';
  UnDMkDAC_PF.AbreMySQLDirectQuery0(DqAux, Dmod.MyDB, [
  'SELECT DISTINCT ' + CampoBase,
  'FROM ovmordemproducao ',
  '']);
  if DqAux.RecordCount > 0 then
  begin
    L := 0;
    Corda := '';
    while not DqAux.Eof do
    begin
      L := L + 1;
      Corda := Corda + DqAux.FieldValueByFieldName(CampoBase) + ', ';
      if L >= VAR_LoadCSVRowsBlc then
      begin
        Corda := Copy(Corda, 1, Length(Corda) - 2);
        Dmod.MyDB.Execute('UPDATE ovgispgercab SET ' + FldAtivXxx + '=' +
          MarcadorNew + ' WHERE ' + CampoBase + ' IN (' + Corda + ') AND ' +
          FldAtivXxx + '=' + MarcadorOld);
        L := 0;
        Corda := '';
      end;
      //
      DqAux.Next;
    end;
    if Length(Corda) > 2 then
    begin
      Corda := Copy(Corda, 1, Length(Corda) - 2);
        Dmod.MyDB.Execute('UPDATE ovgispgercab SET ' + FldAtivXxx + '=' +
          MarcadorNew + ' WHERE ' + CampoBase + ' IN (' + Corda + ') AND ' +
          FldAtivXxx + '=' + MarcadorOld);
      L := 0;
      Corda := '';
    end;
  end;
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
///   Ativar os Locais/artigos/reduzidos de OP marcados como ativos.
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
  Dmod.MyDB.Execute('UPDATE ovgispgercab SET ZtatusIsp=' +
    Geral.FF0(CO_OVS_IMPORT_ALHEIO_5120_APTO_DOWNMOBILE) +
    ', DtHrFecha="0000-00-00 00:00:00" WHERE ' +
    FldAtivXxx + '=' + MarcadorNew);
  //
  UnDMkDAC_PF.AbreMySQLQuery0(QrAux, Dmod.MyDB, [
  'SELECT COUNT(*) Itens',
  'FROM ovgispgercab ',
  'WHERE ZtatusIsp=' + Geral.FF0(CO_OVS_IMPORT_ALHEIO_5120_APTO_DOWNMOBILE),
  EmptyStr]);
  AtvDepois := QrAux.FieldByName('Itens').AsInteger;
  //
  AtvAfRw := AtvDepois - AtvAntes;
  //
  if AtvAfRw <> 0 then
    Mensagem(TSvcMsgKind.smkRunOK, 'Itens reabertos que estavam encerrados: ' +
    Geral.FF0(AtvAfRw));
end;

procedure TFmImportaCSV_ERP_01.ReopenOvpLayEsq();
begin
  UnDmkDAC_PF.AbreQuery(QrOVpLayEsq, Dmod.MyDB);
end;

procedure TFmImportaCSV_ERP_01.SbLoadCSVOthDirClick(Sender: TObject);
var
  IniPath, SelPath: String;
begin
  IniPath := ExtractFilePath(EdLoadCSVOthDir.Text);
  if MyObjects.FileOpenDialog(Self, IniPath, '', 'Selecione o Diret�rio', '',
  [], SelPath) then
    EdLoadCSVOthDir.Text := ExtractFileDir(SelPath);
end;


(*
DROP TABLE ovdpessoas;
DROP TABLE ovdciclo;
DROP TABLE ovdclaslocal;
DROP TABLE ovdempresa;
DROP TABLE ovdlocal;
DROP TABLE ovdlote;
DROP TABLE ovdproduto;
DROP TABLE ovdreferencia;
DROP TABLE ovfordemproducao;
*)

procedure TFmImportaCSV_ERP_01.SbLoadLayOutDirClick(Sender: TObject);
var
  IniPath, SelPath: String;
begin
  IniPath := ExtractFilePath(EdLoadLayOutDir.Text);
  if MyObjects.FileOpenDialog(Self, IniPath, '', 'Selecione o Diret�rio', '',
  [], SelPath) then
    EdLoadLayOutDir.Text := ExtractFileDir(SelPath);
end;

 //http://delphiprogrammingdiary.blogspot.com/2018/08/memory-leak-in-delphi-and-how-to-avoid.html


end.
