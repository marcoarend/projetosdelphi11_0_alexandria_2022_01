unit OVcYnsARQCad;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2, dmkGeral,
  ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt, UMySQLModule, Menus,
  UnInternalConsts2, mySQLDbTables, UnMySQLCuringa, dmkPermissoes, dmkEdit,
  dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup, Grids, DBGrids,
  UnDmkProcFunc, UnDmkENums;

type
  TFmOVcYnsARQCad = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    Label9: TLabel;
    EdNome: TdmkEdit;
    Label1: TLabel;
    DBEdCodigo: TdmkDBEdit;
    Label2: TLabel;
    DBEdNome: TdmkDBEdit;
    QrOVcYnsARQCad: TMySQLQuery;
    DsOVcYnsARQCad: TDataSource;
    QrOVcYnsARQIts: TMySQLQuery;
    DsOVcYnsARQIts: TDataSource;
    PMIts: TPopupMenu;
    ItsInclui1: TMenuItem;
    ItsExclui1: TMenuItem;
    ItsAltera1: TMenuItem;
    PMCab: TPopupMenu;
    CabInclui1: TMenuItem;
    CabAltera1: TMenuItem;
    CabExclui1: TMenuItem;
    BtCab: TBitBtn;
    BtIts: TBitBtn;
    DGDados: TDBGrid;
    GroupBox1: TGroupBox;
    Panel6: TPanel;
    DBEdit3: TDBEdit;
    DBEdit4: TDBEdit;
    Label6: TLabel;
    Label5: TLabel;
    DBEdit2: TDBEdit;
    Label4: TLabel;
    DBEdit1: TDBEdit;
    Label3: TLabel;
    GroupBox2: TGroupBox;
    Panel7: TPanel;
    DBEdit5: TDBEdit;
    DBEdit7: TDBEdit;
    Label11: TLabel;
    DBEdit6: TDBEdit;
    Label10: TLabel;
    Label8: TLabel;
    QrOVcYnsARQCadCodigo: TIntegerField;
    QrOVcYnsARQCadNome: TWideStringField;
    QrOVcYnsARQCadEsquema: TWideStringField;
    QrOVcYnsARQCadNivel: TWideStringField;
    QrOVcYnsARQCadLMQR: TWideStringField;
    QrOVcYnsARQCadNQA: TFloatField;
    QrOVcYnsARQCadPontosTole: TFloatField;
    QrOVcYnsARQCadPontosGrav: TFloatField;
    QrOVcYnsARQCadPontosCrit: TFloatField;
    QrOVcYnsARQCadLk: TIntegerField;
    QrOVcYnsARQCadDataCad: TDateField;
    QrOVcYnsARQCadDataAlt: TDateField;
    QrOVcYnsARQCadUserCad: TIntegerField;
    QrOVcYnsARQCadUserAlt: TIntegerField;
    QrOVcYnsARQCadAlterWeb: TSmallintField;
    QrOVcYnsARQCadAWServerID: TIntegerField;
    QrOVcYnsARQCadAWStatSinc: TSmallintField;
    QrOVcYnsARQCadAtivo: TSmallintField;
    QrOVcYnsARQItsCodigo: TIntegerField;
    QrOVcYnsARQItsControle: TIntegerField;
    QrOVcYnsARQItsQtdPcIni: TIntegerField;
    QrOVcYnsARQItsQtdPcFim: TIntegerField;
    QrOVcYnsARQItsQtdAmostr: TIntegerField;
    QrOVcYnsARQItsPtsAprova: TFloatField;
    QrOVcYnsARQItsPtsResalv: TFloatField;
    QrOVcYnsARQItsPtsRejeit: TFloatField;
    QrOVcYnsARQItsLk: TIntegerField;
    QrOVcYnsARQItsDataCad: TDateField;
    QrOVcYnsARQItsDataAlt: TDateField;
    QrOVcYnsARQItsUserCad: TIntegerField;
    QrOVcYnsARQItsUserAlt: TIntegerField;
    QrOVcYnsARQItsAlterWeb: TSmallintField;
    QrOVcYnsARQItsAWServerID: TIntegerField;
    QrOVcYnsARQItsAWStatSinc: TSmallintField;
    QrOVcYnsARQItsAtivo: TSmallintField;
    GroupBox3: TGroupBox;
    Panel8: TPanel;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    GroupBox4: TGroupBox;
    Panel9: TPanel;
    Label16: TLabel;
    Label17: TLabel;
    Label18: TLabel;
    EdEsquema: TdmkEdit;
    EDNivel: TdmkEdit;
    EdLMQR: TdmkEdit;
    EdNQA: TdmkEdit;
    EdPontosTole: TdmkEdit;
    EdPontosGrav: TdmkEdit;
    EdPontosCrit: TdmkEdit;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrOVcYnsARQCadAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrOVcYnsARQCadBeforeOpen(DataSet: TDataSet);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure QrOVcYnsARQCadAfterScroll(DataSet: TDataSet);
    procedure CabInclui1Click(Sender: TObject);
    procedure CabAltera1Click(Sender: TObject);
    procedure CabExclui1Click(Sender: TObject);
    procedure BtCabClick(Sender: TObject);
    procedure BtItsClick(Sender: TObject);
    procedure ItsInclui1Click(Sender: TObject);
    procedure ItsExclui1Click(Sender: TObject);
    procedure ItsAltera1Click(Sender: TObject);
    procedure PMCabPopup(Sender: TObject);
    procedure PMItsPopup(Sender: TObject);
    procedure QrOVcYnsARQCadBeforeClose(DataSet: TDataSet);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure LocCod(Atual, Codigo: Integer);
    procedure Va(Para: TVaiPara);
    procedure MostraOVcYnsARQIts(SQLType: TSQLType);

  public
    { Public declarations }
    FSeq, FCabIni: Integer;
    FLocIni: Boolean;
    //
    procedure ReopenOVcYnsARQIts(Controle: Integer);

  end;

var
  FmOVcYnsARQCad: TFmOVcYnsARQCad;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, MyDBCheck, DmkDAC_PF, OVcYnsARQIts;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmOVcYnsARQCad.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmOVcYnsARQCad.MostraOVcYnsARQIts(SQLType: TSQLType);
begin
  if DBCheck.CriaFm(TFmOVcYnsARQIts, FmOVcYnsARQIts, afmoNegarComAviso) then
  begin
    FmOVcYnsARQIts.ImgTipo.SQLType := SQLType;
    FmOVcYnsARQIts.FQrCab := QrOVcYnsARQCad;
    FmOVcYnsARQIts.FDsCab := DsOVcYnsARQCad;
    FmOVcYnsARQIts.FQrIts := QrOVcYnsARQIts;
    if SQLType = stIns then
      //
    else
    begin
      FmOVcYnsARQIts.EdControle.ValueVariant := QrOVcYnsARQItsControle.Value;
      //
      FmOVcYnsARQIts.EdQtdPcIni.ValueVariant  := QrOVcYnsARQItsQtdPcIni.Value;
      FmOVcYnsARQIts.EdQtdPcFim.ValueVariant  := QrOVcYnsARQItsQtdPcFim.Value;
      FmOVcYnsARQIts.EdQtdAmostr.ValueVariant := QrOVcYnsARQItsQtdAmostr.Value;
      FmOVcYnsARQIts.EdPtsAprova.ValueVariant := QrOVcYnsARQItsPtsAprova.Value;
      FmOVcYnsARQIts.EdPtsResalv.ValueVariant := QrOVcYnsARQItsPtsResalv.Value;
      FmOVcYnsARQIts.EdPtsRejeit.ValueVariant := QrOVcYnsARQItsPtsRejeit.Value;
    end;
    FmOVcYnsARQIts.ShowModal;
    FmOVcYnsARQIts.Destroy;
  end;
end;

procedure TFmOVcYnsARQCad.PMCabPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemCabUpd(CabAltera1, QrOVcYnsARQCad);
  MyObjects.HabilitaMenuItemCabDel(CabExclui1, QrOVcYnsARQCad, QrOVcYnsARQIts);
end;

procedure TFmOVcYnsARQCad.PMItsPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(ItsInclui1, QrOVcYnsARQCad);
  MyObjects.HabilitaMenuItemItsUpd(ItsAltera1, QrOVcYnsARQIts);
  MyObjects.HabilitaMenuItemItsDel(ItsExclui1, QrOVcYnsARQIts);
end;

procedure TFmOVcYnsARQCad.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrOVcYnsARQCadCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmOVcYnsARQCad.DefParams;
begin
  VAR_GOTOTABELA := 'ovcynsarqcad';
  VAR_GOTOMYSQLTABLE := QrOVcYnsARQCad;
  VAR_GOTONEG := gotoPiZ;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT *');
  VAR_SQLx.Add('FROM ovcynsarqcad');
  VAR_SQLx.Add('WHERE Codigo > 0');
  //
  VAR_SQL1.Add('AND Codigo=:P0');
  //
  //VAR_SQL2.Add('AND CodUsu=:P0');
  //
  VAR_SQLa.Add('AND Nome Like :P0');
  //
end;

procedure TFmOVcYnsARQCad.ItsAltera1Click(Sender: TObject);
begin
  MostraOVcYnsARQIts(stUpd);
end;

procedure TFmOVcYnsARQCad.CabExclui1Click(Sender: TObject);
begin
  Geral.MB_Info('A��o n�o implementada! Solicite � Dermatek:' + sLineBreak +
  Caption + sLineBreak + TMenuItem(Sender).Name);
end;

procedure TFmOVcYnsARQCad.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmOVcYnsARQCad.QueryPrincipalAfterOpen;
begin
end;

procedure TFmOVcYnsARQCad.ItsExclui1Click(Sender: TObject);
var
  Controle: Integer;
begin
  if UMyMod.ExcluiRegistroInt1('Confirma a exclus�o do item selecionado?',
  'OVcYnsARQIts', 'Controle', QrOVcYnsARQItsControle.Value, Dmod.MyDB) = ID_YES then
  begin
    Controle := GOTOy.LocalizaPriorNextIntQr(QrOVcYnsARQIts,
      QrOVcYnsARQItsControle, QrOVcYnsARQItsControle.Value);
    ReopenOVcYnsARQIts(Controle);
  end;
end;

procedure TFmOVcYnsARQCad.ReopenOVcYnsARQIts(Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrOVcYnsARQIts, Dmod.MyDB, [
  'SELECT * ',
  'FROM ovcynsarqits ',
  'WHERE Codigo=' + Geral.FF0(QrOVcYnsARQCadCodigo.Value),
  '']);
  //
  QrOVcYnsARQIts.Locate('Controle', Controle, []);
end;

procedure TFmOVcYnsARQCad.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmOVcYnsARQCad.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmOVcYnsARQCad.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmOVcYnsARQCad.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmOVcYnsARQCad.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmOVcYnsARQCad.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmOVcYnsARQCad.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrOVcYnsARQCadCodigo.Value;
  Close;
end;

procedure TFmOVcYnsARQCad.ItsInclui1Click(Sender: TObject);
begin
  MostraOVcYnsARQIts(stIns);
end;

procedure TFmOVcYnsARQCad.CabAltera1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrOVcYnsARQCad, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'ovcynsarqcad');
end;

procedure TFmOVcYnsARQCad.BtConfirmaClick(Sender: TObject);
var
  Nome, Esquema, Nivel, LMQR: String;
  Codigo: Integer;
  NQA, PontosTole, PontosGrav, PontosCrit: Double;
  SQLType: TSQLType;
begin
  SQLType        := ImgTipo.SQLType;
  Codigo         := EdCodigo.ValueVariant;
  Nome           := EdNome.ValueVariant;
  Esquema        := EdEsquema.ValueVariant;
  Nivel          := EdNivel.ValueVariant;
  LMQR           := EdLMQR.ValueVariant;
  NQA            := EdNQA.ValueVariant;
  PontosTole     := EdPontosTole.ValueVariant;
  PontosGrav     := EdPontosGrav.ValueVariant;
  PontosCrit     := EdPontosCrit.ValueVariant;
  //
  if MyObjects.FIC(Length(Nome) = 0, EdNome, 'Defina uma descri��o!') then Exit;
  //
  //
  Codigo := UMyMod.BPGS1I32('ovcynsarqcad', 'Codigo', '', '', tsPos, SQLType, Codigo);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'ovcynsarqcad', False, [
  'Nome', 'Esquema', 'Nivel',
  'LMQR', 'NQA', 'PontosTole',
  'PontosGrav', 'PontosCrit'], [
  'Codigo'], [
  Nome, Esquema, Nivel,
  LMQR, NQA, PontosTole,
  PontosGrav, PontosCrit], [
  Codigo], True) then
  begin
    ImgTipo.SQLType := stLok;
    PnDados.Visible := True;
    PnEdita.Visible := False;
    GOTOy.BotoesSb(ImgTipo.SQLType);
    //
    LocCod(Codigo, Codigo);
    if FSeq = 1 then Close;
  end;
end;

procedure TFmOVcYnsARQCad.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := EdCodigo.ValueVariant;
  if ImgTipo.SQLType = stIns then
    UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'ovcynsarqcad', Codigo);
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'ovcynsarqcad', 'Codigo');
end;

procedure TFmOVcYnsARQCad.BtItsClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMIts, BtIts);
end;

procedure TFmOVcYnsARQCad.BtCabClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMCab, BtCab);
end;

procedure TFmOVcYnsARQCad.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  GBEdita.Align := alClient;
  DGDados.Align := alClient;
  CriaOForm;
  FSeq := 0;
end;

procedure TFmOVcYnsARQCad.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrOVcYnsARQCadCodigo.Value, LaRegistro.Caption);
end;

procedure TFmOVcYnsARQCad.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmOVcYnsARQCad.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrOVcYnsARQCadCodigo.Value, LaRegistro.Caption);
end;

procedure TFmOVcYnsARQCad.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmOVcYnsARQCad.QrOVcYnsARQCadAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmOVcYnsARQCad.QrOVcYnsARQCadAfterScroll(DataSet: TDataSet);
begin
  ReopenOVcYnsARQIts(0);
end;

procedure TFmOVcYnsARQCad.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //
  if FSeq = 1 then
    CabInclui1Click(Self)
  else
  if (not FLocIni) and (FCabIni <> 0)  then
  begin
    LocCod(FCabIni, FCabIni);
    if QrOVcYnsARQCadCodigo.Value <> FCabIni then
      Geral.MB_Aviso('Grupo n�o localizado!');
    FLocIni := True;
  end;
end;

procedure TFmOVcYnsARQCad.SbQueryClick(Sender: TObject);
begin
  LocCod(QrOVcYnsARQCadCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'ovcynsarqcad', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmOVcYnsARQCad.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmOVcYnsARQCad.CabInclui1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrOVcYnsARQCad, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'ovcynsarqcad');
end;

procedure TFmOVcYnsARQCad.QrOVcYnsARQCadBeforeClose(
  DataSet: TDataSet);
begin
  QrOVcYnsARQIts.Close;
end;

procedure TFmOVcYnsARQCad.QrOVcYnsARQCadBeforeOpen(DataSet: TDataSet);
begin
  QrOVcYnsARQCadCodigo.DisplayFormat := FFormatFloat;
end;

end.

