object FmOVcYnsExgTop: TFmOVcYnsExgTop
  Left = 339
  Top = 185
  Caption = 'OVS-EXGCS-004 :: T'#243'pico de Tabela de Exa'#231#227'o'
  ClientHeight = 456
  ClientWidth = 689
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 317
    Width = 689
    Height = 25
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 2
    object CkContinuar: TCheckBox
      Left = 16
      Top = 4
      Width = 117
      Height = 17
      Caption = 'Continuar inserindo.'
      Checked = True
      State = cbChecked
      TabOrder = 0
    end
  end
  object GroupBox1: TGroupBox
    Left = 0
    Top = 48
    Width = 689
    Height = 69
    Align = alTop
    Caption = ' Dados do cabe'#231'alho:'
    Enabled = False
    TabOrder = 0
    object Label5: TLabel
      Left = 12
      Top = 20
      Width = 14
      Height = 13
      Caption = 'ID:'
      FocusControl = DBEdCodigo
    end
    object Label3: TLabel
      Left = 72
      Top = 20
      Width = 51
      Height = 13
      Caption = 'Descri'#231#227'o:'
      FocusControl = DBEdNome
    end
    object DBEdCodigo: TdmkDBEdit
      Left = 12
      Top = 36
      Width = 56
      Height = 21
      TabStop = False
      DataField = 'Codigo'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ReadOnly = True
      ShowHint = True
      TabOrder = 0
      UpdCampo = 'Codigo'
      UpdType = utYes
      Alignment = taRightJustify
    end
    object DBEdNome: TDBEdit
      Left = 72
      Top = 36
      Width = 605
      Height = 21
      TabStop = False
      Color = clWhite
      DataField = 'Nome'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
    end
  end
  object GroupBox2: TGroupBox
    Left = 0
    Top = 117
    Width = 689
    Height = 200
    Align = alTop
    Caption = ' Dados do item: '
    TabOrder = 1
    object Panel3: TPanel
      Left = 2
      Top = 15
      Width = 685
      Height = 86
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object Label6: TLabel
        Left = 12
        Top = 4
        Width = 14
        Height = 13
        Caption = 'ID:'
      end
      object Label7: TLabel
        Left = 96
        Top = 4
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
        Enabled = False
        Visible = False
      end
      object Label1: TLabel
        Left = 12
        Top = 44
        Width = 42
        Height = 13
        Caption = 'T'#243'pico:  '
      end
      object SbTobiko: TSpeedButton
        Left = 660
        Top = 60
        Width = 21
        Height = 21
        Caption = '...'
        OnClick = SbTobikoClick
      end
      object EdControle: TdmkEdit
        Left = 12
        Top = 20
        Width = 80
        Height = 21
        TabStop = False
        Alignment = taRightJustify
        Enabled = False
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Controle'
        UpdCampo = 'Controle'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdNome: TdmkEdit
        Left = 96
        Top = 20
        Width = 581
        Height = 21
        Enabled = False
        TabOrder = 1
        Visible = False
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdTopyko: TdmkEditCB
        Left = 12
        Top = 60
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 2
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Topico'
        UpdCampo = 'Topico'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnKeyUp = EdTopykoKeyUp
        OnRedefinido = EdTopykoRedefinido
        DBLookupComboBox = CBTopyko
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBTopyko: TdmkDBLookupComboBox
        Left = 72
        Top = 60
        Width = 585
        Height = 21
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsOVcYnsMixTop
        TabOrder = 3
        OnKeyUp = CBTopykoKeyUp
        dmkEditCB = EdTopyko
        QryCampo = 'Topico'
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
    end
    object Panel5: TPanel
      Left = 2
      Top = 101
      Width = 685
      Height = 48
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 1
      object Panel7: TPanel
        Left = 0
        Top = 0
        Width = 144
        Height = 48
        Align = alLeft
        BevelOuter = bvNone
        TabOrder = 0
        object CkMensuravl: TdmkCheckBox
          Left = 12
          Top = 12
          Width = 97
          Height = 17
          Caption = 'Mensur'#225'vel?'
          TabOrder = 0
          OnClick = CkMensuravlClick
          QryCampo = 'Mensuravl'
          UpdCampo = 'Mensuravl'
          UpdType = utYes
          ValCheck = #0
          ValUncheck = #0
          OldValor = #0
        end
      end
      object PnMargem: TPanel
        Left = 144
        Top = 0
        Width = 541
        Height = 48
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 1
        Visible = False
        object SpeedButton5: TSpeedButton
          Left = 164
          Top = 19
          Width = 23
          Height = 23
          Caption = '?'
        end
        object Label8: TLabel
          Left = 32
          Top = 4
          Width = 119
          Height = 13
          Caption = 'Sigla unidade de medida:'
        end
        object EdTolerSglUnMdid: TdmkEdit
          Left = 32
          Top = 20
          Width = 129
          Height = 21
          MaxLength = 20
          TabOrder = 0
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          QryCampo = 'TolerSglUnMdid'
          UpdCampo = 'TolerSglUnMdid'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object CkTlrRndUsaMax: TdmkCheckBox
          Left = 364
          Top = 0
          Width = 165
          Height = 17
          Caption = 'Margem m'#225'xima medida  ou %:'
          TabOrder = 1
          OnClick = CkTlrRndUsaMaxClick
          QryCampo = 'TlrRndUsaMax'
          UpdCampo = 'TlrRndUsaMax'
          UpdType = utYes
          ValCheck = '1'
          ValUncheck = '0'
          OldValor = #0
        end
        object EdTlrRndPrcMax: TdmkEdit
          Left = 364
          Top = 20
          Width = 164
          Height = 21
          Alignment = taRightJustify
          TabOrder = 2
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 3
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,000'
          QryCampo = 'TlrRndPrcMax'
          UpdCampo = 'TlrRndPrcMax'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
          OnRedefinido = EdTlrRndPrcMaxRedefinido
        end
        object EdTlrRndPrcMin: TdmkEdit
          Left = 192
          Top = 20
          Width = 164
          Height = 21
          Alignment = taRightJustify
          TabOrder = 3
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 3
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,000'
          QryCampo = 'TlrRndPrcMin'
          UpdCampo = 'TlrRndPrcMin'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
          OnRedefinido = EdTlrRndPrcMinRedefinido
        end
        object CkTlrRndUsaMin: TdmkCheckBox
          Left = 192
          Top = 0
          Width = 165
          Height = 17
          Caption = 'Margem m'#237'nima medida  ou %:'
          TabOrder = 4
          OnClick = CkTlrRndUsaMinClick
          QryCampo = 'TlrRndUsaMin'
          UpdCampo = 'TlrRndUsaMin'
          UpdType = utYes
          ValCheck = '1'
          ValUncheck = '0'
          OldValor = #0
        end
      end
    end
    object PnMedida: TPanel
      Left = 2
      Top = 149
      Width = 685
      Height = 44
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 2
      Visible = False
      object Label2: TLabel
        Left = 176
        Top = 4
        Width = 65
        Height = 13
        Caption = 'Medida certa:'
      end
      object Label4: TLabel
        Left = 336
        Top = 4
        Width = 75
        Height = 13
        Caption = 'Medida m'#237'nima:'
      end
      object Label9: TLabel
        Left = 508
        Top = 4
        Width = 76
        Height = 13
        Caption = 'Medida m'#225'xima:'
      end
      object RGTolerBasCalc: TdmkRadioGroup
        Left = 8
        Top = 0
        Width = 161
        Height = 43
        Caption = ' Base de c'#225'lculo: '
        Columns = 2
        ItemIndex = 0
        Items.Strings = (
          'Medida'
          'Percentual')
        TabOrder = 0
        OnClick = RGTolerBasCalcClick
        QryCampo = 'TolerBasCalc'
        UpdCampo = 'TolerBasCalc'
        UpdType = utYes
        OldValor = 0
      end
      object EdMedidCer: TdmkEdit
        Left = 176
        Top = 20
        Width = 153
        Height = 21
        Alignment = taRightJustify
        TabOrder = 1
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 3
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,000'
        QryCampo = 'MedidCer'
        UpdCampo = 'MedidCer'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
        OnRedefinido = EdMedidCerRedefinido
      end
      object EdMedidMin: TdmkEdit
        Left = 336
        Top = 20
        Width = 164
        Height = 21
        Alignment = taRightJustify
        TabOrder = 2
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 3
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,000'
        QryCampo = 'MedidMin'
        UpdCampo = 'MedidMin'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdMedidMax: TdmkEdit
        Left = 508
        Top = 20
        Width = 164
        Height = 21
        Alignment = taRightJustify
        TabOrder = 3
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 3
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,000'
        QryCampo = 'MedidMax'
        UpdCampo = 'MedidMax'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 689
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 3
    object GB_R: TGroupBox
      Left = 641
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 593
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 341
        Height = 32
        Caption = 'T'#243'pico de Tabela de Exa'#231#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 341
        Height = 32
        Caption = 'T'#243'pico de Tabela de Exa'#231#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 341
        Height = 32
        Caption = 'T'#243'pico de Tabela de Exa'#231#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 342
    Width = 689
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 4
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 685
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 386
    Width = 689
    Height = 70
    Align = alBottom
    TabOrder = 5
    object PnSaiDesis: TPanel
      Left = 543
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel2: TPanel
      Left = 2
      Top = 15
      Width = 541
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object QrOVcYnsMixTop: TMySQLQuery
    SQL.Strings = (
      'SELECT Codigo, Nome, Mensuravl,'
      'TolerBasCalc, TolerSglUnMdid, TlrRndUsaMin,'
      'TlrRndUsaMax, TlrRndPrcMin, TlrRndPrcMax'
      'FROM ovcynsmixtop'
      'ORDER BY Nome')
    Left = 356
    Top = 112
    object QrOVcYnsMixTopCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrOVcYnsMixTopNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
    object QrOVcYnsMixTopTolerBasCalc: TSmallintField
      FieldName = 'TolerBasCalc'
      Required = True
    end
    object QrOVcYnsMixTopTolerSglUnMdid: TWideStringField
      FieldName = 'TolerSglUnMdid'
      Size = 15
    end
    object QrOVcYnsMixTopTlrRndUsaMin: TSmallintField
      FieldName = 'TlrRndUsaMin'
      Required = True
    end
    object QrOVcYnsMixTopTlrRndUsaMax: TSmallintField
      FieldName = 'TlrRndUsaMax'
      Required = True
    end
    object QrOVcYnsMixTopTlrRndPrcMin: TFloatField
      FieldName = 'TlrRndPrcMin'
      Required = True
    end
    object QrOVcYnsMixTopTlrRndPrcMax: TFloatField
      FieldName = 'TlrRndPrcMax'
      Required = True
    end
    object QrOVcYnsMixTopMensuravl: TSmallintField
      FieldName = 'Mensuravl'
    end
  end
  object DsOVcYnsMixTop: TDataSource
    DataSet = QrOVcYnsMixTop
    Left = 356
    Top = 160
  end
end
