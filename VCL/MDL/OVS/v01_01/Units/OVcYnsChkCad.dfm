object FmOVcYnsChkCad: TFmOVcYnsChkCad
  Left = 368
  Top = 194
  Caption = 'OVS-INCFM-004 :: Cadastros de Check Lists'
  ClientHeight = 505
  ClientWidth = 1008
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnEdita: TPanel
    Left = 0
    Top = 96
    Width = 1008
    Height = 409
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    Visible = False
    object GBEdita: TGroupBox
      Left = 0
      Top = 0
      Width = 1008
      Height = 185
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 0
      object Label7: TLabel
        Left = 16
        Top = 16
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label9: TLabel
        Left = 76
        Top = 16
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label3: TLabel
        Left = 16
        Top = 56
        Width = 82
        Height = 13
        Caption = 'Artigo referencial:'
      end
      object Label4: TLabel
        Left = 16
        Top = 96
        Width = 87
        Height = 13
        Caption = 'Cliente referencial:'
      end
      object SbArtigRef: TSpeedButton
        Left = 736
        Top = 71
        Width = 23
        Height = 23
        Caption = '...'
        OnClick = SbArtigRefClick
      end
      object SbClientRef: TSpeedButton
        Left = 736
        Top = 111
        Width = 23
        Height = 23
        Caption = '...'
        OnClick = SbClientRefClick
      end
      object EdCodigo: TdmkEdit
        Left = 16
        Top = 32
        Width = 56
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Codigo'
        UpdCampo = 'Codigo'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdNome: TdmkEdit
        Left = 76
        Top = 32
        Width = 681
        Height = 21
        MaxLength = 60
        TabOrder = 1
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Nome'
        UpdCampo = 'Nome'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdArtigRef: TdmkEditCB
        Left = 16
        Top = 72
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 2
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'ArtigRef'
        UpdCampo = 'ArtigRef'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBArtigRef
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBArtigRef: TdmkDBLookupComboBox
        Left = 74
        Top = 72
        Width = 659
        Height = 21
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsOVdReferencia
        TabOrder = 3
        dmkEditCB = EdArtigRef
        QryCampo = 'ArtigRef'
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdClientRef: TdmkEditCB
        Left = 16
        Top = 112
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 4
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'ClientRef'
        UpdCampo = 'ClientRef'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBClientRef
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBClientRef: TdmkDBLookupComboBox
        Left = 74
        Top = 112
        Width = 659
        Height = 21
        KeyField = 'Codigo'
        ListField = 'NO_ENT'
        ListSource = DsEntidades
        TabOrder = 5
        dmkEditCB = EdClientRef
        QryCampo = 'ClientRef'
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
    end
    object GBConfirma: TGroupBox
      Left = 0
      Top = 346
      Width = 1008
      Height = 63
      Align = alBottom
      TabOrder = 1
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 12
        Top = 17
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object Panel1: TPanel
        Left = 868
        Top = 15
        Width = 138
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 7
          Top = 2
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
    end
  end
  object PnDados: TPanel
    Left = 0
    Top = 96
    Width = 1008
    Height = 409
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    object GBDados: TGroupBox
      Left = 0
      Top = 0
      Width = 1008
      Height = 141
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 1
      object Label1: TLabel
        Left = 16
        Top = 16
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
        FocusControl = DBEdCodigo
      end
      object Label2: TLabel
        Left = 76
        Top = 16
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
        FocusControl = DBEdNome
      end
      object Label5: TLabel
        Left = 16
        Top = 56
        Width = 82
        Height = 13
        Caption = 'Artigo referencial:'
      end
      object Label6: TLabel
        Left = 16
        Top = 96
        Width = 87
        Height = 13
        Caption = 'Cliente referencial:'
      end
      object DBEdCodigo: TdmkDBEdit
        Left = 16
        Top = 32
        Width = 56
        Height = 21
        TabStop = False
        DataField = 'Codigo'
        DataSource = DsOVcYnsChkCad
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 0
        UpdType = utYes
        Alignment = taRightJustify
      end
      object DBEdNome: TdmkDBEdit
        Left = 76
        Top = 32
        Width = 689
        Height = 21
        Color = clWhite
        DataField = 'Nome'
        DataSource = DsOVcYnsChkCad
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        UpdType = utYes
        Alignment = taLeftJustify
      end
      object DBEdit1: TDBEdit
        Left = 16
        Top = 72
        Width = 56
        Height = 21
        DataField = 'ArtigRef'
        DataSource = DsOVcYnsChkCad
        TabOrder = 2
      end
      object DBEdit2: TDBEdit
        Left = 76
        Top = 72
        Width = 689
        Height = 21
        DataField = 'NO_ArtigRef'
        DataSource = DsOVcYnsChkCad
        TabOrder = 3
      end
      object DBEdit3: TDBEdit
        Left = 16
        Top = 112
        Width = 56
        Height = 21
        DataField = 'ClientRef'
        DataSource = DsOVcYnsChkCad
        TabOrder = 4
      end
      object DBEdit4: TDBEdit
        Left = 76
        Top = 112
        Width = 689
        Height = 21
        DataField = 'NO_ClientRef'
        DataSource = DsOVcYnsChkCad
        TabOrder = 5
      end
    end
    object GBCntrl: TGroupBox
      Left = 0
      Top = 345
      Width = 1008
      Height = 64
      Align = alBottom
      TabOrder = 0
      object Panel5: TPanel
        Left = 2
        Top = 15
        Width = 172
        Height = 47
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 174
        Top = 15
        Width = 154
        Height = 47
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel3: TPanel
        Left = 328
        Top = 15
        Width = 678
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object Panel2: TPanel
          Left = 545
          Top = 0
          Width = 133
          Height = 47
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 0
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
        object BtCab: TBitBtn
          Tag = 1000384
          Left = 4
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = 'Chec&k List'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtCabClick
        end
        object BtIts: TBitBtn
          Tag = 1000146
          Left = 128
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = 'Conte&xto'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = BtItsClick
        end
        object BtTop: TBitBtn
          Tag = 1000494
          Left = 252
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&T'#243'pico'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = BtTopClick
        end
      end
    end
    object PnGrids: TPanel
      Left = 0
      Top = 141
      Width = 1008
      Height = 112
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 2
      object DBGTopico: TDBGrid
        Left = 413
        Top = 0
        Width = 595
        Height = 112
        Align = alClient
        DataSource = DsOVcYnsChkTop
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'Conta'
            Title.Caption = 'ID'
            Width = 56
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Topico'
            Width = 56
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NO_Topico'
            Title.Caption = 'Descri'#231#227'o do T'#243'pico'
            Width = 373
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NO_Magnitude'
            Title.Caption = 'Magnitude'
            Width = 69
            Visible = True
          end>
      end
      object DBGContexto: TDBGrid
        Left = 0
        Top = 0
        Width = 413
        Height = 112
        Align = alLeft
        DataSource = DsOVcYnsChkCtx
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'Controle'
            Title.Caption = 'ID'
            Width = 56
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Contexto'
            Width = 56
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NO_CONTEXTO'
            Title.Caption = 'Descri'#231#227'o do Contexto'
            Width = 261
            Visible = True
          end>
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 12
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 216
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 216
      Top = 0
      Width = 744
      Height = 52
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 314
        Height = 32
        Caption = 'Cadastros de Check Lists'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 314
        Height = 32
        Caption = 'Cadastros de Check Lists'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 314
        Height = 32
        Caption = 'Cadastros de Check Lists'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 52
    Width = 1008
    Height = 44
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = BtCab
    Left = 120
    Top = 64
  end
  object QrOVcYnsChkCad: TMySQLQuery
    Database = FmChmOcoCad.DBTeste
    BeforeOpen = QrOVcYnsChkCadBeforeOpen
    AfterOpen = QrOVcYnsChkCadAfterOpen
    BeforeClose = QrOVcYnsChkCadBeforeClose
    AfterScroll = QrOVcYnsChkCadAfterScroll
    SQL.Strings = (
      'SELECT IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_ClientRef, '
      'ycc.Nome NO_ArtigRef, ycc.* '
      'FROM ovcynschkcad ycc'
      'LEFT JOIN ovdreferencia art ON art.Codigo=ycc.ArtigRef '
      'LEFT JOIN entidades ent ON ent.Codigo=ycc.ClientRef '
      '')
    Left = 92
    Top = 233
    object QrOVcYnsChkCadNO_ClientRef: TWideStringField
      FieldName = 'NO_ClientRef'
      Size = 100
    end
    object QrOVcYnsChkCadNO_ArtigRef: TWideStringField
      FieldName = 'NO_ArtigRef'
      Size = 60
    end
    object QrOVcYnsChkCadCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrOVcYnsChkCadNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
    object QrOVcYnsChkCadArtigRef: TIntegerField
      FieldName = 'ArtigRef'
      Required = True
    end
    object QrOVcYnsChkCadClientRef: TIntegerField
      FieldName = 'ClientRef'
      Required = True
    end
  end
  object DsOVcYnsChkCad: TDataSource
    DataSet = QrOVcYnsChkCad
    Left = 92
    Top = 281
  end
  object QrOVcYnsChkCtx: TMySQLQuery
    Database = FmChmOcoCad.DBTeste
    BeforeClose = QrOVcYnsChkCtxBeforeClose
    AfterScroll = QrOVcYnsChkCtxAfterScroll
    SQL.Strings = (
      'SELECT yqt.Nome NO_CONTEXTO, ycc.*  '
      'FROM ovcynschkctx ycc '
      'LEFT JOIN ovcynsqstctx yqt ON yqt.Codigo=ycc.Contexto '
      'WHERE ycc.Codigo=1 ')
    Left = 188
    Top = 233
    object QrOVcYnsChkCtxNO_CONTEXTO: TWideStringField
      FieldName = 'NO_CONTEXTO'
      Size = 60
    end
    object QrOVcYnsChkCtxCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrOVcYnsChkCtxControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrOVcYnsChkCtxContexto: TIntegerField
      FieldName = 'Contexto'
      Required = True
    end
  end
  object DsOVcYnsChkCtx: TDataSource
    DataSet = QrOVcYnsChkCtx
    Left = 188
    Top = 281
  end
  object PMIts: TPopupMenu
    OnPopup = PMItsPopup
    Left = 424
    Top = 376
    object ItsInclui1: TMenuItem
      Caption = '&Adiciona'
      Enabled = False
      OnClick = ItsInclui1Click
    end
    object ItsAltera1: TMenuItem
      Caption = '&Edita'
      Enabled = False
      Visible = False
      OnClick = ItsAltera1Click
    end
    object ItsExclui1: TMenuItem
      Caption = '&Remove'
      Enabled = False
      OnClick = ItsExclui1Click
    end
  end
  object PMCab: TPopupMenu
    OnPopup = PMCabPopup
    Left = 300
    Top = 372
    object CabInclui1: TMenuItem
      Caption = '&Inclui'
      OnClick = CabInclui1Click
    end
    object CabAltera1: TMenuItem
      Caption = '&Altera'
      Enabled = False
      OnClick = CabAltera1Click
    end
    object CabExclui1: TMenuItem
      Caption = '&Exclui'
      Enabled = False
      OnClick = CabExclui1Click
    end
  end
  object QrEntidades: TMySQLQuery
    Database = FmChmOcoCad.DBTeste
    SQL.Strings = (
      'SELECT Codigo, IF(Tipo=0, RazaoSocial, Nome) NO_ENT'
      'FROM entidades'
      'ORDER BY NO_ENT')
    Left = 932
    Top = 121
    object QrEntidadesCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrEntidadesNO_ENT: TWideStringField
      FieldName = 'NO_ENT'
      Size = 100
    end
  end
  object DsEntidades: TDataSource
    DataSet = QrEntidades
    Left = 932
    Top = 169
  end
  object QrOVdReferencia: TMySQLQuery
    Database = FmChmOcoCad.DBTeste
    SQL.Strings = (
      'SELECT Codigo, Nome '
      'FROM ovdreferencia'
      'ORDER BY Nome')
    Left = 848
    Top = 129
    object QrOVdReferenciaCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrOVdReferenciaNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
  end
  object DsOVdReferencia: TDataSource
    DataSet = QrOVdReferencia
    Left = 848
    Top = 177
  end
  object PMTop: TPopupMenu
    OnPopup = PMTopPopup
    Left = 612
    Top = 380
    object IncluiTopico1: TMenuItem
      Caption = '&Inclui T'#243'pico'
      Enabled = False
      OnClick = IncluiTopico1Click
    end
    object AlteraTopico1: TMenuItem
      Caption = '&Altera T'#243'pico'
      Enabled = False
      OnClick = AlteraTopico1Click
    end
    object ExcluiTopico1: TMenuItem
      Caption = '&Exclui T'#243'pico'
      Enabled = False
      OnClick = ExcluiTopico1Click
    end
  end
  object QrOVcYnsChkTop: TMySQLQuery
    Database = FmChmOcoCad.DBTeste
    SQL.Strings = (
      'SELECT yqc.Nome NO_Topico, yqm.Nome NO_Magnitude, yct.*'
      'FROM ovcynschktop yct'
      'LEFT JOIN ovcynsqsttop yqc ON yqc.Codigo=yct.Topico'
      'LEFT JOIN ovcynsqstmag yqm ON yqm.Codigo=yct.Magnitude')
    Left = 292
    Top = 237
    object QrOVcYnsChkTopCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrOVcYnsChkTopControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrOVcYnsChkTopConta: TIntegerField
      FieldName = 'Conta'
      Required = True
    end
    object QrOVcYnsChkTopTopico: TIntegerField
      FieldName = 'Topico'
      Required = True
    end
    object QrOVcYnsChkTopMagnitude: TIntegerField
      FieldName = 'Magnitude'
      Required = True
    end
    object QrOVcYnsChkTopLk: TIntegerField
      FieldName = 'Lk'
      Required = True
    end
    object QrOVcYnsChkTopDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrOVcYnsChkTopDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrOVcYnsChkTopUserCad: TIntegerField
      FieldName = 'UserCad'
      Required = True
    end
    object QrOVcYnsChkTopUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Required = True
    end
    object QrOVcYnsChkTopAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Required = True
    end
    object QrOVcYnsChkTopAWServerID: TIntegerField
      FieldName = 'AWServerID'
      Required = True
    end
    object QrOVcYnsChkTopAWStatSinc: TSmallintField
      FieldName = 'AWStatSinc'
      Required = True
    end
    object QrOVcYnsChkTopAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
    object QrOVcYnsChkTopNO_Topico: TWideStringField
      FieldName = 'NO_Topico'
      Size = 60
    end
    object QrOVcYnsChkTopNO_Magnitude: TWideStringField
      FieldName = 'NO_Magnitude'
      Size = 60
    end
  end
  object DsOVcYnsChkTop: TDataSource
    DataSet = QrOVcYnsChkTop
    Left = 292
    Top = 285
  end
end
