object FmOVgItxPrfCfg: TFmOVgItxPrfCfg
  Left = 339
  Top = 185
  Caption = 
    'OVS-INSPE-009 :: Cria'#231#227'o do Gerenciamento de Inspe'#231#227'o  de T'#234'xtei' +
    's pelo seu Perfil'
  ClientHeight = 629
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 912
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 793
        Height = 32
        Caption = 'Cria'#231#227'o do Gerenciamento de Inspe'#231#227'o de T'#234'xteis pelo seu Perfil'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 793
        Height = 32
        Caption = 'Cria'#231#227'o do Gerenciamento de Inspe'#231#227'o de T'#234'xteis pelo seu Perfil'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 793
        Height = 32
        Caption = 'Cria'#231#227'o do Gerenciamento de Inspe'#231#227'o de T'#234'xteis pelo seu Perfil'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 1008
    Height = 467
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 1008
      Height = 467
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 1008
        Height = 467
        Align = alClient
        TabOrder = 0
        object DBGNaoCfg: TdmkDBGridZTO
          Left = 2
          Top = 15
          Width = 1004
          Height = 450
          Align = alClient
          DataSource = DsNaoCfg
          Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
          TabOrder = 0
          TitleFont.Charset = ANSI_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          RowColors = <>
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 515
    Width = 1008
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 559
    Width = 1008
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 862
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 860
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 352
    Top = 171
  end
  object QrNaoCfg: TMySQLQuery
    Database = Dmod.ZZDB
    SQL.Strings = (
      'SELECT DiSTINCT ils.Local, ils.NrOP, ils.SeqGrupo,  '
      'ils.NrReduzidoOP '
      'FROM ovgisplassta ils '
      'LEFT JOIN ovdlocal dlo ON dlo.Codigo=ils.Local  '
      'LEFT JOIN ovdreferencia ref ON ref.Codigo=ils.SeqGrupo '
      'LEFT JOIN ovgispprfcab prf ON prf.SeqGrupo=ils.SeqGrupo'
      'WHERE ils.IspZtatus=1024'
      'AND prf.Ativo=1'
      'ORDER BY NrReduzidoOP ')
    Left = 156
    Top = 276
    object QrNaoCfgLocal: TIntegerField
      FieldName = 'Local'
      Required = True
    end
    object QrNaoCfgNrOP: TIntegerField
      FieldName = 'NrOP'
      Required = True
    end
    object QrNaoCfgSeqGrupo: TIntegerField
      FieldName = 'SeqGrupo'
      Required = True
    end
    object QrNaoCfgNrReduzidoOP: TIntegerField
      FieldName = 'NrReduzidoOP'
      Required = True
    end
  end
  object DsNaoCfg: TDataSource
    DataSet = QrNaoCfg
    Left = 156
    Top = 324
  end
  object QrOVgItxPrfCab: TMySQLQuery
    Database = Dmod.ZZDB
    SQL.Strings = (
      'SELECT *   '
      'FROM ovgispprfcab   '
      'WHERE Ativo=1   '
      'AND SeqGrupo=999999999    '
      'ORDER BY Codigo DESC  '
      '    ')
    Left = 156
    Top = 376
    object QrOVgItxPrfCabCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrOVgItxPrfCabNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QrOVgItxPrfCabSeqGrupo: TIntegerField
      FieldName = 'SeqGrupo'
      Required = True
    end
    object QrOVgItxPrfCabOVcYnsExg: TIntegerField
      FieldName = 'OVcYnsExg'
      Required = True
    end
    object QrOVgItxPrfCabPermiFinHow: TSmallintField
      FieldName = 'PermiFinHow'
    end
  end
  object QrParaCfg: TMySQLQuery
    Database = Dmod.ZZDB
    SQL.Strings = (
      'SELECT DiSTINCT ils.Local, ils.NrOP, ils.SeqGrupo,  '
      'ils.NrReduzidoOP '
      'FROM ovgisplassta ils '
      'LEFT JOIN ovdlocal dlo ON dlo.Codigo=ils.Local  '
      'LEFT JOIN ovdreferencia ref ON ref.Codigo=ils.SeqGrupo '
      'LEFT JOIN ovgispprfcab prf ON prf.SeqGrupo=ils.SeqGrupo'
      'WHERE ils.IspZtatus=1024'
      'AND prf.Ativo=1'
      'ORDER BY NrReduzidoOP ')
    Left = 160
    Top = 424
    object QrParaCfgLocal: TIntegerField
      FieldName = 'Local'
      Required = True
    end
    object QrParaCfgNrOP: TIntegerField
      FieldName = 'NrOP'
      Required = True
    end
    object QrParaCfgSeqGrupo: TIntegerField
      FieldName = 'SeqGrupo'
      Required = True
    end
    object QrParaCfgNrReduzidoOP: TIntegerField
      FieldName = 'NrReduzidoOP'
      Required = True
    end
  end
  object QrAx: TMySQLQuery
    Database = Dmod.ZZDB
    Left = 432
    Top = 304
  end
end
