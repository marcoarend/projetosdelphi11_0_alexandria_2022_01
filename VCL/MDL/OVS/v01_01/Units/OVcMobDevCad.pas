unit OVcMobDevCad;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2, dmkGeral,
  ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt, UMySQLModule, Menus,
  UnInternalConsts2, mySQLDbTables, UnMySQLCuringa, dmkPermissoes, dmkEdit,
  dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup, Grids, DBGrids,
  UnDmkProcFunc, UnDmkENums, Vcl.ComCtrls, dmkEditDateTimePicker, dmkCheckBox;

type
  TFmOVcMobDevCad = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    Label9: TLabel;
    EdNome: TdmkEdit;
    Label1: TLabel;
    DBEdCodigo: TdmkDBEdit;
    Label2: TLabel;
    DBEdNome: TdmkDBEdit;
    QrOVcMobDevCad: TMySQLQuery;
    DsOVcMobDevCad: TDataSource;
    QrOVcMobDevAlw: TMySQLQuery;
    DsOVcMobDevAlw: TDataSource;
    PMIts: TPopupMenu;
    ItsInclui1: TMenuItem;
    ItsExclui1: TMenuItem;
    ItsAltera1: TMenuItem;
    PMCab: TPopupMenu;
    CabInclui1: TMenuItem;
    CabAltera1: TMenuItem;
    CabExclui1: TMenuItem;
    BtCab: TBitBtn;
    BtIts: TBitBtn;
    QrOVcMobDevCadCodigo: TIntegerField;
    QrOVcMobDevCadNome: TWideStringField;
    QrOVcMobDevCadDeviceID: TWideStringField;
    QrOVcMobDevCadUserNmePdr: TWideStringField;
    QrOVcMobDevCadDeviceName: TWideStringField;
    QrOVcMobDevCadDvcScreenH: TIntegerField;
    QrOVcMobDevCadDvcScreenW: TIntegerField;
    QrOVcMobDevCadOSName: TWideStringField;
    QrOVcMobDevCadOSNickName: TWideStringField;
    QrOVcMobDevCadOSVersion: TWideStringField;
    QrOVcMobDevCadDtaHabIni: TDateTimeField;
    QrOVcMobDevCadDtaHabFim: TDateTimeField;
    QrOVcMobDevCadAllowed: TSmallintField;
    QrOVcMobDevCadLastSetAlw: TIntegerField;
    QrOVcMobDevCadLk: TIntegerField;
    QrOVcMobDevCadDataCad: TDateField;
    QrOVcMobDevCadDataAlt: TDateField;
    QrOVcMobDevCadUserCad: TIntegerField;
    QrOVcMobDevCadUserAlt: TIntegerField;
    QrOVcMobDevCadAlterWeb: TSmallintField;
    QrOVcMobDevCadAWServerID: TIntegerField;
    QrOVcMobDevCadAWStatSinc: TSmallintField;
    QrOVcMobDevCadAtivo: TSmallintField;
    EdDeviceID: TdmkEdit;
    Label3: TLabel;
    EdDeviceName: TdmkEdit;
    Label5: TLabel;
    EdDvcScreenW: TdmkEdit;
    Label6: TLabel;
    EdDvcScreenH: TdmkEdit;
    EdOSName: TdmkEdit;
    Label8: TLabel;
    EdOSNickName: TdmkEdit;
    Label10: TLabel;
    EdOSVersion: TdmkEdit;
    Label11: TLabel;
    TPDtaHabIni: TdmkEditDateTimePicker;
    EdDtaHabIni: TdmkEdit;
    TPDtaHabFim: TdmkEditDateTimePicker;
    EdDtaHabFim: TdmkEdit;
    Label13: TLabel;
    EdUserNmePdr: TdmkEdit;
    Label4: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    Label18: TLabel;
    Label19: TLabel;
    Label20: TLabel;
    Label21: TLabel;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    Label22: TLabel;
    DBEdit4: TDBEdit;
    DBEdit5: TDBEdit;
    Label23: TLabel;
    DBEdit6: TDBEdit;
    DBEdit7: TDBEdit;
    DBEdit8: TDBEdit;
    DBEdit9: TDBEdit;
    DBEdit10: TDBEdit;
    Label24: TLabel;
    DBEdit11: TDBEdit;
    Label12: TLabel;
    Label25: TLabel;
    Label26: TLabel;
    EdNO_Allowed: TdmkEdit;
    QrOVcMobDevCadNO_Allowed: TWideStringField;
    Aceitarnovo1: TMenuItem;
    QrOVcMobDevAlwCodigo: TIntegerField;
    QrOVcMobDevAlwNome: TWideStringField;
    QrOVcMobDevAlwMobDevCad: TIntegerField;
    QrOVcMobDevAlwDeviceID: TWideStringField;
    QrOVcMobDevAlwAlwReason: TSmallintField;
    QrOVcMobDevAlwDtaInicio: TDateTimeField;
    QrOVcMobDevAlwLk: TIntegerField;
    QrOVcMobDevAlwDataCad: TDateField;
    QrOVcMobDevAlwDataAlt: TDateField;
    QrOVcMobDevAlwUserCad: TIntegerField;
    QrOVcMobDevAlwUserAlt: TIntegerField;
    QrOVcMobDevAlwAlterWeb: TSmallintField;
    QrOVcMobDevAlwAWServerID: TIntegerField;
    QrOVcMobDevAlwAWStatSinc: TSmallintField;
    QrOVcMobDevAlwAtivo: TSmallintField;
    QrOVcMobDevAcs: TMySQLQuery;
    DsOVcMobDevAcs: TDataSource;
    QrOVcMobDevAcsCodigo: TIntegerField;
    QrOVcMobDevAcsNome: TWideStringField;
    QrOVcMobDevAcsMobDevCad: TIntegerField;
    QrOVcMobDevAcsDeviceID: TWideStringField;
    QrOVcMobDevAcsAcsReason: TSmallintField;
    QrOVcMobDevAcsDtaAcsIni: TDateTimeField;
    QrOVcMobDevAcsDtaAcsFim: TDateTimeField;
    QrOVcMobDevAcsLk: TIntegerField;
    QrOVcMobDevAcsDataCad: TDateField;
    QrOVcMobDevAcsDataAlt: TDateField;
    QrOVcMobDevAcsUserCad: TIntegerField;
    QrOVcMobDevAcsUserAlt: TIntegerField;
    QrOVcMobDevAcsAlterWeb: TSmallintField;
    QrOVcMobDevAcsAWServerID: TIntegerField;
    QrOVcMobDevAcsAWStatSinc: TSmallintField;
    QrOVcMobDevAcsAtivo: TSmallintField;
    GBAlw: TGroupBox;
    DGDados: TDBGrid;
    Splitter1: TSplitter;
    GBAcs: TGroupBox;
    DBGrid1: TDBGrid;
    GroupBox1: TGroupBox;
    CkSccConfeccao: TdmkCheckBox;
    CkSccTecelagem: TdmkCheckBox;
    CkSccTinturaria: TdmkCheckBox;
    QrOVcMobDevCadSccConfeccao: TSmallintField;
    QrOVcMobDevCadSccTecelagem: TSmallintField;
    QrOVcMobDevCadSccTinturaria: TSmallintField;
    Panel6: TPanel;
    GroupBox2: TGroupBox;
    Panel7: TPanel;
    DBCheckBox1: TDBCheckBox;
    DBCheckBox2: TDBCheckBox;
    DBCheckBox3: TDBCheckBox;
    GroupBox3: TGroupBox;
    Panel8: TPanel;
    DBCheckBox4: TDBCheckBox;
    QrOVcMobDevCadChmOcorrencias: TSmallintField;
    GroupBox4: TGroupBox;
    Panel9: TPanel;
    CkChmOcorrencias: TdmkCheckBox;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrOVcMobDevCadAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrOVcMobDevCadBeforeOpen(DataSet: TDataSet);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure QrOVcMobDevCadAfterScroll(DataSet: TDataSet);
    procedure CabInclui1Click(Sender: TObject);
    procedure CabAltera1Click(Sender: TObject);
    procedure CabExclui1Click(Sender: TObject);
    procedure BtCabClick(Sender: TObject);
    procedure BtItsClick(Sender: TObject);
    procedure ItsInclui1Click(Sender: TObject);
    procedure ItsExclui1Click(Sender: TObject);
    procedure ItsAltera1Click(Sender: TObject);
    procedure PMCabPopup(Sender: TObject);
    procedure PMItsPopup(Sender: TObject);
    procedure QrOVcMobDevCadBeforeClose(DataSet: TDataSet);
    procedure Aceitarnovo1Click(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure LocCod(Atual, Codigo: Integer);
    procedure Va(Para: TVaiPara);
    procedure MostraOVcMobDevAlw(SQLType: TSQLType);

  public
    { Public declarations }
    FSeq, FCabIni: Integer;
    FLocIni: Boolean;
    //
    procedure ReopenOVcMobDevAlw();
    procedure ReopenOVcMobDevAcs();

  end;

var
  FmOVcMobDevCad: TFmOVcMobDevCad;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, MyDBCheck, DmkDAC_PF(*, OVcMobDevAlw*),
  OVcMobDevNew;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmOVcMobDevCad.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmOVcMobDevCad.MostraOVcMobDevAlw(SQLType: TSQLType);
begin
(*
  if DBCheck.CriaFm(TFmOVcMobDevAlw, FmOVcMobDevAlw, afmoNegarComAviso) then
  begin
    FmOVcMobDevAlw.ImgTipo.SQLType := SQLType;
    FmOVcMobDevAlw.FQrCab := QrOVcMobDevCad;
    FmOVcMobDevAlw.FDsCab := DsCadastro_Com_Itens_CAB;
    FmOVcMobDevAlw.FQrIts := QrOVcMobDevAlw;
    if SQLType = stIns then
      FmOVcMobDevAlw.EdCPF1.ReadOnly := False
    else
    begin
      FmOVcMobDevAlw.EdControle.ValueVariant := QrOVcMobDevAlwControle.Value;
      //
      FmOVcMobDevAlw.EdCPF1.Text := MLAGeral.FormataCNPJ_TFT(QrOVcMobDevAlwCNPJ_CPF.Value);
      FmOVcMobDevAlw.EdNomeEmiSac.Text := QrOVcMobDevAlwNome.Value;
      FmOVcMobDevAlw.EdCPF1.ReadOnly := True;
    end;
    FmOVcMobDevAlw.ShowModal;
    FmOVcMobDevAlw.Destroy;
  end;
*)
end;

procedure TFmOVcMobDevCad.PMCabPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemCabUpd(CabAltera1, QrOVcMobDevCad);
  MyObjects.HabilitaMenuItemCabDel(CabExclui1, QrOVcMobDevCad, QrOVcMobDevAlw);
end;

procedure TFmOVcMobDevCad.PMItsPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(ItsInclui1, QrOVcMobDevCad);
  MyObjects.HabilitaMenuItemItsUpd(ItsAltera1, QrOVcMobDevAlw);
  MyObjects.HabilitaMenuItemItsDel(ItsExclui1, QrOVcMobDevAlw);
end;

procedure TFmOVcMobDevCad.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrOVcMobDevCadCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmOVcMobDevCad.DefParams;
begin
  VAR_GOTOTABELA := 'ovcmobdevcad';
  VAR_GOTOMYSQLTABLE := QrOVcMobDevCad;
  VAR_GOTONEG := gotoPiZ;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT mdc.*, IF(mdc.Allowed=0, "N�O", "SIM") NO_Allowed');
  VAR_SQLx.Add('FROM ovcmobdevcad mdc');
  VAR_SQLx.Add('WHERE mdc.Codigo > 0');
  //
  VAR_SQL1.Add('AND mdc.Codigo=:P0');
  //
  //VAR_SQL2.Add('AND mdc.CodUsu=:P0');
  //
  VAR_SQLa.Add('AND mdc.Nome Like :P0');
  //
end;

procedure TFmOVcMobDevCad.ItsAltera1Click(Sender: TObject);
begin
  MostraOVcMobDevAlw(stUpd);
end;

procedure TFmOVcMobDevCad.CabExclui1Click(Sender: TObject);
begin
  Geral.MB_Info('A��o n�o implementada! Solicite � Dermatek:' + sLineBreak +
  Caption + sLineBreak + TMenuItem(Sender).Name);
end;

procedure TFmOVcMobDevCad.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmOVcMobDevCad.QueryPrincipalAfterOpen;
begin
end;

procedure TFmOVcMobDevCad.ItsExclui1Click(Sender: TObject);
var
  Controle: Integer;
begin
{
  if UMyMod.ExcluiRegistroInt1('Confirma a exclus�o do item selecionado?',
  'OVcMobDevAlw', 'Controle', QrOVcMobDevAlwControle.Value, Dmod.MyDB?) = ID_YES then
  begin
    Controle := GOTOy.LocalizaPriorNextIntQr(QrOVcMobDevAlw,
      QrOVcMobDevAlwControle, QrOVcMobDevAlwControle.Value);
    ReopenOVcMobDevAlw(Controle);
  end;
}
end;

procedure TFmOVcMobDevCad.ReopenOVcMobDevAcs;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrOVcMobDevAcs, Dmod.MyDB, [
  'SELECT * ',
  'FROM ovcmobdevacs ',
  'WHERE MobDevCad=' + Geral.FF0(QrOVcMobDevCadCodigo.Value),
  'ORDER BY DtaAcsIni DESC',
  '']);
  //
//  QrOVcMobDevAcs.Locate('Controle?, Controle, []);
end;

procedure TFmOVcMobDevCad.ReopenOVcMobDevAlw();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrOVcMobDevAlw, Dmod.MyDB, [
  'SELECT * ',
  'FROM ovcmobdevalw ',
  'WHERE MobDevCad=' + Geral.FF0(QrOVcMobDevCadCodigo.Value),
  'ORDER BY DtaInicio DESC',
  '']);
  //
//  QrOVcMobDevAlw.Locate('Controle?, Controle, []);
end;


procedure TFmOVcMobDevCad.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmOVcMobDevCad.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmOVcMobDevCad.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmOVcMobDevCad.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmOVcMobDevCad.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmOVcMobDevCad.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmOVcMobDevCad.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrOVcMobDevCadCodigo.Value;
  Close;
end;

procedure TFmOVcMobDevCad.ItsInclui1Click(Sender: TObject);
begin
  MostraOVcMobDevAlw(stIns);
end;

procedure TFmOVcMobDevCad.CabAltera1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrOVcMobDevCad, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'ovcmobdevcad');
end;

procedure TFmOVcMobDevCad.BtConfirmaClick(Sender: TObject);
var
  //DeviceID, DeviceName, OSName, OSNickName, OSVersion,
  //DtaHabIni, DtaHabFim
  Nome, UserNmePdr: String;
  //DvcScreenH, DvcScreenW,
  //Allowed, LastSetAlw,
  Codigo: Integer;
  SQLType: TSQLType;
  SccConfeccao, SccTecelagem, SccTinturaria, ChmOcorrencias: Integer;
begin
  SQLType        := ImgTipo.SQLType;
  Codigo         := EdCodigo.ValueVariant;
  Nome           := EdNome.ValueVariant;
  //DeviceID       := EdDeviceID.ValueVariant;
  UserNmePdr     := EdUserNmePdr.ValueVariant;
  //DeviceName     := EdDeviceName.ValueVariant;
  //DvcScreenH     := EdDvcScreenH.ValueVariant;
  //DvcScreenW     := EdDvcScreenW.ValueVariant;
  //OSName         := EdOSName.ValueVariant;
  //OSNickName     := EdOSNickName.ValueVariant;
  //OSVersion      := EdOSVersion.ValueVariant;
  //DtaHabIni      := DtaHabIni ;
  //DtaHabIni      := Geral.FDT_TP_Ed(TPDtaHabIni.Date, EdDtaHabIni.Text);
  //DtaHabFim      := DtaHabFim ;
  //DtaHabFim      := Geral.FDT_TP_Ed(TPDtaHabFim.Date, EdDtaHabFim.Text);
  //Allowed        := Allowed   ;
  //LastSetAlw     := LastSetAlw;
  SccConfeccao     := Geral.BoolToInt(CkSccConfeccao.Checked);
  SccTecelagem     := Geral.BoolToInt(CkSccTecelagem.Checked);
  SccTinturaria    := Geral.BoolToInt(CkSccTinturaria.Checked);
  ChmOcorrencias   := Geral.BoolToInt(CkChmOcorrencias.Checked);

  //
  if MyObjects.FIC(Length(Nome) = 0, EdNome, 'Defina uma descri��o!') then Exit;
  if MyObjects.FIC(Length(UserNmePdr) = 0, EdUserNmePdr,
    'Defina o nome do respons�vel padr�o do dispositivo!') then Exit;
  //
  Codigo := UMyMod.BPGS1I32('ovcmobdevcad', 'Codigo', '', '', tsPos, SQLType, Codigo);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'ovcmobdevcad', False, [
  'Nome', (*'DeviceID',*) 'UserNmePdr'(*,
  'DeviceName', 'DvcScreenH', 'DvcScreenW',
  'OSName', 'OSNickName', 'OSVersion',
  'DtaHabIni', 'DtaHabFim', 'Allowed',
  'LastSetAlw'*),
  'SccConfeccao', 'SccTecelagem', 'SccTinturaria',
  'ChmOcorrencias'], [
  'Codigo'], [
  Nome, (*DeviceID,*) UserNmePdr(*,
  DeviceName, DvcScreenH, DvcScreenW,
  OSName, OSNickName, OSVersion,
  DtaHabIni, DtaHabFim, Allowed,
  LastSetAlw*),
  SccConfeccao, SccTecelagem, SccTinturaria,
  ChmOcorrencias], [
  Codigo], True) then
  begin
    ImgTipo.SQLType := stLok;
    PnDados.Visible := True;
    PnEdita.Visible := False;
    GOTOy.BotoesSb(ImgTipo.SQLType);
    //
    LocCod(Codigo, Codigo);
    if FSeq = 1 then Close;
  end;
end;

procedure TFmOVcMobDevCad.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := EdCodigo.ValueVariant;
  if ImgTipo.SQLType = stIns then
    UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'ovcmobdevcad', Codigo);
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'ovcmobdevcad', 'Codigo');
end;

procedure TFmOVcMobDevCad.BtItsClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMIts, BtIts);
end;

procedure TFmOVcMobDevCad.Aceitarnovo1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmOVcMobDevNew, FmOVcMobDevNew, afmoNegarComAviso) then
  begin
    FmOVcMobDevNew.ShowModal;
    FmOVcMobDevNew.Destroy;
  end;

end;

procedure TFmOVcMobDevCad.BtCabClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMCab, BtCab);
end;

procedure TFmOVcMobDevCad.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  GBEdita.Align := alClient;
  GBAcs.Align := alClient;
  CriaOForm;
  FSeq := 0;
end;

procedure TFmOVcMobDevCad.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrOVcMobDevCadCodigo.Value, LaRegistro.Caption);
end;

procedure TFmOVcMobDevCad.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmOVcMobDevCad.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrOVcMobDevCadCodigo.Value, LaRegistro.Caption);
end;

procedure TFmOVcMobDevCad.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmOVcMobDevCad.QrOVcMobDevCadAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmOVcMobDevCad.QrOVcMobDevCadAfterScroll(DataSet: TDataSet);
begin
  ReopenOVcMobDevAlw();
  ReopenOVcMobDevAcs();
end;

procedure TFmOVcMobDevCad.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //
  if FSeq = 1 then
    CabInclui1Click(Self)
  else
  if (not FLocIni) and (FCabIni <> 0)  then
  begin
    LocCod(FCabIni, FCabIni);
    if QrOVcMobDevCadCodigo.Value <> FCabIni then
      Geral.MB_Aviso('Grupo n�o localizado!');
    FLocIni := True;
  end;
end;

procedure TFmOVcMobDevCad.SbQueryClick(Sender: TObject);
begin
  LocCod(QrOVcMobDevCadCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'ovcmobdevcad', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmOVcMobDevCad.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmOVcMobDevCad.CabInclui1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrOVcMobDevCad, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'ovcmobdevcad');
end;

procedure TFmOVcMobDevCad.QrOVcMobDevCadBeforeClose(
  DataSet: TDataSet);
begin
  QrOVcMobDevAlw.Close;
  QrOVcMobDevAcs.Close;
end;

procedure TFmOVcMobDevCad.QrOVcMobDevCadBeforeOpen(DataSet: TDataSet);
begin
  QrOVcMobDevCadCodigo.DisplayFormat := FFormatFloat;
end;

end.

