unit OVgIspPrfCab;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2, dmkGeral,
  ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt, UMySQLModule, Menus,
  UnInternalConsts2, mySQLDbTables, UnMySQLCuringa, dmkPermissoes, dmkEdit,
  dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup, Grids, DBGrids,
  UnDmkProcFunc, UnDmkENums, Vcl.ComCtrls;

type
  TFmOVgIspPrfCab = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    Label9: TLabel;
    EdNome: TdmkEdit;
    QrOVgIspPrfCab: TMySQLQuery;
    DsOVgIspPrfCab: TDataSource;
    PMIts: TPopupMenu;
    ItsInclui1: TMenuItem;
    ItsExclui1: TMenuItem;
    ItsAltera1: TMenuItem;
    PMCab: TPopupMenu;
    CabInclui1: TMenuItem;
    CabAltera1: TMenuItem;
    CabExclui1: TMenuItem;
    BtCab: TBitBtn;
    BtIts: TBitBtn;
    QrOVgIspPrfCabCodigo: TIntegerField;
    QrOVgIspPrfCabSeqGrupo: TIntegerField;
    QrOVgIspPrfCabOVcYnsMed: TIntegerField;
    QrOVgIspPrfCabOVcYnsChk: TIntegerField;
    QrOVgIspPrfCabLimiteChk: TIntegerField;
    QrOVgIspPrfCabLimiteMed: TIntegerField;
    QrOVgIspPrfCabLk: TIntegerField;
    QrOVgIspPrfCabDataCad: TDateField;
    QrOVgIspPrfCabDataAlt: TDateField;
    QrOVgIspPrfCabUserCad: TIntegerField;
    QrOVgIspPrfCabUserAlt: TIntegerField;
    QrOVgIspPrfCabAlterWeb: TSmallintField;
    QrOVgIspPrfCabAWServerID: TIntegerField;
    QrOVgIspPrfCabAWStatSinc: TSmallintField;
    QrOVgIspPrfCabAtivo: TSmallintField;
    QrOVgIspPrfCabNO_Referencia: TWideStringField;
    QrOVgIspPrfCabNome: TWideStringField;
    Panel6: TPanel;
    Label1: TLabel;
    DBEdCodigo: TdmkDBEdit;
    DBEdNome: TdmkDBEdit;
    Label2: TLabel;
    GroupBox3: TGroupBox;
    Panel7: TPanel;
    Label11: TLabel;
    Label12: TLabel;
    DBEdit7: TDBEdit;
    DBEdit8: TDBEdit;
    QrOVgIspPrfCabNO_OVcYnsMed: TWideStringField;
    QrOVgIspPrfCabNO_OVcYnsChk: TWideStringField;
    DBEdit9: TDBEdit;
    DBEdit10: TDBEdit;
    Label16: TLabel;
    DBEdit11: TDBEdit;
    DBEdit12: TDBEdit;
    PCItens: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    QrOVcYnsMedTop: TMySQLQuery;
    QrOVcYnsMedTopNO_TolerBasCalc: TWideStringField;
    QrOVcYnsMedTopNO_TolerUnMdida: TWideStringField;
    QrOVcYnsMedTopNO_TOBIKO: TWideStringField;
    QrOVcYnsMedTopCodigo: TIntegerField;
    QrOVcYnsMedTopControle: TIntegerField;
    QrOVcYnsMedTopTobiko: TIntegerField;
    QrOVcYnsMedTopTolerBasCalc: TSmallintField;
    QrOVcYnsMedTopTolerUnMdida: TSmallintField;
    QrOVcYnsMedTopTolerRndPerc: TFloatField;
    QrOVcYnsMedTopLk: TIntegerField;
    QrOVcYnsMedTopDataCad: TDateField;
    QrOVcYnsMedTopDataAlt: TDateField;
    QrOVcYnsMedTopUserCad: TIntegerField;
    QrOVcYnsMedTopUserAlt: TIntegerField;
    QrOVcYnsMedTopAlterWeb: TSmallintField;
    QrOVcYnsMedTopAWServerID: TIntegerField;
    QrOVcYnsMedTopAWStatSinc: TSmallintField;
    QrOVcYnsMedTopAtivo: TSmallintField;
    DsOVcYnsMedTop: TDataSource;
    QrOVcYnsMedDim: TMySQLQuery;
    QrOVcYnsMedDimCodigo: TIntegerField;
    QrOVcYnsMedDimControle: TIntegerField;
    QrOVcYnsMedDimConta: TIntegerField;
    QrOVcYnsMedDimCodGrade: TIntegerField;
    QrOVcYnsMedDimCodTam: TWideStringField;
    QrOVcYnsMedDimMedidCer: TFloatField;
    QrOVcYnsMedDimMedidMin: TFloatField;
    QrOVcYnsMedDimMedidMax: TFloatField;
    QrOVcYnsMedDimUlWayInz: TSmallintField;
    QrOVcYnsMedDimLk: TIntegerField;
    QrOVcYnsMedDimDataCad: TDateField;
    QrOVcYnsMedDimDataAlt: TDateField;
    QrOVcYnsMedDimUserCad: TIntegerField;
    QrOVcYnsMedDimUserAlt: TIntegerField;
    QrOVcYnsMedDimAlterWeb: TSmallintField;
    QrOVcYnsMedDimAWServerID: TIntegerField;
    QrOVcYnsMedDimAWStatSinc: TSmallintField;
    QrOVcYnsMedDimAtivo: TSmallintField;
    DsOVcYnsMedDim: TDataSource;
    PnGrids: TPanel;
    DBGTopico: TDBGrid;
    DBGContexto: TDBGrid;
    QrOVcYnsChkCtx: TMySQLQuery;
    QrOVcYnsChkCtxNO_CONTEXTO: TWideStringField;
    QrOVcYnsChkCtxCodigo: TIntegerField;
    QrOVcYnsChkCtxControle: TIntegerField;
    QrOVcYnsChkCtxContexto: TIntegerField;
    DsOVcYnsChkCtx: TDataSource;
    QrOVcYnsChkTop: TMySQLQuery;
    QrOVcYnsChkTopCodigo: TIntegerField;
    QrOVcYnsChkTopControle: TIntegerField;
    QrOVcYnsChkTopConta: TIntegerField;
    QrOVcYnsChkTopTopico: TIntegerField;
    QrOVcYnsChkTopMagnitude: TIntegerField;
    QrOVcYnsChkTopLk: TIntegerField;
    QrOVcYnsChkTopDataCad: TDateField;
    QrOVcYnsChkTopDataAlt: TDateField;
    QrOVcYnsChkTopUserCad: TIntegerField;
    QrOVcYnsChkTopUserAlt: TIntegerField;
    QrOVcYnsChkTopAlterWeb: TSmallintField;
    QrOVcYnsChkTopAWServerID: TIntegerField;
    QrOVcYnsChkTopAWStatSinc: TSmallintField;
    QrOVcYnsChkTopAtivo: TSmallintField;
    QrOVcYnsChkTopNO_Topico: TWideStringField;
    QrOVcYnsChkTopNO_Magnitude: TWideStringField;
    DsOVcYnsChkTop: TDataSource;
    Panel8: TPanel;
    DBGrid1: TDBGrid;
    DBGrid2: TDBGrid;
    QrOVgIspPrfCabOVcYnsARQ: TIntegerField;
    Label20: TLabel;
    dmkDBEdit7: TdmkDBEdit;
    dmkDBEdit8: TdmkDBEdit;
    QrOVgIspPrfCabNO_OVcYnsARQ: TWideStringField;
    DBRadioGroup1: TDBRadioGroup;
    QrOVgIspPrfCabPermiFinHow: TIntegerField;
    Label6: TLabel;
    DBEdit4: TDBEdit;
    Label8: TLabel;
    DBEdit5: TDBEdit;
    QrOVgIspPrfCabReferencia: TWideStringField;
    Label3: TLabel;
    DBEdit1: TDBEdit;
    DBCheckBox1: TDBCheckBox;
    QrOVdReferencia: TMySQLQuery;
    QrOVdReferenciaCodigo: TIntegerField;
    QrOVdReferenciaNome: TWideStringField;
    DsOVdReferencia: TDataSource;
    QrOVdReferenciaReferencia: TWideStringField;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrOVgIspPrfCabAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrOVgIspPrfCabBeforeOpen(DataSet: TDataSet);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure QrOVgIspPrfCabAfterScroll(DataSet: TDataSet);
    procedure CabInclui1Click(Sender: TObject);
    procedure CabAltera1Click(Sender: TObject);
    procedure CabExclui1Click(Sender: TObject);
    procedure BtCabClick(Sender: TObject);
    procedure BtItsClick(Sender: TObject);
    procedure PMCabPopup(Sender: TObject);
    procedure QrOVgIspPrfCabBeforeClose(DataSet: TDataSet);
    procedure QrOVcYnsMedTopAfterScroll(DataSet: TDataSet);
    procedure QrOVcYnsMedTopBeforeClose(DataSet: TDataSet);
    procedure QrOVcYnsChkCtxAfterScroll(DataSet: TDataSet);
    procedure QrOVcYnsChkCtxBeforeClose(DataSet: TDataSet);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    //
    procedure ReopenOVcYnsMedTop(Controle: Integer);
    procedure ReopenOVcYnsMedDim(Conta: Integer);
    procedure ReopenOVcYnsChkCtx(Controle: Integer);
    procedure ReopenOVcYnsChkTop(Conta: Integer);

  public
    { Public declarations }
    FSeq, FCabIni: Integer;
    FLocIni: Boolean;
    //
    procedure LocCod(Atual, Codigo: Integer);

  end;

var
  FmOVgIspPrfCab: TFmOVgIspPrfCab;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, MyDBCheck, DmkDAC_PF, UnOVS_Jan, UnOVS_PF,
  UnOVS_Consts, ModuleGeral, GerlShowGrid, PesqNomeOuReferencia;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmOVgIspPrfCab.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmOVgIspPrfCab.PMCabPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemCabUpd(CabAltera1, QrOVgIspPrfCab);
end;

procedure TFmOVgIspPrfCab.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrOVgIspPrfCabCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmOVgIspPrfCab.DefParams;
begin
  VAR_GOTOTABELA := 'ovgispprfcab';
  VAR_GOTOMYSQLTABLE := QrOVgIspPrfCab;
  VAR_GOTONEG := gotoPiZ;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT igc.*, ymc.Nome NO_OVcYnsMed, ycc.Nome NO_OVcYnsChk, ');
  VAR_SQLx.Add('ref.Nome NO_Referencia, ref.Referencia, yac.Nome NO_OVcYnsARQ ');
  VAR_SQLx.Add('FROM ovgispprfcab igc');
  VAR_SQLx.Add('LEFT JOIN ovdreferencia ref ON ref.Codigo=igc.SeqGrupo ');
  VAR_SQLx.Add('LEFT JOIN ovcynsmedcad ymc ON ymc.Codigo=igc.OVcYnsMed');
  VAR_SQLx.Add('LEFT JOIN ovcynschkcad ycc ON ycc.Codigo=igc.OVcYnsChk');
  VAR_SQLx.Add('LEFT JOIN OVcYnsARQCad yac ON yac.Codigo=igc.OVcYnsARQ');
  VAR_SQLx.Add('WHERE igc.Codigo > 0');
  //
  VAR_SQL1.Add('AND igc.Codigo=:P0');
  //
  //VAR_SQL2.Add('AND CodUsu=:P0');
  //
  VAR_SQLa.Add('AND igc.Nome Like :P0');
  //
end;

procedure TFmOVgIspPrfCab.CabExclui1Click(Sender: TObject);
begin
  Geral.MB_Info('A��o n�o implementada! Solicite � Dermatek:' + sLineBreak +
  Caption + sLineBreak + TMenuItem(Sender).Name);
end;

procedure TFmOVgIspPrfCab.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmOVgIspPrfCab.QueryPrincipalAfterOpen;
begin
end;

procedure TFmOVgIspPrfCab.ReopenOVcYnsChkCtx(Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrOVcYnsChkCtx, Dmod.MyDB, [
  'SELECT yqt.Nome NO_CONTEXTO, ycc.*  ',
  'FROM ovcynschkctx ycc ',
  'LEFT JOIN ovcynsqstctx yqt ON yqt.Codigo=ycc.Contexto ',
  'WHERE ycc.Codigo=' + Geral.FF0(QrOVgIspPrfCabOVcYnsChk.Value),
  '']);
  //
  QrOVcYnsChkCtx.Locate('Controle', Controle, []);
end;

procedure TFmOVgIspPrfCab.ReopenOVcYnsChkTop(Conta: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrOVcYnsChkTop, Dmod.MyDB, [
  'SELECT yqc.Nome NO_Topico, yqm.Nome NO_Magnitude, yct.* ',
  'FROM ovcynschktop yct ',
  'LEFT JOIN ovcynsqsttop yqc ON yqc.Codigo=yct.Topico ',
  'LEFT JOIN ovcynsqstmag yqm ON yqm.Codigo=yct.Magnitude ',
  'WHERE yct.Controle=' + Geral.FF0(QrOVcYnsChkCtxControle.Value),
  '']);
  //
  QrOVcYnsChkTop.Locate('Conta', Conta, []);
end;

procedure TFmOVgIspPrfCab.ReopenOVcYnsMedDim(Conta: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrOVcYnsMedDim, Dmod.MyDB, [
  'SELECT ymd.* ',
  'FROM ovcynsmeddim ymd ',
  'WHERE ymd.Controle=' + Geral.FF0(QrOVcYnsMedTopControle.Value),
  '']);
  //
  QrOVcYnsMedDim.Locate('Conta', Conta, []);
end;

procedure TFmOVgIspPrfCab.ReopenOVcYnsMedTop(Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrOVcYnsMedTop, Dmod.MyDB, [
  'SELECT ELT(ymt.TolerBasCalc+1, "Medida", "Percentual") NO_TolerBasCalc, ',
  'ELT(ymt.TolerUnMdida+1, "cm", "inch") NO_TolerUnMdida,  ',
  'ygt.Nome NO_TOBIKO, ymt.* ',
  'FROM ovcynsmedtop ymt ',
  'LEFT JOIN ovcynsgratop ygt ON ygt.Codigo=ymt.Tobiko ',
  'WHERE ymt.Codigo=' + Geral.FF0(QrOVgIspPrfCabOVcYnsMed.Value),
  '']);
  //
  QrOVcYnsMedTop.Locate('Controle', Controle, []);
end;

procedure TFmOVgIspPrfCab.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmOVgIspPrfCab.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmOVgIspPrfCab.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmOVgIspPrfCab.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmOVgIspPrfCab.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmOVgIspPrfCab.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmOVgIspPrfCab.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrOVgIspPrfCabCodigo.Value;
  Close;
end;

procedure TFmOVgIspPrfCab.CabAltera1Click(Sender: TObject);
begin
  OVS_Jan.MostraFormOVgIspPrfCad(stUpd, QrOVgIspPrfCabCodigo.Value,
    QrOVgIspPrfCabSeqGrupo.Value, QrOVgIspPrfCabNO_Referencia.Value,
    QrOVgIspPrfCabReferencia.Value, QrOVgIspPrfCabOVcYnsMed.Value,
    QrOVgIspPrfCabOVcYnsChk.Value, QrOVgIspPrfCabOVcYnsARQ.Value,
    QrOVgIspPrfCabLimiteMed.Value, QrOVgIspPrfCabLimiteChk.Value,
    QrOVgIspPrfCabPermiFinHow.Value, QrOVgIspPrfCabAtivo.Value,
    QrOVgIspPrfCabNome.Value);
  //
  LocCod(QrOVgIspPrfCabCodigo.Value, QrOVgIspPrfCabCodigo.Value);
end;

procedure TFmOVgIspPrfCab.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := EdCodigo.ValueVariant;
  if ImgTipo.SQLType = stIns then
    UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'ovgispprfcab', Codigo);
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'ovgispprfcab', 'Codigo');
end;

procedure TFmOVgIspPrfCab.BtItsClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMIts, BtIts);
end;

procedure TFmOVgIspPrfCab.BtCabClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMCab, BtCab);
end;

procedure TFmOVgIspPrfCab.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  GBEdita.Align := alClient;
  PCItens.Align := alClient;
  PCItens.ActivePageIndex := 0;
  CriaOForm;
  FSeq := 0;
end;

procedure TFmOVgIspPrfCab.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrOVgIspPrfCabCodigo.Value, LaRegistro.Caption);
end;

procedure TFmOVgIspPrfCab.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmOVgIspPrfCab.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrOVgIspPrfCabCodigo.Value, LaRegistro.Caption);
end;

procedure TFmOVgIspPrfCab.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmOVgIspPrfCab.QrOVcYnsChkCtxAfterScroll(DataSet: TDataSet);
begin
  ReopenOVcYnsChkTop(0);
end;

procedure TFmOVgIspPrfCab.QrOVcYnsChkCtxBeforeClose(DataSet: TDataSet);
begin
  QrOVcYnsChkTop.Close;
end;

procedure TFmOVgIspPrfCab.QrOVcYnsMedTopAfterScroll(DataSet: TDataSet);
begin
  ReopenOVcYnsMedDim(0);
end;

procedure TFmOVgIspPrfCab.QrOVcYnsMedTopBeforeClose(DataSet: TDataSet);
begin
  QrOVcYnsMedDim.Close;
end;

procedure TFmOVgIspPrfCab.QrOVgIspPrfCabAfterOpen(DataSet: TDataSet);
begin
  //Geral.MB_Info(QrOVgIspPrfCab.SQL.Text);
  QueryPrincipalAfterOpen;
end;

procedure TFmOVgIspPrfCab.QrOVgIspPrfCabAfterScroll(DataSet: TDataSet);
begin
  ReopenOVcYnsMedTop(0);
  ReopenOVcYnsChkCtx(0);
end;

procedure TFmOVgIspPrfCab.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //
  if FSeq = 1 then
    CabInclui1Click(Self)
  else
  if (not FLocIni) and (FCabIni <> 0)  then
  begin
    LocCod(FCabIni, FCabIni);
    if QrOVgIspPrfCabCodigo.Value <> FCabIni then
      Geral.MB_Aviso('Grupo n�o localizado!');
    FLocIni := True;
  end;
end;

procedure TFmOVgIspPrfCab.SbQueryClick(Sender: TObject);
var
  Codigo: Integer;
begin
  if OVS_Jan.LocalizaOVgIspPrfCab(Codigo) then
    LocCod(Codigo, Codigo);
end;

procedure TFmOVgIspPrfCab.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmOVgIspPrfCab.CabInclui1Click(Sender: TObject);
var
  Codigo, SeqGrupo, OVcYnsMed, OVcYnsChk, OVcYnsARQ, LimiteMed, LimiteChk,
  PermiFinHow, Ativo: Integer;
  NO_Artigo, Referencia, Nome: String;
begin
  SeqGrupo := 0;
  MyObjects.CriaForm_AcessoTotal(TFmPesqNomeOuReferencia, FmPesqNomeOuReferencia);
  FmPesqNomeOuReferencia.FNomeTabela  := 'ovdreferencia';
  FmPesqNomeOuReferencia.FNomeFldNome := 'Nome';
  FmPesqNomeOuReferencia.FNomeFldRefe := 'Referencia';
  FmPesqNomeOuReferencia.FNomeFldCodi := 'Codigo';
  FmPesqNomeOuReferencia.ShowModal;
  if FmPesqNomeOuReferencia.FSelecionou then
  begin
    SeqGrupo   := FmPesqNomeOuReferencia.FValorCodi;
    NO_Artigo  := FmPesqNomeOuReferencia.FValorNome;
    Referencia := FmPesqNomeOuReferencia.FValorRefe;
  end;
  FmPesqNomeOuReferencia.Destroy;
  //
  if SeqGrupo <> 0 then
  begin
    Codigo        := 0;
    OVcYnsMed     := 0;
    OVcYnsChk     := 0;
    OVcYnsARQ     := 0;
    LimiteMed     := 0;
    LimiteChk     := 0;
    PermiFinHow   := 0;
    Ativo         := 0;
    Nome          := '';
    //
    OVS_Jan.MostraFormOVgIspPrfCad(stIns, Codigo, SeqGrupo, NO_Artigo,
    Referencia, OVcYnsMed, OVcYnsChk, OVcYnsARQ, LimiteMed, LimiteChk,
    PermiFinHow, Ativo, Nome);
  end;
end;

procedure TFmOVgIspPrfCab.QrOVgIspPrfCabBeforeClose(
  DataSet: TDataSet);
begin
  QrOVcYnsMedTop.Close;
end;

procedure TFmOVgIspPrfCab.QrOVgIspPrfCabBeforeOpen(DataSet: TDataSet);
begin
  QrOVgIspPrfCabCodigo.DisplayFormat := FFormatFloat;
end;

end.

