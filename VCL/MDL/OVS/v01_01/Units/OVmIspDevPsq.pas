unit OVmIspDevPsq;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkPermissoes,
  dmkDBLookupComboBox, dmkEditCB, mySQLDbTables, dmkEditDateTimePicker,
  dmkDBGridZTO, dmkCheckGroup;

type
  TFmOVmIspDevPsq = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    QrOVdLocal: TMySQLQuery;
    QrOVdLocalCodigo: TIntegerField;
    QrOVdLocalNome: TWideStringField;
    DsOVdLocal: TDataSource;
    QrOVdReferencia: TMySQLQuery;
    QrOVdReferenciaCodigo: TIntegerField;
    QrOVdReferenciaNome: TWideStringField;
    DsOVdReferencia: TDataSource;
    Panel5: TPanel;
    Label7: TLabel;
    Label3: TLabel;
    Label6: TLabel;
    Label1: TLabel;
    Label2: TLabel;
    EdLocal: TdmkEditCB;
    CBLocal: TdmkDBLookupComboBox;
    EdNrOP: TdmkEdit;
    EdNrReduzidoOP: TdmkEdit;
    EdArtigo: TdmkEditCB;
    CBArtigo: TdmkDBLookupComboBox;
    EdReferencia: TdmkEdit;
    QrOVcMobDevCad: TMySQLQuery;
    QrOVcMobDevCadCodigo: TIntegerField;
    QrOVcMobDevCadNome: TWideStringField;
    DsOVcMobDevCad: TDataSource;
    QrOVgIspGerCab: TMySQLQuery;
    QrOVgIspGerCabCodigo: TIntegerField;
    QrOVgIspGerCabNome: TWideStringField;
    DsOVgIspGerCab: TDataSource;
    GroupBox3: TGroupBox;
    Panel7: TPanel;
    Label8: TLabel;
    SbOVgIspGer: TSpeedButton;
    EdOVgIspGer: TdmkEditCB;
    CbOVgIspGer: TdmkDBLookupComboBox;
    QrOVcMobDevCadDeviceID: TWideStringField;
    TPDtPerIni: TdmkEditDateTimePicker;
    TPDtPerFim: TdmkEditDateTimePicker;
    CkDtIni: TCheckBox;
    CkDtFim: TCheckBox;
    EdCodTam: TdmkEdit;
    Label9: TLabel;
    QrOVmIspDevCab: TMySQLQuery;
    QrOVmIspDevCabCodigo: TIntegerField;
    QrOVmIspDevCabCodInMob: TIntegerField;
    QrOVmIspDevCabDeviceSI: TIntegerField;
    QrOVmIspDevCabDeviceID: TWideStringField;
    QrOVmIspDevCabDeviceCU: TIntegerField;
    QrOVmIspDevCabNome: TWideStringField;
    QrOVmIspDevCabOVgIspGer: TIntegerField;
    QrOVmIspDevCabLocal: TIntegerField;
    QrOVmIspDevCabNrOP: TIntegerField;
    QrOVmIspDevCabSeqGrupo: TIntegerField;
    QrOVmIspDevCabNrReduzidoOP: TIntegerField;
    QrOVmIspDevCabDtHrAbert: TDateTimeField;
    QrOVmIspDevCabDtHrFecha: TDateTimeField;
    QrOVmIspDevCabOVcYnsMed: TIntegerField;
    QrOVmIspDevCabOVcYnsChk: TIntegerField;
    QrOVmIspDevCabOVcYnsARQ: TIntegerField;
    QrOVmIspDevCabLimiteChk: TIntegerField;
    QrOVmIspDevCabLimiteMed: TIntegerField;
    QrOVmIspDevCabPecasIsp: TIntegerField;
    QrOVmIspDevCabPecaAtual: TIntegerField;
    QrOVmIspDevCabLk: TIntegerField;
    QrOVmIspDevCabDataCad: TDateField;
    QrOVmIspDevCabDataAlt: TDateField;
    QrOVmIspDevCabUserCad: TIntegerField;
    QrOVmIspDevCabUserAlt: TIntegerField;
    QrOVmIspDevCabAlterWeb: TSmallintField;
    QrOVmIspDevCabAWServerID: TIntegerField;
    QrOVmIspDevCabAWStatSinc: TSmallintField;
    QrOVmIspDevCabAtivo: TSmallintField;
    QrOVmIspDevCabQtReal: TFloatField;
    QrOVmIspDevCabQtLocal: TFloatField;
    QrOVmIspDevCabProduto: TIntegerField;
    QrOVmIspDevCabCodGrade: TIntegerField;
    QrOVmIspDevCabCodTam: TWideStringField;
    QrOVmIspDevCabPontosTot: TIntegerField;
    QrOVmIspDevCabInspResul: TIntegerField;
    QrOVmIspDevCabInspeSeq: TIntegerField;
    QrOVmIspDevCabRandmStr: TWideStringField;
    QrOVmIspDevCabDtHrUpIni: TDateTimeField;
    QrOVmIspDevCabDtHrUpFim: TDateTimeField;
    QrOVmIspDevCabEmpresa: TIntegerField;
    QrOVmIspDevCabNO_OVcYnsMed: TWideStringField;
    QrOVmIspDevCabNO_OVcYnsChk: TWideStringField;
    QrOVmIspDevCabNO_Local: TWideStringField;
    QrOVmIspDevCabNO_Referencia: TWideStringField;
    QrOVmIspDevCabNO_InspResul: TWideStringField;
    QrOVmIspDevCabNO_OVcYnsARQ: TWideStringField;
    QrOVmIspDevCabDtHrAbert_TXT: TWideStringField;
    QrOVmIspDevCabDtHrFecha_TXT: TWideStringField;
    DsOVmIspDevCab: TDataSource;
    DBGrid1: TdmkDBGridZTO;
    Label4: TLabel;
    EdOVcMobDevCad: TdmkEditCB;
    CBOVcMobDevCad: TdmkDBLookupComboBox;
    SbOVcMobDevCad: TSpeedButton;
    EdCodInMob: TdmkEdit;
    Label5: TLabel;
    CGInspResul: TdmkCheckGroup;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure EdArtigoChange(Sender: TObject);
    procedure EdArtigoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdReferenciaChange(Sender: TObject);
    procedure EdReferenciaExit(Sender: TObject);
    procedure EdReferenciaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure CBArtigoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SbOVcMobDevCadClick(Sender: TObject);
    procedure SbOVgIspGerClick(Sender: TObject);
    procedure DBGrid1DblClick(Sender: TObject);
  private
    { Private declarations }

  public
    { Public declarations }
    FOCmIspDevCab: Integer;
  end;

  var
  FmOVmIspDevPsq: TFmOVmIspDevPsq;

implementation

uses UnMyObjects, Module, DmkDAC_PF, UnMySQLCuringa, UnOVS_Jan, UMySQLModule,
  UnDmkProcFunc, UnOVS_Consts,
  ModOVS;

{$R *.DFM}

procedure TFmOVmIspDevPsq.BtOKClick(Sender: TObject);
var
  Local, Artigo, NrOP, NrReduzidoOP, OVcMobDevCad, OVgIspGer, CodInMob,
  InspResul: Integer;
  CodTam,
  SQL_Local, SQL_Artigo, SQL_NrOP, SQL_NrReduzidoOP, SQL_OVcMobDevCad,
  SQL_OVgIspGer, SQL_Periodo, SQL_CodTam, SQL_CodInMob, SQL_InspResul: String;
begin
  if MyObjects.FIC(CGInspResul.Value = 0, CGInspResul,
  'Define pelo menos um tipo de resultado de inspe��o!') then Exit;
  //
  Local            := EdLocal.ValueVariant;
  Artigo           := EdArtigo.ValueVariant;
  NrOP             := EdNrOP.ValueVariant;
  NrReduzidoOP     := EdNrReduzidoOP.ValueVariant;
  OVcMobDevCad     := EdOVcMobDevCad.ValueVariant;
  OVgIspGer        := EdOVgIspGer.ValueVariant; //Configura��o de inspe��o
  CodTam           := EdCodTam.Text;
  CodInMob         := EdCodInMob.ValueVariant;
  InspResul        := CGInspResul.Value;
  //
  SQL_Local        := '';
  SQL_Artigo       := '';
  SQL_NrOP         := '';
  SQL_NrReduzidoOP := '';
  SQL_OVcMobDevCad := '';
  SQL_OVgIspGer    := '';
  SQL_Periodo      := '';
  SQL_CodTam       := '';
  SQL_CodInMob     := '';
  //
  if Local <> 0 then
    SQL_Local := 'AND igc.Local=' + Geral.FF0(Local);
  if Artigo <> 0 then
    SQL_Artigo := 'AND igc.SeqGrupo=' + Geral.FF0(Artigo);
  if NrOP <> 0 then
    SQL_NrOP := 'AND igc.NrOP=' + Geral.FF0(NrOP);
  if NrReduzidoOP <> 0 then
    SQL_NrReduzidoOP := 'AND igc.NrReduzidoOP=' + Geral.FF0(NrReduzidoOP);
  if OVcMobDevCad <> 0 then
    SQL_OVcMobDevCad := 'AND igc.DeviceSI=' + Geral.FF0(OVcMobDevCad);
  if Trim(CodTam) <> EmptyStr then
    SQL_CodTam := 'AND igc.CodTam="' + CodTam + '"';
  if CodInMob <> 0 then
    SQL_CodInMob := 'AND igc.CodInMob=' + Geral.FF0(CodInMob);
  if InspResul <> 0 then
    SQL_InspResul := 'AND igc.InspResul IN (' +
    MyObjects.CordaDeIntInConjunto(InspResul, [
      CO_INSP_RESUL_1024_APROVADO,
      CO_INSP_RESUL_2048_APR_RESALV,
      CO_INSP_RESUL_3072_REJEITADO]) + ')';
  //
  SQL_Periodo :=
    DmkPF.SQL_Periodo('AND DATE(igc.DtHrFecha) ',
    TPDtPerIni.Date, TPDtPerFim.Date, CkDtIni.Checked, CkDtFim.Checked);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrOVmIspDevCab, Dmod.MyDB, [
  'SELECT igc.*, ymc.Nome NO_OVcYnsMed, ycc.Nome NO_OVcYnsChk, ',
  'dlo.Nome NO_Local, ref.Nome NO_Referencia, ',
  'isc.Nome NO_InspResul, yac.Nome NO_OVcYnsARQ, ',
  'IF(igc.DtHrAbert  <= "1899-12-30", "", ',
  '  DATE_FORMAT(igc.DtHrAbert, "%d/%m/%Y %H:%i:%s")) DtHrAbert_TXT, ',
  'IF(igc.DtHrFecha  <= "1899-12-30", "", ',
  '  DATE_FORMAT(igc.DtHrFecha, "%d/%m/%Y %H:%i:%s")) DtHrFecha_TXT ',
  'FROM ovmispdevcab igc ',
  'LEFT JOIN ovdlocal dlo ON dlo.Codigo=igc.Local ',
  'LEFT JOIN ovdreferencia ref ON ref.Codigo=igc.SeqGrupo ',
  'LEFT JOIN ovcynsmedcad ymc ON ymc.Codigo=igc.OVcYnsMed ',
  'LEFT JOIN ovcynschkcad ycc ON ycc.Codigo=igc.OVcYnsChk ',
  'LEFT JOIN ovgisprescad isc ON isc.Codigo=igc.InspResul ',
  'LEFT JOIN OVcYnsARQCad yac ON yac.Codigo=igc.OVcYnsARQ ',
  'WHERE igc.Codigo > -999999999 ',
  SQL_Local        ,
  SQL_Artigo       ,
  SQL_NrOP         ,
  SQL_NrReduzidoOP ,
  SQL_OVcMobDevCad ,
  SQL_OVgIspGer    ,
  SQL_Periodo      ,
  SQL_CodTam       ,
  SQL_CodInMob     ,
  SQL_InspResul    ,
  EmptyStr]);
  //
  //Geral.MB_Info(QrOVmIspDevCab.SQL.Text);
end;

procedure TFmOVmIspDevPsq.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmOVmIspDevPsq.CBArtigoKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key=VK_F3 then
    CuringaLoc.Pesquisa('Codigo', 'Nome', 'Referencia', Dmod.MyDB,
    ''(*Extra*), EdArtigo, CBArtigo, dmktfInteger);
end;

procedure TFmOVmIspDevPsq.DBGrid1DblClick(Sender: TObject);
begin
  FOCmIspDevCab := QrOVmIspDevCabCodigo.Value;
  Close;
end;

procedure TFmOVmIspDevPsq.EdArtigoChange(Sender: TObject);
begin
  if not EdReferencia.Focused then
    DmOVS.PesquisaPorCodigo(EdArtigo.ValueVariant, EdReferencia);
end;

procedure TFmOVmIspDevPsq.EdArtigoKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key=VK_F3 then
    CuringaLoc.Pesquisa('Codigo', 'Nome', 'Referencia', Dmod.MyDB,
    ''(*Extra*), EdArtigo, CBArtigo, dmktfInteger);
end;

procedure TFmOVmIspDevPsq.EdReferenciaChange(Sender: TObject);
begin
  if EdReferencia.Focused then
    DmOVS.PesquisaPorReferencia(False, EdReferencia, EdArtigo, CBArtigo);
end;

procedure TFmOVmIspDevPsq.EdReferenciaExit(Sender: TObject);
begin
  DmOVS.PesquisaPorReferencia(True, EdReferencia, EdArtigo, CBArtigo);
end;

procedure TFmOVmIspDevPsq.EdReferenciaKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key=VK_F3 then
    CuringaLoc.Pesquisa('Codigo', 'Nome', 'Referencia', Dmod.MyDB,
    ''(*Extra*), EdArtigo, CBArtigo, dmktfInteger)
end;

procedure TFmOVmIspDevPsq.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmOVmIspDevPsq.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  FOCmIspDevCab   := 0;
  CGInspResul.Value := CGInspResul.MaxValue;
  UnDmkDAC_PF.AbreQuery(QrOVdLocal, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrOVdReferencia, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrOVcMobDevCad, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrOVgIspGerCab, Dmod.MyDB);
end;

procedure TFmOVmIspDevPsq.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmOVmIspDevPsq.SbOVcMobDevCadClick(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  OVS_Jan.MostraFormOVcMobDevCad();
  UMyMod.SetaCodigoPesquisado(EdOVcMobDevCad, CBOVcMobDevCad, QrOVcMobDevCad, VAR_CADASTRO);
end;

procedure TFmOVmIspDevPsq.SbOVgIspGerClick(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  OVS_Jan.MostraFormOVgIspGerCab(EdOVgIspGer.ValueVariant);
  UMyMod.SetaCodigoPesquisado(EdOVgIspGer, CBOVgIspGer, QrOVgIspGerCab, VAR_CADASTRO);
end;

end.
