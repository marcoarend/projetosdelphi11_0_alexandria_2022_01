unit OVcYnsMixTop;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, Variants, ExtDlgs, ZCF2,
  ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt, UMySQLModule, Menus,
  UnInternalConsts2, mySQLDbTables, UnMySQLCuringa, dmkPermissoes, dmkEdit,
  dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup, Grids, DBGrids,
  UnDmkProcFunc, UnDmkENums, dmkDBLookupComboBox, dmkEditCB, dmkCheckBox,
  dmkGeral, Vcl.ComCtrls, dmkDBGridZTO;

type
  TFmOVcYnsMixTop = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    Label9: TLabel;
    EdNome: TdmkEdit;
    Label1: TLabel;
    DBEdCodigo: TdmkDBEdit;
    Label2: TLabel;
    DBEdNome: TdmkDBEdit;
    QrOVcYnsMixTop: TMySQLQuery;
    DsOVcYnsMixTop: TDataSource;
    QrUsouEm: TMySQLQuery;
    DsUsouEm: TDataSource;
    PMIts: TPopupMenu;
    ItsInclui1: TMenuItem;
    ItsExclui1: TMenuItem;
    ItsAltera1: TMenuItem;
    PMCab: TPopupMenu;
    CabInclui1: TMenuItem;
    CabAltera1: TMenuItem;
    CabExclui1: TMenuItem;
    BtCab: TBitBtn;
    BtIts: TBitBtn;
    Label5: TLabel;
    DBEdit1: TDBEdit;
    Label3: TLabel;
    EdSigla: TdmkEdit;
    QrUsouEmCodigo: TIntegerField;
    QrUsouEmNome: TWideStringField;
    QrOVcYnsMixTopNO_TolerBasCalc: TWideStringField;
    GroupBox1: TGroupBox;
    PnDBMensuravl: TPanel;
    Label6: TLabel;
    DBRadioGroup1: TDBRadioGroup;
    DBEdit3: TDBEdit;
    DBEdit2: TDBEdit;
    Panel7: TPanel;
    DBCkMensuravl: TDBCheckBox;
    GroupBox2: TGroupBox;
    PnMensuravl: TPanel;
    Panel8: TPanel;
    CkMensuravl: TdmkCheckBox;
    RGTolerBasCalc: TdmkRadioGroup;
    Label8: TLabel;
    EdTolerSglUnMdid: TdmkEdit;
    EdTlrRndPrcMin: TdmkEdit;
    SpeedButton5: TSpeedButton;
    QrOVcYnsMixOpc: TMySQLQuery;
    DsOVcYnsMixOpc: TDataSource;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    QrOVcYnsMixOpcNO_Magnitude: TWideStringField;
    DBGrid1: TDBGrid;
    DGDados: TDBGrid;
    CkTlrRndUsaMin: TdmkCheckBox;
    EdTlrRndPrcMax: TdmkEdit;
    CkTlrRndUsaMax: TdmkCheckBox;
    QrOVcYnsMixTopCodigo: TIntegerField;
    QrOVcYnsMixTopNome: TWideStringField;
    QrOVcYnsMixTopSigla: TWideStringField;
    QrOVcYnsMixTopMensuravl: TSmallintField;
    QrOVcYnsMixTopTolerBasCalc: TSmallintField;
    QrOVcYnsMixTopTolerSglUnMdid: TWideStringField;
    QrOVcYnsMixTopTlrRndUsaMin: TSmallintField;
    QrOVcYnsMixTopTlrRndPrcMin: TFloatField;
    QrOVcYnsMixTopTlrRndUsaMax: TSmallintField;
    QrOVcYnsMixTopTlrRndPrcMax: TFloatField;
    QrOVcYnsMixTopLk: TIntegerField;
    QrOVcYnsMixTopDataCad: TDateField;
    QrOVcYnsMixTopDataAlt: TDateField;
    QrOVcYnsMixTopUserCad: TIntegerField;
    QrOVcYnsMixTopUserAlt: TIntegerField;
    QrOVcYnsMixTopAlterWeb: TSmallintField;
    QrOVcYnsMixTopAWServerID: TIntegerField;
    QrOVcYnsMixTopAWStatSinc: TSmallintField;
    QrOVcYnsMixTopAtivo: TSmallintField;
    QrOVcYnsMixOpcCodigo: TIntegerField;
    QrOVcYnsMixOpcControle: TIntegerField;
    QrOVcYnsMixOpcNome: TWideStringField;
    QrOVcYnsMixOpcMagnitude: TIntegerField;
    QrOVcYnsMixOpcPontNeg: TIntegerField;
    QrOVcYnsMixOpcLk: TIntegerField;
    QrOVcYnsMixOpcDataCad: TDateField;
    QrOVcYnsMixOpcDataAlt: TDateField;
    QrOVcYnsMixOpcUserCad: TIntegerField;
    QrOVcYnsMixOpcUserAlt: TIntegerField;
    QrOVcYnsMixOpcAlterWeb: TSmallintField;
    QrOVcYnsMixOpcAWServerID: TIntegerField;
    QrOVcYnsMixOpcAWStatSinc: TSmallintField;
    QrOVcYnsMixOpcAtivo: TSmallintField;
    DBCheckBox1: TDBCheckBox;
    DBCheckBox2: TDBCheckBox;
    DBEdit4: TDBEdit;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrOVcYnsMixTopAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrOVcYnsMixTopBeforeOpen(DataSet: TDataSet);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure QrOVcYnsMixTopAfterScroll(DataSet: TDataSet);
    procedure CabInclui1Click(Sender: TObject);
    procedure CabAltera1Click(Sender: TObject);
    procedure CabExclui1Click(Sender: TObject);
    procedure BtCabClick(Sender: TObject);
    procedure BtItsClick(Sender: TObject);
    procedure ItsInclui1Click(Sender: TObject);
    procedure ItsAltera1Click(Sender: TObject);
    procedure PMCabPopup(Sender: TObject);
    procedure PMItsPopup(Sender: TObject);
    procedure QrOVcYnsMixTopBeforeClose(DataSet: TDataSet);
    procedure EdNomeRedefinido(Sender: TObject);
    procedure CkMensuravlClick(Sender: TObject);
    procedure DBCkMensuravlClick(Sender: TObject);
    procedure SpeedButton5Click(Sender: TObject);
    procedure ItsExclui1Click(Sender: TObject);
    procedure CkTlrRndUsaMinClick(Sender: TObject);
    procedure CkTlrRndUsaMaxClick(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure LocCod(Atual, Codigo: Integer);
    procedure Va(Para: TVaiPara);
    procedure MostraOVcYnsMixOpc(SQLType: TSQLType);

  public
    { Public declarations }
    FSeq, FCabIni: Integer;
    FLocIni: Boolean;
    //
    procedure ReopenUsouEm();
    procedure ReopenOVcYnsMixOpc(Controle: Integer);

  end;

var
  FmOVcYnsMixTop: TFmOVcYnsMixTop;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, ModuleGeral, MyDBCheck, DmkDAC_PF, UnOVS_Jan,
  OVcYnsMixOpc;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmOVcYnsMixTop.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmOVcYnsMixTop.MostraOVcYnsMixOpc(SQLType: TSQLType);
begin
  if DBCheck.CriaFm(TFmOVcYnsMixOpc, FmOVcYnsMixOpc, afmoNegarComAviso) then
  begin
    FmOVcYnsMixOpc.ImgTipo.SQLType := SQLType;
    FmOVcYnsMixOpc.FQrCab := QrOVcYnsMixTop;
    FmOVcYnsMixOpc.FDsCab := DsOVcYnsMixTop;
    FmOVcYnsMixOpc.FQrIts := QrOVcYnsMixOpc;
    if SQLType = stIns then
      //
    else
    begin
      FmOVcYnsMixOpc.EdControle.ValueVariant := QrOVcYnsMixOpcControle.Value;
      //
      FmOVcYnsMixOpc.EdNome.ValueVariant      := QrOVcYnsMixOpcNome.Value;
      FmOVcYnsMixOpc.EdMagnitude.ValueVariant := QrOVcYnsMixOpcMagnitude.Value;
      FmOVcYnsMixOpc.CBMagnitude.KeyValue     := QrOVcYnsMixOpcMagnitude.Value;
      FmOVcYnsMixOpc.EdPontNeg.ValueVariant   := QrOVcYnsMixOpcPontNeg.Value;
    end;
    FmOVcYnsMixOpc.ShowModal;
    FmOVcYnsMixOpc.Destroy;
  end;
end;

procedure TFmOVcYnsMixTop.PMCabPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemCabUpd(CabAltera1, QrOVcYnsMixTop);
  MyObjects.HabilitaMenuItemCabDel(CabExclui1, QrOVcYnsMixTop, QrUsouEm);
end;

procedure TFmOVcYnsMixTop.PMItsPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(ItsInclui1, QrOVcYnsMixTop);
  MyObjects.HabilitaMenuItemItsUpd(ItsAltera1, QrOVcYnsMixOpc);
  MyObjects.HabilitaMenuItemItsDel(ItsExclui1, QrOVcYnsMixOpc);
end;

procedure TFmOVcYnsMixTop.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrOVcYnsMixTopCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmOVcYnsMixTop.DefParams;
begin
  VAR_GOTOTABELA := 'ovcynsmixtop';
  VAR_GOTOMYSQLTABLE := QrOVcYnsMixTop;
  VAR_GOTONEG := gotoPiZ;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT ');
  VAR_SQLx.Add('ELT(ygt.TolerBasCalc+1, "Medida", "Percentual") NO_TolerBasCalc, ');
  VAR_SQLx.Add('ygt.* ');
  VAR_SQLx.Add('FROM ovcynsmixtop ygt ');
  //
  VAR_SQLx.Add('WHERE ygt.Codigo>0');
  VAR_SQLx.Add('AND ygt.Codigo=:P0');
  //
  //VAR_SQL2.Add('AND CodUsu=:P0');
  //
  VAR_SQLa.Add('AND ygt.Nome Like :P0');
  //
end;

procedure TFmOVcYnsMixTop.EdNomeRedefinido(Sender: TObject);
begin
  if EdSigla.Text = '' then
    EdSigla.Text := Copy(EdNome.Text, 1, EdSigla.MaxLength);
end;

procedure TFmOVcYnsMixTop.ItsAltera1Click(Sender: TObject);
begin
  MostraOVcYnsMixOpc(stUpd);
end;

procedure TFmOVcYnsMixTop.ItsExclui1Click(Sender: TObject);
var
  Controle: Integer;
begin
  if UMyMod.ExcluiRegistroInt1('Confirma a exclus�o do item selecionado?',
  'OVcYnsMixOpc', 'Controle', QrOVcYnsMixOpcControle.Value, Dmod.MyDB) = ID_YES then
  begin
    Controle := GOTOy.LocalizaPriorNextIntQr(QrOVcYnsMixOpc,
      QrOVcYnsMixOpcControle, QrOVcYnsMixOpcControle.Value);
    ReopenOVcYnsMixOpc(Controle);
  end;
end;

procedure TFmOVcYnsMixTop.CabExclui1Click(Sender: TObject);
begin
  Geral.MB_Info('A��o n�o implementada! Solicite � Dermatek:' + sLineBreak +
  Caption + sLineBreak + TMenuItem(Sender).Name);
end;

procedure TFmOVcYnsMixTop.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmOVcYnsMixTop.QueryPrincipalAfterOpen;
begin
end;

procedure TFmOVcYnsMixTop.ReopenOVcYnsMixOpc(Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrOVcYnsMixOpc, Dmod.MyDB, [
  'SELECT mag.Nome NO_Magnitude, opc.*  ',
  'FROM ovcynsmixopc opc ',
  'LEFT JOIN ovcynsqstmag mag ON  mag.Codigo=opc.Magnitude ',
  'WHERE opc.Codigo=' + Geral.FF0(QrOVcYnsMixTopCodigo.Value),
  '']);
  //
  if Controle <> 0 then
    QrOVcYnsMixOpc.Locate('Controle', Controle, []);
end;

procedure TFmOVcYnsMixTop.ReopenUsouEm();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrUsouEm, Dmod.MyDB, [
  'SELECT ymc.Codigo, ymc.Nome ',
  'FROM ovcynsmedtop ymt ',
  'LEFT JOIN ovcynsmedcad ymc ON ymc.Codigo=ymt.Tobiko ',
  'WHERE ymt.Tobiko=' + Geral.FF0(QrOVcYnsMixTopCodigo.Value),
  '']);
end;


procedure TFmOVcYnsMixTop.DBCkMensuravlClick(Sender: TObject);
begin
  PnDBMensuravl.Visible := DBCkMensuravl.Checked;
end;

procedure TFmOVcYnsMixTop.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmOVcYnsMixTop.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmOVcYnsMixTop.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmOVcYnsMixTop.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmOVcYnsMixTop.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmOVcYnsMixTop.SpeedButton5Click(Sender: TObject);
const
  Aviso  = '...';
  Titulo = 'Sele��o de Sigla de Unidade de Medida';
  Prompt = 'Informe a unidade de medida: [F7 para pesquisar]';
  Campo  = 'Descricao';
  //
  LocF7CodiFldName = 'Sigla';
  LocF7NameFldName = 'Nome';
var
  Qry: TmySQLQuery;
  PesqSQL: String;
  Res: Variant;
begin
  PesqSQL := Geral.ATS([
  'SELECT Sigla Codigo, Nome Descricao ',
  'FROM unmedcoml',
  'ORDER BY Descricao ',
  '']);
  Res :=
    DBCheck.EscolheTextoUnico(Aviso, Titulo, Prompt, nil, nil, Campo, '',
    [PesqSQL], DModG.AllID_DB, True, LocF7CodiFldName, LocF7NameFldName);
  if Res <> Null then
    EdTolerSglUnMdid.Text := VAR_SELCODTXT;
end;

procedure TFmOVcYnsMixTop.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmOVcYnsMixTop.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrOVcYnsMixTopCodigo.Value;
  Close;
end;

procedure TFmOVcYnsMixTop.ItsInclui1Click(Sender: TObject);
begin
  MostraOVcYnsMixOpc(stIns);
end;

procedure TFmOVcYnsMixTop.CabAltera1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrOVcYnsMixTop, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'ovcynsmixtop');
end;

procedure TFmOVcYnsMixTop.BtConfirmaClick(Sender: TObject);
var
  Nome, Sigla, TolerSglUnMdid: String;
  Codigo, Mensuravl, TolerBasCalc, TlrRndUsaMin, TlrRndUsaMax: Integer;
  TlrRndPrcMin, TlrRndPrcMax: Double;
  SQLType: TSQLType;
begin
  SQLType        := ImgTipo.SQLType;
  Codigo         := EdCodigo.ValueVariant;
  Nome           := EdNome.ValueVariant;
  Sigla          := EdSigla.ValueVariant;
  Mensuravl      := Geral.BoolToInt(CkMensuravl.Checked);
  if CkMensuravl.Checked then
  begin
    TolerBasCalc   := RGTolerBasCalc.ItemIndex;
    TolerSglUnMdid := EdTolerSglUnMdid.Text;
    TlrRndUsaMin   := Geral.BoolToInt(CkTlrRndUsaMin.Checked);
    TlrRndPrcMin   := EdTlrRndPrcMin.ValueVariant;
    TlrRndUsaMax   := Geral.BoolToInt(CkTlrRndUsaMax.Checked);
    TlrRndPrcMax   := EdTlrRndPrcMax.ValueVariant;
  end else
  begin
    TolerBasCalc   := RGTolerBasCalc.ItemIndex;
    TolerSglUnMdid := '';
    TlrRndUsaMin   := 0;
    TlrRndPrcMin   := 0.000;
    TlrRndUsaMax   := 0;
    TlrRndPrcMax   := 0.000;
  end;
  //
  if MyObjects.FIC(Length(Nome) = 0, EdNome, 'Defina uma descri��o!') then Exit;
  //
  Codigo := UMyMod.BPGS1I32('ovcynsmixtop', 'Codigo', '', '', tsPos, SQLType, Codigo);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'ovcynsmixtop', False, [
  'Nome', 'Sigla', 'Mensuravl',
  'TolerBasCalc', 'TolerSglUnMdid', 'TlrRndUsaMin',
  'TlrRndPrcMin', 'TlrRndUsaMax', 'TlrRndPrcMax'], [
  'Codigo'], [
  Nome, Sigla, Mensuravl,
  TolerBasCalc, TolerSglUnMdid, TlrRndUsaMin,
  TlrRndPrcMin, TlrRndUsaMax, TlrRndPrcMax], [
  Codigo], True) then
  begin
    ImgTipo.SQLType := stLok;
    PnDados.Visible := True;
    PnEdita.Visible := False;
    GOTOy.BotoesSb(ImgTipo.SQLType);
    //
    LocCod(Codigo, Codigo);
    if FSeq = 1 then Close;
  end;
end;

procedure TFmOVcYnsMixTop.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := EdCodigo.ValueVariant;
  if ImgTipo.SQLType = stIns then
    UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'ovcynsmixtop', Codigo);
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'ovcynsmixtop', 'Codigo');
end;

procedure TFmOVcYnsMixTop.BtItsClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMIts, BtIts);
end;

procedure TFmOVcYnsMixTop.BtCabClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMCab, BtCab);
end;

procedure TFmOVcYnsMixTop.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //GBEdita.Align := alClient;
  PageControl1.Align := alClient;
  PageControl1.ActivePageIndex := 0;
  CriaOForm;
  FSeq := 0;
end;

procedure TFmOVcYnsMixTop.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrOVcYnsMixTopCodigo.Value, LaRegistro.Caption);
end;

procedure TFmOVcYnsMixTop.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmOVcYnsMixTop.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrOVcYnsMixTopCodigo.Value, LaRegistro.Caption);
end;

procedure TFmOVcYnsMixTop.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmOVcYnsMixTop.QrOVcYnsMixTopAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmOVcYnsMixTop.QrOVcYnsMixTopAfterScroll(DataSet: TDataSet);
begin
  ReopenOVcYnsMixOpc(0);
  ReopenUsouEm();
end;

procedure TFmOVcYnsMixTop.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //
  if FSeq = 1 then
    CabInclui1Click(Self)
  else
  if (not FLocIni) and (FCabIni <> 0)  then
  begin
    LocCod(FCabIni, FCabIni);
    if QrOVcYnsMixTopCodigo.Value <> FCabIni then
      Geral.MB_Aviso('Grupo n�o localizado!');
    FLocIni := True;
  end;
end;

procedure TFmOVcYnsMixTop.SbQueryClick(Sender: TObject);
begin
  LocCod(QrOVcYnsMixTopCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'ovcynsmixtop', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmOVcYnsMixTop.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmOVcYnsMixTop.CabInclui1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrOVcYnsMixTop, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'ovcynsmixtop');
end;

procedure TFmOVcYnsMixTop.CkMensuravlClick(Sender: TObject);
begin
  PnMensuravl.Visible := CkMensuravl.Checked;
end;

procedure TFmOVcYnsMixTop.CkTlrRndUsaMaxClick(Sender: TObject);
begin
  if EdTlrRndPrcMax.CanFocus then
    EdTlrRndPrcMax.SetFocus;
end;

procedure TFmOVcYnsMixTop.CkTlrRndUsaMinClick(Sender: TObject);
begin
  if EdTlrRndPrcMin.CanFocus then
    EdTlrRndPrcMin.SetFocus;
end;

procedure TFmOVcYnsMixTop.QrOVcYnsMixTopBeforeClose(
  DataSet: TDataSet);
begin
  QrOVcYnsMixOpc.Close;
  QrUsouEm.Close;
end;

procedure TFmOVcYnsMixTop.QrOVcYnsMixTopBeforeOpen(DataSet: TDataSet);
begin
  QrOVcYnsMixTopCodigo.DisplayFormat := FFormatFloat;
end;

end.

