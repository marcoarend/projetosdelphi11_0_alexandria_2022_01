object FmOVdProduto: TFmOVdProduto
  Left = 368
  Top = 194
  Caption = 'OVS-CADAS-004 :: Cadastro de Produtos (Reduzidos)'
  ClientHeight = 401
  ClientWidth = 790
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnEdita: TPanel
    Left = 0
    Top = 96
    Width = 790
    Height = 305
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    Visible = False
    ExplicitWidth = 784
    ExplicitHeight = 295
    object GBEdita: TGroupBox
      Left = 0
      Top = 0
      Width = 790
      Height = 125
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 0
      ExplicitWidth = 784
      object Label7: TLabel
        Left = 16
        Top = 16
        Width = 14
        Height = 13
        Caption = 'ID:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label9: TLabel
        Left = 76
        Top = 16
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
        Color = clBtnFace
        ParentColor = False
      end
      object EdCodigo: TdmkEdit
        Left = 16
        Top = 32
        Width = 56
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Codigo'
        UpdCampo = 'Codigo'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdNome: TdmkEdit
        Left = 76
        Top = 32
        Width = 692
        Height = 21
        TabOrder = 1
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Nome'
        UpdCampo = 'Nome'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
    end
    object GBConfirma: TGroupBox
      Left = 0
      Top = 242
      Width = 790
      Height = 63
      Align = alBottom
      TabOrder = 1
      ExplicitTop = 232
      ExplicitWidth = 784
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 12
        Top = 17
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object Panel1: TPanel
        Left = 651
        Top = 15
        Width = 137
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        ExplicitLeft = 645
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 7
          Top = 2
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
    end
  end
  object PnDados: TPanel
    Left = 0
    Top = 96
    Width = 790
    Height = 305
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    ExplicitWidth = 784
    ExplicitHeight = 295
    object GBDados: TGroupBox
      Left = 0
      Top = 0
      Width = 790
      Height = 501
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 1
      ExplicitWidth = 784
      object Label1: TLabel
        Left = 16
        Top = 16
        Width = 14
        Height = 13
        Caption = 'ID:'
        FocusControl = DBEdCodigo
      end
      object Label3: TLabel
        Left = 136
        Top = 16
        Width = 55
        Height = 13
        Caption = 'Refer'#234'ncia:'
        FocusControl = DBEdit1
      end
      object Label5: TLabel
        Left = 596
        Top = 16
        Width = 62
        Height = 13
        Caption = 'Reinser'#231#245'es:'
        FocusControl = DBEdit3
      end
      object Label6: TLabel
        Left = 664
        Top = 16
        Width = 75
        Height = 13
        Caption = #218'ltima inser'#231#227'o:'
        FocusControl = DBEdit4
      end
      object Label2: TLabel
        Left = 76
        Top = 16
        Width = 34
        Height = 13
        Caption = 'ID Ref:'
        FocusControl = dmkDBEdit1
      end
      object Label8: TLabel
        Left = 16
        Top = 56
        Width = 30
        Height = 13
        Caption = 'Artigo:'
        FocusControl = DBEdit5
      end
      object Label19: TLabel
        Left = 16
        Top = 96
        Width = 54
        Height = 13
        Caption = 'Cod.Grade:'
        FocusControl = DBEdit15
      end
      object Label20: TLabel
        Left = 124
        Top = 136
        Width = 65
        Height = 13
        Caption = 'Nr. Tamanho:'
        FocusControl = DBEdit16
      end
      object Label21: TLabel
        Left = 16
        Top = 136
        Width = 40
        Height = 13
        Caption = 'CodTam'
        FocusControl = DBEdit17
      end
      object Label22: TLabel
        Left = 152
        Top = 96
        Width = 27
        Height = 13
        Caption = 'NrCor'
        FocusControl = DBEdit18
      end
      object Label23: TLabel
        Left = 300
        Top = 96
        Width = 35
        Height = 13
        Caption = 'CodCor'
        FocusControl = DBEdit19
      end
      object Label24: TLabel
        Left = 232
        Top = 136
        Width = 65
        Height = 13
        Caption = 'Cod.Unidade:'
        FocusControl = DBEdit20
      end
      object Label25: TLabel
        Left = 340
        Top = 136
        Width = 24
        Height = 13
        Caption = 'CST:'
        FocusControl = DBEdit21
      end
      object Label26: TLabel
        Left = 448
        Top = 136
        Width = 23
        Height = 13
        Caption = 'TIPI:'
        FocusControl = DBEdit22
      end
      object Label27: TLabel
        Left = 556
        Top = 136
        Width = 44
        Height = 13
        Caption = 'Qt. Peso:'
        FocusControl = DBEdit23
      end
      object DBEdCodigo: TdmkDBEdit
        Left = 16
        Top = 32
        Width = 56
        Height = 21
        Hint = 'N'#186' do banco'
        TabStop = False
        DataField = 'Controle'
        DataSource = DsOVdProduto
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 0
        UpdType = utYes
        Alignment = taRightJustify
      end
      object DBEdit1: TDBEdit
        Left = 136
        Top = 32
        Width = 457
        Height = 21
        DataField = 'Referencia'
        DataSource = DsOVdProduto
        TabOrder = 1
      end
      object DBEdit3: TDBEdit
        Left = 596
        Top = 32
        Width = 65
        Height = 21
        DataField = 'ReInsrt'
        DataSource = DsOVdProduto
        TabOrder = 2
      end
      object DBEdit4: TDBEdit
        Left = 664
        Top = 32
        Width = 112
        Height = 21
        DataField = 'LastInsrt'
        DataSource = DsOVdProduto
        TabOrder = 3
      end
      object dmkDBEdit1: TdmkDBEdit
        Left = 76
        Top = 32
        Width = 56
        Height = 21
        Hint = 'N'#186' do banco'
        TabStop = False
        DataField = 'Codigo'
        DataSource = DsOVdProduto
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 4
        UpdType = utYes
        Alignment = taRightJustify
      end
      object DBEdit5: TDBEdit
        Left = 16
        Top = 72
        Width = 758
        Height = 21
        DataField = 'NO_Artigo'
        DataSource = DsOVdProduto
        TabOrder = 5
      end
      object Panel6: TPanel
        Left = 16
        Top = 176
        Width = 757
        Height = 53
        TabOrder = 6
        object Label10: TLabel
          Left = 8
          Top = 4
          Width = 85
          Height = 13
          Caption = 'NrInProdAcabado'
          FocusControl = DBEdit6
        end
        object Label11: TLabel
          Left = 276
          Top = 4
          Width = 64
          Height = 13
          Caption = 'NrInMatPrima'
          FocusControl = DBEdit7
        end
        object Label12: TLabel
          Left = 476
          Top = 4
          Width = 69
          Height = 13
          Caption = 'NrInPatrimonio'
          FocusControl = DBEdit8
        end
        object Label13: TLabel
          Left = 108
          Top = 4
          Width = 82
          Height = 13
          Caption = 'NrInMatConsumo'
          FocusControl = DBEdit9
        end
        object Label14: TLabel
          Left = 348
          Top = 4
          Width = 43
          Height = 13
          Caption = 'NrInativo'
          FocusControl = DBEdit10
        end
        object Label15: TLabel
          Left = 552
          Top = 4
          Width = 75
          Height = 13
          Caption = 'NrInProdPropria'
          FocusControl = DBEdit11
        end
        object Label16: TLabel
          Left = 204
          Top = 4
          Width = 59
          Height = 13
          Caption = 'NrInTerceiro'
          FocusControl = DBEdit12
        end
        object Label17: TLabel
          Left = 400
          Top = 4
          Width = 68
          Height = 13
          Caption = 'NrTpInspecao'
          FocusControl = DBEdit13
        end
        object Label18: TLabel
          Left = 644
          Top = 4
          Width = 52
          Height = 13
          Caption = 'NrPrdSped'
          FocusControl = DBEdit14
        end
        object DBEdit6: TDBEdit
          Left = 8
          Top = 20
          Width = 32
          Height = 21
          DataField = 'NrInProdAcabado'
          DataSource = DsOVdProduto
          TabOrder = 0
        end
        object DBEdit7: TDBEdit
          Left = 276
          Top = 20
          Width = 32
          Height = 21
          DataField = 'NrInMatPrima'
          DataSource = DsOVdProduto
          TabOrder = 1
        end
        object DBEdit8: TDBEdit
          Left = 476
          Top = 20
          Width = 32
          Height = 21
          DataField = 'NrInPatrimonio'
          DataSource = DsOVdProduto
          TabOrder = 2
        end
        object DBEdit9: TDBEdit
          Left = 108
          Top = 20
          Width = 32
          Height = 21
          DataField = 'NrInMatConsumo'
          DataSource = DsOVdProduto
          TabOrder = 3
        end
        object DBEdit10: TDBEdit
          Left = 348
          Top = 20
          Width = 32
          Height = 21
          DataField = 'NrInativo'
          DataSource = DsOVdProduto
          TabOrder = 4
        end
        object DBEdit11: TDBEdit
          Left = 552
          Top = 20
          Width = 32
          Height = 21
          DataField = 'NrInProdPropria'
          DataSource = DsOVdProduto
          TabOrder = 5
        end
        object DBEdit12: TDBEdit
          Left = 204
          Top = 20
          Width = 32
          Height = 21
          DataField = 'NrInTerceiro'
          DataSource = DsOVdProduto
          TabOrder = 6
        end
        object DBEdit13: TDBEdit
          Left = 400
          Top = 20
          Width = 32
          Height = 21
          DataField = 'NrTpInspecao'
          DataSource = DsOVdProduto
          TabOrder = 7
        end
        object DBEdit14: TDBEdit
          Left = 644
          Top = 19
          Width = 32
          Height = 21
          DataField = 'NrPrdSped'
          DataSource = DsOVdProduto
          TabOrder = 8
        end
      end
      object DBEdit15: TDBEdit
        Left = 16
        Top = 112
        Width = 134
        Height = 21
        DataField = 'CodGrade'
        DataSource = DsOVdProduto
        TabOrder = 7
      end
      object DBEdit16: TDBEdit
        Left = 124
        Top = 152
        Width = 104
        Height = 21
        DataField = 'NrTam'
        DataSource = DsOVdProduto
        TabOrder = 8
      end
      object DBEdit17: TDBEdit
        Left = 16
        Top = 152
        Width = 104
        Height = 21
        DataField = 'CodTam'
        DataSource = DsOVdProduto
        TabOrder = 9
      end
      object DBEdit18: TDBEdit
        Left = 152
        Top = 112
        Width = 145
        Height = 21
        DataField = 'NrCor'
        DataSource = DsOVdProduto
        TabOrder = 10
      end
      object DBEdit19: TDBEdit
        Left = 300
        Top = 112
        Width = 473
        Height = 21
        DataField = 'CodCor'
        DataSource = DsOVdProduto
        TabOrder = 11
      end
      object DBEdit20: TDBEdit
        Left = 232
        Top = 152
        Width = 104
        Height = 21
        DataField = 'CodUnidade'
        DataSource = DsOVdProduto
        TabOrder = 12
      end
      object DBEdit21: TDBEdit
        Left = 340
        Top = 152
        Width = 104
        Height = 21
        DataField = 'CodCst'
        DataSource = DsOVdProduto
        TabOrder = 13
      end
      object DBEdit22: TDBEdit
        Left = 448
        Top = 152
        Width = 104
        Height = 21
        DataField = 'CodTipi'
        DataSource = DsOVdProduto
        TabOrder = 14
      end
      object DBEdit23: TDBEdit
        Left = 556
        Top = 152
        Width = 104
        Height = 21
        DataField = 'QtPeso'
        DataSource = DsOVdProduto
        TabOrder = 15
      end
    end
    object GBCntrl: TGroupBox
      Left = 0
      Top = 241
      Width = 790
      Height = 64
      Align = alBottom
      TabOrder = 0
      ExplicitTop = 231
      ExplicitWidth = 784
      object Panel5: TPanel
        Left = 2
        Top = 15
        Width = 172
        Height = 47
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 174
        Top = 15
        Width = 94
        Height = 47
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
        ExplicitWidth = 88
      end
      object Panel3: TPanel
        Left = 268
        Top = 15
        Width = 520
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        ExplicitLeft = 262
        object BtExclui: TBitBtn
          Tag = 12
          Left = 252
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Exclui'
          Enabled = False
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          Visible = False
        end
        object BtAltera: TBitBtn
          Tag = 11
          Left = 128
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Altera'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          Visible = False
          OnClick = BtAlteraClick
        end
        object BtInclui: TBitBtn
          Tag = 10
          Left = 4
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Inclui'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          Visible = False
          OnClick = BtIncluiClick
        end
        object Panel2: TPanel
          Left = 387
          Top = 0
          Width = 133
          Height = 47
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 3
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 790
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    ExplicitWidth = 784
    object GB_R: TGroupBox
      Left = 742
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 0
      ExplicitLeft = 736
      object ImgTipo: TdmkImage
        Left = 8
        Top = 12
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 216
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
        Visible = False
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        Visible = False
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 216
      Top = 0
      Width = 526
      Height = 52
      Align = alClient
      TabOrder = 2
      ExplicitWidth = 520
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 427
        Height = 32
        Caption = ' Cadastro de Produtos (Reduzidos)'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 427
        Height = 32
        Caption = ' Cadastro de Produtos (Reduzidos)'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 427
        Height = 32
        Caption = ' Cadastro de Produtos (Reduzidos)'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 52
    Width = 790
    Height = 44
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    ExplicitWidth = 784
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 786
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 780
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 12
        Height = 17
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 12
        Height = 17
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object QrOVdProduto: TMySQLQuery
    BeforeOpen = QrOVdProdutoBeforeOpen
    AfterOpen = QrOVdProdutoAfterOpen
    SQL.Strings = (
      'SELECT art.Nome NO_Artigo, '
      'art.Referencia Referencia, prd.* '
      'FROM ovdproduto prd'
      'LEFT JOIN ovdreferencia art ON art.Codigo=prd.Codigo'
      '')
    Left = 240
    Top = 52
    object QrOVdProdutoControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrOVdProdutoCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrOVdProdutoDtCadastro: TDateTimeField
      FieldName = 'DtCadastro'
      Required = True
    end
    object QrOVdProdutoNrInProdAcabado: TWideStringField
      FieldName = 'NrInProdAcabado'
      Size = 1
    end
    object QrOVdProdutoNrInMatPrima: TWideStringField
      FieldName = 'NrInMatPrima'
      Size = 1
    end
    object QrOVdProdutoNrInPatrimonio: TWideStringField
      FieldName = 'NrInPatrimonio'
      Size = 1
    end
    object QrOVdProdutoNrInMatConsumo: TWideStringField
      FieldName = 'NrInMatConsumo'
      Size = 1
    end
    object QrOVdProdutoNrInativo: TWideStringField
      FieldName = 'NrInativo'
      Size = 1
    end
    object QrOVdProdutoNrInProdPropria: TWideStringField
      FieldName = 'NrInProdPropria'
      Size = 1
    end
    object QrOVdProdutoNrInTerceiro: TWideStringField
      FieldName = 'NrInTerceiro'
      Size = 1
    end
    object QrOVdProdutoNrTpInspecao: TWideStringField
      FieldName = 'NrTpInspecao'
      Size = 1
    end
    object QrOVdProdutoNrPrdSped: TWideStringField
      FieldName = 'NrPrdSped'
      Size = 1
    end
    object QrOVdProdutoCodGrade: TIntegerField
      FieldName = 'CodGrade'
      Required = True
    end
    object QrOVdProdutoNrTam: TIntegerField
      FieldName = 'NrTam'
      Required = True
    end
    object QrOVdProdutoCodTam: TWideStringField
      FieldName = 'CodTam'
      Size = 30
    end
    object QrOVdProdutoNrCor: TWideStringField
      FieldName = 'NrCor'
      Size = 30
    end
    object QrOVdProdutoCodCor: TWideStringField
      FieldName = 'CodCor'
      Size = 60
    end
    object QrOVdProdutoCodUnidade: TWideStringField
      FieldName = 'CodUnidade'
      Size = 60
    end
    object QrOVdProdutoCodCst: TWideStringField
      FieldName = 'CodCst'
    end
    object QrOVdProdutoCodTipi: TWideStringField
      FieldName = 'CodTipi'
    end
    object QrOVdProdutoQtPeso: TWideStringField
      FieldName = 'QtPeso'
      Size = 30
    end
    object QrOVdProdutoReInsrt: TIntegerField
      FieldName = 'ReInsrt'
      Required = True
    end
    object QrOVdProdutoLastInsrt: TDateTimeField
      FieldName = 'LastInsrt'
      Required = True
    end
    object QrOVdProdutoLk: TIntegerField
      FieldName = 'Lk'
      Required = True
    end
    object QrOVdProdutoDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrOVdProdutoDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrOVdProdutoUserCad: TIntegerField
      FieldName = 'UserCad'
      Required = True
    end
    object QrOVdProdutoUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Required = True
    end
    object QrOVdProdutoAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Required = True
    end
    object QrOVdProdutoAWServerID: TIntegerField
      FieldName = 'AWServerID'
      Required = True
    end
    object QrOVdProdutoAWStatSinc: TSmallintField
      FieldName = 'AWStatSinc'
      Required = True
    end
    object QrOVdProdutoAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
    object QrOVdProdutoNO_Artigo: TWideStringField
      FieldName = 'NO_Artigo'
      Size = 100
    end
    object QrOVdProdutoREFERENCIA: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'REFERENCIA'
      Size = 60
      Calculated = True
    end
  end
  object DsOVdProduto: TDataSource
    DataSet = QrOVdProduto
    Left = 240
    Top = 100
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = BtInclui
    CanUpd01 = BtAltera
    CanDel01 = BtExclui
    Left = 120
    Top = 64
  end
end
