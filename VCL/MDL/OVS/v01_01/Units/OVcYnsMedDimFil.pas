unit OVcYnsMedDimFil;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkPermissoes,
  mySQLDbTables, UnOVS_Vars, mySQLDirectQuery;

type
  THackWinControl = class(TWinControl);
  TFmOVcYnsMedDimFil = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Panel6: TPanel;
    QrTop: TMySQLQuery;
    QrTopNO_TolerBasCalc: TWideStringField;
    QrTopNO_TolerUnMdida: TWideStringField;
    QrTopNO_TOBIKO: TWideStringField;
    QrTopCodigo: TIntegerField;
    QrTopControle: TIntegerField;
    QrTopTobiko: TIntegerField;
    QrTopTolerBasCalc: TSmallintField;
    QrTopTolerUnMdida: TSmallintField;
    QrTopTolerRndPerc: TFloatField;
    QrTopLk: TIntegerField;
    QrTopDataCad: TDateField;
    QrTopDataAlt: TDateField;
    QrTopUserCad: TIntegerField;
    QrTopUserAlt: TIntegerField;
    QrTopAlterWeb: TSmallintField;
    QrTopAWServerID: TIntegerField;
    QrTopAWStatSinc: TSmallintField;
    QrTopAtivo: TSmallintField;
    ScrollBox1: TScrollBox;
    GPBoxes: TGridPanel;
    DqAux: TMySQLDirectQuery;
    QrRow: TMySQLQuery;
    QrRowCodGrade: TIntegerField;
    QrRowCodTam: TWideStringField;
    QrRowOrdTam: TWideStringField;
    BitBtn1: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure BitBtn1Click(Sender: TObject);
    //
    procedure EditKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure EditRedefinido(Sender: TObject);
    //
  private
    { Private declarations }
    FGradesToAdd: TGradesToAdd;
    FTamsToAdd: TTamsToAdd;
    Panels: array of array of TPanel;
    FInEdicao: Boolean;
    //
    //
    procedure ReopenTop();
    //procedure ReopenCol();
    procedure ReopenRow();
  public
    { Public declarations }
    FCodigo: Integer;
    //
(*
    procedure DefineGradeAddingRows();
    procedure DefineGradeAddingCols();
*)
    procedure DefineGradeAddingOwns(Style: TExpandStyle);
  end;

  var
  FmOVcYnsMedDimFil: TFmOVcYnsMedDimFil;

implementation

uses UnMyObjects, DmkDAC_PF, Module, UMySQLDB, UMySQLModule;

{$R *.DFM}

const
  cLinhaHeig = 24;
  cEdtValWid = 54;
  cPanTamWid = 112;
  cPanValWid = (cEdtValWid * 3);// + 16;

procedure TFmOVcYnsMedDimFil.BitBtn1Click(Sender: TObject);
var
  I: Integer;
begin
  for I := 0 to ComponentCount - 1 do
  begin
    if Components[I] is TdmkEdit then
      TdmkEdit(Components[I]).Font.Color := clRed;
  end;
end;

procedure TFmOVcYnsMedDimFil.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmOVcYnsMedDimFil.DefineGradeAddingOwns(Style: TExpandStyle);
var
  H, I, J, W, Col, Row, Cols, Rows, PanelsCount: Integer;
  AllWid, AllHei: Extended;
  Pn, Pn1, Pn2, Pn3: TPanel;
  Ed: TdmkEdit;
  _Controle, _CodGrade, _CodTam: String;
begin
  FInEdicao := False;
  GPBoxes.ExpandStyle := Style; //TExpandStyle.emFixedSize;
  //
  Col := 0;
  Row := 0;
  AllWid := 0;
  AllHei := 0;
  // Limpal GridPanel
  GPBoxes.RowCollection.BeginUpdate;
  GPBoxes.ColumnCollection.BeginUpdate;
  for J := 0 to -1 + GPBoxes.ControlCount do
    GPBoxes.Controls[0].Free;
  GPBoxes.RowCollection.Clear;
  GPBoxes.ColumnCollection.Clear;
  //
  // Definir Quantidade e dados de colunas e linhas
  ReopenRow();
  ReopenTop();
  Cols := QrTop.RecordCount + 1;
  Rows := QrRow.RecordCount + 1;
  PanelsCount := Cols * Rows;
  SetLength(Panels, Rows);
  for I := 0 to Rows - 1 do
    SetLength(Panels[I], Cols);
  //
  //GridPanel1.ControlCollection.Controls[2,3]
  W := cPanTamWid;
  for I := 0 to Cols - 1 do
  begin
    GPBoxes.ColumnCollection.Add;
    GPBoxes.ColumnCollection[I].SizeStyle := ssAbsolute;
    GPBoxes.ColumnCollection[I].Value     := W;
    AllWid := AllWid + GPBoxes.ColumnCollection[I].Value;
    W := cPanValWid;
  end;
  //
  H := cLinhaHeig;
  for I := 0 to Rows - 1 do
  begin
    GPBoxes.RowCollection.Add;
    GPBoxes.RowCollection[I].SizeStyle := ssAbsolute;
    if I = 0 then
      H := cLinhaHeig * 2
    else
      H := cLinhaHeig;
    GPBoxes.RowCollection[I].Value := H;
    AllHei := AllHei + H;
  end;
  //
  //C := 0;
  //R := 1;
  QrTop.First;
  QrRow.First;
  _Controle := '';
  _CodGrade := '';
  _CodTam   := '';
  for I := 0 to PanelsCount - 1 do
  begin
    Pn := TPanel.Create(FmOVcYnsMedDimFil);
    Pn.Parent := GPBoxes; // Adiciona na pr�xima c�lula dispon�vel!
    //Pn.Name   := 'Panel_' + Geral.FF0(I);
    //Pn.Tag    := QrTopControle.Value;
    Pn.Align := alClient;
    Pn.Alignment := taLeftJustify;
    //
    Col := (I + Cols) mod Cols;
    Row := (I (*+ Rows*)) div Cols;
    Panels[Col, Row] := Pn;
    //
    if Col = 0 then
    begin
      QrTop.First;
    end;
    if Row = 0 then
    begin
      if Col > 0 then
      begin
        Pn1 := TPanel.Create(FmOVcYnsMedDimFil);
        Pn1.Parent := Pn;
        Pn1.Height := cLinhaHeig;
        Pn1.Align := alTop;
        Pn1.Alignment := taLeftJustify;
        Pn1.Caption := ' ' + QrTopNO_TOBIKO.Value;
        //
        Pn2 := TPanel.Create(FmOVcYnsMedDimFil);
        Pn2.Parent := Pn;
        Pn2.Height := cLinhaHeig;
        Pn2.Align := alClient;
        Pn2.Alignment := taLeftJustify;
        Pn2.Caption := '';
        Pn2.BevelOuter := bvNone;
        //
        Pn3 := TPanel.Create(FmOVcYnsMedDimFil);
        Pn3.Parent := Pn2;
        Pn3.Width := cEdtValWid;
        Pn3.Align := alLeft;
        Pn3.Alignment := taLeftJustify;
        Pn3.Caption := ' Certo' ;
        //
        Pn3 := TPanel.Create(FmOVcYnsMedDimFil);
        Pn3.Parent := Pn2;
        Pn3.Width := cEdtValWid;
        Pn3.Align := alRight;
        Pn3.Alignment := taLeftJustify;
        Pn3.Caption := ' M�ximo' ;
        //
        Pn3 := TPanel.Create(FmOVcYnsMedDimFil);
        Pn3.Parent := Pn2;
        Pn3.Width := cEdtValWid;
        Pn3.Align := alClient;
        Pn3.Alignment := taLeftJustify;
        Pn3.Caption := ' M�nimo' ;
        //
        if Col < Cols then
          QrTop.Next;
      end;
    end else
    begin
      if Col = 0 then
        Pn.Caption := ' ' + QrRowCodTam.Value;
    end;
    //
    if (Row > 0) and (Col > 0) then
    begin
      Pn.Tag    := QrTopControle.Value;
      _Controle := Geral.FF0(Pn.Tag);
      //
      if Col < Cols then
        QrTop.Next;
      //
      _CodGrade := Geral.FF0(QrRowCodGrade.Value);
      _CodTam   := QrRowCodTam.Value;
      //
      if Col = Cols - 1 then
        QrRow.Next;
      //
      //Pn.Caption := '_' + _Controle + '_' + _CodGrade + '_' + _CodTam;
      //
      UnDmkDAC_PF.AbreMySQLDirectQuery0(DqAux, Dmod.MyDB, [
      'SELECT Conta, MedidCer, MedidMin, MedidMax ',
      'FROM ovcynsmeddim ',
      'WHERE Codigo=' + Geral.FF0(FCodigo),
      'AND Controle=' + _Controle,
      'AND CodGrade=' + _CodGrade,
      'AND CodTam="' + _CodTam + '"',
      '']);
      //  Medida certa -> alLeft
      Ed := TdmkEdit.Create(FmOVcYnsMedDimFil);
      Ed.Parent := Pn;
      Ed.Align := alLeft;
      Ed.Alignment := taRightJustify;
      Ed.Font.Charset := DEFAULT_CHARSET;
      Ed.Font.Color := clWindowText;
//      Ed.Font.Height := GetFontHeight(-19); //-19(*14*));
      Ed.Font.Name := 'Tahoma';
      Ed.Font.Style := [fsBold];
      Ed.Font.Color := clNavy;
      Ed.ParentFont := False;
      //Ed.ReadOnly := True;
      Ed.TabOrder := 0;
      Ed.Text := '0.000';
      Ed.FormatType := dmktfDouble;
      Ed.MskType := fmtNone;
      Ed.DecimalSize := 3;
      Ed.LeftZeros := 0;
      //Ed.NoEnterToTab := False;
      Ed.NoEnterToTab := True;
      Ed.NoForceUppercase := False;
      Ed.ForceNextYear := False;
      Ed.DataFormat := dmkdfShort;
      Ed.HoraFormat := dmkhfShort;
      Ed.Texto := '0,000';
      Ed.UpdType := utYes;
      Ed.Obrigatorio := False;
      Ed.PermiteNulo := False;
      Ed.ValueVariant := 0;
      Ed.ValWarn := False;
      Ed.Width := cEdtValWid;
      Ed.OnKeyUp := EditKeyUp;
      Ed.OnRedefinido := EditRedefinido;
      Ed.TxtPesq := _Controle;
      Ed.Tag := USQLDB.Int(DqAux, 'Conta');
      Ed.Hint := 'MedidCer';
      Ed.ValueVariant := USQLDB.Flu(DqAux, 'MedidCer');
      Ed.Name := 'Ed' + Ed.Hint + Geral.FF0(Ed.Tag);
      //
      //  Medida m�xima -> alRigth
      Ed := TdmkEdit.Create(FmOVcYnsMedDimFil);
      Ed.Parent := Pn;
      Ed.Align := alRight;
      Ed.Alignment := taRightJustify;
      Ed.Font.Charset := DEFAULT_CHARSET;
      Ed.Font.Color := clWindowText;
//      Ed.Font.Height := GetFontHeight(-19); //-19(*14*));
      Ed.Font.Name := 'Tahoma';
      Ed.Font.Style := [fsBold];
      Ed.Font.Color := clRed;
      Ed.ParentFont := False;
      //Ed.ReadOnly := True;
      //Ed.TabOrder := 0;  Vai ser 2!!
      Ed.Text := '0.000';
      Ed.FormatType := dmktfDouble;
      Ed.MskType := fmtNone;
      Ed.DecimalSize := 3;
      Ed.LeftZeros := 0;
      //Ed.NoEnterToTab := False;
      Ed.NoEnterToTab := True;
      Ed.NoForceUppercase := False;
      Ed.ForceNextYear := False;
      Ed.DataFormat := dmkdfShort;
      Ed.HoraFormat := dmkhfShort;
      Ed.Texto := '0,000';
      Ed.UpdType := utYes;
      Ed.Obrigatorio := False;
      Ed.PermiteNulo := False;
      Ed.ValueVariant := 0;
      Ed.ValWarn := False;
      Ed.Width := cEdtValWid;
      Ed.OnKeyUp := EditKeyUp;
      Ed.OnRedefinido := EditRedefinido;
      Ed.Tag := USQLDB.Int(DqAux, 'Conta');
      Ed.Hint := 'MedidMax';
      Ed.ValueVariant := USQLDB.Flu(DqAux, 'MedidMax');
      Ed.Name := 'Ed' + Ed.Hint + Geral.FF0(Ed.Tag);
      //
      //  Medida m�nima -> alClient
      Ed := TdmkEdit.Create(FmOVcYnsMedDimFil);
      Ed.Parent := Pn;
      Ed.Align := alClient;
      Ed.Alignment := taRightJustify;
      Ed.Font.Charset := DEFAULT_CHARSET;
      Ed.Font.Color := clWindowText;
//      Ed.Font.Height := GetFontHeight(-19); //-19(*14*));
      Ed.Font.Name := 'Tahoma';
      Ed.Font.Style := [fsBold];
      Ed.Font.Color := clRed;
      Ed.ParentFont := False;
      //Ed.ReadOnly := True;
      Ed.TabOrder := 1;
      Ed.Text := '0.000';
      Ed.FormatType := dmktfDouble;
      Ed.MskType := fmtNone;
      Ed.DecimalSize := 3;
      Ed.LeftZeros := 0;
      //Ed.NoEnterToTab := False;
      Ed.NoEnterToTab := True;
      Ed.NoForceUppercase := False;
      Ed.ForceNextYear := False;
      Ed.DataFormat := dmkdfShort;
      Ed.HoraFormat := dmkhfShort;
      Ed.Texto := '0,000';
      Ed.UpdType := utYes;
      Ed.Obrigatorio := False;
      Ed.PermiteNulo := False;
      Ed.ValueVariant := 0;
      Ed.ValWarn := False;
      Ed.Width := cEdtValWid;
      Ed.OnKeyUp := EditKeyUp;
      Ed.OnRedefinido := EditRedefinido;
      Ed.Tag := USQLDB.Int(DqAux, 'Conta');
      Ed.Hint := 'MedidMin';
      Ed.ValueVariant := USQLDB.Flu(DqAux, 'MedidMin');
      Ed.Name := 'Ed' + Ed.Hint + Geral.FF0(Ed.Tag);
    end;
  end;
  //GridPanel1.ControlCollection.Controls[2,3]
  GPBoxes.Width  := Trunc(AllWid + 16);
  GPBoxes.Height := Trunc(AllHei + 16);
  FInEdicao := True;
end;

procedure TFmOVcYnsMedDimFil.EditKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_RETURN then
  begin
    //Key := VK_TAB;
    PostMessage(GetParentForm(Self).Handle, wm_NextDlgCtl, Ord((ssShift in Shift)), 0);
    Key := 0;
  end;
end;

procedure TFmOVcYnsMedDimFil.EditRedefinido(Sender: TObject);
const
  UlWayInz = 1;
var
  Conta, TolerBasCalc, I: Integer;
  Campo, sEdMin, sEdMax: String;
  Valor, TolerUnMdida, TolerRndPerc, ValMin, ValMax, ValDif: Double;
begin
  if FInEdicao then
  begin
    Screen.Cursor :=crHourGlass;
    try
      Conta := TdmkEdit(Sender).Tag;
      Campo := TdmkEdit(Sender).Hint;
      Valor := TdmkEdit(Sender).ValueVariant;
      //
      //if
      UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'ovcynsmeddim', False, [
      Campo, 'UlWayInz'], [
      'Conta'], [
      Valor, UlWayInz], [
      Conta], True);
      //
      if Lowercase(Campo) = Lowercase('MedidCer') then
      begin
        UnDMkDAC_PF.AbreMySQLDirectQuery0(DqAux, Dmod.MyDB, [
        'SELECT TolerBasCalc, TolerUnMdida, TolerRndPerc ',
        'FROM ovcynsmedtop',
        'WHERE Controle=' + TdmkEdit(Sender).TxtPesq,
        '']);
        //
        TolerBasCalc := USQLDB.Int(DqAux, 'TolerBasCalc');
        TolerUnMdida := USQLDB.Flu(DqAux, 'TolerUnMdida');
        TolerRndPerc := USQLDB.Flu(DqAux, 'TolerRndPerc');
        case TolerBasCalc of
          0:(*medida*)
          begin
            ValMin := Valor - TolerRndPerc;
            ValMax := Valor + TolerRndPerc;
          end;
          1:(*percentual*)
          begin
            ValDif := (TolerRndPerc * Valor) / 100;
            ValMin := Valor - ValDif;
            ValMax := Valor + ValDif;
          end;
        end;
        sEdMin := 'Ed' + 'MedidMin' + Geral.FF0(Conta);
        sEdMax := 'Ed' + 'MedidMax' + Geral.FF0(Conta);
        for I := 0 to ComponentCount - 1 do
        begin
          if Components[I] is TdmkEdit then
          begin
            if TdmkEdit(Components[I]).Name = sEdMin then
              TdmkEdit(Components[I]).ValueVariant := ValMin
            else
            if TdmkEdit(Components[I]).Name = sEdMax then
              TdmkEdit(Components[I]).ValueVariant := ValMax;
          end;
        end;
      end;
    finally
      Screen.Cursor := crDefault;
    end;
  end;
end;

procedure TFmOVcYnsMedDimFil.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmOVcYnsMedDimFil.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  DqAux.Close;
  QrTop.Close;
  QrRow.Close;
end;

procedure TFmOVcYnsMedDimFil.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  GPBoxes.Align := alNone;
  GPBoxes.Top := 0;
  GPBoxes.Left := 0;
  FInEdicao := False;
end;

procedure TFmOVcYnsMedDimFil.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmOVcYnsMedDimFil.ReopenRow;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrRow, Dmod.MyDB, [
  'SELECT DISTINCT ymd.CodGrade, ymd.CodTam, ',
  'LPAD(CodTam, 30, " ")  OrdTam',
  'FROM ovcynsmeddim ymd ',
  'LEFT JOIN ovdgradetam ogt ON  ',
  '  ogt.Codigo=ymd.CodGrade ',
  '  AND ',
  '  ogt.Nome=ymd.CodTam ',
  'WHERE ymd.Codigo=' + Geral.FF0(FCodigo),
  'ORDER BY ogt.Ordem, OrdTam, ogt.Nome ',
  '']);
end;

procedure TFmOVcYnsMedDimFil.ReopenTop();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrTop, Dmod.MyDB, [
  'SELECT ELT(ymt.TolerBasCalc+1, "Medida", "Percentual") NO_TolerBasCalc, ',
  'ELT(ymt.TolerUnMdida+1, "cm", "inch") NO_TolerUnMdida,  ',
  'ygt.Nome NO_TOBIKO, ymt.* ',
  'FROM ovcynsmedtop ymt ',
  'LEFT JOIN ovcynsgratop ygt ON ygt.Codigo=ymt.Tobiko ',
  'WHERE ymt.Codigo=' + Geral.FF0(FCodigo),
  '']);
  //
  //QrTop.Locate('Controle', Controle, []);
end;

//Falta no cadastro o valor do arredondamento!


end.
