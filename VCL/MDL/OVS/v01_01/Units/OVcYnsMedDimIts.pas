unit OVcYnsMedDimIts;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkPermissoes,
  dmkStringGrid, mySQLDbTables;

type
  TFmOVcYnsMedDimIts = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    SGTam: TStringGrid;
    QrTam: TMySQLQuery;
    QrTamCodTam: TWideStringField;
    QrTamNO_Tobiko: TWideStringField;
    QrTamTobiko: TIntegerField;
    QrTamTolerBasCalc: TSmallintField;
    QrTamTolerRndPerc: TFloatField;
    QrTamCodigo: TIntegerField;
    QrTamControle: TIntegerField;
    QrTamConta: TIntegerField;
    QrTamCodGrade: TIntegerField;
    QrTamCodTam_1: TWideStringField;
    QrTamMedidCer: TFloatField;
    QrTamMedidMin: TFloatField;
    QrTamMedidMax: TFloatField;
    QrTamUlWayInz: TSmallintField;
    QrTamLk: TIntegerField;
    QrTamDataCad: TDateField;
    QrTamDataAlt: TDateField;
    QrTamUserCad: TIntegerField;
    QrTamUserAlt: TIntegerField;
    QrTamAlterWeb: TSmallintField;
    QrTamAWServerID: TIntegerField;
    QrTamAWStatSinc: TSmallintField;
    QrTamAtivo: TSmallintField;
    Memo1: TMemo;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SGTamDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect;
      State: TGridDrawState);
    procedure SGTamSetEditText(Sender: TObject; ACol, ARow: Integer;
      const Value: string);
    procedure SGTamKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure SGTamKeyPress(Sender: TObject; var Key: Char);
    procedure SGTamKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure SGTamExit(Sender: TObject);
    procedure SGTamSelectCell(Sender: TObject; ACol, ARow: Integer;
      var CanSelect: Boolean);
  private
    { Private declarations }
    FCol, FRow, FCountCol, FFixCol, FFixRow: Integer;
    FIDx, F: String;
    FValue: String;
    //
    procedure AlteraRegistro(ACol, ARow: Integer; Value: String; Next: Boolean);
  public
    { Public declarations }
    FCodigo: Integer;
    procedure ReopenTam();
    procedure PreeencheGrade();
  end;

  var
  FmOVcYnsMedDimIts: TFmOVcYnsMedDimIts;

implementation

uses UnMyObjects, DmkDAC_PF, Module;

{$R *.DFM}

const
  FUnidadeCol = 3;
  FFatrMulCol = 4;
  FCertValCol = 5;
  FMiniValCol = 6;
  FMaxiValCol = 7;

procedure TFmOVcYnsMedDimIts.AlteraRegistro(ACol, ARow: Integer; Value: String;
  Next: Boolean);
var
  NCol, NRow: Integer;
  MinVal, MaxVal, Campo, SQL: String;
  Valor: Double;
begin
  if (FCol >= FFixCol) and (FRow >= FFixRow) then
  begin
    if Next then
    begin
      NCol := ACol + 1;
      NRow := ARow;
      if NCol = FCountCol then
      begin
        NCol := FFixCol;
        NRow := NRow + 1;
      end;
      if NRow < SGTam.RowCount then
      begin
        SGTam.Row := NRow;
        SGTam.Col := NCol;
      end;
    end;
    if (FCol <> ACol) or (FRow <> ARow) then
    begin
      if (FCol >= FFixCol) and (FRow >= FFixRow) then
      begin
        //if Value = '' then
        Valor := Geral.DMV(Value);
        Value := Geral.FFT(Valor, 2, siNegativo);
          //Value := '0';
        if FCol = FCertValCol then
        begin
          MinVal := SGTam.Cells[FMiniValCol, FRow];
          MaxVal := SGTam.Cells[FMaxiValCol, FRow];
          //
          SQL := 'UPDATE ovcynsmeddim SET ' +
            ' MedidCer=' + Geral.VariavelToString(Valor) + ',' +
            ' MedidMin=' + Geral.VariavelToString(Geral.DMV(MinVal)) + ',' +
            ' MedidMax=' + Geral.VariavelToString(Geral.DMV(MaxVal)) +
            ' WHERE Conta=' + FIDx;

        end else
        begin
          case FCol of
            FMiniValCol: Campo := 'MedidMin';
            FMaxiValCol: Campo := 'MedidMax';
            else Campo := '?*CAMPO*?';
          end;
          SQL := 'UPDATE ovcynsmeddim SET ' +
            ' ' + Campo + '=' + Geral.VariavelToString(Valor) +
            ' WHERE Conta=' + FIDx;
        end;
        Dmod.MyDB.Execute(SQL);
        //Memo1.Text := SQL + sLineBreak + Memo1.Text;
        //
        FCol   := -1;
        FRow   := -1;
        FValue := '';
        FIDx   := '';
      end;
    end;
  end;
end;

procedure TFmOVcYnsMedDimIts.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmOVcYnsMedDimIts.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmOVcYnsMedDimIts.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  SGTam.Cells[0,0] := 'ID';
  SGTam.Cells[1,0] := 'Tamanho';
  SGTam.Cells[2,0] := 'T�pico';
  SGTam.Cells[FUnidadeCol,0] := 'Un';
  SGTam.Cells[FFatrMulCol,0] := 'Fator';
  SGTam.Cells[FCertValCol,0] := 'Certo';
  SGTam.Cells[FMiniValCol,0] := 'M�nimo';
  SGTam.Cells[FMaxiValCol,0] := 'M�ximo';
  //
  SGTam.ColWidths[0] :=  48;
  SGTam.ColWidths[1] := 100;
  SGTam.ColWidths[2] := 300;
  SGTam.ColWidths[FUnidadeCol] :=  36;
  SGTam.ColWidths[FFatrMulCol] :=  46;
  SGTam.ColWidths[FCertValCol] :=  56;
  SGTam.ColWidths[FMiniValCol] :=  56;
  SGTam.ColWidths[FMaxiValCol] :=  56;
  //
  FCountCol := SGTam.ColCount;
  FFixCol   := SGTam.FixedCols;
  FFixRow   := SGTam.FixedRows;
end;

procedure TFmOVcYnsMedDimIts.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmOVcYnsMedDimIts.PreeencheGrade();
var
  Row: Integer;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrTam, Dmod.MyDB, [
  'SELECT ymd.CodTam, /*LPAD(ymd.CodTam, 30, " ") OrdTam,*/ ',
  'ygt.Nome NO_Tobiko,  ',
  'ymt.Tobiko, ygt.TolerBasCalc, ygt.TolerRndPerc, ymd.*   ',
  'FROM ovcynsmeddim ymd   ',
  'LEFT JOIN ovcynsmedtop ymt ON ymt.Controle=ymd.Controle ',
  'LEFT JOIN ovcynsgratop ygt ON ygt.Codigo=ymt.Tobiko ',
  'WHERE ymd.Codigo=' + Geral.FF0(FCodigo),
  'ORDER BY /*OrdTam*/ymd.CodTam, ymt.Controle ',
  '']);
  MyObjects.LimpaGrade(SGTam, 1, 1, True);
  SGTam.RowCount := QrTam.RecordCount + 1;
  Row := 0;
  QrTam.First;
  while not QrTam.Eof do
  begin
    Row := Row + 1;
    SGTam.Cells[0, Row] := Geral.FF0(QrTamConta.Value);
    SGTam.Cells[1, Row] := QrTamCodTam.Value;
    SGTam.Cells[2, Row] := QrTamNO_Tobiko.Value;
    if QrTamTolerBasCalc.Value = 0 then
      SGTam.Cells[FUnidadeCol, Row] := 'M'
    else
      SGTam.Cells[FUnidadeCol, Row] := '%';
    SGTam.Cells[FFatrMulCol, Row] := Geral.FFT(QrTamTolerRndPerc.Value, 3, siNegativo);
    SGTam.Cells[FCertValCol, Row] := Geral.FFT(QrTamMedidCer.Value, 3, siNegativo);
    SGTam.Cells[FMiniValCol, Row] := Geral.FFT(QrTamMedidMin.Value, 3, siNegativo);
    SGTam.Cells[FMaxiValCol, Row] := Geral.FFT(QrTamMedidMax.Value, 3, siNegativo);
    //
    QrTam.Next;
  end;
end;

procedure TFmOVcYnsMedDimIts.ReopenTam;
begin

end;

procedure TFmOVcYnsMedDimIts.SGTamDrawCell(Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);
var
  a, Cor: TColor;
  Val: Double;
begin
  if (ACol < FFixCol) or (ARow < FFixRow) then
    EXIT//Cor := Panel1.Color
  else
    Cor := clWindow;
  if ACol < FFixCol then
    MyObjects.DesenhaTextoEmStringGrid(SGTam, Rect, clBlack,
      Cor, taLeftJustify,
      SGTam.Cells[Acol, ARow], FFixCol, FFixRow, False)
  else
  begin
    Val := Geral.DMV(SGTam.Cells[Acol, ARow]);
    MyObjects.DesenhaTextoEmStringGrid(SGTam, Rect, clBlack,
      Cor, taRightJustify,
      Geral.FFT(Val, 3, siNegativo), FFixCol, FFixRow, False);
  end;
end;

procedure TFmOVcYnsMedDimIts.SGTamExit(Sender: TObject);
begin
  AlteraRegistro(0(*ACol*), 0(*ARow*), FValue, False);
end;

procedure TFmOVcYnsMedDimIts.SGTamKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
 // if Key = VK_RETURN then
   // Geral.MB_Info('KeyDown: ' + FValue);
end;

procedure TFmOVcYnsMedDimIts.SGTamKeyPress(Sender: TObject; var Key: Char);
begin
  if Key = #13 then
    AlteraRegistro(FCol, FRow, FValue, True);
end;

procedure TFmOVcYnsMedDimIts.SGTamKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
//  if Key = VK_RETURN then
  //  Geral.MB_Info('KeyUP: ' + FValue);
end;

procedure TFmOVcYnsMedDimIts.SGTamSelectCell(Sender: TObject; ACol,
  ARow: Integer; var CanSelect: Boolean);
begin
  AlteraRegistro(ACol, ARow, FValue, False);
end;

procedure TFmOVcYnsMedDimIts.SGTamSetEditText(Sender: TObject; ACol,
  ARow: Integer; const Value: string);
var
  MedidCer, MedidMin, MedidMax, fFator, fDifer: Double;
  sUnidade, sFator: String;
begin
  //Geral.MB_Info(Value);
  //AlteraRegistro(ACol, ARow, Value, False);
  FIDx    := SGTam.Cells[0, ARow];
  FCol   := ACol;
  FRow   := ARow;
  FValue := Value;
  if ACol = FCertValCol then
  begin
    if Value = '' then
      MedidCer := 0
    else
      MedidCer := Geral.DMV(Value);
    //
    sUnidade := SGTam.Cells[FUnidadeCol, ARow];
    sFator   := SGTam.Cells[FFatrMulCol, ARow];
    fFator   := Geral.DMV(sFator);
    //
    if sUnidade = 'M' then
    begin
      MedidMin := MedidCer - fFator;
      MedidMax := MedidCer + fFator;
    end else
    if sUnidade = '%' then
    begin
      fDifer := (fFator * MedidCer) / 100;
      MedidMin := MedidCer - fDifer;
      MedidMax := MedidCer + fDifer;
    end;
    SGTam.Cells[FMiniValCol, FRow] := Geral.FFT(MedidMin, 3, siNegativo);
    SGTam.Cells[FMaxiValCol, FRow] := Geral.FFT(MedidMax, 3, siNegativo);
  end;
end;

end.

