unit OVcYnsChkCad;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2, dmkGeral,
  ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt, UMySQLModule, Menus,
  UnInternalConsts2, mySQLDbTables, UnMySQLCuringa, dmkPermissoes, dmkEdit,
  dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup, Grids, DBGrids,
  UnDmkProcFunc, UnDmkENums, dmkDBLookupComboBox, dmkEditCB, Variants;

type
  TFmOVcYnsChkCad = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    Label9: TLabel;
    EdNome: TdmkEdit;
    Label1: TLabel;
    DBEdCodigo: TdmkDBEdit;
    Label2: TLabel;
    DBEdNome: TdmkDBEdit;
    QrOVcYnsChkCad: TMySQLQuery;
    DsOVcYnsChkCad: TDataSource;
    QrOVcYnsChkCtx: TMySQLQuery;
    DsOVcYnsChkCtx: TDataSource;
    PMIts: TPopupMenu;
    ItsInclui1: TMenuItem;
    ItsExclui1: TMenuItem;
    ItsAltera1: TMenuItem;
    PMCab: TPopupMenu;
    CabInclui1: TMenuItem;
    CabAltera1: TMenuItem;
    CabExclui1: TMenuItem;
    BtCab: TBitBtn;
    BtIts: TBitBtn;
    QrEntidades: TMySQLQuery;
    DsEntidades: TDataSource;
    QrOVdReferencia: TMySQLQuery;
    DsOVdReferencia: TDataSource;
    QrEntidadesCodigo: TIntegerField;
    QrOVdReferenciaCodigo: TIntegerField;
    QrOVdReferenciaNome: TWideStringField;
    Label3: TLabel;
    EdArtigRef: TdmkEditCB;
    CBArtigRef: TdmkDBLookupComboBox;
    Label4: TLabel;
    EdClientRef: TdmkEditCB;
    CBClientRef: TdmkDBLookupComboBox;
    SbArtigRef: TSpeedButton;
    SbClientRef: TSpeedButton;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    DBEdit4: TDBEdit;
    QrEntidadesNO_ENT: TWideStringField;
    QrOVcYnsChkCadNO_ClientRef: TWideStringField;
    QrOVcYnsChkCadNO_ArtigRef: TWideStringField;
    QrOVcYnsChkCadCodigo: TIntegerField;
    QrOVcYnsChkCadNome: TWideStringField;
    QrOVcYnsChkCadArtigRef: TIntegerField;
    QrOVcYnsChkCadClientRef: TIntegerField;
    Label5: TLabel;
    Label6: TLabel;
    QrOVcYnsChkCtxNO_CONTEXTO: TWideStringField;
    QrOVcYnsChkCtxCodigo: TIntegerField;
    QrOVcYnsChkCtxControle: TIntegerField;
    QrOVcYnsChkCtxContexto: TIntegerField;
    BtTop: TBitBtn;
    PMTop: TPopupMenu;
    IncluiTopico1: TMenuItem;
    AlteraTopico1: TMenuItem;
    ExcluiTopico1: TMenuItem;
    QrOVcYnsChkTop: TMySQLQuery;
    DsOVcYnsChkTop: TDataSource;
    QrOVcYnsChkTopCodigo: TIntegerField;
    QrOVcYnsChkTopControle: TIntegerField;
    QrOVcYnsChkTopConta: TIntegerField;
    QrOVcYnsChkTopTopico: TIntegerField;
    QrOVcYnsChkTopMagnitude: TIntegerField;
    QrOVcYnsChkTopLk: TIntegerField;
    QrOVcYnsChkTopDataCad: TDateField;
    QrOVcYnsChkTopDataAlt: TDateField;
    QrOVcYnsChkTopUserCad: TIntegerField;
    QrOVcYnsChkTopUserAlt: TIntegerField;
    QrOVcYnsChkTopAlterWeb: TSmallintField;
    QrOVcYnsChkTopAWServerID: TIntegerField;
    QrOVcYnsChkTopAWStatSinc: TSmallintField;
    QrOVcYnsChkTopAtivo: TSmallintField;
    QrOVcYnsChkTopNO_Topico: TWideStringField;
    QrOVcYnsChkTopNO_Magnitude: TWideStringField;
    PnGrids: TPanel;
    DBGTopico: TDBGrid;
    DBGContexto: TDBGrid;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrOVcYnsChkCadAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrOVcYnsChkCadBeforeOpen(DataSet: TDataSet);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure QrOVcYnsChkCadAfterScroll(DataSet: TDataSet);
    procedure CabInclui1Click(Sender: TObject);
    procedure CabAltera1Click(Sender: TObject);
    procedure CabExclui1Click(Sender: TObject);
    procedure BtCabClick(Sender: TObject);
    procedure BtItsClick(Sender: TObject);
    procedure ItsInclui1Click(Sender: TObject);
    procedure ItsExclui1Click(Sender: TObject);
    procedure ItsAltera1Click(Sender: TObject);
    procedure PMCabPopup(Sender: TObject);
    procedure PMItsPopup(Sender: TObject);
    procedure QrOVcYnsChkCadBeforeClose(DataSet: TDataSet);
    procedure SbClientRefClick(Sender: TObject);
    procedure SbArtigRefClick(Sender: TObject);
    procedure BtTopClick(Sender: TObject);
    procedure IncluiTopico1Click(Sender: TObject);
    procedure AlteraTopico1Click(Sender: TObject);
    procedure PMTopPopup(Sender: TObject);
    procedure QrOVcYnsChkCtxBeforeClose(DataSet: TDataSet);
    procedure ExcluiTopico1Click(Sender: TObject);
    procedure QrOVcYnsChkCtxAfterScroll(DataSet: TDataSet);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure LocCod(Atual, Codigo: Integer);
    procedure Va(Para: TVaiPara);
    procedure MostraFormOVcYnsChkCtx(SQLType: TSQLType);
    procedure MostraFormOVcYnsChkTop(SQLType: TSQLType);

  public
    { Public declarations }
    FSeq, FCabIni: Integer;
    FLocIni: Boolean;
    //
    procedure ReopenOVcYnsChkCtx(Controle: Integer);
    procedure ReopenOVcYnsChkTop(Conta: Integer);
    procedure ReopenEntidades();
    procedure ReopenOVdReferencia();


  end;

var
  FmOVcYnsChkCad: TFmOVcYnsChkCad;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, MyDBCheck, DmkDAC_PF, UnOVS_Jan, UnEntities,
  OVcYnsChkTop;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmOVcYnsChkCad.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmOVcYnsChkCad.MostraFormOVcYnsChkCtx(SQLType: TSQLType);
const
  Aviso  = '...';
  Titulo = 'Sele��o de Contexto de Check List';
  Prompt = 'Informe o Contexto de Check List:';
  Campo  = 'Descricao';
  MostraSbCadastro = True;
var
  Codigo, Controle, Contexto: Integer;
  OVcYnsQstCtx: Variant;
begin
  OVcYnsQstCtx := DBCheck.EscolheCodigoUnico(Aviso, Titulo, Prompt, nil, nil, Campo, 0, [
  'SELECT Codigo, Nome ' + Campo,
  'FROM ovcynsqstctx ',
  'ORDER BY ' + Campo,
  ''], Dmod.MyDB, True, MostraSbCadastro, 'ovcynsqstctx', UnDmkEnums.ncGerlSeq1);
  if OVcYnsQstCtx <> Null then
  begin
    Codigo         := QrOVcYnsChkCadCodigo.Value;
    Controle       := 0;
    Contexto       := Integer(OVcYnsQstCtx);
    //
    if Contexto <> 0 then
    begin
      Controle := UMyMod.BPGS1I32('ovcynschkctx', 'Controle', '', '', tsPos, SQLType, Controle);
      if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'ovcynschkctx', False, [
      'Codigo', 'Contexto'], [
      'Controle'], [
      Codigo, Contexto], [
      Controle], True) then
      begin
        ReopenOVcYnsChkCtx(Controle);
      end;
    end;
  end;
end;

procedure TFmOVcYnsChkCad.MostraFormOVcYnsChkTop(SQLType: TSQLType);
begin
  if UmyMod.FormInsUpd_Cria(TFmOVcYnsChkTop, FmOVcYnsChkTop, afmoNegarComAviso,
    QrOVcYnsChkTop, SQLType) then
  begin
    FmOVcYnsChkTop.FDsCad := DsOVcYnsChkCad;
    FmOVcYnsChkTop.FDsCtx := DsOVcYnsChkCtx;
    FmOVcYnsChkTop.FQrTop := QrOVcYnsChkTop;
    //
    FmOVcYnsChkTop.FContexRef := QrOVcYnsChkCtxContexto.Value;
    //
    FmOVcYnsChkTop.DBEdCodigo.DataSource := DsOVcYnsChkCad;
    FmOVcYnsChkTop.DBEdNome.DataSource   := DsOVcYnsChkCad;
    //
    FmOVcYnsChkTop.DBEdControle.DataSource := DsOVcYnsChkCtx;
    FmOVcYnsChkTop.DBEdCtrlNome.DataSource := DsOVcYnsChkCtx;
    //
    FmOVcYnsChkTop.FContexRef := QrOVcYnsChkCtxContexto.Value;
    //
    FmOVcYnsChkTop.ShowModal;
    FmOVcYnsChkTop.Destroy;
    //
  end;
end;

procedure TFmOVcYnsChkCad.PMCabPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemCabUpd(CabAltera1, QrOVcYnsChkCad);
  MyObjects.HabilitaMenuItemCabDel(CabExclui1, QrOVcYnsChkCad, QrOVcYnsChkCtx);
end;

procedure TFmOVcYnsChkCad.PMItsPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(ItsInclui1, QrOVcYnsChkCad);
  MyObjects.HabilitaMenuItemItsUpd(ItsAltera1, QrOVcYnsChkCtx);
  MyObjects.HabilitaMenuItemCabDel(ItsExclui1, QrOVcYnsChkCtx, QrOVcYnsChkTop);
end;

procedure TFmOVcYnsChkCad.PMTopPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(IncluiTopico1, QrOVcYnsChkCtx);
  MyObjects.HabilitaMenuItemItsUpd(AlteraTopico1, QrOVcYnsChkTop);
  MyObjects.HabilitaMenuItemItsDel(ExcluiTopico1, QrOVcYnsChkTop);

end;

procedure TFmOVcYnsChkCad.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrOVcYnsChkCadCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmOVcYnsChkCad.DefParams;
begin
  VAR_GOTOTABELA := 'ovcynschkcad';
  VAR_GOTOMYSQLTABLE := QrOVcYnsChkCad;
  VAR_GOTONEG := gotoPiZ;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_ClientRef, ');
  VAR_SQLx.Add('art.Nome NO_ArtigRef, ycc.* ');
  VAR_SQLx.Add('FROM ovcynschkcad ycc');
  VAR_SQLx.Add('LEFT JOIN ovdreferencia art ON art.Codigo=ycc.ArtigRef ');
  VAR_SQLx.Add('LEFT JOIN entidades ent ON ent.Codigo=ycc.ClientRef ');
  //
  VAR_SQL1.Add('WHERE ycc.Codigo>0');
  VAR_SQL1.Add('AND ycc.Codigo=:P0');
  //
  //VAR_SQL2.Add('AND CodUsu=:P0');
  //
  VAR_SQLa.Add('AND ycc.Nome Like :P0');
  //
end;

procedure TFmOVcYnsChkCad.ExcluiTopico1Click(Sender: TObject);
begin
 DBCheck.QuaisItens_Exclui(Dmod.QrUpd, QrOVcYnsChkTop, TDBGrid(DBGTopico),
   'OVcYnsChkTop', ['Conta'], ['Conta'], istPergunta, '');
end;

procedure TFmOVcYnsChkCad.IncluiTopico1Click(Sender: TObject);
begin
  MostraFormOVcYnsChkTop(stIns);
end;

procedure TFmOVcYnsChkCad.ItsAltera1Click(Sender: TObject);
begin
  //MostraFormOVcYnsChkCtx(stUpd);
end;

procedure TFmOVcYnsChkCad.CabExclui1Click(Sender: TObject);
begin
  Geral.MB_Info('A��o n�o implementada! Solicite � Dermatek:' + sLineBreak +
  Caption + sLineBreak + TMenuItem(Sender).Name);
end;

procedure TFmOVcYnsChkCad.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmOVcYnsChkCad.QueryPrincipalAfterOpen;
begin
end;

procedure TFmOVcYnsChkCad.ItsExclui1Click(Sender: TObject);
var
  Controle: Integer;
begin
  if UMyMod.ExcluiRegistroInt1('Confirma a exclus�o do contexto selecionado?',
  'ovcynschkctx', 'Controle', QrOVcYnsChkCtxControle.Value, Dmod.MyDB) = ID_YES then
  begin
    Controle := GOTOy.LocalizaPriorNextIntQr(QrOVcYnsChkCtx,
      QrOVcYnsChkCtxControle, QrOVcYnsChkCtxControle.Value);
    ReopenOVcYnsChkCtx(Controle);
  end;
end;

procedure TFmOVcYnsChkCad.ReopenEntidades();
begin
  UnDMkDAC_PF.AbreQuery(QrEntidades, DMod.MyDB);
end;

procedure TFmOVcYnsChkCad.ReopenOVdReferencia;
begin
  UnDMkDAC_PF.AbreQuery(QrOVdReferencia, DMod.MyDB);
end;

procedure TFmOVcYnsChkCad.ReopenOVcYnsChkCtx(Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrOVcYnsChkCtx, Dmod.MyDB, [
  'SELECT yqt.Nome NO_CONTEXTO, ycc.*  ',
  'FROM ovcynschkctx ycc ',
  'LEFT JOIN ovcynsqstctx yqt ON yqt.Codigo=ycc.Contexto ',
  'WHERE ycc.Codigo=' + Geral.FF0(QrOVcYnsChkCadCodigo.Value),
  '']);
  //
  QrOVcYnsChkCtx.Locate('Controle', Controle, []);
end;


procedure TFmOVcYnsChkCad.ReopenOVcYnsChkTop(Conta: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrOVcYnsChkTop, Dmod.MyDB, [
  'SELECT yqc.Nome NO_Topico, yqm.Nome NO_Magnitude, yct.* ',
  'FROM ovcynschktop yct ',
  'LEFT JOIN ovcynsqsttop yqc ON yqc.Codigo=yct.Topico ',
  'LEFT JOIN ovcynsqstmag yqm ON yqm.Codigo=yct.Magnitude ',
  'WHERE yct.Controle=' + Geral.FF0(QrOVcYnsChkCtxControle.Value),
  '']);
  //
  QrOVcYnsChkTop.Locate('Conta', Conta, []);
end;

procedure TFmOVcYnsChkCad.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmOVcYnsChkCad.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmOVcYnsChkCad.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmOVcYnsChkCad.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmOVcYnsChkCad.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmOVcYnsChkCad.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmOVcYnsChkCad.BtTopClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMTop, BtTop);
end;

procedure TFmOVcYnsChkCad.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrOVcYnsChkCadCodigo.Value;
  Close;
end;

procedure TFmOVcYnsChkCad.ItsInclui1Click(Sender: TObject);
begin
  MostraFormOVcYnsChkCtx(stIns);
end;

procedure TFmOVcYnsChkCad.CabAltera1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrOVcYnsChkCad, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'ovcynschkcad');
end;

procedure TFmOVcYnsChkCad.BtConfirmaClick(Sender: TObject);
var
  Nome: String;
  Codigo, ArtigRef, ClientRef: Integer;
  SQLType: TSQLType;
begin
  SQLType        := ImgTipo.SQLType;
  Codigo         := EdCodigo.ValueVariant;
  Nome           := EdNome.ValueVariant;
  ArtigRef       := EdArtigRef.ValueVariant;
  ClientRef      := EdClientRef.ValueVariant;
  //
  if MyObjects.FIC(Length(Nome) = 0, EdNome, 'Defina uma descri��o!') then Exit;
  //
  Codigo := UMyMod.BPGS1I32('ovcynschkcad', 'Codigo', '', '', tsPos, SQLType, Codigo);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'ovcynschkcad', False, [
  'Nome', 'ArtigRef', 'ClientRef'], [
  'Codigo'], [
  Nome, ArtigRef, ClientRef], [
  Codigo], True) then
  begin
    ImgTipo.SQLType := stLok;
    PnDados.Visible := True;
    PnEdita.Visible := False;
    GOTOy.BotoesSb(ImgTipo.SQLType);
    //
    LocCod(Codigo, Codigo);
    if FSeq = 1 then Close;
  end;
end;

procedure TFmOVcYnsChkCad.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := EdCodigo.ValueVariant;
  if ImgTipo.SQLType = stIns then
    UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'ovcynschkcad', Codigo);
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'ovcynschkcad', 'Codigo');
end;

procedure TFmOVcYnsChkCad.BtItsClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMIts, BtIts);
end;

procedure TFmOVcYnsChkCad.AlteraTopico1Click(Sender: TObject);
begin
  MostraFormOVcYnsChkTop(stUpd);
end;

procedure TFmOVcYnsChkCad.BtCabClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMCab, BtCab);
end;

procedure TFmOVcYnsChkCad.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  GBEdita.Align := alClient;
  PnGrids.Align := alClient;
  CriaOForm;
  FSeq := 0;
  QrOVcYnsChkCad.Database := Dmod.MyDB;
  ReopenOVdReferencia();
  ReopenEntidades();
end;

procedure TFmOVcYnsChkCad.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrOVcYnsChkCadCodigo.Value, LaRegistro.Caption);
end;

procedure TFmOVcYnsChkCad.SbArtigRefClick(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  OVS_Jan.MostraFormOVdReferencia();
  if VAR_CADASTRO <> 0 then
  begin
      UMyMod.SetaCodigoPesquisado(EdArtigRef, CBArtigRef, QrOVdReferencia, VAR_CADASTRO);
      EdArtigRef.SetFocus;
  end else
    ReopenOVdReferencia();
end;

procedure TFmOVcYnsChkCad.SbClientRefClick(Sender: TObject);
begin
  Entities.CadastroDeEntidade(0, fmCadSelecionar, fmCadSelecionar, False);
  VAR_CADASTRO := 0;
  if VAR_CADASTRO <> 0 then
  begin
      UMyMod.SetaCodigoPesquisado(EdClientRef, CBClientRef, QrENtidades, VAR_CADASTRO);
      EdClientRef.SetFocus;
  end else
    ReopenEntidades();
end;

procedure TFmOVcYnsChkCad.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmOVcYnsChkCad.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrOVcYnsChkCadCodigo.Value, LaRegistro.Caption);
end;

procedure TFmOVcYnsChkCad.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmOVcYnsChkCad.QrOVcYnsChkCadAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmOVcYnsChkCad.QrOVcYnsChkCadAfterScroll(DataSet: TDataSet);
begin
  ReopenOVcYnsChkCtx(0);
end;

procedure TFmOVcYnsChkCad.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //
  if FSeq = 1 then
    CabInclui1Click(Self)
  else
  if (not FLocIni) and (FCabIni <> 0)  then
  begin
    LocCod(FCabIni, FCabIni);
    if QrOVcYnsChkCadCodigo.Value <> FCabIni then
      Geral.MB_Aviso('Grupo n�o localizado!');
    FLocIni := True;
  end;
end;

procedure TFmOVcYnsChkCad.SbQueryClick(Sender: TObject);
begin
  LocCod(QrOVcYnsChkCadCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'ovcynschkcad', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmOVcYnsChkCad.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmOVcYnsChkCad.CabInclui1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrOVcYnsChkCad, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'ovcynschkcad');
end;

procedure TFmOVcYnsChkCad.QrOVcYnsChkCadBeforeClose(
  DataSet: TDataSet);
begin
  QrOVcYnsChkCtx.Close;
end;

procedure TFmOVcYnsChkCad.QrOVcYnsChkCadBeforeOpen(DataSet: TDataSet);
begin
  QrOVcYnsChkCadCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmOVcYnsChkCad.QrOVcYnsChkCtxAfterScroll(DataSet: TDataSet);
begin
  ReopenOVcYnsChkTop(0);
end;

procedure TFmOVcYnsChkCad.QrOVcYnsChkCtxBeforeClose(DataSet: TDataSet);
begin
  QrOVcYnsChkTop.Close;
end;

end.

