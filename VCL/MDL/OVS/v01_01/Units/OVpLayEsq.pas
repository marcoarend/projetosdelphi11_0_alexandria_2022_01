unit OVpLayEsq;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2, dmkGeral,
  ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt, UMySQLModule, Menus,
  UnInternalConsts2, mySQLDbTables, UnMySQLCuringa, dmkPermissoes, dmkEdit,
  dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup, Grids, DBGrids,
  UnDmkProcFunc, UnDmkENums, Vcl.ComCtrls;

type
  TFmOVpLayEsq = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    Label9: TLabel;
    EdNome: TdmkEdit;
    Label1: TLabel;
    DBEdCodigo: TdmkDBEdit;
    Label2: TLabel;
    DBEdNome: TdmkDBEdit;
    QrOVpLayEsq: TMySQLQuery;
    DsOVpLayEsq: TDataSource;
    QrOVpLayTab: TMySQLQuery;
    DsOVpLayTab: TDataSource;
    PMIts: TPopupMenu;
    ItsInclui1: TMenuItem;
    ItsExclui1: TMenuItem;
    ItsAltera1: TMenuItem;
    PMCab: TPopupMenu;
    CabInclui1: TMenuItem;
    CabAltera1: TMenuItem;
    CabExclui1: TMenuItem;
    BtCab: TBitBtn;
    BtIts: TBitBtn;
    DBGOVpLayTab: TDBGrid;
    QrOVpLayEsqCodigo: TIntegerField;
    QrOVpLayEsqNome: TWideStringField;
    QrOVpLayTabCodigo: TIntegerField;
    QrOVpLayTabControle: TIntegerField;
    QrOVpLayTabNome: TWideStringField;
    QrOVpLayTabTabela: TWideStringField;
    QrOVpLayTabLastLoad: TDateTimeField;
    QrOVpLayTabLk: TIntegerField;
    QrOVpLayTabDataCad: TDateField;
    QrOVpLayTabDataAlt: TDateField;
    QrOVpLayTabUserCad: TIntegerField;
    QrOVpLayTabUserAlt: TIntegerField;
    QrOVpLayTabAlterWeb: TSmallintField;
    QrOVpLayTabAWServerID: TIntegerField;
    QrOVpLayTabAWStatSinc: TSmallintField;
    QrOVpLayTabAtivo: TSmallintField;
    QrOVpLayFld: TMySQLQuery;
    DsOVpLayFld: TDataSource;
    QrOVpLayFldCodigo: TIntegerField;
    QrOVpLayFldControle: TIntegerField;
    QrOVpLayFldConta: TIntegerField;
    QrOVpLayFldColuna: TIntegerField;
    QrOVpLayFldCampo: TWideStringField;
    QrOVpLayFldNome: TWideStringField;
    QrOVpLayFldDataType: TWideStringField;
    QrOVpLayFldLk: TIntegerField;
    QrOVpLayFldDataCad: TDateField;
    QrOVpLayFldDataAlt: TDateField;
    QrOVpLayFldUserCad: TIntegerField;
    QrOVpLayFldUserAlt: TIntegerField;
    QrOVpLayFldAlterWeb: TSmallintField;
    QrOVpLayFldAWServerID: TIntegerField;
    QrOVpLayFldAWStatSinc: TSmallintField;
    QrOVpLayFldAtivo: TSmallintField;
    DBGOVpLayFld: TDBGrid;
    BtColuna: TBitBtn;
    PMColuna: TPopupMenu;
    Incluicoluna1: TMenuItem;
    Alteracolunaatual1: TMenuItem;
    Excluicolunaatual1: TMenuItem;
    N1: TMenuItem;
    Criacamponatabela1: TMenuItem;
    QrOVpLayFldFldType: TWideStringField;
    QrOVpLayFldFldNull: TWideStringField;
    QrOVpLayFldFldDefault: TWideStringField;
    QrFlds: TMySQLQuery;
    PB1: TProgressBar;
    QrFldsTabela: TWideStringField;
    QrFldsCampo: TWideStringField;
    QrFldsDataType: TWideStringField;
    QrFldsFldType: TWideStringField;
    QrFldsFldNull: TWideStringField;
    QrFldsFldDefault: TWideStringField;
    QrFldsFldKey: TWideStringField;
    QrFldsConta: TIntegerField;
    QrOVpLayFldFldKey: TWideStringField;
    QrOVpLayFldTamanho: TWideStringField;
    QrFldsTamanho: TWideStringField;
    AdicionadelayoutCSV1: TMenuItem;
    MeAvisos: TMemo;
    Criatabelanobancodedados1: TMenuItem;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrOVpLayEsqAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrOVpLayEsqBeforeOpen(DataSet: TDataSet);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure QrOVpLayEsqAfterScroll(DataSet: TDataSet);
    procedure CabInclui1Click(Sender: TObject);
    procedure CabAltera1Click(Sender: TObject);
    procedure CabExclui1Click(Sender: TObject);
    procedure BtCabClick(Sender: TObject);
    procedure BtItsClick(Sender: TObject);
    procedure ItsInclui1Click(Sender: TObject);
    procedure ItsExclui1Click(Sender: TObject);
    procedure ItsAltera1Click(Sender: TObject);
    procedure PMCabPopup(Sender: TObject);
    procedure PMItsPopup(Sender: TObject);
    procedure QrOVpLayEsqBeforeClose(DataSet: TDataSet);
    procedure QrOVpLayTabBeforeClose(DataSet: TDataSet);
    procedure QrOVpLayTabAfterScroll(DataSet: TDataSet);
    procedure BtColunaClick(Sender: TObject);
    procedure Incluicoluna1Click(Sender: TObject);
    procedure Alteracolunaatual1Click(Sender: TObject);
    procedure Excluicolunaatual1Click(Sender: TObject);
    procedure PMColunaPopup(Sender: TObject);
    procedure Criacamponatabela1Click(Sender: TObject);
    procedure AdicionadelayoutCSV1Click(Sender: TObject);
    procedure Criatabelanobancodedados1Click(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    procedure MostraOVpLayTab(SQLType: TSQLType);
    procedure MostraOVpLayFld(SQLType: TSQLType);
    function  ObtemTipoETamDeType(const FldTyp: String; var Tipo, Tam: String):
              Boolean;

  public
    { Public declarations }
    FSeq, FCabIni: Integer;
    FLocIni: Boolean;
    //
    procedure LocCod(Atual, Codigo: Integer);
    procedure ReopenOVpLayTab(Controle: Integer);
    procedure ReopenOVpLayFld(Conta: Integer);

  end;

var
  FmOVpLayEsq: TFmOVpLayEsq;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, MyDBCheck, DmkDAC_PF, OVpLayTab, OVpLayFld, UMySQLDB,
  UnImporta_PF;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmOVpLayEsq.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmOVpLayEsq.MostraOVpLayFld(SQLType: TSQLType);
var
  Tipo: Integer;
begin
  if DBCheck.CriaFm(TFmOVpLayFld, FmOVpLayFld, afmoNegarComAviso) then
  begin
    FmOVpLayFld.ImgTipo.SQLType := SQLType;
    FmOVpLayFld.FQrCab := QrOVpLayTab;
    FmOVpLayFld.FDsCab := DsOVpLayTab;
    FmOVpLayFld.FQrIts := QrOVpLayFld;
    if SQLType = stIns then
      //
    else
    begin
      FmOVpLayFld.EdConta.ValueVariant := QrOVpLayFldConta.Value;
      //
      FmOVpLayFld.EdNome.ValueVariant       := QrOVpLayFldNome.Value;
      FmOVpLayFld.EdCampo.ValueVariant      := QrOVpLayFldCampo.Value;
      FmOVpLayFld.EdColuna.ValueVariant     := QrOVpLayFldColuna.Value;
      FmOVpLayFld.EdTamanho.ValueVariant    := QrOVpLayFldTamanho.Value;
      case Ord(QrOVpLayFldDataType.Value[1]) of
        (*c,C*)099, 067: Tipo := 1;
        (*d,D*)100, 068: Tipo := 2;
        (*i,I*)105, 073: Tipo := 3;
        (*n,N*)110, 078: Tipo := 4;
        (*???*)else
        begin
          Tipo := 0;
          Geral.MB_Erro('DataType n�o implementado: "' + QrOVpLayFldDataType.Value + '"');
        end;
      end;
      FmOVpLayFld.EdFldType.ValueVariant    := QrOVpLayFldFldType.Value;
      FmOVpLayFld.EdFldNull.ValueVariant    := QrOVpLayFldFldNull.Value;
      FmOVpLayFld.EdFldDefault.ValueVariant := QrOVpLayFldFldDefault.Value;
      FmOVpLayFld.EdFldKey.ValueVariant     := QrOVpLayFldFldKey.Value;
    end;
    FmOVpLayFld.RGDataType.ItemIndex      := Tipo;
    //
    FmOVpLayFld.ShowModal;
    FmOVpLayFld.Destroy;
  end;
end;

procedure TFmOVpLayEsq.MostraOVpLayTab(SQLType: TSQLType);
begin
  if DBCheck.CriaFm(TFmOVpLayTab, FmOVpLayTab, afmoNegarComAviso) then
  begin
    FmOVpLayTab.ImgTipo.SQLType := SQLType;
    FmOVpLayTab.FQrCab := QrOVpLayEsq;
    FmOVpLayTab.FDsCab := DsOVpLayEsq;
    FmOVpLayTab.FQrIts := QrOVpLayTab;
    if SQLType = stIns then
      //
    else
    begin
      FmOVpLayTab.EdControle.ValueVariant := QrOVpLayTabControle.Value;
      //
      FmOVpLayTab.EdNome.ValueVariant     := QrOVpLayTabNome.Value;
      FmOVpLayTab.EdTabela.ValueVariant   := QrOVpLayTabTabela.Value;
    end;
    FmOVpLayTab.ShowModal;
    FmOVpLayTab.Destroy;
  end;
end;

function TFmOVpLayEsq.ObtemTipoETamDeType(const FldTyp: String; var Tipo, Tam:
  String): Boolean;
var
  pi, pf: Integer;
  s: String;
begin
  Result := True;
  pi := pos('(', FldTyp);
  pf := pos(')', FldTyp);
  if pi > 0 then
  begin
    s := Uppercase(Copy(FldTyp, 1, pi - 1));
    Tam  := Copy(FldTyp, pi + 1, pf - 1 - pi);
  end else
  begin
    s := Uppercase(FldTyp);
    Tam  := '';
  end;
  Tipo := s[1];
  if Tipo = 'F' then
    Tipo := 'N';
end;

procedure TFmOVpLayEsq.PMCabPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemCabUpd(CabAltera1, QrOVpLayEsq);
  MyObjects.HabilitaMenuItemCabDel(CabExclui1, QrOVpLayEsq, QrOVpLayTab);
end;

procedure TFmOVpLayEsq.PMColunaPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(Incluicoluna1, QrOVpLayTab);
  MyObjects.HabilitaMenuItemItsUpd(Alteracolunaatual1, QrOVpLayFld);
  MyObjects.HabilitaMenuItemItsDel(Excluicolunaatual1, QrOVpLayFld);
  MyObjects.HabilitaMenuItemItsUpd(Criacamponatabela1, QrOVpLayFld);
end;

procedure TFmOVpLayEsq.PMItsPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(ItsInclui1, QrOVpLayEsq);
  MyObjects.HabilitaMenuItemItsUpd(ItsAltera1, QrOVpLayTab);
  MyObjects.HabilitaMenuItemItsDel(ItsExclui1, QrOVpLayTab, QrOVpLayFld.RecordCount = 0);
  //
  AdicionadelayoutCSV1.Enabled      := VAR_USUARIO = -1;
  Criatabelanobancodedados1.Enabled := VAR_USUARIO = -1;
end;

procedure TFmOVpLayEsq.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrOVpLayEsqCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmOVpLayEsq.DefParams;
begin
  VAR_GOTOTABELA := 'ovplayesq';
  VAR_GOTOMYSQLTABLE := QrOVpLayEsq;
  VAR_GOTONEG := gotoPiZ;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT *');
  VAR_SQLx.Add('FROM ovplayesq');
  VAR_SQLx.Add('WHERE Codigo > 0');
  //
  VAR_SQL1.Add('AND Codigo=:P0');
  //
  //VAR_SQL2.Add('AND CodUsu=:P0');
  //
  VAR_SQLa.Add('AND Nome Like :P0');
  //
end;

procedure TFmOVpLayEsq.Excluicolunaatual1Click(Sender: TObject);
var
  Conta: Integer;
begin
  if UMyMod.ExcluiRegistroInt1('Confirma a exclus�o da coluna selecionada?',
  'OVpLayFld', 'Conta', QrOVpLayFldConta.Value, Dmod.MyDB) = ID_YES then
  begin
    Conta := GOTOy.LocalizaPriorNextIntQr(QrOVpLayFld,
      QrOVpLayFldConta, QrOVpLayFldConta.Value);
    ReopenOVpLayFld(Conta);
  end;
end;

procedure TFmOVpLayEsq.Incluicoluna1Click(Sender: TObject);
begin
  MostraOVpLayFld(stIns);
end;

procedure TFmOVpLayEsq.ItsAltera1Click(Sender: TObject);
begin
  MostraOVpLayTab(stUpd);
end;

procedure TFmOVpLayEsq.CabExclui1Click(Sender: TObject);
begin
  Geral.MB_Info('A��o n�o implementada! Solicite � Dermatek:' + sLineBreak +
  Caption + sLineBreak + TMenuItem(Sender).Name);
end;

procedure TFmOVpLayEsq.Criacamponatabela1Click(Sender: TObject);
  var
    Tabela: String;
    N: Integer;
  //
  procedure AddCampoEmTabela();
  var
    Campo, (*FldType,*) sNulo, sDefault: String;
  begin
    Campo := QrOVpLayFldCampo.Value;
    //
    UnDmkDAC_PF.AbreMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
    'SHOW COLUMNS FROM ' + Tabela + ' LIKE "' + Campo + '"',
    EmptyStr]);
    //
    if Dmod.QrUpd.RecordCount = 0 then
    begin
      {
      case Ord(QrOVpLayFldDataType.Value[1]) of
        (*c,C*)099, 067: FldType := 'varchar(' + QrOVpLayFldTamanho.Value + ')';
        (*d,D*)100, 068: FldType := 'datetime NOT NUL DEFAULT "0000-00-00 00:00:00"';
        (*i,I*)105, 073: FldType := 'int(11) NOT NULL DEFAULT 0';
        (*n,N*)110, 078: FldType := 'double(15,3) NOT NULL DEFAULT 0.000';
        (*???*)else
        begin
          FldType := '?????';
          Geral.MB_Erro('DataType n�o implementado: "' + QrOVpLayFldDataType.Value + '"');
        end;
      end;
      }
      if Uppercase(QrOVpLayFldFldNull.Value) = 'NO' then
        sNulo := ' NOT NULL '
      else
        sNulo := '';
      if QrOVpLayFldFldDefault.Value <> EmptyStr then
        sDefault := ' DEFAULT "' + QrOVpLayFldFldDefault.Value + '"'
      else
        sDefault := '';
      Dmod.MyDB.Execute('ALTER TABLE ' + Tabela + ' ADD ' + Campo + ' ' +
      QrOVpLayFldFldType.Value + sNulo + sDefault);
      //
      N := N + 1;
    end;
  end;
begin
  Screen.Cursor := crHourGlass;
  try
    N := 0;
    Tabela := QrOVpLayTabTabela.Value;
    AddCampoEmTabela();
    if Lowercase(Tabela) = Lowercase('ovfordemproducao') then
    begin
      Tabela := 'ovmordemproducao';
      AddCampoEmTabela();
    end;
  finally
    Screen.Cursor := crDefault;
    if N > 0 then
      Geral.MB_Info('Campo adicionado!');
  end;
end;

procedure TFmOVpLayEsq.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmOVpLayEsq.Criatabelanobancodedados1Click(Sender: TObject);
var
  Tabela, NomeCampo, SQL, FldType, FldDefault, NotOrNull, PriKey: String;
  Continua: Boolean;
begin
  Tabela := QrOVpLayTabTabela.Value;
  PriKey := '';
  if Tabela <> EmptyStr then
  begin
    Continua := False;
    if USQLDB.TabelaExiste(Tabela, Dmod.MyDB) then
    begin
      if Geral.MB_Pergunta('A tabela ' + Tabela +
      ' j� existe! Deseja exclu�-la para poder recri�-la?') = ID_YES then
      begin
        if Geral.MB_Pergunta('Deseja realmente excluir a tabela ' + Tabela +
        '? Todos dados atuais ser�o perdidos!') = ID_YES then
        begin
          Continua := True;
          Dmod.MyDB.Execute('DROP TABLE ' + Lowercase(Tabela));
        end;
      end;
    end else
      Continua := True;
    if not Continua then
      Exit;
    //
    SQL := 'CREATE TABLE IF NOT EXISTS ' + Lowercase(Tabela) + ' ( ';
    QrOVpLayFld.First;
    while not QrOVpLayFld.Eof do
    begin
      NomeCampo := QrOVpLayFldCampo.Value;
      FldType   := QrOVpLayFldFldType.Value;
      NotOrNull := QrOVpLayFldFldNull.Value;
      if (Uppercase(NotOrNull) = 'YES')
      or (Uppercase(NotOrNull) = 'SIM') then
        NotOrNull := ''
      else
        NotOrNull := ' NOT NULL';
      FldDefault := Trim(QrOVpLayFldFldDefault.Value);
      if FldDefault <> EmptyStr then
        FldDefault := ' DEFAULT "' + FldDefault + '"';
      //
      SQL := SQL + sLineBreak + NomeCampo + ' ' + FldType + NotOrNull + FldDefault;
//      if QrOVpLayFld.RecNo < QrOVpLayFld.RecordCount then
        SQL := SQL + ',';
      //
      if (Uppercase(QrOVpLayFldFldKey.Value) = 'SIM')
      or (Uppercase(QrOVpLayFldFldKey.Value) = 'YES') then
      begin
        if PriKey <> EMptyStr then
          PriKey := PriKey + ', ';
        PriKey := PriKey + QrOVpLayFldCampo.Value;
      end;
      //
      QrOVpLayFld.Next;
    end;
    SQL := SQL + sLineBreak + 'ReInsrt int(11) NOT NULL DEFAULT "0",';
    SQL := SQL + sLineBreak + 'LastInsrt datetime NOT NULL DEFAULT "0000-00-00 00:00:00"';
    if PriKey <> EmptyStr then
      SQL := SQL + ',' + sLineBreak + 'PRIMARY KEY (' + PriKey + ') ';
    SQL := SQL + sLineBreak + ') CHARACTER SET latin1 COLLATE latin1_swedish_ci';
    //
    Dmod.MyDB.Execute(SQL);
    //
    Geral.MB_Info('SQL de cria��o executada!');
  end;
end;

procedure TFmOVpLayEsq.QueryPrincipalAfterOpen;
begin
end;

procedure TFmOVpLayEsq.ItsExclui1Click(Sender: TObject);
var
  Controle: Integer;
begin
  if UMyMod.ExcluiRegistroInt1('Confirma a exclus�o da tabela selecionada?',
  'OVpLayTab', 'Controle', QrOVpLayTabControle.Value, Dmod.MyDB) = ID_YES then
  begin
    Controle := GOTOy.LocalizaPriorNextIntQr(QrOVpLayTab,
      QrOVpLayTabControle, QrOVpLayTabControle.Value);
    ReopenOVpLayTab(Controle);
  end;
end;

procedure TFmOVpLayEsq.ReopenOVpLayFld(Conta: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrOVpLayFld, Dmod.MyDB, [
  'SELECT * ',
  'FROM ovplayfld ',
  'WHERE Controle=' + Geral.FF0(QrOVpLayTabControle.Value),
  'ORDER BY Coluna, Conta ',
  '']);
  //
  QrOVpLayFld.Locate('Conta', Conta, []);
end;

procedure TFmOVpLayEsq.ReopenOVpLayTab(Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrOVpLayTab, Dmod.MyDB, [
  'SELECT * ',
  'FROM ovplaytab ',
  'WHERE Codigo=' + Geral.FF0(QrOVpLayEsqCodigo.Value),
  '']);
  //
  QrOVpLayTab.Locate('Controle', Controle, []);
end;


procedure TFmOVpLayEsq.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmOVpLayEsq.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmOVpLayEsq.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmOVpLayEsq.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmOVpLayEsq.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmOVpLayEsq.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmOVpLayEsq.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrOVpLayEsqCodigo.Value;
  Close;
end;

procedure TFmOVpLayEsq.ItsInclui1Click(Sender: TObject);
begin
  MostraOVpLayTab(stIns);
end;

procedure TFmOVpLayEsq.CabAltera1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrOVpLayEsq, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'ovplayesq');
end;

procedure TFmOVpLayEsq.BtColunaClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMColuna, BtColuna);
end;

procedure TFmOVpLayEsq.BtConfirmaClick(Sender: TObject);
var
  Nome: String;
  Codigo: Integer;
  SQLType: TSQLType;
begin
  SQLType := ImgTipo.SQLType;
  Codigo  := EdCodigo.ValueVariant;
  Nome    := EdNome.ValueVariant;
  if MyObjects.FIC(Length(Nome) = 0, EdNome, 'Defina uma descri��o!') then Exit;

  //
  Codigo := UMyMod.BPGS1I32('ovplayesq', 'Codigo', '', '', tsPos, SQLType, Codigo);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'ovplayesq', False, [
  'Nome'], [
  'Codigo'], [
  Nome], [
  Codigo], True) then
  begin
    ImgTipo.SQLType := stLok;
    PnDados.Visible := True;
    PnEdita.Visible := False;
    GOTOy.BotoesSb(ImgTipo.SQLType);
    //
    LocCod(Codigo, Codigo);
    if FSeq = 1 then Close;
  end;
end;

procedure TFmOVpLayEsq.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := EdCodigo.ValueVariant;
  if ImgTipo.SQLType = stIns then
    UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'ovplayesq', Codigo);
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'ovplayesq', 'Codigo');
end;

procedure TFmOVpLayEsq.BtItsClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMIts, BtIts);
end;

procedure TFmOVpLayEsq.AdicionadelayoutCSV1Click(Sender: TObject);
var
  Arquivo, Tabela: String;
  CodLayout: Integer;
  LstArq, LstLin1, LstLin2, LstLin3, LstLin4, LstLin5, LstLin6, LstLin7: TStringList;
begin
  CodLayout := QrOVpLayEsqCodigo.Value;
  if MyObjects.FIC(CodLayout = 0, nil,
  'Selecione ou crie um esquema v�lido antes de criar a tabela!') then Exit;
  //
  MyObjects.FileOpenDialog(Self, 'C:\pBI\Layout', '',
      'Informe o aqruivo de Layout CSV', '', [], Arquivo);
  if Trim(Arquivo) <> '' then
  begin
(*
    Tabela := ExtractFileName(Arquivo);
    Tabela := copy(Tabela, 1, pos('.', Tabela) - 1);
    //if Trim(Tabela) = '' then
    InputQuery('Nome da Nova Tabela',
      'Informe o nome da nova tabela:', Tabela);
    if Trim(Tabela) <> EmptyStr then
    begin
      if DmkPF.NomeSimplesValido(Tabela) = False then
        Exit;
      //GBAvisos1.Height := 130; // Height Inicial = 56
      //
*)
      Importa_PF.CarregaArquivoParaLayout(CodLayout, Arquivo,
      Tabela, LaAviso1, LaAviso2, MeAvisos,
      LstArq, LstLin1, LstLin2, LstLin3, LstLin4, LstLin5, LstLin6, LstLin7);
//    end;
  end;
end;

procedure TFmOVpLayEsq.Alteracolunaatual1Click(Sender: TObject);
begin
  MostraOVpLayFld(stUpd);
end;

procedure TFmOVpLayEsq.BtCabClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMCab, BtCab);
end;

procedure TFmOVpLayEsq.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  GBEdita.Align := alClient;
  DBGOVpLayFld.Align := alClient;
  CriaOForm;
  FSeq := 0;
end;

procedure TFmOVpLayEsq.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrOVpLayEsqCodigo.Value, LaRegistro.Caption);
end;

procedure TFmOVpLayEsq.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmOVpLayEsq.SbNovoClick(Sender: TObject);
  function Dif(s1, s2: String): Boolean;
  begin
    Result := Lowercase(s1) <> Lowercase(s2);
  end;
var
  Tabela, Campo, sField, sTipo, sNull, sKey, sDefault: String;
  //
var
  DataType, FldType, FldNull, FldDefault, FldKey, Tamanho, Msg: String;
  Codigo, Controle, Conta, Coluna, Itens: Integer;
  SQLType: TSQLType;
begin
  SQLType := stUpd;
  //LaRegistro.Caption := GOTOy.CodUsu(QrOVpLayEsqCodigo.Value, LaRegistro.Caption);
  if Geral.MB_Pergunta(
  'Deseja corrigir os itens deste layout pelos campos das tabelas pesistentes na base da dados?') =
  ID_YES then
  begin
    Screen.Cursor := crHourGlass;
    try
      Itens := 0;
      UnDmkDAC_PF.AbreMySQLQuery0(QrFlds, Dmod.MyDB, [
      'SELECT tab.Tabela, fld.Conta, fld.Coluna, ',
      'fld.Campo, fld.DataType, fld.Tamanho, ',
      'fld.FldType, FldNull, FldDefault, FldKey ',
      'FROM ovplayfld fld ',
      'LEFT JOIN ovplaytab tab ON tab.Controle=fld.Controle ',
      'WHERE fld.Codigo=' + Geral.FF0(QrOVpLayEsqCodigo.Value),
      'ORDER BY Tabela, Coluna',
      EmptyStr]);
      //
      PB1.Position := 0;
      PB1.Max := QrFlds.RecordCount;
      QrFlds.First;
      while not QrFlds.Eof do
      begin
        MyObjects.Informa2EUpdPB(PB1, LaAviso1, LaAviso2, True, 'Verificando item ' +
        Geral.FF0(QrFlds.RecNo) + ' de ' + Geral.FF0(QrFlds.RecordCount));
        //
        Tabela := Lowercase(QrFldsTabela.Value);
        Campo  := QrFldsCampo.Value;
        UnDmkDAC_PF.AbreMySQLQuery0(Dmod.QrAux, Dmod.MyDB, [
        'SHOW COLUMNS FROM ' + Tabela + ' LIKE "' + Campo + '" ',
        EmptyStr]);
        sField   := Dmod.QrAux.FieldByName('Field').AsString;
        sTipo    := Dmod.QrAux.FieldByName('Type').AsString;
        sNull    := Dmod.QrAux.FieldByName('Null').AsString;
        sKey     := Dmod.QrAux.FieldByName('Key').AsString;
        sDefault := Dmod.QrAux.FieldByName('Default').AsString;
        ObtemTipoETamDeType(sTipo, DataType, Tamanho);
        //
        if Dif(sField, QrFldsCampo.Value)
        or Dif(sTipo, QrFldsFldType.Value)
        or Dif(sNull, QrFldsFldNull.Value)
        or Dif(sKey, QrFldsFldKey.Value)
        or Dif(sDefault, QrFldsFldDefault.Value)
        or Dif(DataType, QrFldsDataType.Value)
        or (Tamanho <> QrFldsTamanho.Value) then
        begin
          Conta          := QrFldsConta.Value;
          //DataType       := ;
          FldType        := sTipo;
          FldNull        := sNull;
          FldDefault     := sDefault;
          FldKey         := sKey;
          //
          if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'ovplayfld', False, [
          'DataType',
          'Tamanho', 'FldType', 'FldNull',
          'FldDefault', 'FldKey'], [
          'Conta'], [
          DataType,
          Tamanho, FldType, FldNull,
          FldDefault, FldKey], [
          Conta], True) then
            Itens := Itens + 1;
        end;
        //
        QrFlds.Next;
      end;
    finally
      Screen.Cursor := crDefault;
    end;
    if Itens = 0 then
      Msg := 'Nenhum item foi alterado!'
    else
      Msg := Geral.FF0(Itens) + ' itens foram alterados!';
    //
    MyObjects.Informa2(LaAviso1, LaAviso2, False, Msg);
  end;
end;

procedure TFmOVpLayEsq.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmOVpLayEsq.QrOVpLayEsqAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmOVpLayEsq.QrOVpLayEsqAfterScroll(DataSet: TDataSet);
begin
  ReopenOVpLayTab(0);
end;

procedure TFmOVpLayEsq.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //
  if FSeq = 1 then
    CabInclui1Click(Self)
  else
  if (not FLocIni) and (FCabIni <> 0)  then
  begin
    LocCod(FCabIni, FCabIni);
    if QrOVpLayEsqCodigo.Value <> FCabIni then
      Geral.MB_Aviso('Grupo n�o localizado!');
    FLocIni := True;
  end;
end;

procedure TFmOVpLayEsq.SbQueryClick(Sender: TObject);
begin
  LocCod(QrOVpLayEsqCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'ovplayesq', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmOVpLayEsq.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmOVpLayEsq.CabInclui1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrOVpLayEsq, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'ovplayesq');
end;

procedure TFmOVpLayEsq.QrOVpLayEsqBeforeClose(
  DataSet: TDataSet);
begin
  QrOVpLayTab.Close;
end;

procedure TFmOVpLayEsq.QrOVpLayEsqBeforeOpen(DataSet: TDataSet);
begin
  QrOVpLayEsqCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmOVpLayEsq.QrOVpLayTabAfterScroll(DataSet: TDataSet);
begin
  ReopenOVpLayFld(0);
end;

procedure TFmOVpLayEsq.QrOVpLayTabBeforeClose(DataSet: TDataSet);
begin
  QrOVpLayFld.Close;
end;

end.

