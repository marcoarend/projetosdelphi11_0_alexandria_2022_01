unit UnOVS_LoadCSV;

interface

uses
  Winapi.Windows, System.SysUtils,
  dmkGeral, UnDMkEnums;

type
  TAcaoCvsErpLaituraArq = (acelaNenhum=0, acelaLayout=1, acelaDados=2);
  TUnOVS_LoadCSV = class(TObject)
  private
    {private declaration}
    //procedure AtualizaLog(Status: Integer; Nome: String; Inclusoes: Integer);
    //procedure AdicionaExtraidosDeCarregados();
    //function  DefineNovosItensRelavantes(): Integer;
    //procedure MostraTempoTranscorrido();
    //procedure Mensagem(Msg: String);
  public
    {public declaration}
    //FScanAborted: Boolean;
    //
    //procedure CarregaDados(CodLayout: Integer; Dir: String);
    //procedure LeDiretorio(Dir: String; Acao: TAcaoCvsErpLaituraArq);
  end;

var
  OVS_LoadCSV: TUnOVS_LoadCSV;


implementation

uses UnOVS_ProjGroupVars, UnMyObjects, UMySQLModule, DmkDAC_PF, UnOVS_Consts,
  UnProjGroup_PF,
  Module;

{ TUnOVS_LoadCSV }

const
  _STATUS_000 =    0; // In�cio da importa��o
  _STATUS_100 =  100; // Importa��o de Layout
  _STATUS_200 =  200; // Importa��o de movimento
  _STATUS_300 =  300; // Extraindo dados secund�rios
  _STATUS_400 =  400; // Definindo itens relevantes
  _STATUS_500 =  500; // Importa��o finalizada

var
    FIniTick, FFimTick, FDifTick: DWORD;
    FCodLayout, FCodLog: Integer;
    FDataHora: String;

{
procedure TUnOVS_LoadCSV.AdicionaExtraidosDeCarregados();
var
  AllSQL, Codigo, CodUsu, Nome: String;
  Item, Count: Integer;
begin
  AtualizaLog(_STATUS_300, 'extraindo para ovdgradecad', 0);
  MyObjects.Informa2(VAR_LOAD_CSV_LaAviso1, VAR_LOAD_CSV_LaAviso2, True, 'Carregando dados de cabe�alhos de grade');
  UnDMkDAC_PF.AbreMySQLDirectQuery0(Dmod.DqAux, Dmod.MyDB, [
  'SELECT CodGrade',
  'FROM ovdproduto ',
  'GROUP BY CodGrade',
  'ORDER BY CodGrade',
  '']);
  Count := Dmod.DqAux.RecordCount;
  if Count > 0 then
  begin
    AllSQL := 'INSERT INTO ovdgradecad (Codigo,Nome,Extraido) VALUES ';
    Item := 0;
    while not Dmod.DqAux.Eof do
    begin
      Codigo := Dmod.DqAux.FieldValueByFieldName('CodGrade');
      //
      Item := Item + 1;
      if Item = Count then
        AllSQL := AllSQL + '(' + Codigo + ', "Grade ' + Codigo + '", 1)' + sLineBreak
      else
        AllSQL := AllSQL + '(' + Codigo + ', "Grade ' + Codigo + '", 1),' + sLineBreak;
      //
      Dmod.DqAux.Next;
    end;
    AllSQL := AllSQL + ' ON DUPLICATE KEY UPDATE ReInsrt=ReInsrt+1, LastInsrt="' + FDataHora + '";';
    //Geral.MB_Info(AllSQL);
    DMod.MyDB.Execute(AllSQL);
  end;
  //
  //
  //
  AtualizaLog(_STATUS_300, 'extraindo para ovdgradetam', 0);
  MyObjects.Informa2(VAR_LOAD_CSV_LaAviso1, VAR_LOAD_CSV_LaAviso2, True, 'Carregando dados de tamanhos de grade');
   UnDMkDAC_PF.AbreMySQLDirectQuery0(Dmod.DqAux, Dmod.MyDB, [
  'SELECT CodGrade, CodTam ',
  'FROM ovdproduto ',
  'GROUP BY CodGrade, CodTam ',
  'ORDER BY CodGrade, CodTam ',
  '']);
  Count := Dmod.DqAux.RecordCount;
  if Count > 0 then
  begin
    AllSQL := 'INSERT ovdgradetam (Codigo,Nome,Extraido) VALUES ';
    Item := 0;
    while not Dmod.DqAux.Eof do
    begin
      Codigo := Dmod.DqAux.FieldValueByFieldName('CodGrade');
      Nome   := Geral.VariavelToString(Dmod.DqAux.FieldValueByFieldName('CodTam'));
      //
      Item := Item + 1;
      if Item = Count then
        AllSQL := AllSQL + '(' + Codigo + ', ' + Nome +  ', 1)' + sLineBreak
      else
        AllSQL := AllSQL + '(' + Codigo + ', ' + Nome +  ', 1),' + sLineBreak;
      //
      Dmod.DqAux.Next;
    end;
    AllSQL := AllSQL + ' ON DUPLICATE KEY UPDATE ReInsrt=ReInsrt+1, LastInsrt="' + FDataHora + '";';
    //Geral.MB_Info(AllSQL);
    DMod.MyDB.Execute(AllSQL);
  end;
  //
  //
  //
  AtualizaLog(_STATUS_300, 'extraindo para entidades', 0);
  MyObjects.Informa2(VAR_LOAD_CSV_LaAviso1, VAR_LOAD_CSV_LaAviso2, True, 'Carregando dados de entidades de fornecedores (de locais)');
   UnDMkDAC_PF.AbreMySQLDirectQuery0(Dmod.DqAux, Dmod.MyDB, [
  'SELECT DISTINCT loc.Fornecedor, loc.Nome ',
  'FROM ovdlocal loc ',
  'LEFT JOIN entidades ent ON ent.Codigo=loc.Fornecedor ',
  'WHERE loc.Fornecedor<>0 ',
  'AND ent.Codigo IS NULL ',
  'ORDER BY Fornecedor ',
  '']);
  Count := Dmod.DqAux.RecordCount;
  if Count > 0 then
  begin
    AllSQL := 'INSERT INTO entidades (Codigo,CodUsu,RazaoSocial,Tipo) VALUES ';
    Item := 0;
    while not Dmod.DqAux.Eof do
    begin
      Codigo := Dmod.DqAux.FieldValueByFieldName('Fornecedor');
      CodUsu := Codigo;
      Nome   := Geral.VariavelToString(Dmod.DqAux.FieldValueByFieldName('Nome'));
      //
      Item := Item + 1;
      if Item = Count then
        AllSQL := AllSQL + '(' + Codigo + ', ' + CodUsu + ', ' + Nome +  ', 0)' + sLineBreak
      else
        AllSQL := AllSQL + '(' + Codigo + ', ' + CodUsu + ', ' + Nome +  ', 0),' + sLineBreak;
      //
      Dmod.DqAux.Next;
    end;
    AllSQL := AllSQL + ' ON DUPLICATE KEY UPDATE Ativo=1;';
    //Geral.MB_Info(AllSQL);
    DMod.MyDB.Execute(AllSQL);
  end;
  //
  //
  //
  MyObjects.Informa2(VAR_LOAD_CSV_LaAviso1, VAR_LOAD_CSV_LaAviso2, True, '...');
end;
}

{
procedure TUnOVS_LoadCSV.AtualizaLog(Status: Integer; Nome: String;
  Inclusoes: Integer);
var
  DataHora: String;
  Codigo: Integer;
  SQLType: TSQLType;
begin
  if FCodLog = 0 then
  begin
    SQLType := stIns;
    Codigo := UMyMod.BPGS1I32('ovpimplog', 'Codigo', '', '', tsPos, stIns, 0);
    FCodLog := Codigo;
  end else
  begin
    Codigo  := FCodLog;
    SQLType := stUpd;
  end;
  //Codigo         := ;
  //Nome           := ;
  //Status         := ;
  DataHora       := FDataHora;
  //
  //if
  UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'ovpimplog', False, [
  'Nome', 'Status', 'DataHora',
  'Inclusoes'], [
  'Codigo'], [
  Nome, Status, DataHora,
  Inclusoes], [
  Codigo], True);
end;
}

{
procedure TUnOVS_LoadCSV.CarregaDados(CodLayout: Integer; Dir: String);
var
  //Dir: String;
  Inclusoes: Integer;
begin
  VAR_LOAD_CSV_HORA_INI := Geral.FDT(Dmod.DModG_ObtemAgora(), 109);
  //FCodLayout := EdOVpLayEsq.ValueVariant;
(*
  if MyObjects.FIC(CodLayout = 0, nil, 'Informe o Esquema de Layout!') then
    Exit;
*)
  if CodLayout = 0 then
  begin
    if VAR_RUN_AS_SVC then
      ProjGroup_PF.SalvaEmArquivoLog(TSvcMsgKind.smkGetErr,
        'Esquema de Layout n�o definido!')
    else
      Geral.MB_Aviso('Informe o Esquema de Layout!');
    Exit;
  end;
  //
  //Dir := EdLoadCSVOthDir.Text;
  //
  AtualizaLog(_STATUS_000, '', 0);
  //LeDiretorio(Dir, TAcaoCvsErpLaituraArq.acelaDados);
  //
  AdicionaExtraidosDeCarregados();
  AtualizaLog(_STATUS_400, 'OK', 0);
  //
  Inclusoes := DefineNovosItensRelavantes();
  AtualizaLog(_STATUS_500, 'OK', Inclusoes);
  //
  MostraTempoTranscorrido();
end;
}

{
function TUnOVS_LoadCSV.DefineNovosItensRelavantes: Integer;
var
  SQL: String;
begin
  Result := 0;
  UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
  'DROP TABLE IF EXISTS _Alheio_Relevnt_1024_;',
  '',
  'CREATE TABLE _Alheio_Relevnt_1024_',
  'SELECT fop.Local, fop.NrOP, fop.SeqGrupo, fop.NrReduzidoOP, ',
  'fop.Lk, fop.DataCad, fop.DataAlt, fop.UserCad, fop.UserAlt, ',
  'fop.AlterWeb, fop.AWServerID, fop.AWStatSinc, fop.Ativo ',
  'FROM ovfordemproducao fop ',
  'LEFT JOIN ovdlocal dlo ON dlo.Codigo=fop.Local ',
  'LEFT JOIN ovdproduto prd ON prd.Controle=fop.Produto ',
  'LEFT JOIN ovdreferencia ref ON ref.Codigo=prd.Codigo ',
  ProjGroup_PF.FiltroDadosReleventesAlheios(),
  'GROUP BY Local, NrOP, SeqGrupo, NrReduzidoOP ',
  '']);
  //Geral.MB_SQL(Self, Dmod.QrUpd);
  //
  SQL := UMyMod.ObtemCamposDeTabelaIdentica(Dmod.MyDB, 'ovgisplassta', '_ar.');
  SQL := Geral.Substitui(SQL, ', _ar.IspZtatus', ', ' +
    Geral.FF0(CO_OVS_IMPORT_ALHEIO_1024_IMPORTADO) + ' IspZtatus');
  SQL := Geral.Substitui(SQL, ', _ar.DataHora', ', "' + FDataHora + '" DataHora');
  SQL := Geral.Substitui(SQL, ', _ar.Motivo', ', 0 Motivo');
  //Geral.MB_Info(SQL);
  // Atualiza tabela do �ltimo status
  UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
  'INSERT INTO ovgisplassta ',
  'SELECT ',
  SQL,
(*
  '_ar.Local, _ar.NrOP, _ar.SeqGrupo, _ar.NrReduzidoOP,',
  Geral.FF0(CO_OVS_IMPORT_ALHEIO_1024_IMPORTADO) +
  ' IspZtatus, "' + FDataHora + '" DataHora, 0 Lk,',
  ' "' + Copy(FDataHora, 1, 10) + '" DataCad, "0000-00-00" DataAlt, ' +
  Geral.FF0(CO_USER_SYSTEM) + ' UserCad, 0 UserAlt, ',
  '1 AlterWeb, 0 AWServerID, 0 AWStatSinc, 1 Ativo ',
*)
  'FROM _Alheio_Relevnt_1024_ _ar',
  'LEFT JOIN ovgisplassta ilp ON',
  '   ilp.Local   = _ar.Local',
  '  AND ',
  '   ilp.NrOP    = _ar.NrOP',
  '  AND   ',
  '  ilp.SeqGrupo = _ar.SeqGrupo',
  '  AND',
  '   ilp.NrReduzidoOP  = _ar.NrReduzidoOP',
  'WHERE ilp.DataHora IS NULL',
  '']);
  // deve ser aqui!
  Result := Dmod.QrUpd.RowsAffected;
  //
  // Atualiza Log de todos status
  UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
  'INSERT INTO ovgispallsta',
  'SELECT * ',
  'FROM ovgisplassta',
  'WHERE  DataHora = "' + FDataHora + '"',
  'AND IspZtatus=' + Geral.FF0(CO_OVS_IMPORT_ALHEIO_1024_IMPORTADO),
  '']);
end;
}


{
procedure TUnOVS_LoadCSV.LeDiretorio(Dir: String; Acao: TAcaoCvsErpLaituraArq);
const
  SubDir   = False;
  Extensao = '*.csv';
  LimpaAnterior = True;
var
  Arq, Tabela, Tempo: String;
  I, Itens, ic, n, CodLayout: Integer;
  DataGerou, DataAlter, DataAcess: TDateTime;
begin
  FIniTick := GetTickCount;
  //Screen.Cursor := crHourGlass;
  //try
    FScanAborted  := False;
    //MeAvisos.Text := '';
    //
    if not DirectoryExists(Dir) then
    begin
      App_PF.InformaLoadCSV('N�o foi poss�vel localizar o diret�rio:' + sLineBreak +
      Dir);
      Exit;
    end;
    Itens := 0;
    ListBox1.Clear;
    MyObjects.LimpaGrade(GradeA, 1, 1, True);
    //for I := 0 to MeExtensoes.Lines.Count -1 do
    begin
      dmkPF.GetAllFiles(SubDir, Dir + '\' + Extensao, ListBox1, LimpaAnterior);
      Itens := ListBox1.Items.Count;
      if Itens = 0 then
        Informa('N�o foi localizado nenhum arquivo v�lido no diret�rio:' +
        sLineBreak + Dir);
    end;

    ic := 0;
    for I := 0 to ListBox1.Items.Count -1 do
    begin
      if FScanAborted then
      begin
         Screen.Cursor := crDefault;
         Exit;
      end;
      Arq := ListBox1.Items[I];
      if ObtemDatasArquivo(Arq, DataGerou, DataAlter, DataAcess) then
      begin
        ic := ic + 1;
        GradeA.RowCount := ic + 1;
        GradeA.Cells[0, ic] := IntToStr(ic);
        GradeA.Cells[1, ic] := ExtractFileName(Arq);
        GradeA.Cells[2, ic] := Geral.FDT(DataGerou, 109);
        GradeA.Cells[3, ic] := Geral.FDT(DataAlter, 109);
        GradeA.Cells[4, ic] := Geral.FDT(DataAcess, 109);
      end;
    end;

    ic := 0;
    CodLayout := 0;  // n�o tirar daqui! usa valor no loop apos setar <> de 0!!
    for I := 0 to ListBox1.Items.Count -1 do
    begin
      if FScanAborted then
      begin
         Screen.Cursor := crDefault;
         Exit;
      end;
      Arq := ListBox1.Items[I];
      case Acao of
        TAcaoCvsErpLaituraArq.acelaLayout:
        begin
          Tabela    := '';
          CarregaArquivoParaLayout(CodLayout, Arq, Tabela);
        end;
        TAcaoCvsErpLaituraArq.acelaDados:
        begin
          Tabela    := '';
          CarregaArquivoParaMovimento(Arq);
        end;
      end;
    end;
  //finally
    //Screen.Cursor := crDefault;
 // end;
end;
}

{
procedure TUnOVS_LoadCSV.Mensagem(Msg: String);
begin
//
end;
}

{
procedure TUnOVS_LoadCSV.MostraTempoTranscorrido();
var
  Tempo: String;
begin
(*
  FFimTick := GetTickCount;
  FDifTick := FFimTick - FIniTick;
  Tempo := FormatDateTime('hh:nn:ss:zzz', FDifTick / 86400000);
  EdTempoCarrega.Text := Tempo;
  MyObjects.Informa2(LaAviso1, LaAviso2, False, Tempo);
*)
end;
}

end.
