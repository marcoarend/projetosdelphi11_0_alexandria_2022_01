unit OVgIspGerCad;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkPermissoes,
  dmkDBLookupComboBox, dmkEditCB, mySQLDbTables, dmkRadioGroup;

type
  TFmOVgIspGerCad = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    GroupBox2: TGroupBox;
    PnDadosOri: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    EdLocal: TdmkEdit;
    EdNO_Local: TdmkEdit;
    EdNrOP: TdmkEdit;
    EdSeqGrupo: TdmkEdit;
    EdNO_SeqGrupo: TdmkEdit;
    EdNrReduzidoOP: TdmkEdit;
    GroupBox3: TGroupBox;
    Panel5: TPanel;
    QrOVcYnsMedCad: TMySQLQuery;
    QrOVcYnsMedCadCodigo: TIntegerField;
    QrOVcYnsMedCadNome: TWideStringField;
    DsOVcYnsMedCad: TDataSource;
    QrOVcYnsChkCad: TMySQLQuery;
    DsOVcYnsChkCad: TDataSource;
    Label7: TLabel;
    EdOVcYnsMed: TdmkEditCB;
    CBOVcYnsMed: TdmkDBLookupComboBox;
    Label8: TLabel;
    EdOVcYnsChk: TdmkEditCB;
    CBOVcYnsChk: TdmkDBLookupComboBox;
    SbOVcYnsChk: TSpeedButton;
    SbOVcYnsMed: TSpeedButton;
    QrOVcYnsChkCadCodigo: TIntegerField;
    QrOVcYnsChkCadNome: TWideStringField;
    GroupBox4: TGroupBox;
    Panel6: TPanel;
    EdLimiteChk: TdmkEdit;
    Label9: TLabel;
    GroupBox5: TGroupBox;
    Panel7: TPanel;
    Label10: TLabel;
    EdLimiteMed: TdmkEdit;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    EdCodigo: TdmkEdit;
    Label14: TLabel;
    EdOVcYnsARQ: TdmkEditCB;
    CBOVcYnsARQ: TdmkDBLookupComboBox;
    SBOVcYnsARQ: TSpeedButton;
    QrOVcYnsARQCad: TMySQLQuery;
    DsOVcYnsARQCad: TDataSource;
    QrOVcYnsARQCadCodigo: TIntegerField;
    QrOVcYnsARQCadNome: TWideStringField;
    RGPermiFinHow: TdmkRadioGroup;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure SbOVcYnsMedClick(Sender: TObject);
    procedure SbOVcYnsChkClick(Sender: TObject);
    procedure SBOVcYnsARQClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    FCodigo, FSeqGrupo, FZtatusIsp: Integer;
    //
    procedure ReopenOVcYnsMedCad(ArtigRef: Integer);
  end;

  var
  FmOVgIspGerCad: TFmOVgIspGerCad;

implementation

uses UnMyObjects, UnOVS_Jan, Module, DmkDAC_PF, UMySQLModule, ModuleGeral,
  UnOVS_Consts, UnOVS_PF;

{$R *.DFM}

procedure TFmOVgIspGerCad.BtOKClick(Sender: TObject);
var
  //DtHrFecha,
  DtHrAbert, ZtatusDtH: String;
  Codigo, Local, NrOP, SeqGrupo, NrReduzidoOP, OVcYnsMed, OVcYnsChk, LimiteChk,
  LimiteMed, ZtatusIsp, ZtatusMot, OVcYnsARQ, PermiFinHow,
  SegmntInsp, SeccaoInsp: Integer;
  SQLType: TSQLType;
  Agora: TDateTime;
begin
  SQLType        := ImgTipo.SQLType;
  Agora          := DModG.ObtemAgora();
  Codigo         := EdCodigo.ValueVariant;
  Local          := EdLocal.ValueVariant;
  NrOP           := EdNrOP.ValueVariant;
  SeqGrupo       := EdSeqGrupo.ValueVariant;
  NrReduzidoOP   := EdNrReduzidoOP.ValueVariant;
  DtHrAbert      := Geral.FDT(Agora, 109);
  //DtHrFecha      := ;
  OVcYnsMed      := EdOVcYnsMed.ValueVariant;
  OVcYnsChk      := EdOVcYnsChk.ValueVariant;
  LimiteChk      := EdLimiteChk.ValueVariant;
  LimiteMed      := EdLimiteMed.ValueVariant;
  ZtatusIsp      := FZtatusIsp; //CO_OVS_IMPORT_ALHEIO_5...;
  ZtatusDtH      := DtHrAbert;
  OVcYnsARQ      := EdOVcYnsARQ.ValueVariant;
  PermiFinHow    := RGPermiFinHow.ItemIndex;
  ZtatusMot      := 0;
  //
  if MyObjects.FIC(OVcYnsMed = 0, EdOVcYnsMed,
    '"Informe a tabela de medidas"') then Exit;
  //
  if MyObjects.FIC(OVcYnsChk = 0, EdOVcYnsChk,
    '"Informe o check list de inconformidades"') then Exit;
  //
  if MyObjects.FIC(OVcYnsARQ = 0, EdOVcYnsARQ,
    '"Plano de Amostragem e Regime de Qualidade"') then Exit;
  //
  if MyObjects.FIC(PermiFinHow < 1, RGPermiFinHow,
    'Informe a permiss�o de finaliza��o da inspe��o"') then Exit;
  //
  OVS_PF.ObtemSegmentoESecaoDeLocal(Local, SegmntInsp, SeccaoInsp);
  FCodigo := 0;
  FCodigo := OVS_PF.InsereOVgIspGerCab(Codigo, Local, NrOP, SeqGrupo,
  NrReduzidoOP, OVcYnsMed, OVcYnsChk, LimiteChk, LimiteMed, ZtatusIsp,
  ZtatusMot, OVcYnsARQ, PermiFinHow, DtHrAbert, ZtatusDtH,
  SegmntInsp, SeccaoInsp);
  //
  if FCodigo <> 0 then
    Close;
{
  Codigo := UMyMod.BPGS1I32('ovgispgercab', 'Codigo', '', '', tsPos, SQLType, Codigo);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'ovgispgercab', False, [
  'Local', 'NrOP', 'SeqGrupo',
  'NrReduzidoOP', 'DtHrAbert', (*'DtHrFecha',*)
  'OVcYnsMed', 'OVcYnsChk', 'LimiteChk',
  'LimiteMed', 'ZtatusIsp', 'ZtatusDtH',
  'ZtatusMot', 'OVcYnsARQ', 'PermiFinHow'], [
  'Codigo'], [
  Local, NrOP, SeqGrupo,
  NrReduzidoOP, DtHrAbert, (*DtHrFecha,*)
  OVcYnsMed, OVcYnsChk, LimiteChk,
  LimiteMed, ZtatusIsp, ZtatusDtH,
  ZtatusMot, OVcYnsARQ, PermiFinHow], [
  Codigo], True) then
  begin
    FCodigo := Codigo;
    if SQLType = stIns then
      OVS_PF.AlteraZtatusOVgIspAllSta(CO_OVS_IMPORT_ALHEIO_5...,
        ZtatusMot, Local, NrOP, SeqGrupo, NrReduzidoOP, Agora);
     Close;
  end;
}
end;

procedure TFmOVgIspGerCad.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmOVgIspGerCad.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmOVgIspGerCad.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  FCodigo   := 0;
  FSeqGrupo := 0;
  //UnDmkDAC_PF.AbreQuery(QrOVcYnsMedCad, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrOVcYnsChkCad, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrOVcYnsARQCad, Dmod.MyDB);
end;

procedure TFmOVgIspGerCad.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmOVgIspGerCad.ReopenOVcYnsMedCad(ArtigRef: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrOVcYnsMedCad, Dmod.MyDB, [
  'SELECT Codigo, Nome',
  'FROM ovcynsmedcad',
  'WHERE ArtigRef=' + Geral.FF0(ArtigRef),
  'ORDER BY Nome',
  '']);
end;

procedure TFmOVgIspGerCad.SBOVcYnsARQClick(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  OVS_Jan.MostraFormOVcYnsARQCad();
  UMyMod.SetaCodigoPesquisado(EdOVcYnsARQ, CBOVcYnsARQ, QrOVcYnsARQCad, VAR_CADASTRO);
end;

procedure TFmOVgIspGerCad.SbOVcYnsChkClick(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  OVS_Jan.MostraFormOVcYnsChkCad();
  UMyMod.SetaCodigoPesquisado(EdOVcYnsChk, CBOVcYnsChk, QrOVcYnsChkCad, VAR_CADASTRO);
end;

procedure TFmOVgIspGerCad.SbOVcYnsMedClick(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  OVS_Jan.MostraFormOVcYnsMedCad(FSeqGrupo);
  UMyMod.SetaCodigoPesquisado(EdOVcYnsMed, CBOVcYnsMed, QrOVcYnsMedCad,
    VAR_CADASTRO);
end;

end.
