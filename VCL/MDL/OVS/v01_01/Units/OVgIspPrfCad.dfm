object FmOVgIspPrfCad: TFmOVgIspPrfCad
  Left = 339
  Top = 185
  Caption = 'OVS-INSPE-007 :: Cadastro de Perfil de Inspe'#231#227'o de Fac'#231#245'es'
  ClientHeight = 506
  ClientWidth = 812
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 812
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 764
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 716
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 524
        Height = 32
        Caption = 'Cadastro de Perfil de Inspe'#231#227'o de Fac'#231#245'es'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 524
        Height = 32
        Caption = 'Cadastro de Perfil de Inspe'#231#227'o de Fac'#231#245'es'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 524
        Height = 32
        Caption = 'Cadastro de Perfil de Inspe'#231#227'o de Fac'#231#245'es'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 812
    Height = 344
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 812
      Height = 344
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 812
        Height = 344
        Align = alClient
        TabOrder = 0
        object GroupBox2: TGroupBox
          Left = 2
          Top = 15
          Width = 808
          Height = 58
          Align = alTop
          Caption = '  Dados Importados: '
          TabOrder = 1
          object PnDadosOri: TPanel
            Left = 2
            Top = 15
            Width = 804
            Height = 41
            Align = alClient
            BevelOuter = bvNone
            TabOrder = 0
            object Label4: TLabel
              Left = 8
              Top = 0
              Width = 30
              Height = 13
              Caption = 'Artigo:'
            end
            object Label5: TLabel
              Left = 304
              Top = 0
              Width = 95
              Height = 13
              Caption = 'Descri'#231#227'o do artigo:'
            end
            object Label1: TLabel
              Left = 92
              Top = 0
              Width = 55
              Height = 13
              Caption = 'Refer'#234'ncia:'
            end
            object EdSeqGrupo: TdmkEdit
              Left = 8
              Top = 16
              Width = 80
              Height = 21
              TabStop = False
              Alignment = taRightJustify
              TabOrder = 0
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
            end
            object EdNO_SeqGrupo: TdmkEdit
              Left = 304
              Top = 16
              Width = 497
              Height = 21
              TabStop = False
              TabOrder = 1
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object EdReferencia: TdmkEdit
              Left = 91
              Top = 16
              Width = 210
              Height = 21
              TabStop = False
              TabOrder = 2
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
          end
        end
        object GroupBox3: TGroupBox
          Left = 2
          Top = 73
          Width = 808
          Height = 269
          Align = alClient
          Caption = ' Configura'#231#245'es de Inspe'#231#227'o: '
          TabOrder = 0
          TabStop = True
          object Panel5: TPanel
            Left = 2
            Top = 15
            Width = 804
            Height = 252
            Align = alClient
            BevelOuter = bvNone
            TabOrder = 0
            TabStop = True
            object Label7: TLabel
              Left = 8
              Top = 44
              Width = 93
              Height = 13
              Caption = 'Tabela de medidas:'
            end
            object Label8: TLabel
              Left = 8
              Top = 84
              Width = 147
              Height = 13
              Caption = 'Check list de inconformidades: '
            end
            object SbOVcYnsChk: TSpeedButton
              Left = 776
              Top = 100
              Width = 21
              Height = 21
              Caption = '...'
              OnClick = SbOVcYnsChkClick
            end
            object SbOVcYnsMed: TSpeedButton
              Left = 776
              Top = 60
              Width = 21
              Height = 21
              Caption = '...'
              OnClick = SbOVcYnsMedClick
            end
            object Label13: TLabel
              Left = 8
              Top = 4
              Width = 43
              Height = 13
              Caption = 'ID Perfil: '
            end
            object Label14: TLabel
              Left = 8
              Top = 124
              Width = 220
              Height = 13
              Caption = 'Plano de Amostragem e Regime de Qualidade:'
            end
            object SBOVcYnsARQ: TSpeedButton
              Left = 776
              Top = 140
              Width = 21
              Height = 21
              Caption = '...'
              OnClick = SBOVcYnsARQClick
            end
            object Label15: TLabel
              Left = 148
              Top = 4
              Width = 289
              Height = 13
              Caption = 'Descri'#231#227'o do perfil [F3 = Referencia][F4 Descri'#231#227'o do artigo]:'
            end
            object EdOVcYnsMed: TdmkEditCB
              Left = 8
              Top = 60
              Width = 56
              Height = 21
              Alignment = taRightJustify
              TabOrder = 1
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              QryCampo = 'Topico'
              UpdCampo = 'Topico'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              DBLookupComboBox = CBOVcYnsMed
              IgnoraDBLookupComboBox = False
              AutoSetIfOnlyOneReg = setregOnlyManual
            end
            object CBOVcYnsMed: TdmkDBLookupComboBox
              Left = 64
              Top = 60
              Width = 709
              Height = 21
              KeyField = 'Codigo'
              ListField = 'Nome'
              ListSource = DsOVcYnsMedCad
              TabOrder = 2
              dmkEditCB = EdOVcYnsMed
              QryCampo = 'Topico'
              UpdType = utYes
              LocF7SQLMasc = '$#'
            end
            object EdOVcYnsChk: TdmkEditCB
              Left = 8
              Top = 100
              Width = 56
              Height = 21
              Alignment = taRightJustify
              TabOrder = 3
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              QryCampo = 'Topico'
              UpdCampo = 'Topico'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              DBLookupComboBox = CBOVcYnsChk
              IgnoraDBLookupComboBox = False
              AutoSetIfOnlyOneReg = setregOnlyManual
            end
            object CBOVcYnsChk: TdmkDBLookupComboBox
              Left = 64
              Top = 100
              Width = 709
              Height = 21
              KeyField = 'Codigo'
              ListField = 'Nome'
              ListSource = DsOVcYnsChkCad
              TabOrder = 4
              dmkEditCB = EdOVcYnsChk
              QryCampo = 'Topico'
              UpdType = utYes
              LocF7SQLMasc = '$#'
            end
            object GroupBox4: TGroupBox
              Left = 156
              Top = 164
              Width = 173
              Height = 81
              Caption = ' Check List de inconformidades: '
              TabOrder = 8
              object Panel6: TPanel
                Left = 2
                Top = 15
                Width = 169
                Height = 64
                Align = alClient
                BevelOuter = bvNone
                TabOrder = 0
                object Label9: TLabel
                  Left = 28
                  Top = 4
                  Width = 85
                  Height = 13
                  Caption = 'Limite de Pe'#231'as*: '
                end
                object Label11: TLabel
                  Left = 12
                  Top = 44
                  Width = 122
                  Height = 13
                  Caption = '*0 = Sem limite de pe'#231'as. '
                end
                object EdLimiteChk: TdmkEdit
                  Left = 28
                  Top = 20
                  Width = 80
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 0
                  FormatType = dmktfInteger
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0
                  ValWarn = False
                end
              end
            end
            object GroupBox5: TGroupBox
              Left = 8
              Top = 164
              Width = 141
              Height = 81
              Caption = ' Tabela de medidas: '
              TabOrder = 7
              object Panel7: TPanel
                Left = 2
                Top = 15
                Width = 137
                Height = 64
                Align = alClient
                BevelOuter = bvNone
                TabOrder = 0
                object Label10: TLabel
                  Left = 28
                  Top = 4
                  Width = 85
                  Height = 13
                  Caption = 'Limite de Pe'#231'as*: '
                end
                object Label12: TLabel
                  Left = 8
                  Top = 44
                  Width = 122
                  Height = 13
                  Caption = '*0 = Sem limite de pe'#231'as. '
                end
                object EdLimiteMed: TdmkEdit
                  Left = 28
                  Top = 20
                  Width = 80
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 0
                  FormatType = dmktfInteger
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '10'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 10
                  ValWarn = False
                end
              end
            end
            object EdCodigo: TdmkEdit
              Left = 8
              Top = 20
              Width = 80
              Height = 21
              TabStop = False
              Alignment = taRightJustify
              ReadOnly = True
              TabOrder = 10
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
            end
            object EdOVcYnsARQ: TdmkEditCB
              Left = 8
              Top = 140
              Width = 56
              Height = 21
              Alignment = taRightJustify
              TabOrder = 5
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              QryCampo = 'Topico'
              UpdCampo = 'Topico'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              DBLookupComboBox = CBOVcYnsARQ
              IgnoraDBLookupComboBox = False
              AutoSetIfOnlyOneReg = setregOnlyManual
            end
            object CBOVcYnsARQ: TdmkDBLookupComboBox
              Left = 64
              Top = 140
              Width = 709
              Height = 21
              KeyField = 'Codigo'
              ListField = 'Nome'
              ListSource = DsOVcYnsARQCad
              TabOrder = 6
              dmkEditCB = EdOVcYnsARQ
              QryCampo = 'Topico'
              UpdType = utYes
              LocF7SQLMasc = '$#'
            end
            object RGPermiFinHow: TdmkRadioGroup
              Left = 336
              Top = 164
              Width = 461
              Height = 81
              Caption = ' Permiss'#227'o de finaliza'#231#227'o da inspe'#231#227'o: '
              Columns = 2
              ItemIndex = 0
              Items.Strings = (
                '0 - Indefinido'
                '1 - Somenteao finalizar toda inspe'#231#227'o'
                '2 - Ao atingir a pontua'#231#227'o de reprova'#231#227'o'
                '3 - A qualquer momento')
              TabOrder = 9
              QryCampo = 'PermiFinHow'
              UpdCampo = 'PermiFinHow'
              UpdType = utYes
              OldValor = 0
            end
            object CkAtivo: TdmkCheckBox
              Left = 92
              Top = 24
              Width = 53
              Height = 17
              TabStop = False
              Caption = 'Ativo.'
              Checked = True
              State = cbChecked
              TabOrder = 11
              UpdType = utYes
              ValCheck = #0
              ValUncheck = #0
              OldValor = #0
            end
            object EdNome: TdmkEdit
              Left = 148
              Top = 20
              Width = 649
              Height = 21
              ReadOnly = True
              TabOrder = 0
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
              OnKeyDown = EdNomeKeyDown
            end
          end
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 392
    Width = 812
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 808
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 436
    Width = 812
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 666
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 664
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 440
    Top = 7
  end
  object QrOVcYnsMedCad: TMySQLQuery
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM ovcynsmedcad'
      'ORDER BY Nome')
    Left = 480
    Top = 60
    object QrOVcYnsMedCadCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrOVcYnsMedCadNome: TWideStringField
      DisplayWidth = 60
      FieldName = 'Nome'
      Size = 60
    end
  end
  object DsOVcYnsMedCad: TDataSource
    DataSet = QrOVcYnsMedCad
    Left = 480
    Top = 108
  end
  object QrOVcYnsChkCad: TMySQLQuery
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM ovcynschkcad'
      'ORDER BY Nome')
    Left = 584
    Top = 60
    object QrOVcYnsChkCadCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrOVcYnsChkCadNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
  end
  object DsOVcYnsChkCad: TDataSource
    DataSet = QrOVcYnsChkCad
    Left = 584
    Top = 108
  end
  object QrOVcYnsARQCad: TMySQLQuery
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM ovcynsarqcad'
      'ORDER BY Nome')
    Left = 664
    Top = 160
    object QrOVcYnsARQCadCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrOVcYnsARQCadNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
  end
  object DsOVcYnsARQCad: TDataSource
    DataSet = QrOVcYnsARQCad
    Left = 664
    Top = 208
  end
end
