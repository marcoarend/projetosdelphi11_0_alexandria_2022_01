unit OVcYnsMedDimAdd;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, DBCtrls, dmkLabel, Mask,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkDBEdit, dmkGeral, Variants,
  dmkValUsu, dmkImage, UnDmkEnums, Vcl.Grids, Vcl.DBGrids, dmkDBGridZTO,
  mySQLDirectQuery, Vcl.ComCtrls, UnOVS_Vars;

type
  TFmOVcYnsMedDimAdd = class(TForm)
    Panel1: TPanel;
    CkContinuar: TCheckBox;
    GroupBox1: TGroupBox;
    DBEdCodigo: TdmkDBEdit;
    Label5: TLabel;
    DBEdNome: TDBEdit;
    Label3: TLabel;
    GroupBox2: TGroupBox;
    CBGrade: TdmkDBLookupComboBox;
    EdGrade: TdmkEditCB;
    Label1: TLabel;
    SbTopico: TSpeedButton;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    QrOVdGradeCad: TMySQLQuery;
    QrOVdGradeCadCodigo: TIntegerField;
    QrOVdGradeCadNome: TWideStringField;
    DsOVdGradeCad: TDataSource;
    DBGGradeTam: TdmkDBGridZTO;
    QrOVdGradeTam: TMySQLQuery;
    DsOVdGradeTam: TDataSource;
    QrOVdGradeTamCodigo: TIntegerField;
    QrOVdGradeTamNome: TWideStringField;
    BtTodos: TBitBtn;
    BtNenhum: TBitBtn;
    QrGrade: TMySQLQuery;
    QrGradeCodGrade: TIntegerField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure SbTopicoClick(Sender: TObject);
    procedure EdGradeRedefinido(Sender: TObject);
    procedure QrOVdGradeTamAfterOpen(DataSet: TDataSet);
    procedure QrOVdGradeTamBeforeClose(DataSet: TDataSet);
    procedure BtTodosClick(Sender: TObject);
    procedure BtNenhumClick(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenDim();
    procedure ReopenOVdGradeCad();
    procedure ReopenOVdGradeTam();
  public
    { Public declarations }
    FQrTop: TmySQLQuery;
    FDsCad, FDsCtx: TDataSource;
    FProduto, FContexRef: Integer;
    //
    procedure ReopenGrade(Artigo: Integer);
  end;

  var
  FmOVcYnsMedDimAdd: TFmOVcYnsMedDimAdd;

implementation

uses UnMyObjects, Module, UMySQLModule, UnInternalConsts, MyDBCheck, DmkDAC_PF,
  UnOVS_Jan, OVcYnsMedCad;

{$R *.DFM}

procedure TFmOVcYnsMedDimAdd.BtNenhumClick(Sender: TObject);
begin
  MyObjects.SelecionarLinhasNoDBGrid(TDBGrid(DBGGradeTam), False);
end;

procedure TFmOVcYnsMedDimAdd.BtOKClick(Sender: TObject);
{
const
  iAutomatico = 0;
var
  CodTam, sCodigo, sControle: String;
  //UlWayInz,
  Codigo, Controle, Conta, CodGrade: Integer;
  //MedidCer, MedidMin, MedidMax: Double;
  SQLType: TSQLType;
  //
  I, K, M: Integer;
  //
  function ExecutaSQL(): Boolean;
  begin
    CodGrade := QrOVdGradeTamCodigo.Value;
    CodTam   := QrOVdGradeTamNome.Value;
    //
    UnDmkDAC_PF.AbreMySQLDirectQuery0(DqAux, Dmod.MyDB, [
    'SELECT Conta ',
    'FROM ovcynsmeddim ',
    'WHERE Codigo=' + sCodigo,
    'AND Controle=' + sControle,
    'AND CodGrade=' + Geral.FF0(CodGrade),
    'AND CodTam = "' + CodTam + '" ',
    '']);
    Conta := USQLDB.Int(DqAux, 'Conta');
    if Conta = 0 then
    begin
      Conta := UMyMod.BPGS1I32('ovcynsmeddim', 'Conta', '', '', tsPos, SQLType, Conta);
      //
      if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'ovcynsmeddim', False, [
      'Codigo', 'Controle', 'CodGrade',
      'CodTam'(*, 'MedidCer', 'MedidMin',
      'MedidMax', 'UlWayInz'*)], [
      'Conta'], [
      Codigo, Controle, CodGrade,
      CodTam(*, MedidCer, MedidMin,
      MedidMax, UlWayInz*)], [
      Conta], True) then
        M := M + 1;
    end;
    Result := True;
  end;
}
var
  I, N: Integer;
  GradesToAdd: TGradesToAdd;
  TamsToAdd: TTamsToAdd;
  //
begin
  N := DBGGradeTam.SelectedRows.Count;
  if N > 0 then
  begin
    SetLength(GradesToAdd, N);
    SetLength(TamsToAdd, N);
    //
    for I := 0 to N - 1 do
    begin
      //PB1.Position := PB1.Position + 1;
      //
      with DBGGradeTam.DataSource.DataSet do
      begin
        GotoBookmark(TDBGrid(DBGGradeTam).SelectedRows.Items[I]);
        GradesToAdd[I] := QrOVdGradeTamCodigo.Value;
        TamsToAdd[I]   := QrOVdGradeTamNome.Value;
      end;
    end;
    //
    FmOVcYnsMedCad.VerificaEAdicionaNovosItensGrade(GradesToAdd, TamsToAdd);
  end else
    Geral.MB_Aviso('Nenhum item foi selecionado!');
{
  Screen.Cursor := crHourGlass;
  try
    SQLType        := ImgTipo.SQLType;
    Codigo         := Geral.IMV(DBEdCodigo.Text);
    //Controle       := Geral.IMV(DBEdControle.Text); Todos!
    Conta          := 0;
    //CodGrade       := 0;  // Abaixo. Item a item
    //CodTam         := ''; // Abaixo. Item a item
    //MedidCer       := 0; //  Depois na janela OVcYnsMesDimFil !!!
    //MedidMin       := 0; //  Depois na janela OVcYnsMesDimFil !!!
    //MedidMax       := 0; //  Depois na janela OVcYnsMesDimFil !!!
    //UlWayInz       := iAutomatico;
    //
    M         := 0;
    K         := DBGGradeTam.SelectedRows.Count;
    sCodigo   := Geral.FF0(Codigo);
    sControle := Geral.FF0(Controle);
    PB1.Position := 0;
    PB1.Max := K;
    for I := 0 to K - 1 do
    begin
      PB1.Position := PB1.Position + 1;
      //
      with DBGGradeTam.DataSource.DataSet do
      begin
        GotoBookmark(TDBGrid(DBGGradeTam).SelectedRows.Items[I]);
        if ExecutaSQL() then
          ;
      end;
    end;
    ReopenDim();
  finally
    Screen.Cursor := crDefault;
  end;
  Geral.MB_Info(Geral.FF0(M) + ' registros foram adicionados!');
}
end;

procedure TFmOVcYnsMedDimAdd.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmOVcYnsMedDimAdd.BtTodosClick(Sender: TObject);
begin
  MyObjects.SelecionarLinhasNoDBGrid(TDBGrid(DBGGradeTam), True);
end;

procedure TFmOVcYnsMedDimAdd.EdGradeRedefinido(Sender: TObject);
begin
  ReopenOVdGradeTam();
end;

procedure TFmOVcYnsMedDimAdd.FormActivate(Sender: TObject);
begin
  DBEdCodigo.DataSource   := FDsCad;
  DBEdNome.DataSource     := FDsCad;
  //
  //DBEdControle.DataSource := FDsCtx;
  //DBEdCtrlNome.DataSource := FDsCtx;
  //
  MyObjects.CorIniComponente();
end;

procedure TFmOVcYnsMedDimAdd.FormCreate(Sender: TObject);
begin
  //ImgTipo.SQLType := stLok;
  ImgTipo.SQLType := stIns;
  //
  ReopenOVdGradeCad();
end;

procedure TFmOVcYnsMedDimAdd.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmOVcYnsMedDimAdd.QrOVdGradeTamAfterOpen(DataSet: TDataSet);
begin
  if QrOVdGradeTam.RecordCount > 0 then
  begin
    BtTodos.Enabled  := True;
    BtNenhum.Enabled := True;
  end;
end;

procedure TFmOVcYnsMedDimAdd.QrOVdGradeTamBeforeClose(DataSet: TDataSet);
begin
    BtTodos.Enabled  := False;
    BtNenhum.Enabled := False;
end;

procedure TFmOVcYnsMedDimAdd.ReopenDim();
begin
  if FQrTop <> nil then
  begin
    UnDmkDAC_PF.AbreQuery(FQrTop, FQrTop.Database);
    //
  //  if Conta <> 0 then
  //    FQrTop.Locate('Conta', Conta, []);
  end;
end;

procedure TFmOVcYnsMedDimAdd.ReopenGrade(Artigo: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrGrade, DMod.MyDB, [
  'SELECT DISTINCT CodGrade  ',
  'FROM ovdProduto ',
  'WHERE Codigo=' + Geral.FF0(FProduto),
  '']);
end;

procedure TFmOVcYnsMedDimAdd.ReopenOVdGradeCad();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrOVdGradeCad, DMod.MyDB, [
  'SELECT Codigo, Nome ',
  'FROM ovdgradecad ',
  'ORDER BY Nome ',
  '']);
end;

procedure TFmOVcYnsMedDimAdd.ReopenOVdGradeTam();
var
  _WHERE: String;
  Grade: Integer;
begin
  Grade := EdGrade.ValueVariant;
  if Grade <> 0 then
  begin
    _WHERE := 'WHERE Codigo=' + Geral.FF0(Grade);
    UnDmkDAC_PF.AbreMySQLQuery0(QrOVdGradeTam, DMod.MyDB, [
    'SELECT Codigo, Nome',
    'FROM ovdgradetam',
    _WHERE,
    'ORDER BY Nome',
    '']);
  end else
    QrOVdGradeTam.Close;
end;

procedure TFmOVcYnsMedDimAdd.SbTopicoClick(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  OVS_Jan.MostraFormOVcYnsQstTop();
  UMyMod.SetaCodigoPesquisado(EdGrade, CBGrade, QrOVdGradeCad, VAR_CADASTRO);
end;

{
object DBEdControle: TdmkDBEdit
  Left = 336
  Top = 36
  Width = 56
  Height = 21
  TabStop = False
  DataField = 'Controle'
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  ParentFont = False
  ParentShowHint = False
  ReadOnly = True
  ShowHint = True
  TabOrder = 2
  UpdCampo = 'Codigo'
  UpdType = utYes
  Alignment = taRightJustify
end
object Label2: TLabel
  Left = 336
  Top = 20
  Width = 14
  Height = 13
  Caption = 'ID:'
  FocusControl = DBEdControle
end
object Label4: TLabel
  Left = 396
  Top = 20
  Width = 98
  Height = 13
  Caption = 'Descri'#231#227'o do t'#243'pico:'
  FocusControl = DBEdCtrlNome
end
object DBEdCtrlNome: TDBEdit
  Left = 396
  Top = 36
  Width = 381
  Height = 21
  TabStop = False
  Color = clWhite
  DataField = 'NO_TOBIKO'
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  ParentFont = False
  ParentShowHint = False
  ShowHint = True
  TabOrder = 3
end
}
end.
