unit OVcYnsARQIts;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, DBCtrls, dmkLabel, Mask,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkDBEdit, dmkGeral, Variants,
  dmkValUsu, dmkImage, UnDmkEnums;

type
  TFmOVcYnsARQIts = class(TForm)
    Panel1: TPanel;
    CkContinuar: TCheckBox;
    GroupBox1: TGroupBox;
    DBEdCodigo: TdmkDBEdit;
    Label5: TLabel;
    DBEdNome: TDBEdit;
    Label3: TLabel;
    GroupBox2: TGroupBox;
    EdControle: TdmkEdit;
    Label6: TLabel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    GroupBox3: TGroupBox;
    Panel3: TPanel;
    EdQtdPcIni: TdmkEdit;
    Label1: TLabel;
    GroupBox4: TGroupBox;
    Panel5: TPanel;
    Label2: TLabel;
    EDQtdAmostr: TdmkEdit;
    GroupBox5: TGroupBox;
    Panel6: TPanel;
    Label4: TLabel;
    EdPtsAprova: TdmkEdit;
    Label7: TLabel;
    EdQtdPcFim: TdmkEdit;
    Label8: TLabel;
    EdPtsResalv: TdmkEdit;
    Label9: TLabel;
    EdPtsRejeit: TdmkEdit;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenOVcYnsARQCIts(Controle: Integer);
  public
    { Public declarations }
    FQrCab, FQrIts: TmySQLQuery;
    FDsCab: TDataSource;
  end;

  var
  FmOVcYnsARQIts: TFmOVcYnsARQIts;

implementation

uses UnMyObjects, Module, UMySQLModule, UnInternalConsts, MyDBCheck, DmkDAC_PF;

{$R *.DFM}

procedure TFmOVcYnsARQIts.BtOKClick(Sender: TObject);
var
  Codigo, Controle, QtdPcIni, QtdPcFim, QtdAmostr: Integer;
  PtsAprova, PtsResalv, PtsRejeit: Double;
  SQLType: TSQLType;
begin
  SQLType        := ImgTipo.SQLType;
  Codigo         := Geral.IMV(DBEdCodigo.Text);
  Controle       := EdControle.ValueVariant;
  QtdPcIni       := EdQtdPcIni.ValueVariant;
  QtdPcFim       := EdQtdPcFim.ValueVariant;
  QtdAmostr      := EdQtdAmostr.ValueVariant;
  PtsAprova      := EdPtsAprova.ValueVariant;
  PtsResalv      := EdPtsResalv.ValueVariant;
  PtsRejeit      := EdPtsRejeit.ValueVariant;
  //
  if MyObjects.FIC(QtdPcIni = 0, EdQtdPcIni, 'Informe o campo "De" maior que zero!') then
    Exit;
  if MyObjects.FIC(QtdPcFim = 0, EdQtdPcFim, 'Informe o campo "At�" maior que zero!') then
    Exit;
  if MyObjects.FIC(QtdPcFim < QtdPcIni, EdQtdPcIni, 'Informe o campo "At�" maior ou igual a "De"!') then
    Exit;
  //
  Controle := UMyMod.BPGS1I32('ovcynsarqits', 'Controle', '', '', tsPos, SQLType, Controle);

  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'ovcynsarqits', False, [
  'Codigo', 'QtdPcIni', 'QtdPcFim',
  'QtdAmostr', 'PtsAprova', 'PtsResalv',
  'PtsRejeit'], [
  'Controle'], [
  Codigo, QtdPcIni, QtdPcFim,
  QtdAmostr, PtsAprova, PtsResalv,
  PtsRejeit], [
  Controle], True) then
  begin
    ReopenOVcYnsARQCIts(Controle);
    if CkContinuar.Checked then
    begin
      ImgTipo.SQLType          := stIns;
      EdQtdPcIni.ValueVariant  := EdQtdPcFim.ValueVariant + 1;
      EdControle.ValueVariant  := 0;
      EdQtdPcIni.SetFocus;
    end else Close;
  end;
end;

procedure TFmOVcYnsARQIts.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmOVcYnsARQIts.FormActivate(Sender: TObject);
begin
  DBEdCodigo.DataSource := FDsCab;
  DBEdNome.DataSource := FDsCab;
  MyObjects.CorIniComponente();
end;

procedure TFmOVcYnsARQIts.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
end;

procedure TFmOVcYnsARQIts.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmOVcYnsARQIts.ReopenOVcYnsARQCIts(Controle: Integer);
begin
  if FQrIts <> nil then
  begin
    UnDmkDAC_PF.AbreQuery(FQrIts, FQrIts.Database);
    //
    if Controle <> 0 then
      FQrIts.Locate('Controle', Controle, []);
  end;
end;

end.
