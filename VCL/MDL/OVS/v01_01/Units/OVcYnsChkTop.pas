unit OVcYnsChkTop;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, DBCtrls, dmkLabel, Mask,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkDBEdit, dmkGeral, Variants,
  dmkValUsu, dmkImage, UnDmkEnums;

type
  TFmOVcYnsChkTop = class(TForm)
    Panel1: TPanel;
    CkContinuar: TCheckBox;
    GroupBox1: TGroupBox;
    DBEdCodigo: TdmkDBEdit;
    Label5: TLabel;
    DBEdNome: TDBEdit;
    Label3: TLabel;
    GroupBox2: TGroupBox;
    EdConta: TdmkEdit;
    Label6: TLabel;
    EdNome: TdmkEdit;
    Label7: TLabel;
    CBTopico: TdmkDBLookupComboBox;
    EdTopico: TdmkEditCB;
    Label1: TLabel;
    SbTopico: TSpeedButton;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    QrOVcYnsQstTop: TMySQLQuery;
    QrOVcYnsQstTopCodigo: TIntegerField;
    QrOVcYnsQstTopNome: TWideStringField;
    DsOVcYnsQstTop: TDataSource;
    Label2: TLabel;
    DBEdControle: TdmkDBEdit;
    DBEdCtrlNome: TDBEdit;
    Label4: TLabel;
    Label8: TLabel;
    EdMagnitude: TdmkEditCB;
    CBMagnitude: TdmkDBLookupComboBox;
    SbMagnitude: TSpeedButton;
    QrOVcYnsQstMag: TMySQLQuery;
    DsOVcYnsQstMag: TDataSource;
    QrOVcYnsQstMagCodigo: TIntegerField;
    QrOVcYnsQstMagNome: TWideStringField;
    QrOVcYnsQstTopMagnitRef: TIntegerField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure SbTopicoClick(Sender: TObject);
    procedure SbMagnitudeClick(Sender: TObject);
    procedure EdTopicoRedefinido(Sender: TObject);
    procedure EdTopicoKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure CBTopicoKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
  private
    { Private declarations }
    procedure ReopenTop(Conta: Integer);
    procedure ReopenOVcYnsQstTop(Quais: TWhatPsqHow);
    procedure ReopenOVcYnsQstMag();
    procedure TopicoKeyUp(Sender: TObject; Key: Word; Shift: TShiftState);
  public
    { Public declarations }
    FQrTop: TmySQLQuery;
    FDsCad, FDsCtx: TDataSource;
    FContexRef: Integer;
  end;

  var
  FmOVcYnsChkTop: TFmOVcYnsChkTop;

implementation

uses UnMyObjects, Module, UMySQLModule, UnInternalConsts, MyDBCheck, DmkDAC_PF,
  UnOVS_Jan;

{$R *.DFM}

procedure TFmOVcYnsChkTop.BtOKClick(Sender: TObject);
var
  Codigo, Controle, Conta, Topico, Magnitude: Integer;
  SQLType: TSQLType;
  Nome: String;
begin
  SQLType        := ImgTipo.SQLType;
  Codigo         := Geral.IMV(DBEdCodigo.Text);
  Controle       := Geral.IMV(DBEdControle.Text);
  Conta          := EdConta.ValueVariant;
  Topico         := EdTopico.ValueVariant;
  Magnitude      := EdMagnitude.ValueVariant;
  //
(*
  if MyObjects.FIC(Trim(EdNome.Text) = '', EdNome, 'Informe uma descri��o!') then
    Exit;
*)
  if MyObjects.FIC(Topico = 0, EdTopico, 'Informe o T�pico!') then
    Exit;
  if MyObjects.FIC(Magnitude = 0, EdMagnitude, 'Informe a Magnitude!') then
    Exit;
  //
  Conta := UMyMod.BPGS1I32('ovcynschktop', 'Conta', '', '', tsPos, stIns, Conta);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'ovcynschktop', False, [
  'Codigo', 'Controle', 'Topico',
  'Magnitude'], [
  'Conta'], [
  Codigo, Controle, Topico,
  Magnitude], [
  Conta], True) then
  begin
    ReopenTop(Conta);
    if CkContinuar.Checked then
    begin
      ImgTipo.SQLType           := stIns;
      EdConta.ValueVariant      := 0;
      EdTopico.ValueVariant     := 0;
      CBTopico.KeyValue         := Null;
      EdMagnitude.ValueVariant  := 0;
      CBMagnitude.KeyValue      := Null;
      EdNome.ValueVariant       := '';
      //EdNome.SetFocus;
      EdTopico.SetFocus;
    end else Close;
  end;
end;

procedure TFmOVcYnsChkTop.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmOVcYnsChkTop.CBTopicoKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  TopicoKeyUp(Sender, Key, Shift);
end;

procedure TFmOVcYnsChkTop.EdTopicoKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  TopicoKeyUp(Sender, Key, Shift);
end;

procedure TFmOVcYnsChkTop.EdTopicoRedefinido(Sender: TObject);
begin
  //if EdMagnitude.ValueVariant = 0 then
  begin
    if QrOVcYnsQstTopMagnitRef.Value <> 0 then
    begin
      EdMagnitude.ValueVariant := QrOVcYnsQstTopMagnitRef.Value;
      CBMagnitude.KeyValue     := QrOVcYnsQstTopMagnitRef.Value;
    end;
  end;
end;

procedure TFmOVcYnsChkTop.FormActivate(Sender: TObject);
begin
  DBEdCodigo.DataSource   := FDsCad;
  DBEdNome.DataSource     := FDsCad;
  //
  DBEdControle.DataSource := FDsCtx;
  DBEdCtrlNome.DataSource := FDsCtx;
  //
  MyObjects.CorIniComponente();
end;

procedure TFmOVcYnsChkTop.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  ReopenOVcYnsQstTop(TWhatPsqHow.wphAll);
  ReopenOVcYnsQstMag();
end;

procedure TFmOVcYnsChkTop.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmOVcYnsChkTop.ReopenTop(Conta: Integer);
begin
  if FQrTop <> nil then
  begin
    UnDmkDAC_PF.AbreQuery(FQrTop, FQrTop.Database);
    //
    if Conta <> 0 then
      FQrTop.Locate('Conta', Conta, []);
  end;
end;

procedure TFmOVcYnsChkTop.ReopenOVcYnsQstMag();
begin
  UnDMkDAC_PF.AbreQuery(QrOVcYnsQstMag, DMod.MyDB);
end;

procedure TFmOVcYnsChkTop.ReopenOVcYnsQstTop(Quais: TWhatPsqHow);
var
  _WHERE: String;
begin
  if Quais = TWhatPsqHow.wphParcial then
    _WHERE := 'WHERE ContexRef=' + Geral.FF0(FContexRef);
  UnDmkDAC_PF.AbreMySQLQuery0(QrOVcYnsQstTop, DMod.MyDB, [
  'SELECT Codigo, Nome, MagnitRef ',
  'FROM ovcynsqsttop yqt ',
  _WHERE,
  'ORDER BY Nome ',
  '']);
end;

procedure TFmOVcYnsChkTop.SbMagnitudeClick(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  OVS_Jan.MostraFormOVcYnsQstMag();
  UMyMod.SetaCodigoPesquisado(EdMagnitude, CBMagnitude, QrOVcYnsQstMag, VAR_CADASTRO);
end;

procedure TFmOVcYnsChkTop.SbTopicoClick(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  OVS_Jan.MostraFormOVcYnsQstTop();
  UMyMod.SetaCodigoPesquisado(EdTopico, CBTopico, QrOVcYnsQstTop, VAR_CADASTRO);
end;

procedure TFmOVcYnsChkTop.TopicoKeyUp(Sender: TObject; Key: Word;
  Shift: TShiftState);
begin
    if Key = VK_F3 then
    ReopenOVcYnsQstTop(TWhatPsqHow.wphAll);
  if Key = VK_F4 then
    ReopenOVcYnsQstTop(TWhatPsqHow.wphParcial);
end;

end.
