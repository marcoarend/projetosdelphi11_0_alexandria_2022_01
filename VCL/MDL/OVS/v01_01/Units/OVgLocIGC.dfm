object FmOVgLocIGC: TFmOVgLocIGC
  Left = 339
  Top = 185
  Caption = 'OVS-INSPE-003 :: Localizar Configura'#231#227'o de Inspe'#231#245'es de Fac'#231#227'o'
  ClientHeight = 629
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 912
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 570
        Height = 32
        Caption = 'Localizar Configura'#231#227'o de Inspe'#231#227'o de Fac'#231#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 570
        Height = 32
        Caption = 'Localizar Configura'#231#227'o de Inspe'#231#227'o de Fac'#231#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 570
        Height = 32
        Caption = 'Localizar Configura'#231#227'o de Inspe'#231#227'o de Fac'#231#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 1008
    Height = 467
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 1008
      Height = 467
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 1008
        Height = 467
        Align = alClient
        TabOrder = 0
        object GroupBox2: TGroupBox
          Left = 2
          Top = 15
          Width = 1004
          Height = 106
          Align = alTop
          Caption = ' Filtros de pesquisa: '
          TabOrder = 0
          object Panel5: TPanel
            Left = 2
            Top = 15
            Width = 1000
            Height = 89
            Align = alClient
            TabOrder = 0
            object Label7: TLabel
              Left = 4
              Top = 4
              Width = 29
              Height = 13
              Caption = 'Local:'
            end
            object Label3: TLabel
              Left = 768
              Top = 48
              Width = 18
              Height = 13
              Caption = 'OP:'
            end
            object Label6: TLabel
              Left = 844
              Top = 48
              Width = 52
              Height = 13
              Caption = 'Reduz.OP:'
            end
            object Label1: TLabel
              Left = 4
              Top = 48
              Width = 30
              Height = 13
              Caption = 'Artigo:'
            end
            object Label2: TLabel
              Left = 64
              Top = 48
              Width = 55
              Height = 13
              Caption = 'Refer'#234'ncia:'
            end
            object Label4: TLabel
              Left = 616
              Top = 4
              Width = 174
              Height = 13
              Caption = 'Status da configura'#231#227'o de inspe'#231#227'o:'
            end
            object EdLocal: TdmkEditCB
              Left = 4
              Top = 20
              Width = 56
              Height = 21
              Alignment = taRightJustify
              TabOrder = 0
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              QryCampo = 'Topico'
              UpdCampo = 'Topico'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              DBLookupComboBox = CBLocal
              IgnoraDBLookupComboBox = False
              AutoSetIfOnlyOneReg = setregOnlyManual
            end
            object CBLocal: TdmkDBLookupComboBox
              Left = 60
              Top = 20
              Width = 553
              Height = 21
              KeyField = 'Codigo'
              ListField = 'Nome'
              ListSource = DsOVdLocal
              TabOrder = 1
              dmkEditCB = EdLocal
              UpdType = utYes
              LocF7SQLMasc = '$#'
            end
            object EdNrOP: TdmkEdit
              Left = 768
              Top = 64
              Width = 72
              Height = 21
              Alignment = taRightJustify
              ReadOnly = True
              TabOrder = 2
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
            end
            object EdNrReduzidoOP: TdmkEdit
              Left = 844
              Top = 64
              Width = 72
              Height = 21
              Alignment = taRightJustify
              ReadOnly = True
              TabOrder = 3
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
            end
            object EdArtigo: TdmkEditCB
              Left = 4
              Top = 64
              Width = 56
              Height = 21
              Alignment = taRightJustify
              TabOrder = 4
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              QryCampo = 'Topico'
              UpdCampo = 'Topico'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              OnChange = EdArtigoChange
              OnKeyDown = EdArtigoKeyDown
              DBLookupComboBox = CBArtigo
              IgnoraDBLookupComboBox = False
              AutoSetIfOnlyOneReg = setregOnlyManual
            end
            object CBArtigo: TdmkDBLookupComboBox
              Left = 168
              Top = 64
              Width = 597
              Height = 21
              KeyField = 'Codigo'
              ListField = 'Nome'
              ListSource = DsOVdReferencia
              TabOrder = 5
              OnKeyDown = EdArtigoKeyDown
              dmkEditCB = EdArtigo
              UpdType = utYes
              LocF7SQLMasc = '$#'
            end
            object EdReferencia: TdmkEdit
              Left = 64
              Top = 64
              Width = 101
              Height = 21
              TabOrder = 6
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
              OnChange = EdReferenciaChange
              OnExit = EdReferenciaExit
              OnKeyDown = EdReferenciaKeyDown
            end
            object EdZtatusIsp: TdmkEditCB
              Left = 616
              Top = 20
              Width = 56
              Height = 21
              Alignment = taRightJustify
              TabOrder = 7
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              QryCampo = 'Topico'
              UpdCampo = 'Topico'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              DBLookupComboBox = CBZtatusIsp
              IgnoraDBLookupComboBox = False
              AutoSetIfOnlyOneReg = setregOnlyManual
            end
            object CBZtatusIsp: TdmkDBLookupComboBox
              Left = 672
              Top = 20
              Width = 245
              Height = 21
              KeyField = 'Codigo'
              ListField = 'Nome'
              ListSource = DsOVgIspStaCad
              TabOrder = 8
              dmkEditCB = EdZtatusIsp
              UpdType = utYes
              LocF7SQLMasc = '$#'
            end
          end
        end
        object DBGrid1: TdmkDBGridZTO
          Left = 2
          Top = 121
          Width = 1004
          Height = 344
          Align = alClient
          DataSource = DsOVgIspGerCab
          Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
          TabOrder = 1
          TitleFont.Charset = ANSI_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          RowColors = <>
          OnDblClick = DBGrid1DblClick
          Columns = <
            item
              Expanded = False
              FieldName = 'Codigo'
              Title.Caption = 'C'#243'digo'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Local'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_Local'
              Title.Caption = 'Descri'#231#227'o do Local'
              Width = 293
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'SeqGrupo'
              Title.Caption = 'Artigo'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_Referencia'
              Title.Caption = 'Nome artigo'
              Width = 300
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NrOP'
              Title.Caption = 'N'#186' OP'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NrReduzidoOP'
              Title.Caption = 'N'#186' Red. OP'
              Width = 70
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DtHrAbert_TXT'
              Title.Caption = 'Abertura'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DtHrFecha_TXT'
              Title.Caption = 'Encerramento'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ZtatusIsp'
              Title.Caption = 'Status'
              Visible = True
            end>
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 515
    Width = 1008
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 559
    Width = 1008
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 862
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 860
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 248
    Top = 43
  end
  object QrOVdLocal: TMySQLQuery
    Database = Dmod.ZZDB
    SQL.Strings = (
      'SELECT Codigo, Nome '
      'FROM ovdlocal '
      'ORDER BY Nome ')
    Left = 164
    Top = 274
    object QrOVdLocalCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrOVdLocalNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
  object DsOVdLocal: TDataSource
    DataSet = QrOVdLocal
    Left = 164
    Top = 322
  end
  object QrOVdReferencia: TMySQLQuery
    Database = Dmod.ZZDB
    SQL.Strings = (
      'SELECT Codigo, Referencia, Nome '
      'FROM ovdreferencia '
      'ORDER BY Nome ')
    Left = 244
    Top = 274
    object QrOVdReferenciaCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrOVdReferenciaNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
  object DsOVdReferencia: TDataSource
    DataSet = QrOVdReferencia
    Left = 244
    Top = 322
  end
  object QrOVgIspGerCab: TMySQLQuery
    Database = Dmod.ZZDB
    SQL.Strings = (
      'SELECT igc.*, ymc.Nome NO_OVcYnsMed, ycc.Nome NO_OVcYnsChk,'
      'dlo.Nome NO_Local, ref.Nome NO_Referencia, '
      'isc.Nome NO_ZtatusIsp, '
      'IF(igc.DtHrAbert  <= "1899-12-30", "",  '
      '  DATE_FORMAT(igc.DtHrAbert, "%d/%m/%Y %h:%i")) DtHrAbert_TXT,  '
      'IF(igc.DtHrFecha  <= "1899-12-30", "",  '
      '  DATE_FORMAT(igc.DtHrFecha, "%d/%m/%Y %h:%i")) DtHrFecha_TXT'
      'FROM ovgispgercab igc'
      'LEFT JOIN ovdlocal dlo ON dlo.Codigo=igc.Local  '
      'LEFT JOIN ovdreferencia ref ON ref.Codigo=igc.SeqGrupo '
      'LEFT JOIN ovcynsmedcad ymc ON ymc.Codigo=igc.OVcYnsMed'
      'LEFT JOIN ovcynschkcad ycc ON ycc.Codigo=igc.OVcYnsChk'
      'LEFT JOIN OVgIspStaCad isc ON isc.Codigo=igc.ZtatusIsp'
      ''
      'WHERE igc.Codigo > 0')
    Left = 44
    Top = 277
    object QrOVgIspGerCabCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrOVgIspGerCabNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QrOVgIspGerCabLocal: TIntegerField
      FieldName = 'Local'
      Required = True
    end
    object QrOVgIspGerCabNrOP: TIntegerField
      FieldName = 'NrOP'
      Required = True
    end
    object QrOVgIspGerCabSeqGrupo: TIntegerField
      FieldName = 'SeqGrupo'
      Required = True
    end
    object QrOVgIspGerCabNrReduzidoOP: TIntegerField
      FieldName = 'NrReduzidoOP'
      Required = True
    end
    object QrOVgIspGerCabDtHrAbert: TDateTimeField
      FieldName = 'DtHrAbert'
      Required = True
    end
    object QrOVgIspGerCabDtHrFecha: TDateTimeField
      FieldName = 'DtHrFecha'
      Required = True
    end
    object QrOVgIspGerCabOVcYnsMed: TIntegerField
      FieldName = 'OVcYnsMed'
      Required = True
    end
    object QrOVgIspGerCabOVcYnsChk: TIntegerField
      FieldName = 'OVcYnsChk'
      Required = True
    end
    object QrOVgIspGerCabLimiteChk: TIntegerField
      FieldName = 'LimiteChk'
      Required = True
    end
    object QrOVgIspGerCabLimiteMed: TIntegerField
      FieldName = 'LimiteMed'
      Required = True
    end
    object QrOVgIspGerCabZtatusIsp: TIntegerField
      FieldName = 'ZtatusIsp'
      Required = True
    end
    object QrOVgIspGerCabZtatusDtH: TDateTimeField
      FieldName = 'ZtatusDtH'
      Required = True
    end
    object QrOVgIspGerCabZtatusMot: TIntegerField
      FieldName = 'ZtatusMot'
      Required = True
    end
    object QrOVgIspGerCabLk: TIntegerField
      FieldName = 'Lk'
      Required = True
    end
    object QrOVgIspGerCabDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrOVgIspGerCabDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrOVgIspGerCabUserCad: TIntegerField
      FieldName = 'UserCad'
      Required = True
    end
    object QrOVgIspGerCabUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Required = True
    end
    object QrOVgIspGerCabAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Required = True
    end
    object QrOVgIspGerCabAWServerID: TIntegerField
      FieldName = 'AWServerID'
      Required = True
    end
    object QrOVgIspGerCabAWStatSinc: TSmallintField
      FieldName = 'AWStatSinc'
      Required = True
    end
    object QrOVgIspGerCabAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
    object QrOVgIspGerCabNO_Local: TWideStringField
      FieldName = 'NO_Local'
      Size = 100
    end
    object QrOVgIspGerCabNO_Referencia: TWideStringField
      FieldName = 'NO_Referencia'
      Size = 100
    end
    object QrOVgIspGerCabNO_OVcYnsMed: TWideStringField
      FieldName = 'NO_OVcYnsMed'
      Size = 60
    end
    object QrOVgIspGerCabNO_OVcYnsChk: TWideStringField
      FieldName = 'NO_OVcYnsChk'
      Size = 60
    end
    object QrOVgIspGerCabNO_ZtatusIsp: TWideStringField
      FieldName = 'NO_ZtatusIsp'
      Size = 60
    end
    object QrOVgIspGerCabDtHrAbert_TXT: TWideStringField
      DisplayWidth = 19
      FieldName = 'DtHrAbert_TXT'
      Size = 19
    end
    object QrOVgIspGerCabDtHrFecha_TXT: TWideStringField
      DisplayWidth = 19
      FieldName = 'DtHrFecha_TXT'
      Size = 19
    end
    object QrOVgIspGerCabOVcYnsARQ: TIntegerField
      FieldName = 'OVcYnsARQ'
    end
    object QrOVgIspGerCabNO_OVcYnsARQ: TWideStringField
      FieldName = 'NO_OVcYnsARQ'
      Size = 60
    end
    object QrOVgIspGerCabPermiFinHow: TIntegerField
      FieldName = 'PermiFinHow'
    end
  end
  object DsOVgIspGerCab: TDataSource
    DataSet = QrOVgIspGerCab
    Left = 44
    Top = 325
  end
  object QrOVgIspStaCad: TMySQLQuery
    Database = Dmod.ZZDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM ovgispstacad'
      'ORDER BY Nome')
    Left = 360
    Top = 270
    object QrOVgIspStaCadCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrOVgIspStaCadNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
  end
  object DsOVgIspStaCad: TDataSource
    DataSet = QrOVgIspStaCad
    Left = 360
    Top = 318
  end
end
