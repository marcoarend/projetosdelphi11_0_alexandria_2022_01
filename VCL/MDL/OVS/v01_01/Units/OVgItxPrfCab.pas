unit OVgItxPrfCab;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2, dmkGeral,
  ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt, UMySQLModule, Menus,
  UnInternalConsts2, mySQLDbTables, UnMySQLCuringa, dmkPermissoes, dmkEdit,
  dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup, Grids, DBGrids,
  UnDmkProcFunc, UnDmkENums, Vcl.ComCtrls;

type
  TFmOVgItxPrfCab = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    Label9: TLabel;
    EdNome: TdmkEdit;
    QrOVgItxPrfCab: TMySQLQuery;
    DsOVgItxPrfCab: TDataSource;
    PMIts: TPopupMenu;
    ItsInclui1: TMenuItem;
    ItsExclui1: TMenuItem;
    ItsAltera1: TMenuItem;
    PMCab: TPopupMenu;
    CabInclui1: TMenuItem;
    CabAltera1: TMenuItem;
    CabExclui1: TMenuItem;
    BtCab: TBitBtn;
    BtIts: TBitBtn;
    QrOVgItxPrfCabCodigo: TIntegerField;
    QrOVgItxPrfCabSeqGrupo: TIntegerField;
    QrOVgItxPrfCabOVcYnsExg: TIntegerField;
    QrOVgItxPrfCabLk: TIntegerField;
    QrOVgItxPrfCabDataCad: TDateField;
    QrOVgItxPrfCabDataAlt: TDateField;
    QrOVgItxPrfCabUserCad: TIntegerField;
    QrOVgItxPrfCabUserAlt: TIntegerField;
    QrOVgItxPrfCabAlterWeb: TSmallintField;
    QrOVgItxPrfCabAWServerID: TIntegerField;
    QrOVgItxPrfCabAWStatSinc: TSmallintField;
    QrOVgItxPrfCabAtivo: TSmallintField;
    QrOVgItxPrfCabNO_Referencia: TWideStringField;
    QrOVgItxPrfCabNome: TWideStringField;
    Panel6: TPanel;
    Label1: TLabel;
    DBEdCodigo: TdmkDBEdit;
    DBEdNome: TdmkDBEdit;
    Label2: TLabel;
    GroupBox3: TGroupBox;
    Panel7: TPanel;
    Label11: TLabel;
    DBEdit7: TDBEdit;
    QrOVgItxPrfCabNO_OVcYnsExg: TWideStringField;
    DBEdit9: TDBEdit;
    Label20: TLabel;
    dmkDBEdit7: TdmkDBEdit;
    dmkDBEdit8: TdmkDBEdit;
    Label6: TLabel;
    DBEdit4: TDBEdit;
    Label8: TLabel;
    DBEdit5: TDBEdit;
    QrOVgItxPrfCabReferencia: TWideStringField;
    Label3: TLabel;
    DBEdit1: TDBEdit;
    DBCheckBox1: TDBCheckBox;
    QrOVdReferencia: TMySQLQuery;
    QrOVdReferenciaCodigo: TIntegerField;
    QrOVdReferenciaNome: TWideStringField;
    DsOVdReferencia: TDataSource;
    QrOVdReferenciaReferencia: TWideStringField;
    QrOVcYnsExgTop: TMySQLQuery;
    QrOVcYnsExgTopNO_TolerBasCalc: TWideStringField;
    QrOVcYnsExgTopNO_TOPYKO: TWideStringField;
    QrOVcYnsExgTopNO_Mensuravl: TWideStringField;
    QrOVcYnsExgTopNO_TlrRndUsaMin: TWideStringField;
    QrOVcYnsExgTopNO_TlrRndUsaMax: TWideStringField;
    QrOVcYnsExgTopCodigo: TIntegerField;
    QrOVcYnsExgTopControle: TIntegerField;
    QrOVcYnsExgTopTopyko: TIntegerField;
    QrOVcYnsExgTopTolerBasCalc: TSmallintField;
    QrOVcYnsExgTopTolerSglUnMdid: TWideStringField;
    QrOVcYnsExgTopTlrRndUsaMin: TSmallintField;
    QrOVcYnsExgTopTlrRndPrcMin: TFloatField;
    QrOVcYnsExgTopTlrRndUsaMax: TSmallintField;
    QrOVcYnsExgTopTlrRndPrcMax: TFloatField;
    QrOVcYnsExgTopLk: TIntegerField;
    QrOVcYnsExgTopDataCad: TDateField;
    QrOVcYnsExgTopDataAlt: TDateField;
    QrOVcYnsExgTopUserCad: TIntegerField;
    QrOVcYnsExgTopUserAlt: TIntegerField;
    QrOVcYnsExgTopAlterWeb: TSmallintField;
    QrOVcYnsExgTopAWServerID: TIntegerField;
    QrOVcYnsExgTopAWStatSinc: TSmallintField;
    QrOVcYnsExgTopAtivo: TSmallintField;
    QrOVcYnsExgTopMedidCer: TFloatField;
    QrOVcYnsExgTopMedidMin: TFloatField;
    QrOVcYnsExgTopMedidMax: TFloatField;
    QrOVcYnsExgTopMensuravl: TSmallintField;
    DsOVcYnsExgTop: TDataSource;
    QrOVcYnsMixOpc: TMySQLQuery;
    QrOVcYnsMixOpcNO_Magnitude: TWideStringField;
    QrOVcYnsMixOpcCodigo: TIntegerField;
    QrOVcYnsMixOpcControle: TIntegerField;
    QrOVcYnsMixOpcNome: TWideStringField;
    QrOVcYnsMixOpcMagnitude: TIntegerField;
    QrOVcYnsMixOpcPontNeg: TIntegerField;
    QrOVcYnsMixOpcLk: TIntegerField;
    QrOVcYnsMixOpcDataCad: TDateField;
    QrOVcYnsMixOpcDataAlt: TDateField;
    QrOVcYnsMixOpcUserCad: TIntegerField;
    QrOVcYnsMixOpcUserAlt: TIntegerField;
    QrOVcYnsMixOpcAlterWeb: TSmallintField;
    QrOVcYnsMixOpcAWServerID: TIntegerField;
    QrOVcYnsMixOpcAWStatSinc: TSmallintField;
    QrOVcYnsMixOpcAtivo: TSmallintField;
    DsOVcYnsMixOpc: TDataSource;
    PCItens: TPageControl;
    TabSheet1: TTabSheet;
    PnGrids: TPanel;
    DBGMixOpc: TDBGrid;
    PnGrids2: TPanel;
    DBGExgTop: TDBGrid;
    DBRadioGroup1: TDBRadioGroup;
    QrOVgItxPrfCabPermiFinHow: TSmallintField;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrOVgItxPrfCabAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrOVgItxPrfCabBeforeOpen(DataSet: TDataSet);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure QrOVgItxPrfCabAfterScroll(DataSet: TDataSet);
    procedure CabInclui1Click(Sender: TObject);
    procedure CabAltera1Click(Sender: TObject);
    procedure CabExclui1Click(Sender: TObject);
    procedure BtCabClick(Sender: TObject);
    procedure BtItsClick(Sender: TObject);
    procedure PMCabPopup(Sender: TObject);
    procedure QrOVgItxPrfCabBeforeClose(DataSet: TDataSet);
    procedure QrOVcYnsExgTopAfterScroll(DataSet: TDataSet);
    procedure QrOVcYnsExgTopBeforeClose(DataSet: TDataSet);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    //
    procedure ReopenOVcYnsExgTop(Controle: Integer);
    procedure ReopenOVcYnsMixOpc(Controle: Integer);
  public
    { Public declarations }
    FSeq, FCabIni: Integer;
    FLocIni: Boolean;
    //
    procedure LocCod(Atual, Codigo: Integer);

  end;

var
  FmOVgItxPrfCab: TFmOVgItxPrfCab;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, MyDBCheck, DmkDAC_PF, UnOVS_Jan, UnOVS_PF,
  UnOVS_Consts, ModuleGeral, GerlShowGrid, PesqNomeOuReferencia;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmOVgItxPrfCab.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmOVgItxPrfCab.PMCabPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemCabUpd(CabAltera1, QrOVgItxPrfCab);
end;

procedure TFmOVgItxPrfCab.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrOVgItxPrfCabCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmOVgItxPrfCab.DefParams;
begin
  VAR_GOTOTABELA := 'ovgitxprfcab';
  VAR_GOTOMYSQLTABLE := QrOVgItxPrfCab;
  VAR_GOTONEG := gotoPiZ;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT igc.*, ymc.Nome NO_OVcYnsExg,');
  VAR_SQLx.Add('ref.Nome NO_Referencia, ref.Referencia ');
  VAR_SQLx.Add('FROM ovgitxprfcab igc');
  VAR_SQLx.Add('LEFT JOIN ovdreferencia ref ON ref.Codigo=igc.SeqGrupo ');
  VAR_SQLx.Add('LEFT JOIN ovcynsexgcad ymc ON ymc.Codigo=igc.OVcYnsExg');
  VAR_SQLx.Add('WHERE igc.Codigo > 0');
  //
  VAR_SQL1.Add('AND igc.Codigo=:P0');
  //
  //VAR_SQL2.Add('AND CodUsu=:P0');
  //
  VAR_SQLa.Add('AND igc.Nome Like :P0');
  //
end;

procedure TFmOVgItxPrfCab.CabExclui1Click(Sender: TObject);
begin
  Geral.MB_Info('A��o n�o implementada! Solicite � Dermatek:' + sLineBreak +
  Caption + sLineBreak + TMenuItem(Sender).Name);
end;

procedure TFmOVgItxPrfCab.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmOVgItxPrfCab.QueryPrincipalAfterOpen;
begin
end;

procedure TFmOVgItxPrfCab.ReopenOVcYnsExgTop(Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrOVcYnsExgTop, Dmod.MyDB, [
  'SELECT IF(ymt.Mensuravl=0,"", ELT(ymt.TolerBasCalc+1, "Medida", "Percentual")) NO_TolerBasCalc, ',
  'IF(ymt.Mensuravl=1, "SIM", "N�O") NO_Mensuravl,',
  'IF(ymt.TlrRndUsaMin=1, "SIM", "N�O") NO_TlrRndUsaMin,',
  'IF(ymt.TlrRndUsaMax=1, "SIM", "N�O") NO_TlrRndUsaMax,',
  'ygt.Nome NO_TOPYKO, ymt.* ',
  'FROM ovcynsexgtop ymt ',
  'LEFT JOIN ovcynsmixtop ygt ON ygt.Codigo=ymt.Topyko ',
  'WHERE ymt.Codigo=' + Geral.FF0(QrOVgItxPrfCabOVcYnsExg.Value),
  '']);
  //
  QrOVcYnsExgTop.Locate('Controle', Controle, []);
end;

procedure TFmOVgItxPrfCab.ReopenOVcYnsMixOpc(Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrOVcYnsMixOpc, Dmod.MyDB, [
  'SELECT mag.Nome NO_Magnitude, opc.*  ',
  'FROM ovcynsmixopc opc ',
  'LEFT JOIN ovcynsqstmag mag ON  mag.Codigo=opc.Magnitude ',
  'WHERE opc.Codigo=' + Geral.FF0(QrOVcYnsExgTopTopyko.Value),
  '']);
  //
  if Controle <> 0 then
    QrOVcYnsMixOpc.Locate('Controle', Controle, []);
end;

procedure TFmOVgItxPrfCab.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmOVgItxPrfCab.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmOVgItxPrfCab.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmOVgItxPrfCab.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmOVgItxPrfCab.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmOVgItxPrfCab.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmOVgItxPrfCab.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrOVgItxPrfCabCodigo.Value;
  Close;
end;

procedure TFmOVgItxPrfCab.CabAltera1Click(Sender: TObject);
begin
  OVS_Jan.MostraFormOVgItxPrfCad(stUpd, QrOVgItxPrfCabCodigo.Value,
    QrOVgItxPrfCabSeqGrupo.Value, QrOVgItxPrfCabNO_Referencia.Value,
    QrOVgItxPrfCabReferencia.Value, QrOVgItxPrfCabOVcYnsExg.Value,
    (*QrOVgItxPrfCabOVcYnsChk.Value, QrOVgItxPrfCabOVcYnsARQ.Value,
    QrOVgItxPrfCabLimiteMed.Value, QrOVgItxPrfCabLimiteChk.Value,*)
    QrOVgItxPrfCabPermiFinHow.Value, QrOVgItxPrfCabAtivo.Value,
    QrOVgItxPrfCabNome.Value);
  //
  LocCod(QrOVgItxPrfCabCodigo.Value, QrOVgItxPrfCabCodigo.Value);
end;

procedure TFmOVgItxPrfCab.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := EdCodigo.ValueVariant;
  if ImgTipo.SQLType = stIns then
    UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'ovgitxprfcab', Codigo);
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'ovgitxprfcab', 'Codigo');
end;

procedure TFmOVgItxPrfCab.BtItsClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMIts, BtIts);
end;

procedure TFmOVgItxPrfCab.BtCabClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMCab, BtCab);
end;

procedure TFmOVgItxPrfCab.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  GBEdita.Align   := alClient;
  DBGExgTop.Align := alClient;
  PnGrids2.Align  := alClient;
  PCItens.Align   := alClient;
  //
  PCItens.ActivePageIndex := 0;
  CriaOForm;
  FSeq := 0;
end;

procedure TFmOVgItxPrfCab.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrOVgItxPrfCabCodigo.Value, LaRegistro.Caption);
end;

procedure TFmOVgItxPrfCab.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmOVgItxPrfCab.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrOVgItxPrfCabCodigo.Value, LaRegistro.Caption);
end;

procedure TFmOVgItxPrfCab.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmOVgItxPrfCab.QrOVcYnsExgTopAfterScroll(DataSet: TDataSet);
begin
  ReopenOVcYnsMixOpc(0);
end;

procedure TFmOVgItxPrfCab.QrOVcYnsExgTopBeforeClose(DataSet: TDataSet);
begin
  QrOVcYnsMixOpc.Close;
end;

procedure TFmOVgItxPrfCab.QrOVgItxPrfCabAfterOpen(DataSet: TDataSet);
begin
  //Geral.MB_Info(QrOVgItxPrfCab.SQL.Text);
  QueryPrincipalAfterOpen;
end;

procedure TFmOVgItxPrfCab.QrOVgItxPrfCabAfterScroll(DataSet: TDataSet);
begin
  ReopenOVcYnsExgTop(0);
end;

procedure TFmOVgItxPrfCab.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //
  if FSeq = 1 then
    CabInclui1Click(Self)
  else
  if (not FLocIni) and (FCabIni <> 0)  then
  begin
    LocCod(FCabIni, FCabIni);
    if QrOVgItxPrfCabCodigo.Value <> FCabIni then
      Geral.MB_Aviso('Grupo n�o localizado!');
    FLocIni := True;
  end;
end;

procedure TFmOVgItxPrfCab.SbQueryClick(Sender: TObject);
var
  Codigo: Integer;
begin
  if OVS_Jan.LocalizaOVgItxPrfCab(Codigo) then
    LocCod(Codigo, Codigo);
end;

procedure TFmOVgItxPrfCab.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmOVgItxPrfCab.CabInclui1Click(Sender: TObject);
var
  Codigo, SeqGrupo, OVcYnsExg,
  //OVcYnsChk, OVcYnsARQ, LimiteMed, LimiteChk,
  PermiFinHow, Ativo: Integer;
  NO_Artigo, Referencia, Nome: String;
begin
  SeqGrupo := 0;
  MyObjects.CriaForm_AcessoTotal(TFmPesqNomeOuReferencia, FmPesqNomeOuReferencia);
  FmPesqNomeOuReferencia.FNomeTabela  := 'ovdreferencia';
  FmPesqNomeOuReferencia.FNomeFldNome := 'Nome';
  FmPesqNomeOuReferencia.FNomeFldRefe := 'Referencia';
  FmPesqNomeOuReferencia.FNomeFldCodi := 'Codigo';
  FmPesqNomeOuReferencia.ShowModal;
  if FmPesqNomeOuReferencia.FSelecionou then
  begin
    SeqGrupo   := FmPesqNomeOuReferencia.FValorCodi;
    NO_Artigo  := FmPesqNomeOuReferencia.FValorNome;
    Referencia := FmPesqNomeOuReferencia.FValorRefe;
  end;
  FmPesqNomeOuReferencia.Destroy;
  //
  if SeqGrupo <> 0 then
  begin
    Codigo        := 0;
    OVcYnsExg     := 0;
(*
    OVcYnsChk     := 0;
    OVcYnsARQ     := 0;
    LimiteMed     := 0;
    LimiteChk     := 0;
*)
    PermiFinHow   := 0;
    Ativo         := 0;
    Nome          := '';
    //
    OVS_Jan.MostraFormOVgItxPrfCad(stIns, Codigo, SeqGrupo, NO_Artigo,
    Referencia, OVcYnsExg, (*OVcYnsChk, OVcYnsARQ, LimiteMed, LimiteChk,*)
    PermiFinHow, Ativo, Nome);
  end;
end;

procedure TFmOVgItxPrfCab.QrOVgItxPrfCabBeforeClose(
  DataSet: TDataSet);
begin
  QrOVcYnsExgTop.Close;
end;

procedure TFmOVgItxPrfCab.QrOVgItxPrfCabBeforeOpen(DataSet: TDataSet);
begin
  QrOVgItxPrfCabCodigo.DisplayFormat := FFormatFloat;
end;

end.

