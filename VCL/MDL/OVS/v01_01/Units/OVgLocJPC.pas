unit OVgLocJPC;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkPermissoes,
  dmkDBLookupComboBox, dmkEditCB, mySQLDbTables, dmkDBGridZTO;

type
  TFmOVgLocJPC = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    GroupBox2: TGroupBox;
    Panel5: TPanel;
    Label1: TLabel;
    EdArtigo: TdmkEditCB;
    CBArtigo: TdmkDBLookupComboBox;
    QrOVdReferencia: TMySQLQuery;
    DsOVdReferencia: TDataSource;
    EdReferencia: TdmkEdit;
    Label2: TLabel;
    QrOVdReferenciaCodigo: TIntegerField;
    QrOVdReferenciaNome: TWideStringField;
    QrOVgItxPrfCab: TMySQLQuery;
    QrOVgItxPrfCabCodigo: TIntegerField;
    QrOVgItxPrfCabNome: TWideStringField;
    QrOVgItxPrfCabSeqGrupo: TIntegerField;
    QrOVgItxPrfCabOVcYnsExg: TIntegerField;
    QrOVgItxPrfCabAtivo: TSmallintField;
    QrOVgItxPrfCabNO_Referencia: TWideStringField;
    QrOVgItxPrfCabNO_OVcYnsExg: TWideStringField;
    QrOVgItxPrfCabPermiFinHow: TIntegerField;
    DsOVgItxPrfCab: TDataSource;
    DBGrid1: TdmkDBGridZTO;
    QrOVgItxPrfCabReferencia: TWideStringField;
    RGAtivo: TRadioGroup;
    QrOVgItxPrfCabNO_Ativo: TWideStringField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure EdArtigoChange(Sender: TObject);
    procedure EdArtigoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdReferenciaChange(Sender: TObject);
    procedure EdReferenciaExit(Sender: TObject);
    procedure EdReferenciaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBGrid1DblClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    FCodigo: Integer;
  end;

  var
  FmOVgLocJPC: TFmOVgLocJPC;

implementation

uses UnMyObjects, Module, UnMySQLCuringa, DmkDAC_PF, ModOVS;

{$R *.DFM}

procedure TFmOVgLocJPC.BtOKClick(Sender: TObject);
var
  Artigo, Ativo: Integer;
  SQL_Artigo, SQL_Ativo: String;
begin
  Artigo           := EdArtigo.ValueVariant;
  Ativo            := RGAtivo.ItemIndex;
  //
  SQL_Artigo       := '';
  SQL_Ativo        := '';
  //
  if Artigo <> 0 then
    SQL_Artigo := 'AND igc.SeqGrupo=' + Geral.FF0(Artigo);
  if Ativo <> 2 then
    SQL_Ativo := 'AND igc.Ativo=' + Geral.FF0(Ativo);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrOVgItxPrfCab, Dmod.MyDB, [
  'SELECT igc.*, ymc.Nome NO_OVcYnsExg,  ',
  'dlo.Nome NO_Local, ref.Nome NO_Referencia,  ',
  'isc.Nome NO_ZtatusIsp, ',
  'IF(igc.DtHrAbert  <= "1899-12-30", "",   ',
  '  DATE_FORMAT(igc.DtHrAbert, "%d/%m/%Y %H:%i:%s")) DtHrAbert_TXT,   ',
  'IF(igc.DtHrFecha  <= "1899-12-30", "",   ',
  '  DATE_FORMAT(igc.DtHrFecha, "%d/%m/%Y %H:%i:%s")) DtHrFecha_TXT, ',
  'IF(Ativo=0, "Sim", "N�o") NO_Ativo ',
  'FROM ovgitxprfcab igc ',
  'LEFT JOIN ovdlocal dlo ON dlo.Codigo=igc.Local   ',
  'LEFT JOIN ovdreferencia ref ON ref.Codigo=igc.SeqGrupo  ',
  'LEFT JOIN ovcynsexgcad ymc ON ymc.Codigo=igc.OVcYnsExg ',
  'LEFT JOIN OVgIspStaCad isc ON isc.Codigo=igc.ZtatusIsp ',
  'WHERE igc.Codigo > 0 ',
  SQL_Artigo,
  SQL_Ativo,
  EmptyStr]);
end;

procedure TFmOVgLocJPC.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmOVgLocJPC.DBGrid1DblClick(Sender: TObject);
begin
  if (QrOVgItxPrfCab.State <> dsInactive) and (QrOVgItxPrfCab.RecordCount > 0) then
  begin
    FCodigo := QrOVgItxPrfCabCodigo.Value;
    Close;
  end;

end;

procedure TFmOVgLocJPC.EdArtigoChange(Sender: TObject);
begin
  if not EdReferencia.Focused then
    DmOVS.PesquisaPorCodigo(EdArtigo.ValueVariant, EdReferencia);
end;

procedure TFmOVgLocJPC.EdArtigoKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key=VK_F3 then
    CuringaLoc.Pesquisa('Codigo', 'Nome', 'Referencia', Dmod.MyDB,
    ''(*Extra*), EdArtigo, CBArtigo, dmktfInteger);
end;

procedure TFmOVgLocJPC.EdReferenciaChange(Sender: TObject);
begin
  if EdReferencia.Focused then
    DmOVS.PesquisaPorReferencia(False, EdReferencia, EdArtigo, CBArtigo);
end;

procedure TFmOVgLocJPC.EdReferenciaExit(Sender: TObject);
begin
  DmOVS.PesquisaPorReferencia(True, EdReferencia, EdArtigo, CBArtigo);
end;

procedure TFmOVgLocJPC.EdReferenciaKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key=VK_F3 then
    CuringaLoc.Pesquisa('Codigo', 'Nome', 'Referencia', Dmod.MyDB,
    ''(*Extra*), EdArtigo, CBArtigo, dmktfInteger)
end;

procedure TFmOVgLocJPC.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmOVgLocJPC.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  FCodigo := 0;
  UnDmkDAC_PF.AbreQuery(QrOVdReferencia, Dmod.MyDB);
end;

procedure TFmOVgLocJPC.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

end.
