unit OVcYnsExgCad;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2, dmkGeral,
  ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt, UMySQLModule, Menus,
  UnInternalConsts2, mySQLDbTables, UnMySQLCuringa, dmkPermissoes, dmkEdit,
  dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup, Grids, DBGrids,
  UnDmkProcFunc, UnDmkENums, dmkDBLookupComboBox, dmkEditCB, Variants,
  mySQLDirectQuery, Vcl.ComCtrls;

type
  TFmOVcYnsExgCad = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    Label9: TLabel;
    EdNome: TdmkEdit;
    Label1: TLabel;
    DBEdCodigo: TdmkDBEdit;
    Label2: TLabel;
    DBEdNome: TdmkDBEdit;
    QrOVcYnsExgCad: TMySQLQuery;
    DsOVcYnsExgCad: TDataSource;
    QrOVcYnsExgTop: TMySQLQuery;
    DsOVcYnsExgTop: TDataSource;
    PMTop: TPopupMenu;
    ItsInclui1: TMenuItem;
    ItsExclui1: TMenuItem;
    ItsAltera1: TMenuItem;
    PMTab: TPopupMenu;
    CabInclui1: TMenuItem;
    CabAltera1: TMenuItem;
    CabExclui1: TMenuItem;
    BtTab: TBitBtn;
    BtTop: TBitBtn;
    QrEntidades: TMySQLQuery;
    DsEntidades: TDataSource;
    QrOVdReferencia: TMySQLQuery;
    DsOVdReferencia: TDataSource;
    QrEntidadesCodigo: TIntegerField;
    QrOVdReferenciaCodigo: TIntegerField;
    QrOVdReferenciaNome: TWideStringField;
    Label3: TLabel;
    EdArtigRef: TdmkEditCB;
    CBArtigRef: TdmkDBLookupComboBox;
    Label4: TLabel;
    EdClientRef: TdmkEditCB;
    CBClientRef: TdmkDBLookupComboBox;
    SbArtigRef: TSpeedButton;
    SbClientRef: TSpeedButton;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    DBEdit4: TDBEdit;
    QrEntidadesNO_ENT: TWideStringField;
    Label5: TLabel;
    Label6: TLabel;
    PnGrids: TPanel;
    DBGExgTop: TDBGrid;
    QrGT: TMySQLQuery;
    QrTop: TMySQLQuery;
    QrTopCodigo: TIntegerField;
    QrTopControle: TIntegerField;
    QrGTCodGrade: TIntegerField;
    QrGTCodTam: TWideStringField;
    DqAux: TMySQLDirectQuery;
    PB1: TProgressBar;
    EdPontosCrit: TdmkEdit;
    Label8: TLabel;
    DBEdit5: TDBEdit;
    Label10: TLabel;
    QrOVcYnsExgCadNO_ClientRef: TWideStringField;
    QrOVcYnsExgCadNO_ArtigRef: TWideStringField;
    QrOVcYnsExgCadCodigo: TIntegerField;
    QrOVcYnsExgCadNome: TWideStringField;
    QrOVcYnsExgCadArtigRef: TIntegerField;
    QrOVcYnsExgCadClientRef: TIntegerField;
    QrOVcYnsExgCadPontosCrit: TIntegerField;
    QrOVcYnsExgCadPontosRsv: TIntegerField;
    QrOVcYnsExgCadLk: TIntegerField;
    QrOVcYnsExgCadDataCad: TDateField;
    QrOVcYnsExgCadDataAlt: TDateField;
    QrOVcYnsExgCadUserCad: TIntegerField;
    QrOVcYnsExgCadUserAlt: TIntegerField;
    QrOVcYnsExgCadAlterWeb: TSmallintField;
    QrOVcYnsExgCadAWServerID: TIntegerField;
    QrOVcYnsExgCadAWStatSinc: TSmallintField;
    QrOVcYnsExgCadAtivo: TSmallintField;
    DBEdit6: TDBEdit;
    Label11: TLabel;
    QrOVcYnsExgTopNO_TolerBasCalc: TWideStringField;
    QrOVcYnsExgTopNO_TOPYKO: TWideStringField;
    QrOVcYnsExgTopCodigo: TIntegerField;
    QrOVcYnsExgTopControle: TIntegerField;
    QrOVcYnsExgTopTopyko: TIntegerField;
    QrOVcYnsExgTopTolerBasCalc: TSmallintField;
    QrOVcYnsExgTopTolerSglUnMdid: TWideStringField;
    QrOVcYnsExgTopTlrRndUsaMin: TSmallintField;
    QrOVcYnsExgTopTlrRndPrcMin: TFloatField;
    QrOVcYnsExgTopTlrRndUsaMax: TSmallintField;
    QrOVcYnsExgTopTlrRndPrcMax: TFloatField;
    QrOVcYnsExgTopLk: TIntegerField;
    QrOVcYnsExgTopDataCad: TDateField;
    QrOVcYnsExgTopDataAlt: TDateField;
    QrOVcYnsExgTopUserCad: TIntegerField;
    QrOVcYnsExgTopUserAlt: TIntegerField;
    QrOVcYnsExgTopAlterWeb: TSmallintField;
    QrOVcYnsExgTopAWServerID: TIntegerField;
    QrOVcYnsExgTopAWStatSinc: TSmallintField;
    QrOVcYnsExgTopAtivo: TSmallintField;
    QrOVcYnsExgTopMedidCer: TFloatField;
    QrOVcYnsExgTopMedidMin: TFloatField;
    QrOVcYnsExgTopMedidMax: TFloatField;
    EdPontosRsv: TdmkEdit;
    Label12: TLabel;
    QrOVcYnsExgTopMensuravl: TSmallintField;
    DBGMixOpc: TDBGrid;
    QrOVcYnsMixOpc: TMySQLQuery;
    QrOVcYnsMixOpcNO_Magnitude: TWideStringField;
    QrOVcYnsMixOpcCodigo: TIntegerField;
    QrOVcYnsMixOpcControle: TIntegerField;
    QrOVcYnsMixOpcNome: TWideStringField;
    QrOVcYnsMixOpcMagnitude: TIntegerField;
    QrOVcYnsMixOpcPontNeg: TIntegerField;
    QrOVcYnsMixOpcLk: TIntegerField;
    QrOVcYnsMixOpcDataCad: TDateField;
    QrOVcYnsMixOpcDataAlt: TDateField;
    QrOVcYnsMixOpcUserCad: TIntegerField;
    QrOVcYnsMixOpcUserAlt: TIntegerField;
    QrOVcYnsMixOpcAlterWeb: TSmallintField;
    QrOVcYnsMixOpcAWServerID: TIntegerField;
    QrOVcYnsMixOpcAWStatSinc: TSmallintField;
    QrOVcYnsMixOpcAtivo: TSmallintField;
    DsOVcYnsMixOpc: TDataSource;
    QrOVcYnsExgTopNO_Mensuravl: TWideStringField;
    QrOVcYnsExgTopNO_TlrRndUsaMin: TWideStringField;
    QrOVcYnsExgTopNO_TlrRndUsaMax: TWideStringField;
    Label13: TLabel;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrOVcYnsExgCadAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrOVcYnsExgCadBeforeOpen(DataSet: TDataSet);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure QrOVcYnsExgCadAfterScroll(DataSet: TDataSet);
    procedure CabInclui1Click(Sender: TObject);
    procedure CabAltera1Click(Sender: TObject);
    procedure CabExclui1Click(Sender: TObject);
    procedure BtTabClick(Sender: TObject);
    procedure BtTopClick(Sender: TObject);
    procedure ItsInclui1Click(Sender: TObject);
    procedure ItsExclui1Click(Sender: TObject);
    procedure ItsAltera1Click(Sender: TObject);
    procedure PMTabPopup(Sender: TObject);
    procedure PMTopPopup(Sender: TObject);
    procedure QrOVcYnsExgCadBeforeClose(DataSet: TDataSet);
    procedure SbClientRefClick(Sender: TObject);
    procedure SbArtigRefClick(Sender: TObject);
    procedure PMGraPopup(Sender: TObject);
    procedure EdCodigoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdNomeKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure QrOVcYnsExgTopAfterScroll(DataSet: TDataSet);
    procedure QrOVcYnsExgTopBeforeClose(DataSet: TDataSet);
  private
    FRegCadSel, FRegistrosAdicionados: Integer;
    //
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure LocCod(Atual, Codigo: Integer);
    procedure Va(Para: TVaiPara);
    //
    procedure CopiaNomeArtigo();
    procedure MostraFormOVcYnsExgTop(SQLType: TSQLType);
    procedure ReopenOVcYnsMixOpc(Controle: Integer);

  public
    { Public declarations }
    FSeq, FCabIni, FArtigo: Integer;
    FLocIni: Boolean;
    //
    procedure ReopenOVcYnsExgTop(Controle: Integer);
    procedure ReopenEntidades();
    procedure ReopenOVdReferencia();

  end;

var
  FmOVcYnsExgCad: TFmOVcYnsExgCad;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, MyDBCheck, DmkDAC_PF, UnOVS_Jan, UMySQLDB,
  UnEntities,
  OVcYnsExgTop(*, OVcYnsExgDimAdd, OVcYnsExgDimFil, OVcYnsExgDimIts*);

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmOVcYnsExgCad.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmOVcYnsExgCad.MostraFormOVcYnsExgTop(SQLType: TSQLType);
begin
  if DBCheck.CriaFm(TFmOVcYnsExgTop, FmOVcYnsExgTop, afmoNegarComAviso) then
  begin
    FmOVcYnsExgTop.ImgTipo.SQLType := SQLType;
    FmOVcYnsExgTop.FDsCad := DsOVcYnsExgCad;
    FmOVcYnsExgTop.FDsIts := DsOVcYnsExgTop;
    FmOVcYnsExgTop.FQrIts := QrOVcYnsExgTop;
    //
    if SQLType = stIns then
    begin
      //FmOVcYnsExgTop.EdCPF1.ReadOnly := False
    end else
    begin
      //
      FmOVcYnsExgTop.EdControle.ValueVariant       := QrOVcYnsExgTopControle.Value;
      //
      FmOVcYnsExgTop.EdTopyko.ValueVariant         := QrOVcYnsExgTopTopyko.Value;
      FmOVcYnsExgTop.CBTopyko.KeyValue             := QrOVcYnsExgTopTopyko.Value;
      //
      FmOVcYnsExgTop.CkMensuravl.Checked           := QrOVcYnsExgTopMensuravl.Value = 1;
      //  Devem ser os pen�ltimos? Na ordem abaixo?
      FmOVcYnsExgTop.RGTolerBasCalc.ItemIndex      := QrOVcYnsExgTopTolerBasCalc.Value;
      FmOVcYnsExgTop.EdTolerSglUnMdid.ValueVariant := QrOVcYnsExgTopTolerSglUnMdid.Value;
      FmOVcYnsExgTop.CkTlrRndUsaMin.Checked        := QrOVcYnsExgTopTlrRndUsaMin.Value = 1;
      FmOVcYnsExgTop.EdTlrRndPrcMin.ValueVariant   := QrOVcYnsExgTopTlrRndPrcMin.Value;
      FmOVcYnsExgTop.CkTlrRndUsaMax.Checked        := QrOVcYnsExgTopTlrRndUsaMax.Value = 1;
      FmOVcYnsExgTop.EdTlrRndPrcMax.ValueVariant   := QrOVcYnsExgTopTlrRndPrcMax.Value;
      //  Devem ser os �ltimos? Na ordem abaixo?
      FmOVcYnsExgTop.EdMedidCer.ValueVariant       := QrOVcYnsExgTopMedidCer.Value;
      FmOVcYnsExgTop.EdMedidMin.ValueVariant       := QrOVcYnsExgTopMedidMin.Value;
      FmOVcYnsExgTop.EdMedidMax.ValueVariant       := QrOVcYnsExgTopMedidMax.Value;
    end;
    FmOVcYnsExgTop.ShowModal;
    FmOVcYnsExgTop.Destroy;
  end;
end;

procedure TFmOVcYnsExgCad.PMTabPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemCabUpd(CabAltera1, QrOVcYnsExgCad);
  MyObjects.HabilitaMenuItemCabDel(CabExclui1, QrOVcYnsExgCad, QrOVcYnsExgTop);
end;

procedure TFmOVcYnsExgCad.PMTopPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(ItsInclui1, QrOVcYnsExgCad);
  MyObjects.HabilitaMenuItemItsUpd(ItsAltera1, QrOVcYnsExgTop);
  MyObjects.HabilitaMenuItemItsUpd(ItsExclui1, QrOVcYnsExgTop); //
end;

procedure TFmOVcYnsExgCad.PMGraPopup(Sender: TObject);
begin
{
  MyObjects.HabilitaMenuItemItsIns(AdicionaTamanhos1, QrOVcYnsExgTop);
  //MyObjects.HabilitaMenuItemItsUpd(AlteraTamanho1, QrOVcYnsExgDim);
  MyObjects.HabilitaMenuItemItsDel(RemoveTamanho1, QrOVcYnsExgDim);
}
end;

procedure TFmOVcYnsExgCad.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrOVcYnsExgCadCodigo.Value, LaRegistro.Caption[2]);
end;

procedure TFmOVcYnsExgCad.DefParams;
begin
  VAR_GOTOTABELA := 'ovcynsexgcad';
  VAR_GOTOMYSQLTABLE := QrOVcYnsExgCad;
  VAR_GOTONEG := gotoPiZ;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_ClientRef, ');
  VAR_SQLx.Add('art.Nome NO_ArtigRef, ycc.* ');
  VAR_SQLx.Add('FROM ovcynsexgcad ycc');
  VAR_SQLx.Add('LEFT JOIN ovdreferencia art ON art.Codigo=ycc.ArtigRef ');
  VAR_SQLx.Add('LEFT JOIN entidades ent ON ent.Codigo=ycc.ClientRef ');
  //
  VAR_SQL1.Add('WHERE ycc.Codigo>0');
  VAR_SQL1.Add('AND ycc.Codigo=:P0');
  //
  //VAR_SQL2.Add('AND CodUsu=:P0');
  //
  VAR_SQLa.Add('AND ycc.Nome Like :P0');
  //
end;

procedure TFmOVcYnsExgCad.EdCodigoKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F4 then
    CopiaNomeArtigo();
end;

procedure TFmOVcYnsExgCad.EdNomeKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F4 then
    CopiaNomeArtigo();
end;

procedure TFmOVcYnsExgCad.ItsAltera1Click(Sender: TObject);
begin
  MostraFormOVcYnsExgTop(stUpd);
end;

procedure TFmOVcYnsExgCad.CabExclui1Click(Sender: TObject);
begin
  Geral.MB_Info('A��o n�o implementada! Solicite � Dermatek:' + sLineBreak +
  Caption + sLineBreak + TMenuItem(Sender).Name);
end;

procedure TFmOVcYnsExgCad.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmOVcYnsExgCad.QueryPrincipalAfterOpen;
begin
end;

procedure TFmOVcYnsExgCad.ItsExclui1Click(Sender: TObject);
var
  Controle: Integer;
  Pergunta: String;
begin
  Pergunta := 'Deseja remover o T�pico "' + QrOVcYnsExgTopNO_TOPYKO.Value +
    '" e seus tamanhos?';
  //
  if Geral.MB_Pergunta(Pergunta) = ID_YES then
  begin
    //
    UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
    'DELETE FROM ovcynsexgtop  ',
    'WHERE Controle=' + Geral.FF0(QrOVcYnsExgTopControle.Value),
    '']);
    //
    Controle := GOTOy.LocalizaPriorNextIntQr(QrOVcYnsExgTop,
      QrOVcYnsExgTopControle, QrOVcYnsExgTopControle.Value);
    ReopenOVcYnsExgTop(Controle);
  end;
end;

procedure TFmOVcYnsExgCad.ReopenEntidades();
begin
  UnDMkDAC_PF.AbreQuery(QrEntidades, DMod.MyDB);
end;

procedure TFmOVcYnsExgCad.ReopenOVdReferencia;
begin
  UnDMkDAC_PF.AbreQuery(QrOVdReferencia, DMod.MyDB);
end;

procedure TFmOVcYnsExgCad.ReopenOVcYnsExgTop(Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrOVcYnsExgTop, Dmod.MyDB, [
  'SELECT IF(ymt.Mensuravl=0,"", ELT(ymt.TolerBasCalc+1, "Medida", "Percentual")) NO_TolerBasCalc, ',
  'IF(ymt.Mensuravl=1, "SIM", "N�O") NO_Mensuravl,',
  'IF(ymt.TlrRndUsaMin=1, "SIM", "N�O") NO_TlrRndUsaMin,',
  'IF(ymt.TlrRndUsaMax=1, "SIM", "N�O") NO_TlrRndUsaMax,',
  'ygt.Nome NO_TOPYKO, ymt.* ',
  'FROM ovcynsexgtop ymt ',
  'LEFT JOIN ovcynsmixtop ygt ON ygt.Codigo=ymt.Topyko ',
  'WHERE ymt.Codigo=' + Geral.FF0(QrOVcYnsExgCadCodigo.Value),
  '']);
  //
  QrOVcYnsExgTop.Locate('Controle', Controle, []);
end;

procedure TFmOVcYnsExgCad.ReopenOVcYnsMixOpc(Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrOVcYnsMixOpc, Dmod.MyDB, [
  'SELECT mag.Nome NO_Magnitude, opc.*  ',
  'FROM ovcynsmixopc opc ',
  'LEFT JOIN ovcynsqstmag mag ON  mag.Codigo=opc.Magnitude ',
  'WHERE opc.Codigo=' + Geral.FF0(QrOVcYnsExgTopTopyko.Value),
  '']);
  //
  if Controle <> 0 then
    QrOVcYnsMixOpc.Locate('Controle', Controle, []);
end;

procedure TFmOVcYnsExgCad.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmOVcYnsExgCad.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmOVcYnsExgCad.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmOVcYnsExgCad.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmOVcYnsExgCad.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmOVcYnsExgCad.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmOVcYnsExgCad.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrOVcYnsExgCadCodigo.Value;
  Close;
end;

procedure TFmOVcYnsExgCad.ItsInclui1Click(Sender: TObject);
begin
  MostraFormOVcYnsExgTop(stIns);
end;

procedure TFmOVcYnsExgCad.CabAltera1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrOVcYnsExgCad, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'ovcynsexgcad');
end;

procedure TFmOVcYnsExgCad.BtConfirmaClick(Sender: TObject);
var
  Nome: String;
  Codigo, ArtigRef, ClientRef, PontosCrit, PontosRsv: Integer;
  SQLType: TSQLType;
begin
  SQLType        := ImgTipo.SQLType;
  Codigo         := EdCodigo.ValueVariant;
  Nome           := EdNome.ValueVariant;
  ArtigRef       := EdArtigRef.ValueVariant;
  ClientRef      := EdClientRef.ValueVariant;
  PontosCrit     := EdPontosCrit.ValueVariant;
  PontosRsv      := EdPontosRsv.ValueVariant;
  //
  if MyObjects.FIC(Length(Nome) = 0, EdNome, 'Defina uma descri��o!') then Exit;
  if MyObjects.FIC(EdPontosRsv.ValueVariant = 0, EdPontosRsv,
    'ATEN��O: os pontos (resalva) n�o foram informados!') then (*Exit*);
  if MyObjects.FIC(EdPontosCrit.ValueVariant = 0, EdPontosCrit,
    'ATEN��O: os pontos (cr�tico) n�o foram informados!') then (*Exit*);
  //
  Codigo := UMyMod.BPGS1I32('ovcynsexgcad', 'Codigo', '', '', tsPos, SQLType, Codigo);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'ovcynsexgcad', False, [
  'Nome', 'ArtigRef', 'ClientRef',
  'PontosCrit', 'PontosRsv'], [
  'Codigo'], [
  Nome, ArtigRef, ClientRef,
  PontosCrit, PontosRsv], [
  Codigo], True) then
  begin
    ImgTipo.SQLType := stLok;
    PnDados.Visible := True;
    PnEdita.Visible := False;
    GOTOy.BotoesSb(ImgTipo.SQLType);
    //
    LocCod(Codigo, Codigo);
    if FSeq = 1 then Close;
  end;
end;

procedure TFmOVcYnsExgCad.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := EdCodigo.ValueVariant;
  if ImgTipo.SQLType = stIns then
    UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'ovcynsexgcad', Codigo);
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'ovcynsexgcad', 'Codigo');
end;

procedure TFmOVcYnsExgCad.BtTopClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMTop, BtTop);
end;

procedure TFmOVcYnsExgCad.BtTabClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMTab, BtTab);
end;

procedure TFmOVcYnsExgCad.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  GBEdita.Align := alClient;
  PnGrids.Align := alClient;
  CriaOForm;
  FSeq := 0;
  FRegCadSel := 0;
  FRegistrosAdicionados := 0;
  QrOVcYnsExgCad.Database := Dmod.MyDB;
  ReopenOVdReferencia();
  ReopenEntidades();
end;

procedure TFmOVcYnsExgCad.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrOVcYnsExgCadCodigo.Value, LaRegistro.Caption);
end;

procedure TFmOVcYnsExgCad.SbArtigRefClick(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  OVS_Jan.MostraFormOVdReferencia();
  if VAR_CADASTRO <> 0 then
  begin
      UMyMod.SetaCodigoPesquisado(EdArtigRef, CBArtigRef, QrOVdReferencia, VAR_CADASTRO);
      EdArtigRef.SetFocus;
  end else
    ReopenOVdReferencia();
end;

procedure TFmOVcYnsExgCad.SbClientRefClick(Sender: TObject);
begin
  Entities.CadastroDeEntidade(0, fmCadSelecionar, fmCadSelecionar, False);
  VAR_CADASTRO := 0;
  if VAR_CADASTRO <> 0 then
  begin
      UMyMod.SetaCodigoPesquisado(EdClientRef, CBClientRef, QrENtidades, VAR_CADASTRO);
      EdClientRef.SetFocus;
  end else
    ReopenEntidades();
end;

procedure TFmOVcYnsExgCad.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmOVcYnsExgCad.SbNovoClick(Sender: TObject);
begin
  // LStyleService.GetSystemColor(clHighlightText);
  LaTitulo1C.Font.Color := MyObjects.StyleServiceObtemCor(clBtnFace);
  LaTitulo1B.Font.Color := MyObjects.StyleServiceObtemCor(clHighlightText);
  LaTitulo1A.Font.Color := MyObjects.StyleServiceObtemCor(clHighlight);
  //EXIT;
  LaRegistro.Caption := GOTOy.CodUsu(QrOVcYnsExgCadCodigo.Value, LaRegistro.Caption);
end;

procedure TFmOVcYnsExgCad.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmOVcYnsExgCad.QrOVcYnsExgCadAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmOVcYnsExgCad.QrOVcYnsExgCadAfterScroll(DataSet: TDataSet);
begin
  if FRegCadSel <> QrOVcYnsExgCadCodigo.Value then
    FRegistrosAdicionados := 0;
  FRegCadSel := QrOVcYnsExgCadCodigo.Value;
  //
  ReopenOVcYnsExgTop(0);
end;

procedure TFmOVcYnsExgCad.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //
  if FSeq = 1 then
    CabInclui1Click(Self)
  else
  if (not FLocIni) and (FCabIni <> 0)  then
  begin
    LocCod(FCabIni, FCabIni);
    if QrOVcYnsExgCadCodigo.Value <> FCabIni then
      Geral.MB_Aviso('Grupo n�o localizado!');
    FLocIni := True;
  end;
end;

procedure TFmOVcYnsExgCad.SbQueryClick(Sender: TObject);
begin
  LocCod(QrOVcYnsExgCadCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'ovcynsexgcad', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmOVcYnsExgCad.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmOVcYnsExgCad.CabInclui1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrOVcYnsExgCad, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'ovcynsexgcad');
  EdPontosCrit.ValueVariant := 4;
  EdPontosRsv.ValueVariant  := 1;
  EdArtigRef.ValueVariant   := FArtigo;
  CBArtigRef.KeyValue       := FArtigo;
end;

procedure TFmOVcYnsExgCad.CopiaNomeArtigo();
begin
  EdNome.ValueVariant := CBArtigRef.Text;
end;

procedure TFmOVcYnsExgCad.QrOVcYnsExgCadBeforeClose(
  DataSet: TDataSet);
begin
  QrOVcYnsExgTop.Close;
end;

procedure TFmOVcYnsExgCad.QrOVcYnsExgCadBeforeOpen(DataSet: TDataSet);
begin
  QrOVcYnsExgCadCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmOVcYnsExgCad.QrOVcYnsExgTopAfterScroll(DataSet: TDataSet);
begin
  ReopenOVcYnsMixOpc(0);
end;

procedure TFmOVcYnsExgCad.QrOVcYnsExgTopBeforeClose(DataSet: TDataSet);
begin
  QrOVcYnsMixOpc.Close;
end;

end.

