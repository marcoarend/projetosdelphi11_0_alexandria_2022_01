unit UnApp_Jan;

interface

uses
  Windows, SysUtils, Classes, DB, Menus, UnDmkEnums, mySQLDbTables, Forms,
  Grids, DBGrids, UnInternalConsts;

type
  TUnApp_Jan = class(TObject)
  private
    { Private declarations }

  public
    { Public declarations }
    procedure MostraFormOpcoesApp();
    procedure MostraFormOpcoesGrl();
  end;

var
  App_Jan: TUnApp_Jan;


implementation

uses
  MyDBCheck, Module, OpcoesApp, Opcoes;

{ TUnApp_Jan }

procedure TUnApp_Jan.MostraFormOpcoesApp();
begin
  if DBCheck.CriaFm(TFmOpcoesApp, FmOpcoesApp, afmoSoBoss) then
  begin
    FmOpcoesApp.ShowModal;
    FmOpcoesApp.Destroy;
  end;
end;

procedure TUnApp_Jan.MostraFormOpcoesGrl;
begin
{
  precisa ver o financeiro! contas e carteiras, ...
  if DBCheck.CriaFm(TFmOpcoes, FmOpcoes, afmoSoBoss) then
  begin
    FmOpcoes.ShowModal;
    FmOpcoes.Destroy;
  end;
}
end;

end.
