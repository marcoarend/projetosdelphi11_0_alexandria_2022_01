object FmOVcYnsMedDimAdd: TFmOVcYnsMedDimAdd
  Left = 339
  Top = 185
  Caption = 
    'OVS-TAMAN-004 :: Sele'#231#227'o de Tamanhos de T'#243'pico de Tabela de Medi' +
    'das'
  ClientHeight = 455
  ClientWidth = 784
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 319
    Width = 784
    Height = 29
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 2
    object CkContinuar: TCheckBox
      Left = 16
      Top = 4
      Width = 117
      Height = 17
      Caption = 'Continuar inserindo.'
      Checked = True
      State = cbChecked
      TabOrder = 0
    end
  end
  object GroupBox1: TGroupBox
    Left = 0
    Top = 48
    Width = 784
    Height = 65
    Align = alTop
    Caption = ' Dados do cabe'#231'alho:'
    Enabled = False
    TabOrder = 0
    object Label5: TLabel
      Left = 12
      Top = 20
      Width = 14
      Height = 13
      Caption = 'ID:'
      FocusControl = DBEdCodigo
    end
    object Label3: TLabel
      Left = 72
      Top = 20
      Width = 98
      Height = 13
      Caption = 'Descri'#231#227'o da tabela:'
      FocusControl = DBEdNome
    end
    object DBEdCodigo: TdmkDBEdit
      Left = 12
      Top = 36
      Width = 56
      Height = 21
      TabStop = False
      DataField = 'Codigo'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ReadOnly = True
      ShowHint = True
      TabOrder = 0
      UpdCampo = 'Codigo'
      UpdType = utYes
      Alignment = taRightJustify
    end
    object DBEdNome: TDBEdit
      Left = 72
      Top = 36
      Width = 701
      Height = 21
      TabStop = False
      Color = clWhite
      DataField = 'Nome'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
    end
  end
  object GroupBox2: TGroupBox
    Left = 0
    Top = 113
    Width = 784
    Height = 64
    Align = alTop
    Caption = ' Dados do item: '
    TabOrder = 1
    object Label1: TLabel
      Left = 12
      Top = 16
      Width = 35
      Height = 13
      Caption = 'Grade: '
    end
    object SbTopico: TSpeedButton
      Left = 752
      Top = 32
      Width = 21
      Height = 21
      Caption = '...'
      OnClick = SbTopicoClick
    end
    object CBGrade: TdmkDBLookupComboBox
      Left = 68
      Top = 32
      Width = 681
      Height = 21
      KeyField = 'Codigo'
      ListField = 'Nome'
      ListSource = DsOVdGradeCad
      TabOrder = 1
      dmkEditCB = EdGrade
      QryCampo = 'Topico'
      UpdType = utYes
      LocF7SQLMasc = '$#'
    end
    object EdGrade: TdmkEditCB
      Left = 12
      Top = 32
      Width = 56
      Height = 21
      Alignment = taRightJustify
      TabOrder = 0
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'Topico'
      UpdCampo = 'Topico'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      OnRedefinido = EdGradeRedefinido
      DBLookupComboBox = CBGrade
      IgnoraDBLookupComboBox = False
      AutoSetIfOnlyOneReg = setregOnlyManual
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 784
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 3
    object GB_R: TGroupBox
      Left = 736
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 688
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 669
        Height = 32
        Caption = 'Sele'#231#227'o de Tamanhos de T'#243'pico de Tabela de Medidas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 669
        Height = 32
        Caption = 'Sele'#231#227'o de Tamanhos de T'#243'pico de Tabela de Medidas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 669
        Height = 32
        Caption = 'Sele'#231#227'o de Tamanhos de T'#243'pico de Tabela de Medidas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 348
    Width = 784
    Height = 37
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 4
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 780
      Height = 20
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 385
    Width = 784
    Height = 70
    Align = alBottom
    TabOrder = 5
    object PnSaiDesis: TPanel
      Left = 638
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel2: TPanel
      Left = 2
      Top = 15
      Width = 636
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
      object BtTodos: TBitBtn
        Tag = 127
        Left = 144
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Todos'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtTodosClick
      end
      object BtNenhum: TBitBtn
        Tag = 128
        Left = 268
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Nenhum'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 2
        OnClick = BtNenhumClick
      end
    end
  end
  object DBGGradeTam: TdmkDBGridZTO
    Left = 0
    Top = 177
    Width = 784
    Height = 142
    Align = alClient
    DataSource = DsOVdGradeTam
    Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect, dgTitleClick, dgTitleHotTrack]
    TabOrder = 6
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    RowColors = <>
    Columns = <
      item
        Expanded = False
        FieldName = 'Codigo'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Nome'
        Title.Caption = 'Descri'#231#227'o'
        Visible = True
      end>
  end
  object QrOVdGradeCad: TMySQLQuery
    Database = FmPrincipal.TempDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM ovdgradecad'
      'ORDER BY Nome')
    Left = 236
    Top = 180
    object QrOVdGradeCadCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrOVdGradeCadNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
  end
  object DsOVdGradeCad: TDataSource
    DataSet = QrOVdGradeCad
    Left = 236
    Top = 228
  end
  object QrOVdGradeTam: TMySQLQuery
    Database = FmPrincipal.TempDB
    AfterOpen = QrOVdGradeTamAfterOpen
    BeforeClose = QrOVdGradeTamBeforeClose
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM ovdgradetam'
      'ORDER BY Nome')
    Left = 404
    Top = 200
    object QrOVdGradeTamCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrOVdGradeTamNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
  end
  object DsOVdGradeTam: TDataSource
    DataSet = QrOVdGradeTam
    Left = 404
    Top = 248
  end
  object QrGrade: TMySQLQuery
    Database = FmPrincipal.TempDB
    SQL.Strings = (
      'SELECT DISTINCT CodGrade  '
      'FROM ovdProduto '
      'WHERE Codigo=33089 '
      ' ')
    Left = 276
    Top = 108
    object QrGradeCodGrade: TIntegerField
      FieldName = 'CodGrade'
    end
  end
end
