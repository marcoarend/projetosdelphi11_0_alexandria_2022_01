unit OVgItxGerBtl;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, DBCtrls, dmkLabel, Mask,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkDBEdit, dmkGeral, Variants,
  dmkValUsu, dmkImage, UnDmkEnums;

type
  TFmOVgItxGerBtl = class(TForm)
    Panel1: TPanel;
    CkContinuar: TCheckBox;
    GroupBox1: TGroupBox;
    DBEdCodigo: TdmkDBEdit;
    Label5: TLabel;
    DBEdNome: TDBEdit;
    Label3: TLabel;
    GroupBox2: TGroupBox;
    EdControle: TdmkEdit;
    Label6: TLabel;
    EdSeccaoOP: TdmkEdit;
    Label7: TLabel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    QrEntidades: TmySQLQuery;
    QrEntidadesCodigo: TIntegerField;
    QrEntidadesCodUsu: TIntegerField;
    QrEntidadesNome: TWideStringField;
    DsEntidades: TDataSource;
    Label1: TLabel;
    EdSeccaoMaq: TdmkEdit;
    EdQtReal: TdmkEdit;
    Label2: TLabel;
    Label4: TLabel;
    EdOrdem: TdmkEdit;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenCadastro_Com_Itens_ITS(Controle: Integer);
  public
    { Public declarations }
    //FQrCab,
    FQrIts: TmySQLQuery;
    FDsCab: TDataSource;
  end;

  var
  FmOVgItxGerBtl: TFmOVgItxGerBtl;

implementation

uses UnMyObjects, Module, UMySQLModule, UnInternalConsts, MyDBCheck, DmkDAC_PF;

{$R *.DFM}

procedure TFmOVgItxGerBtl.BtOKClick(Sender: TObject);
var
  SeccaoOP, SeccaoMaq: String;
  Codigo, Controle, Ordem: Integer;
  QtReal: Double;
  SQLType: TSQLType;
begin
  SQLType        := ImgTipo.SQLType;
  Codigo         := Geral.IMV(DBEdCodigo.Text);
  Controle       := EdControle.ValueVariant;
  Ordem          := EdOrdem.ValueVariant;
  SeccaoOP       := Trim(EdSeccaoOP.ValueVariant);
  SeccaoMaq      := Trim(EdSeccaoMaq.ValueVariant);
  QtReal         := EdQtReal.ValueVariant;
  //
  if MyObjects.FIC(Trim(SeccaoOP) = '', EdSeccaoOP, 'Informe a OP!') then
    Exit;
  //
  Controle  := UMyMod.BPGS1I32('ovgitxgerbtl', 'Controle', '', '', tsPos, SQLType, Controle);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'ovgitxgerbtl', False, [
  'Codigo', 'Ordem', 'SeccaoOP',
  'SeccaoMaq', 'QtReal'], [
  'Controle'], [
  Codigo, Ordem, SeccaoOP,
  SeccaoMaq, QtReal], [
  Controle], True) then
  begin
    Dmod.AtualizaQtBtl(Codigo);
    ReopenCadastro_Com_Itens_ITS(Controle);
    if CkContinuar.Checked then
    begin
      ImgTipo.SQLType          := stIns;
      EdControle.ValueVariant  := 0;

      EdOrdem.ValueVariant     := EdOrdem.ValueVariant + 1;
      EdSeccaoOP.ValueVariant  := '';
      EdSeccaoMaq.ValueVariant := '';
      EdQtReal.ValueVariant    := 0;
      //
      EdOrdem.SetFocus;
    end else Close;
  end;
end;

procedure TFmOVgItxGerBtl.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmOVgItxGerBtl.FormActivate(Sender: TObject);
begin
  DBEdCodigo.DataSource := FDsCab;
  DBEdNome.DataSource := FDsCab;
  MyObjects.CorIniComponente();
end;

procedure TFmOVgItxGerBtl.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
end;

procedure TFmOVgItxGerBtl.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmOVgItxGerBtl.ReopenCadastro_Com_Itens_ITS(Controle: Integer);
begin
  if FQrIts <> nil then
  begin
    UnDmkDAC_PF.AbreQuery(FQrIts, FQrIts.Database);
    //
    if Controle <> 0 then
      FQrIts.Locate('Controle', Controle, []);
  end;
end;

end.
