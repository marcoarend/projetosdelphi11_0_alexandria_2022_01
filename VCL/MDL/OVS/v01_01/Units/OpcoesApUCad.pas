unit OpcoesApUCad;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkPermissoes,
  dmkCheckBox;

type
  TFmOpcoesApUCad = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Panel5: TPanel;
    EdCodigo: TdmkEdit;
    EdLogin: TdmkEdit;
    Label1: TLabel;
    Panel6: TPanel;
    Panel8: TPanel;
    CkHabFaccao: TdmkCheckBox;
    GroupBox2: TGroupBox;
    PnFaccoes: TPanel;
    CkHabFacConfeccao: TdmkCheckBox;
    Panel9: TPanel;
    GroupBox3: TGroupBox;
    PnTexteis: TPanel;
    CkHabTexTecelagem: TdmkCheckBox;
    CkHabTextil: TdmkCheckBox;
    CkHabTexTinturaria: TdmkCheckBox;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure CkHabFaccaoClick(Sender: TObject);
    procedure CkHabTextilClick(Sender: TObject);
  private
    { Private declarations }

  public
    { Public declarations }
  end;

  var
  FmOpcoesApUCad: TFmOpcoesApUCad;

implementation

uses UnMyObjects, UMySQLModule, Module;

{$R *.DFM}

procedure TFmOpcoesApUCad.BtOKClick(Sender: TObject);
var
  Codigo, HabFaccao, HabTextil,
  HabFacConfeccao, HabTexTecelagem, HabTexTinturaria: Integer;
  SQLType: TSQLType;
begin
  SQLType           := ImgTipo.SQLType;
  Codigo            := EdCodigo.ValueVariant;
  HabFaccao         := Geral.BoolToInt(CkHabFaccao.Checked);
  HabTextil         := Geral.BoolToInt(CkHabTextil.Checked);
  HabFacConfeccao   := Geral.BoolToInt(CkHabFacConfeccao.Checked);
  HabTexTecelagem   := Geral.BoolToInt(CkHabTexTecelagem.Checked);
  HabTexTinturaria  := Geral.BoolToInt(CkHabTexTinturaria.Checked);
  //
  //Codigo := UMyMod.BPGS1I32('opcoesapu', 'Codigo', '', '', tsPosNeg?, stInsUpd?, CodAtual?);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'opcoesapu', False, [
  'HabFaccao', 'HabTextil',
  'HabFacConfeccao', 'HabTexTecelagem', 'HabTexTinturaria'], [
  'Codigo'], [
  HabFaccao, HabTextil,
  HabFacConfeccao, HabTexTecelagem, HabTexTinturaria], [
  Codigo], True) then
  begin
    Close;
  end;
end;

procedure TFmOpcoesApUCad.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmOpcoesApUCad.CkHabFaccaoClick(Sender: TObject);
begin
  PnFaccoes.Enabled := CkHabFaccao.Checked;
  if CkHabFaccao.Checked = False then
  begin
    CkHabFacConfeccao.Checked := False;
  end;
end;

procedure TFmOpcoesApUCad.CkHabTextilClick(Sender: TObject);
begin
  PnTexteis.Enabled := CkHabTextil.Checked;
  if CkHabFaccao.Checked = False then
  begin
    CkHabTexTecelagem.Checked := False;
    CkHabTexTinturaria.Checked := False;
  end;
end;

procedure TFmOpcoesApUCad.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmOpcoesApUCad.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
end;

procedure TFmOpcoesApUCad.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

end.
