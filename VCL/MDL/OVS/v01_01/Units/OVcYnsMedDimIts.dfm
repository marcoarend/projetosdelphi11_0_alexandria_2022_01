object FmOVcYnsMedDimIts: TFmOVcYnsMedDimIts
  Left = 339
  Top = 185
  Caption = 'OVS-TAMAN-006 :: Defini'#231#227'o de Medidas de Tabela de Medidas'
  ClientHeight = 629
  ClientWidth = 812
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 812
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 764
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 716
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 536
        Height = 32
        Caption = 'Defini'#231#227'o de Medidas de Tabela de Medidas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 536
        Height = 32
        Caption = 'Defini'#231#227'o de Medidas de Tabela de Medidas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 536
        Height = 32
        Caption = 'Defini'#231#227'o de Medidas de Tabela de Medidas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 812
    Height = 467
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 812
      Height = 467
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 812
        Height = 467
        Align = alClient
        TabOrder = 0
        object SGTam: TStringGrid
          Left = 2
          Top = 15
          Width = 808
          Height = 361
          Align = alClient
          ColCount = 8
          DefaultColWidth = 48
          DefaultRowHeight = 21
          FixedCols = 5
          Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goEditing]
          TabOrder = 0
          OnDrawCell = SGTamDrawCell
          OnExit = SGTamExit
          OnKeyDown = SGTamKeyDown
          OnKeyPress = SGTamKeyPress
          OnKeyUp = SGTamKeyUp
          OnSelectCell = SGTamSelectCell
          OnSetEditText = SGTamSetEditText
          ExplicitHeight = 450
          ColWidths = (
            48
            48
            304
            26
            35
            48
            48
            48)
        end
        object Memo1: TMemo
          Left = 2
          Top = 376
          Width = 808
          Height = 89
          Align = alBottom
          TabOrder = 1
          Visible = False
          ExplicitLeft = 192
          ExplicitTop = 320
          ExplicitWidth = 185
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 515
    Width = 812
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 808
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 559
    Width = 812
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 666
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 664
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        Visible = False
      end
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 352
    Top = 171
  end
  object QrTam: TMySQLQuery
    Database = DModG.MyCompressDB
    SQL.Strings = (
      'SELECT ymd.CodTam, /*LPAD(ymd.CodTam, 30, " ") OrdTam,*/ '
      'ygt.Nome NO_Tobiko,  '
      'ymt.Tobiko, ygt.TolerBasCalc, ygt.TolerRndPerc, ymd.*   '
      'FROM ovcynsmeddim ymd   '
      'LEFT JOIN ovcynsmedtop ymt ON ymt.Controle=ymd.Controle '
      'LEFT JOIN ovcynsgratop ygt ON ygt.Codigo=ymt.Tobiko '
      'WHERE ymd.Codigo=2 '
      'ORDER BY /*OrdTam*/ymd.CodTam, ymt.Controle ')
    Left = 448
    Top = 272
    object QrTamCodTam: TWideStringField
      FieldName = 'CodTam'
      Size = 30
    end
    object QrTamNO_Tobiko: TWideStringField
      FieldName = 'NO_Tobiko'
      Size = 60
    end
    object QrTamTobiko: TIntegerField
      FieldName = 'Tobiko'
    end
    object QrTamTolerBasCalc: TSmallintField
      FieldName = 'TolerBasCalc'
    end
    object QrTamTolerRndPerc: TFloatField
      FieldName = 'TolerRndPerc'
    end
    object QrTamCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrTamControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrTamConta: TIntegerField
      FieldName = 'Conta'
      Required = True
    end
    object QrTamCodGrade: TIntegerField
      FieldName = 'CodGrade'
      Required = True
    end
    object QrTamCodTam_1: TWideStringField
      FieldName = 'CodTam_1'
      Size = 30
    end
    object QrTamMedidCer: TFloatField
      FieldName = 'MedidCer'
      Required = True
    end
    object QrTamMedidMin: TFloatField
      FieldName = 'MedidMin'
      Required = True
    end
    object QrTamMedidMax: TFloatField
      FieldName = 'MedidMax'
      Required = True
    end
    object QrTamUlWayInz: TSmallintField
      FieldName = 'UlWayInz'
      Required = True
    end
    object QrTamLk: TIntegerField
      FieldName = 'Lk'
      Required = True
    end
    object QrTamDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrTamDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrTamUserCad: TIntegerField
      FieldName = 'UserCad'
      Required = True
    end
    object QrTamUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Required = True
    end
    object QrTamAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Required = True
    end
    object QrTamAWServerID: TIntegerField
      FieldName = 'AWServerID'
      Required = True
    end
    object QrTamAWStatSinc: TSmallintField
      FieldName = 'AWStatSinc'
      Required = True
    end
    object QrTamAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
  end
end
