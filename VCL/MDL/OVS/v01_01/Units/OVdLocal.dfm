object FmOVdLocal: TFmOVdLocal
  Left = 368
  Top = 194
  Caption = 'OVS-CADAS-000 :: Cadastro de Locais'
  ClientHeight = 350
  ClientWidth = 784
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnEdita: TPanel
    Left = 0
    Top = 96
    Width = 784
    Height = 254
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    Visible = False
    object GBEdita: TGroupBox
      Left = 0
      Top = 0
      Width = 784
      Height = 125
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 0
      object Label7: TLabel
        Left = 16
        Top = 16
        Width = 14
        Height = 13
        Caption = 'ID:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label9: TLabel
        Left = 76
        Top = 16
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
        Color = clBtnFace
        ParentColor = False
      end
      object EdCodigo: TdmkEdit
        Left = 16
        Top = 32
        Width = 56
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Codigo'
        UpdCampo = 'Codigo'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdNome: TdmkEdit
        Left = 76
        Top = 32
        Width = 692
        Height = 21
        TabOrder = 1
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Nome'
        UpdCampo = 'Nome'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
    end
    object GBConfirma: TGroupBox
      Left = 0
      Top = 191
      Width = 784
      Height = 63
      Align = alBottom
      TabOrder = 1
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 12
        Top = 17
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object Panel1: TPanel
        Left = 645
        Top = 15
        Width = 137
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 7
          Top = 2
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
    end
  end
  object PnDados: TPanel
    Left = 0
    Top = 96
    Width = 784
    Height = 254
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    object GBDados: TGroupBox
      Left = 0
      Top = 0
      Width = 784
      Height = 317
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 1
      object Label1: TLabel
        Left = 16
        Top = 16
        Width = 14
        Height = 13
        Caption = 'ID:'
        FocusControl = DBEdCodigo
      end
      object Label2: TLabel
        Left = 76
        Top = 16
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
        FocusControl = DBEdNome
      end
      object Label3: TLabel
        Left = 16
        Top = 56
        Width = 245
        Height = 13
        Caption = 'Entidades dona do local (fornecedora dos servi'#231'os):'
        FocusControl = DBEdit1
      end
      object Label4: TLabel
        Left = 16
        Top = 96
        Width = 80
        Height = 13
        Caption = 'Qt. Funcion'#225'rios:'
        FocusControl = DBEdit3
      end
      object Label5: TLabel
        Left = 152
        Top = 96
        Width = 49
        Height = 13
        Caption = 'Efici'#234'ncia:'
        FocusControl = DBEdit4
      end
      object Label6: TLabel
        Left = 292
        Top = 96
        Width = 53
        Height = 13
        Caption = 'Nr. Tempo:'
        FocusControl = DBEdit5
      end
      object Label8: TLabel
        Left = 432
        Top = 96
        Width = 58
        Height = 13
        Caption = 'Qt. Meta dia'
        FocusControl = DBEdit6
      end
      object Label10: TLabel
        Left = 724
        Top = 16
        Width = 39
        Height = 13
        Caption = 'Externo:'
        FocusControl = DBEdit7
      end
      object Label11: TLabel
        Left = 16
        Top = 136
        Width = 86
        Height = 13
        Caption = 'Descri'#231#227'o Inativo:'
        FocusControl = DBEdit8
      end
      object Label12: TLabel
        Left = 572
        Top = 96
        Width = 75
        Height = 13
        Caption = #218'ltima inser'#231#227'o:'
        FocusControl = DBEdit9
      end
      object SpeedButton5: TSpeedButton
        Left = 748
        Top = 71
        Width = 23
        Height = 23
        Caption = '>'
        OnClick = SpeedButton5Click
      end
      object DBEdCodigo: TdmkDBEdit
        Left = 16
        Top = 32
        Width = 56
        Height = 21
        Hint = 'N'#186' do banco'
        TabStop = False
        DataField = 'Codigo'
        DataSource = DsOVdLocal
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 0
        UpdType = utYes
        Alignment = taRightJustify
      end
      object DBEdNome: TdmkDBEdit
        Left = 76
        Top = 32
        Width = 645
        Height = 21
        Hint = 'Nome do banco'
        Color = clWhite
        DataField = 'Nome'
        DataSource = DsOVdLocal
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        UpdType = utYes
        Alignment = taLeftJustify
      end
      object DBEdit1: TDBEdit
        Left = 16
        Top = 72
        Width = 72
        Height = 21
        DataField = 'Fornecedor'
        DataSource = DsOVdLocal
        TabOrder = 2
      end
      object DBEdit2: TDBEdit
        Left = 92
        Top = 72
        Width = 653
        Height = 21
        DataField = 'NO_FORNECE'
        DataSource = DsOVdLocal
        TabOrder = 3
      end
      object DBEdit3: TDBEdit
        Left = 16
        Top = 112
        Width = 134
        Height = 21
        DataField = 'QtFuncionario'
        DataSource = DsOVdLocal
        TabOrder = 4
      end
      object DBEdit4: TDBEdit
        Left = 152
        Top = 112
        Width = 134
        Height = 21
        DataField = 'Eficiencia'
        DataSource = DsOVdLocal
        TabOrder = 5
      end
      object DBEdit5: TDBEdit
        Left = 292
        Top = 112
        Width = 134
        Height = 21
        DataField = 'NrTempo'
        DataSource = DsOVdLocal
        TabOrder = 6
      end
      object DBEdit6: TDBEdit
        Left = 432
        Top = 112
        Width = 134
        Height = 21
        DataField = 'QtMetadia'
        DataSource = DsOVdLocal
        TabOrder = 7
      end
      object DBEdit7: TDBEdit
        Left = 724
        Top = 32
        Width = 45
        Height = 21
        DataField = 'Externo'
        DataSource = DsOVdLocal
        TabOrder = 8
      end
      object DBEdit8: TDBEdit
        Left = 16
        Top = 152
        Width = 753
        Height = 21
        DataField = 'DsInativo'
        DataSource = DsOVdLocal
        TabOrder = 9
      end
      object DBEdit9: TDBEdit
        Left = 572
        Top = 112
        Width = 197
        Height = 21
        DataField = 'LastInsrt'
        DataSource = DsOVdLocal
        TabOrder = 10
      end
    end
    object GBCntrl: TGroupBox
      Left = 0
      Top = 190
      Width = 784
      Height = 64
      Align = alBottom
      TabOrder = 0
      object Panel5: TPanel
        Left = 2
        Top = 15
        Width = 172
        Height = 47
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 174
        Top = 15
        Width = 88
        Height = 47
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel3: TPanel
        Left = 262
        Top = 15
        Width = 520
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object BtExclui: TBitBtn
          Tag = 12
          Left = 252
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Exclui'
          Enabled = False
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
        end
        object BtAltera: TBitBtn
          Tag = 11
          Left = 128
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Altera'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          Visible = False
          OnClick = BtAlteraClick
        end
        object BtInclui: TBitBtn
          Tag = 10
          Left = 4
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Inclui'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          Visible = False
          OnClick = BtIncluiClick
        end
        object Panel2: TPanel
          Left = 387
          Top = 0
          Width = 133
          Height = 47
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 3
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 784
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 736
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 12
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 216
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 216
      Top = 0
      Width = 520
      Height = 52
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 236
        Height = 32
        Caption = 'Cadastro de Locais'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 236
        Height = 32
        Caption = 'Cadastro de Locais'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 236
        Height = 32
        Caption = 'Cadastro de Locais'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 52
    Width = 784
    Height = 44
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 780
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 12
        Height = 17
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 12
        Height = 17
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object QrOVdLocal: TMySQLQuery
    BeforeOpen = QrOVdLocalBeforeOpen
    AfterOpen = QrOVdLocalAfterOpen
    SQL.Strings = (
      'SELECT IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_FORNECE,'
      'loc.*'
      'FROM ovdlocal loc'
      'LEFT JOIN entidades frn ON frn.Codigo=loc.Fornecedor'
      '')
    Left = 216
    Top = 60
    object QrOVdLocalCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrOVdLocalNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QrOVdLocalFornecedor: TIntegerField
      FieldName = 'Fornecedor'
      Required = True
    end
    object QrOVdLocalQtFuncionario: TIntegerField
      FieldName = 'QtFuncionario'
      Required = True
    end
    object QrOVdLocalEficiencia: TFloatField
      FieldName = 'Eficiencia'
      Required = True
    end
    object QrOVdLocalNrTempo: TFloatField
      FieldName = 'NrTempo'
      Required = True
    end
    object QrOVdLocalQtMetadia: TFloatField
      FieldName = 'QtMetadia'
      Required = True
    end
    object QrOVdLocalExterno: TWideStringField
      FieldName = 'Externo'
      Required = True
      Size = 6
    end
    object QrOVdLocalDsInativo: TWideStringField
      FieldName = 'DsInativo'
      Size = 100
    end
    object QrOVdLocalReInsrt: TIntegerField
      FieldName = 'ReInsrt'
      Required = True
    end
    object QrOVdLocalLastInsrt: TDateTimeField
      FieldName = 'LastInsrt'
      Required = True
    end
    object QrOVdLocalLk: TIntegerField
      FieldName = 'Lk'
      Required = True
    end
    object QrOVdLocalDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrOVdLocalDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrOVdLocalUserCad: TIntegerField
      FieldName = 'UserCad'
      Required = True
    end
    object QrOVdLocalUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Required = True
    end
    object QrOVdLocalAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Required = True
    end
    object QrOVdLocalAWServerID: TIntegerField
      FieldName = 'AWServerID'
      Required = True
    end
    object QrOVdLocalAWStatSinc: TSmallintField
      FieldName = 'AWStatSinc'
      Required = True
    end
    object QrOVdLocalAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
    object QrOVdLocalNO_FORNECE: TWideStringField
      FieldName = 'NO_FORNECE'
      Size = 100
    end
  end
  object DsOVdLocal: TDataSource
    DataSet = QrOVdLocal
    Left = 216
    Top = 104
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = BtInclui
    CanUpd01 = BtAltera
    CanDel01 = BtExclui
    Left = 120
    Top = 64
  end
end
