unit UnOVS_PF;

interface

uses
{
  Forms, StdCtrls, ComCtrls, ExtCtrls, Windows, Messages, SysUtils, Classes,
  Graphics, Controls, Dialogs, Variants, MaskUtils, Registry, ShellAPI, TypInfo,
{
  comobj, ShlObj, Winsock, Math, About, UnMsgInt, mySQLDbTables, DB,
  Vcl.DBCtrls, System.Rtti, dmkEdit, dmkEditCB,
  mySQLExceptions, UnInternalConsts, dmkDBGrid, UnDmkProcFunc,
  UnDmkEnums, UnGrl_Vars, mySQLDirectQuery;
}
  System.Json, (* uLkJSON, UnMyJson, *)
  System.SysUtils, System.Types, Variants,
  dmkGeral, UnDMkEnums, UnProjGroupEnums;

type
  TDeviceAllowReason = (dalUnknown=0, dalAtivado1aVez=1, dalReativado=2,
                        dalBloqueado=3, dalPerdido=4, dalRoubado=5,
                        dalDemitido=6, dalDevcEmDesuso=7, dalDesatOutros=7);
  TDeviceAcsReason = (darUnknown=0, darOutros=1, darSincroTabs=2,
                      darInspectionUpload=3, darAtivarDispositivo=4);
  TUnOVS_PF = class(TObject)
  private
    {private declaration}
    FIniTick, FFimTick, FDifTick: DWORD;
    FCodLayout, FCodLog: Integer;
    FDataHora_TXT: String;
    FDataHora_DTH: TDateTime;
  public
    {public declaration}
    function  AlteraZtatusOVgIspAllSta(IspZtatus, Motivo, Local, NrOP, SeqGrupo,
              NrReduzidoOP: Integer; DtHr: TDateTime): Boolean;
    function  ExcluiZtatusOVgIspAllSta(IspZtatus, Motivo, Local, NrOP, SeqGrupo,
              NrReduzidoOP: Integer; DtHr: TDateTime): Boolean;
    procedure OVcMobDevAlw_MudaPermissao(MobDevCad: Integer; DeviceID: String;
              AlwReason: TDeviceAllowReason; Data: TDateTime);
    procedure OVcModDevAcs_Acesso(MobDevCad: Integer; DeviceID: String;
              AcsReason: TDeviceAcsReason; Data: TDateTime);
    function  DefineNomeArquivoFotoInconformidade(CodInMob, CtrlInMob, IDTabela:
              Integer; Data: Variant): String;
    function  InsereOVgIspGerCab(CodAtual, Local, NrOP, SeqGrupo, NrReduzidoOP,
              OVcYnsMed, OVcYnsChk, LimiteChk, LimiteMed, ZtatusIsp, ZtatusMot,
              OVcYnsARQ, PermiFinHow: Integer; DtHrAbert, ZtatusDtH: String;
              SegmntInsp, SeccaoInsp: Integer): Integer;
    function  InsereOVgItxGerCab(SQLType: TSQLType; CodAtual, Local, NrOP, SeqGrupo, NrReduzidoOP,
              OVcYnsExg, (*OVcYnsChk, LimiteChk, LimiteMed,*) ZtatusIsp, ZtatusMot,
              (*OVcYnsARQ,*) PermiFinHow: Integer; DtHrAbert, ZtatusDtH: String;
              SegmntInsp, SeccaoInsp: Integer): Integer;
    function  ObtemSegmentoESecaoDeLocal(const Local: Integer; var SegmntInsp,
              SeccaoInsp: Integer): Boolean;
    function  ObtemSegmntInspDeSeccaoInsp(SeccaoInsp: Integer): Integer;
    function  CorrigeSeccaoInspDeTextil(): Integer;
    function  JSON_RetornoNotificacaoUnicaMobile(const JsonStr: String; var
              MsgRetorno, MsgErro: String): Boolean;
    //function  EnviaNotificaoMobileChamado(): Boolean;
  end;

var
  OVS_PF: TUnOVS_PF;
  //

implementation

uses UMySQLModule, UnOVS_Consts, DmkDAC_PF, MyDBCheck,
  Module, ModuleGeral;


{ TUnOVS_PF }

function TUnOVS_PF.AlteraZtatusOVgIspAllSta(IspZtatus, Motivo, Local, NrOP,
  SeqGrupo, NrReduzidoOP: Integer; DtHr: TDateTime): Boolean;
var
  DataHora: String;
begin
  Result := False;
  //DataHora := Geral.FDT(DModG.ObtemAgora(), 109);
  DataHora := Geral.FDT(DtHr, 109);
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'ovgispallsta', False, [
  'Local', 'NrOP', 'SeqGrupo',
  'NrReduzidoOP', 'IspZtatus', 'DataHora',
  'Motivo'], [
  ], [
  Local, NrOP, SeqGrupo,
  NrReduzidoOP, IspZtatus, DataHora,
  Motivo], [
  ], True) then
  begin
    Result := UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'ovgisplassta', False, [
    'IspZtatus', 'DataHora', 'Motivo'], [
    'Local', 'NrOP', 'SeqGrupo', 'NrReduzidoOP'], [
    IspZtatus, DataHora, Motivo], [
    Local, NrOP, SeqGrupo, NrReduzidoOP], True);
  end;
end;

function TUnOVS_PF.CorrigeSeccaoInspDeTextil(): Integer;
begin
  Geral.MB_Info('Falta implementar: TUnOVS_PF.CorrigeSeccaoInspDeTextil()');
(*
   Tinturaria incorreto

SELECT loc.*
FROM ovdlocal loc
LEFT JOIN ovdclaslocal cla ON cla.Codigo=loc.Codigo
  AND cla. A t r ibNome="LOCAL"  Mudar para CodNome=10?
WHERE cla. A t r ibValr="TINTURARIA" Mudar para CodValr=4?
AND loc.Codigo IN (
  SELECT DISTINCT itx.Local
  FROM ovgitxgercab itx
  LEFT JOIN ovdlocal lo2 ON lo2.Codigo=itx.Local
  WHERE itx.SeccaoInsp=1024
)

  Corre��o Tinturaria:

UPDATE ovgitxgercab
SET SeccaoInsp=2048
WHERE Local IN (1341,3501,7522)



   Textil incorreto

SELECT loc.*
FROM ovdlocal loc
LEFT JOIN ovdclaslocal cla ON cla.Codigo=loc.Codigo
  AND cla. A t r ibNome="LOCAL"  Mudar para CodNome=10?
WHERE cla. A t r ibValr="TEXTIL" Mudar para CodValr=4?
AND loc.Codigo IN (
  SELECT DISTINCT itx.Local
  FROM ovgitxgercab itx
  LEFT JOIN ovdlocal lo2 ON lo2.Codigo=itx.Local
  WHERE itx.SeccaoInsp=2048
)
*)
end;

function TUnOVS_PF.DefineNomeArquivoFotoInconformidade(CodInMob,
  CtrlInMob, IDTabela: Integer; Data: Variant): String;
var
  DataHora: String;
begin
  Result := 'OVS_' +
            Geral.FFN(CodInMob, 10) + '_' +
            Geral.FFN(IdTabela, 2) + '_' +
            Geral.FFN(CtrlInMob, 10);
  if Data <> null then
  begin
    DataHora := FormatDateTime('_YYYYMMDD_HHNNSS', Data);
    Result := Result + DataHora + '.bmp';
  end;
end;

function TUnOVS_PF.ExcluiZtatusOVgIspAllSta(IspZtatus, Motivo, Local, NrOP,
  SeqGrupo, NrReduzidoOP: Integer; DtHr: TDateTime): Boolean;
var
  DataHora: String;
begin
  Result := False;
{
  DataHora := Geral.FDT(DtHr, 109);
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'ovgispallsta', False, [
  'Local', 'NrOP', 'SeqGrupo',
  'NrReduzidoOP', 'IspZtatus', 'DataHora',
  'Motivo'], [
  ], [
  Local, NrOP, SeqGrupo,
  NrReduzidoOP, IspZtatus, DataHora,
  Motivo], [
  ], True) then
  begin
    Result := UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'ovgisplassta', False, [
    'IspZtatus', 'DataHora', 'Motivo'], [
    'Local', 'NrOP', 'SeqGrupo', 'NrReduzidoOP'], [
    IspZtatus, DataHora, Motivo], [
    Local, NrOP, SeqGrupo, NrReduzidoOP], True);
  end;
}
  Dmod.MyDB.Execute('DELETE FROM ovfordemproducao WHERE' +
    ' Local=' + Geral.FF0(Local) +
    ' AND NrOP=' + Geral.FF0(NrOP) +
    ' AND SeqGrupo=' + Geral.FF0(SeqGrupo) +
    ' AND NrReduzidoOP=' + Geral.FF0(NrReduzidoOP));
  Dmod.MyDB.Execute('DELETE FROM ovmordemproducao WHERE' +
    ' Local=' + Geral.FF0(Local) +
    ' AND NrOP=' + Geral.FF0(NrOP) +
    ' AND SeqGrupo=' + Geral.FF0(SeqGrupo) +
    ' AND NrReduzidoOP=' + Geral.FF0(NrReduzidoOP));
  Dmod.MyDB.Execute('DELETE FROM ovgisplassta WHERE' +
    ' Local=' + Geral.FF0(Local) +
    ' AND NrOP=' + Geral.FF0(NrOP) +
    ' AND SeqGrupo=' + Geral.FF0(SeqGrupo) +
    ' AND NrReduzidoOP=' + Geral.FF0(NrReduzidoOP));
end;

function TUnOVS_PF.InsereOVgIspGerCab(CodAtual, Local, NrOP, SeqGrupo,
  NrReduzidoOP, OVcYnsMed, OVcYnsChk, LimiteChk, LimiteMed, ZtatusIsp,
  ZtatusMot, OVcYnsARQ, PermiFinHow: Integer; DtHrAbert, ZtatusDtH: String;
  SegmntInsp, SeccaoInsp: Integer): Integer;
var
  SQLType: TSQLType;
  Agora: TDateTime;
  Codigo: Integer;
begin
  Codigo := UMyMod.BPGS1I32('ovgispgercab', 'Codigo', '', '', tsPos, SQLType, CodAtual);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'ovgispgercab', False, [
  'Local', 'NrOP', 'SeqGrupo',
  'NrReduzidoOP', 'DtHrAbert', (*'DtHrFecha',*)
  'OVcYnsMed', 'OVcYnsChk', 'LimiteChk',
  'LimiteMed', 'ZtatusIsp', 'ZtatusDtH',
  'ZtatusMot', 'OVcYnsARQ', 'PermiFinHow',
  'SegmntInsp', 'SeccaoInsp'], [
  'Codigo'], [
  Local, NrOP, SeqGrupo,
  NrReduzidoOP, DtHrAbert, (*DtHrFecha,*)
  OVcYnsMed, OVcYnsChk, LimiteChk,
  LimiteMed, ZtatusIsp, ZtatusDtH,
  ZtatusMot, OVcYnsARQ, PermiFinHow,
  SegmntInsp, SeccaoInsp], [
  Codigo], True) then
  begin
    //FCodigo := Codigo;
    Result := Codigo;
    if SQLType = stIns then
      OVS_PF.AlteraZtatusOVgIspAllSta(CO_OVS_IMPORT_ALHEIO_5120_APTO_DOWNMOBILE,
        ZtatusMot, Local, NrOP, SeqGrupo, NrReduzidoOP, Agora);
     //Close;
  end;
end;

function TUnOVS_PF.InsereOVgItxGerCab(SQLType: TSQLType; CodAtual, Local, NrOP, SeqGrupo,
  NrReduzidoOP, OVcYnsExg, ZtatusIsp, ZtatusMot, PermiFinHow: Integer;
  DtHrAbert, ZtatusDtH: String; SegmntInsp, SeccaoInsp: Integer): Integer;
var
  Agora: TDateTime;
  Codigo: Integer;
begin
  Codigo := UMyMod.BPGS1I32('ovgitxgercab', 'Codigo', '', '', tsPos, SQLType, CodAtual);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'ovgitxgercab', False, [
  'Local', 'NrOP', 'SeqGrupo',
  'NrReduzidoOP', 'DtHrAbert', (*'DtHrFecha',*)
  'OVcYnsExg', //'OVcYnsChk', 'LimiteChk',
  //'LimiteMed',
  'ZtatusIsp', 'ZtatusDtH',
  'ZtatusMot', (*'OVcYnsARQ',*) 'PermiFinHow',
  'SegmntInsp', 'SeccaoInsp'], [
  'Codigo'], [
  Local, NrOP, SeqGrupo,
  NrReduzidoOP, DtHrAbert, (*DtHrFecha,*)
  OVcYnsExg, //OVcYnsChk, LimiteChk,
  //LimiteMed,
  ZtatusIsp, ZtatusDtH,
  ZtatusMot, (*OVcYnsARQ,*) PermiFinHow,
  SegmntInsp, SeccaoInsp], [
  Codigo], True) then
  begin
    //FCodigo := Codigo;
    Result := Codigo;
    if SQLType = stIns then
      OVS_PF.AlteraZtatusOVgIspAllSta(CO_OVS_IMPORT_ALHEIO_5120_APTO_DOWNMOBILE,
        ZtatusMot, Local, NrOP, SeqGrupo, NrReduzidoOP, Agora);
     //Close;
  end;
end;

function TUnOVS_PF.JSON_RetornoNotificacaoUnicaMobile(const JsonStr: String; var
  MsgRetorno, MsgErro: String): Boolean;
var
  jsonObj   : TJSONObject;
  jsubObj   : TJSONObject;
  //
  jv        : TJSONValue;
  LProducts : TJSONValue;
  LProduct  : TJSONValue;
  //
  LJPair    : TJSONPair;
  //
  LSize, LIndex: Integer;
  s: String;
(*
'{"multicast_id":8105932566156483185,"success":0,"failure":1,"canonical_ids":0,"results":[{"error":"InvalidRegistration"}]}'
*)
begin
  MsgRetorno := '';
  MsgErro    := '';
  //
  jsonObj := TJSONObject.ParseJSONValue(TEncoding.ASCII.GetBytes(JsonStr), 0)
    as TJSONObject;
  jv := jsonobj.Get('success').JsonValue;

{
  jsubObj := jv as TJSONObject;

  jv := jsubObj.Get('message').JsonValue;
  jsubObj := jv as TJSONObject;

  jv := jsubObj.Get('value').JsonValue;
}

  s := jv.Value;
  Result := Geral.IMV(s) > 0;

  //

  if Result = False then
  begin
    // Mensagem de retorno:
    LProducts := jsonObj.Get('results').JsonValue;
    MsgRetorno := LProducts.ToJSON; //OK  >> [{"error":"InvalidRegistration"}]
    //s := LProducts.GetValue<string>('results[0].error');
    //Geral.MB_Info(s);
    //EXIT;
    LSize     := TJSONArray(LProducts).Size;
    if LSize > 0 then
    begin
      LIndex   := 0;
      LProduct := TJSONArray(LProducts).Get(LIndex);
      //
      //Geral.MB_Info(LProduct.ToJSON);
      jv := TJSONObject(LProduct).Get('error').JsonValue;
      MsgErro := jv.Value;
    end;
  end;
end;

{
function TUnOVS_PF.ObtemSegmentoESecaoDeLocal(const Local: Integer;
  var SegmntInsp, SeccaoInsp: Integer): Boolean;
var
  sLocal,
  sConfeccao, sTecelagem, sTinturaria: String;
begin
  SegmntInsp  := 0;
  SeccaoInsp  := 0;
  //
  sLocal := StringReplace(Dmod.QrOpcoesAppLoadCSV A t b NomLocal.Value, '"', '', [rfReplaceAll, rfIgnoreCase]);
  //
  sConfeccao    := StringReplace(Dmod.QrOpcoesApp S e c Confeccao.Value, '"', '', [rfReplaceAll, rfIgnoreCase]);
  sTecelagem  := StringReplace(Dmod.QrOpcoesApp S e c Tecelagem.Value, '"', '', [rfReplaceAll, rfIgnoreCase]);
  sTinturaria := StringReplace(Dmod.QrOpcoesApp S e c Tinturaria.Value, '"', '', [rfReplaceAll, rfIgnoreCase]);
  //
  if Trim(sConfeccao + sTecelagem + sTinturaria) <> EmptyStr then
  begin
    if Trim(sConfeccao) <> EmptyStr then
      sConfeccao := '  WHEN "' + sConfeccao + '" THEN ' + Geral.FF0(Integer(TSeccaoInsp.sccinspConfeccao));

    if Trim(sTecelagem) <> EmptyStr then
      sTecelagem := '  WHEN "' + sTecelagem + '" THEN ' + Geral.FF0(Integer(TSeccaoInsp.sccinspTecelagem));

    if Trim(sTinturaria) <> EmptyStr then
      sTinturaria := '  WHEN "' + sTinturaria + '" THEN ' + Geral.FF0(Integer(TSeccaoInsp.sccinspTinturaria));

    UnDmkDAC_PF.AbreMySQLQuery0(Dmod.QrAux, Dmod.MyDB, [
    'SELECT CASE cla. A t r ibValr ',
       sConfeccao,
       sTecelagem,
       sTinturaria,
    '  ELSE 0  ',
    'END SeccaoInsp ',
    'FROM ovdlocal loc ',
    'LEFT JOIN ovdclaslocal cla ON cla.Codigo=loc.Codigo ',
    '  AND cla. A t r ibNome="' + sLocal + '" ',
    'WHERE loc.Codigo=' + Geral.FF0(Local),
    'AND cla. A t r ibNome="' + sLocal + '" ',
    EmptyStr]);
    //
    if Dmod.QrAux.RecordCount > 0 then
    begin
      SeccaoInsp := Dmod.QrAux.FieldByName('SeccaoInsp').AsInteger;
      SegmntInsp := ObtemSegmntInspDeSeccaoInsp(SeccaoInsp);
    end;
  end;
end;
}

function TUnOVS_PF.ObtemSegmentoESecaoDeLocal(const Local: Integer;
  var SegmntInsp, SeccaoInsp: Integer): Boolean;
var
  sLocal,
  sNada, sConfeccao, sTecelagem, sTinturaria: String;
begin
  SegmntInsp  := 0;
  SeccaoInsp  := 0;
  //
  sLocal := Geral.FF0(Dmod.QrOpcoesAppLoadCSVCodNomLocal.Value);
  //
  sConfeccao  := '';
  sTecelagem  := '';
  sTinturaria := '';
  //
  if (Dmod.QrOpcoesAppCdScConfeccao.Value <> 0)
  or (Dmod.QrOpcoesAppCdScTecelagem.Value <> 0)
  or (Dmod.QrOpcoesAppCdScTinturaria.Value <> 0) then
  begin
    sNada := '  WHEN 0 THEN 0 ';
    //
    if Dmod.QrOpcoesAppCdScConfeccao.Value <> 0 then
      sConfeccao  := '  WHEN ' + Geral.FF0(Dmod.QrOpcoesAppCdScConfeccao.Value) +
                     ' THEN ' + Geral.FF0(Integer(TSeccaoInsp.sccinspConfeccao));

    if Dmod.QrOpcoesAppCdScTecelagem.Value <> 0 then
      sTecelagem  := '  WHEN ' + Geral.FF0(Dmod.QrOpcoesAppCdScTecelagem.Value) +
                     ' THEN ' + Geral.FF0(Integer(TSeccaoInsp.sccinspTecelagem));

    if Dmod.QrOpcoesAppCdScTinturaria.Value <> 0 then
      sTinturaria := '  WHEN ' + Geral.FF0(Dmod.QrOpcoesAppCdScTinturaria.Value) +
                     ' THEN ' + Geral.FF0(Integer(TSeccaoInsp.sccinspTinturaria));

    UnDmkDAC_PF.AbreMySQLQuery0(Dmod.QrAux, Dmod.MyDB, [
    'SELECT CASE cla.CodValr ',
       sNada,
       sConfeccao,
       sTecelagem,
       sTinturaria,
    '  ELSE 0  ',
    'END SeccaoInsp ',
    'FROM ovdlocal loc ',
    'LEFT JOIN ovdclaslocal cla ON cla.Codigo=loc.Codigo ',
    '  AND cla.CodNome="' + sLocal + '" ',
    'WHERE loc.Codigo=' + Geral.FF0(Local),
    'AND cla.CodNome="' + sLocal + '" ',
    EmptyStr]);
    //
    if Dmod.QrAux.RecordCount > 0 then
    begin
      SeccaoInsp := Dmod.QrAux.FieldByName('SeccaoInsp').AsInteger;
      SegmntInsp := ObtemSegmntInspDeSeccaoInsp(SeccaoInsp);
    end;
  end;
end;

function TUnOVS_PF.ObtemSegmntInspDeSeccaoInsp(SeccaoInsp: Integer): Integer;
const
  sProcName = 'TUnOVS_PF.ObtemSegmntInspDeSeccaoInsp()';
begin
  case TSeccaoInsp(SeccaoInsp) of
    sccinspNone(*0*)         : Result := CO_SGMT_INSP_0000_INDEFINIDO;
    sccinspTecelagem(*1024*) : Result := CO_SGMT_INSP_2048_TEXTIL;
    sccinspTinturaria(*2048*): Result := CO_SGMT_INSP_2048_TEXTIL;
    sccinspConfeccao(*3072*) : Result := CO_SGMT_INSP_1024_FACCAO;
    else
    begin
      Result := CO_SGMT_INSP_0000_INDEFINIDO;
      Geral.MB_Erro('SeccaoInsp n�o definido em ' + sProcName);
    end;
  end;
end;

procedure TUnOVS_PF.OVcMobDevAlw_MudaPermissao(MobDevCad: Integer;
  DeviceID: String; AlwReason: TDeviceAllowReason; Data: TDateTime);
var
  // DeviceID,
  Nome, DtaInicio: String;
  //AlwReason,
  Codigo: Integer;
  SQLType: TSQLType;
begin
  SQLType        := stIns;
  Codigo         := 0;
  Nome           := '';
  //MobDevCad      := ;
  //DeviceID       := ;
  //AlwReason      := ; // 0=Unknown 1=Ativado 1a vez 2=Reativado 3=Bloqueado 4=Perdido 5=Roubado 6=Demitido 7=Desat.Outros
  if Data > 2 then
    DtaInicio      := Geral.FDT(Data, 109)
  else
    DtaInicio      := Geral.FDT(DModG.ObtemAgora(), 109);
  //
  Codigo := UMyMod.BPGS1I32('ovcmobdevalw', 'Codigo', '', '', tsPos, stIns, Codigo);
  //if
  UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'ovcmobdevalw', False, [
  'Nome', 'MobDevCad', 'DeviceID',
  'AlwReason', 'DtaInicio'], [
  'Codigo'], [
  Nome, MobDevCad, DeviceID,
  Integer(AlwReason), DtaInicio], [
  Codigo], True);
end;

procedure TUnOVS_PF.OVcModDevAcs_Acesso(MobDevCad: Integer; DeviceID: String;
  AcsReason: TDeviceAcsReason; Data: TDateTime);
var
  Nome, (*DeviceID,*) DtaAcsIni, DtaAcsFim: String;
  Codigo(*, MobDevCad, AcsReason*): Integer;
  SQLType: TSQLType;
begin
  SQLType        := stIns;
  Codigo         := 0;
  Nome           := '';
  //MobDevCad      := ;
  //DeviceID       := ;
  //AcsReason      := ; //0=Unknown 1= Outros 2=SincroTabs 3=InspectionUpload 4=Ativar dispositivo
  if Data > 2 then
    DtaAcsIni      := Geral.FDT(Data, 109)
  else
    DtaAcsIni      := Geral.FDT(DModG.ObtemAgora(), 109);
  //
  DtaAcsFim      := '0000-00-00';
  //
  Codigo := UMyMod.BPGS1I32('ovcmobdevacs', 'Codigo', '', '', tsPos, stIns, Codigo);
  //if
  UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'ovcmobdevacs', False, [
  'Nome', 'MobDevCad', 'DeviceID',
  'AcsReason', 'DtaAcsIni', 'DtaAcsFim'], [
  'Codigo'], [
  Nome, MobDevCad, DeviceID,
  Integer(AcsReason), DtaAcsIni, DtaAcsFim], [
  Codigo], True);
end;

end.
