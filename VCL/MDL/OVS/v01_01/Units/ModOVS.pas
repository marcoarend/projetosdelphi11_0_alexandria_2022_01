unit ModOVS;

interface

uses
  System.SysUtils, System.Classes, Data.DB, mySQLDbTables,
  dmkEdit, dmkEditCB, dmkDBLookupComboBox, dmkGeral;

type
  TDmOVS = class(TDataModule)
    QrPesq2: TMySQLQuery;
    QrPesq2Referencia: TWideStringField;
    QrPesq1: TMySQLQuery;
    QrPesq1Codigo: TIntegerField;
  private
    { Private declarations }
  public
    { Public declarations }
    procedure PesquisaPorCodigo(Artigo: Integer; EdReferencia: TdmkEdit);
    procedure PesquisaPorReferencia(Limpa: Boolean; EdReferencia: TdmkEdit; EdArtigo:
              TdmkEditCB; CBArtigo: TdmkDBLookupComboBox);
  end;

var
  DmOVS: TDmOVS;

implementation

uses UnMyObjects, Module, DmkDAC_PF;

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

{ TDmOVS }

procedure TDmOVS.PesquisaPorCodigo(Artigo: Integer; EdReferencia: TdmkEdit);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrPesq2, Dmod.MyDB, [
  'SELECT Referencia ',
  'FROM ovdreferencia ',
  'WHERE Codigo= ' + Geral.FF0(Artigo),
  EmptyStr]);
  if QrPesq2.RecordCount > 0 then
  begin
    if EdReferencia.ValueVariant <> QrPesq2Referencia.Value then
      EdReferencia.ValueVariant := QrPesq2Referencia.Value;
  end else EdReferencia.ValueVariant := '';
end;

procedure TDmOVS.PesquisaPorReferencia(Limpa: Boolean; EdReferencia: TdmkEdit;
  EdArtigo: TdmkEditCB; CBArtigo: TdmkDBLookupComboBox);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrPesq1, Dmod.MyDB, [
  'SELECT Codigo ',
  'FROM ovdreferencia ',
  'WHERE Referencia="' + EdReferencia.Text + '"',
  EmptyStr]);
  //
  if QrPesq1.RecordCount > 0 then
  begin
    if EdArtigo.ValueVariant <> QrPesq1Codigo.Value then
      EdArtigo.ValueVariant := QrPesq1Codigo.Value;
    if CBArtigo.KeyValue     <> QrPesq1Codigo.Value then
      CBArtigo.KeyValue     := QrPesq1Codigo.Value;
  end
  else if Limpa then
    EdReferencia.ValueVariant := '';
end;

end.
