object FmOVdCiclo: TFmOVdCiclo
  Left = 368
  Top = 194
  Caption = 'OVS-CADAS-001 :: Cadastro de Ciclos'
  ClientHeight = 572
  ClientWidth = 1008
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnEdita: TPanel
    Left = 0
    Top = 96
    Width = 1008
    Height = 476
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    Visible = False
    object GBEdita: TGroupBox
      Left = 0
      Top = 0
      Width = 1008
      Height = 125
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 0
      object Label7: TLabel
        Left = 16
        Top = 16
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label9: TLabel
        Left = 76
        Top = 16
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
        Color = clBtnFace
        ParentColor = False
      end
      object EdCodigo: TdmkEdit
        Left = 16
        Top = 32
        Width = 56
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Codigo'
        UpdCampo = 'Codigo'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdNome: TdmkEdit
        Left = 76
        Top = 32
        Width = 633
        Height = 21
        TabOrder = 1
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Nome'
        UpdCampo = 'Nome'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
    end
    object GBConfirma: TGroupBox
      Left = 0
      Top = 413
      Width = 1008
      Height = 63
      Align = alBottom
      TabOrder = 1
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 12
        Top = 17
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object Panel1: TPanel
        Left = 868
        Top = 15
        Width = 138
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 7
          Top = 2
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
    end
  end
  object PnDados: TPanel
    Left = 0
    Top = 96
    Width = 1008
    Height = 476
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    object GBDados: TGroupBox
      Left = 0
      Top = 0
      Width = 1008
      Height = 105
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 1
      object Label1: TLabel
        Left = 16
        Top = 16
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
        FocusControl = DBEdCodigo
      end
      object Label2: TLabel
        Left = 76
        Top = 16
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
        FocusControl = DBEdNome
      end
      object Label8: TLabel
        Left = 652
        Top = 16
        Width = 55
        Height = 13
        Caption = 'Data inicial:'
        FocusControl = DBEdit1
      end
      object Label3: TLabel
        Left = 768
        Top = 16
        Width = 48
        Height = 13
        Caption = 'Data final:'
        FocusControl = DBEdit2
      end
      object Label5: TLabel
        Left = 884
        Top = 16
        Width = 48
        Height = 13
        Caption = 'Fechado?'
        FocusControl = DBEdit3
      end
      object Label6: TLabel
        Left = 940
        Top = 16
        Width = 62
        Height = 13
        Caption = 'Reinser'#231#245'es:'
        FocusControl = DBEdit4
      end
      object Label4: TLabel
        Left = 16
        Top = 56
        Width = 44
        Height = 13
        Caption = 'Empresa:'
        FocusControl = DBEdit5
      end
      object Label10: TLabel
        Left = 480
        Top = 56
        Width = 36
        Height = 13
        Caption = 'ID Ent.:'
        FocusControl = DBEdit5
      end
      object Label11: TLabel
        Left = 540
        Top = 56
        Width = 23
        Height = 13
        Caption = 'Filial:'
        FocusControl = DBEdit5
      end
      object Label12: TLabel
        Left = 888
        Top = 56
        Width = 55
        Height = 13
        Caption = 'CNPJ/CPF:'
        FocusControl = DBEdit9
      end
      object DBEdCodigo: TdmkDBEdit
        Left = 16
        Top = 32
        Width = 56
        Height = 21
        TabStop = False
        DataField = 'Codigo'
        DataSource = DsOVdCiclo
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 0
        UpdType = utYes
        Alignment = taRightJustify
      end
      object DBEdNome: TdmkDBEdit
        Left = 76
        Top = 32
        Width = 569
        Height = 21
        Color = clWhite
        DataField = 'Nome'
        DataSource = DsOVdCiclo
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        UpdType = utYes
        Alignment = taLeftJustify
      end
      object DBEdit1: TDBEdit
        Left = 652
        Top = 32
        Width = 112
        Height = 21
        DataField = 'DataIni_TXT'
        DataSource = DsOVdCiclo
        TabOrder = 2
      end
      object DBEdit2: TDBEdit
        Left = 768
        Top = 32
        Width = 112
        Height = 21
        DataField = 'DataFim_TXT'
        DataSource = DsOVdCiclo
        TabOrder = 3
      end
      object DBEdit3: TDBEdit
        Left = 884
        Top = 32
        Width = 53
        Height = 21
        DataField = 'Fechado'
        DataSource = DsOVdCiclo
        TabOrder = 4
      end
      object DBEdit4: TDBEdit
        Left = 940
        Top = 32
        Width = 62
        Height = 21
        DataField = 'ReInsrt'
        DataSource = DsOVdCiclo
        TabOrder = 5
      end
      object DBEdit5: TDBEdit
        Left = 16
        Top = 72
        Width = 56
        Height = 21
        DataField = 'Empresa'
        DataSource = DsOVdCiclo
        TabOrder = 6
      end
      object DBEdit6: TDBEdit
        Left = 76
        Top = 72
        Width = 400
        Height = 21
        DataField = 'NO_Empresa'
        DataSource = DsOVdCiclo
        TabOrder = 7
      end
      object DBEdit7: TDBEdit
        Left = 540
        Top = 72
        Width = 345
        Height = 21
        DataField = 'NO_Empresa'
        DataSource = DsOVdCiclo
        TabOrder = 8
      end
      object DBEdit8: TDBEdit
        Left = 480
        Top = 72
        Width = 56
        Height = 21
        DataField = 'Empresa'
        DataSource = DsOVdCiclo
        TabOrder = 9
      end
      object DBEdit9: TDBEdit
        Left = 888
        Top = 72
        Width = 113
        Height = 21
        DataField = 'CNPJ_CPF'
        DataSource = DsOVdCiclo
        TabOrder = 10
      end
    end
    object GBCntrl: TGroupBox
      Left = 0
      Top = 412
      Width = 1008
      Height = 64
      Align = alBottom
      TabOrder = 0
      object Panel5: TPanel
        Left = 2
        Top = 15
        Width = 172
        Height = 47
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 174
        Top = 15
        Width = 311
        Height = 47
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel3: TPanel
        Left = 485
        Top = 15
        Width = 521
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object Panel2: TPanel
          Left = 388
          Top = 0
          Width = 133
          Height = 47
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 0
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
        object BtCab: TBitBtn
          Tag = 243
          Left = 4
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&???'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          Visible = False
          OnClick = BtCabClick
        end
        object BtIts: TBitBtn
          Tag = 110
          Left = 128
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&???'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          Visible = False
          OnClick = BtItsClick
        end
      end
    end
    object PnConfig: TPanel
      Left = 0
      Top = 105
      Width = 1008
      Height = 60
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 2
      object Panel6: TPanel
        Left = 800
        Top = 0
        Width = 208
        Height = 60
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object BtConfig: TBitBtn
          Tag = 14
          Left = 8
          Top = 13
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Confirma'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtConfigClick
        end
      end
      object RGOrdem1: TRadioGroup
        Left = 0
        Top = 0
        Width = 200
        Height = 60
        Align = alLeft
        Caption = ' Ordem 1:'
        Columns = 2
        Items.Strings = (
          'Local'
          'N'#176' OP'
          'Empresa'
          'Reduzido OP')
        TabOrder = 1
        OnClick = RGOrdem1Click
      end
      object RGOrdem2: TRadioGroup
        Left = 200
        Top = 0
        Width = 200
        Height = 60
        Align = alLeft
        Caption = ' Ordem 2:'
        Columns = 2
        Enabled = False
        Items.Strings = (
          'Local'
          'N'#176' OP'
          'Empresa'
          'Reduzido OP')
        TabOrder = 2
        OnClick = RGOrdem2Click
      end
      object RGOrdem3: TRadioGroup
        Left = 400
        Top = 0
        Width = 200
        Height = 60
        Align = alLeft
        Caption = ' Ordem 3:'
        Columns = 2
        Enabled = False
        Items.Strings = (
          'Local'
          'N'#176' OP'
          'Empresa'
          'Reduzido OP')
        TabOrder = 3
        OnClick = RGOrdem3Click
      end
      object RGOrdem4: TRadioGroup
        Left = 600
        Top = 0
        Width = 200
        Height = 60
        Align = alLeft
        Caption = ' Ordem 4:'
        Columns = 2
        Enabled = False
        Items.Strings = (
          'Local'
          'N'#176' OP'
          'Empresa'
          'Reduzido OP')
        TabOrder = 4
        OnClick = RGOrdem4Click
      end
    end
    object PnGrids: TPanel
      Left = 0
      Top = 165
      Width = 1008
      Height = 113
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 3
      object PnNrOP: TPanel
        Left = 0
        Top = 0
        Width = 101
        Height = 113
        Align = alLeft
        BevelOuter = bvNone
        Caption = 'PnNrOP'
        TabOrder = 0
        Visible = False
        object DBGNrOP: TDBGrid
          Left = 0
          Top = 21
          Width = 101
          Height = 92
          Align = alClient
          DataSource = DsNrOP
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'NrOP'
              Title.Caption = 'N'#176' OP'
              Visible = True
            end>
        end
        object Panel7: TPanel
          Left = 0
          Top = 0
          Width = 101
          Height = 21
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 1
          object Label13: TLabel
            Left = 0
            Top = 0
            Width = 24
            Height = 21
            Align = alLeft
            Alignment = taCenter
            Caption = ' OP: '
            ExplicitHeight = 13
          end
          object SpeedButton5: TSpeedButton
            Left = 81
            Top = 0
            Width = 20
            Height = 21
            Align = alRight
            Caption = '>'
            ExplicitLeft = 78
          end
          object EdLocOP: TdmkEdit
            Left = 24
            Top = 0
            Width = 57
            Height = 21
            Align = alClient
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            OnRedefinido = EdLocOPRedefinido
          end
        end
      end
      object PnLocal: TPanel
        Left = 101
        Top = 0
        Width = 224
        Height = 113
        Align = alLeft
        BevelOuter = bvNone
        Caption = 'PnNrOP'
        TabOrder = 1
        Visible = False
        object DBGLocal: TDBGrid
          Left = 0
          Top = 21
          Width = 224
          Height = 92
          Align = alClient
          DataSource = DsLocal
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'Local'
              Width = 48
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_Local'
              Title.Caption = 'Descri'#231#227'o'
              Visible = True
            end>
        end
        object Panel9: TPanel
          Left = 0
          Top = 0
          Width = 224
          Height = 21
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 1
          Visible = False
          object Label14: TLabel
            Left = 0
            Top = 0
            Width = 14
            Height = 21
            Align = alLeft
            Alignment = taCenter
            Caption = '**: '
            ExplicitHeight = 13
          end
          object SpeedButton6: TSpeedButton
            Left = 204
            Top = 0
            Width = 20
            Height = 21
            Align = alRight
            Caption = '>'
            ExplicitLeft = 78
          end
          object dmkEdit1: TdmkEdit
            Left = 14
            Top = 0
            Width = 190
            Height = 21
            Align = alClient
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            OnRedefinido = EdLocOPRedefinido
          end
        end
      end
      object PnEmpresa: TPanel
        Left = 325
        Top = 0
        Width = 84
        Height = 113
        Align = alLeft
        BevelOuter = bvNone
        Caption = 'PnNrOP'
        TabOrder = 2
        Visible = False
        object DBEmpresa: TDBGrid
          Left = 0
          Top = 21
          Width = 84
          Height = 92
          Align = alClient
          DataSource = DsEmpresa
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'Empresa'
              Width = 48
              Visible = True
            end>
        end
        object Panel10: TPanel
          Left = 0
          Top = 0
          Width = 84
          Height = 21
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 1
          Visible = False
          object Label15: TLabel
            Left = 0
            Top = 0
            Width = 14
            Height = 21
            Align = alLeft
            Alignment = taCenter
            Caption = '**: '
            ExplicitHeight = 13
          end
          object SpeedButton7: TSpeedButton
            Left = 64
            Top = 0
            Width = 20
            Height = 21
            Align = alRight
            Caption = '>'
            ExplicitLeft = 78
          end
          object dmkEdit2: TdmkEdit
            Left = 14
            Top = 0
            Width = 50
            Height = 21
            Align = alClient
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            OnRedefinido = EdLocOPRedefinido
          end
        end
      end
      object PnNrReduzidoOP: TPanel
        Left = 409
        Top = 0
        Width = 108
        Height = 113
        Align = alLeft
        BevelOuter = bvNone
        Caption = 'PnNrOP'
        TabOrder = 3
        Visible = False
        object DBGNrReduzidoOP: TDBGrid
          Left = 0
          Top = 21
          Width = 108
          Height = 92
          Align = alClient
          DataSource = DsNrReduzidoOP
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'NrReduzidoOP'
              Title.Caption = 'Reduz. OP'
              Width = 72
              Visible = True
            end>
        end
        object Panel12: TPanel
          Left = 0
          Top = 0
          Width = 108
          Height = 21
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 1
          object Label16: TLabel
            Left = 0
            Top = 0
            Width = 26
            Height = 21
            Align = alLeft
            Alignment = taCenter
            Caption = 'Red: '
            ExplicitHeight = 13
          end
          object SpeedButton8: TSpeedButton
            Left = 88
            Top = 0
            Width = 20
            Height = 21
            Align = alRight
            Caption = '>'
            ExplicitLeft = 78
          end
          object dmkEdit3: TdmkEdit
            Left = 26
            Top = 0
            Width = 62
            Height = 21
            Align = alClient
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            OnRedefinido = EdLocOPRedefinido
          end
        end
      end
      object DGDados: TDBGrid
        Left = 517
        Top = 0
        Width = 491
        Height = 113
        Align = alClient
        DataSource = DsOVfOrdemProducao
        TabOrder = 4
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'CodPessoa'
            Title.Caption = 'Pessoa'
            Width = 48
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DLO_Externo'
            Title.Caption = 'Externo'
            Width = 41
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DtInclusao'
            Title.Caption = 'Inclus'#227'o'
            Width = 56
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NO_TipoOP'
            Title.Caption = 'Tipo OP'
            Width = 56
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NO_NrTipoProducaoOP'
            Title.Caption = 'TPOP'
            Width = 45
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NO_NrSituacaOP'
            Title.Caption = 'Situa'#231#227'o da OP'
            Width = 118
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NO_CodCategoria'
            Title.Caption = 'Categoria'
            Width = 76
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'SeqGrupo'
            Title.Caption = 'Grupo'
            Width = 48
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NO_Referencia'
            Title.Caption = 'Refer'#234'ncia'
            Width = 240
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CodGrade'
            Title.Caption = 'Grade'
            Width = 32
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Produto'
            Width = 44
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CodTam'
            Title.Caption = 'Tamanho'
            Width = 63
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'QtReal'
            Title.Caption = 'Qt. Real'
            Width = 48
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'QtLocal'
            Title.Caption = 'Qt. Local'
            Width = 48
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NrLote'
            Title.Caption = 'Lote '
            Width = 63
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NO_Lote'
            Title.Caption = 'Descri'#231#227'o do lote'
            Width = 200
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Local'
            Width = 48
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NrOP'
            Title.Caption = 'Nr. OP'
            Width = 48
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Empresa'
            Width = 48
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NrReduzidoOP'
            Title.Caption = 'Nr. Red. OP'
            Width = 63
            Visible = True
          end>
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 12
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 216
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 216
      Top = 0
      Width = 744
      Height = 52
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 232
        Height = 32
        Caption = 'Cadastro de Ciclos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 232
        Height = 32
        Caption = 'Cadastro de Ciclos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 232
        Height = 32
        Caption = 'Cadastro de Ciclos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 52
    Width = 1008
    Height = 44
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = BtCab
    Left = 120
    Top = 64
  end
  object QrOVdCiclo: TMySQLQuery
    Database = FmPrincipal.TempDB
    BeforeOpen = QrOVdCicloBeforeOpen
    AfterOpen = QrOVdCicloAfterOpen
    BeforeClose = QrOVdCicloBeforeClose
    AfterScroll = QrOVdCicloAfterScroll
    SQL.Strings = (
      'SELECT ode.CodPessoa CP_Empresa, ode.Nome NO_Empresa,'
      'ode.GrupoEmpresa, ode.Fantasia, ode.CNPJ_CPF,'
      
        'IF(dci.DataIni  <= "1899-12-30", "",  DATE_FORMAT(dci.DataIni, "' +
        '%d/%m/%Y")) DataIni_TXT, '
      
        'IF(dci.DataFim  <= "1899-12-30", "",  DATE_FORMAT(dci.DataFim, "' +
        '%d/%m/%Y")) DataFim_TXT,'
      'dci.*'
      'FROM ovdciclo dci'
      'LEFT JOIN ovdempresa ode ON ode.Codigo=dci.Empresa'
      'WHERE dci.Codigo > 0')
    Left = 512
    Top = 13
    object QrOVdCicloNO_Empresa: TWideStringField
      FieldName = 'NO_Empresa'
      Size = 100
    end
    object QrOVdCicloGrupoEmpresa: TIntegerField
      FieldName = 'GrupoEmpresa'
    end
    object QrOVdCicloFantasia: TWideStringField
      FieldName = 'Fantasia'
      Size = 100
    end
    object QrOVdCicloCNPJ_CPF: TWideStringField
      FieldName = 'CNPJ_CPF'
      Size = 30
    end
    object QrOVdCicloCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrOVdCicloEmpresa: TIntegerField
      FieldName = 'Empresa'
      Required = True
    end
    object QrOVdCicloNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QrOVdCicloDataIni: TDateField
      FieldName = 'DataIni'
      Required = True
    end
    object QrOVdCicloDataFim: TDateField
      FieldName = 'DataFim'
      Required = True
    end
    object QrOVdCicloFechado: TWideStringField
      FieldName = 'Fechado'
      Required = True
      Size = 6
    end
    object QrOVdCicloReInsrt: TIntegerField
      FieldName = 'ReInsrt'
      Required = True
    end
    object QrOVdCicloLastInsrt: TDateTimeField
      FieldName = 'LastInsrt'
      Required = True
    end
    object QrOVdCicloDataIni_TXT: TWideStringField
      FieldName = 'DataIni_TXT'
      Size = 10
    end
    object QrOVdCicloDataFim_TXT: TWideStringField
      FieldName = 'DataFim_TXT'
      Size = 10
    end
  end
  object DsOVdCiclo: TDataSource
    DataSet = QrOVdCiclo
    Left = 512
    Top = 61
  end
  object QrNrOP: TMySQLQuery
    Database = FmPrincipal.TempDB
    BeforeClose = QrNrOPBeforeClose
    AfterScroll = QrNrOPAfterScroll
    SQL.Strings = (
      'SELECT NrOP, MIN(DtInclusao) MIN_DtInclusao '
      'FROM ovfordemproducao fop '
      'WHERE Ciclo=316 '
      'GROUP BY NrOP '
      'ORDER BY NrOP '
      ' ')
    Left = 56
    Top = 305
    object QrNrOPNrOP: TIntegerField
      FieldName = 'NrOP'
      Required = True
    end
  end
  object DsNrOP: TDataSource
    DataSet = QrNrOP
    Left = 56
    Top = 353
  end
  object PMIts: TPopupMenu
    OnPopup = PMItsPopup
    Left = 660
    Top = 412
    object ItsInclui1: TMenuItem
      Caption = '&Adiciona'
      Enabled = False
      OnClick = ItsInclui1Click
    end
    object ItsAltera1: TMenuItem
      Caption = '&Edita'
      Enabled = False
      OnClick = ItsAltera1Click
    end
    object ItsExclui1: TMenuItem
      Caption = '&Remove'
      Enabled = False
      OnClick = ItsExclui1Click
    end
  end
  object PMCab: TPopupMenu
    OnPopup = PMCabPopup
    Left = 532
    Top = 416
    object CabInclui1: TMenuItem
      Caption = '&Inclui'
      OnClick = CabInclui1Click
    end
    object CabAltera1: TMenuItem
      Caption = '&Altera'
      Enabled = False
      OnClick = CabAltera1Click
    end
    object CabExclui1: TMenuItem
      Caption = '&Exclui'
      Enabled = False
      OnClick = CabExclui1Click
    end
  end
  object QrLocal: TMySQLQuery
    Database = FmPrincipal.TempDB
    BeforeClose = QrLocalBeforeClose
    AfterScroll = QrLocalAfterScroll
    SQL.Strings = (
      'SELECT DISTINCT fop.Local, odl.Nome NO_Local'
      'FROM ovfordemproducao fop '
      'LEFT JOIN ovdlocal odl ON odl.Codigo=fop.Local'
      'WHERE Ciclo>0'
      'ORDER BY NO_Local')
    Left = 240
    Top = 312
    object QrLocalLocal: TIntegerField
      FieldName = 'Local'
      Required = True
    end
    object QrLocalNO_Local: TWideStringField
      FieldName = 'NO_Local'
      Size = 100
    end
  end
  object DsLocal: TDataSource
    DataSet = QrLocal
    Left = 240
    Top = 360
  end
  object QrNrReduzidoOP: TMySQLQuery
    Database = FmPrincipal.TempDB
    BeforeClose = QrNrReduzidoOPBeforeClose
    AfterScroll = QrNrReduzidoOPAfterScroll
    SQL.Strings = (
      'SELECT DISTINCT fop.NrReduzidoOP'
      'FROM ovfordemproducao fop '
      'WHERE Ciclo>0'
      'ORDER BY NrReduzidoOP')
    Left = 452
    Top = 320
    object QrNrReduzidoOPNrReduzidoOP: TIntegerField
      FieldName = 'NrReduzidoOP'
      Required = True
    end
  end
  object DsNrReduzidoOP: TDataSource
    DataSet = QrNrReduzidoOP
    Left = 448
    Top = 364
  end
  object QrEmpresa: TMySQLQuery
    Database = FmPrincipal.TempDB
    BeforeClose = QrEmpresaBeforeClose
    AfterScroll = QrEmpresaAfterScroll
    SQL.Strings = (
      'SELECT DISTINCT fop.Empresa'
      'FROM ovfordemproducao fop '
      'WHERE Ciclo>0'
      'ORDER BY Empresa')
    Left = 360
    Top = 333
    object QrEmpresaEmpresa: TIntegerField
      FieldName = 'Empresa'
      Required = True
    end
  end
  object DsEmpresa: TDataSource
    DataSet = QrEmpresa
    Left = 360
    Top = 381
  end
  object QrOVfOrdemProducao: TMySQLQuery
    Database = FmPrincipal.TempDB
    SQL.Strings = (
      'SELECT fop.*, dlo.Nome NO_Local, dlo.Externo DLO_Externo,'
      'dco.Nome NO_CodCategoria, ref.Nome NO_Referencia,'
      'prd.CodGrade, prd.CodTam, lot.Nome NO_Lote, '
      'nso.Nome NO_NrSituacaOP, tlo.Nome NO_TipoLocalizacao,'
      'top.Nome MO_TipoOP, tpo.Nome NO_NrTipoProducaoOP'
      'FROM ovfordemproducao fop'
      'LEFT JOIN ovdlocal dlo ON dlo.Codigo=fop.Local'
      'LEFT JOIN ovdclaslocal dcl ON dcl.Codigo=fop.Local'
      'LEFT JOIN ovdcodcategoria dco ON dco.Codigo=fop.CodCategoria'
      'LEFT JOIN ovdproduto prd ON prd.Controle=fop.Produto'
      'LEFT JOIN ovdreferencia ref ON ref.Codigo=prd.Codigo'
      'LEFT JOIN ovdlote lot ON lot.Codigo=fop.NrLote'
      'LEFT JOIN ovdnrsituacaoop nso ON nso.Codigo=fop.NrSituacaoOP'
      
        'LEFT JOIN ovdtipolocalizacao tlo ON tlo.Codigo=fop.TipoLocalizac' +
        'ao'
      'LEFT JOIN ovdtipoop top ON top.Codigo=fop.TipoOP'
      
        'LEFT JOIN ovdtipoproducaoop tpo ON tpo.CodTxt=fop.NrTipoProducao' +
        'OP'
      'WHERE Ciclo=316')
    Left = 584
    Top = 309
    object QrOVfOrdemProducaoNrReduzidoOP: TIntegerField
      FieldName = 'NrReduzidoOP'
      Required = True
    end
    object QrOVfOrdemProducaoEmpresa: TIntegerField
      FieldName = 'Empresa'
      Required = True
    end
    object QrOVfOrdemProducaoCiclo: TIntegerField
      FieldName = 'Ciclo'
      Required = True
    end
    object QrOVfOrdemProducaoNrOP: TIntegerField
      FieldName = 'NrOP'
      Required = True
    end
    object QrOVfOrdemProducaoLocal: TIntegerField
      FieldName = 'Local'
      Required = True
    end
    object QrOVfOrdemProducaoProduto: TIntegerField
      FieldName = 'Produto'
      Required = True
    end
    object QrOVfOrdemProducaoSeqGrupo: TIntegerField
      FieldName = 'SeqGrupo'
      Required = True
    end
    object QrOVfOrdemProducaoTipoOP: TIntegerField
      FieldName = 'TipoOP'
      Required = True
    end
    object QrOVfOrdemProducaoPrioridade: TIntegerField
      FieldName = 'Prioridade'
      Required = True
    end
    object QrOVfOrdemProducaoNrSituacaoOP: TIntegerField
      FieldName = 'NrSituacaoOP'
      Required = True
    end
    object QrOVfOrdemProducaoDtInclusao: TDateField
      FieldName = 'DtInclusao'
      Required = True
    end
    object QrOVfOrdemProducaoDtPrevisao: TDateField
      FieldName = 'DtPrevisao'
      Required = True
    end
    object QrOVfOrdemProducaoTipoLocalizacao: TIntegerField
      FieldName = 'TipoLocalizacao'
      Required = True
    end
    object QrOVfOrdemProducaoDtEntrada: TDateField
      FieldName = 'DtEntrada'
      Required = True
    end
    object QrOVfOrdemProducaoQtReal: TFloatField
      FieldName = 'QtReal'
      Required = True
    end
    object QrOVfOrdemProducaoQtLocal: TFloatField
      FieldName = 'QtLocal'
      Required = True
    end
    object QrOVfOrdemProducaoCodCategoria: TIntegerField
      FieldName = 'CodCategoria'
      Required = True
    end
    object QrOVfOrdemProducaoNrLote: TIntegerField
      FieldName = 'NrLote'
      Required = True
    end
    object QrOVfOrdemProducaoNrTipoProducaoOP: TWideStringField
      FieldName = 'NrTipoProducaoOP'
      Size = 6
    end
    object QrOVfOrdemProducaoDtPrevRet: TDateField
      FieldName = 'DtPrevRet'
      Required = True
    end
    object QrOVfOrdemProducaoCodPessoa: TIntegerField
      FieldName = 'CodPessoa'
      Required = True
    end
    object QrOVfOrdemProducaoReInsrt: TIntegerField
      FieldName = 'ReInsrt'
      Required = True
    end
    object QrOVfOrdemProducaoDestino: TWideStringField
      FieldName = 'Destino'
      Size = 255
    end
    object QrOVfOrdemProducaoLk: TIntegerField
      FieldName = 'Lk'
      Required = True
    end
    object QrOVfOrdemProducaoDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrOVfOrdemProducaoDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrOVfOrdemProducaoUserCad: TIntegerField
      FieldName = 'UserCad'
      Required = True
    end
    object QrOVfOrdemProducaoUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Required = True
    end
    object QrOVfOrdemProducaoAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Required = True
    end
    object QrOVfOrdemProducaoAWServerID: TIntegerField
      FieldName = 'AWServerID'
      Required = True
    end
    object QrOVfOrdemProducaoAWStatSinc: TSmallintField
      FieldName = 'AWStatSinc'
      Required = True
    end
    object QrOVfOrdemProducaoAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
    object QrOVfOrdemProducaoLastInsrt: TDateTimeField
      FieldName = 'LastInsrt'
      Required = True
    end
    object QrOVfOrdemProducaoNO_Local: TWideStringField
      FieldName = 'NO_Local'
      Size = 100
    end
    object QrOVfOrdemProducaoDLO_Externo: TWideStringField
      FieldName = 'DLO_Externo'
      Size = 6
    end
    object QrOVfOrdemProducaoNO_CodCategoria: TWideStringField
      FieldName = 'NO_CodCategoria'
      Size = 60
    end
    object QrOVfOrdemProducaoNO_Referencia: TWideStringField
      FieldName = 'NO_Referencia'
      Size = 100
    end
    object QrOVfOrdemProducaoCodGrade: TIntegerField
      FieldName = 'CodGrade'
    end
    object QrOVfOrdemProducaoCodTam: TWideStringField
      FieldName = 'CodTam'
      Size = 30
    end
    object QrOVfOrdemProducaoNO_Lote: TWideStringField
      FieldName = 'NO_Lote'
      Size = 100
    end
    object QrOVfOrdemProducaoNO_NrSituacaOP: TWideStringField
      FieldName = 'NO_NrSituacaOP'
      Size = 60
    end
    object QrOVfOrdemProducaoNO_TipoLocalizacao: TWideStringField
      FieldName = 'NO_TipoLocalizacao'
      Size = 60
    end
    object QrOVfOrdemProducaoNO_TipoOP: TWideStringField
      FieldName = 'NO_TipoOP'
      Size = 60
    end
    object QrOVfOrdemProducaoNO_NrTipoProducaoOP: TWideStringField
      FieldName = 'NO_NrTipoProducaoOP'
      Size = 60
    end
  end
  object DsOVfOrdemProducao: TDataSource
    DataSet = QrOVfOrdemProducao
    Left = 584
    Top = 361
  end
end
