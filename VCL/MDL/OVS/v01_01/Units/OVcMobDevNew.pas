unit OVcMobDevNew;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkPermissoes,
  mySQLDbTables, Vcl.Mask;

type
  TFmOVcMobDevNew = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    QrOVcMobDevCad: TMySQLQuery;
    QrOVcMobDevCadCodigo: TIntegerField;
    QrOVcMobDevCadNome: TWideStringField;
    QrOVcMobDevCadDeviceID: TWideStringField;
    QrOVcMobDevCadUserNmePdr: TWideStringField;
    QrOVcMobDevCadDeviceName: TWideStringField;
    QrOVcMobDevCadDvcScreenH: TIntegerField;
    QrOVcMobDevCadDvcScreenW: TIntegerField;
    QrOVcMobDevCadOSName: TWideStringField;
    QrOVcMobDevCadOSNickName: TWideStringField;
    QrOVcMobDevCadOSVersion: TWideStringField;
    QrOVcMobDevCadDtaHabIni: TDateTimeField;
    QrOVcMobDevCadDtaHabFim: TDateTimeField;
    QrOVcMobDevCadAllowed: TSmallintField;
    QrOVcMobDevCadLastSetAlw: TIntegerField;
    QrOVcMobDevCadLk: TIntegerField;
    QrOVcMobDevCadDataCad: TDateField;
    QrOVcMobDevCadDataAlt: TDateField;
    QrOVcMobDevCadUserCad: TIntegerField;
    QrOVcMobDevCadUserAlt: TIntegerField;
    QrOVcMobDevCadAlterWeb: TSmallintField;
    QrOVcMobDevCadAWServerID: TIntegerField;
    QrOVcMobDevCadAWStatSinc: TSmallintField;
    QrOVcMobDevCadAtivo: TSmallintField;
    DsOVcMobDevCad: TDataSource;
    GBDados: TGroupBox;
    Label16: TLabel;
    Label17: TLabel;
    Label19: TLabel;
    Label20: TLabel;
    Label21: TLabel;
    Label22: TLabel;
    Label23: TLabel;
    DBEdit1: TDBEdit;
    DBEdit3: TDBEdit;
    DBEdit4: TDBEdit;
    DBEdit5: TDBEdit;
    DBEdit6: TDBEdit;
    DBEdit7: TDBEdit;
    DBEdit8: TDBEdit;
    Timer1: TTimer;
    BtExclui: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure QrOVcMobDevCadAfterOpen(DataSet: TDataSet);
    procedure Timer1Timer(Sender: TObject);
    procedure BtExcluiClick(Sender: TObject);
  private
    { Private declarations }
    FTempo: Integer;
    procedure ReopenOVcMobDevCad();
    procedure ConfirmearCadastro(Ativa: Integer);
  public
    { Public declarations }
  end;

  var
  FmOVcMobDevNew: TFmOVcMobDevNew;

implementation

uses UnMyObjects, Module, DmkDAC_PF, UMySQLModule, ModuleGeral, UnOVS_PF;

{$R *.DFM}

procedure TFmOVcMobDevNew.BtExcluiClick(Sender: TObject);
begin
(*
  if Geral.MB_Pergunta('Este dispositivo ser� exclu�do do cadastro! ' +
  sLineBreak + 'Deseja continuar assim mesmo?') = ID_YES then
  begin
    UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB,[
    'DELETE FROM ovcmobdevcad ',
    'WHERE Codigo=' + Geral.FF0(QrOVcMobDevCadCodigo.Value),
    '']);
  end;
  ReopenOVcMobDevCad();
*)
  ConfirmearCadastro(0);
end;

procedure TFmOVcMobDevNew.BtOKClick(Sender: TObject);
begin
  ConfirmearCadastro(1);
end;

procedure TFmOVcMobDevNew.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmOVcMobDevNew.ConfirmearCadastro(Ativa: Integer);
var
  //Nome,
  DeviceID,
  //UserNmePdr, DeviceName, OSName, OSNickName, OSVersion, DtaHabFim,
  DtaHabIni: String;
  //DvcScreenH, DvcScreenW, LastSetAlw,
  Allowed, CodAnt, CodNew: Integer;
  //SQLType: TSQLType;
  Data: TDateTime;
begin
  Data           := DModG.ObtemAgora();
  //SQLType        := ImgTipo.SQLType?;
  CodAnt         := QrOVcMobDevCadCodigo.Value;
  CodNew         := 0;
  //Nome           := ;
  DeviceID       := QrOVcMobDevCadDeviceID.Value;
  //UserNmePdr     := ;
  //DeviceName     := ;
  //DvcScreenH     := ;
  //DvcScreenW     := ;
  //OSName         := ;
  //OSNickName     := ;
  //OSVersion      := ;
  DtaHabIni      := Geral.FDT(Data, 109);
  //DtaHabFim      := ;
  Allowed        := Ativa;  // sim
  //LastSetAlw     := ;

  //
  CodNew := UMyMod.BPGS1I32('ovcmobdevcad', 'Codigo', '', '', tsPos, stIns, 0);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'ovcmobdevcad', False, [
  'Codigo',
  (*'Nome', 'DeviceID', 'UserNmePdr',
  'DeviceName', 'DvcScreenH', 'DvcScreenW',
  'OSName', 'OSNickName', 'OSVersion',*)
  'DtaHabIni', (*'DtaHabFim',*) 'Allowed'(*,
  'LastSetAlw'*)], [
  'Codigo'], [
  CodNew,
  (*Nome, DeviceID, UserNmePdr,
  DeviceName, DvcScreenH, DvcScreenW,
  OSName, OSNickName, OSVersion,*)
  DtaHabIni, (*DtaHabFim,*) Allowed(*,
  LastSetAlw*)], [
  CodAnt], True) then
  begin
    OVS_PF.OVcModDevAcs_Acesso(CodNew, DeviceID,
      TDeviceAcsReason.darAtivarDispositivo, Data);
    //
    OVS_PF.OVcMobDevAlw_MudaPermissao(CodNew, DeviceID,
      TDeviceAllowReason.dalAtivado1aVez, Data);
    //
    Geral.MB_Info('Dispositivo ativado!');
  end;
  ReopenOVcMobDevCad();
end;

procedure TFmOVcMobDevNew.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmOVcMobDevNew.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  FTempo := 14;
  Timer1.Enabled := True;
end;

procedure TFmOVcMobDevNew.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmOVcMobDevNew.QrOVcMobDevCadAfterOpen(DataSet: TDataSet);
var
  Mensagem: String;
begin
  if QrOVcMobDevCad.RecordCount > 0 then
  begin
    Timer1.Enabled := False;
    if QrOVcMobDevCad.RecordCount = 1 then
      Mensagem := 'Um novo dispositivo foi encontrado!'
    else
      Mensagem := Geral.FF0(QrOVcMobDevCad.RecordCount) +
      ' novos dispositivos foram encontrados!';
    MyObjects.Informa2(LaAviso1, LaAviso2, True, Mensagem);
  end;
end;

procedure TFmOVcMobDevNew.ReopenOVcMobDevCad();
begin
  UnDmkDAC_PF.AbreQuery(QrOVcMobDevCad, Dmod.MyDB);
end;

procedure TFmOVcMobDevNew.Timer1Timer(Sender: TObject);
begin
  FTempo := FTempo + 1;
  if (QrOVcMobDevCad.State = dsInactive)
  or (QrOVcMobDevCad.RecordCount = 0) then
  begin
    MyObjects.Informa2(LaAviso1, LaAviso2, True,
    'Nenhum novo dispositivo encontrado. Nova tentativa em ' + Geral.FF0(
    15 - FTempo) + ' segundos.');
    if FTempo >= 15 then
    begin
      Timer1.Enabled := False;
      ReopenOVcMobDevCad();
      FTempo := 0;
      Timer1.Enabled := QrOVcMobDevCad.RecordCount = 0;
    end;
  end;
end;

end.
