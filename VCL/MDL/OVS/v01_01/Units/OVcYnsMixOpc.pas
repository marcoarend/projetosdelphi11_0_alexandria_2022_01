unit OVcYnsMixOpc;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, DBCtrls, dmkLabel, Mask,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkDBEdit, dmkGeral, Variants,
  dmkValUsu, dmkImage, UnDmkEnums;

type
  TFmOVcYnsMixOpc = class(TForm)
    Panel1: TPanel;
    CkContinuar: TCheckBox;
    GroupBox1: TGroupBox;
    DBEdCodigo: TdmkDBEdit;
    Label5: TLabel;
    DBEdNome: TDBEdit;
    Label3: TLabel;
    GroupBox2: TGroupBox;
    EdControle: TdmkEdit;
    Label6: TLabel;
    EdNome: TdmkEdit;
    Label7: TLabel;
    CBMagnitude: TdmkDBLookupComboBox;
    EdMagnitude: TdmkEditCB;
    Label1: TLabel;
    SpeedButton1: TSpeedButton;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    QrOVcYnsQstMag: TMySQLQuery;
    QrOVcYnsQstMagCodigo: TIntegerField;
    QrOVcYnsQstMagNome: TWideStringField;
    DsOVcYnsQstMag: TDataSource;
    EdPontNeg: TdmkEdit;
    Label2: TLabel;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenCadastro_Com_Itens_ITS(Controle: Integer);
  public
    { Public declarations }
    FQrCab, FQrIts: TmySQLQuery;
    FDsCab: TDataSource;
  end;

  var
  FmOVcYnsMixOpc: TFmOVcYnsMixOpc;

implementation

uses UnMyObjects, Module, UMySQLModule, UnInternalConsts, MyDBCheck, DmkDAC_PF,
  UnOVS_Jan;

{$R *.DFM}

procedure TFmOVcYnsMixOpc.BtOKClick(Sender: TObject);
var
  Nome: String;
  Codigo, Controle, Magnitude, PontNeg: Integer;
  SQLType: TSQLType;
begin
  SQLType        := ImgTipo.SQLType;
  Codigo         := Geral.IMV(DBEdCodigo.Text);
  Controle       := EdControle.ValueVariant;
  Nome           := EdNome.ValueVariant;
  Magnitude      := EdMagnitude.ValueVariant;
  PontNeg        := EdPontNeg.ValueVariant;
  //
  if MyObjects.FIC(Trim(Nome) = '', EdNome, 'Informe uma descri��o!') then
    Exit;
  //
  Controle := UMyMod.BPGS1I32('ovcynsmixopc', 'Controle', '', '', tsPos, SQLTYpe, Controle);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'ovcynsmixopc', False, [
  'Codigo', 'Nome', 'Magnitude',
  'PontNeg'], [
  'Controle'], [
  Codigo, Nome, Magnitude,
  PontNeg], [
  Controle], True) then
  begin
    ReopenCadastro_Com_Itens_ITS(Controle);
    if CkContinuar.Checked then
    begin
      ImgTipo.SQLType          := stIns;
      EdControle.ValueVariant  := 0;
      EdMagnitude.ValueVariant := 0;
      CBMagnitude.KeyValue     := Null;
      EdPontNeg.ValueVariant   := 0;
      EdNome.ValueVariant      := '';
      EdNome.SetFocus;
    end else Close;
  end;
end;

procedure TFmOVcYnsMixOpc.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmOVcYnsMixOpc.FormActivate(Sender: TObject);
begin
  DBEdCodigo.DataSource := FDsCab;
  DBEdNome.DataSource := FDsCab;
  MyObjects.CorIniComponente();
end;

procedure TFmOVcYnsMixOpc.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  UnDmkDAC_PF.AbreQuery(QrOVcYnsQstMag, Dmod.MyDB);
end;

procedure TFmOVcYnsMixOpc.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmOVcYnsMixOpc.ReopenCadastro_Com_Itens_ITS(Controle: Integer);
begin
  if FQrIts <> nil then
  begin
    UnDmkDAC_PF.AbreQuery(FQrIts, FQrIts.Database);
    //
    if Controle <> 0 then
      FQrIts.Locate('Controle', Controle, []);
  end;
end;

procedure TFmOVcYnsMixOpc.SpeedButton1Click(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  OVS_Jan.MostraFormOVcYnsQstMag();
  UMyMod.SetaCodigoPesquisado(EdMagnitude, CBMagnitude, QrOVcYnsQstMag, VAR_CADASTRO);
end;

end.
