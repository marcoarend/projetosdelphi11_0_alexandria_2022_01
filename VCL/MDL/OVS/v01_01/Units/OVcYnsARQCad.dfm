object FmOVcYnsARQCad: TFmOVcYnsARQCad
  Left = 368
  Top = 194
  Caption = 'OVS-PARQI-001 :: Planos de Amostragem e Regime de Qualidade'
  ClientHeight = 633
  ClientWidth = 1008
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnEdita: TPanel
    Left = 0
    Top = 96
    Width = 1008
    Height = 537
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    Visible = False
    object GBEdita: TGroupBox
      Left = 0
      Top = 0
      Width = 1008
      Height = 129
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 0
      object Label7: TLabel
        Left = 16
        Top = 16
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label9: TLabel
        Left = 76
        Top = 16
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
        Color = clBtnFace
        ParentColor = False
      end
      object EdCodigo: TdmkEdit
        Left = 16
        Top = 32
        Width = 56
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Codigo'
        UpdCampo = 'Codigo'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdNome: TdmkEdit
        Left = 76
        Top = 32
        Width = 753
        Height = 21
        TabOrder = 1
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Nome'
        UpdCampo = 'Nome'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object GroupBox3: TGroupBox
        Left = 16
        Top = 56
        Width = 373
        Height = 65
        Caption = ' Diretrizes de Amostragem: '
        TabOrder = 2
        object Panel8: TPanel
          Left = 2
          Top = 15
          Width = 369
          Height = 48
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          object Label12: TLabel
            Left = 300
            Top = 4
            Width = 26
            Height = 13
            Caption = 'NQA:'
          end
          object Label13: TLabel
            Left = 176
            Top = 4
            Width = 34
            Height = 13
            Caption = 'LMQR:'
          end
          object Label14: TLabel
            Left = 132
            Top = 4
            Width = 29
            Height = 13
            Caption = 'N'#237'vel:'
          end
          object Label15: TLabel
            Left = 8
            Top = 4
            Width = 47
            Height = 13
            Caption = 'Esquema:'
          end
          object EdEsquema: TdmkEdit
            Left = 8
            Top = 20
            Width = 120
            Height = 21
            TabOrder = 0
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            QryCampo = 'Esquema'
            UpdCampo = 'Esquema'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EDNivel: TdmkEdit
            Left = 132
            Top = 20
            Width = 41
            Height = 21
            TabOrder = 1
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            QryCampo = 'Nivel'
            UpdCampo = 'Nivel'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdLMQR: TdmkEdit
            Left = 176
            Top = 20
            Width = 120
            Height = 21
            TabOrder = 2
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            QryCampo = 'LMQR'
            UpdCampo = 'LMQR'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdNQA: TdmkEdit
            Left = 300
            Top = 20
            Width = 57
            Height = 21
            TabOrder = 3
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            QryCampo = 'NQA'
            UpdCampo = 'NQA'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
        end
      end
      object GroupBox4: TGroupBox
        Left = 392
        Top = 56
        Width = 437
        Height = 65
        Caption = ' Peso em pontos pela classifica'#231#227'o:'
        TabOrder = 3
        object Panel9: TPanel
          Left = 2
          Top = 15
          Width = 433
          Height = 48
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          object Label16: TLabel
            Left = 284
            Top = 4
            Width = 37
            Height = 13
            Caption = 'Cr'#237'tico: '
          end
          object Label17: TLabel
            Left = 148
            Top = 4
            Width = 32
            Height = 13
            Caption = 'Grave:'
          end
          object Label18: TLabel
            Left = 8
            Top = 4
            Width = 47
            Height = 13
            Caption = 'Toler'#225'vel:'
          end
          object EdPontosTole: TdmkEdit
            Left = 8
            Top = 20
            Width = 133
            Height = 21
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'PontosTole'
            UpdCampo = 'PontosTole'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
          object EdPontosGrav: TdmkEdit
            Left = 148
            Top = 20
            Width = 133
            Height = 21
            Alignment = taRightJustify
            TabOrder = 1
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'PontosGrav'
            UpdCampo = 'PontosGrav'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
          object EdPontosCrit: TdmkEdit
            Left = 284
            Top = 20
            Width = 133
            Height = 21
            Alignment = taRightJustify
            TabOrder = 2
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'PontosCrit'
            UpdCampo = 'PontosCrit'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
        end
      end
    end
    object GBConfirma: TGroupBox
      Left = 0
      Top = 474
      Width = 1008
      Height = 63
      Align = alBottom
      TabOrder = 1
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 12
        Top = 17
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object Panel1: TPanel
        Left = 868
        Top = 15
        Width = 138
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 7
          Top = 2
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
    end
  end
  object PnDados: TPanel
    Left = 0
    Top = 96
    Width = 1008
    Height = 537
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    object GBDados: TGroupBox
      Left = 0
      Top = 0
      Width = 1008
      Height = 133
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 1
      object Label1: TLabel
        Left = 16
        Top = 16
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
        FocusControl = DBEdCodigo
      end
      object Label2: TLabel
        Left = 76
        Top = 16
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
        FocusControl = DBEdNome
      end
      object DBEdCodigo: TdmkDBEdit
        Left = 16
        Top = 32
        Width = 56
        Height = 21
        TabStop = False
        DataField = 'Codigo'
        DataSource = DsOVcYnsARQCad
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 0
        UpdType = utYes
        Alignment = taRightJustify
      end
      object DBEdNome: TdmkDBEdit
        Left = 76
        Top = 32
        Width = 749
        Height = 21
        Color = clWhite
        DataField = 'Nome'
        DataSource = DsOVcYnsARQCad
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        UpdType = utYes
        Alignment = taLeftJustify
      end
      object GroupBox1: TGroupBox
        Left = 16
        Top = 56
        Width = 373
        Height = 65
        Caption = ' Diretrizes de Amostragem: '
        TabOrder = 2
        object Panel6: TPanel
          Left = 2
          Top = 15
          Width = 369
          Height = 48
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          object Label6: TLabel
            Left = 300
            Top = 4
            Width = 26
            Height = 13
            Caption = 'NQA:'
            FocusControl = DBEdit4
          end
          object Label5: TLabel
            Left = 176
            Top = 4
            Width = 34
            Height = 13
            Caption = 'LMQR:'
            FocusControl = DBEdit3
          end
          object Label4: TLabel
            Left = 132
            Top = 4
            Width = 29
            Height = 13
            Caption = 'N'#237'vel:'
            FocusControl = DBEdit2
          end
          object Label3: TLabel
            Left = 8
            Top = 4
            Width = 47
            Height = 13
            Caption = 'Esquema:'
            FocusControl = DBEdit1
          end
          object DBEdit3: TDBEdit
            Left = 176
            Top = 20
            Width = 120
            Height = 21
            DataField = 'LMQR'
            DataSource = DsOVcYnsARQCad
            TabOrder = 0
          end
          object DBEdit4: TDBEdit
            Left = 300
            Top = 20
            Width = 57
            Height = 21
            DataField = 'NQA'
            DataSource = DsOVcYnsARQCad
            TabOrder = 1
          end
          object DBEdit2: TDBEdit
            Left = 132
            Top = 20
            Width = 41
            Height = 21
            DataField = 'Nivel'
            DataSource = DsOVcYnsARQCad
            TabOrder = 2
          end
          object DBEdit1: TDBEdit
            Left = 8
            Top = 20
            Width = 120
            Height = 21
            DataField = 'Esquema'
            DataSource = DsOVcYnsARQCad
            TabOrder = 3
          end
        end
      end
      object GroupBox2: TGroupBox
        Left = 392
        Top = 56
        Width = 437
        Height = 65
        Caption = ' Peso em pontos pela classifica'#231#227'o:'
        TabOrder = 3
        object Panel7: TPanel
          Left = 2
          Top = 15
          Width = 433
          Height = 48
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          object Label11: TLabel
            Left = 284
            Top = 4
            Width = 37
            Height = 13
            Caption = 'Cr'#237'tico: '
            FocusControl = DBEdit7
          end
          object Label10: TLabel
            Left = 148
            Top = 4
            Width = 32
            Height = 13
            Caption = 'Grave:'
            FocusControl = DBEdit6
          end
          object Label8: TLabel
            Left = 8
            Top = 4
            Width = 47
            Height = 13
            Caption = 'Toler'#225'vel:'
            FocusControl = DBEdit5
          end
          object DBEdit5: TDBEdit
            Left = 8
            Top = 20
            Width = 134
            Height = 21
            DataField = 'PontosTole'
            DataSource = DsOVcYnsARQCad
            TabOrder = 0
          end
          object DBEdit7: TDBEdit
            Left = 284
            Top = 20
            Width = 134
            Height = 21
            DataField = 'PontosCrit'
            DataSource = DsOVcYnsARQCad
            TabOrder = 1
          end
          object DBEdit6: TDBEdit
            Left = 148
            Top = 20
            Width = 134
            Height = 21
            DataField = 'PontosGrav'
            DataSource = DsOVcYnsARQCad
            TabOrder = 2
          end
        end
      end
    end
    object GBCntrl: TGroupBox
      Left = 0
      Top = 473
      Width = 1008
      Height = 64
      Align = alBottom
      TabOrder = 0
      object Panel5: TPanel
        Left = 2
        Top = 15
        Width = 172
        Height = 47
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 174
        Top = 15
        Width = 311
        Height = 47
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel3: TPanel
        Left = 485
        Top = 15
        Width = 521
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object Panel2: TPanel
          Left = 388
          Top = 0
          Width = 133
          Height = 47
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 0
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
        object BtCab: TBitBtn
          Tag = 1000211
          Left = 4
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Plano'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtCabClick
        end
        object BtIts: TBitBtn
          Tag = 1000224
          Left = 128
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Itens'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = BtItsClick
        end
      end
    end
    object DGDados: TDBGrid
      Left = 0
      Top = 133
      Width = 1008
      Height = 148
      Align = alTop
      DataSource = DsOVcYnsARQIts
      TabOrder = 2
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'Controle'
          Title.Caption = 'ID'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'QtdPcIni'
          Title.Caption = 'Qtd De'
          Width = 92
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'QtdPcFim'
          Title.Caption = 'Qtd at'#233
          Width = 92
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'QtdAmostr'
          Title.Caption = 'Qtd Amostras'
          Width = 92
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'PtsAprova'
          Title.Caption = 'Pts Aprova'
          Width = 92
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'PtsResalv'
          Title.Caption = 'Pts A. c/ Resalva'
          Width = 92
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'PtsRejeit'
          Title.Caption = 'Pts Rejeita'
          Width = 92
          Visible = True
        end>
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 12
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 216
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 216
      Top = 0
      Width = 744
      Height = 52
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 573
        Height = 32
        Caption = 'Planos de Amostragem e Regime de Qualidade'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 573
        Height = 32
        Caption = 'Planos de Amostragem e Regime de Qualidade'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 573
        Height = 32
        Caption = 'Planos de Amostragem e Regime de Qualidade'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 52
    Width = 1008
    Height = 44
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = BtCab
    Left = 120
    Top = 64
  end
  object QrOVcYnsARQCad: TMySQLQuery
    Database = FmOVgIspGerCab.TempDB
    BeforeOpen = QrOVcYnsARQCadBeforeOpen
    AfterOpen = QrOVcYnsARQCadAfterOpen
    BeforeClose = QrOVcYnsARQCadBeforeClose
    AfterScroll = QrOVcYnsARQCadAfterScroll
    SQL.Strings = (
      'SELECT * FROM '
      'ovcynsarqcad'
      'WHERE Codigo <>0')
    Left = 340
    Top = 265
    object QrOVcYnsARQCadCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrOVcYnsARQCadNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
    object QrOVcYnsARQCadEsquema: TWideStringField
      FieldName = 'Esquema'
      Required = True
      Size = 15
    end
    object QrOVcYnsARQCadNivel: TWideStringField
      FieldName = 'Nivel'
      Required = True
      Size = 15
    end
    object QrOVcYnsARQCadLMQR: TWideStringField
      FieldName = 'LMQR'
      Required = True
      Size = 15
    end
    object QrOVcYnsARQCadNQA: TFloatField
      FieldName = 'NQA'
      Required = True
    end
    object QrOVcYnsARQCadPontosTole: TFloatField
      FieldName = 'PontosTole'
      Required = True
    end
    object QrOVcYnsARQCadPontosGrav: TFloatField
      FieldName = 'PontosGrav'
      Required = True
    end
    object QrOVcYnsARQCadPontosCrit: TFloatField
      FieldName = 'PontosCrit'
      Required = True
    end
    object QrOVcYnsARQCadLk: TIntegerField
      FieldName = 'Lk'
      Required = True
    end
    object QrOVcYnsARQCadDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrOVcYnsARQCadDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrOVcYnsARQCadUserCad: TIntegerField
      FieldName = 'UserCad'
      Required = True
    end
    object QrOVcYnsARQCadUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Required = True
    end
    object QrOVcYnsARQCadAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Required = True
    end
    object QrOVcYnsARQCadAWServerID: TIntegerField
      FieldName = 'AWServerID'
      Required = True
    end
    object QrOVcYnsARQCadAWStatSinc: TSmallintField
      FieldName = 'AWStatSinc'
      Required = True
    end
    object QrOVcYnsARQCadAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
  end
  object DsOVcYnsARQCad: TDataSource
    DataSet = QrOVcYnsARQCad
    Left = 340
    Top = 309
  end
  object QrOVcYnsARQIts: TMySQLQuery
    Database = FmOVgIspGerCab.TempDB
    SQL.Strings = (
      'SELECT * FROM '
      'ovcynsarqIts'
      'WHERE Codigo =:P0'
      'ORDER BY QtdPcFim, QtdPcIni'
      '')
    Left = 436
    Top = 265
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrOVcYnsARQItsCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrOVcYnsARQItsControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrOVcYnsARQItsQtdPcIni: TIntegerField
      FieldName = 'QtdPcIni'
      Required = True
    end
    object QrOVcYnsARQItsQtdPcFim: TIntegerField
      FieldName = 'QtdPcFim'
      Required = True
    end
    object QrOVcYnsARQItsQtdAmostr: TIntegerField
      FieldName = 'QtdAmostr'
      Required = True
    end
    object QrOVcYnsARQItsPtsAprova: TFloatField
      FieldName = 'PtsAprova'
      Required = True
    end
    object QrOVcYnsARQItsPtsResalv: TFloatField
      FieldName = 'PtsResalv'
      Required = True
    end
    object QrOVcYnsARQItsPtsRejeit: TFloatField
      FieldName = 'PtsRejeit'
      Required = True
    end
    object QrOVcYnsARQItsLk: TIntegerField
      FieldName = 'Lk'
      Required = True
    end
    object QrOVcYnsARQItsDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrOVcYnsARQItsDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrOVcYnsARQItsUserCad: TIntegerField
      FieldName = 'UserCad'
      Required = True
    end
    object QrOVcYnsARQItsUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Required = True
    end
    object QrOVcYnsARQItsAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Required = True
    end
    object QrOVcYnsARQItsAWServerID: TIntegerField
      FieldName = 'AWServerID'
      Required = True
    end
    object QrOVcYnsARQItsAWStatSinc: TSmallintField
      FieldName = 'AWStatSinc'
      Required = True
    end
    object QrOVcYnsARQItsAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
  end
  object DsOVcYnsARQIts: TDataSource
    DataSet = QrOVcYnsARQIts
    Left = 436
    Top = 309
  end
  object PMIts: TPopupMenu
    OnPopup = PMItsPopup
    Left = 424
    Top = 376
    object ItsInclui1: TMenuItem
      Caption = '&Adiciona'
      Enabled = False
      OnClick = ItsInclui1Click
    end
    object ItsAltera1: TMenuItem
      Caption = '&Edita'
      Enabled = False
      OnClick = ItsAltera1Click
    end
    object ItsExclui1: TMenuItem
      Caption = '&Remove'
      Enabled = False
      OnClick = ItsExclui1Click
    end
  end
  object PMCab: TPopupMenu
    OnPopup = PMCabPopup
    Left = 300
    Top = 372
    object CabInclui1: TMenuItem
      Caption = '&Inclui'
      OnClick = CabInclui1Click
    end
    object CabAltera1: TMenuItem
      Caption = '&Altera'
      Enabled = False
      OnClick = CabAltera1Click
    end
    object CabExclui1: TMenuItem
      Caption = '&Exclui'
      Enabled = False
      OnClick = CabExclui1Click
    end
  end
end
