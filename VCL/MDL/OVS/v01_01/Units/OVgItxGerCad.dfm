object FmOVgItxGerCad: TFmOVgItxGerCad
  Left = 339
  Top = 185
  Caption = 'OVS-EXCAO-002 :: Inclus'#227'o de Inspe'#231#227'o de T'#234'xteis'
  ClientHeight = 422
  ClientWidth = 812
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 812
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 764
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 716
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 391
        Height = 32
        Caption = 'Inclus'#227'o de Inspe'#231#227'o de T'#234'xteis'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 391
        Height = 32
        Caption = 'Inclus'#227'o de Inspe'#231#227'o de T'#234'xteis'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 391
        Height = 32
        Caption = 'Inclus'#227'o de Inspe'#231#227'o de T'#234'xteis'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 812
    Height = 260
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 812
      Height = 260
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 812
        Height = 260
        Align = alClient
        TabOrder = 0
        object GroupBox2: TGroupBox
          Left = 2
          Top = 15
          Width = 808
          Height = 105
          Align = alTop
          Caption = '  Dados Importados: '
          TabOrder = 0
          object PnDadosOri: TPanel
            Left = 2
            Top = 15
            Width = 804
            Height = 94
            Align = alTop
            BevelOuter = bvNone
            TabOrder = 0
            object Label1: TLabel
              Left = 8
              Top = 4
              Width = 29
              Height = 13
              Caption = 'Local:'
            end
            object Label2: TLabel
              Left = 92
              Top = 4
              Width = 91
              Height = 13
              Caption = 'Descri'#231#227'o do local:'
            end
            object Label3: TLabel
              Left = 692
              Top = 4
              Width = 18
              Height = 13
              Caption = 'OP:'
            end
            object Label4: TLabel
              Left = 8
              Top = 48
              Width = 30
              Height = 13
              Caption = 'Artigo:'
            end
            object Label5: TLabel
              Left = 92
              Top = 48
              Width = 95
              Height = 13
              Caption = 'Descri'#231#227'o do artigo:'
            end
            object Label6: TLabel
              Left = 692
              Top = 48
              Width = 52
              Height = 13
              Caption = 'Reduz.OP:'
            end
            object EdLocal: TdmkEdit
              Left = 8
              Top = 20
              Width = 80
              Height = 21
              Alignment = taRightJustify
              ReadOnly = True
              TabOrder = 0
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
            end
            object EdNO_Local: TdmkEdit
              Left = 92
              Top = 20
              Width = 597
              Height = 21
              ReadOnly = True
              TabOrder = 1
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object EdNrOP: TdmkEdit
              Left = 692
              Top = 20
              Width = 105
              Height = 21
              Alignment = taRightJustify
              ReadOnly = True
              TabOrder = 2
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
            end
            object EdSeqGrupo: TdmkEdit
              Left = 8
              Top = 64
              Width = 80
              Height = 21
              Alignment = taRightJustify
              ReadOnly = True
              TabOrder = 3
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
            end
            object EdNO_SeqGrupo: TdmkEdit
              Left = 92
              Top = 64
              Width = 597
              Height = 21
              ReadOnly = True
              TabOrder = 4
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object EdNrReduzidoOP: TdmkEdit
              Left = 692
              Top = 64
              Width = 105
              Height = 21
              Alignment = taRightJustify
              ReadOnly = True
              TabOrder = 5
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
            end
          end
        end
        object GroupBox3: TGroupBox
          Left = 2
          Top = 120
          Width = 808
          Height = 138
          Align = alClient
          Caption = ' Configura'#231#245'es de Inspe'#231#227'o: '
          TabOrder = 1
          TabStop = True
          object Panel5: TPanel
            Left = 2
            Top = 15
            Width = 804
            Height = 121
            Align = alClient
            BevelOuter = bvNone
            TabOrder = 0
            TabStop = True
            object Label7: TLabel
              Left = 92
              Top = 4
              Width = 209
              Height = 13
              Caption = 'Tabela de exa'#231#245'es [F3] do artigo [F4] todas:'
            end
            object SbOVcYnsExg: TSpeedButton
              Left = 776
              Top = 20
              Width = 21
              Height = 21
              Caption = '...'
              OnClick = SbOVcYnsExgClick
            end
            object Label13: TLabel
              Left = 8
              Top = 4
              Width = 64
              Height = 13
              Caption = 'ID Inspe'#231#227'o: '
            end
            object EdOVcYnsExg: TdmkEditCB
              Left = 92
              Top = 20
              Width = 56
              Height = 21
              Alignment = taRightJustify
              TabOrder = 1
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              QryCampo = 'Topico'
              UpdCampo = 'Topico'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              OnKeyDown = EdOVcYnsExgKeyDown
              DBLookupComboBox = CBOVcYnsExg
              IgnoraDBLookupComboBox = False
              AutoSetIfOnlyOneReg = setregOnlyManual
            end
            object CBOVcYnsExg: TdmkDBLookupComboBox
              Left = 148
              Top = 20
              Width = 625
              Height = 21
              KeyField = 'Codigo'
              ListField = 'Nome'
              ListSource = DsOVcYnsExgCad
              TabOrder = 2
              OnKeyDown = CBOVcYnsExgKeyDown
              dmkEditCB = EdOVcYnsExg
              QryCampo = 'Topico'
              UpdType = utYes
              LocF7SQLMasc = '$#'
            end
            object EdCodigo: TdmkEdit
              Left = 8
              Top = 20
              Width = 80
              Height = 21
              TabStop = False
              Alignment = taRightJustify
              ReadOnly = True
              TabOrder = 0
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
            end
            object RGPermiFinHow: TdmkRadioGroup
              Left = 8
              Top = 44
              Width = 789
              Height = 69
              Caption = ' Permiss'#227'o de finaliza'#231#227'o da inspe'#231#227'o: '
              Columns = 2
              ItemIndex = 0
              Items.Strings = (
                '0 - Indefinido'
                '1 - Somente ao finalizar toda inspe'#231#227'o'
                '2 - Ao atingir a pontua'#231#227'o de reprova'#231#227'o'
                '3 - A qualquer momento')
              TabOrder = 3
              QryCampo = 'PermiFinHow'
              UpdCampo = 'PermiFinHow'
              UpdType = utYes
              OldValor = 0
            end
          end
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 308
    Width = 812
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 808
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 352
    Width = 812
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 666
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 664
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 312
    Top = 71
  end
  object QrOVcYnsExgCad: TMySQLQuery
    Database = Dmod.ZZDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM ovcynsmedcad'
      'ORDER BY Nome')
    Left = 480
    Top = 60
    object QrOVcYnsExgCadCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrOVcYnsExgCadNome: TWideStringField
      DisplayWidth = 60
      FieldName = 'Nome'
      Size = 60
    end
  end
  object DsOVcYnsExgCad: TDataSource
    DataSet = QrOVcYnsExgCad
    Left = 480
    Top = 108
  end
end
