unit OVdProduto;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2,
  ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, dmkGeral,
  dmkPermissoes, dmkEdit, dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup,
  unDmkProcFunc, UnDmkEnums;

type
  TFmOVdProduto = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    BtExclui: TBitBtn;
    BtAltera: TBitBtn;
    BtInclui: TBitBtn;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    QrOVdProduto: TMySQLQuery;
    DsOVdProduto: TDataSource;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    Label9: TLabel;
    EdNome: TdmkEdit;
    Label1: TLabel;
    DBEdCodigo: TdmkDBEdit;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    QrOVdProdutoControle: TIntegerField;
    QrOVdProdutoCodigo: TIntegerField;
    QrOVdProdutoDtCadastro: TDateTimeField;
    QrOVdProdutoNrInProdAcabado: TWideStringField;
    QrOVdProdutoNrInMatPrima: TWideStringField;
    QrOVdProdutoNrInPatrimonio: TWideStringField;
    QrOVdProdutoNrInMatConsumo: TWideStringField;
    QrOVdProdutoNrInativo: TWideStringField;
    QrOVdProdutoNrInProdPropria: TWideStringField;
    QrOVdProdutoNrInTerceiro: TWideStringField;
    QrOVdProdutoNrTpInspecao: TWideStringField;
    QrOVdProdutoNrPrdSped: TWideStringField;
    QrOVdProdutoCodGrade: TIntegerField;
    QrOVdProdutoNrTam: TIntegerField;
    QrOVdProdutoCodTam: TWideStringField;
    QrOVdProdutoNrCor: TWideStringField;
    QrOVdProdutoCodCor: TWideStringField;
    QrOVdProdutoCodUnidade: TWideStringField;
    QrOVdProdutoCodCst: TWideStringField;
    QrOVdProdutoCodTipi: TWideStringField;
    QrOVdProdutoQtPeso: TWideStringField;
    QrOVdProdutoReInsrt: TIntegerField;
    QrOVdProdutoLastInsrt: TDateTimeField;
    QrOVdProdutoLk: TIntegerField;
    QrOVdProdutoDataCad: TDateField;
    QrOVdProdutoDataAlt: TDateField;
    QrOVdProdutoUserCad: TIntegerField;
    QrOVdProdutoUserAlt: TIntegerField;
    QrOVdProdutoAlterWeb: TSmallintField;
    QrOVdProdutoAWServerID: TIntegerField;
    QrOVdProdutoAWStatSinc: TSmallintField;
    QrOVdProdutoAtivo: TSmallintField;
    QrOVdProdutoNO_Artigo: TWideStringField;
    QrOVdProdutoREFERENCIA: TWideStringField;
    Label3: TLabel;
    DBEdit1: TDBEdit;
    Label5: TLabel;
    DBEdit3: TDBEdit;
    DBEdit4: TDBEdit;
    Label6: TLabel;
    dmkDBEdit1: TdmkDBEdit;
    Label2: TLabel;
    Label8: TLabel;
    DBEdit5: TDBEdit;
    Panel6: TPanel;
    Label10: TLabel;
    DBEdit6: TDBEdit;
    Label11: TLabel;
    DBEdit7: TDBEdit;
    Label12: TLabel;
    DBEdit8: TDBEdit;
    Label13: TLabel;
    DBEdit9: TDBEdit;
    Label14: TLabel;
    DBEdit10: TDBEdit;
    Label15: TLabel;
    DBEdit11: TDBEdit;
    Label16: TLabel;
    DBEdit12: TDBEdit;
    Label17: TLabel;
    DBEdit13: TDBEdit;
    Label18: TLabel;
    DBEdit14: TDBEdit;
    Label19: TLabel;
    DBEdit15: TDBEdit;
    Label20: TLabel;
    DBEdit16: TDBEdit;
    Label21: TLabel;
    DBEdit17: TDBEdit;
    Label22: TLabel;
    DBEdit18: TDBEdit;
    Label23: TLabel;
    DBEdit19: TDBEdit;
    Label24: TLabel;
    DBEdit20: TDBEdit;
    Label25: TLabel;
    DBEdit21: TDBEdit;
    Label26: TLabel;
    DBEdit22: TDBEdit;
    Label27: TLabel;
    DBEdit23: TDBEdit;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrOVdProdutoAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrOVdProdutoBeforeOpen(DataSet: TDataSet);
    procedure BtIncluiClick(Sender: TObject);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure LocCod(Atual, Codigo: Integer);
    procedure Va(Para: TVaiPara);
  public
    { Public declarations }
  end;

var
  FmOVdProduto: TFmOVdProduto;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmOVdProduto.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmOVdProduto.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrOVdProdutoCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmOVdProduto.DefParams;
begin
  VAR_GOTOTABELA := 'ovdproduto';
  VAR_GOTOMYSQLTABLE := QrOVdProduto;
  VAR_GOTONEG := gotoPos;
  VAR_GOTOCAMPO := 'Controle';
  VAR_GOTONOME := 'art.Nome';
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT art.Nome NO_Artigo, ');
  VAR_SQLx.Add('art.Referencia REFERENCIA, prd.* ');
  VAR_SQLx.Add('FROM ovdproduto prd');
  VAR_SQLx.Add('LEFT JOIN ovdreferencia art ON art.Codigo=prd.Codigo');
  VAR_SQLx.Add('WHERE prd.Controle > 0');
  //
  VAR_SQL1.Add('AND prd.Controle=:P0');
  //
  //VAR_SQL2.Add('AND CodUsu=:P0');
  //
  VAR_SQLa.Add('AND art.Nome Like :P0');
  //
end;

procedure TFmOVdProduto.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmOVdProduto.QueryPrincipalAfterOpen;
begin
end;

procedure TFmOVdProduto.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARAÇÕES///////////////////

procedure TFmOVdProduto.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmOVdProduto.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmOVdProduto.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmOVdProduto.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmOVdProduto.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmOVdProduto.BtAlteraClick(Sender: TObject);
begin
  if (QrOVdProduto.State <> dsInactive) and (QrOVdProduto.RecordCount > 0) then
  begin
    UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrOVdProduto, [PnDados],
      [PnEdita], EdNome, ImgTipo, 'ovdproduto');
  end;
end;

procedure TFmOVdProduto.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrOVdProdutoCodigo.Value;
  Close;
end;

procedure TFmOVdProduto.BtConfirmaClick(Sender: TObject);
var
  Codigo: Integer;
  Nome: String;
begin
  Nome := EdNome.ValueVariant;
  if MyObjects.FIC(Length(Nome) = 0, EdNome, 'Defina uma descrição!') then Exit;
  //
  Codigo := UMyMod.BPGS1I32('ovdproduto', 'Codigo', '', '',
    tsPos, ImgTipo.SQLType, QrOVdProdutoCodigo.Value);
  if UMyMod.ExecSQLInsUpdPanel(ImgTipo.SQLType, Self, PnEdita,
    'ovdproduto', Codigo, Dmod.QrUpd, [PnEdita], [PnDados], ImgTipo, True) then
  begin
(*  Desmarcar se usar "UMyMod.SQLInsUpd(...)" em vez de "UMyMod.ExecSQLInsUpdPanel(...)"
    ImgTipo.SQLType := stLok;
    PnDados.Visible := True;
    PnEdita.Visible := False;
    GOTOy.BotoesSb(ImgTipo.SQLType);
*)
    LocCod(Codigo, Codigo);
  end;
end;

procedure TFmOVdProduto.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := EdCodigo.ValueVariant;
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  //
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'ovdproduto', 'Codigo');
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmOVdProduto.BtIncluiClick(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrOVdProduto, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'ovdproduto');
end;

procedure TFmOVdProduto.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  GBEdita.Align := alClient;
  GBDados.Align := alClient;
  CriaOForm;
end;

procedure TFmOVdProduto.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrOVdProdutoCodigo.Value, LaRegistro.Caption);
end;

procedure TFmOVdProduto.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmOVdProduto.SbNovoClick(Sender: TObject);
begin
  LocCod(QrOVdProdutoCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, '', 'ovdproduto', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmOVdProduto.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmOVdProduto.QrOVdProdutoAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmOVdProduto.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmOVdProduto.SbQueryClick(Sender: TObject);
const
  ReturnCodUsu = False;
begin
  LocCod(QrOVdProdutoControle.Value,
  //
  CuringaLoc.CriaForm('prd.Controle', 'CONCAT(art.Nome, " [", prd.CodTam, "] ", CodCor) ',
  'ovdproduto prd', Dmod.MyDB, '/**/', ReturnCodUsu,
  'LEFT JOIN ovdreferencia art ON art.Codigo=prd.Codigo')
  //
  );
end;

procedure TFmOVdProduto.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmOVdProduto.QrOVdProdutoBeforeOpen(DataSet: TDataSet);
begin
  QrOVdProdutoCodigo.DisplayFormat := FFormatFloat;
end;

end.

