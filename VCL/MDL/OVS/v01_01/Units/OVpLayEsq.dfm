object FmOVpLayEsq: TFmOVpLayEsq
  Left = 368
  Top = 194
  Caption = 'OVS-LAYOU-001 :: Esquema de Layout'
  ClientHeight = 563
  ClientWidth = 1008
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnEdita: TPanel
    Left = 0
    Top = 172
    Width = 1008
    Height = 391
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    Visible = False
    ExplicitTop = 105
    ExplicitHeight = 346
    object GBEdita: TGroupBox
      Left = 0
      Top = 0
      Width = 1008
      Height = 125
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 0
      object Label7: TLabel
        Left = 16
        Top = 16
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label9: TLabel
        Left = 76
        Top = 16
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
        Color = clBtnFace
        ParentColor = False
      end
      object EdCodigo: TdmkEdit
        Left = 16
        Top = 32
        Width = 56
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Codigo'
        UpdCampo = 'Codigo'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdNome: TdmkEdit
        Left = 76
        Top = 32
        Width = 633
        Height = 21
        TabOrder = 1
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Nome'
        UpdCampo = 'Nome'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
    end
    object GBConfirma: TGroupBox
      Left = 0
      Top = 328
      Width = 1008
      Height = 63
      Align = alBottom
      TabOrder = 1
      ExplicitTop = 283
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 12
        Top = 17
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object Panel1: TPanel
        Left = 868
        Top = 15
        Width = 138
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 7
          Top = 2
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
    end
  end
  object PnDados: TPanel
    Left = 0
    Top = 172
    Width = 1008
    Height = 391
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    ExplicitTop = 164
    ExplicitHeight = 287
    object GBDados: TGroupBox
      Left = 0
      Top = 0
      Width = 1008
      Height = 65
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 1
      object Label1: TLabel
        Left = 16
        Top = 16
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
        FocusControl = DBEdCodigo
      end
      object Label2: TLabel
        Left = 76
        Top = 16
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
        FocusControl = DBEdNome
      end
      object DBEdCodigo: TdmkDBEdit
        Left = 16
        Top = 32
        Width = 56
        Height = 21
        TabStop = False
        DataField = 'Codigo'
        DataSource = DsOVpLayEsq
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 0
        UpdType = utYes
        Alignment = taRightJustify
      end
      object DBEdNome: TdmkDBEdit
        Left = 76
        Top = 32
        Width = 689
        Height = 21
        Color = clWhite
        DataField = 'Nome'
        DataSource = DsOVpLayEsq
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        UpdType = utYes
        Alignment = taLeftJustify
      end
    end
    object GBCntrl: TGroupBox
      Left = 0
      Top = 327
      Width = 1008
      Height = 64
      Align = alBottom
      TabOrder = 0
      ExplicitTop = 282
      object Panel5: TPanel
        Left = 2
        Top = 15
        Width = 172
        Height = 47
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 174
        Top = 15
        Width = 311
        Height = 47
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel3: TPanel
        Left = 485
        Top = 15
        Width = 521
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object Panel2: TPanel
          Left = 388
          Top = 0
          Width = 133
          Height = 47
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 0
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
        object BtCab: TBitBtn
          Tag = 243
          Left = 4
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Esquema'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtCabClick
        end
        object BtIts: TBitBtn
          Tag = 110
          Left = 128
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Tabela'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = BtItsClick
        end
        object BtColuna: TBitBtn
          Tag = 110
          Left = 252
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Coluna'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = BtColunaClick
        end
      end
    end
    object DBGOVpLayTab: TDBGrid
      Left = 0
      Top = 65
      Width = 413
      Height = 262
      Align = alLeft
      DataSource = DsOVpLayTab
      TabOrder = 2
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'Controle'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Nome'
          Width = 153
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Tabela'
          Width = 153
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'LastLoad'
          Title.Caption = #218'lia importa'#231#227'o'
          Visible = True
        end>
    end
    object DBGOVpLayFld: TDBGrid
      Left = 413
      Top = 65
      Width = 595
      Height = 262
      Align = alClient
      DataSource = DsOVpLayFld
      TabOrder = 3
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'Conta'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Coluna'
          Width = 38
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DataType'
          Title.Caption = 'Tipo'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Nome'
          Title.Caption = 'Descri'#231#227'o da coluna CSV'
          Width = 135
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Campo'
          Title.Caption = 'Campo na tabela BD'
          Width = 102
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'FldType'
          Title.Caption = 'Tipo'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'FldNull'
          Title.Caption = 'Nulo'
          Width = 28
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Tamanho'
          Title.Caption = 'Tam'
          Width = 40
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'FldDefault'
          Title.Caption = 'Valor padr'#227'o'
          Width = 125
          Visible = True
        end>
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 12
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 216
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 216
      Top = 0
      Width = 744
      Height = 52
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 242
        Height = 32
        Caption = 'Esquema de Layout'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 242
        Height = 32
        Caption = 'Esquema de Layout'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 242
        Height = 32
        Caption = 'Esquema de Layout'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 52
    Width = 1008
    Height = 120
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 38
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object PB1: TProgressBar
        Left = 0
        Top = 21
        Width = 1004
        Height = 17
        Align = alBottom
        TabOrder = 0
        ExplicitTop = 19
      end
    end
    object MeAvisos: TMemo
      Left = 2
      Top = 53
      Width = 1004
      Height = 65
      Align = alClient
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clRed
      Font.Height = -13
      Font.Name = 'Lucida Console'
      Font.Style = []
      ParentFont = False
      ReadOnly = True
      TabOrder = 1
      ExplicitLeft = 44
      ExplicitTop = 92
      ExplicitWidth = 185
      ExplicitHeight = 89
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = BtCab
    Left = 120
    Top = 64
  end
  object QrOVpLayEsq: TMySQLQuery
    Database = Dmod.ZZDB
    BeforeOpen = QrOVpLayEsqBeforeOpen
    AfterOpen = QrOVpLayEsqAfterOpen
    BeforeClose = QrOVpLayEsqBeforeClose
    AfterScroll = QrOVpLayEsqAfterScroll
    SQL.Strings = (
      'SELECT * FROM cadastro_com_itens_cab'
      'WHERE Codigo > 0')
    Left = 92
    Top = 233
    object QrOVpLayEsqCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrOVpLayEsqNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
  end
  object DsOVpLayEsq: TDataSource
    DataSet = QrOVpLayEsq
    Left = 92
    Top = 277
  end
  object QrOVpLayTab: TMySQLQuery
    Database = Dmod.ZZDB
    BeforeClose = QrOVpLayTabBeforeClose
    AfterScroll = QrOVpLayTabAfterScroll
    SQL.Strings = (
      'SELECT * FROM ovplaytab'
      'WHERE Codigo =:P0'
      'ORDER BY Nome')
    Left = 188
    Top = 237
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrOVpLayTabCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrOVpLayTabControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrOVpLayTabNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
    object QrOVpLayTabTabela: TWideStringField
      FieldName = 'Tabela'
      Size = 60
    end
    object QrOVpLayTabLastLoad: TDateTimeField
      FieldName = 'LastLoad'
      Required = True
    end
    object QrOVpLayTabLk: TIntegerField
      FieldName = 'Lk'
      Required = True
    end
    object QrOVpLayTabDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrOVpLayTabDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrOVpLayTabUserCad: TIntegerField
      FieldName = 'UserCad'
      Required = True
    end
    object QrOVpLayTabUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Required = True
    end
    object QrOVpLayTabAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Required = True
    end
    object QrOVpLayTabAWServerID: TIntegerField
      FieldName = 'AWServerID'
      Required = True
    end
    object QrOVpLayTabAWStatSinc: TSmallintField
      FieldName = 'AWStatSinc'
      Required = True
    end
    object QrOVpLayTabAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
  end
  object DsOVpLayTab: TDataSource
    DataSet = QrOVpLayTab
    Left = 188
    Top = 281
  end
  object PMIts: TPopupMenu
    OnPopup = PMItsPopup
    Left = 664
    Top = 376
    object ItsInclui1: TMenuItem
      Caption = '&Adiciona'
      Enabled = False
      OnClick = ItsInclui1Click
    end
    object ItsAltera1: TMenuItem
      Caption = '&Edita'
      Enabled = False
      OnClick = ItsAltera1Click
    end
    object ItsExclui1: TMenuItem
      Caption = '&Remove'
      Enabled = False
      OnClick = ItsExclui1Click
    end
    object AdicionadelayoutCSV1: TMenuItem
      Caption = 'Adiciona de layout CSV'
      OnClick = AdicionadelayoutCSV1Click
    end
    object Criatabelanobancodedados1: TMenuItem
      Caption = 'Cria tabela no banco de dados'
      OnClick = Criatabelanobancodedados1Click
    end
  end
  object PMCab: TPopupMenu
    OnPopup = PMCabPopup
    Left = 528
    Top = 372
    object CabInclui1: TMenuItem
      Caption = '&Inclui'
      OnClick = CabInclui1Click
    end
    object CabAltera1: TMenuItem
      Caption = '&Altera'
      Enabled = False
      OnClick = CabAltera1Click
    end
    object CabExclui1: TMenuItem
      Caption = '&Exclui'
      Enabled = False
      OnClick = CabExclui1Click
    end
  end
  object QrOVpLayFld: TMySQLQuery
    Database = Dmod.ZZDB
    SQL.Strings = (
      'SELECT * FROM ovplayfld'
      'WHERE Codigo > 0')
    Left = 328
    Top = 233
    object QrOVpLayFldCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrOVpLayFldControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrOVpLayFldConta: TIntegerField
      FieldName = 'Conta'
      Required = True
    end
    object QrOVpLayFldColuna: TIntegerField
      FieldName = 'Coluna'
      Required = True
    end
    object QrOVpLayFldCampo: TWideStringField
      FieldName = 'Campo'
      Size = 60
    end
    object QrOVpLayFldNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
    object QrOVpLayFldDataType: TWideStringField
      FieldName = 'DataType'
      Required = True
      Size = 1
    end
    object QrOVpLayFldLk: TIntegerField
      FieldName = 'Lk'
      Required = True
    end
    object QrOVpLayFldDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrOVpLayFldDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrOVpLayFldUserCad: TIntegerField
      FieldName = 'UserCad'
      Required = True
    end
    object QrOVpLayFldUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Required = True
    end
    object QrOVpLayFldAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Required = True
    end
    object QrOVpLayFldAWServerID: TIntegerField
      FieldName = 'AWServerID'
      Required = True
    end
    object QrOVpLayFldAWStatSinc: TSmallintField
      FieldName = 'AWStatSinc'
      Required = True
    end
    object QrOVpLayFldAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
    object QrOVpLayFldFldType: TWideStringField
      FieldName = 'FldType'
      Size = 60
    end
    object QrOVpLayFldFldNull: TWideStringField
      FieldName = 'FldNull'
      Size = 60
    end
    object QrOVpLayFldFldDefault: TWideStringField
      FieldName = 'FldDefault'
      Size = 255
    end
    object QrOVpLayFldFldKey: TWideStringField
      FieldName = 'FldKey'
      Size = 60
    end
    object QrOVpLayFldTamanho: TWideStringField
      FieldName = 'Tamanho'
    end
  end
  object DsOVpLayFld: TDataSource
    DataSet = QrOVpLayFld
    Left = 328
    Top = 277
  end
  object PMColuna: TPopupMenu
    OnPopup = PMColunaPopup
    Left = 784
    Top = 364
    object Incluicoluna1: TMenuItem
      Caption = '&Inclui coluna'
      OnClick = Incluicoluna1Click
    end
    object Alteracolunaatual1: TMenuItem
      Caption = '&Altera coluna atual'
      OnClick = Alteracolunaatual1Click
    end
    object Excluicolunaatual1: TMenuItem
      Caption = '&Exclui coluna atual'
      OnClick = Excluicolunaatual1Click
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object Criacamponatabela1: TMenuItem
      Caption = '&Cria campo na tabela'
      OnClick = Criacamponatabela1Click
    end
  end
  object QrFlds: TMySQLQuery
    Database = Dmod.ZZDB
    SQL.Strings = (
      'SELECT tab.Tabela,  '
      'fld.Campo, fld.DataType, fld.Tamanho,'
      'fld.FldType, FldNull, FldDefault  '
      'FROM ovplayfld fld '
      'LEFT JOIN ovplaytab tab ON tab.Controle=fld.Controle '
      'WHERE fld.Codigo=2 '
      'ORDER BY Tabela, Coluna')
    Left = 600
    Top = 257
    object QrFldsTabela: TWideStringField
      FieldName = 'Tabela'
      Size = 60
    end
    object QrFldsCampo: TWideStringField
      FieldName = 'Campo'
      Size = 60
    end
    object QrFldsDataType: TWideStringField
      FieldName = 'DataType'
      Required = True
      Size = 1
    end
    object QrFldsFldType: TWideStringField
      FieldName = 'FldType'
      Size = 60
    end
    object QrFldsFldNull: TWideStringField
      FieldName = 'FldNull'
      Size = 60
    end
    object QrFldsFldDefault: TWideStringField
      FieldName = 'FldDefault'
      Size = 255
    end
    object QrFldsFldKey: TWideStringField
      FieldName = 'FldKey'
      Size = 60
    end
    object QrFldsConta: TIntegerField
      FieldName = 'Conta'
    end
    object QrFldsTamanho: TWideStringField
      FieldName = 'Tamanho'
    end
  end
end
