unit OVgItxPrfCfg;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkPermissoes,
  mySQLDbTables, dmkDBGridZTO, UnProjGroup_PF;

type
  TFmOVgItxPrfCfg = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    QrNaoCfg: TMySQLQuery;
    GroupBox1: TGroupBox;
    DBGNaoCfg: TdmkDBGridZTO;
    DsNaoCfg: TDataSource;
    QrNaoCfgLocal: TIntegerField;
    QrNaoCfgNrOP: TIntegerField;
    QrNaoCfgSeqGrupo: TIntegerField;
    QrNaoCfgNrReduzidoOP: TIntegerField;
    QrOVgItxPrfCab: TMySQLQuery;
    QrOVgItxPrfCabCodigo: TIntegerField;
    QrOVgItxPrfCabNome: TWideStringField;
    QrOVgItxPrfCabSeqGrupo: TIntegerField;
    QrOVgItxPrfCabOVcYnsExg: TIntegerField;
    QrParaCfg: TMySQLQuery;
    QrParaCfgLocal: TIntegerField;
    QrParaCfgNrOP: TIntegerField;
    QrParaCfgSeqGrupo: TIntegerField;
    QrParaCfgNrReduzidoOP: TIntegerField;
    QrAx: TMySQLQuery;
    QrOVgItxPrfCabPermiFinHow: TSmallintField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenNaoCfg();
    procedure Mensagem(SvcMsgKind: TSvcMsgKind; Msg: String);
  public
    { Public declarations }
    FAutomatico: Boolean;
    FLaAviso1: TLabel;
    FLaAviso2: TLabel;
    //
    function TemNovosItensAconfigurar(): Boolean;
    procedure ConfiguraNovosItensCarregados();
  end;

  var
  FmOVgItxPrfCfg: TFmOVgItxPrfCfg;

implementation

uses UnMyObjects, DmkDAC_PF, UnOVS_Consts, UMySQLModule, UnOVS_PF,
 UnOVS_ProjGroupVars,
 Module, ModuleGeral;

{$R *.DFM}

procedure TFmOVgItxPrfCfg.BtOKClick(Sender: TObject);
begin
  if not FAutomatico then
    ConfiguraNovosItensCarregados();
end;

procedure TFmOVgItxPrfCfg.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmOVgItxPrfCfg.ConfiguraNovosItensCarregados();
var
  SQLType: TSQLType;
  DtHrIni, DtHrFim: String;
  GrupCnfg, QtParaCfg, QtRealCfg, OVgItxGerCab: Integer;
  Inseriu: Boolean;
  //
  Local, NrOP, SeqGrupo, NrReduzidoOP, OVcYnsExg,
  //OVcYnsChk, LimiteChk, LimiteMed, OVcYnsARQ,
  PermiFinHow, ZtatusIsp, ZtatusMot,
  SegmntInsp, SeccaoInsp: Integer;
  DtHrAbert, ZtatusDtH: String;
begin
  SQLType := stIns;
  GrupCnfg       := UMyMod.BPGS1I32('ovgitxprfcfg', 'Codigo', '', '', tsPos, SQLType, 0);
  DtHrIni        := Geral.FDT(DModG.ObtemAgora(), 109);
  DtHrFim        := '0000-00-00 00:00:00';
  QtParaCfg      := QrNaoCfg.RecordCount;
  QtRealCfg      := 0;
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'ovgitxprfcfg', False, [
  'DtHrIni', 'DtHrFim',
  'QtParaCfg', 'QtRealCfg'], [
  'Codigo'], [
  DtHrIni, DtHrFim,
  QtParaCfg, QtRealCfg], [
  GrupCnfg], True) then
  begin
    // Reservando itens para evitar duplicação
    QrNaoCfg.First;
    while not QrNaoCfg.Eof do
    begin
      Dmod.MyDB.Execute(
      ' UPDATE ovgisplassta ' +
      ' SET GrupCnfg=' + Geral.FF0(GrupCnfg) +
      ' WHERE Local=' + Geral.FF0(QrNaoCfgLocal.Value) +
      ' AND NrOP=' + Geral.FF0(QrNaoCfgNrOP.Value) +
      ' AND SeqGrupo=' + Geral.FF0(QrNaoCfgSeqGrupo.Value) +
      ' AND NrReduzidoOP=' + Geral.FF0(QrNaoCfgNrReduzidoOP.Value) +
      ' AND GrupSgmt=' + Geral.FF0(CO_SGMT_INSP_2048_TEXTIL) +
      ' AND GrupCnfg=0 ' +
      EmptyStr);
      //
      UnDmkDAC_PF.AbreMySQLQuery0(QrParaCfg, Dmod.MyDB, [
      'SELECT DISTINCT ils.Local, ils.NrOP, ils.SeqGrupo,  ',
      'ils.NrReduzidoOP ',
      'FROM ovgisplassta ils ',
      'LEFT JOIN ovdlocal dlo ON dlo.Codigo=ils.Local  ',
      'LEFT JOIN ovdreferencia ref ON ref.Codigo=ils.SeqGrupo ',
      'LEFT JOIN ovgitxprfcab prf ON prf.SeqGrupo=ils.SeqGrupo',
      'WHERE ils.IspZtatus=' + Geral.FF0(CO_OVS_IMPORT_ALHEIO_1024_IMPORTADO),
      'AND ils.GrupSgmt=' + Geral.FF0(CO_SGMT_INSP_2048_TEXTIL),
      'AND ils.GrupCnfg=' + Geral.FF0(GrupCnfg),
      'AND ils.Local=' + Geral.FF0(QrNaoCfgLocal.Value),
      'AND ils.NrOP=' + Geral.FF0(QrNaoCfgNrOP.Value),
      'AND ils.SeqGrupo=' + Geral.FF0(QrNaoCfgSeqGrupo.Value),
      'AND ils.NrReduzidoOP=' + Geral.FF0(QrNaoCfgNrReduzidoOP.Value),
      'AND prf.Ativo=1 ',
      'ORDER BY NrReduzidoOP ',
      EmptyStr]);
      while not QrParaCfg.Eof do
      begin
        SeqGrupo := QrParaCfgSeqGrupo.Value;
        //
        UnDmkDAC_PF.AbreMySQLQuery0(QrOVgItxPrfCab, Dmod.MyDB, [
        'SELECT * ',
        'FROM ovgitxprfcab ',
        'WHERE Ativo=1 ',
        'AND SeqGrupo=' + Geral.FF0(SeqGrupo),
        'ORDER BY Codigo DESC ',
        EmptyStr]);
        //
        Inseriu := False;
        if QrOVgItxPrfCab.RecordCount > 0 then
        begin
{
  'SELECT CASE cla. A t r ibValr ',
  '  WHEN "FACCOES" THEN 3072 ',
  '  WHEN "TECELAGEM" THEN 1024 ',
  '  WHEN "TITURARIA" THEN 2048 ',
  '  ELSE 0  ',
  'END SeccaoInsp ',
  'FROM ovdlocal loc ',
  'LEFT JOIN ovdclaslocal cla ON cla.Codigo=loc.Codigo ',
  '  AND cla. A t r ibNome="LOCAL" ',
  'WHERE loc.Codigo=223  ',
  'AND cla. A t r ibNome="LOCAL" ',
}

          Local         := QrParaCfgLocal.Value;
          NrOP          := QrParaCfgNrOP.Value;
          NrReduzidoOP  := QrParaCfgNrReduzidoOP.Value;
          OVcYnsExg     := QrOVgItxPrfCabOVcYnsExg.Value;
(*
          OVcYnsChk     := QrOVgItxPrfCabOVcYnsChk.Value;
          LimiteChk     := QrOVgItxPrfCabLimiteChk.Value;
          LimiteMed     := QrOVgItxPrfCabLimiteMed.Value;
*)
          ZtatusIsp     := CO_OVS_IMPORT_ALHEIO_5120_APTO_DOWNMOBILE;
          ZtatusMot     := 0;
(*
          OVcYnsARQ     := QrOVgItxPrfCabOVcYnsARQ.Value;
*)
          PermiFinHow   := QrOVgItxPrfCabPermiFinHow.Value;
          DtHrAbert     := DtHrIni;
          ZtatusDtH     := DtHrAbert;
          //
          OVS_PF.ObtemSegmentoESecaoDeLocal(Local, SegmntInsp, SeccaoInsp);
          // Criar configuração em itens que conseguiu reservar com sucesso
          OVgItxGerCab := 0;
          OVgItxGerCab := OVS_PF.InsereOVgItxGerCab(stIns, OVgItxGerCab, Local, NrOP,
          SeqGrupo, NrReduzidoOP, OVcYnsExg,
          //OVcYnsChk, LimiteChk, LimiteMed,
          ZtatusIsp, ZtatusMot,
          //OVcYnsARQ,
          PermiFinHow, DtHrAbert, ZtatusDtH,
          SegmntInsp, SeccaoInsp);
          //
          Inseriu := OVgItxGerCab <> 0;
        end;
        if not Inseriu then
        begin
          Dmod.MyDB.Execute(
          ' UPDATE ovgisplassta ' +
          ' SET GrupCnfg=0 ' +
          ' WHERE Local=' + Geral.FF0(QrNaoCfgLocal.Value) +
          ' AND NrOP=' + Geral.FF0(QrNaoCfgNrOP.Value) +
          ' AND SeqGrupo=' + Geral.FF0(QrNaoCfgSeqGrupo.Value) +
          ' AND NrReduzidoOP=' + Geral.FF0(QrNaoCfgNrReduzidoOP.Value) +
          ' AND GrupCnfg=' + Geral.FF0(GrupCnfg) +
          ' AND GrupSgmt=' + Geral.FF0(CO_SGMT_INSP_2048_TEXTIL) +
          EmptyStr);
        end;
        //
        QrParaCfg.Next;
      end;
      //
      QrNaoCfg.Next;
    end;
    // Término dos configurados
    DtHrFim := Geral.FDT(DModG.ObtemAgora(), 109);
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'ovgitxprfcfg', False, [
    'DtHrFim'], ['Codigo'], [DtHrFim], [GrupCnfg], True) then
    begin
    end;
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrAx, Dmod.MyDB, [
    'SELECT *  ',
    'FROM ovgisplassta ils ',
    'WHERE ils.GrupCnfg=' + Geral.FF0(GrupCnfg),
    'AND ils.IspZtatus>' + Geral.FF0(CO_OVS_IMPORT_ALHEIO_1024_IMPORTADO),
    'AND ils.GrupSgmt=' + Geral.FF0(CO_SGMT_INSP_2048_TEXTIL),
    EmptyStr]);
    Mensagem(TSvcMsgKind.smkRunOK, 'Foram configurados ' + Geral.FF0(
    QrAx.RecordCount) + ' itens automaticamente pelo seu perfil');
    //
  end;
end;

procedure TFmOVgItxPrfCfg.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmOVgItxPrfCfg.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
end;

procedure TFmOVgItxPrfCfg.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmOVgItxPrfCfg.Mensagem(SvcMsgKind: TSvcMsgKind; Msg: String);
begin
  if VAR_RUN_AS_SVC then
    ProjGroup_PF.SalvaEmArquivoLog(TSvcMsgKind.smkGetErr, Msg)
  else
  begin
    Geral.MB_Aviso(Msg);
    MyObjects.Informa2(FLaAviso1, FLaAviso2, False, Msg);
  end;
end;

procedure TFmOVgItxPrfCfg.ReopenNaoCfg();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrNaoCfg, Dmod.MyDB, [
  'SELECT DISTINCT ils.Local, ils.NrOP, ils.SeqGrupo,  ',
  'ils.NrReduzidoOP ',
  'FROM ovgisplassta ils ',
  'LEFT JOIN ovdlocal dlo ON dlo.Codigo=ils.Local  ',
  'LEFT JOIN ovdreferencia ref ON ref.Codigo=ils.SeqGrupo ',
  'LEFT JOIN ovgitxprfcab prf ON prf.SeqGrupo=ils.SeqGrupo',
  'WHERE ils.IspZtatus=' + Geral.FF0(CO_OVS_IMPORT_ALHEIO_1024_IMPORTADO),
  'AND ils.GrupSgmt=' + Geral.FF0(CO_SGMT_INSP_2048_TEXTIL),
  'AND prf.Ativo=1',
  'ORDER BY NrReduzidoOP ',
  EmptyStr]);
end;

function TFmOVgItxPrfCfg.TemNovosItensAconfigurar(): Boolean;
begin
  ReopenNaoCfg();
  Result := QrNaoCfg.RecordCount > 0;
end;

end.
