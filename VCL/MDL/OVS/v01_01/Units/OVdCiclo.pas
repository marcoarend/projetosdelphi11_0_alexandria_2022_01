unit OVdCiclo;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2, dmkGeral,
  ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt, UMySQLModule, Menus,
  UnInternalConsts2, mySQLDbTables, UnMySQLCuringa, dmkPermissoes, dmkEdit,
  dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup, Grids, DBGrids,
  UnDmkProcFunc, UnDmkENums;

type
  TFmOVdCiclo = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    Label9: TLabel;
    EdNome: TdmkEdit;
    Label1: TLabel;
    DBEdCodigo: TdmkDBEdit;
    Label2: TLabel;
    DBEdNome: TdmkDBEdit;
    QrOVdCiclo: TMySQLQuery;
    DsOVdCiclo: TDataSource;
    QrNrOP: TMySQLQuery;
    DsNrOP: TDataSource;
    PMIts: TPopupMenu;
    ItsInclui1: TMenuItem;
    ItsExclui1: TMenuItem;
    ItsAltera1: TMenuItem;
    PMCab: TPopupMenu;
    CabInclui1: TMenuItem;
    CabAltera1: TMenuItem;
    CabExclui1: TMenuItem;
    BtCab: TBitBtn;
    BtIts: TBitBtn;
    QrOVdCicloCodigo: TIntegerField;
    QrOVdCicloEmpresa: TIntegerField;
    QrOVdCicloNome: TWideStringField;
    QrOVdCicloDataIni: TDateField;
    QrOVdCicloDataFim: TDateField;
    QrOVdCicloFechado: TWideStringField;
    QrOVdCicloReInsrt: TIntegerField;
    QrOVdCicloLastInsrt: TDateTimeField;
    QrOVdCicloDataIni_TXT: TWideStringField;
    QrOVdCicloDataFim_TXT: TWideStringField;
    Label8: TLabel;
    DBEdit1: TDBEdit;
    Label3: TLabel;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    Label5: TLabel;
    DBEdit4: TDBEdit;
    Label6: TLabel;
    QrOVdCicloNO_Empresa: TWideStringField;
    QrOVdCicloGrupoEmpresa: TIntegerField;
    QrOVdCicloFantasia: TWideStringField;
    QrOVdCicloCNPJ_CPF: TWideStringField;
    Label4: TLabel;
    DBEdit5: TDBEdit;
    DBEdit6: TDBEdit;
    DBEdit7: TDBEdit;
    DBEdit8: TDBEdit;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    DBEdit9: TDBEdit;
    QrNrOPNrOP: TIntegerField;
    QrLocal: TMySQLQuery;
    DsLocal: TDataSource;
    QrNrReduzidoOP: TMySQLQuery;
    DsNrReduzidoOP: TDataSource;
    PnConfig: TPanel;
    QrLocalLocal: TIntegerField;
    QrLocalNO_Local: TWideStringField;
    QrEmpresa: TMySQLQuery;
    DsEmpresa: TDataSource;
    QrEmpresaEmpresa: TIntegerField;
    QrNrReduzidoOPNrReduzidoOP: TIntegerField;
    Panel6: TPanel;
    RGOrdem1: TRadioGroup;
    RGOrdem2: TRadioGroup;
    RGOrdem3: TRadioGroup;
    RGOrdem4: TRadioGroup;
    BtConfig: TBitBtn;
    PnGrids: TPanel;
    PnNrOP: TPanel;
    DBGNrOP: TDBGrid;
    Panel7: TPanel;
    Label13: TLabel;
    SpeedButton5: TSpeedButton;
    EdLocOP: TdmkEdit;
    PnLocal: TPanel;
    DBGLocal: TDBGrid;
    Panel9: TPanel;
    Label14: TLabel;
    SpeedButton6: TSpeedButton;
    dmkEdit1: TdmkEdit;
    PnEmpresa: TPanel;
    DBEmpresa: TDBGrid;
    Panel10: TPanel;
    Label15: TLabel;
    SpeedButton7: TSpeedButton;
    dmkEdit2: TdmkEdit;
    PnNrReduzidoOP: TPanel;
    DBGNrReduzidoOP: TDBGrid;
    Panel12: TPanel;
    Label16: TLabel;
    SpeedButton8: TSpeedButton;
    dmkEdit3: TdmkEdit;
    DGDados: TDBGrid;
    QrOVfOrdemProducao: TMySQLQuery;
    DsOVfOrdemProducao: TDataSource;
    QrOVfOrdemProducaoNrReduzidoOP: TIntegerField;
    QrOVfOrdemProducaoEmpresa: TIntegerField;
    QrOVfOrdemProducaoCiclo: TIntegerField;
    QrOVfOrdemProducaoNrOP: TIntegerField;
    QrOVfOrdemProducaoLocal: TIntegerField;
    QrOVfOrdemProducaoProduto: TIntegerField;
    QrOVfOrdemProducaoSeqGrupo: TIntegerField;
    QrOVfOrdemProducaoTipoOP: TIntegerField;
    QrOVfOrdemProducaoPrioridade: TIntegerField;
    QrOVfOrdemProducaoNrSituacaoOP: TIntegerField;
    QrOVfOrdemProducaoDtInclusao: TDateField;
    QrOVfOrdemProducaoDtPrevisao: TDateField;
    QrOVfOrdemProducaoTipoLocalizacao: TIntegerField;
    QrOVfOrdemProducaoDtEntrada: TDateField;
    QrOVfOrdemProducaoQtReal: TFloatField;
    QrOVfOrdemProducaoQtLocal: TFloatField;
    QrOVfOrdemProducaoCodCategoria: TIntegerField;
    QrOVfOrdemProducaoNrLote: TIntegerField;
    QrOVfOrdemProducaoNrTipoProducaoOP: TWideStringField;
    QrOVfOrdemProducaoDtPrevRet: TDateField;
    QrOVfOrdemProducaoCodPessoa: TIntegerField;
    QrOVfOrdemProducaoReInsrt: TIntegerField;
    QrOVfOrdemProducaoDestino: TWideStringField;
    QrOVfOrdemProducaoLk: TIntegerField;
    QrOVfOrdemProducaoDataCad: TDateField;
    QrOVfOrdemProducaoDataAlt: TDateField;
    QrOVfOrdemProducaoUserCad: TIntegerField;
    QrOVfOrdemProducaoUserAlt: TIntegerField;
    QrOVfOrdemProducaoAlterWeb: TSmallintField;
    QrOVfOrdemProducaoAWServerID: TIntegerField;
    QrOVfOrdemProducaoAWStatSinc: TSmallintField;
    QrOVfOrdemProducaoAtivo: TSmallintField;
    QrOVfOrdemProducaoLastInsrt: TDateTimeField;
    QrOVfOrdemProducaoNO_Local: TWideStringField;
    QrOVfOrdemProducaoDLO_Externo: TWideStringField;
    QrOVfOrdemProducaoNO_CodCategoria: TWideStringField;
    QrOVfOrdemProducaoNO_Referencia: TWideStringField;
    QrOVfOrdemProducaoCodGrade: TIntegerField;
    QrOVfOrdemProducaoCodTam: TWideStringField;
    QrOVfOrdemProducaoNO_Lote: TWideStringField;
    QrOVfOrdemProducaoNO_NrSituacaOP: TWideStringField;
    QrOVfOrdemProducaoNO_TipoLocalizacao: TWideStringField;
    QrOVfOrdemProducaoNO_TipoOP: TWideStringField;
    QrOVfOrdemProducaoNO_NrTipoProducaoOP: TWideStringField;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrOVdCicloAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrOVdCicloBeforeOpen(DataSet: TDataSet);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure QrOVdCicloAfterScroll(DataSet: TDataSet);
    procedure CabInclui1Click(Sender: TObject);
    procedure CabAltera1Click(Sender: TObject);
    procedure CabExclui1Click(Sender: TObject);
    procedure BtCabClick(Sender: TObject);
    procedure BtItsClick(Sender: TObject);
    procedure ItsInclui1Click(Sender: TObject);
    procedure ItsExclui1Click(Sender: TObject);
    procedure ItsAltera1Click(Sender: TObject);
    procedure PMCabPopup(Sender: TObject);
    procedure PMItsPopup(Sender: TObject);
    procedure QrOVdCicloBeforeClose(DataSet: TDataSet);
    procedure EdLocOPRedefinido(Sender: TObject);
    procedure RGOrdem1Click(Sender: TObject);
    procedure RGOrdem2Click(Sender: TObject);
    procedure RGOrdem3Click(Sender: TObject);
    procedure RGOrdem4Click(Sender: TObject);
    procedure QrLocalAfterScroll(DataSet: TDataSet);
    procedure QrNrOPAfterScroll(DataSet: TDataSet);
    procedure QrNrReduzidoOPAfterScroll(DataSet: TDataSet);
    procedure QrEmpresaAfterScroll(DataSet: TDataSet);
    procedure QrLocalBeforeClose(DataSet: TDataSet);
    procedure QrNrOPBeforeClose(DataSet: TDataSet);
    procedure QrNrReduzidoOPBeforeClose(DataSet: TDataSet);
    procedure QrEmpresaBeforeClose(DataSet: TDataSet);
    procedure BtConfigClick(Sender: TObject);
  private
    Panels: array of TPanel;
    Queryes: array of TmySQLQuery;
    //
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure LocCod(Atual, Codigo: Integer);
    procedure Va(Para: TVaiPara);
    procedure MostraCadastro_Com_Itens_ITS(SQLType: TSQLType);

  public
    { Public declarations }
    FSeq, FCabIni, MaxTabIndex, FMaxQrIdx: Integer;
    FLocIni: Boolean;
    FPn1, FPn2, FPn3, FPn4: TPanel;
    //
    procedure CloseQueryByPanel(Panel: TPanel; TabIndex: Integer);
    function  Get_AND(TabIndex: Integer): String;
    procedure HideAllPanels(AfterPanel: Integer);
    procedure ReopenNrOP(TabIndex, NrOP: Integer);
    procedure ReopenNrReduzidoOP(TabIndex, NrReduzidoOP: Integer);
    procedure ReopenEmpresa(TabIndex, Empresa: Integer);
    procedure ReopenLocal(TabIndex, Local: Integer);
    procedure ReopenByIndex(TabIndex: Integer);
    procedure ReopenOVfOrdemProducao(TabIndex: Integer);
    procedure CloseByIndex(TabIndex: Integer);
    procedure SetPanel(var Panel: TPanel; TabIndex: Integer);

  end;

var
  FmOVdCiclo: TFmOVdCiclo;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, MyDBCheck, DmkDAC_PF;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmOVdCiclo.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmOVdCiclo.MostraCadastro_Com_Itens_ITS(SQLType: TSQLType);
begin
{
  if DBCheck.CriaFm(TFmCadastro_Com_Itens_ITS, FmCadastro_Com_Itens_ITS, afmoNegarComAviso) then
  begin
    FmCadastro_Com_Itens_ITS.ImgTipo.SQLType := SQLType;
    FmCadastro_Com_Itens_ITS.FQrCab := QrOVdCiclo;
    FmCadastro_Com_Itens_ITS.FDsCab := DsOVdCiclo;
    FmCadastro_Com_Itens_ITS.FQrIts := QrNrOP;
    if SQLType = stIns then
      FmCadastro_Com_Itens_ITS.EdCPF1.ReadOnly := False
    else
    begin
      FmCadastro_Com_Itens_ITS.EdControle.ValueVariant := QrNrOPControle.Value;
      //
      FmCadastro_Com_Itens_ITS.EdCPF1.Text := MLAGeral.FormataCNPJ_TFT(QrNrOPCNPJ_CPF.Value);
      FmCadastro_Com_Itens_ITS.EdNomeEmiSac.Text := QrNrOPNome.Value;
      FmCadastro_Com_Itens_ITS.EdCPF1.ReadOnly := True;
    end;
    FmCadastro_Com_Itens_ITS.ShowModal;
    FmCadastro_Com_Itens_ITS.Destroy;
  end;
}
end;

procedure TFmOVdCiclo.PMCabPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemCabUpd(CabAltera1, QrOVdCiclo);
  MyObjects.HabilitaMenuItemCabDel(CabExclui1, QrOVdCiclo, QrNrOP);
end;

procedure TFmOVdCiclo.PMItsPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(ItsInclui1, QrOVdCiclo);
  MyObjects.HabilitaMenuItemItsUpd(ItsAltera1, QrNrOP);
  MyObjects.HabilitaMenuItemItsDel(ItsExclui1, QrNrOP);
end;

procedure TFmOVdCiclo.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrOVdCicloCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmOVdCiclo.DefParams;
begin
  VAR_GOTOTABELA := 'ovdciclo';
  VAR_GOTOMYSQLTABLE := QrOVdCiclo;
  VAR_GOTONEG := gotoPiZ;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT ode.CodPessoa CP_Empresa, ode.Nome NO_Empresa,  ');
  VAR_SQLx.Add('ode.GrupoEmpresa, ode.Fantasia, ode.CNPJ_CPF,  ');
  VAR_SQLx.Add('IF(dci.DataIni  <= "1899-12-30", "",  DATE_FORMAT(dci.DataIni, "%d/%m/%Y")) DataIni_TXT,   ');
  VAR_SQLx.Add('IF(dci.DataFim  <= "1899-12-30", "",  DATE_FORMAT(dci.DataFim, "%d/%m/%Y")) DataFim_TXT,  ');
  VAR_SQLx.Add('dci.*  ');
  VAR_SQLx.Add('FROM ovdciclo dci  ');
  VAR_SQLx.Add('LEFT JOIN ovdempresa ode ON ode.Codigo=dci.Empresa  ');
  VAR_SQLx.Add('WHERE dci.Codigo > 0  ');
  //
  VAR_SQL1.Add('AND dci.Codigo=:P0');
  //
  //VAR_SQL2.Add('AND CodUsu=:P0');
  //
  VAR_SQLa.Add('AND dci.Nome Like :P0');
  //
end;

procedure TFmOVdCiclo.EdLocOPRedefinido(Sender: TObject);
var
  NrOP: Integer;
begin
  NrOP := EdLocOP.ValueVariant;
  if QrNrOP.State <> dsInactive then
    QrNrOP.Locate('NrOP', NrOP, []);
end;

procedure TFmOVdCiclo.ItsAltera1Click(Sender: TObject);
begin
  MostraCadastro_Com_Itens_ITS(stUpd);
end;

procedure TFmOVdCiclo.CabExclui1Click(Sender: TObject);
begin
  Geral.MB_Info('A��o n�o implementada! Solicite � Dermatek:' + sLineBreak +
  Caption + sLineBreak + TMenuItem(Sender).Name);
end;

procedure TFmOVdCiclo.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmOVdCiclo.QueryPrincipalAfterOpen;
begin
end;

procedure TFmOVdCiclo.ItsExclui1Click(Sender: TObject);
var
  Controle: Integer;
begin
{
  if UMyMod.ExcluiRegistroInt1('Confirma a exclus�o do item selecionado?',
  'Cadastro_Com_Itens_ITS', 'Controle', QrNrOPControle.Value, Dmod.MyDB?) = ID_YES then
  begin
    Controle := GOTOy.LocalizaPriorNextIntQr(QrNrOP,
      QrNrOPControle, QrNrOPControle.Value);
    ReopenNrOP(Controle);
  end;
}
end;

procedure TFmOVdCiclo.ReopenByIndex(TabIndex: Integer);
var
  RadioGroup: TRadioGroup;
  Panel: TPanel;
begin
  if PnConfig.Visible then
    Exit;
  case TabIndex of
    1: RadioGroup := RGOrdem1;
    2: RadioGroup := RGOrdem2;
    3: RadioGroup := RGOrdem3;
    4: RadioGroup := RGOrdem4;
    else RadioGroup := nil;
  end;
  if RadioGroup <> nil then
  begin
    case RadioGroup.ItemIndex of
      0: ReopenLocal(TabIndex, 0);
      1: ReopenNrOP(TabIndex, 0);
      2: ReopenEmpresa(TabIndex, 0);
      3: ReopenNrReduzidoOP(TabIndex, 0);
    end;
  end;
  //if TabIndex = MaxTabIndex then
  if (TabIndex = FMaxQrIdx) or (FMaxQrIdx = 0) then
    ReopenOVfOrdemProducao(TabIndex);
end;

procedure TFmOVdCiclo.ReopenEmpresa(TabIndex, Empresa: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrEmpresa, Dmod.MyDB, [
  'SELECT DISTINCT fop.Empresa',
  'FROM ovfordemproducao fop ',
  'WHERE Ciclo=' + Geral.FF0(QrOVdCicloCodigo.Value),
  Get_AND(TabIndex),
  'ORDER BY Empresa',
  '']);
  //Geral.MB_SQL(Self, QrEmpresa);
end;

procedure TFmOVdCiclo.ReopenLocal(TabIndex, Local: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrLocal, Dmod.MyDB, [
  'SELECT DISTINCT fop.Local, odl.Nome NO_Local',
  'FROM ovfordemproducao fop ',
  'LEFT JOIN ovdlocal odl ON odl.Codigo=fop.Local',
  'WHERE Ciclo=' + Geral.FF0(QrOVdCicloCodigo.Value),
  Get_AND(TabIndex),
  'ORDER BY NO_Local',
  '']);
  //Geral.MB_SQL(Self, QrLocal);
end;

procedure TFmOVdCiclo.ReopenNrOP(TabIndex, NrOP: Integer);
begin
(*
  UnDmkDAC_PF.AbreMySQLQuery0(QrNrOP, Dmod.MyDB, [
  'SELECT NrOP, MIN(DtInclusao) MIN_DtInclusao ',
  'FROM ovfordemproducao fop ',
  'WHERE Ciclo=' + Geral.FF0(QrOVdCicloCodigo.Value),
  'GROUP BY NrOP ',
  'ORDER BY NrOP ',
  '']);
*)
  UnDmkDAC_PF.AbreMySQLQuery0(QrNrOP, Dmod.MyDB, [
  'SELECT DISTINCT NrOP ',
  'FROM ovfordemproducao fop ',
  'WHERE Ciclo=' + Geral.FF0(QrOVdCicloCodigo.Value),
  Get_AND(TabIndex),
  'ORDER BY NrOP ',
  '']);
  //
  QrNrOP.Locate('NrOP', NrOP, []);
  //Geral.MB_SQL(Self, QrNrOP);
end;


procedure TFmOVdCiclo.ReopenNrReduzidoOP(TabIndex, NrReduzidoOP: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrNrReduzidoOP, Dmod.MyDB, [
  'SELECT DISTINCT NrReduzidoOP ',
  'FROM ovfordemproducao fop ',
  'WHERE Ciclo=' + Geral.FF0(QrOVdCicloCodigo.Value),
   Get_AND(TabIndex),
  'ORDER BY NrReduzidoOP ',
  '']);
  //
  QrNrReduzidoOP.Locate('NrReduzidoOP', NrReduzidoOP, []);
  //Geral.MB_SQL(Self, QrNrReduzidoOP);
end;

procedure TFmOVdCiclo.ReopenOVfOrdemProducao(TabIndex: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrOVfOrdemProducao, Dmod.MyDB, [
  'SELECT fop.*, dlo.Nome NO_Local, dlo.Externo DLO_Externo, ',
  'dco.Nome NO_CodCategoria, ref.Nome NO_Referencia, ',
  'prd.CodGrade, prd.CodTam, lot.Nome NO_Lote,  ',
  'nso.Nome NO_NrSituacaOP, tlo.Nome NO_TipoLocalizacao, ',
  'top.Nome NO_TipoOP, tpo.Nome NO_NrTipoProducaoOP ',
  'FROM ovfordemproducao fop ',
  'LEFT JOIN ovdlocal dlo ON dlo.Codigo=fop.Local ',
  'LEFT JOIN ovdclaslocal dcl ON dcl.Codigo=fop.Local ',
  'LEFT JOIN ovdcodcategoria dco ON dco.Codigo=fop.CodCategoria ',
  'LEFT JOIN ovdproduto prd ON prd.Controle=fop.Produto ',
  'LEFT JOIN ovdreferencia ref ON ref.Codigo=prd.Codigo ',
  'LEFT JOIN ovdlote lot ON lot.Codigo=fop.NrLote ',
  'LEFT JOIN ovdnrsituacaoop nso ON nso.Codigo=fop.NrSituacaoOP ',
  'LEFT JOIN ovdtipolocalizacao tlo ON tlo.Codigo=fop.TipoLocalizacao ',
  'LEFT JOIN ovdtipoop top ON top.Codigo=fop.TipoOP ',
  'LEFT JOIN ovdtipoproducaoop tpo ON tpo.CodTxt=fop.NrTipoProducaoOP ',
  'WHERE Ciclo=' + Geral.FF0(QrOVdCicloCodigo.Value),
   Get_AND(TabIndex + 1),
  '']);
  //
  //Geral.MB_SQL(Self, QrOVfOrdemProducao);
end;

procedure TFmOVdCiclo.RGOrdem1Click(Sender: TObject);
begin
  if MaxTabIndex < 1 then
    MaxTabIndex := 1;
  if MaxTabIndex = 1 then
    FMaxQrIdx := RGOrdem1.ItemIndex;
  //
  HideAllPanels(1);
  RGOrdem4.ItemIndex := -1;
  RGOrdem3.ItemIndex := -1;
  RGOrdem2.ItemIndex := -1;
  if FPn1 <> nil then
  begin
    case FPn1.Tag of
      1: QrLocal.Close;
      2: QrNrOP.Close;
      3: QrEmpresa.Close;
      4: QrNrReduzidoOP.Close;
    end;
  end;
  SetPanel(FPn1, RGOrdem1.ItemIndex);
  FPn1.Tag := 1;
  FPn1.Align := alRight;
  FPn1.Align := alLeft;
  FPn1.Visible := True;
  ReopenByIndex(1);
  RGOrdem2.Enabled := True;
end;

procedure TFmOVdCiclo.RGOrdem2Click(Sender: TObject);
begin
  if MaxTabIndex < 2 then
    MaxTabIndex := 2;
  if MaxTabIndex = 2 then
    FMaxQrIdx := RGOrdem2.ItemIndex;
  //
  RGOrdem1.Enabled := False;
  if FPn2 <> nil then
  begin
    CloseQueryByPanel(FPn2, 2);
    FPn2.Visible := False;
  end;
  SetPanel(FPn2, RGOrdem2.ItemIndex);
  if (FPn2 <> nil) and (RGOrdem2.ItemIndex >= 0) then
  begin
    FPn2.Tag := 2;
    FPn2.Visible := True;
    FPn2.Align := alRight;
    FPn2.Align := alLeft;
    //FPn1.Align := alRight;
    //FPn1.Align := alLeft;
    ReopenByIndex(2);
  end;
  RGOrdem3.Enabled := True;
end;

procedure TFmOVdCiclo.RGOrdem3Click(Sender: TObject);
begin
  if MaxTabIndex < 3 then
    MaxTabIndex := 3;
  if MaxTabIndex = 3 then
    FMaxQrIdx := RGOrdem3.ItemIndex;
  //
  RGOrdem2.Enabled := False;
  if FPn3 <> nil then
  begin
    CloseQueryByPanel(FPn3, 3);
    FPn3.Visible := False;
  end;
  SetPanel(FPn3, RGOrdem3.ItemIndex);
  if (FPn3 <> nil) and (RGOrdem3.ItemIndex >= 0) then
  begin
    FPn3.Tag := 3;
    FPn3.Visible := True;
    FPn3.Align := alRight;
    FPn3.Align := alLeft;
    //FPn1.Align := alRight;
    //FPn1.Align := alLeft;
    ReopenByIndex(3);
  end;
  RGOrdem4.Enabled := True;
end;

procedure TFmOVdCiclo.RGOrdem4Click(Sender: TObject);
begin
  if MaxTabIndex < 4 then
    MaxTabIndex := 4;
  if MaxTabIndex = 4 then
    FMaxQrIdx := RGOrdem4.ItemIndex;
  //
  RGOrdem3.Enabled := True;
  if FPn4 <> nil then
  begin
    CloseQueryByPanel(FPn4, 4);
    FPn4.Visible := False;
  end;
  SetPanel(FPn4, RGOrdem4.ItemIndex);
  if (FPn4 <> nil) and (RGOrdem4.ItemIndex >= 0) then
  begin
    FPn4.Tag := 4;
    FPn4.Visible := True;
    FPn4.Align := alRight;
    FPn4.Align := alLeft;
    //FPn1.Align := alRight;
    //FPn1.Align := alLeft;
    ReopenByIndex(4);
  end;
  //RGOrdem5.Enabled := True;
end;

procedure TFmOVdCiclo.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmOVdCiclo.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmOVdCiclo.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmOVdCiclo.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmOVdCiclo.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmOVdCiclo.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmOVdCiclo.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrOVdCicloCodigo.Value;
  Close;
end;

procedure TFmOVdCiclo.ItsInclui1Click(Sender: TObject);
begin
  MostraCadastro_Com_Itens_ITS(stIns);
end;

procedure TFmOVdCiclo.CabAltera1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrOVdCiclo, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'ovdciclo');
end;

procedure TFmOVdCiclo.BtConfigClick(Sender: TObject);
begin
  PnConfig.Hide;
  ReopenByIndex(1);
end;

procedure TFmOVdCiclo.BtConfirmaClick(Sender: TObject);
var
  Codigo: Integer;
  Nome: String;
begin

{
  Nome := EdNome.ValueVariant;
  if MyObjects.FIC(Length(Nome) = 0, EdNome, 'Defina uma descri��o!') then Exit;
  //
  ?Codigo := UMyMod.BuscaEmLivreY_Def('ovdciclo', 'Codigo', ImgTipo.SQLType,
    QrOVdCicloCodigo.Value);
  ou > ?Codigo := UMyMod.BPGS1I32('ovdciclo', 'Codigo', '', '',
    tsPosNeg?, ImgTipo.SQLType, QrOVdCicloCodigo.Value);
  if UMyMod.ExecSQLInsUpdPanel(ImgTipo.SQLType, Self, PnEdita, 'ovdciclo',
  Codigo, Dmod.QrUpd, [PnEdita], [PnDados], ImgTipo, True) then
  begin
(*  Desmarcar se usar "UMyMod.SQLInsUpd(...)" em vez de "UMyMod.ExecSQLInsUpdPanel(...)"
    ImgTipo.SQLType := stLok;
    PnDados.Visible := True;
    PnEdita.Visible := False;
    GOTOy.BotoesSb(ImgTipo.SQLType);
*)
    LocCod(Codigo, Codigo);
    if FSeq = 1 then Close;
  end;
}
end;

procedure TFmOVdCiclo.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := EdCodigo.ValueVariant;
  if ImgTipo.SQLType = stIns then
    UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'ovdciclo', Codigo);
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'ovdciclo', 'Codigo');
end;

procedure TFmOVdCiclo.BtItsClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMIts, BtIts);
end;

procedure TFmOVdCiclo.BtCabClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMCab, BtCab);
end;

procedure TFmOVdCiclo.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  GBEdita.Align := alClient;
  DGDados.Align := alClient;
  PnGrids.Align := alClient;
  MaxTabIndex := 0;
  //
  SetLength(Panels, 5);
  Panels[0] := PnLocal;
  Panels[1] := PnNrOP;
  Panels[2] := PnEMpresa;
  Panels[3] := PnNrReduzidoOP;
  Panels[4] := nil;
  //
  FPn1 := nil;
  FPn2 := nil;
  FPn3 := nil;
  FPn4 := nil;
  //
  SetLength(Queryes, 5);
  Queryes[0] := QrLocal;
  Queryes[1] := QrNrOP;
  Queryes[2] := QrEMpresa;
  Queryes[3] := QrNrReduzidoOP;
  Queryes[4] := nil;
  //
  CriaOForm;
  FSeq := 0;
end;

procedure TFmOVdCiclo.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrOVdCicloCodigo.Value, LaRegistro.Caption);
end;

procedure TFmOVdCiclo.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmOVdCiclo.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrOVdCicloCodigo.Value, LaRegistro.Caption);
end;

procedure TFmOVdCiclo.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmOVdCiclo.QrEmpresaAfterScroll(DataSet: TDataSet);
begin
  ReopenByIndex(PnEmpresa.Tag + 1);
  if FMaxQrIdx + 1 = 3 then
    ReopenOVfOrdemProducao(3);
end;

procedure TFmOVdCiclo.QrEmpresaBeforeClose(DataSet: TDataSet);
begin
  CloseByIndex(PnEmpresa.Tag + 1);
end;

procedure TFmOVdCiclo.QrLocalAfterScroll(DataSet: TDataSet);
begin
  ReopenByIndex(PnLocal.Tag + 1);
  if FMaxQrIdx + 1 = 1 then
    ReopenOVfOrdemProducao(1);
end;

procedure TFmOVdCiclo.QrLocalBeforeClose(DataSet: TDataSet);
begin
  CloseByIndex(PnLocal.Tag + 1);
end;

procedure TFmOVdCiclo.QrNrOPAfterScroll(DataSet: TDataSet);
begin
  ReopenByIndex(PnNrOP.Tag + 1);
  if FMaxQrIdx + 1 = 2 then
    ReopenOVfOrdemProducao(2);
end;

procedure TFmOVdCiclo.QrNrOPBeforeClose(DataSet: TDataSet);
begin
  CloseByIndex(PnNrOP.Tag + 1);
end;

procedure TFmOVdCiclo.QrNrReduzidoOPAfterScroll(DataSet: TDataSet);
begin
  ReopenByIndex(PnNrReduzidoOP.Tag + 1);
  if FMaxQrIdx + 1 = 4 then
    ReopenOVfOrdemProducao(4);
end;

procedure TFmOVdCiclo.QrNrReduzidoOPBeforeClose(DataSet: TDataSet);
begin
  CloseByIndex(PnNrReduzidoOP.Tag + 1);
end;

procedure TFmOVdCiclo.QrOVdCicloAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmOVdCiclo.QrOVdCicloAfterScroll(DataSet: TDataSet);
begin
  ReopenByIndex(1);
end;

procedure TFmOVdCiclo.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //
  if FSeq = 1 then
    CabInclui1Click(Self)
  else
  if (not FLocIni) and (FCabIni <> 0)  then
  begin
    LocCod(FCabIni, FCabIni);
    if QrOVdCicloCodigo.Value <> FCabIni then
      Geral.MB_Aviso('Grupo n�o localizado!');
    FLocIni := True;
  end;
end;

procedure TFmOVdCiclo.SbQueryClick(Sender: TObject);
begin
  LocCod(QrOVdCicloCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'ovdciclo', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmOVdCiclo.SetPanel(var Panel: TPanel; TabIndex: Integer);
begin
  if (TabIndex >= 0) or (TabIndex <= 3) then
    Panel := Panels[TabIndex]
  else
    Panel := nil;
end;

procedure TFmOVdCiclo.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

function TFmOVdCiclo.Get_AND(TabIndex: Integer): String;
const
  Local   = 0;
  Nr_OP   = 1;
  Empresa = 2;
  Red_OP  = 3;
  //
  function GetLocal(): String;
  begin
    Result := ' AND fop.Local=' + Geral.FF0(QrLocalLocal.Value) + sLineBreak;
  end;
  function GetNrOP(): String;
  begin
    Result := ' AND fop.NrOP=' + Geral.FF0(QrNrOPNrOP.Value) + sLineBreak;
  end;
  function GetEmpresa(): String;
  begin
    Result := ' AND fop.Empresa=' + Geral.FF0(QrEmpresaEmpresa.Value) + sLineBreak;
  end;
  function GetRed_OP(): String;
  begin
    Result := ' AND fop.NrReduzidoOP=' + Geral.FF0(QrNrReduzidoOPNrReduzidoOP.Value) + sLineBreak;
  end;
var
  I: Integer;
  RadioGroup: TRadioGroup;
begin
  Result := '';
  for I := 0 to TabIndex - 2 do
  begin
    case I of
      0: RadioGroup := RGOrdem1;
      1: RadioGroup := RGOrdem2;
      2: RadioGroup := RGOrdem3;
      3: RadioGroup := RGOrdem4;
      else RadioGroup := nil;
    end;
    if RadioGroup <> nil then
    begin
      case RadioGroup.ItemIndex of
        Local   : Result := Result + GetLocal();
        Nr_OP   : Result := Result + GetNrOP();
        Empresa : Result := Result + GetEmpresa();
        Red_OP  : Result := Result + GetRed_OP();
      end;
    end;
  end;
end;

procedure TFmOVdCiclo.HideAllPanels(AfterPanel: Integer);
begin
  //Exit;
  PnLocal.Visible := False;
  PnNrOP.Visible := False;
  PnEmpresa.Visible := False;
  PnNrReduzidoOP.Visible := False;
end;

procedure TFmOVdCiclo.CabInclui1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrOVdCiclo, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'ovdciclo');
end;

procedure TFmOVdCiclo.CloseByIndex(TabIndex: Integer);
var
  RadioGroup: TRadioGroup;
  Panel: TPanel;
begin
  case TabIndex of
    1: RadioGroup := RGOrdem1;
    2: RadioGroup := RGOrdem2;
    3: RadioGroup := RGOrdem3;
    4: RadioGroup := RGOrdem4;
    else RadioGroup := nil;
  end;
  if RadioGroup <> nil then
  begin
    case RadioGroup.ItemIndex of
      0: QrLocal.Close;
      1: QrNrOP.Close;
      2: QrEmpresa.Close;
      3: QrNrReduzidoOP.Close;
    end;
  end;
end;

procedure TFmOVdCiclo.CloseQueryByPanel(Panel: TPanel; TabIndex: Integer);
var
  Query: TmySQLQuery;
  NxtPn: TPanel;
begin
  if (Panel.Tag >=1) and (Panel.Tag <=4) then
    Query := Queryes[Panel.Tag]
  else Query := nil;
  if Query <> nil then
    Query.Close;
  //
  case TabIndex of
    1: NxtPn := FPn2;
    2: NxtPn := FPn3;
    3: NxtPn := FPn4;
    else NxtPn := nil;
  end;
  if NxtPn <> nil then
    CloseQueryByPanel(NxtPn, TabIndex + 1);
end;

procedure TFmOVdCiclo.QrOVdCicloBeforeClose(
  DataSet: TDataSet);
begin
  QrNrOP.Close;
  QrLocal.Close;
  QrEmpresa.Close;
  QrNrReduzidoOP.Close;
  //
  PnNrOP.Visible := False;
  PnLocal.Visible := False;
  PnEmpresa.Visible := False;
  PnNrReduzidoOP.Visible := False;
  //
  PnNrOP.Align := alNone;
  PnLocal.Align := alNone;
  PnEmpresa.Align := alNone;
  PnNrReduzidoOP.Align := alNone;
end;

procedure TFmOVdCiclo.QrOVdCicloBeforeOpen(DataSet: TDataSet);
begin
  QrOVdCicloCodigo.DisplayFormat := FFormatFloat;
end;

end.

