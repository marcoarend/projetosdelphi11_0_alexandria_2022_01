object FmOVgIspPrfCab: TFmOVgIspPrfCab
  Left = 368
  Top = 194
  Caption = 'OVS-INSPE-006 :: Perfis de Inspe'#231#245'es de Fac'#231#245'es'
  ClientHeight = 639
  ClientWidth = 1008
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnEdita: TPanel
    Left = 0
    Top = 96
    Width = 1008
    Height = 543
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    Visible = False
    object GBEdita: TGroupBox
      Left = 0
      Top = 0
      Width = 1008
      Height = 125
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 0
      object Label7: TLabel
        Left = 16
        Top = 16
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label9: TLabel
        Left = 76
        Top = 16
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
        Color = clBtnFace
        ParentColor = False
      end
      object EdCodigo: TdmkEdit
        Left = 16
        Top = 32
        Width = 56
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Codigo'
        UpdCampo = 'Codigo'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdNome: TdmkEdit
        Left = 76
        Top = 32
        Width = 633
        Height = 21
        TabOrder = 1
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Nome'
        UpdCampo = 'Nome'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
    end
    object GBConfirma: TGroupBox
      Left = 0
      Top = 480
      Width = 1008
      Height = 63
      Align = alBottom
      TabOrder = 1
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 12
        Top = 17
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
      end
      object Panel1: TPanel
        Left = 868
        Top = 15
        Width = 138
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 7
          Top = 2
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
    end
  end
  object PnDados: TPanel
    Left = 0
    Top = 96
    Width = 1008
    Height = 543
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    object GBDados: TGroupBox
      Left = 0
      Top = 0
      Width = 1008
      Height = 205
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 1
      object Panel6: TPanel
        Left = 2
        Top = 15
        Width = 1004
        Height = 78
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 0
        object Label1: TLabel
          Left = 8
          Top = 0
          Width = 36
          Height = 13
          Caption = 'C'#243'digo:'
          FocusControl = DBEdCodigo
        end
        object Label2: TLabel
          Left = 68
          Top = 0
          Width = 51
          Height = 13
          Caption = 'Descri'#231#227'o:'
          FocusControl = DBEdNome
        end
        object Label20: TLabel
          Left = 752
          Top = 40
          Width = 223
          Height = 13
          Caption = 'Plano de Amostragem e Regime de Qualidade :'
        end
        object Label6: TLabel
          Left = 8
          Top = 40
          Width = 30
          Height = 13
          Caption = 'Artigo:'
        end
        object Label8: TLabel
          Left = 68
          Top = 40
          Width = 95
          Height = 13
          Caption = 'Descri'#231#227'o do artigo:'
        end
        object Label3: TLabel
          Left = 804
          Top = 0
          Width = 55
          Height = 13
          Caption = 'Refer'#234'ncia:'
          FocusControl = DBEdit1
        end
        object DBEdCodigo: TdmkDBEdit
          Left = 8
          Top = 16
          Width = 56
          Height = 21
          TabStop = False
          DataField = 'Codigo'
          DataSource = DsOVgIspPrfCab
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ReadOnly = True
          ShowHint = True
          TabOrder = 0
          UpdType = utYes
          Alignment = taRightJustify
        end
        object DBEdNome: TdmkDBEdit
          Left = 68
          Top = 16
          Width = 681
          Height = 21
          Color = clWhite
          DataField = 'Nome'
          DataSource = DsOVgIspPrfCab
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          UpdType = utYes
          Alignment = taLeftJustify
        end
        object dmkDBEdit7: TdmkDBEdit
          Left = 752
          Top = 56
          Width = 49
          Height = 21
          Color = clWhite
          DataField = 'OVcYnsARQ'
          DataSource = DsOVgIspPrfCab
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          UpdType = utYes
          Alignment = taLeftJustify
        end
        object dmkDBEdit8: TdmkDBEdit
          Left = 804
          Top = 56
          Width = 193
          Height = 21
          Color = clWhite
          DataField = 'NO_OVcYnsARQ'
          DataSource = DsOVgIspPrfCab
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          UpdType = utYes
          Alignment = taLeftJustify
        end
        object DBEdit4: TDBEdit
          Left = 8
          Top = 56
          Width = 56
          Height = 21
          DataField = 'SeqGrupo'
          DataSource = DsOVgIspPrfCab
          TabOrder = 4
        end
        object DBEdit5: TDBEdit
          Left = 68
          Top = 56
          Width = 681
          Height = 21
          DataField = 'NO_Referencia'
          DataSource = DsOVgIspPrfCab
          TabOrder = 5
        end
        object DBEdit1: TDBEdit
          Left = 804
          Top = 16
          Width = 189
          Height = 21
          DataField = 'Referencia'
          DataSource = DsOVgIspPrfCab
          TabOrder = 6
        end
        object DBCheckBox1: TDBCheckBox
          Left = 752
          Top = 20
          Width = 49
          Height = 17
          Caption = 'Ativo'
          DataField = 'Ativo'
          DataSource = DsOVgIspPrfCab
          TabOrder = 7
          ValueChecked = '1'
          ValueUnchecked = '0'
        end
      end
      object GroupBox3: TGroupBox
        Left = 2
        Top = 93
        Width = 1004
        Height = 110
        Align = alClient
        Caption = ' Configura'#231#245'es de Inspe'#231#227'o: '
        TabOrder = 1
        TabStop = True
        object Panel7: TPanel
          Left = 2
          Top = 15
          Width = 1000
          Height = 93
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          TabStop = True
          object Label11: TLabel
            Left = 8
            Top = 4
            Width = 93
            Height = 13
            Caption = 'Tabela de medidas:'
          end
          object Label12: TLabel
            Left = 472
            Top = 4
            Width = 147
            Height = 13
            Caption = 'Check list de inconformidades: '
          end
          object Label14: TLabel
            Left = 844
            Top = 4
            Width = 68
            Height = 13
            Caption = 'L.p'#231'.tab.med: '
          end
          object Label16: TLabel
            Left = 920
            Top = 4
            Width = 60
            Height = 13
            Caption = 'L.p'#231'.chklist: '
          end
          object DBEdit7: TDBEdit
            Left = 8
            Top = 20
            Width = 56
            Height = 21
            DataField = 'OVcYnsMed'
            DataSource = DsOVgIspPrfCab
            TabOrder = 0
          end
          object DBEdit8: TDBEdit
            Left = 472
            Top = 20
            Width = 56
            Height = 21
            DataField = 'OVcYnsChk'
            DataSource = DsOVgIspPrfCab
            TabOrder = 1
          end
          object DBEdit9: TDBEdit
            Left = 68
            Top = 20
            Width = 400
            Height = 21
            DataField = 'NO_OVcYnsMed'
            DataSource = DsOVgIspPrfCab
            TabOrder = 2
          end
          object DBEdit10: TDBEdit
            Left = 532
            Top = 20
            Width = 309
            Height = 21
            DataField = 'NO_OVcYnsChk'
            DataSource = DsOVgIspPrfCab
            TabOrder = 3
          end
          object DBEdit11: TDBEdit
            Left = 920
            Top = 20
            Width = 72
            Height = 21
            DataField = 'LimiteChk'
            DataSource = DsOVgIspPrfCab
            TabOrder = 4
          end
          object DBEdit12: TDBEdit
            Left = 844
            Top = 20
            Width = 72
            Height = 21
            DataField = 'LimiteMed'
            DataSource = DsOVgIspPrfCab
            TabOrder = 5
          end
          object DBRadioGroup1: TDBRadioGroup
            Left = 8
            Top = 44
            Width = 985
            Height = 45
            Caption = ' Permiss'#227'o de finaliza'#231#227'o da inspe'#231#227'o: '
            Columns = 4
            DataField = 'PermiFinHow'
            DataSource = DsOVgIspPrfCab
            Items.Strings = (
              '0 - Indefinido'
              '1 - Somente ao finalizar toda inspe'#231#227'o'
              '2 - Ao atingir a pontua'#231#227'o de reprova'#231#227'o'
              '3 - A qualquer momento')
            TabOrder = 6
            Values.Strings = (
              '0'
              '1'
              '2'
              '3'
              '4'
              '5'
              '6'
              '7'
              '8'
              '9')
          end
        end
      end
    end
    object GBCntrl: TGroupBox
      Left = 0
      Top = 479
      Width = 1008
      Height = 64
      Align = alBottom
      TabOrder = 0
      object Panel5: TPanel
        Left = 2
        Top = 15
        Width = 172
        Height = 47
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 174
        Top = 15
        Width = 311
        Height = 47
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel3: TPanel
        Left = 485
        Top = 15
        Width = 521
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object Panel2: TPanel
          Left = 388
          Top = 0
          Width = 133
          Height = 47
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 0
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
        object BtCab: TBitBtn
          Tag = 1000076
          Left = 4
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Perfil'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtCabClick
        end
        object BtIts: TBitBtn
          Tag = 110
          Left = 128
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&???'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          Visible = False
          OnClick = BtItsClick
        end
      end
    end
    object PCItens: TPageControl
      Left = 0
      Top = 205
      Width = 1008
      Height = 193
      ActivePage = TabSheet2
      Align = alTop
      TabOrder = 2
      object TabSheet1: TTabSheet
        Caption = ' Tabela de Medidas '
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object PnGrids: TPanel
          Left = 0
          Top = 0
          Width = 1000
          Height = 165
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          object DBGTopico: TDBGrid
            Left = 625
            Top = 0
            Width = 375
            Height = 165
            Align = alClient
            DataSource = DsOVcYnsMedDim
            TabOrder = 0
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            Columns = <
              item
                Expanded = False
                FieldName = 'Conta'
                Title.Caption = 'ID'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'CodGrade'
                Title.Caption = 'C'#243'd. grade'
                Width = 48
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'CodTam'
                Title.Caption = 'C'#243'd. tam'
                Width = 68
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'MedidCer'
                Title.Caption = 'Med. certa'
                Width = 55
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'MedidMin'
                Title.Caption = 'Med. m'#237'nima'
                Width = 55
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'MedidMax'
                Title.Caption = 'Med. m'#225'xima'
                Width = 55
                Visible = True
              end>
          end
          object DBGContexto: TDBGrid
            Left = 0
            Top = 0
            Width = 625
            Height = 165
            Align = alLeft
            DataSource = DsOVcYnsMedTop
            TabOrder = 1
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            Columns = <
              item
                Expanded = False
                FieldName = 'Controle'
                Title.Caption = 'ID'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Tobiko'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NO_TOBIKO'
                Title.Caption = 'Descri'#231#227'o do Contexto'
                Width = 261
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NO_TolerBasCalc'
                Title.Caption = 'Bas.Calc.Toler.'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NO_TolerUnMdida'
                Title.Caption = 'Un.Med.Tol.'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'TolerRndPerc'
                Title.Caption = 'Marg. med/ %:'
                Width = 71
                Visible = True
              end>
          end
        end
      end
      object TabSheet2: TTabSheet
        Caption = 'Check List de Inconformidades'
        ImageIndex = 1
        object Panel8: TPanel
          Left = 0
          Top = 0
          Width = 1000
          Height = 165
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          object DBGrid1: TDBGrid
            Left = 413
            Top = 0
            Width = 587
            Height = 165
            Align = alClient
            DataSource = DsOVcYnsChkTop
            TabOrder = 0
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            Columns = <
              item
                Expanded = False
                FieldName = 'Conta'
                Title.Caption = 'ID'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Topico'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NO_Topico'
                Title.Caption = 'Descri'#231#227'o do T'#243'pico'
                Width = 373
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NO_Magnitude'
                Title.Caption = 'Magnitude'
                Width = 69
                Visible = True
              end>
          end
          object DBGrid2: TDBGrid
            Left = 0
            Top = 0
            Width = 413
            Height = 165
            Align = alLeft
            DataSource = DsOVcYnsChkCtx
            TabOrder = 1
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            Columns = <
              item
                Expanded = False
                FieldName = 'Controle'
                Title.Caption = 'ID'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Contexto'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NO_CONTEXTO'
                Title.Caption = 'Descri'#231#227'o do Contexto'
                Width = 261
                Visible = True
              end>
          end
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 12
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 216
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 216
      Top = 0
      Width = 744
      Height = 52
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 389
        Height = 32
        Caption = 'Perfis de Inspe'#231#245'es de Fac'#231#245'es'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 389
        Height = 32
        Caption = 'Perfis de Inspe'#231#245'es de Fac'#231#245'es'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 389
        Height = 32
        Caption = 'Perfis de Inspe'#231#245'es de Fac'#231#245'es'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 52
    Width = 1008
    Height = 44
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = BtCab
    Left = 120
    Top = 64
  end
  object QrOVgIspPrfCab: TMySQLQuery
    Database = Dmod.ZZDB
    BeforeOpen = QrOVgIspPrfCabBeforeOpen
    AfterOpen = QrOVgIspPrfCabAfterOpen
    BeforeClose = QrOVgIspPrfCabBeforeClose
    AfterScroll = QrOVgIspPrfCabAfterScroll
    SQL.Strings = (
      'SELECT igc.*, ymc.Nome NO_OVcYnsMed, ycc.Nome NO_OVcYnsChk,'
      'dlo.Nome NO_Local, ref.Nome NO_Referencia, ref.Referencia,'
      'isc.Nome NO_ZtatusIsp, '
      'IF(igc.DtHrAbert  <= "1899-12-30", "",  '
      '  DATE_FORMAT(igc.DtHrAbert, "%d/%m/%Y %h:%i")) DtHrAbert_TXT,  '
      'IF(igc.DtHrFecha  <= "1899-12-30", "",  '
      '  DATE_FORMAT(igc.DtHrFecha, "%d/%m/%Y %h:%i")) DtHrFecha_TXT'
      'FROM ovgispgercab igc'
      'LEFT JOIN ovdlocal dlo ON dlo.Codigo=igc.Local  '
      'LEFT JOIN ovdreferencia ref ON ref.Codigo=igc.SeqGrupo '
      'LEFT JOIN ovcynsmedcad ymc ON ymc.Codigo=igc.OVcYnsMed'
      'LEFT JOIN ovcynschkcad ycc ON ycc.Codigo=igc.OVcYnsChk'
      'LEFT JOIN OVgIspStaCad isc ON isc.Codigo=igc.ZtatusIsp'
      ''
      'WHERE igc.Codigo > 0')
    Left = 620
    Top = 5
    object QrOVgIspPrfCabCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrOVgIspPrfCabNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QrOVgIspPrfCabSeqGrupo: TIntegerField
      FieldName = 'SeqGrupo'
      Required = True
    end
    object QrOVgIspPrfCabOVcYnsMed: TIntegerField
      FieldName = 'OVcYnsMed'
      Required = True
    end
    object QrOVgIspPrfCabOVcYnsChk: TIntegerField
      FieldName = 'OVcYnsChk'
      Required = True
    end
    object QrOVgIspPrfCabOVcYnsARQ: TIntegerField
      FieldName = 'OVcYnsARQ'
    end
    object QrOVgIspPrfCabLimiteChk: TIntegerField
      FieldName = 'LimiteChk'
      Required = True
    end
    object QrOVgIspPrfCabLimiteMed: TIntegerField
      FieldName = 'LimiteMed'
      Required = True
    end
    object QrOVgIspPrfCabLk: TIntegerField
      FieldName = 'Lk'
      Required = True
    end
    object QrOVgIspPrfCabDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrOVgIspPrfCabDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrOVgIspPrfCabUserCad: TIntegerField
      FieldName = 'UserCad'
      Required = True
    end
    object QrOVgIspPrfCabUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Required = True
    end
    object QrOVgIspPrfCabAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Required = True
    end
    object QrOVgIspPrfCabAWServerID: TIntegerField
      FieldName = 'AWServerID'
      Required = True
    end
    object QrOVgIspPrfCabAWStatSinc: TSmallintField
      FieldName = 'AWStatSinc'
      Required = True
    end
    object QrOVgIspPrfCabAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
    object QrOVgIspPrfCabNO_Referencia: TWideStringField
      FieldName = 'NO_Referencia'
      Size = 100
    end
    object QrOVgIspPrfCabNO_OVcYnsMed: TWideStringField
      FieldName = 'NO_OVcYnsMed'
      Size = 60
    end
    object QrOVgIspPrfCabNO_OVcYnsChk: TWideStringField
      FieldName = 'NO_OVcYnsChk'
      Size = 60
    end
    object QrOVgIspPrfCabNO_OVcYnsARQ: TWideStringField
      FieldName = 'NO_OVcYnsARQ'
      Size = 60
    end
    object QrOVgIspPrfCabPermiFinHow: TIntegerField
      FieldName = 'PermiFinHow'
    end
    object QrOVgIspPrfCabReferencia: TWideStringField
      FieldName = 'Referencia'
      Size = 100
    end
  end
  object DsOVgIspPrfCab: TDataSource
    DataSet = QrOVgIspPrfCab
    Left = 620
    Top = 53
  end
  object PMIts: TPopupMenu
    Left = 668
    Top = 544
    object ItsInclui1: TMenuItem
      Caption = '&Adiciona'
      Enabled = False
    end
    object ItsAltera1: TMenuItem
      Caption = '&Edita'
      Enabled = False
    end
    object ItsExclui1: TMenuItem
      Caption = '&Remove'
      Enabled = False
    end
  end
  object PMCab: TPopupMenu
    OnPopup = PMCabPopup
    Left = 544
    Top = 544
    object CabInclui1: TMenuItem
      Caption = '&Inclui'
      OnClick = CabInclui1Click
    end
    object CabAltera1: TMenuItem
      Caption = '&Altera'
      Enabled = False
      OnClick = CabAltera1Click
    end
    object CabExclui1: TMenuItem
      Caption = '&Exclui'
      Enabled = False
      OnClick = CabExclui1Click
    end
  end
  object QrOVcYnsMedTop: TMySQLQuery
    Database = Dmod.ZZDB
    BeforeClose = QrOVcYnsMedTopBeforeClose
    AfterScroll = QrOVcYnsMedTopAfterScroll
    SQL.Strings = (
      
        'SELECT ELT(ymt.TolerBasCalc+1, "Medida", "Percentual") NO_TolerB' +
        'asCalc,'
      'ELT(ymt.TolerUnMdida+1, "cm", "inch") NO_TolerUnMdida, '
      'ygt.Nome NO_TOBIKO, ymt.*'
      'FROM ovcynsmedtop ymt'
      'LEFT JOIN ovcynsgratop ygt ON ygt.Codigo=ymt.Tobiko'
      'WHERE ygt.Codigo=1')
    Left = 48
    Top = 365
    object QrOVcYnsMedTopNO_TolerBasCalc: TWideStringField
      FieldName = 'NO_TolerBasCalc'
      Size = 10
    end
    object QrOVcYnsMedTopNO_TolerUnMdida: TWideStringField
      FieldName = 'NO_TolerUnMdida'
      Size = 4
    end
    object QrOVcYnsMedTopNO_TOBIKO: TWideStringField
      FieldName = 'NO_TOBIKO'
      Size = 60
    end
    object QrOVcYnsMedTopCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrOVcYnsMedTopControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrOVcYnsMedTopTobiko: TIntegerField
      FieldName = 'Tobiko'
      Required = True
    end
    object QrOVcYnsMedTopTolerBasCalc: TSmallintField
      FieldName = 'TolerBasCalc'
      Required = True
    end
    object QrOVcYnsMedTopTolerUnMdida: TSmallintField
      FieldName = 'TolerUnMdida'
      Required = True
    end
    object QrOVcYnsMedTopTolerRndPerc: TFloatField
      FieldName = 'TolerRndPerc'
      Required = True
      DisplayFormat = '0.000'
    end
    object QrOVcYnsMedTopLk: TIntegerField
      FieldName = 'Lk'
      Required = True
    end
    object QrOVcYnsMedTopDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrOVcYnsMedTopDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrOVcYnsMedTopUserCad: TIntegerField
      FieldName = 'UserCad'
      Required = True
    end
    object QrOVcYnsMedTopUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Required = True
    end
    object QrOVcYnsMedTopAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Required = True
    end
    object QrOVcYnsMedTopAWServerID: TIntegerField
      FieldName = 'AWServerID'
      Required = True
    end
    object QrOVcYnsMedTopAWStatSinc: TSmallintField
      FieldName = 'AWStatSinc'
      Required = True
    end
    object QrOVcYnsMedTopAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
  end
  object DsOVcYnsMedTop: TDataSource
    DataSet = QrOVcYnsMedTop
    Left = 48
    Top = 413
  end
  object QrOVcYnsMedDim: TMySQLQuery
    Database = Dmod.ZZDB
    SQL.Strings = (
      'SELECT ymd.*  '
      'FROM ovcynsmeddim ymd  '
      'WHERE ymd.Controle=1 ')
    Left = 148
    Top = 365
    object QrOVcYnsMedDimCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrOVcYnsMedDimControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrOVcYnsMedDimConta: TIntegerField
      FieldName = 'Conta'
      Required = True
    end
    object QrOVcYnsMedDimCodGrade: TIntegerField
      FieldName = 'CodGrade'
      Required = True
    end
    object QrOVcYnsMedDimCodTam: TWideStringField
      FieldName = 'CodTam'
      Size = 30
    end
    object QrOVcYnsMedDimMedidCer: TFloatField
      FieldName = 'MedidCer'
      Required = True
    end
    object QrOVcYnsMedDimMedidMin: TFloatField
      FieldName = 'MedidMin'
      Required = True
    end
    object QrOVcYnsMedDimMedidMax: TFloatField
      FieldName = 'MedidMax'
      Required = True
    end
    object QrOVcYnsMedDimUlWayInz: TSmallintField
      FieldName = 'UlWayInz'
      Required = True
    end
    object QrOVcYnsMedDimLk: TIntegerField
      FieldName = 'Lk'
      Required = True
    end
    object QrOVcYnsMedDimDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrOVcYnsMedDimDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrOVcYnsMedDimUserCad: TIntegerField
      FieldName = 'UserCad'
      Required = True
    end
    object QrOVcYnsMedDimUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Required = True
    end
    object QrOVcYnsMedDimAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Required = True
    end
    object QrOVcYnsMedDimAWServerID: TIntegerField
      FieldName = 'AWServerID'
      Required = True
    end
    object QrOVcYnsMedDimAWStatSinc: TSmallintField
      FieldName = 'AWStatSinc'
      Required = True
    end
    object QrOVcYnsMedDimAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
  end
  object DsOVcYnsMedDim: TDataSource
    DataSet = QrOVcYnsMedDim
    Left = 148
    Top = 413
  end
  object QrOVcYnsChkCtx: TMySQLQuery
    Database = Dmod.ZZDB
    BeforeClose = QrOVcYnsChkCtxBeforeClose
    AfterScroll = QrOVcYnsChkCtxAfterScroll
    SQL.Strings = (
      'SELECT yqt.Nome NO_CONTEXTO, ycc.*  '
      'FROM ovcynschkctx ycc '
      'LEFT JOIN ovcynsqstctx yqt ON yqt.Codigo=ycc.Contexto '
      'WHERE ycc.Codigo=1 ')
    Left = 420
    Top = 369
    object QrOVcYnsChkCtxNO_CONTEXTO: TWideStringField
      FieldName = 'NO_CONTEXTO'
      Size = 60
    end
    object QrOVcYnsChkCtxCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrOVcYnsChkCtxControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrOVcYnsChkCtxContexto: TIntegerField
      FieldName = 'Contexto'
      Required = True
    end
  end
  object DsOVcYnsChkCtx: TDataSource
    DataSet = QrOVcYnsChkCtx
    Left = 420
    Top = 417
  end
  object QrOVcYnsChkTop: TMySQLQuery
    Database = Dmod.ZZDB
    SQL.Strings = (
      'SELECT yqc.Nome NO_Topico, yqm.Nome NO_Magnitude, yct.*'
      'FROM ovcynschktop yct'
      'LEFT JOIN ovcynsqsttop yqc ON yqc.Codigo=yct.Topico'
      'LEFT JOIN ovcynsqstmag yqm ON yqm.Codigo=yct.Magnitude')
    Left = 524
    Top = 369
    object QrOVcYnsChkTopCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrOVcYnsChkTopControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrOVcYnsChkTopConta: TIntegerField
      FieldName = 'Conta'
      Required = True
    end
    object QrOVcYnsChkTopTopico: TIntegerField
      FieldName = 'Topico'
      Required = True
    end
    object QrOVcYnsChkTopMagnitude: TIntegerField
      FieldName = 'Magnitude'
      Required = True
    end
    object QrOVcYnsChkTopLk: TIntegerField
      FieldName = 'Lk'
      Required = True
    end
    object QrOVcYnsChkTopDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrOVcYnsChkTopDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrOVcYnsChkTopUserCad: TIntegerField
      FieldName = 'UserCad'
      Required = True
    end
    object QrOVcYnsChkTopUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Required = True
    end
    object QrOVcYnsChkTopAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Required = True
    end
    object QrOVcYnsChkTopAWServerID: TIntegerField
      FieldName = 'AWServerID'
      Required = True
    end
    object QrOVcYnsChkTopAWStatSinc: TSmallintField
      FieldName = 'AWStatSinc'
      Required = True
    end
    object QrOVcYnsChkTopAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
    object QrOVcYnsChkTopNO_Topico: TWideStringField
      FieldName = 'NO_Topico'
      Size = 60
    end
    object QrOVcYnsChkTopNO_Magnitude: TWideStringField
      FieldName = 'NO_Magnitude'
      Size = 60
    end
  end
  object DsOVcYnsChkTop: TDataSource
    DataSet = QrOVcYnsChkTop
    Left = 524
    Top = 417
  end
  object QrOVdReferencia: TMySQLQuery
    Database = Dmod.ZZDB
    SQL.Strings = (
      'SELECT Codigo, Referencia, Nome '
      'FROM ovdreferencia '
      'ORDER BY Nome ')
    Left = 676
    Top = 370
    object QrOVdReferenciaCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrOVdReferenciaNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QrOVdReferenciaReferencia: TWideStringField
      FieldName = 'Referencia'
      Size = 60
    end
  end
  object DsOVdReferencia: TDataSource
    DataSet = QrOVdReferencia
    Left = 676
    Top = 418
  end
end
