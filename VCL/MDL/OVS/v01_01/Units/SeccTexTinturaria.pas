unit SeccTexTinturaria;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkPermissoes,
  dmkDBGridZTO, mySQLDbTables, Vcl.Menus;

type
  TFmSeccTexTinturaria = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtRefresh: TBitBtn;
    LaTitulo1C: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    QrAptos: TMySQLQuery;
    DsAptos: TDataSource;
    QrAptosNO_LOCAL: TWideStringField;
    QrAptosCodigo: TIntegerField;
    QrAptosNome: TWideStringField;
    QrAptosLocal: TIntegerField;
    QrAptosNrOP: TIntegerField;
    QrAptosSeqGrupo: TIntegerField;
    QrAptosNrReduzidoOP: TIntegerField;
    QrAptosDtHrAbert: TDateTimeField;
    QrAptosDtHrFecha: TDateTimeField;
    QrAptosOVcYnsExg: TIntegerField;
    QrAptosZtatusIsp: TIntegerField;
    QrAptosZtatusDtH: TDateTimeField;
    QrAptosZtatusMot: TIntegerField;
    QrAptosPermiFinHow: TIntegerField;
    QrAptosOVgItxPrfCab: TIntegerField;
    QrAptosAtivMan: TSmallintField;
    QrAptosAtivAut: TSmallintField;
    QrAptosLk: TIntegerField;
    QrAptosDataCad: TDateField;
    QrAptosDataAlt: TDateField;
    QrAptosUserCad: TIntegerField;
    QrAptosUserAlt: TIntegerField;
    QrAptosAlterWeb: TSmallintField;
    QrAptosAWServerID: TIntegerField;
    QrAptosAWStatSinc: TSmallintField;
    QrAptosAtivo: TSmallintField;
    QrAptosSegmntInsp: TIntegerField;
    QrAptosSeccaoInsp: TIntegerField;
    QrAptosNO_REFERENCIA: TWideStringField;
    Splitter1: TSplitter;
    Panel5: TPanel;
    PnEdita: TPanel;
    BtAlteraBtl: TBitBtn;
    BtExcluiBtl: TBitBtn;
    BtIncluiBtl: TBitBtn;
    DBGrid1: TDBGrid;
    QrOVgItxGerBtl: TMySQLQuery;
    DsOVgItxGerBtl: TDataSource;
    QrOVgItxGerBtlCodigo: TIntegerField;
    QrOVgItxGerBtlControle: TIntegerField;
    QrOVgItxGerBtlOrdem: TIntegerField;
    QrOVgItxGerBtlSeccaoOP: TWideStringField;
    QrOVgItxGerBtlSeccaoMaq: TWideStringField;
    QrOVgItxGerBtlQtReal: TFloatField;
    QrOVgItxGerBtlLk: TIntegerField;
    QrOVgItxGerBtlDataCad: TDateField;
    QrOVgItxGerBtlDataAlt: TDateField;
    QrOVgItxGerBtlUserCad: TIntegerField;
    QrOVgItxGerBtlUserAlt: TIntegerField;
    QrOVgItxGerBtlAlterWeb: TSmallintField;
    QrOVgItxGerBtlAWServerID: TIntegerField;
    QrOVgItxGerBtlAWStatSinc: TSmallintField;
    QrOVgItxGerBtlAtivo: TSmallintField;
    Panel6: TPanel;
    DBGridZTO: TdmkDBGridZTO;
    Panel7: TPanel;
    BtAlteraCab: TBitBtn;
    QrAptosQtBtl: TFloatField;
    PMAptos: TPopupMenu;
    Encerraitemdefinecomopsconfigurado1: TMenuItem;
    QrCorda: TMySQLQuery;
    QrCordaCodigo: TIntegerField;
    QrAptosCiclo: TIntegerField;
    QrAptosQtReal: TFloatField;
    QrAptosQtLocal: TFloatField;
    QrAptosCfgBtlOk: TSmallintField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtRefreshClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure QrAptosAfterScroll(DataSet: TDataSet);
    procedure QrAptosAfterOpen(DataSet: TDataSet);
    procedure QrAptosBeforeClose(DataSet: TDataSet);
    procedure QrOVgItxGerBtlAfterOpen(DataSet: TDataSet);
    procedure QrOVgItxGerBtlBeforeClose(DataSet: TDataSet);
    procedure BtAlteraCabClick(Sender: TObject);
    procedure Encerraitemdefinecomopsconfigurado1Click(Sender: TObject);
    procedure BtIncluiBtlClick(Sender: TObject);
    procedure BtAlteraBtlClick(Sender: TObject);
    procedure BtExcluiBtlClick(Sender: TObject);
  private
    { Private declarations }
    FCorda: String;
    //
    //procedure MostraFormOVgItxGerBtl(SQLType: TSQLType);
  public
    { Public declarations }
    procedure ReopenItensAPosConfig();
    procedure ReopenOVgItxGerBtl(Controle: Integer);
    procedure DefineCordaInicial();
    procedure ReopenByRefresh();
  end;
var
  FmSeccTexTinturaria: TFmSeccTexTinturaria;

implementation

uses UnMyObjects, DmkDAC_PF, UMySQLModule, MyDBCheck, UnOVS_Jan, UnGOTOy,
  Module, OVgItxGerBtl;

{$R *.DFM}

procedure TFmSeccTexTinturaria.BtAlteraBtlClick(Sender: TObject);
begin
  //MostraFormOVgItxGerBtl(stUpd);
  OVS_Jan.MostraFormOVgItxGerBtl(stUpd, DsAptos, QrOVgItxGerBtl);
  ReopenByRefresh();
end;

procedure TFmSeccTexTinturaria.BtAlteraCabClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMAptos, BtAlteraCab);
end;

procedure TFmSeccTexTinturaria.BtConfirmaClick(Sender: TObject);
{
object Label1: TLabel
  Left = 452
  Top = 16
  Width = 68
  Height = 13
  Caption = 'OP Tinturaria: '
end
object EdSeccaoOP: TdmkEdit
  Left = 524
  Top = 12
  Width = 273
  Height = 21
  TabOrder = 0
  FormatType = dmktfString
  MskType = fmtNone
  DecimalSize = 0
  LeftZeros = 0
  NoEnterToTab = False
  NoForceUppercase = False
  ForceNextYear = False
  DataFormat = dmkdfShort
  HoraFormat = dmkhfShort
  UpdType = utYes
  Obrigatorio = False
  PermiteNulo = False
  ValueVariant = ''
  ValWarn = False
end
object BtConfirma: TBitBtn
  Tag = 11
  Left = 800
  Top = 4
  Width = 201
  Height = 40
  Caption = '&Altera itens selecionados'
  NumGlyphs = 2
  TabOrder = 1
  OnClick = BtConfirmaClick
end
}
{
var
  I: Integer;
  SeccaoOP: String;
}
begin
{
  SeccaoOP := EdSeccaoOP.Text;
  if (DBGridZTO.SelectedRows.Count > 0) then
  begin
    for I := 0 to DBGridZTO.SelectedRows.Count - 1 do
    begin
      QrAptos.GotoBookmark(DBGridZTO.SelectedRows.Items[I]);
      //
      Dmod.MyDB.Execute('UPDATE ovgitxgercab SET SeccaoOP="' + SeccaoOP +
      '" WHERE Codigo=' + Geral.FF0(QrAptosCodigo.Value));
    end;
    ReopenItensAPosConfig();
  end else
    Geral.MB_Aviso('Nenhum item foi selecionado!');
}
end;

procedure TFmSeccTexTinturaria.BtExcluiBtlClick(Sender: TObject);
var
  Codigo, Controle: Integer;
begin
  QrAptosCodigo.Value;
  if UMyMod.ExcluiRegistroInt1('Confirma a exclus�o do item selecionado?',
  'ovgitxgerbtl', 'Controle', QrOVgItxGerBtlControle.Value, Dmod.MyDB) = ID_YES then
  begin
    Controle := GOTOy.LocalizaPriorNextIntQr(QrOVgItxGerBtl,
      QrOVgItxGerBtlControle, QrOVgItxGerBtlControle.Value);
    Dmod.AtualizaQtBtl(Codigo);
    ReopenOVgItxGerBtl(Controle);
  end;
  ReopenByRefresh();
end;

procedure TFmSeccTexTinturaria.BtIncluiBtlClick(Sender: TObject);
begin
  //MostraFormOVgItxGerBtl(stIns);
  OVS_Jan.MostraFormOVgItxGerBtl(stIns, DsAptos, QrOVgItxGerBtl);
  ReopenByRefresh();
end;

procedure TFmSeccTexTinturaria.BtRefreshClick(Sender: TObject);
begin
  ReopenByRefresh();
end;

procedure TFmSeccTexTinturaria.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmSeccTexTinturaria.DefineCordaInicial();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrCorda, Dmod.MyDB, [
  'SELECT itx.Codigo ',
  'FROM ovgitxgercab itx ',
  'WHERE itx.SeccaoInsp=2048 ',
  'AND itx.CfgBtlOk=0 ',
  EmptyStr]);
  //
  FCorda := MyObjects.CordaDeQuery(QrCorda, 'Codigo', EmptyStr);
end;

procedure TFmSeccTexTinturaria.Encerraitemdefinecomopsconfigurado1Click(
  Sender: TObject);
var
  CfgBtlOk, Codigo: Integer;
begin
  if Geral.MB_Pergunta(
  'Deseja encerrar a p�s-configura��o deste item de configura��o?') = ID_Yes then
  begin
    CfgBtlOk := 1;
    Codigo   := QrAptosCodigo.Value;
    //
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'ovgitxgercab', False, [
    'CfgBtlOk'], [
    'Codigo'], [
    CfgBtlOk], [
    Codigo], True) then
      ReopenItensAPosConfig();
  end;
end;

procedure TFmSeccTexTinturaria.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmSeccTexTinturaria.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  FCorda := EmptyStr;
end;

procedure TFmSeccTexTinturaria.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

{
procedure TFmSeccTexTinturaria.MostraFormOVgItxGerBtl(SQLType: TSQLType);
begin
  if DBCheck.CriaFm(TFmOVgItxGerBtl, FmOVgItxGerBtl, afmoNegarComAviso) then
  begin
    FmOVgItxGerBtl.ImgTipo.SQLType := SQLType;
    FmOVgItxGerBtl.FDsCab := DsAptos;
    FmOVgItxGerBtl.FQrIts := QrOVgItxGerBtl;
    //
    if SQLType = stIns then
    begin
      //FmOVgItxGerBtl.EdCPF1.ReadOnly := False
    end else
    begin
      //
      FmOVgItxGerBtl.EdControle.ValueVariant := QrOVgItxGerBtlControle.Value;
      //
      FmOVgItxGerBtl.EdOrdem.ValueVariant     := QrOVgItxGerBtlOrdem.Value;
      FmOVgItxGerBtl.EdSeccaoOP.ValueVariant  := QrOVgItxGerBtlSeccaoOP.Value;
      FmOVgItxGerBtl.EdSeccaoMaq.ValueVariant := QrOVgItxGerBtlSeccaoMaq.Value;
      FmOVgItxGerBtl.EdQtReal.ValueVariant    := QrOVgItxGerBtlQtReal.Value;
    end;
    FmOVgItxGerBtl.ShowModal;
    FmOVgItxGerBtl.Destroy;
  end;
end;
}

procedure TFmSeccTexTinturaria.QrAptosAfterOpen(DataSet: TDataSet);
begin
  BtAlteraCab.Enabled := QrAptos.RecordCount > 0;
  BtIncluiBtl.Enabled := QrAptos.RecordCount > 0;
end;

procedure TFmSeccTexTinturaria.QrAptosAfterScroll(DataSet: TDataSet);
begin
  ReopenOVgItxGerBtl(0);
end;

procedure TFmSeccTexTinturaria.QrAptosBeforeClose(DataSet: TDataSet);
begin
  QrOVgItxGerBtl.Close;
  //
  BtAlteraCab.Enabled := False;
  BtIncluiBtl.Enabled := False;
end;

procedure TFmSeccTexTinturaria.QrOVgItxGerBtlAfterOpen(DataSet: TDataSet);
begin
  BtAlteraBtl.Enabled := QrOVgItxGerBtl.RecordCount > 0;
  BtExcluiBtl.Enabled := QrOVgItxGerBtl.RecordCount > 0;
end;

procedure TFmSeccTexTinturaria.QrOVgItxGerBtlBeforeClose(DataSet: TDataSet);
begin
  BtAlteraBtl.Enabled := False;
  BtExcluiBtl.Enabled := False;
end;

procedure TFmSeccTexTinturaria.ReopenByRefresh();
var
  Codigo: Integer;
begin
  if FCorda <> EmptyStr then
  begin
    Codigo := QrAptosCodigo.Value;
    UnDmkDAC_PF.AbreMySQLQuery0(QrAptos, Dmod.MyDB, [
    'SELECT fop.Ciclo, fop.QtReal, fop.QtLocal, ',
    'loc.Nome NO_LOCAL, art.Nome NO_Referencia, itx.* ',
    'FROM ovgitxgercab itx ',
    'LEFT JOIN ovdlocal loc ON loc.Codigo=itx.Local ',
    'LEFT JOIN ovdreferencia art ON art.Codigo=itx.SeqGrupo ',
    'LEFT JOIN ovfordemproducao fop ON ',
    '  fop.NrReduzidoOP=itx.NrReduzidoOP ',
    '  AND fop.NrOP=itx.NrOP ',
    '  AND fop.Local=itx.Local ',
    '  AND fop.SeqGrupo=itx.SeqGrupo ',
    'WHERE itx.Codigo IN (' + FCorda + ') ',
    EmptyStr]);
    //
    QrAptos.Locate('Codigo', Codigo, []);
  end;
end;

procedure TFmSeccTexTinturaria.ReopenItensAPosConfig();
begin
{
  UnDmkDAC_PF.AbreMySQLQuery0(QrAptos, Dmod.MyDB, [
  'SELECT loc.Nome NO_LOCAL, art.Nome NO_Referencia, itx.* ',
  'FROM ovgitxgercab itx ',
  'LEFT JOIN ovdlocal loc ON loc.Codigo=itx.Local ',
  'LEFT JOIN ovdreferencia art ON art.Codigo=itx.SeqGrupo ',
  'WHERE itx.SeccaoInsp=2048 ',
  'AND ( ',
  '  SeccaoOP IS NULL ',
  '  OR  ',
  '  SeccaoOP = "" ',
  ') ',
  EmptyStr]);
}
  UnDmkDAC_PF.AbreMySQLQuery0(QrAptos, Dmod.MyDB, [
  'SELECT fop.Ciclo, fop.QtReal, fop.QtLocal, ',
  'loc.Nome NO_LOCAL, art.Nome NO_Referencia, itx.* ',
  'FROM ovgitxgercab itx ',
  'LEFT JOIN ovdlocal loc ON loc.Codigo=itx.Local ',
  'LEFT JOIN ovdreferencia art ON art.Codigo=itx.SeqGrupo ',
  'LEFT JOIN ovfordemproducao fop ON ',
  '  fop.NrReduzidoOP=itx.NrReduzidoOP ',
  '  AND fop.NrOP=itx.NrOP',
  '  AND fop.Local=itx.Local',
  '  AND fop.SeqGrupo=itx.SeqGrupo',
  'WHERE itx.SeccaoInsp=2048 ',
  'AND itx.CfgBtlOk=0 ',
  EmptyStr]);
end;

procedure TFmSeccTexTinturaria.ReopenOVgItxGerBtl(Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrOVgItxGerBtl, Dmod.MyDB, [
  'SELECT * ',
  'FROM ovgitxgerbtl ',
  'WHERE Codigo=' + Geral.FF0(QrAptosCodigo.Value),
  EmptyStr]);
  //
  if Controle <> 0 then
    QrOVgItxGerBtl.Locate('Controle', Controle, []);
end;

end.
