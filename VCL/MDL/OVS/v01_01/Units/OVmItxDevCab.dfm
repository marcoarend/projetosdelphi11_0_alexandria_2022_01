object FmOVmItxDevCab: TFmOVmItxDevCab
  Left = 368
  Top = 194
  Caption = 'OVS-EXCAO-004 :: Inspe'#231#245'es de T'#234'xteis Realizadas'
  ClientHeight = 659
  ClientWidth = 1007
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnEdita: TPanel
    Left = 0
    Top = 96
    Width = 1007
    Height = 563
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    Visible = False
    object GBEdita: TGroupBox
      Left = 0
      Top = 0
      Width = 1007
      Height = 125
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 0
      object Label7: TLabel
        Left = 16
        Top = 16
        Width = 14
        Height = 13
        Caption = 'ID:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label9: TLabel
        Left = 76
        Top = 16
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
        Color = clBtnFace
        ParentColor = False
      end
      object EdCodigo: TdmkEdit
        Left = 16
        Top = 32
        Width = 56
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Codigo'
        UpdCampo = 'Codigo'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdNome: TdmkEdit
        Left = 76
        Top = 32
        Width = 692
        Height = 21
        TabOrder = 1
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Nome'
        UpdCampo = 'Nome'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
    end
    object GBConfirma: TGroupBox
      Left = 0
      Top = 500
      Width = 1007
      Height = 63
      Align = alBottom
      TabOrder = 1
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 12
        Top = 17
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
      end
      object Panel1: TPanel
        Left = 868
        Top = 15
        Width = 137
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 7
          Top = 2
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
    end
  end
  object PnDados: TPanel
    Left = 0
    Top = 96
    Width = 1007
    Height = 563
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    object GBCntrl: TGroupBox
      Left = 0
      Top = 499
      Width = 1007
      Height = 64
      Align = alBottom
      TabOrder = 0
      object Panel5: TPanel
        Left = 2
        Top = 15
        Width = 172
        Height = 47
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 174
        Top = 15
        Width = 311
        Height = 47
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel3: TPanel
        Left = 485
        Top = 15
        Width = 520
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object BtExclui: TBitBtn
          Tag = 12
          Left = 252
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Exclui'
          Enabled = False
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          Visible = False
        end
        object BtAltera: TBitBtn
          Tag = 11
          Left = 128
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Altera'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          Visible = False
          OnClick = BtAlteraClick
        end
        object BtEmail: TBitBtn
          Tag = 10
          Left = 4
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Email'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          Visible = False
          OnClick = BtEmailClick
        end
        object Panel2: TPanel
          Left = 387
          Top = 0
          Width = 133
          Height = 47
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 3
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
      end
    end
    object GBDados: TGroupBox
      Left = 0
      Top = 0
      Width = 1007
      Height = 217
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 1
      object Panel6: TPanel
        Left = 2
        Top = 15
        Width = 1003
        Height = 200
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object Label1: TLabel
          Left = 8
          Top = 0
          Width = 36
          Height = 13
          Caption = 'C'#243'digo:'
          FocusControl = DBEdCodigo
        end
        object Label2: TLabel
          Left = 68
          Top = 0
          Width = 51
          Height = 13
          Caption = 'Descri'#231#227'o:'
          FocusControl = DBEdNome
        end
        object Label17: TLabel
          Left = 768
          Top = 40
          Width = 43
          Height = 13
          Caption = 'Abertura:'
          FocusControl = DBEdCodigo
        end
        object Label18: TLabel
          Left = 884
          Top = 40
          Width = 69
          Height = 13
          Caption = 'Encerramento:'
          FocusControl = DBEdNome
        end
        object Label19: TLabel
          Left = 612
          Top = 0
          Width = 62
          Height = 13
          Caption = 'Resultado:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label3: TLabel
          Left = 8
          Top = 80
          Width = 29
          Height = 13
          Caption = 'Local:'
        end
        object Label4: TLabel
          Left = 64
          Top = 80
          Width = 91
          Height = 13
          Caption = 'Descri'#231#227'o do local:'
        end
        object Label6: TLabel
          Left = 8
          Top = 120
          Width = 30
          Height = 13
          Caption = 'Artigo:'
        end
        object Label8: TLabel
          Left = 64
          Top = 120
          Width = 95
          Height = 13
          Caption = 'Descri'#231#227'o do artigo:'
        end
        object Label5: TLabel
          Left = 772
          Top = 120
          Width = 18
          Height = 13
          Caption = 'OP:'
        end
        object Label10: TLabel
          Left = 848
          Top = 120
          Width = 52
          Height = 13
          Caption = 'Reduz.OP:'
        end
        object Label11: TLabel
          Left = 468
          Top = 80
          Width = 89
          Height = 13
          Caption = 'Tabela de exa'#231#227'o:'
        end
        object Label13: TLabel
          Left = 676
          Top = 120
          Width = 48
          Height = 13
          Caption = 'Tamanho:'
        end
        object Label15: TLabel
          Left = 924
          Top = 120
          Width = 32
          Height = 13
          Caption = 'Grade:'
        end
        object Label21: TLabel
          Left = 8
          Top = 160
          Width = 54
          Height = 13
          Caption = 'Dispositivo:'
          FocusControl = DBEdit15
        end
        object Label23: TLabel
          Left = 428
          Top = 0
          Width = 60
          Height = 13
          Caption = 'Cofigura'#231#227'o:'
          FocusControl = DBEdit17
        end
        object Label22: TLabel
          Left = 760
          Top = 160
          Width = 47
          Height = 13
          Caption = 'P'#231' a insp.'
          FocusControl = DBEdit18
        end
        object Label24: TLabel
          Left = 820
          Top = 160
          Width = 53
          Height = 13
          Caption = 'P'#231'. inspec.'
          FocusControl = DBEdit19
        end
        object Label25: TLabel
          Left = 880
          Top = 160
          Width = 37
          Height = 13
          Caption = 'Qt. real:'
          FocusControl = DBEdit20
        end
        object Label26: TLabel
          Left = 940
          Top = 160
          Width = 42
          Height = 13
          Caption = 'Qt. local:'
          FocusControl = DBEdit21
        end
        object Label27: TLabel
          Left = 616
          Top = 120
          Width = 40
          Height = 13
          Caption = 'Produto:'
          FocusControl = DBEdit22
        end
        object Label28: TLabel
          Left = 552
          Top = 0
          Width = 48
          Height = 13
          Caption = 'Pts neg.'
          FocusControl = DBEdit23
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label29: TLabel
          Left = 496
          Top = 0
          Width = 31
          Height = 13
          Caption = 'Seq.:'
          FocusControl = DBEdit24
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label30: TLabel
          Left = 364
          Top = 160
          Width = 66
          Height = 13
          Caption = 'Autentica'#231#227'o:'
          FocusControl = DBEdit25
        end
        object Label32: TLabel
          Left = 820
          Top = 0
          Width = 177
          Height = 13
          Caption = 'Usu'#225'rio padr'#227'o do disposotivo:'
          FocusControl = DBEdit26
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label33: TLabel
          Left = 692
          Top = 160
          Width = 49
          Height = 13
          Caption = '% Medido:'
          FocusControl = DBEdit27
        end
        object Label12: TLabel
          Left = 8
          Top = 40
          Width = 45
          Height = 13
          Caption = 'Batelada:'
          FocusControl = DBEdit8
        end
        object Label14: TLabel
          Left = 68
          Top = 40
          Width = 137
          Height = 13
          Caption = 'OP do fornecedor (Tinturaria)'
          FocusControl = DBEdit10
        end
        object Label16: TLabel
          Left = 364
          Top = 40
          Width = 44
          Height = 13
          Caption = 'M'#225'quina:'
          FocusControl = DBEdit11
        end
        object DBEdCodigo: TdmkDBEdit
          Left = 8
          Top = 16
          Width = 56
          Height = 21
          TabStop = False
          DataField = 'Codigo'
          DataSource = DsOVmItxDevCab
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ReadOnly = True
          ShowHint = True
          TabOrder = 0
          UpdType = utYes
          Alignment = taRightJustify
        end
        object DBEdNome: TdmkDBEdit
          Left = 68
          Top = 16
          Width = 357
          Height = 21
          Color = clWhite
          DataField = 'Nome'
          DataSource = DsOVmItxDevCab
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          UpdType = utYes
          Alignment = taLeftJustify
        end
        object dmkDBEdit1: TdmkDBEdit
          Left = 768
          Top = 56
          Width = 112
          Height = 21
          Color = clWhite
          DataField = 'DtHrAbert_TXT'
          DataSource = DsOVmItxDevCab
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          UpdType = utYes
          Alignment = taLeftJustify
        end
        object dmkDBEdit2: TdmkDBEdit
          Left = 883
          Top = 56
          Width = 112
          Height = 21
          Color = clWhite
          DataField = 'DtHrFecha_TXT'
          DataSource = DsOVmItxDevCab
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          UpdType = utYes
          Alignment = taLeftJustify
        end
        object dmkDBEdit5: TdmkDBEdit
          Left = 612
          Top = 16
          Width = 49
          Height = 21
          Color = clWhite
          DataField = 'InspResul'
          DataSource = DsOVmItxDevCab
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 4
          UpdType = utYes
          Alignment = taLeftJustify
        end
        object dmkDBEdit6: TdmkDBEdit
          Left = 664
          Top = 16
          Width = 153
          Height = 21
          Color = clWhite
          DataField = 'NO_InspResul'
          DataSource = DsOVmItxDevCab
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 5
          UpdType = utYes
          Alignment = taLeftJustify
        end
        object DBEdit1: TDBEdit
          Left = 8
          Top = 96
          Width = 56
          Height = 21
          DataField = 'Local'
          DataSource = DsOVmItxDevCab
          TabOrder = 6
        end
        object DBEdit2: TDBEdit
          Left = 64
          Top = 96
          Width = 401
          Height = 21
          DataField = 'NO_Local'
          DataSource = DsOVmItxDevCab
          TabOrder = 7
        end
        object DBEdit4: TDBEdit
          Left = 8
          Top = 136
          Width = 56
          Height = 21
          DataField = 'SeqGrupo'
          DataSource = DsOVmItxDevCab
          TabOrder = 8
        end
        object DBEdit5: TDBEdit
          Left = 64
          Top = 136
          Width = 545
          Height = 21
          DataField = 'NO_Referencia'
          DataSource = DsOVmItxDevCab
          TabOrder = 9
        end
        object DBEdit3: TDBEdit
          Left = 772
          Top = 136
          Width = 72
          Height = 21
          DataField = 'NrOP'
          DataSource = DsOVmItxDevCab
          TabOrder = 10
        end
        object DBEdit6: TDBEdit
          Left = 848
          Top = 136
          Width = 72
          Height = 21
          DataField = 'NrReduzidoOP'
          DataSource = DsOVmItxDevCab
          TabOrder = 11
        end
        object DBEdit7: TDBEdit
          Left = 468
          Top = 96
          Width = 56
          Height = 21
          DataField = 'OVcYnsExg'
          DataSource = DsOVmItxDevCab
          TabOrder = 12
        end
        object DBEdit9: TDBEdit
          Left = 524
          Top = 96
          Width = 473
          Height = 21
          DataField = 'NO_OVcYnsExg'
          DataSource = DsOVmItxDevCab
          TabOrder = 13
        end
        object DBEdit13: TDBEdit
          Left = 676
          Top = 136
          Width = 93
          Height = 21
          DataField = 'CodTam'
          DataSource = DsOVmItxDevCab
          TabOrder = 14
        end
        object DBEdit14: TDBEdit
          Left = 924
          Top = 136
          Width = 72
          Height = 21
          DataField = 'CodGrade'
          DataSource = DsOVmItxDevCab
          TabOrder = 15
        end
        object DBEdit15: TDBEdit
          Left = 8
          Top = 176
          Width = 56
          Height = 21
          DataField = 'DeviceSI'
          DataSource = DsOVmItxDevCab
          TabOrder = 16
        end
        object DBEdit16: TDBEdit
          Left = 64
          Top = 176
          Width = 296
          Height = 21
          DataField = 'DeviceID'
          DataSource = DsOVmItxDevCab
          TabOrder = 17
        end
        object DBEdit17: TDBEdit
          Left = 428
          Top = 16
          Width = 65
          Height = 21
          DataField = 'OVgItxGer'
          DataSource = DsOVmItxDevCab
          TabOrder = 18
        end
        object DBEdit18: TDBEdit
          Left = 760
          Top = 176
          Width = 56
          Height = 21
          DataField = 'PecasItx'
          DataSource = DsOVmItxDevCab
          TabOrder = 19
        end
        object DBEdit19: TDBEdit
          Left = 820
          Top = 176
          Width = 56
          Height = 21
          DataField = 'PecaAtual'
          DataSource = DsOVmItxDevCab
          TabOrder = 20
        end
        object DBEdit20: TDBEdit
          Left = 880
          Top = 176
          Width = 56
          Height = 21
          DataField = 'QtReal'
          DataSource = DsOVmItxDevCab
          TabOrder = 21
        end
        object DBEdit21: TDBEdit
          Left = 940
          Top = 176
          Width = 56
          Height = 21
          DataField = 'QtLocal'
          DataSource = DsOVmItxDevCab
          TabOrder = 22
        end
        object DBEdit22: TDBEdit
          Left = 616
          Top = 136
          Width = 56
          Height = 21
          DataField = 'Produto'
          DataSource = DsOVmItxDevCab
          TabOrder = 23
        end
        object DBEdit23: TDBEdit
          Left = 552
          Top = 16
          Width = 57
          Height = 21
          DataField = 'PontosTot'
          DataSource = DsOVmItxDevCab
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 24
        end
        object DBEdit24: TDBEdit
          Left = 496
          Top = 16
          Width = 53
          Height = 21
          DataField = 'InspeSeq'
          DataSource = DsOVmItxDevCab
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 25
        end
        object DBEdit25: TDBEdit
          Left = 364
          Top = 176
          Width = 324
          Height = 21
          DataField = 'RandmStr'
          DataSource = DsOVmItxDevCab
          TabOrder = 26
        end
        object DBEdit26: TDBEdit
          Left = 820
          Top = 16
          Width = 177
          Height = 21
          DataField = 'UserNmePdr'
          DataSource = DsOVmItxDevCab
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 27
        end
        object DBEdit27: TDBEdit
          Left = 692
          Top = 176
          Width = 64
          Height = 21
          DataField = 'PerExgReal'
          DataSource = DsOVmItxDevCab
          TabOrder = 28
        end
        object DBEdit8: TDBEdit
          Left = 8
          Top = 56
          Width = 57
          Height = 21
          DataField = 'Batelada'
          DataSource = DsOVmItxDevCab
          TabOrder = 29
        end
        object DBEdit10: TDBEdit
          Left = 68
          Top = 56
          Width = 293
          Height = 21
          DataField = 'SeccaoOP'
          DataSource = DsOVmItxDevCab
          TabOrder = 30
        end
        object DBEdit11: TDBEdit
          Left = 364
          Top = 56
          Width = 401
          Height = 21
          DataField = 'SeccaoMaq'
          DataSource = DsOVmItxDevCab
          TabOrder = 31
        end
      end
    end
    object PGDados: TPageControl
      Left = 0
      Top = 217
      Width = 1007
      Height = 132
      ActivePage = TabSheet1
      Align = alTop
      TabOrder = 2
      object TabSheet1: TTabSheet
        Caption = ' T'#243'picos negativados '
        object DBGrid1: TDBGrid
          Left = 0
          Top = 0
          Width = 457
          Height = 104
          Align = alLeft
          DataSource = DsPontosNeg
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -12
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'PecaSeq'
              Title.Caption = 'P'#231' seq.'
              Width = 36
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Tabela'
              Width = 102
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'QtdFotos'
              Title.Caption = 'Q.Fotos'
              Width = 27
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_Topico'
              Title.Caption = 'T'#243'pico'
              Width = 210
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'PontNeg'
              Title.Caption = 'Ps neg.'
              Width = 41
              Visible = True
            end>
        end
        object Panel7: TPanel
          Left = 457
          Top = 0
          Width = 148
          Height = 104
          Align = alLeft
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 1
          object Label31: TLabel
            Left = 0
            Top = 0
            Width = 148
            Height = 13
            Align = alTop
            Alignment = taCenter
            Caption = 'Fotos do item selecionado'
            ExplicitWidth = 123
          end
          object DBGrid2: TDBGrid
            Left = 0
            Top = 13
            Width = 148
            Height = 91
            Align = alClient
            DataSource = DsOVmItxDevFts
            TabOrder = 0
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -12
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            Columns = <
              item
                Expanded = False
                FieldName = 'DataHora'
                Visible = True
              end>
          end
        end
        object Panel8: TPanel
          Left = 605
          Top = 0
          Width = 394
          Height = 104
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 2
          object ImgFoto: TImage
            Left = 0
            Top = 13
            Width = 394
            Height = 91
            Align = alClient
            Picture.Data = {
              0A544A504547496D61676577020000FFD8FFE000104A46494600010101006000
              600000FFDB004300020101020101020202020202020203050303030303060404
              0305070607070706070708090B0908080A0807070A0D0A0A0B0C0C0C0C07090E
              0F0D0C0E0B0C0C0CFFDB004301020202030303060303060C0807080C0C0C0C0C
              0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C0C
              0C0C0C0C0C0C0C0C0C0C0C0C0CFFC00011080002000203012200021101031101
              FFC4001F0000010501010101010100000000000000000102030405060708090A
              0BFFC400B5100002010303020403050504040000017D01020300041105122131
              410613516107227114328191A1082342B1C11552D1F02433627282090A161718
              191A25262728292A3435363738393A434445464748494A535455565758595A63
              6465666768696A737475767778797A838485868788898A92939495969798999A
              A2A3A4A5A6A7A8A9AAB2B3B4B5B6B7B8B9BAC2C3C4C5C6C7C8C9CAD2D3D4D5D6
              D7D8D9DAE1E2E3E4E5E6E7E8E9EAF1F2F3F4F5F6F7F8F9FAFFC4001F01000301
              01010101010101010000000000000102030405060708090A0BFFC400B5110002
              0102040403040705040400010277000102031104052131061241510761711322
              328108144291A1B1C109233352F0156272D10A162434E125F11718191A262728
              292A35363738393A434445464748494A535455565758595A636465666768696A
              737475767778797A82838485868788898A92939495969798999AA2A3A4A5A6A7
              A8A9AAB2B3B4B5B6B7B8B9BAC2C3C4C5C6C7C8C9CAD2D3D4D5D6D7D8D9DAE2E3
              E4E5E6E7E8E9EAF2F3F4F5F6F7F8F9FAFFDA000C03010002110311003F00FE7F
              E8A28A00FFD9}
            Proportional = True
            ExplicitLeft = 32
            ExplicitTop = 25
            ExplicitHeight = 152
          end
          object LaNomeFoto: TLabel
            Left = 0
            Top = 0
            Width = 394
            Height = 13
            Align = alTop
            Caption = '...'
            ExplicitWidth = 9
          end
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1007
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 959
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 12
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 216
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 216
      Top = 0
      Width = 743
      Height = 52
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 399
        Height = 32
        Caption = 'Inspe'#231#245'es de T'#234'xteis Realizadas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 399
        Height = 32
        Caption = 'Inspe'#231#245'es de T'#234'xteis Realizadas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 399
        Height = 32
        Caption = 'Inspe'#231#245'es de T'#234'xteis Realizadas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 52
    Width = 1007
    Height = 44
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1003
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 12
        Height = 17
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 12
        Height = 17
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object QrOVmItxDevCab: TMySQLQuery
    BeforeOpen = QrOVmItxDevCabBeforeOpen
    AfterOpen = QrOVmItxDevCabAfterOpen
    BeforeClose = QrOVmItxDevCabBeforeClose
    AfterScroll = QrOVmItxDevCabAfterScroll
    SQL.Strings = (
      'SELECT  mdc.UserNmePdr, igc.*, ymc.Nome NO_OVcYnsExg,'
      'dlo.Nome NO_Local, ref.Nome NO_Referencia,'
      'isc.Nome NO_InspResul, '
      'IF(igc.DtHrAbert  <= "1899-12-30", "",'
      '  DATE_FORMAT(igc.DtHrAbert, "%d/%m/%Y %H:%i")) DtHrAbert_TXT,'
      'IF(igc.DtHrFecha  <= "1899-12-30", "",'
      '  DATE_FORMAT(igc.DtHrFecha, "%d/%m/%Y %H:%i")) DtHrFecha_TXT'
      'FROM ovmitxdevcab igc'
      'LEFT JOIN ovdlocal dlo ON dlo.Codigo=igc.Local'
      'LEFT JOIN ovdreferencia ref ON ref.Codigo=igc.SeqGrupo'
      'LEFT JOIN ovcynsmedcad ymc ON ymc.Codigo=igc.OVcYnsExg'
      'LEFT JOIN ovgisprescad isc ON isc.Codigo=igc.InspResul'
      'LEFT JOIN ovcmobdevcad mdc ON mdc.Codigo=igc.DeviceSI'
      'WHERE igc.Codigo > 0')
    Left = 60
    Top = 64
    object QrOVmItxDevCabCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrOVmItxDevCabCodInMob: TIntegerField
      FieldName = 'CodInMob'
      Required = True
    end
    object QrOVmItxDevCabDeviceSI: TIntegerField
      FieldName = 'DeviceSI'
      Required = True
    end
    object QrOVmItxDevCabDeviceID: TWideStringField
      FieldName = 'DeviceID'
      Size = 60
    end
    object QrOVmItxDevCabDeviceCU: TIntegerField
      FieldName = 'DeviceCU'
      Required = True
    end
    object QrOVmItxDevCabNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QrOVmItxDevCabOVgItxGer: TIntegerField
      FieldName = 'OVgItxGer'
      Required = True
    end
    object QrOVmItxDevCabLocal: TIntegerField
      FieldName = 'Local'
      Required = True
    end
    object QrOVmItxDevCabNrOP: TIntegerField
      FieldName = 'NrOP'
      Required = True
    end
    object QrOVmItxDevCabSeqGrupo: TIntegerField
      FieldName = 'SeqGrupo'
      Required = True
    end
    object QrOVmItxDevCabNrReduzidoOP: TIntegerField
      FieldName = 'NrReduzidoOP'
      Required = True
    end
    object QrOVmItxDevCabDtHrAbert: TDateTimeField
      FieldName = 'DtHrAbert'
      Required = True
    end
    object QrOVmItxDevCabDtHrFecha: TDateTimeField
      FieldName = 'DtHrFecha'
      Required = True
    end
    object QrOVmItxDevCabOVcYnsExg: TIntegerField
      FieldName = 'OVcYnsExg'
      Required = True
    end
    object QrOVmItxDevCabPecasItx: TIntegerField
      FieldName = 'PecasItx'
      Required = True
    end
    object QrOVmItxDevCabPecaAtual: TIntegerField
      FieldName = 'PecaAtual'
      Required = True
    end
    object QrOVmItxDevCabLk: TIntegerField
      FieldName = 'Lk'
      Required = True
    end
    object QrOVmItxDevCabDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrOVmItxDevCabDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrOVmItxDevCabUserCad: TIntegerField
      FieldName = 'UserCad'
      Required = True
    end
    object QrOVmItxDevCabUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Required = True
    end
    object QrOVmItxDevCabAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Required = True
    end
    object QrOVmItxDevCabAWServerID: TIntegerField
      FieldName = 'AWServerID'
      Required = True
    end
    object QrOVmItxDevCabAWStatSinc: TSmallintField
      FieldName = 'AWStatSinc'
      Required = True
    end
    object QrOVmItxDevCabAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
    object QrOVmItxDevCabQtReal: TFloatField
      FieldName = 'QtReal'
      Required = True
    end
    object QrOVmItxDevCabQtLocal: TFloatField
      FieldName = 'QtLocal'
      Required = True
    end
    object QrOVmItxDevCabProduto: TIntegerField
      FieldName = 'Produto'
      Required = True
    end
    object QrOVmItxDevCabCodGrade: TIntegerField
      FieldName = 'CodGrade'
      Required = True
    end
    object QrOVmItxDevCabCodTam: TWideStringField
      FieldName = 'CodTam'
      Size = 30
    end
    object QrOVmItxDevCabPontosTot: TIntegerField
      FieldName = 'PontosTot'
      Required = True
    end
    object QrOVmItxDevCabInspResul: TIntegerField
      FieldName = 'InspResul'
      Required = True
    end
    object QrOVmItxDevCabInspeSeq: TIntegerField
      FieldName = 'InspeSeq'
      Required = True
    end
    object QrOVmItxDevCabRandmStr: TWideStringField
      FieldName = 'RandmStr'
      Required = True
      Size = 32
    end
    object QrOVmItxDevCabDtHrUpIni: TDateTimeField
      FieldName = 'DtHrUpIni'
      Required = True
    end
    object QrOVmItxDevCabDtHrUpFim: TDateTimeField
      FieldName = 'DtHrUpFim'
      Required = True
    end
    object QrOVmItxDevCabEmpresa: TIntegerField
      FieldName = 'Empresa'
      Required = True
    end
    object QrOVmItxDevCabNO_OVcYnsExg: TWideStringField
      FieldName = 'NO_OVcYnsExg'
      Size = 60
    end
    object QrOVmItxDevCabNO_Local: TWideStringField
      FieldName = 'NO_Local'
      Size = 100
    end
    object QrOVmItxDevCabNO_Referencia: TWideStringField
      FieldName = 'NO_Referencia'
      Size = 100
    end
    object QrOVmItxDevCabNO_InspResul: TWideStringField
      FieldName = 'NO_InspResul'
      Size = 60
    end
    object QrOVmItxDevCabDtHrAbert_TXT: TWideStringField
      DisplayWidth = 19
      FieldName = 'DtHrAbert_TXT'
      Size = 19
    end
    object QrOVmItxDevCabDtHrFecha_TXT: TWideStringField
      DisplayWidth = 19
      FieldName = 'DtHrFecha_TXT'
      Size = 19
    end
    object QrOVmItxDevCabUserNmePdr: TWideStringField
      FieldName = 'UserNmePdr'
      Size = 60
    end
    object QrOVmItxDevCabPerExgReal: TFloatField
      FieldName = 'PerExgReal'
      Required = True
      DisplayFormat = '0.00'
    end
    object QrOVmItxDevCabDtHrMailSnt: TFloatField
      FieldName = 'DtHrMailSnt'
      Required = True
    end
    object QrOVmItxDevCabSeccaoOP: TWideStringField
      FieldName = 'SeccaoOP'
      Size = 60
    end
    object QrOVmItxDevCabSeccaoMaq: TWideStringField
      FieldName = 'SeccaoMaq'
      Size = 60
    end
    object QrOVmItxDevCabSegmntInsp: TIntegerField
      FieldName = 'SegmntInsp'
      Required = True
    end
    object QrOVmItxDevCabSeccaoInsp: TIntegerField
      FieldName = 'SeccaoInsp'
      Required = True
    end
    object QrOVmItxDevCabBatelada: TIntegerField
      FieldName = 'Batelada'
    end
  end
  object DsOVmItxDevCab: TDataSource
    DataSet = QrOVmItxDevCab
    Left = 92
    Top = 64
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = BtEmail
    CanUpd01 = BtAltera
    CanDel01 = BtExclui
    Left = 120
    Top = 64
  end
  object QrPontosNeg: TMySQLQuery
    Database = Dmod.ZZDB
    BeforeClose = QrPontosNegBeforeClose
    AfterScroll = QrPontosNegAfterScroll
    SQL.Strings = (
      'SELECT  "3" IdTabela, "Exigencia" Tabela,'
      'mmc.CtrlInMob AS CtrlInMob, mmc.PecaSeq AS PecaSeq,'
      'yqt.Nome NO_Topico, mmc.PontNeg AS PontNeg,'
      'mmc.QtdFotos AS QtdFotos,'
      'mmc.Magnitude AS Magnitude'
      'FROM ovmitxdevexg mmc'
      'LEFT JOIN ovcynsmixtop yqt ON yqt.Codigo=mmc.Topyko'
      'WHERE mmc.CodInMob=0'
      ''
      'UNION'
      ''
      'SELECT  "4" IdTabela, "LivreTexto" Tabela,'
      'mml.CtrlInMob AS CtrlInMob, mml.PecaSeq AS PecaSeq,'
      'mml.Descricao NO_Topico, mml.PontNeg AS PontNeg,'
      'mml.QtdFotos AS QtdFotos,'
      'mml.Magnitude AS Magnitude'
      'FROM ovmitxdevlvr mml'
      'WHERE mml.CodInMob=0'
      ''
      'ORDER BY PecaSeq DESC, IdTabela, CtrlInMob')
    Left = 512
    Top = 9
    object QrPontosNegPecaSeq: TIntegerField
      FieldName = 'PecaSeq'
      Required = True
    end
    object QrPontosNegNO_Topico: TWideStringField
      FieldName = 'NO_Topico'
      Size = 60
    end
    object QrPontosNegPontNeg: TIntegerField
      FieldName = 'PontNeg'
      Required = True
    end
    object QrPontosNegIdTabela: TWideStringField
      FieldName = 'IdTabela'
      Size = 1
    end
    object QrPontosNegTabela: TWideStringField
      FieldName = 'Tabela'
    end
    object QrPontosNegQtdFotos: TIntegerField
      FieldName = 'QtdFotos'
    end
    object QrPontosNegCtrlInMob: TIntegerField
      FieldName = 'CtrlInMob'
      Required = True
    end
  end
  object DsPontosNeg: TDataSource
    DataSet = QrPontosNeg
    Left = 512
    Top = 57
  end
  object QrOVmItxDevFts: TMySQLQuery
    Database = Dmod.ZZDB
    AfterScroll = QrOVmItxDevFtsAfterScroll
    SQL.Strings = (
      'SELECT * FROM ovmitxdevfts')
    Left = 604
    Top = 9
    object QrOVmItxDevFtsCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrOVmItxDevFtsIdTabela: TSmallintField
      FieldName = 'IdTabela'
      Required = True
    end
    object QrOVmItxDevFtsCodInMob: TIntegerField
      FieldName = 'CodInMob'
      Required = True
    end
    object QrOVmItxDevFtsCtrlInMob: TIntegerField
      FieldName = 'CtrlInMob'
      Required = True
    end
    object QrOVmItxDevFtsDataHora: TDateTimeField
      FieldName = 'DataHora'
      Required = True
    end
    object QrOVmItxDevFtsNomeArq: TWideStringField
      FieldName = 'NomeArq'
      Size = 255
    end
    object QrOVmItxDevFtsNoArqSvr: TWideStringField
      FieldName = 'NoArqSvr'
      Size = 255
    end
    object QrOVmItxDevFtsLk: TIntegerField
      FieldName = 'Lk'
      Required = True
    end
    object QrOVmItxDevFtsDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrOVmItxDevFtsDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrOVmItxDevFtsUserCad: TIntegerField
      FieldName = 'UserCad'
      Required = True
    end
    object QrOVmItxDevFtsUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Required = True
    end
    object QrOVmItxDevFtsAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Required = True
    end
    object QrOVmItxDevFtsAWServerID: TIntegerField
      FieldName = 'AWServerID'
      Required = True
    end
    object QrOVmItxDevFtsAWStatSinc: TSmallintField
      FieldName = 'AWStatSinc'
      Required = True
    end
    object QrOVmItxDevFtsAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
  end
  object DsOVmItxDevFts: TDataSource
    DataSet = QrOVmItxDevFts
    Left = 604
    Top = 57
  end
  object QrOVmItxDevBmp: TMySQLQuery
    Database = Dmod.ZZDB
    Left = 584
    Top = 413
    object QrOVmItxDevBmpTxtBmp: TWideMemoField
      FieldName = 'TxtBmp'
      BlobType = ftWideMemo
    end
  end
end
