object FmImportaCSV_ERP_01: TFmImportaCSV_ERP_01
  Left = 339
  Top = 185
  Caption = 'OVS-IMPRT-001 :: Importa'#231#227'o de dados CSV'
  ClientHeight = 629
  ClientWidth = 812
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 812
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 764
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 716
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 319
        Height = 32
        Caption = 'Importa'#231#227'o de dados CSV'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 319
        Height = 32
        Caption = 'Importa'#231#227'o de dados CSV'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 319
        Height = 32
        Caption = 'Importa'#231#227'o de dados CSV'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 812
    Height = 504
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 812
      Height = 504
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 812
        Height = 504
        Align = alClient
        TabOrder = 0
        object Panel5: TPanel
          Left = 2
          Top = 15
          Width = 808
          Height = 487
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          object Splitter1: TSplitter
            Left = 0
            Top = 384
            Width = 808
            Height = 5
            Cursor = crVSplit
            Align = alBottom
            ExplicitTop = 330
          end
          object Panel7: TPanel
            Left = 0
            Top = 173
            Width = 808
            Height = 211
            Align = alClient
            Caption = 'Panel7'
            TabOrder = 0
            object GradeA: TStringGrid
              Left = 1
              Top = 1
              Width = 806
              Height = 209
              Align = alClient
              DefaultColWidth = 40
              DefaultRowHeight = 17
              RowCount = 2
              Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goColSizing]
              TabOrder = 0
              ColWidths = (
                40
                370
                119
                110
                116)
            end
          end
          object Panel10: TPanel
            Left = 0
            Top = 389
            Width = 808
            Height = 98
            Align = alBottom
            BevelOuter = bvNone
            TabOrder = 1
            object MeAvisos: TMemo
              Left = 0
              Top = 0
              Width = 808
              Height = 98
              Align = alClient
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Lucida Console'
              Font.Style = []
              ParentFont = False
              TabOrder = 0
            end
          end
          object PCAcao: TPageControl
            Left = 0
            Top = 0
            Width = 808
            Height = 173
            ActivePage = TabSheet1
            Align = alTop
            TabOrder = 2
            object TabSheet1: TTabSheet
              Caption = 'Carregar dados'
              ExplicitLeft = 0
              ExplicitTop = 0
              ExplicitWidth = 0
              ExplicitHeight = 0
              object Panel9: TPanel
                Left = 0
                Top = 0
                Width = 800
                Height = 89
                Align = alTop
                BevelOuter = bvNone
                TabOrder = 0
                object Label1: TLabel
                  Left = 4
                  Top = 8
                  Width = 238
                  Height = 13
                  Caption = 'Diret'#243'rio de rastreio para carregamento dos dados:'
                end
                object SbLoadCSVOthDir: TSpeedButton
                  Left = 772
                  Top = 24
                  Width = 23
                  Height = 22
                  Caption = '...'
                  OnClick = SbLoadCSVOthDirClick
                end
                object Label95: TLabel
                  Left = 112
                  Top = 32
                  Width = 77
                  Height = 13
                  Caption = 'Tipo logradouro:'
                end
                object Label2: TLabel
                  Left = 4
                  Top = 48
                  Width = 163
                  Height = 13
                  Caption = 'Esquema de layout de importa'#231#227'o:'
                end
                object EdLoadCSVOthDir: TEdit
                  Left = 4
                  Top = 24
                  Width = 765
                  Height = 21
                  TabOrder = 0
                end
                object EdOVpLayEsq: TdmkEditCB
                  Left = 4
                  Top = 64
                  Width = 56
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 1
                  FormatType = dmktfInteger
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ValMin = '-2147483647'
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0
                  ValWarn = False
                  DBLookupComboBox = CBOVpLayEsq
                  IgnoraDBLookupComboBox = False
                  AutoSetIfOnlyOneReg = setregOnlyManual
                end
                object CBOVpLayEsq: TdmkDBLookupComboBox
                  Left = 60
                  Top = 64
                  Width = 733
                  Height = 21
                  KeyField = 'Codigo'
                  ListField = 'Nome'
                  ListSource = DsOVpLayEsq
                  TabOrder = 2
                  dmkEditCB = EdOVpLayEsq
                  UpdType = utYes
                  LocF7SQLMasc = '$#'
                end
              end
              object Panel8: TPanel
                Left = 0
                Top = 92
                Width = 800
                Height = 53
                Align = alBottom
                BevelOuter = bvNone
                TabOrder = 1
                object Label5: TLabel
                  Left = 128
                  Top = 4
                  Width = 36
                  Height = 13
                  Caption = 'Tempo:'
                end
                object Label6: TLabel
                  Left = 360
                  Top = 28
                  Width = 85
                  Height = 13
                  Caption = 'Itens encerrados: '
                end
                object LaEncerrados: TLabel
                  Left = 480
                  Top = 28
                  Width = 9
                  Height = 13
                  Caption = '...'
                end
                object LaAEncerrar: TLabel
                  Left = 480
                  Top = 16
                  Width = 9
                  Height = 13
                  Caption = '...'
                  DragCursor = crAppStart
                end
                object Label7: TLabel
                  Left = 360
                  Top = 16
                  Width = 106
                  Height = 13
                  Caption = 'Itens aptos a encerrar:'
                  DragCursor = crAppStart
                end
                object BtCarregaDados: TBitBtn
                  Tag = 14
                  Left = 4
                  Top = 8
                  Width = 120
                  Height = 40
                  Caption = 'Carrega &Dados'
                  NumGlyphs = 2
                  TabOrder = 0
                  OnClick = BtCarregaDadosClick
                end
                object EdTempoCarrega: TdmkEdit
                  Left = 128
                  Top = 20
                  Width = 113
                  Height = 21
                  ReadOnly = True
                  TabOrder = 1
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = ''
                  ValWarn = False
                end
                object CkForcaRecarga: TCheckBox
                  Left = 648
                  Top = 0
                  Width = 97
                  Height = 17
                  Caption = 'For'#231'a recarga.'
                  TabOrder = 2
                end
                object CkForcaEncerramento: TCheckBox
                  Left = 648
                  Top = 16
                  Width = 121
                  Height = 17
                  Caption = 'For'#231'a encerramento.'
                  TabOrder = 3
                end
                object CkForcaReabertura: TCheckBox
                  Left = 648
                  Top = 32
                  Width = 121
                  Height = 17
                  Caption = 'For'#231'a reabertura.'
                  TabOrder = 4
                end
                object CkReplace: TCheckBox
                  Left = 528
                  Top = 32
                  Width = 97
                  Height = 17
                  Caption = 'Substitui dados.'
                  TabOrder = 5
                end
                object Button1: TButton
                  Left = 252
                  Top = 8
                  Width = 101
                  Height = 40
                  Caption = 'Encerra manual'
                  TabOrder = 6
                  OnClick = Button1Click
                end
              end
            end
            object TabSheet2: TTabSheet
              Caption = 'Criar layout'
              ImageIndex = 1
              ExplicitLeft = 0
              ExplicitTop = 0
              ExplicitWidth = 0
              ExplicitHeight = 0
              object Panel11: TPanel
                Left = 0
                Top = 0
                Width = 800
                Height = 89
                Align = alTop
                BevelOuter = bvNone
                TabOrder = 0
                object Label3: TLabel
                  Left = 4
                  Top = 8
                  Width = 194
                  Height = 13
                  Caption = 'Diret'#243'rio de rastreio de layouts a importar:'
                end
                object SbLoadLayOutDir: TSpeedButton
                  Left = 772
                  Top = 24
                  Width = 23
                  Height = 22
                  Caption = '...'
                  OnClick = SbLoadLayOutDirClick
                end
                object Label4: TLabel
                  Left = 112
                  Top = 32
                  Width = 77
                  Height = 13
                  Caption = 'Tipo logradouro:'
                end
                object EdLoadLayOutDir: TEdit
                  Left = 4
                  Top = 24
                  Width = 765
                  Height = 21
                  TabOrder = 0
                end
              end
              object Panel12: TPanel
                Left = 0
                Top = 92
                Width = 800
                Height = 53
                Align = alBottom
                BevelOuter = bvNone
                TabOrder = 1
                object BtCriaLayouy: TBitBtn
                  Tag = 14
                  Left = 12
                  Top = 8
                  Width = 120
                  Height = 40
                  Caption = 'Cria &Layout'
                  NumGlyphs = 2
                  TabOrder = 0
                  OnClick = BtCriaLayouyClick
                end
              end
            end
            object TabSheet3: TTabSheet
              Caption = 'Lista de arquivos encontrados do diret'#243'rio'
              ImageIndex = 2
              ExplicitLeft = 0
              ExplicitTop = 0
              ExplicitWidth = 0
              ExplicitHeight = 0
              object Panel6: TPanel
                Left = 0
                Top = 0
                Width = 800
                Height = 145
                Align = alClient
                BevelOuter = bvNone
                TabOrder = 0
                object LBFound: TListBox
                  Left = 0
                  Top = 0
                  Width = 800
                  Height = 145
                  Align = alClient
                  ItemHeight = 13
                  TabOrder = 0
                end
              end
            end
            object TabSheet4: TTabSheet
              Caption = 'Lista de arquivos lidos'
              ImageIndex = 3
              ExplicitLeft = 0
              ExplicitTop = 0
              ExplicitWidth = 0
              ExplicitHeight = 0
              object LstLoaded: TListBox
                Left = 0
                Top = 0
                Width = 800
                Height = 145
                Align = alClient
                ItemHeight = 13
                TabOrder = 0
              end
            end
            object TabSheet5: TTabSheet
              Caption = 'Lista de arquivos n'#227'o lidos'
              ImageIndex = 4
              ExplicitLeft = 0
              ExplicitTop = 0
              ExplicitWidth = 0
              ExplicitHeight = 0
              object LstNaoLoad: TListBox
                Left = 0
                Top = 0
                Width = 800
                Height = 145
                Align = alClient
                ItemHeight = 13
                TabOrder = 0
              end
            end
            object TabSheet6: TTabSheet
              Caption = 'Teste'
              ImageIndex = 5
              ExplicitLeft = 0
              ExplicitTop = 0
              ExplicitWidth = 0
              ExplicitHeight = 0
              object Panel13: TPanel
                Left = 0
                Top = 0
                Width = 800
                Height = 41
                Align = alTop
                TabOrder = 0
                object Button2: TButton
                  Left = 4
                  Top = 4
                  Width = 133
                  Height = 29
                  Caption = 'SQL Filtro'
                  TabOrder = 0
                  OnClick = Button2Click
                end
              end
              object MeTeste: TMemo
                Left = 0
                Top = 41
                Width = 800
                Height = 104
                Align = alClient
                TabOrder = 1
              end
            end
          end
        end
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 552
    Width = 812
    Height = 77
    Align = alBottom
    TabOrder = 2
    object PnSaiDesis: TPanel
      Left = 666
      Top = 15
      Width = 144
      Height = 60
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 664
      Height = 60
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GBAvisos1: TGroupBox
        Left = 0
        Top = -3
        Width = 664
        Height = 63
        Align = alBottom
        Caption = ' Avisos: '
        TabOrder = 0
        object Panel4: TPanel
          Left = 2
          Top = 15
          Width = 660
          Height = 46
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          object LaAviso1: TLabel
            Left = 13
            Top = 2
            Width = 120
            Height = 17
            Caption = '..............................'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clSilver
            Font.Height = -15
            Font.Name = 'Arial'
            Font.Style = []
            ParentFont = False
            Transparent = True
          end
          object LaAviso2: TLabel
            Left = 12
            Top = 1
            Width = 120
            Height = 17
            Caption = '..............................'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clRed
            Font.Height = -15
            Font.Name = 'Arial'
            Font.Style = []
            ParentFont = False
            Transparent = True
          end
          object PB: TProgressBar
            Left = 0
            Top = 29
            Width = 660
            Height = 17
            Align = alBottom
            TabOrder = 0
          end
        end
      end
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 356
    Top = 115
  end
  object QrOVpLayEsq: TMySQLQuery
    Database = FmVerificaConexoes.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome '
      'FROM ovplayesq'
      'ORDER BY Nome')
    Left = 70
    Top = 300
    object QrOVpLayEsqCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrOVpLayEsqNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
  end
  object DsOVpLayEsq: TDataSource
    DataSet = QrOVpLayEsq
    Left = 70
    Top = 348
  end
  object QrFld: TMySQLQuery
    Database = FmVerificaConexoes.MyDB
    Left = 346
    Top = 311
    object QrFldTabela: TWideStringField
      FieldName = 'Tabela'
      Size = 60
    end
    object QrFldCampo: TWideStringField
      FieldName = 'Campo'
      Size = 60
    end
    object QrFldOrdDataType: TLargeintField
      FieldName = 'OrdDataType'
    end
    object QrFldNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
  end
  object DqAux: TMySQLDirectQuery
    Database = FmVerificaConexoes.MyDB
    Left = 346
    Top = 360
  end
  object QrLL: TMySQLQuery
    Database = FmVerificaConexoes.MyDB
    Left = 462
    Top = 296
  end
  object QrOVpDirXtr: TMySQLQuery
    Database = FmVerificaConexoes.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM ovpdirxtr'
      'ORDER BU Nome')
    Left = 464
    Top = 348
    object QrOVpDirXtrCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrOVpDirXtrNome: TWideStringField
      FieldName = 'Nome'
      Size = 255
    end
  end
  object QrTab: TMySQLQuery
    Database = FmVerificaConexoes.MyDB
    Left = 254
    Top = 332
  end
  object QrEncerrados: TMySQLQuery
    Database = FmVerificaConexoes.MyDB
    SQL.Strings = (
      'DROP TABLE IF EXISTS _Alheio_Encerrado_;'
      'CREATE TABLE _Alheio_Encerrado_'
      'SELECT DISTINCT NrReduzidoOP, Local, SeqGrupo'
      'FROM ovfordemproducao'
      'WHERE LastInsrt="2019-11-20 16:56:59"'
      ';'
      ''
      'DROP TABLE IF EXISTS _Alheio_Configurado_;'
      'CREATE TABLE _Alheio_Configurado_'
      'SELECT DISTINCT NrReduzidoOP, Local, SeqGrupo'
      'FROM ovgispgercab'
      'WHERE ZtatusIsp=5120;'
      ''
      'SELECT enc.NrReduzidoOP enc_NrReduzidoOP,'
      'cfg.*'
      'FROM _Alheio_Configurado_ cfg'
      'LEFT JOIN _Alheio_Encerrado_ enc'
      '  ON enc.NrReduzidoOP=cfg.NrReduzidoOP'
      'AND enc.Local=cfg.Local'
      'AND enc.SeqGrupo=cfg.SeqGrupo'
      'WHERE enc.NrReduzidoOP IS NULL')
    Left = 594
    Top = 352
    object QrEncerradosenc_NrReduzidoOP: TIntegerField
      FieldName = 'enc_NrReduzidoOP'
    end
    object QrEncerradosNrReduzidoOP: TIntegerField
      FieldName = 'NrReduzidoOP'
      Required = True
    end
    object QrEncerradosLocal: TIntegerField
      FieldName = 'Local'
      Required = True
    end
    object QrEncerradosSeqGrupo: TIntegerField
      FieldName = 'SeqGrupo'
      Required = True
    end
  end
  object QrAux: TMySQLQuery
    Database = FmVerificaConexoes.MyDB
    Left = 570
    Top = 280
  end
end
