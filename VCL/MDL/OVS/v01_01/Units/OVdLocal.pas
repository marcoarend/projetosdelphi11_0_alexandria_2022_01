unit OVdLocal;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2,
  ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, dmkGeral,
  dmkPermissoes, dmkEdit, dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup,
  unDmkProcFunc, UnDmkEnums;

type
  TFmOVdLocal = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    BtExclui: TBitBtn;
    BtAltera: TBitBtn;
    BtInclui: TBitBtn;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    QrOVdLocal: TMySQLQuery;
    DsOVdLocal: TDataSource;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    Label9: TLabel;
    EdNome: TdmkEdit;
    Label1: TLabel;
    DBEdCodigo: TdmkDBEdit;
    Label2: TLabel;
    DBEdNome: TdmkDBEdit;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    QrOVdLocalCodigo: TIntegerField;
    QrOVdLocalNome: TWideStringField;
    QrOVdLocalFornecedor: TIntegerField;
    QrOVdLocalQtFuncionario: TIntegerField;
    QrOVdLocalEficiencia: TFloatField;
    QrOVdLocalNrTempo: TFloatField;
    QrOVdLocalQtMetadia: TFloatField;
    QrOVdLocalExterno: TWideStringField;
    QrOVdLocalDsInativo: TWideStringField;
    QrOVdLocalReInsrt: TIntegerField;
    QrOVdLocalLastInsrt: TDateTimeField;
    QrOVdLocalLk: TIntegerField;
    QrOVdLocalDataCad: TDateField;
    QrOVdLocalDataAlt: TDateField;
    QrOVdLocalUserCad: TIntegerField;
    QrOVdLocalUserAlt: TIntegerField;
    QrOVdLocalAlterWeb: TSmallintField;
    QrOVdLocalAWServerID: TIntegerField;
    QrOVdLocalAWStatSinc: TSmallintField;
    QrOVdLocalAtivo: TSmallintField;
    QrOVdLocalNO_FORNECE: TWideStringField;
    Label3: TLabel;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    Label4: TLabel;
    DBEdit3: TDBEdit;
    Label5: TLabel;
    DBEdit4: TDBEdit;
    Label6: TLabel;
    DBEdit5: TDBEdit;
    Label8: TLabel;
    DBEdit6: TDBEdit;
    Label10: TLabel;
    DBEdit7: TDBEdit;
    Label11: TLabel;
    DBEdit8: TDBEdit;
    Label12: TLabel;
    DBEdit9: TDBEdit;
    SpeedButton5: TSpeedButton;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrOVdLocalAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrOVdLocalBeforeOpen(DataSet: TDataSet);
    procedure BtIncluiClick(Sender: TObject);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure SpeedButton5Click(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure LocCod(Atual, Codigo: Integer);
    procedure Va(Para: TVaiPara);
  public
    { Public declarations }
  end;

var
  FmOVdLocal: TFmOVdLocal;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, UnEntities,
  Module;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmOVdLocal.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmOVdLocal.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrOVdLocalCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmOVdLocal.DefParams;
begin
  VAR_GOTOTABELA := 'ovdlocal';
  VAR_GOTOMYSQLTABLE := QrOVdLocal;
  VAR_GOTONEG := gotoPos;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_FORNECE,');
  VAR_SQLx.Add('loc.*');
  VAR_SQLx.Add('FROM ovdlocal loc');
  VAR_SQLx.Add('LEFT JOIN entidades frn ON frn.Codigo=loc.Fornecedor');
  VAR_SQLx.Add('WHERE loc.Codigo > 0');
  //
  VAR_SQL1.Add('AND loc.Codigo=:P0');
  //
  //VAR_SQL2.Add('AND CodUsu=:P0');
  //
  VAR_SQLa.Add('AND loc.Nome Like :P0');
  //
end;

procedure TFmOVdLocal.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmOVdLocal.QueryPrincipalAfterOpen;
begin
end;

procedure TFmOVdLocal.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARAÇÕES///////////////////

procedure TFmOVdLocal.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmOVdLocal.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmOVdLocal.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmOVdLocal.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmOVdLocal.SpeedButton5Click(Sender: TObject);
var
  Entidade: Integer;
begin
  Entidade := 0;
  if QrOVdLocal.State <> dsInactive then
    Entidade := QrOVdLocalFornecedor.Value;
  Entities.CadastroDeEntidade(Entidade, fmCadSelecionar, fmCadSelecionar, False);
end;

procedure TFmOVdLocal.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmOVdLocal.BtAlteraClick(Sender: TObject);
begin
(*
  if (QrOVdLocal.State <> dsInactive) and (QrOVdLocal.RecordCount > 0) then
  begin
    UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrOVdLocal, [PnDados],
      [PnEdita], EdNome, ImgTipo, 'ovdlocal');
  end;
*)
end;

procedure TFmOVdLocal.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrOVdLocalCodigo.Value;
  Close;
end;

procedure TFmOVdLocal.BtConfirmaClick(Sender: TObject);
var
  Codigo: Integer;
  Nome: String;
begin
  Nome := EdNome.ValueVariant;
  if MyObjects.FIC(Length(Nome) = 0, EdNome, 'Defina uma descrição!') then Exit;
  //
  Codigo := UMyMod.BPGS1I32('ovdlocal', 'Codigo', '', '',
    tsPos, ImgTipo.SQLType, QrOVdLocalCodigo.Value);
  if UMyMod.ExecSQLInsUpdPanel(ImgTipo.SQLType, Self, PnEdita,
    'ovdlocal', Codigo, Dmod.QrUpd, [PnEdita], [PnDados], ImgTipo, True) then
  begin
(*  Desmarcar se usar "UMyMod.SQLInsUpd(...)" em vez de "UMyMod.ExecSQLInsUpdPanel(...)"
    ImgTipo.SQLType := stLok;
    PnDados.Visible := True;
    PnEdita.Visible := False;
    GOTOy.BotoesSb(ImgTipo.SQLType);
*)
    LocCod(Codigo, Codigo);
  end;
end;

procedure TFmOVdLocal.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := EdCodigo.ValueVariant;
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  //
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'ovdlocal', 'Codigo');
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmOVdLocal.BtIncluiClick(Sender: TObject);
begin
(*
  UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrOVdLocal, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'ovdlocal');
*)
end;

procedure TFmOVdLocal.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  GBEdita.Align := alClient;
  GBDados.Align := alClient;
  CriaOForm;
end;

procedure TFmOVdLocal.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrOVdLocalCodigo.Value, LaRegistro.Caption);
end;

procedure TFmOVdLocal.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmOVdLocal.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrOVdLocalCodigo.Value, LaRegistro.Caption);
end;

procedure TFmOVdLocal.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmOVdLocal.QrOVdLocalAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmOVdLocal.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmOVdLocal.SbQueryClick(Sender: TObject);
begin
  LocCod(QrOVdLocalCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'ovdlocal', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmOVdLocal.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmOVdLocal.QrOVdLocalBeforeOpen(DataSet: TDataSet);
begin
  QrOVdLocalCodigo.DisplayFormat := FFormatFloat;
end;

end.

