unit OVgLocJGC;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkPermissoes,
  dmkDBLookupComboBox, dmkEditCB, mySQLDbTables, dmkDBGridZTO;

type
  TFmOVgLocJGC = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    GroupBox2: TGroupBox;
    Panel5: TPanel;
    Label7: TLabel;
    EdLocal: TdmkEditCB;
    CBLocal: TdmkDBLookupComboBox;
    QrOVdLocal: TMySQLQuery;
    DsOVdLocal: TDataSource;
    EdNrOP: TdmkEdit;
    Label3: TLabel;
    Label6: TLabel;
    EdNrReduzidoOP: TdmkEdit;
    Label1: TLabel;
    EdArtigo: TdmkEditCB;
    CBArtigo: TdmkDBLookupComboBox;
    QrOVdReferencia: TMySQLQuery;
    DsOVdReferencia: TDataSource;
    EdReferencia: TdmkEdit;
    Label2: TLabel;
    QrOVdLocalCodigo: TIntegerField;
    QrOVdLocalNome: TWideStringField;
    QrOVdReferenciaCodigo: TIntegerField;
    QrOVdReferenciaNome: TWideStringField;
    QrOVgItxGerCab: TMySQLQuery;
    QrOVgItxGerCabCodigo: TIntegerField;
    QrOVgItxGerCabNome: TWideStringField;
    QrOVgItxGerCabLocal: TIntegerField;
    QrOVgItxGerCabNrOP: TIntegerField;
    QrOVgItxGerCabSeqGrupo: TIntegerField;
    QrOVgItxGerCabNrReduzidoOP: TIntegerField;
    QrOVgItxGerCabDtHrAbert: TDateTimeField;
    QrOVgItxGerCabDtHrFecha: TDateTimeField;
    QrOVgItxGerCabOVcYnsExg: TIntegerField;
    QrOVgItxGerCabZtatusIsp: TIntegerField;
    QrOVgItxGerCabZtatusDtH: TDateTimeField;
    QrOVgItxGerCabZtatusMot: TIntegerField;
    QrOVgItxGerCabLk: TIntegerField;
    QrOVgItxGerCabDataCad: TDateField;
    QrOVgItxGerCabDataAlt: TDateField;
    QrOVgItxGerCabUserCad: TIntegerField;
    QrOVgItxGerCabUserAlt: TIntegerField;
    QrOVgItxGerCabAlterWeb: TSmallintField;
    QrOVgItxGerCabAWServerID: TIntegerField;
    QrOVgItxGerCabAWStatSinc: TSmallintField;
    QrOVgItxGerCabAtivo: TSmallintField;
    QrOVgItxGerCabNO_Local: TWideStringField;
    QrOVgItxGerCabNO_Referencia: TWideStringField;
    QrOVgItxGerCabNO_OVcYnsExg: TWideStringField;
    QrOVgItxGerCabNO_ZtatusIsp: TWideStringField;
    QrOVgItxGerCabDtHrAbert_TXT: TWideStringField;
    QrOVgItxGerCabDtHrFecha_TXT: TWideStringField;
    DsOVgItxGerCab: TDataSource;
    DBGrid1: TdmkDBGridZTO;
    Label4: TLabel;
    EdZtatusIsp: TdmkEditCB;
    CBZtatusIsp: TdmkDBLookupComboBox;
    QrOVgIspStaCad: TMySQLQuery;
    DsOVgIspStaCad: TDataSource;
    QrOVgIspStaCadCodigo: TIntegerField;
    QrOVgIspStaCadNome: TWideStringField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure EdArtigoChange(Sender: TObject);
    procedure EdArtigoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdReferenciaChange(Sender: TObject);
    procedure EdReferenciaExit(Sender: TObject);
    procedure EdReferenciaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBGrid1DblClick(Sender: TObject);
  private
    { Private declarations }
(*
    procedure PesquisaPorCodigo(Artigo: Integer; EdReferencia: TdmkEdit);
    procedure PesquisaPorReferencia(Limpa: Boolean; EdReferencia: TdmkEdit; EdArtigo:
              TdmkEditCB; CBArtigo: TdmkDBLookupComboBox);
*)

  public
    { Public declarations }
    FCodigo: Integer;
  end;

  var
  FmOVgLocJGC: TFmOVgLocJGC;

implementation

uses UnMyObjects, Module, UnMySQLCuringa, DmkDAC_PF, ModOVS;

{$R *.DFM}

procedure TFmOVgLocJGC.BtOKClick(Sender: TObject);
var
  Local, Artigo, NrOP, NrReduzidoOP, ZtatusIsp: Integer;
  SQL_Local, SQL_Artigo, SQL_NrOP, SQL_NrReduzidoOP, SQL_ZtatusIsp: String;
begin
  Local            := EdLocal.ValueVariant;
  Artigo           := EdArtigo.ValueVariant;
  NrOP             := EdNrOP.ValueVariant;
  NrReduzidoOP     := EdNrReduzidoOP.ValueVariant;
  ZtatusIsp        := EdZtatusIsp.ValueVariant;
  //
  SQL_Local        := '';
  SQL_Artigo       := '';
  SQL_NrOP         := '';
  SQL_NrReduzidoOP := '';
  SQL_ZtatusIsp    := '';
  //
  if Local <> 0 then
    SQL_Local := 'AND igc.Local=' + Geral.FF0(Local);
  if Artigo <> 0 then
    SQL_Artigo := 'AND igc.SeqGrupo=' + Geral.FF0(Artigo);
  if NrOP <> 0 then
    SQL_NrOP := 'AND igc.NrOP=' + Geral.FF0(NrOP);
  if NrReduzidoOP <> 0 then
    SQL_NrReduzidoOP := 'AND igc.NrReduzidoOP=' + Geral.FF0(NrReduzidoOP);
  if ZtatusIsp <> 0 then
    SQL_ZtatusIsp := 'AND igc.ZtatusIsp=' + Geral.FF0(ZtatusIsp);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrOVgItxGerCab, Dmod.MyDB, [
  'SELECT igc.*, ymc.Nome NO_OVcYnsExg, ',
  'dlo.Nome NO_Local, ref.Nome NO_Referencia,  ',
  'isc.Nome NO_ZtatusIsp, ',
  'IF(igc.DtHrAbert  <= "1899-12-30", "",   ',
  '  DATE_FORMAT(igc.DtHrAbert, "%d/%m/%Y %H:%i:%s:%s")) DtHrAbert_TXT,   ',
  'IF(igc.DtHrFecha  <= "1899-12-30", "",   ',
  '  DATE_FORMAT(igc.DtHrFecha, "%d/%m/%Y %H:%i:%s:%s")) DtHrFecha_TXT ',
  'FROM ovgitxgercab igc ',
  'LEFT JOIN ovdlocal dlo ON dlo.Codigo=igc.Local   ',
  'LEFT JOIN ovdreferencia ref ON ref.Codigo=igc.SeqGrupo  ',
  'LEFT JOIN ovcynsexgcad ymc ON ymc.Codigo=igc.OVcYnsExg ',
  'LEFT JOIN OVgIspStaCad isc ON isc.Codigo=igc.ZtatusIsp ',
  'WHERE igc.Codigo > 0 ',
  SQL_Local        ,
  SQL_Artigo       ,
  SQL_NrOP         ,
  SQL_NrReduzidoOP ,
  SQL_ZtatusIsp    ,
  EmptyStr]);
  MyObjects.Informa2(LaAviso1, LaAviso2, False,
    'Itens localizados: ' + Geral.FF0(QrOVgItxGerCab.RecordCount));
end;

procedure TFmOVgLocJGC.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmOVgLocJGC.DBGrid1DblClick(Sender: TObject);
begin
  if (QrOVgItxGerCab.State <> dsInactive) and (QrOVgItxGerCab.RecordCount > 0) then
  begin
    FCodigo := QrOVgItxGerCabCodigo.Value;
    Close;
  end;

end;

procedure TFmOVgLocJGC.EdArtigoChange(Sender: TObject);
begin
  if not EdReferencia.Focused then
    DmOVS.PesquisaPorCodigo(EdArtigo.ValueVariant, EdReferencia);
end;

procedure TFmOVgLocJGC.EdArtigoKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key=VK_F3 then
    CuringaLoc.Pesquisa('Codigo', 'Nome', 'Referencia', Dmod.MyDB,
    ''(*Extra*), EdArtigo, CBArtigo, dmktfInteger);
end;

procedure TFmOVgLocJGC.EdReferenciaChange(Sender: TObject);
begin
  if EdReferencia.Focused then
    DmOVS.PesquisaPorReferencia(False, EdReferencia, EdArtigo, CBArtigo);
end;

procedure TFmOVgLocJGC.EdReferenciaExit(Sender: TObject);
begin
  DmOVS.PesquisaPorReferencia(True, EdReferencia, EdArtigo, CBArtigo);
end;

procedure TFmOVgLocJGC.EdReferenciaKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key=VK_F3 then
    CuringaLoc.Pesquisa('Codigo', 'Nome', 'Referencia', Dmod.MyDB,
    ''(*Extra*), EdArtigo, CBArtigo, dmktfInteger)
end;

procedure TFmOVgLocJGC.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmOVgLocJGC.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  FCodigo := 0;
  UnDmkDAC_PF.AbreQuery(QrOVdLocal, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrOVdReferencia, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrOVgIspStaCad, Dmod.MyDB);
end;

procedure TFmOVgLocJGC.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

(*
procedure TFmOVgLocIGC.PesquisaPorCodigo(Artigo: Integer;
  EdReferencia: TdmkEdit);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrPesq2, Dmod.MyDB, [
  'SELECT Referencia ',
  'FROM ovdreferencia ',
  'WHERE Codigo= ' + Geral.FF0(Artigo),
  EmptyStr]);
  if QrPesq2.RecordCount > 0 then
  begin
    if EdReferencia.ValueVariant <> QrPesq2Referencia.Value then
      EdReferencia.ValueVariant := QrPesq2Referencia.Value;
  end else EdReferencia.ValueVariant := '';
end;

procedure TFmOVgLocIGC.PesquisaPorReferencia(Limpa: Boolean; EdReferencia: TdmkEdit;
  EdArtigo: TdmkEditCB; CBArtigo: TdmkDBLookupComboBox);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrPesq1, Dmod.MyDB, [
  'SELECT Codigo ',
  'FROM ovdreferencia ',
  'WHERE Referencia="' + EdReferencia.Text + '"',
  EmptyStr]);
  //
  if QrPesq1.RecordCount > 0 then
  begin
    if EdArtigo.ValueVariant <> QrPesq1Codigo.Value then
      EdArtigo.ValueVariant := QrPesq1Codigo.Value;
    if CBArtigo.KeyValue     <> QrPesq1Codigo.Value then
      CBArtigo.KeyValue     := QrPesq1Codigo.Value;
  end
  else if Limpa then
    EdReferencia.ValueVariant := '';
end;
*)

end.
