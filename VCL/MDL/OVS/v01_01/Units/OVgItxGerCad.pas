unit OVgItxGerCad;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkPermissoes,
  dmkDBLookupComboBox, dmkEditCB, mySQLDbTables, dmkRadioGroup;

type
  TFmOVgItxGerCad = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    GroupBox2: TGroupBox;
    PnDadosOri: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    EdLocal: TdmkEdit;
    EdNO_Local: TdmkEdit;
    EdNrOP: TdmkEdit;
    EdSeqGrupo: TdmkEdit;
    EdNO_SeqGrupo: TdmkEdit;
    EdNrReduzidoOP: TdmkEdit;
    GroupBox3: TGroupBox;
    Panel5: TPanel;
    QrOVcYnsExgCad: TMySQLQuery;
    QrOVcYnsExgCadCodigo: TIntegerField;
    QrOVcYnsExgCadNome: TWideStringField;
    DsOVcYnsExgCad: TDataSource;
    Label7: TLabel;
    EdOVcYnsExg: TdmkEditCB;
    CBOVcYnsExg: TdmkDBLookupComboBox;
    SbOVcYnsExg: TSpeedButton;
    Label13: TLabel;
    EdCodigo: TdmkEdit;
    RGPermiFinHow: TdmkRadioGroup;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure SbOVcYnsExgClick(Sender: TObject);
    procedure EdOVcYnsExgKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure CBOVcYnsExgKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
    FCodigo, FSeqGrupo, FZtatusIsp: Integer;
    //
    procedure ReopenOVcYnsExgCad(ArtigRef: Integer; Filtra: Boolean);
  end;

  var
  FmOVgItxGerCad: TFmOVgItxGerCad;

implementation

uses UnMyObjects, UnOVS_Jan, Module, DmkDAC_PF, UMySQLModule, ModuleGeral,
  UnOVS_Consts, UnOVS_PF;

{$R *.DFM}

procedure TFmOVgItxGerCad.BtOKClick(Sender: TObject);
var
  //DtHrFecha,
  DtHrAbert, ZtatusDtH: String;
  Codigo, Local, NrOP, SeqGrupo, NrReduzidoOP, OVcYnsExg,
  //OVcYnsChk, LimiteChk, LimiteMed, , OVcYnsARQ,
  PermiFinHow, ZtatusIsp, ZtatusMot,
  SegmntInsp, SeccaoInsp: Integer;
  SQLType: TSQLType;
  Agora: TDateTime;
begin
  SQLType        := ImgTipo.SQLType;
  Agora          := DModG.ObtemAgora();
  Codigo         := EdCodigo.ValueVariant;
  Local          := EdLocal.ValueVariant;
  NrOP           := EdNrOP.ValueVariant;
  SeqGrupo       := EdSeqGrupo.ValueVariant;
  NrReduzidoOP   := EdNrReduzidoOP.ValueVariant;
  DtHrAbert      := Geral.FDT(Agora, 109);
  //DtHrFecha      := ;
  OVcYnsExg      := EdOVcYnsExg.ValueVariant;
(*
  OVcYnsChk      := EdOVcYnsChk.ValueVariant;
  LimiteChk      := EdLimiteChk.ValueVariant;
  LimiteMed      := EdLimiteMed.ValueVariant;
  OVcYnsARQ      := EdOVcYnsARQ.ValueVariant;
*)
  PermiFinHow    := RGPermiFinHow.ItemIndex;
  ZtatusIsp      := FZtatusIsp; //CO_OVS_IMPORT_ALHEIO_5...;
  ZtatusDtH      := DtHrAbert;
  ZtatusMot      := 0;
  //
  if MyObjects.FIC(OVcYnsExg = 0, EdOVcYnsExg,
    '"Informe a tabela de exa��es"') then Exit;
  //
(*
  if MyObjects.FIC(OVcYnsChk = 0, EdOVcYnsChk,
    '"Informe o check list de inconformidades"') then Exit;
  //
  if MyObjects.FIC(OVcYnsARQ = 0, EdOVcYnsARQ,
    '"Plano de Amostragem e Regime de Qualidade"') then Exit;
  //
*)
  if MyObjects.FIC(PermiFinHow < 1, RGPermiFinHow,
    'Informe a permiss�o de finaliza��o da inspe��o"') then Exit;
  //
  OVS_PF.ObtemSegmentoESecaoDeLocal(Local, SegmntInsp, SeccaoInsp);
  FCodigo := 0;
  FCodigo := OVS_PF.InsereOVgItxGerCab(ImgTipo.SQLTYpe, Codigo, Local, NrOP, SeqGrupo,
  NrReduzidoOP, OVcYnsExg, (*OVcYnsChk, LimiteChk, LimiteMed,*) ZtatusIsp,
  ZtatusMot, (*OVcYnsARQ,*) PermiFinHow, DtHrAbert, ZtatusDtH,
  SegmntInsp, SeccaoInsp);
  //
  if FCodigo <> 0 then
    Close;
end;

procedure TFmOVgItxGerCad.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmOVgItxGerCad.CBOVcYnsExgKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  case Key of
    VK_F3: ReopenOVcYnsExgCad(FSeqGrupo, True);
    VK_F4: ReopenOVcYnsExgCad(FSeqGrupo, False);
  end;
end;

procedure TFmOVgItxGerCad.EdOVcYnsExgKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  case Key of
    VK_F3: ReopenOVcYnsExgCad(FSeqGrupo, True);
    VK_F4: ReopenOVcYnsExgCad(FSeqGrupo, False);
  end;
end;

procedure TFmOVgItxGerCad.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmOVgItxGerCad.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  FCodigo   := 0;
  FSeqGrupo := 0;
  //UnDmkDAC_PF.AbreQuery(QrOVcYnsExgCad, Dmod.MyDB);
end;

procedure TFmOVgItxGerCad.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmOVgItxGerCad.ReopenOVcYnsExgCad(ArtigRef: Integer; Filtra: Boolean);
var
  SQL_WHERE: String;
begin
  if Filtra then
    SQL_WHERE := 'WHERE ArtigRef=' + Geral.FF0(ArtigRef)
  else
    SQL_WHERE := '';
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrOVcYnsExgCad, Dmod.MyDB, [
  'SELECT Codigo, Nome',
  'FROM ovcynsexgcad',
  SQL_WHERE,
  'ORDER BY Nome',
  '']);
end;

procedure TFmOVgItxGerCad.SbOVcYnsExgClick(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  OVS_Jan.MostraFormOVcYnsExgCad(FSeqGrupo);
  UMyMod.SetaCodigoPesquisado(EdOVcYnsExg, CBOVcYnsExg, QrOVcYnsExgCad,
    VAR_CADASTRO);
end;

end.
