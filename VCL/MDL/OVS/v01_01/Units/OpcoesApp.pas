unit OpcoesApp;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, ComCtrls, ExtDlgs, DBCtrls, Db, Variants,
  mySQLDbTables, dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkRadioGroup,
  dmkGeral, UnDmkProcFunc, dmkImage, UnDmkEnums, dmkCheckBox,
  UnGrl_Consts, dmkCheckGroup, Vcl.Menus, dmkValUsu, Vcl.Grids, Vcl.DBGrids,
  dmkDBGridZTO;

type
  TFmOpcoesApp = class(TForm)
    Panel1: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel8: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel9: TPanel;
    PnSaiDesis: TPanel;
    BtOK: TBitBtn;
    BtSaida: TBitBtn;
    PCGeral: TPageControl;
    TabSheet2: TTabSheet;
    Panel6: TPanel;
    Label2: TLabel;
    QrOVpLayEsq: TMySQLQuery;
    QrOVpLayEsqCodigo: TIntegerField;
    QrOVpLayEsqNome: TWideStringField;
    DsOVpLayEsq: TDataSource;
    TabSheet1: TTabSheet;
    Panel10: TPanel;
    Panel11: TPanel;
    Label9: TLabel;
    EdERPNameByCli: TdmkEdit;
    QrEntiTipCto: TMySQLQuery;
    DsEntiTipCto: TDataSource;
    Label10: TLabel;
    SpeedButton1: TSpeedButton;
    Label12: TLabel;
    EdEntiTipCto_Inspecao: TdmkEditCB;
    CBEntiTipCto_Inspecao: TdmkDBLookupComboBox;
    SbEntiTipCto_Inspecao: TSpeedButton;
    dmkValUsu1: TdmkValUsu;
    QrEntiTipCtoCodUsu: TIntegerField;
    QrEntiTipCtoCodigo: TIntegerField;
    QrEntiTipCtoNome: TWideStringField;
    EdMailResInspResul: TdmkEdit;
    Label13: TLabel;
    TabSheet3: TTabSheet;
    Panel12: TPanel;
    Panel13: TPanel;
    DBGrid1: TDBGrid;
    QrOVpDirXtr: TMySQLQuery;
    DsOVpDirXtr: TDataSource;
    BtIncluiPasta: TBitBtn;
    BtExcluiPasta: TBitBtn;
    QrOVpDirXtrCodigo: TIntegerField;
    QrOVpDirXtrNome: TWideStringField;
    CkForcaReabCfgInsp: TdmkCheckBox;
    LaLoadCSVAtbValr2: TLabel;
    Label18: TLabel;
    EdMailResInspResu2: TdmkEdit;
    Panel2: TPanel;
    Label1: TLabel;
    EdLoadCSVOthIP: TEdit;
    EdLoadCSVOthDir: TEdit;
    Label3: TLabel;
    EdOVpLayEsq: TdmkEditCB;
    CBOVpLayEsq: TdmkDBLookupComboBox;
    SbOVpLayEsq: TSpeedButton;
    EdLoadCSVIntrv: TdmkEdit;
    SpeedButton2: TSpeedButton;
    Label11: TLabel;
    PCFiltros: TPageControl;
    TabSheet4: TTabSheet;
    TabSheet5: TTabSheet;
    PanelFaccao: TPanel;
    RGLoadCSVIntExt: TdmkRadioGroup;
    RGLoadCSVSoLote: TdmkRadioGroup;
    Panel3: TPanel;
    GroupBox2: TGroupBox;
    Panel4: TPanel;
    Label4: TLabel;
    EdLoadCSVEmpresas: TdmkEdit;
    Panel5: TPanel;
    Label5: TLabel;
    EdLoadCSVTipOP: TdmkEdit;
    Panel7: TPanel;
    Label6: TLabel;
    EdLoadCSVSitOP: TdmkEdit;
    Panel14: TPanel;
    Label7: TLabel;
    EdLoadCSVTipLoclz: TdmkEdit;
    Panel15: TPanel;
    Label8: TLabel;
    EdLoadCSVTipProdOP: TdmkEdit;
    GroupBox3: TGroupBox;
    Panel17: TPanel;
    Label15: TLabel;
    EdLoadCSVAtbValr1: TdmkEdit;
    PnTexteis: TPanel;
    RGLoadCSVIntEx2: TdmkRadioGroup;
    RGLoadCSVSoLot2: TdmkRadioGroup;
    Panel20: TPanel;
    GroupBox1: TGroupBox;
    Panel21: TPanel;
    Label19: TLabel;
    EdLoadCSVEmpresa2: TdmkEdit;
    Panel22: TPanel;
    Label20: TLabel;
    EdLoadCSVTipO2: TdmkEdit;
    Panel23: TPanel;
    Label21: TLabel;
    EdLoadCSVSitO2: TdmkEdit;
    Panel24: TPanel;
    Label22: TLabel;
    EdLoadCSVTipLocl2: TdmkEdit;
    Panel25: TPanel;
    Label23: TLabel;
    EdLoadCSVTipProdO2: TdmkEdit;
    GroupBox4: TGroupBox;
    Panel18: TPanel;
    Label16: TLabel;
    EdLoadCSVAtbValr2: TdmkEdit;
    Label14: TLabel;
    EdLoadCSVAtbNomLocal: TdmkEdit;
    TabSheet6: TTabSheet;
    Panel16: TPanel;
    BtExclui: TBitBtn;
    BtAltera: TBitBtn;
    BtInclui: TBitBtn;
    DBGOpcoesApU: TdmkDBGridZTO;
    QrOpcoesApU: TMySQLQuery;
    DsOpcoesApU: TDataSource;
    QrOpcoesApUCodigo: TIntegerField;
    QrOpcoesApUHabFaccao: TSmallintField;
    QrOpcoesApUHabTextil: TSmallintField;
    QrOpcoesApUNO_HabFaccao: TWideStringField;
    QrOpcoesApUNO_HabTextil: TWideStringField;
    QrOpcoesApULogin: TWideStringField;
    TabSheet7: TTabSheet;
    GroupBox5: TGroupBox;
    Panel19: TPanel;
    Label24: TLabel;
    EdSecConfeccao: TdmkEdit;
    Panel26: TPanel;
    EdSecTecelagem: TdmkEdit;
    Panel27: TPanel;
    EdSecTinturaria: TdmkEdit;
    Label25: TLabel;
    Label26: TLabel;
    QrOpcoesApUHabFacConfeccao: TSmallintField;
    QrOpcoesApUHabTexTecelagem: TSmallintField;
    QrOpcoesApUHabTexTinturaria: TSmallintField;
    QrOpcoesApUNO_HabFacConfecaocao: TWideStringField;
    QrOpcoesApUNO_HabTexTecelagem: TWideStringField;
    QrOpcoesApUNO_HabTexTinturaria: TWideStringField;
    EdLoadCSVCodNomLocal: TdmkEdit;
    EdCdScConfeccao: TdmkEdit;
    EdCdScTecelagem: TdmkEdit;
    EdCdScTinturaria: TdmkEdit;
    Panel28: TPanel;
    Label27: TLabel;
    EdLoadCSVCodValr1: TdmkEdit;
    Panel29: TPanel;
    Label28: TLabel;
    EdLoadCSVCodValr2: TdmkEdit;
    Label29: TLabel;
    Label17: TLabel;
    Label30: TLabel;
    Label31: TLabel;
    EdPwdDesobriga: TEdit;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SbEntiTipCto_InspecaoClick(Sender: TObject);
    procedure BtIncluiPastaClick(Sender: TObject);
    procedure BtExcluiPastaClick(Sender: TObject);
    procedure SbOVpLayEsqClick(Sender: TObject);
    procedure BtIncluiClick(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtExcluiClick(Sender: TObject);
    procedure QrOpcoesApUAfterOpen(DataSet: TDataSet);
    procedure QrOpcoesApUBeforeClose(DataSet: TDataSet);
    procedure LaLoadCSVAtbValr2DblClick(Sender: TObject);
  private
    { Private declarations }
    //
    procedure ReopenOpcoesApU(Codigo: Integer);
  public
    { Public declarations }
    procedure ReopenOVpDirXtr(Codigo: Integer);
    procedure MostraFormOpcoesApUCad(SQLType: TSQLType; Codigo: Integer; Login:
              String);
  end;

  var
  FmOpcoesApp: TFmOpcoesApp;

implementation

uses UnMyObjects, Module, ModuleGeral, Principal, UMySQLModule,
  UnInternalConsts, MyDBCheck, DmkDAC_PF, UnEntities, UnOVS_Jan,
  OpcoesApUCad;

{$R *.DFM}

procedure TFmOpcoesApp.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmOpcoesApp.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmOpcoesApp.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmOpcoesApp.LaLoadCSVAtbValr2DblClick(Sender: TObject);
begin
  if PCFiltros.ActivePageIndex = 1 then
  begin
    EdLoadCSVAtbValr2.Text := LaLoadCSVAtbValr2.Caption;
  end;
end;

procedure TFmOpcoesApp.MostraFormOpcoesApUCad(SQLType: TSQLType; Codigo: Integer;
  Login: String);
begin
  if DBCheck.CriaFm(TFmOpcoesApUCad, FmOpcoesApUCad, afmoSoBoss) then
  begin
    FmOpcoesApUCad.ImgTipo.SQLType       := SQLType;
    FmOpcoesApUCad.EdCodigo.ValueVariant := Codigo;
    FmOpcoesApUCad.EdLogin.ValueVariant  := Login;
    if SQLType = stUpd then
    begin
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
      FmOpcoesApUCad.CkHabFaccao.Checked := Geral.IntToBool(QrOpcoesApUHabFaccao.Value);
        FmOpcoesApUCad.CkHabFacConfeccao.Checked := Geral.IntToBool(QrOpcoesApUHabFacConfeccao.Value);
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
      FmOpcoesApUCad.CkHabTextil.Checked := Geral.IntToBool(QrOpcoesApUHabTextil.Value);
        FmOpcoesApUCad.CkHabTexTecelagem.Checked := Geral.IntToBool(QrOpcoesApUHabTexTecelagem.Value);
        FmOpcoesApUCad.CkHabTexTinturaria.Checked := Geral.IntToBool(QrOpcoesApUHabTexTinturaria.Value);
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
    end;
    FmOpcoesApUCad.ShowModal;
    FmOpcoesApUCad.Destroy;
    //
    ReopenOpcoesApU(Codigo);
  end;
end;

procedure TFmOpcoesApp.QrOpcoesApUAfterOpen(DataSet: TDataSet);
begin
  BtAltera.Enabled := QrOpcoesApU.RecordCount > 0;
  BtExclui.Enabled := QrOpcoesApU.RecordCount > 0;
end;

procedure TFmOpcoesApp.QrOpcoesApUBeforeClose(DataSet: TDataSet);
begin
  BtAltera.Enabled := False;
  BtExclui.Enabled := False;
end;

procedure TFmOpcoesApp.ReopenOpcoesApU(Codigo: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrOpcoesApU, Dmod.MyDB, [
  'SELECT apu.Codigo, apu.HabFaccao, apu.HabTextil,  ',
  'apu.HabFacConfeccao,  ',
  'apu.HabTexTecelagem, apu.HabTexTinturaria,  ',
  'IF(apu.HabFaccao=1, "SIM", "N�O") NO_HabFaccao,  ',
  'IF(apu.HabFacConfeccao=1, "SIM", "N�O") NO_HabFacConfecaocao,  ',
  'IF(apu.HabTextil=1, "SIM", "N�O") NO_HabTextil,  ',
  'IF(apu.HabTexTecelagem=1, "SIM", "N�O") NO_HabTexTecelagem,  ',
  'IF(apu.HabTexTinturaria=1, "SIM", "N�O") NO_HabTexTinturaria,  ',
  'pwd.Login  ',
  'FROM opcoesapu apu  ',
  'LEFT JOIN senhas pwd ON pwd.Numero=apu.Codigo  ',
  '']);
  if Codigo <> 0 then
    QrOpcoesApU.Locate('Codigo', Codigo, []);
end;

procedure TFmOpcoesApp.ReopenOVpDirXtr(Codigo: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrOVpDirXtr, Dmod.MyDB, [
  'SELECT Codigo, Nome ',
  'FROM ovpdirxtr ',
  'ORDER BY Nome ',
  '']);
  //
  if Codigo <> 0 then
    QrOVpDirXtr.Locate('Codigo', Codigo, []);
end;

procedure TFmOpcoesApp.BtAlteraClick(Sender: TObject);
begin
  MostraFormOpcoesApUCad(stUpd, QrOpcoesApUCodigo.Value, QrOpcoesApULogin.Value);
end;

procedure TFmOpcoesApp.BtExcluiClick(Sender: TObject);
begin
  if Geral.MB_Pergunta('Este login ser� removido apenas desta lista! ' +
  sLineBreak + 'Deseja continuar assim mesmo?') = ID_YES then
  begin
    UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB,[
    'DELETE FROM opcoesapu ',
    'WHERE Codigo=' + Geral.FF0(QrOpcoesApUCodigo.Value),
    '']);
  end;
  ReopenOpcoesApU(0);
end;

procedure TFmOpcoesApp.BtExcluiPastaClick(Sender: TObject);
begin
  if UMyMod.ExcluiRegistroInt1(
  'Confirma a retirada da lista do diret�rio selecionado?',
  'ovpdirxtr', 'Codigo', QrOVpDirXtrCodigo.Value, Dmod.MyDB) = ID_YES then
  begin
    ReopenOVpDirXtr(0);
  end;
end;

procedure TFmOpcoesApp.BtIncluiClick(Sender: TObject);
const
  Aviso  = '...';
  Titulo = 'Sele��o de Login';
  Prompt = 'Informe o usu�rio: [F7 para pesquisar]';
  Campo  = 'Descricao';
var
  Numero: Variant;
  Codigo: Integer;
  PesqSQL: String;
begin
  PesqSQL := Geral.ATS([
  'SELECT Numero Codigo, Login Descricao',
  'FROM senhas ',
  'WHERE Numero > 0 OR Numero=-2',
  'ORDER BY ' + Campo,
  '']);
  Numero :=
    DBCheck.EscolheCodigoUnico(Aviso, Titulo, Prompt, nil, nil, Campo, 0(*Codigo*), [
    PesqSQL], Dmod.MyDB, True);
  if Codigo <> Null then
  begin
    Codigo := Numero;
    //
    Dmod.ReopenOpcoesApU(Codigo);
    if Dmod.QrOpcoesApU.RecordCount = 0 then
      MostraFormOpcoesApUCad(stIns, Codigo, VAR_SELNOM)
    else
    begin
      Geral.MB_Aviso('Usu�rio j� cadastrado!');
      MostraFormOpcoesApUCad(stUpd, Codigo, VAR_SELNOM);
    end;
  end;
end;

procedure TFmOpcoesApp.BtIncluiPastaClick(Sender: TObject);
var
  IniPath, SelPath: String;
var
  Nome: String;
  Codigo: Integer;
  SQLType: TSQLType;
begin
  IniPath := ExtractFilePath(EdLoadCSVOthDir.Text);
  if MyObjects.FileOpenDialog(Self, IniPath, '', 'Selecione o Diret�rio', '',
  [], SelPath) then
  begin
    Codigo         := 0;
    Nome           := ExtractFileDir(SelPath);
    //
    Codigo := UMyMod.BPGS1I32('ovpdirxtr', 'Codigo', '', '', tsPos, stIns, Codigo);
    if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'ovpdirxtr', False, [
    'Nome'], [
    'Codigo'], [
    Nome], [
    Codigo], True) then
    begin
      ReopenOVpDirXtr(Codigo);
    end;
  end;
end;

procedure TFmOpcoesApp.BtOKClick(Sender: TObject);
var
  LoadCSVOthIP, LoadCSVOthDir: String;
  Codigo, OVpLayEsq, LoadCSVIntExt, LoadCSVSoLote, LoadCSVIntEx2, LoadCSVSoLot2,
  EntiTipCto_Inspecao, LoadCSVIntrv, ForcaReabCfgInsp: Integer;
  LoadCSVEmpresas, LoadCSVTipOP, LoadCSVSitOP, LoadCSVTipLoclz,
  LoadCSVTipProdOP, LoadCSVEmpresa2, LoadCSVTipO2, LoadCSVSitO2,
  LoadCSVTipLocl2, LoadCSVTipProdO2, ERPNameByCli, MailResInspResul,
  LoadCSVAtbNomLocal, LoadCSVAtbValr1, LoadCSVAtbValr2, MailResInspResu2,
  SecConfeccao, SecTecelagem, SecTinturaria: String;
  LoadCSVCodNomLocal, CdScConfeccao, CdScTecelagem, CdScTinturaria: Integer;
  LoadCSVCodValr1, LoadCSVCodValr2, PwdDesobriga: String;
  SQLType: TSQLType;
begin
  SQLType             := stUpd;
  Codigo              := 1;
  LoadCSVOthIP        := EdLoadCSVOthIP.Text;
  LoadCSVOthDir       := EdLoadCSVOthDir.Text;
  OVpLayEsq           := EdOVpLayEsq.ValueVariant;
  //
  LoadCSVIntExt       := RGLoadCSVIntExt.ItemIndex;
  LoadCSVSoLote       := RGLoadCSVSoLote.ItemIndex;
  LoadCSVEmpresas     := EdLoadCSVEmpresas.ValueVariant;
  LoadCSVTipOP        := EdLoadCSVTipOP.ValueVariant;
  LoadCSVSitOP        := EdLoadCSVSitOP.ValueVariant;
  LoadCSVTipLoclz     := EdLoadCSVTipLoclz.ValueVariant;
  LoadCSVTipProdOP    := EdLoadCSVTipProdOP.ValueVariant;
  LoadCSVAtbValr1     := EdLoadCSVAtbValr1.ValueVariant;
  MailResInspResul    := EdMailResInspResul.ValueVariant;
  //
  LoadCSVAtbNomLocal  := EdLoadCSVAtbNomLocal.ValueVariant;
  //
  EntiTipCto_Inspecao := EdEntiTipCto_Inspecao.ValueVariant;
  if EntiTipCto_Inspecao <> 0 then
    EntiTipCto_Inspecao := QrEntiTipCtoCodigo.Value;
  //
  LoadCSVIntrv        := EdLoadCSVIntrv.ValueVariant;
  //
  ForcaReabCfgInsp    := Geral.BoolToInt(CkForcaReabCfgInsp.Checked);
  //
  LoadCSVIntEx2       := RGLoadCSVIntEx2.ItemIndex;
  LoadCSVSoLot2       := RGLoadCSVSoLot2.ItemIndex;
  LoadCSVEmpresa2     := EdLoadCSVEmpresa2.ValueVariant;
  LoadCSVTipO2        := EdLoadCSVTipO2.ValueVariant;
  LoadCSVSitO2        := EdLoadCSVSitO2.ValueVariant;
  LoadCSVTipLocl2     := EdLoadCSVTipLocl2.ValueVariant;
  LoadCSVTipProdO2    := EdLoadCSVTipProdO2.ValueVariant;
  LoadCSVAtbValr2     := EdLoadCSVAtbValr2.ValueVariant;
  MailResInspResu2    := EdMailResInspResu2.ValueVariant;
  //
  SecConfeccao           := EdSecConfeccao.ValueVariant;
  SecTecelagem        := EdSecTecelagem.ValueVariant;
  SecTinturaria       := EdSecTinturaria.ValueVariant;
  //
  LoadCSVCodNomLocal  := EdLoadCSVCodNomLocal.ValueVariant;
  LoadCSVCodValr1     := EdLoadCSVCodValr1.ValueVariant;
  LoadCSVCodValr2     := EdLoadCSVCodValr2.ValueVariant;
  CdScConfeccao       := EdCdScConfeccao.ValueVariant;
  CdScTecelagem       := EdCdScTecelagem.ValueVariant;
  CdScTinturaria      := EdCdScTinturaria.ValueVariant;
  //Miscel�nea
  ERPNameByCli        := EdERPNameByCli.ValueVariant;
  //Fim Miscel�nea
  PwdDesobriga        := EdPwdDesobriga.Text;
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'opcoesapp', False, [
  'LoadCSVOthIP', 'LoadCSVOthDir', 'OVpLayEsq',
  'LoadCSVIntExt', 'LoadCSVSoLote', 'LoadCSVEmpresas',
  'LoadCSVTipOP', 'LoadCSVSitOP', 'LoadCSVTipLoclz',
  'LoadCSVTipProdOP', 'EntiTipCto_Inspecao',
  'LoadCSVIntrv', 'MailResInspResul', 'ForcaReabCfgInsp',
  'LoadCSVAtbNomLocal', 'LoadCSVAtbValr1', 'LoadCSVAtbValr2',
  'LoadCSVIntEx2', 'LoadCSVSoLot2', 'LoadCSVEmpresa2',
  'LoadCSVTipO2', 'LoadCSVSitO2', 'LoadCSVTipLocl2',
  'LoadCSVTipProdO2', 'MailResInspResu2',
  'SecConfeccao', 'SecTecelagem', 'SecTinturaria',
  'LoadCSVCodNomLocal', 'LoadCSVCodValr1', 'LoadCSVCodValr2',
  'CdScConfeccao', 'CdScTecelagem', 'CdScTinturaria',
  'PwdDesobriga'
  ], [
  'Codigo'], [
  LoadCSVOthIP, LoadCSVOthDir, OVpLayEsq,
  LoadCSVIntExt, LoadCSVSoLote, LoadCSVEmpresas,
  LoadCSVTipOP, LoadCSVSitOP, LoadCSVTipLoclz,
  LoadCSVTipProdOP, EntiTipCto_Inspecao,
  LoadCSVIntrv, MailResInspResul, ForcaReabCfgInsp,
  LoadCSVAtbNomLocal, LoadCSVAtbValr1, LoadCSVAtbValr2,
  LoadCSVIntEx2, LoadCSVSoLot2, LoadCSVEmpresa2,
  LoadCSVTipO2, LoadCSVSitO2, LoadCSVTipLocl2,
  LoadCSVTipProdO2, MailResInspResu2,
  SecConfeccao, SecTecelagem, SecTinturaria,
  LoadCSVCodNomLocal, LoadCSVCodValr1, LoadCSVCodValr2,
  CdScConfeccao, CdScTecelagem, CdScTinturaria,
  PwdDesobriga
  ], [
  Codigo], True) then
  begin
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'ctrlgeral', False, [
    'ERPNameByCli'], ['Codigo'
    ], [
    ERPNameByCli], [
    1], False) then
    begin
      UnDmkDAC_PF.AbreQuery(Dmod.QrOpcoesApp, Dmod.MyDB);
      //
      UnDmkDAC_PF.AbreQuery(Dmod.QrOpcoesGrl, Dmod.MyDB);
      UnDmkDAC_PF.AbreQuery(DModG.QrCtrlGeral, Dmod.MyDB);
      Close;
    end;
  end;
end;

procedure TFmOpcoesApp.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  PCGeral.ActivePageIndex := 0;
  PCFiltros.ActivePageIndex := 0;
  Dmod.ReopenOpcoesApp();
  UnDmkDAC_PF.AbreQuery(QrOVpLayEsq, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrEntiTipCto, Dmod.MyDB);
  EdLoadCSVOthIP.Text                := Dmod.QrOpcoesAppLoadCSVOthIP.Value;
  EdLoadCSVOthDir.Text               := Dmod.QrOpcoesAppLoadCSVOthDir.Value;
  EdOVpLayEsq.ValueVariant           := Dmod.QrOpcoesAppOVpLayEsq.Value;
  CBOVpLayEsq.KeyValue               := Dmod.QrOpcoesAppOVpLayEsq.Value;
  //
  RGLoadCSVIntExt.ItemIndex          := Dmod.QrOpcoesAppLoadCSVIntExt.Value;
  RGLoadCSVSoLote.ItemIndex          := Dmod.QrOpcoesAppLoadCSVSoLote.Value;
  EdLoadCSVEmpresas.ValueVariant     := Dmod.QrOpcoesAppLoadCSVEmpresas.Value;
  EdLoadCSVTipOP.ValueVariant        := Dmod.QrOpcoesAppLoadCSVTipOP.Value;
  EdLoadCSVSitOP.ValueVariant        := Dmod.QrOpcoesAppLoadCSVSitOP.Value;
  EdLoadCSVTipLoclz.ValueVariant     := Dmod.QrOpcoesAppLoadCSVTipLoclz.Value;
  EdLoadCSVTipProdOP.ValueVariant    := Dmod.QrOpcoesAppLoadCSVTipProdOP.Value;
  EdLoadCSVAtbValr1.ValueVariant     := Dmod.QrOpcoesAppLoadCSVAtbValr1.Value;
  //
  RGLoadCSVIntEx2.ItemIndex          := Dmod.QrOpcoesAppLoadCSVIntEx2.Value;
  RGLoadCSVSoLot2.ItemIndex          := Dmod.QrOpcoesAppLoadCSVSoLot2.Value;
  EdLoadCSVEmpresa2.ValueVariant     := Dmod.QrOpcoesAppLoadCSVEmpresa2.Value;
  EdLoadCSVTipO2.ValueVariant        := Dmod.QrOpcoesAppLoadCSVTipO2.Value;
  EdLoadCSVSitO2.ValueVariant        := Dmod.QrOpcoesAppLoadCSVSitO2.Value;
  EdLoadCSVTipLocl2.ValueVariant     := Dmod.QrOpcoesAppLoadCSVTipLocl2.Value;
  EdLoadCSVTipProdO2.ValueVariant    := Dmod.QrOpcoesAppLoadCSVTipProdO2.Value;
  EdLoadCSVAtbValr2.ValueVariant     := Dmod.QrOpcoesAppLoadCSVAtbValr2.Value;
  //
  EdLoadCSVAtbNomLocal.ValueVariant  := Dmod.QrOpcoesAppLoadCSVAtbNomLocal.Value;
  //
  EdEntiTipCto_Inspecao.ValueVariant := Dmod.QrOpcoesAppEntiTipCto_Inspecao.Value;
  //
  EdLoadCSVIntrv.ValueVariant        := Dmod.QrOpcoesAppLoadCSVIntrv.Value;
  EdMailResInspResul.ValueVariant    := Dmod.QrOpcoesAppMailResInspResul.Value;
  EdMailResInspResu2.ValueVariant    := Dmod.QrOpcoesAppMailResInspResu2.Value;
  CkForcaReabCfgInsp.Checked         := Geral.IntToBool(Dmod.QrOpcoesAppForcaReabCfgInsp.Value);
  //
  EdSecConfeccao.ValueVariant        := Dmod.QrOpcoesAppSecConfeccao.Value;
  EdSecTecelagem.ValueVariant        := Dmod.QrOpcoesAppSecTecelagem.Value;
  EdSecTinturaria.ValueVariant       := Dmod.QrOpcoesAppSecTinturaria.Value;
  //
  EdLoadCSVCodNomLocal.ValueVariant  := Dmod.QrOpcoesAppLoadCSVCodNomLocal.Value;
  EdLoadCSVCodValr1.ValueVariant     := Dmod.QrOpcoesAppLoadCSVCodValr1.Value;
  EdLoadCSVCodValr2.ValueVariant     := Dmod.QrOpcoesAppLoadCSVCodValr2.Value;
  EdCdScConfeccao.ValueVariant       := Dmod.QrOpcoesAppCdScConfeccao.Value;
  EdCdScTecelagem.ValueVariant       := Dmod.QrOpcoesAppCdScTecelagem.Value;
  EdCdScTinturaria.ValueVariant      := Dmod.QrOpcoesAppCdScTinturaria.Value;
  //
  //Miscel�nea
  EdERPNameByCli.ValueVariant        := Dmod.QrOpcoesGrlERPNameByCli.Value;
  //Fim Miscel�nea
  //
  EdPwdDesobriga.Text                := Dmod.QrOpcoesAppPwdDesobriga.Value;
  //
  //
  ReopenOVpDirXtr(0);
  ReopenOpcoesApU(0);
end;

procedure TFmOpcoesApp.SbEntiTipCto_InspecaoClick(Sender: TObject);
begin
  Entities.MostraFormMostraEntiTipCto(dmkValUsu1.ValueVariant,
    EdEntiTipCto_Inspecao, CBEntiTipCto_Inspecao, QrEntiTipCto);
end;

procedure TFmOpcoesApp.SbOVpLayEsqClick(Sender: TObject);
begin
  OVS_Jan.MostraFormOVpLayEsq(EdOVpLayEsq.ValueVariant);
end;

procedure TFmOpcoesApp.SpeedButton2Click(Sender: TObject);
var
  IniPath, SelPath: String;
begin
  IniPath := ExtractFilePath(EdLoadCSVOthDir.Text);
  if MyObjects.FileOpenDialog(Self, IniPath, '', 'Selecione o Diret�rio', '',
  [], SelPath) then
    EdLoadCSVOthDir.Text := ExtractFileDir(SelPath);
end;


{
Tipo de industrializa��o:

SELECT Codigo, AtribValr, COUNT(Codigo) ITENS
FROM ovDClasLocal
WHERE AtribNome = 'LOCAL'
GROUP BY Codigo
ORDER BY ITENS DESC


Nome de Locais (Nome de atributo)
Nome de industrializa��es (Valor de atributo)

SELECT DISTINCT AtribValr
FROM ovDClasLocal
WHERE AtribNome = 'LOCAL'
ORDER BY AtribValr

Campos:

AtbNom

AtbVal
}

end.
