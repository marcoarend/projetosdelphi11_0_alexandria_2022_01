object FmOVmIspDevPsq: TFmOVmIspDevPsq
  Left = 339
  Top = 185
  Caption = 'OVS-INSPE-005 :: Pesquisa de Inspe'#231#245'es'
  ClientHeight = 629
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 912
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 282
        Height = 32
        Caption = 'Pesquisa de Inspe'#231#245'es'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 282
        Height = 32
        Caption = 'Pesquisa de Inspe'#231#245'es'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 282
        Height = 32
        Caption = 'Pesquisa de Inspe'#231#245'es'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 1008
    Height = 467
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 1008
      Height = 467
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 1008
        Height = 105
        Align = alTop
        Caption = ' Dados importados: '
        TabOrder = 0
        object Panel5: TPanel
          Left = 2
          Top = 15
          Width = 1004
          Height = 88
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          object Label7: TLabel
            Left = 4
            Top = 4
            Width = 29
            Height = 13
            Caption = 'Local:'
          end
          object Label3: TLabel
            Left = 432
            Top = 4
            Width = 18
            Height = 13
            Caption = 'OP:'
          end
          object Label6: TLabel
            Left = 508
            Top = 4
            Width = 52
            Height = 13
            Caption = 'Reduz.OP:'
          end
          object Label1: TLabel
            Left = 4
            Top = 48
            Width = 30
            Height = 13
            Caption = 'Artigo:'
          end
          object Label2: TLabel
            Left = 64
            Top = 48
            Width = 55
            Height = 13
            Caption = 'Refer'#234'ncia:'
          end
          object Label9: TLabel
            Left = 728
            Top = 48
            Width = 48
            Height = 13
            Caption = 'Tamanho:'
          end
          object Label4: TLabel
            Left = 584
            Top = 4
            Width = 169
            Height = 13
            Caption = 'Dispositivo que realizou a inspe'#231#227'o:'
          end
          object SbOVcMobDevCad: TSpeedButton
            Left = 844
            Top = 19
            Width = 23
            Height = 23
            Caption = '?'
            OnClick = SbOVcMobDevCadClick
          end
          object Label5: TLabel
            Left = 872
            Top = 4
            Width = 105
            Height = 13
            Caption = 'ID insp. no disp. m'#243'v.:'
          end
          object EdLocal: TdmkEditCB
            Left = 4
            Top = 20
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'Topico'
            UpdCampo = 'Topico'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            DBLookupComboBox = CBLocal
            IgnoraDBLookupComboBox = False
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
          object CBLocal: TdmkDBLookupComboBox
            Left = 60
            Top = 20
            Width = 368
            Height = 21
            KeyField = 'Codigo'
            ListField = 'Nome'
            ListSource = DsOVdLocal
            TabOrder = 1
            dmkEditCB = EdLocal
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
          object EdNrOP: TdmkEdit
            Left = 432
            Top = 20
            Width = 72
            Height = 21
            Alignment = taRightJustify
            TabOrder = 2
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
          object EdNrReduzidoOP: TdmkEdit
            Left = 508
            Top = 20
            Width = 72
            Height = 21
            Alignment = taRightJustify
            TabOrder = 3
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
          object EdArtigo: TdmkEditCB
            Left = 4
            Top = 64
            Width = 56
            Height = 21
            Alignment = taRightJustify
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 4
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'Topico'
            UpdCampo = 'Topico'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            OnChange = EdArtigoChange
            OnKeyDown = EdArtigoKeyDown
            DBLookupComboBox = CBArtigo
            IgnoraDBLookupComboBox = False
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
          object CBArtigo: TdmkDBLookupComboBox
            Left = 168
            Top = 64
            Width = 556
            Height = 21
            KeyField = 'Codigo'
            ListField = 'Nome'
            ListSource = DsOVdReferencia
            TabOrder = 5
            OnKeyDown = CBArtigoKeyDown
            dmkEditCB = EdArtigo
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
          object EdReferencia: TdmkEdit
            Left = 64
            Top = 64
            Width = 101
            Height = 21
            TabOrder = 6
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
            OnChange = EdReferenciaChange
            OnExit = EdReferenciaExit
            OnKeyDown = EdReferenciaKeyDown
          end
          object EdCodTam: TdmkEdit
            Left = 728
            Top = 64
            Width = 65
            Height = 21
            TabOrder = 7
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdOVcMobDevCad: TdmkEditCB
            Left = 584
            Top = 20
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 8
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'Topico'
            UpdCampo = 'Topico'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            DBLookupComboBox = CBOVcMobDevCad
            IgnoraDBLookupComboBox = False
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
          object CBOVcMobDevCad: TdmkDBLookupComboBox
            Left = 640
            Top = 20
            Width = 201
            Height = 21
            KeyField = 'Codigo'
            ListField = 'Nome'
            ListSource = DsOVcMobDevCad
            TabOrder = 9
            dmkEditCB = EdOVcMobDevCad
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
          object EdCodInMob: TdmkEdit
            Left = 872
            Top = 20
            Width = 109
            Height = 21
            Alignment = taRightJustify
            TabOrder = 10
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
        end
      end
      object GroupBox3: TGroupBox
        Left = 0
        Top = 105
        Width = 1008
        Height = 64
        Align = alTop
        Caption = ' Dados da inspe'#231#227'o: '
        TabOrder = 1
        object Panel7: TPanel
          Left = 2
          Top = 15
          Width = 1004
          Height = 47
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          object Label8: TLabel
            Left = 4
            Top = 4
            Width = 127
            Height = 13
            Caption = 'Configura'#231#227'o de inspe'#231#227'o:'
          end
          object SbOVgIspGer: TSpeedButton
            Left = 452
            Top = 19
            Width = 23
            Height = 23
            Caption = '?'
            OnClick = SbOVgIspGerClick
          end
          object EdOVgIspGer: TdmkEditCB
            Left = 4
            Top = 20
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'Topico'
            UpdCampo = 'Topico'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            DBLookupComboBox = CbOVgIspGer
            IgnoraDBLookupComboBox = False
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
          object CbOVgIspGer: TdmkDBLookupComboBox
            Left = 60
            Top = 20
            Width = 389
            Height = 21
            KeyField = 'Codigo'
            ListField = 'Nome'
            ListSource = DsOVgIspGerCab
            TabOrder = 1
            dmkEditCB = EdOVgIspGer
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
          object TPDtPerIni: TdmkEditDateTimePicker
            Left = 480
            Top = 20
            Width = 112
            Height = 21
            Date = 0.476184513885527800
            Time = 0.476184513885527800
            TabOrder = 2
            ReadOnly = False
            DefaultEditMask = '!99/99/99;1;_'
            AutoApplyEditMask = True
            UpdType = utYes
            DatePurpose = dmkdpNone
          end
          object TPDtPerFim: TdmkEditDateTimePicker
            Left = 596
            Top = 20
            Width = 112
            Height = 21
            Date = 0.476184513885527800
            Time = 0.476184513885527800
            TabOrder = 3
            ReadOnly = False
            DefaultEditMask = '!99/99/99;1;_'
            AutoApplyEditMask = True
            UpdType = utYes
            DatePurpose = dmkdpNone
          end
          object CkDtIni: TCheckBox
            Left = 480
            Top = 0
            Width = 113
            Height = 17
            Caption = 'Desde (dt encer.):'
            TabOrder = 4
          end
          object CkDtFim: TCheckBox
            Left = 596
            Top = 0
            Width = 97
            Height = 17
            Caption = 'At'#233' (data encer.):'
            TabOrder = 5
          end
          object CGInspResul: TdmkCheckGroup
            Left = 724
            Top = 0
            Width = 277
            Height = 47
            Caption = ' Resultado da inpe'#231#227'o: '
            Columns = 3
            Items.Strings = (
              'Aprovado'
              'Resalvas'
              'Reprovado')
            TabOrder = 6
            UpdType = utYes
            Value = 0
            OldValor = 0
          end
        end
      end
      object DBGrid1: TdmkDBGridZTO
        Left = 0
        Top = 169
        Width = 1008
        Height = 298
        Align = alClient
        DataSource = DsOVmIspDevCab
        Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
        TabOrder = 2
        TitleFont.Charset = ANSI_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        RowColors = <>
        OnDblClick = DBGrid1DblClick
        Columns = <
          item
            Expanded = False
            FieldName = 'Codigo'
            Title.Caption = 'C'#243'digo'
            Width = 56
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Local'
            Width = 56
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NO_Local'
            Title.Caption = 'Descri'#231#227'o do Local'
            Width = 262
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'SeqGrupo'
            Title.Caption = 'Artigo'
            Width = 56
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NO_Referencia'
            Title.Caption = 'Nome artigo'
            Width = 300
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NrOP'
            Title.Caption = 'N'#186' OP'
            Width = 56
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NrReduzidoOP'
            Title.Caption = 'N'#186' Reduzido OP'
            Width = 56
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DtHrFecha'
            Title.Caption = 'Fechamento'
            Visible = True
          end>
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 515
    Width = 1008
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 559
    Width = 1008
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 862
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 860
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 22
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Pesquisa'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    Top = 65519
  end
  object QrOVdLocal: TMySQLQuery
    Database = Dmod.ZZDB
    SQL.Strings = (
      'SELECT Codigo, Nome '
      'FROM ovdlocal '
      'ORDER BY Nome ')
    Left = 368
    Top = 2
    object QrOVdLocalCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrOVdLocalNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
  object DsOVdLocal: TDataSource
    DataSet = QrOVdLocal
    Left = 368
    Top = 46
  end
  object QrOVdReferencia: TMySQLQuery
    Database = Dmod.ZZDB
    SQL.Strings = (
      'SELECT Codigo, Referencia, Nome '
      'FROM ovdreferencia '
      'ORDER BY Nome ')
    Left = 448
    Top = 2
    object QrOVdReferenciaCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrOVdReferenciaNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
  object DsOVdReferencia: TDataSource
    DataSet = QrOVdReferencia
    Left = 448
    Top = 46
  end
  object QrOVcMobDevCad: TMySQLQuery
    Database = Dmod.ZZDB
    SQL.Strings = (
      'SELECT Codigo, Nome, DeviceID'
      'FROM OVcMobDevCad'
      'WHERE Codigo > 0'
      'ORDER BY Nome')
    Left = 544
    Top = 1
    object QrOVcMobDevCadCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrOVcMobDevCadNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
    object QrOVcMobDevCadDeviceID: TWideStringField
      FieldName = 'DeviceID'
      Size = 60
    end
  end
  object DsOVcMobDevCad: TDataSource
    DataSet = QrOVcMobDevCad
    Left = 544
    Top = 45
  end
  object QrOVgIspGerCab: TMySQLQuery
    Database = Dmod.ZZDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM ovgispgercab '
      'ORDER BY Nome')
    Left = 368
    Top = 89
    object QrOVgIspGerCabCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrOVgIspGerCabNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
  object DsOVgIspGerCab: TDataSource
    DataSet = QrOVgIspGerCab
    Left = 368
    Top = 133
  end
  object QrOVmIspDevCab: TMySQLQuery
    Database = Dmod.ZZDB
    SQL.Strings = (
      'SELECT igc.*, ymc.Nome NO_OVcYnsMed, ycc.Nome NO_OVcYnsChk,'
      'dlo.Nome NO_Local, ref.Nome NO_Referencia,'
      'isc.Nome NO_InspResul, yac.Nome NO_OVcYnsARQ,'
      'IF(igc.DtHrAbert  <= "1899-12-30", "",'
      '  DATE_FORMAT(igc.DtHrAbert, "%d/%m/%Y %h:%i")) DtHrAbert_TXT,'
      'IF(igc.DtHrFecha  <= "1899-12-30", "",'
      '  DATE_FORMAT(igc.DtHrFecha, "%d/%m/%Y %h:%i")) DtHrFecha_TXT'
      'FROM ovmispdevcab igc'
      'LEFT JOIN ovdlocal dlo ON dlo.Codigo=igc.Local'
      'LEFT JOIN ovdreferencia ref ON ref.Codigo=igc.SeqGrupo'
      'LEFT JOIN ovcynsmedcad ymc ON ymc.Codigo=igc.OVcYnsMed'
      'LEFT JOIN ovcynschkcad ycc ON ycc.Codigo=igc.OVcYnsChk'
      'LEFT JOIN ovgisprescad isc ON isc.Codigo=igc.InspResul'
      'LEFT JOIN OVcYnsARQCad yac ON yac.Codigo=igc.OVcYnsARQ'
      'WHERE igc.Codigo > 0')
    Left = 264
    Top = 44
    object QrOVmIspDevCabCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrOVmIspDevCabCodInMob: TIntegerField
      FieldName = 'CodInMob'
      Required = True
    end
    object QrOVmIspDevCabDeviceSI: TIntegerField
      FieldName = 'DeviceSI'
      Required = True
    end
    object QrOVmIspDevCabDeviceID: TWideStringField
      FieldName = 'DeviceID'
      Size = 60
    end
    object QrOVmIspDevCabDeviceCU: TIntegerField
      FieldName = 'DeviceCU'
      Required = True
    end
    object QrOVmIspDevCabNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QrOVmIspDevCabOVgIspGer: TIntegerField
      FieldName = 'OVgIspGer'
      Required = True
    end
    object QrOVmIspDevCabLocal: TIntegerField
      FieldName = 'Local'
      Required = True
    end
    object QrOVmIspDevCabNrOP: TIntegerField
      FieldName = 'NrOP'
      Required = True
    end
    object QrOVmIspDevCabSeqGrupo: TIntegerField
      FieldName = 'SeqGrupo'
      Required = True
    end
    object QrOVmIspDevCabNrReduzidoOP: TIntegerField
      FieldName = 'NrReduzidoOP'
      Required = True
    end
    object QrOVmIspDevCabDtHrAbert: TDateTimeField
      FieldName = 'DtHrAbert'
      Required = True
    end
    object QrOVmIspDevCabDtHrFecha: TDateTimeField
      FieldName = 'DtHrFecha'
      Required = True
    end
    object QrOVmIspDevCabOVcYnsMed: TIntegerField
      FieldName = 'OVcYnsMed'
      Required = True
    end
    object QrOVmIspDevCabOVcYnsChk: TIntegerField
      FieldName = 'OVcYnsChk'
      Required = True
    end
    object QrOVmIspDevCabOVcYnsARQ: TIntegerField
      FieldName = 'OVcYnsARQ'
      Required = True
    end
    object QrOVmIspDevCabLimiteChk: TIntegerField
      FieldName = 'LimiteChk'
      Required = True
    end
    object QrOVmIspDevCabLimiteMed: TIntegerField
      FieldName = 'LimiteMed'
      Required = True
    end
    object QrOVmIspDevCabPecasIsp: TIntegerField
      FieldName = 'PecasIsp'
      Required = True
    end
    object QrOVmIspDevCabPecaAtual: TIntegerField
      FieldName = 'PecaAtual'
      Required = True
    end
    object QrOVmIspDevCabLk: TIntegerField
      FieldName = 'Lk'
      Required = True
    end
    object QrOVmIspDevCabDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrOVmIspDevCabDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrOVmIspDevCabUserCad: TIntegerField
      FieldName = 'UserCad'
      Required = True
    end
    object QrOVmIspDevCabUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Required = True
    end
    object QrOVmIspDevCabAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Required = True
    end
    object QrOVmIspDevCabAWServerID: TIntegerField
      FieldName = 'AWServerID'
      Required = True
    end
    object QrOVmIspDevCabAWStatSinc: TSmallintField
      FieldName = 'AWStatSinc'
      Required = True
    end
    object QrOVmIspDevCabAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
    object QrOVmIspDevCabQtReal: TFloatField
      FieldName = 'QtReal'
      Required = True
    end
    object QrOVmIspDevCabQtLocal: TFloatField
      FieldName = 'QtLocal'
      Required = True
    end
    object QrOVmIspDevCabProduto: TIntegerField
      FieldName = 'Produto'
      Required = True
    end
    object QrOVmIspDevCabCodGrade: TIntegerField
      FieldName = 'CodGrade'
      Required = True
    end
    object QrOVmIspDevCabCodTam: TWideStringField
      FieldName = 'CodTam'
      Size = 30
    end
    object QrOVmIspDevCabPontosTot: TIntegerField
      FieldName = 'PontosTot'
      Required = True
    end
    object QrOVmIspDevCabInspResul: TIntegerField
      FieldName = 'InspResul'
      Required = True
    end
    object QrOVmIspDevCabInspeSeq: TIntegerField
      FieldName = 'InspeSeq'
      Required = True
    end
    object QrOVmIspDevCabRandmStr: TWideStringField
      FieldName = 'RandmStr'
      Required = True
      Size = 32
    end
    object QrOVmIspDevCabDtHrUpIni: TDateTimeField
      FieldName = 'DtHrUpIni'
      Required = True
    end
    object QrOVmIspDevCabDtHrUpFim: TDateTimeField
      FieldName = 'DtHrUpFim'
      Required = True
    end
    object QrOVmIspDevCabEmpresa: TIntegerField
      FieldName = 'Empresa'
      Required = True
    end
    object QrOVmIspDevCabNO_OVcYnsMed: TWideStringField
      FieldName = 'NO_OVcYnsMed'
      Size = 60
    end
    object QrOVmIspDevCabNO_OVcYnsChk: TWideStringField
      FieldName = 'NO_OVcYnsChk'
      Size = 60
    end
    object QrOVmIspDevCabNO_Local: TWideStringField
      FieldName = 'NO_Local'
      Size = 100
    end
    object QrOVmIspDevCabNO_Referencia: TWideStringField
      FieldName = 'NO_Referencia'
      Size = 100
    end
    object QrOVmIspDevCabNO_InspResul: TWideStringField
      FieldName = 'NO_InspResul'
      Size = 60
    end
    object QrOVmIspDevCabNO_OVcYnsARQ: TWideStringField
      FieldName = 'NO_OVcYnsARQ'
      Size = 60
    end
    object QrOVmIspDevCabDtHrAbert_TXT: TWideStringField
      DisplayWidth = 19
      FieldName = 'DtHrAbert_TXT'
      Size = 19
    end
    object QrOVmIspDevCabDtHrFecha_TXT: TWideStringField
      DisplayWidth = 19
      FieldName = 'DtHrFecha_TXT'
      Size = 19
    end
  end
  object DsOVmIspDevCab: TDataSource
    DataSet = QrOVmIspDevCab
    Left = 264
    Top = 88
  end
end
