unit OVgIspPrfCfg;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkPermissoes,
  mySQLDbTables, dmkDBGridZTO, UnProjGroup_PF;

type
  TFmOVgIspPrfCfg = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    QrNaoCfg: TMySQLQuery;
    GroupBox1: TGroupBox;
    DBGNaoCfg: TdmkDBGridZTO;
    DsNaoCfg: TDataSource;
    QrNaoCfgLocal: TIntegerField;
    QrNaoCfgNrOP: TIntegerField;
    QrNaoCfgSeqGrupo: TIntegerField;
    QrNaoCfgNrReduzidoOP: TIntegerField;
    QrOVgIspPrfCab: TMySQLQuery;
    QrOVgIspPrfCabCodigo: TIntegerField;
    QrOVgIspPrfCabNome: TWideStringField;
    QrOVgIspPrfCabSeqGrupo: TIntegerField;
    QrOVgIspPrfCabOVcYnsMed: TIntegerField;
    QrOVgIspPrfCabOVcYnsChk: TIntegerField;
    QrOVgIspPrfCabOVcYnsARQ: TIntegerField;
    QrOVgIspPrfCabLimiteChk: TIntegerField;
    QrOVgIspPrfCabLimiteMed: TIntegerField;
    QrOVgIspPrfCabPermiFinHow: TIntegerField;
    QrParaCfg: TMySQLQuery;
    QrParaCfgLocal: TIntegerField;
    QrParaCfgNrOP: TIntegerField;
    QrParaCfgSeqGrupo: TIntegerField;
    QrParaCfgNrReduzidoOP: TIntegerField;
    QrAx: TMySQLQuery;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenNaoCfg();
    procedure Mensagem(SvcMsgKind: TSvcMsgKind; Msg: String);
  public
    { Public declarations }
    FAutomatico: Boolean;
    FLaAviso1: TLabel;
    FLaAviso2: TLabel;
    //
    function TemNovosItensAconfigurar(): Boolean;
    procedure ConfiguraNovosItensCarregados();
  end;

  var
  FmOVgIspPrfCfg: TFmOVgIspPrfCfg;

implementation

uses UnMyObjects, DmkDAC_PF, UnOVS_Consts, UMySQLModule, UnOVS_PF,
 UnOVS_ProjGroupVars,
 Module, ModuleGeral;

{$R *.DFM}

procedure TFmOVgIspPrfCfg.BtOKClick(Sender: TObject);
begin
  if not FAutomatico then
    ConfiguraNovosItensCarregados();
end;

procedure TFmOVgIspPrfCfg.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmOVgIspPrfCfg.ConfiguraNovosItensCarregados();
var
  SQLType: TSQLType;
  DtHrIni, DtHrFim: String;
  GrupCnfg, QtParaCfg, QtRealCfg, OVgIspGerCab: Integer;
  Inseriu: Boolean;
  //
  Local, NrOP, SeqGrupo, NrReduzidoOP, OVcYnsMed, OVcYnsChk, LimiteChk,
  LimiteMed, ZtatusIsp, ZtatusMot, OVcYnsARQ, PermiFinHow,
  SegmntInsp, SeccaoInsp: Integer;
  DtHrAbert, ZtatusDtH: String;
begin
  SQLType := stIns;
  GrupCnfg       := UMyMod.BPGS1I32('ovgispprfcfg', 'Codigo', '', '', tsPos, SQLType, 0);
  DtHrIni        := Geral.FDT(DModG.ObtemAgora(), 109);
  DtHrFim        := '0000-00-00 00:00:00';
  QtParaCfg      := QrNaoCfg.RecordCount;
  QtRealCfg      := 0;
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'ovgispprfcfg', False, [
  'DtHrIni', 'DtHrFim',
  'QtParaCfg', 'QtRealCfg'], [
  'Codigo'], [
  DtHrIni, DtHrFim,
  QtParaCfg, QtRealCfg], [
  GrupCnfg], True) then
  begin
    // Reservando itens para evitar duplicação
    QrNaoCfg.First;
    while not QrNaoCfg.Eof do
    begin
      Dmod.MyDB.Execute(
      ' UPDATE ovgisplassta ' +
      ' SET GrupCnfg=' + Geral.FF0(GrupCnfg) +
      ' WHERE Local=' + Geral.FF0(QrNaoCfgLocal.Value) +
      ' AND NrOP=' + Geral.FF0(QrNaoCfgNrOP.Value) +
      ' AND SeqGrupo=' + Geral.FF0(QrNaoCfgSeqGrupo.Value) +
      ' AND NrReduzidoOP=' + Geral.FF0(QrNaoCfgNrReduzidoOP.Value) +
      ' AND GrupSgmt=' + Geral.FF0(CO_SGMT_INSP_1024_FACCAO) +
      ' AND GrupCnfg=0 ' +
      EmptyStr);
      //
      UnDmkDAC_PF.AbreMySQLQuery0(QrParaCfg, Dmod.MyDB, [
      'SELECT DISTINCT ils.Local, ils.NrOP, ils.SeqGrupo,  ',
      'ils.NrReduzidoOP ',
      'FROM ovgisplassta ils ',
      'LEFT JOIN ovdlocal dlo ON dlo.Codigo=ils.Local  ',
      'LEFT JOIN ovdreferencia ref ON ref.Codigo=ils.SeqGrupo ',
      'LEFT JOIN ovgispprfcab prf ON prf.SeqGrupo=ils.SeqGrupo',
      'WHERE ils.IspZtatus=' + Geral.FF0(CO_OVS_IMPORT_ALHEIO_1024_IMPORTADO),
      'AND ils.GrupSgmt=' + Geral.FF0(CO_SGMT_INSP_1024_FACCAO),
      'AND ils.GrupCnfg=' + Geral.FF0(GrupCnfg),
      'AND ils.Local=' + Geral.FF0(QrNaoCfgLocal.Value),
      'AND ils.NrOP=' + Geral.FF0(QrNaoCfgNrOP.Value),
      'AND ils.SeqGrupo=' + Geral.FF0(QrNaoCfgSeqGrupo.Value),
      'AND ils.NrReduzidoOP=' + Geral.FF0(QrNaoCfgNrReduzidoOP.Value),
      'AND prf.Ativo=1 ',
      'ORDER BY NrReduzidoOP ',
      EmptyStr]);
      while not QrParaCfg.Eof do
      begin
        SeqGrupo := QrParaCfgSeqGrupo.Value;
        //
        UnDmkDAC_PF.AbreMySQLQuery0(QrOVgIspPrfCab, Dmod.MyDB, [
        'SELECT * ',
        'FROM ovgispprfcab ',
        'WHERE Ativo=1 ',
        'AND SeqGrupo=' + Geral.FF0(SeqGrupo),
        'ORDER BY Codigo DESC ',
        EmptyStr]);
        //
        Inseriu := False;
        if QrOVgIspPrfCab.RecordCount > 0 then
        begin
          Local         := QrParaCfgLocal.Value;
          NrOP          := QrParaCfgNrOP.Value;
          NrReduzidoOP  := QrParaCfgNrReduzidoOP.Value;
          OVcYnsMed     := QrOVgIspPrfCabOVcYnsMed.Value;
          OVcYnsChk     := QrOVgIspPrfCabOVcYnsChk.Value;
          LimiteChk     := QrOVgIspPrfCabLimiteChk.Value;
          LimiteMed     := QrOVgIspPrfCabLimiteMed.Value;
          ZtatusIsp     := CO_OVS_IMPORT_ALHEIO_5120_APTO_DOWNMOBILE;
          ZtatusMot     := 0;
          OVcYnsARQ     := QrOVgIspPrfCabOVcYnsARQ.Value;
          PermiFinHow   := QrOVgIspPrfCabPermiFinHow.Value;
          DtHrAbert     := DtHrIni;
          ZtatusDtH     := DtHrAbert;
          //
          OVS_PF.ObtemSegmentoESecaoDeLocal(Local, SegmntInsp, SeccaoInsp);
          // Criar configuração em itens que conseguiu reservar com sucesso
          OVgIspGerCab := 0;
          OVgIspGerCab := OVS_PF.InsereOVgIspGerCab(OVgIspGerCab, Local, NrOP,
          SeqGrupo, NrReduzidoOP, OVcYnsMed, OVcYnsChk, LimiteChk, LimiteMed,
          ZtatusIsp, ZtatusMot, OVcYnsARQ, PermiFinHow, DtHrAbert, ZtatusDtH,
          SegmntInsp, SeccaoInsp);
          //
          Inseriu := OVgIspGerCab <> 0;
        end;
        if not Inseriu then
        begin
          Dmod.MyDB.Execute(
          ' UPDATE ovgisplassta ' +
          ' SET GrupCnfg=0 ' +
          ' WHERE Local=' + Geral.FF0(QrNaoCfgLocal.Value) +
          ' AND NrOP=' + Geral.FF0(QrNaoCfgNrOP.Value) +
          ' AND SeqGrupo=' + Geral.FF0(QrNaoCfgSeqGrupo.Value) +
          ' AND NrReduzidoOP=' + Geral.FF0(QrNaoCfgNrReduzidoOP.Value) +
          ' AND GrupCnfg=' + Geral.FF0(GrupCnfg) +
          ' AND GrupSgmt=' + Geral.FF0(CO_SGMT_INSP_1024_FACCAO) +
          EmptyStr);
        end;
        //
        QrParaCfg.Next;
      end;
      //
      QrNaoCfg.Next;
    end;
    // Término dos configurados
    DtHrFim := Geral.FDT(DModG.ObtemAgora(), 109);
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'ovgispprfcfg', False, [
    'DtHrFim'], ['Codigo'], [DtHrFim], [GrupCnfg], True) then
    begin
    end;
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrAx, Dmod.MyDB, [
    'SELECT *  ',
    'FROM ovgisplassta ils ',
    'WHERE ils.GrupCnfg=' + Geral.FF0(GrupCnfg),
    'AND ils.IspZtatus>' + Geral.FF0(CO_OVS_IMPORT_ALHEIO_1024_IMPORTADO),
    'AND ils.GrupSgmt=' + Geral.FF0(CO_SGMT_INSP_1024_FACCAO),
    EmptyStr]);
    Mensagem(TSvcMsgKind.smkRunOK, 'Foram configurados ' + Geral.FF0(
    QrAx.RecordCount) + ' itens automaticamente pelo seu perfil');
    //
  end;
end;

procedure TFmOVgIspPrfCfg.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmOVgIspPrfCfg.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
end;

procedure TFmOVgIspPrfCfg.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmOVgIspPrfCfg.Mensagem(SvcMsgKind: TSvcMsgKind; Msg: String);
begin
  if VAR_RUN_AS_SVC then
    ProjGroup_PF.SalvaEmArquivoLog(TSvcMsgKind.smkGetErr, Msg)
  else
  begin
    Geral.MB_Aviso(Msg);
    MyObjects.Informa2(FLaAviso1, FLaAviso2, False, Msg);
  end;
end;

procedure TFmOVgIspPrfCfg.ReopenNaoCfg();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrNaoCfg, Dmod.MyDB, [
  'SELECT DISTINCT ils.Local, ils.NrOP, ils.SeqGrupo,  ',
  'ils.NrReduzidoOP ',
  'FROM ovgisplassta ils ',
  'LEFT JOIN ovdlocal dlo ON dlo.Codigo=ils.Local  ',
  'LEFT JOIN ovdreferencia ref ON ref.Codigo=ils.SeqGrupo ',
  'LEFT JOIN ovgispprfcab prf ON prf.SeqGrupo=ils.SeqGrupo',
  'WHERE ils.IspZtatus=' + Geral.FF0(CO_OVS_IMPORT_ALHEIO_1024_IMPORTADO),
  'AND ils.GrupSgmt=' + Geral.FF0(CO_SGMT_INSP_1024_FACCAO),
  'AND prf.Ativo=1',
  'ORDER BY NrReduzidoOP ',
  EmptyStr]);
end;

function TFmOVgIspPrfCfg.TemNovosItensAconfigurar(): Boolean;
begin
  ReopenNaoCfg();
  Result := QrNaoCfg.RecordCount > 0;
end;

end.
