object FmOVcYnsMixTop: TFmOVcYnsMixTop
  Left = 368
  Top = 194
  Caption = 'OVS-EXGCS-001 :: Cadastros de T'#243'picos de Exa'#231#245'es'
  ClientHeight = 505
  ClientWidth = 784
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnEdita: TPanel
    Left = 0
    Top = 96
    Width = 784
    Height = 409
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    Visible = False
    object GBEdita: TGroupBox
      Left = 0
      Top = 0
      Width = 784
      Height = 61
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 0
      object Label7: TLabel
        Left = 8
        Top = 16
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label9: TLabel
        Left = 68
        Top = 16
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label3: TLabel
        Left = 600
        Top = 16
        Width = 26
        Height = 13
        Caption = 'Sigla:'
      end
      object EdCodigo: TdmkEdit
        Left = 8
        Top = 32
        Width = 56
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Codigo'
        UpdCampo = 'Codigo'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdNome: TdmkEdit
        Left = 68
        Top = 32
        Width = 529
        Height = 21
        MaxLength = 60
        TabOrder = 1
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Nome'
        UpdCampo = 'Nome'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
        OnRedefinido = EdNomeRedefinido
      end
      object EdSigla: TdmkEdit
        Left = 600
        Top = 32
        Width = 165
        Height = 21
        MaxLength = 20
        TabOrder = 2
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Sigla'
        UpdCampo = 'Sigla'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
    end
    object GBConfirma: TGroupBox
      Left = 0
      Top = 346
      Width = 784
      Height = 63
      Align = alBottom
      TabOrder = 2
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 12
        Top = 17
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object Panel1: TPanel
        Left = 644
        Top = 15
        Width = 138
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 7
          Top = 2
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
    end
    object GroupBox2: TGroupBox
      Left = 0
      Top = 61
      Width = 784
      Height = 64
      Align = alTop
      Caption = ' Configura'#231#227'o de medida: (quando aplic'#225'vel):'
      TabOrder = 1
      object PnMensuravl: TPanel
        Left = 101
        Top = 15
        Width = 681
        Height = 47
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 1
        Visible = False
        object Label8: TLabel
          Left = 168
          Top = 4
          Width = 119
          Height = 13
          Caption = 'Sigla unidade de medida:'
        end
        object SpeedButton5: TSpeedButton
          Left = 300
          Top = 19
          Width = 23
          Height = 23
          Caption = '?'
          OnClick = SpeedButton5Click
        end
        object RGTolerBasCalc: TdmkRadioGroup
          Left = 12
          Top = 0
          Width = 153
          Height = 43
          Caption = ' Base de c'#225'lculo: '
          Columns = 2
          ItemIndex = 0
          Items.Strings = (
            'Medida'
            'Percentual')
          TabOrder = 0
          QryCampo = 'TolerBasCalc'
          UpdCampo = 'TolerBasCalc'
          UpdType = utYes
          OldValor = 0
        end
        object EdTolerSglUnMdid: TdmkEdit
          Left = 168
          Top = 20
          Width = 129
          Height = 21
          MaxLength = 20
          TabOrder = 1
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          QryCampo = 'TolerSglUnMdid'
          UpdCampo = 'TolerSglUnMdid'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object EdTlrRndPrcMin: TdmkEdit
          Left = 324
          Top = 20
          Width = 165
          Height = 21
          Alignment = taRightJustify
          TabOrder = 2
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 3
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,000'
          QryCampo = 'TlrRndPrcMin'
          UpdCampo = 'TlrRndPrcMin'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object CkTlrRndUsaMin: TdmkCheckBox
          Left = 324
          Top = 0
          Width = 165
          Height = 17
          Caption = 'Margem m'#237'nima medida  ou %:'
          TabOrder = 3
          OnClick = CkTlrRndUsaMinClick
          QryCampo = 'TlrRndUsaMin'
          UpdCampo = 'TlrRndUsaMin'
          UpdType = utYes
          ValCheck = '1'
          ValUncheck = '0'
          OldValor = #0
        end
        object EdTlrRndPrcMax: TdmkEdit
          Left = 496
          Top = 20
          Width = 165
          Height = 21
          Alignment = taRightJustify
          TabOrder = 4
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 3
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,000'
          QryCampo = 'TlrRndPrcMax'
          UpdCampo = 'TlrRndPrcMax'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object CkTlrRndUsaMax: TdmkCheckBox
          Left = 496
          Top = 0
          Width = 165
          Height = 17
          Caption = 'Margem m'#225'xima medida  ou %:'
          TabOrder = 5
          OnClick = CkTlrRndUsaMaxClick
          QryCampo = 'TlrRndUsaMax'
          UpdCampo = 'TlrRndUsaMax'
          UpdType = utYes
          ValCheck = '1'
          ValUncheck = '0'
          OldValor = #0
        end
      end
      object Panel8: TPanel
        Left = 2
        Top = 15
        Width = 99
        Height = 47
        Align = alLeft
        BevelOuter = bvNone
        TabOrder = 0
        object CkMensuravl: TdmkCheckBox
          Left = 8
          Top = 16
          Width = 97
          Height = 17
          Caption = 'Mensur'#225'vel?'
          TabOrder = 0
          OnClick = CkMensuravlClick
          QryCampo = 'Mensuravl'
          UpdCampo = 'Mensuravl'
          UpdType = utYes
          ValCheck = #0
          ValUncheck = #0
          OldValor = #0
        end
      end
    end
  end
  object PnDados: TPanel
    Left = 0
    Top = 96
    Width = 784
    Height = 409
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    object GBDados: TGroupBox
      Left = 0
      Top = 0
      Width = 784
      Height = 61
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 1
      object Label1: TLabel
        Left = 16
        Top = 16
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
        FocusControl = DBEdCodigo
      end
      object Label2: TLabel
        Left = 76
        Top = 16
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
        FocusControl = DBEdNome
      end
      object Label5: TLabel
        Left = 600
        Top = 16
        Width = 26
        Height = 13
        Caption = 'Sigla:'
      end
      object DBEdCodigo: TdmkDBEdit
        Left = 16
        Top = 32
        Width = 56
        Height = 21
        TabStop = False
        DataField = 'Codigo'
        DataSource = DsOVcYnsMixTop
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 0
        UpdType = utYes
        Alignment = taRightJustify
      end
      object DBEdNome: TdmkDBEdit
        Left = 76
        Top = 32
        Width = 521
        Height = 21
        Color = clWhite
        DataField = 'Nome'
        DataSource = DsOVcYnsMixTop
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        UpdType = utYes
        Alignment = taLeftJustify
      end
      object DBEdit1: TDBEdit
        Left = 600
        Top = 32
        Width = 165
        Height = 21
        DataField = 'Sigla'
        DataSource = DsOVcYnsMixTop
        TabOrder = 2
      end
    end
    object GBCntrl: TGroupBox
      Left = 0
      Top = 345
      Width = 784
      Height = 64
      Align = alBottom
      TabOrder = 0
      object Panel5: TPanel
        Left = 2
        Top = 15
        Width = 172
        Height = 47
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 174
        Top = 15
        Width = 87
        Height = 47
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel3: TPanel
        Left = 261
        Top = 15
        Width = 521
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object Panel2: TPanel
          Left = 388
          Top = 0
          Width = 133
          Height = 47
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 0
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
        object BtCab: TBitBtn
          Tag = 1000494
          Left = 4
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&T'#243'pico'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtCabClick
        end
        object BtIts: TBitBtn
          Tag = 110
          Left = 128
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Avalia'#231#227'o'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = BtItsClick
        end
      end
    end
    object GroupBox1: TGroupBox
      Left = 0
      Top = 61
      Width = 784
      Height = 64
      Align = alTop
      Caption = ' Configura'#231#227'o de medida: (quando aplic'#225'vel):'
      TabOrder = 2
      object PnDBMensuravl: TPanel
        Left = 101
        Top = 15
        Width = 681
        Height = 47
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        Visible = False
        object Label6: TLabel
          Left = 172
          Top = 4
          Width = 134
          Height = 13
          Caption = 'Sigla da unidade de medida:'
          FocusControl = DBEdit3
        end
        object DBRadioGroup1: TDBRadioGroup
          Left = 4
          Top = 0
          Width = 161
          Height = 43
          Caption = ' Base de c'#225'lculo: '
          Columns = 2
          DataField = 'TolerBasCalc'
          DataSource = DsOVcYnsMixTop
          Items.Strings = (
            'Medida'
            'Percentual')
          TabOrder = 0
          Values.Strings = (
            '0'
            '1')
        end
        object DBEdit3: TDBEdit
          Left = 172
          Top = 20
          Width = 149
          Height = 21
          DataField = 'TolerSglUnMdid'
          DataSource = DsOVcYnsMixTop
          TabOrder = 1
        end
        object DBEdit2: TDBEdit
          Left = 328
          Top = 20
          Width = 165
          Height = 21
          DataField = 'TlrRndPrcMin'
          DataSource = DsOVcYnsMixTop
          TabOrder = 2
        end
        object DBCheckBox1: TDBCheckBox
          Left = 328
          Top = 0
          Width = 165
          Height = 17
          Caption = 'Margem m'#237'nima medida  ou %:'
          DataField = 'TlrRndUsaMin'
          DataSource = DsOVcYnsMixTop
          TabOrder = 3
          ValueChecked = '1'
          ValueUnchecked = '0'
        end
        object DBCheckBox2: TDBCheckBox
          Left = 500
          Top = 0
          Width = 165
          Height = 17
          Caption = 'Margem m'#225'xima medida  ou %:'
          DataField = 'TlrRndUsaMax'
          DataSource = DsOVcYnsMixTop
          TabOrder = 4
          ValueChecked = '1'
          ValueUnchecked = '0'
        end
        object DBEdit4: TDBEdit
          Left = 500
          Top = 20
          Width = 165
          Height = 21
          DataField = 'TlrRndPrcMax'
          DataSource = DsOVcYnsMixTop
          TabOrder = 5
        end
      end
      object Panel7: TPanel
        Left = 2
        Top = 15
        Width = 99
        Height = 47
        Align = alLeft
        BevelOuter = bvNone
        TabOrder = 1
        object DBCkMensuravl: TDBCheckBox
          Left = 8
          Top = 16
          Width = 97
          Height = 17
          Caption = 'Mensur'#225'vel.'
          DataField = 'Mensuravl'
          DataSource = DsOVcYnsMixTop
          TabOrder = 0
          ValueChecked = '1'
          ValueUnchecked = '0'
          OnClick = DBCkMensuravlClick
        end
      end
    end
    object PageControl1: TPageControl
      Left = 0
      Top = 125
      Width = 784
      Height = 128
      ActivePage = TabSheet2
      Align = alTop
      TabOrder = 3
      object TabSheet1: TTabSheet
        Caption = ' Opc'#245'es de avalia'#231#227'o'
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object DBGrid1: TDBGrid
          Left = 0
          Top = 0
          Width = 776
          Height = 148
          Align = alTop
          DataSource = DsOVcYnsMixOpc
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'Controle'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Nome'
              Title.Caption = 'Descri'#231#227'o'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_Magnitude'
              Title.Caption = 'Magnitude'
              Width = 140
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'PontNeg'
              Title.Caption = 'Pontos negativos'
              Visible = True
            end>
        end
      end
      object TabSheet2: TTabSheet
        Caption = ' Usado em '
        ImageIndex = 1
        object DGDados: TDBGrid
          Left = 0
          Top = 0
          Width = 776
          Height = 148
          Align = alTop
          DataSource = DsUsouEm
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'Codigo'
              Title.Caption = 'Tabela'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Nome'
              Title.Caption = 'Descri'#231#227'o da tabela'
              Width = 683
              Visible = True
            end>
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 784
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 736
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 12
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 216
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 216
      Top = 0
      Width = 520
      Height = 52
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 415
        Height = 32
        Caption = 'Cadastros de T'#243'picos de Exa'#231#245'es'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 415
        Height = 32
        Caption = 'Cadastros de T'#243'picos de Exa'#231#245'es'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 415
        Height = 32
        Caption = 'Cadastros de T'#243'picos de Exa'#231#245'es'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 52
    Width = 784
    Height = 44
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 780
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = BtCab
    Left = 120
    Top = 64
  end
  object QrOVcYnsMixTop: TMySQLQuery
    Database = Dmod.ZZDB
    BeforeOpen = QrOVcYnsMixTopBeforeOpen
    AfterOpen = QrOVcYnsMixTopAfterOpen
    BeforeClose = QrOVcYnsMixTopBeforeClose
    AfterScroll = QrOVcYnsMixTopAfterScroll
    SQL.Strings = (
      'SELECT '
      
        'ELT(ygt.TolerBasCalc+1, "Medida", "Percentual") NO_TolerBasCalc,' +
        ' '
      'ygt.* '
      'FROM ovcynsmixtop ygt '
      ''
      'WHERE ygt.Codigo>0'
      'AND ygt.Codigo=:P0')
    Left = 92
    Top = 233
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrOVcYnsMixTopNO_TolerBasCalc: TWideStringField
      FieldName = 'NO_TolerBasCalc'
      Size = 10
    end
    object QrOVcYnsMixTopCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrOVcYnsMixTopNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
    object QrOVcYnsMixTopSigla: TWideStringField
      FieldName = 'Sigla'
    end
    object QrOVcYnsMixTopMensuravl: TSmallintField
      FieldName = 'Mensuravl'
      Required = True
    end
    object QrOVcYnsMixTopTolerBasCalc: TSmallintField
      FieldName = 'TolerBasCalc'
      Required = True
    end
    object QrOVcYnsMixTopTolerSglUnMdid: TWideStringField
      FieldName = 'TolerSglUnMdid'
      Size = 15
    end
    object QrOVcYnsMixTopTlrRndUsaMin: TSmallintField
      FieldName = 'TlrRndUsaMin'
      Required = True
    end
    object QrOVcYnsMixTopTlrRndPrcMin: TFloatField
      FieldName = 'TlrRndPrcMin'
      Required = True
    end
    object QrOVcYnsMixTopTlrRndUsaMax: TSmallintField
      FieldName = 'TlrRndUsaMax'
      Required = True
    end
    object QrOVcYnsMixTopTlrRndPrcMax: TFloatField
      FieldName = 'TlrRndPrcMax'
      Required = True
    end
    object QrOVcYnsMixTopLk: TIntegerField
      FieldName = 'Lk'
      Required = True
    end
    object QrOVcYnsMixTopDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrOVcYnsMixTopDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrOVcYnsMixTopUserCad: TIntegerField
      FieldName = 'UserCad'
      Required = True
    end
    object QrOVcYnsMixTopUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Required = True
    end
    object QrOVcYnsMixTopAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Required = True
    end
    object QrOVcYnsMixTopAWServerID: TIntegerField
      FieldName = 'AWServerID'
      Required = True
    end
    object QrOVcYnsMixTopAWStatSinc: TSmallintField
      FieldName = 'AWStatSinc'
      Required = True
    end
    object QrOVcYnsMixTopAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
  end
  object DsOVcYnsMixTop: TDataSource
    DataSet = QrOVcYnsMixTop
    Left = 92
    Top = 281
  end
  object QrUsouEm: TMySQLQuery
    Database = Dmod.ZZDB
    SQL.Strings = (
      'SELECT ymc.Codigo, ymc.Nome '
      'FROM ovcynsmedtop ymt '
      'LEFT JOIN ovcynsmedcad ymc ON ymc.Codigo=ymt.Tobiko '
      'WHERE Tobiko=1 ')
    Left = 448
    Top = 237
    object QrUsouEmCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrUsouEmNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
  end
  object DsUsouEm: TDataSource
    DataSet = QrUsouEm
    Left = 448
    Top = 285
  end
  object PMIts: TPopupMenu
    OnPopup = PMItsPopup
    Left = 424
    Top = 376
    object ItsInclui1: TMenuItem
      Caption = '&Adiciona'
      Enabled = False
      OnClick = ItsInclui1Click
    end
    object ItsAltera1: TMenuItem
      Caption = '&Edita'
      Enabled = False
      OnClick = ItsAltera1Click
    end
    object ItsExclui1: TMenuItem
      Caption = '&Remove'
      Enabled = False
      OnClick = ItsExclui1Click
    end
  end
  object PMCab: TPopupMenu
    OnPopup = PMCabPopup
    Left = 300
    Top = 372
    object CabInclui1: TMenuItem
      Caption = '&Inclui'
      OnClick = CabInclui1Click
    end
    object CabAltera1: TMenuItem
      Caption = '&Altera'
      Enabled = False
      OnClick = CabAltera1Click
    end
    object CabExclui1: TMenuItem
      Caption = '&Exclui'
      Enabled = False
      OnClick = CabExclui1Click
    end
  end
  object QrOVcYnsMixOpc: TMySQLQuery
    Database = Dmod.ZZDB
    SQL.Strings = (
      'SELECT mag.Nome NO_Magnitude, opc.* '
      'FROM ovcynsmixopc opc'
      'LEFT JOIN ovcynsqstmag mag ON  mag.Codigo=opc.Magnitude'
      'WHERE opc.Codigo=0')
    Left = 280
    Top = 237
    object QrOVcYnsMixOpcNO_Magnitude: TWideStringField
      FieldName = 'NO_Magnitude'
      Size = 60
    end
    object QrOVcYnsMixOpcCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrOVcYnsMixOpcControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrOVcYnsMixOpcNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
    object QrOVcYnsMixOpcMagnitude: TIntegerField
      FieldName = 'Magnitude'
      Required = True
    end
    object QrOVcYnsMixOpcPontNeg: TIntegerField
      FieldName = 'PontNeg'
      Required = True
    end
    object QrOVcYnsMixOpcLk: TIntegerField
      FieldName = 'Lk'
      Required = True
    end
    object QrOVcYnsMixOpcDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrOVcYnsMixOpcDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrOVcYnsMixOpcUserCad: TIntegerField
      FieldName = 'UserCad'
      Required = True
    end
    object QrOVcYnsMixOpcUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Required = True
    end
    object QrOVcYnsMixOpcAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Required = True
    end
    object QrOVcYnsMixOpcAWServerID: TIntegerField
      FieldName = 'AWServerID'
      Required = True
    end
    object QrOVcYnsMixOpcAWStatSinc: TSmallintField
      FieldName = 'AWStatSinc'
      Required = True
    end
    object QrOVcYnsMixOpcAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
  end
  object DsOVcYnsMixOpc: TDataSource
    DataSet = QrOVcYnsMixOpc
    Left = 280
    Top = 285
  end
end
