unit OVcYnsExgTop;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, DBCtrls, dmkLabel, Mask,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkDBEdit, dmkGeral, Variants,
  dmkValUsu, dmkImage, UnDmkEnums, dmkRadioGroup, UnOVS_Vars, dmkCheckBox;

type
  TFmOVcYnsExgTop = class(TForm)
    Panel1: TPanel;
    CkContinuar: TCheckBox;
    GroupBox1: TGroupBox;
    DBEdCodigo: TdmkDBEdit;
    Label5: TLabel;
    DBEdNome: TDBEdit;
    Label3: TLabel;
    GroupBox2: TGroupBox;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    QrOVcYnsMixTop: TMySQLQuery;
    DsOVcYnsMixTop: TDataSource;
    Panel3: TPanel;
    Label6: TLabel;
    EdControle: TdmkEdit;
    Label7: TLabel;
    EdNome: TdmkEdit;
    Label1: TLabel;
    EdTopyko: TdmkEditCB;
    CBTopyko: TdmkDBLookupComboBox;
    SbTobiko: TSpeedButton;
    Panel5: TPanel;
    PnMedida: TPanel;
    RGTolerBasCalc: TdmkRadioGroup;
    Label2: TLabel;
    EdMedidCer: TdmkEdit;
    Label4: TLabel;
    EdMedidMin: TdmkEdit;
    Label9: TLabel;
    EdMedidMax: TdmkEdit;
    Panel7: TPanel;
    CkMensuravl: TdmkCheckBox;
    PnMargem: TPanel;
    EdTolerSglUnMdid: TdmkEdit;
    CkTlrRndUsaMax: TdmkCheckBox;
    EdTlrRndPrcMax: TdmkEdit;
    EdTlrRndPrcMin: TdmkEdit;
    CkTlrRndUsaMin: TdmkCheckBox;
    SpeedButton5: TSpeedButton;
    Label8: TLabel;
    QrOVcYnsMixTopCodigo: TIntegerField;
    QrOVcYnsMixTopNome: TWideStringField;
    QrOVcYnsMixTopTolerBasCalc: TSmallintField;
    QrOVcYnsMixTopTolerSglUnMdid: TWideStringField;
    QrOVcYnsMixTopTlrRndUsaMin: TSmallintField;
    QrOVcYnsMixTopTlrRndUsaMax: TSmallintField;
    QrOVcYnsMixTopTlrRndPrcMin: TFloatField;
    QrOVcYnsMixTopTlrRndPrcMax: TFloatField;
    QrOVcYnsMixTopMensuravl: TSmallintField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure SbTobikoClick(Sender: TObject);
    procedure EdTopykoRedefinido(Sender: TObject);
    procedure EdTopykoKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure CBTopykoKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure CkMensuravlClick(Sender: TObject);
    procedure CkTlrRndUsaMinClick(Sender: TObject);
    procedure EdTlrRndPrcMinRedefinido(Sender: TObject);
    procedure CkTlrRndUsaMaxClick(Sender: TObject);
    procedure EdTlrRndPrcMaxRedefinido(Sender: TObject);
    procedure RGTolerBasCalcClick(Sender: TObject);
    procedure EdMedidCerRedefinido(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenTop(Controle: Integer);
    procedure ReopenOVcYnsQstTop(Quais: TWhatPsqHow);
    procedure TopicoKeyUp(Sender: TObject; Key: Word; Shift: TShiftState);
    procedure CalculaMinMax();
  public
    { Public declarations }
    FQrIts: TmySQLQuery;
    FDsCad, FDsIts: TDataSource;
    FContexRef: Integer;
  end;

  var
  FmOVcYnsExgTop: TFmOVcYnsExgTop;

implementation

uses UnMyObjects, Module, UMySQLModule, UnInternalConsts, MyDBCheck, DmkDAC_PF,
  UnOVS_Jan, OVcYnsMedCad;

{$R *.DFM}

procedure TFmOVcYnsExgTop.BtOKClick(Sender: TObject);
var
  TolerSglUnMdid: String;
  Codigo, Controle, Topyko, Mensuravl, TolerBasCalc, TlrRndUsaMin, TlrRndUsaMax: Integer;
  TlrRndPrcMin, TlrRndPrcMax, MedidCer, MedidMin, MedidMax: Double;
  SQLType: TSQLType;
begin
  SQLType        := ImgTipo.SQLType;
  Codigo         := Geral.IMV(DBEdCodigo.Text);
  Controle       := EdControle.ValueVariant;
  Topyko         := EdTopyko.ValueVariant;
  Mensuravl      := Geral.BoolToInt(CkMensuravl.Checked);
  if CkMensuravl.Checked then
  begin
    TolerBasCalc   := RGTolerBasCalc.ItemIndex;
    TolerSglUnMdid := EdTolerSglUnMdid.ValueVariant;
    TlrRndUsaMin   := Geral.BoolToInt(CkTlrRndUsaMin.Checked);
    TlrRndPrcMin   := EdTlrRndPrcMin.ValueVariant;
    TlrRndUsaMax   := Geral.BoolToInt(CkTlrRndUsaMax.Checked);
    TlrRndPrcMax   := EdTlrRndPrcMax.ValueVariant;
    MedidCer       := EdMedidCer.ValueVariant;
    MedidMin       := EdMedidMin.ValueVariant;
    MedidMax       := EdMedidMax.ValueVariant;
  end else
  begin
    TolerBasCalc   := RGTolerBasCalc.ItemIndex;
    TolerSglUnMdid := '';
    TlrRndUsaMin   := 0;
    TlrRndPrcMin   := 0.00;
    TlrRndUsaMax   := 0;
    TlrRndPrcMax   := 0.00;
    MedidCer       := 0.00;
    MedidMin       := 0.00;
    MedidMax       := 0.00;
  end;
  //
  if MyObjects.FIC(Topyko = 0, EdTopyko, 'Informe o T�pico!') then
    Exit;
  //
  Controle := UMyMod.BPGS1I32('ovcynsexgtop', 'Controle', '', '', tsPos, SQLType, Controle);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'ovcynsexgtop', False, [
  'Codigo', 'Topyko', 'Mensuravl',
  'TolerBasCalc', 'TolerSglUnMdid', 'TlrRndUsaMin',
  'TlrRndPrcMin', 'TlrRndUsaMax', 'TlrRndPrcMax',
  'MedidCer', 'MedidMin', 'MedidMax'], [
  'Controle'], [
  Codigo, Topyko, Mensuravl,
  TolerBasCalc, TolerSglUnMdid, TlrRndUsaMin,
  TlrRndPrcMin, TlrRndUsaMax, TlrRndPrcMax,
  MedidCer, MedidMin, MedidMax], [
  Controle], True) then
  begin
    ReopenTop(Controle);
    if CkContinuar.Checked then
    begin
      ImgTipo.SQLType              := stIns;
      EdControle.ValueVariant      := 0;
      EdTopyko.ValueVariant        := 0;
      CBTopyko.KeyValue            := Null;
      //RGTolerBasCalc.ItemIndex     := 0;
      //RGTolerUnMdida.ItemIndex     := 0;
      EdNome.ValueVariant          := '';
      //EdTolerRndPerc.ValueVariant  := 0;
      //
      EdTopyko.SetFocus;
    end else Close;
  end;
end;

procedure TFmOVcYnsExgTop.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmOVcYnsExgTop.CalculaMinMax();
  function MsgErro(): Double;
  begin
    Geral.MB_Erro('Tipo de Base de c�lculo n�o implementado!');
    Result := 0;
  end;
var
  TolerBasCalc: Integer;
  MedidCer, TlrRndPrcMin, TlrRndPrcMax, MedidMin, MedidMax, Incremento: Double;
  Mensuravl, TlrRndUsaMin, TlrRndUsaMax: Boolean;
begin
  MedidMin     := 0;
  MedidMax     := 0;
  Mensuravl    := CkMensuravl.Checked;
  //
  if Mensuravl then
  begin
    TolerBasCalc := RGTolerBasCalc.ItemIndex;
    MedidCer     := EdMedidCer.ValueVariant;
    TlrRndUsaMin := CkTlrRndUsaMin.Checked;
    TlrRndUsaMax := CkTlrRndUsaMax.Checked;
    TlrRndPrcMin := EdTlrRndPrcMin.ValueVariant;
    TlrRndPrcMax := EdTlrRndPrcMax.ValueVariant;
    //
    if TlrRndUsaMin then
    begin
      case TolerBasCalc of
        0: Incremento := TlrRndPrcMin;
        1: Incremento := (TlrRndPrcMin * MedidCer) / 100;
        else Incremento := MsgErro();
      end;
      MedidMin := MedidCer - Incremento;
      if MedidMin < 0 then
        MedidMin := 0;
    end;
    //
    if TlrRndUsaMax then
    begin
      case TolerBasCalc of
        0: Incremento := TlrRndPrcMax;
        1: Incremento := (TlrRndPrcMax * MedidCer) / 100;
        else Incremento := MsgErro();
      end;
      MedidMax := MedidCer + Incremento;
      if MedidMax < 0 then
        MedidMax := 0;
    end;
  end;
  //
  EdMedidMin.ValueVariant := MedidMin;
  EdMedidMax.ValueVariant := MedidMax;
  //
end;

procedure TFmOVcYnsExgTop.CBTopykoKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  TopicoKeyUp(Sender, Key, Shift);
end;

procedure TFmOVcYnsExgTop.CkMensuravlClick(Sender: TObject);
begin
  PnMargem.Visible := CkMensuravl.Checked;
  PnMedida.Visible := CkMensuravl.Checked;
end;

procedure TFmOVcYnsExgTop.CkTlrRndUsaMaxClick(Sender: TObject);
begin
  CalculaMinMax();
end;

procedure TFmOVcYnsExgTop.CkTlrRndUsaMinClick(Sender: TObject);
begin
  CalculaMinMax();
end;

procedure TFmOVcYnsExgTop.EdMedidCerRedefinido(Sender: TObject);
begin
  CalculaMinMax();
end;

procedure TFmOVcYnsExgTop.EdTlrRndPrcMaxRedefinido(Sender: TObject);
begin
  CalculaMinMax();
end;

procedure TFmOVcYnsExgTop.EdTlrRndPrcMinRedefinido(Sender: TObject);
begin
  CalculaMinMax();
end;

procedure TFmOVcYnsExgTop.EdTopykoKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  TopicoKeyUp(Sender, Key, Shift);
end;

procedure TFmOVcYnsExgTop.EdTopykoRedefinido(Sender: TObject);
begin
  CkMensuravl.Checked           := Geral.IntToBool(QrOVcYnsMixTopMensuravl.Value);
  RGTolerBasCalc.ItemIndex      := QrOVcYnsMixTopTolerBasCalc.Value;
  EdTolerSglUnMdid.ValueVariant := QrOVcYnsMixTopTolerSglUnMdid.Value;
  CkTlrRndUsaMin.Checked        := Geral.IntToBool(QrOVcYnsMixTopTlrRndUsaMin.Value);
  CkTlrRndUsaMax.Checked        := Geral.IntToBool(QrOVcYnsMixTopTlrRndUsaMax.Value);
  EdTlrRndPrcMin.ValueVariant   := QrOVcYnsMixTopTlrRndPrcMin.Value;
  EdTlrRndPrcMax.ValueVariant   := QrOVcYnsMixTopTlrRndPrcMax.Value;
  //
  CalculaMinMax();
end;

procedure TFmOVcYnsExgTop.FormActivate(Sender: TObject);
begin
  DBEdCodigo.DataSource   := FDsCad;
  DBEdNome.DataSource     := FDsCad;
  //
  MyObjects.CorIniComponente();
end;

procedure TFmOVcYnsExgTop.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  ReopenOVcYnsQstTop(TWhatPsqHow.wphAll);
end;

procedure TFmOVcYnsExgTop.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmOVcYnsExgTop.ReopenTop(Controle: Integer);
begin
  if FQrIts <> nil then
  begin
    UnDmkDAC_PF.AbreQuery(FQrIts, FQrIts.Database);
    //
    if Controle <> 0 then
      FQrIts.Locate('Controle', Controle, []);
  end;
end;

procedure TFmOVcYnsExgTop.RGTolerBasCalcClick(Sender: TObject);
begin
  CalculaMinMax();
end;

procedure TFmOVcYnsExgTop.ReopenOVcYnsQstTop(Quais: TWhatPsqHow);
begin
  UnDmkDAC_PF.AbreQuery(QrOVcYnsMixTop, DMod.MyDB);
end;

procedure TFmOVcYnsExgTop.SbTobikoClick(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  OVS_Jan.MostraFormOVcYnsMixTop();
  UMyMod.SetaCodigoPesquisado(EdTopyko, CBTopyko, QrOVcYnsMixTop, VAR_CADASTRO);
end;

procedure TFmOVcYnsExgTop.TopicoKeyUp(Sender: TObject; Key: Word;
  Shift: TShiftState);
begin
  //
end;

end.
