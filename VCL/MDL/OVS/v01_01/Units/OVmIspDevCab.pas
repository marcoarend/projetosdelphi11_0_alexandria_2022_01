unit OVmIspDevCab;
interface

uses
  Windows, Messages, SysUtils, Classes, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2,
  ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, dmkGeral,
  dmkPermissoes, dmkEdit, dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup,
  unDmkProcFunc, UnDmkEnums, Vcl.ComCtrls, Vcl.Grids, Vcl.DBGrids,
  System.NetEncoding, System.IOUtils, Vcl.Imaging.jpeg,
  FMX.Graphics, Vcl.Graphics, System.UITypes,
  Soap.EncdDecd, dmkDBGridZTO, UnDmkColor;

type
  TFmOVmIspDevCab = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    BtExclui: TBitBtn;
    BtAltera: TBitBtn;
    BtEmail: TBitBtn;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    QrOVmIspDevCab: TMySQLQuery;
    DsOVmIspDevCab: TDataSource;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    Label9: TLabel;
    EdNome: TdmkEdit;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    QrOVmIspDevCabCodigo: TIntegerField;
    QrOVmIspDevCabCodInMob: TIntegerField;
    QrOVmIspDevCabDeviceSI: TIntegerField;
    QrOVmIspDevCabDeviceID: TWideStringField;
    QrOVmIspDevCabDeviceCU: TIntegerField;
    QrOVmIspDevCabNome: TWideStringField;
    QrOVmIspDevCabOVgIspGer: TIntegerField;
    QrOVmIspDevCabLocal: TIntegerField;
    QrOVmIspDevCabNrOP: TIntegerField;
    QrOVmIspDevCabSeqGrupo: TIntegerField;
    QrOVmIspDevCabNrReduzidoOP: TIntegerField;
    QrOVmIspDevCabDtHrAbert: TDateTimeField;
    QrOVmIspDevCabDtHrFecha: TDateTimeField;
    QrOVmIspDevCabOVcYnsMed: TIntegerField;
    QrOVmIspDevCabOVcYnsChk: TIntegerField;
    QrOVmIspDevCabOVcYnsARQ: TIntegerField;
    QrOVmIspDevCabLimiteChk: TIntegerField;
    QrOVmIspDevCabLimiteMed: TIntegerField;
    QrOVmIspDevCabPecasIsp: TIntegerField;
    QrOVmIspDevCabPecaAtual: TIntegerField;
    QrOVmIspDevCabLk: TIntegerField;
    QrOVmIspDevCabDataCad: TDateField;
    QrOVmIspDevCabDataAlt: TDateField;
    QrOVmIspDevCabUserCad: TIntegerField;
    QrOVmIspDevCabUserAlt: TIntegerField;
    QrOVmIspDevCabAlterWeb: TSmallintField;
    QrOVmIspDevCabAWServerID: TIntegerField;
    QrOVmIspDevCabAWStatSinc: TSmallintField;
    QrOVmIspDevCabAtivo: TSmallintField;
    QrOVmIspDevCabQtReal: TFloatField;
    QrOVmIspDevCabQtLocal: TFloatField;
    QrOVmIspDevCabProduto: TIntegerField;
    QrOVmIspDevCabCodGrade: TIntegerField;
    QrOVmIspDevCabCodTam: TWideStringField;
    QrOVmIspDevCabPontosTot: TIntegerField;
    QrOVmIspDevCabInspResul: TIntegerField;
    QrOVmIspDevCabInspeSeq: TIntegerField;
    QrOVmIspDevCabRandmStr: TWideStringField;
    QrOVmIspDevCabDtHrUpIni: TDateTimeField;
    QrOVmIspDevCabDtHrUpFim: TDateTimeField;
    QrOVmIspDevCabEmpresa: TIntegerField;
    QrOVmIspDevCabNO_OVcYnsMed: TWideStringField;
    QrOVmIspDevCabNO_OVcYnsChk: TWideStringField;
    QrOVmIspDevCabNO_Local: TWideStringField;
    QrOVmIspDevCabNO_Referencia: TWideStringField;
    QrOVmIspDevCabNO_InspResul: TWideStringField;
    QrOVmIspDevCabNO_OVcYnsARQ: TWideStringField;
    QrOVmIspDevCabDtHrAbert_TXT: TWideStringField;
    QrOVmIspDevCabDtHrFecha_TXT: TWideStringField;
    GBDados: TGroupBox;
    Panel6: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Label17: TLabel;
    Label18: TLabel;
    Label19: TLabel;
    Label20: TLabel;
    DBEdCodigo: TdmkDBEdit;
    DBEdNome: TdmkDBEdit;
    dmkDBEdit1: TdmkDBEdit;
    dmkDBEdit2: TdmkDBEdit;
    dmkDBEdit5: TdmkDBEdit;
    dmkDBEdit6: TdmkDBEdit;
    dmkDBEdit7: TdmkDBEdit;
    dmkDBEdit8: TdmkDBEdit;
    Label3: TLabel;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    Label4: TLabel;
    Label6: TLabel;
    DBEdit4: TDBEdit;
    DBEdit5: TDBEdit;
    Label8: TLabel;
    Label5: TLabel;
    DBEdit3: TDBEdit;
    DBEdit6: TDBEdit;
    Label10: TLabel;
    Label11: TLabel;
    DBEdit7: TDBEdit;
    DBEdit9: TDBEdit;
    DBEdit8: TDBEdit;
    Label12: TLabel;
    DBEdit10: TDBEdit;
    Label14: TLabel;
    DBEdit12: TDBEdit;
    DBEdit11: TDBEdit;
    Label16: TLabel;
    PGDados: TPageControl;
    TabSheet1: TTabSheet;
    QrPontosNeg: TMySQLQuery;
    QrPontosNegPecaSeq: TIntegerField;
    QrPontosNegNO_Topico: TWideStringField;
    QrPontosNegPontNeg: TIntegerField;
    DsPontosNeg: TDataSource;
    DBGrid1: TDBGrid;
    Label13: TLabel;
    DBEdit13: TDBEdit;
    DBEdit14: TDBEdit;
    Label15: TLabel;
    QrPontosNegIdTabela: TWideStringField;
    QrPontosNegTabela: TWideStringField;
    Label21: TLabel;
    DBEdit15: TDBEdit;
    DBEdit16: TDBEdit;
    Label23: TLabel;
    DBEdit17: TDBEdit;
    Label22: TLabel;
    DBEdit18: TDBEdit;
    Label24: TLabel;
    DBEdit19: TDBEdit;
    Label25: TLabel;
    DBEdit20: TDBEdit;
    Label26: TLabel;
    DBEdit21: TDBEdit;
    Label27: TLabel;
    DBEdit22: TDBEdit;
    Label28: TLabel;
    DBEdit23: TDBEdit;
    Label29: TLabel;
    DBEdit24: TDBEdit;
    Label30: TLabel;
    DBEdit25: TDBEdit;
    QrPontosNegQtdFotos: TIntegerField;
    Panel7: TPanel;
    DBGrid2: TDBGrid;
    Label31: TLabel;
    QrPontosNegCtrlInMob: TIntegerField;
    QrOVmIspDevFts: TMySQLQuery;
    DsOVmIspDevFts: TDataSource;
    QrOVmIspDevFtsCodigo: TIntegerField;
    QrOVmIspDevFtsIdTabela: TSmallintField;
    QrOVmIspDevFtsCodInMob: TIntegerField;
    QrOVmIspDevFtsCtrlInMob: TIntegerField;
    QrOVmIspDevFtsDataHora: TDateTimeField;
    QrOVmIspDevFtsNomeArq: TWideStringField;
    QrOVmIspDevFtsLk: TIntegerField;
    QrOVmIspDevFtsDataCad: TDateField;
    QrOVmIspDevFtsDataAlt: TDateField;
    QrOVmIspDevFtsUserCad: TIntegerField;
    QrOVmIspDevFtsUserAlt: TIntegerField;
    QrOVmIspDevFtsAlterWeb: TSmallintField;
    QrOVmIspDevFtsAWServerID: TIntegerField;
    QrOVmIspDevFtsAWStatSinc: TSmallintField;
    QrOVmIspDevFtsAtivo: TSmallintField;
    QrOVmIspDevFtsNoArqSvr: TWideStringField;
    Panel8: TPanel;
    ImgFoto: TImage;
    LaNomeFoto: TLabel;
    QrOVmIspDevBmp: TMySQLQuery;
    QrOVmIspDevBmpTxtBmp: TWideMemoField;
    QrOVmIspDevCabUserNmePdr: TWideStringField;
    QrOVmIspDevCabPerMedReal: TFloatField;
    QrOVmIspDevCabPerChkReal: TFloatField;
    QrOVmIspDevCabDtHrMailSnt: TFloatField;
    Label32: TLabel;
    DBEdit26: TDBEdit;
    Label33: TLabel;
    DBEdit27: TDBEdit;
    Label34: TLabel;
    DBEdit28: TDBEdit;
    TabSheet2: TTabSheet;
    Panel9: TPanel;
    DBGMedPecas: TdmkDBGridZTO;
    QrMedPecas: TMySQLQuery;
    QrMedPecasPecaSeq: TIntegerField;
    QrMedPecasItens: TLargeintField;
    DsMedPecas: TDataSource;
    QrMedNPeca: TMySQLQuery;
    DsMedNPeca: TDataSource;
    DBGMedNPeca: TdmkDBGridZTO;
    QrMedNPecaNO_TolerBasCalc: TWideStringField;
    QrMedNPecaNO_TolerUnMdida: TWideStringField;
    QrMedNPecaNO_TOBIKO: TWideStringField;
    QrMedNPecaTolerBasCalc: TSmallintField;
    QrMedNPecaTolerUnMdida: TSmallintField;
    QrMedNPecaTolerRndPerc: TFloatField;
    QrMedNPecaNO_MedidOky: TWideStringField;
    QrMedNPecaCodigo: TIntegerField;
    QrMedNPecaCodInMob: TIntegerField;
    QrMedNPecaCtrlInMob: TIntegerField;
    QrMedNPecaPecaSeq: TIntegerField;
    QrMedNPecaOVcYnsMedDim: TIntegerField;
    QrMedNPecaOVcYnsMedTop: TIntegerField;
    QrMedNPecaTobiko: TIntegerField;
    QrMedNPecaCodGrade: TIntegerField;
    QrMedNPecaCodTam: TWideStringField;
    QrMedNPecaMedidFei: TFloatField;
    QrMedNPecaMedidOky: TSmallintField;
    QrMedNPecaPontNeg: TIntegerField;
    QrMedNPecaMedidCer: TFloatField;
    QrMedNPecaMedidMin: TFloatField;
    QrMedNPecaMedidMax: TFloatField;
    QrMedNPecaUlWayInz: TSmallintField;
    QrMedNPecaQtdFotos: TIntegerField;
    QrMedPecasExatas: TFloatField;
    QrMedPecasDentro: TFloatField;
    QrMedPecasFora: TFloatField;
    QrMedPecasPtsDesvioMed: TFloatField;
    QrMedPecasMaxPtsDesvio: TFloatField;
    QrMedPecasPercDesvio: TFloatField;
    QrMedNPecaPtsDesvio: TFloatField;
    QrMedNPecaPercDesvio: TFloatField;
    Memo1: TMemo;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrOVmIspDevCabAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrOVmIspDevCabBeforeOpen(DataSet: TDataSet);
    procedure BtEmailClick(Sender: TObject);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure QrOVmIspDevCabAfterScroll(DataSet: TDataSet);
    procedure QrOVmIspDevCabBeforeClose(DataSet: TDataSet);
    procedure QrPontosNegAfterScroll(DataSet: TDataSet);
    procedure QrOVmIspDevFtsAfterScroll(DataSet: TDataSet);
    procedure Button1Click(Sender: TObject);
    procedure QrPontosNegBeforeClose(DataSet: TDataSet);
    procedure QrMedPecasBeforeClose(DataSet: TDataSet);
    procedure QrMedPecasAfterScroll(DataSet: TDataSet);
    procedure DBGMedPecasDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure DBGMedNPecaDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    //
    procedure ReopenMedPecas();
    procedure ReopenMedNPeca();
    procedure ReopenPontosNeg();
    procedure ReopenOVmIspDevFts();
    //procedure BitmapFromBase64(Base64: string; Bitmap: TBitmap);
    function ConvertFmxBitmapToVclBitmap(b: FMX.Graphics.TBitmap): Vcl.Graphics.TBitmap;
  public
    { Public declarations }
    procedure LocCod(Atual, Codigo: Integer);

  end;

var
  FmOVmIspDevCab: TFmOVmIspDevCab;
const
  FFormatFloat = '00000';
  FFatorPot = '4';   // Pow(4,4) = 256!
  //FBestHue = 240;  // Azul                      > 240  de 360 (ou 160 de 240 no Paint)
  FBestHue = 200;    // Verde azulado             > 150  de 360 (ou 100 de 240 no Paint)
  FLaranja = 22.5;   // Laranja pouco avermelhado > 22,5 de 360 (ou 15 de 240 no Paint)
  FBestLum = 50;     // Vivacidade 100%           > 50   de 100 (ou 120 de 240 no Paint)

implementation

uses UnMyObjects, Module, DmkDAC_PF, UnOVS_Consts, UnREST_App, UnProjGroupEnums,
  MyDBCheck, OVmIspDevPsq, UnOVS_ProjGroupVars, UnOVS_PF;

{$R *.DFM}

function BitmapFromBase64(const base64: string):FMX.Graphics.TBitmap;
var
  Input: TStringStream;
  Output: TBytesStream;
begin
  Input := TStringStream.Create(base64, TEncoding.ASCII);
  try
    Output := TBytesStream.Create;
    try
      Soap.EncdDecd.DecodeStream(Input, Output);
      Output.Position := 0;
      Result := FMX.Graphics.TBitmap.Create;
      try
        Result.LoadFromStream(Output);
      except
        Result.Free;
        raise;
      end;
    finally
      Output.Free;
    end;
  finally
    Input.Free;
  end;
end;


/////////////////////////////////////////////////////////////////////////////////////
procedure TFmOVmIspDevCab.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmOVmIspDevCab.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrOVmIspDevCabCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmOVmIspDevCab.DefParams;
begin
  VAR_GOTOTABELA := 'ovmIspdevcab';
  VAR_GOTOMYSQLTABLE := QrOVmIspDevCab;
  VAR_GOTONEG := gotoPos;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT igc.*, ymc.Nome NO_OVcYnsMed, ycc.Nome NO_OVcYnsChk,');
  VAR_SQLx.Add('dlo.Nome NO_Local, ref.Nome NO_Referencia,');
  VAR_SQLx.Add('isc.Nome NO_InspResul, yac.Nome NO_OVcYnsARQ,');
  VAR_SQLx.Add('IF(igc.DtHrAbert  <= "1899-12-30", "",');
  VAR_SQLx.Add('  DATE_FORMAT(igc.DtHrAbert, "%d/%m/%Y %H:%i:%s")) DtHrAbert_TXT,');
  VAR_SQLx.Add('IF(igc.DtHrFecha  <= "1899-12-30", "",');
  VAR_SQLx.Add('  DATE_FORMAT(igc.DtHrFecha, "%d/%m/%Y %H:%i:%s")) DtHrFecha_TXT,');
  VAR_SQLx.Add('mdc.UserNmePdr ');
  VAR_SQLx.Add('FROM ovmispdevcab igc');
  VAR_SQLx.Add('LEFT JOIN ovdlocal dlo ON dlo.Codigo=igc.Local');
  VAR_SQLx.Add('LEFT JOIN ovdreferencia ref ON ref.Codigo=igc.SeqGrupo');
  VAR_SQLx.Add('LEFT JOIN ovcynsmedcad ymc ON ymc.Codigo=igc.OVcYnsMed');
  VAR_SQLx.Add('LEFT JOIN ovcynschkcad ycc ON ycc.Codigo=igc.OVcYnsChk');
  VAR_SQLx.Add('LEFT JOIN ovgisprescad isc ON isc.Codigo=igc.InspResul');
  VAR_SQLx.Add('LEFT JOIN OVcYnsARQCad yac ON yac.Codigo=igc.OVcYnsARQ');
  VAR_SQLx.Add('LEFT JOIN ovcmobdevcad mdc ON mdc.Codigo=igc.DeviceSI');
  VAR_SQLx.Add('WHERE igc.Codigo > 0');
  //
  VAR_SQL1.Add('AND igc.Codigo=:P0');
  //
  //VAR_SQL2.Add('AND igc.CodUsu=:P0');
  //
  VAR_SQLa.Add('AND igc.Nome Like :P0');
  //

  VAR_SQLx.Add('');
end;

function TFmOVmIspDevCab.ConvertFmxBitmapToVclBitmap(
  b: FMX.Graphics.TBitmap): Vcl.Graphics.TBitmap;
var
  data:FMX.Graphics.TBitmapData;
  i,j:Integer;
  AlphaColor: TAlphaColor;
begin
  Result := VCL.Graphics.TBitmap.Create;
  Result.SetSize(b.Width, b.Height);
  if(b.Map(TMapAccess.Readwrite, data)) then
  try
    for i := 0 to data.Height-1 do
    begin
      for j := 0 to data.Width-1 do
      begin
        AlphaColor:=data.GetPixel(j,i);
        Result.Canvas.Pixels[j,i]:=
          RGB(
            TAlphaColorRec(AlphaColor).R,
            TAlphaColorRec(AlphaColor).G,
            TAlphaColorRec(AlphaColor).B
          );
      end;
    end;
  finally
    b.Unmap(data);
  end;
end;

procedure TFmOVmIspDevCab.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmOVmIspDevCab.QueryPrincipalAfterOpen;
begin
end;

procedure TFmOVmIspDevCab.ReopenMedNPeca();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrMedNPeca, Dmod.MyDB, [
  'SELECT ',
  'CASE  ',
  '  WHEN MedidFei=MedidCer THEN 0 ',
  '  WHEN MedidFei<MedidCer THEN Pow((MedidCer - MedidFei) / (MedidCer-MedidMin) * 4, ' + FFatorPot + ') ',
  '  WHEN MedidFei>MedidCer THEN Pow((MedidFei - MedidCer) / (MedidMax-MedidCer) * 4, ' + FFatorPot + ') ',
  'END PtsDesvio, ',
  '( ',
  '  CASE  ',
  '    WHEN MedidFei=MedidCer THEN 0 ',
  '    WHEN MedidFei<MedidCer THEN Pow((MedidCer - MedidFei) / (MedidCer-MedidMin) * 4, ' + FFatorPot + ') ',
  '    WHEN MedidFei>MedidCer THEN Pow((MedidFei - MedidCer) / (MedidMax-MedidCer) * 4, ' + FFatorPot + ') ',
  '  END) / POW(4, ' + FFatorPot + ') * 100 PercDesvio, ',
  ' ',
  'ELT(ygt.TolerBasCalc+1, "Medida", "Percentual") NO_TolerBasCalc,',
  'ELT(ygt.TolerUnMdida+1, "cm", "inch") NO_TolerUnMdida, ',
  'ygt.Nome NO_TOBIKO, ygt.TolerBasCalc, ygt.TolerUnMdida,',
  'ygt.TolerRndPerc, IF(idm.MedidOky=0, "N�o", "Sim") NO_MedidOky, ',
  'idm.*',
  'FROM ovmispdevmed idm',
  //'LEFT JOIN ovcynsgratop ygt ON ygt.Codigo=idm.OVcYnsMedTop',
  'LEFT JOIN ovcynsgratop ygt ON ygt.Codigo=idm.Tobiko',
  'WHERE idm.Codigo=' + Geral.FF0(QrOVmIspDevCabCodigo.Value),
  'AND idm.PecaSeq=' + Geral.FF0(QrMedPecasPecaSeq.Value),
  //'ORDER BY idm.CtrlInMob ',
  'ORDER BY ygt.Nome',
  EmptyStr]);
  //
  //Geral.MB_SQL(self, QrMedNPeca);
end;

procedure TFmOVmIspDevCab.ReopenMedPecas();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrMedPecas, Dmod.MyDB, [
  'SELECT PecaSeq, COUNT(PecaSeq) Itens, ',
  'SUM(IF(MedidFei=MedidCer, 1, 0)) Exatas, ',
  'SUM(IF(MedidFei BETWEEN MedidMin AND MedidMax, 1, 0)) - ',
  'SUM(IF(MedidFei=MedidCer, 1, 0)) Dentro, ',
  'SUM(IF(MedidFei BETWEEN MedidMin AND MedidMax, 0, 1)) Fora, ',
  'SUM( ',
  '  CASE  ',
  '    WHEN MedidFei=MedidCer THEN 0 ',
  '    WHEN MedidFei<MedidCer THEN POW((MedidCer - MedidFei) / (MedidCer-MedidMin) * 4, ' + FFatorPot + ') ',
  '    WHEN MedidFei>MedidCer THEN POW((MedidFei - MedidCer) / (MedidMax-MedidCer) * 4, ' + FFatorPot + ') ',
  '  END) ',
  'PtsDesvioMed, COUNT(PecaSeq) * POW(4, ' + FFatorPot + ') MaxPtsDesvio, ',
  'SUM( ',
  '  CASE  ',
  '    WHEN MedidFei=MedidCer THEN 0 ',
  '    WHEN MedidFei<MedidCer THEN POW((MedidCer - MedidFei) / (MedidCer-MedidMin) * 4, ' + FFatorPot + ') ',
  '    WHEN MedidFei>MedidCer THEN POW((MedidFei - MedidCer) / (MedidMax-MedidCer) * 4, ' + FFatorPot + ') ',
  '  END) ',
  '/ (COUNT(PecaSeq) * POW(4, ' + FFatorPot + ')) * 100 PercDesvio ',
  ' ',
  'FROM ovmispdevmed ',
  'WHERE Codigo=' + Geral.FF0(QrOVmIspDevCabCodigo.Value),
  'GROUP BY PecaSeq ',
  'ORDER BY PecaSeq ',
  EmptyStr]);
end;

procedure TFmOVmIspDevCab.ReopenOVmIspDevFts();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrOVmIspDevFts, Dmod.MyDB, [
  'SELECT * ',
  'FROM ovmispdevfts ',
  'WHERE Codigo=' + Geral.FF0(QrOVmIspDevCabCodigo.Value),
  'AND CtrlInMob=' + Geral.FF0(QrPontosNegCtrlInMob.Value),
  'AND IdTabela="' + QrPontosNegIdTabela.Value + '"',
  EmptyStr]);
end;

procedure TFmOVmIspDevCab.ReopenPontosNeg();
var
  sCodigo: String;
begin
  sCodigo := Geral.FF0(QrOVmIspDevCabCodigo.Value);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrPontosNeg, Dmod.MyDB, [
  'SELECT "0" IdTabela, "' + CO_MEM_FLD_TAB_0_Faccao_Inconformidade + '" Tabela,  ',
  'mmc.PecaSeq,  yqt.Nome NO_Topico, mmc.PontNeg,',
  'mmc.QtdFotos, mmc.CtrlInMob ',
  'FROM ovmispdevinc mmc  ',
  'LEFT JOIN ovcynsqsttop yqt ON yqt.Codigo=mmc.Topico  ',
  'WHERE mmc.Codigo=' + sCodigo,
  '  ',
  'UNION  ',
  '  ',
  'SELECT  "1" IdTabela, "' + CO_MEM_FLD_TAB_1_Faccao_Medida + '" Tabela,  ',
  'mmm.PecaSeq, ygt.Nome NO_Topico, mmm.PontNeg, ',
  'mmm.QtdFotos, mmm.CtrlInMob ',
  'FROM ovmispdevmed mmm  ',
  'LEFT JOIN ovcynsmeddim ymd ON ymd.Conta=mmm.ovcynsmeddim  ',
  'LEFT JOIN ovcynsmedtop ymt ON ymt.Controle=ymd.Controle  ',
  'LEFT JOIN ovcynsgratop ygt ON ygt.Codigo=ymt.Tobiko  ',
  'WHERE mmm.MedidOky=0  ',
  'AND mmm.Codigo=' + sCodigo,
  '   ',
  'UNION   ',
  '   ',
  'SELECT  "2" IdTabela, "' + CO_MEM_FLD_TAB_2_Faccao_LivreTexto + '" Tabela,   ',
  'mml.PecaSeq, mml.Descricao NO_Topico, mml.PontNeg, ',
  'mml.QtdFotos, mml.CtrlInMob ',
  'FROM ovmispdevlvr mml   ',
  'WHERE mml.Codigo=' + sCodigo,
  '   ',
  'ORDER BY PecaSeq, NO_Topico ',
  EmptyStr]);
  //
  //Geral.MB_SQL(Self, QrPontosNeg);
end;

procedure TFmOVmIspDevCab.DBGMedPecasDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
const
  //MaxL = 40; (* de 100*)
  MaxS = 100 (* de 100*);
var
  Cor: TColor;
  H(*360*), S(*100*), L(*100*), R, G, B, Perc: Double;
begin
  S := MaxS;
  // Fazer o limite como laranja  (Matiz (H) = 25 de 360 = );
  // Ap�s o limite aumentar a luminosidade para ficar mais vivo (at� o meio = 100% de vivacidade)
  // H = FBestHue >> Azul puro. Depois disso vai ao lilaz e termina o ciclo no 0=Vermelho
  // ent�o o range vai do Vermelho=0, laranja=30, amarelo=60 ... ... ... Azul=240
  // 22,5 = FLaranja >> Laranja fracamente vermelho no limite da medi��o
  H := FBestHue - (QrMedPecasPercDesvio.Value * (FBestHue - FLaranja) / 100);
  if H < 0 then
    H := 0;
  // L = 50 = FBestLum > Luminosidade 100% vivo (acima de 50 vai ao branco abaixo de 50 vai ao preto. (100=Branco, 0=Preto)
  L := FBestLum - ((H / FBestHue) * (FBestLum / 2)); // Vermelho = 100% vivacidade >> Azul 50% (mais escuro!)
  //Memo1.Text := 'H=' + IntToStr(Trunc(H)) + ' S=' + IntToStr(Trunc(S)) + ' L=' + IntToStr(Trunc(L)) + sLineBreak + Memo1.Text;
  DmkColor.HSLtoRGB2(H, S, L, R, G, B);
  Cor := RGB(Trunc(R), Trunc(G), Trunc(B));
  MyObjects.DesenhaTextoEmDBGrid(TDbGrid(DBGMedPecas), Rect, Cor, clWindow,
    Column.Alignment, Column.Field.DisplayText);
{
var
  Cor: TColor;
  H(*360*), S(*100*), L(*100*), R, G, B: Double;
begin
  S := 100 (* de 100*);
  L := 40  (* de 100*);
  if QrMedPecasPercDesvio.Value > 100 then
    H := 0
  else
    H := 240 - (QrMedPecasPercDesvio.Value * 240 / 100);

  DmkColor.HSLtoRGB2(H, S, L, R, G, B);
  Cor := RGB(Trunc(R), Trunc(G), Trunc(B));
  MyObjects.DesenhaTextoEmDBGrid(TDbGrid(DBGMedPecas), Rect, Cor, clWindow,
    Column.Alignment, Column.Field.DisplayText);
}
end;

procedure TFmOVmIspDevCab.DBGMedNPecaDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
const
  //MaxL = 40; (* de 100*)
  MaxS = 100 (* de 100*);
var
  Cor: TColor;
  H(*360*), S(*100*), L(*100*), R, G, B, Perc: Double;
begin
  S := MaxS;
  // Fazer o limite como laranja  (Matiz (H) = 25 de 360 = );
  // Ap�s o limite aumentar a luminosidade para ficar mais vivo (at� o meio = 100% de vivacidade)
  // H = 240 >> Azul puro. Depois disso vai ao lilaz e termina o ciclo no 0=Vermelho
  // ent�o o range vai do Vermelho=0, laranja=30, amarelo=60 ... ... ... Azul=240
  // FLaranja > 22,5  >> Laranja fracamente vermelha no limite da medi��o
  H := FBestHue - (QrMedNPecaPercDesvio.Value * (FBestHue - FLaranja) / 100);  //
  if H < 0 then
    H := 0;
  // L = 50 = FBestLum > Luminosidade 100% vivo (acima de 50 vai ao branco abaixo de 50 vai ao preto. (100=Branco, 0=Preto)
  L := FBestLum - ((H / FBestHue) * (FBestLum / 2)); // Vermelho = 100% vivacidade >> Azul 50% (mais escuro!)
  //Memo1.Text := 'H=' + IntToStr(Trunc(H)) + ' S=' + IntToStr(Trunc(S)) + ' L=' + IntToStr(Trunc(L)) + sLineBreak + Memo1.Text;
  DmkColor.HSLtoRGB2(H, S, L, R, G, B);
  Cor := RGB(Trunc(R), Trunc(G), Trunc(B));
  MyObjects.DesenhaTextoEmDBGrid(TDbGrid(DBGMedNPeca), Rect, Cor, clWindow,
    Column.Alignment, Column.Field.DisplayText);
end;

procedure TFmOVmIspDevCab.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmOVmIspDevCab.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmOVmIspDevCab.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmOVmIspDevCab.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmOVmIspDevCab.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmOVmIspDevCab.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmOVmIspDevCab.Button1Click(Sender: TObject);

{
function Base64FromBitmap(Bitmap: TBitmap): string;
var
  Stream: TBytesStream;
  Encoding: TBase64Encoding;
begin
  Stream := TBytesStream.Create;
  try
    Bitmap.SaveToStream(Stream);
    Encoding := TBase64Encoding.Create(0);
    try
      Result := Encoding.EncodeBytesToString(Copy(Stream.Bytes, 0, Stream.Size));
    finally
      Encoding.Free;
    end;
  finally
    Stream.Free;
  end;
end;
}
{
  procedure BitmapFromBase64(Base64: string; Bitmap: TBitmap);
  var
    Input: TStringStream;
    Output: TBytesStream;
    Encoding: TBase64Encoding;
  begin
    Input := TStringStream.Create(Base64, TEncoding.ASCII);
    try
      Output := TBytesStream.Create;
      try
        Encoding := TBase64Encoding.Create(0);
        try
          Encoding.Decode(Input, Output);
          Output.Position := 0;
          Bitmap.LoadFromStream(Output);
        finally
          Encoding.Free;
        end;
      finally
        Output.Free;
      end;
    finally
      Input.Free;
    end;
  end;
}

function Base64FromBitmap(Bitmap: TBitmap): string;
var
  Input: TBytesStream;
  Output: TStringStream;
begin
  Input := TBytesStream.Create;
  try
    Bitmap.SaveToStream(Input);
    Input.Position := 0;
    Output := TStringStream.Create('', TEncoding.ASCII);
    try
      Soap.EncdDecd.EncodeStream(Input, Output);
      Result := Output.DataString;
    finally
      Output.Free;
    end;
  finally
    Input.Free;
  end;
end;

(*
  function Base64FromBitmap(Bitmap: TBitmap): string;
  var
    Input: TBytesStream;
    Output: TStringStream;
    Encoding: TBase64Encoding;
  begin
    Input := TBytesStream.Create;
    try
      Bitmap.SaveToStream(Input);
      Input.Position := 0;
      Output := TStringStream.Create('', TEncoding.ASCII);
      try
        Encoding := TBase64Encoding.Create(0);
        try
          Encoding.Encode(Input, Output);
          Result := Output.DataString;
        finally
          Encoding.Free;
        end;
      finally
        Output.Free;
      end;
    finally
      Input.Free;
    end;
  end;
*)

{
  procedure DuplicaImagem();
  var
    Bitmap: TBitmap;
    sBase64, Str1, str2: String;
  begin
    Bitmap := TBitmap.Create;
    try
      //BitMap := Image1.Picture.Bitmap;


    Bitmap.Width  := Image1.Width;
    Bitmap.Height := Image1.Height;
    Bitmap.PixelFormat := pf32bit;
    Bitmap.Assign(Image1.Picture.Bitmap);
    Bitmap.PixelFormat := TPixelFormat.pf32bit;


      sBase64 := Base64FromBitmap(Bitmap);
      ShowMessage(IntToStr(Length(sBase64)));
  //    ShowMessage(sBase64);
      Str1 := Copy(sBase64, 1, 1000);
      Str2 := Copy(sBase64, 1001);
     // BitmapFromBase64(str1 + str2, Image1.Picture.Bitmap);
     Image2.Picture.Bitmap := BitmapFromBase64(str1 + str2);
    finally
      //Bitmap.Free;
    end;
  end;
}
begin
  //DuplicaImagem();
end;

(*
procedure TFmOVmIspDevCab.BitmapFromBase64(Base64: string; Bitmap: TBitmap);
{
var
  Input: TStringStream;
  Output: TBytesStream;
  Encoding: TBase64Encoding;
begin
  //Input := TStringStream.Create(Base64, TEncoding.ASCII);
  Input := TStringStream.Create(Base64, TEncoding.UTF8);
  try
    Output := TBytesStream.Create;
    try
      Encoding := TBase64Encoding.Create(0);
      try
        Encoding.Decode(Input, Output);
        Output.Position := 0;
        Bitmap.LoadFromStream(Output);
      finally
        Encoding.Free;
      end;
    finally
      Output.Free;
    end;
  finally
    Input.Free;
  end;
}
var
  Stream: TBytesStream;
  Bytes: TBytes;
  Encoding: TBase64Encoding;
begin
  Stream := TBytesStream.Create;
  try
    Encoding := TBase64Encoding.Create(0);
    try
      Bytes := Encoding.DecodeStringToBytes(Base64);
      Stream.WriteData(Bytes, Length(Bytes));
      Stream.Position := 0;
      Bitmap.LoadFromStream(Stream);
    finally
      Encoding.Free;
    end;
  finally
    Stream.Free;
  end;
end;
*)

procedure TFmOVmIspDevCab.BtAlteraClick(Sender: TObject);
begin
  if (QrOVmIspDevCab.State <> dsInactive) and (QrOVmIspDevCab.RecordCount > 0) then
  begin
    UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrOVmIspDevCab, [PnDados],
      [PnEdita], EdNome, ImgTipo, 'ovmIspdevcab');
  end;
end;

procedure TFmOVmIspDevCab.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrOVmIspDevCabCodigo.Value;
  Close;
end;

procedure TFmOVmIspDevCab.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := EdCodigo.ValueVariant;
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  //
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'ovmIspdevcab', 'Codigo');
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmOVmIspDevCab.BtEmailClick(Sender: TObject);
(*
var
  Inspecao: Integer;
  Retorno: String;
*)
begin
(*
  Inspecao := QrOVmIspDevCabCodigo.Value;
  REST_App.EnviaEmailResultadoInspecao(Inspecao,
  'marco@dermatek.com.br;arendemilly@gmail.com', TTipoEnvioEmailInspecao.smerAll,
  Retorno);
  //'marco@dermatek.com.br;arendemilly@gmail.com');
  //'marco@dermatek.com.br');
  //'');
*)
end;

procedure TFmOVmIspDevCab.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  GBEdita.Align := alClient;
  PGDados.Align := alClient;
  PGDados.ActivePageIndex := 0;
  CriaOForm;
end;

procedure TFmOVmIspDevCab.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrOVmIspDevCabCodigo.Value, LaRegistro.Caption);
end;

procedure TFmOVmIspDevCab.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmOVmIspDevCab.SbNovoClick(Sender: TObject);
var
  Codigo: Integer;
begin
  //LaRegistro.Caption := GOTOy.CodUsu(QrOVmIspDevCabCodigo.Value, LaRegistro.Caption);
  Codigo := 0;
  if DBCheck.CriaFm(TFmOVmIspDevPsq, FmOVmIspDevPsq, afmoNegarComAviso) then
  begin
    FmOVmIspDevPsq.ShowModal;
    if FmOVmIspDevPsq.FOCmIspDevCab <> 0 then
      Codigo := FmOVmIspDevPsq.FOCmIspDevCab;
    FmOVmIspDevPsq.Destroy;
    //
    if Codigo <> 0 then
      LocCod(Codigo, Codigo);
  end;
end;

procedure TFmOVmIspDevCab.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmOVmIspDevCab.QrMedPecasAfterScroll(DataSet: TDataSet);
begin
  ReopenMedNPeca();
end;

procedure TFmOVmIspDevCab.QrMedPecasBeforeClose(DataSet: TDataSet);
begin
  QrMedNPeca.Close;
end;

procedure TFmOVmIspDevCab.QrOVmIspDevCabAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmOVmIspDevCab.QrOVmIspDevCabAfterScroll(DataSet: TDataSet);
begin
  ReopenPontosNeg();
  ReopenMedPecas();
end;

procedure TFmOVmIspDevCab.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmOVmIspDevCab.SbQueryClick(Sender: TObject);
begin
  LocCod(QrOVmIspDevCabCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'ovmIspdevcab', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmOVmIspDevCab.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmOVmIspDevCab.QrOVmIspDevCabBeforeClose(DataSet: TDataSet);
begin
  QrPontosNeg.Close;
  QrMedPecas.Close;
end;

procedure TFmOVmIspDevCab.QrOVmIspDevCabBeforeOpen(DataSet: TDataSet);
begin
  QrOVmIspDevCabCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmOVmIspDevCab.QrOVmIspDevFtsAfterScroll(DataSet: TDataSet);
(*
function Decode(const Input: AnsiString): string;
var
  bytes: TBytes;
  utf8: UTF8String;
begin
  bytes := EncdDecd.DecodeBase64(Input);
  SetLength(utf8, Length(bytes));
  Move(Pointer(bytes)^, Pointer(utf8)^, Length(bytes));
  Result := string(utf8);
end;

function Base64ToBitmap(const S: string): TBitmap;
var
SS: TStringStream;
V: string;
begin
V := Decode(S);
SS := TStringStream.Create(V);
try
Result := TBitmap.Create;
Result.LoadFromStream(SS);
finally
SS.Free;
end;
end;
*)

var
  NomeArq, NoArqSvr, Base64: AnsiString;
  //Bitmap: TBitmap;
  FMX_Bitmap: FMX.Graphics.TBitmap;
  VCL_Bitmap: VCL.Graphics.TBitmap;
  Codigo, IdTabela, CtrlInMob: Integer;
  DataHora: String;
  CriarArqLocal: Boolean;
begin
//EXIT;
  CriarArqLocal := False;
  NomeArq := CO_DIR_FOTOS_INSPECOES + QrOVmIspDevFtsNoArqSvr.Value;
  if QrOVmIspDevFtsNoArqSvr.Value <> EmptyStr then
  begin
    if FileExists(NomeArq) then
      ImgFoto.Picture.Bitmap.LoadFromFile(NomeArq)
    else
      CriarArqLocal := True;
  end else
    CriarArqLocal := True;
  if CriarArqLocal then
  begin
    if not DirectoryExists(CO_DIR_FOTOS_INSPECOES) then
      ForceDirectories(CO_DIR_FOTOS_INSPECOES);
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrOVmIspDevBmp, Dmod.MyDB, [
    'SELECT TxtBmp ',
    'FROM ovmispdevbmp ',
    'WHERE Codigo=' + Geral.FF0(QrOVmIspDevFtsCodigo.Value),
    'AND IdTabela=' + Geral.FF0(QrOVmIspDevFtsIdTabela.Value),
    'AND CtrlInMob=' + Geral.FF0(QrOVmIspDevFtsCtrlInMob.Value),
    'AND DataHora="' + Geral.FDT(QrOVmIspDevFtsDataHora.Value, 109) + '"',
    'ORDER BY Pedaco',
    EmptyStr]);
    //
    Base64 := '';
    QrOVmIspDevBmp.First;
    while not QrOVmIspDevBmp.Eof do
    begin
      Base64 := Base64 + QrOVmIspDevBmpTxtBmp.Value;
      //
      QrOVmIspDevBmp.Next;
    end;
    if Base64 <> EmptyStr then
    begin
      FMX_BitMap := FMX.Graphics.TBitmap.Create;
      VCL_BitMap := VCL.Graphics.TBitmap.Create;
      //
      FMX_Bitmap := BitmapFromBase64(Base64);
      //Converter aqui de FMX para VCL!
      VCL_BitMap := ConvertFmxBitmapToVclBitmap(FMX_Bitmap);
      ImgFoto.Picture.Bitmap.Assign(VCL_Bitmap);
      Codigo    := QrOVmIspDevFtsCodigo.Value;
      IdTabela  := QrOVmIspDevFtsIDTabela.Value;
      CtrlInMob := QrOVmIspDevFtsCtrlInMob.Value;
      DataHora  := Geral.FDT(QrOVmIspDevFtsDataHora.Value, 109);
      NoArqSvr  := OVS_PF.DefineNomeArquivoFotoInconformidade(Codigo, CtrlInMob,
        IDTabela, QrOVmIspDevFtsDataHora.Value);
      NomeArq := CO_DIR_FOTOS_INSPECOES + NoArqSvr;
      ImgFoto.Picture.Bitmap.SaveToFile(NomeArq);
      if FileExists(NomeArq) and (QrOVmIspDevFtsNoArqSvr.Value = EmptyStr) then
      begin
        if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'ovmispdevfts', False, [
        'NoArqSvr'], [
        'Codigo', 'IdTabela', 'CtrlInMob', 'DataHora'], [
        NoArqSvr], [
        Codigo, IdTabela, CtrlInMob, DataHora], True) then
        begin
          // ???? Excluir dados da tabela de ovmispdevbmp?????
        end;
      end;
    end;
  end;
end;

procedure TFmOVmIspDevCab.QrPontosNegAfterScroll(DataSet: TDataSet);
begin
  if ImgFoto.Picture <> nil then
    ImgFoto.Picture := nil;
  ReopenOVmIspDevFts();
end;
procedure TFmOVmIspDevCab.QrPontosNegBeforeClose(DataSet: TDataSet);
begin
  if ImgFoto.Picture <> nil then
    ImgFoto.Picture := nil;
  QrOVmIspDevFts.Close;
end;

{
/  Blob Field
https://www.devmedia.com.br/armazenando-imagens-no-mysql/32104
//
/ FMX.Bitmap to Vcl.Bitmap
https://stackoverflow.com/questions/36823042/how-can-convert-fmx-graphics-tbitmap-to-vcl-graphics-tbitmap-or-vcl-imaging-pngi
function
    ConvertFmxBitmapToVclBitmap(b:FMX.Graphics.TBitmap):Vcl.Graphics.TBitmap;
var
  data:FMX.Graphics.TBitmapData;
  i,j:Integer;
  AlphaColor:TAlphaColor;
begin
  Result:=VCL.Graphics.TBitmap.Create;
  Result.SetSize(b.Width,b.Height);
  if(b.Map(TMapAccess.Readwrite,data))then
  try
    for i := 0 to data.Height-1 do begin
      for j := 0 to data.Width-1 do begin
        AlphaColor:=data.GetPixel(j,i);
        Result.Canvas.Pixels[j,i]:=
          RGB(
            TAlphaColorRec(AlphaColor).R,
            TAlphaColorRec(AlphaColor).G,
            TAlphaColorRec(AlphaColor).B);
      end;
    end;
  finally
    b.Unmap(data);
  end;
end;
}
end.

