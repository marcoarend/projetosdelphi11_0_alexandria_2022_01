unit OVgItxPrfCad;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkPermissoes,
  dmkDBLookupComboBox, dmkEditCB, mySQLDbTables, dmkRadioGroup, dmkCheckBox;

type
  TFmOVgItxPrfCad = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    GroupBox2: TGroupBox;
    GroupBox3: TGroupBox;
    Panel5: TPanel;
    QrOVcYnsExgCad: TMySQLQuery;
    QrOVcYnsExgCadCodigo: TIntegerField;
    QrOVcYnsExgCadNome: TWideStringField;
    DsOVcYnsExgCad: TDataSource;
    Label7: TLabel;
    EdOVcYnsExg: TdmkEditCB;
    CBOVcYnsExg: TdmkDBLookupComboBox;
    SbOVcYnsExg: TSpeedButton;
    Label13: TLabel;
    EdCodigo: TdmkEdit;
    PnDadosOri: TPanel;
    Label4: TLabel;
    Label5: TLabel;
    EdSeqGrupo: TdmkEdit;
    EdNO_SeqGrupo: TdmkEdit;
    Label1: TLabel;
    EdReferencia: TdmkEdit;
    CkAtivo: TdmkCheckBox;
    Label15: TLabel;
    EdNome: TdmkEdit;
    RGPermiFinHow: TdmkRadioGroup;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure SbOVcYnsExgClick(Sender: TObject);
    procedure EdNomeKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure EdOVcYnsExgKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure CBOVcYnsExgKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
    FCodigo, FSeqGrupo: Integer;
    //
    procedure ReopenOVcYnsExgCad(ArtigRef: Integer; Filtra: Boolean);
  end;

  var
  FmOVgItxPrfCad: TFmOVgItxPrfCad;

implementation

uses UnMyObjects, UnOVS_Jan, Module, DmkDAC_PF, UMySQLModule, ModuleGeral,
  UnOVS_Consts, UnOVS_PF;

{$R *.DFM}

procedure TFmOVgItxPrfCad.BtOKClick(Sender: TObject);
{
var
  Nome: String;
  Codigo, SeqGrupo, OVcYnsExg: Integer;
  SQLType: TSQLType;
begin
  SQLType := ImgTipo.SQLType?;
  Codigo         := ;
  Nome           := ;
  SeqGrupo       := ;
  OVcYnsExg      := ;

  //
? := UMyMod.BuscaEmLivreY_Def('ovgitxprfcab', 'Codigo', SQLType, CodAtual?);
ou > ? := UMyMod.BPGS1I32('ovgitxprfcab', 'Codigo', '', '', tsPosNeg?, stInsUpd?, CodAtual?);
if UMyMod.SQLInsUpd_IGNORE?(Dmod.QrUpd?, SQLType, 'ovgitxprfcab', auto_increment?[
'Nome', 'SeqGrupo', 'OVcYnsExg'], [
'Codigo'], [
Nome, SeqGrupo, OVcYnsExg], [
Codigo], UserDataAlterweb?, IGNORE?
}
var
  Nome: String;
  Codigo, SeqGrupo, OVcYnsExg,
  //OVcYnsChk, LimiteChk, LimiteMed, OVcYnsARQ,
  PermiFinHow, Ativo: Integer;
  SQLType: TSQLType;
  Agora: TDateTime;
begin
  SQLType        := ImgTipo.SQLType;
  Agora          := DModG.ObtemAgora();
  Codigo         := EdCodigo.ValueVariant;
  SeqGrupo       := EdSeqGrupo.ValueVariant;
  OVcYnsExg      := EdOVcYnsExg.ValueVariant;
(*
  OVcYnsChk      := EdOVcYnsChk.ValueVariant;
  LimiteChk      := EdLimiteChk.ValueVariant;
  LimiteMed      := EdLimiteMed.ValueVariant;
  OVcYnsARQ      := EdOVcYnsARQ.ValueVariant;
*)
  PermiFinHow    := RGPermiFinHow.ItemIndex;
  Ativo          := Geral.BoolToInt(CkAtivo.Checked);
  Nome           := EdNome.ValueVariant;
  //
  if MyObjects.FIC(Trim(Nome) = EmptyStr, EdNome,
    '"Informe uma descri��o para o perfil"') then Exit;
  //
  if MyObjects.FIC(OVcYnsExg = 0, EdOVcYnsExg,
    '"Informe a tabela de exa��es"') then Exit;
  //
(*
  if MyObjects.FIC(OVcYnsChk = 0, EdOVcYnsChk,
    '"Informe o check list de inconformidades"') then Exit;
  //
  if MyObjects.FIC(OVcYnsARQ = 0, EdOVcYnsARQ,
    '"Plano de Amostragem e Regime de Qualidade"') then Exit;
  //
*)
  if MyObjects.FIC(PermiFinHow < 1, RGPermiFinHow,
    'Informe a permiss�o de finaliza��o da inspe��o"') then Exit;
  //
  Codigo := UMyMod.BPGS1I32('ovgitxprfcab', 'Codigo', '', '', tsPos, SQLType, Codigo);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'ovgitxprfcab', False, [
  'Nome', 'SeqGrupo',
  'OVcYnsExg', (*'OVcYnsChk', 'LimiteChk',
  'LimiteMed', 'OVcYnsARQ',*) 'PermiFinHow',
  'Ativo'], [
  'Codigo'], [
  Nome, SeqGrupo,
  OVcYnsExg, (*OVcYnsChk, LimiteChk,
  LimiteMed, OVcYnsARQ,*) PermiFinHow,
  Ativo], [
  Codigo], True) then
  begin
    FCodigo := Codigo;
    Close;
  end;
end;

procedure TFmOVgItxPrfCad.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmOVgItxPrfCad.CBOVcYnsExgKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  case Key of
    VK_F3: ReopenOVcYnsExgCad(FSeqGrupo, True);
    VK_F4: ReopenOVcYnsExgCad(FSeqGrupo, False);
  end;
end;

procedure TFmOVgItxPrfCad.EdNomeKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F3 then
    EdNome.Text := EdReferencia.Text;
  if Key = VK_F4 then
    EdNome.Text := EdNO_SeqGrupo.Text;
end;

procedure TFmOVgItxPrfCad.EdOVcYnsExgKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  case Key of
    VK_F3: ReopenOVcYnsExgCad(FSeqGrupo, True);
    VK_F4: ReopenOVcYnsExgCad(FSeqGrupo, False);
  end;
end;

procedure TFmOVgItxPrfCad.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  if EdNome.Text = EMptyStr then
  try
    EdNome.SetFocus;
  except
    // nada
  end;
end;

procedure TFmOVgItxPrfCad.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  FCodigo   := 0;
  FSeqGrupo := 0;
end;

procedure TFmOVgItxPrfCad.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmOVgItxPrfCad.ReopenOVcYnsExgCad(ArtigRef: Integer; Filtra: Boolean);
var
  SQL_WHERE: String;
begin
  if Filtra then
    SQL_WHERE := 'WHERE ArtigRef=' + Geral.FF0(ArtigRef)
  else
    SQL_WHERE := '';
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrOVcYnsExgCad, Dmod.MyDB, [
  'SELECT Codigo, Nome',
  'FROM ovcynsexgcad',
  SQL_WHERE,
  'ORDER BY Nome',
  '']);
end;

procedure TFmOVgItxPrfCad.SbOVcYnsExgClick(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  OVS_Jan.MostraFormOVcYnsExgCad(FSeqGrupo);
  UMyMod.SetaCodigoPesquisado(EdOVcYnsExg, CBOVcYnsExg, QrOVcYnsExgCad,
    VAR_CADASTRO);
end;

end.
