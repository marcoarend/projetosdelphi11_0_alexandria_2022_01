object FmOVgItxGerCab: TFmOVgItxGerCab
  Left = 368
  Top = 194
  Caption = 'OVS-EXCAO-001 :: Configura'#231#227'o de Inspe'#231#245'es de T'#234'xteis'
  ClientHeight = 692
  ClientWidth = 1008
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnEdita: TPanel
    Left = 0
    Top = 96
    Width = 1008
    Height = 596
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    Visible = False
    ExplicitHeight = 543
    object GBEdita: TGroupBox
      Left = 0
      Top = 0
      Width = 1008
      Height = 125
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 0
      object Label7: TLabel
        Left = 16
        Top = 16
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label9: TLabel
        Left = 76
        Top = 16
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
        Color = clBtnFace
        ParentColor = False
      end
      object EdCodigo: TdmkEdit
        Left = 16
        Top = 32
        Width = 56
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Codigo'
        UpdCampo = 'Codigo'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdNome: TdmkEdit
        Left = 76
        Top = 32
        Width = 633
        Height = 21
        TabOrder = 1
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Nome'
        UpdCampo = 'Nome'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
    end
    object GBConfirma: TGroupBox
      Left = 0
      Top = 533
      Width = 1008
      Height = 63
      Align = alBottom
      TabOrder = 1
      ExplicitTop = 480
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 12
        Top = 17
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
      end
      object Panel1: TPanel
        Left = 868
        Top = 15
        Width = 138
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 7
          Top = 2
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
    end
  end
  object PnDados: TPanel
    Left = 0
    Top = 96
    Width = 1008
    Height = 596
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    ExplicitHeight = 543
    object Splitter1: TSplitter
      Left = 0
      Top = 297
      Width = 1008
      Height = 5
      Cursor = crVSplit
      Align = alTop
      ExplicitLeft = 16
      ExplicitTop = 281
    end
    object GBCntrl: TGroupBox
      Left = 0
      Top = 532
      Width = 1008
      Height = 64
      Align = alBottom
      TabOrder = 0
      ExplicitTop = 479
      object Panel5: TPanel
        Left = 2
        Top = 15
        Width = 172
        Height = 47
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 174
        Top = 15
        Width = 311
        Height = 47
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel3: TPanel
        Left = 485
        Top = 15
        Width = 521
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object Panel2: TPanel
          Left = 388
          Top = 0
          Width = 133
          Height = 47
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 0
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
        object BtCab: TBitBtn
          Tag = 1000574
          Left = 4
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Inspe'#231#227'o'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtCabClick
        end
        object BtBtl: TBitBtn
          Tag = 110
          Left = 128
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Bateladas'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = BtBtlClick
        end
      end
    end
    object Panel6: TPanel
      Left = 0
      Top = 0
      Width = 1008
      Height = 169
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 1
      object Panel8: TPanel
        Left = 0
        Top = 0
        Width = 1008
        Height = 78
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 0
        ExplicitLeft = 2
        ExplicitTop = 15
        ExplicitWidth = 1004
        object Label1: TLabel
          Left = 8
          Top = 0
          Width = 36
          Height = 13
          Caption = 'C'#243'digo:'
          FocusControl = DBEdCodigo
        end
        object Label2: TLabel
          Left = 68
          Top = 0
          Width = 51
          Height = 13
          Caption = 'Descri'#231#227'o:'
          FocusControl = DBEdNome
        end
        object Label17: TLabel
          Left = 768
          Top = 0
          Width = 43
          Height = 13
          Caption = 'Abertura:'
          FocusControl = DBEdCodigo
        end
        object Label18: TLabel
          Left = 884
          Top = 0
          Width = 69
          Height = 13
          Caption = 'Encerramento:'
          FocusControl = DBEdNome
        end
        object Label19: TLabel
          Left = 256
          Top = 40
          Width = 62
          Height = 13
          Caption = 'Status atual :'
        end
        object Label13: TLabel
          Left = 616
          Top = 40
          Width = 91
          Height = 13
          Caption = 'Status atual desde:'
          FocusControl = dmkDBEdit3
        end
        object Label15: TLabel
          Left = 732
          Top = 40
          Width = 107
          Height = 13
          Caption = 'Motivo do status atual:'
          FocusControl = dmkDBEdit4
        end
        object Label20: TLabel
          Left = 8
          Top = 40
          Width = 223
          Height = 13
          Caption = 'Plano de Amostragem e Regime de Qualidade :'
        end
        object DBEdCodigo: TdmkDBEdit
          Left = 8
          Top = 16
          Width = 56
          Height = 21
          TabStop = False
          DataField = 'Codigo'
          DataSource = DsOVgItxGerCab
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ReadOnly = True
          ShowHint = True
          TabOrder = 0
          UpdType = utYes
          Alignment = taRightJustify
        end
        object DBEdNome: TdmkDBEdit
          Left = 68
          Top = 16
          Width = 697
          Height = 21
          Color = clWhite
          DataField = 'Nome'
          DataSource = DsOVgItxGerCab
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          UpdType = utYes
          Alignment = taLeftJustify
        end
        object dmkDBEdit1: TdmkDBEdit
          Left = 768
          Top = 16
          Width = 112
          Height = 21
          Color = clWhite
          DataField = 'DtHrAbert_TXT'
          DataSource = DsOVgItxGerCab
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          UpdType = utYes
          Alignment = taLeftJustify
        end
        object dmkDBEdit2: TdmkDBEdit
          Left = 883
          Top = 16
          Width = 112
          Height = 21
          Color = clWhite
          DataField = 'DtHrFecha_TXT'
          DataSource = DsOVgItxGerCab
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          UpdType = utYes
          Alignment = taLeftJustify
        end
        object dmkDBEdit5: TdmkDBEdit
          Left = 256
          Top = 56
          Width = 49
          Height = 21
          Color = clWhite
          DataField = 'ZtatusIsp'
          DataSource = DsOVgItxGerCab
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 4
          UpdType = utYes
          Alignment = taLeftJustify
        end
        object dmkDBEdit6: TdmkDBEdit
          Left = 308
          Top = 56
          Width = 305
          Height = 21
          Color = clWhite
          DataField = 'NO_ZtatusIsp'
          DataSource = DsOVgItxGerCab
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 5
          UpdType = utYes
          Alignment = taLeftJustify
        end
        object dmkDBEdit3: TdmkDBEdit
          Left = 616
          Top = 56
          Width = 112
          Height = 21
          TabStop = False
          DataField = 'ZtatusDtH'
          DataSource = DsOVgItxGerCab
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ReadOnly = True
          ShowHint = True
          TabOrder = 6
          UpdType = utYes
          Alignment = taLeftJustify
        end
        object dmkDBEdit4: TdmkDBEdit
          Left = 732
          Top = 56
          Width = 261
          Height = 21
          Color = clWhite
          DataField = 'Nome'
          DataSource = DsOVgItxGerCab
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 7
          UpdType = utYes
          Alignment = taLeftJustify
        end
        object dmkDBEdit7: TdmkDBEdit
          Left = 8
          Top = 56
          Width = 49
          Height = 21
          Color = clWhite
          DataSource = DsOVgItxGerCab
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 8
          UpdType = utYes
          Alignment = taLeftJustify
        end
        object dmkDBEdit8: TdmkDBEdit
          Left = 60
          Top = 56
          Width = 193
          Height = 21
          Color = clWhite
          DataSource = DsOVgItxGerCab
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 9
          UpdType = utYes
          Alignment = taLeftJustify
        end
      end
      object PnDadosOri: TPanel
        Left = 0
        Top = 78
        Width = 1008
        Height = 39
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 1
        object Label3: TLabel
          Left = 8
          Top = 0
          Width = 29
          Height = 13
          Caption = 'Local:'
        end
        object Label4: TLabel
          Left = 68
          Top = 0
          Width = 91
          Height = 13
          Caption = 'Descri'#231#227'o do local:'
        end
        object Label5: TLabel
          Left = 844
          Top = 0
          Width = 18
          Height = 13
          Caption = 'OP:'
        end
        object Label6: TLabel
          Left = 416
          Top = 0
          Width = 30
          Height = 13
          Caption = 'Artigo:'
        end
        object Label8: TLabel
          Left = 476
          Top = 0
          Width = 95
          Height = 13
          Caption = 'Descri'#231#227'o do artigo:'
        end
        object Label10: TLabel
          Left = 920
          Top = 0
          Width = 52
          Height = 13
          Caption = 'Reduz.OP:'
        end
        object DBEdit1: TDBEdit
          Left = 8
          Top = 16
          Width = 56
          Height = 21
          DataField = 'Local'
          DataSource = DsOVgItxGerCab
          TabOrder = 0
        end
        object DBEdit2: TDBEdit
          Left = 68
          Top = 16
          Width = 345
          Height = 21
          DataField = 'NO_Local'
          DataSource = DsOVgItxGerCab
          TabOrder = 1
        end
        object DBEdit3: TDBEdit
          Left = 844
          Top = 16
          Width = 72
          Height = 21
          DataField = 'NrOP'
          DataSource = DsOVgItxGerCab
          TabOrder = 2
        end
        object DBEdit4: TDBEdit
          Left = 416
          Top = 16
          Width = 56
          Height = 21
          DataField = 'SeqGrupo'
          DataSource = DsOVgItxGerCab
          TabOrder = 3
        end
        object DBEdit5: TDBEdit
          Left = 476
          Top = 16
          Width = 365
          Height = 21
          DataField = 'NO_Referencia'
          DataSource = DsOVgItxGerCab
          TabOrder = 4
        end
        object DBEdit6: TDBEdit
          Left = 920
          Top = 16
          Width = 72
          Height = 21
          DataField = 'NrReduzidoOP'
          DataSource = DsOVgItxGerCab
          TabOrder = 5
        end
      end
      object Panel7: TPanel
        Left = 0
        Top = 117
        Width = 1008
        Height = 52
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 2
        TabStop = True
        ExplicitTop = 248
        ExplicitHeight = 105
        object DBRadioGroup1: TDBRadioGroup
          Left = 549
          Top = 0
          Width = 444
          Height = 52
          Align = alLeft
          Caption = ' Permiss'#227'o de finaliza'#231#227'o da inspe'#231#227'o: '
          Columns = 2
          DataField = 'PermiFinHow'
          DataSource = DsOVgItxGerCab
          Items.Strings = (
            '0 - Indefinido'
            '1 - Somente ao finalizar toda inspe'#231#227'o'
            '2 - Ao atingir a pontua'#231#227'o de reprova'#231#227'o'
            '3 - A qualquer momento')
          TabOrder = 0
          Values.Strings = (
            '0'
            '1'
            '2'
            '3'
            '4'
            '5'
            '6'
            '7'
            '8'
            '9')
          ExplicitLeft = 509
          ExplicitHeight = 47
        end
        object Panel9: TPanel
          Left = 0
          Top = 0
          Width = 549
          Height = 52
          Align = alLeft
          BevelOuter = bvNone
          TabOrder = 1
          ExplicitHeight = 47
          object Label11: TLabel
            Left = 8
            Top = 4
            Width = 94
            Height = 13
            Caption = 'Tabela de exa'#231#245'es:'
          end
          object DBEdit7: TDBEdit
            Left = 8
            Top = 20
            Width = 56
            Height = 21
            DataField = 'OVcYnsExg'
            DataSource = DsOVgItxGerCab
            TabOrder = 0
          end
          object DBEdit9: TDBEdit
            Left = 68
            Top = 20
            Width = 477
            Height = 21
            DataField = 'NO_OVcYnsExg'
            DataSource = DsOVgItxGerCab
            TabOrder = 1
          end
        end
      end
    end
    object PCItens: TPageControl
      Left = 0
      Top = 302
      Width = 1008
      Height = 200
      ActivePage = TabSheet1
      Align = alTop
      TabOrder = 2
      ExplicitLeft = 12
      ExplicitTop = 488
      object TabSheet1: TTabSheet
        Caption = ' Tabela de Exa'#231#245'es'
        object PnGrids: TPanel
          Left = 0
          Top = 0
          Width = 1000
          Height = 172
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          object DBGMixOpc: TDBGrid
            Left = 412
            Top = 117
            Width = 588
            Height = 55
            Align = alRight
            DataSource = DsOVcYnsMixOpc
            TabOrder = 0
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            Columns = <
              item
                Expanded = False
                FieldName = 'Controle'
                Width = 48
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Nome'
                Width = 338
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NO_Magnitude'
                Title.Caption = 'Magnitude'
                Width = 100
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'PontNeg'
                Title.Caption = 'Pontos neg.'
                Visible = True
              end>
          end
          object PnGrids2: TPanel
            Left = 0
            Top = 0
            Width = 1000
            Height = 117
            Align = alTop
            BevelOuter = bvNone
            TabOrder = 1
            object DBGExgTop: TDBGrid
              Left = 0
              Top = 0
              Width = 1000
              Height = 117
              Align = alClient
              DataSource = DsOVcYnsExgTop
              TabOrder = 0
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              Columns = <
                item
                  Expanded = False
                  FieldName = 'Controle'
                  Title.Caption = 'ID'
                  Width = 56
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Topyko'
                  Title.Caption = 'T'#243'pico'
                  Width = 56
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NO_TOPYKO'
                  Title.Caption = 'Descri'#231#227'o do T'#243'pico'
                  Width = 335
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NO_Mensuravl'
                  Title.Caption = 'Medir?'
                  Width = 37
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'MedidCer'
                  Title.Caption = 'Medida certa'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'MedidMin'
                  Title.Caption = 'Medida m'#237'n.'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'MedidMax'
                  Title.Caption = 'Medida m'#225'x.'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NO_TolerBasCalc'
                  Title.Caption = 'Bas.Calc.Toler.'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'TolerSglUnMdid'
                  Title.Caption = 'Unidade'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NO_TlrRndUsaMin'
                  Title.Caption = 'M'#237'nimo?'
                  Width = 46
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'TlrRndPrcMin'
                  Title.Caption = 'Marg m'#237'n. med/ %:'
                  Width = 71
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NO_TlrRndUsaMax'
                  Title.Caption = 'M'#225'ximo?'
                  Width = 48
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'TlrRndPrcMax'
                  Title.Caption = 'Marg m'#225'x. med/ %:'
                  Visible = True
                end>
            end
          end
        end
      end
    end
    object DBGrid1: TDBGrid
      Left = 0
      Top = 169
      Width = 1008
      Height = 128
      Align = alTop
      DataSource = DsOVgItxGerBtl
      TabOrder = 3
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'Controle'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'SeccaoOP'
          Title.Caption = 'OP tingimento'
          Width = 140
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'QtReal'
          Title.Caption = 'Qtde. real'
          Width = 72
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'SeccaoMaq'
          Title.Caption = 'Identifica'#231#227'o da m'#225'quina'
          Width = 140
          Visible = True
        end>
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 12
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 216
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 216
      Top = 0
      Width = 744
      Height = 52
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 464
        Height = 32
        Caption = 'Configura'#231#227'o de Inspe'#231#245'es de T'#234'xteis'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 464
        Height = 32
        Caption = 'Configura'#231#227'o de Inspe'#231#245'es de T'#234'xteis'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 464
        Height = 32
        Caption = 'Configura'#231#227'o de Inspe'#231#245'es de T'#234'xteis'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 52
    Width = 1008
    Height = 44
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = BtCab
    Left = 120
    Top = 64
  end
  object QrOVgItxGerCab: TMySQLQuery
    Database = Dmod.ZZDB
    BeforeOpen = QrOVgItxGerCabBeforeOpen
    AfterOpen = QrOVgItxGerCabAfterOpen
    BeforeClose = QrOVgItxGerCabBeforeClose
    AfterScroll = QrOVgItxGerCabAfterScroll
    SQL.Strings = (
      'SELECT igc.*, ymc.Nome NO_OVcYnsMed, ycc.Nome NO_OVcYnsChk,'
      'dlo.Nome NO_Local, ref.Nome NO_Referencia, '
      'isc.Nome NO_ZtatusIsp, '
      'IF(igc.DtHrAbert  <= "1899-12-30", "",  '
      '  DATE_FORMAT(igc.DtHrAbert, "%d/%m/%Y %h:%i")) DtHrAbert_TXT,  '
      'IF(igc.DtHrFecha  <= "1899-12-30", "",  '
      '  DATE_FORMAT(igc.DtHrFecha, "%d/%m/%Y %h:%i")) DtHrFecha_TXT'
      'FROM ovgispgercab igc'
      'LEFT JOIN ovdlocal dlo ON dlo.Codigo=igc.Local  '
      'LEFT JOIN ovdreferencia ref ON ref.Codigo=igc.SeqGrupo '
      'LEFT JOIN ovcynsmedcad ymc ON ymc.Codigo=igc.OVcYnsMed'
      'LEFT JOIN ovcynschkcad ycc ON ycc.Codigo=igc.OVcYnsChk'
      'LEFT JOIN OVgIspStaCad isc ON isc.Codigo=igc.ZtatusIsp'
      ''
      'WHERE igc.Codigo > 0')
    Left = 620
    Top = 5
    object QrOVgItxGerCabCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrOVgItxGerCabNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QrOVgItxGerCabLocal: TIntegerField
      FieldName = 'Local'
      Required = True
    end
    object QrOVgItxGerCabNrOP: TIntegerField
      FieldName = 'NrOP'
      Required = True
    end
    object QrOVgItxGerCabSeqGrupo: TIntegerField
      FieldName = 'SeqGrupo'
      Required = True
    end
    object QrOVgItxGerCabNrReduzidoOP: TIntegerField
      FieldName = 'NrReduzidoOP'
      Required = True
    end
    object QrOVgItxGerCabDtHrAbert: TDateTimeField
      FieldName = 'DtHrAbert'
      Required = True
    end
    object QrOVgItxGerCabDtHrFecha: TDateTimeField
      FieldName = 'DtHrFecha'
      Required = True
    end
    object QrOVgItxGerCabOVcYnsExg: TIntegerField
      FieldName = 'OVcYnsExg'
      Required = True
    end
    object QrOVgItxGerCabZtatusIsp: TIntegerField
      FieldName = 'ZtatusIsp'
      Required = True
    end
    object QrOVgItxGerCabZtatusDtH: TDateTimeField
      FieldName = 'ZtatusDtH'
      Required = True
    end
    object QrOVgItxGerCabZtatusMot: TIntegerField
      FieldName = 'ZtatusMot'
      Required = True
    end
    object QrOVgItxGerCabLk: TIntegerField
      FieldName = 'Lk'
      Required = True
    end
    object QrOVgItxGerCabDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrOVgItxGerCabDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrOVgItxGerCabUserCad: TIntegerField
      FieldName = 'UserCad'
      Required = True
    end
    object QrOVgItxGerCabUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Required = True
    end
    object QrOVgItxGerCabAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Required = True
    end
    object QrOVgItxGerCabAWServerID: TIntegerField
      FieldName = 'AWServerID'
      Required = True
    end
    object QrOVgItxGerCabAWStatSinc: TSmallintField
      FieldName = 'AWStatSinc'
      Required = True
    end
    object QrOVgItxGerCabAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
    object QrOVgItxGerCabNO_Local: TWideStringField
      FieldName = 'NO_Local'
      Size = 100
    end
    object QrOVgItxGerCabNO_Referencia: TWideStringField
      FieldName = 'NO_Referencia'
      Size = 100
    end
    object QrOVgItxGerCabNO_OVcYnsExg: TWideStringField
      FieldName = 'NO_OVcYnsExg'
      Size = 60
    end
    object QrOVgItxGerCabNO_ZtatusIsp: TWideStringField
      FieldName = 'NO_ZtatusIsp'
      Size = 60
    end
    object QrOVgItxGerCabDtHrAbert_TXT: TWideStringField
      DisplayWidth = 19
      FieldName = 'DtHrAbert_TXT'
      Size = 19
    end
    object QrOVgItxGerCabDtHrFecha_TXT: TWideStringField
      DisplayWidth = 19
      FieldName = 'DtHrFecha_TXT'
      Size = 19
    end
    object QrOVgItxGerCabPermiFinHow: TIntegerField
      FieldName = 'PermiFinHow'
    end
  end
  object DsOVgItxGerCab: TDataSource
    DataSet = QrOVgItxGerCab
    Left = 620
    Top = 53
  end
  object PMBtl: TPopupMenu
    OnPopup = PMBtlPopup
    Left = 672
    Top = 548
    object ItsInclui1: TMenuItem
      Caption = '&Adiciona'
      Enabled = False
      OnClick = ItsInclui1Click
    end
    object ItsAltera1: TMenuItem
      Caption = '&Edita'
      Enabled = False
      OnClick = ItsAltera1Click
    end
    object ItsExclui1: TMenuItem
      Caption = '&Remove'
      Enabled = False
      OnClick = ItsExclui1Click
    end
  end
  object PMCab: TPopupMenu
    OnPopup = PMCabPopup
    Left = 544
    Top = 544
    object CabInclui1: TMenuItem
      Caption = '&Inclui'
      Enabled = False
      Visible = False
      OnClick = CabInclui1Click
    end
    object CabAltera1: TMenuItem
      Caption = '&Altera'
      Enabled = False
      OnClick = CabAltera1Click
    end
    object CabExclui1: TMenuItem
      Caption = '&Exclui'
      Enabled = False
      Visible = False
      OnClick = CabExclui1Click
    end
    object LiberarParaInspecao1: TMenuItem
      Caption = 'Liberar para inspe'#231#227'o'
      OnClick = LiberarParaInspecao1Click
    end
  end
  object QrSumZtatusIsp: TMySQLQuery
    Database = Dmod.ZZDB
    SQL.Strings = (
      'SELECT igc.ZtatusIsp, ist.Nome NO_ZTATUS,  '
      'COUNT(igc.Codigo) ITENS '
      'FROM ovgispgercab igc '
      'LEFT JOIN ovgispstacad ist ON ist.Codigo=igc.ZtatusIsp '
      'WHERE igc.Codigo <> 0 '
      'GROUP BY igc.ZtatusIsp ')
    Left = 696
    Top = 393
    object QrSumZtatusIspZtatusIsp: TIntegerField
      DisplayLabel = 'Status'
      FieldName = 'ZtatusIsp'
      Required = True
    end
    object QrSumZtatusIspNO_ZTATUS: TWideStringField
      DisplayLabel = 'Descri'#231#227'o'
      FieldName = 'NO_ZTATUS'
      Size = 60
    end
    object QrSumZtatusIspITENS: TLargeintField
      DisplayLabel = 'Itens'
      FieldName = 'ITENS'
      Required = True
    end
  end
  object DsSumZtatusIsp: TDataSource
    DataSet = QrSumZtatusIsp
    Left = 696
    Top = 441
  end
  object QrOVcYnsExgTop: TMySQLQuery
    Database = Dmod.ZZDB
    BeforeClose = QrOVcYnsExgTopBeforeClose
    AfterScroll = QrOVcYnsExgTopAfterScroll
    SQL.Strings = (
      
        'SELECT IF(ymt.Mensuravl=0,"", ELT(ymt.TolerBasCalc+1, "Medida", ' +
        '"Percentual")) NO_TolerBasCalc, '
      'IF(ymt.Mensuravl=1, "SIM", "N'#195'O") NO_Mensuravl,'
      'IF(ymt.TlrRndUsaMin=1, "SIM", "N'#195'O") NO_TlrRndUsaMin,'
      'IF(ymt.TlrRndUsaMax=1, "SIM", "N'#195'O") NO_TlrRndUsaMax,'
      'ygt.Nome NO_TOPYKO, ymt.* '
      'FROM ovcynsexgtop ymt '
      'LEFT JOIN ovcynsmixtop ygt ON ygt.Codigo=ymt.Topyko '
      'WHERE ymt.Codigo>0')
    Left = 144
    Top = 305
    object QrOVcYnsExgTopNO_TolerBasCalc: TWideStringField
      FieldName = 'NO_TolerBasCalc'
      Size = 10
    end
    object QrOVcYnsExgTopNO_TOPYKO: TWideStringField
      FieldName = 'NO_TOPYKO'
      Size = 60
    end
    object QrOVcYnsExgTopNO_Mensuravl: TWideStringField
      FieldName = 'NO_Mensuravl'
      Required = True
      Size = 3
    end
    object QrOVcYnsExgTopNO_TlrRndUsaMin: TWideStringField
      FieldName = 'NO_TlrRndUsaMin'
      Required = True
      Size = 3
    end
    object QrOVcYnsExgTopNO_TlrRndUsaMax: TWideStringField
      FieldName = 'NO_TlrRndUsaMax'
      Required = True
      Size = 3
    end
    object QrOVcYnsExgTopCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrOVcYnsExgTopControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrOVcYnsExgTopTopyko: TIntegerField
      FieldName = 'Topyko'
      Required = True
    end
    object QrOVcYnsExgTopTolerBasCalc: TSmallintField
      FieldName = 'TolerBasCalc'
      Required = True
    end
    object QrOVcYnsExgTopTolerSglUnMdid: TWideStringField
      FieldName = 'TolerSglUnMdid'
      Size = 15
    end
    object QrOVcYnsExgTopTlrRndUsaMin: TSmallintField
      FieldName = 'TlrRndUsaMin'
      Required = True
    end
    object QrOVcYnsExgTopTlrRndPrcMin: TFloatField
      FieldName = 'TlrRndPrcMin'
      Required = True
      DisplayFormat = '#,###,##0.000;-#,###,##0.000; '
    end
    object QrOVcYnsExgTopTlrRndUsaMax: TSmallintField
      FieldName = 'TlrRndUsaMax'
      Required = True
    end
    object QrOVcYnsExgTopTlrRndPrcMax: TFloatField
      FieldName = 'TlrRndPrcMax'
      Required = True
      DisplayFormat = '#,###,##0.000;-#,###,##0.000; '
    end
    object QrOVcYnsExgTopLk: TIntegerField
      FieldName = 'Lk'
      Required = True
    end
    object QrOVcYnsExgTopDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrOVcYnsExgTopDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrOVcYnsExgTopUserCad: TIntegerField
      FieldName = 'UserCad'
      Required = True
    end
    object QrOVcYnsExgTopUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Required = True
    end
    object QrOVcYnsExgTopAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Required = True
    end
    object QrOVcYnsExgTopAWServerID: TIntegerField
      FieldName = 'AWServerID'
      Required = True
    end
    object QrOVcYnsExgTopAWStatSinc: TSmallintField
      FieldName = 'AWStatSinc'
      Required = True
    end
    object QrOVcYnsExgTopAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
    object QrOVcYnsExgTopMedidCer: TFloatField
      FieldName = 'MedidCer'
      Required = True
      DisplayFormat = '#,###,##0.000;-#,###,##0.000; '
    end
    object QrOVcYnsExgTopMedidMin: TFloatField
      FieldName = 'MedidMin'
      Required = True
      DisplayFormat = '#,###,##0.000;-#,###,##0.000; '
    end
    object QrOVcYnsExgTopMedidMax: TFloatField
      FieldName = 'MedidMax'
      Required = True
      DisplayFormat = '#,###,##0.000;-#,###,##0.000; '
    end
    object QrOVcYnsExgTopMensuravl: TSmallintField
      FieldName = 'Mensuravl'
    end
  end
  object DsOVcYnsExgTop: TDataSource
    DataSet = QrOVcYnsExgTop
    Left = 144
    Top = 353
  end
  object QrOVcYnsMixOpc: TMySQLQuery
    Database = Dmod.ZZDB
    SQL.Strings = (
      'SELECT mag.Nome NO_Magnitude, opc.* '
      'FROM ovcynsmixopc opc'
      'LEFT JOIN ovcynsqstmag mag ON  mag.Codigo=opc.Magnitude'
      'WHERE opc.Codigo=0')
    Left = 244
    Top = 301
    object QrOVcYnsMixOpcNO_Magnitude: TWideStringField
      FieldName = 'NO_Magnitude'
      Size = 60
    end
    object QrOVcYnsMixOpcCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrOVcYnsMixOpcControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrOVcYnsMixOpcNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
    object QrOVcYnsMixOpcMagnitude: TIntegerField
      FieldName = 'Magnitude'
      Required = True
    end
    object QrOVcYnsMixOpcPontNeg: TIntegerField
      FieldName = 'PontNeg'
      Required = True
    end
    object QrOVcYnsMixOpcLk: TIntegerField
      FieldName = 'Lk'
      Required = True
    end
    object QrOVcYnsMixOpcDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrOVcYnsMixOpcDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrOVcYnsMixOpcUserCad: TIntegerField
      FieldName = 'UserCad'
      Required = True
    end
    object QrOVcYnsMixOpcUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Required = True
    end
    object QrOVcYnsMixOpcAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Required = True
    end
    object QrOVcYnsMixOpcAWServerID: TIntegerField
      FieldName = 'AWServerID'
      Required = True
    end
    object QrOVcYnsMixOpcAWStatSinc: TSmallintField
      FieldName = 'AWStatSinc'
      Required = True
    end
    object QrOVcYnsMixOpcAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
  end
  object DsOVcYnsMixOpc: TDataSource
    DataSet = QrOVcYnsMixOpc
    Left = 244
    Top = 349
  end
  object QrOVgItxGerBtl: TMySQLQuery
    Database = Dmod.ZZDB
    SQL.Strings = (
      'SELECT * '
      'FROM ovgitxgerbtl'
      'WHERE Codigo=0')
    Left = 344
    Top = 304
    object QrOVgItxGerBtlCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrOVgItxGerBtlControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrOVgItxGerBtlOrdem: TIntegerField
      FieldName = 'Ordem'
      Required = True
    end
    object QrOVgItxGerBtlSeccaoOP: TWideStringField
      FieldName = 'SeccaoOP'
      Size = 60
    end
    object QrOVgItxGerBtlSeccaoMaq: TWideStringField
      FieldName = 'SeccaoMaq'
      Size = 60
    end
    object QrOVgItxGerBtlQtReal: TFloatField
      FieldName = 'QtReal'
      Required = True
    end
    object QrOVgItxGerBtlLk: TIntegerField
      FieldName = 'Lk'
      Required = True
    end
    object QrOVgItxGerBtlDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrOVgItxGerBtlDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrOVgItxGerBtlUserCad: TIntegerField
      FieldName = 'UserCad'
      Required = True
    end
    object QrOVgItxGerBtlUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Required = True
    end
    object QrOVgItxGerBtlAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Required = True
    end
    object QrOVgItxGerBtlAWServerID: TIntegerField
      FieldName = 'AWServerID'
      Required = True
    end
    object QrOVgItxGerBtlAWStatSinc: TSmallintField
      FieldName = 'AWStatSinc'
      Required = True
    end
    object QrOVgItxGerBtlAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
  end
  object DsOVgItxGerBtl: TDataSource
    DataSet = QrOVgItxGerBtl
    Left = 344
    Top = 352
  end
end
