unit OVcYnsMedTop;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, DBCtrls, dmkLabel, Mask,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkDBEdit, dmkGeral, Variants,
  dmkValUsu, dmkImage, UnDmkEnums, dmkRadioGroup, UnOVS_Vars;

type
  TFmOVcYnsMedTop = class(TForm)
    Panel1: TPanel;
    CkContinuar: TCheckBox;
    GroupBox1: TGroupBox;
    DBEdCodigo: TdmkDBEdit;
    Label5: TLabel;
    DBEdNome: TDBEdit;
    Label3: TLabel;
    GroupBox2: TGroupBox;
    EdControle: TdmkEdit;
    Label6: TLabel;
    EdNome: TdmkEdit;
    Label7: TLabel;
    CBTobiko: TdmkDBLookupComboBox;
    EdTobiko: TdmkEditCB;
    Label1: TLabel;
    SbTobiko: TSpeedButton;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    QrOVcYnsGraTop: TMySQLQuery;
    DsOVcYnsGraTop: TDataSource;
    Label11: TLabel;
    RGTolerBasCalc: TdmkRadioGroup;
    RGTolerUnMdida: TdmkRadioGroup;
    EdTolerRndPerc: TdmkEdit;
    QrOVcYnsGraTopCodigo: TIntegerField;
    QrOVcYnsGraTopNome: TWideStringField;
    QrOVcYnsGraTopTolerBasCalc: TSmallintField;
    QrOVcYnsGraTopTolerUnMdida: TSmallintField;
    QrOVcYnsGraTopTolerRndPerc: TFloatField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure SbTobikoClick(Sender: TObject);
    procedure EdTobikoRedefinido(Sender: TObject);
    procedure EdTobikoKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure CBTobikoKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
  private
    { Private declarations }
    procedure ReopenTop(Controle: Integer);
    procedure ReopenOVcYnsQstTop(Quais: TWhatPsqHow);
    procedure TopicoKeyUp(Sender: TObject; Key: Word; Shift: TShiftState);
  public
    { Public declarations }
    FQrIts: TmySQLQuery;
    FDsCad, FDsIts: TDataSource;
    FContexRef: Integer;
  end;

  var
  FmOVcYnsMedTop: TFmOVcYnsMedTop;

implementation

uses UnMyObjects, Module, UMySQLModule, UnInternalConsts, MyDBCheck, DmkDAC_PF,
  UnOVS_Jan, OVcYnsMedCad;

{$R *.DFM}

procedure TFmOVcYnsMedTop.BtOKClick(Sender: TObject);
var
  Codigo, Controle, Tobiko, TolerBasCalc, TolerUnMdida: Integer;
  TolerRndPerc: Double;
  SQLType: TSQLType;
  GradesToAdd: TGradesToAdd;
  TamsToAdd: TTamsToAdd;
begin
  SQLType        := ImgTipo.SQLType;
  Codigo         := Geral.IMV(DBEdCodigo.Text);
  Controle       := EdControle.ValueVariant;
  Tobiko         := EdTobiko.ValueVariant;
  TolerBasCalc   := RGTolerBasCalc.ItemIndex;
  TolerUnMdida   := RGTolerUnMdida.ItemIndex;
  TolerRndPerc   := EDTolerRndPerc.ValueVariant;
  //
  if MyObjects.FIC(Tobiko = 0, EdTobiko, 'Informe o T�pico!') then
    Exit;
  Controle := UMyMod.BPGS1I32('ovcynsmedtop', 'Controle', '', '', tsPos, SQLType, Controle);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'ovcynsmedtop', False, [
  'Codigo', 'Tobiko', 'TolerBasCalc',
  'TolerUnMdida', 'TolerRndPerc'], [
  'Controle'], [
  Codigo, Tobiko, TolerBasCalc,
  TolerUnMdida, TolerRndPerc], [
  Controle], True) then
  begin
    SetLength(GradesToAdd, 0);
    SetLength(TamsToAdd, 0);
    //
    FmOVcYnsMedCad.VerificaEAdicionaNovosItensGrade(GradesToAdd, TamsToAdd);
    ReopenTop(Controle);
    if CkContinuar.Checked then
    begin
      ImgTipo.SQLType              := stIns;
      EdControle.ValueVariant      := 0;
      EdTobiko.ValueVariant        := 0;
      CBTobiko.KeyValue            := Null;
      //RGTolerBasCalc.ItemIndex     := 0;
      //RGTolerUnMdida.ItemIndex     := 0;
      EdNome.ValueVariant          := '';
      //EdTolerRndPerc.ValueVariant  := 0;
      //
      EdTobiko.SetFocus;
    end else Close;
  end;
end;

procedure TFmOVcYnsMedTop.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmOVcYnsMedTop.CBTobikoKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  TopicoKeyUp(Sender, Key, Shift);
end;

procedure TFmOVcYnsMedTop.EdTobikoKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  TopicoKeyUp(Sender, Key, Shift);
end;

procedure TFmOVcYnsMedTop.EdTobikoRedefinido(Sender: TObject);
begin
  RGTolerBasCalc.ItemIndex    := QrOVcYnsGraTopTolerBasCalc.Value;
  RGTolerUnMdida.ItemIndex    := QrOVcYnsGraTopTolerUnMdida.Value;
  EdTolerRndPerc.ValueVariant := QrOVcYnsGraTopTolerRndPerc.Value;
end;

procedure TFmOVcYnsMedTop.FormActivate(Sender: TObject);
begin
  DBEdCodigo.DataSource   := FDsCad;
  DBEdNome.DataSource     := FDsCad;
  //
  MyObjects.CorIniComponente();
end;

procedure TFmOVcYnsMedTop.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  ReopenOVcYnsQstTop(TWhatPsqHow.wphAll);
end;

procedure TFmOVcYnsMedTop.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmOVcYnsMedTop.ReopenTop(Controle: Integer);
begin
  if FQrIts <> nil then
  begin
    UnDmkDAC_PF.AbreQuery(FQrIts, FQrIts.Database);
    //
    if Controle <> 0 then
      FQrIts.Locate('Controle', Controle, []);
  end;
end;

procedure TFmOVcYnsMedTop.ReopenOVcYnsQstTop(Quais: TWhatPsqHow);
begin
  UnDmkDAC_PF.AbreQuery(QrOVcYnsGraTop, DMod.MyDB);
end;

procedure TFmOVcYnsMedTop.SbTobikoClick(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  OVS_Jan.MostraFormOVcYnsGraTop();
  UMyMod.SetaCodigoPesquisado(EdTobiko, CBTobiko, QrOVcYnsGraTop, VAR_CADASTRO);
end;

procedure TFmOVcYnsMedTop.TopicoKeyUp(Sender: TObject; Key: Word;
  Shift: TShiftState);
begin
end;

end.
