unit Anotacoes;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnInternalConsts, Db, MyDBCheck,
  mySQLDbTables, DBCtrls, ComCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB,
  UMySQLModule, dmkLabel, (*ABSMain,*) Grids, DBGrids, dmkDBGrid, frxClass,
  frxDBSet, Mask, dmkDBEdit, dmkMemo, Menus, dmkCheckBox, dmkGeral, UnGotoy,
  dmkImage, UnDmkEnums, UnDmkProcFunc;

type
  TFmAnotacoes = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    Label2: TLabel;
    EdNome: TdmkEdit;
    Label3: TLabel;
    MeTexto: TdmkMemo;
    Panel1: TPanel;
    dmkDBGrid1: TdmkDBGrid;
    EdPesquisa: TdmkEdit;
    PnCont: TPanel;
    frxCAD_ANOTA_001: TfrxReport;
    QrAnot: TmySQLQuery;
    QrAnotCodigo: TIntegerField;
    QrAnotNome: TWideStringField;
    QrAnotTexto: TWideMemoField;
    DsAnot: TDataSource;
    frxDsAnot: TfrxDBDataset;
    Panel6: TPanel;
    Label9: TLabel;
    EdCodigo: TdmkEdit;
    PMImprime: TPopupMenu;
    Atual1: TMenuItem;
    Pesquisados1: TMenuItem;
    Panel7: TPanel;
    Panel8: TPanel;
    BtPesquisa: TBitBtn;
    Panel9: TPanel;
    Label4: TLabel;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    FindDialog1: TFindDialog;
    DBRichEdit1: TDBRichEdit;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel3: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBCntrl: TGroupBox;
    Panel10: TPanel;
    Panel11: TPanel;
    BtSaida: TBitBtn;
    BtInclui: TBitBtn;
    BtAltera: TBitBtn;
    BtExclui: TBitBtn;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel2: TPanel;
    BtDesiste: TBitBtn;
    QrAnotAtivo: TSmallintField;
    CkRestrito: TdmkCheckBox;
    QrAnotRestrito: TIntegerField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure EdPesquisaEnter(Sender: TObject);
    procedure EdPesquisaExit(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure EdPesquisaChange(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtIncluiClick(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtExcluiClick(Sender: TObject);
    procedure Atual1Click(Sender: TObject);
    procedure Pesquisados1Click(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
    procedure BtPesquisaClick(Sender: TObject);
    procedure FindDialog1Find(Sender: TObject);
    procedure SbNovoClick(Sender: TObject);
  private
    { Private declarations }
    procedure MostraEdicao(Mostra : Boolean; SQLType: TSQLType; Codigo : Integer);
    procedure ReabreAnot(LocCod, Codigo: Integer; Nome: String);
  public
    { Public declarations }
    FCodigo, FControle: Integer;
  end;

  var
  FmAnotacoes: TFmAnotacoes;

implementation

{$R *.DFM}

uses UnMyObjects, Module, MyListas;

procedure TFmAnotacoes.Atual1Click(Sender: TObject);
begin
  ReabreAnot(0, QrAnotCodigo.Value, '');
  MyObjects.frxMostra(frxCAD_ANOTA_001, 'Anota��es');
end;

procedure TFmAnotacoes.BtAlteraClick(Sender: TObject);
begin
  if QrAnotCodigo.Value > 0 then
  begin
    MostraEdicao(True, stUpd, 0);
  end;
end;

procedure TFmAnotacoes.BtConfirmaClick(Sender: TObject);
var
  Codigo, Restrito: Integer;
  Nome: String;
begin
  Nome     := EdNome.ValueVariant;
  Restrito := Geral.BoolToInt(CkRestrito.Checked);
  //
  if MyObjects.FIC(Nome = '', EdNome, 'Defina uma descri��o!') then Exit;
  //
  Codigo := UMyMod.BuscaEmLivreY_Def('anotacoes', 'Codigo', ImgTipo.SQLType,
    QrAnotCodigo.Value);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'anotacoes', False,
  [
    'Nome', 'Texto', 'Restrito'
  ], ['Codigo'],
  [
    Nome, MeTexto.Text, Restrito
  ], [Codigo], True) then
  begin
    ReabreAnot(Codigo, Codigo, '');
    MostraEdicao(False, stLok, 0);
  end;
end;

procedure TFmAnotacoes.BtDesisteClick(Sender: TObject);
var
  Codigo: Integer;
begin
  Codigo := Geral.IMV(EdCodigo.Text);
  if ImgTipo.SQLType = stIns then UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'anotacoes', Codigo);
  MostraEdicao(False, stLok, 0);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'anotacoes', 'Codigo');
end;

procedure TFmAnotacoes.BtExcluiClick(Sender: TObject);
var
  Codigo: Integer;
begin
  if QrAnotCodigo.Value > 0 then
  begin
    if Geral.MB_Pergunta('Confirma a exclus�o do item ' + QrAnotNome.Value +
    '?') = ID_YES then
    begin
      Screen.Cursor := crHourGlass;
      Codigo := QrAnotCodigo.Value;
      //
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('DELETE FROM anotacoes WHERE Codigo=:P0');
      Dmod.QrUpd.Params[0].AsInteger := Codigo;
      Dmod.QrUpd.ExecSQL;
      //
      ReabreAnot(0, 0, '');
      //
      Screen.Cursor := crDefault;
    end;
  end;
end;

procedure TFmAnotacoes.BtIncluiClick(Sender: TObject);
begin
  MostraEdicao(True, stIns, 0);
end;

procedure TFmAnotacoes.BtPesquisaClick(Sender: TObject);
begin
  FindDialog1.Execute;
end;

procedure TFmAnotacoes.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmAnotacoes.EdPesquisaChange(Sender: TObject);
begin
  if (UpperCase(EdPesquisa.ValueVariant) <> 'PESQUISAR') and
  (Length(EdPesquisa.ValueVariant) > 0) then
    ReabreAnot(0, 0, EdPesquisa.ValueVariant)
  else
    ReabreAnot(0, 0, '');
end;

procedure TFmAnotacoes.EdPesquisaEnter(Sender: TObject);
begin
  if Length(EdPesquisa.ValueVariant) > 0 then
    EdPesquisa.ValueVariant := '';
end;

procedure TFmAnotacoes.EdPesquisaExit(Sender: TObject);
begin
  if Length(EdPesquisa.ValueVariant) = 0 then
    EdPesquisa.ValueVariant := 'Pesquisar';
end;

procedure TFmAnotacoes.FindDialog1Find(Sender: TObject);
var
  FoundAt: LongInt;
  StartPos, ToEnd: Integer;
  mySearchTypes : TSearchTypes;
  //myFindOptions : TFindOptions;
begin
  mySearchTypes := [];
  with DBRichEdit1 do
  begin
    if frMatchCase in FindDialog1.Options then
       mySearchTypes := mySearchTypes + [stMatchCase];
    if frWholeWord in FindDialog1.Options then
       mySearchTypes := mySearchTypes + [stWholeWord];
    { begin the search after the current selection if there is one }
    { otherwise, begin at the start of the text }
    if SelLength <> 0 then
      StartPos := SelStart + SelLength
    else
      StartPos := 0;
    { ToEnd is the length from StartPos to the end of the
      text in the rich edit control }
    ToEnd := Length(Text) - StartPos;
    FoundAt :=
      FindText(FindDialog1.FindText, StartPos, ToEnd, mySearchTypes);
    if FoundAt <> -1 then
    begin
      SelStart  := FoundAt;
      SelLength := Length(FindDialog1.FindText);
      SetFocus;
      Perform(EM_SCROLLCARET,SB_LINEDOWN,0);
    end else
      Beep;
  end;
end;

procedure TFmAnotacoes.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmAnotacoes.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  ReabreAnot(0, 0, '');
  MostraEdicao(False, stLok, 0);
  PnCont.Align  := alClient;
  MeTexto.Align := alClient;
end;

procedure TFmAnotacoes.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmAnotacoes.MostraEdicao(Mostra: Boolean; SQLType: TSQLType;
  Codigo: Integer);
begin
  if Mostra then
  begin
    PnDados.Visible := False;
    PnEdita.Visible := True;
    if SQLType = stIns then
    begin
      EdCodigo.ValueVariant := FormatFloat('000', Codigo);
      CkRestrito.Checked    := False;
      EdNome.ValueVariant   := '';
      MeTexto.Text          := '';
    end else begin
      EdCodigo.ValueVariant := QrAnotCodigo.Value;
      CkRestrito.Checked    := Geral.IntToBool(QrAnotRestrito.Value);
      EdNome.ValueVariant   := QrAnotNome.Value;
      MeTexto.Text          := QrAnotTexto.Value;
    end;
    CkRestrito.SetFocus;
  end else begin
    PnDados.Visible := True;
    PnEdita.Visible := False;
  end;
  ImgTipo.SQLType := SQLType;
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmAnotacoes.Pesquisados1Click(Sender: TObject);
begin
  MyObjects.frxMostra(frxCAD_ANOTA_001, 'Anota��es');
end;

procedure TFmAnotacoes.ReabreAnot(LocCod, Codigo: Integer; Nome: String);
var
  Usuario: Integer;
begin
  Usuario := VAR_USUARIO;
  //
  QrAnot.Close;
  QrAnot.SQL.Clear;
  QrAnot.SQL.Add('SELECT Codigo, Nome, Texto, Restrito, Ativo');
  QrAnot.SQL.Add('FROM anotacoes');
  QrAnot.SQL.Add('WHERE Codigo > 0');
  QrAnot.SQL.Add('AND (Restrito = 0 OR (Restrito = 1 AND UserCad = ' + Geral.FF0(Usuario) + '))');
  //
  if Length(Nome) > 0 then
    QrAnot.SQL.Add('AND Nome LIKE "%' + Nome + '%"');
  if Codigo > 0 then
    QrAnot.SQL.Add('AND Codigo=' + Geral.FF0(Codigo));
  QrAnot.Open;
  //
  if LocCod <> 0 then
    QrAnot.Locate('Codigo', Codigo, []);
  //
  EdNome.ValueVariant := QrAnotNome.Value;
  MeTexto.Text        := QrAnotTexto.Value;
end;

procedure TFmAnotacoes.SbImprimeClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMImprime, SbImprime);
end;

procedure TFmAnotacoes.SbNovoClick(Sender: TObject);
begin
  Geral.MB_Aviso('Pesquisa n�o dispon�vel para esta janela!');
end;

end.



