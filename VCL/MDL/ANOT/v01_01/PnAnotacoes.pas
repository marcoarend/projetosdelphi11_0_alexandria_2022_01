unit PnAnotacoes;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnInternalConsts, Db, MyDBCheck,
  mySQLDbTables, DBCtrls, ComCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB,
  UMySQLModule, dmkLabel, ABSMain, Grids, DBGrids, dmkDBGrid, frxClass,
  frxDBSet, Mask, dmkDBEdit, dmkMemo, Menus, dmkCheckBox, dmkGeral, UnGotoy,
  Vcl.CheckLst, Variants, dmkDBGridZTO, UnDmkProcFunc, dmkImage, UnDmkEnums;

type
  TFmPnAnotacoes = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    Label2: TLabel;
    EdNome: TdmkEdit;
    Label3: TLabel;
    MeTexto: TdmkMemo;
    Panel1: TPanel;
    dmkDBGrid1: TdmkDBGridZTO;
    EdPesquisa: TdmkEdit;
    PnCont: TPanel;
    frxCAD_ANOTA_001: TfrxReport;
    QrAnot: TmySQLQuery;
    QrAnotCodigo: TIntegerField;
    QrAnotNome: TWideStringField;
    QrAnotTexto: TWideMemoField;
    DsAnot: TDataSource;
    frxDsAnot: TfrxDBDataset;
    Panel6: TPanel;
    Label9: TLabel;
    EdCodigo: TdmkEdit;
    PMImprime: TPopupMenu;
    Atual1: TMenuItem;
    Pesquisados1: TMenuItem;
    Panel7: TPanel;
    Panel8: TPanel;
    BtPesquisa: TBitBtn;
    Panel9: TPanel;
    Label4: TLabel;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    FindDialog1: TFindDialog;
    DBRichEdit1: TDBRichEdit;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel3: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBCntrl: TGroupBox;
    Panel10: TPanel;
    Panel11: TPanel;
    BtSaida: TBitBtn;
    BtInclui: TBitBtn;
    BtAltera: TBitBtn;
    BtExclui: TBitBtn;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel2: TPanel;
    BtDesiste: TBitBtn;
    PMTudo: TPopupMenu;
    Inclui1: TMenuItem;
    Altera1: TMenuItem;
    Exclui1: TMenuItem;
    N1: TMenuItem;
    Imprime1: TMenuItem;
    Atual2: TMenuItem;
    Pesquisados2: TMenuItem;
    Panel4: TPanel;
    Panel5: TPanel;
    CBLinkTabs: TCheckListBox;
    BtTudo: TBitBtn;
    BtNenhum: TBitBtn;
    EdLinkTab: TdmkEdit;
    Label5: TLabel;
    EdLinkTab_TXT: TdmkEdit;
    Label6: TLabel;
    EdLinkID1: TdmkEdit;
    QrAnotAtivo: TSmallintField;
    QrLoc: TmySQLQuery;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure EdPesquisaEnter(Sender: TObject);
    procedure EdPesquisaExit(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure EdPesquisaChange(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtIncluiClick(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtExcluiClick(Sender: TObject);
    procedure Atual1Click(Sender: TObject);
    procedure Pesquisados1Click(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
    procedure BtPesquisaClick(Sender: TObject);
    procedure FindDialog1Find(Sender: TObject);
    procedure Exclui1Click(Sender: TObject);
    procedure Altera1Click(Sender: TObject);
    procedure Inclui1Click(Sender: TObject);
    procedure Atual2Click(Sender: TObject);
    procedure Pesquisados2Click(Sender: TObject);
    procedure CBLinkTabsClickCheck(Sender: TObject);
    procedure BtTudoClick(Sender: TObject);
    procedure BtNenhumClick(Sender: TObject);
    procedure EdLinkTabChange(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
  private
    { Private declarations }
    (*FLinkTip: array of: Integer;
    FLinkTbN, FLinkFlN: array of String;
    FLinkDbX: array of TmySQLDatabase;
    *)
    //
    procedure MostraEdicao(Mostra : Boolean; SQLType: TSQLType; Codigo : Integer);
  public
    { Public declarations }
    FLinkID1, FLinkTab: array of Variant;
    FCodigo, FControle: Integer;
    //
    procedure ReabreAnot(LocCod, Codigo: Integer);
    procedure DefineFiltroLink(CodTabs: array of Integer);
  end;

  var
  FmPnAnotacoes: TFmPnAnotacoes;

implementation

{$R *.DFM}

uses UnMyObjects, Module, MyListas, DmkDAC_PF, MyGlyfs, Principal, UnAnotacoes_Tabs;

const
  _Item_Menu_ = 'Item_Menu_';
  _Item_Leng_ = 9;

procedure TFmPnAnotacoes.Altera1Click(Sender: TObject);
begin
  BtAlteraClick(Self);
end;

procedure TFmPnAnotacoes.Atual1Click(Sender: TObject);
begin
  //ReabreAnot(0, QrAnotCodigo.Value, '');
  ReabreAnot(0, QrAnotCodigo.Value);
  MyObjects.frxMostra(frxCAD_ANOTA_001, 'Anota��es');
end;

procedure TFmPnAnotacoes.Atual2Click(Sender: TObject);
begin
  Atual1Click(Self);
end;

procedure TFmPnAnotacoes.BtAlteraClick(Sender: TObject);
begin
  if QrAnotCodigo.Value > 0 then
  begin
    MostraEdicao(True, stUpd, 0);
  end;
end;

procedure TFmPnAnotacoes.BtConfirmaClick(Sender: TObject);
var
  Codigo, LinkTab, LinkID1: Integer;
  Nome, Texto: String;
begin
  Nome := EdNome.Text;
  if Length(Nome) = 0 then
  begin
    Geral.MB_Aviso('Defina uma descri��o.');
    EdNome.SetFocus;
    Exit;
  end;
  Texto := MeTexto.Text;
  LinkTab := EdLinkTab.ValueVariant;
  LinkID1 := EdLinkID1.ValueVariant;
  //
  Codigo := UMyMod.BuscaEmLivreY_Def('anotacoes', 'Codigo', ImgTipo.SQLType,
    QrAnotCodigo.Value);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'anotacoes', False, [
  (*
    'Nome', 'Texto'
  ], ['Codigo'],
  [
    Nome, MeTexto.Text
  *)
  'Nome', 'Texto', 'LinkTab',
  'LinkID1'], [
  'Codigo'], [
  Nome, Texto, LinkTab,
  LinkID1], [
  Codigo], True) then
  begin
    //ReabreAnot(Codigo, Codigo, '');
    ReabreAnot(Codigo, Codigo);
    MostraEdicao(False, stLok, 0);
  end;
end;

procedure TFmPnAnotacoes.BtDesisteClick(Sender: TObject);
var
  Codigo: Integer;
begin
  Codigo := Geral.IMV(EdCodigo.Text);
  if ImgTipo.SQLType = stIns then UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'anotacoes', Codigo);
  MostraEdicao(False, stLok, 0);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'anotacoes', 'Codigo');
end;

procedure TFmPnAnotacoes.BtExcluiClick(Sender: TObject);
var
  Codigo: Integer;
begin
  if QrAnotCodigo.Value > 0 then
  begin
    if Geral.MB_Pergunta('Confirma a exclus�o do item ' +
    QrAnotNome.Value+'?') = ID_YES then
    begin
      Screen.Cursor := crHourGlass;
      Codigo := QrAnotCodigo.Value;
      //
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('DELETE FROM anotacoes WHERE Codigo=:P0');
      Dmod.QrUpd.Params[0].AsInteger := Codigo;
      Dmod.QrUpd.ExecSQL;
      //
      //ReabreAnot(0, 0, '');
      ReabreAnot(0, 0);
      //
      Screen.Cursor := crDefault;
    end;
  end;
end;

procedure TFmPnAnotacoes.BtIncluiClick(Sender: TObject);
begin
  MostraEdicao(True, stIns, 0);
end;

procedure TFmPnAnotacoes.BtNenhumClick(Sender: TObject);
begin
  CBLinkTabs.CheckAll(cbUnchecked);
  ReabreAnot(0, 0);
end;

procedure TFmPnAnotacoes.BtPesquisaClick(Sender: TObject);
begin
  FindDialog1.Execute;
end;

procedure TFmPnAnotacoes.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmPnAnotacoes.BtTudoClick(Sender: TObject);
begin
  CBLinkTabs.CheckAll(cbChecked);
  ReabreAnot(0, 0);
end;

procedure TFmPnAnotacoes.CBLinkTabsClickCheck(Sender: TObject);
begin
  ReabreAnot(0, 0);
end;

procedure TFmPnAnotacoes.DefineFiltroLink(CodTabs: array of Integer);
const
  Separador = '@|@';
var
  I, J, L, P, K: Integer;
  Lista: array of string;
  Novo: TMenuItem;
  Dados: String;
begin
  CBLinkTabs.Items.Clear;
  //
  K := High(CodTabs);
  for I := 0 to K do
    CBLinkTabs.Items.Add(ARRAY_ANOTACOES_TAB[CodTabs[I]] + Separador +
    Geral.FF0(CodTabs[I])(* + Separador + Geral.FF0(CodID1s[I])*));
  //
  CBLinkTabs.Sorted := True;
  //
  SetLength(Lista, CBLinkTabs.Items.Count);
  for I := 0 to K do
    Lista[I] := CBLinkTabs.Items[I];
  //
  CBLinkTabs.Sorted := False;
  CBLinkTabs.Clear;
  L := Length(Separador);
  SetLength(FLinkTab, Length(CodTabs));
  {
  //SetLength(FLinkID1, Length(CodTabs));
  SetLength(FLinkTip, Length(CodTabs));
  SetLength(FLinkDbX, Length(CodTabs));
  SetLength(FLinkTbN, Length(CodTabs));
  SetLength(FLinkFlN, Length(CodTabs));
  }
  for I := 0 to K do
  begin
    P := Pos(Separador, Lista[I]);
    CBLinkTabs.Items.Add(Copy(Lista[I], 1, P -1));
    Dados := Copy(Lista[I], P + L);
    FLinkTab[I] := Geral.IMV(Dados);
    //P := Pos(Separador, Dados);
    //FLinkTab[I] := Geral.IMV(Copy(Dados, 1, P - 1));
    //FLinkID1[I] := Geral.IMV(Copy(Dados, P + L));
    {
    for J := 0 to K do
    begin
      if CodTabs[J] = FLinkTab[I] then
      begin
        FLinkTip[I] := LinkTip[J];
        FLinkDbX[I] := LinkDbX[J];
        FLinkTbN[I] := LinkTbN[J];
        FLinkFlN[I] := LinkFlN[J];
      end;
    end;
    }
  end;
  CBLinkTabs.ItemHeight := Trunc(CBLinkTabs.ItemHeight * 1.5);
  //
  ReabreAnot(0, 0); // Movido do OnCreate!!
  //
  for I := 0 to K do
  begin
    //ShowMessage (IntToStr(FLinkTab[I]) + ' - ' + IntToStr(FLinkID1[I]));
    Novo := TMenuItem.Create(Self);
    Novo.Name := _Item_Menu_ + Geral.FFN(FLinkTab[I], _Item_Leng_) + '_' + CBLinkTabs.Items[I];
    //Novo.Tag  := FCodTabs[I];
    Novo.Caption := CBLinkTabs.Items[I];
    Novo.OnClick := Inclui1.Onclick;
    try
      Inclui1.Add(Novo);
    except
      Geral.MB_Erro('ERRO!' + sLineBreak + Novo.Caption + sLineBreak +
      Inclui1.Caption);
    end;
  end;
  //
end;

procedure TFmPnAnotacoes.EdLinkTabChange(Sender: TObject);
var
  LinkTab: Integer;
begin
  LinkTab := EdLinkTab.ValueVariant;
  EdLinkTab_TXT.Text := ARRAY_ANOTACOES_TAB[LinkTab];
end;

procedure TFmPnAnotacoes.EdPesquisaChange(Sender: TObject);
begin
{
  if (UpperCase(EdPesquisa.ValueVariant) <> 'PESQUISAR') and
  (Length(EdPesquisa.ValueVariant) > 0) then
    ReabreAnot(0, 0, EdPesquisa.ValueVariant)
  else
    ReabreAnot(0, 0, '');
}
  ReabreAnot(0, 0);
end;

procedure TFmPnAnotacoes.EdPesquisaEnter(Sender: TObject);
begin
  if Length(EdPesquisa.ValueVariant) > 0 then
    EdPesquisa.ValueVariant := '';
end;

procedure TFmPnAnotacoes.EdPesquisaExit(Sender: TObject);
begin
  if Length(EdPesquisa.ValueVariant) = 0 then
    EdPesquisa.ValueVariant := 'Pesquisar';
end;

procedure TFmPnAnotacoes.Exclui1Click(Sender: TObject);
begin
  BtExcluiClick(Self);
end;

procedure TFmPnAnotacoes.FindDialog1Find(Sender: TObject);
var
  FoundAt: LongInt;
  StartPos, ToEnd: Integer;
  mySearchTypes : TSearchTypes;
  //myFindOptions : TFindOptions;
begin
  mySearchTypes := [];
  with DBRichEdit1 do
  begin
    if frMatchCase in FindDialog1.Options then
       mySearchTypes := mySearchTypes + [stMatchCase];
    if frWholeWord in FindDialog1.Options then
       mySearchTypes := mySearchTypes + [stWholeWord];
    { begin the search after the current selection if there is one }
    { otherwise, begin at the start of the text }
    if SelLength <> 0 then
      StartPos := SelStart + SelLength
    else
      StartPos := 0;
    { ToEnd is the length from StartPos to the end of the
      text in the rich edit control }
    ToEnd := Length(Text) - StartPos;
    FoundAt :=
      FindText(FindDialog1.FindText, StartPos, ToEnd, mySearchTypes);
    if FoundAt <> -1 then
    begin
      SelStart  := FoundAt;
      SelLength := Length(FindDialog1.FindText);
      SetFocus;
      Perform(EM_SCROLLCARET,SB_LINEDOWN,0);
    end else
      Beep;
  end;
end;

procedure TFmPnAnotacoes.FormActivate(Sender: TObject);
begin
  if TFmPnAnotacoes(Self).Owner is TApplication then
  begin
    MyObjects.CorIniComponente();
  end;
end;

procedure TFmPnAnotacoes.FormCreate(Sender: TObject);
var
  I: Integer;
begin
  ImgTipo.SQLType := stLok;
  //
  //ReabreAnot(0, 0, '');
  //FLinkTab := 0;
  SetLength(FLinkID1, MAX_ANOTACOES_TAB + 1);
  for I := 0 to MAX_ANOTACOES_TAB do
    FLinkID1[I] := 0;
  //
  MostraEdicao(False, stLok, 0);
  PnCont.Align  := alClient;
  MeTexto.Align := alClient;
end;

procedure TFmPnAnotacoes.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmPnAnotacoes.FormShow(Sender: TObject);
begin
  FmMyGlyfs.DefineGlyfsTDI(FmPrincipal.sd1, Sender);
end;

procedure TFmPnAnotacoes.Inclui1Click(Sender: TObject);
const
  TxtErr = 'Inclus�o de anota��o cnacelada!' + sLineBreak;
var
  Nome: String;
  LinkTab, LinkID1, I, LinkTip: Integer;
  LinkDbX: TmySQLDataBase;
  LinkTbN, LinkFlN: String;
  Qry: TmySQLQuery;
  //Achou: Boolean;
begin
  //Achou := False;
  LinkID1 := 0;
  Nome := TComponent(Sender).Name;
  if Nome <> 'Inclui1' then
  begin
    Nome := Copy(Nome, pos(_Item_Menu_, Nome) + Length(_Item_Menu_), _Item_Leng_);
    LinkTab := Geral.IMV(Nome);
    if FLinkID1[LinkTab] <> Null then
    begin
      LinkID1 := FLinkID1[LinkTab];
      {
      for I := 0 to High(FLinkTab) do
      begin
        if FLinkTab[I] = LinkTab then
        begin
          LinkTip := FLinkTip[I];
          LinkDbX := FLinkDbX[I];
          LinkTbN := FLinkTbN[I];
          LinkFlN := FLinkFlN[I];
          //
          Achou := True;
        end;
      end;
      if Achou then
      begin
        if LinkTip = TYP_ANOTACOES_TAB_0_TABELA then
        begin
          Qry := TmySQLQuery.Create(Dmod);
          try
            N'ao funciona! precisa de Condi;'ao WHERE e AND
            UnDmkDAC_PF.AbreMySQLQuery0(Qry, LinkDbX, [
            'SELECT ' + LinkFlN,
            'FROM ' + LinkTbN,
            '']);
            LinkID1 := Qry.FieldByName(LinkFlN).AsInteger;
          finally
            Qry.Free;
          end;
          //
          EdLinkTab.ValueVariant := LinkTab;
          EdLinkID1.ValueVariant := LinkID1;
          BtConfirmaClick(Self);
        end else
        begin
          Geral.MB_Erro('Relacionamento com a tabela de anota��es indefinida!');
          Exit;
        end;
      end else
      begin
        Geral.MB_Erro('Relacionamento com a tabela de anota��es n�o localizada!');
        Exit;
      end;
      }
      EdLinkTab.ValueVariant := LinkTab;
      EdLinkID1.ValueVariant := LinkID1;
      BtIncluiClick(Self);
    end else
    begin
      if not (LinkTab in [0..MAX_ANOTACOES_TAB]) then
        Geral.MB_Aviso(TxtErr + 'Tabela n�o definida!')
      else
      if FLinkID1[LinkTab] = Null then
        Geral.MB_Aviso(TxtErr + 'ID de registro n�o definido para a tabela: ' +
        sLineBreak + ARRAY_ANOTACOES_TAB[LinkTab]);
    end;
  end;
end;

procedure TFmPnAnotacoes.MostraEdicao(Mostra: Boolean; SQLType: TSQLType;
  Codigo: Integer);
begin
  if Mostra then
  begin
    PnDados.Visible := False;
    PnEdita.Visible := True;
    if SQLType = stIns then
    begin
      EdCodigo.ValueVariant := FormatFloat('000', Codigo);
      EdNome.ValueVariant   := '';
      MeTexto.Text          := '';
      //
      EdNome.SetFocus;
    end else begin
      EdCodigo.ValueVariant := QrAnotCodigo.Value;
      EdNome.ValueVariant   := QrAnotNome.Value;
      MeTexto.Text          := QrAnotTexto.Value;
      //
      UnDmkDAC_PF.AbreMySQLQuery0(QrLoc, Dmod.MyDB, [
      'SELECT LinkTab, LinkID1 ',
      'FROM anotacoes ',
      'WHERE Codigo=' + Geral.FF0(QrAnotCodigo.Value),
      '']);
      EdLinkTab.ValueVariant := QrLoc.FieldByName('LinkTab').AsInteger;
      EdLinkID1.ValueVariant := QrLoc.FieldByName('LinkID1').AsLargeInt;
      //
      EdNome.SetFocus;
    end;
  end else begin
    PnDados.Visible := True;
    PnEdita.Visible := False;
  end;
  ImgTipo.SQLType := SQLType;
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmPnAnotacoes.Pesquisados1Click(Sender: TObject);
begin
  MyObjects.frxMostra(frxCAD_ANOTA_001, 'Anota��es');
end;

procedure TFmPnAnotacoes.Pesquisados2Click(Sender: TObject);
begin
  Pesquisados1Click(Self);
end;

procedure TFmPnAnotacoes.ReabreAnot(LocCod, Codigo: Integer);
var
  Usuario, I, N: Integer;
  Nome, LinkTab: String;
begin
  Screen.Cursor := crHourGlass;
  try
    if (UpperCase(EdPesquisa.ValueVariant) <> 'PESQUISAR') and
    (Length(EdPesquisa.ValueVariant) > 0) then
      Nome := 'AND Nome LIKE "%' + EdPesquisa.ValueVariant + '%"'
    else
      Nome := '';
    //
    LinkTab := '';
    for I := 0 to CBLinkTabs.Items.Count - 1 do
    begin
      if CBLinkTabs.Checked[I] then
      begin
        //LinkTab := LinkTab + ', ' + Geral.FF0(FLinkTab[I]);
        //

        N := FLinkTab[I];
        LinkTab := LinkTab + ' OR (LinkTab=' + Geral.FF0(N);
        ///
        ///
        if N <> CO_ANOTACOES_TAB_0001_UNIVERSAL then
          LinkTab := LinkTab + ' AND LinkID1=' + Geral.FF0(FLinkID1[N]);
        ///
        ///
        LinkTab := LinkTab + ')' + sLineBreak;
      end;
    end;
    if LinkTab <> '' then
    begin
      //LinkTab := 'AND LinkTab IN (' + Copy(LinkTab, 3) + ')';
      // Copy � para tirar o primeiro "OR"
      LinkTab := 'AND (' + sLineBreak + Copy(LinkTab, 4) + ')' + sLineBreak;
      //
      Usuario := VAR_USUARIO;
      //
      UnDmkDAC_PF.AbreMySQLQuery0(QrAnot, Dmod.MyDB, [
        'SELECT Codigo, Nome, Texto, Ativo ',
        'FROM anotacoes ',
        'WHERE Codigo > 0 ',
        'AND (Restrito = 0 OR (Restrito = 1 AND UserCad = ' + Geral.FF0(Usuario) + '))',
        Nome,
        Geral.ATS_If( Codigo > 0, ['AND Codigo=' + Geral.FF0(Codigo)]),
        LinkTab,
        '']);
      if LocCod <> 0 then
        QrAnot.Locate('Codigo', Codigo, []);
      //
      EdNome.ValueVariant := QrAnotNome.Value;
      MeTexto.Text        := QrAnotTexto.Value;
    end else
      QrAnot.Close;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmPnAnotacoes.SbImprimeClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMImprime, SbImprime);
end;

procedure TFmPnAnotacoes.SbNomeClick(Sender: TObject);
begin
  //DefParams;  // precisa para evitar bug
  //... continuar aqui!
end;

procedure TFmPnAnotacoes.SbNumeroClick(Sender: TObject);
begin
  //DefParams;  // precisa para evitar bug
  //... continuar aqui!

end;

//ver melhor o componente TdmkDBGridZTO

end.



