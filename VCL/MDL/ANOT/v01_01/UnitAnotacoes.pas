unit UnitAnotacoes;

interface

uses
  StdCtrls, ExtCtrls, Windows, Messages, SysUtils, Classes, Graphics, Controls,
  ComCtrls, dmkGeral, Forms, Dialogs, Menus, UnMsgInt, UnInternalConsts, Db,
  DbCtrls, UnInternalConsts2, Variants;

type
  TUnitAnotacoes = class(TObject)
  private
    { Private declarations }
  public
    { Public declarations }
    procedure DefineVariavelEmFormTDIPnl(
      Form: TForm; (*ArrVar: array of Variant;*) Index: Integer; Valor: Variant;
      ZeroIsNull: Boolean);
  end;

var
  UnAnotacoes: TUnitAnotacoes;

implementation

uses PnAnotacoes;

{ TUnitAnotacoes }

procedure TUnitAnotacoes.DefineVariavelEmFormTDIPnl(Form: TForm;
  (*ArrVar: array of Variant;*) Index: Integer; Valor: Variant;
  ZeroIsNull: Boolean);
var
  Inteiro: Integer;
  Res: Variant;
begin
  if Form <> nil then
  begin
    Res := Valor;
    if Res <> Null then
    begin
    //if VarType(Valor) = vtInteger then
    //begin
      Inteiro := Valor;
      if ZeroIsNull and (Inteiro = 0) then
        Res := Null
      else
        Res := Valor;
    //end else
      //Res := Valor;
    end;
    TFmPnAnotacoes(Form).FLinkID1[Index] := Res;
  end;
end;

end.
