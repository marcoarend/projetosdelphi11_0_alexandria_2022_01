unit FTPWebArqCad;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, dmkCheckGroup, dmkDBLookupComboBox,
  dmkEditCB, dmkEdit, dmkPermissoes, dmkValUsu, Variants, mySQLDbTables,
  UnDmkEnums, DmkDAC_PF, dmkEditDateTimePicker;

type
  TFmFTPWebArqCad = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    Label12: TLabel;
    EdCodigo: TdmkEdit;
    VUEmpresa: TdmkValUsu;
    dmkPermissoes1: TdmkPermissoes;
    GroupBox2: TGroupBox;
    Label13: TLabel;
    EdNome: TdmkEdit;
    Label3: TLabel;
    EdFTPDir: TdmkEditCB;
    CBFTPDir: TdmkDBLookupComboBox;
    QrFTPWebDir: TmySQLQuery;
    IntegerField1: TIntegerField;
    StringField1: TWideStringField;
    DsFTPWebDir: TDataSource;
    QrFTPWebDirWeb_FTPh: TWideStringField;
    QrFTPWebDirWeb_FTPu: TWideStringField;
    QrFTPWebDirSENHA: TWideStringField;
    QrFTPWebDirPassivo: TIntegerField;
    QrFTPWebDirCaminho: TWideStringField;
    Label4: TLabel;
    EdTags: TdmkEdit;
    LaDataExp: TLabel;
    TPDataExp: TdmkEditDateTimePicker;
    Label2: TLabel;
    EdVersaoArq: TdmkEdit;
    QrLoc: TmySQLQuery;
    QrFTPWebDirNivel: TIntegerField;
    QrFTPWebDirCliInt: TIntegerField;
    Panel5: TPanel;
    PnEntidades: TPanel;
    Panel6: TPanel;
    BtInclui: TBitBtn;
    BtExclui: TBitBtn;
    Grade: TStringGrid;
    CGNivel: TdmkCheckGroup;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    Label1: TLabel;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure BtIncluiClick(Sender: TObject);
    procedure BtExcluiClick(Sender: TObject);
    procedure EdFTPDirChange(Sender: TObject);
  private
    { Private declarations }
    procedure MostraEdicao(Codigo: Integer);
    procedure ReopenFTPWebDir(FTPConfig, FTPTipoDir: Integer);
    function  VerificaSeArquivoExiste(Codigo: Integer; Arquivo: String): Integer;
  public
    { Public declarations }
    FNome, FPastaCam, FTags, FVersaoArq: String;
    FDmkIdApp, FCodigoApp, FCodigo, FFTPConfig, FFTPTipoDir, FFTPDir, FEmpresa,
    FNivel: Integer;
    FDataExp: TDate;
    FDesiste: Boolean;
    FEntidades: TStringList;
  end;

  var
  FmFTPWebArqCad: TFmFTPWebArqCad;

implementation

uses UnMyObjects, Module, ModuleGeral, UMySQLModule, UnFTP, UnDmkWeb,
  UnGrlUsuarios, UnGrl_Consts;

{$R *.DFM}

procedure TFmFTPWebArqCad.MostraEdicao(Codigo: Integer);
begin
  DModG.ReopenEmpresas(VAR_USUARIO, 0, EdEmpresa, CBEmpresa);
  //
  if Codigo = 0 then //Inclus�o
  begin
    PnEntidades.Visible   := True;
    //FmFTPWebArqCad.Height := 590;
    //GroupBox2.Height      := 215;
  end else
  begin
    PnEntidades.Visible   := False;
    FmFTPWebArqCad.Height := FmFTPWebArqCad.Height - PnEntidades.Height;
    GroupBox2.Height      := GroupBox2.Height - PnEntidades.Height;
    //FmFTPWebArqCad.Height := 590 - PnEntidades.Height;
    //GroupBox2.Height      := 215 - PnEntidades.Height;
  end;
  //
  EdCodigo.ValueVariant    := FormatFloat('0', Codigo);
  EdNome.ValueVariant      := FNome;
  EdVersaoArq.ValueVariant := FVersaoArq;
  EdTags.ValueVariant      := FTags;
  EdFTPDir.ValueVariant    := FFTPDir;
  CBFTPDir.KeyValue        := FFTPDir;
  VUEmpresa.ValueVariant   := FEmpresa;
  CGNivel.Value            := FNivel;
  TPDataExp.Date           := FDataExp;
  LaDataExp.Visible        := False; //N�o implematado
  TPDataExp.Visible        := False; //N�o implematado
  EdNome.SetFocus;
end;

procedure TFmFTPWebArqCad.ReopenFTPWebDir(FTPConfig, FTPTipoDir: Integer);
var
  SQLLeftJoin, Passivo: String;
begin
  if FDmkIdApp = 17 then //DControl
  begin
    Passivo     := 'cfg.Passivo';
    SQLLeftJoin := 'LEFT JOIN ftpconfig cfg ON cfg.Codigo = dir.FTPConfig';
  end else
  begin
    Passivo     := 'cfg.Web_FTPpassivo Passivo';
    SQLLeftJoin := 'LEFT JOIN opcoesgerl cfg ON cfg.Codigo = dir.FTPConfig';
  end;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrFTPWebDir, DMod.MyDBn, [
    'SELECT dir.Nivel, dir.CliInt, dir.Codigo, dir.Nome, dir.Caminho,  ',
    'AES_DECRYPT(cfg.Web_FTPs, "' + CO_RandStrWeb01 + '") SENHA,',
    'cfg.Web_FTPh, cfg.Web_FTPu, ' + Passivo + ' ',
    'FROM ftpwebdir dir ',
    SQLLeftJoin,
    'WHERE dir.Ativo = 1 ',
    'AND dir.TipoDir =' + Geral.FF0(FTPTipoDir),
    'AND dir.FTPConfig=' + Geral.FF0(FTPConfig),
    'ORDER BY dir.Ordem, dir.Nome ',
    '']);
end;

function TFmFTPWebArqCad.VerificaSeArquivoExiste(Codigo: Integer; Arquivo: String): Integer;
var
  Query: TmySQLQuery;
begin
  Result := 0;
  //
  try
    Query := TmySQLQuery.Create(Dmod);
    //
    UnDmkDAC_PF.AbreMySQLQuery0(Query, Dmod.MyDBn, [
      'SELECT Codigo ',
      'FROM ftpwebarq ',
      'WHERE Nome = "' + Arquivo + '" ',
      '']);
    if Query.RecordCount > 0 then
    begin
      if Query.FieldByName('Codigo').AsInteger <> Codigo then
        Result := Query.FieldByName('Codigo').AsInteger;
    end;
  finally
    Query.Free;
  end;
end;

procedure TFmFTPWebArqCad.BtExcluiClick(Sender: TObject);
begin
  MyObjects.ExcluiLinhaStringGrid(Grade);
end;

procedure TFmFTPWebArqCad.BtIncluiClick(Sender: TObject);
begin
  //Usa dblocal para ficar mais r�pido
  UFTP.AdicionaUsuarios(Dmod.QrAux, Dmod.MyDB, Grade);
end;

procedure TFmFTPWebArqCad.BtOKClick(Sender: TObject);
var
  Nome, VersaoArq, PastaCam, Tags: String;
  Codigo, FTPDir, Empresa, Nivel, I: Integer;
  DataExp: TDate;
  Entidades: TStringList;
begin
  Codigo    := EdCodigo.ValueVariant;
  Nome      := EdNome.ValueVariant;
  VersaoArq := EdVersaoArq.ValueVariant;
  FTPDir    := EdFTPDir.ValueVariant;
  Empresa   := VUEmpresa.ValueVariant;
  Nivel     := CGNivel.Value;
  Tags      := EdTags.ValueVariant;
  PastaCam  := QrFTPWebDirCaminho.Value;
  DataExp   := TPDataExp.Date;
  //
  if MyObjects.FIC(Nome = '', EdNome, 'Defina uma descri��o!') then Exit;
  if MyObjects.FIC(FTPDir = 0, EdFTPDir, 'Defina o diret�rio!') then Exit;
  if MyObjects.FIC(VerificaSeArquivoExiste(Codigo, Nome) <> 0, EdNome, 'Este arquivo j� foi cadastrado!') then Exit;
  //
  Entidades := TStringList.Create;
  try
    for I := 1 to Grade.RowCount - 1 do
    begin
      Entidades.Add(Grade.Cells[2, I]);
    end;
  except
    Entidades.Free;
  end;
  //
  FNome      := Nome;
  FVersaoArq := VersaoArq;
  FFTPDir    := FTPDir;
  FEmpresa   := Empresa;
  FEntidades := Entidades;
  FNivel     := Nivel;
  FTags      := Tags;
  FPastaCam  := PastaCam;
  FDataExp   := DataExp;
  //
  VAR_COD := Codigo;
  Close;
end;

procedure TFmFTPWebArqCad.BtSaidaClick(Sender: TObject);
begin
  FNome      := '';
  FVersaoArq := '';
  FFTPDir    := 0;
  FEmpresa   := 0;
  FEntidades := nil;
  FNivel     := 0;
  FPastaCam  := '';
  FDataExp   := 0;
  FDesiste   := True;
  //
  Close;
end;

procedure TFmFTPWebArqCad.EdFTPDirChange(Sender: TObject);
var
  Diretorio: Integer;
begin
  Diretorio := EdFTPDir.ValueVariant;
  //
  if Diretorio <> 0 then
  begin
    if EdEmpresa.ValueVariant = 0 then
      VUEmpresa.ValueVariant := QrFTPWebDirCliInt.Value;
    if CGNivel.Value = 0 then
      CGNivel.Value := QrFTPWebDirNivel.Value;
  end;
end;

procedure TFmFTPWebArqCad.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmFTPWebArqCad.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  CBFTPDir.ListSource  := DsFTPWebDir;
  CBEmpresa.ListSource := DModG.DsEmpresas;
  FDesiste             := False;
  //
  UFTP.ConfiguraStringGridUsuarios(Grade);
end;

procedure TFmFTPWebArqCad.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
    [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmFTPWebArqCad.FormShow(Sender: TObject);
begin
  CGNivel.Items.Clear;
  CGNivel.Items.AddStrings(GrlUsuarios.ConfiguraNiveis(FCodigoApp));
  //
  ReopenFTPWebDir(FFTPConfig, FFTPTipoDir);
  //
  MostraEdicao(FCodigo);
end;

end.
