unit UnFTP;

interface

uses dmkGeral, dmkImage, Forms, Controls, Windows, SysUtils, ComCtrls, IdHTTP,
  IdFTPList, UnDmkProcFunc, ExtCtrls, UnInternalConsts, IdFTP, mySQLDbTables,
  DmkDAC_PF, Dialogs, IdFTPCommon, UnDmkEnums, Vcl.StdCtrls, System.Classes,
  UnDmkImg, Vcl.Graphics, frxExportPDF, frxPreview, Grids,
  Variants, IdSSLOpenSSL;

type
  TOrigFTP = (dtPad, dtWOS, dtBak, dtWeb, dtApp, dtWTexto, dtNul);
  TTipoCompFTP = (dtDir, dtArq);
  TUnFTP   = class(TObject)
  private
    procedure ObtemDadosProxy(var ProxUser, ProxPass, ProxServ: String;
                var ProxEnab, ProxPort: Integer);
    function  TraduzOrigem(Origem: TOrigFTP): Integer;
  public
    procedure ReopenFTPWebDir(Configuracao, Codigo, DmkIDApp: Integer;
                Database: TmySQLDatabase; Query: TmySQLQuery);
    procedure ReopenFTPArq(Codigo, Diretorio, CliInt, Depto,
                DmkIDApp: Integer; Nome, Tags: String; Database: TmySQLDatabase;
                Query: TmySQLQuery; LaInfo: TLabel; var AtivaAfterScroll: Boolean);
    procedure DesconectarDoFTP(FTPComp: TIdFTP);
    procedure ReopenFTPConfig(Codigo, DmkIDApp: Integer; QueryFTPCfg: TmySQLQuery;
                DataBase: TmySQLDatabase);
    procedure ImportaConfigFTP(const Form: TForm; var Descricao, URL, Host,
                WebRaiz, Login, Senha: String; var Passivo: Integer);
    function  ConectaServidorFTP(Host, User, Pass: String; Passivo: Boolean;
                FTPComp: TIdFTP): Integer;
    function  InsUpdFTPConfig(SQLTipo: TSQLType; QueryUpd: TmySQLQuery;
                DataBase: TmySQLDatabase; ID: Integer; Nome, Servidor, Usuario,
                Senha, DirRaiz, URL: String; Ativo, Passivo, CfgPadrao: Integer): Integer;
    function  VerificaFTP(Host, User, Pass: String; Passivo: Boolean): Boolean;
    function  ObtemNomeDir(Caminho: String): String;
    function  VerificaSeDiretorioExiste(Query: TmySQLQuery;
                DataBase: TmySQLDatabase; Caminho: String;
                FTPConfig: Integer): Integer;
    function  VerificaSeBibliotecaDeArquivosExiste(Query: TmySQLQuery;
                DataBase: TmySQLDatabase; TipoDir: Integer): Integer;
    function  MontaCaminhoDirFTP(CurDir, NomeDir: String): String;
    function  AtualizaCaminhoDirFTP(Codigo: Integer; Caminho: String;
                QueryUpd: TmySQLQuery): Boolean;
    function  AtualizaArquivoFTP(QueryUpd: TmySQLQuery; Codigo: Integer;
                Arquivo: String): Boolean;
    function  InsUpdRegistroFTPArq(Query, QueryAux: TmySQLQuery;
                DataBase: TmySQLDatabase; DmkIDApp, CliInt, FTPDir,
                Nivel: Integer; Tam: Double; Nome, VersaoArq, Tags,
                ArqNome: String; DataExp: TDate; Origem: TOrigFTP;
                Entidades: TStringList): Integer;
    function  VerificaSeArquivoExiteNoDB(Query: TmySQLQuery;
                DataBase: TmySQLDatabase; DmkIDApp, FTPDir: Integer;
                Arquivo: String): Integer;
    function  CriaDiretorio(Caminho: String; FTPComp: TIdFTP): Boolean;
    function  LocalizaAplicativo(Aplicativo: String; QueryLoc: TmySQLQuery;
                DataBase: TmySQLDatabase): Integer;
    function  VerificaSeArquivoFTPExiteDB(Query: TmySQLQuery; FTPDir: Integer;
                Arquivo: String): Integer;
    function  TestaConexaoFTP(Form: TForm; Host, User, Pass: String; ModoPassivo,
                Avisa: Boolean): Boolean;
    function  ConfiguraTipoDir(): TStringList;
    function  DownloadFileHTTPIndy(AbrirEmAba: Boolean; InOwner: TWincontrol;
                Pager: TWinControl; URL, Arquivo: String;
                Destino: String = ''): Boolean;
    function  DownloadFileIndy(InOwner: TWincontrol;
                Pager: TWinControl; SaveFile: TSaveDialog;
                Caminho, Arquivo, Destino, Host, User, Pass: String;
                Passivo: Boolean): String;
    function  UploadFTP(InOwner: TWincontrol; Pager: TWinControl;
                PastaCam, Host, User, Pass: String; Passivo, Delete: Boolean;
                Arquivos: TStrings; InsereDataHora: Boolean = False): String; overload;
    function  UploadFTP(PastaCam, Host, User, Pass: String; Passivo, Delete: Boolean;
                Arquivos: TStrings; InsereDataHora: Boolean = False): String; overload;
    function  UploadFTP_Fotos(InOwner: TWincontrol;
                Pager: TWinControl; PastaCam, Host, User,
                Pass: String; Passivo, Delete: Boolean; Arquivos: TStrings;
                DmkIDApp, Empresa, Depto, FTPDir, Nivel: Integer; Origem: TOrigFTP;
                Query: TmySQLQuery; DataBase: TmySQLDatabase;
                InsereDataHora: Boolean = False): String;
    function  UploadFTP_DB(InOwner: TWincontrol;
                Pager: TWinControl; SQLType: TSQLType; ArqNome,
                Nome, VersaoArq, Tags: String; Codigo, DmkIDApp, FTPConfig,
                FTPTipoDir, FTPDir, Empresa, Nivel: Integer;
                Origem: TOrigFTP; TamArq: Double; DataExp: TDate;
                Arquivos: TStrings; MostraJanelaInsUpd: Boolean;
                Query, QueryAux: TmySQLQuery; DataBase: TmySQLDatabase;
                var PastaCam: String; Delete: Boolean = False): Integer;
    function  InsUpdFTPWebDir(QueryAux, QueryUpd: TmySQLQuery;
                Codigo, FTPConfig, TipoDir, CliInt, Nivel: Integer; Nome,
                Pasta, Caminho: String; Entidades: TStringList): Integer;
    {$IfDef UsaModuloFTP}
    procedure MostraFTPWebArqCad(SQLType: TSQLType; Codigo, CodigoApp,
                FTPConfig, FTPTipoDir, DmkIDApp: Integer; var Nome, VersaoArq,
                PastaCam, Tags: String; var FTPDir, Empresa, Nivel: Integer;
                var DataExp: TDate; var Entidades: TStringList;
                var Desiste: Boolean);
    {$EndIf}
    {$IfDef TEM_UH and TEM_DBWEB}
    procedure MostraUploadsIts(SQLType: TSQLType; Codigo: Integer; var Nome,
                PastaCam, Tags: String; var FTPDir, Empresa: Integer;
                var DataExp: TDate; var Desiste: Boolean);
    {$EndIf}
    procedure ReopenDepto(DataBase: TmySQLDatabase; Query: TmySQLQuery;
                CodigoApp, Empresa: Integer);
    function  DeletaArquvivoFTP(Arquivo, Caminho, Host, User, Pass: String;
                Passivo, FTPWebArq: Integer; Origem: TOrigFTP;
                DataBase: TmySQLDatabase; ExcluiComOrigem: Boolean = False): Boolean;
    function  VerificaSeArquivoExisteNoFTP(FTPComp: TIdFTP; Arquivo, Host, User,
                Pass: String; Passivo: Boolean; NaoDesconectar: Boolean = False): Boolean;
    function  VerificaSeDiretorioExisteNoFTP(FTPComp: TIdFTP; Diretorio, Host, User,
                Pass: String; Passivo: Boolean; NaoDesconectar: Boolean = False): Boolean;
    function  VerificaSeDiretorioEstaVazio(FTPComp: TIdFTP; Diretorio, Host, User,
                Pass: String; Passivo: Boolean; NaoDesconectar: Boolean = False): Boolean;
    function  FTPDeleteArquivo(FTPComp: TIdFTP; Caminho, Arquivo, Host,
                User, Pass: String; Passivo: Boolean;
                NaoDesconectar: Boolean = False): Boolean;
    function  ConverteOrigem(Origem: Integer): TOrigFTP;
    function  ObterLinkArquivo(Web_MyURL, WebId, Arquivo: String; DMKID_APP: Integer): String;
    function  RetiraNomeFTPList(Item: String): String;
    function  RetiraTamFTPList(Item: String): String;
    function  RetiraDataFTPList(Item: String): String;
    function  AdicionaUsuarioComp(Tipo: TTipoCompFTP; Codigo: Integer;
                QueryUpd, QueryAux: TmySQLQuery; DataBase: TmySQLDatabase): Boolean;
    function  ExcluiUsuarioComp(Controle: Integer; DataBase: TmySQLDatabase): Boolean;
    procedure CarregaMiniatura(const FTPConfig: Integer; const Imagem: String;
                const FTPGer: TIdFTP; var Img: TImage);
    procedure PublicaRelatorioFRXnaWeb(DMKID_APP: Integer; HabilModulos: String;
                DBn: TmySQLDatabase; QryWebParams, QueryUpdN,
                QueryAux: TmySQLQuery; frxPDFExport: TfrxPDFExport;
                frxPreview: TfrxPreview);
    procedure ConfiguraStringGridUsuarios(Grade: TStringGrid);
    procedure AdicionaUsuarios(Query: TmySQLQuery; DataBase: TmySQLDatabase;
                Grade: TStringGrid);
    procedure ArqDirCompartilhaEntidades(Codigo: Integer; Tipo: TTipoCompFTP;
                Entidades: TStringList; QueryAux, QueryUpd: TmySQLQuery);
    procedure ReopenFTPCom(Tipo: TTipoCompFTP; Codigo: Integer;
                DataBase: TmySQLDatabase; Query: TmySQLQuery);
  end;

var
  UFTP: TUnFTP;
const
  CO_FTP_DIRPAD = '_local_';

implementation

uses UMySQLModule, UnMyObjects, FTPUploads, MyDBCheck, UnGrl_Consts,
  {$IfDef UsaModuloFTP} FTPWebArqCad, {$EndIf}
  {$IfDef TEM_UH and TEM_DBWEB} UploadsIts, {$EndIf} UnDmkWeb;

{ TUnFTP }

function TUnFTP.RetiraNomeFTPList(Item: String): String;
var
  I: Integer;
begin
  Result := '';
  //
  for I := Length(Item) downto 1 do
  begin
    if Item[I] <> ';' then
      Result := Item[I] + Result
    else
      Break;
  end;
  Result := Trim(Result);
end;

function TUnFTP.RetiraDataFTPList(Item: String): String;
var
  I: Integer;
  Txt: String;
  Data: TDateTime;
begin
  I      := pos('modify=', Lowercase(Item)) + 7;
  Txt    := Copy(Item, I);
  I      := pos(';', Txt) - 1;
  Txt    := Copy(Txt, 1, I);
  Data   := dmkPF.FormataTimeStamp(Txt);
  Result := Trim(Geral.FDT(Data, 0));
end;

function TUnFTP.RetiraTamFTPList(Item: String): String;
var
  I: Integer;
  Tam: Double;
  Txt: String;
begin
  I   := pos('size=', Lowercase(Item)) + 5;
  Txt := Copy(Item, I);
  I   := pos(';', Txt) - 1;
  Txt := Copy(Txt, 1, I);
  Tam := Geral.IMV(Txt);
  //
  if Tam <> 0 then
    Tam := (Tam / 1024) / 1024;
  //
  Result := Trim(Geral.FFT(Tam, 3, siPositivo) + ' MB');
end;

function TUnFTP.FTPDeleteArquivo(FTPComp: TIdFTP; Caminho, Arquivo, Host,
  User, Pass: String; Passivo: Boolean; NaoDesconectar: Boolean = False): Boolean;
var
  Cam: String;
begin
  try
    Result := False;
    //
    Cam := Caminho + '/' + Arquivo;
    //
    if not FTPComp.Connected then
      UFTP.ConectaServidorFTP(Host, User, Pass, Passivo, FTPComp);
    //
    if FTPComp.Connected then
    begin
      FTPComp.Delete(Cam);
      //
      Result := True;
    end;
  finally
    if not FTPComp.Connected then
      DesconectarDoFTP(FTPComp);
  end;
end;

function TUnFTP.VerificaSeDiretorioEstaVazio(FTPComp: TIdFTP; Diretorio, Host,
  User, Pass: String; Passivo, NaoDesconectar: Boolean): Boolean;

  function FileFTPExiste(Diretorio: String): Boolean;
  var
    I: Integer;
    DirList: TStringList;
  begin
    Result  := False;
    DirList := TStringList.Create;
    //
    FTPComp.List(DirList, Diretorio, True);
    //
    if DirList.Count > 0 then
    begin
      for I := 0 to DirList.Count - 1 do
      begin
        if (pos('type=cdir;', DirList[I]) = 0) and (pos('type=pdir;', DirList[I]) = 0) then
        begin
          Result := True;
          Exit;
        end;
      end;
    end;
    //
    DirList.Free;
  end;

begin
  try
    Result := True;
    //
    if not FTPComp.Connected then
      ConectaServidorFTP(Host, User, Pass, Passivo, FTPComp);
    //
    Result := not FileFTPExiste(Diretorio);
  finally
    if not NaoDesconectar then
      UFTP.DesconectarDoFTP(FTPComp);
  end;
end;

function TUnFTP.VerificaSeArquivoExisteNoFTP(FTPComp: TIdFTP; Arquivo, Host,
  User, Pass: String; Passivo, NaoDesconectar: Boolean): Boolean;
var
  Lista: TStringList;
  I, K: Integer;
begin
  try
    Result := False;
    //
    if not FTPComp.Connected then
      ConectaServidorFTP(Host, User, Pass, Passivo, FTPComp);
    //
    Lista := TStringList.Create;
    try
      FTPComp.List(Lista, '', True);
      //
      for I := 0 to Lista.Count - 1 do
      begin
        K := pos('type=file;', Lista[I]);
        //
        if K > 0 then
        begin
          if RetiraNomeFTPList(Lista[I]) = Arquivo then
          begin
            Result := True;
            Exit;
          end;
        end;
      end;
    finally
      Lista.Free;
    end;
  finally
    if not NaoDesconectar then
      UFTP.DesconectarDoFTP(FTPComp);
  end;
end;

function TUnFTP.VerificaSeDiretorioExisteNoFTP(FTPComp: TIdFTP; Diretorio, Host,
  User, Pass: String; Passivo: Boolean; NaoDesconectar: Boolean = False): Boolean;

  function DiretorioExiste(Dir: String): Boolean;
  var
    Lista: TStringList;
    I, K: Integer;
    DirAtu: String;
  begin
    Result := False;
    //
    Lista := TStringList.Create;
    try
      FTPComp.List(Lista, '', True);
      //
      for I := 0 to Lista.Count - 1 do
      begin
        K := pos('type=dir;', Lista[I]);
        //
        if K > 0 then
        begin
          DirAtu := RetiraNomeFTPList(Lista[I]);
          //
          if DirAtu = Dir then
          begin
            Result := True;
            Exit;
          end;
        end;
      end;
    finally
      Lista.Free;
    end;
  end;

var
  Diretorios: TStringList;
  I: Integer;
  Cam: String;
begin
  Result := True;
  //
  try
    if not FTPComp.Connected then
      ConectaServidorFTP(Host, User, Pass, Passivo, FTPComp);
    //
    Diretorios := TStringList.Create;
    //
    try
      Diretorios := Geral.Explode(Diretorio, '/', 1);
      //
      for I := 0 to Diretorios.Count - 1 do
      begin
        if I = 0 then
          Cam := '/' + Diretorios[I]
        else
          Cam := Cam + '/' + Diretorios[I];
        //
        FTPComp.ChangeDir(Cam);
        //
        if I < Diretorios.Count - 1 then
        begin
          if not DiretorioExiste(Diretorios[I + 1]) then
          begin
            Result := False;
            Exit;
          end;
        end;
      end;
    finally
      Diretorios.Free;
    end;
  finally
    if not NaoDesconectar then
      UFTP.DesconectarDoFTP(FTPComp);
  end;
end;

function TUnFTP.VerificaFTP(Host, User, Pass: String;
  Passivo: Boolean): Boolean;
var
  FTP: TIdFTP;
  ProxServ, ProxUser, ProxPass: String;
  ProxEnab, ProxPort: Integer;
begin
  //True  = Conectou
  //False = N�o conectou
  //
  FTP    := TIdFTP.Create();
  Result := False;
  //
  ObtemDadosProxy(ProxUser, ProxPass, ProxServ, ProxEnab, ProxPort);
  //
  try
    if ProxEnab = 0 then
    begin
      FTP.ProxySettings.Password := '';
      FTP.ProxySettings.Port     := 0;
      FTP.ProxySettings.Host     := '';
      FTP.ProxySettings.UserName := '';
    end else
    begin
      FTP.ProxySettings.Password := ProxPass;
      FTP.ProxySettings.Port     := ProxPort;
      FTP.ProxySettings.Host     := ProxServ;
      FTP.ProxySettings.UserName := ProxUser;
    end;
    FTP.Host     := Host;
    FTP.Username := User;
    FTP.Password := Pass;
    FTP.Passive  := Passivo;
    FTP.UseHOST  := False;
    //
    FTP.Connect;
    //
    if FTP.Connected then
      Result := True;
    //
    FTP.Disconnect;
  finally
    FTP.Destroy;
  end;
end;

function TUnFTP.InsUpdFTPWebDir(QueryAux, QueryUpd: TmySQLQuery;
  Codigo, FTPConfig, TipoDir, CliInt, Nivel: Integer; Nome, Pasta,
  Caminho: String; Entidades: TStringList): Integer;
var
  SQLTipo: TSQLType;
  Cod: Integer;
begin
  Result := 0;
  //
  if Codigo = 0 then
  begin
    SQLTipo := stIns;
    Cod     := UMyMod.BuscaNovoCodigo_Int(QueryUpd, 'ftpwebdir', 'Codigo', [],
                 [], stIns, 0, siPositivo, nil);
  end else
  begin
    SQLTipo := stUpd;
    Cod     := Codigo;
  end;
  //
  if UMyMod.SQLInsUpd(QueryUpd, SQLTipo, 'ftpwebdir', False, ['Nome', 'Pasta',
    'Caminho', 'FTPConfig', 'TipoDir', 'CliInt', 'Nivel'], ['Codigo'],
    [Nome, Pasta, Caminho, FTPConfig, TipoDir, CliInt, Nivel], [Cod],
    True) then
  begin
    if SQLTipo = stIns then
      ArqDirCompartilhaEntidades(Cod, dtDir, Entidades, QueryAux, QueryUpd);
    //
    Result := Cod;
  end;
end;

function TUnFTP.InsUpdFTPConfig(SQLTipo: TSQLType; QueryUpd: TmySQLQuery;
  DataBase: TmySQLDatabase; ID: Integer; Nome, Servidor, Usuario, Senha,
  DirRaiz, URL: String; Ativo, Passivo, CfgPadrao: Integer): Integer;
var
  Codigo: Integer;
  Crypt: String;
begin
  Result := 0;
  Crypt  := CO_RandStrWeb01;
  //
  if SQLTipo =  stIns then
    Codigo := UMyMod.BuscaNovoCodigo_Int(QueryUpd, 'ftpconfig', 'Codigo',
      [], [], stIns, 0, siPositivo, nil)
  else
    Codigo := ID;
  //
  QueryUpd.Close;
  QueryUpd.SQL.Clear;
  if SQLTipo =  stIns then
    QueryUpd.SQL.Add('INSERT INTO ftpconfig SET ')
  else
    QueryUpd.SQL.Add('UPDATE ftpconfig SET ');
  //
  QueryUpd.SQL.Add('Nome=:P0, Web_FTPh=:P1, Web_Raiz=:P2, ');
  QueryUpd.SQL.Add('Web_FTPu=:P3, Web_FTPs=AES_ENCRYPT(:P4, :P5), ');
  QueryUpd.SQL.Add('Web_MyURL=:P6, Passivo=:P7, CfgPadrao=:P8, Ativo=:P9, ');
  //
  if SQLTipo = stIns then
    QueryUpd.SQL.Add('DataCad=:Px, UserCad=:Py, Codigo=:Pz')
  else
    QueryUpd.SQL.Add('DataAlt=:Px, UserAlt=:Py WHERE Codigo=:Pz');
  //
  QueryUpd.Params[00].AsString  := Nome;
  QueryUpd.Params[01].AsString  := Servidor;
  QueryUpd.Params[02].AsString  := DirRaiz;
  QueryUpd.Params[03].AsString  := Usuario;
  QueryUpd.Params[04].AsString  := Senha;
  QueryUpd.Params[05].AsString  := Crypt;
  QueryUpd.Params[06].AsString  := URL;
  QueryUpd.Params[07].AsInteger := Passivo;
  QueryUpd.Params[08].AsInteger := CfgPadrao;
  QueryUpd.Params[09].AsInteger := Ativo;
  QueryUpd.Params[10].AsString  := FormatDateTime(VAR_FORMATDATE, Date);
  QueryUpd.Params[11].AsInteger := VAR_USUARIO;
  QueryUpd.Params[12].AsInteger := Codigo;
  QueryUpd.ExecSQL;
  //
  if CfgPadrao = 1 then
  begin
    UnDmkDAC_PF.ExecutaMySQLQuery0(QueryUpd, DataBase, [
      'UPDATE ftpconfig SET CfgPadrao=0 ',
      'WHERE Codigo <> ' + Geral.FF0(Codigo),
      '']);
  end;
  Result := Codigo;
end;

procedure TUnFTP.ImportaConfigFTP(const Form: TForm; var Descricao, URL, Host,
  WebRaiz, Login, Senha: String; var Passivo: Integer);
var
  Arquivo, Linha, Pass: String;
  F: TextFile;
  i, j, Num: Integer;
begin
  if MyObjects.FileOpenDialog(Form, '', '', 'Abrir', '*.llm', [], Arquivo) then
  begin
    i := 0;
    //
    if ExtractFileExt(Arquivo) = '.llm' then
    begin
      AssignFile(F, Arquivo);
      Reset(F);
      while not Eof(F) do
      begin
        i := i + 1;
        Readln(F, Linha);
        //
        case i of
           6: Descricao := Linha;
           8: Host      := Linha;
          10: WebRaiz   := Linha;
          12: URL       := Linha;
          14: Passivo   := Geral.IMV(Linha);
          16: Login     := Linha;
          18:
          begin
            if Length(Linha) > 0 then
            begin
              for j := 1 to Trunc(Length(Linha) / 3) do
              begin
                try
                  Num  := Geral.IMV(Copy(Linha, 1 + ((j-1) * 3), 3));
                  Pass := Pass + (Char(Num));
                except

                end;
              end;
              Linha := dmkPF.Criptografia(Pass, CO_RandStrWeb01);
              Senha := Linha;
            end;
          end;
        end;
      end;
      CloseFile(f);
    end else
      Geral.MB_Aviso('Arquivo inv�lido!');
  end;
end;

function TUnFTP.VerificaSeArquivoExiteNoDB(Query: TmySQLQuery;
  DataBase: TmySQLDatabase; DmkIDApp, FTPDir: Integer; Arquivo: String): Integer;
begin
  if DmkIDApp = 4 then //Syndic
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(Query, DataBase, [
      'SELECT Codigo',
      'FROM uploads',
      'WHERE DirWeb=' + Geral.FF0(FTPDir),
      'AND Arquivo="' + Arquivo + '"',
      '']);
  end else
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(Query, DataBase, [
      'SELECT Codigo',
      'FROM ftpwebarq',
      'WHERE FTPDir=' + Geral.FF0(FTPDir),
      'AND Arquivo="' + Arquivo + '"',
      '']);
  end;
  if Query.RecordCount > 0 then
    Result := Query.FieldByName('Codigo').Value
  else
    Result := 0;
end;

function TUnFTP.VerificaSeArquivoFTPExiteDB(Query: TmySQLQuery; FTPDir: Integer;
  Arquivo: String): Integer;
begin
  Query.Close;
  Query.SQL.Clear;
  Query.SQL.Add('SELECT Codigo');
  Query.SQL.Add('FROM ftpwebarq');
  Query.SQL.Add('WHERE FTPDir=:P0');
  Query.SQL.Add('AND Arquivo=:P1');
  Query.Params[0].AsInteger := FTPDir;
  Query.Params[1].AsString  := Arquivo;
  UnDmkDAC_PF.AbreQueryApenas(Query);
  if Query.RecordCount > 0 then
    Result := Query.FieldByName('Codigo').Value
  else
    Result := 0;
end;

function TUnFTP.VerificaSeDiretorioExiste(Query: TmySQLQuery;
  DataBase: TmySQLDatabase; Caminho: String; FTPConfig: Integer): Integer;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(Query, DataBase, [
    'SELECT Codigo ',
    'FROM ftpwebdir ',
    'WHERE FTPConfig=' + Geral.FF0(FTPConfig),
    'AND Caminho="' + Caminho + '"',
    '']);
  if Query.RecordCount > 0 then
    Result := Query.FieldByName('Codigo').AsInteger //Diret�rio existe
  else
    Result := 0; //Diret�rio n�o existe
end;

function TUnFTP.VerificaSeBibliotecaDeArquivosExiste(Query: TmySQLQuery;
  DataBase: TmySQLDatabase; TipoDir: Integer): Integer;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(Query, DataBase, [
    'SELECT Codigo ',
    'FROM ftpwebdir ',
    'WHERE TipoDir=' + Geral.FF0(TipoDir),
    '']);
  if Query.RecordCount > 0 then
    Result := Query.FieldByName('Codigo').AsInteger //Diret�rio existe
  else
    Result := 0; //Diret�rio n�o existe
end;

function TUnFTP.AdicionaUsuarioComp(Tipo: TTipoCompFTP; Codigo: Integer;
  QueryUpd, QueryAux: TmySQLQuery; DataBase: TmySQLDatabase): Boolean;
const
  Aviso  = '...';
  Titulo = 'Sele��o de entidade';
  Prompt = 'Informe a entidade: [F7 para pesquisar]';
  Campo  = 'Descricao';
var
  Entidade: Variant;
  Controle, Tipo_Int: Integer;
begin
  Result := False;
  //
  Entidade := DBCheck.EscolheCodigoUnico(Aviso, Titulo, Prompt, nil, nil, Campo, 0, [
    'SELECT Codigo, IF(Tipo=0, RazaoSocial, Nome) ' + Campo,
    'FROM entidades ',
    'WHERE Codigo <> 0',
    'ORDER BY ' + Campo,
    ''], DataBase, True);
  //
  if Entidade <> Null then
  begin
    case tipo of
      dtDir: Tipo_Int := 0;
      dtArq: Tipo_Int := 1;
    end;
    //
    Controle := UMyMod.BuscaNovoCodigo_Int(QueryAux, 'ftpwebcom', 'Controle',
      [], [], stIns, 0, siPositivo, nil);
    //
    Result := UMyMod.SQLInsUpd(QueryUpd, stIns, 'ftpwebcom', False,
                ['Entidade', 'Tipo', 'Codigo'], ['Controle'],
                [Entidade, Tipo_Int, Codigo], [Controle], True);
  end;
end;

procedure TUnFTP.AdicionaUsuarios(Query: TmySQLQuery; DataBase: TmySQLDatabase;
  Grade: TStringGrid);
const
  Aviso  = '...';
  Titulo = 'Sele��o de entidade';
  Prompt = 'Informe a entidade: [F7 para pesquisar]';
  Campo  = 'Descricao';
var
  Entidade: Variant;
  Row: Integer;
  Nome: String;
begin
  Entidade := DBCheck.EscolheCodigoUnico(Aviso, Titulo, Prompt, nil, nil, Campo, 0, [
    'SELECT Codigo, IF(Tipo=0, RazaoSocial, Nome) ' + Campo,
    'FROM entidades ',
    'WHERE Codigo <> 0',
    'ORDER BY ' + Campo,
    ''], DataBase, True);
  //
  if Entidade <> Null then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(Query, DataBase, [
      'SELECT Codigo, IF(Tipo=0, RazaoSocial, Nome) Nome ',
      'FROM entidades ',
      'WHERE Codigo=' + Geral.FF0(Entidade),
      '']);
    if Query.RecordCount > 0 then
    begin
      Nome := Query.FieldByName('Nome').AsString;
      //
      if (Grade.RowCount = 2) and (Length(Grade.Cells[1, 1]) = 0) then
      begin
        Grade.Cells[1, 1] := Nome;
        Grade.Cells[2, 1] := Geral.FF0(Entidade);
      end else
      begin
        Row := Grade.RowCount + 1;
        //
        Grade.Cells[1, Row - 1] := Nome;
        Grade.Cells[2, Row - 1] := Geral.FF0(Entidade);
        Grade.RowCount := Row;
      end;
    end;
  end;
end;

procedure TUnFTP.ArqDirCompartilhaEntidades(Codigo: Integer; Tipo: TTipoCompFTP;
  Entidades: TStringList; QueryAux, QueryUpd: TmySQLQuery);
var
  Controle, Entidade, Tipo_Int, I: Integer;
begin
  if (QueryAux <> nil) and (Entidades <> nil) then
  begin
    if Codigo = 0 then
    begin
      Geral.MB_Aviso('Codigo n�o informado na fun��o: "ArqDirCompartilhaEntidades"');
      Exit;
    end;
    //
    case Tipo of
      dtDir: Tipo_Int := 0;
      dtArq: Tipo_Int := 1;
    end;
    //
    if (Entidades <> nil) and (Entidades.Count > 0) then
    begin
      for I := 0 to Entidades.Count - 1 do
      begin
        Entidade := Geral.IMV(Entidades[I]);
        //
        if Entidade <> 0 then
        begin
          Controle := UMyMod.BuscaNovoCodigo_Int(QueryAux, 'ftpwebcom', 'Controle',
            [], [], stIns, 0, siPositivo, nil);
          //
          if not UMyMod.SQLInsUpd(QueryUpd, stIns, 'ftpwebcom', False,
            ['Entidade', 'Tipo', 'Codigo'], ['Controle'],
            [Entidade, Tipo_Int, Codigo], [Controle], True)
          then
            Geral.MB_Aviso('Falha ao compartilhar com a entidade ID ' + Geral.FF0(Entidade));
        end;
      end;
    end;
  end;
end;

function TUnFTP.AtualizaArquivoFTP(QueryUpd: TmySQLQuery; Codigo: Integer;
  Arquivo: String): Boolean;
begin
  if UMyMod.SQLInsUpd(QueryUpd, stUpd, 'ftpwebarq', False,
    ['Arquivo'], ['Codigo'], [Arquivo], [Codigo], True) 
  then
    Result := True
  else
    Result := False;
end;

function TUnFTP.AtualizaCaminhoDirFTP(Codigo: Integer;
  Caminho: String; QueryUpd: TmySQLQuery): Boolean;
var
  Pasta: String;
begin
  Pasta := ObtemNomeDir(Caminho);
  //
  if UMyMod.SQLInsUpd(QueryUpd, stUpd, 'ftpwebdir', False,
    ['Pasta', 'Caminho'], ['Codigo'], [Pasta, Caminho], [Codigo], True)
  then
    Result := True
  else
    Result := False;
end;

function TUnFTP.TestaConexaoFTP(Form: TForm; Host, User, Pass: String;
  ModoPassivo, Avisa: Boolean): Boolean;
var
  ProxServ, ProxUser, ProxPass: String;
  ProxEnab, ProxPort: Integer;
  IdFTP: TIdFTP;
begin
  IdFTP := TIdFTP.Create(Form);
  try
    try
      if IdFTP.Connected then
        IdFTP.Disconnect;
      //
      ObtemDadosProxy(ProxUser, ProxPass, ProxServ, ProxEnab, ProxPort);
      //
      if ProxEnab = 0 then
      begin
        IdFTP.ProxySettings.Password := '';
        IdFTP.ProxySettings.Port     := 0;
        IdFTP.ProxySettings.Host     := '';
        IdFTP.ProxySettings.UserName := '';
      end else
      begin
        IdFTP.ProxySettings.Password := ProxPass;
        IdFTP.ProxySettings.Port     := ProxPort;
        IdFTP.ProxySettings.Host     := ProxServ;
        IdFTP.ProxySettings.UserName := ProxUser;
      end;
      IdFTP.Host     := Host;
      IdFTP.Username := User;
      IdFTP.Password := Pass;
      IdFTP.Passive  := ModoPassivo;
      IdFTP.UseHOST  := False;
      IdFTP.Connect;
    finally
      Result := IdFTP.Connected;
      if Avisa then
      begin
        if Result then
          Geral.MB_Info('Conex�o FTP com o serivdor ' + Host +
            ' estabelecida com �xito!')
        else
          Geral.MB_Aviso('N�o foi poss�vel abrir conex�o FTP com o servidor ' +
            Host + '!');
      end;
      IdFTP.Disconnect;
    end;
  finally
    IdFtp.Free;
  end;
end;

function TUnFTP.ConverteOrigem(Origem: Integer): TOrigFTP;
begin
  case Origem of
      0: Result := dtPad; //Padr�o gerenciador de FTP
      1: Result := dtWOS; //Ordem de servi�os
      2: Result := dtBak; //Backup de diret�rios
      3: Result := dtWeb; //Arquivos gerenciados na web
      4: Result := dtApp; //Aplicativos Dermatek
    else Result := dtNul; //N�o localizou
  end;
end;

function TUnFTP.TraduzOrigem(Origem: TOrigFTP): Integer;
begin
  case Origem of
    dtPad:    Result :=  0; //Padr�o gerenciador de FTP
    dtWOS:    Result :=  1; //Ordem de servi�os
    dtBak:    Result :=  2; //Backup de diret�rios
    dtWeb:    Result :=  3; //Arquivos gerenciados na web
    dtApp:    Result :=  4; //Aplicativos Dermatek
    dtWTexto: Result :=  5; //Textos WEB
    else      Result := -1; //N�o definido
  end;
end;

function TUnFTP.UploadFTP(InOwner: TWincontrol; Pager: TWinControl;
  PastaCam, Host, User, Pass: String; Passivo, Delete: Boolean;
  Arquivos: TStrings; InsereDataHora: Boolean = False): String;
var
  Form: TForm;
  I, TotArq: Integer;
  Nome, ArqNome: String;
  Continua: Boolean;
begin
  Result := '';
  Form   := MyObjects.FormTDICria(TFmFTPUploads, InOwner, Pager, True, False);
  //
  ConectaServidorFTP(Host, User, Pass, Passivo, TFmFTPUploads(Form).IdFTP1);
  //
  if TFmFTPUploads(Form).IdFTP1.Connected then
  begin
    TotArq := Arquivos.Count;
    //
    TFmFTPUploads(Form).LaArquivosTot.Visible  := True;
    TFmFTPUploads(Form).PBArquivosTot.Visible  := True;
    TFmFTPUploads(Form).LaArquivosTot.Caption  := 'Arquivo 1 de ' + Geral.FF0(TotArq);
    TFmFTPUploads(Form).PBArquivosTot.Position := 0;
    TFmFTPUploads(Form).PBArquivosTot.Max      := TotArq;
    //
    try
      for I := 0 to TotArq -1 do
      begin
        Nome    := Arquivos.Strings[I];
        ArqNome := ExtractFileName(Nome);
        ArqNome := Geral.SemAcento(ArqNome);
        ArqNome := StringReplace(ArqNome, ' ', '_', [rfReplaceAll, rfIgnoreCase]);
        //
        if InsereDataHora then
          ArqNome := FormatDateTime('yymmdd_hhnnss', Now) + '_' + ArqNome;
        //
        //Altera para o diret�rio correto
        TFmFTPUploads(Form).IdFTP1.ChangeDir(PastaCam);
        //
        //Verifica se arquivo existe no servidor
        if VerificaSeArquivoExisteNoFTP(TFmFTPUploads(Form).IdFTP1, ArqNome,
          Host, User, Pass, Passivo, True) then
        begin
          if Geral.MB_Pergunta('J� existe um arquivo com este nome no diret�rio selecionado do servidor!' +
            sLineBreak + 'Deseja substitu�-lo?') <> ID_YES
          then
            Continua := False
          else
            Continua := True;
        end else
          Continua := True;
        //
        TFmFTPUploads(Form).LaArquivoAtu.Caption   := 'Copiando arquivo "' + ArqNome + '"';
        TFmFTPUploads(Form).LaArquivosTot.Caption  := 'Arquivo ' + Geral.FF0(I + 1) + ' de ' + Geral.FF0(TotArq);
        TFmFTPUploads(Form).PBArquivosTot.Position := TFmFTPUploads(Form).PBArquivosTot.Position + 1;
        TFmFTPUploads(Form).PBArquivosTot.Update;
        Application.ProcessMessages;
        //
        if Continua then
        begin
          //Faz o upload
          TFmFTPUploads(Form).IdFTP1.Put(Nome, ArqNome);
          try
            if Delete = True then
            begin
              DeleteFile(Nome);
            end;
          finally

          end;
          Result := ArqNome;
        end;
      end;
    finally
      DesconectarDoFTP(TFmFTPUploads(Form).IdFTP1);
      //
      if Form <> nil then
        MyObjects.FormTDIFecha(Form, TTabSheet(TFmFTPUploads(Form).CSTabSheetChamou.Component));
    end;
  end;
end;

function TUnFTP.UploadFTP(PastaCam, Host, User, Pass: String; Passivo,
  Delete: Boolean; Arquivos: TStrings; InsereDataHora:
  Boolean = False): String;
var
  Form: TForm;
  I, TotArq: Integer;
  Nome, ArqNome: String;
  Continua: Boolean;
begin
  Result := '';
  //
  if DBCheck.CriaFm(TFmFTPUploads, FmFTPUploads, afmoNegarComAviso) then
  begin
    FmFTPUploads.Show;
    ConectaServidorFTP(Host, User, Pass, Passivo, FmFTPUploads.IdFTP1);
    //
    if FmFTPUploads.IdFTP1.Connected then
    begin
      TotArq := Arquivos.Count;
      //
      FmFTPUploads.LaArquivosTot.Visible  := True;
      FmFTPUploads.PBArquivosTot.Visible  := True;
      FmFTPUploads.LaArquivosTot.Caption  := 'Arquivo 1 de ' + Geral.FF0(TotArq);
      FmFTPUploads.PBArquivosTot.Position := 0;
      FmFTPUploads.PBArquivosTot.Max      := TotArq;
      //
      try
        for I := 0 to TotArq -1 do
        begin
          Nome    := Arquivos.Strings[I];
          ArqNome := ExtractFileName(Nome);
          ArqNome := Geral.SemAcento(ArqNome);
          ArqNome := StringReplace(ArqNome, ' ', '_', [rfReplaceAll, rfIgnoreCase]);
          //
          if InsereDataHora then
            ArqNome := FormatDateTime('yymmdd_hhnnss', Now) + '_' + ArqNome;
          //
          //Altera para o diret�rio correto
          FmFTPUploads.IdFTP1.ChangeDir(PastaCam);
          //
          //Verifica se arquivo existe no servidor
          if VerificaSeArquivoExisteNoFTP(FmFTPUploads.IdFTP1, ArqNome, Host,
            User, Pass, Passivo, True) then
          begin
            if Geral.MB_Pergunta('J� existe um arquivo com este nome no diret�rio selecionado do servidor!' +
              sLineBreak + 'Deseja substitu�-lo?') <> ID_YES
            then
              Continua := False
            else
              Continua := True;
          end else
            Continua := True;
          //
          FmFTPUploads.LaArquivoAtu.Caption   := 'Copiando arquivo "' + ArqNome + '"';
          FmFTPUploads.LaArquivosTot.Caption  := 'Arquivo ' + Geral.FF0(I + 1) + ' de ' + Geral.FF0(TotArq);
          FmFTPUploads.PBArquivosTot.Position := FmFTPUploads.PBArquivosTot.Position + 1;
          FmFTPUploads.PBArquivosTot.Update;
          Application.ProcessMessages;
          //
          if Continua then
          begin
            //Faz o upload
            FmFTPUploads.IdFTP1.Put(Nome, ArqNome);
            try
              if Delete = True then
              begin
                DeleteFile(Nome);
              end;
            finally

            end;
            Result := ArqNome;
          end;
        end;
      finally
        DesconectarDoFTP(FmFTPUploads.IdFTP1);
        //
        FmFTPUploads.Close;
      end;
    end;
  end;
end;

function TUnFTP.UploadFTP_DB(InOwner: TWincontrol;
  Pager: TWinControl; SQLType: TSQLType; ArqNome, Nome, VersaoArq,
  Tags: String; Codigo, DmkIDApp, FTPConfig, FTPTipoDir, FTPDir, Empresa,
  Nivel: Integer; Origem: TOrigFTP; TamArq: Double; DataExp: TDate;
  Arquivos: TStrings; MostraJanelaInsUpd: Boolean; Query, QueryAux: TmySQLQuery;
  DataBase: TmySQLDatabase; var PastaCam: String; Delete: Boolean = False): Integer;
var
  SQLTyp: TTypeSQL;
  Msg, ArquivoNome, Host, Usuario, Senha: String;
  Res, Passivo: Integer;
  Tam: Double;
  Desiste: Boolean;
  Entidades: TStringList;
begin
  Result  := 0;
  Res     := 0;
  Desiste := False;
  //
  if (Arquivos <> nil) and (Arquivos.Count > 1) then
  begin
    Geral.MB_Aviso('Voc� deve selecionar apenas um arquivo!');
    Exit;
  end;
  Entidades := TStringList.Create;
  //
  if MostraJanelaInsUpd then
  begin
    {$IfDef UsaModuloFTP}
    MostraFTPWebArqCad(SQLType, Codigo, DmkIDApp, FTPConfig, FTPTipoDir,
      DmkIDApp, Nome, VersaoArq, PastaCam, Tags, FTPDir, Empresa, Nivel,
      DataExp, Entidades, Desiste);
    {$EndIf}
    {$IfDef TEM_UH and TEM_DBWEB}
    MostraUploadsIts(SQLType, Codigo, Nome, PastaCam, Tags, FTPDir, Empresa,
      DataExp, Desiste);
    //
    Nivel := 0;
    {$EndIf}
  end;
  if not Desiste then
  begin
    ReopenFTPConfig(FTPConfig, DmkIDApp, Query, DataBase);
    //
    Host    := Query.FieldByName('Web_FTPh').AsString;
    Usuario := Query.FieldByName('Web_FTPu').AsString;
    Senha   := Query.FieldByName('SENHA').AsString;
    Passivo := Query.FieldByName('Passivo').AsInteger;
    //
    if Host = ''  then
    begin
      Geral.MB_Aviso('Host n�o definido!');
      Exit;
    end;
    if Usuario = '' then
    begin
      Geral.MB_Aviso('Usu�rio n�o definido!');
      Exit;
    end;
    if Senha = '' then
    begin
      Geral.MB_Aviso('Senha n�o definida!');
      Exit;
    end;
    //
    if (Nome <> '') and (FTPDir <> 0) then
    begin
      if SQLType = stIns then
      begin
        Tam := DmkWeb.TamanhoArquivoBytes(Arquivos.Strings[0]);
        //
        if InOwner = nil then
          ArquivoNome := UploadFTP(PastaCam, Host, Usuario, Senha,
                           Geral.IntToBool(Passivo), Delete, Arquivos)
        else
          ArquivoNome := UploadFTP(InOwner, Pager, PastaCam, Host,
                           Usuario, Senha, Geral.IntToBool(Passivo), Delete,
                           Arquivos);
      end else
      begin
        Tam         := TamArq;
        ArquivoNome := ArqNome;
      end;
      //
      if ArquivoNome <> '' then
      begin
        if SQLType = stIns then
          SQLTyp := dtIns
        else
          SQLTyp := dtUpd;
        //
        Res := InsUpdRegistroFTPArq(Query, QueryAux, DataBase, DmkIDApp,
                 Empresa, FTPDir, Nivel, Tam, Nome, VersaoArq, Tags,
                 ArquivoNome, DataExp, Origem, Entidades);
        //
        if Res <> 0 then
        begin
          Result := Res;
        end else
          Geral.MB_Aviso(Msg);
      end;
    end;
  end;
  Entidades.Free;
end;

function TUnFTP.UploadFTP_Fotos(InOwner: TWincontrol;
  Pager: TWinControl; PastaCam, Host, User, Pass: String;
  Passivo, Delete: Boolean; Arquivos: TStrings; DmkIDApp, Empresa, Depto,
  FTPDir, Nivel: Integer; Origem: TOrigFTP; Query: TmySQLQuery;
  DataBase: TmySQLDatabase; InsereDataHora: Boolean = False): String;
var
  Form: TForm;
  I, TotArq: Integer;
  Tam: Double;
  Nome, ArqNome: String;
  Continua: Boolean;
  Miniatura: TGraphic;
  MinStream: TStream;
begin
  Result := '';
  Form   := MyObjects.FormTDICria(TFmFTPUploads, InOwner, Pager, False, False);
  //
  ConectaServidorFTP(Host, User, Pass, Passivo, TFmFTPUploads(Form).IdFTP1);
  //
  if TFmFTPUploads(Form).IdFTP1.Connected then
  begin
    TotArq := Arquivos.Count;
    //
    TFmFTPUploads(Form).LaArquivosTot.Visible  := True;
    TFmFTPUploads(Form).PBArquivosTot.Visible  := True;
    TFmFTPUploads(Form).LaArquivosTot.Caption  := 'Arquivo 1 de ' + Geral.FF0(TotArq);
    TFmFTPUploads(Form).PBArquivosTot.Position := 0;
    TFmFTPUploads(Form).PBArquivosTot.Max      := TotArq;
    //
    try
      for I := 0 to TotArq -1 do
      begin
        Nome    := Arquivos.Strings[I];
        ArqNome := ExtractFileName(Nome);
        ArqNome := Geral.SemAcento(ArqNome);
        ArqNome := StringReplace(ArqNome, ' ', '_', [rfReplaceAll, rfIgnoreCase]);
        Tam     := DmkWeb.TamanhoArquivoBytes(Nome);
        //
        if InsereDataHora then
          ArqNome := FormatDateTime('yymmdd_hhnnss', Now) + '_' + ArqNome;
        //
        //Altera para o diret�rio correto
        TFmFTPUploads(Form).IdFTP1.ChangeDir(PastaCam);
        //
        //Verifica se arquivo existe no servidor
        if VerificaSeArquivoExisteNoFTP(TFmFTPUploads(Form).IdFTP1, ArqNome,
          Host, User, Pass, Passivo, True) then
        begin
          if Geral.MB_Pergunta('J� existe um arquivo com este nome no diret�rio selecionado do servidor!' +
            sLineBreak + 'Deseja substitu�-lo?') <> ID_YES
          then
            Continua := False
          else
            Continua := True;
        end else
          Continua := True;
        //
        TFmFTPUploads(Form).LaArquivoAtu.Caption   := 'Copiando arquivo "' + ArqNome + '"';
        TFmFTPUploads(Form).LaArquivosTot.Caption  := 'Arquivo ' + Geral.FF0(I + 1) + ' de ' + Geral.FF0(TotArq);
        TFmFTPUploads(Form).PBArquivosTot.Position := TFmFTPUploads(Form).PBArquivosTot.Position + 1;
        TFmFTPUploads(Form).PBArquivosTot.Update;
        Application.ProcessMessages;
        //
        if Continua then
        begin
          //Faz o upload
          TFmFTPUploads(Form).IdFTP1.Put(Nome, ArqNome);
          //
          //Faz o upload da miniatura
          Miniatura := DmkImg.RedimencionaImgMiniatura(Nome);
          //
          if Miniatura <> nil then
          begin
            try
              MinStream := TMemoryStream.Create;
              //
              Miniatura.SaveToStream(MinStream);
              //
              TFmFTPUploads(Form).IdFTP1.Put(MinStream, 'min_' + ArqNome);
            finally
              MinStream.Free;
            end;
          end else
          begin
            Geral.MB_Aviso('Falha ao criar miniatura!');
          end;
          try
            if Delete = True then
            begin
              DeleteFile(Nome);
            end;
            InsUpdRegistroFTPArq(Query, nil, DataBase, DmkIDApp, Empresa, FTPDir,
              Nivel, Tam, '', '', '', ArqNome, 0, Origem, nil);
          finally

          end;
          Result := ArqNome;
        end;
      end;
    finally
      DesconectarDoFTP(TFmFTPUploads(Form).IdFTP1);
      //
      if Form <> nil then
        MyObjects.FormTDIFecha(Form, TTabSheet(TFmFTPUploads(Form).CSTabSheetChamou.Component));
    end;
  end;
end;

procedure TUnFTP.CarregaMiniatura(const FTPConfig: Integer; const
  Imagem: String; const FTPGer: TIdFTP; var Img: TImage);
var
  Destino, Ext: String;
  Tam, TamWeb: Int64;
begin
  if DmkImg.ValidaExtensao(Imagem, Ext) then
  begin
    Destino := CO_DIR_RAIZ_DMK + '\FTP\' + Geral.FF0(VAR_USUARIO) + '\' + Geral.FF0(FTPConfig);
    //
    if not DirectoryExists(Destino) then
    begin
      if not ForceDirectories(Destino) then
      begin
        Geral.MB_Erro('Falha ao criar diret�rio tempor�rio no destino: ' + Destino + '!');
        Exit;
      end;
    end;
    Screen.Cursor := crHourGlass;
    try
      Destino := Destino + '\' + Imagem;
      //
      if not FileExists(Destino) then
      begin
        if VerificaSeArquivoExisteNoFTP(FTPGer, Imagem,
          '', '', '', True, True) then
        begin
          FTPGer.TransferType := ftBinary;
          FTPGer.Get(Imagem, Destino, True);
          //
          TamWeb := FTPGer.Size(Imagem);
          Tam    := dmkPF.TamanhoDeArquivo(Destino);
          //
          if Tam <> TamWeb then
          begin
            if FileExists(Destino) then
              DeleteFile(Destino);
            //
            Geral.MB_Aviso('Falha ao carregar imagem!');
          end;
        end else
        begin
          Geral.MB_Aviso('Falha oa carregar imagem!' + sLineBreak +
            'Motivo: O arquivo n�o foi localizado no servidor Web!');
          Exit;
        end;
      end;
      Img.Picture := nil;
      Img.Picture.LoadFromFile(Destino);
    finally
      Screen.Cursor := crDefault;
    end;
  end else
    Img.Picture := nil;
end;

function TUnFTP.ConectaServidorFTP(Host, User, Pass: String; Passivo: Boolean;
  FTPComp: TIdFTP): Integer;
var
  ProxServ, ProxUser, ProxPass: String;
  ProxEnab, ProxPort: Integer;
begin
  Result := 0;
  try
    if FTPComp.Connected then
      FTPComp.Disconnect;
    //
    ObtemDadosProxy(ProxUser, ProxPass, ProxServ, ProxEnab, ProxPort);
    //
    if ProxEnab = 0 then
    begin
      FTPComp.ProxySettings.Password := '';
      FTPComp.ProxySettings.Port     := 0;
      FTPComp.ProxySettings.Host     := '';
      FTPComp.ProxySettings.UserName := '';
    end else
    begin
      FTPComp.ProxySettings.Password := ProxPass;
      FTPComp.ProxySettings.Port     := ProxPort;
      FTPComp.ProxySettings.Host     := ProxServ;
      FTPComp.ProxySettings.UserName := ProxUser;
    end;
    if (Host <> '') and (User <> '') and (Pass <> '') then
    begin
      FTPComp.Host         := Host;
      FTPComp.Username     := User;
      FTPComp.Password     := Pass;
      FTPComp.Passive      := Passivo;
      FTPComp.TransferType := ftBinary;
      FTPComp.UseHOST      := False;
      //
      FTPComp.Connect;
    end else
      Geral.MB_Aviso('Dados para conex�o com o servidor WEB n�o informados!');
    //
    Result := Geral.BoolToInt(FTPComp.Connected);
  except
    on E : Exception do
    begin
      if (Trim(UpperCase(E.ClassName)) = UpperCase('EIdReplyRFCError')) and
        (Trim(UpperCase(E.Message)) = UpperCase('Login authentication failed')) then
      begin
        Geral.MB_Aviso('Falha ao conectar!' + sLineBreak +
          'Prov�vel motivo: Os dados de autentica��o est�o incorretos!');
        Result := -1;
      end;
    end;
  end;
end;

procedure TUnFTP.ConfiguraStringGridUsuarios(Grade: TStringGrid);
begin
  MyObjects.LimpaGrade(Grade, 1, 1, True);
  //
  Grade.ColCount := 3;
  Grade.RowCount := 2;
  //
  Grade.ColWidths[1] := 250;
  Grade.ColWidths[2] := 75;
  //
  Grade.Cells[1, 0] := 'Entidade';
  Grade.Cells[2, 0] := 'ID';
end;

function TUnFTP.ConfiguraTipoDir(): TStringList;
var
  TipoDir: TStringList;
begin
  try
    TipoDir := TStringList.Create;
    //
    TipoDir.Add('Biblioteca de arquivos');
    TipoDir.Add('Diversos');
    TipoDir.Add('Armazenamento de fotos');
  finally
    Result := TipoDir;
  end;
end;

function TUnFTP.CriaDiretorio(Caminho: String; FTPComp: TIdFTP): Boolean;
begin
  try
    FTPComp.MakeDir(Caminho);
    //
    Result := True;
  except
    Result := False;
  end;
end;

function TUnFTP.DeletaArquvivoFTP(Arquivo, Caminho, Host, User, Pass: String;
  Passivo, FTPWebArq: Integer; Origem: TOrigFTP; DataBase: TmySQLDatabase;
  ExcluiComOrigem: Boolean = False): Boolean;
var
  IdFTP: TIdFTP;
  Orig: Integer;
begin
  Result := False;
  Orig   := TraduzOrigem(Origem);
  //
  if (Orig = 0) or (ExcluiComOrigem = True) then
  begin
    IdFTP := TIdFTP.Create();
    try
      try
        ConectaServidorFTP(Host, User, Pass, Geral.IntToBool(Passivo), IdFTP);
        //
        IdFTP.ChangeDir(Caminho);
        IdFTP.Delete(Arquivo);
        //
        if FTPWebArq <> 0 then
          UMyMod.ExcluiRegistroInt1('', 'ftpwebarq', 'Codigo', FTPWebArq, DataBase);
        //
        Result := True;
      finally
        IdFTP.Disconnect;
      end;
    finally
      IdFtp.Free;
    end;
  end;
end;

procedure TUnFTP.DesconectarDoFTP(FTPComp: TIdFTP);
begin
  if FTPComp.Connected then
    FTPComp.Disconnect;
end;

function TUnFTP.DownloadFileHTTPIndy(AbrirEmAba: Boolean; InOwner: TWincontrol;
  Pager: TWinControl; URL, Arquivo: String; Destino: String = ''): Boolean;

  function DownloadFile(Frm: TForm; IdHTTP: TIdHTTP;
    IdSSL: TIdSSLIOHandlerSocketOpenSSL): Boolean;
  var
    SaveFile: TSaveDialog;
    Dest: String;
    MyFile: TFileStream;
  begin
    Result := False;
    //
    if Length(Destino) = 0 then
    begin
      SaveFile := TSaveDialog.Create(Frm);
      try
        //Arquivo
        SaveFile.FileName := Arquivo;
        //
        if SaveFile.Execute then
          Dest := SaveFile.FileName
        else
          Exit;
        //
        if Length(Dest) > 0 then
        begin
          if FileExists(Dest) then
            if Geral.MB_Pergunta('O arquivo j� existe deseja substitu�-lo?') <> ID_YES then
              Exit;
        end;
      finally
        SaveFile.Free;
      end;
    end else
      Dest := Destino;
    //
    MyFile := TFileStream.Create(Dest, fmCreate);
    try
      DmkWeb.ConfiguraHTTP(IdHTTP, URL, IdSSL);
      IdHTTP.Get(URL, MyFile);
      //
      Result := True;
    finally
      MyFile.Free;
    end;
  end;

var
  Frm: TForm;
begin
  Result := False;
  //
  if AbrirEmAba then
  begin
    Frm := MyObjects.FormTDICria(TFmFTPUploads, InOwner, Pager, False, False);
    //
    if Frm <> nil then
    begin
      TFmFTPUploads(Frm).LaArquivosTot.Visible := False;
      TFmFTPUploads(Frm).PBArquivosTot.Visible := False;
      TFmFTPUploads(Frm).LaArquivoAtu.Caption  := 'Fazendo download dp arquivo ' + URL;
      //
      Result := DownloadFile(Frm, TFmFTPUploads(Frm).IdHTTP1, TFmFTPUploads(Frm).IdSSL1);
    end;
  end else
  begin
    if DBCheck.CriaFm(TFmFTPUploads, FmFTPUploads, afmoNegarComAviso) then
    begin
      FmFTPUploads.LaArquivosTot.Visible := False;
      FmFTPUploads.PBArquivosTot.Visible := False;
      FmFTPUploads.LaArquivoAtu.Caption  := 'Fazendo download dp arquivo ' + URL;
      //
      FmFTPUploads.Show;
      //
      Result := DownloadFile(FmFTPUploads, FmFTPUploads.IdHTTP1, FmFTPUploads.IdSSL1);
      //
      FmFTPUploads.Hide;
    end;
  end;
end;

function TUnFTP.DownloadFileIndy(InOwner: TWincontrol;
  Pager: TWinControl; SaveFile: TSaveDialog; Caminho, Arquivo,
  Destino, Host, User, Pass: String; Passivo: Boolean): String;
var
  Form: TForm;
  Destin, Ext: String;
begin
  Result := '';
  //
  //Arquivo
  Ext               := ExtractFileExt(Arquivo);
  SaveFile.Filter   := Ext + ' arquivos|*' + Ext + '|Todos os Arquivos|*.*';
  SaveFile.FileName := Arquivo;
  //
  if Length(Destino) = 0 then
  begin
    if SaveFile.Execute then
      Destin := SaveFile.FileName
    else
      Exit;
  end else
    Destin := Destino;
  //
  if Length(Destin) > 0 then
  begin
    if FileExists(Destin) then
      if Geral.MB_Pergunta('O arquivo j� existe deseja substitu�-lo?') <> ID_YES then
        Exit;
    try
      Form := MyObjects.FormTDICria(TFmFTPUploads, InOwner, Pager, False, False);
      //
      ConectaServidorFTP(Host, User, Pass, Passivo, TFmFTPUploads(Form).IdFTP1);
      //
      if TFmFTPUploads(Form).IdFTP1.Connected then
      begin
        TFmFTPUploads(Form).LaArquivosTot.Visible := False;
        TFmFTPUploads(Form).PBArquivosTot.Visible := False;
        TFmFTPUploads(Form).LaArquivoAtu.Caption  := 'Fazendo download dp arquivo ' + Arquivo;
        //
        DeleteFile(Destin);
        TFmFTPUploads(Form).IdFTP1.ChangeDir(Caminho);
        TFmFTPUploads(Form).IdFTP1.TransferType := ftBinary;
        TFmFTPUploads(Form).IdFTP1.Get(Arquivo, Destin, True);
      end;
    finally
      DesconectarDoFTP(TFmFTPUploads(Form).IdFTP1);
      if Form <> nil then
        MyObjects.FormTDIFecha(Form, TTabSheet(TFmFTPUploads(Form).CSTabSheetChamou.Component));
      //
      Result := Destin;
    end;
  end;
end;

function TUnFTP.ExcluiUsuarioComp(Controle: Integer; DataBase: TmySQLDatabase): Boolean;
begin
  try
    UMyMod.ExcluiRegistroInt1('Confirma a exclus�o da entidade selecionada?',
      'ftpwebcom', 'Controle', Controle, DataBase);
    Result := True;
  except
    Result := False;
  end;
end;

function TUnFTP.InsUpdRegistroFTPArq(Query, QueryAux: TmySQLQuery;
  DataBase: TmySQLDatabase; DmkIDApp, CliInt, FTPDir, Nivel: Integer;
  Tam: Double; Nome, VersaoArq, Tags, ArqNome: String; DataExp: TDate;
  Origem: TOrigFTP; Entidades: TStringList): Integer;
var
  Status: TSQLType;
  Codigo, Orig: Integer;
  DtaExp, DataHora: String;
begin
  Result   := 0;
  Orig     := TraduzOrigem(Origem);
  DataHora := Geral.FDT(Now, 109);
  DtaExp   := Geral.FDT(DataExp, 1);
  //
  if Orig in [0, 1, 2, 3, 4, 5] then
  begin
    Codigo := VerificaSeArquivoExiteNoDB(Query, DataBase, DmkIDApp, FTPDir, ArqNome);
    //
    if Codigo > 0 then
      Status := stUpd
    else begin
      Status := stIns;
      //
      if DmkIDApp = 4 then //Syndic
      begin
        Codigo := UMyMod.BuscaNovoCodigo_Int(Query, 'uploads', 'Codigo', [],
                    [], stIns, 0, siPositivo, nil);
      end else
      begin
        Codigo := UMyMod.BuscaNovoCodigo_Int(Query, 'ftpwebarq', 'Codigo', [],
                    [], stIns, 0, siPositivo, nil);
      end;
    end;
    if DmkIDApp = 4 then //Syndic
    begin
      if Status = stIns then
      begin
        if UMyMod.SQLInsUpd(Query, Status, 'uploads', False,
        [
          'Cond', 'Nome', 'Arquivo', 'DirWeb', 'DataExp', 'Tags', 'DataHora'
        ], ['Codigo'],
        [
          CliInt, Nome, ArqNome, FTPDir, DtaExp, Tags, DataHora
        ], [Codigo], True)
        then
          Result := Codigo;
      end else
      begin
        if UMyMod.SQLInsUpd(Query, Status, 'uploads', False,
        [
          'Cond', 'Nome', 'Arquivo', 'DirWeb', 'DataExp', 'Tags'
        ], ['Codigo'],
        [
          CliInt, Nome, ArqNome, FTPDir, DtaExp, Tags
        ], [Codigo], True)
        then
          Result := Codigo;
      end;
    end else
    begin
      if Status = stIns then
      begin
        if UMyMod.SQLInsUpd(Query, Status, 'ftpwebarq', False,
        [
          'CliInt', 'Nome', 'Arquivo', 'FTPDir', 'ArqTam', 'Nivel',
          'VersaoArq', 'Tags', 'DataExp', 'DataHora', 'Origem'
        ], ['Codigo'],
        [
          CliInt, Nome, ArqNome, FTPDir, Tam, Nivel,
          VersaoArq, Tags, DtaExp, DataHora, Orig
        ], [Codigo], True) then
        begin
          ArqDirCompartilhaEntidades(Codigo, dtArq, Entidades, QueryAux, Query);
          //
          Result := Codigo;
        end;
      end else
      begin
        if UMyMod.SQLInsUpd(Query, Status, 'ftpwebarq', False,
        [
          'CliInt', 'Nome', 'Arquivo', 'FTPDir', 'ArqTam', 'Nivel',
          'VersaoArq', 'Tags', 'DataExp', 'Origem'
        ], ['Codigo'],
        [
          CliInt, Nome, ArqNome, FTPDir, Tam, Nivel,
          VersaoArq, Tags, DtaExp, Orig
        ], [Codigo], True)
        then
          Result := Codigo;
      end;
    end;
  end else
  begin
    Geral.MB_Erro('Origem n�o implementada!');
    Result := 0;
  end;
end;

function TUnFTP.LocalizaAplicativo(Aplicativo: String; QueryLoc: TmySQLQuery;
  DataBase: TmySQLDatabase): Integer;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QueryLoc, DataBase, [
    'SELECT Codigo FROM aplicativos',
    'WHERE Nome LIKE "%' + Aplicativo + '%"',
    '']);
  if QueryLoc.RecordCount > 0 then
    Result := QueryLoc.FieldByName('Codigo').AsInteger
  else
    Result := 0;
  //
  QueryLoc.Close;
end;

function TUnFTP.MontaCaminhoDirFTP(CurDir, NomeDir: String): String;
var
  Tam: Integer;
  UltimoCar: String;
begin
  Tam       := Length(CurDir);
  UltimoCar := Copy(CurDir, Tam, 1);
  //
  if UltimoCar <> '/' then
    Result := CurDir + '/' + NomeDir
  else
    Result := CurDir + NomeDir;
end;

{$IfDef TEM_UH and TEM_DBWEB}
procedure TUnFTP.MostraUploadsIts(SQLType: TSQLType; Codigo: Integer;
  var Nome, PastaCam, Tags: String; var FTPDir, Empresa: Integer;
  var DataExp: TDate; var Desiste: Boolean);
begin
  if DBCheck.CriaFm(TFmUploadsIts, FmUploadsIts, afmoNegarComAviso) then
  begin
    FmUploadsIts.ImgTipo.SQLType := SQLType;
    FmUploadsIts.FCodigo         := Codigo;
    FmUploadsIts.FNome           := Nome;
    FmUploadsIts.FTags           := Tags;
    FmUploadsIts.FFTPDir         := FTPDir;
    FmUploadsIts.FEmpresa        := Empresa;
    FmUploadsIts.FDataExp        := DataExp;
    //
    FmUploadsIts.ShowModal;
    //
    Nome     := FmUploadsIts.FNome;
    FTPDir   := FmUploadsIts.FFTPDir;
    Empresa  := FmUploadsIts.FEmpresa;
    Tags     := FmUploadsIts.FTags;
    PastaCam := FmUploadsIts.FPastaCam;
    DataExp  := FmUploadsIts.FDataExp;
    Desiste  := FmUploadsIts.FDesiste;
    //
    FmUploadsIts.Destroy;
  end;
end;
{$EndIf}

{$IfDef UsaModuloFTP}
procedure TUnFTP.MostraFTPWebArqCad(SQLType: TSQLType; Codigo, CodigoApp,
  FTPConfig, FTPTipoDir, DmkIDApp: Integer; var Nome, VersaoArq, PastaCam,
  Tags: String; var FTPDir, Empresa, Nivel: Integer; var DataExp: TDate;
  var Entidades: TStringList; var Desiste: Boolean);
begin
  if DBCheck.CriaFm(TFmFTPWebArqCad, FmFTPWebArqCad, afmoNegarComAviso) then
  begin
    FmFTPWebArqCad.ImgTipo.SQLType := SQLType;
    FmFTPWebArqCad.FCodigoApp      := CodigoApp;
    FmFTPWebArqCad.FDmkIdApp       := DmkIDApp;
    FmFTPWebArqCad.FCodigo         := Codigo;
    FmFTPWebArqCad.FNome           := Nome;
    FmFTPWebArqCad.FVersaoArq      := VersaoArq;
    FmFTPWebArqCad.FTags           := Tags;
    FmFTPWebArqCad.FFTPConfig      := FTPConfig;
    FmFTPWebArqCad.FFTPTipoDir     := FTPTipoDir;
    FmFTPWebArqCad.FFTPDir         := FTPDir;
    FmFTPWebArqCad.FEmpresa        := Empresa;
    FmFTPWebArqCad.FEntidades      := Entidades;
    FmFTPWebArqCad.FNivel          := Nivel;
    FmFTPWebArqCad.FDataExp        := DataExp;
    //
    FmFTPWebArqCad.ShowModal;
    //
    Nome      := FmFTPWebArqCad.FNome;
    VersaoArq := FmFTPWebArqCad.FVersaoArq;
    FTPDir    := FmFTPWebArqCad.FFTPDir;
    Empresa   := FmFTPWebArqCad.FEmpresa;
    Entidades := FmFTPWebArqCad.FEntidades;
    Nivel     := FmFTPWebArqCad.FNivel;
    Tags      := FmFTPWebArqCad.FTags;
    PastaCam  := FmFTPWebArqCad.FPastaCam;
    DataExp   := FmFTPWebArqCad.FDataExp;
    Desiste   := FmFTPWebArqCad.FDesiste;
    //
    FmFTPWebArqCad.Destroy;
  end;
end;
{$EndIf}

procedure TUnFTP.ObtemDadosProxy(var ProxUser, ProxPass, ProxServ: String;
  var ProxEnab, ProxPort: Integer);
var
  User, Pass, Serv: String;
  Port, Enab: Integer;
begin
  try
    User := Geral.ReadAppKeyCU('ProxyUser', 'Dermatek', ktString, '');
    Pass := Geral.ReadAppKeyCU('ProxyPass', 'Dermatek', ktString, '');
    Serv := Geral.ReadAppKeyCU('ProxyServ', 'Dermatek', ktString, '');
    Port := Geral.ReadAppKeyCU('ProxyPort', 'Dermatek', ktInteger, 0);
    Enab := Geral.ReadAppKeyCU('ProxyEnable', 'Dermatek', ktInteger, 0);
    //
    if User <> '' then
      User := dmkPF.PWDExDecode(User, CO_RandStrWeb01);
    if Pass <> '' then
      Pass := dmkPF.PWDExDecode(Pass, CO_RandStrWeb01);
  except
    User := '';
    Pass := '';
    Serv := '';
    Port := 0;
    Enab := 0;
  end;
  ProxUser := User;
  ProxPass := Pass;
  ProxServ := Serv;
  ProxEnab := Enab;
  ProxPort := Port;
end;

function TUnFTP.ObtemNomeDir(Caminho: String): String;
var
  x, Novo: String;
  Continua: Boolean;
begin
  Novo := Caminho;
  repeat
    Continua := Geral.SeparaPrimeiraOcorrenciaDeTexto('/', Novo, x, Novo);
  until
    Continua = False;
  Result := x;
end;

function TUnFTP.ObterLinkArquivo(Web_MyURL, WebId, Arquivo: String;
  DMKID_APP: Integer): String;
begin
  if DMKID_APP = 17 then //DControl
    Result := Web_MyURL + '?page=downloads&id=' + Arquivo
  else if DMKID_APP = 24 then //Bugstrol
    Result := 'http://bugstrolweb.dermatek.net.br/' + WebId + '.php?page=downloads&id=' + Arquivo
  else
    Result := 'Gera��o de Link Web n�o implementada para este aplicativo!';
end;

procedure TUnFTP.PublicaRelatorioFRXnaWeb(DMKID_APP: Integer;
  HabilModulos: String; DBn: TmySQLDatabase; QryWebParams, QueryUpdN,
  QueryAux: TmySQLQuery;  frxPDFExport: TfrxPDFExport; frxPreview: TfrxPreview);
var
  Nome, Caminho, Destino, Arquivo, Msg: String;
  Arquivos: TStringList;
begin
{$IfDef UsaWSuport}
  if DmkWeb.VerificaSeModuloWebEstaAtivo(DMKID_APP, HabilModulos, DBn, Msg) then
  begin
    if DmkWeb.ConexaoRemota(DBn, QryWebParams, 1) then
    begin
      try
        Destino := CO_DIR_RAIZ_DMK + '\FTP\' + Geral.FF0(VAR_USUARIO);
        //
        if not DirectoryExists(Destino) then
        begin
          if not ForceDirectories(Destino) then
          begin
            Geral.MB_Erro('Falha ao criar diret�rio tempor�rio no destino: ' + Destino + '!');
            Exit;
          end;
        end;
        Arquivo := Destino + '\' + Geral.FDT(Now(), 26) + '.pdf';
        //
        Arquivos := TStringList.Create;
        Arquivos.Add(Arquivo);
        //
        MyObjects.frxConfiguraPDF(frxPDFExport);
        frxPDFExport.FileName   := Arquivo;
        frxPDFExport.ShowDialog := True;
        frxPreview.Export(frxPDFExport);
        //
        if FileExists(Arquivo) then
        begin
          Nome    := frxPreview.Report.ReportOptions.Name;
          Caminho := '';
          //
          UploadFTP_DB(nil, nil, stIns, Nome, Nome, '', '', 0, DMKID_APP, 1, 1,
            0, 0, 0, dtPad, 0, 0, Arquivos, True, QueryUpdN, QueryAux, DBn,
            Caminho, True);
        end;
      finally
        Arquivos.Free;
      end;
    end;
  end else
    Geral.MB_Aviso(Msg);
{$EndIf}
end;

procedure TUnFTP.ReopenDepto(DataBase: TmySQLDatabase; Query: TmySQLQuery;
  CodigoApp, Empresa: Integer);
begin
  if CodigoApp = 4 then //Syndic
  begin
  //DB local para ficar mais r�pido
  UnDmkDAC_PF.AbreMySQLQuery0(Query, DataBase, [
    'SELECT Conta Codigo, Unidade Nome ',
    'FROM condimov ',
    'WHERE Codigo=' + Geral.FF0(Empresa),
    'AND Ativo = 1 ',
    'ORDER BY Nome ',
    '']);
  end else
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(Query, DataBase, [
      'SELECT Codigo, IF(Tipo=0, RazaoSocial, Nome) Nome ',
      'FROM entidades ',
      'WHERE Ativo = 1 ',
      'ORDER BY Nome ',
      '']);
  end;
end;

procedure TUnFTP.ReopenFTPArq(Codigo, Diretorio, CliInt, Depto,
  DmkIDApp: Integer; Nome, Tags: String; Database: TmySQLDatabase;
  Query: TmySQLQuery; LaInfo: TLabel; var AtivaAfterScroll: Boolean);
var
  Its: Integer;
  SQL1, SQL2, SQLCliInt, SQLDepto, SQLNome, SQLTags, SQLGroupBy: String;
begin
  if CliInt <> 0 then
    SQLCliInt := 'AND arq.CliInt=' + Geral.FF0(CliInt)
  else
    SQLCliInt := '';
  if Depto <> 0 then
  begin
    if DmkIDApp = 4 then //Syndic
      SQLDepto := 'AND arq.Depto=' + Geral.FF0(Depto)
    else
      SQLDepto := 'AND com.Entidade=' + Geral.FF0(Depto);
  end
  else
    SQLDepto := '';
  if Nome <> '' then
    SQLNome := 'AND arq.Nome LIKE "%' + Nome + '%"'
  else
    SQLNome := '';
  if Tags <> '' then
    SQLTags := 'AND arq.Tags LIKE "%' + Tags + '%"'
  else
    SQLTags := '';
  //
  if DmkIDApp = 4 then //Syndic
  begin
    SQL1       := 'imv.Unidade Depto_TXT, ';
    SQL2       := 'LEFT JOIN condimov imv ON imv.Conta = arq.Depto';
    SQLGroupBy := '';
  end else
  begin
    SQL1       := 'IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) Depto_TXT, '; //Compatibilidade
    SQL2       := 'LEFT JOIN ftpwebcom com ON com.Codigo = arq.Codigo';
    SQLGroupBy := 'GROUP BY arq.Codigo';
  end;
  UnDmkDAC_PF.AbreMySQLQuery0(Query, Database, [
    'SELECT arq.Codigo, dir.Caminho, (arq.ArqTam / 1024) / 1024 ArqTam_MB, ',
    'IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) CliInt_TXT, ',
    SQL1,
    'arq.ArqTam, dir.FTPConfig, arq.* ',
    'FROM ftpwebarq arq ',
    'LEFT JOIN entidades emp ON emp.Codigo = arq.CliInt ',
    SQL2,
    'LEFT JOIN ftpwebdir dir ON dir.Codigo = arq.FTPDir ',
    'WHERE arq.FTPDir=' + Geral.FF0(Diretorio),
    SQLCliInt,
    SQLDepto,
    SQLNome,
    SQLTags,
    SQLGroupBy,
    '']);
  //
  Its              := Query.RecordCount;
  AtivaAfterScroll := True;
  //
  if Its = 1 then
    LaInfo.Caption := Geral.FF0(Its) + ' item'
  else
    LaInfo.Caption := Geral.FF0(Its) + ' itens';
  //
  if Codigo <> 0 then
    Query.Locate('Codigo', Codigo, []);
end;

procedure TUnFTP.ReopenFTPCom(Tipo: TTipoCompFTP; Codigo: Integer;
  DataBase: TmySQLDatabase; Query: TmySQLQuery);
var
  Tipo_Int: Integer;
begin
  case Tipo of
    dtDir: Tipo_Int := 0;
    dtArq: Tipo_Int := 1;
  end;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(Query, DataBase, [
    'SELECT com.Controle, IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) Entidade_TXT ',
    'FROM ftpwebcom com ',
    'LEFT JOIN entidades ent ON ent.Codigo = com.Entidade ',
    'WHERE com.Tipo=' + Geral.FF0(Tipo_Int),
    'AND com.Codigo=' + Geral.FF0(Codigo),
    'ORDER BY Entidade_TXT ',
    '']);
end;

procedure TUnFTP.ReopenFTPConfig(Codigo, DmkIDApp: Integer; QueryFTPCfg: TmySQLQuery;
  DataBase: TmySQLDatabase);
begin
  QueryFTPCfg.Close;
  //
  //
  if DmkIDApp <> 17 then //DControl
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QueryFTPCfg, DataBase, [
      'SELECT Codigo, "Configura��o WEB" Nome, Web_FTPs SENHA, ',
      'Web_FTPu, Web_FTPs, Web_FTPh, Web_Raiz, Web_MyURL, ',
      'Web_FTPpassivo Passivo, WebID, Ativo ',
      'FROM opcoesgerl ',
      '']);
  end else
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QueryFTPCfg, DataBase, [
      'SELECT Codigo, Nome, AES_DECRYPT(Web_FTPs, "' + CO_RandStrWeb01 + '") SENHA, ',
      'Web_FTPu, Web_FTPs, Web_FTPh, Web_Raiz, Web_MyURL, ',
      'Passivo, "" WebID, Ativo ',
      'FROM ftpconfig ',
      'WHERE Ativo = 1 ',
      '']);
    if Codigo > 0 then
      QueryFTPCfg.Locate('Codigo', Codigo, []);
  end;
end;

procedure TUnFTP.ReopenFTPWebDir(Configuracao, Codigo, DmkIDApp: Integer;
  Database: TmySQLDatabase; Query: TmySQLQuery);
var
  FTPConfig: Integer;
begin
  if DmkIDApp = 17 then //DControl outros apenas uma configura��o
    FTPConfig := Configuracao
  else
    FTPConfig := 1;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(Query, DataBase, [
    'SELECT dir.*, ',
    'CASE dir.TipoDir ',
    'WHEN 0 THEN "Biblioteca de arquivos" ',
    'WHEN 1 THEN "Diversos" ',
    'WHEN 2 THEN "Armazenamento de fotos" ',
    'ELSE "" END TipoDir_TXT, ',
    'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) CliInt_TXT ',
    'FROM ftpwebdir dir ',
    'LEFT JOIN entidades ent ON ent.Codigo= dir.CliInt ',
    'WHERE dir.FTPConfig=' + Geral.FF0(FTPConfig),
    'ORDER BY dir.Nome ',
    '']);
  if Codigo <> 0 then
    Query.Locate('Codigo', Codigo, []);
end;

end.
