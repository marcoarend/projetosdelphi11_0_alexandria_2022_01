unit FTPUploads;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkLabel, dmkEdit,
  dmkEditCB, DBCtrls, dmkDBLookupComboBox, ComCtrls, IdBaseComponent,
  IdComponent, IdTCPConnection, IdTCPClient, IdExplicitTLSClientServerBase,
  IdFTP, CommCtrl, ApplicationConfiguration, DB, mySQLDbTables, FTPSiteInfo,
  UnInternalConsts, dmkGeral, Variants, IdFTPCommon, UnDmkProcFunc, UnMyObjects,
  dmkImage, dmkPermissoes, dmkCompoStore, UnDmkEnums, IdHTTP, IdIOHandler,
  IdIOHandlerSocket, IdIOHandlerStack, IdSSL, IdSSLOpenSSL;

type
  TFmFTPUploads = class(TForm)
    Panel1: TPanel;
    Panel5: TPanel;
    lbStatus: TListBox;
    GroupBox1: TGroupBox;
    Label2: TLabel;
    Label3: TLabel;
    LaTamTot: TLabel;
    LaTamRec: TLabel;
    EdTamTot: TdmkEdit;
    EdTamRec: TdmkEdit;
    GroupBox3: TGroupBox;
    Label9: TLabel;
    EdPerRec: TdmkEdit;
    GroupBox2: TGroupBox;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    EdTmpTot: TdmkEdit;
    EdTmpRun: TdmkEdit;
    EdTmpFal: TdmkEdit;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBRodaPe: TGroupBox;
    Panel3: TPanel;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    CSTabSheetChamou: TdmkCompoStore;
    dmkPermissoes1: TdmkPermissoes;
    LaArquivoAtu: TLabel;
    LaArquivosTot: TLabel;
    PBArquivoAtu: TProgressBar;
    PBArquivosTot: TProgressBar;
    IdFTP1: TIdFTP;
    IdHTTP1: TIdHTTP;
    IdSSL1: TIdSSLIOHandlerSocketOpenSSL;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure IdFTP1Status(ASender: TObject; const AStatus: TIdStatus;
      const AStatusText: string);
    procedure IdFTP1Work(ASender: TObject; AWorkMode: TWorkMode;
      AWorkCount: Int64);
    procedure IdFTP1WorkBegin(ASender: TObject; AWorkMode: TWorkMode;
      AWorkCountMax: Int64);
    procedure IdFTP1WorkEnd(ASender: TObject; AWorkMode: TWorkMode);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure IdHTTP1Status(ASender: TObject; const AStatus: TIdStatus;
      const AStatusText: string);
    procedure IdHTTP1Work(ASender: TObject; AWorkMode: TWorkMode;
      AWorkCount: Int64);
    procedure IdHTTP1WorkBegin(ASender: TObject; AWorkMode: TWorkMode;
      AWorkCountMax: Int64);
    procedure IdHTTP1WorkEnd(ASender: TObject; AWorkMode: TWorkMode);
  private
    { Private declarations }
    FIdFTPAtivo: Boolean;
    FWorkCountMax: Integer;
    FTimeIni: TDateTime;
    procedure Status(AStatus: TIdStatus; AStatusText: String);
    procedure Work(AWorkMode: TWorkMode; AWorkCount: Int64);
    procedure WorkBegin(AWorkMode: TWorkMode; AWorkCountMax: Int64);
    procedure WorkEnd(AWorkMode: TWorkMode);
    procedure Log(Msg: String; Color: TColor);
  public
    { Public declarations }
  end;

  var
  FmFTPUploads: TFmFTPUploads;

implementation

uses UnFTP, Module, MyGlyfs, Principal, UnDmkWeb;

{$R *.DFM}

procedure TFmFTPUploads.BtSaidaClick(Sender: TObject);
begin
  if FIdFTPAtivo then
    IdFTP1.Abort;
  //
  if TFmFTPUploads(Self).Owner is TApplication then
    Close
  else
    MyObjects.FormTDIFecha(Self, TTabSheet(CSTabSheetChamou.Component));
end;

procedure TFmFTPUploads.Log(Msg: String; Color: TColor);
begin
  lbStatus.Items.AddObject(Msg, Pointer(Color));
  lbStatus.ItemIndex := lbStatus.Items.Count -1;
end;

procedure TFmFTPUploads.Status(AStatus: TIdStatus; AStatusText: String);
begin
  Log(AStatusText, clBlack);
end;

procedure TFmFTPUploads.Work(AWorkMode: TWorkMode; AWorkCount: Int64);
var
  PerNum, Tam: Double;
  Niv: Integer;
  Nom: String;
  TimeAtu, TimeTot: TDateTime;
begin
  PBArquivoAtu.Position := AWorkCount;
  PBArquivoAtu.Update;
  Application.ProcessMessages;
  Log('Work ' + IntToStr(AWorkCount), clPurple);
  //
  TimeAtu    := Now() - FTimeIni;
  PerNum     := 0;
  if FWorkCountMax > 0 then
    PerNum := AWorkCount / FWorkCountMax;
  if PerNum > 0 then
    TimeTot := TimeAtu / PerNum
  else
    TimeTot := 0;
  PerNum := PerNum * 100;
  Geral.AdequaMedida(grandezaBytes, AWorkCount, 0, Tam, Niv, Nom);
  EdTamRec.DecimalSize  := Niv;
  EdTamRec.ValueVariant := Tam;
  LaTamRec.Caption      := Nom;
  EdPerRec.ValueVariant := PerNum;
  EdTmpRun.ValueVariant := TimeAtu;
  EdTmpTot.ValueVariant := TimeTot;
  EdTmpFal.ValueVariant := TimeTot - TimeAtu;
end;

procedure TFmFTPUploads.WorkBegin(AWorkMode: TWorkMode; AWorkCountMax: Int64);
var
  Tam: Double;
  Niv: Integer;
  Nom: String;
begin
  PBArquivoAtu.Max      := AWorkCountMax;
  PBArquivoAtu.Position := 0;
  PBArquivoAtu.Visible  := True;
  FTimeIni              := Now();
  FWorkCountMax         := AWorkCountMax;
  Log('Work begin ' + IntToStr(FWorkCountMax), clPurple);
  //
  Geral.AdequaMedida(grandezaBytes, FWorkCountMax, 0, Tam, Niv, Nom);
  EdTamTot.DecimalSize  := Niv;
  EdTamTot.ValueVariant := Tam;
  LaTamTot.Caption := Nom;
  //
  FIdFTPAtivo := True;
end;

procedure TFmFTPUploads.WorkEnd(AWorkMode: TWorkMode);
begin
  PBArquivoAtu.Position := 0;
  Log('Work end', clPurple);
  //
  FIdFTPAtivo := False;
end;

procedure TFmFTPUploads.FormActivate(Sender: TObject);
begin
  if TFmFTPUploads(Self).Owner is TApplication then
  begin
    MyObjects.CorIniComponente();
  end;
end;

procedure TFmFTPUploads.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  //IdFTP1.Abort;
  //UFTP.DesconectarDoFTP(IdFTP1);
end;

procedure TFmFTPUploads.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  FIdFTPAtivo     := False;
end;

procedure TFmFTPUploads.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
    [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmFTPUploads.FormShow(Sender: TObject);
begin
{$IfNDef cSkinRank} //Berlin
{$IfNDef cAlphaSkin} //Berlin
  FmMyGlyfs.DefineGlyfs(TForm(Sender));
{$EndIf}
{$EndIf}
{$IfDef cSkinRank} //Berlin
  if FmPrincipal.Sd1.Active then
    FmMyGlyfs.DefineGlyfsTDI(FmPrincipal.sd1, Sender);
{$EndIf}
{$IfDef cAlphaSkin} //Berlin
  if FmPrincipal.sSkinManager1.Active then
    FmMyGlyfs.DefineGlyfsTDI2(FmPrincipal.sSkinManager1, Sender);
{$EndIf}
end;

procedure TFmFTPUploads.IdFTP1Status(ASender: TObject; const AStatus: TIdStatus;
  const AStatusText: string);
begin
  Status(AStatus, AStatusText);
end;

procedure TFmFTPUploads.IdFTP1Work(ASender: TObject; AWorkMode: TWorkMode;
  AWorkCount: Int64);
begin
  Work(AWorkMode, AWorkCount);
end;

procedure TFmFTPUploads.IdFTP1WorkBegin(ASender: TObject; AWorkMode: TWorkMode;
  AWorkCountMax: Int64);
begin
  WorkBegin(AWorkMode, AWorkCountMax);
end;

procedure TFmFTPUploads.IdFTP1WorkEnd(ASender: TObject; AWorkMode: TWorkMode);
begin
  WorkEnd(AWorkMode);
end;

procedure TFmFTPUploads.IdHTTP1Status(ASender: TObject;
  const AStatus: TIdStatus; const AStatusText: string);
begin
  Status(AStatus, AStatusText);
end;

procedure TFmFTPUploads.IdHTTP1Work(ASender: TObject; AWorkMode: TWorkMode;
  AWorkCount: Int64);
begin
  Work(AWorkMode, AWorkCount);
end;

procedure TFmFTPUploads.IdHTTP1WorkBegin(ASender: TObject; AWorkMode: TWorkMode;
  AWorkCountMax: Int64);
begin
  WorkBegin(AWorkMode, AWorkCountMax);
end;

procedure TFmFTPUploads.IdHTTP1WorkEnd(ASender: TObject; AWorkMode: TWorkMode);
begin
  WorkEnd(AWorkMode);
end;

end.
