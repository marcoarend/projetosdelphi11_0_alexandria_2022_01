object FmFTPUploads: TFmFTPUploads
  Left = 339
  Top = 185
  Caption = 'XXX-XXXXX-001 :: Upload / Download'
  ClientHeight = 362
  ClientWidth = 544
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object LaArquivoAtu: TLabel
    Left = 0
    Top = 83
    Width = 544
    Height = 20
    Align = alTop
    Caption = 'Copiando arquivo ""'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    ExplicitWidth = 139
  end
  object LaArquivosTot: TLabel
    Left = 0
    Top = 48
    Width = 544
    Height = 20
    Align = alTop
    Caption = 'Arquivo 0 de 0'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    ExplicitWidth = 101
  end
  object Panel1: TPanel
    Left = 0
    Top = 118
    Width = 544
    Height = 130
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object Panel5: TPanel
      Left = 0
      Top = 0
      Width = 544
      Height = 70
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 4
        Top = 4
        Width = 213
        Height = 61
        Caption = ' Tamanho do arquivo: '
        Enabled = False
        TabOrder = 0
        object Label2: TLabel
          Left = 12
          Top = 16
          Width = 27
          Height = 13
          Caption = 'Total:'
        end
        object Label3: TLabel
          Left = 112
          Top = 16
          Width = 49
          Height = 13
          Caption = 'Recebido:'
        end
        object LaTamTot: TLabel
          Left = 80
          Top = 36
          Width = 25
          Height = 13
          Caption = 'bytes'
        end
        object LaTamRec: TLabel
          Left = 180
          Top = 36
          Width = 25
          Height = 13
          Caption = 'bytes'
        end
        object EdTamTot: TdmkEdit
          Left = 12
          Top = 32
          Width = 64
          Height = 21
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object EdTamRec: TdmkEdit
          Left = 112
          Top = 32
          Width = 64
          Height = 21
          Alignment = taRightJustify
          TabOrder = 1
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
      end
      object GroupBox3: TGroupBox
        Left = 224
        Top = 4
        Width = 89
        Height = 61
        Caption = ' Percentual: '
        Enabled = False
        TabOrder = 1
        object Label9: TLabel
          Left = 12
          Top = 16
          Width = 63
          Height = 13
          Caption = '% Conclu'#237'do:'
        end
        object EdPerRec: TdmkEdit
          Left = 12
          Top = 32
          Width = 64
          Height = 21
          Alignment = taRightJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 0
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
      end
      object GroupBox2: TGroupBox
        Left = 320
        Top = 4
        Width = 217
        Height = 61
        Caption = ' Tempo: '
        Enabled = False
        TabOrder = 2
        object Label6: TLabel
          Left = 12
          Top = 16
          Width = 27
          Height = 13
          Caption = 'Total:'
        end
        object Label7: TLabel
          Left = 80
          Top = 16
          Width = 62
          Height = 13
          Caption = 'Transcorrido:'
        end
        object Label8: TLabel
          Left = 148
          Top = 16
          Width = 46
          Height = 13
          Caption = 'Restante:'
        end
        object EdTmpTot: TdmkEdit
          Left = 12
          Top = 32
          Width = 64
          Height = 21
          TabOrder = 0
          FormatType = dmktfTime
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfLong
          Texto = '00:00:00'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object EdTmpRun: TdmkEdit
          Left = 80
          Top = 32
          Width = 64
          Height = 21
          TabOrder = 1
          FormatType = dmktfTime
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfLong
          Texto = '00:00:00'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object EdTmpFal: TdmkEdit
          Left = 148
          Top = 32
          Width = 64
          Height = 21
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 2
          FormatType = dmktfTime
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfLong
          Texto = '00:00:00'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
      end
    end
    object lbStatus: TListBox
      Left = 0
      Top = 70
      Width = 544
      Height = 60
      Hint = 'Transaction Log'
      Style = lbOwnerDrawFixed
      Align = alClient
      TabOrder = 1
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 544
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 496
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 448
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 229
        Height = 32
        Caption = 'Upload / Download'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 229
        Height = 32
        Caption = 'Upload / Download'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 237
        Height = 32
        Caption = 'Upload / Download '
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 292
    Width = 544
    Height = 70
    Align = alBottom
    TabOrder = 2
    object Panel3: TPanel
      Left = 2
      Top = 15
      Width = 540
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object PnSaiDesis: TPanel
        Left = 396
        Top = 0
        Width = 144
        Height = 53
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 0
        object BtSaida: TBitBtn
          Tag = 15
          Left = 11
          Top = 3
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Cancela'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 248
    Width = 544
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 540
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object PBArquivoAtu: TProgressBar
    Left = 0
    Top = 103
    Width = 544
    Height = 15
    Align = alTop
    Position = 50
    TabOrder = 4
  end
  object PBArquivosTot: TProgressBar
    Left = 0
    Top = 68
    Width = 544
    Height = 15
    Align = alTop
    Position = 50
    TabOrder = 5
  end
  object CSTabSheetChamou: TdmkCompoStore
    Left = 240
    Top = 304
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 211
    Top = 304
  end
  object IdFTP1: TIdFTP
    OnStatus = IdFTP1Status
    OnWork = IdFTP1Work
    OnWorkBegin = IdFTP1WorkBegin
    OnWorkEnd = IdFTP1WorkEnd
    IPVersion = Id_IPv4
    TransferType = ftBinary
    NATKeepAlive.UseKeepAlive = False
    NATKeepAlive.IdleTimeMS = 0
    NATKeepAlive.IntervalMS = 0
    ProxySettings.ProxyType = fpcmNone
    ProxySettings.Port = 0
    Left = 264
    Top = 184
  end
  object IdHTTP1: TIdHTTP
    OnStatus = IdHTTP1Status
    IOHandler = IdSSL1
    OnWork = IdHTTP1Work
    OnWorkBegin = IdHTTP1WorkBegin
    OnWorkEnd = IdHTTP1WorkEnd
    AllowCookies = True
    ProtocolVersion = pv1_0
    ProxyParams.BasicAuthentication = False
    ProxyParams.ProxyPort = 0
    Request.ContentLength = -1
    Request.ContentRangeEnd = -1
    Request.ContentRangeStart = -1
    Request.ContentRangeInstanceLength = -1
    Request.Accept = 'text/html, */*'
    Request.BasicAuthentication = False
    Request.UserAgent = 'Mozilla/3.0 (compatible; Indy Library)'
    Request.Ranges.Units = 'bytes'
    Request.Ranges = <>
    HTTPOptions = [hoForceEncodeParams]
    Left = 428
    Top = 260
  end
  object IdSSL1: TIdSSLIOHandlerSocketOpenSSL
    MaxLineAction = maException
    Port = 0
    DefaultPort = 0
    SSLOptions.Mode = sslmUnassigned
    SSLOptions.VerifyMode = []
    SSLOptions.VerifyDepth = 0
    Left = 456
    Top = 260
  end
end
