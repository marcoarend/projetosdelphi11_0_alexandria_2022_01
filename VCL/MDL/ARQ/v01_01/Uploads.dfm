object FmUploads: TFmUploads
  Left = 339
  Top = 185
  Caption = 'WEB-UPLOA-001 :: Uploads de arquivos'
  ClientHeight = 712
  ClientWidth = 812
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 812
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 724
      Top = 0
      Width = 88
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 48
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
      object ImgWEB: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 676
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 247
        Height = 32
        Caption = 'Uploads de arquivos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 247
        Height = 32
        Caption = 'Uploads de arquivos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 247
        Height = 32
        Caption = 'Uploads de arquivos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 812
    Height = 550
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 812
      Height = 550
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 812
        Height = 550
        Align = alClient
        TabOrder = 0
        object Panel5: TPanel
          Left = 2
          Top = 15
          Width = 808
          Height = 245
          Align = alTop
          TabOrder = 0
          object Label1: TLabel
            Left = 573
            Top = 9
            Width = 42
            Height = 13
            Caption = 'Diret'#243'rio:'
          end
          object Label2: TLabel
            Left = 6
            Top = 9
            Width = 60
            Height = 13
            Caption = 'Condom'#237'nio:'
          end
          object ImgIconDB: TImage
            Left = 622
            Top = 104
            Width = 180
            Height = 135
            Center = True
            IncrementalDisplay = True
            Proportional = True
            Stretch = True
            Transparent = True
          end
          object EdCond: TdmkEditCB
            Left = 6
            Top = 28
            Width = 57
            Height = 21
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            DBLookupComboBox = CBCond
            IgnoraDBLookupComboBox = False
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
          object CBCond: TdmkDBLookupComboBox
            Left = 65
            Top = 28
            Width = 502
            Height = 21
            KeyField = 'Codigo'
            ListField = 'Nome'
            ListSource = DsCond
            TabOrder = 1
            dmkEditCB = EdCond
            UpdType = utYes
            LocF7SQLMasc = '$#'
            LocF7PreDefProc = f7pNone
          end
          object BitBtn1: TBitBtn
            Tag = 18
            Left = 4
            Top = 170
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Hint = 'Sai da janela atual'
            Caption = '&Reabre'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 10
            OnClick = BitBtn1Click
          end
          object CBDiretorio: TdmkDBLookupComboBox
            Left = 632
            Top = 28
            Width = 168
            Height = 21
            KeyField = 'Codigo'
            ListField = 'Nome'
            ListSource = DsDirWeb
            TabOrder = 3
            dmkEditCB = EdDiretorio
            UpdType = utYes
            LocF7SQLMasc = '$#'
            LocF7PreDefProc = f7pNone
          end
          object EdDiretorio: TdmkEditCB
            Left = 573
            Top = 28
            Width = 57
            Height = 21
            Alignment = taRightJustify
            TabOrder = 2
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            DBLookupComboBox = CBDiretorio
            IgnoraDBLookupComboBox = False
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
          object EdDescricao: TdmkEdit
            Left = 6
            Top = 77
            Width = 250
            Height = 21
            TabOrder = 5
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object CkDescricao: TCheckBox
            Left = 6
            Top = 55
            Width = 250
            Height = 17
            Caption = 'Descri'#231#227'o'
            TabOrder = 4
          end
          object CkTags: TCheckBox
            Left = 262
            Top = 55
            Width = 250
            Height = 17
            Caption = 'Tags'
            TabOrder = 6
          end
          object EdTags: TdmkEdit
            Left = 262
            Top = 77
            Width = 540
            Height = 21
            TabOrder = 7
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object GBEmissao: TGroupBox
            Left = 6
            Top = 104
            Width = 200
            Height = 60
            Caption = ' Per'#237'odo de cadastro: '
            TabOrder = 8
            object CkDataIni: TCheckBox
              Left = 8
              Top = 14
              Width = 90
              Height = 17
              Caption = 'Data inicial:'
              TabOrder = 0
            end
            object TPDataIni: TDateTimePicker
              Left = 8
              Top = 32
              Width = 90
              Height = 21
              CalColors.TextColor = clMenuText
              Date = 37636.000000000000000000
              Time = 0.777157974502188200
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
              TabOrder = 1
            end
            object CkDataFim: TCheckBox
              Left = 100
              Top = 14
              Width = 90
              Height = 17
              Caption = 'Data final:'
              TabOrder = 2
            end
            object TPDataFim: TDateTimePicker
              Left = 100
              Top = 32
              Width = 90
              Height = 21
              Date = 37636.000000000000000000
              Time = 0.777203761601413100
              TabOrder = 3
            end
          end
          object GroupBox2: TGroupBox
            Left = 212
            Top = 104
            Width = 200
            Height = 60
            Caption = ' Per'#237'odo de expira'#231#227'o:'
            TabOrder = 9
            object CkDataExpIni: TCheckBox
              Left = 8
              Top = 14
              Width = 90
              Height = 17
              Caption = 'Data inicial:'
              TabOrder = 0
            end
            object TPDataExpIni: TDateTimePicker
              Left = 8
              Top = 32
              Width = 90
              Height = 21
              CalColors.TextColor = clMenuText
              Date = 37636.000000000000000000
              Time = 0.777157974502188200
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
              TabOrder = 1
            end
            object CkDataExpFim: TCheckBox
              Left = 100
              Top = 14
              Width = 90
              Height = 17
              Caption = 'Data final:'
              TabOrder = 2
            end
            object TPDataExpFim: TDateTimePicker
              Left = 100
              Top = 32
              Width = 90
              Height = 21
              Date = 37636.000000000000000000
              Time = 0.777203761601413100
              TabOrder = 3
            end
          end
          object BtLimpar: TBitBtn
            Tag = 169
            Left = 130
            Top = 170
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Caption = 'R&edefinir'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 11
            OnClick = BtLimparClick
          end
          object CkFotos: TCheckBox
            Left = 256
            Top = 185
            Width = 280
            Height = 17
            Caption = 'Mostrar miniaturas de fotos para diret'#243'rio de fotos'
            Checked = True
            State = cbChecked
            TabOrder = 12
            OnClick = CkFotosClick
          end
        end
        object PageControl1: TdmkPageControl
          Left = 2
          Top = 277
          Width = 808
          Height = 271
          ActivePage = TabSheet1
          Align = alClient
          TabHeight = 25
          TabOrder = 1
          EhAncora = True
          object TabSheet1: TTabSheet
            Caption = 'Pesquisa'
            object DBGArquivos: TdmkDBGrid
              Left = 0
              Top = 0
              Width = 800
              Height = 236
              Align = alClient
              Columns = <
                item
                  Expanded = False
                  FieldName = 'Codigo'
                  Title.Caption = 'ID'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'DirFotos'
                  Title.Caption = 'Foto'
                  Width = 35
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Nome'
                  Width = 150
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Arquivo'
                  Width = 150
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'DataExp_TXT'
                  Title.Caption = 'Data da expira'#231#227'o'
                  Width = 100
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NCONDOM'
                  Title.Caption = 'Condom'#237'nio'
                  Width = 250
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'DIRNOME'
                  Title.Caption = 'Diret'#243'rio'
                  Width = 100
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'DataHora'
                  Title.Caption = 'Data/Hora'
                  Width = 120
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Tags'
                  Width = 200
                  Visible = True
                end>
              Color = clWindow
              DataSource = DsUploads
              Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
              TabOrder = 0
              TitleFont.Charset = ANSI_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              Columns = <
                item
                  Expanded = False
                  FieldName = 'Codigo'
                  Title.Caption = 'ID'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'DirFotos'
                  Title.Caption = 'Foto'
                  Width = 35
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Nome'
                  Width = 150
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Arquivo'
                  Width = 150
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'DataExp_TXT'
                  Title.Caption = 'Data da expira'#231#227'o'
                  Width = 100
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NCONDOM'
                  Title.Caption = 'Condom'#237'nio'
                  Width = 250
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'DIRNOME'
                  Title.Caption = 'Diret'#243'rio'
                  Width = 100
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'DataHora'
                  Title.Caption = 'Data/Hora'
                  Width = 120
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Tags'
                  Width = 200
                  Visible = True
                end>
            end
          end
        end
        object PB1: TProgressBar
          Left = 2
          Top = 260
          Width = 808
          Height = 17
          Align = alTop
          TabOrder = 2
          Visible = False
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 598
    Width = 812
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 808
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 642
    Width = 812
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 666
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 664
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtInclui: TBitBtn
        Tag = 10
        Left = 5
        Top = 4
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Inclui'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtIncluiClick
      end
      object BtAltera: TBitBtn
        Tag = 11
        Left = 129
        Top = 4
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Altera'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        OnClick = BtAlteraClick
      end
      object BtExclui: TBitBtn
        Tag = 12
        Left = 252
        Top = 4
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Exclui sel.'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 2
        OnClick = BtExcluiClick
      end
      object BtExcluiExp: TBitBtn
        Tag = 12
        Left = 375
        Top = 4
        Width = 150
        Height = 40
        Cursor = crHandPoint
        Caption = '&Exclui expirados'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 3
        OnClick = BtExcluiExpClick
      end
    end
  end
  object QrUploads: TMySQLQuery
    Database = DModG.RV_CEP_DB
    BeforeClose = QrUploadsBeforeClose
    AfterScroll = QrUploadsAfterScroll
    SQL.Strings = (
      'SELECT upl.*, dir.Pasta NDIRWEB, dir.Nome DIRNOME,'
      'IF (rec.Tipo=0, rec.RazaoSocial, rec.Nome) NCONDOM,'
      
        'IF (upl.DataExp = 0, "", DATE_FORMAT(upl.DataExp, "%d/%m/%Y")) D' +
        'ataExp_TXT '
      'FROM uploads upl'
      'LEFT JOIN dirweb dir ON dir.Codigo=upl.dirweb'
      'LEFT JOIN cond con ON con.Codigo=upl.Cond'
      'LEFT JOIN entidades rec ON rec.Codigo=con.Cliente'
      'WHERE upl.DirWeb=:P0'
      'ORDER BY NCONDOM, upl.DirWeb, upl.Nome')
    Left = 264
    Top = 413
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrUploadsCodigo: TAutoIncField
      FieldName = 'Codigo'
      Origin = 'uploads.Codigo'
    end
    object QrUploadsNome: TWideStringField
      FieldName = 'Nome'
      Origin = 'uploads.Nome'
      Size = 32
    end
    object QrUploadsArquivo: TWideStringField
      FieldName = 'Arquivo'
      Origin = 'uploads.Arquivo'
      Size = 32
    end
    object QrUploadsCond: TIntegerField
      FieldName = 'Cond'
      Origin = 'uploads.Cond'
    end
    object QrUploadsLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'uploads.Lk'
    end
    object QrUploadsDataCad: TDateField
      FieldName = 'DataCad'
      Origin = 'uploads.DataCad'
    end
    object QrUploadsDataAlt: TDateField
      FieldName = 'DataAlt'
      Origin = 'uploads.DataAlt'
    end
    object QrUploadsUserCad: TIntegerField
      FieldName = 'UserCad'
      Origin = 'uploads.UserCad'
    end
    object QrUploadsUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Origin = 'uploads.UserAlt'
    end
    object QrUploadsAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Origin = 'uploads.AlterWeb'
    end
    object QrUploadsAtivo: TSmallintField
      FieldName = 'Ativo'
      Origin = 'uploads.Ativo'
      Required = True
    end
    object QrUploadsDirWeb: TIntegerField
      FieldName = 'DirWeb'
      Origin = 'uploads.DirWeb'
      Required = True
    end
    object QrUploadsNDIRWEB: TWideStringField
      FieldName = 'NDIRWEB'
      Origin = 'dirweb.Pasta'
      Size = 32
    end
    object QrUploadsDIRNOME: TWideStringField
      FieldName = 'DIRNOME'
      Origin = 'dirweb.Nome'
      Size = 32
    end
    object QrUploadsNCONDOM: TWideStringField
      FieldName = 'NCONDOM'
      Origin = 'NCONDOM'
      Size = 100
    end
    object QrUploadsDataExp: TDateField
      FieldName = 'DataExp'
    end
    object QrUploadsDataExp_TXT: TWideStringField
      FieldName = 'DataExp_TXT'
      Size = 10
    end
    object QrUploadsDataHora: TDateTimeField
      FieldName = 'DataHora'
    end
    object QrUploadsTags: TWideStringField
      FieldName = 'Tags'
      Size = 255
    end
    object QrUploadsPasta: TWideStringField
      FieldName = 'Pasta'
      Size = 32
    end
    object QrUploadsDirFotos: TSmallintField
      FieldName = 'DirFotos'
      MaxValue = 1
    end
  end
  object DsUploads: TDataSource
    DataSet = QrUploads
    Left = 292
    Top = 413
  end
  object QrCond: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      
        'SELECT con.Codigo, IF(ent.Tipo = 0, ent.RazaoSocial, ent.Nome) N' +
        'ome'
      'FROM cond con'
      'LEFT JOIN entidades  ent ON ent.Codigo = con.Cliente'
      'ORDER BY Nome')
    Left = 320
    Top = 413
    object QrCondCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCondNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
  object DsCond: TDataSource
    DataSet = QrCond
    Left = 348
    Top = 413
  end
  object QrDirWeb: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'SELECT Codigo, Nome, Fotos'
      'FROM dirweb'
      'ORDER BY Nome')
    Left = 376
    Top = 413
    object IntegerField1: TIntegerField
      FieldName = 'Codigo'
    end
    object QrDirWebNome: TWideStringField
      FieldName = 'Nome'
      Size = 32
    end
    object QrDirWebFotos: TSmallintField
      FieldName = 'Fotos'
    end
  end
  object DsDirWeb: TDataSource
    DataSet = QrDirWeb
    Left = 404
    Top = 413
  end
  object OpenDialog1: TOpenDialog
    Options = [ofHideReadOnly, ofAllowMultiSelect, ofShareAware, ofEnableSizing]
    Left = 433
    Top = 413
  end
  object QrLoc: TMySQLQuery
    Database = DModG.RV_CEP_DB
    Left = 462
    Top = 413
  end
  object PMInclui: TPopupMenu
    Left = 152
    Top = 400
    object Arquivos1: TMenuItem
      Caption = '&Arquivos'
      OnClick = Arquivos1Click
    end
    object Fotos1: TMenuItem
      Caption = '&Fotos'
      OnClick = Fotos1Click
    end
  end
  object FTPGer: TIdFTP
    ConnectTimeout = 0
    NATKeepAlive.UseKeepAlive = False
    NATKeepAlive.IdleTimeMS = 0
    NATKeepAlive.IntervalMS = 0
    ProxySettings.ProxyType = fpcmNone
    ProxySettings.Port = 0
    Left = 188
    Top = 473
  end
end
