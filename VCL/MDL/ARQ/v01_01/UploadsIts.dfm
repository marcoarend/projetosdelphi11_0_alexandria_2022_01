object FmUploadsIts: TFmUploadsIts
  Left = 339
  Top = 185
  Caption = 'WEB-UPLOA-002 :: Uploads de arquivos - Itens'
  ClientHeight = 435
  ClientWidth = 460
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 460
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 412
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 364
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 332
        Height = 32
        Caption = 'Uploads de arquivos - Itens'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 332
        Height = 32
        Caption = 'Uploads de arquivos - Itens'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 332
        Height = 32
        Caption = 'Uploads de arquivos - Itens'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 460
    Height = 273
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 460
      Height = 273
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 460
        Height = 273
        Align = alClient
        TabOrder = 0
        object Label9: TLabel
          Left = 16
          Top = 8
          Width = 36
          Height = 13
          Caption = 'C'#243'digo:'
        end
        object Label10: TLabel
          Left = 16
          Top = 49
          Width = 51
          Height = 13
          Caption = 'Descri'#231#227'o:'
        end
        object LaCond: TLabel
          Left = 16
          Top = 138
          Width = 60
          Height = 13
          Caption = 'Condom'#237'nio:'
        end
        object Label1: TLabel
          Left = 16
          Top = 182
          Width = 70
          Height = 13
          Caption = 'Diret'#243'rio WEB:'
        end
        object Label4: TLabel
          Left = 15
          Top = 224
          Width = 90
          Height = 13
          Caption = 'Data da expira'#231#227'o:'
        end
        object Label2: TLabel
          Left = 16
          Top = 94
          Width = 27
          Height = 13
          Caption = 'Tags:'
        end
        object EdNome: TdmkEdit
          Left = 15
          Top = 66
          Width = 385
          Height = 21
          MaxLength = 32
          TabOrder = 1
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object EdCond: TdmkEditCB
          Left = 16
          Top = 154
          Width = 57
          Height = 21
          Alignment = taRightJustify
          TabOrder = 3
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          DBLookupComboBox = CBCond
          IgnoraDBLookupComboBox = False
        end
        object CBCond: TdmkDBLookupComboBox
          Left = 75
          Top = 154
          Width = 325
          Height = 21
          KeyField = 'Codigo'
          ListField = 'Nome'
          ListSource = DsCond
          TabOrder = 4
          dmkEditCB = EdCond
          UpdType = utYes
          LocF7SQLMasc = '$#'
        end
        object EdDirWeb: TdmkEditCB
          Left = 16
          Top = 198
          Width = 57
          Height = 21
          Alignment = taRightJustify
          TabOrder = 5
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          DBLookupComboBox = CBDirWeb
          IgnoraDBLookupComboBox = False
        end
        object CBDirWeb: TdmkDBLookupComboBox
          Left = 76
          Top = 198
          Width = 324
          Height = 21
          KeyField = 'Codigo'
          ListField = 'Nome'
          ListSource = DsDirWeb
          TabOrder = 6
          dmkEditCB = EdDirWeb
          UpdType = utYes
          LocF7SQLMasc = '$#'
        end
        object TPDataExp: TdmkEditDateTimePicker
          Left = 15
          Top = 240
          Width = 112
          Height = 21
          Date = 40375.706008634260000000
          Time = 40375.706008634260000000
          TabOrder = 7
          ReadOnly = False
          DefaultEditMask = '!99/99/99;1;_'
          AutoApplyEditMask = True
          UpdType = utYes
        end
        object EdTags: TdmkEdit
          Left = 15
          Top = 111
          Width = 385
          Height = 21
          MaxLength = 255
          TabOrder = 2
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object EdCodigo: TdmkEdit
          Left = 16
          Top = 25
          Width = 64
          Height = 21
          Alignment = taRightJustify
          Color = clInactiveCaption
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBackground
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          ReadOnly = True
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utInc
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 321
    Width = 460
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 456
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 365
    Width = 460
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 314
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 312
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 13
        Top = 3
        Width = 120
        Height = 40
        Caption = '&Confirma'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object QrCond: TmySQLQuery
    Database = Dmod.MyDBn
    SQL.Strings = (
      
        'SELECT con.Codigo, IF(ent.Tipo = 0, ent.RazaoSocial, ent.Nome) N' +
        'ome'
      'FROM cond con'
      'LEFT JOIN entidades  ent ON ent.Codigo = con.Cliente'
      'WHERE con.Ativo = 1'
      'ORDER BY Nome')
    Left = 208
    Top = 61
    object QrCondCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCondNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
  object DsCond: TDataSource
    DataSet = QrCond
    Left = 236
    Top = 61
  end
  object QrDirWeb: TmySQLQuery
    Database = Dmod.MyDBn
    SQL.Strings = (
      'SELECT Codigo, Nome, Pasta, Fotos'
      'FROM dirweb'
      'WHERE Ativo = 1'#11
      'AND Fotos = 0'
      'ORDER BY Nome')
    Left = 264
    Top = 61
    object IntegerField1: TIntegerField
      FieldName = 'Codigo'
    end
    object QrDirWebNome: TWideStringField
      FieldName = 'Nome'
      Size = 32
    end
    object QrDirWebFotos: TSmallintField
      FieldName = 'Fotos'
    end
    object QrDirWebPasta: TWideStringField
      FieldName = 'Pasta'
      Size = 32
    end
  end
  object DsDirWeb: TDataSource
    DataSet = QrDirWeb
    Left = 292
    Top = 61
  end
end
