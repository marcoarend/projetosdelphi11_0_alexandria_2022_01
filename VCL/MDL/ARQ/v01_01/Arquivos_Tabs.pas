unit Arquivos_Tabs;

{ Colocar no MyListas:

Uses Arquivos_Tabs;


//
function TMyListas.CriaListaTabelas:
  Arquivos_Tb.CarregaListaTabelas(FTabelas);
//
function TMyListas.CriaListaSQL:
  Arquivos_Tb.CarregaListaSQL(Tabela, FListaSQL);
//
function TMyListas.CompletaListaSQL:
  Arquivos_Tb.ComplementaListaSQL(Tabela, FListaSQL);
//
function TMyListas.CriaListaIndices:
  Arquivos_Tb.CarregaListaFRIndices(Tabela, FRIndices, FLIndices);
//
function TMyListas.CriaListaCampos:
  Arquivos_Tb.CarregaListaFRCampos(Tabela, FRCampos, FLCampos);
  Arquivos_Tb.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
//
function TMyListas.CriaListaJanelas:
  Arquivos_Tb.CompletaListaFRJanelas(FRJanelas, FLJanelas);

}
interface

uses
  System.Generics.Collections, Windows, Messages, SysUtils, Classes, Graphics,
  Controls, Dialogs, DB, (*DBTables,*) UnMyLinguas, Forms, mysqlDBTables, dmkGeral,
  UnDmkEnums, UnGrl_Vars;

type
  TArquivos_Tabs = class(TObject)
  private
    { Private declarations }
  public
    { Public declarations }
    function CarregaListaTabelas(Database: TmySQLDatabase; Lista: TList<TTabelas>): Boolean;
    function CarregaListaFRCampos(Tabela: String; FRCampos:
             TCampos; FLCampos: TList<TCampos>; var TemControle: TTemControle): Boolean;
    function CompletaListaFRJanelas(FRJanelas:
             TJanelas; FLJanelas: TList<TJanelas>): Boolean;
    function CarregaListaFRIndices(Tabela: String; FRIndices:
             TIndices; FLIndices: TList<TIndices>): Boolean;
    function CarregaListaSQL(Tabela: String; FListaSQL: TStringList):
             Boolean;
    function ComplementaListaSQL(Tabela: String; FListaSQL: TStringList):
             Boolean;
    function CompletaListaFRCampos(Tabela: String; FRCampos:
             TCampos; FLCampos: TList<TCampos>): Boolean;
  end;

//const

var
  Arquivos_Tb: TArquivos_Tabs;

implementation

uses UMySQLModule;

function TArquivos_Tabs.CarregaListaTabelas(Database: TmySQLDatabase;
  Lista: TList<TTabelas>): Boolean;
begin
  try
    Result := True;
    //
    if Database.DatabaseName = VAR_DBWEB then
    begin
      MyLinguas.AdTbLst(Lista, False, Lowercase('arq_opcoes'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('arq_videos'), '');
      //
      MyLinguas.AdTbLst(Lista, False, LowerCase('ftpconfig'), '');
      MyLinguas.AdTbLst(Lista, False, LowerCase('ftpwebarq'), '');
      MyLinguas.AdTbLst(Lista, False, LowerCase('ftpwebcom'), '');
      MyLinguas.AdTbLst(Lista, False, LowerCase('ftpwebdir'), '');
    end;
  except
    raise;
    Result := False;
  end;
end;

function TArquivos_Tabs.CarregaListaSQL(Tabela: String; FListaSQL:
TStringList): Boolean;
begin
  Result := True;
  {
  if Uppercase(Tabela) = Uppercase('XXXXX') then
  begin
    FListaSQL.Add('Codigo');
    FListaSQL.Add('1');
  end
  }
end;

function TArquivos_Tabs.ComplementaListaSQL(Tabela: String; FListaSQL:
TStringList): Boolean;
begin
  Result := True;
  if Uppercase(Tabela) = Uppercase('') then
  begin
  end else
  if Uppercase(Tabela) = Uppercase('arq_opcoes') then
  begin
    if FListaSQL.Count = 0 then
    FListaSQL.Add('Codigo');
    FListaSQL.Add('     1');
  end else
  if UpperCase(Tabela) = UpperCase('ftpconfig') then
  begin
    FListaSQL.Add('Codigo');
    FListaSQL.Add('     1');
  end;
end;


function TArquivos_Tabs.CarregaListaFRIndices(Tabela: String; FRIndices:
 TIndices; FLIndices: TList<TIndices>): Boolean;
begin
  Result := True;
  if Uppercase(Tabela) = Uppercase('arq_opcoes') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('arq_videos') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('ftpconfig') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('ftpwebarq') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('ftpwebcom') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Controle';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('ftpwebdir') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
  end;
end;

function TArquivos_Tabs.CarregaListaFRCampos(Tabela: String; FRCampos:
 TCampos; FLCampos: TList<TCampos>; var TemControle: TTemControle): Boolean;
begin
  Result := True;
  TemControle := TemControle + cTemControleNao;
  try
    if Uppercase(Tabela) = Uppercase('arq_opcoes') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'YouTube_IdCanal';
      FRCampos.Tipo       := 'varchar(50)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'YouTube_IdPlayList';
      FRCampos.Tipo       := 'varchar(50)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('arq_videos') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Provedor';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '1';  //1 = YouTube
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(100)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PublicadoEm';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Id';
      FRCampos.Tipo       := 'varchar(50)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'IdCanal';
      FRCampos.Tipo       := 'varchar(50)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'IdPlayList';
      FRCampos.Tipo       := 'varchar(50)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'IdVideo';
      FRCampos.Tipo       := 'varchar(50)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'NomeCanal';
      FRCampos.Tipo       := 'varchar(50)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Thumbnail';
      FRCampos.Tipo       := 'varchar(100)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('ftpconfig') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(50)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Web_FTPu';
      FRCampos.Tipo       := 'varchar(50)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Web_FTPs';
      FRCampos.Tipo       := 'varchar(50)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Web_FTPh';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Web_Raiz';
      FRCampos.Tipo       := 'varchar(50)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CfgPadrao';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Passivo';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Web_MyURL';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('ftpwebarq') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(50)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Arquivo';
      FRCampos.Tipo       := 'varchar(50)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'FTPDir';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ArqTam';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);                     //01 - Liberado
      FRCampos.Field      := 'Nivel';    //02 - Boss
      FRCampos.Tipo       := 'int(11)';  //04 - Administrador
      FRCampos.Null       := '';         //08 - Cliente
      FRCampos.Key        := '';         //16 - Usu�rio
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CliInt';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'VersaoArq';
      FRCampos.Tipo       := 'varchar(15)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);                       //Origem
      FRCampos.Field      := 'Origem';     //0: FtpWebArq  - Padr�o gerenciador de FTP
      FRCampos.Tipo       := 'int(11)';    //1: WordSer    - Ordem de servi�os
      FRCampos.Null       := '';           //2: BackupDir  - Backup de diret�rios
      FRCampos.Key        := '';           //3: Web        - Arquivos gerenciados na web
      FRCampos.Default    := '0';          //4: Aplicativo - Upload de vers�es de aplicativos
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Tags';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DataHora';
      FRCampos.Tipo       := 'datetime';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00 00:00:00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DataExp'; //Data de expira��o do arquivo
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0000-00-00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('ftpwebcom') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Entidade';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Tipo';        //0 => Diretorio
      FRCampos.Tipo       := 'tinyint(1)';  //1 => Arquivo
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('ftpwebdir') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(50)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Pasta';
      FRCampos.Tipo       := 'varchar(50)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Caminho';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'FTPConfig';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Ordem';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ArqPri';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);                     //01 - Liberado
      FRCampos.Field      := 'Nivel';    //02 - Boss
      FRCampos.Tipo       := 'int(11)';  //04 - Administrador
      FRCampos.Null       := '';         //08 - Cliente
      FRCampos.Key        := '';         //16 - Usu�rio
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CliInt';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Depto';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'TipoDir';    //00 - Biblioteca de arquivos
      FRCampos.Tipo       := 'tinyint(1)'; //01 - Diversos
      FRCampos.Null       := '';           //02 - Armazenamento de fotos
      FRCampos.Key        := '';
      FRCampos.Default    := '1';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end;
  except
    raise;
    Result := False;
  end;
end;

function TArquivos_Tabs.CompletaListaFRCampos(Tabela: String; FRCampos:
 TCampos; FLCampos: TList<TCampos>): Boolean;
begin
  Result := True;
  try
    if Uppercase(Tabela) = Uppercase('Controle') then
    begin
      {New(FRCampos);
      FRCampos.Field      := 'XXX';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //}
    end;
  except
    raise;
    Result := False;
  end;
end;

function TArquivos_Tabs.CompletaListaFRJanelas(FRJanelas:
 TJanelas; FLJanelas: TList<TJanelas>): Boolean;
begin
  // FTP-CONFI-001 :: Configura��es FTP
  New(FRJanelas);
  FRJanelas.ID        := 'FTP-CONFI-001';
  FRJanelas.Nome      := 'FmFTPConfig';
  FRJanelas.Descricao := 'Configura��es FTP';
  FLJanelas.Add(FRJanelas);
  //
  // FTP-UPLOA-001 :: Upload / Download
  New(FRJanelas);
  FRJanelas.ID        := 'FTP-UPLOA-001';
  FRJanelas.Nome      := 'FmFTPUploads';
  FRJanelas.Descricao := 'Upload / Download';
  FLJanelas.Add(FRJanelas);
  //
  // WEB-FTPDI-001 :: Diret�rios FTP
  New(FRJanelas);
  FRJanelas.ID        := 'WEB-FTPDI-001';
  FRJanelas.Nome      := 'FmFTPWebDir';
  FRJanelas.Descricao := 'Diret�rios FTP';
  FLJanelas.Add(FRJanelas);
  //
  // WEB-FTPAR-001 :: Arquivos WEB
  New(FRJanelas);
  FRJanelas.ID        := 'WEB-FTPAR-001';
  FRJanelas.Nome      := 'FmFTPWebArq';
  FRJanelas.Descricao := 'Arquivos WEB';
  FLJanelas.Add(FRJanelas);
  //
  // WEB-FTPAR-002 :: Arquivos FTP Cadastro
  New(FRJanelas);
  FRJanelas.ID        := 'WEB-FTPAR-002';
  FRJanelas.Nome      := 'FmFTPWebArqCad';
  FRJanelas.Descricao := 'Arquivos FTP Cadastro';
  FLJanelas.Add(FRJanelas);
  //
  //////////////////////////////////////////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////////
  Result := True;
  //////////////////////////////////////////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////////
end;

end.
