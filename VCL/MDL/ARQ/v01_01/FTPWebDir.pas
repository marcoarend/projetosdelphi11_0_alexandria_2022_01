unit FTPWebDir;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, dmkCheckGroup, dmkEdit, mySQLDbTables,
  UnDmkEnums, dmkEditCB, dmkDBLookupComboBox, dmkValUsu, Variants, DmkDAC_PF,
  dmkPermissoes, UnProjGroup_Consts;

type
  TFmFTPWebDir = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    Label12: TLabel;
    EdCodigo: TdmkEdit;
    EdNome: TdmkEdit;
    Label13: TLabel;
    QrLoc: TmySQLQuery;
    RGTipoDir: TRadioGroup;
    GroupBox2: TGroupBox;
    CGNivel: TdmkCheckGroup;
    CBEmpresa: TdmkDBLookupComboBox;
    EdEmpresa: TdmkEditCB;
    Label1: TLabel;
    VUEmpresa: TdmkValUsu;
    dmkPermissoes1: TdmkPermissoes;
    PnEntidades: TPanel;
    Panel5: TPanel;
    BtInclui: TBitBtn;
    BtExclui: TBitBtn;
    Grade: TStringGrid;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure BtIncluiClick(Sender: TObject);
    procedure BtExcluiClick(Sender: TObject);
  private
    { Private declarations }
    FDMKID_APP: Integer;
    procedure MostraEdicao(SQLType: TSQLType);
  public
    { Public declarations }
    FPasta, FCaminho, FNome: String;
    FFTPConfig, FCodigo, FNivel, FCodigoApp, FTipoDir, FCliInt: Integer;
  end;

  var
    FmFTPWebDir: TFmFTPWebDir;

implementation

uses UnMyObjects, Module, UnDmkWeb, UMySQLModule, UnFTP, ModuleGeral, MyListas,
  UnGrlUsuarios;

{$R *.DFM}

procedure TFmFTPWebDir.BtExcluiClick(Sender: TObject);
begin
  MyObjects.ExcluiLinhaStringGrid(Grade);
end;

procedure TFmFTPWebDir.BtIncluiClick(Sender: TObject);
begin
  //Usa dblocal para ficar mais r�pido
  UFTP.AdicionaUsuarios(Dmod.QrAux, Dmod.MyDB, Grade);
end;

procedure TFmFTPWebDir.BtOKClick(Sender: TObject);
var
  Codigo, I: Integer;
  Nome, Msg: String;
  Entidades: TStringList;
  TipoDir, CliInt, FTPConfig, Res: Integer;
begin
  Nome     := EdNome.ValueVariant;
  TipoDir  := RGTipoDir.ItemIndex;
  //
  if EdEmpresa.ValueVariant = 0 then
    CliInt := 0
  else
    CliInt := VUEmpresa.ValueVariant;
  //
  if CO_DMKID_APP = 17 then
    FTPConfig := FFTPConfig
  else
    FTPConfig := 1; //Os demais aplicativos s� poder�o gerenciar uma conta FTP no aplicativo
  //
  if MyObjects.FIC(Length(Nome) = 0, EdNome, 'Defina uma descri��o') then Exit;
  if MyObjects.FIC(FTPConfig = 0, nil, 'Servidor FTP n�o definido!') then Exit;
  if MyObjects.FIC(Length(FPasta) = 0, nil, 'Diret�rio FTP n�o definido!') then Exit;
  if MyObjects.FIC(Length(FCaminho) = 0, nil, 'Endere�o do diret�rio FTP n�o definido!') then Exit;
  if MyObjects.FIC(TipoDir = -1, RGTipoDir, 'Defina o tipo de diret�rio!') then Exit;
  //
  if (TipoDir = 0) and (ImgTipo.SQLType = stIns) then
  begin
    if MyObjects.FIC(UFTP.VerificaSeBibliotecaDeArquivosExiste(QrLoc,
      Dmod.MyDBn, 0) > 0, RGTipoDir, 'Pode haver apenas um diret�rio para o tipo selecionado!')
    then
      Exit;
  end;
  if ImgTipo.SQLType = stIns then
  begin
    if MyObjects.FIC(UFTP.VerificaSeDiretorioExiste(QrLoc, Dmod.MyDBn, FCaminho,
      FTPConfig) <> 0, nil, 'Este diret�rio j� est� cadastrado!') then Exit;
  end;
  if ImgTipo.SQLType = stIns then
    Codigo := 0
  else
    Codigo := EdCodigo.ValueVariant;
  //
  //Cria lista de entidades
  Entidades := TStringList.Create;
  try
    for I := 1 to Grade.RowCount - 1 do
    begin
      Entidades.Add(Grade.Cells[2, I]);
    end;
  except
    Entidades.Free;
  end;
  //
  Res := UFTP.InsUpdFTPWebDir(Dmod.QrAuxN, Dmod.QrUpdN, Codigo, FTPConfig,
           TipoDir, CliInt, CGNivel.Value, Nome, FPasta, FCaminho, Entidades);
  if Res <> 0 then
  begin
    VAR_COD := Res;
    Close;
  end else
    Geral.MB_Aviso('Falha ao incluir diret�rio!');
end;

procedure TFmFTPWebDir.BtSaidaClick(Sender: TObject);
begin
  VAR_COD := EdCodigo.ValueVariant;
  Close;
end;

procedure TFmFTPWebDir.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmFTPWebDir.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  case CO_DMKID_APP of
    17: //DControl
      FDMKID_APP := 31;
    24: //Bugstrol
      FDMKID_APP := 33;
    else
      Geral.MB_Aviso('Aplicativo n�o implementado!');
  end;
  //
  CBEmpresa.ListSource := DModG.DsEmpresas;
  //
  UFTP.ConfiguraStringGridUsuarios(Grade);
end;

procedure TFmFTPWebDir.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
    [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmFTPWebDir.FormShow(Sender: TObject);
begin
  CGNivel.Items.Clear;
  CGNivel.Items.AddStrings(GrlUsuarios.ConfiguraNiveis(FCodigoApp));
  //
  RGTipoDir.Items.Clear;
  RGTipoDir.Items.AddStrings(UFTP.ConfiguraTipoDir);
  //
  MostraEdicao(ImgTipo.SQLType);
end;

procedure TFmFTPWebDir.MostraEdicao(SQLType: TSQLType);
begin
  //
  if SQLType = stIns then
  begin
    PnEntidades.Visible := True;
    FmFTPWebDir.Height  := 580;
  end else
  begin
    PnEntidades.Visible := False;
    FmFTPWebDir.Height  := 580 - PnEntidades.Height;
    GroupBox2.Height    := 215 - PnEntidades.Height;
  end;
  //
  if SQLType = stIns then
  begin
    EdCodigo.ValueVariant  := FormatFloat('000', 0);
    EdNome.ValueVariant    := FPasta;
    RGTipoDir.ItemIndex    := 1;
    EdEmpresa.ValueVariant := DModG.QrFiliLogFilial.Value;
    CBEmpresa.KeyValue     := DModG.QrFiliLogFilial.Value;
    CGNivel.Value          := 0;
  end else begin
    EdCodigo.ValueVariant  := FCodigo;
    EdNome.ValueVariant    := FNome;
    RGTipoDir.ItemIndex    := FTipoDir;
    VUEmpresa.ValueVariant := FCliInt;
    CGNivel.Value          := FNivel;
  end;
  EdNome.SetFocus;
end;

end.
