object FmFTPWebArq: TFmFTPWebArq
  Left = 339
  Top = 185
  Caption = 'WEB-FTPAR-001 :: Arquivos WEB'
  ClientHeight = 561
  ClientWidth = 756
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 756
    Height = 47
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    ExplicitWidth = 949
    object GB_R: TGroupBox
      Left = 1078
      Top = 0
      Width = 108
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 59
        Top = 14
        Width = 39
        Height = 39
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Transparent = True
        SQLType = stNil
      end
      object ImgWEB: TdmkImage
        Left = 10
        Top = 14
        Width = 39
        Height = 39
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 59
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 59
      Top = 0
      Width = 1019
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 9
        Top = 11
        Width = 212
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Arquivos WEB'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 11
        Top = 14
        Width = 212
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Arquivos WEB'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 10
        Top = 12
        Width = 212
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Arquivos WEB'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object ImgArq: TImage
        Left = 719
        Top = 26
        Width = 19
        Height = 20
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Picture.Data = {
          07544269746D617036030000424D360300000000000036000000280000001000
          000010000000010018000000000000030000120B0000120B0000000000000000
          0000FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
          FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF2A2A2A3535
          35353535353535353535353535353535353535353535353535353535353535FF
          00FFFF00FFFF00FFFF00FF2A2A2AFFFFFFF9F9F9F2F2F2EAEAEADFDFDFD5D5D5
          CBCBCBC1C1C1B8B8B8B1B1B1353535FF00FFFF00FFFF00FFFF00FF2A2A2AFFFF
          FFF9F9F9F2F2F2EAEAEADFDFDFD5D5D5CBCBCBC1C1C1B8B8B8B1B1B1353535FF
          00FFFF00FFFF00FFFF00FF2A2A2AFFFFFFF9F9F9F2F2F2EAEAEADFDFDFD5D5D5
          CBCBCBC1C1C1B8B8B8B1B1B1353535FF00FFFF00FFFF00FFFF00FF2A2A2AFFFF
          FFF9F9F9F2F2F2EAEAEADFDFDFD5D5D5CBCBCBC1C1C1B8B8B8B1B1B1353535FF
          00FFFF00FFFF00FFFF00FF2A2A2AFFFFFFF9F9F9F2F2F2EAEAEADFDFDFD5D5D5
          CBCBCBC1C1C1B8B8B8B1B1B1353535FF00FFFF00FFFF00FFFF00FF2A2A2AFFFF
          FFF9F9F9F2F2F2EAEAEADFDFDFD5D5D5CBCBCBC1C1C1B8B8B8B1B1B1353535FF
          00FFFF00FFFF00FFFF00FF2A2A2AFFFFFFF9F9F9F2F2F2EAEAEADFDFDFD5D5D5
          CBCBCBC1C1C1B8B8B8B1B1B1353535FF00FFFF00FFFF00FFFF00FF2A2A2AFFFF
          FFF9F9F9F2F2F2EAEAEADFDFDFD5D5D5CBCBCBC1C1C1B8B8B8B1B1B1353535FF
          00FFFF00FFFF00FFFF00FF2A2A2AFFFFFFF9F9F9F2F2F2EAEAEADFDFDFD5D5D5
          CBCBCBC1C1C1B8B8B8B1B1B1353535FF00FFFF00FFFF00FFFF00FF2A2A2AFFFF
          FFF9F9F9F2F2F2EAEAEADFDFDFD5D5D5CBCBCBC1C1C1B8B8B8B1B1B1353535FF
          00FFFF00FFFF00FFFF00FF2A2A2AFFFFFFF9F9F9F2F2F2EAEAEADFDFDFD5D5D5
          CBCBCB343434343434343434353535FF00FFFF00FFFF00FFFF00FF2A2A2AFFFF
          FFF9F9F9F2F2F2EAEAEADFDFDFD5D5D5CBCBCB343434ABABABABABAB353535FF
          00FFFF00FFFF00FFFF00FF2A2A2AFFFFFFF9F9F9F2F2F2EAEAEADFDFDFD5D5D5
          CBCBCB343434ABABAB353535FF00FFFF00FFFF00FFFF00FFFF00FF2A2A2A3434
          34343434343434343434343434343434343434343434353535FF00FFFF00FFFF
          00FF}
        Proportional = True
        Transparent = True
      end
      object ImgDir: TImage
        Left = 748
        Top = 26
        Width = 20
        Height = 20
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Picture.Data = {
          07544269746D617036030000424D360300000000000036000000280000001000
          000010000000010018000000000000030000120B0000120B0000000000000000
          0000FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
          FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
          FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
          00FF42A5C684E7FF73DEFF6BDEFF6BE7FF6BE7FF6BE7FF6BE7FF6BE7FF6BE7FF
          6BE7FF6BE7FF6BDEFF73E7FF63CEEFFF00FF009CCE31D6FF10CEFF10CEFF08CE
          FF08CEFF08CEFF08CEFF08CEFF08CEFF08CEFF08CEFF10CEFF18CEFF94E7FFFF
          00FF0094CE39D6FF18D6FF18D6FF18D6FF18D6FF18D6FF18D6FF18D6FF18D6FF
          18D6FF18D6FF18D6FF21D6FF9CEFFFFF00FF0094CE4ADEFF29D6FF29D6FF29D6
          FF29D6FF29D6FF29D6FF29D6FF29D6FF29D6FF29D6FF21D6FF31D6FF9CEFFFFF
          00FF0094CE52DEFF39DEFF39DEFF39DEFF39DEFF39DEFF39DEFF39DEFF39DEFF
          39DEFF39DEFF39DEFF42DEFFADEFFFFF00FF0094CE63E7FF42E7FF42E7FF42E7
          FF42E7FF42E7FF42E7FF42E7FF42E7FF42E7FF42E7FF4AE7FF4AE7FFADF7FFFF
          00FF009CCE6BE7FF52E7FF52E7FF52E7FF52E7FF52E7FF52E7FF52E7FF52E7FF
          52E7FF5AE7FF52E7FF5AE7FFB5F7FFFF00FF009CCE7BEFFF63EFFF63EFFF63EF
          FF63EFFF63EFFF63EFFF63EFFF63EFFF63EFFF63EFFF63EFFF6BEFFFBDF7FFFF
          00FF0094CE8CF7FF7BF7FF7BEFFF7BF7FF7BEFFF7BEFFF73EFFF73EFFF73EFFF
          73EFFF73EFFF73F7FF7BEFFFC6F7FFFF00FF009CCEB5F7FFA5F7FFA5F7FFA5F7
          FFA5F7FF9CF7FF94F7FF84F7FF7BF7FF84F7FF84F7FF7BF7FF84F7FFCEF7FFFF
          00FF3994BDE7FFFFE7FFFFE7FFFFDEFFFFE7FFFFDEFFFFC6FFFFA5FFFF9CFFFF
          9CFFFF9CFFFF9CFFFFA5F7FFD6FFFFFF00FF9C9CA5FFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFF5ABDDEDEFFFFC6FFFFC6FFFFCEFFFFC6FFFFC6FFFFE7FFFFFF
          00FFBDBDBDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC6C6C65ABDDE009CCE
          0094CE0094CE0094CE0094CE5ABDDEFF00FFBDBDBDBDBDBDBDBDBDBDBDBDBDBD
          BDBDBDBDBDBDBDBDBDBDFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
          00FF}
        Proportional = True
        Transparent = True
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 47
    Width = 756
    Height = 401
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    ExplicitWidth = 949
    ExplicitHeight = 551
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 756
      Height = 401
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 1186
      ExplicitHeight = 689
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 756
        Height = 401
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        TabOrder = 0
        ExplicitWidth = 1186
        ExplicitHeight = 689
        object PnConectar: TPanel
          Left = 2
          Top = 15
          Width = 752
          Height = 60
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alTop
          ParentBackground = False
          TabOrder = 0
          ExplicitTop = 18
          ExplicitWidth = 1182
          object Label4: TLabel
            Left = 14
            Top = 7
            Width = 42
            Height = 13
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Servidor:'
          end
          object SBFTPConfig: TSpeedButton
            Left = 825
            Top = 27
            Width = 25
            Height = 26
            Hint = 'Inclui item de carteira'
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = '...'
            OnClick = SBFTPConfigClick
          end
          object EdFTPConfig: TdmkEditCB
            Left = 14
            Top = 27
            Width = 68
            Height = 26
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            DBLookupComboBox = CBFTPConfig
            IgnoraDBLookupComboBox = False
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
          object CBFTPConfig: TdmkDBLookupComboBox
            Left = 84
            Top = 27
            Width = 738
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            KeyField = 'Codigo'
            ListField = 'Nome'
            ListSource = DsFTPConfig
            TabOrder = 1
            dmkEditCB = EdFTPConfig
            UpdType = utYes
            LocF7SQLMasc = '$#'
            LocF7PreDefProc = f7pNone
          end
          object BtConectar: TBitBtn
            Tag = 561
            Left = 858
            Top = 3
            Width = 136
            Height = 49
            Cursor = crHandPoint
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = '&Conectar'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -15
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            NumGlyphs = 2
            ParentFont = False
            ParentShowHint = False
            ShowHint = True
            TabOrder = 2
            OnClick = BtConectarClick
          end
        end
        object PnDados: TPanel
          Left = 2
          Top = 75
          Width = 752
          Height = 324
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 1
          ExplicitTop = 78
          ExplicitWidth = 1182
          ExplicitHeight = 609
          object PnFTP: TPanel
            Left = 0
            Top = 0
            Width = 752
            Height = 324
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alClient
            BevelOuter = bvNone
            TabOrder = 0
            ExplicitWidth = 1182
            ExplicitHeight = 609
            object Panel6: TPanel
              Left = 0
              Top = 0
              Width = 752
              Height = 49
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alTop
              BevelOuter = bvNone
              TabOrder = 0
              ExplicitWidth = 1182
              object BtOk: TBitBtn
                Tag = 14
                Left = 1132
                Top = 0
                Width = 50
                Height = 49
                Cursor = crHandPoint
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Align = alRight
                NumGlyphs = 2
                ParentShowHint = False
                ShowHint = True
                TabOrder = 0
                OnClick = BtOkClick
              end
              object Panel7: TPanel
                Left = 0
                Top = 0
                Width = 1132
                Height = 49
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Align = alClient
                BevelOuter = bvNone
                TabOrder = 1
                object EdCaminho: TdmkEdit
                  Left = 0
                  Top = 23
                  Width = 1132
                  Height = 26
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Align = alBottom
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -15
                  Font.Name = 'MS Sans Serif'
                  Font.Style = []
                  ParentFont = False
                  TabOrder = 0
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = True
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = ''
                  ValWarn = False
                end
              end
            end
            object PnDiretorios: TPanel
              Left = 0
              Top = 49
              Width = 431
              Height = 275
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alLeft
              BevelOuter = bvNone
              TabOrder = 1
              ExplicitHeight = 560
              object StaticText3: TStaticText
                Left = 0
                Top = 0
                Width = 431
                Height = 35
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Align = alTop
                Alignment = taCenter
                BorderStyle = sbsSunken
                Caption = 'Diret'#243'rios'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -27
                Font.Name = 'Times New Roman'
                Font.Style = []
                ParentFont = False
                TabOrder = 0
                ExplicitWidth = 113
              end
              object PnMenu1: TPanel
                Left = 0
                Top = 35
                Width = 431
                Height = 59
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Align = alTop
                TabOrder = 1
                object BtHome: TBitBtn
                  Tag = 10072
                  Left = 5
                  Top = 5
                  Width = 49
                  Height = 49
                  Cursor = crHandPoint
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  NumGlyphs = 2
                  ParentShowHint = False
                  ShowHint = True
                  TabOrder = 0
                  OnClick = BtHomeClick
                end
                object BtUpFolder: TBitBtn
                  Tag = 10070
                  Left = 54
                  Top = 5
                  Width = 49
                  Height = 49
                  Cursor = crHandPoint
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  NumGlyphs = 2
                  ParentShowHint = False
                  ShowHint = True
                  TabOrder = 1
                  OnClick = BtUpFolderClick
                end
                object BtVoltar: TBitBtn
                  Tag = 10071
                  Left = 103
                  Top = 5
                  Width = 50
                  Height = 49
                  Cursor = crHandPoint
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  NumGlyphs = 2
                  ParentShowHint = False
                  ShowHint = True
                  TabOrder = 2
                  OnClick = BtVoltarClick
                end
                object BtIncluiDir: TBitBtn
                  Tag = 10077
                  Left = 153
                  Top = 5
                  Width = 49
                  Height = 49
                  Cursor = crHandPoint
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  NumGlyphs = 2
                  ParentShowHint = False
                  ShowHint = True
                  TabOrder = 3
                  OnClick = BtIncluiDirClick
                end
                object BtAlteraDir: TBitBtn
                  Tag = 11
                  Left = 202
                  Top = 5
                  Width = 49
                  Height = 49
                  Cursor = crHandPoint
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  NumGlyphs = 2
                  ParentShowHint = False
                  ShowHint = True
                  TabOrder = 4
                  OnClick = BtAlteraDirClick
                end
                object BtExcluiDir: TBitBtn
                  Tag = 12
                  Left = 251
                  Top = 5
                  Width = 49
                  Height = 49
                  Cursor = crHandPoint
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  NumGlyphs = 2
                  ParentShowHint = False
                  ShowHint = True
                  TabOrder = 5
                  OnClick = BtExcluiDirClick
                end
                object BtFavoritos: TBitBtn
                  Tag = 10076
                  Left = 300
                  Top = 5
                  Width = 50
                  Height = 49
                  Cursor = crHandPoint
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  NumGlyphs = 2
                  ParentShowHint = False
                  ShowHint = True
                  TabOrder = 6
                  OnClick = BtFavoritosClick
                end
                object BtPesquisa: TBitBtn
                  Tag = 22
                  Left = 350
                  Top = 5
                  Width = 49
                  Height = 49
                  Cursor = crHandPoint
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  NumGlyphs = 2
                  ParentShowHint = False
                  ShowHint = True
                  TabOrder = 7
                  OnClick = BtPesquisaClick
                end
              end
              object PCDiretorios: TPageControl
                Left = 0
                Top = 94
                Width = 431
                Height = 181
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                ActivePage = TabSheet3
                Align = alClient
                TabOrder = 2
                OnChange = PCDiretoriosChange
                ExplicitHeight = 328
                object TabSheet1: TTabSheet
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Caption = 'Disco WEB'
                  object LvDiretorios: TListView
                    Left = 0
                    Top = 0
                    Width = 423
                    Height = 435
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    Align = alClient
                    Columns = <
                      item
                        AutoSize = True
                        Caption = 'Descri'#231#227'o'
                      end>
                    ReadOnly = True
                    RowSelect = True
                    SmallImages = ImageList1
                    TabOrder = 0
                    ViewStyle = vsReport
                    OnDblClick = LvDiretoriosDblClick
                    OnKeyPress = LvDiretoriosKeyPress
                  end
                end
                object TabSheet3: TTabSheet
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Caption = 'Meus diret'#243'rios'
                  ImageIndex = 2
                  object DBGItens: TDBGrid
                    Left = 0
                    Top = 0
                    Width = 423
                    Height = 183
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    Align = alClient
                    DataSource = DsFTPWebDir
                    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
                    TabOrder = 0
                    TitleFont.Charset = ANSI_CHARSET
                    TitleFont.Color = clWindowText
                    TitleFont.Height = -11
                    TitleFont.Name = 'MS Sans Serif'
                    TitleFont.Style = []
                    OnDrawColumnCell = DBGItensDrawColumnCell
                    Columns = <
                      item
                        Expanded = False
                        FieldName = 'Nome'
                        Title.Caption = 'Descri'#231#227'o'
                        Width = 100
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'Pasta'
                        Title.Caption = 'Diret'#243'rio'
                        Width = 100
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'Codigo'
                        Title.Caption = 'ID'
                        Width = 65
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'TipoDir_TXT'
                        Title.Caption = 'Tipo de diret'#243'rio'
                        Visible = True
                      end
                      item
                        Expanded = False
                        Width = 25
                        Visible = True
                      end>
                  end
                  object GroupBox2: TGroupBox
                    Left = 0
                    Top = 183
                    Width = 423
                    Height = 252
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    Align = alBottom
                    Caption = 'Compartilhamento'
                    TabOrder = 1
                    object Label5: TLabel
                      Left = 10
                      Top = 18
                      Width = 394
                      Height = 16
                      Margins.Left = 4
                      Margins.Top = 4
                      Margins.Right = 4
                      Margins.Bottom = 4
                      AutoSize = False
                      Caption = 'Empresa:'
                      Font.Charset = ANSI_CHARSET
                      Font.Color = clWindowText
                      Font.Height = -15
                      Font.Name = 'MS Sans Serif'
                      Font.Style = [fsBold]
                      ParentFont = False
                    end
                    object DBText7: TDBText
                      Left = 10
                      Top = 43
                      Width = 394
                      Height = 16
                      Margins.Left = 4
                      Margins.Top = 4
                      Margins.Right = 4
                      Margins.Bottom = 4
                      DataField = 'CliInt_TXT'
                      DataSource = DsFTPWebDir
                      Font.Charset = ANSI_CHARSET
                      Font.Color = clWindowText
                      Font.Height = -15
                      Font.Name = 'MS Sans Serif'
                      Font.Style = []
                      ParentFont = False
                    end
                    object DBCGNivel: TdmkDBCheckGroup
                      Left = 2
                      Top = 65
                      Width = 419
                      Height = 74
                      Margins.Left = 4
                      Margins.Top = 4
                      Margins.Right = 4
                      Margins.Bottom = 4
                      Align = alBottom
                      Caption = 'N'#237'veis de permiss'#227'o'
                      Columns = 3
                      DataField = 'Nivel'
                      DataSource = DsFTPWebDir
                      Items.Strings = (
                        'N'#237'vel 0'
                        'N'#237'vel 1'
                        'N'#237'vel 2'
                        'N'#237'vel 3'
                        'N'#237'vel 4')
                      ParentBackground = False
                      TabOrder = 0
                    end
                    object Panel5: TPanel
                      Left = 2
                      Top = 139
                      Width = 419
                      Height = 111
                      Margins.Left = 4
                      Margins.Top = 4
                      Margins.Right = 4
                      Margins.Bottom = 4
                      Align = alBottom
                      BevelOuter = bvNone
                      TabOrder = 1
                      object Panel8: TPanel
                        Left = 360
                        Top = 0
                        Width = 59
                        Height = 111
                        Margins.Left = 4
                        Margins.Top = 4
                        Margins.Right = 4
                        Margins.Bottom = 4
                        Align = alRight
                        BevelOuter = bvNone
                        TabOrder = 0
                        object BtInclui: TBitBtn
                          Tag = 10
                          Left = 5
                          Top = 5
                          Width = 49
                          Height = 49
                          Cursor = crHandPoint
                          Hint = 'Inclui nova entidade'
                          Margins.Left = 4
                          Margins.Top = 4
                          Margins.Right = 4
                          Margins.Bottom = 4
                          NumGlyphs = 2
                          ParentShowHint = False
                          ShowHint = True
                          TabOrder = 0
                          OnClick = BtIncluiClick
                        end
                        object BtExclui: TBitBtn
                          Tag = 12
                          Left = 5
                          Top = 54
                          Width = 49
                          Height = 49
                          Cursor = crHandPoint
                          Hint = 'Exclui entidade atual'
                          Margins.Left = 4
                          Margins.Top = 4
                          Margins.Right = 4
                          Margins.Bottom = 4
                          NumGlyphs = 2
                          ParentShowHint = False
                          ShowHint = True
                          TabOrder = 1
                          OnClick = BtExcluiClick
                        end
                      end
                      object DBGrid3: TDBGrid
                        Left = 0
                        Top = 0
                        Width = 360
                        Height = 111
                        Margins.Left = 4
                        Margins.Top = 4
                        Margins.Right = 4
                        Margins.Bottom = 4
                        Align = alClient
                        DataSource = DsFTPWebComDir
                        TabOrder = 1
                        TitleFont.Charset = ANSI_CHARSET
                        TitleFont.Color = clWindowText
                        TitleFont.Height = -11
                        TitleFont.Name = 'MS Sans Serif'
                        TitleFont.Style = []
                        Columns = <
                          item
                            Expanded = False
                            FieldName = 'Entidade_TXT'
                            Title.Caption = 'Entidades'
                            Width = 190
                            Visible = True
                          end
                          item
                            Expanded = False
                            FieldName = 'Controle'
                            Title.Caption = 'ID'
                            Width = 65
                            Visible = True
                          end>
                      end
                    end
                  end
                end
              end
            end
            object PnItens: TPanel
              Left = 431
              Top = 49
              Width = 321
              Height = 275
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alClient
              BevelOuter = bvNone
              TabOrder = 2
              ExplicitWidth = 751
              ExplicitHeight = 560
              object StaticText1: TStaticText
                Left = 0
                Top = 0
                Width = 321
                Height = 35
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Align = alTop
                Alignment = taCenter
                BorderStyle = sbsSunken
                Caption = 'Arquivos'
                Color = clBtnFace
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -27
                Font.Name = 'Times New Roman'
                Font.Style = []
                ParentColor = False
                ParentFont = False
                TabOrder = 0
                ExplicitWidth = 103
              end
              object PnMenu2: TPanel
                Left = 0
                Top = 35
                Width = 321
                Height = 59
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Align = alTop
                TabOrder = 1
                ExplicitWidth = 751
                object LaInfo: TLabel
                  Left = 712
                  Top = 1
                  Width = 31
                  Height = 13
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Align = alRight
                  Caption = '0 itens'
                end
                object BtUpload: TBitBtn
                  Tag = 10075
                  Left = 6
                  Top = 5
                  Width = 49
                  Height = 49
                  Cursor = crHandPoint
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  NumGlyphs = 2
                  ParentShowHint = False
                  ShowHint = True
                  TabOrder = 0
                  OnClick = BtUploadClick
                end
                object BtDownload: TBitBtn
                  Tag = 10073
                  Left = 55
                  Top = 5
                  Width = 50
                  Height = 49
                  Cursor = crHandPoint
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  NumGlyphs = 2
                  ParentShowHint = False
                  ShowHint = True
                  TabOrder = 1
                  OnClick = BtDownloadClick
                end
                object BtAlteraArq: TBitBtn
                  Tag = 11
                  Left = 105
                  Top = 5
                  Width = 49
                  Height = 49
                  Cursor = crHandPoint
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  NumGlyphs = 2
                  ParentShowHint = False
                  ShowHint = True
                  TabOrder = 2
                  OnClick = BtAlteraArqClick
                end
                object BtExcluiArq: TBitBtn
                  Tag = 12
                  Left = 154
                  Top = 5
                  Width = 49
                  Height = 49
                  Cursor = crHandPoint
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  NumGlyphs = 2
                  ParentShowHint = False
                  ShowHint = True
                  TabOrder = 3
                  OnClick = BtExcluiArqClick
                end
                object BtPesqArq: TBitBtn
                  Tag = 22
                  Left = 203
                  Top = 5
                  Width = 49
                  Height = 49
                  Cursor = crHandPoint
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  NumGlyphs = 2
                  ParentShowHint = False
                  ShowHint = True
                  TabOrder = 4
                  OnClick = BtPesqArqClick
                end
                object BtLinkArq: TBitBtn
                  Tag = 311
                  Left = 252
                  Top = 5
                  Width = 50
                  Height = 49
                  Cursor = crHandPoint
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  NumGlyphs = 2
                  ParentShowHint = False
                  ShowHint = True
                  TabOrder = 5
                  OnClick = BtLinkArqClick
                end
              end
              object PCArquivos: TPageControl
                Left = 0
                Top = 94
                Width = 321
                Height = 181
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                ActivePage = TSArquivosWEB
                Align = alClient
                TabOrder = 2
                ExplicitWidth = 514
                ExplicitHeight = 328
                object TSArquivosWEB: TTabSheet
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Caption = 'Arquivos WEB'
                  object LvArquivos: TListView
                    Left = 0
                    Top = 0
                    Width = 226
                    Height = 435
                    Hint = 'Remote system files'
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    Align = alClient
                    Columns = <
                      item
                        AutoSize = True
                        Caption = 'Descri'#231#227'o'
                        WidthType = (
                          -271)
                      end
                      item
                        Alignment = taRightJustify
                        Caption = 'Tamanho'
                        Width = 185
                      end
                      item
                        Caption = 'Modificado em'
                        Width = 308
                      end>
                    ColumnClick = False
                    MultiSelect = True
                    ReadOnly = True
                    RowSelect = True
                    SortType = stData
                    TabOrder = 0
                    ViewStyle = vsReport
                    OnSelectItem = LvArquivosSelectItem
                  end
                  object GBDetalhes: TGroupBox
                    Left = 226
                    Top = 0
                    Width = 517
                    Height = 435
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    Align = alRight
                    Caption = 'Detalhes'
                    TabOrder = 1
                    object Label1: TLabel
                      Left = 12
                      Top = 74
                      Width = 222
                      Height = 16
                      Margins.Left = 4
                      Margins.Top = 4
                      Margins.Right = 4
                      Margins.Bottom = 4
                      AutoSize = False
                      Caption = 'Tamanho:'
                      Font.Charset = ANSI_CHARSET
                      Font.Color = clWindowText
                      Font.Height = -15
                      Font.Name = 'MS Sans Serif'
                      Font.Style = [fsBold]
                      ParentFont = False
                    end
                    object ImgIcon: TImage
                      Left = 289
                      Top = 25
                      Width = 222
                      Height = 166
                      Margins.Left = 4
                      Margins.Top = 4
                      Margins.Right = 4
                      Margins.Bottom = 4
                      Center = True
                      IncrementalDisplay = True
                      Proportional = True
                      Stretch = True
                      Transparent = True
                    end
                    object LaTamanho: TLabel
                      Left = 12
                      Top = 98
                      Width = 222
                      Height = 16
                      Margins.Left = 4
                      Margins.Top = 4
                      Margins.Right = 4
                      Margins.Bottom = 4
                      AutoSize = False
                      Caption = '0,000 MB'
                      Font.Charset = ANSI_CHARSET
                      Font.Color = clWindowText
                      Font.Height = -15
                      Font.Name = 'MS Sans Serif'
                      Font.Style = []
                      ParentFont = False
                    end
                    object LaModify: TLabel
                      Left = 12
                      Top = 148
                      Width = 222
                      Height = 16
                      Margins.Left = 4
                      Margins.Top = 4
                      Margins.Right = 4
                      Margins.Bottom = 4
                      AutoSize = False
                      Caption = '00/00/0000 00:00:00'
                      Font.Charset = ANSI_CHARSET
                      Font.Color = clWindowText
                      Font.Height = -15
                      Font.Name = 'MS Sans Serif'
                      Font.Style = []
                      ParentFont = False
                    end
                    object Label3: TLabel
                      Left = 12
                      Top = 123
                      Width = 222
                      Height = 16
                      Margins.Left = 4
                      Margins.Top = 4
                      Margins.Right = 4
                      Margins.Bottom = 4
                      AutoSize = False
                      Caption = 'Modificado em:'
                      Font.Charset = ANSI_CHARSET
                      Font.Color = clWindowText
                      Font.Height = -15
                      Font.Name = 'MS Sans Serif'
                      Font.Style = [fsBold]
                      ParentFont = False
                    end
                    object Label2: TLabel
                      Left = 12
                      Top = 25
                      Width = 222
                      Height = 16
                      Margins.Left = 4
                      Margins.Top = 4
                      Margins.Right = 4
                      Margins.Bottom = 4
                      AutoSize = False
                      Caption = 'Arquivo:'
                      Font.Charset = ANSI_CHARSET
                      Font.Color = clWindowText
                      Font.Height = -15
                      Font.Name = 'MS Sans Serif'
                      Font.Style = [fsBold]
                      ParentFont = False
                    end
                    object LaArquivo: TLabel
                      Left = 12
                      Top = 49
                      Width = 222
                      Height = 16
                      Margins.Left = 4
                      Margins.Top = 4
                      Margins.Right = 4
                      Margins.Bottom = 4
                      AutoSize = False
                      Caption = 'Arquivo'
                      Font.Charset = ANSI_CHARSET
                      Font.Color = clWindowText
                      Font.Height = -15
                      Font.Name = 'MS Sans Serif'
                      Font.Style = []
                      ParentFont = False
                    end
                  end
                end
                object TSArquivos: TTabSheet
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Caption = 'Meus arquivos'
                  ImageIndex = 1
                  object DBGItensArq: TDBGrid
                    Left = 0
                    Top = 0
                    Width = 226
                    Height = 435
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    Align = alClient
                    DataSource = DsFTPWebArq
                    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
                    TabOrder = 0
                    TitleFont.Charset = ANSI_CHARSET
                    TitleFont.Color = clWindowText
                    TitleFont.Height = -11
                    TitleFont.Name = 'MS Sans Serif'
                    TitleFont.Style = []
                    OnDrawColumnCell = DBGItensArqDrawColumnCell
                    Columns = <
                      item
                        Expanded = False
                        Width = 25
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'Nome'
                        Title.Caption = 'Descri'#231#227'o'
                        Width = 250
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'Arquivo'
                        Width = 250
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'VersaoArq'
                        Title.Caption = 'Vers'#227'o'
                        Width = 100
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'ArqTam_MB'
                        Title.Caption = 'Tamanho (MB)'
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'Codigo'
                        Title.Caption = 'ID'
                        Width = 65
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'DataHora'
                        Title.Caption = 'Cadastrado em'
                        Width = 120
                        Visible = True
                      end>
                  end
                  object GBDetalhesDB: TGroupBox
                    Left = 226
                    Top = 0
                    Width = 517
                    Height = 435
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    Align = alRight
                    Caption = 'Detalhes'
                    TabOrder = 1
                    object ImgIconDB: TImage
                      Left = 388
                      Top = 18
                      Width = 123
                      Height = 124
                      Margins.Left = 4
                      Margins.Top = 4
                      Margins.Right = 4
                      Margins.Bottom = 4
                      Center = True
                      IncrementalDisplay = True
                      Proportional = True
                      Stretch = True
                      Transparent = True
                    end
                    object DBText1: TDBText
                      Left = 12
                      Top = 150
                      Width = 468
                      Height = 16
                      Margins.Left = 4
                      Margins.Top = 4
                      Margins.Right = 4
                      Margins.Bottom = 4
                      DataField = 'Nome'
                      DataSource = DsFTPWebArq
                      Font.Charset = ANSI_CHARSET
                      Font.Color = clWindowText
                      Font.Height = -15
                      Font.Name = 'MS Sans Serif'
                      Font.Style = []
                      ParentFont = False
                    end
                    object Label11: TLabel
                      Left = 12
                      Top = 129
                      Width = 222
                      Height = 16
                      Margins.Left = 4
                      Margins.Top = 4
                      Margins.Right = 4
                      Margins.Bottom = 4
                      AutoSize = False
                      Caption = 'Descri'#231#227'o:'
                      Font.Charset = ANSI_CHARSET
                      Font.Color = clWindowText
                      Font.Height = -15
                      Font.Name = 'MS Sans Serif'
                      Font.Style = [fsBold]
                      ParentFont = False
                    end
                    object Label6: TLabel
                      Left = 12
                      Top = 18
                      Width = 222
                      Height = 16
                      Margins.Left = 4
                      Margins.Top = 4
                      Margins.Right = 4
                      Margins.Bottom = 4
                      AutoSize = False
                      Caption = 'Arquivo:'
                      Font.Charset = ANSI_CHARSET
                      Font.Color = clWindowText
                      Font.Height = -15
                      Font.Name = 'MS Sans Serif'
                      Font.Style = [fsBold]
                      ParentFont = False
                    end
                    object Label7: TLabel
                      Left = 12
                      Top = 55
                      Width = 222
                      Height = 16
                      Margins.Left = 4
                      Margins.Top = 4
                      Margins.Right = 4
                      Margins.Bottom = 4
                      AutoSize = False
                      Caption = 'Tamanho (MB):'
                      Font.Charset = ANSI_CHARSET
                      Font.Color = clWindowText
                      Font.Height = -15
                      Font.Name = 'MS Sans Serif'
                      Font.Style = [fsBold]
                      ParentFont = False
                    end
                    object Label9: TLabel
                      Left = 12
                      Top = 92
                      Width = 123
                      Height = 16
                      Margins.Left = 4
                      Margins.Top = 4
                      Margins.Right = 4
                      Margins.Bottom = 4
                      AutoSize = False
                      Caption = 'Cadastrado em:'
                      Font.Charset = ANSI_CHARSET
                      Font.Color = clWindowText
                      Font.Height = -15
                      Font.Name = 'MS Sans Serif'
                      Font.Style = [fsBold]
                      ParentFont = False
                    end
                    object DBText4: TDBText
                      Left = 12
                      Top = 37
                      Width = 222
                      Height = 16
                      Margins.Left = 4
                      Margins.Top = 4
                      Margins.Right = 4
                      Margins.Bottom = 4
                      DataField = 'Arquivo'
                      DataSource = DsFTPWebArq
                      Font.Charset = ANSI_CHARSET
                      Font.Color = clWindowText
                      Font.Height = -15
                      Font.Name = 'MS Sans Serif'
                      Font.Style = []
                      ParentFont = False
                    end
                    object DBText5: TDBText
                      Left = 12
                      Top = 74
                      Width = 222
                      Height = 16
                      Margins.Left = 4
                      Margins.Top = 4
                      Margins.Right = 4
                      Margins.Bottom = 4
                      DataField = 'ArqTam_MB'
                      DataSource = DsFTPWebArq
                      Font.Charset = ANSI_CHARSET
                      Font.Color = clWindowText
                      Font.Height = -15
                      Font.Name = 'MS Sans Serif'
                      Font.Style = []
                      ParentFont = False
                    end
                    object DBText6: TDBText
                      Left = 12
                      Top = 111
                      Width = 123
                      Height = 16
                      Margins.Left = 4
                      Margins.Top = 4
                      Margins.Right = 4
                      Margins.Bottom = 4
                      DataField = 'DataCad'
                      DataSource = DsFTPWebArq
                      Font.Charset = ANSI_CHARSET
                      Font.Color = clWindowText
                      Font.Height = -15
                      Font.Name = 'MS Sans Serif'
                      Font.Style = []
                      ParentFont = False
                    end
                    object Label10: TLabel
                      Left = 148
                      Top = 92
                      Width = 123
                      Height = 16
                      Margins.Left = 4
                      Margins.Top = 4
                      Margins.Right = 4
                      Margins.Bottom = 4
                      AutoSize = False
                      Caption = 'Modificado em:'
                      Font.Charset = ANSI_CHARSET
                      Font.Color = clWindowText
                      Font.Height = -15
                      Font.Name = 'MS Sans Serif'
                      Font.Style = [fsBold]
                      ParentFont = False
                    end
                    object DBText9: TDBText
                      Left = 148
                      Top = 111
                      Width = 123
                      Height = 16
                      Margins.Left = 4
                      Margins.Top = 4
                      Margins.Right = 4
                      Margins.Bottom = 4
                      DataField = 'DataAlt'
                      DataSource = DsFTPWebArq
                      Font.Charset = ANSI_CHARSET
                      Font.Color = clWindowText
                      Font.Height = -15
                      Font.Name = 'MS Sans Serif'
                      Font.Style = []
                      ParentFont = False
                    end
                    object GroupBox3: TGroupBox
                      Left = 2
                      Top = 180
                      Width = 513
                      Height = 253
                      Margins.Left = 4
                      Margins.Top = 4
                      Margins.Right = 4
                      Margins.Bottom = 4
                      Align = alBottom
                      Caption = 'Compartilhamento'
                      TabOrder = 0
                      object Label12: TLabel
                        Left = 10
                        Top = 18
                        Width = 468
                        Height = 16
                        Margins.Left = 4
                        Margins.Top = 4
                        Margins.Right = 4
                        Margins.Bottom = 4
                        AutoSize = False
                        Caption = 'Empresa:'
                        Font.Charset = ANSI_CHARSET
                        Font.Color = clWindowText
                        Font.Height = -15
                        Font.Name = 'MS Sans Serif'
                        Font.Style = [fsBold]
                        ParentFont = False
                      end
                      object DBText2: TDBText
                        Left = 10
                        Top = 43
                        Width = 468
                        Height = 16
                        Margins.Left = 4
                        Margins.Top = 4
                        Margins.Right = 4
                        Margins.Bottom = 4
                        DataField = 'CliInt_TXT'
                        DataSource = DsFTPWebArq
                        Font.Charset = ANSI_CHARSET
                        Font.Color = clWindowText
                        Font.Height = -15
                        Font.Name = 'MS Sans Serif'
                        Font.Style = []
                        ParentFont = False
                      end
                      object DBCGNivelArq: TdmkDBCheckGroup
                        Left = 2
                        Top = 66
                        Width = 509
                        Height = 74
                        Margins.Left = 4
                        Margins.Top = 4
                        Margins.Right = 4
                        Margins.Bottom = 4
                        Align = alBottom
                        Caption = 'N'#237'veis de permiss'#227'o'
                        Columns = 3
                        DataField = 'Nivel'
                        DataSource = DsFTPWebArq
                        Items.Strings = (
                          'N'#237'vel 0'
                          'N'#237'vel 1'
                          'N'#237'vel 2'
                          'N'#237'vel 3'
                          'N'#237'vel 4')
                        ParentBackground = False
                        TabOrder = 0
                      end
                      object Panel9: TPanel
                        Left = 2
                        Top = 140
                        Width = 509
                        Height = 111
                        Margins.Left = 4
                        Margins.Top = 4
                        Margins.Right = 4
                        Margins.Bottom = 4
                        Align = alBottom
                        BevelOuter = bvNone
                        TabOrder = 1
                        object Panel10: TPanel
                          Left = 450
                          Top = 0
                          Width = 59
                          Height = 111
                          Margins.Left = 4
                          Margins.Top = 4
                          Margins.Right = 4
                          Margins.Bottom = 4
                          Align = alRight
                          BevelOuter = bvNone
                          TabOrder = 0
                          object BitBtn2: TBitBtn
                            Tag = 10
                            Left = 5
                            Top = 5
                            Width = 49
                            Height = 49
                            Cursor = crHandPoint
                            Hint = 'Inclui nova entidade'
                            Margins.Left = 4
                            Margins.Top = 4
                            Margins.Right = 4
                            Margins.Bottom = 4
                            NumGlyphs = 2
                            ParentShowHint = False
                            ShowHint = True
                            TabOrder = 0
                            OnClick = BitBtn2Click
                          end
                          object BitBtn3: TBitBtn
                            Tag = 12
                            Left = 5
                            Top = 54
                            Width = 49
                            Height = 49
                            Cursor = crHandPoint
                            Hint = 'Exclui entidade atual'
                            Margins.Left = 4
                            Margins.Top = 4
                            Margins.Right = 4
                            Margins.Bottom = 4
                            NumGlyphs = 2
                            ParentShowHint = False
                            ShowHint = True
                            TabOrder = 1
                            OnClick = BitBtn3Click
                          end
                        end
                        object DBGrid1: TDBGrid
                          Left = 0
                          Top = 0
                          Width = 450
                          Height = 111
                          Margins.Left = 4
                          Margins.Top = 4
                          Margins.Right = 4
                          Margins.Bottom = 4
                          Align = alClient
                          DataSource = DsFTPWebComArq
                          TabOrder = 1
                          TitleFont.Charset = ANSI_CHARSET
                          TitleFont.Color = clWindowText
                          TitleFont.Height = -11
                          TitleFont.Name = 'MS Sans Serif'
                          TitleFont.Style = []
                          Columns = <
                            item
                              Expanded = False
                              FieldName = 'Entidade_TXT'
                              Title.Caption = 'Entidades'
                              Width = 190
                              Visible = True
                            end
                            item
                              Expanded = False
                              FieldName = 'Controle'
                              Title.Caption = 'ID'
                              Width = 65
                              Visible = True
                            end>
                        end
                      end
                    end
                  end
                end
                object TSArquivosWEBPesq: TTabSheet
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Caption = 'Pesquisa'
                  ImageIndex = 2
                  object Label14: TLabel
                    Left = 6
                    Top = 140
                    Width = 44
                    Height = 13
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    Caption = 'Empresa:'
                  end
                  object Label8: TLabel
                    Left = 6
                    Top = 196
                    Width = 48
                    Height = 13
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    Caption = 'Entidade: '
                  end
                  object DBText3: TDBText
                    Left = 6
                    Top = 246
                    Width = 709
                    Height = 21
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    Alignment = taRightJustify
                    DataField = 'Apelido'
                    DataSource = DsEntidades
                  end
                  object CBPesEmpresa: TdmkDBLookupComboBox
                    Left = 75
                    Top = 160
                    Width = 640
                    Height = 21
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    KeyField = 'Codigo'
                    ListField = 'NOMEFILIAL'
                    ListSource = DModG.DsEmpresas
                    TabOrder = 5
                    dmkEditCB = EdPesEmpresa
                    UpdType = utYes
                    LocF7SQLMasc = '$#'
                    LocF7PreDefProc = f7pNone
                  end
                  object EdPesEmpresa: TdmkEditCB
                    Left = 6
                    Top = 160
                    Width = 69
                    Height = 26
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    Alignment = taRightJustify
                    TabOrder = 4
                    FormatType = dmktfInteger
                    MskType = fmtNone
                    DecimalSize = 0
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ValMin = '-2147483647'
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '0'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 0
                    ValWarn = False
                    DBLookupComboBox = CBPesEmpresa
                    IgnoraDBLookupComboBox = False
                    AutoSetIfOnlyOneReg = setregOnlyManual
                  end
                  object EdPesNome: TdmkEdit
                    Left = 6
                    Top = 37
                    Width = 709
                    Height = 27
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    TabOrder = 1
                    FormatType = dmktfString
                    MskType = fmtNone
                    DecimalSize = 2
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = ''
                    ValWarn = False
                  end
                  object CkPesNome: TCheckBox
                    Left = 6
                    Top = 6
                    Width = 308
                    Height = 21
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    Caption = 'Descri'#231#227'o'
                    TabOrder = 0
                  end
                  object BitBtn1: TBitBtn
                    Tag = 18
                    Left = 6
                    Top = 277
                    Width = 148
                    Height = 49
                    Cursor = crHandPoint
                    Hint = 'Sai da janela atual'
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    Caption = '&Reabre'
                    NumGlyphs = 2
                    ParentShowHint = False
                    ShowHint = True
                    TabOrder = 8
                    OnClick = BitBtn1Click
                  end
                  object BtLimpar: TBitBtn
                    Tag = 169
                    Left = 160
                    Top = 277
                    Width = 148
                    Height = 49
                    Cursor = crHandPoint
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    Caption = 'R&edefinir'
                    NumGlyphs = 2
                    ParentShowHint = False
                    ShowHint = True
                    TabOrder = 9
                    OnClick = BtLimparClick
                  end
                  object EdPesTags: TdmkEdit
                    Left = 6
                    Top = 105
                    Width = 709
                    Height = 27
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    TabOrder = 3
                    FormatType = dmktfString
                    MskType = fmtNone
                    DecimalSize = 2
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = ''
                    ValWarn = False
                  end
                  object CkPesTags: TCheckBox
                    Left = 6
                    Top = 76
                    Width = 308
                    Height = 21
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    Caption = 'Tags'
                    TabOrder = 2
                  end
                  object BtDesiste: TBitBtn
                    Tag = 15
                    Left = 314
                    Top = 277
                    Width = 148
                    Height = 49
                    Hint = 'Desiste da inclus'#227'o / altera'#231#227'o'
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    Caption = '&Desiste'
                    NumGlyphs = 2
                    TabOrder = 10
                    OnClick = BtDesisteClick
                  end
                  object EdEntidade: TdmkEditCB
                    Left = 6
                    Top = 215
                    Width = 69
                    Height = 26
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    Alignment = taRightJustify
                    TabOrder = 6
                    FormatType = dmktfInteger
                    MskType = fmtNone
                    DecimalSize = 0
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ValMin = '-2147483647'
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '0'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 0
                    ValWarn = False
                    DBLookupComboBox = CBEntidade
                    IgnoraDBLookupComboBox = False
                    AutoSetIfOnlyOneReg = setregOnlyManual
                  end
                  object CBEntidade: TdmkDBLookupComboBox
                    Left = 75
                    Top = 215
                    Width = 640
                    Height = 21
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    KeyField = 'Codigo'
                    ListField = 'Nome'
                    ListSource = DsEntidades
                    TabOrder = 7
                    dmkEditCB = EdEntidade
                    UpdType = utYes
                    LocF7SQLMasc = '$#'
                    LocF7PreDefProc = f7pNone
                  end
                end
              end
            end
          end
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 448
    Width = 756
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    ExplicitTop = 598
    ExplicitWidth = 949
    object Panel4: TPanel
      Left = 2
      Top = 18
      Width = 1182
      Height = 34
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 16
        Top = 2
        Width = 150
        Height = 19
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -17
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 15
        Top = 1
        Width = 150
        Height = 19
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -17
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 492
    Width = 756
    Height = 69
    Align = alBottom
    TabOrder = 3
    ExplicitTop = 642
    ExplicitWidth = 949
    object PnSaiDesis: TPanel
      Left = 1007
      Top = 18
      Width = 177
      Height = 67
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 15
        Top = 4
        Width = 147
        Height = 49
        Cursor = crHandPoint
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 18
      Width = 1005
      Height = 67
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
    end
  end
  object CSTabSheetChamou: TdmkCompoStore
    Left = 420
    Top = 11
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 391
    Top = 11
  end
  object QrFTPConfig: TMySQLQuery
    Database = Dmod.MyDBn
    SQL.Strings = (
      'SELECT Codigo, Nome, AES_DECRYPT(Web_FTPs, :P0) SENHA, '
      'Web_MyURL, Web_FTPu, Web_FTPs, Web_FTPh, Web_Raiz, '
      'Passivo, Ativo'
      'FROM ftpconfig'
      'WHERE Ativo=1')
    Left = 420
    Top = 532
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrFTPConfigNome: TWideStringField
      FieldName = 'Nome'
      Origin = 'ftpconfig.Nome'
      Size = 50
    end
    object QrFTPConfigSENHA: TWideStringField
      FieldName = 'SENHA'
      Size = 50
    end
    object QrFTPConfigWeb_FTPu: TWideStringField
      FieldName = 'Web_FTPu'
      Origin = 'ftpconfig.Web_FTPu'
      Size = 50
    end
    object QrFTPConfigWeb_FTPs: TWideStringField
      FieldName = 'Web_FTPs'
      Origin = 'ftpconfig.Web_FTPs'
      Size = 50
    end
    object QrFTPConfigWeb_FTPh: TWideStringField
      FieldName = 'Web_FTPh'
      Origin = 'ftpconfig.Web_FTPh'
      Size = 255
    end
    object QrFTPConfigWeb_Raiz: TWideStringField
      FieldName = 'Web_Raiz'
      Origin = 'ftpconfig.Web_Raiz'
      Size = 50
    end
    object QrFTPConfigCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'ftpconfig.Codigo'
    end
    object QrFTPConfigAtivo: TSmallintField
      FieldName = 'Ativo'
      Origin = 'ftpconfig.Ativo'
    end
    object QrFTPConfigPassivo: TSmallintField
      FieldName = 'Passivo'
    end
    object QrFTPConfigWeb_MyURL: TWideStringField
      FieldName = 'Web_MyURL'
      Size = 255
    end
    object QrFTPConfigWebId: TWideStringField
      FieldName = 'WebId'
    end
  end
  object DsFTPConfig: TDataSource
    DataSet = QrFTPConfig
    Left = 448
    Top = 532
  end
  object QrFTPWebDir: TMySQLQuery
    Database = Dmod.MyDBn
    BeforeClose = QrFTPWebDirBeforeClose
    AfterScroll = QrFTPWebDirAfterScroll
    SQL.Strings = (
      'SELECT *'
      'FROM ftpwebdir'
      'WHERE FTPConfig=:P0'
      'AND TipoDir = 1'
      'ORDER BY Nome')
    Left = 477
    Top = 532
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrFTPWebDirCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'ftpwebdir.Codigo'
    end
    object QrFTPWebDirNome: TWideStringField
      FieldName = 'Nome'
      Origin = 'ftpwebdir.Nome'
      Size = 50
    end
    object QrFTPWebDirPasta: TWideStringField
      FieldName = 'Pasta'
      Origin = 'ftpwebdir.Pasta'
      Size = 50
    end
    object QrFTPWebDirCaminho: TWideStringField
      FieldName = 'Caminho'
      Origin = 'ftpwebdir.Caminho'
      Size = 255
    end
    object QrFTPWebDirFTPConfig: TIntegerField
      FieldName = 'FTPConfig'
      Origin = 'ftpwebdir.FTPConfig'
    end
    object QrFTPWebDirLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'ftpwebdir.Lk'
    end
    object QrFTPWebDirDataCad: TDateField
      FieldName = 'DataCad'
      Origin = 'ftpwebdir.DataCad'
    end
    object QrFTPWebDirDataAlt: TDateField
      FieldName = 'DataAlt'
      Origin = 'ftpwebdir.DataAlt'
    end
    object QrFTPWebDirUserCad: TIntegerField
      FieldName = 'UserCad'
      Origin = 'ftpwebdir.UserCad'
    end
    object QrFTPWebDirUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Origin = 'ftpwebdir.UserAlt'
    end
    object QrFTPWebDirAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Origin = 'ftpwebdir.AlterWeb'
    end
    object QrFTPWebDirAtivo: TSmallintField
      FieldName = 'Ativo'
      Origin = 'ftpwebdir.Ativo'
    end
    object QrFTPWebDirOrdem: TIntegerField
      FieldName = 'Ordem'
      Origin = 'ftpwebdir.Ordem'
    end
    object QrFTPWebDirCliInt: TIntegerField
      FieldName = 'CliInt'
      Origin = 'ftpwebdir.CliInt'
    end
    object QrFTPWebDirArqPri: TIntegerField
      FieldName = 'ArqPri'
      Origin = 'ftpwebdir.ArqPri'
    end
    object QrFTPWebDirNivel: TIntegerField
      FieldName = 'Nivel'
      Origin = 'ftpwebdir.Nivel'
    end
    object QrFTPWebDirTipoDir: TSmallintField
      FieldName = 'TipoDir'
      Origin = 'ftpwebdir.TipoDir'
    end
    object QrFTPWebDirCliInt_TXT: TWideStringField
      FieldName = 'CliInt_TXT'
      Size = 100
    end
    object QrFTPWebDirTipoDir_TXT: TWideStringField
      FieldName = 'TipoDir_TXT'
      Size = 30
    end
  end
  object DsFTPWebDir: TDataSource
    DataSet = QrFTPWebDir
    Left = 505
    Top = 532
  end
  object QrFTPWebArq: TMySQLQuery
    Database = Dmod.MyDBn
    BeforeClose = QrFTPWebArqBeforeClose
    AfterScroll = QrFTPWebArqAfterScroll
    SQL.Strings = (
      'SELECT arq.Codigo, dir.Caminho, con.Nome NOMEFTPCONFIG, '
      'IF(arq.Versao=0, arq.VersaoArq, ver.Versao) VERSAO_TXT, '
      'IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NOMEEMP,'
      
        'CASE WHEN arq.Aplic > 0 THEN apl.Nome ELSE arq.Nome END NOMEARQU' +
        'IV,'
      'arq.ArqExt, arq.ArqTam, dir.FTPConfig, arq.*'
      'FROM ftpwebarq arq'
      'LEFT JOIN entidades emp ON emp.Codigo = arq.CliInt'
      'LEFT JOIN ftpwebdir dir ON dir.Codigo = arq.FTPDir'
      'LEFT JOIN ftpconfig con ON con.Codigo = dir.FTPConfig'
      'LEFT JOIN aplicativos apl ON apl.Codigo = arq.Aplic'
      'LEFT JOIN waplicver ver ON ver.Codigo = arq.Versao'
      'WHERE arq.FTPDir=:P0'
      'AND dir.TipoDir = 1'
      
        'AND CASE WHEN arq.Aplic > 0 THEN apl.Nome ELSE arq.Nome END LIKE' +
        ' :P1'
      'ORDER BY NOMEARQUIV')
    Left = 533
    Top = 532
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrFTPWebArqCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrFTPWebArqCliInt: TIntegerField
      FieldName = 'CliInt'
    end
    object QrFTPWebArqNome: TWideStringField
      FieldName = 'Nome'
    end
    object QrFTPWebArqArqTam: TFloatField
      FieldName = 'ArqTam'
      DisplayFormat = '#,###,###0.000'
    end
    object QrFTPWebArqFTPConfig: TIntegerField
      FieldName = 'FTPConfig'
    end
    object QrFTPWebArqFTPDir: TIntegerField
      FieldName = 'FTPDir'
    end
    object QrFTPWebArqArquivo: TWideStringField
      FieldName = 'Arquivo'
    end
    object QrFTPWebArqCaminho: TWideStringField
      FieldName = 'Caminho'
      Size = 255
    end
    object QrFTPWebArqNivel: TIntegerField
      FieldName = 'Nivel'
    end
    object QrFTPWebArqOrigem: TIntegerField
      FieldName = 'Origem'
    end
    object QrFTPWebArqCliInt_TXT: TWideStringField
      FieldName = 'CliInt_TXT'
      Size = 100
    end
    object QrFTPWebArqDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrFTPWebArqDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrFTPWebArqArqTam_MB: TFloatField
      FieldName = 'ArqTam_MB'
      DisplayFormat = '#,###,###0.000'
    end
    object QrFTPWebArqTags: TWideStringField
      FieldName = 'Tags'
      Size = 255
    end
    object QrFTPWebArqDataHora: TDateTimeField
      FieldName = 'DataHora'
    end
    object QrFTPWebArqVersaoArq: TWideStringField
      FieldName = 'VersaoArq'
      Size = 15
    end
    object QrFTPWebArqDataExp: TDateField
      FieldName = 'DataExp'
    end
  end
  object DsFTPWebArq: TDataSource
    DataSet = QrFTPWebArq
    Left = 561
    Top = 532
  end
  object QrLoc: TMySQLQuery
    Database = Dmod.MyDBn
    Left = 589
    Top = 532
  end
  object SaveFile: TSaveDialog
    DefaultExt = '*'
    Left = 304
    Top = 16
  end
  object OpenDialog1: TOpenDialog
    Options = [ofHideReadOnly, ofAllowMultiSelect, ofShareAware, ofEnableSizing]
    Left = 332
    Top = 16
  end
  object FTPGer: TIdFTP
    OnDisconnected = FTPGerDisconnected
    ConnectTimeout = 0
    TransferType = ftBinary
    NATKeepAlive.UseKeepAlive = False
    NATKeepAlive.IdleTimeMS = 0
    NATKeepAlive.IntervalMS = 0
    ProxySettings.ProxyType = fpcmNone
    ProxySettings.Port = 0
    OnAfterClientLogin = FTPGerAfterClientLogin
    Left = 880
    Top = 520
  end
  object QrFTPWebComArq: TMySQLQuery
    Database = Dmod.MyDBn
    SQL.Strings = (
      'SELECT arq.Codigo, dir.Caminho, con.Nome NOMEFTPCONFIG, '
      'IF(arq.Versao=0, arq.VersaoArq, ver.Versao) VERSAO_TXT, '
      'IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NOMEEMP,'
      
        'CASE WHEN arq.Aplic > 0 THEN apl.Nome ELSE arq.Nome END NOMEARQU' +
        'IV,'
      'arq.ArqExt, arq.ArqTam, dir.FTPConfig, arq.*'
      'FROM ftpwebarq arq'
      'LEFT JOIN entidades emp ON emp.Codigo = arq.CliInt'
      'LEFT JOIN ftpwebdir dir ON dir.Codigo = arq.FTPDir'
      'LEFT JOIN ftpconfig con ON con.Codigo = dir.FTPConfig'
      'LEFT JOIN aplicativos apl ON apl.Codigo = arq.Aplic'
      'LEFT JOIN waplicver ver ON ver.Codigo = arq.Versao'
      'WHERE arq.FTPDir=:P0'
      'AND dir.TipoDir = 1'
      
        'AND CASE WHEN arq.Aplic > 0 THEN apl.Nome ELSE arq.Nome END LIKE' +
        ' :P1'
      'ORDER BY NOMEARQUIV')
    Left = 617
    Top = 532
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrFTPWebComArqControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrFTPWebComArqEntidade_TXT: TWideStringField
      FieldName = 'Entidade_TXT'
      Size = 100
    end
  end
  object DsFTPWebComArq: TDataSource
    DataSet = QrFTPWebComArq
    Left = 645
    Top = 532
  end
  object DsFTPWebComDir: TDataSource
    DataSet = QrFTPWebComDir
    Left = 701
    Top = 532
  end
  object QrFTPWebComDir: TMySQLQuery
    Database = Dmod.MyDBn
    SQL.Strings = (
      'SELECT arq.Codigo, dir.Caminho, con.Nome NOMEFTPCONFIG, '
      'IF(arq.Versao=0, arq.VersaoArq, ver.Versao) VERSAO_TXT, '
      'IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NOMEEMP,'
      
        'CASE WHEN arq.Aplic > 0 THEN apl.Nome ELSE arq.Nome END NOMEARQU' +
        'IV,'
      'arq.ArqExt, arq.ArqTam, dir.FTPConfig, arq.*'
      'FROM ftpwebarq arq'
      'LEFT JOIN entidades emp ON emp.Codigo = arq.CliInt'
      'LEFT JOIN ftpwebdir dir ON dir.Codigo = arq.FTPDir'
      'LEFT JOIN ftpconfig con ON con.Codigo = dir.FTPConfig'
      'LEFT JOIN aplicativos apl ON apl.Codigo = arq.Aplic'
      'LEFT JOIN waplicver ver ON ver.Codigo = arq.Versao'
      'WHERE arq.FTPDir=:P0'
      'AND dir.TipoDir = 1'
      
        'AND CASE WHEN arq.Aplic > 0 THEN apl.Nome ELSE arq.Nome END LIKE' +
        ' :P1'
      'ORDER BY NOMEARQUIV')
    Left = 673
    Top = 532
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrFTPWebComDirControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrFTPWebComDirEntidade_TXT: TWideStringField
      FieldName = 'Entidade_TXT'
      Size = 100
    end
  end
  object QrEntidades: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo,  '
      'IF(Tipo=0, RazaoSocial, Nome) Nome, '
      ' IF(Tipo=0, Fantasia, Apelido) Apelido'
      'FROM entidades'
      'ORDER BY Nome')
    Left = 729
    Top = 532
    object QrEntidadesCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEntidadesApelido: TWideStringField
      FieldName = 'Apelido'
      Size = 60
    end
    object QrEntidadesNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
  object DsEntidades: TDataSource
    DataSet = QrEntidades
    Left = 757
    Top = 532
  end
  object ImageList1: TImageList
    Left = 568
    Top = 24
    Bitmap = {
      494C010102000800040010001000FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      0000000000003600000028000000400000001000000001002000000000000010
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000002A2A
      2A00353535003535350035353500353535003535350035353500353535003535
      3500353535003535350035353500000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000042A5C60084E7FF0073DEFF006BDE
      FF006BE7FF006BE7FF006BE7FF006BE7FF006BE7FF006BE7FF006BE7FF006BE7
      FF006BDEFF0073E7FF0063CEEF00000000000000000000000000000000002A2A
      2A00FFFFFF00F9F9F900F2F2F200EAEAEA00DFDFDF00D5D5D500CBCBCB00C1C1
      C100B8B8B800B1B1B10035353500000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000009CCE0031D6FF0010CEFF0010CE
      FF0008CEFF0008CEFF0008CEFF0008CEFF0008CEFF0008CEFF0008CEFF0008CE
      FF0010CEFF0018CEFF0094E7FF00000000000000000000000000000000002A2A
      2A00FFFFFF00F9F9F900F2F2F200EAEAEA00DFDFDF00D5D5D500CBCBCB00C1C1
      C100B8B8B800B1B1B10035353500000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000094CE0039D6FF0018D6FF0018D6
      FF0018D6FF0018D6FF0018D6FF0018D6FF0018D6FF0018D6FF0018D6FF0018D6
      FF0018D6FF0021D6FF009CEFFF00000000000000000000000000000000002A2A
      2A00FFFFFF00F9F9F900F2F2F200EAEAEA00DFDFDF00D5D5D500CBCBCB00C1C1
      C100B8B8B800B1B1B10035353500000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000094CE004ADEFF0029D6FF0029D6
      FF0029D6FF0029D6FF0029D6FF0029D6FF0029D6FF0029D6FF0029D6FF0029D6
      FF0021D6FF0031D6FF009CEFFF00000000000000000000000000000000002A2A
      2A00FFFFFF00F9F9F900F2F2F200EAEAEA00DFDFDF00D5D5D500CBCBCB00C1C1
      C100B8B8B800B1B1B10035353500000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000094CE0052DEFF0039DEFF0039DE
      FF0039DEFF0039DEFF0039DEFF0039DEFF0039DEFF0039DEFF0039DEFF0039DE
      FF0039DEFF0042DEFF00ADEFFF00000000000000000000000000000000002A2A
      2A00FFFFFF00F9F9F900F2F2F200EAEAEA00DFDFDF00D5D5D500CBCBCB00C1C1
      C100B8B8B800B1B1B10035353500000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000094CE0063E7FF0042E7FF0042E7
      FF0042E7FF0042E7FF0042E7FF0042E7FF0042E7FF0042E7FF0042E7FF0042E7
      FF004AE7FF004AE7FF00ADF7FF00000000000000000000000000000000002A2A
      2A00FFFFFF00F9F9F900F2F2F200EAEAEA00DFDFDF00D5D5D500CBCBCB00C1C1
      C100B8B8B800B1B1B10035353500000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000009CCE006BE7FF0052E7FF0052E7
      FF0052E7FF0052E7FF0052E7FF0052E7FF0052E7FF0052E7FF0052E7FF005AE7
      FF0052E7FF005AE7FF00B5F7FF00000000000000000000000000000000002A2A
      2A00FFFFFF00F9F9F900F2F2F200EAEAEA00DFDFDF00D5D5D500CBCBCB00C1C1
      C100B8B8B800B1B1B10035353500000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000009CCE007BEFFF0063EFFF0063EF
      FF0063EFFF0063EFFF0063EFFF0063EFFF0063EFFF0063EFFF0063EFFF0063EF
      FF0063EFFF006BEFFF00BDF7FF00000000000000000000000000000000002A2A
      2A00FFFFFF00F9F9F900F2F2F200EAEAEA00DFDFDF00D5D5D500CBCBCB00C1C1
      C100B8B8B800B1B1B10035353500000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000094CE008CF7FF007BF7FF007BEF
      FF007BF7FF007BEFFF007BEFFF0073EFFF0073EFFF0073EFFF0073EFFF0073EF
      FF0073F7FF007BEFFF00C6F7FF00000000000000000000000000000000002A2A
      2A00FFFFFF00F9F9F900F2F2F200EAEAEA00DFDFDF00D5D5D500CBCBCB00C1C1
      C100B8B8B800B1B1B10035353500000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000009CCE00B5F7FF00A5F7FF00A5F7
      FF00A5F7FF00A5F7FF009CF7FF0094F7FF0084F7FF007BF7FF0084F7FF0084F7
      FF007BF7FF0084F7FF00CEF7FF00000000000000000000000000000000002A2A
      2A00FFFFFF00F9F9F900F2F2F200EAEAEA00DFDFDF00D5D5D500CBCBCB00C1C1
      C100B8B8B800B1B1B10035353500000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000003994BD00E7FFFF00E7FFFF00E7FF
      FF00DEFFFF00E7FFFF00DEFFFF00C6FFFF00A5FFFF009CFFFF009CFFFF009CFF
      FF009CFFFF00A5F7FF00D6FFFF00000000000000000000000000000000002A2A
      2A00FFFFFF00F9F9F900F2F2F200EAEAEA00DFDFDF00D5D5D500CBCBCB003434
      3400343434003434340035353500000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000009C9CA500FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF005ABDDE00DEFFFF00C6FFFF00C6FFFF00CEFF
      FF00C6FFFF00C6FFFF00E7FFFF00000000000000000000000000000000002A2A
      2A00FFFFFF00F9F9F900F2F2F200EAEAEA00DFDFDF00D5D5D500CBCBCB003434
      3400ABABAB00ABABAB0035353500000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000BDBDBD00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00C6C6C6005ABDDE00009CCE000094CE000094
      CE000094CE000094CE005ABDDE00000000000000000000000000000000002A2A
      2A00FFFFFF00F9F9F900F2F2F200EAEAEA00DFDFDF00D5D5D500CBCBCB003434
      3400ABABAB003535350000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000BDBDBD00BDBDBD00BDBDBD00BDBD
      BD00BDBDBD00BDBDBD00BDBDBD00BDBDBD000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000002A2A
      2A00343434003434340034343400343434003434340034343400343434003434
      3400353535000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000424D3E000000000000003E000000
      2800000040000000100000000100010000000000800000000000000000000000
      000000000000000000000000FFFFFF00FFFFFFFF00000000FFFFE00100000000
      0001E001000000000001E001000000000001E001000000000001E00100000000
      0001E001000000000001E001000000000001E001000000000001E00100000000
      0001E001000000000001E001000000000001E001000000000001E00100000000
      0001E0030000000000FFE0070000000000000000000000000000000000000000
      000000000000}
  end
end
