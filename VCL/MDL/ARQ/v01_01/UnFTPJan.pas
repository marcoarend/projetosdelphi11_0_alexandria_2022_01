unit UnFTPJan;

interface

uses mySQLDbTables, UMySQLModule, dmkGeral, ModuleGeral, dmkImage, Forms,
  Controls, Windows, SysUtils, ComCtrls, Grids, DBGrids, UnDmkProcFunc,
  ExtCtrls, dmkDBGrid, Math, UnDmkEnums, UnProjGroup_Consts;

type
  TUnFTPJan = class(TObject)
  private

  public
    procedure MostraFTPConfig(Codigo: Integer);
    procedure MostraFTPWebArq(AbrirEmAba: Boolean; InOwner: TWincontrol;
                Pager: TWinControl);
    procedure MostraFTPWebDir(SQLType: TSQLType; Codigo, FTPConfig,
                Nivel, TipoDir, CliInt, CodigoApp: Integer; Nome,
                Caminho: String; QueryFTPWebDir: TmySQLQuery);
  end;
                        
var
  UFTPJan: TUnFTPJan;

implementation

uses MyListas, Module, DmkDAC_PF, UnMyObjects, MyDBCheck, FTPConfig, UnDmkWeb,
  FTPWebArq, FTPWebDir, UnFTP, UnInternalConsts;

{ TUnFTPJan }

procedure TUnFTPJan.MostraFTPConfig(Codigo: Integer);
var
  Msg: String;
begin
  if DmkWeb.VerificaSeModuloWebEstaAtivo(CO_DMKID_APP,
    DModG.QrMasterHabilModulos.Value, Dmod.MyDBn, Msg) then
  begin
    if DmkWeb.ConexaoRemota(Dmod.MyDBn, DmodG.QrWebParams, 1) then
    begin
      if DBCheck.CriaFm(TFmFTPConfig, FmFTPConfig, afmoNegarComAviso) then
      begin
        if Codigo <> 0 then
          FmFTPConfig.LocCod(Codigo, Codigo);
        FmFTPConfig.ShowModal;
        FmFTPConfig.Destroy;
      end;
    end;
  end else
    Geral.MB_Aviso(Msg);
end;

procedure TUnFTPJan.MostraFTPWebArq(AbrirEmAba: Boolean; InOwner: TWincontrol;
  Pager: TWinControl);
var
  Form: TForm;
  Msg: String;
begin
  if DmkWeb.VerificaSeModuloWebEstaAtivo(CO_DMKID_APP,
    DModG.QrMasterHabilModulos.Value, Dmod.MyDBn, Msg) then
  begin
    if DmkWeb.ConexaoRemota(Dmod.MyDBn, DmodG.QrWebParams, 1) then
    begin
      if AbrirEmAba then
      begin
        if FmFTPWebArq = nil then
        begin
          Form := MyObjects.FormTDICria(TFmFTPWebArq, InOwner, Pager, True, True);
          //
          TFmFTPWebArq(Form).FInOwner := InOwner;
          TFmFTPWebArq(Form).FPager   := Pager
        end;
      end else
      begin
        if DBCheck.CriaFm(TFmFTPWebArq, FmFTPWebArq, afmoNegarComAviso) then
        begin
          FmFTPWebArq.ShowModal;
          FmFTPWebArq.Destroy;
          //
          FmFTPWebArq := nil;
        end;
      end;
    end;
  end else
    Geral.MB_Aviso(Msg);
end;

procedure TUnFTPJan.MostraFTPWebDir(SQLType: TSQLType; Codigo, FTPConfig,
  Nivel, TipoDir, CliInt, CodigoApp: Integer; Nome, Caminho: String;
  QueryFTPWebDir: TmySQLQuery);
var
  Msg: String;
begin
  if DmkWeb.VerificaSeModuloWebEstaAtivo(CO_DMKID_APP,
    DModG.QrMasterHabilModulos.Value, Dmod.MyDBn, Msg) then
  begin
    if DBCheck.CriaFm(TFmFTPWebDir, FmFTPWebDir, afmoNegarComAviso) then
    begin
      FmFTPWebDir.ImgTipo.SQLType := SQLType;
      FmFTPWebDir.FFTPConfig      := FTPConfig;
      FmFTPWebDir.FCaminho        := Caminho;
      FmFTPWebDir.FPasta          := UFTP.ObtemNomeDir(Caminho);
      FmFTPWebDir.FCodigoApp      := CodigoApp;
      //
      if SQLType = stIns then
      begin
        FmFTPWebDir.FCodigo  := 0;
        FmFTPWebDir.FNome    := '';
        FmFTPWebDir.FTipoDir := 0;
        FmFTPWebDir.FCliInt  := 0;
        FmFTPWebDir.FNivel   := 0;
      end else
      begin
        FmFTPWebDir.FCodigo  := Codigo;
        FmFTPWebDir.FNome    := Nome;
        FmFTPWebDir.FTipoDir := TipoDir;
        FmFTPWebDir.FCliInt  := CliInt;
        FmFTPWebDir.FNivel   := Nivel;
      end;
      //
      FmFTPWebDir.ShowModal;
      FmFTPWebDir.Destroy;
    end;
  end else
    Geral.MB_Aviso(Msg);
end;

end.
