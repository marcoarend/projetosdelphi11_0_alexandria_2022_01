unit FTPWebArq;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, dmkEdit, dmkCheckGroup, FTPSiteInfo,
  dmkDBLookupComboBox, dmkEditCB, DBCGrids, dmkDBGrid, dmkCompoStore, CommCtrl,
  dmkPermissoes, IdBaseComponent, IdComponent, IdTCPConnection, IdTCPClient,
  IdExplicitTLSClientServerBase, IdFTP, mySQLDbTables, UnDmkEnums, Vcl.Menus,
  IdFTPList, UnDmkProcFunc, IdFTPCommon, StrUtils, Variants, Vcl.ImgList,
  System.ImageList, UnProjGroup_Consts;

type
  TFmFTPWebArq = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    LaTitulo1C: TLabel;
    CSTabSheetChamou: TdmkCompoStore;
    dmkPermissoes1: TdmkPermissoes;
    ImgWEB: TdmkImage;
    QrFTPConfig: TmySQLQuery;
    QrFTPConfigNome: TWideStringField;
    QrFTPConfigSENHA: TWideStringField;
    QrFTPConfigWeb_FTPu: TWideStringField;
    QrFTPConfigWeb_FTPs: TWideStringField;
    QrFTPConfigWeb_FTPh: TWideStringField;
    QrFTPConfigWeb_Raiz: TWideStringField;
    QrFTPConfigCodigo: TIntegerField;
    QrFTPConfigAtivo: TSmallintField;
    QrFTPConfigPassivo: TSmallintField;
    DsFTPConfig: TDataSource;
    PnConectar: TPanel;
    Label4: TLabel;
    SBFTPConfig: TSpeedButton;
    EdFTPConfig: TdmkEditCB;
    CBFTPConfig: TdmkDBLookupComboBox;
    BtConectar: TBitBtn;
    PnDados: TPanel;
    QrFTPWebDir: TmySQLQuery;
    QrFTPWebDirCodigo: TIntegerField;
    QrFTPWebDirNome: TWideStringField;
    QrFTPWebDirPasta: TWideStringField;
    QrFTPWebDirCaminho: TWideStringField;
    QrFTPWebDirFTPConfig: TIntegerField;
    QrFTPWebDirLk: TIntegerField;
    QrFTPWebDirDataCad: TDateField;
    QrFTPWebDirDataAlt: TDateField;
    QrFTPWebDirUserCad: TIntegerField;
    QrFTPWebDirUserAlt: TIntegerField;
    QrFTPWebDirAlterWeb: TSmallintField;
    QrFTPWebDirAtivo: TSmallintField;
    QrFTPWebDirOrdem: TIntegerField;
    QrFTPWebDirCliInt: TIntegerField;
    QrFTPWebDirArqPri: TIntegerField;
    QrFTPWebDirNivel: TIntegerField;
    QrFTPWebDirTipoDir: TSmallintField;
    DsFTPWebDir: TDataSource;
    QrFTPWebArq: TmySQLQuery;
    QrFTPWebArqCodigo: TIntegerField;
    QrFTPWebArqCliInt: TIntegerField;
    QrFTPWebArqNome: TWideStringField;
    QrFTPWebArqArqTam: TFloatField;
    QrFTPWebArqFTPConfig: TIntegerField;
    QrFTPWebArqFTPDir: TIntegerField;
    QrFTPWebArqArquivo: TWideStringField;
    QrFTPWebArqCaminho: TWideStringField;
    QrFTPWebArqNivel: TIntegerField;
    QrFTPWebArqOrigem: TIntegerField;
    DsFTPWebArq: TDataSource;
    QrLoc: TmySQLQuery;
    QrFTPWebArqCliInt_TXT: TWideStringField;
    PnFTP: TPanel;
    Panel6: TPanel;
    BtOk: TBitBtn;
    Panel7: TPanel;
    EdCaminho: TdmkEdit;
    PnDiretorios: TPanel;
    StaticText3: TStaticText;
    PnMenu1: TPanel;
    BtHome: TBitBtn;
    BtUpFolder: TBitBtn;
    BtVoltar: TBitBtn;
    BtIncluiDir: TBitBtn;
    BtAlteraDir: TBitBtn;
    BtExcluiDir: TBitBtn;
    BtFavoritos: TBitBtn;
    PCDiretorios: TPageControl;
    TabSheet1: TTabSheet;
    LvDiretorios: TListView;
    PnItens: TPanel;
    StaticText1: TStaticText;
    PnMenu2: TPanel;
    LaInfo: TLabel;
    BtUpload: TBitBtn;
    BtDownload: TBitBtn;
    BtAlteraArq: TBitBtn;
    BtExcluiArq: TBitBtn;
    PCArquivos: TPageControl;
    TSArquivosWEB: TTabSheet;
    LvArquivos: TListView;
    TSArquivos: TTabSheet;
    DBGItensArq: TDBGrid;
    GBDetalhes: TGroupBox;
    Label1: TLabel;
    ImgIcon: TImage;
    LaTamanho: TLabel;
    LaModify: TLabel;
    Label3: TLabel;
    SaveFile: TSaveDialog;
    Label2: TLabel;
    LaArquivo: TLabel;
    GBDetalhesDB: TGroupBox;
    ImgIconDB: TImage;
    DBText1: TDBText;
    Label11: TLabel;
    GroupBox3: TGroupBox;
    DBCGNivelArq: TdmkDBCheckGroup;
    Label12: TLabel;
    DBText2: TDBText;
    Label6: TLabel;
    Label7: TLabel;
    Label9: TLabel;
    DBText4: TDBText;
    DBText5: TDBText;
    DBText6: TDBText;
    QrFTPWebDirCliInt_TXT: TWideStringField;
    QrFTPWebDirTipoDir_TXT: TWideStringField;
    OpenDialog1: TOpenDialog;
    Label10: TLabel;
    DBText9: TDBText;
    QrFTPWebArqDataCad: TDateField;
    QrFTPWebArqDataAlt: TDateField;
    QrFTPWebArqArqTam_MB: TFloatField;
    QrFTPWebArqTags: TWideStringField;
    QrFTPWebArqDataHora: TDateTimeField;
    QrFTPWebArqDataExp: TDateField;
    BtPesquisa: TBitBtn;
    TSArquivosWEBPesq: TTabSheet;
    CBPesEmpresa: TdmkDBLookupComboBox;
    Label14: TLabel;
    EdPesEmpresa: TdmkEditCB;
    EdPesNome: TdmkEdit;
    CkPesNome: TCheckBox;
    BitBtn1: TBitBtn;
    BtLimpar: TBitBtn;
    BtPesqArq: TBitBtn;
    EdPesTags: TdmkEdit;
    CkPesTags: TCheckBox;
    BtDesiste: TBitBtn;
    FTPGer: TIdFTP;
    BtLinkArq: TBitBtn;
    QrFTPConfigWeb_MyURL: TWideStringField;
    QrFTPConfigWebId: TWideStringField;
    TabSheet3: TTabSheet;
    DBGItens: TDBGrid;
    GroupBox2: TGroupBox;
    Label5: TLabel;
    DBText7: TDBText;
    DBCGNivel: TdmkDBCheckGroup;
    Panel5: TPanel;
    Panel8: TPanel;
    BtInclui: TBitBtn;
    BtExclui: TBitBtn;
    DBGrid3: TDBGrid;
    Panel9: TPanel;
    Panel10: TPanel;
    BitBtn2: TBitBtn;
    BitBtn3: TBitBtn;
    DBGrid1: TDBGrid;
    QrFTPWebArqVersaoArq: TWideStringField;
    QrFTPWebComArq: TmySQLQuery;
    DsFTPWebComArq: TDataSource;
    DsFTPWebComDir: TDataSource;
    QrFTPWebComDir: TmySQLQuery;
    QrFTPWebComArqControle: TIntegerField;
    QrFTPWebComArqEntidade_TXT: TWideStringField;
    QrFTPWebComDirControle: TIntegerField;
    QrFTPWebComDirEntidade_TXT: TWideStringField;
    Label8: TLabel;
    EdEntidade: TdmkEditCB;
    CBEntidade: TdmkDBLookupComboBox;
    QrEntidades: TmySQLQuery;
    DsEntidades: TDataSource;
    QrEntidadesCodigo: TIntegerField;
    DBText3: TDBText;
    QrEntidadesApelido: TWideStringField;
    QrEntidadesNome: TWideStringField;
    ImageList1: TImageList;
    ImgArq: TImage;
    ImgDir: TImage;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure SBFTPConfigClick(Sender: TObject);
    procedure BtConectarClick(Sender: TObject);
    procedure FTPGerDisconnected(Sender: TObject);
    procedure FTPGerAfterClientLogin(Sender: TObject);
    procedure PCDiretoriosChange(Sender: TObject);
    procedure QrFTPWebDirAfterScroll(DataSet: TDataSet);
    procedure QrFTPWebDirBeforeClose(DataSet: TDataSet);
    procedure BtOkClick(Sender: TObject);
    procedure BtHomeClick(Sender: TObject);
    procedure BtUpFolderClick(Sender: TObject);
    procedure BtVoltarClick(Sender: TObject);
    procedure QrFTPWebArqAfterScroll(DataSet: TDataSet);
    procedure LvArquivosSelectItem(Sender: TObject; Item: TListItem;
      Selected: Boolean);
    procedure LvDiretoriosDblClick(Sender: TObject);
    procedure BtIncluiDirClick(Sender: TObject);
    procedure BtAlteraDirClick(Sender: TObject);
    procedure BtExcluiDirClick(Sender: TObject);
    procedure BtFavoritosClick(Sender: TObject);
    procedure BtUploadClick(Sender: TObject);
    procedure BtAlteraArqClick(Sender: TObject);
    procedure BtExcluiArqClick(Sender: TObject);
    procedure BtDownloadClick(Sender: TObject);
    procedure LvDiretoriosKeyPress(Sender: TObject; var Key: Char);
    procedure BtPesquisaClick(Sender: TObject);
    procedure BtPesqArqClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure BtLimparClick(Sender: TObject);
    procedure BtLinkArqClick(Sender: TObject);
    procedure QrFTPWebArqBeforeClose(DataSet: TDataSet);
    procedure BtIncluiClick(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure BtExcluiClick(Sender: TObject);
    procedure BitBtn3Click(Sender: TObject);
    procedure DBGItensArqDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure DBGItensDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
  private
    { Private declarations }
    FLastDirStack: TStringList;
    FRootDir: String;
    FAtivaAfterScroll: Boolean;
    procedure DisplayFTP;
    procedure MostraEdicao(Mostra: Integer);
    procedure ConfiguraTipo(Tab: Integer);
    procedure ChangeFTPDir(NewDir : String);
    procedure ConfiguraPainelDetalhes(DB: Boolean);
    procedure SelecionaDiretorio();
    procedure VoltarDiretorio();
    procedure RedefinirPesquisa();
    procedure ReopenFTPArq(Codigo, Diretorio, DmkIDApp: Integer;
                Database: TmySQLDatabase; Query: TmySQLQuery);
    function  VerificaSeEstaConectado(): Boolean;
  public
    { Public declarations }
    FInOwner: TWincontrol;
    FPager: TWinControl;
  end;

  var
    FmFTPWebArq: TFmFTPWebArq;
  const
    DMKID_APP = 31; //Dmk => DControl na WEB

implementation

uses UnMyObjects, Module, MyGlyfs, Principal, DmkDAC_PF, UnFTPJan, UnFTP,
  QuaisItens, MyDBCheck, ZForge, UnDmkWeb, ModuleGeral, MyListas,
  UMySQLModule, WOpcoes, UnGrlUsuarios;

{$R *.DFM}

procedure TFmFTPWebArq.BitBtn1Click(Sender: TObject);
var
  CliInt: Integer;
  Nome, Tags: String;
begin
  ReopenFTPArq(0, QrFTPWebArqFTPDir.Value, CO_DMKID_APP, Dmod.MyDBn, QrFTPWebArq);
  //
  ChangeFTPDir(QrFTPWebDirCaminho.Value);
  ConfiguraTipo(1);
end;

procedure TFmFTPWebArq.BitBtn2Click(Sender: TObject);
begin
  if (QrFTPWebArq.State <> dsInactive) and (QrFTPWebArq.RecordCount > 0) then
  begin
    if UFTP.AdicionaUsuarioComp(dtArq, QrFTPWebArqCodigo.Value, Dmod.QrUpdN,
      Dmod.QrAuxN, Dmod.MyDBn)
    then
      UFTP.ReopenFTPCom(dtArq, QrFTPWebArqCodigo.Value, Dmod.MyDBn, QrFTPWebComArq)
    else
      Geral.MB_Erro('Falha ao incluir entidade!');
  end;
end;

procedure TFmFTPWebArq.BitBtn3Click(Sender: TObject);
begin
  if (QrFTPWebComArq.State <> dsInactive) and (QrFTPWebComArq.RecordCount > 0) then
  begin
    if UFTP.ExcluiUsuarioComp(QrFTPWebComArqControle.Value, Dmod.MyDBn) then
      UFTP.ReopenFTPCom(dtArq, QrFTPWebArqCodigo.Value, Dmod.MyDBn, QrFTPWebComArq)
    else
      Geral.MB_Erro('Falha ao remover entidade!');
  end;
end;

procedure TFmFTPWebArq.BtAlteraArqClick(Sender: TObject);
var
  Item: TListItem;
  Arquivo, Nome, ArqNome, Caminho: String;
  DirFTP, Codigo, FTPDir, Empresa, Nivel, Res: Integer;
begin
  if VerificaSeEstaConectado then
  begin
    if PCDiretorios.ActivePageIndex = 0 then //WEB
    begin
      Item := LvArquivos.Selected;
      //
      if Item <> nil then
      begin
        Arquivo := Item.Caption;
        DirFTP  := UFTP.VerificaSeDiretorioExiste(QrLoc, Dmod.MyDBn,
                     EdCaminho.ValueVariant, QrFTPConfigCodigo.Value);
        //
        if DirFTP <> 0 then
          Codigo := UFTP.VerificaSeArquivoExiteNoDB(QrLoc, Dmod.MyDBn,
                      CO_DMKID_APP, DirFTP, Arquivo)
        else
          Codigo := 0;
        //
        if InputQuery('Renomear arquivo', 'Nome:', Arquivo) then
        begin
          Arquivo := Trim(Geral.SemAcento(Arquivo));
          Arquivo := StringReplace(Arquivo, ' ', '_', [rfReplaceAll, rfIgnoreCase]);
          //
          FTPGer.Rename(Item.Caption, Arquivo);
        end;
        //
        if Codigo <> 0 then
        begin
          UFTP.AtualizaArquivoFTP(QrLoc, Codigo, Arquivo);
          ReopenFTPArq(Codigo, QrFTPWebDirCodigo.Value, CO_DMKID_APP,
            Dmod.MyDBn, QrFTPWebArq);
        end;
        DisplayFTP;
      end else
        Geral.MB_Aviso('Voc� precisa primeiro selecionar um arquivo para renome�-lo!');
    end else
    begin
      if (QrFTPWebArq.State <> dsInactive) and (QrFTPWebArq.RecordCount > 0) then
      begin
        Caminho := QrFTPWebDirCaminho.Value;
        //
        UFTP.UploadFTP_DB(FInOwner, FPager, stUpd, QrFTPWebArqArquivo.Value,
          QrFTPWebArqNome.Value, QrFTPWebArqVersaoArq.Value,
          QrFTPWebArqTags.Value, QrFTPWebArqCodigo.Value, CO_DMKID_APP,
          QrFTPWebArqFTPConfig.Value, QrFTPWebDirTipoDir.Value,
          QrFTPWebArqFTPDir.Value, QrFTPWebArqCliInt.Value,
          QrFTPWebArqNivel.Value, dtPad, QrFTPWebArqArqTam.Value,
          QrFTPWebArqDataExp.Value, nil, True, Dmod.QrUpdN, Dmod.QrAuxN,
          Dmod.MyDBn, Caminho);
        ReopenFTPArq(QrFTPWebArqCodigo.Value, QrFTPWebArqFTPDir.Value,
          CO_DMKID_APP, Dmod.MyDBn, QrFTPWebArq);
      end;
    end;
  end;
end;

procedure TFmFTPWebArq.BtAlteraDirClick(Sender: TObject);
var
  Item: TListItem;
  Diretorio, DiretorioNew: String;
  Codigo: Integer;
begin
  if VerificaSeEstaConectado then
  begin
    if PCDiretorios.ActivePageIndex = 0 then //WEB
    begin
      Item := LvDiretorios.Selected;
      //
      if Item <> nil then
      begin
        Diretorio    := Item.Caption;
        DiretorioNew := Diretorio;
        //
        //Atualiza tamb�m no banco de dados se existir
        Codigo := UFTP.VerificaSeDiretorioExiste(QrLoc, Dmod.MyDBn,
          UFTP.MontaCaminhoDirFTP(FTPGer.RetrieveCurrentDir, Diretorio),
          QrFTPConfigCodigo.Value);
        //
        if InputQuery('Renomear diret�rio', 'Nome:', DiretorioNew) then
        begin
          DiretorioNew := Trim(Geral.SemAcento(DiretorioNew));
          DiretorioNew := StringReplace(DiretorioNew, ' ', '_', [rfReplaceAll, rfIgnoreCase]);
          //
          FTPGer.Rename(Diretorio, DiretorioNew);
        end;
        //
        if Codigo > 0 then
        begin
          UFTP.AtualizaCaminhoDirFTP(Codigo, UFTP.MontaCaminhoDirFTP(
            FTPGer.RetrieveCurrentDir, DiretorioNew), Dmod.QrUpdN);
          UFTP.ReopenFTPWebDir(EdFTPConfig.ValueVariant, 0, CO_DMKID_APP,
            Dmod.MyDBn, QrFTPWebDir);
        end;
        DisplayFTP;
      end else
        Geral.MB_Aviso('Voc� precisa primeiro selecionar um deret�rio para renome�-lo!');
    end else
    begin  //DB
      if (QrFTPWebDir.State <> dsInactive) and (QrFTPWebDir.RecordCount > 0) then
      begin
        UFTPJan.MostraFTPWebDir(stUpd, QrFTPWebDirCodigo.Value,
          EdFTPConfig.ValueVariant, QrFTPWebDirNivel.Value,
          QrFTPWebDirTipoDir.Value, QrFTPWebDirCliInt.Value, CO_DMKID_APP,
          QrFTPWebDirNome.Value, EdCaminho.ValueVariant, QrFTPWebDir);
        if VAR_COD <> 0 then
          UFTP.ReopenFTPWebDir(EdFTPConfig.ValueVariant, VAR_COD, CO_DMKID_APP,
            Dmod.MyDBn, QrFTPWebDir);
      end;
    end;
  end;
end;

procedure TFmFTPWebArq.BtConectarClick(Sender: TObject);
var
  Conectado: Boolean;
begin
  if MyObjects.FIC(EdFTPConfig.ValueVariant = 0, EdFTPConfig, 'Servidor n�o definido!') then Exit;
  //
  Conectado := VerificaSeEstaConectado();
  //
  if Conectado then
  begin
    FTPGer.Disconnect;
  end else
  begin
    Screen.Cursor := crHourGlass;
    try
      UFTP.ReopenFTPWebDir(EdFTPConfig.ValueVariant, 0, CO_DMKID_APP,
        Dmod.MyDBn, QrFTPWebDir);
      ConfiguraTipo(-1);
      if UFTP.ConectaServidorFTP(QrFTPConfigWeb_FTPh.Value,
          QrFTPConfigWeb_FTPu.Value, QrFTPConfigSENHA.Value,
          Geral.IntToBool(QrFTPConfigPassivo.Value), FTPGer) = -1 then
      begin
        if Geral.MB_Pergunta('Deseja editar as configura��es?') = ID_YES then
        begin
          if CO_DMKID_APP = 17 then //DControl
          begin
            UFTPJan.MostraFTPConfig(EdFTPConfig.ValueVariant);
          end else
          begin
            if DBCheck.CriaFm(TFmWOpcoes, FmWOpcoes, afmoNegarComAviso) then
            begin
              FmWOpcoes.ShowModal;
              FmWOpcoes.Destroy;
            end;
          end;
        end;
      end;
    finally
      Screen.Cursor := crDefault;
    end;
  end;
end;

procedure TFmFTPWebArq.BtDesisteClick(Sender: TObject);
begin
  RedefinirPesquisa();
  ChangeFTPDir(QrFTPWebDirCaminho.Value);
  ConfiguraTipo(1);
end;

procedure TFmFTPWebArq.BtDownloadClick(Sender: TObject);
var
  Item: TListItem;
  Arquivo: String;
begin
  if VerificaSeEstaConectado then
  begin
    if PCDiretorios.ActivePageIndex = 0 then //WEB
    begin
      Item := LvArquivos.Selected;
      //
      if Item <> nil then
      begin
        try
          BtDownload.Enabled := False;
          //
          Arquivo := UFTP.DownloadFileIndy(FInOwner, FPager, SaveFile,
                       EdCaminho.ValueVariant, Item.Caption, '',
                       QrFTPConfigWeb_FTPh.Value, QrFTPConfigWeb_FTPu.Value,
                       QrFTPConfigSENHA.Value, Geral.IntToBool(QrFTPConfigPassivo.Value));
          //
          if Arquivo <> '' then
          begin
            if Geral.MB_Pergunta('Deseja abrir o arquivo?') = ID_YES then
            begin
              if FileExists(Arquivo) then
                Geral.AbreArquivo(Arquivo, True);
            end;
          end;
        finally
          BtDownload.Enabled := True;
        end;
      end else
        Geral.MB_Aviso('Voc� precisa primeiro selecionar um arquivo para download no servidor!');
    end else
    begin
      if (QrFTPWebArq.State <> dsInactive) and (QrFTPWebArq.RecordCount > 0) then
      begin
        try
          BtDownload.Enabled := False;
          //
          Arquivo := UFTP.DownloadFileIndy(FInOwner, FPager, SaveFile,
                       EdCaminho.ValueVariant, QrFTPWebArqArquivo.Value, '',
                       QrFTPConfigWeb_FTPh.Value, QrFTPConfigWeb_FTPu.Value,
                       QrFTPConfigSENHA.Value, Geral.IntToBool(QrFTPConfigPassivo.Value));
          //
          if Arquivo <> '' then
          begin
            if Geral.MB_Pergunta('Deseja abrir o arquivo?') = ID_YES then
            begin
              if FileExists(Arquivo) then
                Geral.AbreArquivo(Arquivo, True);
            end;
          end;
        finally
          BtDownload.Enabled := True;
        end;
      end;
    end;
  end;
end;

procedure TFmFTPWebArq.BtExcluiArqClick(Sender: TObject);
var
  Item: TListItem;
  I, FTPDir, Codigo: Integer;
  Arquivo: String;
begin
  if VerificaSeEstaConectado then
  begin
    if PCDiretorios.ActivePageIndex = 0 then //WEB
    begin
      Item := LvArquivos.Selected;
      //
      if Item <> nil then
      begin
        if LvArquivos.SelCount > 0 then
        begin
          if LvArquivos.SelCount > 1 then
          begin
            if Geral.MB_Pergunta('Tem certeza que deseja excluir os arquivos selecionados do servidor?') <> ID_YES then
              Exit;
          end;
          for I := 0 to LvArquivos.Items.Count - 1 do
          begin
            if LvArquivos.Items[i].Selected then
            begin
              Arquivo := LvArquivos.Items[i].Caption;
              FTPDir  := UFTP.VerificaSeDiretorioExiste(QrLoc, Dmod.MyDBn,
                           EdCaminho.ValueVariant, QrFTPConfigCodigo.Value);
              //
              if FTPDir <> 0 then
                Codigo := UFTP.VerificaSeArquivoExiteNoDB(QrLoc, Dmod.MyDBn,
                            CO_DMKID_APP, FTPDir, Arquivo)
              else
                Codigo := 0;
              //
              if Codigo <> 0 then
              begin
                Geral.MB_Aviso('Exclus�o cancelada!' + sLineBreak +
                  'Motivo: Este arquivo est� adicionado em Meus diret�rios.' +
                  sLineBreak + 'Remova-o de Meus diret�rios e tente excluir novamente.');
              end else
              begin
                if LvArquivos.SelCount = 1 then
                begin
                  if Geral.MB_Pergunta('Tem certeza que deseja excluir o arquivo "' +
                    Arquivo + '" do servidor?') = ID_YES
                  then
                    FTPGer.Delete(Arquivo);
                end else
                  FTPGer.Delete(Arquivo);
              end;
            end;
          end;
          DisplayFTP;
        end else
          Geral.MB_Aviso('Voc� precisa primeiro selecionar um arquivo para excluir do servidor!');
      end else
        Geral.MB_Aviso('Voc� precisa primeiro selecionar um arquivo para excluir do servidor!');
    end else
    begin
      if (QrFTPWebArq.State <> dsInactive) and (QrFTPWebArq.RecordCount > 0) then
      begin
        Codigo := QrFTPWebArqCodigo.Value;
        //
        if MyObjects.FIC(QrFTPWebArqOrigem.Value <> 0, nil, 'Exclus�o cancelada!' +
          sLineBreak + 'Motivo: Este arquivo � gerenciado por outra janela!') then Exit;
        //
        if UMyMod.ExcluiRegistroInt1('Confirma a exclus�o do arquivo "' +
          QrFTPWebArqNome.Value + '"?', 'ftpwebarq', 'Codigo', Codigo,
          DMod.MyDBn) = ID_YES then
        begin
          UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpdN, Dmod.MyDBn, [
            'DELETE FROM ftpwebcom WHERE Tipo = 1 AND Codigo=' + Geral.FF0(Codigo),
            '']);
          ReopenFTPArq(0, QrFTPWebDirCodigo.Value, CO_DMKID_APP, Dmod.MyDBn,
            QrFTPWebArq);
          //
          Geral.MB_Aviso('O arquivo foi removido apenas do banco de dados!' +
            sLineBreak + 'Para remov�-lo do servidor selecione a aba Disco WEB, ' +
            sLineBreak + 'selecione o arquivo e clique no bot�o de exclus�o.');
        end;
      end;
    end;
  end;
end;

procedure TFmFTPWebArq.BtExcluiClick(Sender: TObject);
begin
  if (QrFTPWebComDir.State <> dsInactive) and (QrFTPWebComDir.RecordCount > 0) then
  begin
    if UFTP.ExcluiUsuarioComp(QrFTPWebComDirControle.Value, Dmod.MyDBn) then
      UFTP.ReopenFTPCom(dtDir, QrFTPWebDirCodigo.Value, Dmod.MyDBn, QrFTPWebComDir)
    else
      Geral.MB_Erro('Falha ao remover entidade!');
  end;
end;

procedure TFmFTPWebArq.BtExcluiDirClick(Sender: TObject);
var
  Diretorio: String;
  Item: TListItem;
  Codigo: Integer;
begin
  if VerificaSeEstaConectado then
  begin
    if PCDiretorios.ActivePageIndex = 0 then //WEB
    begin
      Item := LvDiretorios.Selected;
      //
      if Item <> nil then
      begin
        Diretorio := Item.Caption;
        //
        if UFTP.VerificaSeDiretorioExiste(QrLoc, Dmod.MyDBn,
          UFTP.MontaCaminhoDirFTP(FTPGer.RetrieveCurrentDir, Diretorio),
          QrFTPConfigCodigo.Value) <> 0 then
        begin
          Geral.MB_Aviso('Exclus�o cancelada!' + sLineBreak +
            'Motivo: Este diret�rio est� adicionado em Meus diret�rios.' +
            sLineBreak + 'Remova-o de Meus diret�rios e tente excluir novamente.');
        end else
        begin
          if Geral.MB_Pergunta('Tem certeza que deseja excluir o diret�rio "' +
            Diretorio + '" do servidor?') = ID_YES
          then
            FTPGer.RemoveDir(Diretorio);
        end;
        DisplayFTP;
      end else
        Geral.MB_Aviso('Voc� precisa primeiro selecionar um ' +
          'diret�rio para excluir do servidor!');
    end else
    begin //DB
      if (QrFTPWebDir.State <> dsInactive) and (QrFTPWebDir.RecordCount > 0) then
      begin
        if (QrFTPWebArq.State <> dsInactive) and (QrFTPWebArq.RecordCount > 0) then
          Geral.MB_Aviso('Exclus�o abortada!' + sLineBreak +
            'Motivo: O diret�rio possui item(ns) atrelado(s) a ele!')
        else begin
          Codigo := QrFTPWebDirCodigo.Value;
          //
          UMyMod.ExcluiRegistroInt1('Confirma a exclus�o do diret�rio "' +
            QrFTPWebDirPasta.Value + '"?', 'ftpwebdir', 'Codigo', Codigo, DMod.MyDBn);
          //
          UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpdN, Dmod.MyDBn, [
            'DELETE FROM ftpwebcom WHERE Tipo = 0 AND Codigo=' + Geral.FF0(Codigo),
            '']);
          //
          UFTP.ReopenFTPWebDir(EdFTPConfig.ValueVariant, 0, CO_DMKID_APP,
            Dmod.MyDBn, QrFTPWebDir);
          //
          Geral.MB_Aviso('O diret�rio foi removido apenas do banco de dados!' +
            sLineBreak + 'Para remov�-lo do servidor, selecione a aba Disco WEB, ' +
            sLineBreak + 'selecione a pasta e clique no bot�o de exclus�o.');
        end;
      end;
    end;
  end;
end;

procedure TFmFTPWebArq.BtFavoritosClick(Sender: TObject);
begin
  if VerificaSeEstaConectado then
  begin
    if PCDiretorios.ActivePageIndex = 0 then //WEB
    begin
      UFTPJan.MostraFTPWebDir(stIns, 0, EdFTPConfig.ValueVariant, -1, -1, 0,
        CO_DMKID_APP, '', EdCaminho.ValueVariant, QrFTPWebDir);
      UFTP.ReopenFTPWebDir(EdFTPConfig.ValueVariant, VAR_COD, CO_DMKID_APP,
        Dmod.MyDBn, QrFTPWebDir);
      ConfiguraTipo(-1);
    end;
  end;
end;

function TFmFTPWebArq.VerificaSeEstaConectado(): Boolean;
begin
  try
    if not FTPGer.Connected then
    begin
      MostraEdicao(0);
      //
      Result := False;
    end else
      Result := True;
  except
    FTPGer.Disconnect;
    //
    MostraEdicao(0);
    //
    Result := False;
  end;
end;

procedure TFmFTPWebArq.VoltarDiretorio;
var
  Mostra: Integer;
  s: String;
begin
  if VerificaSeEstaConectado then
  begin
    if FLastDirStack.Count > 0 then
    begin
      s := FLastDirStack[FLastDirStack.Count -1];
      ChangeFTPDir(s);
      // Delete S
      FLastDirStack.Delete(FLastDirStack.Count -1);
      // Delete the jump from S
      FLastDirStack.Delete(FLastDirStack.Count -1);
      //
      if VerificaSeEstaConectado() then
        Mostra := 1
      else
        Mostra := 0;
      //
      MostraEdicao(Mostra);
    end;
    ConfiguraTipo(0);
  end;
end;

procedure TFmFTPWebArq.BtHomeClick(Sender: TObject);
begin
  if VerificaSeEstaConectado then
  begin
    ChangeFTPDir(FRootDir);
    ConfiguraTipo(0);
  end;
end;

procedure TFmFTPWebArq.BtIncluiClick(Sender: TObject);
begin
  if (QrFTPWebDir.State <> dsInactive) and (QrFTPWebDir.RecordCount > 0) then
  begin
    if UFTP.AdicionaUsuarioComp(dtDir, QrFTPWebDirCodigo.Value, Dmod.QrUpdN,
      Dmod.QrAuxN, Dmod.MyDBn)
    then
      UFTP.ReopenFTPCom(dtDir, QrFTPWebDirCodigo.Value, Dmod.MyDBn, QrFTPWebComDir)
    else
      Geral.MB_Erro('Falha ao incluir entidade!');
  end;
end;

procedure TFmFTPWebArq.BtIncluiDirClick(Sender: TObject);
var
  Diretorio: String;
begin
  if VerificaSeEstaConectado then
  begin
    Diretorio := 'Nova_Pasta';
    //
    if InputQuery('Novo diretorio', 'Nome do diret�rio: (Sem espa�os em branco)', Diretorio) then
    begin
      Diretorio := Trim(Geral.SemAcento(Diretorio));
      Diretorio := StringReplace(Diretorio, ' ', '_', [rfReplaceAll, rfIgnoreCase]);
      try
        UFTP.CriaDiretorio(Diretorio, FTPGer);
      finally
        ChangeFTPDir(FTPGer.RetrieveCurrentDir);
      end;
    end;
  end;
end;

procedure TFmFTPWebArq.BtLimparClick(Sender: TObject);
begin
  RedefinirPesquisa();
end;

procedure TFmFTPWebArq.BtLinkArqClick(Sender: TObject);
var
  Link, Arquivo: String;
begin
  if (QrFTPWebArq.State <> dsInactive) and (QrFTPWebArq.RecordCount > 0) then
  begin
    Arquivo := DModG.MD5_por_SQL(Geral.FF0(QrFTPWebArqCodigo.Value));
    Link    := UFTP.ObterLinkArquivo(QrFTPConfigWeb_MyURL.Value, QrFTPConfigWebId.Value, Arquivo, CO_DMKID_APP);
    //
    Geral.MB_Aviso('URL do arquivo:' + sLineBreak + Link);
  end;
end;

procedure TFmFTPWebArq.BtOkClick(Sender: TObject);
var
  Caminho: String;
begin
  if VerificaSeEstaConectado then
  begin
    Caminho := EdCaminho.ValueVariant;
    //
    if Caminho = '' then
      Caminho := '/';
    if Copy(Caminho, 1, 1) <> '/'  then
      Caminho := '/' + Caminho;
    //
    EdCaminho.ValueVariant := Caminho;
    //
    ChangeFTPDir(EdCaminho.ValueVariant);
    ConfiguraTipo(0);
  end;
end;

procedure TFmFTPWebArq.BtPesqArqClick(Sender: TObject);
begin
  ConfiguraTipo(2);
end;

procedure TFmFTPWebArq.BtPesquisaClick(Sender: TObject);
var
  Nome: String;
begin
  if (QrFTPWebDir.State <> dsInactive) and (QrFTPWebDir.RecordCount > 0) then
  begin
    if InputQuery('Diretorio', 'Diret�rio:', Nome) then
    begin
      if not QrFTPWebDir.Locate('Nome', Nome, [loCaseInsensitive, loPartialKey]) then
        Geral.MB_Aviso('Diret�rio n�o localizado!');
    end;
  end;
end;

procedure TFmFTPWebArq.BtSaidaClick(Sender: TObject);
begin
  if TFmFTPWebArq(Self).Owner is TApplication then
    Close
  else
    MyObjects.FormTDIFecha(Self, TTabSheet(CSTabSheetChamou.Component));
end;

procedure TFmFTPWebArq.BtUpFolderClick(Sender: TObject);
begin
  if VerificaSeEstaConectado then
  begin
    FTPGer.ChangeDirUp;
    DisplayFTP;
    ConfiguraTipo(0);
  end;
end;

procedure TFmFTPWebArq.BtUploadClick(Sender: TObject);
var
  FTPTipoDir: Integer;
  Nome, Arq, PastaCam: String;
begin
  if VerificaSeEstaConectado then
  begin
    if PCDiretorios.ActivePageIndex = 0 then //WEB
    begin
      if OpenDialog1.Execute then
      begin
        OpenDialog1.Options := [ofHideReadOnly, ofAllowMultiSelect, ofShareAware, ofEnableSizing];
        //
        PastaCam := EdCaminho.ValueVariant;
        try
          BtUpload.Enabled := False;
          //
          UFTP.UploadFTP(FInOwner, FPager, PastaCam,
            QrFTPConfigWeb_FTPh.Value, QrFTPConfigWeb_FTPu.Value,
            QrFTPConfigSENHA.Value, Geral.IntToBool(QrFTPConfigPassivo.Value),
            False, OpenDialog1.Files);
        finally
          DisplayFTP;
          //
          BtUpload.Enabled := True;
        end;
      end;
    end else
    begin
      FTPTipoDir := QrFTPWebDirTipoDir.Value;
      //
      if FTPTipoDir = 2 then //Fotos
      begin
        OpenDialog1.Options := [ofHideReadOnly, ofAllowMultiSelect, ofShareAware, ofEnableSizing];
        //
        if OpenDialog1.Execute then
        begin
          UFTP.ReopenFTPConfig(0, CO_DMKID_APP, QrLoc, Dmod.MyDBn);
          //
          PastaCam := EdCaminho.ValueVariant;
          //
          if UFTP.UploadFTP_Fotos(FInOwner, FPager, PastaCam,
            QrFTPConfigWeb_FTPh.Value, QrFTPConfigWeb_FTPu.Value,
            QrFTPConfigSENHA.Value, Geral.IntToBool(QrFTPConfigPassivo.Value),
            False, OpenDialog1.Files, CO_DMKID_APP, QrFTPWebDirCliInt.Value, 0,
            QrFTPWebDirCodigo.Value, QrFTPWebDirNivel.Value, dtPad,
            Dmod.QrUpdN, Dmod.MyDBn) <> '' then
          begin
            ReopenFTPArq(0, QrFTPWebDirCodigo.Value, CO_DMKID_APP, Dmod.MyDBn,
              QrFTPWebArq);
          end;
        end;
      end else
      begin
        OpenDialog1.Options := [ofHideReadOnly, ofShareAware, ofEnableSizing];
        //
        if OpenDialog1.Execute then
        begin
          Arq      := ExtractFileName(OpenDialog1.FileName);
          Nome     := ChangeFileExt(Arq, '');
          PastaCam := QrFTPWebDirCaminho.Value;
          //
          UFTP.UploadFTP_DB(FInOwner, FPager, stIns, Arq, Nome, '',
            '', 0, CO_DMKID_APP, EdFTPConfig.ValueVariant, FTPTipoDir,
            QrFTPWebDirCodigo.Value, QrFTPWebDirCliInt.Value,
            QrFTPWebDirNivel.Value, dtPad, 0, 0, OpenDialog1.Files, True,
            Dmod.QrUpdN, Dmod.QrAuxN, Dmod.MyDBn, PastaCam);
          ReopenFTPArq(0, QrFTPWebDirCodigo.Value, CO_DMKID_APP, Dmod.MyDBn,
            QrFTPWebArq);
        end;
      end;
    end;
  end;
end;

procedure TFmFTPWebArq.BtVoltarClick(Sender: TObject);
begin
  VoltarDiretorio();
end;

procedure TFmFTPWebArq.ChangeFTPDir(NewDir: String);
var
  Msg: String;
begin
  try
    Msg                := LaAviso1.Caption;
    PnFTP.Enabled      := False;
    PnConectar.Enabled := False;
    //
    MyObjects.Informa2(LaAviso1, LaAviso2, True, '');
    FLastDirStack.Add(FTPGer.RetrieveCurrentDir);
    FTPGer.ChangeDir(NewDir);
    DisplayFTP;
  finally
    PnFTP.Enabled      := True;
    PnConectar.Enabled := True;
    //
    MyObjects.Informa2(LaAviso1, LaAviso2, False, Msg);
  end;
end;

procedure TFmFTPWebArq.ConfiguraPainelDetalhes(DB: Boolean);
var
  Mostra: Boolean;
  Sel: TListItem;
  Img: String;
  FTPConfig: Integer;
begin
  if VerificaSeEstaConectado then
  begin
    FTPConfig := EdFTPConfig.ValueVariant;
    //
    if DB then
    begin
      GBDetalhesDB.Visible := True;
      //
      if FTPGer.RetrieveCurrentDir <> QrFTPWebArqCaminho.Value then
        ChangeFTPDir(QrFTPWebArqCaminho.Value);
      //
      if QrFTPWebDirTipoDir.Value = 2 then
      begin
        if Pos('min_', QrFTPWebArqArquivo.Value) > 0 then
          Img := QrFTPWebArqArquivo.Value
        else
          Img := 'min_' + QrFTPWebArqArquivo.Value;
      end else
        Img := QrFTPWebArqArquivo.Value;
      //
      UFTP.CarregaMiniatura(FTPConfig, Img, FTPGer, ImgIconDB);
    end else
    begin
      GBDetalhes.Visible := True;
      Sel                := LvArquivos.Selected;
      //
      if Sel <> nil then
      begin
        LaArquivo.Caption := Sel.Caption;
        LaTamanho.Caption := Sel.SubItems[0];
        LaModify.Caption  := Sel.SubItems[1];
        //
        Img := LaArquivo.Caption;
        //
        UFTP.CarregaMiniatura(FTPConfig, Img, FTPGer, ImgIcon);
      end;
    end;
  end;
end;

procedure TFmFTPWebArq.ConfiguraTipo(Tab: Integer);
var
  Aba: Integer;
begin
  Aba := Tab;
  if not Aba in [0, 1, 2] then
  begin
    if QrFTPWebDir.RecordCount > 0 then
      Aba := 1
    else
      Aba := 0;
  end;
  //
  case Aba of
    0: //FTP
    begin
      BtHome.Enabled      := True;
      BtUpFolder.Enabled  := True;
      BtVoltar.Enabled    := True;
      BtIncluiDir.Enabled := True;
      BtAlteraDir.Enabled := True;
      BtExcluiDir.Enabled := True;
      BtFavoritos.Enabled := True;
      BtPesquisa.Enabled  := False;
      //
      BtUpload.Enabled    := True;
      BtDownload.Enabled  := True;
      BtAlteraArq.Enabled := True;
      BtExcluiArq.Enabled := True;
      BtPesqArq.Enabled   := False;
      BtLinkArq.Enabled   := False;
      //
      BtOk.Enabled := True;
      //
      PCDiretorios.ActivePageIndex := 0;
      TSArquivosWEB.TabVisible     := True;
      TSArquivos.TabVisible        := False;
      TSArquivosWEBPesq.TabVisible := False;
      //
      MyObjects.Informa2(LaAviso1, LaAviso2, False, 'Uploads feitos neste local n�o ser�o salvos no banco de dados!');
    end;
    1: //MySQL
    begin
      BtHome.Enabled      := False;
      BtUpFolder.Enabled  := False;
      BtVoltar.Enabled    := False;
      BtIncluiDir.Enabled := False;
      BtAlteraDir.Enabled := True;
      BtExcluiDir.Enabled := True;
      BtFavoritos.Enabled := False;
      BtPesquisa.Enabled  := True;
      //
      BtUpload.Enabled    := True;
      BtDownload.Enabled  := True;
      BtAlteraArq.Enabled := True;
      BtExcluiArq.Enabled := True;
      BtPesqArq.Enabled   := True;
      BtLinkArq.Enabled   := True;
      //
      BtOk.Enabled := True;
      //
      PCDiretorios.ActivePageIndex := 1;
      TSArquivosWEB.TabVisible     := False;
      TSArquivos.TabVisible        := True;
      TSArquivosWEBPesq.TabVisible := False;
      //
      MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
    end;
    2: //MySQL - Pesquisa
    begin
      BtHome.Enabled      := False;
      BtUpFolder.Enabled  := False;
      BtVoltar.Enabled    := False;
      BtIncluiDir.Enabled := False;
      BtAlteraDir.Enabled := False;
      BtExcluiDir.Enabled := False;
      BtFavoritos.Enabled := False;
      BtPesquisa.Enabled  := False;
      //
      BtUpload.Enabled    := False;
      BtDownload.Enabled  := False;
      BtAlteraArq.Enabled := False;
      BtExcluiArq.Enabled := False;
      BtPesqArq.Enabled   := False;
      BtLinkArq.Enabled   := False;
      //
      BtOk.Enabled := False;
      //
      PCDiretorios.ActivePageIndex := 1;
      TSArquivosWEB.TabVisible     := False;
      TSArquivos.TabVisible        := False;
      TSArquivosWEBPesq.TabVisible := True;
      //
      MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
    end;
  end;
end;

procedure TFmFTPWebArq.DBGItensArqDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
  if DataCol = 0 then
  begin
    DBGItensArq.Canvas.FillRect(Rect);
    DBGItensArq.Canvas.Draw(Rect.Left + 6, Rect.Top + 2, ImgArq.Picture.Bitmap);
  end;
end;

procedure TFmFTPWebArq.DBGItensDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
  if DataCol = 0 then
  begin
    DBGItens.Canvas.FillRect(Rect);
    DBGItens.Canvas.Draw(Rect.Left + 6, Rect.Top + 2, ImgDir.Picture.Bitmap);
  end;
end;

procedure TFmFTPWebArq.DisplayFTP;
var
  K, i, c, Mostra, Its: Integer;
  s, Tam, Data: String;
  Item: TListItem;
  Lista: TStringList;
begin
  EdCaminho.ValueVariant := FTPGer.RetrieveCurrentDir;
  //
  LvDiretorios.Items.Clear;
  LvArquivos.Items.Clear;
  //
  Lista := TStringList.Create;
  try
    FTPGer.List(Lista, '', True);
    //
    for I := 0 to Lista.Count - 1 do
    begin
      K := pos('type=dir;', Lista[I]);
      if K > 0 then
      begin
        Item := LvDiretorios.Items.Add;
        Item.Caption    := UFTP.RetiraNomeFTPList(Lista[I]);
        Item.ImageIndex := 0;
      end else
      begin
        K := pos('type=file;', Lista[I]);
        if K > 0 then
        begin
          s    := UFTP.RetiraNomeFTPList(Lista[I]);
          Tam  := UFTP.RetiraTamFTPList(Lista[I]);
          Data := UFTP.RetiraDataFTPList(Lista[I]);
          //
          Item            := LvArquivos.Items.Add;
          Item.Caption    := s;
          Item.ImageIndex := 1;
          Item.SubItems.Add(Tam);
          Item.SubItems.Add(Data);
        end;
      end;
    end;
  finally
    Lista.Free;
  end;
  Its := LvArquivos.Items.Count;
  //
  if Its = 1 then
    LaInfo.Caption := Geral.FF0(Its) + ' item'
  else
    LaInfo.Caption := Geral.FF0(Its) + ' itens';
end;

procedure TFmFTPWebArq.FormActivate(Sender: TObject);
begin
  if TFmFTPWebArq(Self).Owner is TApplication then
  begin
    MyObjects.CorIniComponente();
  end;
  DmkWeb.CarregaImagemFormWEB(ImgWEB);
end;

procedure TFmFTPWebArq.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType   := stLok;
  FAtivaAfterScroll := False;
  FLastDirStack     := TStringList.Create;
  //
  FInOwner := nil;
  FPager   := nil;
  //
  DBCGNivel.Items.Clear;
  DBCGNivelArq.Items.Clear;
  //
  DBCGNivel.Items.AddStrings(GrlUsuarios.ConfiguraNiveis(CO_DMKID_APP));
  DBCGNivelArq.Items.AddStrings(GrlUsuarios.ConfiguraNiveis(CO_DMKID_APP));
  //
  UnDmkDAC_PF.AbreQuery(QrEntidades, Dmod.MyDB); //DB local para ser mais r�pido
  UFTP.ReopenFTPConfig(0, CO_DMKID_APP, QrFTPConfig, Dmod.MyDBn);
  //
  LvDiretorios.SmallImages := ImageList1;
  LvArquivos.SmallImages   := ImageList1;
  //
  ImgDir.Visible := False;
  ImgArq.Visible := False;
  //
  MostraEdicao(0);
end;

procedure TFmFTPWebArq.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
    [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmFTPWebArq.FormShow(Sender: TObject);
begin
{$IfNDef cSkinRank} //Berlin
{$IfNDef cAlphaSkin} //Berlin
  FmMyGlyfs.DefineGlyfs(TForm(Sender));
{$EndIf}
{$EndIf}
{$IfDef cSkinRank} //Berlin
  if FmPrincipal.Sd1.Active then
    FmMyGlyfs.DefineGlyfsTDI(FmPrincipal.sd1, Sender);
{$EndIf}
{$IfDef cAlphaSkin} //Berlin
  if FmPrincipal.sSkinManager1.Active then
    FmMyGlyfs.DefineGlyfsTDI2(FmPrincipal.sSkinManager1, Sender);
{$EndIf}
end;

procedure TFmFTPWebArq.FTPGerAfterClientLogin(Sender: TObject);
begin
  MostraEdicao(1);
  //
  if PCDiretorios.ActivePageIndex = 1 then
    EdCaminho.ValueVariant := QrFTPWebDirCaminho.Value
  else
    DisplayFtp;
  //
  FLastDirStack.Clear;
  //
  FRootDir := FTPGer.RetrieveCurrentDir;
end;

procedure TFmFTPWebArq.FTPGerDisconnected(Sender: TObject);
begin
  MostraEdicao(0);
end;

procedure TFmFTPWebArq.LvArquivosSelectItem(Sender: TObject; Item: TListItem;
  Selected: Boolean);
begin
  ConfiguraPainelDetalhes(False);
end;

procedure TFmFTPWebArq.LvDiretoriosDblClick(Sender: TObject);
begin
  SelecionaDiretorio();
end;

procedure TFmFTPWebArq.LvDiretoriosKeyPress(Sender: TObject; var Key: Char);
begin
  case Key of
    #13:
      SelecionaDiretorio();
  end;
end;

procedure TFmFTPWebArq.MostraEdicao(Mostra: Integer);
begin
  GBDetalhes.Visible   := False;
  GBDetalhesDB.Visible := False;
  //
  case Mostra of
    0:
    begin
      PnConectar.Visible := True;
      PnDados.Visible    := False;
      //
      EdFTPConfig.Enabled := True;
      CBFTPConfig.Enabled := True;
      //
      if CO_DMKID_APP = 17 then //DControl
        SBFTPConfig.Enabled := True
      else
        SBFTPConfig.Enabled := False;
      //
      BtConectar.Caption := '&Conectar';
      //
      FmMyGlyfs.ObtemBitmapDeGlyph(191, BtConectar);
      //
      MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
    end;
    1:
    begin
      PnConectar.Visible := True;
      PnDados.Visible    := True;
      //
      EdFTPConfig.Enabled := False;
      CBFTPConfig.Enabled := False;
      SBFTPConfig.Enabled := False;
      //
      BtConectar.Caption := '&Desconectar';
      //
      FmMyGlyfs.ObtemBitmapDeGlyph(189, BtConectar);
    end;
  end;
  Application.ProcessMessages;
end;

procedure TFmFTPWebArq.PCDiretoriosChange(Sender: TObject);
var
  Tab: Integer;
begin
  Tab := PCDiretorios.ActivePageIndex;
  //
  ChangeFTPDir(QrFTPWebDirCaminho.Value);
  ConfiguraTipo(Tab);
end;

procedure TFmFTPWebArq.QrFTPWebArqAfterScroll(DataSet: TDataSet);
begin
  UFTP.ReopenFTPCom(dtArq, QrFTPWebArqCodigo.Value, Dmod.MyDBn, QrFTPWebComArq);
  //
  if FAtivaAfterScroll then
    ConfiguraPainelDetalhes(True);
end;

procedure TFmFTPWebArq.QrFTPWebArqBeforeClose(DataSet: TDataSet);
begin
  QrFTPWebComArq.Close;
end;

procedure TFmFTPWebArq.QrFTPWebDirAfterScroll(DataSet: TDataSet);
var
  Msg: String;
begin
  try
    Msg                    := LaAviso1.Caption;
    DBGItens.Enabled       := False;
    EdCaminho.ValueVariant := QrFTPWebDirCaminho.Value;
    //
    MyObjects.Informa2(LaAviso1, LaAviso2, True, '');
    ReopenFTPArq(0, QrFTPWebDirCodigo.Value, CO_DMKID_APP, Dmod.MyDBn, QrFTPWebArq);
    UFTP.ReopenFTPCom(dtDir, QrFTPWebDirCodigo.Value, Dmod.MyDBn, QrFTPWebComDir);
  finally
    MyObjects.Informa2(LaAviso1, LaAviso2, False, Msg);
    //
    DBGItens.Enabled := True;
    Screen.Cursor    := crDefault;
  end;
end;

procedure TFmFTPWebArq.QrFTPWebDirBeforeClose(DataSet: TDataSet);
begin
  QrFTPWebArq.Close;
  QrFTPWebComDir.Close;
end;

procedure TFmFTPWebArq.RedefinirPesquisa;
begin
  CkPesNome.Checked         := False;
  EdPesNome.ValueVariant    := '';
  CkPesTags.Checked         := False;
  EdPesTags.ValueVariant    := '';
  EdPesEmpresa.ValueVariant := 0;
  CBPesEmpresa.KeyValue     := Null;
  EdEntidade.ValueVariant   := 0;
  CBEntidade.KeyValue       := Null;
end;

procedure TFmFTPWebArq.ReopenFTPArq(Codigo, Diretorio, DmkIDApp: Integer;
  Database: TmySQLDatabase; Query: TmySQLQuery);
var
  CliInt, Depto: Integer;
  Nome, Tags: String;
begin
  CliInt := EdPesEmpresa.ValueVariant;
  Depto  := EdEntidade.ValueVariant;
  //
  if CkPesNome.Checked then
    Nome := EdPesNome.ValueVariant
  else
    Nome := '';
  if CkPesTags.Checked then
    Tags := EdPesTags.ValueVariant
  else
    Tags := '';
  //
  UFTP.ReopenFTPArq(Codigo, Diretorio, CliInt, Depto, DmkIDApp, Nome, Tags,
    Database, Query, LaInfo, FAtivaAfterScroll);
end;

procedure TFmFTPWebArq.SBFTPConfigClick(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  //
  UFTPJan.MostraFTPConfig(EdFTPConfig.ValueVariant);
  //
  if VAR_CADASTRO <> 0 then
  begin
    UMyMod.SetaCodigoPesquisado(EdFTPConfig, CBFTPConfig, QrFTPConfig, VAR_CADASTRO);
    //
    EdFTPConfig.SetFocus;
  end;
end;

procedure TFmFTPWebArq.SelecionaDiretorio;
var
  Selected: TListItem;
begin
  if VerificaSeEstaConectado then
  begin
    Selected := LvDiretorios.Selected;
    //
    if Selected <> nil then
      ChangeFTPDir(Selected.Caption);
  end;
end;

end.
