unit Uploads;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkDBLookupComboBox, dmkEdit,
  dmkEditCB, Variants, DmkDAC_PF, mySQLDbTables, UnDmkProcFunc, dmkDBGrid,
  dmkPageControl, Vcl.Menus, dmkCheckGroup, IdBaseComponent, IdComponent,
  IdTCPConnection, IdTCPClient, IdExplicitTLSClientServerBase, IdFTP;

type
  TFmUploads = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    LaTitulo1C: TLabel;
    Panel5: TPanel;
    EdCond: TdmkEditCB;
    CBCond: TdmkDBLookupComboBox;
    BitBtn1: TBitBtn;
    CBDiretorio: TdmkDBLookupComboBox;
    Label1: TLabel;
    EdDiretorio: TdmkEditCB;
    EdDescricao: TdmkEdit;
    CkDescricao: TCheckBox;
    CkTags: TCheckBox;
    EdTags: TdmkEdit;
    GBEmissao: TGroupBox;
    CkDataIni: TCheckBox;
    TPDataIni: TDateTimePicker;
    CkDataFim: TCheckBox;
    TPDataFim: TDateTimePicker;
    GroupBox2: TGroupBox;
    CkDataExpIni: TCheckBox;
    TPDataExpIni: TDateTimePicker;
    CkDataExpFim: TCheckBox;
    TPDataExpFim: TDateTimePicker;
    Label2: TLabel;
    BtLimpar: TBitBtn;
    QrUploads: TmySQLQuery;
    QrUploadsCodigo: TAutoIncField;
    QrUploadsNome: TWideStringField;
    QrUploadsArquivo: TWideStringField;
    QrUploadsCond: TIntegerField;
    QrUploadsLk: TIntegerField;
    QrUploadsDataCad: TDateField;
    QrUploadsDataAlt: TDateField;
    QrUploadsUserCad: TIntegerField;
    QrUploadsUserAlt: TIntegerField;
    QrUploadsAlterWeb: TSmallintField;
    QrUploadsAtivo: TSmallintField;
    QrUploadsDirWeb: TIntegerField;
    QrUploadsNDIRWEB: TWideStringField;
    QrUploadsDIRNOME: TWideStringField;
    QrUploadsNCONDOM: TWideStringField;
    QrUploadsDataExp: TDateField;
    QrUploadsDataExp_TXT: TWideStringField;
    DsUploads: TDataSource;
    QrUploadsDataHora: TDateTimeField;
    QrUploadsTags: TWideStringField;
    QrCond: TmySQLQuery;
    DsCond: TDataSource;
    QrCondCodigo: TIntegerField;
    QrCondNome: TWideStringField;
    QrDirWeb: TmySQLQuery;
    IntegerField1: TIntegerField;
    DsDirWeb: TDataSource;
    QrDirWebNome: TWideStringField;
    BtInclui: TBitBtn;
    BtAltera: TBitBtn;
    BtExclui: TBitBtn;
    QrDirWebFotos: TSmallintField;
    PageControl1: TdmkPageControl;
    TabSheet1: TTabSheet;
    DBGArquivos: TdmkDBGrid;
    OpenDialog1: TOpenDialog;
    CkFotos: TCheckBox;
    BtExcluiExp: TBitBtn;
    QrLoc: TmySQLQuery;
    PMInclui: TPopupMenu;
    Arquivos1: TMenuItem;
    Fotos1: TMenuItem;
    ImgIconDB: TImage;
    FTPGer: TIdFTP;
    QrUploadsPasta: TWideStringField;
    PB1: TProgressBar;
    QrUploadsDirFotos: TSmallintField;
    ImgWEB: TdmkImage;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtLimparClick(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure BtIncluiClick(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure Arquivos1Click(Sender: TObject);
    procedure Fotos1Click(Sender: TObject);
    procedure QrUploadsAfterScroll(DataSet: TDataSet);
    procedure CkFotosClick(Sender: TObject);
    procedure QrUploadsBeforeClose(DataSet: TDataSet);
    procedure BtExcluiClick(Sender: TObject);
    procedure BtExcluiExpClick(Sender: TObject);
  private
    { Private declarations }
    FDirRaiz: String;
    procedure RedefinirPesquisa();
    procedure ReopenUploads(Codigo: Integer);
    procedure ExcluiArquivosExpirados(Diretorio: Integer);
    procedure HabilitaBotoes(Habilita: Boolean);
    function  ExcluiRegistro(Codigo, Foto: Integer; Arquivo, Nome, DirWeb: String;
                var Cond, CodDirWeb: Integer): Boolean;
    function  VerificaSeEstaConectado(): Boolean;
  public
    { Public declarations }
  end;

  var
  FmUploads: TFmUploads;

implementation

uses UnMyObjects, Module, ModuleGeral, UnFTP, MyListas, MyDBCheck, UnDmkWeb;

{$R *.DFM}

procedure TFmUploads.Arquivos1Click(Sender: TObject);
var
  CliInt, FTPDir, FTPWebArq: Integer;
  Arq, Nome, PastaCam: String;
begin
  try
    OpenDialog1.Options := [ofHideReadOnly, ofShareAware, ofEnableSizing];
    //
    if OpenDialog1.Execute then
    begin
      HabilitaBotoes(False);
      try
        Arq      := ExtractFileName(OpenDialog1.FileName);
        Nome     := ChangeFileExt(Arq, '');
        PastaCam := '';
        FTPDir   := EdDiretorio.ValueVariant;
        CliInt   := EdCond.ValueVariant;
        //
        FTPWebArq := UFTP.UploadFTP_DB(FmUploads.PageControl1, nil, stIns,
                       Arq, Nome, '', '', 0, CO_DMKID_APP, 0, 0, FTPDir, CliInt, 0,
                       dtPad, 0, 0, OpenDialog1.Files, True, Dmod.QrUpdN,
                       Dmod.QrAuxN, Dmod.MyDBn, PastaCam);
        //
        if FTPWebArq <> 0 then
        begin
          UnDmkDAC_PF.AbreMySQLQuery0(QrLoc, Dmod.MyDBn, [
            'SELECT Cond, DirWeb ',
            'FROM uploads ',
            'WHERE Codigo=' + Geral.FF0(FTPWebArq),
            '']);
          //
          CliInt := QrLoc.FieldByName('Cond').AsInteger;
          //
          RedefinirPesquisa;
          //
          EdCond.ValueVariant := CliInt;
          CBCond.KeyValue     := CliInt;
          //
          ReopenUploads(FTPWebArq);
        end;
      finally
        HabilitaBotoes(True);
      end;
    end;
  except
    Geral.MB_Aviso('Falha ao realizar upload!');
  end;
end;

procedure TFmUploads.BitBtn1Click(Sender: TObject);
var
  Cond: Integer;
begin
  Cond := EdCond.ValueVariant;
  //
  if MyObjects.FIC(Cond = 0, EdCond, 'Defina o condom�nio!') then Exit;
  //
  ReopenUploads(0);
end;

procedure TFmUploads.BtAlteraClick(Sender: TObject);
var
  Codigo: Integer;
  PastaCam, Nome: String;
  Res: Boolean;
begin
  if (QrUploads.State <> dsInactive) and (QrUploads.RecordCount > 0) then
  begin
    Codigo   := QrUploadsCodigo.Value;
    PastaCam := '';
    Res      := False;
    //
    if QrUploadsDirFotos.Value = 1 then //Fotos
    begin
      Nome := QrUploadsNome.Value;
      //
      if InputQuery('Descri��o', 'Informe uma descri��o:', Nome) then
      begin
        Res := UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpdN, Dmod.MyDBn, [
                 'UPDATE uploads SET Nome="' + Nome + '" ',
                 'WHERE Codigo=' + Geral.FF0(Codigo),
                 '']);
      end;
    end else
    begin
    Res := UFTP.UploadFTP_DB(FmUploads.PageControl1, nil, stUpd,
             QrUploadsArquivo.Value, QrUploadsNome.Value, '', QrUploadsTags.Value,
             Codigo, CO_DMKID_APP, 0, 0, QrUploadsDirWeb.Value,
             QrUploadsCond.Value, 0, dtPad, 0, QrUploadsDataExp.Value, nil,
             True, Dmod.QrUpdN, Dmod.QrAuxN, Dmod.MyDBn, PastaCam) <> 0;
    end;
    if Res then
      ReopenUploads(Codigo);
  end;
end;

procedure TFmUploads.BtExcluiClick(Sender: TObject);
var
  i, Cond, CodDirWeb: Integer;
begin
  if (QrUploads.State <> dsInactive) and (QrUploads.RecordCount > 0) then
  begin
    if MyObjects.FIC(EdCond.ValueVariant = 0, EdCond, 'Selecione o condom�nio e tente novamente!') then Exit;
    if MyObjects.FIC(EdDiretorio.ValueVariant = 0, EdDiretorio, 'Selecione o diret�rio e tente novamente!') then Exit;
    //
    if DBGArquivos.SelectedRows.Count > 1 then
    begin
      if Geral.MB_Pergunta('Confirma a exclus�o dos �tens selecionados?' + sLineBreak +
        'ATEN��O: Ao excluir o item, este tamb�m ser� exclu�do do servidor FTP!') <> ID_YES
      then
        Exit;
      //
      HabilitaBotoes(False);
      try
        with DBGArquivos.DataSource.DataSet do
        begin
          for i:= 0 to DBGArquivos.SelectedRows.Count-1 do
          begin
            //GotoBookmark(pointer(DBGArquivos.SelectedRows.Items[i]));
            GotoBookmark(DBGArquivos.SelectedRows.Items[i]);
            try
              Screen.Cursor := crHourGlass;
              //
              Cond      := QrUploadsCond.Value;
              CodDirWeb := QrUploadsDirWeb.Value;
              //
              if not ExcluiRegistro(QrUploadsCodigo.Value, QrUploadsDirFotos.Value,
                QrUploadsArquivo.Value, QrUploadsNome.Value, QrUploadsNDIRWEB.Value,
                Cond, CodDirWeb) then
              begin
                Geral.MensagemBox('Falha ao excluir arquivo!', 'Aviso',
                  MB_OK+MB_ICONWARNING);
                Exit;
              end;
            finally
              Screen.Cursor := crDefault;
            end;
          end;
        end;
        ReopenUploads(0);
      finally
        HabilitaBotoes(True);
      end;
    end else
    begin
      try
        Screen.Cursor := crHourGlass;
        //
        if Geral.MB_Pergunta('Confirma a exclus�o do �tem ' + QrUploadsNome.Value + '?'
          + sLineBreak +
          'ATEN��O: Ao excluir o item, este tamb�m ser� exclu�do do servidor FTP!') = ID_YES then
        begin
          HabilitaBotoes(False);
          try
            Cond      := QrUploadsCond.Value;
            CodDirWeb := QrUploadsDirWeb.Value;
            //
            if not ExcluiRegistro(QrUploadsCodigo.Value, QrUploadsDirFotos.Value,
              QrUploadsArquivo.Value, QrUploadsNome.Value, QrUploadsNDIRWEB.Value,
              Cond, CodDirWeb) then
            begin
              Geral.MensagemBox('Falha ao excluir arquivo!', 'Aviso',
                MB_OK+MB_ICONWARNING);
              Exit;
            end;
            ReopenUploads(0);
          finally
            HabilitaBotoes(True);
          end;
        end;
      finally
        Screen.Cursor := crDefault;
      end;
    end;
  end;
end;

procedure TFmUploads.BtExcluiExpClick(Sender: TObject);
begin
  if Geral.MensagemBox('Confirma a exclus�o de todos os arquivos expirados?',
    'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES
  then
    ExcluiArquivosExpirados(0);
end;

procedure TFmUploads.BtIncluiClick(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 0;
  //
  MyObjects.MostraPopUpDeBotao(PMInclui, BtInclui);
end;

procedure TFmUploads.BtLimparClick(Sender: TObject);
begin
  RedefinirPesquisa;
end;

procedure TFmUploads.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmUploads.CkFotosClick(Sender: TObject);
begin
  QrUploads.Close;
end;

procedure TFmUploads.ExcluiArquivosExpirados(Diretorio: Integer);
var
  Cond, CodDirWeb: Integer;
  SQL: String;
begin
  if Diretorio <> 0 then
    SQL := 'AND upl.DirWeb=' + Geral.FF0(Diretorio)
  else
    SQL := '';
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrLoc, Dmod.MyDBn, [
  'SELECT upl.*, dir.Nome DIRNOME, dir.Fotos DirFotos ',
  'FROM uploads upl ',
  'LEFT JOIN dirweb dir ON dir.Codigo=upl.dirweb ',
  'WHERE upl.DataExp > "1899-12-30" ',
  SQL,
  'AND upl.DataExp < "' + Geral.FDT(DmodG.ObtemAgora(), 1) + '"',
  '']);
  if QrLoc.RecordCount > 0 then
  begin
    try
      Screen.Cursor := crHourGlass;
      //
      PB1.Position := 0;
      PB1.Max      := QrLoc.RecordCount;
      MyObjects.Informa2(LaAviso1, LaAviso2, False,
        'Excluindo itens expirados!');
      //
      QrLoc.First;
      while not QrLoc.Eof do
      begin
        Cond      := QrLoc.FieldByName('Cond').AsInteger;
        CodDirWeb := QrLoc.FieldByName('DirWeb').AsInteger;
        //
        if not ExcluiRegistro(QrLoc.FieldByName('Codigo').AsInteger,
          QrLoc.FieldByName('DirFotos').AsInteger,
          QrLoc.FieldByName('Arquivo').AsString, QrLoc.FieldByName('Nome').AsString,
          QrLoc.FieldByName('DIRNOME').AsString, Cond, CodDirWeb) then
        begin
          Geral.MensagemBox('Falha ao remover arquivo do servidor FTP!', 'Aviso',
            MB_OK+MB_ICONWARNING);
          Exit;
        end;
        PB1.Position := PB1.Position + 1;
        PB1.Update;
        Application.ProcessMessages;
        //
        QrLoc.Next;
      end;
      ReopenUploads(0);
    finally
      MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
      Screen.Cursor := crDefault;
    end;
  end;
end;

function TFmUploads.ExcluiRegistro(Codigo, Foto: Integer; Arquivo, Nome,
  DirWeb: String; var Cond, CodDirWeb: Integer): Boolean;
var
  R, L: String;
  Continua: Boolean;
begin
  if MyObjects.FIC(Codigo = 0, nil, 'C�digo n�o definido!') then Exit;
  if MyObjects.FIC(Arquivo = '', nil, 'Arquivo n�o definido!') then Exit;
  if MyObjects.FIC(DirWeb = '', nil, 'Nome diret�rio WEB n�o definido!') then Exit;
  if MyObjects.FIC(Cond = 0, nil, 'Condom�nio n�o definido!') then Exit;
  if MyObjects.FIC(CodDirWeb = 0, nil, 'C�digo do diret�rio WEB n�o definido!') then Exit;
  //
  if not VerificaSeEstaConectado then
  begin
    Geral.MB_Aviso('Voc� n�o est� conectado no servidor Web!' + sLineBreak +
      'Clique no bot�o Reabre e tente novamente!');
    Exit;
  end;
  //
  Continua := False;
  L        := Arquivo;
  R        := DirWeb + '/' + L;
  //
  FTPGer.ChangeDir(DirWeb);
  if UFTP.VerificaSeArquivoExisteNoFTP(FTPGer, L, '', '', '', False, True) then //Verifica se arquivo existe no servidor FTP
  begin
    //Exclui arquivo do servidor FTP
    Continua := UFTP.FTPDeleteArquivo(FTPGer, Geral.Substitui(R, ('/' + L), ''),
                  L, '', '', '', False, True);
    //
    if not Continua then
    begin
      Geral.MensagemBox('Falha ao remover arquivo do servidor FTP!', 'Aviso',
        MB_OK+MB_ICONWARNING);
      Exit;
    end;
  end else
  begin
    if Geral.MB_Pergunta('O arquivo n�o foi localizado no servidor FTP!' +
      sLineBreak + 'Deseja exclu�-lo do banco de dados?') <> ID_YES then
    begin
      Result := False;
      Exit;
    end else
      Continua := True;
  end;
  //
  //Exclui miniatura
  if Foto = 1 then
  begin
    Continua := False;
    L        := 'min_' + Arquivo;
    R        := DirWeb + '/' + L;
    //
    FTPGer.ChangeDir(DirWeb);
    if UFTP.VerificaSeArquivoExisteNoFTP(FTPGer, L, '', '', '', False, True) then //Verifica se arquivo existe no servidor FTP
    begin
      //Exclui arquivo do servidor FTP
      Continua := UFTP.FTPDeleteArquivo(FTPGer, Geral.Substitui(R, ('/' + L), ''),
                    L, '', '', '', False, True);
      //
      if not Continua then
      begin
        Geral.MensagemBox('Falha ao remover arquivo do servidor FTP!', 'Aviso',
          MB_OK+MB_ICONWARNING);
        Exit;
      end;
    end else
    begin
      if Geral.MB_Pergunta('O arquivo n�o foi localizado no servidor FTP!' +
        sLineBreak + 'Deseja exclu�-lo do banco de dados?') <> ID_YES then
      begin
        Result := False;
        Exit;
      end else
        Continua := True;
    end;
  end;
  if Continua then
  begin
    try
      Screen.Cursor := crHourGlass;
      //
      Dmod.QrUpdN.SQL.Clear;
      Dmod.QrUpdN.SQL.Add('DELETE FROM uploads WHERE Codigo=:P0');
      Dmod.QrUpdN.Params[0].AsInteger := Codigo;
      Dmod.QrUpdN.ExecSQL;
    finally
      Screen.Cursor := crDefault;
    end;
  end;
  Result := Continua;
end;

procedure TFmUploads.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  DmkWeb.CarregaImagemFormWEB(ImgWEB);
end;

procedure TFmUploads.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  FDirRaiz        := '';
  //
  UnDmkDAC_PF.AbreQuery(QrCond, Dmod.MyDB); //Para ser mais r�pido
  UnDmkDAC_PF.AbreQuery(QrDirWeb, Dmod.MyDBn);
end;

procedure TFmUploads.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmUploads.Fotos1Click(Sender: TObject);
const
  Aviso  = '...';
var
  Titulo, Prompt, Campo, Host, Usuario, Senha, Raiz, Pasta, PastaCam: String;
  Passivo: Boolean;
  Cond: Integer;
  CliInt, FTPDir: Variant;
begin
  try
    OpenDialog1.Options := [ofHideReadOnly, ofAllowMultiSelect, ofShareAware, ofEnableSizing];
    //
    if OpenDialog1.Execute then
    begin
      Titulo := 'Sele��o de condom�nio';
      Prompt := 'Informe o condom�nio:';
      Campo  := 'Descricao';
      Cond   := EdCond.ValueVariant;
      //
      CliInt := DBCheck.EscolheCodigoUnico(Aviso, Titulo, Prompt, nil, nil, Campo, Cond, [
        'SELECT con.Codigo, IF(ent.Tipo = 0, ent.RazaoSocial, ent.Nome) ' + Campo + ' ',
        'FROM cond con ',
        'LEFT JOIN entidades  ent ON ent.Codigo = con.Cliente ',
        'WHERE con.Ativo = 1 ',
        'ORDER BY ' + Campo + ' ',
        ''], Dmod.MyDB, False);
      //
      if CliInt <> Null then
      begin
        Titulo := 'Sele��o de diret�rio WEB';
        Prompt := 'Informe o diret�rio:';
        Campo  := 'Descricao';
        //
        FTPDir := DBCheck.EscolheCodigoUnico(Aviso, Titulo, Prompt, nil, nil, Campo, 0, [
          'SELECT Codigo, Nome ' + Campo + ' ',
          'FROM dirweb ',
          'WHERE Ativo = 1 ',
          'AND Fotos = 1 ',
          'ORDER BY ' + Campo + ' ',
          ''], Dmod.MyDBn, False);
        //
        if FTPDir <> Null then
        begin
          UnDmkDAC_PF.AbreMySQLQuery0(QrLoc, Dmod.MyDBn, [
            'SELECT Pasta ',
            'FROM dirweb ',
            'WHERE Codigo=' + Geral.FF0(FTPDir),
            '']);
          if QrLoc.RecordCount > 0 then
            Pasta := QrLoc.FieldByName('Pasta').AsString
          else
            Exit;
          //
          HabilitaBotoes(False);
          try
            UFTP.ReopenFTPConfig(0, CO_DMKID_APP, QrLoc, Dmod.MyDBn);
            //
            Host     := QrLoc.FieldByName('Web_FTPh').AsString;
            Usuario  := QrLoc.FieldByName('Web_FTPu').AsString;
            Senha    := QrLoc.FieldByName('SENHA').AsString;
            Passivo  := Geral.IntToBool(QrLoc.FieldByName('Passivo').AsInteger);
            Raiz     := QrLoc.FieldByName('Web_Raiz').AsString;
            PastaCam := Pasta;
            //
            if UFTP.UploadFTP_Fotos(FmUploads.PageControl1, nil, PastaCam,
              Host, Usuario, Senha, Passivo, False, OpenDialog1.Files,
              CO_DMKID_APP, CliInt, 0, FTPDir, 0, dtPad, Dmod.QrUpdN, Dmod.MyDBn) <> '' then
            begin
              EdCond.ValueVariant      := CliInt;
              CBCond.KeyValue          := CliInt;
              EdDiretorio.ValueVariant := FTPDir;
              CBDiretorio.KeyValue     := FTPDir;
              //
              ReopenUploads(0);
            end;
          finally
            HabilitaBotoes(True);
          end;
        end;
      end;
    end;
  except
    Geral.MB_Aviso('Falha ao realizar upload!');
  end;
end;

procedure TFmUploads.HabilitaBotoes(Habilita: Boolean);
begin
  BtInclui.Enabled    := Habilita;
  BtAltera.Enabled    := Habilita;
  BtExclui.Enabled    := Habilita;
  BtExcluiExp.Enabled := Habilita;
end;

procedure TFmUploads.QrUploadsAfterScroll(DataSet: TDataSet);
var
  Cam: String;
begin
  if (CkFotos.Checked) and (VerificaSeEstaConectado) then
  begin
    if (QrUploadsDirFotos.Value = 1) then
    begin
      Cam               := QrUploadsPasta.Value;
      ImgIconDB.Visible := True;
      //
      FTPGer.ChangeDir(Cam);
      //
      UFTP.CarregaMiniatura(0, 'min_' + QrUploadsArquivo.Value, FTPGer, ImgIconDB);
    end else
      ImgIconDB.Visible := False;
  end else
    ImgIconDB.Visible := False;
end;

procedure TFmUploads.QrUploadsBeforeClose(DataSet: TDataSet);
begin
  ImgIconDB.Picture := nil;
end;

procedure TFmUploads.RedefinirPesquisa;
begin
  EdCond.ValueVariant      := 0;
  CBCond.KeyValue          := Null;
  EdDiretorio.ValueVariant := 0;
  CBDiretorio.KeyValue     := Null;
  CkDescricao.Checked      := False;
  EdDescricao.ValueVariant := '';
  CkTags.Checked           := False;
  EdTags.ValueVariant      := '';
  CkDataIni.Checked        := False;
  TPDataIni.Date           := DmodG.ObtemAgora;
  CkDataFim.Checked        := False;
  TPDataFim.Date           := DModG.ObtemAgora;
  CkDataExpIni.Checked     := False;
  TPDataExpIni.Date        := DModG.ObtemAgora;
  CkDataExpFim.Checked     := False;
  TPDataExpFim.Date        := DModG.ObtemAgora;
  CkFotos.Checked          := True;
  //
  QrUploads.Close;
end;

procedure TFmUploads.ReopenUploads(Codigo: Integer);
var
  Cond, Dir: Integer;
  Passivo: Boolean;
  Descri, Tags, SQLDir, SQLDescri, SQLTag, SQLData, SQLDataExp,
  Host, Usuario, Senha, PastaCam: String;
begin
  if (CkFotos.Checked) and (not VerificaSeEstaConectado) then
  begin
    Screen.Cursor := crHourGlass;
    try
      UFTP.ReopenFTPConfig(0, CO_DMKID_APP, QrLoc, Dmod.MyDBn);
      //
      Host     := QrLoc.FieldByName('Web_FTPh').AsString;
      Usuario  := QrLoc.FieldByName('Web_FTPu').AsString;
      Senha    := QrLoc.FieldByName('SENHA').AsString;
      Passivo  := Geral.IntToBool(QrLoc.FieldByName('Passivo').AsInteger);
      FDirRaiz := QrLoc.FieldByName('Web_Raiz').AsString;
      PastaCam := FDirRaiz + '/' + CO_FTP_DIRPAD;
      //
      try
        UFTP.ConectaServidorFTP(Host, Usuario, Senha, Passivo, FTPGer);
      except
        Geral.MB_Aviso('Falha ao conectar no servidor WEB!');
      end;
    finally
      Screen.Cursor := crDefault;
    end;
  end;
  //
  Cond   := EdCond.ValueVariant;
  Dir    := EdDiretorio.ValueVariant;
  Descri := EdDescricao.ValueVariant;
  Tags   := EdTags.ValueVariant;
  //
  if CkDescricao.Checked then
    SQLDescri := 'AND upl.Nome LIKE "%' + Descri + '%"'
  else
    SQLDescri := '';
  if CkTags.Checked then
    SQLTag := 'AND upl.Tags LIKE "%' + Tags + '%"'
  else
    SQLTag := '';
  if Dir <> 0 then
    SQLDir := 'AND upl.DirWeb=' + Geral.FF0(Dir)
  else
    SQLDir := '';
  //
  SQLData := dmkPF.SQL_Periodo('AND upl.DataHora ', TPDataIni.Date,
               TPDataFim.Date, CkDataIni.Checked, CkDataFim.Checked);
  //
  SQLTag := dmkPF.SQL_Periodo('AND upl.DataExp ', TPDataExpIni.Date,
              TPDataExpFim.Date, CkDataExpIni.Checked, CkDataExpFim.Checked);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrUploads, Dmod.MyDBn, [
    'SELECT upl.*, dir.Pasta NDIRWEB, dir.Nome DIRNOME, dir.Fotos DirFotos, dir.Pasta, ',
    'IF (rec.Tipo=0, rec.RazaoSocial, rec.Nome) NCONDOM, ',
    'IF(upl.DataExp <= "1899-12-30", "", DATE_FORMAT(upl.DataExp, "%d/%m/%Y")) DataExp_TXT ',
    'FROM uploads upl ',
    'LEFT JOIN dirweb dir ON dir.Codigo=upl.dirweb ',
    'LEFT JOIN cond con ON con.Codigo=upl.Cond ',
    'LEFT JOIN entidades rec ON rec.Codigo=con.Cliente ',
    'WHERE upl.Cond=' + Geral.FF0(Cond),
    SQLDir,
    SQLDescri,
    SQLTag,
    SQLData,
    SQLDataExp,
    'ORDER BY NCONDOM, upl.DirWeb, upl.Nome ',
    '']);
  //
  if Codigo <> 0 then
    QrUploads.Locate('Codigo', Codigo, []);
end;

function TFmUploads.VerificaSeEstaConectado(): Boolean;
var
  Conectado: Boolean;
begin
  try
    Conectado := FTPGer.Connected;
  except
    Conectado := False;
  end;
  Result := Conectado;
end;

end.
