unit UploadsIts;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEditDateTimePicker,
  dmkDBLookupComboBox, dmkEditCB, dmkEdit, mySQLDbTables, DmkDAC_PF;

type
  TFmUploadsIts = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    EdNome: TdmkEdit;
    LaCond: TLabel;
    EdCond: TdmkEditCB;
    CBCond: TdmkDBLookupComboBox;
    Label1: TLabel;
    EdDirWeb: TdmkEditCB;
    CBDirWeb: TdmkDBLookupComboBox;
    TPDataExp: TdmkEditDateTimePicker;
    Label4: TLabel;
    QrCond: TmySQLQuery;
    QrCondCodigo: TIntegerField;
    QrCondNome: TWideStringField;
    DsCond: TDataSource;
    QrDirWeb: TmySQLQuery;
    IntegerField1: TIntegerField;
    QrDirWebNome: TWideStringField;
    QrDirWebFotos: TSmallintField;
    DsDirWeb: TDataSource;
    Label2: TLabel;
    EdTags: TdmkEdit;
    QrDirWebPasta: TWideStringField;
    EdCodigo: TdmkEdit;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    procedure MostraEdicao(Codigo: Integer);
  public
    { Public declarations }
    FNome, FPastaCam, FTags: String;
    FCodigo, FFTPDir, FEmpresa: Integer;
    FDataExp: TDate;
    FDesiste: Boolean;
  end;

  var
  FmUploadsIts: TFmUploadsIts;

implementation

uses UnMyObjects, Module;

{$R *.DFM}

procedure TFmUploadsIts.BtOKClick(Sender: TObject);
var
  Cond, Dir: Integer;
  Nome, PastaCam, Tags: String;
  DataExp: TDate;
begin
  Cond     := EdCond.ValueVariant;
  Dir      := EdDirWeb.ValueVariant;
  Nome     := EdNome.ValueVariant;
  Tags     := EdTags.ValueVariant;
  PastaCam := QrDirWebPasta.Value;
  DataExp  := TPDataExp.Date;
  //
  if MyObjects.FIC(Nome = '', EdNome, 'Defina uma descri��o!') then Exit;
  if MyObjects.FIC(Cond = 0, EdCond, 'Defina um condom�nio!') then Exit;  
  if MyObjects.FIC(Dir = 0, EdDirWeb, 'Defina um diret�rio WEB!') then Exit;
  //
  FNome     := Nome;
  FTags     := Tags;
  FFTPDir   := Dir;
  FEmpresa  := Cond;
  FPastaCam := PastaCam;
  FDataExp  := DataExp;
  //
  VAR_COD := EdCodigo.ValueVariant;
  Close;
end;

procedure TFmUploadsIts.BtSaidaClick(Sender: TObject);
begin
  FNome     := '';
  FTags     := '';
  FFTPDir   := 0;
  FEmpresa  := 0;
  FPastaCam := '';
  FDataExp  := 0;
  FDesiste  := True;
  //
  Close;
end;

procedure TFmUploadsIts.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmUploadsIts.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  UnDmkDAC_PF.AbreQuery(QrCond, Dmod.MyDB); //Para ser mais r�pido
  UnDmkDAC_PF.AbreQuery(QrDirWeb, Dmod.MyDBn);
  //
  FDesiste := False;
end;

procedure TFmUploadsIts.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
    [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmUploadsIts.FormShow(Sender: TObject);
begin
  MostraEdicao(FCodigo);
end;

procedure TFmUploadsIts.MostraEdicao(Codigo: Integer);
begin
  EdCodigo.ValueVariant   := FormatFloat('0', Codigo);
  EdNome.ValueVariant     := FNome;
  EdDirWeb.ValueVariant   := FFTPDir;
  CBDirWeb.KeyValue       := FFTPDir;
  EdCond.ValueVariant     := FEmpresa;
  CBCond.KeyValue         := FEmpresa;
  EdTags.ValueVariant     := FTags;
  TPDataExp.Date          := FDataExp;
  EdNome.SetFocus;
end;

end.
