unit FTPConfig;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2,
  ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt, UnInternalConsts2,
  UMySQLModule, mySQLDbTables, UnMySQLCuringa, dmkGeral, dmkPermissoes,
  dmkEdit, dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup, unDmkProcFunc,
  dmkCheckBox, Vcl.Grids, UnDmkEnums, UnDmkWeb, DmkDAC_PF;

type
  TFmFTPConfig = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    BtExclui: TBitBtn;
    BtAltera: TBitBtn;
    BtInclui: TBitBtn;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    QrFTPConfig: TmySQLQuery;
    DsFTPConfig: TDataSource;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    Label1: TLabel;
    DBEdCodigo: TdmkDBEdit;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    CkPassivo: TCheckBox;
    dmkCkAtivo: TdmkCheckBox;
    CkContinuar: TCheckBox;
    GroupBox3: TGroupBox;
    Label14: TLabel;
    Label15: TLabel;
    Label17: TLabel;
    Label5: TLabel;
    Label3: TLabel;
    Label16: TLabel;
    EdWeb_Raiz: TdmkEdit;
    EdWeb_FTPu: TdmkEdit;
    EdWeb_FTPh: TdmkEdit;
    EdWeb_MyURL: TdmkEdit;
    EdWeb_FTPs: TdmkEdit;
    EdNome: TdmkEdit;
    Label10: TLabel;
    QrFTPConfigWeb_FTPsenha: TWideStringField;
    QrFTPConfigCodigo: TIntegerField;
    QrFTPConfigNome: TWideStringField;
    QrFTPConfigWeb_MyURL: TWideStringField;
    QrFTPConfigWeb_FTPu: TWideStringField;
    QrFTPConfigWeb_FTPs: TWideStringField;
    QrFTPConfigWeb_FTPh: TWideStringField;
    QrFTPConfigWeb_Raiz: TWideStringField;
    QrFTPConfigLk: TIntegerField;
    QrFTPConfigDataCad: TDateField;
    QrFTPConfigDataAlt: TDateField;
    QrFTPConfigUserCad: TIntegerField;
    QrFTPConfigUserAlt: TIntegerField;
    QrFTPConfigAlterWeb: TSmallintField;
    QrFTPConfigAtivo: TSmallintField;
    QrFTPConfigPassivo: TSmallintField;
    Label2: TLabel;
    dmkDBEdit2: TdmkDBEdit;
    GroupBox1: TGroupBox;
    Label4: TLabel;
    Label6: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    dmkDBEdit3: TdmkDBEdit;
    dmkDBEdit4: TdmkDBEdit;
    dmkDBEdit5: TdmkDBEdit;
    dmkDBEdit6: TdmkDBEdit;
    dmkDBEdit7: TdmkDBEdit;
    DBCheckBox2: TDBCheckBox;
    CkAtivo: TDBCheckBox;
    BtTestar: TBitBtn;
    Grade: TStringGrid;
    SbImporta: TBitBtn;
    SbExporta: TBitBtn;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    ImgWEB: TdmkImage;
    CkCfgPadrao: TCheckBox;
    QrFTPConfigCfgPadrao: TIntegerField;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrFTPConfigAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrFTPConfigBeforeOpen(DataSet: TDataSet);
    procedure BtIncluiClick(Sender: TObject);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
    procedure BtTestarClick(Sender: TObject);
    procedure SbImportaClick(Sender: TObject);
    procedure SbExportaClick(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    //
    procedure MostraEdicao(Mostra : Integer; SQLType: TSQLType; Codigo : Integer);
  public
    DBCheckBox1: TDBCheckBox;
    { Public declarations }
    procedure LocCod(Atual, Codigo: Integer);
  end;

var
  FmFTPConfig: TFmFTPConfig;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, UnFTP, MyListas, UnGrl_Consts;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmFTPConfig.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmFTPConfig.MostraEdicao(Mostra: Integer; SQLType: TSQLType;
  Codigo: Integer);
begin
  case Mostra of
    1:  
    begin
      PnDados.Visible := False;
      PnEdita.Visible := True;
      //
      if SQLType = stIns then
      begin
        EdCodigo.ValueVariant    := FormatFloat('000', Codigo);
        EdNome.ValueVariant      := '';
        EdWeb_FTPh.ValueVariant  := '';
        EdWeb_Raiz.ValueVariant  := '';
        EdWeb_FTPu.ValueVariant  := '';
        EdWeb_FTPs.ValueVariant  := '';
        EdWeb_MyURL.ValueVariant := '';
        dmkCkAtivo.Checked       := True;
        CkPassivo.Checked        := False;
        CkCfgPadrao.Checked      := False;
        CkContinuar.Checked      := True;
        CkContinuar.Visible      := True;
      end else begin
        EdCodigo.ValueVariant    := QrFTPConfigCodigo.Value;
        EdNome.ValueVariant      := QrFTPConfigNome.Value;
        EdWeb_FTPh.ValueVariant  := QrFTPConfigWeb_FTPh.Value;
        EdWeb_Raiz.ValueVariant  := QrFTPConfigWeb_Raiz.Value;
        EdWeb_FTPu.ValueVariant  := QrFTPConfigWeb_FTPu.Value;
        EdWeb_FTPs.ValueVariant  := QrFTPConfigWeb_FTPsenha.Value;
        EdWeb_MyURL.ValueVariant := QrFTPConfigWeb_MyURL.Value;
        CkPassivo.Checked        := Geral.IntToBool(QrFTPConfigPassivo.Value);
        dmkCkAtivo.Checked       := Geral.IntToBool(QrFTPConfigAtivo.Value);
        CkCfgPadrao.Checked      := Geral.IntToBool(QrFTPConfigCfgPadrao.Value);
        CkContinuar.Checked      := False;
        CkContinuar.Visible      := False;
      end;
      EdNome.SetFocus;
    end else
    begin
      PnDados.Visible := True;
      PnEdita.Visible := False;
    end;
  end;
  ImgTipo.SQLType := SQLType;
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmFTPConfig.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrFTPConfigCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmFTPConfig.DefParams;
var
  Crypt: String;
begin
  //Adicionar a vari�vel CO_RandStrWeb01 na internet caso exista
  Crypt := CO_RandStrWeb01;
  //
  VAR_GOTOTABELA := 'ftpconfig';
  VAR_GOTOMYSQLTABLE := QrFTPConfig;
  VAR_GOTONEG := gotoPos;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDBn;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT *,');
  VAR_SQLx.Add('AES_DECRYPT(Web_FTPs, "' + Crypt +'") Web_FTPSenha');
  VAR_SQLx.Add('FROM ftpconfig');
  VAR_SQLx.Add('WHERE Codigo <> 0');
  //
  VAR_SQL1.Add('AND Codigo=:P0');
  //
  //VAR_SQL2.Add('AND CodUsu=:P0');
  //
  VAR_SQLa.Add('AND Nome Like :P0');
end;

procedure TFmFTPConfig.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmFTPConfig.QueryPrincipalAfterOpen;
begin
end;

procedure TFmFTPConfig.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmFTPConfig.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmFTPConfig.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmFTPConfig.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmFTPConfig.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmFTPConfig.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmFTPConfig.BtTestarClick(Sender: TObject);
const
  Avisa = True;
var
  Host, User, Pass: String;
  Passivo, Conectou: Boolean;
begin
  Host    := EdWeb_FTPh.ValueVariant;
  User    := EdWeb_FTPu.ValueVariant;
  Pass    := EdWeb_FTPs.ValueVariant;
  Passivo := CkPassivo.Checked;
  //
  if (Host = '') or (User = '') or (Pass = '') then
  begin
    Geral.MB_Aviso('Todos os campos devem ser preenchidos!');
    Exit;
  end;
  UFTP.TestaConexaoFTP(Self, Host, User, Pass, Passivo, Avisa);
end;

procedure TFmFTPConfig.BtAlteraClick(Sender: TObject);
begin
  if (QrFTPConfig.State <> dsInactive) and (QrFTPConfig.RecordCount > 0) then
    MostraEdicao(1, stUpd, 0);
end;

procedure TFmFTPConfig.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrFTPConfigCodigo.Value;
  Close;
end;

procedure TFmFTPConfig.BtConfirmaClick(Sender: TObject);
var
  Codigo: Integer;
  Crypt, Nome: String;
begin
  //Adicionar a vari�vel CO_RandStrWeb01 na internet caso exista
  Crypt := CO_RandStrWeb01;
  Nome  := EdNome.Text;
  //
  if MyObjects.FIC(Length(Nome) = 0, EdNome, 'Defina uma descri��o!') then Exit;
  //
  Codigo := UFTP.InsUpdFTPConfig(ImgTipo.SQLType, Dmod.QrUpdN, Dmod.MyDBn,
              QrFTPConfigCodigo.Value, Nome, EdWeb_FTPh.ValueVariant,
              EdWeb_FTPu.ValueVariant, EdWeb_FTPs.ValueVariant,
              EdWeb_Raiz.ValueVariant, EdWeb_MyURL.ValueVariant,
              Geral.BoolToInt(CkAtivo.Checked),
              Geral.BoolToInt(CkPassivo.Checked),
              Geral.BoolToInt(CkCfgPadrao.Checked));
  if Codigo = 0 then  
  begin
    Geral.MB_Aviso('Falha ao inserir dados!');
    Exit;
  end;
  if CkContinuar.Checked then
  begin
    Geral.MB_Info('Dados salvos com sucesso!');
    //
    ImgTipo.SQLType := stIns;
    //
    MostraEdicao(1, stIns, 0);
  end else
    MostraEdicao(0, stLok, 0);
  //
  LocCod(Codigo, Codigo);
end;

procedure TFmFTPConfig.BtDesisteClick(Sender: TObject);
begin
  MostraEdicao(0, stLok, 0);
end;

procedure TFmFTPConfig.BtIncluiClick(Sender: TObject);
begin
  MostraEdicao(1, stIns, 0);
end;

procedure TFmFTPConfig.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  GBEdita.Align   := alClient;
  GBDados.Align   := alClient;
  //
  CriaOForm;
end;

procedure TFmFTPConfig.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrFTPConfigCodigo.Value, LaRegistro.Caption);
end;

procedure TFmFTPConfig.SbExportaClick(Sender: TObject);
  procedure CamposToGrade(Item: Integer);
  begin
    Grade.Cells[00, Item] := Geral.FF0(Item);
    Grade.Cells[01, Item] := QrFTPConfigNome.Value;
    Grade.Cells[02, Item] := QrFTPConfigWeb_FTPh.Value;
    Grade.Cells[03, Item] := QrFTPConfigWeb_Raiz.Value;
    Grade.Cells[04, Item] := QrFTPConfigWeb_MyURL.Value;
    Grade.Cells[05, Item] := Geral.FF0(QrFTPConfigPassivo.Value);
    Grade.Cells[06, Item] := QrFTPConfigWeb_FTPu.Value;
    Grade.Cells[07, Item] := QrFTPConfigWeb_FTPsenha.Value;
  end;
  procedure SalvaStringGridToFile_PWD(Grade: TStringGrid; Arq: String);
  var
    F: TextFile;
    x, y, i: Integer;
    Senha, Nums: String;
  begin
    AssignFile(F, Arq);
    Rewrite(F);
    Writeln(F, Grade.ColCount);
    Writeln(F, Grade.RowCount);
    for x:=0 to Grade.ColCount-1 do
    begin
      for y:=0 to Grade.RowCount-1 do
      begin
        if x >= Grade.ColCount-1 then
        begin
          Senha := dmkPF.Criptografia(Grade.Cells[x,y], CO_RandStrWeb01);
          Nums := '';
          for i := 1 to Length(Senha) do
            Nums := Nums + FormatFloat('000', ord(Senha[i]));
          Writeln(F, Nums);
        end else
          Writeln(F, Grade.Cells[x,y]);
      end;
    end;
    CloseFile(F);
  end;
var
  Arquivo: String;
begin
  if (QrFTPConfig.State <> dsInactive) and (QrFTPConfig.RecordCount > 0) then
  begin
    if MyObjects.FileOpenDialog(Self, '', QrFTPConfigNome.Value, 'Abrir',
      '*.llm', [], Arquivo) then
    begin
      Grade.Cells[00, 00] := 'Item';
      Grade.Cells[01, 00] := 'Descri��o';
      Grade.Cells[02, 00] := 'Servidor';
      Grade.Cells[03, 00] := 'Pasta raiz';
      Grade.Cells[04, 00] := 'URL do site';
      Grade.Cells[05, 00] := 'Passivo';
      Grade.Cells[06, 00] := 'Usu�rio';
      Grade.Cells[07, 00] := 'Senha';
      //
      MyObjects.LimpaGrade(Grade, 1, 1, True);
      //
      CamposToGrade(1);
      //
      SalvaStringGridToFile_PWD(Grade, Arquivo + '.llm');
      //
      Geral.MB_Info('Arquivo salvo com sucesso!');
    end;
  end;
end;

procedure TFmFTPConfig.SbImportaClick(Sender: TObject);
var
  Nome, URL, Host, WebRaiz, Usuario, Senha: String;
  Codigo, Passivo: Integer;
begin
  UFTP.ImportaConfigFTP(Self, Nome, URL, Host, WebRaiz, Usuario, Senha, Passivo);
  //
  if (Nome <> '') and (URL <> '') and (Host <> '') and (Usuario <> '') and
    (Senha <> '') then
  begin
    if CO_DMKID_APP = 17 then //DControl
      Codigo := UFTP.InsUpdFTPConfig(stIns, Dmod.QrUpd, Dmod.MyDBn, 0, Nome,
                  Host, Usuario, Senha, WebRaiz, URL, 1, Passivo, 0)
    else
      Codigo := UFTP.InsUpdFTPConfig(stUpd, Dmod.QrUpdN, Dmod.MyDBn, 1, Nome,
                  Host, Usuario, Senha, WebRaiz, URL, 1, Passivo, 0);
    //
    if Codigo <> 0 then
    begin
      Geral.MB_Aviso('Importa��o realizada com sucesso!');
      LocCod(Codigo, Codigo);
    end else
      Geral.MB_Aviso('Falha ao importar dados!');
  end;
end;

procedure TFmFTPConfig.SbImprimeClick(Sender: TObject);
begin
  Geral.MB_Aviso('Impress�o n�o dispon�vel para esta janela!');
end;

procedure TFmFTPConfig.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmFTPConfig.SbNovoClick(Sender: TObject);
begin
  Geral.MB_Aviso('Pesquisa n�o dispon�vel para esta janela!');
end;

procedure TFmFTPConfig.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmFTPConfig.QrFTPConfigAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
  //
  if CO_DMKID_APP = 17 then //DControl
  begin
    BtInclui.Visible  := True;
    SbExporta.Visible := True;
  end else
  begin
    BtInclui.Visible  := False;
    SbExporta.Visible := False;
  end;
end;

procedure TFmFTPConfig.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  DmkWeb.CarregaImagemFormWEB(ImgWEB);
end;

procedure TFmFTPConfig.SbQueryClick(Sender: TObject);
begin
  LocCod(QrFTPConfigCodigo.Value, CuringaLoc.CriaForm(CO_CODIGO, CO_NOME,
    'ftpconfig', Dmod.MyDBn, CO_VAZIO));
end;

procedure TFmFTPConfig.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
    [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmFTPConfig.QrFTPConfigBeforeOpen(DataSet: TDataSet);
begin
  QrFTPConfigCodigo.DisplayFormat := FFormatFloat;
end;

end.
