object FmFTPWebArq2: TFmFTPWebArq2
  Left = 339
  Top = 185
  Caption = 'WEB-FTPAR-001 :: Arquivos WEB'
  ClientHeight = 774
  ClientWidth = 1057
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -14
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 120
  TextHeight = 16
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1057
    Height = 59
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 998
      Top = 0
      Width = 59
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 10
        Top = 14
        Width = 39
        Height = 39
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 59
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 59
      Top = 0
      Width = 939
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 9
        Top = 11
        Width = 212
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Arquivos WEB'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 11
        Top = 14
        Width = 212
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Arquivos WEB'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 10
        Top = 12
        Width = 212
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Arquivos WEB'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 59
    Width = 1057
    Height = 575
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 1057
      Height = 575
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 1057
        Height = 575
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        TabOrder = 0
        object PnConectar: TPanel
          Left = 2
          Top = 18
          Width = 1053
          Height = 60
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alTop
          ParentBackground = False
          TabOrder = 0
          object Label4: TLabel
            Left = 14
            Top = 7
            Width = 54
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Servidor:'
          end
          object SBFTPConfig: TSpeedButton
            Left = 825
            Top = 27
            Width = 25
            Height = 26
            Hint = 'Inclui item de carteira'
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = '...'
            OnClick = SBFTPConfigClick
          end
          object EdFTPConfig: TdmkEditCB
            Left = 14
            Top = 27
            Width = 68
            Height = 26
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            DBLookupComboBox = CBFTPConfig
            IgnoraDBLookupComboBox = False
          end
          object CBFTPConfig: TdmkDBLookupComboBox
            Left = 84
            Top = 27
            Width = 738
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            KeyField = 'Codigo'
            ListField = 'Nome'
            ListSource = DsFTPConfig
            TabOrder = 1
            dmkEditCB = EdFTPConfig
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
          object BtConectar: TBitBtn
            Tag = 561
            Left = 855
            Top = 4
            Width = 136
            Height = 49
            Cursor = crHandPoint
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = '&Conectar'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -15
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            NumGlyphs = 2
            ParentFont = False
            ParentShowHint = False
            ShowHint = True
            TabOrder = 2
            OnClick = BtConectarClick
          end
        end
        object PnDados: TPanel
          Left = 2
          Top = 78
          Width = 1053
          Height = 495
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 1
          object LvArquivos: TListView
            Left = 0
            Top = 109
            Width = 1053
            Height = 386
            Hint = 'Remote system files'
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alClient
            Columns = <
              item
                AutoSize = True
                Caption = 'Descri'#231#227'o'
              end
              item
                Alignment = taRightJustify
                Caption = 'Tamanho'
                Width = 100
              end
              item
                Caption = 'Modificado em'
                Width = 130
              end
              item
                Alignment = taRightJustify
                Caption = 'Itens'
                Width = 100
              end>
            ColumnClick = False
            MultiSelect = True
            ReadOnly = True
            RowSelect = True
            SortType = stData
            TabOrder = 0
            ViewStyle = vsReport
            ExplicitTop = 60
            ExplicitHeight = 435
          end
          object Panel5: TPanel
            Left = 0
            Top = 49
            Width = 1053
            Height = 60
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alTop
            ParentBackground = False
            TabOrder = 1
            ExplicitTop = 1
          end
          object Panel6: TPanel
            Left = 0
            Top = 0
            Width = 1053
            Height = 49
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alTop
            BevelOuter = bvNone
            TabOrder = 2
            ExplicitWidth = 1182
            object BtOk: TBitBtn
              Tag = 14
              Left = 1003
              Top = 0
              Width = 50
              Height = 49
              Cursor = crHandPoint
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alRight
              NumGlyphs = 2
              ParentShowHint = False
              ShowHint = True
              TabOrder = 0
            end
            object Panel7: TPanel
              Left = 0
              Top = 0
              Width = 1003
              Height = 49
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alClient
              BevelOuter = bvNone
              TabOrder = 1
              object EdCaminho: TdmkEdit
                Left = 0
                Top = 23
                Width = 1003
                Height = 26
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Align = alBottom
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -15
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
                TabOrder = 0
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = True
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
            end
          end
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 634
    Width = 1057
    Height = 54
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 18
      Width = 1053
      Height = 34
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 16
        Top = 2
        Width = 150
        Height = 19
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -17
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 15
        Top = 1
        Width = 150
        Height = 19
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -17
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 688
    Width = 1057
    Height = 86
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 878
      Top = 18
      Width = 177
      Height = 66
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 15
        Top = 5
        Width = 147
        Height = 49
        Cursor = crHandPoint
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 18
      Width = 876
      Height = 66
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 360
    Top = 315
  end
  object CSTabSheetChamou: TdmkCompoStore
    Left = 460
    Top = 443
  end
  object QrFTPConfig: TmySQLQuery
    Database = Dmod.MyDBn
    SQL.Strings = (
      'SELECT Codigo, Nome, AES_DECRYPT(Web_FTPs, :P0) SENHA, '
      'Web_MyURL, Web_FTPu, Web_FTPs, Web_FTPh, Web_Raiz, '
      'Passivo, Ativo'
      'FROM ftpconfig'
      'WHERE Ativo=1')
    Left = 548
    Top = 292
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrFTPConfigNome: TWideStringField
      FieldName = 'Nome'
      Origin = 'ftpconfig.Nome'
      Size = 50
    end
    object QrFTPConfigSENHA: TWideStringField
      FieldName = 'SENHA'
      Size = 50
    end
    object QrFTPConfigWeb_FTPu: TWideStringField
      FieldName = 'Web_FTPu'
      Origin = 'ftpconfig.Web_FTPu'
      Size = 50
    end
    object QrFTPConfigWeb_FTPs: TWideStringField
      FieldName = 'Web_FTPs'
      Origin = 'ftpconfig.Web_FTPs'
      Size = 50
    end
    object QrFTPConfigWeb_FTPh: TWideStringField
      FieldName = 'Web_FTPh'
      Origin = 'ftpconfig.Web_FTPh'
      Size = 255
    end
    object QrFTPConfigWeb_Raiz: TWideStringField
      FieldName = 'Web_Raiz'
      Origin = 'ftpconfig.Web_Raiz'
      Size = 50
    end
    object QrFTPConfigCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'ftpconfig.Codigo'
    end
    object QrFTPConfigAtivo: TSmallintField
      FieldName = 'Ativo'
      Origin = 'ftpconfig.Ativo'
    end
    object QrFTPConfigPassivo: TSmallintField
      FieldName = 'Passivo'
    end
    object QrFTPConfigWeb_MyURL: TWideStringField
      FieldName = 'Web_MyURL'
      Size = 255
    end
  end
  object DsFTPConfig: TDataSource
    DataSet = QrFTPConfig
    Left = 576
    Top = 292
  end
  object FTPGer: TIdFTP
    OnDisconnected = FTPGerDisconnected
    IPVersion = Id_IPv4
    TransferType = ftBinary
    NATKeepAlive.UseKeepAlive = False
    NATKeepAlive.IdleTimeMS = 0
    NATKeepAlive.IntervalMS = 0
    ProxySettings.ProxyType = fpcmNone
    ProxySettings.Port = 0
    OnAfterClientLogin = FTPGerAfterClientLogin
    Left = 232
    Top = 400
  end
  object ImageList1: TImageList
    Left = 576
    Top = 400
  end
end
