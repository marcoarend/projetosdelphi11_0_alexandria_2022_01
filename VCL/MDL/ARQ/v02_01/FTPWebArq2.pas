unit FTPWebArq2;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkPermissoes,
  dmkCompoStore, dmkDBLookupComboBox, dmkEditCB, mySQLDbTables, IdBaseComponent,
  IdComponent, IdTCPConnection, IdTCPClient, IdExplicitTLSClientServerBase,
  IdFTP, UnFTP2, Vcl.ImgList;

type
  TFmFTPWebArq2 = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    LaTitulo1C: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    CSTabSheetChamou: TdmkCompoStore;
    PnConectar: TPanel;
    Label4: TLabel;
    SBFTPConfig: TSpeedButton;
    EdFTPConfig: TdmkEditCB;
    CBFTPConfig: TdmkDBLookupComboBox;
    BtConectar: TBitBtn;
    QrFTPConfig: TmySQLQuery;
    QrFTPConfigNome: TWideStringField;
    QrFTPConfigSENHA: TWideStringField;
    QrFTPConfigWeb_FTPu: TWideStringField;
    QrFTPConfigWeb_FTPs: TWideStringField;
    QrFTPConfigWeb_FTPh: TWideStringField;
    QrFTPConfigWeb_Raiz: TWideStringField;
    QrFTPConfigCodigo: TIntegerField;
    QrFTPConfigAtivo: TSmallintField;
    QrFTPConfigPassivo: TSmallintField;
    QrFTPConfigWeb_MyURL: TWideStringField;
    DsFTPConfig: TDataSource;
    PnDados: TPanel;
    FTPGer: TIdFTP;
    LvArquivos: TListView;
    Panel5: TPanel;
    Panel6: TPanel;
    BtOk: TBitBtn;
    Panel7: TPanel;
    EdCaminho: TdmkEdit;
    ImageList1: TImageList;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure SBFTPConfigClick(Sender: TObject);
    procedure BtConectarClick(Sender: TObject);
    procedure FTPGerDisconnected(Sender: TObject);
    procedure FTPGerAfterClientLogin(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
  private
    { Private declarations }
    FFTP: TDmFTP;
    function  VerificaSeEstaConectado(): Boolean;
    procedure MostraEdicao(Mostra: Integer);
  public
    { Public declarations }
    FDB: TmySQLDatabase;
    FEmpresa: Integer;
  end;

  var
  FmFTPWebArq2: TFmFTPWebArq2;

implementation

uses MyGlyfs, Module, Principal, UnMyObjects, MyListas, UnFTPJan2, UMySQLModule;

{$R *.DFM}

procedure TFmFTPWebArq2.BtConectarClick(Sender: TObject);
var
  Conectado: Boolean;
begin
  if MyObjects.FIC(EdFTPConfig.ValueVariant = 0, EdFTPConfig, 'Servidor n�o definido!') then Exit;
  //
  FFTP := TDmFTP.Create;
  try
    Conectado := FFTP.EstaConectado(FTPGer);
    //
    if Conectado then
    begin
      FFTP.Desconecta(FTPGer);
    end else
    begin
      Screen.Cursor := crHourGlass;
      try
        MostraEdicao(0);
        //
        FFTP.ConectaServidor(QrFTPConfig, FTPGer);
      finally
        Screen.Cursor := crDefault;
      end;
    end;
  finally
    FFTP.Free;
  end;
end;

procedure TFmFTPWebArq2.BtSaidaClick(Sender: TObject);
begin
  if TFmFTPWebArq2(Self).Owner is TApplication then
    Close
  else
    MyObjects.FormTDIFecha(Self, TTabSheet(CSTabSheetChamou.Component));
end;

procedure TFmFTPWebArq2.FormActivate(Sender: TObject);
begin
  if TFmFTPWebArq2(Self).Owner is TApplication then
  begin
    MyObjects.CorIniComponente();
  end;
end;

procedure TFmFTPWebArq2.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  MostraEdicao(0);
end;

procedure TFmFTPWebArq2.FormDestroy(Sender: TObject);
begin
  FFTP.Free;
end;

procedure TFmFTPWebArq2.MostraEdicao(Mostra: Integer);
begin
  case Mostra of
    0:
    begin
      PnConectar.Visible := True;
      PnDados.Visible    := False;
      //
      EdFTPConfig.Enabled := True;
      CBFTPConfig.Enabled := True;
      //
      if CO_DMKID_APP = 17 then //DControl
        SBFTPConfig.Enabled := True
      else
        SBFTPConfig.Enabled := False;
      //
      BtConectar.Caption := '&Conectar';
      //
      FmMyGlyfs.ObtemBitmapDeGlyph(191, BtConectar);
      //
      MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
    end;
    1:
    begin
      PnConectar.Visible := True;
      PnDados.Visible    := True;
      //
      EdFTPConfig.Enabled := False;
      CBFTPConfig.Enabled := False;
      SBFTPConfig.Enabled := False;
      //
      BtConectar.Caption := '&Desconectar';
      //
      FmMyGlyfs.ObtemBitmapDeGlyph(189, BtConectar);
    end;
  end;
  Application.ProcessMessages;
end;

function TFmFTPWebArq2.VerificaSeEstaConectado(): Boolean;
begin
  try
    if not FTPGer.Connected then
    begin
      MostraEdicao(0);
      //
      Result := False;
    end else
      Result := True;
  except
    FTPGer.Disconnect;
    //
    MostraEdicao(0);
    //
    Result := False;
  end;
end;

procedure TFmFTPWebArq2.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
    [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmFTPWebArq2.FormShow(Sender: TObject);
begin
  FmMyGlyfs.DefineGlyfsTDI(FmPrincipal.sd1, Sender);
  //
  FFTP := TDmFTP.Create;
  FFTP.ReopenFTPConfig(QrFTPConfig, FDB, CO_DMKID_APP, FEmpresa);
end;

procedure TFmFTPWebArq2.FTPGerAfterClientLogin(Sender: TObject);
begin
  MostraEdicao(1);
end;

procedure TFmFTPWebArq2.FTPGerDisconnected(Sender: TObject);
begin
  MostraEdicao(0);
end;

procedure TFmFTPWebArq2.SBFTPConfigClick(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  //
  UFTPJan2.MostraFTPConfig(EdFTPConfig.ValueVariant);
  //
  if VAR_CADASTRO <> 0 then
  begin
    UMyMod.SetaCodUsuDeCodigo(EdFTPConfig, CBFTPConfig, QrFTPConfig, VAR_CADASTRO);
    //
    EdFTPConfig.SetFocus;
  end;
end;

end.
