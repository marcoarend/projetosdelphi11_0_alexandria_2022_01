unit UnFTP2;

interface

uses System.SysUtils, IdFTP, IdFTPCommon, mySQLDbTables;

type
  TDmFTP = class
  private
    FHost: String;
    FUser: String;
    FPass: String;
    FPassivo: Boolean;
    FProx_User: String;
    FProx_Pass: String;
    FProx_Serv: String;
    FProx_Enab: Integer;
    FProx_Port: Integer;
  private
    procedure ObtemDadosProxy();
    procedure ConfiguraDadosFTP(Qry: TmySQLQuery);
  public
    function  EstaConectado(FTPComp: TIdFTP): Boolean;
    function  ConectaServidor(Qry: TmySQLQuery; FTPComp: TIdFTP): Boolean;
    procedure ReopenFTPConfig(Qry: TmySQLQuery; DB: TmySQLDatabase;
              IDApp, Empresa: Integer; Codigo: Integer = 0);
    procedure Desconecta(FTPComp: TIdFTP);
  published
    constructor Create; virtual;
    property Host: String read FHost;
    property User: String read FUser;
    property Pass: String read FPass;
    property Passivo: Boolean read FPassivo;
  end;

implementation

uses dmkGeral, UnDmkProcFunc, UnGrl_Consts, DmkDAC_PF;

{ TDmFTP }

constructor TDmFTP.Create;
begin
  FHost    := '';
  FUser    := '';
  FPass    := '';
  FPassivo := False;
  //
  FProx_User := '';
  FProx_Pass := '';
  FProx_Serv := '';
  FProx_Enab := 0;
  FProx_Port := 0;
end;

procedure TDmFTP.ObtemDadosProxy();
var
  User, Pass, Serv: String;
  Port, Enab: Integer;
begin
  try
    User := Geral.ReadAppKeyCU('ProxyUser', 'Dermatek', ktString, '');
    Pass := Geral.ReadAppKeyCU('ProxyPass', 'Dermatek', ktString, '');
    Serv := Geral.ReadAppKeyCU('ProxyServ', 'Dermatek', ktString, '');
    Port := Geral.ReadAppKeyCU('ProxyPort', 'Dermatek', ktInteger, 0);
    Enab := Geral.ReadAppKeyCU('ProxyEnable', 'Dermatek', ktInteger, 0);
    //
    if User <> '' then
      User := dmkPF.PWDExDecode(User, CO_RandStrWeb01);
    if Pass <> '' then
      Pass := dmkPF.PWDExDecode(Pass, CO_RandStrWeb01);
  except
    User := '';
    Pass := '';
    Serv := '';
    Port := 0;
    Enab := 0;
  end;
  FProx_User := User;
  FProx_Pass := Pass;
  FProx_Serv := Serv;
  FProx_Enab := Enab;
  FProx_Port := Port;
end;

function TDmFTP.EstaConectado(FTPComp: TIdFTP): Boolean;
begin
  try
    if not FTPComp.Connected then
      Result := False
    else
      Result := True;
  except
    FTPComp.Disconnect;
    //
    Result := False;
  end;
end;

procedure TDmFTP.ReopenFTPConfig(Qry: TmySQLQuery; DB: TmySQLDatabase;
  IDApp, Empresa: Integer; Codigo: Integer = 0);
begin
  Qry.Close;
  //
  if IDApp <> 17 then //DControl
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, DB, [
      'SELECT Empresa Codigo, "Configura��o WEB" Nome, ',
      'AES_DECRYPT(Web_FTPs, "' + CO_RandStrWeb01 + '") SENHA, ',
      'Web_FTPu, Web_FTPs, Web_FTPh, Web_Raiz, Web_MyURL, ',
      'Web_FTPpassivo Passivo, Ativo ',
      'FROM web_params ',
      'WHERE Empresa=' + Geral.FF0(Empresa),
      '']);
  end else
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, DB, [
      'SELECT Codigo, Nome, ',
      'AES_DECRYPT(Web_FTPs, "' + CO_RandStrWeb01 + '") SENHA, ',
      'Web_FTPu, Web_FTPs, Web_FTPh, Web_Raiz, Web_MyURL, ',
      'Passivo, Ativo ',
      'FROM ftpconfig ',
      'WHERE Ativo = 1 ',
      '']);
    if Codigo > 0 then
      Qry.Locate('Codigo', Codigo, []);
  end;
end;

procedure TDmFTP.Desconecta(FTPComp: TIdFTP);
begin
  FTPComp.Disconnect;
end;

procedure TDmFTP.ConfiguraDadosFTP(Qry: TmySQLQuery);
begin
  FHost    := Qry.FieldByName('Web_FTPh').AsString;
  FUser    := Qry.FieldByName('Web_FTPu').AsString;
  FPass    := Qry.FieldByName('SENHA').AsString;
  FPassivo := Geral.IntToBool(Qry.FieldByName('Passivo').AsInteger);
end;

function TDmFTP.ConectaServidor(Qry: TmySQLQuery; FTPComp: TIdFTP): Boolean;
begin
  Result := False;
  try
    if FTPComp.Connected then
      FTPComp.Disconnect;
    //
    ObtemDadosProxy();
    ConfiguraDadosFTP(Qry);
    //
    if FProx_Enab = 0 then
    begin
      FTPComp.ProxySettings.Password := '';
      FTPComp.ProxySettings.Port     := 0;
      FTPComp.ProxySettings.Host     := '';
      FTPComp.ProxySettings.UserName := '';
    end else
    begin
      FTPComp.ProxySettings.Password := FProx_Pass;
      FTPComp.ProxySettings.Port     := FProx_Port;
      FTPComp.ProxySettings.Host     := FProx_Serv;
      FTPComp.ProxySettings.UserName := FProx_User;
    end;
    if (Host <> '') and (User <> '') and (Pass <> '') then
    begin
      FTPComp.Host         := Host;
      FTPComp.Username     := User;
      FTPComp.Password     := Pass;
      FTPComp.Passive      := Passivo;
      FTPComp.TransferType := ftBinary;
      FTPComp.UseHOST      := False;
      //
      FTPComp.Connect;
    end else
      Geral.MB_Aviso('Dados para conex�o com o servidor WEB n�o informados!');
    //
    Result := FTPComp.Connected;
  except
    on E : Exception do
    begin
      if (Trim(UpperCase(E.ClassName)) = UpperCase('EIdReplyRFCError')) and
        (Trim(UpperCase(E.Message)) = UpperCase('Login authentication failed')) then
      begin
        Geral.MB_Aviso('Falha ao conectar!' + sLineBreak +
          'Prov�vel motivo: Os dados de autentica��o est�o incorretos!');
      end;
    end;
  end;
end;

end.
