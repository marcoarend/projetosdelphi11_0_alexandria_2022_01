unit UnFTPJan2;

interface

uses mySQLDbTables, UMySQLModule, dmkGeral, ModuleGeral, dmkImage, Forms,
  Controls, Windows, SysUtils, ComCtrls, Grids, DBGrids, AdvToolBar,
  UnDmkProcFunc, ExtCtrls, dmkDBGrid, Math, UnDmkEnums;

type
  TUnFTPJan2 = class(TObject)
  private

  public
    procedure MostraFTPConfig(Codigo: Integer);
    procedure MostraFTPWebArq(DB: TmySQLDatabase; Empresa: Integer);
  end;
                        
var
  UFTPJan2: TUnFTPJan2;

implementation

uses MyListas, Module, DmkDAC_PF, UnMyObjects, MyDBCheck, FTPConfig, UnDmkWeb2,
  FTPWebArq2, UnInternalConsts;

{ TUnFTPJan2 }

procedure TUnFTPJan2.MostraFTPWebArq(DB: TmySQLDatabase; Empresa: Integer);
(*
var
  Msg: String;
*)
begin
(*
  if DmkWeb2.VerificaSeModuloWebEstaAtivo(CO_DMKID_APP,
    DModG.QrMasterHabilModulos.Value, Msg) then
  begin
    if DmkWeb2.ConexaoRemota(Dmod.MyDBn, DmodG.QrWebParams, 1) then
    begin
      if DBCheck.CriaFm(TFmFTPWebArq2, FmFTPWebArq2, afmoNegarComAviso) then
      begin
        FmFTPWebArq2.FDB := DB;
        FmFTPWebArq2.FEmpresa := Empresa;
        FmFTPWebArq2.ShowModal;
        FmFTPWebArq2.Destroy;
      end;
*)
      Application.CreateForm(TFmFTPWebArq2, FmFTPWebArq2);
      FmFTPWebArq2.FDB := DB;
      FmFTPWebArq2.FEmpresa := Empresa;
      FmFTPWebArq2.ShowModal;
      FmFTPWebArq2.Destroy;
(*
    end;
  end else
    Geral.MB_Aviso(Msg);
*)
end;

procedure TUnFTPJan2.MostraFTPConfig(Codigo: Integer);
var
  Msg: String;
begin
  if DmkWeb2.VerificaSeModuloWebEstaAtivo(CO_DMKID_APP,
    DModG.QrMasterHabilModulos.Value, Msg) then
  begin
    if DmkWeb2.ConexaoRemota(Dmod.MyDBn, DmodG.QrWebParams, 1) then
    begin
      if DBCheck.CriaFm(TFmFTPConfig, FmFTPConfig, afmoNegarComAviso) then
      begin
        if Codigo <> 0 then
          FmFTPConfig.LocCod(Codigo, Codigo);
        FmFTPConfig.ShowModal;
        FmFTPConfig.Destroy;
      end;
    end;
  end else
    Geral.MB_Aviso(Msg);
end;

end.

