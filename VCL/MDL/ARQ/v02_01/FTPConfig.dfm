object FmFTPConfig: TFmFTPConfig
  Left = 368
  Top = 194
  Caption = 'FTP-CONFI-001 :: Configura'#231#245'es FTP'
  ClientHeight = 481
  ClientWidth = 784
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnDados: TPanel
    Left = 0
    Top = 96
    Width = 784
    Height = 385
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    object GBDados: TGroupBox
      Left = 0
      Top = 0
      Width = 784
      Height = 300
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 1
      object Label1: TLabel
        Left = 16
        Top = 16
        Width = 14
        Height = 13
        Caption = 'ID:'
        FocusControl = DBEdCodigo
      end
      object Label2: TLabel
        Left = 16
        Top = 58
        Width = 31
        Height = 13
        Caption = 'Nome:'
      end
      object DBEdCodigo: TdmkDBEdit
        Left = 16
        Top = 32
        Width = 56
        Height = 21
        Hint = 'N'#186' do banco'
        TabStop = False
        DataField = 'Codigo'
        DataSource = DsFTPConfig
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 0
        UpdType = utYes
        Alignment = taRightJustify
      end
      object dmkDBEdit2: TdmkDBEdit
        Left = 16
        Top = 74
        Width = 424
        Height = 21
        DataField = 'Nome'
        DataSource = DsFTPConfig
        TabOrder = 1
        UpdType = utYes
        Alignment = taLeftJustify
      end
      object GroupBox1: TGroupBox
        Left = 16
        Top = 102
        Width = 424
        Height = 163
        Caption = 'Dados FTP'
        TabOrder = 2
        object Label4: TLabel
          Left = 8
          Top = 17
          Width = 42
          Height = 13
          Caption = 'Servidor:'
        end
        object Label6: TLabel
          Left = 8
          Top = 60
          Width = 39
          Height = 13
          Caption = 'Usu'#225'rio:'
        end
        object Label8: TLabel
          Left = 215
          Top = 17
          Width = 49
          Height = 13
          Caption = 'Pasta raiz:'
        end
        object Label9: TLabel
          Left = 8
          Top = 102
          Width = 44
          Height = 13
          Caption = 'URL site:'
        end
        object Label11: TLabel
          Left = 256
          Top = 142
          Width = 160
          Height = 13
          Caption = 'Exemplo: http://www.seusite.com'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = cl3DDkShadow
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object Label12: TLabel
          Left = 215
          Top = 60
          Width = 34
          Height = 13
          Caption = 'Senha:'
        end
        object dmkDBEdit3: TdmkDBEdit
          Left = 8
          Top = 36
          Width = 201
          Height = 21
          DataField = 'Web_FTPh'
          DataSource = DsFTPConfig
          TabOrder = 0
          UpdType = utYes
          Alignment = taLeftJustify
        end
        object dmkDBEdit4: TdmkDBEdit
          Left = 215
          Top = 36
          Width = 201
          Height = 21
          DataField = 'Web_Raiz'
          DataSource = DsFTPConfig
          TabOrder = 1
          UpdType = utYes
          Alignment = taLeftJustify
        end
        object dmkDBEdit5: TdmkDBEdit
          Left = 8
          Top = 76
          Width = 201
          Height = 21
          DataField = 'Web_FTPu'
          DataSource = DsFTPConfig
          TabOrder = 2
          UpdType = utYes
          Alignment = taLeftJustify
        end
        object dmkDBEdit6: TdmkDBEdit
          Left = 215
          Top = 76
          Width = 201
          Height = 20
          DataField = 'Web_FTPsenha'
          DataSource = DsFTPConfig
          Font.Charset = SYMBOL_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Wingdings'
          Font.Style = []
          ParentFont = False
          PasswordChar = '|'
          TabOrder = 3
          UpdType = utYes
          Alignment = taLeftJustify
        end
        object dmkDBEdit7: TdmkDBEdit
          Left = 8
          Top = 118
          Width = 408
          Height = 21
          DataField = 'Web_MyURL'
          DataSource = DsFTPConfig
          TabOrder = 4
          UpdType = utYes
          Alignment = taLeftJustify
        end
      end
      object DBCheckBox2: TDBCheckBox
        Left = 16
        Top = 270
        Width = 90
        Height = 17
        Caption = 'Modo passivo'
        DataField = 'Passivo'
        DataSource = DsFTPConfig
        TabOrder = 3
        ValueChecked = '1'
        ValueUnchecked = '0'
      end
      object CkAtivo: TDBCheckBox
        Left = 236
        Top = 271
        Width = 62
        Height = 17
        Caption = 'Ativo'
        DataField = 'Ativo'
        DataSource = DsFTPConfig
        TabOrder = 5
        ValueChecked = '1'
        ValueUnchecked = '0'
      end
      object Grade: TStringGrid
        Left = 690
        Top = 15
        Width = 92
        Height = 283
        Align = alRight
        ColCount = 8
        DefaultColWidth = 32
        DefaultRowHeight = 18
        TabOrder = 6
        Visible = False
      end
      object DBCheckBox1: TDBCheckBox
        Left = 110
        Top = 270
        Width = 120
        Height = 17
        Caption = 'Configura'#231#227'o padr'#227'o'
        DataField = 'CfgPadrao'
        DataSource = DsFTPConfig
        TabOrder = 4
        ValueChecked = '1'
        ValueUnchecked = '0'
      end
    end
    object GBCntrl: TGroupBox
      Left = 0
      Top = 321
      Width = 784
      Height = 64
      Align = alBottom
      TabOrder = 0
      object Panel5: TPanel
        Left = 2
        Top = 15
        Width = 172
        Height = 47
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 174
        Top = 15
        Width = 30
        Height = 17
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel3: TPanel
        Left = 261
        Top = 15
        Width = 521
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object BtExclui: TBitBtn
          Tag = 12
          Left = 188
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Exclui'
          Enabled = False
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
        end
        object BtAltera: TBitBtn
          Tag = 11
          Left = 96
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Altera'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtAlteraClick
        end
        object BtInclui: TBitBtn
          Tag = 10
          Left = 4
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Inclui'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtIncluiClick
        end
        object Panel2: TPanel
          Left = 388
          Top = 0
          Width = 133
          Height = 47
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 3
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
      end
    end
  end
  object PnEdita: TPanel
    Left = 0
    Top = 96
    Width = 784
    Height = 385
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    Visible = False
    object GBEdita: TGroupBox
      Left = 0
      Top = 0
      Width = 784
      Height = 305
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 0
      object Label7: TLabel
        Left = 16
        Top = 16
        Width = 14
        Height = 13
        Caption = 'ID:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label10: TLabel
        Left = 16
        Top = 58
        Width = 31
        Height = 13
        Caption = 'Nome:'
      end
      object EdCodigo: TdmkEdit
        Left = 16
        Top = 32
        Width = 56
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Codigo'
        UpdCampo = 'Codigo'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object CkPassivo: TCheckBox
        Left = 16
        Top = 273
        Width = 90
        Height = 17
        Caption = 'Modo Passivo'
        TabOrder = 3
      end
      object dmkCkAtivo: TdmkCheckBox
        Left = 242
        Top = 273
        Width = 48
        Height = 17
        Caption = 'Ativo'
        TabOrder = 5
        QryCampo = 'Ativo'
        UpdCampo = 'Ativo'
        UpdType = utYes
        ValCheck = '1'
        ValUncheck = '0'
        OldValor = #0
      end
      object CkContinuar: TCheckBox
        Left = 296
        Top = 273
        Width = 112
        Height = 17
        Caption = 'Continuar inserindo.'
        TabOrder = 6
      end
      object GroupBox3: TGroupBox
        Left = 16
        Top = 99
        Width = 424
        Height = 163
        Caption = 'Dados FTP'
        TabOrder = 2
        object Label14: TLabel
          Left = 8
          Top = 17
          Width = 42
          Height = 13
          Caption = 'Servidor:'
        end
        object Label15: TLabel
          Left = 8
          Top = 60
          Width = 39
          Height = 13
          Caption = 'Usu'#225'rio:'
        end
        object Label17: TLabel
          Left = 215
          Top = 17
          Width = 49
          Height = 13
          Caption = 'Pasta raiz:'
        end
        object Label5: TLabel
          Left = 8
          Top = 102
          Width = 44
          Height = 13
          Caption = 'URL site:'
        end
        object Label3: TLabel
          Left = 256
          Top = 142
          Width = 160
          Height = 13
          Caption = 'Exemplo: http://www.seusite.com'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = cl3DDkShadow
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object Label16: TLabel
          Left = 215
          Top = 60
          Width = 34
          Height = 13
          Caption = 'Senha:'
        end
        object EdWeb_Raiz: TdmkEdit
          Left = 215
          Top = 36
          Width = 201
          Height = 21
          TabOrder = 1
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = True
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          QryCampo = 'Web_Raiz'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object EdWeb_FTPu: TdmkEdit
          Left = 8
          Top = 76
          Width = 201
          Height = 21
          TabOrder = 2
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = True
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          QryCampo = 'Web_FTPu'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object EdWeb_FTPh: TdmkEdit
          Left = 8
          Top = 36
          Width = 201
          Height = 21
          TabOrder = 0
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = True
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          QryCampo = 'Web_FTPh'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object EdWeb_MyURL: TdmkEdit
          Left = 8
          Top = 119
          Width = 408
          Height = 21
          TabOrder = 4
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = True
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          QryCampo = 'Web_MyURL'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object EdWeb_FTPs: TdmkEdit
          Left = 215
          Top = 76
          Width = 201
          Height = 21
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 3
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = True
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          QryCampo = 'Web_FTPSenha'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
      end
      object EdNome: TdmkEdit
        Left = 16
        Top = 74
        Width = 424
        Height = 21
        TabOrder = 1
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Nome'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object CkCfgPadrao: TCheckBox
        Left = 112
        Top = 273
        Width = 125
        Height = 17
        Caption = 'Configura'#231#227'o padr'#227'o'
        TabOrder = 4
      end
    end
    object GBConfirma: TGroupBox
      Left = 0
      Top = 322
      Width = 784
      Height = 63
      Align = alBottom
      TabOrder = 1
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 12
        Top = 17
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object Panel1: TPanel
        Left = 644
        Top = 15
        Width = 138
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 2
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 7
          Top = 2
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
      object BtTestar: TBitBtn
        Tag = 553
        Left = 135
        Top = 17
        Width = 150
        Height = 40
        Cursor = crHandPoint
        Caption = '&Testar configura'#231#227'o'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        OnClick = BtTestarClick
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 784
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 300
      Height = 52
      Align = alLeft
      TabOrder = 0
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
        OnClick = SbImprimeClick
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
      object SbImporta: TBitBtn
        Tag = 39
        Left = 214
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 5
        OnClick = SbImportaClick
      end
      object SbExporta: TBitBtn
        Tag = 24
        Left = 256
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 6
        OnClick = SbExportaClick
      end
    end
    object GB_M: TGroupBox
      Left = 300
      Top = 0
      Width = 396
      Height = 52
      Align = alClient
      TabOrder = 1
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 233
        Height = 32
        Caption = 'Configura'#231#245'es FTP'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 233
        Height = 32
        Caption = 'Configura'#231#245'es FTP'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 233
        Height = 32
        Caption = 'Configura'#231#245'es FTP'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
    object GB_R: TGroupBox
      Left = 696
      Top = 0
      Width = 88
      Height = 52
      Align = alRight
      TabOrder = 2
      object ImgTipo: TdmkImage
        Left = 48
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
      object ImgWEB: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 52
    Width = 784
    Height = 44
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 780
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object QrFTPConfig: TmySQLQuery
    Database = Dmod.MyDBn
    BeforeOpen = QrFTPConfigBeforeOpen
    AfterOpen = QrFTPConfigAfterOpen
    SQL.Strings = (
      'SELECT *, '
      'AES_DECRYPT(Web_FTPs, "wkljweryhvbirt") Web_FTPsenha'
      'FROM ftpconfig')
    Left = 64
    Top = 64
    object QrFTPConfigWeb_FTPsenha: TWideStringField
      FieldName = 'Web_FTPsenha'
      Size = 50
    end
    object QrFTPConfigCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrFTPConfigNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
    object QrFTPConfigWeb_MyURL: TWideStringField
      FieldName = 'Web_MyURL'
      Size = 255
    end
    object QrFTPConfigWeb_FTPu: TWideStringField
      FieldName = 'Web_FTPu'
      Size = 50
    end
    object QrFTPConfigWeb_FTPs: TWideStringField
      FieldName = 'Web_FTPs'
      Size = 50
    end
    object QrFTPConfigWeb_FTPh: TWideStringField
      FieldName = 'Web_FTPh'
      Size = 255
    end
    object QrFTPConfigWeb_Raiz: TWideStringField
      FieldName = 'Web_Raiz'
      Size = 50
    end
    object QrFTPConfigLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrFTPConfigDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrFTPConfigDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrFTPConfigUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrFTPConfigUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrFTPConfigAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrFTPConfigAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrFTPConfigPassivo: TSmallintField
      FieldName = 'Passivo'
    end
    object QrFTPConfigCfgPadrao: TIntegerField
      FieldName = 'CfgPadrao'
    end
  end
  object DsFTPConfig: TDataSource
    DataSet = QrFTPConfig
    Left = 92
    Top = 64
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = BtInclui
    CanUpd01 = BtAltera
    CanDel01 = BtExclui
    Left = 120
    Top = 64
  end
end
