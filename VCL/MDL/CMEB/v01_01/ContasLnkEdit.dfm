object FmContasLnkEdit: TFmContasLnkEdit
  Left = 396
  Top = 181
  Caption = 'FIN-PLCTA-024 :: Link de Conta (Concilia'#231#227'o)'
  ClientHeight = 403
  ClientWidth = 688
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelDados: TPanel
    Left = 0
    Top = 48
    Width = 688
    Height = 241
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    ExplicitLeft = 232
    ExplicitTop = 20
    ExplicitHeight = 298
    object LaConta: TLabel
      Left = 12
      Top = 4
      Width = 131
      Height = 13
      Caption = 'Conta (do plano de contas):'
    end
    object Label2: TLabel
      Left = 348
      Top = 4
      Width = 44
      Height = 13
      Caption = 'Hist'#243'rico:'
    end
    object Label3: TLabel
      Left = 576
      Top = 4
      Width = 58
      Height = 13
      Caption = 'Documento:'
    end
    object Label4: TLabel
      Left = 12
      Top = 48
      Width = 127
      Height = 13
      Caption = 'Entidade: (zero para todas)'
    end
    object Label5: TLabel
      Left = 348
      Top = 48
      Width = 116
      Height = 13
      Caption = 'Banco: (zero para todos)'
    end
    object Label1: TLabel
      Left = 12
      Top = 92
      Width = 141
      Height = 13
      Caption = 'Fornecedor: (zero para v'#225'rios)'
    end
    object Label6: TLabel
      Left = 348
      Top = 92
      Width = 119
      Height = 13
      Caption = 'Cliente: (zero para v'#225'rios)'
    end
    object CBConta: TdmkDBLookupComboBox
      Left = 80
      Top = 20
      Width = 264
      Height = 21
      KeyField = 'Codigo'
      ListField = 'Nome'
      ListSource = DsContas
      TabOrder = 1
      dmkEditCB = EdConta
      UpdType = utYes
      LocF7SQLMasc = '$#'
    end
    object EdConta: TdmkEditCB
      Left = 12
      Top = 20
      Width = 65
      Height = 21
      Alignment = taRightJustify
      TabOrder = 0
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      DBLookupComboBox = CBConta
      IgnoraDBLookupComboBox = False
    end
    object EdTexto: TdmkEdit
      Left = 348
      Top = 20
      Width = 225
      Height = 21
      MaxLength = 50
      TabOrder = 2
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      ValWarn = False
    end
    object EdDoc: TdmkEdit
      Left = 576
      Top = 20
      Width = 101
      Height = 21
      MaxLength = 20
      TabOrder = 3
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      ValWarn = False
    end
    object EdCliInt: TdmkEditCB
      Left = 12
      Top = 64
      Width = 65
      Height = 21
      Alignment = taRightJustify
      TabOrder = 4
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      DBLookupComboBox = CBCliInt
      IgnoraDBLookupComboBox = False
    end
    object CBCliInt: TdmkDBLookupComboBox
      Left = 80
      Top = 64
      Width = 264
      Height = 21
      KeyField = 'Codigo'
      ListField = 'NOMEENTI'
      ListSource = DsCliInt
      TabOrder = 5
      dmkEditCB = EdCliInt
      UpdType = utYes
      LocF7SQLMasc = '$#'
    end
    object EdBanco: TdmkEditCB
      Left = 348
      Top = 64
      Width = 65
      Height = 21
      Alignment = taRightJustify
      TabOrder = 6
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      DBLookupComboBox = CBBanco
      IgnoraDBLookupComboBox = False
    end
    object CBBanco: TdmkDBLookupComboBox
      Left = 416
      Top = 64
      Width = 265
      Height = 21
      KeyField = 'Codigo'
      ListField = 'Nome'
      ListSource = DsBancos
      TabOrder = 7
      dmkEditCB = EdBanco
      UpdType = utYes
      LocF7SQLMasc = '$#'
    end
    object EdFornece: TdmkEditCB
      Left = 12
      Top = 108
      Width = 65
      Height = 21
      Alignment = taRightJustify
      TabOrder = 8
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      DBLookupComboBox = CBFornece
      IgnoraDBLookupComboBox = False
    end
    object CBFornece: TdmkDBLookupComboBox
      Left = 80
      Top = 108
      Width = 264
      Height = 21
      KeyField = 'Codigo'
      ListField = 'NOMEENTI'
      ListSource = DsFornece
      TabOrder = 9
      dmkEditCB = EdFornece
      UpdType = utYes
      LocF7SQLMasc = '$#'
    end
    object EdCliente: TdmkEditCB
      Left = 348
      Top = 108
      Width = 65
      Height = 21
      Alignment = taRightJustify
      TabOrder = 10
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      DBLookupComboBox = CBCliente
      IgnoraDBLookupComboBox = False
    end
    object CBCliente: TdmkDBLookupComboBox
      Left = 416
      Top = 108
      Width = 265
      Height = 21
      KeyField = 'Codigo'
      ListField = 'NOMEENTI'
      ListSource = DsCliente
      TabOrder = 11
      dmkEditCB = EdCliente
      UpdType = utYes
      LocF7SQLMasc = '$#'
    end
    object CkUsaEntBank: TdmkCheckBox
      Left = 12
      Top = 136
      Width = 669
      Height = 17
      Caption = 
        'Usar como fornecedor e/ou cliente, a entidade banc'#225'ria informada' +
        ' no cadastro do banco.'
      TabOrder = 12
      UpdType = utYes
      ValCheck = #0
      ValUncheck = #0
      OldValor = #0
    end
    object CGComposHist: TdmkCheckGroup
      Left = 12
      Top = 156
      Width = 669
      Height = 77
      Caption = 
        ' Composi'#231#227'o da descri'#231#227'o (Hist'#243'rico) para gera'#231#227'o do lan'#231'amento:' +
        ' '
      Items.Strings = (
        'Descri'#231#227'o do extrato'
        'Descri'#231#227'o da conta (Plano de contas)'
        'Nome do Terceiro (Forneceor / Cliente)')
      TabOrder = 13
      UpdType = utYes
      Value = 0
      OldValor = 0
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 688
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    ExplicitWidth = 617
    object GB_R: TGroupBox
      Left = 640
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      ExplicitLeft = 569
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 592
      Height = 48
      Align = alClient
      TabOrder = 2
      ExplicitWidth = 521
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 334
        Height = 32
        Caption = 'Link de Conta (Concilia'#231#227'o)'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 334
        Height = 32
        Caption = 'Link de Conta (Concilia'#231#227'o)'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 334
        Height = 32
        Caption = 'Link de Conta (Concilia'#231#227'o)'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 289
    Width = 688
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    ExplicitTop = 243
    ExplicitWidth = 617
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 684
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 613
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 333
    Width = 688
    Height = 70
    Align = alBottom
    TabOrder = 3
    ExplicitTop = 287
    ExplicitWidth = 617
    object PnSaiDesis: TPanel
      Left = 542
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      ExplicitLeft = 471
      object BtDesiste: TBitBtn
        Tag = 15
        Left = 14
        Top = 4
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Hint = 'Cancela inclus'#227'o / altera'#231#227'o'
        Caption = '&Desiste'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtDesisteClick
      end
    end
    object Panel2: TPanel
      Left = 2
      Top = 15
      Width = 540
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 469
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 14
        Top = 4
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Hint = 'Confima Inclus'#227'o / altera'#231#227'o'
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
    end
  end
  object DsContas: TDataSource
    DataSet = QrContas
    Left = 236
    Top = 58
  end
  object QrContas: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM contas'
      'ORDER BY Nome')
    Left = 208
    Top = 58
    object QrContasCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrContasNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object QrCliInt: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, CASE WHEN Tipo=0 THEN RazaoSocial '
      'ELSE Nome END NOMEENTI'
      'FROM entidades'
      'WHERE Codigo<10'
      'OR CliInt<>0'
      'ORDER BY NOMEENTI')
    Left = 208
    Top = 98
    object QrCliIntCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCliIntNOMEENTI: TWideStringField
      FieldName = 'NOMEENTI'
      Size = 100
    end
  end
  object DsCliInt: TDataSource
    DataSet = QrCliInt
    Left = 236
    Top = 98
  end
  object QrBancos: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM bancos'
      'ORDER BY Nome')
    Left = 476
    Top = 98
    object QrBancosCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrBancosNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
  object DsBancos: TDataSource
    DataSet = QrBancos
    Left = 504
    Top = 98
  end
  object QrFornece: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, CASE WHEN Tipo=0 THEN RazaoSocial '
      'ELSE Nome END NOMEENTI'
      'FROM entidades'
      'WHERE Fornece1="V"'
      'OR Fornece2="V"'
      'OR Fornece3="V"'
      'OR Fornece4="V"'
      'OR Fornece5="V"'
      'OR Fornece6="V"'
      'ORDER BY NOMEENTI')
    Left = 208
    Top = 146
    object QrForneceCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrForneceNOMEENTI: TWideStringField
      FieldName = 'NOMEENTI'
      Size = 100
    end
  end
  object DsFornece: TDataSource
    DataSet = QrFornece
    Left = 236
    Top = 146
  end
  object QrCliente: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, CASE WHEN Tipo=0 THEN RazaoSocial '
      'ELSE Nome END NOMEENTI'
      'FROM entidades'
      'WHERE Cliente1="V"'
      'OR Cliente2="V"'
      'OR Cliente3="V"'
      'OR Cliente4="V"'
      'ORDER BY NOMEENTI')
    Left = 476
    Top = 142
    object IntegerField1: TIntegerField
      FieldName = 'Codigo'
    end
    object StringField1: TWideStringField
      FieldName = 'NOMEENTI'
      Size = 100
    end
  end
  object DsCliente: TDataSource
    DataSet = QrCliente
    Left = 504
    Top = 142
  end
end
