unit ConciliaMultiCtas;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, DB, dmkImage,
  dmkGeral, UnDmkEnums;

type
  TFmConciliaMultiCtas = class(TForm)
    Panel1: TPanel;
    DBGrid1: TDBGrid;
    DsLocCta: TDataSource;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel3: TPanel;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    BtOK: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure DBGrid1DblClick(Sender: TObject);
  private
    { Private declarations }
    procedure Confirma();
  public
    { Public declarations }
    //FSelecionou: Boolean;
  end;

  var
  FmConciliaMultiCtas: TFmConciliaMultiCtas;

implementation

uses UnMyObjects, ModuleGeral, Concilia, UMySQLModule;

{$R *.DFM}

procedure TFmConciliaMultiCtas.BtOKClick(Sender: TObject);
begin
  Confirma();
end;

procedure TFmConciliaMultiCtas.Confirma();
var
  Perio: Integer;
  NOMETERCE: String;
begin
  //FSelecionou := True;
  //
  if FmConcilia.QrLocCtaMensal.Value <> 'V' then Perio := 0 else
    Perio := Geral.Periodo2000(FmConcilia.QrConcilia0DataM.Value);
  //
  if FmConcilia.QrLocCtaCliente.Value <> 0 then
    NOMETERCE := FmConcilia.QrLocCtaNOME_CLIENTE.Value
  else
  if FmConcilia.QrLocCtaFornece.Value <> 0 then
    NOMETERCE := FmConcilia.QrLocCtaNOME_FORNECE.Value
  else NOMETERCE := '';
  //
  DmodG.QrUpdPID1.SQL.Clear;
  DmodG.QrUpdPID1.SQL.Add('UPDATE ' + FmConcilia.FConcilia + ' SET ');
  DmodG.QrUpdPID1.SQL.Add('Conta=:P0, NOMECONTA=:P1, Perio=:P2, Forne=:P3, ');
  DmodG.QrUpdPID1.SQL.Add('Clien=:P4, CtaLk=:P5, NOMETERCE=:P6, Acao=1 ');
  DmodG.QrUpdPID1.SQL.Add('WHERE Linha=:Pa');
  DmodG.QrUpdPID1.Params[00].AsInteger := FmConcilia.QrLocCtaConta.Value;
  DmodG.QrUpdPID1.Params[01].AsString  := FmConcilia.QrLocCtaNome.Value;
  DmodG.QrUpdPID1.Params[02].AsInteger := Perio;
  DmodG.QrUpdPID1.Params[03].AsInteger := FmConcilia.QrLocCtaFornece.Value;
  DmodG.QrUpdPID1.Params[04].AsInteger := FmConcilia.QrLocCtaCliente.Value;
  DmodG.QrUpdPID1.Params[05].AsInteger := FmConcilia.QrLocCtaComposHist.Value;
  DmodG.QrUpdPID1.Params[06].AsString  := NOMETERCE;
  //
  DmodG.QrUpdPID1.Params[07].AsInteger := FmConcilia.QrConcilia0Linha.Value;
  //
  UMyMod.ExecutaQuery(DmodG.QrUpdPID1);
  //
  FmConcilia.ReopenConcilia(FmConcilia.QrConcilia0Linha.Value);
  //
  Close;
end;

procedure TFmConciliaMultiCtas.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmConciliaMultiCtas.DBGrid1DblClick(Sender: TObject);
begin
  Confirma();
end;

procedure TFmConciliaMultiCtas.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmConciliaMultiCtas.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  //FSelecionou := False;
end;

procedure TFmConciliaMultiCtas.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

end.
