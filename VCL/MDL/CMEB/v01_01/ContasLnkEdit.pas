unit ContasLnkEdit;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, UnInternalConsts, Buttons, DBCtrls, Db, (*DBTables,*)
  UnGOTOy, Mask, UMySQLModule, mySQLDbTables, dmkCheckGroup,
  dmkLabel, dmkEdit, dmkEditCB, dmkDBLookupComboBox, dmkCheckBox, dmkGeral,
  dmkImage, UnDmkEnums;

type
  TFmContasLnkEdit = class(TForm)
    PainelDados: TPanel;
    LaConta: TLabel;
    CBConta: TdmkDBLookupComboBox;
    DsContas: TDataSource;
    EdConta: TdmkEditCB;
    QrContas: TmySQLQuery;
    QrContasCodigo: TIntegerField;
    QrContasNome: TWideStringField;
    EdTexto: TdmkEdit;
    Label2: TLabel;
    Label3: TLabel;
    EdDoc: TdmkEdit;
    Label4: TLabel;
    EdCliInt: TdmkEditCB;
    CBCliInt: TdmkDBLookupComboBox;
    EdBanco: TdmkEditCB;
    CBBanco: TdmkDBLookupComboBox;
    Label5: TLabel;
    QrCliInt: TmySQLQuery;
    DsCliInt: TDataSource;
    QrCliIntCodigo: TIntegerField;
    QrCliIntNOMEENTI: TWideStringField;
    QrBancos: TmySQLQuery;
    DsBancos: TDataSource;
    QrBancosCodigo: TIntegerField;
    QrBancosNome: TWideStringField;
    Label1: TLabel;
    EdFornece: TdmkEditCB;
    CBFornece: TdmkDBLookupComboBox;
    Label6: TLabel;
    EdCliente: TdmkEditCB;
    CBCliente: TdmkDBLookupComboBox;
    QrFornece: TmySQLQuery;
    DsFornece: TDataSource;
    QrCliente: TmySQLQuery;
    IntegerField1: TIntegerField;
    StringField1: TWideStringField;
    DsCliente: TDataSource;
    QrForneceCodigo: TIntegerField;
    QrForneceNOMEENTI: TWideStringField;
    CkUsaEntBank: TdmkCheckBox;
    CGComposHist: TdmkCheckGroup;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    Panel2: TPanel;
    BtConfirma: TBitBtn;
    BtDesiste: TBitBtn;
    procedure BtDesisteClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormResize(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    FConta, FControle: Integer;
  end;

var
  FmContasLnkEdit: TFmContasLnkEdit;

implementation

uses UnMyObjects, Module, ContasLnk, DmkDAC_PF;

{$R *.DFM}

procedure TFmContasLnkEdit.BtDesisteClick(Sender: TObject);
begin
  VAR_CONTA := 0;
  Close;
end;

procedure TFmContasLnkEdit.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmContasLnkEdit.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  UnDmkDAC_PF.AbreQuery(QrContas, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrBancos, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrCliInt, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrCliente, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrFornece, Dmod.MyDB);
end;

procedure TFmContasLnkEdit.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmContasLnkEdit.BtConfirmaClick(Sender: TObject);
var
  Texto, Doc: String;
  CliInt, Banco, Codigo, Considera, Cliente, Fornece, UsaEntBank, ComposHist,
  Controle: Integer;
begin
  VAR_CONTA := Geral.IMV(EdConta.Text);
  if (VAR_CONTA = 0) and (EdConta.Enabled) then
  begin
    Geral.MB_Aviso('Informe uma Conta!');
    EdConta.SetFocus;
    Exit;
  end;
  {
  Dmod.QrUpd.SQL.Clear;
  if ImgTipo.Caption = stIns then
  begin
    Dmod.QrUpd.SQL.Add('INSERT INTO contaslnk SET ');
    Controle := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
        'ContasLnk', 'ContasLnk', 'Controle');
  end else begin
    Dmod.QrUpd.SQL.Add('UPDATE contaslnk SET ');
    Controle := FControle;
  end;
  Dmod.QrUpd.SQL.Add('Texto=:P0, Doc=:P1, CliInt=:P2, Banco=:P3, ');
  Dmod.QrUpd.SQL.Add('Codigo=:P4, Considera=:P5, Cliente=:P6, Fornece=:P7, ');
  Dmod.QrUpd.SQL.Add('UsaEntBank=:P8, ComposHist=:P9, ');
  if ImgTipo.Caption = stIns then
    Dmod.QrUpd.SQL.Add('DataCad=:Pa, UserCad=:Pb, Controle=:Pc')
  else begin
    Dmod.QrUpd.SQL.Add('DataAlt=:Pa, UserAlt=:Pb WHERE Controle=:Pc ');
    Dmod.QrUpd.SQL.Add('AND Codigo=:Pd');
  end;
  Dmod.QrUpd.Params[00].AsString  := EdTexto.Text;
  Dmod.QrUpd.Params[01].AsString  := EdDoc.Text;
  Dmod.QrUpd.Params[02].AsInteger := Geral.IMV(EdCliInt.Text);
  Dmod.QrUpd.Params[03].AsInteger := Geral.IMV(EdBanco.Text);
  Dmod.QrUpd.Params[04].AsInteger := VAR_CONTA;
  Dmod.QrUpd.Params[05].AsInteger := Geral.BoolToInt(EdConta.Enabled);
  Dmod.QrUpd.Params[06].AsInteger := Geral.IMV(EdCliente.Text);
  Dmod.QrUpd.Params[07].AsInteger := Geral.IMV(EdFornece.Text);
  Dmod.QrUpd.Params[08].AsInteger := Geral.BoolToInt(CkUsaEntBank.Checked);
  Dmod.QrUpd.Params[09].AsInteger := CGComposHist.Value;
  //
  Dmod.QrUpd.Params[10].AsString  := Geral.FDT(Date, 1);
  Dmod.QrUpd.Params[11].AsInteger := VAR_USUARIO;
  Dmod.QrUpd.Params[12].AsInteger := Controle;
  if ImgTipo.Caption = stUpd then
    Dmod.QrUpd.Params[13].AsInteger := FConta;
  Dmod.QrUpd.ExecSQL;
  }
  Texto      := EdTexto.Text;
  Doc        := EdDoc.Text;
  CliInt     := Geral.IMV(EdCliInt.Text);
  Banco      := Geral.IMV(EdBanco.Text);
  Codigo     := VAR_CONTA;
  Considera  := Geral.BoolToInt(EdConta.Enabled);
  Cliente    := Geral.IMV(EdCliente.Text);
  Fornece    := Geral.IMV(EdFornece.Text);
  UsaEntBank := Geral.BoolToInt(CkUsaEntBank.Checked);
  ComposHist := CGComposHist.Value;
  //
  Controle := UMyMod.BuscaEmLivreY_Def('contaslnk', 'Controle', ImgTipo.SQLType, FControle);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'contaslnk', False, [
    'Codigo', 'CliInt', 'Banco',
    'Texto', 'Doc', 'Considera',
    'Cliente', 'Fornece', 'UsaEntBank',
    'ComposHist'
  ], ['Controle'], [
    Codigo, CliInt, Banco,
    Texto, Doc, Considera,
    Cliente, Fornece, UsaEntBank,
    ComposHist
  ], [Controle], True) then
  begin
    try
      if FindWindow('TFmContasLnk', nil) > 0 then
        FmContasLnk.LocCod(FmContasLnk.QrContasLnkControle.Value, Controle);
    finally
      Close;
    end;
  end;
end;

end.
