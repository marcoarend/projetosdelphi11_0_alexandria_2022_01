object FmConcilia: TFmConcilia
  Left = 345
  Top = 171
  Caption = 'FIN-CONCI-003 :: Concilia'#231#227'o de Arquivos Banc'#225'rios'
  ClientHeight = 610
  ClientWidth = 898
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnImporta: TPanel
    Left = 197
    Top = 47
    Width = 158
    Height = 523
    TabOrder = 0
    object PB1: TProgressBar
      Left = 1
      Top = 1
      Width = 156
      Height = 17
      Align = alTop
      TabOrder = 0
    end
    object Grade: TStringGrid
      Left = 1
      Top = 18
      Width = 156
      Height = 457
      Align = alClient
      ColCount = 7
      DefaultColWidth = 51
      DefaultRowHeight = 14
      RowCount = 2
      Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goColMoving, goEditing]
      TabOrder = 1
      OnDrawCell = GradeDrawCell
      ColWidths = (
        51
        102
        83
        91
        46
        51
        51)
      RowHeights = (
        14
        13)
    end
    object Panel2: TPanel
      Left = 1
      Top = 475
      Width = 156
      Height = 47
      Align = alBottom
      TabOrder = 2
      object BtImporta: TBitBtn
        Tag = 19
        Left = 9
        Top = 4
        Width = 89
        Height = 39
        Caption = 'Im&porta'
        TabOrder = 0
        OnClick = BtImportaClick
      end
      object BtSalva: TBitBtn
        Tag = 24
        Left = 103
        Top = 4
        Width = 89
        Height = 39
        Caption = 'Sal&va'
        Enabled = False
        TabOrder = 1
        OnClick = BtSalvaClick
      end
      object BtVerSalvos: TBitBtn
        Tag = 10009
        Left = 289
        Top = 4
        Width = 117
        Height = 39
        Caption = '&Ver salvos'
        TabOrder = 2
        OnClick = BtVerSalvosClick
      end
      object TPanel
        Left = 52
        Top = 1
        Width = 103
        Height = 45
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 3
        object BitBtn2: TBitBtn
          Tag = 13
          Left = 5
          Top = 3
          Width = 89
          Height = 39
          Caption = '&Sair'
          TabOrder = 0
          OnClick = BitBtn2Click
        end
      end
    end
  end
  object PnConcilia: TPanel
    Left = 0
    Top = 47
    Width = 898
    Height = 563
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    Visible = False
    object Splitter1: TSplitter
      Left = 0
      Top = 314
      Width = 898
      Height = 5
      Cursor = crVSplit
      Align = alBottom
    end
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 898
      Height = 51
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 1
      object Label1: TLabel
        Left = 12
        Top = 4
        Width = 73
        Height = 13
        Caption = 'Conta corrente:'
      end
      object Label2: TLabel
        Left = 86
        Top = 4
        Width = 429
        Height = 13
        Caption = 
          '(a conta corrente deve ter obrigatoriamente o c'#243'digo do banco in' +
          'formado em seu cadastro)'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
      end
      object Label3: TLabel
        Left = 516
        Top = 4
        Width = 34
        Height = 13
        Caption = 'Banco:'
        FocusControl = DBEdit1
      end
      object Label4: TLabel
        Left = 555
        Top = 4
        Width = 45
        Height = 13
        Caption = 'Entidade:'
        FocusControl = DBEdit2
      end
      object EdCarteira: TdmkEditCB
        Left = 12
        Top = 20
        Width = 64
        Height = 20
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = EdCarteiraChange
        DBLookupComboBox = CBCarteira
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBCarteira: TdmkDBLookupComboBox
        Left = 78
        Top = 20
        Width = 435
        Height = 21
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsCarteiras
        TabOrder = 1
        dmkEditCB = EdCarteira
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object DBEdit1: TDBEdit
        Left = 516
        Top = 20
        Width = 36
        Height = 21
        DataField = 'Banco1'
        DataSource = DsCarteiras
        Enabled = False
        TabOrder = 2
      end
      object DBEdit2: TDBEdit
        Left = 555
        Top = 20
        Width = 45
        Height = 21
        DataField = 'EntiBank'
        DataSource = DsCarteiras
        Enabled = False
        TabOrder = 3
      end
    end
    object DBGrid1: TDBGrid
      Left = 0
      Top = 51
      Width = 898
      Height = 263
      Align = alClient
      DataSource = DsConcilia0
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      OnDrawColumnCell = DBGrid1DrawColumnCell
      OnDblClick = DBGrid1DblClick
      Columns = <
        item
          Expanded = False
          FieldName = 'Linha'
          Title.Caption = 'Seq.'
          Width = 26
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DataM'
          Title.Caption = 'Data'
          Width = 45
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Texto'
          Title.Caption = 'Hist'#243'rico'
          Width = 144
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Docum'
          Title.Caption = 'Documento'
          Width = 61
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Valor'
          Width = 59
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Saldo'
          Width = 59
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMEACAO'
          Title.Caption = 'A'#231#227'o'
          Width = 65
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Periodo_TXT'
          Title.Caption = 'Per'#237'odo'
          Width = 38
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMECONTA'
          Title.Caption = 'Conta (Plano de contas)'
          Width = 128
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMEDEPTO'
          Title.Caption = 'Depto'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMETERCE'
          Title.Caption = 'Cliente / Fornecedor'
          Width = 128
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DifEr'
          Title.Caption = 'Diferen'#231'a'
          Width = 45
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CtrlE'
          Title.Caption = 'Existe'
          Width = 38
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'EXPLICACAO'
          Title.Caption = 'Explica'#231#227'o'
          Width = 256
          Visible = True
        end>
    end
    object DBGrid2: TDBGrid
      Left = 0
      Top = 319
      Width = 898
      Height = 132
      Align = alBottom
      DataSource = DsConcilia1
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
      TabOrder = 2
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'Linha'
          Title.Caption = 'Seq.'
          Width = 26
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DataM'
          Title.Caption = 'Data'
          Width = 45
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Texto'
          Title.Caption = 'Hist'#243'rico'
          Width = 144
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Docum'
          Title.Caption = 'Documento'
          Width = 61
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Valor'
          Width = 59
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Saldo'
          Width = 59
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMEACAO'
          Title.Caption = 'A'#231#227'o'
          Width = 59
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Periodo_TXT'
          Title.Caption = 'Per'#237'odo'
          Width = 38
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMECONTA'
          Title.Caption = 'Conta (Plano de contas)'
          Width = 128
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMEDEPTO'
          Title.Caption = 'Depto'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMETERCE'
          Title.Caption = 'Cliente / Fornecedor'
          Width = 128
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DifEr'
          Title.Caption = 'Diferen'#231'a'
          Width = 45
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CtrlE'
          Title.Caption = 'Existe'
          Width = 38
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'EXPLICACAO'
          Title.Caption = 'Explica'#231#227'o'
          Width = 256
          Visible = True
        end>
    end
    object GBAvisos1: TGroupBox
      Left = 0
      Top = 451
      Width = 898
      Height = 43
      Align = alBottom
      Caption = ' Avisos: '
      TabOrder = 3
      object Panel4: TPanel
        Left = 2
        Top = 15
        Width = 894
        Height = 26
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object LaAviso1: TLabel
          Left = 13
          Top = 2
          Width = 120
          Height = 16
          Caption = '..............................'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clSilver
          Font.Height = -14
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          Transparent = True
        end
        object LaAviso2: TLabel
          Left = 12
          Top = 1
          Width = 120
          Height = 16
          Caption = '..............................'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clRed
          Font.Height = -14
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          Transparent = True
        end
      end
    end
    object GBRodaPe: TGroupBox
      Left = 0
      Top = 494
      Width = 898
      Height = 69
      Align = alBottom
      TabOrder = 4
      object Panel1: TPanel
        Left = 2
        Top = 15
        Width = 894
        Height = 52
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object Panel5: TPanel
          Left = 686
          Top = 0
          Width = 208
          Height = 52
          Align = alRight
          BevelOuter = bvNone
          TabOrder = 0
          object BitBtn3: TBitBtn
            Tag = 13
            Left = 107
            Top = 3
            Width = 89
            Height = 39
            Caption = '&Sair'
            TabOrder = 1
            OnClick = BitBtn2Click
          end
          object BitBtn5: TBitBtn
            Tag = 10013
            Left = 14
            Top = 3
            Width = 89
            Height = 39
            Caption = '&Voltar'
            TabOrder = 0
            OnClick = BitBtn5Click
          end
        end
        object BtLinkar: TBitBtn
          Tag = 311
          Left = 9
          Top = 3
          Width = 89
          Height = 39
          Caption = '&Linkar'
          Enabled = False
          TabOrder = 1
          OnClick = BtLinkarClick
        end
        object BtCadastro: TBitBtn
          Tag = 10010
          Left = 103
          Top = 3
          Width = 89
          Height = 39
          Caption = 'Ca&dastro'
          Enabled = False
          TabOrder = 2
          OnClick = BtCadastroClick
        end
        object BtAcao: TBitBtn
          Tag = 10010
          Left = 198
          Top = 3
          Width = 88
          Height = 39
          Caption = '&A'#231#227'o'
          Enabled = False
          TabOrder = 3
          OnClick = BtAcaoClick
        end
        object BitBtn1: TBitBtn
          Tag = 3
          Left = 293
          Top = 3
          Width = 88
          Height = 39
          Caption = '&Pr'#243'x. Indef.'
          TabOrder = 4
          OnClick = BitBtn1Click
        end
        object BitBtn4: TBitBtn
          Tag = 5
          Left = 387
          Top = 3
          Width = 88
          Height = 39
          Caption = '&Imprime'
          TabOrder = 5
          OnClick = BitBtn4Click
        end
        object BtConciliar: TBitBtn
          Tag = 10011
          Left = 482
          Top = 3
          Width = 88
          Height = 39
          Caption = '&Conciliar'
          Enabled = False
          TabOrder = 6
          OnClick = BtConciliarClick
        end
        object BitBtn6: TBitBtn
          Tag = 10011
          Left = 576
          Top = 3
          Width = 89
          Height = 39
          Caption = 'Pes&quisar'
          TabOrder = 7
          OnClick = BitBtn6Click
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 898
    Height = 47
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 850
      Top = 0
      Width = 48
      Height = 47
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 31
        Height = 31
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 47
      Height = 47
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 47
      Top = 0
      Width = 803
      Height = 47
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 397
        Height = 31
        Caption = 'Concilia'#231#227'o de Arquivos Banc'#225'rios'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -26
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 397
        Height = 31
        Caption = 'Concilia'#231#227'o de Arquivos Banc'#225'rios'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -26
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 397
        Height = 31
        Caption = 'Concilia'#231#227'o de Arquivos Banc'#225'rios'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -26
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object OpenDialog3: TOpenDialog
    DefaultExt = '*.xls'
    Filter = 'Arquivos XLS|*.xls'
    Options = [ofEnableSizing]
    Left = 460
    Top = 200
  end
  object QrConcilia0: TMySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrConcilia0AfterOpen
    BeforeClose = QrConcilia0BeforeClose
    AfterScroll = QrConcilia0AfterScroll
    OnCalcFields = QrConcilia0CalcFields
    SQL.Strings = (
      'SELECT cnc.* '
      'FROM concilia cnc'
      'WHERE cnc.Nivel=0'
      'ORDER BY cnc.Linha')
    Left = 740
    Top = 9
    object QrConcilia0Linha: TIntegerField
      FieldName = 'Linha'
      Origin = 'concilia.Linha'
    end
    object QrConcilia0DataM: TDateField
      FieldName = 'DataM'
      Origin = 'concilia.DataM'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrConcilia0Texto: TWideStringField
      FieldName = 'Texto'
      Origin = 'concilia.Texto'
      Size = 50
    end
    object QrConcilia0Valor: TFloatField
      FieldName = 'Valor'
      Origin = 'concilia.Valor'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrConcilia0Docum: TWideStringField
      FieldName = 'Docum'
      Origin = 'concilia.Docum'
    end
    object QrConcilia0Saldo: TFloatField
      FieldName = 'Saldo'
      Origin = 'concilia.Saldo'
      DisplayFormat = '#,###,##0.00'
    end
    object QrConcilia0Conta: TIntegerField
      FieldName = 'Conta'
      Origin = 'concilia.Conta'
      DisplayFormat = '0;-0; '
    end
    object QrConcilia0Acao: TIntegerField
      FieldName = 'Acao'
      Origin = 'concilia.Acao'
    end
    object QrConcilia0NOMEACAO: TWideStringField
      DisplayWidth = 11
      FieldKind = fkCalculated
      FieldName = 'NOMEACAO'
      Size = 11
      Calculated = True
    end
    object QrConcilia0EXPLICACAO: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'EXPLICACAO'
      Size = 100
      Calculated = True
    end
    object QrConcilia0DifEr: TFloatField
      FieldName = 'DifEr'
      Origin = 'concilia.DifEr'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrConcilia0Clien: TIntegerField
      FieldName = 'Clien'
      Origin = 'concilia.Clien'
    end
    object QrConcilia0Forne: TIntegerField
      FieldName = 'Forne'
      Origin = 'concilia.Forne'
    end
    object QrConcilia0Perio: TIntegerField
      FieldName = 'Perio'
    end
    object QrConcilia0Periodo_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'Periodo_TXT'
      Size = 7
      Calculated = True
    end
    object QrConcilia0SERIE: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'SERIE'
      Size = 30
      Calculated = True
    end
    object QrConcilia0CtrlE: TIntegerField
      FieldName = 'CtrlE'
      DisplayFormat = '0;-0; '
    end
    object QrConcilia0CartT: TIntegerField
      FieldName = 'CartT'
    end
    object QrConcilia0CtaLk: TIntegerField
      FieldName = 'CtaLk'
    end
    object QrConcilia0CHEQUE: TFloatField
      FieldKind = fkCalculated
      FieldName = 'CHEQUE'
      Calculated = True
    end
    object QrConcilia0Depto: TIntegerField
      FieldName = 'Depto'
    end
    object QrConcilia0NOMEDEPTO: TWideStringField
      FieldName = 'NOMEDEPTO'
      Size = 100
    end
    object QrConcilia0NOMECONTA: TWideStringField
      FieldName = 'NOMECONTA'
      Size = 100
    end
    object QrConcilia0NOMETERCE: TWideStringField
      FieldName = 'NOMETERCE'
      Size = 100
    end
    object QrConcilia0NOMECARTE: TWideStringField
      FieldName = 'NOMECARTE'
      Size = 100
    end
    object QrConcilia0TIPOCARTE: TIntegerField
      FieldName = 'TIPOCARTE'
    end
    object QrConcilia0Nivel: TIntegerField
      FieldName = 'Nivel'
    end
    object QrConcilia0LinIt: TIntegerField
      FieldName = 'LinIt'
    end
  end
  object DsConcilia0: TDataSource
    DataSet = QrConcilia0
    Left = 768
    Top = 9
  end
  object QrCarteiras: TMySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrCarteirasAfterOpen
    SQL.Strings = (
      'SELECT IF(en.Tipo=0, en.RazaoSocial, en.Nome) NOMEBCO, '
      'car.Codigo, car.Nome, car.Banco1, ban.Entidade EntiBank,'
      'car.Tipo, car.ForneceI'
      'FROM carteiras car'
      'LEFT JOIN bancos ban ON ban.Codigo=car.Banco1'
      'LEFT JOIN entidades en ON en.Codigo=ban.Entidade'
      'WHERE car.Tipo=1'
      'AND car.Banco1 > 0'
      'AND ban.Codigo>0'
      'AND car.ForneceI=:P0'
      'ORDER BY car.Nome')
    Left = 720
    Top = 309
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrCarteirasCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCarteirasNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QrCarteirasBanco1: TIntegerField
      FieldName = 'Banco1'
    end
    object QrCarteirasEntiBank: TIntegerField
      FieldName = 'EntiBank'
    end
    object QrCarteirasTipo: TIntegerField
      FieldName = 'Tipo'
      Required = True
    end
    object QrCarteirasForneceI: TIntegerField
      FieldName = 'ForneceI'
      Required = True
    end
    object QrCarteirasNOMEBCO: TWideStringField
      FieldName = 'NOMEBCO'
      Size = 100
    end
  end
  object DsCarteiras: TDataSource
    DataSet = QrCarteiras
    Left = 748
    Top = 309
  end
  object PMLinkar: TPopupMenu
    OnPopup = PMLinkarPopup
    Left = 36
    Top = 453
    object Fazervarreduraparaconsolidaoporlink1: TMenuItem
      Caption = '&Fazer varredura para consolida'#231#227'o por link'
      OnClick = Fazervarreduraparaconsolidaoporlink1Click
    end
    object N2: TMenuItem
      Caption = '-'
    end
    object Informaaconta1: TMenuItem
      Caption = 'Informar a &Conta (ser'#225'  criado um lan'#231'amento)'
      OnClick = Informaaconta1Click
    end
    object Selecionacontaadequada2: TMenuItem
      Caption = 
        '&Seleciona conta adequada (quando houver multiplas contas cadast' +
        'radas)'
      Enabled = False
      OnClick = Selecionacontaadequada2Click
    end
    object N5: TMenuItem
      Caption = '-'
    end
    object Transferncia1: TMenuItem
      Caption = '&Transfer'#234'ncia entre carteiras'
      OnClick = Transferncia1Click
    end
  end
  object QrLocLanct0: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      ''
      '')
    Left = 312
    Top = 385
    object QrLocLanct0Documento: TFloatField
      FieldName = 'Documento'
    end
    object QrLocLanct0Debito: TFloatField
      FieldName = 'Debito'
    end
    object QrLocLanct0Controle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrLocLanct0Sub: TSmallintField
      FieldName = 'Sub'
      Required = True
    end
    object QrLocLanct0Credito: TFloatField
      FieldName = 'Credito'
    end
    object QrLocLanct0Carteira: TIntegerField
      FieldName = 'Carteira'
      Required = True
    end
    object QrLocLanct0Descricao: TWideStringField
      FieldName = 'Descricao'
      Size = 100
    end
    object QrLocLanct0NotaFiscal: TIntegerField
      FieldName = 'NotaFiscal'
    end
    object QrLocLanct0Cliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrLocLanct0Fornecedor: TIntegerField
      FieldName = 'Fornecedor'
    end
    object QrLocLanct0Account: TIntegerField
      FieldName = 'Account'
    end
    object QrLocLanct0Vendedor: TIntegerField
      FieldName = 'Vendedor'
    end
    object QrLocLanct0Tipo: TSmallintField
      FieldName = 'Tipo'
      Required = True
    end
    object QrLocLanct0Vencimento: TDateField
      FieldName = 'Vencimento'
      Required = True
    end
    object QrLocLanct0Genero: TIntegerField
      FieldName = 'Genero'
    end
    object QrLocLanct0CliInt: TIntegerField
      FieldName = 'CliInt'
    end
    object QrLocLanct0Mez: TIntegerField
      FieldName = 'Mez'
      Required = True
    end
    object QrLocLanct0CentroCusto: TIntegerField
      FieldName = 'CentroCusto'
    end
  end
  object QrLocCta: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT DISTINCT lnk.Codigo Conta, lnk.Cliente, '
      'lnk.Fornece, lnk.UsaEntBank, lnk.ComposHist, '
      'lnk.Controle, lnk.MultiCtas, con.Mensal, con.Nome,'
      'IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NOME_CLIENTE,'
      'IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NOME_FORNECE'
      'FROM contaslnk lnk'
      'LEFT JOIN contas con ON con.Codigo=lnk.Codigo'
      'LEFT JOIN entidades cli ON cli.Codigo=lnk.Cliente'
      'LEFT JOIN entidades frn ON frn.Codigo=lnk.Fornece'
      'WHERE lnk.Considera=1'
      'AND lnk.Texto = TRIM(:P0)'
      'AND lnk.Doc = TRIM(:P1)'
      'AND lnk.CliInt IN (0, :P2)'
      'AND lnk.Banco IN (0, :P3)')
    Left = 252
    Top = 417
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
    object QrLocCtaConta: TIntegerField
      FieldName = 'Conta'
      Required = True
    end
    object QrLocCtaCliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrLocCtaFornece: TIntegerField
      FieldName = 'Fornece'
    end
    object QrLocCtaUsaEntBank: TSmallintField
      FieldName = 'UsaEntBank'
    end
    object QrLocCtaMensal: TWideStringField
      FieldName = 'Mensal'
      Size = 1
    end
    object QrLocCtaComposHist: TSmallintField
      FieldName = 'ComposHist'
      Required = True
    end
    object QrLocCtaNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
    object QrLocCtaMultiCtas: TIntegerField
      FieldName = 'MultiCtas'
    end
    object QrLocCtaControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrLocCtaNOME_CLIENTE: TWideStringField
      FieldName = 'NOME_CLIENTE'
      Size = 100
    end
    object QrLocCtaNOME_FORNECE: TWideStringField
      FieldName = 'NOME_FORNECE'
      Size = 100
    end
  end
  object QrLocDsc: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT DISTINCT Controle'
      'FROM contaslnk'
      'WHERE Considera=0'
      'AND Texto = TRIM(:P0)'
      'AND Doc = TRIM(:P1)'
      'AND CliInt IN (0, :P2)'
      'AND Banco IN (0, :P3)')
    Left = 280
    Top = 417
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
    object QrLocDscControle: TIntegerField
      FieldName = 'Controle'
    end
  end
  object QrMascaras: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Texto, Doc, Codigo Conta'
      'FROM contaslnk'
      'WHERE Considera =:P0 '
      'AND (Locate("%", Doc) > 0'
      '  OR Locate("%", Texto) > 0) '
      'AND Banco IN(0, :P1)'
      'AND CliInt IN(0, :P2)')
    Left = 308
    Top = 417
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrMascarasTexto: TWideStringField
      FieldName = 'Texto'
      Size = 50
    end
    object QrMascarasDoc: TWideStringField
      FieldName = 'Doc'
    end
    object QrMascarasConta: TIntegerField
      FieldName = 'Conta'
      Required = True
    end
  end
  object QrDesLoc: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Linha FROM concilia'
      'WHERE Acao = 0'
      'AND Docum LIKE "COB%"'
      'AND Texto = "TARIFA SERV.COBR.TITULOS"'
      '')
    Left = 308
    Top = 445
    object QrDesLocLinha: TIntegerField
      FieldName = 'Linha'
    end
  end
  object PMCadastro: TPopupMenu
    Left = 128
    Top = 449
    object CadastrarnovoLinkdeconta1: TMenuItem
      Caption = '&Cadastrar novo &Link de conta'
      OnClick = CadastrarnovoLinkdeconta1Click
    end
    object Cadastrarnovoitemignorvel1: TMenuItem
      Caption = '&Cadastrar novo item ignor'#225'vel'
      OnClick = Cadastrarnovoitemignorvel1Click
    end
    object N8: TMenuItem
      Caption = '-'
    end
    object Gerenciamentodelinks1: TMenuItem
      Caption = 'Gerenciamento de links'
      OnClick = Gerenciamentodelinks1Click
    end
  end
  object PMAcao: TPopupMenu
    OnPopup = PMAcaoPopup
    Left = 228
    Top = 448
    object Limpa1: TMenuItem
      Caption = '&Limpa a'#231#227'o das linhas selecionadas'
      OnClick = Limpa1Click
    end
    object Ignora1: TMenuItem
      Caption = 'Muda a'#231#227'o das linhas selecionadas para I&GNORA'
      OnClick = Ignora1Click
    end
    object Mudaaoparainclui1: TMenuItem
      Caption = 'Muda a'#231#227'o das linhas selecionadas para I&NCLUI'
      OnClick = Mudaaoparainclui1Click
    end
    object N9: TMenuItem
      Caption = '-'
    end
    object Gerarnmerododocumentopelohistrico1: TMenuItem
      Caption = 'Gerar n'#250'mero do documento pelo hist'#243'rico'
      OnClick = Gerarnmerododocumentopelohistrico1Click
    end
    object irarpontodonmerododocumento1: TMenuItem
      Caption = 'Tirar ponto do n'#250'mero do documento'
      OnClick = irarpontodonmerododocumento1Click
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object Altera1: TMenuItem
      Caption = 'Altera &Hist'#243'rico da linha selecionada'
      OnClick = Altera1Click
    end
    object Selecionacontaadequada1: TMenuItem
      Caption = '&Seleciona conta adequada'
      Enabled = False
      OnClick = Selecionacontaadequada1Click
    end
    object N6: TMenuItem
      Caption = '-'
    end
    object AlteraClienteFornecedor1: TMenuItem
      Caption = '&Altera Cliente / Fornecedor das linhas selecionadas'
      Enabled = False
      OnClick = AlteraClienteFornecedor1Click
    end
    object AlteraDepto1: TMenuItem
      Caption = 'Altera Depto das linhas selecionadas'
      Enabled = False
      OnClick = AlteraDepto1Click
    end
    object Alteraperodomsdecompetnciadolanamento1: TMenuItem
      Caption = 'Altera per'#237'odo (m'#234's) de compet'#234'ncia do lan'#231'amento'
      Enabled = False
      OnClick = Alteraperodomsdecompetnciadolanamento1Click
    end
    object N3: TMenuItem
      Caption = '-'
    end
    object xcluirlinhasselecionadas1: TMenuItem
      Caption = '&Excluir itens'
      object Excluirlinhasselecionadas1: TMenuItem
        Caption = '&Excluir linhas selecionadas'
        OnClick = Excluirlinhasselecionadas1Click
      end
      object ExcluirTODOSitensIgnorados1: TMenuItem
        Caption = 'Excluir TODOS itens &Ignorados'
        OnClick = ExcluirTODOSitensIgnorados1Click
      end
      object ExcluirTODOSitensDuplicados1: TMenuItem
        Caption = 'Excluir TODOS itens &Duplicados'
        OnClick = ExcluirTODOSitensDuplicados1Click
      end
      object ExcluirTODOSitensJQuitados1: TMenuItem
        Caption = 'Excluir TODOS itens J'#225' &Quitados'
        OnClick = ExcluirTODOSitensJQuitados1Click
      end
      object N4: TMenuItem
        Caption = '-'
      end
      object DeixarapenasConciliveis1: TMenuItem
        Caption = 'Deixar apenas &Concili'#225'veis'
        object Manteritenssemerro1: TMenuItem
          Caption = '&Manter itens sem erro'
          OnClick = Manteritenssemerro1Click
        end
        object Nomanteritenssemerro1: TMenuItem
          Caption = '&N'#227'o manter itens sem erro'
          OnClick = Nomanteritenssemerro1Click
        end
      end
    end
  end
  object QrImpede: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT COUNT(*)  Itens'
      'FROM concilia '
      'WHERE Acao<1')
    Left = 420
    Top = 393
    object QrImpedeItens: TLargeintField
      FieldName = 'Itens'
      Required = True
    end
  end
  object QrDupl: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      '')
    Left = 416
    Top = 421
    object QrDuplControle: TIntegerField
      FieldName = 'Controle'
    end
  end
  object frxConcilia: TfrxReport
    Version = '2022.1'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39405.820723553200000000
    ReportOptions.LastChange = 39405.820723553200000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    OnGetValue = frxConciliaGetValue
    Left = 240
    Top = 148
    Datasets = <
      item
        DataSet = frxDsConcilia
        DataSetName = 'frxDsConcilia'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      Frame.Typ = []
      MirrorMode = []
      object ReportTitle1: TfrxReportTitle
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 128.504020000000000000
        Top = 18.897650000000000000
        Width = 793.701300000000000000
        object Memo1: TfrxMemoView
          AllowVectorExport = True
          Left = 56.692950000000000000
          Top = 52.913420000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'EXTRATO IMPORTADO EM CONCILIA'#65533#65533'O BANC'#65533'RIA')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          AllowVectorExport = True
          Left = 56.692950000000000000
          Top = 37.795300000000000000
          Width = 680.315400000000000000
          Height = 15.118120000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[VAR_HORA_DATA]')
          ParentFont = False
        end
        object Memo19: TfrxMemoView
          AllowVectorExport = True
          Left = 56.692950000000000000
          Top = 79.370130000000000000
          Width = 45.354360000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Banco:')
          ParentFont = False
        end
        object Memo20: TfrxMemoView
          AllowVectorExport = True
          Left = 102.047310000000000000
          Top = 79.370130000000000000
          Width = 64.252010000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[VARF_BANCO]')
          ParentFont = False
        end
        object Memo21: TfrxMemoView
          AllowVectorExport = True
          Left = 56.692950000000000000
          Top = 98.267780000000000000
          Width = 79.370130000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Condom'#65533'nio:')
          ParentFont = False
        end
        object Memo22: TfrxMemoView
          AllowVectorExport = True
          Left = 136.063080000000000000
          Top = 98.267780000000000000
          Width = 600.945270000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[VARF_CONDOMINIO]')
          ParentFont = False
        end
        object Memo23: TfrxMemoView
          AllowVectorExport = True
          Left = 166.299320000000000000
          Top = 79.370130000000000000
          Width = 154.960730000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Conta corrente vinculada:')
          ParentFont = False
        end
        object Memo24: TfrxMemoView
          AllowVectorExport = True
          Left = 321.260050000000000000
          Top = 79.370130000000000000
          Width = 415.748300000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[VARF_CONTA_VINCUL]')
          ParentFont = False
        end
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 22.677165354330700000
        Top = 245.669450000000000000
        Width = 793.701300000000000000
        DataSet = frxDsConcilia
        DataSetName = 'frxDsConcilia'
        RowCount = 0
        object Memo3: TfrxMemoView
          AllowVectorExport = True
          Left = 56.692950000000000000
          Width = 26.456710000000000000
          Height = 22.677165350000000000
          DataField = 'Linha'
          DataSet = frxDsConcilia
          DataSetName = 'frxDsConcilia'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsConcilia."Linha"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo4: TfrxMemoView
          AllowVectorExport = True
          Left = 83.149660000000000000
          Width = 49.133890000000000000
          Height = 22.677165350000000000
          DataField = 'DataM'
          DataSet = frxDsConcilia
          DataSetName = 'frxDsConcilia'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsConcilia."DataM"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo5: TfrxMemoView
          AllowVectorExport = True
          Left = 132.283550000000000000
          Width = 132.283550000000000000
          Height = 22.677165354330700000
          DataField = 'Texto'
          DataSet = frxDsConcilia
          DataSetName = 'frxDsConcilia'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsConcilia."Texto"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo6: TfrxMemoView
          AllowVectorExport = True
          Left = 264.567100000000000000
          Width = 94.488250000000000000
          Height = 22.677165350000000000
          DataField = 'Docum'
          DataSet = frxDsConcilia
          DataSetName = 'frxDsConcilia'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsConcilia."Docum"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo7: TfrxMemoView
          AllowVectorExport = True
          Left = 359.055350000000000000
          Width = 64.251968503937000000
          Height = 22.677165354330700000
          DataField = 'Valor'
          DataSet = frxDsConcilia
          DataSetName = 'frxDsConcilia'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsConcilia."Valor"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo8: TfrxMemoView
          AllowVectorExport = True
          Left = 423.307360000000000000
          Width = 75.590551180000000000
          Height = 22.677165350000000000
          DataField = 'Saldo'
          DataSet = frxDsConcilia
          DataSetName = 'frxDsConcilia'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsConcilia."Saldo"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo18: TfrxMemoView
          AllowVectorExport = True
          Left = 498.897960000000000000
          Width = 238.110390000000000000
          Height = 22.677165350000000000
          DataField = 'EXPLICACAO'
          DataSet = frxDsConcilia
          DataSetName = 'frxDsConcilia'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsConcilia."EXPLICACAO"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118120000000000000
        Top = 170.078850000000000000
        Width = 793.701300000000000000
        object Memo9: TfrxMemoView
          AllowVectorExport = True
          Left = 56.692950000000000000
          Width = 26.456710000000000000
          Height = 15.118120000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Seq.')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo10: TfrxMemoView
          AllowVectorExport = True
          Left = 83.149660000000000000
          Width = 49.133890000000000000
          Height = 15.118120000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Data')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo11: TfrxMemoView
          AllowVectorExport = True
          Left = 132.283550000000000000
          Width = 132.283550000000000000
          Height = 15.118120000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Hist'#65533'rico')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo12: TfrxMemoView
          AllowVectorExport = True
          Left = 264.567100000000000000
          Width = 94.488250000000000000
          Height = 15.118120000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Documento')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo13: TfrxMemoView
          AllowVectorExport = True
          Left = 359.055350000000000000
          Width = 64.251968503937000000
          Height = 15.118120000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Valor')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo14: TfrxMemoView
          AllowVectorExport = True
          Left = 423.307360000000000000
          Width = 75.590551180000000000
          Height = 15.118120000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Saldo')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo17: TfrxMemoView
          AllowVectorExport = True
          Left = 498.897960000000000000
          Width = 238.110390000000000000
          Height = 15.118120000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Coment'#65533'rios')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 52.913420000000000000
        Top = 374.173470000000000000
        Width = 793.701300000000000000
        object Memo15: TfrxMemoView
          AllowVectorExport = True
          Left = 548.031850000000000000
          Width = 188.976500000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#65533'gina [Page#] de [TotalPages#]')
          ParentFont = False
        end
      end
      object Footer1: TfrxFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 22.677180000000000000
        Top = 291.023810000000000000
        Width = 793.701300000000000000
        object Memo16: TfrxMemoView
          AllowVectorExport = True
          Left = 56.692950000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          ParentFont = False
          VAlign = vaCenter
        end
      end
    end
  end
  object frxDsConcilia: TfrxDBDataset
    UserName = 'frxDsConcilia'
    CloseDataSource = False
    DataSet = QrConcilia0
    BCDToCurrency = False
    DataSetOptions = []
    Left = 268
    Top = 148
  end
  object PMImporta: TPopupMenu
    Left = 48
    Top = 148
    object Excel1: TMenuItem
      Caption = '&Excel'
      OnClick = Excel1Click
    end
    object Money20001: TMenuItem
      Caption = '&Money / Quicken'
      OnClick = Money20001Click
    end
  end
  object Timer1: TTimer
    Enabled = False
    Interval = 10
    OnTimer = Timer1Timer
    Left = 428
    Top = 128
  end
  object PMDoubleClick: TPopupMenu
    OnPopup = PMDoubleClickPopup
    Left = 164
    Top = 148
    object Selecionacontaadequada3: TMenuItem
      Caption = '&Seleciona conta adequada'
      Enabled = False
      OnClick = Selecionacontaadequada3Click
    end
    object AlteraHistorico2: TMenuItem
      Caption = 'Altera &Hist'#243'rico da linha selecionada'
      OnClick = AlteraHistorico2Click
    end
    object InformaraContasercriadoumlanamento1: TMenuItem
      Caption = 'Informar a &Conta (ser'#225'  criado um lan'#231'amento)'
      OnClick = InformaraContasercriadoumlanamento1Click
    end
    object ransferncia1: TMenuItem
      Caption = '&Transfer'#234'ncia entre carteiras'
      OnClick = ransferncia1Click
    end
    object N7: TMenuItem
      Caption = '-'
    end
    object AlteraClienteFornecedor2: TMenuItem
      Caption = '&Altera Cliente / Fornecedor das linhas selecionadas'
      Enabled = False
      OnClick = AlteraClienteFornecedor2Click
    end
    object AlteraDepto2: TMenuItem
      Caption = 'Altera Depto das linhas selecionadas'
      Enabled = False
      OnClick = AlteraDepto2Click
    end
    object Alteraperiodo2: TMenuItem
      Caption = 'Altera per'#237'odo (m'#234's) de compet'#234'ncia do lan'#231'amento'
      Enabled = False
      OnClick = Alteraperiodo2Click
    end
  end
  object QrContasLnkIts: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT cta.Nome NOMEGENERO, cta.Mensal, '
      'IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NOME_CLIENTE,'
      'IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NOME_FORNECE,'
      'clk.* '
      'FROM contaslnkits clk'
      'LEFT JOIN contas cta ON cta.Codigo=clk.Genero'
      'LEFT JOIN entidades cli ON cli.Codigo=clk.Cliente'
      'LEFT JOIN entidades frn ON frn.Codigo=clk.Fornece'
      'WHERE clk.Controle=:P0'
      'ORDER BY clk.TipoCalc')
    Left = 476
    Top = 436
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrContasLnkItsSubCtrl: TIntegerField
      FieldName = 'SubCtrl'
    end
    object QrContasLnkItsGenero: TIntegerField
      FieldName = 'Genero'
    end
    object QrContasLnkItsTexto: TWideStringField
      FieldName = 'Texto'
      Size = 50
    end
    object QrContasLnkItsTipoCalc: TSmallintField
      FieldName = 'TipoCalc'
    end
    object QrContasLnkItsFator: TFloatField
      FieldName = 'Fator'
    end
    object QrContasLnkItsConsidera: TSmallintField
      FieldName = 'Considera'
    end
    object QrContasLnkItsCliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrContasLnkItsFornece: TIntegerField
      FieldName = 'Fornece'
    end
    object QrContasLnkItsUsaEntBank: TSmallintField
      FieldName = 'UsaEntBank'
    end
    object QrContasLnkItsComposHist: TSmallintField
      FieldName = 'ComposHist'
    end
    object QrContasLnkItsNOMEGENERO: TWideStringField
      FieldName = 'NOMEGENERO'
      Size = 50
    end
    object QrContasLnkItsMensal: TWideStringField
      FieldName = 'Mensal'
      Size = 1
    end
    object QrContasLnkItsNOME_CLIENTE: TWideStringField
      FieldName = 'NOME_CLIENTE'
      Size = 100
    end
    object QrContasLnkItsNOME_FORNECE: TWideStringField
      FieldName = 'NOME_FORNECE'
      Size = 100
    end
  end
  object QrConcilia1: TMySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrConcilia1CalcFields
    SQL.Strings = (
      'SELECT cnc.* '
      'FROM concilia cnc'
      'WHERE cnc.Nivel=1'
      'AND cnc.Linha=:P0'
      'ORDER BY cnc.Linha')
    Left = 796
    Top = 9
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrConcilia1Linha: TIntegerField
      FieldName = 'Linha'
      Origin = 'concilia.Linha'
    end
    object QrConcilia1DataM: TDateField
      FieldName = 'DataM'
      Origin = 'concilia.DataM'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrConcilia1Texto: TWideStringField
      FieldName = 'Texto'
      Origin = 'concilia.Texto'
      Size = 50
    end
    object QrConcilia1Valor: TFloatField
      FieldName = 'Valor'
      Origin = 'concilia.Valor'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrConcilia1Docum: TWideStringField
      FieldName = 'Docum'
      Origin = 'concilia.Docum'
    end
    object QrConcilia1Saldo: TFloatField
      FieldName = 'Saldo'
      Origin = 'concilia.Saldo'
      DisplayFormat = '#,###,##0.00'
    end
    object QrConcilia1Conta: TIntegerField
      FieldName = 'Conta'
      Origin = 'concilia.Conta'
      DisplayFormat = '0;-0; '
    end
    object QrConcilia1Acao: TIntegerField
      FieldName = 'Acao'
      Origin = 'concilia.Acao'
    end
    object QrConcilia1NOMEACAO: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMEACAO'
      Size = 10
      Calculated = True
    end
    object QrConcilia1EXPLICACAO: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'EXPLICACAO'
      Size = 100
      Calculated = True
    end
    object QrConcilia1DifEr: TFloatField
      FieldName = 'DifEr'
      Origin = 'concilia.DifEr'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrConcilia1Clien: TIntegerField
      FieldName = 'Clien'
      Origin = 'concilia.Clien'
    end
    object QrConcilia1Forne: TIntegerField
      FieldName = 'Forne'
      Origin = 'concilia.Forne'
    end
    object QrConcilia1Perio: TIntegerField
      FieldName = 'Perio'
    end
    object QrConcilia1Periodo_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'Periodo_TXT'
      Size = 7
      Calculated = True
    end
    object QrConcilia1SERIE: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'SERIE'
      Size = 30
      Calculated = True
    end
    object QrConcilia1CtrlE: TIntegerField
      FieldName = 'CtrlE'
      DisplayFormat = '0;-0; '
    end
    object QrConcilia1CartT: TIntegerField
      FieldName = 'CartT'
    end
    object QrConcilia1CtaLk: TIntegerField
      FieldName = 'CtaLk'
    end
    object QrConcilia1CHEQUE: TFloatField
      FieldKind = fkCalculated
      FieldName = 'CHEQUE'
      Calculated = True
    end
    object QrConcilia1Depto: TIntegerField
      FieldName = 'Depto'
    end
    object QrConcilia1NOMEDEPTO: TWideStringField
      FieldName = 'NOMEDEPTO'
      Size = 100
    end
    object QrConcilia1NOMECONTA: TWideStringField
      FieldName = 'NOMECONTA'
      Size = 100
    end
    object QrConcilia1NOMETERCE: TWideStringField
      FieldName = 'NOMETERCE'
      Size = 100
    end
    object QrConcilia1NOMECARTE: TWideStringField
      FieldName = 'NOMECARTE'
      Size = 100
    end
    object QrConcilia1TIPOCARTE: TIntegerField
      FieldName = 'TIPOCARTE'
    end
  end
  object DsConcilia1: TDataSource
    DataSet = QrConcilia1
    Left = 824
    Top = 9
  end
  object QrLocLanct1: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      ''
      '')
    Left = 372
    Top = 349
    object QrLocLanct1Valor: TFloatField
      FieldName = 'Valor'
    end
    object QrLocLanct1Itens: TLargeintField
      FieldName = 'Itens'
      Required = True
    end
  end
end
