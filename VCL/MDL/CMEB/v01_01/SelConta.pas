unit SelConta;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, UnInternalConsts, Buttons, DBCtrls, Db, (*DBTables,*)
  UnGOTOy, Mask, UMySQLModule, mySQLDbTables, dmkEdit, dmkEditCB,
  dmkDBLookupComboBox, Grids, DBGrids, dmkLabel, dmkGeral, dmkImage, UnDmkEnums;

type
  TFmSelConta = class(TForm)
    PainelDados: TPanel;
    Label1: TLabel;
    CBConta: TdmkDBLookupComboBox;
    DsContas: TDataSource;
    EdConta: TdmkEditCB;
    QrContas: TmySQLQuery;
    QrContasCodigo: TIntegerField;
    QrContasNome: TWideStringField;
    Panel2: TPanel;
    DBGrid1: TDBGrid;
    QrLct: TmySQLQuery;
    DsLct: TDataSource;
    QrLctData: TDateField;
    QrLctMesAno: TWideStringField;
    QrLctControle: TIntegerField;
    QrLctDescricao: TWideStringField;
    QrLctNotaFiscal: TIntegerField;
    QrLctDebito: TFloatField;
    QrLctCredito: TFloatField;
    QrLctVALOR: TFloatField;
    QrLctNO_TER: TWideStringField;
    QrLctFornecedor: TIntegerField;
    QrLctCliente: TIntegerField;
    EdDescricao: TdmkEdit;
    Label2: TLabel;
    dmkLabel1: TdmkLabel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel3: TPanel;
    PnSaiDesis: TPanel;
    BtConfirma: TBitBtn;
    BtDesiste: TBitBtn;
    procedure BtDesisteClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure EdContaChange(Sender: TObject);
    procedure DBGrid1DblClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    FTabLctA: String;
  end;

var
  FmSelConta: TFmSelConta;

implementation

uses UnMyObjects, Module, Entidades, DmkDAC_PF;

{$R *.DFM}

procedure TFmSelConta.BtDesisteClick(Sender: TObject);
begin
  VAR_CONTA := 0;
  Close;
end;

procedure TFmSelConta.DBGrid1DblClick(Sender: TObject);
begin
  EdDescricao.Text := QrLctDescricao.Value;
end;

procedure TFmSelConta.EdContaChange(Sender: TObject);
begin
  QrLct.Close;
  QrLct.SQL.Clear;
  QrLct.SQL.Add('SELECT lan.Data,  lan.Credito-lan.Debito VALOR,');
  QrLct.SQL.Add('IF(lan.Mez=0,"", CONCAT(RIGHT(lan.Mez, 2),');
  QrLct.SQL.Add('"/", LEFT(LPAD(lan.Mez, 4, "0"), 2)) ) MesAno,');
  QrLct.SQL.Add('lan.Controle, lan.Descricao, lan.NotaFiscal, lan.Debito,');
  QrLct.SQL.Add('lan.Credito, lan.Fornecedor, lan.Cliente,');
  QrLct.SQL.Add('IF(lan.Debito< lan.Credito,');
  QrLct.SQL.Add('IF(cli.Tipo=0,cli.RazaoSocial,cli.Nome),');
  QrLct.SQL.Add('IF(frn.Tipo=0,frn.RazaoSocial,frn.Nome)) NO_TER');
{### Fazer B e D?
}
  QrLct.SQL.Add('FROM ' + FTabLctA + ' lan');
  QrLct.SQL.Add('LEFT JOIN entidades cli ON cli.Codigo=lan.Cliente');
  QrLct.SQL.Add('LEFT JOIN entidades frn ON frn.Codigo=lan.Fornecedor');
  QrLct.SQL.Add('WHERE lan.Genero=:P0');
  QrLct.SQL.Add('AND lan.Sub=0');
  QrLct.SQL.Add('ORDER BY lan.Data DESC');
  QrLct.Params[0].AsInteger := EdConta.ValueVariant;
  UnDmkDAC_PF.AbreQuery(QrLct, Dmod.MyDB);
end;

procedure TFmSelConta.BtConfirmaClick(Sender: TObject);
begin
  VAR_CONTA := EdConta.ValueVariant;
  if VAR_CONTA = 0 then
  begin
    Geral.MB_Aviso('Informe uma Conta!');
    EdConta.SetFocus;
    Exit;
  end;
  VAR_LANCTO  := QrLctControle.Value;
  VAR_CLIENTE := QrLctCliente.Value;
  VAR_FORNECE := QrLctFornecedor.Value;
  VAR_NO_TERC := QrLctNO_TER.Value;
  VAR_CONTA_NOME := QrContasNome.Value;
  VAR_HISTORICO  := EdDescricao.Text;
  Close;
end;

procedure TFmSelConta.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmSelConta.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmSelConta.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stUpd;
end;

end.
