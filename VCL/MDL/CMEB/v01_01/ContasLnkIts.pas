unit ContasLnkIts;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, UnInternalConsts, Buttons, DBCtrls, Db, (*DBTables,*)
  UnGOTOy, Mask, UMySQLModule, mySQLDbTables, dmkCheckGroup,
  dmkLabel, dmkEdit, dmkEditCB, dmkDBLookupComboBox, dmkCheckBox, dmkRadioGroup,
  dmkGeral, dmkImage, UnDmkEnums;

type
  TFmContasLnkIts = class(TForm)
    PainelDados: TPanel;
    LaConta: TLabel;
    CBConta: TdmkDBLookupComboBox;
    DsContas: TDataSource;
    EdConta: TdmkEditCB;
    QrContas: TmySQLQuery;
    QrContasCodigo: TIntegerField;
    QrContasNome: TWideStringField;
    EdTexto: TdmkEdit;
    Label2: TLabel;
    Label4: TLabel;
    EdCliInt: TdmkEditCB;
    CBCliInt: TdmkDBLookupComboBox;
    EdBanco: TdmkEditCB;
    CBBanco: TdmkDBLookupComboBox;
    Label5: TLabel;
    QrCliInt: TmySQLQuery;
    DsCliInt: TDataSource;
    QrCliIntCodigo: TIntegerField;
    QrCliIntNOMEENTI: TWideStringField;
    QrBancos: TmySQLQuery;
    DsBancos: TDataSource;
    QrBancosCodigo: TIntegerField;
    QrBancosNome: TWideStringField;
    Label1: TLabel;
    EdFornece: TdmkEditCB;
    CBFornece: TdmkDBLookupComboBox;
    Label6: TLabel;
    EdCliente: TdmkEditCB;
    CBCliente: TdmkDBLookupComboBox;
    QrFornece: TmySQLQuery;
    DsFornece: TDataSource;
    QrCliente: TmySQLQuery;
    IntegerField1: TIntegerField;
    StringField1: TWideStringField;
    DsCliente: TDataSource;
    QrForneceCodigo: TIntegerField;
    QrForneceNOMEENTI: TWideStringField;
    CkUsaEntBank: TdmkCheckBox;
    CGComposHist: TdmkCheckGroup;
    RGTipoCalc: TdmkRadioGroup;
    EdFator: TdmkEdit;
    Label3: TLabel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel1: TPanel;
    PnSaiDesis: TPanel;
    BtConfirma: TBitBtn;
    BtDesiste: TBitBtn;
    procedure BtDesisteClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormResize(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    FConta, FControle: Integer;
  end;

var
  FmContasLnkIts: TFmContasLnkIts;

implementation

uses UnMyObjects, Module, ContasLnk, DmkDAC_PF;

{$R *.DFM}

procedure TFmContasLnkIts.BtDesisteClick(Sender: TObject);
begin
  Close;
end;

procedure TFmContasLnkIts.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  if RGTipoCalc.ItemIndex < 0 then
    RGTipoCalc.ItemIndex := 0;
end;

procedure TFmContasLnkIts.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  UnDmkDAC_PF.AbreQuery(QrContas, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrBancos, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrCliInt, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrCliente, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrFornece, Dmod.MyDB);
end;

procedure TFmContasLnkIts.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmContasLnkIts.BtConfirmaClick(Sender: TObject);
var
  Texto(*, Doc*): String;
  (*Codigo, *)Considera, Cliente, Fornece, UsaEntBank, ComposHist, Controle,
  Genero, SubCtrl, TipoCalc: Integer;
  Fator: Double;
begin
  Genero := Geral.IMV(EdConta.Text);
  if (Genero = 0) and (EdConta.Enabled) then
  begin
    Geral.MB_Aviso('Informe uma Conta!');
    EdConta.SetFocus;
    Exit;
  end;
  Texto      := EdTexto.Text;
  {
  Doc        := EdDoc.Text;
  CliInt     := Geral.IMV(EdCliInt.Text);
  Banco      := Geral.IMV(EdBanco.Text);
  }
  Considera  := Geral.BoolToInt(EdConta.Enabled);
  Cliente    := Geral.IMV(EdCliente.Text);
  Fornece    := Geral.IMV(EdFornece.Text);
  UsaEntBank := Geral.BoolToInt(CkUsaEntBank.Checked);
  ComposHist := CGComposHist.Value;
  TipoCalc   := RGTipoCalc.ItemIndex;
  Controle   := FmContasLnk.QrContasLnkControle.Value;
  SubCtrl    := FmContasLnk.QrContasLnkItsSubCtrl.Value;
  Fator      := EdFator.ValueVariant;
  //
  SubCtrl := UMyMod.BuscaEmLivreY_Def('contaslnkits', 'SubCtrl', ImgTipo.SQLType, SubCtrl);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'contaslnkits', False, [
  'Controle', 'Genero', 'Texto',
  'TipoCalc', 'Fator', 'Considera',
  'Cliente', 'Fornece', 'UsaEntBank',
  'ComposHist'], [
  'SubCtrl'], [
  Controle, Genero, Texto,
  TipoCalc, Fator, Considera,
  Cliente, Fornece, UsaEntBank,
  ComposHist], [
  SubCtrl], True) then
  begin
    FmContasLnk.AtualizaMultiCtas();
    FmContasLnk.ReopenContasLnkIts(0);
    Close;
  end;
end;

end.
