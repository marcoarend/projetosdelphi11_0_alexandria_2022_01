object FmContasLnkIts: TFmContasLnkIts
  Left = 396
  Top = 181
  Caption = 'FIN-PLCTA-027 :: Sub-itens de Links de Consolida'#231#227'o Banc'#225'ria'
  ClientHeight = 464
  ClientWidth = 784
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelDados: TPanel
    Left = 0
    Top = 48
    Width = 784
    Height = 302
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    ExplicitLeft = 100
    ExplicitTop = 44
    ExplicitHeight = 298
    object LaConta: TLabel
      Left = 12
      Top = 4
      Width = 131
      Height = 13
      Caption = 'Conta (do plano de contas):'
    end
    object Label2: TLabel
      Left = 360
      Top = 156
      Width = 124
      Height = 13
      Caption = 'Complemento de hist'#243'rico:'
    end
    object Label4: TLabel
      Left = 12
      Top = 48
      Width = 127
      Height = 13
      Caption = 'Entidade: (zero para todas)'
      Enabled = False
    end
    object Label5: TLabel
      Left = 392
      Top = 48
      Width = 116
      Height = 13
      Caption = 'Banco: (zero para todos)'
      Enabled = False
    end
    object Label1: TLabel
      Left = 12
      Top = 92
      Width = 60
      Height = 13
      Caption = 'Fornecedor: '
    end
    object Label6: TLabel
      Left = 392
      Top = 92
      Width = 35
      Height = 13
      Caption = 'Cliente:'
    end
    object Label3: TLabel
      Left = 520
      Top = 196
      Width = 100
      Height = 13
      Caption = '('#185') Valor / percentual:'
    end
    object CBConta: TdmkDBLookupComboBox
      Left = 80
      Top = 20
      Width = 689
      Height = 21
      KeyField = 'Codigo'
      ListField = 'Nome'
      ListSource = DsContas
      TabOrder = 1
      dmkEditCB = EdConta
      QryCampo = 'Genero'
      UpdType = utYes
      LocF7SQLMasc = '$#'
    end
    object EdConta: TdmkEditCB
      Left = 12
      Top = 20
      Width = 65
      Height = 21
      Alignment = taRightJustify
      TabOrder = 0
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'Genero'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      DBLookupComboBox = CBConta
      IgnoraDBLookupComboBox = False
    end
    object EdTexto: TdmkEdit
      Left = 360
      Top = 172
      Width = 409
      Height = 21
      MaxLength = 50
      TabOrder = 12
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      QryCampo = 'Texto'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      ValWarn = False
    end
    object EdCliInt: TdmkEditCB
      Left = 12
      Top = 64
      Width = 65
      Height = 21
      Alignment = taRightJustify
      Enabled = False
      TabOrder = 2
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      DBLookupComboBox = CBCliInt
      IgnoraDBLookupComboBox = False
    end
    object CBCliInt: TdmkDBLookupComboBox
      Left = 80
      Top = 64
      Width = 308
      Height = 21
      Enabled = False
      KeyField = 'Codigo'
      ListField = 'NOMEENTI'
      ListSource = DsCliInt
      TabOrder = 3
      dmkEditCB = EdCliInt
      UpdType = utYes
      LocF7SQLMasc = '$#'
    end
    object EdBanco: TdmkEditCB
      Left = 392
      Top = 64
      Width = 65
      Height = 21
      Alignment = taRightJustify
      Enabled = False
      TabOrder = 4
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      DBLookupComboBox = CBBanco
      IgnoraDBLookupComboBox = False
    end
    object CBBanco: TdmkDBLookupComboBox
      Left = 460
      Top = 64
      Width = 308
      Height = 21
      Enabled = False
      KeyField = 'Codigo'
      ListField = 'Nome'
      ListSource = DsBancos
      TabOrder = 5
      dmkEditCB = EdBanco
      UpdType = utYes
      LocF7SQLMasc = '$#'
    end
    object EdFornece: TdmkEditCB
      Left = 12
      Top = 108
      Width = 65
      Height = 21
      Alignment = taRightJustify
      TabOrder = 6
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'Fornece'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      DBLookupComboBox = CBFornece
      IgnoraDBLookupComboBox = False
    end
    object CBFornece: TdmkDBLookupComboBox
      Left = 80
      Top = 108
      Width = 308
      Height = 21
      KeyField = 'Codigo'
      ListField = 'NOMEENTI'
      ListSource = DsFornece
      TabOrder = 7
      dmkEditCB = EdFornece
      QryCampo = 'Fornece'
      UpdType = utYes
      LocF7SQLMasc = '$#'
    end
    object EdCliente: TdmkEditCB
      Left = 392
      Top = 108
      Width = 65
      Height = 21
      Alignment = taRightJustify
      TabOrder = 8
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'Cliente'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      DBLookupComboBox = CBCliente
      IgnoraDBLookupComboBox = False
    end
    object CBCliente: TdmkDBLookupComboBox
      Left = 460
      Top = 108
      Width = 308
      Height = 21
      KeyField = 'Codigo'
      ListField = 'NOMEENTI'
      ListSource = DsCliente
      TabOrder = 9
      dmkEditCB = EdCliente
      QryCampo = 'Cliente'
      UpdType = utYes
      LocF7SQLMasc = '$#'
    end
    object CkUsaEntBank: TdmkCheckBox
      Left = 12
      Top = 136
      Width = 669
      Height = 17
      Caption = 
        'Usar como fornecedor e/ou cliente, a entidade banc'#225'ria informada' +
        ' no cadastro do banco.'
      TabOrder = 10
      QryCampo = 'UsaEntBank'
      UpdType = utYes
      ValCheck = #0
      ValUncheck = #0
      OldValor = #0
    end
    object CGComposHist: TdmkCheckGroup
      Left = 12
      Top = 156
      Width = 341
      Height = 129
      Caption = 
        ' Composi'#231#227'o da descri'#231#227'o (Hist'#243'rico) para gera'#231#227'o do lan'#231'amento:' +
        ' '
      Items.Strings = (
        'Descri'#231#227'o do extrato'
        'Descri'#231#227'o da conta (Plano de contas)'
        'Nome do Terceiro (Forneceor / Cliente)'
        'Complemento de hist'#243'rico')
      TabOrder = 11
      QryCampo = 'ComposHist'
      UpdType = utYes
      Value = 0
      OldValor = 0
    end
    object RGTipoCalc: TdmkRadioGroup
      Left = 360
      Top = 196
      Width = 153
      Height = 89
      Caption = ' Forma de debitar / creditar: '
      ItemIndex = 0
      Items.Strings = (
        'Ser'#225' informado'
        'Valor fixo $ ('#185')'
        '% do total ('#185')'
        '% Saldo restante ('#185')')
      TabOrder = 13
      QryCampo = 'TipoCalc'
      UpdType = utYes
      OldValor = 0
    end
    object EdFator: TdmkEdit
      Left = 520
      Top = 212
      Width = 101
      Height = 21
      Alignment = taRightJustify
      TabOrder = 14
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 2
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,00'
      QryCampo = 'Fator'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 784
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    ExplicitWidth = 635
    object GB_R: TGroupBox
      Left = 736
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      ExplicitLeft = 587
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 688
      Height = 48
      Align = alClient
      TabOrder = 2
      ExplicitWidth = 539
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 550
        Height = 32
        Caption = 'Sub-itens de Links de Consolida'#231#227'o Banc'#225'ria'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 550
        Height = 32
        Caption = 'Sub-itens de Links de Consolida'#231#227'o Banc'#225'ria'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 550
        Height = 32
        Caption = 'Sub-itens de Links de Consolida'#231#227'o Banc'#225'ria'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 350
    Width = 784
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    ExplicitTop = 186
    ExplicitWidth = 635
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 780
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 631
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 394
    Width = 784
    Height = 70
    Align = alBottom
    TabOrder = 3
    ExplicitTop = 230
    ExplicitWidth = 635
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 780
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 631
      object PnSaiDesis: TPanel
        Left = 636
        Top = 0
        Width = 144
        Height = 53
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 0
        ExplicitLeft = 487
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 10
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Hint = 'Cancela inclus'#227'o / altera'#231#227'o'
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 14
        Top = 4
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Hint = 'Confima Inclus'#227'o / altera'#231#227'o'
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        OnClick = BtConfirmaClick
      end
    end
  end
  object DsContas: TDataSource
    DataSet = QrContas
    Left = 236
    Top = 58
  end
  object QrContas: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM contas'
      'ORDER BY Nome')
    Left = 208
    Top = 58
    object QrContasCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrContasNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object QrCliInt: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, CASE WHEN Tipo=0 THEN RazaoSocial '
      'ELSE Nome END NOMEENTI'
      'FROM entidades'
      'WHERE Cliente1="V"'
      'ORDER BY NOMEENTI')
    Left = 208
    Top = 98
    object QrCliIntCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCliIntNOMEENTI: TWideStringField
      FieldName = 'NOMEENTI'
      Size = 100
    end
  end
  object DsCliInt: TDataSource
    DataSet = QrCliInt
    Left = 236
    Top = 98
  end
  object QrBancos: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM bancos'
      'ORDER BY Nome')
    Left = 476
    Top = 98
    object QrBancosCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrBancosNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
  object DsBancos: TDataSource
    DataSet = QrBancos
    Left = 504
    Top = 98
  end
  object QrFornece: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, CASE WHEN Tipo=0 THEN RazaoSocial '
      'ELSE Nome END NOMEENTI'
      'FROM entidades'
      'WHERE Fornece1="V"'
      'OR Fornece2="V"'
      'OR Fornece3="V"'
      'OR Fornece4="V"'
      'OR Fornece5="V"'
      'OR Fornece6="V"'
      'ORDER BY NOMEENTI')
    Left = 208
    Top = 146
    object QrForneceCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrForneceNOMEENTI: TWideStringField
      FieldName = 'NOMEENTI'
      Size = 100
    end
  end
  object DsFornece: TDataSource
    DataSet = QrFornece
    Left = 236
    Top = 146
  end
  object QrCliente: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, CASE WHEN Tipo=0 THEN RazaoSocial '
      'ELSE Nome END NOMEENTI'
      'FROM entidades'
      'WHERE Cliente1="V"'
      'OR Cliente2="V"'
      'OR Cliente3="V"'
      'OR Cliente4="V"'
      'ORDER BY NOMEENTI')
    Left = 476
    Top = 142
    object IntegerField1: TIntegerField
      FieldName = 'Codigo'
    end
    object StringField1: TWideStringField
      FieldName = 'NOMEENTI'
      Size = 100
    end
  end
  object DsCliente: TDataSource
    DataSet = QrCliente
    Left = 504
    Top = 142
  end
end
