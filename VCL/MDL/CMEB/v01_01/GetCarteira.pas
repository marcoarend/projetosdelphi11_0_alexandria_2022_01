unit GetCarteira;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DBCtrls, Db, mySQLDbTables,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkImage, dmkGeral, UnDmkEnums;

type
  TFmGetCarteira = class(TForm)
    Panel1: TPanel;
    Label1: TLabel;
    EdCarteira: TdmkEditCB;
    CBCarteira: TdmkDBLookupComboBox;
    QrSelCarteiras: TmySQLQuery;
    DsSelCareteiras: TDataSource;
    QrSelCarteirasCodigo: TIntegerField;
    QrSelCarteirasNome: TWideStringField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel3: TPanel;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    BtOK: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    FCarteira: Integer;
  end;

  var
  FmGetCarteira: TFmGetCarteira;

implementation

uses UnMyObjects, Module, Principal, DmkDAC_PF;

{$R *.DFM}

procedure TFmGetCarteira.BtSaidaClick(Sender: TObject);
begin
  FCarteira := -1000;
  Close;
end;

procedure TFmGetCarteira.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmGetCarteira.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmGetCarteira.BtOKClick(Sender: TObject);
begin
  FCarteira := EdCarteira.ValueVariant;
  Close;
end;

procedure TFmGetCarteira.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  UnDmkDAC_PF.AbreQuery(QrSelCarteiras, Dmod.MyDB);
end;

end.
