object FmConciliaOFX: TFmConciliaOFX
  Left = 339
  Top = 185
  Caption = 'FIN-CONCI-004 :: Abrir Arquivos Money / Quicken'
  ClientHeight = 416
  ClientWidth = 792
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel3: TPanel
    Left = 0
    Top = 48
    Width = 792
    Height = 73
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object Label1: TLabel
      Left = 8
      Top = 4
      Width = 39
      Height = 13
      Caption = 'Arquivo:'
    end
    object LaAviso: TLabel
      Left = 420
      Top = 48
      Width = 9
      Height = 13
      Caption = '...'
    end
    object EdArquivo: TEdit
      Left = 8
      Top = 20
      Width = 681
      Height = 21
      TabOrder = 0
    end
    object PBInfo: TProgressBar
      Left = 8
      Top = 48
      Width = 405
      Height = 17
      TabOrder = 1
    end
    object BtAbrir: TBitBtn
      Tag = 14
      Left = 692
      Top = 12
      Width = 90
      Height = 40
      Caption = '&Abrir'
      NumGlyphs = 2
      TabOrder = 2
      OnClick = BtAbrirClick
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 121
    Width = 792
    Height = 181
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Memo1: TMemo
      Left = 0
      Top = 17
      Width = 792
      Height = 164
      Align = alClient
      ReadOnly = True
      TabOrder = 0
    end
    object StaticText1: TStaticText
      Left = 0
      Top = 0
      Width = 792
      Height = 17
      Align = alTop
      Alignment = taCenter
      Caption = 'Avisos:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 792
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 744
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 696
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 383
        Height = 32
        Caption = 'Abrir Arquivos Money / Quicken'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 383
        Height = 32
        Caption = 'Abrir Arquivos Money / Quicken'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 383
        Height = 32
        Caption = 'Abrir Arquivos Money / Quicken'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 302
    Width = 792
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 788
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 346
    Width = 792
    Height = 70
    Align = alBottom
    TabOrder = 4
    object Panel5: TPanel
      Left = 2
      Top = 15
      Width = 788
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object PnSaiDesis: TPanel
        Left = 644
        Top = 0
        Width = 144
        Height = 53
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 0
        object BtSaida: TBitBtn
          Tag = 13
          Left = 12
          Top = 3
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Sa'#237'da'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
        end
      end
    end
  end
  object QrBancos: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *'
      'FROM bancos'
      'ORDER BY Nome')
    Left = 12
    Top = 12
    object QrBancosCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrBancosNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QrBancosXlsLinha: TIntegerField
      FieldName = 'XlsLinha'
    end
    object QrBancosXlsData: TWideStringField
      FieldName = 'XlsData'
      Size = 2
    end
    object QrBancosXlsHist: TWideStringField
      FieldName = 'XlsHist'
      Size = 2
    end
    object QrBancosXlsDocu: TWideStringField
      FieldName = 'XlsDocu'
      Size = 2
    end
    object QrBancosXlsHiDo: TWideStringField
      FieldName = 'XlsHiDo'
      Size = 2
    end
    object QrBancosXlsCred: TWideStringField
      FieldName = 'XlsCred'
      Size = 2
    end
    object QrBancosXlsDebi: TWideStringField
      FieldName = 'XlsDebi'
      Size = 2
    end
    object QrBancosXlsCrDb: TWideStringField
      FieldName = 'XlsCrDb'
      Size = 2
    end
    object QrBancosXlsDouC: TWideStringField
      FieldName = 'XlsDouC'
      Size = 2
    end
    object QrBancosXlsTCDB: TSmallintField
      FieldName = 'XlsTCDB'
    end
    object QrBancosXlsComp: TWideStringField
      FieldName = 'XlsComp'
      Size = 2
    end
    object QrBancosXlsCPMF: TWideStringField
      FieldName = 'XlsCPMF'
      Size = 2
    end
    object QrBancosXlsSldo: TWideStringField
      FieldName = 'XlsSldo'
      Size = 2
    end
  end
  object DsBancos: TDataSource
    DataSet = QrBancos
    Left = 40
    Top = 12
  end
  object PMAbrir: TPopupMenu
    Left = 488
    Top = 176
    object OFC1: TMenuItem
      Caption = 'OF&C'
      OnClick = OFC1Click
    end
    object OFX1: TMenuItem
      Caption = 'OF&X'
      OnClick = OFX1Click
    end
  end
end
