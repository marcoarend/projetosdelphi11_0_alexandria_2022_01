unit Cartas;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls, Db, (*DBTables,*) JPEG, ExtDlgs,
  ZCF2, ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt, UnDmkListas,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, ComCtrls,
  dmkGeral, dmkEditCB, dmkDBLookupComboBox, dmkEdit, UnDmkProcFunc, dmkImage,
  UnDmkEnums, DmkDAC_PF;

type
  TFmCartas = class(TForm)
    PainelDados: TPanel;
    DsCartas: TDataSource;
    QrCartas: TmySQLQuery;
    PainelEdita: TPanel;
    Label9: TLabel;
    Label10: TLabel;
    EdCodigo: TdmkEdit;
    EdNome: TdmkEdit;
    Panel1: TPanel;
    Label1: TLabel;
    DBEdCodigo: TDBEdit;
    DBEdNome: TDBEdit;
    Label2: TLabel;
    QrCartasCodigo: TIntegerField;
    QrCartasTitulo: TWideStringField;
    QrCartasTexto: TWideMemoField;
    QrCartasLk: TIntegerField;
    QrCartasDataCad: TDateField;
    QrCartasDataAlt: TDateField;
    QrCartasUserCad: TIntegerField;
    QrCartasUserAlt: TIntegerField;
    TbTexto: TmySQLTable;
    DsTexto: TDataSource;
    TbTextoTexto: TWideMemoField;
    TbTextoCodigo: TIntegerField;
    TbTextoTitulo: TWideStringField;
    EdOrdem: TdmkEdit;
    Label3: TLabel;
    CBGrupo: TdmkDBLookupComboBox;
    EdGrupo: TdmkEditCB;
    Label5: TLabel;
    QrCartasG: TmySQLQuery;
    DsCartasG: TDataSource;
    QrCartasGCodigo: TIntegerField;
    QrCartasGNome: TWideStringField;
    QrCartasGLk: TIntegerField;
    QrCartasGDataCad: TDateField;
    QrCartasGDataAlt: TDateField;
    QrCartasGUserCad: TIntegerField;
    QrCartasGUserAlt: TIntegerField;
    QrCartasCartaG: TIntegerField;
    QrCartasPagina: TIntegerField;
    Label4: TLabel;
    DBEdit1: TDBEdit;
    Label6: TLabel;
    DBEdit2: TDBEdit;
    QrCartasNOMECARTAG: TWideStringField;
    RGTipoTexto: TRadioGroup;
    QrCartasTipo: TIntegerField;
    DBEdit3: TDBEdit;
    Label7: TLabel;
    QrCartasNOMETIPO: TWideStringField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel6: TPanel;
    BtDesiste: TBitBtn;
    BtInclui: TBitBtn;
    BtAltera: TBitBtn;
    BtItens: TBitBtn;
    BtExclui: TBitBtn;
    RETxtCartas: TRichEdit;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtIncluiClick(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrCartasAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure QrCartasAfterScroll(DataSet: TDataSet);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrCartasBeforeOpen(DataSet: TDataSet);
    procedure BtItensClick(Sender: TObject);
    procedure QrCartasAfterClose(DataSet: TDataSet);
    procedure QrCartasCalcFields(DataSet: TDataSet);
    procedure SbImprimeClick(Sender: TObject);
    procedure SbNovoClick(Sender: TObject);
    procedure BtExcluiClick(Sender: TObject);
    procedure QrCartasBeforeClose(DataSet: TDataSet);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure IncluiRegistro;
    procedure AlteraRegistro;
    procedure MostraEdicao(Mostra: Boolean; SQLType: TSQLType; Codigo: Integer);
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    procedure DefineTipoTexto;
    procedure EditaTexto(SubTipo: Integer);
  public
    { Public declarations }
    //FSubT,
    FTipo: Integer;
    procedure LocCod(Atual, Codigo: Integer);
  end;

var
  FmCartas: TFmCartas;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, Textos, Principal, ModuleGeral;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmCartas.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmCartas.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrCartasCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmCartas.DefParams;
begin
  VAR_GOTOTABELA := 'Cartas';
  VAR_GOTOMYSQLTABLE := QrCartas;
  VAR_GOTONEG := gotoPiZ;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT cg.Nome NOMECARTAG, ca.* FROM cartas ca');
  VAR_SQLx.Add('LEFT JOIN cartag cg ON cg.Codigo=ca.CartaG');
  VAR_SQLx.Add('WHERE ca.Codigo > 0');
  //
  VAR_SQL1.Add('AND ca.Codigo=:P0');
  //
  VAR_SQLa.Add('AND ca.Nome Like :P0');
  //
end;

procedure TFmCartas.MostraEdicao(Mostra: Boolean; SQLType: TSQLType; Codigo: Integer);
begin
  if Mostra then
  begin
    DefineTipoTexto;
    PainelEdita.Visible := True;
    PainelDados.Visible := False;
    if SQLType = stIns then
    begin
      EdCodigo.Text := FormatFloat(FFormatFloat, Codigo);
      //EdGrupo.Text  := '';
      //EdNome.Text   := '';
      //CBGrupo.KeyValue := NULL;
      EdOrdem.Text := IntToStr(Geral.IMV(EdOrdem.Text)+1);
      //RGTipoTexto.ItemIndex := 0;
    end else begin
      EdCodigo.Text := DBEdCodigo.Text;
      EdNome.Text := DBEdNome.Text;
      EdGrupo.Text  := IntToStr(QrCartasCartaG.Value);
      CBGrupo.KeyValue := QrCartasCartaG.Value;
      EdOrdem.Text := IntToStr(QrCartasPagina.Value);
      RGTipoTexto.ItemIndex := QrCartasTipo.Value;
    end;
    EdNome.SetFocus;
  end else begin
    PainelDados.Visible := True;
    PainelEdita.Visible := False;
  end;
  ImgTipo.SQLType := SQLType;
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmCartas.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmCartas.AlteraRegistro;
var
  Cartas : Integer;
begin
  if (QrCartas.State = dsInactive) or (QrCartas.RecordCount = 0) then
    Exit;  
  Cartas := QrCartasCodigo.Value;
  if QrCartasCodigo.Value = 0 then
  begin
    BtAltera.Enabled := False;
    Exit;
  end;
  if not UMyMod.SelLockY(Cartas, Dmod.MyDB, 'Cartas', 'Codigo') then
  begin
    try
      UMyMod.UpdLockY(Cartas, Dmod.MyDB, 'Cartas', 'Codigo');
      MostraEdicao(True, stUpd, 0);
    finally
      Screen.Cursor := Cursor;
    end;
  end;
end;

procedure TFmCartas.IncluiRegistro;
var
  Cursor : TCursor;
begin
  Cursor := Screen.Cursor;
  Screen.Cursor := crHourglass;
  Refresh;
  try
    (*if Length(FormatFloat(FFormatFloat, Cartas))>Length(FFormatFloat) then
    begin
      Geral.MensagemBox(
      'Inclus�o cancelada. Limite de cadastros extrapolado', 'Erro',
      MB_OK+MB_ICONERROR);
      Screen.Cursor := Cursor;
      Exit;
    end;*)
    MostraEdicao(True, stIns, 0);
  finally
    Screen.Cursor := Cursor;
  end;
end;

procedure TFmCartas.QueryPrincipalAfterOpen;
begin
end;

procedure TFmCartas.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmCartas.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmCartas.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmCartas.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmCartas.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmCartas.BtIncluiClick(Sender: TObject);
begin
  IncluiRegistro;
end;

procedure TFmCartas.BtAlteraClick(Sender: TObject);
begin
  AlteraRegistro;
end;

procedure TFmCartas.BtSaidaClick(Sender: TObject);
var
  Codigo: Integer;
begin
  Codigo := QrCartasCodigo.Value;
  //
  VAR_CADASTRO := Codigo;
  VAR_CARTA    := Codigo;
  Close;
end;

procedure TFmCartas.BtConfirmaClick(Sender: TObject);
var
  Codigo : Integer;
  Nome : String;
begin
  Nome := EdNome.Text;
  if Length(Nome) = 0 then
  begin
    Geral.MensagemBox('Defina um t�tulo.', 'Erro', MB_OK+MB_ICONERROR);
    Exit;
  end;
  Dmod.QrUpdU.SQL.Clear;
  if ImgTipo.SQLType = stIns then
    Dmod.QrUpdU.SQL.Add('INSERT INTO cartas SET ')
  else Dmod.QrUpdU.SQL.Add('UPDATE cartas SET ');
  Dmod.QrUpdU.SQL.Add('Titulo=:P0, CartaG=:P1, Pagina=:P2, Tipo=:P3, ');
  //
  if ImgTipo.SQLType = stIns then
  begin
    Dmod.QrUpdU.SQL.Add('DataCad=:Px, UserCad=:Py, Codigo=:Pz');
    Codigo := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
    'Cartas', 'Cartas', 'Codigo');
  end else begin
    Dmod.QrUpdU.SQL.Add('DataAlt=:Px, UserAlt=:Py WHERE Codigo=:Pz');
    Codigo := Geral.IMV(EdCodigo.Text);
  end;
  Dmod.QrUpdU.Params[00].AsString  := Nome;
  Dmod.QrUpdU.Params[01].AsInteger := Geral.IMV(EdGrupo.Text);
  Dmod.QrUpdU.Params[02].AsInteger := Geral.IMV(EdOrdem.Text);
  Dmod.QrUpdU.Params[03].AsInteger := RGTipoTexto.ItemIndex;
  //
  Dmod.QrUpdU.Params[04].AsString  := FormatDateTime(VAR_FORMATDATE, Date);
  Dmod.QrUpdU.Params[05].AsInteger := VAR_USUARIO;
  Dmod.QrUpdU.Params[06].AsInteger := Codigo;
  Dmod.QrUpdU.ExecSQL;
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'Cartas', 'Codigo');
  LocCod(Codigo,Codigo);
  if ImgTipo.SQLType = stIns then
  begin
    BtItensClick(Self);
    EdOrdem.Text := IntToStr(Geral.IMV(EdOrdem.Text)+1);
    MostraEdicao(False, stLok, 0);
  end else begin
    MostraEdicao(False, stLok, 0);
  end;
end;

procedure TFmCartas.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := Geral.IMV(EdCodigo.Text);
  if ImgTipo.SQLType = stIns then UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'Cartas', Codigo);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'Cartas', 'Codigo');
  MostraEdicao(False, stLok, 0);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'Cartas', 'Codigo');
end;

procedure TFmCartas.BtExcluiClick(Sender: TObject);
begin
//
end;

procedure TFmCartas.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType   := stLok;
  FTipo             := 0;
  PainelEdita.Align := alClient;
  PainelDados.Align := alClient;
  RETxtCartas.Align := alClient;
  //
  CriaOForm;
  UnDmkDAC_PF.AbreQuery(QrCartasG, Dmod.MyDB);
end;

procedure TFmCartas.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrCartasCodigo.Value,LaRegistro.Caption);
end;

procedure TFmCartas.SbImprimeClick(Sender: TObject);
begin
  Geral.MB_Aviso('Impress�o n�o dispon�vel para esta janela!');
end;

procedure TFmCartas.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmCartas.SbNovoClick(Sender: TObject);
begin
  Geral.MB_Aviso('Pesquisa n�o dispon�vel para esta janela!');
end;

procedure TFmCartas.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmCartas.QrCartasAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
  BtAltera.Enabled := Geral.IntToBool_0(QrCartasCodigo.Value);
  BtItens.Enabled  := Geral.IntToBool_0(QrCartasCodigo.Value);
end;

procedure TFmCartas.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  Caption := DmkListas.DefineTituloCarta(FTipo, False);
  // Evitar erro no OnCalcFields
  LocCod(QrCartasCodigo.Value, QrCartasCodigo.Value);
end;

procedure TFmCartas.QrCartasAfterScroll(DataSet: TDataSet);
begin
  BtAltera.Enabled := GOTOy.BtEnabled(QrCartasCodigo.Value, False);
  //
  MyObjects.DefineTextoRichEdit(RETxtCartas, QrCartasTexto.Value);
end;

procedure TFmCartas.SbQueryClick(Sender: TObject);
begin
  LocCod(QrCartasCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'Cartas', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmCartas.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], 'TXT-CARTA-001 :: ' +
  DmkListas.DefineTituloCarta(FTipo, True), True, taCenter, 2, 10, 20);
end;

procedure TFmCartas.QrCartasBeforeClose(DataSet: TDataSet);
begin
  RETxtCartas.Text := '';
end;

procedure TFmCartas.QrCartasBeforeOpen(DataSet: TDataSet);
begin
  QrCartasCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmCartas.BtItensClick(Sender: TObject);
begin
  EditaTexto(QrCartasTipo.Value);
end;

procedure TFmCartas.EditaTexto(SubTipo: Integer);
var
  TxtCarta: AnsiString;
  Txt1, Txt2, Txt3, Txt4, Txt5: String;
begin
  Application.CreateForm(TFmTextos, FmTextos);
  FmTextos.FAtributesSize   := 10;
  FmTextos.FAtributesName   := 'Arial';
  FmTextos.FSaveSit         := 3;
  FmTextos.FRichEdit        := RETxtCartas;
  FmTextos.LBItensMD.Sorted := False;
  case FTipo of
    0: // Geral - Carta
    with FmTextos.LBItensMD.Items do
    begin
      Add('[NOME] Nome da entidade');
      Add('[IDADE] Idade da entidade');
      Add('[NASC] Data de nascimento');
      Add('[PRONOME] Pronome de Tratamento');
      Add('[SAUDACAO] Sauda��o inicial');
      Add('[DATA] Data do anivers�rio');
    end;
    1:  // Teach - Contrato
    with FmTextos.LBItensMD.Items do
    begin
      //Add('[IDADE] Idade da entidade');
      //Add('[PRONOME] Pronome de Tratamento');
      //Add('[DATA] Data do anivers�rio');
      //Add('[NASC    ] Data de nascimento');
      //Add('[SAUDACAO] Sauda��o inicial');
      Add('[NOME    ] Nome da entidade');
      Add('[RG      ] Documento de identidade');
      Add('[CPF     ] Cadastro de pessoa f�sica');
      Add('[P_T_LOGR] Rua, avenida, etc...');
      Add('[P_N_LOGR] Nome da rua, avenida, etc...');
      Add('[P_N�MERO] N�mero (endere�o)');
      Add('[P_COMPLE] Complemento de endere�o');
      Add('[P_BAIRRO] Bairro');
      Add('[P_CIDADE] Cidade');
      Add('[P_UF    ] Estado');
      Add('[P_CEP   ] CEP');
      Add('[P_TEL1  ] Telefone 1');
      Add('[CURSOS  ] Cursos e cargas hor�rias');
      Add('[(S)CURSO] Plural cursos');
      Add('[HORARIOS] Tabela de hor�rios');
      Add('[INVESTIM] Investimento no Curso');
      Add('[PARCELAS] Parcelas');
      Add('[HOJE    ] Data de hoje');
      Add('[OA      ] Pronome');
      Add('[_A      ] Pronome');
      Add('[CONTRATO] N�mero do contrato');
    end;
    2:  // LetWear - Contrato
    with FmTextos.LBItensMD.Items do
    begin
      Add('[PRONOME ] Pronome de Tratamento');
      Add('[NASC    ] Data de nascimento');
      Add('[NOME    ] Nome da entidade');
      Add('[RG      ] Documento de identidade');
      Add('[CPF     ] Cadastro de pessoa f�sica');
      Add('[P_T_LOGR] Rua, avenida, etc...');
      Add('[P_N_LOGR] Nome da rua, avenida, etc...');
      Add('[P_N�MERO] N�mero (endere�o)');
      Add('[P_COMPLE] Complemento de endere�o');
      Add('[P_BAIRRO] Bairro');
      Add('[P_CIDADE] Cidade');
      Add('[P_UF    ] Estado');
      Add('[P_CEP   ] CEP');
      Add('[P_TEL1  ] Telefone 1');
      Add('[PARCELAS] Parcelas');
      Add('[HOJE    ] Data de hoje');
      Add('[OA      ] Pronome o');
      Add('[_A      ] Pronome');
      Add('[NOME_PAI] Nome do Pai');
      Add('[NOME_MAE] Nome da M�e');
      Add('[CIDADE_N] Cidade natal');
      Add('[MERCADOR] Mercadorias a locar');
      Add('[VALOR_ME] Valor real da mercadoria');
      Add('[VALOR_TX] Valor por extenso');
      Add('[CONTRATO] N�mero do contrato');
    end;
    3:  // Academy - Contrato
    with FmTextos.LBItensMD.Items do
    begin
      Add('[(S)ATIVI] Plural atividades');
      Add('[C_NOME  ] Nome do contratante (aluno ou respons.)');
      Add('[C_RG_NUM] Documento de identidade do contratante');
      Add('[C_RG_ORG] �rg�o expeditor do RG do contratante');
      Add('[C_RG_DTA] Data de expedi��o do RG do contratante');
      Add('[C_CPF   ] Cadastro de pessoa f�sica do contratante');
      Add('[C_NACION] Nacionalidade do contratante');
      Add('[C_ESTCIV] Estado civil do contratante');
      Add('[C_PROFIS] Profiss�o do contratante');
      Add('[C_MAIL  ] Email do contratante');

      Add('[F_NOME  ] Nome do Favorecido (aluno)');
      Add('[F_NASCIM] Data de nascimento do Favorecido (aluno)');

      Add('[P_T_LOGR] Rua, avenida, etc...');
      Add('[P_N_LOGR] Nome da rua, avenida, etc...');
      Add('[P_N�MERO] N�mero (endere�o)');
      Add('[P_COMPLE] Complemento de endere�o');
      Add('[P_BAIRRO] Bairro');
      Add('[P_CIDADE] Cidade');
      Add('[P_UF    ] Estado');
      Add('[P_CEP   ] CEP');
      Add('[P_TEL1  ] Telefone 1');
      Add('[P_TEL2  ] Telefone 2');
      Add('[P_CELU  ] Celular');
      Add('[CURSOS  ] Atividades e hor�rios');
      Add('[INVESTIM] Investimento');
      Add('[PARCELAS] Parcelas com seus vencimentos e valores');
      Add('[HOJE    ] Data de hoje');
      Add('[OA      ] Pronome');
      Add('[_A      ] Pronome');
      Add('[CONTRATO] N�mero do contrato');
      Add('[HORARIOS] Turmas com seus hor�rios e atividades');
      Add('[PERIODO ] Data inicia e final das atividades');
    end;
    4:  // Cred itor - Contrato
    with FmTextos.LBItensMD.Items do
    begin
      case QrCartasTipo.Value of
        5:
        begin
          Add('[NOME_SAC] Nome do sacado');
          Add('[TIPD_SAC] Tipo de doc. sacado (CNPJ/CPF)');
          Add('[DOCU_SAC] N�mero do doc. do sacado');
          Add('[ENDE_SAC] Endere�o do sacado');
          Add('[COMARCAD] Comarca de ...');
        end;
        6:
        begin
          Add('[NOME_EMIT] Nome do emitente');
          Add('[CHEQEMIT] Descri��o dos cheques do emitente');
        end
        else begin
          Add('[LIMITE  ] Limite de fian�a (Contrato)');
          Add('[NUM_CONT] N�mero do contrato');
          Add('[ADITIVO ] Aditivo de contrato');
          Add('[DATA_CON] Data do contrato');
          Add('[MEMPRESA] Nome da empresa');//+DmodG.QrDonoNomeDono.Value);
          Add('[NOME_CLI] Nome do Cliente');
          Add('[NOME_SAC] Nome do sacado');
        end;
      end;
      Add('[PLU1_TIT] Plural de T�tulo(s)');
      Add('[PLU2_TIT] Plural de t�tulos i(ram)');
      Add('[PLU3_TIT] Plural de t�tulos �(�o)');
      Add('[PLU4_TIT] Plural de t�tulos (em)');
      Add('[DUPLISAC] Descri��o das dupl. do sacado');
      Add('[PLU5_TIT] Plural de t�tulos l(is)');
      Add('[PLU6_TIT] Plural de t�tulos �o(�es)');
      Add('[PLU7_TIT] Plural de t�tulos (es)');
    end;
    5:  // IRent - Contrato de loca��o imobili�ria
    with FmTextos.LBItensMD.Items do
    begin
      Add('[PERIODOT] Tipo de per�odo (meses, sem. ou dias)');
      Add('[PERIODOQ] Quantidade de per�odos (Ex: 12 (meses)');
      Add('[ALUGUELV] Valor do aluguel por per�odo');
      Add('[DIAPAGTO] Dia do pagamento no per�odo');
      Add('[CARENCIA] Car�ncia em dias para o pagamento');
      Add('[FIMCAREN] Dia do m�s final da car�ncia');
      Add('[OA    LT] Pronome "O" se masculino ou "A" se feminino - locat�rio');
      Add('[_A    LT] "" se Masculino ou pronome "A" se feminino - locat�rio');
      Add('[oa    LT] Pronome "o" se masculino ou "a" se feminino - locat�rio');
      Add('[_a    LC] "" se Masculino ou pronome "a" se feminino - locador');
      Add('[_a    LT] "" se Masculino ou pronome "a" se feminino - locat�rio');
      Add('[ao_�  LT] "ao" se Masculino ou "�" se feminino - locat�rio');
      Add('[AO_�  LT] "AO" se Masculino ou "�" se feminino - locat�rio');
      Add('[_es   FD] "" se singular ou "es" se plural - fiador(es)');
      Add('[_ES   FD] "" se singular ou "ES" se plural - fiador(es)');
      Add('[O_OS  FD] "O" se singular ou "OS" se plural - fiador(es)');
      Add('[o_os  FD] "o" se singular ou "os" se plural - fiador(es)');
      Add('[a_as  FD] "a" se singular ou "as" se plural - fiador(es)');
      Add('[MULT_MOR] Multa morat�ria');
      Add('[MULT_DIA] Dia do m�s da multa morat. sobre o n�o pag. do aluguel');
      Add('[MULT_QCO] Multa contratual (quebra de contrato)');
      Add('[JURO_MOR] Juros morat�rios mensais');
      Add('[ADVO_ADM] Honor�rios advocat�cios administrativos');
      Add('[ADVO_JUD] Honor�rios advocat�cios judiciais');
      Add('[ADVO_QCO] Honor�rios advocat�cios na quebra de contrato');
      Add('[BONIFVAL] Bonifica��o do aluguel para pagamento em dia');
      Add('[FIADOR_1] Nome do primeiro fiador');
      Add('[FIADOR_2] Nome do segundo fiador');
      Add('[_e    FD] " e " caso fiadores, "" caso fiador');
      Add('[_m    FD] "m" caso fiadores, "" caso fiador');
      Add('[l_is  FD] "is" caso fiadores, "l" caso fiador');
      Add('[Sr.   FD] Pronome de tratamento - fiador 1');
      Add('[NUM_CONT] N�mero do contrato (informado)');
      Add('[NUM_CTRL] N�mero do contrato (controle)');
      Add('[DATA_CON] Data inicial do contrato');
      Add('[DATA_COF] Data final do contrato - descritiva');
      Add('[DESC_CON] Data inicial do contrato descritiva');
      Add('[DESC_COF] Data final do contrato');
      Add('[CIDADE_O] Cidade (cadastrada em op��es)');
      Add('[HOJE    ] Data de hoje');
      Add('[LOCADOR ] Nome do locador');
      Add('[LOCATARI] Nome do locat�rio');
      Add('[FIADOR?1] Foi definido o fiador 1?');
      Add('[FIADOR?2] Foi definido o fiador 2?');

      (*Add('[MEMPRESA] '+DmodG.QrDonoNomeDono.Value);
      Add('[NOME_CLI] Nome do Cliente');
      //
      Add('[MODALIDA] Modalidade de loca��o');
      Add('[PRONOME ] Pronome de Tratamento');
      Add('[NASC    ] Data de nascimento');
      Add('[NOME    ] Nome da entidade');
      Add('[RG      ] Documento de identidade');
      Add('[CPF     ] Cadastro de pessoa f�sica');
      Add('[P_T_LOGR] Rua, avenida, etc...');
      Add('[P_N_LOGR] Nome da rua, avenida, etc...');
      Add('[P_N�MERO] N�mero (endere�o)');
      Add('[P_COMPLE] Complemento de endere�o');
      Add('[P_BAIRRO] Bairro');
      Add('[P_CIDADE] Cidade');
      Add('[P_UF    ] Estado');
      Add('[P_CEP   ] CEP');
      Add('[P_TEL1  ] Telefone 1');
      Add('[PARCELAS] Parcelas');
      Add('[NOME_PAI] Nome do Pai');
      Add('[NOME_MAE] Nome da M�e');
      Add('[MERCADOR] Mercadorias a locar');
      Add('[VALOR_ME] Valor real da mercadoria');
      Add('[VALOR_TX] Valor por extenso');*)
    end;
    6:  // Syndic (Synker?) - Certid�o negativa de d�bitos condominiais
    begin
      Txt1 := DModG.ReCaptionTexto(VAR_P_R_O_P_R_I_E_T_A_R_I_O);
      Txt2 := DModG.ReCaptionTexto(VAR_U_H);
      Txt3 := DModG.ReCaptionTexto(VAR_M_O_R_A_D_O_R);
      Txt4 := DModG.ReCaptionTexto(VAR_C_O_N_D_O_M_I_N_I_O);
      Txt5 := DModG.ReCaptionTexto(VAR_U_H_LONGO);
      with FmTextos.LBItensMD.Items do
      begin
        case SubTipo of
          01: // Certid�o negativa de d�bitos condominiais
          begin
            Add('[DATA_NEG] Data da negativa de d�bitos');
            Add('[PROPNOME] Nome do(a) ' + Txt1  + ' da ' + Txt2);
            Add('[PROP_DOC] CNPJ/CPF do(a) ' + Txt1 + ' da ' + Txt2);
            Add('[USU_NOME] Nome do(a) ' + Txt3 + ' da ' + Txt2);
            Add('[USU__DOC] CNPJ/CPF do(a) ' + Txt3 + ' da ' + Txt2);
          end;
          02: // Carta de cobran�a de d�bitos condominiais
          begin
            Add('[LDEBITOS] Lista de d�bitos e suas totaliza��es');
          end;
        end;
        Add('[CONDOMIN] Nome do ' + Txt4);
        Add('[UNIDADE ] ' + Txt5 + ' (' + Txt2  + ')');
        Add('[ADMINIST] Nome da minha empresa (administradora)');
        Add('[UN_SENHA] Senha para logar no site');
        Add('[UN_LOGIN] Login para logar no site');
        //
        Add('[COND_END]  Endere�o do condom�nio');
        Add('[PROP_END]  Endere�o do propriet�rio');
        Add('[PROP_ENDP] Endere�o parcial do propriet�rio');
        Add('[PROP_CEP]  CEP do propriet�rio');
        Add('[PROP_MUN]  Cidade do endere�o do propriet�rio');
        Add('[PROP_UF]   UF do endere�o do propriet�rio');
        Add('[SIND_NOME] Nome do s�ndico');
      end;
    end;
    7:  // LeSew - Carta de avalia��o
    with FmTextos.LBItensMD.Items do
    begin
      Add('[CIDADE] Cidade');
      Add('[COR] Cor');
      Add('[DATAATUAL] Data atual');
      Add('[ENTIDADE] Nome da entidade');
      Add('[MODELO] Modelo');
      Add('[PERCALUNO] Percentual de lucro aluno');
      Add('[QUANTIDADE] Quantidade');
      Add('[TAMANHO] Tamanho');
      Add('[TECIDO] Tecido');
      Add('[TEXTO] Texto');
      Add('[USUARIO] Usu�rio');
      Add('[VALORCAMEN] Valor total do or�amento');
      Add('[VALORPGTO] Valor do pagamento');
    end;
    8:  // Synker - Certid�o negativa de d�bitos condominiais
    with FmTextos.LBItensMD.Items do
    begin
      Add('[DATA_NEG] Data da negativa de d�bitos');
      Add('[PROPNOME] Nome do(a) ' + Txt1  + ' da ' + Txt2);
      Add('[PROP_DOC] CNPJ/CPF do(a) ' + Txt1  + ' da ' + Txt2);
      Add('[USU_NOME] Nome do(a) ' + Txt3  + ' da ' + Txt2);
      Add('[USU__DOC] CNPJ/CPF do(a) ' + Txt3  + ' da ' + Txt2);
      Add('[LDEBITO1] Lista de d�bitos e suas totaliza��es (Com o n� do bloqueto)');
      Add('[LDEBITO2] Lista de d�bitos e suas totaliza��es (Sem o n� do bloqueto)');
      Add('[CONDOMIN] Nome do ' + Txt4);
      Add('[UNIDADE ] ' + Txt5 + ' (' + Txt2  + ')');
      Add('[ADMINIST] Nome da minha empresa (administradora)');
      Add('[CONDENDE] Endere�o do ' + Txt4);
      Add('[PROPENDE] Endere�o do(a) ' + Txt1  + ' da ' + Txt1);
    end;
    9:  // ToolRent - Contrato de Loca��o
    with FmTextos.LBItensMD.Items do
    begin
      //  Fazer
    end;
  end;
  FmTextos.LBItensMD.Sorted := True;
  MyObjects.TextoEntreRichEdits(RETxtCartas, FmTextos.Editor);
  FmTextos.ShowModal;
  FmTextos.Destroy;
  //
  TxtCarta := MyObjects.ObtemTextoRichEdit(Self, RETxtCartas);
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'cartas', False, ['Texto'],
    ['Codigo'], [TxtCarta], [QrCartasCodigo.Value], False)
  then
    LocCod(QrCartasCodigo.Value, QrCartasCodigo.Value);
end;

procedure TFmCartas.QrCartasAfterClose(DataSet: TDataSet);
begin
  BtAltera.Enabled := False;
  BtItens.Enabled := False;
end;

procedure TFmCartas.DefineTipoTexto;
var
  Texto, Antes: String;
  Conta: Integer;
begin
  Conta := -1;
  RGTipoTexto.Items.Clear;
  Texto := '?';
  Antes := '';
  while (Texto <> 'Desconhecido') and (Texto <> Antes) do
  begin
    Antes := Texto;
    Conta := Conta + 1;
    Texto := DmkListas.TipoTextoCarta(FTipo, Conta);
    if Texto <> Antes then
      RGTipoTexto.Items.Add(Texto);
  end;
  RGTipoTexto.Columns := Conta div 3 + 1;
end;

procedure TFmCartas.QrCartasCalcFields(DataSet: TDataSet);
begin
  QrCartasNOMETIPO.Value := DmkListas.TipoTextoCarta(FTipo, QrCartasTipo.Value);
end;

end.

