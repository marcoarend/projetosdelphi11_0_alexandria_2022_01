unit CartasEnv;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, frxClass, frxDBSet, mySQLDbTables, dmkGeral, UnDmkProcFunc,
  ComCtrls, Grids, DBGrids, dmkDBGrid, ExtCtrls, StdCtrls, Buttons, dmkImage, UnDmkEnums;

type
  TFmCartasEnv = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    Panel1: TPanel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    BtSaida: TBitBtn;
    DBGrid1: TDBGrid;
    PB1: TProgressBar;
    QrLotImp: TmySQLQuery;
    DsLotImp: TDataSource;
    QrLotImpVencimento: TDateField;
    QrLotImpMez: TIntegerField;
    QrLotImpDepto: TIntegerField;
    QrLotImpFatNum: TFloatField;
    QrLotImpCadastro: TIntegerField;
    QrLotImpConsidEnvi: TDateTimeField;
    QrLotImpPropriet: TIntegerField;
    QrLotImpEmprEnti: TIntegerField;
    QrLotImpMEZ_TXT: TWideStringField;
    QrLotImpENVIADO: TLargeintField;
    QrLotImpNO_PRP: TWideStringField;
    QrLotImpNO_EMP: TWideStringField;
    QrLotImpDEPTO_TXT: TWideStringField;
    frxCNDC: TfrxReport;
    frxDsUnidades: TfrxDBDataset;
    QrUnidades: TmySQLQuery;
    QrUnidadesVALOR: TFloatField;
    QrUnidadesNOME_CON: TWideStringField;
    QrUnidadesNOME_PRP: TWideStringField;
    QrUnidadesUnidade: TWideStringField;
    QrUnidadesDepto: TIntegerField;
    QrUnidadesDOC_PRP: TWideStringField;
    QrUnidadesNOMEUSU: TWideStringField;
    QrUnidadesDOC_USU: TWideStringField;
    QrUnidadesConta: TIntegerField;
    QrUnidadesCOD_PRP: TIntegerField;
    QrUnidadesCOD_CON: TIntegerField;
    QrUnidadesCEP_PRP: TFloatField;
    QrUnidadesUF_PRP: TFloatField;
    QrUnidadesMUNI_PRP: TWideStringField;
    QrUnidadesCliInt: TIntegerField;
    QrInad: TmySQLQuery;
    QrInadMES: TWideStringField;
    QrInadData: TDateField;
    QrInadFatNum: TFloatField;
    QrInadVencimento: TDateField;
    QrInadMez: TIntegerField;
    QrInadDescricao: TWideStringField;
    QrInadCREDITO: TFloatField;
    QrInadPAGO: TFloatField;
    QrInadJuros: TFloatField;
    QrInadMulta: TFloatField;
    QrInadTOTAL: TFloatField;
    QrInadSALDO: TFloatField;
    QrInadPEND_VAL: TFloatField;
    QrInadVALOR: TFloatField;
    QrInadATZ_VAL: TFloatField;
    frxDsInad: TfrxDBDataset;
    frxCDCU: TfrxReport;
    frxCarta: TfrxReport;
    frxDsCartas: TfrxDBDataset;
    QrTexto: TmySQLQuery;
    QrTextoCodigo: TIntegerField;
    QrTextoTitulo: TWideStringField;
    QrTextoTexto: TWideMemoField;
    QrTextoTipo: TIntegerField;
    QrLotImpCodigo: TIntegerField;
    QrLotImpDiarCEMIts: TIntegerField;
    QrLotImpMetodo: TSmallintField;
    BtTodos: TBitBtn;
    BtNenhum: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure DBGrid1DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure DBGrid1CellClick(Column: TColumn);
    procedure BtTodosClick(Sender: TObject);
    procedure BtNenhumClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
  private
    { Private declarations }
    FLotImp: Int64;
    FConsidEnvi: String;
    //
    procedure ConfirmaTodos(ConsidEnvi: String);
  public
    { Public declarations }
    procedure ReopenLastLotImp(LotImp: Int64; Codigo, DiarCEMIts,
              Metodo: Integer);
  end;

var
  FmCartasEnv: TFmCartasEnv;

implementation

uses Module, UnGOTOy, ModuleGeral, UnMyObjects, DmkDAC_PF, MyVCLSkin,
UnDiario_Tabs, UnFinanceiro, Meufrx, UMySQLModule, UnInternalConsts, Principal;

{$R *.dfm}

procedure TFmCartasEnv.BtNenhumClick(Sender: TObject);
begin
  ConfirmaTodos('0000-00-00');
end;

procedure TFmCartasEnv.BtSaidaClick(Sender: TObject);
begin
  DModG.VerificaCartasAImprimir(FmPrincipal.GB_Cartas,
    FmPrincipal.LaCartas1A, FmPrincipal.LaCartas1B, FmPrincipal.Lacartas1C);
  //
  Close;
end;

procedure TFmCartasEnv.BtTodosClick(Sender: TObject);
begin
  ConfirmaTodos(FConsidEnvi);
end;

procedure TFmCartasEnv.ConfirmaTodos(ConsidEnvi: String);
var
  Codigo, DiarCEMIts, Metodo, LastlotImp: Integer;
begin
  Codigo     := QrLotImpCodigo.Value;
  DiarCEMIts := QrLotImpDiarCEMIts.Value;
  Metodo     := QrLotImpMetodo.Value;
  LastlotImp := FLotImp;
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'diarcemenv', False, [
  'ConsidEnvi'], ['LastLotImp'], [
  ConsidEnvi], [LastlotImp], True) then
    ReopenLastLotImp(FLotImp, Codigo, DiarCEMIts, Metodo);
end;

procedure TFmCartasEnv.DBGrid1CellClick(Column: TColumn);
var
  ConsidEnvi: String;
  Codigo, DiarCEMIts, Metodo: Integer;
begin
  if Column.FieldName = 'ENVIADO' then
  begin
    if QrLotImpENVIADO.Value = 0 then
      ConsidEnvi := FConsidEnvi
    else
      ConsidEnvi := '0000-00-00';
    //
    Codigo     := QrLotImpCodigo.Value;
    DiarCEMIts := QrLotImpDiarCEMIts.Value;
    Metodo     := QrLotImpMetodo.Value;
    //
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'diarcemenv', False, [
    'ConsidEnvi'], [
    'Codigo', 'DiarCEMIts', 'Metodo'], [
    ConsidEnvi], [
    Codigo, DiarCEMIts, Metodo], True) then
      ReopenLastLotImp(FLotImp, Codigo, DiarCEMIts, Metodo);
  end;
end;

procedure TFmCartasEnv.DBGrid1DrawColumnCell(Sender: TObject; const Rect: TRect;
  DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
  if Column.FieldName = 'ENVIADO' then
    MeuVCLSkin.DrawGrid(DBGrid1, Rect, 1, QrLotImpENVIADO.Value);
end;

procedure TFmCartasEnv.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmCartasEnv.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  FConsidEnvi := Geral.FDT(DModG.ObtemAgora(), 109);
end;

procedure TFmCartasEnv.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmCartasEnv.ReopenLastLotImp(LotImp: Int64; Codigo, DiarCEMIts,
Metodo: Integer);
begin
  FLotImp := LotImp;
  UnDmkDAC_PF.AbreMySQLQuery0(QrLotImp, Dmod.MyDB, [
  'SELECT env.Codigo, env.DiarCEMIts, env.Metodo,',
  'bol.Vencimento, bol.Mez, bol.Depto,  ',
  'bol.FatNum, env.Cadastro, ',
  'env.ConsidEnvi, bol.Propriet, bol.EmprEnti, ',
  'CONCAT(LPAD(MOD(bol.Mez, 100), 2, "0"), "/",  ',
  'LPAD(TRUNCATE(bol.Mez/ 100, 0), 2, "0")) MEZ_TXT, ',
  'IF(env.ConsidEnvi >"1900-01-01", 1, 0) ENVIADO, ',
  'IF(prp.Tipo=0, prp.RazaoSocial, prp.Nome) NO_PRP, ',
  'IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NO_EMP, ',
  'imv.Unidade DEPTO_TXT ',
  'FROM diarcemenv env ',
  'LEFT JOIN diarcembol bol ON bol.Codigo=env.Codigo ',
  'LEFT JOIN condimov imv ON imv .Conta=bol.Depto ',
  'LEFT JOIN entidades prp ON prp .Codigo=bol.Propriet ',
  'LEFT JOIN entidades emp ON emp .Codigo=bol.EmprEnti ',
  'WHERE env.LastLotImp=' + Geral.FI64(LotImp),
  '']);
  //
  QrlotImp.Locate('Codigo;DiarCEMIts;Metodo',
    VarArrayOf([Codigo, DiarCEMIts, Metodo]), []);
end;

{function TFmCartasImp.IncorporaQry(Dest, Source: TmySQLQuery): Boolean;
var
  I, J: Integer;
  Achou: Boolean;
begin
  Achou := False;
  for I := 0 to Dest.Fields.Count - 1 do
  begin
    Achou := False;
    for J := 0 to Source.Fields.Count - 1 do
    begin
      if Lowercase(Dest.Fields[I].FieldName) =
      Lowercase(Source.Fields[I].FieldName) then
      begin
        Dest.Fields[I] := Source.Fields[J];
        Achou := True;
      end;
    end;
    if not Achou then
    begin
      Result := False;
      Geral.MB_Erro('Campo incorporador n�o lozalizado!' + sLineBreak +
      'Campo: ' + Dest.Fields[I].FieldName + sLineBreak +
      'Tabela: ' + Dest.Name);
      Exit;
    end;
  end;
  Dest := Source;
  Result := True;
end;
}

end.
