unit WTextosOpc;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkDBLookupComboBox,
  dmkEditCB, mySQLDbTables;

type
  TFmWTextosOpc = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    GroupBox33: TGroupBox;
    Label178: TLabel;
    SbNFSeCertDigital: TSpeedButton;
    EdCertDigital: TdmkEdit;
    GroupBox3: TGroupBox;
    Label2: TLabel;
    EdDirWebPad: TdmkEditCB;
    CBDirWebPad: TdmkDBLookupComboBox;
    QrFTPWebDir: TmySQLQuery;
    QrFTPWebDirCodigo: TIntegerField;
    QrFTPWebDirNome: TWideStringField;
    DsFTPWebDir: TDataSource;
    QrWTextosOpc: TmySQLQuery;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNFSeCertDigitalClick(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  var
  FmWTextosOpc: TFmWTextosOpc;

implementation

uses UnMyObjects, Module, MyDBCheck, CapicomListas, UMySQLModule;

{$R *.DFM}

procedure TFmWTextosOpc.BtOKClick(Sender: TObject);
var
  Codigo, DirWebPad: Integer;
  CertDigital: String;
begin
  Codigo      := QrWTextosOpc.FieldByName('Codigo').AsInteger;
  DirWebPad   := EdDirWebPad.ValueVariant;
  CertDigital := EdCertDigital.ValueVariant;
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpdN, stUpd, 'wtextosopc', False,
    ['CertDigital', 'DirWebPad'], ['Codigo'],
    [CertDigital, DirWebPad], [Codigo], True)
  then
    Close;
end;

procedure TFmWTextosOpc.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmWTextosOpc.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmWTextosOpc.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  UMyMod.AbreQuery(QrFTPWebDir, DMod.MyDBn);
  UMyMod.AbreQuery(QrWTextosOpc, DMod.MyDBn);
  //
  EdCertDigital.ValueVariant := QrWTextosOpc.FieldByName('CertDigital').AsString;
  EdDirWebPad.ValueVariant   := QrWTextosOpc.FieldByName('DirWebPad').AsInteger;
  CBDirWebPad.KeyValue       := QrWTextosOpc.FieldByName('DirWebPad').AsInteger;
end;

procedure TFmWTextosOpc.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmWTextosOpc.SbNFSeCertDigitalClick(Sender: TObject);
var
  CertDigital: String;
begin
  CertDigital := '';
  //
  if DBCheck.CriaFm(TFmCapicomListas, FmCapicomListas, afmoSoBoss) then
  begin
    FmCapicomListas.ShowModal;
    //
    if FmCapicomListas.FSelected then
      CertDigital := FmCapicomListas.FSerialNumber;
    //
    FmCapicomListas.Destroy;
  end;
  EdCertDigital.ValueVariant := CertDigital;
end;

end.
