object FmCartasEnv: TFmCartasEnv
  Left = 0
  Top = 0
  Caption = 'CIA-GEREN-003 :: Confirma'#231#227'o de Impress'#227'o de Cartas'
  ClientHeight = 658
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 912
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 453
        Height = 32
        Caption = 'Confirma'#231#227'o de Impress'#227'o de Cartas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 453
        Height = 32
        Caption = 'Confirma'#231#227'o de Impress'#227'o de Cartas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 453
        Height = 32
        Caption = 'Confirma'#231#227'o de Impress'#227'o de Cartas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 528
    Width = 1008
    Height = 60
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 1
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 43
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object PB1: TProgressBar
        Left = 0
        Top = 26
        Width = 1004
        Height = 17
        Align = alBottom
        TabOrder = 0
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 588
    Width = 1008
    Height = 70
    Align = alBottom
    TabOrder = 2
    object PnSaiDesis: TPanel
      Left = 862
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 860
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtTodos: TBitBtn
        Tag = 127
        Left = 5
        Top = 0
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Hint = 'Sai da janela atual'
        Caption = '&Todos'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtTodosClick
      end
      object BtNenhum: TBitBtn
        Tag = 128
        Left = 129
        Top = 0
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Hint = 'Sai da janela atual'
        Caption = '&Nenhum'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        OnClick = BtNenhumClick
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 1008
    Height = 480
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 3
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 1008
      Height = 480
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 1008
        Height = 480
        Align = alClient
        TabOrder = 0
        object DBGrid1: TDBGrid
          Left = 2
          Top = 15
          Width = 1004
          Height = 463
          Align = alClient
          DataSource = DsLotImp
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'Tahoma'
          TitleFont.Style = []
          OnCellClick = DBGrid1CellClick
          OnDrawColumnCell = DBGrid1DrawColumnCell
          Columns = <
            item
              Expanded = False
              FieldName = 'ENVIADO'
              Title.Caption = 'Ok'
              Width = 17
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'Vencimento'
              Title.Alignment = taCenter
              Title.Caption = 'Vencto'
              Width = 51
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'MEZ_TXT'
              Title.Alignment = taCenter
              Title.Caption = 'M'#234's'
              Width = 36
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'FatNum'
              Title.Caption = 'Boleto'
              Width = 84
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DEPTO_TXT'
              Title.Caption = 'UH'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Cadastro'
              Title.Caption = 'Carta'
              Width = 32
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_PRP'
              Title.Caption = 'Propriet'#225'rio'
              Width = 280
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_EMP'
              Title.Caption = 'Condom'#237'nio'
              Width = 280
              Visible = True
            end>
        end
      end
    end
  end
  object QrLotImp: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT env.Codigo, env.DiarCEMIts, env.Metodo,'
      'bol.Vencimento, bol.Mez, bol.Depto,  '
      'bol.FatNum, env.Cadastro, '
      'env.ConsidEnvi, bol.Propriet, bol.EmprEnti, '
      'CONCAT(LPAD(MOD(Mez, 100), 2, "0"), "/",  '
      'LPAD(TRUNCATE(Mez/ 100, 0), 2, "0")) MEZ_TXT, '
      'IF(env.ConsidEnvi >"1900-01-01", 1, 0) ENVIADO, '
      'IF(prp.Tipo=0, prp.RazaoSocial, prp.Nome) NO_PRP, '
      'IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NO_EMP, '
      'imv.Unidade DEPTO_TXT '
      'FROM diarcemenv env '
      'LEFT JOIN diarcembol bol ON bol.Codigo=env.Codigo '
      'LEFT JOIN condimov imv ON imv .Conta=bol.Depto '
      'LEFT JOIN entidades prp ON prp .Codigo=bol.Propriet '
      'LEFT JOIN entidades emp ON emp .Codigo=bol.EmprEnti '
      'WHERE env.LastLotImp>0  ')
    Left = 608
    Top = 36
    object QrLotImpVencimento: TDateField
      FieldName = 'Vencimento'
      Origin = 'diarcembol.Vencimento'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrLotImpMez: TIntegerField
      FieldName = 'Mez'
      Origin = 'diarcembol.Mez'
    end
    object QrLotImpDepto: TIntegerField
      FieldName = 'Depto'
      Origin = 'diarcembol.Depto'
    end
    object QrLotImpFatNum: TFloatField
      FieldName = 'FatNum'
      Origin = 'diarcembol.FatNum'
    end
    object QrLotImpCadastro: TIntegerField
      FieldName = 'Cadastro'
      Origin = 'diarcemenv.Cadastro'
    end
    object QrLotImpConsidEnvi: TDateTimeField
      FieldName = 'ConsidEnvi'
      Origin = 'diarcemenv.ConsidEnvi'
    end
    object QrLotImpPropriet: TIntegerField
      FieldName = 'Propriet'
      Origin = 'diarcembol.Propriet'
    end
    object QrLotImpEmprEnti: TIntegerField
      FieldName = 'EmprEnti'
      Origin = 'diarcembol.EmprEnti'
    end
    object QrLotImpMEZ_TXT: TWideStringField
      FieldName = 'MEZ_TXT'
      Size = 5
    end
    object QrLotImpENVIADO: TLargeintField
      FieldName = 'ENVIADO'
      Required = True
    end
    object QrLotImpNO_PRP: TWideStringField
      FieldName = 'NO_PRP'
      Size = 100
    end
    object QrLotImpNO_EMP: TWideStringField
      FieldName = 'NO_EMP'
      Size = 100
    end
    object QrLotImpDEPTO_TXT: TWideStringField
      FieldName = 'DEPTO_TXT'
      Origin = 'condimov.Unidade'
      Size = 10
    end
    object QrLotImpCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrLotImpDiarCEMIts: TIntegerField
      FieldName = 'DiarCEMIts'
    end
    object QrLotImpMetodo: TSmallintField
      FieldName = 'Metodo'
    end
  end
  object DsLotImp: TDataSource
    DataSet = QrLotImp
    Left = 636
    Top = 36
  end
  object frxCNDC: TfrxReport
    Version = '4.14'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39815.746004548600000000
    ReportOptions.LastChange = 39815.746004548600000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'procedure PageHeader1OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  if <MeuLogoExiste> = True then'
      '    Picture1.LoadFromFile(<MeuLogoCaminho>);'
      'end;'
      ''
      'begin'
      'end.')
    Left = 356
    Top = 212
    Datasets = <
      item
        DataSet = frxDsCartas
        DataSetName = 'frxDsCartas'
      end
      item
      end
      item
      end
      item
        DataSet = frxDsInad
        DataSetName = 'frxDsInad'
      end
      item
        DataSet = frxDsUnidades
        DataSetName = 'frxDsUnidades'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object DetailData1: TfrxDetailData
        Height = 20.000000000000000000
        Top = 158.740260000000000000
        Width = 718.110700000000000000
        Columns = 1
        ColumnWidth = 200.000000000000000000
        ColumnGap = 20.000000000000000000
        DataSet = frxDsCartas
        DataSetName = 'frxDsCartas'
        RowCount = 0
        Stretched = True
        object Rich1: TfrxRichView
          Left = 2.629870000000000000
          Width = 706.929190000000000000
          Height = 20.000000000000000000
          ShowHint = False
          StretchMode = smMaxHeight
          DataField = 'Texto'
          DataSet = frxDsCartas
          DataSetName = 'frxDsCartas'
          GapX = 2.000000000000000000
          GapY = 1.000000000000000000
          RichEdit = {
            7B5C727466315C616E73695C616E7369637067313235325C64656666305C6465
            666C616E67313034367B5C666F6E7474626C7B5C66305C666E696C204D532053
            616E732053657269663B7D7D0D0A7B5C636F6C6F7274626C203B5C726564305C
            677265656E305C626C7565303B7D0D0A7B5C2A5C67656E657261746F72204D73
            66746564697420352E34312E32312E323531303B7D5C766965776B696E64345C
            7563315C706172645C6366315C66305C667331365C7061720D0A7D0D0A00}
        end
      end
      object DadosMestre1: TfrxMasterData
        Height = 115.118120000000000000
        Top = 18.897650000000000000
        Width = 718.110700000000000000
        Columns = 1
        ColumnWidth = 200.000000000000000000
        ColumnGap = 20.000000000000000000
        DataSet = frxDsUnidades
        DataSetName = 'frxDsUnidades'
        RowCount = 0
        StartNewPage = True
        object Memo1: TfrxMemoView
          Top = 0.000000000000000014
          Width = 196.440630000000000000
          Height = 115.118120000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo154: TfrxMemoView
          Left = 196.535433070000000000
          Top = 0.000000000000000014
          Width = 521.480210000000000000
          Height = 115.118120000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo155: TfrxMemoView
          Left = 199.606370000000000000
          Top = 1.889763780000000000
          Width = 509.291280000000000000
          Height = 18.897637800000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
        end
        object Memo157: TfrxMemoView
          Left = 241.181200000000000000
          Top = 20.787401570000000000
          Width = 168.000000000000000000
          Height = 15.118110240000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsDono."CNPJ_TXT"]')
          ParentFont = False
        end
        object Memo158: TfrxMemoView
          Left = 199.858380000000000000
          Top = 20.787401570000000000
          Width = 41.322820000000000000
          Height = 15.118110240000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'CNPJ:')
          ParentFont = False
        end
        object Memo159: TfrxMemoView
          Left = 541.212740000000000000
          Top = 20.787401570000000000
          Width = 168.000000000000000000
          Height = 15.118110240000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsDono."IE_RG"]')
          ParentFont = False
        end
        object Memo160: TfrxMemoView
          Left = 515.008040000000000000
          Top = 20.787401570000000000
          Width = 26.204700000000000000
          Height = 15.118110240000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'I.E.:')
          ParentFont = False
        end
        object Memo163: TfrxMemoView
          Left = 252.519790000000000000
          Top = 35.905511810000000000
          Width = 456.377860000000000000
          Height = 15.118110240000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsDono."E_LNR"]')
          ParentFont = False
        end
        object Memo164: TfrxMemoView
          Left = 357.149660000000000000
          Top = 51.023622050000000000
          Width = 275.779530000000000000
          Height = 15.118110240000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsDono."Cidade"]')
          ParentFont = False
        end
        object Memo165: TfrxMemoView
          Left = 312.047310000000000000
          Top = 51.023622050000000000
          Width = 45.102350000000000000
          Height = 15.118110240000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'Cidade: ')
          ParentFont = False
        end
        object Memo166: TfrxMemoView
          Left = 678.031540000000000000
          Top = 51.023622050000000000
          Width = 32.000000000000000000
          Height = 15.118110240000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsDono."NOMEUF"]')
          ParentFont = False
        end
        object Memo167: TfrxMemoView
          Left = 632.929190000000000000
          Top = 51.023622050000000000
          Width = 45.102350000000000000
          Height = 15.118110240000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'Estado: ')
          ParentFont = False
        end
        object Memo168: TfrxMemoView
          Left = 233.622140000000000000
          Top = 51.023622050000000000
          Width = 78.425170000000000000
          Height = 15.118110240000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsDono."ECEP_TXT"]')
          ParentFont = False
        end
        object Memo13: TfrxMemoView
          Left = 199.858380000000000000
          Top = 35.905511810000000000
          Width = 52.661410000000000000
          Height = 15.118110240000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'Endere'#231'o:')
          ParentFont = False
        end
        object Memo14: TfrxMemoView
          Left = 199.858380000000000000
          Top = 51.023622050000000000
          Width = 33.763760000000000000
          Height = 15.118110240000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'CEP:')
          ParentFont = False
        end
        object Memo169: TfrxMemoView
          Left = 252.519790000000000000
          Top = 66.141732280000000000
          Width = 237.574830000000000000
          Height = 15.118110240000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsDono."TE1_TXT"]')
          ParentFont = False
        end
        object Memo170: TfrxMemoView
          Left = 199.858380000000000000
          Top = 66.141732280000000000
          Width = 52.661410000000000000
          Height = 15.118110240000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'Telefone:')
          ParentFont = False
        end
        object Memo171: TfrxMemoView
          Left = 516.299320000000000000
          Top = 66.141732280000000000
          Width = 193.795300000000000000
          Height = 15.118110240000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsDono."FAX_TXT"]')
          ParentFont = False
        end
        object Memo172: TfrxMemoView
          Left = 490.094620000000000000
          Top = 66.141732280000000000
          Width = 26.204700000000000000
          Height = 15.118110240000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'Fax:')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          Left = 237.858380000000000000
          Top = 83.149660000000000000
          Width = 471.905690000000000000
          Height = 15.118110240000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsControle."Web_MyURL"]')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          Left = 200.315090000000000000
          Top = 83.149660000000000000
          Width = 37.543290000000000000
          Height = 15.118110240000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'Portal:')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          Left = 237.858380000000000000
          Top = 98.267780000000000000
          Width = 471.905690000000000000
          Height = 15.118110240000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsDono."EMail"]')
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          Left = 200.315090000000000000
          Top = 98.267780000000000000
          Width = 37.543290000000000000
          Height = 15.118110240000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'Emeio:')
          ParentFont = False
        end
        object Picture1: TfrxPictureView
          Left = 0.755905510000000000
          Top = 0.755905510000000000
          Width = 194.645669290000000000
          Height = 113.385826770000000000
          ShowHint = False
          Center = True
          HightQuality = True
          Transparent = False
          TransparentColor = clWhite
        end
      end
    end
  end
  object frxDsUnidades: TfrxDBDataset
    UserName = 'frxDsUnidades'
    CloseDataSource = False
    DataSet = QrUnidades
    BCDToCurrency = False
    Left = 384
    Top = 212
  end
  object QrUnidades: TmySQLQuery
    Database = Dmod.MyDB
    Filter = 'VALOR=0'
    Filtered = True
    Left = 412
    Top = 212
    object QrUnidadesVALOR: TFloatField
      FieldName = 'VALOR'
    end
    object QrUnidadesNOME_CON: TWideStringField
      FieldName = 'NOME_CON'
      Size = 100
    end
    object QrUnidadesNOME_PRP: TWideStringField
      FieldName = 'NOME_PRP'
      Size = 100
    end
    object QrUnidadesUnidade: TWideStringField
      FieldName = 'Unidade'
      Size = 10
    end
    object QrUnidadesDepto: TIntegerField
      FieldName = 'Depto'
    end
    object QrUnidadesDOC_PRP: TWideStringField
      FieldName = 'DOC_PRP'
      Size = 18
    end
    object QrUnidadesNOMEUSU: TWideStringField
      FieldName = 'NOMEUSU'
      Size = 100
    end
    object QrUnidadesDOC_USU: TWideStringField
      FieldName = 'DOC_USU'
      Size = 18
    end
    object QrUnidadesConta: TIntegerField
      FieldName = 'Conta'
    end
    object QrUnidadesCOD_PRP: TIntegerField
      FieldName = 'COD_PRP'
    end
    object QrUnidadesCOD_CON: TIntegerField
      FieldName = 'COD_CON'
    end
    object QrUnidadesCEP_PRP: TFloatField
      FieldName = 'CEP_PRP'
    end
    object QrUnidadesUF_PRP: TFloatField
      FieldName = 'UF_PRP'
    end
    object QrUnidadesMUNI_PRP: TWideStringField
      FieldName = 'MUNI_PRP'
      Size = 100
    end
    object QrUnidadesCliInt: TIntegerField
      FieldName = 'CliInt'
    end
  end
  object QrInad: TmySQLQuery
    Database = Dmod.MyDB
    Filter = 'VALOR=0'
    Left = 412
    Top = 240
    object QrInadMES: TWideStringField
      FieldName = 'MES'
      Required = True
      Size = 7
    end
    object QrInadData: TDateField
      FieldName = 'Data'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrInadFatNum: TFloatField
      FieldName = 'FatNum'
      DisplayFormat = '000000'
    end
    object QrInadVencimento: TDateField
      FieldName = 'Vencimento'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrInadMez: TIntegerField
      FieldName = 'Mez'
      Required = True
    end
    object QrInadDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 100
    end
    object QrInadCREDITO: TFloatField
      FieldName = 'CREDITO'
      DisplayFormat = '#,###,##0.00'
    end
    object QrInadPAGO: TFloatField
      FieldName = 'PAGO'
      DisplayFormat = '#,###,##0.00'
    end
    object QrInadJuros: TFloatField
      FieldName = 'Juros'
      DisplayFormat = '#,###,##0.00'
    end
    object QrInadMulta: TFloatField
      FieldName = 'Multa'
      DisplayFormat = '#,###,##0.00'
    end
    object QrInadTOTAL: TFloatField
      FieldName = 'TOTAL'
      DisplayFormat = '#,###,##0.00'
    end
    object QrInadSALDO: TFloatField
      FieldName = 'SALDO'
      DisplayFormat = '#,###,##0.00'
    end
    object QrInadPEND_VAL: TFloatField
      FieldName = 'PEND_VAL'
      DisplayFormat = '#,###,##0.00'
    end
    object QrInadVALOR: TFloatField
      FieldName = 'VALOR'
    end
    object QrInadATZ_VAL: TFloatField
      FieldName = 'ATZ_VAL'
    end
  end
  object frxDsInad: TfrxDBDataset
    UserName = 'frxDsInad'
    CloseDataSource = False
    DataSet = QrInad
    BCDToCurrency = False
    Left = 384
    Top = 240
  end
  object frxCDCU: TfrxReport
    Version = '4.14'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39815.746004548600000000
    ReportOptions.LastChange = 39815.746004548600000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      'end.')
    Left = 356
    Top = 240
    Datasets = <
      item
        DataSet = frxDsCartas
        DataSetName = 'frxDsCartas'
      end
      item
      end
      item
      end
      item
        DataSet = frxDsInad
        DataSetName = 'frxDsInad'
      end
      item
        DataSet = frxDsUnidades
        DataSetName = 'frxDsUnidades'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 9.000000000000000000
      BottomMargin = 10.000000000000000000
      object DetailData1: TfrxDetailData
        Height = 20.000000000000000000
        Top = 45.354360000000000000
        Width = 718.110700000000000000
        Columns = 1
        ColumnWidth = 200.000000000000000000
        ColumnGap = 20.000000000000000000
        DataSet = frxDsCartas
        DataSetName = 'frxDsCartas'
        RowCount = 0
        Stretched = True
        object Rich1: TfrxRichView
          Left = 2.629870000000000000
          Width = 706.929190000000000000
          Height = 20.000000000000000000
          ShowHint = False
          StretchMode = smMaxHeight
          DataField = 'Texto'
          DataSet = frxDsCartas
          DataSetName = 'frxDsCartas'
          GapX = 2.000000000000000000
          GapY = 1.000000000000000000
          RichEdit = {
            7B5C727466315C616E73695C616E7369637067313235325C64656666305C6465
            666C616E67313034367B5C666F6E7474626C7B5C66305C666E696C204D532053
            616E732053657269663B7D7D0D0A7B5C636F6C6F7274626C203B5C726564305C
            677265656E305C626C7565303B7D0D0A7B5C2A5C67656E657261746F72204D73
            66746564697420352E34312E32312E323531303B7D5C766965776B696E64345C
            7563315C706172645C6366315C66305C667331365C7061720D0A7D0D0A00}
        end
      end
      object DadosMestre1: TfrxMasterData
        Height = 3.779527560000000000
        Top = 18.897650000000000000
        Width = 718.110700000000000000
        Columns = 1
        ColumnWidth = 200.000000000000000000
        ColumnGap = 20.000000000000000000
        DataSet = frxDsUnidades
        DataSetName = 'frxDsUnidades'
        RowCount = 0
        StartNewPage = True
      end
    end
  end
  object frxCarta: TfrxReport
    Version = '4.14'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39911.688433344920000000
    ReportOptions.LastChange = 39911.688433344920000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    Left = 356
    Top = 268
    Datasets = <>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
    end
  end
  object frxDsCartas: TfrxDBDataset
    UserName = 'frxDsCartas'
    CloseDataSource = False
    DataSet = QrTexto
    BCDToCurrency = False
    Left = 384
    Top = 268
  end
  object QrTexto: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Titulo, Texto, Tipo'
      'FROM cartas'
      'WHERE Codigo=:P0')
    Left = 412
    Top = 268
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrTextoCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrTextoTitulo: TWideStringField
      FieldName = 'Titulo'
      Size = 100
    end
    object QrTextoTexto: TWideMemoField
      FieldName = 'Texto'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrTextoTipo: TIntegerField
      FieldName = 'Tipo'
    end
  end
end
