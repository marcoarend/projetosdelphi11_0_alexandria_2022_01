unit WTermos;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2,
  ResIntStrings, UnMLAGeral, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, dmkGeral,
  dmkPermissoes, dmkEdit, dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup,
  unDmkProcFunc, UnDmkEnums, Vcl.ComCtrls, dmkEditDateTimePicker, dmkMemo,
  Xml.xmldom, Xml.XMLIntf, Xml.Win.msxmldom, Xml.XMLDoc, msxml, CAPICOM_TLB,
  ACBrMSXML2_TLB, StrUtils, ComObj, dmkCheckBox, Vcl.Grids, Vcl.DBGrids,
  Vcl.OleCtrls, SHDocVw, dmkValUsu, Vcl.Menus, dmkCheckGroup,
  UnProjGroup_Consts;

type
  TFmWTermos = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    BtPublica: TBitBtn;
    BtAltera: TBitBtn;
    BtInclui: TBitBtn;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    QrWTextos: TmySQLQuery;
    QrWTextosCodigo: TIntegerField;
    DsWTextos: TDataSource;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    Label9: TLabel;
    EdNome: TdmkEdit;
    Label1: TLabel;
    DBEdCodigo: TdmkDBEdit;
    Label2: TLabel;
    DBEdNome: TdmkDBEdit;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    MeTexto: TdmkMemo;
    QrWTextosNome: TWideStringField;
    QrWTextosVersao: TIntegerField;
    QrWTextosTexto: TWideMemoField;
    QrWTextosPublicDtaUTC: TDateTimeField;
    QrWTextosVigenIniDtaUTC: TDateTimeField;
    QrWTextosVigenIniDtaTZ_TXT: TWideStringField;
    QrWTextosPublicDtaTZ_TXT: TWideStringField;
    CkVigencia: TdmkCheckBox;
    PageControl1: TPageControl;
    TSRich: TTabSheet;
    TSGrid: TTabSheet;
    DBMeTexto: TDBMemo;
    QrWTextosHashArq: TWideStringField;
    TPVigenIniDtaUTC: TdmkEditDateTimePicker;
    LaVigenIniDtaUTC: TLabel;
    QrWTextosOpc: TmySQLQuery;
    BtOpcoes: TBitBtn;
    QrWTextosArqWeb: TIntegerField;
    Label3: TLabel;
    EdPublic: TEdit;
    DBGrid1: TDBGrid;
    QrWTextosArquivo: TWideStringField;
    QrWTextosHtml: TIntegerField;
    QrWTextosCodUsu: TIntegerField;
    EdCodUsu: TdmkEdit;
    Label4: TLabel;
    EdVersao: TdmkEdit;
    Label5: TLabel;
    PMInclui: TPopupMenu;
    Incluinovotermo1: TMenuItem;
    Incluinovaversoparaotermoatual1: TMenuItem;
    Label6: TLabel;
    dmkDBEdit1: TdmkDBEdit;
    Label8: TLabel;
    dmkDBEdit2: TdmkDBEdit;
    DBRGStatus: TDBRadioGroup;
    DBCGNivel: TdmkDBCheckGroup;
    RGStatus: TdmkRadioGroup;
    CGNivel: TdmkCheckGroup;
    QrWTextosNivel: TIntegerField;
    QrWTextosStatus: TIntegerField;
    VUTipo: TdmkValUsu;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    ImgWEB: TdmkImage;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrWTextosAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrWTextosBeforeOpen(DataSet: TDataSet);
    procedure BtIncluiClick(Sender: TObject);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure CkVigenciaClick(Sender: TObject);
    procedure BtPublicaClick(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
    procedure BtOpcoesClick(Sender: TObject);
    procedure QrWTextosAfterScroll(DataSet: TDataSet);
    procedure QrWTextosBeforeClose(DataSet: TDataSet);
    procedure Incluinovaversoparaotermoatual1Click(Sender: TObject);
    procedure Incluinovotermo1Click(Sender: TObject);
    procedure PMIncluiPopup(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    procedure MostraWTextosOpc();
    procedure IncluiTermo(CodUsu: Integer);
  public
    { Public declarations }
    procedure LocCod(Atual, Codigo: Integer);
  end;

var
  FmWTermos: TFmWTermos;

const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, ModuleGeral, UnDmkWeb, DocsAssina, UnitDocsAssina,
  MyDBCheck, ZForge, UnFTP, MyListas, DmkDAC_PF, UnWTextos_Jan, UnWTextos_PF,
  UnGrlUsuarios;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmWTermos.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmWTermos.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmWTermos.MostraWTextosOpc;
begin
  WTextos_Jan.MostraFormWTextosOpc(FmWTermos);
  //
  UWTextos_PF.ReopenWTextosOpc(QrWTextosOpc);
end;

procedure TFmWTermos.PMIncluiPopup(Sender: TObject);
var
  Enab: Boolean;
begin
  Enab := (QrWTextos.State <> dsInactive) and (QrWTextos.RecordCount > 0) and (QrWTextosCodigo.Value <> 0);
  //
  Incluinovaversoparaotermoatual1.Enabled := Enab;
end;

procedure TFmWTermos.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrWTextosCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmWTermos.DefParams;
var
  TZD_UTC_Str: String;
begin
  TZD_UTC_Str := '+00:00';//DModG.QrParamsEmpTZD_UTC_Str.Value;
  //
  VAR_GOTOTABELA := 'wtextos';
  VAR_GOTOMYSQLTABLE := QrWTextos;
  VAR_GOTONEG := gotoPos;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDBn;
  VAR_GOTOVAR := 1;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT wte.*, ');
  VAR_SQLx.Add('IF(wte.VigenIniDtaUTC <= "1900-01-01", "", DATE_FORMAT(CONVERT_TZ(wte.VigenIniDtaUTC, "+00:00", "'+ TZD_UTC_Str +'"), "%d/%m/%Y")) VigenIniDtaTZ_TXT, ');
  VAR_SQLx.Add('IF(wte.PublicDtaUTC <= "1900-01-01", "", DATE_FORMAT(CONVERT_TZ(wte.PublicDtaUTC, "+00:00", "' + TZD_UTC_Str + '"), "%d/%m/%Y %H:%i:%s")) PublicDtaTZ_TXT, ');
  VAR_SQLx.Add('arq.Arquivo ');
  VAR_SQLx.Add('FROM wtextos wte ');
  VAR_SQLx.Add('LEFT JOIN ftpwebarq arq ON arq.Codigo = wte.ArqWeb');
  VAR_SQLx.Add('WHERE wte.Tipo = 1'); //Apenas termos
  //
  VAR_SQL1.Add('AND wte.Codigo=:P0');
  //
  //VAR_SQL2.Add('AND wte.CodUsu=:P0');
  //
  VAR_SQLa.Add('AND wte.Nome Like :P0');
  //
  VAR_GOTOVAR1 := 'Tipo = 1';
end;

procedure TFmWTermos.CkVigenciaClick(Sender: TObject);
var
  Visi: Boolean;
begin
  Visi := CkVigencia.Checked;
  //
  LaVigenIniDtaUTC.Visible := Visi;
  TPVigenIniDtaUTC.Visible := Visi;
end;

procedure TFmWTermos.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmWTermos.QueryPrincipalAfterOpen;
begin
end;

procedure TFmWTermos.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmWTermos.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmWTermos.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmWTermos.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmWTermos.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmWTermos.BtAlteraClick(Sender: TObject);
var
  Enab: Boolean;
begin
  if (QrWTextos.State <> dsInactive) and (QrWTextos.RecordCount > 0) then
  begin
    if QrWTextosArqWeb.Value <> 0 then
    begin
      Geral.MB_Aviso('Textos publicados n�o podem ser editados!');
      Exit;
    end;
    if QrWTextosHtml.Value = 1 then
    begin
      Geral.MB_Aviso('Textos Html n�o podem ser editados neste local!');
      Exit;
    end;
    //
    UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrWTextos, [PnDados],
      [PnEdita], EdNome, ImgTipo, 'wtextos');
    //
    EdNome.ReadOnly := False;
    //
    if QrWTextosVigenIniDtaUTC.Value < 2 then
      Enab := False
    else
      Enab := True;
    //
    CkVigencia.Checked       := Enab;
    LaVigenIniDtaUTC.Visible := Enab;
    TPVigenIniDtaUTC.Visible := Enab;
  end;
end;

procedure TFmWTermos.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrWTextosCodigo.Value;
  Close;
end;

procedure TFmWTermos.BtConfirmaClick(Sender: TObject);
var
  Codigo: Integer;
  Nome, Texto: String;
begin
  Nome  := EdNome.ValueVariant;
  Texto := MeTexto.Text;
  //
  VUTipo.ValueVariant := 1; //Termo � fixo
  //
  if MyObjects.FIC(Length(Nome) = 0, EdNome, 'Defina uma descri��o!') then Exit;
  if MyObjects.FIC(Length(Texto) = 0, MeTexto, 'Defina um texto!') then Exit;
  //
  if not CkVigencia.Checked then
    TPVigenIniDtaUTC.Date := 0;
  //
  if ImgTipo.SQLType = stIns then
  begin
    Codigo := UMyMod.BuscaNovoCodigo_Int(Dmod.QrUpdN, 'wtextos', 'Codigo',
      [], [], stIns, 0, siPositivo, nil);
    //
    EdVersao.ValueVariant := UWTextos_PF.ObtemVersaoTermo(EdCodUsu.ValueVariant);
    //
    if EdCodUsu.ValueVariant = 0 then
      EdCodUsu.ValueVariant := Codigo;
  end else
    Codigo := QrWTextosCodigo.Value;
  //
  if UMyMod.ExecSQLInsUpdPanel(ImgTipo.SQLType, Self, PnEdita,
    'wtextos', Codigo, Dmod.QrUpdN, [PnEdita], [PnDados], ImgTipo, True) then
  begin
    LocCod(Codigo, Codigo);
  end;
end;

procedure TFmWTermos.BtDesisteClick(Sender: TObject);
var
  Codigo: Integer;
begin
  Codigo          := EdCodigo.ValueVariant;
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  //
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDBn, 'wtextos', 'Codigo');
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmWTermos.BtIncluiClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMInclui, BtInclui);
end;

procedure TFmWTermos.BtOpcoesClick(Sender: TObject);
begin
  MostraWTextosOpc;
end;

procedure TFmWTermos.BtPublicaClick(Sender: TObject);

  function GeraTextoArquivo(Diretorio: String; PublicadoEm: TDateTime): String;
  var
    Caminho, Nome, Public_Txt: String;
    Arq: TStringList;
   begin
    Result     := '';
    Nome       := Geral.SemAcento(QrWTextosNome.Value);
    Nome       := StringReplace(Nome, ' ', '_', [rfReplaceAll, rfIgnoreCase]);
    Caminho    := Diretorio + '\' + Nome + '.txt';
    Public_Txt := Geral.FDT(PublicadoEm, 2);
    //
    if not DirectoryExists(Diretorio) then
      ForceDirectories(Diretorio);
    //
    if FileExists(Caminho) then
      DeleteFile(Caminho);
    //
    DBMeTexto.Lines.SaveToFile(Caminho);
    //
    try
      Arq := TStringList.Create;
      //
      Arq.LoadFromFile(Caminho);
      Arq.Insert(0, 'Publicado em: ' + Public_Txt);
      Arq.SaveToFile(Caminho);
    finally
      Arq.Free;
    end;
    //
    if QrWTextosVigenIniDtaUTC.Value > 2 then
    begin
      try
        Arq := TStringList.Create;
        //
        Arq.LoadFromFile(Caminho);
        Arq.Insert(1, 'Com vig�ncia a partir de: ' + QrWTextosVigenIniDtaTZ_TXT.Value);
        Arq.SaveToFile(Caminho);
      finally
        Arq.Free;
      end;
    end;
    //
    Result := Caminho;
  end;

var
  Agora: TDateTime;
  Hash, Serial, Arquivo, Diretorio, ArqAssinado, ArquivoZip, NomeDir, Caminho: String;
  Codigo, FTPConfig, DirWeb, ArqWeb, Nivel: Integer;
  Arquivos: TStringList;
begin
  if (QrWTextos.State <> dsInactive) and (QrWTextos.RecordCount > 0) then
  begin
    if QrWTextosArqWeb.Value = 0 then
    begin
      Agora     := DModG.ObtemAgora;
      Serial    := QrWTextosOpc.FieldByName('CertDigital').AsString;
      DirWeb    := QrWTextosOpc.FieldByName('DirWebPad').AsInteger;
      FTPConfig := QrWTextosOpc.FieldByName('FTPConfig').AsInteger;
      Caminho   := QrWTextosOpc.FieldByName('Caminho').AsString;
      Nivel     := QrWTextosOpc.FieldByName('Nivel').AsInteger;
      NomeDir   := 'Termo_ID_' + Geral.FF0(QrWTextosCodigo.Value);
      Diretorio := CO_DIR_RAIZ_DMK + '\WTextos\' + NomeDir;
      Arquivo   := GeraTextoArquivo(Diretorio, Agora);
      Codigo    := QrWTextosCodigo.Value;
      //
      if Serial = '' then
      begin
        if Geral.MB_Pergunta('Certificado digital n�o definido!' + sLineBreak +
          'Deseja defin�-lo agora?') = ID_YES
        then
          MostraWTextosOpc;
        Exit;
      end;
      if DirWeb = 0 then
      begin
        if Geral.MB_Pergunta('Diret�rio WEB para armazenamento de arquivos n�o definido!' + sLineBreak +
          'Deseja defin�-lo agora?') = ID_YES
        then
          MostraWTextosOpc;
        Exit;
      end;
      if MyObjects.FIC(Arquivo = '', nil, 'Falha ao gerar arquivo tempor�rio!') then Exit;
      //Assina
      ArqAssinado := UnDocsAssina.AssinaDocumento(Agora, Serial, Arquivo, Diretorio);
      //
      if MyObjects.FIC(ArqAssinado = '', nil, 'Falha ao assinar arquivo! Publica��o abortada!') then Exit;
      //Valida assinatura
      UnDocsAssina.VerificaAssinaturaDeDocumento(ArqAssinado);
      //
      if Geral.MB_Pergunta('Valida��o do arquivo assinado finalizada' +
        sLineBreak + 'Deseja continuar?') = ID_YES then
      begin
        //Obtem Hash do arquivo
        Hash := UnDocsAssina.CalculaHash(ArqAssinado, istSHA1);
        //
        if MyObjects.FIC(Hash = '', nil, 'Falha ao obter hash do arquivo assinado! Publica��o abortada!') then Exit;
        //Zipar
        if DBCheck.CriaFm(TFmZForge, FmZForge, afmoLiberado) then
        begin
          FmZForge.Show;
          ArquivoZip := FmZForge.ZipaArquivo(zftDiretorio, ExtractFileDir(ArqAssinado),
                          Diretorio, NomeDir, '', False, False);
          FmZForge.Destroy;
        end;
        if MyObjects.FIC(ArquivoZip = '', nil, 'Falha ao zipar arquivo! Publica��o abortada!') then Exit;
        //Fazer upload
        Arquivos := TStringList.Create;
        try
          Arquivos.Add(ArquivoZip);
          //
          ArqWeb := UFTP.UploadFTP_DB(nil, nil, stIns, ExtractFileName(ArquivoZip),
                      NomeDir, '', '', 0, CO_DMKID_APP, FTPConfig, 1, DirWeb,
                      VAR_LIB_EMPRESA_SEL, Nivel, dtWTexto, 0, 0, Arquivos, True,
                      Dmod.QrUpdN, Dmod.QrAuxN, Dmod.MyDBn, Caminho, True);
          //
          if MyObjects.FIC(ArqWeb = 0, nil, 'Falha ao fazer upload do arquivo para o servidor WEB! Publica��o abortada!') then Exit;
          //
          //Atualiza Data Base
          if UMyMod.SQLInsUpd(Dmod.QrUpdN, stUpd, 'wtextos', False,
            ['ArqWeb', 'HashArq', 'PublicDtaUTC', 'Publicado'], ['Codigo'],
            [ArqWeb, Hash, Agora, 1], [Codigo], True)  then
          begin
            Geral.MB_Aviso('Texto publicado comm sucesso!');
            //
            try
              dmkPF.RemoveArquivosDeDiretorio(Diretorio + '\');
              RemoveDir(Diretorio);
            except
              Geral.MB_Erro('Falha ao remover arquivos tempor�rios!');
            end;
            LocCod(Codigo, Codigo);
          end else
            Geral.MB_Aviso('Falha ao atualizar dados da publica��o!');
        finally
          Arquivos.Free;
        end;
      end;
    end else
      Geral.MB_Aviso('Voc� deve selecionar um texto que ainda n�o foi publicado!');
  end;
end;

procedure TFmWTermos.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType    := stLok;
  MeTexto.Align      := alClient;
  PageControl1.Align := alClient;
  //
  DModG.ReopenParamsEmp(VAR_LIB_EMPRESA_SEL);
  UWTextos_PF.ReopenWTextosOpc(QrWTextosOpc);
  //
  CriaOForm;
  //
  CGNivel.Items.Clear;
  DBCGNivel.Items.Clear;
  //
  CGNivel.Items.AddStrings(GrlUsuarios.ConfiguraNiveis(CO_DMKID_APP));
  DBCGNivel.Items.AddStrings(GrlUsuarios.ConfiguraNiveis(CO_DMKID_APP));
  //
  RGStatus.Items.Clear;
  RGStatus.Items.AddStrings(UWTextos_PF.ConfiguraStatus());
  //
  DBRGStatus.Items.Clear;
  DBRGStatus.Items.AddStrings(UWTextos_PF.ConfiguraStatus());
  //
  PageControl1.ActivePageIndex := 0;
end;

procedure TFmWTermos.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrWTextosCodigo.Value, LaRegistro.Caption);
end;

procedure TFmWTermos.SbImprimeClick(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmDocsAssina, FmDocsAssina, afmoNegarComAviso) then
  begin
    FmDocsAssina.ShowModal;
    FmDocsAssina.Destroy;
  end;
end;

procedure TFmWTermos.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmWTermos.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrWTextosCodigo.Value, LaRegistro.Caption);
end;

procedure TFmWTermos.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmWTermos.QrWTextosAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmWTermos.QrWTextosAfterScroll(DataSet: TDataSet);
var
  Publicado: Boolean;
begin
  PageControl1.ActivePageIndex := 0;
  //
  if QrWTextosArqWeb.Value <> 0 then
  begin
    TSGrid.TabVisible := True;
    EdPublic.Text     := 'Sim';
  end else
  begin
    TSGrid.TabVisible := False;
    EdPublic.Text     := 'N�o';
  end;
end;

procedure TFmWTermos.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  DmkWeb.CarregaImagemFormWEB(ImgWEB);
end;

procedure TFmWTermos.SbQueryClick(Sender: TObject);
begin
  LocCod(QrWTextosCodigo.Value, CuringaLoc.CriaForm(CO_CODIGO, CO_NOME,
    'wtextos', Dmod.MyDBn, CO_VAZIO));
end;

procedure TFmWTermos.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmWTermos.Incluinovaversoparaotermoatual1Click(Sender: TObject);
begin
  IncluiTermo(QrWTextosCodUsu.Value);
end;

procedure TFmWTermos.IncluiTermo(CodUsu: Integer);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrWTextos, [PnDados],
    [PnEdita], EdNome, ImgTipo, 'wtextos');
  //
  if CodUsu <> 0 then
  begin
    EdCodUsu.ValueVariant := CodUsu;
    EdNome.ValueVariant   := QrWTextosNome.Value;
    MeTexto.Text          := QrWTextosTexto.Value;
    EdNome.ReadOnly       := True;
    //
    MeTexto.SetFocus;
  end else
  begin
    EdCodUsu.ValueVariant := 0;
    EdNome.ReadOnly       := False;
    //
    EdNome.SetFocus;
  end;
  //
  CkVigencia.Checked       := False;
  LaVigenIniDtaUTC.Visible := False;
  TPVigenIniDtaUTC.Visible := False;
  TPVigenIniDtaUTC.Date    := DModG.ObtemAgora + 1;
end;

procedure TFmWTermos.Incluinovotermo1Click(Sender: TObject);
begin
  IncluiTermo(0);
end;

procedure TFmWTermos.QrWTextosBeforeClose(DataSet: TDataSet);
begin
  EdPublic.Text := '';
end;

procedure TFmWTermos.QrWTextosBeforeOpen(DataSet: TDataSet);
begin
  QrWTextosCodigo.DisplayFormat := FFormatFloat;
end;

end.

