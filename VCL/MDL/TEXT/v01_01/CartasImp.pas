unit CartasImp;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB, frxClass,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnMLAGeral, UnInternalConsts,
  frxDBSet, UnDmkProcFunc, dmkDBGridDAC, frxBarcode, UnDmkEnums;

type
  TTipoFormCartasImp = (tfciCertidaoNeg, tfciCobrancaGer);
  TFmCartasImp = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtImprime: TBitBtn;
    LaTitulo1C: TLabel;
    QrEnvCartas: TmySQLQuery;
    frxCNDC: TfrxReport;
    frxDsUnidades: TfrxDBDataset;
    QrUnidades: TmySQLQuery;
    QrUnidadesVALOR: TFloatField;
    QrUnidadesNOME_CON: TWideStringField;
    QrUnidadesNOME_PRP: TWideStringField;
    QrUnidadesUnidade: TWideStringField;
    QrUnidadesDepto: TIntegerField;
    QrUnidadesDOC_PRP: TWideStringField;
    QrUnidadesNOMEUSU: TWideStringField;
    QrUnidadesDOC_USU: TWideStringField;
    QrUnidadesConta: TIntegerField;
    QrUnidadesCOD_PRP: TIntegerField;
    QrUnidadesCOD_CON: TIntegerField;
    QrUnidadesCEP_PRP: TFloatField;
    QrUnidadesUF_PRP: TFloatField;
    QrUnidadesMUNI_PRP: TWideStringField;
    QrUnidadesCliInt: TIntegerField;
    QrInad: TmySQLQuery;
    QrInadMES: TWideStringField;
    QrInadData: TDateField;
    QrInadFatNum: TFloatField;
    QrInadVencimento: TDateField;
    QrInadMez: TIntegerField;
    QrInadDescricao: TWideStringField;
    QrInadCREDITO: TFloatField;
    QrInadPAGO: TFloatField;
    QrInadJuros: TFloatField;
    QrInadMulta: TFloatField;
    QrInadTOTAL: TFloatField;
    QrInadSALDO: TFloatField;
    QrInadPEND_VAL: TFloatField;
    QrInadVALOR: TFloatField;
    QrInadATZ_VAL: TFloatField;
    frxDsInad: TfrxDBDataset;
    frxCDCU: TfrxReport;
    frxCarta: TfrxReport;
    frxDsCartas: TfrxDBDataset;
    QrTexto: TmySQLQuery;
    QrTextoCodigo: TIntegerField;
    QrTextoTitulo: TWideStringField;
    QrTextoTexto: TWideMemoField;
    QrTextoTipo: TIntegerField;
    QrUsers: TmySQLQuery;
    QrUsersUsername: TWideStringField;
    QrUsersPassword: TWideStringField;
    CkDesign: TCheckBox;
    PB1: TProgressBar;
    QrCondComCartas: TmySQLQuery;
    DsCondComCartas: TDataSource;
    DBGrid1: TdmkDBGridDAC;
    QrCartasDeCond: TmySQLQuery;
    DsCartasDeCond: TDataSource;
    Splitter1: TSplitter;
    DBGrid2: TDBGrid;
    QrCondComCartasCodigo: TIntegerField;
    QrCondComCartasItens: TLargeintField;
    QrCondComCartasNome: TWideStringField;
    QrCondComCartasAtivo: TSmallintField;
    BtTodos: TBitBtn;
    BtNenhum: TBitBtn;
    QrSelConds: TmySQLQuery;
    QrSelCondsCodigo: TIntegerField;
    QrSelCondsNome: TWideStringField;
    QrSelCondsItens: TLargeintField;
    QrCartasDeCondVencimento: TDateField;
    QrCartasDeCondData: TDateField;
    QrCartasDeCondMez: TIntegerField;
    QrCartasDeCondDepto: TIntegerField;
    QrCartasDeCondFatNum: TFloatField;
    QrCartasDeCondCodigo: TIntegerField;
    QrCartasDeCondPropriet: TIntegerField;
    QrCartasDeCondUsuario: TIntegerField;
    QrCartasDeCondEmpresa: TIntegerField;
    QrCartasDeCondEmprEnti: TIntegerField;
    QrCartasDeCondJuridica: TSmallintField;
    QrCartasDeCondDiarCEMIts: TIntegerField;
    QrCartasDeCondMetodo: TSmallintField;
    QrCartasDeCondCadastro: TIntegerField;
    QrCartasDeCondReInclu: TIntegerField;
    QrCartasDeCondDtaInicio: TDateField;
    QrCartasDeCondDtalimite: TDateField;
    QrCartasDeCondMEZ_TXT: TWideStringField;
    QrCartasDeCondDEPTO_TXT: TWideStringField;
    QrCartasDeCondNO_PRP: TWideStringField;
    QrCartoesAR: TmySQLQuery;
    QrCartoesARPropriet: TIntegerField;
    QrCartoesARCartaoAR: TSmallintField;
    QrCartoesARNO_PRP: TWideStringField;
    QrCartoesARRUA: TWideStringField;
    QrCartoesARNumero: TFloatField;
    QrCartoesARCompl: TWideStringField;
    QrCartoesARBairro: TWideStringField;
    QrCartoesARCidade: TWideStringField;
    QrCartoesARUF: TFloatField;
    QrCartoesARCEP: TFloatField;
    QrCartoesARPais: TWideStringField;
    frxDsCartoesAR: TfrxDBDataset;
    frxCartoesAR: TfrxReport;
    QrEnvCartasVencimento: TDateField;
    QrEnvCartasData: TDateField;
    QrEnvCartasMez: TIntegerField;
    QrEnvCartasFatNum: TFloatField;
    QrEnvCartasDepto: TIntegerField;
    QrEnvCartasPropriet: TIntegerField;
    QrEnvCartasUsuario: TIntegerField;
    QrEnvCartasEmpresa: TIntegerField;
    QrEnvCartasEmprEnti: TIntegerField;
    QrEnvCartasCodigo: TIntegerField;
    QrEnvCartasDiarCEMIts: TIntegerField;
    QrEnvCartasMetodo: TSmallintField;
    QrEnvCartasCadastro: TIntegerField;
    QrEnvCartasReInclu: TIntegerField;
    QrEnvCartasDtaInicio: TDateField;
    QrEnvCartasDtaLimite: TDateField;
    QrEnvCartasTentativas: TIntegerField;
    QrEnvCartasLastExec: TDateTimeField;
    QrEnvCartasConsidEnvi: TDateTimeField;
    QrEnvCartasLk: TIntegerField;
    QrEnvCartasDataCad: TDateField;
    QrEnvCartasDataAlt: TDateField;
    QrEnvCartasUserCad: TIntegerField;
    QrEnvCartasUserAlt: TIntegerField;
    QrEnvCartasAlterWeb: TSmallintField;
    QrEnvCartasAtivo: TSmallintField;
    QrEnvCartasLastLotImp: TLargeintField;
    QrEnvCartasAutoCancel: TDateTimeField;
    QrEnvCartasLastVerify: TDateTimeField;
    QrEnvCartasNaoEnvTip: TIntegerField;
    QrEnvCartasNaoEnvMot: TIntegerField;
    QrEnvCartasNaoEnvLst: TSmallintField;
    QrCartoesARENDERECO: TWideStringField;
    QrCartoesARNOMELOGRAD: TWideStringField;
    QrCartoesARNUMERO_TXT: TWideStringField;
    QrCartoesARCEP_TXT: TWideStringField;
    QrCartoesARNO_UF: TWideStringField;
    frxBarCodeObject1: TfrxBarCodeObject;
    QrCartoesAREAN128: TWideStringField;
    QrCartoesARControle: TIntegerField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure QrEnvCartasAfterScroll(DataSet: TDataSet);
    procedure QrUnidadesAfterScroll(DataSet: TDataSet);
    procedure QrUnidadesBeforeClose(DataSet: TDataSet);
    procedure frxCNDCGetValue(const VarName: string; var Value: Variant);
    procedure BtImprimeClick(Sender: TObject);
    procedure QrCondComCartasAfterScroll(DataSet: TDataSet);
    procedure QrCondComCartasBeforeClose(DataSet: TDataSet);
    procedure BtTodosClick(Sender: TObject);
    procedure BtNenhumClick(Sender: TObject);
    procedure QrSelCondsBeforeClose(DataSet: TDataSet);
    procedure QrSelCondsAfterOpen(DataSet: TDataSet);
    procedure QrCondComCartasAfterOpen(DataSet: TDataSet);
    procedure QrCartoesARCalcFields(DataSet: TDataSet);
  private
    { Private declarations }
    FMemoryStream: TMemoryStream;
    FCondComCartas: String;
    //
    procedure ReabreCartasDeCond();
    procedure AtivarTodos(Ativo: Integer);
  public
    { Public declarations }
    FEmpresa, FEmprEnti, FDepto, FPropriet, FCarta,
    FRGModoItemIndex : Integer;
    FDataPesq: TDateTime;
    FTipoFormCartasImp: TTipoFormCartasImp;
    //FCkDesignChecked,
    FUnidadesFiltered, FMultaJur: Boolean;
    FTabLctA: String;
    //

    function  GeraRelatorio(ComoFastReportGera: TComoFastReportGera): Boolean;
    function  IncorporaQryUnidades(Source_: TmySQLQuery;
              Source_VALOR: TFloatField;
              Source_NOME_CON: TWideStringField;
              Source_NOME_PRP: TWideStringField;
              Source_Unidade: TWideStringField;
              Source_Depto: TIntegerField;
              Source_DOC_PRP: TWideStringField;
              Source_NOMEUSU: TWideStringField;
              Source_DOC_USU: TWideStringField;
              Source_Conta: TIntegerField;
              Source_COD_PRP: TIntegerField;
              Source_COD_CON: TIntegerField;
              Source_CEP_PRP: TFloatField;
              Source_UF_PRP: TFloatField;
              Source_MUNI_PRP: TWideStringField;
              Source_CliInt: TIntegerField): Boolean;
    function  IncorporaQryInad(Source_: TmySQLQuery;
              Source_MES: TWideStringField;
              Source_Data: TDateField;
              Source_FatNum: TFloatField;
              Source_Vencimento: TDateField;
              Source_Mez: TIntegerField;
              Source_Descricao: TWideStringField;
              Source_CREDITO: TFloatField;
              Source_PAGO: TFloatField;
              Source_Juros: TFloatField;
              Source_Multa: TFloatField;
              Source_TOTAL: TFloatField;
              Source_SALDO: TFloatField;
              Source_PEND_VAL: TFloatField;
              Source_VALOR: TFloatField;
              Source_ATZ_VAL: TFloatField): Boolean;
    function  GeraCertidoes(SQL: String): Boolean;
    function  GeraCartasCobranca(PBx: TProgressBar): Boolean;
    procedure ReopenUnidades();
    procedure ReopenInad();
    //procedure ConsideraExacutado_Carta(PBx: TProgressBar);
    procedure MostraFormLastLotImp(LotImp: Int64);
    procedure ReopenCondComCartas(Recria: Boolean; Codigo: Integer);
  end;

  var
  FmCartasImpCN: TFmCartasImp; // Usar somente em TFmCertidaoNeg !!!
  FmCartasImpCG: TFmCartasImp; // Usar somente em TFmCobrarGer !!!

implementation

uses Module, UnGOTOy, ModuleGeral, UnMyObjects, DmkDAC_PF, UnDiario_Tabs,
UnFinanceiro, Meufrx, UMySQLModule, CartasEnv, MyDBCheck, CreateGeral, MyVCLSkin;

{$R *.DFM}

procedure TFmCartasImp.AtivarTodos(Ativo: Integer);
var
  Codigo: Integer;
begin
  Screen.Cursor := crHourGlass;
  try
    Codigo := QrCondComCartasCodigo.Value;
    UnDmkDAC_PF.ExecutaMySQLQuery0(DmodG.QrUpdPID1, DmodG.MyPID_DB, [
    'UPDATE ' + FCondComCartas + ' SET ',
    'Ativo=' + Geral.FF0(Ativo),
    '']);
    //
    ReopenCondComCartas(False, Codigo);
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmCartasImp.BtNenhumClick(Sender: TObject);
begin
  AtivarTodos(0);
end;

procedure TFmCartasImp.BtImprimeClick(Sender: TObject);
begin
  if FTipoFormCartasImp = tfciCobrancaGer then
    GeraCartasCobranca(PB1)
  else
    Geral.MB_Aviso('Tipo de impress�o de carta n�o implementado!' +sLineBreak +
    'Solicite � DERMATEK!');
end;

procedure TFmCartasImp.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmCartasImp.BtTodosClick(Sender: TObject);
begin
  AtivarTodos(1);
end;

procedure TFmCartasImp.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmCartasImp.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  frxCNDC.OnGetValue      := frxCNDCGetValue;
  frxCDCU.OnGetValue      := frxCNDCGetValue;
  frxCartoesAR.OnGetValue := frxCNDCGetValue;
  //
  DModG.ReopenOpcoesGerl;
end;

procedure TFmCartasImp.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmCartasImp.frxCNDCGetValue(const VarName: string;
  var Value: Variant);
const
  L = sLineBreak;
var
  EndParc, Txt_Multa, Txt_Juros, Txt_ATZ_VAL, Txt_Tot_Multa, Txt_Tot_Juros,
  Txt_Tot_ATZ_VAL: String;
  Val, Mul, Jur, Atz, Pag, Pen: Double;
begin
  if VarName = 'CONDOMIN' then
    Value := QrUnidadesNOME_CON.Value
  else
  if VarName = 'VAR_MYURL' then
    Value := DModG.QrOpcoesGerl.FieldByName('Web_MyURL').AsString
  else
  if VarName = 'UNIDADE ' then
    Value := QrUnidadesUnidade.Value
  else
  if VarName = 'DATA_NEG' then
    Value := Geral.FDT(FDataPesq, 6)
  else
  if VarName = 'ADMINIST' then
    Value := Dmod.QrDonoNOMEDONO.Value
  else
  if VarName = 'PROPNOME' then
    Value := QrUnidadesNOME_PRP.Value
  else
  if VarName = 'COND_END' then
    Value := GOTOy.EnderecoDeEntidade1(QrUnidadesCOD_CON.Value, 0)
  else
  if VarName = 'PROP_END' then
    Value := GOTOy.EnderecoDeEntidade1(QrUnidadesCOD_PRP.Value, 0)
  else
  if VarName = 'PROP_ENDP' then
  begin
    if DModG.ReopenEndereco(QrUnidadesCOD_PRP.Value) then
    begin
      EndParc := DModG.QrEnderecoNOMELOGRAD.Value;
      if Trim(EndParc) <> '' then
        EndParc := EndParc + ' ';
      EndParc := EndParc + DModG.QrEnderecoRUA.Value;
      if Trim(DModG.QrEnderecoRUA.Value) <> '' then
        EndParc := EndParc + ', ' + DModG.QrEnderecoNUMERO_TXT.Value;
      if Trim(DModG.QrEnderecoCOMPL.Value) <> '' then
        EndParc := EndParc + ' ' + DModG.QrEnderecoCOMPL.Value;
      if Trim(DModG.QrEnderecoBAIRRO.Value) <> '' then
        EndParc := EndParc + ' - ' + DModG.QrEnderecoBAIRRO.Value;
      //
      Value := EndParc;
    end;
  end
  else
  if VarName = 'PROP_CEP' then
    Value := Geral.FormataCEP_TT(FormatFloat('0', QrUnidadesCEP_PRP.Value))
  else
  if VarName = 'PROP_UF' then
    Value := DModG.ObtemUFX(Trunc(QrUnidadesUF_PRP.Value))
  else
  if VarName = 'PROP_MUN' then
    Value := QrUnidadesMUNI_PRP.Value
  else
  if VarName = 'PROP_DOC' then
    Value := Geral.FormataCNPJ_TT(QrUnidadesDOC_PRP.Value)
  else
  if VarName = 'SIND_NOME' then
    Value := Dmod.ObtemSindicoCond(QrUnidadesCliInt.Value)
  else
  if VarName = 'USU_NOME' then
    Value := QrUnidadesNOMEUSU.Value
  else
  if VarName = 'USU__DOC' then
    Value := Geral.FormataCNPJ_TT(QrUnidadesDOC_USU.Value)
  else
  if VarName = 'UN_LOGIN' then
  begin
    QrUsers.Close;
    QrUsers.Params[0].AsInteger := QrUnidadesConta.Value;
    QrUsers.Open;
    Value := QrUsersUsername.Value;
  end else
  if VarName = 'UN_SENHA' then
  begin
    QrUsers.Close;
    QrUsers.Params[0].AsInteger := QrUnidadesConta.Value;
    QrUsers.Open;
    Value := QrUsersPassword.Value;
  end else
  if VarName = 'LDEBITOS' then
  begin
    Val := 0;
    Mul := 0;
    Jur := 0;
    Atz := 0;
    Pag := 0;
    Pen := 0;
    QrInad.First;
    Value := '';
    //
    if FMultaJur = True then
      Value := Value + 'N�mero bloqueto  Compet.    Valor R$   Vencim.       Multa       Juros  Atualizado        Pago    Pendente' + L
    else
      Value := Value + 'N�mero bloqueto  Compet.    Valor R$   Vencim.       Pago    Pendente' + L;
    //'---------------123456789012345--01/2009--123.654,00--01/02/09--123.456,00--123.456,00--123.456,00--123.456,00--123.456,00'+sLineBreak;
    while not QrInad.Eof do
    begin
      Val := Val + QrInadCREDITO.Value;
      Mul := Mul + QrInadMulta.Value;
      Jur := Jur + QrInadJuros.Value;
      Atz := Atz + QrInadATZ_VAL.Value;
      Pag := Pag + QrInadPAGO.Value;
      Pen := Pen + QrInadPEND_VAL.Value;
      //
      if FMultaJur = True then
      begin
        Txt_Multa   := Geral.CompletaString(Geral.FFT(QrInadMulta.Value, 2, siPositivo), ' ', 10, taRightJustify, True) + '  ';
        Txt_Juros   := Geral.CompletaString(Geral.FFT(QrInadJuros.Value, 2, siPositivo), ' ', 10, taRightJustify, True) + '  ';
        Txt_ATZ_VAL := Geral.CompletaString(Geral.FFT(QrInadATZ_VAL.Value, 2, siPositivo), ' ', 10, taRightJustify, True) + '  ';
      end else
      begin
        Txt_Multa   := '';
        Txt_Juros   := '';
        Txt_ATZ_VAL := '';
      end;
      //
      //
      Value := Value +
      Geral.CompletaString(dmkPF.FFP(QrInadFatNum.Value, 0),' ', 15, taRightJustify, True) + '  ' +
      Geral.CompletaString(QrInadMES.Value, ' ', 07, taRightJustify, True) + '  ' +
      Geral.CompletaString(Geral.FFT(QrInadCREDITO.Value, 2, siPositivo), ' ', 10, taRightJustify, True)+ '  '+
      Geral.CompletaString(FormatDateTime(VAR_FORMATDATE3, QrInadVencimento.Value), ' ', 8, taRightJustify, True)+ '  '+
      //
      Txt_Multa +
      Txt_Juros +
      Txt_ATZ_VAL +
      //
      Geral.CompletaString(Geral.FFT(QrInadPAGO.Value, 2, siPositivo), ' ', 10, taRightJustify, True) + '  ' +
      Geral.CompletaString(Geral.FFT(QrInadPEND_VAL.Value, 2, siPositivo), ' ', 10, taRightJustify, True) + '  ' +
      L;
      //
      QrInad.Next;
    end;
    if QrInad.RecordCount > 1 then
    begin
      if FMultaJur = True then
      begin
        Txt_Tot_Multa   := Geral.CompletaString(Geral.FFT(Mul, 2, siPositivo), ' ', 10, taRightJustify, True) + '  ';
        Txt_Tot_Juros   := Geral.CompletaString(Geral.FFT(Jur, 2, siPositivo), ' ', 10, taRightJustify, True) + '  ';
        Txt_Tot_ATZ_VAL := Geral.CompletaString(Geral.FFT(Atz, 2, siPositivo), ' ', 10, taRightJustify, True) + '  ';
      end else
      begin
        Txt_Tot_Multa   := '';
        Txt_Tot_Juros   := '';
        Txt_Tot_ATZ_VAL := '';
      end;
      //
      if FMultaJur = True then
        Value := Value + '----------------------------------------------------------------------------------------------------------' + L
      else
        Value := Value + '----------------------------------------------------------------------' + L;
      //
      Value := Value +
      Geral.CompletaString('TOTAL',' ', 15, taRightJustify, True) + '  ' +
      Geral.CompletaString('', ' ', 07, taRightJustify, True) + '  ' +
      Geral.CompletaString(Geral.FFT(Val, 2, siPositivo), ' ', 10, taRightJustify, True)+ '  '+
      Geral.CompletaString('', ' ', 8, taRightJustify, True)+ '  '+
      //
      Txt_Tot_Multa +
      Txt_Tot_Juros +
      Txt_Tot_ATZ_VAL +
      //
      Geral.CompletaString(Geral.FFT(Pag, 2, siPositivo), ' ', 10, taRightJustify, True) + '  ' +
      Geral.CompletaString(Geral.FFT(Pen, 2, siPositivo), ' ', 10, taRightJustify, True) + '  ' +
      L;
    end;
  end
end;

procedure TFmCartasImp.QrCartoesARCalcFields(DataSet: TDataSet);
begin
  QrCartoesARCEP_TXT.Value := Geral.FormataCEP_NT(QrCartoesARCEP.Value);
  //
  QrCartoesARNUMERO_TXT.Value :=
    MLAGeral.FormataNumeroDeRua(QrCartoesARRUA.Value,
    Trunc(QrCartoesARNumero.Value), False);
  //
  QrCartoesAREndereco.Value := QrCartoesARNOMELOGRAD.Value;
  if Trim(QrCartoesAREndereco.Value) <> '' then QrCartoesAREndereco.Value :=
    QrCartoesAREndereco.Value + ' ';
  QrCartoesAREndereco.Value := QrCartoesAREndereco.Value + QrCartoesARRua.Value;
  if Trim(QrCartoesARRua.Value) <> '' then QrCartoesAREndereco.Value :=
    QrCartoesAREndereco.Value + ', ' + QrCartoesARNUMERO_TXT.Value;
  if Trim(QrCartoesARCompl.Value) <>  '' then QrCartoesAREndereco.Value :=
    QrCartoesAREndereco.Value + ' ' + QrCartoesARCompl.Value;
  if Trim(QrCartoesARBairro.Value) <>  '' then QrCartoesAREndereco.Value :=
    QrCartoesAREndereco.Value + ' - ' + QrCartoesARBairro.Value;
  //
  QrCartoesAREAN128.Value := '-C<' + Geral.FFN(QrCartoesARControle.Value, 11);
end;

procedure TFmCartasImp.QrCondComCartasAfterOpen(DataSet: TDataSet);
begin
  UnDmkDAC_PF.AbreQuery(QrSelConds, DModG.MyPID_DB);
end;

procedure TFmCartasImp.QrCondComCartasAfterScroll(DataSet: TDataSet);
begin
  ReabreCartasDeCond();
end;

procedure TFmCartasImp.QrCondComCartasBeforeClose(DataSet: TDataSet);
begin
  QrCartasDeCond.Close;
end;

procedure TFmCartasImp.QrEnvCartasAfterScroll(DataSet: TDataSet);
begin
  FCarta := QrEnvCartasCadastro.Value;
  FDataPesq := QrEnvCartasDataCad.Value;
  // Abrir unidade atual
  ReopenUnidades();
end;

procedure TFmCartasImp.QrSelCondsAfterOpen(DataSet: TDataSet);
begin
  BtImprime.Enabled := QrSelConds.RecordCount > 0;
end;

procedure TFmCartasImp.QrSelCondsBeforeClose(DataSet: TDataSet);
begin
  BtImprime.Enabled := False;
end;

procedure TFmCartasImp.QrUnidadesAfterScroll(DataSet: TDataSet);
begin
  ReopenInad();
end;

procedure TFmCartasImp.QrUnidadesBeforeClose(DataSet: TDataSet);
begin
  QrInad.Close;
end;

function TFmCartasImp.GeraCartasCobranca(PBx: TProgressBar): Boolean;
var
  LastExec: String;
  LastLotImp, Codigo, DiarCEMIts, Metodo: Integer;
begin
  LastExec := Geral.FDT(DModG.ObtemAgora(), 109);
  LastLotImp := UMyMod.BPGS1I32('diarcemenv', 'LastLotImp', '', '', tsPos, stIns, 0);
  //
  FMemoryStream := TMemoryStream.Create;
  //frxCarta.Clear;
  frxCarta.PreviewPages.Clear;
  frxCarta.OnGetValue := frxCNDC.OnGetValue;
  frxCarta.OnUserFunction := frxCNDC.OnUserFunction;
  frxCarta.ReportOptions.Name := 'Cartas para cond�minos';
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrEnvCartas, DModG.MyPID_DB, [
  'SELECT bol.Vencimento, bol.Data, bol.Mez, bol.FatNum,  ',
  'bol.Depto, bol.Propriet, bol.Usuario, bol.Empresa, ',
  'bol.EmprEnti, env.*  ',
  'FROM ' + TMeuDB + '.diarcemenv env ',
  'LEFT JOIN ' + TMeuDB + '.diarcembol bol ON bol.Codigo=env.Codigo ',
  'WHERE env.Metodo=' + Geral.FF0(CO_DIARIO_ENVIO_MENSAGEM_CARTA),
  'AND bol.Juridica=0',
  'AND env.ConsidEnvi < "1900-01-01" ',
  'AND env.AutoCancel < "1900-01-01" ',
  'AND env.DtaLimite >= SYSDATE() ',
  'AND bol.Empresa IN (',
  '     SELECT Codigo ',
  '     FROM ' + FCondComCartas,
  '     WHERE Ativo=1',
  ')',
  '']);
  //
  PBx.Position := 0;
  PBx.Max := QrEnvCartas.RecordCount;
  QrEnvCartas.First;
  while not QrEnvCartas.Eof do
  begin
    MyObjects.UpdPB(PBx, nil, nil);
    //
    //ReopenUnidades();
    // Mode a ser impresso
    FmCartasImpCG.FRGModoItemIndex := CO_INADIMPL_APENAS_COM_PENDENCIAS;
    //
    if GeraRelatorio(cfrgCompoe) then
    begin
      // Adicionar ao lote virtual "LastLotImp"
      Codigo     := QrEnvCartasCodigo.Value;
      DiarCEMIts := QrEnvCartasDiarCEMIts.Value;
      Metodo     := QrEnvCartasMetodo.Value;
      //
      UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'diarcemenv', False, [
      CO_JOKE_SQL, 'LastExec', 'LastLotImp'], [
      'Codigo', 'DiarCEMIts', 'Metodo'], [
      'Tentativas=Tentativas+1', LastExec, LastLotImp], [
      Codigo, DiarCEMIts, Metodo], True);
      //
    end
    else
      Exit;
    //
    QrEnvCartas.Next;
  end;
  FMemoryStream.Free;
  Application.CreateForm(TFmMeufrx, FmMeufrx);
  if CkDesign.Checked then
  begin
    frxCarta.DesignReport;
  end else
  begin
    frxCarta.Preview := FmMeufrx.PvVer;
    //
    FmMeufrx.PvVer.OutlineWidth := frxCarta.PreviewOptions.OutlineWidth;
    FmMeufrx.PvVer.Zoom := frxCarta.PreviewOptions.Zoom;
    FmMeufrx.PvVer.ZoomMode := frxCarta.PreviewOptions.ZoomMode;
    FmMeufrx.UpdateZoom;
    //
    FmMeufrx.ShowModal;
    FmMeufrx.Destroy;
  end;
  //
  // CARTOES AR
  //////////////////////////////////////////////////////////////////////////////
  UnDmkDAC_PF.AbreMySQLQuery0(QrCartoesAR, DModG.MyPID_DB, [
  'SELECT env.Controle, bol.Propriet, dci.CartaoAR, ',
  'IF (Tipo=0, prp.RazaoSocial, prp.Nome) NO_PRP, ',
  'IF (Tipo=0, elg.Nome, plg.Nome) NOMELOGRAD, ',
  'IF (Tipo=0, prp.ERua,    prp.PRua    ) RUA, ',
  'IF (Tipo=0, prp.ENumero+0.000, prp.PNumero+0.000 ) Numero, ',
  'IF (Tipo=0, prp.ECompl,  prp.PCompl  ) Compl, ',
  'IF (Tipo=0, prp.EBairro, prp.PBairro ) Bairro, ',
  'IF (Tipo=0, prp.ECidade, prp.PCidade ) Cidade, ',
  'IF (Tipo=0, prp.EUF,     prp.PUF     ) + 0.000 UF, ',
  'IF (Tipo=0, prp.ECEP,    prp.PCEP    ) + 0.000 CEP, ',
  'IF (Tipo=0, prp.EPais,   prp.PPais   ) Pais, ',
  'IF (Tipo=0, euf.Nome,     puf.Nome     ) NO_UF ',
  'FROM ' + TMeuDB + '.diarcemenv env ',
  'LEFT JOIN ' + TMeuDB + '.diarcembol bol ON bol.Codigo=env.Codigo ',
  'LEFT JOIN ' + TMeuDB + '.diarcemits dci ON dci.Controle=env.DiarCEMIts',
  'LEFT JOIN ' + TMeuDB + '.entidades prp ON prp.Codigo=bol.Propriet',
  'LEFT JOIN ' + TMeuDB + '.listalograd elg ON elg.Codigo=prp.ELograd',
  'LEFT JOIN ' + TMeuDB + '.listalograd plg ON plg.Codigo=prp.PLograd',
  'LEFT JOIN ' + TMeuDB + '.ufs euf ON euf.Codigo=prp.EUF',
  'LEFT JOIN ' + TMeuDB + '.ufs puf ON puf.Codigo=prp.PUF',
  'WHERE env.Metodo=' + Geral.FF0(CO_DIARIO_ENVIO_MENSAGEM_CARTA),
  'AND bol.Juridica=0',
  'AND env.ConsidEnvi < "1900-01-01" ',
  'AND env.AutoCancel < "1900-01-01" ',
  'AND env.DtaLimite >= SYSDATE() ',
  'AND bol.Empresa IN (',
  '     SELECT Codigo ',
  '     FROM ' + FCondComCartas,
  '     WHERE Ativo=1',
  ')',
  'AND dci.CartaoAR=1',
  '']);
  //
  MyObjects.frxMostra(frxCartoesAR, 'Cart�es AR');
  //
  MostraFormLastLotImp(LastLotImp);
end;

function TFmCartasImp.GeraCertidoes(SQL: String): Boolean;
begin
  QrUnidades.Close;
  QrUnidades.Filtered := FUnidadesFiltered;
  QrUnidades.SQL.Text := SQL;
  UnDmkDAC_PF.AbreQuery(QrUnidades, Dmod.MyDB);
  //
  GeraRelatorio(cfrgMostra);
end;

function TFmCartasImp.GeraRelatorio(ComoFastReportGera: TComoFastReportGera): Boolean;
var
  Continua: Integer;
begin
  Result := False;
  //
  frxDsInad.DataSet := QrInad;
  frxDsUnidades.DataSet := QrUnidades;
  //
  // Carta a ser impressa!
  QrTexto.Close;
  QrTexto.Params[0].AsInteger := FCarta;
  QrTexto.Open;
  //
  case QrTextoTipo.Value of
    0: Geral.MensagemBox('Defina o modelo de certid�o a ser usado!',
       'Aviso', MB_OK+MB_ICONWARNING);
    1:
    begin
      MyObjects.frxDefineDataSets(frxCNDC, [
        frxDsUnidades,
        frxDsInad,
        frxDsCartas,
        DmodG.frxDsDono
        ]);
      if FRGModoItemIndex in (
      [CO_INADIMPL_APENAS_COM_PENDENCIAS(*1*),
      CO_INADIMPL_COM_E_SEM_PENDENCIAS(*3*)])then
        Continua := Geral.MensagemBox('A pesquisa foi realizada ' +
        'incluindo unidades que podem ter pend�ncias! Deseja emitir o a ' +
        'certid�o negativa assim mesmo?',
        'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION)
      else Continua := ID_YES;
      if Continua = ID_YES then
      begin
        if ComoFastReportGera = cfrgCompoe then
        begin
          frxCNDC.SaveToStream(FMemoryStream);
          FMemoryStream.Position := 0;
          frxCarta.LoadFromStream(FMemoryStream);
          frxCarta.PrepareReport(False);
        end else
          MyObjects.frxMostra(frxCNDC, 'Certid�o negativa de d�bitos condominiais');
        //
        Result := True;
      end;
    end;
    2:
    begin
      MyObjects.frxDefineDataSets(frxCDCU, [
        frxDsUnidades,
        frxDsInad,
        frxDsCartas
        ]);
      if ComoFastReportGera = cfrgCompoe then
      begin
        frxCDCU.SaveToStream(FMemoryStream);
        FMemoryStream.Position := 0;
        frxCarta.LoadFromStream(FMemoryStream);
        frxCarta.PrepareReport(False);
      end else
        MyObjects.frxMostra(frxCDCU, 'Carta de cobran�a de d�bitos condominiais');
      //
      Result := True;
    end;
    else Geral.MensagemBox('Defina o modelo de certid�o a ser usado!',
       'Aviso', MB_OK+MB_ICONWARNING);
  end;
end;

function TFmCartasImp.IncorporaQryInad(Source_: TmySQLQuery;
  Source_MES: TWideStringField; Source_Data: TDateField; Source_FatNum: TFloatField;
  Source_Vencimento: TDateField; Source_Mez: TIntegerField;
  Source_Descricao: TWideStringField; Source_CREDITO, Source_PAGO, Source_Juros,
  Source_Multa, Source_TOTAL, Source_SALDO, Source_PEND_VAL, Source_VALOR,
  Source_ATZ_VAL: TFloatField): Boolean;
begin
  Result := False;
  try
    QrInad           := Source_;
    QrInadMES        := Source_MES;
    QrInadData       := Source_Data;
    QrInadFatNum     := Source_FatNum;
    QrInadVencimento := Source_Vencimento;
    QrInadMez        := Source_Mez;
    QrInadDescricao  := Source_Descricao;
    QrInadCREDITO    := Source_CREDITO;
    QrInadPAGO       := Source_PAGO;
    QrInadJuros      := Source_Juros;
    QrInadMulta      := Source_Multa;
    QrInadTOTAL      := Source_TOTAL;
    QrInadSALDO      := Source_SALDO;
    QrInadPEND_VAL   := Source_PEND_VAL;
    QrInadVALOR      := Source_VALOR;
    QrInadATZ_VAL    := Source_ATZ_VAL;
    //
    QrInad.SQL.Text:= Source_.SQL.Text;
    //
    Result := True;
  except
    Geral.MB_Erro('N�o foi poss�vel a incorpora��o da query "Inad"');
    raise;
  end;
end;

function TFmCartasImp.IncorporaQryUnidades(Source_: TmySQLQuery;
  Source_VALOR: TFloatField; Source_NOME_CON, Source_NOME_PRP,
  Source_Unidade: TWideStringField; Source_Depto: TIntegerField; Source_DOC_PRP,
  Source_NOMEUSU, Source_DOC_USU: TWideStringField; Source_Conta, Source_COD_PRP,
  Source_COD_CON: TIntegerField; Source_CEP_PRP, Source_UF_PRP: TFloatField;
  Source_MUNI_PRP: TWideStringField; Source_CliInt: TIntegerField): Boolean;
begin
  Result := False;
  try
    QrUnidades         := Source_;
    QrUnidadesVALOR    := Source_VALOR;
    QrUnidadesNOME_CON := Source_NOME_CON;
    QrUnidadesNOME_PRP := Source_NOME_PRP;
    QrUnidadesUnidade  := Source_Unidade;
    QrUnidadesDepto    := Source_Depto;
    QrUnidadesDOC_PRP  := Source_DOC_PRP;
    QrUnidadesNOMEUSU  := Source_NOMEUSU;
    QrUnidadesDOC_USU  := Source_DOC_USU;
    QrUnidadesConta    := Source_Conta;
    QrUnidadesCOD_PRP  := Source_COD_PRP;
    QrUnidadesCOD_CON  := Source_COD_CON;
    QrUnidadesCEP_PRP  := Source_CEP_PRP;
    QrUnidadesUF_PRP   := Source_UF_PRP;
    QrUnidadesMUNI_PRP := Source_MUNI_PRP;
    QrUnidadesCliInt   := Source_CliInt;
    //
    QrUnidades.SQL.Text:= Source_.SQL.Text;
    //
    Result := True;
  except
    Geral.MB_Erro('N�o foi poss�vel a incorpora��o da query "Unidades"');
    raise;
  end;
end;

procedure TFmCartasImp.ReabreCartasDeCond();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrCartasDeCond, Dmod.MyDB, [
  'SELECT bol.Vencimento, bol.Data, bol.Mez, bol.Depto, bol.FatNum, ',
  'bol.Codigo, bol.Propriet, bol.Usuario, Bol.Empresa, bol.EmprEnti, bol.Juridica, ',
  'env.DiarCEMIts, env.Metodo, env.Cadastro, env.ReInclu, env.DtaInicio, env.Dtalimite, ',
  'CONCAT(LPAD(bol.Mez MOD 100, 2, "0"), "/", LPAD(bol.Mez DIV 100, 2, "0"))  ',
  'MEZ_TXT, cim.Unidade DEPTO_TXT,  ',
  'IF(prp.Tipo=0, prp.RazaoSocial, prp.Nome) NO_PRP ',
  'FROM diarcemenv env ',
  'LEFT JOIN diarcembol bol ON bol.Codigo=env.Codigo ',
  'LEFT JOIN condimov cim ON cim.Conta=bol.Depto ',
  'LEFT JOIN entidades prp ON prp.Codigo=bol.Propriet ',
  'WHERE env.Metodo=' + Geral.FF0(CO_DIARIO_ENVIO_MENSAGEM_CARTA),
  'AND bol.Juridica=0 ',
  'AND env.ConsidEnvi < "1900-01-01"  ',
  'AND env.DtaLimite >= SYSDATE()  ',
  'AND bol.Empresa=' + Geral.FF0(QrCondComCartasCodigo.Value),
  '']);
end;

procedure TFmCartasImp.ReopenCondComCartas(Recria: Boolean; Codigo: Integer);
begin
  if Recria then
  begin
    FCondComCartas := UnCreateGeral.RecriaTempTableNovo(ntrtt_ItensPorCod, DmodG.QrUpdPID1, False);
    DBGrid1.SQLTable := FCondComCartas;
    //
    //UnDmkDAC_PF.AbreMySQLQuery0(QrCondComCartas, DModG.MyPID_DB, [
    UnDmkDAC_PF.ExecutaMySQLQuery0(QrCondComCartas, DmodG.MyPID_DB, [
    'DELETE FROM ' + FCondComCartas + ';',
    'INSERT INTO ' + FCondComCartas + '',
    'SELECT bol.Empresa, ',
    'IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NO_EMP,',
    'COUNT(bol.Empresa) ITENS, 1 Ativo',
    'FROM ' + TMeuDB + '.diarcemenv env ',
    'LEFT JOIN ' + TMeuDB + '.diarcembol bol ON bol.Codigo=env.Codigo ',
    'LEFT JOIN ' + TMeuDB + '.entidades emp ON emp.Codigo=bol.EmprEnti',
    'WHERE env.Metodo=' + Geral.FF0(CO_DIARIO_ENVIO_MENSAGEM_CARTA),
    'AND bol.Juridica=0',
    'AND env.ConsidEnvi < "1900-01-01" ',
    'AND env.AutoCancel < "1900-01-01" ',
    'AND env.DtaLimite >= SYSDATE() ',
    'GROUP BY bol.Empresa;',
{   N�o pode por cousa do click no Check do Grid
    'SELECT * ',
    'FROM ' + FCondComCartas + '',
    'ORDER BY Itens DESC, Nome;',
}
    '']);
  end;
  UnDmkDAC_PF.AbreMySQLQuery0(QrCondComCartas, DModG.MyPID_DB, [
  'SELECT * ',
  'FROM ' + FCondComCartas + '',
  'ORDER BY Itens DESC, Nome;',
  '']);
  //
  QrCondComCartas.Locate('Codigo', Codigo, []);
end;

procedure TFmCartasImp.ReopenInad();
  procedure ReopenBoletoEspecifico();
  var
    Entidade, EntiCond, Propriet, Depto, Mez: Integer;
    //Continua: Word;
    Data, Vencto, TabLctA: String;
    Filtered: Boolean;
    FatNum: Double;
  begin
    //Carta    := Geral.IMV(EdCarta.Text);
    EntiCond := QrEnvCartasEmpresa.Value; //Geral.IMV(EdEmpresa.Text);
    Entidade := QrEnvCartasEmprEnti.Value;
    Propriet := QrEnvCartasPropriet.Value;
    Depto    := QrEnvCartasDepto.Value;
    Data     := Geral.FDT(QrEnvCartasData.Value, 1);
    Vencto   := Geral.FDT(QrEnvCartasVencimento.Value, 1);
    Mez      := QrEnvCartasMez.Value;
    FatNum   := QrEnvCartasFatNum.Value;
    TabLctA  := dmkPF.NomeTabelaLtc(EntiCond, ttA);
    //
    QrInad.Close;
    QrInad.SQL.Text := UFinanceiro.Pesquisa4_Cartas(CO_INADIMPL_BOLETO_ESPECIFICO,
      Data, Vencto, EntiCond, Entidade, Depto, Propriet, Mez, FatNum, TabLctA, Filtered);
    QrInad.Filtered := Filtered;
    UnDmkDAC_PF.AbreQuery(QrInad, Dmod.MyDB);
  end;
  procedure ReopenBoletoCertidao();
  const
    Data = '0000-00-00';
    Mez = 0;
    FatNum = 0.000;
  var
    Entidade, EntiCond, Propriet, Depto: Integer;
    //Continua: Word;
    Vencto, TabLctA: String;
    Filtered: Boolean;
  begin
    //Carta    := Geral.IMV(EdCarta.Text);
    EntiCond := FEmpresa;
    Entidade := FEmprEnti;
    Propriet := FPropriet;
    Depto    := QrUnidadesDepto.Value;
    Vencto   := Geral.FDT(FDataPesq, 1);
    TabLctA  := FTabLctA;
    //
    QrInad.Close;
    QrInad.SQL.Text := UFinanceiro.Pesquisa4_Cartas(FRGModoItemIndex, Data, Vencto,
      EntiCond, Entidade, Depto, Propriet, Mez, FatNum, TabLctA, Filtered);
    QrInad.Filtered := Filtered;
    UnDmkDAC_PF.AbreQuery(QrInad, Dmod.MyDB);
  end;
begin
  case FTipoFormCartasImp of
    tfciCertidaoNeg: ReopenBoletoCertidao();
    tfciCobrancaGer: ReopenBoletoEspecifico();
    else Geral.MB_Aviso('"FTipoFormCartasImp" n�o definido em "ReopenInad()"');
  end;
end;

procedure TFmCartasImp.MostraFormLastLotImp(LotImp: Int64);
begin
  if DBCheck.CriaFm(TFmCartasEnv, FmCartasEnv, afmoNegarComAviso) then
  begin
    FmCartasEnv.ReopenLastlotImp(LotImp, 0, 0, 0);
    FmCartasEnv.ShowModal;
    ReopenCondComCartas(True, QrCondComCartasCodigo.Value);
    FmCartasEnv.Destroy;
  end;
end;

procedure TFmCartasImp.ReopenUnidades();
begin
  case FTipoFormCartasImp of
    tfciCertidaoNeg:
    begin
    // N�o precisa! Abre apenas uma vez! J� abre na function principal de impress�o!
    end;
    tfciCobrancaGer:
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(QrUnidades, Dmod.MyDB, [
      'SELECT 0.000 VALOR, ',
      'IF(con.Tipo=0,con.RazaoSocial, con.Nome) NOME_CON, ',
      'con.Codigo COD_CON, ',
      'IF(prp.Tipo=0, prp.RazaoSocial, prp.Nome) NOME_PRP, ',
      'prp.Codigo COD_PRP, IF(prp.Tipo=0, prp.ECEP, prp.PCEP) + 0.000 CEP_PRP, ',
      'IF(prp.Tipo=0, prp.CNPJ, prp.CPF) DOC_PRP, ',
      'IF(prp.Tipo=0, prp.EUF, prp.PUF) + 0.000 UF_PRP, ',
      'IF(LENGTH(IF(prp.Tipo=0, prp.ECidade, prp.PCidade))=0, ',
      'IF(prp.Tipo=0, dme.Nome, dmp.Nome), ',
      'IF(prp.Tipo=0, prp.ECidade, prp.PCidade)) MUNI_PRP, ',
      'IF(usu.Tipo=0, usu.RazaoSocial, usu.Nome) NOMEUSU, ',
      'IF(usu.Tipo=0, usu.CNPJ, usu.CPF) DOC_USU, ',
      'imv.Unidade, imv.Conta, imv.Conta Depto, imv.Codigo CliInt ',
      'FROM condimov  imv ',
      'LEFT JOIN entidades con ON con.Codigo=imv.Codigo ',
      'LEFT JOIN entidades prp ON prp.Codigo=imv.Propriet ',
      'LEFT JOIN locsyndicall.dtb_munici dme ON dme.Codigo=prp.ECodMunici ',
      'LEFT JOIN locsyndicall.dtb_munici dmp ON dmp.Codigo=prp.PCodMunici ',
      'LEFT JOIN entidades usu ON usu.Codigo=imv.Usuario ',
      'LEFT JOIN locsyndicall.dtb_munici mue ON mue.Codigo=prp.ECodMunici ',
      'LEFT JOIN locsyndicall.dtb_munici mup ON mup.Codigo=prp.PCodMunici ',
      'WHERE imv.Conta=' + Geral.FF0(QrEnvCartasDepto.Value),
      '']);
    end;
    else Geral.MB_Aviso('"FTipoFormCartasImp" n�o definido em "ReopenUnidades()"');
  end;
end;

end.
