unit UnWTextos_Jan;

interface

uses
  Windows, SysUtils, Classes, DB, mySQLDbTables, DmkGeral, DmkDAC_PF, Forms,
  UnInternalConsts, UnDmkProcFunc, Menus, dmkDBLookupComboBox, UnDmkEnums;

type
  TUnWTextos_Jan = class(TObject)
  private
    { Private declarations }
  public
    { Public declarations }
    procedure MostraFormWTermos(Form: TForm; Codigo: Integer);
    procedure MostraFormWTextosOpc(Form: TForm);
  end;

var
  WTextos_Jan: TUnWTextos_Jan;


implementation

uses MyDBCheck, Module, WTermos, WTextosOpc, UnGrlUsuarios;

{ TUnWTextos_Jan }

procedure TUnWTextos_Jan.MostraFormWTermos(Form: TForm; Codigo: Integer);
begin
  if GrlUsuarios.Efetuar_Login(Form, stDesktop, Dmod.MyDB, Dmod.QrUpd,
    Dmod.QrAux, VAR_USUARIO) then
  begin
    if DBCheck.CriaFm(TFmWTermos, FmWTermos, afmoNegarComAviso) then
    begin
      if Codigo <> 0 then
        FmWTermos.LocCod(Codigo, Codigo);
      FmWTermos.ShowModal;
      FmWTermos.Destroy;
    end;
  end;
end;

procedure TUnWTextos_Jan.MostraFormWTextosOpc(Form: TForm);
begin
  if GrlUsuarios.Efetuar_Login(Form, stDesktop, Dmod.MyDB, Dmod.QrUpd,
    Dmod.QrAux, VAR_USUARIO) then
  begin
    if DBCheck.CriaFm(TFmWTextosOpc, FmWTextosOpc, afmoSoBoss) then
    begin
      FmWTextosOpc.ShowModal;
      FmWTextosOpc.Destroy;
    end;
  end;
end;

end.
