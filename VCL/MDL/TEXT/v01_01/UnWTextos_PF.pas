unit UnWTextos_PF;

interface

uses mySQLDbTables, UMySQLModule, dmkGeral, dmkImage, Forms, Controls, Windows,
  SysUtils, ComCtrls, DB, UnDmkProcFunc, ExtCtrls, Math, UnDmkEnums, Classes;

type
  TUnWTextos_PF = class(TObject)
  private
    { Private declarations }
  public
    { Public declarations }
    function  ConfiguraStatus(MostraBranco: Boolean = False): TStringList;
    function  ObtemVersaoTermo(CodUsu: Integer): Integer;
    procedure ReopenWTextosOpc(Query: TmySQLQuery);
  end;

var
  UWTextos_PF: TUnWTextos_PF;

implementation

uses Module, DmkDAC_PF;

{ TUnWTextos_PF }

function TUnWTextos_PF.ObtemVersaoTermo(CodUsu: Integer): Integer;
var
  Qry: TmySQLQuery;
begin
  Result := 1;
  //
  if CodUsu <> 0 then
  begin
    Qry := TmySQLQuery.Create(Dmod);
    try
      UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDBn, [
        'SELECT MAX(Versao) Versao ',
        'FROM wtextos ',
        'WHERE CodUsu = ' + Geral.FF0(CodUsu),
        '']);
      if Qry.RecordCount > 0 then
        Result := Qry.FieldByName('Versao').AsInteger + 1;
    finally
      Qry.Free;
    end;
  end;
end;

procedure TUnWTextos_PF.ReopenWTextosOpc(Query: TmySQLQuery);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(Query, Dmod.MyDBn, [
    'SELECT dir.FTPConfig, dir.Caminho, dir.Nivel, opc.* ',
    'FROM wtextosopc opc ',
    'LEFT JOIN ftpwebdir dir ON dir.Codigo = opc.DirWebPad ',
    '']);
end;

function TUnWTextos_PF.ConfiguraStatus(MostraBranco: Boolean): TStringList;
var
  Status: TStringList;
begin
  Status := TStringList.Create;
  try
    if MostraBranco = True then
      Status.Add('N�o informado');
    Status.Add('Cadastrando');
    Status.Add('Aguardando revis�o');
    Status.Add('Finalizado');
  finally
    Result := Status;
  end;
end;

end.
