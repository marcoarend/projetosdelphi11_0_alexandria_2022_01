unit CartaG;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls, Db, (*DBTables,*) JPEG, ExtDlgs,
  ZCF2, ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt, DmkDAC_PF,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, Grids,
  DBGrids, ComCtrls, dmkGeral, dmkEdit, dmkLabel, UnDmkProcFunc, dmkImage,
  UnDmkEnums, UnDmkListas;

type
  TFmCartaG = class(TForm)
    PainelDados: TPanel;
    DsCartaG: TDataSource;
    QrCartaG: TmySQLQuery;
    PainelEdita: TPanel;
    PainelEdit: TPanel;
    Label9: TLabel;
    EdCodigo: TdmkEdit;
    EdNome: TdmkEdit;
    Label10: TLabel;
    PainelData: TPanel;
    Label1: TLabel;
    DBEdCodigo: TDBEdit;
    DBEdNome: TDBEdit;
    Label2: TLabel;
    QrCartaGCodigo: TIntegerField;
    QrCartaGNome: TWideStringField;
    QrCartaGLk: TIntegerField;
    QrCartaGDataCad: TDateField;
    QrCartaGDataAlt: TDateField;
    QrCartaGUserCad: TIntegerField;
    QrCartaGUserAlt: TIntegerField;
    QrCartas: TmySQLQuery;
    DsCartas: TDataSource;
    QrCartasCodigo: TIntegerField;
    QrCartasTitulo: TWideStringField;
    QrCartasTexto: TWideMemoField;
    QrCartasLk: TIntegerField;
    QrCartasDataCad: TDateField;
    QrCartasDataAlt: TDateField;
    QrCartasUserCad: TIntegerField;
    QrCartasUserAlt: TIntegerField;
    QrCartasCartaG: TIntegerField;
    QrCartasPagina: TIntegerField;
    DBGrid1: TDBGrid;
    Splitter1: TSplitter;
    DBRichEdit1: TDBRichEdit;
    QrCartasTipo: TIntegerField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    BtExclui: TBitBtn;
    BtAltera: TBitBtn;
    BtInclui: TBitBtn;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtIncluiClick(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrCartaGAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure QrCartaGAfterScroll(DataSet: TDataSet);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrCartaGBeforeOpen(DataSet: TDataSet);
    procedure SbImprimeClick(Sender: TObject);
    procedure SbNovoClick(Sender: TObject);
    procedure BtExcluiClick(Sender: TObject);
  private
    procedure CriaOForm;
//    procedure SubQuery1Reopen;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure IncluiRegistro;
    procedure AlteraRegistro;
//    procedure IncluiSubRegistro;
//    procedure ExcluiSubRegistro;
//    procedure AlteraSubRegistro;
//    procedure TravaOForm;
   ////Procedures do form
    procedure MostraEdicao(Mostra: Boolean; SQLType: TSQLType; Codigo: Integer);
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    //
  public
    { Public declarations }
    FTipo: Integer;
    procedure LocCod(Atual, Codigo: Integer);
  end;

var
  FmCartaG: TFmCartaG;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmCartaG.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmCartaG.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrCartaGCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmCartaG.DefParams;
begin
  VAR_GOTOTABELA := 'CartaG';
  VAR_GOTOMYSQLTABLE := QrCartaG;
  VAR_GOTONEG := gotoPiZ;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;
  VAR_SQLx.Add('SELECT *');
  VAR_SQLx.Add('FROM cartag');
  VAR_SQLx.Add('WHERE Codigo > 0');
  //
  VAR_SQL1.Add('AND Codigo=:P0');
  //
  VAR_SQLa.Add('AND Nome Like :P0');
  //
end;

procedure TFmCartaG.MostraEdicao(Mostra: Boolean; SQLType: TSQLType; Codigo: Integer);
begin
  if Mostra then
  begin
    PainelEdita.Visible := True;
    PainelDados.Visible := False;
    EdNome.Text := CO_VAZIO;
    EdNome.Visible := True;
    if SQLType = stIns then
    begin
      EdCodigo.Text := FormatFloat(FFormatFloat, Codigo);
      EdNome.Text   := '';
    end else begin
      EdCodigo.Text := DBEdCodigo.Text;
      EdNome.Text   := DBEdNome.Text;
    end;
    EdNome.SetFocus;
  end else begin
    PainelDados.Visible := True;
    PainelEdita.Visible := False;
  end;
  ImgTipo.SQLType := SQLType;
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmCartaG.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmCartaG.AlteraRegistro;
var
  CartaG : Integer;
begin
  CartaG := QrCartaGCodigo.Value;
  if QrCartaGCodigo.Value = 0 then
  begin
    BtAltera.Enabled := False;
    Exit;
  end;
  if not UMyMod.SelLockY(CartaG, Dmod.MyDB, 'CartaG', 'Codigo') then
  begin
    try
      UMyMod.UpdLockY(CartaG, Dmod.MyDB, 'CartaG', 'Codigo');
      MostraEdicao(True, stUpd, 0);
    finally
      Screen.Cursor := Cursor;
    end;
  end;
end;

procedure TFmCartaG.IncluiRegistro;
var
  Cursor : TCursor;
  CartaG : Integer;
begin
  Cursor := Screen.Cursor;
  Screen.Cursor := crHourglass;
  Refresh;
  try
    CartaG := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
    'CartaG', 'CartaG', 'Codigo');
    if Length(FormatFloat(FFormatFloat, CartaG))>Length(FFormatFloat) then
    begin
      Geral.MensagemBox(
      'Inclus�o cancelada. Limite de cadastros extrapolado', 'Erro',
      MB_OK+MB_ICONERROR);
      Screen.Cursor := Cursor;
      Exit;
    end;
    MostraEdicao(True, stIns, CartaG);
  finally
    Screen.Cursor := Cursor;
  end;
end;

procedure TFmCartaG.QueryPrincipalAfterOpen;
begin
end;

procedure TFmCartaG.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmCartaG.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmCartaG.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmCartaG.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmCartaG.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmCartaG.BtIncluiClick(Sender: TObject);
begin
  IncluiRegistro;
end;

procedure TFmCartaG.BtAlteraClick(Sender: TObject);
begin
  AlteraRegistro;
end;

procedure TFmCartaG.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrCartaGCodigo.Value;
  Close;
end;

procedure TFmCartaG.BtConfirmaClick(Sender: TObject);
var
  Codigo : Integer;
  Nome : String;
begin
  Nome := EdNome.Text;
  if Length(Nome) = 0 then
  begin
    Geral.MensagemBox('Defina uma descri��o.', 'Erro', MB_OK+MB_ICONERROR);
    Exit;
  end;
  Codigo := Geral.IMV(EdCodigo.Text);
  Dmod.QrUpdU.SQL.Clear;
  if ImgTipo.SQLType = stIns then
    Dmod.QrUpdU.SQL.Add('INSERT INTO cartag SET ')
  else Dmod.QrUpdU.SQL.Add('UPDATE cartag SET ');
  Dmod.QrUpdU.SQL.Add('Nome=:P0,');
  //
  if ImgTipo.SQLType = stIns then
    Dmod.QrUpdU.SQL.Add('DataCad=:Px, UserCad=:Py, Codigo=:Pz')
  else Dmod.QrUpdU.SQL.Add('DataAlt=:Px, UserAlt=:Py WHERE Codigo=:Pz');
  Dmod.QrUpdU.Params[00].AsString  := Nome;
  Dmod.QrUpdU.Params[01].AsString  := FormatDateTime(VAR_FORMATDATE, Date);
  Dmod.QrUpdU.Params[02].AsInteger := VAR_USUARIO;
  Dmod.QrUpdU.Params[03].AsInteger := Codigo;
  Dmod.QrUpdU.ExecSQL;
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'CartaG', 'Codigo');
  MostraEdicao(False, stLok, 0);
  LocCod(Codigo,Codigo);
end;

procedure TFmCartaG.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := Geral.IMV(EdCodigo.Text);
  if ImgTipo.SQLType = stIns then UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'CartaG', Codigo);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'CartaG', 'Codigo');
  MostraEdicao(False, stLok, 0);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'CartaG', 'Codigo');
end;

procedure TFmCartaG.BtExcluiClick(Sender: TObject);
begin
//
end;

procedure TFmCartaG.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //PainelEdita.Align := alClient;
  DBRichEdit1.Align := alClient;
  CriaOForm;
end;

procedure TFmCartaG.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrCartaGCodigo.Value,LaRegistro.Caption);
end;

procedure TFmCartaG.SbImprimeClick(Sender: TObject);
begin
//
end;

procedure TFmCartaG.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmCartaG.SbNovoClick(Sender: TObject);
begin
//
end;

procedure TFmCartaG.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmCartaG.QrCartaGAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmCartaG.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //if UMyMod.NegaInclusaoY(Dmod.MyDB, 'Controle', 'CartaG', 'Livres', 99) then
  //BtInclui.Enabled := False;
  Caption := DmkListas.DefineTituloCarta(FTipo, True);
end;

procedure TFmCartaG.QrCartaGAfterScroll(DataSet: TDataSet);
begin
  BtAltera.Enabled := GOTOy.BtEnabled(QrCartaGCodigo.Value, False);
  //
  QrCartas.Close;
  QrCartas.Params[0].AsInteger := QrCartaGCodigo.Value;
  UnDmkDAC_PF.AbreQuery(QrCartas, Dmod.MyDB);
end;

procedure TFmCartaG.SbQueryClick(Sender: TObject);
begin
  LocCod(QrCartaGCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'CartaG', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmCartaG.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], 'TXT-CARTA-002 :: ' +
  DmkListas.DefineTituloCarta(FTipo, True), True, taCenter, 2, 10, 20);
end;

procedure TFmCartaG.QrCartaGBeforeOpen(DataSet: TDataSet);
begin
  QrCartaGCodigo.DisplayFormat := FFormatFloat;
end;

end.

