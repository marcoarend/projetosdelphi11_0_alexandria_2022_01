object FmCartas: TFmCartas
  Left = 358
  Top = 229
  Caption = 'TXT-CARTA-001 :: Cadastro de Textos de Cartas'
  ClientHeight = 534
  ClientWidth = 880
  Color = clBtnFace
  Constraints.MinHeight = 320
  Constraints.MinWidth = 788
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -14
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 120
  TextHeight = 16
  object PainelEdita: TPanel
    Left = 0
    Top = 118
    Width = 880
    Height = 416
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
    Visible = False
    object Label9: TLabel
      Left = 10
      Top = 5
      Width = 47
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'C'#243'digo:'
    end
    object Label10: TLabel
      Left = 138
      Top = 5
      Width = 65
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Descri'#231#227'o:'
    end
    object Label3: TLabel
      Left = 482
      Top = 54
      Width = 68
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Seq'#252#234'ncia:'
    end
    object Label5: TLabel
      Left = 10
      Top = 54
      Width = 40
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Grupo:'
    end
    object EdCodigo: TdmkEdit
      Left = 10
      Top = 25
      Width = 123
      Height = 25
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Alignment = taRightJustify
      Font.Charset = DEFAULT_CHARSET
      Font.Color = 8281908
      Font.Height = -15
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      ReadOnly = True
      TabOrder = 0
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 2
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '00'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object EdNome: TdmkEdit
      Left = 138
      Top = 25
      Width = 731
      Height = 25
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      MaxLength = 100
      TabOrder = 1
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      ValWarn = False
    end
    object EdOrdem: TdmkEdit
      Left = 482
      Top = 74
      Width = 80
      Height = 26
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Alignment = taRightJustify
      TabOrder = 4
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object CBGrupo: TdmkDBLookupComboBox
      Left = 94
      Top = 74
      Width = 385
      Height = 24
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      KeyField = 'Codigo'
      ListField = 'Nome'
      ListSource = DsCartasG
      TabOrder = 3
      dmkEditCB = EdGrupo
      UpdType = utYes
      LocF7SQLMasc = '$#'
    end
    object EdGrupo: TdmkEditCB
      Left = 10
      Top = 74
      Width = 80
      Height = 26
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Alignment = taRightJustify
      TabOrder = 2
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      DBLookupComboBox = CBGrupo
      IgnoraDBLookupComboBox = False
    end
    object RGTipoTexto: TRadioGroup
      Left = 10
      Top = 108
      Width = 858
      Height = 80
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = ' Tipo de Texto: '
      Items.Strings = (
        'Inativo')
      TabOrder = 5
    end
    object GBConfirma: TGroupBox
      Left = 0
      Top = 338
      Width = 880
      Height = 78
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alBottom
      TabOrder = 6
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 15
        Top = 21
        Width = 111
        Height = 49
        Cursor = crHandPoint
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object Panel6: TPanel
        Left = 745
        Top = 18
        Width = 133
        Height = 58
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 9
          Top = 2
          Width = 110
          Height = 50
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
    end
  end
  object PainelDados: TPanel
    Left = 0
    Top = 118
    Width = 880
    Height = 416
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    object Panel1: TPanel
      Left = 0
      Top = 0
      Width = 880
      Height = 108
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alTop
      BevelOuter = bvNone
      Enabled = False
      TabOrder = 0
      object Label1: TLabel
        Left = 10
        Top = 5
        Width = 47
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'C'#243'digo:'
        FocusControl = DBEdCodigo
      end
      object Label2: TLabel
        Left = 138
        Top = 5
        Width = 65
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Descri'#231#227'o:'
        FocusControl = DBEdNome
      end
      object Label4: TLabel
        Left = 10
        Top = 54
        Width = 40
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Grupo:'
        FocusControl = DBEdit1
      end
      object Label6: TLabel
        Left = 793
        Top = 54
        Width = 68
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Seq'#252#234'ncia:'
        FocusControl = DBEdit2
      end
      object Label7: TLabel
        Left = 542
        Top = 54
        Width = 81
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Tipo de texto:'
        FocusControl = DBEdit3
      end
      object DBEdCodigo: TDBEdit
        Left = 10
        Top = 25
        Width = 123
        Height = 24
        Hint = 'N'#186' do banco'
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        TabStop = False
        DataField = 'Codigo'
        DataSource = DsCartas
        Font.Charset = DEFAULT_CHARSET
        Font.Color = 8281908
        Font.Height = -15
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 0
      end
      object DBEdNome: TDBEdit
        Left = 138
        Top = 25
        Width = 731
        Height = 24
        Hint = 'Nome do banco'
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        DataField = 'Titulo'
        DataSource = DsCartas
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
      end
      object DBEdit1: TDBEdit
        Left = 10
        Top = 74
        Width = 528
        Height = 24
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        DataField = 'NOMECARTAG'
        DataSource = DsCartas
        TabOrder = 2
      end
      object DBEdit2: TDBEdit
        Left = 793
        Top = 74
        Width = 78
        Height = 24
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        DataField = 'Pagina'
        DataSource = DsCartas
        TabOrder = 3
      end
      object DBEdit3: TDBEdit
        Left = 542
        Top = 74
        Width = 246
        Height = 24
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        DataField = 'NOMETIPO'
        DataSource = DsCartas
        TabOrder = 4
      end
    end
    object GBCntrl: TGroupBox
      Left = 0
      Top = 337
      Width = 880
      Height = 79
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alBottom
      TabOrder = 1
      object Panel5: TPanel
        Left = 2
        Top = 18
        Width = 212
        Height = 59
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 158
          Top = 5
          Width = 49
          Height = 49
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 108
          Top = 5
          Width = 50
          Height = 49
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 59
          Top = 5
          Width = 49
          Height = 49
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 10
          Top = 5
          Width = 49
          Height = 49
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 214
        Top = 18
        Width = 35
        Height = 20
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel3: TPanel
        Left = 298
        Top = 18
        Width = 580
        Height = 59
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object Panel2: TPanel
          Left = 453
          Top = 0
          Width = 127
          Height = 59
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 0
          object BtSaida: TBitBtn
            Tag = 13
            Left = 5
            Top = 5
            Width = 111
            Height = 49
            Cursor = crHandPoint
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
        object BtInclui: TBitBtn
          Tag = 10
          Left = 5
          Top = 5
          Width = 111
          Height = 49
          Cursor = crHandPoint
          Hint = 'Inclui novo banco'
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Inclui'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtIncluiClick
        end
        object BtAltera: TBitBtn
          Tag = 11
          Left = 118
          Top = 5
          Width = 111
          Height = 49
          Cursor = crHandPoint
          Hint = 'Altera banco atual'
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Altera'
          Enabled = False
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = BtAlteraClick
        end
        object BtItens: TBitBtn
          Tag = 121
          Left = 231
          Top = 5
          Width = 111
          Height = 49
          Cursor = crHandPoint
          Hint = 'Edita texto'
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Texto'
          Enabled = False
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = BtItensClick
        end
        object BtExclui: TBitBtn
          Tag = 12
          Left = 345
          Top = 5
          Width = 110
          Height = 49
          Cursor = crHandPoint
          Hint = 'Exclui banco atual'
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Exclui'
          Enabled = False
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 4
          OnClick = BtExcluiClick
        end
      end
    end
    object RETxtCartas: TRichEdit
      Left = 0
      Top = 246
      Width = 880
      Height = 91
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alBottom
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      Lines.Strings = (
        'RichEdit1')
      ParentFont = False
      ReadOnly = True
      TabOrder = 2
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 880
    Height = 64
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 821
      Top = 0
      Width = 59
      Height = 64
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 10
        Top = 15
        Width = 39
        Height = 39
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 266
      Height = 64
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 5
        Top = 10
        Width = 49
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 0
        OnClick = SbImprimeClick
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 57
        Top = 10
        Width = 49
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 108
        Top = 10
        Width = 50
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 160
        Top = 10
        Width = 49
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 212
        Top = 10
        Width = 49
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 266
      Top = 0
      Width = 555
      Height = 64
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 9
        Top = 11
        Width = 422
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Cadastro de Textos de Cartas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 11
        Top = 14
        Width = 422
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Cadastro de Textos de Cartas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 10
        Top = 12
        Width = 422
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Cadastro de Textos de Cartas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 64
    Width = 880
    Height = 54
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 18
      Width = 876
      Height = 34
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 16
        Top = 2
        Width = 15
        Height = 19
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -17
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 15
        Top = 1
        Width = 15
        Height = 19
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -17
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object DsCartas: TDataSource
    DataSet = QrCartas
    Left = 516
    Top = 13
  end
  object QrCartas: TmySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrCartasBeforeOpen
    AfterOpen = QrCartasAfterOpen
    BeforeClose = QrCartasBeforeClose
    AfterClose = QrCartasAfterClose
    AfterScroll = QrCartasAfterScroll
    OnCalcFields = QrCartasCalcFields
    SQL.Strings = (
      'SELECT cg.Nome NOMECARTAG, ca.* FROM cartas ca'
      'LEFT JOIN cartag cg ON cg.Codigo=ca.CartaG'
      'WHERE ca.Codigo > 0')
    Left = 488
    Top = 13
    object QrCartasCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCartasTitulo: TWideStringField
      FieldName = 'Titulo'
      Size = 100
    end
    object QrCartasTexto: TWideMemoField
      FieldName = 'Texto'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrCartasLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrCartasDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrCartasDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrCartasUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrCartasUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrCartasCartaG: TIntegerField
      FieldName = 'CartaG'
    end
    object QrCartasPagina: TIntegerField
      FieldName = 'Pagina'
    end
    object QrCartasNOMECARTAG: TWideStringField
      FieldName = 'NOMECARTAG'
      Size = 50
    end
    object QrCartasTipo: TIntegerField
      FieldName = 'Tipo'
      Required = True
    end
    object QrCartasNOMETIPO: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMETIPO'
      Size = 100
      Calculated = True
    end
  end
  object TbTexto: TmySQLTable
    Database = Dmod.MyDB
    TableName = 'cartas'
    Left = 460
    Top = 134
    object TbTextoTexto: TWideMemoField
      FieldName = 'Texto'
      BlobType = ftWideMemo
      Size = 4
    end
    object TbTextoCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object TbTextoTitulo: TWideStringField
      FieldName = 'Titulo'
      Size = 100
    end
  end
  object DsTexto: TDataSource
    DataSet = TbTexto
    Left = 488
    Top = 134
  end
  object QrCartasG: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM cartag'
      'ORDER BY Nome')
    Left = 168
    Top = 144
    object QrCartasGCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCartasGNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
    object QrCartasGLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrCartasGDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrCartasGDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrCartasGUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrCartasGUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
  end
  object DsCartasG: TDataSource
    DataSet = QrCartasG
    Left = 196
    Top = 144
  end
end
