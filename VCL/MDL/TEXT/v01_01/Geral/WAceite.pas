unit WAceite;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, Vcl.OleCtrls, SHDocVw,
  dmkCheckBox, Xml.xmldom, Xml.XMLIntf, Xml.Win.msxmldom, Xml.XMLDoc;

type
  TFmWAceite = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    WebBrowser1: TWebBrowser;
    XMLDocument1: TXMLDocument;
    CkAceito: TdmkCheckBox;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure CkAceitoClick(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    procedure CarregaTextoWeb();
  public
    { Public declarations }
    FHashArq: String;
    FUsuario, FTermo: Integer;
    FDataHora: TDateTime;
    FAceitou: Boolean;
  end;

  var
    FmWAceite: TFmWAceite;
  const
    FURL = 'http://www.dermatek.net.br/dmkREST.php';

implementation

uses UnMyObjects, {$IFNDEF NAO_USA_DB_GERAL} ModuleGeral, {$ENDIF} UnDmkWeb;

{$R *.DFM}

procedure TFmWAceite.BtOKClick(Sender: TObject);
begin
  {$IFNDEF NAO_USA_DB_GERAL}
  FDataHora := DModG.ObtemAgora;
  {$ELSE}
  FDataHora := Now;
  {$ENDIF}
  FUsuario  := VAR_USUARIO;
  FAceitou  := True;
  //
  Close;
end;

procedure TFmWAceite.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmWAceite.CarregaTextoWeb;

  function TiraQuebras(Texto: String): String;
  var
    I: Integer;
  begin
    Result := '';
    for I := 1 to Length(Texto) do
    begin
      case Ord(Texto[I]) of
        10: Result := Result + '</br>';
        13: ;//nada
        else Result := Result + Texto[I];
      end;
    end;
    Result := Trim(Result);
  end;

var
  XML: TStringStream;
  i: Integer;
  Texto: String;
begin
  XML := TStringStream.Create('');
  //
  try
    if DmkWeb.URLPost(FURL,
      [
      'method=REST_Dmk_ObtemTexto',
      'Texto=' + Geral.FF0(FTermo)
      ], XML) then
    begin
      XMLDocument1.LoadFromStream(XML);
      //
      for i := 0 to XMLDocument1.DocumentElement.ChildNodes.Count - 1 do
      begin
        with XMLDocument1.DocumentElement.ChildNodes[i] do
        begin
          FHashArq := ChildNodes['HashArq'].Text;
          Texto    := ChildNodes['Texto'].Text;
          Texto    := TiraQuebras(Texto);
          Texto    := '<html xmlns="http://www.w3.org/1999/xhtml"><head><title>Aceita��o de termo</title><meta content="text/html; charset=iso-8859-1" http-equiv="Content-Type" /></head><body>' + Texto + '</body></html>';
          //
          DmkWeb.CarregaTextNoWebBrowser(WebBrowser1, Texto);
        end;
      end;
    end;
  finally
    XML.Free;
  end;
end;

procedure TFmWAceite.CkAceitoClick(Sender: TObject);
begin
  BtOK.Enabled := CkAceito.Checked;
end;

procedure TFmWAceite.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmWAceite.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  FAceitou        := False;
end;

procedure TFmWAceite.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmWAceite.FormShow(Sender: TObject);
begin
  try
    FHashArq := '';
    //
    CarregaTextoWeb();
    //
    CkAceito.Visible := True;
  except
    CkAceito.Visible := False;
  end;
end;

end.
