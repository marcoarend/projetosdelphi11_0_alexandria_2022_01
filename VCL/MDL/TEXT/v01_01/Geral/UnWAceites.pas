unit UnWAceites;

interface

uses dmkImage, Forms, Controls, Windows, SysUtils, ComCtrls, ExtCtrls, dmkGeral,
  UnDmkEnums;

type
  TAceite = (istVisNenhum, istCfgBoleto, istTZD_UTC_Auto, istIBPTax);
  TUnWAceites = class(TObject)
  private
  public
    function  AceitaTermos(const TipoAceite: TAceite; var Termo,
                Usuario: Integer; var DataHora: TDateTime): Boolean;
    function  AceitaTermos2(const Termo_ID: Integer; var Termo, Usuario: Integer;
                var HashTermo: String; var DataHora: TDateTime): Boolean;
  end;

var
  UWAceites: TUnWAceites;

implementation

uses WAceite;

{ TUnWAceites }

function TUnWAceites.AceitaTermos(const TipoAceite: TAceite; var Termo,
  Usuario: Integer; var DataHora: TDateTime): Boolean;
var
  Aceite: Integer;
  Aceitou: Boolean;
begin
  Termo    := 0;
  Usuario  := 0;
  DataHora := 0;
  Result   := False;
  //
  case TipoAceite of //C�digo do termo cadastrado no site Dermatek sempre que mudar o termo mudar aqui
    istVisNenhum:
      Aceite := 0;
    istCfgBoleto:
      Aceite := 10; //Termo de configura��o de boleto
    istTZD_UTC_Auto:
      Aceite := 12; //Termo hor�rio de ver�o autom�tico
    istIBPTax:
      Aceite := 13; //Termo IBPTax
  end;
  //
  if Aceite = 0 then
  begin
    Geral.MB_Aviso('Tipo de aceite inv�lido!');
    Exit;
  end;
  //
  //N�o usar DBCheck por causa do DermatekBK
  Application.CreateForm(TFmWAceite, FmWAceite);
  FmWAceite.FTermo := Aceite;
  //
  FmWAceite.ShowModal;
  //
  Aceitou := FmWAceite.FAceitou;
  //
  if Aceitou then
  begin
    Termo    := FmWAceite.FTermo;
    Usuario  := FmWAceite.FUsuario;
    DataHora := FmWAceite.FDataHora;
    //
    Result := True;
  end;
  //
  FmWAceite.Destroy;
end;

function TUnWAceites.AceitaTermos2(const Termo_ID: Integer; var Termo,
  Usuario: Integer; var HashTermo: String; var DataHora: TDateTime): Boolean;
var
  Aceitou: Boolean;
begin
  Termo    := 0;
  Usuario  := 0;
  DataHora := 0;
  Result   := False;
  //
  if Termo_ID = 0 then
  begin
    Geral.MB_Aviso('Termo inv�lido!');
    Exit;
  end;
  //
  //N�o usar DBCheck por causa do DermatekBK
  Application.CreateForm(TFmWAceite, FmWAceite);
  FmWAceite.FTermo := Termo_ID;
  //
  FmWAceite.ShowModal;
  //
  Aceitou := FmWAceite.FAceitou;
  //
  if Aceitou then
  begin
    Termo     := FmWAceite.FTermo;
    Usuario   := FmWAceite.FUsuario;
    DataHora  := FmWAceite.FDataHora;
    HashTermo := FmWAceite.FHashArq;
    //
    Result := True;
  end;
  //
  FmWAceite.Destroy;
end;

end.
