object FmCartasImp: TFmCartasImp
  Left = 339
  Top = 185
  Caption = 'CIA-GEREN-002 :: Impress'#227'o Automatizada de Cartas'
  ClientHeight = 643
  ClientWidth = 1318
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -14
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 120
  TextHeight = 17
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1318
    Height = 63
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 1255
      Top = 0
      Width = 63
      Height = 63
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 10
        Top = 14
        Width = 42
        Height = 42
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 63
      Height = 63
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 63
      Top = 0
      Width = 1192
      Height = 63
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 9
        Top = 12
        Width = 532
        Height = 40
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Impress'#227'o Automatizada de Cartas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -35
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 12
        Top = 14
        Width = 532
        Height = 40
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Impress'#227'o Automatizada de Cartas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -35
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 10
        Top = 13
        Width = 532
        Height = 40
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Impress'#227'o Automatizada de Cartas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -35
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 63
    Width = 1318
    Height = 418
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 1318
      Height = 418
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 1318
        Height = 418
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        TabOrder = 0
        object Splitter1: TSplitter
          Left = 521
          Top = 19
          Width = 6
          Height = 397
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          ExplicitLeft = 522
          ExplicitTop = 20
          ExplicitHeight = 396
        end
        object DBGrid1: TdmkDBGridDAC
          Left = 2
          Top = 19
          Width = 519
          Height = 397
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          SQLFieldsToChange.Strings = (
            'Ativo')
          SQLIndexesOnUpdate.Strings = (
            'Codigo')
          Align = alLeft
          Columns = <
            item
              Expanded = False
              FieldName = 'Ativo'
              Title.Caption = 'Ok'
              Width = 17
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Nome'
              Title.Caption = 'Condom'#237'nio'
              Width = 300
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Itens'
              Title.Caption = 'Cartas'
              Width = 40
              Visible = True
            end>
          Color = clWindow
          DataSource = DsCondComCartas
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
          TabOrder = 0
          TitleFont.Charset = ANSI_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -14
          TitleFont.Name = 'Tahoma'
          TitleFont.Style = []
          SQLTable = '_itensporcod_'
          EditForceNextYear = False
          Columns = <
            item
              Expanded = False
              FieldName = 'Ativo'
              Title.Caption = 'Ok'
              Width = 17
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Nome'
              Title.Caption = 'Condom'#237'nio'
              Width = 300
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Itens'
              Title.Caption = 'Cartas'
              Width = 40
              Visible = True
            end>
        end
        object DBGrid2: TDBGrid
          Left = 527
          Top = 19
          Width = 789
          Height = 397
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alClient
          DataSource = DsCartasDeCond
          TabOrder = 1
          TitleFont.Charset = ANSI_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -14
          TitleFont.Name = 'Tahoma'
          TitleFont.Style = []
          Columns = <
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'Vencimento'
              Title.Alignment = taCenter
              Title.Caption = 'Vencto'
              Width = 51
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'MEZ_TXT'
              Title.Alignment = taCenter
              Title.Caption = 'M'#234's'
              Width = 36
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'FatNum'
              Title.Caption = 'Boleto'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DEPTO_TXT'
              Title.Caption = 'UH'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Cadastro'
              Title.Caption = 'Carta'
              Width = 32
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_PRP'
              Title.Caption = 'Propriet'#225'rio'
              Width = 293
              Visible = True
            end>
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 481
    Width = 1318
    Height = 71
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 19
      Width = 1314
      Height = 50
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 17
        Top = 3
        Width = 150
        Height = 19
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -17
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 16
        Top = 1
        Width = 150
        Height = 19
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -17
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object PB1: TProgressBar
        Left = 0
        Top = 28
        Width = 1314
        Height = 22
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alBottom
        TabOrder = 0
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 552
    Width = 1318
    Height = 91
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 1127
      Top = 19
      Width = 189
      Height = 70
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 16
        Top = 4
        Width = 157
        Height = 52
        Cursor = crHandPoint
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 19
      Width = 1125
      Height = 70
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtImprime: TBitBtn
        Tag = 5
        Left = 16
        Top = 5
        Width = 157
        Height = 53
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Imprime'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtImprimeClick
      end
      object CkDesign: TCheckBox
        Left = 192
        Top = 21
        Width = 111
        Height = 22
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Design mode.'
        TabOrder = 1
      end
      object BtTodos: TBitBtn
        Tag = 127
        Left = 336
        Top = 5
        Width = 157
        Height = 53
        Cursor = crHandPoint
        Hint = 'Sai da janela atual'
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Todos'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 2
        OnClick = BtTodosClick
      end
      object BtNenhum: TBitBtn
        Tag = 128
        Left = 498
        Top = 5
        Width = 157
        Height = 53
        Cursor = crHandPoint
        Hint = 'Sai da janela atual'
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Nenhum'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 3
        OnClick = BtNenhumClick
      end
    end
  end
  object QrEnvCartas: TmySQLQuery
    Database = DModG.MyPID_DB
    AfterScroll = QrEnvCartasAfterScroll
    SQL.Strings = (
      'SELECT bol.Vencimento, bol.Data, bol.Mez, bol.FatNum,  '
      'bol.Depto, bol.Propriet, bol.Usuario, bol.Empresa, '
      'bol.EmprEnti, env.*  '
      'FROM syndic.diarcemenv env '
      'LEFT JOIN syndic.diarcembol bol ON bol.Codigo=env.Codigo '
      'WHERE env.Metodo=3'
      'AND bol.Juridica=0'
      'AND env.ConsidEnvi < "1900-01-01" '
      'AND env.AutoCancel < "1900-01-01" '
      'AND env.DtaLimite >= SYSDATE() '
      'AND bol.Empresa IN ('
      '     SELECT Codigo '
      '     FROM _itensporcod_'
      '     WHERE Ativo=1'
      ')')
    Left = 608
    Top = 8
    object QrEnvCartasVencimento: TDateField
      FieldName = 'Vencimento'
    end
    object QrEnvCartasData: TDateField
      FieldName = 'Data'
    end
    object QrEnvCartasMez: TIntegerField
      FieldName = 'Mez'
    end
    object QrEnvCartasFatNum: TFloatField
      FieldName = 'FatNum'
    end
    object QrEnvCartasDepto: TIntegerField
      FieldName = 'Depto'
    end
    object QrEnvCartasPropriet: TIntegerField
      FieldName = 'Propriet'
    end
    object QrEnvCartasUsuario: TIntegerField
      FieldName = 'Usuario'
    end
    object QrEnvCartasEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrEnvCartasEmprEnti: TIntegerField
      FieldName = 'EmprEnti'
    end
    object QrEnvCartasCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrEnvCartasDiarCEMIts: TIntegerField
      FieldName = 'DiarCEMIts'
      Required = True
    end
    object QrEnvCartasMetodo: TSmallintField
      FieldName = 'Metodo'
      Required = True
    end
    object QrEnvCartasCadastro: TIntegerField
      FieldName = 'Cadastro'
      Required = True
    end
    object QrEnvCartasReInclu: TIntegerField
      FieldName = 'ReInclu'
      Required = True
    end
    object QrEnvCartasDtaInicio: TDateField
      FieldName = 'DtaInicio'
      Required = True
    end
    object QrEnvCartasDtaLimite: TDateField
      FieldName = 'DtaLimite'
      Required = True
    end
    object QrEnvCartasTentativas: TIntegerField
      FieldName = 'Tentativas'
      Required = True
    end
    object QrEnvCartasLastExec: TDateTimeField
      FieldName = 'LastExec'
      Required = True
    end
    object QrEnvCartasConsidEnvi: TDateTimeField
      FieldName = 'ConsidEnvi'
      Required = True
    end
    object QrEnvCartasLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrEnvCartasDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrEnvCartasDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrEnvCartasUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrEnvCartasUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrEnvCartasAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Required = True
    end
    object QrEnvCartasAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
    object QrEnvCartasLastLotImp: TLargeintField
      FieldName = 'LastLotImp'
      Required = True
    end
    object QrEnvCartasAutoCancel: TDateTimeField
      FieldName = 'AutoCancel'
      Required = True
    end
    object QrEnvCartasLastVerify: TDateTimeField
      FieldName = 'LastVerify'
      Required = True
    end
    object QrEnvCartasNaoEnvTip: TIntegerField
      FieldName = 'NaoEnvTip'
      Required = True
    end
    object QrEnvCartasNaoEnvMot: TIntegerField
      FieldName = 'NaoEnvMot'
      Required = True
    end
    object QrEnvCartasNaoEnvLst: TSmallintField
      FieldName = 'NaoEnvLst'
      Required = True
    end
  end
  object frxCNDC: TfrxReport
    Version = '5.5'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39815.746004548600000000
    ReportOptions.LastChange = 39815.746004548600000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'procedure PageHeader1OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  if <MeuLogoExiste> = True then'
      '    Picture1.LoadFromFile(<MeuLogoCaminho>);'
      'end;'
      ''
      'begin'
      'end.')
    OnGetValue = frxCNDCGetValue
    Left = 356
    Top = 212
    Datasets = <
      item
        DataSet = frxDsCartas
        DataSetName = 'frxDsCartas'
      end
      item
      end
      item
      end
      item
        DataSet = frxDsInad
        DataSetName = 'frxDsInad'
      end
      item
        DataSet = frxDsUnidades
        DataSetName = 'frxDsUnidades'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object DetailData1: TfrxDetailData
        FillType = ftBrush
        Height = 20.000000000000000000
        Top = 158.740260000000000000
        Width = 718.110700000000000000
        Columns = 1
        ColumnWidth = 200.000000000000000000
        ColumnGap = 20.000000000000000000
        DataSet = frxDsCartas
        DataSetName = 'frxDsCartas'
        RowCount = 0
        Stretched = True
        object Rich1: TfrxRichView
          Left = 2.629870000000000000
          Width = 706.929190000000000000
          Height = 20.000000000000000000
          StretchMode = smMaxHeight
          DataField = 'Texto'
          DataSet = frxDsCartas
          DataSetName = 'frxDsCartas'
          GapX = 2.000000000000000000
          GapY = 1.000000000000000000
          RichEdit = {
            7B5C727466315C616E73695C616E7369637067313235325C64656666305C6E6F
            7569636F6D7061745C6465666C616E67313034367B5C666F6E7474626C7B5C66
            305C666E696C204D532053616E732053657269663B7D7D0D0A7B5C636F6C6F72
            74626C203B5C726564305C677265656E305C626C7565303B7D0D0A7B5C2A5C67
            656E657261746F722052696368656432302031302E302E31353036337D5C7669
            65776B696E64345C756331200D0A5C706172645C6366315C66305C667331365C
            7061720D0A7D0D0A00}
        end
      end
      object DadosMestre1: TfrxMasterData
        FillType = ftBrush
        Height = 115.118120000000000000
        Top = 18.897650000000000000
        Width = 718.110700000000000000
        Columns = 1
        ColumnWidth = 200.000000000000000000
        ColumnGap = 20.000000000000000000
        DataSet = frxDsUnidades
        DataSetName = 'frxDsUnidades'
        RowCount = 0
        StartNewPage = True
        object Memo1: TfrxMemoView
          Width = 196.440630000000000000
          Height = 115.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo154: TfrxMemoView
          Left = 196.535433070000000000
          Width = 521.480210000000000000
          Height = 115.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo155: TfrxMemoView
          Left = 199.606370000000000000
          Top = 1.889763779999999000
          Width = 509.291280000000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
        end
        object Memo157: TfrxMemoView
          Left = 241.181200000000000000
          Top = 20.787401570000000000
          Width = 168.000000000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsDono."CNPJ_TXT"]')
          ParentFont = False
        end
        object Memo158: TfrxMemoView
          Left = 199.858380000000000000
          Top = 20.787401570000000000
          Width = 41.322820000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'CNPJ:')
          ParentFont = False
        end
        object Memo159: TfrxMemoView
          Left = 541.212740000000000000
          Top = 20.787401570000000000
          Width = 168.000000000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsDono."IE_RG"]')
          ParentFont = False
        end
        object Memo160: TfrxMemoView
          Left = 515.008040000000000000
          Top = 20.787401570000000000
          Width = 26.204700000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'I.E.:')
          ParentFont = False
        end
        object Memo163: TfrxMemoView
          Left = 252.519790000000000000
          Top = 35.905511810000000000
          Width = 456.377860000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsDono."E_LNR"]')
          ParentFont = False
        end
        object Memo164: TfrxMemoView
          Left = 357.149660000000000000
          Top = 51.023622050000000000
          Width = 275.779530000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsDono."Cidade"]')
          ParentFont = False
        end
        object Memo165: TfrxMemoView
          Left = 312.047310000000000000
          Top = 51.023622050000000000
          Width = 45.102350000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'Cidade: ')
          ParentFont = False
        end
        object Memo166: TfrxMemoView
          Left = 678.031540000000000000
          Top = 51.023622050000000000
          Width = 32.000000000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsDono."NOMEUF"]')
          ParentFont = False
        end
        object Memo167: TfrxMemoView
          Left = 632.929190000000000000
          Top = 51.023622050000000000
          Width = 45.102350000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'Estado: ')
          ParentFont = False
        end
        object Memo168: TfrxMemoView
          Left = 233.622140000000000000
          Top = 51.023622050000000000
          Width = 78.425170000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsDono."ECEP_TXT"]')
          ParentFont = False
        end
        object Memo13: TfrxMemoView
          Left = 199.858380000000000000
          Top = 35.905511810000000000
          Width = 52.661410000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'Endere'#231'o:')
          ParentFont = False
        end
        object Memo14: TfrxMemoView
          Left = 199.858380000000000000
          Top = 51.023622050000000000
          Width = 33.763760000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'CEP:')
          ParentFont = False
        end
        object Memo169: TfrxMemoView
          Left = 252.519790000000000000
          Top = 66.141732280000000000
          Width = 237.574830000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsDono."TE1_TXT"]')
          ParentFont = False
        end
        object Memo170: TfrxMemoView
          Left = 199.858380000000000000
          Top = 66.141732280000000000
          Width = 52.661410000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'Telefone:')
          ParentFont = False
        end
        object Memo171: TfrxMemoView
          Left = 516.299320000000000000
          Top = 66.141732280000000000
          Width = 193.795300000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsDono."FAX_TXT"]')
          ParentFont = False
        end
        object Memo172: TfrxMemoView
          Left = 490.094620000000000000
          Top = 66.141732280000000000
          Width = 26.204700000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'Fax:')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          Left = 237.858380000000000000
          Top = 83.149660000000000000
          Width = 471.905690000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[VAR_MYURL]')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          Left = 200.315090000000000000
          Top = 83.149660000000000000
          Width = 37.543290000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'Portal:')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          Left = 237.858380000000000000
          Top = 98.267780000000000000
          Width = 471.905690000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsDono."EMail"]')
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          Left = 200.315090000000000000
          Top = 98.267780000000000000
          Width = 37.543290000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'Emeio:')
          ParentFont = False
        end
        object Picture1: TfrxPictureView
          Left = 0.755905510000000000
          Top = 0.755905510000001700
          Width = 194.645669290000000000
          Height = 113.385826770000000000
          Center = True
          HightQuality = True
          Transparent = False
          TransparentColor = clWhite
        end
      end
    end
  end
  object frxDsUnidades: TfrxDBDataset
    UserName = 'frxDsUnidades'
    CloseDataSource = False
    DataSet = QrUnidades
    BCDToCurrency = False
    Left = 384
    Top = 212
  end
  object QrUnidades: TmySQLQuery
    Database = Dmod.MyDB
    Filter = 'VALOR=0'
    Filtered = True
    BeforeClose = QrUnidadesBeforeClose
    AfterScroll = QrUnidadesAfterScroll
    Left = 412
    Top = 212
    object QrUnidadesVALOR: TFloatField
      FieldName = 'VALOR'
    end
    object QrUnidadesNOME_CON: TWideStringField
      FieldName = 'NOME_CON'
      Size = 100
    end
    object QrUnidadesNOME_PRP: TWideStringField
      FieldName = 'NOME_PRP'
      Size = 100
    end
    object QrUnidadesUnidade: TWideStringField
      FieldName = 'Unidade'
      Size = 10
    end
    object QrUnidadesDepto: TIntegerField
      FieldName = 'Depto'
    end
    object QrUnidadesDOC_PRP: TWideStringField
      FieldName = 'DOC_PRP'
      Size = 18
    end
    object QrUnidadesNOMEUSU: TWideStringField
      FieldName = 'NOMEUSU'
      Size = 100
    end
    object QrUnidadesDOC_USU: TWideStringField
      FieldName = 'DOC_USU'
      Size = 18
    end
    object QrUnidadesConta: TIntegerField
      FieldName = 'Conta'
    end
    object QrUnidadesCOD_PRP: TIntegerField
      FieldName = 'COD_PRP'
    end
    object QrUnidadesCOD_CON: TIntegerField
      FieldName = 'COD_CON'
    end
    object QrUnidadesCEP_PRP: TFloatField
      FieldName = 'CEP_PRP'
    end
    object QrUnidadesUF_PRP: TFloatField
      FieldName = 'UF_PRP'
    end
    object QrUnidadesMUNI_PRP: TWideStringField
      FieldName = 'MUNI_PRP'
      Size = 100
    end
    object QrUnidadesCliInt: TIntegerField
      FieldName = 'CliInt'
    end
  end
  object QrInad: TmySQLQuery
    Database = Dmod.MyDB
    Filter = 'VALOR=0'
    Left = 412
    Top = 240
    object QrInadMES: TWideStringField
      FieldName = 'MES'
      Required = True
      Size = 7
    end
    object QrInadData: TDateField
      FieldName = 'Data'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrInadFatNum: TFloatField
      FieldName = 'FatNum'
      DisplayFormat = '000000'
    end
    object QrInadVencimento: TDateField
      FieldName = 'Vencimento'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrInadMez: TIntegerField
      FieldName = 'Mez'
      Required = True
    end
    object QrInadDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 100
    end
    object QrInadCREDITO: TFloatField
      FieldName = 'CREDITO'
      DisplayFormat = '#,###,##0.00'
    end
    object QrInadPAGO: TFloatField
      FieldName = 'PAGO'
      DisplayFormat = '#,###,##0.00'
    end
    object QrInadJuros: TFloatField
      FieldName = 'Juros'
      DisplayFormat = '#,###,##0.00'
    end
    object QrInadMulta: TFloatField
      FieldName = 'Multa'
      DisplayFormat = '#,###,##0.00'
    end
    object QrInadTOTAL: TFloatField
      FieldName = 'TOTAL'
      DisplayFormat = '#,###,##0.00'
    end
    object QrInadSALDO: TFloatField
      FieldName = 'SALDO'
      DisplayFormat = '#,###,##0.00'
    end
    object QrInadPEND_VAL: TFloatField
      FieldName = 'PEND_VAL'
      DisplayFormat = '#,###,##0.00'
    end
    object QrInadVALOR: TFloatField
      FieldName = 'VALOR'
    end
    object QrInadATZ_VAL: TFloatField
      FieldName = 'ATZ_VAL'
    end
  end
  object frxDsInad: TfrxDBDataset
    UserName = 'frxDsInad'
    CloseDataSource = False
    DataSet = QrInad
    BCDToCurrency = False
    Left = 384
    Top = 240
  end
  object frxCDCU: TfrxReport
    Version = '5.5'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39815.746004548600000000
    ReportOptions.LastChange = 39815.746004548600000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      'end.')
    OnGetValue = frxCNDCGetValue
    Left = 356
    Top = 240
    Datasets = <
      item
        DataSet = frxDsCartas
        DataSetName = 'frxDsCartas'
      end
      item
      end
      item
      end
      item
        DataSet = frxDsInad
        DataSetName = 'frxDsInad'
      end
      item
        DataSet = frxDsUnidades
        DataSetName = 'frxDsUnidades'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 9.000000000000000000
      BottomMargin = 10.000000000000000000
      object DetailData1: TfrxDetailData
        FillType = ftBrush
        Height = 20.000000000000000000
        Top = 45.354360000000000000
        Width = 718.110700000000000000
        Columns = 1
        ColumnWidth = 200.000000000000000000
        ColumnGap = 20.000000000000000000
        DataSet = frxDsCartas
        DataSetName = 'frxDsCartas'
        RowCount = 0
        Stretched = True
        object Rich1: TfrxRichView
          Left = 2.629870000000000000
          Width = 706.929190000000000000
          Height = 20.000000000000000000
          StretchMode = smMaxHeight
          DataField = 'Texto'
          DataSet = frxDsCartas
          DataSetName = 'frxDsCartas'
          GapX = 2.000000000000000000
          GapY = 1.000000000000000000
          RichEdit = {
            7B5C727466315C616E73695C616E7369637067313235325C64656666305C6E6F
            7569636F6D7061745C6465666C616E67313034367B5C666F6E7474626C7B5C66
            305C666E696C204D532053616E732053657269663B7D7D0D0A7B5C636F6C6F72
            74626C203B5C726564305C677265656E305C626C7565303B7D0D0A7B5C2A5C67
            656E657261746F722052696368656432302031302E302E31353036337D5C7669
            65776B696E64345C756331200D0A5C706172645C6366315C66305C667331365C
            7061720D0A7D0D0A00}
        end
      end
      object DadosMestre1: TfrxMasterData
        FillType = ftBrush
        Height = 3.779527560000000000
        Top = 18.897650000000000000
        Width = 718.110700000000000000
        Columns = 1
        ColumnWidth = 200.000000000000000000
        ColumnGap = 20.000000000000000000
        DataSet = frxDsUnidades
        DataSetName = 'frxDsUnidades'
        RowCount = 0
        StartNewPage = True
      end
    end
  end
  object frxCarta: TfrxReport
    Version = '5.5'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39911.688433344920000000
    ReportOptions.LastChange = 39911.688433344920000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    Left = 356
    Top = 268
    Datasets = <>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
    end
  end
  object frxDsCartas: TfrxDBDataset
    UserName = 'frxDsCartas'
    CloseDataSource = False
    DataSet = QrTexto
    BCDToCurrency = False
    Left = 384
    Top = 268
  end
  object QrTexto: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Titulo, Texto, Tipo'
      'FROM cartas'
      'WHERE Codigo=:P0')
    Left = 412
    Top = 268
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrTextoCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrTextoTitulo: TWideStringField
      FieldName = 'Titulo'
      Size = 100
    end
    object QrTextoTexto: TWideMemoField
      FieldName = 'Texto'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrTextoTipo: TIntegerField
      FieldName = 'Tipo'
    end
  end
  object QrUsers: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT DISTINCT usu.CodigoEsp, usu.Username, usu.Password'
      'FROM condimov imv'
      
        'LEFT JOIN entidades ent ON IF(imv.Usuario>0, imv.Usuario, imv.Pr' +
        'opriet)=ent.Codigo'
      'LEFT JOIN users usu ON usu.CodigoEsp=imv.Conta '
      '  AND (usu.Tipo=1 OR usu.Tipo IS NULL)'
      'WHERE imv.Conta=:P0')
    Left = 636
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrUsersUsername: TWideStringField
      FieldName = 'Username'
      Origin = 'users.Username'
      Size = 32
    end
    object QrUsersPassword: TWideStringField
      FieldName = 'Password'
      Origin = 'users.Password'
      Size = 32
    end
  end
  object QrCondComCartas: TmySQLQuery
    Database = DModG.MyPID_DB
    AfterOpen = QrCondComCartasAfterOpen
    BeforeClose = QrCondComCartasBeforeClose
    AfterScroll = QrCondComCartasAfterScroll
    SQL.Strings = (
      'SELECT bol.Empresa, COUNT(bol.Empresa) ITENS,'
      'IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NO_EMP'
      'FROM diarcemenv env '
      'LEFT JOIN diarcembol bol ON bol.Codigo=env.Codigo '
      'LEFT JOIN entidades emp ON emp.Codigo=bol.Empresa'
      'WHERE env.Metodo=3'
      'AND bol.Juridica=0'
      'AND env.ConsidEnvi < "1900-01-01" '
      'AND env.DtaLimite >= SYSDATE() '
      'GROUP BY bol.Empresa'
      'ORDER BY ITENS DESC, NO_EMP')
    Left = 84
    Top = 120
    object QrCondComCartasCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCondComCartasNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QrCondComCartasItens: TLargeintField
      FieldName = 'Itens'
    end
    object QrCondComCartasAtivo: TSmallintField
      FieldName = 'Ativo'
      MaxValue = 1
    end
  end
  object DsCondComCartas: TDataSource
    DataSet = QrCondComCartas
    Left = 112
    Top = 120
  end
  object QrCartasDeCond: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT bol.Vencimento, bol.Data, bol.Mez, bol.Depto, bol.FatNum,'
      
        'bol.Codigo, bol.Propriet, bol.Usuario, Bol.Empresa, bol.EmprEnti' +
        ', bol.Juridica,'
      
        'env.DiarCEMIts, env.Metodo, env.Cadastro, env.ReInclu, env.DtaIn' +
        'icio, env.Dtalimite,'
      
        'CONCAT(LPAD(bol.Mez MOD 100, 2, "0"), "/", LPAD(bol.Mez DIV 100,' +
        ' 2, "0")) '
      'MEZ_TXT, cim.Unidade DEPTO_TXT, '
      'IF(prp.Tipo=0, prp.RazaoSocial, prp.Nome) NO_PRP'
      'FROM diarcemenv env'
      'LEFT JOIN diarcembol bol ON bol.Codigo=env.Codigo'
      'LEFT JOIN condimov cim ON cim.Conta=bol.Depto'
      'LEFT JOIN entidades prp ON prp.Codigo=bol.Propriet'
      'WHERE env.Metodo=3'
      'AND bol.Juridica=0'
      'AND env.ConsidEnvi < "1900-01-01" '
      'AND env.DtaLimite >= SYSDATE() '
      'AND bol.Empresa>0'
      ''
      '')
    Left = 84
    Top = 148
    object QrCartasDeCondVencimento: TDateField
      FieldName = 'Vencimento'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrCartasDeCondData: TDateField
      FieldName = 'Data'
    end
    object QrCartasDeCondMez: TIntegerField
      FieldName = 'Mez'
    end
    object QrCartasDeCondDepto: TIntegerField
      FieldName = 'Depto'
    end
    object QrCartasDeCondFatNum: TFloatField
      FieldName = 'FatNum'
    end
    object QrCartasDeCondCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrCartasDeCondPropriet: TIntegerField
      FieldName = 'Propriet'
    end
    object QrCartasDeCondUsuario: TIntegerField
      FieldName = 'Usuario'
    end
    object QrCartasDeCondEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrCartasDeCondEmprEnti: TIntegerField
      FieldName = 'EmprEnti'
    end
    object QrCartasDeCondJuridica: TSmallintField
      FieldName = 'Juridica'
    end
    object QrCartasDeCondDiarCEMIts: TIntegerField
      FieldName = 'DiarCEMIts'
    end
    object QrCartasDeCondMetodo: TSmallintField
      FieldName = 'Metodo'
    end
    object QrCartasDeCondCadastro: TIntegerField
      FieldName = 'Cadastro'
    end
    object QrCartasDeCondReInclu: TIntegerField
      FieldName = 'ReInclu'
    end
    object QrCartasDeCondDtaInicio: TDateField
      FieldName = 'DtaInicio'
    end
    object QrCartasDeCondDtalimite: TDateField
      FieldName = 'Dtalimite'
    end
    object QrCartasDeCondMEZ_TXT: TWideStringField
      FieldName = 'MEZ_TXT'
      Size = 5
    end
    object QrCartasDeCondDEPTO_TXT: TWideStringField
      FieldName = 'DEPTO_TXT'
      Size = 10
    end
    object QrCartasDeCondNO_PRP: TWideStringField
      FieldName = 'NO_PRP'
      Size = 100
    end
  end
  object DsCartasDeCond: TDataSource
    DataSet = QrCartasDeCond
    Left = 112
    Top = 148
  end
  object QrSelConds: TmySQLQuery
    Database = DModG.MyPID_DB
    AfterOpen = QrSelCondsAfterOpen
    BeforeClose = QrSelCondsBeforeClose
    SQL.Strings = (
      'SELECT *'
      'FROM _itensporcod_'
      'WHERE Ativo=1')
    Left = 140
    Top = 120
    object QrSelCondsCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrSelCondsNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QrSelCondsItens: TLargeintField
      FieldName = 'Itens'
    end
  end
  object QrCartoesAR: TmySQLQuery
    Database = DModG.MyPID_DB
    OnCalcFields = QrCartoesARCalcFields
    SQL.Strings = (
      'SELECT env.Controle, bol.Propriet, dci.CartaoAR, '
      'IF (Tipo=0, prp.RazaoSocial, prp.Nome) NO_PRP,'
      'IF (Tipo=0, elg.Nome, plg.Nome) NOMELOGRAD,'
      'IF (Tipo=0, prp.ERua,    prp.PRua    ) RUA,'
      'IF (Tipo=0, prp.ENumero+0.000, prp.PNumero+0.000 ) Numero,'
      'IF (Tipo=0, prp.ECompl,  prp.PCompl  ) Compl,'
      'IF (Tipo=0, prp.EBairro, prp.PBairro ) Bairro,'
      'IF (Tipo=0, prp.ECidade, prp.PCidade ) Cidade,'
      'IF (Tipo=0, prp.EUF,     prp.PUF     ) + 0.000 UF,'
      'IF (Tipo=0, euf.Nome,     puf.Nome     ) NO_UF,'
      'IF (Tipo=0, prp.ECEP,    prp.PCEP    ) + 0.000 CEP,'
      'IF (Tipo=0, prp.EPais,   prp.PPais   ) Pais'
      'FROM syndic.diarcemenv env '
      'LEFT JOIN syndic.diarcembol bol ON bol.Codigo=env.Codigo '
      'LEFT JOIN syndic.diarcemits dci ON dci.Controle=env.DiarCEMIts'
      'LEFT JOIN syndic.entidades prp ON prp.Codigo=bol.Propriet'
      'LEFT JOIN syndic.listalograd elg ON elg.Codigo=prp.ELograd'
      'LEFT JOIN syndic.listalograd plg ON plg.Codigo=prp.PLograd'
      'LEFT JOIN syndic.ufs euf ON euf.Codigo=prp.EUF'
      'LEFT JOIN syndic.ufs puf ON puf.Codigo=prp.PUF'
      'WHERE env.Metodo=3'
      'AND bol.Juridica=0'
      'AND env.ConsidEnvi < "1900-01-01" '
      'AND env.AutoCancel < "1900-01-01" '
      'AND env.DtaLimite >= SYSDATE() ')
    Left = 412
    Top = 296
    object QrCartoesARPropriet: TIntegerField
      FieldName = 'Propriet'
    end
    object QrCartoesARCartaoAR: TSmallintField
      FieldName = 'CartaoAR'
    end
    object QrCartoesARNO_PRP: TWideStringField
      FieldName = 'NO_PRP'
      Size = 100
    end
    object QrCartoesARRUA: TWideStringField
      FieldName = 'RUA'
      Size = 60
    end
    object QrCartoesARNumero: TFloatField
      FieldName = 'Numero'
    end
    object QrCartoesARCompl: TWideStringField
      FieldName = 'Compl'
      Size = 60
    end
    object QrCartoesARBairro: TWideStringField
      FieldName = 'Bairro'
      Size = 60
    end
    object QrCartoesARCidade: TWideStringField
      FieldName = 'Cidade'
      Size = 60
    end
    object QrCartoesARUF: TFloatField
      FieldName = 'UF'
    end
    object QrCartoesARCEP: TFloatField
      FieldName = 'CEP'
    end
    object QrCartoesARPais: TWideStringField
      FieldName = 'Pais'
      Size = 60
    end
    object QrCartoesARENDERECO: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'ENDERECO'
      Calculated = True
    end
    object QrCartoesARNOMELOGRAD: TWideStringField
      FieldName = 'NOMELOGRAD'
      Size = 15
    end
    object QrCartoesARNUMERO_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NUMERO_TXT'
      Size = 255
      Calculated = True
    end
    object QrCartoesARCEP_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CEP_TXT'
      Size = 15
      Calculated = True
    end
    object QrCartoesARNO_UF: TWideStringField
      FieldName = 'NO_UF'
      Size = 2
    end
    object QrCartoesAREAN128: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'EAN128'
      Size = 14
      Calculated = True
    end
    object QrCartoesARControle: TIntegerField
      FieldName = 'Controle'
    end
  end
  object frxDsCartoesAR: TfrxDBDataset
    UserName = 'frxDsCartoesAR'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Propriet=Propriet'
      'CartaoAR=CartaoAR'
      'NO_PRP=NO_PRP'
      'RUA=RUA'
      'Numero=Numero'
      'Compl=Compl'
      'Bairro=Bairro'
      'Cidade=Cidade'
      'UF=UF'
      'CEP=CEP'
      'Pais=Pais'
      'ENDERECO=ENDERECO'
      'NOMELOGRAD=NOMELOGRAD'
      'NUMERO_TXT=NUMERO_TXT'
      'CEP_TXT=CEP_TXT'
      'NO_UF=NO_UF'
      'EAN128=EAN128'
      'Controle=Controle')
    DataSet = QrCartoesAR
    BCDToCurrency = False
    Left = 384
    Top = 296
  end
  object frxCartoesAR: TfrxReport
    Version = '5.5'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 41311.409431794000000000
    ReportOptions.LastChange = 41311.409431794000000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    OnGetValue = frxCNDCGetValue
    Left = 356
    Top = 296
    Datasets = <
      item
        DataSet = frxDsCartoesAR
        DataSetName = 'frxDsCartoesAR'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 186.000000000000000000
      PaperHeight = 114.000000000000000000
      PaperSize = 256
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        Height = 400.630180000000000000
        Top = 18.897650000000000000
        Width = 702.992580000000000000
        DataSet = frxDsCartoesAR
        DataSetName = 'frxDsCartoesAR'
        RowCount = 0
        object Memo1: TfrxMemoView
          Left = 75.590553620000000000
          Top = 52.913373620000000000
          Width = 332.598425200000000000
          Height = 37.795287800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsCartoesAR."NO_PRP"]')
          ParentFont = False
          VAlign = vaBottom
        end
        object BarCode1: TfrxBarCodeView
          Left = 423.307086614173200000
          Top = 52.913373620000000000
          Width = 134.000000000000000000
          Height = 56.692913390000000000
          BarType = bcCodeEAN128B
          DataField = 'EAN128'
          DataSet = frxDsCartoesAR
          DataSetName = 'frxDsCartoesAR'
          Rotation = 0
          Text = '12345678'
          WideBarRatio = 2.000000000000000000
          Zoom = 1.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
        end
        object Memo2: TfrxMemoView
          Left = 75.590600000000000000
          Top = 113.385826770000000000
          Width = 559.370078740000000000
          Height = 18.897637800000000000
          DataField = 'ENDERECO'
          DataSet = frxDsCartoesAR
          DataSetName = 'frxDsCartoesAR'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsCartoesAR."ENDERECO"]')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          Left = 75.590600000000000000
          Top = 154.960730000000000000
          Width = 132.283462130000000000
          Height = 18.897637800000000000
          DataField = 'CEP_TXT'
          DataSet = frxDsCartoesAR
          DataSetName = 'frxDsCartoesAR'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsCartoesAR."CEP_TXT"]')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          Left = 215.433210000000000000
          Top = 154.960730000000000000
          Width = 222.992125980000000000
          Height = 18.897637800000000000
          DataField = 'Cidade'
          DataSet = frxDsCartoesAR
          DataSetName = 'frxDsCartoesAR'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsCartoesAR."Cidade"]')
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          Left = 453.543307090000000000
          Top = 154.960730000000000000
          Width = 22.677165350000000000
          Height = 18.897637800000000000
          DataField = 'NO_UF'
          DataSet = frxDsCartoesAR
          DataSetName = 'frxDsCartoesAR'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsCartoesAR."NO_UF"]')
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          Left = 483.779527560000000000
          Top = 154.960730000000000000
          Width = 147.401574800000000000
          Height = 18.897637800000000000
          DataField = 'Pais'
          DataSet = frxDsCartoesAR
          DataSetName = 'frxDsCartoesAR'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsCartoesAR."Pais"]')
          ParentFont = False
        end
      end
    end
  end
  object frxBarCodeObject1: TfrxBarCodeObject
    Left = 140
    Top = 148
  end
end
