unit UnTextos_Jan;

interface

uses
  Windows, SysUtils, Classes, mySQLDbTables, SHDocVw, DmkGeral, DmkDAC_PF,
  UnDmkProcFunc, UnDmkEnums;

type
  TUnTextos_Jan = class(TObject)
  private
    { Private declarations }

  public
    { Public declarations }
    procedure ConfigarTabela(WebBrowser: TWebBrowser);
    procedure MostraTextosHTML(Codigo: Integer; HTML: WideString; Tablela,
              FldCodigo, FldTexto: String; DataBase: TmySQLDatabase;
              Custom: Boolean = False);
  end;

var
  Textos_Jan: TUnTextos_Jan;


implementation

uses MyDBCheck, TextosHTML, TextosHTMLTab, UnDmkHTML2;

{ TUnTextos_Jan }

procedure TUnTextos_Jan.ConfigarTabela(WebBrowser: TWebBrowser);
var
  Colunas, Linhas: Integer;
  Listrado: Boolean;
begin
  if DBCheck.CriaFm(TFmTextosHTMLTab, FmTextosHTMLTab, afmoNegarComAviso) then
  begin
    FmTextosHTMLTab.ShowModal;
    //
    Colunas  := FmTextosHTMLTab.FColunas;
    Linhas   := FmTextosHTMLTab.FLinhas;
    Listrado := FmTextosHTMLTab.FListrado;
    //
    FmTextosHTMLTab.Destroy;
    //
    if (Colunas <> 0) and (Linhas <> 0) then
      dmkHTML2.InserirTabela(WebBrowser, ttlLayout1, Colunas, Linhas, Listrado);
  end;
end;

procedure TUnTextos_Jan.MostraTextosHTML(Codigo: Integer; HTML: WideString;
  Tablela, FldCodigo, FldTexto: String; DataBase: TmySQLDatabase;
  Custom: Boolean = False);
begin
  if DBCheck.CriaFm(TFmTextosHTML, FmTextosHTML, afmoNegarComAviso) then
  begin
    FmTextosHTML.FCodigo    := Codigo;
    FmTextosHTML.FTablela   := Tablela;
    FmTextosHTML.FFldCodigo := FldCodigo;
    FmTextosHTML.FFldTexto  := FldTexto;
    FmTextosHTML.FDataBase  := DataBase;
    FmTextosHTML.FHTML      := HTML;
    FmTextosHTML.FCustom    := Custom;
    FmTextosHTML.ShowModal;
    FmTextosHTML.Destroy;
  end;
end;

end.
