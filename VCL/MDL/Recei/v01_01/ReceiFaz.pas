unit ReceiFaz;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, DBCtrls, dmkLabel, Mask,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkDBEdit, dmkGeral, Variants,
  dmkValUsu, dmkImage, UnDmkEnums, dmkMemo;

type
  TFmReceiFaz = class(TForm)
    Panel1: TPanel;
    CkContinuar: TCheckBox;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    Panel3: TPanel;
    Label6: TLabel;
    EdControle: TdmkEdit;
    Label7: TLabel;
    EdNome: TdmkEdit;
    EdNivel1: TdmkEdit;
    Label1: TLabel;
    EdNivel2: TdmkEdit;
    Label2: TLabel;
    Label3: TLabel;
    MeDescricao: TdmkMemo;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenReceiFaz(Controle: Integer);
  public
    { Public declarations }
    FCodigo: Integer;
    FQrCab, FQrIts: TmySQLQuery;
    FDsCab: TDataSource;
  end;

  var
  FmReceiFaz: TFmReceiFaz;

implementation

uses UnMyObjects, Module, UMySQLModule, UnInternalConsts, MyDBCheck, DmkDAC_PF;

{$R *.DFM}

procedure TFmReceiFaz.BtOKClick(Sender: TObject);
var
  Nome, Descricao: String;
  Codigo, Controle, Nivel1, Nivel2: Integer;
  SQLType: TSQLType;
  Impede: Boolean;
begin
  SQLType        := ImgTipo.SQLType;
  Codigo         := FCodigo;
  Controle       := EdControle.ValueVariant;
  Nivel1         := EdNivel1.ValueVariant;
  Nivel2         := EdNivel2.ValueVariant;
  Nome           := EdNome.ValueVariant;
  Descricao      := MeDescricao.Text;
  //
  Impede := (Trim(EdNome.Text) = '') and (MeDescricao.Text = '');
  if MyObjects.FIC(Impede, EdNome, 'Informe um T�tlo ou um modo de preparo!') then
    Exit;
  //
  Controle := UMyMod.BPGS1I32('receifaz', 'Controle', '', '', tsPos, SQLType, Controle);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'receifaz', False, [
  'Codigo', 'Nivel1', 'Nivel2',
  'Nome', 'Descricao'], [
  'Controle'], [
  Codigo, Nivel1, Nivel2,
  Nome, Descricao], [
  Controle], True) then
  begin
    ReopenReceiFaz(Controle);
    if CkContinuar.Checked then
    begin
      ImgTipo.SQLType          := stIns;
      EdControle.ValueVariant  := 0;
      //EdNivel1.ValueVariant  := 0;
      EdNivel2.ValueVariant  := EdNivel2.ValueVariant + 1;
      EdNome.ValueVariant      := '';
      //
      EdNivel1.SetFocus;
    end else Close;
  end;
end;

procedure TFmReceiFaz.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmReceiFaz.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmReceiFaz.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
end;

procedure TFmReceiFaz.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmReceiFaz.ReopenReceiFaz(Controle: Integer);
begin
  if FQrIts <> nil then
  begin
    UnDmkDAC_PF.AbreQuery(FQrIts, Dmod.MyDB);
    //
    if Controle <> 0 then
      FQrIts.Locate('Controle', Controle, []);
  end;
end;

end.
