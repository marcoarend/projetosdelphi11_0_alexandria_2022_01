unit ReceiCab;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2, dmkGeral,
  ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt, UMySQLModule, Menus,
  UnInternalConsts2, mySQLDbTables, UnMySQLCuringa, dmkPermissoes, dmkEdit,
  dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup, Grids, DBGrids,
  UnDmkProcFunc, UnDmkENums, frxClass, frxDBSet;

type
  TFmReceiCab = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    Label9: TLabel;
    EdNome: TdmkEdit;
    DBEdCodigo: TdmkDBEdit;
    DBEdNome: TdmkDBEdit;
    QrReceiCab: TMySQLQuery;
    DsReceiCab: TDataSource;
    QrReceiIng: TMySQLQuery;
    DsReceiIng: TDataSource;
    PMIts: TPopupMenu;
    ItsInclui1: TMenuItem;
    ItsExclui1: TMenuItem;
    ItsAltera1: TMenuItem;
    PMCab: TPopupMenu;
    CabInclui1: TMenuItem;
    CabAltera1: TMenuItem;
    CabExclui1: TMenuItem;
    BtCab: TBitBtn;
    BtIts: TBitBtn;
    DGDados: TDBGrid;
    EdSigla: TdmkEdit;
    Label3: TLabel;
    EdFonte: TdmkEdit;
    Label4: TLabel;
    EdHrPreparo: TdmkEdit;
    Label5: TLabel;
    EdPorcoes: TdmkEdit;
    Label6: TLabel;
    EdCustoTotal: TdmkEdit;
    Label8: TLabel;
    EdCustoPorcao: TdmkEdit;
    Label10: TLabel;
    Label11: TLabel;
    EdPrecoPorcao: TdmkEdit;
    EdPrecoTotal: TdmkEdit;
    Label12: TLabel;
    QrReceiCabCodigo: TIntegerField;
    QrReceiCabSigla: TWideStringField;
    QrReceiCabNome: TWideStringField;
    QrReceiCabFonte: TWideStringField;
    QrReceiCabHrPreparo: TTimeField;
    QrReceiCabPorcoes: TFloatField;
    QrReceiCabCustoTotal: TFloatField;
    QrReceiCabCustoPorcao: TFloatField;
    QrReceiCabPrecoPorcao: TFloatField;
    QrReceiCabPrecoTotal: TFloatField;
    QrReceiIngControle: TIntegerField;
    QrReceiIngNO_PRD_TAM_COR: TWideStringField;
    QrReceiIngNO_UnidMed: TWideStringField;
    QrReceiIngCodigo: TIntegerField;
    QrReceiIngNome: TWideStringField;
    QrReceiIngUnidMed: TIntegerField;
    QrReceiIngQuantidade: TFloatField;
    QrReceiIngGraGruX: TIntegerField;
    Label1: TLabel;
    Label2: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    Label18: TLabel;
    Label19: TLabel;
    Label20: TLabel;
    dmkDBEdit1: TdmkDBEdit;
    dmkDBEdit2: TdmkDBEdit;
    dmkDBEdit3: TdmkDBEdit;
    dmkDBEdit4: TdmkDBEdit;
    dmkDBEdit5: TdmkDBEdit;
    dmkDBEdit6: TdmkDBEdit;
    dmkDBEdit7: TdmkDBEdit;
    dmkDBEdit8: TdmkDBEdit;
    BtModo: TBitBtn;
    BtDicas: TBitBtn;
    PMModo: TPopupMenu;
    PMDicas: TPopupMenu;
    QrReceiFaz: TMySQLQuery;
    DsReceiFaz: TDataSource;
    QrReceiFazCodigo: TIntegerField;
    QrReceiFazControle: TIntegerField;
    QrReceiFazNivel1: TIntegerField;
    QrReceiFazNivel2: TIntegerField;
    QrReceiFazNome: TWideStringField;
    QrReceiFazDescricao: TWideStringField;
    PnTextos: TPanel;
    DBGModo: TDBGrid;
    Splitter1: TSplitter;
    DBGDicas: TDBGrid;
    QrReceiXtr: TMySQLQuery;
    QrReceiXtrCodigo: TIntegerField;
    QrReceiXtrControle: TIntegerField;
    QrReceiXtrNivel1: TIntegerField;
    QrReceiXtrNivel2: TIntegerField;
    QrReceiXtrNome: TWideStringField;
    QrReceiXtrDescricao: TWideStringField;
    DsReceiXtr: TDataSource;
    ItsInclui2: TMenuItem;
    ItsAltera2: TMenuItem;
    ItsExclui2: TMenuItem;
    ItsInclui3: TMenuItem;
    ItsAltera3: TMenuItem;
    ItsExclui3: TMenuItem;
    Splitter2: TSplitter;
    frxReceita: TfrxReport;
    frxDsReceiCab: TfrxDBDataset;
    frxDsReceiIng: TfrxDBDataset;
    frxDsReceiFaz: TfrxDBDataset;
    frxDsReceiXtr: TfrxDBDataset;
    QrReceiIngCustoUni: TFloatField;
    QrReceiIngCustoTot: TFloatField;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrReceiCabAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrReceiCabBeforeOpen(DataSet: TDataSet);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure QrReceiCabAfterScroll(DataSet: TDataSet);
    procedure CabInclui1Click(Sender: TObject);
    procedure CabAltera1Click(Sender: TObject);
    procedure CabExclui1Click(Sender: TObject);
    procedure BtCabClick(Sender: TObject);
    procedure BtItsClick(Sender: TObject);
    procedure ItsInclui1Click(Sender: TObject);
    procedure ItsExclui1Click(Sender: TObject);
    procedure ItsAltera1Click(Sender: TObject);
    procedure PMCabPopup(Sender: TObject);
    procedure PMItsPopup(Sender: TObject);
    procedure QrReceiCabBeforeClose(DataSet: TDataSet);
    procedure BtModoClick(Sender: TObject);
    procedure BtDicasClick(Sender: TObject);
    procedure ItsInclui2Click(Sender: TObject);
    procedure ItsAltera2Click(Sender: TObject);
    procedure ItsExclui2Click(Sender: TObject);
    procedure ItsInclui3Click(Sender: TObject);
    procedure ItsAltera3Click(Sender: TObject);
    procedure ItsExclui3Click(Sender: TObject);
    procedure PMModoPopup(Sender: TObject);
    procedure PMDicasPopup(Sender: TObject);
    procedure frxReceitaGetValue(const VarName: string; var Value: Variant);
    procedure SbImprimeClick(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    procedure MostraReceiIng(SQLType: TSQLType);
    procedure MostraReceiFaz(SQLType: TSQLType);
    procedure MostraReceiXtr(SQLType: TSQLType);

  public
    { Public declarations }
    FSeq, FCabIni: Integer;
    FLocIni: Boolean;
    //
    procedure AtualizaTotal();
    procedure LocCod(Atual, Codigo: Integer);
    procedure ReopenReceiIng(Controle: Integer);
    procedure ReopenReceiFaz(Controle: Integer);
    procedure ReopenReceiXtr(Controle: Integer);

  end;

var
  FmReceiCab: TFmReceiCab;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, MyDBCheck, DmkDAC_PF, ReceiIng, ReceiFaz;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmReceiCab.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmReceiCab.MostraReceiFaz(SQLType: TSQLType);
var
  Codigo: Integer;
begin
  Codigo := QrReceiCabCodigo.Value;
  if DBCheck.CriaFm(TFmReceiFaz, FmReceiFaz, afmoNegarComAviso) then
  begin
    FmReceiFaz.ImgTipo.SQLType := SQLType;
    FmReceiFaz.FCodigo := Codigo;
    FmReceiFaz.FQrCab := QrReceiCab;
    FmReceiFaz.FDsCab := DsReceiCab;
    FmReceiFaz.FQrIts := QrReceiFaz;
    if SQLType = stIns then
      //
    else
    begin
      FmReceiFaz.EdControle.ValueVariant := QrReceiFazControle.Value;
      //
      FmReceiFaz.EdNome.Text               := QrReceiFazNome.Value;
      FmReceiFaz.EdNivel1.ValueVariant     := QrReceiFazNivel1.Value;
      FmReceiFaz.EdNivel2.ValueVariant     := QrReceiFazNivel2.Value;
      FmReceiFaz.MeDescricao.Text          := QrReceiFazDescricao.Value;
    end;
    FmReceiFaz.ShowModal;
    FmReceiFaz.Destroy;
    LocCod(Codigo, Codigo);
  end;
end;

procedure TFmReceiCab.MostraReceiIng(SQLType: TSQLType);
var
  Codigo: Integer;
begin
  if DBCheck.CriaFm(TFmReceiIng, FmReceiIng, afmoNegarComAviso) then
  begin
    Codigo := QrReceiCabCodigo.Value;
    FmReceiIng.ImgTipo.SQLType := SQLType;
    FmReceiIng.FCodigo := Codigo;
    FmReceiIng.FQrCab := QrReceiCab;
    FmReceiIng.FDsCab := DsReceiCab;
    FmReceiIng.FQrIts := QrReceiIng;
    if SQLType = stIns then
      //
    else
    begin
      FmReceiIng.EdControle.ValueVariant := QrReceiIngControle.Value;
      //
      FmReceiIng.EdNome.Text               := QrReceiIngNome.Value;
      FmReceiIng.EdGraGruX.ValueVariant    := QrReceiIngGraGruX.Value;
      FmReceiIng.CBGraGruX.KeyValue        := QrReceiIngGraGruX.Value;
      FmReceiIng.EdUnidMed.ValueVariant    := QrReceiIngUnidMed.Value;
      FmReceiIng.CBUnidMed.KeyValue        := QrReceiIngUnidMed.Value;
      FmReceiIng.EdQuantidade.ValueVariant := QrReceiIngQuantidade.Value;
      FmReceiIng.EdCustoUni.ValueVariant   := QrReceiIngCustoUni.Value;
      FmReceiIng.EdCustoTot.ValueVariant   := QrReceiIngCustoTot.Value;
    end;
    FmReceiIng.ShowModal;
    FmReceiIng.Destroy;
    LocCod(Codigo, Codigo);
  end;
end;

procedure TFmReceiCab.MostraReceiXtr(SQLType: TSQLType);
begin

end;

procedure TFmReceiCab.PMCabPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemCabUpd(CabAltera1, QrReceiCab);
  MyObjects.HabilitaMenuItemCabDel(CabExclui1, QrReceiCab, QrReceiIng);
end;

procedure TFmReceiCab.PMDicasPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(ItsInclui3, QrReceiCab);
  MyObjects.HabilitaMenuItemItsUpd(ItsAltera3, QrReceiXtr);
  MyObjects.HabilitaMenuItemItsDel(ItsExclui3, QrReceiXtr);
end;

procedure TFmReceiCab.PMItsPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(ItsInclui1, QrReceiCab);
  MyObjects.HabilitaMenuItemItsUpd(ItsAltera1, QrReceiIng);
  MyObjects.HabilitaMenuItemItsDel(ItsExclui1, QrReceiIng);
end;

procedure TFmReceiCab.PMModoPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(ItsInclui2, QrReceiCab);
  MyObjects.HabilitaMenuItemItsUpd(ItsAltera2, QrReceiFaz);
  MyObjects.HabilitaMenuItemItsDel(ItsExclui2, QrReceiFaz);
end;

procedure TFmReceiCab.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrReceiCabCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmReceiCab.DefParams;
begin
  VAR_GOTOTABELA := 'receicab';
  VAR_GOTOMYSQLTABLE := QrReceiCab;
  VAR_GOTONEG := gotoPiZ;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT *');
  VAR_SQLx.Add('FROM receicab');
  VAR_SQLx.Add('WHERE Codigo > 0');
  //
  VAR_SQL1.Add('AND Codigo=:P0');
  //
  //VAR_SQL2.Add('AND CodUsu=:P0');
  //
  VAR_SQLa.Add('AND Nome Like :P0');
  //
end;

procedure TFmReceiCab.ItsExclui2Click(Sender: TObject);
var
  Controle: Integer;
begin
  if UMyMod.ExcluiRegistroInt1('Confirma a exclus�o do item selecionado?',
  'ReceiFaz', 'Controle', QrReceiFazControle.Value, Dmod.MyDB) = ID_YES then
  begin
    Controle := GOTOy.LocalizaPriorNextIntQr(QrReceiFaz,
      QrReceiFazControle, QrReceiFazControle.Value);
    ReopenReceiFaz(Controle);
  end;
end;

procedure TFmReceiCab.ItsExclui3Click(Sender: TObject);
var
  Controle: Integer;
begin
  if UMyMod.ExcluiRegistroInt1('Confirma a exclus�o do item selecionado?',
  'ReceiXtr', 'Controle', QrReceiXtrControle.Value, Dmod.MyDB) = ID_YES then
  begin
    Controle := GOTOy.LocalizaPriorNextIntQr(QrReceiXtr,
      QrReceiXtrControle, QrReceiXtrControle.Value);
    ReopenReceiXtr(Controle);
  end;
end;

procedure TFmReceiCab.ItsInclui2Click(Sender: TObject);
begin
  MostraReceiFaz(stIns);
end;

procedure TFmReceiCab.ItsInclui3Click(Sender: TObject);
begin
  MostraReceiXtr(stUpd);
end;

procedure TFmReceiCab.ItsAltera1Click(Sender: TObject);
begin
  MostraReceiIng(stUpd);
end;

procedure TFmReceiCab.CabExclui1Click(Sender: TObject);
begin
  Geral.MB_Info('A��o n�o implementada! Solicite � Dermatek:' + sLineBreak +
  Caption + sLineBreak + TMenuItem(Sender).Name);
end;

procedure TFmReceiCab.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmReceiCab.QueryPrincipalAfterOpen;
begin
end;

procedure TFmReceiCab.ItsExclui1Click(Sender: TObject);
var
  Controle: Integer;
begin
  if UMyMod.ExcluiRegistroInt1('Confirma a exclus�o do item selecionado?',
  'ReceiIng', 'Controle', QrReceiIngControle.Value, Dmod.MyDB) = ID_YES then
  begin
    Controle := GOTOy.LocalizaPriorNextIntQr(QrReceiIng,
      QrReceiIngControle, QrReceiIngControle.Value);
    AtualizaTotal();
    ReopenReceiIng(Controle);
  end;
end;

procedure TFmReceiCab.ReopenReceiFaz(Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrReceiFaz, Dmod.MyDB, [
  'SELECT ref.* ',
  'FROM receifaz        ref ',
  'WHERE ref.Codigo > 0  ',
  'ORDER BY Nivel1, Nivel2, Controle',
  '']);
  //
  QrReceiFaz.Locate('Controle', Controle, []);
end;

procedure TFmReceiCab.ReopenReceiIng(Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrReceiIng, Dmod.MyDB, [
  'SELECT  ',
  'IF(rei.GraGruX <> 0, CONCAT(gg1.Nome,  ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)),  ',
  'IF(gcc.PrintCor=0 OR gcc.Codigo IS NULL, "", CONCAT(" ", gcc.Nome))),  ',
  'rei.Nome) NO_PRD_TAM_COR,',
  'med.Nome NO_UnidMed, rei.*  ',
  '',
  'FROM receiing        rei ',
  'LEFT JOIN gragrux    ggx ON ggx.Controle=rei.GraGruX',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC  ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad  ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI  ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1  ',
  'LEFT JOIN unidmed    med ON med.Codigo=rei.UnidMed',
  'WHERE rei.Codigo=' + Geral.FF0(QrReceiCabCodigo.Value),
  //'ORDER BY NO_PRD_TAM_COR ',
  'ORDER BY Controle ',
  '']);
  //
  QrReceiIng.Locate('Controle', Controle, []);
end;


procedure TFmReceiCab.ReopenReceiXtr(Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrReceiXtr, Dmod.MyDB, [
  'SELECT xtr.* ',
  'FROM receixtr        xtr ',
  'WHERE xtr.Codigo > 0  ',
  'ORDER BY Nivel1, Nivel2, Controle',
  '']);
  //
  QrReceiXtr.Locate('Controle', Controle, []);
end;

procedure TFmReceiCab.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmReceiCab.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmReceiCab.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmReceiCab.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmReceiCab.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmReceiCab.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmReceiCab.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrReceiCabCodigo.Value;
  Close;
end;

procedure TFmReceiCab.ItsInclui1Click(Sender: TObject);
begin
  MostraReceiIng(stIns);
end;

procedure TFmReceiCab.CabAltera1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrReceiCab, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'receicab');
end;

procedure TFmReceiCab.BtConfirmaClick(Sender: TObject);
var
  Sigla, Nome, Fonte, HrPreparo: String;
  Codigo: Integer;
  Porcoes, CustoTotal, CustoPorcao, PrecoPorcao, PrecoTotal: Double;
  SQLType: TSQLType;
begin
  SQLType        := ImgTipo.SQLType;
  Codigo         := EdCodigo.ValueVariant;
  Sigla          := EdSigla.ValueVariant;
  Nome           := EdNome.ValueVariant;
  Fonte          := EdFonte.ValueVariant;
  HrPreparo      := EdHrPreparo.Text;
  Porcoes        := EdPorcoes.ValueVariant;
  CustoTotal     := EdCustoTotal.ValueVariant;
  CustoPorcao    := EdCustoPorcao.ValueVariant;
  PrecoPorcao    := EdPrecoPorcao.ValueVariant;
  PrecoTotal     := EdPrecoTotal.ValueVariant;
  //
  if MyObjects.FIC(Length(Nome) = 0, EdNome, 'Defina uma descri��o!') then Exit;
  //
  Codigo := UMyMod.BPGS1I32('receicab', 'Codigo', '', '', tsPos, SQLType, Codigo);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'receicab', False, [
  'Sigla', 'Nome', 'Fonte',
  'HrPreparo', 'Porcoes', 'CustoTotal',
  'CustoPorcao', 'PrecoPorcao', 'PrecoTotal'], [
  'Codigo'], [
  Sigla, Nome, Fonte,
  HrPreparo, Porcoes, CustoTotal,
  CustoPorcao, PrecoPorcao, PrecoTotal], [
  Codigo], True) then
  begin
    ImgTipo.SQLType := stLok;
    PnDados.Visible := True;
    PnEdita.Visible := False;
    GOTOy.BotoesSb(ImgTipo.SQLType);
    //
    LocCod(Codigo, Codigo);
    if FSeq = 1 then Close;
  end;
end;

procedure TFmReceiCab.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := EdCodigo.ValueVariant;
  if ImgTipo.SQLType = stIns then
    UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'receicab', Codigo);
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'receicab', 'Codigo');
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmReceiCab.BtDicasClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMDicas, BtDicas);
end;

procedure TFmReceiCab.BtItsClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMIts, BtIts);
end;

procedure TFmReceiCab.BtModoClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMModo, BtModo);
end;

procedure TFmReceiCab.ItsAltera2Click(Sender: TObject);
begin
  MostraReceiFaz(stUpd);
end;

procedure TFmReceiCab.ItsAltera3Click(Sender: TObject);
begin
  MostraReceiXtr(stUpd);
end;

procedure TFmReceiCab.AtualizaTotal();
var
  sCodigo: String;
  CustoTot: Double;
begin
  sCodigo := Geral.FF0(QrReceiCabCodigo.Value);
  UnDmkDAC_PF.AbreMySQLQuery0(Dmod.QrAux, Dmod.MyDB, [
  'SELECT SUM(CustoTot) CustoTot',
  'FROM receiing',
  'WHERE Codigo=' + sCodigo,
  '']);
  CustoTot := Dmod.QrAUx.FieldByName('CustoTot').AsFloat;
  //
  UnDmkDAC_PF.ExecutaDB(Dmod.MyDB,
  ' UPDATE receicab SET CustoTotal=' +
  Geral.FFT_Dot(CustoTot, 2, siNegativo) +
  ' WHERE Codigo=' + sCodigo);
  //
  UnDmkDAC_PF.ExecutaDB(Dmod.MyDB,
  ' UPDATE receicab SET CustoPorcao = CustoTotal / Porcoes ' +
  ' WHERE Codigo=' + sCodigo);
  //
end;

procedure TFmReceiCab.BtCabClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMCab, BtCab);
end;

procedure TFmReceiCab.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  GBEdita.Align   := alClient;
  DGDados.Align   := alLeft;
  PnTextos.Align  := alClient;
  DBGModo.Align   := alClient;
  CriaOForm;
  FSeq := 0;
end;

procedure TFmReceiCab.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrReceiCabCodigo.Value, LaRegistro.Caption);
end;

procedure TFmReceiCab.SbImprimeClick(Sender: TObject);
begin
  MyObjects.frxDefineDataSets(frxReceita, [
  frxDsReceiCab,
  frxDsReceiIng,
  frxDsReceiFaz,
  frxDsReceiXtr
  ]);
  //
  MyObjects.frxMostra(frxReceita, 'Receita');
end;

procedure TFmReceiCab.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmReceiCab.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrReceiCabCodigo.Value, LaRegistro.Caption);
end;

procedure TFmReceiCab.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmReceiCab.QrReceiCabAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmReceiCab.QrReceiCabAfterScroll(DataSet: TDataSet);
begin
  ReopenReceiIng(0);
  ReopenReceiFaz(0);
  ReopenReceiXtr(0);
end;

procedure TFmReceiCab.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //
  if FSeq = 1 then
    CabInclui1Click(Self)
  else
  if (not FLocIni) and (FCabIni <> 0)  then
  begin
    LocCod(FCabIni, FCabIni);
    if QrReceiCabCodigo.Value <> FCabIni then
      Geral.MB_Aviso('Grupo n�o localizado!');
    FLocIni := True;
  end;
end;

procedure TFmReceiCab.SbQueryClick(Sender: TObject);
begin
  LocCod(QrReceiCabCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'receicab', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmReceiCab.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmReceiCab.frxReceitaGetValue(const VarName: string;
  var Value: Variant);
begin
  if VarName = 'VAR_NOMEEMPRESA' then
    Value := VAR_NOMEEMPRESA;
end;

procedure TFmReceiCab.CabInclui1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrReceiCab, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'receicab');
end;

procedure TFmReceiCab.QrReceiCabBeforeClose(
  DataSet: TDataSet);
begin
  QrReceiIng.Close;
  QrReceiFaz.Close;
  QrReceiXtr.Close;
end;

procedure TFmReceiCab.QrReceiCabBeforeOpen(DataSet: TDataSet);
begin
  QrReceiCabCodigo.DisplayFormat := FFormatFloat;
end;

end.

