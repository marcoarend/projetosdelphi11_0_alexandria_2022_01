unit UnRecei_Jan;

interface

uses
  Windows, SysUtils, Classes, DB, Menus, UnDmkEnums, mySQLDbTables, Forms,
  Grids, DBGrids, UnInternalConsts, dmkGeral, Controls, dmkPageControl,
  UnProjGroup_Vars;

type
  TUnRecei_Jan = class(TObject)
  private
    { Private declarations }

  public
    { Public declarations }
    procedure MostraFormReceiCab(Codigo: Integer);
  end;

var
  Recei_Jan: TUnRecei_Jan;


implementation

uses
  MyDBCheck, Module, OpcoesApp, Opcoes, ModuleGeral, UnMyObjects, ReceiCab;

{ TUnRecei_Jan }

procedure TUnRecei_Jan.MostraFormReceiCab(Codigo: Integer);
begin
  if DModG.QrParamsEmp.State = dsInactive then
    DModG.ReopenParamsEmp(Geral.IMV(VAR_LIB_EMPRESAS));
  if DBCheck.CriaFm(TFmReceiCab, FmReceiCab, afmoSoBoss) then
  begin
    if Codigo <> 0 then
      FmReceiCab.LocCod(Codigo, Codigo);
    FmReceiCab.ShowModal;
    FmReceiCab.Destroy;
  end;
end;

end.
