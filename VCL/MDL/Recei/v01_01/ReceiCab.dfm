object FmReceiCab: TFmReceiCab
  Left = 368
  Top = 194
  Caption = 'REC-EITAS-001 :: Cadastro de Receitas'
  ClientHeight = 555
  ClientWidth = 1008
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnEdita: TPanel
    Left = 0
    Top = 96
    Width = 1008
    Height = 459
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    Visible = False
    object GBEdita: TGroupBox
      Left = 0
      Top = 0
      Width = 1008
      Height = 101
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 0
      object Label7: TLabel
        Left = 8
        Top = 4
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label9: TLabel
        Left = 68
        Top = 4
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label3: TLabel
        Left = 812
        Top = 4
        Width = 26
        Height = 13
        Caption = 'Sigla:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label4: TLabel
        Left = 8
        Top = 44
        Width = 111
        Height = 13
        Caption = 'Fonte (Livro, URL etc.):'
        Color = clBtnFace
        ParentColor = False
      end
      object Label5: TLabel
        Left = 468
        Top = 44
        Width = 75
        Height = 13
        Caption = 'Tempo preparo:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label6: TLabel
        Left = 552
        Top = 44
        Width = 61
        Height = 13
        Caption = 'Qtd por'#231#245'es:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label8: TLabel
        Left = 640
        Top = 44
        Width = 66
        Height = 13
        Caption = '$ Custo Total:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label10: TLabel
        Left = 728
        Top = 44
        Width = 76
        Height = 13
        Caption = '$ Custo Por'#231#227'o:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label11: TLabel
        Left = 816
        Top = 44
        Width = 80
        Height = 13
        Caption = '$ Venda Por'#231#227'o:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label12: TLabel
        Left = 904
        Top = 44
        Width = 70
        Height = 13
        Caption = '$ Venda Total:'
        Color = clBtnFace
        ParentColor = False
      end
      object EdCodigo: TdmkEdit
        Left = 8
        Top = 20
        Width = 56
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Codigo'
        UpdCampo = 'Codigo'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdNome: TdmkEdit
        Left = 68
        Top = 20
        Width = 741
        Height = 21
        TabOrder = 1
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Nome'
        UpdCampo = 'Nome'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdSigla: TdmkEdit
        Left = 812
        Top = 20
        Width = 180
        Height = 21
        TabOrder = 2
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Sigla'
        UpdCampo = 'Sigla'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdFonte: TdmkEdit
        Left = 8
        Top = 60
        Width = 457
        Height = 21
        TabOrder = 3
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Fonte'
        UpdCampo = 'Fonte'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdHrPreparo: TdmkEdit
        Left = 468
        Top = 60
        Width = 81
        Height = 21
        TabOrder = 4
        FormatType = dmktfTime
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfLong
        Texto = '00:00:00'
        QryCampo = 'HrPreparo'
        UpdCampo = 'HrPreparo'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdPorcoes: TdmkEdit
        Left = 552
        Top = 60
        Width = 85
        Height = 21
        Alignment = taRightJustify
        TabOrder = 5
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 3
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,000'
        QryCampo = 'Porcoes'
        UpdCampo = 'Porcoes'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdCustoTotal: TdmkEdit
        Left = 640
        Top = 60
        Width = 85
        Height = 21
        Alignment = taRightJustify
        TabOrder = 6
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        QryCampo = 'CustoTotal'
        UpdCampo = 'CustoTotal'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdCustoPorcao: TdmkEdit
        Left = 728
        Top = 60
        Width = 85
        Height = 21
        Alignment = taRightJustify
        TabOrder = 7
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 4
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,0000'
        QryCampo = 'CustoPorcao'
        UpdCampo = 'CustoPorcao'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdPrecoPorcao: TdmkEdit
        Left = 816
        Top = 60
        Width = 85
        Height = 21
        Alignment = taRightJustify
        TabOrder = 8
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        QryCampo = 'PrecoPorcao'
        UpdCampo = 'PrecoPorcao'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdPrecoTotal: TdmkEdit
        Left = 904
        Top = 60
        Width = 85
        Height = 21
        Alignment = taRightJustify
        TabOrder = 9
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 4
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,0000'
        QryCampo = 'PrecoTotal'
        UpdCampo = 'PrecoTotal'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
    end
    object GBConfirma: TGroupBox
      Left = 0
      Top = 396
      Width = 1008
      Height = 63
      Align = alBottom
      TabOrder = 1
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 12
        Top = 17
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object Panel1: TPanel
        Left = 868
        Top = 15
        Width = 138
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 7
          Top = 2
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
    end
  end
  object PnDados: TPanel
    Left = 0
    Top = 96
    Width = 1008
    Height = 459
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    object Splitter1: TSplitter
      Left = 637
      Top = 85
      Width = 5
      Height = 310
      ExplicitLeft = 505
    end
    object GBDados: TGroupBox
      Left = 0
      Top = 0
      Width = 1008
      Height = 85
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 1
      object Label1: TLabel
        Left = 8
        Top = 4
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label2: TLabel
        Left = 68
        Top = 4
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label13: TLabel
        Left = 8
        Top = 44
        Width = 111
        Height = 13
        Caption = 'Fonte (Livro, URL etc.):'
        Color = clBtnFace
        ParentColor = False
      end
      object Label14: TLabel
        Left = 468
        Top = 44
        Width = 75
        Height = 13
        Caption = 'Tempo preparo:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label15: TLabel
        Left = 552
        Top = 44
        Width = 61
        Height = 13
        Caption = 'Qtd por'#231#245'es:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label16: TLabel
        Left = 640
        Top = 44
        Width = 66
        Height = 13
        Caption = '$ Custo Total:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label17: TLabel
        Left = 728
        Top = 44
        Width = 76
        Height = 13
        Caption = '$ Custo Por'#231#227'o:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label18: TLabel
        Left = 816
        Top = 44
        Width = 80
        Height = 13
        Caption = '$ Venda Por'#231#227'o:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label19: TLabel
        Left = 904
        Top = 44
        Width = 70
        Height = 13
        Caption = '$ Venda Total:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label20: TLabel
        Left = 812
        Top = 4
        Width = 26
        Height = 13
        Caption = 'Sigla:'
        Color = clBtnFace
        ParentColor = False
      end
      object DBEdCodigo: TdmkDBEdit
        Left = 8
        Top = 20
        Width = 56
        Height = 21
        TabStop = False
        DataField = 'Codigo'
        DataSource = DsReceiCab
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 0
        UpdType = utYes
        Alignment = taRightJustify
      end
      object DBEdNome: TdmkDBEdit
        Left = 68
        Top = 20
        Width = 741
        Height = 21
        Color = clWhite
        DataField = 'Nome'
        DataSource = DsReceiCab
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        UpdType = utYes
        Alignment = taLeftJustify
      end
      object dmkDBEdit1: TdmkDBEdit
        Left = 812
        Top = 20
        Width = 177
        Height = 21
        Color = clWhite
        DataField = 'Sigla'
        DataSource = DsReceiCab
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 2
        UpdType = utYes
        Alignment = taLeftJustify
      end
      object dmkDBEdit2: TdmkDBEdit
        Left = 8
        Top = 60
        Width = 457
        Height = 21
        Color = clWhite
        DataField = 'Fonte'
        DataSource = DsReceiCab
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 3
        UpdType = utYes
        Alignment = taLeftJustify
      end
      object dmkDBEdit3: TdmkDBEdit
        Left = 468
        Top = 60
        Width = 81
        Height = 21
        Color = clWhite
        DataField = 'HrPreparo'
        DataSource = DsReceiCab
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 4
        UpdType = utYes
        Alignment = taLeftJustify
      end
      object dmkDBEdit4: TdmkDBEdit
        Left = 552
        Top = 60
        Width = 85
        Height = 21
        Color = clWhite
        DataField = 'Porcoes'
        DataSource = DsReceiCab
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 5
        UpdType = utYes
        Alignment = taLeftJustify
      end
      object dmkDBEdit5: TdmkDBEdit
        Left = 640
        Top = 60
        Width = 85
        Height = 21
        Color = clWhite
        DataField = 'CustoTotal'
        DataSource = DsReceiCab
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 6
        UpdType = utYes
        Alignment = taLeftJustify
      end
      object dmkDBEdit6: TdmkDBEdit
        Left = 728
        Top = 60
        Width = 85
        Height = 21
        Color = clWhite
        DataField = 'CustoPorcao'
        DataSource = DsReceiCab
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 7
        UpdType = utYes
        Alignment = taLeftJustify
      end
      object dmkDBEdit7: TdmkDBEdit
        Left = 816
        Top = 60
        Width = 85
        Height = 21
        Color = clWhite
        DataField = 'PrecoPorcao'
        DataSource = DsReceiCab
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 8
        UpdType = utYes
        Alignment = taLeftJustify
      end
      object dmkDBEdit8: TdmkDBEdit
        Left = 904
        Top = 60
        Width = 85
        Height = 21
        Color = clWhite
        DataField = 'PrecoTotal'
        DataSource = DsReceiCab
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 9
        UpdType = utYes
        Alignment = taLeftJustify
      end
    end
    object GBCntrl: TGroupBox
      Left = 0
      Top = 395
      Width = 1008
      Height = 64
      Align = alBottom
      TabOrder = 0
      object Panel5: TPanel
        Left = 2
        Top = 15
        Width = 172
        Height = 47
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 174
        Top = 15
        Width = 126
        Height = 47
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel3: TPanel
        Left = 300
        Top = 15
        Width = 706
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object Panel2: TPanel
          Left = 573
          Top = 0
          Width = 133
          Height = 47
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 0
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
        object BtCab: TBitBtn
          Tag = 243
          Left = 4
          Top = 4
          Width = 140
          Height = 40
          Cursor = crHandPoint
          Caption = '&Receita'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtCabClick
        end
        object BtIts: TBitBtn
          Tag = 110
          Left = 144
          Top = 4
          Width = 140
          Height = 40
          Cursor = crHandPoint
          Caption = '&Ingredientes'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = BtItsClick
        end
        object BtModo: TBitBtn
          Tag = 243
          Left = 284
          Top = 4
          Width = 140
          Height = 40
          Cursor = crHandPoint
          Caption = '&Modo de fazer'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = BtModoClick
        end
        object BtDicas: TBitBtn
          Tag = 110
          Left = 424
          Top = 4
          Width = 140
          Height = 40
          Cursor = crHandPoint
          Caption = '&Dicas && Truques'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 4
          OnClick = BtDicasClick
        end
      end
    end
    object DGDados: TDBGrid
      Left = 0
      Top = 85
      Width = 637
      Height = 310
      Align = alLeft
      DataSource = DsReceiIng
      TabOrder = 2
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'Controle'
          Width = 44
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Quantidade'
          Width = 62
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_UnidMed'
          Title.Caption = 'Unidade'
          Width = 53
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'GraGruX'
          Title.Caption = 'Reduzido'
          Width = 51
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_PRD_TAM_COR'
          Title.Caption = 'Descri'#231#227'o'
          Width = 254
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CustoUni'
          Title.Caption = '$ Unit.'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CustoTot'
          Title.Caption = '$ Total'
          Visible = True
        end>
    end
    object PnTextos: TPanel
      Left = 768
      Top = 89
      Width = 280
      Height = 184
      BevelOuter = bvNone
      TabOrder = 3
      object Splitter2: TSplitter
        Left = 0
        Top = 87
        Width = 280
        Height = 5
        Cursor = crVSplit
        Align = alBottom
        ExplicitTop = 89
        ExplicitWidth = 452
      end
      object DBGModo: TDBGrid
        Left = 0
        Top = 0
        Width = 280
        Height = 49
        Align = alTop
        DataSource = DsReceiFaz
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'Controle'
            Width = 46
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Nivel1'
            Title.Caption = 'Nivel 1'
            Width = 38
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Nivel2'
            Title.Caption = 'Nivel 2'
            Width = 38
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Nome'
            Title.Caption = 'T'#237'tulo'
            Width = 140
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Descricao'
            Title.Caption = 'Descri'#231#227'o'
            Width = 300
            Visible = True
          end>
      end
      object DBGDicas: TDBGrid
        Left = 0
        Top = 92
        Width = 280
        Height = 92
        Align = alBottom
        DataSource = DsReceiXtr
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 12
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 216
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
        OnClick = SbImprimeClick
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 216
      Top = 0
      Width = 744
      Height = 52
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 264
        Height = 32
        Caption = 'Cadastro de Receitas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 264
        Height = 32
        Caption = 'Cadastro de Receitas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 264
        Height = 32
        Caption = 'Cadastro de Receitas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 52
    Width = 1008
    Height = 44
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = BtCab
    Left = 120
    Top = 64
  end
  object QrReceiCab: TMySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrReceiCabBeforeOpen
    AfterOpen = QrReceiCabAfterOpen
    BeforeClose = QrReceiCabBeforeClose
    AfterScroll = QrReceiCabAfterScroll
    SQL.Strings = (
      'SELECT * FROM receicab'
      'WHERE Codigo > 0')
    Left = 92
    Top = 233
    object QrReceiCabCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrReceiCabSigla: TWideStringField
      FieldName = 'Sigla'
      Required = True
    end
    object QrReceiCabNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 255
    end
    object QrReceiCabFonte: TWideStringField
      FieldName = 'Fonte'
      Size = 255
    end
    object QrReceiCabHrPreparo: TTimeField
      FieldName = 'HrPreparo'
      Required = True
    end
    object QrReceiCabPorcoes: TFloatField
      FieldName = 'Porcoes'
      Required = True
    end
    object QrReceiCabCustoTotal: TFloatField
      FieldName = 'CustoTotal'
      Required = True
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrReceiCabCustoPorcao: TFloatField
      FieldName = 'CustoPorcao'
      Required = True
      DisplayFormat = '#,###,###,##0.0000;-#,###,###,##0.0000; '
    end
    object QrReceiCabPrecoPorcao: TFloatField
      FieldName = 'PrecoPorcao'
      Required = True
      DisplayFormat = '#,###,###,##0.0000;-#,###,###,##0.0000; '
    end
    object QrReceiCabPrecoTotal: TFloatField
      FieldName = 'PrecoTotal'
      Required = True
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
  end
  object DsReceiCab: TDataSource
    DataSet = QrReceiCab
    Left = 92
    Top = 277
  end
  object QrReceiIng: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT '
      'IF(rei.GraGruX <> 0, CONCAT(gg1.Nome,  '
      
        'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nom' +
        'e)),  '
      
        'IF(gcc.PrintCor=0 OR gcc.Codigo IS NULL, "", CONCAT(" ", gcc.Nom' +
        'e))),  '
      'rei.Nome) PRD_TAM_COR,'
      'med.Nome NO_UnidMed, rei.*  '
      ''
      'FROM receiing        rei '
      'LEFT JOIN gragrux    ggx ON ggx.Controle=rei.GraGruX'
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC  '
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad  '
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI  '
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1  '
      'LEFT JOIN unidmed    med ON med.Codigo=rei.UnidMed'
      'WHERE ggx.Controle > 0  '
      'ORDER BY PRD_TAM_COR ')
    Left = 188
    Top = 237
    object QrReceiIngControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrReceiIngNO_PRD_TAM_COR: TWideStringField
      FieldName = 'NO_PRD_TAM_COR'
      Size = 255
    end
    object QrReceiIngNO_UnidMed: TWideStringField
      FieldName = 'NO_UnidMed'
      Size = 30
    end
    object QrReceiIngCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrReceiIngNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 255
    end
    object QrReceiIngUnidMed: TIntegerField
      FieldName = 'UnidMed'
      Required = True
    end
    object QrReceiIngQuantidade: TFloatField
      FieldName = 'Quantidade'
      Required = True
      DisplayFormat = '#,###,###,##0.000;-#,###,###,##0.000; '
    end
    object QrReceiIngGraGruX: TIntegerField
      FieldName = 'GraGruX'
      Required = True
      DisplayFormat = '0;-0; '
    end
    object QrReceiIngCustoUni: TFloatField
      FieldName = 'CustoUni'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrReceiIngCustoTot: TFloatField
      FieldName = 'CustoTot'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
  end
  object DsReceiIng: TDataSource
    DataSet = QrReceiIng
    Left = 188
    Top = 281
  end
  object PMIts: TPopupMenu
    OnPopup = PMItsPopup
    Left = 488
    Top = 464
    object ItsInclui1: TMenuItem
      Caption = '&Adiciona'
      Enabled = False
      OnClick = ItsInclui1Click
    end
    object ItsAltera1: TMenuItem
      Caption = '&Edita'
      Enabled = False
      OnClick = ItsAltera1Click
    end
    object ItsExclui1: TMenuItem
      Caption = '&Remove'
      Enabled = False
      OnClick = ItsExclui1Click
    end
  end
  object PMCab: TPopupMenu
    OnPopup = PMCabPopup
    Left = 360
    Top = 476
    object CabInclui1: TMenuItem
      Caption = '&Inclui'
      OnClick = CabInclui1Click
    end
    object CabAltera1: TMenuItem
      Caption = '&Altera'
      Enabled = False
      OnClick = CabAltera1Click
    end
    object CabExclui1: TMenuItem
      Caption = '&Exclui'
      Enabled = False
      OnClick = CabExclui1Click
    end
  end
  object PMModo: TPopupMenu
    OnPopup = PMModoPopup
    Left = 632
    Top = 468
    object ItsInclui2: TMenuItem
      Caption = '&Inclui'
      OnClick = ItsInclui2Click
    end
    object ItsAltera2: TMenuItem
      Caption = '&Altera'
      OnClick = ItsAltera2Click
    end
    object ItsExclui2: TMenuItem
      Caption = '&Exclui'
      OnClick = ItsExclui2Click
    end
  end
  object PMDicas: TPopupMenu
    OnPopup = PMDicasPopup
    Left = 776
    Top = 468
    object ItsInclui3: TMenuItem
      Caption = '&Inclui'
      OnClick = ItsInclui3Click
    end
    object ItsAltera3: TMenuItem
      Caption = '&Altera'
      OnClick = ItsAltera3Click
    end
    object ItsExclui3: TMenuItem
      Caption = '&Exclui'
      OnClick = ItsExclui3Click
    end
  end
  object QrReceiFaz: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ref.* '
      'FROM receifaz        ref '
      'WHERE ref.Codigo > 0  '
      'ORDER BY Nivel1, Nivel2, Controle')
    Left = 272
    Top = 237
    object QrReceiFazCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrReceiFazControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrReceiFazNivel1: TIntegerField
      FieldName = 'Nivel1'
      Required = True
    end
    object QrReceiFazNivel2: TIntegerField
      FieldName = 'Nivel2'
      Required = True
    end
    object QrReceiFazNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 60
    end
    object QrReceiFazDescricao: TWideStringField
      FieldName = 'Descricao'
      Required = True
      Size = 511
    end
  end
  object DsReceiFaz: TDataSource
    DataSet = QrReceiFaz
    Left = 272
    Top = 281
  end
  object QrReceiXtr: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ref.* '
      'FROM receifaz        ref '
      'WHERE ref.Codigo > 0  '
      'ORDER BY Nivel1, Nivel2, Controle')
    Left = 348
    Top = 237
    object QrReceiXtrCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrReceiXtrControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrReceiXtrNivel1: TIntegerField
      FieldName = 'Nivel1'
      Required = True
    end
    object QrReceiXtrNivel2: TIntegerField
      FieldName = 'Nivel2'
      Required = True
    end
    object QrReceiXtrNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 60
    end
    object QrReceiXtrDescricao: TWideStringField
      FieldName = 'Descricao'
      Required = True
      Size = 511
    end
  end
  object DsReceiXtr: TDataSource
    DataSet = QrReceiXtr
    Left = 348
    Top = 281
  end
  object frxReceita: TfrxReport
    Version = '2022.1'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39720.595331481500000000
    ReportOptions.LastChange = 41571.408359375000000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      '{'
      
        '  if <VFR_GRUPO1> = -1 then GH1.Visible := False else GH1.Visibl' +
        'e := True;'
      
        '  if <VFR_GRUPO1> = -1 then GF1.Visible := False else GF1.Visibl' +
        'e := True;'
      
        '  if <VFR_GRUPO2> = -1 then GH2.Visible := False else GH2.Visibl' +
        'e := True;'
      
        '  if <VFR_GRUPO2> = -1 then GF2.Visible := False else GF2.Visibl' +
        'e := True;'
      '  //'
      '  GH1.Condition := <VFR_CODITION_A1>;'
      '  GH2.Condition := <VFR_CODITION_B1>;      '
      '}'
      'end.')
    OnGetValue = frxReceitaGetValue
    Left = 224
    Top = 388
    Datasets = <
      item
        DataSet = frxDsReceiCab
        DataSetName = 'frxDsReceiCab'
      end
      item
        DataSet = frxDsReceiFaz
        DataSetName = 'frxDsReceiFaz'
      end
      item
        DataSet = frxDsReceiIng
        DataSetName = 'frxDsReceiIng'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
      MirrorMode = []
      object ReportTitle1: TfrxReportTitle
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 102.047229450000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Shape2: TfrxShapeView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo59: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Width = 665.197280000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          ParentFont = False
          VAlign = vaCenter
        end
        object Line9: TfrxLineView
          AllowVectorExport = True
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo60: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[Date]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MePagina: TfrxMemoView
          AllowVectorExport = True
          Left = 574.488560000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo61: TfrxMemoView
          AllowVectorExport = True
          Left = 3.779530000000000000
          Top = 39.685039370000000000
          Width = 672.756340000000000000
          Height = 37.795300000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsReceiCab."Nome"]')
          ParentFont = False
          VAlign = vaCenter
          Formats = <
            item
            end
            item
            end>
        end
        object Memo62: TfrxMemoView
          AllowVectorExport = True
          Left = 102.047310000000000000
          Top = 18.897650000000000000
          Width = 472.441005910000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[VAR_NOMEEMPRESA]')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          AllowVectorExport = True
          Left = 90.708676060000000000
          Top = 83.149601420000000000
          Width = 128.503624570000000000
          Height = 15.118110240000000000
          DisplayFormat.FormatStr = 'hh:mm:ss'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsReceiCab."HrPreparo"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo11: TfrxMemoView
          AllowVectorExport = True
          Top = 83.149601420000000000
          Width = 90.677180000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Tempo de preparo:')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo38: TfrxMemoView
          AllowVectorExport = True
          Left = 336.378126060000000000
          Top = 83.149660000000000000
          Width = 128.503624570000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsReceiCab."Porcoes"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo39: TfrxMemoView
          AllowVectorExport = True
          Left = 226.771800000000000000
          Top = 83.149660000000000000
          Width = 102.015770000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Quantidade de por'#231#245'es:')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object GF2: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 3.779527559055118000
        Top = 502.677490000000000000
        Width = 680.315400000000000000
      end
      object GH2: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 22.677167800000000000
        Top = 411.968770000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsReceiFaz."Nivel1"'
        object Memo1: TfrxMemoView
          AllowVectorExport = True
          Width = 680.315097320000000000
          Height = 22.677167800000000000
          DataSet = frxDsReceiIng
          DataSetName = 'frxDsReceiIng'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -21
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsReceiFaz."Nome"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 22.677180000000000000
        Top = 457.323130000000000000
        Width = 680.315400000000000000
        Columns = 1
        ColumnWidth = 200.000000000000000000
        ColumnGap = 20.000000000000000000
        DataSet = frxDsReceiFaz
        DataSetName = 'frxDsReceiFaz'
        RowCount = 0
        Stretched = True
        object Memo3: TfrxMemoView
          AllowVectorExport = True
          Left = 34.015770000000000000
          Width = 646.299630000000000000
          Height = 22.677180000000000000
          StretchMode = smMaxHeight
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsReceiFaz."Descricao"]')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          AllowVectorExport = True
          Width = 34.015730940000000000
          Height = 22.677167800000000000
          StretchMode = smMaxHeight
          DataField = 'Nivel2'
          DataSet = frxDsReceiFaz
          DataSetName = 'frxDsReceiFaz'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsReceiFaz."Nivel2"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
      end
      object ColumnHeader1: TfrxColumnHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118110240000000000
        Top = 143.622140000000000000
        Width = 680.315400000000000000
      end
      object GH1: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 31.401360000000000000
        Top = 359.055350000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsReceiFaz."Codigo"'
        object Memo43: TfrxMemoView
          AllowVectorExport = True
          Width = 680.314960629921300000
          Height = 26.456700240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'MODO DE PREPARO')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object GF1: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 3.779527559055118000
        Top = 529.134200000000000000
        Width = 680.315400000000000000
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 22.677180000000000000
        Top = 593.386210000000000000
        Width = 680.315400000000000000
        object Memo4: TfrxMemoView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
      end
      object GroupHeader1: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 49.133877800000000000
        Top = 219.212740000000000000
        Width = 680.315400000000000000
        Condition = 'frxDsReceiIng."Codigo"'
        object Memo31: TfrxMemoView
          AllowVectorExport = True
          Left = 136.063001890000000000
          Top = 30.236240000000000000
          Width = 75.590551180000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Unidade')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo15: TfrxMemoView
          AllowVectorExport = True
          Top = 30.236240000000000000
          Width = 60.472440940000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Reduzido')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo22: TfrxMemoView
          AllowVectorExport = True
          Left = 60.472480000000000000
          Top = 30.236240000000000000
          Width = 75.590551180000000000
          Height = 18.897637800000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.00;-#,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Quantidade')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo42: TfrxMemoView
          AllowVectorExport = True
          Top = 0.062770000000000010
          Width = 680.314960629921300000
          Height = 26.456700240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'INGREDIENTES:')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo7: TfrxMemoView
          AllowVectorExport = True
          Left = 211.653680000000000000
          Top = 30.236240000000000000
          Width = 468.661417320000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Descri'#231#227'o')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
      end
      object MasterData2: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 18.897637800000000000
        Top = 291.023810000000000000
        Width = 680.315400000000000000
        Columns = 1
        ColumnWidth = 200.000000000000000000
        ColumnGap = 20.000000000000000000
        DataSet = frxDsReceiIng
        DataSetName = 'frxDsReceiIng'
        RowCount = 0
        object Memo52: TfrxMemoView
          AllowVectorExport = True
          Left = 136.063001890000000000
          Width = 75.590551180000000000
          Height = 18.897637800000000000
          DataField = 'NO_UnidMed'
          DataSet = frxDsReceiIng
          DataSetName = 'frxDsReceiIng'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsReceiIng."NO_UnidMed"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo64: TfrxMemoView
          AllowVectorExport = True
          Width = 60.472440940000000000
          Height = 18.897637800000000000
          DataField = 'GraGruX'
          DataSet = frxDsReceiIng
          DataSetName = 'frxDsReceiIng'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsReceiIng."GraGruX"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo68: TfrxMemoView
          AllowVectorExport = True
          Left = 60.472480000000000000
          Width = 75.590551180000000000
          Height = 18.897637800000000000
          DataField = 'Quantidade'
          DataSet = frxDsReceiIng
          DataSetName = 'frxDsReceiIng'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,##0.000;-#,###,##0.000; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsReceiIng."Quantidade"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo26: TfrxMemoView
          AllowVectorExport = True
          Left = 211.653680000000000000
          Width = 468.661417320000000000
          Height = 18.897637800000000000
          DataField = 'NO_PRD_TAM_COR'
          DataSet = frxDsReceiIng
          DataSetName = 'frxDsReceiIng'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsReceiIng."NO_PRD_TAM_COR"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
      end
      object GroupFooter1: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 3.779527559055118000
        Top = 332.598640000000000000
        Width = 680.315400000000000000
      end
    end
  end
  object frxDsReceiCab: TfrxDBDataset
    UserName = 'frxDsReceiCab'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Codigo=Codigo'
      'Sigla=Sigla'
      'Nome=Nome'
      'Fonte=Fonte'
      'HrPreparo=HrPreparo'
      'Porcoes=Porcoes'
      'CustoTotal=CustoTotal'
      'CustoPorcao=CustoPorcao'
      'PrecoPorcao=PrecoPorcao'
      'PrecoTotal=PrecoTotal')
    DataSet = QrReceiCab
    BCDToCurrency = False
    DataSetOptions = []
    Left = 88
    Top = 324
  end
  object frxDsReceiIng: TfrxDBDataset
    UserName = 'frxDsReceiIng'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Controle=Controle'
      'NO_PRD_TAM_COR=NO_PRD_TAM_COR'
      'NO_UnidMed=NO_UnidMed'
      'Codigo=Codigo'
      'Nome=Nome'
      'UnidMed=UnidMed'
      'Quantidade=Quantidade'
      'GraGruX=GraGruX')
    DataSet = QrReceiIng
    BCDToCurrency = False
    DataSetOptions = []
    Left = 184
    Top = 328
  end
  object frxDsReceiFaz: TfrxDBDataset
    UserName = 'frxDsReceiFaz'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Codigo=Codigo'
      'Controle=Controle'
      'Nivel1=Nivel1'
      'Nivel2=Nivel2'
      'Nome=Nome'
      'Descricao=Descricao')
    DataSet = QrReceiFaz
    BCDToCurrency = False
    DataSetOptions = []
    Left = 272
    Top = 328
  end
  object frxDsReceiXtr: TfrxDBDataset
    UserName = 'frxDsReceiXtr'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Codigo=Codigo'
      'Controle=Controle'
      'Nivel1=Nivel1'
      'Nivel2=Nivel2'
      'Nome=Nome'
      'Descricao=Descricao')
    DataSet = QrReceiXtr
    BCDToCurrency = False
    DataSetOptions = []
    Left = 348
    Top = 332
  end
end
