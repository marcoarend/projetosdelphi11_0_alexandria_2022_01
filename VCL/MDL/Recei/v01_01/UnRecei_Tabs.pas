unit UnRecei_Tabs;
{
function TMyListas.CriaListaTabelas(Database: TmySQLDatabase; Lista: TList): Boolean;
      Recei_Tabs.CarregaListaTabelas(FTabelas);

function TMyListas.CriaListaSQL(Tabela: String; FListaSQL: TStringList): Boolean;
    Recei_Tabs.CarregaListaSQL(Tabela, FListaSQL);

function TMyListas.CriaListaIndices(TabelaBase, TabelaNome: String; FLIndices: TList): Boolean;
      Recei_Tabs.CarregaListaFRIndices(TabelaBase, FRIndices, FLindices);

function TMyListas.CriaListaCampos(Tabela: String; FLCampos: TList; var Temcontrole: TTemControle): Boolean;
      Recei_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
    Recei_Tabs.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);

function TMyListas.CriaListaJanelas(FLJanelas: TList): Boolean;
  Recei_Tabs.CompletaListaFRJanelas(FRJanelas, FLJanelas);
}

interface

uses
  System.Generics.Collections,
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Dialogs, DB,
  (*DBTables,*) mySQLDBTables, UnMyLinguas, Forms, UnInternalConsts, MyDBCheck,
  dmkGeral, UnDmkEnums;

type
  TRecei_Tabs = class(TObject)
  private
    { Private declarations }
  public
    { Public declarations }
    function CarregaListaTabelas(Database: TmySQLDatabase;
             Lista: TList<TTabelas>): Boolean;
    function CarregaListaFRCampos(Tabela: String; FRCampos:
             TCampos; FLCampos: TList<TCampos>; var TemControle: TTemControle): Boolean;
    function CompletaListaFRJanelas(FRJanelas:
             TJanelas; FLJanelas: TList<TJanelas>): Boolean;
    function CarregaListaFRIndices(Tabela: String; FRIndices:
             TIndices; FLIndices: TList<TIndices>): Boolean;
    function CarregaListaSQL(Tabela: String; FListaSQL: TStringList;
             DMKID_APP: Integer): Boolean;
    function CarregaListaFRQeiLnk(TabelaCria: String; FRQeiLnk: TQeiLnk;
             FLQeiLnk: TList<TQeiLnk>): Boolean;
    function ComplementaListaSQL(Tabela: String; FListaSQL: TStringList):
             Boolean;
    function CompletaListaFRCampos(Tabela: String; FRCampos:
             TCampos; FLCampos: TList<TCampos>): Boolean;
  end;

//const

var
  Recei_Tabs: TRecei_Tabs;

implementation

uses UMySQLModule, Module, MyListas;

function TRecei_Tabs.CarregaListaTabelas(Database: TmySQLDatabase;
  Lista: TList<TTabelas>): Boolean;
begin
  try
    if Database = Dmod.MyDB then
    begin
      (*
      if CO_SIGLA_APP = 'CLRC' then
      begin
        MyLinguas.AdTbLst(Lista, False, Lowercase('UnidMed'), '');
      end else
      *)
      begin
        //MyLinguas.AdTbLst(Lista, False, Lowercase('???'), '_lst_sample');
        MyLinguas.AdTbLst(Lista, False, Lowercase('ReceiCab'), '');
        MyLinguas.AdTbLst(Lista, False, Lowercase('ReceiIng'), '');
        MyLinguas.AdTbLst(Lista, False, Lowercase('ReceiFaz'), '');
        MyLinguas.AdTbLst(Lista, False, Lowercase('ReceiXtr'), 'ReceiFaz');
      end;
      //
    end;
    Result := True;
  except
    raise;
    Result := False;
  end;
end;

function TRecei_Tabs.CarregaListaSQL(Tabela: String; FListaSQL:
  TStringList; DMKID_APP: Integer): Boolean;
begin
  Result := True;
  //end else
  if Uppercase(Tabela) = Uppercase('???') then
  begin
    if FListaSQL.Count = 0 then  // evitar erro no blue derm! 2012-01-16
    FListaSQL.Add('Codigo|Nome');
    FListaSQL.Add('0|"???"');
  end else
  if Uppercase(Tabela) = Uppercase('?????') then
  begin
    if FListaSQL.Count = 0 then  // evitar erro no blue derm! 2012-01-16
      FListaSQL.Add('Codigo|CodUsu|Sequencia|Grandeza|Sigla|Nome');
    //
    if DMKID_APP = 24 then //Bugstrol
    begin
      FListaSQL.Add('1|001|000|000|"P�"|"PE�A"');
      FListaSQL.Add('2|002|000|000|"PCTE"|"PACOTE"');
      FListaSQL.Add('3|003|000|005|"L"|"LITROS"');
      FListaSQL.Add('4|004|000|005|"ML"|"MILILITROS"');
      FListaSQL.Add('5|005|000|000|"BL"|"BLOCO"');
      FListaSQL.Add('6|006|000|002|"G"|"GRAMAS"');
      FListaSQL.Add('7|007|000|000|"UN"|"UNIDADE"');
    end else
    begin
      FListaSQL.Add('-1|-1|000|003|"??"|"? ? ? ? ?"');
      FListaSQL.Add('0|000|000|003|""|""');
    end;
  end else
end;

function TRecei_Tabs.ComplementaListaSQL(Tabela: String; FListaSQL:
TStringList): Boolean;
begin
  Result := True;
  if Uppercase(Tabela) = Uppercase('?????') then
  begin
  end else
end;

function TRecei_Tabs.CarregaListaFRIndices(Tabela: String; FRIndices:
 TIndices; FLIndices: TList<TIndices>): Boolean;
begin
  Result := True;
  //end else
  if Uppercase(Tabela) = Uppercase('ReceiCab') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('ReceiIng') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Controle';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('ReceiFaz') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Controle';
    FLIndices.Add(FRIndices);
    //
  end else
end;

function TRecei_Tabs.CarregaListaFRQeiLnk(TabelaCria: String; FRQeiLnk: TQeiLnk;
  FLQeiLnk: TList<TQeiLnk>): Boolean;
begin
// n�o implementado!
  Result := False;
end;

function TRecei_Tabs.CompletaListaFRCampos(Tabela: String; FRCampos:
 TCampos; FLCampos: TList<TCampos>): Boolean;
begin
  Result := True;
  try
    if Uppercase(Tabela) = Uppercase('Controle') then
    begin
      //
(*
      New(FRCampos);
      FRCampos.Field      := 'UnidMed';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES'; // ber-lin DEFAUL ERROR
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
*)
    end;
  except
    raise;
    Result := False;
  end;
end;

function TRecei_Tabs.CarregaListaFRCampos(Tabela: String; FRCampos:
 TCampos; FLCampos: TList<TCampos>; var TemControle: TTemControle): Boolean;
begin
  Result := True;
  TemControle := TemControle + cTemControleNao;
  try
    //end else
    if Uppercase(Tabela) = Uppercase('ReceiCab') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Sigla';
      FRCampos.Tipo       := 'varchar(20)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '??';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '? ? ?';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Fonte';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'HrPreparo';
      FRCampos.Tipo       := 'time';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '00:00:00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Porcoes';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CustoTotal';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CustoPorcao';
      FRCampos.Tipo       := 'double(15,4)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.0000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PrecoPorcao';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'PrecoTotal';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('ReceiIng') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'UnidMed';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'GraGruX';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Quantidade';
      FRCampos.Tipo       := 'double(15,3)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CustoUni';
      FRCampos.Tipo       := 'double(15,6)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.000000';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CustoTot';
      FRCampos.Tipo       := 'double(15,2)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0.00';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('ReceiFaz') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Controle';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nivel1';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nivel2';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Descricao';
      FRCampos.Tipo       := 'varchar(511)';
      FRCampos.Null       := 'NO';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
  except
    raise;
    Result := False;
  end;
end;

function TRecei_Tabs.CompletaListaFRJanelas(FRJanelas:
 TJanelas; FLJanelas: TList<TJanelas>): Boolean;
begin
  //
  // REC-EITAS-001 :: Cadastro de Receitas
  New(FRJanelas);
  FRJanelas.ID        := 'REC-EITAS-001';
  FRJanelas.Nome      := 'FmReceiCab';
  FRJanelas.Descricao := 'Cadastro de Receitas';
  FRJanelas.Modulo    := 'Rcei';
  FLJanelas.Add(FRJanelas);
  //
  // REC-EITAS-002 :: Ingrediente de Receita
  New(FRJanelas);
  FRJanelas.ID        := 'REC-EITAS-002';
  FRJanelas.Nome      := 'FmReceiIng';
  FRJanelas.Descricao := 'Ingrediente de Receita';
  FRJanelas.Modulo    := 'Rcei';
  FLJanelas.Add(FRJanelas);
  //
  // REC-EITAS-003 :: Item de Modo de Fazer
  New(FRJanelas);
  FRJanelas.ID        := 'REC-EITAS-003';
  FRJanelas.Nome      := 'FmReceiFaz';
  FRJanelas.Descricao := 'Item de Modo de Fazer';
  FRJanelas.Modulo    := 'Rcei';
  FLJanelas.Add(FRJanelas);
  //
  // REC-EITAS-004 :: Item de Dicas & Truques
  New(FRJanelas);
  FRJanelas.ID        := 'REC-EITAS-004';
  FRJanelas.Nome      := 'FmReceiXtr';
  FRJanelas.Descricao := 'Item de Dicas & Truques';
  FRJanelas.Modulo    := 'Rcei';
  FLJanelas.Add(FRJanelas);
  //
  //
  //
  //////////////////////////////////////////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////////
  Result := True;
  //////////////////////////////////////////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////////
end;

end.
