unit ReceiIng;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, DBCtrls, dmkLabel, Mask,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkDBEdit, dmkGeral, Variants,
  dmkValUsu, dmkImage, UnDmkEnums;

type
  TFmReceiIng = class(TForm)
    Panel1: TPanel;
    CkContinuar: TCheckBox;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    QrGraGruX: TMySQLQuery;
    DsGraGruX: TDataSource;
    Panel3: TPanel;
    Label6: TLabel;
    EdControle: TdmkEdit;
    Label7: TLabel;
    EdNome: TdmkEdit;
    Label1: TLabel;
    EdGraGruX: TdmkEditCB;
    CBGraGruX: TdmkDBLookupComboBox;
    SpeedButton1: TSpeedButton;
    QrGraGruXControle: TIntegerField;
    QrGraGruXNO_PRD_TAM_COR: TWideStringField;
    QrUnidMed: TMySQLQuery;
    QrUnidMedCodigo: TIntegerField;
    QrUnidMedCodUsu: TIntegerField;
    QrUnidMedSigla: TWideStringField;
    QrUnidMedNome: TWideStringField;
    DsUnidMed: TDataSource;
    EdUnidMed: TdmkEditCB;
    Label2: TLabel;
    EdSigla: TdmkEdit;
    CBUnidMed: TdmkDBLookupComboBox;
    SBUnidMed: TSpeedButton;
    Label3: TLabel;
    EdQuantidade: TdmkEdit;
    EdCustoUni: TdmkEdit;
    Label4: TLabel;
    EdCustoTot: TdmkEdit;
    Label5: TLabel;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure EdUnidMedChange(Sender: TObject);
    procedure EdUnidMedKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdSiglaChange(Sender: TObject);
    procedure EdSiglaExit(Sender: TObject);
    procedure EdSiglaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure CBUnidMedKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SBUnidMedClick(Sender: TObject);
    procedure EdQuantidadeRedefinido(Sender: TObject);
    procedure EdCustoUniRedefinido(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenReceiIng(Controle: Integer);
    procedure CalculaTotal();

  public
    { Public declarations }
    FCodigo: Integer;
    FQrCab, FQrIts: TmySQLQuery;
    FDsCab: TDataSource;
  end;

  var
  FmReceiIng: TFmReceiIng;

implementation

uses UnMyObjects, Module, UMySQLModule, UnInternalConsts, MyDBCheck, DmkDAC_PF,
  ModProd, UnUMedi_PF, UnMySQLCuringa, ReceiCab;

{$R *.DFM}

procedure TFmReceiIng.BtOKClick(Sender: TObject);
var
  Nome: String;
  Codigo, Controle, UnidMed, GraGruX: Integer;
  Quantidade, CustoUni, CustoTot: Double;
  SQLType: TSQLType;
  Impede: Boolean;
begin
  SQLType        := ImgTipo.SQLType;
  Codigo         := FCodigo;
  Controle       := EdControle.ValueVariant;
  Nome           := EdNome.ValueVariant;
  UnidMed        := EdUnidMed.ValueVariant;
  Quantidade     := EdQuantidade.ValueVariant;
  GraGruX        := EdGraGruX.ValueVariant;
  CustoUni       := EdCustoUni.ValueVariant;
  CustoTot       := EdCustoTot.ValueVariant;
  //
  Impede := (Trim(EdNome.Text) = '') and (GraGruX = 0);
  if MyObjects.FIC(Impede, EdNome, 'Informe uma descri��o ou reduzido!') then
    Exit;
  if Quantidade = 0 then
    if Geral.MB_Pergunta('A quantidade n�o foi informada!' +
    sLineBreak + 'Deseja informar antes de confirmar?') = ID_YES then
      Exit;
  //
  Controle := UMyMod.BPGS1I32('receiing', 'Controle', '', '', tsPos, SQLType, Controle);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'receiing', False, [
  'Codigo', 'Nome', 'UnidMed',
  'Quantidade', 'GraGruX',
  'CustoUni', 'CustoTot'], [
  'Controle'], [
  Codigo, Nome, UnidMed,
  Quantidade, GraGruX,
  CustoUni, CustoTot], [
  Controle], True) then
  begin
    FmReceiCab.AtualizaTotal();
    ReopenReceiIng(Controle);
    if CkContinuar.Checked then
    begin
      ImgTipo.SQLType            := stIns;
      EdControle.ValueVariant    := 0;
      EdGraGruX.ValueVariant     := 0;
      CBGraGruX.KeyValue         := Null;
      EdUnidMed.ValueVariant     := 0;
      CBUnidMed.KeyValue         := Null;
      EdNome.ValueVariant        := '';
      EdQuantidade.ValueVariant        := '';
      EdNome.SetFocus;
    end else Close;
  end;
end;

procedure TFmReceiIng.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmReceiIng.CalculaTotal();
var
  Quantidade, CustoUni, CustoTot: Double;
begin
  Quantidade := EdQuantidade.ValueVariant;
  CustoUni   := EdCustoUni.ValueVariant;
  CustoTot   := Quantidade * CustoUni;
  //
  EdCustoTot.ValueVariant := CustoTot;
end;

procedure TFmReceiIng.CBUnidMedKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key=VK_F3 then
    CuringaLoc.Pesquisa('CodUsu', 'Nome', 'UnidMed', Dmod.MyDB,
    ''(*Extra*), EdUnidMed, CBUnidMed, dmktfInteger);
end;

procedure TFmReceiIng.EdCustoUniRedefinido(Sender: TObject);
begin
  CalculaTotal();
end;

procedure TFmReceiIng.EdQuantidadeRedefinido(Sender: TObject);
begin
  CalculaTotal();
end;

procedure TFmReceiIng.EdSiglaChange(Sender: TObject);
begin
  if EdSigla.Focused then
    DmProd.PesquisaPorSigla(False, EdSigla, EdUnidMed, CBUnidMed);
end;

procedure TFmReceiIng.EdSiglaExit(Sender: TObject);
begin
  DmProd.PesquisaPorSigla(True, EdSigla, EdUnidMed, CBUnidMed);
end;

procedure TFmReceiIng.EdSiglaKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key=VK_F3 then
    CuringaLoc.Pesquisa('CodUsu', 'Nome', 'UnidMed', Dmod.MyDB,
    ''(*Extra*), EdUnidMed, CBUnidMed, dmktfInteger)
end;

procedure TFmReceiIng.EdUnidMedChange(Sender: TObject);
begin
  if not EdSigla.Focused then
    DmProd.PesquisaPorCodigo(EdUnidMed.ValueVariant, EdSigla);
end;

procedure TFmReceiIng.EdUnidMedKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key=VK_F3 then
    CuringaLoc.Pesquisa('CodUsu', 'Nome', 'UnidMed', Dmod.MyDB,
    ''(*Extra*), EdUnidMed, CBUnidMed, dmktfInteger);
end;

procedure TFmReceiIng.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmReceiIng.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrGraGruX, Dmod.MyDB, [
  'SELECT ggx.Controle,  ',
  'CONCAT(gg1.Nome,  ',
  'IF(gti.PrintTam=0 OR gti.Codigo IS NULL, "", CONCAT(" ", gti.Nome)),  ',
  'IF(gcc.PrintCor=0 OR gcc.Codigo IS NULL, "", CONCAT(" ", gcc.Nome)))  ',
  'NO_PRD_TAM_COR  ',
  'FROM gragrux ggx  ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC  ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad  ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI  ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1  ',
  'WHERE ggx.Controle > 0  ',
  'ORDER BY NO_PRD_TAM_COR ',
  '']);
  UnDmkDAC_PF.AbreMySQLQuery0(QrUnidMed, Dmod.MyDB, [
  'SELECT Codigo, CodUsu, Sigla, Nome ',
  'FROM unidmed ',
  'ORDER BY Nome ',
  '']);
end;

procedure TFmReceiIng.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmReceiIng.ReopenReceiIng(Controle: Integer);
begin
  if FQrIts <> nil then
  begin
    UnDmkDAC_PF.AbreQuery(FQrIts, Dmod.MyDB);
    //
    if Controle <> 0 then
      FQrIts.Locate('Controle', Controle, []);
  end;
end;

procedure TFmReceiIng.SBUnidMedClick(Sender: TObject);
var
  Codigo: Integer;
begin
  Codigo       := EdUnidMed.ValueVariant;
  UMedi_PF.CriaEEscolheUnidMed(Codigo, EdUnidMed, CBUnidMed, QrUnidMed);
end;

procedure TFmReceiIng.SpeedButton1Click(Sender: TObject);
begin
(*
  VAR_CADASTRO := 0;
  if DBCheck.CriaFm(TFm_CadSel_, Fm_CadSel_, afmoNegarComAviso) then
  begin
    Fm_CadSel_.ShowModal;
    Fm_CadSel_.Destroy;
  end;
  UMyMod.SetaCodigoPesquisado(EdSel, CBSel, QrSel, VAR_CADASTRO);
*)
end;

end.
