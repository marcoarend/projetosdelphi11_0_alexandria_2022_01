unit Module;

//Lic_Dmk.LiberaUso5

interface

uses
  Winapi.Windows, Vcl.Dialogs, StdCtrls,
  (*Messages, Graphics, Controls, Forms,
  DBTables, FileCtrl, UMySQLModule, dmkEdit,
  Winsock, MySQLBatch, frxClass, frxDBSet,
  Variants, ABSMain,  ComCtrls, IdBaseComponent, IdComponent,
  IdIPWatch, UnBugs_Tabs, UnDmkEnums, UnProjGroup_Consts;*)
  System.SysUtils, System.Classes, Data.DB, mySQLDbTables,
  Vcl.Forms,
  DmkGeral, ZCF2, UnInternalConsts, UnGrl_Vars, UnDmkEnums, UnGrl_Consts,
  mySQLDirectQuery;

type
  TDmod = class(TDataModule)
    MyDB: TmySQLDatabase;
    QrUpd: TmySQLQuery;
    QrAux: TmySQLQuery;
    QrMas: TmySQLQuery;
    QrSQL: TmySQLQuery;
    QrIdx: TmySQLQuery;
    QrNTV: TmySQLQuery;
    MyDBn: TmySQLDatabase;
    QrControle: TmySQLQuery;
    QrNTI: TmySQLQuery;
    QrPriorNext: TmySQLQuery;
    QrAgora: TmySQLQuery;
    QrAgoraANO: TLargeintField;
    QrAgoraMES: TLargeintField;
    QrAgoraDIA: TLargeintField;
    QrAgoraHORA: TLargeintField;
    QrAgoraMINUTO: TLargeintField;
    QrAgoraSEGUNDO: TLargeintField;
    QrAgoraAGORA: TDateTimeField;
    QrTerminal: TmySQLQuery;
    QrTerminalIP: TWideStringField;
    QrTerminalTerminal: TIntegerField;
    QrUpdU: TmySQLQuery;
    MyLocDatabase: TmySQLDatabase;
    ZZDB: TmySQLDatabase;
    QrUpdM: TmySQLQuery;
    QrAuxL: TmySQLQuery;
    QrMaster: TmySQLQuery;
    QrMasterCNPJ_TXT: TWideStringField;
    QrMasterTE1_TXT: TWideStringField;
    QrMasterCEP_TXT: TWideStringField;
    QrMasterEm: TWideStringField;
    QrMasterTipo: TSmallintField;
    QrMasterLogo: TBlobField;
    QrMasterDono: TIntegerField;
    QrMasterVersao: TIntegerField;
    QrMasterCNPJ: TWideStringField;
    QrMasterIE: TWideStringField;
    QrMasterECidade: TWideStringField;
    QrMasterNOMEUF: TWideStringField;
    QrMasterEFax: TWideStringField;
    QrMasterERua: TWideStringField;
    QrMasterEBairro: TWideStringField;
    QrMasterECompl: TWideStringField;
    QrMasterEContato: TWideStringField;
    QrMasterECel: TWideStringField;
    QrMasterETe1: TWideStringField;
    QrMasterETe2: TWideStringField;
    QrMasterETe3: TWideStringField;
    QrMasterEPais: TWideStringField;
    QrMasterRespons1: TWideStringField;
    QrMasterRespons2: TWideStringField;
    QrMasterECEP: TIntegerField;
    QrMasterLogo2: TBlobField;
    QrMasterLimite: TSmallintField;
    QrMasterSolicitaSenha: TIntegerField;
    QrMasterENumero: TFloatField;
    QrMasterUsaAccMngr: TSmallintField;
    QrControleSoMaiusculas: TWideStringField;
    QrControlePaperLef: TIntegerField;
    QrControlePaperTop: TIntegerField;
    QrControlePaperHei: TIntegerField;
    QrControlePaperWid: TIntegerField;
    QrControlePaperFcl: TIntegerField;
    QrControleMoeda: TWideStringField;
    QrControleCodigo: TIntegerField;
    QrControleVersao: TIntegerField;
    QrControleDono: TIntegerField;
    QrControleUFPadrao: TIntegerField;
    QrControleTravaCidade: TSmallintField;
    QrControleCNPJ: TWideStringField;
    QrControleCidade: TWideStringField;
    QrControleVerBcoTabs: TIntegerField;
    QrOpcoesApp: TmySQLQuery;
    QrOpcoesAppLoadCSVOthIP: TWideStringField;
    QrOpcoesAppLoadCSVOthDir: TWideStringField;
    QrOpcoesAppOVpLayEsq: TIntegerField;
    QrOPcoesGrl: TMySQLQuery;
    QrOPcoesGrlERPNameByCli: TWideStringField;
    DqAux: TMySQLDirectQuery;
    QrOpcoesApU: TMySQLQuery;
    QrOpcoesApUCodigo: TIntegerField;
    QrOpcoesApUHabFaccao: TSmallintField;
    QrOpcoesApUHabTextil: TSmallintField;
    QrOpcoesApUNO_HabFaccao: TWideStringField;
    QrOpcoesApUNO_HabTextil: TWideStringField;
    QrOpcoesApULogin: TWideStringField;
    QrOpcoesApUHabFacConfeccao: TSmallintField;
    QrOpcoesApUHabTexTecelagem: TSmallintField;
    QrOpcoesApUHabTexTinturaria: TSmallintField;
    QrSumBtl: TMySQLQuery;
    QrSumBtlQtReal: TFloatField;
    QrAu2: TMySQLQuery;
    QrControleCasasProd: TIntegerField;
    QrLocY: TMySQLQuery;
    QrLocYRecord: TIntegerField;
    QrAux3: TMySQLQuery;
    QrControleMoraDD: TFloatField;
    QrControleMulta: TFloatField;
    QrControleMensalSempre: TSmallintField;
    QrControleEntraSemValor: TSmallintField;
    QrControleCorRecibo: TIntegerField;
    QrControleIdleMinutos: TIntegerField;
    QrControleMyPagCar: TSmallintField;
    QrControleMyPgParc: TSmallintField;
    QrControleMyPgQtdP: TSmallintField;
    QrControleMyPgPeri: TSmallintField;
    QrControleMyPgDias: TSmallintField;
    QrControleVendaCartPg: TIntegerField;
    QrControleVendaParcPg: TIntegerField;
    QrControleVendaPeriPg: TIntegerField;
    QrControleVendaDiasPg: TIntegerField;
    QrControleCNABCtaTar: TIntegerField;
    QrControleCNABCtaJur: TIntegerField;
    QrControleCNABCtaMul: TIntegerField;
    QrControleLogoNF: TWideStringField;
    QrControleMeuLogoPath: TWideStringField;
    QrControleLogoBig1: TWideStringField;
    QrControleMoedaBr: TIntegerField;
    QrControleMyPerJuros: TFloatField;
    QrControleMyPerMulta: TFloatField;
    QrOpcoesAppPlaConCtaVen: TIntegerField;
    procedure DataModuleCreate(Sender: TObject);
    procedure QrMasterAfterOpen(DataSet: TDataSet);
    procedure QrMasterCalcFields(DataSet: TDataSet);
    procedure QrControleAfterOpen(DataSet: TDataSet);
    procedure MyDBBeforeConnect(Sender: TObject);
    procedure ZZDBBeforeConnect(Sender: TObject);
    procedure MyLocDatabaseBeforeConnect(Sender: TObject);
    procedure MyDBnBeforeConnect(Sender: TObject);
    procedure MyDBAfterConnect(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }

    FFmtPrc, FProdDelete: Integer;
    FStrFmtPrc, FStrFmtCus: String;

    function  AtualizaQtBtl(Codigo: Integer): Double;
    procedure CriaItensChmOcoDon(LaAviso1, LaAviso2: TLabel; Codigo: Integer);
    procedure ExcluiItensOrfaosChmOcoDon(LaAviso1, LaAviso2: TLabel; Codigo: Integer);
    function  DModG_ObtemAgora(UTC: Boolean = False): TDateTime;
    procedure ReopenControle();
    procedure ReopenParamsEspecificos(Empresa: Integer);
    procedure ReopenOpcoesApp();
    procedure ReopenOpcoesApU(Usuario: Integer);
    function  TabelasQueNaoQueroCriar(): String;
    //

  end;

var
  Dmod: TDmod;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

uses
  DmkDAC_PF, UnGOTOy, Principal, Servidor, ModuleGeral, VerifiDB, UnLic_Dmk,
  {[***Desmarcar***]
  VerifiDBi,
  [***NomeApp***]_Dmk,
  }
  ModuleFin,
  MyListas, UnDmkWeb, UnDmkProcFunc, UnYRO_ProjGroupVars,
  Descanso, UnMyObjects, UMySQLDB, UMySQLModule, MyDBCheck;

{ TDmod }

function TDmod.AtualizaQtBtl(Codigo: Integer): Double;
var
  QtBtl: Double;
begin
  QtBtl := 0;
  UnDmkDAC_PF.AbreMySQLQuery0(QrSumBtl, Dmod.MyDB, [
  'SELECT SUM(QtReal) QtReal  ',
  'FROM ovgitxgerbtl ',
  'WHERE Codigo=' + Geral.FF0(Codigo),
  EmptyStr]);
  //
  QtBtl := QrSumBtlQtReal.Value;
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'ovgitxgercab', False, [
  'QtBtl'], [
  'Codigo'], [
  QtBtl], [
  Codigo], True) then
    Result := QtBtl;
end;

procedure TDmod.CriaItensChmOcoDon(LaAviso1, LaAviso2: TLabel; Codigo: Integer);
  procedure InsereItemAtual();
  var
    DoneDtHr, CloseDtHr, Observacao: String;
    //Codigo,
    Controle, ChmOcoEtp, ChmOcoWho, CloseUser: Integer;
    SQLType: TSQLType;
  begin
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Inserindo item ' +
    Geral.FF0(QrAu2.RecNo) + ' de ' + Geral.FF0(QrAu2.RecordCount));
    //
    SQLType        := stIns;
    //Codigo         := ;
    Controle       := 0;
    ChmOcoEtp      := QrAu2.FieldByName('ChmOcoEtp').AsInteger;
    ChmOcoWho      := QrAu2.FieldByName('ChmOcoWho').AsInteger;
    DoneDtHr       := '0000-00-00 00:00:00';
    CloseUser      := 0;
    CloseDtHr      := '0000-00-00 00:00:00';
    Observacao     := '';
    //
    Controle := UMyMod.BPGS1I32('chmocodon', 'Controle', '', '', tsPos, SQLType, Controle);
    UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'chmocodon', False, [
    'Codigo', 'ChmOcoEtp', 'ChmOcoWho',
    'DoneDtHr', 'CloseUser', 'CloseDtHr',
    'Observacao'], [
    'Controle'], [
    Codigo, ChmOcoEtp, ChmOcoWho,
    DoneDtHr, CloseUser, CloseDtHr,
    Observacao], [
    Controle], True);
  end;
var
  TextoAnterior: String;
begin
  if LaAviso1 <> nil then
    TextoAnterior := LaAviso1.Caption
  else
  if LaAviso2 <> nil then
    TextoAnterior := LaAviso2.Caption
  else
    TextoAnterior := '';
  try
    MyObjects.Informa2(LaAviso1, LaAviso2, True,
      'Preparando gera��o de itens de ocorr�ncias');
    UnDmkDAC_PF.AbreMySQLQuery0(QrAu2, Dmod.MyDB, [
    'DROP TABLE IF EXISTS _Temp_chmocoetp_; ',
    'CREATE TABLE _Temp_chmocoetp_ ',
    'SELECT Controle ',
    'FROM chmocoetp',
    'WHERE Codigo=' + Geral.FF0(Codigo),
    ';',
    'DROP TABLE IF EXISTS _Temp_chmocowho_; ',
    'CREATE TABLE _Temp_chmocowho_ ',
    'SELECT Controle ',
    'FROM chmocowho',
    'WHERE Codigo=' + Geral.FF0(Codigo),
    ';',
    'DROP TABLE IF EXISTS _Temp_chmocodon_; ',
    'CREATE TABLE _Temp_chmocodon_ ',
    'SELECT ',
    'etp.Controle chmocoetp, ',
    'who.Controle chmocowho',
    'FROM _Temp_chmocoetp_ etp',
    'INNER JOIN _Temp_chmocowho_ who',
    ';',
    'SELECT don.Controle, tmp.chmocoetp, tmp.chmocowho ',
    'FROM _Temp_chmocodon_ tmp',
    'LEFT JOIN chmocodon don ON ',
    '  don.chmocoetp=tmp.chmocoetp',
    '  AND',
    '  don.chmocowho=tmp.chmocowho',
    'WHERE don.Controle IS NULL;',
    'DROP TABLE IF EXISTS _Temp_chmocoetp_;',
    'DROP TABLE IF EXISTS _Temp_chmocowho_; ',
    'DROP TABLE IF EXISTS _Temp_chmocodon_; ',
    '']);
    QrAu2.First;
    while not QrAu2.Eof do
    begin
      InsereItemAtual();
      //
      QrAu2.Next;
    end;
  finally
    MyObjects.Informa2(LaAviso1, LaAviso2, False, '');
  end;
end;

procedure TDmod.DataModuleCreate(Sender: TObject);
var
  Versao, VerZero, Resp: Integer;
  BD, OthSetCon: String;
  Verifica: Boolean;
var
  Porta, Server: Integer;
  IPServ, DataBase, OthUser: String;
begin
  if MyDB.Connected then
    Geral.MB_Aviso('MyDB est� connectado antes da configura��o!');
  if MyLocDataBase.Connected then
    Geral.MB_Aviso('MyLocDataBase est� connectado antes da configura��o!');
  if MyDBn.Connected then
    Geral.MB_Aviso('MyDBn est� connectado antes da configura��o!');

  MyDB.LoginPrompt := False;

  ZZDB.LoginPrompt := False;
  MyLocDatabase.LoginPrompt := False;


  // 2019-09-27
  OthSetCon := Geral.ReadAppKeyCU('OthSetCon', Application.Title, ktString, '');
  if OthSetCon = EmptyStr then
    VAR_BDSENHA := CO_USERSPNOW
  else
    VAR_BDSENHA := dmkPF.Criptografia(OthSetCon, CO_LLM_SecureStr);
  //
  VAR_PORTA := Geral.ReadAppKeyCU('Porta', 'Dermatek', ktInteger, 3306);
  Server := Geral.ReadAppKeyCU('Server', Application.Title, ktInteger, 0);
  //
  case Server of
    1:   VAR_SERVIDOR := 0;
    2:   VAR_SERVIDOR := 1;
    3:   VAR_SERVIDOR := 2;
    else VAR_SERVIDOR := -1;
  end;
  VAR_IP       := Geral.ReadAppKeyCU('IPServer', Application.Title, ktString, CO_VAZIO);
  DataBase     := Geral.ReadAppKeyCU('Database', Application.Title, ktString, 'mysql');
  VAR_SQLUSER  := Geral.ReadAppKeyCU('OthUser', Application.Title, ktString, 'root');
  if VAR_BDSENHA = '' then
    VAR_BDSENHA := CO_USERSPNOW;
  if VAR_SQLUSER = '' then
    VAR_SQLUSER := 'root';
  // FIM 2019-09-27


  {[***Desmarcar***]
  if not GOTOy.OMySQLEstaInstalado(Fm[NomeApp]_Dmk.LaAviso1,
    Fm[NomeApp]_Dmk.LaAviso2, Fm[NomeApp]_Dmk.ProgressBar1) then
  begin
    MyObjects.Informa2(Fm[***NomeApp***]_Dmk.LaAviso1, Fm[***NomeApp***]_Dmk.LaAviso2, False,
      'N�o foi poss�vel a conex�o ao IP: [' + VAR_IP + ']');
    Fm[***NomeApp***]_Dmk.BtEntra.Visible := False;
    Exit;
  end;
  }
  /////////////////////////////////////////
  if GOTOy.SenhaDesconhecida then
  begin
    raise EAbort.Create('Senha desconhecida');
    Exit;
  end;
  ///////////////////////////////////////////////////////////
{
  ZZDB.Host         := VAR_SQLHOST;
  ZZDB.UserName     := VAR_SQLUSER;
  ZZDB.UserPassword := VAR_BDSENHA;
  ZZDB.Port         := VAR_PORTA;
  try
    ZZDB.Connected := True;
  except
    ZZDB.UserPassword := CO_SPDESCONHECIDA;
    try
      ZZDB.Connected := True;
      QrUpd.Database := ZZDB;
      QrUpd.SQL.Clear;
      QrUpd.SQL.Add('UPDATE User SET Password=PASSWORD("'+VAR_BDSENHA+'")');
      QrUpd.SQL.Add('');
      QrUpd.SQL.Add('WHERE User="root"');
      QrUpd.ExecSQL;
      ///////////
      QrUpd.SQL.Clear;
      QrUpd.SQL.Add('FLUSH PRIVILEGES');
      QrUpd.ExecSQL;
      ///////////
      FmPrincipal.Close;
      Application.Terminate;
      Exit;
    except
      if VAR_SERVIDOR = 2 then Geral.MB_Aviso('Banco de dados teste n�o se conecta!');
    end;
  end;
}
  /////////////////////////////////////////
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  //
  VAR_SQLx := TStringList.Create;
  VAR_SQL1 := TStringList.Create;
  VAR_SQL2 := TStringList.Create;
  VAR_SQLa := TStringList.Create;
  //
  MAR_SQLx := TStringList.Create;
  MAR_SQL1 := TStringList.Create;
  MAR_SQL2 := TStringList.Create;
  MAR_SQLa := TStringList.Create;
  try
    if VAR_SERVIDOR = 1 then
    begin
      if not GetSystemMetrics(SM_NETWORK) and $01 = $01 then
      begin
      Geral.MB_Aviso('M�quina cliente sem rede.');
        Application.Terminate;
      end;
    end;
  except
    Application.Terminate;
    Exit;
  end;
  if VAR_APPTERMINATE then
  begin
    Application.Terminate;
    Exit;
  end;
  Geral.DefineFormatacoes;
  //
  VAR_SERVIDOR := Geral.ReadAppKey('Server', Application.Title, ktInteger, 0,
    HKEY_LOCAL_MACHINE);
  if not (VAR_SERVIDOR in [1,2]) then
  begin
    Application.CreateForm(TFmServidor, FmServidor);
    FmServidor.ShowModal;
    FmServidor.Destroy;
  end;
  if (VAR_SERVIDOR = 2) and (Trim(VAR_IP) = EMptyStr) then
    VAR_IP := '127.0.0.1';

{
  if VAR_IP = CO_VAZIO then
  begin
    VAR_IP := '127.0.0.1';
    if VAR_SERVIDOR = 1 then
    begin
      if InputQuery('IP do Servidor', 'Defina o IP do Servidor', VAR_IP) then
      begin
        Geral.WriteAppKey('IPServer', Application.Title, VAR_IP, ktString,
          HKEY_LOCAL_MACHINE);
      end else
      begin
        Application.Terminate;
        Exit;
      end;
    end;
  end;
}
  if (Trim(Database) = EmptyStr)
  or (Trim(VAR_IP) = EmptyStr)
  or (VAR_PORTA = 0)
  or (Trim(VAR_SQLUSER) = EmptyStr)
  or (Trim(VAR_BDSENHA) = EmptyStr) then
    USQLDB.ConfiguraDB(True);

{  UnDmkDAC_PF.ConectaMyDB_DAC(MyDB, 'mysql',  VAR_IP, VAR_PORTA,  VAR_SQLUSER,
    VAR_BDSENHA, (*Desconecta*)True, (*Configura*)True, (*Conecta*)False);
}
  UnDmkDAC_PF.ConectaMyDB_DAC(MyDB, Database,  VAR_IP, VAR_PORTA,  VAR_SQLUSER,
    VAR_BDSENHA, (*Desconecta*)True, (*Configura*)True, (*Conecta*)False);
  //

  try
    QrAux.Close;
    QrAux.SQL.Clear;
    QrAux.SQL.Add('SHOW DATABASES');
    UnDmkDAC_PF.AbreQuery(QrAux, MyDB);
    BD := CO_VAZIO;
    while not QrAux.Eof do
    begin
      if Uppercase(QrAux.FieldByName('Database').AsString)=Uppercase(TMeuDB) then
        BD := TMeuDB;
      QrAux.Next;
    end;
    MyDB.Close;
    MyDB.DataBaseName := BD;
    if MyDB.DataBaseName = CO_VAZIO then
    begin
      Resp := Geral.MB_Pergunta('O banco de dados ' + TMeuDB +
        ' n�o existe e deve ser criado. Confirma a cria��o?');
      if Resp = ID_YES then
      begin
        QrAux.Close;
        QrAux.SQL.Clear;
        QrAux.SQL.Add('CREATE DATABASE '+TMeuDB);
        QrAux.ExecSQL;
        MyDB.Disconnect;
        MyDB.DatabaseName := TMeuDB;
        MyDB.Connect;
        UnDmkDAC_PF.AbreMySQLQuery0(QrUpd, MyDB, [
        'SHOW TABLES FROM ' + Lowercase(TMeuDB) + ' LIKE "controle"',
        '']);
        if QrUpd.Fields[0].AsString = EmptyStr then
        begin
          DBCheck.CriaTabela(MyDB, 'controle', 'controle', nil, actCreate,
          (*Tem_Del*)False, nil, nil, nil, nil, (*Tem_Sync*)False);
        end;
        DModG.VerificaDB();
        //
      end else if Resp = ID_CANCEL then
      begin
        Geral.MB_Aviso('O aplicativo ser� encerrado!');
        Application.Terminate;
        Exit;
      end;
    end;
  except
    on E: Exception do
    begin
      Geral.MB_Erro(E.Message);
      USQLDB.ConfiguraDB(True);
    end;
  end;
  //
  GOTOy.DefinePathMySQL;
  //
{$IfNDef SemDBLocal}
  MyLocDatabase.UserName     := VAR_SQLUSER;
  MyLocDatabase.UserPassword := VAR_BDSENHA;
  MyLocDatabase.Port         := VAR_PORTA;
  MyLocDatabase.DatabaseName := 'mysql';// existe com certeza se estiver instalado
  //
  QrAuxL.Close;
  QrAuxL.SQL.Clear;
  QrAuxL.SQL.Add('SHOW DATABASES');
  UnDmkDAC_PF.AbreQuery(QrAux, MyLocDatabase);
  BD := CO_VAZIO;
  while not QrAuxL.Eof do
  begin
    if Uppercase(QrAuxL.FieldByName('Database').AsString)=Uppercase(TLocDB) then
      BD := TLocDB;
    QrAuxL.Next;
  end;
  MyLocDatabase.Close;
  MyLocDatabase.DatabaseName := BD;
  if MyLocDatabase.DataBaseName = CO_VAZIO then
  begin
    Resp := Geral.MB_Pergunta('O banco de dados local '+TLocDB+
      ' n�o existe e deve ser criado. Confirma a cria��o?');
    if Resp = ID_YES then
    begin
      QrAuxL.Close;
      QrAuxL.SQL.Clear;
      QrAuxL.SQL.Add('CREATE DATABASE '+TLocDB);
      QrAuxL.ExecSQL;
      MyLocDatabase.DatabaseName := TLocDB;
    end else if Resp = ID_CANCEL then
    begin
      Geral.MB_Aviso('O aplicativo ser� encerrado!');
      Application.Terminate;
      Exit;
    end;
  end;
{$EndIf}
  //
  VerZero := Geral.ReadAppKey('Versao', Application.Title, ktInteger,
    0, HKEY_LOCAL_MACHINE);
  Versao := Geral.ReadAppKey('Versao', Application.Title, ktInteger,
    CO_VERSAO, HKEY_LOCAL_MACHINE);
  Verifica := False;
  if Versao < CO_Versao then
    Verifica := True;
  if VerZero = 0 then
  begin
    Resp := Geral.MB_Pergunta('N�o h� informa��o de vers�o no registro, '+
    'se esta n�o for a primeira execu��o do aplicativo selecione cancelar e '+
    'informe o respons�vel!. Caso confirme, ser� verificado a composi��o do '+
    'banco de dados. Confirma a Verifica��o?');
    if Resp = ID_YES then
      Verifica := True
    else if Resp = ID_CANCEL then
    begin
      Application.Terminate;
      Exit;
    end;
  end;
  Mylist.ConfiguracoesIniciais(1, Application.Name);
  //
  if DModG = nil then
  begin
    try
      Application.CreateForm(TDmodG, DmodG);
    except
      Geral.MB_Erro('Imposs�vel criar Modulo de dados Geral');
      Application.Terminate;
      Exit;
    end;
  end;
  try
    Application.CreateForm(TDModFin, DModFin);
  except
    Geral.MB_Erro('Imposs�vel criar Modulo Financeiro');
    Application.Terminate;
    Exit;
  end;
  if Verifica then
    DModG.VerificaDB();
  //
  try
    UnDmkDAC_PF.AbreQuery(QrMaster, Dmod.MyDB);
  {[***Desmarcar***]
    ReopenOpcoes[***SiglaApp***]();
}
    VAR_EMPRESANOME := QrMasterEm.Value;
  except
    try
      DModG.VerificaDB();
      if ZZTerminate then
        Exit;
    except;
      MyDB.DatabaseName := CO_VAZIO;
      raise;
    end;
  end;
  {[***Desmarcar***]
  //
  // Conex�o na base de dados na url
  // S� pode ser ap�s abrir a tabela controle!
  // ver se est� ativado s� ent�o fazer !
  try
    if DmkWeb.ConexaoRemota(MyDBn, DmodG.QrOpcoesGerl, 1) then
    begin
      MyDBn.Connect;
      if MyDBn.Connected and Verifica then
      begin
        if not VAR_VERIFI_DB_CANCEL then
        begin
          Application.CreateForm(TFmVerifiDBi, FmVerifiDBi);
          with FmVerifiDbi do
          begin
            BtSair.Enabled := False;
            FVerifi := True;
            ShowModal;
            FVerifi := False;
            Destroy;
          end;
        end;
      end;
    end;
  except
    Geral.MB_Aviso('N�o foi poss�vel se conectar � base de dados remota!');
  end;
}
  //
////////////////////////////////////////////////////////////////////////////////
///  N�o tem sentido aqui pois depende do site dermatek!
///  Ver o que fazer!
  Lic_Dmk.LiberaUso5;
  //Geral.MB_Aviso('"Uso5" desabilitado!');
////////////////////////////////////////////////////////////////////////////////
  VAR_DB := MyDB.DatabaseName;
end;

function TDmod.DModG_ObtemAgora(UTC: Boolean): TDateTime;
begin
  Result := DModG.ObtemAgora(UTC);
end;

procedure TDmod.ExcluiItensOrfaosChmOcoDon(LaAviso1, LaAviso2: TLabel; Codigo: Integer);
  procedure ExcluiItemAtual();
  begin
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Excluindo item ' +
    Geral.FF0(QrAu2.RecNo) + ' de ' + Geral.FF0(QrAu2.RecordCount));
    //
    Dmod.MyDB.Execute('DELETE FROM chmocodon WHERE Controle=' +
      Geral.FF0(QrAu2.FieldByName('Controle').AsInteger));
  end;
var
  TextoAnterior: String;
begin
  if LaAviso1 <> nil then
    TextoAnterior := LaAviso1.Caption
  else
  if LaAviso2 <> nil then
    TextoAnterior := LaAviso2.Caption
  else
    TextoAnterior := '';
  try
    MyObjects.Informa2(LaAviso1, LaAviso2, True,
      'Preparando gera��o de itens de ocorr�ncias');
    UnDmkDAC_PF.AbreMySQLQuery0(QrAu2, Dmod.MyDB, [
    'SELECT don.Codigo, don.Controle, ',
    'etp.Controle chmocoetp,   ',
    'who.Controle chmocowho   ',
    'FROM chmocodon don ',
    'LEFT JOIN chmocoetp etp ON etp.Controle=don.chmocoetp  ',
    'LEFT JOIN chmocowho who ON who.Controle=don.chmocowho ',
    'WHERE don.Codigo=' + Geral.FF0(Codigo),
    'AND ( ',
    '    (etp.Controle IS NULL) ',
    '  OR ',
    '    (who.Controle IS NULL) ',
    ') ',
   '']);
    QrAu2.First;
    while not QrAu2.Eof do
    begin
      ExcluiItemAtual();
      //
      QrAu2.Next;
    end;
  finally
    MyObjects.Informa2(LaAviso1, LaAviso2, False, '');
  end;
end;

procedure TDmod.MyDBAfterConnect(Sender: TObject);
begin
  FmPrincipal.TimerPingServer.Enabled := False;
  FmPrincipal.TimerPingServer.Interval := 30000;
  FmPrincipal.TimerPingServer.Enabled := True;
end;

procedure TDmod.MyDBBeforeConnect(Sender: TObject);
begin
  USQLDB.MsgBeforeConectaDB(Sender);
end;

procedure TDmod.MyDBnBeforeConnect(Sender: TObject);
begin
  USQLDB.MsgBeforeConectaDB(Sender);
end;

procedure TDmod.MyLocDatabaseBeforeConnect(Sender: TObject);
begin
  USQLDB.MsgBeforeConectaDB(Sender);
end;

procedure TDmod.QrControleAfterOpen(DataSet: TDataSet);
begin
  FFmtPrc    := Dmod.QrControleCasasProd.Value;
  FStrFmtPrc := dmkPF.StrFmt_Double(Dmod.QrControleCasasProd.Value);
  FStrFmtCus := dmkPF.StrFmt_Double(4);
  //
  ReopenOpcoesApp();
end;

procedure TDmod.QrMasterAfterOpen(DataSet: TDataSet);
begin
{
CNPJ n�o definido ou Empresa n�o definida.
Algumas ferramentas do aplicativo poder�o ficar inacess�veis.
C�digo = ?
}
  if Trim(QrMasterCNPJ.Value) = CO_VAZIO then
  begin
    Geral.MB_Aviso('CNPJ n�o definido ou Empresa n�o definida. ' + sLineBreak +
    'Algumas ferramentas do aplicativo poder�o ficar inacess�veis.');
    FmPrincipal.Caption := Application.Title + '  ::  ' + QrMasterEm.Value +
    ' # ' + QrMasterCNPJ.Value;
    //
    Geral.MB_Teste(QrMaster.SQL.Text);
  end;
  //
  ReopenControle();
  DModG.ReopenOpcoesGerl;
  //
  if QrControleSoMaiusculas.Value = 'V' then
    VAR_SOMAIUSCULAS := True;
{$IfNDef SemEntidade}
  VAR_UFPADRAO        := QrControleUFPadrao.Value;
  VAR_CIDADEPADRAO    := QrControleCidade.Value;
  VAR_TRAVACIDADE     := QrControleTravaCidade.Value;
{$EndIf}
{$IfNDef SemCotacoes}
  VAR_MOEDA           := QrControleMoeda.Value;
{$EndIf}
{$IfNDef NO_FINANCEIRO}
(*&�%$#@!"
  VAR_CARTVEN         := QrControleCartVen.Value;
  VAR_CARTCOM         := QrControleCartCom.Value;
  VAR_CARTRES         := QrControleCartReS.Value;
  VAR_CARTDES         := QrControleCartDeS.Value;
  VAR_CARTREG         := QrControleCartReG.Value;
  VAR_CARTDEG         := QrControleCartDeG.Value;
  VAR_CARTCOE         := QrControleCartCoE.Value;
  VAR_CARTCOC         := QrControleCartCoC.Value;
  VAR_CARTEMD         := QrControleCartEmD.Value;
  VAR_CARTEMA         := QrControleCartEmA.Value;
*)
{$EndIf}
  VAR_PAPERSET        := True;
  VAR_PAPERTOP        := QrControlePaperTop.Value;
  VAR_PAPERLEF        := QrControlePaperLef.Value;
  VAR_PAPERWID        := QrControlePaperWid.Value;
  VAR_PAPERHEI        := QrControlePaperHei.Value;
  VAR_PAPERFCL        := QrControlePaperFcl.Value;
  //
  if DModG = nil then
  begin
    try
      Application.CreateForm(TDModG, DModG);
    except
      Geral.MB_Aviso('N�o foi poss�vel criar o Module Geral');
      Application.Terminate;
      Exit;
    end;
  end;
  MyObjects.FormTDICria(TFmDescanso, FmPrincipal.PageControl1,
  FmPrincipal.AdvToolBarPagerNovo, False, False, afmoSemVerificar);
end;

procedure TDmod.QrMasterCalcFields(DataSet: TDataSet);
begin
  QrMasterTE1_TXT.Value  := Geral.FormataTelefone_TT(QrMasterETe1.Value);
  QrMasterCEP_TXT.Value  := Geral.FormataCEP_NT(QrMasterECEP.Value);
  QrMasterCNPJ_TXT.Value := Geral.FormataCNPJ_TT(QrMasterCNPJ.Value);
end;

procedure TDmod.ReopenControle;
begin
  UnDmkDAC_PF.AbreQuery(QrControle, Dmod.MyDB);
end;


procedure TDmod.ReopenOpcoesApp();
begin
  UnDmkDAC_PF.AbreQuery(QrOpcoesApp, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrOpcoesGrl, Dmod.MyDB);
end;

procedure TDmod.ReopenOpcoesApU(Usuario: Integer);
begin
(*&�%$#@!"
  UnDmkDAC_PF.AbreMySQLQuery0(QrOpcoesApU, Dmod.MyDB, [
  'SELECT apu.Codigo, apu.HabFaccao, apu.HabTextil,  ',
  'apu.HabFacConfeccao,  ',
  'apu.HabTexTecelagem, apu.HabTexTinturaria,  ',
  'IF(apu.HabFaccao=1, "SIM", "N�O") NO_HabFaccao,  ',
  'IF(apu.HabFacConfeccao=1, "SIM", "N�O") NO_HabFacConfecaocao,  ',
  'IF(apu.HabTextil=1, "SIM", "N�O") NO_HabTextil,  ',
  'IF(apu.HabTexTecelagem=1, "SIM", "N�O") NO_HabTexTecelagem,  ',
  'IF(apu.HabTexTinturaria=1, "SIM", "N�O") NO_HabTexTinturaria,  ',
  'pwd.Login  ',
  'FROM opcoesapu apu  ',
  'LEFT JOIN senhas pwd ON pwd.Numero=apu.Codigo  ',
  'WHERE apu.Codigo=' + Geral.FF0(Usuario),
  '']);
*)
end;

procedure TDmod.ReopenParamsEspecificos(Empresa: Integer);
begin
  // Compatibilidade
end;

function TDmod.TabelasQueNaoQueroCriar: String;
begin
  // Compatibilidade
end;

procedure TDmod.ZZDBBeforeConnect(Sender: TObject);
begin
  USQLDB.MsgBeforeConectaDB(Sender);
end;

end.
