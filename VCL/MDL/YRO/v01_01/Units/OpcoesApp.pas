unit OpcoesApp;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, ComCtrls, ExtDlgs, DBCtrls, Db, Variants,
  mySQLDbTables, dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkRadioGroup,
  dmkGeral, UnDmkProcFunc, dmkImage, UnDmkEnums, dmkCheckBox,
  UnGrl_Consts, dmkCheckGroup, Vcl.Menus, dmkValUsu, Vcl.Grids, Vcl.DBGrids,
  dmkDBGridZTO, dmkEditEdTxt, dmkEditEdIdx;

type
  TFmOpcoesApp = class(TForm)
    Panel1: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel8: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel9: TPanel;
    PnSaiDesis: TPanel;
    BtOK: TBitBtn;
    BtSaida: TBitBtn;
    PCGeral: TPageControl;
    TabSheet2: TTabSheet;
    Panel6: TPanel;
    QrOVpLayEsq: TMySQLQuery;
    QrOVpLayEsqCodigo: TIntegerField;
    QrOVpLayEsqNome: TWideStringField;
    DsOVpLayEsq: TDataSource;
    TabSheet1: TTabSheet;
    Panel10: TPanel;
    Panel11: TPanel;
    Label9: TLabel;
    EdERPNameByCli: TdmkEdit;
    QrEntiTipCto: TMySQLQuery;
    DsEntiTipCto: TDataSource;
    Label10: TLabel;
    SpeedButton1: TSpeedButton;
    dmkValUsu1: TdmkValUsu;
    QrEntiTipCtoCodUsu: TIntegerField;
    QrEntiTipCtoCodigo: TIntegerField;
    QrEntiTipCtoNome: TWideStringField;
    TabSheet3: TTabSheet;
    QrOVpDirXtr: TMySQLQuery;
    DsOVpDirXtr: TDataSource;
    QrOVpDirXtrCodigo: TIntegerField;
    QrOVpDirXtrNome: TWideStringField;
    Panel2: TPanel;
    Label1: TLabel;
    EdLoadCSVOthIP: TEdit;
    EdLoadCSVOthDir: TEdit;
    Label3: TLabel;
    EdOVpLayEsq: TdmkEditCB;
    CBOVpLayEsq: TdmkDBLookupComboBox;
    SbOVpLayEsq: TSpeedButton;
    EdLoadCSVIntrv: TdmkEdit;
    SpeedButton2: TSpeedButton;
    Label11: TLabel;
    TabSheet6: TTabSheet;
    Panel16: TPanel;
    BtExclui: TBitBtn;
    BtAltera: TBitBtn;
    BtInclui: TBitBtn;
    DBGOpcoesApU: TdmkDBGridZTO;
    QrOpcoesApU: TMySQLQuery;
    DsOpcoesApU: TDataSource;
    QrOpcoesApUCodigo: TIntegerField;
    QrOpcoesApUHabFaccao: TSmallintField;
    QrOpcoesApUHabTextil: TSmallintField;
    QrOpcoesApUNO_HabFaccao: TWideStringField;
    QrOpcoesApUNO_HabTextil: TWideStringField;
    QrOpcoesApULogin: TWideStringField;
    QrOpcoesApUHabFacConfeccao: TSmallintField;
    QrOpcoesApUHabTexTecelagem: TSmallintField;
    QrOpcoesApUHabTexTinturaria: TSmallintField;
    QrOpcoesApUNO_HabFacConfecaocao: TWideStringField;
    QrOpcoesApUNO_HabTexTecelagem: TWideStringField;
    QrOpcoesApUNO_HabTexTinturaria: TWideStringField;
    Label2: TLabel;
    QrContas: TMySQLQuery;
    DsContas: TDataSource;
    QrContasCodigo: TIntegerField;
    QrContasNome: TWideStringField;
    EdPlaConCtaVen_TXT: TdmkEditEdTxt;
    EdPlaConCtaVen: TdmkEditEdIdx;
    Label4: TLabel;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SbOVpLayEsqClick(Sender: TObject);
    procedure BtIncluiClick(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtExcluiClick(Sender: TObject);
    procedure QrOpcoesApUAfterOpen(DataSet: TDataSet);
    procedure QrOpcoesApUBeforeClose(DataSet: TDataSet);
  private
    { Private declarations }
    //
    procedure ReopenOpcoesApU(Codigo: Integer);
  public
    { Public declarations }
    procedure ReopenOVpDirXtr(Codigo: Integer);
    procedure MostraFormOpcoesApUCad(SQLType: TSQLType; Codigo: Integer; Login:
              String);
  end;

  var
  FmOpcoesApp: TFmOpcoesApp;

implementation

uses UnMyObjects, Module, ModuleGeral, Principal, UMySQLModule,
  //OpcoesApUCad;
  UnInternalConsts, MyDBCheck, DmkDAC_PF, UnEntities, UnYRO_Jan;

{$R *.DFM}

procedure TFmOpcoesApp.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmOpcoesApp.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmOpcoesApp.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmOpcoesApp.MostraFormOpcoesApUCad(SQLType: TSQLType; Codigo: Integer;
  Login: String);
begin
(*&�%$#@!"
  if DBCheck.CriaFm(TFmOpcoesApUCad, FmOpcoesApUCad, afmoSoBoss) then
  begin
    FmOpcoesApUCad.ImgTipo.SQLType       := SQLType;
    FmOpcoesApUCad.EdCodigo.ValueVariant := Codigo;
    FmOpcoesApUCad.EdLogin.ValueVariant  := Login;
    if SQLType = stUpd then
    begin
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
      FmOpcoesApUCad.CkHabFaccao.Checked := Geral.IntToBool(QrOpcoesApUHabFaccao.Value);
        FmOpcoesApUCad.CkHabFacConfeccao.Checked := Geral.IntToBool(QrOpcoesApUHabFacConfeccao.Value);
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
      FmOpcoesApUCad.CkHabTextil.Checked := Geral.IntToBool(QrOpcoesApUHabTextil.Value);
        FmOpcoesApUCad.CkHabTexTecelagem.Checked := Geral.IntToBool(QrOpcoesApUHabTexTecelagem.Value);
        FmOpcoesApUCad.CkHabTexTinturaria.Checked := Geral.IntToBool(QrOpcoesApUHabTexTinturaria.Value);
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
    end;
    FmOpcoesApUCad.ShowModal;
    FmOpcoesApUCad.Destroy;
    //
    ReopenOpcoesApU(Codigo);
  end;
*)
end;

procedure TFmOpcoesApp.QrOpcoesApUAfterOpen(DataSet: TDataSet);
begin
  BtAltera.Enabled := QrOpcoesApU.RecordCount > 0;
  BtExclui.Enabled := QrOpcoesApU.RecordCount > 0;
end;

procedure TFmOpcoesApp.QrOpcoesApUBeforeClose(DataSet: TDataSet);
begin
  BtAltera.Enabled := False;
  BtExclui.Enabled := False;
end;

procedure TFmOpcoesApp.ReopenOpcoesApU(Codigo: Integer);
begin
(*
  UnDmkDAC_PF.AbreMySQLQuery0(QrOpcoesApU, Dmod.MyDB, [
  'SELECT apu.Codigo, apu.HabFaccao, apu.HabTextil,  ',
  'apu.HabFacConfeccao,  ',
  'apu.HabTexTecelagem, apu.HabTexTinturaria,  ',
  'IF(apu.HabFaccao=1, "SIM", "N�O") NO_HabFaccao,  ',
  'IF(apu.HabFacConfeccao=1, "SIM", "N�O") NO_HabFacConfecaocao,  ',
  'IF(apu.HabTextil=1, "SIM", "N�O") NO_HabTextil,  ',
  'IF(apu.HabTexTecelagem=1, "SIM", "N�O") NO_HabTexTecelagem,  ',
  'IF(apu.HabTexTinturaria=1, "SIM", "N�O") NO_HabTexTinturaria,  ',
  'pwd.Login  ',
  'FROM opcoesapu apu  ',
  'LEFT JOIN senhas pwd ON pwd.Numero=apu.Codigo  ',
  '']);
  if Codigo <> 0 then
    QrOpcoesApU.Locate('Codigo', Codigo, []);
*)end;

procedure TFmOpcoesApp.ReopenOVpDirXtr(Codigo: Integer);
begin
(*
  UnDmkDAC_PF.AbreMySQLQuery0(QrOVpDirXtr, Dmod.MyDB, [
  'SELECT Codigo, Nome ',
  'FROM ovpdirxtr ',
  'ORDER BY Nome ',
  '']);
  //
  if Codigo <> 0 then
    QrOVpDirXtr.Locate('Codigo', Codigo, []);
*)
end;

procedure TFmOpcoesApp.BtAlteraClick(Sender: TObject);
begin
  MostraFormOpcoesApUCad(stUpd, QrOpcoesApUCodigo.Value, QrOpcoesApULogin.Value);
end;

procedure TFmOpcoesApp.BtExcluiClick(Sender: TObject);
begin
  if Geral.MB_Pergunta('Este login ser� removido apenas desta lista! ' +
  sLineBreak + 'Deseja continuar assim mesmo?') = ID_YES then
  begin
    UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB,[
    'DELETE FROM opcoesapu ',
    'WHERE Codigo=' + Geral.FF0(QrOpcoesApUCodigo.Value),
    '']);
  end;
  ReopenOpcoesApU(0);
end;

procedure TFmOpcoesApp.BtIncluiClick(Sender: TObject);
const
  Aviso  = '...';
  Titulo = 'Sele��o de Login';
  Prompt = 'Informe o usu�rio: [F7 para pesquisar]';
  Campo  = 'Descricao';
var
  Numero: Variant;
  Codigo: Integer;
  PesqSQL: String;
begin
  PesqSQL := Geral.ATS([
  'SELECT Numero Codigo, Login Descricao',
  'FROM senhas ',
  'WHERE Numero > 0 OR Numero=-2',
  'ORDER BY ' + Campo,
  '']);
  Numero :=
    DBCheck.EscolheCodigoUnico(Aviso, Titulo, Prompt, nil, nil, Campo, 0(*Codigo*), [
    PesqSQL], Dmod.MyDB, True);
  if Codigo <> Null then
  begin
    Codigo := Numero;
    //
    Dmod.ReopenOpcoesApU(Codigo);
    if Dmod.QrOpcoesApU.RecordCount = 0 then
      MostraFormOpcoesApUCad(stIns, Codigo, VAR_SELNOM)
    else
    begin
      Geral.MB_Aviso('Usu�rio j� cadastrado!');
      MostraFormOpcoesApUCad(stUpd, Codigo, VAR_SELNOM);
    end;
  end;
end;

procedure TFmOpcoesApp.BtOKClick(Sender: TObject);
var
  LoadCSVOthIP, LoadCSVOthDir: String;
  Codigo, OVpLayEsq, PlaConCtaVen: Integer;
(*
  LoadCSVEmpresas, LoadCSVTipOP, LoadCSVSitOP, LoadCSVTipLoclz,
  LoadCSVTipProdOP, LoadCSVEmpresa2, LoadCSVTipO2, LoadCSVSitO2,
  LoadCSVTipLocl2, LoadCSVTipProdO2, MailResInspResul,
  LoadCSVAtbNomLocal, LoadCSVAtbValr1, LoadCSVAtbValr2, MailResInspResu2,
  SecConfeccao, SecTecelagem, SecTinturaria*)
  ERPNameByCli: String;
(*
  LoadCSVCodNomLocal, CdScConfeccao, CdScTecelagem, CdScTinturaria: Integer;
  LoadCSVCodValr1, LoadCSVCodValr2: String;
*)
  SQLType: TSQLType;
begin
  SQLType             := stUpd;
  Codigo              := 1;
  LoadCSVOthIP        := EdLoadCSVOthIP.Text;
  LoadCSVOthDir       := EdLoadCSVOthDir.Text;
  OVpLayEsq           := EdOVpLayEsq.ValueVariant;
  PlaConCtaVen        := EdPlaConCtaVen.ValueVariant;
  //
  //Miscel�nea
  ERPNameByCli        := EdERPNameByCli.ValueVariant;
  //Fim Miscel�nea
  if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'opcoesapp', False, [
  'LoadCSVOthIP', 'LoadCSVOthDir', 'OVpLayEsq',
  'PlaConCtaVen'
  ], [
  'Codigo'], [
  LoadCSVOthIP, LoadCSVOthDir, OVpLayEsq,
  PlaConCtaVen
  ], [
  Codigo], True) then
  begin
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'ctrlgeral', False, [
    'ERPNameByCli'], ['Codigo'
    ], [
    ERPNameByCli], [
    1], False) then
    begin
      UnDmkDAC_PF.AbreQuery(Dmod.QrOpcoesApp, Dmod.MyDB);
      //
      UnDmkDAC_PF.AbreQuery(Dmod.QrOpcoesGrl, Dmod.MyDB);
      UnDmkDAC_PF.AbreQuery(DModG.QrCtrlGeral, Dmod.MyDB);
      Close;
    end;
  end;
end;

procedure TFmOpcoesApp.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  PCGeral.ActivePageIndex := 0;
  Dmod.ReopenOpcoesApp();
  //UnDmkDAC_PF.AbreQuery(QrOVpLayEsq, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrEntiTipCto, Dmod.MyDB);
  EdLoadCSVOthIP.Text                := Dmod.QrOpcoesAppLoadCSVOthIP.Value;
  EdLoadCSVOthDir.Text               := Dmod.QrOpcoesAppLoadCSVOthDir.Value;
  EdOVpLayEsq.ValueVariant           := Dmod.QrOpcoesAppOVpLayEsq.Value;
  CBOVpLayEsq.KeyValue               := Dmod.QrOpcoesAppOVpLayEsq.Value;
  EdPlaConCtaVen.ValueVariant        := Dmod.QrOpcoesAppPlaConCtaVen.Value;
  //
  //Miscel�nea
  EdERPNameByCli.ValueVariant        := Dmod.QrOpcoesGrlERPNameByCli.Value;
  //Fim Miscel�nea
  //
  //
  //
  ReopenOVpDirXtr(0);
  ReopenOpcoesApU(0);
end;

procedure TFmOpcoesApp.SbOVpLayEsqClick(Sender: TObject);
begin
(*&�%$#@!"
  YRO_Jan.MostraFormOVpLayEsq(EdOVpLayEsq.ValueVariant);
*)
end;

procedure TFmOpcoesApp.SpeedButton2Click(Sender: TObject);
var
  IniPath, SelPath: String;
begin
  IniPath := ExtractFilePath(EdLoadCSVOthDir.Text);
  if MyObjects.FileOpenDialog(Self, IniPath, '', 'Selecione o Diret�rio', '',
  [], SelPath) then
    EdLoadCSVOthDir.Text := ExtractFileDir(SelPath);
end;

end.
