unit UnProjGroup_Jan;

interface

uses
  Windows, Forms,SysUtils, Classes, Controls, ExtCtrls, StdCtrls, DB, Menus,
  Grids, DBGrids, Variants,
  UnDmkEnums, mySQLDbTables,
  UnInternalConsts;

type
  TUnProjGroup_Jan = class(TObject)
  private
    { Private declarations }
  public
    { Public declarations }
{
    function  MostraFormOVgIspPrfCfg(Automatico: Boolean; LaAviso1, LaAviso2:
              TLabel): Boolean;
    function  MostraFormOVgItxPrfCfg(Automatico: Boolean; LaAviso1, LaAviso2:
              TLabel): Boolean;
}
  end;

var
  ProjGroup_Jan: TUnProjGroup_Jan;

implementation

uses
  dmkGeral, MyDBCheck,
(*
  OVgIspPrfCfg,
  OVgItxPrfCfg,
*)
  Module;

{ TUnProjGroup_Jan }

{
function TUnProjGroup_Jan.MostraFormOVgIspPrfCfg(Automatico: Boolean;
  LaAviso1, LaAviso2: TLabel): Boolean;
var
  Mostra: Boolean;
begin
  Result := False;
  if DBCheck.CriaFm(TFmOVgIspPrfCfg, FmOVgIspPrfCfg, afmoNegarComAviso) then
  begin
    FmOVgIspPrfCfg.FAutomatico := Automatico;
    FmOVgIspPrfCfg.FLaAviso1   := LaAviso1;
    FmOVgIspPrfCfg.FLaAviso1   := LaAviso2;
    if Automatico then
    begin
      if FmOVgIspPrfCfg.TemNovosItensAconfigurar() then
      begin
        Result := True;
        FmOVgIspPrfCfg.ConfiguraNovosItensCarregados();
      end;
    end else
    begin
      if FmOVgIspPrfCfg.TemNovosItensAconfigurar() then
      begin
        Result := True;
        FmOVgIspPrfCfg.ShowModal;
      end else
        Geral.MB_Info('N�o h� novos itens carregados para configurar inspe��o!');
    end;
    FmOVgIspPrfCfg.Destroy;
  end;
end;

function TUnProjGroup_Jan.MostraFormOVgItxPrfCfg(Automatico: Boolean; LaAviso1,
  LaAviso2: TLabel): Boolean;
var
  Mostra: Boolean;
begin
  Result := False;
  if DBCheck.CriaFm(TFmOVgItxPrfCfg, FmOVgItxPrfCfg, afmoNegarComAviso) then
  begin
    FmOVgItxPrfCfg.FAutomatico := Automatico;
    FmOVgItxPrfCfg.FLaAviso1   := LaAviso1;
    FmOVgItxPrfCfg.FLaAviso1   := LaAviso2;
    if Automatico then
    begin
      if FmOVgItxPrfCfg.TemNovosItensAconfigurar() then
      begin
        Result := True;
        FmOVgItxPrfCfg.ConfiguraNovosItensCarregados();
      end;
    end else
    begin
      if FmOVgItxPrfCfg.TemNovosItensAconfigurar() then
      begin
        Result := True;
        FmOVgItxPrfCfg.ShowModal;
      end else
        Geral.MB_Info('N�o h� novos itens carregados para configurar inspe��o!');
    end;
    FmOVgItxPrfCfg.Destroy;
  end;
end;
}

end.
