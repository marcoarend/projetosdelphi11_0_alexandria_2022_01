unit YOcMobDevCad;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2, dmkGeral,
  ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt, UMySQLModule, Menus,
  UnInternalConsts2, mySQLDbTables, UnMySQLCuringa, dmkPermissoes, dmkEdit,
  dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup, Grids, DBGrids,
  UnDmkProcFunc, UnDmkENums, Vcl.ComCtrls, dmkEditDateTimePicker, dmkCheckBox;

type
  TFmYOcMobDevCad = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    Label9: TLabel;
    EdNome: TdmkEdit;
    Label1: TLabel;
    DBEdCodigo: TdmkDBEdit;
    Label2: TLabel;
    DBEdNome: TdmkDBEdit;
    QrYOcMobDevCad: TMySQLQuery;
    DsYOcMobDevCad: TDataSource;
    QrYOcMobDevAlw: TMySQLQuery;
    DsYOcMobDevAlw: TDataSource;
    PMIts: TPopupMenu;
    ItsInclui1: TMenuItem;
    ItsExclui1: TMenuItem;
    ItsAltera1: TMenuItem;
    PMCab: TPopupMenu;
    CabInclui1: TMenuItem;
    CabAltera1: TMenuItem;
    CabExclui1: TMenuItem;
    BtCab: TBitBtn;
    BtIts: TBitBtn;
    QrYOcMobDevCadCodigo: TIntegerField;
    QrYOcMobDevCadNome: TWideStringField;
    QrYOcMobDevCadDeviceID: TWideStringField;
    QrYOcMobDevCadUserNmePdr: TWideStringField;
    QrYOcMobDevCadDeviceName: TWideStringField;
    QrYOcMobDevCadDvcScreenH: TIntegerField;
    QrYOcMobDevCadDvcScreenW: TIntegerField;
    QrYOcMobDevCadOSName: TWideStringField;
    QrYOcMobDevCadOSNickName: TWideStringField;
    QrYOcMobDevCadOSVersion: TWideStringField;
    QrYOcMobDevCadDtaHabIni: TDateTimeField;
    QrYOcMobDevCadDtaHabFim: TDateTimeField;
    QrYOcMobDevCadAllowed: TSmallintField;
    QrYOcMobDevCadLastSetAlw: TIntegerField;
    QrYOcMobDevCadLk: TIntegerField;
    QrYOcMobDevCadDataCad: TDateField;
    QrYOcMobDevCadDataAlt: TDateField;
    QrYOcMobDevCadUserCad: TIntegerField;
    QrYOcMobDevCadUserAlt: TIntegerField;
    QrYOcMobDevCadAlterWeb: TSmallintField;
    QrYOcMobDevCadAWServerID: TIntegerField;
    QrYOcMobDevCadAWStatSinc: TSmallintField;
    QrYOcMobDevCadAtivo: TSmallintField;
    EdDeviceID: TdmkEdit;
    Label3: TLabel;
    EdDeviceName: TdmkEdit;
    Label5: TLabel;
    EdDvcScreenW: TdmkEdit;
    Label6: TLabel;
    EdDvcScreenH: TdmkEdit;
    EdOSName: TdmkEdit;
    Label8: TLabel;
    EdOSNickName: TdmkEdit;
    Label10: TLabel;
    EdOSVersion: TdmkEdit;
    Label11: TLabel;
    TPDtaHabIni: TdmkEditDateTimePicker;
    EdDtaHabIni: TdmkEdit;
    TPDtaHabFim: TdmkEditDateTimePicker;
    EdDtaHabFim: TdmkEdit;
    Label13: TLabel;
    EdUserNmePdr: TdmkEdit;
    Label4: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    Label18: TLabel;
    Label19: TLabel;
    Label20: TLabel;
    Label21: TLabel;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    Label22: TLabel;
    DBEdit4: TDBEdit;
    DBEdit5: TDBEdit;
    Label23: TLabel;
    DBEdit6: TDBEdit;
    DBEdit7: TDBEdit;
    DBEdit8: TDBEdit;
    DBEdit9: TDBEdit;
    DBEdit10: TDBEdit;
    Label24: TLabel;
    DBEdit11: TDBEdit;
    Label12: TLabel;
    Label25: TLabel;
    Label26: TLabel;
    EdNO_Allowed: TdmkEdit;
    QrYOcMobDevCadNO_Allowed: TWideStringField;
    Aceitarnovo1: TMenuItem;
    QrYOcMobDevAlwCodigo: TIntegerField;
    QrYOcMobDevAlwNome: TWideStringField;
    QrYOcMobDevAlwMobDevCad: TIntegerField;
    QrYOcMobDevAlwDeviceID: TWideStringField;
    QrYOcMobDevAlwAlwReason: TSmallintField;
    QrYOcMobDevAlwDtaInicio: TDateTimeField;
    QrYOcMobDevAlwLk: TIntegerField;
    QrYOcMobDevAlwDataCad: TDateField;
    QrYOcMobDevAlwDataAlt: TDateField;
    QrYOcMobDevAlwUserCad: TIntegerField;
    QrYOcMobDevAlwUserAlt: TIntegerField;
    QrYOcMobDevAlwAlterWeb: TSmallintField;
    QrYOcMobDevAlwAWServerID: TIntegerField;
    QrYOcMobDevAlwAWStatSinc: TSmallintField;
    QrYOcMobDevAlwAtivo: TSmallintField;
    QrYOcMobDevAcs: TMySQLQuery;
    DsYOcMobDevAcs: TDataSource;
    QrYOcMobDevAcsCodigo: TIntegerField;
    QrYOcMobDevAcsNome: TWideStringField;
    QrYOcMobDevAcsMobDevCad: TIntegerField;
    QrYOcMobDevAcsDeviceID: TWideStringField;
    QrYOcMobDevAcsAcsReason: TSmallintField;
    QrYOcMobDevAcsDtaAcsIni: TDateTimeField;
    QrYOcMobDevAcsDtaAcsFim: TDateTimeField;
    QrYOcMobDevAcsLk: TIntegerField;
    QrYOcMobDevAcsDataCad: TDateField;
    QrYOcMobDevAcsDataAlt: TDateField;
    QrYOcMobDevAcsUserCad: TIntegerField;
    QrYOcMobDevAcsUserAlt: TIntegerField;
    QrYOcMobDevAcsAlterWeb: TSmallintField;
    QrYOcMobDevAcsAWServerID: TIntegerField;
    QrYOcMobDevAcsAWStatSinc: TSmallintField;
    QrYOcMobDevAcsAtivo: TSmallintField;
    GBAlw: TGroupBox;
    DGDados: TDBGrid;
    Splitter1: TSplitter;
    GBAcs: TGroupBox;
    DBGrid1: TDBGrid;
    GroupBox1: TGroupBox;
    CkSccConfeccao: TdmkCheckBox;
    CkSccTecelagem: TdmkCheckBox;
    CkSccTinturaria: TdmkCheckBox;
    QrYOcMobDevCadSccConfeccao: TSmallintField;
    QrYOcMobDevCadSccTecelagem: TSmallintField;
    QrYOcMobDevCadSccTinturaria: TSmallintField;
    Panel6: TPanel;
    GroupBox2: TGroupBox;
    Panel7: TPanel;
    DBCheckBox1: TDBCheckBox;
    DBCheckBox2: TDBCheckBox;
    DBCheckBox3: TDBCheckBox;
    GroupBox3: TGroupBox;
    Panel8: TPanel;
    DBCheckBox4: TDBCheckBox;
    QrYOcMobDevCadChmOcorrencias: TSmallintField;
    GroupBox4: TGroupBox;
    Panel9: TPanel;
    CkChmOcorrencias: TdmkCheckBox;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrYOcMobDevCadAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrYOcMobDevCadBeforeOpen(DataSet: TDataSet);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure QrYOcMobDevCadAfterScroll(DataSet: TDataSet);
    procedure CabInclui1Click(Sender: TObject);
    procedure CabAltera1Click(Sender: TObject);
    procedure CabExclui1Click(Sender: TObject);
    procedure BtCabClick(Sender: TObject);
    procedure BtItsClick(Sender: TObject);
    procedure ItsInclui1Click(Sender: TObject);
    procedure ItsExclui1Click(Sender: TObject);
    procedure ItsAltera1Click(Sender: TObject);
    procedure PMCabPopup(Sender: TObject);
    procedure PMItsPopup(Sender: TObject);
    procedure QrYOcMobDevCadBeforeClose(DataSet: TDataSet);
    procedure Aceitarnovo1Click(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure LocCod(Atual, Codigo: Integer);
    procedure Va(Para: TVaiPara);
    procedure MostraYOcMobDevAlw(SQLType: TSQLType);

  public
    { Public declarations }
    FSeq, FCabIni: Integer;
    FLocIni: Boolean;
    //
    procedure ReopenYOcMobDevAlw();
    procedure ReopenYOcMobDevAcs();

  end;

var
  FmYOcMobDevCad: TFmYOcMobDevCad;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, MyDBCheck, DmkDAC_PF(*, YOcMobDevAlw*),
  YOcMobDevNew;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmYOcMobDevCad.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmYOcMobDevCad.MostraYOcMobDevAlw(SQLType: TSQLType);
begin
(*
  if DBCheck.CriaFm(TFmYOcMobDevAlw, FmYOcMobDevAlw, afmoNegarComAviso) then
  begin
    FmYOcMobDevAlw.ImgTipo.SQLType := SQLType;
    FmYOcMobDevAlw.FQrCab := QrYOcMobDevCad;
    FmYOcMobDevAlw.FDsCab := DsCadastro_Com_Itens_CAB;
    FmYOcMobDevAlw.FQrIts := QrYOcMobDevAlw;
    if SQLType = stIns then
      FmYOcMobDevAlw.EdCPF1.ReadOnly := False
    else
    begin
      FmYOcMobDevAlw.EdControle.ValueVariant := QrYOcMobDevAlwControle.Value;
      //
      FmYOcMobDevAlw.EdCPF1.Text := MLAGeral.FormataCNPJ_TFT(QrYOcMobDevAlwCNPJ_CPF.Value);
      FmYOcMobDevAlw.EdNomeEmiSac.Text := QrYOcMobDevAlwNome.Value;
      FmYOcMobDevAlw.EdCPF1.ReadOnly := True;
    end;
    FmYOcMobDevAlw.ShowModal;
    FmYOcMobDevAlw.Destroy;
  end;
*)
end;

procedure TFmYOcMobDevCad.PMCabPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemCabUpd(CabAltera1, QrYOcMobDevCad);
  MyObjects.HabilitaMenuItemCabDel(CabExclui1, QrYOcMobDevCad, QrYOcMobDevAlw);
end;

procedure TFmYOcMobDevCad.PMItsPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(ItsInclui1, QrYOcMobDevCad);
  MyObjects.HabilitaMenuItemItsUpd(ItsAltera1, QrYOcMobDevAlw);
  MyObjects.HabilitaMenuItemItsDel(ItsExclui1, QrYOcMobDevAlw);
end;

procedure TFmYOcMobDevCad.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrYOcMobDevCadCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmYOcMobDevCad.DefParams;
begin
  VAR_GOTOTABELA := 'yocmobdevcad';
  VAR_GOTOMYSQLTABLE := QrYOcMobDevCad;
  VAR_GOTONEG := gotoPiZ;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT mdc.*, IF(mdc.Allowed=0, "N�O", "SIM") NO_Allowed');
  VAR_SQLx.Add('FROM yocmobdevcad mdc');
  VAR_SQLx.Add('WHERE mdc.Codigo > 0');
  //
  VAR_SQL1.Add('AND mdc.Codigo=:P0');
  //
  //VAR_SQL2.Add('AND mdc.CodUsu=:P0');
  //
  VAR_SQLa.Add('AND mdc.Nome Like :P0');
  //
end;

procedure TFmYOcMobDevCad.ItsAltera1Click(Sender: TObject);
begin
  MostraYOcMobDevAlw(stUpd);
end;

procedure TFmYOcMobDevCad.CabExclui1Click(Sender: TObject);
begin
  Geral.MB_Info('A��o n�o implementada! Solicite � Dermatek:' + sLineBreak +
  Caption + sLineBreak + TMenuItem(Sender).Name);
end;

procedure TFmYOcMobDevCad.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmYOcMobDevCad.QueryPrincipalAfterOpen;
begin
end;

procedure TFmYOcMobDevCad.ItsExclui1Click(Sender: TObject);
var
  Controle: Integer;
begin
{
  if UMyMod.ExcluiRegistroInt1('Confirma a exclus�o do item selecionado?',
  'YOcMobDevAlw', 'Controle', QrYOcMobDevAlwControle.Value, Dmod.MyDB?) = ID_YES then
  begin
    Controle := GOTOy.LocalizaPriorNextIntQr(QrYOcMobDevAlw,
      QrYOcMobDevAlwControle, QrYOcMobDevAlwControle.Value);
    ReopenYOcMobDevAlw(Controle);
  end;
}
end;

procedure TFmYOcMobDevCad.ReopenYOcMobDevAcs;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrYOcMobDevAcs, Dmod.MyDB, [
  'SELECT * ',
  'FROM yocmobdevacs ',
  'WHERE MobDevCad=' + Geral.FF0(QrYOcMobDevCadCodigo.Value),
  'ORDER BY DtaAcsIni DESC',
  '']);
  //
//  QrYOcMobDevAcs.Locate('Controle?, Controle, []);
end;

procedure TFmYOcMobDevCad.ReopenYOcMobDevAlw();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrYOcMobDevAlw, Dmod.MyDB, [
  'SELECT * ',
  'FROM yocmobdevalw ',
  'WHERE MobDevCad=' + Geral.FF0(QrYOcMobDevCadCodigo.Value),
  'ORDER BY DtaInicio DESC',
  '']);
  //
//  QrYOcMobDevAlw.Locate('Controle?, Controle, []);
end;


procedure TFmYOcMobDevCad.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmYOcMobDevCad.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmYOcMobDevCad.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmYOcMobDevCad.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmYOcMobDevCad.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmYOcMobDevCad.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmYOcMobDevCad.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrYOcMobDevCadCodigo.Value;
  Close;
end;

procedure TFmYOcMobDevCad.ItsInclui1Click(Sender: TObject);
begin
  MostraYOcMobDevAlw(stIns);
end;

procedure TFmYOcMobDevCad.CabAltera1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrYOcMobDevCad, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'yocmobdevcad');
end;

procedure TFmYOcMobDevCad.BtConfirmaClick(Sender: TObject);
var
  //DeviceID, DeviceName, OSName, OSNickName, OSVersion,
  //DtaHabIni, DtaHabFim
  Nome, UserNmePdr: String;
  //DvcScreenH, DvcScreenW,
  //Allowed, LastSetAlw,
  Codigo: Integer;
  SQLType: TSQLType;
  SccConfeccao, SccTecelagem, SccTinturaria, ChmOcorrencias: Integer;
begin
  SQLType        := ImgTipo.SQLType;
  Codigo         := EdCodigo.ValueVariant;
  Nome           := EdNome.ValueVariant;
  //DeviceID       := EdDeviceID.ValueVariant;
  UserNmePdr     := EdUserNmePdr.ValueVariant;
  //DeviceName     := EdDeviceName.ValueVariant;
  //DvcScreenH     := EdDvcScreenH.ValueVariant;
  //DvcScreenW     := EdDvcScreenW.ValueVariant;
  //OSName         := EdOSName.ValueVariant;
  //OSNickName     := EdOSNickName.ValueVariant;
  //OSVersion      := EdOSVersion.ValueVariant;
  //DtaHabIni      := DtaHabIni ;
  //DtaHabIni      := Geral.FDT_TP_Ed(TPDtaHabIni.Date, EdDtaHabIni.Text);
  //DtaHabFim      := DtaHabFim ;
  //DtaHabFim      := Geral.FDT_TP_Ed(TPDtaHabFim.Date, EdDtaHabFim.Text);
  //Allowed        := Allowed   ;
  //LastSetAlw     := LastSetAlw;
  SccConfeccao     := Geral.BoolToInt(CkSccConfeccao.Checked);
  SccTecelagem     := Geral.BoolToInt(CkSccTecelagem.Checked);
  SccTinturaria    := Geral.BoolToInt(CkSccTinturaria.Checked);
  ChmOcorrencias   := Geral.BoolToInt(CkChmOcorrencias.Checked);

  //
  if MyObjects.FIC(Length(Nome) = 0, EdNome, 'Defina uma descri��o!') then Exit;
  if MyObjects.FIC(Length(UserNmePdr) = 0, EdUserNmePdr,
    'Defina o nome do respons�vel padr�o do dispositivo!') then Exit;
  //
  Codigo := UMyMod.BPGS1I32('yocmobdevcad', 'Codigo', '', '', tsPos, SQLType, Codigo);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'yocmobdevcad', False, [
  'Nome', (*'DeviceID',*) 'UserNmePdr'(*,
  'DeviceName', 'DvcScreenH', 'DvcScreenW',
  'OSName', 'OSNickName', 'OSVersion',
  'DtaHabIni', 'DtaHabFim', 'Allowed',
  'LastSetAlw'*),
  'SccConfeccao', 'SccTecelagem', 'SccTinturaria',
  'ChmOcorrencias'], [
  'Codigo'], [
  Nome, (*DeviceID,*) UserNmePdr(*,
  DeviceName, DvcScreenH, DvcScreenW,
  OSName, OSNickName, OSVersion,
  DtaHabIni, DtaHabFim, Allowed,
  LastSetAlw*),
  SccConfeccao, SccTecelagem, SccTinturaria,
  ChmOcorrencias], [
  Codigo], True) then
  begin
    ImgTipo.SQLType := stLok;
    PnDados.Visible := True;
    PnEdita.Visible := False;
    GOTOy.BotoesSb(ImgTipo.SQLType);
    //
    LocCod(Codigo, Codigo);
    if FSeq = 1 then Close;
  end;
end;

procedure TFmYOcMobDevCad.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := EdCodigo.ValueVariant;
  if ImgTipo.SQLType = stIns then
    UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'yocmobdevcad', Codigo);
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'yocmobdevcad', 'Codigo');
end;

procedure TFmYOcMobDevCad.BtItsClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMIts, BtIts);
end;

procedure TFmYOcMobDevCad.Aceitarnovo1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmYOcMobDevNew, FmYOcMobDevNew, afmoNegarComAviso) then
  begin
    FmYOcMobDevNew.ShowModal;
    FmYOcMobDevNew.Destroy;
  end;

end;

procedure TFmYOcMobDevCad.BtCabClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMCab, BtCab);
end;

procedure TFmYOcMobDevCad.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  GBEdita.Align := alClient;
  GBAcs.Align := alClient;
  CriaOForm;
  FSeq := 0;
end;

procedure TFmYOcMobDevCad.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrYOcMobDevCadCodigo.Value, LaRegistro.Caption);
end;

procedure TFmYOcMobDevCad.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmYOcMobDevCad.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrYOcMobDevCadCodigo.Value, LaRegistro.Caption);
end;

procedure TFmYOcMobDevCad.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmYOcMobDevCad.QrYOcMobDevCadAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmYOcMobDevCad.QrYOcMobDevCadAfterScroll(DataSet: TDataSet);
begin
  ReopenYOcMobDevAlw();
  ReopenYOcMobDevAcs();
end;

procedure TFmYOcMobDevCad.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //
  if FSeq = 1 then
    CabInclui1Click(Self)
  else
  if (not FLocIni) and (FCabIni <> 0)  then
  begin
    LocCod(FCabIni, FCabIni);
    if QrYOcMobDevCadCodigo.Value <> FCabIni then
      Geral.MB_Aviso('Grupo n�o localizado!');
    FLocIni := True;
  end;
end;

procedure TFmYOcMobDevCad.SbQueryClick(Sender: TObject);
begin
  LocCod(QrYOcMobDevCadCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'yocmobdevcad', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmYOcMobDevCad.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmYOcMobDevCad.CabInclui1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrYOcMobDevCad, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'yocmobdevcad');
end;

procedure TFmYOcMobDevCad.QrYOcMobDevCadBeforeClose(
  DataSet: TDataSet);
begin
  QrYOcMobDevAlw.Close;
  QrYOcMobDevAcs.Close;
end;

procedure TFmYOcMobDevCad.QrYOcMobDevCadBeforeOpen(DataSet: TDataSet);
begin
  QrYOcMobDevCadCodigo.DisplayFormat := FFormatFloat;
end;

end.

