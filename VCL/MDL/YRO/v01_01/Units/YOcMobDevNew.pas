unit YOcMobDevNew;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkPermissoes,
  mySQLDbTables, Vcl.Mask;

type
  TFmYOcMobDevNew = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    QrYOcMobDevCad: TMySQLQuery;
    QrYOcMobDevCadCodigo: TIntegerField;
    QrYOcMobDevCadNome: TWideStringField;
    QrYOcMobDevCadDeviceID: TWideStringField;
    QrYOcMobDevCadUserNmePdr: TWideStringField;
    QrYOcMobDevCadDeviceName: TWideStringField;
    QrYOcMobDevCadDvcScreenH: TIntegerField;
    QrYOcMobDevCadDvcScreenW: TIntegerField;
    QrYOcMobDevCadOSName: TWideStringField;
    QrYOcMobDevCadOSNickName: TWideStringField;
    QrYOcMobDevCadOSVersion: TWideStringField;
    QrYOcMobDevCadDtaHabIni: TDateTimeField;
    QrYOcMobDevCadDtaHabFim: TDateTimeField;
    QrYOcMobDevCadAllowed: TSmallintField;
    QrYOcMobDevCadLastSetAlw: TIntegerField;
    QrYOcMobDevCadLk: TIntegerField;
    QrYOcMobDevCadDataCad: TDateField;
    QrYOcMobDevCadDataAlt: TDateField;
    QrYOcMobDevCadUserCad: TIntegerField;
    QrYOcMobDevCadUserAlt: TIntegerField;
    QrYOcMobDevCadAlterWeb: TSmallintField;
    QrYOcMobDevCadAWServerID: TIntegerField;
    QrYOcMobDevCadAWStatSinc: TSmallintField;
    QrYOcMobDevCadAtivo: TSmallintField;
    DsYOcMobDevCad: TDataSource;
    GBDados: TGroupBox;
    Label16: TLabel;
    Label17: TLabel;
    Label19: TLabel;
    Label20: TLabel;
    Label21: TLabel;
    Label22: TLabel;
    Label23: TLabel;
    DBEdit1: TDBEdit;
    DBEdit3: TDBEdit;
    DBEdit4: TDBEdit;
    DBEdit5: TDBEdit;
    DBEdit6: TDBEdit;
    DBEdit7: TDBEdit;
    DBEdit8: TDBEdit;
    Timer1: TTimer;
    BtExclui: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure QrYOcMobDevCadAfterOpen(DataSet: TDataSet);
    procedure Timer1Timer(Sender: TObject);
    procedure BtExcluiClick(Sender: TObject);
  private
    { Private declarations }
    FTempo: Integer;
    procedure ReopenYOcMobDevCad();
    procedure ConfirmearCadastro(Ativa: Integer);
  public
    { Public declarations }
  end;

  var
  FmYOcMobDevNew: TFmYOcMobDevNew;

implementation

uses UnMyObjects, Module, DmkDAC_PF, UMySQLModule, ModuleGeral, UnYRO_PF;

{$R *.DFM}

procedure TFmYOcMobDevNew.BtExcluiClick(Sender: TObject);
begin
(*
  if Geral.MB_Pergunta('Este dispositivo ser� exclu�do do cadastro! ' +
  sLineBreak + 'Deseja continuar assim mesmo?') = ID_YES then
  begin
    UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB,[
    'DELETE FROM ovcmobdevcad ',
    'WHERE Codigo=' + Geral.FF0(QrYOcMobDevCadCodigo.Value),
    '']);
  end;
  ReopenYOcMobDevCad();
*)
  ConfirmearCadastro(0);
end;

procedure TFmYOcMobDevNew.BtOKClick(Sender: TObject);
begin
  ConfirmearCadastro(1);
end;

procedure TFmYOcMobDevNew.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmYOcMobDevNew.ConfirmearCadastro(Ativa: Integer);
var
  //Nome,
  DeviceID,
  //UserNmePdr, DeviceName, OSName, OSNickName, OSVersion, DtaHabFim,
  DtaHabIni: String;
  //DvcScreenH, DvcScreenW, LastSetAlw,
  Allowed, CodAnt, CodNew: Integer;
  //SQLType: TSQLType;
  Data: TDateTime;
begin
  Data           := DModG.ObtemAgora();
  //SQLType        := ImgTipo.SQLType?;
  CodAnt         := QrYOcMobDevCadCodigo.Value;
  CodNew         := 0;
  //Nome           := ;
  DeviceID       := QrYOcMobDevCadDeviceID.Value;
  //UserNmePdr     := ;
  //DeviceName     := ;
  //DvcScreenH     := ;
  //DvcScreenW     := ;
  //OSName         := ;
  //OSNickName     := ;
  //OSVersion      := ;
  DtaHabIni      := Geral.FDT(Data, 109);
  //DtaHabFim      := ;
  Allowed        := Ativa;  // sim
  //LastSetAlw     := ;

  //
  CodNew := UMyMod.BPGS1I32('ovcmobdevcad', 'Codigo', '', '', tsPos, stIns, 0);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'ovcmobdevcad', False, [
  'Codigo',
  (*'Nome', 'DeviceID', 'UserNmePdr',
  'DeviceName', 'DvcScreenH', 'DvcScreenW',
  'OSName', 'OSNickName', 'OSVersion',*)
  'DtaHabIni', (*'DtaHabFim',*) 'Allowed'(*,
  'LastSetAlw'*)], [
  'Codigo'], [
  CodNew,
  (*Nome, DeviceID, UserNmePdr,
  DeviceName, DvcScreenH, DvcScreenW,
  OSName, OSNickName, OSVersion,*)
  DtaHabIni, (*DtaHabFim,*) Allowed(*,
  LastSetAlw*)], [
  CodAnt], True) then
  begin
    YRO_PF.YOcModDevAcs_Acesso(CodNew, DeviceID,
      TDeviceAcsReason.darAtivarDispositivo, Data);
    //
    YRO_PF.YOcMobDevAlw_MudaPermissao(CodNew, DeviceID,
      TDeviceAllowReason.dalAtivado1aVez, Data);
    //
    Geral.MB_Info('Dispositivo ativado!');
  end;
  ReopenYOcMobDevCad();
end;

procedure TFmYOcMobDevNew.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmYOcMobDevNew.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  FTempo := 14;
  Timer1.Enabled := True;
end;

procedure TFmYOcMobDevNew.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmYOcMobDevNew.QrYOcMobDevCadAfterOpen(DataSet: TDataSet);
var
  Mensagem: String;
begin
  if QrYOcMobDevCad.RecordCount > 0 then
  begin
    Timer1.Enabled := False;
    if QrYOcMobDevCad.RecordCount = 1 then
      Mensagem := 'Um novo dispositivo foi encontrado!'
    else
      Mensagem := Geral.FF0(QrYOcMobDevCad.RecordCount) +
      ' novos dispositivos foram encontrados!';
    MyObjects.Informa2(LaAviso1, LaAviso2, True, Mensagem);
  end;
end;

procedure TFmYOcMobDevNew.ReopenYOcMobDevCad();
begin
  UnDmkDAC_PF.AbreQuery(QrYOcMobDevCad, Dmod.MyDB);
end;

procedure TFmYOcMobDevNew.Timer1Timer(Sender: TObject);
begin
  FTempo := FTempo + 1;
  if (QrYOcMobDevCad.State = dsInactive)
  or (QrYOcMobDevCad.RecordCount = 0) then
  begin
    MyObjects.Informa2(LaAviso1, LaAviso2, True,
    'Nenhum novo dispositivo encontrado. Nova tentativa em ' + Geral.FF0(
    15 - FTempo) + ' segundos.');
    if FTempo >= 15 then
    begin
      Timer1.Enabled := False;
      ReopenYOcMobDevCad();
      FTempo := 0;
      Timer1.Enabled := QrYOcMobDevCad.RecordCount = 0;
    end;
  end;
end;

end.
