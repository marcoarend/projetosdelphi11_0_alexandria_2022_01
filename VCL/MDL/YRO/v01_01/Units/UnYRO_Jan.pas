unit UnYRO_Jan;

interface

uses
  Windows, SysUtils, Classes, DB, Menus, UnDmkEnums, mySQLDbTables, Forms,
  Grids, DBGrids, UnInternalConsts, Variants;

type
  TUnYRO_Jan = class(TObject)
  private
    { Private declarations }
    procedure AvisoDadosTerceiros();
  public
    { Public declarations }
{
    function  LocalizaOVgIspGerCab(var Codigo: Integer): Boolean;
    function  LocalizaOVgIspPrfCab(var Codigo: Integer): Boolean;

    function  LocalizaOVgItxGerCab(var Codigo: Integer): Boolean;
    function  LocalizaOVgItxPrfCab(var Codigo: Integer): Boolean;
}

    procedure MostraFormImportaCSV_ERP_01();

{
    procedure MostraFormOVdCiclo();
    procedure MostraFormOVdLocal();
    procedure MostraFormOVdReferencia();
    procedure MostraFormOVdLote();
    procedure MostraFormOVdProduto();

    procedure MostraFormOVcYnsQstTop();
    procedure MostraFormOVcYnsQstCtx();
    procedure MostraFormOVcYnsQstMag();

    procedure MostraFormOVcYnsChkCad();

    procedure MostraFormOVcYnsGraTop();

    procedure MostraFormOVcYnsARQCad();

    procedure MostraFormOVcYnsMedCad(Artigo: Integer);

    procedure MostraFormOVfOPGerFil_Fac(MostraSeEmpty: Boolean);
    procedure MostraFormOVfOPGerFil_Tex(MostraSeEmpty: Boolean);

    function  MostraFormOVgIspGerCad(SQLType: TSQLType; Codigo, Local: Integer;
              NO_Local: String; NrOP, SeqGrupo: Integer; NO_SeqGrupo: String;
              NrReduzidoOP, OVcYnsMed, OVcYnsChk, OVcYnsARQ, LimiteMed,
              LimiteChk, PermiFinHow, ZtatusIsp: Integer): Integer;
    procedure MostraFormOVgIspGerCab(Codigo: Integer);
    function  MostraFormOVgIspPrfCad(SQLType: TSQLType; Codigo, SeqGrupo:
              Integer; NO_SeqGrupo, Referencia: String; OVcYnsMed, OVcYnsChk,
              OVcYnsARQ, LimiteMed, LimiteChk, PermiFinHow, Ativo: Integer;
              Nome: String): Integer;
    procedure MostraFormOVgIspPrfCab(Codigo: Integer);
    procedure MostraFormOVmIspDevCab(Codigo: Integer);
}

    procedure MostraFormOVcMobDevCad();

    procedure MostraFormOVpLayEsq(Esquema: Integer);

{
    procedure MostraFormOVcYnsMixTop();
    procedure MostraFormOVcYnsExgCad(Artigo: Integer);

    function  MostraFormOVgItxGerCad(SQLType: TSQLType; Codigo, Local: Integer;
              NO_Local: String; NrOP, SeqGrupo: Integer; NO_SeqGrupo: String;
              NrReduzidoOP, OVcYnsExg, (*OVcYnsChk, OVcYnsARQ, LimiteMed,
              LimiteChk,*) PermiFinHow, ZtatusIsp: Integer): Integer;
    procedure MostraFormOVgItxGerCab(Codigo: Integer);
    function  MostraFormOVgItxPrfCad(SQLType: TSQLType; Codigo, SeqGrupo:
              Integer; NO_SeqGrupo, Referencia: String; OVcYnsExg, (*OVcYnsChk,
              OVcYnsARQ, LimiteMed, LimiteChk,*) PermiFinHow, Ativo: Integer;
              Nome: String): Integer;
    procedure MostraFormOVgItxPrfCab(Codigo: Integer);
    procedure MostraFormOVmItxDevCab(Codigo: Integer);
    procedure MostraFormOVgItxGerBtl(SQLType: TSQLType; DsCab: TDatasource; QrOVgItxGerBtl: TmySQLQuery);

    procedure MostraFormSeccTexTinturaria(Forca: Boolean);
}
  end;

var
  YRO_Jan: TUnYRO_Jan;


implementation

uses
  dmkGeral, MyDBCheck, Module,
{
  OVdCiclo, OVdLocal,  OVdProduto, OVdReferencia, (*OVdLote,*)
  OVcYnsQstTop,
  OVcYnsChkCad, OVcYnsGraTop, OVcYnsARQCad, OVcYnsMedCad,
  ImportaCSV_ERP_01, OVgIspPrfCfg,
  OVfOPGerFil_Fac, OVfOPGerFil_Tex,
  OVgIspGerCad, OVgIspGerCab, OVmIspDevCab, OVgIspPrfCab, OVgIspPrfCad,
  OVgItxPrfCad,
  OVgLocIGC, OVgLocIPC,
  OVcMobDevCad,
  OVpLayEsq,
  OVcYnsMixTop, OVcYnsExgCad,
  OVgItxGerCad, OVgItxGerCab, OVmItxDevCab, OVgItxPrfCab,
  OVgLocJGC, OVgLocJPC,
  SeccTexTinturaria, OVgItxGerBtl;
}

  CfgCadLista, YOcMobDevCad;
{ TUnYRO_Jan }

procedure TUnYRO_Jan.AvisoDadosTerceiros();
begin
  Geral.MB_Info(
  'Os dados solicitados foram importados de base de dados de terceiros e n�o ' +
  '� aconselhavel edit�-los pois podem ser sobrescrevidos na pr�xima importa��o!!!');
end;

{
function TUnYRO_Jan.LocalizaOVgIspGerCab(var Codigo: Integer): Boolean;
begin
  if DBCheck.CriaFm(TFmOVgLocIGC, FmOVgLocIGC, afmoNegarComAviso) then
  begin
    FmOVgLocIGC.ShowModal;
    Codigo := FmOVgLocIGC.FCodigo;
    FmOVgLocIGC.Destroy;
    Result := Codigo <> 0;
  end;
end;

function TUnYRO_Jan.LocalizaOVgIspPrfCab(var Codigo: Integer): Boolean;
begin
  if DBCheck.CriaFm(TFmOVgLocIPC, FmOVgLocIPC, afmoNegarComAviso) then
  begin
    FmOVgLocIPC.ShowModal;
    Codigo := FmOVgLocIPC.FCodigo;
    FmOVgLocIPC.Destroy;
    Result := Codigo <> 0;
  end;
end;

function TUnYRO_Jan.LocalizaOVgItxGerCab(var Codigo: Integer): Boolean;
begin
  if DBCheck.CriaFm(TFmOVgLocJGC, FmOVgLocJGC, afmoNegarComAviso) then
  begin
    FmOVgLocJGC.ShowModal;
    Codigo := FmOVgLocJGC.FCodigo;
    FmOVgLocJGC.Destroy;
    Result := Codigo <> 0;
  end;
end;

function TUnYRO_Jan.LocalizaOVgItxPrfCab(var Codigo: Integer): Boolean;
begin
  if DBCheck.CriaFm(TFmOVgLocJPC, FmOVgLocJPC, afmoNegarComAviso) then
  begin
    FmOVgLocJPC.ShowModal;
    Codigo := FmOVgLocJPC.FCodigo;
    FmOVgLocJPC.Destroy;
    Result := Codigo <> 0;
  end;
end;
}

procedure TUnYRO_Jan.MostraFormImportaCSV_ERP_01();
begin
(*&�%$#@!"
  if DBCheck.CriaFm(TFmImportaCSV_ERP_01, FmImportaCSV_ERP_01, afmoNegarComAviso) then
  begin
    FmImportaCSV_ERP_01.ShowModal;
    FmImportaCSV_ERP_01.Destroy;
  end;
*)
end;

procedure TUnYRO_Jan.MostraFormOVcMobDevCad();
begin
  if DBCheck.CriaFm(TFmYOcMobDevCad, FmYOcMobDevCad, afmoNegarComAviso) then
  begin
    FmYOcMobDevCad.ShowModal;
    FmYOcMobDevCad.Destroy;
  end;
end;

{
procedure TUnYRO_Jan.MostraFormOVcYnsARQCad();
begin
  if DBCheck.CriaFm(TFmOVcYnsARQCad, FmOVcYnsARQCad, afmoNegarComAviso) then
  begin
    FmOVcYnsARQCad.ShowModal;
    FmOVcYnsARQCad.Destroy;
  end;
end;

procedure TUnYRO_Jan.MostraFormOVcYnsChkCad();
begin
  if DBCheck.CriaFm(TFmOVcYnsChkCad, FmOVcYnsChkCad, afmoNegarComAviso) then
  begin
    FmOVcYnsChkCad.ShowModal;
    FmOVcYnsChkCad.Destroy;
  end;
end;

procedure TUnYRO_Jan.MostraFormOVcYnsExgCad(Artigo: Integer);
begin
  if DBCheck.CriaFm(TFmOVcYnsExgCad, FmOVcYnsExgCad, afmoNegarComAviso) then
  begin
    FmOVcYnsExgCad.FArtigo := Artigo;
    FmOVcYnsExgCad.EdArtigRef.ValueVariant := Artigo;
    FmOVcYnsExgCad.CBArtigRef.KeyValue     := Artigo;
    FmOVcYnsExgCad.ShowModal;
    FmOVcYnsExgCad.Destroy;
  end;
end;

procedure TUnYRO_Jan.MostraFormOVcYnsMixTop();
begin
  if DBCheck.CriaFm(TFmOVcYnsMixTop, FmOVcYnsMixTop, afmoNegarComAviso) then
  begin
    FmOVcYnsMixTop.ShowModal;
    FmOVcYnsMixTop.Destroy;
  end;
end;

procedure TUnYRO_Jan.MostraFormOVcYnsGraTop();
begin
  if DBCheck.CriaFm(TFmOVcYnsGraTop, FmOVcYnsGraTop, afmoNegarComAviso) then
  begin
    FmOVcYnsGraTop.ShowModal;
    FmOVcYnsGraTop.Destroy;
  end;
end;

procedure TUnYRO_Jan.MostraFormOVcYnsMedCad(Artigo: Integer);
begin
  if DBCheck.CriaFm(TFmOVcYnsMedCad, FmOVcYnsMedCad, afmoNegarComAviso) then
  begin
    FmOVcYnsMedCad.FArtigo := Artigo;
    FmOVcYnsMedCad.EdArtigRef.ValueVariant := Artigo;
    FmOVcYnsMedCad.CBArtigRef.KeyValue     := Artigo;
    FmOVcYnsMedCad.ShowModal;
    FmOVcYnsMedCad.Destroy;
  end;
end;

procedure TUnYRO_Jan.MostraFormOVcYnsQstCtx();
begin
  UnCfgCadLista.MostraCadLista(Dmod.MyDB, 'OVcYnsQstCtx', 60, UnDmkEnums.ncGerlSeq1,
  'Contextos de Inconformidades',
  [], False, Null, [], [], False);
end;

procedure TUnYRO_Jan.MostraFormOVcYnsQstMag();
begin
  UnCfgCadLista.MostraCadLista(Dmod.MyDB, 'OVcYnsQstMag', 60, UnDmkEnums.ncGerlSeq1,
  'Magnitudes de Inconformidade',
  [], True, Null(*Maximo 0*), [], [], False);
end;

procedure TUnYRO_Jan.MostraFormOVcYnsQstTop();
begin
  if DBCheck.CriaFm(TFmOVcYnsQstTop, FmOVcYnsQstTop, afmoNegarComAviso) then
  begin
    FmOVcYnsQstTop.ShowModal;
    FmOVcYnsQstTop.Destroy;
  end;
end;

procedure TUnYRO_Jan.MostraFormOVdCiclo();
begin
  if DBCheck.CriaFm(TFmOVdCiclo, FmOVdCiclo, afmoNegarComAviso) then
  begin
    FmOVdCiclo.ShowModal;
    FmOVdCiclo.Destroy;
  end;
end;

procedure TUnYRO_Jan.MostraFormOVdLocal;
begin
  if DBCheck.CriaFm(TFmOVdLocal, FmOVdLocal, afmoNegarComAviso) then
  begin
    FmOVdLocal.ShowModal;
    FmOVdLocal.Destroy;
  end;
end;

procedure TUnYRO_Jan.MostraFormOVdLote();
begin
(*
  if DBCheck.CriaFm(TFmOVdLote, FmOVdLote, afmoNegarComAviso) then
  begin
    FmOVdLote.ShowModal;
    FmOVdLote.Destroy;
  end;
*)
end;

procedure TUnYRO_Jan.MostraFormOVdProduto();
begin
  if DBCheck.CriaFm(TFmOVdProduto, FmOVdProduto, afmoNegarComAviso) then
  begin
    FmOVdProduto.ShowModal;
    FmOVdProduto.Destroy;
  end;
end;

procedure TUnYRO_Jan.MostraFormOVdReferencia();
begin
  if DBCheck.CriaFm(TFmOVdReferencia, FmOVdReferencia, afmoNegarComAviso) then
  begin
    FmOVdReferencia.ShowModal;
    FmOVdReferencia.Destroy;
  end;
end;

procedure TUnYRO_Jan.MostraFormOVfOPGerFil_Fac(MostraSeEmpty: Boolean);
begin
  if DBCheck.CriaFm(TFmOVfOPGerFil_Fac, FmOVfOPGerFil_Fac, afmoNegarComAviso) then
  begin
    FmOVfOPGerFil_Fac.ReopenNovasReduzOPs();
    //if (FmOVfOPGerFil_Fac.QrNovasReduzOPs.RecordCount > 0) or
    if (FmOVfOPGerFil_Fac.QrOVgIspLasSta.RecordCount > 0) or
    (MostraSeEmpty = True) then
      FmOVfOPGerFil_Fac.ShowModal;
    FmOVfOPGerFil_Fac.Destroy;
  end;
end;

procedure TUnYRO_Jan.MostraFormOVfOPGerFil_Tex(MostraSeEmpty: Boolean);
begin
  if DBCheck.CriaFm(TFmOVfOPGerFil_Tex, FmOVfOPGerFil_Tex, afmoNegarComAviso) then
  begin
    FmOVfOPGerFil_Tex.ReopenNovasReduzOPs();
    //if (FmOVfOPGerFil_Tex.QrNovasReduzOPs.RecordCount > 0) or
    if (FmOVfOPGerFil_Tex.QrOVgIspLasSta.RecordCount > 0) or
    (MostraSeEmpty = True) then
      FmOVfOPGerFil_Tex.ShowModal;
    FmOVfOPGerFil_Tex.Destroy;
  end;
end;

procedure TUnYRO_Jan.MostraFormOVgIspGerCab(Codigo: Integer);
begin
  if DBCheck.CriaFm(TFmOVgIspGerCab, FmOVgIspGerCab, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
      FmOVgIspGerCab.LocCod(Codigo, Codigo);
    FmOVgIspGerCab.ShowModal;
    FmOVgIspGerCab.Destroy;
  end;
end;

function TUnYRO_Jan.MostraFormOVgIspGerCad(SQLType: TSQLType; Codigo, Local:
  Integer; NO_Local: String; NrOP, SeqGrupo: Integer; NO_SeqGrupo: String;
  NrReduzidoOP, OVcYnsMed, OVcYnsChk, OVcYnsARQ, LimiteMed, LimiteChk,
  PermiFinHow, ZtatusIsp: Integer): Integer;
begin
  Result := 0;
  if DBCheck.CriaFm(TFmOVgIspGerCad, FmOVgIspGerCad, afmoNegarComAviso) then
  begin
    FmOVgIspGerCad.ImgTipo.SQLType             := SQLType;
    FmOVgIspGerCad.FSeqGrupo                   := SeqGrupo;
    FmOVgIspGerCad.EdLocal.ValueVariant        := Local;
    FmOVgIspGerCad.EdNO_Local.ValueVariant     := NO_Local;
    FmOVgIspGerCad.EdNrOP.ValueVariant         := NrOP;
    FmOVgIspGerCad.EdSeqGrupo.ValueVariant     := SeqGrupo;
    FmOVgIspGerCad.EdNO_SeqGrupo.ValueVariant  := NO_SeqGrupo;
    FmOVgIspGerCad.EdNrReduzidoOP.ValueVariant := NrReduzidoOP;
    FmOVgIspGerCad.ReopenOVcYnsMedCad(SeqGrupo);
    if SQLTYpe = stUpd then
    begin
      FmOVgIspGerCad.EdCodigo.ValueVariant := Codigo;
      FmOVgIspGerCad.EdOVcYnsMed.ValueVariant := OVcYnsMed;
      FmOVgIspGerCad.CBOVcYnsMed.KeyValue     := OVcYnsMed;
      FmOVgIspGerCad.EdOVcYnsChk.ValueVariant := OVcYnsChk;
      FmOVgIspGerCad.CBOVcYnsChk.KeyValue     := OVcYnsChk;
      FmOVgIspGerCad.EdOVcYnsARQ.ValueVariant := OVcYnsARQ;
      FmOVgIspGerCad.CBOVcYnsARQ.KeyValue     := OVcYnsARQ;
      FmOVgIspGerCad.EdLimiteMed.ValueVariant := LimiteMed;
      FmOVgIspGerCad.EdLimiteChk.ValueVariant := LimiteChk;
      FmOVgIspGerCad.RGPermiFinHow.ItemIndex  := PermiFinHow;
    end;
    FmOVgIspGerCad.FZtatusIsp := ZtatusIsp; //CO_O V S_IMPORT_ALHEIO_3972_CONFIGURADO
    //
    FmOVgIspGerCad.ShowModal;
    Result := FmOVgIspGerCad.FCodigo;
    FmOVgIspGerCad.Destroy;
  end;
end;

procedure TUnYRO_Jan.MostraFormOVgIspPrfCab(Codigo: Integer);
begin
  if DBCheck.CriaFm(TFmOVgIspPrfCab, FmOVgIspPrfCab, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
      FmOVgIspPrfCab.LocCod(Codigo, Codigo);
    FmOVgIspPrfCab.ShowModal;
    FmOVgIspPrfCab.Destroy;
  end;
end;

function TUnYRO_Jan.MostraFormOVgIspPrfCad(SQLType: TSQLType; Codigo,
  SeqGrupo: Integer; NO_SeqGrupo, Referencia: String; OVcYnsMed, OVcYnsChk,
  OVcYnsARQ, LimiteMed, LimiteChk, PermiFinHow, Ativo: Integer; Nome: String):
  Integer;
begin
  Result := 0;
  if DBCheck.CriaFm(TFmOVgIspPrfCad, FmOVgIspPrfCad, afmoNegarComAviso) then
  begin
    FmOVgIspPrfCad.ImgTipo.SQLType             := SQLType;
    FmOVgIspPrfCad.FSeqGrupo                   := SeqGrupo;
    FmOVgIspPrfCad.EdSeqGrupo.ValueVariant     := SeqGrupo;
    FmOVgIspPrfCad.EdNO_SeqGrupo.ValueVariant  := NO_SeqGrupo;
    FmOVgIspPrfCad.EdReferencia.ValueVariant   := Referencia;
    FmOVgIspPrfCad.ReopenOVcYnsMedCad(SeqGrupo);
    if SQLTYpe = stUpd then
    begin
      FmOVgIspPrfCad.EdCodigo.ValueVariant := Codigo;
      FmOVgIspPrfCad.EdOVcYnsMed.ValueVariant := OVcYnsMed;
      FmOVgIspPrfCad.CBOVcYnsMed.KeyValue     := OVcYnsMed;
      FmOVgIspPrfCad.EdOVcYnsChk.ValueVariant := OVcYnsChk;
      FmOVgIspPrfCad.CBOVcYnsChk.KeyValue     := OVcYnsChk;
      FmOVgIspPrfCad.EdOVcYnsARQ.ValueVariant := OVcYnsARQ;
      FmOVgIspPrfCad.CBOVcYnsARQ.KeyValue     := OVcYnsARQ;
      FmOVgIspPrfCad.EdLimiteMed.ValueVariant := LimiteMed;
      FmOVgIspPrfCad.EdLimiteChk.ValueVariant := LimiteChk;
      FmOVgIspPrfCad.RGPermiFinHow.ItemIndex  := PermiFinHow;
      FmOVgIspPrfCad.CkAtivo.Checked          := Geral.IntToBool(Ativo);
      FmOVgIspPrfCad.EdNome.ValueVariant      := Nome;
    end;
    //
    FmOVgIspPrfCad.ShowModal;
    Result := FmOVgIspPrfCad.FCodigo;
    FmOVgIspPrfCad.Destroy;
  end;
end;

procedure TUnYRO_Jan.MostraFormOVgItxGerBtl(SQLType: TSQLType; DsCab:
  TDatasource; QrOVgItxGerBtl: TmySQLQuery);
begin
  if DBCheck.CriaFm(TFmOVgItxGerBtl, FmOVgItxGerBtl, afmoNegarComAviso) then
  begin
    try
      FmOVgItxGerBtl.ImgTipo.SQLType := SQLType;
      FmOVgItxGerBtl.FDsCab := DsCab;
      FmOVgItxGerBtl.FQrIts := QrOVgItxGerBtl;
      //
      if SQLType = stIns then
      begin
        //FmOVgItxGerBtl.EdCPF1.ReadOnly := False
      end else
      begin
        //
        FmOVgItxGerBtl.EdControle.ValueVariant := QrOVgItxGerBtl.FieldByName('Controle').AsInteger;
        //
        FmOVgItxGerBtl.EdOrdem.ValueVariant     := QrOVgItxGerBtl.FieldByName('Ordem').AsInteger;
        FmOVgItxGerBtl.EdSeccaoOP.ValueVariant  := QrOVgItxGerBtl.FieldByName('SeccaoOP').AsString;
        FmOVgItxGerBtl.EdSeccaoMaq.ValueVariant := QrOVgItxGerBtl.FieldByName('SeccaoMaq').AsString;
        FmOVgItxGerBtl.EdQtReal.ValueVariant    := QrOVgItxGerBtl.FieldByName('QtReal').AsFloat;
      end;
      FmOVgItxGerBtl.ShowModal;
    finally
      FmOVgItxGerBtl.Destroy;
    end;
  end;
end;

procedure TUnYRO_Jan.MostraFormOVgItxGerCab(Codigo: Integer);
begin
  if DBCheck.CriaFm(TFmOVgItxGerCab, FmOVgItxGerCab, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
      FmOVgItxGerCab.LocCod(Codigo, Codigo);
    FmOVgItxGerCab.ShowModal;
    FmOVgItxGerCab.Destroy;
  end;
end;

function TUnYRO_Jan.MostraFormOVgItxGerCad(SQLType: TSQLType; Codigo,
  Local: Integer; NO_Local: String; NrOP, SeqGrupo: Integer;
  NO_SeqGrupo: String; NrReduzidoOP, OVcYnsExg, PermiFinHow, ZtatusIsp:
  Integer): Integer;
begin
  Result := 0;
  if DBCheck.CriaFm(TFmOVgItxGerCad, FmOVgItxGerCad, afmoNegarComAviso) then
  begin
    FmOVgItxGerCad.ImgTipo.SQLType             := SQLType;
    FmOVgItxGerCad.FSeqGrupo                   := SeqGrupo;
    FmOVgItxGerCad.EdLocal.ValueVariant        := Local;
    FmOVgItxGerCad.EdNO_Local.ValueVariant     := NO_Local;
    FmOVgItxGerCad.EdNrOP.ValueVariant         := NrOP;
    FmOVgItxGerCad.EdSeqGrupo.ValueVariant     := SeqGrupo;
    FmOVgItxGerCad.EdNO_SeqGrupo.ValueVariant  := NO_SeqGrupo;
    FmOVgItxGerCad.EdNrReduzidoOP.ValueVariant := NrReduzidoOP;
    FmOVgItxGerCad.ReopenOVcYnsExgCad(SeqGrupo, True);
    if SQLTYpe = stUpd then
    begin
      FmOVgItxGerCad.EdCodigo.ValueVariant := Codigo;
      FmOVgItxGerCad.EdOVcYnsExg.ValueVariant := OVcYnsExg;
      FmOVgItxGerCad.CBOVcYnsExg.KeyValue     := OVcYnsExg;
(*
      FmOVgItxGerCad.EdOVcYnsChk.ValueVariant := OVcYnsChk;
      FmOVgItxGerCad.CBOVcYnsChk.KeyValue     := OVcYnsChk;
      FmOVgItxGerCad.EdOVcYnsARQ.ValueVariant := OVcYnsARQ;
      FmOVgItxGerCad.CBOVcYnsARQ.KeyValue     := OVcYnsARQ;
      FmOVgItxGerCad.EdLimiteMed.ValueVariant := LimiteMed;
      FmOVgItxGerCad.EdLimiteChk.ValueVariant := LimiteChk;
*)
      FmOVgItxGerCad.RGPermiFinHow.ItemIndex  := PermiFinHow;
    end;
    FmOVgItxGerCad.FZtatusIsp := ZtatusIsp; //CO_O V S_IMPORT_ALHEIO_3972_CONFIGURADO
    //
    FmOVgItxGerCad.ShowModal;
    Result := FmOVgItxGerCad.FCodigo;
    FmOVgItxGerCad.Destroy;
  end;
end;

procedure TUnYRO_Jan.MostraFormOVgItxPrfCab(Codigo: Integer);
begin
  if DBCheck.CriaFm(TFmOVgItxPrfCab, FmOVgItxPrfCab, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
      FmOVgItxPrfCab.LocCod(Codigo, Codigo);
    FmOVgItxPrfCab.ShowModal;
    FmOVgItxPrfCab.Destroy;
  end;
end;

function TUnYRO_Jan.MostraFormOVgItxPrfCad(SQLType: TSQLType; Codigo,
  SeqGrupo: Integer; NO_SeqGrupo, Referencia: String; OVcYnsExg, PermiFinHow,
  Ativo: Integer; Nome: String): Integer;
begin
  Result := 0;
  if DBCheck.CriaFm(TFmOVgItxPrfCad, FmOVgItxPrfCad, afmoNegarComAviso) then
  begin
    FmOVgItxPrfCad.ImgTipo.SQLType             := SQLType;
    FmOVgItxPrfCad.FSeqGrupo                   := SeqGrupo;
    FmOVgItxPrfCad.EdSeqGrupo.ValueVariant     := SeqGrupo;
    FmOVgItxPrfCad.EdNO_SeqGrupo.ValueVariant  := NO_SeqGrupo;
    FmOVgItxPrfCad.EdReferencia.ValueVariant   := Referencia;
    FmOVgItxPrfCad.ReopenOVcYnsExgCad(SeqGrupo, True);
    if SQLTYpe = stUpd then
    begin
      FmOVgItxPrfCad.EdCodigo.ValueVariant := Codigo;
      FmOVgItxPrfCad.EdOVcYnsExg.ValueVariant := OVcYnsExg;
      FmOVgItxPrfCad.CBOVcYnsExg.KeyValue     := OVcYnsExg;
(*
      FmOVgItxPrfCad.EdOVcYnsChk.ValueVariant := OVcYnsChk;
      FmOVgItxPrfCad.CBOVcYnsChk.KeyValue     := OVcYnsChk;
      FmOVgItxPrfCad.EdOVcYnsARQ.ValueVariant := OVcYnsARQ;
      FmOVgItxPrfCad.CBOVcYnsARQ.KeyValue     := OVcYnsARQ;
      FmOVgItxPrfCad.EdLimiteMed.ValueVariant := LimiteMed;
      FmOVgItxPrfCad.EdLimiteChk.ValueVariant := LimiteChk;
*)
      FmOVgItxPrfCad.RGPermiFinHow.ItemIndex  := PermiFinHow;
      FmOVgItxPrfCad.CkAtivo.Checked          := Geral.IntToBool(Ativo);
      FmOVgItxPrfCad.EdNome.ValueVariant      := Nome;
    end;
    //
    FmOVgItxPrfCad.ShowModal;
    Result := FmOVgItxPrfCad.FCodigo;
    FmOVgItxPrfCad.Destroy;
  end;
end;

procedure TUnYRO_Jan.MostraFormOVmIspDevCab(Codigo: Integer);
begin
  if DBCheck.CriaFm(TFmOVmIspDevCab, FmOVmIspDevCab, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
      FmOVmIspDevCab.LocCod(Codigo, Codigo);
    FmOVmIspDevCab.ShowModal;
    FmOVmIspDevCab.Destroy;
  end;
end;

procedure TUnYRO_Jan.MostraFormOVmItxDevCab(Codigo: Integer);
begin
  if DBCheck.CriaFm(TFmOVmItxDevCab, FmOVmItxDevCab, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
      FmOVmItxDevCab.LocCod(Codigo, Codigo);
    FmOVmItxDevCab.ShowModal;
    FmOVmItxDevCab.Destroy;
  end;
end;
}

procedure TUnYRO_Jan.MostraFormOVpLayEsq(Esquema: Integer);
begin
(*&�%$#@!"
  if DBCheck.CriaFm(TFmOVpLayEsq, FmOVpLayEsq, afmoNegarComAviso) then
  begin
    if Esquema <> 0 then
      FmOVpLayEsq.LocCod(Esquema, Esquema);
    FmOVpLayEsq.ShowModal;
    FmOVpLayEsq.Destroy;
  end;
*)
end;

{
procedure TUnYRO_Jan.MostraFormSeccTexTinturaria(Forca: Boolean);
begin
  if DBCheck.CriaFm(TFmSeccTexTinturaria, FmSeccTexTinturaria, afmoNegarComAviso) then
  begin
    FmSeccTexTinturaria.ReopenItensAPosConfig();
    FmSeccTexTinturaria.DefineCordaInicial();
    if (FmSeccTexTinturaria.QrAptos.RecordCount > 0) or Forca then
      FmSeccTexTinturaria.ShowModal;
    FmSeccTexTinturaria.Destroy;
  end;
end;
}

end.
