object FmOpcoesApp: TFmOpcoesApp
  Left = 339
  Top = 185
  Caption = 'APP-OPCAO-000 :: Op'#231#245'es Espec'#237'ficas do Sistema'
  ClientHeight = 657
  ClientWidth = 767
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 767
    Height = 501
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object PCGeral: TPageControl
      Left = 0
      Top = 0
      Width = 767
      Height = 501
      ActivePage = TabSheet1
      Align = alClient
      TabOrder = 0
      object TabSheet2: TTabSheet
        Caption = 'Importa'#231#227'o de dados'
        ImageIndex = 1
        object Panel6: TPanel
          Left = 0
          Top = 0
          Width = 759
          Height = 473
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          object Panel2: TPanel
            Left = 0
            Top = 0
            Width = 759
            Height = 89
            Align = alTop
            Enabled = False
            TabOrder = 0
            object Label1: TLabel
              Left = 4
              Top = 4
              Width = 80
              Height = 13
              Caption = 'IP arquivos CSV:'
              Enabled = False
            end
            object Label3: TLabel
              Left = 4
              Top = 44
              Width = 163
              Height = 13
              Caption = 'Esquema de layout de importa'#231#227'o:'
              Enabled = False
            end
            object SbOVpLayEsq: TSpeedButton
              Left = 596
              Top = 59
              Width = 23
              Height = 23
              Caption = '...'
              Enabled = False
              OnClick = SbOVpLayEsqClick
            end
            object SpeedButton2: TSpeedButton
              Left = 728
              Top = 20
              Width = 23
              Height = 22
              Enabled = False
              OnClick = SpeedButton2Click
            end
            object Label11: TLabel
              Left = 624
              Top = 44
              Width = 115
              Height = 13
              Caption = 'Intervalo scan (minutos):'
              Enabled = False
            end
            object Label2: TLabel
              Left = 188
              Top = 4
              Width = 175
              Height = 13
              Caption = 'Caminho da pasta dos arquivos CSV:'
              Enabled = False
            end
            object EdLoadCSVOthIP: TEdit
              Left = 4
              Top = 20
              Width = 181
              Height = 21
              Enabled = False
              TabOrder = 0
            end
            object EdLoadCSVOthDir: TEdit
              Left = 188
              Top = 20
              Width = 537
              Height = 21
              Enabled = False
              TabOrder = 1
            end
            object EdOVpLayEsq: TdmkEditCB
              Left = 4
              Top = 60
              Width = 56
              Height = 21
              Alignment = taRightJustify
              Enabled = False
              TabOrder = 2
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              DBLookupComboBox = CBOVpLayEsq
              IgnoraDBLookupComboBox = False
              AutoSetIfOnlyOneReg = setregOnlyManual
            end
            object CBOVpLayEsq: TdmkDBLookupComboBox
              Left = 62
              Top = 60
              Width = 531
              Height = 21
              Enabled = False
              KeyField = 'Codigo'
              ListField = 'Nome'
              ListSource = DsOVpLayEsq
              TabOrder = 3
              dmkEditCB = EdOVpLayEsq
              UpdType = utYes
              LocF7SQLMasc = '$#'
              LocF7PreDefProc = f7pNone
            end
            object EdLoadCSVIntrv: TdmkEdit
              Left = 624
              Top = 60
              Width = 125
              Height = 21
              Alignment = taRightJustify
              Enabled = False
              TabOrder = 4
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
            end
          end
        end
      end
      object TabSheet3: TTabSheet
        Caption = 'Outras pastas'
        ImageIndex = 1
      end
      object TabSheet1: TTabSheet
        Caption = 'Miscel'#226'nea'
        ImageIndex = 2
        object Panel10: TPanel
          Left = 0
          Top = 0
          Width = 759
          Height = 473
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          object Label10: TLabel
            Left = 15
            Top = 69
            Width = 69
            Height = 13
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Tipo de e-mail:'
          end
          object SpeedButton1: TSpeedButton
            Left = 635
            Top = 89
            Width = 26
            Height = 25
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = '...'
            OnClick = SpeedButton2Click
          end
          object Panel11: TPanel
            Left = 0
            Top = 0
            Width = 759
            Height = 317
            Align = alTop
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 0
            ExplicitLeft = 4
            object Label9: TLabel
              Left = 8
              Top = 8
              Width = 129
              Height = 13
              Caption = 'Nome amig'#225'vel do sistema:'
            end
            object Label4: TLabel
              Left = 8
              Top = 48
              Width = 244
              Height = 13
              Caption = 'Conta padr'#227'o do plano de contas para vendas: [F4]'
            end
            object EdERPNameByCli: TdmkEdit
              Left = 8
              Top = 24
              Width = 205
              Height = 21
              TabOrder = 0
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              QryCampo = 'ERPNameByCli'
              UpdCampo = 'ERPNameByCli'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object EdPlaConCtaVen_TXT: TdmkEditEdTxt
              Left = 64
              Top = 64
              Width = 300
              Height = 21
              TabStop = False
              ReadOnly = True
              TabOrder = 1
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object EdPlaConCtaVen: TdmkEditEdIdx
              Left = 8
              Top = 64
              Width = 56
              Height = 21
              Alignment = taRightJustify
              TabOrder = 2
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              DmkEditEdTxt = EdPlaConCtaVen_TXT
              IgnoradmkEdit = False
              AutoSetIfOnlyOneReg = setregOnlyManual
              LocF7CodiFldName = 'Codigo'
              LocF7NameFldName = 'Nome'
              LocF7TableName = 'contas'
              LocF7SQLMasc = '$#'
              LocF7PreDefProc = f7pNone
              LocF7DmkDataBase = ddluMyDB
            end
          end
        end
      end
      object TabSheet6: TTabSheet
        Caption = 'Usu'#225'rios'
        ImageIndex = 3
        object Panel16: TPanel
          Left = 0
          Top = 425
          Width = 759
          Height = 48
          Align = alBottom
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          object BtExclui: TBitBtn
            Tag = 12
            Left = 252
            Top = 4
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Caption = '&Exclui'
            Enabled = False
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 2
            OnClick = BtExcluiClick
          end
          object BtAltera: TBitBtn
            Tag = 11
            Left = 128
            Top = 4
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Caption = '&Altera'
            Enabled = False
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 1
            OnClick = BtAlteraClick
          end
          object BtInclui: TBitBtn
            Tag = 10
            Left = 4
            Top = 4
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Caption = '&Inclui'
            Enabled = False
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtIncluiClick
          end
        end
        object DBGOpcoesApU: TdmkDBGridZTO
          Left = 0
          Top = 0
          Width = 759
          Height = 425
          Align = alClient
          DataSource = DsOpcoesApU
          Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
          TabOrder = 1
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          RowColors = <>
          Columns = <
            item
              Expanded = False
              FieldName = 'Codigo'
              Title.Caption = 'ID'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Login'
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'NO_HabFaccao'
              Title.Caption = 'Fac'#231#227'o'
              Width = 44
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_HabFacConfecaocao'
              Title.Caption = 'Confec'#231#227'o'
              Width = 44
              Visible = True
            end
            item
              Alignment = taCenter
              Expanded = False
              FieldName = 'NO_HabTextil'
              Title.Caption = 'T'#234'xtil'
              Width = 44
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_HabTexTecelagem'
              Title.Caption = 'Tecelagem'
              Width = 44
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_HabTexTinturaria'
              Title.Caption = 'Tinturaria'
              Width = 44
              Visible = True
            end>
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 767
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 719
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 671
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 384
        Height = 32
        Caption = 'Op'#231#245'es Espec'#237'ficas do Sistema'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 384
        Height = 32
        Caption = 'Op'#231#245'es Espec'#237'ficas do Sistema'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 384
        Height = 32
        Caption = 'Op'#231#245'es Espec'#237'ficas do Sistema'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 549
    Width = 767
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel8: TPanel
      Left = 2
      Top = 15
      Width = 763
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 593
    Width = 767
    Height = 64
    Align = alBottom
    TabOrder = 3
    object Panel9: TPanel
      Left = 2
      Top = 15
      Width = 763
      Height = 47
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object PnSaiDesis: TPanel
        Left = 619
        Top = 0
        Width = 144
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtSaida: TBitBtn
          Tag = 15
          Left = 6
          Top = 3
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Hint = 'Sai da janela atual'
          Caption = '&Desiste'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
        end
      end
      object BtOK: TBitBtn
        Tag = 14
        Left = 14
        Top = 3
        Width = 119
        Height = 40
        Caption = '&Salva'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object QrOVpLayEsq: TMySQLQuery
    Database = Dmod.ZZDB
    SQL.Strings = (
      'SELECT Codigo, Nome '
      'FROM ovplayesq'
      'ORDER BY Nome')
    Left = 538
    Top = 24
    object QrOVpLayEsqCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrOVpLayEsqNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
  end
  object DsOVpLayEsq: TDataSource
    DataSet = QrOVpLayEsq
    Left = 538
    Top = 72
  end
  object QrEntiTipCto: TMySQLQuery
    Database = Dmod.ZZDB
    SQL.Strings = (
      'SELECT Codigo,CodUsu, Nome '
      'FROM entitipcto'
      'ORDER BY Nome')
    Left = 630
    Top = 28
    object QrEntiTipCtoCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrEntiTipCtoCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEntiTipCtoNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
  end
  object DsEntiTipCto: TDataSource
    DataSet = QrEntiTipCto
    Left = 630
    Top = 76
  end
  object dmkValUsu1: TdmkValUsu
    QryCampo = 'EntiTipCto'
    UpdCampo = 'EntiTipCto'
    RefCampo = 'Codigo'
    UpdType = utYes
    Left = 64
    Top = 12
  end
  object QrOVpDirXtr: TMySQLQuery
    Database = Dmod.ZZDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM ovpdirxtr'
      'ORDER BU Nome')
    Left = 392
    Top = 108
    object QrOVpDirXtrCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrOVpDirXtrNome: TWideStringField
      FieldName = 'Nome'
      Size = 255
    end
  end
  object DsOVpDirXtr: TDataSource
    DataSet = QrOVpDirXtr
    Left = 392
    Top = 156
  end
  object QrOpcoesApU: TMySQLQuery
    Database = Dmod.ZZDB
    AfterOpen = QrOpcoesApUAfterOpen
    BeforeClose = QrOpcoesApUBeforeClose
    SQL.Strings = (
      'SELECT apu.Codigo, apu.HabFaccao, apu.HabTextil, '
      'apu.HabFacConfeccao, '
      'apu.HabTexTecelagem, apu.HabTexTinturaria, '
      'IF(apu.HabFaccao=1, "SIM", "N'#195'O") NO_HabFaccao, '
      'IF(apu.HabFacConfeccao=1, "SIM", "N'#195'O") NO_HabFacConfecaocao, '
      'IF(apu.HabTextil=1, "SIM", "N'#195'O") NO_HabTextil, '
      'IF(apu.HabTexTecelagem=1, "SIM", "N'#195'O") NO_HabTexTecelagem, '
      'IF(apu.HabTexTinturaria=1, "SIM", "N'#195'O") NO_HabTexTinturaria, '
      'pwd.Login '
      'FROM opcoesapu apu '
      'LEFT JOIN senhas pwd ON pwd.Numero=apu.Codigo ')
    Left = 146
    Top = 216
    object QrOpcoesApUCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrOpcoesApUHabFaccao: TSmallintField
      FieldName = 'HabFaccao'
      Required = True
    end
    object QrOpcoesApUHabTextil: TSmallintField
      FieldName = 'HabTextil'
      Required = True
    end
    object QrOpcoesApUNO_HabFaccao: TWideStringField
      FieldName = 'NO_HabFaccao'
      Required = True
      Size = 3
    end
    object QrOpcoesApUNO_HabTextil: TWideStringField
      FieldName = 'NO_HabTextil'
      Required = True
      Size = 3
    end
    object QrOpcoesApULogin: TWideStringField
      FieldName = 'Login'
      Size = 30
    end
    object QrOpcoesApUHabFacConfeccao: TSmallintField
      FieldName = 'HabFacConfeccao'
      Required = True
    end
    object QrOpcoesApUHabTexTecelagem: TSmallintField
      FieldName = 'HabTexTecelagem'
      Required = True
    end
    object QrOpcoesApUHabTexTinturaria: TSmallintField
      FieldName = 'HabTexTinturaria'
      Required = True
    end
    object QrOpcoesApUNO_HabFacConfecaocao: TWideStringField
      FieldName = 'NO_HabFacConfecaocao'
      Required = True
      Size = 3
    end
    object QrOpcoesApUNO_HabTexTecelagem: TWideStringField
      FieldName = 'NO_HabTexTecelagem'
      Required = True
      Size = 3
    end
    object QrOpcoesApUNO_HabTexTinturaria: TWideStringField
      FieldName = 'NO_HabTexTinturaria'
      Required = True
      Size = 3
    end
  end
  object DsOpcoesApU: TDataSource
    DataSet = QrOpcoesApU
    Left = 146
    Top = 264
  end
  object QrContas: TMySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrOpcoesApUAfterOpen
    BeforeClose = QrOpcoesApUBeforeClose
    SQL.Strings = (
      'SELECT Codigo, Nome '
      'FROM contas'
      'WHERE Codigo > 0'
      'AND Credito = "V"'
      'ORDER BY Nome')
    Left = 346
    Top = 248
    object QrContasCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrContasNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
  end
  object DsContas: TDataSource
    DataSet = QrContas
    Left = 346
    Top = 296
  end
end
