unit UnImporta_PF;

interface

uses
{
  Forms, StdCtrls, ComCtrls, ExtCtrls, Windows, Messages, SysUtils, Classes,
  Graphics, Controls, Dialogs, Variants, MaskUtils, Registry, ShellAPI, TypInfo,
{
  comobj, ShlObj, Winsock, Math, About, UnMsgInt, mySQLDbTables, DB,
  Vcl.DBCtrls, System.Rtti, dmkEdit, dmkEditCB,
  mySQLExceptions, UnInternalConsts, dmkDBGrid, UnDmkProcFunc,
  UnDmkEnums, UnGrl_Vars, mySQLDirectQuery;
}
  System.Json, (* uLkJSON, UnMyJson, *)
  System.SysUtils, System.Types, System.Classes,
  StdCtrls,
  //Forms,
  Dialogs,
  Variants,
  dmkGeral, UnDMkEnums, UnProjGroupEnums;

(*
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkPermissoes,
  mySQLDbTables, dmkDBLookupComboBox, dmkEditCB, mySQLDirectQuery,
  UnProjGroup_PF;
*)
type
  TUnImporta_PF = class(TObject)
  private
    {private declaration}
(*
    FIniTick, FFimTick, FDifTick: DWORD;
    FCodLayout, FCodLog: Integer;
    FDataHora_TXT: String;
    FDataHora_DTH: TDateTime;
*)
  public
    {public declaration}
    // 2020-10-31
    //procedure AtualizaLog(Status: Integer; Nome, Campo: String; ValInt: Integer);
    procedure CarregaArquivoParaLayout((*Form: TForm;*) var CodLayout: Integer;
              const Arquivo: String; var Tabela: String; const LaAviso1,
              LaAviso2: TLabel; MeAvisos: TMemo; var LstArq, LstLin1, LstLin2,
              LstLin3, LstLin4, LstLin5, LstLin6, LstLin7: TStringList);
    procedure Informa(MeAvisos: TMemo; Texto: String);
    function  InsereTabelaOVPLayEsq(var Nome: String; var Codigo: Integer):
              Boolean;
    function  InsereTabelaOVPLayTab(const Codigo: Integer; var Controle:
              Integer; const Nome: String; var Tabela: String; const MeAvisos:
              TMemo): Boolean;
    procedure InsereTabelaOVPLayFld(const Codigo, Controle: Integer; var Conta:
              Integer; const Campo, Nome, DataType, Tamanho, FldNull, FldDefault,
              FldKey: String; var Coluna: Integer);

  end;
var
  Importa_PF: TUnImporta_PF;
  //
const
  _STATUS_000 =    0; // In�cio da importa��o
  _STATUS_100 =  100; // Importa��o de Layout
  _STATUS_200 =  200; // Importa��o de movimento
  _STATUS_300 =  300; // Extraindo dados secund�rios
  _STATUS_400 =  400; // Extra��o finalizada
  _STATUS_500 =  500; // Definindo itens relevantes: Faccoes
  _STATUS_600 =  600; // Definindo itens relevantes: Texteis
  _STATUS_700 =  700; // Define itens encerrados
  _STATUS_800 =  800; // Reabrindo itens encerrados
  _STATUS_999 =  999; // Importa��o finalizada


implementation

uses UMySQLModule, UnYRO_Consts, DmkDAC_PF, MyDBCheck,
  Module, ModuleGeral, UnMyObjects;



{ TUnImporta_PF }

(*
procedure TUnImporta_PF.AtualizaLog(Status: Integer; Nome, Campo: String;
  ValInt: Integer);
var
  DataHora: String;
  Codigo: Integer;
  SQLType: TSQLType;
begin
  if FCodLog = 0 then
  begin
    SQLType := stIns;
    Codigo := UMyMod.BPGS1I32('ovpimplog', 'Codigo', '', '', tsPos, stIns, 0);
    FCodLog := Codigo;
  end else
  begin
    Codigo  := FCodLog;
    SQLType := stUpd;
  end;
  //Codigo         := ;
  //Nome           := ;
  //Status         := ;
  DataHora       := FDataHora_TXT;
  //
  //if
  if Campo <> EmptyStr then
  begin
    UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'ovpimplog', False, [
    'Nome', 'Status', 'DataHora',
    Campo], [
    'Codigo'], [
    Nome, Status, DataHora,
    ValInt], [
    Codigo], True);
  end else
  begin
    UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'ovpimplog', False, [
    'Nome', 'Status', 'DataHora'], ['Codigo'], [
    Nome, Status, DataHora], [Codigo], True);
  end;
end;
*)

procedure TUnImporta_PF.CarregaArquivoParaLayout((*Form: TForm; *)var CodLayout:
  Integer; const Arquivo: String; var Tabela: String; const LaAviso1, LaAviso2:
  TLabel; MeAvisos: TMemo; var LstArq, LstLin1, LstLin2, LstLin3, LstLin4,
  LstLin5, LstLin6, LstLin7: TStringList);
var
  I, Controle, Conta, Coluna: Integer;
  Linha, CodTxt, Nome, DataIni, DataFim, NomeEsq, NomeArq, NomeTab, NomeFld,
  Campo, DataType: String;
  Tamanho, FldType, FldNull, FldDefault, FldKey: String;
  //LstArq, LstLin1, LstLin2, LstLin3: TStringList;
  Continua: Boolean;
begin
  //AtualizaLog(_STATUS_100, '', EmptyStr, 0);
  //FItens := 0;
  if FileExists(Arquivo) then
  begin
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Abrindo arquivo: ' + Arquivo + '.');
    LstArq := TStringList.Create;
    try
      LstLin1 := TStringList.Create;
      try
        LstLin2 := TStringList.Create;
        try
          LstLin3 := TStringList.Create;
          try
            LstLin4 := TStringList.Create;
            try
              LstLin5 := TStringList.Create;
              try
                LstLin4 := TStringList.Create;
                try
                  LstLin5 := TStringList.Create;
                  try
                    LstArq.LoadFromFile(Arquivo);
                    if lstArq.Count > 0 then
                    begin
                      Linha := ';' + LstArq[0] + ';';
                      //
                      LstLin1 := Geral.Explode3(Linha, ';');
                      if LstLin1.Count > 0 then
                      begin
                        Linha := ';' + LstArq[1] + ';';
                        LstLin2 := Geral.Explode3(Linha, ';');
                        if LstLin2.Count > 0 then
                        begin
                          if LstLin1.Count <> LstLin2.Count then
                          begin
                            Informa(MeAvisos, '[CAPL] Quantidade de Colunas difere (2): ' +
                              sLineBreak + Arquivo);
                            Exit;
                          end;

                          Linha := ';' + LstArq[2] + ';';
                          LstLin3 := Geral.Explode3(Linha, ';');
                          if LstLin3.Count > 0 then
                          begin
                            if LstLin1.Count <> LstLin3.Count then
                            begin
                              Informa(MeAvisos, '[CAPL] Quantidade de Colunas difere (3): ' +
                                sLineBreak + Arquivo);
                              Exit;
                            end;



                            Linha := ';' + LstArq[3] + ';';
                            LstLin4 := Geral.Explode3(Linha, ';');
                            if LstLin4.Count > 0 then
                            begin
                              if LstLin1.Count <> LstLin4.Count then
                              begin
                                Informa(MeAvisos, '[CAPL] Quantidade de Colunas difere (4): ' +
                                  sLineBreak + Arquivo);
                                Exit;
                              end;



                              Linha := ';' + LstArq[4] + ';';
                              LstLin5 := Geral.Explode3(Linha, ';');
                              if LstLin5.Count > 0 then
                              begin
                                if LstLin1.Count <> LstLin5.Count then
                                begin
                                  Informa(MeAvisos, '[CAPL] Quantidade de Colunas difere (5): ' +
                                    sLineBreak + Arquivo);
                                  Exit;
                                end;





                                Linha := ';' + LstArq[5] + ';';
                                LstLin6 := Geral.Explode3(Linha, ';');
                                if LstLin6.Count > 0 then
                                begin
                                  if LstLin1.Count <> LstLin6.Count then
                                  begin
                                    Informa(MeAvisos, '[CAPL] Quantidade de Colunas difere (6): ' +
                                      sLineBreak + Arquivo);
                                    Exit;
                                  end;



                                  Linha := ';' + LstArq[6] + ';';
                                  LstLin7 := Geral.Explode3(Linha, ';');
                                  if LstLin7.Count > 0 then
                                  begin
                                    if LstLin1.Count <> LstLin7.Count then
                                    begin
                                      Informa(MeAvisos, '[CAPL] Quantidade de Colunas difere (7): ' +
                                        sLineBreak + Arquivo);
                                      Exit;
                                    end;


                                    NomeEsq  := '';
                                    Controle := 0;
                                    NomeArq  := ExtractFileName(Arquivo);
                                    NomeTab  := '';
                                    if CodLayout = 0 then
                                      Continua := InsereTabelaOVPLayEsq(NomeEsq, CodLayout)
                                    else
                                      Continua := True;
                                    if Continua then
                                    begin
                                      if InsereTabelaOVPLayTab(CodLayout, Controle, NomeArq, NomeTab, MeAvisos) then
                                      begin
                                        for I := 0 to LstLin1.Count - 1 do
                                        begin
                                          Conta      := 0;
                                          Coluna     := I + 1;
                                          NomeFld    := LstLin1[I];
                                          //NomeFld  := UTF8ToWIdeString(LstLin1[I]);
                                          Campo      := LstLin2[I];
                                          DataType   := LstLin3[I];
                                          Tamanho    := LstLin4[I];
                                          //FldType,
                                          FldNull    := LstLin5[I];
                                          FldDefault := LstLin6[I];
                                          FldKey     := LstLin7[I];
                                          //
                                          InsereTabelaOVPLayFld(CodLayout, Controle, Conta, Campo,
                                            NomeFld, DataType,
                                            Tamanho, FldNull, FldDefault, FldKey,
                                            Coluna);
                                        end;
                                      end else
                                        Exit;
                                    end;
                                  end;
                                end;
                              end;
                            end;
                          end;
                        end;
                      end;
                    end;
                    //
                    MyObjects.Informa2(LaAviso1, LaAviso2, False, Geral.FDT(
                      Dmod.DModG_ObtemAgora(), 2) + ' Obten��o layout finalizado do arquivo: ' +
                      Arquivo);
                    //PB.Position := 0;
                  finally
                    if LstLin7 <> nil then
                      LstLin7.Free;
                  end;
                finally
                  if LstLin6 <> nil then
                    LstLin6.Free;
                end;
              finally
                if LstLin5 <> nil then
                  LstLin5.Free;
              end;
            finally
              if LstLin4 <> nil then
                LstLin4.Free;
            end;
          finally
            if LstLin3 <> nil then
              LstLin3.Free;
          end;
        finally
          if LstLin2 <> nil then
            LstLin2.Free;
        end;
      finally
        if LstLin1 <> nil then
          LstLin1.Free;
      end;
    finally
      if LstArq <> nil then
        LstArq.Free;
    end;
  end;
end;

procedure TUnImporta_PF.Informa(MeAvisos: TMemo; Texto: String);
begin
  if MeAvisos <> nil then
    MeAvisos.Text := Geral.FDT(Dmod.DModG_ObtemAgora(), 2) + ' ' + Texto +
      sLineBreak + MeAvisos.Text;
end;

function TUnImporta_PF.InsereTabelaOVPLayEsq(var Nome: String;
  var Codigo: Integer): Boolean;
var
  SQLType: TSQLType;
begin
  Result := False;
  if Trim(Nome) = '' then
    InputQuery('Nome do Esquema de Layout',
    'Informe o nome do esquema do layout:', Nome);
  if Codigo = 0 then
    SQLType := TSQLType.stIns
  else
    SQLType := TSQLType.stUpd;
  //
  Codigo := UMyMod.BPGS1I32('ovplayesq', 'Codigo', '', '', tsPos, SQLType, Codigo);
  if Trim(Nome) = '' then
    Nome := 'Esquema de layout ' + Geral.FF0(Codigo);
  //
  Result :=  UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'ovplayesq', False, [
    'Nome'], ['Codigo'], [
    Nome], [Codigo], True);
end;

procedure TUnImporta_PF.InsereTabelaOVPLayFld(const Codigo, Controle: Integer;
  var Conta: Integer; const Campo, Nome, DataType, Tamanho, FldNull, FldDefault,
  FldKey: String; var Coluna: Integer);
{
var
  SQLType: TSQLType;
begin
  if Conta = 0 then
    SQLType := TSQLType.stIns
  else
    SQLType := TSQLType.stUpd;
  //Codigo         := ;
  //Controle       := ;
  //Conta          := ;
  //Coluna         := ;
  //Campo          := ;
  //Nome           := ;
  Conta := UMyMod.BPGS1I32('ovplayfld', 'Conta', '', '', tsPos, SQLType, Conta);
  //if
  UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'ovplayfld', False, [
  'Codigo', 'Controle', 'Coluna',
  'Campo', 'Nome', 'DataType'], [
  'Conta'], [
  Codigo, Controle, Coluna,
  Campo, Nome, DataType], [
  Conta], True);
}
var
  FldType: String;
  SQLType: TSQLType;
  EhData, EhHora: Boolean;
begin
  SQLType        := stIns;
  //
  if Uppercase(DataType) = 'C' then
  begin
    if Geral.IMV(Tamanho) > 4 then
      FldType := 'varchar(' + Tamanho + ')'
    else
      FldType := 'char(' + Tamanho + ')';
  end
  else
  if Uppercase(DataType) = 'D' then
  begin
    EhHora := (pos(':', FldDefault)) > 0;
    EhData := (pos('-', FldDefault)) > 0;
    if (pos('/', FldDefault)) > 0 then
      FldType := 'err/date'
    else
    if EhHora and EhData then
      FldType := 'datetime'
    else
    if EhData then
      FldType := 'date'
    else
    if EhHora then
      FldType := 'time'
    else
      FldType := 'dt??';
  end
  else
  if Uppercase(DataType) = 'I' then
  begin
    if Geral.IMV(Tamanho) > 3 then
      FldType := 'int(' + Tamanho + ')'
    else
      FldType := 'tinyint(' + Tamanho + ')';
  end
  else
  if Uppercase(DataType) = 'N' then
  begin
    FldType := 'double(' + Tamanho + ')';
  end
  else
    FldType := '????';

  //
  Conta := UMyMod.BPGS1I32('ovplayfld', 'Conta', '', '', tsPos, SQLType, Conta);
  //
  //if
  UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'ovplayfld', False, [
  'Codigo', 'Controle', 'Coluna',
  'Campo', 'Nome', 'DataType',
  'Tamanho', 'FldType', 'FldNull',
  'FldDefault', 'FldKey'], [
  'Conta'], [
  Codigo, Controle, Coluna,
  Campo, Nome, DataType,
  Tamanho, FldType, FldNull,
  FldDefault, FldKey], [
  Conta], True);
end;

function TUnImporta_PF.InsereTabelaOVPLayTab(const Codigo: Integer;
  var Controle: Integer; const Nome: String; var Tabela: String; const MeAvisos:
  TMemo): Boolean;
const
  cArquivos: array[0..10] of String = ('',
  'DPESSOAS.csv',
  'DCICLO.csv',
  'DCLASLOCAL.csv',
  'DEMPRESA.csv',
  'DLOCAL.csv',
  //'DLOTE.csv',
  'DLOTEPRODUCAO.csv',
  'DPRODUTO.csv',
  'DREFERENCIA.csv',
  //'FORDEMPRODUCAO.csv');
  'FORDENSPRODUCAO.csv',
  'FTRANSACAO.csv'
  );
  cTabelas: array[0..10] of String = ('',
  'ovDPessoas',
  'ovDCiclo',
  'ovDClasLocal',
  'ovDEmpresa',
  'ovDLocal',
  'ovDLote',
  'ovDProduto',
  'ovDReferencia',
  'ovFOrdemProducao',
  'ovFTransacao'
  );
var
  P: Integer;
  SQLType: TSQLType;
begin
  if Controle = 0 then
    SQLType := TSQLType.stIns
  else
    SQLType := TSQLType.stUpd;
  //
  if Trim(Nome) = '' then
  begin
    Informa(MeAvisos, '[ITTab] Nome tabela indefinido');
    Exit;
  end;
  //
  if Trim(Tabela) = '' then
  begin
    //P := pos(Nome, cTabelas);
    for P := 0 to Length(cArquivos) - 1 do
    begin
      if Lowercase(cArquivos[P]) = Lowercase(Nome) then
      begin
        Tabela := cTabelas[P];
        Break;
      end;
    end;
  end;
(*
  DPESSOAS.csv
  DCICLO.csv
  DCLASLOCAL.csv
  DEMPRESA.csv
  DLOCAL.csv
  DLOTE.csv
  DPRODUTO.csv
  DREFERENCIA.csv
  FORDEMPRODUCAO.csv
*)
  //Codigo         := ;
  //Controle       := ;
  //Nome           := ;
  //Tabela         := ;

  //
  if Trim(Tabela) <> EmptyStr then
  begin
    Controle := UMyMod.BPGS1I32('ovplaytab', 'Controle', '', '', tsPos, SQLType, Controle);
    //
    Result := UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'ovplaytab', False, [
    'Codigo', 'Nome', 'Tabela'], [
    'Controle'], [
    Codigo, Nome, Tabela], [
    Controle], True);
  end else
  begin
    Result := False;
    Geral.MB_Aviso('Nome de arquivo n�o reconhecido ou n�o implementado!' +
    sLineBreak + 'Arquivo: ' + Nome);
  end;
end;

end.
