object FmYOcMobDevNew: TFmYOcMobDevNew
  Left = 339
  Top = 185
  Caption = 'YRO-DEVIC-004 :: Aceite de Equipamento Mobile'
  ClientHeight = 266
  ClientWidth = 573
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 573
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 525
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 477
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 369
        Height = 32
        Caption = 'Aceite de Equipamento Mobile'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 369
        Height = 32
        Caption = 'Aceite de Equipamento Mobile'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 369
        Height = 32
        Caption = 'Aceite de Equipamento Mobile'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 573
    Height = 104
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 573
      Height = 104
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 573
        Height = 104
        Align = alClient
        TabOrder = 0
        object GBDados: TGroupBox
          Left = 2
          Top = 15
          Width = 569
          Height = 145
          Align = alTop
          Color = clBtnFace
          ParentBackground = False
          ParentColor = False
          TabOrder = 0
          object Label16: TLabel
            Left = 12
            Top = 4
            Width = 25
            Height = 13
            Caption = 'IMEI:'
            Color = clBtnFace
            ParentColor = False
          end
          object Label17: TLabel
            Left = 196
            Top = 4
            Width = 98
            Height = 13
            Caption = 'Nome do dispositivo:'
            Color = clBtnFace
            ParentColor = False
          end
          object Label19: TLabel
            Left = 12
            Top = 44
            Width = 122
            Height = 13
            Caption = 'Sistema operacional (OS):'
            Color = clBtnFace
            ParentColor = False
          end
          object Label20: TLabel
            Left = 144
            Top = 44
            Width = 71
            Height = 13
            Caption = 'Apelido do OS:'
            Color = clBtnFace
            ParentColor = False
          end
          object Label21: TLabel
            Left = 424
            Top = 44
            Width = 69
            Height = 13
            Caption = 'Vers'#227'o do OS:'
            Color = clBtnFace
            ParentColor = False
          end
          object Label22: TLabel
            Left = 424
            Top = 4
            Width = 131
            Height = 13
            Caption = 'Resolu'#231#227'o Largura x Altura:'
            Color = clBtnFace
            ParentColor = False
          end
          object Label23: TLabel
            Left = 928
            Top = 80
            Width = 5
            Height = 13
            Caption = 'x'
          end
          object DBEdit1: TDBEdit
            Left = 12
            Top = 20
            Width = 181
            Height = 21
            DataField = 'DeviceID'
            DataSource = DsYOcMobDevCad
            TabOrder = 0
          end
          object DBEdit3: TDBEdit
            Left = 196
            Top = 20
            Width = 225
            Height = 21
            DataField = 'DeviceName'
            DataSource = DsYOcMobDevCad
            TabOrder = 1
          end
          object DBEdit4: TDBEdit
            Left = 424
            Top = 20
            Width = 60
            Height = 21
            DataField = 'DvcScreenH'
            DataSource = DsYOcMobDevCad
            TabOrder = 2
          end
          object DBEdit5: TDBEdit
            Left = 496
            Top = 20
            Width = 60
            Height = 21
            DataField = 'DvcScreenW'
            DataSource = DsYOcMobDevCad
            TabOrder = 3
          end
          object DBEdit6: TDBEdit
            Left = 12
            Top = 60
            Width = 129
            Height = 21
            DataField = 'OSName'
            DataSource = DsYOcMobDevCad
            TabOrder = 4
          end
          object DBEdit7: TDBEdit
            Left = 144
            Top = 60
            Width = 277
            Height = 21
            DataField = 'OSNickName'
            DataSource = DsYOcMobDevCad
            TabOrder = 5
          end
          object DBEdit8: TDBEdit
            Left = 424
            Top = 60
            Width = 133
            Height = 21
            DataField = 'OSVersion'
            DataSource = DsYOcMobDevCad
            TabOrder = 6
          end
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 152
    Width = 573
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 569
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 196
    Width = 573
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 427
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sair'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 425
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 8
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Aceitar'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
      object BtExclui: TBitBtn
        Tag = 13
        Left = 132
        Top = 4
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&N'#195'O aceitar'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        OnClick = BtExcluiClick
      end
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 24
    Top = 11
  end
  object QrYOcMobDevCad: TMySQLQuery
    Database = Dmod.ZZDB
    AfterOpen = QrYOcMobDevCadAfterOpen
    SQL.Strings = (
      'SELECT * '
      'FROM OVcMobDevCad'
      'WHERE Codigo < 0'
      'ORDER BY Codigo DESC')
    Left = 344
    Top = 53
    object QrYOcMobDevCadCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrYOcMobDevCadNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
    object QrYOcMobDevCadDeviceID: TWideStringField
      DisplayWidth = 60
      FieldName = 'DeviceID'
      Size = 60
    end
    object QrYOcMobDevCadUserNmePdr: TWideStringField
      FieldName = 'UserNmePdr'
      Size = 60
    end
    object QrYOcMobDevCadDeviceName: TWideStringField
      FieldName = 'DeviceName'
      Size = 60
    end
    object QrYOcMobDevCadDvcScreenH: TIntegerField
      FieldName = 'DvcScreenH'
      Required = True
    end
    object QrYOcMobDevCadDvcScreenW: TIntegerField
      FieldName = 'DvcScreenW'
      Required = True
    end
    object QrYOcMobDevCadOSName: TWideStringField
      FieldName = 'OSName'
      Size = 60
    end
    object QrYOcMobDevCadOSNickName: TWideStringField
      FieldName = 'OSNickName'
      Size = 60
    end
    object QrYOcMobDevCadOSVersion: TWideStringField
      FieldName = 'OSVersion'
      Size = 60
    end
    object QrYOcMobDevCadDtaHabIni: TDateTimeField
      FieldName = 'DtaHabIni'
      Required = True
    end
    object QrYOcMobDevCadDtaHabFim: TDateTimeField
      FieldName = 'DtaHabFim'
      Required = True
    end
    object QrYOcMobDevCadAllowed: TSmallintField
      FieldName = 'Allowed'
      Required = True
    end
    object QrYOcMobDevCadLastSetAlw: TIntegerField
      FieldName = 'LastSetAlw'
      Required = True
    end
    object QrYOcMobDevCadLk: TIntegerField
      FieldName = 'Lk'
      Required = True
    end
    object QrYOcMobDevCadDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrYOcMobDevCadDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrYOcMobDevCadUserCad: TIntegerField
      FieldName = 'UserCad'
      Required = True
    end
    object QrYOcMobDevCadUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Required = True
    end
    object QrYOcMobDevCadAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Required = True
    end
    object QrYOcMobDevCadAWServerID: TIntegerField
      FieldName = 'AWServerID'
      Required = True
    end
    object QrYOcMobDevCadAWStatSinc: TSmallintField
      FieldName = 'AWStatSinc'
      Required = True
    end
    object QrYOcMobDevCadAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
  end
  object DsYOcMobDevCad: TDataSource
    DataSet = QrYOcMobDevCad
    Left = 344
    Top = 97
  end
  object Timer1: TTimer
    Enabled = False
    OnTimer = Timer1Timer
    Left = 346
    Top = 147
  end
end
