unit UnApp_Jan;

interface

uses
  Windows, SysUtils, Classes, DB, Menus, UnDmkEnums, mySQLDbTables, Forms,
  Grids, DBGrids, UnInternalConsts, dmkGeral, Controls, dmkPageControl,
  UnProjGroup_Vars;

type
  TUnApp_Jan = class(TObject)
  private
    { Private declarations }

  public
    { Public declarations }
    procedure MostraFormComandaCab(Codigo: Integer);
    procedure MostraFormComandaPan(AbrirEmAba: Boolean; InOwner: TWincontrol;
              AdvToolBarPager: TdmkPageControl; Codigo: Integer);
    procedure MostraFormOpcoesApp();
    procedure MostraFormOpcoesGrl();
    procedure MostraFormUsoEConsumo();
    procedure MostraFormMateriaPrima();
    procedure MostraFormStqCnsgGoCab(Codigo, StqCiclCab, Fornece, Empresa: Integer);
    procedure MostraFormStqCnsgBkCab(Codigo, StqCiclCab, Fornece, Empresa: Integer);
    procedure MostraFormStqCnsgVeCab(Codigo, StqCiclCab, Fornece, Empresa: Integer);
    procedure MostraFormStqCiclCab(Codigo: Integer);
    procedure MostraFormGerCiclCab(Fornece: Integer);
    procedure MostraFormTabePrcCab();
  end;

var
  App_Jan: TUnApp_Jan;


implementation

uses
  MyDBCheck, Module, OpcoesApp, Opcoes, ComandaCab, ModuleGeral, UnMyObjects,
  ComandaPan, UnGrade_Jan, TabePrcCab, StqCnsgGoCab, StqCnsgVeCab, StqCnsgBkCab,
  StqCiclCab, GerCiclCab;

{ TUnApp_Jan }

procedure TUnApp_Jan.MostraFormComandaPan(AbrirEmAba: Boolean; InOwner: TWincontrol;
  AdvToolBarPager: TdmkPageControl; Codigo: Integer);
begin
  if AbrirEmAba then
  begin
    AdvToolBarPager.Visible := False;
    if FmComandaPan = nil then
    begin
      VAR_FmComandaPan := MyObjects.FormTDICria(TFmComandaPan, InOwner, AdvToolBarPager, True, True);
      //TFmComandaPan(VAR_FmComandaPan).QrComanda?.Locate('Codigo', Codigo, []);
    end;
  end else
  begin
    if DBCheck.CriaFm(TFmComandaPan, FmComandaPan, afmoNegarComAviso) then
    begin
      FmComandaPan.ShowModal;
      FmComandaPan.Destroy;
      //
      FmComandaPan := nil;
    end;
  end;
end;

procedure TUnApp_Jan.MostraFormGerCiclCab(Fornece: Integer);
begin
  if DBCheck.CriaFm(TFmGerCiclCab, FmGerCiclCab, afmoSoBoss) then
  begin
    FmGerCiclCab.EdFornece.ValueVariant := Fornece;
    FmGerCiclCab.CBFornece.KeyValue := Fornece;
    FmGerCiclCab.ShowModal;
    FmGerCiclCab.Destroy;
  end;
end;

procedure TUnApp_Jan.MostraFormMateriaPrima;
begin
  Grade_Jan.MostraFormGraGruY(0, 0, '');
end;

procedure TUnApp_Jan.MostraFormComandaCab(Codigo: Integer);
begin
  if DModG.QrParamsEmp.State = dsInactive then
    DModG.ReopenParamsEmp(Geral.IMV(VAR_LIB_EMPRESAS));
  if DBCheck.CriaFm(TFmComandaCab, FmComandaCab, afmoSoBoss) then
  begin
    if Codigo <> 0 then
      FmComandaCab.LocCod(Codigo, Codigo);
    FmComandaCab.ShowModal;
    FmComandaCab.Destroy;
  end;
end;

procedure TUnApp_Jan.MostraFormOpcoesApp();
begin
  if DBCheck.CriaFm(TFmOpcoesApp, FmOpcoesApp, afmoSoBoss) then
  begin
    FmOpcoesApp.ShowModal;
    FmOpcoesApp.Destroy;
  end;
end;

procedure TUnApp_Jan.MostraFormOpcoesGrl;
begin
  if DBCheck.CriaFm(TFmOpcoes, FmOpcoes, afmoSoBoss) then
  begin
    FmOpcoes.ShowModal;
    FmOpcoes.Destroy;
  end;
end;

procedure TUnApp_Jan.MostraFormStqCiclCab(Codigo: Integer);
begin
  if DBCheck.CriaFm(TFmStqCiclCab, FmStqCiclCab, afmoSoBoss) then
  begin
    if Codigo <> 0 then
      FmStqCiclCab.QrStqCiclCab.Locate('Codigo', Codigo, []);
    FmStqCiclCab.ShowModal;
    FmStqCiclCab.Destroy;
  end;
end;

procedure TUnApp_Jan.MostraFormStqCnsgBkCab(Codigo, StqCiclCab, Fornece, Empresa: Integer);
begin
  if DBCheck.CriaFm(TFmStqCnsgBkCab, FmStqCnsgBkCab, afmoSoBoss) then
  begin
    FmStqCnsgBkCab.FStqCiclCab := StqCiclCab;
    FmStqCnsgBkCab.FFornece := Fornece;
    FmStqCnsgBkCab.FEmpresa := Empresa;
    if Codigo <> 0 then
      FmStqCnsgBkCab.QrStqCnsgBkCab.Locate('Codigo', Codigo, []);
    FmStqCnsgBkCab.ShowModal;
    FmStqCnsgBkCab.Destroy;
  end;
end;

procedure TUnApp_Jan.MostraFormStqCnsgGoCab(Codigo, StqCiclCab, Fornece, Empresa: Integer);
begin
  if DBCheck.CriaFm(TFmStqCnsgGoCab, FmStqCnsgGoCab, afmoSoBoss) then
  begin
    FmStqCnsgGoCab.FStqCiclCab := StqCiclCab;
    FmStqCnsgGoCab.FFornece := Fornece;
    FmStqCnsgGoCab.FEmpresa := Empresa;
    if Codigo <> 0 then
      FmStqCnsgGoCab.QrStqCnsgGoCab.Locate('Codigo', Codigo, []);
    FmStqCnsgGoCab.ShowModal;
    FmStqCnsgGoCab.Destroy;
  end;
end;

procedure TUnApp_Jan.MostraFormStqCnsgVeCab(Codigo, StqCiclCab, Fornece, Empresa: Integer);
begin
  if DBCheck.CriaFm(TFmStqCnsgVeCab, FmStqCnsgVeCab, afmoSoBoss) then
  begin
    FmStqCnsgVeCab.FStqCiclCab := StqCiclCab;
    FmStqCnsgVeCab.FFornece := Fornece;
    FmStqCnsgVeCab.FEmpresa := Empresa;
    if Codigo <> 0 then
      FmStqCnsgVeCab.QrStqCnsgVeCab.Locate('Codigo', Codigo, []);
    FmStqCnsgVeCab.ShowModal;
    FmStqCnsgVeCab.Destroy;
  end;
end;

procedure TUnApp_Jan.MostraFormTabePrcCab;
begin
  if DBCheck.CriaFm(TFmTabePrcCab, FmTabePrcCab, afmoNegarComAviso) then
  begin
    FmTabePrcCab.ShowModal;
    FmTabePrcCab.Destroy;
  end;
end;

procedure TUnApp_Jan.MostraFormUsoEConsumo();
begin

end;

end.
