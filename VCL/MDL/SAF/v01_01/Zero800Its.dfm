object FmZero800Its: TFmZero800Its
  Left = 339
  Top = 185
  Caption = 'CAD-PRTER-002 :: Cadastro de Protocolos de Terceiros - Itens'
  ClientHeight = 494
  ClientWidth = 784
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 784
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 736
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 688
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 525
        Height = 32
        Caption = 'Cadastro de Protocolos de Terceiros - Itens'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 525
        Height = 32
        Caption = 'Cadastro de Protocolos de Terceiros - Itens'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 525
        Height = 32
        Caption = 'Cadastro de Protocolos de Terceiros - Itens'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 784
    Height = 332
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 784
      Height = 332
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 784
        Height = 332
        Align = alClient
        TabOrder = 0
        object Splitter1: TSplitter
          Left = 447
          Top = 15
          Width = 10
          Height = 315
          ExplicitLeft = 706
          ExplicitTop = 14
          ExplicitHeight = 313
        end
        object Panel5: TPanel
          Left = 2
          Top = 15
          Width = 445
          Height = 315
          Align = alLeft
          BevelOuter = bvNone
          Constraints.MinHeight = 315
          Constraints.MinWidth = 445
          TabOrder = 0
          object Label9: TLabel
            Left = 5
            Top = 3
            Width = 36
            Height = 13
            Caption = 'C'#243'digo:'
          end
          object Label1: TLabel
            Left = 5
            Top = 44
            Width = 42
            Height = 13
            Caption = 'Entidade'
          end
          object SpeedButton5: TSpeedButton
            Left = 415
            Top = 60
            Width = 21
            Height = 21
            Hint = 'Inclui item de carteira'
            Caption = '...'
            OnClick = SpeedButton5Click
          end
          object Label4: TLabel
            Left = 5
            Top = 91
            Width = 97
            Height = 13
            Caption = 'Protocolo de origem:'
          end
          object Label2: TLabel
            Left = 210
            Top = 91
            Width = 48
            Height = 13
            Caption = 'Protocolo:'
          end
          object Label3: TLabel
            Left = 5
            Top = 133
            Width = 52
            Height = 13
            Caption = 'Atendente:'
          end
          object Label5: TLabel
            Left = 260
            Top = 133
            Width = 48
            Height = 13
            Caption = 'Matr'#237'cula:'
          end
          object EdCodigo: TdmkEdit
            Left = 5
            Top = 18
            Width = 71
            Height = 21
            Alignment = taRightJustify
            Color = clInactiveCaption
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBackground
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
            ReadOnly = True
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'Codigo'
            UpdCampo = 'Codigo'
            UpdType = utInc
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
          object EdEntidade: TdmkEditCB
            Left = 5
            Top = 60
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 2
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            DBLookupComboBox = CBEntidade
            IgnoraDBLookupComboBox = False
          end
          object CBEntidade: TdmkDBLookupComboBox
            Left = 60
            Top = 60
            Width = 350
            Height = 21
            KeyField = 'Codigo'
            ListField = 'Nome'
            ListSource = DsEntidades
            TabOrder = 3
            dmkEditCB = EdEntidade
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
          object EdProtOri: TdmkEdit
            Left = 5
            Top = 106
            Width = 200
            Height = 21
            TabOrder = 4
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdProt: TdmkEdit
            Left = 210
            Top = 106
            Width = 200
            Height = 21
            TabOrder = 5
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdAtendente: TdmkEdit
            Left = 5
            Top = 148
            Width = 250
            Height = 21
            TabOrder = 6
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdMatricula: TdmkEdit
            Left = 260
            Top = 148
            Width = 150
            Height = 21
            TabOrder = 7
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object BtSalvar: TBitBtn
            Tag = 24
            Left = 5
            Top = 175
            Width = 120
            Height = 40
            Caption = 'S&alvar'
            NumGlyphs = 2
            TabOrder = 8
            OnClick = BtSalvarClick
          end
          object GroupBox2: TGroupBox
            Left = 85
            Top = 3
            Width = 240
            Height = 42
            Caption = 'Abertura'
            TabOrder = 1
            object Label18: TLabel
              Left = 7
              Top = 20
              Width = 26
              Height = 13
              Caption = 'Data:'
            end
            object Label17: TLabel
              Left = 133
              Top = 20
              Width = 26
              Height = 13
              Caption = 'Hora:'
            end
            object TPData: TDateTimePicker
              Left = 39
              Top = 16
              Width = 90
              Height = 21
              Date = 39422.782497824100000000
              Time = 39422.782497824100000000
              TabOrder = 0
            end
            object TPHora: TDateTimePicker
              Left = 161
              Top = 17
              Width = 73
              Height = 21
              Date = 39422.782497824100000000
              Time = 39422.782497824100000000
              Kind = dtkTime
              TabOrder = 1
            end
          end
        end
        object PnZero800Its: TPanel
          Left = 457
          Top = 15
          Width = 325
          Height = 315
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 1
          object Panel7: TPanel
            Left = 0
            Top = 184
            Width = 325
            Height = 131
            Align = alBottom
            BevelOuter = bvNone
            TabOrder = 0
            object Panel8: TPanel
              Left = 0
              Top = 0
              Width = 325
              Height = 57
              Align = alTop
              TabOrder = 0
              object GroupBox3: TGroupBox
                Left = 6
                Top = 6
                Width = 178
                Height = 45
                Caption = 'Data / Hora'
                TabOrder = 0
                object TPDataIts: TDateTimePicker
                  Left = 5
                  Top = 17
                  Width = 90
                  Height = 21
                  Date = 39422.782497824100000000
                  Time = 39422.782497824100000000
                  TabOrder = 0
                end
                object TPHoraIts: TDateTimePicker
                  Left = 100
                  Top = 17
                  Width = 73
                  Height = 21
                  Date = 39422.782497824100000000
                  Time = 39422.782497824100000000
                  Kind = dtkTime
                  TabOrder = 1
                end
              end
              object RGQuem: TRadioGroup
                Left = 185
                Top = 6
                Width = 128
                Height = 45
                Caption = 'Quem?'
                Items.Strings = (
                  'Cadastrante'
                  'Atendente')
                TabOrder = 1
              end
            end
            object MeTexto: TMemo
              Left = 0
              Top = 57
              Width = 285
              Height = 74
              Align = alClient
              Lines.Strings = (
                'Memo1')
              MaxLength = 255
              TabOrder = 1
            end
            object BtConfirma: TBitBtn
              Tag = 14
              Left = 285
              Top = 57
              Width = 40
              Height = 74
              Align = alRight
              NumGlyphs = 2
              TabOrder = 2
              OnClick = BtConfirmaClick
            end
          end
          object REDBTexto: TRichEdit
            Left = 0
            Top = 0
            Width = 325
            Height = 184
            Align = alClient
            Lines.Strings = (
              'REDBTexto')
            ReadOnly = True
            ScrollBars = ssVertical
            TabOrder = 1
          end
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 380
    Width = 784
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 780
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 424
    Width = 784
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 638
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 636
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
    end
  end
  object QrEntidades: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ent.Codigo, ent.CodUsu,'
      'IF (ent.Tipo=0, ent.RazaoSocial, ent.Nome) Nome'
      'FROM entidades ent'
      'ORDER BY Nome')
    Left = 664
    Top = 11
    object QrEntidadesCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEntidadesNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 100
    end
    object QrEntidadesCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
  end
  object DsEntidades: TDataSource
    DataSet = QrEntidades
    Left = 692
    Top = 11
  end
  object QrZero800Its: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT its.*, '
      'DATE_FORMAT(its.DataHora, "%d/%m/%Y %H:%i:%s") DATAHORA_TXT,'
      'cab.Atendente'
      'FROM zero800its its'
      'LEFT JOIN zero800cab cab ON cab.Codigo = its.Codigo'
      'WHERE its.Codigo=:P0'
      'ORDER BY its.DataHora DESC')
    Left = 608
    Top = 11
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrZero800ItsCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'zero800its.Codigo'
    end
    object QrZero800ItsControle: TIntegerField
      FieldName = 'Controle'
      Origin = 'zero800its.Controle'
    end
    object QrZero800ItsNome: TWideStringField
      FieldName = 'Nome'
      Origin = 'zero800its.Nome'
      Size = 255
    end
    object QrZero800ItsQuem: TSmallintField
      FieldName = 'Quem'
      Origin = 'zero800its.Quem'
    end
    object QrZero800ItsDataHora: TDateTimeField
      FieldName = 'DataHora'
      Origin = 'zero800its.DataHora'
    end
    object QrZero800ItsLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'zero800its.Lk'
    end
    object QrZero800ItsDataCad: TDateField
      FieldName = 'DataCad'
      Origin = 'zero800its.DataCad'
    end
    object QrZero800ItsDataAlt: TDateField
      FieldName = 'DataAlt'
      Origin = 'zero800its.DataAlt'
    end
    object QrZero800ItsUserCad: TIntegerField
      FieldName = 'UserCad'
      Origin = 'zero800its.UserCad'
    end
    object QrZero800ItsUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Origin = 'zero800its.UserAlt'
    end
    object QrZero800ItsAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Origin = 'zero800its.AlterWeb'
    end
    object QrZero800ItsAtivo: TSmallintField
      FieldName = 'Ativo'
      Origin = 'zero800its.Ativo'
    end
    object QrZero800ItsDATAHORA_TXT: TWideStringField
      FieldName = 'DATAHORA_TXT'
      Size = 19
    end
    object QrZero800ItsAtendente: TWideStringField
      FieldName = 'Atendente'
      Size = 60
    end
  end
  object DsZero800Its: TDataSource
    DataSet = QrZero800Its
    Left = 636
    Top = 11
  end
end
