unit Zero800Its;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, DmkDAC_PF, 
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnInternalConsts,
  dmkImage, dmkDBGrid, Variants, UnDmkEnums;

type
  TFmZero800Its = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    LaTitulo1C: TLabel;
    Panel5: TPanel;
    PnZero800Its: TPanel;
    Splitter1: TSplitter;
    Label9: TLabel;
    EdCodigo: TdmkEdit;
    Label1: TLabel;
    EdEntidade: TdmkEditCB;
    CBEntidade: TdmkDBLookupComboBox;
    SpeedButton5: TSpeedButton;
    Label4: TLabel;
    EdProtOri: TdmkEdit;
    Label2: TLabel;
    EdProt: TdmkEdit;
    EdAtendente: TdmkEdit;
    Label3: TLabel;
    EdMatricula: TdmkEdit;
    Label5: TLabel;
    BtSalvar: TBitBtn;
    Panel7: TPanel;
    GroupBox2: TGroupBox;
    Label18: TLabel;
    Label17: TLabel;
    TPData: TDateTimePicker;
    TPHora: TDateTimePicker;
    Panel8: TPanel;
    GroupBox3: TGroupBox;
    TPDataIts: TDateTimePicker;
    TPHoraIts: TDateTimePicker;
    RGQuem: TRadioGroup;
    MeTexto: TMemo;
    BtConfirma: TBitBtn;
    QrEntidades: TmySQLQuery;
    QrEntidadesCodigo: TIntegerField;
    QrEntidadesNome: TWideStringField;
    QrEntidadesCodUsu: TIntegerField;
    DsEntidades: TDataSource;
    QrZero800Its: TmySQLQuery;
    DsZero800Its: TDataSource;
    QrZero800ItsCodigo: TIntegerField;
    QrZero800ItsControle: TIntegerField;
    QrZero800ItsNome: TWideStringField;
    QrZero800ItsQuem: TSmallintField;
    QrZero800ItsDataHora: TDateTimeField;
    QrZero800ItsLk: TIntegerField;
    QrZero800ItsDataCad: TDateField;
    QrZero800ItsDataAlt: TDateField;
    QrZero800ItsUserCad: TIntegerField;
    QrZero800ItsUserAlt: TIntegerField;
    QrZero800ItsAlterWeb: TSmallintField;
    QrZero800ItsAtivo: TSmallintField;
    REDBTexto: TRichEdit;
    QrZero800ItsDATAHORA_TXT: TWideStringField;
    QrZero800ItsAtendente: TWideStringField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtSalvarClick(Sender: TObject);
    procedure SpeedButton5Click(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
  private
    { Private declarations }
    procedure MostraEdicao(SQLType: TSQLType; Codigo: Integer; ApenasIts: Boolean);
    procedure ReopenZero800Its(Codigo: Integer);
  public
    { Public declarations }
    FCodigo: Integer;
  end;

  var
  FmZero800Its: TFmZero800Its;
  const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, ModuleGeral, UMySQLModule, Zero800Cab;

{$R *.DFM}

procedure TFmZero800Its.BtConfirmaClick(Sender: TObject);
var
  Codigo, Controle, Quem: Integer;
  DataHora, Texto: String;
begin
  Codigo   := FCodigo;
  Quem     := RGQuem.ItemIndex;
  Texto    := Trim(MeTexto.Text);
  DataHora := Geral.FDT(Int(TPDataIts.Date) + TPHoraIts.Time - Int(TPHoraIts.Time), 9);
  Controle := UMyMod.BuscaEmLivreY_Def('zero800its', 'Controle', stIns, 0);
  //
  if MyObjects.FIC(Codigo = 0, nil, 'C�digo n�o definido!') then Exit;
  if MyObjects.FIC(Quem < 0, RGQuem, 'Defina quem fez o coment�rio') then Exit;
  if MyObjects.FIC(Length(Texto) = 0, MeTexto, 'Defina o texto!') then Exit;
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'zero800its', False,
  [
    'Codigo', 'Nome', 'Quem', 'DataHora'
  ], ['Controle'], [
    Codigo, Texto, Quem, DataHora
  ], [Controle], True) then
  begin
    MostraEdicao(stUpd, Codigo, True);
    ReopenZero800Its(Codigo);
    FmZero800Cab.ReopenZero800Its(Codigo, Controle);
    //
    MeTexto.SetFocus;
  end;
end;

procedure TFmZero800Its.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmZero800Its.BtSalvarClick(Sender: TObject);
var
  Codigo, Entidade: Integer;
  ProtOri, Prot, Atendente, Matricula, DataHora: String;
begin
  Entidade  := EdEntidade.ValueVariant;
  ProtOri   := EdProtOri.ValueVariant;
  Prot      := EdProt.ValueVariant;
  Atendente := EdAtendente.ValueVariant;
  Matricula := EdMatricula.ValueVariant;
  DataHora  := Geral.FDT(Int(TPData.Date) + TPHora.Time - Int(TPHora.Time), 9);
  Codigo    := UMyMod.BuscaEmLivreY_Def('zero800cab', 'Codigo', ImgTipo.SQLType,
    EdCodigo.ValueVariant);
  //
  if MyObjects.FIC(Entidade = 0, EdEntidade, 'Defina uma entidade!') then Exit;
  if MyObjects.FIC(Length(Prot) = 0, EdProt, 'Defina o protocolo!') then Exit;
  if MyObjects.FIC(Length(Atendente) = 0, EdAtendente, 'Defina o atendente!') then Exit;
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'zero800cab', False,
  [
    'Entidade', 'ProtoOri', 'ProtoNum', 'Atendente', 'Matricula', 'DataHora'
  ], ['Codigo'], [
    Entidade, ProtOri, Prot, Atendente, Matricula, DataHora
  ], [Codigo], True) then
  begin
    FmZero800Cab.ReopenZero800CabEnt('', Entidade);
    FmZero800Cab.ReopenZero800Cab(Codigo, Entidade);
    //
    FCodigo         := Codigo;
    ImgTipo.SQLType := stUpd;
    //
    MostraEdicao(stUpd, Codigo, False);
    //
    MeTexto.SetFocus;
  end;
end;

procedure TFmZero800Its.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //
  MostraEdicao(ImgTipo.SQLType, FCodigo, False);
end;

procedure TFmZero800Its.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  QrEntidades.Open;
end;

procedure TFmZero800Its.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmZero800Its.MostraEdicao(SQLType: TSQLType; Codigo: Integer;
  ApenasIts: Boolean);
begin
  if not ApenasIts then
  begin
    if SQLType = stIns then
    begin
      PnZero800Its.Visible := False;
      Splitter1.Visible    := False;
      QrZero800Its.Close;
      //
      EdCodigo.ValueVariant    := FormatFloat(FFormatFloat, Codigo);
      TPData.Date              := DModG.ObtemAgora;
      TPHora.Time              := DModG.ObtemAgora;
      EdEntidade.ValueVariant  := 0;
      CBEntidade.KeyValue      := Null;
      EdProtOri.ValueVariant   := '';
      EdProt.ValueVariant      := '';
      EdAtendente.ValueVariant := '';
      EdMatricula.ValueVariant := '';
      EdEntidade.SetFocus;
    end else
    begin
      PnZero800Its.Visible := True;
      Splitter1.Visible    := True;
      ReopenZero800Its(FCodigo);
      //
      EdCodigo.ValueVariant    := FmZero800Cab.QrZero800CabCodigo.Value;
      TPData.Date              := FmZero800Cab.QrZero800CabDataHora.Value;
      TPHora.Time              := FmZero800Cab.QrZero800CabDataHora.Value;
      EdEntidade.ValueVariant  := FmZero800Cab.QrZero800CabEntidade.Value;
      CBEntidade.KeyValue      := FmZero800Cab.QrZero800CabEntidade.Value;
      EdProtOri.ValueVariant   := FmZero800Cab.QrZero800CabProtoOri.Value;
      EdProt.ValueVariant      := FmZero800Cab.QrZero800CabProtoNum.Value;
      EdAtendente.ValueVariant := FmZero800Cab.QrZero800CabAtendente.Value;
      EdMatricula.ValueVariant := FmZero800Cab.QrZero800CabMatricula.Value;
      EdEntidade.SetFocus;
    end;
  end;
  TPDataIts.Date   := DModG.ObtemAgora;
  TPHoraIts.Time   := DModG.ObtemAgora;
  RGQuem.ItemIndex := 0;
  MeTexto.Text     := '';
end;

procedure TFmZero800Its.ReopenZero800Its(Codigo: Integer);
var
  Quem: Integer;
  Texto, DataHora, Atendente: String;
begin
  try
    Screen.Cursor := crHourGlass;
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrZero800Its, Dmod.MyDB, [
      'SELECT its.*, ',
      'DATE_FORMAT(its.DataHora, "%d/%m/%Y %H:%i:%s") DATAHORA_TXT, ',
      'cab.Atendente ',
      'FROM zero800its its ',
      'LEFT JOIN zero800cab cab ON cab.Codigo = its.Codigo ',
      'WHERE its.Codigo=' + Geral.FF0(Codigo) + ' ',
      'ORDER BY its.DataHora DESC ',
    '']);
    REDBTexto.Lines.Clear;
    //
    if QrZero800Its.RecordCount > 0 then
    begin
      QrZero800Its.First;
      while not QrZero800Its.Eof do
      begin
        Quem      := QrZero800ItsQuem.Value;
        Texto     := QrZero800ItsNome.Value;
        DataHora  := QrZero800ItsDATAHORA_TXT.Value;
        Atendente := QrZero800ItsAtendente.Value;
        //
        if Quem = 0 then
        begin
          REDBTexto.Lines.Add('�s ' + DataHora + ' voc� diz:');
          REDBTexto.Lines.Add(Texto + sLineBreak);
        end else
        begin
          REDBTexto.SelAttributes.Style := REDBTexto.SelAttributes.Style + [fsBold];
          REDBTexto.Lines.Add('�s ' + DataHora + ' ' + Atendente + ' diz:');
          REDBTexto.Lines.Add(Texto + sLineBreak);
        end;
        //
        QrZero800Its.Next;
      end;
    end;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmZero800Its.SpeedButton5Click(Sender: TObject);
begin
  DmodG.CadastroDeEntidade(EdEntidade.ValueVariant, fmcadEntidade2, fmcadEntidade2);
  //
  QrEntidades.Close;
  QrEntidades.Open;
  //
  if VAR_CADASTRO > 0 then
    UMyMod.SetaCodigoPesquisado(EdEntidade, CBEntidade, QrEntidades, VAR_CADASTRO);
end;

end.
