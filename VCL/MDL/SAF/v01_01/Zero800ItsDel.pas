unit Zero800ItsDel;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnInternalConsts,
  dmkImage, dmkDBGrid, UnDmkEnums;

type
  TFmZero800ItsDel = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtExclui: TBitBtn;
    LaTitulo1C: TLabel;
    dmkDBGrid2: TdmkDBGrid;
    DBMemo1: TDBMemo;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtExcluiClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  var
  FmZero800ItsDel: TFmZero800ItsDel;

implementation

uses UnMyObjects, Module, Zero800Cab, UMySQLModule;

{$R *.DFM}

procedure TFmZero800ItsDel.BtExcluiClick(Sender: TObject);
begin
  if FmZero800Cab.QrZero800Its.RecordCount > 0 then
  begin
    if UMyMod.ExcluiRegistros('Deseja excluir o coment�rio ID n�mero ' +
      Geral.FF0(FmZero800Cab.QrZero800ItsControle.Value) + '?', Dmod.QrUpd,
      'zero800its', ['Controle'], ['='],
      [FmZero800Cab.QrZero800ItsControle.Value], '') then
    begin
      FmZero800Cab.ReopenZero800Its(FmZero800Cab.QrZero800CabCodigo.Value, 0);
    end;
  end;
end;

procedure TFmZero800ItsDel.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmZero800ItsDel.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmZero800ItsDel.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
end;

procedure TFmZero800ItsDel.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

end.
