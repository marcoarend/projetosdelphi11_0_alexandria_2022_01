unit TelCtaCab;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2,
  ResIntStrings, UnMLAGeral, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, dmkGeral,
  dmkPermissoes, dmkEdit, dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup,
  unDmkProcFunc, ComCtrls, Grids, DBGrids, dmkDBGrid, DmkDAC_PF, Menus,
  UnDmkEnums;

type
  THackDBGrid = class(TDBGrid);
  TFmTelCtaCab = class(TForm)
    PnDados: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    BtExclui: TBitBtn;
    BtInclui: TBitBtn;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    QrTelCtasCab: TmySQLQuery;
    DsTelCtasCab: TDataSource;
    dmkPermissoes1: TdmkPermissoes;
    Label1: TLabel;
    DBEdCodigo: TdmkDBEdit;
    Label2: TLabel;
    DBEdNome: TdmkDBEdit;
    PB1: TProgressBar;
    QrTelCtasCabCodigo: TIntegerField;
    QrTelCtasCabContaCobr: TWideStringField;
    QrTelCtasCabNumFatura: TIntegerField;
    QrTelCtasCabIniVigen: TDateField;
    QrTelCtasCabFimVigen: TDateField;
    QrTelCtasCabEmissao: TDateField;
    QrTelCtasCabVencto: TDateField;
    QrTelCtasCabCliente: TWideStringField;
    QrTelCtasCabUF: TWideStringField;
    QrTelCtasCabLk: TIntegerField;
    QrTelCtasCabDataCad: TDateField;
    QrTelCtasCabDataAlt: TDateField;
    QrTelCtasCabUserCad: TIntegerField;
    QrTelCtasCabUserAlt: TIntegerField;
    QrTelCtasCabAlterWeb: TSmallintField;
    QrTelCtasCabAtivo: TSmallintField;
    Label3: TLabel;
    dmkDBEdit1: TdmkDBEdit;
    dmkDBEdit2: TdmkDBEdit;
    Label4: TLabel;
    dmkDBEdit3: TdmkDBEdit;
    Label5: TLabel;
    dmkDBEdit4: TdmkDBEdit;
    Label6: TLabel;
    Label7: TLabel;
    dmkDBEdit5: TdmkDBEdit;
    Label8: TLabel;
    dmkDBEdit6: TdmkDBEdit;
    Label9: TLabel;
    dmkDBEdit7: TdmkDBEdit;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    TabSheet3: TTabSheet;
    TabSheet4: TTabSheet;
    PnChamTop: TPanel;
    PnMensTop: TPanel;
    PnEveTop: TPanel;
    PnDesTop: TPanel;
    PnChamBot: TPanel;
    QrTelCtasTel: TmySQLQuery;
    DsTelCtasTel: TDataSource;
    QrTelCtasTelCodigo: TIntegerField;
    QrTelCtasTelControle: TIntegerField;
    QrTelCtasTelTelOrigem: TWideStringField;
    QrTelCtasTelDataHora: TDateTimeField;
    QrTelCtasTelDuracao: TTimeField;
    QrTelCtasTelTelDestino: TWideStringField;
    QrTelCtasTelCidade: TWideStringField;
    QrTelCtasTelUF: TWideStringField;
    QrTelCtasTelTipo: TWideStringField;
    QrTelCtasTelHorario: TWideStringField;
    QrTelCtasTelValor: TFloatField;
    QrTelCtasTelLk: TIntegerField;
    QrTelCtasTelDataCad: TDateField;
    QrTelCtasTelDataAlt: TDateField;
    QrTelCtasTelUserCad: TIntegerField;
    QrTelCtasTelUserAlt: TIntegerField;
    QrTelCtasTelAlterWeb: TSmallintField;
    QrTelCtasTelAtivo: TSmallintField;
    QrTelCtasTelPrestadora: TWideStringField;
    QrTelCtasTelTipoCham: TWideStringField;
    QrTelCtasTelVencto: TDateField;
    QrTelCtasMsg: TmySQLQuery;
    DsTelCtasMsg: TDataSource;
    QrTelCtasMsgCodigo: TIntegerField;
    QrTelCtasMsgControle: TIntegerField;
    QrTelCtasMsgTipo: TWideStringField;
    QrTelCtasMsgID: TWideStringField;
    QrTelCtasMsgDescricao: TWideStringField;
    QrTelCtasMsgDataIni: TDateField;
    QrTelCtasMsgDataFim: TDateField;
    QrTelCtasMsgValor: TFloatField;
    QrTelCtasMsgLk: TIntegerField;
    QrTelCtasMsgDataCad: TDateField;
    QrTelCtasMsgDataAlt: TDateField;
    QrTelCtasMsgUserCad: TIntegerField;
    QrTelCtasMsgUserAlt: TIntegerField;
    QrTelCtasMsgAlterWeb: TSmallintField;
    QrTelCtasMsgAtivo: TSmallintField;
    QrTelCtasMsgPrestadora: TWideStringField;
    QrTelCtasEve: TmySQLQuery;
    DsTelCtasEve: TDataSource;
    QrTelCtasEveCodigo: TIntegerField;
    QrTelCtasEveControle: TIntegerField;
    QrTelCtasEveTipo: TWideStringField;
    QrTelCtasEveID: TWideStringField;
    QrTelCtasEveDescricao: TWideStringField;
    QrTelCtasEveValor: TFloatField;
    QrTelCtasEveLk: TIntegerField;
    QrTelCtasEveDataCad: TDateField;
    QrTelCtasEveDataAlt: TDateField;
    QrTelCtasEveUserCad: TIntegerField;
    QrTelCtasEveUserAlt: TIntegerField;
    QrTelCtasEveAlterWeb: TSmallintField;
    QrTelCtasEveAtivo: TSmallintField;
    QrTelCtasEvePrestadora: TWideStringField;
    QrTelCtasDes: TmySQLQuery;
    DsTelCtasDes: TDataSource;
    QrTelCtasDesCodigo: TIntegerField;
    QrTelCtasDesControle: TIntegerField;
    QrTelCtasDesDescricao: TWideStringField;
    QrTelCtasDesValor: TFloatField;
    QrTelCtasDesLk: TIntegerField;
    QrTelCtasDesDataCad: TDateField;
    QrTelCtasDesDataAlt: TDateField;
    QrTelCtasDesUserCad: TIntegerField;
    QrTelCtasDesUserAlt: TIntegerField;
    QrTelCtasDesAlterWeb: TSmallintField;
    QrTelCtasDesAtivo: TSmallintField;
    QrTelCtasDesPrestadora: TWideStringField;
    DBGDes: TDBGrid;
    DBGEve: TDBGrid;
    DBGMsg: TDBGrid;
    DBGTel: TDBGrid;
    QrTelCtasTelTELORIGEM_TXT: TWideStringField;
    QrTelCtasTelTELDESTINO_TXT: TWideStringField;
    CBChamOrdem: TComboBox;
    Label10: TLabel;
    Label11: TLabel;
    CBChamOrdenar: TComboBox;
    BtTelConcilia: TBitBtn;
    QrLoc: TmySQLQuery;
    QrTelCtasTelEntOrigem: TIntegerField;
    QrTelCtasTelEntDestino: TIntegerField;
    QrTelCtasTelENTORIGNOME: TWideStringField;
    QrTelCtasTelENTDESTNOME: TWideStringField;
    PnDesBot: TPanel;
    Panel1: TPanel;
    Label12: TLabel;
    EdDesTotal: TdmkEdit;
    PnEveBot: TPanel;
    Panel7: TPanel;
    Label13: TLabel;
    EdEveTotal: TdmkEdit;
    Panel6: TPanel;
    Panel8: TPanel;
    Label14: TLabel;
    EdMsgTotal: TdmkEdit;
    Panel9: TPanel;
    Label15: TLabel;
    EdTelChamTot: TdmkEdit;
    EdTelDurTot: TdmkEdit;
    Label16: TLabel;
    EdTelTotal: TdmkEdit;
    Label17: TLabel;
    BtTelExclui: TBitBtn;
    BtMsgExclui: TBitBtn;
    BtEveExclui: TBitBtn;
    BtDesExclui: TBitBtn;
    PMTelExclui: TPopupMenu;
    Exclui1: TMenuItem;
    Excluitodosquenopossuirementidadedeorigem1: TMenuItem;
    Excluitodosquenopossuirementidadededestino1: TMenuItem;
    GroupBox1: TGroupBox;
    LaTotal: TLabel;
    BtTelEntidade: TBitBtn;
    EdTelSomaLinha: TdmkEdit;
    Label18: TLabel;
    N1: TMenuItem;
    N2: TMenuItem;
    PMTelEntidade: TPopupMenu;
    Cadastraentidade1: TMenuItem;
    Cadastraentidadeaignorar1: TMenuItem;
    QrTelCtasTelIGNORARORIGEM: TFloatField;
    QrTelCtasTelIGNORARDESTINO: TFloatField;
    Excluitodasentidadesorigemaignorar1: TMenuItem;
    Excluitodasentidadesdestinoaignorar1: TMenuItem;
    PMInclui: TPopupMenu;
    ContaGVT1: TMenuItem;
    BtPagto: TBitBtn;
    TabSheet5: TTabSheet;
    DBGrid4: TDBGrid;
    PMPagto: TPopupMenu;
    Inclui1: TMenuItem;
    Exclui2: TMenuItem;
    QrTelCtasCabCliInt_TXT: TWideStringField;
    QrTelCtasCabCliInt: TIntegerField;
    Label19: TLabel;
    dmkDBEdit8: TdmkDBEdit;
    QrPagtos: TmySQLQuery;
    QrPagtosData: TDateField;
    QrPagtosVencimento: TDateField;
    QrPagtosBanco: TIntegerField;
    QrPagtosSEQ: TIntegerField;
    QrPagtosFatID: TIntegerField;
    QrPagtosContaCorrente: TWideStringField;
    QrPagtosDocumento: TFloatField;
    QrPagtosDescricao: TWideStringField;
    QrPagtosFatParcela: TIntegerField;
    QrPagtosFatNum: TFloatField;
    QrPagtosNOMECARTEIRA: TWideStringField;
    QrPagtosNOMECARTEIRA2: TWideStringField;
    QrPagtosBanco1: TIntegerField;
    QrPagtosAgencia1: TIntegerField;
    QrPagtosConta1: TWideStringField;
    QrPagtosTipoDoc: TSmallintField;
    QrPagtosControle: TIntegerField;
    QrPagtosNOMEFORNECEI: TWideStringField;
    QrPagtosCARTEIRATIPO: TIntegerField;
    QrPagtosDebito: TFloatField;
    QrPagtosAgencia: TIntegerField;
    DsPagtos: TDataSource;
    QrPagtosFornecedor: TIntegerField;
    QrPagtosNOMEFORNECE: TWideStringField;
    QrTelCtasCabCodCliInt: TIntegerField;
    QrPagtosCarteira: TIntegerField;
    QrPagtosGenero: TIntegerField;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrTelCtasCabAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrTelCtasCabBeforeOpen(DataSet: TDataSet);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure QrTelCtasCabAfterScroll(DataSet: TDataSet);
    procedure QrTelCtasCabBeforeClose(DataSet: TDataSet);
    procedure QrTelCtasTelCalcFields(DataSet: TDataSet);
    procedure CBChamOrdemChange(Sender: TObject);
    procedure CBChamOrdenarChange(Sender: TObject);
    procedure BtTelConciliaClick(Sender: TObject);
    procedure DBGTelDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure DBGMsgDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure DBGEveDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure DBGDesDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure BtTelExcluiClick(Sender: TObject);
    procedure BtMsgExcluiClick(Sender: TObject);
    procedure BtEveExcluiClick(Sender: TObject);
    procedure BtDesExcluiClick(Sender: TObject);
    procedure BtExcluiClick(Sender: TObject);
    procedure Exclui1Click(Sender: TObject);
    procedure Excluitodosquenopossuirementidadedeorigem1Click(Sender: TObject);
    procedure Excluitodosquenopossuirementidadededestino1Click(Sender: TObject);
    procedure DBGTelDblClick(Sender: TObject);
    procedure DBGTelKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure Cadastraentidade1Click(Sender: TObject);
    procedure BtTelEntidadeClick(Sender: TObject);
    procedure Cadastraentidadeaignorar1Click(Sender: TObject);
    procedure Excluitodasentidadesorigemaignorar1Click(Sender: TObject);
    procedure Excluitodasentidadesdestinoaignorar1Click(Sender: TObject);
    procedure BtIncluiClick(Sender: TObject);
    procedure ContaGVT1Click(Sender: TObject);
    procedure BtPagtoClick(Sender: TObject);
    procedure Inclui1Click(Sender: TObject);
    procedure Exclui2Click(Sender: TObject);
    procedure PMPagtoPopup(Sender: TObject);
  private
    FThisFatID: Integer;
    FTabLctA: String;
    FValorTotal: Double;
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure LocCod(Atual, Codigo: Integer);
    procedure Va(Para: TVaiPara);
    procedure ReopenTelCtasTel(SoAtualizaTotal: Boolean; Controle: Integer);
    procedure ReopenTelCtasMsg(SoAtualizaTotal: Boolean; Controle: Integer);
    procedure ReopenTelCtasEve(SoAtualizaTotal: Boolean; Controle: Integer);
    procedure ReopenTelCtasDes(SoAtualizaTotal: Boolean; Controle: Integer);
    procedure ReopenTotal(dmkEdit: TdmkEdit; Tabela, Campo: String);
    procedure ExcluiTelefoneSemEntidade(Origem, Destino: Boolean);
    procedure ExcluiTelefoneAIgnorar(Origem, Destino: Boolean);
    procedure CalculaTotalGeral;
    procedure SomaLinhaTel;
    procedure ImportaLayoutGVT(CliInt: Integer);
    procedure ReopenPagtos();
  public
    { Public declarations }
  end;

var
  FmTelCtaCab: TFmTelCtaCab;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, MyDBCheck, ModuleGeral, TelCtaCadIgnor, MyVCLSkin,
  {$IfNDef NO_FINANCEIRO} ModuleFin, {$EndIf} UnPagtos, UnInternalConsts3;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmTelCtaCab.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmTelCtaCab.PMPagtoPopup(Sender: TObject);
var
  Enab, Enab2: Boolean;
begin
  Enab  := (QrTelCtasCab.State <> dsInactive) and (QrTelCtasCab.RecordCount > 0);
  Enab2 := (QrPagtos.State <> dsInactive) and (QrPagtos.RecordCount > 0);
  //
  Inclui1.Enabled := Enab and not Enab2;
  Exclui2.Enabled := Enab and Enab2;
end;

procedure TFmTelCtaCab.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrTelCtasCabCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmTelCtaCab.DefParams;
begin
  VAR_GOTOTABELA := 'telctacab';
  VAR_GOTOMYSQLTABLE := QrTelCtasCab;
  VAR_GOTONEG := gotoPos;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT cab.*, eci.CodCliInt, ');
  VAR_SQLx.Add('IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) CliInt_TXT');
  VAR_SQLx.Add('FROM telctacab cab ');
  VAR_SQLx.Add('LEFT JOIN enticliint eci ON eci.CodEnti = cab.CliInt');
  VAR_SQLx.Add('LEFT JOIN entidades ent ON ent.Codigo = cab.CliInt');
  VAR_SQLx.Add('WHERE cab.Codigo > 0');
  //
  VAR_SQL1.Add('AND cab.Codigo=:P0');
  //
  //VAR_SQL2.Add('AND cab.CodUsu=:P0');
  //
  //VAR_SQLa.Add('AND cab.Nome Like :P0');
  //
end;

procedure TFmTelCtaCab.Exclui1Click(Sender: TObject);
begin
  DBCheck.QuaisItens_Exclui(Dmod.QrUpd, QrTelCtasTel, DBGTel,
    'telctastel', ['Controle'], ['Controle'], istPergunta, '');
  //
  ReopenTelCtasTel(True, 0);
end;

procedure TFmTelCtaCab.Exclui2Click(Sender: TObject);
{$IfNDef NO_FINANCEIRO}
var
  Quitados: Integer;
begin
  Quitados := DModFin.FaturamentosQuitados(FTabLctA, FThisFatID, QrTelCtasCabCodigo.Value);
  //
  if Quitados > 0 then
  begin
    Geral.MB_Aviso('Existem ' + Geral.FF0(Quitados) +
      ' itens j� quitados que impedem a exclus�o deste faturamento');
    Exit;
  end;
  DBCheck.QuaisItens_Exclui(Dmod.QrUpd, QrPagtos, DBGrid4,
    FTabLctA, ['Controle'], ['Controle'], istPergunta, '');
  ReopenPagtos;
{$Else}
begin
  dmkPF.InfoSemModulo(TDmkModuloApp.mdlappFinanceiro);
{$EndIf}
end;

procedure TFmTelCtaCab.ExcluiTelefoneAIgnorar(Origem, Destino: Boolean);
  function VerificarEntidadeAIgnorar(Entidade: Integer): Boolean;
  begin
    //False = N�o Ingnora
    //True  = Ignora
    UnDmkDAC_PF.AbreMySQLQuery0(DModG.QrAux, Dmod.MyDB, [
    'SELECT Ignorar',
    'FROM telctaslnk ',
    'WHERE Entidade=' + Geral.FF0(Entidade),
    '']);
    if DModG.QrAux.RecordCount > 0 then
    begin
      if DModG.QrAux.FieldByName('Ignorar').AsInteger = 1 then
        Result := True
      else
        Result := False;      
    end else
      Result := False;
  end;
var
  Controle: Integer;
  Campo: String;
begin
  try
    Screen.Cursor := crHourGlass;
    //
    if Origem  then Campo := 'EntOrigem';
    if Destino then Campo := 'EntDestino';
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrLoc, Dmod.MyDB, [
    'SELECT Controle, ' + Campo,
    'FROM telctastel ',
    'WHERE Codigo=' + Geral.FF0(QrTelCtasCabCodigo.Value),
    '']);
    if QrLoc.RecordCount > 0 then
    begin
      QrLoc.First;
      while not QrLoc.Eof do
      begin
        Controle := QrLoc.FieldByName('Controle').AsInteger;
        //
        if VerificarEntidadeAIgnorar(QrLoc.FieldByName(Campo).AsInteger) then
          UMyMod.ExcluiRegistroInt1('', 'telctastel', 'Controle', Controle, Dmod.MyDB);
        //
        QrLoc.Next;
      end;
    end;
  finally
    ReopenTelCtasTel(False, 0);
    //
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmTelCtaCab.ExcluiTelefoneSemEntidade(Origem, Destino: Boolean);
  procedure ExcluiRegistros(Campo: String);
  var
    Controle: Integer;
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrLoc, Dmod.MyDB, [
    'SELECT Controle ',
    'FROM telctastel ',
    'WHERE ' + Campo + ' = 0 ',
    'AND Codigo = ' + Geral.FF0(QrTelCtasCabCodigo.Value), 
    '']);
    if QrLoc.RecordCount > 0 then
    begin
      QrLoc.First;
      while not QrLoc.Eof do
      begin
        Controle := QrLoc.FieldByName('Controle').AsInteger;
        //
        UmyMod.ExcluiRegistroInt1('', 'telctastel', 'Controle', Controle, Dmod.MyDB);
        //
        QrLoc.Next;
      end;
    end;
  end;
begin
  try
    Screen.Cursor := crHourGlass;
    //
    if Origem  then ExcluiRegistros('EntOrigem');
    if Destino then ExcluiRegistros('EntDestino');
  finally
    ReopenTelCtasTel(False, 0);
    //
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmTelCtaCab.Excluitodasentidadesdestinoaignorar1Click(
  Sender: TObject);
begin
  ExcluiTelefoneAIgnorar(False, True);
end;

procedure TFmTelCtaCab.Excluitodasentidadesorigemaignorar1Click(
  Sender: TObject);
begin
  ExcluiTelefoneAIgnorar(True, False);
end;

procedure TFmTelCtaCab.Excluitodosquenopossuirementidadededestino1Click(
  Sender: TObject);
begin
  //Exclui Destino
  if Geral.MensagemBox('Confirma a exclus�o de todos os itens sem entidade destino?',
    'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES
  then
    ExcluiTelefoneSemEntidade(False, True);
end;

procedure TFmTelCtaCab.Excluitodosquenopossuirementidadedeorigem1Click(
  Sender: TObject);
begin
  //Exclui Origem
  if Geral.MensagemBox('Confirma a exclus�o de todos os itens sem entidade origem?',
    'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES
  then
    ExcluiTelefoneSemEntidade(True, False);
end;

procedure TFmTelCtaCab.Cadastraentidade1Click(Sender: TObject);
begin
  DmodG.CadastroDeEntidade(QrTelCtasTelEntDestino.Value, fmcadEntidade2, fmcadEntidade2)
end;

procedure TFmTelCtaCab.Cadastraentidadeaignorar1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmTelCtaCadIgnor, FmTelCtaCadIgnor, afmoNegarComAviso) then
  begin
    FmTelCtaCadIgnor.ShowModal;
    FmTelCtaCadIgnor.Destroy;
  end;
end;

procedure TFmTelCtaCab.CalculaTotalGeral;
var
  Tel, Msg, Eve, Des, Total: Double;
begin
  Tel := Geral.DMV(EdTelTotal.ValueVariant);
  Msg := Geral.DMV(EdMsgTotal.ValueVariant);
  Eve := Geral.DMV(EdEveTotal.ValueVariant);
  Des := Geral.DMV(EdDesTotal.ValueVariant);
  //
  Total := Tel + Msg + Eve + Des;
  //
  FValorTotal     := Total;
  LaTotal.Caption := Geral.TFT(FloatToStr(Total), 2, siNegativo) 
end;

procedure TFmTelCtaCab.CBChamOrdemChange(Sender: TObject);
begin
  ReopenTelCtasTel(False, QrTelCtasTelControle.Value);
end;

procedure TFmTelCtaCab.CBChamOrdenarChange(Sender: TObject);
begin
  ReopenTelCtasTel(False, QrTelCtasTelControle.Value);
end;

procedure TFmTelCtaCab.ContaGVT1Click(Sender: TObject);
var
  CliInt: Integer;
begin
  if not DmodG.SelecionarCliInt(CliInt, True) then
    Exit;
  //
  ImportaLayoutGVT(CliInt);
end;

procedure TFmTelCtaCab.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmTelCtaCab.QueryPrincipalAfterOpen;
begin
end;

procedure TFmTelCtaCab.ReopenTotal(dmkEdit: TdmkEdit; Tabela, Campo: String);
var
  Total: Double;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrLoc, Dmod.MyDB, [
  'SELECT SUM(' + Campo + ') Total ',
  'FROM ' + Tabela,
  'WHERE Codigo=' + Geral.FF0(QrTelCtasCabCodigo.Value),
  '']);
  Total := QrLoc.FieldByName('Total').AsFloat;
  //
  if Total = 0 then
    dmkEdit.Font.Color := clBlack
  else if Total > 0 then
    dmkEdit.Font.Color := clRed
  else if Total < 0 then
    dmkEdit.Font.Color := clBlue;
  //
  dmkEdit.Font.Style   := [fsBold];
  dmkEdit.ValueVariant := Total;
end;

procedure TFmTelCtaCab.ReopenPagtos;
begin
{$IfNDef NO_FINANCEIRO}
  UnDmkDAC_PF.AbreMySQLQuery0(QrPagtos, Dmod.MyDB, [
    'SELECT la.Data, la.Vencimento, la.Debito, la.Banco, la.Agencia, ',
    'la.FatID, la.ContaCorrente, la.Documento, la.Descricao, ',
    'la.FatParcela, la.FatNum, la.Fornecedor, la.Carteira, la.Genero, ',
    'ca.Nome NOMECARTEIRA, ca.Nome2 NOMECARTEIRA2, ca.Banco1, ',
    'ca.Agencia1, ca.Conta1, ca.TipoDoc, la.Controle,',
    'CASE WHEN fo.Tipo=0 THEN fo.RazaoSocial ',
    'ELSE fo.Nome END NOMEFORNECEI,',
    'ca.Tipo CARTEIRATIPO, ',
    'IF(fr.Tipo=0, fr.RazaoSocial, fr.Nome) NOMEFORNECE ',
    'FROM ' + FTabLctA + ' la',
    'LEFT JOIN carteiras ca ON ca.Codigo=la.Carteira',
    'LEFT JOIN entidades fo ON fo.Codigo=ca.ForneceI',
    'LEFT JOIN entidades fr ON fr.Codigo=la.Fornecedor',
    'WHERE FatNum=' + Geral.FF0(QrTelCtasCabCodigo.Value),
    'AND FatID=' + Geral.FF0(FThisFatID),
    'ORDER BY la.FatParcela, la.Vencimento',
    '']);
{$EndIf}
end;

procedure TFmTelCtaCab.ReopenTelCtasDes(SoAtualizaTotal: Boolean; Controle: Integer);
begin
  if not SoAtualizaTotal then
  begin
    QrTelCtasDes.Close;
    QrTelCtasDes.Params[0].AsInteger := QrTelCtasCabCodigo.Value;
    UMyMod.AbreQuery(QrTelCtasDes, Dmod.MyDB, 'TFmTelCtaCab.ReopenTelCtasDes() > QrTelCtasDes');
  end;
  ReopenTotal(EdDesTotal, 'telctasdes', 'Valor');
  CalculaTotalGeral;
  if not SoAtualizaTotal then
  begin
    if Controle <> 0 then
      QrTelCtasDes.Locate('Controle', Controle, []);
  end;
end;

procedure TFmTelCtaCab.ReopenTelCtasEve(SoAtualizaTotal: Boolean; Controle: Integer);
begin
  if not SoAtualizaTotal then
  begin
    QrTelCtasEve.Close;
    QrTelCtasEve.Params[0].AsInteger := QrTelCtasCabCodigo.Value;
    UMyMod.AbreQuery(QrTelCtasEve, Dmod.MyDB, 'TFmTelCtaCab.ReopenTelCtasEve() > QrTelCtasEve');
  end;
  ReopenTotal(EdEveTotal, 'telctaseve', 'Valor');
  CalculaTotalGeral;
  if not SoAtualizaTotal then
  begin
    if Controle <> 0 then
      QrTelCtasEve.Locate('Controle', Controle, []);
  end;
end;

procedure TFmTelCtaCab.ReopenTelCtasMsg(SoAtualizaTotal: Boolean; Controle: Integer);
begin
  if not SoAtualizaTotal then
  begin
    QrTelCtasMsg.Close;
    QrTelCtasMsg.Params[0].AsInteger := QrTelCtasCabCodigo.Value;
    UMyMod.AbreQuery(QrTelCtasMsg, Dmod.MyDB, 'TFmTelCtaCab.ReopenTelCtasMsg() > QrTelCtasMsg');
  end;
  ReopenTotal(EdMsgTotal, 'telctasmsg', 'Valor');
  CalculaTotalGeral;
  if not SoAtualizaTotal then
  begin
    if Controle <> 0 then
      QrTelCtasMsg.Locate('Controle', Controle, []);
  end;
end;

procedure TFmTelCtaCab.ReopenTelCtasTel(SoAtualizaTotal: Boolean; Controle: Integer);
  procedure ReopenTotalDuracao(dmkEdit: TdmkEdit);
    function SecToTime(Sec: Integer): string;
    var
      H, M, S: string;
      Hor, Min, Seg: Integer;
    begin
      Hor := Sec div 3600;
      Min := Sec div 60 - Hor * 60;
      Seg := Sec - (Hor * 3600 + Min * 60) ;
      H := FormatFloat('0',Hor);
      M := FormatFloat('00',Min);
      S := FormatFloat('00',Seg);
      Result :=  H + ':' + M + ':' + S;
    end;
  var
    Total: Integer;
    Txt: String;
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrLoc, Dmod.MyDB, [
    'SELECT SUM(TIME_TO_SEC(Duracao)) Total ',
    'FROM telctastel ',
    'WHERE Codigo=' + Geral.FF0(QrTelCtasCabCodigo.Value),
    '']);
    Total := QrLoc.FieldByName('Total').AsInteger;
    Txt   := SecToTime(Total);                        
    //
    EdTelDurTot.ValueVariant := Txt;
  end;
var
  Ordenar, Ordem: String;
begin
  if not SoAtualizaTotal then
  begin
    case CBChamOrdenar.ItemIndex of
        0: Ordenar := 'tel.TelOrigem';
        1: Ordenar := 'ENTORIGNOME';
        2: Ordenar := 'IGNORARORIGEM';
        4: Ordenar := 'tel.Duracao';
        5: Ordenar := 'tel.Valor';
        6: Ordenar := 'tel.TelDestino';
        7: Ordenar := 'ENTDESTNOME';
        8: Ordenar := 'IGNORARDESTINO';
        9: Ordenar := 'tel.Prestadora';
       10: Ordenar := 'tel.Cidade';
       11: Ordenar := 'tel.UF';
       12: Ordenar := 'tel.TipoCham';
       13: Ordenar := 'tel.Horario';
       14: Ordenar := 'tel.Controle';
      else Ordenar := 'tel.DataHora';
    end;
    case CBChamOrdem.ItemIndex of
        1: Ordem := 'DESC';
      else Ordem := 'ASC';
    end;
    UnDmkDAC_PF.AbreMySQLQuery0(QrTelCtasTel, Dmod.MyDB, [
    'SELECT ',
    'IF (eno.Tipo=0, eno.RazaoSocial, eno.Nome) ENTORIGNOME, ',
    'IF (end.Tipo=0, end.RazaoSocial, end.Nome) ENTDESTNOME, ',
    '( ',
    'SELECT Ignorar + 0.000 ',
    'FROM telctaslnk ',
    'WHERE Entidade = tel.EntOrigem ',
    ') IGNORARORIGEM, ',
    '( ',
    'SELECT Ignorar + 0.000 ',
    'FROM telctaslnk ',
    'WHERE Entidade = tel.EntDestino ',
    ') IGNORARDESTINO, ',
    'tel.* ',
    'FROM telctastel tel ',
    'LEFT JOIN entidades eno ON eno.Codigo = tel.EntOrigem ',
    'LEFT JOIN entidades end ON end.Codigo = tel.EntDestino ',
    'WHERE tel.Codigo=' + Geral.FF0(QrTelCtasCabCodigo.Value),
    'ORDER BY ' + Ordenar + ' ' + Ordem,
    '']);
  end;
  ReopenTotalDuracao(EdTelDurTot);
  ReopenTotal(EdTelTotal, 'telctastel', 'Valor');
  EdTelChamTot.ValueVariant := QrTelCtasTel.RecordCount;
  CalculaTotalGeral;
  if not SoAtualizaTotal then
  begin
    if Controle <> 0 then
      QrTelCtasTel.Locate('Controle', Controle, []);
  end;
end;

procedure TFmTelCtaCab.DBGDesDrawColumnCell(Sender: TObject; const Rect: TRect;
  DataCol: Integer; Column: TColumn; State: TGridDrawState);
var
  Cor: TColor;
  Bold: Boolean;
begin
  if (Column.FieldName = 'Valor')then
  begin
    Bold := True;
    //
    if QrTelCtasDesValor.Value = 0 then
      Cor  := clBlack
    else if QrTelCtasDesValor.Value > 0 then
      Cor := clRed
    else
      Cor := clBlue;
    with DBGDes.Canvas do
    begin
      if Bold then Font.Style := [fsBold] else Font.Style := [];
      Font.Color := Cor;
      Column.Alignment := taRightJustify;
      FillRect(Rect);
      TextOut(Rect.Left+2,rect.Top+2,Column.Field.DisplayText);
    end;
  end;
end;

procedure TFmTelCtaCab.DBGEveDrawColumnCell(Sender: TObject; const Rect: TRect;
  DataCol: Integer; Column: TColumn; State: TGridDrawState);
var
  Cor: TColor;
  Bold: Boolean;
begin
  if (Column.FieldName = 'Valor')then
  begin
    Bold := True;
    //
    if QrTelCtasEveValor.Value = 0 then
      Cor  := clBlack
    else if QrTelCtasEveValor.Value > 0 then
      Cor := clRed
    else
      Cor := clBlue;
    with DBGEve.Canvas do
    begin
      if Bold then Font.Style := [fsBold] else Font.Style := [];
      Font.Color := Cor;
      FillRect(Rect);
      TextOut(Rect.Left+2,rect.Top+2,Column.Field.DisplayText);
    end;
  end;
end;

procedure TFmTelCtaCab.DBGMsgDrawColumnCell(Sender: TObject; const Rect: TRect;
  DataCol: Integer; Column: TColumn; State: TGridDrawState);
var
  Cor: TColor;
  Bold: Boolean;
begin
  if (Column.FieldName = 'Valor')then
  begin
    Bold := True;
    //
    if QrTelCtasMsgValor.Value = 0 then
      Cor  := clBlack
    else if QrTelCtasMsgValor.Value > 0 then
      Cor := clRed
    else
      Cor := clBlue;
    with DBGMsg.Canvas do
    begin
      if Bold then Font.Style := [fsBold] else Font.Style := [];
      Font.Color := Cor;
      FillRect(Rect);
      TextOut(Rect.Left+2,rect.Top+2,Column.Field.DisplayText);
    end;
  end;
end;

procedure TFmTelCtaCab.DBGTelDblClick(Sender: TObject);
var
  Campo: String;
begin
  if (QrTelCtasTel.State <> dsInactive) and (QrTelCtasTel.RecordCount > 0) then
  begin
    Campo := UpperCase(TDBGrid(Sender).Columns[THackDBGrid(Sender).Col -1].FieldName);
    //
    if Campo = 'ENTORIGNOME' then
      DmodG.CadastroDeEntidade(QrTelCtasTelEntOrigem.Value, fmcadEntidade2, fmcadEntidade2)
    else if Campo = 'ENTDESTNOME' then
      DmodG.CadastroDeEntidade(QrTelCtasTelEntDestino.Value, fmcadEntidade2, fmcadEntidade2);
  end;
end;

procedure TFmTelCtaCab.DBGTelDrawColumnCell(Sender: TObject; const Rect: TRect;
  DataCol: Integer; Column: TColumn; State: TGridDrawState);
var
  Cor: TColor;
  Bold: Boolean;
begin
  if Column.FieldName = 'IGNORARORIGEM' then
    MeuVCLSkin.DrawGrid(DBGTel, Rect, 1, Trunc(QrTelCtasTelIGNORARORIGEM.Value));
  //
  if Column.FieldName = 'IGNORARDESTINO' then
    MeuVCLSkin.DrawGrid(DBGTel, Rect, 1, Trunc(QrTelCtasTelIGNORARDESTINO.Value));
  //
  if (Column.FieldName = 'ENTORIGNOME')then
  begin
    if QrTelCtasTelEntOrigem.Value <> 0 then
    begin
      Cor  := clBlue;
      Bold := True;
    end else
    begin
      Cor  := clBlack;
      Bold := False;
    end;
    with DBGTel.Canvas do
    begin
      if Bold then Font.Style := [fsBold] else Font.Style := [];
      Font.Color := Cor;
      FillRect(Rect);
      TextOut(Rect.Left+2,rect.Top+2,Column.Field.DisplayText);
    end;
  end;
  if (Column.FieldName = 'ENTDESTNOME')then
  begin
    if QrTelCtasTelEntDestino.Value <> 0 then
    begin
      Cor  := clBlue;
      Bold := True;
    end else
    begin
      Cor  := clBlack;
      Bold := False;
    end;
    with DBGTel.Canvas do
    begin
      if Bold then Font.Style := [fsBold] else Font.Style := [];
      Font.Color := Cor;
      FillRect(Rect);
      TextOut(Rect.Left+2,rect.Top+2,Column.Field.DisplayText);
    end;
  end;
  if (Column.FieldName = 'Valor')then
  begin
    Bold := True;
    //
    if QrTelCtasTelValor.Value = 0 then
      Cor  := clBlack
    else if QrTelCtasTelValor.Value > 0 then
      Cor := clRed
    else
      Cor := clBlue;
    with DBGTel.Canvas do
    begin
      if Bold then Font.Style := [fsBold] else Font.Style := [];
      Font.Color := Cor;
      FillRect(Rect);
      TextOut(Rect.Left+2,rect.Top+2,Column.Field.DisplayText);
    end;
  end;
end;

procedure TFmTelCtaCab.DBGTelKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (key=VK_F9) then
    SomaLinhaTel;
end;

procedure TFmTelCtaCab.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmTelCtaCab.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmTelCtaCab.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmTelCtaCab.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmTelCtaCab.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmTelCtaCab.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmTelCtaCab.BtMsgExcluiClick(Sender: TObject);
begin
  DBCheck.QuaisItens_Exclui(Dmod.QrUpd, QrTelCtasMsg, DBGMsg,
    'telctasmsg', ['Controle'], ['Controle'], istPergunta, '');
  //
  ReopenTelCtasMsg(True, 0);
end;

procedure TFmTelCtaCab.BtPagtoClick(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 4;
  MyObjects.MostraPopUpDeBotao(PMPagto, BtPagto);
end;

procedure TFmTelCtaCab.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrTelCtasCabCodigo.Value;
  Close;
end;

procedure TFmTelCtaCab.BtTelConciliaClick(Sender: TObject);
  function LocalizaEntidadeTelefone(Telefone: String): Integer;
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrLoc, Dmod.MyDB, [
    'SELECT Codigo ',
    'FROM entidades ',
    'WHERE ETe1 LIKE "%' + Telefone + '%" ',
    'OR PTe1 LIKE "%' + Telefone + '%" ',
    '']);
    if QrLoc.RecordCount > 0 then
      Result := QrLoc.FieldByName('Codigo').AsInteger
    else
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(QrLoc, Dmod.MyDB, [
      'SELECT Codigo ',
      'FROM entitel ',
      'WHERE Telefone LIKE "%' + Telefone + '%" ',
      '']);
      if QrLoc.RecordCount > 0 then
        Result := QrLoc.FieldByName('Codigo').AsInteger
      else
        Result := 0;
    end;
  end;
var
  Controle, EntDestino, EntOrigem: Integer;
  TelDestino, TelOrigem: String;
begin
  PB1.Position := 0;
  PB1.Visible  := True;
  PB1.Max      := QrTelCtasTel.RecordCount;
  //
  QrTelCtasTel.First;
  while not QrTelCtasTel.Eof do
  begin
    Controle   := QrTelCtasTelControle.Value;
    TelDestino := QrTelCtasTelTelDestino.Value;
    TelOrigem  := QrTelCtasTelTelOrigem.Value;
    EntDestino := LocalizaEntidadeTelefone(TelDestino);
    EntOrigem  := LocalizaEntidadeTelefone(TelOrigem);
    //
    UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'telctastel', False, [
      'EntDestino', 'EntOrigem'], ['Controle'], [EntDestino, EntOrigem], [Controle], True);
    //
    PB1.Position := PB1.Position + 1;
    PB1.Update;
    Application.ProcessMessages;
    //
    QrTelCtasTel.Next;
  end;
  ReopenTelCtasTel(False, 0);
  PB1.Visible := False;
end;

procedure TFmTelCtaCab.BtTelEntidadeClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMTelEntidade, BtTelEntidade);
end;

procedure TFmTelCtaCab.BtTelExcluiClick(Sender: TObject);
begin
  if (QrTelCtasTel.State = dsInactive) or (QrTelCtasTel.RecordCount = 0) then
    Exit;
  //
  MyObjects.MostraPopUpDeBotao(PMTelExclui, BtTelExclui);
end;

procedure TFmTelCtaCab.BtDesExcluiClick(Sender: TObject);
begin
  DBCheck.QuaisItens_Exclui(Dmod.QrUpd, QrTelCtasDes, DBGDes,
    'telctasdes', ['Controle'], ['Controle'], istPergunta, '');
  //
  ReopenTelCtasDes(True, 0);
end;

procedure TFmTelCtaCab.BtEveExcluiClick(Sender: TObject);
begin
  DBCheck.QuaisItens_Exclui(Dmod.QrUpd, QrTelCtasEve, DBGEve,
    'telctaseve', ['Controle'], ['Controle'], istPergunta, '');
  //
  ReopenTelCtasEve(True, 0);
end;

procedure TFmTelCtaCab.BtExcluiClick(Sender: TObject);
var
  Codigo: Integer;
begin
  if (QrTelCtasCab.State <> dsInactive) and (QrTelCtasCab.RecordCount > 0) then
  begin
    if (QrTelCtasTel.State <> dsInactive) and (QrTelCtasTel.RecordCount > 0) then
    begin
      Geral.MB_Info('Exclus�o abortada!' + #13#10 + 'Esta conta possui chamadas cadastradas nela.');
      Exit;
    end;
    if (QrTelCtasMsg.State <> dsInactive) and (QrTelCtasMsg.RecordCount > 0) then
    begin
      Geral.MB_Info('Exclus�o abortada!' + #13#10 + 'Esta conta possui servi�os mensais cadastrados nela.');
      Exit;
    end;
    if (QrTelCtasEve.State <> dsInactive) and (QrTelCtasEve.RecordCount > 0) then
    begin
      Geral.MB_Info('Exclus�o abortada!' + #13#10 + 'Esta conta possui servi�os eventuais cadastrados nela.');
      Exit;
    end;
    if (QrTelCtasDes.State <> dsInactive) and (QrTelCtasDes.RecordCount > 0) then
    begin
      Geral.MB_Info('Exclus�o abortada!' + #13#10 + 'Esta conta possui descontos cadastrados nela.');
      Exit;
    end;
    //
    UmyMod.ExcluiRegistroInt1('Confirma a exclus�o da conta com vencimento em ' +
      Geral.FDT(QrTelCtasCabVencto.Value, 02) +'?',
    'telctacab', 'Codigo', QrTelCtasCabCodigo.Value, Dmod.MyDB);
    //
    Codigo := GOTOy.LocalizaPriorNextIntQr(QrTelCtasCab, QrTelCtasCabCodigo, QrTelCtasCabCodigo.Value);
    //
    LocCod(Codigo, Codigo);
  end;
end;

procedure TFmTelCtaCab.BtIncluiClick(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 0;
  MyObjects.MostraPopUpDeBotao(PMInclui, BtInclui);
end;

procedure TFmTelCtaCab.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  PB1.Visible     := False;
  //
  PageControl1.ActivePageIndex := 0;
  //
  CBChamOrdem.ItemIndex   := 0;
  CBChamOrdenar.ItemIndex := 2;
  //
  FThisFatID  := VAR_FATID_0070;
  FValorTotal := 0;
  //
  CriaOForm;
end;

procedure TFmTelCtaCab.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrTelCtasCabCodigo.Value, LaRegistro.Caption);
end;

procedure TFmTelCtaCab.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmTelCtaCab.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrTelCtasCabCodigo.Value, LaRegistro.Caption);
end;

procedure TFmTelCtaCab.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmTelCtaCab.QrTelCtasCabAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmTelCtaCab.QrTelCtasCabAfterScroll(DataSet: TDataSet);
begin
  if QrTelCtasCabCliInt.Value <> 0 then
    FTabLctA := DmodG.NomeTab(TMeuDB, ntLct, False, ttA, QrTelCtasCabCodCliInt.Value)
  else
    FTabLctA := sTabLctErr;
  //
  ReopenTelCtasTel(False, 0);
  ReopenTelCtasMsg(False, 0);
  ReopenTelCtasEve(False, 0);
  ReopenTelCtasDes(False, 0);
  ReopenPagtos();
end;

procedure TFmTelCtaCab.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmTelCtaCab.SbQueryClick(Sender: TObject);
begin
  LocCod(QrTelCtasCabCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'telctacab', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmTelCtaCab.SomaLinhaTel;
var
  Valor: Double;
  i: Integer;
begin
  Valor := 0;
  //
  if DBGTel.SelectedRows.Count = 0 then
      Valor := QrTelCtasTelValor.Value;
  with DBGTel.DataSource.DataSet do
  begin
    for i:= 0 to DBGTel.SelectedRows.Count-1 do
    begin
      GotoBookmark(DBGTel.SelectedRows.Items[i]);
      //
      Valor := Valor + QrTelCtasTelValor.Value;
    end;
  end;
  EdTelSomaLinha.ValueVariant := Valor;
end;

procedure TFmTelCtaCab.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmTelCtaCab.ImportaLayoutGVT(CliInt: Integer);
  function FormataHora(Hora: String): String;
  begin
    Result := Copy(Hora, 1, 2) + ':' + Copy(Hora, 3, 2) + ':' + Copy(Hora, 5, 2);
  end;
  function FormataDataHora(DataHora: String): String;
  begin
    Result := Copy(DataHora, 1, 4) + '-'  + Copy(DataHora, 5, 2) + '-' +
              Copy(DataHora, 7, 2) + ' '  + Copy(DataHora, 9, 2) + ':' +
              Copy(DataHora, 11, 2) + ':' + Copy(DataHora, 13, 2);
  end;
  function FormataData(Data: String): String;
  begin
    Result := Copy(Data, 1, 4) + '-' + Copy(Data, 5, 2) + '-' + Copy(Data, 7, 2);
  end;
  //Chamadas
  procedure IncluiTelCtasTel(Codigo: Integer; Linha, Prestadora: String);
  var
    Lista: array of string;
    Lin, Reg, DataHora, Duracao, TelDestino, TelOrigem, Cidade, UF, Tipo,
    TipoCham, Horario, Vencto: String;
    I, N, Controle: Integer;
    Valor: Double;
  begin
    Lin   := Linha;
    N     := 0;
    Valor := 0;
    //
    if Pos('ORIGEM:', AnsiUpperCase(Linha)) = 0 then
    begin
      while Lin <> '' do
      begin
        N := N + 1;
        SetLength(Lista, N);
        Geral.SeparaPrimeiraOcorrenciaDeTexto(#9, Lin, Reg, Lin);
        Lista[N - 1] := Reg;
      end;
      if Length(Lista) = 11 then
      begin
        for I := 0 to N - 1 do
        begin
          case I of
             0: TelOrigem  := Utf8ToAnsi(Trim(Lista[I]));
             1: DataHora   := FormataDataHora(Lista[I]);
             2: Duracao    := FormataHora(Lista[I]);
             3: TelDestino := Utf8ToAnsi(Trim(Lista[I]));
             4: Cidade     := Utf8ToAnsi(Trim(Lista[I]));
             5: UF         := Utf8ToAnsi(Trim(Lista[I]));
             6: Tipo       := Utf8ToAnsi(Trim(Lista[I]));
             7: TipoCham   := Utf8ToAnsi(Trim(Lista[I]));
             8: Horario    := FormataHora(Lista[I]);
             9: Valor      := Geral.DMV(Lista[I]) / 100;
            10: Vencto     := FormataData(Lista[I]);
          end;
        end;
        Controle := UMyMod.BuscaEmLivreY_Def('telctastel', 'Controle', stIns, 0, nil);
        //
        UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'telctastel', False, [
          'Prestadora', 'TelOrigem', 'DataHora', 'Duracao', 'TelDestino',
          'Cidade', 'UF', 'Tipo', 'TipoCham', 'Horario', 'Valor', 'Vencto',
          'Codigo'], ['Controle'], [
          Prestadora, TelOrigem, DataHora, Duracao, TelDestino,
          Cidade, UF, Tipo, TipoCham, Horario, Valor, Vencto,
          Codigo], [Controle], True);
      end;
    end;
  end;
  //Descontos
  procedure IncluiTelCtasDes(Codigo: Integer; Linha, Prestadora: String);
  var
    Lista: array of string;
    Lin, Reg, Descricao: String;
    I, N, Controle: Integer;
    Valor: Double;
  begin
    Lin   := Linha;
    N     := 0;
    Valor := 0;
    //
    if Pos('DESCRI��O:', AnsiUpperCase(Linha)) = 0 then
    begin
      while Lin <> '' do
      begin
        N := N + 1;
        SetLength(Lista, N);
        Geral.SeparaPrimeiraOcorrenciaDeTexto(#9, Lin, Reg, Lin);
        Lista[N - 1] := Reg;
      end;
      if Length(Lista) = 2 then
      begin
        for I := 0 to N - 1 do
        begin
          case I of
            0: Descricao := Utf8ToAnsi(Trim(Lista[I]));
            1: Valor     := Geral.DMV(Lista[I]) / 100;
          end;
        end;
        Controle := UMyMod.BuscaEmLivreY_Def('telctasdes', 'Controle', stIns, 0, nil);
        //
        UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'telctasdes', False, [
          'Prestadora', 'Descricao', 'Valor', 'Codigo'], ['Controle'],
          [Prestadora, Descricao, Valor, Codigo], [Controle], True);
      end;
    end;
  end;
  //Servi�os Eventuais
  procedure IncluiTelCtasEve(Codigo: Integer; Linha, Prestadora: String);
  var
    Lista: array of string;
    Lin, Reg, Tipo, ID, Descricao: String;
    I, N, Controle: Integer;
    Valor: Double;
  begin
    Lin   := Linha;
    N     := 0;
    Valor := 0;
    //
    if Pos('TIPO:', AnsiUpperCase(Linha)) = 0 then
    begin
      while Lin <> '' do
      begin
        N := N + 1;
        SetLength(Lista, N);
        Geral.SeparaPrimeiraOcorrenciaDeTexto(#9, Lin, Reg, Lin);
        Lista[N - 1] := Reg;
      end;
      if Length(Lista) = 5 then
      begin
        for I := 0 to N - 1 do
        begin
          case I of
            0: Tipo      := Utf8ToAnsi(Trim(Lista[I]));
            1: ID        := Utf8ToAnsi(Trim(Lista[I]));
            2: Descricao := Utf8ToAnsi(Trim(Lista[I]));
            3: Valor     := Geral.DMV(Lista[I]) / 100;
          end;
        end;
        Controle := UMyMod.BuscaEmLivreY_Def('telctaseve', 'Controle', stIns, 0, nil);
        //
        UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'telctaseve', False, [
          'Prestadora', 'Tipo', 'ID', 'Descricao', 'Valor', 'Codigo'],
          ['Controle'], [Prestadora, Tipo, ID, Descricao, Valor, Codigo],
          [Controle], True);
      end;
    end;
  end;
  //Servi�os Mensais
  procedure IncluiTelCtasMsg(Codigo: Integer; Linha, Prestadora: String);
  var
    Lista: array of string;
    Lin, Reg, Tipo, ID, Descricao, DataIni, DataFim: String;
    I, N, Controle: Integer;
    Valor: Double;
  begin
    Lin   := Linha;
    N     := 0;
    Valor := 0;
    //
    if Pos('TIPO:', AnsiUpperCase(Linha)) = 0 then
    begin
      while Lin <> '' do
      begin
        N := N + 1;
        SetLength(Lista, N);
        Geral.SeparaPrimeiraOcorrenciaDeTexto(#9, Lin, Reg, Lin);
        Lista[N - 1] := Reg;
      end;
      if Length(Lista) = 6 then
      begin
        for I := 0 to N - 1 do
        begin
          case I of
            0: Tipo      := Utf8ToAnsi(Trim(Lista[I]));
            1: ID        := Utf8ToAnsi(Trim(Lista[I]));
            2: Descricao := Utf8ToAnsi(Trim(Lista[I]));
            3: DataIni   := FormataData(Lista[I]);
            4: DataFim   := FormataData(Lista[I]);
            5: Valor     := Geral.DMV(Lista[I]) / 100;
          end;
        end;
        Controle := UMyMod.BuscaEmLivreY_Def('telctasmsg', 'Controle', stIns, 0, nil);
        //
        UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'telctasmsg', False, [
          'Prestadora', 'Tipo', 'ID', 'Descricao', 'DataIni', 'DataFim', 'Valor',
          'Codigo'], ['Controle'],
          [Prestadora, Tipo, ID, Descricao, DataIni, DataFim, Valor, Codigo],
          [Controle], True);
      end;
    end;
  end;
  //Cabe�alho
  function IncluiTelCtaCab(CodCab, Linha: Integer; ContaTel: TStringList): Integer;
    function InsereDados(ValorPri: Integer; Tabela, CampoPri, Campo: String;
      Valor: Variant; SQLType: TSQLType): Integer;
    begin
      Result := 0;
      //
      if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, Tabela, False, [
        Campo], [CampoPri], [Valor], [ValorPri], True)
      then
        Result := ValorPri;
    end;
  var
    Codigo: Integer;
    SQLType: TSQLType;
    NumFatura, ContaCobr, IniVigen, FimVigen, Emissao, Vencto, Cliente, UF: String;
  begin
    NumFatura := '0';
    ContaCobr := '';
    FimVigen  := '0000-00-00';
    Emissao   := '0000-00-00';
    Vencto    := '0000-00-00';
    Cliente   := '';
    UF        := '';
    Codigo    := 0;
    //
    if Linha = 0 then
    begin
      Codigo  := UMyMod.BuscaEmLivreY_Def('telctacab', 'Codigo', stIns, Codigo, nil);
      SQLType := stIns;
    end else
    begin
      Codigo  := CodCab;
      SQLType := stUpd;
    end;
    //
    case Linha of
      0:
      begin
        ContaCobr := Trim(ContaTel[Linha]);
        Result    := InsereDados(Codigo, 'telctacab', 'Codigo', 'ContaCobr', ContaCobr, SQLType);
      end;
      1:
      begin
        IniVigen  := FormataData(Trim(ContaTel[Linha]));
        Result    := InsereDados(Codigo, 'telctacab', 'Codigo', 'IniVigen', IniVigen, SQLType);
      end;
      2:
      begin
        FimVigen := FormataData(Trim(ContaTel[Linha]));
        Result   := InsereDados(Codigo, 'telctacab', 'Codigo', 'FimVigen', FimVigen, SQLType);
      end;
      3:
      begin
        Cliente := Trim(ContaTel[Linha]);
        Result  := InsereDados(Codigo, 'telctacab', 'Codigo', 'Cliente', Cliente, SQLType);
      end;
      4:
      begin
        Vencto := FormataData(Trim(ContaTel[Linha]));
        Result := InsereDados(Codigo, 'telctacab', 'Codigo', 'Vencto', Vencto, SQLType);
      end;
      5:
      begin
        Emissao := FormataData(Trim(ContaTel[Linha]));
        Result  := InsereDados(Codigo, 'telctacab', 'Codigo', 'Emissao', Emissao, SQLType);
      end;
      6:
      begin
        UF     := Trim(ContaTel[Linha]);
        Result := InsereDados(Codigo, 'telctacab', 'Codigo', 'UF', UF, SQLType);
      end;
      7:
      begin
        NumFatura := Trim(ContaTel[Linha]);
        Result    := InsereDados(Codigo, 'telctacab', 'Codigo', 'NumFatura', NumFatura, SQLType);
      end;
      else
        Result := Codigo;
    end;
  end;
var
  ContaTel: TStringList;
  Arquivo, Prestadora, Linha, Titulo: String;
  i, Codigo, Controle: Integer;
begin
  if MyObjects.FIC(CliInt = 0, nil, 'Empresa n�o definida!') then Exit;  
  //
  if MyObjects.FileOpenDialog(Self, '', '', 'Caminho', '', [], Arquivo) then
  begin
    if Length(Arquivo) > 0 then
    begin
      Codigo   := 0;
      Controle := 0;
      ContaTel := TStringList.Create;
      Titulo   := 'Header';
      ContaTel.LoadFromFile(Arquivo);
      //
      PB1.Max      := ContaTel.Count;
      PB1.Position := 0;
      PB1.Visible  := True;
      //
      for i := 0 to ContaTel.Count - 1 do
      begin
        Linha := ContaTel[i];
        //
        if (Length(Linha) = 0) and (Titulo = 'Header') then
          Titulo := '';
        //
        if Titulo = 'Header' then
        begin
          try
            Codigo := IncluiTelCtaCab(Codigo, i, ContaTel);
          except
            Geral.MB_Aviso('Importa��o cancelada!' + sLineBreak + 'Motivo: Este arquivo j� foi importado!');
            Exit;
          end;
        end else
        begin
          if Pos('PRESTADORA', AnsiUpperCase(Linha)) = 1 then
            Prestadora := StringReplace(Linha, 'PRESTADORA', '', [rfReplaceAll, rfIgnoreCase]);
          //
          if Pos('PRESTADORA', AnsiUpperCase(Linha)) = 0 then
          begin
            if (Length(Prestadora) > 0) and (Pos('SERVI�OS MENSAIS', AnsiUpperCase(Linha)) = 1) then
              Titulo := 'Servi�os Mensais'
            else if (Length(Prestadora) > 0) and (Pos('SERVI�OS EVENTUAIS', AnsiUpperCase(Linha)) = 1) then
              Titulo := 'Servi�os Eventuais'
            else if (Length(Prestadora) > 0) and (Pos('DESCONTOS', AnsiUpperCase(Linha)) = 1) then
              Titulo := 'Descontos'
            else if (Length(Prestadora) > 0) and (Pos('CHAMADAS', AnsiUpperCase(Linha)) = 1) then
              Titulo := 'Chamadas';
            //
            if Titulo = 'Servi�os Mensais' then
            begin
              if Pos('SERVI�OS MENSAIS', AnsiUpperCase(Linha)) = 0 then
              begin
                if (Length(Prestadora) > 0) and (Titulo = 'Servi�os Mensais') and
                  (Length(Linha) > 0) and (Linha <> #9) and (Codigo <> 0)
                then
                  IncluiTelCtasMsg(Codigo, Linha, Prestadora);
              end;
            end;
            if Titulo = 'Servi�os Eventuais' then
            begin
              if Pos('SERVI�OS EVENTUAIS', AnsiUpperCase(Linha)) = 0 then
              begin
                if (Length(Prestadora) > 0) and (Titulo = 'Servi�os Eventuais') and
                  (Length(Linha) > 0) and (Linha <> #9) and (Codigo <> 0)
                then
                  IncluiTelCtasEve(Codigo, Linha, Prestadora);
              end;
            end;
            if Titulo = 'Descontos' then
            begin
              if Pos('DESCONTOS', AnsiUpperCase(Linha)) = 0 then
              begin
                if (Length(Prestadora) > 0) and (Titulo = 'Descontos') and
                  (Length(Linha) > 0) and (Linha <> #9) and (Codigo <> 0)
                then
                  IncluiTelCtasDes(Codigo, Linha, Prestadora);
              end;
            end;
            if Titulo = 'Chamadas' then
            begin
              if Pos('CHAMADAS', AnsiUpperCase(Linha)) = 0 then
              begin
                if (Length(Prestadora) > 0) and (Titulo = 'Chamadas') and
                  (Length(Linha) > 0) and (Linha <> #9) and (Codigo <> 0)
                then
                  IncluiTelCtasTel(Codigo, Linha, Prestadora);
              end;
            end;
          end;
        end;
        PB1.Position := PB1.Position + 1;
        PB1.Update;
        Application.ProcessMessages;
      end;
      UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrAux, Dmod.MyDB, [
        'UPDATE telctacab SET CliInt=' + Geral.FF0(CliInt),
        'WHERE Codigo=' + Geral.FF0(Codigo),
        '']);
      //
      ContaTel.Free;
      PB1.Visible := False;
      LocCod(Codigo, Codigo);
      Geral.MB_Info('Importa��o conclu�da!');
    end;
  end;
end;

procedure TFmTelCtaCab.Inclui1Click(Sender: TObject);
{$IfNDef NO_FINANCEIRO}
  procedure ObtemDados(var Fornecedor, Carteira, Genero: Integer);
  var
    Codigo: Integer;
    Qry: TmySQLQuery;
  begin
    Fornecedor := 0;
    Carteira   := 0;
    Genero     := 0;
    //
    if (QrPagtos.State <> dsInactive) and (QrPagtos.RecordCount > 0) then
    begin
      Fornecedor := QrPagtosFornecedor.Value;
      Carteira   := QrPagtosCarteira.Value;
      Genero     := QrPagtosGenero.Value;
    end else
    begin
      Qry := TmySQLQuery.Create(TDataModule(Dmod.MyDB.Owner));
      try
        UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
          'SELECT Codigo ',
          'FROM telctacab ',
          'ORDER BY Codigo DESC LIMIT 2 ',
          '']);
        if Qry.RecordCount = 2 then
        begin
          Qry.Next;
          Codigo := Qry.FieldByName('Codigo').AsInteger;
          //
          UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
            'SELECT Fornecedor, Carteira, Genero ',
            'FROM ' + FTabLctA,
            'WHERE FatNum=' + Geral.FF0(Codigo),
            'AND FatID=' + Geral.FF0(FThisFatID),
            '']);
          Fornecedor := Qry.FieldByName('Fornecedor').AsInteger;
          Carteira   := Qry.FieldByName('Carteira').AsInteger;
          Genero     := Qry.FieldByName('Genero').AsInteger;
        end;
      finally
        Qry.Free;
      end;
    end;
  end;
{$EndIf}
var
  Terceiro, Carteira, Genero, Codigo, Empresa: Integer;
  Valor: Double;
  Descricao: String;
begin
{$IfNDef NO_FINANCEIRO}
  ObtemDados(Terceiro, Carteira, Genero);
  //
  Codigo        := QrTelCtasCabCodigo.Value;
  IC3_ED_FatNum := Codigo;
  IC3_ED_NF     := 0;
  IC3_ED_Data   := Date;
  IC3_ED_Vencto := Date;
  Descricao     := 'Fatura conta telef�nica n� ' + Geral.FF0(QrTelCtasCabNumFatura.Value);
  Empresa       := QrTelCtasCabCliInt.Value;
  Valor         := FValorTotal;
  //
  UPagtos.Pagto(QrPagtos, tpDeb, Codigo, Terceiro, FThisFatID, Genero, 0, stIns,
    'Pagto. Conta Telef�nica', Valor, VAR_USUARIO, 0, Empresa, mmNenhum, 0, 0,
    True, True, Carteira, 0, 0, 0, 0, FTabLctA, Descricao);
  //
  ReopenPagtos;
{$Else}
  dmkPF.InfoSemModulo(TDmkModuloApp.mdlappFinanceiro);
{$EndIf}
end;

procedure TFmTelCtaCab.QrTelCtasCabBeforeClose(DataSet: TDataSet);
begin
  QrTelCtasTel.Close;
  QrTelCtasMsg.Close;
  QrTelCtasEve.Close;
  QrTelCtasDes.Close;
end;

procedure TFmTelCtaCab.QrTelCtasCabBeforeOpen(DataSet: TDataSet);
begin
  QrTelCtasCabCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmTelCtaCab.QrTelCtasTelCalcFields(DataSet: TDataSet);
begin
  QrTelCtasTelTELORIGEM_TXT.Value  := Geral.FormataTelefone_TT_Curto(QrTelCtasTelTelOrigem.Value);
  QrTelCtasTelTELDESTINO_TXT.Value := Geral.FormataTelefone_TT_Curto(QrTelCtasTelTelDestino.Value);
end;

end.

