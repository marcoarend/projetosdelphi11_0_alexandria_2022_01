unit Zero800Cab;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, DmkDAC_PF,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnInternalConsts,
  dmkImage, dmkDBGrid, Menus, Mask, UnDmkEnums;

type
  TFmZero800Cab = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtGerencia: TBitBtn;
    LaTitulo1C: TLabel;
    Splitter1: TSplitter;
    Panel5: TPanel;
    PMGerencia: TPopupMenu;
    Inclui1: TMenuItem;
    Alteraselecionado1: TMenuItem;
    Excluiselecionado1: TMenuItem;
    QrZero800CabEnt: TmySQLQuery;
    DsZero800CabEnt: TDataSource;
    QrZero800CabEntEntidade: TIntegerField;
    QrZero800CabEntENTNOME: TWideStringField;
    DsZero800Cab: TDataSource;
    QrZero800Cab: TmySQLQuery;
    QrZero800CabCodigo: TIntegerField;
    QrZero800CabEntidade: TIntegerField;
    QrZero800CabProtoOri: TWideStringField;
    QrZero800CabProtoNum: TWideStringField;
    QrZero800CabAtendente: TWideStringField;
    QrZero800CabMatricula: TWideStringField;
    QrZero800CabDataHora: TDateTimeField;
    QrZero800CabLk: TIntegerField;
    QrZero800CabDataCad: TDateField;
    QrZero800CabDataAlt: TDateField;
    QrZero800CabUserCad: TIntegerField;
    QrZero800CabUserAlt: TIntegerField;
    QrZero800CabAlterWeb: TSmallintField;
    QrZero800CabAtivo: TSmallintField;
    dmkDBGrid3: TdmkDBGrid;
    Splitter2: TSplitter;
    QrZero800Its: TmySQLQuery;
    DsZero800Its: TDataSource;
    QrZero800ItsCodigo: TIntegerField;
    QrZero800ItsControle: TIntegerField;
    QrZero800ItsNome: TWideStringField;
    QrZero800ItsQuem: TSmallintField;
    QrZero800ItsDataHora: TDateTimeField;
    QrZero800ItsLk: TIntegerField;
    QrZero800ItsDataCad: TDateField;
    QrZero800ItsDataAlt: TDateField;
    QrZero800ItsUserCad: TIntegerField;
    QrZero800ItsUserAlt: TIntegerField;
    QrZero800ItsAlterWeb: TSmallintField;
    QrZero800ItsAtivo: TSmallintField;
    QrZero800ItsDATAHORA_TXT: TWideStringField;
    QrZero800ItsAtendente: TWideStringField;
    QrZero800ItsQUEM_TXT: TWideStringField;
    Panel6: TPanel;
    dmkDBGrid2: TdmkDBGrid;
    Panel7: TPanel;
    BtExclui: TBitBtn;
    DBMemo1: TDBMemo;
    StaticText6: TStaticText;
    QrLoc: TmySQLQuery;
    Panel8: TPanel;
    dmkDBGrid1: TdmkDBGrid;
    EdPesq: TdmkEdit;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtGerenciaClick(Sender: TObject);
    procedure Inclui1Click(Sender: TObject);
    procedure QrZero800CabEntAfterScroll(DataSet: TDataSet);
    procedure QrZero800CabEntBeforeClose(DataSet: TDataSet);
    procedure Alteraselecionado1Click(Sender: TObject);
    procedure QrZero800ItsCalcFields(DataSet: TDataSet);
    procedure QrZero800CabAfterScroll(DataSet: TDataSet);
    procedure QrZero800CabBeforeClose(DataSet: TDataSet);
    procedure Excluiselecionado1Click(Sender: TObject);
    procedure BtExcluiClick(Sender: TObject);
    procedure EdPesqChange(Sender: TObject);
  private
    { Private declarations }
    procedure MostraZero800Its(SQLType: TSQLType; Codigo: Integer);
  public
    { Public declarations }
    procedure ReopenZero800CabEnt(Nome: String; Entidade: Integer);
    procedure ReopenZero800Cab(Codigo, Entidade: Integer);
    procedure ReopenZero800Its(Codigo, Controle: Integer);
  end;

  var
  FmZero800Cab: TFmZero800Cab;

implementation

uses UnMyObjects, Module, UMySQLModule, Zero800Its, MyDBCheck, Zero800ItsDel;

{$R *.DFM}

procedure TFmZero800Cab.Alteraselecionado1Click(Sender: TObject);
begin
  MostraZero800Its(stUpd, QrZero800CabCodigo.Value);
end;

procedure TFmZero800Cab.BtExcluiClick(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmZero800ItsDel, FmZero800ItsDel, afmoNegarComAviso) then
  begin
    FmZero800ItsDel.ShowModal;
    FmZero800ItsDel.Destroy;
  end;
end;

procedure TFmZero800Cab.BtGerenciaClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMGerencia, BtGerencia);
end;

procedure TFmZero800Cab.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmZero800Cab.EdPesqChange(Sender: TObject);
begin
  ReopenZero800CabEnt(EdPesq.ValueVariant, 0);
end;

procedure TFmZero800Cab.Excluiselecionado1Click(Sender: TObject);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrLoc, Dmod.MyDB, [
    'SELECT Controle ',
    'FROM zero800its ',
    'WHERE Codigo=' + Geral.FF0(QrZero800CabCodigo.Value) + ' ',
  '']);
  if QrLoc.RecordCount > 0 then
  begin
    Geral.MB_Aviso('Exclusão cancelada!' + sLineBreak +
    'Motivo: Este protocolo possui comentários cadastrados nele!' +
    sLineBreak + 'Exclua os comentários e tente novamente.');
    Exit;
  end;
  if UMyMod.ExcluiRegistros('Deseja excluir o protocolo ' +
    QrZero800CabProtoNum.Value + '?', Dmod.QrUpd, 'zero800cab', ['Codigo'],
    ['='], [QrZero800CabCodigo.Value], '') then
  begin
    ReopenZero800Cab(0, QrZero800CabEntidade.Value);
    ReopenZero800CabEnt('', QrZero800CabEntidade.Value);
  end;
end;

procedure TFmZero800Cab.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmZero800Cab.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType  := stLok;
  BtExclui.Enabled := False;
  //
  ReopenZero800CabEnt('', 0);
end;

procedure TFmZero800Cab.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmZero800Cab.Inclui1Click(Sender: TObject);
begin
  MostraZero800Its(stIns, 0);
end;

procedure TFmZero800Cab.MostraZero800Its(SQLType: TSQLType; Codigo: Integer);
begin
  if DBCheck.CriaFm(TFmZero800Its, FmZero800Its, afmoNegarComAviso) then
  begin
    FmZero800Its.ImgTipo.SQLType := SQLType;
    FmZero800Its.FCodigo         := Codigo;
    FmZero800Its.ShowModal;
    FmZero800Its.Destroy;
  end;
end;

procedure TFmZero800Cab.QrZero800CabAfterScroll(DataSet: TDataSet);
begin
  ReopenZero800Its(QrZero800CabCodigo.Value, 0);
end;

procedure TFmZero800Cab.QrZero800CabBeforeClose(DataSet: TDataSet);
begin
  QrZero800Its.Close;
  BtExclui.Enabled := False;
end;

procedure TFmZero800Cab.QrZero800CabEntAfterScroll(DataSet: TDataSet);
begin
  ReopenZero800Cab(0, QrZero800CabEntEntidade.Value);
end;

procedure TFmZero800Cab.QrZero800CabEntBeforeClose(DataSet: TDataSet);
begin
  QrZero800Cab.Close;
end;

procedure TFmZero800Cab.QrZero800ItsCalcFields(DataSet: TDataSet);
begin
  if QrZero800ItsQuem.Value = 0 then
    QrZero800ItsQUEM_TXT.Value := 'Cadastrante'
  else
    QrZero800ItsQUEM_TXT.Value := 'Atendente';
end;

procedure TFmZero800Cab.ReopenZero800Cab(Codigo, Entidade: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrZero800Cab, Dmod.MyDB, [
    'SELECT * ',
    'FROM zero800cab ',
    'WHERE Entidade=' + Geral.FF0(Entidade) + ' ',
    'ORDER BY DataHora DESC ',
  '']);
  //
  if Codigo <> 0 then
    QrZero800Cab.Locate('Codigo', Codigo, []);
end;

procedure TFmZero800Cab.ReopenZero800CabEnt(Nome: String; Entidade: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrZero800CabEnt, Dmod.MyDB, [
    'SELECT cab.Entidade, ',
    'IF (ent.Tipo=0, ent.RazaoSocial, ent.Nome) ENTNOME ',
    'FROM zero800cab cab ',
    'LEFT JOIN entidades ent ON ent.Codigo = cab.Entidade ',
    'WHERE IF (ent.Tipo=0, ent.RazaoSocial, ent.Nome) LIKE "%'+ Nome +'%" ',
    'GROUP BY ENTNOME ',
    '']);
  //
  if Entidade <> 0 then
    QrZero800CabEnt.Locate('Entidade', Entidade, []);
end;

procedure TFmZero800Cab.ReopenZero800Its(Codigo, Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrZero800Its, Dmod.MyDB, [
    'SELECT its.*, ',
    'DATE_FORMAT(its.DataHora, "%d/%m/%Y %H:%i:%s") DATAHORA_TXT, ',
    'cab.Atendente ',
    'FROM zero800its its ',
    'LEFT JOIN zero800cab cab ON cab.Codigo = its.Codigo ',
    'WHERE its.Codigo=' + Geral.FF0(Codigo),
    'ORDER BY its.DataHora DESC ',
    '']);
  //
  BtExclui.Enabled := QrZero800Its.RecordCount > 0;
  //
  if Controle <> 0 then
    QrZero800Its.Locate('Controle', Controle, []);
end;

end.
