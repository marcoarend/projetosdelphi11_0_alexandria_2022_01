unit TelCtaCadIgnor;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2,
  ResIntStrings, UnMLAGeral, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, dmkGeral,
  dmkPermissoes, dmkEdit, dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup,
  unDmkProcFunc, dmkDBLookupComboBox, dmkEditCB, dmkValUsu, UnDmkEnums;

type
  TFmTelCtaCadIgnor = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    BtExclui: TBitBtn;
    BtAltera: TBitBtn;
    BtInclui: TBitBtn;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    QrTelCtasLnk: TmySQLQuery;
    DsTelCtasLnk: TDataSource;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    Label9: TLabel;
    Label1: TLabel;
    DBEdCodigo: TdmkDBEdit;
    Label2: TLabel;
    DBEdNome: TdmkDBEdit;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    QrTelCtasLnkCodigo: TIntegerField;
    QrTelCtasLnkEntidade: TIntegerField;
    QrTelCtasLnkIgnorar: TIntegerField;
    DBRadioGroup1: TDBRadioGroup;
    QrTelCtasLnkNOMEENT: TWideStringField;
    EdEntidade: TdmkEditCB;
    CBEntidade: TdmkDBLookupComboBox;
    SpeedButton5: TSpeedButton;
    QrEntidades: TmySQLQuery;
    QrEntidadesCodigo: TIntegerField;
    QrEntidadesNome: TWideStringField;
    QrEntidadesCodUsu: TIntegerField;
    DsEntidades: TDataSource;
    VUEntidade: TdmkValUsu;
    RGIgnorar: TdmkRadioGroup;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrTelCtasLnkAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrTelCtasLnkBeforeOpen(DataSet: TDataSet);
    procedure BtIncluiClick(Sender: TObject);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure SpeedButton5Click(Sender: TObject);
    procedure BtExcluiClick(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure LocCod(Atual, Codigo: Integer);
    procedure Va(Para: TVaiPara);
  public
    { Public declarations }
  end;

var
  FmTelCtaCadIgnor: TFmTelCtaCadIgnor;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, ModuleGeral;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmTelCtaCadIgnor.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmTelCtaCadIgnor.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrTelCtasLnkCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmTelCtaCadIgnor.DefParams;
begin
  VAR_GOTOTABELA := 'telctaslnk';
  VAR_GOTOMYSQLTABLE := QrTelCtasLnk;
  VAR_GOTONEG := gotoPos;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT IF (ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENT, lnk.*');
  VAR_SQLx.Add('FROM telctaslnk lnk');
  VAR_SQLx.Add('LEFT JOIN entidades ent ON ent.Codigo = lnk.Entidade');
  VAR_SQLx.Add('WHERE lnk.Codigo > 0');
  //
  VAR_SQL1.Add('AND lnk.Codigo=:P0');
  //
  //VAR_SQL2.Add('AND lnk.CodUsu=:P0');
  //
  VAR_SQLa.Add('AND lnk.Nome Like :P0');
  //
end;

procedure TFmTelCtaCadIgnor.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmTelCtaCadIgnor.QueryPrincipalAfterOpen;
begin
end;

procedure TFmTelCtaCadIgnor.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmTelCtaCadIgnor.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmTelCtaCadIgnor.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmTelCtaCadIgnor.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmTelCtaCadIgnor.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmTelCtaCadIgnor.SpeedButton5Click(Sender: TObject);
begin
  DmodG.CadastroDeEntidade(VUEntidade.ValueVariant, fmcadEntidade2, fmcadEntidade2);
  //
  QrEntidades.Close;
  QrEntidades.Open;
  //
  if VAR_CADASTRO > 0 then
    UMyMod.SetaCodUsuDeCodigo(EdEntidade, CBEntidade, QrEntidades, VAR_CADASTRO);
end;

procedure TFmTelCtaCadIgnor.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmTelCtaCadIgnor.BtAlteraClick(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrTelCtasLnk, [PnDados],
  [PnEdita], EdEntidade, ImgTipo, 'telctaslnk');
end;

procedure TFmTelCtaCadIgnor.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrTelCtasLnkCodigo.Value;
  Close;
end;

procedure TFmTelCtaCadIgnor.BtConfirmaClick(Sender: TObject);
var
  Codigo, Entidade, Ignorar: Integer;
begin
  Entidade := EdEntidade.ValueVariant;
  Ignorar  := RGIgnorar.ItemIndex;
  //
  if MyObjects.FIC(Entidade = 0, EdEntidade, 'Defina uma entidade!') then Exit;
  if MyObjects.FIC(Ignorar < 0, RGIgnorar, 'Defina um valor para o campo Ignorar entidade!') then Exit;
  //
  Codigo := UMyMod.BPGS1I32('telctaslnk', 'Codigo', '', '',
    tsPos, ImgTipo.SQLType, QrTelCtasLnkCodigo.Value);
  if UMyMod.ExecSQLInsUpdPanel(ImgTipo.SQLType, Self, PnEdita,
    'telctaslnk', Codigo, Dmod.QrUpd, [PnEdita], [PnDados], ImgTipo, True) then
  begin
    LocCod(Codigo, Codigo);
  end;
end;

procedure TFmTelCtaCadIgnor.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := EdCodigo.ValueVariant;
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'telctaslnk', 'Codigo');
end;

procedure TFmTelCtaCadIgnor.BtExcluiClick(Sender: TObject);
var
  Codigo: Integer;
begin
  if (QrTelCtasLnk.State <> dsInactive) and (QrTelCtasLnk.RecordCount > 0) then
  begin
    Codigo := QrTelCtasLnkCodigo.Value;
    //
    if UMyMod.ExcluiRegistroInt1('Confirma a exclus�o do item selecionado?',
      'telctaslnk', 'Codigo', Codigo, Dmod.MyDB) = ID_YES then
    begin
      LocCod(Codigo, Codigo);
      Va(vpLast);
    end;
  end;
end;

procedure TFmTelCtaCadIgnor.BtIncluiClick(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrTelCtasLnk, [PnDados],
  [PnEdita], EdEntidade, ImgTipo, 'telctaslnk');
end;

procedure TFmTelCtaCadIgnor.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  GBEdita.Align := alClient;
  GBDados.Align := alClient;
  CriaOForm;
  //
  UMyMod.AbreQuery(QrEntidades, DMod.MyDB);
end;

procedure TFmTelCtaCadIgnor.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrTelCtasLnkCodigo.Value, LaRegistro.Caption);
end;

procedure TFmTelCtaCadIgnor.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmTelCtaCadIgnor.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrTelCtasLnkCodigo.Value, LaRegistro.Caption);
end;

procedure TFmTelCtaCadIgnor.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmTelCtaCadIgnor.QrTelCtasLnkAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmTelCtaCadIgnor.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmTelCtaCadIgnor.SbQueryClick(Sender: TObject);
begin
  LocCod(QrTelCtasLnkCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'telctaslnk', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmTelCtaCadIgnor.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmTelCtaCadIgnor.QrTelCtasLnkBeforeOpen(DataSet: TDataSet);
begin
  QrTelCtasLnkCodigo.DisplayFormat := FFormatFloat;
end;

end.

