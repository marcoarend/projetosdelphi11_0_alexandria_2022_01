object FmZero800Cab: TFmZero800Cab
  Left = 339
  Top = 185
  Caption = 'CAD-PRTER-001 :: Cadastro de Protocolos de Terceiros'
  ClientHeight = 692
  ClientWidth = 1088
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -14
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 120
  TextHeight = 16
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1088
    Height = 59
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 1029
      Top = 0
      Width = 59
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 10
        Top = 14
        Width = 39
        Height = 39
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 59
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 59
      Top = 0
      Width = 970
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 9
        Top = 11
        Width = 517
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Cadastro de Protocolos de Terceiros'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 11
        Top = 14
        Width = 517
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Cadastro de Protocolos de Terceiros'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 10
        Top = 12
        Width = 517
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Cadastro de Protocolos de Terceiros'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 59
    Width = 1088
    Height = 492
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 1088
      Height = 492
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 1088
        Height = 492
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        TabOrder = 0
        object Splitter1: TSplitter
          Left = 396
          Top = 18
          Width = 13
          Height = 472
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
        end
        object Panel5: TPanel
          Left = 409
          Top = 18
          Width = 677
          Height = 472
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          object Splitter2: TSplitter
            Left = 0
            Top = 198
            Width = 677
            Height = 12
            Cursor = crVSplit
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alTop
          end
          object dmkDBGrid3: TdmkDBGrid
            Left = 0
            Top = 0
            Width = 677
            Height = 198
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alTop
            Columns = <
              item
                Expanded = False
                FieldName = 'DataHora'
                Title.Caption = 'Data / Hora'
                Width = 120
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ProtoNum'
                Title.Caption = 'Protocolo'
                Width = 110
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Atendente'
                Width = 200
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Matricula'
                Title.Caption = 'Matr'#237'cula'
                Width = 100
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ProtoOri'
                Title.Caption = 'Protocolo de origem'
                Width = 110
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Codigo'
                Title.Caption = 'ID'
                Width = 65
                Visible = True
              end>
            Color = clWindow
            DataSource = DsZero800Cab
            Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
            TabOrder = 0
            TitleFont.Charset = ANSI_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -14
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            Columns = <
              item
                Expanded = False
                FieldName = 'DataHora'
                Title.Caption = 'Data / Hora'
                Width = 120
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ProtoNum'
                Title.Caption = 'Protocolo'
                Width = 110
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Atendente'
                Width = 200
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Matricula'
                Title.Caption = 'Matr'#237'cula'
                Width = 100
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ProtoOri'
                Title.Caption = 'Protocolo de origem'
                Width = 110
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Codigo'
                Title.Caption = 'ID'
                Width = 65
                Visible = True
              end>
          end
          object Panel6: TPanel
            Left = 0
            Top = 210
            Width = 677
            Height = 261
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alClient
            BevelOuter = bvNone
            TabOrder = 1
            object dmkDBGrid2: TdmkDBGrid
              Left = 0
              Top = 21
              Width = 406
              Height = 240
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alClient
              Columns = <
                item
                  Expanded = False
                  FieldName = 'DATAHORA_TXT'
                  Title.Caption = 'Data / hora'
                  Width = 120
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'QUEM_TXT'
                  Title.Caption = 'Quem?'
                  Width = 90
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Controle'
                  Title.Caption = 'ID'
                  Width = 65
                  Visible = True
                end>
              Color = clWindow
              DataSource = DsZero800Its
              Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
              TabOrder = 0
              TitleFont.Charset = ANSI_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -14
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              Columns = <
                item
                  Expanded = False
                  FieldName = 'DATAHORA_TXT'
                  Title.Caption = 'Data / hora'
                  Width = 120
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'QUEM_TXT'
                  Title.Caption = 'Quem?'
                  Width = 90
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Controle'
                  Title.Caption = 'ID'
                  Width = 65
                  Visible = True
                end>
            end
            object Panel7: TPanel
              Left = 406
              Top = 21
              Width = 271
              Height = 240
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alRight
              BevelOuter = bvNone
              TabOrder = 1
              object BtExclui: TBitBtn
                Tag = 12
                Left = 0
                Top = 191
                Width = 271
                Height = 49
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Align = alBottom
                Caption = '&Exclui'
                NumGlyphs = 2
                TabOrder = 0
                OnClick = BtExcluiClick
              end
              object DBMemo1: TDBMemo
                Left = 0
                Top = 0
                Width = 271
                Height = 191
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Align = alClient
                DataField = 'Nome'
                DataSource = DsZero800Its
                TabOrder = 1
              end
            end
            object StaticText6: TStaticText
              Left = 0
              Top = 0
              Width = 91
              Height = 20
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alTop
              Alignment = taCenter
              BorderStyle = sbsSunken
              Caption = 'Coment'#225'rios'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -15
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsBold]
              ParentFont = False
              TabOrder = 2
            end
          end
        end
        object Panel8: TPanel
          Left = 2
          Top = 18
          Width = 394
          Height = 472
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alLeft
          BevelOuter = bvNone
          TabOrder = 1
          object dmkDBGrid1: TdmkDBGrid
            Left = 0
            Top = 26
            Width = 394
            Height = 445
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alLeft
            Columns = <
              item
                Expanded = False
                FieldName = 'ENTNOME'
                Title.Caption = 'Entidade'
                Width = 260
                Visible = True
              end>
            Color = clWindow
            DataSource = DsZero800CabEnt
            Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
            TabOrder = 0
            TitleFont.Charset = ANSI_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -14
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            Columns = <
              item
                Expanded = False
                FieldName = 'ENTNOME'
                Title.Caption = 'Entidade'
                Width = 260
                Visible = True
              end>
          end
          object EdPesq: TdmkEdit
            Left = 0
            Top = 0
            Width = 394
            Height = 26
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alTop
            TabOrder = 1
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
            OnChange = EdPesqChange
          end
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 551
    Width = 1088
    Height = 55
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 18
      Width = 1084
      Height = 34
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 16
        Top = 2
        Width = 150
        Height = 19
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -17
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 15
        Top = 1
        Width = 150
        Height = 19
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -17
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 606
    Width = 1088
    Height = 86
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 908
      Top = 18
      Width = 178
      Height = 66
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 15
        Top = 5
        Width = 147
        Height = 49
        Cursor = crHandPoint
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 18
      Width = 906
      Height = 66
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtGerencia: TBitBtn
        Tag = 14
        Left = 15
        Top = 5
        Width = 147
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Gerencia'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtGerenciaClick
      end
    end
  end
  object PMGerencia: TPopupMenu
    Left = 72
    Top = 320
    object Inclui1: TMenuItem
      Caption = '&Inclui'
      OnClick = Inclui1Click
    end
    object Alteraselecionado1: TMenuItem
      Caption = '&Altera selecionado'
      OnClick = Alteraselecionado1Click
    end
    object Excluiselecionado1: TMenuItem
      Caption = '&Exclui selecionado'
      OnClick = Excluiselecionado1Click
    end
  end
  object QrZero800CabEnt: TmySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrZero800CabEntBeforeClose
    AfterScroll = QrZero800CabEntAfterScroll
    SQL.Strings = (
      'SELECT cab.Entidade, '
      'IF (ent.Tipo=0, ent.RazaoSocial, ent.Nome) ENTNOME'
      'FROM zero800cab cab'
      'LEFT JOIN entidades ent ON ent.Codigo = cab.Entidade'
      'WHERE IF (ent.Tipo=0, ent.RazaoSocial, ent.Nome) LIKE "%P:0%"'
      'GROUP BY cab.Entidade')
    Left = 148
    Top = 172
    object QrZero800CabEntEntidade: TIntegerField
      FieldName = 'Entidade'
      Origin = 'zero800cab.Entidade'
    end
    object QrZero800CabEntENTNOME: TWideStringField
      FieldName = 'ENTNOME'
      Origin = 'ENTNOME'
      Size = 100
    end
  end
  object DsZero800CabEnt: TDataSource
    DataSet = QrZero800CabEnt
    Left = 176
    Top = 172
  end
  object DsZero800Cab: TDataSource
    DataSet = QrZero800Cab
    Left = 176
    Top = 200
  end
  object QrZero800Cab: TmySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrZero800CabBeforeClose
    AfterScroll = QrZero800CabAfterScroll
    SQL.Strings = (
      'SELECT *'
      'FROM zero800cab'
      'ORDER BY DataHora')
    Left = 148
    Top = 200
    object QrZero800CabCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'zero800cab.Codigo'
    end
    object QrZero800CabEntidade: TIntegerField
      FieldName = 'Entidade'
      Origin = 'zero800cab.Entidade'
    end
    object QrZero800CabProtoOri: TWideStringField
      FieldName = 'ProtoOri'
      Origin = 'zero800cab.ProtoOri'
      Size = 60
    end
    object QrZero800CabProtoNum: TWideStringField
      FieldName = 'ProtoNum'
      Origin = 'zero800cab.ProtoNum'
      Size = 60
    end
    object QrZero800CabAtendente: TWideStringField
      FieldName = 'Atendente'
      Origin = 'zero800cab.Atendente'
      Size = 60
    end
    object QrZero800CabMatricula: TWideStringField
      FieldName = 'Matricula'
      Origin = 'zero800cab.Matricula'
      Size = 60
    end
    object QrZero800CabDataHora: TDateTimeField
      FieldName = 'DataHora'
      Origin = 'zero800cab.DataHora'
    end
    object QrZero800CabLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'zero800cab.Lk'
    end
    object QrZero800CabDataCad: TDateField
      FieldName = 'DataCad'
      Origin = 'zero800cab.DataCad'
    end
    object QrZero800CabDataAlt: TDateField
      FieldName = 'DataAlt'
      Origin = 'zero800cab.DataAlt'
    end
    object QrZero800CabUserCad: TIntegerField
      FieldName = 'UserCad'
      Origin = 'zero800cab.UserCad'
    end
    object QrZero800CabUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Origin = 'zero800cab.UserAlt'
    end
    object QrZero800CabAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Origin = 'zero800cab.AlterWeb'
    end
    object QrZero800CabAtivo: TSmallintField
      FieldName = 'Ativo'
      Origin = 'zero800cab.Ativo'
    end
  end
  object QrZero800Its: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrZero800ItsCalcFields
    SQL.Strings = (
      'SELECT its.*, '
      'DATE_FORMAT(its.DataHora, "%d/%m/%Y %H:%i:%s") DATAHORA_TXT,'
      'cab.Atendente'
      'FROM zero800its its'
      'LEFT JOIN zero800cab cab ON cab.Codigo = its.Codigo'
      'WHERE its.Codigo=:P0'
      'ORDER BY its.DataHora DESC')
    Left = 148
    Top = 228
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrZero800ItsCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrZero800ItsControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrZero800ItsNome: TWideStringField
      FieldName = 'Nome'
      Size = 255
    end
    object QrZero800ItsQuem: TSmallintField
      FieldName = 'Quem'
    end
    object QrZero800ItsDataHora: TDateTimeField
      FieldName = 'DataHora'
    end
    object QrZero800ItsLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrZero800ItsDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrZero800ItsDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrZero800ItsUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrZero800ItsUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrZero800ItsAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrZero800ItsAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrZero800ItsDATAHORA_TXT: TWideStringField
      FieldName = 'DATAHORA_TXT'
      Size = 19
    end
    object QrZero800ItsAtendente: TWideStringField
      FieldName = 'Atendente'
      Size = 60
    end
    object QrZero800ItsQUEM_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'QUEM_TXT'
      Size = 50
      Calculated = True
    end
  end
  object DsZero800Its: TDataSource
    DataSet = QrZero800Its
    Left = 176
    Top = 228
  end
  object QrLoc: TmySQLQuery
    Database = Dmod.MyDB
    Left = 164
    Top = 284
  end
end
