object FmTelCtaCab: TFmTelCtaCab
  Left = 368
  Top = 194
  Caption = 'TEL-CTACA-001 :: Gerenciamento de Conta Telef'#244'nica'
  ClientHeight = 730
  ClientWidth = 1008
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnDados: TPanel
    Left = 0
    Top = 113
    Width = 1008
    Height = 617
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    object GBDados: TGroupBox
      Left = 0
      Top = 0
      Width = 1008
      Height = 145
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 1
      object Label1: TLabel
        Left = 16
        Top = 16
        Width = 14
        Height = 13
        Caption = 'ID:'
        FocusControl = DBEdCodigo
      end
      object Label2: TLabel
        Left = 76
        Top = 16
        Width = 35
        Height = 13
        Caption = 'Cliente:'
        FocusControl = DBEdNome
      end
      object Label3: TLabel
        Left = 16
        Top = 56
        Width = 79
        Height = 13
        Caption = 'Conta cobran'#231'a:'
        FocusControl = dmkDBEdit1
      end
      object Label4: TLabel
        Left = 112
        Top = 56
        Width = 85
        Height = 13
        Caption = 'N'#250'mero da fatura:'
        FocusControl = dmkDBEdit2
      end
      object Label5: TLabel
        Left = 208
        Top = 56
        Width = 73
        Height = 13
        Caption = 'In'#237'cio vig'#234'ncia:'
        FocusControl = dmkDBEdit3
      end
      object Label6: TLabel
        Left = 304
        Top = 56
        Width = 62
        Height = 13
        Caption = 'Fim vig'#234'ncia:'
        FocusControl = dmkDBEdit4
      end
      object Label7: TLabel
        Left = 400
        Top = 56
        Width = 42
        Height = 13
        Caption = 'Emiss'#227'o:'
        FocusControl = dmkDBEdit5
      end
      object Label8: TLabel
        Left = 496
        Top = 56
        Width = 59
        Height = 13
        Caption = 'Vencimento:'
        FocusControl = dmkDBEdit6
      end
      object Label9: TLabel
        Left = 546
        Top = 16
        Width = 17
        Height = 13
        Caption = 'UF:'
        FocusControl = dmkDBEdit7
      end
      object Label19: TLabel
        Left = 16
        Top = 96
        Width = 44
        Height = 13
        Caption = 'Empresa:'
        FocusControl = dmkDBEdit8
      end
      object DBEdCodigo: TdmkDBEdit
        Left = 16
        Top = 32
        Width = 56
        Height = 21
        Hint = 'N'#186' do banco'
        TabStop = False
        DataField = 'Codigo'
        DataSource = DsTelCtasCab
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 0
        UpdType = utYes
        Alignment = taRightJustify
      end
      object DBEdNome: TdmkDBEdit
        Left = 76
        Top = 32
        Width = 464
        Height = 21
        Hint = 'Nome do banco'
        Color = clWhite
        DataField = 'Cliente'
        DataSource = DsTelCtasCab
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        UpdType = utYes
        Alignment = taLeftJustify
      end
      object dmkDBEdit1: TdmkDBEdit
        Left = 16
        Top = 72
        Width = 90
        Height = 21
        Hint = 'N'#186' do banco'
        TabStop = False
        DataField = 'ContaCobr'
        DataSource = DsTelCtasCab
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 3
        UpdType = utYes
        Alignment = taLeftJustify
      end
      object dmkDBEdit2: TdmkDBEdit
        Left = 112
        Top = 72
        Width = 90
        Height = 21
        Hint = 'N'#186' do banco'
        TabStop = False
        DataField = 'NumFatura'
        DataSource = DsTelCtasCab
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 4
        UpdType = utYes
        Alignment = taLeftJustify
      end
      object dmkDBEdit3: TdmkDBEdit
        Left = 208
        Top = 72
        Width = 90
        Height = 21
        Hint = 'N'#186' do banco'
        TabStop = False
        DataField = 'IniVigen'
        DataSource = DsTelCtasCab
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 5
        UpdType = utYes
        Alignment = taLeftJustify
      end
      object dmkDBEdit4: TdmkDBEdit
        Left = 304
        Top = 72
        Width = 90
        Height = 21
        Hint = 'N'#186' do banco'
        TabStop = False
        DataField = 'FimVigen'
        DataSource = DsTelCtasCab
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 6
        UpdType = utYes
        Alignment = taLeftJustify
      end
      object dmkDBEdit5: TdmkDBEdit
        Left = 400
        Top = 72
        Width = 90
        Height = 21
        Hint = 'N'#186' do banco'
        TabStop = False
        DataField = 'Emissao'
        DataSource = DsTelCtasCab
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 7
        UpdType = utYes
        Alignment = taLeftJustify
      end
      object dmkDBEdit6: TdmkDBEdit
        Left = 496
        Top = 72
        Width = 90
        Height = 21
        Hint = 'N'#186' do banco'
        TabStop = False
        DataField = 'Vencto'
        DataSource = DsTelCtasCab
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 8
        UpdType = utYes
        Alignment = taLeftJustify
      end
      object dmkDBEdit7: TdmkDBEdit
        Left = 546
        Top = 32
        Width = 40
        Height = 21
        Hint = 'N'#186' do banco'
        TabStop = False
        DataField = 'UF'
        DataSource = DsTelCtasCab
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 2
        UpdType = utYes
        Alignment = taLeftJustify
      end
      object GroupBox1: TGroupBox
        Left = 821
        Top = 15
        Width = 185
        Height = 128
        Align = alRight
        Caption = 'Total em reais'
        TabOrder = 9
        object LaTotal: TLabel
          Left = 115
          Top = 15
          Width = 68
          Height = 37
          Align = alClient
          Alignment = taRightJustify
          Caption = '0,00'
          FocusControl = dmkDBEdit7
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -35
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
      end
      object dmkDBEdit8: TdmkDBEdit
        Left = 16
        Top = 112
        Width = 570
        Height = 21
        Hint = 'Nome do banco'
        Color = clWhite
        DataField = 'CliInt_TXT'
        DataSource = DsTelCtasCab
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 10
        UpdType = utYes
        Alignment = taLeftJustify
      end
    end
    object GBCntrl: TGroupBox
      Left = 0
      Top = 553
      Width = 1008
      Height = 64
      Align = alBottom
      TabOrder = 0
      object Panel5: TPanel
        Left = 2
        Top = 15
        Width = 172
        Height = 47
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 174
        Top = 15
        Width = 30
        Height = 17
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel3: TPanel
        Left = 485
        Top = 15
        Width = 521
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object BtExclui: TBitBtn
          Tag = 12
          Left = 124
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Exclui'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtExcluiClick
        end
        object BtInclui: TBitBtn
          Tag = 10
          Left = 4
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Inclui'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtIncluiClick
        end
        object Panel2: TPanel
          Left = 388
          Top = 0
          Width = 133
          Height = 47
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 2
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
        object BtPagto: TBitBtn
          Tag = 187
          Left = 244
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Pagto'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = BtPagtoClick
        end
      end
    end
    object PageControl1: TPageControl
      Left = 0
      Top = 145
      Width = 1008
      Height = 408
      ActivePage = TabSheet5
      Align = alClient
      TabHeight = 25
      TabOrder = 2
      object TabSheet1: TTabSheet
        Caption = 'Chamadas'
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object PnChamTop: TPanel
          Left = 0
          Top = 0
          Width = 1000
          Height = 47
          Align = alTop
          ParentBackground = False
          TabOrder = 0
          object BtTelConcilia: TBitBtn
            Tag = 10011
            Left = 10
            Top = 2
            Width = 40
            Height = 40
            Cursor = crHandPoint
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtTelConciliaClick
          end
          object BtTelExclui: TBitBtn
            Tag = 12
            Left = 52
            Top = 2
            Width = 40
            Height = 40
            Cursor = crHandPoint
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 1
            OnClick = BtTelExcluiClick
          end
          object BtTelEntidade: TBitBtn
            Tag = 101
            Left = 95
            Top = 2
            Width = 40
            Height = 40
            Cursor = crHandPoint
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 2
            OnClick = BtTelEntidadeClick
          end
        end
        object PnChamBot: TPanel
          Left = 0
          Top = 318
          Width = 1000
          Height = 55
          Align = alBottom
          ParentBackground = False
          TabOrder = 1
          object Label10: TLabel
            Left = 16
            Top = 8
            Width = 34
            Height = 13
            Caption = 'Ordem:'
          end
          object Label11: TLabel
            Left = 168
            Top = 8
            Width = 41
            Height = 13
            Caption = 'Ordenar:'
          end
          object CBChamOrdem: TComboBox
            Left = 16
            Top = 24
            Width = 145
            Height = 21
            Style = csDropDownList
            ItemIndex = 0
            TabOrder = 0
            Text = 'Crescente'
            OnChange = CBChamOrdemChange
            Items.Strings = (
              'Crescente'
              'Decrescente')
          end
          object CBChamOrdenar: TComboBox
            Left = 168
            Top = 24
            Width = 145
            Height = 21
            Style = csDropDownList
            TabOrder = 1
            OnChange = CBChamOrdenarChange
            Items.Strings = (
              'Telefone de origem'
              'Entidade origem'
              'Entidade origem a ignorar'
              'Data / hora'
              'Dura'#231#227'o'
              'Valor'
              'Telefone destino'
              'Entidade destino'
              'Entidade destino a ignorar'
              'Prestadora'
              'Cidade'
              'UF'
              'Tipo'
              'Hor'#225'rio'
              'ID')
          end
          object Panel9: TPanel
            Left = 529
            Top = 1
            Width = 470
            Height = 53
            Align = alRight
            BevelOuter = bvNone
            TabOrder = 2
            object Label15: TLabel
              Left = 118
              Top = 7
              Width = 94
              Height = 13
              Caption = 'Total de chamadas:'
            end
            object Label16: TLabel
              Left = 232
              Top = 7
              Width = 67
              Height = 13
              Caption = 'Dura'#231#227'o total:'
            end
            object Label17: TLabel
              Left = 346
              Top = 7
              Width = 47
              Height = 13
              Caption = 'Valor total'
            end
            object Label18: TLabel
              Left = 1
              Top = 7
              Width = 101
              Height = 13
              Caption = 'Soma das linhas: [F9]'
            end
            object EdTelChamTot: TdmkEdit
              Left = 118
              Top = 24
              Width = 110
              Height = 21
              Alignment = taRightJustify
              ReadOnly = True
              TabOrder = 0
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
            end
            object EdTelDurTot: TdmkEdit
              Left = 232
              Top = 24
              Width = 110
              Height = 21
              Alignment = taCenter
              ReadOnly = True
              TabOrder = 1
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '00:00'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = '00:00'
              ValWarn = False
            end
            object EdTelTotal: TdmkEdit
              Left = 346
              Top = 24
              Width = 110
              Height = 21
              Alignment = taRightJustify
              ReadOnly = True
              TabOrder = 2
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
            end
            object EdTelSomaLinha: TdmkEdit
              Left = 1
              Top = 24
              Width = 110
              Height = 21
              Alignment = taRightJustify
              Color = clBtnFace
              ReadOnly = True
              TabOrder = 3
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
            end
          end
        end
        object DBGTel: TDBGrid
          Left = 0
          Top = 47
          Width = 1000
          Height = 271
          Align = alClient
          DataSource = DsTelCtasTel
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
          TabOrder = 2
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          OnDrawColumnCell = DBGTelDrawColumnCell
          OnDblClick = DBGTelDblClick
          OnKeyDown = DBGTelKeyDown
          Columns = <
            item
              Expanded = False
              FieldName = 'TELORIGEM_TXT'
              Title.Caption = 'Telefone de origem'
              Width = 150
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ENTORIGNOME'
              Title.Caption = 'Entidade origem'
              Width = 250
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'IGNORARORIGEM'
              Title.Caption = 'Ignora?'
              Width = 45
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DataHora'
              Title.Caption = 'Data / hora'
              Width = 115
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Duracao'
              Title.Caption = 'Dura'#231#227'o'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Valor'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'TELDESTINO_TXT'
              Title.Caption = 'Telefone destino'
              Width = 150
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ENTDESTNOME'
              Title.Caption = 'Entidade destino'
              Width = 250
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'IGNORARDESTINO'
              Title.Caption = 'Ignora?'
              Width = 45
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Cidade'
              Width = 200
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'UF'
              Width = 20
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'TipoCham'
              Title.Caption = 'Tipo'
              Width = 100
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Horario'
              Title.Caption = 'Hor'#225'rio'
              Width = 45
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Prestadora'
              Width = 125
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Controle'
              Title.Caption = 'ID'
              Visible = True
            end>
        end
      end
      object TabSheet2: TTabSheet
        Caption = 'Servi'#231'os Mensais'
        ImageIndex = 1
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object PnMensTop: TPanel
          Left = 0
          Top = 0
          Width = 1000
          Height = 47
          Align = alTop
          ParentBackground = False
          TabOrder = 0
          object BtMsgExclui: TBitBtn
            Tag = 12
            Left = 10
            Top = 2
            Width = 40
            Height = 40
            Cursor = crHandPoint
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtMsgExcluiClick
          end
        end
        object DBGMsg: TDBGrid
          Left = 0
          Top = 47
          Width = 1000
          Height = 271
          Align = alClient
          DataSource = DsTelCtasMsg
          Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
          TabOrder = 1
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          OnDrawColumnCell = DBGMsgDrawColumnCell
          Columns = <
            item
              Expanded = False
              FieldName = 'Tipo'
              Width = 150
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ID'
              Title.Caption = 'Identifica'#231#227'o'
              Width = 150
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Descricao'
              Title.Caption = 'Descri'#231#227'o'
              Width = 150
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DataIni'
              Title.Caption = 'Data in'#237'cio'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DataFim'
              Title.Caption = 'Data t'#233'rmino'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Valor'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Controle'
              Title.Caption = 'ID'
              Visible = True
            end>
        end
        object Panel6: TPanel
          Left = 0
          Top = 318
          Width = 1000
          Height = 55
          Align = alBottom
          ParentBackground = False
          TabOrder = 2
          object Panel8: TPanel
            Left = 874
            Top = 1
            Width = 125
            Height = 53
            Align = alRight
            BevelOuter = bvNone
            TabOrder = 0
            object Label14: TLabel
              Left = 6
              Top = 3
              Width = 50
              Height = 13
              Caption = 'Valor total:'
            end
            object EdMsgTotal: TdmkEdit
              Left = 6
              Top = 20
              Width = 110
              Height = 21
              Alignment = taRightJustify
              ReadOnly = True
              TabOrder = 0
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
            end
          end
        end
      end
      object TabSheet3: TTabSheet
        Caption = 'Servi'#231'os Eventuais'
        ImageIndex = 2
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object PnEveTop: TPanel
          Left = 0
          Top = 0
          Width = 1000
          Height = 47
          Align = alTop
          ParentBackground = False
          TabOrder = 0
          object BtEveExclui: TBitBtn
            Tag = 12
            Left = 10
            Top = 2
            Width = 40
            Height = 40
            Cursor = crHandPoint
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtEveExcluiClick
          end
        end
        object DBGEve: TDBGrid
          Left = 0
          Top = 47
          Width = 1000
          Height = 271
          Align = alClient
          DataSource = DsTelCtasEve
          Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
          TabOrder = 1
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          OnDrawColumnCell = DBGEveDrawColumnCell
          Columns = <
            item
              Expanded = False
              FieldName = 'Tipo'
              Width = 150
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ID'
              Title.Caption = 'Identificador'
              Width = 150
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Descricao'
              Title.Caption = 'Descri'#231#227'o'
              Width = 300
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Valor'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Controle'
              Title.Caption = 'ID'
              Visible = True
            end>
        end
        object PnEveBot: TPanel
          Left = 0
          Top = 318
          Width = 1000
          Height = 55
          Align = alBottom
          ParentBackground = False
          TabOrder = 2
          object Panel7: TPanel
            Left = 874
            Top = 1
            Width = 125
            Height = 53
            Align = alRight
            BevelOuter = bvNone
            TabOrder = 0
            object Label13: TLabel
              Left = 6
              Top = 3
              Width = 50
              Height = 13
              Caption = 'Valor total:'
            end
            object EdEveTotal: TdmkEdit
              Left = 6
              Top = 20
              Width = 110
              Height = 21
              Alignment = taRightJustify
              ReadOnly = True
              TabOrder = 0
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
            end
          end
        end
      end
      object TabSheet4: TTabSheet
        Caption = 'Descontos'
        ImageIndex = 3
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object PnDesTop: TPanel
          Left = 0
          Top = 0
          Width = 1000
          Height = 47
          Align = alTop
          ParentBackground = False
          TabOrder = 0
          object BtDesExclui: TBitBtn
            Tag = 12
            Left = 10
            Top = 2
            Width = 40
            Height = 40
            Cursor = crHandPoint
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtDesExcluiClick
          end
        end
        object DBGDes: TDBGrid
          Left = 0
          Top = 47
          Width = 1000
          Height = 271
          Align = alClient
          DataSource = DsTelCtasDes
          Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
          TabOrder = 1
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          OnDrawColumnCell = DBGDesDrawColumnCell
          Columns = <
            item
              Expanded = False
              FieldName = 'Descricao'
              Title.Caption = 'Descri'#231#227'o'
              Width = 300
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Valor'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Controle'
              Title.Caption = 'ID'
              Visible = True
            end>
        end
        object PnDesBot: TPanel
          Left = 0
          Top = 318
          Width = 1000
          Height = 55
          Align = alBottom
          ParentBackground = False
          TabOrder = 2
          object Panel1: TPanel
            Left = 874
            Top = 1
            Width = 125
            Height = 53
            Align = alRight
            BevelOuter = bvNone
            TabOrder = 0
            object Label12: TLabel
              Left = 6
              Top = 3
              Width = 50
              Height = 13
              Caption = 'Valor total:'
            end
            object EdDesTotal: TdmkEdit
              Left = 6
              Top = 20
              Width = 110
              Height = 21
              Alignment = taRightJustify
              ReadOnly = True
              TabOrder = 0
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
            end
          end
        end
      end
      object TabSheet5: TTabSheet
        Caption = 'Pagto.'
        ImageIndex = 4
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object DBGrid4: TDBGrid
          Left = 0
          Top = 0
          Width = 1000
          Height = 373
          Align = alClient
          DataSource = DsPagtos
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'SEQ'
              Title.Caption = 'N'#186
              Width = 22
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Data'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Vencimento'
              Title.Caption = 'Vencto'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Debito'
              Title.Alignment = taRightJustify
              Title.Caption = 'Valor'
              Width = 70
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMECARTEIRA'
              Title.Caption = 'Carteira'
              Width = 101
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Descricao'
              Title.Caption = 'Descri'#231#227'o'
              Width = 250
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMEFORNECE'
              Title.Caption = 'Fornecedor'
              Width = 250
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Controle'
              Title.Caption = 'Lan'#231'amento'
              Visible = True
            end>
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 12
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 216
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 216
      Top = 0
      Width = 744
      Height = 52
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 433
        Height = 32
        Caption = 'Gerenciamento de Conta Telef'#244'nica'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 433
        Height = 32
        Caption = 'Gerenciamento de Conta Telef'#244'nica'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 433
        Height = 32
        Caption = 'Gerenciamento de Conta Telef'#244'nica'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 52
    Width = 1008
    Height = 44
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 599
        Height = 16
        Caption = 
          'Para localizar o cadastro da entidade clique duas vezes na colun' +
          'a Entidade origem ou Entidade destino'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 599
        Height = 16
        Caption = 
          'Para localizar o cadastro da entidade clique duas vezes na colun' +
          'a Entidade origem ou Entidade destino'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object PB1: TProgressBar
    Left = 0
    Top = 96
    Width = 1008
    Height = 17
    Align = alTop
    TabOrder = 3
    Visible = False
  end
  object QrTelCtasCab: TMySQLQuery
    Database = DModG.RV_CEP_DB
    BeforeOpen = QrTelCtasCabBeforeOpen
    AfterOpen = QrTelCtasCabAfterOpen
    BeforeClose = QrTelCtasCabBeforeClose
    AfterScroll = QrTelCtasCabAfterScroll
    SQL.Strings = (
      'SELECT cab.*, eci.CodCliInt,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) CliInt_TXT'
      'FROM telctacab cab '
      'LEFT JOIN enticliint eci ON eci.CodEnti = cab.CliInt'
      'LEFT JOIN entidades ent ON ent.Codigo = cab.CliInt'
      'WHERE cab.Codigo > 0'
      '')
    Left = 64
    Top = 64
    object QrTelCtasCabCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrTelCtasCabContaCobr: TWideStringField
      FieldName = 'ContaCobr'
      Size = 10
    end
    object QrTelCtasCabNumFatura: TIntegerField
      FieldName = 'NumFatura'
    end
    object QrTelCtasCabIniVigen: TDateField
      FieldName = 'IniVigen'
    end
    object QrTelCtasCabFimVigen: TDateField
      FieldName = 'FimVigen'
    end
    object QrTelCtasCabEmissao: TDateField
      FieldName = 'Emissao'
    end
    object QrTelCtasCabVencto: TDateField
      FieldName = 'Vencto'
    end
    object QrTelCtasCabCliente: TWideStringField
      FieldName = 'Cliente'
      Size = 100
    end
    object QrTelCtasCabUF: TWideStringField
      FieldName = 'UF'
      Size = 2
    end
    object QrTelCtasCabLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrTelCtasCabDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrTelCtasCabDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrTelCtasCabUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrTelCtasCabUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrTelCtasCabAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrTelCtasCabAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrTelCtasCabCliInt_TXT: TWideStringField
      FieldName = 'CliInt_TXT'
      Size = 100
    end
    object QrTelCtasCabCliInt: TIntegerField
      FieldName = 'CliInt'
    end
    object QrTelCtasCabCodCliInt: TIntegerField
      FieldName = 'CodCliInt'
    end
  end
  object DsTelCtasCab: TDataSource
    DataSet = QrTelCtasCab
    Left = 92
    Top = 64
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = BtInclui
    CanUpd01 = QrTelCtasTel
    CanDel01 = BtExclui
    Left = 120
    Top = 64
  end
  object QrTelCtasTel: TMySQLQuery
    Database = DModG.RV_CEP_DB
    OnCalcFields = QrTelCtasTelCalcFields
    SQL.Strings = (
      'SELECT '
      'IF (eno.Tipo=0, eno.RazaoSocial, eno.Nome) ENTORIGNOME, '
      'IF (end.Tipo=0, end.RazaoSocial, end.Nome) ENTDESTNOME,'
      '('
      'SELECT Ignorar + 0.000'
      'FROM telctaslnk'
      'WHERE Entidade = tel.EntOrigem'
      ') IGNORARORIGEM, '
      '('
      'SELECT Ignorar + 0.000'
      'FROM telctaslnk '
      'WHERE Entidade = tel.EntDestino'
      ') IGNORARDESTINO,'
      'tel.*'
      'FROM telctastel tel'
      'LEFT JOIN entidades eno ON eno.Codigo = tel.EntOrigem'
      'LEFT JOIN entidades end ON end.Codigo = tel.EntDestino'
      'WHERE tel.Codigo=:P0')
    Left = 148
    Top = 64
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrTelCtasTelCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrTelCtasTelControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrTelCtasTelTelOrigem: TWideStringField
      FieldName = 'TelOrigem'
      Size = 24
    end
    object QrTelCtasTelDataHora: TDateTimeField
      FieldName = 'DataHora'
    end
    object QrTelCtasTelDuracao: TTimeField
      FieldName = 'Duracao'
    end
    object QrTelCtasTelTelDestino: TWideStringField
      FieldName = 'TelDestino'
      Size = 24
    end
    object QrTelCtasTelCidade: TWideStringField
      FieldName = 'Cidade'
      Size = 35
    end
    object QrTelCtasTelUF: TWideStringField
      FieldName = 'UF'
      Size = 2
    end
    object QrTelCtasTelTipo: TWideStringField
      FieldName = 'Tipo'
      Size = 100
    end
    object QrTelCtasTelHorario: TWideStringField
      FieldName = 'Horario'
      Size = 40
    end
    object QrTelCtasTelValor: TFloatField
      FieldName = 'Valor'
      DisplayFormat = '#,###,##0.00'
    end
    object QrTelCtasTelLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrTelCtasTelDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrTelCtasTelDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrTelCtasTelUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrTelCtasTelUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrTelCtasTelAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrTelCtasTelAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrTelCtasTelPrestadora: TWideStringField
      FieldName = 'Prestadora'
    end
    object QrTelCtasTelTipoCham: TWideStringField
      FieldName = 'TipoCham'
      Size = 80
    end
    object QrTelCtasTelVencto: TDateField
      FieldName = 'Vencto'
    end
    object QrTelCtasTelTELORIGEM_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TELORIGEM_TXT'
      Size = 30
      Calculated = True
    end
    object QrTelCtasTelTELDESTINO_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TELDESTINO_TXT'
      Size = 30
      Calculated = True
    end
    object QrTelCtasTelEntOrigem: TIntegerField
      FieldName = 'EntOrigem'
    end
    object QrTelCtasTelEntDestino: TIntegerField
      FieldName = 'EntDestino'
    end
    object QrTelCtasTelENTORIGNOME: TWideStringField
      FieldName = 'ENTORIGNOME'
      Size = 100
    end
    object QrTelCtasTelENTDESTNOME: TWideStringField
      FieldName = 'ENTDESTNOME'
      Size = 100
    end
    object QrTelCtasTelIGNORARORIGEM: TFloatField
      FieldName = 'IGNORARORIGEM'
      MaxValue = 1.000000000000000000
    end
    object QrTelCtasTelIGNORARDESTINO: TFloatField
      FieldName = 'IGNORARDESTINO'
      MaxValue = 1.000000000000000000
    end
  end
  object DsTelCtasTel: TDataSource
    DataSet = QrTelCtasTel
    Left = 176
    Top = 64
  end
  object QrTelCtasMsg: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'SELECT *'
      'FROM telctasmsg'
      'WHERE Codigo=:P0')
    Left = 204
    Top = 64
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrTelCtasMsgCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrTelCtasMsgControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrTelCtasMsgTipo: TWideStringField
      FieldName = 'Tipo'
      Size = 80
    end
    object QrTelCtasMsgID: TWideStringField
      FieldName = 'ID'
      Size = 48
    end
    object QrTelCtasMsgDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 80
    end
    object QrTelCtasMsgDataIni: TDateField
      FieldName = 'DataIni'
    end
    object QrTelCtasMsgDataFim: TDateField
      FieldName = 'DataFim'
    end
    object QrTelCtasMsgValor: TFloatField
      FieldName = 'Valor'
      DisplayFormat = '#,###,##0.00'
    end
    object QrTelCtasMsgLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrTelCtasMsgDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrTelCtasMsgDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrTelCtasMsgUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrTelCtasMsgUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrTelCtasMsgAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrTelCtasMsgAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrTelCtasMsgPrestadora: TWideStringField
      FieldName = 'Prestadora'
    end
  end
  object DsTelCtasMsg: TDataSource
    DataSet = QrTelCtasMsg
    Left = 232
    Top = 64
  end
  object QrTelCtasEve: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'SELECT *'
      'FROM telctaseve'
      'WHERE Codigo=:P0')
    Left = 260
    Top = 64
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrTelCtasEveCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrTelCtasEveControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrTelCtasEveTipo: TWideStringField
      FieldName = 'Tipo'
      Size = 80
    end
    object QrTelCtasEveID: TWideStringField
      FieldName = 'ID'
      Size = 48
    end
    object QrTelCtasEveDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 80
    end
    object QrTelCtasEveValor: TFloatField
      FieldName = 'Valor'
      DisplayFormat = '#,###,##0.00'
    end
    object QrTelCtasEveLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrTelCtasEveDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrTelCtasEveDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrTelCtasEveUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrTelCtasEveUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrTelCtasEveAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrTelCtasEveAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrTelCtasEvePrestadora: TWideStringField
      FieldName = 'Prestadora'
    end
  end
  object DsTelCtasEve: TDataSource
    DataSet = QrTelCtasEve
    Left = 288
    Top = 64
  end
  object QrTelCtasDes: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'SELECT *'
      'FROM telctasdes'
      'WHERE Codigo=:P0')
    Left = 316
    Top = 64
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrTelCtasDesCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrTelCtasDesControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrTelCtasDesDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 80
    end
    object QrTelCtasDesValor: TFloatField
      FieldName = 'Valor'
      DisplayFormat = '#,###,##0.00'
    end
    object QrTelCtasDesLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrTelCtasDesDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrTelCtasDesDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrTelCtasDesUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrTelCtasDesUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrTelCtasDesAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrTelCtasDesAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrTelCtasDesPrestadora: TWideStringField
      FieldName = 'Prestadora'
    end
  end
  object DsTelCtasDes: TDataSource
    DataSet = QrTelCtasDes
    Left = 344
    Top = 64
  end
  object QrLoc: TMySQLQuery
    Database = DModG.RV_CEP_DB
    Left = 372
    Top = 64
  end
  object PMTelExclui: TPopupMenu
    Left = 456
    Top = 64
    object Exclui1: TMenuItem
      Caption = '&Exclui'
      OnClick = Exclui1Click
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object Excluitodosquenopossuirementidadedeorigem1: TMenuItem
      Caption = 'Exclui todos que n'#227'o possuirem entidade de &origem'
      OnClick = Excluitodosquenopossuirementidadedeorigem1Click
    end
    object Excluitodasentidadesorigemaignorar1: TMenuItem
      Caption = 'Exclui todas entidades origem a &ignorar'
      OnClick = Excluitodasentidadesorigemaignorar1Click
    end
    object N2: TMenuItem
      Caption = '-'
    end
    object Excluitodosquenopossuirementidadededestino1: TMenuItem
      Caption = 'Exclui todos que n'#227'o possuirem entidade de &destino'
      OnClick = Excluitodosquenopossuirementidadededestino1Click
    end
    object Excluitodasentidadesdestinoaignorar1: TMenuItem
      Caption = 'Exclui todas entidades destino a i&gnorar'
      OnClick = Excluitodasentidadesdestinoaignorar1Click
    end
  end
  object PMTelEntidade: TPopupMenu
    Left = 484
    Top = 64
    object Cadastraentidade1: TMenuItem
      Caption = '&Cadastra entidade'
      OnClick = Cadastraentidade1Click
    end
    object Cadastraentidadeaignorar1: TMenuItem
      Caption = 'Cadastra entidade a &ignorar'
      OnClick = Cadastraentidadeaignorar1Click
    end
  end
  object PMInclui: TPopupMenu
    Left = 528
    Top = 632
    object ContaGVT1: TMenuItem
      Caption = 'Conta &GVT'
      OnClick = ContaGVT1Click
    end
  end
  object PMPagto: TPopupMenu
    OnPopup = PMPagtoPopup
    Left = 768
    Top = 632
    object Inclui1: TMenuItem
      Caption = '&Inclui'
      OnClick = Inclui1Click
    end
    object Exclui2: TMenuItem
      Caption = '&Exclui'
      OnClick = Exclui2Click
    end
  end
  object QrPagtos: TMySQLQuery
    Database = DModG.RV_CEP_DB
    Left = 400
    Top = 64
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrPagtosData: TDateField
      FieldName = 'Data'
      Required = True
    end
    object QrPagtosVencimento: TDateField
      FieldName = 'Vencimento'
      Required = True
    end
    object QrPagtosBanco: TIntegerField
      FieldName = 'Banco'
    end
    object QrPagtosSEQ: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'SEQ'
      Calculated = True
    end
    object QrPagtosFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrPagtosContaCorrente: TWideStringField
      FieldName = 'ContaCorrente'
      Size = 15
    end
    object QrPagtosDocumento: TFloatField
      FieldName = 'Documento'
    end
    object QrPagtosDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 100
    end
    object QrPagtosFatParcela: TIntegerField
      FieldName = 'FatParcela'
    end
    object QrPagtosFatNum: TFloatField
      FieldName = 'FatNum'
    end
    object QrPagtosNOMECARTEIRA: TWideStringField
      FieldName = 'NOMECARTEIRA'
      Size = 100
    end
    object QrPagtosNOMECARTEIRA2: TWideStringField
      FieldName = 'NOMECARTEIRA2'
      Size = 100
    end
    object QrPagtosBanco1: TIntegerField
      FieldName = 'Banco1'
    end
    object QrPagtosAgencia1: TIntegerField
      FieldName = 'Agencia1'
    end
    object QrPagtosConta1: TWideStringField
      FieldName = 'Conta1'
      Size = 15
    end
    object QrPagtosTipoDoc: TSmallintField
      FieldName = 'TipoDoc'
    end
    object QrPagtosControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrPagtosNOMEFORNECEI: TWideStringField
      FieldName = 'NOMEFORNECEI'
      Size = 100
    end
    object QrPagtosCARTEIRATIPO: TIntegerField
      FieldName = 'CARTEIRATIPO'
      Required = True
    end
    object QrPagtosDebito: TFloatField
      FieldName = 'Debito'
      DisplayFormat = '#,###,##0.00'
    end
    object QrPagtosAgencia: TIntegerField
      FieldName = 'Agencia'
    end
    object QrPagtosFornecedor: TIntegerField
      FieldName = 'Fornecedor'
    end
    object QrPagtosNOMEFORNECE: TWideStringField
      FieldName = 'NOMEFORNECE'
      Size = 100
    end
    object QrPagtosCarteira: TIntegerField
      FieldName = 'Carteira'
    end
    object QrPagtosGenero: TIntegerField
      FieldName = 'Genero'
    end
  end
  object DsPagtos: TDataSource
    DataSet = QrPagtos
    Left = 428
    Top = 64
  end
end
