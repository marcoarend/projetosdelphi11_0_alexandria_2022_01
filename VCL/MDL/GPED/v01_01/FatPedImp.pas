unit FatPedImp;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, dmkCheckGroup, DB, frxClass, frxDBSet,
  mySQLDbTables, dmkImage, UnDmkEnums, UnDmkProcFunc;

type
  TFmFatPedImp = class(TForm)
    Panel1: TPanel;
    RGTipoRomaneio: TRadioGroup;
    RGAgruparPorVolume: TRadioGroup;
    CGMostrar: TdmkCheckGroup;
    QrSmvaEmp: TmySQLQuery;
    QrSmvaEmpEmpresas: TLargeintField;
    frxDsLctosV: TfrxDBDataset;
    QrLctosV: TmySQLQuery;
    QrLctosVFatParcela: TIntegerField;
    QrLctosVCredito: TFloatField;
    QrLctosVVencimento: TDateField;
    QrLctosVFatNum: TFloatField;
    QrRomaneioCT: TmySQLQuery;
    frxDsRomaneioCT: TfrxDBDataset;
    QrRomaneioCTGraGruC: TIntegerField;
    QrRomaneioCTGraTamI: TIntegerField;
    QrRomaneioCTGraCorCad: TIntegerField;
    QrRomaneioCTNO_COR: TWideStringField;
    QrRomaneioCTCU_Cor: TIntegerField;
    QrRomaneioCTNO_Tam: TWideStringField;
    QrRomaneioCTOriCodi: TIntegerField;
    QrRomaneioCTTotal: TFloatField;
    QrRomaneioCTPrecoCalc: TFloatField;
    QrRomaneioCTQtde: TFloatField;
    QrRomaneioCTNO_UniMed: TWideStringField;
    QrRomaneioCTCU_NIVEL1: TIntegerField;
    QrRomaneioCTNO_NIVEL1: TWideStringField;
    frxFAT_PEDID_008_02: TfrxReport;
    frxDsRomaneioGP: TfrxDBDataset;
    frxFAT_PEDID_008_01: TfrxReport;
    QrRomaneioGP: TmySQLQuery;
    QrRomaneioGPOriCodi: TIntegerField;
    QrRomaneioGPTotal: TFloatField;
    QrRomaneioGPPrecoCalc: TFloatField;
    QrRomaneioGPQtde: TFloatField;
    QrRomaneioGPCU_NIVEL1: TIntegerField;
    QrRomaneioGPNO_NIVEL1: TWideStringField;
    QrRomaneioGPNO_UniMed: TWideStringField;
    CkLinhas: TCheckBox;
    frxDsCli: TfrxDBDataset;
    QrCli: TmySQLQuery;
    QrCliE_ALL: TWideStringField;
    QrCliCNPJ_TXT: TWideStringField;
    QrCliNOME_TIPO_DOC: TWideStringField;
    QrCliTE1_TXT: TWideStringField;
    QrCliFAX_TXT: TWideStringField;
    QrCliNUMERO_TXT: TWideStringField;
    QrCliCEP_TXT: TWideStringField;
    QrCliCodigo: TIntegerField;
    QrCliTipo: TSmallintField;
    QrCliCodUsu: TIntegerField;
    QrCliNOME_ENT: TWideStringField;
    QrCliCNPJ_CPF: TWideStringField;
    QrCliIE_RG: TWideStringField;
    QrCliRUA: TWideStringField;
    QrCliCOMPL: TWideStringField;
    QrCliBAIRRO: TWideStringField;
    QrCliCIDADE: TWideStringField;
    QrCliNOMELOGRAD: TWideStringField;
    QrCliNOMEUF: TWideStringField;
    QrCliPais: TWideStringField;
    QrCliENDEREF: TWideStringField;
    QrCliTE1: TWideStringField;
    QrCliFAX: TWideStringField;
    QrCliIE: TWideStringField;
    QrCliCAD_FEDERAL: TWideStringField;
    QrCliCAD_ESTADUAL: TWideStringField;
    QrCliIE_TXT: TWideStringField;
    QrRomaneioGPOriCnta: TFloatField;
    QrRomaneioCTOriCnta: TFloatField;
    RGOrdem: TRadioGroup;
    QrRomaneioGPTOTAL_POSI: TFloatField;
    QrRomaneioGPQTDE_POSI: TFloatField;
    QrRomaneioCTTOTAL_POSI: TFloatField;
    QrRomaneioCTQTDE_POSI: TFloatField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel3: TPanel;
    PnSaiDesis: TPanel;
    BtOK: TBitBtn;
    BtSaida: TBitBtn;
    QrCliNUMERO: TFloatField;
    QrCliLograd: TFloatField;
    QrCliCEP: TFloatField;
    QrCliUF: TFloatField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure frxFAT_PEDID_008_01GetValue(const VarName: string;
      var Value: Variant);
    procedure RGTipoRomaneioClick(Sender: TObject);
    procedure RGAgruparPorVolumeClick(Sender: TObject);
    procedure QrCliCalcFields(DataSet: TDataSet);
    procedure QrRomaneioGPCalcFields(DataSet: TDataSet);
    procedure QrRomaneioCTCalcFields(DataSet: TDataSet);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    const
      cValorUnitETotal = 1;
      cDescontoGeral   = 2;
      cParcelamento    = 4;
    procedure HabilitaImpressao();
  public
    { Public declarations }
    FFatID, FCliente, FOriCodigo, FEmpresa, FPedido, FTipoMov: Integer;
    FTituloPedido, FNomeCondicaoPg: String;
    FDescoGeral_Val, FDescoGeral_Per, FComissao_Per, FComissao_Val, FValLiq: Double;
  end;

  var
  FmFatPedImp: TFmFatPedImp;

implementation

uses UnMyObjects, ModuleGeral, Module, UnInternalConsts, dmkGeral, UMySQLModule;

{$R *.DFM}

procedure TFmFatPedImp.BtOKClick(Sender: TObject);
  function Ordenar(): String;
  begin
    Result := 'ORDER BY ';
    if (RGAgruparPorVolume.ItemIndex = 1) and (FFatID=1) then
      Result := Result + 'smva.OriCnta, ';
    case RGOrdem.ItemIndex of
      0: Result := Result + 'CU_NIVEL1';
      1: Result := Result + 'NO_NIVEL1';
      else RESULT := RESULT + '?$?'; // Gerar erro
    end;
    case RGTipoRomaneio.ItemIndex of
      1: ; // nada
      2: Result := Result + ', gcc.Nome, gti.Controle';
      else ; // nada
    end;
  end;
var
  Mens: String;
begin
  Mens := 'Impress�o n�o implementada para o tipo de roamaneio "' +
    RGTipoRomaneio.Items[RGTipoRomaneio.ItemIndex] + '" no movimento ID = ' +
    FormatFloat('000', FFatID) + '.' + sLineBreak + 'AVISE A DERMATEK!';
  if QrCli.State = dsInactive then
  begin
    QrCli.Close;
    QrCli.Params[0].AsInteger := FCliente;
    UMyMod.AbreQuery(QrCli, Dmod.MyDB, 'TFmFatPedImp.BtOKClick()');
  end;
  DModG.ReopenParamsEmp(FEmpresa);
  DModG.ReopenEndereco(FEmpresa);
  //
  QrSmvaEmp.Close;
  QrSmvaEmp.Params[00].AsInteger := FOriCodigo;
  UMyMod.AbreQuery(QrSmvaEmp, Dmod.MyDB, 'TFmFatPedImp.BtOKClick()');
  //
  if dmkPF.IntInConjunto2(cParcelamento(*4*), CGMostrar.Value) then
  begin
    QrLctosV.Close;
    QrLctosV.Params[00].AsInteger := FFatID;
    QrLctosV.Params[01].AsInteger := FOriCodigo;
    UMyMod.AbreQuery(QrLctosV, Dmod.MyDB, 'TFmFatPedImp.BtOKClick()');
  end;
  //
  QrRomaneioGP.Close;
  QrRomaneioGP.SQL.Clear;
  //
  QrRomaneioCT.Close;
  QrRomaneioCT.SQL.Clear;
  case RGTipoRomaneio.ItemIndex of
    1:
    begin
      case FFatID of
        001:
        begin
          QrRomaneioGP.SQL.Add('SELECT smva.OriCodi, smva.OriCnta+0.0 OriCnta, ');
          QrRomaneioGP.SQL.Add('SUM(smva.Total) Total,');
          QrRomaneioGP.SQL.Add('SUM(smva.Total) / SUM(smva.Qtde) PrecoCalc,');
          QrRomaneioGP.SQL.Add('SUM(smva.Qtde) Qtde, ');
          QrRomaneioGP.SQL.Add('med.Nome NO_UniMed,');
          QrRomaneioGP.SQL.Add('gg1.CodUsu CU_NIVEL1, gg1.Nome NO_NIVEL1');
          QrRomaneioGP.SQL.Add('FROM stqmovvala smva');
          QrRomaneioGP.SQL.Add('LEFT JOIN gragrux ggx ON ggx.Controle=smva.GraGruX');
          QrRomaneioGP.SQL.Add('LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1');
          QrRomaneioGP.SQL.Add('LEFT JOIN fatpedvol fpv ON fpv.Cnta=smva.OriCnta');
          QrRomaneioGP.SQL.Add('LEFT JOIN unidmed med ON med.Codigo=fpv.UnidMed');
          QrRomaneioGP.SQL.Add('WHERE smva.Tipo=:P0');
          QrRomaneioGP.SQL.Add('AND smva.OriCodi=:P1');
          QrRomaneioGP.SQL.Add('AND smva.Empresa IN (' + VAR_LIB_EMPRESAS + ')');
          // GROUP BY
          QrRomaneioGP.SQL.Add('GROUP BY ');
          if RGAgruparPorVolume.ItemIndex = 1 then
            QrRomaneioGP.SQL.Add('smva.OriCnta, ');
          QrRomaneioGP.SQL.Add('ggx.GraGru1');
          // ORDER BY
          QrRomaneioGP.SQL.Add(Ordenar());
          //
          QrRomaneioGP.Params[00].AsInteger := FFatID;
          QrRomaneioGP.Params[01].AsInteger := FOriCodigo;
        end;
        003:
        begin
          QrRomaneioGP.SQL.Add('SELECT fci.Codigo OriCodi, 0.0 OriCnta,');
          QrRomaneioGP.SQL.Add('SUM(fci.ValLiq) Total, fci.PrecoR PrecoCalc,');
          QrRomaneioGP.SQL.Add('SUM(fci.QuantP) Qtde, "" NO_UniMed,');
          QrRomaneioGP.SQL.Add('gg1.CodUsu CU_NIVEL1, gg1.Nome NO_NIVEL1');
          QrRomaneioGP.SQL.Add('FROM fatconits fci');
          QrRomaneioGP.SQL.Add('LEFT JOIN gragrux ggx ON ggx.Controle=fci.GraGruX');
          QrRomaneioGP.SQL.Add('LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1');
          QrRomaneioGP.SQL.Add('WHERE fci.Codigo=:P0');
          // GROUP BY
          QrRomaneioGP.SQL.Add('GROUP BY ggx.GraGru1');
          // ORDER BY
          QrRomaneioGP.SQL.Add(Ordenar());
          QrRomaneioGP.Params[00].AsInteger := FOriCodigo;
        end;
        004:
        begin
          QrRomaneioGP.SQL.Add('SELECT fci.Codigo OriCodi, 0.0 OriCnta,');
          QrRomaneioGP.SQL.Add('SUM(fci.ValFat) Total, fci.PrecoR PrecoCalc,');
          QrRomaneioGP.SQL.Add('SUM(fci.QuantV) Qtde, "" NO_UniMed,');
          QrRomaneioGP.SQL.Add('gg1.CodUsu CU_NIVEL1, gg1.Nome NO_NIVEL1');
          QrRomaneioGP.SQL.Add('FROM fatconits fci');
          QrRomaneioGP.SQL.Add('LEFT JOIN fatconcad fcc ON fcc.Codigo=fci.Codigo');
          QrRomaneioGP.SQL.Add('LEFT JOIN gragrux ggx ON ggx.Controle=fci.GraGruX');
          QrRomaneioGP.SQL.Add('LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1');
          QrRomaneioGP.SQL.Add('WHERE fcc.FatConRet=:P0');
          // GROUP BY
          QrRomaneioGP.SQL.Add('GROUP BY ggx.GraGru1');
          // ORDER BY
          QrRomaneioGP.SQL.Add(Ordenar());
          QrRomaneioGP.Params[00].AsInteger := FOriCodigo;
        end;
        099:
        begin
          QrRomaneioGP.SQL.Add('SELECT smi.Codigo OriCodi, 0.0 OriCnta,');
          QrRomaneioGP.SQL.Add('SUM(smi.Valor) Total, smi.Preco PrecoCalc,');
          QrRomaneioGP.SQL.Add('SUM(smi.Qtde) Qtde, "" NO_UniMed,');
          QrRomaneioGP.SQL.Add('gg1.CodUsu CU_NIVEL1, gg1.Nome NO_NIVEL1');
          QrRomaneioGP.SQL.Add('FROM stqmanits smi');
          QrRomaneioGP.SQL.Add('LEFT JOIN gragrux ggx ON ggx.Controle=smi.GraGruX');
          QrRomaneioGP.SQL.Add('LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1');
          QrRomaneioGP.SQL.Add('WHERE smi.Codigo=:P0');
          // GROUP BY
          QrRomaneioGP.SQL.Add('GROUP BY ggx.GraGru1');
          // ORDER BY
          QrRomaneioGP.SQL.Add(Ordenar());
          QrRomaneioGP.Params[00].AsInteger := FOriCodigo;
        end;
        else
        begin
          Geral.MB_Erro(Mens);
          Exit;
        end;
      end;
      //
      UMyMod.AbreQuery(QrRomaneioGP, Dmod.MyDB, 'TFmFatPedImp.BtOKClick()');
      MyObjects.frxMostra(frxFAT_PEDID_008_01, 'Romaneio');
    end;
    2:
    begin
      case FFatID of
        001:
        begin
          QrRomaneioCT.SQL.Add('SELECT ggx.GraGruC, ggx.GraTamI, ggc.GraCorCad,');
          QrRomaneioCT.SQL.Add('gcc.Nome NO_COR, gcc.CodUsu CU_Cor, gti.Nome NO_Tam,');
          QrRomaneioCT.SQL.Add('smva.OriCodi, 0.0+smva.OriCnta OriCnta, SUM(smva.Total) Total,');
          QrRomaneioCT.SQL.Add('SUM(smva.Total) / SUM(smva.Qtde) PrecoCalc,');
          QrRomaneioCT.SQL.Add('SUM(smva.Qtde) Qtde, med.Nome NO_UniMed,');
          QrRomaneioCT.SQL.Add('gg1.CodUsu CU_NIVEL1, gg1.Nome NO_NIVEL1');
          QrRomaneioCT.SQL.Add('FROM stqmovvala smva');
          QrRomaneioCT.SQL.Add('LEFT JOIN gragrux ggx ON ggx.Controle=smva.GraGruX');
          QrRomaneioCT.SQL.Add('LEFT JOIN gragruc ggc ON ggc.Controle=ggx.GraGruC');
          QrRomaneioCT.SQL.Add('LEFT JOIN gracorcad gcc ON gcc.Codigo=ggc.GraCorCad');
          QrRomaneioCT.SQL.Add('LEFT JOIN gratamits gti ON gti.Controle=ggx.GraTamI');
          QrRomaneioCT.SQL.Add('LEFT JOIN gragru1   gg1 ON gg1.Nivel1=ggx.GraGru1');
          QrRomaneioCT.SQL.Add('LEFT JOIN fatpedvol fpv ON fpv.Cnta=smva.OriCnta');
          QrRomaneioCT.SQL.Add('LEFT JOIN unidmed med ON med.Codigo=fpv.UnidMed');
          QrRomaneioCT.SQL.Add('WHERE smva.Tipo=:P0');
          QrRomaneioCT.SQL.Add('AND smva.OriCodi=:P1');
          QrRomaneioCT.SQL.Add('AND smva.Empresa IN (' + VAR_LIB_EMPRESAS + ')');
          //
          QrRomaneioCT.SQL.Add('GROUP BY ');
          if RGAgruparPorVolume.ItemIndex = 1 then
            QrRomaneioCT.SQL.Add('smva.OriCnta,');
          QrRomaneioCT.SQL.Add('ggx.GraGru1, ggc.GraCorCad, ggx.GraTamI');
          //
          QrRomaneioCT.SQL.Add(Ordenar());
          //
          QrRomaneioCT.Params[00].AsInteger := FFatID;
          QrRomaneioCT.Params[01].AsInteger := FOriCodigo;
        end;
        003:
        begin
          QrRomaneioCT.SQL.Add('SELECT ggx.GraGruC, ggx.GraTamI, ggc.GraCorCad,');
          QrRomaneioCT.SQL.Add('gcc.Nome NO_COR, gcc.CodUsu CU_Cor, gti.Nome NO_Tam,');
          QrRomaneioCT.SQL.Add('fci.Codigo OriCodi, 0.0 OriCnta, SUM(fci.ValLiq) Total,');
          QrRomaneioCT.SQL.Add('fci.PrecoR PrecoCalc,');
          QrRomaneioCT.SQL.Add('SUM(fci.QuantP) Qtde, "" NO_UniMed,');
          QrRomaneioCT.SQL.Add('gg1.CodUsu CU_NIVEL1, gg1.Nome NO_NIVEL1');
          QrRomaneioCT.SQL.Add('FROM fatconits fci');
          QrRomaneioCT.SQL.Add('LEFT JOIN gragrux ggx ON ggx.Controle=fci.GraGruX');
          QrRomaneioCT.SQL.Add('LEFT JOIN gragruc ggc ON ggc.Controle=ggx.GraGruC');
          QrRomaneioCT.SQL.Add('LEFT JOIN gracorcad gcc ON gcc.Codigo=ggc.GraCorCad');
          QrRomaneioCT.SQL.Add('LEFT JOIN gratamits gti ON gti.Controle=ggx.GraTamI');
          QrRomaneioCT.SQL.Add('LEFT JOIN gragru1   gg1 ON gg1.Nivel1=ggx.GraGru1');
          QrRomaneioCT.SQL.Add('WHERE fci.Codigo=:P0');
          QrRomaneioCT.SQL.Add('GROUP BY');
          QrRomaneioCT.SQL.Add('ggx.GraGru1, ggc.GraCorCad, ggx.GraTamI');
          //
          QrRomaneioCT.SQL.Add(Ordenar());
          //
          QrRomaneioCT.Params[00].AsInteger := FOriCodigo;
        end;
        004:
        begin
          QrRomaneioCT.SQL.Add('SELECT ggx.GraGruC, ggx.GraTamI, ggc.GraCorCad,');
          QrRomaneioCT.SQL.Add('gcc.Nome NO_COR, gcc.CodUsu CU_Cor, gti.Nome NO_Tam,');
          QrRomaneioCT.SQL.Add('fci.Codigo OriCodi, 0.0 OriCnta, SUM(fci.ValFat) Total,');
          QrRomaneioCT.SQL.Add('fci.PrecoR PrecoCalc,');
          QrRomaneioCT.SQL.Add('SUM(fci.QuantV) Qtde, "" NO_UniMed,');
          QrRomaneioCT.SQL.Add('gg1.CodUsu CU_NIVEL1, gg1.Nome NO_NIVEL1');
          QrRomaneioCT.SQL.Add('FROM fatconits fci');
          QrRomaneioCT.SQL.Add('LEFT JOIN fatconcad fcc ON fcc.Codigo=fci.Codigo');
          QrRomaneioCT.SQL.Add('LEFT JOIN gragrux ggx ON ggx.Controle=fci.GraGruX');
          QrRomaneioCT.SQL.Add('LEFT JOIN gragruc ggc ON ggc.Controle=ggx.GraGruC');
          QrRomaneioCT.SQL.Add('LEFT JOIN gracorcad gcc ON gcc.Codigo=ggc.GraCorCad');
          QrRomaneioCT.SQL.Add('LEFT JOIN gratamits gti ON gti.Controle=ggx.GraTamI');
          QrRomaneioCT.SQL.Add('LEFT JOIN gragru1   gg1 ON gg1.Nivel1=ggx.GraGru1');
          QrRomaneioCT.SQL.Add('WHERE fcc.FatConRet=:P0');
          QrRomaneioCT.SQL.Add('GROUP BY');
          QrRomaneioCT.SQL.Add('ggx.GraGru1, ggc.GraCorCad, ggx.GraTamI');
          //
          QrRomaneioCT.SQL.Add(Ordenar());
          //
          QrRomaneioCT.Params[00].AsInteger := FOriCodigo;
        end;
        099:
        begin
          QrRomaneioCT.SQL.Add('SELECT ggx.GraGruC, ggx.GraTamI, ggc.GraCorCad,');
          QrRomaneioCT.SQL.Add('gcc.Nome NO_COR, gcc.CodUsu CU_Cor, gti.Nome NO_Tam,');
          QrRomaneioCT.SQL.Add('smi.Codigo OriCodi, 0.0 OriCnta, SUM(smi.Valor) Total,');
          QrRomaneioCT.SQL.Add('smi.Preco PrecoCalc,');
          QrRomaneioCT.SQL.Add('SUM(smi.Qtde) Qtde, "" NO_UniMed,');
          QrRomaneioCT.SQL.Add('gg1.CodUsu CU_NIVEL1, gg1.Nome NO_NIVEL1');
          QrRomaneioCT.SQL.Add('FROM stqmanits smi');
          QrRomaneioCT.SQL.Add('LEFT JOIN gragrux ggx ON ggx.Controle=smi.GraGruX');
          QrRomaneioCT.SQL.Add('LEFT JOIN gragruc ggc ON ggc.Controle=ggx.GraGruC');
          QrRomaneioCT.SQL.Add('LEFT JOIN gracorcad gcc ON gcc.Codigo=ggc.GraCorCad');
          QrRomaneioCT.SQL.Add('LEFT JOIN gratamits gti ON gti.Controle=ggx.GraTamI');
          QrRomaneioCT.SQL.Add('LEFT JOIN gragru1   gg1 ON gg1.Nivel1=ggx.GraGru1');
          QrRomaneioCT.SQL.Add('WHERE smi.Codigo=:P0');
          QrRomaneioCT.SQL.Add('GROUP BY');
          QrRomaneioCT.SQL.Add('ggx.GraGru1, ggc.GraCorCad, ggx.GraTamI');
          //
          QrRomaneioCT.SQL.Add(Ordenar());
          //
          QrRomaneioCT.Params[00].AsInteger := FOriCodigo;
        end;
        else
        begin
          Geral.MB_Erro(Mens);
          Exit;
        end;
      end;
      UMyMod.AbreQuery(QrRomaneioCT, Dmod.MyDB, 'TFmFatPedImp.BtOKClick()');
      MyObjects.frxMostra(frxFAT_PEDID_008_02, 'Romaneio');
    end;
  end;
end;

procedure TFmFatPedImp.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmFatPedImp.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmFatPedImp.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  FDescoGeral_Val := 0;
  FDescoGeral_Per := 0;
  FComissao_Val   := 0;
  FComissao_Per   := 0;
  FValLiq         := 0;
end;

procedure TFmFatPedImp.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmFatPedImp.frxFAT_PEDID_008_01GetValue(const VarName: string;
  var Value: Variant);
begin
  if VarName = 'MeuLogo3x1Existe' then
    Value := FileExists(DModG.QrParamsEmpLogo3x1.Value)
  else
  if VarName = 'MeuLogo3x1Caminho' then
    Value := DModG.QrParamsEmpLogo3x1.Value
  else
  if VarName = 'VARF_AGRUPA_VOLUME' then
    Value := RGAgruparPorVolume.ItemIndex = 1
  else   
  if VarName = 'VARF_MOSTRA_PARCELAMENTO' then
    Value := dmkPF.IntInConjunto2(cParcelamento, CGMostrar.Value)
  else
  if VarName = 'VARF_MOSTRA_DESCONTO' then
    Value := dmkPF.IntInConjunto2(cDescontoGeral, CGMostrar.Value)
  else
  if VarName = 'VARF_MOSTRA_VALORES' then
    Value := dmkPF.IntInConjunto2(cValorUnitETotal, CGMostrar.Value)
  else
  if VarName = 'VARF_FORMATA_QTDE' then
    Value := dmkPF.FormataCasas(Dmod.QrControleCasasProd.Value)
  else
  if VarName = 'VARF_TIT_PED' then
    Value := FTituloPedido
  else
  if VarName = 'VARF_PEDIDO' then
    Value := FPedido
  else
  if VarName = 'VARF_MOSTRA_LINHA' then
    Value := CkLinhas.Checked
  else
  if VarName = 'VARF_NOMECONDICAOPG' then
   //[frxDsPediVda."NOMECONDICAOPG"]
    Value := FNOMECONDICAOPG
  else
  if VarName = 'VARF_ENTRA_SAI' then
  begin
    case FTipoMov of
      0: Value := 'ENTRADA';
      1: Value := 'SA�DA';
    end;
  end else
  if VarName = 'VARF_DESCONTO_GERAL_VAL' then
    Value := FDescoGeral_Val
  else
  if VarName = 'VARF_DESCONTO_GERAL_PER' then
    Value := FDescoGeral_Per
  else
  if VarName = 'VARF_COMISSAO_VAL' then
    Value := FComissao_Val
  else
  if VarName = 'VARF_COMISSAO_PER' then
    Value := FComissao_Per
  else
  if VarName = 'VARF_VAL_LIQ' then
    Value := FValLiq
  else
end;

procedure TFmFatPedImp.HabilitaImpressao;
var
  Habilita: Boolean;
begin
  Habilita := (RGTipoRomaneio.ItemIndex > 0)
              and
              (RGAgruparPorVolume.ItemIndex < 2);
  BtOK.Enabled := Habilita;
end;

procedure TFmFatPedImp.QrCliCalcFields(DataSet: TDataSet);
begin
  QrCliTE1_TXT.Value :=
    Geral.FormataTelefone_TT_Curto(QrCliTe1.Value);
  QrCliFAX_TXT.Value :=
    Geral.FormataTelefone_TT_Curto(QrCliFax.Value);
  QrCliCNPJ_TXT.Value :=
    Geral.FormataCNPJ_TT(QrCliCNPJ_CPF.Value);
  QrCliIE_TXT.Value :=
    Geral.Formata_IE(QrCliIE_RG.Value, QrCliUF.Value, '??');
  QrCliNUMERO_TXT.Value :=
    Geral.FormataNumeroDeRua(QrCliRua.Value, QrCliNumero.Value, False);
  //
  QrCliE_ALL.Value := Uppercase(QrCliNOMELOGRAD.Value);
  if Trim(QrCliE_ALL.Value) <> '' then QrCliE_ALL.Value :=
    QrCliE_ALL.Value + ' ';
  QrCliE_ALL.Value := QrCliE_ALL.Value + Uppercase(QrCliRua.Value);
  if Trim(QrCliRua.Value) <> '' then QrCliE_ALL.Value :=
    QrCliE_ALL.Value + ', ' + QrCliNUMERO_TXT.Value;
  if Trim(QrCliCompl.Value) <>  '' then QrCliE_ALL.Value :=
    QrCliE_ALL.Value + ' ' + Uppercase(QrCliCompl.Value);
  if Trim(QrCliBairro.Value) <>  '' then QrCliE_ALL.Value :=
    QrCliE_ALL.Value + ' - ' + Uppercase(QrCliBairro.Value);
  if QrCliCEP.Value > 0 then QrCliE_ALL.Value :=
    QrCliE_ALL.Value + ' CEP ' +Geral.FormataCEP_NT(QrCliCEP.Value);
  if Trim(QrCliCidade.Value) <>  '' then QrCliE_ALL.Value :=
    QrCliE_ALL.Value + ' - ' + Uppercase(QrCliCidade.Value);
  if Trim(QrCliNOMEUF.Value) <>  '' then QrCliE_ALL.Value :=
    QrCliE_ALL.Value + ', ' + QrCliNOMEUF.Value;
  if Trim(QrCliPais.Value) <>  '' then QrCliE_ALL.Value :=
    QrCliE_ALL.Value + ' - ' + QrCliPais.Value;
  //
  //QrCliCEP_TXT.Value :=Geral.FormataCEP_NT(QrCliCEP.Value);
  //
end;

procedure TFmFatPedImp.QrRomaneioCTCalcFields(DataSet: TDataSet);
begin
  if QrRomaneioCTTotal.Value < 0 then
    QrRomaneioCTTotal_POSI.Value :=  -QrRomaneioCTTotal.Value
  else
    QrRomaneioCTTotal_POSI.Value := QrRomaneioCTTotal.Value;
  //
  if QrRomaneioCTQtde.Value < 0 then
    QrRomaneioCTQtde_POSI.Value :=  -QrRomaneioCTQtde.Value
  else
    QrRomaneioCTQtde_POSI.Value := QrRomaneioCTQtde.Value;
end;

procedure TFmFatPedImp.QrRomaneioGPCalcFields(DataSet: TDataSet);
begin
  if QrRomaneioGPTotal.Value < 0 then
    QrRomaneioGPTotal_POSI.Value :=  -QrRomaneioGPTotal.Value
  else
    QrRomaneioGPTotal_POSI.Value := QrRomaneioGPTotal.Value;
  //
  if QrRomaneioGPQtde.Value < 0 then
    QrRomaneioGPQtde_POSI.Value :=  -QrRomaneioGPQtde.Value
  else
    QrRomaneioGPQtde_POSI.Value := QrRomaneioGPQtde.Value;
end;

procedure TFmFatPedImp.RGAgruparPorVolumeClick(Sender: TObject);
begin
  HabilitaImpressao();
end;

procedure TFmFatPedImp.RGTipoRomaneioClick(Sender: TObject);
begin
  HabilitaImpressao();
end;

end.

