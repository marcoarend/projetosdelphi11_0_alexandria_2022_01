unit UnGPed_Jan;

interface

uses
  Windows, Forms, SysUtils, Classes, DB, mySQLDbTables, Controls,
  DmkGeral, DmkDAC_PF,
  UnInternalConsts, UnDmkProcFunc, frxClass, frxDBSet, Menus,
  dmkDBLookupComboBox, dmkEditCB, UnDmkEnums;

type
  TUnGPed_Jan = class(TObject)
  private
    { Private declarations }

  public
    { Public declarations }

    //procedure MostraFormFatConCad();
    procedure MostraFormPediAcc();
    procedure MostraFormPediVda();
    procedure MostraFormPediVdaImp();
    procedure MostraFormGeoSite();
    procedure MostraFormRegioes();

  end;

var
  GPed_Jan: TUnGPed_Jan;


implementation

uses MyDBCheck, ModuleGeral, ModProd,
  PediVda, (*FatConCad,*) ModPediVda, PediVdaImp, PediAcc, GeoSite, Regioes;


{ TUnGPed_Jan }

{
procedure TUnGPed_Jan.MostraFormFatConCad();
begin
  if DBCheck.CriaFm(TFmFatConCad, FmFatConCad, afmoNegarComAviso) then
  begin
    FmFatConCad.ShowModal;
    FmFatConCad.Destroy;
  end;
end;
}

procedure TUnGPed_Jan.MostraFormGeoSite;
begin
  if DBCheck.CriaFm(TFmGeosite, FmGeosite, afmoNegarComAviso) then
  begin
    FmGeosite.ShowModal;
    FmGeosite.Destroy;
  end;
end;

procedure TUnGPed_Jan.MostraFormPediAcc;
begin
  if DBCheck.CriaFm(TFmPediAcc, FmPediAcc, afmoNegarComAviso) then
  begin
    FmPediAcc.ShowModal;
    FmPediAcc.Destroy;
  end;
end;

procedure TUnGPed_Jan.MostraFormPediVda;
begin
  Screen.Cursor := crHourGlass;
  DmodG.ReopenEmpresas(VAR_USUARIO, 0);
  DmPediVda.ReopenTabelasPedido();
  Screen.Cursor := crDefault;
  //
  if DBCheck.CriaFm(TFmPediVda, FmPediVda, afmoNegarComAviso) then
  begin
    FmPediVda.ShowModal;
    FmPediVda.Destroy;
  end;
end;

procedure TUnGPed_Jan.MostraFormPediVdaImp();
begin
  Screen.Cursor := crHourGlass;
  DModG.ReopenEmpresas(VAR_USUARIO, 0);
  // N�o precisa todas, Mudar?
  DmPediVda.ReopenTabelasPedido();
  Screen.Cursor := crDefault;
  //
  if DBCheck.CriaFm(TFmPediVdaImp, FmPediVdaImp, afmoNegarComAviso) then
  begin
    FmPediVdaImp.ShowModal;
    FmPediVdaImp.Destroy;
  end;
end;

procedure TUnGPed_Jan.MostraFormRegioes();
begin
  if DBCheck.CriaFm(TFmRegioes, FmRegioes, afmoNegarComAviso) then
  begin
    FmRegioes.ShowModal;
    FmRegioes.Destroy;
  end;
end;

end.
