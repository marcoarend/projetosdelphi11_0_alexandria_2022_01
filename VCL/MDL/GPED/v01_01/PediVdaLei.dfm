object FmPediVdaLei: TFmPediVdaLei
  Left = 339
  Top = 185
  Caption = 'PED-VENDA-004 :: Item de Pedido - Inclus'#227'o por Leitura'
  ClientHeight = 756
  ClientWidth = 1241
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -14
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 120
  TextHeight = 16
  object Panel1: TPanel
    Left = 0
    Top = 59
    Width = 1241
    Height = 564
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object PnLeitura: TPanel
      Left = 0
      Top = 0
      Width = 1241
      Height = 129
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alTop
      BevelOuter = bvNone
      ParentBackground = False
      TabOrder = 0
      object Panel4: TPanel
        Left = 0
        Top = 0
        Width = 814
        Height = 59
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alLeft
        BevelOuter = bvNone
        TabOrder = 0
        object Label3: TLabel
          Left = 5
          Top = 5
          Width = 109
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Leitura / digita'#231#227'o:'
        end
        object LaQtdeLei: TLabel
          Left = 231
          Top = 5
          Width = 32
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Qtde:'
        end
        object Label1: TLabel
          Left = 286
          Top = 5
          Width = 80
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Pre'#231'o tabela:'
          Enabled = False
        end
        object Label2: TLabel
          Left = 374
          Top = 5
          Width = 65
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Pre'#231'o real:'
          Enabled = False
        end
        object Label10: TLabel
          Left = 463
          Top = 5
          Width = 73
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '%Desconto:'
          Enabled = False
        end
        object EdQtdLei: TdmkEdit
          Left = 231
          Top = 25
          Width = 50
          Height = 25
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Alignment = taRightJustify
          Enabled = False
          TabOrder = 1
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = True
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '1'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 1
          ValWarn = False
          OnEnter = EdQtdLeiEnter
          OnKeyDown = EdQtdLeiKeyDown
        end
        object BtOK: TBitBtn
          Tag = 14
          Left = 551
          Top = 4
          Width = 111
          Height = 49
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&OK'
          NumGlyphs = 2
          TabOrder = 5
          OnClick = BtOKClick
        end
        object EdPrecoO: TdmkEdit
          Left = 286
          Top = 25
          Width = 83
          Height = 25
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Alignment = taRightJustify
          Enabled = False
          TabOrder = 2
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object EdPrecoR: TdmkEdit
          Left = 374
          Top = 25
          Width = 84
          Height = 25
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Alignment = taRightJustify
          Enabled = False
          TabOrder = 3
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object EdDescoP: TdmkEdit
          Left = 463
          Top = 25
          Width = 83
          Height = 25
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Alignment = taRightJustify
          Enabled = False
          TabOrder = 4
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 4
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,0000'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object PnJuros: TPanel
          Left = 671
          Top = 0
          Width = 143
          Height = 59
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alRight
          BevelOuter = bvLowered
          Enabled = False
          TabOrder = 6
          object Label11: TLabel
            Left = 5
            Top = 5
            Width = 66
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = '% Jur/m'#234's:'
            FocusControl = DBEdit4
          end
          object Label12: TLabel
            Left = 74
            Top = 5
            Width = 41
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = '% C.F.:'
            FocusControl = DBEdit4
          end
          object DBEdit4: TDBEdit
            Left = 5
            Top = 25
            Width = 65
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            DataField = 'JurosMes'
            DataSource = FmPediVda.DsPediVda
            TabOrder = 0
          end
          object EdCustoFin: TdmkEdit
            Left = 74
            Top = 25
            Width = 65
            Height = 25
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Alignment = taRightJustify
            TabOrder = 1
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 4
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,0000'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
        end
        object EdLeitura: TEdit
          Left = 5
          Top = 25
          Width = 219
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          TabOrder = 0
          OnChange = EdLeituraChange
          OnExit = EdLeituraExit
          OnKeyDown = EdLeituraKeyDown
        end
      end
      object Panel9: TPanel
        Left = 814
        Top = 0
        Width = 427
        Height = 59
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        Enabled = False
        TabOrder = 1
        object Label4: TLabel
          Left = 5
          Top = 5
          Width = 108
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Grupo de produto:'
          FocusControl = DBEdit1
        end
        object Label5: TLabel
          Left = 256
          Top = 5
          Width = 24
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Cor:'
          FocusControl = DBEdit2
        end
        object Label6: TLabel
          Left = 359
          Top = 5
          Width = 61
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Tamanho:'
          FocusControl = DBEdit3
        end
        object DBEdit1: TDBEdit
          Left = 5
          Top = 25
          Width = 246
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          DataField = 'NOMENIVEL1'
          DataSource = DsItem
          TabOrder = 0
        end
        object DBEdit2: TDBEdit
          Left = 256
          Top = 25
          Width = 98
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          DataField = 'NOMECOR'
          DataSource = DsItem
          TabOrder = 1
        end
        object DBEdit3: TDBEdit
          Left = 359
          Top = 25
          Width = 61
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          DataField = 'NOMETAM'
          DataSource = DsItem
          TabOrder = 2
        end
      end
      object Panel3: TPanel
        Left = 0
        Top = 59
        Width = 1241
        Height = 70
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alBottom
        BevelOuter = bvNone
        TabOrder = 2
        object ST1: TStaticText
          Left = 0
          Top = 4
          Width = 1241
          Height = 22
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alBottom
          Alignment = taCenter
          Caption = 'ST1'
          Color = clBtnFace
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clRed
          Font.Height = -15
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentColor = False
          ParentFont = False
          TabOrder = 0
          Transparent = False
        end
        object ST2: TStaticText
          Left = 0
          Top = 26
          Width = 1241
          Height = 22
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alBottom
          Alignment = taCenter
          Caption = 'ST2'
          Color = clBtnFace
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clRed
          Font.Height = -15
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentColor = False
          ParentFont = False
          TabOrder = 1
          Transparent = False
        end
        object ST3: TStaticText
          Left = 0
          Top = 48
          Width = 1241
          Height = 22
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alBottom
          Alignment = taCenter
          Caption = 'ST3'
          Color = clBtnFace
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clRed
          Font.Height = -15
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentColor = False
          ParentFont = False
          TabOrder = 2
          Transparent = False
        end
      end
    end
    object PnSimu: TPanel
      Left = 0
      Top = 498
      Width = 1241
      Height = 66
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alBottom
      BevelOuter = bvNone
      TabOrder = 1
      Visible = False
      object Label9: TLabel
        Left = 453
        Top = 5
        Width = 68
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Sequ'#234'ncia:'
      end
      object Label8: TLabel
        Left = 350
        Top = 5
        Width = 61
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Reduzido:'
      end
      object Label7: TLabel
        Left = 246
        Top = 5
        Width = 95
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Ordem de serv.:'
      end
      object dmkEdit2: TdmkEdit
        Left = 350
        Top = 25
        Width = 99
        Height = 25
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 6
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '000010'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 10
        ValWarn = False
        OnChange = EdLeituraChange
      end
      object dmkEdit3: TdmkEdit
        Left = 453
        Top = 25
        Width = 100
        Height = 25
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        TabOrder = 1
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 8
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '00000001'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 1
        ValWarn = False
        OnChange = EdLeituraChange
      end
      object dmkEdit1: TdmkEdit
        Left = 246
        Top = 25
        Width = 100
        Height = 25
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        TabOrder = 2
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 6
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '000000'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = EdLeituraChange
      end
      object Button1: TButton
        Left = 5
        Top = 15
        Width = 228
        Height = 31
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Simula leitura do c'#243'digo de barras'
        TabOrder = 3
        OnClick = Button1Click
      end
    end
    object DBGPediVdaIts: TDBGrid
      Left = 0
      Top = 129
      Width = 1241
      Height = 369
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      DataSource = DsPediVdaIts
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
      TabOrder = 2
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -14
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'CU_Nivel1'
          Title.Caption = 'Produto'
          Width = 68
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_Nivel1'
          Title.Caption = 'Descri'#231#227'o'
          Width = 283
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CU_Cor'
          Title.Caption = 'Cor'
          Width = 59
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_Cor'
          Title.Caption = 'Descri'#231#227'o'
          Width = 175
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_Tam'
          Title.Caption = 'Tamanho'
          Width = 78
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'QuantP'
          Title.Caption = 'Quantidade'
          Width = 69
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'PrecoF'
          Title.Caption = 'Pre'#231'o'
          Width = 100
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ValLiq'
          Title.Caption = 'Val.Liq.'
          Width = 100
          Visible = True
        end>
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1241
    Height = 59
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 1182
      Top = 0
      Width = 59
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 10
        Top = 14
        Width = 39
        Height = 39
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 59
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 59
      Top = 0
      Width = 1123
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 9
        Top = 11
        Width = 524
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Item de Pedido - Inclus'#227'o por Leitura'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 11
        Top = 14
        Width = 524
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Item de Pedido - Inclus'#227'o por Leitura'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 10
        Top = 12
        Width = 524
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Item de Pedido - Inclus'#227'o por Leitura'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 623
    Width = 1241
    Height = 54
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel5: TPanel
      Left = 2
      Top = 18
      Width = 1237
      Height = 34
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 16
        Top = 2
        Width = 150
        Height = 19
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -17
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 15
        Top = 1
        Width = 150
        Height = 19
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -17
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 677
    Width = 1241
    Height = 79
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alBottom
    TabOrder = 3
    object Panel6: TPanel
      Left = 2
      Top = 18
      Width = 1237
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object PnSaiDesis: TPanel
        Left = 1059
        Top = 0
        Width = 178
        Height = 59
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 4
        object BtSaida: TBitBtn
          Tag = 13
          Left = 2
          Top = 4
          Width = 148
          Height = 49
          Cursor = crHandPoint
          Hint = 'Sai da janela atual'
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Sa'#237'da'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
        end
      end
      object CkFixo: TCheckBox
        Left = 15
        Top = 20
        Width = 154
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Quantidade fixa.'
        Checked = True
        State = cbChecked
        TabOrder = 0
        OnClick = CkFixoClick
      end
      object BtExclui: TBitBtn
        Tag = 12
        Left = 498
        Top = 4
        Width = 173
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Excluir sequ'#234'ncia'
        NumGlyphs = 2
        TabOrder = 3
        Visible = False
        OnClick = BtExcluiClick
      end
      object BtGraGruN: TBitBtn
        Tag = 30
        Left = 140
        Top = 4
        Width = 173
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Produtos'
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtGraGruNClick
      end
      object BtTabePrcCab: TBitBtn
        Tag = 10107
        Left = 320
        Top = 4
        Width = 172
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Tabela de pre'#231'os'
        NumGlyphs = 2
        TabOrder = 2
        OnClick = BtTabePrcCabClick
      end
    end
  end
  object QrItem: TmySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrItemBeforeClose
    SQL.Strings = (
      'SELECT gg1.Nome NOMENIVEL1, ggc.GraCorCad, '
      'gcc.Nome NOMECOR,  gti.Nome NOMETAM, '
      'ggx.Controle GraGruX, ggx.GraGru1, '
      'gg1.CodUsu CU_Nivel1, gg1.IPI_Alq, pgt.MadeBy,'
      'gg1.PrdGrupTip, gg1.NCM, gg1.UnidMed'
      'FROM gragrux ggx '
      'LEFT JOIN gragru1   gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN gragruc ggc ON ggc.Controle=ggx.GraGruC'
      'LEFT JOIN gracorcad gcc ON gcc.Codigo=ggc.GraCorCad'
      'LEFT JOIN gratamits gti ON gti.Controle=ggx.GraTamI'
      'LEFT JOIN prdgruptip pgt ON pgt.Codigo=gg1.PrdGrupTip'
      'WHERE ggx.Controle=:P0'
      '')
    Left = 68
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrItemNOMENIVEL1: TWideStringField
      FieldName = 'NOMENIVEL1'
      Origin = 'gragru1.Nome'
      Size = 30
    end
    object QrItemGraCorCad: TIntegerField
      FieldName = 'GraCorCad'
      Origin = 'gragruc.GraCorCad'
    end
    object QrItemNOMECOR: TWideStringField
      FieldName = 'NOMECOR'
      Origin = 'gracorcad.Nome'
      Size = 30
    end
    object QrItemNOMETAM: TWideStringField
      FieldName = 'NOMETAM'
      Origin = 'gratamits.Nome'
      Size = 5
    end
    object QrItemGraGruX: TIntegerField
      FieldName = 'GraGruX'
      Origin = 'gragrux.Controle'
      Required = True
    end
    object QrItemGraGru1: TIntegerField
      FieldName = 'GraGru1'
      Origin = 'gragrux.GraGru1'
      Required = True
    end
    object QrItemCU_Nivel1: TIntegerField
      FieldName = 'CU_Nivel1'
      Required = True
    end
    object QrItemIPI_Alq: TFloatField
      FieldName = 'IPI_Alq'
    end
    object QrItemMadeBy: TSmallintField
      FieldName = 'MadeBy'
    end
    object QrItemPrdGrupTip: TIntegerField
      FieldName = 'PrdGrupTip'
    end
    object QrItemNCM: TWideStringField
      FieldName = 'NCM'
      Size = 10
    end
    object QrItemUnidMed: TIntegerField
      FieldName = 'UnidMed'
    end
  end
  object DsItem: TDataSource
    DataSet = QrItem
    Left = 96
    Top = 8
  end
  object QrPreco: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT pvi.PrecoF, '
      '(QuantP-QuantC-QuantV) QuantF'
      'FROM pedivdaits pvi'
      'WHERE pvi.Codigo=:P0'
      'AND pvi.GraGruX=:P1'
      ' ')
    Left = 124
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrPrecoPrecoF: TFloatField
      FieldName = 'PrecoF'
      Required = True
    end
    object QrPrecoQuantF: TFloatField
      FieldName = 'QuantF'
      Required = True
    end
  end
  object DsPreco: TDataSource
    DataSet = QrPreco
    Left = 152
    Top = 8
  end
  object QrFator: TmySQLQuery
    Database = Dmod.MyDB
    Left = 180
    Top = 8
  end
  object QrPediVdaIts: TmySQLQuery
    Database = Dmod.MyDB
    AfterScroll = QrPediVdaItsAfterScroll
    SQL.Strings = (
      'SELECT gg1.CodUsu CU_Nivel1, gg1.Nome NO_Nivel1,  '
      'gcc.CodUsu CU_Cor, gcc.Nome NO_Cor, gti.Nome NO_Tam, '
      'pvi.GraGruX, pvi.PrecoF,pvi.QuantP, pvi.QuantC, '
      'pvi.QuantV, pvi.Controle, pvi.ValLiq, gg1.Nivel1'
      'FROM pedivdaits pvi'
      'LEFT JOIN gragrux   ggx ON ggx.Controle=pvi.GraGruX'
      'LEFT JOIN gratamits gti ON gti.Controle=ggx.GraTamI'
      'LEFT JOIN gragru1   gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN gragruc   ggc ON ggc.Controle=ggx.GraGruC'
      'LEFT JOIN gracorcad   gcc ON gcc.Codigo=ggc.GraCorCad'
      'WHERE pvi.Codigo=:P0'
      'ORDER BY NO_Nivel1, NO_Cor, NO_Tam, PrecoF')
    Left = 708
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPediVdaItsCU_Nivel1: TIntegerField
      FieldName = 'CU_Nivel1'
    end
    object QrPediVdaItsNO_Nivel1: TWideStringField
      FieldName = 'NO_Nivel1'
      Size = 30
    end
    object QrPediVdaItsCU_Cor: TIntegerField
      FieldName = 'CU_Cor'
    end
    object QrPediVdaItsNO_Cor: TWideStringField
      FieldName = 'NO_Cor'
      Size = 30
    end
    object QrPediVdaItsNO_Tam: TWideStringField
      FieldName = 'NO_Tam'
      Size = 5
    end
    object QrPediVdaItsGraGruX: TIntegerField
      FieldName = 'GraGruX'
      Required = True
    end
    object QrPediVdaItsPrecoF: TFloatField
      FieldName = 'PrecoF'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrPediVdaItsQuantP: TFloatField
      FieldName = 'QuantP'
    end
    object QrPediVdaItsQuantC: TFloatField
      FieldName = 'QuantC'
      Required = True
    end
    object QrPediVdaItsQuantV: TFloatField
      FieldName = 'QuantV'
      Required = True
    end
    object QrPediVdaItsControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrPediVdaItsValLiq: TFloatField
      FieldName = 'ValLiq'
      Required = True
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrPediVdaItsNivel1: TIntegerField
      FieldName = 'Nivel1'
      Required = True
    end
  end
  object DsPediVdaIts: TDataSource
    DataSet = QrPediVdaIts
    Left = 736
    Top = 8
  end
  object QrLista: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT GraCusPrc'
      'FROM fisregmvt'
      'WHERE TipoMov=1'
      'AND Empresa=:P0'
      'AND Codigo=:P1')
    Left = 208
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrListaGraCusPrc: TIntegerField
      FieldName = 'GraCusPrc'
    end
  end
  object QrExiste: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle, QuantP, PrecoR, DescoP'
      'FROM pedivdaits'
      'WHERE Codigo=:P0'
      'AND GraGruX=:P1'
      '')
    Left = 764
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrExisteControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrExisteQuantP: TFloatField
      FieldName = 'QuantP'
    end
    object QrExistePrecoR: TFloatField
      FieldName = 'PrecoR'
    end
    object QrExisteDescoP: TFloatField
      FieldName = 'DescoP'
    end
  end
  object QrPediVdaLei: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Conta, Sequencia'
      'FROM pedivdalei'
      'WHERE Controle=:P0'
      'ORDER BY Sequencia')
    Left = 792
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPediVdaLeiConta: TIntegerField
      FieldName = 'Conta'
    end
    object QrPediVdaLeiSequencia: TIntegerField
      FieldName = 'Sequencia'
    end
  end
  object DsPediVdaLei: TDataSource
    DataSet = QrPediVdaLei
    Left = 820
    Top = 8
  end
end
