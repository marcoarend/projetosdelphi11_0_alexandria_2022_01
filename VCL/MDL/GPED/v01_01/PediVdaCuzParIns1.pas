unit PediVdaCuzParIns1;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB,
  DB, mySQLDbTables, Grids, dmkRadioGroup, Mask, Variants, dmkLabel, dmkImage,
  UnDmkEnums, UnDmkProcFunc, DmkDAC_PF;

type
  TFmPediVdaCuzParIns1 = class(TForm)
    Panel1: TPanel;
    Panel3: TPanel;
    GroupBox2: TGroupBox;
    Label2: TLabel;
    SpeedButton1: TSpeedButton;
    EdMatPartCad: TdmkEditCB;
    CBMatPartCad: TdmkDBLookupComboBox;
    RGGrupTip: TRadioGroup;
    PnDados: TPanel;
    QrMatPartCad: TmySQLQuery;
    QrMatPartCadCodigo: TIntegerField;
    QrMatPartCadCodUsu: TIntegerField;
    QrMatPartCadNome: TWideStringField;
    DsMatPartCad: TDataSource;
    QrGraGru1: TmySQLQuery;
    QrGraGru1Nivel1: TIntegerField;
    QrGraGru1CodUsu: TIntegerField;
    QrGraGru1Nome: TWideStringField;
    QrGraGru1GraTamCad: TIntegerField;
    DsGraGru1: TDataSource;
    GroupBox3: TGroupBox;
    Label4: TLabel;
    Label5: TLabel;
    EdPesqGraGru1: TEdit;
    CBGraGru1: TdmkDBLookupComboBox;
    EdGraGru1: TdmkEditCB;
    GradeC: TStringGrid;
    Panel4: TPanel;
    GradeA: TStringGrid;
    GradeX: TStringGrid;
    GroupBox1: TGroupBox;
    EdMedidaC: TdmkEdit;
    EdMedidaL: TdmkEdit;
    EdMedidaA: TdmkEdit;
    QrGraGruX: TmySQLQuery;
    DsGraGrux: TDataSource;
    QrGraGruXGraGruC: TIntegerField;
    QrGraGruXGraGru1: TIntegerField;
    QrGraGruXGraTamI: TIntegerField;
    QrGraGruXControle: TIntegerField;
    RGTipDimens: TDBRadioGroup;
    QrGraGru1TipDimens: TSmallintField;
    GroupBox7: TGroupBox;
    Label11: TLabel;
    DBEdit2: TDBEdit;
    Label12: TLabel;
    DBEdit1: TDBEdit;
    Label10: TLabel;
    DBEdit3: TDBEdit;
    Label1: TLabel;
    DBEdit4: TDBEdit;
    GroupBox8: TGroupBox;
    Label14: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    Label18: TLabel;
    EdGraGruX: TdmkEdit;
    EdQuantP: TdmkEdit;
    EdPerCustom: TdmkEdit;
    EdDescoP: TdmkEdit;
    QrGraGru1Medida1: TWideStringField;
    QrGraGru1Medida2: TWideStringField;
    QrGraGru1Medida3: TWideStringField;
    QrGraGru1Medida4: TWideStringField;
    DBText1: TDBText;
    DBText2: TDBText;
    DBText3: TDBText;
    QrGraGru1PerCuztMin: TFloatField;
    QrGraGru1PerCuztMax: TFloatField;
    Panel5: TPanel;
    GroupBox6: TGroupBox;
    Label3: TLabel;
    Label9: TLabel;
    DBEdit5: TDBEdit;
    DBEdit6: TDBEdit;
    DBEdit7: TDBEdit;
    GroupBox9: TGroupBox;
    EdPrecoF: TdmkEdit;
    dmkEdit3: TdmkEdit;
    Label8: TLabel;
    EdValLiq: TdmkEdit;
    Label7: TLabel;
    Label6: TLabel;
    DBText4: TDBText;
    EdMedidaE: TdmkEdit;
    Label13: TLabel;
    EdQuantX: TdmkEdit;
    Label17: TLabel;
    EdPrecoO: TdmkEdit;
    CkContinuar: TCheckBox;
    QrGraGruXPrdGrupTip: TIntegerField;
    QrNeed: TmySQLQuery;
    QrGraGru1PrdGrupTip: TIntegerField;
    GBAvisos1: TGroupBox;
    Panel7: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel8: TPanel;
    PnSaiDesis: TPanel;
    BtOK: TBitBtn;
    BtSaida: TBitBtn;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrGraGru1BeforeClose(DataSet: TDataSet);
    procedure GradeCClick(Sender: TObject);
    procedure GradeCDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect;
      State: TGridDrawState);
    procedure EdPesqGraGru1Exit(Sender: TObject);
    procedure RGGrupTipClick(Sender: TObject);
    procedure EdGraGru1Change(Sender: TObject);
    procedure EdGgxItemChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure QrGraGru1AfterOpen(DataSet: TDataSet);
    procedure EdMatPartCadChange(Sender: TObject);
    procedure EdMatPartCadEnter(Sender: TObject);
    procedure EdMatPartCadExit(Sender: TObject);
    procedure RGTipDimensChange(Sender: TObject);
    procedure DBEdit5Change(Sender: TObject);
    procedure DBEdit6Change(Sender: TObject);
    procedure DBEdit7Change(Sender: TObject);
    procedure EdGraGruXChange(Sender: TObject);
    procedure EdPrecoOChange(Sender: TObject);
    procedure EdQuantPChange(Sender: TObject);
    procedure EdPerCustomChange(Sender: TObject);
    procedure EdDescoPChange(Sender: TObject);
    procedure EdMedidaCChange(Sender: TObject);
    procedure EdMedidaLChange(Sender: TObject);
    procedure EdMedidaAChange(Sender: TObject);
    procedure EdMedidaEChange(Sender: TObject);
    procedure EdValLiqChange(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure EdPerCustomEnter(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure EdMatPartCadKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure QrGraGru1AfterScroll(DataSet: TDataSet);
  private
    { Private declarations }
    procedure ReopenGraGru1(Nivel1: Integer);
    procedure HabilitaOK();
    procedure CalculaCusto();
    procedure CalculaVars(
              var GraGruX: Integer;
              var PrecoO, QuantP, DescoP, MedidaC, MedidaL, MedidaA, MedidaE,
              QuantX, ValTot, PerCustom, DescoI, PrecoR, PrecoF, DescoV,
              ValBru, ValLiq, Fator: Double);
  public
    { Public declarations }
    //FGraGruX,
    FLimitaCorTam: Boolean;
    FControle, FXsel: Integer;
    procedure ReopenGraGruX(GraGruX: Integer);
  end;

  var
  FmPediVdaCuzParIns1: TFmPediVdaCuzParIns1;

implementation

uses ModProd, UnMyObjects, UMySQLModule, PediVda1, Module, dmkGeral;

{$R *.DFM}

procedure TFmPediVdaCuzParIns1.BtOKClick(Sender: TObject);
var
  MatPartCad,
  GraGruX, Conta, TipDimens: Integer;
  PrecoO, QuantP, DescoP, MedidaC, MedidaL, MedidaA, MedidaE,
  QuantX, ValTot, PerCustom, DescoI, PrecoR, PrecoF, DescoV,
  ValBru, ValLiq, Fator: Double;
begin
  if not UMyMod.ObtemCodigoDeCodUsu(EdMatPartCad, MatPartCad,
    'Informe a parte!') then Exit;
  CalculaVars(
    GraGruX,
    PrecoO, QuantP, DescoP, MedidaC, MedidaL, MedidaA, MedidaE, QuantX, ValTot,
    PerCustom, DescoI, PrecoR, PrecoF, DescoV, ValBru, ValLiq, Fator);
  //
  TipDimens := RGTipDimens.ItemIndex;
  Conta := UMyMod.BuscaEmLivreY_Def('pedivdacuz', 'Conta', ImgTipo.SQLType, 0);
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'pedivdacuz', False, [
    'Controle', 'MatPartCad', 'GraGruX',
    'MedidaC', 'MedidaL', 'MedidaA',
    'MedidaE', 'QuantP', 'QuantX',
    'PrecoO', 'PrecoR', 'PrecoF',
    'ValBru', 'DescoP', 'DescoV',
    'PerCustom', 'ValLiq', 'TipDimens'
  ], ['Conta'], [
    FControle, MatPartCad, GraGruX,
    MedidaC, MedidaL, MedidaA,
    MedidaE, QuantP, QuantX,
    PrecoO, PrecoR, PrecoF,
    ValBru, DescoP, DescoV,
    PerCustom, ValLiq, TipDimens
  ], [Conta], True) then
  begin
    FmPediVda1.AtualizaItemCustomizado(FControle);
    Close;
  end;
end;

procedure TFmPediVdaCuzParIns1.BtSaidaClick(Sender: TObject);
begin
  CkContinuar.Checked := False;
  Close;
end;

procedure TFmPediVdaCuzParIns1.CalculaVars(
  var GraGruX: Integer;
  var PrecoO, QuantP, DescoP, MedidaC, MedidaL, MedidaA, MedidaE, QuantX, ValTot,
  PerCustom, DescoI, PrecoR, PrecoF, DescoV, ValBru, ValLiq, Fator: Double);
var
  Casas: Integer;
begin
  //Codigo     := FmPediVda1.QrPediVdaCodigo.Value;
  GraGruX    := EdGraGruX.ValueVariant;
  PrecoO     := EdPrecoO.ValueVariant;
  QuantP     := EdQuantP.ValueVariant;
  DescoP     := EdDescoP.ValueVariant;
  MedidaC    := EdMedidaC.ValueVariant;
  MedidaL    := EdMedidaL.ValueVariant;
  MedidaA    := EdMedidaA.ValueVariant;
  MedidaE    := EdMedidaE.ValueVariant;
  PerCustom := EdPerCustom.ValueVariant;
  Fator      := (PerCustom / 100) + 1;
  //
  if (RGTipDimens.ItemIndex < 0) and (MedidaC = 0) then MedidaC := 1;
  if (RGTipDimens.ItemIndex < 1) and (MedidaL = 0) then MedidaL := 1;
  if (RGTipDimens.ItemIndex < 2) and (MedidaA = 0) then MedidaA := 1;
  if (RGTipDimens.ItemIndex < 3) and (MedidaE = 0) then MedidaE := 1;
  //
  QuantX := MedidaC * MedidaL * MedidaA * MedidaE;
  PrecoR := QuantX * PrecoO * Fator;
  //
  Casas  := Dmod.QrControleCasasProd.Value;
  DescoI := dmkPF.FFF(PrecoR * DescoP / 100, Casas, siPositivo);
  PrecoF := PrecoR - DescoI;
  DescoV := DescoI * QuantP;
  ValBru := PrecoR * QuantP;
  ValLiq := ValBru - DescoV;
  ValTot := ValLiq + 0;// Parei aqui ver total das soma dos itens!
end;

procedure TFmPediVdaCuzParIns1.CalculaCusto();
var
  GraGruX: Integer;
  PrecoO, QuantP, DescoP, MedidaC, MedidaL, MedidaA, MedidaE, QuantX, ValTot,
  PerCustom, DescoI, PrecoR, PrecoF, DescoV, ValBru, ValLiq, Fator: Double;
begin
  CalculaVars(GraGruX,
    PrecoO, QuantP, DescoP, MedidaC, MedidaL, MedidaA, MedidaE, QuantX, ValTot,
    PerCustom, DescoI, PrecoR, PrecoF, DescoV, ValBru, ValLiq, Fator);
  EdQuantX.ValueVariant := QuantX;
  EdPrecoF.ValueVariant := PrecoF;
  EdValLiq.ValueVariant := ValLiq;

end;

procedure TFmPediVdaCuzParIns1.DBEdit5Change(Sender: TObject);
begin
  EdPerCustom.ValMin := DBEdit5.Text;
end;

procedure TFmPediVdaCuzParIns1.DBEdit6Change(Sender: TObject);
begin
  EdPerCustom.ValMax := DBEdit6.Text;
end;

procedure TFmPediVdaCuzParIns1.DBEdit7Change(Sender: TObject);
begin
  EdDescoP.ValMax := DBEdit7.Text;
end;

procedure TFmPediVdaCuzParIns1.EdDescoPChange(Sender: TObject);
begin
  CalculaCusto();
end;

procedure TFmPediVdaCuzParIns1.EdGgxItemChange(Sender: TObject);
begin
  HabilitaOK();
end;

procedure TFmPediVdaCuzParIns1.EdGraGru1Change(Sender: TObject);
begin
  if EdGraGru1.ValueVariant > 0 then
    DmProd.ConfigGrades0(QrGraGru1GraTamCad.Value, QrGraGru1Nivel1.Value,
    GradeA, GradeX, GradeC)
  else MyObjects.LimpaGrades([GradeA, GradeC, GradeX], 0, 0, True);

end;

procedure TFmPediVdaCuzParIns1.EdGraGruXChange(Sender: TObject);
var
  GraGruX: Integer;
begin
  HabilitaOK();
  GraGruX := Geral.IMV(EdGraGruX.Text);
  EdPrecoO.ValueVariant := DmProd.ObtemPrecoGraGruX_GraCusPrc(
    GraGruX, FmPediVda1.QrPediVdaTabelaPrc.Value);
end;

procedure TFmPediVdaCuzParIns1.EdMatPartCadChange(Sender: TObject);
begin
  if not EdMatPartCad.Focused then
    ReopenGraGru1(0);
end;

procedure TFmPediVdaCuzParIns1.EdMatPartCadEnter(Sender: TObject);
begin
  FXsel := EdMatPartCad.ValueVariant;
end;

procedure TFmPediVdaCuzParIns1.EdMatPartCadExit(Sender: TObject);
begin
  if FXsel <> EdMatPartCad.ValueVariant then
    ReopenGraGru1(0);
end;

procedure TFmPediVdaCuzParIns1.EdMatPartCadKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  if Key = VK_ESCAPE then Close;
end;

procedure TFmPediVdaCuzParIns1.EdMedidaAChange(Sender: TObject);
begin
  CalculaCusto();
end;

procedure TFmPediVdaCuzParIns1.EdMedidaCChange(Sender: TObject);
begin
  CalculaCusto();
end;

procedure TFmPediVdaCuzParIns1.EdMedidaEChange(Sender: TObject);
begin
  CalculaCusto();
end;

procedure TFmPediVdaCuzParIns1.EdMedidaLChange(Sender: TObject);
begin
  CalculaCusto();
end;

procedure TFmPediVdaCuzParIns1.EdPerCustomChange(Sender: TObject);
begin
  CalculaCusto();
end;

procedure TFmPediVdaCuzParIns1.EdPerCustomEnter(Sender: TObject);
begin
  //ShowMessage('M�nimo: ' + EdPerCustom.ValMin);
  //ShowMessage('M�ximo: ' + EdPerCustom.ValMax);
end;

procedure TFmPediVdaCuzParIns1.EdPesqGraGru1Exit(Sender: TObject);
begin
  ReopenGraGru1(0);
  EdGraGru1.SetFocus;
end;

procedure TFmPediVdaCuzParIns1.EdPrecoOChange(Sender: TObject);
begin
  CalculaCusto();
end;

procedure TFmPediVdaCuzParIns1.EdQuantPChange(Sender: TObject);
begin
  CalculaCusto();
end;

procedure TFmPediVdaCuzParIns1.EdValLiqChange(Sender: TObject);
begin
  HabilitaOK();
end;

procedure TFmPediVdaCuzParIns1.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //ReopenGraGruX(FGraGruX);
  if EdGraGruX.ValueVariant <> 0 then
    EdPerCustom.SetFocus;
end;

procedure TFmPediVdaCuzParIns1.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  if not Pndados.Visible then
    CkContinuar.Checked := False;
end;

procedure TFmPediVdaCuzParIns1.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  UnDmkDAC_PF.AbreQuery(QrMatPartCad, Dmod.MyDB);
  QrGraGruX.Close;
end;

procedure TFmPediVdaCuzParIns1.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmPediVdaCuzParIns1.GradeCClick(Sender: TObject);
var
  PermitePgt, PermiteGg1, PermiteGgc, PermiteGti, PermiteGgx,
  AbrangePgt, AbrangeGg1, AbrangeGgc, AbrangeGti, AbrangeGgx: Integer;
begin
  if (GradeC.Col > 0) and (GradeC.Row > 0) then
  begin
    //
    AbrangePgt := DmProd.QrNeed1PrdGrupTip.Value;
    AbrangeGg1 := DmProd.QrNeed1GraGru1.Value;
    AbrangeGgc := DmProd.QrNeed1GraGruC.Value;
    AbrangeGti := DmProd.QrNeed1GraTamI.Value;
    AbrangeGgx := DmProd.QrNeed1Controle.Value;
    //
    PermitePgt := QrGraGru1PrdGrupTip.Value;
    PermiteGg1 := QrGraGru1Nivel1.Value;
    PermiteGgc := Geral.IMV(GradeX.Cells[0, GradeC.Row]);
    PermiteGti := Geral.IMV(GradeX.Cells[GradeC.Col, 0]);
    PermiteGgx := Geral.IMV(GradeC.Cells[GradeC.Col, GradeC.Row]);
    //
    QrNeed.Close;
    QrNeed.SQL.Clear;
    QrNeed.SQL.Add('SELECT *');
    QrNeed.SQL.Add('FROM gramatits');
    QrNeed.SQL.Add('WHERE');
    QrNeed.SQL.Add('(');
    QrNeed.SQL.Add('  ((NivAbrange=5) AND (AbrangePgt='+ FormatFloat('0', AbrangePgt) + '))');
    QrNeed.SQL.Add('  OR');
    QrNeed.SQL.Add('  ((NivAbrange=4) AND (AbrangeGg1='+ FormatFloat('0', AbrangeGg1) + '))');
    QrNeed.SQL.Add('  OR');
    QrNeed.SQL.Add('  ((NivAbrange=3) AND (AbrangeGgc='+ FormatFloat('0', AbrangeGgc) + '))');
    QrNeed.SQL.Add('  OR');
    QrNeed.SQL.Add('  ((NivAbrange=2) AND (AbrangeGti='+ FormatFloat('0', AbrangeGti) + '))');
    QrNeed.SQL.Add('  OR');
    QrNeed.SQL.Add('  ((NivAbrange=1) AND (AbrangeGgx='+ FormatFloat('0', AbrangeGgx) + '))');
    QrNeed.SQL.Add(')');
    QrNeed.SQL.Add('AND');
    QrNeed.SQL.Add('(');
    QrNeed.SQL.Add('  ((NivPermite=5) AND (PermitePgt='+ FormatFloat('0', PermitePgt) + '))');
    QrNeed.SQL.Add('  OR');
    QrNeed.SQL.Add('  ((NivPermite=4) AND (PermiteGg1='+ FormatFloat('0', PermiteGg1) + '))');
    QrNeed.SQL.Add('  OR');
    QrNeed.SQL.Add('  ((NivPermite=3) AND (PermiteGgc='+ FormatFloat('0', PermiteGgc) + '))');
    QrNeed.SQL.Add('  OR');
    QrNeed.SQL.Add('  ((NivPermite=2) AND (PermiteGti='+ FormatFloat('0', PermiteGti) + '))');
    QrNeed.SQL.Add('  OR');
    QrNeed.SQL.Add('  ((NivPermite=1) AND (PermiteGgx='+ FormatFloat('0', PermiteGgx) + '))');
    QrNeed.SQL.Add(')');
    UnDmkDAC_PF.AbreQuery(QrNeed, Dmod.MyDB);
    //
    if QrNeed.RecordCount > 0 then
      EdGraGruX.ValueVariant := PermiteGgx
    else
    begin
      Geral.MensagemBox('O reduzido selecionado (' + IntToStr(PermiteGgx) +
      ') n�o � permitido para o reduzido a ser customizado!', 'Aviso',
      MB_OK+ MB_ICONWARNING);
      EdGraGruX.Text := '0';
    end;
    //
    EdQuantP.SetFocus;
  end;
end;

procedure TFmPediVdaCuzParIns1.GradeCDrawCell(Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);
begin
  MyObjects.DesenhaGradeN(GradeC, GradeA, nil, ACol, ARow, Rect, State,
  '0', 0, 0, False);
end;

procedure TFmPediVdaCuzParIns1.HabilitaOK();
var
  Produto, Reduzido: Integer;
  ValLiq: Double;
begin
  Produto  := Geral.IMV(EdGraGru1.Text);
  Reduzido := Geral.IMV(EdGraGruX.Text);
  ValLiq   := Geral.DMV(EdValLiq.Text);
  BtOK.Enabled := (Produto > 0) and (Reduzido > 0) and (ValLiq > 0);
end;

procedure TFmPediVdaCuzParIns1.QrGraGru1AfterOpen(DataSet: TDataSet);
begin
  PnDados.Visible := QrGraGru1.RecordCount > 0;
  EdGraGru1.ValueVariant := 0;
  CBGraGru1.KeyValue := Null;
end;

procedure TFmPediVdaCuzParIns1.QrGraGru1AfterScroll(DataSet: TDataSet);
begin
  EdMedidaL.Visible := QrGraGru1Medida2.Value <> '';
  EdMedidaA.Visible := QrGraGru1Medida3.Value <> '';
  EdMedidaE.Visible := QrGraGru1Medida4.Value <> '';
end;

procedure TFmPediVdaCuzParIns1.QrGraGru1BeforeClose(DataSet: TDataSet);
begin
  MyObjects.LimpaGrades([GradeA, GradeC, GradeX], 0, 0, True);
  PnDados.Visible := False;
end;

procedure TFmPediVdaCuzParIns1.ReopenGraGru1(Nivel1: Integer);
  function CancelaZero(Campo: String): String;
  begin
    if Campo = '0' then
      Result := '-1000'
    else
      Result := Campo;
  end;
var
  Grupo, GamaT, Grade, Reduz, PrdTi: String;
  Parte: Integer;
begin
  Screen.Cursor := crHourGlass;
  try
    QrGraGru1.Close;
    if RGGrupTip.ItemIndex = 0 then Exit;
    if not UmyMod.ObtemCodigoDeCodUsu(EdMatPartCad, Parte,
      'Informe a parte do produto!') then Exit;
    PrdTi := CancelaZero(FormatFloat('0', QrGraGruXPrdGrupTip.Value));
    Grupo := CancelaZero(FormatFloat('0', QrGraGruXGraGru1.Value));
    GamaT := CancelaZero(FormatFloat('0', QrGraGruXGraGruC.Value));
    Grade := CancelaZero(FormatFloat('0', QrGraGruXGraTamI.Value));
    Reduz := CancelaZero(FormatFloat('0', QrGraGruXControle.Value));
    //
    QrGraGru1.SQL.Clear;
    QrGraGru1.SQL.Add('SELECT DISTINCT gg1.Nivel1, gg1.CodUsu, gg1.Nome, ');
    QrGraGru1.SQL.Add('gg1.GraTamCad,gg1.TipDimens, gg1.PerCuztMin, ');
    QrGraGru1.SQL.Add('gg1.PerCuztMax,mor.Medida1, mor.Medida2, mor.Medida3, ');
    QrGraGru1.SQL.Add('mor.Medida4, gg1.PrdGrupTip');
    QrGraGru1.SQL.Add('FROM gragru1 gg1');
    QrGraGru1.SQL.Add('LEFT JOIN prdgruptip pgt ON pgt.Codigo=gg1.PrdGrupTip');
    QrGraGru1.SQL.Add('LEFT JOIN medordem mor ON mor.Codigo=gg1.MedOrdem ');

    // C�digos de Nivel1, Grade, Gama e reduzido
    // vai gerar erro caso n�o haja nenhum
    QrGraGru1.SQL.Add('LEFT JOIN gramatits gmi ON gmi.PermitePgt=gg1.PrdGrupTip AND gmi.MatPartCad=' + FormatFloat('0', Parte));
    QrGraGru1.SQL.Add('WHERE (gmi.NivAbrange=5 AND gmi.PermitePgt=' + PrdTi + ')');
    QrGraGru1.SQL.Add('OR gg1.Nivel1 IN (');

    //QrGraGru1.SQL.Add('WHERE gg1.Nivel1 IN (');

    QrGraGru1.SQL.Add('     SELECT PermiteGg1 ');
    QrGraGru1.SQL.Add('     FROM gramatits ');
    QrGraGru1.SQL.Add('     WHERE MatPartCad=' + FormatFloat('0', Parte));
    QrGraGru1.SQL.Add('     AND ');
    QrGraGru1.SQL.Add('     ( ');
    QrGraGru1.SQL.Add('          (AbrangePgt=' + PrdTi + ') OR ');
    QrGraGru1.SQL.Add('          (AbrangeGg1=' + Grupo + ') OR ');
    QrGraGru1.SQL.Add('          (AbrangeGti=' + Grade + ') OR ');
    QrGraGru1.SQL.Add('          (AbrangeGgc=' + GamaT + ') OR ');
    QrGraGru1.SQL.Add('          (AbrangeGgx=' + Reduz + ') ');
    QrGraGru1.SQL.Add('     ) ');
    QrGraGru1.SQL.Add(') ');

    // Tipo de grupo de produto
    case RGGrupTip.ItemIndex of
      3: QrGraGru1.SQL.Add('AND pgt.TipPrd in (1,2)');
      else QrGraGru1.SQL.Add('AND pgt.TipPrd=' + FormatFloat('0', RGGrupTip.ItemIndex));
    end;

    // Filtro por nome do grupo
    QrGraGru1.SQL.Add('AND gg1.Nome LIKE "%' + EdPesqGraGru1.Text + '%"');

    QrGraGru1.SQL.Add('ORDER BY gg1.Nome');
    UMyMod.AbreQuery(QrGraGru1, Dmod.MyDB, 'TFmPediVdaCuzParIns.ReopenGraGru1()');

    if Nivel1 <> 0 then
      QrGraGru1.Locate('Nivel1', Nivel1, []);
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmPediVdaCuzParIns1.ReopenGraGruX(GraGruX: Integer);
begin
  if QrGraGruX.State = dsInactive then
  begin
    QrGraGruX.Close;
    QrGraGruX.Params[0].AsInteger := GraGruX;
    UnDmkDAC_PF.AbreQuery(QrGraGruX, Dmod.MyDB);
  end;
end;

procedure TFmPediVdaCuzParIns1.RGGrupTipClick(Sender: TObject);
begin
  ReopenGraGru1(0);
end;

procedure TFmPediVdaCuzParIns1.RGTipDimensChange(Sender: TObject);
begin
  EdMedidaC.Enabled := RGTipDimens.ItemIndex >= 0;
  EdMedidaL.Enabled := RGTipDimens.ItemIndex >= 1;
  EdMedidaA.Enabled := RGTipDimens.ItemIndex >= 2;
  EdMedidaE.Enabled := RGTipDimens.ItemIndex >= 3;
  //
  if EdMedidaC.Enabled then EdMedidaC.ValueVariant := 0;
  if EdMedidaL.Enabled then EdMedidaL.ValueVariant := 0;
  if EdMedidaA.Enabled then EdMedidaA.ValueVariant := 0;
  if EdMedidaE.Enabled then EdMedidaE.ValueVariant := 0;
end;

end.
