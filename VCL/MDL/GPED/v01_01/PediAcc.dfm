object FmPediAcc: TFmPediAcc
  Left = 368
  Top = 194
  Caption = 'PED-AGENT-001 :: Cadastro de Agentes Comerciais'
  ClientHeight = 611
  ClientWidth = 1008
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelEdita: TPanel
    Left = 0
    Top = 96
    Width = 1008
    Height = 515
    Align = alClient
    Color = clBtnShadow
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
    Visible = False
    object PainelConfirma: TPanel
      Left = 1
      Top = 466
      Width = 1006
      Height = 48
      Align = alBottom
      TabOrder = 0
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 8
        Top = 3
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object Panel1: TPanel
        Left = 897
        Top = 1
        Width = 108
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 7
          Top = 2
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
    end
    object Panel6: TPanel
      Left = 1
      Top = 1
      Width = 1006
      Height = 48
      Align = alTop
      Enabled = False
      TabOrder = 1
      object Label4: TLabel
        Left = 4
        Top = 4
        Width = 45
        Height = 13
        Caption = 'Entidade:'
      end
      object Label5: TLabel
        Left = 64
        Top = 4
        Width = 31
        Height = 13
        Caption = 'Nome:'
        FocusControl = DBEdit04
      end
      object DBEdit04: TDBEdit
        Left = 64
        Top = 20
        Width = 349
        Height = 21
        Hint = 'Nome do banco'
        Color = clWhite
        DataField = 'NOMEACC'
        DataSource = DsPediAcc
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
      end
      object EdCodigo: TdmkEdit
        Left = 4
        Top = 20
        Width = 56
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 1
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Codigo'
        UpdCampo = 'Codigo'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
    end
    object Panel7: TPanel
      Left = 1
      Top = 49
      Width = 1006
      Height = 48
      Align = alTop
      TabOrder = 2
      object Label7: TLabel
        Left = 12
        Top = 4
        Width = 53
        Height = 13
        Caption = 'Supervisor:'
      end
      object SbSupervisor: TSpeedButton
        Left = 568
        Top = 20
        Width = 21
        Height = 21
        Caption = '...'
        OnClick = SbSupervisorClick
      end
      object EdSupervisor: TdmkEditCB
        Left = 12
        Top = 20
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Supervisor'
        UpdCampo = 'Supervisor'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBSupervisor
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBSupervisor: TdmkDBLookupComboBox
        Left = 72
        Top = 20
        Width = 492
        Height = 21
        KeyField = 'Codigo'
        ListField = 'NOMEENT'
        ListSource = DsSupervisores
        TabOrder = 1
        dmkEditCB = EdSupervisor
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
    end
    object Panel4: TPanel
      Left = 1
      Top = 97
      Width = 1006
      Height = 244
      Align = alTop
      TabOrder = 3
      object GroupBox1: TGroupBox
        Left = 1
        Top = 1
        Width = 1004
        Height = 242
        Align = alClient
        Caption = 
          ' Pagamento de comiss'#245'es: (os percentuais de comiss'#245'es informados' +
          ' aqui sobrep'#245'e as informa'#231#245'es do produto)'
        TabOrder = 0
        object Panel8: TPanel
          Left = 2
          Top = 15
          Width = 1000
          Height = 46
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 0
          object Label8: TLabel
            Left = 4
            Top = 4
            Width = 108
            Height = 13
            Caption = 'Carteira: (do tipo caixa)'
          end
          object SbCartFin: TSpeedButton
            Left = 380
            Top = 20
            Width = 21
            Height = 21
            Caption = '...'
            OnClick = SbCartFinClick
          end
          object Label9: TLabel
            Left = 404
            Top = 4
            Width = 133
            Height = 13
            Caption = 'Conta (do Plano de Contas):'
          end
          object SBContaFin: TSpeedButton
            Left = 780
            Top = 20
            Width = 21
            Height = 21
            Caption = '...'
            OnClick = SBContaFinClick
          end
          object Label2: TLabel
            Left = 804
            Top = 4
            Width = 85
            Height = 13
            Caption = '% comiss'#227'o fatur.:'
          end
          object Label11: TLabel
            Left = 900
            Top = 4
            Width = 91
            Height = 13
            Caption = '% comiss'#227'o receb.:'
          end
          object EdCartFin: TdmkEditCB
            Left = 4
            Top = 20
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'CartFin'
            UpdCampo = 'CartFin'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            DBLookupComboBox = CBCartFin
            IgnoraDBLookupComboBox = False
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
          object CBCartFin: TdmkDBLookupComboBox
            Left = 60
            Top = 20
            Width = 320
            Height = 21
            KeyField = 'Codigo'
            ListField = 'Nome'
            ListSource = DsCarteiras
            TabOrder = 1
            dmkEditCB = EdCartFin
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
          object EdContaFin: TdmkEditCB
            Left = 404
            Top = 20
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 2
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'ContaFin'
            UpdCampo = 'ContaFin'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            DBLookupComboBox = CBContaFin
            IgnoraDBLookupComboBox = False
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
          object CBContaFin: TdmkDBLookupComboBox
            Left = 460
            Top = 20
            Width = 320
            Height = 21
            KeyField = 'Codigo'
            ListField = 'Nome'
            ListSource = DsContas
            TabOrder = 3
            dmkEditCB = EdContaFin
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
          object EdPerComissR: TdmkEdit
            Left = 900
            Top = 20
            Width = 92
            Height = 21
            Alignment = taRightJustify
            TabOrder = 5
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 6
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '0'
            ValMax = '100'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,000000'
            QryCampo = 'PerComissR'
            UpdCampo = 'PerComissR'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
          object EdPerComissF: TdmkEdit
            Left = 804
            Top = 20
            Width = 92
            Height = 21
            Alignment = taRightJustify
            TabOrder = 4
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 6
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '0'
            ValMax = '100'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,000000'
            QryCampo = 'PerComissF'
            UpdCampo = 'PerComissF'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
        end
        object RGTpEmiCoRec: TdmkRadioGroup
          Left = 2
          Top = 61
          Width = 1000
          Height = 45
          Align = alTop
          Caption = ' Pagamento da comiss'#227'o no recebimento do valor do cliente: '
          Columns = 3
          ItemIndex = 0
          Items.Strings = (
            'N'#227'o definido'
            'Quando receber tudo'
            'Proporcional a cada quita'#231#227'o')
          TabOrder = 1
          QryCampo = 'TpEmiCoRec'
          UpdCampo = 'TpEmiCoRec'
          UpdType = utYes
          OldValor = 0
        end
      end
    end
    object GBRodaPe: TGroupBox
      Left = 1
      Top = 402
      Width = 1006
      Height = 64
      Align = alBottom
      TabOrder = 4
      object Panel12: TPanel
        Left = 2
        Top = 15
        Width = 1002
        Height = 47
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object PnSaiDesis: TPanel
          Left = 858
          Top = 0
          Width = 144
          Height = 47
          Align = alRight
          BevelOuter = bvNone
          TabOrder = 0
        end
      end
    end
  end
  object PainelDados: TPanel
    Left = 0
    Top = 96
    Width = 1008
    Height = 515
    Align = alClient
    Color = clAppWorkSpace
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    object PnCabeca: TPanel
      Left = 1
      Top = 1
      Width = 1006
      Height = 48
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 1
      object Label1: TLabel
        Left = 8
        Top = 4
        Width = 85
        Height = 13
        Caption = 'Agente comercial:'
        FocusControl = DBEdCodigo
      end
      object Label3: TLabel
        Left = 588
        Top = 4
        Width = 104
        Height = 13
        Caption = 'Supervisor do agente:'
        FocusControl = DBEdit02
      end
      object DBEdCodigo: TDBEdit
        Left = 8
        Top = 20
        Width = 56
        Height = 21
        Hint = 'N'#186' do banco'
        TabStop = False
        DataField = 'Codigo'
        DataSource = DsPediAcc
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 0
      end
      object DBEdNome: TDBEdit
        Left = 64
        Top = 20
        Width = 520
        Height = 21
        Hint = 'Nome do banco'
        Color = clWhite
        DataField = 'NOMEACC'
        DataSource = DsPediAcc
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
      end
      object DBEdit02: TDBEdit
        Left = 588
        Top = 20
        Width = 56
        Height = 21
        Hint = 'N'#186' do banco'
        TabStop = False
        DataField = 'Supervisor'
        DataSource = DsPediAcc
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 2
      end
      object DBEdit01: TDBEdit
        Left = 644
        Top = 20
        Width = 352
        Height = 21
        Hint = 'Nome do banco'
        Color = clWhite
        DataField = 'NOMESUP'
        DataSource = DsPediAcc
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 3
      end
    end
    object PainelControle: TPanel
      Left = 1
      Top = 466
      Width = 1006
      Height = 48
      Align = alBottom
      TabOrder = 0
      object LaRegistro: TLabel
        Left = 173
        Top = 1
        Width = 26
        Height = 13
        Align = alClient
        Caption = '[N]: 0'
      end
      object Panel5: TPanel
        Left = 1
        Top = 1
        Width = 172
        Height = 46
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object Panel3: TPanel
        Left = 536
        Top = 1
        Width = 469
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object BtAgente: TBitBtn
          Left = 4
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Agente'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtAgenteClick
        end
        object Panel2: TPanel
          Left = 360
          Top = 0
          Width = 109
          Height = 46
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 1
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 90
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
        object BtFiliais: TBitBtn
          Left = 96
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Regi'#245'es'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = BtFiliaisClick
        end
      end
    end
    object dmkDBGrid1: TdmkDBGrid
      Left = 1
      Top = 351
      Width = 1006
      Height = 115
      Align = alBottom
      Columns = <
        item
          Expanded = False
          FieldName = 'CodUsu'
          Title.Caption = 'C'#243'digo'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Nome'
          Title.Caption = 'Regi'#227'o'
          Width = 275
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Codigo'
          Title.Caption = 'ID'
          Width = 56
          Visible = True
        end>
      Color = clWindow
      DataSource = DsPediAccReg
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
      TabOrder = 2
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'CodUsu'
          Title.Caption = 'C'#243'digo'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Nome'
          Title.Caption = 'Regi'#227'o'
          Width = 275
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Codigo'
          Title.Caption = 'ID'
          Width = 56
          Visible = True
        end>
    end
    object GroupBox2: TGroupBox
      Left = 1
      Top = 49
      Width = 1006
      Height = 114
      Align = alTop
      Caption = ' Pagamento de comiss'#245'es: '
      TabOrder = 3
      object Panel9: TPanel
        Left = 2
        Top = 15
        Width = 1002
        Height = 50
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 0
        object Label6: TLabel
          Left = 8
          Top = 4
          Width = 39
          Height = 13
          Caption = 'Carteira:'
        end
        object Label10: TLabel
          Left = 408
          Top = 4
          Width = 133
          Height = 13
          Caption = 'Conta (do Plano de Contas):'
        end
        object Label55: TLabel
          Left = 808
          Top = 4
          Width = 85
          Height = 13
          Caption = '% comiss'#227'o fatur.:'
        end
        object Label56: TLabel
          Left = 904
          Top = 4
          Width = 91
          Height = 13
          Caption = '% comiss'#227'o receb.:'
        end
        object DBEdit1: TDBEdit
          Left = 8
          Top = 20
          Width = 56
          Height = 21
          DataField = 'CartFin'
          DataSource = DsPediAcc
          TabOrder = 0
        end
        object DBEdit2: TDBEdit
          Left = 64
          Top = 20
          Width = 340
          Height = 21
          DataField = 'NO_CART'
          DataSource = DsPediAcc
          TabOrder = 1
        end
        object DBEdit3: TDBEdit
          Left = 408
          Top = 20
          Width = 56
          Height = 21
          DataField = 'ContaFin'
          DataSource = DsPediAcc
          TabOrder = 2
        end
        object DBEdit4: TDBEdit
          Left = 464
          Top = 20
          Width = 340
          Height = 21
          DataField = 'NO_CONTA'
          DataSource = DsPediAcc
          TabOrder = 3
        end
        object DBEdit51: TDBEdit
          Left = 808
          Top = 20
          Width = 92
          Height = 21
          DataField = 'PerComissF'
          DataSource = DsPediAcc
          TabOrder = 4
        end
        object DBEdit52: TDBEdit
          Left = 904
          Top = 20
          Width = 92
          Height = 21
          DataField = 'PerComissR'
          DataSource = DsPediAcc
          TabOrder = 5
        end
      end
      object dmkRadioGroup1: TDBRadioGroup
        Left = 2
        Top = 65
        Width = 1002
        Height = 47
        Align = alClient
        Caption = ' Pagamento da comiss'#227'o no recebimento do valor do cliente: '
        Columns = 3
        DataField = 'TpEmiCoRec'
        DataSource = DsPediAcc
        Items.Strings = (
          'N'#227'o definido'
          'Quando receber tudo'
          'Proporcional a cada quita'#231#227'o')
        TabOrder = 1
        Values.Strings = (
          '0'
          '1'
          '2')
      end
    end
  end
  object Panel10: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 12
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 216
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 216
      Top = 0
      Width = 744
      Height = 52
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 400
        Height = 32
        Caption = 'Cadastro de Agentes Comerciais'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 400
        Height = 32
        Caption = 'Cadastro de Agentes Comerciais'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 400
        Height = 32
        Caption = 'Cadastro de Agentes Comerciais'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 52
    Width = 1008
    Height = 44
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel11: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object DsPediAcc: TDataSource
    DataSet = QrPediAcc
    Left = 40
    Top = 12
  end
  object QrPediAcc: TMySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrPediAccBeforeOpen
    AfterOpen = QrPediAccAfterOpen
    BeforeClose = QrPediAccBeforeClose
    AfterScroll = QrPediAccAfterScroll
    SQL.Strings = (
      'SELECT pac.Codigo, pac.Supervisor, pac.Ativo,'
      'IF(acc.Tipo=0, acc.RazaoSocial, acc.Nome) NOMEACC,'
      'IF(sup.Tipo=0, sup.RazaoSocial, sup.Nome) NOMESUP,'
      'pac.CartFin, pac.ContaFin, pac.TpEmiCoRec,'
      'crt.Nome NO_CART, cta.Nome NO_CONTA'
      'FROM pediacc pac'
      'LEFT JOIN entidades acc ON acc.Codigo=pac.Codigo'
      'LEFT JOIN entidades sup ON sup.Codigo=pac.Supervisor'
      'LEFT JOIN carteiras crt ON crt.Codigo=pac.CartFin'
      'LEFT JOIN contas    cta ON cta.Codigo=pac.ContaFin'
      '')
    Left = 12
    Top = 12
    object QrPediAccCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'pediacc.Codigo'
      Required = True
    end
    object QrPediAccSupervisor: TIntegerField
      FieldName = 'Supervisor'
      Origin = 'pediacc.Supervisor'
      Required = True
    end
    object QrPediAccAtivo: TSmallintField
      FieldName = 'Ativo'
      Origin = 'pediacc.Ativo'
      Required = True
    end
    object QrPediAccNOMEACC: TWideStringField
      FieldName = 'NOMEACC'
      Size = 100
    end
    object QrPediAccNOMESUP: TWideStringField
      FieldName = 'NOMESUP'
      Size = 100
    end
    object QrPediAccCartFin: TIntegerField
      FieldName = 'CartFin'
      Origin = 'pediacc.CartFin'
    end
    object QrPediAccContaFin: TIntegerField
      FieldName = 'ContaFin'
      Origin = 'pediacc.ContaFin'
    end
    object QrPediAccNO_CART: TWideStringField
      FieldName = 'NO_CART'
      Origin = 'carteiras.Nome'
      Size = 100
    end
    object QrPediAccNO_CONTA: TWideStringField
      FieldName = 'NO_CONTA'
      Origin = 'contas.Nome'
      Size = 50
    end
    object QrPediAccTpEmiCoRec: TIntegerField
      FieldName = 'TpEmiCoRec'
      Origin = 'pediacc.TpEmiCoRec'
    end
    object QrGraGru1PerComissF: TFloatField
      FieldName = 'PerComissF'
      Origin = 'gragru1.PerComissF'
      DisplayFormat = '0.000000;-0.000000; '
    end
    object QrGraGru1PerComissR: TFloatField
      FieldName = 'PerComissR'
      Origin = 'gragru1.PerComissR'
      DisplayFormat = '0.000000;-0.000000; '
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = BtAgente
    Left = 68
    Top = 12
  end
  object PMAgente: TPopupMenu
    OnPopup = PMAgentePopup
    Left = 564
    Top = 380
    object Ativanovoagente1: TMenuItem
      Caption = 'At&iva novo agente'
      OnClick = Ativanovoagente1Click
    end
    object Alteradadosdoagenteatual1: TMenuItem
      Caption = '&Altera dados do agente atual'
      OnClick = Alteradadosdoagenteatual1Click
    end
    object Desativaagenteatual1: TMenuItem
      Caption = '&Desativa agente atual'
      Enabled = False
    end
  end
  object QrPediAccTab: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ppi.Controle, ppi.Dias, '
      'ppi.Percent1, ppi.Percent2,'
      'ppi.Percent1 + ppi.Percent2 PERCENTT'
      'FROM pediprzits ppi'
      'WHERE ppi.Codigo=:P0'
      'ORDER BY Dias')
    Left = 96
    Top = 12
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPediAccTabControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrPediAccTabDias: TIntegerField
      FieldName = 'Dias'
      Required = True
    end
    object QrPediAccTabPercent1: TFloatField
      FieldName = 'Percent1'
      Required = True
      DisplayFormat = '0.00;-0.00; '
    end
    object QrPediAccTabPercent2: TFloatField
      FieldName = 'Percent2'
      Required = True
      DisplayFormat = '0.00;-0.00; '
    end
    object QrPediAccTabPERCENTT: TFloatField
      FieldName = 'PERCENTT'
      Required = True
      DisplayFormat = '0.00;-0.00; '
    end
  end
  object DsPediAccTab: TDataSource
    DataSet = QrPediAccTab
    Left = 124
    Top = 12
  end
  object QrPediAccReg: TMySQLQuery
    Database = Dmod.MyDB
    SortFieldNames = 'CodUsu ASC'
    SQL.Strings = (
      'SELECT reg.CodUsu, Reg.Nome, reg.Codigo '
      'FROM regioes reg'
      'WHERE reg.Entidade=:P0')
    Left = 156
    Top = 12
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPediAccRegCodUsu: TIntegerField
      FieldName = 'CodUsu'
      Required = True
    end
    object QrPediAccRegNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 30
    end
    object QrPediAccRegCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
  end
  object DsPediAccReg: TDataSource
    DataSet = QrPediAccReg
    Left = 184
    Top = 12
  end
  object QrSupervisores: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENT'
      'FROM entidades ent'
      'WHERE ent.Codigo>0'
      'AND Fornece6="V"'
      'ORDER BY NOMEENT')
    Left = 748
    Top = 8
    object QrSupervisoresCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrSupervisoresNOMEENT: TWideStringField
      FieldName = 'NOMEENT'
      Required = True
      Size = 100
    end
  end
  object DsSupervisores: TDataSource
    DataSet = QrSupervisores
    Left = 776
    Top = 8
  end
  object QrContas: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome '
      'FROM contas'
      'WHERE Codigo > 0'
      'ORDER BY Nome')
    Left = 804
    Top = 8
    object QrContasCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrContasNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsContas: TDataSource
    DataSet = QrContas
    Left = 832
    Top = 8
  end
  object DsCarteiras: TDataSource
    DataSet = QrCarteiras
    Left = 888
    Top = 8
  end
  object QrCarteiras: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM carteiras'
      'WHERE Tipo=0'
      'ORDER BY Nome')
    Left = 860
    Top = 8
    object QrCarteirasCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCarteirasNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
end
