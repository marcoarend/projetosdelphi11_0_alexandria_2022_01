unit PediVdaImpPrd2;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DB, DBGrids, (*&&UnDmkABS_PF,*)
  dmkDBGrid, DBCtrls, mySQLDbTables, Menus, ComCtrls, UnDmkProcFunc, DmkDAC_PF;

type
  THackDBGrid = class(TDBGrid);
  TFmPediVdaImpPrd2 = class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    PMPesq: TPopupMenu;
    Ativa1: TMenuItem;
    Desativa1: TMenuItem;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    Panel3: TPanel;
    Label1: TLabel;
    EdPesq: TEdit;
    DBGrid1: TdmkDBGrid;
    Panel4: TPanel;
    DBGrid2: TdmkDBGrid;
    RGSelecao: TRadioGroup;
    BtTodos: TBitBtn;
    BtPesq: TBitBtn;
    BtNenhum: TBitBtn;
    procedure FormActivate(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure DBGrid1CellClick(Column: TColumn);
    procedure FormStartDock(Sender: TObject; var DragObject: TDragDockObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure EdPesqChange(Sender: TObject);
    procedure FormUnDock(Sender: TObject; Client: TControl;
      NewTarget: TWinControl; var Allow: Boolean);
    procedure BtNenhumClick(Sender: TObject);
    procedure BtTodosClick(Sender: TObject);
    procedure BtPesqClick(Sender: TObject);
    procedure Ativa1Click(Sender: TObject);
    procedure Desativa1Click(Sender: TObject);
    procedure PageControl1Change(Sender: TObject);
    procedure FormDeactivate(Sender: TObject);
    procedure RGSelecaoClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    procedure Pesquisar();
    procedure AtivaItens(Status: Integer);
    procedure AtivaSelecionados(Status: Integer);
    procedure HabilitaItemUnico();
  public
    { Public declarations }
    class function CreateDockForm(const aColor: TColor): TCustomForm;
    //class procedure FiltaAtivos();
  end;

var
  FmPediVdaImpPrd2: TFmPediVdaImpPrd2;

implementation

uses UnMyObjects, Module, UMySQLModule, TabePrcCab, PediVdaImp2, ModPediVda, MyGlyfs,
  PediVdaImp;

{$R *.DFM}

///// PROCEDURES E FUNCTIONS DO DOCKFORM

procedure TFmPediVdaImpPrd2.Ativa1Click(Sender: TObject);
begin
  AtivaSelecionados(1);
end;

procedure TFmPediVdaImpPrd2.AtivaItens(Status: Integer);
begin
(*&&
  Screen.Cursor := crHourGlass;
  DmPediVda.QrPVIPrdIts.Close;
  DmPediVda.QrPVIPrdIts.SQL.Clear;
  DmPediVda.QrPVIPrdIts.SQL.Add('UPDATE pviprd SET Ativo=' + dmkPF.FFP(Status, 0) + ';');
  DmPediVda.QrPVIPrdIts.SQL.Add('SELECT * FROM pviprd;');
  DmkABS_PF.AbreQuery(DmPediVda.QrPVIPrdIts);
  //
  HabilitaItemUnico();
  Screen.Cursor := crDefault;
*)
end;

procedure TFmPediVdaImpPrd2.AtivaSelecionados(Status: Integer);
begin
(*&&
  Screen.Cursor := crHourGlass;
  DmPediVda.QrPVIPrdIts.First;
  while not DmPediVda.QrPVIPrdIts.Eof do
  begin
    if DmPediVda.QrPVIPrdItsAtivo.Value <> Status then
    begin
      DmPediVda.QrPVIPrdIts.Edit;
      DmPediVda.QrPVIPrdItsAtivo.Value := Status;
      DmPediVda.QrPVIPrdIts.Post;
    end;
    DmPediVda.QrPVIPrdIts.Next;
  end;
  HabilitaItemUnico();
  Screen.Cursor := crDefault;
*)
end;

procedure TFmPediVdaImpPrd2.BtPesqClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMPesq, BtPesq);
end;

procedure TFmPediVdaImpPrd2.BtNenhumClick(Sender: TObject);
begin
  AtivaItens(0);
end;

procedure TFmPediVdaImpPrd2.BtTodosClick(Sender: TObject);
begin
  AtivaItens(1);
end;

class function TFmPediVdaImpPrd2.CreateDockForm(const aColor: TColor): TCustomForm;
begin
  result := TFmPediVdaImpPrd2.Create(Application);
  //result.Color := aColor;
  //result.Caption := ColorToString(aColor);
  FmMyGlyfs.ConfiguraFormDock(result);
  result.Show;
end;

procedure TFmPediVdaImpPrd2.FormClose(Sender: TObject; var Action: TCloseAction);
begin
(*&&
  DmPediVda.QrPVIPrdIts.Close;
  DmPediVda.QrPVIPrdIts.SQL.Clear;
  DmPediVda.QrPVIPrdIts.SQL.Add('DROP TABLE PVIPrd; ');
  DmPediVda.QrPVIPrdIts.ExecSQL;
  //
  ManualFloat(Rect(0, 0, 0, 0));
  Action := caFree;
*)
end;

procedure TFmPediVdaImpPrd2.FormShow(Sender: TObject);
begin
  FmPediVdaImp2.BtConfirma.Enabled := False;
end;

procedure TFmPediVdaImpPrd2.FormStartDock(Sender: TObject;
  var DragObject: TDragDockObject);
begin
  DragObject := TDragDockObjectEx.Create(Self);
  DragObject.Brush.Color := clAqua; // this will display a red outline
end;


procedure TFmPediVdaImpPrd2.FormUnDock(Sender: TObject; Client: TControl;
  NewTarget: TWinControl; var Allow: Boolean);
begin
end;

///// FIM DAS PROCEDURES E FUNCTIONS DO DOCKFORM

procedure TFmPediVdaImpPrd2.PageControl1Change(Sender: TObject);
begin
  BtPesq.Enabled := PageControl1.ActivePageIndex = 0;
  case PageControl1.ActivePageIndex of
    0: Pesquisar();
    1: RGSelecaoClick(Self);
  end;
end;

procedure TFmPediVdaImpPrd2.Pesquisar();
begin
(*&&
  Screen.Cursor := crHourGlass;
  //
  DmPediVda.QrPVIPrdIts.Filter   := 'Nome LIKE "%' + EdPesq.Text + '%"';
  DmPediVda.QrPVIPrdIts.Filtered := True;
  //
  Screen.Cursor := crDefault;
*)
end;

procedure TFmPediVdaImpPrd2.RGSelecaoClick(Sender: TObject);
begin
(*&&
  DmPediVda.QrPVIPrdIts.Filtered := False;
  case RGSelecao.ItemIndex of
    0: DmPediVda.QrPVIPrdIts.Filter := 'Ativo=0';
    1: DmPediVda.QrPVIPrdIts.Filter := 'Ativo=1';
    2: DmPediVda.QrPVIPrdIts.Filter := '';
  end;
  if RGSelecao.ItemIndex < 2 then
    DmPediVda.QrPVIPrdIts.Filtered := True;
*)
end;

procedure TFmPediVdaImpPrd2.DBGrid1CellClick(Column: TColumn);
var
  Status: Integer;
begin
(*&&
  if Column.FieldName = 'Ativo' then
  begin
    if DmPediVda.QrPVIPrdIts.FieldByName('Ativo').Value = 1 then
      Status := 0
    else
      Status := 1;
    //
    DmPediVda.QrPVIPrdIts.Edit;
    DmPediVda.QrPVIPrdIts.FieldByName('Ativo').Value := Status;
    DmPediVda.QrPVIPrdIts.Post;
  end;
  HabilitaItemUnico();
*)
end;

procedure TFmPediVdaImpPrd2.HabilitaItemUnico();
var
  Habilita: Boolean;
begin
(*&&
  DmPediVda.QrPVIPrdAti.Close;
  DmkABS_PF.AbreQuery(DmPediVda.QrPVIPrdAti);
  Habilita :=  DmPediVda.QrPVIPrdAtiItens.Value = 0;
  FmPediVdaImp.LaPVIPrd.Enabled := Habilita;
  FmPediVdaImp.EdPVIPrd.Enabled := Habilita;
  FmPediVdaImp.CBPVIPrd.Enabled := Habilita;
  if not Habilita then
  begin
    FmPediVdaImp.EdPVIPrd.ValueVariant := 0;
    FmPediVdaImp.CBPVIPrd.KeyValue     := 0;
  end;
*)
end;

procedure TFmPediVdaImpPrd2.Desativa1Click(Sender: TObject);
begin
  AtivaSelecionados(0);
end;

procedure TFmPediVdaImpPrd2.EdPesqChange(Sender: TObject);
begin
  Pesquisar();
end;

procedure TFmPediVdaImpPrd2.FormActivate(Sender: TObject);
begin
  //MyObjects.CorIniComponente();
end;

procedure TFmPediVdaImpPrd2.FormCreate(Sender: TObject);
const
  Txt1 = 'INSERT INTO pviprd (Codigo,CodUsu,Nome,Ativo) VALUES(';
var
  Txt2: String;
begin
(*&&
  DmPediVda.QrPVIPrdIts.Close;
  DmPediVda.QrPVIPrdIts.SQL.Clear;
  DmPediVda.QrPVIPrdIts.SQL.Add('DROP TABLE PVIPrd; ');
  DmPediVda.QrPVIPrdIts.SQL.Add('CREATE TABLE PVIPrd (');
  DmPediVda.QrPVIPrdIts.SQL.Add('  Codigo  integer      ,');
  DmPediVda.QrPVIPrdIts.SQL.Add('  CodUsu  integer      ,');
  DmPediVda.QrPVIPrdIts.SQL.Add('  Nome    varchar(50)  ,');
  DmPediVda.QrPVIPrdIts.SQL.Add('  Ativo   smallint      ');
  DmPediVda.QrPVIPrdIts.SQL.Add(');');
  //
  DmPediVda.QrPVIPrdCad.Close;
  UnDmkDAC_PF.AbreQuery(DmPediVda.QrPVIPrdCad, Dmod.MyDB);
  while not DmPediVda.QrPVIPrdCad.Eof do
  begin
    Txt2 :=
      dmkPF.FFP(DmPediVda.QrPVIPrdCadNivel1.Value, 0) + ',' +
      dmkPF.FFP(DmPediVda.QrPVIPrdCadCodUsu.Value, 0) + ',' +
      '"' + dmkPF.DuplicaAspasDuplas(DmPediVda.QrPVIPrdCadNome.Value) + '",' +
      '0);';
  //for i  := 1 to 10 do
    DmPediVda.QrPVIPrdIts.SQL.Add(Txt1 + Txt2);
    DmPediVda.QrPVIPrdCad.Next;
  end;
  //
  DmPediVda.QrPVIPrdIts.SQL.Add('SELECT * FROM pviprd;');
  DmkABS_PF.AbreQuery(DmPediVda.QrPVIPrdIts);
  //
  PageControl1.ActivePageIndex := 0;
*)
end;

procedure TFmPediVdaImpPrd2.FormDeactivate(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 1;
  RGSelecao.ItemIndex := 1;
  //
  if FindWindow('TFmPediVdaImp', nil) > 0 then
    FmPediVdaImp.AtivaBtConfirma();
end;

end.

