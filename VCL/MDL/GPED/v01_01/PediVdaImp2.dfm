object FmPediVdaImp2: TFmPediVdaImp2
  Left = 0
  Top = 0
  Caption = 'PED-IMPRI-000 :: Vendas por Pedido de Venda'
  ClientHeight = 691
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Splitter1: TSplitter
    Left = 985
    Top = 48
    Height = 551
    Align = alRight
    ExplicitLeft = 272
    ExplicitTop = 160
    ExplicitHeight = 100
  end
  object DockTabSet1: TTabSet
    Left = 988
    Top = 48
    Width = 20
    Height = 551
    Align = alRight
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ShrinkToFit = True
    Style = tsModernPopout
    TabPosition = tpLeft
  end
  object pDockLeft: TPanel
    Left = 988
    Top = 48
    Width = 0
    Height = 551
    Align = alRight
    BevelOuter = bvNone
    DockSite = True
    TabOrder = 1
    OnDockDrop = pDockLeftDockDrop
    OnDockOver = pDockLeftDockOver
    OnUnDock = pDockLeftUnDock
  end
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 985
    Height = 551
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 2
    object Panel2: TPanel
      Left = 0
      Top = 0
      Width = 517
      Height = 551
      Align = alLeft
      BevelOuter = bvNone
      TabOrder = 0
      object Panel5: TPanel
        Left = 0
        Top = 0
        Width = 517
        Height = 105
        Align = alTop
        TabOrder = 0
        object LaPVIEmp: TLabel
          Left = 8
          Top = 8
          Width = 45
          Height = 13
          Caption = 'Empresa:'
        end
        object LaPVICli: TLabel
          Left = 8
          Top = 32
          Width = 37
          Height = 13
          Caption = 'Cliente:'
        end
        object LaPVIRep: TLabel
          Left = 8
          Top = 56
          Width = 76
          Height = 13
          Caption = 'Representante:'
        end
        object LaPVIPrd: TLabel
          Left = 8
          Top = 80
          Width = 77
          Height = 13
          Caption = 'Grupo de prod.:'
          Enabled = False
        end
        object EdPVIPrd: TdmkEditCB
          Left = 96
          Top = 76
          Width = 52
          Height = 21
          Alignment = taRightJustify
          Enabled = False
          TabOrder = 6
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          DBLookupComboBox = CBPVIPrd
          IgnoraDBLookupComboBox = False
          AutoSetIfOnlyOneReg = setregOnlyManual
        end
        object EdPVIRep: TdmkEditCB
          Left = 96
          Top = 52
          Width = 52
          Height = 21
          Alignment = taRightJustify
          TabOrder = 4
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'Represen'
          UpdCampo = 'Represen'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          DBLookupComboBox = CBPVIRep
          IgnoraDBLookupComboBox = False
          AutoSetIfOnlyOneReg = setregOnlyManual
        end
        object EdPVICli: TdmkEditCB
          Left = 96
          Top = 28
          Width = 52
          Height = 21
          Alignment = taRightJustify
          TabOrder = 2
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'Cliente'
          UpdCampo = 'Cliente'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          DBLookupComboBox = CBPVICli
          IgnoraDBLookupComboBox = False
          AutoSetIfOnlyOneReg = setregOnlyManual
        end
        object EdPVIEmp: TdmkEditCB
          Left = 96
          Top = 4
          Width = 52
          Height = 21
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          DBLookupComboBox = CBPVIEmp
          IgnoraDBLookupComboBox = False
          AutoSetIfOnlyOneReg = setregOnlyManual
        end
        object CBPVIEmp: TdmkDBLookupComboBox
          Left = 152
          Top = 4
          Width = 356
          Height = 21
          KeyField = 'Filial'
          ListField = 'NOMEENT'
          TabOrder = 1
          dmkEditCB = EdPVIEmp
          UpdType = utYes
          LocF7SQLMasc = '$#'
          LocF7PreDefProc = f7pNone
        end
        object CBPVICli: TdmkDBLookupComboBox
          Left = 152
          Top = 28
          Width = 356
          Height = 21
          KeyField = 'Codigo'
          ListField = 'NOMEENT'
          TabOrder = 3
          dmkEditCB = EdPVICli
          QryCampo = 'Cliente'
          UpdType = utNil
          LocF7SQLMasc = '$#'
          LocF7PreDefProc = f7pNone
        end
        object CBPVIRep: TdmkDBLookupComboBox
          Left = 152
          Top = 52
          Width = 356
          Height = 21
          KeyField = 'Codigo'
          ListField = 'NOMEENT'
          TabOrder = 5
          dmkEditCB = EdPVIRep
          QryCampo = 'Represen'
          UpdType = utNil
          LocF7SQLMasc = '$#'
          LocF7PreDefProc = f7pNone
        end
        object CBPVIPrd: TdmkDBLookupComboBox
          Left = 152
          Top = 76
          Width = 356
          Height = 21
          Enabled = False
          KeyField = 'CodUsu'
          ListField = 'Nome'
          TabOrder = 7
          dmkEditCB = EdPVIPrd
          UpdType = utYes
          LocF7SQLMasc = '$#'
          LocF7PreDefProc = f7pNone
        end
      end
      object Panel6: TPanel
        Left = 0
        Top = 105
        Width = 517
        Height = 144
        Align = alTop
        TabOrder = 1
        object GroupBox1: TGroupBox
          Left = 4
          Top = 4
          Width = 249
          Height = 65
          Caption = ' Per'#237'odo de inclus'#227'o: '
          TabOrder = 0
          object TPIncluIni: TdmkEditDateTimePicker
            Left = 12
            Top = 36
            Width = 112
            Height = 21
            Date = 39892.000000000000000000
            Time = 0.420627939813130100
            TabOrder = 1
            ReadOnly = False
            DefaultEditMask = '!99/99/99;1;_'
            AutoApplyEditMask = True
            UpdType = utYes
            DatePurpose = dmkdpNone
          end
          object TPIncluFim: TdmkEditDateTimePicker
            Left = 128
            Top = 36
            Width = 112
            Height = 21
            Date = 39892.000000000000000000
            Time = 0.420627939813130100
            TabOrder = 3
            ReadOnly = False
            DefaultEditMask = '!99/99/99;1;_'
            AutoApplyEditMask = True
            UpdType = utYes
            DatePurpose = dmkdpNone
          end
          object CkIncluIni: TCheckBox
            Left = 12
            Top = 16
            Width = 77
            Height = 17
            Caption = 'Data inicial:'
            Checked = True
            Enabled = False
            State = cbChecked
            TabOrder = 0
          end
          object CkIncluFim: TCheckBox
            Left = 128
            Top = 16
            Width = 77
            Height = 17
            Caption = 'Data final:'
            Checked = True
            Enabled = False
            State = cbChecked
            TabOrder = 2
          end
        end
        object GroupBox2: TGroupBox
          Left = 256
          Top = 4
          Width = 249
          Height = 65
          Caption = ' Per'#237'odo de emiss'#227'o: '
          TabOrder = 1
          object TPEmissIni: TdmkEditDateTimePicker
            Left = 12
            Top = 36
            Width = 112
            Height = 21
            Date = 39892.000000000000000000
            Time = 0.420627939813130100
            TabOrder = 1
            ReadOnly = False
            DefaultEditMask = '!99/99/99;1;_'
            AutoApplyEditMask = True
            UpdType = utYes
            DatePurpose = dmkdpNone
          end
          object TPEmissFim: TdmkEditDateTimePicker
            Left = 128
            Top = 36
            Width = 112
            Height = 21
            Date = 39892.000000000000000000
            Time = 0.420627939813130100
            TabOrder = 3
            ReadOnly = False
            DefaultEditMask = '!99/99/99;1;_'
            AutoApplyEditMask = True
            UpdType = utYes
            DatePurpose = dmkdpNone
          end
          object CkEmissIni: TCheckBox
            Left = 12
            Top = 16
            Width = 77
            Height = 17
            Caption = 'Data inicial:'
            Checked = True
            Enabled = False
            State = cbChecked
            TabOrder = 0
          end
          object CkEmissFim: TCheckBox
            Left = 128
            Top = 16
            Width = 77
            Height = 17
            Caption = 'Data final:'
            Checked = True
            Enabled = False
            State = cbChecked
            TabOrder = 2
          end
        end
        object GroupBox3: TGroupBox
          Left = 4
          Top = 72
          Width = 249
          Height = 65
          Caption = ' Per'#237'odo de chegada: '
          TabOrder = 2
          object TPEntraIni: TdmkEditDateTimePicker
            Left = 12
            Top = 36
            Width = 112
            Height = 21
            Date = 39892.000000000000000000
            Time = 0.420627939813130100
            TabOrder = 1
            ReadOnly = False
            DefaultEditMask = '!99/99/99;1;_'
            AutoApplyEditMask = True
            UpdType = utYes
            DatePurpose = dmkdpNone
          end
          object TPEntraFim: TdmkEditDateTimePicker
            Left = 128
            Top = 36
            Width = 112
            Height = 21
            Date = 39892.000000000000000000
            Time = 0.420627939813130100
            TabOrder = 3
            ReadOnly = False
            DefaultEditMask = '!99/99/99;1;_'
            AutoApplyEditMask = True
            UpdType = utYes
            DatePurpose = dmkdpNone
          end
          object CkEntraIni: TCheckBox
            Left = 12
            Top = 16
            Width = 77
            Height = 17
            Caption = 'Data inicial:'
            Checked = True
            Enabled = False
            State = cbChecked
            TabOrder = 0
          end
          object CkEntraFim: TCheckBox
            Left = 128
            Top = 16
            Width = 77
            Height = 17
            Caption = 'Data final:'
            Checked = True
            Enabled = False
            State = cbChecked
            TabOrder = 2
          end
        end
        object GroupBox4: TGroupBox
          Left = 256
          Top = 72
          Width = 249
          Height = 65
          Caption = ' Per'#237'odo de previs'#227'o de entrega: '
          TabOrder = 3
          object TPPreviIni: TdmkEditDateTimePicker
            Left = 12
            Top = 36
            Width = 112
            Height = 21
            Date = 39892.000000000000000000
            Time = 0.420627939813130100
            TabOrder = 1
            ReadOnly = False
            DefaultEditMask = '!99/99/99;1;_'
            AutoApplyEditMask = True
            UpdType = utYes
            DatePurpose = dmkdpNone
          end
          object TPPreviFim: TdmkEditDateTimePicker
            Left = 128
            Top = 36
            Width = 112
            Height = 21
            Date = 39892.000000000000000000
            Time = 0.420627939813130100
            TabOrder = 3
            ReadOnly = False
            DefaultEditMask = '!99/99/99;1;_'
            AutoApplyEditMask = True
            UpdType = utYes
            DatePurpose = dmkdpNone
          end
          object CkPreviIni: TCheckBox
            Left = 12
            Top = 16
            Width = 77
            Height = 17
            Caption = 'Data inicial:'
            Checked = True
            Enabled = False
            State = cbChecked
            TabOrder = 0
          end
          object CkPreviFim: TCheckBox
            Left = 128
            Top = 16
            Width = 77
            Height = 17
            Caption = 'Data final:'
            Checked = True
            Enabled = False
            State = cbChecked
            TabOrder = 2
          end
        end
      end
      object Panel7: TPanel
        Left = 0
        Top = 249
        Width = 517
        Height = 148
        Align = alTop
        TabOrder = 2
        object RGTipoSaldo: TRadioGroup
          Left = 4
          Top = 4
          Width = 501
          Height = 41
          Caption = ' Quantidade para ordena'#231#227'o: '
          Columns = 4
          Items.Strings = (
            'Pedida'
            'Pendente'
            'Cancelada'
            'Faturada')
          TabOrder = 0
          OnClick = RGTipoSaldoClick
        end
        object RGOrdemIts: TRadioGroup
          Left = 4
          Top = 48
          Width = 501
          Height = 41
          Caption = ' Ordena'#231#227'o dos itens: '
          Columns = 2
          Items.Strings = (
            'Quantidade'
            'Valor')
          TabOrder = 1
          OnClick = RGOrdemAgr_Click
        end
        object RGAgrupa: TRadioGroup
          Left = 4
          Top = 92
          Width = 501
          Height = 53
          Caption = ' Sintetizar por: '
          Columns = 3
          Items.Strings = (
            'Grupo de produto'
            'Cliente'
            'Representante'
            'Atributo de produto'
            'Produto')
          TabOrder = 2
          OnClick = RGAgrupaClick
        end
      end
      object Panel8: TPanel
        Left = 0
        Top = 397
        Width = 517
        Height = 154
        Align = alClient
        TabOrder = 3
        object RGOrdemAgr_: TRadioGroup
          Left = 1
          Top = 1
          Width = 515
          Height = 56
          Align = alTop
          Caption = ' Ordena'#231#227'o dos agrupamentos: '
          Columns = 2
          Items.Strings = (
            'Nome / descri'#231#227'o'
            'C'#243'digo')
          TabOrder = 0
          Visible = False
          OnClick = RGOrdemAgr_Click
        end
        object Panel10: TPanel
          Left = 1
          Top = 57
          Width = 515
          Height = 28
          Align = alTop
          TabOrder = 1
          object CkAgruPGT: TCheckBox
            Left = 8
            Top = 4
            Width = 209
            Height = 17
            Caption = 'Agrupar pelo Tipo de Produto.'
            TabOrder = 0
          end
        end
      end
    end
    object Panel9: TPanel
      Left = 517
      Top = 0
      Width = 468
      Height = 551
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 1
      object DBGrid2: TDBGrid
        Left = 0
        Top = 402
        Width = 468
        Height = 149
        Align = alBottom
        DataSource = DsPVI
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = []
        Visible = False
        Columns = <
          item
            Expanded = False
            FieldName = 'Tabela'
            Title.Caption = 'Filtro'
            Width = 100
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CodUsu1'
            Title.Caption = 'C'#243'digo'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Nome1'
            Title.Caption = 'Descri'#231#227'o'
            Width = 140
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CodUsu2'
            Title.Caption = 'C'#243'digo'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Nome2'
            Title.Caption = 'Descri'#231#227'o'
            Width = 140
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CodUsu3'
            Title.Caption = 'C'#243'digo'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Nome3'
            Title.Caption = 'Descri'#231#227'o'
            Width = 140
            Visible = True
          end>
      end
      object DBGrid1: TdmkDBGrid
        Left = 0
        Top = 0
        Width = 468
        Height = 402
        Align = alClient
        Columns = <
          item
            Expanded = False
            FieldName = 'CODI_AGRUP'
            Title.Caption = 'Codigo'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NOME_AGRUP'
            Title.Caption = 'Descri'#231#227'o / nome'
            Width = 247
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NO_COR'
            Title.Caption = 'Cor'
            Width = 150
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NO_TAM'
            Title.Caption = 'Tamanho'
            Width = 50
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'QuantP'
            Title.Caption = 'Q.Ped.'
            Width = 36
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'QuantC'
            Title.Caption = 'Q.Can.'
            Width = 36
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'QuantV'
            Title.Caption = 'Q.Fat.'
            Width = 36
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'QuantF'
            Title.Caption = 'Q.Pen.'
            Width = 36
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'ValLiq'
            Width = 72
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'SEQ'
            Width = 36
            Visible = True
          end>
        Color = clWindow
        DataSource = Ds1
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'CODI_AGRUP'
            Title.Caption = 'Codigo'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NOME_AGRUP'
            Title.Caption = 'Descri'#231#227'o / nome'
            Width = 247
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NO_COR'
            Title.Caption = 'Cor'
            Width = 150
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NO_TAM'
            Title.Caption = 'Tamanho'
            Width = 50
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'QuantP'
            Title.Caption = 'Q.Ped.'
            Width = 36
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'QuantC'
            Title.Caption = 'Q.Can.'
            Width = 36
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'QuantV'
            Title.Caption = 'Q.Fat.'
            Width = 36
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'QuantF'
            Title.Caption = 'Q.Pen.'
            Width = 36
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'ValLiq'
            Width = 72
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'SEQ'
            Width = 36
            Visible = True
          end>
      end
    end
  end
  object Panel3: TPanel
    Left = 0
    Top = 643
    Width = 1008
    Height = 48
    Align = alBottom
    TabOrder = 3
    object BtConfirma: TBitBtn
      Tag = 18
      Left = 8
      Top = 4
      Width = 120
      Height = 40
      Caption = '&Pesquisa'
      NumGlyphs = 2
      TabOrder = 0
      OnClick = BtConfirmaClick
    end
    object Panel4: TPanel
      Left = 868
      Top = 1
      Width = 139
      Height = 46
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 3
      object BtDesiste: TBitBtn
        Tag = 13
        Left = 10
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sair'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtDesisteClick
      end
    end
    object BtImprime: TBitBtn
      Tag = 5
      Left = 132
      Top = 4
      Width = 120
      Height = 40
      Caption = '&Imprime'
      Enabled = False
      NumGlyphs = 2
      TabOrder = 1
      OnClick = BtImprimeClick
    end
    object BtLimpa: TBitBtn
      Tag = 68
      Left = 384
      Top = 4
      Width = 120
      Height = 40
      Caption = '&Limpa'
      NumGlyphs = 2
      TabOrder = 2
      OnClick = BtLimpaClick
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 4
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 912
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 350
        Height = 32
        Caption = 'Vendas por Pedido de Venda'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 350
        Height = 32
        Caption = 'Vendas por Pedido de Venda'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 350
        Height = 32
        Caption = 'Vendas por Pedido de Venda'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 599
    Width = 1008
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 5
    object Panel11: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object Qr1: TMySQLQuery
    Database = Dmod.MyDB
    AfterOpen = Qr1AfterOpen
    BeforeClose = Qr1BeforeClose
    OnCalcFields = Qr1CalcFields
    SQL.Strings = (
      'SELECT SUM(pvi.QuantP) QuantP, SUM(pvi.QuantC) QuantC, '
      'SUM(pvi.QuantV) QuantV, '
      'SUM(pvi.QuantP-pvi.QuantC-pvi.QuantV) QuantF, '
      'SUM(pvi.ValLiq) ValLiq, '
      'SUM(pvi.QuantP) QUANTX, '
      'SUM(pvi.ValLiq) VAL_X_, '
      'gg1.Nome NOME_AGRUP, "" NO_TAM, "" NO_COR, gg1.CodUsu CU_COR, '
      'gg1.CodUsu + 0.0 CODI_AGRUP, '
      
        'gg1.CodUsu, gg1.Nome, gg1.Nivel1, gg1.PrdGrupTip, pgt.Nome NO_PG' +
        'T '
      'FROM pedivdaits pvi'
      'LEFT JOIN pedivda   pvd ON pvd.Codigo=pvi.Codigo'
      'LEFT JOIN gragrux   ggx ON ggx.Controle=pvi.GraGruX'
      'LEFT JOIN gratamits gti ON gti.Controle=ggx.GraTamI'
      'LEFT JOIN gragru1   gg1 ON gg1.Nivel1=ggx.GraGru1'
      
        'LEFT JOIN gragruatr gga ON gga.Nivel1=gg1.Nivel1 AND gga.GraAtrC' +
        'ad=0'
      'LEFT JOIN prdgruptip pgt ON pgt.Codigo=gg1.PrdGrupTip'
      'WHERE pvi.QuantP > 0'
      'AND pvd.DtaInclu  BETWEEN "0000-00-00" AND "2019-04-10 23:59:59"'
      'AND pvd.DtaEmiss  BETWEEN "2019-02-01" AND "2019-02-28 23:59:59"'
      'AND pvd.DtaEntra  BETWEEN "0000-00-00" AND "2019-04-10 23:59:59"'
      'AND pvd.DtaPrevi  BETWEEN "0000-00-00" AND "2019-10-07 23:59:59"'
      'AND pvd.Empresa = -14'
      'GROUP BY gg1.Nivel1'
      'ORDER BY NO_PGT, pgt.Codigo, QUANTX DESC, VAL_X_ DESC')
    Left = 556
    Top = 116
    object Qr1QuantP: TFloatField
      FieldName = 'QuantP'
      Origin = 'QuantP'
    end
    object Qr1QuantC: TFloatField
      FieldName = 'QuantC'
      Origin = 'QuantC'
    end
    object Qr1QuantV: TFloatField
      FieldName = 'QuantV'
      Origin = 'QuantV'
    end
    object Qr1QuantF: TFloatField
      FieldName = 'QuantF'
      Origin = 'QuantF'
    end
    object Qr1ValLiq: TFloatField
      FieldName = 'ValLiq'
      Origin = 'ValLiq'
      DisplayFormat = '#,###,##0.00'
    end
    object Qr1CodUsu: TIntegerField
      FieldName = 'CodUsu'
      Origin = 'gragru1.CodUsu'
    end
    object Qr1SEQ: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'SEQ'
      Calculated = True
    end
    object Qr1QUANTX: TFloatField
      FieldName = 'QUANTX'
    end
    object Qr1VAL_X_: TFloatField
      FieldName = 'VAL_X_'
    end
    object Qr1NOME_AGRUP: TWideStringField
      FieldName = 'NOME_AGRUP'
      Size = 100
    end
    object Qr1CODI_AGRUP: TFloatField
      FieldName = 'CODI_AGRUP'
    end
    object Qr1NO_TAM: TWideStringField
      FieldName = 'NO_TAM'
      Size = 5
    end
    object Qr1NO_COR: TWideStringField
      FieldName = 'NO_COR'
      Size = 30
    end
    object Qr1CU_COR: TIntegerField
      FieldName = 'CU_COR'
    end
    object Qr1Nome: TWideStringField
      FieldName = 'Nome'
      Size = 120
    end
    object Qr1Nivel1: TIntegerField
      FieldName = 'Nivel1'
    end
    object Qr1PrdGrupTip: TIntegerField
      FieldName = 'PrdGrupTip'
    end
    object Qr1NO_PGT: TWideStringField
      FieldName = 'NO_PGT'
      Size = 60
    end
  end
  object Ds1: TDataSource
    DataSet = Qr1
    Left = 556
    Top = 164
  end
  object frxDs1: TfrxDBDataset
    UserName = 'frxDs1'
    CloseDataSource = False
    FieldAliases.Strings = (
      'QuantP=QuantP'
      'QuantC=QuantC'
      'QuantV=QuantV'
      'QuantF=QuantF'
      'ValLiq=ValLiq'
      'CodUsu=CodUsu'
      'SEQ=SEQ'
      'QUANTX=QUANTX'
      'VAL_X_=VAL_X_'
      'NOME_AGRUP=NOME_AGRUP'
      'CODI_AGRUP=CODI_AGRUP'
      'NO_TAM=NO_TAM'
      'NO_COR=NO_COR'
      'CU_COR=CU_COR'
      'Nome=Nome'
      'Nivel1=Nivel1'
      'PrdGrupTip=PrdGrupTip'
      'NO_PGT=NO_PGT')
    DataSet = Qr1
    BCDToCurrency = False
    DataSetOptions = []
    Left = 560
    Top = 308
  end
  object frxPED_INPRI_000_01: TfrxReport
    Version = '2022.1'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39596.679376979200000000
    ReportOptions.LastChange = 39596.679376979200000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'procedure GH2OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      
        '  GH2.Visible := <VARF_AGRUPA1>;                                ' +
        '             '
      
        '  GF2.Visible := <VARF_AGRUPA1>;                                ' +
        '             '
      'end;'
      ''
      'begin'
      
        '  MeVARF_CODI_FRX.Text := Copy(Report.Name, 4, Length(Report.Nam' +
        'e));'
      'end.')
    OnGetValue = frxPED_INPRI_000_01GetValue
    Left = 696
    Top = 112
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDs1
        DataSetName = 'frxDs1'
      end
      item
        DataSet = frxDsPVI
        DataSetName = 'frxDsPVI'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 15.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
      MirrorMode = []
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 41.574830000000000000
        Top = 56.692950000000000000
        Width = 699.213050000000000000
        object Shape1: TfrxShapeView
          AllowVectorExport = True
          Width = 699.213050000000000000
          Height = 37.795300000000000000
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo13: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Width = 684.094930000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line1: TfrxLineView
          AllowVectorExport = True
          Top = 18.897650000000010000
          Width = 699.213050000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo14: TfrxMemoView
          AllowVectorExport = True
          Left = 120.944960000000000000
          Top = 18.897650000000010000
          Width = 457.323130000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'VENDAS POR PEDIDO DE VENDA - [VARF_SUB_TITULO]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo15: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Top = 18.897650000000010000
          Width = 113.385900000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'impresso em [Date], [Time]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo16: TfrxMemoView
          AllowVectorExport = True
          Left = 578.267716535433100000
          Top = 18.897650000000010000
          Width = 113.385826770000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page] de [TotalPages]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object ReportTitle1: TfrxReportTitle
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 13.228346460000000000
        Top = 18.897650000000000000
        Width = 699.213050000000000000
      end
      object GroupHeader1: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 18.897640240000000000
        Top = 211.653680000000000000
        Width = 699.213050000000000000
        Condition = 'frxDsPVI."Tabela"'
        object Memo22: TfrxMemoView
          AllowVectorExport = True
          Top = 3.779529999999994000
          Width = 699.213050000000000000
          Height = 15.118110240000000000
          DataSet = frxDsPVI
          DataSetName = 'frxDsPVI'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsPVI."Tabela"]')
          ParentFont = False
        end
      end
      object MasterData2: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -9
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Top = 253.228510000000000000
        Width = 699.213050000000000000
        ColumnGap = 37.795275590551200000
        DataSet = frxDsPVI
        DataSetName = 'frxDsPVI'
        RowCount = 0
      end
      object GroupFooter1: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Top = 275.905690000000000000
        Width = 699.213050000000000000
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118110236220470000
        Top = 381.732530000000000000
        Width = 699.213050000000000000
        DataSet = frxDs1
        DataSetName = 'frxDs1'
        RowCount = 0
        object Memo3: TfrxMemoView
          AllowVectorExport = True
          Left = 94.488250000000000000
          Width = 343.937230000000000000
          Height = 15.118110236220470000
          DataField = 'NOME_AGRUP'
          DataSet = frxDs1
          DataSetName = 'frxDs1'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDs1."NOME_AGRUP"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo21: TfrxMemoView
          AllowVectorExport = True
          Left = 438.425480000000000000
          Width = 109.606370000000000000
          Height = 15.118110236220470000
          DataField = 'QUANTX'
          DataSet = frxDs1
          DataSetName = 'frxDs1'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDs1."QUANTX"]')
          ParentFont = False
        end
        object Memo23: TfrxMemoView
          AllowVectorExport = True
          Left = 548.031849999999900000
          Width = 94.488250000000000000
          Height = 15.118110236220470000
          DataField = 'VAL_X_'
          DataSet = frxDs1
          DataSetName = 'frxDs1'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDs1."VAL_X_"]')
          ParentFont = False
        end
        object Memo25: TfrxMemoView
          AllowVectorExport = True
          Left = 642.520100000000000000
          Width = 56.692950000000010000
          Height = 15.118110236220470000
          DataField = 'SEQ'
          DataSet = frxDs1
          DataSetName = 'frxDs1'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDs1."SEQ"]')
          ParentFont = False
        end
        object Memo38: TfrxMemoView
          AllowVectorExport = True
          Width = 94.488250000000000000
          Height = 15.118110236220470000
          DataField = 'CODI_AGRUP'
          DataSet = frxDs1
          DataSetName = 'frxDs1'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDs1."CODI_AGRUP"]')
          ParentFont = False
          WordWrap = False
        end
      end
      object ReportSummary1: TfrxReportSummary
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 37.795300000000000000
        Top = 495.118430000000000000
        Width = 699.213050000000000000
        object Memo24: TfrxMemoView
          AllowVectorExport = True
          Top = 11.338589999999900000
          Width = 438.425480000000000000
          Height = 15.118110236220470000
          DataSetName = 'frxDsConsIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'TOTAL GERAL')
          ParentFont = False
        end
        object Memo43: TfrxMemoView
          AllowVectorExport = True
          Left = 438.425480000000000000
          Top = 11.338589999999900000
          Width = 109.606370000000000000
          Height = 15.118110236220470000
          DataSet = frxDs1
          DataSetName = 'frxDs1'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDs1."QUANTX">, MasterData1)]')
          ParentFont = False
        end
        object Memo44: TfrxMemoView
          AllowVectorExport = True
          Left = 548.031849999999900000
          Top = 11.338589999999900000
          Width = 94.488250000000000000
          Height = 15.118110236220470000
          DataSet = frxDs1
          DataSetName = 'frxDs1'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDs1."VAL_X_">, MasterData1)]')
          ParentFont = False
        end
        object Memo45: TfrxMemoView
          AllowVectorExport = True
          Left = 642.520100000000000000
          Top = 11.338589999999900000
          Width = 56.692950000000000000
          Height = 15.118110236220470000
          DataSet = frxDs1
          DataSetName = 'frxDs1'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
        end
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 13.228346460000000000
        Top = 555.590910000000000000
        Width = 699.213050000000000000
        object Memo1: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Width = 362.834880000000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'www.dermatek.com.br - software customizado')
          ParentFont = False
          VAlign = vaCenter
        end
        object MeVARF_CODI_FRX: TfrxMemoView
          AllowVectorExport = True
          Left = 370.393940000000000000
          Width = 321.260050000000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'VARF_CODI_FRX')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object Header2: TfrxHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 18.897637795275590000
        Top = 298.582870000000000000
        Width = 699.213050000000000000
        object Memo11: TfrxMemoView
          AllowVectorExport = True
          Left = 94.488250000000000000
          Top = 3.779530000000022000
          Width = 343.937230000000000000
          Height = 15.118110236220470000
          DataSet = frxDs1
          DataSetName = 'frxDs1'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Descri'#231#227'o / Nome')
          ParentFont = False
        end
        object Memo26: TfrxMemoView
          AllowVectorExport = True
          Left = 438.425480000000000000
          Top = 3.779530000000022000
          Width = 109.606370000000000000
          Height = 15.118110236220470000
          DataSet = frxDs1
          DataSetName = 'frxDs1'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Quantidade')
          ParentFont = False
        end
        object Memo27: TfrxMemoView
          AllowVectorExport = True
          Left = 548.031850000000000000
          Top = 3.779530000000022000
          Width = 94.488250000000000000
          Height = 15.118110236220470000
          DataSet = frxDs1
          DataSetName = 'frxDs1'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Valor')
          ParentFont = False
        end
        object Memo28: TfrxMemoView
          AllowVectorExport = True
          Left = 642.520100000000000000
          Top = 3.779530000000022000
          Width = 56.692950000000000000
          Height = 15.118110236220470000
          DataSet = frxDs1
          DataSetName = 'frxDs1'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Seq.')
          ParentFont = False
        end
        object Memo20: TfrxMemoView
          AllowVectorExport = True
          Top = 3.779530000000022000
          Width = 94.488250000000000000
          Height = 15.118110236220470000
          DataSet = frxDs1
          DataSetName = 'frxDs1'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'C'#243'digo')
          ParentFont = False
        end
      end
      object MasterData3: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 30.125996460000000000
        Top = 158.740260000000000000
        Width = 699.213050000000000000
        RowCount = 1
        object Memo29: TfrxMemoView
          AllowVectorExport = True
          Width = 177.637795280000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Data de inclus'#227'o')
          ParentFont = False
        end
        object Memo30: TfrxMemoView
          AllowVectorExport = True
          Top = 13.118120000000000000
          Width = 177.637795280000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_PERIODO_INCLU]')
          ParentFont = False
        end
        object Memo31: TfrxMemoView
          AllowVectorExport = True
          Left = 177.637910000000000000
          Width = 173.858267720000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Data de emiss'#227'o')
          ParentFont = False
        end
        object Memo32: TfrxMemoView
          AllowVectorExport = True
          Left = 177.637910000000000000
          Top = 13.118120000000000000
          Width = 173.858267720000000000
          Height = 13.228346460000000000
          DataSet = frxDsPVI
          DataSetName = 'frxDsPVI'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_PERIODO_EMISS]')
          ParentFont = False
        end
        object Memo33: TfrxMemoView
          AllowVectorExport = True
          Left = 351.496290000000000000
          Width = 173.858267720000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Data de chegada')
          ParentFont = False
        end
        object Memo34: TfrxMemoView
          AllowVectorExport = True
          Left = 351.496290000000000000
          Top = 13.118120000000000000
          Width = 173.858267720000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_PERIODO_ENTRA]')
          ParentFont = False
        end
        object Memo35: TfrxMemoView
          AllowVectorExport = True
          Left = 525.354670000000000000
          Width = 173.858267720000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Previs'#227'o de entrega')
          ParentFont = False
        end
        object Memo36: TfrxMemoView
          AllowVectorExport = True
          Left = 525.354670000000000000
          Top = 13.118120000000000000
          Width = 173.858267720000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_PERIODO_PREVI]')
          ParentFont = False
        end
      end
      object GH2: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 18.897637800000000000
        Top = 340.157700000000000000
        Width = 699.213050000000000000
        OnBeforePrint = 'GH2OnBeforePrint'
        Condition = 'frxDs1."PrdGrupTip"'
        object Memo2: TfrxMemoView
          AllowVectorExport = True
          Left = 94.488250000000000000
          Top = 3.779530000000022000
          Width = 604.724800000000000000
          Height = 15.118110236220470000
          DataSet = frxDs1
          DataSetName = 'frxDs1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDs1."NO_PGT"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo4: TfrxMemoView
          AllowVectorExport = True
          Top = 3.779530000000022000
          Width = 94.488250000000000000
          Height = 15.118110236220470000
          DataSet = frxDs1
          DataSetName = 'frxDs1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDs1."PrdGrupTip"]')
          ParentFont = False
          WordWrap = False
        end
      end
      object GF2: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118110236220470000
        Top = 419.527830000000000000
        Width = 699.213050000000000000
        object Memo5: TfrxMemoView
          AllowVectorExport = True
          Width = 438.425480000000000000
          Height = 15.118110236220470000
          DataSetName = 'frxDsConsIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'SUBTOTAL [frxDs1."NO_PGT"]:')
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          AllowVectorExport = True
          Left = 438.425480000000000000
          Width = 109.606370000000000000
          Height = 15.118110236220470000
          DataSet = frxDs1
          DataSetName = 'frxDs1'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDs1."QUANTX">, MasterData1)]')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          AllowVectorExport = True
          Left = 548.031849999999900000
          Width = 94.488250000000000000
          Height = 15.118110236220470000
          DataSet = frxDs1
          DataSetName = 'frxDs1'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDs1."VAL_X_">, MasterData1)]')
          ParentFont = False
        end
      end
    end
  end
  object QrGraAtrCad: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, CodUsu, Nome'
      'FROM graatrcad'
      'ORDER BY Nome')
    Left = 8
    Top = 8
    object QrGraAtrCadCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrGraAtrCadCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrGraAtrCadNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
  end
  object DsPVI: TDataSource
    DataSet = QrPVI
    Left = 560
    Top = 260
  end
  object frxDsPVI: TfrxDBDataset
    UserName = 'frxDsPVI'
    CloseDataSource = False
    DataSet = QrPVI
    BCDToCurrency = False
    DataSetOptions = []
    Left = 560
    Top = 356
  end
  object frxPED_INPRI_000_02: TfrxReport
    Version = '2022.1'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39596.679376979200000000
    ReportOptions.LastChange = 40819.429065879600000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'procedure GH2OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      
        '  GH2.Visible := <VARF_AGRUPA1>;                                ' +
        '             '
      '  GF2.Visible := <VARF_AGRUPA1>;  '
      'end;'
      ''
      'begin'
      
        '  MeVARF_CODI_FRX.Text := Copy(Report.Name, 4, Length(Report.Nam' +
        'e));'
      'end.')
    OnGetValue = frxPED_INPRI_000_01GetValue
    Left = 696
    Top = 160
    Datasets = <
      item
        DataSet = frxDs1
        DataSetName = 'frxDs1'
      end
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsPVI
        DataSetName = 'frxDsPVI'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 15.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
      MirrorMode = []
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 41.574830000000000000
        Top = 56.692950000000010000
        Width = 699.213050000000000000
        object Shape1: TfrxShapeView
          AllowVectorExport = True
          Width = 699.213050000000000000
          Height = 37.795300000000000000
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo13: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Width = 684.094930000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line1: TfrxLineView
          AllowVectorExport = True
          Top = 18.897650000000010000
          Width = 699.213050000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo14: TfrxMemoView
          AllowVectorExport = True
          Left = 120.944960000000000000
          Top = 18.897650000000010000
          Width = 457.323130000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'VENDAS POR PEDIDO DE VENDA - [VARF_SUB_TITULO]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo15: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Top = 18.897650000000010000
          Width = 113.385900000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'impresso em [Date], [Time]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo16: TfrxMemoView
          AllowVectorExport = True
          Left = 578.267716535433000000
          Top = 18.897650000000010000
          Width = 113.385826770000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page] de [TotalPages]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object ReportTitle1: TfrxReportTitle
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 13.228346460000000000
        Top = 18.897650000000000000
        Width = 699.213050000000000000
      end
      object GroupHeader1: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 18.897640240000000000
        Top = 211.653680000000000000
        Width = 699.213050000000000000
        Condition = 'frxDsPVI."Tabela"'
        object Memo22: TfrxMemoView
          AllowVectorExport = True
          Top = 3.779529999999994000
          Width = 699.213050000000000000
          Height = 15.118110240000000000
          DataSet = frxDsPVI
          DataSetName = 'frxDsPVI'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsPVI."Tabela"]')
          ParentFont = False
        end
      end
      object MasterData2: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -9
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Top = 253.228510000000000000
        Width = 699.213050000000000000
        ColumnGap = 37.795275590551200000
        DataSet = frxDsPVI
        DataSetName = 'frxDsPVI'
        RowCount = 0
      end
      object GroupFooter1: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Top = 275.905690000000000000
        Width = 699.213050000000000000
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 13.228346460000000000
        Top = 438.425480000000000000
        Width = 699.213050000000000000
        DataSet = frxDs1
        DataSetName = 'frxDs1'
        RowCount = 0
        object Memo21: TfrxMemoView
          AllowVectorExport = True
          Left = 495.118430000000000000
          Width = 83.149606300000000000
          Height = 13.228346460000000000
          DataField = 'QUANTX'
          DataSet = frxDs1
          DataSetName = 'frxDs1'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDs1."QUANTX"]')
          ParentFont = False
        end
        object Memo23: TfrxMemoView
          AllowVectorExport = True
          Left = 578.268090000000000000
          Width = 83.149606300000000000
          Height = 13.228346460000000000
          DataField = 'VAL_X_'
          DataSet = frxDs1
          DataSetName = 'frxDs1'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDs1."VAL_X_"]')
          ParentFont = False
        end
        object Memo25: TfrxMemoView
          AllowVectorExport = True
          Left = 661.417750000000000000
          Width = 37.795300000000000000
          Height = 13.228346460000000000
          DataField = 'SEQ'
          DataSet = frxDs1
          DataSetName = 'frxDs1'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDs1."SEQ"]')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          AllowVectorExport = True
          Left = 434.645950000000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          DataField = 'NO_TAM'
          DataSet = frxDs1
          DataSetName = 'frxDs1'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDs1."NO_TAM"]')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          AllowVectorExport = True
          Left = 37.795300000000000000
          Width = 396.850610940000000000
          Height = 13.228346460000000000
          DataSet = frxDs1
          DataSetName = 'frxDs1'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
          WordWrap = False
        end
      end
      object ReportSummary1: TfrxReportSummary
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 24.566936460000000000
        Top = 627.401980000000000000
        Width = 699.213050000000000000
        object Memo24: TfrxMemoView
          AllowVectorExport = True
          Top = 11.338589999999950000
          Width = 495.118430000000000000
          Height = 13.228346460000000000
          DataSetName = 'frxDsConsIts'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'TOTAIS')
          ParentFont = False
        end
        object Memo43: TfrxMemoView
          AllowVectorExport = True
          Left = 495.118430000000000000
          Top = 11.338589999999950000
          Width = 83.149660000000000000
          Height = 13.228346460000000000
          DataSet = frxDs1
          DataSetName = 'frxDs1'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDs1."QUANTX">, MasterData1)]')
          ParentFont = False
        end
        object Memo44: TfrxMemoView
          AllowVectorExport = True
          Left = 578.268090000000000000
          Top = 11.338589999999950000
          Width = 83.149660000000000000
          Height = 13.228346460000000000
          DataSet = frxDs1
          DataSetName = 'frxDs1'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDs1."VAL_X_">, MasterData1)]')
          ParentFont = False
        end
        object Memo45: TfrxMemoView
          AllowVectorExport = True
          Left = 661.417750000000000000
          Top = 11.338589999999950000
          Width = 37.795300000000000000
          Height = 13.228346460000000000
          DataSet = frxDs1
          DataSetName = 'frxDs1'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
        end
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 13.228346460000000000
        Top = 676.535870000000000000
        Width = 699.213050000000000000
        object Memo1: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Width = 362.834880000000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'www.dermatek.com.br - software customizado')
          ParentFont = False
          VAlign = vaCenter
        end
        object MeVARF_CODI_FRX: TfrxMemoView
          AllowVectorExport = True
          Left = 370.393940000000000000
          Width = 321.260050000000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'VARF_CODI_FRX')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object MasterData3: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 30.125996460000000000
        Top = 158.740260000000000000
        Width = 699.213050000000000000
        RowCount = 1
        object Memo29: TfrxMemoView
          AllowVectorExport = True
          Width = 177.637795280000000000
          Height = 13.228346460000000000
          DataSet = frxDsPVI
          DataSetName = 'frxDsPVI'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Data de inclus'#227'o')
          ParentFont = False
        end
        object Memo30: TfrxMemoView
          AllowVectorExport = True
          Top = 13.118120000000000000
          Width = 177.637795280000000000
          Height = 13.228346460000000000
          DataSet = frxDsPVI
          DataSetName = 'frxDsPVI'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_PERIODO_INCLU]')
          ParentFont = False
        end
        object Memo31: TfrxMemoView
          AllowVectorExport = True
          Left = 177.637910000000000000
          Width = 173.858267720000000000
          Height = 13.228346460000000000
          DataSet = frxDsPVI
          DataSetName = 'frxDsPVI'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Data de emiss'#227'o')
          ParentFont = False
        end
        object Memo32: TfrxMemoView
          AllowVectorExport = True
          Left = 177.637910000000000000
          Top = 13.118120000000000000
          Width = 173.858267720000000000
          Height = 13.228346460000000000
          DataSet = frxDsPVI
          DataSetName = 'frxDsPVI'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_PERIODO_EMISS]')
          ParentFont = False
        end
        object Memo33: TfrxMemoView
          AllowVectorExport = True
          Left = 351.496290000000000000
          Width = 173.858267720000000000
          Height = 13.228346460000000000
          DataSet = frxDsPVI
          DataSetName = 'frxDsPVI'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Data de chegada')
          ParentFont = False
        end
        object Memo34: TfrxMemoView
          AllowVectorExport = True
          Left = 351.496290000000000000
          Top = 13.118120000000000000
          Width = 173.858267720000000000
          Height = 13.228346460000000000
          DataSet = frxDsPVI
          DataSetName = 'frxDsPVI'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_PERIODO_ENTRA]')
          ParentFont = False
        end
        object Memo35: TfrxMemoView
          AllowVectorExport = True
          Left = 525.354670000000000000
          Width = 173.858267720000000000
          Height = 13.228346460000000000
          DataSet = frxDsPVI
          DataSetName = 'frxDsPVI'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Previs'#227'o de entrega')
          ParentFont = False
        end
        object Memo36: TfrxMemoView
          AllowVectorExport = True
          Left = 525.354670000000000000
          Top = 13.118120000000000000
          Width = 173.858267720000000000
          Height = 13.228346460000000000
          DataSet = frxDsPVI
          DataSetName = 'frxDsPVI'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_PERIODO_PREVI]')
          ParentFont = False
        end
      end
      object GroupHeader2: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 20.787404020000000000
        Top = 343.937230000000000000
        Width = 699.213050000000000000
        Condition = 'frxDs1."NOME_AGRUP"'
        object Memo5: TfrxMemoView
          AllowVectorExport = True
          Top = 3.779530000000022000
          Width = 37.795300000000000000
          Height = 15.118110240000000000
          DataSet = frxDs1
          DataSetName = 'frxDs1'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Grupo:')
          ParentFont = False
          WordWrap = False
        end
        object Memo6: TfrxMemoView
          AllowVectorExport = True
          Left = 37.795300000000000000
          Top = 3.779530000000022000
          Width = 94.488250000000000000
          Height = 15.118110240000000000
          DataSet = frxDs1
          DataSetName = 'frxDs1'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDs1."CODI_AGRUP"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo8: TfrxMemoView
          AllowVectorExport = True
          Left = 132.283550000000000000
          Top = 3.779530000000022000
          Width = 510.236550000000000000
          Height = 15.118110240000000000
          DataSet = frxDs1
          DataSetName = 'frxDs1'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDs1."NOME_AGRUP"]')
          ParentFont = False
          WordWrap = False
        end
      end
      object GroupFooter2: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 13.228346460000000000
        Top = 476.220780000000000000
        Width = 699.213050000000000000
        object Memo7: TfrxMemoView
          AllowVectorExport = True
          Left = 37.795300000000000000
          Width = 60.472480000000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'T O T A L')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo40: TfrxMemoView
          AllowVectorExport = True
          Left = 495.118430000000000000
          Width = 83.149606300000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDs1."QUANTX">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo9: TfrxMemoView
          AllowVectorExport = True
          Left = 578.268090000000000000
          Width = 83.149606300000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDs1."VAL_X_">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo11: TfrxMemoView
          AllowVectorExport = True
          Left = 661.417750000000000000
          Width = 37.795300000000000000
          Height = 13.228346460000000000
          DataSet = frxDs1
          DataSetName = 'frxDs1'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          AllowVectorExport = True
          Left = 98.267780000000000000
          Width = 396.850650000000000000
          Height = 13.228346460000000000
          DataSet = frxDs1
          DataSetName = 'frxDs1'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
        end
      end
      object GroupHeader3: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 26.456692920000000000
        Top = 389.291590000000000000
        Width = 699.213050000000000000
        Condition = 'frxDs1."CU_COR"'
        object Memo18: TfrxMemoView
          AllowVectorExport = True
          Left = 495.118430000000000000
          Top = 13.228344020000010000
          Width = 83.149606300000000000
          Height = 13.228346460000000000
          DataSet = frxDs1
          DataSetName = 'frxDs1'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Quantidade')
          ParentFont = False
          WordWrap = False
        end
        object Memo19: TfrxMemoView
          AllowVectorExport = True
          Left = 661.417750000000000000
          Top = 13.228344020000010000
          Width = 37.795246300000000000
          Height = 13.228346460000000000
          DataSet = frxDs1
          DataSetName = 'frxDs1'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Seq.')
          ParentFont = False
          WordWrap = False
        end
        object Memo37: TfrxMemoView
          AllowVectorExport = True
          Left = 37.795300000000000000
          Width = 56.692950000000000000
          Height = 13.228346460000000000
          DataSet = frxDs1
          DataSetName = 'frxDs1'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDs1."CU_COR"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo39: TfrxMemoView
          AllowVectorExport = True
          Left = 94.488250000000000000
          Width = 548.031849999999900000
          Height = 13.228346460000000000
          DataSet = frxDs1
          DataSetName = 'frxDs1'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDs1."NO_COR"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo41: TfrxMemoView
          AllowVectorExport = True
          Left = 434.645950000000000000
          Top = 13.228344020000010000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          DataSet = frxDs1
          DataSetName = 'frxDs1'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Tamanho')
          ParentFont = False
          WordWrap = False
        end
        object Memo46: TfrxMemoView
          AllowVectorExport = True
          Left = 578.268090000000000000
          Top = 13.228346460000010000
          Width = 83.149606300000000000
          Height = 13.228346460000000000
          DataSet = frxDs1
          DataSetName = 'frxDs1'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Valor')
          ParentFont = False
          WordWrap = False
        end
        object Memo47: TfrxMemoView
          AllowVectorExport = True
          Left = 15.118120000000000000
          Width = 22.677180000000000000
          Height = 13.228346460000000000
          DataSet = frxDs1
          DataSetName = 'frxDs1'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Cor:')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo2: TfrxMemoView
          AllowVectorExport = True
          Left = 37.795300000000000000
          Top = 13.228346460000010000
          Width = 396.850610940000000000
          Height = 13.228346460000000000
          DataSet = frxDs1
          DataSetName = 'frxDs1'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '')
          ParentFont = False
          WordWrap = False
        end
      end
      object GroupFooter3: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 7.559060000000000000
        Top = 514.016080000000000000
        Width = 699.213050000000000000
        object Memo10: TfrxMemoView
          AllowVectorExport = True
          Width = 699.213050000000000000
          Height = 7.559060000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GH2: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 22.677180000000000000
        Top = 298.582870000000000000
        Width = 699.213050000000000000
        OnBeforePrint = 'GH2OnBeforePrint'
        Condition = 'frxDs1."PrdGrupTip"'
        object Memo17: TfrxMemoView
          AllowVectorExport = True
          Left = 94.488250000000000000
          Top = 3.779530000000022000
          Width = 604.724800000000000000
          Height = 18.897650000000000000
          DataSet = frxDs1
          DataSetName = 'frxDs1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDs1."NO_PGT"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo20: TfrxMemoView
          AllowVectorExport = True
          Top = 3.779530000000022000
          Width = 94.488250000000000000
          Height = 18.897650000000000000
          DataSet = frxDs1
          DataSetName = 'frxDs1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDs1."PrdGrupTip"]')
          ParentFont = False
          WordWrap = False
        end
      end
      object GF2: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 22.677180000000000000
        Top = 544.252320000000100000
        Width = 699.213050000000000000
        object Memo26: TfrxMemoView
          AllowVectorExport = True
          Width = 495.118430000000000000
          Height = 13.228346460000000000
          DataSetName = 'frxDsConsIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'SUBTOTAL [frxDs1."NO_PGT"]:')
          ParentFont = False
        end
        object Memo27: TfrxMemoView
          AllowVectorExport = True
          Left = 495.118430000000000000
          Width = 83.149660000000000000
          Height = 13.228346460000000000
          DataSet = frxDs1
          DataSetName = 'frxDs1'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDs1."QUANTX">, MasterData1)]')
          ParentFont = False
        end
        object Memo28: TfrxMemoView
          AllowVectorExport = True
          Left = 578.268090000000000000
          Width = 83.149660000000000000
          Height = 13.228346460000000000
          DataSet = frxDs1
          DataSetName = 'frxDs1'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDs1."VAL_X_">, MasterData1)]')
          ParentFont = False
        end
        object Memo38: TfrxMemoView
          AllowVectorExport = True
          Left = 661.417750000000000000
          Width = 37.795300000000000000
          Height = 13.228346460000000000
          DataSet = frxDs1
          DataSetName = 'frxDs1'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
        end
        object Memo42: TfrxMemoView
          AllowVectorExport = True
          Left = 661.417750000000000000
          Width = 37.795300000000000000
          Height = 13.228346460000000000
          DataSet = frxDs1
          DataSetName = 'frxDs1'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
        end
      end
    end
  end
  object QrPVI: TABSQuery
    CurrentVersion = '7.93 '
    DatabaseName = 'MEMORY'
    InMemory = True
    ReadOnly = False
    RequestLive = True
    Left = 560
    Top = 212
    object QrPVITabela: TWideStringField
      FieldName = 'Tabela'
      Size = 30
    end
    object QrPVICodUsu1: TIntegerField
      FieldName = 'CodUsu1'
    end
    object QrPVICodUsu2: TIntegerField
      FieldName = 'CodUsu2'
    end
    object QrPVICodUsu3: TIntegerField
      FieldName = 'CodUsu3'
    end
    object QrPVINome1: TWideStringField
      FieldName = 'Nome1'
      Size = 100
    end
    object QrPVINome2: TWideStringField
      FieldName = 'Nome2'
      Size = 100
    end
    object QrPVINome3: TWideStringField
      FieldName = 'Nome3'
      Size = 100
    end
  end
  object frxReport1: TfrxReport
    Version = '2022.1'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39596.679376979200000000
    ReportOptions.LastChange = 39596.679376979200000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'procedure GH2OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      
        '  GH2.Visible := <VARF_AGRUPA1>;                                ' +
        '             '
      
        '  GF2.Visible := <VARF_AGRUPA1>;                                ' +
        '             '
      'end;'
      ''
      'begin'
      
        '  MeVARF_CODI_FRX.Text := Copy(Report.Name, 4, Length(Report.Nam' +
        'e));'
      'end.')
    OnGetValue = frxPED_INPRI_000_01GetValue
    Left = 820
    Top = 116
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDs1
        DataSetName = 'frxDs1'
      end
      item
        DataSet = frxDsPVI
        DataSetName = 'frxDsPVI'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 15.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
      MirrorMode = []
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 41.574830000000000000
        Top = 56.692950000000000000
        Width = 699.213050000000000000
        object Shape1: TfrxShapeView
          AllowVectorExport = True
          Width = 699.213050000000000000
          Height = 37.795300000000000000
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo13: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Width = 684.094930000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line1: TfrxLineView
          AllowVectorExport = True
          Top = 18.897650000000010000
          Width = 699.213050000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo14: TfrxMemoView
          AllowVectorExport = True
          Left = 120.944960000000000000
          Top = 18.897650000000010000
          Width = 457.323130000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'VENDAS POR PEDIDO DE VENDA - [VARF_SUB_TITULO]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo15: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Top = 18.897650000000010000
          Width = 113.385900000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'impresso em [Date], [Time]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo16: TfrxMemoView
          AllowVectorExport = True
          Left = 578.267716535433100000
          Top = 18.897650000000010000
          Width = 113.385826770000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page] de [TotalPages]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object ReportTitle1: TfrxReportTitle
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 13.228346460000000000
        Top = 18.897650000000000000
        Width = 699.213050000000000000
      end
      object GroupHeader1: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 18.897640240000000000
        Top = 211.653680000000000000
        Width = 699.213050000000000000
        Condition = 'frxDsPVI."Tabela"'
        object Memo22: TfrxMemoView
          AllowVectorExport = True
          Top = 3.779529999999994000
          Width = 699.213050000000000000
          Height = 15.118110240000000000
          DataSet = frxDsPVI
          DataSetName = 'frxDsPVI'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsPVI."Tabela"]')
          ParentFont = False
        end
      end
      object MasterData2: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -9
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Top = 253.228510000000000000
        Width = 699.213050000000000000
        ColumnGap = 37.795275590551200000
        DataSet = frxDsPVI
        DataSetName = 'frxDsPVI'
        RowCount = 0
      end
      object GroupFooter1: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Top = 275.905690000000000000
        Width = 699.213050000000000000
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118110236220470000
        Top = 381.732530000000000000
        Width = 699.213050000000000000
        DataSet = frxDs1
        DataSetName = 'frxDs1'
        RowCount = 0
        object Memo3: TfrxMemoView
          AllowVectorExport = True
          Left = 94.488250000000000000
          Width = 343.937230000000000000
          Height = 15.118110236220470000
          DataField = 'NOME_AGRUP'
          DataSet = frxDs1
          DataSetName = 'frxDs1'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDs1."NOME_AGRUP"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo21: TfrxMemoView
          AllowVectorExport = True
          Left = 438.425480000000000000
          Width = 109.606370000000000000
          Height = 15.118110236220470000
          DataField = 'QUANTX'
          DataSet = frxDs1
          DataSetName = 'frxDs1'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDs1."QUANTX"]')
          ParentFont = False
        end
        object Memo23: TfrxMemoView
          AllowVectorExport = True
          Left = 548.031849999999900000
          Width = 94.488250000000000000
          Height = 15.118110236220470000
          DataField = 'VAL_X_'
          DataSet = frxDs1
          DataSetName = 'frxDs1'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDs1."VAL_X_"]')
          ParentFont = False
        end
        object Memo25: TfrxMemoView
          AllowVectorExport = True
          Left = 642.520100000000000000
          Width = 56.692950000000010000
          Height = 15.118110236220470000
          DataField = 'SEQ'
          DataSet = frxDs1
          DataSetName = 'frxDs1'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDs1."SEQ"]')
          ParentFont = False
        end
        object Memo38: TfrxMemoView
          AllowVectorExport = True
          Width = 94.488250000000000000
          Height = 15.118110236220470000
          DataField = 'CODI_AGRUP'
          DataSet = frxDs1
          DataSetName = 'frxDs1'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDs1."CODI_AGRUP"]')
          ParentFont = False
          WordWrap = False
        end
      end
      object ReportSummary1: TfrxReportSummary
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 37.795300000000000000
        Top = 495.118430000000000000
        Width = 699.213050000000000000
        object Memo24: TfrxMemoView
          AllowVectorExport = True
          Top = 11.338589999999900000
          Width = 438.425480000000000000
          Height = 15.118110236220470000
          DataSetName = 'frxDsConsIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'TOTAL GERAL')
          ParentFont = False
        end
        object Memo43: TfrxMemoView
          AllowVectorExport = True
          Left = 438.425480000000000000
          Top = 11.338589999999900000
          Width = 109.606370000000000000
          Height = 15.118110236220470000
          DataSet = frxDs1
          DataSetName = 'frxDs1'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDs1."QUANTX">, MasterData1)]')
          ParentFont = False
        end
        object Memo44: TfrxMemoView
          AllowVectorExport = True
          Left = 548.031849999999900000
          Top = 11.338589999999900000
          Width = 94.488250000000000000
          Height = 15.118110236220470000
          DataSet = frxDs1
          DataSetName = 'frxDs1'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDs1."VAL_X_">, MasterData1)]')
          ParentFont = False
        end
        object Memo45: TfrxMemoView
          AllowVectorExport = True
          Left = 642.520100000000000000
          Top = 11.338589999999900000
          Width = 56.692950000000000000
          Height = 15.118110236220470000
          DataSet = frxDs1
          DataSetName = 'frxDs1'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
        end
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 13.228346460000000000
        Top = 555.590910000000000000
        Width = 699.213050000000000000
        object Memo1: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Width = 362.834880000000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'www.dermatek.com.br - software customizado')
          ParentFont = False
          VAlign = vaCenter
        end
        object MeVARF_CODI_FRX: TfrxMemoView
          AllowVectorExport = True
          Left = 370.393940000000000000
          Width = 321.260050000000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'VARF_CODI_FRX')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object Header2: TfrxHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 18.897637795275590000
        Top = 298.582870000000000000
        Width = 699.213050000000000000
        object Memo11: TfrxMemoView
          AllowVectorExport = True
          Left = 94.488250000000000000
          Top = 3.779530000000022000
          Width = 343.937230000000000000
          Height = 15.118110236220470000
          DataSet = frxDs1
          DataSetName = 'frxDs1'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Descri'#231#227'o / Nome')
          ParentFont = False
        end
        object Memo26: TfrxMemoView
          AllowVectorExport = True
          Left = 438.425480000000000000
          Top = 3.779530000000022000
          Width = 109.606370000000000000
          Height = 15.118110236220470000
          DataSet = frxDs1
          DataSetName = 'frxDs1'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Quantidade')
          ParentFont = False
        end
        object Memo27: TfrxMemoView
          AllowVectorExport = True
          Left = 548.031850000000000000
          Top = 3.779530000000022000
          Width = 94.488250000000000000
          Height = 15.118110236220470000
          DataSet = frxDs1
          DataSetName = 'frxDs1'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Valor')
          ParentFont = False
        end
        object Memo28: TfrxMemoView
          AllowVectorExport = True
          Left = 642.520100000000000000
          Top = 3.779530000000022000
          Width = 56.692950000000000000
          Height = 15.118110236220470000
          DataSet = frxDs1
          DataSetName = 'frxDs1'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Seq.')
          ParentFont = False
        end
        object Memo20: TfrxMemoView
          AllowVectorExport = True
          Top = 3.779530000000022000
          Width = 94.488250000000000000
          Height = 15.118110236220470000
          DataSet = frxDs1
          DataSetName = 'frxDs1'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'C'#243'digo')
          ParentFont = False
        end
      end
      object MasterData3: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 30.125996460000000000
        Top = 158.740260000000000000
        Width = 699.213050000000000000
        RowCount = 1
        object Memo29: TfrxMemoView
          AllowVectorExport = True
          Width = 177.637795280000000000
          Height = 13.228346460000000000
          DataSet = frxDsPVI
          DataSetName = 'frxDsPVI'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Data de inclus'#227'o')
          ParentFont = False
        end
        object Memo30: TfrxMemoView
          AllowVectorExport = True
          Top = 13.118120000000000000
          Width = 177.637795280000000000
          Height = 13.228346460000000000
          DataSet = frxDsPVI
          DataSetName = 'frxDsPVI'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_PERIODO_INCLU]')
          ParentFont = False
        end
        object Memo31: TfrxMemoView
          AllowVectorExport = True
          Left = 177.637910000000000000
          Width = 173.858267720000000000
          Height = 13.228346460000000000
          DataSet = frxDsPVI
          DataSetName = 'frxDsPVI'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Data de emiss'#227'o')
          ParentFont = False
        end
        object Memo32: TfrxMemoView
          AllowVectorExport = True
          Left = 177.637910000000000000
          Top = 13.118120000000000000
          Width = 173.858267720000000000
          Height = 13.228346460000000000
          DataSet = frxDsPVI
          DataSetName = 'frxDsPVI'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_PERIODO_EMISS]')
          ParentFont = False
        end
        object Memo33: TfrxMemoView
          AllowVectorExport = True
          Left = 351.496290000000000000
          Width = 173.858267720000000000
          Height = 13.228346460000000000
          DataSet = frxDsPVI
          DataSetName = 'frxDsPVI'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Data de chegada')
          ParentFont = False
        end
        object Memo34: TfrxMemoView
          AllowVectorExport = True
          Left = 351.496290000000000000
          Top = 13.118120000000000000
          Width = 173.858267720000000000
          Height = 13.228346460000000000
          DataSet = frxDsPVI
          DataSetName = 'frxDsPVI'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_PERIODO_ENTRA]')
          ParentFont = False
        end
        object Memo35: TfrxMemoView
          AllowVectorExport = True
          Left = 525.354670000000000000
          Width = 173.858267720000000000
          Height = 13.228346460000000000
          DataSet = frxDsPVI
          DataSetName = 'frxDsPVI'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Previs'#227'o de entrega')
          ParentFont = False
        end
        object Memo36: TfrxMemoView
          AllowVectorExport = True
          Left = 525.354670000000000000
          Top = 13.118120000000000000
          Width = 173.858267720000000000
          Height = 13.228346460000000000
          DataSet = frxDsPVI
          DataSetName = 'frxDsPVI'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_PERIODO_PREVI]')
          ParentFont = False
        end
      end
      object GH2: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 18.897637800000000000
        Top = 340.157700000000000000
        Width = 699.213050000000000000
        OnBeforePrint = 'GH2OnBeforePrint'
        Condition = 'frxDs1."PrdGrupTip"'
        object Memo2: TfrxMemoView
          AllowVectorExport = True
          Left = 94.488250000000000000
          Top = 3.779530000000022000
          Width = 604.724800000000000000
          Height = 15.118110236220470000
          DataSet = frxDs1
          DataSetName = 'frxDs1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDs1."NO_PGT"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo4: TfrxMemoView
          AllowVectorExport = True
          Top = 3.779530000000022000
          Width = 94.488250000000000000
          Height = 15.118110236220470000
          DataSet = frxDs1
          DataSetName = 'frxDs1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDs1."PrdGrupTip"]')
          ParentFont = False
          WordWrap = False
        end
      end
      object GF2: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118110236220470000
        Top = 419.527830000000000000
        Width = 699.213050000000000000
        object Memo5: TfrxMemoView
          AllowVectorExport = True
          Width = 438.425480000000000000
          Height = 15.118110236220470000
          DataSetName = 'frxDsConsIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'SUBTOTAL [frxDs1."NO_PGT"]:')
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          AllowVectorExport = True
          Left = 438.425480000000000000
          Width = 109.606370000000000000
          Height = 15.118110236220470000
          DataSet = frxDs1
          DataSetName = 'frxDs1'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDs1."QUANTX">, MasterData1)]')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          AllowVectorExport = True
          Left = 548.031849999999900000
          Width = 94.488250000000000000
          Height = 15.118110236220470000
          DataSet = frxDs1
          DataSetName = 'frxDs1'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDs1."VAL_X_">, MasterData1)]')
          ParentFont = False
        end
      end
    end
  end
end
