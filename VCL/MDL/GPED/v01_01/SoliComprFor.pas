unit SoliComprFor;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, UnInternalConsts, Buttons, DBCtrls, Db, (*DBTables,*)
  UnGOTOy, Mask, UMySQLModule, mySQLDbTables, dmkEdit, dmkEditCB,
  dmkDBLookupComboBox, dmkGeral, dmkImage, Variants, UnDmkEnums;

type
  TFmSoliComprFor = class(TForm)
    PainelDados: TPanel;
    LaPrompt: TLabel;
    CBEntidade: TdmkDBLookupComboBox;
    DsEntidades: TDataSource;
    EdEntidade: TdmkEditCB;
    LaOrdem: TLabel;
    QrEntidades: TMySQLQuery;
    EdRelevancia: TdmkEdit;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    CkContinua: TCheckBox;
    QrEntidadesCodigo: TIntegerField;
    QrEntidadesNO_FORNECE: TWideStringField;
    procedure FormResize(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    //
    FCodigo, FFornece: Integer;
  end;

var
  FmSoliComprFor: TFmSoliComprFor;

implementation

uses UnMyObjects, Module, DmkDAC_PF;

{$R *.DFM}

procedure TFmSoliComprFor.BtOKClick(Sender: TObject);
var
  FFornece, Relevancia: Integer;
begin
  FFornece   := EdEntidade.ValueVariant;
  Relevancia := EdRelevancia.ValueVariant;
  //
  UMyMod.SQLInsUpd_IGNORE(Dmod.QrUpd, stIns, 'solicomprfor', False, [
  'Relevancia'], [
  'Codigo', 'Fornece'], [
  Relevancia], [
  FCodigo, FFornece], True);
  //
  if CkContinua.Checked then
  begin
    EdEntidade.ValueVariant := 0;
    CBEntidade.KeyValue := 0;
    //
    EdEntidade.SetFocus;
  end else
    Close;
end;

procedure TFmSoliComprFor.BtSaidaClick(Sender: TObject);
begin
  VAR_SELCOD := 0;
  Close;
end;

procedure TFmSoliComprFor.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmSoliComprFor.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmSoliComprFor.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stIns;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrEntidades, Dmod.MyDB, [
  'SELECT ent.Codigo, ',
  'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_FORNECE',
  'FROM entidades ent ',
  'WHERE (ent.Fornece1="V" OR ent.Fornece2="V" OR ent.Fornece3="V" OR ',
  'ent.Fornece4="V" OR ent.Fornece5="V" OR ent.Fornece6="V" ',
  'OR ent.Fornece7="V" OR ent.Fornece8="V") ',
  'ORDER BY NO_FORNECE',
  '']);
end;

end.
