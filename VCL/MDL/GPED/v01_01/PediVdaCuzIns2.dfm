object FmPediVdaCuzIns2: TFmPediVdaCuzIns2
  Left = 339
  Top = 185
  Caption = 'PED-VENDA-005 :: Item de Pedido - Inclus'#227'o Customizada'
  ClientHeight = 646
  ClientWidth = 669
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 669
    Height = 490
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 669
      Height = 68
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object Label1: TLabel
        Left = 8
        Top = 12
        Width = 40
        Height = 13
        Caption = 'Produto:'
      end
      object SpeedButton1: TSpeedButton
        Left = 65
        Top = 28
        Width = 22
        Height = 21
        Caption = '?'
        OnClick = SpeedButton1Click
      end
      object EdGraGru1: TdmkEditCB
        Left = 8
        Top = 28
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = EdGraGru1Change
        OnKeyDown = EdGraGru1KeyDown
        DBLookupComboBox = CBGraGru1
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBGraGru1: TdmkDBLookupComboBox
        Left = 88
        Top = 28
        Width = 569
        Height = 21
        KeyField = 'CodUsu'
        ListField = 'NOME_EX'
        ListSource = DsGraGru1
        TabOrder = 1
        dmkEditCB = EdGraGru1
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
    end
    object Panel4: TPanel
      Left = 0
      Top = 68
      Width = 669
      Height = 360
      Align = alClient
      Caption = 'Panel4'
      TabOrder = 1
      object GradeC: TStringGrid
        Left = 1
        Top = 1
        Width = 667
        Height = 220
        Align = alClient
        ColCount = 2
        DefaultColWidth = 65
        DefaultRowHeight = 18
        FixedCols = 0
        RowCount = 2
        FixedRows = 0
        TabOrder = 0
        OnClick = GradeCClick
        OnDblClick = GradeCDblClick
        OnDrawCell = GradeCDrawCell
        RowHeights = (
          18
          18)
      end
      object GradeA: TStringGrid
        Left = 1
        Top = 221
        Width = 667
        Height = 138
        Align = alBottom
        ColCount = 2
        DefaultColWidth = 65
        DefaultRowHeight = 18
        FixedCols = 0
        RowCount = 2
        FixedRows = 0
        TabOrder = 1
        Visible = False
        RowHeights = (
          18
          18)
      end
      object GradeX: TStringGrid
        Left = 372
        Top = 20
        Width = 205
        Height = 41
        ColCount = 2
        DefaultRowHeight = 18
        FixedCols = 0
        RowCount = 2
        FixedRows = 0
        Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goColSizing, goEditing]
        TabOrder = 2
        Visible = False
        RowHeights = (
          18
          18)
      end
    end
    object Panel5: TPanel
      Left = 0
      Top = 428
      Width = 669
      Height = 62
      Align = alBottom
      BevelOuter = bvNone
      TabOrder = 2
      object GroupBox2: TGroupBox
        Left = 0
        Top = 0
        Width = 136
        Height = 62
        Align = alLeft
        Caption = ' Dados de inclus'#227'o: '
        TabOrder = 0
        object Label3: TLabel
          Left = 8
          Top = 16
          Width = 48
          Height = 13
          Caption = 'Reduzido:'
        end
        object Label4: TLabel
          Left = 68
          Top = 16
          Width = 58
          Height = 13
          Caption = 'Quantidade:'
        end
        object EdGraGruX: TdmkEdit
          Left = 8
          Top = 32
          Width = 56
          Height = 21
          TabStop = False
          Alignment = taRightJustify
          Enabled = False
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          OnChange = EdGraGruXChange
        end
        object EdQuantP: TdmkEdit
          Left = 68
          Top = 32
          Width = 61
          Height = 21
          Alignment = taRightJustify
          TabOrder = 1
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
          OnChange = EdQuantPChange
        end
      end
      object GroupBox1: TGroupBox
        Left = 136
        Top = 0
        Width = 533
        Height = 62
        Align = alClient
        Caption = 
          ' Medidas gerais ('#250'teis) do produto (pe'#231'as, kg, litros, metros, e' +
          'tc.): '
        TabOrder = 1
        object DBText1: TDBText
          Left = 8
          Top = 16
          Width = 100
          Height = 13
          DataField = 'Medida1'
          DataSource = DsGraGru1
        end
        object DBText2: TDBText
          Left = 112
          Top = 16
          Width = 100
          Height = 13
          DataField = 'Medida2'
          DataSource = DsGraGru1
        end
        object DBText3: TDBText
          Left = 216
          Top = 16
          Width = 100
          Height = 13
          DataField = 'Medida3'
          DataSource = DsGraGru1
        end
        object DBText4: TDBText
          Left = 320
          Top = 16
          Width = 100
          Height = 13
          DataField = 'Medida4'
          DataSource = DsGraGru1
        end
        object EdMedidaC: TdmkEdit
          Left = 8
          Top = 32
          Width = 100
          Height = 21
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 4
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,0000'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
          OnChange = EdGraGruXChange
        end
        object EdMedidaL: TdmkEdit
          Left = 112
          Top = 32
          Width = 100
          Height = 21
          Alignment = taRightJustify
          TabOrder = 1
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 4
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,0000'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
          OnChange = EdQuantPChange
        end
        object EdMedidaA: TdmkEdit
          Left = 216
          Top = 32
          Width = 100
          Height = 21
          Alignment = taRightJustify
          TabOrder = 2
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 4
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,0000'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
          OnChange = EdQuantPChange
        end
        object EdMedidaE: TdmkEdit
          Left = 320
          Top = 32
          Width = 100
          Height = 21
          Alignment = taRightJustify
          TabOrder = 3
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 6
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,000000'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
          OnChange = EdGraGruXChange
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 669
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 621
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 573
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 475
        Height = 32
        Caption = 'Item de Pedido - Inclus'#227'o Customizada'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 475
        Height = 32
        Caption = 'Item de Pedido - Inclus'#227'o Customizada'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 475
        Height = 32
        Caption = 'Item de Pedido - Inclus'#227'o Customizada'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 538
    Width = 669
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel6: TPanel
      Left = 2
      Top = 15
      Width = 665
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 582
    Width = 669
    Height = 64
    Align = alBottom
    TabOrder = 3
    object Panel7: TPanel
      Left = 2
      Top = 15
      Width = 665
      Height = 47
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object PnSaiDesis: TPanel
        Left = 521
        Top = 0
        Width = 144
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtSaida: TBitBtn
          Tag = 15
          Left = 2
          Top = 3
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
        end
      end
      object BtOK: TBitBtn
        Tag = 14
        Left = 20
        Top = 3
        Width = 120
        Height = 40
        Caption = '&OK'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object QrGraGru1: TmySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrGraGru1BeforeClose
    AfterScroll = QrGraGru1AfterScroll
    SQL.Strings = (
      'SELECT gg1.Codusu, gg1.Nivel3, gg1.Nivel2, gg1.Nivel1, '
      'gg1.Nome, gg1.PrdGrupTip, gg1.GraTamCad, '
      'gg1.PerCuztMin, gg1.PerCuztMax,'
      'gtc.Nome NOMEGRATAMCAD, gtc.CodUsu CODUSUGRATAMCAD,'
      'gg1.CST_A, gg1.CST_B, gg1.UnidMed, gg1.NCM, gg1.Peso,'
      'unm.Sigla SIGLAUNIDMED, unm.CodUsu CODUSUUNIDMED, '
      'unm.Nome NOMEUNIDMED,'
      'CONCAT(LPAD(gg1.CodUsu, 8, '#39'0'#39'), '#39' - '#39', gg1.Nome) NOME_EX,'
      'mor.Medida1, mor.Medida2, mor.Medida3, mor.Medida4  '
      'FROM gragru1 gg1'
      'LEFT JOIN prdgruptip tip ON tip.Codigo=gg1.PrdGrupTip'
      'LEFT JOIN gratamcad gtc ON gtc.Codigo=gg1.GraTamCad'
      'LEFT JOIN unidmed   unm ON unm.Codigo=gg1.UnidMed'
      'LEFT JOIN medordem mor ON mor.Codigo=gg1.MedOrdem'
      'WHERE tip.TipPrd=1'
      'AND tip.Customizav=1'
      'ORDER BY gg1.Nome')
    Left = 8
    Top = 8
    object QrGraGru1Codusu: TIntegerField
      FieldName = 'Codusu'
      Required = True
    end
    object QrGraGru1Nivel3: TIntegerField
      FieldName = 'Nivel3'
      Required = True
    end
    object QrGraGru1Nivel2: TIntegerField
      FieldName = 'Nivel2'
      Required = True
    end
    object QrGraGru1Nivel1: TIntegerField
      FieldName = 'Nivel1'
      Required = True
    end
    object QrGraGru1Nome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 30
    end
    object QrGraGru1PrdGrupTip: TIntegerField
      FieldName = 'PrdGrupTip'
      Required = True
    end
    object QrGraGru1GraTamCad: TIntegerField
      FieldName = 'GraTamCad'
      Required = True
    end
    object QrGraGru1NOMEGRATAMCAD: TWideStringField
      FieldName = 'NOMEGRATAMCAD'
      Size = 50
    end
    object QrGraGru1CODUSUGRATAMCAD: TIntegerField
      FieldName = 'CODUSUGRATAMCAD'
      Required = True
    end
    object QrGraGru1CST_A: TSmallintField
      FieldName = 'CST_A'
      Required = True
    end
    object QrGraGru1CST_B: TSmallintField
      FieldName = 'CST_B'
      Required = True
    end
    object QrGraGru1UnidMed: TIntegerField
      FieldName = 'UnidMed'
      Required = True
    end
    object QrGraGru1NCM: TWideStringField
      FieldName = 'NCM'
      Required = True
      Size = 10
    end
    object QrGraGru1Peso: TFloatField
      FieldName = 'Peso'
      Required = True
    end
    object QrGraGru1SIGLAUNIDMED: TWideStringField
      FieldName = 'SIGLAUNIDMED'
      Size = 3
    end
    object QrGraGru1CODUSUUNIDMED: TIntegerField
      FieldName = 'CODUSUUNIDMED'
      Required = True
    end
    object QrGraGru1NOMEUNIDMED: TWideStringField
      FieldName = 'NOMEUNIDMED'
      Size = 30
    end
    object QrGraGru1NOME_EX: TWideStringField
      FieldName = 'NOME_EX'
      Required = True
      Size = 44
    end
    object QrGraGru1PerCuztMin: TFloatField
      FieldName = 'PerCuztMin'
      DisplayFormat = '0.00'
    end
    object QrGraGru1PerCuztMax: TFloatField
      FieldName = 'PerCuztMax'
      DisplayFormat = '0.00'
    end
    object QrGraGru1Medida1: TWideStringField
      FieldName = 'Medida1'
    end
    object QrGraGru1Medida2: TWideStringField
      FieldName = 'Medida2'
    end
    object QrGraGru1Medida3: TWideStringField
      FieldName = 'Medida3'
    end
    object QrGraGru1Medida4: TWideStringField
      FieldName = 'Medida4'
    end
  end
  object DsGraGru1: TDataSource
    DataSet = QrGraGru1
    Left = 36
    Top = 8
  end
end
