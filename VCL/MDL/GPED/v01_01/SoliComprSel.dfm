object FmSoliComprSel: TFmSoliComprSel
  Left = 366
  Top = 203
  Caption = 'SLC-COMPR-005 :: Sele'#231#227'o de Solicita'#231#227'o de Compra / Servi'#231'o'
  ClientHeight = 467
  ClientWidth = 950
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 950
    Height = 300
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object PageControl1: TPageControl
      Left = 0
      Top = 0
      Width = 950
      Height = 300
      ActivePage = TabSheet2
      Align = alClient
      TabOrder = 0
      object TabSheet1: TTabSheet
        Caption = ' Pesquisa '
        object Panel3: TPanel
          Left = 0
          Top = 0
          Width = 942
          Height = 272
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          object PainelDados: TPanel
            Left = 0
            Top = 0
            Width = 942
            Height = 49
            Align = alTop
            BevelOuter = bvNone
            TabOrder = 0
            object Label1: TLabel
              Left = 12
              Top = 4
              Width = 40
              Height = 13
              Caption = 'Numero:'
            end
            object Label2: TLabel
              Left = 104
              Top = 4
              Width = 55
              Height = 13
              Caption = 'Refer'#234'ncia:'
            end
            object SpeedButton1: TSpeedButton
              Left = 70
              Top = 18
              Width = 25
              Height = 25
              Caption = '>'
              OnClick = SpeedButton1Click
            end
            object SpeedButton2: TSpeedButton
              Left = 428
              Top = 18
              Width = 25
              Height = 25
              Caption = '>'
              OnClick = SpeedButton2Click
            end
            object EdCodUsu: TdmkEdit
              Left = 12
              Top = 20
              Width = 56
              Height = 21
              Alignment = taRightJustify
              TabOrder = 0
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
            end
            object EdReferencia: TdmkEdit
              Left = 104
              Top = 20
              Width = 321
              Height = 21
              TabOrder = 1
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '%%'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = '%%'
              ValWarn = False
            end
          end
          object DBGrid1: TDBGrid
            Left = 0
            Top = 49
            Width = 733
            Height = 223
            Align = alLeft
            DataSource = DsSoliComprCab
            TabOrder = 1
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            OnDblClick = DBGrid1DblClick
            Columns = <
              item
                Expanded = False
                FieldName = 'CodUsu'
                Title.Caption = 'C'#243'digo'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ReferenPedi'
                Width = 120
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'DtaEntra'
                Title.Caption = 'Dta cadastro'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'DtaPrevi'
                Title.Caption = 'Dta previs'#227'o'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Cliente'
                Title.Caption = 'Fornecedor'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NO_ENT'
                Title.Caption = 'Nome do Fornecedor'
                Width = 200
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'QuantP'
                Title.Caption = 'Itens'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ValLiq'
                Title.Caption = 'Val. Liq.'
                Visible = True
              end>
          end
          object DBMemo1: TDBMemo
            Left = 733
            Top = 49
            Width = 209
            Height = 223
            Align = alClient
            DataField = 'Observa'
            DataSource = DsSoliComprCab
            TabOrder = 2
          end
        end
      end
      object TabSheet2: TTabSheet
        Caption = ' Verifica e confirma'
        ImageIndex = 1
        object Panel5: TPanel
          Left = 0
          Top = 0
          Width = 942
          Height = 272
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          object DBGGru: TdmkDBGrid
            Left = 0
            Top = 0
            Width = 942
            Height = 272
            Align = alClient
            Columns = <
              item
                Expanded = False
                FieldName = 'CodUsu'
                Title.Caption = 'C'#243'digo'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Nome'
                Title.Caption = 'Descri'#231#227'o'
                Width = 403
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'SaldoQtd'
                Title.Caption = 'Saldo pedido'
                Width = 72
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'QuantS'
                Title.Caption = 'Qtd pedido'
                Width = 72
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'PrecoF'
                Title.Caption = 'Pre'#231'o'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ValBru'
                Title.Caption = 'Val. Bru.'
                Width = 72
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ValLiq'
                Title.Caption = 'Val. liq.'
                Width = 72
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Controle'
                Title.Caption = 'ID'
                Visible = True
              end>
            Color = clWindow
            DataSource = DsSoliComprGru
            Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
            TabOrder = 0
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            Columns = <
              item
                Expanded = False
                FieldName = 'CodUsu'
                Title.Caption = 'C'#243'digo'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Nome'
                Title.Caption = 'Descri'#231#227'o'
                Width = 403
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'SaldoQtd'
                Title.Caption = 'Saldo pedido'
                Width = 72
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'QuantS'
                Title.Caption = 'Qtd pedido'
                Width = 72
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'PrecoF'
                Title.Caption = 'Pre'#231'o'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ValBru'
                Title.Caption = 'Val. Bru.'
                Width = 72
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ValLiq'
                Title.Caption = 'Val. liq.'
                Width = 72
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Controle'
                Title.Caption = 'ID'
                Visible = True
              end>
          end
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 950
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 902
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 854
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 532
        Height = 32
        Caption = 'Sele'#231#227'o de Solicita'#231#227'o de Compra / Servi'#231'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 532
        Height = 32
        Caption = 'Sele'#231#227'o de Solicita'#231#227'o de Compra / Servi'#231'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 532
        Height = 32
        Caption = 'Sele'#231#227'o de Solicita'#231#227'o de Compra / Servi'#231'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 348
    Width = 950
    Height = 55
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 946
      Height = 38
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object PB1: TProgressBar
        Left = 0
        Top = 21
        Width = 946
        Height = 17
        Align = alBottom
        TabOrder = 0
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 403
    Width = 950
    Height = 64
    Align = alBottom
    TabOrder = 3
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 946
      Height = 47
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object PnSaiDesis: TPanel
        Left = 802
        Top = 0
        Width = 144
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 0
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 12
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Hint = 'Cancela inclus'#227'o / altera'#231#227'o'
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Hint = 'Confima Inclus'#227'o / altera'#231#227'o'
        Caption = '&Confirma'
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        OnClick = BtConfirmaClick
      end
      object BtAtzSdos: TBitBtn
        Tag = 18
        Left = 136
        Top = 5
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Hint = 'Confima Inclus'#227'o / altera'#231#227'o'
        Caption = '&Atz Saldos'
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 2
        OnClick = BtAtzSdosClick
      end
    end
  end
  object QrFornece: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT DISTINCT fo.Codigo,'
      'CASE WHEN fo.Tipo=0 THEN fo.RazaoSocial'
      'ELSE fo.Nome END NOMEENTIDADE'
      'FROM entidades fo, PQPed pp'
      'WHERE pp.Fornece=fo.Codigo'
      'AND pp.Sit <:P0'
      'AND fo.Fornece2='#39'V'#39
      'ORDER BY NOMEENTIDADE')
    Left = 32
    Top = 172
    ParamData = <
      item
        DataType = ftInteger
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrForneceCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrForneceNOMEENTIDADE: TWideStringField
      FieldName = 'NOMEENTIDADE'
      FixedChar = True
      Size = 128
    end
  end
  object DsFornece: TDataSource
    DataSet = QrFornece
    Left = 28
    Top = 220
  end
  object QrSoliComprCab: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT pvd.Codigo, pvd.CodUsu, pvd.Cliente, '
      'pvd.DtaEntra, pvd.DtaPrevi, pvd.Observa, '
      'pvd.QuantP, pvd.ValLiq, pvd.ReferenPedi,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_ENT '
      'FROM pedivda pvd'
      'LEFT JOIN entidades ent ON ent.Codigo=pvd.Cliente'
      'WHERE pvd.EntSai=0'
      'AND pvd.CodUsu=2'
      'OR ReferenPedi LIKE "%74%"')
    Left = 292
    Top = 208
    object QrSoliComprCabCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrSoliComprCabCodUsu: TIntegerField
      FieldName = 'CodUsu'
      Required = True
    end
    object QrSoliComprCabCliente: TIntegerField
      FieldName = 'Cliente'
      Required = True
    end
    object QrSoliComprCabDtaEntra: TDateField
      FieldName = 'DtaEntra'
      Required = True
    end
    object QrSoliComprCabDtaPrevi: TDateField
      FieldName = 'DtaPrevi'
      Required = True
    end
    object QrSoliComprCabObserva: TWideMemoField
      FieldName = 'Observa'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrSoliComprCabQuantS: TFloatField
      FieldName = 'QuantS'
      Required = True
    end
    object QrSoliComprCabValLiq: TFloatField
      FieldName = 'ValLiq'
      Required = True
    end
    object QrSoliComprCabReferenPedi: TWideStringField
      FieldName = 'ReferenPedi'
      Size = 60
    end
    object QrSoliComprCabNO_ENT: TWideStringField
      FieldName = 'NO_ENT'
      Size = 100
    end
    object QrSoliComprCabCentroCusto: TIntegerField
      FieldName = 'CentroCusto'
    end
  end
  object DsSoliComprCab: TDataSource
    DataSet = QrSoliComprCab
    Left = 292
    Top = 260
  end
  object QrSoliComprGru: TMySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrSoliComprGruAfterOpen
    BeforeClose = QrSoliComprGruBeforeClose
    SQL.Strings = (
      'SELECT pvi.Controle, SUM(QuantP) QuantP, SUM(ValLiq) ValLiq, '
      'SUM(Customizad) ItensCustomizados,'
      'gti.Codigo GRATAMCAD,'
      'gg1.CodUsu, gg1.Nome, gg1.Nivel1, pgt.Fracio'
      'FROM pedivdaits pvi'
      'LEFT JOIN gragrux   ggx ON ggx.Controle=pvi.GraGruX'
      'LEFT JOIN gratamits gti ON gti.Controle=ggx.GraTamI'
      'LEFT JOIN gragru1   gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN prdgruptip   pgt ON pgt.Codigo=gg1.PrdGrupTip'
      'WHERE pvi.Codigo=:P0'
      'GROUP BY gg1.Nivel1'
      'ORDER BY pci.Controle')
    Left = 392
    Top = 208
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSoliComprGruCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrSoliComprGruNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
    object QrSoliComprGruNivel1: TIntegerField
      FieldName = 'Nivel1'
    end
    object QrSoliComprGruGRATAMCAD: TIntegerField
      FieldName = 'GRATAMCAD'
    end
    object QrSoliComprGruQuantS: TFloatField
      FieldName = 'QuantS'
    end
    object QrSoliComprGruValLiq: TFloatField
      FieldName = 'ValLiq'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrSoliComprGruItensCustomizados: TFloatField
      FieldName = 'ItensCustomizados'
    end
    object QrSoliComprGruFracio: TSmallintField
      FieldName = 'Fracio'
    end
    object QrSoliComprGruControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrSoliComprGruValBru: TFloatField
      FieldName = 'ValBru'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrSoliComprGruPrecoF: TFloatField
      FieldName = 'PrecoF'
      DisplayFormat = '#,###,###,##0.000000'
    end
    object QrSoliComprGruSaldoQtd: TFloatField
      FieldName = 'SaldoQtd'
    end
  end
  object DsSoliComprGru: TDataSource
    DataSet = QrSoliComprGru
    Left = 392
    Top = 260
  end
  object QrSoliComprIts: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM pedivdaits')
    Left = 492
    Top = 205
    object QrSoliComprItsCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrSoliComprItsControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrSoliComprItsGraGruX: TIntegerField
      FieldName = 'GraGruX'
      Required = True
    end
    object QrSoliComprItsPrecoO: TFloatField
      FieldName = 'PrecoO'
      Required = True
    end
    object QrSoliComprItsPrecoR: TFloatField
      FieldName = 'PrecoR'
      Required = True
    end
    object QrSoliComprItsQuantS: TFloatField
      FieldName = 'QuantS'
      Required = True
    end
    object QrSoliComprItsQuantC: TFloatField
      FieldName = 'QuantC'
      Required = True
    end
    object QrSoliComprItsQuantP: TFloatField
      FieldName = 'QuantP'
      Required = True
    end
    object QrSoliComprItsValBru: TFloatField
      FieldName = 'ValBru'
      Required = True
    end
    object QrSoliComprItsDescoP: TFloatField
      FieldName = 'DescoP'
      Required = True
    end
    object QrSoliComprItsDescoV: TFloatField
      FieldName = 'DescoV'
      Required = True
    end
    object QrSoliComprItsValLiq: TFloatField
      FieldName = 'ValLiq'
      Required = True
    end
    object QrSoliComprItsPrecoF: TFloatField
      FieldName = 'PrecoF'
      Required = True
    end
    object QrSoliComprItsMedidaC: TFloatField
      FieldName = 'MedidaC'
      Required = True
    end
    object QrSoliComprItsMedidaL: TFloatField
      FieldName = 'MedidaL'
      Required = True
    end
    object QrSoliComprItsMedidaA: TFloatField
      FieldName = 'MedidaA'
      Required = True
    end
    object QrSoliComprItsMedidaE: TFloatField
      FieldName = 'MedidaE'
      Required = True
    end
    object QrSoliComprItsPercCustom: TFloatField
      FieldName = 'PercCustom'
      Required = True
    end
    object QrSoliComprItsCustomizad: TSmallintField
      FieldName = 'Customizad'
      Required = True
    end
    object QrSoliComprItsInfAdCuztm: TIntegerField
      FieldName = 'InfAdCuztm'
      Required = True
    end
    object QrSoliComprItsOrdem: TIntegerField
      FieldName = 'Ordem'
      Required = True
    end
    object QrSoliComprItsReferencia: TWideStringField
      FieldName = 'Referencia'
      Size = 11
    end
    object QrSoliComprItsMulti: TIntegerField
      FieldName = 'Multi'
      Required = True
    end
    object QrSoliComprItsvProd: TFloatField
      FieldName = 'vProd'
      Required = True
    end
    object QrSoliComprItsvFrete: TFloatField
      FieldName = 'vFrete'
      Required = True
    end
    object QrSoliComprItsvSeg: TFloatField
      FieldName = 'vSeg'
      Required = True
    end
    object QrSoliComprItsvOutro: TFloatField
      FieldName = 'vOutro'
      Required = True
    end
    object QrSoliComprItsvDesc: TFloatField
      FieldName = 'vDesc'
      Required = True
    end
    object QrSoliComprItsvBC: TFloatField
      FieldName = 'vBC'
      Required = True
    end
  end
end
