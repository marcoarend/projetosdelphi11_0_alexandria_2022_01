object FmPediVda: TFmPediVda
  Left = 368
  Top = 194
  Caption = 'PED-VENDA-001 :: Pedidos de Compra e Venda'
  ClientHeight = 763
  ClientWidth = 1008
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelDados: TPanel
    Left = 0
    Top = 96
    Width = 1008
    Height = 667
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    object GroupBox9: TGroupBox
      Left = 0
      Top = 0
      Width = 1008
      Height = 96
      Align = alTop
      TabOrder = 0
      object Label56: TLabel
        Left = 8
        Top = 20
        Width = 44
        Height = 13
        Caption = 'Empresa:'
      end
      object Label57: TLabel
        Left = 8
        Top = 44
        Width = 59
        Height = 13
        Caption = 'Destinat'#225'rio:'
      end
      object Label58: TLabel
        Left = 8
        Top = 72
        Width = 95
        Height = 13
        Caption = 'Situa'#231#227'o do pedido:'
      end
      object Label59: TLabel
        Left = 280
        Top = 72
        Width = 93
        Height = 13
        Caption = 'Motivo da situa'#231#227'o:'
      end
      object Label60: TLabel
        Left = 736
        Top = 72
        Width = 68
        Height = 13
        Caption = 'Data inclus'#227'o:'
        Enabled = False
      end
      object Label61: TLabel
        Left = 920
        Top = 72
        Width = 14
        Height = 13
        Caption = 'ID:'
        Enabled = False
      end
      object Label62: TLabel
        Left = 852
        Top = 20
        Width = 43
        Height = 13
        Caption = 'N'#250'mero: '
      end
      object DBEdit1: TDBEdit
        Left = 916
        Top = 12
        Width = 80
        Height = 27
        DataField = 'CodUsu'
        DataSource = DsPediVda
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 0
      end
      object DBEdit3: TDBEdit
        Left = 73
        Top = 16
        Width = 53
        Height = 21
        DataField = 'SHOW_COD_FILIAL'
        DataSource = DsPediVda
        TabOrder = 1
      end
      object DBEdit2: TDBEdit
        Left = 128
        Top = 16
        Width = 719
        Height = 21
        DataField = 'SHOW_TXT_FILIAL'
        DataSource = DsPediVda
        TabOrder = 2
      end
      object DBEdit4: TDBEdit
        Left = 73
        Top = 40
        Width = 53
        Height = 21
        DataField = 'SHOW_COD_CLIFOR'
        DataSource = DsPediVda
        TabOrder = 3
      end
      object DBEdit5: TDBEdit
        Left = 128
        Top = 40
        Width = 516
        Height = 21
        DataField = 'SHOW_TXT_CLIFOR'
        DataSource = DsPediVda
        TabOrder = 4
      end
      object DBEdit30: TDBEdit
        Left = 648
        Top = 40
        Width = 313
        Height = 21
        DataField = 'CIDADECLI'
        DataSource = DsPediVda
        Enabled = False
        TabOrder = 5
      end
      object DBEdit31: TDBEdit
        Left = 964
        Top = 40
        Width = 32
        Height = 21
        DataField = 'NOMEUF'
        DataSource = DsPediVda
        Enabled = False
        TabOrder = 6
      end
      object DBEdit20: TDBEdit
        Left = 108
        Top = 68
        Width = 21
        Height = 21
        DataField = 'Situacao'
        DataSource = DsPediVda
        TabOrder = 7
      end
      object DBEdit29: TDBEdit
        Left = 132
        Top = 68
        Width = 145
        Height = 21
        DataField = 'NOMESITUACAO'
        DataSource = DsPediVda
        TabOrder = 8
      end
      object DBEdit8: TDBEdit
        Left = 808
        Top = 68
        Width = 105
        Height = 21
        DataField = 'DtaInclu'
        DataSource = DsPediVda
        TabOrder = 9
      end
      object DBEdit22: TDBEdit
        Left = 936
        Top = 68
        Width = 60
        Height = 21
        DataField = 'Codigo'
        DataSource = DsPediVda
        TabOrder = 10
      end
      object DBEdit27: TDBEdit
        Left = 380
        Top = 68
        Width = 56
        Height = 21
        DataField = 'CODUSU_MOT'
        DataSource = DsPediVda
        TabOrder = 11
      end
      object DBEdit32: TDBEdit
        Left = 440
        Top = 68
        Width = 293
        Height = 21
        DataField = 'NOMEMOTIVO'
        DataSource = DsPediVda
        TabOrder = 12
      end
    end
    object PageControl1: TPageControl
      Left = 0
      Top = 132
      Width = 1008
      Height = 437
      ActivePage = TabSheet11
      Align = alTop
      TabOrder = 1
      OnChange = PageControl1Change
      object TabSheet1: TTabSheet
        Caption = ' Dados do pedido '
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object Panel4: TPanel
          Left = 0
          Top = 0
          Width = 1000
          Height = 409
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          object GroupBox10: TGroupBox
            Left = 0
            Top = 0
            Width = 1000
            Height = 36
            Align = alTop
            TabOrder = 0
            object Label54: TLabel
              Left = 8
              Top = 12
              Width = 67
              Height = 13
              Caption = 'Data emiss'#227'o:'
            end
            object Label55: TLabel
              Left = 188
              Top = 12
              Width = 71
              Height = 13
              Caption = 'Data chegada:'
            end
            object Label63: TLabel
              Left = 404
              Top = 12
              Width = 70
              Height = 13
              Caption = 'Pedido cliente:'
            end
            object Label64: TLabel
              Left = 636
              Top = 12
              Width = 50
              Height = 13
              Caption = 'Prioridade:'
            end
            object Label65: TLabel
              Left = 752
              Top = 12
              Width = 101
              Height = 13
              Caption = 'Previs'#227'o entrega:'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsBold]
              ParentFont = False
            end
            object DBEdit9: TDBEdit
              Left = 80
              Top = 7
              Width = 105
              Height = 21
              DataField = 'DtaEmiss'
              DataSource = DsPediVda
              TabOrder = 0
            end
            object DBEdit10: TDBEdit
              Left = 264
              Top = 7
              Width = 105
              Height = 21
              DataField = 'DtaEntra'
              DataSource = DsPediVda
              TabOrder = 1
            end
            object DBEdit12: TDBEdit
              Left = 690
              Top = 7
              Width = 53
              Height = 21
              DataField = 'Prioridade'
              DataSource = DsPediVda
              TabOrder = 2
            end
            object DBEdit11: TDBEdit
              Left = 856
              Top = 7
              Width = 129
              Height = 21
              DataField = 'DtaPrevi'
              DataSource = DsPediVda
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsBold]
              ParentFont = False
              TabOrder = 3
            end
            object DBEdit19: TDBEdit
              Left = 480
              Top = 7
              Width = 145
              Height = 21
              DataField = 'PedidoCli'
              DataSource = DsPediVda
              TabOrder = 4
            end
          end
          object GroupBox11: TGroupBox
            Left = 0
            Top = 36
            Width = 1000
            Height = 60
            Align = alTop
            TabOrder = 1
            object Label1: TLabel
              Left = 8
              Top = 13
              Width = 81
              Height = 13
              Caption = 'Tabela de pre'#231'o:'
            end
            object Label3: TLabel
              Left = 8
              Top = 37
              Width = 119
              Height = 13
              Caption = 'Condi'#231#227'o de pagamento:'
            end
            object Label5: TLabel
              Left = 530
              Top = 36
              Width = 39
              Height = 13
              Caption = 'Carteira:'
            end
            object Label10: TLabel
              Left = 714
              Top = 13
              Width = 36
              Height = 13
              Caption = 'Moeda:'
            end
            object DBEdit21: TDBEdit
              Left = 128
              Top = 8
              Width = 56
              Height = 21
              DataField = 'CODUSU_TPC'
              DataSource = DsPediVda
              TabOrder = 0
            end
            object DBEdit24: TDBEdit
              Left = 188
              Top = 8
              Width = 517
              Height = 21
              DataField = 'NOMETABEPRCCAD'
              DataSource = DsPediVda
              TabOrder = 1
            end
            object DBEdit18: TDBEdit
              Left = 758
              Top = 8
              Width = 44
              Height = 21
              DataField = 'CODUSU_MDA'
              DataSource = DsPediVda
              TabOrder = 2
            end
            object DBEdit25: TDBEdit
              Left = 804
              Top = 8
              Width = 181
              Height = 21
              DataField = 'NOMEMOEDA'
              DataSource = DsPediVda
              TabOrder = 3
            end
            object DBEdit17: TDBEdit
              Left = 128
              Top = 32
              Width = 56
              Height = 21
              DataField = 'CODUSU_PPC'
              DataSource = DsPediVda
              TabOrder = 4
            end
            object DBEdit26: TDBEdit
              Left = 188
              Top = 32
              Width = 333
              Height = 21
              DataField = 'NOMECONDICAOPG'
              DataSource = DsPediVda
              TabOrder = 5
            end
            object DBEdit6: TDBEdit
              Left = 574
              Top = 32
              Width = 44
              Height = 21
              DataField = 'CartEmis'
              DataSource = DsPediVda
              TabOrder = 6
            end
            object DBEdit7: TDBEdit
              Left = 620
              Top = 32
              Width = 365
              Height = 21
              DataField = 'NOMECARTEMIS'
              DataSource = DsPediVda
              TabOrder = 7
            end
          end
          object GroupBox8: TGroupBox
            Left = 0
            Top = 96
            Width = 1000
            Height = 36
            Align = alTop
            TabOrder = 2
            object Label11: TLabel
              Left = 8
              Top = 12
              Width = 87
              Height = 13
              Caption = 'Lote de produ'#231#227'o:'
              FocusControl = DBEdit33
            end
            object DBEdit33: TDBEdit
              Left = 100
              Top = 8
              Width = 92
              Height = 21
              TabStop = False
              Color = clGradientInactiveCaption
              DataField = 'LoteProd'
              DataSource = DsPediVda
              TabOrder = 0
            end
          end
          object GroupBox14: TGroupBox
            Left = 0
            Top = 132
            Width = 1000
            Height = 106
            Align = alTop
            TabOrder = 3
            object Label51: TLabel
              Left = 8
              Top = 13
              Width = 45
              Height = 13
              Caption = 'Frete por:'
            end
            object Label52: TLabel
              Left = 8
              Top = 36
              Width = 75
              Height = 13
              Caption = 'Transportadora:'
            end
            object Label53: TLabel
              Left = 8
              Top = 61
              Width = 64
              Height = 13
              Caption = 'Redespacho:'
            end
            object Label66: TLabel
              Left = 8
              Top = 84
              Width = 117
              Height = 13
              Caption = '% Despesas acess'#243'rias: '
            end
            object Label67: TLabel
              Left = 176
              Top = 84
              Width = 115
              Height = 13
              Caption = '$ Despesas acess'#243'rias: '
            end
            object Label68: TLabel
              Left = 452
              Top = 84
              Width = 38
              Height = 13
              Caption = '% Frete:'
            end
            object Label69: TLabel
              Left = 456
              Top = 13
              Width = 92
              Height = 13
              Caption = 'Local de entrega: >'
            end
            object Label70: TLabel
              Left = 552
              Top = 84
              Width = 36
              Height = 13
              Caption = '$ Frete:'
            end
            object Label71: TLabel
              Left = 736
              Top = 84
              Width = 48
              Height = 13
              Caption = '% Seguro:'
            end
            object Label72: TLabel
              Left = 844
              Top = 84
              Width = 46
              Height = 13
              Caption = '$ Seguro:'
            end
            object DBEdit16: TDBEdit
              Left = 56
              Top = 8
              Width = 21
              Height = 21
              DataField = 'FretePor'
              DataSource = DsPediVda
              TabOrder = 0
            end
            object DBEdit23: TDBEdit
              Left = 80
              Top = 8
              Width = 373
              Height = 21
              DataField = 'NOMEFRETEPOR'
              DataSource = DsPediVda
              TabOrder = 1
            end
            object Memo2: TMemo
              Left = 552
              Top = 8
              Width = 433
              Height = 69
              TabStop = False
              ReadOnly = True
              TabOrder = 2
            end
            object DBEdit34: TDBEdit
              Left = 88
              Top = 32
              Width = 56
              Height = 21
              DataField = 'CODUSU_TRA'
              DataSource = DsPediVda
              TabOrder = 3
            end
            object DBEdit35: TDBEdit
              Left = 88
              Top = 56
              Width = 56
              Height = 21
              DataField = 'CODUSU_RED'
              DataSource = DsPediVda
              TabOrder = 4
            end
            object DBEdit36: TDBEdit
              Left = 148
              Top = 32
              Width = 397
              Height = 21
              DataField = 'NOMETRANSP'
              DataSource = DsPediVda
              TabOrder = 5
            end
            object DBEdit37: TDBEdit
              Left = 148
              Top = 56
              Width = 397
              Height = 21
              DataField = 'NOMEREDESP'
              DataSource = DsPediVda
              TabOrder = 6
            end
            object DBEdit38: TDBEdit
              Left = 300
              Top = 80
              Width = 80
              Height = 21
              DataField = 'DesoAces_V'
              DataSource = DsPediVda
              TabOrder = 7
            end
            object DBEdit39: TDBEdit
              Left = 124
              Top = 80
              Width = 48
              Height = 21
              DataField = 'DesoAces_P'
              DataSource = DsPediVda
              TabOrder = 8
            end
            object DBEdit40: TDBEdit
              Left = 600
              Top = 80
              Width = 80
              Height = 21
              DataField = 'Frete_V'
              DataSource = DsPediVda
              TabOrder = 9
            end
            object DBEdit41: TDBEdit
              Left = 496
              Top = 80
              Width = 48
              Height = 21
              DataField = 'Frete_P'
              DataSource = DsPediVda
              TabOrder = 10
            end
            object DBEdit42: TDBEdit
              Left = 904
              Top = 80
              Width = 80
              Height = 21
              DataField = 'Seguro_V'
              DataSource = DsPediVda
              TabOrder = 11
            end
            object DBEdit43: TDBEdit
              Left = 788
              Top = 80
              Width = 48
              Height = 21
              DataField = 'Seguro_P'
              DataSource = DsPediVda
              TabOrder = 12
            end
          end
          object GroupBox12: TGroupBox
            Left = 0
            Top = 238
            Width = 1000
            Height = 40
            Align = alTop
            TabOrder = 4
            object Label16: TLabel
              Left = 12
              Top = 16
              Width = 73
              Height = 13
              Caption = 'Representante:'
            end
            object Label17: TLabel
              Left = 640
              Top = 15
              Width = 117
              Height = 13
              Caption = '% comiss'#227'o faturamento:'
            end
            object Label18: TLabel
              Left = 816
              Top = 15
              Width = 119
              Height = 13
              Caption = '% comiss'#227'o recebimento:'
            end
            object DBEdit44: TDBEdit
              Left = 88
              Top = 12
              Width = 57
              Height = 21
              DataField = 'CODUSU_ACC'
              DataSource = DsPediVda
              TabOrder = 0
            end
            object DBEdit45: TDBEdit
              Left = 148
              Top = 12
              Width = 489
              Height = 21
              DataField = 'NOMEACC'
              DataSource = DsPediVda
              TabOrder = 1
            end
            object DBEdit46: TDBEdit
              Left = 760
              Top = 12
              Width = 48
              Height = 21
              DataField = 'ComisFat'
              DataSource = DsPediVda
              TabOrder = 2
            end
            object DBEdit47: TDBEdit
              Left = 936
              Top = 12
              Width = 48
              Height = 21
              DataField = 'ComisRec'
              DataSource = DsPediVda
              TabOrder = 3
            end
          end
          object GroupBox15: TGroupBox
            Left = 0
            Top = 278
            Width = 1000
            Height = 131
            Align = alTop
            Caption = ' Fiscal:'
            TabOrder = 5
            object Label34: TLabel
              Left = 12
              Top = 20
              Width = 73
              Height = 13
              Caption = 'Movimenta'#231#227'o:'
            end
            object Label35: TLabel
              Left = 640
              Top = 20
              Width = 55
              Height = 13
              Caption = 'Modelo NF:'
            end
            object Label37: TLabel
              Left = 12
              Top = 88
              Width = 518
              Height = 13
              Caption = 
                'Indicador de presen'#231'a do comprador no estabelecimento comercial ' +
                'no momento da opera'#231#227'o (NFe 3.10): [F4]'
            end
            object Label38: TLabel
              Left = 624
              Top = 88
              Width = 148
              Height = 13
              Caption = 'Finalidade de emiss'#227'o da NF-e:'
              FocusControl = DBEdfinNFe
            end
            object DBEdit48: TDBEdit
              Left = 88
              Top = 16
              Width = 57
              Height = 21
              DataField = 'CODUSU_FRC'
              DataSource = DsPediVda
              TabOrder = 0
            end
            object DBEdit49: TDBEdit
              Left = 148
              Top = 16
              Width = 489
              Height = 21
              DataField = 'NOMEFISREGCAD'
              DataSource = DsPediVda
              TabOrder = 1
            end
            object DBEdit50: TDBEdit
              Left = 700
              Top = 16
              Width = 283
              Height = 21
              DataField = 'NOMEMODELONF'
              DataSource = DsPediVda
              TabOrder = 2
            end
            object DBRGIndDest: TDBRadioGroup
              Left = 12
              Top = 42
              Width = 689
              Height = 39
              Caption = ' Local de Destino da Opera'#231#227'o: (v'#225'lida a partir da NFe 3.10) '
              Columns = 4
              DataField = 'idDest'
              DataSource = DsPediVda
              Items.Strings = (
                '0 - N'#227'o definido'
                '1 - Opera'#231#227'o interna'
                '2 - Opera'#231#227'o interestadual'
                '3 - Opera'#231#227'o com exterior')
              TabOrder = 3
              Values.Strings = (
                '0'
                '1'
                '2'
                '3'
                '4'
                '5'
                '6'
                '7'
                '8'
                '9')
            end
            object RGDBIndFinal: TDBRadioGroup
              Left = 704
              Top = 42
              Width = 280
              Height = 39
              Caption = ' Opera'#231#227'o com consumidor (NFe 3.10): '
              Columns = 2
              DataField = 'indFinal'
              DataSource = DsPediVda
              Items.Strings = (
                '0 - Normal'
                '1 - Consumidor final')
              TabOrder = 4
              Values.Strings = (
                '0'
                '1'
                '2'
                '3'
                '4'
                '5'
                '6'
                '7'
                '8'
                '9')
            end
            object DBEdindPres: TDBEdit
              Left = 12
              Top = 104
              Width = 29
              Height = 21
              DataField = 'indPres'
              DataSource = DsPediVda
              TabOrder = 5
              OnChange = DBEdindPresChange
            end
            object EdDBindPres_TXT: TEdit
              Left = 41
              Top = 104
              Width = 578
              Height = 21
              ReadOnly = True
              TabOrder = 6
            end
            object DBRadioGroup1: TDBRadioGroup
              Left = 780
              Top = 90
              Width = 204
              Height = 39
              Caption = ' Envio da NF-e ao Fisco (NFe 3.10):'
              Columns = 2
              DataField = 'indSinc'
              DataSource = DsPediVda
              Items.Strings = (
                'Ass'#237'ncrono'
                'S'#237'ncrono')
              TabOrder = 8
              Values.Strings = (
                '0'
                '1'
                '2'
                '3'
                '4'
                '5'
                '6'
                '7'
                '8'
                '9')
            end
            object DBEdfinNFe: TDBEdit
              Left = 624
              Top = 104
              Width = 21
              Height = 21
              DataField = 'finNFe'
              DataSource = DsPediVda
              TabOrder = 7
              OnChange = DBEdfinNFeChange
            end
            object EdDBfinNFe_TXT: TEdit
              Left = 645
              Top = 104
              Width = 130
              Height = 21
              ReadOnly = True
              TabOrder = 9
            end
          end
        end
      end
      object TabSheet2: TTabSheet
        Caption = ' Itens do pedido '
        ImageIndex = 1
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object DBGGru: TdmkDBGrid
          Left = 0
          Top = 0
          Width = 519
          Height = 409
          Align = alLeft
          Columns = <
            item
              Expanded = False
              FieldName = 'CodUsu'
              Title.Caption = 'C'#243'digo'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Nome'
              Title.Caption = 'Descri'#231#227'o'
              Width = 220
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'QuantP'
              Title.Caption = 'Qtd pedido'
              Width = 58
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ValLiq'
              Title.Caption = 'Val. liq.'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Controle'
              Title.Caption = 'ID'
              Visible = True
            end>
          Color = clWindow
          DataSource = DsPediVdaGru
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -12
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'CodUsu'
              Title.Caption = 'C'#243'digo'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Nome'
              Title.Caption = 'Descri'#231#227'o'
              Width = 220
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'QuantP'
              Title.Caption = 'Qtd pedido'
              Width = 58
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ValLiq'
              Title.Caption = 'Val. liq.'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Controle'
              Title.Caption = 'ID'
              Visible = True
            end>
        end
        object PageControl3: TPageControl
          Left = 519
          Top = 0
          Width = 481
          Height = 409
          ActivePage = TabSheet5
          Align = alClient
          MultiLine = True
          TabOrder = 1
          object TabSheet5: TTabSheet
            Caption = ' Quantidade '
            ImageIndex = 2
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object GradeQ: TStringGrid
              Left = 0
              Top = 0
              Width = 473
              Height = 364
              Align = alClient
              ColCount = 2
              DefaultColWidth = 65
              DefaultRowHeight = 18
              FixedCols = 0
              RowCount = 2
              FixedRows = 0
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goTabs]
              ParentFont = False
              TabOrder = 0
              OnDblClick = GradeQDblClick
              OnDrawCell = GradeQDrawCell
            end
            object StaticText1: TStaticText
              Left = 0
              Top = 364
              Width = 507
              Height = 17
              Align = alBottom
              Alignment = taCenter
              BorderStyle = sbsSunken
              Caption = 
                'D'#234' um duplo clique na c'#233'lula, coluna ou linha correspondente par' +
                'a alterar a quantidade.'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlue
              Font.Height = -12
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsBold]
              ParentFont = False
              TabOrder = 1
              Visible = False
            end
          end
          object TabSheet4: TTabSheet
            Caption = ' Valor unit'#225'rio '
            ImageIndex = 5
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object GradeF: TStringGrid
              Left = 0
              Top = 0
              Width = 476
              Height = 387
              Align = alClient
              ColCount = 2
              DefaultColWidth = 100
              DefaultRowHeight = 18
              FixedCols = 0
              RowCount = 2
              FixedRows = 0
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goTabs]
              ParentFont = False
              TabOrder = 0
              OnDrawCell = GradeFDrawCell
            end
          end
          object TabSheet6: TTabSheet
            Caption = ' Desconto '
            ImageIndex = 6
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object GradeD: TStringGrid
              Left = 0
              Top = 0
              Width = 476
              Height = 371
              Align = alClient
              ColCount = 2
              DefaultColWidth = 100
              DefaultRowHeight = 18
              FixedCols = 0
              RowCount = 2
              FixedRows = 0
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goTabs]
              ParentFont = False
              TabOrder = 0
              OnDrawCell = GradeDDrawCell
            end
            object StaticText3: TStaticText
              Left = 0
              Top = 371
              Width = 544
              Height = 17
              Align = alBottom
              Alignment = taCenter
              BorderStyle = sbsSunken
              Caption = 
                'D'#234' um duplo clique na c'#233'lula, coluna ou linha correspondente par' +
                'a incluir / alterar o desconto.'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlue
              Font.Height = -12
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsBold]
              ParentFont = False
              TabOrder = 1
            end
          end
          object TabSheet7: TTabSheet
            Caption = ' Valor l'#237'quido'
            ImageIndex = 1
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object GradeV: TStringGrid
              Left = 0
              Top = 0
              Width = 476
              Height = 387
              Align = alClient
              ColCount = 2
              DefaultColWidth = 100
              DefaultRowHeight = 18
              FixedCols = 0
              RowCount = 2
              FixedRows = 0
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goTabs]
              ParentFont = False
              TabOrder = 0
              OnDrawCell = GradeVDrawCell
            end
          end
          object TabSheet8: TTabSheet
            Caption = ' C'#243'digos '
            ImageIndex = 3
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object GradeC: TStringGrid
              Left = 0
              Top = 0
              Width = 476
              Height = 371
              Align = alClient
              ColCount = 1
              DefaultColWidth = 65
              DefaultRowHeight = 18
              FixedCols = 0
              RowCount = 1
              FixedRows = 0
              TabOrder = 0
              OnDrawCell = GradeCDrawCell
              RowHeights = (
                18)
            end
            object StaticText6: TStaticText
              Left = 0
              Top = 371
              Width = 502
              Height = 17
              Align = alBottom
              Alignment = taCenter
              BorderStyle = sbsSunken
              Caption = 
                'Para gerar o c'#243'digo, clique na c'#233'lula, coluna ou linha correspon' +
                'dente na guia "Ativos".'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlue
              Font.Height = -12
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsBold]
              ParentFont = False
              TabOrder = 1
              Visible = False
            end
          end
          object TabSheet9: TTabSheet
            Caption = ' Ativos '
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object GradeA: TStringGrid
              Left = 0
              Top = 0
              Width = 476
              Height = 371
              Align = alClient
              ColCount = 2
              DefaultColWidth = 65
              DefaultRowHeight = 18
              FixedCols = 0
              RowCount = 2
              FixedRows = 0
              TabOrder = 0
              OnDrawCell = GradeADrawCell
              RowHeights = (
                18
                18)
            end
            object StaticText2: TStaticText
              Left = 0
              Top = 371
              Width = 475
              Height = 17
              Align = alBottom
              Alignment = taCenter
              BorderStyle = sbsSunken
              Caption = 
                'Clique na c'#233'lula, coluna ou linha correspondente para ativar / d' +
                'esativar o produto.'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlue
              Font.Height = -12
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsBold]
              ParentFont = False
              TabOrder = 1
              Visible = False
            end
          end
          object TabSheet10: TTabSheet
            Caption = ' X '
            ImageIndex = 6
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object GradeX: TStringGrid
              Left = 0
              Top = 0
              Width = 476
              Height = 387
              Align = alClient
              ColCount = 2
              DefaultColWidth = 65
              DefaultRowHeight = 18
              FixedCols = 0
              RowCount = 2
              FixedRows = 0
              TabOrder = 0
              OnDrawCell = GradeXDrawCell
              RowHeights = (
                18
                18)
            end
          end
        end
      end
      object TabSheet11: TTabSheet
        Caption = ' Dados dos itens customizados '
        ImageIndex = 2
        object Panel7: TPanel
          Left = 0
          Top = 0
          Width = 1000
          Height = 409
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          object Panel8: TPanel
            Left = 0
            Top = 0
            Width = 1000
            Height = 173
            Align = alTop
            BevelOuter = bvNone
            TabOrder = 0
            object Splitter1: TSplitter
              Left = 417
              Top = 0
              Width = 5
              Height = 173
            end
            object dmkDBGrid1: TdmkDBGrid
              Left = 0
              Top = 0
              Width = 417
              Height = 173
              Align = alLeft
              Columns = <
                item
                  Expanded = False
                  FieldName = 'CodUsu'
                  Title.Caption = 'C'#243'digo'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Nome'
                  Title.Caption = 'Descri'#231#227'o'
                  Width = 191
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'QuantP'
                  Title.Caption = 'Qtd pedido'
                  Width = 58
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'ValLiq'
                  Title.Caption = 'Val. liq.'
                  Visible = True
                end>
              Color = clWindow
              DataSource = DsPediVdaGru
              Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
              TabOrder = 0
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -12
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              Columns = <
                item
                  Expanded = False
                  FieldName = 'CodUsu'
                  Title.Caption = 'C'#243'digo'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Nome'
                  Title.Caption = 'Descri'#231#227'o'
                  Width = 191
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'QuantP'
                  Title.Caption = 'Qtd pedido'
                  Width = 58
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'ValLiq'
                  Title.Caption = 'Val. liq.'
                  Visible = True
                end>
            end
            object Panel10: TPanel
              Left = 422
              Top = 0
              Width = 578
              Height = 173
              Align = alClient
              TabOrder = 1
              object DBGrid1: TDBGrid
                Left = 1
                Top = 1
                Width = 576
                Height = 171
                Align = alClient
                DataSource = DsCustomizados
                TabOrder = 0
                TitleFont.Charset = DEFAULT_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -12
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                Columns = <
                  item
                    Expanded = False
                    FieldName = 'NO_COR'
                    Title.Caption = 'Cor'
                    Width = 162
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'NO_TAM'
                    Title.Caption = 'Tamanho'
                    Width = 56
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'MedidaC'
                    Title.Caption = 'Medida 1'
                    Width = 60
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'MedidaL'
                    Title.Caption = 'Medida 2'
                    Width = 60
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'MedidaA'
                    Title.Caption = 'Medida 3'
                    Width = 60
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'MedidaE'
                    Title.Caption = 'Medida 4'
                    Width = 60
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'ValLiq'
                    Title.Caption = 'Valor Liq.'
                    Width = 72
                    Visible = True
                  end>
              end
            end
          end
          object Panel9: TPanel
            Left = 0
            Top = 173
            Width = 1000
            Height = 236
            Align = alClient
            TabOrder = 1
            object DBGrid2: TDBGrid
              Left = 1
              Top = 1
              Width = 998
              Height = 234
              Align = alClient
              DataSource = DsPediVdaCuz
              TabOrder = 0
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -12
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              Columns = <
                item
                  Expanded = False
                  FieldName = 'NO_PARTE'
                  Title.Caption = 'Parte'
                  Width = 150
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NO_GRUPO'
                  Title.Caption = 'Produto'
                  Width = 180
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NO_COR'
                  Title.Caption = 'Cor'
                  Width = 140
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NO_TAM'
                  Title.Caption = 'Tamanho'
                  Width = 52
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'GraGruX'
                  Title.Caption = 'Reduzido'
                  Width = 52
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'QuantP'
                  Title.Caption = 'Quantidade'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'MedidaC_TXT'
                  Title.Caption = 'Medida 1'
                  Width = 68
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'MedidaL_TXT'
                  Title.Caption = 'Medida 2'
                  Width = 68
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'MedidaA_TXT'
                  Title.Caption = 'Medida 3'
                  Width = 68
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'MedidaE_TXT'
                  Title.Caption = 'Medida 4'
                  Width = 68
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'PrecoR'
                  Title.Caption = 'Pre'#231'o'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'DescoP'
                  Title.Caption = '% Desco.'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'DescoV'
                  Title.Caption = '$ Desco.'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'ValBru'
                  Title.Caption = 'Val. Bruto'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'ValLiq'
                  Title.Caption = 'Valor Liq.'
                  Visible = True
                end>
            end
          end
        end
      end
    end
    object GroupBox13: TGroupBox
      Left = 0
      Top = 96
      Width = 1008
      Height = 36
      Align = alTop
      TabOrder = 2
      object Label19: TLabel
        Left = 616
        Top = 12
        Width = 93
        Height = 13
        Caption = 'Quantidade pedida:'
        FocusControl = DBEdit13
      end
      object Label21: TLabel
        Left = 796
        Top = 12
        Width = 112
        Height = 13
        Caption = 'Valor l'#237'quido do pedido:'
        FocusControl = DBEdit14
      end
      object Label4: TLabel
        Left = 12
        Top = 12
        Width = 51
        Height = 13
        Caption = 'Pedido de:'
      end
      object EdDbSAIENT_TXT: TDBText
        Left = 72
        Top = 8
        Width = 105
        Height = 25
        DataField = 'ENTSAI_TXT'
        DataSource = DsPediVda
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -19
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object DBEdit13: TDBEdit
        Left = 712
        Top = 8
        Width = 77
        Height = 21
        DataField = 'QuantP'
        DataSource = DsPediVda
        TabOrder = 0
      end
      object DBEdit14: TDBEdit
        Left = 916
        Top = 8
        Width = 80
        Height = 21
        DataField = 'ValLiq'
        DataSource = DsPediVda
        TabOrder = 1
      end
    end
    object GBCntrl: TGroupBox
      Left = 0
      Top = 603
      Width = 1008
      Height = 64
      Align = alBottom
      TabOrder = 3
      object Panel5: TPanel
        Left = 2
        Top = 15
        Width = 172
        Height = 47
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 174
        Top = 15
        Width = 134
        Height = 47
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel3: TPanel
        Left = 308
        Top = 15
        Width = 698
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object Panel2: TPanel
          Left = 565
          Top = 0
          Width = 133
          Height = 47
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 0
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
        object BtPedidos: TBitBtn
          Tag = 303
          Left = 4
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Pedido'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtPedidosClick
        end
        object BtItens: TBitBtn
          Tag = 10054
          Left = 96
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Itens'
          Enabled = False
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = BtItensClick
        end
        object BtCustom: TBitBtn
          Tag = 10055
          Left = 188
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Customiz.'
          Enabled = False
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = BtCustomClick
        end
        object BtVisual: TBitBtn
          Tag = 338
          Left = 280
          Top = 4
          Width = 90
          Height = 40
          Caption = '&Visual'
          TabOrder = 4
          Visible = False
          OnClick = BtVisualClick
        end
        object BitBtn1: TBitBtn
          Tag = 5
          Left = 372
          Top = 4
          Width = 90
          Height = 40
          Caption = '&Pesquisa'
          TabOrder = 5
          Visible = False
          OnClick = BitBtn1Click
        end
        object BtRecalcula: TBitBtn
          Tag = 18
          Left = 464
          Top = 4
          Width = 90
          Height = 40
          Caption = '&Recalc.'
          TabOrder = 6
          OnClick = BtRecalculaClick
        end
      end
    end
  end
  object PainelEdita: TPanel
    Left = 0
    Top = 96
    Width = 1008
    Height = 667
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    Visible = False
    object GroupBox1: TGroupBox
      Left = 0
      Top = 0
      Width = 1008
      Height = 96
      Align = alTop
      TabOrder = 0
      object Label9: TLabel
        Left = 8
        Top = 20
        Width = 44
        Height = 13
        Caption = 'Empresa:'
      end
      object Label2: TLabel
        Left = 8
        Top = 44
        Width = 59
        Height = 13
        Caption = 'Destinat'#225'rio:'
      end
      object Label8: TLabel
        Left = 852
        Top = 20
        Width = 61
        Height = 13
        Caption = 'N'#250'mero: [F4]'
      end
      object SpeedButton5: TSpeedButton
        Left = 605
        Top = 40
        Width = 20
        Height = 21
        Caption = '...'
        OnClick = SpeedButton5Click
      end
      object Label23: TLabel
        Left = 8
        Top = 72
        Width = 95
        Height = 13
        Caption = 'Situa'#231#227'o do pedido:'
      end
      object Label26: TLabel
        Left = 280
        Top = 72
        Width = 93
        Height = 13
        Caption = 'Motivo da situa'#231#227'o:'
      end
      object Label7: TLabel
        Left = 920
        Top = 72
        Width = 14
        Height = 13
        Caption = 'ID:'
        Enabled = False
      end
      object Label6: TLabel
        Left = 736
        Top = 72
        Width = 68
        Height = 13
        Caption = 'Data inclus'#227'o:'
        Enabled = False
      end
      object SpeedButton7: TSpeedButton
        Left = 712
        Top = 67
        Width = 21
        Height = 21
        Caption = '...'
        OnClick = SpeedButton7Click
      end
      object SpeedButton8: TSpeedButton
        Left = 626
        Top = 40
        Width = 20
        Height = 21
        Caption = '!'
        OnClick = SpeedButton8Click
      end
      object EdCliente: TdmkEditCB
        Left = 72
        Top = 40
        Width = 52
        Height = 21
        Alignment = taRightJustify
        TabOrder = 3
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Cliente'
        UpdCampo = 'Cliente'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = EdClienteChange
        DBLookupComboBox = CBCliente
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object EdEmpresa: TdmkEditCB
        Left = 72
        Top = 16
        Width = 52
        Height = 21
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = EdEmpresaChange
        DBLookupComboBox = CBEmpresa
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBEmpresa: TdmkDBLookupComboBox
        Left = 127
        Top = 16
        Width = 719
        Height = 21
        KeyField = 'Filial'
        ListField = 'NOMEFILIAL'
        TabOrder = 1
        dmkEditCB = EdEmpresa
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object CBCliente: TdmkDBLookupComboBox
        Left = 127
        Top = 40
        Width = 475
        Height = 21
        KeyField = 'Codigo'
        ListField = 'NOMEENT'
        TabOrder = 4
        dmkEditCB = EdCliente
        QryCampo = 'Cliente'
        UpdType = utNil
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object DBEdCidade: TDBEdit
        Left = 648
        Top = 40
        Width = 313
        Height = 21
        DataField = 'CIDADE'
        Enabled = False
        TabOrder = 5
      end
      object EdCodUsu: TdmkEdit
        Left = 916
        Top = 12
        Width = 80
        Height = 25
        Alignment = taRightJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 2
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 6
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '000000'
        QryCampo = 'CodUsu'
        UpdCampo = 'CodUsu'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnKeyDown = EdCodUsuKeyDown
      end
      object DBEdNOMEUF: TDBEdit
        Left = 964
        Top = 40
        Width = 32
        Height = 21
        DataField = 'NOMEUF'
        Enabled = False
        TabOrder = 6
      end
      object EdSituacao: TdmkEditCB
        Left = 112
        Top = 67
        Width = 21
        Height = 21
        Alignment = taRightJustify
        TabOrder = 7
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Situacao'
        UpdCampo = 'Situacao'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBSituacao
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBSituacao: TdmkDBLookupComboBox
        Left = 136
        Top = 67
        Width = 141
        Height = 21
        KeyField = 'Codigo'
        ListField = 'Nome'
        TabOrder = 8
        dmkEditCB = EdSituacao
        QryCampo = 'Situacao'
        UpdType = utNil
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object EdMotivoSit: TdmkEditCB
        Left = 380
        Top = 67
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 9
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBMotivoSit
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBMotivoSit: TdmkDBLookupComboBox
        Left = 440
        Top = 67
        Width = 269
        Height = 21
        KeyField = 'CodUsu'
        ListField = 'Nome'
        TabOrder = 10
        OnExit = CBMotivoSitExit
        dmkEditCB = EdMotivoSit
        UpdType = utNil
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object EdCodigo: TdmkEdit
        Left = 940
        Top = 68
        Width = 56
        Height = 21
        TabStop = False
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 12
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Codigo'
        UpdCampo = 'Codigo'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object TPDtaInclu: TdmkEditDateTimePicker
        Left = 808
        Top = 68
        Width = 105
        Height = 21
        Date = 39789.688972615740000000
        Time = 39789.688972615740000000
        Enabled = False
        TabOrder = 11
        TabStop = False
        ReadOnly = False
        DefaultEditMask = '!99/99/99;1;_'
        AutoApplyEditMask = True
        QryCampo = 'DtaInclu'
        UpdCampo = 'DtaInclu'
        UpdType = utYes
        DatePurpose = dmkdpNone
      end
    end
    object PageControl2: TPageControl
      Left = 0
      Top = 96
      Width = 1008
      Height = 461
      ActivePage = TabSheet3
      Align = alTop
      TabOrder = 1
      object TabSheet3: TTabSheet
        Caption = ' Dados do Pedido: '
        object Panel6: TPanel
          Left = 0
          Top = 0
          Width = 1000
          Height = 433
          Align = alClient
          ParentBackground = False
          TabOrder = 0
          object GroupBox2: TGroupBox
            Left = 1
            Top = 1
            Width = 998
            Height = 36
            Align = alTop
            TabOrder = 0
            object Label12: TLabel
              Left = 8
              Top = 12
              Width = 67
              Height = 13
              Caption = 'Data emiss'#227'o:'
            end
            object Label13: TLabel
              Left = 188
              Top = 12
              Width = 71
              Height = 13
              Caption = 'Data chegada:'
            end
            object Label15: TLabel
              Left = 636
              Top = 12
              Width = 50
              Height = 13
              Caption = 'Prioridade:'
            end
            object Label14: TLabel
              Left = 752
              Top = 12
              Width = 101
              Height = 13
              Caption = 'Previs'#227'o entrega:'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsBold]
              ParentFont = False
            end
            object Label27: TLabel
              Left = 392
              Top = 12
              Width = 70
              Height = 13
              Caption = 'Pedido cliente:'
            end
            object TPDtaEmiss: TdmkEditDateTimePicker
              Left = 80
              Top = 8
              Width = 105
              Height = 21
              Date = 39789.688972615740000000
              Time = 39789.688972615740000000
              TabOrder = 0
              ReadOnly = False
              DefaultEditMask = '!99/99/99;1;_'
              AutoApplyEditMask = True
              QryCampo = 'DtaEmiss'
              UpdCampo = 'DtaEmiss'
              UpdType = utYes
              DatePurpose = dmkdpNone
            end
            object TPDtaEntra: TdmkEditDateTimePicker
              Left = 264
              Top = 8
              Width = 105
              Height = 21
              Date = 39789.688972615740000000
              Time = 39789.688972615740000000
              TabOrder = 1
              ReadOnly = False
              DefaultEditMask = '!99/99/99;1;_'
              AutoApplyEditMask = True
              QryCampo = 'DtaEntra'
              UpdCampo = 'DtaEntra'
              UpdType = utYes
              DatePurpose = dmkdpNone
            end
            object EdPrioridade: TdmkEdit
              Left = 688
              Top = 8
              Width = 53
              Height = 21
              Alignment = taRightJustify
              TabOrder = 3
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 2
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '0'
              ValMax = '99'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '00'
              QryCampo = 'Prioridade'
              UpdCampo = 'Prioridade'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
            end
            object TPDtaPrevi: TdmkEditDateTimePicker
              Left = 856
              Top = 8
              Width = 129
              Height = 21
              Date = 39789.688972615740000000
              Time = 39789.688972615740000000
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsBold]
              ParentFont = False
              TabOrder = 4
              ReadOnly = False
              DefaultEditMask = '!99/99/99;1;_'
              AutoApplyEditMask = True
              QryCampo = 'DtaPrevi'
              UpdCampo = 'DtaPrevi'
              UpdType = utYes
              DatePurpose = dmkdpNone
            end
            object EdPedidoCli: TdmkEdit
              Left = 468
              Top = 8
              Width = 145
              Height = 21
              TabOrder = 2
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 2
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '0'
              ValMax = '99'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              QryCampo = 'PedidoCli'
              UpdCampo = 'PedidoCli'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
          end
          object GroupBox3: TGroupBox
            Left = 1
            Top = 37
            Width = 998
            Height = 60
            Align = alTop
            TabOrder = 1
            object BtTabelaPrc: TSpeedButton
              Left = 684
              Top = 8
              Width = 21
              Height = 21
              Caption = '...'
              OnClick = BtTabelaPrcClick
            end
            object LaTabelaPrc: TLabel
              Left = 8
              Top = 13
              Width = 81
              Height = 13
              Caption = 'Tabela de pre'#231'o:'
            end
            object SpeedButton9: TSpeedButton
              Left = 964
              Top = 8
              Width = 21
              Height = 21
              Caption = '...'
              OnClick = SpeedButton9Click
            end
            object Label22: TLabel
              Left = 714
              Top = 13
              Width = 36
              Height = 13
              Caption = 'Moeda:'
            end
            object BtCondicaoPG: TSpeedButton
              Left = 504
              Top = 32
              Width = 21
              Height = 21
              Caption = '...'
              OnClick = BtCondicaoPGClick
            end
            object Label24: TLabel
              Left = 530
              Top = 37
              Width = 39
              Height = 13
              Caption = 'Carteira:'
            end
            object LaCondicaoPG: TLabel
              Left = 8
              Top = 37
              Width = 119
              Height = 13
              Caption = 'Condi'#231#227'o de pagamento:'
            end
            object SpeedButton13: TSpeedButton
              Left = 964
              Top = 32
              Width = 21
              Height = 21
              Caption = '...'
              OnClick = SpeedButton13Click
            end
            object CBTabelaPrc: TdmkDBLookupComboBox
              Left = 188
              Top = 8
              Width = 493
              Height = 21
              KeyField = 'CodUsu'
              ListField = 'Nome'
              TabOrder = 1
              dmkEditCB = EdTabelaPrc
              QryCampo = 'TabelaPrc'
              UpdType = utNil
              LocF7SQLMasc = '$#'
              LocF7PreDefProc = f7pNone
            end
            object EdTabelaPrc: TdmkEditCB
              Left = 128
              Top = 8
              Width = 56
              Height = 21
              Alignment = taRightJustify
              TabOrder = 0
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              OnChange = EdTabelaPrcChange
              DBLookupComboBox = CBTabelaPrc
              IgnoraDBLookupComboBox = False
              AutoSetIfOnlyOneReg = setregOnlyManual
            end
            object CBCartEmis: TdmkDBLookupComboBox
              Left = 620
              Top = 32
              Width = 341
              Height = 21
              KeyField = 'Codigo'
              ListField = 'Nome'
              TabOrder = 7
              dmkEditCB = EdCartEmis
              QryCampo = 'CartEmis'
              UpdType = utNil
              LocF7SQLMasc = '$#'
              LocF7PreDefProc = f7pNone
            end
            object CBMoeda: TdmkDBLookupComboBox
              Left = 804
              Top = 8
              Width = 157
              Height = 21
              KeyField = 'CodUsu'
              ListField = 'Nome'
              TabOrder = 3
              dmkEditCB = EdMoeda
              UpdType = utNil
              LocF7SQLMasc = '$#'
              LocF7PreDefProc = f7pNone
            end
            object EdMoeda: TdmkEditCB
              Left = 758
              Top = 8
              Width = 44
              Height = 21
              Alignment = taRightJustify
              TabOrder = 2
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              DBLookupComboBox = CBMoeda
              IgnoraDBLookupComboBox = False
              AutoSetIfOnlyOneReg = setregOnlyManual
            end
            object EdCartEmis: TdmkEditCB
              Left = 574
              Top = 32
              Width = 44
              Height = 21
              Alignment = taRightJustify
              TabOrder = 6
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              QryCampo = 'CartEmis'
              UpdCampo = 'CartEmis'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              DBLookupComboBox = CBCartEmis
              IgnoraDBLookupComboBox = False
              AutoSetIfOnlyOneReg = setregOnlyManual
            end
            object CBCondicaoPG: TdmkDBLookupComboBox
              Left = 184
              Top = 32
              Width = 313
              Height = 21
              KeyField = 'CodUsu'
              ListField = 'Nome'
              TabOrder = 5
              dmkEditCB = EdCondicaoPG
              UpdType = utNil
              LocF7SQLMasc = '$#'
              LocF7PreDefProc = f7pNone
            end
            object EdCondicaoPG: TdmkEditCB
              Left = 128
              Top = 32
              Width = 56
              Height = 21
              Alignment = taRightJustify
              TabOrder = 4
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              OnChange = EdCondicaoPGChange
              DBLookupComboBox = CBCondicaoPG
              IgnoraDBLookupComboBox = False
              AutoSetIfOnlyOneReg = setregOnlyManual
            end
          end
          object GroupBox4: TGroupBox
            Left = 1
            Top = 97
            Width = 998
            Height = 36
            Align = alTop
            TabOrder = 2
            object Label28: TLabel
              Left = 8
              Top = 12
              Width = 87
              Height = 13
              Caption = 'Lote de produ'#231#227'o:'
              FocusControl = DBEdit15
            end
            object DBEdit15: TDBEdit
              Left = 100
              Top = 8
              Width = 92
              Height = 21
              TabStop = False
              Color = clGradientInactiveCaption
              DataField = 'LoteProd'
              DataSource = DsPediVda
              TabOrder = 0
            end
          end
          object GroupBox5: TGroupBox
            Left = 1
            Top = 133
            Width = 998
            Height = 106
            Align = alTop
            TabOrder = 3
            object Label29: TLabel
              Left = 8
              Top = 13
              Width = 45
              Height = 13
              Caption = 'Frete por:'
            end
            object Label30: TLabel
              Left = 8
              Top = 36
              Width = 75
              Height = 13
              Caption = 'Transportadora:'
            end
            object Label31: TLabel
              Left = 8
              Top = 61
              Width = 64
              Height = 13
              Caption = 'Redespacho:'
            end
            object SpeedButton6: TSpeedButton
              Left = 524
              Top = 32
              Width = 21
              Height = 21
              Caption = '...'
              OnClick = SpeedButton6Click
            end
            object SpeedButton11: TSpeedButton
              Left = 524
              Top = 56
              Width = 21
              Height = 21
              Caption = '...'
              OnClick = SpeedButton11Click
            end
            object Label20: TLabel
              Left = 456
              Top = 13
              Width = 92
              Height = 13
              Caption = 'Local de entrega: >'
            end
            object Label32: TLabel
              Left = 176
              Top = 84
              Width = 115
              Height = 13
              Caption = '$ Despesas acess'#243'rias: '
            end
            object Label41: TLabel
              Left = 552
              Top = 84
              Width = 36
              Height = 13
              Caption = '$ Frete:'
            end
            object Label42: TLabel
              Left = 736
              Top = 84
              Width = 48
              Height = 13
              Caption = '% Seguro:'
            end
            object Label43: TLabel
              Left = 8
              Top = 84
              Width = 117
              Height = 13
              Caption = '% Despesas acess'#243'rias: '
            end
            object Label44: TLabel
              Left = 452
              Top = 84
              Width = 38
              Height = 13
              Caption = '% Frete:'
            end
            object Label45: TLabel
              Left = 844
              Top = 84
              Width = 46
              Height = 13
              Caption = '$ Seguro:'
            end
            object EdFretePor: TdmkEditCB
              Left = 56
              Top = 8
              Width = 21
              Height = 21
              Alignment = taRightJustify
              TabOrder = 0
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              QryCampo = 'FretePor'
              UpdCampo = 'FretePor'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              DBLookupComboBox = CBFretePor
              IgnoraDBLookupComboBox = False
              AutoSetIfOnlyOneReg = setregOnlyManual
            end
            object CBFretePor: TdmkDBLookupComboBox
              Left = 80
              Top = 8
              Width = 369
              Height = 21
              KeyField = 'Codigo'
              ListField = 'Nome'
              TabOrder = 1
              dmkEditCB = EdFretePor
              QryCampo = 'FretePor'
              UpdType = utNil
              LocF7SQLMasc = '$#'
              LocF7PreDefProc = f7pNone
            end
            object EdTransporta: TdmkEditCB
              Left = 88
              Top = 32
              Width = 56
              Height = 21
              Alignment = taRightJustify
              TabOrder = 2
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              QryCampo = 'Transporta'
              UpdCampo = 'Transporta'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              DBLookupComboBox = CBTransporta
              IgnoraDBLookupComboBox = False
              AutoSetIfOnlyOneReg = setregOnlyManual
            end
            object CBTransporta: TdmkDBLookupComboBox
              Left = 148
              Top = 32
              Width = 373
              Height = 21
              KeyField = 'Codigo'
              ListField = 'NOMEENT'
              TabOrder = 3
              dmkEditCB = EdTransporta
              QryCampo = 'Transporta'
              UpdType = utNil
              LocF7SQLMasc = '$#'
              LocF7PreDefProc = f7pNone
            end
            object EdRedespacho: TdmkEditCB
              Left = 88
              Top = 56
              Width = 56
              Height = 21
              Alignment = taRightJustify
              TabOrder = 4
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              QryCampo = 'Redespacho'
              UpdCampo = 'Redespacho'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              DBLookupComboBox = CBRedespacho
              IgnoraDBLookupComboBox = False
              AutoSetIfOnlyOneReg = setregOnlyManual
            end
            object CBRedespacho: TdmkDBLookupComboBox
              Left = 148
              Top = 56
              Width = 373
              Height = 21
              KeyField = 'Codigo'
              ListField = 'NOMEENT'
              TabOrder = 5
              dmkEditCB = EdRedespacho
              QryCampo = 'Redespacho'
              UpdType = utNil
              LocF7SQLMasc = '$#'
              LocF7PreDefProc = f7pNone
            end
            object MeEnderecoEntrega1: TMemo
              Left = 552
              Top = 8
              Width = 433
              Height = 69
              TabStop = False
              ReadOnly = True
              TabOrder = 6
            end
            object EdDesoAces_V: TdmkEdit
              Left = 300
              Top = 80
              Width = 80
              Height = 21
              Alignment = taRightJustify
              TabOrder = 8
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              QryCampo = 'DesoAces_V'
              UpdCampo = 'DesoAces_V'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
            end
            object EdFrete_V: TdmkEdit
              Left = 600
              Top = 80
              Width = 80
              Height = 21
              Alignment = taRightJustify
              TabOrder = 10
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              QryCampo = 'Frete_V'
              UpdCampo = 'Frete_V'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
            end
            object EdSeguro_P: TdmkEdit
              Left = 788
              Top = 80
              Width = 48
              Height = 21
              Alignment = taRightJustify
              TabOrder = 11
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              QryCampo = 'Seguro_P'
              UpdCampo = 'Seguro_P'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
              OnChange = EdSeguro_PChange
            end
            object EdDesoAces_P: TdmkEdit
              Left = 124
              Top = 80
              Width = 48
              Height = 21
              Alignment = taRightJustify
              TabOrder = 7
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              QryCampo = 'DesoAces_P'
              UpdCampo = 'DesoAces_P'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
              OnChange = EdDesoAces_PChange
            end
            object EdFrete_P: TdmkEdit
              Left = 496
              Top = 80
              Width = 48
              Height = 21
              Alignment = taRightJustify
              TabOrder = 9
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              QryCampo = 'Frete_P'
              UpdCampo = 'Frete_P'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
              OnChange = EdFrete_PChange
            end
            object EdSeguro_V: TdmkEdit
              Left = 904
              Top = 80
              Width = 80
              Height = 21
              Alignment = taRightJustify
              TabOrder = 12
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              QryCampo = 'Seguro_V'
              UpdCampo = 'Seguro_V'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
            end
          end
          object GroupBox7: TGroupBox
            Left = 1
            Top = 239
            Width = 998
            Height = 40
            Align = alTop
            TabOrder = 4
            object Label46: TLabel
              Left = 12
              Top = 16
              Width = 73
              Height = 13
              Caption = 'Representante:'
            end
            object SpeedButton12: TSpeedButton
              Left = 616
              Top = 12
              Width = 21
              Height = 21
              Caption = '...'
              OnClick = SpeedButton12Click
            end
            object Label47: TLabel
              Left = 640
              Top = 15
              Width = 117
              Height = 13
              Caption = '% comiss'#227'o faturamento:'
            end
            object Label48: TLabel
              Left = 816
              Top = 15
              Width = 119
              Height = 13
              Caption = '% comiss'#227'o recebimento:'
            end
            object EdRepresen: TdmkEditCB
              Left = 88
              Top = 12
              Width = 56
              Height = 21
              Alignment = taRightJustify
              TabOrder = 0
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              QryCampo = 'Represen'
              UpdCampo = 'Represen'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              OnChange = EdRepresenChange
              DBLookupComboBox = CBRepresen
              IgnoraDBLookupComboBox = False
              AutoSetIfOnlyOneReg = setregOnlyManual
            end
            object CBRepresen: TdmkDBLookupComboBox
              Left = 148
              Top = 12
              Width = 465
              Height = 21
              KeyField = 'Codigo'
              ListField = 'NOMEACC'
              TabOrder = 1
              dmkEditCB = EdRepresen
              QryCampo = 'Represen'
              UpdType = utNil
              LocF7SQLMasc = '$#'
              LocF7PreDefProc = f7pNone
            end
            object EdComisFat: TdmkEdit
              Left = 760
              Top = 11
              Width = 48
              Height = 21
              Alignment = taRightJustify
              TabOrder = 2
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              QryCampo = 'ComisFat'
              UpdCampo = 'ComisFat'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
            end
            object EdComisRec: TdmkEdit
              Left = 936
              Top = 11
              Width = 48
              Height = 21
              Alignment = taRightJustify
              TabOrder = 3
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              QryCampo = 'ComisRec'
              UpdCampo = 'ComisRec'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
            end
          end
          object GroupBox6: TGroupBox
            Left = 1
            Top = 279
            Width = 998
            Height = 142
            Align = alTop
            Caption = ' Fiscal:'
            TabOrder = 5
            object Label25: TLabel
              Left = 12
              Top = 20
              Width = 73
              Height = 13
              Caption = 'Movimenta'#231#227'o:'
            end
            object SbRegrFiscal: TSpeedButton
              Left = 616
              Top = 16
              Width = 21
              Height = 21
              Caption = '...'
              OnClick = SbRegrFiscalClick
            end
            object Label33: TLabel
              Left = 640
              Top = 20
              Width = 55
              Height = 13
              Caption = 'Modelo NF:'
            end
            object Label36: TLabel
              Left = 12
              Top = 88
              Width = 518
              Height = 13
              Caption = 
                'Indicador de presen'#231'a do comprador no estabelecimento comercial ' +
                'no momento da opera'#231#227'o (NFe 3.10): [F4]'
            end
            object Label208: TLabel
              Left = 624
              Top = 88
              Width = 134
              Height = 13
              Caption = 'Finalidade emis'#227'o NF-e: [F4]'
              FocusControl = Edide_finNFe
            end
            object EdRegrFiscal: TdmkEditCB
              Left = 88
              Top = 16
              Width = 56
              Height = 21
              Alignment = taRightJustify
              TabOrder = 0
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              OnChange = EdRegrFiscalChange
              OnExit = EdRegrFiscalExit
              DBLookupComboBox = CBRegrFiscal
              IgnoraDBLookupComboBox = False
              AutoSetIfOnlyOneReg = setregOnlyManual
            end
            object CBRegrFiscal: TdmkDBLookupComboBox
              Left = 148
              Top = 16
              Width = 465
              Height = 21
              KeyField = 'CodUsu'
              ListField = 'Nome'
              TabOrder = 1
              dmkEditCB = EdRegrFiscal
              UpdType = utNil
              LocF7SQLMasc = '$#'
              LocF7PreDefProc = f7pNone
            end
            object EdModeloNF: TdmkEdit
              Left = 696
              Top = 16
              Width = 287
              Height = 21
              ReadOnly = True
              TabOrder = 2
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object RG_idDest: TdmkRadioGroup
              Left = 12
              Top = 42
              Width = 693
              Height = 39
              Caption = ' Local de Destino da Opera'#231#227'o:'
              Columns = 4
              ItemIndex = 0
              Items.Strings = (
                '0 - N'#227'o definido'
                '1 - Opera'#231#227'o interna'
                '2 - Opera'#231#227'o interestadual'
                '3 - Opera'#231#227'o com exterior')
              TabOrder = 3
              QryCampo = 'idDest'
              UpdCampo = 'idDest'
              UpdType = utYes
              OldValor = 0
            end
            object RG_indFinal: TdmkRadioGroup
              Left = 712
              Top = 42
              Width = 272
              Height = 39
              Caption = ' Opera'#231#227'o com consumidor: '
              Columns = 2
              ItemIndex = 0
              Items.Strings = (
                '0 - Normal'
                '1 - Consumidor final')
              TabOrder = 4
              QryCampo = 'indFinal'
              UpdCampo = 'indFinal'
              UpdType = utYes
              OldValor = 0
            end
            object EdindPres: TdmkEdit
              Left = 12
              Top = 104
              Width = 29
              Height = 21
              CharCase = ecUpperCase
              TabOrder = 5
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              QryCampo = 'indPres'
              UpdCampo = 'indPres'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
              OnChange = EdindPresChange
              OnKeyDown = EdindPresKeyDown
            end
            object EdindPres_TXT: TEdit
              Left = 42
              Top = 104
              Width = 576
              Height = 21
              TabStop = False
              ReadOnly = True
              TabOrder = 6
            end
            object RGIndSinc: TdmkRadioGroup
              Left = 780
              Top = 90
              Width = 204
              Height = 39
              Caption = ' Envio da NF-e ao Fisco:'
              Columns = 2
              Enabled = False
              ItemIndex = 1
              Items.Strings = (
                'Ass'#237'ncrono'
                'S'#237'ncrono')
              TabOrder = 9
              QryCampo = 'IndSinc'
              UpdCampo = 'IndSinc'
              UpdType = utYes
              OldValor = 0
            end
            object Edide_finNFe: TdmkEdit
              Left = 624
              Top = 104
              Width = 21
              Height = 21
              Alignment = taRightJustify
              TabOrder = 7
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '0'
              ValMax = '4'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              QryCampo = 'finNFe'
              UpdCampo = 'finNFe'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              OnChange = Edide_finNFeChange
              OnKeyDown = Edide_finNFeKeyDown
            end
            object Edide_finNFe_TXT: TdmkEdit
              Left = 646
              Top = 104
              Width = 128
              Height = 21
              ReadOnly = True
              TabOrder = 8
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
          end
        end
      end
    end
    object GBRodaPe: TGroupBox
      Left = 0
      Top = 603
      Width = 1008
      Height = 64
      Align = alBottom
      TabOrder = 2
      object Panel12: TPanel
        Left = 2
        Top = 15
        Width = 1004
        Height = 47
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object Label49: TLabel
          Left = 232
          Top = 17
          Width = 78
          Height = 13
          Caption = '% Fatura parcial:'
          Visible = False
        end
        object PnSaiDesis: TPanel
          Left = 859
          Top = 0
          Width = 145
          Height = 47
          Align = alRight
          BevelOuter = bvNone
          TabOrder = 3
          object BtDesiste: TBitBtn
            Tag = 15
            Left = 7
            Top = 2
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Caption = '&Desiste'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            NumGlyphs = 2
            ParentFont = False
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtDesisteClick
          end
        end
        object BtConfirma: TBitBtn
          Tag = 14
          Left = 13
          Top = 2
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Confirma'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtConfirmaClick
        end
        object CkAFP_Sit: TdmkCheckBox
          Left = 140
          Top = 15
          Width = 89
          Height = 17
          Caption = 'Fatura parcial?'
          TabOrder = 1
          Visible = False
          QryCampo = 'AFP_Sit'
          UpdCampo = 'AFP_Sit'
          UpdType = utYes
          ValCheck = #0
          ValUncheck = #0
          OldValor = #0
        end
        object EdAFP_Per: TdmkEdit
          Left = 312
          Top = 14
          Width = 56
          Height = 21
          Alignment = taRightJustify
          TabOrder = 2
          Visible = False
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 4
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,0000'
          QryCampo = 'AFP_Per'
          UpdCampo = 'AFP_Per'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 12
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 288
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 5
        Top = 5
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
        OnClick = SbImprimeClick
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 45
        Top = 5
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 85
        Top = 5
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 125
        Top = 5
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 165
        Top = 5
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
      object BtGraGruN: TBitBtn
        Tag = 30
        Left = 245
        Top = 5
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 5
        OnClick = BtGraGruNClick
      end
      object BtFisRegCad: TBitBtn
        Tag = 10104
        Left = 205
        Top = 5
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 6
        OnClick = BtFisRegCadClick
      end
    end
    object GB_M: TGroupBox
      Left = 288
      Top = 0
      Width = 672
      Height = 52
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 346
        Height = 32
        Caption = 'Pedidos de Compra e Venda'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 346
        Height = 32
        Caption = 'Pedidos de Compra e Venda'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 346
        Height = 32
        Caption = 'Pedidos de Compra e Venda'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 52
    Width = 1008
    Height = 44
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel11: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 12
        Height = 17
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 12
        Height = 17
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object DsPediVda: TDataSource
    DataSet = QrPediVda
    Left = 56
    Top = 68
  end
  object QrPediVda: TMySQLQuery
    BeforeOpen = QrPediVdaBeforeOpen
    AfterOpen = QrPediVdaAfterOpen
    BeforeClose = QrPediVdaBeforeClose
    AfterScroll = QrPediVdaAfterScroll
    OnCalcFields = QrPediVdaCalcFields
    SQL.Strings = (
      
        'SELECT pvd.Codigo, pvd.finNFe, pvd.CodUsu, pvd.Empresa, pvd.Clie' +
        'nte,'
      'pvd.DtaEmiss, pvd.DtaEntra, pvd.DtaInclu, pvd.DtaPrevi,'
      'pvd.Prioridade, pvd.CondicaoPG, pvd.Moeda,'
      'pvd.Situacao, pvd.TabelaPrc, pvd.MotivoSit, pvd.LoteProd,'
      'pvd.PedidoCli, pvd.FretePor, pvd.Transporta, pvd.Redespacho,'
      'pvd.RegrFiscal, pvd.DesoAces_V, pvd.DesoAces_P,'
      'pvd.Frete_V, pvd.Frete_P, pvd.Seguro_V, pvd.Seguro_P,'
      'pvd.TotalQtd, pvd.Total_Vlr, pvd.Total_Des, pvd.Total_Tot,'
      'pvd.Observa, pvd.EntSai, '
      'tpc.Nome NOMETABEPRCCAD, tpc.DescoMax,'
      'mda.Nome NOMEMOEDA, pvd.Represen, pvd.ComisFat,'
      'pvd.ComisRec, pvd.CartEmis, pvd.AFP_Sit, pvd.AFP_Per,'
      'ppc.MedDDSimpl, ppc.MedDDReal, ppc.MaxDesco, ppc.JurosMes,'
      'pvd.ValLiq, pvd.QuantP, frc.Nome NOMEFISREGCAD,'
      'imp.Nome NOMEMODELONF,'
      'IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NOMEEMP,'
      'IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NOMECLI,'
      'IF(cli.Tipo=0, cli.ECidade, cli.PCidade) CIDADECLI,'
      'IF(ven.Tipo=0, ven.RazaoSocial, ven.NOME) NOMEACC,'
      'IF(tra.Tipo=0, tra.RazaoSocial, tra.NOME) NOMETRANSP,'
      'IF(red.Tipo=0, red.RazaoSocial, red.NOME) NOMEREDESP,'
      'uf1.Nome NOMEUF, emp.Filial, car.Nome NOMECARTEMIS,'
      'ppc.Nome NOMECONDICAOPG, mot.Nome NOMEMOTIVO,'
      'cli.CodUsu CODUSU_CLI, ven.CodUsu CODUSU_ACC,'
      'tra.CodUsu CODUSU_TRA, red.CodUsu CODUSU_RED,'
      'mot.CodUsu CODUSU_MOT, tpc.CodUsu CODUSU_TPC,'
      'mda.CodUsu CODUSU_MDA, ppc.CodUsu CODUSU_PPC,'
      'frc.CodUsu CODUSU_FRC, imp.Codigo MODELO_NF,'
      'emp.CodUsu CODUSU_FOR, cli.Filial FILIAL_CLI'
      'FROM pedivda pvd'
      'LEFT JOIN entidades  emp ON emp.Codigo=pvd.Empresa'
      'LEFT JOIN entidades  cli ON cli.Codigo=pvd.Cliente'
      'LEFT JOIN entidades  tra ON tra.Codigo=pvd.Transporta'
      'LEFT JOIN entidades  red ON red.Codigo=pvd.Redespacho'
      
        'LEFT JOIN ufs        uf1 ON uf1.Codigo=IF(cli.Tipo=0, cli.EUF, c' +
        'li.PUF)'
      'LEFT JOIN tabeprccab tpc ON tpc.Codigo=pvd.TabelaPrc'
      'LEFT JOIN cambiomda  mda ON mda.Codigo=pvd.Moeda'
      'LEFT JOIN pediprzcab ppc ON ppc.Codigo=pvd.CondicaoPG'
      'LEFT JOIN motivos    mot ON mot.Codigo=pvd.MotivoSit'
      'LEFT JOIN pediacc    acc ON acc.Codigo=pvd.Represen'
      'LEFT JOIN entidades  ven ON ven.Codigo=acc.Codigo'
      'LEFT JOIN carteiras  car ON car.Codigo=pvd.CartEmis'
      'LEFT JOIN fisregcad  frc ON frc.Codigo=pvd.RegrFiscal'
      'LEFT JOIN imprime    imp ON imp.Codigo=frc.ModeloNF'
      '')
    Left = 28
    Top = 68
    object QrPediVdaCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrPediVdaCodUsu: TIntegerField
      FieldName = 'CodUsu'
      Required = True
      DisplayFormat = '000000'
    end
    object QrPediVdaEmpresa: TIntegerField
      FieldName = 'Empresa'
      Required = True
    end
    object QrPediVdaCliente: TIntegerField
      FieldName = 'Cliente'
      Required = True
    end
    object QrPediVdaDtaEmiss: TDateField
      FieldName = 'DtaEmiss'
      Required = True
      DisplayFormat = 'dd/mm/yyyy'
    end
    object QrPediVdaDtaEntra: TDateField
      FieldName = 'DtaEntra'
      Required = True
      DisplayFormat = 'dd/mm/yyyy'
    end
    object QrPediVdaDtaInclu: TDateField
      FieldName = 'DtaInclu'
      Required = True
      DisplayFormat = 'dd/mm/yyyy'
    end
    object QrPediVdaDtaPrevi: TDateField
      FieldName = 'DtaPrevi'
      Required = True
      DisplayFormat = 'dd/mm/yyyy'
    end
    object QrPediVdaNOMEEMP: TWideStringField
      FieldName = 'NOMEEMP'
      Size = 100
    end
    object QrPediVdaNOMECLI: TWideStringField
      FieldName = 'NOMECLI'
      Size = 100
    end
    object QrPediVdaCIDADECLI: TWideStringField
      FieldName = 'CIDADECLI'
      Size = 25
    end
    object QrPediVdaNOMEUF: TWideStringField
      FieldName = 'NOMEUF'
      Required = True
      Size = 2
    end
    object QrPediVdaFilial: TIntegerField
      FieldName = 'Filial'
    end
    object QrPediVdaPrioridade: TSmallintField
      FieldName = 'Prioridade'
      Required = True
      DisplayFormat = '00'
    end
    object QrPediVdaCondicaoPG: TIntegerField
      FieldName = 'CondicaoPG'
      Required = True
    end
    object QrPediVdaMoeda: TIntegerField
      FieldName = 'Moeda'
      Required = True
    end
    object QrPediVdaSituacao: TIntegerField
      FieldName = 'Situacao'
      Required = True
    end
    object QrPediVdaTabelaPrc: TIntegerField
      FieldName = 'TabelaPrc'
      Required = True
    end
    object QrPediVdaMotivoSit: TIntegerField
      FieldName = 'MotivoSit'
      Required = True
    end
    object QrPediVdaLoteProd: TIntegerField
      FieldName = 'LoteProd'
      Required = True
    end
    object QrPediVdaPedidoCli: TWideStringField
      FieldName = 'PedidoCli'
    end
    object QrPediVdaFretePor: TSmallintField
      FieldName = 'FretePor'
      Required = True
    end
    object QrPediVdaTransporta: TIntegerField
      FieldName = 'Transporta'
      Required = True
    end
    object QrPediVdaRedespacho: TIntegerField
      FieldName = 'Redespacho'
      Required = True
    end
    object QrPediVdaRegrFiscal: TIntegerField
      FieldName = 'RegrFiscal'
      Required = True
    end
    object QrPediVdaDesoAces_V: TFloatField
      FieldName = 'DesoAces_V'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrPediVdaDesoAces_P: TFloatField
      FieldName = 'DesoAces_P'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrPediVdaFrete_V: TFloatField
      FieldName = 'Frete_V'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrPediVdaFrete_P: TFloatField
      FieldName = 'Frete_P'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrPediVdaSeguro_V: TFloatField
      FieldName = 'Seguro_V'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrPediVdaSeguro_P: TFloatField
      FieldName = 'Seguro_P'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrPediVdaTotalQtd: TFloatField
      FieldName = 'TotalQtd'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrPediVdaTotal_Vlr: TFloatField
      FieldName = 'Total_Vlr'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrPediVdaTotal_Des: TFloatField
      FieldName = 'Total_Des'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrPediVdaTotal_Tot: TFloatField
      FieldName = 'Total_Tot'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrPediVdaObserva: TWideMemoField
      FieldName = 'Observa'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrPediVdaNOMETABEPRCCAD: TWideStringField
      FieldName = 'NOMETABEPRCCAD'
      Size = 50
    end
    object QrPediVdaNOMEMOEDA: TWideStringField
      FieldName = 'NOMEMOEDA'
      Size = 30
    end
    object QrPediVdaNOMECONDICAOPG: TWideStringField
      FieldName = 'NOMECONDICAOPG'
      Size = 50
    end
    object QrPediVdaNOMEMOTIVO: TWideStringField
      FieldName = 'NOMEMOTIVO'
      Size = 50
    end
    object QrPediVdaNOMESITUACAO: TWideStringField
      FieldKind = fkLookup
      FieldName = 'NOMESITUACAO'
      LookupKeyFields = 'Codigo'
      LookupResultField = 'Nome'
      KeyFields = 'Situacao'
      Size = 50
      Lookup = True
    end
    object QrPediVdaNOMECARTEMIS: TWideStringField
      FieldName = 'NOMECARTEMIS'
      Size = 100
    end
    object QrPediVdaNOMEFRETEPOR: TWideStringField
      DisplayWidth = 50
      FieldKind = fkCalculated
      FieldName = 'NOMEFRETEPOR'
      Size = 50
      Calculated = True
    end
    object QrPediVdaNOMETRANSP: TWideStringField
      FieldName = 'NOMETRANSP'
      Size = 100
    end
    object QrPediVdaNOMEREDESP: TWideStringField
      FieldName = 'NOMEREDESP'
      Size = 100
    end
    object QrPediVdaNOMEACC: TWideStringField
      FieldName = 'NOMEACC'
      Size = 100
    end
    object QrPediVdaNOMEFISREGCAD: TWideStringField
      FieldName = 'NOMEFISREGCAD'
      Size = 50
    end
    object QrPediVdaNOMEMODELONF: TWideStringField
      FieldName = 'NOMEMODELONF'
      Size = 100
    end
    object QrPediVdaRepresen: TIntegerField
      FieldName = 'Represen'
      Required = True
    end
    object QrPediVdaComisFat: TFloatField
      FieldName = 'ComisFat'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrPediVdaComisRec: TFloatField
      FieldName = 'ComisRec'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrPediVdaCartEmis: TIntegerField
      FieldName = 'CartEmis'
      Required = True
    end
    object QrPediVdaAFP_Sit: TSmallintField
      FieldName = 'AFP_Sit'
      Required = True
    end
    object QrPediVdaAFP_Per: TFloatField
      FieldName = 'AFP_Per'
      Required = True
      DisplayFormat = '#,###,##0.0000'
    end
    object QrPediVdaCODUSU_CLI: TIntegerField
      FieldName = 'CODUSU_CLI'
      Required = True
    end
    object QrPediVdaCODUSU_ACC: TIntegerField
      FieldName = 'CODUSU_ACC'
      Required = True
    end
    object QrPediVdaCODUSU_TRA: TIntegerField
      FieldName = 'CODUSU_TRA'
      Required = True
    end
    object QrPediVdaCODUSU_RED: TIntegerField
      FieldName = 'CODUSU_RED'
      Required = True
    end
    object QrPediVdaCODUSU_MOT: TIntegerField
      FieldName = 'CODUSU_MOT'
      Required = True
    end
    object QrPediVdaCODUSU_TPC: TIntegerField
      FieldName = 'CODUSU_TPC'
      Required = True
    end
    object QrPediVdaCODUSU_MDA: TIntegerField
      FieldName = 'CODUSU_MDA'
      Required = True
    end
    object QrPediVdaCODUSU_PPC: TIntegerField
      FieldName = 'CODUSU_PPC'
      Required = True
    end
    object QrPediVdaCODUSU_FRC: TIntegerField
      FieldName = 'CODUSU_FRC'
      Required = True
    end
    object QrPediVdaMODELO_NF: TIntegerField
      FieldName = 'MODELO_NF'
      Required = True
    end
    object QrPediVdaMedDDSimpl: TFloatField
      FieldName = 'MedDDSimpl'
    end
    object QrPediVdaMedDDReal: TFloatField
      FieldName = 'MedDDReal'
    end
    object QrPediVdaValLiq: TFloatField
      FieldName = 'ValLiq'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrPediVdaQuantP: TFloatField
      FieldName = 'QuantP'
      Required = True
    end
    object QrPediVdaMaxDesco: TFloatField
      FieldName = 'MaxDesco'
    end
    object QrPediVdaJurosMes: TFloatField
      FieldName = 'JurosMes'
      DisplayFormat = '0.000000'
    end
    object QrPediVdaDescoMax: TFloatField
      FieldName = 'DescoMax'
      DisplayFormat = '0.00'
    end
    object QrPediVdaEntSai: TSmallintField
      FieldName = 'EntSai'
    end
    object QrPediVdaENTSAI_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'ENTSAI_TXT'
      Size = 6
      Calculated = True
    end
    object QrPediVdaSHOW_COD_FILIAL: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'SHOW_COD_FILIAL'
      Calculated = True
    end
    object QrPediVdaSHOW_COD_CLIFOR: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'SHOW_COD_CLIFOR'
      Calculated = True
    end
    object QrPediVdaSHOW_TXT_FILIAL: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'SHOW_TXT_FILIAL'
      Size = 100
      Calculated = True
    end
    object QrPediVdaSHOW_TXT_CLIFOR: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'SHOW_TXT_CLIFOR'
      Size = 100
      Calculated = True
    end
    object QrPediVdaCODUSU_FOR: TIntegerField
      FieldName = 'CODUSU_FOR'
      Required = True
    end
    object QrPediVdaFILIAL_CLI: TIntegerField
      FieldName = 'FILIAL_CLI'
    end
    object QrPediVdaidDest: TSmallintField
      FieldName = 'idDest'
    end
    object QrPediVdaindFinal: TSmallintField
      FieldName = 'indFinal'
    end
    object QrPediVdaindPres: TSmallintField
      FieldName = 'indPres'
    end
    object QrPediVdaindSinc: TSmallintField
      FieldName = 'indSinc'
    end
    object QrPediVdafinNFe: TSmallintField
      FieldName = 'finNFe'
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 140
    Top = 68
  end
  object PMPedidos: TPopupMenu
    OnPopup = PMPedidosPopup
    Left = 380
    Top = 592
    object Incluinovopedido1: TMenuItem
      Caption = '&Inclui novo pedido'
      OnClick = Incluinovopedido1Click
    end
    object Alterapedidoatual1: TMenuItem
      Caption = '&Altera pedido atual'
      OnClick = Alterapedidoatual1Click
    end
    object Excluipedidoatual1: TMenuItem
      Caption = '&Exclui pedido atual'
      Enabled = False
    end
    object N4: TMenuItem
      Caption = '-'
    end
    object Duplica1: TMenuItem
      Caption = '&Duplicar pedido atual'
      OnClick = Duplica1Click
    end
  end
  object PMItens: TPopupMenu
    Left = 484
    Top = 592
    object Incluinovositensdegrupo1: TMenuItem
      Caption = '&Inclui novos itens por &Grade'
      OnClick = Incluinovositensdegrupo1Click
    end
    object IncluinovositensporLeitura1: TMenuItem
      Caption = 'Inclui novos itens por &Leitura'
      OnClick = IncluinovositensporLeitura1Click
    end
    object N1: TMenuItem
      Caption = '-'
      OnClick = N1Click
    end
    object AlteraExcluiIncluiitemselecionado1: TMenuItem
      Caption = '&Altera / Exclui / Inclui item selecionado'
      OnClick = AlteraExcluiIncluiitemselecionado1Click
    end
    object N2: TMenuItem
      Caption = '-'
    end
  end
  object VuEmpresa: TdmkValUsu
    dmkEditCB = EdEmpresa
    Panel = PainelEdita
    QryCampo = 'Empresa'
    UpdCampo = 'Empresa'
    RefCampo = 'Codigo'
    UpdType = utYes
    Left = 168
    Top = 68
  end
  object VuCondicaoPG: TdmkValUsu
    dmkEditCB = EdCondicaoPG
    Panel = PainelEdita
    QryCampo = 'CondicaoPG'
    UpdCampo = 'CondicaoPG'
    RefCampo = 'Codigo'
    UpdType = utYes
    Left = 196
    Top = 68
  end
  object VuCambioMda: TdmkValUsu
    dmkEditCB = EdMoeda
    Panel = PainelEdita
    QryCampo = 'Moeda'
    UpdCampo = 'Moeda'
    RefCampo = 'Codigo'
    UpdType = utYes
    Left = 224
    Top = 68
  end
  object VuTabelaPrc: TdmkValUsu
    dmkEditCB = EdTabelaPrc
    Panel = PainelEdita
    QryCampo = 'TabelaPrc'
    UpdCampo = 'TabelaPrc'
    RefCampo = 'Codigo'
    UpdType = utYes
    Left = 252
    Top = 68
  end
  object VuMotivoSit: TdmkValUsu
    dmkEditCB = EdMotivoSit
    Panel = PainelEdita
    QryCampo = 'MotivoSit'
    UpdCampo = 'MotivoSit'
    RefCampo = 'Codigo'
    UpdType = utYes
    Left = 280
    Top = 68
  end
  object QrPediVdaGru: TMySQLQuery
    AfterOpen = QrPediVdaGruAfterOpen
    BeforeClose = QrPediVdaGruBeforeClose
    AfterScroll = QrPediVdaGruAfterScroll
    SQL.Strings = (
      'SELECT pvi.Controle, SUM(QuantP) QuantP, SUM(ValLiq) ValLiq, '
      'SUM(Customizad) ItensCustomizados,'
      'gti.Codigo GRATAMCAD, '
      'gg1.CodUsu, gg1.Nome, gg1.Nivel1, pgt.Fracio'
      'FROM pedivdaits pvi'
      'LEFT JOIN gragrux   ggx ON ggx.Controle=pvi.GraGruX'
      'LEFT JOIN gratamits gti ON gti.Controle=ggx.GraTamI'
      'LEFT JOIN gragru1   gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN prdgruptip   pgt ON pgt.Codigo=gg1.PrdGrupTip'
      'WHERE pvi.Codigo=:P0'
      'GROUP BY gg1.Nivel1')
    Left = 84
    Top = 68
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPediVdaGruCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrPediVdaGruNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
    object QrPediVdaGruNivel1: TIntegerField
      FieldName = 'Nivel1'
    end
    object QrPediVdaGruGRATAMCAD: TIntegerField
      FieldName = 'GRATAMCAD'
    end
    object QrPediVdaGruQuantP: TFloatField
      FieldName = 'QuantP'
    end
    object QrPediVdaGruValLiq: TFloatField
      FieldName = 'ValLiq'
      DisplayFormat = '#,###,##0.00'
    end
    object QrPediVdaGruItensCustomizados: TFloatField
      FieldName = 'ItensCustomizados'
    end
    object QrPediVdaGruFracio: TSmallintField
      FieldName = 'Fracio'
    end
    object QrPediVdaGruControle: TIntegerField
      FieldName = 'Controle'
    end
  end
  object DsPediVdaGru: TDataSource
    DataSet = QrPediVdaGru
    Left = 112
    Top = 68
  end
  object VuFisRegCad: TdmkValUsu
    dmkEditCB = EdRegrFiscal
    Panel = PainelEdita
    QryCampo = 'RegrFiscal'
    UpdCampo = 'RegrFiscal'
    RefCampo = 'Codigo'
    UpdType = utYes
    Left = 308
    Top = 68
  end
  object PMCustom: TPopupMenu
    OnPopup = PMCustomPopup
    Left = 536
    Top = 548
    object ItemprodutoCustomizvel1: TMenuItem
      Caption = '&Item (produto) Customiz'#225'vel'
      object IncluinovoitemprodutoCustomizvel1: TMenuItem
        Caption = 'Inclui novo item (produto) &Customiz'#225'vel'
        OnClick = IncluinovoitemprodutoCustomizvel1Click
      end
      object Alteraitemprodutoselecionado1: TMenuItem
        Caption = '&Altera item (produto) selecionado'
        OnClick = Alteraitemprodutoselecionado1Click
      end
      object Excluiitemprodutoselecionado1: TMenuItem
        Caption = '&Exclui item (produto) selecionado'
        OnClick = Excluiitemprodutoselecionado1Click
      end
      object N3: TMenuItem
        Caption = '-'
      end
      object AtualizaValoresdoitem1: TMenuItem
        Caption = '&Atualiza &Valores do item'
        OnClick = AtualizaValoresdoitem1Click
      end
    end
    object Partedoitemprodutoselecionado1: TMenuItem
      Caption = '&Parte do item (produto) selecionado'
      object Adicionapartesaoitemselecionado1: TMenuItem
        Caption = '&Adiciona partes ao produto selecionado'
        Enabled = False
        OnClick = Adicionapartesaoitemselecionado1Click
      end
      object Excluipartedoprodutoselecionado1: TMenuItem
        Caption = '&Exclui parte do produto selecionado'
        Enabled = False
        OnClick = Excluipartedoprodutoselecionado1Click
      end
    end
  end
  object QrCustomizados: TMySQLQuery
    BeforeClose = QrCustomizadosBeforeClose
    AfterScroll = QrCustomizadosAfterScroll
    SQL.Strings = (
      'SELECT pvi.Controle, pvi.GraGruX, pvi.ValLiq,'
      'pvi.MedidaC, pvi.MedidaL, pvi.MedidaA, pvi.MedidaE,'
      'gti.Codigo GRATAMCAD, gti.Controle GRATAMITS, '
      'gti.Nome NO_TAM, gcc.Nome NO_COR'
      'FROM pedivdaits pvi'
      'LEFT JOIN gragrux   ggx ON ggx.Controle=pvi.GraGruX'
      'LEFT JOIN gratamits gti ON gti.Controle=ggx.GraTamI'
      'LEFT JOIN gragruc ggc ON ggc.Controle=ggx.GraGruC'
      'LEFT JOIN gracorcad gcc ON gcc.Codigo=ggc.GraCorCad'
      'LEFT JOIN gragru1   gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'WHERE pvi.Customizad=1'
      'AND pvi.Codigo=:P0'
      'AND gg1.Nivel1=:P1')
    Left = 476
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrCustomizadosControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrCustomizadosGRATAMCAD: TIntegerField
      FieldName = 'GRATAMCAD'
    end
    object QrCustomizadosGRATAMITS: TAutoIncField
      FieldName = 'GRATAMITS'
    end
    object QrCustomizadosNO_TAM: TWideStringField
      FieldName = 'NO_TAM'
      Size = 5
    end
    object QrCustomizadosNO_COR: TWideStringField
      FieldName = 'NO_COR'
      Size = 30
    end
    object QrCustomizadosGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrCustomizadosMedidaC: TFloatField
      FieldName = 'MedidaC'
      DisplayFormat = '#,###,###,##0.0000;-#,###,###,##0.0000; '
    end
    object QrCustomizadosMedidaL: TFloatField
      FieldName = 'MedidaL'
      DisplayFormat = '#,###,###,##0.0000;-#,###,###,##0.0000; '
    end
    object QrCustomizadosMedidaA: TFloatField
      FieldName = 'MedidaA'
      DisplayFormat = '#,###,###,##0.0000;-#,###,###,##0.0000; '
    end
    object QrCustomizadosMedidaE: TFloatField
      FieldName = 'MedidaE'
      DisplayFormat = '#,###,###,##0.000000;-#,###,###,##0.000000; '
    end
    object QrCustomizadosValLiq: TFloatField
      FieldName = 'ValLiq'
      DisplayFormat = '#,###,###,##0.00'
    end
  end
  object DsCustomizados: TDataSource
    DataSet = QrCustomizados
    Left = 504
    Top = 8
  end
  object QrPediVdaCuz: TMySQLQuery
    OnCalcFields = QrPediVdaCuzCalcFields
    SQL.Strings = (
      'SELECT gti.Nome NO_TAM, gcc.Nome NO_COR, '
      'mpc.Nome NO_PARTE, gg1.Nome NO_GRUPO, '
      'gg1.SiglaCustm, pvc.* '
      'FROM pedivdacuz pvc'
      'LEFT JOIN gragrux   ggx ON ggx.Controle=pvc.GraGruX'
      'LEFT JOIN gratamits gti ON gti.Controle=ggx.GraTamI'
      'LEFT JOIN gragruc ggc ON ggc.Controle=ggx.GraGruC'
      'LEFT JOIN gracorcad gcc ON gcc.Codigo=ggc.GraCorCad'
      'LEFT JOIN gragru1   gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'LEFT JOIN matpartcad mpc ON mpc.Codigo=pvc.MatPartCad'
      'WHERE pvc.Controle=:P0')
    Left = 536
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPediVdaCuzNO_PARTE: TWideStringField
      FieldName = 'NO_PARTE'
      Size = 50
    end
    object QrPediVdaCuzNO_GRUPO: TWideStringField
      FieldName = 'NO_GRUPO'
      Size = 30
    end
    object QrPediVdaCuzNO_TAM: TWideStringField
      FieldName = 'NO_TAM'
      Size = 5
    end
    object QrPediVdaCuzNO_COR: TWideStringField
      FieldName = 'NO_COR'
      Size = 30
    end
    object QrPediVdaCuzControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrPediVdaCuzConta: TIntegerField
      FieldName = 'Conta'
    end
    object QrPediVdaCuzMatPartCad: TIntegerField
      FieldName = 'MatPartCad'
    end
    object QrPediVdaCuzGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrPediVdaCuzMedidaC: TFloatField
      FieldName = 'MedidaC'
      DisplayFormat = '#,###,###,##0.0000;-#,###,###,##0.0000; '
    end
    object QrPediVdaCuzMedidaL: TFloatField
      FieldName = 'MedidaL'
      DisplayFormat = '#,###,###,##0.0000;-#,###,###,##0.0000; '
    end
    object QrPediVdaCuzMedidaA: TFloatField
      FieldName = 'MedidaA'
      DisplayFormat = '#,###,###,##0.0000;-#,###,###,##0.0000; '
    end
    object QrPediVdaCuzMedidaE: TFloatField
      FieldName = 'MedidaE'
      DisplayFormat = '#,###,###,##0.000000;-#,###,###,##0.000000; '
    end
    object QrPediVdaCuzQuantP: TFloatField
      FieldName = 'QuantP'
    end
    object QrPediVdaCuzQuantX: TFloatField
      FieldName = 'QuantX'
    end
    object QrPediVdaCuzPrecoO: TFloatField
      FieldName = 'PrecoO'
      DisplayFormat = '#,###,###,##0.0000;-#,###,###,##0.0000; '
    end
    object QrPediVdaCuzPrecoR: TFloatField
      FieldName = 'PrecoR'
      DisplayFormat = '#,###,###,##0.0000;-#,###,###,##0.0000; '
    end
    object QrPediVdaCuzPrecoF: TFloatField
      FieldName = 'PrecoF'
      DisplayFormat = '#,###,###,##0.0000;-#,###,###,##0.0000; '
    end
    object QrPediVdaCuzValBru: TFloatField
      FieldName = 'ValBru'
      DisplayFormat = '#,###,###,##0.0000;-#,###,###,##0.0000; '
    end
    object QrPediVdaCuzDescoP: TFloatField
      FieldName = 'DescoP'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrPediVdaCuzDescoV: TFloatField
      FieldName = 'DescoV'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrPediVdaCuzPerCustom: TFloatField
      FieldName = 'PerCustom'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrPediVdaCuzValLiq: TFloatField
      FieldName = 'ValLiq'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrPediVdaCuzTipDimens: TSmallintField
      FieldName = 'TipDimens'
    end
    object QrPediVdaCuzMedidaC_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'MedidaC_TXT'
      Calculated = True
    end
    object QrPediVdaCuzMedidaL_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'MedidaL_TXT'
      Calculated = True
    end
    object QrPediVdaCuzMedidaA_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'MedidaA_TXT'
      Calculated = True
    end
    object QrPediVdaCuzMedidaE_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'MedidaE_TXT'
      Calculated = True
    end
    object QrPediVdaCuzSiglaCustm: TWideStringField
      FieldName = 'SiglaCustm'
      Size = 15
    end
  end
  object DsPediVdaCuz: TDataSource
    DataSet = QrPediVdaCuz
    Left = 564
    Top = 8
  end
  object frxDsCli: TfrxDBDataset
    UserName = 'frxDsCli'
    CloseDataSource = False
    FieldAliases.Strings = (
      'E_ALL=E_ALL'
      'CNPJ_TXT=CNPJ_TXT'
      'NOME_TIPO_DOC=NOME_TIPO_DOC'
      'TE1_TXT=TE1_TXT'
      'FAX_TXT=FAX_TXT'
      'NUMERO_TXT=NUMERO_TXT'
      'CEP_TXT=CEP_TXT'
      'Codigo=Codigo'
      'Tipo=Tipo'
      'CodUsu=CodUsu'
      'NOME_ENT=NOME_ENT'
      'CNPJ_CPF=CNPJ_CPF'
      'IE_RG=IE_RG'
      'RUA=RUA'
      'COMPL=COMPL'
      'BAIRRO=BAIRRO'
      'CIDADE=CIDADE'
      'NOMELOGRAD=NOMELOGRAD'
      'NOMEUF=NOMEUF'
      'Pais=Pais'
      'ENDEREF=ENDEREF'
      'TE1=TE1'
      'FAX=FAX'
      'IE=IE'
      'UF=UF'
      'CAD_FEDERAL=CAD_FEDERAL'
      'CAD_ESTADUAL=CAD_ESTADUAL'
      'IE_TXT=IE_TXT'
      'NUMERO=NUMERO'
      'CEP=CEP')
    DataSet = QrCli
    BCDToCurrency = False
    Left = 776
    Top = 4
  end
  object QrCli: TMySQLQuery
    OnCalcFields = QrCliCalcFields
    SQL.Strings = (
      'SELECT en.Codigo, Tipo, CodUsu, IE, '
      
        'CASE WHEN en.Tipo=0 THEN en.RazaoSocial ELSE en.Nome     END NOM' +
        'E_ENT, '
      
        'CASE WHEN en.Tipo=0 THEN en.CNPJ        ELSE en.CPF      END CNP' +
        'J_CPF, '
      
        'CASE WHEN en.Tipo=0 THEN en.IE          ELSE en.RG       END IE_' +
        'RG, '
      
        'CASE WHEN en.Tipo=0 THEN en.ERua        ELSE en.PRua     END RUA' +
        ', '
      
        'CASE WHEN en.Tipo=0 THEN en.ENumero + 0.000 ELSE en.PNumero + 0.' +
        '000 END NUMERO,'
      
        'CASE WHEN en.Tipo=0 THEN en.ECompl      ELSE en.PCompl   END COM' +
        'PL, '
      
        'CASE WHEN en.Tipo=0 THEN en.EBairro     ELSE en.PBairro  END BAI' +
        'RRO, '
      
        'CASE WHEN en.Tipo=0 THEN en.ECidade     ELSE en.PCidade  END CID' +
        'ADE, '
      
        'CASE WHEN en.Tipo=0 THEN en.EUF + 0.000     ELSE en.PUF + 0.000 ' +
        ' END UF, '
      
        'CASE WHEN en.Tipo=0 THEN lle.Nome       ELSE llp.Nome    END NOM' +
        'ELOGRAD, '
      
        'CASE WHEN en.Tipo=0 THEN ufe.Nome       ELSE ufp.Nome    END NOM' +
        'EUF, '
      
        'CASE WHEN en.Tipo=0 THEN en.EPais       ELSE en.PPais    END Pai' +
        's, '
      
        '/*CASE WHEN en.Tipo=0 THEN en.ELograd     ELSE en.PLograd END Lo' +
        'grad,*/'
      
        'CASE WHEN en.Tipo=0 THEN en.ECEP + 0.000 ELSE en.PCEP + 0.000 EN' +
        'D CEP,'
      
        'CASE WHEN en.Tipo=0 THEN en.EEndeRef    ELSE en.PEndeRef END END' +
        'EREF, '
      
        'CASE WHEN en.Tipo=0 THEN en.ETe1        ELSE en.PTe1     END TE1' +
        ', '
      
        'CASE WHEN en.Tipo=0 THEN en.EFax        ELSE en.PFax     END FAX' +
        ', '
      
        'IF(en.Tipo=0, "CNPJ", "CPF") CAD_FEDERAL, IF(en.Tipo=0, "I.E.", ' +
        '"RG") CAD_ESTADUAL'
      'FROM entidades en '
      'LEFT JOIN ufs ufe ON ufe.Codigo=en.EUF'
      'LEFT JOIN ufs ufp ON ufp.Codigo=en.PUF'
      'LEFT JOIN listalograd lle ON lle.Codigo=en.ELograd'
      'LEFT JOIN listalograd llp ON llp.Codigo=en.PLograd'
      'WHERE en.Codigo=:P0')
    Left = 748
    Top = 4
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrCliE_ALL: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'E_ALL'
      Size = 256
      Calculated = True
    end
    object QrCliCNPJ_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CNPJ_TXT'
      Size = 30
      Calculated = True
    end
    object QrCliNOME_TIPO_DOC: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOME_TIPO_DOC'
      Size = 10
      Calculated = True
    end
    object QrCliTE1_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TE1_TXT'
      Size = 40
      Calculated = True
    end
    object QrCliFAX_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'FAX_TXT'
      Size = 40
      Calculated = True
    end
    object QrCliNUMERO_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NUMERO_TXT'
      Size = 30
      Calculated = True
    end
    object QrCliCEP_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CEP_TXT'
      Calculated = True
    end
    object QrCliCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrCliTipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrCliCodUsu: TIntegerField
      FieldName = 'CodUsu'
      Required = True
    end
    object QrCliNOME_ENT: TWideStringField
      FieldName = 'NOME_ENT'
      Size = 100
    end
    object QrCliCNPJ_CPF: TWideStringField
      FieldName = 'CNPJ_CPF'
      Size = 18
    end
    object QrCliIE_RG: TWideStringField
      FieldName = 'IE_RG'
    end
    object QrCliRUA: TWideStringField
      FieldName = 'RUA'
      Size = 30
    end
    object QrCliCOMPL: TWideStringField
      FieldName = 'COMPL'
      Size = 30
    end
    object QrCliBAIRRO: TWideStringField
      FieldName = 'BAIRRO'
      Size = 30
    end
    object QrCliCIDADE: TWideStringField
      FieldName = 'CIDADE'
      Size = 25
    end
    object QrCliNOMELOGRAD: TWideStringField
      FieldName = 'NOMELOGRAD'
      Size = 10
    end
    object QrCliNOMEUF: TWideStringField
      FieldName = 'NOMEUF'
      Size = 2
    end
    object QrCliPais: TWideStringField
      FieldName = 'Pais'
    end
    object QrCliENDEREF: TWideStringField
      FieldName = 'ENDEREF'
      Size = 100
    end
    object QrCliTE1: TWideStringField
      FieldName = 'TE1'
    end
    object QrCliFAX: TWideStringField
      FieldName = 'FAX'
    end
    object QrCliIE: TWideStringField
      FieldName = 'IE'
    end
    object QrCliCAD_FEDERAL: TWideStringField
      FieldName = 'CAD_FEDERAL'
      Required = True
      Size = 4
    end
    object QrCliCAD_ESTADUAL: TWideStringField
      FieldName = 'CAD_ESTADUAL'
      Required = True
      Size = 4
    end
    object QrCliIE_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'IE_TXT'
      Size = 50
      Calculated = True
    end
    object QrCliNUMERO: TFloatField
      FieldName = 'NUMERO'
    end
    object QrCliCEP: TFloatField
      FieldName = 'CEP'
    end
    object QrCliUF: TFloatField
      FieldName = 'UF'
    end
  end
  object frxPED_VENDA_001_01: TfrxReport
    Version = '5.6.18'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39956.739524675900000000
    ReportOptions.LastChange = 39956.739524675900000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      
        '  MeVARF_CODI_FRX.Text := Copy(Report.Name, 4, Length(Report.Nam' +
        'e));'
      '  if <MeuLogo3x1Existe> = True then '
      '    Picture1.LoadFromFile(<MeuLogo3x1Caminho>);'
      'end.')
    OnGetValue = frxPED_VENDA_001_01GetValue
    Left = 720
    Top = 4
    Datasets = <
      item
        DataSet = frxDsCli
        DataSetName = 'frxDsCli'
      end
      item
        DataSetName = 'frxDsEndereco'
      end
      item
        DataSet = frxDsItsC
        DataSetName = 'frxDsItsC'
      end
      item
        DataSet = frxDsItsN
        DataSetName = 'frxDsItsN'
      end
      item
        DataSet = frxDsItsZ
        DataSetName = 'frxDsItsZ'
      end
      item
        DataSet = frxDsPediVda
        DataSetName = 'frxDsPediVda'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 15.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        Height = 192.756030000000000000
        Top = 18.897650000000000000
        Width = 699.213050000000000000
        object Shape1: TfrxShapeView
          Width = 699.213050000000000000
          Height = 56.692950000000000000
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo13: TfrxMemoView
          Left = 154.960730000000000000
          Width = 393.071120000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsEndereco."NO_2_ENT"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line1: TfrxLineView
          Left = 551.811380000000000000
          Top = 18.897650000000000000
          Width = 147.401670000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo14: TfrxMemoView
          Left = 154.960730000000000000
          Top = 18.897650000000000000
          Width = 393.071120000000000000
          Height = 37.795275590000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsEndereco."E_ALL"]')
          ParentFont = False
        end
        object Memo15: TfrxMemoView
          Left = 548.031850000000000000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[Date]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo16: TfrxMemoView
          Left = 548.031850000000000000
          Top = 18.897650000000000000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page] de [TotalPages]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Picture1: TfrxPictureView
          Left = 3.779530000000000000
          Top = 3.779530000000000000
          Width = 147.401574800000000000
          Height = 49.133890000000000000
          HightQuality = False
          Transparent = False
          TransparentColor = clWhite
        end
        object Shape2: TfrxShapeView
          Top = 68.031540000000000000
          Width = 699.213050000000000000
          Height = 75.590600000000000000
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object frxMemoView1: TfrxMemoView
          Left = 75.590600000000000000
          Top = 68.031540000000000000
          Width = 616.063390000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsCli."CodUsu"] - [frxDsCli."NOME_ENT"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo4: TfrxMemoView
          Left = 75.590600000000000000
          Top = 86.929190000000000000
          Width = 616.063390000000000000
          Height = 37.795275590000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsCli."E_ALL"]')
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          Left = 3.779530000000000000
          Top = 68.031540000000000000
          Width = 71.811070000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            'Destinat'#225'rio:')
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          Left = 3.779530000000000000
          Top = 151.181200000000000000
          Width = 687.874460000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            
              'INFORME DO PEDIDO N'#186' [FormatFloat('#39'000000'#39', <frxDsPediVda."CodUs' +
              'u">)]')
          ParentFont = False
        end
        object Memo29: TfrxMemoView
          Left = 132.283550000000000000
          Top = 124.724490000000000000
          Width = 170.078850000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsCli."CNPJ_TXT"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo30: TfrxMemoView
          Left = 75.590600000000000000
          Top = 124.724490000000000000
          Width = 56.692950000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsCli."CAD_FEDERAL"]:')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo38: TfrxMemoView
          Left = 359.055350000000000000
          Top = 124.724490000000000000
          Width = 151.181200000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsCli."IE_TXT"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo39: TfrxMemoView
          Left = 302.362400000000000000
          Top = 124.724490000000000000
          Width = 56.692950000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsCli."CAD_ESTADUAL"]:')
          ParentFont = False
          VAlign = vaCenter
        end
        object frxDsEnderecoTE1: TfrxMemoView
          Left = 548.031850000000000000
          Top = 37.795300000000000000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsEndereco."TE1_TXT"]')
          ParentFont = False
        end
        object Memo41: TfrxMemoView
          Left = 566.929500000000000000
          Top = 124.724490000000000000
          Width = 124.724490000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsCli."TE1_TXT"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo42: TfrxMemoView
          Left = 510.236550000000000000
          Top = 124.724490000000000000
          Width = 56.692950000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            'Telefone:')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo64: TfrxMemoView
          Top = 173.858380000000000000
          Width = 302.362400000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            'N'#186' DO PEDIDO DO CLIENTE: [frxDsPediVda."PedidoCli"]')
          ParentFont = False
        end
        object Memo65: TfrxMemoView
          Left = 306.141930000000000000
          Top = 173.858380000000000000
          Width = 393.071120000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'REPRESENTANTE: [frxDsPediVda."NOMEACC"]')
          ParentFont = False
        end
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        Height = 13.228346460000000000
        Top = 695.433520000000000000
        Width = 699.213050000000000000
        object Memo1: TfrxMemoView
          Left = 7.559060000000000000
          Width = 362.834880000000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - software customizado')
          ParentFont = False
          VAlign = vaCenter
        end
        object MeVARF_CODI_FRX: TfrxMemoView
          Left = 370.393940000000000000
          Width = 321.260050000000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'VARF_CODI_FRX')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object GroupHeader1: TfrxGroupHeader
        FillType = ftBrush
        Height = 15.118110240000000000
        Top = 272.126160000000000000
        Width = 699.213050000000000000
        Condition = 'frxDsItsN."KGT"'
        object Memo40: TfrxMemoView
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'C'#243'digo')
          ParentFont = False
          WordWrap = False
        end
        object MeTitD: TfrxMemoView
          Left = 52.913420000000000000
          Width = 249.448857950000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Descri'#231#227'o')
          ParentFont = False
        end
        object MeTitB: TfrxMemoView
          Left = 498.897960000000000000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Pre'#231'o')
          ParentFont = False
          WordWrap = False
        end
        object MeTitA: TfrxMemoView
          Left = 445.984540000000000000
          Width = 52.913390710000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Qtde.')
          ParentFont = False
          WordWrap = False
        end
        object Memo7: TfrxMemoView
          Left = 302.362400000000000000
          Width = 102.047244090000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Cor')
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          Left = 404.409710000000000000
          Width = 41.574803150000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Tamanho')
          ParentFont = False
        end
        object Memo18: TfrxMemoView
          Left = 638.740570000000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Vlr. Total')
          ParentFont = False
          WordWrap = False
        end
        object Memo20: TfrxMemoView
          Left = 578.268090000000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Vlr. Unit.')
          ParentFont = False
          WordWrap = False
        end
        object Memo22: TfrxMemoView
          Left = 544.252320000000000000
          Width = 34.015748030000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '% Desc.')
          ParentFont = False
          WordWrap = False
        end
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        Height = 15.118110240000000000
        Top = 309.921460000000000000
        Width = 699.213050000000000000
        DataSet = frxDsItsN
        DataSetName = 'frxDsItsN'
        RowCount = 0
        object Memo2: TfrxMemoView
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          DataField = 'CU_NIVEL1'
          DataSet = frxDsItsN
          DataSetName = 'frxDsItsN'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsItsN."CU_NIVEL1"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo3: TfrxMemoView
          Left = 52.913420000000000000
          Width = 249.448857950000000000
          Height = 15.118110240000000000
          DataField = 'NO_NIVEL1'
          DataSet = frxDsItsN
          DataSetName = 'frxDsItsN'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsItsN."NO_NIVEL1"]')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          Left = 498.897960000000000000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          DataField = 'PrecoR'
          DataSet = frxDsItsN
          DataSetName = 'frxDsItsN'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsItsN."PrecoR"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo11: TfrxMemoView
          Left = 445.984540000000000000
          Width = 52.913390710000000000
          Height = 15.118110240000000000
          DataField = 'QuantP'
          DataSet = frxDsItsN
          DataSetName = 'frxDsItsN'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsItsN."QuantP"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo12: TfrxMemoView
          Left = 302.362400000000000000
          Width = 102.047244090000000000
          Height = 15.118110240000000000
          DataField = 'NO_COR'
          DataSet = frxDsItsN
          DataSetName = 'frxDsItsN'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsItsN."NO_COR"]')
          ParentFont = False
        end
        object Memo17: TfrxMemoView
          Left = 404.409710000000000000
          Width = 41.574803150000000000
          Height = 15.118110240000000000
          DataField = 'NO_TAM'
          DataSet = frxDsItsN
          DataSetName = 'frxDsItsN'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsItsN."NO_TAM"]')
          ParentFont = False
        end
        object Memo19: TfrxMemoView
          Left = 638.740570000000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          DataField = 'ValLiq'
          DataSet = frxDsItsN
          DataSetName = 'frxDsItsN'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsItsN."ValLiq"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo21: TfrxMemoView
          Left = 578.268090000000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          DataField = 'PrecoF'
          DataSet = frxDsItsN
          DataSetName = 'frxDsItsN'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsItsN."PrecoF"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo23: TfrxMemoView
          Left = 544.252320000000000000
          Width = 34.015748030000000000
          Height = 15.118110240000000000
          DataField = 'DescoP'
          DataSet = frxDsItsN
          DataSetName = 'frxDsItsN'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsItsN."DescoP"]')
          ParentFont = False
          WordWrap = False
        end
      end
      object GroupFooter1: TfrxGroupFooter
        FillType = ftBrush
        Height = 3.779530000000000000
        Top = 347.716760000000000000
        Width = 699.213050000000000000
      end
      object GroupHeader2: TfrxGroupHeader
        FillType = ftBrush
        Height = 34.015760240000000000
        Top = 374.173470000000000000
        Width = 699.213050000000000000
        Condition = 'frxDsItsC."KGT"'
        object Memo8: TfrxMemoView
          Width = 249.448857950000000000
          Height = 18.897640240000000000
          DataSet = frxDsItsN
          DataSetName = 'frxDsItsN'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Itens customiz'#225'veis:')
          ParentFont = False
        end
        object Memo26: TfrxMemoView
          Top = 18.897650000000000000
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          DataSet = frxDsItsN
          DataSetName = 'frxDsItsN'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'C'#243'digo')
          ParentFont = False
          WordWrap = False
        end
        object Memo27: TfrxMemoView
          Left = 52.913420000000000000
          Top = 18.897650000000000000
          Width = 238.110267950000000000
          Height = 15.118110240000000000
          DataSet = frxDsItsN
          DataSetName = 'frxDsItsN'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Descri'#231#227'o')
          ParentFont = False
        end
        object Memo28: TfrxMemoView
          Left = 464.882190000000000000
          Top = 18.897650000000000000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          DataSet = frxDsItsN
          DataSetName = 'frxDsItsN'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Pre'#231'o')
          ParentFont = False
          WordWrap = False
        end
        object Memo31: TfrxMemoView
          Left = 411.968770000000000000
          Top = 18.897650000000000000
          Width = 52.913390710000000000
          Height = 15.118110240000000000
          DataSet = frxDsItsN
          DataSetName = 'frxDsItsN'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Qtde.')
          ParentFont = False
          WordWrap = False
        end
        object Memo32: TfrxMemoView
          Left = 291.023810000000000000
          Top = 18.897650000000000000
          Width = 79.370064090000000000
          Height = 15.118110240000000000
          DataSet = frxDsItsN
          DataSetName = 'frxDsItsN'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Cor')
          ParentFont = False
        end
        object Memo33: TfrxMemoView
          Left = 370.393940000000000000
          Top = 18.897650000000000000
          Width = 41.574803150000000000
          Height = 15.118110240000000000
          DataSet = frxDsItsN
          DataSetName = 'frxDsItsN'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Tamanho')
          ParentFont = False
        end
        object Memo49: TfrxMemoView
          Left = 638.740570000000000000
          Top = 18.897650000000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          DataSet = frxDsItsN
          DataSetName = 'frxDsItsN'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Vlr. Total')
          ParentFont = False
          WordWrap = False
        end
        object Memo59: TfrxMemoView
          Left = 578.268090000000000000
          Top = 18.897650000000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          DataSet = frxDsItsN
          DataSetName = 'frxDsItsN'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Vlr. Unit.')
          ParentFont = False
          WordWrap = False
        end
        object Memo60: TfrxMemoView
          Left = 544.252320000000000000
          Top = 18.897650000000000000
          Width = 34.015748030000000000
          Height = 15.118110240000000000
          DataSet = frxDsItsN
          DataSetName = 'frxDsItsN'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '% Desc.')
          ParentFont = False
          WordWrap = False
        end
        object Memo61: TfrxMemoView
          Left = 510.236550000000000000
          Top = 18.897650000000000000
          Width = 34.015748030000000000
          Height = 15.118110240000000000
          DataSet = frxDsItsC
          DataSetName = 'frxDsItsC'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '% Pers.')
          ParentFont = False
          WordWrap = False
        end
      end
      object MasterData2: TfrxMasterData
        FillType = ftBrush
        Height = 30.236230240000000000
        Top = 430.866420000000000000
        Width = 699.213050000000000000
        DataSet = frxDsItsC
        DataSetName = 'frxDsItsC'
        RowCount = 0
        object Memo34: TfrxMemoView
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          DataField = 'CU_NIVEL1'
          DataSet = frxDsItsC
          DataSetName = 'frxDsItsC'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsItsC."CU_NIVEL1"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo35: TfrxMemoView
          Left = 52.913420000000000000
          Width = 238.110267950000000000
          Height = 15.118110240000000000
          DataField = 'NO_NIVEL1'
          DataSet = frxDsItsC
          DataSetName = 'frxDsItsC'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsItsC."NO_NIVEL1"]')
          ParentFont = False
        end
        object Memo36: TfrxMemoView
          Left = 464.882190000000000000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          DataField = 'PrecoR'
          DataSet = frxDsItsC
          DataSetName = 'frxDsItsC'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsItsC."PrecoR"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo37: TfrxMemoView
          Left = 411.968770000000000000
          Width = 52.913390710000000000
          Height = 15.118110240000000000
          DataField = 'QuantP'
          DataSet = frxDsItsC
          DataSetName = 'frxDsItsC'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsItsC."QuantP"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo43: TfrxMemoView
          Left = 291.023810000000000000
          Width = 79.370064090000000000
          Height = 15.118110240000000000
          DataField = 'NO_COR'
          DataSet = frxDsItsC
          DataSetName = 'frxDsItsC'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsItsC."NO_COR"]')
          ParentFont = False
        end
        object Memo44: TfrxMemoView
          Left = 370.393940000000000000
          Width = 41.574803150000000000
          Height = 15.118110240000000000
          DataField = 'NO_TAM'
          DataSet = frxDsItsC
          DataSetName = 'frxDsItsC'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsItsC."NO_TAM"]')
          ParentFont = False
        end
        object Memo45: TfrxMemoView
          Left = 638.740570000000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          DataField = 'ValLiq'
          DataSet = frxDsItsC
          DataSetName = 'frxDsItsC'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsItsC."ValLiq"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo46: TfrxMemoView
          Left = 578.268090000000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          DataField = 'PrecoF'
          DataSet = frxDsItsC
          DataSetName = 'frxDsItsC'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsItsC."PrecoF"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo47: TfrxMemoView
          Left = 544.252320000000000000
          Width = 34.015748030000000000
          Height = 15.118110240000000000
          DataField = 'DescoP'
          DataSet = frxDsItsC
          DataSetName = 'frxDsItsC'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsItsC."DescoP"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo48: TfrxMemoView
          Top = 15.118120000000000000
          Width = 699.212927950000000000
          Height = 15.118110240000000000
          DataField = 'DESCRICAO'
          DataSet = frxDsItsC
          DataSetName = 'frxDsItsC'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsItsC."DESCRICAO"]')
          ParentFont = False
        end
        object Memo24: TfrxMemoView
          Left = 510.236550000000000000
          Width = 34.015748030000000000
          Height = 15.118110240000000000
          DataField = 'PercCustom'
          DataSet = frxDsItsC
          DataSetName = 'frxDsItsC'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsItsC."PercCustom"]')
          ParentFont = False
          WordWrap = False
        end
      end
      object GroupFooter2: TfrxGroupFooter
        FillType = ftBrush
        Height = 1.889763779527559000
        Top = 585.827150000000000000
        Width = 699.213050000000000000
      end
      object GroupHeader3: TfrxGroupHeader
        FillType = ftBrush
        Top = 483.779840000000000000
        Width = 699.213050000000000000
        Condition = 'frxDsItsZ."KGT"'
      end
      object GroupFooter3: TfrxGroupFooter
        FillType = ftBrush
        Height = 1.889763780000000000
        Top = 559.370440000000000000
        Width = 699.213050000000000000
      end
      object DetailData1: TfrxDetailData
        FillType = ftBrush
        Height = 30.236230240000000000
        Top = 506.457020000000000000
        Width = 699.213050000000000000
        DataSet = frxDsItsZ
        DataSetName = 'frxDsItsZ'
        RowCount = 0
        object Memo50: TfrxMemoView
          Left = 52.913420000000000000
          Width = 238.110267950000000000
          Height = 15.118110240000000000
          DataField = 'NO_NIVEL1'
          DataSet = frxDsItsZ
          DataSetName = 'frxDsItsZ'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsItsZ."NO_NIVEL1"]')
          ParentFont = False
        end
        object Memo51: TfrxMemoView
          Left = 464.882190000000000000
          Width = 45.354330710000000000
          Height = 15.118110240000000000
          DataField = 'PrecoR'
          DataSet = frxDsItsZ
          DataSetName = 'frxDsItsZ'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clGray
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsItsZ."PrecoR"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo52: TfrxMemoView
          Left = 411.968770000000000000
          Width = 52.913390710000000000
          Height = 15.118110240000000000
          DataField = 'QuantP'
          DataSet = frxDsItsZ
          DataSetName = 'frxDsItsZ'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsItsZ."QuantP"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo53: TfrxMemoView
          Left = 291.023810000000000000
          Width = 79.370064090000000000
          Height = 15.118110240000000000
          DataField = 'NO_COR'
          DataSet = frxDsItsZ
          DataSetName = 'frxDsItsZ'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsItsZ."NO_COR"]')
          ParentFont = False
        end
        object Memo54: TfrxMemoView
          Left = 370.393940000000000000
          Width = 41.574803150000000000
          Height = 15.118110240000000000
          DataField = 'NO_TAM'
          DataSet = frxDsItsZ
          DataSetName = 'frxDsItsZ'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsItsZ."NO_TAM"]')
          ParentFont = False
        end
        object Memo55: TfrxMemoView
          Left = 638.740570000000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          DataField = 'ValLiq'
          DataSet = frxDsItsZ
          DataSetName = 'frxDsItsZ'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clGray
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsItsZ."ValLiq"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo56: TfrxMemoView
          Left = 578.268090000000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          DataField = 'PrecoF'
          DataSet = frxDsItsZ
          DataSetName = 'frxDsItsZ'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clGray
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsItsZ."PrecoF"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo57: TfrxMemoView
          Left = 544.252320000000000000
          Width = 34.015748030000000000
          Height = 15.118110240000000000
          DataField = 'DescoP'
          DataSet = frxDsItsZ
          DataSetName = 'frxDsItsZ'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clGray
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsItsZ."DescoP"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo58: TfrxMemoView
          Left = 52.913420000000000000
          Top = 15.118120000000000000
          Width = 646.299507950000000000
          Height = 15.118110240000000000
          DataField = 'DESCRICAO'
          DataSet = frxDsItsZ
          DataSetName = 'frxDsItsZ'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsItsZ."DESCRICAO"]')
          ParentFont = False
        end
        object Memo25: TfrxMemoView
          Left = 510.236550000000000000
          Width = 34.015748030000000000
          Height = 15.118110240000000000
          DataField = 'PerCustom'
          DataSet = frxDsItsZ
          DataSetName = 'frxDsItsZ'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clGray
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsItsZ."PerCustom"]')
          ParentFont = False
          WordWrap = False
        end
      end
      object ReportSummary1: TfrxReportSummary
        FillType = ftBrush
        Height = 22.677180000000000000
        Top = 650.079160000000000000
        Width = 699.213050000000000000
        object Memo62: TfrxMemoView
          Left = 578.268090000000000000
          Width = 120.944920940000000000
          Height = 15.118110240000000000
          DataSet = frxDsItsZ
          DataSetName = 'frxDsItsZ'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPediVda."ValLiq"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo63: TfrxMemoView
          Left = 340.157700000000000000
          Width = 238.110267950000000000
          Height = 15.118110240000000000
          DataSet = frxDsItsZ
          DataSetName = 'frxDsItsZ'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'TOTAL ')
          ParentFont = False
        end
      end
    end
  end
  object QrItsN: TMySQLQuery
    SQL.Strings = (
      'SELECT gg1.CodUsu CU_NIVEL1, gg1.Nome NO_NIVEL1,'
      'gti.Nome NO_TAM, gcc.Nome NO_COR,   '
      'pvi.QuantP, pvi.PrecoR, pvi.DescoP, '
      'pvi.PrecoF, pvi.ValBru, pvi.ValLiq, 0 KGT'
      'FROM pedivdaits pvi'
      'LEFT JOIN gragrux ggx ON ggx.Controle=pvi.GraGrux'
      'LEFT JOIN gratamits gti ON gti.Controle=ggx.GraTamI'
      'LEFT JOIN gragruc ggc ON ggc.Controle=ggx.GraGruC'
      'LEFT JOIN gracorcad gcc ON gcc.Codigo=ggc.GraCorCad'
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1'
      'WHERE pvi.Codigo=:P0'
      'AND pvi.Customizad=0'
      '')
    Left = 748
    Top = 32
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrItsNCU_NIVEL1: TIntegerField
      FieldName = 'CU_NIVEL1'
      Required = True
    end
    object QrItsNNO_NIVEL1: TWideStringField
      FieldName = 'NO_NIVEL1'
      Size = 30
    end
    object QrItsNNO_TAM: TWideStringField
      FieldName = 'NO_TAM'
      Size = 5
    end
    object QrItsNNO_COR: TWideStringField
      FieldName = 'NO_COR'
      Size = 30
    end
    object QrItsNQuantP: TFloatField
      FieldName = 'QuantP'
    end
    object QrItsNPrecoR: TFloatField
      FieldName = 'PrecoR'
    end
    object QrItsNDescoP: TFloatField
      FieldName = 'DescoP'
    end
    object QrItsNPrecoF: TFloatField
      FieldName = 'PrecoF'
    end
    object QrItsNValBru: TFloatField
      FieldName = 'ValBru'
    end
    object QrItsNValLiq: TFloatField
      FieldName = 'ValLiq'
    end
    object QrItsNKGT: TLargeintField
      FieldName = 'KGT'
      Required = True
    end
  end
  object QrItsC: TMySQLQuery
    AfterScroll = QrItsCAfterScroll
    OnCalcFields = QrItsCCalcFields
    SQL.Strings = (
      'SELECT gg1.CodUsu CU_NIVEL1, gg1.Nome NO_NIVEL1,'
      'gti.Nome NO_TAM, gcc.Nome NO_COR,  pvi.QuantP, '
      'pvi.PrecoR, pvi.DescoP, pvi.PrecoF, pvi.ValBru, pvi.ValLiq, '
      'pvi.Controle, pvi.MedidaC, pvi.MedidaL, pvi.MedidaA, '
      'pvi.MedidaE, PercCustom, pvi.InfAdCuztm, gg1.TipDimens,'
      'med.Medida1, med.Medida2, med.Medida3, med.Medida4, '
      'med.Sigla1, med.Sigla2, med.Sigla3, med.Sigla4, 0 KGT'
      'FROM pedivdaits pvi'
      'LEFT JOIN gragrux ggx ON ggx.Controle=pvi.GraGrux'
      'LEFT JOIN gratamits gti ON gti.Controle=ggx.GraTamI'
      'LEFT JOIN gragruc ggc ON ggc.Controle=ggx.GraGruC'
      'LEFT JOIN gracorcad gcc ON gcc.Codigo=ggc.GraCorCad'
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN medordem med ON med.Codigo=gg1.MedOrdem'
      'WHERE pvi.Codigo=:P0'
      'AND pvi.Customizad=1'
      '')
    Left = 748
    Top = 60
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrItsCCU_NIVEL1: TIntegerField
      FieldName = 'CU_NIVEL1'
      Required = True
    end
    object QrItsCNO_NIVEL1: TWideStringField
      FieldName = 'NO_NIVEL1'
      Size = 30
    end
    object QrItsCNO_TAM: TWideStringField
      FieldName = 'NO_TAM'
      Size = 5
    end
    object QrItsCNO_COR: TWideStringField
      FieldName = 'NO_COR'
      Size = 30
    end
    object QrItsCQuantP: TFloatField
      FieldName = 'QuantP'
    end
    object QrItsCPrecoR: TFloatField
      FieldName = 'PrecoR'
    end
    object QrItsCDescoP: TFloatField
      FieldName = 'DescoP'
    end
    object QrItsCPrecoF: TFloatField
      FieldName = 'PrecoF'
    end
    object QrItsCValBru: TFloatField
      FieldName = 'ValBru'
    end
    object QrItsCValLiq: TFloatField
      FieldName = 'ValLiq'
    end
    object QrItsCMedidaC: TFloatField
      FieldName = 'MedidaC'
    end
    object QrItsCMedidaL: TFloatField
      FieldName = 'MedidaL'
    end
    object QrItsCMedidaA: TFloatField
      FieldName = 'MedidaA'
    end
    object QrItsCMedidaE: TFloatField
      FieldName = 'MedidaE'
    end
    object QrItsCPercCustom: TFloatField
      FieldName = 'PercCustom'
    end
    object QrItsCInfAdCuztm: TIntegerField
      FieldName = 'InfAdCuztm'
    end
    object QrItsCMedida1: TWideStringField
      FieldName = 'Medida1'
    end
    object QrItsCMedida2: TWideStringField
      FieldName = 'Medida2'
    end
    object QrItsCMedida3: TWideStringField
      FieldName = 'Medida3'
    end
    object QrItsCMedida4: TWideStringField
      FieldName = 'Medida4'
    end
    object QrItsCSigla1: TWideStringField
      FieldName = 'Sigla1'
      Size = 6
    end
    object QrItsCSigla2: TWideStringField
      FieldName = 'Sigla2'
      Size = 6
    end
    object QrItsCSigla3: TWideStringField
      FieldName = 'Sigla3'
      Size = 6
    end
    object QrItsCSigla4: TWideStringField
      FieldName = 'Sigla4'
      Size = 6
    end
    object QrItsCDESCRICAO: TWideStringField
      DisplayWidth = 255
      FieldKind = fkCalculated
      FieldName = 'DESCRICAO'
      Size = 255
      Calculated = True
    end
    object QrItsCControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrItsCKGT: TLargeintField
      FieldName = 'KGT'
      Required = True
    end
    object QrItsCTipDimens: TSmallintField
      FieldName = 'TipDimens'
    end
  end
  object frxDsItsN: TfrxDBDataset
    UserName = 'frxDsItsN'
    CloseDataSource = False
    DataSet = QrItsN
    BCDToCurrency = False
    Left = 776
    Top = 32
  end
  object frxDsPediVda: TfrxDBDataset
    UserName = 'frxDsPediVda'
    CloseDataSource = False
    DataSet = QrPediVda
    BCDToCurrency = False
    Left = 28
    Top = 96
  end
  object frxDsItsC: TfrxDBDataset
    UserName = 'frxDsItsC'
    CloseDataSource = False
    DataSet = QrItsC
    BCDToCurrency = False
    Left = 776
    Top = 60
  end
  object QrItsZ: TMySQLQuery
    OnCalcFields = QrItsZCalcFields
    SQL.Strings = (
      'SELECT gg1.CodUsu CU_NIVEL1, gg1.Nome NO_NIVEL1,'
      'gg1.TipDimens, gti.Nome NO_TAM, gcc.Nome NO_COR,   '
      'pvi.QuantP, pvi.PrecoR, pvi.DescoP, pvi.PrecoF, pvi.ValBru, '
      'pvi.ValLiq, pvi.MedidaC, pvi.MedidaL, pvi.MedidaA, '
      'pvi.MedidaE, pvi.PerCustom, mpc.Nome NO_MatPartCad,'
      'med.Medida1, med.Medida2, med.Medida3, med.Medida4, '
      'med.Sigla1, med.Sigla2, med.Sigla3, med.Sigla4, 0 KGT'
      'FROM pedivdacuz pvi'
      'LEFT JOIN gragrux ggx ON ggx.Controle=pvi.GraGrux'
      'LEFT JOIN gratamits gti ON gti.Controle=ggx.GraTamI'
      'LEFT JOIN gragruc ggc ON ggc.Controle=ggx.GraGruC'
      'LEFT JOIN gracorcad gcc ON gcc.Codigo=ggc.GraCorCad'
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN medordem med ON med.Codigo=gg1.MedOrdem'
      'LEFT JOIN matpartcad mpc ON mpc.Codigo=pvi.MatPartCad'
      'WHERE pvi.Controle=:P0'
      '')
    Left = 748
    Top = 88
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrItsZCU_NIVEL1: TIntegerField
      FieldName = 'CU_NIVEL1'
      Required = True
    end
    object QrItsZNO_NIVEL1: TWideStringField
      FieldName = 'NO_NIVEL1'
      Size = 30
    end
    object QrItsZNO_TAM: TWideStringField
      FieldName = 'NO_TAM'
      Size = 5
    end
    object QrItsZNO_COR: TWideStringField
      FieldName = 'NO_COR'
      Size = 30
    end
    object QrItsZQuantP: TFloatField
      FieldName = 'QuantP'
    end
    object QrItsZPrecoR: TFloatField
      FieldName = 'PrecoR'
    end
    object QrItsZDescoP: TFloatField
      FieldName = 'DescoP'
    end
    object QrItsZPrecoF: TFloatField
      FieldName = 'PrecoF'
    end
    object QrItsZValBru: TFloatField
      FieldName = 'ValBru'
    end
    object QrItsZValLiq: TFloatField
      FieldName = 'ValLiq'
    end
    object QrItsZMedidaC: TFloatField
      FieldName = 'MedidaC'
    end
    object QrItsZMedidaL: TFloatField
      FieldName = 'MedidaL'
    end
    object QrItsZMedidaA: TFloatField
      FieldName = 'MedidaA'
    end
    object QrItsZMedidaE: TFloatField
      FieldName = 'MedidaE'
    end
    object QrItsZPerCustom: TFloatField
      FieldName = 'PerCustom'
    end
    object QrItsZNO_MatPartCad: TWideStringField
      FieldName = 'NO_MatPartCad'
      Size = 50
    end
    object QrItsZMedida1: TWideStringField
      FieldName = 'Medida1'
    end
    object QrItsZMedida2: TWideStringField
      FieldName = 'Medida2'
    end
    object QrItsZMedida3: TWideStringField
      FieldName = 'Medida3'
    end
    object QrItsZMedida4: TWideStringField
      FieldName = 'Medida4'
    end
    object QrItsZSigla1: TWideStringField
      FieldName = 'Sigla1'
      Size = 6
    end
    object QrItsZSigla2: TWideStringField
      FieldName = 'Sigla2'
      Size = 6
    end
    object QrItsZSigla3: TWideStringField
      FieldName = 'Sigla3'
      Size = 6
    end
    object QrItsZDESCRICAO: TWideStringField
      DisplayWidth = 255
      FieldKind = fkCalculated
      FieldName = 'DESCRICAO'
      Size = 255
      Calculated = True
    end
    object QrItsZSigla4: TWideStringField
      FieldName = 'Sigla4'
      Size = 6
    end
    object QrItsZKGT: TLargeintField
      FieldName = 'KGT'
      Required = True
    end
    object QrItsZTipDimens: TSmallintField
      FieldName = 'TipDimens'
    end
  end
  object frxDsItsZ: TfrxDBDataset
    UserName = 'frxDsItsZ'
    CloseDataSource = False
    DataSet = QrItsZ
    BCDToCurrency = False
    Left = 776
    Top = 88
  end
  object QrPediVdaIts: TMySQLQuery
    AfterOpen = QrPediVdaGruAfterOpen
    BeforeClose = QrPediVdaGruBeforeClose
    AfterScroll = QrPediVdaGruAfterScroll
    SQL.Strings = (
      'SELECT pvi.*'
      'FROM pedivdaits pvi'
      'WHERE pvi.Codigo=:P0')
    Left = 124
    Top = 340
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPediVdaItsControle: TIntegerField
      FieldName = 'Controle'
    end
  end
  object PMRegrFiscal: TPopupMenu
    Left = 701
    Top = 389
    object Estrio1: TMenuItem
      Caption = '&Estri'#231#227'o'
      OnClick = Estrio1Click
    end
    object Cadastro1: TMenuItem
      Caption = '&Cadastro'
      OnClick = Cadastro1Click
    end
  end
end
