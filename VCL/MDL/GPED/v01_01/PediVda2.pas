unit PediVda2;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls, Db, (*DBTables,*) JPEG, ExtDlgs,
  ZCF2, ResIntStrings, UnDmkProcFunc, UnGOTOy, UnInternalConsts,
  UnMsgInt, UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa,
  dmkGeral, dmkPermissoes, dmkEdit, dmkLabel, dmkRadioGroup, Grids, DBGrids,
  dmkDBGrid, Menus, dmkValUsu, dmkDBLookupComboBox, dmkEditCB, ComCtrls,
  dmkEditDateTimePicker, dmkCheckBox, frxClass, frxDBSet, dmkImage, DmkDAC_PF,
  UnDmkEnums, UnGrl_Geral, dmkLabelRotate, UnGrl_Vars, dmkMemo,
  UnInternalConsts3;

type
  TCampoIncrementoItemNFe = (ciifND=0, ciifVFrete=1, ciifVSeg=2, ciifVOutro=3);
  //
  TFmPediVda2 = class(TForm)
    PainelDados: TPanel;
    DsPediVda: TDataSource;
    QrPediVda: TmySQLQuery;
    PainelEdita: TPanel;
    dmkPermissoes1: TdmkPermissoes;
    PMPedidos: TPopupMenu;
    PMItens: TPopupMenu;
    Incluinovopedido1: TMenuItem;
    Alterapedidoatual1: TMenuItem;
    Excluipedidoatual1: TMenuItem;
    VuEmpresa: TdmkValUsu;
    QrPediVdaCodigo: TIntegerField;
    QrPediVdaCodUsu: TIntegerField;
    QrPediVdaEmpresa: TIntegerField;
    QrPediVdaCliente: TIntegerField;
    QrPediVdaDtaEmiss: TDateField;
    QrPediVdaDtaEntra: TDateField;
    QrPediVdaDtaInclu: TDateField;
    QrPediVdaDtaPrevi: TDateField;
    QrPediVdaNOMEEMP: TWideStringField;
    QrPediVdaNOMECLI: TWideStringField;
    QrPediVdaCIDADECLI: TWideStringField;
    QrPediVdaNOMEUF: TWideStringField;
    QrPediVdaFilial: TIntegerField;
    QrPediVdaPrioridade: TSmallintField;
    QrPediVdaCondicaoPG: TIntegerField;
    QrPediVdaMoeda: TIntegerField;
    QrPediVdaSituacao: TIntegerField;
    VuCondicaoPG: TdmkValUsu;
    VuCambioMda: TdmkValUsu;
    VuTabelaPrc: TdmkValUsu;
    QrPediVdaTabelaPrc: TIntegerField;
    VuMotivoSit: TdmkValUsu;
    QrPediVdaMotivoSit: TIntegerField;
    QrPediVdaLoteProd: TIntegerField;
    QrPediVdaPedidoCli: TWideStringField;
    QrPediVdaFretePor: TSmallintField;
    QrPediVdaTransporta: TIntegerField;
    QrPediVdaRedespacho: TIntegerField;
    QrPediVdaRegrFiscal: TIntegerField;
    QrPediVdaDesoAces_V: TFloatField;
    QrPediVdaDesoAces_P: TFloatField;
    QrPediVdaFrete_V: TFloatField;
    QrPediVdaFrete_P: TFloatField;
    QrPediVdaSeguro_V: TFloatField;
    QrPediVdaSeguro_P: TFloatField;
    QrPediVdaTotalQtd: TFloatField;
    QrPediVdaTotal_Vlr: TFloatField;
    QrPediVdaTotal_Des: TFloatField;
    QrPediVdaTotal_Tot: TFloatField;
    QrPediVdaObserva: TWideMemoField;
    QrPediVdaNOMETABEPRCCAD: TWideStringField;
    QrPediVdaNOMEMOEDA: TWideStringField;
    QrPediVdaNOMECONDICAOPG: TWideStringField;
    QrPediVdaNOMEMOTIVO: TWideStringField;
    QrPediVdaNOMESITUACAO: TWideStringField;
    GBDadosTop: TGroupBox;
    Label9: TLabel;
    Label2: TLabel;
    EdCliente: TdmkEditCB;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    CBCliente: TdmkDBLookupComboBox;
    DBEdDOCENT: TDBEdit;
    EdCodUsu: TdmkEdit;
    Label8: TLabel;
    DBEdNOMEUF: TDBEdit;
    SpeedButton5: TSpeedButton;
    Label23: TLabel;
    EdSituacao: TdmkEditCB;
    CBSituacao: TdmkDBLookupComboBox;
    Label26: TLabel;
    EdMotivoSit: TdmkEditCB;
    CBMotivoSit: TdmkDBLookupComboBox;
    EdCodigo: TdmkEdit;
    Label7: TLabel;
    TPDtaInclu: TdmkEditDateTimePicker;
    Label6: TLabel;
    SpeedButton7: TSpeedButton;
    QrPediVdaRepresen: TIntegerField;
    QrPediVdaComisFat: TFloatField;
    QrPediVdaComisRec: TFloatField;
    QrPediVdaNOMEACC: TWideStringField;
    QrPediVdaCartEmis: TIntegerField;
    QrPediVdaAFP_Sit: TSmallintField;
    QrPediVdaAFP_Per: TFloatField;
    QrPediVdaNOMECARTEMIS: TWideStringField;
    GroupBox9: TGroupBox;
    Label56: TLabel;
    Label57: TLabel;
    Label58: TLabel;
    Label59: TLabel;
    Label60: TLabel;
    Label61: TLabel;
    DBEdit1: TDBEdit;
    DBEdit3: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit4: TDBEdit;
    DBEdit5: TDBEdit;
    DBEdit30: TDBEdit;
    DBEdit31: TDBEdit;
    DBEdit20: TDBEdit;
    DBEdit29: TDBEdit;
    DBEdit8: TDBEdit;
    DBEdit22: TDBEdit;
    DBEdit27: TDBEdit;
    DBEdit32: TDBEdit;
    QrPediVdaNOMEFRETEPOR: TWideStringField;
    QrPediVdaNOMETRANSP: TWideStringField;
    QrPediVdaNOMEREDESP: TWideStringField;
    QrPediVdaCODUSU_CLI: TIntegerField;
    QrPediVdaCODUSU_ACC: TIntegerField;
    QrPediVdaCODUSU_TRA: TIntegerField;
    QrPediVdaCODUSU_RED: TIntegerField;
    QrPediVdaCODUSU_MOT: TIntegerField;
    QrPediVdaCODUSU_TPC: TIntegerField;
    QrPediVdaCODUSU_MDA: TIntegerField;
    QrPediVdaCODUSU_PPC: TIntegerField;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    Panel4: TPanel;
    GroupBox10: TGroupBox;
    Label54: TLabel;
    Label55: TLabel;
    Label63: TLabel;
    Label64: TLabel;
    Label65: TLabel;
    DBEdit9: TDBEdit;
    DBEdit10: TDBEdit;
    DBEdit12: TDBEdit;
    DBEdit11: TDBEdit;
    DBEdit19: TDBEdit;
    GroupBox11: TGroupBox;
    Label1: TLabel;
    Label3: TLabel;
    Label5: TLabel;
    DBEdit21: TDBEdit;
    DBEdit24: TDBEdit;
    DBEdit18: TDBEdit;
    DBEdit25: TDBEdit;
    DBEdit17: TDBEdit;
    DBEdit26: TDBEdit;
    DBEdit6: TDBEdit;
    DBEdit7: TDBEdit;
    GroupBox8: TGroupBox;
    Label11: TLabel;
    DBEdit33: TDBEdit;
    GroupBox14: TGroupBox;
    Label51: TLabel;
    Label52: TLabel;
    Label53: TLabel;
    Label66: TLabel;
    Label67: TLabel;
    Label68: TLabel;
    Label69: TLabel;
    Label70: TLabel;
    Label71: TLabel;
    Label72: TLabel;
    DBEdit16: TDBEdit;
    DBEdit23: TDBEdit;
    MeEnderecoEntrega2: TMemo;
    DBEdit34: TDBEdit;
    DBEdit35: TDBEdit;
    DBEdit36: TDBEdit;
    DBEdit37: TDBEdit;
    DBEdit38: TDBEdit;
    DBEdit39: TDBEdit;
    DBEdit40: TDBEdit;
    DBEdit41: TDBEdit;
    DBEdit42: TDBEdit;
    DBEdit43: TDBEdit;
    GroupBox12: TGroupBox;
    Label16: TLabel;
    Label17: TLabel;
    Label18: TLabel;
    DBEdit44: TDBEdit;
    DBEdit45: TDBEdit;
    DBEdit46: TDBEdit;
    DBEdit47: TDBEdit;
    PCDadosTop: TPageControl;
    TabSheet3: TTabSheet;
    Panel6: TPanel;
    GroupBox2: TGroupBox;
    Label12: TLabel;
    Label13: TLabel;
    Label15: TLabel;
    Label14: TLabel;
    Label27: TLabel;
    TPDtaEmiss: TdmkEditDateTimePicker;
    TPDtaEntra: TdmkEditDateTimePicker;
    EdPrioridade: TdmkEdit;
    TPDtaPrevi: TdmkEditDateTimePicker;
    EdPedidoCli: TdmkEdit;
    GroupBox3: TGroupBox;
    BtTabelaPrc: TSpeedButton;
    LaTabelaPrc: TLabel;
    SpeedButton9: TSpeedButton;
    Label22: TLabel;
    BtCondicaoPG: TSpeedButton;
    Label24: TLabel;
    LaCondicaoPG: TLabel;
    SpeedButton13: TSpeedButton;
    CBTabelaPrc: TdmkDBLookupComboBox;
    EdTabelaPrc: TdmkEditCB;
    CBCartEmis: TdmkDBLookupComboBox;
    CBMoeda: TdmkDBLookupComboBox;
    EdMoeda: TdmkEditCB;
    EdCartEmis: TdmkEditCB;
    CBCondicaoPG: TdmkDBLookupComboBox;
    EdCondicaoPG: TdmkEditCB;
    GroupBox5: TGroupBox;
    Label29: TLabel;
    Label30: TLabel;
    Label31: TLabel;
    SpeedButton6: TSpeedButton;
    SpeedButton11: TSpeedButton;
    Label20: TLabel;
    Label32: TLabel;
    Label41: TLabel;
    Label42: TLabel;
    Label43: TLabel;
    Label44: TLabel;
    Label45: TLabel;
    EdFretePor: TdmkEditCB;
    CBFretePor: TdmkDBLookupComboBox;
    EdTransporta: TdmkEditCB;
    CBTransporta: TdmkDBLookupComboBox;
    EdRedespacho: TdmkEditCB;
    CBRedespacho: TdmkDBLookupComboBox;
    MeEnderecoEntrega1: TMemo;
    EdDesoAces_V: TdmkEdit;
    EdFrete_V: TdmkEdit;
    EdSeguro_P: TdmkEdit;
    EdDesoAces_P: TdmkEdit;
    EdFrete_P: TdmkEdit;
    EdSeguro_V: TdmkEdit;
    GroupBox7: TGroupBox;
    Label46: TLabel;
    SpeedButton12: TSpeedButton;
    Label47: TLabel;
    Label48: TLabel;
    EdRepresen: TdmkEditCB;
    CBRepresen: TdmkDBLookupComboBox;
    EdComisFat: TdmkEdit;
    EdComisRec: TdmkEdit;
    GroupBox6: TGroupBox;
    Incluinovositensdegrupo1: TMenuItem;
    QrPediVdaGru: TMySQLQuery;
    DsPediVdaGru: TDataSource;
    QrPediVdaGruCodUsu: TIntegerField;
    QrPediVdaGruNome: TWideStringField;
    QrPediVdaGruNivel1: TIntegerField;
    QrPediVdaMedDDSimpl: TFloatField;
    QrPediVdaMedDDReal: TFloatField;
    DBGGru: TdmkDBGrid;
    PageControl3: TPageControl;
    TabSheet5: TTabSheet;
    GradeQ: TStringGrid;
    StaticText1: TStaticText;
    TabSheet4: TTabSheet;
    GradeF: TStringGrid;
    TabSheet6: TTabSheet;
    GradeD: TStringGrid;
    StaticText3: TStaticText;
    TabSheet7: TTabSheet;
    GradeV: TStringGrid;
    TabSheet8: TTabSheet;
    GradeC: TStringGrid;
    StaticText6: TStaticText;
    TabSheet9: TTabSheet;
    GradeA: TStringGrid;
    StaticText2: TStaticText;
    TabSheet10: TTabSheet;
    GradeX: TStringGrid;
    QrPediVdaGruGRATAMCAD: TIntegerField;
    QrPediVdaGruQuantP: TFloatField;
    QrPediVdaGruValLiq: TFloatField;
    GroupBox13: TGroupBox;
    QrPediVdaValLiq: TFloatField;
    QrPediVdaQuantP: TFloatField;
    Label19: TLabel;
    DBEdit13: TDBEdit;
    Label21: TLabel;
    DBEdit14: TDBEdit;
    AlteraExcluiIncluiitemselecionado1: TMenuItem;
    Label25: TLabel;
    EdRegrFiscal: TdmkEditCB;
    CBRegrFiscal: TdmkDBLookupComboBox;
    SbRegrFiscal: TSpeedButton;
    VuFisRegCad: TdmkValUsu;
    EdModeloNF: TdmkEdit;
    Label33: TLabel;
    GroupBox15: TGroupBox;
    Label34: TLabel;
    Label35: TLabel;
    QrPediVdaNOMEFISREGCAD: TWideStringField;
    QrPediVdaNOMEMODELONF: TWideStringField;
    QrPediVdaCODUSU_FRC: TIntegerField;
    QrPediVdaMODELO_NF: TIntegerField;
    DBEdit48: TDBEdit;
    DBEdit49: TDBEdit;
    DBEdit50: TDBEdit;
    QrPediVdaMaxDesco: TFloatField;
    QrPediVdaJurosMes: TFloatField;
    N1: TMenuItem;
    IncluinovositensporLeitura1: TMenuItem;
    N2: TMenuItem;
    QrPediVdaDescoMax: TFloatField;
    TabSheet11: TTabSheet;
    Panel7: TPanel;
    Panel8: TPanel;
    dmkDBGrid1: TdmkDBGrid;
    Panel9: TPanel;
    Panel10: TPanel;
    PMCustom: TPopupMenu;
    IncluinovoitemprodutoCustomizvel1: TMenuItem;
    Adicionapartesaoitemselecionado1: TMenuItem;
    QrCustomizados: TmySQLQuery;
    QrPediVdaGruItensCustomizados: TFloatField;
    DsCustomizados: TDataSource;
    QrCustomizadosGRATAMCAD: TIntegerField;
    QrCustomizadosGRATAMITS: TAutoIncField;
    QrCustomizadosNO_TAM: TWideStringField;
    QrCustomizadosNO_COR: TWideStringField;
    DBGrid2: TDBGrid;
    QrCustomizadosGraGruX: TIntegerField;
    QrCustomizadosControle: TIntegerField;
    QrPediVdaCuz: TmySQLQuery;
    DsPediVdaCuz: TDataSource;
    Splitter1: TSplitter;
    QrPediVdaCuzNO_TAM: TWideStringField;
    QrPediVdaCuzNO_COR: TWideStringField;
    QrPediVdaCuzControle: TIntegerField;
    QrPediVdaCuzConta: TIntegerField;
    QrPediVdaCuzMatPartCad: TIntegerField;
    QrPediVdaCuzGraGruX: TIntegerField;
    QrPediVdaCuzMedidaC: TFloatField;
    QrPediVdaCuzMedidaL: TFloatField;
    QrPediVdaCuzMedidaA: TFloatField;
    QrPediVdaCuzMedidaE: TFloatField;
    QrPediVdaCuzQuantP: TFloatField;
    QrPediVdaCuzQuantX: TFloatField;
    QrPediVdaCuzPrecoO: TFloatField;
    QrPediVdaCuzPrecoR: TFloatField;
    QrPediVdaCuzPrecoF: TFloatField;
    QrPediVdaCuzValBru: TFloatField;
    QrPediVdaCuzDescoP: TFloatField;
    QrPediVdaCuzDescoV: TFloatField;
    QrPediVdaCuzPerCustom: TFloatField;
    QrPediVdaCuzValLiq: TFloatField;
    QrPediVdaCuzTipDimens: TSmallintField;
    QrPediVdaCuzNO_PARTE: TWideStringField;
    QrPediVdaCuzNO_GRUPO: TWideStringField;
    N3: TMenuItem;
    AtualizaValoresdoitem1: TMenuItem;
    ItemprodutoCustomizvel1: TMenuItem;
    Partedoitemprodutoselecionado1: TMenuItem;
    Alteraitemprodutoselecionado1: TMenuItem;
    QrPediVdaCuzMedidaC_TXT: TWideStringField;
    QrPediVdaCuzMedidaL_TXT: TWideStringField;
    QrPediVdaCuzMedidaA_TXT: TWideStringField;
    QrPediVdaCuzMedidaE_TXT: TWideStringField;
    QrCustomizadosMedidaC: TFloatField;
    QrCustomizadosMedidaL: TFloatField;
    QrCustomizadosMedidaA: TFloatField;
    QrCustomizadosMedidaE: TFloatField;
    QrCustomizadosValLiq: TFloatField;
    Excluipartedoprodutoselecionado1: TMenuItem;
    Excluiitemprodutoselecionado1: TMenuItem;
    DBGrid1: TDBGrid;
    QrPediVdaCuzSiglaCustm: TWideStringField;
    frxDsCli: TfrxDBDataset;
    QrCli: TmySQLQuery;
    QrCliE_ALL: TWideStringField;
    QrCliCNPJ_TXT: TWideStringField;
    QrCliNOME_TIPO_DOC: TWideStringField;
    QrCliTE1_TXT: TWideStringField;
    QrCliFAX_TXT: TWideStringField;
    QrCliNUMERO_TXT: TWideStringField;
    QrCliCEP_TXT: TWideStringField;
    QrCliCodigo: TIntegerField;
    QrCliTipo: TSmallintField;
    QrCliCodUsu: TIntegerField;
    QrCliNOME_ENT: TWideStringField;
    QrCliCNPJ_CPF: TWideStringField;
    QrCliIE_RG: TWideStringField;
    QrCliRUA: TWideStringField;
    QrCliCOMPL: TWideStringField;
    QrCliBAIRRO: TWideStringField;
    QrCliCIDADE: TWideStringField;
    QrCliNOMELOGRAD: TWideStringField;
    QrCliNOMEUF: TWideStringField;
    QrCliPais: TWideStringField;
    QrCliENDEREF: TWideStringField;
    QrCliTE1: TWideStringField;
    QrCliFAX: TWideStringField;
    QrCliIE: TWideStringField;
    QrCliCAD_FEDERAL: TWideStringField;
    QrCliCAD_ESTADUAL: TWideStringField;
    QrCliIE_TXT: TWideStringField;
    frxPED_VENDA_001_01: TfrxReport;
    QrItsN: TmySQLQuery;
    QrItsNCU_NIVEL1: TIntegerField;
    QrItsNNO_NIVEL1: TWideStringField;
    QrItsNNO_TAM: TWideStringField;
    QrItsNNO_COR: TWideStringField;
    QrItsNQuantP: TFloatField;
    QrItsNPrecoR: TFloatField;
    QrItsNDescoP: TFloatField;
    QrItsNPrecoF: TFloatField;
    QrItsNValBru: TFloatField;
    QrItsNValLiq: TFloatField;
    QrItsC: TmySQLQuery;
    frxDsItsN: TfrxDBDataset;
    QrItsNKGT: TLargeintField;
    frxDsPediVda: TfrxDBDataset;
    QrItsCCU_NIVEL1: TIntegerField;
    QrItsCNO_NIVEL1: TWideStringField;
    QrItsCNO_TAM: TWideStringField;
    QrItsCNO_COR: TWideStringField;
    QrItsCQuantP: TFloatField;
    QrItsCPrecoR: TFloatField;
    QrItsCDescoP: TFloatField;
    QrItsCPrecoF: TFloatField;
    QrItsCValBru: TFloatField;
    QrItsCValLiq: TFloatField;
    QrItsCMedidaC: TFloatField;
    QrItsCMedidaL: TFloatField;
    QrItsCMedidaA: TFloatField;
    QrItsCMedidaE: TFloatField;
    QrItsCPercCustom: TFloatField;
    QrItsCInfAdCuztm: TIntegerField;
    QrItsCMedida1: TWideStringField;
    QrItsCMedida2: TWideStringField;
    QrItsCMedida3: TWideStringField;
    QrItsCMedida4: TWideStringField;
    QrItsCSigla1: TWideStringField;
    QrItsCSigla2: TWideStringField;
    QrItsCSigla3: TWideStringField;
    QrItsCSigla4: TWideStringField;
    frxDsItsC: TfrxDBDataset;
    QrItsCDESCRICAO: TWideStringField;
    QrItsZ: TmySQLQuery;
    frxDsItsZ: TfrxDBDataset;
    QrItsZCU_NIVEL1: TIntegerField;
    QrItsZNO_NIVEL1: TWideStringField;
    QrItsZNO_TAM: TWideStringField;
    QrItsZNO_COR: TWideStringField;
    QrItsZQuantP: TFloatField;
    QrItsZPrecoR: TFloatField;
    QrItsZDescoP: TFloatField;
    QrItsZPrecoF: TFloatField;
    QrItsZValBru: TFloatField;
    QrItsZValLiq: TFloatField;
    QrItsZMedidaC: TFloatField;
    QrItsZMedidaL: TFloatField;
    QrItsZMedidaA: TFloatField;
    QrItsZMedidaE: TFloatField;
    QrItsZPerCustom: TFloatField;
    QrItsZNO_MatPartCad: TWideStringField;
    QrItsZMedida1: TWideStringField;
    QrItsZMedida2: TWideStringField;
    QrItsZMedida3: TWideStringField;
    QrItsZMedida4: TWideStringField;
    QrItsZSigla1: TWideStringField;
    QrItsZSigla2: TWideStringField;
    QrItsZSigla3: TWideStringField;
    QrItsZSigla4: TWideStringField;
    QrItsZDESCRICAO: TWideStringField;
    QrItsCControle: TIntegerField;
    QrItsCKGT: TLargeintField;
    QrItsZKGT: TLargeintField;
    QrItsCTipDimens: TSmallintField;
    QrItsZTipDimens: TSmallintField;
    QrPediVdaGruFracio: TSmallintField;
    Label10: TLabel;
    Label4: TLabel;
    EdDbSAIENT_TXT: TDBText;
    QrPediVdaEntSai: TSmallintField;
    QrPediVdaENTSAI_TXT: TWideStringField;
    QrPediVdaSHOW_TXT_FILIAL: TWideStringField;
    QrPediVdaSHOW_TXT_CLIFOR: TWideStringField;
    QrPediVdaSHOW_COD_FILIAL: TIntegerField;
    QrPediVdaSHOW_COD_CLIFOR: TIntegerField;
    QrPediVdaCODUSU_FOR: TIntegerField;
    QrPediVdaFILIAL_CLI: TIntegerField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel11: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel12: TPanel;
    PnSaiDesis: TPanel;
    BtConfirma: TBitBtn;
    CkAFP_Sit: TdmkCheckBox;
    Label49: TLabel;
    EdAFP_Per: TdmkEdit;
    BtDesiste: TBitBtn;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    BtPedidos: TBitBtn;
    BtItens: TBitBtn;
    BtCustom: TBitBtn;
    BtVisual: TBitBtn;
    BitBtn1: TBitBtn;
    BtRecalcula: TBitBtn;
    N4: TMenuItem;
    Duplica1: TMenuItem;
    QrPediVdaGruControle: TIntegerField;
    QrPediVdaIts: TmySQLQuery;
    QrPediVdaItsControle: TIntegerField;
    RG_idDest: TdmkRadioGroup;
    QrPediVdaidDest: TSmallintField;
    RG_indFinal: TdmkRadioGroup;
    QrPediVdaindFinal: TSmallintField;
    QrPediVdaindPres: TSmallintField;
    Label36: TLabel;
    EdindPres: TdmkEdit;
    EdindPres_TXT: TEdit;
    DBRGIndDest: TDBRadioGroup;
    Label37: TLabel;
    RGDBIndFinal: TDBRadioGroup;
    DBEdindPres: TDBEdit;
    EdDBindPres_TXT: TEdit;
    RGIndSinc: TdmkRadioGroup;
    DBRadioGroup1: TDBRadioGroup;
    QrPediVdaindSinc: TSmallintField;
    BtGraGruN: TBitBtn;
    BtFisRegCad: TBitBtn;
    Edide_finNFe: TdmkEdit;
    Edide_finNFe_TXT: TdmkEdit;
    Label208: TLabel;
    DBEdfinNFe: TDBEdit;
    Label38: TLabel;
    QrPediVdafinNFe: TSmallintField;
    EdDBfinNFe_TXT: TEdit;
    QrCliNUMERO: TFloatField;
    QrCliCEP: TFloatField;
    QrCliUF: TFloatField;
    SpeedButton8: TSpeedButton;
    TabSheet12: TTabSheet;
    TabSheet13: TTabSheet;
    Panel1: TPanel;
    CkEntregaUsa: TdmkCheckBox;
    PnEntregaEntiAll: TPanel;
    PnEntregaEntiSel: TPanel;
    Label39: TLabel;
    SbEntregaEnti: TSpeedButton;
    EdEntregaEnti: TdmkEditCB;
    CBEntregaEnti: TdmkDBLookupComboBox;
    PnEntregaEntiViw: TPanel;
    Label40: TLabel;
    Label50: TLabel;
    Label73: TLabel;
    Label74: TLabel;
    Label75: TLabel;
    Label76: TLabel;
    Label77: TLabel;
    Label78: TLabel;
    Label79: TLabel;
    Label80: TLabel;
    Label81: TLabel;
    Label82: TLabel;
    Label83: TLabel;
    DBEdit28: TDBEdit;
    DBEdit51: TDBEdit;
    DBEdit52: TDBEdit;
    DBEdit53: TDBEdit;
    DBEdit54: TDBEdit;
    DBEdit55: TDBEdit;
    DBEdit56: TDBEdit;
    DBEdit57: TDBEdit;
    DBEdit58: TDBEdit;
    DBEdit59: TDBEdit;
    DBEdit60: TDBEdit;
    DBEdit61: TDBEdit;
    DBEdit62: TDBEdit;
    DBEdit63: TDBEdit;
    DBEdit64: TDBEdit;
    DBEdit65: TDBEdit;
    Panel13: TPanel;
    CkRetiradaUsa: TdmkCheckBox;
    PnRetiradaEntiAll: TPanel;
    PnRetiradaEntiSel: TPanel;
    Label84: TLabel;
    SbRetiradaEnti: TSpeedButton;
    EdRetiradaEnti: TdmkEditCB;
    CBRetiradaEnti: TdmkDBLookupComboBox;
    PnRetiradaEntiViw: TPanel;
    Label85: TLabel;
    Label86: TLabel;
    Label87: TLabel;
    Label88: TLabel;
    Label89: TLabel;
    Label90: TLabel;
    Label91: TLabel;
    Label92: TLabel;
    Label93: TLabel;
    Label94: TLabel;
    Label95: TLabel;
    Label96: TLabel;
    Label97: TLabel;
    DBEdit66: TDBEdit;
    DBEdit67: TDBEdit;
    DBEdit68: TDBEdit;
    DBEdit69: TDBEdit;
    DBEdit70: TDBEdit;
    DBEdit71: TDBEdit;
    DBEdit72: TDBEdit;
    DBEdit73: TDBEdit;
    DBEdit74: TDBEdit;
    DBEdit75: TDBEdit;
    DBEdit76: TDBEdit;
    DBEdit77: TDBEdit;
    DBEdit78: TDBEdit;
    DBEdit79: TDBEdit;
    DBEdit80: TDBEdit;
    DBEdit81: TDBEdit;
    QrPediVdaRetiradaUsa: TSmallintField;
    QrPediVdaRetiradaEnti: TIntegerField;
    QrPediVdaEntregaUsa: TSmallintField;
    QrPediVdaEntregaEnti: TIntegerField;
    QrPediVdaNOMEENTRETIRADA: TWideStringField;
    QrPediVdaNOMEENTENTREGA: TWideStringField;
    QrEntregaCli: TmySQLQuery;
    QrEntregaCliE_ALL: TWideStringField;
    QrEntregaCliCNPJ_TXT: TWideStringField;
    QrEntregaCliNOME_TIPO_DOC: TWideStringField;
    QrEntregaCliTE1_TXT: TWideStringField;
    QrEntregaCliNUMERO_TXT: TWideStringField;
    QrEntregaCliCEP_TXT: TWideStringField;
    QrEntregaCliL_CNPJ: TWideStringField;
    QrEntregaCliL_CPF: TWideStringField;
    QrEntregaCliL_Nome: TWideStringField;
    QrEntregaCliLograd: TSmallintField;
    QrEntregaCliNOMELOGRAD: TWideStringField;
    QrEntregaCliRUA: TWideStringField;
    QrEntregaCliNUMERO: TIntegerField;
    QrEntregaCliCOMPL: TWideStringField;
    QrEntregaCliBAIRRO: TWideStringField;
    QrEntregaCliLCodMunici: TIntegerField;
    QrEntregaCliNO_MUNICI: TWideStringField;
    QrEntregaCliNOMEUF: TWideStringField;
    QrEntregaCliCEP: TIntegerField;
    QrEntregaCliCodiPais: TIntegerField;
    QrEntregaCliNO_PAIS: TWideStringField;
    QrEntregaCliENDEREF: TWideStringField;
    QrEntregaCliTE1: TWideStringField;
    QrEntregaCliEmail: TWideStringField;
    QrEntregaCliL_IE: TWideStringField;
    QrEntregaEnti: TmySQLQuery;
    QrEntregaEntiCodigo: TIntegerField;
    QrEntregaEntiCadastro: TDateField;
    QrEntregaEntiENatal: TDateField;
    QrEntregaEntiPNatal: TDateField;
    QrEntregaEntiTipo: TSmallintField;
    QrEntregaEntiRespons1: TWideStringField;
    QrEntregaEntiRespons2: TWideStringField;
    QrEntregaEntiENumero: TIntegerField;
    QrEntregaEntiPNumero: TIntegerField;
    QrEntregaEntiELograd: TSmallintField;
    QrEntregaEntiPLograd: TSmallintField;
    QrEntregaEntiECEP: TIntegerField;
    QrEntregaEntiPCEP: TIntegerField;
    QrEntregaEntiNOME_ENT: TWideStringField;
    QrEntregaEntiNO_2_ENT: TWideStringField;
    QrEntregaEntiCNPJ_CPF: TWideStringField;
    QrEntregaEntiIE_RG: TWideStringField;
    QrEntregaEntiNIRE_: TWideStringField;
    QrEntregaEntiRUA: TWideStringField;
    QrEntregaEntiSITE: TWideStringField;
    QrEntregaEntiNUMERO: TIntegerField;
    QrEntregaEntiCOMPL: TWideStringField;
    QrEntregaEntiBAIRRO: TWideStringField;
    QrEntregaEntiCIDADE: TWideStringField;
    QrEntregaEntiNOMELOGRAD: TWideStringField;
    QrEntregaEntiNOMEUF: TWideStringField;
    QrEntregaEntiPais: TWideStringField;
    QrEntregaEntiLograd: TSmallintField;
    QrEntregaEntiCEP: TIntegerField;
    QrEntregaEntiTE1: TWideStringField;
    QrEntregaEntiFAX: TWideStringField;
    QrEntregaEntiEMAIL: TWideStringField;
    QrEntregaEntiTRATO: TWideStringField;
    QrEntregaEntiENDEREF: TWideStringField;
    QrEntregaEntiCODMUNICI: TFloatField;
    QrEntregaEntiCODPAIS: TFloatField;
    QrEntregaEntiRG: TWideStringField;
    QrEntregaEntiSSP: TWideStringField;
    QrEntregaEntiDataRG: TDateField;
    QrEntregaEntiIE: TWideStringField;
    QrEntregaEntiE_ALL: TWideStringField;
    QrEntregaEntiCNPJ_TXT: TWideStringField;
    QrEntregaEntiNOME_TIPO_DOC: TWideStringField;
    QrEntregaEntiTE1_TXT: TWideStringField;
    QrEntregaEntiFAX_TXT: TWideStringField;
    QrEntregaEntiNUMERO_TXT: TWideStringField;
    QrEntregaEntiCEP_TXT: TWideStringField;
    QrPediVdaL_Ativo: TSmallintField;
    PMRegrFiscal: TPopupMenu;
    Estrio1: TMenuItem;
    Cadastro1: TMenuItem;
    QrPediVdaTipoMov: TSmallintField;
    QrUsuario: TMySQLQuery;
    QrUsuarioUserCad: TIntegerField;
    QrUsuarioUserAlt: TIntegerField;
    QrUsuarioNO_LANCADOR: TWideStringField;
    QrUsuarioNO_ALTERADOR: TWideStringField;
    frxDsUsuario: TfrxDBDataset;
    TabSheet14: TTabSheet;
    MeObserva: TdmkMemo;
    TabSheet15: TTabSheet;
    DBMemo1: TDBMemo;
    EdReferenPedi: TdmkEdit;
    Label98: TLabel;
    QrPediVdaReferenPedi: TWideStringField;
    Label99: TLabel;
    Label100: TLabel;
    DBEdit82: TDBEdit;
    Panel14: TPanel;
    RGEntSai: TdmkRadioGroup;
    Label62: TLabel;
    EdDesco_P: TdmkEdit;
    Label101: TLabel;
    EdDesco_V: TdmkEdit;
    QrPediVdaDesco_P: TFloatField;
    QrPediVdaDesco_V: TFloatField;
    QrPediVdaGruValBru: TFloatField;
    DistribuirFrete1: TMenuItem;
    Distribuirdesconto1: TMenuItem;
    QrSumIts: TMySQLQuery;
    Label102: TLabel;
    DBEdit83: TDBEdit;
    DsSumIts: TDataSource;
    QrSumItsQuantP: TFloatField;
    QrSumItsDescoV: TFloatField;
    QrSumItsValLiq: TFloatField;
    QrSumItsValBru: TFloatField;
    QrSumItsItensCustomizados: TFloatField;
    QrSumItsvProd: TFloatField;
    QrSumItsvFrete: TFloatField;
    QrSumItsvSeg: TFloatField;
    QrSumItsvOutro: TFloatField;
    QrSumItsvDesc: TFloatField;
    QrSumItsvBC: TFloatField;
    DBEdit84: TDBEdit;
    Label103: TLabel;
    DBEdit85: TDBEdit;
    Label104: TLabel;
    DBEdit86: TDBEdit;
    Label105: TLabel;
    Label106: TLabel;
    DBEdit87: TDBEdit;
    Label107: TLabel;
    DBEdit88: TDBEdit;
    QrPediVdaGruPrecoF: TFloatField;
    Splitter2: TSplitter;
    DistribuirSeguro1: TMenuItem;
    DistribuirDespesasacessrias1: TMenuItem;
    N5: TMenuItem;
    frxPED_VENDA_001_02: TfrxReport;
    PMImprime: TPopupMenu;
    Modelo11: TMenuItem;
    Modelosemgrade1: TMenuItem;
    QrItsNvProd: TFloatField;
    QrItsNvFrete: TFloatField;
    QrItsNvSeg: TFloatField;
    QrItsNvOutro: TFloatField;
    QrItsNvDesc: TFloatField;
    QrItsNvBC: TFloatField;
    Label108: TLabel;
    EdIND_PGTO: TdmkEdit;
    EdIND_PGTO_TXT: TdmkEdit;
    QrPediVdaIND_PGTO: TWideStringField;
    Label109: TLabel;
    DBEdIND_PGTO: TDBEdit;
    EdDBIND_PGTO_TEXT: TEdit;
    Incluipedidodeumasolicitao1: TMenuItem;
    Label110: TLabel;
    EdSoliComprCab: TdmkEdit;
    EdSoliCompr_TXT: TdmkEdit;
    QrPediVdaSoliComprCab: TIntegerField;
    QrPediVdaCentroCusto: TIntegerField;
    QrPediVdaNO_CENTROCUSTO: TWideStringField;
    Label111: TLabel;
    DBEdit89: TDBEdit;
    DBEdit90: TDBEdit;
    QrSoliComprIts: TMySQLQuery;
    QrSoliComprItsCodigo: TIntegerField;
    QrSoliComprItsControle: TIntegerField;
    QrSoliComprItsGraGruX: TIntegerField;
    QrSoliComprItsPrecoO: TFloatField;
    QrSoliComprItsPrecoR: TFloatField;
    QrSoliComprItsQuantP: TFloatField;
    QrSoliComprItsQuantC: TFloatField;
    QrSoliComprItsQuantV: TFloatField;
    QrSoliComprItsValBru: TFloatField;
    QrSoliComprItsDescoP: TFloatField;
    QrSoliComprItsDescoV: TFloatField;
    QrSoliComprItsValLiq: TFloatField;
    QrSoliComprItsPrecoF: TFloatField;
    QrSoliComprItsMedidaC: TFloatField;
    QrSoliComprItsMedidaL: TFloatField;
    QrSoliComprItsMedidaA: TFloatField;
    QrSoliComprItsMedidaE: TFloatField;
    QrSoliComprItsPercCustom: TFloatField;
    QrSoliComprItsCustomizad: TSmallintField;
    QrSoliComprItsInfAdCuztm: TIntegerField;
    QrSoliComprItsOrdem: TIntegerField;
    QrSoliComprItsReferencia: TWideStringField;
    QrSoliComprItsMulti: TIntegerField;
    QrSoliComprItsvProd: TFloatField;
    QrSoliComprItsvFrete: TFloatField;
    QrSoliComprItsvSeg: TFloatField;
    QrSoliComprItsvOutro: TFloatField;
    QrSoliComprItsvDesc: TFloatField;
    QrSoliComprItsvBC: TFloatField;
    QrSoliComprItsQuantS: TFloatField;
    QrPVIts: TMySQLQuery;
    QrPVItsControle: TIntegerField;
    Panel15: TPanel;
    RGMadeBy: TdmkRadioGroup;
    GroupBox4: TGroupBox;
    Label28: TLabel;
    DBEdit15: TDBEdit;
    QrPediVdaMadeBy: TSmallintField;
    QrCentroCusto: TMySQLQuery;
    QrCentroCustoREF_NIV1: TWideStringField;
    QrCentroCustoCodigo: TIntegerField;
    QrCentroCustoNome: TWideStringField;
    QrCentroCustoAtivo: TSmallintField;
    DsCentroCusto: TDataSource;
    Label112: TLabel;
    DBEdREF_NIV1: TDBEdit;
    EdCentroCusto: TdmkEditCB;
    CBCentroCusto: TdmkDBLookupComboBox;
    SbCentroCusto: TSpeedButton;
    QrPVG: TMySQLQuery;
    QrPVGCodUsu: TIntegerField;
    QrPVGNome: TWideStringField;
    QrPVGNivel1: TIntegerField;
    QrPVGGRATAMCAD: TIntegerField;
    QrPVGQuantP: TFloatField;
    QrPVGValLiq: TFloatField;
    QrPVGItensCustomizados: TFloatField;
    QrPVGFracio: TSmallintField;
    QrPVGControle: TIntegerField;
    QrPVGValBru: TFloatField;
    QrPVGPrecoF: TFloatField;
    QrEmissF: TMySQLQuery;
    QrEmissFData: TDateField;
    QrEmissFTipo: TSmallintField;
    QrEmissFCarteira: TIntegerField;
    QrEmissFAutorizacao: TIntegerField;
    QrEmissFGenero: TIntegerField;
    QrEmissFDescricao: TWideStringField;
    QrEmissFNotaFiscal: TIntegerField;
    QrEmissFDebito: TFloatField;
    QrEmissFCredito: TFloatField;
    QrEmissFCompensado: TDateField;
    QrEmissFDocumento: TFloatField;
    QrEmissFSit: TIntegerField;
    QrEmissFVencimento: TDateField;
    QrEmissFLk: TIntegerField;
    QrEmissFFatID: TIntegerField;
    QrEmissFFatParcela: TIntegerField;
    QrEmissFNOMESIT: TWideStringField;
    QrEmissFNOMETIPO: TWideStringField;
    QrEmissFNOMECARTEIRA: TWideStringField;
    QrEmissFID_Sub: TSmallintField;
    QrEmissFSub: TIntegerField;
    QrEmissFFatura: TWideStringField;
    QrEmissFBanco: TIntegerField;
    QrEmissFLocal: TIntegerField;
    QrEmissFCartao: TIntegerField;
    QrEmissFLinha: TIntegerField;
    QrEmissFPago: TFloatField;
    QrEmissFMez: TIntegerField;
    QrEmissFFornecedor: TIntegerField;
    QrEmissFCliente: TIntegerField;
    QrEmissFControle: TIntegerField;
    QrEmissFID_Pgto: TIntegerField;
    QrEmissFCliInt: TIntegerField;
    QrEmissFQtde: TFloatField;
    QrEmissFFatID_Sub: TIntegerField;
    QrEmissFOperCount: TIntegerField;
    QrEmissFLancto: TIntegerField;
    QrEmissFForneceI: TIntegerField;
    QrEmissFMoraDia: TFloatField;
    QrEmissFMulta: TFloatField;
    QrEmissFProtesto: TDateField;
    QrEmissFDataDoc: TDateField;
    QrEmissFCtrlIni: TIntegerField;
    QrEmissFNivel: TIntegerField;
    QrEmissFVendedor: TIntegerField;
    QrEmissFAccount: TIntegerField;
    QrEmissFICMS_P: TFloatField;
    QrEmissFICMS_V: TFloatField;
    QrEmissFDuplicata: TWideStringField;
    QrEmissFDepto: TIntegerField;
    QrEmissFDescoPor: TIntegerField;
    QrEmissFDataCad: TDateField;
    QrEmissFDataAlt: TDateField;
    QrEmissFUserCad: TIntegerField;
    QrEmissFUserAlt: TIntegerField;
    QrEmissFEmitente: TWideStringField;
    QrEmissFContaCorrente: TWideStringField;
    QrEmissFCNPJCPF: TWideStringField;
    QrEmissFFatNum: TFloatField;
    QrEmissFAgencia: TIntegerField;
    QrEmissFSerieNF: TWideStringField;
    QrEmissFGenCtb: TIntegerField;
    QrEmissFGenCtbD: TIntegerField;
    QrEmissFGenCtbC: TIntegerField;
    QrEmissFCentroCusto: TIntegerField;
    DsEmissF: TDataSource;
    QrSumF: TMySQLQuery;
    QrSumFDebito: TFloatField;
    Pagamentovistaantecipado1: TMenuItem;
    TabSheet17: TTabSheet;
    GridF: TDBGrid;
    DBEdCidade: TDBEdit;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrPediVdaAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrPediVdaBeforeOpen(DataSet: TDataSet);
    procedure EdCodUsuKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SbNovoClick(Sender: TObject);
    procedure BtPedidosClick(Sender: TObject);
    procedure PMPedidosPopup(Sender: TObject);
    procedure QrPediVdaBeforeClose(DataSet: TDataSet);
    procedure QrPediVdaAfterScroll(DataSet: TDataSet);
    procedure BtItensClick(Sender: TObject);
    procedure Incluinovopedido1Click(Sender: TObject);
    procedure Alterapedidoatual1Click(Sender: TObject);
    procedure SpeedButton5Click(Sender: TObject);
    procedure SpeedButton6Click(Sender: TObject);
    procedure SpeedButton7Click(Sender: TObject);
    procedure BtTabelaPrcClick(Sender: TObject);
    procedure SpeedButton9Click(Sender: TObject);
    procedure BtCondicaoPGClick(Sender: TObject);
    procedure EdTabelaPrcChange(Sender: TObject);
    procedure SpeedButton11Click(Sender: TObject);
    procedure EdClienteChange(Sender: TObject);
    procedure SpeedButton12Click(Sender: TObject);
    procedure SpeedButton13Click(Sender: TObject);
    procedure EdDesoAces_PChange(Sender: TObject);
    procedure EdFrete_PChange(Sender: TObject);
    procedure EdSeguro_PChange(Sender: TObject);
    procedure QrPediVdaCalcFields(DataSet: TDataSet);
    procedure Incluinovositensdegrupo1Click(Sender: TObject);
    procedure QrPediVdaGruAfterScroll(DataSet: TDataSet);
    procedure BtVisualClick(Sender: TObject);
    procedure PageControl1Change(Sender: TObject);
    procedure GradeADrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect;
      State: TGridDrawState);
    procedure GradeCDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect;
      State: TGridDrawState);
    procedure GradeQDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect;
      State: TGridDrawState);
    procedure GradeFDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect;
      State: TGridDrawState);
    procedure GradeDDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect;
      State: TGridDrawState);
    procedure GradeVDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect;
      State: TGridDrawState);
    procedure GradeXDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect;
      State: TGridDrawState);
    procedure BitBtn1Click(Sender: TObject);
    procedure QrPediVdaGruAfterOpen(DataSet: TDataSet);
    procedure GradeQDblClick(Sender: TObject);
    procedure BtRecalculaClick(Sender: TObject);
    procedure AlteraExcluiIncluiitemselecionado1Click(Sender: TObject);
    procedure SbRegrFiscalClick(Sender: TObject);
    procedure EdRegrFiscalChange(Sender: TObject);
    procedure EdRegrFiscalExit(Sender: TObject);
    procedure IncluinovositensporLeitura1Click(Sender: TObject);
    procedure CBMotivoSitExit(Sender: TObject);
    procedure Adicionapartesaoitemselecionado1Click(Sender: TObject);
    procedure Custumizao1Click(Sender: TObject);
    procedure BtCustomClick(Sender: TObject);
    procedure IncluinovoitemprodutoCustomizvel1Click(Sender: TObject);
    procedure PMCustomPopup(Sender: TObject);
    procedure QrPediVdaGruBeforeClose(DataSet: TDataSet);
    procedure QrCustomizadosAfterScroll(DataSet: TDataSet);
    procedure QrCustomizadosBeforeClose(DataSet: TDataSet);
    procedure AtualizaValoresdoitem1Click(Sender: TObject);
    procedure Alteraitemprodutoselecionado1Click(Sender: TObject);
    procedure QrPediVdaCuzCalcFields(DataSet: TDataSet);
    procedure Excluipartedoprodutoselecionado1Click(Sender: TObject);
    procedure Excluiitemprodutoselecionado1Click(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
    procedure QrCliCalcFields(DataSet: TDataSet);
    procedure QrItsCCalcFields(DataSet: TDataSet);
    procedure QrItsZCalcFields(DataSet: TDataSet);
    procedure QrItsCAfterScroll(DataSet: TDataSet);
    procedure frxPED_VENDA_001_01GetValue(const VarName: string;
      var Value: Variant);
    procedure EdCondicaoPGChange(Sender: TObject);
    procedure EdRepresenChange(Sender: TObject);
    procedure Duplica1Click(Sender: TObject);
    procedure EdindPresChange(Sender: TObject);
    procedure EdindPresKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdindPresChange(Sender: TObject);
    procedure EdEmpresaChange(Sender: TObject);
    procedure BtGraGruNClick(Sender: TObject);
    procedure BtFisRegCadClick(Sender: TObject);
    procedure Edide_finNFeChange(Sender: TObject);
    procedure Edide_finNFeKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdfinNFeChange(Sender: TObject);
    procedure SpeedButton8Click(Sender: TObject);
    procedure EdEntregaEntiRedefinido(Sender: TObject);
    procedure CkEntregaUsaClick(Sender: TObject);
    procedure SbEntregaEntiClick(Sender: TObject);
    procedure CkRetiradaUsaClick(Sender: TObject);
    procedure EdRetiradaEntiRedefinido(Sender: TObject);
    procedure QrEntregaEntiCalcFields(DataSet: TDataSet);
    procedure QrEntregaCliCalcFields(DataSet: TDataSet);
    procedure Estrio1Click(Sender: TObject);
    procedure Cadastro1Click(Sender: TObject);
    procedure EdRegrFiscalRedefinido(Sender: TObject);
    procedure EdCodUsuRedefinido(Sender: TObject);
    procedure EdCodUsuEnter(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure RGEntSaiClick(Sender: TObject);
    procedure AplicardescontoGeral1Click(Sender: TObject);
    procedure DistribuirFrete1Click(Sender: TObject);
    procedure Distribuirdesconto1Click(Sender: TObject);
    procedure DistribuirSeguro1Click(Sender: TObject);
    procedure DistribuirDespesasacessrias1Click(Sender: TObject);
    procedure Modelosemgrade1Click(Sender: TObject);
    procedure Modelo11Click(Sender: TObject);
    procedure EdIND_PGTOChange(Sender: TObject);
    procedure EdIND_PGTOKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdIND_PGTOChange(Sender: TObject);
    procedure Incluipedidodeumasolicitao1Click(Sender: TObject);
    procedure EdCentroCustoChange(Sender: TObject);
    procedure Pagamentovistaantecipado1Click(Sender: TObject);
    procedure EdReferenPediKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdCartEmisRedefinido(Sender: TObject);
    procedure EdCondicaoPGRedefinido(Sender: TObject);
  private
    { Private declarations }
    FindPres, F_finNFe, F_IND_PGTO_EFD, F_IND_FRT_EFD: MyArrayLista;
    FDBGWidth: Integer;
    FTabLctA: String;
    //FCodUsuTxt_Old: String;
    //
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
   ////Procedures do form
    procedure MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo: Integer);
    procedure MostraEnderecoDeEntrega2();
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    //
    procedure CalculaDesoAces_V();
    procedure CalculaFrete_V();
    procedure CalculaSeguro_V();
    procedure DefineEnderecoDeEntrega();
    procedure DesabilitaComponentes();

    procedure MostraPediVdaGru(SQLType: TSQLType);
    procedure MostraPediVdaLei(SQLType: TSQLType);
    procedure GradeQDblClick2();
    procedure MostraFormEntidade2(Entidade: Integer; Query: TmySQLQuery;
              EditCliente: TdmkEditCB; ComboCliente: TdmkDBLookupComboBox);
    //function ObtemQtde(var Qtde: Double): Boolean;
    procedure DistribuiValorIncremento(CampoIncrementoItemNFe:
              TCampoIncrementoItemNFe);
    procedure Imprime(Gradeado: Boolean);
    procedure AlteraOuExcluiItem();
    procedure ReopenCentroCusto();
    procedure IncluiLctoF(Sender: TObject);
    procedure DefineVarDup();
    function  CalculaDiferencas(): Double;
    function  VerificaCarteira_x_CondPg(Avisa: Boolean): Boolean;
    procedure DefineIndicadorDoTipoDePagamento();

  public
    { Public declarations }
    procedure LocCod(Atual, Codigo: Integer);
    procedure ReopenPediVdaGru(Nivel1: Integer);
    procedure ReopenPediVdaCuz(Conta: Integer);
    procedure ReopenEmissF(FatParcela: Integer);
    procedure MostraFmPediVdaCuzParIns(CodUsu, PrdGrupTip, Nivel1, GraGruX, Controle:
              Integer; SQLType: TSQLType;
              QuantP, MedidaC, MedidaL, MedidaA, MedidaE: Double);
    procedure AtualizaItemCustomizado(Controle: Integer);
    procedure ReopenSumIts();
    procedure IncluiNovoPedido(Solicitacao: Integer; SoliTXT: String;
              CentroCusto: Integer);
    procedure InsereItensDeSoliComprCab(SoliComprCab, PediVda: Integer);
    procedure ObtemContasContabeis(var GenCtbD, GenCtbC: Integer);


  end;

var
  FmPediVda2: TFmPediVda2;
const
  FFormatFloat = '00000';

implementation

uses
  {$IfNDef SemNFe_0000} NFe_PF, {$EndIf}
  {$IfNDef NAO_GFAT}UnGrade_Jan, UnGFat_Jan, {$EndIf}
  {$IfNDef NO_FINANCEIRO}UnFinanceiroJan, {$EndIf}
  UnMyObjects, Module, GraAtrIts, MyDBCheck, ModuleGeral, ModPediVda,
  // ini 2022-04-03
  PediVdaAlt2_PediCompra, //PediVdaAlt2,
  // fim 2022-04-03
  Motivos, CambioMda, PediPrzCab1, PediAcc, PediVdaGru2, ModProd, PediVdaImp,
  GetValor, Principal, PediVdaLei2, PediVdaCuzIns2, PediVdaCuzUpd2, UnPagtos,
  PediVdaCuzParIns2, UnPraz_PF, UnEntities, SoliComprSel, UnFinanceiro;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmPediVda2.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmPediVda2.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrPediVdaCodigo.Value, LaRegistro.Caption[2]);
end;

function TFmPediVda2.VerificaCarteira_x_CondPg(Avisa: Boolean): Boolean;
var
  Combina: Boolean;
begin
  Combina := False;
  //
  if (EdCondicaoPg.ValueVariant = 0) or (EdCartEmis.ValueVariant = 0) then
    Exit;
(* CondPg
0-Indefinido
1-Adiantado
2-� vista
3-� Prazo
4-Outros
*)
  case DmPediVda.QrPediPrzCabCondPg.Value of
    0: Combina := True;
    1,
    2: Combina := DmPediVda.QrCartEmisTipo.Value in ([0, 1]);
    3: Combina := DmPediVda.QrCartEmisTipo.Value = 2;
    4: Combina := True;
  end;
  MyObjects.FIC(Combina = False, EdCondicaoPG,
    'A carteira n�o combina com a condi��o de pagamento selecionada!');
  Result := Combina;
end;

/////////////////////////////////////////////////////////////////////////////////////

procedure TFmPediVda2.DefParams;
begin
  VAR_GOTOTABELA := 'PediVda';
  VAR_GOTOMYSQLTABLE := QrPediVda;
  VAR_GOTONEG := gotoAll;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 1;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT pvd.Codigo, pvd.finNFe, pvd.CodUsu, pvd.Empresa, pvd.Cliente,');
  VAR_SQLx.Add('pvd.DtaEmiss, pvd.DtaEntra, pvd.DtaInclu, pvd.DtaPrevi,');
  VAR_SQLx.Add('pvd.Prioridade, pvd.CondicaoPG, pvd.Moeda,');
  VAR_SQLx.Add('pvd.Situacao, pvd.TabelaPrc, pvd.MotivoSit, pvd.LoteProd,');
  VAR_SQLx.Add('pvd.PedidoCli, pvd.FretePor, pvd.Transporta, pvd.Redespacho,');
  VAR_SQLx.Add('pvd.RegrFiscal, pvd.DesoAces_V, pvd.DesoAces_P,');
  VAR_SQLx.Add('pvd.Frete_V, pvd.Frete_P, pvd.Seguro_V, pvd.Seguro_P,');
  VAR_SQLx.Add('pvd.TotalQtd, pvd.Total_Vlr, pvd.Total_Des, pvd.Total_Tot,');
  VAR_SQLx.Add('pvd.Observa, pvd.EntSai, ');
  // NFe 3.10
  VAR_SQLx.Add('pvd.idDest, pvd.indFinal, pvd.indPres, pvd.indSinc, ');
  // NFe 4.00 NT 2018/05
  VAR_SQLx.Add('pvd.RetiradaUsa, pvd.RetiradaEnti,');
  VAR_SQLx.Add('pvd.EntregaUsa, pvd.EntregaEnti,');
  //
  //
  VAR_SQLx.Add('tpc.Nome NOMETABEPRCCAD, tpc.DescoMax,');
  VAR_SQLx.Add('mda.Nome NOMEMOEDA, pvd.Represen, pvd.ComisFat,');
  VAR_SQLx.Add('pvd.ComisRec, pvd.CartEmis, pvd.AFP_Sit, pvd.AFP_Per,');
  VAR_SQLx.Add('ppc.MedDDSimpl, ppc.MedDDReal, ppc.MaxDesco, ppc.JurosMes,');
  VAR_SQLx.Add('pvd.ValLiq, pvd.QuantP, frc.Nome NOMEFISREGCAD,');
  // ini 2022-04-22
  VAR_SQLx.Add('pvd.SoliComprCab, pvd.CentroCusto, ccr.Nome NO_CENTROCUSTO, ');
  VAR_SQLx.Add('pvd.MadeBy, ');
  // fim 2022-04-22
  // ini 2022-04-02
  VAR_SQLx.Add('pvd.ReferenPedi, pvd.Desco_P, pvd.Desco_V, pvd.IND_PGTO, ');
  // fim 2022-04-02
  // ini 2022-03-16
  VAR_SQLx.Add('frc.TipoMov,');
  // fim 2022-03-16
  VAR_SQLx.Add('imp.Nome NOMEMODELONF,');

  VAR_SQLx.Add('IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NOMEEMP,');
  VAR_SQLx.Add('IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NOMECLI,');
  VAR_SQLx.Add('mun.Nome CIDADECLI,');
  VAR_SQLx.Add('IF(ven.Tipo=0, ven.RazaoSocial, ven.NOME) NOMEACC,');
  VAR_SQLx.Add('IF(tra.Tipo=0, tra.RazaoSocial, tra.NOME) NOMETRANSP,');
  VAR_SQLx.Add('IF(red.Tipo=0, red.RazaoSocial, red.NOME) NOMEREDESP,');
  // NFe 4.00 NT 2018/05
  VAR_SQLx.Add('IF(ret.Tipo=0, ret.RazaoSocial, ret.NOME) NOMEENTRETIRADA, ');
  VAR_SQLx.Add('IF(ent.Tipo=0, ent.RazaoSocial, ent.NOME) NOMEENTENTREGA, ');
  //
  VAR_SQLx.Add('uf1.Nome NOMEUF, emp.Filial, car.Nome NOMECARTEMIS,');
  VAR_SQLx.Add('ppc.Nome NOMECONDICAOPG, mot.Nome NOMEMOTIVO,');
  VAR_SQLx.Add('cli.CodUsu CODUSU_CLI, ven.CodUsu CODUSU_ACC,');
  VAR_SQLx.Add('tra.CodUsu CODUSU_TRA, red.CodUsu CODUSU_RED,');
  VAR_SQLx.Add('mot.CodUsu CODUSU_MOT, tpc.CodUsu CODUSU_TPC,');
  VAR_SQLx.Add('mda.CodUsu CODUSU_MDA, ppc.CodUsu CODUSU_PPC,');
  VAR_SQLx.Add('frc.CodUsu CODUSU_FRC, imp.Codigo MODELO_NF,');
  VAR_SQLx.Add('emp.CodUsu CODUSU_FOR, cli.Filial FILIAL_CLI,');
  VAR_SQLx.Add('cli.L_Ativo ');

  VAR_SQLx.Add('FROM pedivda pvd');
  VAR_SQLx.Add('LEFT JOIN entidades  emp ON emp.Codigo=pvd.Empresa');
  VAR_SQLx.Add('LEFT JOIN entidades  cli ON cli.Codigo=pvd.Cliente');
  VAR_SQLx.Add('LEFT JOIN entidades  tra ON tra.Codigo=pvd.Transporta');
  VAR_SQLx.Add('LEFT JOIN entidades  red ON red.Codigo=pvd.Redespacho');
  VAR_SQLx.Add('LEFT JOIN ufs        uf1 ON uf1.Codigo=IF(cli.Tipo=0, cli.EUF, cli.PUF)');
  VAR_SQLx.Add('LEFT JOIN tabeprccab tpc ON tpc.Codigo=pvd.TabelaPrc');
  VAR_SQLx.Add('LEFT JOIN cambiomda  mda ON mda.Codigo=pvd.Moeda');
  VAR_SQLx.Add('LEFT JOIN pediprzcab ppc ON ppc.Codigo=pvd.CondicaoPG');
  VAR_SQLx.Add('LEFT JOIN motivos    mot ON mot.Codigo=pvd.MotivoSit');
  VAR_SQLx.Add('LEFT JOIN pediacc    acc ON acc.Codigo=pvd.Represen');
  VAR_SQLx.Add('LEFT JOIN entidades  ven ON ven.Codigo=acc.Codigo');
  VAR_SQLx.Add('LEFT JOIN carteiras  car ON car.Codigo=pvd.CartEmis');
  VAR_SQLx.Add('LEFT JOIN fisregcad  frc ON frc.Codigo=pvd.RegrFiscal');
  VAR_SQLx.Add('LEFT JOIN imprime    imp ON imp.Codigo=frc.ModeloNF');
  VAR_SQLx.Add('LEFT JOIN ' + VAR_AllID_DB_NOME + '.dtb_munici mun ON mun.Codigo=IF(cli.Tipo=0, cli.ECodMunici, cli.PCodMunici)');
  // NFe 4.00 NT 2018/05
  VAR_SQLx.Add('LEFT JOIN entidades  ret ON ret.Codigo=pvd.RetiradaEnti ');
  VAR_SQLx.Add('LEFT JOIN entidades  ent ON ent.Codigo=pvd.EntregaEnti');
  VAR_SQLx.Add('LEFT JOIN centrocusto ccr ON ccr.Codigo=pvd.CentroCusto');
  VAR_SQLx.Add('');
  VAR_SQLx.Add('WHERE pvd.Codigo > -1000');
  VAR_SQLx.Add('AND pvd.CodUsu > 0 ');
  VAR_SQLx.Add('AND pvd.Empresa IN (' + VAR_LIB_EMPRESAS + ')');
  //
  VAR_SQL1.Add('AND pvd.Codigo=:P0');
  //
  VAR_SQL2.Add('AND pvd.CodUsu=:P0');
  //
  //VAR_SQLa.Add('AND pvd.Nome Like :P0');
  //
  VAR_GOTOVAR1 := 'Empresa IN (' + VAR_LIB_EMPRESAS + ') AND CodUsu > 0';
end;

procedure TFmPediVda2.DesabilitaComponentes();
var
  Habilita0, Habilita1, Habilita2: Boolean;
begin
  DmodG.ReopenParamsEmp(QrPediVdaEmpresa.Value);
  //
  Habilita0 := not ((ImgTipo.SQLType = stUpd) and (QrPediVdaGru.RecordCount > 0));
  Habilita1 := False;
  if not Habilita0 then
  begin
    case DModG.QrParamsEmpPedVdaMudPrazo.Value of
      0: Habilita1 := False;
      1: Habilita1 := VAR_USUARIO < 0;
      2: Habilita1 := True;
    end;
  end else Habilita1 := True;
  //
  Habilita2 := Habilita1 or (QrPediVdaCondicaoPG.Value = 0);
  LaCondicaoPG.Enabled := Habilita2;
  EdCondicaoPG.Enabled := Habilita2;
  CBCondicaoPG.Enabled := Habilita2;
  BtCondicaoPG.Enabled := Habilita2;

  //

  if not Habilita0 then
  begin
    case DModG.QrParamsEmpPedVdaMudLista.Value of
      0: Habilita1 := False;
      1: Habilita1 := VAR_USUARIO < 0;
      2: Habilita1 := True;
    end;
  end else Habilita1 := True;
  Habilita2 := Habilita1 or (QrPediVdaTabelaPrc.Value = 0);
  LaTabelaPrc.Enabled := Habilita2;
  EdTabelaPrc.Enabled := Habilita2;
  CBTabelaPrc.Enabled := Habilita2;
  BtTabelaPrc.Enabled := Habilita2;
  //
end;

procedure TFmPediVda2.Distribuirdesconto1Click(Sender: TObject);
const
  FormCaption  = 'Valor Total do Desconto';
  ValCaption   = 'Informe o Valor:';
  WidthCaption = Length(ValCaption) * 7;
var
  ValorTotal, (*QtdeTotal,*) DescontoTotal, Restante, vProd, Fator, DescoV,
  DescoP, ValLiq, QuantP, vFrete, vSeg, vOutro, vDesc,
  ValBru, vBC, PrecoF: Double;
  ValVar: Variant;
  (*IDCtrl, OriCodi, OriCtrl, Tipo, Empresa, iniID*)Controle: Integer;
  Qry: TmySQLQuery;
begin
  PageControl1.ActivePageIndex := 1;
  //
  ReopenSumIts();
  ValorTotal := QrSumItsvProd.Value;
  if MyObjects.GetValorDmk(TFmGetValor, FmGetValor, dmktfDouble,
    0.00, 2, 0, '0,00', '', True, FormCaption, ValCaption, WidthCaption,
    ValVar) then
  begin
    Screen.Cursor := crHourGlass;
    try
      DescontoTotal := Geral.DMV(ValVar);
      //
      if DescontoTotal > ValorTotal then
      begin
        Geral.MB_Aviso('Desconto n�o pode ser maior que o valor dos produtos!');
        Exit;
      end;
      Restante := DescontoTotal;
      //
      Qry := TmySQLQuery.Create(Dmod);
      try
        UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
        'SELECT Controle, QuantP, ',
        'vProd, vFrete, vSeg, vOutro, vDesc, DescoV ',
        'FROM pedivdaits pvi ',
        'WHERE pvi.Codigo=' + Geral.FF0(QrPediVdaCodigo.Value),
        '']);
        if Qry.RecordCount > 0 then
        begin
          Qry.First;
          while not Qry.Eof do
          begin
            QuantP     := Qry.FieldByName('QuantP').AsFloat;
            DescoV     := Qry.FieldByName('DescoV').AsFloat;
            vFrete     := Qry.FieldByName('vFrete').AsFloat;
            vSeg       := Qry.FieldByName('vSeg').AsFloat;
            vOutro     := Qry.FieldByName('vOutro').AsFloat;
            vDesc      := Qry.FieldByName('vDesc').AsFloat;
            vProd      := Qry.FieldByName('vProd').AsFloat;
            Controle   := Qry.FieldByName('Controle').AsInteger;
            //
            if ValorTotal > 0 then
              Fator := vProd / ValorTotal
            else
              Fator := 0;
            //
            if Qry.RecNo = Qry.RecordCount then
              DescoV := Restante
            else
            begin
              DescoV := Round(Fator * DescontoTotal * 100) / 100;
              Restante := Restante - DescoV;
            end;
            if vProd > 0 then
              DescoP := (DescoV / vProd) * 100
            else
              DescoP := 0.00;
            //ValLiq := vProd - DescoV;
            vDesc    := DescoV;
            ValBru   := vProd + vFrete + vSeg (*- vDesc*) + vOutro;
            vBC      := vProd + vFrete + vSeg - vDesc + vOutro;
            ValLiq   := vBC;
            PrecoF := Round(vBC / QuantP * 100) / 100;

            if vProd > 0 then
              DescoP := vDesc / vProd * 100
            else
              DescoP := 0.00;

            //
            UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'pedivdaits', False, [
            'DescoV', 'DescoP', 'ValLiq',
            'vFrete', 'vSeg', 'vOutro', 'vDesc',
            'ValBru', 'vBC', 'PrecoF'
            ], [
            'Controle'], [
            DescoV, DescoP, ValLiq,
            vFrete, vSeg, vOutro, vDesc,
            ValBru, vBC, PrecoF
            ], [
            Controle], False);
            DmPediVda.AtualizaUmItemPediVda(Controle);
            //
            Qry.Next;
          end;
          //
          DmPediVda.AtzSdosPedido(FmPediVda2.QrPediVdaCodigo.Value);
          LocCod(FmPediVda2.QrPediVdaCodigo.Value, FmPediVda2.QrPediVdaCodigo.Value);
          //ReopenPediVdaGru(0);
        end else
          Geral.MB_Aviso('Nenhum registro de item foi localizado!');
      finally
        Qry.Free;
      end;
    finally
      Screen.Cursor := crDefault;
    end;
  end;
end;

procedure TFmPediVda2.DistribuirDespesasacessrias1Click(Sender: TObject);
begin
  DistribuiValorIncremento(TCampoIncrementoItemNFe.ciifVOutro);
end;

procedure TFmPediVda2.DistribuirFrete1Click(Sender: TObject);
begin
  DistribuiValorIncremento(TCampoIncrementoItemNFe.ciifVFrete);
end;

procedure TFmPediVda2.DistribuirSeguro1Click(Sender: TObject);
begin
  DistribuiValorIncremento(TCampoIncrementoItemNFe.ciifVSeg);
end;

procedure TFmPediVda2.DistribuiValorIncremento(CampoIncrementoItemNFe:
  TCampoIncrementoItemNFe);
const
 sProcname = 'TFmPediVda2.DistribuiValorIncremento()';
const
  ValCaption   = 'Informe o Valor:';
  WidthCaption = Length(ValCaption) * 7;
var
  ValorTotal, (*QtdeTotal,*) IncrementoTotal, Restante, vProd, Fator, DescoV,
  ValLiq, QuantP, vFrete, vSeg, vOutro, vDesc, DescoP,
  ValBru, vBC, PrecoF,
  IncremUni: Double;
  ValVar: Variant;
  (*IDCtrl, OriCodi, OriCtrl, Tipo, Empresa, iniID*)Controle: Integer;
  Qry: TmySQLQuery;
  FormCaption, NomNoTit, Campo: String;
begin
  PageControl1.ActivePageIndex := 1;
  //
  case CampoIncrementoItemNFe of
    TCampoIncrementoItemNFe.ciifVFrete:
    begin
      NomNoTit := 'Frete';
      Campo    := 'vFrete';
    end;
    TCampoIncrementoItemNFe.ciifVSeg:
    begin
      NomNoTit := 'Frete';
      Campo    := 'vFrete';
    end;
    TCampoIncrementoItemNFe.ciifVOutro:
    begin
      NomNoTit := 'Frete';
      Campo    := 'vFrete';
    end;
    //TCampoIncrementoItemNFe.ciifND:
    else
    begin
      Geral.MB_Erro('Campo n�o definido (1) em ' + sProcname);
      Exit;
    end;
  end;
  //
  ReopenSumIts();
  ValorTotal := QrSumItsvProd.Value;
  FormCaption  := 'XXX-XXXXX-001 :: Valor Total do ' + NomNoTit;
  if MyObjects.GetValorDmk(TFmGetValor, FmGetValor, dmktfDouble,
    0.00, 2, 0, '0,00', '', True, FormCaption, ValCaption, WidthCaption,
    ValVar) then
  begin
    Screen.Cursor := crHourGlass;
    try
      IncrementoTotal := Geral.DMV(ValVar);
      //
      Restante := IncrementoTotal;
      //
      Qry := TmySQLQuery.Create(Dmod);
      try
        UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
        'SELECT Controle, QuantP, ',
        'vProd, vFrete, vSeg, vOutro, vDesc, DescoV ',
        'FROM pedivdaits pvi ',
        'WHERE pvi.Codigo=' + Geral.FF0(QrPediVdaCodigo.Value),
        '']);
        if Qry.RecordCount > 0 then
        begin
          Qry.First;
          while not Qry.Eof do
          begin
            QuantP     := Qry.FieldByName('QuantP').AsFloat;
            DescoV     := Qry.FieldByName('DescoV').AsFloat;
            vFrete     := Qry.FieldByName('vFrete').AsFloat;
            vSeg       := Qry.FieldByName('vSeg').AsFloat;
            vOutro     := Qry.FieldByName('vOutro').AsFloat;
            vDesc      := Qry.FieldByName('vDesc').AsFloat;
            vProd      := Qry.FieldByName('vProd').AsFloat;
            Controle   := Qry.FieldByName('Controle').AsInteger;
            //
            if ValorTotal > 0 then
              Fator := vProd / ValorTotal
            else
              Fator := 0;
            //
            if Qry.RecNo = Qry.RecordCount then
              IncremUni := Restante
            else
            begin
              IncremUni := Round(Fator * IncrementoTotal * 100) / 100;
              Restante := Restante - IncremUni;
            end;
            //
            case CampoIncrementoItemNFe of
              TCampoIncrementoItemNFe.ciifVFrete: vFrete := IncremUni;
              TCampoIncrementoItemNFe.ciifVSeg:   vSeg   := IncremUni;
              TCampoIncrementoItemNFe.ciifVOutro: vOutro := IncremUni;
              //TCampoIncrementoItemNFe.ciifND:
              else
              begin
                Geral.MB_Erro('Campo n�o definido (2) em ' + sProcname);
                Exit;
              end;
            end;
            vDesc    := DescoV;
            ValBru   := vProd + vFrete + vSeg (*- vDesc*) + vOutro;
            vBC      := vProd + vFrete + vSeg - vDesc + vOutro;
            ValLiq   := vBC;
            PrecoF := Round(vBC / QuantP * 100) / 100;

            if vProd > 0 then
              DescoP := vDesc / vProd * 100
            else
              DescoP := 0.00;

            //
            UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'pedivdaits', False, [
            'DescoV', 'DescoP', 'ValLiq',
            'vFrete', 'vSeg', 'vOutro', 'vDesc',
            'ValBru', 'vBC', 'PrecoF'
            ], [
            'Controle'], [
            DescoV, DescoP, ValLiq,
            vFrete, vSeg, vOutro, vDesc,
            ValBru, vBC, PrecoF
            ], [
            Controle], False);
            DmPediVda.AtualizaUmItemPediVda(Controle);
            //
            Qry.Next;
          end;
          //
          DmPediVda.AtzSdosPedido(FmPediVda2.QrPediVdaCodigo.Value);
          LocCod(FmPediVda2.QrPediVdaCodigo.Value, FmPediVda2.QrPediVdaCodigo.Value);
          //ReopenPediVdaGru(0);
        end else
          Geral.MB_Aviso('Nenhum registro de item foi localizado!');
      finally
        Qry.Free;
      end;
    finally
      Screen.Cursor := crDefault;
    end;
  end;
end;

procedure TFmPediVda2.Duplica1Click(Sender: TObject);
var
  Codigo, Controle, Conta, CodUsu: Integer;
begin
  if (QrPediVda.State = dsInactive) or (QrPediVda.RecordCount = 0) then Exit;
  //
  if Geral.MB_Pergunta('Confirma a duplica��o do pedido atual?') = ID_YES then
  begin
    Screen.Cursor := crHourGlass;
    try
      QrPediVda.DisableControls;
      QrPediVdaCuz.DisableControls;
      //
      Codigo := UMyMod.BuscaEmLivreY_Def('pedivda', 'Codigo', stIns, 0);
      CodUsu := UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, 'PediVda', 'CodUsu', [], [], stIns, 0, siPositivo, nil);
      //
      if MyObjects.FIC(Codigo = 0, nil, 'Falha ao definir C�digo para a tabela "pedivda"!') then Exit;
      if MyObjects.FIC(CodUsu = 0, nil, 'Falha ao definir CodUsu para a tabela "pedivda"!') then Exit;
      //
      if UnDmkDAC_PF.InsereRegistrosPorCopia(DModG.QrUpdPID1, 'pedivda', TMeuDB,
        ['Codigo'], [QrPediVdaCodigo.Value],
        ['Codigo', 'CodUsu'],
        [Codigo, CodUsu],
        '', True, LaAviso1, LaAviso2) then
      begin
        QrPediVdaIts.Close;
        QrPediVdaIts.Params[0].AsInteger := QrPediVdaCodigo.Value;
        UMyMod.AbreQuery(QrPediVdaIts, Dmod.MyDB);
        //
        if QrPediVdaIts.RecordCount > 0 then
        begin
          QrPediVdaIts.First;
          while not QrPediVdaIts.Eof do
          begin
            Controle := UMyMod.BuscaEmLivreY_Def('pedivdaits', 'Controle', stIns, 0);
            //
            if MyObjects.FIC(Controle = 0, nil, 'Falha ao definir Controle para a tabela "pedivdaits"!') then Exit;
            //
            if UnDmkDAC_PF.InsereRegistrosPorCopia(DModG.QrUpdPID1, 'pedivdaits', TMeuDB,
              ['Controle'], [QrPediVdaItsControle.Value],
              ['Codigo', 'Controle', 'QuantV'],
              [Codigo, Controle, 0],
              '', True, LaAviso1, LaAviso2) then
            begin
              if (QrPediVdaCuz.State <> dsInactive) and (QrPediVdaCuz.RecordCount > 0) then
              begin
                QrPediVdaCuz.First;
                while not QrPediVdaCuz.Eof do
                begin
                  Conta := UMyMod.BuscaEmLivreY_Def('pedivdacuz', 'Conta', stIns, 0);
                  //
                  if MyObjects.FIC(Conta = 0, nil, 'Falha ao definir Conta para a tabela "pedivdacuz"!') then Exit;
                  //
                  if UnDmkDAC_PF.InsereRegistrosPorCopia(DModG.QrUpdPID1, 'pedivdacuz', TMeuDB,
                    ['Conta'], [QrPediVdaCuzConta.Value],
                    ['Conta', 'Controle'],
                    [Conta, Controle],
                    '', True, LaAviso1, LaAviso2) then
                  begin
                    //
                  end;
                  QrPediVdaCuz.Next;
                end;
              end;
            end;
            //
            QrPediVdaIts.Next;
          end;
        end;
      end;
    finally
      QrPediVda.EnableControls;
      QrPediVdaCuz.EnableControls;
      QrPediVdaIts.Close;
      Va(vpLast);
      MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
      //
      Screen.Cursor := crDefault;
      Geral.MB_Aviso('Duplica��o finalizada!');
    end;
  end;
end;

procedure TFmPediVda2.EdCartEmisRedefinido(Sender: TObject);
begin
  DefineIndicadorDoTipoDePagamento();
end;

procedure TFmPediVda2.EdCentroCustoChange(Sender: TObject);
begin
  if EdCentroCusto.ValueVariant = 0 then
    DBEdREF_NIV1.DataField := ''
  else
    DBEdREF_NIV1.DataField := 'REF_NIV1';
end;

procedure TFmPediVda2.EdClienteChange(Sender: TObject);
begin
  DefineEnderecoDeEntrega();
{$IfNDef SemNFe_0000}
  (*
  if ImgTipo.SQLType = stIns then
  begin
  Sempre atualiza para evitar erros
  *)
    UnNFe_PF.Configura_idDest(EdEmpresa, RG_idDest);
    UnNFe_PF.Configura_indFinal(RG_indFinal);
  (*
  end;
  *)
{$EndIf}
  if (ImgTipo.SQLType = stIns) and (RGEntSai.ItemIndex = 0) then
  begin
    RGMadeBy.ItemIndex := DmPediVda.QrClientesMadeBy.Value;
  end;
end;

procedure TFmPediVda2.EdCodUsuEnter(Sender: TObject);
begin
  //FCodUsuTxt_Old := EdCodUsu.Text;
end;

procedure TFmPediVda2.EdCodUsuKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key = VK_F4) and (ImgTipo.SQLType = stIns) then
    UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, 'PediVda', 'CodUsu', [], [],
    stIns, 0, siPositivo, EdCodUsu);
end;

procedure TFmPediVda2.EdCodUsuRedefinido(Sender: TObject);
(*
var
  CodUsuTxt: String;
*)
begin
(*
  if (EdReferenPedi.ValueVariant = FCodUsuTxt_Old) or
  (Trim(EdReferenPedi.ValueVariant) = EmptyStr) then
    EdReferenPedi.ValueVariant := EdCodUsu.Text;
*)
end;

procedure TFmPediVda2.EdCondicaoPGChange(Sender: TObject);
begin
  CkAFP_Sit.Checked := DmPediVda.QrPediPrzCabPercent2.Value > 0;
  EdAfp_Per.ValueVariant := DmPediVda.QrPediPrzCabPercent2.Value;
end;

procedure TFmPediVda2.EdCondicaoPGRedefinido(Sender: TObject);
begin
  DefineIndicadorDoTipoDePagamento();
end;

procedure TFmPediVda2.EdDesoAces_PChange(Sender: TObject);
begin
  if EdDesoAces_P.ValueVariant <> 0 then
  begin
    EdDesoAces_V.Enabled := False;
    CalculaDesoAces_V();
  end else EdDesoAces_V.Enabled := True;
end;

procedure TFmPediVda2.EdEmpresaChange(Sender: TObject);
begin
{$IfNDef SemNFe_0000}
  if ImgTipo.SQLType = stIns then
    UnNFe_PF.Configura_idDest(EdEmpresa, RG_idDest);
{$EndIf}
  DmPediVda.ReopenCartEmis(VuEmpresa.ValueVariant);
end;

procedure TFmPediVda2.EdEntregaEntiRedefinido(Sender: TObject);
begin
  PnEntregaEntiViw.Visible := EdEntregaEnti.ValueVariant <> 0;
  DefineEnderecoDeEntrega();
end;

procedure TFmPediVda2.EdFrete_PChange(Sender: TObject);
begin
  if EdFrete_P.ValueVariant <> 0 then
  begin
    EdFrete_V.Enabled := False;
    CalculaFrete_V();
  end else EdFrete_V.Enabled := True;
end;

procedure TFmPediVda2.Edide_finNFeChange(Sender: TObject);
begin
  Edide_finNFe_TXT.Text := UnNFe_PF.TextoDeCodigoNFe(nfeCTide_finNFe,  Edide_finNFe.ValueVariant);
end;

procedure TFmPediVda2.Edide_finNFeKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  TitCols: array[0..1] of String;
begin
  if Key = VK_F4 then
  begin
    TitCols[0] := 'C�d.';
    TitCols[1] := 'Descri��o';
    Edide_finNFe.Text := Geral.SelecionaItem(F_finNFe, 0,
      'SEL-LISTA-000 :: Finalidade de emiss�o da NF-e',
      TitCols, Screen.Width)
  end;
end;

procedure TFmPediVda2.EdindPresChange(Sender: TObject);
var
  Texto: String;
begin
  Geral.DescricaoDeArrStrStr(EdindPres.Text, FindPres, Texto, 0, 1);
  EdindPres_TXT.Text := Texto;
end;

procedure TFmPediVda2.EdindPresKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  TitCols: array[0..1] of String;
begin
  if Key = VK_F4 then
  begin
    TitCols[0] := 'C�d.';
    TitCols[1] := 'Descri��o';
    EdindPres.Text := Geral.SelecionaItem(FindPres, 0,
    'SEL-LISTA-000 :: Indicador de presen�a do comprador no estabelecimento comercial no momento da opera��o',
    TitCols, Screen.Width)
  end;
end;

procedure TFmPediVda2.EdIND_PGTOChange(Sender: TObject);
begin
  EdIND_PGTO_TXT.Text := UnNFe_PF.TextoDeCodigoNFe(nfeIND_PAG_EFD,  EdIND_PGTO.ValueVariant);
end;

procedure TFmPediVda2.EdIND_PGTOKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  TitCols: array[0..1] of String;
begin
  if Key = VK_F4 then
  begin
    TitCols[0] := 'C�d.';
    TitCols[1] := 'Descri��o';
    EdIND_PGTO.Text := Geral.SelecionaItem(F_IND_PGTO_EFD, 0,
      'SEL-LISTA-000 :: Indicador do tipo de pagamento',
      TitCols, Screen.Width)
  end;
end;


procedure TFmPediVda2.EdReferenPediKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key = VK_F4) and (EdSoliCompr_TXT.Text <> EmptyStr) then
    EdReferenPedi.ValueVariant := EdSoliCompr_TXT.Text;
end;

procedure TFmPediVda2.EdRegrFiscalChange(Sender: TObject);
begin
  EdModeloNF.Text := '';
  if not EdRegrFiscal.Focused then
    EdModeloNF.Text := DmPediVda.QrFisRegCadNO_MODELO_NF.Value;
end;

procedure TFmPediVda2.EdRegrFiscalExit(Sender: TObject);
begin
  EdModeloNF.Text := DmPediVda.QrFisRegCadNO_MODELO_NF.Value;
end;

procedure TFmPediVda2.EdRegrFiscalRedefinido(Sender: TObject);
begin
  RGentSai.ItemIndex := DmPediVda.QrFisRegCadTipoMov.Value;
end;

procedure TFmPediVda2.EdRepresenChange(Sender: TObject);
begin
  EdComisFat.ValueVariant := DmPediVda.QrPediAccPerComissF.Value;
  EdComisRec.ValueVariant := DmPediVda.QrPediAccPerComissR.Value;
end;

procedure TFmPediVda2.EdRetiradaEntiRedefinido(Sender: TObject);
begin
  PnRetiradaEntiViw.Visible := EdRetiradaEnti.ValueVariant <> 0;
end;

procedure TFmPediVda2.EdSeguro_PChange(Sender: TObject);
begin
  if EdSeguro_P.ValueVariant <> 0 then
  begin
    EdSeguro_V.Enabled := False;
    CalculaSeguro_V();
  end else EdSeguro_V.Enabled := True;
end;

procedure TFmPediVda2.EdTabelaPrcChange(Sender: TObject);
begin
  if ImgTipo.SQLType = stIns then
  begin
    EdMoeda.ValueVariant := DmPediVda.QrTabePrcCabMoeda.Value;
    CBMoeda.KeyValue     := DmPediVda.QrTabePrcCabMoeda.Value;
  end;
end;

procedure TFmPediVda2.Estrio1Click(Sender: TObject);
var
  Entidade: Integer;
begin
  Entidade := EdCliente.ValueVariant;
  if Entidade <> 0 then
    GFat_Jan.MostraFormFisRegEnPsq(Entidade, EdRegrFiscal, CBRegrFiscal)
  else
    Geral.MB_Aviso('Defina o destinat�rio!');
end;

procedure TFmPediVda2.Excluiitemprodutoselecionado1Click(Sender: TObject);
begin
  DBCheck.QuaisItens_Exclui(Dmod.QrUpd, QrCustomizados, DBGrid1, 'pedivdaits',
   ['Controle'], ['Controle'], istPergunta, '');
  DmPediVda.AtzSdosPedido(QrPediVdaCodigo.Value);
  ReopenPediVdaGru(QrPediVdaGruNivel1.Value);
end;

procedure TFmPediVda2.Excluipartedoprodutoselecionado1Click(Sender: TObject);
begin
  DBCheck.QuaisItens_Exclui(Dmod.QrUpd, QrPediVdaCuz, DBGrid2, 'pedivdacuz',
   ['Conta'], ['Conta'], istPergunta, '');
  AtualizaItemCustomizado(QrCustomizadosControle.Value);
  DmPediVda.AtzSdosPedido(QrPediVdaCodigo.Value);
end;

procedure TFmPediVda2.Modelo11Click(Sender: TObject);
begin
  Imprime(True);
end;

procedure TFmPediVda2.Modelosemgrade1Click(Sender: TObject);
begin
  Imprime(False);
end;

procedure TFmPediVda2.MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo: Integer);
begin
  case Mostra of
    0:
    begin
      PainelDados.Visible := True;
      PainelEdita.Visible := False;
    end;
    else
      Geral.MB_Aviso('A��o de Inclus�o/altera��o n�o definida!');
  end;
  ImgTipo.SQLType := SQLType;
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmPediVda2.MostraEnderecoDeEntrega2();
var
  Entidade: Integer;
begin
  if (QrPediVdaL_Ativo.Value = 1) and (QrPediVdaEntregaEnti.Value = 0) then
  begin
    QrEntregaCli.Close;
    QrEntregaCli.Params[00].AsInteger := QrPediVdaCliente.Value;
    UMyMod.AbreQuery(QrEntregaCli, Dmod.MyDB, 'TFmFatDivGer2.QrCliAfterOpen()');
    //
    //DsEntregaEnti.DataSet := QrEntregaCli;
    MeEnderecoEntrega2.Text := QrEntregaCliE_ALL.Value;
  end else
  begin
    if QrPediVdaEntregaEnti.Value = 0 then
      Entidade := QrPediVdaCliente.Value
    else
      Entidade := QrPediVdaEntregaEnti.Value;
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrEntregaEnti, Dmod.MyDB, [
    'SELECT en.Codigo, en.Cadastro, ENatal, PNatal, Tipo, Respons1, Respons2, ',
    'ENumero, PNumero, ELograd, PLograd, ECEP, PCEP, ',
    'IF(en.Tipo=0, en.RazaoSocial, en.Nome   ) NOME_ENT, ',
    'IF(en.Tipo=0, en.Fantasia   , en.Apelido) NO_2_ENT, ',
    'IF(en.Tipo=0, en.CNPJ       , en.CPF    ) CNPJ_CPF, ',
    'IF(en.Tipo=0, en.IE         , en.RG     ) IE_RG, ',
    'IF(en.Tipo=0, en.NIRE       , ""        ) NIRE_, ',
    'IF(en.Tipo=0, en.ERua       , en.PRua   ) RUA, ',
    'IF(en.Tipo=0, en.ESite       , en.PSite   ) SITE, ',
    'IF(en.Tipo=0, en.ENumero    , en.PNumero) NUMERO, ',
    'IF(en.Tipo=0, en.ECompl     , en.PCompl ) COMPL, ',
    'IF(en.Tipo=0, en.EBairro    , en.PBairro) BAIRRO, ',
    'mun.Nome CIDADE, ',
    'IF(en.Tipo=0, lle.Nome      , llp.Nome  ) NOMELOGRAD, ',
    'IF(en.Tipo=0, ufe.Nome      , ufp.Nome  ) NOMEUF, ',
    'pai.Nome Pais, ',
    'IF(en.Tipo=0, en.ELograd    , en.PLograd) Lograd, ',
    'IF(en.Tipo=0, en.ECEP       , en.PCEP   ) CEP, ',
    'IF(en.Tipo=0, en.ETe1       , en.PTe1   ) TE1, ',
    'IF(en.Tipo=0, en.EFax       , en.PFax   ) FAX, ',
    'IF(en.Tipo=0, en.EEmail     , en.PEmail ) EMAIL, ',
    'IF(en.Tipo=0, lle.Trato     , llp.Trato ) TRATO, ',
    'IF(en.Tipo=0, en.EEndeRef   , en.PEndeRef  ) ENDEREF, ',
    'IF(en.Tipo=0, en.ECodMunici , en.PCodMunici) + 0.000 CODMUNICI, ',
    'IF(en.Tipo=0, en.ECodiPais  , en.PCodiPais ) + 0.000 CODPAIS, ',
    'RG, SSP, DataRG, IE ',
    'FROM entidades en ',
    'LEFT JOIN ufs ufe ON ufe.Codigo=en.EUF ',
    'LEFT JOIN ufs ufp ON ufp.Codigo=en.PUF ',
    'LEFT JOIN listalograd lle ON lle.Codigo=en.ELograd ',
    'LEFT JOIN listalograd llp ON llp.Codigo=en.PLograd ',
    'LEFT JOIN ' + VAR_AllID_DB_NOME + '.dtb_munici mun ON mun.Codigo = IF(en.Tipo=0, en.ECodMunici, en.PCodMunici) ',
    'LEFT JOIN ' + VAR_AllID_DB_NOME + '.bacen_pais pai ON pai.Codigo = IF(en.Tipo=0, en.ECodiPais, en.PCodiPais) ',
    'WHERE en.Codigo=' + Geral.FF0(Entidade),
    'ORDER BY NOME_ENT',
    '']);
    //DsEntregaEnti.DataSet := QrEntregaEnti;
    MeEnderecoEntrega2.Text := QrEntregaEntiE_ALL.Value;
  end;
end;

procedure TFmPediVda2.MostraFmPediVdaCuzParIns(CodUsu, PrdGrupTip, Nivel1, GraGruX, Controle:
  Integer; SQLType: TSQLType;
  QuantP, MedidaC, MedidaL, MedidaA, MedidaE: Double);
var
  Continuar: Boolean;
  Num: String;
begin
  if DBCheck.CriaFm(TFmPediVdaCuzParIns2, FmPediVdaCuzParIns2, afmoNegarComAviso) then
  begin
    FmPediVdaCuzParIns2.ImgTipo.SQLType := SQLType;
    //FmPediVdaCuzParIns2.FGraGruX := GraGruX;
    FmPediVdaCuzParIns2.ReopenGraGruX(GraGruX);
    FmPediVdaCuzParIns2.FControle := Controle;
    if CodUsu > 0 then
    begin
      DmPediVda.QrPP.Close;
      DmPediVda.QrPP.Params[0].AsInteger := Nivel1;
      UnDmkDAC_PF.AbreQuery(DmPediVda.QrPP, Dmod.MyDB);
      //
      FmPediVdaCuzParIns2.RGGrupTip.ItemIndex       := 3;
      FmPediVdaCuzParIns2.EdMatPartCad.ValueVariant := DmPediVda.QrPPPartePrinc.Value;
      FmPediVdaCuzParIns2.CBMatPartCad.KeyValue     := DmPediVda.QrPPPartePrinc.Value;
      FmPediVdaCuzParIns2.EdGraGru1.ValueVariant    := CodUsu;
      FmPediVdaCuzParIns2.CBGraGru1.KeyValue        := CodUsu;
      FmPediVdaCuzParIns2.EdMedidaC.ValueVariant    := MedidaC;
      FmPediVdaCuzParIns2.EdMedidaL.ValueVariant    := MedidaL;
      FmPediVdaCuzParIns2.EdMedidaA.ValueVariant    := MedidaA;
      FmPediVdaCuzParIns2.EdMedidaE.ValueVariant    := MedidaE;
      FmPediVdaCuzParIns2.EdQuantP.ValueVariant     := 1; //QuantP;  Deve ser s� um !
      //
      Num := FormatFloat('0', GraGruX);
      {
      for C := 1 to GradeC.ColCount - 1 do
        for R := 1 to GradeC.RowCount do
        begin
          Txt := Geral.SoNumero_TT(GradeC.Cells[C,R]);
          if Txt = Num then
          begin
            FmPediVdaCuzParIns2.EdGraGruX.Text := Num;
            Break;
          end;
        end;
      }
      FmPediVdaCuzParIns2.EdGraGruX.Text := Num;
      FmPediVdaCuzParIns2.FLimitaCorTam := False;
    end else begin
      {
      }
    end;
    DmProd.QrNeed1.Close;
    DmProd.QrNeed1.Params[00].AsInteger := GraGruX;
    UnDmkDAC_PF.AbreQuery(DmProd.QrNeed1, Dmod.MyDB);
    FmPediVdaCuzParIns2.ShowModal;
    Continuar :=
      FmPediVdaCuzParIns2.CkContinuar.Checked;
    FmPediVdaCuzParIns2.Destroy;
    //
    if Continuar then
      MostraFmPediVdaCuzParIns(0, 0, 0, GraGruX, Controle, SQLType, 0, 0, 0, 0, 0);
  end;
end;

procedure TFmPediVda2.MostraFormEntidade2(Entidade: Integer; Query: TmySQLQuery;
  EditCliente: TdmkEditCB; ComboCliente: TdmkDBLookupComboBox);
begin
  VAR_CADASTRO := 0;
  //
  DModG.CadastroDeEntidade(Entidade, fmcadEntidade2, fmcadEntidade2);
  //
  if VAR_CADASTRO <> 0 then
  begin
    UnDmkDAC_PF.AbreQuery(Query, Dmod.MyDB);
    //
    if Query.Locate('Codigo', VAR_CADASTRO, []) then
    begin
      EditCliente.ValueVariant := VAR_ENTIDADE;
      ComboCliente.KeyValue    := VAR_ENTIDADE;
      EditCliente.SetFocus;
    end;
  end;
end;

procedure TFmPediVda2.MostraPediVdaGru(SQLType: TSQLType);
begin
  DmPediVda.ReopenParamsEmp(QrPediVdaEmpresa.Value, True);
  if DBCheck.CriaFm(TFmPediVdaGru2, FmPediVdaGru2, afmoNegarComAviso) then
  begin
    FmPediVdaGru2.FPedidoCompra := True;
    FmPediVdaGru2.ImgTipo.SQLType := SQLType;
    if SQLType = stUpd then
    begin
      FmPediVdaGru2.PnSeleciona.Enabled    := False;
      FmPediVdaGru2.EdGraGru1.ValueVariant := QrPediVdaGruNivel1.Value;
      FmPediVdaGru2.CBGraGru1.KeyValue     := QrPediVdaGruNivel1.Value;
    end;
    FmPediVdaGru2.ShowModal;
    FmPediVdaGru2.Destroy;
  end;
end;

procedure TFmPediVda2.MostraPediVdaLei(SQLType: TSQLType);
begin
  DmPediVda.ReopenParamsEmp(QrPediVdaEmpresa.Value, True);
  if DBCheck.CriaFm(TFmPediVdaLei2, FmPediVdaLei2, afmoNegarComAviso) then
  begin
    FmPediVdaLei2.ImgTipo.SQLType := SQLType;
    FmPediVdaLei2.ShowModal;
    FmPediVdaLei2.Destroy;
    ReopenPediVdaGru(QrPediVdaGruNivel1.Value);
  end;
end;

procedure TFmPediVda2.ObtemContasContabeis(var GenCtbD, GenCtbC: Integer);
var
  Qry: TmySQLQuery;
begin
  GenCtbD := 0;
  GenCtbC := 0;
  Qry := TmySQLQuery.Create(Dmod);
  //
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT GenCtbD, GenCtbC ',
    'FROM fisregcad ',
    'WHERE Codigo=' + Geral.FF0(QrPediVdaRegrFiscal.Value),
    '']);
    GenCtbD := Qry.FieldByName('GenCtbD').AsInteger;
    GenCtbC := Qry.FieldByName('GenCtbC').AsInteger;
  finally
    Qry.Free;
  end;
end;

{
function TFmPediVda2.ObtemQtde(var Qtde: Double): Boolean;
var
  ResVar: Variant;
begin
  // N�o pode, j� vem definido
  //Qtde := 0;
  Result := False;
  if MyObjects.GetValorDmk(TFmGetValor, FmGetValor, dmktfDouble,
  0, 3, 0, '', '', True, 'Itens', 'Informe a quantidade de itens: ',
  0, ResVar) then
  begin
    Qtde := Geral.IMV(ResVar);
    Result := True;
  end;
end;
}

procedure TFmPediVda2.PMCustomPopup(Sender: TObject);
var
  Habilita: Boolean;
begin
  Habilita :=
    (QrCustomizados.State <> dsInactive)
    and
    (QrCustomizados.RecordCount > 0);
  Adicionapartesaoitemselecionado1.Enabled := Habilita;

  //

  Habilita := Habilita and
    (QrCustomizados.State <> dsInactive)
    and
    (QrCustomizados.RecordCount > 0)
    and
    (QrPediVdaCuz.State <> dsInactive)
    and
    (QrPediVdaCuz.RecordCount = 0);
  Excluiitemprodutoselecionado1.Enabled := Habilita;

  //

  Habilita :=
    (QrPediVdaCuz.State <> dsInactive)
    and
    (QrPediVdaCuz.RecordCount > 0);
  Excluipartedoprodutoselecionado1.Enabled := Habilita;
end;

procedure TFmPediVda2.PMPedidosPopup(Sender: TObject);
var
  Habilita: Boolean;
begin
  Habilita := (QrPediVda.State <> dsInactive) and (QrPediVda.RecordCount > 0);
  Alterapedidoatual1.Enabled := Habilita;
  Excluipedidoatual1.Enabled := Habilita;
  //
  DistribuirFrete1.Enabled := Habilita;
  DistribuirSeguro1.Enabled := Habilita;
  DistribuirDespesasacessrias1.Enabled := Habilita;
  Distribuirdesconto1.Enabled := Habilita;
  //
end;

procedure TFmPediVda2.Pagamentovistaantecipado1Click(Sender: TObject);
begin
  IncluiLctoF(Sender);
end;

procedure TFmPediVda2.PageControl1Change(Sender: TObject);
begin
  case PageControl1.ActivePageIndex of
    0: BtVisual.Visible := False;
    1: BtVisual.Visible := True;
  end;
end;

procedure TFmPediVda2.Cadastro1Click(Sender: TObject);
{$IfNDef NAO_GFAT}
var
  RegrFiscal: Integer;
begin
  VAR_CADASTRO := 0;
  //
  if not UMyMod.ObtemCodigoDeCodUsu(EdRegrFiscal, RegrFiscal,
    'Informe a regra fiscal!', 'Codigo', 'CodUsu') then Exit;
  //
  Grade_Jan.MostraFormFisRegCad(RegrFiscal);
  //
  if VAR_CADASTRO <> 0 then
  begin
    DmPediVda.QrFisRegCad.Close;
    UnDmkDAC_PF.AbreQuery(DmPediVda.QrFisRegCad, Dmod.MyDB);
    if DmPediVda.QrFisRegCad.Locate('Codigo', VAR_CADASTRO, []) then
    begin
      EdRegrFiscal.ValueVariant := DmPediVda.QrFisRegCadCodUsu.Value;
      CBRegrFiscal.KeyValue     := DmPediVda.QrFisRegCadCodUsu.Value;
      EdRegrFiscal.SetFocus;
    end;
  end;
{$Else}
  Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappGFat);
{$EndIf}
end;

procedure TFmPediVda2.CalculaDesoAces_V;
begin
  // Parei aqui!!! Falta fazer
end;

function TFmPediVda2.CalculaDiferencas(): Double;
begin
  Result := 0.00;
  if UFinanceiro.TabLctNaoDef(FTabLctA, False) then
    Exit;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrSumF, Dmod.MyDB, [
    'SELECT SUM(la.Debito) Debito ',
    'FROM ' + FTabLctA + ' la ',
    'WHERE la.FatID=' + Geral.FF0(VAR_FATID_1001),
    //'AND la.FatNum=' + Geral.FF0(QrPQECodigo.Value),
    'AND la.FatParcRef=' + Geral.FF0(QrPediVdaCodigo.Value),
    'AND la.ID_Pgto = 0 ',
    '']);
  //
  Result := QrPediVdaValLiq.Value - QrSumFDebito.Value;
end;

procedure TFmPediVda2.CalculaFrete_V;
begin
  // Parei aqui!!! Falta fazer
end;

procedure TFmPediVda2.CalculaSeguro_V;
begin
  // Parei aqui!!! Falta fazer
end;

procedure TFmPediVda2.CBMotivoSitExit(Sender: TObject);
begin
  TPDtaEmiss.SetFocus;
end;

procedure TFmPediVda2.CkEntregaUsaClick(Sender: TObject);
begin
  PnEntregaEntiAll.Visible := CkEntregaUsa.Checked;
  DefineEnderecoDeEntrega();
end;

procedure TFmPediVda2.CkRetiradaUsaClick(Sender: TObject);
begin
  PnRetiradaEntiAll.Visible := CkRetiradaUsa.Checked;
end;

procedure TFmPediVda2.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  //Va(vpLast);
end;

procedure TFmPediVda2.Custumizao1Click(Sender: TObject);
begin

end;

{
procedure TFmPediVda2.AlteraRegistro;
var
  PediVda : Integer;
begin
  PediVda := QrPediVdaCodigo.Value;
  if QrPediVdaCodigo.Value = 0 then
  begin
    BtAltera.Enabled := False;
    Exit;
  end;
  if not UMyMod.SelLockY(PediVda, Dmod.MyDB, 'PediVda', 'Codigo') then
  begin
    try
      UMyMod.UpdLockY(PediVda, Dmod.MyDB, 'PediVda', 'Codigo');
      MostraEdicao(1, CO_ALTERACAO, 0);
    finally
      Screen.Cursor := Cursor;
    end;
  end;
end;

procedure TFmPediVda2.IncluiRegistro;
var
  Cursor : TCursor;
  PediVda : Integer;
begin
  Cursor := Screen.Cursor;
  Screen.Cursor := crHourglass;
  Refresh;
  try
    PediVda := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
    'PediVda', 'PediVda', 'Codigo');
    if Length(FormatFloat(FFormatFloat, PediVda))>Length(FFormatFloat) then
    begin
      Geral.MB_Erro('Inclus�o cancelada. Limite de cadastros extrapolado');
      Screen.Cursor := Cursor;
      Exit;
    end;
    MostraEdicao(1, stIns, PediVda);
  finally
    Screen.Cursor := Cursor;
  end;
end;
}

procedure TFmPediVda2.QueryPrincipalAfterOpen;
begin
end;

procedure TFmPediVda2.DBEdindPresChange(Sender: TObject);
var
  Texto: String;
begin
  Geral.DescricaoDeArrStrStr(DBEdindPres.Text, FindPres, Texto, 0, 1);
  EdDBindPres_TXT.Text := Texto;
end;

procedure TFmPediVda2.DBEdIND_PGTOChange(Sender: TObject);
var
  Texto: String;
begin
  Geral.DescricaoDeArrStrStr(DBEdIND_PGTO.Text, F_IND_PGTO_EFD, Texto, 0, 1);
  EdDBIND_PGTO_TEXT.Text := Texto;
end;

procedure TFmPediVda2.DBEdfinNFeChange(Sender: TObject);
var
  Texto: String;
begin
  Geral.DescricaoDeArrStrStr(DBEdfinNFe.Text, F_finNFe, Texto, 0, 1);
  EdDBfinNFe_TXT.Text := Texto;
end;

procedure TFmPediVda2.DefineEnderecoDeEntrega();
var
  Linha1, linha2, linha3: String;
  Entidade: Integer;
begin
  if (CkEntregaUsa.Checked) and (EdEntregaEnti.ValueVariant <> 0) then
    Entidade := EdEntregaEnti.ValueVariant
  else
    Entidade := EdCliente.ValueVariant;
  //
  MeEnderecoEntrega1.Text := DModG.ObtemEnderecoEntrega3Linhas(
    Entidade, Linha1, Linha2, Linha3);
  if EdCliente.ValueVariant = 0 then
  begin
    DBEdDOCENT.DataField := '';
    DBEdCidade.DataField := '';
    DBEdNOMEUF.DataField := '';
  end else begin
    DBEdDOCENT.DataField := 'DOCENT';
    DBEdCidade.DataField := 'CIDADE';
    DBEdNOMEUF.DataField := 'NOMEUF';
  end;
end;

procedure TFmPediVda2.DefineIndicadorDoTipoDePagamento;
begin
(* CondPg
0-Indefinido
1-Adiantado
2-� vista
3-� Prazo
4-Outros
*)
  if VerificaCarteira_x_CondPg(False) then
  begin
    //0: Result := '� Vista';
    //1: Result := '� Prazo;';
    //2: Result := 'Outros';
    case DmPediVda.QrPediPrzCabCondPg.Value of
      0: ; // nada!
      1,
      2: EdIND_PGTO.ValueVariant := 0;
      3: EdIND_PGTO.ValueVariant := 1;
      4: ; // nada!
    end;
  end;
end;

procedure TFmPediVda2.DefineONomeDoForm;
begin
end;

procedure TFmPediVda2.DefineVarDup();
begin
  IC3_ED_FatNum := 0; //QrPQECodigo.Value;
  IC3_ED_NF     := 0; //QrPQENF.Value;
  IC3_ED_Data   := QrPediVdaDtaEmiss.Value;
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmPediVda2.BtVisualClick(Sender: TObject);
begin
  if DBGGru.Align = alLeft then
  begin
    FDBGWidth := DBGGru.Width;
    DBGGru.Align := alTop;
    DBGGru.Height := 68; // 20 + 18 + 20
  end else begin
    DBGGru.Align := alLeft;
    DBGGru.Width := FDBGWidth;
  end;
end;

procedure TFmPediVda2.BitBtn1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmPediVdaImp, FmPediVdaImp, afmoNegarComAviso) then
  begin
    FmPediVdaImp.ShowModal;
    FmPediVdaImp.Destroy;
  end;
end;

procedure TFmPediVda2.BtCondicaoPGClick(Sender: TObject);
var
  CondicaoPG: Integer;
begin
  VAR_CADASTRO := 0;
  //
  if DmPediVda.QrPediPrzCab.RecordCount > 0 then
  begin
    if not UMyMod.ObtemCodigoDeCodUsu(EdCondicaoPG, CondicaoPG,
      'Informe a condi��o de pagamento!', 'Codigo', 'CodUsu') then Exit;
  end else
    CondicaoPG := 0;
  //
  Praz_PF.MostraFormPediPrzCab1(CondicaoPG);
  //
  if VAR_CADASTRO <> 0 then
  begin
    DmPediVda.QrPediPrzCab.Close;
    UnDmkDAC_PF.AbreQuery(DmPediVda.QrPediPrzCab, Dmod.MyDB);
    if DmPediVda.QrPediPrzCab.Locate('Codigo', VAR_CADASTRO, []) then
    begin
      EdCondicaoPg.ValueVariant := VAR_CADASTRO;
      CBCondicaoPg.KeyValue     := VAR_CADASTRO;
      EdCondicaoPG.SetFocus;
    end;
  end;
end;

procedure TFmPediVda2.SpeedButton11Click(Sender: TObject);
begin
  MostraFormEntidade2(EdRedespacho.ValueVariant, DmPediVda.QrTransportas,
    EdRedespacho, CBRedespacho);
end;

procedure TFmPediVda2.SpeedButton12Click(Sender: TObject);
var
  Represen: Integer;
begin
  VAR_CADASTRO := 0;
  Represen     := EdRepresen.ValueVariant;
  //
  if DBCheck.CriaFm(TFmPediAcc, FmPediAcc, afmoNegarComAviso) then
  begin
    if Represen <> 0 then
      FmPediAcc.LocCod(Represen, Represen);
    FmPediAcc.ShowModal;
    FmPediAcc.Destroy;
    if VAR_CADASTRO <> 0 then
    begin
      DmPediVda.QrPediAcc.Close;
      UnDmkDAC_PF.AbreQuery(DmPediVda.QrPediAcc, Dmod.MyDB);
      //if DmPediVda.QrPediAcc.Locate('Codigo', VAR_CADASTRO, []) then
      //begin
        EdRepresen.ValueVariant := VAR_CADASTRO;
        CBRepresen.KeyValue     := VAR_CADASTRO;
        EdRepresen.SetFocus;
      //end;
    end;
  end;
end;

procedure TFmPediVda2.SpeedButton13Click(Sender: TObject);
{$IfNDef NO_FINANCEIRO}
var
  CartEmis: Integer;
begin
  VAR_CADASTRO := 0;
  CartEmis     := EdCartEmis.ValueVariant;
  //
  FinanceiroJan.CadastroDeCarteiras(CartEmis);
  //
  if VAR_CADASTRO <> 0 then
  begin
    DmPediVda.ReopenCartEmis(VuEmpresa.ValueVariant);
    //
    EdCartEmis.ValueVariant := VAR_CADASTRO;
    CBCartEmis.KeyValue     := VAR_CADASTRO;
    EdCartEmis.SetFocus;
  end;
{$Else}
  Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappFinanceiro);
{$EndIf}
end;

procedure TFmPediVda2.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmPediVda2.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmPediVda2.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmPediVda2.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmPediVda2.SpeedButton5Click(Sender: TObject);
begin
  MostraFormEntidade2(EdCliente.ValueVariant, DmPediVda.QrClientes, EdCliente,
    CBCliente);
end;

procedure TFmPediVda2.SpeedButton6Click(Sender: TObject);
begin
  MostraFormEntidade2(EdTransporta.ValueVariant, DmPediVda.QrTransportas,
    EdTransporta, CBTransporta);
end;

procedure TFmPediVda2.SpeedButton7Click(Sender: TObject);
var
  MotivoSit: Integer;
begin
  VAR_CADASTRO := 0;
  //
  if not UMyMod.ObtemCodigoDeCodUsu(EdMotivoSit, MotivoSit,
    'Informe o motivo da situa��o!', 'Codigo', 'CodUsu') then Exit;
  //
  if DBCheck.CriaFm(TFmMotivos, FmMotivos, afmoNegarComAviso) then
  begin
    if MotivoSit <> 0 then
      FmMotivos.LocCod(MotivoSit, MotivoSit);
    FmMotivos.ShowModal;
    FmMotivos.Destroy;
    if VAR_CADASTRO <> 0 then
    begin
      DmPediVda.QrMotivos.Close;
      UnDmkDAC_PF.AbreQuery(DmPediVda.QrMotivos, Dmod.MyDB);
      if DmPediVda.QrMotivos.Locate('Codigo', VAR_CADASTRO, []) then
      begin
        EdMotivoSit.ValueVariant := VAR_CADASTRO;
        CBMotivoSit.KeyValue     := VAR_CADASTRO;
        EdMotivoSit.SetFocus;
      end;
    end;
  end;
end;

procedure TFmPediVda2.SpeedButton8Click(Sender: TObject);
begin
  if EdCliente.ValueVariant <> 0 then
  begin
    Geral.MB_Aviso('OBSERVA��ES:' + sLineBreak + DmPediVda.QrClientesObservacoes.Value);
  end else
    Geral.MB_Aviso('Cliente n�o informado!');
end;

procedure TFmPediVda2.BtTabelaPrcClick(Sender: TObject);
{$IfNDef NAO_GFAT}
var
  TabelaPrc: Integer;
begin
  VAR_CADASTRO := 0;
  //
  if not UMyMod.ObtemCodigoDeCodUsu(EdTabelaPrc, TabelaPrc,
    'Informe a tabela de pre�o!', 'Codigo', 'CodUsu') then Exit;
  //
  GFat_Jan.MostraFormTabePrcCab(TabelaPrc);
  //
  if VAR_CADASTRO <> 0 then
  begin
    DmPediVda.QrTabePrcCab.Close;
    UnDmkDAC_PF.AbreQuery(DmPediVda.QrTabePrcCab, Dmod.MyDB);
    if DmPediVda.QrTabePrcCab.Locate('Codigo', VAR_CADASTRO, []) then
    begin
      EdTabelaPrc.ValueVariant := VAR_CADASTRO;
      CBTabelaPrc.KeyValue     := VAR_CADASTRO;
      EdTabelaPrc.SetFocus;
    end;
  end;
{$Else}
  Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappGFat);
{$EndIf}
end;

procedure TFmPediVda2.SpeedButton9Click(Sender: TObject);
var
  Moeda: Integer;
begin
  VAR_CADASTRO := 0;
  //
  if not UMyMod.ObtemCodigoDeCodUsu(EdMoeda, Moeda,
    'Informe a moeda!', 'Codigo', 'CodUsu') then Exit;
  //
  if DBCheck.CriaFm(TFmCambioMda, FmCambioMda, afmoNegarComAviso) then
  begin
    if Moeda <> 0 then
      FmCambioMda.LocCod(Moeda, Moeda);
    FmCambioMda.ShowModal;
    FmCambioMda.Destroy;
    if VAR_CADASTRO <> 0 then
    begin
      DModG.QrCambioMda.Close;
      UnDmkDAC_PF.AbreQuery(DModG.QrCambioMda, Dmod.MyDB);
      if DModG.QrCambioMda.Locate('Codigo', VAR_CADASTRO, []) then
      begin
        EdMoeda.ValueVariant := VAR_CADASTRO;
        CBMoeda.KeyValue     := VAR_CADASTRO;
        EdMoeda.SetFocus;
      end;
    end;
  end;
end;

procedure TFmPediVda2.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrPediVdaCodigo.Value;
  Close;
end;

procedure TFmPediVda2.Adicionapartesaoitemselecionado1Click(Sender: TObject);
begin
  MostraFmPediVdaCuzParIns(0, 0, 0, QrCustomizadosGraGruX.Value,
    QrCustomizadosControle.Value, stIns, 0, 0, 0, 0, 0);
end;

procedure TFmPediVda2.AlteraExcluiIncluiitemselecionado1Click(Sender: TObject);
begin
  AlteraOuExcluiItem();
end;

procedure TFmPediVda2.Alteraitemprodutoselecionado1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmPediVdaCuzUpd2, FmPediVdaCuzUpd2, afmoNegarComAviso) then
  begin
    FmPediVdaCuzUpd2.ReopenPediVdaIts(QrCustomizadosControle.Value);
    FmPediVdaCuzUpd2.ShowModal;
    FmPediVdaCuzUpd2.Destroy;
  end;
end;

procedure TFmPediVda2.AlteraOuExcluiItem();
begin
  //n�o alterar / excluir quando tiver customiza��o
  GradeQDblClick2();
  DmPediVda.AtualizaTodosItensSoliCompr_(QrPediVdaSoliComprCab.Value);
end;

procedure TFmPediVda2.Alterapedidoatual1Click(Sender: TObject);
begin
  // ini 2022-04-03
  // Evitar erros
  RGEntSai.ItemIndex := QrPediVdaEntSai.Value;
  // fim 2022-04-03
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PainelEdita, QrPediVda, [PainelDados],
    [PainelEdita], nil, ImgTipo, 'pedivda');
  //
  RGIndSinc.ItemIndex := 1;
  RGIndSinc.Enabled   := False;
  //
  DesabilitaComponentes();
end;

procedure TFmPediVda2.AplicardescontoGeral1Click(Sender: TObject);
begin
//
end;

procedure TFmPediVda2.AtualizaItemCustomizado(Controle: Integer);
var
  PrecoO, PrecoR, PrecoF, ValBru, ValLiq, DescoP, DescoV, PercCustom, QuantP: Double;
  InfAdCuztm, Codigo: Integer;
  Nome: String;
begin
  InfAdCuztm := 0;
  DmPediVda.QrIts.Close;
  DmPediVda.QrIts.Params[0].AsInteger := Controle;
  UnDmkDAC_PF.AbreQuery(DmPediVda.QrIts, Dmod.MyDB);
  //
  DmPediVda.QrSumCuz.Close;
  DmPediVda.QrSumCuz.Params[0].AsInteger := Controle;
  UnDmkDAC_PF.AbreQuery(DmPediVda.QrSumCuz, Dmod.MyDB);
  //
  DmPediVda.QrCustomiz.Close;
  DmPediVda.QrCustomiz.Params[0].AsInteger := Controle;
  UnDmkDAC_PF.AbreQuery(DmPediVda.QrCustomiz, Dmod.MyDB);
  //
  Nome := '';
  while not DmPediVda.QrCustomiz.Eof do
  begin
    Nome := Nome + ' ' + DmPediVda.QrCustomizSiglaCustm.Value;
    DmPediVda.QrCustomiz.Next;
  end;
  Nome := Copy(Trim(Nome), 1, 255);
  if Nome <> '' then
  begin
    DmPediVda.QrPesInfCuz.Close;
    DmPediVda.QrPesInfCuz.Params[0].AsString := Nome;
    UnDmkDAC_PF.AbreQuery(DmPediVda.QrPesInfCuz, Dmod.MyDB);
    if DmPediVda.QrPesInfCuz.RecordCount > 0 then
      InfAdCuztm := DmPediVda.QrPesInfCuzCodigo.Value
    else begin
      InfAdCuztm := DModG.BuscaProximoCodigoInt('nfectrl', 'nfeinfcuz', '', 0);
      UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'nfeinfcuz', False, [
      'Nome'], ['Codigo'], [Nome], [InfAdCuztm], True);
    end;
  end;
  {
  ValLiq := QrSumCuzValLiq.Value;
  DescoV := QrSumCuzDescoV.Value;
  ValBru := ValLiq + DescoV;
  //
  }
  QuantP := DmPediVda.QrItsQuantP.Value;
  ValLiq := DmPediVda.QrSumCuzValLiq.Value * QuantP;
  DescoV := DmPediVda.QrSumCuzDescoV.Value * QuantP;
  ValBru := ValLiq + DescoV;
  if DmPediVda.QrItsQuantP.Value = 0 then
    PrecoO := 0
  else
    PrecoO := Round(ValBru / QuantP * 100) / 100;
  PrecoR := PrecoO;
  //
  if ValBru = 0 then
    DescoP := 0
  else
    DescoP := DescoV / ValBru * 100;
  //
  if QuantP > 0 then
    PrecoF := ValLiq / QuantP// * ((100 - DescoP)/100)
  else
    PrecoF := 0;
  PercCustom :=   DmPediVda.QrSumCuzPercCustom.Value;
  if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'pedivdaits', False, [
    'PrecoO', 'PrecoR', 'ValBru', 'DescoP', 'DescoV',
    'ValLiq', 'PrecoF', 'InfAdCuztm', 'PercCustom'
  ], ['Controle'], [
    PrecoO, PrecoR, ValBru, DescoP, DescoV,
    ValLiq, PrecoF, InfAdCuztm, PercCustom
  ], [Controle], True) then
  begin
    Codigo := QrPediVdaCodigo.Value;
    DmPediVda.AtzSdosPedido(Codigo);
    LocCod(Codigo, Codigo);
    QrPediVdaGru.Locate('Nivel1', DmPediVda.QrItsGraGru1.Value, []);
    if QrCustomizados.State <> dsInactive then
      QrCustomizados.Locate('Controle', Controle, []);
  end;
end;

procedure TFmPediVda2.AtualizaValoresdoitem1Click(Sender: TObject);
begin
  AtualizaItemCustomizado(QrCustomizadosControle.Value);
end;

procedure TFmPediVda2.BtPedidosClick(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 0;
  MyObjects.MostraPopUpDeBotao(PMPedidos, BtPedidos);
end;

procedure TFmPediVda2.BtRecalculaClick(Sender: TObject);
begin
  Screen.Cursor := crHourGlass;
  try
    DmPediVda.AtualizaTodosItensPediVda_(QrPediVdaCodigo.Value);
    DmPediVda.AtzSdosPedido(QrPediVdaCodigo.Value);
    DmPediVda.AtualizaTodosItensPediVda_(QrPediVdaCodigo.Value);
    LocCod(QrPediVdaCodigo.Value, QrPediVdaCodigo.Value);
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmPediVda2.BtConfirmaClick(Sender: TObject);
  {
  function FaturaParcialIncorreta(var Texto: String): Boolean;
  var
    Valor: Double;
  begin
    if CkAFP_Sit.Checked then
      Valor := EdAFP_Per.ValueVariant
    else
      Valor := 0;
    //
    Result := int(Valor * 10000) <> int(DmPediVda.QrPediPrzCabPercent2.Value * 10000);
    if Result then Texto := FloatToStr(Valor) + ' � diferente de ' +
      FloatToStr(DmPediVda.QrPediPrzCabPercent2.Value)
  end;
  }
var
  Codigo, RegrFiscal, indPres, finNFe: Integer;
  Mensagem: String;
  FretePor, modFrete, SoliComprCab, Lin, Col: Integer;
  SQLType: TSQLType;
begin
  SQLType := ImgTipo.SQLType;
  //
  if MyObjects.FIC(EdCodUsu.ValueVariant = 0, EdCodUsu,
    'Informe o n�mero do pedido!') then Exit;
  if MyObjects.FIC(EdEmpresa.ValueVariant = 0, EdEmpresa,
    'Informe a empresa!') then Exit;
  if MyObjects.FIC(EdCliente.ValueVariant = 0, EdCliente,
    'Informe o destinat�rio!') then Exit;
  if MyObjects.FIC(TPDtaPrevi.Date < 2, TPDtaPrevi,
    'Informe a data de previs�o de entrega!') then Exit;
  //if MyObjects.FIC(EdTabelaPrc.ValueVariant = 0, EdTabelaPrc,
    //'Informe a tabela de pre�os!') then Exit;
  if MyObjects.FIC(EdCondicaoPG.ValueVariant = 0, EdCondicaoPG,
    'Informe a condi��o de pagamento!') then Exit;
  if MyObjects.FIC(EdCartEmis.ValueVariant = 0, EdCartEmis,
    'Informe a carteira!') then Exit;
  if MyObjects.FIC(EdRegrFiscal.ValueVariant = 0, EdRegrFiscal,
    'Informe a movimenta��o (fiscal)!') then Exit;
  if MyObjects.FIC(VuEmpresa.ValueVariant = EdCliente.ValueVariant, EdCliente,
    'O Destinat�rio e a Empresa n�o podem ser iguais!') then Exit;
  //ErroFatura := FaturaParcialIncorreta(Txt);
  //if MyObjects.FIC(ErroFatura, CkAFP_Sit,
    //'Fatura parcial n�o permitida!' + sLineBreak + Txt) then Exit;
  Codigo := UMyMod.BuscaEmLivreY_Def('PediVda', 'Codigo', SQLType,
    QrPediVdaCodigo.Value);
  // ini 2022-04-02
(*
  if MyObjects.FIC((EdFretePor.ValueVariant > 1) and (DModG.QrPrmsEmpNFeversao.Value < 2),
    EdFretePor, 'Tipo de frete inv�lido na vers�o atual da NFe! Selecione 0 ou 1') then Exit;
*)
  if MyObjects.FIC(RGMadeBy.ItemIndex = 0, RGMadeBy,
    'Informe o modo de fornecimento dos produtos!') then Exit;
  DModG.DefineFretePor(EdFretePor.ValueVariant, FretePor, modFrete);
  // fim 2022-04-02
  //
  if CkEntregaUsa.Checked = False then
  begin
    EdEntregaEnti.ValueVariant := 0;
    CBEntregaEnti.KeyValue := 0;
  end;
  if CkRetiradaUsa.Checked = False then
  begin
    EdRetiradaEnti.ValueVariant := 0;
    CBRetiradaEnti.KeyValue := 0;
  end;
  //Valida regra fiscal com os dados do cliente
  if not UMyMod.ObtemCodigoDeCodUsu(EdRegrFiscal, RegrFiscal,
    'Informe a regra fiscal!', 'Codigo', 'CodUsu') then Exit;
  //
  if DmPediVda.QrParamsEmpPediVdaNElertas.Value = 0 then //Para quem usa o pedido mas n�o usa a nota
  begin
    if MyObjects.FIC(DmPediVda.QrClientesDOCENT.Value = '', EdCliente,
      'O Destinat�rio n�o possui CNPJ / CPF cadastrado!') then
    begin
      if Geral.MB_Pergunta('Deseja editar agora?' + sLineBreak +
        'AVISO: Verifique se os demais campos est�o devidamente preenchidos!') = ID_YES then
      begin
        MostraFormEntidade2(EdCliente.ValueVariant, DmPediVda.QrClientes,
          EdCliente, CBCliente);
      end;
      Exit;
    end;
    // NFe 3.10
    if MyObjects.FIC(RG_idDest.ItemIndex <= 0, RG_idDest,
      'Informe o Local de Destino da Opera��o!') then Exit;
    // NFe 3.10
    if not Entities.ValidaIndicadorDeIEEntidade_2(DmPediVda.QrClientesIE.Value,
      DmPediVda.QrClientesindIEDest.Value, DmPediVda.QrClientesTipo.Value,
       RG_idDest.ItemIndex, RG_indFinal.ItemIndex, False, Mensagem) then
    begin
      Geral.MB_Aviso(Mensagem);
      Exit;
    end;
    if MyObjects.FIC(RG_indFinal.ItemIndex < 0, RG_indFinal,
      'Informe a opera��o com o consumidor final!') then Exit;
    {$IfNDef SemNFe_0000}
      if MyObjects.FIC(EdindPres.Text = '', EdindPres,
        'Informe o indicador de presen�a do comprador!') then Exit;
      indPres := Geral.IMV(EdindPres.Text);
      if MyObjects.FIC(not (indPres in [0,1,2,3,4,9]), EdindPres,
        'Informe o indicador de presen�a do comprador!') then Exit;

      if MyObjects.FIC(Edide_finNFe.Text = '', Edide_finNFe,
        'Informe a finalidade de emiss�o da NF-e!') then Exit;
      finNFe := Geral.IMV(Edide_finNFe.Text);
      if MyObjects.FIC(not (finNFe in [1,2,3,4]), Edide_finNFe,
        'Informe a finalidade de emiss�o da NF-e!') then Exit;
    {$EndIf}
    SoliComprCab := EdSoliComprCab.ValueVariant;
    //
    if not VerificaCarteira_x_CondPG(True) then
      Exit;


    if not DmPediVda.ValidaRegraFiscalCFOPCliente(RG_idDest.ItemIndex,
      RG_indFinal.ItemIndex, DmPediVda.QrClientesindIEDest.Value, RegrFiscal,
      DmPediVda.QrFisRegCadNome.Value, DmPediVda.QrClientesIE.Value,
      DmPediVda.QrClientesNOMEUF.Value, DmPediVda.QrParamsEmpUF_WebServ.Value,
      Mensagem) then
    begin
      Geral.MB_Info(Mensagem);
      //
      if Geral.MB_Pergunta('Deseja configurar a Regra Fiscal agora?') = ID_YES then
        Grade_Jan.MostraFormFisRegCad(RegrFiscal);
      //
      Exit;
    end;
  end;
  //
  if UMyMod.ExecSQLInsUpdPanel(SQLType, FmPediVda2, PainelEdita,
    'PediVda', Codigo, Dmod.QrUpd, [PainelEdita], [PainelDados], ImgTipo, True) then
  begin
    if SQLType = stIns then
    begin
      if SoliComprCab <> 0 then
        InsereItensDeSoliComprCab(SoliComprCab, Codigo);
      LocCod(Codigo, Codigo);
      RGEntSai.ItemIndex := 0;
      PageControl1.ActivePageIndex := 1;
      if (SoliComprCab <> 0) and (QrPediVdaCodigo.Value = Codigo) then
      begin
        Geral.MB_Info('N�o esque�a de alterar os pre�os dos itens!');
        // Parei aqui! ver o que fazer!
        QrPVG.Close;
        QrPVG.SQL.Text := QrPediVdaGru.SQL.Text;
        QrPVG.Open;
        //
        QrPVG.First;
        while not QrPVG.Eof do
        begin
          QrPediVdaGru.Locate('Nivel1', QrPVGNivel1.Value, []);
          for Lin := 1 to GradeQ.RowCount -1 do
          begin
            for Col := 1 to GradeQ.ColCount -1 do
            begin
              GradeQ.Row := Lin;
              GradeQ.Col := Col;
              //
              AlteraOuExcluiItem();
            end;
          end;
          QrPVG.Next;
        end;
      end;
    end else
    begin
      LocCod(Codigo, Codigo);
      RGEntSai.ItemIndex := 0;
      PageControl1.ActivePageIndex := 1;
    end;
  end;
end;

procedure TFmPediVda2.BtCustomClick(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 2;
  MyObjects.MostraPopUpDeBotao(PMCustom, BtCustom);
end;

procedure TFmPediVda2.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := Geral.IMV(EdCodigo.Text);
  if ImgTipo.SQLType = stIns then UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'PediVda', Codigo);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'PediVda', 'Codigo');
  MostraEdicao(0, stLok, 0);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'PediVda', 'Codigo');
end;

procedure TFmPediVda2.BtFisRegCadClick(Sender: TObject);
{$IfNDef NAO_GFAT}
var
  Codigo, FisRegCad: Integer;
begin
  Codigo    := QrPediVdaCodigo.Value;
  FisRegCad := QrPediVdaRegrFiscal.Value;
  //
  Grade_Jan.MostraFormFisRegCad(FisRegCad);
  try
    Screen.Cursor := crHourGlass;
    LocCod(Codigo, Codigo);
  finally
    Screen.Cursor := crDefault;
  end;
{$Else}
  Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappGFat);
{$EndIf}
end;

procedure TFmPediVda2.BtGraGruNClick(Sender: TObject);
{$IfNDef NAO_GFAT}
var
  Codigo, Nivel1: Integer;
begin
  Codigo := QrPediVdaCodigo.Value;
  Nivel1 := QrPediVdaGruNivel1.Value;
  //
  Grade_Jan.MostraFormGraGruN(Nivel1);
  try
    Screen.Cursor := crHourGlass;
    LocCod(Codigo, Codigo);
  finally
    Screen.Cursor := crDefault;
  end;
{$Else}
  Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappGFat);
{$EndIf}
end;

procedure TFmPediVda2.BtItensClick(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 1;
  MyObjects.MostraPopUpDeBotao(PMItens, BtItens);
end;

procedure TFmPediVda2.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLtype := stLok;
  //
  CBEmpresa.ListSource     := DModG.DsEmpresas;
  CBSituacao.ListSource    := DModG.DsSituacao;
  CBCLiente.ListSource     := DmPediVda.DsClientes;
  CBMotivoSit.ListSource   := DmPediVda.DsMotivos;
  CBTabelaPrc.ListSource   := DmPediVda.DsTabePrcCab;
  CBCondicaoPG.ListSource  := DmPediVda.DsPediPrzCab;
  CBCartEmis.ListSource    := DmPediVda.DsCartEmis;
  CBFretePor.ListSource    := DmPediVda.DsFretePor;
  CBTransporta.ListSource  := DmPediVda.DsTransportas;
  CBRedespacho.ListSource  := DmPediVda.DsRedespachos;
  CBRepresen.ListSource    := DmPediVda.DsPediAcc;
  CBRegrFiscal.ListSource  := DmPediVda.DsFisRegCad;
  CBmoeda.ListSource       := DModG.DsCambioMda;
  //
  DBEdDOCENT.DataSource    := DmPediVda.DsClientes;
  DBEdCidade.DataSource    := DmPediVda.DsClientes;
  DBEdNOMEUF.DataSource    := DmPediVda.DsClientes;
  //
  QrPediVdaGruValLiq.DisplayFormat   := Dmod.FStrFmtPrc;
  QrPediVdaGruValLiq.DisplayFormat   := Dmod.FStrFmtPrc;
  QrCustomizadosValLiq.DisplayFormat := Dmod.FStrFmtPrc;
  QrPediVdaCuzPrecoR.DisplayFormat   := Dmod.FStrFmtPrc;
  QrPediVdaCuzValBru.DisplayFormat   := Dmod.FStrFmtPrc;
  QrPediVdaCuzValLiq.DisplayFormat   := Dmod.FStrFmtPrc;
  //
  QrPediVda.Database      := Dmod.MyDB;
  QrPediVdaGru.Database   := Dmod.MyDB;
  QrPediVdaIts.Database   := Dmod.MyDB;
  QrCustomizados.Database := Dmod.MyDB;
  QrPediVdaCuz.Database   := Dmod.MyDB;
  QrCli.Database          := Dmod.MyDB;
  QrItsN.Database         := Dmod.MyDB;
  QrItsC.Database         := Dmod.MyDB;
  QrItsZ.Database         := Dmod.MyDB;
  //
  QrPediVdaNOMESITUACAO.LookupDataSet := DModG.QrPediSit;
  DBEdCidade.DataField := '';
  DBEdNOMEUF.DataField := '';
  DBEdREF_NIV1.DataField := '';
  //
  PageControl1.ActivePageIndex := 0;
  PcDadosTop.ActivePageIndex := 0;
  PageControl3.ActivePageIndex := 0;
  PageControl1.Align := alClient;
  PcDadosTop.Align := alClient;
  //
  RGIndSinc.ItemIndex := 1;
  RGIndSinc.Enabled   := False;
  //
  TPDtaEmiss.Date := Date;
  TPDtaEntra.Date := Date;
  TPDtaInclu.Date := Date;
  TPDtaPrevi.Date := Date;
  CriaOForm;
  GradeA.ColWidths[0] := 128;
  GradeC.ColWidths[0] := 128;
  GradeX.ColWidths[0] := 128;
  GradeQ.ColWidths[0] := 128;
  GradeF.ColWidths[0] := 128;
  GradeD.ColWidths[0] := 128;
  GradeV.ColWidths[0] := 128;
{$IfNDef SemNFe_0000}
  FindPres := UnNFe_PF.ListaIndicadorDePresencaComprador();
  F_finNFe := UnNFe_PF.ListaFinalidadeDeEmiss�oDaNFe(True);
  F_IND_PGTO_EFD := UnNFe_PF.ListaIND_PAG_EFD();
{$Else}
  FindPres := 0;
  F_finNFe := 0;
{$EndIf}
  ReopenCentroCusto();
end;


procedure TFmPediVda2.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrPediVdaCodigo.Value, LaRegistro.Caption);
end;

procedure TFmPediVda2.SbEntregaEntiClick(Sender: TObject);
begin
  MostraFormEntidade2(EdEntregaEnti.ValueVariant, DmPediVda.QrEntregaEnti,
    EdEntregaEnti, CBEntregaEnti);
end;

procedure TFmPediVda2.SbImprimeClick(Sender: TObject);
begin
  MyObjects.MostraPopupDeBotao(PMImprime, SbImprime);
end;

procedure TFmPediVda2.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmPediVda2.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrPediVdaCodUsu.Value, LaRegistro.Caption);
end;

procedure TFmPediVda2.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmPediVda2.QrPediVdaAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
  BtItens.Enabled := QrPediVda.RecordCount > 0;
  BtCustom.Enabled := QrPediVda.RecordCount > 0;
end;

procedure TFmPediVda2.QrPediVdaAfterScroll(DataSet: TDataSet);
(*
var
  Linha1, Linha2, Linha3: String;
  Entidade: Integer;
*)
begin
  if QrPediVdaEmpresa.Value <> 0 then
    FTabLctA := DmodG.NomeTab(TMeuDB, ntLct, False, ttA, QrPediVdaFilial.Value)
  else
    FTabLctA := sTabLctErr;
  //
  ReopenSumIts();
  ReopenPediVdaGru(0);
  ReopenEmissF(0);
{ ini 2022-04-03
  case QrPediVdaEntSai.Value of
    0(*ent*): DmPediVda.ReopenParamsEmp(QrPediVdaCliente.Value, True);
    1(*sai*): DmPediVda.ReopenParamsEmp(QrPediVdaEmpresa.Value, True);
  end;
}
  DmPediVda.ReopenParamsEmp(QrPediVdaEmpresa.Value, True);
  // fim 2022-04-03
  //
  MostraEnderecoDeEntrega2();
end;

procedure TFmPediVda2.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmPediVda2.SbQueryClick(Sender: TObject);
begin
  LocCod(QrPediVdaCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'PediVda', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmPediVda2.SbRegrFiscalClick(Sender: TObject);
begin
  MyObjects.MostraPopupDeBotao(PMRegrFiscal, SbRegrFiscal);
end;

procedure TFmPediVda2.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmPediVda2.FormShow(Sender: TObject);
begin
  //FCodUsuTxt_Old := EdCodUsu.Text;
end;

procedure TFmPediVda2.frxPED_VENDA_001_01GetValue(const VarName: string;
  var Value: Variant);
begin
  if VarName = 'MeuLogo3x1Existe' then
    Value := FileExists(DModG.QrParamsEmpLogo3x1.Value)
  else
  if VarName = 'MeuLogo3x1Caminho' then
    Value := DModG.QrParamsEmpLogo3x1.Value
  else
  if VarName = 'VARF_TIPOMOV_TIPOENTI' then
  begin
    if QrPediVdaEntSai.Value = 1 then
      Value := 'Cliente:'
    else
      Value := 'Fornecedor:'
  end else
  if VarName = 'VARF_TIPOMOV_NUMPED' then
  begin
    if QrPediVdaEntSai.Value = 1 then
      Value := 'N� DO PEDIDO DO CLIENTE: ' + QrPediVdaPedidoCli.Value
    else
      Value := 'N� DO OR�AMENTO DO FORNECEDOR: ' + QrPediVdaPedidoCli.Value
  end
  else
  if VarName = 'VARF_TIPOMOV_NOREPRESENT' then
  begin
    if QrPediVdaNOMEACC.Value <> EmptyStr then
    begin
      Value := 'Representante:' + QrPediVdaNOMEACC.Value
    end else
      Value := '';
  end
  else
end;

procedure TFmPediVda2.GradeADrawCell(Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);
begin
  MyObjects.DesenhaGradeA(GradeA, ACol, ARow, Rect, State, True);
end;

procedure TFmPediVda2.GradeCDrawCell(Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);
begin
  MyObjects.DesenhaGradeN(GradeC, GradeA, nil, ACol, ARow, Rect, State,
  '0', 0, 0, True);
end;

procedure TFmPediVda2.GradeDDrawCell(Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);
begin
  MyObjects.DesenhaGradeN(GradeD, GradeA, nil, ACol, ARow, Rect, State,
  Dmod.FStrFmtPrc, 0, 0, False);
end;

procedure TFmPediVda2.GradeFDrawCell(Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);
begin
  MyObjects.DesenhaGradeN(GradeF, GradeA, nil, ACol, ARow, Rect, State,
  Dmod.FStrFmtPrc, 0, 0, False);
end;

procedure TFmPediVda2.GradeQDblClick(Sender: TObject);
begin
  GradeQDblClick2();
end;

procedure TFmPediVda2.GradeQDblClick2;
var
  GraGruX: Integer;
  SQLType: TSQLType;
begin
  // somente item n�o customizado
  if QrPediVdaGruItensCustomizados.Value = 0 then
  begin
    if (GradeQ.Col > 0) and (GradeQ.Row > 0) then
    begin
      GraGruX := Geral.IMV(GradeC.Cells[GradeQ.Col, GradeQ.Row]);
      if (QrPediVdaGru.RecordCount > 0) and (GraGruX > 0)
      //and (GradeQ.Col > 0) and (GradeQ.Row > 0)
      then
      begin
        DmPediVda.QrItemPVI.Close;
        DmPediVda.QrItemPVI.Params[00].AsInteger := QrPediVdaCodigo.Value;
        DmPediVda.QrItemPVI.Params[01].AsInteger := GraGruX;
        UnDmkDAC_PF.AbreQuery(DmPediVda.QrItemPVI, Dmod.MyDB);
        //
  // ini 2022-04-03
        {if DmPediVda.QrItemPVI.RecordCount > 0 then
        begin
          UMyMod.FormInsUpd_Show(TFmPediVdaAlt2, FmPediVdaAlt2, afmoNegarComAviso,
          DmPediVda.QrItemPVI, stUpd);
        end else begin
          UMyMod.FormInsUpd_Show(TFmPediVdaAlt2, FmPediVdaAlt2, afmoNegarComAviso,
          DmPediVda.QrItemPVI, stIns);
        end;}
        if DmPediVda.QrItemPVI.RecordCount > 0 then
          SQLType := stUpd
        else
          SQLType := stIns;
        UMyMod.FormInsUpd_Show(TFmPediVdaAlt2_PediCompra, FmPediVdaAlt2_PediCompra,
          afmoNegarComAviso, DmPediVda.QrItemPVI, SQLType);
  // fim 2022-04-03
      end else
        Geral.MB_Aviso('N�o h� item a ser editado/exclu�do!');
    end else
      Geral.MensagemBox(
        'Antes clicar para editar/excluir, selecione a c�lula do item a ser editado/exclu�do!' +
        sLineBreak + 'Uma forma direta de editar/excluir � dar um duplo clique direto na c�lula.',
        'Aviso', MB_OK+MB_ICONWARNING);
  end else
    Geral.MensagemBox('Este item � customizado! ' +
      'Para edit�-lo, clique no bot�o pr�prio!', 'Aviso', MB_OK+MB_ICONWARNING);
end;

procedure TFmPediVda2.GradeQDrawCell(Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);
begin
  MyObjects.DesenhaGradeN(GradeQ, GradeA, nil, ACol, ARow, Rect, State,
  dmkPF.FormataCasas(QrPediVdaGruFracio.Value), 0, 0, False);
end;

procedure TFmPediVda2.GradeVDrawCell(Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);
begin
  MyObjects.DesenhaGradeN(GradeV, GradeA, nil, ACol, ARow, Rect, State,
  Dmod.FStrFmtPrc, 0, 0, true);
end;

procedure TFmPediVda2.GradeXDrawCell(Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);
begin
  MyObjects.DesenhaGradeN(GradeX, GradeA, nil, ACol, ARow, Rect, State,
  '0', 0, 0, True);
end;

procedure TFmPediVda2.Imprime(Gradeado: Boolean);
var
  Titulo: String;
begin
  if QrCli.State = dsInactive then
  begin
    QrCli.Close;
    QrCli.Params[0].AsInteger := QrPediVdaCliente.Value;
    UMyMod.AbreQuery(QrCli, Dmod.MyDB, 'TFmPediVda2.SbImprimeClick()');
  end;
  DModG.ReopenParamsEmp(QrPediVdaEmpresa.Value);
  DModG.ReopenEndereco(QrPediVdaEmpresa.Value);
  //
  QrItsN.Close;
  QrItsN.Params[0].AsInteger := QrPediVdaCodigo.Value;
  UnDmkDAC_PF.AbreQuery(QrItsN, Dmod.MyDB);
  //
  QrItsC.Close;
  QrItsC.Params[0].AsInteger := QrPediVdaCodigo.Value;
  UnDmkDAC_PF.AbreQuery(QrItsC, Dmod.MyDB);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrUsuario, Dmod.MyDB, [
  'SELECT pvd.UserCad, pvd.UserAlt,',
  'IF(se1.Funcionario<>0, IF(en1.Tipo=0, en1.RazaoSocial, en1.Nome), ',
  '  se1.Login) NO_LANCADOR,',
  'IF(se2.Funcionario<>0, IF(en2.Tipo=0, en2.RazaoSocial, en2.Nome), ',
  '  se2.Login) NO_ALTERADOR',
  'FROM pedivda pvd',
  'LEFT JOIN senhas se1 ON se1.Numero=pvd.UserCad',
  'LEFT JOIN entidades en1 ON en1.Codigo=se1.Funcionario',
  'LEFT JOIN senhas se2 ON se2.Numero=pvd.UserAlt',
  'LEFT JOIN entidades en2 ON en2.Codigo=se2.Funcionario',
  'WHERE pvd.Codigo=' + Geral.FF0(QrPediVdaCodigo.Value),
  '']);
  //
  Titulo := 'Informe do Pedido n� ' + FormatFloat('000000', QrPediVdaCodUsu.Value);
  if Gradeado then
    MyObjects.frxMostra(frxPED_VENDA_001_01, Titulo)
  else
    MyObjects.frxMostra(frxPED_VENDA_001_02, Titulo);

end;

procedure TFmPediVda2.IncluiLctoF(Sender: TObject);
const
  Qtde = 0.000;
  Qtd2 = 0.000;
var
  Terceiro, Cod, Genero, Empresa, NF, CentroCusto: Integer;
  SerieNF: String;
  Valor: Double;
  GenCtbD, GenCtbC, CartEmis, FatParcRef_PgtoAdiantado: Integer;
  TypCtbCadMoF: TTypCtbCadMoF;
  Descricao: String;
  Reconfig: Boolean;
begin
  PageControl1.ActivePageIndex := 0;
  if UFinanceiro.TabLctNaoDef(FTabLctA) then
    Exit;
  //
  DefineVarDup;
  //
  Cod      := 0; // QrPQECodigo.Value;  ver depois no PQE!
  Terceiro := QrPediVdaCliente.Value;
  Valor    := CalculaDiferencas();
  Empresa  := DModG.QrEmpresasCodigo.Value;
  SerieNF  := ''; //Geral.FF0(QrPQESerie.Value);
  NF       := 0; //QrPQENF.Value;
  //
  if Valor = 0 then
    Valor := QrPediVdaValLiq.Value;
  //
  //buscar da regra!
  ObtemContasContabeis(GenCtbD, GenCtbC);
  // ini 2022-06-11
  //Genero   := Dmod.QrControleCtaPQCompr.Value;
  Genero := GenCtbD;
  // fim 2022-06-11
(*
  if DModG.QrCtrlGeralUsarCtbEstqParaUC.Value = 1 then
    TypCtbCadMoF := TTypCtbCadMoF.tccmfEstoqueIn
  else
    TypCtbCadMoF := TTypCtbCadMoF.tccmfUsoConsumoIn;
  //
  if QrPQEIts.RecordCount = 1 then
    PQ_PF.DefineContasContabeisEntradaInsumosPQE_UmItem(TypCtbCadMoF, Cod, GenCtbD, GenCtbC, Descricao)
  else
    PQ_PF.DefineContasContabeisEntradaInsumosPQE_VariosItens(TypCtbCadMoF, Cod, GenCtbD, GenCtbC, Descricao);
*)
  //
  CartEmis := QrPediVdaCartEmis.Value;
  CentroCusto := QrPediVdaCentroCusto.Value;
  FatParcRef_PgtoAdiantado := QrPediVdaCodigo.Value;
  //
  Reconfig := True;  // Setar carteira
  //
  UPagtos.Pagto(QrEmissF, tpDeb, Cod, Terceiro, VAR_FATID_1001, GenCtbD, GenCtbC, stIns,
  'Pedido de uso e consumo', Valor, VAR_USUARIO, (*FatID_Sub*)0, Empresa, mmNenhum,
  (*ValMin, ValMax*)0, 0, (*Show*)True, Reconfig, CartEmis, (*Parcelas*)0, (*Periodic*)0,
  (*DiasParc*)0, (*NotaFiscal*)NF, FTabLctA, Descricao, SerieNF, Qtde,
  Qtd2, GenCtbD, GenCtbC, CentroCusto, FatParcRef_PgtoAdiantado);
  (*
  UPagtos.Pagto(QrEmissF, tpDeb, Cod, Terceiro, VAR_FATID_1001, GenCtbD, GenCtbC, stIns,
    'Compra de uso e consumo', Valor, VAR_USUARIO, 0, Empresa, mmNenhum, 0,
    0, True, False, 0, 0, 0, 0, NF, FTabLctA, Descricao, SerieNF, Qtde, Qtd2, GenCtbD, GenCtbC);
  *)
  ReopenEmissF(0);
end;

procedure TFmPediVda2.IncluinovoitemprodutoCustomizvel1Click(Sender: TObject);
begin
  if DmProd.ImpedePeloPrazoMedio(QrPediVdaTabelaPrc.Value,
    QrPediVdaMedDDSimpl.Value, QrPediVdaMedDDReal.Value,
    DmPediVda.QrParamsEmpTipMediaDD.Value) then Exit;
  if DBCheck.CriaFm(TFmPediVdaCuzIns2, FmPediVdaCuzIns2, afmoNegarComAviso) then
  begin
    FmPediVdaCuzIns2.ShowModal;
    FmPediVdaCuzIns2.Destroy;
  end;
end;

procedure TFmPediVda2.IncluiNovoPedido(Solicitacao: Integer; SoliTXT: String;
  CentroCusto: Integer);
var
  Agora: TDateTime;
begin
  if not DModG.SoUmaEmpresaLogada(True) then Exit;
  //
  UMyMod.ConfigPanelInsUpd(stIns, Self, PainelEdita, QrPediVda, [PainelDados],
    [PainelEdita], RGEntSai, ImgTipo, 'pedivda');
  //
  DmPediVda.ReopenParamsEmp(DmodG.QrFiliLogFilial.Value, True);
  //
  RGIndSinc.ItemIndex     := 1;
  RGIndSinc.Enabled       := False;
  EdSituacao.ValueVariant := DmPediVda.QrParamsEmpSituacao.Value;//2; // Liberado
  CBSituacao.KeyValue     := DmPediVda.QrParamsEmpSituacao.Value; // Liberado
  EdEmpresa.ValueVariant  := DmodG.QrFiliLogFilial.Value;
  CBEmpresa.KeyValue      := DmodG.QrFiliLogFilial.Value;
  //
  DesabilitaComponentes();
  //
  TPDtaEmiss.Date           := 0;
  TPDtaEntra.Date           := 0;
  TPDtaInclu.Date           := Date;
  TPDtaPrevi.Date           := 0;
  EdindPres.ValueVariant    := 9;
  Edide_finNFe.ValueVariant := 0;
  //
  if (*Grl_Vars*)VAR_INFO_DATA_HOJE then
  begin
    Agora := DmodG.ObtemAgora();
    //
    TPDtaEmiss.Date := Agora;
    TPDtaEntra.Date := Agora;
    TPDtaPrevi.Date := Agora;
  end;
  //
  //EdCodUsu.SetFocus; 2022-04-02

  // Pedido sem solicita��o - Fazer aqui para n�o ser zerado!
  EdSoliComprCab.ValueVariant  := Solicitacao;
  EdSoliCompr_TXT.ValueVariant := SoliTxt;
  EdCentroCusto.ValueVariant   := CentroCusto;
  CBCentroCusto.KeyValue       := CentroCusto;
  //
  RGEntSai.ItemIndex := 0;
  //
end;

procedure TFmPediVda2.Incluinovopedido1Click(Sender: TObject);
begin
  IncluiNovoPedido(0, '', 0);
end;

procedure TFmPediVda2.Incluinovositensdegrupo1Click(Sender: TObject);
begin
  MostraPediVdaGru(stIns);
  DmPediVda.AtualizaTodosItensSoliCompr_(QrPediVdaSoliComprCab.Value);
end;

procedure TFmPediVda2.IncluinovositensporLeitura1Click(Sender: TObject);
begin
  MostraPediVdaLei(stIns);
  DmPediVda.AtualizaTodosItensSoliCompr_(QrPediVdaSoliComprCab.Value);
end;

procedure TFmPediVda2.Incluipedidodeumasolicitao1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmSoliComprSel, FmSoliComprSel, afmoSoBoss) then
  begin
    FmSoliComprSel.ShowModal;
    FmSoliComprSel.Destroy;
  end;
end;

procedure TFmPediVda2.InsereItensDeSoliComprCab(SoliComprCab, PediVda: Integer);
  procedure InsereItemAtual();
  var
    Referencia: String;
    Codigo, Controle, GraGruX, Customizad, InfAdCuztm, Ordem, Multi, SoliPart:
    Integer;
    PrecoO, PrecoR, QuantP, QuantC, QuantV, ValBru, DescoP, DescoV, ValLiq,
    PrecoF, MedidaC, MedidaL, MedidaA, MedidaE, PercCustom, vProd, vFrete,
    vSeg, vOutro, vDesc, vBC: Double;
    SQLType: TSQLType;
  begin
    SQLType        := stIns;
    Codigo         := PediVda;
    Controle       := 0;
    GraGruX        := QrSoliComprItsGraGrux.Value;
    PrecoO         := QrSoliComprItsPrecoO.Value;
    PrecoR         := QrSoliComprItsPrecoR.Value;
    QuantP         := QrSoliComprItsQuantS.Value;  // S para P (Solicitado para pedido)!
    QuantC         := QrSoliComprItsQuantC.Value;
    QuantV         := QrSoliComprItsQuantV.Value;
    ValBru         := QrSoliComprItsValBru.Value;
    DescoP         := QrSoliComprItsDescoP.Value;
    DescoV         := QrSoliComprItsDescoV.Value;
    ValLiq         := QrSoliComprItsValLiq.Value;
    PrecoF         := QrSoliComprItsPrecoF.Value;
    MedidaC        := QrSoliComprItsMedidaC.Value;
    MedidaL        := QrSoliComprItsMedidaL.Value;
    MedidaA        := QrSoliComprItsMedidaA.Value;
    MedidaE        := QrSoliComprItsMedidaE.Value;
    PercCustom     := QrSoliComprItsPercCustom.Value;
    Customizad     := QrSoliComprItsCustomizad.Value;
    InfAdCuztm     := QrSoliComprItsInfAdCuztm.Value;
    Ordem          := QrSoliComprItsOrdem.Value;
    Referencia     := QrSoliComprItsReferencia.Value;
    Multi          := QrSoliComprItsMulti.Value;
    vProd          := QrSoliComprItsvProd.Value;
    vFrete         := QrSoliComprItsvFrete.Value;
    vSeg           := QrSoliComprItsvSeg.Value;
    vOutro         := QrSoliComprItsvOutro.Value;
    vDesc          := QrSoliComprItsvDesc .Value;
    vBC            := QrSoliComprItsvBC.Value;
    SoliPart       := QrSoliComprItsControle.Value;
    //
    Controle := UMyMod.BuscaEmLivreY_Def('pedivdaits', 'Controle', SQLType, 0);
    //if
    UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'pedivdaits', False, [
    'Codigo', 'GraGruX', 'PrecoO',
    'PrecoR', 'QuantP', 'QuantC',
    'QuantV', 'ValBru', 'DescoP',
    'DescoV', 'ValLiq', 'PrecoF',
    'MedidaC', 'MedidaL', 'MedidaA',
    'MedidaE', 'PercCustom', 'Customizad',
    'InfAdCuztm', 'Ordem', 'Referencia',
    'Multi', 'vProd', 'vFrete',
    'vSeg', 'vOutro', 'vDesc',
    'vBC', 'SoliPart'], [
    'Controle'], [
    Codigo, GraGruX, PrecoO,
    PrecoR, QuantP, QuantC,
    QuantV, ValBru, DescoP,
    DescoV, ValLiq, PrecoF,
    MedidaC, MedidaL, MedidaA,
    MedidaE, PercCustom, Customizad,
    InfAdCuztm, Ordem, Referencia,
    Multi, vProd, vFrete,
    vSeg, vOutro, vDesc,
    vBC, SoliPart], [
    Controle], True);
  end;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrSoliComprIts, Dmod.MyDB, [
  'SELECT * ',
  'FROM solicomprits',
  'WHERE Codigo=' + Geral.FF0(SoliComprCab),
  '']);
  //
  QrSoliComprIts.First;
  while not QrSoliComprIts.Eof do
  begin
    InsereItemAtual();
    //
    QrSoliComprIts.Next;
  end;
end;

procedure TFmPediVda2.QrPediVdaBeforeClose(DataSet: TDataSet);
begin
  BtItens.Enabled := False;
  BtCustom.Enabled := False;
  QrPediVdaGru.Close;
  QrSumIts.Close;
  QrEmissF.Close;
  MeEnderecoEntrega2.Text := '';
end;

procedure TFmPediVda2.QrPediVdaBeforeOpen(DataSet: TDataSet);
begin
  QrPediVdaCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmPediVda2.QrPediVdaCalcFields(DataSet: TDataSet);
begin
  QrPediVdaNOMEFRETEPOR.Value := dmkPF.FretePor_Txt(QrPediVdaFretePor.Value);
  if QrPediVdaEntSai.Value = 1 then
  begin
    QrPediVdaSHOW_COD_FILIAL.Value := QrPediVdaFilial.Value;
    QrPediVdaSHOW_COD_CLIFOR.Value := QrPediVdaCODUSU_CLI.Value;
    QrPediVdaSHOW_TXT_FILIAL.Value := QrPediVdaNOMEEMP.Value;
    QrPediVdaSHOW_TXT_CLIFOR.Value := QrPediVdaNOMECLI.Value;
    QrPediVdaENTSAI_TXT.Value := 'Venda';
    EdDbSAIENT_TXT.Font.Color := clBlue;
  end else begin
    //emp.CodUsu CODUSU_FOR, cli.Filial FILIAL_CLI
    QrPediVdaSHOW_COD_FILIAL.Value := QrPediVdaFilial_CLI.Value;
    QrPediVdaSHOW_COD_CLIFOR.Value := QrPediVdaCODUSU_FOR.Value;
    QrPediVdaSHOW_TXT_FILIAL.Value := QrPediVdaNOMECLI.Value;
    QrPediVdaSHOW_TXT_CLIFOR.Value := QrPediVdaNOMEEMP.Value;          
    QrPediVdaENTSAI_TXT.Value := 'Compra';
    EdDbSAIENT_TXT.Font.Color := clRed;
  end;
end;

procedure TFmPediVda2.QrPediVdaCuzCalcFields(DataSet: TDataSet);
begin
  if QrPediVdaCuzTipDimens.Value < 0 then
    QrPediVdaCuzMedidaC_TXT.Value := ''
  else
    QrPediVdaCuzMedidaC_TXT.Value := Geral.FFT(QrPediVdaCuzMedidaC.Value, 2, siPositivo);

  //

  if QrPediVdaCuzTipDimens.Value < 1 then
    QrPediVdaCuzMedidaL_TXT.Value := ''
  else
    QrPediVdaCuzMedidaL_TXT.Value := Geral.FFT(QrPediVdaCuzMedidaL.Value, 2, siPositivo);

  //

  if QrPediVdaCuzTipDimens.Value < 2 then
    QrPediVdaCuzMedidaA_TXT.Value := ''
  else
    QrPediVdaCuzMedidaA_TXT.Value := Geral.FFT(QrPediVdaCuzMedidaA.Value, 2, siPositivo);

  //

  if QrPediVdaCuzTipDimens.Value < 3 then
    QrPediVdaCuzMedidaE_TXT.Value := ''
  else
    QrPediVdaCuzMedidaE_TXT.Value := Geral.FFT(QrPediVdaCuzMedidaE.Value, 2, siPositivo);

  //

end;

procedure TFmPediVda2.QrCliCalcFields(DataSet: TDataSet);
begin
  QrCliTE1_TXT.Value :=
    Geral.FormataTelefone_TT_Curto(QrCliTe1.Value);
  QrCliFAX_TXT.Value :=
    Geral.FormataTelefone_TT_Curto(QrCliFax.Value);
  QrCliCNPJ_TXT.Value :=
    Geral.FormataCNPJ_TT(QrCliCNPJ_CPF.Value);
  QrCliIE_TXT.Value :=
    Geral.Formata_IE(QrCliIE_RG.Value, QrCliUF.Value, '??', QrCliTipo.Value);
  QrCliNUMERO_TXT.Value :=
    Geral.FormataNumeroDeRua(QrCliRua.Value, QrCliNumero.Value, False);
  //
  QrCliE_ALL.Value := Uppercase(QrCliNOMELOGRAD.Value);
  if Trim(QrCliE_ALL.Value) <> '' then QrCliE_ALL.Value :=
    QrCliE_ALL.Value + ' ';
  QrCliE_ALL.Value := QrCliE_ALL.Value + Uppercase(QrCliRua.Value);
  if Trim(QrCliRua.Value) <> '' then QrCliE_ALL.Value :=
    QrCliE_ALL.Value + ', ' + QrCliNUMERO_TXT.Value;
  if Trim(QrCliCompl.Value) <>  '' then QrCliE_ALL.Value :=
    QrCliE_ALL.Value + ' ' + Uppercase(QrCliCompl.Value);
  if Trim(QrCliBairro.Value) <>  '' then QrCliE_ALL.Value :=
    QrCliE_ALL.Value + ' - ' + Uppercase(QrCliBairro.Value);
  if QrCliCEP.Value > 0 then QrCliE_ALL.Value :=
    QrCliE_ALL.Value + ' CEP ' +Geral.FormataCEP_NT(QrCliCEP.Value);
  if Trim(QrCliCidade.Value) <>  '' then QrCliE_ALL.Value :=
    QrCliE_ALL.Value + ' - ' + Uppercase(QrCliCidade.Value);
  if Trim(QrCliNOMEUF.Value) <>  '' then QrCliE_ALL.Value :=
    QrCliE_ALL.Value + ', ' + QrCliNOMEUF.Value;
  if Trim(QrCliPais.Value) <>  '' then QrCliE_ALL.Value :=
    QrCliE_ALL.Value + ' - ' + QrCliPais.Value;
  //
  QrCliCEP_TXT.Value :=Geral.FormataCEP_NT(QrCliCEP.Value);
  //
end;

procedure TFmPediVda2.QrCustomizadosAfterScroll(DataSet: TDataSet);
begin
  ReopenPediVdaCuz(0);
end;

procedure TFmPediVda2.QrCustomizadosBeforeClose(DataSet: TDataSet);
begin
  QrPediVdaCuz.Close;
end;

procedure TFmPediVda2.QrEntregaCliCalcFields(DataSet: TDataSet);
begin
  QrEntregaCliTE1_TXT.Value :=
    Geral.FormataTelefone_TT_Curto(QrEntregaCliTe1.Value);
(*
  QrEntregaCliFAX_TXT.Value :=
    Geral.FormataTelefone_TT_Curto(QrEntregaCliFax.Value);
*)
  {
  QrEntregaCliCNPJ_TXT.Value :=
    Geral.FormataCNPJ_TT(QrEntregaCliCNPJ_CPF.Value);
  QrEntregaCliNOME_TIPO_DOC.Value :=
    dmkPF.ObtemTipoDocTxtDeCPFCNPJ(QrEntregaCliCNPJ_CPF.Value) + ':';
  }
  QrEntregaCliNUMERO_TXT.Value :=
    Geral.FormataNumeroDeRua(QrEntregaCliRua.Value, QrEntregaCliNumero.Value, False);
  //
/////////////////////////////////////////////////////////////////////////////////
  QrEntregaCliE_ALL.Value := QrEntregaCliL_Nome.Value;
  if QrEntregaCliL_CNPJ.Value <> '' then
    QrEntregaCliE_ALL.Value := QrEntregaCliE_ALL.Value + ' CNPJ: ' +
    Geral.FormataCNPJ_TT(QrEntregaCliL_CNPJ.Value)
  else
    QrEntregaCliE_ALL.Value := QrEntregaCliE_ALL.Value + ' CPF: ' +
    Geral.FormataCNPJ_TT(QrEntregaCliL_CPF.Value);
  if Trim(QrEntregaCliL_IE.Value) <> '' then QrEntregaCliE_ALL.Value :=
    QrEntregaCliE_ALL.Value + ' I.E. ' + QrEntregaCliL_IE.Value;
  if Trim(QrEntregaCliTE1.Value) <> '' then QrEntregaCliE_ALL.Value :=
    QrEntregaCliE_ALL.Value + ' Fone: ' + QrEntregaCliTE1_TXT.Value;
  if Trim(QrEntregaCliEMAIL.Value) <> '' then QrEntregaCliE_ALL.Value :=
    QrEntregaCliE_ALL.Value + ' Email: ' + QrEntregaCliEMAIL.Value;

  QrEntregaCliE_ALL.Value := QrEntregaCliE_ALL.Value + sLineBreak +
    Uppercase(QrEntregaCliNOMELOGRAD.Value);
  if Trim(QrEntregaCliE_ALL.Value) <> '' then QrEntregaCliE_ALL.Value :=
    QrEntregaCliE_ALL.Value + ' ';
  QrEntregaCliE_ALL.Value := QrEntregaCliE_ALL.Value + Uppercase(QrEntregaCliRua.Value);
  if Trim(QrEntregaCliRua.Value) <> '' then QrEntregaCliE_ALL.Value :=
    QrEntregaCliE_ALL.Value + ', ' + QrEntregaCliNUMERO_TXT.Value;
  if Trim(QrEntregaCliCompl.Value) <>  '' then QrEntregaCliE_ALL.Value :=
    QrEntregaCliE_ALL.Value + ' ' + Uppercase(QrEntregaCliCompl.Value);
  if Trim(QrEntregaCliBairro.Value) <>  '' then QrEntregaCliE_ALL.Value :=
    QrEntregaCliE_ALL.Value + ' - ' + Uppercase(QrEntregaCliBairro.Value);
  if QrEntregaCliCEP.Value > 0 then QrEntregaCliE_ALL.Value :=
    QrEntregaCliE_ALL.Value + ' CEP ' +Geral.FormataCEP_NT(QrEntregaCliCEP.Value);
  if Trim(QrEntregaCliNO_MUNICI.Value) <>  '' then QrEntregaCliE_ALL.Value :=
    QrEntregaCliE_ALL.Value + ' - ' + Uppercase(QrEntregaCliNO_MUNICI.Value);
  if Trim(QrEntregaCliNOMEUF.Value) <>  '' then QrEntregaCliE_ALL.Value :=
    QrEntregaCliE_ALL.Value + ', ' + QrEntregaCliNOMEUF.Value;
  if Trim(QrEntregaCliNO_PAIS.Value) <>  '' then QrEntregaCliE_ALL.Value :=
    QrEntregaCliE_ALL.Value + ' - ' + QrEntregaCliNO_PAIS.Value;
  //
  //QrEntregaCliCEP_TXT.Value :=Geral.FormataCEP_NT(QrEntregaCliCEP.Value);
  //
  //if Trim(QrEntregaCliE_ALL.Value) = '' then
    //QrEntregaCliE_ALL.Value := QrCliE_ALL.Value;
end;

procedure TFmPediVda2.QrEntregaEntiCalcFields(DataSet: TDataSet);
begin
  QrEntregaEntiTE1_TXT.Value :=
    Geral.FormataTelefone_TT_Curto(QrEntregaEntiTe1.Value);
  QrEntregaEntiFAX_TXT.Value :=
    Geral.FormataTelefone_TT_Curto(QrEntregaEntiFax.Value);
  {
  QrEntregaEntiCNPJ_TXT.Value :=
    Geral.FormataCNPJ_TT(QrEntregaEntiCNPJ_CPF.Value);
  QrEntregaEntiNOME_TIPO_DOC.Value :=
    dmkPF.ObtemTipoDocTxtDeCPFCNPJ(QrEntregaEntiCNPJ_CPF.Value) + ':';
  }
  QrEntregaEntiNUMERO_TXT.Value :=
    Geral.FormataNumeroDeRua(QrEntregaEntiRua.Value, QrEntregaEntiNumero.Value, False);
  //
  QrEntregaEntiE_ALL.Value := QrEntregaEntiNOME_ENT.Value;
  case QrEntregaEntiTipo.Value of
    0: QrEntregaEntiE_ALL.Value := QrEntregaEntiE_ALL.Value + ' CNPJ: ' +
       Geral.FormataCNPJ_TT(QrEntregaEntiCNPJ_CPF.Value);
    1: QrEntregaEntiE_ALL.Value := QrEntregaEntiE_ALL.Value + ' CPF: ' +
       Geral.FormataCNPJ_TT(QrEntregaEntiCNPJ_CPF.Value);
  end;
  if Trim(QrEntregaEntiIE.Value) <> '' then QrEntregaEntiE_ALL.Value :=
    QrEntregaEntiE_ALL.Value + ' I.E. ' + QrEntregaEntiIE.Value;
  if Trim(QrEntregaEntiTE1.Value) <> '' then QrEntregaEntiE_ALL.Value :=
    QrEntregaEntiE_ALL.Value + ' Fone: ' + QrEntregaEntiTE1_TXT.Value;
  if Trim(QrEntregaEntiEMAIL.Value) <> '' then QrEntregaEntiE_ALL.Value :=
    QrEntregaEntiE_ALL.Value + ' Email: ' + QrEntregaEntiEMAIL.Value;

  QrEntregaEntiE_ALL.Value := QrEntregaEntiE_ALL.Value + sLineBreak +
    Uppercase(QrEntregaEntiNOMELOGRAD.Value);
  if Trim(QrEntregaEntiE_ALL.Value) <> '' then QrEntregaEntiE_ALL.Value :=
    QrEntregaEntiE_ALL.Value + ' ';
  QrEntregaEntiE_ALL.Value := QrEntregaEntiE_ALL.Value + Uppercase(QrEntregaEntiRua.Value);
  if Trim(QrEntregaEntiRua.Value) <> '' then QrEntregaEntiE_ALL.Value :=
    QrEntregaEntiE_ALL.Value + ', ' + QrEntregaEntiNUMERO_TXT.Value;
  if Trim(QrEntregaEntiCompl.Value) <>  '' then QrEntregaEntiE_ALL.Value :=
    QrEntregaEntiE_ALL.Value + ' ' + Uppercase(QrEntregaEntiCompl.Value);
  if Trim(QrEntregaEntiBairro.Value) <>  '' then QrEntregaEntiE_ALL.Value :=
    QrEntregaEntiE_ALL.Value + ' - ' + Uppercase(QrEntregaEntiBairro.Value);
  if QrEntregaEntiCEP.Value > 0 then QrEntregaEntiE_ALL.Value :=
    QrEntregaEntiE_ALL.Value + ' CEP ' +Geral.FormataCEP_NT(QrEntregaEntiCEP.Value);
  if Trim(QrEntregaEntiCidade.Value) <>  '' then QrEntregaEntiE_ALL.Value :=
    QrEntregaEntiE_ALL.Value + ' - ' + Uppercase(QrEntregaEntiCidade.Value);
  if Trim(QrEntregaEntiNOMEUF.Value) <>  '' then QrEntregaEntiE_ALL.Value :=
    QrEntregaEntiE_ALL.Value + ', ' + QrEntregaEntiNOMEUF.Value;
  if Trim(QrEntregaEntiPais.Value) <>  '' then QrEntregaEntiE_ALL.Value :=
    QrEntregaEntiE_ALL.Value + ' - ' + QrEntregaEntiPais.Value;
  //
  //QrEntregaEntiCEP_TXT.Value :=Geral.FormataCEP_NT(QrEntregaEntiCEP.Value);
  //
  //if Trim(QrEntregaEntiE_ALL.Value) = '' then
    //QrEntregaEntiE_ALL.Value := QrCliE_ALL.Value;
end;

procedure TFmPediVda2.QrItsCAfterScroll(DataSet: TDataSet);
begin
  QrItsZ.Close;
  QrItsZ.Params[0].AsInteger := QrItsCControle.Value;
  UnDmkDAC_PF.AbreQuery(QrItsZ, Dmod.MyDB);
end;

procedure TFmPediVda2.QrItsCCalcFields(DataSet: TDataSet);
var
  Txt: String;
begin
  Txt := '';
  if QrItsCTipDimens.Value >= 0 then
  begin
    if QrItsCMedida1.Value <> '' then
      Txt := Txt + QrItsCMedida1.Value + ' ';
    Txt := Txt + Geral.FFT_MinMaxCasas(QrItsCMedidaC.Value, 2, 4) + ' ';
    if QrItsCSigla1.Value <> '' then
      Txt := Txt + QrItsCSigla1.Value + ' ';
  end;
  //
  if QrItsCTipDimens.Value >= 1 then
  begin
    if QrItsCMedida2.Value <> '' then
      Txt := Txt + QrItsCMedida2.Value + ' ';
    Txt := Txt + Geral.FFT_MinMaxCasas(QrItsCMedidaL.Value, 2, 4) + ' ';
    if QrItsCSigla2.Value <> '' then
      Txt := Txt + QrItsCSigla2.Value + ' ';
  end;
  //
  if QrItsCTipDimens.Value >= 2 then
  begin
    if QrItsCMedida3.Value <> '' then
      Txt := Txt + QrItsCMedida3.Value + ' ';
    Txt := Txt + Geral.FFT_MinMaxCasas(QrItsCMedidaA.Value, 2, 4) + ' ';
    if QrItsCSigla3.Value <> '' then
      Txt := Txt + QrItsCSigla3.Value + ' ';
  end;
  //
  if QrItsCTipDimens.Value >= 3 then
  begin
    if QrItsCMedida4.Value <> '' then
      Txt := Txt + QrItsCMedida4.Value + ' ';
    Txt := Txt + Geral.FFT_MinMaxCasas(QrItsCMedidaE.Value, 3, 6) + ' ';
    if QrItsCSigla4.Value <> '' then
      Txt := Txt + QrItsCSigla4.Value + ' ';
  end;
  //
  QrItsCDESCRICAO.Value := Txt;
end;

procedure TFmPediVda2.QrItsZCalcFields(DataSet: TDataSet);
var
  Txt: String;
begin
  Txt := '';
  if QrItsZTipDimens.Value >= 0 then
  begin
    if QrItsZMedida1.Value <> '' then
      Txt := Txt + QrItsZMedida1.Value + ' ';
    Txt := Txt + Geral.FFT_MinMaxCasas(QrItsZMedidaC.Value, 2, 4) + ' ';
    if QrItsZSigla1.Value <> '' then
      Txt := Txt + QrItsZSigla1.Value + ' ';
  end;
  //
  if QrItsZTipDimens.Value >= 1 then
  begin
    if QrItsZMedida2.Value <> '' then
      Txt := Txt + QrItsZMedida2.Value + ' ';
    Txt := Txt + Geral.FFT_MinMaxCasas(QrItsZMedidaL.Value, 2, 4) + ' ';
    if QrItsZSigla2.Value <> '' then
      Txt := Txt + QrItsZSigla2.Value + ' ';
  end;
  //
  if QrItsZTipDimens.Value >= 2 then
  begin
    if QrItsZMedida3.Value <> '' then
      Txt := Txt + QrItsZMedida3.Value + ' ';
    Txt := Txt + Geral.FFT_MinMaxCasas(QrItsZMedidaA.Value, 2, 4) + ' ';
    if QrItsZSigla3.Value <> '' then
      Txt := Txt + QrItsZSigla3.Value + ' ';
  end;
  //
  if QrItsZTipDimens.Value >= 3 then
  begin
    if QrItsZMedida4.Value <> '' then
      Txt := Txt + QrItsZMedida4.Value + ' ';
    Txt := Txt + Geral.FFT_MinMaxCasas(QrItsZMedidaE.Value, 3, 6) + ' ';
    if QrItsZSigla4.Value <> '' then
      Txt := Txt + QrItsZSigla4.Value + ' ';
  end;
  //
  QrItsZDESCRICAO.Value := Txt;
end;

procedure TFmPediVda2.QrPediVdaGruAfterOpen(DataSet: TDataSet);
begin
  StaticText1.Visible := QrPediVdaGru.RecordCount > 0;
end;

procedure TFmPediVda2.QrPediVdaGruAfterScroll(DataSet: TDataSet);
var
  Grade, Nivel1, Pedido: Integer;
begin
  Grade      := QrPediVdaGruGRATAMCAD.Value;
  Nivel1     := QrPediVdaGruNivel1.Value;
  Pedido     := QrPediVdaCodigo.Value;
  //
  DmProd.ConfigGrades6(Grade, Nivel1, Pedido,
  GradeA, GradeX, GradeC, GradeQ, GradeF, GradeD, GradeV);
  //
  QrCustomizados.Close;
  // S� se tiver itens customizados para n�o perder tempo abrindo tabela a toa
  if QrPediVdaGruItensCustomizados.Value > 0 then
  begin
    QrCustomizados.Params[00].AsInteger := Pedido;
    QrCustomizados.Params[01].AsInteger := Nivel1;
    UnDmkDAC_PF.AbreQuery(QrCustomizados, Dmod.MyDB);
  end;
end;

procedure TFmPediVda2.QrPediVdaGruBeforeClose(DataSet: TDataSet);
begin
  MyObjects.LimpaGrades([GradeQ, GradeF, GradeD, GradeV, GradeC, GradeA, GradeX],
    0, 0, True);
  QrCustomizados.Close;
end;

procedure TFmPediVda2.ReopenCentroCusto();
var
  SQL_PagRec: String;
begin
(*
  case FtpPagto of
    TTipoPagto.tpDeb: SQL_PagRec := 'AND niv5.PagRec=1 ';
    TTipoPagto.tpCred: SQL_PagRec := 'AND niv5.PagRec=2 ';
    TTipoPagto.tpAny: SQL_PagRec := 'AND niv5.PagRec in (1, 2) ';
    else
      SQL_PagRec := '';
  end;
*)
  SQL_PagRec := 'AND niv5.PagRec=1 ';
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrCentroCusto, Dmod.MyDB, [
  'SELECT ',
  'CONCAT(',
  '  LPAD(niv4.Ordem, 1, "0"), ".",',
  '  LPAD(niv3.Ordem, 2, "0"), ".",',
  '  LPAD(niv2.Ordem, 3, "0"), ".",',
  '  LPAD(niv1.Ordem, 4, "0") ',
  ') REF_NIV1,',
  'niv1.Codigo, niv1.Nome,',
  'niv1.Ativo ',
  'FROM centrocust5 niv5 ',
  'LEFT JOIN centrocust4 niv4 ON niv5.Codigo=niv4.NivSup',
  'LEFT JOIN centrocust3 niv3 ON niv4.Codigo=niv3.NivSup',
  'LEFT JOIN centrocust2 niv2 ON niv3.Codigo=niv2.NivSup',
  'LEFT JOIN centrocusto niv1 ON niv2.Codigo=niv1.NivSup',
  'WHERE niv1.Ativo=1',
  SQL_PagRec,
  'ORDER BY niv1.Nome',
  '']);
end;

procedure TFmPediVda2.ReopenEmissF(FatParcela: Integer);
begin
  if UFinanceiro.TabLctNaoDef(FTabLctA, False) then
    Exit;
  UnDmkDAC_PF.AbreMySQLQuery0(QrEmissF, Dmod.MyDB, [
    'SELECT la.*, ca.Nome NOMECARTEIRA ',
    'FROM ' + FTabLctA + ' la  ',
    'LEFT JOIN carteiras ca ON ca.Codigo=la.Carteira ',
    'WHERE la.FatID=' + Geral.FF0(VAR_FATID_1001),
    //'AND la.FatNum=' + Geral.FF0(QrPQECodigo.Value),
    'AND la.FatParcRef=' + Geral.FF0(QrPediVdaCodigo.Value),
    'AND la.ID_Pgto = 0 ',
    'ORDER BY FatParcela ',
    '']);
end;

procedure TFmPediVda2.ReopenPediVdaCuz(Conta: Integer);
begin
  QrPediVdaCuz.Close;
  QrPediVdaCuz.Params[0].AsInteger := QrCustomizadosControle.Value;
  UnDmkDAC_PF.AbreQuery(QrPediVdaCuz, Dmod.MyDB);
  //
  if Conta <> 0 then
    QrPediVdaCuz.Locate('Conta', Conta, []);
end;

procedure TFmPediVda2.ReopenPediVdaGru(Nivel1: Integer);
begin
(* ini 2022-04-03
  QrPediVdaGru.Close;
  QrPediVdaGru.Params[0].AsInteger := QrPediVdaCodigo.Value;
  UnDmkDAC_PF.AbreQuery(QrPediVdaGru, Dmod.MyDB);
*)
  UnDmkDAC_PF.AbreMySQLQuery0(QrPediVdaGru, Dmod.MyDB, [
  'SELECT pvi.Controle, SUM(QuantP) QuantP, ',
  'SUM(ValLiq) ValLiq, SUM(ValBru) ValBru, ',
  'SUM(Customizad) ItensCustomizados, ',
  'gti.Codigo GRATAMCAD, ',
  'gg1.CodUsu, gg1.Nome, gg1.Nivel1, pgt.Fracio, ',
  'IF(QuantP <= 0, 0.00, vProd / QuantP) PrecoF ',
  'FROM pedivdaits pvi ',
  'LEFT JOIN gragrux   ggx ON ggx.Controle=pvi.GraGruX ',
  'LEFT JOIN gratamits gti ON gti.Controle=ggx.GraTamI ',
  'LEFT JOIN gragru1   gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN prdgruptip   pgt ON pgt.Codigo=gg1.PrdGrupTip ',
  'WHERE pvi.Codigo=' + Geral.FF0(QrPediVdaCodigo.Value),
  'GROUP BY gg1.Nivel1 ',
  'ORDER BY pvi.Controle ',
  '']);
  //
  if Nivel1 <> 0 then
  begin
    DmPediVda.QrLocNiv1.Close;
    DmPediVda.QrLocNiv1.Params[0].AsInteger := Nivel1;
    UnDmkDAC_PF.AbreQuery(DmPediVda.QrLocNiv1, Dmod.MyDB);
    //
    QrPediVdaGru.Locate('Nivel1', DmPediVda.QrLocNiv1GraGru1.Value, []);
  end;
end;

procedure TFmPediVda2.ReopenSumIts();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrSumIts, Dmod.MyDB, [
  'SELECT SUM(QuantP) QuantP, ',
  'SUM(DescoV) DescoV,',
  'SUM(ValLiq) ValLiq, SUM(ValBru) ValBru, ',
  'SUM(Customizad) ItensCustomizados,',
  '',
  'SUM(vProd) vProd, SUM(vFrete) vFrete, ',
  'SUM(vSeg) vSeg, SUM(vOutro) vOutro, ',
  'SUM(vDesc) vDesc, SUM(vBC) vBC',
  '',
  'FROM pedivdaits pvi ',
  'WHERE pvi.Codigo=' + Geral.FF0(QrPediVdaCodigo.Value),
  '']);
end;

procedure TFmPediVda2.RGEntSaiClick(Sender: TObject);
begin
  GBDadosTop.Visible := RGEntSai.ItemIndex >= 0;
  PCDadosTop.Visible := RGEntSai.ItemIndex >= 0;
  DmPediVda.ReopenClientes(nil, RGEntSai.ItemIndex);
  DmPediVda.ReopenFisRegCad(RGEntSai.ItemIndex);
  if RGEntSai.ItemIndex >= 0 then
  begin
    try
      if (ImgTipo.SQLType = stIns) and EdEmpresa.Visible then
        EdEmpresa.SetFocus;
    except
      //
    end;
  end;
  if RGEntSai.ItemIndex = 0 then
    Edide_finNFe.ValueVariant := 1;
end;

{
configurar DBGrid2
Exlui parte
altera parte
altera item customizado
}


end.

