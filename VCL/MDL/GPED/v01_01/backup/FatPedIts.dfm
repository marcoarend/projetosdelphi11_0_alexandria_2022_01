object FmFatPedIts: TFmFatPedIts
  Left = 339
  Top = 185
  Caption = 'FAT-PEDID-003 :: Itens de Faturamento de Pedido'
  ClientHeight = 864
  ClientWidth = 1241
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -14
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 120
  TextHeight = 16
  object Panel1: TPanel
    Left = 0
    Top = 59
    Width = 1241
    Height = 672
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 1241
      Height = 60
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object Panel7: TPanel
        Left = 316
        Top = 0
        Width = 925
        Height = 60
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object Label2: TLabel
          Left = 15
          Top = 5
          Width = 599
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 
            'Centro de estoque (Cadastrados nos itens de movimenta'#231#227'o da regr' +
            'a fiscal selecionada no pedido):'
        end
        object Label15: TLabel
          Left = 613
          Top = 5
          Width = 75
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Qtd '#224' faturar:'
        end
        object Label16: TLabel
          Left = 724
          Top = 5
          Width = 76
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Qtd faturada:'
        end
        object EdStqCenCad: TdmkEditCB
          Left = 15
          Top = 25
          Width = 69
          Height = 25
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          OnChange = EdStqCenCadChange
          OnEnter = EdStqCenCadEnter
          OnExit = EdStqCenCadExit
          DBLookupComboBox = CBStqCenCad
          IgnoraDBLookupComboBox = False
        end
        object CBStqCenCad: TdmkDBLookupComboBox
          Left = 84
          Top = 25
          Width = 508
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          KeyField = 'CodUsu'
          ListField = 'Nome'
          ListSource = DsStqCenCad
          TabOrder = 1
          dmkEditCB = EdStqCenCad
          UpdType = utYes
          LocF7SQLMasc = '$#'
        end
        object EdQtdAFat: TdmkEdit
          Left = 613
          Top = 25
          Width = 105
          Height = 25
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Alignment = taRightJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          ReadOnly = True
          TabOrder = 2
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 3
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,000'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
          OnChange = EdQtdLeiChange
          OnEnter = EdQtdLeiEnter
          OnExit = EdQtdLeiExit
          OnKeyDown = EdQtdLeiKeyDown
        end
        object EdQtdFat: TdmkEdit
          Left = 724
          Top = 25
          Width = 104
          Height = 25
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Alignment = taRightJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          ReadOnly = True
          TabOrder = 3
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 3
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,000'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
          OnChange = EdQtdLeiChange
          OnEnter = EdQtdLeiEnter
          OnExit = EdQtdLeiExit
          OnKeyDown = EdQtdLeiKeyDown
        end
      end
      object Panel8: TPanel
        Left = 0
        Top = 0
        Width = 316
        Height = 60
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alLeft
        BevelOuter = bvNone
        Enabled = False
        TabOrder = 1
        object Label1: TLabel
          Left = 123
          Top = 5
          Width = 49
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Volume:'
          FocusControl = DBEdit5
        end
        object Label10: TLabel
          Left = 10
          Top = 5
          Width = 52
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'ID fatura:'
          FocusControl = DBEdit6
        end
        object DBEdit5: TdmkDBEdit
          Left = 123
          Top = 25
          Width = 69
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          DataField = 'Cnta'
          DataSource = DsFatPedVol
          TabOrder = 0
          UpdCampo = 'Cnta'
          UpdType = utYes
          Alignment = taLeftJustify
        end
        object DBEdit6: TdmkDBEdit
          Left = 10
          Top = 25
          Width = 109
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          DataField = 'Codigo'
          DataSource = DsFatPedCab
          TabOrder = 1
          UpdCampo = 'Codigo'
          UpdType = utYes
          Alignment = taLeftJustify
        end
        object dmkDBEdit1: TdmkDBEdit
          Left = 197
          Top = 25
          Width = 109
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          DataField = 'NO_UnidMed'
          DataSource = DsFatPedVol
          TabOrder = 2
          UpdCampo = 'Cnta'
          UpdType = utYes
          Alignment = taLeftJustify
        end
      end
    end
    object PageControl1: TPageControl
      Left = 0
      Top = 60
      Width = 1241
      Height = 128
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      ActivePage = TabSheet1
      Align = alTop
      TabOrder = 1
      object TabSheet1: TTabSheet
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = ' por &Leitura '
        object PnLeitura: TPanel
          Left = 0
          Top = 0
          Width = 1233
          Height = 59
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alTop
          BevelOuter = bvNone
          Enabled = False
          ParentBackground = False
          TabOrder = 0
          object Panel4: TPanel
            Left = 0
            Top = 0
            Width = 351
            Height = 59
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alLeft
            BevelOuter = bvNone
            TabOrder = 0
            object Label3: TLabel
              Left = 15
              Top = 5
              Width = 109
              Height = 16
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Leitura / digita'#231#227'o:'
            end
            object LaQtdeLei: TLabel
              Left = 241
              Top = 5
              Width = 73
              Height = 16
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Quantidade:'
            end
            object EdLeitura: TEdit
              Left = 15
              Top = 25
              Width = 221
              Height = 24
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              MaxLength = 20
              TabOrder = 0
              OnChange = EdLeituraChange
              OnExit = EdLeituraExit
              OnKeyDown = EdLeituraKeyDown
            end
            object EdQtdLei: TdmkEdit
              Left = 241
              Top = 25
              Width = 105
              Height = 25
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Alignment = taRightJustify
              TabOrder = 1
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 3
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,000'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
              OnChange = EdQtdLeiChange
              OnEnter = EdQtdLeiEnter
              OnExit = EdQtdLeiExit
              OnKeyDown = EdQtdLeiKeyDown
            end
          end
          object PnMultiGrandeza: TPanel
            Left = 351
            Top = 0
            Width = 433
            Height = 59
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alLeft
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 1
            Visible = False
            object LaPecas: TLabel
              Left = 5
              Top = 5
              Width = 42
              Height = 16
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Pe'#231'as:'
            end
            object LaAreaM2: TLabel
              Left = 113
              Top = 5
              Width = 50
              Height = 16
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = #193'rea m'#178':'
            end
            object LaAreaP2: TLabel
              Left = 222
              Top = 5
              Width = 45
              Height = 16
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = #193'rea ft'#178':'
            end
            object LaPeso: TLabel
              Left = 320
              Top = 5
              Width = 61
              Height = 16
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Peso (kg):'
            end
            object EdPecas: TdmkEdit
              Left = 5
              Top = 25
              Width = 105
              Height = 25
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Alignment = taRightJustify
              TabOrder = 0
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 3
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,000'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
            end
            object EdPeso: TdmkEdit
              Left = 320
              Top = 25
              Width = 105
              Height = 25
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Alignment = taRightJustify
              TabOrder = 3
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 3
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,000'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
            end
            object EdAreaM2: TdmkEditCalc
              Left = 113
              Top = 25
              Width = 99
              Height = 25
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Alignment = taRightJustify
              TabOrder = 1
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '0'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
              dmkEditCalcA = EdAreaP2
              CalcType = ctM2toP2
              CalcFrac = cfQuarto
            end
            object EdAreaP2: TdmkEditCalc
              Left = 217
              Top = 25
              Width = 98
              Height = 25
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Alignment = taRightJustify
              TabOrder = 2
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '0'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
              dmkEditCalcA = EdAreaM2
              CalcType = ctP2toM2
              CalcFrac = cfCento
            end
          end
          object Panel6: TPanel
            Left = 784
            Top = 0
            Width = 130
            Height = 59
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alLeft
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 2
            object BtOK: TBitBtn
              Tag = 14
              Left = 10
              Top = 4
              Width = 111
              Height = 49
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = '&OK'
              NumGlyphs = 2
              TabOrder = 0
              OnClick = BtOKClick
            end
          end
          object Panel13: TPanel
            Left = 914
            Top = 0
            Width = 319
            Height = 59
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alClient
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 3
            object BtGraGruN: TBitBtn
              Tag = 30
              Left = 10
              Top = 4
              Width = 49
              Height = 49
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              NumGlyphs = 2
              TabOrder = 0
              OnClick = BtGraGruNClick
            end
            object BtTabePrcCab: TBitBtn
              Tag = 10107
              Left = 60
              Top = 4
              Width = 50
              Height = 49
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              NumGlyphs = 2
              TabOrder = 1
              OnClick = BtTabePrcCabClick
            end
            object BtFisRegCad: TBitBtn
              Tag = 10104
              Left = 111
              Top = 4
              Width = 49
              Height = 49
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              NumGlyphs = 2
              TabOrder = 2
              OnClick = BtFisRegCadClick
            end
          end
        end
        object Panel9: TPanel
          Left = 0
          Top = 59
          Width = 1233
          Height = 38
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 1
          object Label4: TLabel
            Left = 15
            Top = 10
            Width = 108
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Grupo de produto:'
            FocusControl = DBEdit1
          end
          object Label5: TLabel
            Left = 482
            Top = 10
            Width = 24
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Cor:'
            FocusControl = DBEdit2
          end
          object Label6: TLabel
            Left = 709
            Top = 10
            Width = 61
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Tamanho:'
            FocusControl = DBEdit3
          end
          object Label12: TLabel
            Left = 837
            Top = 10
            Width = 39
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Pre'#231'o:'
            FocusControl = DBEdit7
          end
          object Label11: TLabel
            Left = 965
            Top = 10
            Width = 33
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = '% IPI:'
            FocusControl = DBEdit4
          end
          object DBEdit1: TDBEdit
            Left = 128
            Top = 5
            Width = 346
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            DataField = 'NOMENIVEL1'
            DataSource = DsItem
            TabOrder = 0
          end
          object DBEdit2: TDBEdit
            Left = 512
            Top = 5
            Width = 193
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            DataField = 'NOMECOR'
            DataSource = DsItem
            TabOrder = 1
          end
          object DBEdit3: TDBEdit
            Left = 773
            Top = 5
            Width = 60
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            DataField = 'NOMETAM'
            DataSource = DsItem
            TabOrder = 2
          end
          object DBEdit7: TDBEdit
            Left = 881
            Top = 5
            Width = 79
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            DataField = 'PrecoF'
            DataSource = DsPreco
            TabOrder = 3
          end
          object DBEdit4: TDBEdit
            Left = 1004
            Top = 5
            Width = 50
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            DataField = 'IPI_Alq'
            DataSource = DsItem
            TabOrder = 4
          end
        end
      end
    end
    object PageControl2: TPageControl
      Left = 0
      Top = 188
      Width = 1241
      Height = 484
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      ActivePage = TabSheet6
      Align = alClient
      TabOrder = 2
      OnChange = PageControl2Change
      object TabSheet4: TTabSheet
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = ' Itens em faturamento '
        ImageIndex = 1
        object PageControl3: TPageControl
          Left = 471
          Top = 0
          Width = 762
          Height = 453
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          ActivePage = TabSheet3
          Align = alClient
          TabOrder = 0
          object TabSheet7: TTabSheet
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = ' A faturar '
            ImageIndex = 2
            object GradeQ: TStringGrid
              Left = 0
              Top = 0
              Width = 754
              Height = 422
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alClient
              ColCount = 2
              DefaultColWidth = 65
              DefaultRowHeight = 18
              FixedCols = 0
              RowCount = 2
              FixedRows = 0
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -15
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goEditing, goTabs]
              ParentFont = False
              TabOrder = 0
              OnDrawCell = GradeQDrawCell
            end
          end
          object TabSheet3: TTabSheet
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = ' Pendentes '
            ImageIndex = 4
            object GradeP: TStringGrid
              Left = 0
              Top = 0
              Width = 754
              Height = 422
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alClient
              ColCount = 2
              DefaultColWidth = 65
              DefaultRowHeight = 18
              FixedCols = 0
              RowCount = 2
              FixedRows = 0
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -15
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goEditing, goTabs]
              ParentFont = False
              TabOrder = 0
              OnClick = GradePClick
              OnDblClick = GradePDblClick
              OnDrawCell = GradePDrawCell
              OnSelectCell = GradePSelectCell
            end
          end
          object TabSheet8: TTabSheet
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = ' C'#243'digos '
            ImageIndex = 3
            object GradeC: TStringGrid
              Left = 0
              Top = 0
              Width = 754
              Height = 422
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alClient
              ColCount = 2
              DefaultColWidth = 65
              DefaultRowHeight = 18
              RowCount = 2
              TabOrder = 0
              OnDrawCell = GradeCDrawCell
              RowHeights = (
                18
                19)
            end
          end
          object TabSheet9: TTabSheet
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = ' Ativos '
            object GradeA: TStringGrid
              Left = 0
              Top = 0
              Width = 754
              Height = 422
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alClient
              ColCount = 2
              DefaultColWidth = 65
              DefaultRowHeight = 18
              FixedCols = 0
              RowCount = 2
              FixedRows = 0
              TabOrder = 0
              OnDrawCell = GradeADrawCell
              RowHeights = (
                18
                18)
            end
          end
          object TabSheet10: TTabSheet
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = ' X '
            ImageIndex = 6
            object GradeX: TStringGrid
              Left = 0
              Top = 0
              Width = 754
              Height = 422
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alClient
              ColCount = 2
              DefaultColWidth = 65
              DefaultRowHeight = 18
              FixedCols = 0
              RowCount = 2
              FixedRows = 0
              TabOrder = 0
              OnDrawCell = GradeXDrawCell
              RowHeights = (
                18
                18)
            end
          end
        end
        object Panel11: TPanel
          Left = 0
          Top = 0
          Width = 471
          Height = 453
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alLeft
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 1
          object DBGGru: TdmkDBGrid
            Left = 0
            Top = 26
            Width = 471
            Height = 427
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alClient
            Columns = <
              item
                Expanded = False
                FieldName = 'CodUsu'
                Title.Caption = 'C'#243'digo'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Nome'
                Title.Caption = 'Descri'#231#227'o'
                Width = 220
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'QuantF'
                Title.Caption = 'Pendente'
                Width = 61
                Visible = True
              end>
            Color = clWindow
            DataSource = DsPediGru
            Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
            TabOrder = 0
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -14
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            Columns = <
              item
                Expanded = False
                FieldName = 'CodUsu'
                Title.Caption = 'C'#243'digo'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Nome'
                Title.Caption = 'Descri'#231#227'o'
                Width = 220
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'QuantF'
                Title.Caption = 'Pendente'
                Width = 61
                Visible = True
              end>
          end
          object Panel12: TPanel
            Left = 0
            Top = 0
            Width = 471
            Height = 26
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alTop
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 1
            object CkSoItensAFat: TCheckBox
              Left = 5
              Top = 4
              Width = 308
              Height = 21
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Mostrar somente grupos com itens a faturar.'
              TabOrder = 0
              OnClick = CkSoItensAFatClick
            end
          end
        end
      end
      object TabSheet6: TTabSheet
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = ' Lista '
        ImageIndex = 3
        object DBGFatPedIts: TDBGrid
          Left = 0
          Top = 122
          Width = 1233
          Height = 331
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alClient
          DataSource = FmFatPedCab.DsFatPedIts
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -14
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'CU_NIVEL1'
              Title.Caption = 'Produto'
              Width = 75
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_NIVEL1'
              Title.Caption = 'Descri'#231#227'o'
              Width = 325
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'CU_COR'
              Title.Caption = 'Cor'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_COR'
              Title.Caption = 'Descri'#231#227'o'
              Width = 199
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_TAM'
              Title.Caption = 'Tamanho'
              Width = 104
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'QTDE_POSITIVO'
              Title.Caption = 'Quantidade'
              Width = 86
              Visible = True
            end>
        end
        object PnSimu: TPanel
          Left = 0
          Top = 0
          Width = 1233
          Height = 65
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alTop
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 1
          Visible = False
          object Label9: TLabel
            Left = 453
            Top = 5
            Width = 68
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Sequ'#234'ncia:'
          end
          object Label8: TLabel
            Left = 350
            Top = 5
            Width = 61
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Reduzido:'
          end
          object Label7: TLabel
            Left = 246
            Top = 5
            Width = 95
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Ordem de serv.:'
          end
          object dmkEdit2: TdmkEdit
            Left = 350
            Top = 25
            Width = 99
            Height = 25
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 6
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '000010'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 10
            ValWarn = False
            OnChange = EdLeituraChange
          end
          object dmkEdit3: TdmkEdit
            Left = 453
            Top = 25
            Width = 100
            Height = 25
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Alignment = taRightJustify
            TabOrder = 1
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 8
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '00000001'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 1
            ValWarn = False
            OnChange = EdLeituraChange
          end
          object dmkEdit1: TdmkEdit
            Left = 246
            Top = 25
            Width = 100
            Height = 25
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Alignment = taRightJustify
            TabOrder = 2
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 6
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '000000'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            OnChange = EdLeituraChange
          end
          object Button1: TButton
            Left = 5
            Top = 15
            Width = 228
            Height = 31
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Simula leitura do c'#243'digo de barras'
            TabOrder = 3
            OnClick = Button1Click
          end
        end
        object Panel10: TPanel
          Left = 0
          Top = 65
          Width = 1233
          Height = 57
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alTop
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 2
          Visible = False
          object Label13: TLabel
            Left = 5
            Top = 5
            Width = 41
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = '% C.F.:'
            FocusControl = DBEdit8
          end
          object Label14: TLabel
            Left = 69
            Top = 5
            Width = 81
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'ICMS UF>UF:'
            FocusControl = DBEdit16
          end
          object DBEdit8: TDBEdit
            Left = 5
            Top = 25
            Width = 59
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            DataField = 'Variacao'
            TabOrder = 0
          end
          object DBCkICMS: TDBCheckBox
            Left = 162
            Top = 2
            Width = 60
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'ICMS:'
            DataField = 'ICMS_Usa'
            TabOrder = 1
            ValueChecked = '1'
            ValueUnchecked = '0'
          end
          object DBEdit9: TDBEdit
            Left = 162
            Top = 25
            Width = 60
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            DataField = 'ICMS_Alq'
            TabOrder = 2
          end
          object DBEdit10: TDBEdit
            Left = 290
            Top = 25
            Width = 60
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            DataField = 'PIS_Alq'
            TabOrder = 3
          end
          object DBCheckBox2: TDBCheckBox
            Left = 290
            Top = 2
            Width = 60
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'PIS:'
            DataField = 'PIS_Usa'
            TabOrder = 4
            ValueChecked = '1'
            ValueUnchecked = '0'
          end
          object DBEdit11: TDBEdit
            Left = 354
            Top = 25
            Width = 60
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            DataField = 'COFINS_Alq'
            TabOrder = 5
          end
          object DBCheckBox3: TDBCheckBox
            Left = 354
            Top = 2
            Width = 60
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Cofins:'
            DataField = 'COFINS_Usa'
            TabOrder = 6
            ValueChecked = '1'
            ValueUnchecked = '0'
          end
          object DBEdit12: TDBEdit
            Left = 418
            Top = 25
            Width = 60
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            DataField = 'IR_Alq'
            TabOrder = 7
          end
          object DBCheckBox4: TDBCheckBox
            Left = 418
            Top = 2
            Width = 60
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'IR:'
            DataField = 'IR_Usa'
            TabOrder = 8
            ValueChecked = '1'
            ValueUnchecked = '0'
          end
          object DBEdit13: TDBEdit
            Left = 482
            Top = 25
            Width = 60
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            DataField = 'CS_Alq'
            TabOrder = 9
          end
          object DBCheckBox5: TDBCheckBox
            Left = 482
            Top = 2
            Width = 60
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'CS:'
            DataField = 'CS_Usa'
            TabOrder = 10
            ValueChecked = '1'
            ValueUnchecked = '0'
          end
          object DBEdit14: TDBEdit
            Left = 226
            Top = 25
            Width = 60
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            DataField = 'IPI_Alq'
            TabOrder = 11
          end
          object DBCheckBox6: TDBCheckBox
            Left = 226
            Top = 2
            Width = 60
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'IPI:'
            DataField = 'IPI_Usa'
            TabOrder = 12
            ValueChecked = '1'
            ValueUnchecked = '0'
          end
          object DBEdit15: TDBEdit
            Left = 546
            Top = 25
            Width = 60
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            DataField = 'ISS_Alq'
            TabOrder = 13
          end
          object DBCheckBox7: TDBCheckBox
            Left = 546
            Top = 2
            Width = 60
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'ISS:'
            DataField = 'ISS_Usa'
            TabOrder = 14
            ValueChecked = '1'
            ValueUnchecked = '0'
          end
          object DBEdit16: TDBEdit
            Left = 69
            Top = 25
            Width = 90
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            DataField = 'ICMS_V'
            TabOrder = 15
          end
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1241
    Height = 59
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 1182
      Top = 0
      Width = 59
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 10
        Top = 14
        Width = 39
        Height = 39
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 59
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 59
      Top = 0
      Width = 1123
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 9
        Top = 11
        Width = 455
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Itens de Faturamento de Pedido'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 11
        Top = 14
        Width = 455
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Itens de Faturamento de Pedido'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 10
        Top = 12
        Width = 455
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Itens de Faturamento de Pedido'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 731
    Width = 1241
    Height = 54
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel5: TPanel
      Left = 2
      Top = 18
      Width = 1237
      Height = 34
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 16
        Top = 2
        Width = 150
        Height = 19
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -17
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 15
        Top = 1
        Width = 150
        Height = 19
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -17
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 785
    Width = 1241
    Height = 79
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alBottom
    TabOrder = 3
    object Panel14: TPanel
      Left = 2
      Top = 18
      Width = 1237
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object PnSaiDesis: TPanel
        Left = 1059
        Top = 0
        Width = 178
        Height = 59
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 3
        object BtSaida: TBitBtn
          Tag = 13
          Left = 2
          Top = 4
          Width = 148
          Height = 49
          Cursor = crHandPoint
          Hint = 'Sai da janela atual'
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Sa'#237'da'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
        end
      end
      object CkFixo: TCheckBox
        Left = 15
        Top = 20
        Width = 124
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Quantidade fixa.'
        Checked = True
        State = cbChecked
        TabOrder = 0
      end
      object BtFatura: TBitBtn
        Left = 148
        Top = 4
        Width = 147
        Height = 49
        Cursor = crHandPoint
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Fatura'
        Enabled = False
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        OnClick = BtFaturaClick
      end
      object BtExclui: TBitBtn
        Tag = 12
        Left = 562
        Top = 4
        Width = 148
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Excluir'
        NumGlyphs = 2
        TabOrder = 2
        Visible = False
        OnClick = BtExcluiClick
      end
    end
  end
  object QrStqCenCad: TmySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrStqCenCadAfterOpen
    SQL.Strings = (
      'SELECT Codigo, CodUsu, Nome'
      'FROM stqcencad scc'
      'WHERE Codigo IN '
      '('
      '  SELECT StqCenCad'
      '  FROM fisregmvt'
      '  WHERE Codigo=:P0'
      '  AND Empresa=:P1'
      ') '
      'ORDER BY Nome')
    Left = 8
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrStqCenCadCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrStqCenCadCodUsu: TIntegerField
      FieldName = 'CodUsu'
      Required = True
    end
    object QrStqCenCadNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 50
    end
  end
  object DsStqCenCad: TDataSource
    DataSet = QrStqCenCad
    Left = 36
    Top = 8
  end
  object QrItem: TmySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrItemBeforeClose
    SQL.Strings = (
      'SELECT gg1.Nome NOMENIVEL1, ggc.GraCorCad, '
      'gcc.Nome NOMECOR,  gti.Nome NOMETAM, '
      'ggx.Controle GraGruX, ggx.GraGru1, '
      'gg1.CodUsu CU_Nivel1, gg1.IPI_Alq, '
      'pgt.MadeBy, pgt.Fracio, '
      'gg1.HowBxaEstq, gg1.GerBxaEstq, gg1.prod_indTot'
      'FROM gragrux ggx '
      'LEFT JOIN gragru1   gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN gragruc ggc ON ggc.Controle=ggx.GraGruC'
      'LEFT JOIN gracorcad gcc ON gcc.Codigo=ggc.GraCorCad'
      'LEFT JOIN gratamits gti ON gti.Controle=ggx.GraTamI'
      'LEFT JOIN prdgruptip pgt ON pgt.Codigo=gg1.PrdGrupTip'
      'WHERE ggx.Controle=:P0'
      '')
    Left = 68
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrItemNOMENIVEL1: TWideStringField
      FieldName = 'NOMENIVEL1'
      Origin = 'gragru1.Nome'
      Size = 30
    end
    object QrItemGraCorCad: TIntegerField
      FieldName = 'GraCorCad'
      Origin = 'gragruc.GraCorCad'
    end
    object QrItemNOMECOR: TWideStringField
      FieldName = 'NOMECOR'
      Origin = 'gracorcad.Nome'
      Size = 30
    end
    object QrItemNOMETAM: TWideStringField
      FieldName = 'NOMETAM'
      Origin = 'gratamits.Nome'
      Size = 5
    end
    object QrItemGraGruX: TIntegerField
      FieldName = 'GraGruX'
      Origin = 'gragrux.Controle'
      Required = True
    end
    object QrItemGraGru1: TIntegerField
      FieldName = 'GraGru1'
      Origin = 'gragrux.GraGru1'
      Required = True
    end
    object QrItemCU_Nivel1: TIntegerField
      FieldName = 'CU_Nivel1'
      Required = True
    end
    object QrItemIPI_Alq: TFloatField
      FieldName = 'IPI_Alq'
    end
    object QrItemMadeBy: TSmallintField
      FieldName = 'MadeBy'
    end
    object QrItemFracio: TSmallintField
      FieldName = 'Fracio'
    end
    object QrItemHowBxaEstq: TSmallintField
      FieldName = 'HowBxaEstq'
    end
    object QrItemGerBxaEstq: TSmallintField
      FieldName = 'GerBxaEstq'
    end
    object QrItemprod_indTot: TSmallintField
      FieldName = 'prod_indTot'
    end
  end
  object DsItem: TDataSource
    DataSet = QrItem
    Left = 96
    Top = 8
  end
  object DsFatPedVol: TDataSource
    DataSet = FmFatPedCab.QrFatPedVol
    Left = 192
    Top = 84
  end
  object DsFatPedCab: TDataSource
    DataSet = FmFatPedCab.QrFatPedCab
    Left = 164
    Top = 84
  end
  object QrPreco: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT pvi.PrecoF, InfAdCuztm,'
      '(QuantP-QuantC-QuantV) QuantF,'
      'PercCustom, MedidaC, MedidaL,'
      'MedidaA, MedidaE, MedOrdem'
      'FROM pedivdaits pvi'
      'LEFT JOIN gragrux ggx ON ggx.Controle=pvi.GraGruX'
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1'
      'WHERE pvi.Controle=:P0'
      '')
    Left = 124
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPrecoPrecoF: TFloatField
      FieldName = 'PrecoF'
      Required = True
    end
    object QrPrecoQuantF: TFloatField
      FieldName = 'QuantF'
      Required = True
    end
    object QrPrecoInfAdCuztm: TIntegerField
      FieldName = 'InfAdCuztm'
    end
    object QrPrecoPercCustom: TFloatField
      FieldName = 'PercCustom'
    end
    object QrPrecoMedidaC: TFloatField
      FieldName = 'MedidaC'
    end
    object QrPrecoMedidaL: TFloatField
      FieldName = 'MedidaL'
    end
    object QrPrecoMedidaA: TFloatField
      FieldName = 'MedidaA'
    end
    object QrPrecoMedidaE: TFloatField
      FieldName = 'MedidaE'
    end
    object QrPrecoMedOrdem: TIntegerField
      FieldName = 'MedOrdem'
    end
  end
  object DsPreco: TDataSource
    DataSet = QrPreco
    Left = 152
    Top = 8
  end
  object QrFator: TmySQLQuery
    Database = Dmod.MyDB
    Left = 180
    Top = 8
  end
  object QrPediGru: TmySQLQuery
    Database = Dmod.MyDB
    Filter = 'QuantF>0'
    AfterScroll = QrPediGruAfterScroll
    SQL.Strings = (
      'SELECT SUM(pvi.QuantP-pvi.QuantC-pvi.QuantV) QuantF,'
      'gti.Codigo GRATAMCAD, pgt.Fracio,'
      'gg1.CodUsu, gg1.Nome, gg1.Nivel1'
      'FROM pedivdaits pvi'
      'LEFT JOIN gragrux   ggx ON ggx.Controle=pvi.GraGruX'
      'LEFT JOIN gratamits gti ON gti.Controle=ggx.GraTamI'
      'LEFT JOIN gragru1   gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN prdgruptip pgt ON pgt.Codigo=gg1.PrdGrupTip'
      'WHERE pvi.Codigo=:P0'
      'GROUP BY gg1.Nivel1')
    Left = 708
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPediGruQuantF: TFloatField
      FieldName = 'QuantF'
    end
    object QrPediGruCodUsu: TIntegerField
      FieldName = 'CodUsu'
      Origin = 'gragru1.CodUsu'
      Required = True
    end
    object QrPediGruNome: TWideStringField
      FieldName = 'Nome'
      Origin = 'gragru1.Nome'
      Size = 30
    end
    object QrPediGruNivel1: TIntegerField
      FieldName = 'Nivel1'
      Origin = 'gragru1.Nivel1'
      Required = True
    end
    object QrPediGruGRATAMCAD: TIntegerField
      FieldName = 'GRATAMCAD'
      Origin = 'gratamits.Codigo'
    end
    object QrPediGruFracio: TSmallintField
      FieldName = 'Fracio'
    end
  end
  object DsPediGru: TDataSource
    DataSet = QrPediGru
    Left = 736
    Top = 8
  end
  object QrCFOP: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT CFOP '
      'FROM fisregcfop'
      'WHERE Codigo=:P0'
      'AND Contribui=:P1'
      'AND Interno=:P2'
      'AND Proprio=:P3'
      '')
    Left = 204
    Top = 144
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
    object QrCFOPCFOP: TWideStringField
      FieldName = 'CFOP'
      Size = 5
    end
  end
  object QrPediGruTot: TmySQLQuery
    Database = Dmod.MyDB
    Filter = 'QuantF>0'
    AfterScroll = QrPediGruAfterScroll
    SQL.Strings = (
      'SELECT SUM(QuantP - QuantC - QuantV) QuantF, SUM(QuantV) QuantV'
      'FROM pedivdaits'
      'WHERE Codigo=:P0'
      'GROUP BY Codigo')
    Left = 764
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPediGruTotQuantV: TFloatField
      FieldName = 'QuantV'
    end
    object QrPediGruTotQuantF: TFloatField
      FieldName = 'QuantF'
    end
  end
end
