object FmSoliComprCab: TFmSoliComprCab
  Left = 368
  Top = 194
  Caption = 'SLC-COMPR-001 :: Solicita'#231#227'o de Compra / Servi'#231'o'
  ClientHeight = 763
  ClientWidth = 1008
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object PainelDados: TPanel
    Left = 0
    Top = 96
    Width = 1008
    Height = 667
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    object Splitter1: TSplitter
      Left = 0
      Top = 464
      Width = 1008
      Height = 5
      Cursor = crVSplit
      Align = alBottom
      ExplicitTop = 253
    end
    object GroupBox9: TGroupBox
      Left = 0
      Top = 0
      Width = 1008
      Height = 133
      Align = alTop
      TabOrder = 0
      object GroupBox10: TGroupBox
        Left = 2
        Top = 97
        Width = 1004
        Height = 34
        Align = alClient
        TabOrder = 0
        object Label54: TLabel
          Left = 8
          Top = 12
          Width = 67
          Height = 13
          Caption = 'Data emiss'#227'o:'
        end
        object Label55: TLabel
          Left = 188
          Top = 12
          Width = 71
          Height = 13
          Caption = 'Data chegada:'
        end
        object Label63: TLabel
          Left = 404
          Top = 12
          Width = 70
          Height = 13
          Caption = 'Pedido cliente:'
        end
        object Label64: TLabel
          Left = 636
          Top = 12
          Width = 50
          Height = 13
          Caption = 'Prioridade:'
        end
        object Label65: TLabel
          Left = 752
          Top = 12
          Width = 101
          Height = 13
          Caption = 'Previs'#227'o entrega:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object DBEdit9: TDBEdit
          Left = 80
          Top = 7
          Width = 105
          Height = 21
          DataField = 'DtaEmiss'
          DataSource = DsSoliComprCab
          TabOrder = 0
        end
        object DBEdit10: TDBEdit
          Left = 264
          Top = 7
          Width = 105
          Height = 21
          DataField = 'DtaEntra'
          DataSource = DsSoliComprCab
          TabOrder = 1
        end
        object DBEdit12: TDBEdit
          Left = 690
          Top = 7
          Width = 53
          Height = 21
          DataField = 'Prioridade'
          DataSource = DsSoliComprCab
          TabOrder = 2
        end
        object DBEdit11: TDBEdit
          Left = 856
          Top = 7
          Width = 129
          Height = 21
          DataField = 'DtaPrevi'
          DataSource = DsSoliComprCab
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 3
        end
        object DBEdit19: TDBEdit
          Left = 480
          Top = 7
          Width = 145
          Height = 21
          DataField = 'PedidoCli'
          DataSource = DsSoliComprCab
          TabOrder = 4
        end
      end
      object Panel1: TPanel
        Left = 2
        Top = 15
        Width = 1004
        Height = 82
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 1
        object Label56: TLabel
          Left = 8
          Top = 0
          Width = 44
          Height = 13
          Caption = 'Empresa:'
        end
        object Label99: TLabel
          Left = 756
          Top = 0
          Width = 43
          Height = 13
          Caption = 'N'#250'mero: '
        end
        object Label100: TLabel
          Left = 840
          Top = 0
          Width = 55
          Height = 13
          Caption = 'Refer'#234'ncia:'
        end
        object Label60: TLabel
          Left = 800
          Top = 40
          Width = 68
          Height = 13
          Caption = 'Data inclus'#227'o:'
          Enabled = False
        end
        object Label61: TLabel
          Left = 908
          Top = 40
          Width = 14
          Height = 13
          Caption = 'ID:'
          Enabled = False
        end
        object Label1: TLabel
          Left = 8
          Top = 40
          Width = 78
          Height = 13
          Caption = 'Centro de custo:'
          FocusControl = DBEdit4
        end
        object DBEdit3: TDBEdit
          Left = 9
          Top = 16
          Width = 53
          Height = 21
          DataField = 'Empresa'
          DataSource = DsSoliComprCab
          TabOrder = 0
        end
        object DBEdit2: TDBEdit
          Left = 64
          Top = 16
          Width = 689
          Height = 21
          DataField = 'NOMEEMP'
          DataSource = DsSoliComprCab
          TabOrder = 1
        end
        object DBEdit1: TDBEdit
          Left = 756
          Top = 16
          Width = 80
          Height = 23
          DataField = 'CodUsu'
          DataSource = DsSoliComprCab
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 2
        end
        object DBEdit82: TDBEdit
          Left = 840
          Top = 16
          Width = 129
          Height = 21
          DataField = 'ReferenPedi'
          DataSource = DsSoliComprCab
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 3
        end
        object DBEdit8: TDBEdit
          Left = 800
          Top = 56
          Width = 105
          Height = 21
          DataField = 'DtaInclu'
          DataSource = DsSoliComprCab
          TabOrder = 4
        end
        object DBEdit22: TDBEdit
          Left = 908
          Top = 56
          Width = 60
          Height = 21
          DataField = 'Codigo'
          DataSource = DsSoliComprCab
          TabOrder = 5
        end
        object DBEdit4: TDBEdit
          Left = 8
          Top = 56
          Width = 56
          Height = 21
          DataField = 'CentroCusto'
          DataSource = DsSoliComprCab
          TabOrder = 6
        end
        object DBEdit5: TDBEdit
          Left = 68
          Top = 56
          Width = 729
          Height = 21
          DataField = 'NO_CENTROCUSTO'
          DataSource = DsSoliComprCab
          TabOrder = 7
        end
      end
    end
    object PageControl1: TPageControl
      Left = 0
      Top = 133
      Width = 1008
      Height = 159
      ActivePage = TabSheet1
      Align = alTop
      TabOrder = 1
      OnChange = PageControl1Change
      object TabSheet1: TTabSheet
        Caption = 'Itens do pedido '
        object Panel4: TPanel
          Left = 0
          Top = 0
          Width = 1000
          Height = 131
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          object Splitter2: TSplitter
            Left = 381
            Top = 0
            Width = 5
            Height = 131
            ExplicitLeft = 804
            ExplicitTop = 20
            ExplicitHeight = 412
          end
          object DBGGru: TdmkDBGrid
            Left = 0
            Top = 0
            Width = 381
            Height = 131
            Align = alLeft
            Columns = <
              item
                Expanded = False
                FieldName = 'CodUsu'
                Title.Caption = 'C'#243'digo'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Nome'
                Title.Caption = 'Descri'#231#227'o'
                Width = 220
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'QuantS'
                Title.Caption = 'Qtd solic.'
                Width = 58
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'PrecoF'
                Title.Caption = 'Pre'#231'o'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ValBru'
                Title.Caption = 'Val. Bru.'
                Width = 72
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ValLiq'
                Title.Caption = 'Val. liq.'
                Width = 72
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Controle'
                Title.Caption = 'ID'
                Visible = True
              end>
            Color = clWindow
            DataSource = DsSoliComprGru
            Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
            TabOrder = 0
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -12
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            Columns = <
              item
                Expanded = False
                FieldName = 'CodUsu'
                Title.Caption = 'C'#243'digo'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Nome'
                Title.Caption = 'Descri'#231#227'o'
                Width = 220
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'QuantS'
                Title.Caption = 'Qtd solic.'
                Width = 58
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'PrecoF'
                Title.Caption = 'Pre'#231'o'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ValBru'
                Title.Caption = 'Val. Bru.'
                Width = 72
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ValLiq'
                Title.Caption = 'Val. liq.'
                Width = 72
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Controle'
                Title.Caption = 'ID'
                Visible = True
              end>
          end
          object PageControl3: TPageControl
            Left = 386
            Top = 0
            Width = 614
            Height = 131
            ActivePage = TabSheet5
            Align = alClient
            MultiLine = True
            TabOrder = 1
            object TabSheet5: TTabSheet
              Caption = ' Quantidade '
              ImageIndex = 2
              object GradeQ: TStringGrid
                Left = 0
                Top = 0
                Width = 606
                Height = 86
                Align = alClient
                ColCount = 2
                DefaultColWidth = 65
                DefaultRowHeight = 18
                FixedCols = 0
                RowCount = 2
                FixedRows = 0
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -12
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goTabs]
                ParentFont = False
                TabOrder = 0
                OnDblClick = GradeQDblClick
                OnDrawCell = GradeQDrawCell
                ColWidths = (
                  65
                  65)
                RowHeights = (
                  18
                  18)
              end
              object StaticText1: TStaticText
                Left = 0
                Top = 86
                Width = 606
                Height = 17
                Align = alBottom
                Alignment = taCenter
                BorderStyle = sbsSunken
                Caption = 
                  'D'#234' um duplo clique na c'#233'lula, coluna ou linha correspondente par' +
                  'a alterar a quantidade.'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlue
                Font.Height = -12
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                ParentFont = False
                TabOrder = 1
                Visible = False
              end
            end
            object TabSheet4: TTabSheet
              Caption = ' Valor unit'#225'rio '
              ImageIndex = 5
              object GradeF: TStringGrid
                Left = 0
                Top = 0
                Width = 992
                Height = 384
                Align = alClient
                ColCount = 2
                DefaultColWidth = 100
                DefaultRowHeight = 18
                FixedCols = 0
                RowCount = 2
                FixedRows = 0
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -12
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goTabs]
                ParentFont = False
                TabOrder = 0
                OnDrawCell = GradeFDrawCell
                ExplicitWidth = 476
                ExplicitHeight = 387
                ColWidths = (
                  100
                  100)
                RowHeights = (
                  18
                  18)
              end
            end
            object TabSheet6: TTabSheet
              Caption = ' Desconto '
              ImageIndex = 6
              object GradeD: TStringGrid
                Left = 0
                Top = 0
                Width = 992
                Height = 367
                Align = alClient
                ColCount = 2
                DefaultColWidth = 100
                DefaultRowHeight = 18
                FixedCols = 0
                RowCount = 2
                FixedRows = 0
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -12
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goTabs]
                ParentFont = False
                TabOrder = 0
                OnDrawCell = GradeDDrawCell
                ExplicitWidth = 476
                ExplicitHeight = 371
                ColWidths = (
                  100
                  100)
                RowHeights = (
                  18
                  18)
              end
              object StaticText3: TStaticText
                Left = 0
                Top = 367
                Width = 544
                Height = 17
                Align = alBottom
                Alignment = taCenter
                BorderStyle = sbsSunken
                Caption = 
                  'D'#234' um duplo clique na c'#233'lula, coluna ou linha correspondente par' +
                  'a incluir / alterar o desconto.'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlue
                Font.Height = -12
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                ParentFont = False
                TabOrder = 1
              end
            end
            object TabSheet7: TTabSheet
              Caption = ' Valor l'#237'quido'
              ImageIndex = 1
              object GradeV: TStringGrid
                Left = 0
                Top = 0
                Width = 992
                Height = 384
                Align = alClient
                ColCount = 2
                DefaultColWidth = 100
                DefaultRowHeight = 18
                FixedCols = 0
                RowCount = 2
                FixedRows = 0
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -12
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goTabs]
                ParentFont = False
                TabOrder = 0
                OnDrawCell = GradeVDrawCell
                ExplicitWidth = 476
                ExplicitHeight = 387
                ColWidths = (
                  100
                  100)
                RowHeights = (
                  18
                  18)
              end
            end
            object TabSheet8: TTabSheet
              Caption = ' C'#243'digos '
              ImageIndex = 3
              object GradeC: TStringGrid
                Left = 0
                Top = 0
                Width = 992
                Height = 367
                Align = alClient
                ColCount = 1
                DefaultColWidth = 65
                DefaultRowHeight = 18
                FixedCols = 0
                RowCount = 1
                FixedRows = 0
                TabOrder = 0
                OnDrawCell = GradeCDrawCell
                ExplicitWidth = 476
                ExplicitHeight = 371
                ColWidths = (
                  65)
                RowHeights = (
                  18)
              end
              object StaticText6: TStaticText
                Left = 0
                Top = 367
                Width = 502
                Height = 17
                Align = alBottom
                Alignment = taCenter
                BorderStyle = sbsSunken
                Caption = 
                  'Para gerar o c'#243'digo, clique na c'#233'lula, coluna ou linha correspon' +
                  'dente na guia "Ativos".'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlue
                Font.Height = -12
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                ParentFont = False
                TabOrder = 1
                Visible = False
              end
            end
            object TabSheet9: TTabSheet
              Caption = ' Ativos '
              object GradeA: TStringGrid
                Left = 0
                Top = 0
                Width = 992
                Height = 367
                Align = alClient
                ColCount = 2
                DefaultColWidth = 65
                DefaultRowHeight = 18
                FixedCols = 0
                RowCount = 2
                FixedRows = 0
                TabOrder = 0
                OnDrawCell = GradeADrawCell
                ExplicitWidth = 476
                ExplicitHeight = 371
                ColWidths = (
                  65
                  65)
                RowHeights = (
                  18
                  18)
              end
              object StaticText2: TStaticText
                Left = 0
                Top = 367
                Width = 475
                Height = 17
                Align = alBottom
                Alignment = taCenter
                BorderStyle = sbsSunken
                Caption = 
                  'Clique na c'#233'lula, coluna ou linha correspondente para ativar / d' +
                  'esativar o produto.'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlue
                Font.Height = -12
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                ParentFont = False
                TabOrder = 1
                Visible = False
              end
            end
            object TabSheet10: TTabSheet
              Caption = ' X '
              ImageIndex = 6
              object GradeX: TStringGrid
                Left = 0
                Top = 0
                Width = 992
                Height = 384
                Align = alClient
                ColCount = 2
                DefaultColWidth = 65
                DefaultRowHeight = 18
                FixedCols = 0
                RowCount = 2
                FixedRows = 0
                TabOrder = 0
                OnDrawCell = GradeXDrawCell
                ExplicitWidth = 476
                ExplicitHeight = 387
                ColWidths = (
                  65
                  65)
                RowHeights = (
                  18
                  18)
              end
            end
          end
        end
      end
      object TabSheet15: TTabSheet
        Caption = ' Observa'#231#245'es '
        ImageIndex = 3
        object DBMemo1: TDBMemo
          Left = 0
          Top = 0
          Width = 1000
          Height = 131
          Align = alClient
          DataField = 'Observa'
          DataSource = DsSoliComprCab
          TabOrder = 0
          ExplicitHeight = 412
        end
      end
    end
    object GBCntrl: TGroupBox
      Left = 0
      Top = 603
      Width = 1008
      Height = 64
      Align = alBottom
      TabOrder = 2
      object Panel5: TPanel
        Left = 2
        Top = 15
        Width = 172
        Height = 47
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 174
        Top = 15
        Width = 134
        Height = 47
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel3: TPanel
        Left = 308
        Top = 15
        Width = 698
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object Panel2: TPanel
          Left = 565
          Top = 0
          Width = 133
          Height = 47
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 0
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
        object BtSolicita: TBitBtn
          Tag = 303
          Left = 4
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Solicita'#231#227'o'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtSolicitaClick
        end
        object BtItens: TBitBtn
          Tag = 10054
          Left = 96
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Itens'
          Enabled = False
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = BtItensClick
        end
        object BtFornece: TBitBtn
          Tag = 10062
          Left = 188
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Forneced.'
          Enabled = False
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = BtForneceClick
        end
        object BtVisual: TBitBtn
          Tag = 338
          Left = 280
          Top = 4
          Width = 90
          Height = 40
          Caption = '&Visual'
          TabOrder = 4
          Visible = False
          OnClick = BtVisualClick
        end
        object BitBtn1: TBitBtn
          Tag = 5
          Left = 372
          Top = 4
          Width = 90
          Height = 40
          Caption = '&Pesquisa'
          TabOrder = 5
          Visible = False
          OnClick = BitBtn1Click
        end
        object BtRecalcula: TBitBtn
          Tag = 18
          Left = 464
          Top = 4
          Width = 90
          Height = 40
          Caption = '&Recalc.'
          TabOrder = 6
          Visible = False
          OnClick = BtRecalculaClick
        end
      end
    end
    object DBGSoliComprFor: TDBGrid
      Left = 0
      Top = 469
      Width = 1008
      Height = 134
      Align = alBottom
      DataSource = DsSoliComprFor
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgMultiSelect, dgTitleClick, dgTitleHotTrack]
      TabOrder = 3
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -12
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'Fornece'
          Title.Caption = 'C'#243'd. Forn.'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_FORNECE'
          Title.Caption = 'Fornecedor'
          Width = 474
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Relevancia'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          Title.Caption = 'Relev'#226'ncia'
          Width = 58
          Visible = True
        end>
    end
  end
  object PainelEdita: TPanel
    Left = 0
    Top = 96
    Width = 1008
    Height = 667
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    Visible = False
    ExplicitLeft = 92
    ExplicitTop = 84
    object PCDadosTop: TPageControl
      Left = 0
      Top = 85
      Width = 1008
      Height = 461
      ActivePage = TabSheet3
      Align = alTop
      TabOrder = 1
      TabStop = False
      Visible = False
      object TabSheet3: TTabSheet
        Caption = ' Dados do Pedido: '
        object Panel6: TPanel
          Left = 0
          Top = 0
          Width = 1000
          Height = 433
          Align = alClient
          ParentBackground = False
          TabOrder = 0
          ExplicitLeft = 96
          ExplicitTop = 8
          object GroupBox2: TGroupBox
            Left = 1
            Top = 1
            Width = 998
            Height = 36
            Align = alTop
            TabOrder = 0
            object Label12: TLabel
              Left = 8
              Top = 12
              Width = 67
              Height = 13
              Caption = 'Data emiss'#227'o:'
            end
            object LaDtaEntra: TLabel
              Left = 188
              Top = 12
              Width = 71
              Height = 13
              Caption = 'Data chegada:'
            end
            object Label15: TLabel
              Left = 636
              Top = 12
              Width = 50
              Height = 13
              Caption = 'Prioridade:'
            end
            object Label14: TLabel
              Left = 752
              Top = 12
              Width = 101
              Height = 13
              Caption = 'Previs'#227'o entrega:'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsBold]
              ParentFont = False
            end
            object LaPedidoCli: TLabel
              Left = 392
              Top = 12
              Width = 70
              Height = 13
              Caption = 'Pedido cliente:'
            end
            object TPDtaEmiss: TdmkEditDateTimePicker
              Left = 80
              Top = 8
              Width = 105
              Height = 21
              Date = 39789.000000000000000000
              Time = 0.688972615738748600
              TabOrder = 0
              ReadOnly = False
              DefaultEditMask = '!99/99/99;1;_'
              AutoApplyEditMask = True
              QryCampo = 'DtaEmiss'
              UpdCampo = 'DtaEmiss'
              UpdType = utYes
              DatePurpose = dmkdpNone
            end
            object TPDtaEntra: TdmkEditDateTimePicker
              Left = 264
              Top = 8
              Width = 105
              Height = 21
              Date = 39789.000000000000000000
              Time = 0.688972615738748600
              TabOrder = 1
              ReadOnly = False
              DefaultEditMask = '!99/99/99;1;_'
              AutoApplyEditMask = True
              QryCampo = 'DtaEntra'
              UpdCampo = 'DtaEntra'
              UpdType = utYes
              DatePurpose = dmkdpNone
            end
            object EdPrioridade: TdmkEdit
              Left = 688
              Top = 8
              Width = 53
              Height = 21
              Alignment = taRightJustify
              TabOrder = 3
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 2
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '0'
              ValMax = '99'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '00'
              QryCampo = 'Prioridade'
              UpdCampo = 'Prioridade'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
            end
            object TPDtaPrevi: TdmkEditDateTimePicker
              Left = 856
              Top = 8
              Width = 129
              Height = 21
              Date = 39789.000000000000000000
              Time = 0.688972615738748600
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsBold]
              ParentFont = False
              TabOrder = 4
              ReadOnly = False
              DefaultEditMask = '!99/99/99;1;_'
              AutoApplyEditMask = True
              QryCampo = 'DtaPrevi'
              UpdCampo = 'DtaPrevi'
              UpdType = utYes
              DatePurpose = dmkdpNone
            end
            object EdPedidoCli: TdmkEdit
              Left = 468
              Top = 8
              Width = 145
              Height = 21
              TabOrder = 2
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 2
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '0'
              ValMax = '99'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              QryCampo = 'PedidoCli'
              UpdCampo = 'PedidoCli'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
          end
        end
      end
      object TabSheet14: TTabSheet
        Caption = ' Observa'#231#245'es '
        ImageIndex = 3
        object MeObserva: TdmkMemo
          Left = 0
          Top = 0
          Width = 1000
          Height = 433
          Align = alClient
          TabOrder = 0
          QryCampo = 'Observa'
          UpdCampo = 'Observa'
          UpdType = utYes
        end
      end
    end
    object GBRodaPe: TGroupBox
      Left = 0
      Top = 603
      Width = 1008
      Height = 64
      Align = alBottom
      TabOrder = 2
      object Panel12: TPanel
        Left = 2
        Top = 15
        Width = 1004
        Height = 47
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object PnSaiDesis: TPanel
          Left = 859
          Top = 0
          Width = 145
          Height = 47
          Align = alRight
          BevelOuter = bvNone
          TabOrder = 1
          object BtDesiste: TBitBtn
            Tag = 15
            Left = 7
            Top = 2
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Caption = '&Desiste'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            NumGlyphs = 2
            ParentFont = False
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtDesisteClick
          end
        end
        object BtConfirma: TBitBtn
          Tag = 14
          Left = 13
          Top = 2
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Confirma'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtConfirmaClick
        end
      end
    end
    object Panel14: TPanel
      Left = 0
      Top = 0
      Width = 1008
      Height = 85
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object GBDadosTop: TGroupBox
        Left = 77
        Top = 0
        Width = 931
        Height = 85
        Align = alClient
        TabOrder = 1
        Visible = False
        object Label9: TLabel
          Left = 8
          Top = 4
          Width = 44
          Height = 13
          Caption = 'Empresa:'
        end
        object Label8: TLabel
          Left = 688
          Top = 4
          Width = 61
          Height = 13
          Caption = 'N'#250'mero: [F4]'
        end
        object Label7: TLabel
          Left = 848
          Top = 44
          Width = 14
          Height = 13
          Caption = 'ID:'
          Enabled = False
        end
        object Label6: TLabel
          Left = 740
          Top = 44
          Width = 68
          Height = 13
          Caption = 'Data inclus'#227'o:'
          Enabled = False
        end
        object Label98: TLabel
          Left = 772
          Top = 4
          Width = 55
          Height = 13
          Caption = 'Refer'#234'ncia:'
        end
        object Label29: TLabel
          Left = 8
          Top = 43
          Width = 78
          Height = 13
          Caption = 'Centro de custo:'
        end
        object SbCentroCusto: TSpeedButton
          Left = 714
          Top = 58
          Width = 23
          Height = 23
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '...'
          OnClick = SbCentroCustoClick
        end
        object EdEmpresa: TdmkEditCB
          Left = 8
          Top = 20
          Width = 52
          Height = 21
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          OnChange = EdEmpresaChange
          DBLookupComboBox = CBEmpresa
          IgnoraDBLookupComboBox = False
          AutoSetIfOnlyOneReg = setregOnlyManual
        end
        object CBEmpresa: TdmkDBLookupComboBox
          Left = 63
          Top = 20
          Width = 622
          Height = 21
          KeyField = 'Filial'
          ListField = 'NOMEFILIAL'
          TabOrder = 1
          dmkEditCB = EdEmpresa
          UpdType = utYes
          LocF7SQLMasc = '$#'
          LocF7PreDefProc = f7pNone
        end
        object EdCodUsu: TdmkEdit
          Left = 688
          Top = 20
          Width = 80
          Height = 21
          Alignment = taRightJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 2
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 6
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '000000'
          QryCampo = 'CodUsu'
          UpdCampo = 'CodUsu'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          OnEnter = EdCodUsuEnter
          OnKeyDown = EdCodUsuKeyDown
        end
        object EdCodigo: TdmkEdit
          Left = 848
          Top = 60
          Width = 56
          Height = 21
          TabStop = False
          Alignment = taRightJustify
          Enabled = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ReadOnly = True
          TabOrder = 5
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'Codigo'
          UpdCampo = 'Codigo'
          UpdType = utInc
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
        object TPDtaInclu: TdmkEditDateTimePicker
          Left = 740
          Top = 60
          Width = 105
          Height = 21
          Date = 39789.000000000000000000
          Time = 0.688972615738748600
          Enabled = False
          TabOrder = 4
          TabStop = False
          ReadOnly = False
          DefaultEditMask = '!99/99/99;1;_'
          AutoApplyEditMask = True
          QryCampo = 'DtaInclu'
          UpdCampo = 'DtaInclu'
          UpdType = utYes
          DatePurpose = dmkdpNone
        end
        object EdReferenPedi: TdmkEdit
          Left = 772
          Top = 20
          Width = 133
          Height = 21
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 3
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          QryCampo = 'ReferenPedi'
          UpdCampo = 'ReferenPedi'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object DBEdREF_NIV1: TDBEdit
          Left = 8
          Top = 59
          Width = 113
          Height = 21
          TabStop = False
          DataField = 'REF_NIV1'
          DataSource = DsCentroCusto
          ReadOnly = True
          TabOrder = 6
        end
        object EdCentroCusto: TdmkEditCB
          Left = 124
          Top = 59
          Width = 56
          Height = 21
          Alignment = taRightJustify
          TabOrder = 7
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'CentroCusto'
          UpdCampo = 'CentroCusto'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          OnChange = EdCentroCustoChange
          DBLookupComboBox = CBCentroCusto
          IgnoraDBLookupComboBox = False
          AutoSetIfOnlyOneReg = setregOnlyManual
        end
        object CBCentroCusto: TdmkDBLookupComboBox
          Left = 184
          Top = 59
          Width = 529
          Height = 21
          Color = clWhite
          KeyField = 'Codigo'
          ListField = 'Nome'
          ListSource = DsCentroCusto
          TabOrder = 8
          dmkEditCB = EdCentroCusto
          QryCampo = 'CentroCusto'
          UpdType = utYes
          LocF7SQLMasc = '$#'
          LocF7PreDefProc = f7pNone
        end
      end
      object RGEntSai: TdmkRadioGroup
        Left = 0
        Top = 0
        Width = 77
        Height = 85
        Align = alLeft
        Caption = '  Movimento: '
        Items.Strings = (
          'Compra'
          'Venda')
        TabOrder = 0
        OnClick = RGEntSaiClick
        UpdCampo = 'EntSai'
        UpdType = utYes
        OldValor = 0
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 12
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 288
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 5
        Top = 5
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
        OnClick = SbImprimeClick
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 45
        Top = 5
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 85
        Top = 5
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 125
        Top = 5
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 165
        Top = 5
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
      object BtGraGruN: TBitBtn
        Tag = 30
        Left = 245
        Top = 5
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 5
        OnClick = BtGraGruNClick
      end
      object BtFisRegCad: TBitBtn
        Tag = 10104
        Left = 205
        Top = 5
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 6
        OnClick = BtFisRegCadClick
      end
    end
    object GB_M: TGroupBox
      Left = 288
      Top = 0
      Width = 672
      Height = 52
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 388
        Height = 32
        Caption = 'Solicita'#231#227'o de Compra / Servi'#231'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 388
        Height = 32
        Caption = 'Solicita'#231#227'o de Compra / Servi'#231'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 388
        Height = 32
        Caption = 'Solicita'#231#227'o de Compra / Servi'#231'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 52
    Width = 1008
    Height = 44
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel11: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 12
        Height = 17
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 12
        Height = 17
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object DsSoliComprCab: TDataSource
    DataSet = QrSoliComprCab
    Left = 28
    Top = 116
  end
  object QrSoliComprCab: TMySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrSoliComprCabBeforeOpen
    AfterOpen = QrSoliComprCabAfterOpen
    BeforeClose = QrSoliComprCabBeforeClose
    AfterScroll = QrSoliComprCabAfterScroll
    SQL.Strings = (
      'SELECT pvd.Codigo, pvd.CodUsu, pvd.Empresa, pvd.Cliente,'
      'pvd.DtaEmiss, pvd.DtaEntra, pvd.DtaInclu, pvd.DtaPrevi,'
      'pvd.Prioridade, pvd.CondicaoPG, pvd.Moeda,'
      'pvd.Situacao, pvd.TabelaPrc, pvd.MotivoSit, pvd.LoteProd,'
      'pvd.PedidoCli, pvd.FretePor, pvd.Transporta, pvd.Redespacho,'
      'pvd.Observa, pvd.EntSai, '
      'pvd.ReferenPedi,'
      'IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NOMEEMP,'
      'IF(tra.Tipo=0, tra.RazaoSocial, tra.NOME) NOMETRANSP,'
      'IF(red.Tipo=0, red.RazaoSocial, red.NOME) NOMEREDESP, '
      'emp.Filial, '
      'tra.CodUsu CODUSU_TRA, '
      'red.CodUsu CODUSU_RED,'
      'mot.CodUsu CODUSU_MOT, '
      'emp.CodUsu CODUSU_FOR'
      'FROM solicomprcab pvd'
      'LEFT JOIN entidades  emp ON emp.Codigo=pvd.Empresa'
      'LEFT JOIN entidades  tra ON tra.Codigo=pvd.Transporta'
      'LEFT JOIN entidades  red ON red.Codigo=pvd.Redespacho'
      'LEFT JOIN motivos    mot ON mot.Codigo=pvd.MotivoSit'
      ''
      ''
      'WHERE pvd.Codigo > -1000'
      'AND pvd.CodUsu > 0 '
      'AND pvd.Empresa IN (-11)')
    Left = 28
    Top = 68
    object QrSoliComprCabCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'solicomprcab.Codigo'
      Required = True
    end
    object QrSoliComprCabCodUsu: TIntegerField
      FieldName = 'CodUsu'
      Origin = 'solicomprcab.CodUsu'
      Required = True
    end
    object QrSoliComprCabEmpresa: TIntegerField
      FieldName = 'Empresa'
      Origin = 'solicomprcab.Empresa'
      Required = True
    end
    object QrSoliComprCabCliente: TIntegerField
      FieldName = 'Cliente'
      Origin = 'solicomprcab.Cliente'
      Required = True
    end
    object QrSoliComprCabDtaEmiss: TDateField
      FieldName = 'DtaEmiss'
      Origin = 'solicomprcab.DtaEmiss'
      Required = True
    end
    object QrSoliComprCabDtaEntra: TDateField
      FieldName = 'DtaEntra'
      Origin = 'solicomprcab.DtaEntra'
      Required = True
    end
    object QrSoliComprCabDtaInclu: TDateField
      FieldName = 'DtaInclu'
      Origin = 'solicomprcab.DtaInclu'
      Required = True
    end
    object QrSoliComprCabDtaPrevi: TDateField
      FieldName = 'DtaPrevi'
      Origin = 'solicomprcab.DtaPrevi'
      Required = True
    end
    object QrSoliComprCabPrioridade: TSmallintField
      FieldName = 'Prioridade'
      Origin = 'solicomprcab.Prioridade'
      Required = True
    end
    object QrSoliComprCabCondicaoPG: TIntegerField
      FieldName = 'CondicaoPG'
      Origin = 'solicomprcab.CondicaoPG'
      Required = True
    end
    object QrSoliComprCabMoeda: TIntegerField
      FieldName = 'Moeda'
      Origin = 'solicomprcab.Moeda'
      Required = True
    end
    object QrSoliComprCabSituacao: TIntegerField
      FieldName = 'Situacao'
      Origin = 'solicomprcab.Situacao'
      Required = True
    end
    object QrSoliComprCabTabelaPrc: TIntegerField
      FieldName = 'TabelaPrc'
      Origin = 'solicomprcab.TabelaPrc'
      Required = True
    end
    object QrSoliComprCabMotivoSit: TIntegerField
      FieldName = 'MotivoSit'
      Origin = 'solicomprcab.MotivoSit'
      Required = True
    end
    object QrSoliComprCabLoteProd: TIntegerField
      FieldName = 'LoteProd'
      Origin = 'solicomprcab.LoteProd'
      Required = True
    end
    object QrSoliComprCabPedidoCli: TWideStringField
      FieldName = 'PedidoCli'
      Origin = 'solicomprcab.PedidoCli'
    end
    object QrSoliComprCabFretePor: TSmallintField
      FieldName = 'FretePor'
      Origin = 'solicomprcab.FretePor'
      Required = True
    end
    object QrSoliComprCabTransporta: TIntegerField
      FieldName = 'Transporta'
      Origin = 'solicomprcab.Transporta'
      Required = True
    end
    object QrSoliComprCabRedespacho: TIntegerField
      FieldName = 'Redespacho'
      Origin = 'solicomprcab.Redespacho'
      Required = True
    end
    object QrSoliComprCabObserva: TWideMemoField
      FieldName = 'Observa'
      Origin = 'solicomprcab.Observa'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrSoliComprCabEntSai: TSmallintField
      FieldName = 'EntSai'
      Origin = 'solicomprcab.EntSai'
      Required = True
    end
    object QrSoliComprCabReferenPedi: TWideStringField
      FieldName = 'ReferenPedi'
      Origin = 'solicomprcab.ReferenPedi'
      Size = 60
    end
    object QrSoliComprCabNOMEEMP: TWideStringField
      FieldName = 'NOMEEMP'
      Size = 100
    end
    object QrSoliComprCabNOMETRANSP: TWideStringField
      FieldName = 'NOMETRANSP'
      Size = 100
    end
    object QrSoliComprCabNOMEREDESP: TWideStringField
      FieldName = 'NOMEREDESP'
      Size = 100
    end
    object QrSoliComprCabFilial: TIntegerField
      FieldName = 'Filial'
      Origin = 'entidades.Filial'
    end
    object QrSoliComprCabCODUSU_TRA: TIntegerField
      FieldName = 'CODUSU_TRA'
      Origin = 'entidades.CodUsu'
    end
    object QrSoliComprCabCODUSU_RED: TIntegerField
      FieldName = 'CODUSU_RED'
      Origin = 'entidades.CodUsu'
    end
    object QrSoliComprCabCODUSU_MOT: TIntegerField
      FieldName = 'CODUSU_MOT'
      Origin = 'motivos.CodUsu'
    end
    object QrSoliComprCabCODUSU_FOR: TIntegerField
      FieldName = 'CODUSU_FOR'
      Origin = 'entidades.CodUsu'
    end
    object QrSoliComprCabNO_CENTROCUSTO: TWideStringField
      FieldName = 'NO_CENTROCUSTO'
      Size = 60
    end
    object QrSoliComprCabCentroCusto: TIntegerField
      FieldName = 'CentroCusto'
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 140
    Top = 68
  end
  object PMSolicita: TPopupMenu
    OnPopup = PMSolicitaPopup
    Left = 364
    Top = 412
    object Incluinovasolicitao1: TMenuItem
      Caption = '&Inclui nova solicita'#231#227'o'
      OnClick = Incluinovasolicitao1Click
    end
    object Alterasolicitaoatual1: TMenuItem
      Caption = '&Altera solicita'#231#227'o atual'
      OnClick = Alterasolicitaoatual1Click
    end
    object Excluisolicitaoatual1: TMenuItem
      Caption = '&Exclui solicita'#231#227'o atual'
      OnClick = Excluisolicitaoatual1Click
    end
    object N4: TMenuItem
      Caption = '-'
    end
    object Duplicarsolicitaoatual1: TMenuItem
      Caption = '&Duplicar solicita'#231#227'o atual'
      OnClick = Duplicarsolicitaoatual1Click
    end
  end
  object PMItens: TPopupMenu
    Left = 468
    Top = 412
    object Incluinovositensdegrupo1: TMenuItem
      Caption = '&Inclui novos itens por &Grade'
      OnClick = Incluinovositensdegrupo1Click
    end
    object IncluinovositensporLeitura1: TMenuItem
      Caption = 'Inclui novos itens por &Leitura'
      Visible = False
      OnClick = IncluinovositensporLeitura1Click
    end
    object N1: TMenuItem
      Caption = '-'
      OnClick = N1Click
    end
    object AlteraExcluiIncluiitemselecionado1: TMenuItem
      Caption = '&Altera / Exclui / Inclui item selecionado'
      OnClick = AlteraExcluiIncluiitemselecionado1Click
    end
    object N2: TMenuItem
      Caption = '-'
    end
  end
  object VuEmpresa: TdmkValUsu
    dmkEditCB = EdEmpresa
    Panel = PainelEdita
    QryCampo = 'Empresa'
    UpdCampo = 'Empresa'
    RefCampo = 'Codigo'
    UpdType = utYes
    Left = 168
    Top = 68
  end
  object QrSoliComprGru: TMySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrSoliComprGruAfterOpen
    BeforeClose = QrSoliComprGruBeforeClose
    AfterScroll = QrSoliComprGruAfterScroll
    SQL.Strings = (
      'SELECT pvi.Controle, SUM(QuantP) QuantP, SUM(ValLiq) ValLiq, '
      'SUM(Customizad) ItensCustomizados,'
      'gti.Codigo GRATAMCAD,'
      'gg1.CodUsu, gg1.Nome, gg1.Nivel1, pgt.Fracio'
      'FROM pedivdaits pvi'
      'LEFT JOIN gragrux   ggx ON ggx.Controle=pvi.GraGruX'
      'LEFT JOIN gratamits gti ON gti.Controle=ggx.GraTamI'
      'LEFT JOIN gragru1   gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN prdgruptip   pgt ON pgt.Codigo=gg1.PrdGrupTip'
      'WHERE pvi.Codigo=:P0'
      'GROUP BY gg1.Nivel1'
      'ORDER BY pci.Controle')
    Left = 84
    Top = 68
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSoliComprGruCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrSoliComprGruNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
    object QrSoliComprGruNivel1: TIntegerField
      FieldName = 'Nivel1'
    end
    object QrSoliComprGruGRATAMCAD: TIntegerField
      FieldName = 'GRATAMCAD'
    end
    object QrSoliComprGruQuantS: TFloatField
      FieldName = 'QuantS'
    end
    object QrSoliComprGruValLiq: TFloatField
      FieldName = 'ValLiq'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrSoliComprGruItensCustomizados: TFloatField
      FieldName = 'ItensCustomizados'
    end
    object QrSoliComprGruFracio: TSmallintField
      FieldName = 'Fracio'
    end
    object QrSoliComprGruControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrSoliComprGruValBru: TFloatField
      FieldName = 'ValBru'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrSoliComprGruPrecoF: TFloatField
      FieldName = 'PrecoF'
      DisplayFormat = '#,###,###,##0.000000'
    end
  end
  object DsSoliComprGru: TDataSource
    DataSet = QrSoliComprGru
    Left = 88
    Top = 120
  end
  object PMCustom: TPopupMenu
    OnPopup = PMCustomPopup
    Left = 520
    Top = 368
    object ItemprodutoCustomizvel1: TMenuItem
      Caption = '&Item (produto) Customiz'#225'vel'
      object IncluinovoitemprodutoCustomizvel1: TMenuItem
        Caption = 'Inclui novo item (produto) &Customiz'#225'vel'
      end
      object Alteraitemprodutoselecionado1: TMenuItem
        Caption = '&Altera item (produto) selecionado'
        OnClick = Alteraitemprodutoselecionado1Click
      end
      object Excluiitemprodutoselecionado1: TMenuItem
        Caption = '&Exclui item (produto) selecionado'
        OnClick = Excluiitemprodutoselecionado1Click
      end
      object N3: TMenuItem
        Caption = '-'
      end
      object AtualizaValoresdoitem1: TMenuItem
        Caption = '&Atualiza &Valores do item'
        OnClick = AtualizaValoresdoitem1Click
      end
    end
    object Partedoitemprodutoselecionado1: TMenuItem
      Caption = '&Parte do item (produto) selecionado'
      object Adicionapartesaoitemselecionado1: TMenuItem
        Caption = '&Adiciona partes ao produto selecionado'
        Enabled = False
        OnClick = Adicionapartesaoitemselecionado1Click
      end
      object Excluipartedoprodutoselecionado1: TMenuItem
        Caption = '&Exclui parte do produto selecionado'
        Enabled = False
        OnClick = Excluipartedoprodutoselecionado1Click
      end
    end
  end
  object QrItsN: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT gg1.CodUsu CU_NIVEL1, gg1.Nome NO_NIVEL1,'
      'gti.Nome NO_TAM, gcc.Nome NO_COR,   '
      'pvi.QuantS, pvi.PrecoR, pvi.DescoP,'
      'pvi.PrecoF, pvi.ValBru, pvi.ValLiq, 0 KGT,'
      ''
      'pvi.vProd, pvi.vFrete, pvi.vSeg, pvi.vOutro,'
      'pvi.vDesc, pvi.vBC'
      ''
      'FROM solicomprits pvi'
      'LEFT JOIN gragrux ggx ON ggx.Controle=pvi.GraGrux'
      'LEFT JOIN gratamits gti ON gti.Controle=ggx.GraTamI'
      'LEFT JOIN gragruc ggc ON ggc.Controle=ggx.GraGruC'
      'LEFT JOIN gracorcad gcc ON gcc.Codigo=ggc.GraCorCad'
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1'
      'WHERE pvi.Codigo=:P0'
      'AND pvi.Customizad=0')
    Left = 888
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrItsNCU_NIVEL1: TIntegerField
      FieldName = 'CU_NIVEL1'
      Required = True
    end
    object QrItsNNO_NIVEL1: TWideStringField
      FieldName = 'NO_NIVEL1'
      Size = 30
    end
    object QrItsNNO_TAM: TWideStringField
      FieldName = 'NO_TAM'
      Size = 5
    end
    object QrItsNNO_COR: TWideStringField
      FieldName = 'NO_COR'
      Size = 30
    end
    object QrItsNQuantS: TFloatField
      FieldName = 'QuantS'
    end
    object QrItsNPrecoR: TFloatField
      FieldName = 'PrecoR'
    end
    object QrItsNDescoP: TFloatField
      FieldName = 'DescoP'
    end
    object QrItsNPrecoF: TFloatField
      FieldName = 'PrecoF'
    end
    object QrItsNValBru: TFloatField
      FieldName = 'ValBru'
    end
    object QrItsNValLiq: TFloatField
      FieldName = 'ValLiq'
    end
    object QrItsNKGT: TLargeintField
      FieldName = 'KGT'
      Required = True
    end
    object QrItsNvProd: TFloatField
      FieldName = 'vProd'
      Required = True
    end
    object QrItsNvFrete: TFloatField
      FieldName = 'vFrete'
      Required = True
    end
    object QrItsNvSeg: TFloatField
      FieldName = 'vSeg'
      Required = True
    end
    object QrItsNvOutro: TFloatField
      FieldName = 'vOutro'
      Required = True
    end
    object QrItsNvDesc: TFloatField
      FieldName = 'vDesc'
      Required = True
    end
    object QrItsNvBC: TFloatField
      FieldName = 'vBC'
      Required = True
    end
  end
  object frxDsItsN: TfrxDBDataset
    UserName = 'frxDsItsN'
    CloseDataSource = False
    FieldAliases.Strings = (
      'CU_NIVEL1=CU_NIVEL1'
      'NO_NIVEL1=NO_NIVEL1'
      'NO_TAM=NO_TAM'
      'NO_COR=NO_COR'
      'QuantS=QuantS'
      'PrecoR=PrecoR'
      'DescoP=DescoP'
      'PrecoF=PrecoF'
      'ValBru=ValBru'
      'ValLiq=ValLiq'
      'KGT=KGT'
      'vProd=vProd'
      'vFrete=vFrete'
      'vSeg=vSeg'
      'vOutro=vOutro'
      'vDesc=vDesc'
      'vBC=vBC')
    DataSet = QrItsN
    BCDToCurrency = False
    DataSetOptions = []
    Left = 888
    Top = 60
  end
  object frxDsSoliComprCab: TfrxDBDataset
    UserName = 'frxDsSoliComprCab'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Codigo=Codigo'
      'CodUsu=CodUsu'
      'Empresa=Empresa'
      'Cliente=Cliente'
      'DtaEmiss=DtaEmiss'
      'DtaEntra=DtaEntra'
      'DtaInclu=DtaInclu'
      'DtaPrevi=DtaPrevi'
      'NOMEEMP=NOMEEMP'
      'NOMECLI=NOMECLI'
      'CIDADECLI=CIDADECLI'
      'NOMEUF=NOMEUF'
      'Filial=Filial'
      'Prioridade=Prioridade'
      'CondicaoPG=CondicaoPG'
      'Moeda=Moeda'
      'Situacao=Situacao'
      'TabelaPrc=TabelaPrc'
      'MotivoSit=MotivoSit'
      'LoteProd=LoteProd'
      'PedidoCli=PedidoCli'
      'FretePor=FretePor'
      'Transporta=Transporta'
      'Redespacho=Redespacho'
      'RegrFiscal=RegrFiscal'
      'DesoAces_V=DesoAces_V'
      'DesoAces_P=DesoAces_P'
      'Frete_V=Frete_V'
      'Frete_P=Frete_P'
      'Seguro_V=Seguro_V'
      'Seguro_P=Seguro_P'
      'TotalQtd=TotalQtd'
      'Total_Vlr=Total_Vlr'
      'Total_Des=Total_Des'
      'Total_Tot=Total_Tot'
      'Observa=Observa'
      'NOMETABEPRCCAD=NOMETABEPRCCAD'
      'NOMEMOEDA=NOMEMOEDA'
      'NOMECONDICAOPG=NOMECONDICAOPG'
      'NOMEMOTIVO=NOMEMOTIVO'
      'NOMESITUACAO=NOMESITUACAO'
      'NOMECARTEMIS=NOMECARTEMIS'
      'NOMEFRETEPOR=NOMEFRETEPOR'
      'NOMETRANSP=NOMETRANSP'
      'NOMEREDESP=NOMEREDESP'
      'NOMEACC=NOMEACC'
      'NOMEFISREGCAD=NOMEFISREGCAD'
      'NOMEMODELONF=NOMEMODELONF'
      'Represen=Represen'
      'ComisFat=ComisFat'
      'ComisRec=ComisRec'
      'CartEmis=CartEmis'
      'AFP_Sit=AFP_Sit'
      'AFP_Per=AFP_Per'
      'CODUSU_CLI=CODUSU_CLI'
      'CODUSU_ACC=CODUSU_ACC'
      'CODUSU_TRA=CODUSU_TRA'
      'CODUSU_RED=CODUSU_RED'
      'CODUSU_MOT=CODUSU_MOT'
      'CODUSU_TPC=CODUSU_TPC'
      'CODUSU_MDA=CODUSU_MDA'
      'CODUSU_PPC=CODUSU_PPC'
      'CODUSU_FRC=CODUSU_FRC'
      'MODELO_NF=MODELO_NF'
      'MedDDSimpl=MedDDSimpl'
      'MedDDReal=MedDDReal'
      'ValLiq=ValLiq'
      'QuantP=QuantP'
      'MaxDesco=MaxDesco'
      'JurosMes=JurosMes'
      'DescoMax=DescoMax'
      'EntSai=EntSai'
      'ENTSAI_TXT=ENTSAI_TXT'
      'SHOW_COD_FILIAL=SHOW_COD_FILIAL'
      'SHOW_COD_CLIFOR=SHOW_COD_CLIFOR'
      'SHOW_TXT_FILIAL=SHOW_TXT_FILIAL'
      'SHOW_TXT_CLIFOR=SHOW_TXT_CLIFOR'
      'CODUSU_FOR=CODUSU_FOR'
      'FILIAL_CLI=FILIAL_CLI'
      'idDest=idDest'
      'indFinal=indFinal'
      'indPres=indPres'
      'indSinc=indSinc'
      'finNFe=finNFe'
      'RetiradaUsa=RetiradaUsa'
      'RetiradaEnti=RetiradaEnti'
      'EntregaUsa=EntregaUsa'
      'EntregaEnti=EntregaEnti'
      'NOMEENTRETIRADA=NOMEENTRETIRADA'
      'NOMEENTENTREGA=NOMEENTENTREGA'
      'L_Ativo=L_Ativo'
      'TipoMov=TipoMov'
      'ReferenPedi=ReferenPedi'
      'Desco_P=Desco_P'
      'Desco_V=Desco_V')
    DataSet = QrSoliComprCab
    BCDToCurrency = False
    DataSetOptions = []
    Left = 28
    Top = 200
  end
  object QrSoliComprIts: TMySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrSoliComprGruAfterOpen
    BeforeClose = QrSoliComprGruBeforeClose
    AfterScroll = QrSoliComprGruAfterScroll
    SQL.Strings = (
      'SELECT pvi.*'
      'FROM pedivdaits pvi'
      'WHERE pvi.Codigo=:P0')
    Left = 28
    Top = 248
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSoliComprItsControle: TIntegerField
      FieldName = 'Controle'
    end
  end
  object QrEntregaCli: TMySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrEntregaCliCalcFields
    SQL.Strings = (
      'SELECT en.L_CNPJ, en.L_CPF,'
      'en.L_Nome,'
      'en.LLograd  Lograd,'
      'lll.Nome    NOMELOGRAD,'
      'en.LRua     RUA,'
      'en.LNumero  NUMERO,'
      'en.LCompl   COMPL,'
      'en.LBairro  BAIRRO,'
      'en.LCodMunici,'
      'mun.Nome NO_MUNICI, '
      'ufl.Nome    NOMEUF,'
      'en.LCEP     CEP,'
      'en.LCodiPais    CodiPais,'
      'pai.Nome NO_PAIS, '
      'en.LEndeRef ENDEREF,'
      'en.LTel     TE1,'
      'en.LEmail     Email,'
      'en.L_IE'
      'FROM entidades en'
      'LEFT JOIN ufs ufl ON ufl.Codigo=en.LUF'
      'LEFT JOIN listalograd lll ON lll.Codigo=en.LLograd'
      
        'LEFT JOIN locbdermall.dtb_munici mun ON mun.Codigo = IF(en.Tipo=' +
        '0, en.ECodMunici, en.PCodMunici) '
      
        'LEFT JOIN locbdermall.bacen_pais pai ON pai.Codigo = IF(en.Tipo=' +
        '0, en.ECodiPais, en.PCodiPais) '
      'WHERE en.Codigo=:P0')
    Left = 700
    Top = 176
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrEntregaCliE_ALL: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'E_ALL'
      Size = 256
      Calculated = True
    end
    object QrEntregaCliCNPJ_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CNPJ_TXT'
      Size = 30
      Calculated = True
    end
    object QrEntregaCliNOME_TIPO_DOC: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOME_TIPO_DOC'
      Size = 10
      Calculated = True
    end
    object QrEntregaCliTE1_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TE1_TXT'
      Size = 40
      Calculated = True
    end
    object QrEntregaCliNUMERO_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NUMERO_TXT'
      Size = 30
      Calculated = True
    end
    object QrEntregaCliCEP_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CEP_TXT'
      Calculated = True
    end
    object QrEntregaCliL_CNPJ: TWideStringField
      FieldName = 'L_CNPJ'
      Size = 14
    end
    object QrEntregaCliL_CPF: TWideStringField
      FieldName = 'L_CPF'
      Size = 18
    end
    object QrEntregaCliL_Nome: TWideStringField
      FieldName = 'L_Nome'
      Size = 60
    end
    object QrEntregaCliLograd: TSmallintField
      FieldName = 'Lograd'
    end
    object QrEntregaCliNOMELOGRAD: TWideStringField
      FieldName = 'NOMELOGRAD'
      Size = 15
    end
    object QrEntregaCliRUA: TWideStringField
      FieldName = 'RUA'
      Size = 60
    end
    object QrEntregaCliNUMERO: TIntegerField
      FieldName = 'NUMERO'
    end
    object QrEntregaCliCOMPL: TWideStringField
      FieldName = 'COMPL'
      Size = 60
    end
    object QrEntregaCliBAIRRO: TWideStringField
      FieldName = 'BAIRRO'
      Size = 60
    end
    object QrEntregaCliLCodMunici: TIntegerField
      FieldName = 'LCodMunici'
    end
    object QrEntregaCliNO_MUNICI: TWideStringField
      FieldName = 'NO_MUNICI'
      Size = 100
    end
    object QrEntregaCliNOMEUF: TWideStringField
      FieldName = 'NOMEUF'
      Required = True
      Size = 2
    end
    object QrEntregaCliCEP: TIntegerField
      FieldName = 'CEP'
    end
    object QrEntregaCliCodiPais: TIntegerField
      FieldName = 'CodiPais'
    end
    object QrEntregaCliNO_PAIS: TWideStringField
      FieldName = 'NO_PAIS'
      Size = 100
    end
    object QrEntregaCliENDEREF: TWideStringField
      FieldName = 'ENDEREF'
      Size = 100
    end
    object QrEntregaCliTE1: TWideStringField
      FieldName = 'TE1'
    end
    object QrEntregaCliEmail: TWideStringField
      FieldName = 'Email'
      Size = 100
    end
    object QrEntregaCliL_IE: TWideStringField
      FieldName = 'L_IE'
    end
  end
  object QrEntregaEnti: TMySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrEntregaEntiCalcFields
    SQL.Strings = (
      
        'SELECT en.Codigo, en.Cadastro, ENatal, PNatal, Tipo, Respons1, R' +
        'espons2, '
      'ENumero, PNumero, ELograd, PLograd, ECEP, PCEP, '
      'IF(en.Tipo=0, en.RazaoSocial, en.Nome   ) NOME_ENT, '
      'IF(en.Tipo=0, en.Fantasia   , en.Apelido) NO_2_ENT, '
      'IF(en.Tipo=0, en.CNPJ       , en.CPF    ) CNPJ_CPF, '
      'IF(en.Tipo=0, en.IE         , en.RG     ) IE_RG, '
      'IF(en.Tipo=0, en.NIRE       , ""        ) NIRE_, '
      'IF(en.Tipo=0, en.ERua       , en.PRua   ) RUA, '
      'IF(en.Tipo=0, en.ESite       , en.PSite   ) SITE, '
      'IF(en.Tipo=0, en.ENumero    , en.PNumero) NUMERO, '
      'IF(en.Tipo=0, en.ECompl     , en.PCompl ) COMPL, '
      'IF(en.Tipo=0, en.EBairro    , en.PBairro) BAIRRO, '
      'mun.Nome CIDADE, '
      'IF(en.Tipo=0, lle.Nome      , llp.Nome  ) NOMELOGRAD, '
      'IF(en.Tipo=0, ufe.Nome      , ufp.Nome  ) NOMEUF, '
      'pai.Nome Pais, '
      'IF(en.Tipo=0, en.ELograd    , en.PLograd) Lograd, '
      'IF(en.Tipo=0, en.ECEP       , en.PCEP   ) CEP, '
      'IF(en.Tipo=0, en.ETe1       , en.PTe1   ) TE1, '
      'IF(en.Tipo=0, en.EFax       , en.PFax   ) FAX, '
      'IF(en.Tipo=0, en.EEmail     , en.PEmail ) EMAIL, '
      'IF(en.Tipo=0, lle.Trato     , llp.Trato ) TRATO, '
      'IF(en.Tipo=0, en.EEndeRef   , en.PEndeRef  ) ENDEREF, '
      'IF(en.Tipo=0, en.ECodMunici , en.PCodMunici) + 0.000 CODMUNICI, '
      'IF(en.Tipo=0, en.ECodiPais  , en.PCodiPais ) + 0.000 CODPAIS, '
      'RG, SSP, DataRG '
      'FROM entidades en '
      'LEFT JOIN ufs ufe ON ufe.Codigo=en.EUF '
      'LEFT JOIN ufs ufp ON ufp.Codigo=en.PUF '
      'LEFT JOIN listalograd lle ON lle.Codigo=en.ELograd '
      'LEFT JOIN listalograd llp ON llp.Codigo=en.PLograd '
      
        'LEFT JOIN locbdermall.dtb_munici mun ON mun.Codigo = IF(en.Tipo=' +
        '0, en.ECodMunici, en.PCodMunici) '
      
        'LEFT JOIN locbdermall.bacen_pais pai ON pai.Codigo = IF(en.Tipo=' +
        '0, en.ECodiPais, en.PCodiPais) '
      'ORDER BY NOME_ENT')
    Left = 612
    Top = 176
    object QrEntregaEntiCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEntregaEntiCadastro: TDateField
      FieldName = 'Cadastro'
    end
    object QrEntregaEntiENatal: TDateField
      FieldName = 'ENatal'
    end
    object QrEntregaEntiPNatal: TDateField
      FieldName = 'PNatal'
    end
    object QrEntregaEntiTipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrEntregaEntiRespons1: TWideStringField
      FieldName = 'Respons1'
      Size = 100
    end
    object QrEntregaEntiRespons2: TWideStringField
      FieldName = 'Respons2'
      Size = 100
    end
    object QrEntregaEntiENumero: TIntegerField
      FieldName = 'ENumero'
    end
    object QrEntregaEntiPNumero: TIntegerField
      FieldName = 'PNumero'
    end
    object QrEntregaEntiELograd: TSmallintField
      FieldName = 'ELograd'
    end
    object QrEntregaEntiPLograd: TSmallintField
      FieldName = 'PLograd'
    end
    object QrEntregaEntiECEP: TIntegerField
      FieldName = 'ECEP'
    end
    object QrEntregaEntiPCEP: TIntegerField
      FieldName = 'PCEP'
    end
    object QrEntregaEntiNOME_ENT: TWideStringField
      FieldName = 'NOME_ENT'
      Size = 100
    end
    object QrEntregaEntiNO_2_ENT: TWideStringField
      FieldName = 'NO_2_ENT'
      Size = 60
    end
    object QrEntregaEntiCNPJ_CPF: TWideStringField
      FieldName = 'CNPJ_CPF'
      Size = 18
    end
    object QrEntregaEntiIE_RG: TWideStringField
      FieldName = 'IE_RG'
    end
    object QrEntregaEntiNIRE_: TWideStringField
      FieldName = 'NIRE_'
      Size = 15
    end
    object QrEntregaEntiRUA: TWideStringField
      FieldName = 'RUA'
      Size = 60
    end
    object QrEntregaEntiSITE: TWideStringField
      FieldName = 'SITE'
      Size = 100
    end
    object QrEntregaEntiNUMERO: TIntegerField
      FieldName = 'NUMERO'
    end
    object QrEntregaEntiCOMPL: TWideStringField
      FieldName = 'COMPL'
      Size = 60
    end
    object QrEntregaEntiBAIRRO: TWideStringField
      FieldName = 'BAIRRO'
      Size = 60
    end
    object QrEntregaEntiCIDADE: TWideStringField
      FieldName = 'CIDADE'
      Size = 100
    end
    object QrEntregaEntiNOMELOGRAD: TWideStringField
      FieldName = 'NOMELOGRAD'
      Size = 15
    end
    object QrEntregaEntiNOMEUF: TWideStringField
      FieldName = 'NOMEUF'
      Size = 2
    end
    object QrEntregaEntiPais: TWideStringField
      FieldName = 'Pais'
      Size = 100
    end
    object QrEntregaEntiLograd: TSmallintField
      FieldName = 'Lograd'
      Required = True
    end
    object QrEntregaEntiCEP: TIntegerField
      FieldName = 'CEP'
    end
    object QrEntregaEntiTE1: TWideStringField
      FieldName = 'TE1'
    end
    object QrEntregaEntiFAX: TWideStringField
      FieldName = 'FAX'
    end
    object QrEntregaEntiEMAIL: TWideStringField
      FieldName = 'EMAIL'
      Size = 100
    end
    object QrEntregaEntiTRATO: TWideStringField
      FieldName = 'TRATO'
      Size = 10
    end
    object QrEntregaEntiENDEREF: TWideStringField
      FieldName = 'ENDEREF'
      Size = 100
    end
    object QrEntregaEntiCODMUNICI: TFloatField
      FieldName = 'CODMUNICI'
      Required = True
    end
    object QrEntregaEntiCODPAIS: TFloatField
      FieldName = 'CODPAIS'
      Required = True
    end
    object QrEntregaEntiRG: TWideStringField
      FieldName = 'RG'
      Size = 15
    end
    object QrEntregaEntiSSP: TWideStringField
      FieldName = 'SSP'
      Size = 10
    end
    object QrEntregaEntiDataRG: TDateField
      FieldName = 'DataRG'
    end
    object QrEntregaEntiIE: TWideStringField
      FieldName = 'IE'
    end
    object QrEntregaEntiE_ALL: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'E_ALL'
      Size = 512
      Calculated = True
    end
    object QrEntregaEntiCNPJ_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CNPJ_TXT'
      Size = 30
      Calculated = True
    end
    object QrEntregaEntiNOME_TIPO_DOC: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOME_TIPO_DOC'
      Size = 10
      Calculated = True
    end
    object QrEntregaEntiTE1_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TE1_TXT'
      Size = 40
      Calculated = True
    end
    object QrEntregaEntiFAX_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'FAX_TXT'
      Size = 40
      Calculated = True
    end
    object QrEntregaEntiNUMERO_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NUMERO_TXT'
      Size = 30
      Calculated = True
    end
    object QrEntregaEntiCEP_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CEP_TXT'
      Calculated = True
    end
  end
  object PMRegrFiscal: TPopupMenu
    Left = 685
    Top = 389
    object Estrio1: TMenuItem
      Caption = '&Estri'#231#227'o'
      OnClick = Estrio1Click
    end
    object Cadastro1: TMenuItem
      Caption = '&Cadastro'
      OnClick = Cadastro1Click
    end
  end
  object QrUsuario: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT pvd.UserCad, pvd.UserAlt,'
      
        'IF(se1.Funcionario<>0, IF(en1.Tipo=0, en1.RazaoSocial, en1.Nome)' +
        ', '
      '  se1.Login) NO_LANCADOR,'
      
        'IF(se2.Funcionario<>0, IF(en2.Tipo=0, en2.RazaoSocial, en2.Nome)' +
        ', '
      '  se2.Login) NO_ALTERADOR'
      'FROM pedivda pvd'
      'LEFT JOIN senhas se1 ON se1.Numero=pvd.UserCad'
      'LEFT JOIN entidades en1 ON en1.Codigo=se1.Funcionario'
      'LEFT JOIN senhas se2 ON se2.Numero=pvd.UserAlt'
      'LEFT JOIN entidades en2 ON en2.Codigo=se2.Funcionario'
      'WHERE pvd.Codigo=3')
    Left = 428
    Top = 208
    object QrUsuarioUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrUsuarioUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrUsuarioNO_LANCADOR: TWideStringField
      FieldName = 'NO_LANCADOR'
      Size = 255
    end
    object QrUsuarioNO_ALTERADOR: TWideStringField
      FieldName = 'NO_ALTERADOR'
    end
  end
  object frxDsUsuario: TfrxDBDataset
    UserName = 'frxDsUsuario'
    CloseDataSource = False
    FieldAliases.Strings = (
      'UserCad=UserCad'
      'UserAlt=UserAlt'
      'NO_LANCADOR=NO_LANCADOR'
      'NO_ALTERADOR=NO_ALTERADOR')
    DataSet = QrUsuario
    BCDToCurrency = False
    DataSetOptions = []
    Left = 428
    Top = 264
  end
  object QrSumIts: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(QuantS) QuantS, '
      'SUM(DescoV) DescoV,'
      'SUM(ValLiq) ValLiq, SUM(ValBru) ValBru, '
      'SUM(Customizad) ItensCustomizados,'
      ''
      'SUM(vProd) vProd, SUM(vFrete) vFrete, '
      'SUM(vSeg) vSeg, SUM(vOutro) vOutro, '
      'SUM(vDesc) vDesc, SUM(vBC) vBC'
      ''
      ''
      'FROM pedivdaits pvi '
      'WHERE pvi.Codigo=15')
    Left = 148
    Top = 300
    object QrSumItsQuantS: TFloatField
      FieldName = 'QuantS'
    end
    object QrSumItsDescoV: TFloatField
      FieldName = 'DescoV'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrSumItsValLiq: TFloatField
      FieldName = 'ValLiq'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrSumItsValBru: TFloatField
      FieldName = 'ValBru'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrSumItsItensCustomizados: TFloatField
      FieldName = 'ItensCustomizados'
    end
    object QrSumItsvProd: TFloatField
      FieldName = 'vProd'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrSumItsvFrete: TFloatField
      FieldName = 'vFrete'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrSumItsvSeg: TFloatField
      FieldName = 'vSeg'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrSumItsvOutro: TFloatField
      FieldName = 'vOutro'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrSumItsvDesc: TFloatField
      FieldName = 'vDesc'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrSumItsvBC: TFloatField
      FieldName = 'vBC'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
  end
  object DsSumIts: TDataSource
    DataSet = QrSumIts
    Left = 148
    Top = 352
  end
  object QrSoliComprFor: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT scf.*,'
      'IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_FORNECE'
      'FROM solicomprfor scf'
      'LEFT JOIN entidades frn ON frn.Codigo=scf.Fornece')
    Left = 852
    Top = 364
    object QrSoliComprForCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrSoliComprForFornece: TIntegerField
      FieldName = 'Fornece'
      Required = True
    end
    object QrSoliComprForRelevancia: TSmallintField
      FieldName = 'Relevancia'
      Required = True
    end
    object QrSoliComprForNO_FORNECE: TWideStringField
      FieldName = 'NO_FORNECE'
      Size = 100
    end
  end
  object DsSoliComprFor: TDataSource
    DataSet = QrSoliComprFor
    Left = 852
    Top = 416
  end
  object PmFornece: TPopupMenu
    Left = 292
    Top = 436
    object Incluiforneceor1: TMenuItem
      Caption = '&Adiciona fornecedor'
      OnClick = Incluiforneceor1Click
    end
    object Removefornecedor1: TMenuItem
      Caption = '&Remove fornecedor'
      OnClick = Removefornecedor1Click
    end
  end
  object QrFor: TMySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrForCalcFields
    SQL.Strings = (
      'SELECT scf.Relevancia, en.Codigo, Tipo, en.CodUsu, IE, '
      
        'CASE WHEN en.Tipo=0 THEN en.RazaoSocial ELSE en.Nome     END NOM' +
        'E_ENT, '
      
        'CASE WHEN en.Tipo=0 THEN en.CNPJ        ELSE en.CPF      END CNP' +
        'J_CPF, '
      
        'CASE WHEN en.Tipo=0 THEN en.IE          ELSE en.RG       END IE_' +
        'RG, '
      
        'CASE WHEN en.Tipo=0 THEN en.ERua        ELSE en.PRua     END RUA' +
        ', '
      
        'CASE WHEN en.Tipo=0 THEN en.ENumero + 0.000 ELSE en.PNumero + 0.' +
        '000 END NUMERO,'
      
        'CASE WHEN en.Tipo=0 THEN en.ECompl      ELSE en.PCompl   END COM' +
        'PL, '
      
        'CASE WHEN en.Tipo=0 THEN en.EBairro     ELSE en.PBairro  END BAI' +
        'RRO, '
      
        'CASE WHEN en.Tipo=0 THEN mue.Nome       ELSE mup.Nome  END CIDAD' +
        'E, '
      
        'CASE WHEN en.Tipo=0 THEN en.EUF + 0.000     ELSE en.PUF + 0.000 ' +
        ' END UF, '
      
        'CASE WHEN en.Tipo=0 THEN lle.Nome       ELSE llp.Nome    END NOM' +
        'ELOGRAD, '
      
        'CASE WHEN en.Tipo=0 THEN ufe.Nome       ELSE ufp.Nome    END NOM' +
        'EUF, '
      
        'CASE WHEN en.Tipo=0 THEN pae.Nome       ELSE pap.NOme    END Pai' +
        's, '
      
        '/*CASE WHEN en.Tipo=0 THEN en.ELograd     ELSE en.PLograd END Lo' +
        'grad,*/'
      
        'CASE WHEN en.Tipo=0 THEN en.ECEP + 0.000 ELSE en.PCEP + 0.000 EN' +
        'D CEP,'
      
        'CASE WHEN en.Tipo=0 THEN en.EEndeRef    ELSE en.PEndeRef END END' +
        'EREF, '
      
        'CASE WHEN en.Tipo=0 THEN en.ETe1        ELSE en.PTe1     END TE1' +
        ','
      
        'CASE WHEN en.Tipo=0 THEN en.ECel        ELSE en.PCel     END CE1' +
        ', '
      
        'CASE WHEN en.Tipo=0 THEN en.EFax        ELSE en.PFax     END FAX' +
        ', '
      
        'IF(en.Tipo=0, "CNPJ", "CPF") CAD_FEDERAL, IF(en.Tipo=0, "I.E.", ' +
        '"RG") CAD_ESTADUAL,'
      'en.L_Ativo'
      'FROM entidades en '
      'LEFT JOIN locbdermall.dtb_munici mue ON mue.Codigo=en.ECodMunici'
      'LEFT JOIN locbdermall.dtb_munici mup ON mup.Codigo=en.PCodMunici'
      'LEFT JOIN locbdermall.bacen_pais pae ON pae.Codigo=en.ECodiPais'
      'LEFT JOIN locbdermall.bacen_pais pap ON pap.Codigo=en.PCodiPais'
      'LEFT JOIN ufs ufe ON ufe.Codigo=en.EUF'
      'LEFT JOIN ufs ufp ON ufp.Codigo=en.PUF'
      'LEFT JOIN listalograd lle ON lle.Codigo=en.ELograd'
      'LEFT JOIN listalograd llp ON llp.Codigo=en.PLograd'
      'LEFT JOIN solicomprfor scf ON scf.Fornece=en.Codigo'
      'WHERE scf.Codigo>0'
      'ORDER BY scf.Relevancia DESC, NOME_ENT')
    Left = 748
    Top = 4
    object QrForE_ALL: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'E_ALL'
      Size = 256
      Calculated = True
    end
    object QrForCNPJ_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CNPJ_TXT'
      Size = 30
      Calculated = True
    end
    object QrForNOME_TIPO_DOC: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOME_TIPO_DOC'
      Size = 10
      Calculated = True
    end
    object QrForTE1_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TE1_TXT'
      Size = 40
      Calculated = True
    end
    object QrForFAX_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'FAX_TXT'
      Size = 40
      Calculated = True
    end
    object QrForNUMERO_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NUMERO_TXT'
      Size = 30
      Calculated = True
    end
    object QrForCEP_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CEP_TXT'
      Calculated = True
    end
    object QrForCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrForTipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrForCodUsu: TIntegerField
      FieldName = 'CodUsu'
      Required = True
    end
    object QrForNOME_ENT: TWideStringField
      FieldName = 'NOME_ENT'
      Size = 100
    end
    object QrForCNPJ_CPF: TWideStringField
      FieldName = 'CNPJ_CPF'
      Size = 18
    end
    object QrForIE_RG: TWideStringField
      FieldName = 'IE_RG'
    end
    object QrForRUA: TWideStringField
      FieldName = 'RUA'
      Size = 30
    end
    object QrForCOMPL: TWideStringField
      FieldName = 'COMPL'
      Size = 30
    end
    object QrForBAIRRO: TWideStringField
      FieldName = 'BAIRRO'
      Size = 30
    end
    object QrForCIDADE: TWideStringField
      FieldName = 'CIDADE'
      Size = 25
    end
    object QrForNOMELOGRAD: TWideStringField
      FieldName = 'NOMELOGRAD'
      Size = 10
    end
    object QrForNOMEUF: TWideStringField
      FieldName = 'NOMEUF'
      Size = 2
    end
    object QrForPais: TWideStringField
      FieldName = 'Pais'
    end
    object QrForENDEREF: TWideStringField
      FieldName = 'ENDEREF'
      Size = 100
    end
    object QrForTE1: TWideStringField
      FieldName = 'TE1'
    end
    object QrForFAX: TWideStringField
      FieldName = 'FAX'
    end
    object QrForIE: TWideStringField
      FieldName = 'IE'
    end
    object QrForCAD_FEDERAL: TWideStringField
      FieldName = 'CAD_FEDERAL'
      Required = True
      Size = 4
    end
    object QrForCAD_ESTADUAL: TWideStringField
      FieldName = 'CAD_ESTADUAL'
      Required = True
      Size = 4
    end
    object QrForIE_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'IE_TXT'
      Size = 50
      Calculated = True
    end
    object QrForNUMERO: TFloatField
      FieldName = 'NUMERO'
    end
    object QrForCEP: TFloatField
      FieldName = 'CEP'
    end
    object QrForUF: TFloatField
      FieldName = 'UF'
    end
    object QrForRelevancia: TSmallintField
      FieldName = 'Relevancia'
    end
    object QrForL_Ativo: TSmallintField
      FieldName = 'L_Ativo'
      Required = True
    end
    object QrForCE1: TWideStringField
      FieldName = 'CE1'
    end
    object QrForCE1_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CE1_TXT'
      Size = 40
      Calculated = True
    end
  end
  object frxDsFor: TfrxDBDataset
    UserName = 'frxDsFor'
    CloseDataSource = False
    FieldAliases.Strings = (
      'E_ALL=E_ALL'
      'CNPJ_TXT=CNPJ_TXT'
      'NOME_TIPO_DOC=NOME_TIPO_DOC'
      'TE1_TXT=TE1_TXT'
      'FAX_TXT=FAX_TXT'
      'NUMERO_TXT=NUMERO_TXT'
      'CEP_TXT=CEP_TXT'
      'Codigo=Codigo'
      'Tipo=Tipo'
      'CodUsu=CodUsu'
      'NOME_ENT=NOME_ENT'
      'CNPJ_CPF=CNPJ_CPF'
      'IE_RG=IE_RG'
      'RUA=RUA'
      'COMPL=COMPL'
      'BAIRRO=BAIRRO'
      'CIDADE=CIDADE'
      'NOMELOGRAD=NOMELOGRAD'
      'NOMEUF=NOMEUF'
      'Pais=Pais'
      'ENDEREF=ENDEREF'
      'TE1=TE1'
      'FAX=FAX'
      'IE=IE'
      'CAD_FEDERAL=CAD_FEDERAL'
      'CAD_ESTADUAL=CAD_ESTADUAL'
      'IE_TXT=IE_TXT'
      'NUMERO=NUMERO'
      'CEP=CEP'
      'UF=UF'
      'Relevancia=Relevancia'
      'L_Ativo=L_Ativo'
      'CE1=CE1'
      'CE1_TXT=CE1_TXT')
    DataSet = QrFor
    BCDToCurrency = False
    DataSetOptions = []
    Left = 748
    Top = 56
  end
  object frxSLC_COMPRA_001_A: TfrxReport
    Version = '2022.1'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39956.739524675900000000
    ReportOptions.LastChange = 39956.739524675900000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      
        '  MeVARF_CODI_FRX.Text := Copy(Report.Name, 4, Length(Report.Nam' +
        'e));'
      '  if <MeuLogo3x1Existe> = True then '
      '    Picture1.LoadFromFile(<MeuLogo3x1Caminho>);'
      'end.')
    OnGetValue = frxPED_VENDA_001_01GetValue
    Left = 280
    Top = 60
    Datasets = <
      item
        DataSet = frxDsItsN
        DataSetName = 'frxDsItsN'
      end
      item
        DataSet = frxDsSoliComprCab
        DataSetName = 'frxDsSoliComprCab'
      end
      item
        DataSet = frxDsUsuario
        DataSetName = 'frxDsUsuario'
      end
      item
        DataSet = DModG.frxDsEndereco
        DataSetName = 'frxDsEndereco'
      end
      item
        DataSet = frxDsFor
        DataSetName = 'frxDsFor'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 15.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
      MirrorMode = []
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 102.047244094488200000
        Top = 18.897650000000000000
        Width = 699.213050000000000000
        object Shape3: TfrxShapeView
          AllowVectorExport = True
          Top = 22.677180000000000000
          Width = 548.031850000000000000
          Height = 75.590600000000000000
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Shape1: TfrxShapeView
          AllowVectorExport = True
          Left = 548.031850000000000000
          Top = 22.677180000000000000
          Width = 151.181200000000000000
          Height = 75.590600000000000000
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo13: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Top = 22.677180000000000000
          Width = 332.598640000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsEndereco."NO_2_ENT"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line1: TfrxLineView
          AllowVectorExport = True
          Left = 548.031850000000000000
          Top = 41.574830000000000000
          Width = 151.181200000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo14: TfrxMemoView
          AllowVectorExport = True
          Left = 158.740260000000000000
          Top = 45.354360000000000000
          Width = 381.732530000000000000
          Height = 30.236215590000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsEndereco."E_ALL"]')
          ParentFont = False
        end
        object Memo15: TfrxMemoView
          AllowVectorExport = True
          Left = 548.031850000000000000
          Top = 22.677180000000000000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[Date]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo16: TfrxMemoView
          AllowVectorExport = True
          Left = 548.031850000000000000
          Top = 41.574830000000000000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page] de [TotalPages]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo6: TfrxMemoView
          AllowVectorExport = True
          Width = 1028.032160000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            
              'SOLICITA'#199#195'O DE COMPRA N'#186' [FormatFloat('#39'000000'#39', <frxDsSoliComprC' +
              'ab."CodUsu">)]  [frxDsSoliComprCab."ReferenPedi"]')
          ParentFont = False
          Formats = <
            item
            end
            item
            end>
        end
        object frxDsEnderecoTE1: TfrxMemoView
          AllowVectorExport = True
          Left = 396.850650000000000000
          Top = 22.677180000000000000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsEndereco."TE1_TXT"]')
          ParentFont = False
        end
        object Memo64: TfrxMemoView
          AllowVectorExport = True
          Left = 158.740260000000000000
          Top = 75.590600000000000000
          Width = 381.732530000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_TIPOMOV_NUMPED]')
          ParentFont = False
        end
        object Memo84: TfrxMemoView
          AllowVectorExport = True
          Left = 340.157700000000000000
          Top = 22.677180000000000000
          Width = 56.692950000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Telefone:')
          ParentFont = False
          VAlign = vaCenter
        end
        object Picture1: TfrxPictureView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Top = 41.574830000000000000
          Width = 147.401574800000000000
          Height = 49.133890000000000000
          Visible = False
          Frame.Typ = []
          HightQuality = False
          Transparent = False
          TransparentColor = clWhite
        end
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 13.228346460000000000
        Top = 570.709030000000000000
        Width = 699.213050000000000000
        object Memo1: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Width = 411.968770000000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - software customizado')
          ParentFont = False
          VAlign = vaCenter
        end
        object MeVARF_CODI_FRX: TfrxMemoView
          AllowVectorExport = True
          Left = 381.732530000000000000
          Width = 321.260050000000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'VARF_CODI_FRX')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object GroupHeader1: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118110240000000000
        Top = 181.417440000000000000
        Width = 699.213050000000000000
        Condition = 'frxDsItsN."KGT"'
        object Memo40: TfrxMemoView
          AllowVectorExport = True
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'C'#243'digo')
          ParentFont = False
          WordWrap = False
        end
        object MeTitD: TfrxMemoView
          AllowVectorExport = True
          Left = 128.504020000000000000
          Width = 570.708661417322800000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Descri'#231#227'o')
          ParentFont = False
        end
        object MeTitA: TfrxMemoView
          AllowVectorExport = True
          Left = 52.913420000000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Qtde.')
          ParentFont = False
          WordWrap = False
        end
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118110240000000000
        Top = 219.212740000000000000
        Width = 699.213050000000000000
        DataSet = frxDsItsN
        DataSetName = 'frxDsItsN'
        RowCount = 0
        object Memo2: TfrxMemoView
          AllowVectorExport = True
          Width = 52.913385830000000000
          Height = 15.118110240000000000
          DataField = 'CU_NIVEL1'
          DataSet = frxDsItsN
          DataSetName = 'frxDsItsN'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsItsN."CU_NIVEL1"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo3: TfrxMemoView
          AllowVectorExport = True
          Left = 128.504020000000000000
          Width = 570.708661417322800000
          Height = 15.118110240000000000
          DataField = 'NO_NIVEL1'
          DataSet = frxDsItsN
          DataSetName = 'frxDsItsN'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsItsN."NO_NIVEL1"]')
          ParentFont = False
        end
        object Memo11: TfrxMemoView
          AllowVectorExport = True
          Left = 52.913420000000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          DataField = 'QuantS'
          DataSet = frxDsItsN
          DataSetName = 'frxDsItsN'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsItsN."QuantS"]')
          ParentFont = False
          WordWrap = False
        end
      end
      object GroupFooter1: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 37.795300000000000000
        Top = 257.008040000000000000
        Width = 699.213050000000000000
        object Memo74: TfrxMemoView
          AllowVectorExport = True
          Width = 52.913297950000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'TOTAL ')
          ParentFont = False
        end
        object Memo43: TfrxMemoView
          AllowVectorExport = True
          Left = 52.913420000000000000
          Width = 75.590560940000000000
          Height = 15.118110240000000000
          DataSet = frxDsItsN
          DataSetName = 'frxDsItsN'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsItsN."QuantS">,MasterData1)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo17: TfrxMemoView
          AllowVectorExport = True
          Top = 22.677180000000000000
          Width = 699.212927950000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Poss'#237'veis fornecedores:')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo18: TfrxMemoView
          AllowVectorExport = True
          Left = 128.504020000000000000
          Width = 570.708907950000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'TOTAL ')
          ParentFont = False
        end
      end
      object ReportSummary1: TfrxReportSummary
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 7.559060000000000000
        Top = 540.472790000000000000
        Width = 699.213050000000000000
      end
      object MasterData3: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 26.456710000000000000
        Top = 408.189240000000000000
        Width = 699.213050000000000000
        RowCount = 1
        object Memo70: TfrxMemoView
          AllowVectorExport = True
          Top = 3.779530000000000000
          Width = 699.212610630000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            
              'Digitado por: [frxDsUsuario."NO_LANCADOR"]     Alterado por: [fr' +
              'xDsUsuario."NO_ALTERADOR"]')
          ParentFont = False
        end
      end
      object MasterData4: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 22.677180000000000000
        Top = 457.323130000000000000
        Width = 699.213050000000000000
        DataSet = frxDsSoliComprCab
        DataSetName = 'frxDsSoliComprCab'
        RowCount = 0
        Stretched = True
        object Memo71: TfrxMemoView
          AllowVectorExport = True
          Width = 699.213050000000000000
          Height = 18.897650000000000000
          StretchMode = smMaxHeight
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Observa'#231#227'o: [frxDsSoliComprCab."Observa"]')
          ParentFont = False
        end
      end
      object MasterData5: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 68.031540000000000000
        Top = 317.480520000000000000
        Width = 699.213050000000000000
        DataSet = frxDsFor
        DataSetName = 'frxDsFor'
        RowCount = 0
        object Shape2: TfrxShapeView
          AllowVectorExport = True
          Top = 3.779530000000000000
          Width = 699.213050000000000000
          Height = 64.252010000000000000
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo5: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Top = 7.559060000000000000
          Width = 71.811070000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[VARF_TIPOMOV_TIPOENTI]')
          ParentFont = False
        end
        object frxMemoView1: TfrxMemoView
          AllowVectorExport = True
          Left = 75.590600000000000000
          Top = 7.559060000000000000
          Width = 472.441250000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsFor."CodUsu"] - [frxDsFor."NOME_ENT"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo4: TfrxMemoView
          AllowVectorExport = True
          Left = 75.590600000000000000
          Top = 26.456710000000000000
          Width = 302.362400000000000000
          Height = 37.795275590000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsFor."E_ALL"]')
          ParentFont = False
        end
        object Memo30: TfrxMemoView
          AllowVectorExport = True
          Left = 381.732530000000000000
          Top = 26.456710000000000000
          Width = 49.133858267716540000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsFor."CAD_FEDERAL"]:')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo29: TfrxMemoView
          AllowVectorExport = True
          Left = 427.086890000000000000
          Top = 26.456710000000000000
          Width = 120.944881890000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsFor."CNPJ_TXT"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo39: TfrxMemoView
          AllowVectorExport = True
          Left = 381.732530000000000000
          Top = 45.354360000000000000
          Width = 49.133858267716540000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsFor."CAD_ESTADUAL"]:')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo38: TfrxMemoView
          AllowVectorExport = True
          Left = 427.086890000000000000
          Top = 45.354360000000000000
          Width = 120.944881890000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsFor."IE_TXT"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo42: TfrxMemoView
          AllowVectorExport = True
          Left = 551.811380000000000000
          Top = 7.559060000000000000
          Width = 49.133858267716540000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Telefone:')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo41: TfrxMemoView
          AllowVectorExport = True
          Left = 600.945270000000000000
          Top = 7.559060000000000000
          Width = 90.708661420000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsFor."TE1_TXT"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo19: TfrxMemoView
          AllowVectorExport = True
          Left = 9.448818900000000000
          Top = 28.346454250000000000
          Width = 60.472440940000000000
          Height = 17.007874020000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Relev'#226'ncia:')
          ParentFont = False
        end
        object Memo20: TfrxMemoView
          AllowVectorExport = True
          Left = 9.448818897637795000
          Top = 45.354360000000000000
          Width = 60.472440940000000000
          Height = 17.007874020000000000
          DataField = 'Relevancia'
          DataSet = frxDsFor
          DataSetName = 'frxDsFor'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsFor."Relevancia"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo21: TfrxMemoView
          AllowVectorExport = True
          Left = 551.811380000000000000
          Top = 26.456710000000000000
          Width = 49.133858267716540000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Celular:')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo31: TfrxMemoView
          AllowVectorExport = True
          Left = 600.945270000000000000
          Top = 26.456710000000000000
          Width = 90.708661420000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsFor."CE1_TXT"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo32: TfrxMemoView
          AllowVectorExport = True
          Left = 551.811380000000000000
          Top = 45.354360000000000000
          Width = 49.133858267716540000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'Fone/fax:')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo33: TfrxMemoView
          AllowVectorExport = True
          Left = 600.945270000000000000
          Top = 45.354360000000000000
          Width = 90.708661420000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsFor."FAX_TXT"]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
    end
  end
  object QrCentroCusto: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT '
      'CONCAT('
      '  LPAD(niv4.Ordem, 1, "0"), ".",'
      '  LPAD(niv3.Ordem, 2, "0"), ".",'
      '  LPAD(niv2.Ordem, 3, "0"), ".",'
      '  LPAD(niv1.Ordem, 4, "0") '
      ') REF_NIV1,'
      'niv1.Codigo, niv1.Nome,'
      'niv1.Ativo '
      'FROM centrocust5 niv5 '
      'LEFT JOIN centrocust4 niv4 ON niv5.Codigo=niv4.NivSup'
      'LEFT JOIN centrocust3 niv3 ON niv4.Codigo=niv3.NivSup'
      'LEFT JOIN centrocust2 niv2 ON niv3.Codigo=niv2.NivSup'
      'LEFT JOIN centrocusto niv1 ON niv2.Codigo=niv1.NivSup'
      'WHERE niv1.Ativo=1'
      'AND niv5.PagRec=1'
      'ORDER BY niv1.Nome')
    Left = 852
    Top = 468
    object QrCentroCustoREF_NIV1: TWideStringField
      FieldName = 'REF_NIV1'
      Size = 13
    end
    object QrCentroCustoCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCentroCustoNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
    object QrCentroCustoAtivo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object DsCentroCusto: TDataSource
    DataSet = QrCentroCusto
    Left = 852
    Top = 516
  end
end
