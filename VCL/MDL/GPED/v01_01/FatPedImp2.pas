unit FatPedImp2;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkDBLookupComboBox, dmkEdit,
  dmkEditCB, dmkEditDateTimePicker, dmkDBGrid, mySQLDbTables, DmkDAC_PF;

type
  TFmFatPedImp2 = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    Panel5: TPanel;
    Panel6: TPanel;
    LaPVIEmp: TLabel;
    LaPVICli: TLabel;
    LaPVIRep: TLabel;
    LaPVIPrd: TLabel;
    EdPVIPrd: TdmkEditCB;
    EdPVIRep: TdmkEditCB;
    EdPVICli: TdmkEditCB;
    EdPVIEmp: TdmkEditCB;
    CBPVIEmp: TdmkDBLookupComboBox;
    CBPVICli: TdmkDBLookupComboBox;
    CBPVIRep: TdmkDBLookupComboBox;
    CBPVIPrd: TdmkDBLookupComboBox;
    Panel9: TPanel;
    Panel10: TPanel;
    DBGrid2: TDBGrid;
    DBGrid1: TdmkDBGrid;
    GroupBox2: TGroupBox;
    TPIncluIni: TdmkEditDateTimePicker;
    TPIncluFim: TdmkEditDateTimePicker;
    CkIncluIni: TCheckBox;
    CkIncluFim: TCheckBox;
    RGOrdemIts: TRadioGroup;
    RGAgrupa: TRadioGroup;
    RGOrdemAgr_: TRadioGroup;
    DsPVICliCad: TDataSource;
    QrPVICliCad: TmySQLQuery;
    QrPVICliCadCodUsu: TIntegerField;
    QrPVICliCadCodigo: TIntegerField;
    QrPVICliCadNOMEENT: TWideStringField;
    QrPVIRepCad: TmySQLQuery;
    QrPVIRepCadCodUsu: TIntegerField;
    QrPVIRepCadCodigo: TIntegerField;
    QrPVIRepCadNOMEENT: TWideStringField;
    DsPVIRepCad: TDataSource;
    QrPVIPrdCad: TmySQLQuery;
    QrPVIPrdCadCodusu: TIntegerField;
    QrPVIPrdCadNivel1: TIntegerField;
    QrPVIPrdCadNome: TWideStringField;
    DsPVIPrdCad: TDataSource;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenPVICliCad();
    procedure ReopenPVIRepCad();
    procedure ReopenPVIPrdCad();
  public
    { Public declarations }
  end;

  var
  FmFatPedImp2: TFmFatPedImp2;

implementation

uses UnMyObjects, Module, ModuleGeral;

{$R *.DFM}

procedure TFmFatPedImp2.BtOKClick(Sender: TObject);
begin
  //
end;

procedure TFmFatPedImp2.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmFatPedImp2.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmFatPedImp2.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  CBPVICli.ListSource := DsPVICliCad;
  CBPVIRep.ListSource := DsPVIRepCad;
  CBPVIPrd.ListSource := DsPVIPrdCad;
  //
  ReopenPVICliCad;
  ReopenPVIRepCad;
  ReopenPVIPrdCad;
end;

procedure TFmFatPedImp2.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
    [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmFatPedImp2.ReopenPVICliCad;
begin
  QrPVICliCad.Close;
  UnDmkDAC_PF.AbreQuery(QrPVICliCad, Dmod.MyDB);
end;

procedure TFmFatPedImp2.ReopenPVIPrdCad;
begin
  QrPVIPrdCad.Close;
  UnDmkDAC_PF.AbreQuery(QrPVIPrdCad, Dmod.MyDB);
end;

procedure TFmFatPedImp2.ReopenPVIRepCad;
begin
  QrPVIRepCad.Close;
  UnDmkDAC_PF.AbreQuery(QrPVIRepCad, Dmod.MyDB);
end;

end.
