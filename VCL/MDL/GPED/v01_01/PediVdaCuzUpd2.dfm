object FmPediVdaCuzUpd2: TFmPediVdaCuzUpd2
  Left = 339
  Top = 185
  Caption = 'PED-VENDA-006 :: Item de Pedido - Altera'#231#227'o de Item Customizado'
  ClientHeight = 226
  ClientWidth = 704
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 704
    Height = 70
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 704
      Height = 70
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 201
        Top = 0
        Width = 503
        Height = 70
        Align = alClient
        Caption = 
          ' Medidas gerais ('#250'teis) do produto (pe'#231'as, kg, litros, metros, e' +
          'tc.): '
        TabOrder = 1
        object Label2: TLabel
          Left = 8
          Top = 16
          Width = 47
          Height = 13
          Caption = 'Medida 1:'
        end
        object Label5: TLabel
          Left = 112
          Top = 16
          Width = 47
          Height = 13
          Caption = 'Medida 2:'
        end
        object Label6: TLabel
          Left = 216
          Top = 16
          Width = 47
          Height = 13
          Caption = 'Medida 3:'
        end
        object Label7: TLabel
          Left = 320
          Top = 16
          Width = 47
          Height = 13
          Caption = 'Medida 4:'
        end
        object EdMedidaC: TdmkEdit
          Left = 8
          Top = 32
          Width = 100
          Height = 21
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 4
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,0000'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object EdMedidaL: TdmkEdit
          Left = 112
          Top = 32
          Width = 100
          Height = 21
          Alignment = taRightJustify
          TabOrder = 1
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 4
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,0000'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object EdMedidaA: TdmkEdit
          Left = 216
          Top = 32
          Width = 100
          Height = 21
          Alignment = taRightJustify
          TabOrder = 2
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 4
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,0000'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object EdMedidaE: TdmkEdit
          Left = 320
          Top = 32
          Width = 100
          Height = 21
          Alignment = taRightJustify
          TabOrder = 3
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 6
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,000000'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
      end
      object GroupBox2: TGroupBox
        Left = 0
        Top = 0
        Width = 201
        Height = 70
        Align = alLeft
        Caption = ' Dados de inclus'#227'o: '
        TabOrder = 0
        object Label3: TLabel
          Left = 72
          Top = 16
          Width = 48
          Height = 13
          Caption = 'Reduzido:'
        end
        object Label4: TLabel
          Left = 132
          Top = 16
          Width = 58
          Height = 13
          Caption = 'Quantidade:'
        end
        object Label1: TLabel
          Left = 12
          Top = 16
          Width = 36
          Height = 13
          Caption = 'ID item:'
          FocusControl = DBEdit1
        end
        object EdQuantP: TdmkEdit
          Left = 132
          Top = 32
          Width = 61
          Height = 21
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object DBEdit1: TDBEdit
          Left = 12
          Top = 32
          Width = 56
          Height = 21
          DataField = 'Controle'
          DataSource = DsPediVdaIts
          TabOrder = 1
        end
        object DBEdit2: TDBEdit
          Left = 72
          Top = 32
          Width = 56
          Height = 21
          DataField = 'GraGruX'
          DataSource = DsPediVdaIts
          TabOrder = 2
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 704
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 656
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 608
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 586
        Height = 32
        Caption = 'Item de Pedido - Altera'#231#227'o de Item Customizado'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 586
        Height = 32
        Caption = 'Item de Pedido - Altera'#231#227'o de Item Customizado'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 586
        Height = 32
        Caption = 'Item de Pedido - Altera'#231#227'o de Item Customizado'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 118
    Width = 704
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 700
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 162
    Width = 704
    Height = 64
    Align = alBottom
    TabOrder = 3
    object Panel5: TPanel
      Left = 2
      Top = 15
      Width = 700
      Height = 47
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object PnSaiDesis: TPanel
        Left = 556
        Top = 0
        Width = 144
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtSaida: TBitBtn
          Tag = 15
          Left = 2
          Top = 3
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Hint = 'Sai da janela atual'
          Caption = '&Desiste'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
        end
      end
      object BtOK: TBitBtn
        Tag = 14
        Left = 20
        Top = 3
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object QrPediVdaIts: TmySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrPediVdaItsAfterOpen
    SQL.Strings = (
      'SELECT pvi.Controle, pvi.GraGruX, pvi.QuantP,'
      'pvi.MedidaC, pvi.MedidaL, pvi.MedidaA, pvi.MedidaE,'
      'gti.Codigo GRATAMCAD, gti.Controle GRATAMITS, '
      'gti.Nome NO_TAM, gcc.Nome NO_COR'
      'FROM pedivdaits pvi'
      'LEFT JOIN gragrux   ggx ON ggx.Controle=pvi.GraGruX'
      'LEFT JOIN gratamits gti ON gti.Controle=ggx.GraTamI'
      'LEFT JOIN gragruc ggc ON ggc.Controle=ggx.GraGruC'
      'LEFT JOIN gracorcad gcc ON gcc.Codigo=ggc.GraCorCad'
      'LEFT JOIN gragru1   gg1 ON gg1.Nivel1=ggx.GraGru1 '
      'WHERE pvi.Customizad=1'
      'AND pvi.Controle=:P0')
    Left = 8
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPediVdaItsControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrPediVdaItsGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrPediVdaItsMedidaC: TFloatField
      FieldName = 'MedidaC'
    end
    object QrPediVdaItsMedidaL: TFloatField
      FieldName = 'MedidaL'
    end
    object QrPediVdaItsMedidaA: TFloatField
      FieldName = 'MedidaA'
    end
    object QrPediVdaItsMedidaE: TFloatField
      FieldName = 'MedidaE'
    end
    object QrPediVdaItsGRATAMCAD: TIntegerField
      FieldName = 'GRATAMCAD'
    end
    object QrPediVdaItsGRATAMITS: TAutoIncField
      FieldName = 'GRATAMITS'
    end
    object QrPediVdaItsNO_TAM: TWideStringField
      FieldName = 'NO_TAM'
      Size = 5
    end
    object QrPediVdaItsNO_COR: TWideStringField
      FieldName = 'NO_COR'
      Size = 30
    end
    object QrPediVdaItsQuantP: TFloatField
      FieldName = 'QuantP'
    end
  end
  object DsPediVdaIts: TDataSource
    DataSet = QrPediVdaIts
    Left = 36
    Top = 8
  end
end
