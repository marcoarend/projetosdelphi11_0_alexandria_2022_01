unit PediVdaImpEmp2;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DB, DBGrids, (*&&UnDmkABS_PF,*)
  dmkDBGrid, DBCtrls, mySQLDbTables, Menus, ComCtrls, UnDmkProcFunc, DmkDAC_PF;

type
  THackDBGrid = class(TDBGrid);
  TFmPediVdaImpEmp2 = class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    BtNenhum: TBitBtn;
    BtTodos: TBitBtn;
    BtPesq: TBitBtn;
    PMPesq: TPopupMenu;
    Ativa1: TMenuItem;
    Desativa1: TMenuItem;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    Panel3: TPanel;
    Label1: TLabel;
    EdPesq: TEdit;
    DBGrid1: TdmkDBGrid;
    Panel4: TPanel;
    DBGrid2: TdmkDBGrid;
    RGSelecao: TRadioGroup;
    procedure FormCreate(Sender: TObject);
    procedure DBGrid1CellClick(Column: TColumn);
    procedure FormStartDock(Sender: TObject; var DragObject: TDragDockObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure EdPesqChange(Sender: TObject);
    procedure FormUnDock(Sender: TObject; Client: TControl;
      NewTarget: TWinControl; var Allow: Boolean);
    procedure BtNenhumClick(Sender: TObject);
    procedure BtTodosClick(Sender: TObject);
    procedure BtPesqClick(Sender: TObject);
    procedure Ativa1Click(Sender: TObject);
    procedure Desativa1Click(Sender: TObject);
    procedure PageControl1Change(Sender: TObject);
    procedure FormDeactivate(Sender: TObject);
    procedure RGSelecaoClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    procedure Pesquisar();
    procedure AtivaItens(Status: Integer);
    procedure AtivaSelecionados(Status: Integer);
    procedure HabilitaItemUnico();
  public
    { Public declarations }
    class function CreateDockForm(const aColor: TColor): TCustomForm;
    //class procedure FiltaAtivos();
  end;

var
  FmPediVdaImpEmp2: TFmPediVdaImpEmp2;

implementation

uses UnMyObjects, Module, UMySQLModule, TabePrcCab, PediVdaImp2, ModPediVda, MyGlyfs;

{$R *.DFM}

///// PROCEDURES E FUNCTIONS DO DOCKFORM

procedure TFmPediVdaImpEmp2.Ativa1Click(Sender: TObject);
begin
  AtivaSelecionados(1);
end;

procedure TFmPediVdaImpEmp2.AtivaItens(Status: Integer);
begin
(*&&
  Screen.Cursor := crHourGlass;
  DmPediVda.QrPVIEmpIts.Close;
  DmPediVda.QrPVIEmpIts.SQL.Clear;
  DmPediVda.QrPVIEmpIts.SQL.Add('UPDATE pviemp SET Ativo=' + dmkPF.FFP(Status, 0) + ';');
  DmPediVda.QrPVIEmpIts.SQL.Add('SELECT * FROM pviemp;');
  DmkABS_PF.AbreQuery(DmPediVda.QrPVIEmpIts);
  HabilitaItemUnico();
  Screen.Cursor := crDefault;
*)
end;

procedure TFmPediVdaImpEmp2.AtivaSelecionados(Status: Integer);
begin
(*&&
  Screen.Cursor := crHourGlass;
  DmPediVda.QrPVIEmpIts.First;
  while not DmPediVda.QrPVIEmpIts.Eof do
  begin
    if DmPediVda.QrPVIEmpItsAtivo.Value <> Status then
    begin
      DmPediVda.QrPVIEmpIts.Edit;
      DmPediVda.QrPVIEmpItsAtivo.Value := Status;
      DmPediVda.QrPVIEmpIts.Post;
    end;
    DmPediVda.QrPVIEmpIts.Next;
  end;
  HabilitaItemUnico();
  Screen.Cursor := crDefault;
*)
end;

procedure TFmPediVdaImpEmp2.BtPesqClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMPesq, BtPesq);
end;

procedure TFmPediVdaImpEmp2.BtNenhumClick(Sender: TObject);
begin
  AtivaItens(0);
end;

procedure TFmPediVdaImpEmp2.BtTodosClick(Sender: TObject);
begin
  AtivaItens(1);
end;

class function TFmPediVdaImpEmp2.CreateDockForm(const aColor: TColor): TCustomForm;
begin
  result := TFmPediVdaImpEmp2.Create(Application);
  //result.Color := aColor;
  //result.Caption := ColorToString(aColor);
  FmMyGlyfs.ConfiguraFormDock(result);
  result.Show;
end;

procedure TFmPediVdaImpEmp2.FormClose(Sender: TObject; var Action: TCloseAction);
begin
(*&&
  DmPediVda.QrPVIEmpIts.Close;
  DmPediVda.QrPVIEmpIts.SQL.Clear;
  DmPediVda.QrPVIEmpIts.SQL.Add('DROP TABLE PVIEmp; ');
  DmPediVda.QrPVIEmpIts.ExecSQL;
  //
  ManualFloat(Rect(0, 0, 0, 0));
  Action := caFree;
*)
end;

procedure TFmPediVdaImpEmp2.FormShow(Sender: TObject);
begin
  FmPediVdaImp2.BtConfirma.Enabled := False;
end;

procedure TFmPediVdaImpEmp2.FormStartDock(Sender: TObject;
  var DragObject: TDragDockObject);
begin
  DragObject := TDragDockObjectEx.Create(Self);
  DragObject.Brush.Color := clAqua; // this will display a red outline
end;


procedure TFmPediVdaImpEmp2.FormUnDock(Sender: TObject; Client: TControl;
  NewTarget: TWinControl; var Allow: Boolean);
begin
end;

///// FIM DAS PROCEDURES E FUNCTIONS DO DOCKFORM

procedure TFmPediVdaImpEmp2.PageControl1Change(Sender: TObject);
begin
  BtPesq.Enabled := PageControl1.ActivePageIndex = 0;
  case PageControl1.ActivePageIndex of
    0: Pesquisar();
    1: RGSelecaoClick(Self);
  end;
end;

procedure TFmPediVdaImpEmp2.Pesquisar();
begin
(*&&
  Screen.Cursor := crHourGlass;
  //
  DmPediVda.QrPVIEmpIts.Filter   := 'Nome LIKE "%' + EdPesq.Text + '%"';
  DmPediVda.QrPVIEmpIts.Filtered := True;
  //
  Screen.Cursor := crDefault;
*)
end;

procedure TFmPediVdaImpEmp2.RGSelecaoClick(Sender: TObject);
begin
(*&&
  DmPediVda.QrPVIEmpIts.Filtered := False;
  case RGSelecao.ItemIndex of
    0: DmPediVda.QrPVIEmpIts.Filter := 'Ativo=0';
    1: DmPediVda.QrPVIEmpIts.Filter := 'Ativo=1';
    2: DmPediVda.QrPVIEmpIts.Filter := '';
  end;
  if RGSelecao.ItemIndex < 2 then
    DmPediVda.QrPVIEmpIts.Filtered := True;
*)
end;

procedure TFmPediVdaImpEmp2.DBGrid1CellClick(Column: TColumn);
var
  Status: Integer;
begin
(*&&
  if Column.FieldName = 'Ativo' then
  begin
    if DmPediVda.QrPVIEmpIts.FieldByName('Ativo').Value = 1 then
      Status := 0
    else
      Status := 1;
    //
    DmPediVda.QrPVIEmpIts.Edit;
    DmPediVda.QrPVIEmpIts.FieldByName('Ativo').Value := Status;
    DmPediVda.QrPVIEmpIts.Post;
  end;
  HabilitaItemUnico();
*)
end;

procedure TFmPediVdaImpEmp2.HabilitaItemUnico();
var
  Habilita: Boolean;
begin
(*&&
  DmPediVda.QrPVIEmpAti.Close;
  DmkABS_PF.AbreQuery(DmPediVda.QrPVIEmpAti);
  Habilita :=  DmPediVda.QrPVIEmpAtiItens.Value = 0;
  FmPediVdaImp2.LaPVIEmp.Enabled := Habilita;
  FmPediVdaImp2.EdPVIEmp.Enabled := Habilita;
  FmPediVdaImp2.CBPVIEmp.Enabled := Habilita;
  if not Habilita then
  begin
    FmPediVdaImp2.EdPVIEmp.ValueVariant := 0;
    FmPediVdaImp2.CBPVIEmp.KeyValue     := 0;
  end;
*)
end;

procedure TFmPediVdaImpEmp2.Desativa1Click(Sender: TObject);
begin
  AtivaSelecionados(0);
end;

procedure TFmPediVdaImpEmp2.EdPesqChange(Sender: TObject);
begin
  Pesquisar();
end;

procedure TFmPediVdaImpEmp2.FormCreate(Sender: TObject);
const
  Txt1 = 'INSERT INTO pviemp (Codigo,CodUsu,Nome,Ativo) VALUES(';
var
  Txt2: String;
begin
(*&&
  DmPediVda.QrPVIEmpIts.Close;
  DmPediVda.QrPVIEmpIts.SQL.Clear;
  DmPediVda.QrPVIEmpIts.SQL.Add('DROP TABLE PVIEmp; ');
  DmPediVda.QrPVIEmpIts.SQL.Add('CREATE TABLE PVIEmp (');
  DmPediVda.QrPVIEmpIts.SQL.Add('  Codigo  integer      ,');
  DmPediVda.QrPVIEmpIts.SQL.Add('  CodUsu  integer      ,');
  DmPediVda.QrPVIEmpIts.SQL.Add('  Nome    varchar(50)  ,');
  DmPediVda.QrPVIEmpIts.SQL.Add('  Ativo   smallint      ');
  DmPediVda.QrPVIEmpIts.SQL.Add(');');
  //
  DmPediVda.QrPVIEmpCad.Close;
  UnDmkDAC_PF.AbreQuery(DmPediVda.QrPVIEmpCad, Dmod.MyDB);
  while not DmPediVda.QrPVIEmpCad.Eof do
  begin
    Txt2 :=
      dmkPF.FFP(DmPediVda.QrPVIEmpCadCodigo.Value, 0) + ',' +
      dmkPF.FFP(DmPediVda.QrPVIEmpCadFilial.Value, 0) + ',' +
      '"' + DmPediVda.QrPVIEmpCadNOMEENT.Value + '",' +
      '0);';
  //for i  := 1 to 10 do
    DmPediVda.QrPVIEmpIts.SQL.Add(Txt1 + Txt2);
    DmPediVda.QrPVIEmpCad.Next;
  end;
  //
  DmPediVda.QrPVIEmpIts.SQL.Add('SELECT * FROM pviemp;');
  DmkABS_PF.AbreQuery(DmPediVda.QrPVIEmpIts);
  PageControl1.ActivePageIndex := 0;
*)
end;

procedure TFmPediVdaImpEmp2.FormDeactivate(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 1;
  RGSelecao.ItemIndex := 1;
  //
  if FindWindow('TFmPediVdaImp', nil) > 0 then
    FmPediVdaImp2.AtivaBtConfirma();
end;

end.

