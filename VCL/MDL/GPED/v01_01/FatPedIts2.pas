unit FatPedIts2;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DBCtrls, dmkDBLookupComboBox, DmkDAC_PF,
  dmkEdit, dmkEditCB, DB, mySQLDbTables, Mask, Variants, Grids, DBGrids,
  ComCtrls, dmkLabel, dmkDBEdit, dmkDBGrid, dmkEditCalc, UnDmkProcFunc,
  dmkImage, UnDmkEnums, UnGrl_Geral;

type
  TFmFatPedIts2 = class(TForm)
    Panel1: TPanel;
    Panel3: TPanel;
    QrStqCenCad: TmySQLQuery;
    QrStqCenCadCodigo: TIntegerField;
    QrStqCenCadCodUsu: TIntegerField;
    QrStqCenCadNome: TWideStringField;
    DsStqCenCad: TDataSource;
    QrItem: TmySQLQuery;
    DsItem: TDataSource;
    QrItemNOMENIVEL1: TWideStringField;
    QrItemGraCorCad: TIntegerField;
    QrItemNOMECOR: TWideStringField;
    QrItemNOMETAM: TWideStringField;
    QrItemGraGruX: TIntegerField;
    QrItemGraGru1: TIntegerField;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    PnLeitura: TPanel;
    Panel4: TPanel;
    Label3: TLabel;
    LaQtdeLei: TLabel;
    EdLeitura: TEdit;
    EdQtdLei: TdmkEdit;
    Panel7: TPanel;
    Label2: TLabel;
    EdStqCenCad: TdmkEditCB;
    CBStqCenCad: TdmkDBLookupComboBox;
    Panel8: TPanel;
    Label1: TLabel;
    DBEdit5: TdmkDBEdit;
    DsFatPedVol: TDataSource;
    Label10: TLabel;
    DBEdit6: TdmkDBEdit;
    DsFatPedCab: TDataSource;
    QrPreco: TmySQLQuery;
    DsPreco: TDataSource;
    QrFator: TmySQLQuery;
    QrItemCU_Nivel1: TIntegerField;
    QrItemIPI_Alq: TFloatField;
    PageControl2: TPageControl;
    TabSheet4: TTabSheet;
    TabSheet6: TTabSheet;
    PageControl3: TPageControl;
    TabSheet7: TTabSheet;
    GradeQ: TStringGrid;
    TabSheet8: TTabSheet;
    GradeC: TStringGrid;
    TabSheet9: TTabSheet;
    GradeA: TStringGrid;
    TabSheet10: TTabSheet;
    GradeX: TStringGrid;
    QrPediGru: TmySQLQuery;
    QrPediGruQuantF: TFloatField;
    QrPediGruCodUsu: TIntegerField;
    QrPediGruNome: TWideStringField;
    QrPediGruNivel1: TIntegerField;
    DsPediGru: TDataSource;
    QrPediGruGRATAMCAD: TIntegerField;
    TabSheet3: TTabSheet;
    GradeP: TStringGrid;
    QrPrecoPrecoF: TFloatField;
    QrPrecoQuantF: TFloatField;
    Panel11: TPanel;
    DBGGru: TdmkDBGrid;
    Panel12: TPanel;
    CkSoItensAFat: TCheckBox;
    DBGFatPedIts: TDBGrid;
    QrItemMadeBy: TSmallintField;
    QrCFOP: TmySQLQuery;
    QrCFOPCFOP: TWideStringField;
    dmkDBEdit1: TdmkDBEdit;
    PnSimu: TPanel;
    Label9: TLabel;
    Label8: TLabel;
    Label7: TLabel;
    dmkEdit2: TdmkEdit;
    dmkEdit3: TdmkEdit;
    dmkEdit1: TdmkEdit;
    Button1: TButton;
    Panel10: TPanel;
    Label13: TLabel;
    Label14: TLabel;
    DBEdit8: TDBEdit;
    DBCkICMS: TDBCheckBox;
    DBEdit9: TDBEdit;
    DBEdit10: TDBEdit;
    DBCheckBox2: TDBCheckBox;
    DBEdit11: TDBEdit;
    DBCheckBox3: TDBCheckBox;
    DBEdit12: TDBEdit;
    DBCheckBox4: TDBCheckBox;
    DBEdit13: TDBEdit;
    DBCheckBox5: TDBCheckBox;
    DBEdit14: TDBEdit;
    DBCheckBox6: TDBCheckBox;
    DBEdit15: TDBEdit;
    DBCheckBox7: TDBCheckBox;
    DBEdit16: TDBEdit;
    QrPrecoInfAdCuztm: TIntegerField;
    QrPrecoPercCustom: TFloatField;
    QrPrecoMedidaC: TFloatField;
    QrPrecoMedidaL: TFloatField;
    QrPrecoMedidaA: TFloatField;
    QrPrecoMedidaE: TFloatField;
    QrPrecoMedOrdem: TIntegerField;
    QrItemFracio: TSmallintField;
    QrPediGruFracio: TSmallintField;
    Panel9: TPanel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label12: TLabel;
    Label11: TLabel;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    DBEdit7: TDBEdit;
    DBEdit4: TDBEdit;
    PnMultiGrandeza: TPanel;
    Panel6: TPanel;
    BtOK: TBitBtn;
    EdPecas: TdmkEdit;
    LaPecas: TLabel;
    LaAreaM2: TLabel;
    LaAreaP2: TLabel;
    EdPeso: TdmkEdit;
    LaPeso: TLabel;
    Panel13: TPanel;
    EdAreaM2: TdmkEditCalc;
    EdAreaP2: TdmkEditCalc;
    QrItemHowBxaEstq: TSmallintField;
    QrItemGerBxaEstq: TSmallintField;
    QrItemprod_indTot: TSmallintField;
    BtGraGruN: TBitBtn;
    BtTabePrcCab: TBitBtn;
    BtFisRegCad: TBitBtn;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel5: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel14: TPanel;
    PnSaiDesis: TPanel;
    CkFixo: TCheckBox;
    BtFatura: TBitBtn;
    BtExclui: TBitBtn;
    BtSaida: TBitBtn;
    Label15: TLabel;
    EdQtdAFat: TdmkEdit;
    EdQtdFat: TdmkEdit;
    Label16: TLabel;
    QrPediGruTot: TmySQLQuery;
    QrPediGruTotQuantF: TFloatField;
    QrPediGruTotQuantV: TFloatField;
    QrItem2: TmySQLQuery;
    PB1: TProgressBar;
    PB2: TProgressBar;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure EdStqCenCadEnter(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure EdLeituraChange(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure EdQtdLeiKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure BtExcluiClick(Sender: TObject);
    procedure EdLeituraKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure QrItemBeforeClose(DataSet: TDataSet);
    procedure EdQtdLeiEnter(Sender: TObject);
    procedure EdQtdLeiExit(Sender: TObject);
    procedure EdStqCenCadChange(Sender: TObject);
    procedure EdStqCenCadExit(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure GradeADrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect;
      State: TGridDrawState);
    procedure GradeXDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect;
      State: TGridDrawState);
    procedure GradeCDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect;
      State: TGridDrawState);
    procedure GradeQDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect;
      State: TGridDrawState);
    procedure QrPediGruAfterScroll(DataSet: TDataSet);
    procedure GradePDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect;
      State: TGridDrawState);
    procedure CkSoItensAFatClick(Sender: TObject);
    procedure PageControl2Change(Sender: TObject);
    procedure GradePClick(Sender: TObject);
    procedure BtFaturaClick(Sender: TObject);
    procedure QrStqCenCadAfterOpen(DataSet: TDataSet);
    procedure GradePDblClick(Sender: TObject);
    procedure EdQtdLeiChange(Sender: TObject);
    procedure BtGraGruNClick(Sender: TObject);
    procedure BtTabePrcCabClick(Sender: TObject);
    procedure BtFisRegCadClick(Sender: TObject);
    procedure EdLeituraExit(Sender: TObject);
    procedure GradePSelectCell(Sender: TObject; ACol, ARow: Integer;
      var CanSelect: Boolean);
  private
    { Private declarations }
    FOriCtrl,
    FStqCenCad: Integer;
    FTam20: Boolean;
    function  ReopenItem(GraGruX: Integer): Boolean;
    //procedure InsereItem(Qtde: Double);
    procedure InsereItem2(QueryItem, QueryPreco: TmySQLQuery; Qtde: Double;
              StqCenCad, TipoCalc: Integer);
    procedure ReconfiguraGradesFat();
    //procedure ReopenPrecoOld(Nivel1, GraGruX: Integer);
    function  MultiGrandeza(Avisa: Boolean): Boolean;
  public
    { Public declarations }
    //procedure ReopenLidos(OriCtrl: Integer);
    //procedure ReopenPrecoOld(Nivel1, GraGruX: Integer);
    procedure ReopenPrecoSel(PediVda, GraGruX: Integer);
    procedure ReopenPrecoFat(Controle: Integer);
    procedure ReopenPediGru(Nivel1: Integer);
    procedure ReopenItensFat(PediVda: Integer);
  end;

  var
  FmFatPedIts2: TFmFatPedIts2;

implementation

uses
  {$IfNDef SemNFe_0000} ModuleNFe_0000, {$EndIF}
  {$IfNDef NAO_GFAT}UnGrade_Jan, UnGFat_Jan, {$EndIf}
  UnMyObjects, Module, UMySQLModule, dmkGeral, FatPedCab2, ModuleGeral,
  UnInternalConsts, MyDBCheck, GetValor, StqCenCad, Principal, ModPediVda,
  (*,ModFatuVda*) ModProd, (*PediVda,*) QuaisItens, UnGrade_Tabs,
  ModuleFatura, UnGrade_PF;

{$R *.DFM}

procedure TFmFatPedIts2.BtExcluiClick(Sender: TObject);
var
  Codi: String;
begin
  PB2.Position := 0;
  PB2.Max      := 2;
  Codi         := dmkPF.FFP(FmFatPedCab2.QrFatPedCabCodigo.Value, 0);
  //
  try
    PB2.Position := 1;
    //
    DmPediVda.ExcluiItensFaturamento(Codi, FmFatPedCab2.QrFatPedIts, DBGFatPedIts, PB1);
    //
    PB2.Position := 2;
    //
    DmPediVda.AtualizaTodosItensPediVda_(FmFatPedCab2.QrPediVdaCodigo.Value, PB1);
  finally
    ReopenPediGru(QrPediGruNivel1.Value);
    //
    PB1.Position := 0;
    PB2.Position := 0;
  end;
end;

procedure TFmFatPedIts2.BtFaturaClick(Sender: TObject);

  function IncluiTodoGrupo(StqCencad, TipoCalc: Integer): Boolean;
  var
    c, r, Reduzido: Integer;
    Quantidade: Double;
  begin
    Result       := False;
    PB1.Position := 0;
    PB1.Max      := GradeP.ColCount;
    //
    for c := 1 to GradeP.ColCount - 1 do
    begin
      for r := 1 to GradeP.RowCount - 1 do
      begin
        Quantidade := Geral.DMV(GradeP.Cells[c,r]);
        //
        if Quantidade > 0 then
        begin
          Reduzido := Geral.IMV(GradeC.Cells[c,r]);
          //
          if Reduzido > 0 then
          begin
            EdLeitura.Text        := dmkPF.FFP(Reduzido, 0);
            EdQtdLei.ValueVariant := Quantidade;
            //
            if Length(EdLeitura.Text) <= 6 then
            begin
              if QrItem2.Locate('GraGrux', Geral.IMV(EdLeitura.Text), []) then
              begin
                InsereItem2(QrItem2, QrItem2, Quantidade, StqCencad, TipoCalc);
                Result := True;
                // n�o pode aqui por causa do GOTOBOOKMARK
                //ReopenPediGru(QrPediGruNivel1.Value);
              end;
            end;
          end;
        end;
      end;
      PB1.Position := PB1.Position + 1;
      PB1.Update;
      Application.ProcessMessages;
    end;
    //DmPediVda.AtualizaTodosItensPediVda_(FmFatPedCab2.QrPediVdaCodigo.Value);
  end;

var
  q: TSelType;
  m, n, StqCencad, Empresa, TipoCalc: Integer;
begin
  if MultiGrandeza(True) then Exit;
  //
  q := istDesiste;
  //
  if MyObjects.CriaForm_AcessoTotal(TFmQuaisItens, FmQuaisItens) then
  begin
    with FmQuaisItens do
    begin
      ShowModal;
      if not FSelecionou then
        Geral.MB_Aviso('Faturamento cancelado pelo usu�rio!')
      else
        q := FEscolha;
      Destroy;
    end;
  end;
  if q = istDesiste then Exit;
  //
  Screen.Cursor := crHourGlass;
  try
    ReopenItensFat(FmFatPedCab2.QrFatPedCabPedido.Value);
    //
    if QrItem2.RecordCount = 0 then Exit;
    //
    if not UMyMod.ObtemCodigoDeCodUsu(EdStqCenCad, StqCencad,
      'Informe o Centro de estoque!', 'Codigo', 'CodUsu')
    then
      Exit;
    //
    Empresa  := FmFatPedCab2.QrFatPedCabEmpresa.Value;
    TipoCalc := DmProd.DefineTipoCalc(FmFatPedCab2.QrFatPedCabRegrFiscal.Value,
                  StqCenCad, Empresa);
    //
    PB1.Position      := 0;
    PB2.Position      := 0;
    PnLeitura.Enabled := False;
    m := 0;
    if (q = istSelecionados) and (DBGGru.SelectedRows.Count < 2) then
      q := istAtual;
    case q of
      istAtual:
      begin
        if Geral.MB_Pergunta('Confirma o faturamento de todo grupo selecionado?') = ID_YES then
        begin
          if IncluiTodoGrupo(StqCencad, TipoCalc) then
            m := 1;
        end;
      end;
      istSelecionados:
      begin
        if Geral.MB_Pergunta('Confirma o faturamento dos ' + Geral.FF0(DBGGru.SelectedRows.Count) +
          ' grupos selecionados?') = ID_YES then
        begin
          with DBGGru.DataSource.DataSet do
          begin
            PB1.Max := DBGGru.SelectedRows.Count;
            //
            for n := 0 to DBGGru.SelectedRows.Count - 1 do
            begin
              //GotoBookmark(pointer(DBGGru.SelectedRows.Items[n]));
              GotoBookmark(DBGGru.SelectedRows.Items[n]);
              //
              if IncluiTodoGrupo(StqCencad, TipoCalc) then
                inc(m, 1);
              //
              PB2.Position := PB2.Position + 1;
              PB2.Update;
              Application.ProcessMessages;
            end;
          end;
        end;
      end;
      istTodos:
      begin
        if Geral.MB_Pergunta('Confirma o faturamento de todos os ' +
          Geral.FF0(QrPediGru.RecordCount) + ' grupos de produtos?') = ID_YES then
        begin
          QrPediGru.First;
          //
          PB1.Max := QrPediGru.RecordCount;
          //
          while not QrPediGru.Eof do
          begin
            if IncluiTodoGrupo(StqCencad, TipoCalc) then
              inc(m, 1);
            //
            PB2.Position := PB2.Position + 1;
            PB2.Update;
            Application.ProcessMessages;
            //
            QrPediGru.Next;
          end;
        end;
      end;
    end;
    ReopenPediGru(QrPediGruNivel1.Value);
    if m > 0 then
    begin
      FmFatPedCab2.ReopenFatPedIts(FOriCtrl);
      if m = 1 then
        Geral.MB_Info('Um grupo de produtos foi faturado!')
      else
        Geral.MB_Info(Geral.FF0(m) + ' grupos foram faturados!');
    end;
  finally
    PnLeitura.Enabled := True;
    Screen.Cursor     := crDefault;
    //
    PB1.Position := 0;
    PB2.Position := 0;
  end;
end;

procedure TFmFatPedIts2.BtFisRegCadClick(Sender: TObject);
{$IfNDef NAO_GFAT}
var
  FisRegCad: Integer;
begin
  FisRegCad := FmFatPedCab2.QrPediVdaRegrFiscal.Value;
  //
  Grade_Jan.MostraFormFisRegCad(FisRegCad);
  try
    Screen.Cursor := crHourGlass;
    UnDmkDAC_PF.AbreQuery(QrStqCenCad, Dmod.MyDB);
    ReconfiguraGradesFat();
  finally
    Screen.Cursor := crDefault;
  end;
{$Else}
  Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappGFat);
{$EndIf}
end;

procedure TFmFatPedIts2.BtGraGruNClick(Sender: TObject);
{$IfNDef NAO_GFAT}
var
  Nivel1: Integer;
begin
  if (QrPediGru.State <> dsInactive) and (QrPediGru.RecordCount > 0) then
    Nivel1 := QrPediGruNivel1.Value
  else
    Nivel1 := 0;
  //
  Grade_Jan.MostraFormGraGruN(Nivel1);
  try
    Screen.Cursor := crHourGlass;
    //
    QrPediGru.Close;
    UnDmkDAC_PF.AbreQuery(QrPediGru, Dmod.MyDB);
    QrPediGru.Locate('Nivel1', Nivel1, []);
    //
    ReconfiguraGradesFat();
  finally
    Screen.Cursor := crDefault;
  end;
{$Else}
  Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappGFat);
{$EndIf}
end;

procedure TFmFatPedIts2.BtOKClick(Sender: TObject);
var
  Tam, GraGruX, Empresa, StqCencad, TipoCalc, Seq: Integer;
  Qtde: Double;
begin
  Tam := Length(EdLeitura.Text);
  //
  if (Tam <= 6) or ((DmProd.QrOpcoesGrad.FieldByName('UsaCodFornece').AsInteger = 1)
    and (Tam = 13)) then
  begin
    DmProd.ObtemGraGruXDeCodigoDeBarraProduto(EdLeitura.Text, GraGruX, Seq);
    //
    if ReopenItem(GraGruX) then
    begin
      Qtde     := Geral.DMV(EdQtdLei.Text);
      Empresa  := FmFatPedCab2.QrFatPedCabEmpresa.Value;
      TipoCalc := DmProd.DefineTipoCalc(FmFatPedCab2.QrFatPedCabRegrFiscal.Value,
                    StqCenCad, Empresa);
      //
      if not UMyMod.ObtemCodigoDeCodUsu(EdStqCenCad, StqCencad,
        'Informe o Centro de estoque!', 'Codigo', 'CodUsu')
      then
        Exit;
      //
      InsereItem2(QrItem, QrPreco, Qtde, StqCencad, TipoCalc);
      ReopenPediGru(QrPediGruNivel1.Value);
      FmFatPedCab2.ReopenFatPedIts(FOriCtrl);
    end;
  end;
end;

procedure TFmFatPedIts2.InsereItem2(QueryItem, QueryPreco: TmySQLQuery;
  Qtde: Double; StqCenCad, TipoCalc: Integer);
var
  NFe_FatID, Cliente, RegrFiscal, GraGruX,
  FatSemEstq, AFP_Sit: Integer; AFP_Per: Double;
  Cli_Tipo: Integer; Cli_IE: String; Cli_UF, EMP_UF, EMP_FILIAL,
  ASS_CO_UF, ASS_FILIAL, Item_MadeBy: Integer; Item_IPI_ALq: Double;
  Preco_PrecoF, Preco_PercCustom, Preco_MedidaC, Preco_MedidaL,
  Preco_MedidaA, Preco_MedidaE: Double; Preco_MedOrdem: Integer;
  SQLType: TSQLType;
  PediVda: Integer;
  TabelaPrc, OriCodi, Empresa, OriCnta, Associada, OriPart, InfAdCuztm: Integer;
  NO_tablaPrc, Msg: String;
  //
  Falta, Pecas, AreaM2, AreaP2, Peso: Double;
  prod_indTot, IDCtrl: Integer;
  prod_vFrete, prod_vSeg, prod_vDesc, prod_vOutro: Double;
begin
  Screen.Cursor := crHourGlass;
  try
    //StqCenCad        := Geral.IMV(EdStqCenCad.Text);
    if StqCencad = 0 then
      Exit;
    if TipoCalc = -1 then
      Exit;
    //
    NFe_FatID        := VAR_FATID_0001;
    Cliente          := FmFatPedCab2.QrCliCodigo.Value;
    RegrFiscal       := FmFatPedCab2.QrPediVdaRegrFiscal.Value;
    GraGruX          := QueryItem.FieldByName('GraGruX').AsInteger;
    FatSemEstq       := FmFatPedCab2.QrFatPedCabFatSemEstq.Value;
    AFP_Sit          := FmFatPedCab2.QrFatPedCabAFP_Sit.Value;
    AFP_Per          := FmFatPedCab2.QrFatPedCabAFP_Per.Value;
    Cli_Tipo         := FmFatPedCab2.QrCliTipo.Value;
    Cli_IE           := FmFatPedCab2.QrCliIE.Value;
    Cli_UF           := Trunc(FmFatPedCab2.QrCliUF.Value);
    EMP_UF           := Trunc(FmFatPedCab2.QrPediVdaEMP_UF.Value);
    EMP_FILIAL       := FmFatPedCab2.QrFatPedCabEMP_FILIAL.Value;
    ASS_CO_UF        := FmFatPedCab2.QrFatPedCabASS_CO_UF.Value;
    ASS_FILIAL       := FmFatPedCab2.QrFatPedCabASS_FILIAL.Value;
    Item_MadeBy      := QueryItem.FieldByName('MadeBy').AsInteger;
    ITEM_IPI_Alq     := QueryItem.FieldByName('IPI_Alq').AsFloat;
    Preco_PrecoF     := QueryPreco.FieldByName('PrecoF').AsFloat;
    Preco_PercCustom := QueryPreco.FieldByName('PercCustom').AsFloat;
    Preco_MedidaC    := QueryPreco.FieldByName('MedidaC').AsFloat;
    Preco_MedidaL    := QueryPreco.FieldByName('MedidaL').AsFloat;
    Preco_MedidaA    := QueryPreco.FieldByName('MedidaA').AsFloat;
    Preco_MedidaE    := QueryPreco.FieldByName('MedidaE').AsFloat;
    Preco_MedOrdem   := QueryPreco.FieldByName('MedOrdem').AsInteger;
    SQLType          := ImgTipo.SQLType;
    PediVda          := FmFatPedCab2.QrFatPedCabPedido.Value;
    TabelaPrc        := FmFatPedCab2.QrFatPedCabTabelaPrc.Value;
    NO_tablaPrc      := FmFatPedCab2.QrFatPedCabNO_TabelaPrc.Value;
    OriCodi          := FmFatPedCab2.QrFatPedCabCodigo.Value;
    Empresa          := FmFatPedCab2.QrFatPedCabEmpresa.Value;
    OriCnta          := FmFatPedCab2.QrFatPedVolCnta.Value;
    Associada        := FmFatPedCab2.QrFatPedCabAssociada.Value;
    prod_indTot      := QueryItem.FieldByName('prod_indTot').AsInteger;
    //
    if (QueryItem.State = dsInactive) or (QueryItem.RecordCount = 0) then
    begin
      Geral.MB_Aviso('Reduzido n�o definido!');
      Exit;
    end;
    //
    if QrPediGruFracio.Value <> QueryItem.FieldByName('Fracio').AsInteger then
    begin
      Geral.MB_Aviso('Fracionamento n�o confere! AVISE A DERMATEK');
      Exit;
    end;
    if PediVda > 0 then
    begin
      OriPart := DmPediVda.SaldoRedudidoPed(PediVda, GraGruX, Trunc(Qtde + 0.00001), Falta);
      //
      if OriPart = 0 then
        Exit
      else
        ReopenPrecoFat(OriPart);
      //
      Preco_PrecoF     := QueryPreco.FieldByName('PrecoF').AsFloat;
      Preco_PercCustom := QueryPreco.FieldByName('PercCustom').AsFloat;
      Preco_MedidaC    := QueryPreco.FieldByName('MedidaC').AsFloat;
      Preco_MedidaL    := QueryPreco.FieldByName('MedidaL').AsFloat;
      Preco_MedidaA    := QueryPreco.FieldByName('MedidaA').AsFloat;
      Preco_MedidaE    := QueryPreco.FieldByName('MedidaE').AsFloat;
      Preco_MedOrdem   := QueryPreco.FieldByName('MedOrdem').AsInteger;
      //
      if (Qtde <= 0) then
      begin
        Geral.MB_Aviso('Informe a quantidade!');
        Exit;
      end;
      if Qtde > Falta then
      begin
        Geral.MB_Aviso('A qtde informada � al�m do necess�rio para este reduzido!'
          + sLineBreak + ' Necess�rio: ' + FloatToStr(Falta) + sLineBreak +
          ' Informado: ' + FloatToStr(Qtde) + ' Inclus�o de item abortada!');
        Exit;
      end;
      if QueryPreco.RecordCount = 0 then
      begin
        Geral.MB_Aviso('Pre�o n�o definido!' + sLineBreak + ' Produto: ' +
          FloatToStr(QueryItem.FieldByName('CU_Nivel1').AsInteger) + ' - ' +
          QueryItem.FieldByName('NOMENIVEL1').AsString + sLineBreak + 'Tabela: ' +
          FloatToStr(TabelaPrc) + ' - ' + FmFatPedCab2.QrFatPedCabNO_TabelaPrc.Value);
        Exit;
      end else
        InfAdCuztm := QueryPreco.FieldByName('InfAdCuztm').AsInteger;
    end else
    begin
      OriPart := 0;
      InfAdCuztm := 0;
    end;

    if PnMultiGrandeza.Visible then
    begin
      Pecas  := - EdPecas.ValueVariant;
      AreaM2 := - EdAreaM2.ValueVariant;
      AreaP2 := - EdAreaP2.ValueVariant;
      Peso   := - EdPeso.ValueVariant;
      //
      if not Grade_PF.ValidaGrandeza(TipoCalc,
        QueryItem.FieldByName('HowBxaEstq').AsInteger, Pecas, AreaM2, AreaP2,
        Peso, Msg) then
      begin
        Geral.MB_Aviso(Msg);
        Exit;
      end;
    end else begin
      Pecas  := 0;
      AreaM2 := 0;
      AreaP2 := 0;
      Peso   := 0;
    end;
    IDCtrl := 0;
    //
    prod_vFrete := 0.00;
    prod_vSeg   := 0.00;
    prod_vDesc  := 0.00;
    prod_vOutro := 0.00;
    //
    if DmFatura.InsereItemStqMov(VAR_FATID_0001, OriCodi, OriCnta, Empresa,
      Cliente, Associada, RegrFiscal, GraGruX, NO_tablaPrc, '', StqCenCad,
      FatSemEstq, AFP_Sit, AFP_Per, Qtde, Cli_Tipo, Cli_IE, Cli_UF, EMP_UF,
      EMP_FILIAL, ASS_CO_UF, ASS_FILIAL, Item_MadeBy, Item_IPI_ALq, Preco_PrecoF,
      Preco_PercCustom, Preco_MedidaC, Preco_MedidaL, Preco_MedidaA,
      Preco_MedidaE, Preco_MedOrdem, SQLType, PediVda, OriPart, InfAdCuztm,
      (*TipoNF*)0, (*modNF*)0, (*Serie*)0, (*nNF*)0, (*SitDevolu*)0, (*Servico*)0,
      (*refNFe*)'', 0, FOriCtrl, Pecas, AreaM2, AreaP2, Peso, TipoCalc,
      prod_indTot,
      prod_vFrete, prod_vSeg, prod_vDesc, prod_vOutro,
      IDCtrl) then
    begin
      EdLeitura.Text        := '';
      EdQtdLei.ValueVariant := 1;
      //
      if PnLeitura.Enabled then
        EdLeitura.SetFocus;
      // n�o pode aqui por causa do GOTOBOOKMARK
      //ReopenPediGru(GraGru1);
    end;
  finally
    Screen.Cursor := crDefault;
  end;
end;

function TFmFatPedIts2.MultiGrandeza(Avisa: Boolean): Boolean;
begin
  Result := PnMultiGrandeza.Visible;
  if Result then
    Geral.MB_Aviso('Inclus�o de "Multi Grandeza" n�o permitida para "Multi Itens"');
end;

procedure TFmFatPedIts2.PageControl2Change(Sender: TObject);
begin
  BtExclui.Visible := PageControl2.ActivePageIndex = 1;
end;

procedure TFmFatPedIts2.QrItemBeforeClose(DataSet: TDataSet);
begin
  QrPreco.Close;
  //#PnSimu.Visible := True;
end;

procedure TFmFatPedIts2.QrPediGruAfterScroll(DataSet: TDataSet);
begin
  EdQtdLei.DecimalSize  := QrPediGruFracio.Value;
  EdQtdAFat.DecimalSize := QrPediGruFracio.Value;
  EdQtdFat.DecimalSize  := QrPediGruFracio.Value;
  ReconfiguraGradesFat;
end;

procedure TFmFatPedIts2.QrStqCenCadAfterOpen(DataSet: TDataSet);
begin
  EdStqCenCad.ValueVariant := QrStqCenCadCodUsu.Value;
  CBStqCenCad.KeyValue     := QrStqCenCadCodUsu.Value;
  PageControl1.ActivePageIndex := 0;
end;

procedure TFmFatPedIts2.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmFatPedIts2.BtTabePrcCabClick(Sender: TObject);
{$IfNDef NAO_GFAT}
var
  TabPrc: Integer;
begin
  TabPrc := FmFatPedCab2.QrPediVdaTabelaPrc.Value;
  //
  GFat_Jan.MostraFormTabePrcCab(TabPrc);
  try
    Screen.Cursor := crHourGlass;
    ReconfiguraGradesFat();
  finally
    Screen.Cursor := crDefault;
  end;
{$Else}
  Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappGFat);
{$EndIf}
end;

procedure TFmFatPedIts2.Button1Click(Sender: TObject);
const
  Tick = 25;
var
  i: Integer;
begin
  EdLeitura.Text := '';
  //
  for i := 1 to Length(dmkEdit1.Text) do
  begin
    EdLeitura.Text := EdLeitura.Text + dmkEdit1.Text[i];
    sleep(Tick);
  end;
  //
  for i := 1 to Length(dmkEdit2.Text) do
  begin
    EdLeitura.Text := EdLeitura.Text + dmkEdit2.Text[i];
    sleep(Tick);
  end;
  //
  for i := 1 to Length(dmkEdit3.Text) do
  begin
    EdLeitura.Text := EdLeitura.Text + dmkEdit3.Text[i];
    sleep(Tick);
  end;
end;

procedure TFmFatPedIts2.CkSoItensAFatClick(Sender: TObject);
begin
  QrPediGru.Filtered := CkSoItensAFat.Checked;
  ReopenPediGru(QrPediGruNivel1.Value);
end;

procedure TFmFatPedIts2.EdLeituraChange(Sender: TObject);
(*
var
  Tam, GraGruX, Seq: Integer;
  Qtde: Double;
*)
begin
(*
  A leitora n�o mostra as mensagens no onChange mudado para o onExit

  Tam := Length(EdLeitura.Text);
  //
  if (Tam = 20) or
    ((DmProd.QrOpcoesGrad.FieldByName('UsaCodFornece').AsInteger = 1) and (Tam = 13)) then
  begin
    DmProd.ObtemGraGruXDeCodigoDeBarraProduto(EdLeitura.Text, GraGruX, Seq);
    //
    if ReopenItem(GraGruX) then
      if CkFixo.Checked then
      begin
        FTam20 := True;
        Qtde := Geral.DMV(EdQtdLei.Text);
        InsereItem2(Qtde);
        FmFatPedCab2.ReopenFatPedIts(FOriCtrl);
        ReopenPediGru(QrPediGruNivel1.Value);
        if PnLeitura.Enabled then
          EdLeitura.SetFocus;
        EdLeitura.Text := '';
      end;
  end else if Tam = 0 then QrItem.Close;
*)
end;

procedure TFmFatPedIts2.EdLeituraExit(Sender: TObject);
var
  Tam, GraGruX, StqCencad, Empresa, TipoCalc, Seq: Integer;
  Qtde: Double;
begin
  Tam := Length(EdLeitura.Text);
  //
  if (Tam = 20) or
    ((DmProd.QrOpcoesGrad.FieldByName('UsaCodFornece').AsInteger = 1) and (Tam = 13)) then
  begin
    DmProd.ObtemGraGruXDeCodigoDeBarraProduto(EdLeitura.Text, GraGruX, Seq);
    //
    if ReopenItem(GraGruX) then
      if CkFixo.Checked then
      begin
        FTam20   := True;
        Qtde     := Geral.DMV(EdQtdLei.Text);
        Empresa  := FmFatPedCab2.QrFatPedCabEmpresa.Value;
        TipoCalc := DmProd.DefineTipoCalc(FmFatPedCab2.QrFatPedCabRegrFiscal.Value,
                      StqCenCad, Empresa);
        //
        if not UMyMod.ObtemCodigoDeCodUsu(EdStqCenCad, StqCencad,
          'Informe o Centro de estoque!', 'Codigo', 'CodUsu')
        then
          Exit;
        //
        InsereItem2(QrItem, QrPreco, Qtde, StqCencad, TipoCalc);
        FmFatPedCab2.ReopenFatPedIts(FOriCtrl);
        ReopenPediGru(QrPediGruNivel1.Value);
        //
        if PnLeitura.Enabled then
          EdLeitura.SetFocus;
        //
        EdLeitura.Text := '';
      end;
  end else if Tam = 0 then QrItem.Close;
end;

procedure TFmFatPedIts2.EdLeituraKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  (*
  if Key = VK_RETURN then
  begin
    if Length(EdLeitura.Text) <= 6 then
      ReopenItem(Geral.IMV(EdLeitura.Text))
    else
      Geral.MB_Aviso('Quantidade de caracteres inv�lida para localiza��o pelo reduzido!');
  end;
  *)
end;

procedure TFmFatPedIts2.EdQtdLeiChange(Sender: TObject);
begin
  case QrItemGerBxaEstq.Value of
    1: EdPecas.ValueVariant  := EdQtdLei.ValueVariant;
    2: EdAreaM2.ValueVariant := EdQtdLei.ValueVariant;
    3: EdPeso.ValueVariant   := EdQtdLei.ValueVariant;
    else ;
  end;  
end;

procedure TFmFatPedIts2.EdQtdLeiEnter(Sender: TObject);
begin
  if CkFixo.Checked and (EdLeitura.Text = '') and FTam20 then
  begin
    FTam20 := False;
    if PnLeitura.Enabled then
      EdLeitura.SetFocus;
  end;
end;

procedure TFmFatPedIts2.EdQtdLeiExit(Sender: TObject);
begin
  //EdQtdLei.Text := Geral.TFT(EdQtdLei.Text,
    //FmFatPedCab2.QrFatPedCabCasasProd.Value, siPositivo);
end;

procedure TFmFatPedIts2.EdQtdLeiKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  StqCencad, Empresa, TipoCalc: Integer;
  Qtde: Double;
begin
  if Key = VK_RETURN then
  begin
    Qtde     := Geral.DMV(EdQtdLei.Text);
    Empresa  := FmFatPedCab2.QrFatPedCabEmpresa.Value;
    TipoCalc := DmProd.DefineTipoCalc(FmFatPedCab2.QrFatPedCabRegrFiscal.Value,
                  StqCenCad, Empresa);
    //
    if not UMyMod.ObtemCodigoDeCodUsu(EdStqCenCad, StqCencad,
      'Informe o Centro de estoque!', 'Codigo', 'CodUsu')
    then
      Exit;
    //
    InsereItem2(QrItem, QrPreco, Qtde, StqCencad, TipoCalc);
    FmFatPedCab2.ReopenFatPedIts(FOriCtrl);
    ReopenPediGru(QrPediGruNivel1.Value);
  end;  
end;

procedure TFmFatPedIts2.EdStqCenCadChange(Sender: TObject);
var
  Habilita: Boolean;
begin
  Habilita :=
    (EdStqCenCad.ValueVariant <> Null)
  and
    (EdStqCenCad.ValueVariant <> 0);
  //
  PnLeitura.Enabled := Habilita;
  BtFatura.Enabled  := BtFatura.Visible and Habilita;
  //#BtGrade.Enabled   := Habilita;
  //
end;

procedure TFmFatPedIts2.EdStqCenCadEnter(Sender: TObject);
begin
  FStqCenCad := EdStqCenCad.ValueVariant;
end;

procedure TFmFatPedIts2.EdStqCenCadExit(Sender: TObject);
begin
  //ReopenLidos(0);
end;

procedure TFmFatPedIts2.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  if EdLeitura.Visible and PnLeitura.Enabled then
  try
    EdLeitura.SetFocus;
  except
    ;
  end;
end;

procedure TFmFatPedIts2.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  FmFatPedCab2.ReopenFatPedIts(FOriCtrl);
end;

procedure TFmFatPedIts2.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  PnMultiGrandeza.Visible := FmFatPedCab2.FMultiGrandeza;
  BtFatura.Visible        := not FmFatPedCab2.FMultiGrandeza;
  //
  if not Grade_PF.ReopenFatStqCenCad(FmFatPedCab2.QrFatPedCabRegrFiscal.Value,
    FmFatPedCab2.QrFatPedCabEmpresa.Value, DModG.QrFiliLogNomeEmp.Value,
    FmFatPedCab2.QrPediVdaNOMEFISREGCAD.Value, QrStqCenCad) then
  begin
    if Geral.MB_Pergunta('Deseja configurar a Regra Fiscal agora?') = ID_YES then
    begin
      Grade_Jan.MostraFormFisRegCad(FmFatPedCab2.QrFatPedCabRegrFiscal.Value);
      UnDmkDAC_PF.AbreQuery(QrStqCenCad, Dmod.MyDB);
    end;
  end;
  ReopenPediGru(0);
  PageControl1.ActivePageIndex := 0;
  PageControl2.ActivePageIndex := 0;
  PageControl3.ActivePageIndex := 1;
  EdQtdLei.ValueVariant := 1;
end;

procedure TFmFatPedIts2.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmFatPedIts2.GradeADrawCell(Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);
begin
  MyObjects.DesenhaGradeA(GradeA, ACol, ARow, Rect, State, True);
end;

procedure TFmFatPedIts2.GradeCDrawCell(Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);
begin
  MyObjects.DesenhaGradeN(GradeC, GradeA, nil, ACol, ARow, Rect, State,
  '0', 0, 0, False);
end;

procedure TFmFatPedIts2.GradePClick(Sender: TObject);
var
  GraGruX: Integer;
  CodBarra: String;
begin
  if (GradeP.Col = 0) or (GradeP.Row = 0) then
    Exit;
  //
  GraGruX := Geral.IMV(GradeC.Cells[GradeP.Col,GradeP.Row]);
  //
  DmProd.ObtemCodigoDeBarraProdutoDeGraGruX(GraGruX, CodBarra);
  //
  EdLeitura.Text := CodBarra;
  //
  if Geral.SoNumero1a9_TT(EdLeitura.Text) <> '' then
  begin
    ReopenItem(GraGruX);
    EdQtdLei.ValueVariant := 0;
  end;
end;

procedure TFmFatPedIts2.GradePDblClick(Sender: TObject);
begin
  if (GradeP.Col > 0) and (GradeP.Row > 0) then
    EdQtdLei.Text := GradeP.Cells[GradeP.Col, GradeP.Row];
end;

procedure TFmFatPedIts2.GradePDrawCell(Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);
begin
  MyObjects.DesenhaGradeN(GradeP, GradeA, nil, ACol, ARow, Rect, State,
  dmkPF.FormataCasas(QrPediGruFracio.Value), 0, 0, False);
end;

procedure TFmFatPedIts2.GradePSelectCell(Sender: TObject; ACol, ARow: Integer;
  var CanSelect: Boolean);
begin
  if (ACol = 0) or (ARow = 0) then
    GradeP.Options := GradeP.Options - [goEditing]
  else
    GradeP.Options := GradeP.Options + [goEditing];
end;

procedure TFmFatPedIts2.GradeQDrawCell(Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);
begin
  MyObjects.DesenhaGradeN(GradeQ, GradeA, nil, ACol, ARow, Rect, State,
    dmkPF.FormataCasas(QrPediGruFracio.Value), 0, 0, False);
end;

procedure TFmFatPedIts2.GradeXDrawCell(Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);
begin
  MyObjects.DesenhaGradeN(GradeX, GradeA, nil, ACol, ARow, Rect, State,
  '0', 0, 0, False);
end;

procedure TFmFatPedIts2.ReconfiguraGradesFat;
begin

  DmProd.ConfigGrades7(
    QrPediGruGRATAMCAD.Value,
    QrPediGruNivel1.Value,
    FmFatPedCab2.QrFatPedCabPedido.Value,
    FmFatPedCab2.QrFatPedCabCodigo.Value,
    GradeA, GradeX, GradeC, GradeQ, GradeP);
end;

function TFmFatPedIts2.ReopenItem(GraGruX: Integer): Boolean;
begin
  Result := False;
  QrItem.Close;
  if GraGruX <> 0 then
  begin
    QrItem.Params[0].AsInteger := GraGrux;
    UMyMod.AbreQuery(QrItem, Dmod.MyDB, 'TFmFatPedIts.ReopenItem()');
    //
    if QrItem.RecordCount > 0 then
    begin
      Result := True;
      if PnLeitura.Enabled then
        EdQtdLei.SetFocus;
      //#PnLido.Visible := True;
      //#PnSimu.Visible := False;
      ReopenPrecoSel(FmFatPedCab2.QrFatPedCabPedido.Value, GraGruX);
    end else
      Geral.MB_Aviso('Reduzido n�o localizado!');
  end;
end;

{
procedure TFmFatPedIts.ReopenLidos(OriCtrl: Integer);
var
  StqCenCad: Integer;
begin
  QrLidos.Close;
  QrTotal.Close;
  StqCenCad := Geral.IMV(EdStqCenCad.Text);
  if StqCenCad <> 0 then
  begin
    QrLidos.Params[00].AsInteger := FmFatPedCab2.QrFatPedCabCodigo.Value;
    QrLidos.Params[01].AsInteger := StqCenCad;
    UMyMod.AbreQuery(QrLidos, '??????????????');
    QrLidos.Locate('OriCtrl', OriCtrl, []);
    //
    QrTotal.Params[00].AsInteger := FmFatPedCab2.QrFatPedCabCodigo.Value;
    QrTotal.Params[01].AsInteger := StqCenCad;
    UMyMod.AbreQuery(QrTotal, '?????????????????');
    //
  end;
end;
}

procedure TFmFatPedIts2.ReopenPediGru(Nivel1: Integer);
begin
  QrPediGru.Close;
  QrPediGru.Params[0].AsInteger := FmFatPedCab2.QrFatPedCabPedido.Value;
  UMyMod.AbreQuery(QrPediGru, Dmod.MyDB, 'TFmFatPedIts.ReopenPediGru()');
  //
  QrPediGruTot.Close;
  QrPediGruTot.Params[0].AsInteger := FmFatPedCab2.QrFatPedCabPedido.Value;
  UMyMod.AbreQuery(QrPediGruTot, Dmod.MyDB, 'TFmFatPedIts.ReopenPediGru()');
  //
  EdQtdAFat.ValueVariant := QrPediGruTotQuantF.Value;
  EdQtdFat.ValueVariant  := QrPediGruTotQuantV.Value;
  //
  if Nivel1 <> 0 then
    QrPediGru.Locate('Nivel1', Nivel1, []);
end;

{
procedure TFmFatPedIts.ReopenPrecoOld(Nivel1, GraGruX: Integer);
begin
  QrPreco.Close;
  QrPreco.SQL.Clear;
  QrPreco.SQL.Add('SELECT Preco');
  QrPreco.SQL.Add('FROM tabeprcgri');
  QrPreco.SQL.Add('WHERE GraGruX=:P0');
  QrPreco.SQL.Add('AND TabePrcCab=:P1');
  QrPreco.Params[00].AsInteger := GraGruX;
  QrPreco.Params[01].AsInteger := FmFatPedCab2.QrFatPedCabTabelaPrc.Value;
  UMyMod.AbreQuery(QrPreco, 'TFmFatPedIts.ReopenPrecoOld()');
  if QrPreco.RecordCount = 0 then
  begin
    QrPreco.Close;
    QrPreco.SQL.Clear;
    QrPreco.SQL.Add('SELECT Preco');
    QrPreco.SQL.Add('FROM tabeprcgrg');
    QrPreco.SQL.Add('WHERE Nivel1=:P0');
    QrPreco.SQL.Add('AND Codigo=:P0');
    QrPreco.Params[00].AsInteger := Nivel1;
    QrPreco.Params[01].AsInteger := FmFatPedCab2.QrFatPedCabTabelaPrc.Value;
    UMyMod.AbreQuery(QrPreco, 'TFmFatPedIts.ReopenPrecoOld()');
  end;
end;
}

procedure TFmFatPedIts2.ReopenPrecoFat(Controle: Integer);
begin
  QrPreco.Close;
  QrPreco.SQL.Clear;
  QrPreco.SQL.Add('SELECT pvi.PrecoF, InfAdCuztm, ');
  QrPreco.SQL.Add('(QuantP-QuantC-QuantV) QuantF, ');
  QrPreco.SQL.Add('PercCustom, MedidaC, MedidaL, ');
  QrPreco.SQL.Add('MedidaA, MedidaE, MedOrdem ');
  QrPreco.SQL.Add('FROM pedivdaits pvi ');
  QrPreco.SQL.Add('LEFT JOIN gragrux ggx ON ggx.Controle=pvi.GraGruX ');
  QrPreco.SQL.Add('LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1 ');
  // aqui muda
  QrPreco.SQL.Add('WHERE pvi.Controle=:P0');
  QrPreco.Params[00].AsInteger := Controle;
  UMyMod.AbreQuery(QrPreco, Dmod.MyDB, 'TFmFatPedIts.ReopenPrecoFat()');
end;

procedure TFmFatPedIts2.ReopenPrecoSel(PediVda, GraGruX: Integer);
begin
  QrPreco.Close;
  QrPreco.Close;
  QrPreco.SQL.Clear;
  QrPreco.SQL.Add('SELECT pvi.PrecoF, InfAdCuztm, ');
  QrPreco.SQL.Add('(QuantP-QuantC-QuantV) QuantF, ');
  QrPreco.SQL.Add('PercCustom, MedidaC, MedidaL, ');
  QrPreco.SQL.Add('MedidaA, MedidaE, MedOrdem ');
  QrPreco.SQL.Add('FROM pedivdaits pvi ');
  QrPreco.SQL.Add('LEFT JOIN gragrux ggx ON ggx.Controle=pvi.GraGruX ');
  QrPreco.SQL.Add('LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1 ');
  // aqui muda
  QrPreco.SQL.Add('WHERE pvi.Codigo=:P0');
  QrPreco.SQL.Add('AND pvi.GraGruX=:P1');
  QrPreco.Params[00].AsInteger := PediVda;
  QrPreco.Params[01].AsInteger := GraGruX;
  UMyMod.AbreQuery(QrPreco, Dmod.MyDB, 'TFmFatPedIts.ReopenPrecoNew()');
end;

procedure TFmFatPedIts2.ReopenItensFat(PediVda: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrItem2, Dmod.MyDB, [
    'SELECT pvi.PrecoF, InfAdCuztm, ',
    '(QuantP-QuantC-QuantV) QuantF, ',
    'PercCustom, MedidaC, MedidaL, ',
    'MedidaA, MedidaE, MedOrdem, ',
    'gg1.Nome NOMENIVEL1, ggc.GraCorCad, ',
    'gcc.Nome NOMECOR,  gti.Nome NOMETAM, ',
    'ggx.Controle GraGruX, ggx.GraGru1, ',
    'gg1.CodUsu CU_Nivel1, gg1.IPI_Alq, ',
    'pgt.MadeBy, pgt.Fracio, ',
    'gg1.HowBxaEstq, gg1.GerBxaEstq, gg1.prod_indTot ',
    'FROM pedivdaits pvi ',
    'LEFT JOIN gragrux ggx ON ggx.Controle=pvi.GraGruX ',
    'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1 ',
    'LEFT JOIN gragruc ggc ON ggc.Controle=ggx.GraGruC ',
    'LEFT JOIN gracorcad gcc ON gcc.Codigo=ggc.GraCorCad ',
    'LEFT JOIN gratamits gti ON gti.Controle=ggx.GraTamI ',
    'LEFT JOIN prdgruptip pgt ON pgt.Codigo=gg1.PrdGrupTip ',
    'WHERE pvi.Codigo=' + Geral.FF0(PediVda),
    '']);
end;

end.

