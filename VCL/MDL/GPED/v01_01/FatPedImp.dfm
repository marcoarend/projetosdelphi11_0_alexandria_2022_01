object FmFatPedImp: TFmFatPedImp
  Left = 339
  Top = 185
  Caption = 'FAT-PEDID-008 :: Impress'#227'o de Romaneio de Pedido'
  ClientHeight = 480
  ClientWidth = 662
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -14
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 120
  TextHeight = 16
  object Panel1: TPanel
    Left = 0
    Top = 59
    Width = 662
    Height = 288
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object RGTipoRomaneio: TRadioGroup
      Left = 0
      Top = 0
      Width = 662
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alTop
      Caption = ' Tipo de Romaneio: '
      Columns = 3
      ItemIndex = 0
      Items.Strings = (
        'Nenhum'
        'Grupos de produtos'
        'Reduzidos (Cor/Tamanho)')
      TabOrder = 0
      OnClick = RGTipoRomaneioClick
    end
    object RGAgruparPorVolume: TRadioGroup
      Left = 0
      Top = 59
      Width = 662
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alTop
      Caption = ' Agrupar por volume: '
      Columns = 3
      ItemIndex = 2
      Items.Strings = (
        'N'#227'o'
        'Sim'
        '?')
      TabOrder = 1
      OnClick = RGAgruparPorVolumeClick
    end
    object CGMostrar: TdmkCheckGroup
      Left = 0
      Top = 177
      Width = 662
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alTop
      Caption = ' Informa'#231#245'es a serem mostradas: '
      Columns = 3
      ItemIndex = 0
      Items.Strings = (
        'Valores (unit. e total)'
        '????'
        'Parcelamento')
      TabOrder = 2
      UpdType = utYes
      Value = 1
      OldValor = 0
    end
    object CkLinhas: TCheckBox
      Left = 10
      Top = 246
      Width = 134
      Height = 21
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Linhas entre itens.'
      Checked = True
      State = cbChecked
      TabOrder = 3
    end
    object RGOrdem: TRadioGroup
      Left = 0
      Top = 118
      Width = 662
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alTop
      Caption = ' Ordenar itens por: '
      Columns = 2
      ItemIndex = 0
      Items.Strings = (
        'C'#243'digo do produto'
        'Nome do produto')
      TabOrder = 4
      OnClick = RGAgruparPorVolumeClick
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 662
    Height = 59
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 603
      Top = 0
      Width = 59
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 10
        Top = 14
        Width = 39
        Height = 39
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 59
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 59
      Top = 0
      Width = 544
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 9
        Top = 11
        Width = 499
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Impress'#227'o de Romaneio de Pedido'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 11
        Top = 14
        Width = 499
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Impress'#227'o de Romaneio de Pedido'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 10
        Top = 12
        Width = 499
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Impress'#227'o de Romaneio de Pedido'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 347
    Width = 662
    Height = 54
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 18
      Width = 658
      Height = 34
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 16
        Top = 2
        Width = 150
        Height = 19
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -17
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 15
        Top = 1
        Width = 150
        Height = 19
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -17
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 401
    Width = 662
    Height = 79
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alBottom
    TabOrder = 3
    object Panel3: TPanel
      Left = 2
      Top = 18
      Width = 658
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object PnSaiDesis: TPanel
        Left = 481
        Top = 0
        Width = 177
        Height = 59
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtSaida: TBitBtn
          Tag = 13
          Left = 2
          Top = 4
          Width = 148
          Height = 49
          Cursor = crHandPoint
          Hint = 'Sai da janela atual'
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Sa'#237'da'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
        end
      end
      object BtOK: TBitBtn
        Tag = 14
        Left = 25
        Top = 4
        Width = 147
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&OK'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object QrSmvaEmp: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT COUNT(DISTINCT Empresa) Empresas'
      'FROM stqmovvala smva'
      'WHERE smva.Tipo=1'
      'AND smva.OriCodi=:P0')
    Left = 384
    Top = 100
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSmvaEmpEmpresas: TLargeintField
      FieldName = 'Empresas'
      Required = True
    end
  end
  object frxDsLctosV: TfrxDBDataset
    UserName = 'frxDsLctosV'
    CloseDataSource = False
    DataSet = QrLctosV
    BCDToCurrency = False
    Left = 440
    Top = 100
  end
  object QrLctosV: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT FatNum, FatParcela, SUM(Credito) Credito, Vencimento'
      'FROM lanctos'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'GROUP BY FatParcela'
      'ORDER BY Vencimento, FatParcela')
    Left = 416
    Top = 100
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrLctosVFatParcela: TIntegerField
      FieldName = 'FatParcela'
    end
    object QrLctosVCredito: TFloatField
      FieldName = 'Credito'
    end
    object QrLctosVVencimento: TDateField
      FieldName = 'Vencimento'
    end
    object QrLctosVFatNum: TFloatField
      FieldName = 'FatNum'
    end
  end
  object QrRomaneioCT: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrRomaneioCTCalcFields
    SQL.Strings = (
      'SELECT ggx.GraGruC, ggx.GraTamI, ggc.GraCorCad, '
      'gcc.Nome NO_COR, gcc.CodUsu CU_Cor, gti.Nome NO_Tam,'
      'smva.OriCodi, 0.0+smva.OriCnta OriCnta, '
      
        'IF(SUM(smva.Total) < 0, -SUM(smva.Total), SUM(smva.Total)) Total' +
        ', '
      'SUM(smva.Total) / SUM(smva.Qtde) PrecoCalc,'
      'SUM(smva.Qtde) Qtde, med.Nome NO_UniMed,'
      'gg1.CodUsu CU_NIVEL1, gg1.Nome NO_NIVEL1'
      'FROM stqmovvala smva'
      'LEFT JOIN gragrux ggx ON ggx.Controle=smva.GraGruX'
      'LEFT JOIN gragruc ggc ON ggc.Controle=ggx.GraGruC'
      'LEFT JOIN gracorcad gcc ON gcc.Codigo=ggc.GraCorCad'
      'LEFT JOIN gratamits gti ON gti.Controle=ggx.GraTamI'
      'LEFT JOIN gragru1   gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN fatpedvol fpv ON fpv.Cnta=smva.OriCnta'
      'LEFT JOIN unidmed med ON med.Codigo=fpv.UnidMed'
      'WHERE smva.Tipo=:P0'
      'AND smva.Empresa=:P1'
      'AND smva.OriCodi=:P2'
      'GROUP BY '
      'smva.OriCnta, '
      'ggx.GraGru1'
      '  , ggc.GraCorCad, ggx.GraTamI'
      'ORDER BY '
      'smva.OriCnta, '
      'NO_NIVEL1'
      '  , gcc.Nome, gti.Nome')
    Left = 384
    Top = 156
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrRomaneioCTGraGruC: TIntegerField
      FieldName = 'GraGruC'
    end
    object QrRomaneioCTGraTamI: TIntegerField
      FieldName = 'GraTamI'
    end
    object QrRomaneioCTGraCorCad: TIntegerField
      FieldName = 'GraCorCad'
    end
    object QrRomaneioCTNO_COR: TWideStringField
      FieldName = 'NO_COR'
      Size = 30
    end
    object QrRomaneioCTCU_Cor: TIntegerField
      FieldName = 'CU_Cor'
    end
    object QrRomaneioCTNO_Tam: TWideStringField
      FieldName = 'NO_Tam'
      Size = 5
    end
    object QrRomaneioCTOriCodi: TIntegerField
      FieldName = 'OriCodi'
      Required = True
    end
    object QrRomaneioCTTotal: TFloatField
      FieldName = 'Total'
    end
    object QrRomaneioCTPrecoCalc: TFloatField
      FieldName = 'PrecoCalc'
    end
    object QrRomaneioCTQtde: TFloatField
      FieldName = 'Qtde'
    end
    object QrRomaneioCTNO_UniMed: TWideStringField
      FieldName = 'NO_UniMed'
      Size = 30
    end
    object QrRomaneioCTCU_NIVEL1: TIntegerField
      FieldName = 'CU_NIVEL1'
    end
    object QrRomaneioCTNO_NIVEL1: TWideStringField
      FieldName = 'NO_NIVEL1'
      Size = 30
    end
    object QrRomaneioCTOriCnta: TFloatField
      FieldName = 'OriCnta'
      Required = True
    end
    object QrRomaneioCTTOTAL_POSI: TFloatField
      FieldKind = fkCalculated
      FieldName = 'TOTAL_POSI'
      Calculated = True
    end
    object QrRomaneioCTQTDE_POSI: TFloatField
      FieldKind = fkCalculated
      FieldName = 'QTDE_POSI'
      Calculated = True
    end
  end
  object frxDsRomaneioCT: TfrxDBDataset
    UserName = 'frxDsRomaneioCT'
    CloseDataSource = False
    DataSet = QrRomaneioCT
    BCDToCurrency = False
    Left = 412
    Top = 156
  end
  object frxFAT_PEDID_008_02: TfrxReport
    Version = '5.5'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39956.739524675900000000
    ReportOptions.LastChange = 39956.739524675900000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'procedure PageHeader1OnBeforePrint(Sender: TfrxComponent);'
      'var'
      '  Mostra: Boolean;'
      '  Fmt: String;                             '
      'begin'
      
        '  MeVARF_CODI_FRX.Text := Copy(Report.Name, 4, Length(Report.Nam' +
        'e));'
      '  if <MeuLogo3x1Existe> = True then '
      '    Picture1.LoadFromFile(<MeuLogo3x1Caminho>);'
      '  //'
      '  Mostra := <VARF_AGRUPA_VOLUME>;                              '
      
        '  GroupHeader2.Visible := Mostra;                               ' +
        '                                  '
      
        '  GroupFooter2.Visible := Mostra;                               ' +
        '                                  '
      '  //'
      
        '  Mostra := <VARF_MOSTRA_PARCELAMENTO>;                         ' +
        '     '
      
        '  GroupHeader3.Visible := Mostra;                               ' +
        '                                  '
      
        '  GroupFooter3.Visible := Mostra;                               ' +
        '                                  '
      '  MasterData3.Visible  := Mostra;'
      '  //'
      '  MD_Desconto.Visible := <VARF_DESCONTO_GERAL_VAL> >= 0.01;'
      '  MD_Comissao.Visible := <VARF_COMISSAO_VAL> >= 0.01;'
      
        '  MD_ValLiq.Visible   := MD_Comissao.Visible or MD_Desconto.Visi' +
        'ble;'
      '  //'
      '  Mostra := <VARF_MOSTRA_VALORES>;'
      
        '  Fmt    := <VARF_FORMATA_QTDE>;                                ' +
        '         '
      '  if Mostra then'
      '  begin'
      
        '    MeTitA.Memo.Text := '#39'Qtde'#39';                                 ' +
        '                                '
      
        '    MeTitB.Memo.Text := '#39'Vlr.Unit.'#39';                            ' +
        '                                     '
      
        '    MeTitC.Memo.Text := '#39'Vlr.Total'#39';                            ' +
        '                                     '
      '    //          '
      
        '    MeValA.Memo.Text := '#39'[FormatFloat('#39#39#39'+Fmt+'#39#39#39', <frxDsRomanei' +
        'oCT."QTDE_POSI">)]'#39';'
      
        '    MeValB.Memo.Text := '#39'[FormatFloat('#39#39'#,###,##0.00'#39#39', <frxDsRo' +
        'maneioCT."PrecoCalc">)]'#39';                                       ' +
        '                          '
      
        '    MeValC.Memo.Text := '#39'[FormatFloat('#39#39'#,###,##0.00'#39#39', <frxDsRo' +
        'maneioCT."TOTAL_POSI">)]'#39';                                      ' +
        '                           '
      '    //'
      
        '    MeSumA.Memo.Text := '#39'[FormatFloat('#39#39#39'+Fmt+'#39#39#39', SUM(<frxDsRom' +
        'aneioCT."QTDE_POSI">))]'#39';                                       ' +
        '                          '
      
        '    MeSumB.Memo.Text := '#39#39';                                     ' +
        '                            '
      
        '    MeSumC.Memo.Text := '#39'[FormatFloat('#39#39'#,###,##0.00'#39#39', SUM(<frx' +
        'DsRomaneioCT."TOTAL_POSI">))]'#39';                                 ' +
        '                                '
      '    //        '
      '  end else begin'
      
        '    MeTitA.Memo.Text := '#39#39';                                     ' +
        '                            '
      
        '    MeTitB.Memo.Text := '#39#39';                                     ' +
        '                            '
      
        '    MeTitC.Memo.Text := '#39'Qtde.'#39';                                ' +
        '                                 '
      '    //          '
      
        '    MeValA.Memo.Text := '#39#39';                                     ' +
        '                            '
      
        '    MeValB.Memo.Text := '#39#39';                                     ' +
        '                            '
      
        '    MeValC.Memo.Text := '#39'[FormatFloat('#39#39#39'+Fmt+'#39#39#39', <frxDsRomanei' +
        'oCT."QTDE_POSI">)]'#39';                                            ' +
        '                     '
      '    //          '
      
        '    MeSumA.Memo.Text := '#39#39';                                     ' +
        '                            '
      
        '    MeSumB.Memo.Text := '#39#39';                                     ' +
        '                            '
      
        '    MeSumC.Memo.Text := '#39'[FormatFloat('#39#39#39'+Fmt+'#39#39#39', SUM(<frxDsRom' +
        'aneioCT."QTDE_POSI">))]'#39';                                       ' +
        '                          '
      '    //        '
      '  end;            '
      
        '  Line2.Visible := <VARF_MOSTRA_LINHA>;                         ' +
        '                     '
      'end;'
      ''
      'begin'
      'end.')
    OnGetValue = frxFAT_PEDID_008_01GetValue
    Left = 440
    Top = 156
    Datasets = <
      item
        DataSet = frxDsCli
        DataSetName = 'frxDsCli'
      end
      item
        DataSet = DModG.frxDsEndereco
        DataSetName = 'frxDsEndereco'
      end
      item
        DataSet = frxDsLctosV
        DataSetName = 'frxDsLctosV'
      end
      item
        DataSet = frxDsRomaneioCT
        DataSetName = 'frxDsRomaneioCT'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 15.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        Height = 188.976500000000000000
        Top = 18.897650000000000000
        Width = 699.213050000000000000
        OnBeforePrint = 'PageHeader1OnBeforePrint'
        object Shape1: TfrxShapeView
          Width = 699.213050000000000000
          Height = 56.692950000000000000
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo13: TfrxMemoView
          Left = 154.960730000000000000
          Width = 393.071120000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsEndereco."NO_2_ENT"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line1: TfrxLineView
          Left = 551.811380000000000000
          Top = 18.897650000000000000
          Width = 147.401670000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo14: TfrxMemoView
          Left = 154.960730000000000000
          Top = 18.897650000000000000
          Width = 393.071120000000000000
          Height = 37.795275590000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsEndereco."E_ALL"]')
          ParentFont = False
        end
        object Memo15: TfrxMemoView
          Left = 548.031850000000000000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[Date]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo16: TfrxMemoView
          Left = 548.031850000000000000
          Top = 18.897650000000000000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page] de [TotalPages]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Picture1: TfrxPictureView
          Left = 3.779530000000000000
          Top = 3.779530000000000000
          Width = 147.401574800000000000
          Height = 49.133890000000000000
          HightQuality = False
          Transparent = False
          TransparentColor = clWhite
        end
        object Shape2: TfrxShapeView
          Top = 68.031540000000000000
          Width = 699.213050000000000000
          Height = 75.590600000000000000
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo2: TfrxMemoView
          Left = 75.590600000000000000
          Top = 68.031540000000000000
          Width = 616.063390000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsCli."CodUsu"] - [frxDsCli."NOME_ENT"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo4: TfrxMemoView
          Left = 75.590600000000000000
          Top = 86.929190000000000000
          Width = 616.063390000000000000
          Height = 37.795275590000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsCli."E_ALL"]')
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          Left = 3.779530000000000000
          Top = 68.031540000000000000
          Width = 71.811070000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            'Destinat'#225'rio:')
          ParentFont = False
        end
        object Memo40: TfrxMemoView
          Top = 170.078850000000000000
          Width = 60.472440940000000000
          Height = 18.897650000000000000
          DataSet = frxDsRomaneioCT
          DataSetName = 'frxDsRomaneioCT'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'C'#243'digo')
          ParentFont = False
          WordWrap = False
        end
        object MeTitD: TfrxMemoView
          Left = 60.472480000000000000
          Top = 170.078850000000000000
          Width = 204.094620000000000000
          Height = 18.897650000000000000
          DataSet = frxDsRomaneioCT
          DataSetName = 'frxDsRomaneioCT'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Descri'#231#227'o')
          ParentFont = False
        end
        object MeTitC: TfrxMemoView
          Left = 638.740570000000000000
          Top = 170.078850000000000000
          Width = 60.472440940000000000
          Height = 18.897650000000000000
          DataSet = frxDsRomaneioCT
          DataSetName = 'frxDsRomaneioCT'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Vlr. Total')
          ParentFont = False
          WordWrap = False
        end
        object MeTitB: TfrxMemoView
          Left = 578.268090000000000000
          Top = 170.078850000000000000
          Width = 60.472440940000000000
          Height = 18.897650000000000000
          DataSet = frxDsRomaneioCT
          DataSetName = 'frxDsRomaneioCT'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Vlr. Unit.')
          ParentFont = False
          WordWrap = False
        end
        object MeTitA: TfrxMemoView
          Left = 517.795610000000000000
          Top = 170.078850000000000000
          Width = 60.472440940000000000
          Height = 18.897650000000000000
          DataSet = frxDsRomaneioCT
          DataSetName = 'frxDsRomaneioCT'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Qtde.')
          ParentFont = False
          WordWrap = False
        end
        object Memo7: TfrxMemoView
          Left = 264.567100000000000000
          Top = 170.078850000000000000
          Width = 204.094620000000000000
          Height = 18.897650000000000000
          DataSet = frxDsRomaneioCT
          DataSetName = 'frxDsRomaneioCT'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Cor')
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          Left = 468.661720000000000000
          Top = 170.078850000000000000
          Width = 49.133858270000000000
          Height = 18.897650000000000000
          DataSet = frxDsRomaneioCT
          DataSetName = 'frxDsRomaneioCT'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Tamanho')
          ParentFont = False
        end
        object Memo29: TfrxMemoView
          Left = 132.283550000000000000
          Top = 124.724490000000000000
          Width = 170.078850000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsCli."CNPJ_TXT"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo30: TfrxMemoView
          Left = 75.590600000000000000
          Top = 124.724490000000000000
          Width = 56.692950000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsCli."CAD_FEDERAL"]:')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo38: TfrxMemoView
          Left = 359.055350000000000000
          Top = 124.724490000000000000
          Width = 151.181200000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsCli."IE_TXT"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo39: TfrxMemoView
          Left = 302.362400000000000000
          Top = 124.724490000000000000
          Width = 56.692950000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsCli."CAD_ESTADUAL"]:')
          ParentFont = False
          VAlign = vaCenter
        end
        object frxDsEnderecoTE1: TfrxMemoView
          Left = 548.031850000000000000
          Top = 37.795300000000000000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          DataSet = DModG.frxDsEndereco
          DataSetName = 'frxDsEndereco'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsEndereco."TE1_TXT"]')
          ParentFont = False
        end
        object Memo41: TfrxMemoView
          Left = 566.929500000000000000
          Top = 124.724490000000000000
          Width = 124.724490000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsCli."TE1_TXT"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo42: TfrxMemoView
          Left = 510.236550000000000000
          Top = 124.724490000000000000
          Width = 56.692950000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            'Telefone:')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo6: TfrxMemoView
          Left = 3.779530000000000000
          Top = 147.401670000000000000
          Width = 374.173470000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            'ROMANEIO DE [VARF_ENTRA_SAI]')
          ParentFont = False
        end
        object Memo18: TfrxMemoView
          Left = 623.622450000000000000
          Top = 147.401670000000000000
          Width = 75.590600000000000000
          Height = 18.897650000000000000
          DataSet = frxDsRomaneioGP
          DataSetName = 'frxDsRomaneioGP'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_PEDIDO]')
          ParentFont = False
          WordWrap = False
        end
        object Memo28: TfrxMemoView
          Left = 381.732530000000000000
          Top = 147.401670000000000000
          Width = 241.889920000000000000
          Height = 18.897650000000000000
          DataSet = frxDsRomaneioGP
          DataSetName = 'frxDsRomaneioGP'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_TIT_PED]:')
          ParentFont = False
          WordWrap = False
        end
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        Height = 15.118120000000000000
        Top = 332.598640000000000000
        Width = 699.213050000000000000
        DataSet = frxDsRomaneioCT
        DataSetName = 'frxDsRomaneioCT'
        RowCount = 0
        object Memo3: TfrxMemoView
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          DataSet = frxDsRomaneioCT
          DataSetName = 'frxDsRomaneioCT'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsRomaneioCT."CU_NIVEL1"] - ')
          ParentFont = False
          WordWrap = False
        end
        object Memo21: TfrxMemoView
          Left = 60.472480000000000000
          Width = 204.094620000000000000
          Height = 15.118110240000000000
          DataField = 'NO_NIVEL1'
          DataSet = frxDsRomaneioCT
          DataSetName = 'frxDsRomaneioCT'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsRomaneioCT."NO_NIVEL1"]')
          ParentFont = False
        end
        object MeValC: TfrxMemoView
          Left = 638.740570000000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          DataSet = frxDsRomaneioCT
          DataSetName = 'frxDsRomaneioCT'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
          WordWrap = False
        end
        object MeValB: TfrxMemoView
          Left = 578.268090000000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          DataSet = frxDsRomaneioCT
          DataSetName = 'frxDsRomaneioCT'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
          WordWrap = False
        end
        object MeValA: TfrxMemoView
          Left = 517.795610000000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          DataSet = frxDsRomaneioCT
          DataSetName = 'frxDsRomaneioCT'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
          WordWrap = False
        end
        object Memo8: TfrxMemoView
          Left = 264.567100000000000000
          Width = 60.472480000000000000
          Height = 15.118110240000000000
          DataField = 'CU_Cor'
          DataSet = frxDsRomaneioCT
          DataSetName = 'frxDsRomaneioCT'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsRomaneioCT."CU_Cor"]')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          Left = 468.661720000000000000
          Width = 49.133858270000000000
          Height = 15.118110240000000000
          DataField = 'NO_Tam'
          DataSet = frxDsRomaneioCT
          DataSetName = 'frxDsRomaneioCT'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsRomaneioCT."NO_Tam"]')
          ParentFont = False
        end
        object Memo11: TfrxMemoView
          Left = 325.039580000000000000
          Width = 143.622140000000000000
          Height = 15.118110240000000000
          DataField = 'NO_COR'
          DataSet = frxDsRomaneioCT
          DataSetName = 'frxDsRomaneioCT'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsRomaneioCT."NO_COR"]')
          ParentFont = False
        end
        object Line2: TfrxLineView
          Top = 15.118120000000000000
          Width = 699.213050000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        Height = 13.228346460000000000
        Top = 763.465060000000000000
        Width = 699.213050000000000000
        object Memo1: TfrxMemoView
          Left = 7.559060000000000000
          Width = 362.834880000000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - software customizado')
          ParentFont = False
          VAlign = vaCenter
        end
        object MeVARF_CODI_FRX: TfrxMemoView
          Left = 370.393940000000000000
          Width = 321.260050000000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'VARF_CODI_FRX')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object GroupHeader1: TfrxGroupHeader
        FillType = ftBrush
        Height = 3.779530000000000000
        Top = 268.346630000000000000
        Width = 699.213050000000000000
        Condition = 'frxDsRomaneioCT."OriCodi"'
      end
      object GroupFooter1: TfrxGroupFooter
        FillType = ftBrush
        Height = 15.118110240000000000
        Top = 404.409710000000000000
        Width = 699.213050000000000000
        object MeSumC: TfrxMemoView
          Left = 638.740570000000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          DataSet = frxDsRomaneioCT
          DataSetName = 'frxDsRomaneioCT'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsRomaneioCT."TOTAL_POSI">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo20: TfrxMemoView
          Width = 517.795610000000000000
          Height = 15.118110240000000000
          DataSet = frxDsRomaneioCT
          DataSetName = 'frxDsRomaneioCT'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Total: ')
          ParentFont = False
          WordWrap = False
        end
        object MeSumA: TfrxMemoView
          Left = 517.795610000000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          DataSet = frxDsRomaneioCT
          DataSetName = 'frxDsRomaneioCT'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsRomaneioCT."QTDE_POSI">)]')
          ParentFont = False
          WordWrap = False
        end
        object MeSumB: TfrxMemoView
          Left = 578.268090000000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          DataSet = frxDsRomaneioCT
          DataSetName = 'frxDsRomaneioCT'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
          WordWrap = False
        end
      end
      object MasterData3: TfrxMasterData
        FillType = ftBrush
        Height = 15.118110236220500000
        Top = 661.417750000000000000
        Width = 699.213050000000000000
        DataSet = frxDsLctosV
        DataSetName = 'frxDsLctosV'
        RowCount = 0
        object Memo34: TfrxMemoView
          Left = 94.488250000000000000
          Width = 94.488250000000000000
          Height = 15.118110236220500000
          DataField = 'Credito'
          DataSet = frxDsLctosV
          DataSetName = 'frxDsLctosV'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsLctosV."Credito"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo35: TfrxMemoView
          Width = 94.488250000000000000
          Height = 15.118110236220500000
          DataField = 'Vencimento'
          DataSet = frxDsLctosV
          DataSetName = 'frxDsLctosV'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsLctosV."Vencimento"]')
          ParentFont = False
          WordWrap = False
        end
      end
      object GroupHeader2: TfrxGroupHeader
        FillType = ftBrush
        Height = 15.118110240000000000
        Top = 294.803340000000000000
        Width = 699.213050000000000000
        Condition = 'frxDsRomaneioCT."OriCnta"'
        object Memo12: TfrxMemoView
          Width = 90.708720000000000000
          Height = 15.118110240000000000
          DataSet = frxDsRomaneioCT
          DataSetName = 'frxDsRomaneioCT'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsRomaneioCT."NO_UniMed"] n'#186' ')
          ParentFont = False
          WordWrap = False
        end
        object Memo17: TfrxMemoView
          Left = 90.708720000000000000
          Width = 608.504330000000000000
          Height = 15.118110240000000000
          DataSet = frxDsRomaneioCT
          DataSetName = 'frxDsRomaneioCT'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[FormatFloat('#39'000000;-000000; '#39', <frxDsRomaneioCT."OriCnta">)]')
          ParentFont = False
          WordWrap = False
        end
      end
      object GroupFooter2: TfrxGroupFooter
        FillType = ftBrush
        Height = 11.338585120000000000
        Top = 370.393940000000000000
        Width = 699.213050000000000000
        object Memo19: TfrxMemoView
          Width = 699.213050000000000000
          Height = 3.779525120000000000
          DataSet = frxDsRomaneioCT
          DataSetName = 'frxDsRomaneioCT'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          ParentFont = False
          WordWrap = False
        end
      end
      object GroupHeader3: TfrxGroupHeader
        FillType = ftBrush
        Height = 52.913410240000000000
        Top = 585.827150000000000000
        Width = 699.213050000000000000
        Condition = 'frxDsLctosV."FatNum"'
        object Memo31: TfrxMemoView
          Width = 699.213050000000000000
          Height = 18.897650000000000000
          DataSet = frxDsRomaneioCT
          DataSetName = 'frxDsRomaneioCT'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Parcelamento:')
          ParentFont = False
          WordWrap = False
        end
        object Memo32: TfrxMemoView
          Top = 18.897649999999900000
          Width = 188.976500000000000000
          Height = 18.897650000000000000
          DataSet = frxDsRomaneioCT
          DataSetName = 'frxDsRomaneioCT'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Condi'#231#227'o de pagamento:')
          ParentFont = False
          WordWrap = False
        end
        object Memo33: TfrxMemoView
          Left = 188.976500000000000000
          Top = 18.897650000000000000
          Width = 510.236550000000000000
          Height = 18.897650000000000000
          DataSet = frxDsRomaneioCT
          DataSetName = 'frxDsRomaneioCT'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[VARF_NOMECONDICAOPG]')
          ParentFont = False
          WordWrap = False
        end
        object Memo36: TfrxMemoView
          Left = 94.488250000000000000
          Top = 37.795299999999900000
          Width = 94.488250000000000000
          Height = 15.118110236220500000
          DataSet = frxDsRomaneioCT
          DataSetName = 'frxDsRomaneioCT'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Valor parcela')
          ParentFont = False
          WordWrap = False
        end
        object Memo37: TfrxMemoView
          Top = 37.795299999999900000
          Width = 94.488250000000000000
          Height = 15.118110236220500000
          DataSet = frxDsRomaneioCT
          DataSetName = 'frxDsRomaneioCT'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Vencimento')
          ParentFont = False
          WordWrap = False
        end
      end
      object GroupFooter3: TfrxGroupFooter
        FillType = ftBrush
        Height = 3.779530000000000000
        Top = 699.213050000000000000
        Width = 699.213050000000000000
      end
      object MD_Desconto: TfrxMasterData
        FillType = ftBrush
        Height = 26.456710000000000000
        Top = 442.205010000000000000
        Width = 699.213050000000000000
        RowCount = 1
        object Memo22: TfrxMemoView
          Left = 623.622450000000000000
          Top = 3.779530000000000000
          Width = 75.590600000000000000
          Height = 18.897650000000000000
          DataSet = frxDsRomaneioGP
          DataSetName = 'frxDsRomaneioGP'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_DESCONTO_GERAL_VAL]')
          ParentFont = False
          WordWrap = False
        end
        object Memo24: TfrxMemoView
          Left = 472.441250000000000000
          Top = 3.779530000000000000
          Width = 75.590600000000000000
          Height = 18.897650000000000000
          DataSet = frxDsRomaneioGP
          DataSetName = 'frxDsRomaneioGP'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.4n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_DESCONTO_GERAL_PER]')
          ParentFont = False
          WordWrap = False
        end
        object Memo25: TfrxMemoView
          Left = 548.031850000000000000
          Top = 3.779530000000000000
          Width = 71.811070000000000000
          Height = 18.897650000000000000
          DataSet = frxDsRomaneioGP
          DataSetName = 'frxDsRomaneioGP'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '% de desconto: ')
          ParentFont = False
          WordWrap = False
        end
      end
      object MD_Comissao: TfrxMasterData
        FillType = ftBrush
        Height = 26.456710000000000000
        Top = 491.338900000000000000
        Width = 699.213050000000000000
        RowCount = 1
        object Memo23: TfrxMemoView
          Left = 623.622450000000000000
          Top = 3.779530000000000000
          Width = 75.590600000000000000
          Height = 18.897650000000000000
          DataSet = frxDsRomaneioGP
          DataSetName = 'frxDsRomaneioGP'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_COMISSAO_VAL]')
          ParentFont = False
          WordWrap = False
        end
        object Memo26: TfrxMemoView
          Left = 472.441250000000000000
          Top = 3.779530000000000000
          Width = 75.590600000000000000
          Height = 18.897650000000000000
          DataSet = frxDsRomaneioGP
          DataSetName = 'frxDsRomaneioGP'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.4n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_COMISSAO_PER]')
          ParentFont = False
          WordWrap = False
        end
        object Memo27: TfrxMemoView
          Left = 548.031850000000000000
          Top = 3.779530000000000000
          Width = 71.811070000000000000
          Height = 18.897650000000000000
          DataSet = frxDsRomaneioGP
          DataSetName = 'frxDsRomaneioGP'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '% de comiss'#227'o: ')
          ParentFont = False
          WordWrap = False
        end
      end
      object MD_ValLiq: TfrxMasterData
        FillType = ftBrush
        Height = 22.677180000000000000
        Top = 540.472790000000000000
        Width = 699.213050000000000000
        RowCount = 1
        object Memo43: TfrxMemoView
          Left = 623.622450000000000000
          Top = 3.779530000000000000
          Width = 75.590600000000000000
          Height = 15.118110240000000000
          DataSet = frxDsRomaneioGP
          DataSetName = 'frxDsRomaneioGP'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_VAL_LIQ]')
          ParentFont = False
          WordWrap = False
        end
        object Memo44: TfrxMemoView
          Top = 3.779530000000000000
          Width = 623.622450000000000000
          Height = 15.118110240000000000
          DataSet = frxDsRomaneioGP
          DataSetName = 'frxDsRomaneioGP'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Total l'#237'quido: ')
          ParentFont = False
          WordWrap = False
        end
      end
    end
  end
  object frxDsRomaneioGP: TfrxDBDataset
    UserName = 'frxDsRomaneioGP'
    CloseDataSource = False
    DataSet = QrRomaneioGP
    BCDToCurrency = False
    Left = 412
    Top = 128
  end
  object frxFAT_PEDID_008_01: TfrxReport
    Version = '5.5'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39956.739524675940000000
    ReportOptions.LastChange = 39956.739524675940000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'var'
      '  Mostra: Boolean;'
      '  Fmt: String;                             '
      'begin'
      
        '  MeVARF_CODI_FRX.Text := Copy(Report.Name, 4, Length(Report.Nam' +
        'e));'
      '  if <MeuLogo3x1Existe> = True then '
      '    Picture1.LoadFromFile(<MeuLogo3x1Caminho>);'
      '  //'
      '  Mostra := <VARF_AGRUPA_VOLUME>;                              '
      
        '  GroupHeader2.Visible := Mostra;                               ' +
        '                                  '
      
        '  GroupFooter2.Visible := Mostra;                               ' +
        '                                  '
      '  //'
      
        '  Mostra := <VARF_MOSTRA_PARCELAMENTO>;                         ' +
        '     '
      
        '  GroupHeader3.Visible := Mostra;                               ' +
        '                                  '
      
        '  GroupFooter3.Visible := Mostra;                               ' +
        '                                  '
      '  MasterData3.Visible  := Mostra;'
      '  //'
      '  MD_Desconto.Visible := <VARF_DESCONTO_GERAL_VAL> >= 0.01;'
      '  MD_Comissao.Visible := <VARF_COMISSAO_VAL> >= 0.01;'
      
        '  MD_ValLiq.Visible   := MD_Comissao.Visible or MD_Desconto.Visi' +
        'ble;'
      '  //'
      '  Mostra := <VARF_MOSTRA_VALORES>;'
      
        '  Fmt    := <VARF_FORMATA_QTDE>;                                ' +
        '         '
      '  if Mostra then'
      '  begin'
      
        '    MeTitA.Memo.Text := '#39'Qtde'#39';                                 ' +
        '                                '
      
        '    MeTitB.Memo.Text := '#39'Vlr.Unit.'#39';                            ' +
        '                                     '
      
        '    MeTitC.Memo.Text := '#39'Vlr.Total'#39';                            ' +
        '                                     '
      '    //          '
      
        '    MeValA.Memo.Text := '#39'[FormatFloat('#39#39#39'+Fmt+'#39#39#39', <frxDsRomanei' +
        'oGP."QTDE_POSI">)]'#39';                                            ' +
        '                     '
      
        '    MeValB.Memo.Text := '#39'[FormatFloat('#39#39'#,###,##0.00'#39#39', <frxDsRo' +
        'maneioGP."PrecoCalc">)]'#39';                                       ' +
        '                          '
      
        '    MeValC.Memo.Text := '#39'[FormatFloat('#39#39'#,###,##0.00'#39#39', <frxDsRo' +
        'maneioGP."TOTAL_POSI">)]'#39';                                      ' +
        '                           '
      '    //'
      
        '    MeSumA.Memo.Text := '#39'[FormatFloat('#39#39#39'+Fmt+'#39#39#39', SUM(<frxDsRom' +
        'aneioGP."QTDE_POSI">))]'#39';                                       ' +
        '                          '
      
        '    MeSumB.Memo.Text := '#39#39';                                     ' +
        '                            '
      
        '    MeSumC.Memo.Text := '#39'[FormatFloat('#39#39'#,###,##0.00'#39#39', SUM(<frx' +
        'DsRomaneioGP."TOTAL_POSI">))]'#39';                                 ' +
        '                                '
      '    //        '
      '  end else begin'
      
        '    MeTitA.Memo.Text := '#39#39';                                     ' +
        '                            '
      
        '    MeTitB.Memo.Text := '#39#39';                                     ' +
        '                            '
      
        '    MeTitC.Memo.Text := '#39'Qtde.'#39';                                ' +
        '                                 '
      '    //          '
      
        '    MeValA.Memo.Text := '#39#39';                                     ' +
        '                            '
      
        '    MeValB.Memo.Text := '#39#39';                                     ' +
        '                            '
      
        '    MeValC.Memo.Text := '#39'[FormatFloat('#39#39#39'+Fmt+'#39#39#39', <frxDsRomanei' +
        'oGP."QTDE_POSI">)]'#39';                                            ' +
        '                     '
      '    //          '
      
        '    MeSumA.Memo.Text := '#39#39';                                     ' +
        '                            '
      
        '    MeSumB.Memo.Text := '#39#39';                                     ' +
        '                            '
      
        '    MeSumC.Memo.Text := '#39'[FormatFloat('#39#39#39'+Fmt+'#39#39#39', SUM(<frxDsRom' +
        'aneioGP."QTDE_POSI">))]'#39';                                       ' +
        '                          '
      '    //        '
      '  end;'
      
        '  Line2.Visible := <VARF_MOSTRA_LINHA>;                         ' +
        '                     '
      'end.')
    OnGetValue = frxFAT_PEDID_008_01GetValue
    Left = 440
    Top = 128
    Datasets = <
      item
        DataSet = frxDsCli
        DataSetName = 'frxDsCli'
      end
      item
        DataSet = DModG.frxDsEndereco
        DataSetName = 'frxDsEndereco'
      end
      item
        DataSet = frxDsLctosV
        DataSetName = 'frxDsLctosV'
      end
      item
        DataSet = frxDsRomaneioGP
        DataSetName = 'frxDsRomaneioGP'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 15.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        Height = 188.976500000000000000
        Top = 18.897650000000000000
        Width = 699.213050000000000000
        object Shape1: TfrxShapeView
          Width = 699.213050000000000000
          Height = 56.692950000000000000
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo13: TfrxMemoView
          Left = 154.960730000000000000
          Width = 393.071120000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsEndereco."NO_2_ENT"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line1: TfrxLineView
          Left = 551.811380000000000000
          Top = 18.897650000000000000
          Width = 147.401670000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo14: TfrxMemoView
          Left = 154.960730000000000000
          Top = 18.897650000000000000
          Width = 393.071120000000000000
          Height = 37.795275590000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsEndereco."E_ALL"]')
          ParentFont = False
        end
        object Memo15: TfrxMemoView
          Left = 548.031850000000000000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[Date]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo16: TfrxMemoView
          Left = 548.031850000000000000
          Top = 18.897650000000000000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page] de [TotalPages]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Picture1: TfrxPictureView
          Left = 3.779530000000000000
          Top = 3.779530000000001000
          Width = 147.401574800000000000
          Height = 49.133890000000000000
          HightQuality = False
          Transparent = False
          TransparentColor = clWhite
        end
        object Shape2: TfrxShapeView
          Top = 68.031540000000000000
          Width = 699.213050000000000000
          Height = 75.590600000000000000
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo2: TfrxMemoView
          Left = 75.590600000000000000
          Top = 68.031540000000000000
          Width = 616.063390000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsCli."CodUsu"] - [frxDsCli."NOME_ENT"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo4: TfrxMemoView
          Left = 75.590600000000000000
          Top = 86.929190000000000000
          Width = 616.063390000000000000
          Height = 37.795275590000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsCli."E_ALL"]')
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          Left = 3.779530000000000000
          Top = 68.031540000000010000
          Width = 71.811070000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            'Destinat'#225'rio:')
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          Left = 3.779530000000000000
          Top = 151.181200000000000000
          Width = 374.173470000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            'ROMANEIO DE [VARF_ENTRA_SAI]')
          ParentFont = False
        end
        object Memo40: TfrxMemoView
          Top = 170.078850000000000000
          Width = 75.590600000000000000
          Height = 18.897650000000000000
          DataSet = frxDsRomaneioGP
          DataSetName = 'frxDsRomaneioGP'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'C'#243'digo')
          ParentFont = False
          WordWrap = False
        end
        object MeTitD: TfrxMemoView
          Left = 75.590600000000000000
          Top = 170.078850000000000000
          Width = 377.953000000000000000
          Height = 18.897650000000000000
          DataSet = frxDsRomaneioGP
          DataSetName = 'frxDsRomaneioGP'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Descri'#231#227'o')
          ParentFont = False
        end
        object MeTitC: TfrxMemoView
          Left = 604.724800000000000000
          Top = 170.078850000000000000
          Width = 94.488250000000000000
          Height = 18.897650000000000000
          DataSet = frxDsRomaneioGP
          DataSetName = 'frxDsRomaneioGP'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Vlr. Total')
          ParentFont = False
          WordWrap = False
        end
        object MeTitB: TfrxMemoView
          Left = 529.134200000000000000
          Top = 170.078850000000000000
          Width = 75.590600000000000000
          Height = 18.897650000000000000
          DataSet = frxDsRomaneioGP
          DataSetName = 'frxDsRomaneioGP'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Vlr. Unit.')
          ParentFont = False
          WordWrap = False
        end
        object MeTitA: TfrxMemoView
          Left = 453.543600000000000000
          Top = 170.078850000000000000
          Width = 75.590600000000000000
          Height = 18.897650000000000000
          DataSet = frxDsRomaneioGP
          DataSetName = 'frxDsRomaneioGP'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Qtde.')
          ParentFont = False
          WordWrap = False
        end
        object Memo7: TfrxMemoView
          Left = 623.622450000000000000
          Top = 151.181200000000000000
          Width = 75.590600000000000000
          Height = 18.897650000000000000
          DataSet = frxDsRomaneioGP
          DataSetName = 'frxDsRomaneioGP'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_PEDIDO]')
          ParentFont = False
          WordWrap = False
        end
        object Memo8: TfrxMemoView
          Left = 381.732530000000000000
          Top = 151.181200000000000000
          Width = 241.889920000000000000
          Height = 18.897650000000000000
          DataSet = frxDsRomaneioGP
          DataSetName = 'frxDsRomaneioGP'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_TIT_PED]:')
          ParentFont = False
          WordWrap = False
        end
        object Memo29: TfrxMemoView
          Left = 132.283550000000000000
          Top = 124.724490000000000000
          Width = 170.078850000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsCli."CNPJ_TXT"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo30: TfrxMemoView
          Left = 75.590600000000000000
          Top = 124.724490000000000000
          Width = 56.692950000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsCli."CAD_FEDERAL"]:')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo38: TfrxMemoView
          Left = 359.055350000000000000
          Top = 124.724490000000000000
          Width = 151.181200000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsCli."IE_TXT"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo39: TfrxMemoView
          Left = 302.362400000000000000
          Top = 124.724490000000000000
          Width = 56.692950000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsCli."CAD_ESTADUAL"]:')
          ParentFont = False
          VAlign = vaCenter
        end
        object frxDsEnderecoTE1: TfrxMemoView
          Left = 548.031850000000000000
          Top = 37.795300000000000000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          DataSet = DModG.frxDsEndereco
          DataSetName = 'frxDsEndereco'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsEndereco."TE1_TXT"]')
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          Left = 566.929500000000000000
          Top = 124.724490000000000000
          Width = 124.724490000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsCli."TE1_TXT"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo10: TfrxMemoView
          Left = 510.236550000000000000
          Top = 124.724490000000000000
          Width = 56.692950000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            'Telefone:')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        Height = 18.897650000000000000
        Top = 332.598640000000000000
        Width = 699.213050000000000000
        DataSet = frxDsRomaneioGP
        DataSetName = 'frxDsRomaneioGP'
        RowCount = 0
        object Memo3: TfrxMemoView
          Width = 75.590600000000000000
          Height = 18.897637800000000000
          DataSet = frxDsRomaneioGP
          DataSetName = 'frxDsRomaneioGP'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsRomaneioGP."CU_NIVEL1"] - ')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo21: TfrxMemoView
          Left = 75.590600000000000000
          Width = 377.953000000000000000
          Height = 18.897637800000000000
          DataField = 'NO_NIVEL1'
          DataSet = frxDsRomaneioGP
          DataSetName = 'frxDsRomaneioGP'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsRomaneioGP."NO_NIVEL1"]')
          ParentFont = False
          VAlign = vaBottom
        end
        object MeValC: TfrxMemoView
          Left = 604.724800000000000000
          Width = 94.488250000000000000
          Height = 18.897637800000000000
          DataSet = frxDsRomaneioGP
          DataSetName = 'frxDsRomaneioGP'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsRomaneioGP."TOTAL_POSI"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object MeValB: TfrxMemoView
          Left = 529.134200000000000000
          Width = 75.590600000000000000
          Height = 18.897637800000000000
          DataSet = frxDsRomaneioGP
          DataSetName = 'frxDsRomaneioGP'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsRomaneioGP."PrecoCalc"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object MeValA: TfrxMemoView
          Left = 453.543600000000000000
          Width = 75.590600000000000000
          Height = 18.897637800000000000
          DataField = 'QTDE_POSI'
          DataSet = frxDsRomaneioGP
          DataSetName = 'frxDsRomaneioGP'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsRomaneioGP."QTDE_POSI"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Line2: TfrxLineView
          Top = 18.897650000000000000
          Width = 699.213050000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        Height = 13.228346460000000000
        Top = 774.803650000000000000
        Width = 699.213050000000000000
        object Memo1: TfrxMemoView
          Left = 7.559060000000000000
          Width = 362.834880000000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - software customizado')
          ParentFont = False
          VAlign = vaCenter
        end
        object MeVARF_CODI_FRX: TfrxMemoView
          Left = 370.393940000000000000
          Width = 321.260050000000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'VARF_CODI_FRX')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object GroupHeader1: TfrxGroupHeader
        FillType = ftBrush
        Height = 3.779530000000000000
        Top = 268.346630000000000000
        Width = 699.213050000000000000
        Condition = 'frxDsRomaneioGP."OriCodi"'
      end
      object GroupFooter1: TfrxGroupFooter
        FillType = ftBrush
        Height = 15.118110240000000000
        Top = 408.189240000000000000
        Width = 699.213050000000000000
        object MeSumC: TfrxMemoView
          Left = 604.724800000000000000
          Width = 94.488250000000000000
          Height = 15.118110240000000000
          DataSet = frxDsRomaneioGP
          DataSetName = 'frxDsRomaneioGP'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsRomaneioGP."TOTAL_POSI">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo20: TfrxMemoView
          Width = 453.543600000000000000
          Height = 15.118110240000000000
          DataSet = frxDsRomaneioGP
          DataSetName = 'frxDsRomaneioGP'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Total: ')
          ParentFont = False
          WordWrap = False
        end
        object MeSumA: TfrxMemoView
          Left = 453.543600000000000000
          Width = 75.590600000000000000
          Height = 15.118110240000000000
          DataSet = frxDsRomaneioGP
          DataSetName = 'frxDsRomaneioGP'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsRomaneioGP."QTDE_POSI">)]')
          ParentFont = False
          WordWrap = False
        end
        object MeSumB: TfrxMemoView
          Left = 529.134200000000000000
          Width = 75.590600000000000000
          Height = 15.118110240000000000
          DataSet = frxDsRomaneioGP
          DataSetName = 'frxDsRomaneioGP'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
          WordWrap = False
        end
      end
      object MasterData3: TfrxMasterData
        FillType = ftBrush
        Height = 15.118110236220470000
        Top = 672.756340000000000000
        Width = 699.213050000000000000
        DataSet = frxDsLctosV
        DataSetName = 'frxDsLctosV'
        RowCount = 0
        object Memo34: TfrxMemoView
          Left = 94.488250000000000000
          Width = 94.488250000000000000
          Height = 15.118110236220470000
          DataField = 'Credito'
          DataSet = frxDsLctosV
          DataSetName = 'frxDsLctosV'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsLctosV."Credito"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo35: TfrxMemoView
          Width = 94.488250000000000000
          Height = 15.118110236220470000
          DataField = 'Vencimento'
          DataSet = frxDsLctosV
          DataSetName = 'frxDsLctosV'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsLctosV."Vencimento"]')
          ParentFont = False
          WordWrap = False
        end
      end
      object GroupHeader2: TfrxGroupHeader
        FillType = ftBrush
        Height = 15.118110236220470000
        Top = 294.803340000000000000
        Width = 699.213050000000000000
        Condition = 'frxDsRomaneioGP."OriCnta"'
        object Memo12: TfrxMemoView
          Width = 90.708720000000000000
          Height = 15.118110240000000000
          DataSet = frxDsRomaneioGP
          DataSetName = 'frxDsRomaneioGP'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsRomaneioGP."NO_UniMed"] n'#186' ')
          ParentFont = False
          WordWrap = False
        end
        object Memo17: TfrxMemoView
          Left = 90.708720000000000000
          Width = 608.504330000000000000
          Height = 15.118110240000000000
          DataSet = frxDsRomaneioGP
          DataSetName = 'frxDsRomaneioGP'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[FormatFloat('#39'000000;-000000; '#39', <frxDsRomaneioGP."OriCnta">)]')
          ParentFont = False
          WordWrap = False
        end
      end
      object GroupFooter2: TfrxGroupFooter
        FillType = ftBrush
        Height = 11.338585120000000000
        Top = 374.173470000000000000
        Width = 699.213050000000000000
        object Memo19: TfrxMemoView
          Width = 699.213050000000000000
          Height = 3.779525120000000000
          DataSet = frxDsRomaneioGP
          DataSetName = 'frxDsRomaneioGP'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          ParentFont = False
          WordWrap = False
        end
      end
      object GroupHeader3: TfrxGroupHeader
        FillType = ftBrush
        Height = 52.913410240000000000
        Top = 597.165740000000000000
        Width = 699.213050000000000000
        Condition = 'frxDsLctosV."FatNum"'
        object Memo31: TfrxMemoView
          Width = 699.213050000000000000
          Height = 18.897650000000000000
          DataSet = frxDsRomaneioGP
          DataSetName = 'frxDsRomaneioGP'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Parcelamento:')
          ParentFont = False
          WordWrap = False
        end
        object Memo32: TfrxMemoView
          Top = 18.897649999999880000
          Width = 188.976500000000000000
          Height = 18.897650000000000000
          DataSet = frxDsRomaneioGP
          DataSetName = 'frxDsRomaneioGP'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Condi'#231#227'o de pagamento:')
          ParentFont = False
          WordWrap = False
        end
        object Memo33: TfrxMemoView
          Left = 188.976500000000000000
          Top = 18.897650000000000000
          Width = 510.236550000000000000
          Height = 18.897650000000000000
          DataSet = frxDsRomaneioGP
          DataSetName = 'frxDsRomaneioGP'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[VARF_NOMECONDICAOPG]')
          ParentFont = False
          WordWrap = False
        end
        object Memo36: TfrxMemoView
          Left = 94.488250000000000000
          Top = 37.795299999999880000
          Width = 94.488250000000000000
          Height = 15.118110236220470000
          DataSet = frxDsRomaneioGP
          DataSetName = 'frxDsRomaneioGP'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Valor parcela')
          ParentFont = False
          WordWrap = False
        end
        object Memo37: TfrxMemoView
          Top = 37.795299999999880000
          Width = 94.488250000000000000
          Height = 15.118110236220470000
          DataSet = frxDsRomaneioGP
          DataSetName = 'frxDsRomaneioGP'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.3n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Vencimento')
          ParentFont = False
          WordWrap = False
        end
      end
      object GroupFooter3: TfrxGroupFooter
        FillType = ftBrush
        Height = 3.779530000000000000
        Top = 710.551640000000000000
        Width = 699.213050000000000000
      end
      object MD_Desconto: TfrxMasterData
        FillType = ftBrush
        Height = 30.236240000000000000
        Top = 445.984540000000000000
        Width = 699.213050000000000000
        RowCount = 1
        object Memo22: TfrxMemoView
          Left = 604.724800000000000000
          Top = 7.559060000000000000
          Width = 94.488250000000000000
          Height = 18.897650000000000000
          DataSet = frxDsRomaneioGP
          DataSetName = 'frxDsRomaneioGP'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_DESCONTO_GERAL_VAL]')
          ParentFont = False
          WordWrap = False
        end
        object Memo24: TfrxMemoView
          Left = 457.323130000000000000
          Top = 7.559060000000000000
          Width = 75.590600000000000000
          Height = 18.897650000000000000
          DataSet = frxDsRomaneioGP
          DataSetName = 'frxDsRomaneioGP'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.4n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_DESCONTO_GERAL_PER]')
          ParentFont = False
          WordWrap = False
        end
        object Memo25: TfrxMemoView
          Left = 532.913730000000000000
          Top = 7.559060000000000000
          Width = 71.811070000000000000
          Height = 18.897650000000000000
          DataSet = frxDsRomaneioGP
          DataSetName = 'frxDsRomaneioGP'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '% de desconto: ')
          ParentFont = False
          WordWrap = False
        end
      end
      object MD_Comissao: TfrxMasterData
        FillType = ftBrush
        Height = 30.236240000000000000
        Top = 498.897960000000000000
        Width = 699.213050000000000000
        RowCount = 1
        object Memo11: TfrxMemoView
          Left = 604.724800000000000000
          Top = 3.779530000000000000
          Width = 94.488250000000000000
          Height = 18.897650000000000000
          DataSet = frxDsRomaneioGP
          DataSetName = 'frxDsRomaneioGP'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_COMISSAO_VAL]')
          ParentFont = False
          WordWrap = False
        end
        object Memo18: TfrxMemoView
          Left = 457.323130000000000000
          Top = 3.779530000000000000
          Width = 75.590600000000000000
          Height = 18.897650000000000000
          DataSet = frxDsRomaneioGP
          DataSetName = 'frxDsRomaneioGP'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.4n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_COMISSAO_PER]')
          ParentFont = False
          WordWrap = False
        end
        object Memo23: TfrxMemoView
          Left = 532.913730000000000000
          Top = 3.779530000000000000
          Width = 71.811070000000000000
          Height = 18.897650000000000000
          DataSet = frxDsRomaneioGP
          DataSetName = 'frxDsRomaneioGP'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '% de comiss'#227'o: ')
          ParentFont = False
          WordWrap = False
        end
      end
      object MD_ValLiq: TfrxMasterData
        FillType = ftBrush
        Height = 22.677180000000000000
        Top = 551.811380000000000000
        Width = 699.213050000000000000
        RowCount = 1
        object Memo26: TfrxMemoView
          Left = 623.622450000000000000
          Top = 3.779530000000000000
          Width = 75.590600000000000000
          Height = 15.118110240000000000
          DataSet = frxDsRomaneioGP
          DataSetName = 'frxDsRomaneioGP'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_VAL_LIQ]')
          ParentFont = False
          WordWrap = False
        end
        object Memo27: TfrxMemoView
          Top = 3.779530000000000000
          Width = 623.622450000000000000
          Height = 15.118110240000000000
          DataSet = frxDsRomaneioGP
          DataSetName = 'frxDsRomaneioGP'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Total l'#237'quido: ')
          ParentFont = False
          WordWrap = False
        end
      end
    end
  end
  object QrRomaneioGP: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrRomaneioGPCalcFields
    SQL.Strings = (
      'SELECT smva.OriCodi, 0.0+smva.OriCnta OriCnta, '
      'SUM(smva.Total)) Total,'
      'SUM(smva.Total) / SUM(smva.Qtde) PrecoCalc,'
      'SUM(smva.Qtde) Qtde, med.Nome NO_UniMed,'
      'gg1.CodUsu CU_NIVEL1, gg1.Nome NO_NIVEL1'
      'FROM stqmovvala smva'
      'LEFT JOIN gragrux ggx ON ggx.Controle=smva.GraGruX'
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN fatpedvol fpv ON fpv.Cnta=smva.OriCnta'
      'LEFT JOIN unidmed med ON med.Codigo=fpv.UnidMed'
      'WHERE smva.Tipo=:P0'
      'AND smva.Empresa=:P1'
      'AND smva.OriCodi=:P2'
      'GROUP BY '
      'smva.OriCnta, '
      'ggx.GraGru1'
      'ORDER BY '
      'smva.OriCnta,'
      'NO_NIVEL1')
    Left = 384
    Top = 128
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrRomaneioGPOriCodi: TIntegerField
      FieldName = 'OriCodi'
      Origin = 'stqmovvala.OriCodi'
      Required = True
    end
    object QrRomaneioGPTotal: TFloatField
      FieldName = 'Total'
      Origin = 'Total'
    end
    object QrRomaneioGPPrecoCalc: TFloatField
      FieldName = 'PrecoCalc'
    end
    object QrRomaneioGPQtde: TFloatField
      FieldName = 'Qtde'
      Origin = 'Qtde'
    end
    object QrRomaneioGPCU_NIVEL1: TIntegerField
      FieldName = 'CU_NIVEL1'
      Origin = 'gragru1.CodUsu'
    end
    object QrRomaneioGPNO_NIVEL1: TWideStringField
      FieldName = 'NO_NIVEL1'
      Origin = 'gragru1.Nome'
      Size = 30
    end
    object QrRomaneioGPNO_UniMed: TWideStringField
      FieldName = 'NO_UniMed'
      Origin = 'unidmed.Nome'
      Size = 30
    end
    object QrRomaneioGPOriCnta: TFloatField
      FieldName = 'OriCnta'
      Required = True
    end
    object QrRomaneioGPTOTAL_POSI: TFloatField
      FieldKind = fkCalculated
      FieldName = 'TOTAL_POSI'
      Calculated = True
    end
    object QrRomaneioGPQTDE_POSI: TFloatField
      FieldKind = fkCalculated
      FieldName = 'QTDE_POSI'
      Calculated = True
    end
  end
  object frxDsCli: TfrxDBDataset
    UserName = 'frxDsCli'
    CloseDataSource = False
    FieldAliases.Strings = (
      'E_ALL=E_ALL'
      'CNPJ_TXT=CNPJ_TXT'
      'NOME_TIPO_DOC=NOME_TIPO_DOC'
      'TE1_TXT=TE1_TXT'
      'FAX_TXT=FAX_TXT'
      'NUMERO_TXT=NUMERO_TXT'
      'CEP_TXT=CEP_TXT'
      'Codigo=Codigo'
      'Tipo=Tipo'
      'CodUsu=CodUsu'
      'NOME_ENT=NOME_ENT'
      'CNPJ_CPF=CNPJ_CPF'
      'IE_RG=IE_RG'
      'RUA=RUA'
      'COMPL=COMPL'
      'BAIRRO=BAIRRO'
      'CIDADE=CIDADE'
      'NOMELOGRAD=NOMELOGRAD'
      'NOMEUF=NOMEUF'
      'Pais=Pais'
      'ENDEREF=ENDEREF'
      'TE1=TE1'
      'FAX=FAX'
      'IE=IE'
      'CAD_FEDERAL=CAD_FEDERAL'
      'CAD_ESTADUAL=CAD_ESTADUAL'
      'IE_TXT=IE_TXT'
      'NUMERO=NUMERO'
      'Lograd=Lograd'
      'CEP=CEP'
      'UF=UF')
    DataSet = QrCli
    BCDToCurrency = False
    Left = 412
    Top = 184
  end
  object QrCli: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrCliCalcFields
    SQL.Strings = (
      'SELECT en.Codigo, Tipo, CodUsu, IE, '
      
        'CASE WHEN en.Tipo=0 THEN en.RazaoSocial ELSE en.Nome     END NOM' +
        'E_ENT, '
      
        'CASE WHEN en.Tipo=0 THEN en.CNPJ        ELSE en.CPF      END CNP' +
        'J_CPF, '
      
        'CASE WHEN en.Tipo=0 THEN en.IE          ELSE en.RG       END IE_' +
        'RG, '
      
        'CASE WHEN en.Tipo=0 THEN en.ERua        ELSE en.PRua     END RUA' +
        ', '
      
        'CASE WHEN en.Tipo=0 THEN en.ENumero + 0.000     ELSE en.PNumero ' +
        '+ 0.000 END NUMERO, '
      
        'CASE WHEN en.Tipo=0 THEN en.ECompl      ELSE en.PCompl   END COM' +
        'PL, '
      
        'CASE WHEN en.Tipo=0 THEN en.EBairro     ELSE en.PBairro  END BAI' +
        'RRO, '
      
        'CASE WHEN en.Tipo=0 THEN en.ECidade     ELSE en.PCidade  END CID' +
        'ADE, '
      
        'CASE WHEN en.Tipo=0 THEN en.EUF + 0.000     ELSE en.PUF + 0.000 ' +
        'END UF, '
      
        'CASE WHEN en.Tipo=0 THEN lle.Nome       ELSE llp.Nome    END NOM' +
        'ELOGRAD, '
      
        'CASE WHEN en.Tipo=0 THEN ufe.Nome       ELSE ufp.Nome    END NOM' +
        'EUF, '
      
        'CASE WHEN en.Tipo=0 THEN en.EPais       ELSE en.PPais    END Pai' +
        's, '
      
        'CASE WHEN en.Tipo=0 THEN en.ELograd + 0.000     ELSE en.PLograd ' +
        '+ 0.000 END Lograd, '
      
        'CASE WHEN en.Tipo=0 THEN en.ECEP + 0.000        ELSE en.PCEP  + ' +
        '0.000   END CEP, '
      
        'CASE WHEN en.Tipo=0 THEN en.EEndeRef    ELSE en.PEndeRef END END' +
        'EREF, '
      
        'CASE WHEN en.Tipo=0 THEN en.ETe1        ELSE en.PTe1     END TE1' +
        ', '
      
        'CASE WHEN en.Tipo=0 THEN en.EFax        ELSE en.PFax     END FAX' +
        ', '
      
        'IF(en.Tipo=0, "CNPJ", "CPF") CAD_FEDERAL, IF(en.Tipo=0, "I.E.", ' +
        '"RG") CAD_ESTADUAL'
      'FROM entidades en '
      'LEFT JOIN ufs ufe ON ufe.Codigo=en.EUF'
      'LEFT JOIN ufs ufp ON ufp.Codigo=en.PUF'
      'LEFT JOIN listalograd lle ON lle.Codigo=en.ELograd'
      'LEFT JOIN listalograd llp ON llp.Codigo=en.PLograd'
      'WHERE en.Codigo=:P0')
    Left = 384
    Top = 184
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrCliE_ALL: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'E_ALL'
      Size = 256
      Calculated = True
    end
    object QrCliCNPJ_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CNPJ_TXT'
      Size = 30
      Calculated = True
    end
    object QrCliNOME_TIPO_DOC: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOME_TIPO_DOC'
      Size = 10
      Calculated = True
    end
    object QrCliTE1_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TE1_TXT'
      Size = 40
      Calculated = True
    end
    object QrCliFAX_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'FAX_TXT'
      Size = 40
      Calculated = True
    end
    object QrCliNUMERO_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NUMERO_TXT'
      Size = 30
      Calculated = True
    end
    object QrCliCEP_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CEP_TXT'
      Calculated = True
    end
    object QrCliCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrCliTipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrCliCodUsu: TIntegerField
      FieldName = 'CodUsu'
      Required = True
    end
    object QrCliNOME_ENT: TWideStringField
      FieldName = 'NOME_ENT'
      Size = 100
    end
    object QrCliCNPJ_CPF: TWideStringField
      FieldName = 'CNPJ_CPF'
      Size = 18
    end
    object QrCliIE_RG: TWideStringField
      FieldName = 'IE_RG'
    end
    object QrCliRUA: TWideStringField
      FieldName = 'RUA'
      Size = 30
    end
    object QrCliCOMPL: TWideStringField
      FieldName = 'COMPL'
      Size = 30
    end
    object QrCliBAIRRO: TWideStringField
      FieldName = 'BAIRRO'
      Size = 30
    end
    object QrCliCIDADE: TWideStringField
      FieldName = 'CIDADE'
      Size = 25
    end
    object QrCliNOMELOGRAD: TWideStringField
      FieldName = 'NOMELOGRAD'
      Size = 10
    end
    object QrCliNOMEUF: TWideStringField
      FieldName = 'NOMEUF'
      Size = 2
    end
    object QrCliPais: TWideStringField
      FieldName = 'Pais'
    end
    object QrCliENDEREF: TWideStringField
      FieldName = 'ENDEREF'
      Size = 100
    end
    object QrCliTE1: TWideStringField
      FieldName = 'TE1'
    end
    object QrCliFAX: TWideStringField
      FieldName = 'FAX'
    end
    object QrCliIE: TWideStringField
      FieldName = 'IE'
    end
    object QrCliCAD_FEDERAL: TWideStringField
      FieldName = 'CAD_FEDERAL'
      Required = True
      Size = 4
    end
    object QrCliCAD_ESTADUAL: TWideStringField
      FieldName = 'CAD_ESTADUAL'
      Required = True
      Size = 4
    end
    object QrCliIE_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'IE_TXT'
      Size = 50
      Calculated = True
    end
    object QrCliNUMERO: TFloatField
      FieldName = 'NUMERO'
    end
    object QrCliLograd: TFloatField
      FieldName = 'Lograd'
    end
    object QrCliCEP: TFloatField
      FieldName = 'CEP'
    end
    object QrCliUF: TFloatField
      FieldName = 'UF'
    end
  end
end
