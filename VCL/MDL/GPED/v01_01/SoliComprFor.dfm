object FmSoliComprFor: TFmSoliComprFor
  Left = 450
  Top = 286
  Caption = 'SLC-COMPR-003 :: Fornecedores de Solicita'#231#227'o de Compra / Servi'#231'o'
  ClientHeight = 211
  ClientWidth = 727
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelDados: TPanel
    Left = 0
    Top = 48
    Width = 727
    Height = 49
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    ExplicitWidth = 624
    object LaPrompt: TLabel
      Left = 12
      Top = 4
      Width = 57
      Height = 13
      Caption = 'Fornecedor:'
    end
    object LaOrdem: TLabel
      Left = 644
      Top = 4
      Width = 57
      Height = 13
      Caption = 'Relev'#226'ncia:'
    end
    object CBEntidade: TdmkDBLookupComboBox
      Left = 80
      Top = 20
      Width = 557
      Height = 21
      KeyField = 'Codigo'
      ListField = 'NO_FORNECE'
      ListSource = DsEntidades
      TabOrder = 1
      dmkEditCB = EdEntidade
      UpdType = utYes
      LocF7SQLMasc = '$#'
      LocF7PreDefProc = f7pNone
    end
    object EdEntidade: TdmkEditCB
      Left = 12
      Top = 20
      Width = 65
      Height = 21
      Alignment = taRightJustify
      TabOrder = 0
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      DBLookupComboBox = CBEntidade
      IgnoraDBLookupComboBox = False
      AutoSetIfOnlyOneReg = setregOnlyManual
    end
    object EdRelevancia: TdmkEdit
      Left = 644
      Top = 20
      Width = 69
      Height = 21
      Alignment = taRightJustify
      TabOrder = 2
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '0'
      ValMax = '99'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '50'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 50
      ValWarn = False
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 727
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    ExplicitWidth = 624
    object GB_R: TGroupBox
      Left = 679
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      ExplicitLeft = 576
      object ImgTipo: TdmkImage
        Left = 8
        Top = 7
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 631
      Height = 48
      Align = alClient
      TabOrder = 2
      ExplicitWidth = 528
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 602
        Height = 32
        Caption = 'Fornecedores de Solicita'#231#227'o de Compra / Servi'#231'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 602
        Height = 32
        Caption = 'Fornecedores de Solicita'#231#227'o de Compra / Servi'#231'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 602
        Height = 32
        Caption = 'Fornecedores de Solicita'#231#227'o de Compra / Servi'#231'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 97
    Width = 727
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 3
    ExplicitWidth = 624
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 723
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 620
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 141
    Width = 727
    Height = 70
    Align = alBottom
    TabOrder = 1
    ExplicitWidth = 624
    object PnSaiDesis: TPanel
      Left = 581
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      ExplicitLeft = 478
      object BtSaida: TBitBtn
        Tag = 15
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 579
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 476
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
      object CkContinua: TCheckBox
        Left = 140
        Top = 16
        Width = 117
        Height = 17
        Caption = 'Continua inserindo.'
        TabOrder = 1
        Visible = False
      end
    end
  end
  object DsEntidades: TDataSource
    DataSet = QrEntidades
    Left = 208
    Top = 118
  end
  object QrEntidades: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT '
      'CASE WHEN Tipo=0 THEN RazaoSocial'
      'ELSE Nome END Descricao, Codigo'
      'FROM entidades '
      'ORDER BY Descricao')
    Left = 208
    Top = 66
    object QrEntidadesCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEntidadesNO_FORNECE: TWideStringField
      FieldName = 'NO_FORNECE'
      Size = 120
    end
  end
end
