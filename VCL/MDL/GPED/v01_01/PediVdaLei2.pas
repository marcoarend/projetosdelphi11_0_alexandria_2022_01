unit PediVdaLei2;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DBCtrls, dmkDBLookupComboBox, UnDmkProcFunc,
  dmkEdit, dmkEditCB, DB, mySQLDbTables, Mask, Variants, Grids, DBGrids,
  ComCtrls, dmkLabel, dmkDBEdit, dmkDBGrid, dmkImage, UnDmkEnums, DmkDAC_PF;

type
  TFmPediVdaLei2 = class(TForm)
    Panel1: TPanel;
    QrItem: TmySQLQuery;
    DsItem: TDataSource;
    QrItemNOMENIVEL1: TWideStringField;
    QrItemGraCorCad: TIntegerField;
    QrItemNOMECOR: TWideStringField;
    QrItemNOMETAM: TWideStringField;
    QrItemGraGruX: TIntegerField;
    QrItemGraGru1: TIntegerField;
    QrPreco: TmySQLQuery;
    DsPreco: TDataSource;
    QrFator: TmySQLQuery;
    QrItemCU_Nivel1: TIntegerField;
    QrItemIPI_Alq: TFloatField;
    QrPediVdaIts: TmySQLQuery;
    QrPrecoPrecoF: TFloatField;
    QrPrecoQuantF: TFloatField;
    QrItemMadeBy: TSmallintField;
    DsPediVdaIts: TDataSource;
    PnLeitura: TPanel;
    Panel4: TPanel;
    Label3: TLabel;
    LaQtdeLei: TLabel;
    EdQtdLei: TdmkEdit;
    BtOK: TBitBtn;
    Panel9: TPanel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    PnSimu: TPanel;
    Label9: TLabel;
    Label8: TLabel;
    Label7: TLabel;
    dmkEdit2: TdmkEdit;
    dmkEdit3: TdmkEdit;
    dmkEdit1: TdmkEdit;
    Button1: TButton;
    DBGPediVdaIts: TDBGrid;
    EdPrecoO: TdmkEdit;
    Label1: TLabel;
    Panel3: TPanel;
    ST1: TStaticText;
    ST2: TStaticText;
    ST3: TStaticText;
    Label2: TLabel;
    EdPrecoR: TdmkEdit;
    EdDescoP: TdmkEdit;
    Label10: TLabel;
    QrPediVdaItsCU_Nivel1: TIntegerField;
    QrPediVdaItsNO_Nivel1: TWideStringField;
    QrPediVdaItsCU_Cor: TIntegerField;
    QrPediVdaItsNO_Cor: TWideStringField;
    QrPediVdaItsNO_Tam: TWideStringField;
    QrPediVdaItsGraGruX: TIntegerField;
    QrPediVdaItsPrecoF: TFloatField;
    QrPediVdaItsQuantP: TFloatField;
    QrLista: TmySQLQuery;
    QrListaGraCusPrc: TIntegerField;
    PnJuros: TPanel;
    Label11: TLabel;
    Label12: TLabel;
    DBEdit4: TDBEdit;
    EdCustoFin: TdmkEdit;
    QrPediVdaItsControle: TIntegerField;
    QrExiste: TmySQLQuery;
    QrExisteControle: TIntegerField;
    QrExisteQuantP: TFloatField;
    QrPediVdaLei: TmySQLQuery;
    DsPediVdaLei: TDataSource;
    QrPediVdaLeiConta: TIntegerField;
    QrPediVdaLeiSequencia: TIntegerField;
    QrPediVdaItsQuantC: TFloatField;
    QrPediVdaItsQuantV: TFloatField;
    QrExistePrecoR: TFloatField;
    QrExisteDescoP: TFloatField;
    QrPediVdaItsValLiq: TFloatField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel5: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel6: TPanel;
    PnSaiDesis: TPanel;
    CkFixo: TCheckBox;
    BtExclui: TBitBtn;
    BtSaida: TBitBtn;
    QrItemPrdGrupTip: TIntegerField;
    QrItemNCM: TWideStringField;
    BtGraGruN: TBitBtn;
    QrPediVdaItsNivel1: TIntegerField;
    BtTabePrcCab: TBitBtn;
    EdLeitura: TEdit;
    QrItemUnidMed: TIntegerField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure EdLeituraChange(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure EdQtdLeiKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure BtExcluiClick(Sender: TObject);
    procedure QrItemBeforeClose(DataSet: TDataSet);
    procedure EdQtdLeiEnter(Sender: TObject);
    procedure QrPediVdaItsAfterScroll(DataSet: TDataSet);
    procedure CkFixoClick(Sender: TObject);
    procedure BtGraGruNClick(Sender: TObject);
    procedure BtTabePrcCabClick(Sender: TObject);
    procedure EdLeituraKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdLeituraExit(Sender: TObject);
  private
    { Private declarations }
    FTam20: Boolean;
    FGraGruX, FSequencia: Integer;
    function ReopenPreco(): Boolean;
    function ReopenItem(): Boolean;
    procedure InsereItem();
  public
    { Public declarations }
    procedure ReopenPediVdaIts(GraGruX: Integer);
    procedure ReopenPediVdaLei(Conta: Integer);

  end;

  var
  FmPediVdaLei2: TFmPediVdaLei2;

implementation

uses
  UnMyObjects, Module, UMySQLModule, dmkGeral, ModuleGeral, UnInternalConsts,
  MyDBCheck, GetValor, Principal, ModPediVda, UnGrade_PF, (*,ModFatuVda*)
  ModProd, PediVda2, FatPedCab2, QuaisItens, GraGruN, TabePrcCab;

{$R *.DFM}

procedure TFmPediVdaLei2.BtExcluiClick(Sender: TObject);
{
var
  Casas, AExcluir: Integer;
  QuantP, PrecoR, DescoP, DescoI, DescoV, ValBru, ValLiq, Indevidos,
  Excluidos: Double;
}
begin
  {
  Excluidos := 0;
  Indevidos := QrPediVdaItsQuantC.Value + QrPediVdaItsQuantV.Value;
  // N�o pode por que n�o se saberia quanto se deve diminuir do pedivdaits.QuantP!
  (*
  if Indevidos < 0.001 then
  begin
    DBCheck.QuaisItens_Exclui(Dmod.QrUpd, QrPediVdaLei, DBGPediVdaLei,
    'PediVdaLei', ['Conta'], ['Conta'], istPergunta, '');
  end else begin
  *)
    AExcluir := DBGPediVdaLei.SelectedRows.Count;
    if AExcluir = 0 then AExcluir := 1;
    if AExcluir > (QrPediVdaItsQuantP.Value - Indevidos) then
    begin
      Geral.MB_Aviso('Exclus�o negada! J� existe faturamento com ' +
        'quantidade superior ao saldo posterior � exclus�o!');
      Exit;
    end;
    if AExcluir > 1 then
      Excluidos := DBCheck.QuaisItens_Exclui(Dmod.QrUpd, QrPediVdaLei, DBGPediVdaLei,
      'PediVdaLei', ['Conta'], ['Conta'], istSelecionados, '')
    else
      Excluidos := DBCheck.QuaisItens_Exclui(Dmod.QrUpd, QrPediVdaLei, DBGPediVdaLei,
      'PediVdaLei', ['Conta'], ['Conta'], istAtual, '');
    //
    if Excluidos > 0 then
    begin
      QrExiste.Close;
      QrExiste.Params[00].AsInteger := FmPediVda2.QrPediVdaCodigo.Value;
      QrExiste.Params[01].AsInteger := QrPediVdaItsGraGruX.Value;
      UnDmkDAC_PF.AbreQuery(QrExiste, Dmod.MyDB);
      //
      Casas      := Dmod.QrControleCasasProd.Value;
      QuantP     := QrExisteQuantP.Value - Excluidos;
      if QuantP = 0 then
      begin
        Dmod.QrUpd.SQL.Clear;
        Dmod.QrUpd.SQL.Add('DELETE FROM pedivdaits WHERE Controle=:Pa');
        Dmod.QrUpd.Params[00].AsFloat := QrExisteControle.Value;
        Dmod.QrUpd.ExecSQL;
      end else begin
        PrecoR     := QrExistePrecoR.Value;
        DescoP     := QrExisteDescoP.Value;
        DescoI     := dmkPF.FFF(PrecoR * DescoP / 100, Casas, siPositivo);
        DescoV     := DescoI * QuantP;
        ValBru     := PrecoR * QuantP;
        ValLiq     := ValBru - DescoV;
        //
        Dmod.QrUpd.SQL.Clear;
        Dmod.QrUpd.SQL.Add('UPDATE pedivdaits SET QuantP=:P0, ValBru=:P1, ');
        Dmod.QrUpd.SQL.Add('DescoV=:P2, ValLiq=:P3 WHERE Controle=:Pa');
        Dmod.QrUpd.Params[00].AsFloat := QuantP;
        Dmod.QrUpd.Params[01].AsFloat := ValBru;
        Dmod.QrUpd.Params[02].AsFloat := DescoV;
        Dmod.QrUpd.Params[03].AsFloat := ValLiq;
        //
        Dmod.QrUpd.Params[04].AsFloat := QrExisteControle.Value;
        Dmod.QrUpd.ExecSQL;
      end;
    end;
  //end;
  ReopenPediVdaIts(QrPediVdaItsControle.Value);
  }
end;

procedure TFmPediVdaLei2.BtGraGruNClick(Sender: TObject);
var
  Nivel1: Integer;
begin
  Nivel1 := QrPediVdaItsNivel1.Value;
  //
  if DBCheck.CriaFm(TFmGraGruN, FmGraGruN, afmoNegarComAviso) then
  begin
    if (QrPediVdaIts.State <> dsInactive) and (QrPediVdaIts.RecordCount > 0) then
      FmGraGruN.LocalizaIDNivel1(Nivel1);
    FmGraGruN.ShowModal;
    FmGraGruN.Destroy;
    //
    try
      Screen.Cursor := crHourGlass;
      //
      QrPediVdaIts.Close;
      UnDmkDAC_PF.AbreQuery(QrPediVdaIts, Dmod.MyDB);
      QrPediVdaIts.Locate('Nivel1', Nivel1, []);
    finally
      Screen.Cursor := crDefault;
    end;
  end;
end;

procedure TFmPediVdaLei2.BtOKClick(Sender: TObject);
begin
  InsereItem();
end;

procedure TFmPediVdaLei2.InsereItem();
var
  GraGruX, Codigo, Controle: Integer;
  Qtd, PrecoO, PrecoR, PrecoF, QuantP, ValBru, ValLiq,
  DescoP, DescoV, DescoI, vProd, vBC: Double;
  Casas: Integer;
  Mensagem, Msg: String;
begin
  Msg := '';
  //
  if (FGragruX = 0) then
  begin
    Msg := 'Defina o produto!';
    Exit;
  end;
  //
  //Nivel1 := QrItemGraGru1.Value;
  Screen.Cursor := crHourGlass;
  try
    Codigo := FmPediVda2.QrPediVdaCodigo.Value;
    //ErrPreco  := 0;
    //AvisoPrc  := 0;
    //QtdRed    := 0;
    Qtd := Geral.IMV(EdQtdLei.Text);
    if Qtd = 0 then
    begin
      Msg := 'Definina a quantidade!';
      Exit;
    end;
    //
    (*
    if not Grade_PF.ValidaRegraFiscalCFOPProdutos(QrItemPrdGrupTip.Value,
      FmPediVda2.QrPediVdaRegrFiscal.Value, FmPediVda2.QrPediVdaNOMEFISREGCAD.Value,
      Mensagem) then
    begin
      Msg := Mensagem;
      Exit;
    end;
    *)
    if (QrItemNCM.Value = '') and (DmPediVda.QrParamsEmpPediVdaNElertas.Value = 0) then
    begin
      Msg := 'O produto selecionado n�o possui NCM cadastrado!';
      Exit;
    end;
    //
    if (QrItemUnidMed.Value = 0) and (DmPediVda.QrParamsEmpPediVdaNElertas.Value = 0) then
    begin
      Msg := 'O produto selecionado n�o possui Unidade de Medida cadastrada!';
      Exit;
    end;
    //
    PrecoR := Geral.DMV(EdPrecoR.Text);
    if PrecoR = 0 then
    begin
      Msg := 'Defina o pre�o de faturamento!';
      Exit;
    end;
    //
    if (FGraGruX > 0) and (Msg = '') then
    begin
      GraGruX    := FGraGruX;
      FGraGruX   := 0;
      //Sequencia  := FSequencia;
      FSequencia := 0;
      QuantP     := Geral.DMV(EdQtdLei.Text);
      //Conta := UMyMod.BuscaEmLivreY_Def('pedivdalei', 'Conta', ImgTipo.SQLType, 0);
      QrExiste.Close;
      QrExiste.Params[00].AsInteger := Codigo;
      QrExiste.Params[01].AsInteger := GraGruX;
      UnDmkDAC_PF.AbreQuery(QrExiste, Dmod.MyDB);
      if QrExisteControle.Value > 0 then
      begin
        Controle := QrExisteControle.Value;
        QuantP   := QuantP + QrExisteQuantP.Value;
        ImgTipo.SQLType := stUpd;
      end else begin
        ImgTipo.SQLType := stIns;
        Controle := UMyMod.BuscaEmLivreY_Def('pedivdaits', 'Controle',
          ImgTipo.SQLType, 0);
      end;
      Casas      := Dmod.QrControleCasasProd.Value;
      PrecoO     := Geral.DMV(EdPrecoO.Text);
      //ValCal     := PrecoR;// * QuantP;
      DescoP     := Geral.DMV(EdDescoP.Text);
      DescoI     := dmkPF.FFF(PrecoR * DescoP / 100, Casas, siPositivo);
      PrecoF     := PrecoR - DescoI;
      DescoV     := DescoI * QuantP;
      ValBru     := PrecoR * QuantP;
      ValLiq     := ValBru - DescoV;
      vProd  := ValLiq;
      vBC    := ValLiq;
      //
      {
      if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'pedivdalei', False, [
      'Controle', 'GraGruX', 'Sequencia'], ['Conta'], [
      Controle, GraGruX, Sequencia], [Conta], True) then
      begin
      }
        if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'pedivdaits', False,[
        'Codigo', 'GraGruX', 'PrecoO', 'PrecoR', 'QuantP', 'ValBru',
        'DescoP', 'DescoV', 'ValLiq', 'PrecoF', 'vProd', 'vBC'], ['Controle'],
        [Codigo, GraGruX, PrecoO, PrecoR, QuantP, ValBru,
        DescoP, DescoV, ValLiq, PrecoF, vProd, vBC], [Controle],
        True) then
        DmPediVda.AtzSdosPedido(Codigo);
      {
      end else begin
        if ImgTipo.SQLType = stIns then
          UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'pedivdaits', Controle);
      end;
      }
    end;
    ImgTipo.SQLType := stIns;
    EdLeitura.Text  := '';
    //EdLeitura.SetFocus;
    EdQtdLei.SetFocus;
    ReopenPediVdaIts(FGraGruX);
  finally
    Screen.Cursor := crDefault;
    //
    if Msg <> '' then
      Geral.MB_Aviso(Msg);
  end;
end;

procedure TFmPediVdaLei2.QrItemBeforeClose(DataSet: TDataSet);
begin
 QrPreco.Close;
end;

procedure TFmPediVdaLei2.QrPediVdaItsAfterScroll(DataSet: TDataSet);
begin
  ReopenPediVdaLei(0);
end;

procedure TFmPediVdaLei2.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmPediVdaLei2.BtTabePrcCabClick(Sender: TObject);
var
  TabPrc: Integer;
begin
  TabPrc := FmPediVda2.QrPediVdaTabelaPrc.Value;
  //
  if DBCheck.CriaFm(TFmTabePrcCab, FmTabePrcCab, afmoNegarComAviso) then
  begin
    FmTabePrcCab.LocCod(TabPrc, TabPrc);
    FmTabePrcCab.ShowModal;
    FmTabePrcCab.Destroy;
  end;
end;

procedure TFmPediVdaLei2.Button1Click(Sender: TObject);
const
  Tick = 25;
var
  i: Integer;
begin
  EdLeitura.Text := '';
  //
  for i := 1 to Length(dmkEdit1.Text) do
  begin
    EdLeitura.Text := EdLeitura.Text + dmkEdit1.Text[i];
    sleep(Tick);
  end;
  //
  for i := 1 to Length(dmkEdit2.Text) do
  begin
    EdLeitura.Text := EdLeitura.Text + dmkEdit2.Text[i];
    sleep(Tick);
  end;
  //
  for i := 1 to Length(dmkEdit3.Text) do
  begin
    EdLeitura.Text := EdLeitura.Text + dmkEdit3.Text[i];
    sleep(Tick);
  end;
end;

procedure TFmPediVdaLei2.CkFixoClick(Sender: TObject);
begin
  EdQtdLei.Enabled := not CkFixo.Checked;
end;

procedure TFmPediVdaLei2.EdLeituraChange(Sender: TObject);
(*
var
  Tam: Integer;
*)
begin
  (*
  A leitora n�o mostra as mensagens no onChange mudado para o onExit

  Tam := Length(Trim(EdLeitura.Text));
  //
  if (Tam = 20) or
    ((DmProd.QrOpcoesGrad.FieldByName('UsaCodFornece').AsInteger = 1) and (Tam = 13)) then
  begin
    DmProd.ObtemGraGruXDeCodigoDeBarraProduto(EdLeitura.Text, FGraGruX, FSequencia);
    //
    if ReopenItem() then
    begin
      if CkFixo.Checked then
      begin
        FTam20 := True;
        //
        InsereItem();
        //
        EdLeitura.Text := '';
        EdLeitura.SetFocus;
      end else
        EdQtdLei.SetFocus;
    end;
  end else if Tam = 0 then
    QrItem.Close;
  *)
end;

procedure TFmPediVdaLei2.EdLeituraExit(Sender: TObject);
var
  Tam: Integer;
begin
  Tam := Length(Trim(EdLeitura.Text));
  //
  if (Tam = 20) or
    ((DmProd.QrOpcoesGrad.FieldByName('UsaCodFornece').AsInteger = 1) and (Tam = 13)) then
  begin
    DmProd.ObtemGraGruXDeCodigoDeBarraProduto(EdLeitura.Text, FGraGruX, FSequencia);
    //
    if ReopenItem() then
    begin
      if CkFixo.Checked then
      begin
        FTam20 := True;
        //
        InsereItem();
        //
        EdLeitura.Text := '';
        //EdLeitura.SetFocus;
        EdQtdLei.SetFocus;
      end else
        EdQtdLei.SetFocus;
    end;
  end else if Tam = 0 then
    QrItem.Close;
end;

procedure TFmPediVdaLei2.EdLeituraKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  (*
  N�o � necess�rio

  if Key = VK_RETURN then
  begin
    if ((Length(EdLeitura.Text) <= 6) or
      ((DmProd.QrOpcoesGrad.FieldByName('UsaCodFornece').AsInteger = 1) and (Length(EdLeitura.Text) = 13))) then
    begin
      ReopenItem();
    end else
      Geral.MB_Aviso('Quantidade de caracteres inv�lida para ' +
        'localiza��o pelo reduzido!');
  end;
  *)
end;

procedure TFmPediVdaLei2.EdQtdLeiEnter(Sender: TObject);
begin
  if CkFixo.Checked and (EdLeitura.Text = '') and FTam20 then
  begin
    FTam20 := False;
    // est� desabilitado
    //EdPrecoR.SetFocus;
  end;
end;

procedure TFmPediVdaLei2.EdQtdLeiKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_RETURN then
  begin
    InsereItem();
  end;
end;

procedure TFmPediVdaLei2.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //
  QrPediVdaItsPrecoF.DisplayFormat := Dmod.FStrFmtPrc;
  QrPediVdaItsValLiq.DisplayFormat := Dmod.FStrFmtPrc;
end;

procedure TFmPediVdaLei2.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  if VAR_USUARIO = -1 then
    PnSimu.Visible := True;
  ST1.Caption := '';
  ST2.Caption := '';
  ST3.Caption := '';
  ST1.Font.Color := clRed;
  ST2.Font.Color := clRed;
  ST3.Font.Color := clRed;
  ST1.Color := PnLeitura.Color;
  ST2.Color := PnLeitura.Color;
  ST3.Color := PnLeitura.Color;
  //
  DmProd.ReopenOpcoesGrad;
  ReopenPediVdaIts(0);
  EdQtdLei.ValueVariant := 1;
end;

procedure TFmPediVdaLei2.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

function TFmPediVdaLei2.ReopenItem(): Boolean;
begin
  Result := False;
  QrItem.Close;
  if FGraGruX > 0 then
  begin
    QrItem.Params[0].AsInteger := FGraGrux;
    UnDmkDAC_PF.AbreQuery(QrItem, Dmod.MyDB);
    //
    if QrItem.RecordCount > 0 then
    begin
      Result := True;
      // est� desabilitado
      //EdPrecoR.SetFocus;
      //# PnLido.Visible := True;
      //#
      PnSimu.Visible := False;
      ReopenPreco();
    end else
      Geral.MB_Aviso('Reduzido n�o localizado!');
  end;
end;

procedure TFmPediVdaLei2.ReopenPediVdaIts(GraGruX: Integer);
begin
  QrPediVdaIts.Close;
  QrPediVdaIts.Params[0].AsInteger := FmPediVda2.QrPediVdaCodigo.Value;
  UnDmkDAC_PF.AbreQuery(QrPediVdaIts, Dmod.MyDB);
  //
  if GraGruX <> 0 then
    QrPediVdaIts.Locate('GraGruX', GraGruX, []);
end;

procedure TFmPediVdaLei2.ReopenPediVdaLei(Conta: Integer);
begin
  EXIT;
  //ERRADO
  QrPediVdaLei.Close;
  QrPediVdaLei.Params[0].AsInteger := QrPediVdaItsControle.Value;
  UnDmkDAC_PF.AbreQuery(QrPediVdaLei, Dmod.MyDB);
  //
  if Conta <> 0 then
    QrPediVdaLei.Locate('Conta', Conta, []);
end;

function TFmPediVdaLei2.ReopenPreco(): Boolean;
var
  MedDDSimpl, MedDDReal, MediaSel, TaxaM, Juros: Double;
  Tabela, CondicaoPG, Lista, TipMediaDD, FatSemPrcL: Integer;
begin
  PnJuros.Visible := False;
  Tabela     := FmPediVda2.QrPediVdaTabelaPrc.Value;
  MedDDSimpl := FmPediVda2.QrPediVdaMedDDSimpl.Value;
  MedDDReal  := FmPediVda2.QrPediVdaMedDDReal.Value;
  CondicaoPG := FmPediVda2.QrPediVdaCondicaoPG.Value;
  TipMediaDD := DmPediVda.QrParamsEmpTipMediaDD.Value;
  FatSemPrcL := DmPediVda.QrParamsEmpFatSemPrcL.Value;
  //
  if Tabela > 0 then
  begin
    DmProd.Atualiza_dmkEditsPrecos3(FGraGruX, Tabela, CondicaoPG,
      EdPrecoO, EdPrecoR, MedDDSimpl, MedDDReal, ST1, ST2, ST3, ImgTipo, False,
      TipMediaDD, FatSemPrcL);
      //DmPediVda.QrParamsEmpTipMediaDD.Value, DmPediVda.QrParamsEmpFatSemPrcL.Value);
    //
  end else
  begin
    Geral.MB_Aviso('Tabela de pre�o n�o definida!');
    (*
    2017-08-23 => N�o existe mais a op��o de selecionar a tabela na movimenta��o da Regra Fiscal

    QrLista.Close;
    QrLista.Params[00].AsInteger := FmPediVda2.QrPediVdaEmpresa.Value;
    QrLista.Params[01].AsInteger := FmPediVda2.QrPediVdaRegrFiscal.Value;
    UnDmkDAC_PF.AbreQuery(QrLista, Dmod.MyDB);
    if QrLista.RecordCount = 1 then
    begin
      Lista := QrListaGraCusPrc.Value;
      case DmPediVda.QrParamsEmpTipMediaDD.Value of
        1: MediaSel := MedDDSimpl;
        2: MediaSel := MedDDReal;
        else MediaSel := 0;
      end;
      TaxaM := FmPediVda2.QrPediVdaJurosMes.Value;
      case DmPediVda.QrParamsEmpTipCalcJuro.Value of
        1: Juros := dmkPF.CalculaJuroSimples(TaxaM, MediaSel);
        2: Juros := dmkPF.CalculaJuroComposto(TaxaM, MediaSel);
        else Juros := 0;
      end;
      EdCustoFin.ValueVariant := Juros;
      PnJuros.Visible := True;
      DmProd.Atualiza_dmkEditsPrecos4(FGraGruX, Lista, EdPrecoO, EdPrecoR, Juros);
    end else
    begin
      if QrLista.RecordCount = 0 then
        Geral.MB_Aviso('Nenhuma lista est� definida com o tipo ' +
          'de movimento "Sa�da" na regra fiscal "' + FmPediVda2.QrPediVdaNOMEFISREGCAD.Value
          + '" para a empresa ' + FormatFloat('000', FmPediVda2.QrPediVdaFilial.Value) + '!')
      else
        Geral.MB_Aviso('H� ' + Geral.FF0(QrLista.RecordCount) +
          'listas habilitadas para o movimento "Sa�da" na regra fiscal "' +
          FmPediVda2.QrPediVdaNOMEFISREGCAD.Value + '" para a empresa ' +
          FormatFloat('000', FmPediVda2.QrPediVdaFilial.Value) + '. Nenhuma ser�' +
          'considerada!');
    end;
    *)
  end;
  Result := True;
end;

end.

