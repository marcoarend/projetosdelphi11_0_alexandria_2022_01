object FmFatPedIts2: TFmFatPedIts2
  Left = 339
  Top = 185
  Caption = 'FAT-PEDID-003 :: Itens de Faturamento de Pedido'
  ClientHeight = 702
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 1008
    Height = 512
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 1008
      Height = 49
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object Panel7: TPanel
        Left = 257
        Top = 0
        Width = 751
        Height = 49
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object Label2: TLabel
          Left = 12
          Top = 4
          Width = 469
          Height = 13
          Caption = 
            'Centro de estoque (Cadastrados nos itens de movimenta'#231#227'o da regr' +
            'a fiscal selecionada no pedido):'
        end
        object Label15: TLabel
          Left = 498
          Top = 4
          Width = 62
          Height = 13
          Caption = 'Qtd '#224' faturar:'
        end
        object Label16: TLabel
          Left = 588
          Top = 4
          Width = 62
          Height = 13
          Caption = 'Qtd faturada:'
        end
        object EdStqCenCad: TdmkEditCB
          Left = 12
          Top = 20
          Width = 56
          Height = 21
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          OnChange = EdStqCenCadChange
          OnEnter = EdStqCenCadEnter
          OnExit = EdStqCenCadExit
          DBLookupComboBox = CBStqCenCad
          IgnoraDBLookupComboBox = False
          AutoSetIfOnlyOneReg = setregOnlyManual
        end
        object CBStqCenCad: TdmkDBLookupComboBox
          Left = 68
          Top = 20
          Width = 413
          Height = 21
          KeyField = 'CodUsu'
          ListField = 'Nome'
          ListSource = DsStqCenCad
          TabOrder = 1
          dmkEditCB = EdStqCenCad
          UpdType = utYes
          LocF7SQLMasc = '$#'
        end
        object EdQtdAFat: TdmkEdit
          Left = 498
          Top = 20
          Width = 85
          Height = 21
          Alignment = taRightJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          ReadOnly = True
          TabOrder = 2
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 3
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,000'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
          OnChange = EdQtdLeiChange
          OnEnter = EdQtdLeiEnter
          OnExit = EdQtdLeiExit
          OnKeyDown = EdQtdLeiKeyDown
        end
        object EdQtdFat: TdmkEdit
          Left = 588
          Top = 20
          Width = 85
          Height = 21
          Alignment = taRightJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          ReadOnly = True
          TabOrder = 3
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 3
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,000'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
          OnChange = EdQtdLeiChange
          OnEnter = EdQtdLeiEnter
          OnExit = EdQtdLeiExit
          OnKeyDown = EdQtdLeiKeyDown
        end
      end
      object Panel8: TPanel
        Left = 0
        Top = 0
        Width = 257
        Height = 49
        Align = alLeft
        BevelOuter = bvNone
        Enabled = False
        TabOrder = 1
        object Label1: TLabel
          Left = 100
          Top = 4
          Width = 38
          Height = 13
          Caption = 'Volume:'
          FocusControl = DBEdit5
        end
        object Label10: TLabel
          Left = 8
          Top = 4
          Width = 44
          Height = 13
          Caption = 'ID fatura:'
          FocusControl = DBEdit6
        end
        object DBEdit5: TdmkDBEdit
          Left = 100
          Top = 20
          Width = 56
          Height = 21
          DataField = 'Cnta'
          DataSource = DsFatPedVol
          TabOrder = 0
          UpdCampo = 'Cnta'
          UpdType = utYes
          Alignment = taLeftJustify
        end
        object DBEdit6: TdmkDBEdit
          Left = 8
          Top = 20
          Width = 89
          Height = 21
          DataField = 'Codigo'
          DataSource = DsFatPedCab
          TabOrder = 1
          UpdCampo = 'Codigo'
          UpdType = utYes
          Alignment = taLeftJustify
        end
        object dmkDBEdit1: TdmkDBEdit
          Left = 160
          Top = 20
          Width = 89
          Height = 21
          DataField = 'NO_UnidMed'
          DataSource = DsFatPedVol
          TabOrder = 2
          UpdCampo = 'Cnta'
          UpdType = utYes
          Alignment = taLeftJustify
        end
      end
    end
    object PageControl1: TPageControl
      Left = 0
      Top = 49
      Width = 1008
      Height = 104
      ActivePage = TabSheet1
      Align = alTop
      TabOrder = 1
      object TabSheet1: TTabSheet
        Caption = ' por &Leitura '
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object PnLeitura: TPanel
          Left = 0
          Top = 0
          Width = 1000
          Height = 48
          Align = alTop
          BevelOuter = bvNone
          Enabled = False
          ParentBackground = False
          TabOrder = 0
          object Panel4: TPanel
            Left = 0
            Top = 0
            Width = 285
            Height = 48
            Align = alLeft
            BevelOuter = bvNone
            TabOrder = 0
            object Label3: TLabel
              Left = 12
              Top = 4
              Width = 89
              Height = 13
              Caption = 'Leitura / digita'#231#227'o:'
            end
            object LaQtdeLei: TLabel
              Left = 196
              Top = 4
              Width = 58
              Height = 13
              Caption = 'Quantidade:'
            end
            object EdLeitura: TEdit
              Left = 12
              Top = 20
              Width = 180
              Height = 21
              MaxLength = 20
              TabOrder = 0
              OnChange = EdLeituraChange
              OnExit = EdLeituraExit
              OnKeyDown = EdLeituraKeyDown
            end
            object EdQtdLei: TdmkEdit
              Left = 196
              Top = 20
              Width = 85
              Height = 21
              Alignment = taRightJustify
              TabOrder = 1
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 3
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,000'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
              OnChange = EdQtdLeiChange
              OnEnter = EdQtdLeiEnter
              OnExit = EdQtdLeiExit
              OnKeyDown = EdQtdLeiKeyDown
            end
          end
          object PnMultiGrandeza: TPanel
            Left = 285
            Top = 0
            Width = 352
            Height = 48
            Align = alLeft
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 1
            Visible = False
            object LaPecas: TLabel
              Left = 4
              Top = 4
              Width = 33
              Height = 13
              Caption = 'Pe'#231'as:'
            end
            object LaAreaM2: TLabel
              Left = 92
              Top = 4
              Width = 39
              Height = 13
              Caption = #193'rea m'#178':'
            end
            object LaAreaP2: TLabel
              Left = 180
              Top = 4
              Width = 37
              Height = 13
              Caption = #193'rea ft'#178':'
            end
            object LaPeso: TLabel
              Left = 260
              Top = 4
              Width = 48
              Height = 13
              Caption = 'Peso (kg):'
            end
            object EdPecas: TdmkEdit
              Left = 4
              Top = 20
              Width = 85
              Height = 21
              Alignment = taRightJustify
              TabOrder = 0
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 3
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,000'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
            end
            object EdPeso: TdmkEdit
              Left = 260
              Top = 20
              Width = 85
              Height = 21
              Alignment = taRightJustify
              TabOrder = 3
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 3
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,000'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
            end
            object EdAreaM2: TdmkEditCalc
              Left = 92
              Top = 20
              Width = 80
              Height = 21
              Alignment = taRightJustify
              TabOrder = 1
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '0'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
              dmkEditCalcA = EdAreaP2
              CalcType = ctM2toP2
              CalcFrac = cfQuarto
            end
            object EdAreaP2: TdmkEditCalc
              Left = 176
              Top = 20
              Width = 80
              Height = 21
              Alignment = taRightJustify
              TabOrder = 2
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '0'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
              dmkEditCalcA = EdAreaM2
              CalcType = ctP2toM2
              CalcFrac = cfCento
            end
          end
          object Panel6: TPanel
            Left = 637
            Top = 0
            Width = 106
            Height = 48
            Align = alLeft
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 2
            object BtOK: TBitBtn
              Tag = 14
              Left = 8
              Top = 3
              Width = 90
              Height = 40
              Caption = '&OK'
              NumGlyphs = 2
              TabOrder = 0
              OnClick = BtOKClick
            end
          end
          object Panel13: TPanel
            Left = 743
            Top = 0
            Width = 257
            Height = 48
            Align = alClient
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 3
            object BtGraGruN: TBitBtn
              Tag = 30
              Left = 8
              Top = 3
              Width = 40
              Height = 40
              NumGlyphs = 2
              TabOrder = 0
              OnClick = BtGraGruNClick
            end
            object BtTabePrcCab: TBitBtn
              Tag = 10107
              Left = 49
              Top = 3
              Width = 40
              Height = 40
              NumGlyphs = 2
              TabOrder = 1
              OnClick = BtTabePrcCabClick
            end
            object BtFisRegCad: TBitBtn
              Tag = 10104
              Left = 90
              Top = 3
              Width = 40
              Height = 40
              NumGlyphs = 2
              TabOrder = 2
              OnClick = BtFisRegCadClick
            end
          end
        end
        object Panel9: TPanel
          Left = 0
          Top = 48
          Width = 1000
          Height = 28
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 1
          object Label4: TLabel
            Left = 12
            Top = 8
            Width = 86
            Height = 13
            Caption = 'Grupo de produto:'
            FocusControl = DBEdit1
          end
          object Label5: TLabel
            Left = 392
            Top = 8
            Width = 19
            Height = 13
            Caption = 'Cor:'
            FocusControl = DBEdit2
          end
          object Label6: TLabel
            Left = 576
            Top = 8
            Width = 48
            Height = 13
            Caption = 'Tamanho:'
            FocusControl = DBEdit3
          end
          object Label12: TLabel
            Left = 680
            Top = 8
            Width = 31
            Height = 13
            Caption = 'Pre'#231'o:'
            FocusControl = DBEdit7
          end
          object Label11: TLabel
            Left = 784
            Top = 8
            Width = 27
            Height = 13
            Caption = '% IPI:'
            FocusControl = DBEdit4
          end
          object DBEdit1: TDBEdit
            Left = 104
            Top = 4
            Width = 281
            Height = 21
            DataField = 'NOMENIVEL1'
            DataSource = DsItem
            TabOrder = 0
          end
          object DBEdit2: TDBEdit
            Left = 416
            Top = 4
            Width = 157
            Height = 21
            DataField = 'NOMECOR'
            DataSource = DsItem
            TabOrder = 1
          end
          object DBEdit3: TDBEdit
            Left = 628
            Top = 4
            Width = 49
            Height = 21
            DataField = 'NOMETAM'
            DataSource = DsItem
            TabOrder = 2
          end
          object DBEdit7: TDBEdit
            Left = 716
            Top = 4
            Width = 64
            Height = 21
            DataField = 'PrecoF'
            DataSource = DsPreco
            TabOrder = 3
          end
          object DBEdit4: TDBEdit
            Left = 816
            Top = 4
            Width = 40
            Height = 21
            DataField = 'IPI_Alq'
            DataSource = DsItem
            TabOrder = 4
          end
        end
      end
    end
    object PageControl2: TPageControl
      Left = 0
      Top = 153
      Width = 1008
      Height = 359
      ActivePage = TabSheet4
      Align = alClient
      TabOrder = 2
      OnChange = PageControl2Change
      object TabSheet4: TTabSheet
        Caption = ' Itens em faturamento '
        ImageIndex = 1
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object PageControl3: TPageControl
          Left = 383
          Top = 0
          Width = 617
          Height = 331
          ActivePage = TabSheet3
          Align = alClient
          TabOrder = 0
          object TabSheet7: TTabSheet
            Caption = ' A faturar '
            ImageIndex = 2
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object GradeQ: TStringGrid
              Left = 0
              Top = 0
              Width = 613
              Height = 309
              Align = alClient
              ColCount = 2
              DefaultColWidth = 65
              DefaultRowHeight = 18
              FixedCols = 0
              RowCount = 2
              FixedRows = 0
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goEditing, goTabs]
              ParentFont = False
              TabOrder = 0
              OnDrawCell = GradeQDrawCell
            end
          end
          object TabSheet3: TTabSheet
            Caption = ' Pendentes '
            ImageIndex = 4
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object GradeP: TStringGrid
              Left = 0
              Top = 0
              Width = 609
              Height = 303
              Align = alClient
              ColCount = 2
              DefaultColWidth = 65
              DefaultRowHeight = 18
              FixedCols = 0
              RowCount = 2
              FixedRows = 0
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goEditing, goTabs]
              ParentFont = False
              TabOrder = 0
              OnClick = GradePClick
              OnDblClick = GradePDblClick
              OnDrawCell = GradePDrawCell
              OnSelectCell = GradePSelectCell
            end
          end
          object TabSheet8: TTabSheet
            Caption = ' C'#243'digos '
            ImageIndex = 3
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object GradeC: TStringGrid
              Left = 0
              Top = 0
              Width = 613
              Height = 309
              Align = alClient
              ColCount = 2
              DefaultColWidth = 65
              DefaultRowHeight = 18
              RowCount = 2
              TabOrder = 0
              OnDrawCell = GradeCDrawCell
              RowHeights = (
                18
                19)
            end
          end
          object TabSheet9: TTabSheet
            Caption = ' Ativos '
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object GradeA: TStringGrid
              Left = 0
              Top = 0
              Width = 613
              Height = 309
              Align = alClient
              ColCount = 2
              DefaultColWidth = 65
              DefaultRowHeight = 18
              FixedCols = 0
              RowCount = 2
              FixedRows = 0
              TabOrder = 0
              OnDrawCell = GradeADrawCell
              RowHeights = (
                18
                18)
            end
          end
          object TabSheet10: TTabSheet
            Caption = ' X '
            ImageIndex = 6
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object GradeX: TStringGrid
              Left = 0
              Top = 0
              Width = 613
              Height = 309
              Align = alClient
              ColCount = 2
              DefaultColWidth = 65
              DefaultRowHeight = 18
              FixedCols = 0
              RowCount = 2
              FixedRows = 0
              TabOrder = 0
              OnDrawCell = GradeXDrawCell
              RowHeights = (
                18
                18)
            end
          end
        end
        object Panel11: TPanel
          Left = 0
          Top = 0
          Width = 383
          Height = 331
          Align = alLeft
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 1
          object DBGGru: TdmkDBGrid
            Left = 0
            Top = 21
            Width = 383
            Height = 310
            Align = alClient
            Columns = <
              item
                Expanded = False
                FieldName = 'CodUsu'
                Title.Caption = 'C'#243'digo'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Nome'
                Title.Caption = 'Descri'#231#227'o'
                Width = 220
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'QuantF'
                Title.Caption = 'Pendente'
                Width = 61
                Visible = True
              end>
            Color = clWindow
            DataSource = DsPediGru
            Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
            TabOrder = 0
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            Columns = <
              item
                Expanded = False
                FieldName = 'CodUsu'
                Title.Caption = 'C'#243'digo'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Nome'
                Title.Caption = 'Descri'#231#227'o'
                Width = 220
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'QuantF'
                Title.Caption = 'Pendente'
                Width = 61
                Visible = True
              end>
          end
          object Panel12: TPanel
            Left = 0
            Top = 0
            Width = 383
            Height = 21
            Align = alTop
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 1
            object CkSoItensAFat: TCheckBox
              Left = 4
              Top = 3
              Width = 250
              Height = 17
              Caption = 'Mostrar somente grupos com itens a faturar.'
              TabOrder = 0
              OnClick = CkSoItensAFatClick
            end
          end
        end
      end
      object TabSheet6: TTabSheet
        Caption = ' Lista '
        ImageIndex = 3
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object DBGFatPedIts: TDBGrid
          Left = 0
          Top = 99
          Width = 1002
          Height = 235
          Align = alClient
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'CU_NIVEL1'
              Title.Caption = 'Produto'
              Width = 75
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_NIVEL1'
              Title.Caption = 'Descri'#231#227'o'
              Width = 325
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'CU_COR'
              Title.Caption = 'Cor'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_COR'
              Title.Caption = 'Descri'#231#227'o'
              Width = 199
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_TAM'
              Title.Caption = 'Tamanho'
              Width = 104
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'QTDE_POSITIVO'
              Title.Caption = 'Quantidade'
              Width = 86
              Visible = True
            end>
        end
        object PnSimu: TPanel
          Left = 0
          Top = 0
          Width = 1002
          Height = 53
          Align = alTop
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 1
          Visible = False
          object Label9: TLabel
            Left = 368
            Top = 4
            Width = 54
            Height = 13
            Caption = 'Sequ'#234'ncia:'
          end
          object Label8: TLabel
            Left = 284
            Top = 4
            Width = 48
            Height = 13
            Caption = 'Reduzido:'
          end
          object Label7: TLabel
            Left = 200
            Top = 4
            Width = 75
            Height = 13
            Caption = 'Ordem de serv.:'
          end
          object dmkEdit2: TdmkEdit
            Left = 284
            Top = 20
            Width = 81
            Height = 21
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 6
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '000010'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 10
            ValWarn = False
            OnChange = EdLeituraChange
          end
          object dmkEdit3: TdmkEdit
            Left = 368
            Top = 20
            Width = 81
            Height = 21
            Alignment = taRightJustify
            TabOrder = 1
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 8
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '00000001'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 1
            ValWarn = False
            OnChange = EdLeituraChange
          end
          object dmkEdit1: TdmkEdit
            Left = 200
            Top = 20
            Width = 81
            Height = 21
            Alignment = taRightJustify
            TabOrder = 2
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 6
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '000000'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            OnChange = EdLeituraChange
          end
          object Button1: TButton
            Left = 4
            Top = 12
            Width = 185
            Height = 25
            Caption = 'Simula leitura do c'#243'digo de barras'
            TabOrder = 3
            OnClick = Button1Click
          end
        end
        object Panel10: TPanel
          Left = 0
          Top = 53
          Width = 1002
          Height = 46
          Align = alTop
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 2
          Visible = False
          object Label13: TLabel
            Left = 4
            Top = 4
            Width = 33
            Height = 13
            Caption = '% C.F.:'
            FocusControl = DBEdit8
          end
          object Label14: TLabel
            Left = 56
            Top = 4
            Width = 66
            Height = 13
            Caption = 'ICMS UF>UF:'
            FocusControl = DBEdit16
          end
          object DBEdit8: TDBEdit
            Left = 4
            Top = 20
            Width = 48
            Height = 24
            DataField = 'Variacao'
            TabOrder = 0
          end
          object DBCkICMS: TDBCheckBox
            Left = 132
            Top = 2
            Width = 48
            Height = 17
            Caption = 'ICMS:'
            DataField = 'ICMS_Usa'
            TabOrder = 1
            ValueChecked = '1'
            ValueUnchecked = '0'
          end
          object DBEdit9: TDBEdit
            Left = 132
            Top = 20
            Width = 48
            Height = 24
            DataField = 'ICMS_Alq'
            TabOrder = 2
          end
          object DBEdit10: TDBEdit
            Left = 236
            Top = 20
            Width = 48
            Height = 24
            DataField = 'PIS_Alq'
            TabOrder = 3
          end
          object DBCheckBox2: TDBCheckBox
            Left = 236
            Top = 2
            Width = 48
            Height = 17
            Caption = 'PIS:'
            DataField = 'PIS_Usa'
            TabOrder = 4
            ValueChecked = '1'
            ValueUnchecked = '0'
          end
          object DBEdit11: TDBEdit
            Left = 288
            Top = 20
            Width = 48
            Height = 24
            DataField = 'COFINS_Alq'
            TabOrder = 5
          end
          object DBCheckBox3: TDBCheckBox
            Left = 288
            Top = 2
            Width = 48
            Height = 17
            Caption = 'Cofins:'
            DataField = 'COFINS_Usa'
            TabOrder = 6
            ValueChecked = '1'
            ValueUnchecked = '0'
          end
          object DBEdit12: TDBEdit
            Left = 340
            Top = 20
            Width = 48
            Height = 24
            DataField = 'IR_Alq'
            TabOrder = 7
          end
          object DBCheckBox4: TDBCheckBox
            Left = 340
            Top = 2
            Width = 48
            Height = 17
            Caption = 'IR:'
            DataField = 'IR_Usa'
            TabOrder = 8
            ValueChecked = '1'
            ValueUnchecked = '0'
          end
          object DBEdit13: TDBEdit
            Left = 392
            Top = 20
            Width = 48
            Height = 24
            DataField = 'CS_Alq'
            TabOrder = 9
          end
          object DBCheckBox5: TDBCheckBox
            Left = 392
            Top = 2
            Width = 48
            Height = 17
            Caption = 'CS:'
            DataField = 'CS_Usa'
            TabOrder = 10
            ValueChecked = '1'
            ValueUnchecked = '0'
          end
          object DBEdit14: TDBEdit
            Left = 184
            Top = 20
            Width = 48
            Height = 24
            DataField = 'IPI_Alq'
            TabOrder = 11
          end
          object DBCheckBox6: TDBCheckBox
            Left = 184
            Top = 2
            Width = 48
            Height = 17
            Caption = 'IPI:'
            DataField = 'IPI_Usa'
            TabOrder = 12
            ValueChecked = '1'
            ValueUnchecked = '0'
          end
          object DBEdit15: TDBEdit
            Left = 444
            Top = 20
            Width = 48
            Height = 24
            DataField = 'ISS_Alq'
            TabOrder = 13
          end
          object DBCheckBox7: TDBCheckBox
            Left = 444
            Top = 2
            Width = 48
            Height = 17
            Caption = 'ISS:'
            DataField = 'ISS_Usa'
            TabOrder = 14
            ValueChecked = '1'
            ValueUnchecked = '0'
          end
          object DBEdit16: TDBEdit
            Left = 56
            Top = 20
            Width = 73
            Height = 24
            DataField = 'ICMS_V'
            TabOrder = 15
          end
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 912
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 389
        Height = 32
        Caption = 'Itens de Faturamento de Pedido'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 389
        Height = 32
        Caption = 'Itens de Faturamento de Pedido'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 389
        Height = 32
        Caption = 'Itens de Faturamento de Pedido'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 594
    Width = 1008
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel5: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 638
    Width = 1008
    Height = 64
    Align = alBottom
    TabOrder = 3
    object Panel14: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 47
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object PnSaiDesis: TPanel
        Left = 859
        Top = 0
        Width = 145
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 3
        object BtSaida: TBitBtn
          Tag = 13
          Left = 2
          Top = 3
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Hint = 'Sai da janela atual'
          Caption = '&Sa'#237'da'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
        end
      end
      object CkFixo: TCheckBox
        Left = 12
        Top = 16
        Width = 101
        Height = 17
        Caption = 'Quantidade fixa.'
        Checked = True
        State = cbChecked
        TabOrder = 0
      end
      object BtFatura: TBitBtn
        Tag = 10102
        Left = 120
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Fatura'
        Enabled = False
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        OnClick = BtFaturaClick
      end
      object BtExclui: TBitBtn
        Tag = 12
        Left = 457
        Top = 3
        Width = 120
        Height = 40
        Caption = '&Excluir'
        NumGlyphs = 2
        TabOrder = 2
        Visible = False
        OnClick = BtExcluiClick
      end
    end
  end
  object PB1: TProgressBar
    Left = 0
    Top = 560
    Width = 1008
    Height = 17
    Margins.Left = 2
    Margins.Top = 2
    Margins.Right = 2
    Margins.Bottom = 2
    Align = alBottom
    TabOrder = 4
  end
  object PB2: TProgressBar
    Left = 0
    Top = 577
    Width = 1008
    Height = 17
    Margins.Left = 2
    Margins.Top = 2
    Margins.Right = 2
    Margins.Bottom = 2
    Align = alBottom
    TabOrder = 5
  end
  object QrStqCenCad: TMySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrStqCenCadAfterOpen
    SQL.Strings = (
      'SELECT Codigo, CodUsu, Nome'
      'FROM stqcencad scc'
      'WHERE Codigo IN '
      '('
      '  SELECT StqCenCad'
      '  FROM fisregmvt'
      '  WHERE Codigo=:P0'
      '  AND Empresa=:P1'
      ') '
      'ORDER BY Nome')
    Left = 8
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrStqCenCadCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrStqCenCadCodUsu: TIntegerField
      FieldName = 'CodUsu'
      Required = True
    end
    object QrStqCenCadNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 50
    end
  end
  object DsStqCenCad: TDataSource
    DataSet = QrStqCenCad
    Left = 36
    Top = 8
  end
  object QrItem: TMySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrItemBeforeClose
    SQL.Strings = (
      'SELECT gg1.Nome NOMENIVEL1, ggc.GraCorCad, '
      'gcc.Nome NOMECOR,  gti.Nome NOMETAM, '
      'ggx.Controle GraGruX, ggx.GraGru1, '
      'gg1.CodUsu CU_Nivel1, gg1.IPI_Alq, '
      'pgt.MadeBy, pgt.Fracio, '
      'gg1.HowBxaEstq, gg1.GerBxaEstq, gg1.prod_indTot'
      'FROM gragrux ggx '
      'LEFT JOIN gragru1   gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN gragruc ggc ON ggc.Controle=ggx.GraGruC'
      'LEFT JOIN gracorcad gcc ON gcc.Codigo=ggc.GraCorCad'
      'LEFT JOIN gratamits gti ON gti.Controle=ggx.GraTamI'
      'LEFT JOIN prdgruptip pgt ON pgt.Codigo=gg1.PrdGrupTip'
      'WHERE ggx.Controle=:P0'
      '')
    Left = 68
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrItemNOMENIVEL1: TWideStringField
      FieldName = 'NOMENIVEL1'
      Origin = 'gragru1.Nome'
      Size = 30
    end
    object QrItemGraCorCad: TIntegerField
      FieldName = 'GraCorCad'
      Origin = 'gragruc.GraCorCad'
    end
    object QrItemNOMECOR: TWideStringField
      FieldName = 'NOMECOR'
      Origin = 'gracorcad.Nome'
      Size = 30
    end
    object QrItemNOMETAM: TWideStringField
      FieldName = 'NOMETAM'
      Origin = 'gratamits.Nome'
      Size = 5
    end
    object QrItemGraGruX: TIntegerField
      FieldName = 'GraGruX'
      Origin = 'gragrux.Controle'
      Required = True
    end
    object QrItemGraGru1: TIntegerField
      FieldName = 'GraGru1'
      Origin = 'gragrux.GraGru1'
      Required = True
    end
    object QrItemCU_Nivel1: TIntegerField
      FieldName = 'CU_Nivel1'
      Required = True
    end
    object QrItemIPI_Alq: TFloatField
      FieldName = 'IPI_Alq'
    end
    object QrItemMadeBy: TSmallintField
      FieldName = 'MadeBy'
    end
    object QrItemFracio: TSmallintField
      FieldName = 'Fracio'
    end
    object QrItemHowBxaEstq: TSmallintField
      FieldName = 'HowBxaEstq'
    end
    object QrItemGerBxaEstq: TSmallintField
      FieldName = 'GerBxaEstq'
    end
    object QrItemprod_indTot: TSmallintField
      FieldName = 'prod_indTot'
    end
  end
  object DsItem: TDataSource
    DataSet = QrItem
    Left = 96
    Top = 8
  end
  object DsFatPedVol: TDataSource
    Left = 192
    Top = 84
  end
  object DsFatPedCab: TDataSource
    Left = 164
    Top = 84
  end
  object QrPreco: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT pvi.PrecoF, InfAdCuztm,'
      '(QuantP-QuantC-QuantV) QuantF,'
      'PercCustom, MedidaC, MedidaL,'
      'MedidaA, MedidaE, MedOrdem'
      'FROM pedivdaits pvi'
      'LEFT JOIN gragrux ggx ON ggx.Controle=pvi.GraGruX'
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1'
      'WHERE pvi.Controle=:P0'
      '')
    Left = 124
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPrecoPrecoF: TFloatField
      FieldName = 'PrecoF'
      Required = True
    end
    object QrPrecoQuantF: TFloatField
      FieldName = 'QuantF'
      Required = True
    end
    object QrPrecoInfAdCuztm: TIntegerField
      FieldName = 'InfAdCuztm'
    end
    object QrPrecoPercCustom: TFloatField
      FieldName = 'PercCustom'
    end
    object QrPrecoMedidaC: TFloatField
      FieldName = 'MedidaC'
    end
    object QrPrecoMedidaL: TFloatField
      FieldName = 'MedidaL'
    end
    object QrPrecoMedidaA: TFloatField
      FieldName = 'MedidaA'
    end
    object QrPrecoMedidaE: TFloatField
      FieldName = 'MedidaE'
    end
    object QrPrecoMedOrdem: TIntegerField
      FieldName = 'MedOrdem'
    end
  end
  object DsPreco: TDataSource
    DataSet = QrPreco
    Left = 152
    Top = 8
  end
  object QrFator: TMySQLQuery
    Database = Dmod.MyDB
    Left = 180
    Top = 8
  end
  object QrPediGru: TMySQLQuery
    Database = Dmod.MyDB
    Filter = 'QuantF>0'
    AfterScroll = QrPediGruAfterScroll
    SQL.Strings = (
      'SELECT SUM(pvi.QuantP-pvi.QuantC-pvi.QuantV) QuantF,'
      'gti.Codigo GRATAMCAD, pgt.Fracio,'
      'gg1.CodUsu, gg1.Nome, gg1.Nivel1'
      'FROM pedivdaits pvi'
      'LEFT JOIN gragrux   ggx ON ggx.Controle=pvi.GraGruX'
      'LEFT JOIN gratamits gti ON gti.Controle=ggx.GraTamI'
      'LEFT JOIN gragru1   gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN prdgruptip pgt ON pgt.Codigo=gg1.PrdGrupTip'
      'WHERE pvi.Codigo=:P0'
      'GROUP BY gg1.Nivel1')
    Left = 708
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPediGruQuantF: TFloatField
      FieldName = 'QuantF'
    end
    object QrPediGruCodUsu: TIntegerField
      FieldName = 'CodUsu'
      Origin = 'gragru1.CodUsu'
      Required = True
    end
    object QrPediGruNome: TWideStringField
      FieldName = 'Nome'
      Origin = 'gragru1.Nome'
      Size = 30
    end
    object QrPediGruNivel1: TIntegerField
      FieldName = 'Nivel1'
      Origin = 'gragru1.Nivel1'
      Required = True
    end
    object QrPediGruGRATAMCAD: TIntegerField
      FieldName = 'GRATAMCAD'
      Origin = 'gratamits.Codigo'
    end
    object QrPediGruFracio: TSmallintField
      FieldName = 'Fracio'
    end
  end
  object DsPediGru: TDataSource
    DataSet = QrPediGru
    Left = 736
    Top = 8
  end
  object QrCFOP: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT CFOP '
      'FROM fisregcfop'
      'WHERE Codigo=:P0'
      'AND Contribui=:P1'
      'AND Interno=:P2'
      'AND Proprio=:P3'
      '')
    Left = 204
    Top = 144
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
    object QrCFOPCFOP: TWideStringField
      FieldName = 'CFOP'
      Size = 5
    end
  end
  object QrPediGruTot: TMySQLQuery
    Database = Dmod.MyDB
    Filter = 'QuantF>0'
    AfterScroll = QrPediGruAfterScroll
    SQL.Strings = (
      'SELECT SUM(QuantP - QuantC - QuantV) QuantF, SUM(QuantV) QuantV'
      'FROM pedivdaits'
      'WHERE Codigo=:P0'
      'GROUP BY Codigo')
    Left = 764
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPediGruTotQuantV: TFloatField
      FieldName = 'QuantV'
    end
    object QrPediGruTotQuantF: TFloatField
      FieldName = 'QuantF'
    end
  end
  object QrItem2: TMySQLQuery
    Database = Dmod.MyDB
    Left = 356
    Top = 504
  end
end
