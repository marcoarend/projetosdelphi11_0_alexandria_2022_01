object FmPediVdaAlt2_PediCompra: TFmPediVdaAlt2_PediCompra
  Left = 339
  Top = 185
  Caption = 'PED-VENDA-003 :: Item de Pedido '
  ClientHeight = 456
  ClientWidth = 554
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 554
    Height = 300
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    ExplicitHeight = 231
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 554
      Height = 76
      Align = alTop
      BevelOuter = bvNone
      Enabled = False
      TabOrder = 1
      object GroupBox1: TGroupBox
        Left = 4
        Top = 4
        Width = 217
        Height = 65
        Caption = ' Controle: '
        TabOrder = 0
        object Label3: TLabel
          Left = 8
          Top = 20
          Width = 50
          Height = 13
          Caption = 'ID Pedido:'
        end
        object Label4: TLabel
          Left = 76
          Top = 20
          Width = 46
          Height = 13
          Caption = 'ID Grupo:'
        end
        object Label5: TLabel
          Left = 144
          Top = 20
          Width = 37
          Height = 13
          Caption = 'ID Item:'
        end
        object EdCodigo: TdmkEdit
          Left = 8
          Top = 36
          Width = 65
          Height = 21
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'Codigo'
          UpdCampo = 'Codigo'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
        object EdGraGruX: TdmkEdit
          Left = 76
          Top = 36
          Width = 65
          Height = 21
          Alignment = taRightJustify
          TabOrder = 1
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'GraGruX'
          UpdCampo = 'GraGruX'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
        object EdControle: TdmkEdit
          Left = 144
          Top = 36
          Width = 65
          Height = 21
          Alignment = taRightJustify
          TabOrder = 2
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'Controle'
          UpdCampo = 'Controle'
          UpdType = utInc
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
      end
      object GroupBox2: TGroupBox
        Left = 224
        Top = 4
        Width = 317
        Height = 65
        Caption = ' Informa'#231#245'es: '
        TabOrder = 1
        object Label1: TLabel
          Left = 8
          Top = 20
          Width = 63
          Height = 13
          Caption = 'Qtde m'#237'nima:'
        end
        object Label6: TLabel
          Left = 84
          Top = 20
          Width = 52
          Height = 13
          Caption = 'Pre'#231'o lista:'
        end
        object Label8: TLabel
          Left = 160
          Top = 20
          Width = 64
          Height = 13
          Caption = 'Itens cancel.:'
        end
        object Label9: TLabel
          Left = 236
          Top = 20
          Width = 53
          Height = 13
          Caption = 'Itens fatur.:'
        end
        object EdQtdeMin: TdmkEdit
          Left = 8
          Top = 36
          Width = 72
          Height = 21
          Alignment = taRightJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
        object EdPrecoO: TdmkEdit
          Left = 84
          Top = 36
          Width = 72
          Height = 21
          Alignment = taRightJustify
          TabOrder = 1
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          QryCampo = 'PrecoO'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object EdQuantC_A: TdmkEdit
          Left = 160
          Top = 36
          Width = 72
          Height = 21
          Alignment = taRightJustify
          TabOrder = 2
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 3
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,000'
          QryCampo = 'QuantC'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object EdQuantV: TdmkEdit
          Left = 236
          Top = 36
          Width = 72
          Height = 21
          Alignment = taRightJustify
          TabOrder = 3
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 3
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,000'
          QryCampo = 'QuantV'
          UpdType = utInc
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
          OnChange = EdQuantVChange
        end
      end
    end
    object Panel4: TPanel
      Left = 0
      Top = 76
      Width = 554
      Height = 224
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitHeight = 155
      object GroupBox3: TGroupBox
        Left = 52
        Top = 0
        Width = 421
        Height = 145
        Caption = ' Digita'#231#227'o:  '
        TabOrder = 0
        object Label2: TLabel
          Left = 12
          Top = 20
          Width = 50
          Height = 13
          Caption = 'Itens ped.:'
        end
        object LaPrecoR: TLabel
          Left = 112
          Top = 20
          Width = 52
          Height = 13
          Caption = 'Pre'#231'o lista:'
        end
        object Label7: TLabel
          Left = 314
          Top = 100
          Width = 56
          Height = 13
          Caption = 'Itens canc.:'
        end
        object Label14: TLabel
          Left = 212
          Top = 20
          Width = 49
          Height = 13
          Caption = '$ Produto:'
          Enabled = False
        end
        object Label247: TLabel
          Left = 12
          Top = 60
          Width = 36
          Height = 13
          Caption = '$ Frete:'
        end
        object Label248: TLabel
          Left = 112
          Top = 60
          Width = 46
          Height = 13
          Caption = '$ Seguro:'
        end
        object Label249: TLabel
          Left = 312
          Top = 20
          Width = 58
          Height = 13
          Caption = '$ Desconto:'
        end
        object Label15: TLabel
          Left = 212
          Top = 60
          Width = 67
          Height = 13
          Caption = '$ Desp. Aces:'
        end
        object Label16: TLabel
          Left = 312
          Top = 60
          Width = 32
          Height = 13
          Caption = '$ vBC:'
          Enabled = False
        end
        object Label10: TLabel
          Left = 12
          Top = 100
          Width = 37
          Height = 13
          Caption = '$ Bruto:'
          Enabled = False
        end
        object Label11: TLabel
          Left = 212
          Top = 100
          Width = 48
          Height = 13
          Caption = '$ L'#237'quido:'
          Enabled = False
        end
        object Label12: TLabel
          Left = 112
          Top = 100
          Width = 58
          Height = 13
          Caption = '$ Desconto:'
          Enabled = False
        end
        object EdQuantP: TdmkEdit
          Left = 12
          Top = 36
          Width = 92
          Height = 21
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 3
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,000'
          QryCampo = 'QuantP'
          UpdCampo = 'QuantP'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
          OnChange = Recalcula_vBC
        end
        object EdPrecoR: TdmkEdit
          Left = 112
          Top = 36
          Width = 92
          Height = 21
          Alignment = taRightJustify
          TabOrder = 1
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 6
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,000000'
          QryCampo = 'PrecoR'
          UpdCampo = 'PrecoR'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
          OnChange = Recalcula_vBC
        end
        object EdQuantC_D: TdmkEdit
          Left = 314
          Top = 116
          Width = 92
          Height = 21
          Alignment = taRightJustify
          TabOrder = 11
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 3
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,000'
          QryCampo = 'QuantC'
          UpdCampo = 'QuantC'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
          OnChange = EdQuantPChange
        end
        object EdvProd: TdmkEdit
          Left = 212
          Top = 36
          Width = 92
          Height = 21
          Alignment = taRightJustify
          Enabled = False
          TabOrder = 2
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          QryCampo = 'vProd'
          UpdCampo = 'vProd'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object EdvFrete: TdmkEdit
          Left = 12
          Top = 76
          Width = 92
          Height = 21
          Alignment = taRightJustify
          TabOrder = 4
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          QryCampo = 'vFrete'
          UpdCampo = 'vFrete'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
          OnChange = Recalcula_vBC
        end
        object EdvSeg: TdmkEdit
          Left = 112
          Top = 76
          Width = 92
          Height = 21
          Alignment = taRightJustify
          TabOrder = 5
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          QryCampo = 'vSeg'
          UpdCampo = 'vSeg'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
          OnChange = Recalcula_vBC
        end
        object EdvDesc: TdmkEdit
          Left = 312
          Top = 36
          Width = 92
          Height = 21
          Alignment = taRightJustify
          TabOrder = 3
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          QryCampo = 'vDesc'
          UpdCampo = 'vDesc'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
          OnChange = Recalcula_vBC
        end
        object EdvOutro: TdmkEdit
          Left = 212
          Top = 76
          Width = 92
          Height = 21
          Alignment = taRightJustify
          TabOrder = 6
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          QryCampo = 'vOutro'
          UpdCampo = 'vOutro'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
          OnChange = Recalcula_vBC
        end
        object EdvBC: TdmkEdit
          Left = 312
          Top = 76
          Width = 92
          Height = 21
          Alignment = taRightJustify
          Enabled = False
          TabOrder = 7
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          QryCampo = 'vBC'
          UpdCampo = 'vBC'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object EdValBru: TdmkEdit
          Left = 12
          Top = 116
          Width = 92
          Height = 21
          Alignment = taRightJustify
          Enabled = False
          TabOrder = 8
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          QryCampo = 'ValBru'
          UpdCampo = 'ValBru'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object EdValLiq: TdmkEdit
          Left = 212
          Top = 116
          Width = 92
          Height = 21
          Alignment = taRightJustify
          Enabled = False
          TabOrder = 10
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          QryCampo = 'ValLiq'
          UpdCampo = 'ValLiq'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object EdDescoV: TdmkEdit
          Left = 112
          Top = 116
          Width = 92
          Height = 21
          Alignment = taRightJustify
          Enabled = False
          TabOrder = 9
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          QryCampo = 'DescoV'
          UpdCampo = 'DescoV'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
          OnChange = Recalcula_vBC
        end
      end
      object GroupBox4: TGroupBox
        Left = 52
        Top = 152
        Width = 421
        Height = 69
        Caption = ' Outras informa'#231#245'es: '
        TabOrder = 1
        object Label13: TLabel
          Left = 11
          Top = 24
          Width = 90
          Height = 13
          Caption = 'Pre'#231'o faturamento:'
          Enabled = False
        end
        object Label17: TLabel
          Left = 118
          Top = 24
          Width = 60
          Height = 13
          Caption = '% Desconto:'
          Enabled = False
        end
        object EdPrecoF: TdmkEdit
          Left = 13
          Top = 40
          Width = 100
          Height = 21
          TabStop = False
          Alignment = taRightJustify
          Enabled = False
          ReadOnly = True
          TabOrder = 0
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          QryCampo = 'PrecoF'
          UpdCampo = 'PrecoF'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
          OnChange = EdPrecoRChange
        end
        object EdDescoP: TdmkEdit
          Left = 118
          Top = 40
          Width = 100
          Height = 21
          TabStop = False
          Alignment = taRightJustify
          Enabled = False
          ReadOnly = True
          TabOrder = 1
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 10
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,0000000000'
          QryCampo = 'DescoP'
          UpdCampo = 'DescoP'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 554
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 506
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 458
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 190
        Height = 32
        Caption = 'Item de Pedido '
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 190
        Height = 32
        Caption = 'Item de Pedido '
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 190
        Height = 32
        Caption = 'Item de Pedido '
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 348
    Width = 554
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    ExplicitTop = 279
    object Panel5: TPanel
      Left = 2
      Top = 15
      Width = 550
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 392
    Width = 554
    Height = 64
    Align = alBottom
    TabOrder = 3
    ExplicitTop = 323
    object Panel6: TPanel
      Left = 2
      Top = 15
      Width = 550
      Height = 47
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object PnSaiDesis: TPanel
        Left = 406
        Top = 0
        Width = 144
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtSaida: TBitBtn
          Tag = 15
          Left = 2
          Top = 3
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Hint = 'Sai da janela atual'
          Caption = '&Desiste'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
        end
      end
      object BtOK: TBitBtn
        Tag = 14
        Left = 20
        Top = 3
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
end
