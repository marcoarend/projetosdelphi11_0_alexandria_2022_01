object FmPediVdaGru1: TFmPediVdaGru1
  Left = 339
  Top = 185
  Caption = 'PED-VENDA-002 :: Itens de Pedido - Por Grupo'
  ClientHeight = 596
  ClientWidth = 784
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 784
    Height = 428
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object PnSeleciona: TPanel
      Left = 0
      Top = 52
      Width = 784
      Height = 104
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object Label1: TLabel
        Left = 12
        Top = 4
        Width = 40
        Height = 13
        Caption = 'Produto:'
      end
      object SBMenu: TSpeedButton
        Left = 465
        Top = 20
        Width = 20
        Height = 21
        Caption = '...'
        OnClick = SBMenuClick
      end
      object Label14: TLabel
        Left = 489
        Top = 4
        Width = 27
        Height = 13
        Caption = 'NCM:'
      end
      object Label4: TLabel
        Left = 593
        Top = 4
        Width = 41
        Height = 13
        Caption = 'U. Med.:'
      end
      object EdGraGru1: TdmkEditCB
        Left = 12
        Top = 20
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = EdGraGru1Change
        OnEnter = EdGraGru1Enter
        OnExit = EdGraGru1Exit
        DBLookupComboBox = CBGraGru1
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBGraGru1: TdmkDBLookupComboBox
        Left = 72
        Top = 20
        Width = 390
        Height = 21
        KeyField = 'CodUsu'
        ListField = 'NOME_EX'
        ListSource = DsGraGru1
        TabOrder = 1
        OnExit = CBGraGru1Exit
        dmkEditCB = EdGraGru1
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object ST3: TStaticText
        Left = 0
        Top = 85
        Width = 784
        Height = 19
        Align = alBottom
        Alignment = taCenter
        Caption = 'ST3'
        Color = clBtnFace
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -12
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
        TabOrder = 6
        Transparent = False
      end
      object ST2: TStaticText
        Left = 0
        Top = 66
        Width = 784
        Height = 19
        Align = alBottom
        Alignment = taCenter
        BevelOuter = bvRaised
        Caption = 'ST2'
        Color = clBtnFace
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -12
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
        TabOrder = 5
        Transparent = False
      end
      object ST1: TStaticText
        Left = 0
        Top = 47
        Width = 784
        Height = 19
        Align = alBottom
        Alignment = taCenter
        Caption = 'ST1'
        Color = clBtnFace
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -12
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentColor = False
        ParentFont = False
        TabOrder = 4
        Transparent = False
      end
      object PnJuros: TPanel
        Left = 627
        Top = 0
        Width = 157
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        Enabled = False
        TabOrder = 3
        object Label2: TLabel
          Left = 11
          Top = 4
          Width = 61
          Height = 13
          Caption = '% Juros m'#234's:'
          FocusControl = DBEdit1
        end
        object Label3: TLabel
          Left = 76
          Top = 4
          Width = 76
          Height = 13
          Caption = '% Custo financ.:'
          FocusControl = DBEdit1
        end
        object DBEdit1: TDBEdit
          Left = 8
          Top = 20
          Width = 65
          Height = 21
          DataField = 'JurosMes'
          TabOrder = 0
        end
        object EdCustoFin: TdmkEdit
          Left = 76
          Top = 20
          Width = 76
          Height = 21
          Alignment = taRightJustify
          TabOrder = 1
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 4
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,0000'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
      end
      object DBEdit2: TDBEdit
        Left = 489
        Top = 20
        Width = 100
        Height = 21
        DataField = 'NCM'
        DataSource = DsGraGru1
        TabOrder = 2
      end
      object DBEdit3: TDBEdit
        Left = 593
        Top = 20
        Width = 38
        Height = 21
        DataField = 'SIGLAUNIDMED'
        DataSource = DsGraGru1
        TabOrder = 7
      end
    end
    object PageControl1: TPageControl
      Left = 0
      Top = 156
      Width = 784
      Height = 272
      ActivePage = TabSheet5
      Align = alClient
      MultiLine = True
      TabOrder = 1
      Visible = False
      object TabSheet5: TTabSheet
        Caption = ' Quantidade '
        ImageIndex = 2
        object GradeQ: TStringGrid
          Left = 0
          Top = 0
          Width = 776
          Height = 227
          Align = alClient
          ColCount = 2
          DefaultColWidth = 65
          DefaultRowHeight = 18
          FixedCols = 0
          RowCount = 2
          FixedRows = 0
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goTabs]
          ParentFont = False
          TabOrder = 0
          OnDblClick = GradeQDblClick
          OnDrawCell = GradeQDrawCell
          OnKeyDown = GradeQKeyDown
          OnSelectCell = GradeQSelectCell
        end
        object StaticText1: TStaticText
          Left = 0
          Top = 227
          Width = 776
          Height = 17
          Align = alBottom
          Alignment = taCenter
          BorderStyle = sbsSunken
          Caption = 
            'D'#234' um duplo clique na c'#233'lula, coluna ou linha correspondente par' +
            'a incluir / alterar a quantidade.'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlue
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 1
          Visible = False
        end
      end
      object TabSheet2: TTabSheet
        Caption = ' Valor unit'#225'rio '
        ImageIndex = 5
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object GradeF: TStringGrid
          Left = 0
          Top = 0
          Width = 778
          Height = 247
          Align = alClient
          ColCount = 2
          DefaultColWidth = 100
          DefaultRowHeight = 18
          FixedCols = 0
          RowCount = 2
          FixedRows = 0
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goEditing, goTabs]
          ParentFont = False
          TabOrder = 0
          OnDrawCell = GradeFDrawCell
          OnKeyDown = GradeFKeyDown
          OnSelectCell = GradeFSelectCell
        end
      end
      object TabSheet4: TTabSheet
        Caption = ' Desconto '
        ImageIndex = 6
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object GradeD: TStringGrid
          Left = 0
          Top = 0
          Width = 778
          Height = 231
          Align = alClient
          ColCount = 2
          DefaultColWidth = 65
          DefaultRowHeight = 18
          FixedCols = 0
          RowCount = 2
          FixedRows = 0
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goEditing, goTabs]
          ParentFont = False
          TabOrder = 0
          OnClick = GradeDClick
          OnDrawCell = GradeDDrawCell
          OnKeyDown = GradeDKeyDown
          OnSelectCell = GradeDSelectCell
        end
        object StaticText3: TStaticText
          Left = 0
          Top = 231
          Width = 544
          Height = 17
          Align = alBottom
          Alignment = taCenter
          BorderStyle = sbsSunken
          Caption = 
            'D'#234' um duplo clique na c'#233'lula, coluna ou linha correspondente par' +
            'a incluir / alterar o desconto.'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlue
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 1
        end
      end
      object TabSheet1: TTabSheet
        Caption = ' Pre'#231'o da lista'
        ImageIndex = 1
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object GradeL: TStringGrid
          Left = 0
          Top = 0
          Width = 778
          Height = 247
          Align = alClient
          ColCount = 2
          DefaultColWidth = 100
          DefaultRowHeight = 18
          FixedCols = 0
          RowCount = 2
          FixedRows = 0
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goTabs]
          ParentFont = False
          TabOrder = 0
          OnDrawCell = GradeLDrawCell
        end
      end
      object TabSheet6: TTabSheet
        Caption = ' C'#243'digos '
        ImageIndex = 3
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object GradeC: TStringGrid
          Left = 0
          Top = 0
          Width = 778
          Height = 231
          Align = alClient
          ColCount = 1
          DefaultColWidth = 65
          DefaultRowHeight = 18
          FixedCols = 0
          RowCount = 1
          FixedRows = 0
          TabOrder = 0
          OnDrawCell = GradeCDrawCell
          RowHeights = (
            18)
        end
        object StaticText6: TStaticText
          Left = 0
          Top = 231
          Width = 502
          Height = 17
          Align = alBottom
          Alignment = taCenter
          BorderStyle = sbsSunken
          Caption = 
            'Para gerar o c'#243'digo, clique na c'#233'lula, coluna ou linha correspon' +
            'dente na guia "Ativos".'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlue
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 1
          Visible = False
        end
      end
      object TabSheet3: TTabSheet
        Caption = ' Ativos '
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object GradeA: TStringGrid
          Left = 0
          Top = 0
          Width = 778
          Height = 231
          Align = alClient
          ColCount = 2
          DefaultColWidth = 65
          DefaultRowHeight = 18
          FixedCols = 0
          RowCount = 2
          FixedRows = 0
          TabOrder = 0
          OnDrawCell = GradeADrawCell
          RowHeights = (
            18
            18)
        end
        object StaticText2: TStaticText
          Left = 0
          Top = 231
          Width = 475
          Height = 17
          Align = alBottom
          Alignment = taCenter
          BorderStyle = sbsSunken
          Caption = 
            'Clique na c'#233'lula, coluna ou linha correspondente para ativar / d' +
            'esativar o produto.'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlue
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 1
          Visible = False
        end
      end
      object TabSheet9: TTabSheet
        Caption = ' X '
        ImageIndex = 6
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object GradeX: TStringGrid
          Left = 0
          Top = 0
          Width = 778
          Height = 247
          Align = alClient
          ColCount = 2
          DefaultColWidth = 65
          DefaultRowHeight = 18
          FixedCols = 0
          RowCount = 2
          FixedRows = 0
          TabOrder = 0
          OnDrawCell = GradeXDrawCell
          RowHeights = (
            18
            18)
        end
      end
    end
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 784
      Height = 52
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 2
      object RGLista: TRadioGroup
        Left = 492
        Top = 0
        Width = 292
        Height = 52
        Align = alRight
        Caption = '  Como listar produto: '
        Columns = 2
        ItemIndex = 0
        Items.Strings = (
          'Codigo - Nome'
          'Somente nome')
        TabOrder = 0
        OnClick = RGListaClick
      end
      object RGTipPrd: TdmkRadioGroup
        Left = 0
        Top = 0
        Width = 488
        Height = 52
        Align = alLeft
        Caption = 'Tipifica'#231#227'o: '
        Columns = 4
        ItemIndex = 1
        Items.Strings = (
          'Nenhum'
          'Produto'
          'Mat. prima'
          'Uso/consu.')
        TabOrder = 1
        OnClick = RGTipPrdClick
        QryCampo = 'TipPrd'
        UpdCampo = 'TipPrd'
        UpdType = utYes
        OldValor = 0
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 784
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 736
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
        OnChange = ImgTipoChange
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 688
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 338
        Height = 32
        Caption = 'Itens de Pedido - Por Grupo'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 338
        Height = 32
        Caption = 'Itens de Pedido - Por Grupo'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 338
        Height = 32
        Caption = 'Itens de Pedido - Por Grupo'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 476
    Width = 784
    Height = 56
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 780
      Height = 39
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 559
        Height = 17
        Caption = 
          'Verifique se o produto selecionado tem pre'#231'o cadastrado na lista' +
          ' de pre'#231'os do pedido.'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 559
        Height = 17
        Caption = 
          'Verifique se o produto selecionado tem pre'#231'o cadastrado na lista' +
          ' de pre'#231'os do pedido.'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object PB1: TProgressBar
        Left = 0
        Top = 22
        Width = 780
        Height = 17
        Align = alBottom
        TabOrder = 0
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 532
    Width = 784
    Height = 64
    Align = alBottom
    TabOrder = 3
    object Panel5: TPanel
      Left = 2
      Top = 15
      Width = 780
      Height = 47
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object PnSaiDesis: TPanel
        Left = 636
        Top = 0
        Width = 144
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtSaida: TBitBtn
          Tag = 13
          Left = 2
          Top = 3
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Hint = 'Sai da janela atual'
          Caption = '&Sa'#237'da'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
        end
      end
      object BtOK: TBitBtn
        Tag = 14
        Left = 20
        Top = 3
        Width = 120
        Height = 40
        Caption = '&OK'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object QrGraGru1: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT gg1.Codusu, gg1.Nivel3, gg1.Nivel2, gg1.Nivel1,'
      'gg1.Nome, gg1.PrdGrupTip, gg1.GraTamCad,'
      'gtc.Nome NOMEGRATAMCAD, gtc.CodUsu CODUSUGRATAMCAD,'
      'gg1.CST_A, gg1.CST_B, gg1.UnidMed, gg1.NCM, gg1.Peso,'
      'unm.Sigla SIGLAUNIDMED, unm.CodUsu CODUSUUNIDMED,'
      'unm.Nome NOMEUNIDMED,  pgt.Fracio,'
      'CONCAT(LPAD(gg1.CodUsu, 8, "0"), " - ", gg1.Nome) NOME_EX '
      'FROM gragru1 gg1'
      'LEFT JOIN gratamcad gtc ON gtc.Codigo=gg1.GraTamCad'
      'LEFT JOIN unidmed   unm ON unm.Codigo=gg1.UnidMed'
      'LEFT JOIN prdgruptip pgt ON pgt.Codigo=gg1.PrdGrupTip'
      'WHERE pgt.TipPrd=1'
      ''
      'AND gg1.Nivel1 NOT IN ('
      '  SELECT DISTINCT gg1.Nivel1'
      '  FROM pedivdaits pvi'
      '  LEFT JOIN gragrux   ggx ON ggx.Controle=pvi.GraGruX'
      '  LEFT JOIN gragru1   gg1 ON gg1.Nivel1=ggx.GraGru1'
      '  WHERE pvi.Codigo=:P0'
      ')'
      ''
      'ORDER BY gg1.Nome')
    Left = 8
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrGraGru1Codusu: TIntegerField
      FieldName = 'Codusu'
      Required = True
    end
    object QrGraGru1Nivel3: TIntegerField
      FieldName = 'Nivel3'
      Required = True
    end
    object QrGraGru1Nivel2: TIntegerField
      FieldName = 'Nivel2'
      Required = True
    end
    object QrGraGru1Nivel1: TIntegerField
      FieldName = 'Nivel1'
      Required = True
    end
    object QrGraGru1Nome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 30
    end
    object QrGraGru1PrdGrupTip: TIntegerField
      FieldName = 'PrdGrupTip'
      Required = True
    end
    object QrGraGru1GraTamCad: TIntegerField
      FieldName = 'GraTamCad'
      Required = True
    end
    object QrGraGru1NOMEGRATAMCAD: TWideStringField
      FieldName = 'NOMEGRATAMCAD'
      Size = 50
    end
    object QrGraGru1CODUSUGRATAMCAD: TIntegerField
      FieldName = 'CODUSUGRATAMCAD'
      Required = True
    end
    object QrGraGru1CST_A: TSmallintField
      FieldName = 'CST_A'
      Required = True
    end
    object QrGraGru1CST_B: TSmallintField
      FieldName = 'CST_B'
      Required = True
    end
    object QrGraGru1UnidMed: TIntegerField
      FieldName = 'UnidMed'
      Required = True
    end
    object QrGraGru1NCM: TWideStringField
      FieldName = 'NCM'
      Required = True
      Size = 10
    end
    object QrGraGru1Peso: TFloatField
      FieldName = 'Peso'
      Required = True
    end
    object QrGraGru1SIGLAUNIDMED: TWideStringField
      FieldName = 'SIGLAUNIDMED'
      Size = 3
    end
    object QrGraGru1CODUSUUNIDMED: TIntegerField
      FieldName = 'CODUSUUNIDMED'
      Required = True
    end
    object QrGraGru1NOMEUNIDMED: TWideStringField
      FieldName = 'NOMEUNIDMED'
      Size = 30
    end
    object QrGraGru1NOME_EX: TWideStringField
      FieldName = 'NOME_EX'
      Required = True
      Size = 44
    end
    object QrGraGru1Fracio: TSmallintField
      FieldName = 'Fracio'
    end
  end
  object DsGraGru1: TDataSource
    DataSet = QrGraGru1
    Left = 36
    Top = 8
  end
  object QrLoc: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT MAX(Sequencia) Ultimo'
      'FROM etqgeraits'
      'WHERE GraGruX = :P0'
      '')
    Left = 64
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrLocUltimo: TIntegerField
      FieldName = 'Ultimo'
      Required = True
    end
  end
  object QrLista: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT GraCusPrc'
      'FROM fisregmvt'
      'WHERE TipoMov=1'
      'AND Empresa=:P0'
      'AND Codigo=:P1')
    Left = 96
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrListaGraCusPrc: TIntegerField
      FieldName = 'GraCusPrc'
    end
  end
  object PMMenu: TPopupMenu
    Left = 504
    Top = 152
    object Produtos1: TMenuItem
      Caption = '&Produtos'
      OnClick = Produtos1Click
    end
    object abelasdepreos1: TMenuItem
      Caption = '&Tabelas de pre'#231'os'
      OnClick = abelasdepreos1Click
    end
    object Regrasfiscais1: TMenuItem
      Caption = '&Regras fiscais'
      OnClick = Regrasfiscais1Click
    end
  end
end
