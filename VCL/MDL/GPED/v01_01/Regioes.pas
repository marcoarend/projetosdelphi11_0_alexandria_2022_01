unit Regioes;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls, Db, (*DBTables,*) JPEG, ExtDlgs,
  ZCF2, ResIntStrings, unDmkProcFunc, DmkDAC_PF, UnGOTOy,
  UnInternalConsts, UnMsgInt, UnInternalConsts2, UMySQLModule, mySQLDbTables,
  UnMySQLCuringa, dmkGeral, dmkPermissoes, dmkEdit, dmkLabel, dmkRadioGroup,
  dmkDBLookupComboBox, dmkEditCB, dmkValUsu, Menus, Grids, DBGrids, Variants,
  DCPcrypt2, DCPblockciphers, DCPtwofish, dmkImage, UnDmkEnums;

type
  TFmRegioes = class(TForm)
    PainelDados: TPanel;
    DsRegioes: TDataSource;
    QrRegioes: TmySQLQuery;
    PainelEdita: TPanel;
    PainelEdit: TPanel;
    EdCodigo: TdmkEdit;
    EdNome: TdmkEdit;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    EdCodUsu: TdmkEdit;
    QrRegioesCodigo: TIntegerField;
    QrRegioesCodUsu: TIntegerField;
    QrRegioesNome: TWideStringField;
    EdGeosite: TdmkEditCB;
    CBGeosite: TdmkDBLookupComboBox;
    Label4: TLabel;
    QrGeosite: TmySQLQuery;
    DsGeosite: TDataSource;
    QrGeositeCodigo: TIntegerField;
    QrGeositeCodUsu: TIntegerField;
    QrGeositeNome: TWideStringField;
    dmkValUsu1: TdmkValUsu;
    QrRegioesGeosite: TIntegerField;
    QrRegioesNOMEGEOSITE: TWideStringField;
    SpeedButton5: TSpeedButton;
    PMRegiao: TPopupMenu;
    Incluinovaregio1: TMenuItem;
    Alteraregioatual1: TMenuItem;
    Excluiregioatual1: TMenuItem;
    PMLocais: TPopupMenu;
    QrRegioesIts: TmySQLQuery;
    DsRegioesIts: TDataSource;
    Adicionanovolocal1: TMenuItem;
    Excluilocal1: TMenuItem;
    PnLocais: TPanel;
    Panel4: TPanel;
    Label6: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    DBEdit01: TDBEdit;
    DBEdit02: TDBEdit;
    DBEdit4: TDBEdit;
    DBEdit5: TDBEdit;
    DBEdit6: TDBEdit;
    DBGrid2: TDBGrid;
    Panel8: TPanel;
    GB_DTB: TGroupBox;
    QrDTB_UFs: TmySQLQuery;
    DsDTB_UFs: TDataSource;
    QrDTB_UFsCodigo: TSmallintField;
    QrDTB_UFsSigla: TWideStringField;
    QrDTB_Meso: TmySQLQuery;
    DsDTB_Meso: TDataSource;
    QrDTB_MesoCodigo: TIntegerField;
    QrDTB_MesoNome: TWideStringField;
    QrDTB_Micro: TmySQLQuery;
    DsDTB_Micro: TDataSource;
    QrDTB_MicroCodigo: TIntegerField;
    QrDTB_MicroNome: TWideStringField;
    QrDTB_Munici: TmySQLQuery;
    DsDTB_Munici: TDataSource;
    QrDTB_MuniciCodigo: TIntegerField;
    QrDTB_MuniciNome: TWideStringField;
    QrDTB_Distri: TmySQLQuery;
    DsDTB_Distri: TDataSource;
    QrDTB_DistriCodigo: TIntegerField;
    QrDTB_DistriNome: TWideStringField;
    QrDTB_SubDis: TmySQLQuery;
    IntegerField1: TIntegerField;
    StringField1: TWideStringField;
    DsDTB_SubDis: TDataSource;
    QrDTB_DistriDTB_Munici: TIntegerField;
    GB_DNE: TGroupBox;
    QrDNE_Locali: TQuery;
    QrDNE_LocaliLOC_NU_SEQUENCIAL: TIntegerField;
    QrDNE_LocaliLOC_NOSUB: TWideStringField;
    QrDNE_LocaliLOC_NO: TWideStringField;
    QrDNE_LocaliCEP: TWideStringField;
    QrDNE_LocaliUFE_SG: TWideStringField;
    QrDNE_LocaliLOC_IN_SITUACAO: TIntegerField;
    QrDNE_LocaliLOC_IN_TIPO_LOCALIDADE: TWideStringField;
    QrDNE_LocaliLOC_NU_SEQUENCIAL_SUB: TIntegerField;
    QrDNE_LocaliCEP_DECRYPT: TWideStringField;
    QrDNE_LocaliLOC_KEY_DNE: TWideStringField;
    DsCidades: TDataSource;
    DNCEP: TDatabase;
    DCP_twofish1: TDCP_twofish;
    QrDNE_Bairros: TQuery;
    DsDNE_Bairros: TDataSource;
    QrDNE_BairrosBAI_NU_SEQUENCIAL: TIntegerField;
    QrDNE_BairrosUFE_SG: TWideStringField;
    QrDNE_BairrosLOC_NU_SEQUENCIAL: TIntegerField;
    QrDNE_BairrosBAI_NO: TWideStringField;
    QrDNE_BairrosBAI_NO_ABREV: TWideStringField;
    DsDNE_Lograd: TDataSource;
    QrDNE_Lograd: TQuery;
    QrDNE_LogradLOG_NU_SEQUENCIAL: TIntegerField;
    QrDNE_LogradUFE_SG: TWideStringField;
    QrDNE_LogradLOC_NU_SEQUENCIAL: TIntegerField;
    QrDNE_LogradLOG_NO: TWideStringField;
    QrDNE_LogradLOG_NOME: TWideStringField;
    QrDNE_LogradBAI_NU_SEQUENCIAL_INI: TIntegerField;
    QrDNE_LogradBAI_NU_SEQUENCIAL_FIM: TIntegerField;
    QrDNE_LogradCEP: TWideStringField;
    QrDNE_LogradLOG_COMPLEMENTO: TWideStringField;
    QrDNE_LogradLOG_TIPO_LOGRADOURO: TWideStringField;
    QrDNE_LogradLOG_STATUS_TIPO_LOG: TWideStringField;
    QrDNE_LogradLOG_NO_SEM_ACENTO: TWideStringField;
    QrDNE_LogradLOG_KEY_DNE: TWideStringField;
    Panel9: TPanel;
    QrDTB_Paises: TmySQLQuery;
    DsDTB_Paises: TDataSource;
    GroupBox3: TGroupBox;
    PnDTB_Paises: TPanel;
    CBDTB_Pais: TdmkDBLookupComboBox;
    EdPesqDTB_Pais: TEdit;
    dmkLabel11: TdmkLabel;
    Panel10: TPanel;
    PnDTB_UFs: TPanel;
    dmkLabel1: TdmkLabel;
    EdPesqDTB_UF: TEdit;
    EdDTB_UF: TdmkEditCB;
    CBDTB_UF: TdmkDBLookupComboBox;
    Panel12: TPanel;
    PnDTB_Meso: TPanel;
    dmkLabel2: TdmkLabel;
    EdPesqDTB_Meso: TEdit;
    EdDTB_Meso: TdmkEditCB;
    CBDTB_Meso: TdmkDBLookupComboBox;
    Panel14: TPanel;
    PnDTB_Micro: TPanel;
    dmkLabel3: TdmkLabel;
    EdPesqDTB_Micro: TEdit;
    EdDTB_Micro: TdmkEditCB;
    CBDTB_Micro: TdmkDBLookupComboBox;
    Panel16: TPanel;
    PnDTB_Munici: TPanel;
    dmkLabel4: TdmkLabel;
    EdPesqDTB_Munici: TEdit;
    EdDTB_Munici: TdmkEditCB;
    CBDTB_Munici: TdmkDBLookupComboBox;
    QrDTB_PaisesNome: TWideStringField;
    QrDTB_PaisesLocal: TWideStringField;
    QrDTB_PaisesPopulacao: TFloatField;
    QrDTB_UFsNome: TWideStringField;
    QrDTB_MesoSigla: TWideStringField;
    QrDTB_MesoDTB_UF: TSmallintField;
    QrDTB_MicroDTB_UF: TSmallintField;
    QrDTB_MicroDTB_Meso: TIntegerField;
    RGNivel: TdmkRadioGroup;
    QrDTB_MuniciDTB_UF: TSmallintField;
    QrDTB_MuniciDTB_Meso: TIntegerField;
    QrDTB_MuniciDTB_Micro: TIntegerField;
    QrDTB_MuniciDNE: TIntegerField;
    Panel13: TPanel;
    Panel11: TPanel;
    PnDNE_Locali: TPanel;
    dmkLabel8: TdmkLabel;
    EdDNE_Locali: TdmkEditCB;
    CBDNE_Locali: TdmkDBLookupComboBox;
    EdDNE_Filtro_Local: TEdit;
    Panel15: TPanel;
    PnDNE_Bairro: TPanel;
    dmkLabel9: TdmkLabel;
    EdDNE_Bairro: TdmkEditCB;
    CBDNE_Bairro: TdmkDBLookupComboBox;
    EdDNE_Filtro_Bairro: TEdit;
    Panel17: TPanel;
    Panel18: TPanel;
    PnDNE_Lograd: TPanel;
    dmkLabel10: TdmkLabel;
    EdDNE_Filtro_Lograd: TEdit;
    CBDNE_Lograd: TdmkDBLookupComboBox;
    EdDNE_Lograd: TdmkEditCB;
    Panel19: TPanel;
    PnDNE_CEP: TPanel;
    BtCEP: TBitBtn;
    PMCEP: TPopupMenu;
    Descobrir1: TMenuItem;
    Verificar1: TMenuItem;
    LaCEP: TLabel;
    EdDNE_CEP: TEdit;
    QrRegioesItsControle: TIntegerField;
    QrRegioesItsNivel: TSmallintField;
    QrRegioesItsNIVEL_TXT: TWideStringField;
    QrRegioesItsDTB_Pais: TWideStringField;
    QrRegioesItsUF: TWideStringField;
    QrRegioesItsNOMEMESO: TWideStringField;
    QrRegioesItsNOMEMICRO: TWideStringField;
    QrRegioesItsNOMEMUNICI: TWideStringField;
    QrRegioesItsDTB_UF: TSmallintField;
    QrRegioesItsDTB_Meso: TIntegerField;
    QrRegioesItsDTB_Micro: TIntegerField;
    QrRegioesItsDTB_Munici: TIntegerField;
    QrRegioesItsDNE_Locali: TIntegerField;
    QrRegioesItsDNE_Bairro: TIntegerField;
    QrRegioesItsDNE_Lograd: TIntegerField;
    QrRegioesItsDNE_CEP: TIntegerField;
    QrRegioesItsDNE_CEP_TXT: TWideStringField;
    QrRegioesItsDNE_NO_Loc: TWideStringField;
    QrRegioesItsDNE_NO_Bai: TWideStringField;
    QrRegioesItsDNE_NO_Log: TWideStringField;
    DBGrid1: TDBGrid;
    QrSub: TmySQLQuery;
    QrSubCodUsu: TIntegerField;
    QrSubNOMEREG: TWideStringField;
    QrSubControle: TIntegerField;
    QrSubNivel: TSmallintField;
    QrSubNIVEL_TXT: TWideStringField;
    QrSubDTB_Pais: TWideStringField;
    QrSubUF: TWideStringField;
    QrSubNOMEMESO: TWideStringField;
    QrSubNOMEMICRO: TWideStringField;
    QrSubNOMEMUNICI: TWideStringField;
    QrSubDTB_UF: TSmallintField;
    QrSubDTB_Meso: TIntegerField;
    QrSubDTB_Micro: TIntegerField;
    QrSubDTB_Munici: TIntegerField;
    QrSubDNE_Locali: TIntegerField;
    QrSubDNE_Bairro: TIntegerField;
    QrSubDNE_Lograd: TIntegerField;
    QrSubDNE_CEP: TIntegerField;
    QrSubDNE_NO_Loc: TWideStringField;
    QrSubDNE_NO_Bai: TWideStringField;
    QrSubDNE_NO_Log: TWideStringField;
    DsSub: TDataSource;
    QrSubDNE_CEP_TXT: TWideStringField;
    DBEdit7: TDBEdit;
    DBEdit8: TDBEdit;
    Label13: TLabel;
    PanelData: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label5: TLabel;
    Label14: TLabel;
    DBEdit03: TDBEdit;
    DBEdit04: TDBEdit;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    DBEdit9: TDBEdit;
    DBEdit10: TDBEdit;
    QrRegioesNOMEENT: TWideStringField;
    CBEntidade: TdmkDBLookupComboBox;
    SpeedButton6: TSpeedButton;
    EdEntidade: TdmkEditCB;
    Label15: TLabel;
    QrAgentes: TmySQLQuery;
    DsAgentes: TDataSource;
    QrAgentesCodigo: TIntegerField;
    QrAgentesNOMEENT: TWideStringField;
    QrRegioesEntidade: TIntegerField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel20: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel21: TPanel;
    PnSaiDesis: TPanel;
    BtConfirma: TBitBtn;
    BtDesiste: TBitBtn;
    GroupBox1: TGroupBox;
    Panel1: TPanel;
    Panel22: TPanel;
    BitBtn1: TBitBtn;
    dmkLabel5: TdmkLabel;
    EdDTB_Distri: TdmkEditCB;
    CBDTB_Distri: TdmkDBLookupComboBox;
    dmkLabel6: TdmkLabel;
    EdDTB_SubDis: TdmkEditCB;
    CBDTB_SubDis: TdmkDBLookupComboBox;
    BitBtn2: TBitBtn;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    BtRegiao: TBitBtn;
    BtLocais: TBitBtn;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrRegioesAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrRegioesBeforeOpen(DataSet: TDataSet);
    procedure BtRegiaoClick(Sender: TObject);
    procedure EdCodUsuKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SbNovoClick(Sender: TObject);
    procedure SpeedButton5Click(Sender: TObject);
    procedure Incluinovaregio1Click(Sender: TObject);
    procedure Alteraregioatual1Click(Sender: TObject);
    procedure QrRegioesBeforeClose(DataSet: TDataSet);
    procedure QrRegioesAfterScroll(DataSet: TDataSet);
    procedure EdDTB_UFChange(Sender: TObject);
    procedure EdDTB_MesoChange(Sender: TObject);
    procedure EdDTB_MicroChange(Sender: TObject);
    procedure EdDTB_MuniciChange(Sender: TObject);
    procedure EdDTB_DistriChange(Sender: TObject);
    procedure RGNivelClick(Sender: TObject);
    procedure EdDNE_Filtro_LocalExit(Sender: TObject);
    procedure EdDNE_LocaliChange(Sender: TObject);
    procedure EdDNE_Filtro_BairroChange(Sender: TObject);
    procedure EdDNE_BairroChange(Sender: TObject);
    procedure EdDNE_Filtro_LogradExit(Sender: TObject);
    procedure EdPesqDTB_UFExit(Sender: TObject);
    procedure EdPesqDTB_PaisExit(Sender: TObject);
    procedure CBDTB_PaisClick(Sender: TObject);
    procedure EdPesqDTB_MesoExit(Sender: TObject);
    procedure EdPesqDTB_MicroExit(Sender: TObject);
    procedure EdPesqDTB_MuniciExit(Sender: TObject);
    procedure BtLocaisClick(Sender: TObject);
    procedure Adicionanovolocal1Click(Sender: TObject);
    procedure EdDNE_CEPExit(Sender: TObject);
    procedure BtCEPClick(Sender: TObject);
    procedure Descobrir1Click(Sender: TObject);
    procedure Verificar1Click(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure Excluilocal1Click(Sender: TObject);
    procedure QrRegioesItsCalcFields(DataSet: TDataSet);
    procedure QrSubCalcFields(DataSet: TDataSet);
    procedure SpeedButton6Click(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
   ////Procedures do form
    procedure MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo: Integer);
    procedure DefParams;
    procedure LocCod(Atual, Codigo: Integer);
    procedure Va(Para: TVaiPara);
    procedure ConfiguraPorCEP(UF, Locali, Bairro, Lograd: Integer; CEP: String);
    procedure ReopenAgentes({Atual: Integer});

    function JaTemSubItens: Boolean;
  public
    { Public declarations }
    procedure ReopenRegioesIts(Controle: Integer);
    procedure ReopenDTB_Paises();
    procedure ReopenDTB_UFs(UF: Integer);
    procedure ReopenDTB_Meso(Meso: Integer);
    procedure ReopenDTB_Micro(Micro: Integer);
    procedure ReopenDTB_Munici(Munici: Integer);
    //procedure ReopenDTB_Distri();
    //procedure ReopenDTB_SubDis();
    //
    procedure VerifiDTBs();
    //
    procedure ReopenDNE_Locali();
    procedure ReopenDNE_Bairros();
    procedure ReopenDNE_Lograd();
  end;

var
  FmRegioes: TFmRegioes;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, Geosite, MyDBCheck, EntiCEP, RegioesSub, Entidades,
  ModuleGeral;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmRegioes.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmRegioes.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrRegioesCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmRegioes.DefParams;
begin
  VAR_GOTOTABELA := 'Regioes';
  VAR_GOTOMYSQLTABLE := QrRegioes;
  VAR_GOTONEG := gotoPiZ;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;
  VAR_SQLx.Add('SELECT reg.Codigo, reg.CodUsu, reg.Nome, ');
  VAR_SQLx.Add('reg.Geosite, geo.Nome NOMEGEOSITE, reg.Entidade, ');
  VAR_SQLx.Add('IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENT ');
  VAR_SQLx.Add('FROM regioes reg');
  VAR_SQLx.Add('LEFT JOIN geosite   geo ON geo.Codigo=reg.Geosite');
  VAR_SQLx.Add('LEFT JOIN entidades ent ON ent.Codigo=reg.Entidade');
  VAR_SQLx.Add('');
  VAR_SQLx.Add('WHERE reg.Codigo > 0');
  //
  VAR_SQL1.Add('AND reg.Codigo=:P0');
  //
  VAR_SQL2.Add('AND reg.CodUsu=:P0');
  //
  VAR_SQLa.Add('AND reg.Nome Like :P0');
  //
end;

procedure TFmRegioes.Descobrir1Click(Sender: TObject);
var
  Confirmou: Boolean;
  CEP: String;
  UF, Locali, Bairro, Lograd: Integer;
begin
  if DBCheck.CriaFm(TFmEntiCEP, FmEntiCEP, afmoNegarComAviso) then
  begin
    FmEntiCEP.DBGrid2.Visible := False;
    FmEntiCEP.Painel2.Visible := False;
    FmEntiCEP.ShowModal;
    Confirmou := FmEntiCEP.FConfirmou;
    CEP       := FmEntiCEP.FCEP;
    UF        := FmEntiCEP.FUF_Cod;
    Locali    := FmEntiCEP.FLocalCod;
    Bairro    := FmEntiCEP.FBairICod;
    Lograd    := FmEntiCEP.FLogrCod;
    FmEntiCEP.Destroy;
    //
    if Confirmou then ConfiguraPorCEP(UF, Locali, Bairro, Lograd, CEP);
  end;
end;

procedure TFmRegioes.EdCodUsuKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key = VK_F4) and (ImgTipo.SQLType = stIns) then
    UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, 'Regioes', 'CodUsu', [], [],
    stIns, 0, siPositivo, EdCodUsu);
end;

procedure TFmRegioes.EdDNE_BairroChange(Sender: TObject);
begin
  ReopenDNE_Lograd();
  //
  EdDNE_Lograd.ValueVariant := Null;
  CBDNE_Lograd.KeyValue     := Null;
end;

procedure TFmRegioes.EdDNE_CEPExit(Sender: TObject);
var
  CEP: String;
begin
  CEP := Geral.SoNumero_TT(EdDNE_CEP.Text);
  //
  EdDNE_CEP.Text := Geral.FormataCEP_TT(CEP);
end;

procedure TFmRegioes.EdDNE_LocaliChange(Sender: TObject);
begin
  ReopenDNE_Bairros();
  //
  EdDNE_Bairro.ValueVariant := Null;
  CBDNE_Bairro.KeyValue     := Null;
end;

procedure TFmRegioes.EdDNE_Filtro_BairroChange(Sender: TObject);
begin
  ReopenDNE_Bairros();
end;

procedure TFmRegioes.EdDNE_Filtro_LocalExit(Sender: TObject);
begin
  ReopenDNE_Locali();
end;

procedure TFmRegioes.EdDNE_Filtro_LogradExit(Sender: TObject);
begin
  ReopenDNE_Lograd();
end;

procedure TFmRegioes.EdDTB_DistriChange(Sender: TObject);
begin
  //ReopenDTB_SubDis();
end;

procedure TFmRegioes.EdDTB_MesoChange(Sender: TObject);
begin
  VerifiDTBs();
  //
  ReopenDTB_Micro(0);
  ReopenDTB_Munici(0);
  //ReopenDTB_Distri();
  //ReopenDTB_SubDis();
  //
  if EdDTB_Meso.Focused or CBDTB_Meso.Focused then
  begin
    EdDTB_Micro.ValueVariant := 0;
    CBDTB_Micro.KeyValue     := 0;
    //
    EdDTB_Munici.ValueVariant := 0;
    CBDTB_MUnici.KeyValue     := 0;
    //
  end;
end;

procedure TFmRegioes.EdDTB_MicroChange(Sender: TObject);
begin
  VerifiDTBs();
  //
  ReopenDTB_Munici(0);
  //ReopenDTB_Distri();
  //ReopenDTB_SubDis();
  //
  if EdDTB_Micro.Focused or CBDTB_Micro.Focused then
  begin
    EdDTB_Munici.ValueVariant := 0;
    CBDTB_MUnici.KeyValue     := 0;
    //
  end;
end;

procedure TFmRegioes.EdDTB_MuniciChange(Sender: TObject);
begin
  VerifiDTBs();
  //
  //ReopenDTB_Distri();
  //ReopenDTB_SubDis();
  //
  ReopenDNE_Locali();
  //
  EdDNE_Locali.ValueVariant := Null;
  CBDNE_Locali.KeyValue     := Null;
end;

procedure TFmRegioes.EdDTB_UFChange(Sender: TObject);
begin
  ReopenDTB_Meso(0);
  ReopenDTB_Micro(0);
  ReopenDTB_Munici(0);
  //ReopenDTB_Distri();
  //ReopenDTB_SubDis();
  if EdDTB_UF.Focused or CBDTB_UF.Focused then
  begin
    EdDTB_Meso.ValueVariant := 0;
    CBDTB_Meso.KeyValue     := Null;
    //
    EdDTB_Micro.ValueVariant := 0;
    CBDTB_Micro.KeyValue     := Null;
    //
    EdDTB_Munici.ValueVariant := 0;
    CBDTB_MUnici.KeyValue     := Null;
    //
    EdDNE_CEP.Text := '';
  end;
end;

procedure TFmRegioes.EdPesqDTB_MesoExit(Sender: TObject);
begin
  ReopenDTB_Meso(0);
end;

procedure TFmRegioes.EdPesqDTB_MicroExit(Sender: TObject);
begin
  ReopenDTB_Micro(0);
end;

procedure TFmRegioes.EdPesqDTB_MuniciExit(Sender: TObject);
begin
  ReopenDTB_Munici(0);
end;

procedure TFmRegioes.EdPesqDTB_PaisExit(Sender: TObject);
begin
  ReopenDTB_Paises();
end;

procedure TFmRegioes.EdPesqDTB_UFExit(Sender: TObject);
begin
  ReopenDTB_UFs(0);
end;

procedure TFmRegioes.Excluilocal1Click(Sender: TObject);
begin
  DBCheck.QuaisItens_Exclui(Dmod.QrUpd, QrRegioesIts, DBGrid1, 'regioesits',
  ['Controle'], ['Controle'], istPergunta, '');
end;

procedure TFmRegioes.MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo: Integer);
begin
  case Mostra of
    0:
    begin
      PainelDados.Visible    := True;
      PainelEdita.Visible    := False;
      PnLocais.Visible       := False;
    end;
    {
    1:
    begin
      PainelEdita.Visible := True;
      PainelDados.Visible := False;
      PainelControle.Visible:=False;
      if SQLType = stIns then
      begin
        EdCodigo.Text := FormatFloat(FFormatFloat, Codigo);
        EdNome.Text := '';
        //...
      end else begin
        EdCodigo.Text := DBEdCodigo.Text;
        EdNome.Text := DBEdNome.Text;
        //...
      end;
      EdNome.SetFocus;
    end;
    }
    2:
    begin
      PnLocais.Visible       := True;
      PainelDados.Visible    := False;
      //

    end
    else Geral.MB_Aviso('A��o de Inclus�o/altera��o n�o definida!');
  end;
  ImgTipo.SQLType := SQLType;
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmRegioes.CBDTB_PaisClick(Sender: TObject);
begin
  if Uppercase(CBDTB_Pais.Text) = 'BRASIL' then
  begin
    GB_DNE.Visible := True;
    GB_DTB.Visible := True;
  end else begin
    GB_DNE.Visible := False;
    GB_DTB.Visible := False;
  end;
end;

procedure TFmRegioes.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

{
procedure TFmRegioes.AlteraRegistro;
var
  Regioes : Integer;
begin
  Regioes := QrRegioesCodigo.Value;
  if QrRegioesCodigo.Value = 0 then
  begin
    BtAltera.Enabled := False;
    Exit;
  end;
  if not UMyMod.SelLockY(Regioes, Dmod.MyDB, 'Regioes', 'Codigo') then
  begin
    try
      UMyMod.UpdLockY(Regioes, Dmod.MyDB, 'Regioes', 'Codigo');
      MostraEdicao(1, CO_ALTERACAO, 0);
    finally
      Screen.Cursor := Cursor;
    end;
  end;
end;

procedure TFmRegioes.IncluiRegistro;
var
  Cursor : TCursor;
  Regioes : Integer;
begin
  Cursor        := Screen.Cursor;
  Screen.Cursor := crHourglass;
  Refresh;
  try
    Regioes := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
                 'Regioes', 'Regioes', 'Codigo');
    //
    if Length(FormatFloat(FFormatFloat, Regioes))>Length(FFormatFloat) then
    begin
      Geral.MB_Erro('Inclus�o cancelada. Limite de cadastros extrapolado');
    end;
    MostraEdicao(1, stIns, Regioes);
  finally
    Screen.Cursor := Cursor;
  end;
end;
}

procedure TFmRegioes.QueryPrincipalAfterOpen;
begin
end;

procedure TFmRegioes.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmRegioes.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmRegioes.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmRegioes.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmRegioes.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmRegioes.SpeedButton5Click(Sender: TObject);
var
  Geosite: Integer;
begin
  VAR_CADASTRO := 0;
  Geosite      := EdGeosite.ValueVariant;
  //
  if DBCheck.CriaFm(TFmGeosite, FmGeosite, afmoNegarComAviso) then
  begin
    if Geosite <> 0 then
      FmGeosite.LocCod(Geosite, Geosite);
    FmGeosite.ShowModal;
    FmGeosite.Destroy;
    //
    if VAR_CADASTRO > 0 then
    begin
      UMyMod.SetaCodigoPesquisado(EdGeosite, CBGeosite, QrGeosite, VAR_CADASTRO);
      EdGeosite.SetFocus;
    end;
  end;
end;

procedure TFmRegioes.SpeedButton6Click(Sender: TObject);
var
  Entidade: Integer;
begin
  VAR_CADASTRO := 0;
  Entidade     := EdEntidade.ValueVariant;
  //
  DModG.CadastroDeEntidade(Entidade, fmcadEntidade2, fmcadEntidade2, True);
  //
  if VAR_CADASTRO > 0 then
  begin
    UMyMod.SetaCodigoPesquisado(EdEntidade, CBEntidade, QrAgentes, VAR_CADASTRO);
    EdEntidade.SetFocus;
  end;
end;

procedure TFmRegioes.BitBtn1Click(Sender: TObject);
  function AvisaESai(Sai: Boolean; Msg: String): Boolean;
  begin
    if Sai then
    begin
      Geral.MB_Aviso(Msg);
      Screen.Cursor := crDefault;
      Result := True;
    end else
      Result := False;
  end;
  function ErrVeriInt(var Val: Integer; const Compo: TControl; Panel: TPanel; 
  const Msg: String): Boolean;
  var
    Visivel: Boolean;
  begin
    Val := 0;
    if Panel <> nil then Visivel := Panel.Visible else Visivel := Compo.Visible;
    if Visivel then
    begin
      if Compo is TdmkEditCB then Val := Geral.IMV(TdmkEditCB(Compo).Text) else
      if Compo is TEdit then Val := Geral.IMV(TEdit(Compo).Text) else
      if Compo is TdmkRadioGroup then Val := TdmkRadioGroup(Compo).ItemIndex
      else ShowMessage('O componente ' + Compo.Name + ' n�o foi implementado ' +
      'na funtion "VeriInt"!');
      Result := AvisaESai(Val = 0, Msg);
    end else Result := False;
  end;
var
  DTB_Pais, DNE_NO_Loc, DNE_NO_Bai, DNE_NO_Log: String;
  Controle, Codigo, Nivel,
  DTB_UF, DTB_Meso, DTB_Micro, DTB_Munici, 
  DNE_Locali, DNE_Bairro, DNE_Lograd, DNE_CEP: Integer;
begin
  Screen.Cursor := crHourGlass;
  if ErrVeriInt(Nivel, RGNivel, nil, 'Informe o n�vel de abrang�ncia!') then Exit;
  DTB_Pais   := CBDTB_Pais.Text;
  if AvisaESai(Trim(DTB_Pais) = '' , 'Infrome o pa�s!') then Exit;
  {
  DTB_UF     := 0;
  DTB_Meso   := 0;
  DTB_Micro  := 0;
  DTB_Munici := 0;
  DNE_Locali := 0;
  DNE_Lograd := 0;
  DNE_CEP    := 0;
  }
  //
  if ErrVeriInt(DTB_UF, EdDTB_UF, PnDTB_UFs, 'Informe a UF!') then Exit;
  if ErrVeriInt(DTB_Meso, EdDTB_Meso, PnDTB_Meso, 'Informe a mesorregi�o!') then Exit;
  if ErrVeriInt(DTB_Micro, EdDTB_Micro, PnDTB_Micro, 'Informe a microrregi�o!') then Exit;
  if ErrVeriInt(DTB_Munici, EdDTB_Munici, PnDTB_Munici, 'Informe a munic�pio!') then Exit;
  //
  if ErrVeriInt(DNE_Locali, EdDNE_Locali, PnDNE_Locali, 'Informe a localidade!') then Exit;
  if ErrVeriInt(DNE_Bairro, EdDNE_Bairro, PnDNE_Bairro, 'Informe o bairro!') then Exit;
  if ErrVeriInt(DNE_Lograd, EdDNE_Lograd, PnDNE_Lograd, 'Informe o logradouro!') then Exit;
  if ErrVeriInt(DNE_CEP, EdDNE_CEP, PnDNE_CEP, 'Informe o CEP!') then Exit;
  //
  if JaTemSubItens then
  begin
    Screen.Cursor := crDefault;
    Exit;
  end;
  //
  if DNE_Locali = 0 then DNE_NO_Loc := '' else
    DNE_NO_Loc := CBDNE_Locali.Text;
  if DNE_Bairro = 0 then DNE_NO_Bai := '' else
    DNE_NO_Bai := CBDNE_Bairro.Text;
  if DNE_Lograd = 0 then DNE_NO_Log := '' else
    DNE_NO_Log := CBDNE_Lograd.Text;
  //
  Codigo := QrRegioesCodigo.Value;
  Controle := UMyMod.BuscaEmLivreY_Def('regioesits', 'Controle', ImgTipo.SQLType,
    QrRegioesItsControle.Value);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'regioesits', False, [
  'Codigo', 'Nivel', 'DTB_Pais',
  'DTB_UF', 'DTB_Meso', 'DTB_Micro',
  'DTB_Munici', 'DNE_Locali', 'DNE_Bairro',
  'DNE_Lograd', 'DNE_CEP', 'DNE_NO_Loc',
  'DNE_NO_Bai', 'DNE_NO_Log'], [
  'Controle'], [
  Codigo, Nivel, DTB_Pais,
  DTB_UF, DTB_Meso, DTB_Micro,
  DTB_Munici, DNE_Locali, DNE_Bairro,
  DNE_Lograd, DNE_CEP, DNE_NO_Loc,
  DNE_NO_Bai, DNE_NO_Log], [
  Controle], True) then
  begin
    MostraEdicao(0, stLok, Codigo);
    ReopenRegioesIts(Controle);
  end;
  Screen.Cursor := crDefault;
end;

procedure TFmRegioes.BtCEPClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMCEP, BtCEP);
end;

procedure TFmRegioes.Adicionanovolocal1Click(Sender: TObject);
begin
  MostraEdicao(2, stIns, 0);
end;

procedure TFmRegioes.Alteraregioatual1Click(Sender: TObject);
begin
  ReopenAgentes({QrRegioesEntidade.Value});
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PainelEdita, QrRegioes, [PainelDados],
  [PainelEdita], EdCodUsu, ImgTipo, 'regioes');
end;

procedure TFmRegioes.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrRegioesCodigo.Value;
  Close;
end;

procedure TFmRegioes.BtConfirmaClick(Sender: TObject);
var
  Codigo: Integer;
  Nome: String;
begin
  Nome := EdNome.Text;
  //
  if MyObjects.FIC(Length(Nome) = 0, EdNome, 'Defina uma descri��o!') then Exit;
  //
  Codigo := UMyMod.BuscaEmLivreY_Def('Regioes', 'Codigo', ImgTipo.SQLType,
              QrRegioesCodigo.Value);
  //
  if UMyMod.ExecSQLInsUpdPanel(ImgTipo.SQLType, FmRegioes, PainelEdit,
    'Regioes', Codigo, Dmod.QrUpd, [PainelEdita], [PainelDados], ImgTipo, True) then
  begin
    LocCod(Codigo, Codigo);
  end;
end;

procedure TFmRegioes.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := Geral.IMV(EdCodigo.Text);
  //
  if ImgTipo.SQLType = stIns then
    UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'Regioes', Codigo);
  //
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'Regioes', 'Codigo');
  MostraEdicao(0, stLok, 0);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'Regioes', 'Codigo');
end;

procedure TFmRegioes.BtLocaisClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMLocais, BtLocais);
end;

procedure TFmRegioes.BtRegiaoClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMRegiao, BtRegiao);
end;

procedure TFmRegioes.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  PainelEdit.Align  := alClient;
  DBGrid1.Align     := alClient;
  DBGrid2.Align     := alClient;
  CriaOForm;
  UnDmkDAC_PF.AbreQuery(QrGeosite, Dmod.MyDB);
  //
  ReopenDTB_Paises();
  ReopenDTB_UFs(0);
  ReopenDTB_Meso(0);
  ReopenDTB_Micro(0);
  ReopenDTB_Munici(0);
  //ReopenDTB_Distri();
  //ReopenDTB_SubDis();
end;

procedure TFmRegioes.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrRegioesCodigo.Value, LaRegistro.Caption);
end;

procedure TFmRegioes.SbImprimeClick(Sender: TObject);
begin
//
end;

procedure TFmRegioes.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmRegioes.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrRegioesCodUsu.Value, LaRegistro.Caption);
end;

procedure TFmRegioes.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmRegioes.QrRegioesAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
  BtLocais.Enabled := QrRegioes.RecordCount > 0;
end;

procedure TFmRegioes.QrRegioesAfterScroll(DataSet: TDataSet);
begin
  ReopenRegioesIts(0);
end;

procedure TFmRegioes.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmRegioes.SbQueryClick(Sender: TObject);
begin
  LocCod(QrRegioesCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'Regioes', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmRegioes.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmRegioes.Incluinovaregio1Click(Sender: TObject);
begin
  ReopenAgentes({0});
  UMyMod.ConfigPanelInsUpd(stIns, Self, PainelEdit, QrRegioes, [PainelDados],
  [PainelEdita], EdCodUsu, ImgTipo, 'regioes');
end;

procedure TFmRegioes.QrRegioesBeforeClose(DataSet: TDataSet);
begin
  QrRegioesIts.Close;
  BtLocais.Enabled := False;
end;

procedure TFmRegioes.QrRegioesBeforeOpen(DataSet: TDataSet);
begin
  QrRegioesCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmRegioes.QrRegioesItsCalcFields(DataSet: TDataSet);
begin
  QrRegioesItsDNE_CEP_TXT.Value :=
   Geral.FormataCEP_NT(QrRegioesItsDNE_CEP.Value);
end;

procedure TFmRegioes.QrSubCalcFields(DataSet: TDataSet);
begin
  QrSubDNE_CEP_TXT.Value :=Geral.FormataCEP_NT(QrSubDNE_CEP.Value);
end;

procedure TFmRegioes.ReopenRegioesIts(Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrRegioesIts, Dmod.MyDB, [
  'SELECT rei.Controle, rei.Nivel, ELT(rei.Nivel+1,  ',
  '"0. Nenhum", "1. CEP", "2. Logradouro", "3. Bairro",  ',
  '"4. Localidade", "5. Munic�pio", "6. Microrregi�o", ',
  '"7. Mesorregi�o", "8. Estado", "9. Pa�s") NIVEL_TXT, ',
  'rei.DTB_Pais, ufs.Sigla UF, mes.Nome NOMEMESO, ',
  'mic.Nome NOMEMICRO, mun.Nome NOMEMUNICI, ',
  'rei.DTB_UF, rei.DTB_Meso, rei.DTB_Micro,  ',
  'rei.DTB_Munici, rei.DNE_Locali, rei.DNE_Bairro, ',
  'rei.DNE_Lograd, rei.DNE_CEP, ',
  'rei.DNE_NO_Loc, rei.DNE_NO_Bai, rei.DNE_NO_Log  ',
  'FROM regioesits rei ',
  'LEFT JOIN ' + VAR_AllID_DB_NOME + '.dtb_ufs ufs ON ufs.Codigo=rei.DTB_UF ',
  'LEFT JOIN ' + VAR_AllID_DB_NOME + '.dtb_meso mes ON mes.Codigo=rei.DTB_Meso ',
  'LEFT JOIN ' + VAR_AllID_DB_NOME + '.dtb_micro mic ON mic.Codigo=rei.DTB_Micro ',
  'LEFT JOIN ' + VAR_AllID_DB_NOME + '.dtb_munici mun ON mun.Codigo=rei.DTB_Munici ',
  'WHERE rei.Codigo=' + Geral.FF0(QrRegioesCodigo.Value),
  'ORDER BY rei.Nivel DESC, rei.DTB_Pais ',
  '']);
  //
  QrRegioesIts.Locate('Controle', Controle, []);
end;

procedure TFmRegioes.RGNivelClick(Sender: TObject);
begin
  if (RGNivel.ItemIndex > 0) and (RGNivel.ItemIndex < 9) then
  begin
    if QrDTB_Paises.Locate('Nome', 'Brasil', [loCaseInsensitive]) then
      CBDTB_Pais.KeyValue := QrDTB_PaisesNome.Value;
    CBDTB_PaisClick(Self);  
  end;

  PnDTB_UFs   .Visible := RGNivel.ItemIndex in ([1..8]);
  PnDTB_Meso  .Visible := RGNivel.ItemIndex in ([1..7]);
  PnDTB_Micro .Visible := RGNivel.ItemIndex in ([1..6]);
  PnDTB_Munici.Visible := RGNivel.ItemIndex in ([1..5]);
  //
  PnDNE_Locali.Visible := RGNivel.ItemIndex in ([1..4]);
  PnDNE_Bairro.Visible := RGNivel.ItemIndex in ([1..3]);
  PnDNE_Lograd.Visible := RGNivel.ItemIndex in ([1..2]);
  PnDNE_CEP   .Visible := RGNivel.ItemIndex in ([1..1]);
  //
  ReopenDTB_UFs(QrDTB_UFsCodigo.Value);

  // Evitar duplicidades reabrindo tabela sem os j� cadastrados
  case RGNivel.ItemIndex of
    //1: EdDNE_CEP.Text := '';
    2: ReopenDNE_Lograd();
    3: ReopenDNE_Bairros();
    4: ReopenDNE_Locali();
    5: ReopenDTB_Munici(0);
    6: ReopenDTB_Micro(0);
    7: ReopenDTB_Meso(0);
    8: ReopenDTB_UFs(0);
    9: ReopenDTB_Paises();
  end;
end;

procedure TFmRegioes.ReopenDTB_Paises();
var
  Nome: String;
begin
  Nome := CBDTB_Pais.Text;
  QrDTB_Paises.Close;
  QrDTB_Paises.SQL.Clear;
  QrDTB_Paises.SQL.Add('SELECT * FROM dtb_paises');
  QrDTB_Paises.SQL.Add('WHERE Nome LIKE "%' + EdPesqDTB_Pais.Text + '%"');
  //if RGNivel.ItemIndex = 9 then
  //begin
    QrDTB_Paises.SQL.Add('AND NOT (Nome IN(');
    QrDTB_Paises.SQL.Add('  SELECT DISTINCT DTB_Pais');
    QrDTB_Paises.SQL.Add('  FROM ' + TMeuDB + '.regioesits');
    QrDTB_Paises.SQL.Add('  WHERE Nivel=9');
    QrDTB_Paises.SQL.Add('))');
  //end;
  QrDTB_Paises.SQL.Add('ORDER BY Nome');
  UnDmkDAC_PF.AbreQuery(QrDTB_Paises, DModG.AllID_DB);
  //
  if QrDTB_Paises.Locate('Nome', Nome, [loCaseInsensitive]) then
    CBDTB_Pais.KeyValue := QrDTB_PaisesNome.Value
  else if Trim(CBDTB_Pais.Text) <> '' then
  begin
    if not QrDTB_Paises.Locate('Nome', EdPesqDTB_Pais.Text, [loCaseInsensitive]) then
      CBDTB_Pais.KeyValue := Null;
  end;
end;

procedure TFmRegioes.ReopenDTB_UFs(UF: Integer);
var
  Atual: Integer;
begin
  Atual := QrDTB_UFsCodigo.Value;
  //
  QrDTB_UFs.Close;
  QrDTB_UFs.SQL.Clear;
  QrDTB_UFs.SQL.Add('SELECT Codigo, Sigla, Nome ');
  QrDTB_UFs.SQL.Add('FROM dtb_ufs');
  QrDTB_UFs.SQL.Add('WHERE Nome LIKE "%' + EdPesqDTB_UF.Text + '%"');
  //if RGNivel.ItemIndex = 8 then
  //begin
    QrDTB_UFs.SQL.Add('AND NOT (Codigo IN(');
    QrDTB_UFs.SQL.Add('  SELECT DTB_UF');
    QrDTB_UFs.SQL.Add('  FROM ' + TMeuDB + '.regioesits');
    QrDTB_UFs.SQL.Add('  WHERE Nivel=8');
    QrDTB_UFs.SQL.Add('))');
  //end;
  QrDTB_UFs.SQL.Add('ORDER BY Sigla');
  UnDmkDAC_PF.AbreQuery(QrDTB_UFs, DModG.AllID_DB);
  //
  if Atual <> 0 then
    if not QrDTB_UFs.Locate('Codigo', Atual, []) then
    begin
      CBDTB_UF.KeyValue     := Null;
      EdDTB_UF.ValueVariant := 0;
    end;
  //
  if UF > 0 then
  begin
    if QrDTB_UFs.Locate('Codigo', UF, []) then
    begin
      EdDTB_UF.ValueVariant := UF;
      CBDTB_UF.KeyValue     := UF;
    end;
  end else
  begin
    if QrDTB_UFs.Locate('Nome', EdPesqDTB_UF.Text, [loCaseInsensitive]) then
    begin
      EdDTB_UF.ValueVariant := QrDTB_UFsCodigo.Value;
      CBDTB_UF.KeyValue     := QrDTB_UFsCodigo.Value;
    end;
  end;
end;

procedure TFmRegioes.ReopenDTB_Meso(Meso: Integer);
var
  UF, Cod: Integer;
  Txt: String;
begin
  UF  := Geral.IMV(EdDTB_UF.Text);
  Cod := Geral.IMV(EdDTB_Meso.Text);
  Txt := EdPesqDTB_Meso.Text;
  //
  QrDTB_Meso.Close;
  QrDTB_Meso.SQL.Clear;
  QrDTB_Meso.SQL.Add('SELECT mes.Codigo, mes.Nome, mes.DTB_UF, ufs.Sigla');
  QrDTB_Meso.SQL.Add('FROM dtb_meso mes');
  QrDTB_Meso.SQL.Add('LEFT JOIN dtb_ufs ufs ON ufs.Codigo=mes.DTB_UF');
  QrDTB_Meso.SQL.Add('WHERE mes.Codigo > -1000');
  if UF <> 0 then
    QrDTB_Meso.SQL.Add('AND mes.DTB_UF=' + FormatFloat('0', UF));
  if Trim(Txt) <> '' then
    QrDTB_Meso.SQL.Add('AND mes.Nome LIKE "%' + Txt + '%"');
  //if RGNivel.ItemIndex = 7 then
  //begin
    QrDTB_Meso.SQL.Add('AND NOT (mes.Codigo IN(');
    QrDTB_Meso.SQL.Add('  SELECT DTB_Meso');
    QrDTB_Meso.SQL.Add('  FROM ' + TMeuDB + '.regioesits');
    QrDTB_Meso.SQL.Add('  WHERE Nivel=7');
    QrDTB_Meso.SQL.Add('))');
  //end;
  QrDTB_Meso.SQL.Add('ORDER BY mes.Nome');
  UnDmkDAC_PF.AbreQuery(QrDTB_Meso, DModG.AllID_DB);
  //
  if Meso > 0 then
  begin
    if QrDTB_Meso.Locate('Codigo', Meso, []) then
    begin
      EdDTB_Meso.ValueVariant := Meso;
      CBDTB_Meso.KeyValue     := Meso;
    end else begin
      EdDTB_Meso.ValueVariant := 0;
      CBDTB_Meso.KeyValue     := Null;
    end;
  end else begin
    if QrDTB_Meso.Locate('Codigo', Cod, []) then
    begin
      EdDTB_Meso.ValueVariant := Cod;
      CBDTB_Meso.KeyValue     := Cod;
    end else begin
      EdDTB_Meso.ValueVariant := 0;
      CBDTB_Meso.KeyValue     := Null;
    end;
  end;
end;

procedure TFmRegioes.ReopenDTB_Micro(Micro: Integer);
var
  UF, Meso, Cod: Integer;
  Txt: String;
begin
  UF   := Geral.IMV(EdDTB_UF.Text);
  Meso := Geral.IMV(EdDTB_Meso.Text);
  Cod  := Geral.IMV(EdDTB_Micro.Text);
  //
  Txt  := EdPesqDTB_Micro.Text;
  QrDTB_Micro.Close;
  QrDTB_Micro.SQL.Clear;
  QrDTB_Micro.SQL.Add('SELECT mic.Codigo, mic.Nome, mic.DTB_UF, mic.DTB_Meso');
  QrDTB_Micro.SQL.Add('FROM dtb_micro mic');
  QrDTB_Micro.SQL.Add('LEFT JOIN dtb_meso mes ON mes.Codigo=mic.DTB_Meso');
  QrDTB_Micro.SQL.Add('LEFT JOIN dtb_ufs ufs ON ufs.Codigo=mic.DTB_UF');
  QrDTB_Micro.SQL.Add('WHERE mic.Codigo > -1000');
  if Meso <> 0 then
    QrDTB_Micro.SQL.Add('AND mic.DTB_Meso=' + FormatFloat('0', Meso))
  else if UF <> 0 then
    QrDTB_Micro.SQL.Add('AND mic.DTB_UF=' + FormatFloat('0', UF));
  //
  if Trim(Txt) <> '' then
    QrDTB_Micro.SQL.Add('AND mic.NOME LIKE "%' + Txt + '%"');

  //if RGNivel.ItemIndex = 6 then
  //begin
    QrDTB_Micro.SQL.Add('AND NOT (mic.Codigo IN(');
    QrDTB_Micro.SQL.Add('  SELECT DTB_Micro');
    QrDTB_Micro.SQL.Add('  FROM ' + TMeuDB + '.regioesits');
    QrDTB_Micro.SQL.Add('  WHERE Nivel=6');
    QrDTB_Micro.SQL.Add('))');
  //end;
  QrDTB_Micro.SQL.Add('ORDER BY mic.Nome');
  UnDmkDAC_PF.AbreQuery(QrDTB_Micro, DModG.AllID_DB);
  //
  if Micro > 0 then
  begin
    if QrDTB_Micro.Locate('Codigo', Micro, []) then
    begin
      EdDTB_Micro.ValueVariant := Micro;
      CBDTB_Micro.KeyValue     := Micro;
    end else begin
      EdDTB_Micro.ValueVariant := 0;
      CBDTB_Micro.KeyValue     := Null;
    end;
  end else begin
    if QrDTB_Micro.Locate('Codigo', Cod, []) then
    begin
      EdDTB_Micro.ValueVariant := Cod;
      CBDTB_Micro.KeyValue     := Cod;
    end else begin
      EdDTB_Micro.ValueVariant := 0;
      CBDTB_Micro.KeyValue     := Null;
    end;
  end;
end;

procedure TFmRegioes.ReopenDTB_Munici(Munici: Integer);
var
  UF, Meso, Micro, Cod: Integer;
  Txt: String;
begin
  UF    := Geral.IMV(EdDTB_UF.Text);
  Meso  := Geral.IMV(EdDTB_Meso.Text);
  Micro := Geral.IMV(EdDTB_Micro.Text);
  Cod   := Geral.IMV(EdDTB_Munici.Text);
  Txt   := EdPesqDTB_Munici.Text;
  //
  QrDTB_Munici.Close;
  QrDTB_Munici.SQL.Clear;
  QrDTB_Munici.SQL.Add('SELECT Codigo, Nome, DTB_UF, DTB_Meso, DTB_Micro, DNE');
  QrDTB_Munici.SQL.Add('FROM dtb_munici');
  QrDTB_Munici.SQL.Add('WHERE Codigo > -1000');
  if Micro <> 0 then
    QrDTB_Munici.SQL.Add('AND DTB_Micro=' + FormatFloat('0', Micro))
  else if Meso <> 0 then
    QrDTB_Munici.SQL.Add('AND DTB_Meso=' + FormatFloat('0', Meso))
  else if UF <> 0 then
    QrDTB_Munici.SQL.Add('AND DTB_UF=' + FormatFloat('0', UF));
  //
  if Trim(Txt) <> '' then
    QrDTB_Munici.SQL.Add('AND Nome LIKE "%' + Txt + '%"');
  //if RGNivel.ItemIndex = 8 then
  //begin
    QrDTB_Munici.SQL.Add('AND NOT (Codigo IN(');
    QrDTB_Munici.SQL.Add('  SELECT DTB_Munici');
    QrDTB_Munici.SQL.Add('  FROM ' + TMeuDB + '.regioesits');
    QrDTB_Munici.SQL.Add('  WHERE Nivel=5');
    QrDTB_Munici.SQL.Add('))');
  //end;
  QrDTB_Munici.SQL.Add('ORDER BY Nome');
  UnDmkDAC_PF.AbreQuery(QrDTB_Munici, DModG.AllID_DB);
  //
  if Munici > 0 then
  begin
    if QrDTB_Munici.Locate('Codigo', Munici, []) then
    begin
      EdDTB_Munici.ValueVariant := Munici;
      CBDTB_Munici.KeyValue     := Munici;
    end else begin
      EdDTB_Munici.ValueVariant := 0;
      CBDTB_Munici.KeyValue     := Null;
    end;
  end else begin
    if QrDTB_Munici.Locate('Codigo', Cod, []) then
    begin
      EdDTB_Munici.ValueVariant := Cod;
      CBDTB_Munici.KeyValue     := Cod;
    end else begin
      EdDTB_Munici.ValueVariant := 0;
      CBDTB_Munici.KeyValue     := Null;
    end;
  end;
end;

{
procedure TFmRegioes.ReopenDTB_Distri();
var
  UF, Meso, Micro, Munici, Cod: Integer;
begin
  UF     := Geral.IMV(EdDTB_UF.Text);
  Meso   := Geral.IMV(EdDTB_Meso.Text);
  Micro  := Geral.IMV(EdDTB_Micro.Text);
  Munici := Geral.IMV(EdDTB_Munici.Text);
  Cod    := Geral.IMV(EdDTB_Distri.Text);
  //
  QrDTB_Distri.Close;
  QrDTB_Distri.SQL.Clear;
  QrDTB_Distri.SQL.Add('SELECT Codigo, Nome, DTB_Munici');
  QrDTB_Distri.SQL.Add('FROM dtb_distri');
  if Munici <> 0 then
    QrDTB_Distri.SQL.Add('WHERE DTB_Munici=' + FormatFloat('0', Munici))
  else if Micro <> 0 then
    QrDTB_Distri.SQL.Add('WHERE DTB_Micro=' + FormatFloat('0', Micro))
  else if Meso <> 0 then
    QrDTB_Distri.SQL.Add('WHERE DTB_Meso=' + FormatFloat('0', Meso))
  else if UF <> 0 then
    QrDTB_Distri.SQL.Add('WHERE DTB_UF=' + FormatFloat('0', UF));
  QrDTB_Distri.SQL.Add('ORDER BY Nome');
  UnDmkDAC_PF.AbreQuery(QrDTB_Distri, DModG.AllID_DB);
  //
  if QrDTB_Distri.Locate('Codigo;DTB_Munici', VarArrayOf([Cod,Munici]), []) then
  begin
    EdDTB_Distri.ValueVariant := Cod;
    CBDTB_Distri.KeyValue     := Cod;
  end else begin
    EdDTB_Distri.ValueVariant := 0;
    CBDTB_Distri.KeyValue     := Null;
  end;
end;

{
procedure TFmRegioes.ReopenDTB_SubDis();
var
  UF, Meso, Micro, Munici, Distri, Cod: Integer;
begin
  UF     := Geral.IMV(EdDTB_UF.Text);
  Meso   := Geral.IMV(EdDTB_Meso.Text);
  Micro  := Geral.IMV(EdDTB_Micro.Text);
  Munici := Geral.IMV(EdDTB_Munici.Text);
  Distri := Geral.IMV(EdDTB_Distri.Text);
  Cod    := Geral.IMV(EdDTB_SubDis.Text);
  //
  QrDTB_SubDis.Close;
  QrDTB_SubDis.SQL.Clear;
  QrDTB_SubDis.SQL.Add('SELECT Codigo, Nome');
  QrDTB_SubDis.SQL.Add('FROM dtb_subdis');
  QrDTB_SubDis.SQL.Add('WHERE DTB_Distri=' + FormatFloat('0', Distri));
  QrDTB_SubDis.SQL.Add('AND DTB_Munici=' + FormatFloat('0', Munici));
  QrDTB_SubDis.SQL.Add('ORDER BY Nome');
  UnDmkDAC_PF.AbreQuery(QrDTB_SubDis, DModG.AllID_DB);
  //
  if QrDTB_SubDis.Locate('Codigo', Cod, []) then
  begin
    EdDTB_SubDis.ValueVariant := Cod;
    CBDTB_SubDis.KeyValue     := Cod;
  end else begin
    EdDTB_SubDis.ValueVariant := 0;
    CBDTB_SubDis.KeyValue     := Null;
  end;
end;
}

procedure TFmRegioes.ReopenDNE_Locali();
var
  Munici, Cidade, UF_Cod: Integer;
  UF_Txt: String;
begin
  Munici := Geral.IMV(EdDTB_Munici.Text);
  UF_Cod := Geral.IMV(EdDTB_UF.Text);
  //
  QrDNE_Locali.Close;
  if (Munici > 0) or (UF_Cod > 0) then
  begin
    QrDNE_Locali.SQL.Clear;
    QrDNE_Locali.SQL.Add('SELECT *');
    QrDNE_Locali.SQL.Add('FROM log_localidade');
    QrDNE_Locali.SQL.Add('WHERE LOC_NU_SEQUENCIAL > -1000');
    if UF_Cod > 0 then
    begin
      UF_Txt := QrDTB_UFsSigla.Value;
      QrDNE_Locali.SQL.Add('AND UFE_SG=''' + UF_Txt + '''');
    end;
    if Munici > 0 then
    begin
      Cidade := QrDTB_MuniciDNE.Value;
      QrDNE_Locali.SQL.Add('AND (LOC_NU_SEQUENCIAL=' + FormatFloat('0', Cidade));
      QrDNE_Locali.SQL.Add('OR LOC_NU_SEQUENCIAL_SUB=' + FormatFloat('0', Cidade) + ')');
    end;
    QrDNE_Locali.SQL.Add('ORDER BY LOC_NO');
    try
      UMyMod.AbreBDEQuery(QrDNE_Locali);
    except
      ShowMessage(QrDNE_Locali.SQL.Text);
    end;
  end;
end;

procedure TFmRegioes.ReopenAgentes({Atual: Integer});
begin
  QrAgentes.Close;
  //QrAgentes.Params[0].AsInteger := Atual;
  UnDmkDAC_PF.AbreQuery(QrAgentes, Dmod.MyDB);
end;

procedure TFmRegioes.ReopenDNE_Bairros();
var
  Locali: Integer;
begin
  Locali := Geral.IMV(EdDNE_Locali.Text);
  //
  QrDNE_Bairros.Close;
  QrDNE_Bairros.Params[00].AsInteger := Locali;
  QrDNE_Bairros.Params[01].AsString  := '%' + Trim(EdDNE_Filtro_Bairro.Text) + '%';
  UMyMod.AbreBDEQuery(QrDNE_Bairros);
  //
end;

procedure TFmRegioes.ReopenDNE_Lograd();
var
  Bairro: Integer;
begin
  Bairro := Geral.IMV(EdDNE_Bairro.Text);
  //
  QrDNE_Lograd.Close;
  QrDNE_Lograd.Params[00].AsInteger := Bairro;
  QrDNE_Lograd.Params[01].AsInteger := Bairro;
  QrDNE_Lograd.Params[02].AsString  := '%' + Trim(EdDNE_Filtro_Lograd.Text) + '%';
  UMyMod.AbreBDEQuery(QrDNE_Lograd);
  //
end;

procedure TFmRegioes.Verificar1Click(Sender: TObject);
var
  Confirmou: Boolean;
  CEP: String;
  UF, Locali, Bairro, Lograd: Integer;
begin
  if DBCheck.CriaFm(TFmEntiCEP, FmEntiCEP, afmoNegarComAviso) then
  begin
    FmEntiCEP.DBGrid1.Visible := False;
    FmEntiCEP.Painel1.Visible := False;
    FmEntiCEP.EdCEPLoc.Text   := EdDNE_CEP.Text;
    FmEntiCEP.ShowModal;
    Confirmou := FmEntiCEP.FConfirmou;
    CEP       := FmEntiCEP.FCEP;
    UF        := FmEntiCEP.FUF_Cod;
    Locali    := FmEntiCEP.FLocalCod;
    Bairro    := FmEntiCEP.FBairICod;
    Lograd    := FmEntiCEP.FLogrCod;
    FmEntiCEP.Destroy;
    //
    if Confirmou then ConfiguraPorCEP(UF, Locali, Bairro, Lograd, CEP);
    Destroy;
  end;
end;

procedure TFmRegioes.VerifiDTBs();
var
  UF_Atu, UF_New, Meso_Atu, Meso_New, Micro_Atu, Micro_New,
  Munici_Atu(*, Munici_New*): Integer;
begin
  UF_Atu     := Geral.IMV(EdDTB_UF.Text);
  Meso_Atu   := Geral.IMV(EdDTB_Meso.Text);
  Micro_Atu  := Geral.IMV(EdDTB_Micro.Text);
  Munici_Atu := Geral.IMV(EdDTB_Munici.Text);
  //
  UF_New     := UF_Atu;
  Meso_New   := Meso_Atu;
  Micro_New  := Micro_Atu;
  //Munici_New := Munici_Atu;
  //
  if UF_Atu = 0 then
  begin
    EdPesqDTB_Meso.Text := '';
    if Meso_Atu > 0 then
    begin
      QrDTB_Meso.Locate('Codigo', Meso_Atu, []);
      UF_New := QrDTB_MesoDTB_UF.Value;
    end else begin
      EdPesqDTB_Micro.Text := '';
      if Micro_Atu > 0 then
      begin
        QrDTB_Micro.Locate('Codigo', Micro_Atu, []);
        UF_New   := QrDTB_MicroDTB_UF.Value;
        Meso_New := QrDTB_MicroDTB_Meso.Value;
      end else begin
        EdPesqDTB_Munici.Text := '';
        if Munici_Atu > 0 then
        begin
          QrDTB_Munici.Locate('Codigo', Munici_Atu, []);
          UF_New    := QrDTB_MuniciDTB_UF.Value;
          Meso_New  := QrDTB_MuniciDTB_Meso.Value;
          Micro_New := QrDTB_MUniciDTB_Micro.Value;
        end else begin
          //
        end;
      end;
    end;
  end else if Meso_Atu = 0 then
  begin
    EdPesqDTB_Micro.Text := '';
    if Micro_Atu > 0 then
    begin
      QrDTB_Micro.Locate('Codigo', Micro_Atu, []);
      UF_New   := QrDTB_MicroDTB_UF.Value;
      Meso_New := QrDTB_MicroDTB_Meso.Value;
    end else begin
      EdPesqDTB_Munici.Text := '';
      if Munici_Atu > 0 then
      begin
        QrDTB_Munici.Locate('Codigo', Munici_Atu, []);
        UF_New    := QrDTB_MuniciDTB_UF.Value;
        Meso_New  := QrDTB_MuniciDTB_Meso.Value;
        Micro_New := QrDTB_MUniciDTB_Micro.Value;
      end else begin
        //
      end;
    end;
   end else if Micro_Atu = 0 then
   begin
    EdPesqDTB_Munici.Text := '';
    if Munici_Atu > 0 then
    begin
      QrDTB_Munici.Locate('Codigo', Munici_Atu, []);
      UF_New    := QrDTB_MuniciDTB_UF.Value;
      Meso_New  := QrDTB_MuniciDTB_Meso.Value;
      Micro_New := QrDTB_MUniciDTB_Micro.Value;
    end else begin
      //
    end;
  end;
  //
  if (UF_New <> UF_Atu) and (UF_New > 0) then
    ReopenDTB_UFs(UF_New);
  if (Meso_New <> Meso_Atu) and (Meso_New > 0) then
    ReopenDTB_Meso(Meso_New);
  if (Micro_New <> Micro_Atu) and (Micro_New > 0) then
    ReopenDTB_Micro(Micro_New);
end;

procedure TFmRegioes.ConfiguraPorCEP(UF, Locali, Bairro, Lograd: Integer; CEP: String);
var
  UF_DTB, Munici: Integer;
  UF_Txt: String;
begin
  EdDNE_CEP.Text := CEP;
  UF_Txt := Geral.GetSiglaUF_do_CodigoUF(UF);
  if QrDTB_UFs.Locate('Sigla', UF_Txt, [loCaseInsensitive]) then
  begin
    UF_DTB := Geral.GetCodigoUF_IBGE_DTB_da_SiglaUF(UF_Txt);
    EdDTB_UF.ValueVariant := UF_DTB;
    CBDTB_UF.KeyValue     := UF_DTB;
    //
    Dmod.QrAux.Close;
    Dmod.QrAux.SQL.Clear;
    Dmod.QrAux.SQL.Add('SELECT Codigo');
    Dmod.QrAux.SQL.Add('FROM dtb_munici');
    Dmod.QrAux.SQL.Add('WHERE DNE = ' + FormatFloat('0', Locali));
    UnDmkDAC_PF.AbreQuery(Dmod.QrAux, DModG.AllID_DB);
    if Dmod.QrAux.RecordCount > 0 then
    begin
      Munici := Dmod.QrAux.FieldByName('Codigo').AsInteger;
      if QrDTB_Munici.Locate('Codigo', Munici, []) then
      begin
        EdDTB_Munici.ValueVariant := Munici;
        CBDTB_Munici.KeyValue     := Munici;
        //
        if QrDNE_Locali.Locate('LOC_NU_SEQUENCIAL', Locali, []) then
        begin
          EdDNE_Locali.ValueVariant := Locali;
          CBDNE_Locali.KeyValue     := Locali;
          //
          if QrDNE_Bairros.Locate('BAI_NU_SEQUENCIAL', Bairro, []) then
          begin
            EdDNE_Bairro.ValueVariant := Bairro;
            CBDNE_Bairro.KeyValue     := Bairro;
            //
            if QrDNE_Lograd.Locate('LOG_NU_SEQUENCIAL', Lograd, []) then
            begin
              EdDNE_Lograd.ValueVariant := Lograd;
              CBDNE_Lograd.KeyValue     := Lograd;
              //
            end else
              Geral.MB_Aviso('N�o foi poss�vel definir o ' +
                'logradouro (C�digo DNE "' + FormatFloat('0', Lograd) +
                '")!');
          end else
            Geral.MB_Aviso('N�o foi poss�vel definir o ' +
              'bairro (C�digo DNE "' + Geral.FF0(bairro) +'")!');
        end else
          Geral.MB_Aviso('N�o foi poss�vel definir a ' +
            'localidade (C�digo DNE "' + Geral.FF0(Locali) +
            '")!');
      end else
        Geral.MB_Aviso('N�o foi poss�vel localizar a cidade ' +
          '(C�digo DTB "' + Geral.FF0(Munici) + '")!');
    end else
      Geral.MB_Aviso('N�o foi poss�vel definir a cidade da ' +
        'localidade (C�digo DNE "' + Geral.FF0(Locali) + '")!');
  end else
    Geral.MB_Aviso('N�o foi poss�vel definir a UF "' + UF_Txt +
      '". Verifique se ela est� dispon�vel!');
end;

function TFmRegioes.JaTemSubItens: Boolean;
  function SubItensCount(SQL: String; Codigo: Integer): Integer;
  var
    Txt: String;
  begin
    Txt := SQL;
    if Codigo <> 0 then
      Txt := SQL + FormatFloat('0', Codigo);
    QrSub.Close;
    QrSub.SQL.Clear;
    QrSub.SQL.Add('SELECT reg.CodUsu, reg.Nome NOMEREG,');
    QrSub.SQL.Add('rei.Controle, rei.Nivel, ELT(rei.Nivel+1,');
    QrSub.SQL.Add('"0. Nenhum", "1. CEP", "2. Logradouro", "3. Bairro",');
    QrSub.SQL.Add('"4. Localidade", "5. Munic�pio", "6. Microrregi�o",');
    QrSub.SQL.Add('"7. Mesorregi�o", "8. Estado", "9. Pa�s") NIVEL_TXT,');
    QrSub.SQL.Add('rei.DTB_Pais, ufs.Sigla UF, mes.Nome NOMEMESO,');
    QrSub.SQL.Add('mic.Nome NOMEMICRO, mun.Nome NOMEMUNICI,');
    QrSub.SQL.Add('rei.DTB_UF, rei.DTB_Meso, rei.DTB_Micro,');
    QrSub.SQL.Add('rei.DTB_Munici, rei.DNE_Locali, rei.DNE_Bairro,');
    QrSub.SQL.Add('rei.DNE_Lograd, rei.DNE_CEP,');
    QrSub.SQL.Add('rei.DNE_NO_Loc, rei.DNE_NO_Bai, rei.DNE_NO_Log');
    QrSub.SQL.Add('FROM regioesits rei');
    QrSub.SQL.Add('LEFT JOIN regioes reg ON reg.Codigo=rei.Codigo');
    QrSub.SQL.Add('LEFT JOIN ' + VAR_AllID_DB_NOME + '.dtb_ufs ufs ON ufs.Codigo=rei.DTB_UF');
    QrSub.SQL.Add('LEFT JOIN ' + VAR_AllID_DB_NOME + '.dtb_meso mes ON mes.Codigo=rei.DTB_Meso');
    QrSub.SQL.Add('LEFT JOIN ' + VAR_AllID_DB_NOME + '.dtb_micro mic ON mic.Codigo=rei.DTB_Micro');
    QrSub.SQL.Add('LEFT JOIN ' + VAR_AllID_DB_NOME + '.dtb_munici mun ON mun.Codigo=rei.DTB_Munici');
    QrSub.SQL.Add(Txt);
    QrSub.SQL.Add('ORDER BY rei.Nivel DESC, rei.DTB_Pais');
    QrSub.SQL.Add('');
    try
      UnDmkDAC_PF.AbreQuery(QrSub, Dmod.MyDB);
    except
      UnDmkDAC_PF.LeMeuSQL_Fixo_y(QrSub, '', nil, True, True);
    end;
    //
    Result := QrSub.RecordCount;
    //
    if Result > 0 then
    begin
      if Result = 1 then Txt := 'um n�vel inferior cadastrado!' + sLineBreak + 'Deseja visualiz�-lo?'
      else Txt := IntToStr(Result) +
        ' n�veis inferiores cadastrados!' + sLineBreak + 'Deseja visualizar a lista?';
      if Geral.MB_Pergunta('O n�vel selecionado j� possui ' + Txt) = ID_YES then
      begin
        if DBCheck.CriaFm(TFmRegioesSub, FmRegioesSub, afmoNegarComAviso) then
        begin
          FmRegioesSub.ShowModal;
          FmRegioesSub.Destroy;
        end;
      end;
    end;
  end;
begin
  case RGNivel.ItemIndex of
    3: Result := SubItensCount('WHERE rei.DNE_Bairro=', Geral.IMV(EdDNE_Bairro.Text)) > 0;
    4: Result := SubItensCount('WHERE rei.DNE_Locali=', Geral.IMV(EdDNE_Locali.Text)) > 0;
    5: Result := SubItensCount('WHERE rei.DTB_Munici=', Geral.IMV(EdDTB_Munici.Text)) > 0;
    6: Result := SubItensCount('WHERE rei.DTB_Micro=', Geral.IMV(EdDTB_Micro.Text)) > 0;
    7: Result := SubItensCount('WHERE rei.DTB_Meso=', Geral.IMV(EdDTB_Meso.Text)) > 0;
    8: Result := SubItensCount('WHERE rei.DTB_UF=', Geral.IMV(EdDTB_UF.Text)) > 0;
    //
    else Result := False;
  end;
end;

end.

