object FmPediVdaImp: TFmPediVdaImp
  Left = 0
  Top = 0
  Caption = 'PED-IMPRI-000 :: Vendas por Pedido de Venda'
  ClientHeight = 712
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Splitter1: TSplitter
    Left = 809
    Top = 48
    Height = 616
    Align = alRight
    ExplicitLeft = 272
    ExplicitTop = 160
    ExplicitHeight = 100
  end
  object DockTabSet1: TTabSet
    Left = 812
    Top = 48
    Width = 196
    Height = 616
    Align = alRight
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ShrinkToFit = True
    Style = tsModernPopout
    TabPosition = tpLeft
  end
  object pDockLeft: TPanel
    Left = 812
    Top = 48
    Width = 0
    Height = 616
    Align = alRight
    BevelOuter = bvNone
    DockSite = True
    TabOrder = 1
    OnDockDrop = pDockLeftDockDrop
    OnDockOver = pDockLeftDockOver
    OnUnDock = pDockLeftUnDock
    ExplicitLeft = 988
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    Caption = 'Vendas por Pedido de Venda'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 2
    object Image1: TImage
      Left = 1
      Top = 1
      Width = 1006
      Height = 46
      Align = alClient
      Transparent = True
      ExplicitLeft = 2
      ExplicitTop = 2
      ExplicitWidth = 788
      ExplicitHeight = 44
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 809
    Height = 616
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 3
    ExplicitWidth = 985
    object Panel2: TPanel
      Left = 0
      Top = 0
      Width = 517
      Height = 616
      Align = alLeft
      BevelOuter = bvNone
      TabOrder = 0
      object Panel5: TPanel
        Left = 0
        Top = 0
        Width = 517
        Height = 105
        Align = alTop
        TabOrder = 0
        object LaPVIEmp: TLabel
          Left = 8
          Top = 8
          Width = 45
          Height = 13
          Caption = 'Empresa:'
        end
        object LaPVICli: TLabel
          Left = 8
          Top = 32
          Width = 37
          Height = 13
          Caption = 'Cliente:'
        end
        object LaPVIRep: TLabel
          Left = 8
          Top = 56
          Width = 76
          Height = 13
          Caption = 'Representante:'
        end
        object LaPVIPrd: TLabel
          Left = 8
          Top = 80
          Width = 77
          Height = 13
          Caption = 'Grupo de prod.:'
        end
        object EdPVIPrd: TdmkEditCB
          Left = 96
          Top = 76
          Width = 52
          Height = 21
          Alignment = taRightJustify
          TabOrder = 6
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          DBLookupComboBox = CBPVIPrd
          IgnoraDBLookupComboBox = False
          AutoSetIfOnlyOneReg = setregOnlyManual
        end
        object EdPVIRep: TdmkEditCB
          Left = 96
          Top = 52
          Width = 52
          Height = 21
          Alignment = taRightJustify
          TabOrder = 4
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'Represen'
          UpdCampo = 'Represen'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          DBLookupComboBox = CBPVIRep
          IgnoraDBLookupComboBox = False
          AutoSetIfOnlyOneReg = setregOnlyManual
        end
        object EdPVICli: TdmkEditCB
          Left = 96
          Top = 28
          Width = 52
          Height = 21
          Alignment = taRightJustify
          TabOrder = 2
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'Cliente'
          UpdCampo = 'Cliente'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          DBLookupComboBox = CBPVICli
          IgnoraDBLookupComboBox = False
          AutoSetIfOnlyOneReg = setregOnlyManual
        end
        object EdPVIEmp: TdmkEditCB
          Left = 96
          Top = 4
          Width = 52
          Height = 21
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          DBLookupComboBox = CBPVIEmp
          IgnoraDBLookupComboBox = False
          AutoSetIfOnlyOneReg = setregOnlyManual
        end
        object CBPVIEmp: TdmkDBLookupComboBox
          Left = 152
          Top = 4
          Width = 356
          Height = 21
          KeyField = 'Filial'
          ListField = 'NOMEENT'
          ListSource = DmPediVda.DsPVIEmpCad
          TabOrder = 1
          dmkEditCB = EdPVIEmp
          UpdType = utYes
          LocF7SQLMasc = '$#'
          LocF7PreDefProc = f7pNone
        end
        object CBPVICli: TdmkDBLookupComboBox
          Left = 152
          Top = 28
          Width = 356
          Height = 21
          KeyField = 'Codigo'
          ListField = 'NOMEENT'
          ListSource = DmPediVda.DsPVICliCad
          TabOrder = 3
          dmkEditCB = EdPVICli
          QryCampo = 'Cliente'
          UpdType = utNil
          LocF7SQLMasc = '$#'
          LocF7PreDefProc = f7pNone
        end
        object CBPVIRep: TdmkDBLookupComboBox
          Left = 152
          Top = 52
          Width = 356
          Height = 21
          KeyField = 'Codigo'
          ListField = 'NOMEENT'
          ListSource = DmPediVda.DsPVIRepCad
          TabOrder = 5
          dmkEditCB = EdPVIRep
          QryCampo = 'Represen'
          UpdType = utNil
          LocF7SQLMasc = '$#'
          LocF7PreDefProc = f7pNone
        end
        object CBPVIPrd: TdmkDBLookupComboBox
          Left = 152
          Top = 76
          Width = 356
          Height = 21
          KeyField = 'CodUsu'
          ListField = 'Nome'
          ListSource = DmPediVda.DsPVIPrdCad
          TabOrder = 7
          dmkEditCB = EdPVIPrd
          UpdType = utYes
          LocF7SQLMasc = '$#'
          LocF7PreDefProc = f7pNone
        end
      end
      object Panel6: TPanel
        Left = 0
        Top = 105
        Width = 517
        Height = 144
        Align = alTop
        TabOrder = 1
        object GroupBox1: TGroupBox
          Left = 4
          Top = 4
          Width = 249
          Height = 65
          Caption = ' Per'#237'odo de inclus'#227'o: '
          TabOrder = 0
          object TPIncluIni: TdmkEditDateTimePicker
            Left = 12
            Top = 36
            Width = 112
            Height = 21
            Date = 39892.000000000000000000
            Time = 0.420627939813130100
            TabOrder = 1
            ReadOnly = False
            DefaultEditMask = '!99/99/99;1;_'
            AutoApplyEditMask = True
            UpdType = utYes
            DatePurpose = dmkdpNone
          end
          object TPIncluFim: TdmkEditDateTimePicker
            Left = 128
            Top = 36
            Width = 112
            Height = 21
            Date = 39892.000000000000000000
            Time = 0.420627939813130100
            TabOrder = 3
            ReadOnly = False
            DefaultEditMask = '!99/99/99;1;_'
            AutoApplyEditMask = True
            UpdType = utYes
            DatePurpose = dmkdpNone
          end
          object CkIncluIni: TCheckBox
            Left = 12
            Top = 16
            Width = 77
            Height = 17
            Caption = 'Data inicial:'
            Checked = True
            Enabled = False
            State = cbChecked
            TabOrder = 0
          end
          object CkIncluFim: TCheckBox
            Left = 128
            Top = 16
            Width = 77
            Height = 17
            Caption = 'Data final:'
            Checked = True
            Enabled = False
            State = cbChecked
            TabOrder = 2
          end
        end
        object GroupBox2: TGroupBox
          Left = 256
          Top = 4
          Width = 249
          Height = 65
          Caption = ' Per'#237'odo de emiss'#227'o: '
          TabOrder = 1
          object TPEmissIni: TdmkEditDateTimePicker
            Left = 12
            Top = 36
            Width = 112
            Height = 21
            Date = 39892.000000000000000000
            Time = 0.420627939813130100
            TabOrder = 1
            ReadOnly = False
            DefaultEditMask = '!99/99/99;1;_'
            AutoApplyEditMask = True
            UpdType = utYes
            DatePurpose = dmkdpNone
          end
          object TPEmissFim: TdmkEditDateTimePicker
            Left = 128
            Top = 36
            Width = 112
            Height = 21
            Date = 39892.000000000000000000
            Time = 0.420627939813130100
            TabOrder = 3
            ReadOnly = False
            DefaultEditMask = '!99/99/99;1;_'
            AutoApplyEditMask = True
            UpdType = utYes
            DatePurpose = dmkdpNone
          end
          object CkEmissIni: TCheckBox
            Left = 12
            Top = 16
            Width = 77
            Height = 17
            Caption = 'Data inicial:'
            Checked = True
            Enabled = False
            State = cbChecked
            TabOrder = 0
          end
          object CkEmissFim: TCheckBox
            Left = 128
            Top = 16
            Width = 77
            Height = 17
            Caption = 'Data final:'
            Checked = True
            Enabled = False
            State = cbChecked
            TabOrder = 2
          end
        end
        object GroupBox3: TGroupBox
          Left = 4
          Top = 72
          Width = 249
          Height = 65
          Caption = ' Per'#237'odo de chegada: '
          TabOrder = 2
          object TPEntraIni: TdmkEditDateTimePicker
            Left = 12
            Top = 36
            Width = 112
            Height = 21
            Date = 39892.000000000000000000
            Time = 0.420627939813130100
            TabOrder = 1
            ReadOnly = False
            DefaultEditMask = '!99/99/99;1;_'
            AutoApplyEditMask = True
            UpdType = utYes
            DatePurpose = dmkdpNone
          end
          object TPEntraFim: TdmkEditDateTimePicker
            Left = 128
            Top = 36
            Width = 112
            Height = 21
            Date = 39892.000000000000000000
            Time = 0.420627939813130100
            TabOrder = 3
            ReadOnly = False
            DefaultEditMask = '!99/99/99;1;_'
            AutoApplyEditMask = True
            UpdType = utYes
            DatePurpose = dmkdpNone
          end
          object CkEntraIni: TCheckBox
            Left = 12
            Top = 16
            Width = 77
            Height = 17
            Caption = 'Data inicial:'
            Checked = True
            Enabled = False
            State = cbChecked
            TabOrder = 0
          end
          object CkEntraFim: TCheckBox
            Left = 128
            Top = 16
            Width = 77
            Height = 17
            Caption = 'Data final:'
            Checked = True
            Enabled = False
            State = cbChecked
            TabOrder = 2
          end
        end
        object GroupBox4: TGroupBox
          Left = 256
          Top = 72
          Width = 249
          Height = 65
          Caption = ' Per'#237'odo de previs'#227'o de entrega: '
          TabOrder = 3
          object TPPreviIni: TdmkEditDateTimePicker
            Left = 12
            Top = 36
            Width = 112
            Height = 21
            Date = 39892.000000000000000000
            Time = 0.420627939813130100
            TabOrder = 1
            ReadOnly = False
            DefaultEditMask = '!99/99/99;1;_'
            AutoApplyEditMask = True
            UpdType = utYes
            DatePurpose = dmkdpNone
          end
          object TPPreviFim: TdmkEditDateTimePicker
            Left = 128
            Top = 36
            Width = 112
            Height = 21
            Date = 39892.000000000000000000
            Time = 0.420627939813130100
            TabOrder = 3
            ReadOnly = False
            DefaultEditMask = '!99/99/99;1;_'
            AutoApplyEditMask = True
            UpdType = utYes
            DatePurpose = dmkdpNone
          end
          object CkPreviIni: TCheckBox
            Left = 12
            Top = 16
            Width = 77
            Height = 17
            Caption = 'Data inicial:'
            Checked = True
            Enabled = False
            State = cbChecked
            TabOrder = 0
          end
          object CkPreviFim: TCheckBox
            Left = 128
            Top = 16
            Width = 77
            Height = 17
            Caption = 'Data final:'
            Checked = True
            Enabled = False
            State = cbChecked
            TabOrder = 2
          end
        end
      end
      object Panel7: TPanel
        Left = 0
        Top = 249
        Width = 517
        Height = 176
        Align = alTop
        TabOrder = 2
        object RGTipoSaldo: TRadioGroup
          Left = 4
          Top = 4
          Width = 501
          Height = 41
          Caption = ' Quantidade para ordena'#231#227'o: '
          Columns = 4
          Items.Strings = (
            'Pedida'
            'Pendente'
            'Cancelada'
            'Faturada')
          TabOrder = 0
          OnClick = RGTipoSaldoClick
        end
        object RGOrdemIts: TRadioGroup
          Left = 4
          Top = 48
          Width = 501
          Height = 41
          Caption = ' Ordena'#231#227'o dos itens: '
          Columns = 2
          Items.Strings = (
            'Quantidade'
            'Valor')
          TabOrder = 1
          OnClick = RGOrdemAgr_Click
        end
        object RGAgrupa: TRadioGroup
          Left = 4
          Top = 92
          Width = 501
          Height = 53
          Caption = ' Sintetizar por: '
          Columns = 3
          Items.Strings = (
            'Grupo de produto'
            'Cliente'
            'Representante'
            'Atributo de produto'
            'Produto')
          TabOrder = 2
          OnClick = RGAgrupaClick
        end
      end
      object Panel8: TPanel
        Left = 0
        Top = 425
        Width = 517
        Height = 191
        Align = alClient
        TabOrder = 3
        object RGOrdemAgr_: TRadioGroup
          Left = 1
          Top = 1
          Width = 515
          Height = 189
          Align = alClient
          Caption = ' Ordena'#231#227'o dos agrupamentos: '
          Columns = 2
          Items.Strings = (
            'Nome / descri'#231#227'o'
            'C'#243'digo')
          TabOrder = 0
          Visible = False
          OnClick = RGOrdemAgr_Click
        end
      end
    end
    object Panel9: TPanel
      Left = 517
      Top = 0
      Width = 292
      Height = 616
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 1
      ExplicitWidth = 468
      object DBGrid2: TDBGrid
        Left = 0
        Top = 467
        Width = 292
        Height = 149
        Align = alBottom
        DataSource = DsPVI
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = []
        Visible = False
        Columns = <
          item
            Expanded = False
            FieldName = 'Tabela'
            Title.Caption = 'Filtro'
            Width = 100
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CodUsu1'
            Title.Caption = 'C'#243'digo'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Nome1'
            Title.Caption = 'Descri'#231#227'o'
            Width = 140
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CodUsu2'
            Title.Caption = 'C'#243'digo'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Nome2'
            Title.Caption = 'Descri'#231#227'o'
            Width = 140
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CodUsu3'
            Title.Caption = 'C'#243'digo'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Nome3'
            Title.Caption = 'Descri'#231#227'o'
            Width = 140
            Visible = True
          end>
      end
      object DBGrid1: TdmkDBGrid
        Left = 0
        Top = 0
        Width = 292
        Height = 467
        Align = alClient
        Columns = <
          item
            Expanded = False
            FieldName = 'CODI_AGRUP'
            Title.Caption = 'Codigo'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NOME_AGRUP'
            Title.Caption = 'Descri'#231#227'o / nome'
            Width = 247
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NO_COR'
            Title.Caption = 'Cor'
            Width = 150
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NO_TAM'
            Title.Caption = 'Tamanho'
            Width = 50
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'QuantP'
            Title.Caption = 'Q.Ped.'
            Width = 36
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'QuantC'
            Title.Caption = 'Q.Can.'
            Width = 36
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'QuantV'
            Title.Caption = 'Q.Fat.'
            Width = 36
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'QuantF'
            Title.Caption = 'Q.Pen.'
            Width = 36
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'ValLiq'
            Width = 72
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'SEQ'
            Width = 36
            Visible = True
          end>
        Color = clWindow
        DataSource = Ds1
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'CODI_AGRUP'
            Title.Caption = 'Codigo'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NOME_AGRUP'
            Title.Caption = 'Descri'#231#227'o / nome'
            Width = 247
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NO_COR'
            Title.Caption = 'Cor'
            Width = 150
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NO_TAM'
            Title.Caption = 'Tamanho'
            Width = 50
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'QuantP'
            Title.Caption = 'Q.Ped.'
            Width = 36
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'QuantC'
            Title.Caption = 'Q.Can.'
            Width = 36
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'QuantV'
            Title.Caption = 'Q.Fat.'
            Width = 36
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'QuantF'
            Title.Caption = 'Q.Pen.'
            Width = 36
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'ValLiq'
            Width = 72
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'SEQ'
            Width = 36
            Visible = True
          end>
      end
    end
  end
  object Panel3: TPanel
    Left = 0
    Top = 664
    Width = 1008
    Height = 48
    Align = alBottom
    TabOrder = 4
    object BtConfirma: TBitBtn
      Tag = 20
      Left = 20
      Top = 4
      Width = 90
      Height = 40
      Caption = '&Pesquisa'
      NumGlyphs = 2
      TabOrder = 0
      OnClick = BtConfirmaClick
    end
    object Panel4: TPanel
      Left = 896
      Top = 1
      Width = 111
      Height = 46
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 3
      object BtDesiste: TBitBtn
        Tag = 13
        Left = 2
        Top = 3
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sair'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtDesisteClick
      end
    end
    object BtImprime: TBitBtn
      Tag = 5
      Left = 112
      Top = 4
      Width = 90
      Height = 40
      Caption = '&Imprime'
      Enabled = False
      NumGlyphs = 2
      TabOrder = 1
      OnClick = BtImprimeClick
    end
    object BtLimpa: TBitBtn
      Tag = 20
      Left = 204
      Top = 4
      Width = 90
      Height = 40
      Caption = '&Limpa'
      NumGlyphs = 2
      TabOrder = 2
      OnClick = BtLimpaClick
    end
  end
  object Qr1: TMySQLQuery
    Database = Dmod.MyDB
    AfterOpen = Qr1AfterOpen
    BeforeClose = Qr1BeforeClose
    OnCalcFields = Qr1CalcFields
    SQL.Strings = (
      'SELECT DISTINCT SUM(pvi.QuantP) QuantP, SUM(pvi.QuantC) QuantC, '
      'SUM(pvi.QuantV) QuantV, '
      'SUM(pvi.QuantP-pvi.QuantC-pvi.QuantV) QuantF, '
      'SUM(pvi.ValLiq) ValLiq, '
      'SUM(pvi.QuantP-pvi.QuantC-pvi.QuantV) QUANTX, '
      
        'SUM(pvi.ValLiq) / SUM(pvi.QuantP) * SUM(pvi.QuantP-pvi.QuantC-pv' +
        'i.QuantV) VAL_X_, '
      'gg1.Nome NOME_AGRUP, '
      'gti.Nome NO_TAM, gcc.Nome NO_COR, gcc.CodUsu CU_COR,'
      'gg1.CodUsu + 0.0 CODI_AGRUP'
      'FROM pedivdaits pvi'
      'LEFT JOIN pedivda   pvd ON pvd.Codigo=pvi.Codigo'
      'LEFT JOIN gragrux   ggx ON ggx.Controle=pvi.GraGruX'
      'LEFT JOIN gratamits gti ON gti.Controle=ggx.GraTamI'
      'LEFT JOIN gragru1   gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN entidades cli ON cli.Codigo=pvd.Cliente'
      'LEFT JOIN entidades rep ON rep.Codigo=pvd.Represen'
      'LEFT JOIN gragruc ggc ON ggc.Controle=ggx.GraGruC'
      'LEFT JOIN gracorcad gcc ON gcc.Codigo=ggc.GraCorCad'
      'WHERE (pvi.QuantP - pvi.QuantC - pvi.QuantV) > 0'
      'GROUP BY gg1.Nivel1')
    Left = 556
    Top = 116
    object Qr1QuantP: TFloatField
      FieldName = 'QuantP'
      Origin = 'QuantP'
    end
    object Qr1QuantC: TFloatField
      FieldName = 'QuantC'
      Origin = 'QuantC'
    end
    object Qr1QuantV: TFloatField
      FieldName = 'QuantV'
      Origin = 'QuantV'
    end
    object Qr1QuantF: TFloatField
      FieldName = 'QuantF'
      Origin = 'QuantF'
    end
    object Qr1ValLiq: TFloatField
      FieldName = 'ValLiq'
      Origin = 'ValLiq'
      DisplayFormat = '#,###,##0.00'
    end
    object Qr1CodUsu: TIntegerField
      FieldName = 'CodUsu'
      Origin = 'gragru1.CodUsu'
    end
    object Qr1SEQ: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'SEQ'
      Calculated = True
    end
    object Qr1QUANTX: TFloatField
      FieldName = 'QUANTX'
    end
    object Qr1VAL_X_: TFloatField
      FieldName = 'VAL_X_'
    end
    object Qr1NOME_AGRUP: TWideStringField
      FieldName = 'NOME_AGRUP'
      Size = 100
    end
    object Qr1CODI_AGRUP: TFloatField
      FieldName = 'CODI_AGRUP'
    end
    object Qr1NO_TAM: TWideStringField
      FieldName = 'NO_TAM'
      Size = 5
    end
    object Qr1NO_COR: TWideStringField
      FieldName = 'NO_COR'
      Size = 30
    end
    object Qr1CU_COR: TIntegerField
      FieldName = 'CU_COR'
    end
  end
  object Ds1: TDataSource
    DataSet = Qr1
    Left = 556
    Top = 164
  end
  object frxDs1: TfrxDBDataset
    UserName = 'frxDs1'
    CloseDataSource = False
    FieldAliases.Strings = (
      'QuantP=QuantP'
      'QuantC=QuantC'
      'QuantV=QuantV'
      'QuantF=QuantF'
      'ValLiq=ValLiq'
      'CodUsu=CodUsu'
      'SEQ=SEQ'
      'QUANTX=QUANTX'
      'VAL_X_=VAL_X_'
      'NOME_AGRUP=NOME_AGRUP'
      'CODI_AGRUP=CODI_AGRUP'
      'NO_TAM=NO_TAM'
      'NO_COR=NO_COR'
      'CU_COR=CU_COR')
    DataSet = Qr1
    BCDToCurrency = False
    DataSetOptions = []
    Left = 560
    Top = 308
  end
  object frxPED_INPRI_000_01: TfrxReport
    Version = '2022.1'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39596.679376979200000000
    ReportOptions.LastChange = 39596.679376979200000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      
        '  MeVARF_CODI_FRX.Text := Copy(Report.Name, 4, Length(Report.Nam' +
        'e));'
      'end.')
    OnGetValue = frxPED_INPRI_000_01GetValue
    Left = 692
    Top = 8
    Datasets = <
      item
        DataSet = frxDs1
        DataSetName = 'frxDs1'
      end
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsPVI
        DataSetName = 'frxDsPVI'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 15.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
      MirrorMode = []
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 41.574830000000000000
        Top = 56.692950000000010000
        Width = 699.213050000000000000
        object Shape1: TfrxShapeView
          AllowVectorExport = True
          Width = 699.213050000000000000
          Height = 37.795300000000000000
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo13: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Width = 684.094930000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line1: TfrxLineView
          AllowVectorExport = True
          Top = 18.897650000000010000
          Width = 699.213050000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo14: TfrxMemoView
          AllowVectorExport = True
          Left = 120.944960000000000000
          Top = 18.897650000000010000
          Width = 457.323130000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'VENDAS POR PEDIDO DE VENDA - [VARF_SUB_TITULO]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo15: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Top = 18.897650000000010000
          Width = 113.385900000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'impresso em [Date], [Time]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo16: TfrxMemoView
          AllowVectorExport = True
          Left = 578.267716535433100000
          Top = 18.897650000000010000
          Width = 113.385826770000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page] de [TotalPages]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object ReportTitle1: TfrxReportTitle
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 13.228346460000000000
        Top = 18.897650000000000000
        Width = 699.213050000000000000
      end
      object GroupHeader1: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 18.897640240000000000
        Top = 211.653680000000000000
        Width = 699.213050000000000000
        Condition = 'frxDsPVI."Tabela"'
        object Memo22: TfrxMemoView
          AllowVectorExport = True
          Top = 3.779529999999994000
          Width = 699.213050000000000000
          Height = 15.118110240000000000
          DataSet = frxDsPVI
          DataSetName = 'frxDsPVI'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsPVI."Tabela"]')
          ParentFont = False
        end
      end
      object MasterData2: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -9
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Top = 253.228510000000000000
        Width = 699.213050000000000000
        ColumnGap = 37.795275590551200000
        DataSet = frxDsPVI
        DataSetName = 'frxDsPVI'
        RowCount = 0
      end
      object GroupFooter1: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Top = 275.905690000000000000
        Width = 699.213050000000000000
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 18.897650000000000000
        Top = 343.937230000000000000
        Width = 699.213050000000000000
        DataSet = frxDs1
        DataSetName = 'frxDs1'
        RowCount = 0
        object Memo3: TfrxMemoView
          AllowVectorExport = True
          Left = 94.488250000000000000
          Width = 343.937230000000000000
          Height = 18.897650000000000000
          DataField = 'NOME_AGRUP'
          DataSet = frxDs1
          DataSetName = 'frxDs1'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDs1."NOME_AGRUP"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo21: TfrxMemoView
          AllowVectorExport = True
          Left = 438.425480000000000000
          Width = 109.606370000000000000
          Height = 18.897650000000000000
          DataField = 'QUANTX'
          DataSet = frxDs1
          DataSetName = 'frxDs1'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDs1."QUANTX"]')
          ParentFont = False
        end
        object Memo23: TfrxMemoView
          AllowVectorExport = True
          Left = 548.031849999999900000
          Width = 94.488250000000000000
          Height = 18.897650000000000000
          DataField = 'VAL_X_'
          DataSet = frxDs1
          DataSetName = 'frxDs1'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDs1."VAL_X_"]')
          ParentFont = False
        end
        object Memo25: TfrxMemoView
          AllowVectorExport = True
          Left = 642.520100000000000000
          Width = 56.692950000000000000
          Height = 18.897650000000000000
          DataField = 'SEQ'
          DataSet = frxDs1
          DataSetName = 'frxDs1'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDs1."SEQ"]')
          ParentFont = False
        end
        object Memo38: TfrxMemoView
          AllowVectorExport = True
          Width = 94.488250000000000000
          Height = 18.897650000000000000
          DataField = 'CODI_AGRUP'
          DataSet = frxDs1
          DataSetName = 'frxDs1'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDs1."CODI_AGRUP"]')
          ParentFont = False
          WordWrap = False
        end
      end
      object ReportSummary1: TfrxReportSummary
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 37.795300000000000000
        Top = 423.307360000000000000
        Width = 699.213050000000000000
        object Memo24: TfrxMemoView
          AllowVectorExport = True
          Top = 11.338590000000010000
          Width = 438.425480000000000000
          Height = 18.897650000000000000
          DataSetName = 'frxDsConsIts'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'TOTAIS')
          ParentFont = False
        end
        object Memo43: TfrxMemoView
          AllowVectorExport = True
          Left = 438.425480000000000000
          Top = 11.338590000000010000
          Width = 109.606370000000000000
          Height = 18.897650000000000000
          DataSet = frxDs1
          DataSetName = 'frxDs1'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDs1."QUANTX">, MasterData1)]')
          ParentFont = False
        end
        object Memo44: TfrxMemoView
          AllowVectorExport = True
          Left = 548.031850000000000000
          Top = 11.338590000000010000
          Width = 94.488250000000000000
          Height = 18.897650000000000000
          DataSet = frxDs1
          DataSetName = 'frxDs1'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDs1."VAL_X_">, MasterData1)]')
          ParentFont = False
        end
        object Memo45: TfrxMemoView
          AllowVectorExport = True
          Left = 642.520100000000000000
          Top = 11.338590000000010000
          Width = 56.692950000000000000
          Height = 18.897650000000000000
          DataSet = frxDs1
          DataSetName = 'frxDs1'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
        end
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 13.228346460000000000
        Top = 483.779840000000000000
        Width = 699.213050000000000000
        object Memo1: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Width = 362.834880000000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'www.dermatek.com.br - software customizado')
          ParentFont = False
          VAlign = vaCenter
        end
        object MeVARF_CODI_FRX: TfrxMemoView
          AllowVectorExport = True
          Left = 370.393940000000000000
          Width = 321.260050000000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'VARF_CODI_FRX')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object Header2: TfrxHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 22.677180000000000000
        Top = 298.582870000000000000
        Width = 699.213050000000000000
        object Memo11: TfrxMemoView
          AllowVectorExport = True
          Left = 94.488250000000000000
          Top = 3.779530000000022000
          Width = 343.937230000000000000
          Height = 18.897650000000000000
          DataSet = frxDs1
          DataSetName = 'frxDs1'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Descri'#231#227'o / Nome')
          ParentFont = False
        end
        object Memo26: TfrxMemoView
          AllowVectorExport = True
          Left = 438.425480000000000000
          Top = 3.779530000000022000
          Width = 109.606370000000000000
          Height = 18.897650000000000000
          DataSet = frxDs1
          DataSetName = 'frxDs1'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Quantidade')
          ParentFont = False
        end
        object Memo27: TfrxMemoView
          AllowVectorExport = True
          Left = 548.031850000000000000
          Top = 3.779530000000022000
          Width = 94.488250000000000000
          Height = 18.897650000000000000
          DataSet = frxDs1
          DataSetName = 'frxDs1'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Valor')
          ParentFont = False
        end
        object Memo28: TfrxMemoView
          AllowVectorExport = True
          Left = 642.520100000000000000
          Top = 3.779530000000022000
          Width = 56.692950000000000000
          Height = 18.897650000000000000
          DataSet = frxDs1
          DataSetName = 'frxDs1'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Seq.')
          ParentFont = False
        end
        object Memo20: TfrxMemoView
          AllowVectorExport = True
          Top = 3.779530000000022000
          Width = 94.488250000000000000
          Height = 18.897650000000000000
          DataSet = frxDs1
          DataSetName = 'frxDs1'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'C'#243'digo')
          ParentFont = False
        end
      end
      object MasterData3: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 30.125996460000000000
        Top = 158.740260000000000000
        Width = 699.213050000000000000
        RowCount = 1
        object Memo29: TfrxMemoView
          AllowVectorExport = True
          Width = 177.637795280000000000
          Height = 13.228346460000000000
          DataSet = frxDsPVI
          DataSetName = 'frxDsPVI'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Data de inclus'#227'o')
          ParentFont = False
        end
        object Memo30: TfrxMemoView
          AllowVectorExport = True
          Top = 13.118120000000000000
          Width = 177.637795280000000000
          Height = 13.228346460000000000
          DataSet = frxDsPVI
          DataSetName = 'frxDsPVI'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_PERIODO_INCLU]')
          ParentFont = False
        end
        object Memo31: TfrxMemoView
          AllowVectorExport = True
          Left = 177.637910000000000000
          Width = 173.858267720000000000
          Height = 13.228346460000000000
          DataSet = frxDsPVI
          DataSetName = 'frxDsPVI'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Data de emiss'#227'o')
          ParentFont = False
        end
        object Memo32: TfrxMemoView
          AllowVectorExport = True
          Left = 177.637910000000000000
          Top = 13.118120000000000000
          Width = 173.858267720000000000
          Height = 13.228346460000000000
          DataSet = frxDsPVI
          DataSetName = 'frxDsPVI'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_PERIODO_EMISS]')
          ParentFont = False
        end
        object Memo33: TfrxMemoView
          AllowVectorExport = True
          Left = 351.496290000000000000
          Width = 173.858267720000000000
          Height = 13.228346460000000000
          DataSet = frxDsPVI
          DataSetName = 'frxDsPVI'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Data de chegada')
          ParentFont = False
        end
        object Memo34: TfrxMemoView
          AllowVectorExport = True
          Left = 351.496290000000000000
          Top = 13.118120000000000000
          Width = 173.858267720000000000
          Height = 13.228346460000000000
          DataSet = frxDsPVI
          DataSetName = 'frxDsPVI'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_PERIODO_ENTRA]')
          ParentFont = False
        end
        object Memo35: TfrxMemoView
          AllowVectorExport = True
          Left = 525.354670000000000000
          Width = 173.858267720000000000
          Height = 13.228346460000000000
          DataSet = frxDsPVI
          DataSetName = 'frxDsPVI'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Previs'#227'o de entrega')
          ParentFont = False
        end
        object Memo36: TfrxMemoView
          AllowVectorExport = True
          Left = 525.354670000000000000
          Top = 13.118120000000000000
          Width = 173.858267720000000000
          Height = 13.228346460000000000
          DataSet = frxDsPVI
          DataSetName = 'frxDsPVI'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_PERIODO_PREVI]')
          ParentFont = False
        end
      end
    end
  end
  object QrGraAtrCad: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, CodUsu, Nome'
      'FROM graatrcad'
      'ORDER BY Nome')
    Left = 8
    Top = 8
    object QrGraAtrCadCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrGraAtrCadCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrGraAtrCadNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
  end
  object DsPVI: TDataSource
    Left = 560
    Top = 260
  end
  object frxDsPVI: TfrxDBDataset
    UserName = 'frxDsPVI'
    CloseDataSource = False
    BCDToCurrency = False
    DataSetOptions = []
    Left = 560
    Top = 356
  end
  object frxPED_INPRI_000_02: TfrxReport
    Version = '2022.1'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39596.679376979200000000
    ReportOptions.LastChange = 40819.429065879600000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      
        '  MeVARF_CODI_FRX.Text := Copy(Report.Name, 4, Length(Report.Nam' +
        'e));'
      'end.')
    OnGetValue = frxPED_INPRI_000_01GetValue
    Left = 720
    Top = 8
    Datasets = <
      item
        DataSet = frxDs1
        DataSetName = 'frxDs1'
      end
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsPVI
        DataSetName = 'frxDsPVI'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 15.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
      MirrorMode = []
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 41.574830000000000000
        Top = 56.692950000000000000
        Width = 699.213050000000000000
        object Shape1: TfrxShapeView
          AllowVectorExport = True
          Width = 699.213050000000000000
          Height = 37.795300000000000000
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo13: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Width = 684.094930000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line1: TfrxLineView
          AllowVectorExport = True
          Top = 18.897650000000000000
          Width = 699.213050000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo14: TfrxMemoView
          AllowVectorExport = True
          Left = 120.944960000000000000
          Top = 18.897650000000000000
          Width = 457.323130000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'VENDAS POR PEDIDO DE VENDA - [VARF_SUB_TITULO]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo15: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 113.385900000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'impresso em [Date], [Time]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo16: TfrxMemoView
          AllowVectorExport = True
          Left = 578.267716535433000000
          Top = 18.897650000000000000
          Width = 113.385826770000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page] de [TotalPages]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object ReportTitle1: TfrxReportTitle
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 13.228346460000000000
        Top = 18.897650000000000000
        Width = 699.213050000000000000
      end
      object GroupHeader1: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 18.897640240000000000
        Top = 211.653680000000000000
        Width = 699.213050000000000000
        Condition = 'frxDsPVI."Tabela"'
        object Memo22: TfrxMemoView
          AllowVectorExport = True
          Top = 3.779530000000000000
          Width = 699.213050000000000000
          Height = 15.118110240000000000
          DataSet = frxDsPVI
          DataSetName = 'frxDsPVI'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsPVI."Tabela"]')
          ParentFont = False
        end
      end
      object MasterData2: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -9
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Top = 253.228510000000000000
        Width = 699.213050000000000000
        ColumnGap = 37.795275590551200000
        DataSet = frxDsPVI
        DataSetName = 'frxDsPVI'
        RowCount = 0
      end
      object GroupFooter1: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Top = 275.905690000000000000
        Width = 699.213050000000000000
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 13.228346460000000000
        Top = 393.071120000000000000
        Width = 699.213050000000000000
        DataSet = frxDs1
        DataSetName = 'frxDs1'
        RowCount = 0
        object Memo21: TfrxMemoView
          AllowVectorExport = True
          Left = 495.118430000000000000
          Width = 83.149606300000000000
          Height = 13.228346460000000000
          DataField = 'QUANTX'
          DataSet = frxDs1
          DataSetName = 'frxDs1'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDs1."QUANTX"]')
          ParentFont = False
        end
        object Memo23: TfrxMemoView
          AllowVectorExport = True
          Left = 578.268090000000000000
          Width = 83.149606300000000000
          Height = 13.228346460000000000
          DataField = 'VAL_X_'
          DataSet = frxDs1
          DataSetName = 'frxDs1'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDs1."VAL_X_"]')
          ParentFont = False
        end
        object Memo25: TfrxMemoView
          AllowVectorExport = True
          Left = 661.417750000000000000
          Width = 37.795300000000000000
          Height = 13.228346460000000000
          DataField = 'SEQ'
          DataSet = frxDs1
          DataSetName = 'frxDs1'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDs1."SEQ"]')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          AllowVectorExport = True
          Left = 434.645950000000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          DataField = 'NO_TAM'
          DataSet = frxDs1
          DataSetName = 'frxDs1'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDs1."NO_TAM"]')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          AllowVectorExport = True
          Left = 37.795300000000000000
          Width = 396.850610940000000000
          Height = 13.228346460000000000
          DataSet = frxDs1
          DataSetName = 'frxDs1'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
          WordWrap = False
        end
      end
      object ReportSummary1: TfrxReportSummary
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 24.566936460000000000
        Top = 536.693260000000000000
        Width = 699.213050000000000000
        object Memo24: TfrxMemoView
          AllowVectorExport = True
          Top = 11.338590000000000000
          Width = 495.118430000000000000
          Height = 13.228346460000000000
          DataSetName = 'frxDsConsIts'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'TOTAIS')
          ParentFont = False
        end
        object Memo43: TfrxMemoView
          AllowVectorExport = True
          Left = 495.118430000000000000
          Top = 11.338590000000000000
          Width = 83.149660000000000000
          Height = 13.228346460000000000
          DataSet = frxDs1
          DataSetName = 'frxDs1'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDs1."QUANTX">, MasterData1)]')
          ParentFont = False
        end
        object Memo44: TfrxMemoView
          AllowVectorExport = True
          Left = 578.268090000000000000
          Top = 11.338590000000000000
          Width = 83.149660000000000000
          Height = 13.228346460000000000
          DataSet = frxDs1
          DataSetName = 'frxDs1'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDs1."VAL_X_">, MasterData1)]')
          ParentFont = False
        end
        object Memo45: TfrxMemoView
          AllowVectorExport = True
          Left = 661.417750000000000000
          Top = 11.338590000000000000
          Width = 37.795300000000000000
          Height = 13.228346460000000000
          DataSet = frxDs1
          DataSetName = 'frxDs1'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
        end
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 13.228346460000000000
        Top = 585.827150000000000000
        Width = 699.213050000000000000
        object Memo1: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Width = 362.834880000000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'www.dermatek.com.br - software customizado')
          ParentFont = False
          VAlign = vaCenter
        end
        object MeVARF_CODI_FRX: TfrxMemoView
          AllowVectorExport = True
          Left = 370.393940000000000000
          Width = 321.260050000000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'VARF_CODI_FRX')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object MasterData3: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 30.125996460000000000
        Top = 158.740260000000000000
        Width = 699.213050000000000000
        RowCount = 1
        object Memo29: TfrxMemoView
          AllowVectorExport = True
          Width = 177.637795280000000000
          Height = 13.228346460000000000
          DataSet = frxDsPVI
          DataSetName = 'frxDsPVI'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Data de inclus'#227'o')
          ParentFont = False
        end
        object Memo30: TfrxMemoView
          AllowVectorExport = True
          Top = 13.118120000000000000
          Width = 177.637795280000000000
          Height = 13.228346460000000000
          DataSet = frxDsPVI
          DataSetName = 'frxDsPVI'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_PERIODO_INCLU]')
          ParentFont = False
        end
        object Memo31: TfrxMemoView
          AllowVectorExport = True
          Left = 177.637910000000000000
          Width = 173.858267720000000000
          Height = 13.228346460000000000
          DataSet = frxDsPVI
          DataSetName = 'frxDsPVI'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Data de emiss'#227'o')
          ParentFont = False
        end
        object Memo32: TfrxMemoView
          AllowVectorExport = True
          Left = 177.637910000000000000
          Top = 13.118120000000000000
          Width = 173.858267720000000000
          Height = 13.228346460000000000
          DataSet = frxDsPVI
          DataSetName = 'frxDsPVI'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_PERIODO_EMISS]')
          ParentFont = False
        end
        object Memo33: TfrxMemoView
          AllowVectorExport = True
          Left = 351.496290000000000000
          Width = 173.858267720000000000
          Height = 13.228346460000000000
          DataSet = frxDsPVI
          DataSetName = 'frxDsPVI'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Data de chegada')
          ParentFont = False
        end
        object Memo34: TfrxMemoView
          AllowVectorExport = True
          Left = 351.496290000000000000
          Top = 13.118120000000000000
          Width = 173.858267720000000000
          Height = 13.228346460000000000
          DataSet = frxDsPVI
          DataSetName = 'frxDsPVI'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_PERIODO_ENTRA]')
          ParentFont = False
        end
        object Memo35: TfrxMemoView
          AllowVectorExport = True
          Left = 525.354670000000000000
          Width = 173.858267720000000000
          Height = 13.228346460000000000
          DataSet = frxDsPVI
          DataSetName = 'frxDsPVI'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Previs'#227'o de entrega')
          ParentFont = False
        end
        object Memo36: TfrxMemoView
          AllowVectorExport = True
          Left = 525.354670000000000000
          Top = 13.118120000000000000
          Width = 173.858267720000000000
          Height = 13.228346460000000000
          DataSet = frxDsPVI
          DataSetName = 'frxDsPVI'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_PERIODO_PREVI]')
          ParentFont = False
        end
      end
      object GroupHeader2: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 20.787404020000000000
        Top = 298.582870000000000000
        Width = 699.213050000000000000
        Condition = 'frxDs1."NOME_AGRUP"'
        object Memo5: TfrxMemoView
          AllowVectorExport = True
          Top = 3.779530000000000000
          Width = 37.795300000000000000
          Height = 15.118110240000000000
          DataSet = frxDs1
          DataSetName = 'frxDs1'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Grupo:')
          ParentFont = False
          WordWrap = False
        end
        object Memo6: TfrxMemoView
          AllowVectorExport = True
          Left = 37.795300000000000000
          Top = 3.779530000000000000
          Width = 94.488250000000000000
          Height = 15.118110240000000000
          DataSet = frxDs1
          DataSetName = 'frxDs1'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDs1."CODI_AGRUP"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo8: TfrxMemoView
          AllowVectorExport = True
          Left = 132.283550000000000000
          Top = 3.779530000000000000
          Width = 510.236550000000000000
          Height = 15.118110240000000000
          DataSet = frxDs1
          DataSetName = 'frxDs1'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDs1."NOME_AGRUP"]')
          ParentFont = False
          WordWrap = False
        end
      end
      object GroupFooter2: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 13.228346460000000000
        Top = 430.866420000000000000
        Width = 699.213050000000000000
        object Memo7: TfrxMemoView
          AllowVectorExport = True
          Left = 37.795300000000000000
          Width = 60.472480000000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'T O T A L')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo40: TfrxMemoView
          AllowVectorExport = True
          Left = 495.118430000000000000
          Width = 83.149606300000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDs1."QUANTX">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo9: TfrxMemoView
          AllowVectorExport = True
          Left = 578.268090000000000000
          Width = 83.149606300000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDs1."VAL_X_">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo11: TfrxMemoView
          AllowVectorExport = True
          Left = 661.417750000000000000
          Width = 37.795300000000000000
          Height = 13.228346460000000000
          DataSet = frxDs1
          DataSetName = 'frxDs1'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          AllowVectorExport = True
          Left = 98.267780000000000000
          Width = 396.850650000000000000
          Height = 13.228346460000000000
          DataSet = frxDs1
          DataSetName = 'frxDs1'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
        end
      end
      object GroupHeader3: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 26.456692920000000000
        Top = 343.937230000000000000
        Width = 699.213050000000000000
        Condition = 'frxDs1."CU_COR"'
        object Memo18: TfrxMemoView
          AllowVectorExport = True
          Left = 495.118430000000000000
          Top = 13.228344020000000000
          Width = 83.149606300000000000
          Height = 13.228346460000000000
          DataSet = frxDs1
          DataSetName = 'frxDs1'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Quantidade')
          ParentFont = False
          WordWrap = False
        end
        object Memo19: TfrxMemoView
          AllowVectorExport = True
          Left = 661.417750000000000000
          Top = 13.228344020000000000
          Width = 37.795246300000000000
          Height = 13.228346460000000000
          DataSet = frxDs1
          DataSetName = 'frxDs1'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Seq.')
          ParentFont = False
          WordWrap = False
        end
        object Memo37: TfrxMemoView
          AllowVectorExport = True
          Left = 37.795300000000000000
          Width = 56.692950000000000000
          Height = 13.228346460000000000
          DataSet = frxDs1
          DataSetName = 'frxDs1'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDs1."CU_COR"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo39: TfrxMemoView
          AllowVectorExport = True
          Left = 94.488250000000000000
          Width = 548.031850000000000000
          Height = 13.228346460000000000
          DataSet = frxDs1
          DataSetName = 'frxDs1'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDs1."NO_COR"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo41: TfrxMemoView
          AllowVectorExport = True
          Left = 434.645950000000000000
          Top = 13.228344020000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          DataSet = frxDs1
          DataSetName = 'frxDs1'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Tamanho')
          ParentFont = False
          WordWrap = False
        end
        object Memo46: TfrxMemoView
          AllowVectorExport = True
          Left = 578.268090000000000000
          Top = 13.228346460000000000
          Width = 83.149606300000000000
          Height = 13.228346460000000000
          DataSet = frxDs1
          DataSetName = 'frxDs1'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Valor')
          ParentFont = False
          WordWrap = False
        end
        object Memo47: TfrxMemoView
          AllowVectorExport = True
          Left = 15.118120000000000000
          Width = 22.677180000000000000
          Height = 13.228346460000000000
          DataSet = frxDs1
          DataSetName = 'frxDs1'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Cor:')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo2: TfrxMemoView
          AllowVectorExport = True
          Left = 37.795300000000000000
          Top = 13.228346460000000000
          Width = 396.850610940000000000
          Height = 13.228346460000000000
          DataSet = frxDs1
          DataSetName = 'frxDs1'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '')
          ParentFont = False
          WordWrap = False
        end
      end
      object GroupFooter3: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 7.559060000000000000
        Top = 468.661720000000000000
        Width = 699.213050000000000000
        object Memo10: TfrxMemoView
          AllowVectorExport = True
          Width = 699.213050000000000000
          Height = 7.559060000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
    end
  end
end
