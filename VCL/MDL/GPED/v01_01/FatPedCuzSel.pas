unit FatPedCuzSel;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, dmkGeral, Grids, DBGrids, dmkImage, UnDmkEnums;

type
  TFmFatPedCuzSel = class(TForm)
    Panel1: TPanel;
    DBGrid1: TDBGrid;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel3: TPanel;
    PnSaiDesis: TPanel;
    BtOK: TBitBtn;
    BtSaida: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure DBGrid1DblClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    FControle: Integer;
  end;

  var
  FmFatPedCuzSel: TFmFatPedCuzSel;

implementation

uses UnMyObjects, ModPediVda;

{$R *.DFM}

procedure TFmFatPedCuzSel.BtOKClick(Sender: TObject);
begin
  FControle := DmPediVda.QrSdoRedPedControle.Value;
  Close;
end;

procedure TFmFatPedCuzSel.BtSaidaClick(Sender: TObject);
begin
  FControle := 0;
  Close;
end;

procedure TFmFatPedCuzSel.DBGrid1DblClick(Sender: TObject);
begin
  FControle := DmPediVda.QrSdoRedPedControle.Value;
  Close;
end;

procedure TFmFatPedCuzSel.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmFatPedCuzSel.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  FControle := 0;
  //
  DBGrid1.Columns[0].Title.Caption := DmPediVda.QrSdoRedPedMedida1.Value;
  DBGrid1.Columns[1].Title.Caption := DmPediVda.QrSdoRedPedMedida2.Value;
  DBGrid1.Columns[2].Title.Caption := DmPediVda.QrSdoRedPedMedida3.Value;
  DBGrid1.Columns[3].Title.Caption := DmPediVda.QrSdoRedPedMedida4.Value;
end;

procedure TFmFatPedCuzSel.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

end.
