unit PediVdaGru2;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, Grids, ComCtrls,
  DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkGeral, UnDmkEnums,
  Variants, dmkLabel, Mask, dmkRadioGroup, dmkImage, Menus, UnDmkProcFunc,
  DmkDAC_PF, UnGrl_Geral;

type
  TFmPediVdaGru2 = class(TForm)
    Panel1: TPanel;
    QrGraGru1: TmySQLQuery;
    DsGraGru1: TDataSource;
    QrGraGru1Codusu: TIntegerField;
    QrGraGru1Nivel3: TIntegerField;
    QrGraGru1Nivel2: TIntegerField;
    QrGraGru1Nivel1: TIntegerField;
    QrGraGru1Nome: TWideStringField;
    QrGraGru1PrdGrupTip: TIntegerField;
    QrGraGru1GraTamCad: TIntegerField;
    QrGraGru1NOMEGRATAMCAD: TWideStringField;
    QrGraGru1CODUSUGRATAMCAD: TIntegerField;
    QrGraGru1CST_A: TSmallintField;
    QrGraGru1CST_B: TSmallintField;
    QrGraGru1UnidMed: TIntegerField;
    QrGraGru1NCM: TWideStringField;
    QrGraGru1Peso: TFloatField;
    QrGraGru1SIGLAUNIDMED: TWideStringField;
    QrGraGru1CODUSUUNIDMED: TIntegerField;
    QrGraGru1NOMEUNIDMED: TWideStringField;
    PnSeleciona: TPanel;
    EdGraGru1: TdmkEditCB;
    CBGraGru1: TdmkDBLookupComboBox;
    Label1: TLabel;
    PageControl1: TPageControl;
    TabSheet3: TTabSheet;
    GradeA: TStringGrid;
    StaticText2: TStaticText;
    TabSheet5: TTabSheet;
    GradeQ: TStringGrid;
    StaticText1: TStaticText;
    TabSheet6: TTabSheet;
    GradeC: TStringGrid;
    StaticText6: TStaticText;
    TabSheet9: TTabSheet;
    GradeX: TStringGrid;
    QrLoc: TmySQLQuery;
    QrLocUltimo: TIntegerField;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    GradeL: TStringGrid;
    GradeF: TStringGrid;
    ST3: TStaticText;
    ST2: TStaticText;
    ST1: TStaticText;
    TabSheet4: TTabSheet;
    GradeD: TStringGrid;
    StaticText3: TStaticText;
    QrGraGru1NOME_EX: TWideStringField;
    QrLista: TmySQLQuery;
    QrListaGraCusPrc: TIntegerField;
    PnJuros: TPanel;
    DBEdit1: TDBEdit;
    Label2: TLabel;
    EdCustoFin: TdmkEdit;
    Label3: TLabel;
    QrGraGru1Fracio: TSmallintField;
    Panel3: TPanel;
    RGLista: TRadioGroup;
    RGTipPrd: TdmkRadioGroup;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel5: TPanel;
    PnSaiDesis: TPanel;
    BtOK: TBitBtn;
    BtSaida: TBitBtn;
    PB1: TProgressBar;
    SBMenu: TSpeedButton;
    DBEdit2: TDBEdit;
    Label14: TLabel;
    PMMenu: TPopupMenu;
    Produtos1: TMenuItem;
    abelasdepreos1: TMenuItem;
    Regrasfiscais1: TMenuItem;
    Label4: TLabel;
    DBEdit3: TDBEdit;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure EdGraGru1Change(Sender: TObject);
    procedure EdGraGru1Exit(Sender: TObject);
    procedure GradeQDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect;
      State: TGridDrawState);
    procedure GradeADrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect;
      State: TGridDrawState);
    procedure GradeCDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect;
      State: TGridDrawState);
    procedure BtOKClick(Sender: TObject);
    procedure GradeQDblClick(Sender: TObject);
    procedure GradeQKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure GradeLDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect;
      State: TGridDrawState);
    procedure GradeFDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect;
      State: TGridDrawState);
    procedure GradeFKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure EdGraGru1Enter(Sender: TObject);
    procedure GradeDDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect;
      State: TGridDrawState);
    procedure GradeDKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure GradeDClick(Sender: TObject);
    procedure GradeDSelectCell(Sender: TObject; ACol, ARow: Integer;
      var CanSelect: Boolean);
    procedure GradeXDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect;
      State: TGridDrawState);
    procedure CBGraGru1Exit(Sender: TObject);
    procedure GradeFSelectCell(Sender: TObject; ACol, ARow: Integer;
      var CanSelect: Boolean);
    procedure GradeQSelectCell(Sender: TObject; ACol, ARow: Integer;
      var CanSelect: Boolean);
    procedure RGListaClick(Sender: TObject);
    procedure RGTipPrdClick(Sender: TObject);
    procedure SBMenuClick(Sender: TObject);
    procedure abelasdepreos1Click(Sender: TObject);
    procedure ImgTipoChange(Sender: TObject);
    procedure Regrasfiscais1Click(Sender: TObject);
    procedure Produtos1Click(Sender: TObject);
  private
    { Private declarations }
    FOldNivel1: Integer;
    procedure ReconfiguraGradeQ();
    // Qtde de itens
    function QtdeItemCorTam(GridP, GridC, GridX: TStringGrid): Boolean;
    function QtdeVariosItensCorTam(GridP, GridA, GridC, GridX: TStringGrid): Boolean;
    function ObtemQtde(var Qtde: Double): Boolean;
    //
    function  DescontoVariosItensCorTam(GridD, GridA, GridC, GridX:
              TStringGrid): Boolean;
    function  ObtemDesconto(var Desconto: Double): Boolean;
    function  ValidaDadosProduto(): Boolean;
    procedure ReopenGraGru1();
    procedure MostraFormGraGruN(SubForm: Integer);
  public
    { Public declarations }
    FPedidoCompra: Boolean;
  end;

  var
  FmPediVdaGru2: TFmPediVdaGru2;

implementation

uses
  {$IfNDef NAO_GFAT}UnGrade_Jan, UnGFat_Jan, {$EndIf}
  UnMyObjects, Module, ModProd, MyVCLSkin, GetValor, UMySQLModule,
  UnInternalConsts, PediVda2, GraGruVal, MyDBCheck, ModPediVda;

{$R *.DFM}

procedure TFmPediVdaGru2.abelasdepreos1Click(Sender: TObject);
{$IfNDef NAO_GFAT}
var
  TabPrc: Integer;
begin
  TabPrc := FmPediVda2.QrPediVdaTabelaPrc.Value;
  //
  GFat_Jan.MostraFormTabePrcCab(TabPrc);
  //
  try
    Screen.Cursor := crHourGlass;
    ReconfiguraGradeQ();
  finally
    Screen.Cursor := crDefault;
  end;
{$Else}
  Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappGFat);
{$EndIf}
end;

procedure TFmPediVdaGru2.BtOKClick(Sender: TObject);
var
  Col, Row, Nivel1, GraGruX, Codigo, Controle: Integer;
  Tot, Qtd, PrecoO, PrecoR, PrecoF, QuantP, ValBru, ValLiq,
  DescoP, DescoV, DescoI: Double;
  ErrPreco, AvisoPrc, QtdRed, Casas: Integer;
  Mensagem: String;
  // Pedido de compra
  vProd, vBC: Double;
begin
  if EdGraGru1.ValueVariant = 0 then
  begin
    Geral.MB_Aviso('Defina o produto!');
    Exit;
  end;
  //Valida regra fiscal com os dados do produto
  if not DmPediVda.ValidaRegraFiscalCFOPProdutos(QrGraGru1PrdGrupTip.Value,
    FmPediVda2.QrPediVdaRegrFiscal.Value, FmPediVda2.QrPediVdaNOMEFISREGCAD.Value,
    Mensagem) then
  begin
    Geral.MB_Info(Mensagem);
    Exit;
  end;
  if not ValidaDadosProduto() then Exit;
  //
  Nivel1 := QrGraGru1Nivel1.Value;
  Screen.Cursor := crHourGlass;
  try
    Codigo := FmPediVda2.QrPediVdaCodigo.Value;
    Tot       := 0;
    ErrPreco  := 0;
    AvisoPrc  := 0;
    QtdRed    := 0;
    for Col := 1 to GradeQ.ColCount -1 do
    begin
      for Row := 1 to GradeQ.RowCount - 1 do
      begin
        Qtd := Geral.IMV(GradeQ.Cells[Col,Row]);
        Tot := Tot + Qtd;
        if Qtd > 0 then
          QtdRed := QtdRed + 1;
        PrecoO := Geral.DMV(GradeL.Cells[Col,Row]);
        PrecoR := Geral.DMV(GradeF.Cells[Col,Row]);
        if (PrecoO <>  PrecoR) and (Qtd = 0) then
          AvisoPrc := AvisoPrc + 1;
        if (Qtd > 0) and (PrecoR = 0) then
          ErrPreco := ErrPreco + 1;
      end;
    end;
    if Tot = 0 then
    begin
      Geral.MB_Aviso('Não há definição de quantidades!');
      Exit;
    end
    else if ErrPreco > 0 then
    begin
      Geral.MB_Aviso('Existem ' + Geral.FF0(ErrPreco) +
        ' itens sem preço de faturamento definido!');
      Exit;
    end
    else if AvisoPrc > 0 then
    begin
      if Geral.MB_Pergunta('Existem ' + Geral.FF0(AvisoPrc) +
        ' itens com preço de faturamento alterado, mas não há definição de ' +
        'quantidades de itens!' + sLineBreak +
        'Deseja continuar assim mesmo?') <> ID_YES then
      begin
        Exit;
      end;
    end;
    PB1.Position := 0;
    PB1.Max := QtdRed;
    for Col := 1 to GradeQ.ColCount -1 do
    begin
      if GradeX.Cells[Col,0] <> '' then
      begin
        for Row := 1 to GradeQ.RowCount - 1 do
        begin
          if Geral.IMV(GradeX.Cells[0,Row]) <> 0 then
          begin
            Qtd := Geral.DMV(GradeQ.Cells[Col, Row]);
            if Qtd > 0 then
            begin
              //ShowMessage('Col: ' + Geral.FF0(Col) + '  Row: ' + Geral.FF0(Row) + sLineBreak + Geral.FF0(Qtd));
              GraGruX := Geral.IMV(GradeC.Cells[Col, Row]);
              if GraGruX <> 0 then
              begin
                Casas  := Dmod.QrControleCasasProd.Value;
                PrecoO := Geral.DMV(GradeL.Cells[Col, Row]);
                PrecoR := Geral.DMV(GradeF.Cells[Col, Row]);
                QuantP := Geral.DMV(GradeQ.Cells[Col, Row]);
                //ValCal := PrecoR;// * QuantP;
                DescoP := Geral.DMV(GradeD.Cells[Col, Row]);
                DescoI := dmkPF.FFF(PrecoR * DescoP / 100, Casas, siPositivo);
                PrecoF := PrecoR - DescoI;
                DescoV := DescoI * QuantP;
                ValBru := PrecoR * QuantP;
                ValLiq := ValBru - DescoV;
                //
                Controle := UMyMod.BuscaEmLivreY_Def('pedivdaits', 'Controle',
                  ImgTipo.SQLType, 0);
                {
                if FPedidoCompra = False then
                begin
                  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'pedivdaits', False,[
                  'Codigo', 'GraGruX', 'PrecoO',
                  'PrecoR', 'QuantP', 'ValBru',
                  'DescoP', 'DescoV', 'ValLiq',
                  'PrecoF'  ], [
                  'Controle'], [
                  Codigo, GraGruX, PrecoO,
                  PrecoR, QuantP, ValBru,
                  DescoP, DescoV, ValLiq,
                  PrecoF], [
                  Controle], True) then
                    DmPediVda.AtzSdosPedido(Codigo);
                end else
                begin
                }
                  vProd  := ValLiq;
                  vBC    := ValLiq;
                  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'pedivdaits', False,[
                  'Codigo', 'GraGruX', 'PrecoO',
                  'PrecoR', 'QuantP', 'ValBru',
                  'DescoP', 'DescoV', 'ValLiq',
                  'PrecoF',
                  'vProd', 'vBC'], [
                  'Controle'], [
                  Codigo, GraGruX, PrecoO,
                  PrecoR, QuantP, ValBru,
                  DescoP, DescoV, ValLiq,
                  PrecoF,
                  vProd, vBC], [
                  Controle], True) then
                    DmPediVda.AtzSdosPedido(Codigo);
                //end;
              end;
            end;
          end;
        end;
      end;
    end;
    { só teste
    DmPediVda.AtualizaTodosItensPediVda_(FmPediVda2.QrPediVdaCodigo.Value);
    DmPediVda.AtzSdosPedido(FmPediVda2.QrPediVdaCodigo.Value);
    }
    FmPediVda2.LocCod(Codigo, Codigo);
    FmPediVda2.ReopenPediVdaGru(Nivel1);
    EdGraGru1.ValueVariant := 0;
    EdGraGru1.SetFocus;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmPediVdaGru2.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmPediVdaGru2.CBGraGru1Exit(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 0;
  GradeQ.SetFocus;
  GradeQ.Col := 1;
  GradeQ.Row := 1;
end;

function TFmPediVdaGru2.DescontoVariosItensCorTam(GridD, GridA, GridC,
  GridX: TStringGrid): Boolean;
var
  Coluna, Linha, c, l, Ativos, ColI, ColF, RowI, RowF,
  CountC, CountL: Integer;
  Desconto: Double;
  DescontoTxt, ItensTxt: String;
begin
  Result := False;
  Ativos := 0;
  RowI := 0;
  RowF := 0;
  ColI := 0;
  ColF := 0;
  Coluna := GridD.Col;
  Linha  := GridD.Row;
  if (Coluna = 0) and (Linha = 0) then
  begin
    ColI := 1;
    ColF := GridD.ColCount - 1;
    RowI := 1;
    RowF := GridD.RowCount - 1;
  end else if Coluna = 0 then
  begin
    ColI := 1;
    ColF := GridD.ColCount - 1;
    RowI := Linha;
    RowF := Linha;
  end else if Linha = 0 then
  begin
    ColI := Coluna;
    ColF := Coluna;
    RowI := 1;
    RowF := GridD.RowCount - 1;
  end;
  //
  CountC := 0;
  CountL := 0;
  Screen.Cursor := crHourGlass;
  try
    for c := ColI to ColF do
      for l := RowI to RowF do
    begin
      CountC := CountC + Geral.IMV(GridX.Cells[c, 0]);
      CountL := CountL + Geral.IMV(GridX.Cells[0, l]);
    end;
    if CountC = 0 then
    begin
      Geral.MB_Aviso('Tamanho não definido!');
      Exit;
    end;
    if CountL = 0 then
    begin
      Geral.MB_Aviso('Cor não definida!');
      Exit;
    end;
    //
    //Nivel1 := QrGraGru1Nivel1.Value;
    Desconto  := Geral.DMV(GridD.Cells[ColI, RowI]);
    for c := ColI to ColF do
      for l := RowI to RowF do
        Ativos := Ativos + Geral.IMV(GridA.Cells[c, l]);
    if Ativos = 0 then
    begin
      Geral.MB_Aviso('Não há nenhum item com código na seleção ' +
        'para que se possa incluir / alterar o preço!');
      Exit;
    end;
    //
    if ObtemDesconto(Desconto) then
    begin
      DescontoTxt := Geral.FFT(Desconto, 2, siNegativo);
      if (Coluna = 0) and (Linha = 0) then
      begin
        ItensTxt := 'todo grupo';
      end else if Coluna = 0 then
      begin
        ItensTxt := 'todos tamanhos da cor ' + GridA.Cells[0, Linha];
      end else if Linha = 0 then
      begin
        ItensTxt := 'todas cores do tamanho ' + GridA.Cells[Coluna, 0];
      end;
      if Geral.MB_Pergunta('Confirma o valor de ' + DescontoTxt + ' para ' +
        ItensTxt + '?') <> ID_YES then
      begin
        Exit;
      end;
      //
      for c := ColI to ColF do
        for l := RowI to RowF do
          GridD.Cells[c, l] := DescontoTxt;
      Result := True;
    end;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmPediVdaGru2.EdGraGru1Change(Sender: TObject);
begin
  if EdGraGru1.Focused = False then
    ReconfiguraGradeQ();
end;

procedure TFmPediVdaGru2.EdGraGru1Enter(Sender: TObject);
begin
  FOldNivel1 := EdGraGru1.ValueVariant;
end;

procedure TFmPediVdaGru2.EdGraGru1Exit(Sender: TObject);
begin
  if FOldNivel1 <> EdGraGru1.ValueVariant then
  begin
    FOldNivel1 := EdGraGru1.ValueVariant;
    ReconfiguraGradeQ();
  end;
end;

procedure TFmPediVdaGru2.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmPediVdaGru2.FormCreate(Sender: TObject);
begin
  FPedidoCompra := False;
  ImgTipo.SQLType := stLok;
  //
  ST1.Caption := '';
  ST2.Caption := '';
  ST3.Caption := '';
  ST1.Font.Color := clRed;
  ST2.Font.Color := clRed;
  ST3.Font.Color := clRed;
  ST1.Color := PnSeleciona.Color;
  ST2.Color := PnSeleciona.Color;
  ST3.Color := PnSeleciona.Color;
  //
  PageControl1.ActivePageIndex := 0;
  GradeA.ColWidths[0] := 128;
  GradeC.ColWidths[0] := 128;
  GradeX.ColWidths[0] := 128;
  GradeQ.ColWidths[0] := 128;
  GradeL.ColWidths[0] := 128;
  GradeF.ColWidths[0] := 128;
  GradeD.ColWidths[0] := 128;
  //
  ReopenGraGru1();
end;

procedure TFmPediVdaGru2.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmPediVdaGru2.GradeADrawCell(Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);
begin
  MyObjects.DesenhaGradeA(GradeA, ACol, ARow, Rect, State, True);
end;

procedure TFmPediVdaGru2.GradeCDrawCell(Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);
begin
  MyObjects.DesenhaGradeN(GradeC, GradeA, nil, ACol, ARow, Rect, State,
  '0', 0, 0, True);
end;

procedure TFmPediVdaGru2.GradeDClick(Sender: TObject);
begin
  if (GradeD.Col = 0) or (GradeD.Row = 0) then
    DescontoVariosItensCorTam(GradeD, GradeA, GradeC, GradeX)
end;

procedure TFmPediVdaGru2.GradeDDrawCell(Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);
begin
  MyObjects.DesenhaGradeN(GradeD, GradeA, nil, ACol, ARow, Rect, State,
  '0.0000' + ' %', 0, 0, False);
end;

procedure TFmPediVdaGru2.GradeDKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  MyObjects.PulaCelulaGradeInput(GradeD, GradeC, Key, Shift);
end;

procedure TFmPediVdaGru2.GradeDSelectCell(Sender: TObject; ACol, ARow: Integer;
  var CanSelect: Boolean);
begin
  if (ACol = 0) or (ARow = 0) then
    GradeD.Options := GradeD.Options - [goEditing]
  else
    GradeD.Options := GradeD.Options + [goEditing];
end;

procedure TFmPediVdaGru2.GradeFDrawCell(Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);
begin
  MyObjects.DesenhaGradeN(GradeF, GradeA, GradeL, ACol, ARow, Rect, State,
  Dmod.FStrFmtPrc, 0, 0, False);
end;

procedure TFmPediVdaGru2.GradeFKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  MyObjects.PulaCelulaGradeInput(GradeF, GradeC, Key, Shift);
end;

procedure TFmPediVdaGru2.GradeFSelectCell(Sender: TObject; ACol, ARow: Integer;
  var CanSelect: Boolean);
begin
  if (ACol = 0) or (ARow = 0) then
    GradeF.Options := GradeF.Options - [goEditing]
  else
    GradeF.Options := GradeF.Options + [goEditing];
end;

procedure TFmPediVdaGru2.GradeLDrawCell(Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);
begin
  MyObjects.DesenhaGradeN(GradeL, GradeA, nil, ACol, ARow, Rect, State,
    Dmod.FStrFmtPrc, 0, 0, False);
end;

procedure TFmPediVdaGru2.GradeQDblClick(Sender: TObject);
begin
  if (GradeQ.Col = 0) or (GradeQ.Row = 0) then
    QtdeVariosItensCorTam(GradeQ, GradeA, GradeC, GradeX)
  else
    QtdeItemCorTam(GradeQ, GradeC, GradeX);
end;

procedure TFmPediVdaGru2.GradeQDrawCell(Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);
begin
  MyObjects.DesenhaGradeN(GradeQ, GradeA, nil, ACol, ARow, Rect, State,
  dmkPF.FormataCasas(QrGraGru1Fracio.Value), 0, 0, False);
end;

procedure TFmPediVdaGru2.GradeQKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  MyObjects.PulaCelulaGradeInputEx(GradeQ, GradeC, Key, Shift, BtOK);
end;

procedure TFmPediVdaGru2.GradeQSelectCell(Sender: TObject; ACol, ARow: Integer;
  var CanSelect: Boolean);
begin
  if (ACol = 0) or (ARow = 0) then
    GradeQ.Options := GradeQ.Options - [goEditing]
  else
    GradeQ.Options := GradeQ.Options + [goEditing];
end;

procedure TFmPediVdaGru2.GradeXDrawCell(Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);
begin
  MyObjects.DesenhaGradeN(GradeX, nil, nil, ACol, ARow, Rect, State,
  '0', 0, 0, False);
end;

procedure TFmPediVdaGru2.ImgTipoChange(Sender: TObject);
begin
  if ImgTipo.SQLType in ([stIns, stUpd]) then
  begin
    BtOK.Enabled   := True;
    GradeQ.Options := GradeQ.Options + [goEditing];
    GradeD.Options := GradeD.Options + [goEditing];
  end else begin
    BtOK.Enabled   := False;
    GradeQ.Options := GradeQ.Options - [goEditing];
    GradeD.Options := GradeQ.Options - [goEditing];
  end;
end;

procedure TFmPediVdaGru2.ReconfiguraGradeQ;
var
  MedDDSimpl, MedDDReal, MediaSel, TaxaM, Juros: Double;
  Grade, Nivel1, Tabela, CondicaoPG, Lista: Integer;
begin
  PnJuros.Visible := False;
  Tabela     := FmPediVda2.QrPediVdaTabelaPrc.Value;
  MedDDSimpl := FmPediVda2.QrPediVdaMedDDSimpl.Value;
  MedDDReal  := FmPediVda2.QrPediVdaMedDDReal.Value;
  Grade      := QrGraGru1GraTamCad.Value;
  Nivel1     := QrGraGru1Nivel1.Value;
  CondicaoPG := FmPediVda2.QrPediVdaCondicaoPG.Value;
  //
  if Tabela > 0 then
  begin
    PageControl1.Visible := True;
    DmProd.ConfigGrades5(Grade, Nivel1, Tabela, CondicaoPG,
    GradeA, GradeX, GradeC, GradeL, GradeF, GradeQ, GradeD,
    MedDDSimpl, MedDDReal, ST1, ST2, ST3, ImgTipo, False,
    DmPediVda.QrParamsEmpTipMediaDD.Value, DmPediVda.QrParamsEmpFatSemPrcL.Value);
    //
  end else begin
    QrLista.Close;
    QrLista.Params[00].AsInteger := FmPediVda2.QrPediVdaEmpresa.Value;
    QrLista.Params[01].AsInteger := FmPediVda2.QrPediVdaRegrFiscal.Value;
    UnDmkDAC_PF.AbreQuery(QrLista, Dmod.MyDB);
    if QrLista.RecordCount = 1 then
    begin
      PageControl1.Visible := True;
      Lista := QrListaGraCusPrc.Value;
      case DmPediVda.QrParamsEmpTipMediaDD.Value of
        1: MediaSel := MedDDSimpl;
        2: MediaSel := MedDDReal;
        else MediaSel := 0;
      end;
      TaxaM := FmPediVda2.QrPediVdaJurosMes.Value;
      case DmPediVda.QrParamsEmpTipCalcJuro.Value of
        1: Juros := dmkPF.CalculaJuroSimples(TaxaM, MediaSel);
        2: Juros := dmkPF.CalculaJuroComposto(TaxaM, MediaSel);
        else Juros := 0;
      end;
      EdCustoFin.ValueVariant := Juros;
      PnJuros.Visible := True;
      DmProd.ConfigGrades9(Grade, Nivel1, Lista, FmPediVda2.QrPediVdaEmpresa.Value,
      GradeA, GradeX, GradeC, GradeL, GradeF, GradeQ, GradeD,
      nil, nil, nil, nil, nil,
      Juros, ST1, ST2, ST3, ImgTipo);
    end else begin
      PageControl1.Visible := False;
      //
      MyObjects.LimpaGrade(GradeQ, 0, 0, True);
      MyObjects.LimpaGrade(GradeF, 0, 0, True);
      MyObjects.LimpaGrade(GradeD, 0, 0, True);
      MyObjects.LimpaGrade(GradeL, 0, 0, True);
      MyObjects.LimpaGrade(GradeC, 0, 0, True);
      MyObjects.LimpaGrade(GradeA, 0, 0, True);
      MyObjects.LimpaGrade(GradeX, 0, 0, True);
      //
      if QrLista.RecordCount = 0 then
        Geral.MB_Aviso('Nenhuma lista está definida com o tipo ' +
          'de movimento "Saída" na regra fiscal "' + FmPediVda2.QrPediVdaNOMEFISREGCAD.Value
          + '" para a empresa ' + FormatFloat('000', FmPediVda2.QrPediVdaFilial.Value)
          + '!')
      else
        Geral.MB_Aviso('Há ' + Geral.FF0(QrLista.RecordCount) +
          'listas habilitadas para o movimento "Saída" na regra fiscal "' +
          FmPediVda2.QrPediVdaNOMEFISREGCAD.Value + '" para a empresa ' +
          FormatFloat('000', FmPediVda2.QrPediVdaFilial.Value) + '. Nenhuma será' +
          'considerada!');
    end;
  end;
end;

procedure TFmPediVdaGru2.Regrasfiscais1Click(Sender: TObject);
{$IfNDef NAO_GFAT}
var
  FisRegCad: Integer;
begin
  FisRegCad := FmPediVda2.QrPediVdaRegrFiscal.Value;
  //
  Grade_Jan.MostraFormFisRegCad(FisRegCad);
  try
    Screen.Cursor := crHourGlass;
    ReconfiguraGradeQ();
  finally
    Screen.Cursor := crDefault;
  end;
{$Else}
  Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappGFat);
{$EndIf}
end;

procedure TFmPediVdaGru2.ReopenGraGru1();
begin
  DmProd.ReopenGraGru1(QrGraGru1, RGTipPrd.ItemIndex,
    FmPediVda2.QrPediVdaCodigo.Value, '=', CBGraGru1);
  //
  EdGraGru1.ValueVariant := 0;
  CBGraGru1.KeyValue     := 0;
end;

procedure TFmPediVdaGru2.RGListaClick(Sender: TObject);
begin
  case RGLista.ItemIndex of
    0: CBGraGru1.ListField := 'NOME_EX';
    1: CBGraGru1.ListField := 'Nome';
  end;
end;

procedure TFmPediVdaGru2.RGTipPrdClick(Sender: TObject);
begin
  ReopenGraGru1();
end;

procedure TFmPediVdaGru2.SBMenuClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMMenu, SBMenu);
end;

function TFmPediVdaGru2.ValidaDadosProduto: Boolean;
begin
  Result := True;
  //
  if (QrGraGru1NCM.Value = '') and (DmPediVda.QrParamsEmpPediVdaNElertas.Value = 0) then
  begin
    Result := False;
    //
    Geral.MB_Aviso('O produto selecionado não possui NCM cadastrado!');
    //
    if Geral.MB_Pergunta('Deseja cadastrar agora?') = ID_YES then
    begin
      MostraFormGraGruN(Integer(tsfDadosFiscais));
    end;
    Exit;
  end;
  if (QrGraGru1UnidMed.Value = 0) and (DmPediVda.QrParamsEmpPediVdaNElertas.Value = 0) then
  begin
    Result := False;
    //
    Geral.MB_Aviso('O produto selecionado não possui Unidade de Medida cadastrada!');
    //
    if Geral.MB_Pergunta('Deseja cadastrar agora?') = ID_YES then
    begin
      MostraFormGraGruN(Integer(tsfDadosGerais));
    end;
    Exit;
  end;
end;

function TFmPediVdaGru2.ObtemDesconto(var Desconto: Double): Boolean;
begin
  Result := False;
  if DBCheck.CriaFm(TFmGraGruVal, FmGraGruVal, afmoNegarComAviso) then
  begin
    FmGraGruVal.EdValor.ValueVariant := Desconto;
    FmGraGruVal.ShowModal;
    Result   := FmGraGruVal.FConfirmou;
    Desconto := FmGraGruVal.EdValor.ValueVariant;
    FmGraGruVal.Destroy;
  end;
end;

function TFmPediVdaGru2.ObtemQtde(var Qtde: Double): Boolean;
var
  ResVar: Variant;
  CasasDecimais: Integer;
begin
  Qtde          := 0;
  Result        := False;
  CasasDecimais := QrGraGru1Fracio.Value;
  if MyObjects.GetValorDmk(TFmGetValor, FmGetValor, dmktfDouble,
  0, CasasDecimais, 0, '', '', True, 'Itens', 'Informe a quantidade de itens: ',
  0, ResVar) then
  begin
    Qtde := Geral.DMV(ResVar);
    Result := True;
  end;
end;

procedure TFmPediVdaGru2.Produtos1Click(Sender: TObject);
begin
  MostraFormGraGruN(Integer(tsfNenhum));
end;

procedure TFmPediVdaGru2.MostraFormGraGruN(SubForm: Integer);
{$IfNDef NAO_GFAT}
var
  SubFrm: TSubForm;
  Nivel1: Integer;
begin
  if EdGraGru1.ValueVariant <> 0 then
    Nivel1 := QrGraGru1Nivel1.Value
  else
    Nivel1 := 0;
  //
  if SubForm = 1 then
    SubFrm := tsfDadosGerais
  else if SubForm = 2 then
    SubFrm := tsfDadosFiscais
  else
    SubFrm := tsfNenhum;
  //
  Grade_Jan.MostraFormGraGruN(Nivel1, 0, tpGraCusPrc, SubFrm);
  try
    Screen.Cursor := crHourGlass;
    //
    UnDmkDAC_PF.AbreQuery(QrGraGru1, Dmod.MyDB);
    QrGraGru1.Locate('Nivel1', Nivel1, []);
    //
    ReconfiguraGradeQ();
    ReopenGraGru1;
  finally
    Screen.Cursor := crDefault;
  end;
{$Else}
begin
  Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappGFat);
{$EndIf}
end;

function TFmPediVdaGru2.QtdeItemCorTam(GridP, GridC, GridX: TStringGrid): Boolean;
var
  Qtde: Double;
  c, l, GraGruX, Coluna, Linha: Integer;
begin
  Result  := False;
  Coluna  := GridP.Col;
  Linha   := GridP.Row;
  c       := Geral.IMV(GridX.Cells[Coluna, 0]);
  l       := Geral.IMV(GridX.Cells[0, Linha]);
  GraGruX := Geral.IMV(GridC.Cells[Coluna, Linha]);
  //
  if not ValidaDadosProduto() then Exit;
  //
  Screen.Cursor := crHourGlass;
  try
    if GridX.Cells[Coluna, 0] = '' then
    begin
      Geral.MB_Aviso('Tamanho não definido!');
      Exit;
    end;
    if GridX.Cells[0, Linha] = '' then
    begin
      Geral.MB_Aviso('Cor não definida!');
      Exit;
    end;
    if GraGruX = 0 then
    begin
      Geral.MB_Aviso('O item nunca foi ativado!');
      Exit;
    end;
    if (c = 0) or (l = 0) or (GraGruX = 0) then Exit;
    //
    //Nivel1 := QrGraGru1Nivel1.Value;
    Qtde := Geral.DMV(GridP.Cells[Coluna, Linha]);
    //
    if ObtemQtde(Qtde) then
      GridP.Cells[Coluna, Linha] := FloatToStr(Qtde);
    Result := True;
  finally
    Screen.Cursor := crDefault;
  end;
end;

function TFmPediVdaGru2.QtdeVariosItensCorTam(GridP, GridA, GridC, GridX:
TStringGrid): Boolean;
var
  Coluna, Linha, c, l, Ativos, ColI, ColF, RowI, RowF,
  CountC, CountL, GraGruX: Integer;
  Qtde: Double;
  QtdeTxt: String;
begin
  Result := False;
  Ativos := 0;
  RowI   := 0;
  RowF   := 0;
  ColI   := 0;
  ColF   := 0;
  Coluna := GridP.Col;
  Linha  := GridP.Row;
  //
  if not ValidaDadosProduto() then Exit;
  //
  if (Coluna = 0) and (Linha = 0) then
  begin
    ColI := 1;
    ColF := GridP.ColCount - 1;
    RowI := 1;
    RowF := GridP.RowCount - 1;
  end else if Coluna = 0 then
  begin
    ColI := 1;
    ColF := GridP.ColCount - 1;
    RowI := Linha;
    RowF := Linha;
  end else if Linha = 0 then
  begin
    ColI := Coluna;
    ColF := Coluna;
    RowI := 1;
    RowF := GridP.RowCount - 1;
  end;
  //
  Screen.Cursor := crHourGlass;
  try
    CountC := 0;
    CountL := 0;
    //
    for c := ColI to ColF do
      for l := RowI to RowF do
    begin
      CountC := CountC + Geral.IMV(GridX.Cells[c, 0]);
      CountL := CountL + Geral.IMV(GridX.Cells[0, l]);
    end;
    if CountC = 0 then
    begin
      Geral.MB_Aviso('Tamanho não definido!');
      Exit;
    end;
    if CountL = 0 then
    begin
      Geral.MB_Aviso('Cor não definida!');
      Exit;
    end;
    //
    //Nivel1 := QrGraGru1Nivel1.Value;
    Qtde   := Geral.IMV(GridP.Cells[ColI, RowI]);
    for c := ColI to ColF do
      for l := RowI to RowF do
        Ativos := Ativos + Geral.IMV(GridA.Cells[c, l]);
    if Ativos = 0 then
    begin
      Geral.MB_Aviso('Não há nenhum item com código na seleção ' +
        'para que se possa incluir / alterar o preço!');
      Exit;
    end;
    //
    if ObtemQtde(Qtde) then
    begin
      QtdeTxt := FloatToStr(Qtde);
      {
      if (Coluna = 0) and (Linha = 0) then
      begin
        ItensTxt := 'todo grupo';
      end else if Coluna = 0 then
      begin
        ItensTxt := 'todos tamanhos da cor ' + GridA.Cells[0, Linha];
      end else if Linha = 0 then
      begin
        ItensTxt := 'todas cores do tamanho ' + GridA.Cells[Coluna, 0];
      end;
      if Geral.MB_Pergunta('Confirma a quantidade de ' + QtdeTxt +
        ' para ' + ItensTxt + '?') <> ID_YES then
      begin
        Screen.Cursor := crDefault;
        Exit;
      end;
      //
      }
      for c := ColI to ColF do
        for l := RowI to RowF do
      begin
        GraGruX := Geral.IMV(GridC.Cells[c, l]);
        //
        if GraGruX > 0 then
          GradeQ.Cells[c,l] := QtdeTxt;
      end;
      Result := True;
    end;
  finally
    Screen.Cursor := crDefault;
  end;
end;

end.

