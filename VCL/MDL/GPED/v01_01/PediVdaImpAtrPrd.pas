unit PediVdaImpAtrPrd;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DB, DBGrids, (*&&UnDmkABS_PF,*)
  dmkDBGrid, DBCtrls, mySQLDbTables, Menus, ComCtrls, dmkEdit, dmkEditCB,
  dmkDBLookupComboBox, UnDmkProcFunc, DmkDAC_PF;

type
  THackDBGrid = class(TDBGrid);
  TFmPediVdaImpAtrPrd = class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    BtNenhum: TBitBtn;
    BtTodos: TBitBtn;
    BtPesq: TBitBtn;
    PMPesq: TPopupMenu;
    Ativa1: TMenuItem;
    Desativa1: TMenuItem;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    Panel3: TPanel;
    Label1: TLabel;
    EdPesq: TEdit;
    DBGrid1: TdmkDBGrid;
    Panel4: TPanel;
    DBGrid2: TdmkDBGrid;
    RGSelecao: TRadioGroup;
    Panel5: TPanel;
    LaPVIAtrCli: TLabel;
    CBPVIAtrPrd: TdmkDBLookupComboBox;
    EdPVIAtrPrd: TdmkEditCB;
    procedure FormCreate(Sender: TObject);
    procedure DBGrid1CellClick(Column: TColumn);
    procedure FormStartDock(Sender: TObject; var DragObject: TDragDockObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure EdPesqChange(Sender: TObject);
    procedure FormUnDock(Sender: TObject; Client: TControl;
      NewTarget: TWinControl; var Allow: Boolean);
    procedure BtNenhumClick(Sender: TObject);
    procedure BtTodosClick(Sender: TObject);
    procedure BtPesqClick(Sender: TObject);
    procedure Ativa1Click(Sender: TObject);
    procedure Desativa1Click(Sender: TObject);
    procedure PageControl1Change(Sender: TObject);
    procedure FormDeactivate(Sender: TObject);
    procedure RGSelecaoClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure EdPVIAtrPrdChange(Sender: TObject);
    procedure EdPVIAtrPrdExit(Sender: TObject);
    procedure EdPVIAtrPrdEnter(Sender: TObject);
  private
    { Private declarations }
    FItem: Integer;
    procedure RecarregaItens();
    procedure Pesquisar();
    procedure AtivaItens(Status: Integer);
    procedure AtivaSelecionados(Status: Integer);
  public
    { Public declarations }
    class function CreateDockForm(const aColor: TColor): TCustomForm;
    //class procedure FiltaAtivos();
  end;

var
  FmPediVdaImpAtrPrd: TFmPediVdaImpAtrPrd;

implementation

uses UnMyObjects, Module, UMySQLModule, TabePrcCab, PediVdaImp, ModPediVda, MyGlyfs;

{$R *.DFM}

///// PROCEDURES E FUNCTIONS DO DOCKFORM

procedure TFmPediVdaImpAtrPrd.Ativa1Click(Sender: TObject);
begin
  AtivaSelecionados(1);
end;

procedure TFmPediVdaImpAtrPrd.AtivaItens(Status: Integer);
begin
(*&&
  Screen.Cursor := crHourGlass;
  DmPediVda.QrPVIAtrPrdIts.Close;
  DmPediVda.QrPVIAtrPrdIts.SQL.Clear;
  DmPediVda.QrPVIAtrPrdIts.SQL.Add('UPDATE pviatrprd SET Ativo=' + dmkPF.FFP(Status, 0) + ';');
  DmPediVda.QrPVIAtrPrdIts.SQL.Add('SELECT * FROM pviatrprd;');
  DmkABS_PF.AbreQuery(DmPediVda.QrPVIAtrPrdIts);
  Screen.Cursor := crDefault;
*)
end;

procedure TFmPediVdaImpAtrPrd.AtivaSelecionados(Status: Integer);
begin
(*&&
  Screen.Cursor := crHourGlass;
  DmPediVda.QrPVIAtrPrdIts.First;
  while not DmPediVda.QrPVIAtrPrdIts.Eof do
  begin
    if DmPediVda.QrPVIAtrPrdItsAtivo.Value <> Status then
    begin
      DmPediVda.QrPVIAtrPrdIts.Edit;
      DmPediVda.QrPVIAtrPrdItsAtivo.Value := Status;
      DmPediVda.QrPVIAtrPrdIts.Post;
    end;
    DmPediVda.QrPVIAtrPrdIts.Next;
  end;
  Screen.Cursor := crDefault;
*)
end;

procedure TFmPediVdaImpAtrPrd.BtPesqClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMPesq, BtPesq);
end;

procedure TFmPediVdaImpAtrPrd.BtNenhumClick(Sender: TObject);
begin
  AtivaItens(0);
end;

procedure TFmPediVdaImpAtrPrd.BtTodosClick(Sender: TObject);
begin
  AtivaItens(1);
end;

procedure TFmPediVdaImpAtrPrd.RecarregaItens();
const
  Txt1 = 'INSERT INTO pviatrprd (Codigo,CodUsu,Nome,Ativo) VALUES(';
var
  Txt2: String;
begin
(*&&
  DmPediVda.QrPVIAtrPrdIts.Close;
  DmPediVda.QrPVIAtrPrdIts.SQL.Clear;
  //
  DmPediVda.QrPVIAtrPrdIts.SQL.Add('DELETE FROM pviatrprd; ');
  {
  DmPediVda.QrPVIAtrPrdIts.SQL.Add('DROP TABLE PVIAtrPrd; ');
  DmPediVda.QrPVIAtrPrdIts.SQL.Add('CREATE TABLE PVIAtrPrd (');
  DmPediVda.QrPVIAtrPrdIts.SQL.Add('  Codigo  integer      ,');
  DmPediVda.QrPVIAtrPrdIts.SQL.Add('  CodUsu  integer      ,');
  DmPediVda.QrPVIAtrPrdIts.SQL.Add('  Nome    varchar(50)  ,');
  DmPediVda.QrPVIAtrPrdIts.SQL.Add('  Ativo   smallint      ');
  DmPediVda.QrPVIAtrPrdIts.SQL.Add(');');
  //
  DmPediVda.QrPVIAtrPrdCad.Close;
  DmkABS_PF.AbreQuery(DmPediVda.QrPVIAtrPrdCad);
  }
  DmPediVda.QrAtrPrdSubIts.Close;
  DmPediVda.QrAtrPrdSubIts.Params[0].AsInteger := DmPediVda.QrPVIAtrPrdCadCodigo.Value;
  UnDmkDAC_PF.AbreQuery(DmPediVda.QrAtrPrdSubIts, Dmod.MyDB);
  DmPediVda.QrAtrPrdSubIts.First;
  while not DmPediVda.QrAtrPrdSubIts.Eof do
  begin
    Txt2 :=
      dmkPF.FFP(DmPediVda.QrAtrPrdSubItsCodigo.Value, 0) + ',' +
      dmkPF.FFP(DmPediVda.QrAtrPrdSubItsCodUsu.Value, 0) + ',' +
      '"' + DmPediVda.QrAtrPrdSubItsNome.Value + '",' +
      '0);';
    DmPediVda.QrPVIAtrPrdIts.SQL.Add(Txt1 + Txt2);
    DmPediVda.QrAtrPrdSubIts.Next;
  end;
  //
  DmPediVda.QrPVIAtrPrdIts.SQL.Add('SELECT * FROM pviatrprd;');
  DmkABS_PF.AbreQuery(DmPediVda.QrPVIAtrPrdIts);
  PageControl1.ActivePageIndex := 0;

  if DmPediVda.QrPVIAtrPrdCadCodigo.Value <> 0 then
  begin
    FmPediVdaImp.FPVIAtrPrd_Cod := DmPediVda.QrPVIAtrPrdCadCodigo.Value;
    FmPediVdaImp.FPVIAtrPrd_Txt := DmPediVda.QrPVIAtrPrdCadNome.Value;
  end else begin
    FmPediVdaImp.FPVIAtrPrd_Txt := '';
    FmPediVdaImp.FPVIAtrPrd_Cod := 0;
  end;
*)
end;

class function TFmPediVdaImpAtrPrd.CreateDockForm(const aColor: TColor): TCustomForm;
begin
  result := TFmPediVdaImpAtrPrd.Create(Application);
  //result.Color := aColor;
  //result.Caption := ColorToString(aColor);
  FmMyGlyfs.ConfiguraFormDock(result);
  result.Show;
end;

procedure TFmPediVdaImpAtrPrd.FormClose(Sender: TObject; var Action: TCloseAction);
begin
(*&&
  DmPediVda.QrPVIAtrPrdIts.Close;
  DmPediVda.QrPVIAtrPrdIts.SQL.Clear;
  DmPediVda.QrPVIAtrPrdIts.SQL.Add('DROP TABLE PVIAtrPrd; ');
  DmPediVda.QrPVIAtrPrdIts.ExecSQL;
  //
  try
    ManualFloat(Rect(0, 0, 0, 0));
  except

  end;
  Action := caFree;
*)
end;

procedure TFmPediVdaImpAtrPrd.FormShow(Sender: TObject);
begin
  FmPediVdaImp.BtConfirma.Enabled := False;
end;

procedure TFmPediVdaImpAtrPrd.FormStartDock(Sender: TObject;
  var DragObject: TDragDockObject);
begin
  DragObject := TDragDockObjectEx.Create(Self);
  DragObject.Brush.Color := clAqua; // this will display a red outline
end;


procedure TFmPediVdaImpAtrPrd.FormUnDock(Sender: TObject; Client: TControl;
  NewTarget: TWinControl; var Allow: Boolean);
begin
end;

///// FIM DAS PROCEDURES E FUNCTIONS DO DOCKFORM

procedure TFmPediVdaImpAtrPrd.PageControl1Change(Sender: TObject);
begin
  BtPesq.Enabled := PageControl1.ActivePageIndex = 0;
  case PageControl1.ActivePageIndex of
    0: Pesquisar();
    1: RGSelecaoClick(Self);
  end;
end;

procedure TFmPediVdaImpAtrPrd.Pesquisar();
begin
(*&&
  Screen.Cursor := crHourGlass;
  //
  DmPediVda.QrPVIAtrPrdIts.Filter   := 'Nome LIKE "%' + EdPesq.Text + '%"';
  DmPediVda.QrPVIAtrPrdIts.Filtered := True;
  //
  Screen.Cursor := crDefault;
*)
end;

procedure TFmPediVdaImpAtrPrd.RGSelecaoClick(Sender: TObject);
begin
(*&&
  DmPediVda.QrPVIAtrPrdIts.Filtered := False;
  case RGSelecao.ItemIndex of
    0: DmPediVda.QrPVIAtrPrdIts.Filter := 'Ativo=0';
    1: DmPediVda.QrPVIAtrPrdIts.Filter := 'Ativo=1';
    2: DmPediVda.QrPVIAtrPrdIts.Filter := '';
  end;
  if RGSelecao.ItemIndex < 2 then
    DmPediVda.QrPVIAtrPrdIts.Filtered := True;
*)
end;

procedure TFmPediVdaImpAtrPrd.DBGrid1CellClick(Column: TColumn);
var
  Status: Byte;
begin
(*&&
  if Column.FieldName = 'Ativo' then
  begin
    if DmPediVda.QrPVIAtrPrdIts.FieldByName('Ativo').Value = 1 then
      Status := 0
    else
      Status := 1;
    //
    DmPediVda.QrPVIAtrPrdIts.Edit;
    DmPediVda.QrPVIAtrPrdIts.FieldByName('Ativo').Value := Status;
    DmPediVda.QrPVIAtrPrdIts.Post;
  end;
  DmPediVda.QrPVIAtrPrdAti.Close;
  DmkABS_PF.AbreQuery(DmPediVda.QrPVIAtrPrdAti);
  {
  Habilita :=  DmPediVda.QrPVIAtrPrdAtiItens.Value = 0;
  FmPediVdaImp.LaPVIAtrPrd.Enabled := Habilita;
  FmPediVdaImp.EdPVIAtrPrd.Enabled := Habilita;
  FmPediVdaImp.CBPVIAtrPrd.Enabled := Habilita;
  if not Habilita then
  begin
    FmPediVdaImp.EdPVIAtrPrd.ValueVariant := 0;
    FmPediVdaImp.CBPVIAtrPrd.KeyValue     := 0;
  end;
  }
*)
end;

procedure TFmPediVdaImpAtrPrd.Desativa1Click(Sender: TObject);
begin
  AtivaSelecionados(0);
end;

procedure TFmPediVdaImpAtrPrd.EdPesqChange(Sender: TObject);
begin
  Pesquisar();
end;

procedure TFmPediVdaImpAtrPrd.EdPVIAtrPrdChange(Sender: TObject);
begin
  if not EdPVIAtrPrd.Focused then
    RecarregaItens();
end;

procedure TFmPediVdaImpAtrPrd.EdPVIAtrPrdEnter(Sender: TObject);
begin
  FItem := EdPVIAtrPrd.ValueVariant;
end;

procedure TFmPediVdaImpAtrPrd.EdPVIAtrPrdExit(Sender: TObject);
begin
  if FItem <> EdPVIAtrPrd.ValueVariant then
  begin
    FItem := EdPVIAtrPrd.ValueVariant;
    RecarregaItens();
  end;
end;

procedure TFmPediVdaImpAtrPrd.FormCreate(Sender: TObject);
const
  Txt1 = 'INSERT INTO pviatrprd (Codigo,CodUsu,Nome,Ativo) VALUES(';
begin
(*&&
  DmPediVda.QrPVIAtrPrdIts.Close;
  DmPediVda.QrPVIAtrPrdIts.SQL.Clear;
  DmPediVda.QrPVIAtrPrdIts.SQL.Add('DROP TABLE PVIAtrPrd; ');
  DmPediVda.QrPVIAtrPrdIts.SQL.Add('CREATE TABLE PVIAtrPrd (');
  DmPediVda.QrPVIAtrPrdIts.SQL.Add('  Codigo  integer      ,');
  DmPediVda.QrPVIAtrPrdIts.SQL.Add('  CodUsu  integer      ,');
  DmPediVda.QrPVIAtrPrdIts.SQL.Add('  Nome    varchar(50)  ,');
  DmPediVda.QrPVIAtrPrdIts.SQL.Add('  Ativo   smallint      ');
  DmPediVda.QrPVIAtrPrdIts.SQL.Add(');');
  //
  DmPediVda.QrPVIAtrPrdCad.Close;
  UnDmkDAC_PF.AbreQuery(DmPediVda.QrPVIAtrPrdCad, Dmod.MyDB);
  {
  while not DmPediVda.QrPVIAtrPrdCad.Eof do
  begin
    Txt2 :=
      dmkPF.FFP(DmPediVda.QrPVIAtrPrdCadCodigo.Value, 0) + ',' +
      dmkPF.FFP(DmPediVda.QrPVIAtrPrdCadCodUsu.Value, 0) + ',' +
      '"' + DmPediVda.QrPVIAtrPrdCadNome.Value + '",' +
      '0);';
  //for i  := 1 to 10 do
    DmPediVda.QrPVIAtrPrdIts.SQL.Add(Txt1 + Txt2);
    DmPediVda.QrPVIAtrPrdCad.Next;
  end;
  //
  }
  DmPediVda.QrPVIAtrPrdIts.SQL.Add('SELECT * FROM pviatrprd;');
  DmkABS_PF.AbreQuery(DmPediVda.QrPVIAtrPrdIts);
  PageControl1.ActivePageIndex := 0;
*)
end;

procedure TFmPediVdaImpAtrPrd.FormDeactivate(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 1;
  RGSelecao.ItemIndex := 1;
  //
  if FindWindow('TFmPediVdaImp', nil) > 0 then
    FmPediVdaImp.AtivaBtConfirma();
end;

end.

