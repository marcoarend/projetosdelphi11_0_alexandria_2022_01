unit PediVdaAlt;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, dmkEdit, dmkLabel, dmkImage, UnDmkProcFunc,
  UnDmkEnums;

type
  TFmPediVdaAlt = class(TForm)
    Panel1: TPanel;
    Panel3: TPanel;
    Panel4: TPanel;
    GroupBox1: TGroupBox;
    Label3: TLabel;
    EdCodigo: TdmkEdit;
    EdGraGruX: TdmkEdit;
    Label4: TLabel;
    Label5: TLabel;
    EdControle: TdmkEdit;
    GroupBox2: TGroupBox;
    Label1: TLabel;
    EdQtdeMin: TdmkEdit;
    Label6: TLabel;
    EdPrecoO: TdmkEdit;
    Label8: TLabel;
    EdQuantC_A: TdmkEdit;
    EdQuantV: TdmkEdit;
    Label9: TLabel;
    GroupBox3: TGroupBox;
    EdQuantP: TdmkEdit;
    Label2: TLabel;
    LaPrecoR: TLabel;
    EdPrecoR: TdmkEdit;
    EdDescoP: TdmkEdit;
    LaDescoP: TLabel;
    GroupBox4: TGroupBox;
    EdDescoV: TdmkEdit;
    EdValLiq: TdmkEdit;
    Label13: TLabel;
    Label12: TLabel;
    Label11: TLabel;
    EdValBru: TdmkEdit;
    EdQuantC_D: TdmkEdit;
    Label7: TLabel;
    EdPrecoF: TdmkEdit;
    Label10: TLabel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel5: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel6: TPanel;
    PnSaiDesis: TPanel;
    BtOK: TBitBtn;
    BtSaida: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure EdQuantPChange(Sender: TObject);
    procedure EdPrecoRChange(Sender: TObject);
    procedure EdDescoPChange(Sender: TObject);
    procedure EdQuantVChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    procedure CalculaValores();
    procedure AtualizaQuantMin();
  public
    { Public declarations }
  end;

  var
  FmPediVdaAlt: TFmPediVdaAlt;

implementation

uses UnMyObjects, dmkGeral, ModPediVda, Module, UMySQLModule, PediVda;

{$R *.DFM}

procedure TFmPediVdaAlt.AtualizaQuantMin();
begin
  EdQtdeMin.ValueVariant := EdQuantC_D.ValueVariant + EdQuantV.ValueVariant;
end;

procedure TFmPediVdaAlt.BtOKClick(Sender: TObject);
var
  Controle, Codigo, GraGruX: Integer;
begin
  if EdQuantP.ValueVariant < EdQtdeMin.ValueVariant then
  begin
    Geral.MB_Aviso('Quantidade informada abaixo do m�nimo!');
    Exit;
  end;

  //
  if EdQuantP.ValueVariant < (EdQuantC_D.ValueVariant + EdQuantV.ValueVariant) then
  begin
    Geral.MB_Aviso('Quantidade informada � menor que soma de vendidos e cancelados!');
    Exit;
  end;

  //

  if (ImgTipo.SQLType = stUpd) and (EdQuantP.ValueVariant = 0) then
  begin
    //
    if Geral.MB_Pergunta('Confirma a EXCLUS�O do item?') = ID_YES then
    begin
      Codigo  := EdCodigo.ValueVariant;
      GraGruX := EdGraGruX.ValueVariant;
      //
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('DELETE FROM pedivdaits WHERE ');
      Dmod.QrUpd.SQL.Add('Codigo=:P0 AND GraGruX=:P1');
      Dmod.QrUpd.Params[00].AsInteger := Codigo;
      Dmod.QrUpd.Params[01].AsInteger := GraGruX;
      Dmod.QrUpd.ExecSQL;
      //
      DmPediVda.AtzSdosPedido(Codigo);
      FmPediVda.LocCod(Codigo, Codigo);
      FmPediVda.ReopenPediVdaGru(GraGruX);
      //
      Close;
    end;
  end else
  begin
    Controle := UMyMod.BuscaEmLivreY_Def('pedivdaits', 'Controle', ImgTipo.SQLType,
      EdControle.ValueVariant);
    EdControle.ValueVariant := Controle;
    if UMyMod.ExecSQLInsUpdFm(FmPediVdaAlt, ImgTipo.SQLType, 'pedivdaits', Controle,
    Dmod.QrUpd) then
    begin
      (* 2018-02-02 => N�o precisa atualizar tudo pois s� edita um item
      DmPediVda.AtualizaTodosItensPediVda_(FmPediVda.QrPediVdaCodigo.Value);
      *)
      DmPediVda.AtualizaUmItemPediVda(Controle);
      DmPediVda.AtzSdosPedido(FmPediVda.QrPediVdaCodigo.Value);
      FmPediVda.LocCod(FmPediVda.QrPediVdaCodigo.Value, FmPediVda.QrPediVdaCodigo.Value);
      FmPediVda.ReopenPediVdaGru(EdGraGruX.ValueVariant);
      Close;
    end;
  end;
end;

procedure TFmPediVdaAlt.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmPediVdaAlt.CalculaValores;
var
  QuantP, PrecoR, DescoP, ValCal, DescoV, ValBru, ValLiq, PrecoF: Double;
begin
  QuantP := EdQuantP.ValueVariant;
  PrecoR := EdPrecoR.ValueVariant;
  DescoP := EdDescoP.ValueVariant;
  //
  //PrecoO := Geral.DMV(GradeL.Cells[Col, Row]);
  //PrecoR := Geral.DMV(GradeF.Cells[Col, Row]);
  //QuantP := Geral.DMV(GradeQ.Cells[Col, Row]);
  ValCal := PrecoR * QuantP;
  //DescoP := Geral.DMV(GradeD.Cells[Col, Row]);
  DescoV := dmkPF.FFF(ValCal * DescoP / 100, Dmod.QrControleCasasProd.Value, siPositivo);
  ValBru := dmkPF.FFF(ValCal, Dmod.QrControleCasasProd.Value, siPositivo);
  ValLiq := ValBru - DescoV;
  if QuantP = 0 then
    PrecoF := 0
  else
    PrecoF := Round(ValLiq / QuantP * 100) / 100;
  ValLiq := PrecoF * QuantP;
  //
  EdValBru.ValueVariant := ValBru;
  EdDescoV.ValueVariant := DescoV;
  EdValLiq.ValueVariant := ValLiq;
  EdPrecoF.ValueVariant := PrecoF;
end;

procedure TFmPediVdaAlt.EdDescoPChange(Sender: TObject);
begin
  CalculaValores();
end;

procedure TFmPediVdaAlt.EdPrecoRChange(Sender: TObject);
begin
  CalculaValores();
end;

procedure TFmPediVdaAlt.EdQuantPChange(Sender: TObject);
begin
  CalculaValores();
  AtualizaQuantMin();
end;

procedure TFmPediVdaAlt.EdQuantVChange(Sender: TObject);
begin
  AtualizaQuantMin();
end;

procedure TFmPediVdaAlt.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //
  EdQuantC_D.DecimalSize := FmPediVda.QrPediVdaGruFracio.Value;
  EdQuantP.DecimalSize   := FmPediVda.QrPediVdaGruFracio.Value;
  EdValLiq.DecimalSize   := Dmod.FFmtPrc;
  EdPrecoF.DecimalSize   := Dmod.FFmtPrc;
  EdPrecoR.DecimalSize   := Dmod.FFmtPrc;

  if ImgTipo.SQLType = stUpd then
  begin
    if DmPediVda.QrItemPVIQuantV.Value > 0 then
    begin
      EdDescoP.Enabled := False;
      EdPrecoR.Enabled := False;
      LaDescoP.Enabled := False;
      LaPrecoR.Enabled := False;
    end;
  end else begin
    EdQtdeMin.ValueVariant := 1;
    EdCodigo.ValueVariant  := FmPediVda.QrPediVdaCodigo.Value;
    EdGraGruX.ValueVariant := Geral.IMV(
      FmPediVda.GradeC.Cells[FmPediVda.GradeQ.Col, FmPediVda.GradeQ.Row]);
  end;
end;

procedure TFmPediVdaAlt.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
end;

procedure TFmPediVdaAlt.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
    [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

end.

