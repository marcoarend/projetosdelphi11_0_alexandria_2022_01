unit SoliComprSel;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, DBCtrls, Db, (*DBTables,*) Grids, DBGrids, ExtCtrls,
  dmkGeral, mySQLDbTables, Variants, dmkImage, UnDmkEnums, DmkDAC_PF, dmkEdit,
  Vcl.ComCtrls, dmkDBGrid;

type
  TFmSoliComprSel = class(TForm)
    QrFornece: TmySQLQuery;
    DsFornece: TDataSource;
    Panel2: TPanel;
    QrForneceCodigo: TIntegerField;
    QrForneceNOMEENTIDADE: TWideStringField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel1: TPanel;
    PnSaiDesis: TPanel;
    BtConfirma: TBitBtn;
    BtDesiste: TBitBtn;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    Panel3: TPanel;
    TabSheet2: TTabSheet;
    PainelDados: TPanel;
    Label1: TLabel;
    DBGrid1: TDBGrid;
    Panel5: TPanel;
    EdCodUsu: TdmkEdit;
    Label2: TLabel;
    EdReferencia: TdmkEdit;
    SpeedButton1: TSpeedButton;
    SpeedButton2: TSpeedButton;
    QrSoliComprCab: TMySQLQuery;
    QrSoliComprCabCodigo: TIntegerField;
    QrSoliComprCabCodUsu: TIntegerField;
    QrSoliComprCabCliente: TIntegerField;
    QrSoliComprCabDtaEntra: TDateField;
    QrSoliComprCabDtaPrevi: TDateField;
    QrSoliComprCabObserva: TWideMemoField;
    QrSoliComprCabQuantS: TFloatField;
    QrSoliComprCabValLiq: TFloatField;
    QrSoliComprCabReferenPedi: TWideStringField;
    QrSoliComprCabNO_ENT: TWideStringField;
    DsSoliComprCab: TDataSource;
    DBMemo1: TDBMemo;
    QrSoliComprGru: TMySQLQuery;
    QrSoliComprGruCodUsu: TIntegerField;
    QrSoliComprGruNome: TWideStringField;
    QrSoliComprGruNivel1: TIntegerField;
    QrSoliComprGruGRATAMCAD: TIntegerField;
    QrSoliComprGruQuantS: TFloatField;
    QrSoliComprGruValLiq: TFloatField;
    QrSoliComprGruItensCustomizados: TFloatField;
    QrSoliComprGruFracio: TSmallintField;
    QrSoliComprGruControle: TIntegerField;
    QrSoliComprGruValBru: TFloatField;
    QrSoliComprGruPrecoF: TFloatField;
    DsSoliComprGru: TDataSource;
    DBGGru: TdmkDBGrid;
    QrSoliComprGruSaldoQtd: TFloatField;
    QrSoliComprIts: TMySQLQuery;
    QrSoliComprItsCodigo: TIntegerField;
    QrSoliComprItsControle: TIntegerField;
    QrSoliComprItsGraGruX: TIntegerField;
    QrSoliComprItsPrecoO: TFloatField;
    QrSoliComprItsPrecoR: TFloatField;
    QrSoliComprItsQuantS: TFloatField;
    QrSoliComprItsQuantC: TFloatField;
    QrSoliComprItsQuantP: TFloatField;
    QrSoliComprItsValBru: TFloatField;
    QrSoliComprItsDescoP: TFloatField;
    QrSoliComprItsDescoV: TFloatField;
    QrSoliComprItsValLiq: TFloatField;
    QrSoliComprItsPrecoF: TFloatField;
    QrSoliComprItsMedidaC: TFloatField;
    QrSoliComprItsMedidaL: TFloatField;
    QrSoliComprItsMedidaA: TFloatField;
    QrSoliComprItsMedidaE: TFloatField;
    QrSoliComprItsPercCustom: TFloatField;
    QrSoliComprItsCustomizad: TSmallintField;
    QrSoliComprItsInfAdCuztm: TIntegerField;
    QrSoliComprItsOrdem: TIntegerField;
    QrSoliComprItsReferencia: TWideStringField;
    QrSoliComprItsMulti: TIntegerField;
    QrSoliComprItsvProd: TFloatField;
    QrSoliComprItsvFrete: TFloatField;
    QrSoliComprItsvSeg: TFloatField;
    QrSoliComprItsvOutro: TFloatField;
    QrSoliComprItsvDesc: TFloatField;
    QrSoliComprItsvBC: TFloatField;
    BtAtzSdos: TBitBtn;
    PB1: TProgressBar;
    QrSoliComprCabCentroCusto: TIntegerField;
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure DBGrid1DblClick(Sender: TObject);
    procedure QrSoliComprGruAfterOpen(DataSet: TDataSet);
    procedure QrSoliComprGruBeforeClose(DataSet: TDataSet);
    procedure BtAtzSdosClick(Sender: TObject);
  private
    { Private declarations }
    FConfirma: Boolean;
    procedure ReopenSoliCompr(CodUsu: Integer; Referencia: String);
    procedure ReopenSoliComprIts(Codigo: Integer);
  public
    { Public declarations }
  end;

var
  FmSoliComprSel: TFmSoliComprSel;

implementation

uses UnMyObjects, Module, ModPediVda, PediVda2;

{$R *.DFM}

procedure TFmSoliComprSel.BtDesisteClick(Sender: TObject);
begin
  Close;
end;

procedure TFmSoliComprSel.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  FConfirma := False;
  //
  QrFornece.Params[0].AsInteger := 2;
  UnDmkDAC_PF.AbreQuery(QrFornece, Dmod.MyDB);
  PageControl1.ActivePageIndex := 0;
end;

procedure TFmSoliComprSel.DBGrid1DblClick(Sender: TObject);
begin
  if (QrSoliComprCab.State <> dsInactive) and (QrSoliComprCab.RecordCount > 0) then
  begin
    FConfirma := True;
    UnDmkDAC_PF.AbreMySQLQuery0(QrSoliComprGru, Dmod.MyDB, [
    'SELECT pvi.Controle, SUM(QuantS) QuantS, ',
    'SUM(ValLiq) ValLiq, SUM(ValBru) ValBru, ',
    'SUM(Customizad) ItensCustomizados, ',
    'gti.Codigo GRATAMCAD, ',
    'gg1.CodUsu, gg1.Nome, gg1.Nivel1, pgt.Fracio, ',
    'IF(QuantS <= 0, 0.00, vProd / QuantS) PrecoF, ',
    'SUM(QuantS - QuantP) SaldoQtd ',
    'FROM solicomprits pvi ',
    'LEFT JOIN gragrux   ggx ON ggx.Controle=pvi.GraGruX ',
    'LEFT JOIN gratamits gti ON gti.Controle=ggx.GraTamI ',
    'LEFT JOIN gragru1   gg1 ON gg1.Nivel1=ggx.GraGru1 ',
    'LEFT JOIN prdgruptip   pgt ON pgt.Codigo=gg1.PrdGrupTip ',
    'WHERE pvi.Codigo=' + Geral.FF0(QrSoliComprCabCodigo.Value),
    'GROUP BY gg1.Nivel1 ',
    'ORDER BY pvi.Controle ',
    '']);
    //
    PageControl1.ActivePageIndex := 1;
  end;
end;

procedure TFmSoliComprSel.BtAtzSdosClick(Sender: TObject);
var
  SoliCompr: Integer;
begin
  SoliCompr := QrSoliComprCabCodigo.Value;
  DmPediVda.AtualizaTodosItensSoliCompr_(SoliCompr, PB1);
  ReopenSoliComprIts(SoliCompr);
end;

procedure TFmSoliComprSel.BtConfirmaClick(Sender: TObject);
var
  SoliCompr : Integer;
begin
  if (QrSoliComprCab.State <> dsInactive) and (QrSoliComprCab.RecordCount > 0) and
  (QrSoliComprGru.State <> dsInactive) and (QrSoliComprGru.RecordCount > 0) then
    SoliCompr := QrSoliComprCabCodigo.Value
  else
    SoliCompr := 0;
  //
  if SoliCompr = 0 then
  begin
    Geral.MB_Aviso('Defina a solicita��o!');
    PageControl1.ActivePageIndex := 0;
    Exit;
  end else
  begin
    ReopenSoliComprIts(SoliCompr);
    if QrSoliComprIts.RecordCount = 0 then
    begin
      Geral.MB_Aviso('Esta solicita��o n�o tem mais nenhum item com saldo!');
      Exit;
    end;
    Hide;
    //FmPQE.InsereRegCondicionado2(SoliCompr);
    FmPediVda2.IncluiNovoPedido(
     QrSoliComprCabCodigo.Value, QrSoliComprCabReferenPedi.Value,
       QrSoliComprCabCentroCusto.Value);
    Close;
  end;
end;

procedure TFmSoliComprSel.QrSoliComprGruAfterOpen(DataSet: TDataSet);
begin
  BtConfirma.Enabled := QrSoliComprGru.RecordCount > 0;
  BtAtzSdos.Enabled  := QrSoliComprGru.RecordCount > 0;
end;

procedure TFmSoliComprSel.QrSoliComprGruBeforeClose(DataSet: TDataSet);
begin
  BtConfirma.Enabled := False;
  BtAtzSdos.Enabled  := False;
end;

procedure TFmSoliComprSel.ReopenSoliCompr(CodUsu: Integer; Referencia: String);
var
  SQL_Pesq: String;
begin
  if CodUsu <> 0 then
    SQL_Pesq := 'AND pvd.CodUsu=' + Geral.FF0(CodUsu)
  else
    SQL_Pesq := 'AND ReferenPedi LIKE "' + Referencia + '" ';
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrSoliComprCab, Dmod.MyDB, [
  'SELECT pvd.Codigo, pvd.CodUsu, pvd.Cliente,  ',
  'pvd.DtaEntra, pvd.DtaPrevi, pvd.Observa,  ',
  'pvd.QuantS, pvd.ValLiq, pvd.ReferenPedi, ',
  'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_ENT, ',
  'pvd.CentroCusto ',
  'FROM solicomprcab pvd ',
  'LEFT JOIN entidades ent ON ent.Codigo=pvd.Cliente ',
  'WHERE pvd.EntSai=0 ',
  SQL_Pesq,
  '']);
end;

procedure TFmSoliComprSel.ReopenSoliComprIts(Codigo: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrSoliComprIts, Dmod.MyDB, [
  'SELECT * ',
  'FROM solicomprits',
  'WHERE Codigo=' + Geral.FF0(Codigo),
  'AND QuantS > QuantP ',
  'ORDER BY Controle ',
  '']);
end;

procedure TFmSoliComprSel.SpeedButton1Click(Sender: TObject);
begin
  ReopenSoliCompr(EdCodUsu.ValueVariant, '');
end;

procedure TFmSoliComprSel.SpeedButton2Click(Sender: TObject);
begin
  ReopenSoliCompr(0, EdReferencia.ValueVariant);
end;

procedure TFmSoliComprSel.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmSoliComprSel.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

end.
