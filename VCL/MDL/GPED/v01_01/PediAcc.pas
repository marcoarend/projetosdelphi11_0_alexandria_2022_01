unit PediAcc;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls, Db, (*DBTables,*) JPEG, ExtDlgs,
  ZCF2, ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt, DmkDAC_PF,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, dmkGeral,
  dmkPermissoes, dmkEdit, dmkLabel, dmkRadioGroup, Grids, DBGrids, dmkDBGrid,
  Menus, dmkDBLookupComboBox, dmkEditCB, UnDmkProcFunc, dmkImage, UnDmkEnums;

type
  TFmPediAcc = class(TForm)
    PainelDados: TPanel;
    DsPediAcc: TDataSource;
    QrPediAcc: TmySQLQuery;
    PainelEdita: TPanel;
    PainelConfirma: TPanel;
    BtConfirma: TBitBtn;
    PainelControle: TPanel;
    LaRegistro: TLabel;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    Panel3: TPanel;
    PnCabeca: TPanel;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    dmkPermissoes1: TdmkPermissoes;
    PMAgente: TPopupMenu;
    QrPediAccTab: TmySQLQuery;
    DsPediAccTab: TDataSource;
    BtAgente: TBitBtn;
    QrPediAccTabControle: TIntegerField;
    QrPediAccTabDias: TIntegerField;
    QrPediAccTabPercent1: TFloatField;
    QrPediAccTabPercent2: TFloatField;
    QrPediAccTabPERCENTT: TFloatField;
    BtFiliais: TBitBtn;
    QrPediAccReg: TmySQLQuery;
    DsPediAccReg: TDataSource;
    Ativanovoagente1: TMenuItem;
    Alteradadosdoagenteatual1: TMenuItem;
    Desativaagenteatual1: TMenuItem;
    QrPediAccCodigo: TIntegerField;
    QrPediAccSupervisor: TIntegerField;
    QrPediAccAtivo: TSmallintField;
    QrPediAccNOMEACC: TWideStringField;
    QrPediAccNOMESUP: TWideStringField;
    Panel6: TPanel;
    Label4: TLabel;
    Label5: TLabel;
    DBEdit04: TDBEdit;
    Panel7: TPanel;
    Label7: TLabel;
    EdSupervisor: TdmkEditCB;
    CBSupervisor: TdmkDBLookupComboBox;
    QrSupervisores: TmySQLQuery;
    QrSupervisoresCodigo: TIntegerField;
    QrSupervisoresNOMEENT: TWideStringField;
    DsSupervisores: TDataSource;
    EdCodigo: TdmkEdit;
    QrPediAccRegCodUsu: TIntegerField;
    QrPediAccRegNome: TWideStringField;
    QrPediAccRegCodigo: TIntegerField;
    dmkDBGrid1: TdmkDBGrid;
    Label1: TLabel;
    DBEdCodigo: TDBEdit;
    DBEdNome: TDBEdit;
    Label3: TLabel;
    DBEdit02: TDBEdit;
    DBEdit01: TDBEdit;
    Panel4: TPanel;
    GroupBox1: TGroupBox;
    SbSupervisor: TSpeedButton;
    QrContas: TmySQLQuery;
    DsContas: TDataSource;
    DsCarteiras: TDataSource;
    QrCarteiras: TmySQLQuery;
    QrContasCodigo: TIntegerField;
    QrContasNome: TWideStringField;
    QrCarteirasCodigo: TIntegerField;
    QrCarteirasNome: TWideStringField;
    GroupBox2: TGroupBox;
    QrPediAccCartFin: TIntegerField;
    QrPediAccContaFin: TIntegerField;
    QrPediAccNO_CART: TWideStringField;
    QrPediAccNO_CONTA: TWideStringField;
    Panel8: TPanel;
    Label8: TLabel;
    EdCartFin: TdmkEditCB;
    CBCartFin: TdmkDBLookupComboBox;
    SbCartFin: TSpeedButton;
    EdContaFin: TdmkEditCB;
    Label9: TLabel;
    CBContaFin: TdmkDBLookupComboBox;
    SBContaFin: TSpeedButton;
    RGTpEmiCoRec: TdmkRadioGroup;
    QrPediAccTpEmiCoRec: TIntegerField;
    Panel9: TPanel;
    Label6: TLabel;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    Label10: TLabel;
    DBEdit3: TDBEdit;
    DBEdit4: TDBEdit;
    dmkRadioGroup1: TDBRadioGroup;
    Label2: TLabel;
    Label11: TLabel;
    EdPerComissR: TdmkEdit;
    EdPerComissF: TdmkEdit;
    QrGraGru1PerComissF: TFloatField;
    QrGraGru1PerComissR: TFloatField;
    Label55: TLabel;
    DBEdit51: TDBEdit;
    DBEdit52: TDBEdit;
    Label56: TLabel;
    Panel10: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel11: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel12: TPanel;
    PnSaiDesis: TPanel;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrPediAccAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrPediAccBeforeOpen(DataSet: TDataSet);
    procedure SbNovoClick(Sender: TObject);
    procedure BtAgenteClick(Sender: TObject);
    procedure PMAgentePopup(Sender: TObject);
    procedure QrPediAccBeforeClose(DataSet: TDataSet);
    procedure QrPediAccAfterScroll(DataSet: TDataSet);
    procedure BtFiliaisClick(Sender: TObject);
    procedure Alteradadosdoagenteatual1Click(Sender: TObject);
    procedure Ativanovoagente1Click(Sender: TObject);
    procedure SbSupervisorClick(Sender: TObject);
    procedure SbCartFinClick(Sender: TObject);
    procedure SBContaFinClick(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
   ////Procedures do form
    procedure MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo: Integer);
    procedure DefParams;
    procedure Va(Para: TVaiPara);
  public
    { Public declarations }
    procedure LocCod(Atual, Codigo: Integer);
    procedure ReopenPediAccTab(Controle: Integer);
    procedure ReopenPediAccReg(Codigo: Integer);
  end;

var
  FmPediAcc: TFmPediAcc;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, MyDBCheck, MasterSelFilial, PediAccAti,
{$IfDef cDbTable}
  Regioes,
{$Else}
  //  TODO Berlin
{$EndIf}
  ModuleGeral, UnFinanceiroJan;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmPediAcc.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmPediAcc.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrPediAccCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmPediAcc.DefParams;
begin
  VAR_GOTOTABELA := 'PediAcc';
  VAR_GOTOMYSQLTABLE := QrPediAcc;
  VAR_GOTONEG := gotoAll;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT pac.Codigo, pac.Supervisor, pac.Ativo,');
  VAR_SQLx.Add('pac.PerComissF, pac.PerComissR, ');
  VAR_SQLx.Add('IF(acc.Tipo=0, acc.RazaoSocial, acc.Nome) NOMEACC,');
  VAR_SQLx.Add('IF(sup.Tipo=0, sup.RazaoSocial, sup.Nome) NOMESUP,');
  VAR_SQLx.Add('pac.CartFin, pac.ContaFin, pac.TpEmiCoRec,');
  VAR_SQLx.Add('crt.Nome NO_CART, cta.Nome NO_CONTA');
  VAR_SQLx.Add('FROM pediacc pac');
  VAR_SQLx.Add('LEFT JOIN entidades acc ON acc.Codigo=pac.Codigo');
  VAR_SQLx.Add('LEFT JOIN entidades sup ON sup.Codigo=pac.Supervisor');
  VAR_SQLx.Add('LEFT JOIN carteiras crt ON crt.Codigo=pac.CartFin');
  VAR_SQLx.Add('LEFT JOIN contas    cta ON cta.Codigo=pac.ContaFin');
  VAR_SQLx.Add('');
  VAR_SQLx.Add('WHERE pac.Codigo > -1000');
  //
  VAR_SQL1.Add('AND pac.Codigo=:P0');
  //
  //VAR_SQL2.Add('AND pac.CodUsu=:P0');
  //
  VAR_SQLa.Add('AND IF(acc.Tipo=0, acc.RazaoSocial, acc.Nome) LIKE :P0');
  //
end;

procedure TFmPediAcc.MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo: Integer);
begin
  case Mostra of
    0:
    begin
      PainelControle.Visible:=True;
      PainelDados.Visible := True;
      PainelEdita.Visible := False;
    end;
    1:
    begin
      {
      PainelEdita.Visible := True;
      PainelDados.Visible := False;
      PainelControle.Visible:=False;
      if Status = stIns then
      begin
        EdCodigo.Text := FormatFloat(FFormatFloat, Codigo);
        EdNome.Text := '';
        //...
      end else begin
        EdCodigo.Text := DBEdCodigo.Text;
        EdNome.Text := DBEdNome.Text;
        //...
      end;
      EdNome.SetFocus;
      }
    end;
    else Application.MessageBox('A��o de Inclus�o/altera��o n�o definida!',
    'Aviso', MB_OK+MB_ICONWARNING);
  end;
  ImgTipo.SQLType := SQLType;
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmPediAcc.PMAgentePopup(Sender: TObject);
begin
  Alteradadosdoagenteatual1.Enabled :=
    (QrPediAcc.State <> dsInactive) and (QrPediAcc.RecordCount > 0);
end;

procedure TFmPediAcc.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmPediAcc.QueryPrincipalAfterOpen;
begin
end;

procedure TFmPediAcc.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmPediAcc.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmPediAcc.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmPediAcc.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmPediAcc.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmPediAcc.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrPediAccCodigo.Value;
  Close;
end;

procedure TFmPediAcc.Alteradadosdoagenteatual1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PainelEdita, QrPediAcc, [PainelDados],
  [PainelEdita], EdSupervisor, ImgTipo, 'pediacc');
end;

procedure TFmPediAcc.Ativanovoagente1Click(Sender: TObject);
var
  Cadastro: Integer;
begin
  if DBCheck.CriaFm(TFmPediAccAti, FmPediAccAti, afmoNegarComAviso) then
  begin
    FmPediAccAti.ShowModal;
    Cadastro := FmPediAccAti.FCadastro;
    FmPediAccAti.Destroy;
    //
    if Cadastro <> 0 then
      Alteradadosdoagenteatual1Click(Self);
  end;
end;

procedure TFmPediAcc.BtAgenteClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMAgente, BtAgente);
end;

procedure TFmPediAcc.BtConfirmaClick(Sender: TObject);
var
  Codigo: Integer;
begin
  Codigo := QrPediAccCodigo.Value;
  {
  Supervisor := Geral.IMV(EdSupervisor.Text);
  if Supervisor = 0 then
  begin
    Application.MessageBox('Defina um descri��o.', 'Erro', MB_OK+MB_ICONERROR);
    Exit;
  end;
  Codigo := UMyMod.BuscaEmLivreY_Def('PediAcc', 'Codigo', ImgTipo.SQLType,
    QrPediAccCodigo.Value);
  }
  if UMyMod.ExecSQLInsUpdPanel(ImgTipo.SQLType, FmPediAcc, PainelEdita,
    'PediAcc', 0, Dmod.QrUpd, [PainelEdita], [PainelDados], ImgTipo, true) then
  begin
    LocCod(Codigo, Codigo);
  end;
end;

procedure TFmPediAcc.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := Geral.IMV(EdCodigo.Text);
  if ImgTipo.SQLType = stIns then UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'PediAcc', Codigo);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'PediAcc', 'Codigo');
  MostraEdicao(0, stLok, 0);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'PediAcc', 'Codigo');
end;

procedure TFmPediAcc.BtFiliaisClick(Sender: TObject);
begin
{$IfDef cDbTable}
  if DBCheck.CriaFm(TFmRegioes, FmRegioes, afmoNegarComAviso) then
  begin
    FmRegioes.ShowModal;
    FmRegioes.Destroy;
    //
    ReopenPediAccReg(QrPediAccRegCodigo.Value);
  end;
{$Else}
  Geral.MB_Info('Solicite implementa��o � Dermatek! (DBE Table)');  //  TODO Berlin
{$EndIf}
end;

procedure TFmPediAcc.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  dmkDBGrid1.Align  := alClient;
  Panel4.Align      := alClient;
  CriaOForm;
  UnDmkDAC_PF.AbreQuery(QrSupervisores, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrCarteiras, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrContas, Dmod.MyDB);
end;

procedure TFmPediAcc.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrPediAccCodigo.Value, LaRegistro.Caption);
end;

procedure TFmPediAcc.SbCartFinClick(Sender: TObject);
var
  CartFin: Integer;
begin
  VAR_CADASTRO := 0;
  CartFin      := EdCartFin.ValueVariant;
  //
  FinanceiroJan.CadastroDeCarteiras(CartFin);
  //
  if VAR_CADASTRO <> 0 then
  begin
    UMyMod.SetaCodigoPesquisado(EdCartFin, CBCartFin, QrCarteiras, VAR_CADASTRO);
    EdCartFin.SetFocus;
  end;
end;

procedure TFmPediAcc.SBContaFinClick(Sender: TObject);
var
  ContaFin: Integer;
begin
  VAR_CADASTRO := 0;
  ContaFin     := EdContaFin.ValueVariant;
  //
  FinanceiroJan.CadastroDeContas(ContaFin);
  //
  if VAR_CADASTRO <> 0 then
  begin
    UMyMod.SetaCodigoPesquisado(EdCartFin, CBCartFin, QrContas, VAR_CADASTRO);
    EdCartFin.SetFocus;
  end;
end;

procedure TFmPediAcc.SbImprimeClick(Sender: TObject);
begin
//
end;

procedure TFmPediAcc.SbNomeClick(Sender: TObject);
begin
  //LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmPediAcc.SbNovoClick(Sender: TObject);
begin
  //LaRegistro.Caption := GOTOy.CodUsu(QrPediAccCodUsu.Value, LaRegistro.Caption);
end;

procedure TFmPediAcc.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmPediAcc.QrPediAccAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmPediAcc.QrPediAccAfterScroll(DataSet: TDataSet);
begin
  ReopenPediAccTab(0);
  ReopenPediAccReg(0);
end;

procedure TFmPediAcc.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmPediAcc.SbQueryClick(Sender: TObject);
begin
  LocCod(QrPediAccCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, 'IF(Tipo=0, RazaoSocial, Nome)',
  'entidades', Dmod.MyDB, 'AND Codigo IN (SELECT Codigo FROM pediacc)'));
end;

procedure TFmPediAcc.SbSupervisorClick(Sender: TObject);
var
  Supervisor: Integer;
begin
  VAR_CADASTRO := 0;
  Supervisor   := EdSupervisor.ValueVariant;
  //
  DModG.CadastroDeEntidade(Supervisor, fmcadEntidade2, fmcadEntidade2, True);
  //
  if VAR_CADASTRO <> 0 then
  begin
    UMyMod.SetaCodigoPesquisado(EdSupervisor, CBSupervisor, QrSupervisores, VAR_CADASTRO);
    EdSupervisor.SetFocus;
  end;
end;

procedure TFmPediAcc.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmPediAcc.QrPediAccBeforeClose(DataSet: TDataSet);
begin
  QrPediAccTab.Close;
  QrPediAccReg.Close;
end;

procedure TFmPediAcc.QrPediAccBeforeOpen(DataSet: TDataSet);
begin
  QrPediAccCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmPediAcc.ReopenPediAccTab(Controle: Integer);
begin
  QrPediAccTab.Close;
  QrPediAccTab.Params[0].AsInteger :=   QrPediAccCodigo.Value;
  UnDmkDAC_PF.AbreQuery(QrPediAccTab, Dmod.MyDB);
  //
  if Controle <> 0 then
    QrPediAccTab.Locate('Controle', Controle, []);
end;

procedure TFmPediAcc.ReopenPediAccReg(Codigo: Integer);
begin
  QrPediAccReg.Close;
  QrPediAccReg.Params[0].AsInteger :=   QrPediAccCodigo.Value;
  UnDmkDAC_PF.AbreQuery(QrPediAccReg, Dmod.MyDB);
  //
  if Codigo <> 0 then
    QrPediAccReg.Locate('Codigo', Codigo, []);
end;

end.

