object FmRegioes: TFmRegioes
  Left = 368
  Top = 194
  Caption = 'PED-GERAL-002 :: Cadastro de Regi'#245'es'
  ClientHeight = 705
  ClientWidth = 867
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelEdita: TPanel
    Left = 0
    Top = 96
    Width = 867
    Height = 609
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    Visible = False
    object PainelEdit: TPanel
      Left = 0
      Top = 0
      Width = 867
      Height = 104
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object Label7: TLabel
        Left = 4
        Top = 4
        Width = 14
        Height = 13
        Caption = 'ID:'
      end
      object Label8: TLabel
        Left = 64
        Top = 4
        Width = 57
        Height = 13
        Caption = 'C'#243'digo: [F4]'
      end
      object Label9: TLabel
        Left = 148
        Top = 4
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
      end
      object Label4: TLabel
        Left = 4
        Top = 44
        Width = 49
        Height = 13
        Caption = 'Geografia:'
      end
      object SpeedButton5: TSpeedButton
        Left = 404
        Top = 60
        Width = 21
        Height = 21
        OnClick = SpeedButton5Click
      end
      object SpeedButton6: TSpeedButton
        Left = 832
        Top = 60
        Width = 21
        Height = 21
        OnClick = SpeedButton6Click
      end
      object Label15: TLabel
        Left = 432
        Top = 44
        Width = 85
        Height = 13
        Caption = 'Agente comercial:'
      end
      object EdCodigo: TdmkEdit
        Left = 4
        Top = 20
        Width = 56
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Codigo'
        UpdCampo = 'Codigo'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdNome: TdmkEdit
        Left = 148
        Top = 20
        Width = 705
        Height = 21
        TabOrder = 2
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Nome'
        UpdCampo = 'Nome'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdCodUsu: TdmkEdit
        Left = 64
        Top = 20
        Width = 80
        Height = 21
        Alignment = taRightJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 1
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'CodUsu'
        UpdCampo = 'CodUsu'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnKeyDown = EdCodUsuKeyDown
      end
      object EdGeosite: TdmkEditCB
        Left = 4
        Top = 60
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 3
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBGeosite
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBGeosite: TdmkDBLookupComboBox
        Left = 64
        Top = 60
        Width = 337
        Height = 21
        KeyField = 'CodUsu'
        ListField = 'Nome'
        ListSource = DsGeosite
        TabOrder = 4
        dmkEditCB = EdGeosite
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object CBEntidade: TdmkDBLookupComboBox
        Left = 492
        Top = 60
        Width = 337
        Height = 21
        KeyField = 'Codigo'
        ListField = 'NOMEENT'
        ListSource = DsAgentes
        TabOrder = 6
        dmkEditCB = EdEntidade
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdEntidade: TdmkEditCB
        Left = 432
        Top = 60
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 5
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Entidade'
        UpdCampo = 'Entidade'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBEntidade
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
    end
    object GBRodaPe: TGroupBox
      Left = 0
      Top = 545
      Width = 867
      Height = 64
      Align = alBottom
      TabOrder = 1
      object Panel21: TPanel
        Left = 2
        Top = 15
        Width = 863
        Height = 47
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object PnSaiDesis: TPanel
          Left = 719
          Top = 0
          Width = 144
          Height = 47
          Align = alRight
          BevelOuter = bvNone
          TabOrder = 1
          object BtDesiste: TBitBtn
            Tag = 15
            Left = 7
            Top = 2
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Caption = '&Desiste'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            NumGlyphs = 2
            ParentFont = False
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtDesisteClick
          end
        end
        object BtConfirma: TBitBtn
          Tag = 14
          Left = 8
          Top = 2
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Confirma'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtConfirmaClick
        end
      end
    end
  end
  object PainelDados: TPanel
    Left = 0
    Top = 96
    Width = 867
    Height = 609
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    object DBGrid1: TDBGrid
      Left = 0
      Top = 88
      Width = 867
      Height = 72
      Align = alTop
      DataSource = DsRegioesIts
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'NIVEL_TXT'
          Title.Caption = 'N'#237'vel'
          Width = 72
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DTB_Pais'
          Title.Caption = 'Pa'#237's'
          Width = 77
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'UF'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMEMESO'
          Title.Caption = 'Mesorregi'#227'o'
          Width = 132
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMEMICRO'
          Title.Caption = 'Microrregi'#227'o'
          Width = 132
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMEMUNICI'
          Title.Caption = 'Munic'#237'pio'
          Width = 132
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DNE_NO_Loc'
          Title.Caption = 'Localidade'
          Width = 120
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DNE_NO_Bai'
          Title.Caption = 'Bairro'
          Width = 120
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DNE_NO_Log'
          Title.Caption = 'Logradouro'
          Width = 180
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DNE_CEP_TXT'
          Title.Caption = 'CEP'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Controle'
          Width = 56
          Visible = True
        end>
    end
    object PanelData: TPanel
      Left = 0
      Top = 0
      Width = 867
      Height = 88
      Align = alTop
      BevelOuter = bvNone
      Enabled = False
      TabOrder = 1
      object Label1: TLabel
        Left = 4
        Top = 4
        Width = 14
        Height = 13
        Caption = 'ID:'
        FocusControl = DBEdit03
      end
      object Label2: TLabel
        Left = 148
        Top = 4
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
        FocusControl = DBEdit04
      end
      object Label3: TLabel
        Left = 64
        Top = 4
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
        FocusControl = DBEdit1
      end
      object Label5: TLabel
        Left = 4
        Top = 44
        Width = 49
        Height = 13
        Caption = 'Geografia:'
      end
      object Label14: TLabel
        Left = 432
        Top = 44
        Width = 85
        Height = 13
        Caption = 'Agente comercial:'
      end
      object DBEdit03: TDBEdit
        Left = 4
        Top = 20
        Width = 56
        Height = 21
        Hint = 'N'#186' do banco'
        TabStop = False
        DataField = 'Codigo'
        DataSource = DsRegioes
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 0
      end
      object DBEdit04: TDBEdit
        Left = 148
        Top = 20
        Width = 709
        Height = 21
        Hint = 'Nome do banco'
        Color = clWhite
        DataField = 'Nome'
        DataSource = DsRegioes
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
      end
      object DBEdit1: TDBEdit
        Left = 64
        Top = 20
        Width = 80
        Height = 21
        DataField = 'CodUsu'
        DataSource = DsRegioes
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 2
      end
      object DBEdit2: TDBEdit
        Left = 4
        Top = 60
        Width = 56
        Height = 21
        DataField = 'Geosite'
        DataSource = DsRegioes
        TabOrder = 3
      end
      object DBEdit3: TDBEdit
        Left = 64
        Top = 60
        Width = 365
        Height = 21
        DataField = 'NOMEGEOSITE'
        DataSource = DsRegioes
        TabOrder = 4
      end
      object DBEdit9: TDBEdit
        Left = 492
        Top = 60
        Width = 365
        Height = 21
        DataField = 'NOMEENT'
        DataSource = DsRegioes
        TabOrder = 5
      end
      object DBEdit10: TDBEdit
        Left = 432
        Top = 60
        Width = 56
        Height = 21
        DataField = 'Entidade'
        DataSource = DsRegioes
        TabOrder = 6
      end
    end
    object GBCntrl: TGroupBox
      Left = 0
      Top = 545
      Width = 867
      Height = 64
      Align = alBottom
      TabOrder = 2
      object Panel5: TPanel
        Left = 2
        Top = 15
        Width = 172
        Height = 47
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 174
        Top = 15
        Width = 30
        Height = 17
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel3: TPanel
        Left = 344
        Top = 15
        Width = 521
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object Panel2: TPanel
          Left = 388
          Top = 0
          Width = 133
          Height = 47
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 2
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
        object BtRegiao: TBitBtn
          Left = 4
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Regi'#227'o'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtRegiaoClick
        end
        object BtLocais: TBitBtn
          Left = 128
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Locais'
          Enabled = False
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtLocaisClick
        end
      end
    end
  end
  object PnLocais: TPanel
    Left = 0
    Top = 96
    Width = 867
    Height = 609
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 2
    Visible = False
    object Panel4: TPanel
      Left = 0
      Top = 0
      Width = 867
      Height = 88
      Align = alTop
      BevelOuter = bvNone
      Enabled = False
      TabOrder = 0
      object Label6: TLabel
        Left = 4
        Top = 4
        Width = 14
        Height = 13
        Caption = 'ID:'
        FocusControl = DBEdit01
      end
      object Label10: TLabel
        Left = 148
        Top = 4
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
        FocusControl = DBEdit02
      end
      object Label11: TLabel
        Left = 64
        Top = 4
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
        FocusControl = DBEdit4
      end
      object Label12: TLabel
        Left = 4
        Top = 44
        Width = 49
        Height = 13
        Caption = 'Geografia:'
      end
      object Label13: TLabel
        Left = 432
        Top = 44
        Width = 85
        Height = 13
        Caption = 'Agente comercial:'
      end
      object DBEdit01: TDBEdit
        Left = 4
        Top = 20
        Width = 56
        Height = 21
        Hint = 'N'#186' do banco'
        TabStop = False
        DataField = 'Codigo'
        DataSource = DsRegioes
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 0
      end
      object DBEdit02: TDBEdit
        Left = 148
        Top = 20
        Width = 709
        Height = 21
        Hint = 'Nome do banco'
        Color = clWhite
        DataField = 'Nome'
        DataSource = DsRegioes
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
      end
      object DBEdit4: TDBEdit
        Left = 64
        Top = 20
        Width = 80
        Height = 21
        DataField = 'CodUsu'
        DataSource = DsRegioes
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 2
      end
      object DBEdit5: TDBEdit
        Left = 4
        Top = 60
        Width = 56
        Height = 21
        DataField = 'Geosite'
        DataSource = DsRegioes
        TabOrder = 3
      end
      object DBEdit6: TDBEdit
        Left = 64
        Top = 60
        Width = 365
        Height = 21
        DataField = 'NOMEGEOSITE'
        DataSource = DsRegioes
        TabOrder = 4
      end
      object DBEdit7: TDBEdit
        Left = 492
        Top = 60
        Width = 365
        Height = 21
        DataField = 'NOMEENT'
        DataSource = DsRegioes
        TabOrder = 5
      end
      object DBEdit8: TDBEdit
        Left = 432
        Top = 60
        Width = 56
        Height = 21
        DataField = 'Entidade'
        DataSource = DsRegioes
        TabOrder = 6
      end
    end
    object DBGrid2: TDBGrid
      Left = 0
      Top = 88
      Width = 867
      Height = 60
      Align = alTop
      DataSource = DsRegioesIts
      TabOrder = 1
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'NIVEL_TXT'
          Title.Caption = 'N'#237'vel'
          Width = 72
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DTB_Pais'
          Title.Caption = 'Pa'#237's'
          Width = 77
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'UF'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMEMESO'
          Title.Caption = 'Mesorregi'#227'o'
          Width = 132
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMEMICRO'
          Title.Caption = 'Microrregi'#227'o'
          Width = 132
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMEMUNICI'
          Title.Caption = 'Munic'#237'pio'
          Width = 132
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DNE_NO_Loc'
          Title.Caption = 'Localidade'
          Width = 120
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DNE_NO_Bai'
          Title.Caption = 'Bairro'
          Width = 120
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DNE_NO_Log'
          Title.Caption = 'Logradouro'
          Width = 180
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DNE_CEP_TXT'
          Title.Caption = 'CEP'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Controle'
          Width = 56
          Visible = True
        end>
    end
    object Panel8: TPanel
      Left = 0
      Top = 237
      Width = 867
      Height = 308
      Align = alBottom
      BevelOuter = bvNone
      TabOrder = 2
      object GB_DTB: TGroupBox
        Left = 0
        Top = 80
        Width = 867
        Height = 80
        Align = alTop
        Caption = ' Dados da DTB (distribui'#231#227'o territorial brasileira): '
        TabOrder = 1
        Visible = False
        object Panel10: TPanel
          Left = 2
          Top = 15
          Width = 75
          Height = 63
          Align = alLeft
          BevelOuter = bvNone
          TabOrder = 0
          object PnDTB_UFs: TPanel
            Left = 0
            Top = 0
            Width = 75
            Height = 63
            Align = alClient
            BevelOuter = bvNone
            TabOrder = 0
            Visible = False
            object dmkLabel1: TdmkLabel
              Left = 4
              Top = 0
              Width = 17
              Height = 13
              Caption = 'UF:'
              UpdType = utYes
              SQLType = stNil
            end
            object EdPesqDTB_UF: TEdit
              Left = 4
              Top = 16
              Width = 69
              Height = 21
              TabOrder = 0
              OnExit = EdPesqDTB_UFExit
            end
            object EdDTB_UF: TdmkEditCB
              Left = 4
              Top = 38
              Width = 24
              Height = 21
              Alignment = taRightJustify
              TabOrder = 1
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              OnChange = EdDTB_UFChange
              DBLookupComboBox = CBDTB_UF
              IgnoraDBLookupComboBox = False
              AutoSetIfOnlyOneReg = setregOnlyManual
            end
            object CBDTB_UF: TdmkDBLookupComboBox
              Left = 28
              Top = 38
              Width = 45
              Height = 21
              KeyField = 'Codigo'
              ListField = 'Sigla'
              ListSource = DsDTB_UFs
              TabOrder = 2
              dmkEditCB = EdDTB_UF
              UpdType = utYes
              LocF7SQLMasc = '$#'
            end
          end
        end
        object Panel12: TPanel
          Left = 77
          Top = 15
          Width = 252
          Height = 63
          Align = alLeft
          BevelOuter = bvNone
          TabOrder = 1
          object PnDTB_Meso: TPanel
            Left = 0
            Top = 0
            Width = 252
            Height = 63
            Align = alClient
            BevelOuter = bvNone
            TabOrder = 0
            Visible = False
            object dmkLabel2: TdmkLabel
              Left = 4
              Top = 0
              Width = 61
              Height = 13
              Caption = 'Mesorregi'#227'o:'
              UpdType = utYes
              SQLType = stNil
            end
            object EdPesqDTB_Meso: TEdit
              Left = 4
              Top = 16
              Width = 245
              Height = 21
              TabOrder = 0
              OnExit = EdPesqDTB_MesoExit
            end
            object EdDTB_Meso: TdmkEditCB
              Left = 4
              Top = 38
              Width = 40
              Height = 21
              Alignment = taRightJustify
              TabOrder = 1
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              OnChange = EdDTB_MesoChange
              DBLookupComboBox = CBDTB_Meso
              IgnoraDBLookupComboBox = False
              AutoSetIfOnlyOneReg = setregOnlyManual
            end
            object CBDTB_Meso: TdmkDBLookupComboBox
              Left = 44
              Top = 38
              Width = 205
              Height = 21
              KeyField = 'Codigo'
              ListField = 'Nome'
              ListSource = DsDTB_Meso
              TabOrder = 2
              dmkEditCB = EdDTB_Meso
              UpdType = utYes
              LocF7SQLMasc = '$#'
            end
          end
        end
        object Panel14: TPanel
          Left = 329
          Top = 15
          Width = 272
          Height = 63
          Align = alLeft
          BevelOuter = bvNone
          TabOrder = 2
          object PnDTB_Micro: TPanel
            Left = 0
            Top = 0
            Width = 272
            Height = 63
            Align = alClient
            BevelOuter = bvNone
            TabOrder = 0
            Visible = False
            object dmkLabel3: TdmkLabel
              Left = 4
              Top = 0
              Width = 61
              Height = 13
              Caption = 'Microrregi'#227'o:'
              UpdType = utYes
              SQLType = stNil
            end
            object EdPesqDTB_Micro: TEdit
              Left = 4
              Top = 16
              Width = 265
              Height = 21
              TabOrder = 0
              OnExit = EdPesqDTB_MicroExit
            end
            object EdDTB_Micro: TdmkEditCB
              Left = 4
              Top = 38
              Width = 48
              Height = 21
              Alignment = taRightJustify
              TabOrder = 1
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              OnChange = EdDTB_MicroChange
              DBLookupComboBox = CBDTB_Micro
              IgnoraDBLookupComboBox = False
              AutoSetIfOnlyOneReg = setregOnlyManual
            end
            object CBDTB_Micro: TdmkDBLookupComboBox
              Left = 52
              Top = 38
              Width = 217
              Height = 21
              KeyField = 'Codigo'
              ListField = 'Nome'
              ListSource = DsDTB_Micro
              TabOrder = 2
              dmkEditCB = EdDTB_Micro
              UpdType = utYes
              LocF7SQLMasc = '$#'
            end
          end
        end
        object Panel16: TPanel
          Left = 601
          Top = 15
          Width = 256
          Height = 63
          Align = alLeft
          BevelOuter = bvNone
          TabOrder = 3
          object PnDTB_Munici: TPanel
            Left = 0
            Top = 0
            Width = 256
            Height = 63
            Align = alClient
            BevelOuter = bvNone
            TabOrder = 0
            Visible = False
            object dmkLabel4: TdmkLabel
              Left = 4
              Top = 0
              Width = 50
              Height = 13
              Caption = 'Munic'#237'pio:'
              UpdType = utYes
              SQLType = stNil
            end
            object EdPesqDTB_Munici: TEdit
              Left = 4
              Top = 16
              Width = 249
              Height = 21
              TabOrder = 0
              OnExit = EdPesqDTB_MuniciExit
            end
            object EdDTB_Munici: TdmkEditCB
              Left = 4
              Top = 38
              Width = 56
              Height = 21
              Alignment = taRightJustify
              TabOrder = 1
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              OnChange = EdDTB_MuniciChange
              DBLookupComboBox = CBDTB_Munici
              IgnoraDBLookupComboBox = False
              AutoSetIfOnlyOneReg = setregOnlyManual
            end
            object CBDTB_Munici: TdmkDBLookupComboBox
              Left = 60
              Top = 38
              Width = 193
              Height = 21
              KeyField = 'Codigo'
              ListField = 'Nome'
              ListSource = DsDTB_Munici
              TabOrder = 2
              dmkEditCB = EdDTB_Munici
              UpdType = utYes
              LocF7SQLMasc = '$#'
            end
          end
        end
      end
      object GB_DNE: TGroupBox
        Left = 0
        Top = 160
        Width = 867
        Height = 140
        Align = alTop
        Caption = ' Dados do DNE (diret'#243'rio nacional de endere'#231'os): '
        TabOrder = 2
        Visible = False
        object Panel13: TPanel
          Left = 2
          Top = 15
          Width = 863
          Height = 60
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 0
          object Panel11: TPanel
            Left = 0
            Top = 0
            Width = 445
            Height = 60
            Align = alLeft
            BevelOuter = bvNone
            TabOrder = 0
            object PnDNE_Locali: TPanel
              Left = 0
              Top = 0
              Width = 445
              Height = 60
              Align = alClient
              BevelOuter = bvNone
              TabOrder = 0
              Visible = False
              object dmkLabel8: TdmkLabel
                Left = 4
                Top = 0
                Width = 55
                Height = 13
                Caption = 'Localidade:'
                UpdType = utYes
                SQLType = stNil
              end
              object EdDNE_Locali: TdmkEditCB
                Left = 4
                Top = 38
                Width = 56
                Height = 21
                Alignment = taRightJustify
                TabOrder = 0
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
                OnChange = EdDNE_LocaliChange
                DBLookupComboBox = CBDNE_Locali
                IgnoraDBLookupComboBox = False
                AutoSetIfOnlyOneReg = setregOnlyManual
              end
              object CBDNE_Locali: TdmkDBLookupComboBox
                Left = 60
                Top = 38
                Width = 385
                Height = 21
                KeyField = 'LOC_NU_SEQUENCIAL'
                ListField = 'LOC_NO'
                ListSource = DsCidades
                TabOrder = 1
                dmkEditCB = EdDNE_Locali
                UpdType = utYes
                LocF7SQLMasc = '$#'
              end
              object EdDNE_Filtro_Local: TEdit
                Left = 4
                Top = 16
                Width = 441
                Height = 21
                TabOrder = 2
                OnExit = EdDNE_Filtro_LocalExit
              end
            end
          end
          object Panel15: TPanel
            Left = 445
            Top = 0
            Width = 418
            Height = 60
            Align = alClient
            BevelOuter = bvNone
            TabOrder = 1
            object PnDNE_Bairro: TPanel
              Left = 0
              Top = 0
              Width = 418
              Height = 60
              Align = alClient
              BevelOuter = bvNone
              TabOrder = 0
              Visible = False
              object dmkLabel9: TdmkLabel
                Left = 4
                Top = 0
                Width = 30
                Height = 13
                Caption = 'Bairro:'
                UpdType = utYes
                SQLType = stNil
              end
              object EdDNE_Bairro: TdmkEditCB
                Left = 4
                Top = 38
                Width = 48
                Height = 21
                Alignment = taRightJustify
                TabOrder = 0
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
                OnChange = EdDNE_BairroChange
                DBLookupComboBox = CBDNE_Bairro
                IgnoraDBLookupComboBox = False
                AutoSetIfOnlyOneReg = setregOnlyManual
              end
              object CBDNE_Bairro: TdmkDBLookupComboBox
                Left = 52
                Top = 38
                Width = 357
                Height = 21
                KeyField = 'BAI_NU_SEQUENCIAL'
                ListField = 'BAI_NO'
                ListSource = DsDNE_Bairros
                TabOrder = 1
                dmkEditCB = EdDNE_Bairro
                UpdType = utYes
                LocF7SQLMasc = '$#'
              end
              object EdDNE_Filtro_Bairro: TEdit
                Left = 4
                Top = 16
                Width = 405
                Height = 21
                TabOrder = 2
                OnChange = EdDNE_Filtro_BairroChange
                OnExit = EdDNE_Filtro_LocalExit
              end
            end
          end
        end
        object Panel17: TPanel
          Left = 2
          Top = 75
          Width = 863
          Height = 63
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 1
          object Panel18: TPanel
            Left = 0
            Top = 0
            Width = 677
            Height = 63
            Align = alLeft
            BevelOuter = bvNone
            TabOrder = 0
            object PnDNE_Lograd: TPanel
              Left = 0
              Top = 0
              Width = 677
              Height = 63
              Align = alClient
              BevelOuter = bvNone
              TabOrder = 0
              Visible = False
              object dmkLabel10: TdmkLabel
                Left = 4
                Top = 0
                Width = 57
                Height = 13
                Caption = 'Logradouro:'
                UpdType = utYes
                SQLType = stNil
              end
              object EdDNE_Filtro_Lograd: TEdit
                Left = 4
                Top = 16
                Width = 441
                Height = 21
                TabOrder = 0
                OnChange = EdDNE_Filtro_BairroChange
                OnExit = EdDNE_Filtro_LogradExit
              end
              object CBDNE_Lograd: TdmkDBLookupComboBox
                Left = 53
                Top = 38
                Width = 392
                Height = 21
                KeyField = 'LOG_NU_SEQUENCIAL'
                ListField = 'LOG_NOME'
                ListSource = DsDNE_Lograd
                TabOrder = 1
                dmkEditCB = EdDNE_Lograd
                UpdType = utYes
                LocF7SQLMasc = '$#'
              end
              object EdDNE_Lograd: TdmkEditCB
                Left = 4
                Top = 38
                Width = 48
                Height = 21
                Alignment = taRightJustify
                TabOrder = 2
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
                DBLookupComboBox = CBDNE_Lograd
                IgnoraDBLookupComboBox = False
                AutoSetIfOnlyOneReg = setregOnlyManual
              end
            end
          end
          object Panel19: TPanel
            Left = 677
            Top = 0
            Width = 186
            Height = 63
            Align = alClient
            BevelOuter = bvNone
            TabOrder = 1
            object PnDNE_CEP: TPanel
              Left = 0
              Top = 0
              Width = 186
              Height = 63
              Align = alClient
              BevelOuter = bvNone
              TabOrder = 0
              object LaCEP: TLabel
                Left = 1
                Top = 4
                Width = 27
                Height = 13
                Caption = 'CEP: '
              end
              object BtCEP: TBitBtn
                Left = 2
                Top = 20
                Width = 37
                Height = 37
                TabOrder = 0
                OnClick = BtCEPClick
              end
              object EdDNE_CEP: TEdit
                Left = 43
                Top = 20
                Width = 133
                Height = 37
                TabStop = False
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlue
                Font.Height = -24
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                ParentFont = False
                ReadOnly = True
                TabOrder = 1
                OnExit = EdDNE_CEPExit
              end
            end
          end
        end
      end
      object Panel9: TPanel
        Left = 0
        Top = 0
        Width = 867
        Height = 80
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 0
        object GroupBox3: TGroupBox
          Left = 552
          Top = 0
          Width = 315
          Height = 80
          Align = alClient
          Caption = ' Pa'#237's: '
          TabOrder = 0
          object PnDTB_Paises: TPanel
            Left = 2
            Top = 15
            Width = 311
            Height = 63
            Align = alClient
            BevelOuter = bvNone
            TabOrder = 0
            object dmkLabel11: TdmkLabel
              Left = 4
              Top = 0
              Width = 117
              Height = 13
              Caption = 'Pesquisa pa'#237's por nome:'
              UpdType = utYes
              SQLType = stNil
            end
            object CBDTB_Pais: TdmkDBLookupComboBox
              Left = 4
              Top = 40
              Width = 285
              Height = 21
              KeyField = 'Nome'
              ListField = 'Nome'
              ListSource = DsDTB_Paises
              TabOrder = 0
              OnClick = CBDTB_PaisClick
              UpdType = utYes
              LocF7SQLMasc = '$#'
            end
            object EdPesqDTB_Pais: TEdit
              Left = 4
              Top = 16
              Width = 285
              Height = 21
              TabOrder = 1
              OnExit = EdPesqDTB_PaisExit
            end
          end
        end
        object RGNivel: TdmkRadioGroup
          Left = 0
          Top = 0
          Width = 552
          Height = 80
          Align = alLeft
          Caption = ' N'#237'vel da abrang'#234'ncia: '
          Columns = 5
          ItemIndex = 0
          Items.Strings = (
            '0. Nenhum'
            '1. CEP'
            '2. Logradouro'
            '3. Bairro'
            '4. Localidade'
            '5. Munic'#237'pio'
            '6. Microrregi'#227'o'
            '7. Mesorregi'#227'o'
            '8. Estado'
            '9. Pa'#237's')
          TabOrder = 1
          OnClick = RGNivelClick
          UpdType = utYes
          OldValor = 0
        end
      end
    end
    object GroupBox1: TGroupBox
      Left = 0
      Top = 545
      Width = 867
      Height = 64
      Align = alBottom
      TabOrder = 3
      object Panel1: TPanel
        Left = 2
        Top = 15
        Width = 863
        Height = 47
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object dmkLabel5: TdmkLabel
          Left = 132
          Top = 3
          Width = 35
          Height = 13
          Caption = 'Distrito:'
          Visible = False
          UpdType = utYes
          SQLType = stNil
        end
        object dmkLabel6: TdmkLabel
          Left = 388
          Top = 3
          Width = 55
          Height = 13
          Caption = 'Sub-distrito:'
          Visible = False
          UpdType = utYes
          SQLType = stNil
        end
        object Panel22: TPanel
          Left = 719
          Top = 0
          Width = 144
          Height = 47
          Align = alRight
          BevelOuter = bvNone
          TabOrder = 0
          object BitBtn2: TBitBtn
            Tag = 15
            Left = 7
            Top = 2
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Caption = '&Desiste'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            NumGlyphs = 2
            ParentFont = False
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtDesisteClick
          end
        end
        object BitBtn1: TBitBtn
          Tag = 14
          Left = 8
          Top = 3
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Confirma'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BitBtn1Click
        end
        object EdDTB_Distri: TdmkEditCB
          Left = 132
          Top = 19
          Width = 32
          Height = 21
          Alignment = taRightJustify
          TabOrder = 2
          Visible = False
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          OnChange = EdDTB_DistriChange
          DBLookupComboBox = CBDTB_Distri
          IgnoraDBLookupComboBox = False
          AutoSetIfOnlyOneReg = setregOnlyManual
        end
        object CBDTB_Distri: TdmkDBLookupComboBox
          Left = 164
          Top = 19
          Width = 221
          Height = 21
          KeyField = 'Codigo'
          ListField = 'Nome'
          ListSource = DsDTB_Distri
          TabOrder = 3
          Visible = False
          dmkEditCB = EdDTB_Distri
          UpdType = utYes
          LocF7SQLMasc = '$#'
        end
        object EdDTB_SubDis: TdmkEditCB
          Left = 388
          Top = 19
          Width = 32
          Height = 21
          Alignment = taRightJustify
          TabOrder = 4
          Visible = False
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          DBLookupComboBox = CBDTB_SubDis
          IgnoraDBLookupComboBox = False
          AutoSetIfOnlyOneReg = setregOnlyManual
        end
        object CBDTB_SubDis: TdmkDBLookupComboBox
          Left = 420
          Top = 19
          Width = 297
          Height = 21
          KeyField = 'Codigo'
          ListField = 'Nome'
          ListSource = DsDTB_SubDis
          TabOrder = 5
          Visible = False
          dmkEditCB = EdDTB_SubDis
          UpdType = utYes
          LocF7SQLMasc = '$#'
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 867
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 3
    object GB_R: TGroupBox
      Left = 819
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 12
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 216
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
        OnClick = SbImprimeClick
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 216
      Top = 0
      Width = 603
      Height = 52
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 257
        Height = 32
        Caption = 'Cadastro de Regi'#245'es'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 257
        Height = 32
        Caption = 'Cadastro de Regi'#245'es'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 257
        Height = 32
        Caption = 'Cadastro de Regi'#245'es'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 52
    Width = 867
    Height = 44
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 4
    object Panel20: TPanel
      Left = 2
      Top = 15
      Width = 863
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object DsRegioes: TDataSource
    DataSet = QrRegioes
    Left = 40
    Top = 12
  end
  object QrRegioes: TMySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrRegioesBeforeOpen
    AfterOpen = QrRegioesAfterOpen
    BeforeClose = QrRegioesBeforeClose
    AfterScroll = QrRegioesAfterScroll
    SQL.Strings = (
      'SELECT reg.Codigo, reg.CodUsu, reg.Nome, '
      'reg.Geosite, geo.Nome NOMEGEOSITE, reg.Entidade,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENT '
      'FROM regioes reg'#13
      'LEFT JOIN geosite   geo ON geo.Codigo=reg.Geosite'
      'LEFT JOIN entidades ent ON ent.Codigo=reg.Entidade'#10
      ''
      '')
    Left = 12
    Top = 12
    object QrRegioesCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrRegioesCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrRegioesNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
    object QrRegioesGeosite: TIntegerField
      FieldName = 'Geosite'
      Required = True
    end
    object QrRegioesNOMEGEOSITE: TWideStringField
      FieldName = 'NOMEGEOSITE'
      Size = 30
    end
    object QrRegioesEntidade: TIntegerField
      FieldName = 'Entidade'
      Required = True
    end
    object QrRegioesNOMEENT: TWideStringField
      FieldName = 'NOMEENT'
      Size = 100
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 68
    Top = 12
  end
  object QrGeosite: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, CodUsu, Nome'
      'FROM geosite'
      'ORDER BY Nome')
    Left = 796
    Top = 20
    object QrGeositeCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrGeositeCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrGeositeNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
  end
  object DsGeosite: TDataSource
    DataSet = QrGeosite
    Left = 824
    Top = 20
  end
  object dmkValUsu1: TdmkValUsu
    dmkEditCB = EdGeosite
    Panel = PainelEdita
    QryCampo = 'Geosite'
    UpdCampo = 'Geosite'
    RefCampo = 'Codigo'
    UpdType = utYes
    Left = 96
    Top = 12
  end
  object PMRegiao: TPopupMenu
    Left = 428
    Top = 532
    object Incluinovaregio1: TMenuItem
      Caption = '&Inclui nova regi'#227'o'
      OnClick = Incluinovaregio1Click
    end
    object Alteraregioatual1: TMenuItem
      Caption = '&Altera regi'#227'o atual'
      OnClick = Alteraregioatual1Click
    end
    object Excluiregioatual1: TMenuItem
      Caption = '&Exclui regi'#227'o atual'
      Enabled = False
    end
  end
  object PMLocais: TPopupMenu
    Left = 528
    Top = 536
    object Adicionanovolocal1: TMenuItem
      Caption = '&Adiciona novo local'
      OnClick = Adicionanovolocal1Click
    end
    object Excluilocal1: TMenuItem
      Caption = '&Exclui local'
      OnClick = Excluilocal1Click
    end
  end
  object QrRegioesIts: TMySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrRegioesItsCalcFields
    SQL.Strings = (
      'SELECT rei.Controle, rei.Nivel, ELT(rei.Nivel+1, '
      '"0. Nenhum", "1. CEP", "2. Logradouro", "3. Bairro", '
      '"4. Localidade", "5. Munic'#237'pio", "6. Microrregi'#227'o",'
      '"7. Mesorregi'#227'o", "8. Estado", "9. Pa'#237's") NIVEL_TXT,'
      'rei.DTB_Pais, ufs.Sigla UF, mes.Nome NOMEMESO,'
      'mic.Nome NOMEMICRO, mun.Nome NOMEMUNICI,'
      'rei.DTB_UF, rei.DTB_Meso, rei.DTB_Micro, '
      'rei.DTB_Munici, rei.DNE_Locali, rei.DNE_Bairro,'
      'rei.DNE_Lograd, rei.DNE_CEP,'
      'rei.DNE_NO_Loc, rei.DNE_NO_Bai, rei.DNE_NO_Log '
      'FROM regioesits rei'
      'LEFT JOIN dtb_ufs ufs ON ufs.Codigo=rei.DTB_UF'
      'LEFT JOIN dtb_meso mes ON mes.Codigo=rei.DTB_Meso'
      ''
      'LEFT JOIN dtb_micro mic ON mic.Codigo=rei.DTB_Micro'
      'LEFT JOIN dtb_munici mun ON mun.Codigo=rei.DTB_Munici'
      'WHERE rei.Codigo=:P0'
      'ORDER BY rei.Nivel DESC, rei.DTB_Pais')
    Left = 124
    Top = 12
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrRegioesItsControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrRegioesItsNivel: TSmallintField
      FieldName = 'Nivel'
      Required = True
    end
    object QrRegioesItsNIVEL_TXT: TWideStringField
      FieldName = 'NIVEL_TXT'
      Size = 15
    end
    object QrRegioesItsDTB_Pais: TWideStringField
      FieldName = 'DTB_Pais'
      Required = True
      Size = 100
    end
    object QrRegioesItsUF: TWideStringField
      FieldName = 'UF'
      Size = 2
    end
    object QrRegioesItsNOMEMESO: TWideStringField
      FieldName = 'NOMEMESO'
      Size = 100
    end
    object QrRegioesItsNOMEMICRO: TWideStringField
      FieldName = 'NOMEMICRO'
      Size = 100
    end
    object QrRegioesItsNOMEMUNICI: TWideStringField
      FieldName = 'NOMEMUNICI'
      Size = 100
    end
    object QrRegioesItsDTB_UF: TSmallintField
      FieldName = 'DTB_UF'
      Required = True
    end
    object QrRegioesItsDTB_Meso: TIntegerField
      FieldName = 'DTB_Meso'
      Required = True
      DisplayFormat = '0;-0; '
    end
    object QrRegioesItsDTB_Micro: TIntegerField
      FieldName = 'DTB_Micro'
      Required = True
      DisplayFormat = '0;-0; '
    end
    object QrRegioesItsDTB_Munici: TIntegerField
      FieldName = 'DTB_Munici'
      Required = True
      DisplayFormat = '0;-0; '
    end
    object QrRegioesItsDNE_Locali: TIntegerField
      FieldName = 'DNE_Locali'
      Required = True
      DisplayFormat = '0;-0; '
    end
    object QrRegioesItsDNE_Bairro: TIntegerField
      FieldName = 'DNE_Bairro'
      Required = True
      DisplayFormat = '0;-0; '
    end
    object QrRegioesItsDNE_Lograd: TIntegerField
      FieldName = 'DNE_Lograd'
      Required = True
      DisplayFormat = '0;-0; '
    end
    object QrRegioesItsDNE_CEP: TIntegerField
      FieldName = 'DNE_CEP'
      Required = True
    end
    object QrRegioesItsDNE_CEP_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DNE_CEP_TXT'
      Size = 9
      Calculated = True
    end
    object QrRegioesItsDNE_NO_Loc: TWideStringField
      FieldName = 'DNE_NO_Loc'
      Size = 72
    end
    object QrRegioesItsDNE_NO_Bai: TWideStringField
      FieldName = 'DNE_NO_Bai'
      Size = 72
    end
    object QrRegioesItsDNE_NO_Log: TWideStringField
      FieldName = 'DNE_NO_Log'
      Size = 100
    end
  end
  object DsRegioesIts: TDataSource
    DataSet = QrRegioesIts
    Left = 152
    Top = 12
  end
  object QrDTB_UFs: TMySQLQuery
    Database = DModG.AllID_DB
    SQL.Strings = (
      'SELECT Codigo, Sigla, Nome '
      'FROM dtb_ufs'
      'WHERE NOT (Codigo IN('
      '  SELECT DISTINCT DTB_UF '
      '  FROM regioesits'#13
      '  WHERE Nivel=8'
      '))'#13
      'AND Nome LIKE :P0'
      'ORDER BY Sigla')
    Left = 560
    Top = 12
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrDTB_UFsCodigo: TSmallintField
      FieldName = 'Codigo'
    end
    object QrDTB_UFsSigla: TWideStringField
      FieldName = 'Sigla'
      Size = 2
    end
    object QrDTB_UFsNome: TWideStringField
      FieldName = 'Nome'
    end
  end
  object DsDTB_UFs: TDataSource
    DataSet = QrDTB_UFs
    Left = 588
    Top = 12
  end
  object QrDTB_Meso: TMySQLQuery
    Database = DModG.AllID_DB
    SQL.Strings = (
      'SELECT mes.Codigo, mes.Nome, mes.DTB_UF, ufs.Sigla'
      'FROM dtb_meso mes'
      'LEFT JOIN dtb_ufs ufs ON ufs.Codigo=mes.DTB_UF'
      'ORDER BY mes.Nome')
    Left = 616
    Top = 12
    object QrDTB_MesoCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrDTB_MesoNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QrDTB_MesoSigla: TWideStringField
      FieldName = 'Sigla'
      Size = 2
    end
    object QrDTB_MesoDTB_UF: TSmallintField
      FieldName = 'DTB_UF'
      Required = True
    end
  end
  object DsDTB_Meso: TDataSource
    DataSet = QrDTB_Meso
    Left = 644
    Top = 12
  end
  object QrDTB_Micro: TMySQLQuery
    Database = DModG.AllID_DB
    SQL.Strings = (
      'SELECT mic.Codigo, mic.Nome, mic.DTB_UF, mic.DTB_Meso'
      'FROM dtb_micro mic'
      'LEFT JOIN dtb_meso mes ON mes.Codigo=mic.DTB_Meso'
      'LEFT JOIN dtb_ufs ufs ON ufs.Codigo=mic.DTB_UF'
      'ORDER BY mic.Nome'#10
      ''
      ''
      '')
    Left = 672
    Top = 12
    object QrDTB_MicroCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrDTB_MicroNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QrDTB_MicroDTB_UF: TSmallintField
      FieldName = 'DTB_UF'
      Required = True
    end
    object QrDTB_MicroDTB_Meso: TIntegerField
      FieldName = 'DTB_Meso'
      Required = True
    end
  end
  object DsDTB_Micro: TDataSource
    DataSet = QrDTB_Micro
    Left = 700
    Top = 12
  end
  object QrDTB_Munici: TMySQLQuery
    Database = DModG.AllID_DB
    SQL.Strings = (
      'SELECT Codigo, Nome, DTB_UF, DTB_Meso, DTB_Micro, DNE'
      'FROM dtb_munici'
      'ORDER BY Nome'#10
      ''
      ''
      '')
    Left = 728
    Top = 12
    object QrDTB_MuniciCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrDTB_MuniciNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QrDTB_MuniciDTB_UF: TSmallintField
      FieldName = 'DTB_UF'
    end
    object QrDTB_MuniciDTB_Meso: TIntegerField
      FieldName = 'DTB_Meso'
    end
    object QrDTB_MuniciDTB_Micro: TIntegerField
      FieldName = 'DTB_Micro'
    end
    object QrDTB_MuniciDNE: TIntegerField
      FieldName = 'DNE'
    end
  end
  object DsDTB_Munici: TDataSource
    DataSet = QrDTB_Munici
    Left = 756
    Top = 12
  end
  object QrDTB_Distri: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome, DTB_Munici'
      'FROM dtb_distri'
      'ORDER BY Nome'#10
      ''
      ''
      '')
    Left = 292
    Top = 65520
    object QrDTB_DistriCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrDTB_DistriNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QrDTB_DistriDTB_Munici: TIntegerField
      FieldName = 'DTB_Munici'
    end
  end
  object DsDTB_Distri: TDataSource
    DataSet = QrDTB_Distri
    Left = 320
    Top = 65520
  end
  object QrDTB_SubDis: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM dtb_subdis'
      'ORDER BY Nome'#10
      ''
      ''
      ''
      '')
    Left = 348
    Top = 65520
    object IntegerField1: TIntegerField
      FieldName = 'Codigo'
    end
    object StringField1: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
  object DsDTB_SubDis: TDataSource
    DataSet = QrDTB_SubDis
    Left = 376
    Top = 65520
  end
  object DsCidades: TDataSource
    Left = 180
    Top = 112
  end
  object DCP_twofish1: TDCP_twofish
    Id = 6
    Algorithm = 'Twofish'
    MaxKeySize = 256
    BlockSize = 128
    Left = 124
    Top = 112
  end
  object DsDNE_Bairros: TDataSource
    Left = 236
    Top = 112
  end
  object DsDNE_Lograd: TDataSource
    Left = 292
    Top = 112
  end
  object QrDTB_Paises: TMySQLQuery
    Database = DModG.AllID_DB
    SQL.Strings = (
      'SELECT * FROM dtb_paises'
      'WHERE Nome LIKE :P0'
      'ORDER BY Nome')
    Left = 504
    Top = 12
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrDTB_PaisesNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QrDTB_PaisesLocal: TWideStringField
      FieldName = 'Local'
      Size = 60
    end
    object QrDTB_PaisesPopulacao: TFloatField
      FieldName = 'Populacao'
    end
  end
  object DsDTB_Paises: TDataSource
    DataSet = QrDTB_Paises
    Left = 532
    Top = 12
  end
  object PMCEP: TPopupMenu
    Left = 768
    Top = 454
    object Descobrir1: TMenuItem
      Caption = '&Descobrir'
      OnClick = Descobrir1Click
    end
    object Verificar1: TMenuItem
      Caption = '&Verificar'
      OnClick = Verificar1Click
    end
  end
  object QrSub: TMySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrSubCalcFields
    SQL.Strings = (
      'SELECT reg.CodUsu, reg.Nome NOMEREG, '
      'rei.Controle, rei.Nivel, ELT(rei.Nivel+1, '
      '"0. Nenhum", "1. CEP", "2. Logradouro", "3. Bairro", '
      '"4. Localidade", "5. Munic'#237'pio", "6. Microrregi'#227'o",'
      '"7. Mesorregi'#227'o", "8. Estado", "9. Pa'#237's") NIVEL_TXT,'
      'rei.DTB_Pais, ufs.Sigla UF, mes.Nome NOMEMESO,'
      'mic.Nome NOMEMICRO, mun.Nome NOMEMUNICI,'
      'rei.DTB_UF, rei.DTB_Meso, rei.DTB_Micro, '
      'rei.DTB_Munici, rei.DNE_Locali, rei.DNE_Bairro,'
      'rei.DNE_Lograd, rei.DNE_CEP,'
      'rei.DNE_NO_Loc, rei.DNE_NO_Bai, rei.DNE_NO_Log '
      'FROM regioesits rei'
      'LEFT JOIN regioes reg ON reg.Codigo=rei.Codigo'
      'LEFT JOIN dtb_ufs ufs ON ufs.Codigo=rei.DTB_UF'
      'LEFT JOIN dtb_meso mes ON mes.Codigo=rei.DTB_Meso'
      'LEFT JOIN dtb_micro mic ON mic.Codigo=rei.DTB_Micro'
      'LEFT JOIN dtb_munici mun ON mun.Codigo=rei.DTB_Munici'
      'WHERE rei.Codigo=:P0'
      'ORDER BY rei.Nivel DESC, rei.DTB_Pais')
    Left = 320
    Top = 112
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSubCodUsu: TIntegerField
      FieldName = 'CodUsu'
      Required = True
    end
    object QrSubNOMEREG: TWideStringField
      FieldName = 'NOMEREG'
      Size = 30
    end
    object QrSubControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrSubNivel: TSmallintField
      FieldName = 'Nivel'
      Required = True
    end
    object QrSubNIVEL_TXT: TWideStringField
      FieldName = 'NIVEL_TXT'
      Size = 15
    end
    object QrSubDTB_Pais: TWideStringField
      FieldName = 'DTB_Pais'
      Required = True
      Size = 100
    end
    object QrSubUF: TWideStringField
      FieldName = 'UF'
      Size = 2
    end
    object QrSubNOMEMESO: TWideStringField
      FieldName = 'NOMEMESO'
      Size = 100
    end
    object QrSubNOMEMICRO: TWideStringField
      FieldName = 'NOMEMICRO'
      Size = 100
    end
    object QrSubNOMEMUNICI: TWideStringField
      FieldName = 'NOMEMUNICI'
      Size = 100
    end
    object QrSubDTB_UF: TSmallintField
      FieldName = 'DTB_UF'
      Required = True
    end
    object QrSubDTB_Meso: TIntegerField
      FieldName = 'DTB_Meso'
      Required = True
    end
    object QrSubDTB_Micro: TIntegerField
      FieldName = 'DTB_Micro'
      Required = True
    end
    object QrSubDTB_Munici: TIntegerField
      FieldName = 'DTB_Munici'
      Required = True
    end
    object QrSubDNE_Locali: TIntegerField
      FieldName = 'DNE_Locali'
      Required = True
    end
    object QrSubDNE_Bairro: TIntegerField
      FieldName = 'DNE_Bairro'
      Required = True
    end
    object QrSubDNE_Lograd: TIntegerField
      FieldName = 'DNE_Lograd'
      Required = True
    end
    object QrSubDNE_CEP: TIntegerField
      FieldName = 'DNE_CEP'
      Required = True
    end
    object QrSubDNE_NO_Loc: TWideStringField
      FieldName = 'DNE_NO_Loc'
      Size = 72
    end
    object QrSubDNE_NO_Bai: TWideStringField
      FieldName = 'DNE_NO_Bai'
      Size = 72
    end
    object QrSubDNE_NO_Log: TWideStringField
      FieldName = 'DNE_NO_Log'
      Size = 100
    end
    object QrSubDNE_CEP_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DNE_CEP_TXT'
      Size = 9
      Calculated = True
    end
  end
  object DsSub: TDataSource
    DataSet = QrSub
    Left = 348
    Top = 112
  end
  object QrAgentes: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ent.Codigo,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENT '
      'FROM entidades ent '
      'WHERE Codigo IN ('
      '  SELECT Codigo'
      '  FROM pediacc'
      ')'
      '/*'
      'AND NOT ( Codigo IN ('
      '  SELECT Entidade'
      '  FROM regioes'
      '  WHERE entidade <> :P0)'
      ')'
      '*/')
    Left = 796
    Top = 48
    object QrAgentesCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrAgentesNOMEENT: TWideStringField
      FieldName = 'NOMEENT'
      Required = True
      Size = 100
    end
  end
  object DsAgentes: TDataSource
    DataSet = QrAgentes
    Left = 824
    Top = 48
  end
end
