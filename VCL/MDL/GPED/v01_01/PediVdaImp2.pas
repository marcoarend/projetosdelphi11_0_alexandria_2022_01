unit PediVdaImp2;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Tabs, DockTabSet, ExtCtrls, StdCtrls, Buttons, DBCtrls, (*&&*)UnDmkABS_PF,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, ComCtrls, dmkEditDateTimePicker, DB,
  mySQLDbTables, Grids, DBGrids, frxClass, frxDBSet, dmkDBGrid,
  dmkGeral, UnDmkProcFunc, DmkDAC_PF, dmkImage, ABSMain;

type
  TFmPediVdaImp2 = class(TForm)
    DockTabSet1: TTabSet;
    pDockLeft: TPanel;
    Splitter1: TSplitter;
    Panel1: TPanel;
    Panel2: TPanel;
    Panel3: TPanel;
    BtConfirma: TBitBtn;
    Panel4: TPanel;
    BtDesiste: TBitBtn;
    Qr1: TmySQLQuery;
    Qr1QuantP: TFloatField;
    Qr1QuantC: TFloatField;
    Qr1QuantV: TFloatField;
    Qr1QuantF: TFloatField;
    Qr1ValLiq: TFloatField;
    Qr1CodUsu: TIntegerField;
    Ds1: TDataSource;
    Qr1SEQ: TIntegerField;
    frxDs1: TfrxDBDataset;
    frxPED_INPRI_000_01: TfrxReport;
    BtImprime: TBitBtn;
    Panel5: TPanel;
    LaPVIEmp: TLabel;
    LaPVICli: TLabel;
    LaPVIRep: TLabel;
    LaPVIPrd: TLabel;
    EdPVIPrd: TdmkEditCB;
    EdPVIRep: TdmkEditCB;
    EdPVICli: TdmkEditCB;
    EdPVIEmp: TdmkEditCB;
    CBPVIEmp: TdmkDBLookupComboBox;
    CBPVICli: TdmkDBLookupComboBox;
    CBPVIRep: TdmkDBLookupComboBox;
    CBPVIPrd: TdmkDBLookupComboBox;
    Panel6: TPanel;
    GroupBox1: TGroupBox;
    TPIncluIni: TdmkEditDateTimePicker;
    TPIncluFim: TdmkEditDateTimePicker;
    CkIncluIni: TCheckBox;
    CkIncluFim: TCheckBox;
    GroupBox2: TGroupBox;
    TPEmissIni: TdmkEditDateTimePicker;
    TPEmissFim: TdmkEditDateTimePicker;
    CkEmissIni: TCheckBox;
    CkEmissFim: TCheckBox;
    GroupBox3: TGroupBox;
    TPEntraIni: TdmkEditDateTimePicker;
    TPEntraFim: TdmkEditDateTimePicker;
    CkEntraIni: TCheckBox;
    CkEntraFim: TCheckBox;
    GroupBox4: TGroupBox;
    TPPreviIni: TdmkEditDateTimePicker;
    TPPreviFim: TdmkEditDateTimePicker;
    CkPreviIni: TCheckBox;
    CkPreviFim: TCheckBox;
    Panel7: TPanel;
    RGTipoSaldo: TRadioGroup;
    RGOrdemIts: TRadioGroup;
    Panel8: TPanel;
    QrGraAtrCad: TmySQLQuery;
    QrGraAtrCadCodigo: TIntegerField;
    QrGraAtrCadCodUsu: TIntegerField;
    QrGraAtrCadNome: TWideStringField;
    DsPVI: TDataSource;
    Panel9: TPanel;
    DBGrid2: TDBGrid;
    DBGrid1: TdmkDBGrid;
    frxDsPVI: TfrxDBDataset;
    Qr1QUANTX: TFloatField;
    Qr1VAL_X_: TFloatField;
    Qr1NOME_AGRUP: TWideStringField;
    BtLimpa: TBitBtn;
    RGOrdemAgr_: TRadioGroup;
    Qr1CODI_AGRUP: TFloatField;
    RGAgrupa: TRadioGroup;
    Qr1NO_TAM: TWideStringField;
    Qr1NO_COR: TWideStringField;
    frxPED_INPRI_000_02: TfrxReport;
    Qr1CU_COR: TIntegerField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    Panel10: TPanel;
    CkAgruPGT: TCheckBox;
    Qr1Nome: TWideStringField;
    Qr1Nivel1: TIntegerField;
    Qr1PrdGrupTip: TIntegerField;
    Qr1NO_PGT: TWideStringField;
    GBAvisos1: TGroupBox;
    Panel11: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    QrPVI: TABSQuery;
    QrPVITabela: TWideStringField;
    QrPVICodUsu1: TIntegerField;
    QrPVICodUsu2: TIntegerField;
    QrPVICodUsu3: TIntegerField;
    QrPVINome1: TWideStringField;
    QrPVINome2: TWideStringField;
    QrPVINome3: TWideStringField;
    frxReport1: TfrxReport;
    procedure DockTabSet1TabRemoved(Sender: TObject);
    procedure DockTabSet1DockDrop(Sender: TObject; Source: TDragDockObject; X, Y: Integer);
    procedure pDockLeftDockOver(Sender: TObject; Source: TDragDockObject; X, Y: Integer; State: TDragState;
      var Accept: Boolean);
    procedure pDockLeftUnDock(Sender: TObject; Client: TControl; NewTarget: TWinControl; var Allow: Boolean);
    procedure pDockLeftDockDrop(Sender: TObject; Source: TDragDockObject; X, Y: Integer);
    procedure FormCreate(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure Qr1CalcFields(DataSet: TDataSet);
    procedure BtConfirmaClick(Sender: TObject);
    procedure RGTipoSaldoClick(Sender: TObject);
    procedure RGOrdemAgr_Click(Sender: TObject);
    procedure RGAgrupaClick(Sender: TObject);
    procedure BtImprimeClick(Sender: TObject);
    procedure Qr1BeforeClose(DataSet: TDataSet);
    procedure Qr1AfterOpen(DataSet: TDataSet);
    procedure frxPED_INPRI_000_01GetValue(const VarName: string;
      var Value: Variant);
    procedure FormActivate(Sender: TObject);
    procedure DockTabSet1TabAdded(Sender: TObject);
    procedure BtLimpaClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
  private
    { Private declarations }
    FCriouPVI: Boolean;
    FInRevise: Boolean;
    //
    procedure RecriaDockForms();
    procedure RevisaDockForms();
    procedure CriaQrPVI();
    procedure ListaQrPVI();
    //
    procedure VerificaSQLPrd(Qry: TmySQLQuery);
    procedure VerificaSQLCli(Qry: TmySQLQuery);
    procedure VerificaSQLEmp(Qry: TmySQLQuery);
    procedure VerificaSQLRep(Qry: TmySQLQuery);
    procedure VerificaSQLAtr(Qry: TmySQLQuery);

  public
    { Public declarations }
    //FPVIAtrCli_Txt,
    //FPVIAtrRep_Txt,
    FPVIAtrPrd_Txt: String;
    //FPVIAtrCli_Cod,
    //FPVIAtrRep_Cod,
    FPVIAtrPrd_Cod: Integer;
    //
    procedure AtivaBtConfirma();
  end;

var
  FmPediVdaImp2: TFmPediVdaImp2;

implementation

uses UnMyObjects, UMySQLModule,
// DockForms
PediVdaImpCli2, PediVdaImpPrd2, PediVdaImpEmp2, PediVdaImpRep2, PediVdaImpAtrPrd2,
// FIM DockForms
Module, ModPediVda, ModuleGeral;

{$R *.dfm}

procedure TFmPediVdaImp2.AtivaBtConfirma();
begin
  // Quando fecha as filhas fecham depois e geram um erro silencioso
  try
  BtConfirma.Enabled :=
    (RGTipoSaldo.ItemIndex > -1) and
    //(RGOrdemAgr.ItemIndex > -1) and
    (RGOrdemIts.ItemIndex > -1) and
    (RGAgrupa.ItemIndex > -1);
  finally
    ;
  end;
end;

procedure TFmPediVdaImp2.BtImprimeClick(Sender: TObject);
begin
  case RGAgrupa.ItemIndex of
    4: MyObjects.frxMostra(frxPED_INPRI_000_02, Caption);
    else MyObjects.frxMostra(frxPED_INPRI_000_01, Caption);
  end;
end;

procedure TFmPediVdaImp2.BtLimpaClick(Sender: TObject);
begin
  RecriaDockForms();
  //
  EdPVICli.ValueVariant := 0;
  CBPVICli.KeyValue     := Null;
  EdPVIEmp.ValueVariant := 0;
  CBPVIEmp.KeyValue     := Null;
  EdPVIRep.ValueVariant := 0;
  CBPVIRep.KeyValue     := Null;
  EdPVIPrd.ValueVariant := 0;
  CBPVIPrd.KeyValue     := Null;
  //
  CkEmissFim.Checked    := True;
  CkEmissIni.Checked    := True;
  CkEntraFim.Checked    := True;
  CkEntraIni.Checked    := True;
  CkIncluFim.Checked    := True;
  CkIncluIni.Checked    := True;
  CkPreviFim.Checked    := True;
  CkPreviIni.Checked    := True;
  //
  TPIncluIni.Date := 0;
  TPIncluFim.Date := Date;
  TPEmissIni.Date := 0;
  TPEmissFim.Date := Date;
  TPEntraIni.Date := 0;
  TPEntraFim.Date := Date;
  TPPreviIni.Date := 0;
  TPPreviFim.Date := Date + 180;
  //
  RGTipoSaldo.ItemIndex := -1;
  RGAgrupa.ItemIndex    := -1;
  //RGOrdemAgr.ItemIndex  := -1;
  RGOrdemIts.ItemIndex  := -1;
  //
  Qr1.Close;
(*&&
  QrPVI.Close;
*)
end;

procedure TFmPediVdaImp2.BtConfirmaClick(Sender: TObject);
var
  Continua: Boolean;
  SQL_Temp, OrderByA, OrderByB: String;
  Agrup: Integer;
begin
  case RGAgrupa.ItemIndex of
    3: Agrup := FPVIAtrPrd_Cod;
    //5: Agrup := FPVIAtrCli_Cod;
    //6: Agrup := FPVIAtrRep_Cod;
    else Agrup := 0;
  end;
  //if (RGAgrupa.ItemIndex > 2) and (Agrup = 0) then
  if (RGAgrupa.ItemIndex = 3) and (Agrup = 0) then
  begin
    Geral.MB_Aviso('O agrupamento selecionado: "' +
    RGAgrupa.Items[RGAgrupa.ItemIndex] + '" necessita da sele��o de um ' +
    'atributo! (Abas da lateral direita)');
    Exit;
  end;
  //if not FCriouPVI then
  CriaQrPVI();
  ListaQrPVI();

  Qr1.Close;
  Qr1.SQL.Clear;
  Qr1.SQL.Add('SELECT SUM(pvi.QuantP) QuantP, SUM(pvi.QuantC) QuantC, ');
  Qr1.SQL.Add('SUM(pvi.QuantV) QuantV, ');
  Qr1.SQL.Add('SUM(pvi.QuantP-pvi.QuantC-pvi.QuantV) QuantF, ');
  Qr1.SQL.Add('SUM(pvi.ValLiq) ValLiq, ');
  case RGTipoSaldo.ItemIndex of
    0: SQL_Temp := 'SUM(pvi.QuantP) QUANTX, ';
    1: SQL_Temp := 'SUM(pvi.QuantP-pvi.QuantC-pvi.QuantV) QUANTX, ';
    2: SQL_Temp := 'SUM(pvi.QuantC) QUANTX, ';
    3: SQL_Temp := 'SUM(pvi.QuantV) QUANTX, ';
    else SQL_Temp := '0 QUANTX, ';
  end;
  Qr1.SQL.Add(SQL_Temp);
  case RGTipoSaldo.ItemIndex of
    0: SQL_Temp := 'SUM(pvi.ValLiq) VAL_X_, ';
    1: SQL_Temp := 'SUM(pvi.ValLiq) / SUM(pvi.QuantP) * SUM(pvi.QuantP-pvi.QuantC-pvi.QuantV) VAL_X_, ';
    2: SQL_Temp := 'SUM(pvi.ValLiq) / SUM(pvi.QuantP) * SUM(pvi.QuantC) VAL_X_, ';
    3: SQL_Temp := 'SUM(pvi.ValLiq) / SUM(pvi.QuantP) * SUM(pvi.QuantV) VAL_X_, ';
    else SQL_Temp := '0 VAL_X_, ';
  end;
  Qr1.SQL.Add(SQL_Temp);
  case RGAgrupa.ItemIndex of
    0: Qr1.SQL.Add('gg1.Nome NOME_AGRUP, "" NO_TAM, "" NO_COR, gg1.CodUsu CU_COR, ');
    1: Qr1.SQL.Add('IF(cli.Tipo=0,cli.RazaoSocial,cli.Nome) NOME_AGRUP, "" NO_TAM, "" NO_COR, gg1.CodUsu CU_COR, ');
    2: Qr1.SQL.Add('IF(rep.Tipo=0,rep.RazaoSocial,rep.Nome) NOME_AGRUP, "" NO_TAM, "" NO_COR, gg1.CodUsu CU_COR, ');
    3: Qr1.SQL.Add('IF(gai.Controle IS NULL, "*** ??? ***", gai.Nome) NOME_AGRUP, "" NO_TAM, "" NO_COR, gg1.CodUsu CU_COR, ');
    4: Qr1.SQL.Add('gg1.Nome NOME_AGRUP, gti.Nome NO_TAM, gti.Controle CO_TAM, gcc.Nome NO_COR, gcc.CodUsu CU_COR, ');
    {N�o existe mais
    5: Qr1.SQL.Add('IF(eaicli.Controle IS NULL,"*** ??? ***", eaicli.Nome) NOME_AGRUP, ');
    6: Qr1.SQL.Add('IF(eairep.Controle IS NULL,"*** ??? ***", eairep.Nome) NOME_AGRUP, ');
    }
  end;
  case RGAgrupa.ItemIndex of
    0: Qr1.SQL.Add('gg1.CodUsu + 0.0 CODI_AGRUP, ');
    1: Qr1.SQL.Add('cli.CodUsu + 0.0 CODI_AGRUP, ');
    2: Qr1.SQL.Add('rep.CodUsu + 0.0 CODI_AGRUP, ');
    3: Qr1.SQL.Add('gai.CodUsu + 0.0 CODI_AGRUP, ');
    4: Qr1.SQL.Add('gg1.CodUsu + 0.0 CODI_AGRUP, ');
    {N�o existe mais
    5: Qr1.SQL.Add('eaicli.Controle + 0.0 CODI_AGRUP, ');
    6: Qr1.SQL.Add('eairep.Controle + 0.0 CODI_AGRUP, ');
    }
  end;
  Qr1.SQL.Add('gg1.CodUsu, gg1.Nome, gg1.Nivel1, ');
  Qr1.SQL.Add('gg1.PrdGrupTip, pgt.Nome NO_PGT  ');
  Qr1.SQL.Add('FROM pedivdaits pvi');
  Qr1.SQL.Add('LEFT JOIN pedivda   pvd ON pvd.Codigo=pvi.Codigo');
  Qr1.SQL.Add('LEFT JOIN gragrux   ggx ON ggx.Controle=pvi.GraGruX');
  Qr1.SQL.Add('LEFT JOIN gratamits gti ON gti.Controle=ggx.GraTamI');
  Qr1.SQL.Add('LEFT JOIN gragru1   gg1 ON gg1.Nivel1=ggx.GraGru1');
  Qr1.SQL.Add('LEFT JOIN prdgruptip pgt ON pgt.Codigo=gg1.PrdGrupTip');
  case RGAgrupa.ItemIndex of
    0: Qr1.SQL.Add('LEFT JOIN gragruatr gga ON gga.Nivel1=gg1.Nivel1 AND gga.GraAtrCad=' + FormatFloat('0', FPVIAtrPrd_Cod));
    1: Qr1.SQL.Add('LEFT JOIN entidades cli ON cli.Codigo=pvd.Cliente ');
    2: Qr1.SQL.Add('LEFT JOIN entidades rep ON rep.Codigo=pvd.Represen ');
    3:
    begin
      Qr1.SQL.Add('LEFT JOIN gragruatr gga ON gga.Nivel1=ggx.GraGru1 AND gga.GraAtrCad=' + FormatFloat('0', FPVIAtrPrd_Cod));
      Qr1.SQL.Add('LEFT JOIN graatrits gai ON gai.Controle=gga.GraAtrIts ');
    end;
    4:
    begin
      Qr1.SQL.Add('LEFT JOIN gragruc ggc ON ggc.Controle=ggx.GraGruC');
      Qr1.SQL.Add('LEFT JOIN gracorcad gcc ON gcc.Codigo=ggc.GraCorCad');
    end;
    {
    5:
    begin
      Qr1.SQL.Add('LEFT JOIN entdefatr edacli ON edacli.Entidade=pvd.Cliente AND edacli.EntAtrCad=' + FormatFloat('0', FPVIAtrCli_Cod));
      Qr1.SQL.Add('LEFT JOIN entatrits eaicli ON eaicli.Controle=edacli.EntAtrIts ');
    end;
    6:
    begin
      Qr1.SQL.Add('LEFT JOIN entdefatr edarep ON edarep.Entidade=pvd.Represen AND edarep.EntAtrCad=' + FormatFloat('0', FPVIAtrRep_Cod));
      Qr1.SQL.Add('LEFT JOIN entatrits eairep ON eairep.Controle=edarep.EntAtrIts ');
    end;
    }
  end;
  Continua := True;//ImprimeRel1();
  if Continua then
  begin
    // Quantidade de itens
    case RGTipoSaldo.ItemIndex of
      0: Qr1.SQL.Add('WHERE pvi.QuantP > 0');
      1: Qr1.SQL.Add('WHERE (pvi.QuantP - pvi.QuantC - pvi.QuantV) > 0');
      2: Qr1.SQL.Add('WHERE pvi.QuantC > 0');
      3: Qr1.SQL.Add('WHERE pvi.QuantV > 0');
      else Qr1.SQL.Add('### ERRO RGTipoSaldo.ItemIndex  - ("WHERE ???") ###');
    end;
    // Data da inclus�o
    SQL_Temp := dmkPF.SQL_Periodo('AND pvd.DtaInclu ',
      TPIncluIni.Date, TPIncluFim.Date, CkIncluIni.Checked, CkIncluFim.Checked);
    if SQL_Temp <> '' then
      Qr1.SQL.Add(SQL_Temp);
    //
    // Data da emiss�o
    SQL_Temp := dmkPF.SQL_Periodo('AND pvd.DtaEmiss ',
      TPEmissIni.Date, TPEmissFim.Date, CkEmissIni.Checked, CkEmissFim.Checked);
    if SQL_Temp <> '' then
      Qr1.SQL.Add(SQL_Temp);
    //
    // Data da entrada
    SQL_Temp := dmkPF.SQL_Periodo('AND pvd.DtaEntra ',
      TPEntraIni.Date, TPEntraFim.Date, CkEntraIni.Checked, CkEntraFim.Checked);
    if SQL_Temp <> '' then
      Qr1.SQL.Add(SQL_Temp);
    //
    // Data da previs�o
    SQL_Temp := dmkPF.SQL_Periodo('AND pvd.DtaPrevi ',
      TPPreviIni.Date, TPPreviFim.Date, CkPreviIni.Checked, CkPreviFim.Checked);
    if SQL_Temp <> '' then
      Qr1.SQL.Add(SQL_Temp);
    //
    VerificaSQLPrd(Qr1);
    VerificaSQLCli(Qr1);
    VerificaSQLEmp(Qr1);
    VerificaSQLRep(Qr1);
    VerificaSQLAtr(Qr1);
    //
    //
    case RGAgrupa.Itemindex of
      0: Qr1.SQL.Add('GROUP BY gg1.Nivel1');
      1: Qr1.SQL.Add('GROUP BY cli.CodUsu');
      2: Qr1.SQL.Add('GROUP BY rep.CodUsu');
      3: Qr1.SQL.Add('GROUP BY gai.CodUsu');
      4: Qr1.SQL.Add('GROUP BY gg1.Nivel1, ggx.GraTamI, ggx.GraGruC');
    end;
    //

    {
    case RGOrdemAgr.ItemIndex of
      0: SQL_Temp := 'ORDER BY CODI_AGRUP,';
      1: SQL_Temp := 'ORDER BY NOME_AGRUP,';
    end;
    case RGOrdemIts.ItemIndex of
      0: SQL_Temp := SQL_Temp + 'QUANTX ';
      1: SQL_Temp := SQL_Temp + 'VAL_X_ ';
    end;
    SQL_Temp := SQL_Temp + ' DESC';
    Qr1.SQL.Add(SQL_Temp);
    }
    //
    if CkAgruPGT.Checked then
      OrderByA := 'ORDER BY NO_PGT, pgt.Codigo, '
    else
      OrderByA := 'ORDER BY ';
    //
    if RGAgrupa.ItemIndex = 4 then
    begin
      case RGOrdemIts.ItemIndex of
        0: OrderByB := ' NOME_AGRUP, NO_COR, CO_TAM, QUANTX DESC, VAL_X_ DESC';
        1: OrderByB := ' NOME_AGRUP, NO_COR, CO_TAM, VAL_X_ DESC, QUANTX DESC';
      end;
    end else
    begin
      case RGOrdemIts.ItemIndex of
        0: OrderByB := ' QUANTX DESC, VAL_X_ DESC';
        1: OrderByB := ' VAL_X_ DESC, QUANTX DESC';
      end;
    end;
    Qr1.SQL.Add(OrderByA + OrderByB);
    //
    UMyMod.AbreQuery(Qr1, Dmod.MyDB, 'TFmPediVdaImp2.BtConfirmaClick()');
    //Geral.MB_SQL(Self, Qr1);
  end;
end;

procedure TFmPediVdaImp2.BtDesisteClick(Sender: TObject);
begin
  Close;
end;

procedure TFmPediVdaImp2.DockTabSet1DockDrop(Sender: TObject; Source: TDragDockObject; X, Y: Integer);
begin
  DockTabSet1.Visible := True;
end;

procedure TFmPediVdaImp2.DockTabSet1TabAdded(Sender: TObject);
begin
  DockTabSet1.Visible := DockTabSet1.Tabs.Count > 0;
end;

procedure TFmPediVdaImp2.DockTabSet1TabRemoved(Sender: TObject);
begin
  DockTabSet1.Visible := DockTabSet1.Tabs.Count > 0;
  if FInRevise then Exit;
  RevisaDockForms();
end;

procedure TFmPediVdaImp2.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmPediVdaImp2.FormCreate(Sender: TObject);
begin
  RecriaDockForms();
  UnDmkDAC_PF.AbreQuery(QrGraAtrCad, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(DmPediVda.QrPVIEmpCad, Dmod.MyDB);
  TPIncluIni.Date := 0;
  TPIncluFim.Date := Date;
  TPEmissIni.Date := 0;
  TPEmissFim.Date := Date;
  TPEntraIni.Date := 0;
  TPEntraFim.Date := Date;
  TPPreviIni.Date := 0;
  TPPreviFim.Date := Date + 180;
  //
  CBPVIEmp.ListSource := DmPediVda.DsPVIEmpCad; //DModG.DsEmpresas;
  CBPVICli.ListSource := DmPediVda.DsClientes;
  CBPVIRep.ListSource := DmPediVda.DsPediAcc;
  //CBPVIPrd.ListSource := DmPediVda.DsGraGraGru1????;
  //
  EdPVIEmp.ValueVariant := DModG.QrFiliLogFilial.Value;
  CBPVIEmp.KeyValue     := DModG.QrFiliLogFilial.Value;
end;

procedure TFmPediVdaImp2.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmPediVdaImp2.frxPED_INPRI_000_01GetValue(const VarName: string;
  var Value: Variant);
begin
  if VarName = 'VARF_SUB_TITULO' then
    Value := 'QUANTIDADE ' +
      Geral.Maiusculas(RGTipoSaldo.Items[RGTipoSaldo.ItemIndex], False) +
      ' POR ' + Geral.Maiusculas(RGAgrupa.Items[RGAgrupa.ItemIndex], False)
  else
  if VarName = 'VARF_PERIODO_INCLU' then
    Value := dmkPF.PeriodoImp2(TPIncluIni.Date, TPIncluFim.Date,
      CkIncluIni.Checked, CkIncluFim.Checked, '', '', '')
  else
  if VarName = 'VARF_PERIODO_EMISS' then
    Value := dmkPF.PeriodoImp2(TPEmissIni.Date, TPEmissFim.Date,
      CkEmissIni.Checked, CkEmissFim.Checked, '', '', '')
  else
  if VarName = 'VARF_PERIODO_ENTRA' then
    Value := dmkPF.PeriodoImp2(TPEntraIni.Date, TPEntraFim.Date,
      CkEntraIni.Checked, CkEntraFim.Checked, '', '', '')
  else
  if VarName = 'VARF_PERIODO_PREVI' then
    Value := dmkPF.PeriodoImp2(TPPreviIni.Date, TPPreviFim.Date,
      CkPreviIni.Checked, CkPreviFim.Checked, '', '', '')
  else
  if VarName = 'VARF_AGRUPA1' then
    Value := CkAgruPGT.Checked;
end;

procedure TFmPediVdaImp2.pDockLeftDockDrop(Sender: TObject; Source: TDragDockObject; X, Y: Integer);
begin
  if pDockLeft.Width = 0 then
    pDockLeft.Width := 150;
  Splitter1.Visible := True;
  Splitter1.Left := pDockLeft.Width;
end;

procedure TFmPediVdaImp2.pDockLeftDockOver(Sender: TObject; Source: TDragDockObject; X, Y: Integer; State: TDragState;
  var Accept: Boolean);
var
  lRect: TRect;
begin
  Accept := Source.Control is TFmPediVdaImpCli2;
  if Accept then
  begin
    lRect.TopLeft := pDockLeft.ClientToScreen(Point(0, 0));
    lRect.BottomRight := pDockLeft.ClientToScreen(Point(150, pDockLeft.Height));
    Source.DockRect := lRect;
  end;
end;

procedure TFmPediVdaImp2.pDockLeftUnDock(Sender: TObject; Client: TControl; NewTarget: TWinControl; var Allow: Boolean);
begin
  if pDockLeft.DockClientCount = 1 then
  begin
    pDockLeft.Width := 0;
    Splitter1.Visible := False;
  end;
end;

procedure TFmPediVdaImp2.Qr1AfterOpen(DataSet: TDataSet);
begin
  BtImprime.Enabled := Qr1.RecordCount > 0;
end;

procedure TFmPediVdaImp2.Qr1BeforeClose(DataSet: TDataSet);
begin
  BtImprime.Enabled := False;
end;

procedure TFmPediVdaImp2.Qr1CalcFields(DataSet: TDataSet);
begin
  Qr1SEQ.Value := Qr1.RecNo;
end;

procedure TFmPediVdaImp2.RecriaDockForms();
var
  i: Integer;
begin
  Screen.Cursor := crHourGlass;
  try
    for i := 0 to Screen.FormCount - 1 do
    begin
      if Screen.Forms[i] is TFmPediVdaImpEmp2 then
        Screen.Forms[i].Close;
      if Screen.Forms[i] is TFmPediVdaImpCli2 then
        Screen.Forms[i].Close;
      if Screen.Forms[i] is TFmPediVdaImpRep2 then
        Screen.Forms[i].Close;
      if Screen.Forms[i] is TFmPediVdaImpPrd2 then
        Screen.Forms[i].Close;
      {if Screen.Forms[i] is TFmPediVdaImpAtrCli2 then
        Screen.Forms[i].Close;
      if Screen.Forms[i] is TFmPediVdaImpAtrRep2 then
        Screen.Forms[i].Close;}
      if Screen.Forms[i] is TFmPediVdaImpAtrPrd2 then
        Screen.Forms[i].Close;
    end;
    TFmPediVdaImpEmp2.CreateDockForm(clWhite).ManualDock(DockTabSet1);
    TFmPediVdaImpCli2.CreateDockForm(clWhite).ManualDock(DockTabSet1);
    TFmPediVdaImpRep2.CreateDockForm(clWhite).ManualDock(DockTabSet1);
    TFmPediVdaImpPrd2.CreateDockForm(clWhite).ManualDock(DockTabSet1);
    //TFmPediVdaImpAtrCli2.CreateDockForm(clWhite).ManualDock(DockTabSet1);
    //TFmPediVdaImpAtrRep2.CreateDockForm(clWhite).ManualDock(DockTabSet1);
    TFmPediVdaImpAtrPrd2.CreateDockForm(clWhite).ManualDock(DockTabSet1);
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmPediVdaImp2.RevisaDockForms();
const
  Janelas: array [0..6] of String = ('Empresas', 'Clientes', 'Representantes',
  'Grupos de Produtos', 'Atributos de Clientes', 'Atributos de Representantes',
  'Atributos de Produtos');
var
  I, J, K: Integer;
  Achou: Boolean;
  //CustomForm: TCustomForm;
begin
  FInRevise := True;
  //for J := Low(Janelas) to High(Janelas) do
  for J := 0 to 6 do
  begin
    Achou := False;
    for I  := 0 to DockTabSet1.Tabs.Count - 1 do
    begin
      if Uppercase(Janelas[J]) = Uppercase(DockTabSet1.Tabs[I]) then
      begin
        Achou := True;
        Break;
      end;
    end;
    if not Achou then
    begin
      case J of
        0:
        begin
          for K := 0 to Screen.FormCount - 1 do
          begin
            if Screen.Forms[K] is TFmPediVdaImpEmp2 then
            begin
              Screen.Forms[K].Close;
              TFmPediVdaImpEmp2.CreateDockForm(clWhite).ManualDock(DockTabSet1);
            end;
          end;
        end;
        1:
        begin
          for K := 0 to Screen.FormCount - 1 do
          begin
            if Screen.Forms[K] is TFmPediVdaImpCli2 then
            begin
              Screen.Forms[K].Close;
              TFmPediVdaImpCli2.CreateDockForm(clWhite).ManualDock(DockTabSet1);
            end;
          end;
        end;
        2:
        begin
          for K := 0 to Screen.FormCount - 1 do
          begin
            if Screen.Forms[K] is TFmPediVdaImpRep2 then
            begin
              Screen.Forms[K].Close;
              TFmPediVdaImpRep2.CreateDockForm(clWhite).ManualDock(DockTabSet1);
            end;
          end;
        end;
        3:
        begin
          for K := 0 to Screen.FormCount - 1 do
          begin
            if Screen.Forms[K] is TFmPediVdaImpPrd2 then
            begin
              Screen.Forms[K].Close;
              TFmPediVdaImpPrd2.CreateDockForm(clWhite).ManualDock(DockTabSet1);
            end;
          end;
        end;
        {
        4:
        begin
          for K := 0 to Screen.FormCount - 1 do
          begin
            if Screen.Forms[K] is TFmPediVdaImpAtrCli2 then
            begin
              Screen.Forms[K].Close;
              TFmPediVdaImpAtrCli2.CreateDockForm(clWhite).ManualDock(DockTabSet1);
            end;
          end;
        end;
        5:
        begin
          for K := 0 to Screen.FormCount - 1 do
          begin
            if Screen.Forms[K] is TFmPediVdaImpAtrRep2 then
            begin
              Screen.Forms[K].Close;
              TFmPediVdaImpAtrRep2.CreateDockForm(clWhite).ManualDock(DockTabSet1);
            end;
          end;
        end;
        }
        6:
        begin
          for K := 0 to Screen.FormCount - 1 do
          begin
            if Screen.Forms[K] is TFmPediVdaImpAtrPrd2 then
            begin
              Screen.Forms[K].Close;
              TFmPediVdaImpAtrPrd2.CreateDockForm(clWhite).ManualDock(DockTabSet1);
            end;
          end;
        end;
        //
      end;
    end;
  end;
  FInRevise := False;
end;

procedure TFmPediVdaImp2.RGAgrupaClick(Sender: TObject);
begin
  DBGrid1.Columns[2].Visible := RGAgrupa.ItemIndex = 4;
  DBGrid1.Columns[3].Visible := RGAgrupa.ItemIndex = 4;
  DBGrid1.Columns[2].Width   := 150;
  DBGrid1.Columns[3].Width   := 50;
  //
  AtivaBtConfirma();
end;

procedure TFmPediVdaImp2.RGOrdemAgr_Click(Sender: TObject);
begin
  AtivaBtConfirma();
end;

procedure TFmPediVdaImp2.RGTipoSaldoClick(Sender: TObject);
begin
  AtivaBtConfirma();
end;

procedure TFmPediVdaImp2.VerificaSQLPrd(Qry: TmySQLQuery);
var
  Sep, Aux: String;
begin
(*&&*)
  if EdPVIPrd.Enabled  then
  begin
    if EdPVIPrd.ValueVariant <> 0 then
    begin
      Aux := 'AND ggx.gragru1 = ' +
        FormatFloat('0', DmPediVda.QrPVIPrdCadNivel1.Value);
      Qry.SQL.Add(Aux);
    end;
  end else begin
    {
    FilterTemp := DmPediVda.QrPVIPrdIts.Filter;
    FilterStatus := DmPediVda.QrPVIPrdIts.Filtered;
    //
    DmPediVda.QrPVIPrdIts.Filtered := False;
    DmPediVda.QrPVIPrdIts.Filter := 'Ativo=1';
    DmPediVda.QrPVIPrdIts.Filtered := True;
    }
    if DmPediVda.QrPVIPrdIts.RecordCount > 0 then
    begin
      Sep := '';
      Aux := 'AND ggx.gragru1 IN (';
      DmPediVda.QrPVIPrdIts.First;
      while not DmPediVda.QrPVIPrdIts.Eof do
      begin
        Aux := Aux + Sep + FormatFloat('0', DmPediVda.QrPVIPrdItsCodigo.Value);
        Sep := ',';
        //
        DmPediVda.QrPVIPrdIts.Next;
      end;
      Qry.SQL.Add(Aux + ')');
    end;
    {
    DmPediVda.QrPVIPrdIts.Filter := FilterTemp;
    DmPediVda.QrPVIPrdIts.Filtered := FilterStatus;
    }
  end;
(**)
end;

procedure TFmPediVdaImp2.VerificaSQLCli(Qry: TmySQLQuery);
var
  Sep, Aux: String;
begin
  if EdPVICli.Enabled  then
  begin
    if EdPVICli.ValueVariant <> 0 then
    begin
      Aux := 'AND pvd.Cliente = ' + FormatFloat('0', EdPVICli.ValueVariant);
      Qry.SQL.Add(Aux);
    end;
  end else begin
    if DmPediVda.TbPVICliIts.RecordCount > 0 then
    begin
      Sep := '';
      Aux := 'AND pvd.Cliente IN (';
      DmPediVda.TbPVICliIts.First;
      while not DmPediVda.TbPVICliIts.Eof do
      begin
        Aux := Aux + Sep + FormatFloat('0', DmPediVda.TbPVICliItsCodigo.Value);
        Sep := ',';
        //
        DmPediVda.TbPVICliIts.Next;
      end;
      Qry.SQL.Add(Aux + ')');
    end;
  end;
end;

procedure TFmPediVdaImp2.VerificaSQLEmp(Qry: TmySQLQuery);
var
  Sep, Aux: String;
begin
(*&&
  if EdPVIEmp.Enabled  then
  begin
    if EdPVIEmp.ValueVariant <> 0 then
    begin
      Aux := 'AND pvd.Empresa = ' + FormatFloat('0',
        DmPediVda.QrPVIEmpCadCodigo.Value);
      Qry.SQL.Add(Aux);
    end;
  end else begin
    {
    FilterTemp := DmPediVda.QrPVIEmpIts.Filter;
    FilterStatus := DmPediVda.QrPVIEmpIts.Filtered;
    //
    DmPediVda.QrPVIEmpIts.Filtered := False;
    DmPediVda.QrPVIEmpIts.Filter := 'Ativo=1';
    DmPediVda.QrPVIEmpIts.Filtered := True;
    }
    if DmPediVda.QrPVIEmpIts.RecordCount > 0 then
    begin
      Sep := '';
      Aux := 'AND pvd.Empresa IN (';
      DmPediVda.QrPVIEmpIts.First;
      while not DmPediVda.QrPVIEmpIts.Eof do
      begin
        Aux := Aux + Sep + FormatFloat('0', DmPediVda.QrPVIEmpItsCodigo.Value);
        Sep := ',';
        //
        DmPediVda.QrPVIEmpIts.Next;
      end;
      Qry.SQL.Add(Aux + ')');
    end;
    {
    DmPediVda.QrPVIEmpIts.Filter := FilterTemp;
    DmPediVda.QrPVIEmpIts.Filtered := FilterStatus;
    }
  end;
*)
end;

procedure TFmPediVdaImp2.VerificaSQLRep(Qry: TmySQLQuery);
var
  Sep, Aux: String;
begin
(*&&
  if EdPVIRep.Enabled  then
  begin
    if EdPVIRep.ValueVariant <> 0 then
    begin
      Aux := 'AND pvd.Represen = ' + FormatFloat('0', EdPVIRep.ValueVariant);
      Qry.SQL.Add(Aux);
    end;
  end else begin
    {
    FilterTemp := DmPediVda.QrPVIRepIts.Filter;
    FilterStatus := DmPediVda.QrPVIRepIts.Filtered;
    //
    DmPediVda.QrPVIRepIts.Filtered := False;
    DmPediVda.QrPVIRepIts.Filter := 'Ativo=1';
    DmPediVda.QrPVIRepIts.Filtered := True;
    }
    if DmPediVda.QrPVIRepIts.RecordCount > 0 then
    begin
      Sep := '';
      Aux := 'AND pvd.Represen IN (';
      DmPediVda.QrPVIRepIts.First;
      while not DmPediVda.QrPVIRepIts.Eof do
      begin
        Aux := Aux + Sep + FormatFloat('0', DmPediVda.QrPVIRepItsCodigo.Value);
        Sep := ',';
        //
        DmPediVda.QrPVIRepIts.Next;
      end;
      Qry.SQL.Add(Aux + ')');
    end;
    {
    DmPediVda.QrPVIRepIts.Filter := FilterTemp;
    DmPediVda.QrPVIRepIts.Filtered := FilterStatus;
    }
  end;
*)
end;

procedure TFmPediVdaImp2.VerificaSQLAtr(Qry: TmySQLQuery);
var
  Sep, Aux: String;
(*&&
  QrABS: TABSQuery;
*)
begin
(*&&
  //if RGAgrupa.ItemIndex > 2 then
  if RGAgrupa.ItemIndex = 3 then
  begin
    case RGAgrupa.ItemIndex of
      3: QrABS := DmPediVda.QrPVIAtrPrdIts;
      {
      5: QrABS := DmPediVda.QrPVIAtrCliIts;
      6: QrABS := DmPediVda.QrPVIAtrRepIts;
      }
      else QrABS := nil;
    end;
    if QrABS.RecordCount > 0 then
    begin
      Sep := '';
      case RGAgrupa.ItemIndex of
        3: Aux := 'AND gga.GraAtrIts IN (';
        {
        5: Aux := 'AND eda.EntAtrIts IN (';
        6: Aux := 'AND eda.EntAtrIts IN (';
        }
        else QrABS := nil;
      end;
      //
      QrABS.First;
      while not QrABS.Eof do
      begin
        Aux := Aux + Sep + FormatFloat('0', QrABS.FieldByName('Codigo').AsInteger);
        Sep := ',';
        //
        QrABS.Next;
      end;
      Qry.SQL.Add(Aux + ')');
    end;
  end;
*)
end;

procedure TFmPediVdaImp2.CriaQrPVI();
const
  Txt1 = 'INSERT INTO pvi (Codigo,CodUsu,Nome,Ativo) VALUES(';
begin
(*&&
  QrPVI.Close;
  QrPVI.SQL.Clear;
  QrPVI.SQL.Add('DROP TABLE PVI;         ');
  QrPVI.SQL.Add('CREATE TABLE PVI (      ');
  QrPVI.SQL.Add('  Tabela  varchar(30)  ,');
  QrPVI.SQL.Add('  CodUsu1 integer      ,');
  QrPVI.SQL.Add('  Nome1   varchar(50)  ,');
  QrPVI.SQL.Add('  CodUsu2 integer      ,');
  QrPVI.SQL.Add('  Nome2   varchar(50)  ,');
  QrPVI.SQL.Add('  CodUsu3 integer      ,');
  QrPVI.SQL.Add('  Nome3   varchar(50)  ,');
  QrPVI.SQL.Add('  Ativo   smallint      ');
  QrPVI.SQL.Add(');                      ');
  //
  QrPVI.SQL.Add('SELECT * FROM pvi;');
  DmkABS_PF.AbreQuery(QrPVI);
  //
  FCriouPVI := True;
*)
end;

procedure TFmPediVdaImp2.ListaQrPVI();
(*&&*)
  procedure AdicionaItensTabela(Tabela: String; Qry: TABSQuery);
  const
    Txt1 = 'INSERT INTO pvi (Tabela,' +
    'CodUsu1,Nome1,CodUsu2,Nome2,CodUsu3,Nome3,Ativo) VALUES(';
  var
    Txt2: String;
    j: Integer;
    Str1, Str2, Str3: String;
  begin
    j := 0;
    Qry.First;
    while not Qry.Eof do
    begin
      if Qry.FieldByName('Ativo').AsInteger = 1 then
      begin
        if j = 0 then
        begin
          Txt2 := '"' + Tabela + '",';
          Str1 := '0," ",';
          Str2 := '0," ",';
          Str3 := '0," ",1);';
          j := 1;
        end;
        case j of
          1:
          begin
            Str1 :=
            dmkPF.FFP(Qry.FieldByName('CodUsu').AsInteger, 0) + ',' +
            '"' + Qry.FieldByName('Nome').AsString + '",';
            j := 2;
          end;
          2:
          begin
            Str2 :=
            dmkPF.FFP(Qry.FieldByName('CodUsu').AsInteger, 0) + ',' +
            '"' + Qry.FieldByName('Nome').AsString + '",';
            j := 3;
          end;
          3:
          begin
            Str3 :=
            dmkPF.FFP(Qry.FieldByName('CodUsu').AsInteger, 0) + ',' +
            '"' + Qry.FieldByName('Nome').AsString + '",1);';
            //
            QrPVI.SQL.Add(Txt1 + Txt2 + Str1 + Str2 + Str3);
            j := 0;
          end;
        end;
      end;
      Qry.Next;
    end;
    if j > 0 then
      QrPVI.SQL.Add(Txt1 + Txt2 + Str1 + Str2 + Str3);
  end;
  var
    AtribPrd: String;
begin
  QrPVI.Close;
  QrPVI.SQL.Clear;
  QrPVI.SQL.Add('DELETE FROM pvi; ');
  //
  AtribPrd := 'Atributo de produto: ' + FPVIAtrPrd_Txt;
  {
  AtribCli := 'Atributo de cliente: ' + FPVIAtrCli_Txt;
  AtribRep := 'Atributo de representante: ' + FPVIAtrRep_Txt;
  }
  //
  AdicionaItensTabela('Grupo de produto',          DmPediVda.QrPVIPrdIts);
  //AdicionaItensTabela('Cliente',                   DmPediVda.TbPVICliIts);
(*&&
  AdicionaItensTabela('Filial',                    DmPediVda.QrPVIEmpIts);
  AdicionaItensTabela('Representante',             DmPediVda.QrPVIRepIts);
  AdicionaItensTabela(AtribPrd,                    DmPediVda.QrPVIAtrPrdIts);
  {
  AdicionaItensTabela(AtribCli,                    DmPediVda.QrPVIAtrCliIts);
  AdicionaItensTabela(AtribRep,                    DmPediVda.QrPVIAtrRepIts);
  }
  //
*)
  QrPVI.SQL.Add('SELECT * FROM pvi;');
  DmkABS_PF.AbreQuery(QrPVI);
end;

(*&&
{
object DockTabSet1: TDockTabSet
  Left = 988
  Top = 48
  Width = 20
  Height = 551
  Align = alRight
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Tahoma'
  Font.Style = [fsBold]
  ShrinkToFit = True
  Style = tsModernPopout
  TabPosition = tpLeft
  DockSite = False
  DestinationDockSite = pDockLeft
  OnDockDrop = DockTabSet1DockDrop
  OnTabAdded = DockTabSet1TabAdded
  OnTabRemoved = DockTabSet1TabRemoved
end
}
*)
end.

