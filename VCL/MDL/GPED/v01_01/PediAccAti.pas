unit PediAccAti;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, dmkGeral, Variants, DB, mySQLDbTables,
  DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkLabel, dmkImage,
  UnDmkEnums, DmkDAC_PF;

type
  TFmPediAccAti = class(TForm)
    Panel1: TPanel;
    RGQuali: TRadioGroup;
    QrEntidades: TmySQLQuery;
    DsEntidades: TDataSource;
    QrEntidadesCodigo: TIntegerField;
    QrEntidadesNOMEENT: TWideStringField;
    EdEntidade: TdmkEditCB;
    CBEntidade: TdmkDBLookupComboBox;
    Label1: TLabel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel3: TPanel;
    PnSaiDesis: TPanel;
    BtOK: TBitBtn;
    BtSaida: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure RGQualiClick(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenEntidades();
  public
    { Public declarations }
    FCadastro: Integer;
  end;

  var
  FmPediAccAti: TFmPediAccAti;

implementation

uses UnMyObjects, Module, UMySQLModule, PediAcc, UnInternalConsts;

{$R *.DFM}

procedure TFmPediAccAti.BtOKClick(Sender: TObject);
var
  Codigo: Integer;
begin
  Codigo := Geral.IMV(EdEntidade.Text);
  if UMyMod.ExecSQLInsUpdFm(Self, stIns, 'pediacc', 0, Dmod.QrUpd) then
  begin
    if Geral.MB_Pergunta('A entidade "' + CBEntidade.Text +'" foi ' +
      'ativada com sucesso! Deseja edit�-la?') = ID_YES
    then
      FCadastro := Codigo;
    FmPediAcc.LocCod(Codigo, Codigo);
    Close;
  end;
end;

procedure TFmPediAccAti.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmPediAccAti.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmPediAccAti.FormCreate(Sender: TObject);
  procedure DefineTextoItem(Item: Integer; Texto: String);
  begin
    if Texto <> '' then
      RGQuali.Items[Item] := Texto
    else
      RGQuali.Items[Item] := '[N�o definido]'
  end;
begin
  ImgTipo.SQLType := stLok;
  //
  FCadastro := 0;
  DefineTextoItem(1, VAR_FORNECE1);
  DefineTextoItem(2, VAR_FORNECE2);
  DefineTextoItem(3, VAR_FORNECE3);
  DefineTextoItem(4, VAR_FORNECE4);
  DefineTextoItem(5, VAR_FORNECE5);
  DefineTextoItem(6, VAR_FORNECE6);
  DefineTextoItem(7, VAR_FORNECE7);
  DefineTextoItem(8, VAR_FORNECE8);
end;

procedure TFmPediAccAti.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmPediAccAti.RGQualiClick(Sender: TObject);
begin
  ReopenEntidades();
end;

procedure TFmPediAccAti.ReopenEntidades();
var
  Entidade: Integer;
begin
  Screen.Cursor := crHourGlass;
  Entidade := Geral.IMV(EdEntidade.Text);
  QrEntidades.Close;
  if RGQuali.ItemIndex > 0 then
  begin
    QrEntidades.SQL.Clear;
    QrEntidades.SQL.Add('SELECT Codigo,');
    QrEntidades.SQL.Add('IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENT');
    QrEntidades.SQL.Add('FROM entidades ent');
    QrEntidades.SQL.Add('WHERE ent.Codigo>0');
    QrEntidades.SQL.Add('AND NOT (ent.Codigo IN (');
    QrEntidades.SQL.Add('  SELECT Codigo');
    QrEntidades.SQL.Add('  FROM pediacc');
    QrEntidades.SQL.Add('))');
    case RGQuali.ItemIndex of
      1: QrEntidades.SQL.Add('AND Fornece1="V"');
      2: QrEntidades.SQL.Add('AND Fornece2="V"');
      3: QrEntidades.SQL.Add('AND Fornece3="V"');
      4: QrEntidades.SQL.Add('AND Fornece4="V"');
      5: QrEntidades.SQL.Add('AND Fornece5="V"');
      6: QrEntidades.SQL.Add('AND Fornece6="V"');
      7: QrEntidades.SQL.Add('AND Fornece7="V"');
      8: QrEntidades.SQL.Add('AND Fornece8="V"');
    end;
    QrEntidades.SQL.Add('ORDER BY NOMEENT');
    UnDmkDAC_PF.AbreQuery(QrEntidades, Dmod.MyDB);
  end;
  if not QrEntidades.Locate('Codigo', Entidade, []) then
  begin
    EdEntidade.Text     := '';
    CBEntidade.KeyValue := Null;
  end;
  Screen.Cursor := crDefault;
end;

end.

