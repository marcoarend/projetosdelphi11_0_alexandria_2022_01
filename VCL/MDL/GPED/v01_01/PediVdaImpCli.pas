unit PediVdaImpCli;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DB, DBGrids, dmkDBGrid, DBCtrls,
  mySQLDbTables, Menus, ComCtrls, (*&&UnDmkABS_PF,*) DmkDAC_PF;

type
  THackDBGrid = class(TDBGrid);
  TFmPediVdaImpCli = class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    BtNenhum: TBitBtn;
    BtTodos: TBitBtn;
    BtPesq: TBitBtn;
    PMPesq: TPopupMenu;
    Ativa1: TMenuItem;
    Desativa1: TMenuItem;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    Panel3: TPanel;
    Label1: TLabel;
    EdPesq: TEdit;
    DBGrid1: TdmkDBGrid;
    Panel4: TPanel;
    DBGrid2: TdmkDBGrid;
    RGSelecao: TRadioGroup;
    procedure FormCreate(Sender: TObject);
    procedure DBGrid1CellClick(Column: TColumn);
    procedure FormStartDock(Sender: TObject; var DragObject: TDragDockObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure EdPesqChange(Sender: TObject);
    procedure FormUnDock(Sender: TObject; Client: TControl;
      NewTarget: TWinControl; var Allow: Boolean);
    procedure BtNenhumClick(Sender: TObject);
    procedure BtTodosClick(Sender: TObject);
    procedure BtPesqClick(Sender: TObject);
    procedure Ativa1Click(Sender: TObject);
    procedure Desativa1Click(Sender: TObject);
    procedure PageControl1Change(Sender: TObject);
    procedure FormDeactivate(Sender: TObject);
    procedure RGSelecaoClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    FPVICli: String;
    procedure Pesquisar();
    procedure AtivaItens(Status: Integer);
    procedure AtivaSelecionados(Status: Integer);
    procedure HabilitaItemUnico();
  public
    { Public declarations }
    class function CreateDockForm(const aColor: TColor): TCustomForm;
    //class procedure FiltaAtivos();
  end;

var
  FmPediVdaImpCli: TFmPediVdaImpCli;

implementation

uses UnMyObjects, Module, UMySQLModule, TabePrcCab, PediVdaImp, ModPediVda, ModuleGeral,
UCreate, UnInternalConsts, MyGlyfs;

{$R *.DFM}

///// PROCEDURES E FUNCTIONS DO DOCKFORM

procedure TFmPediVdaImpCli.Ativa1Click(Sender: TObject);
begin
  AtivaSelecionados(1);
end;

procedure TFmPediVdaImpCli.AtivaItens(Status: Integer);
begin
  Screen.Cursor := crHourGlass;
  DmPediVda.TbPVICliIts.Edit;
  DmPediVda.TbPVICliItsAtivo.Value := Status;
  DmPediVda.TbPVICliIts.Post;
  HabilitaItemUnico();
  Screen.Cursor := crDefault;
end;

procedure TFmPediVdaImpCli.AtivaSelecionados(Status: Integer);
begin
  Screen.Cursor := crHourGlass;
  DmPediVda.TbPVICliIts.First;
  while not DmPediVda.TbPVICliIts.Eof do
  begin
    if DmPediVda.TbPVICliItsAtivo.Value <> Status then
    begin
      DmPediVda.TbPVICliIts.Edit;
      DmPediVda.TbPVICliItsAtivo.Value := Status;
      DmPediVda.TbPVICliIts.Post;
    end;
    DmPediVda.TbPVICliIts.Next;
  end;
  HabilitaItemUnico();
  Screen.Cursor := crDefault;
end;

procedure TFmPediVdaImpCli.BtPesqClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMPesq, BtPesq);
end;

procedure TFmPediVdaImpCli.BtNenhumClick(Sender: TObject);
begin
  AtivaItens(0);
end;

procedure TFmPediVdaImpCli.BtTodosClick(Sender: TObject);
begin
  AtivaItens(1);
end;

class function TFmPediVdaImpCli.CreateDockForm(const aColor: TColor): TCustomForm;
begin
  result := TFmPediVdaImpCli.Create(Application);
  //result.Color := aColor;
  //result.Caption := ColorToString(aColor);
  FmMyGlyfs.ConfiguraFormDock(result);
  result.Show;
end;

procedure TFmPediVdaImpCli.FormClose(Sender: TObject; var Action: TCloseAction);
begin
(*&&
  //Ini Marcelo 20/09/2011
  DmPediVda.QrPVIAtrCliAti.Close;
  DmPediVda.QrPVIAtrCliAti.SQL.Clear;
  DmPediVda.QrPVIAtrCliAti.SQL.Add('DROP TABLE PVIEmp; ');
  DmPediVda.QrPVIAtrCliAti.ExecSQL;
  //Fim Marcelo 20/09/2011
  //
  {
  DmodG.QrUpdPID1.Close;
  DmodG.QrUpdPID1.SQL.Clear;
  DmodG.QrUpdPID1.SQL.Add('DROP TABLE ' + FPVICli + '; ');
  UMyMod.ExecutaQuery(DmodG.QrUpdPID1);
  //
  }
  ManualFloat(Rect(0, 0, 0, 0));
  Action := caFree;
*)
end;

procedure TFmPediVdaImpCli.FormShow(Sender: TObject);
begin
  FmPediVdaImp.BtConfirma.Enabled := False;
end;

procedure TFmPediVdaImpCli.FormStartDock(Sender: TObject;
  var DragObject: TDragDockObject);
begin
  DragObject := TDragDockObjectEx.Create(Self);
  DragObject.Brush.Color := clAqua; // this will display a red outline
end;


procedure TFmPediVdaImpCli.FormUnDock(Sender: TObject; Client: TControl;
  NewTarget: TWinControl; var Allow: Boolean);
begin
end;

///// FIM DAS PROCEDURES E FUNCTIONS DO DOCKFORM

procedure TFmPediVdaImpCli.PageControl1Change(Sender: TObject);
begin
  BtPesq.Enabled := PageControl1.ActivePageIndex = 0;
  case PageControl1.ActivePageIndex of
    0: Pesquisar();
    1: RGSelecaoClick(Self);
  end;
end;

procedure TFmPediVdaImpCli.Pesquisar();
begin
  Screen.Cursor := crHourGlass;
  //
  DmPediVda.TbPVICliIts.Filter   := 'Nome LIKE "%' + EdPesq.Text + '%"';
  DmPediVda.TbPVICliIts.Filtered := True;
  //
  Screen.Cursor := crDefault;
end;

procedure TFmPediVdaImpCli.RGSelecaoClick(Sender: TObject);
begin
  DmPediVda.TbPVICliIts.Filtered := False;
  case RGSelecao.ItemIndex of
    0: DmPediVda.TbPVICliIts.Filter := 'Ativo=0';
    1: DmPediVda.TbPVICliIts.Filter := 'Ativo=1';
    2: DmPediVda.TbPVICliIts.Filter := '';
  end;
  if RGSelecao.ItemIndex < 2 then
    DmPediVda.TbPVICliIts.Filtered := True;
end;

procedure TFmPediVdaImpCli.DBGrid1CellClick(Column: TColumn);
var
  Status: Integer;
begin
  if Column.FieldName = 'Ativo' then
  begin
    if DmPediVda.TbPVICliIts.FieldByName('Ativo').Value = 1 then
      Status := 0
    else
      Status := 1;
    //
    DmPediVda.TbPVICliIts.Edit;
    DmPediVda.TbPVICliIts.FieldByName('Ativo').Value := Status;
    DmPediVda.TbPVICliIts.Post;
  end;
  HabilitaItemUnico();
end;

procedure TFmPediVdaImpCli.HabilitaItemUnico();
var
  Habilita: Boolean;
begin
  DmPediVda.QrPVICliAti.Close;
  DmPediVda.QrPVICliAti.Database := DModG.MyPID_DB;
  UnDmkDAC_PF.AbreQuery(DmPediVda.QrPVICliAti, DModG.MyPID_DB);
  Habilita :=  DmPediVda.QrPVICliAtiItens.Value = 0;
  //
  FmPediVdaImp.LaPVICli.Enabled := Habilita;
  FmPediVdaImp.EdPVICli.Enabled := Habilita;
  FmPediVdaImp.CBPVICli.Enabled := Habilita;
  if not Habilita then
  begin
    FmPediVdaImp.EdPVICli.ValueVariant := 0;
    FmPediVdaImp.CBPVICli.KeyValue     := 0;
  end;
end;


procedure TFmPediVdaImpCli.Desativa1Click(Sender: TObject);
begin
  AtivaSelecionados(0);
end;

procedure TFmPediVdaImpCli.EdPesqChange(Sender: TObject);
begin
  Pesquisar();
end;

procedure TFmPediVdaImpCli.FormCreate(Sender: TObject);
{const
  Txt1 = 'INSERT INTO pvicli (Codigo,CodUsu,Nome,Ativo) VALUES(';
var
  Txt2: String;
begin
  DmPediVda.TbPVICliIts.Close;
  DmPediVda.TbPVICliIts.SQL.Clear;
  DmPediVda.TbPVICliIts.SQL.Add('DROP TABLE PVICli; ');
  DmPediVda.TbPVICliIts.SQL.Add('CREATE TABLE PVICli (');
  DmPediVda.TbPVICliIts.SQL.Add('  Codigo  integer      ,');
  DmPediVda.TbPVICliIts.SQL.Add('  CodUsu  integer      ,');
  DmPediVda.TbPVICliIts.SQL.Add('  Nome    varchar(50)  ,');
  DmPediVda.TbPVICliIts.SQL.Add('  Ativo   smallint      ');
  DmPediVda.TbPVICliIts.SQL.Add(');');
  //
  DmPediVda.QrPVICliCad.Close;
  DmkABS_PF.AbreQuery(DmPediVda.QrPVICliCad ???
  while not DmPediVda.QrPVICliCad.Eof do
  begin
    Txt2 :=
      dmkPF.FFP(DmPediVda.QrPVICliCadCodigo.Value, 0) + ',' +
      dmkPF.FFP(DmPediVda.QrPVICliCadCodUsu.Value, 0) + ',' +
      '"' + DmPediVda.QrPVICliCadNOMEENT.Value + '",' +
      '0);';
  //for i  := 1 to 10 do
    DmPediVda.TbPVICliIts.SQL.Add(Txt1 + Txt2);
    DmPediVda.QrPVICliCad.Next;
  end;
  //
  DmPediVda.TbPVICliIts.SQL.Add('SELECT * FROM pvicli;');
  DmkABS_PF.AbreQuery(DmPediVda.TbPVICliIts ???
  PageControl1.ActivePageIndex := 0;
}
begin
  DmPediVda.QrPVICliCad.Close;
  UnDmkDAC_PF.AbreQuery(DmPediVda.QrPVICliCad, Dmod.MyDB);
  //
  DmPediVda.TbPVICliIts.Close;
  DmPediVda.TbPVICliIts.Database := DModG.MyPID_DB;
  FPVICli := UCriar.RecriaTempTable('PVICli', DmodG.QrUpdPID1, False);
  DmodG.QrUpdPID1.SQL.Clear;
  DmodG.QrUpdPID1.SQL.Add('INSERT INTO ' + FPVICli);
  DmodG.QrUpdPID1.SQL.Add('SELECT Codigo, CodUsu,');
  DmodG.QrUpdPID1.SQL.Add('IF(Tipo=0,RazaoSocial,Nome) Nome, 0');
  DmodG.QrUpdPID1.SQL.Add('FROM ' + TMeuDB + '.entidades');
  DmodG.QrUpdPID1.SQL.Add('WHERE Cliente1="V"');
  UMyMod.ExecutaQuery(DmodG.QrUpdPID1);
  UnDmkDAC_PF.AbreTable(DmPediVda.TbPVICliIts, DModG.MyPID_DB);
  PageControl1.ActivePageIndex := 0;
end;

procedure TFmPediVdaImpCli.FormDeactivate(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 1;
  RGSelecao.ItemIndex := 1;
  //
  if FindWindow('TFmPediVdaImp', nil) > 0 then
    FmPediVdaImp.AtivaBtConfirma();
end;

end.

