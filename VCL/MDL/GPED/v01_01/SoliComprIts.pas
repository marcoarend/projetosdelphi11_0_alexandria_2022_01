unit SoliComprIts;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, dmkEdit, dmkLabel, dmkImage, UnDmkProcFunc,
  UnDmkEnums;

type
  TFmSoliComprIts = class(TForm)
    Panel1: TPanel;
    Panel3: TPanel;
    Panel4: TPanel;
    GroupBox1: TGroupBox;
    Label3: TLabel;
    EdCodigo: TdmkEdit;
    EdGraGruX: TdmkEdit;
    Label4: TLabel;
    Label5: TLabel;
    EdControle: TdmkEdit;
    GroupBox2: TGroupBox;
    Label1: TLabel;
    EdQtdeMin: TdmkEdit;
    Label6: TLabel;
    EdPrecoO: TdmkEdit;
    Label8: TLabel;
    EdQuantC_A: TdmkEdit;
    EdQuantV: TdmkEdit;
    Label9: TLabel;
    GroupBox3: TGroupBox;
    EdQuantS: TdmkEdit;
    Label2: TLabel;
    LaPrecoR: TLabel;
    EdPrecoR: TdmkEdit;
    EdQuantC_D: TdmkEdit;
    Label7: TLabel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel5: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel6: TPanel;
    PnSaiDesis: TPanel;
    BtOK: TBitBtn;
    BtSaida: TBitBtn;
    GroupBox4: TGroupBox;
    EdPrecoF: TdmkEdit;
    Label13: TLabel;
    Label17: TLabel;
    EdDescoP: TdmkEdit;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure EdPrecoRChange(Sender: TObject);
    procedure EdDescoVChange(Sender: TObject);
    procedure EdQuantVChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    //meu
    procedure Recalcula_vBC(Sender: TObject);
    procedure EdQuantSChange(Sender: TObject);
  private
    { Private declarations }
    procedure CalculaValores();
    procedure AtualizaQuantMin();
  public
    { Public declarations }
  end;

  var
  FmSoliComprIts: TFmSoliComprIts;

implementation

uses UnMyObjects, dmkGeral, ModPediVda, Module, UMySQLModule, SoliComprCab;

{$R *.DFM}

procedure TFmSoliComprIts.AtualizaQuantMin();
begin
  EdQtdeMin.ValueVariant := EdQuantC_D.ValueVariant + EdQuantV.ValueVariant;
end;

procedure TFmSoliComprIts.BtOKClick(Sender: TObject);
var
  Controle, Codigo, GraGruX: Integer;
begin
  if EdQuantS.ValueVariant < EdQtdeMin.ValueVariant then
  begin
    Geral.MB_Aviso('Quantidade informada abaixo do m�nimo!');
    Exit;
  end;

  //
  if EdQuantS.ValueVariant < (EdQuantC_D.ValueVariant + EdQuantV.ValueVariant) then
  begin
    Geral.MB_Aviso('Quantidade informada � menor que soma de solicita��es e cancelados!');
    Exit;
  end;

  //

  if (ImgTipo.SQLType = stUpd) and (EdQuantS.ValueVariant = 0) then
  begin
    //
    if Geral.MB_Pergunta('Confirma a EXCLUS�O do item?') = ID_YES then
    begin
      Codigo  := EdCodigo.ValueVariant;
      GraGruX := EdGraGruX.ValueVariant;
      //
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('DELETE FROM solicomprits WHERE ');
      Dmod.QrUpd.SQL.Add('Codigo=:P0 AND GraGruX=:P1');
      Dmod.QrUpd.Params[00].AsInteger := Codigo;
      Dmod.QrUpd.Params[01].AsInteger := GraGruX;
      Dmod.QrUpd.ExecSQL;
      //
      DmPediVda.AtzSdosSoliCompr(Codigo);
      FmSoliComprCab.LocCod(Codigo, Codigo);
      FmSoliComprCab.ReopenSoliComprGru(GraGruX);
      //
      Close;
    end;
  end else
  begin
    //Controle := UMyMod.BuscaEmLivreY_Def('solicomprits', 'Controle', ImgTipo.SQLType, EdControle.ValueVariant);
    Controle := UMyMod.BPGS1I32('solicomprits', 'Controle', '', '', tsPos, ImgTipo.SQLType, EdControle.ValueVariant);
    EdControle.ValueVariant := Controle;
    if UMyMod.ExecSQLInsUpdFm(Self, ImgTipo.SQLType, 'solicomprits', Controle,
    Dmod.QrUpd) then
    begin
      DmPediVda.AtualizaUmItemSoliCompr(Controle);
      DmPediVda.AtzSdosSoliCompr(FmSoliComprCab.QrSoliComprCabCodigo.Value);
      FmSoliComprCab.LocCod(FmSoliComprCab.QrSoliComprCabCodigo.Value, FmSoliComprCab.QrSoliComprCabCodigo.Value);
      FmSoliComprCab.ReopenSoliComprGru(EdGraGruX.ValueVariant);
      Close;
    end;
  end;
end;

procedure TFmSoliComprIts.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmSoliComprIts.CalculaValores;
var
  QuantS, PrecoR, DescoP, ValCal, DescoV, ValBru, ValLiq, PrecoF: Double;
begin
{
  QuantS := EdQuantS.ValueVariant;
  PrecoR := EdPrecoR.ValueVariant;
  DescoP := EdDescoP.ValueVariant;
  //
  //PrecoO := Geral.DMV(GradeL.Cells[Col, Row]);
  //PrecoR := Geral.DMV(GradeF.Cells[Col, Row]);
  //QuantS := Geral.DMV(GradeQ.Cells[Col, Row]);
  ValCal := PrecoR * QuantS;
  //DescoP := Geral.DMV(GradeD.Cells[Col, Row]);
  DescoV := dmkPF.FFF(ValCal * DescoP / 100, Dmod.QrControleCasasProd.Value, siPositivo);
  ValBru := dmkPF.FFF(ValCal, Dmod.QrControleCasasProd.Value, siPositivo);
  ValLiq := ValBru - DescoV;
  if QuantS = 0 then
    PrecoF := 0
  else
    PrecoF := Round(ValLiq / QuantS * 100) / 100;
  ValLiq := PrecoF * QuantS;
  //
  EdValBru.ValueVariant := ValBru;
  EdDescoV.ValueVariant := DescoV;
  EdValLiq.ValueVariant := ValLiq;
  EdPrecoF.ValueVariant := PrecoF;
}
end;

procedure TFmSoliComprIts.EdDescoVChange(Sender: TObject);
begin
  CalculaValores();
end;

procedure TFmSoliComprIts.EdPrecoRChange(Sender: TObject);
begin
  CalculaValores();
end;

procedure TFmSoliComprIts.EdQuantSChange(Sender: TObject);
begin
  CalculaValores();
  AtualizaQuantMin();
  Recalcula_vBC(Sender);
end;

procedure TFmSoliComprIts.EdQuantVChange(Sender: TObject);
begin
  AtualizaQuantMin();
end;

procedure TFmSoliComprIts.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //
  EdQuantC_D.DecimalSize := 3; //FmSoliComprCab.QrSoliComprCabGruFracio.Value;
  EdQuantS.DecimalSize   := 3; //FmSoliComprCab.QrSoliComprCabGruFracio.Value;
  //EdValLiq.DecimalSize   := Dmod.FFmtPrc;
  //EdPrecoF.DecimalSize   := Dmod.FFmtPrc;
  //EdPrecoR.DecimalSize   := Dmod.FFmtPrc;

  if ImgTipo.SQLType = stUpd then
  begin
    if DmPediVda.QrItemSCIQuantC.Value > 0 then
    begin
      //EdDescoP.Enabled := False;
      EdPrecoR.Enabled := False;
      //LaDescoP.Enabled := False;
      LaPrecoR.Enabled := False;
    end;
  end else
  begin
    EdQtdeMin.ValueVariant := 1;
    EdCodigo.ValueVariant  := FmSoliComprCab.QrSoliComprCabCodigo.Value;
    EdGraGruX.ValueVariant := Geral.IMV(
      FmSoliComprCab.GradeC.Cells[FmSoliComprCab.GradeQ.Col, FmSoliComprCab.GradeQ.Row]);
  end;
end;

procedure TFmSoliComprIts.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
end;

procedure TFmSoliComprIts.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
    [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmSoliComprIts.Recalcula_vBC(Sender: TObject);
{
var
  QuantS, PrecoR, vProd, vFrete, vSeg, vDesc, vOutro, vBC, PrecoF, ValBru,
  DescoP: Double;
  SQLType: TSQLType;
}
begin
{
  SQLType := stUpd;
  //
  QuantS   := EdQuantS.ValueVariant;
  PrecoR   := EdPrecoR.ValueVariant;
  vProd    := QuantS * PrecoR;
  vFrete   := EdvFrete.ValueVariant;
  vSeg     := EdvSeg.ValueVariant;
  vDesc    := EdvDesc.ValueVariant;
  vOutro   := EdvOutro.ValueVariant;
  //
  ValBru   := vProd + vFrete + vSeg (*- vDesc*) + vOutro;
  vBC      := vProd + vFrete + vSeg - vDesc + vOutro;

  PrecoF := Round(vBC / QuantS * 100) / 100;

  if vProd > 0 then
    DescoP := vDesc / vProd * 100
  else
    DescoP := 0.00;

  EdDescoP.ValueVariant := DescoP;
  EdPrecoF.ValueVariant := PrecoF;
  EdvProd.ValueVariant  := vProd;
  EdDescoV.ValueVariant := vDesc;
  EdValBru.ValueVariant := ValBru;
  EdValLiq.ValueVariant := vBC;
  EdvBC.ValueVariant    := vBC;
}
end;

end.

