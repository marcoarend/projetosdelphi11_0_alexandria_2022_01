unit SoliComprCab;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls, Db, (*DBTables,*) JPEG, ExtDlgs,
  ZCF2, ResIntStrings, UnDmkProcFunc, UnGOTOy, UnInternalConsts,
  UnMsgInt, UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa,
  dmkGeral, dmkPermissoes, dmkEdit, dmkLabel, dmkRadioGroup, Grids, DBGrids,
  dmkDBGrid, Menus, dmkValUsu, dmkDBLookupComboBox, dmkEditCB, ComCtrls,
  dmkEditDateTimePicker, dmkCheckBox, frxClass, frxDBSet, dmkImage, DmkDAC_PF,
  UnDmkEnums, UnGrl_Geral, dmkLabelRotate, UnGrl_Vars, dmkMemo;

type
  TCampoIncrementoItemNFe = (ciifND=0, ciifVFrete=1, ciifVSeg=2, ciifVOutro=3);
  //
  TFmSoliComprCab = class(TForm)
    PainelDados: TPanel;
    DsSoliComprCab: TDataSource;
    QrSoliComprCab: TMySQLQuery;
    PainelEdita: TPanel;
    dmkPermissoes1: TdmkPermissoes;
    PMSolicita: TPopupMenu;
    PMItens: TPopupMenu;
    VuEmpresa: TdmkValUsu;
    GBDadosTop: TGroupBox;
    Label9: TLabel;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    EdCodUsu: TdmkEdit;
    Label8: TLabel;
    EdCodigo: TdmkEdit;
    Label7: TLabel;
    TPDtaInclu: TdmkEditDateTimePicker;
    Label6: TLabel;
    GroupBox9: TGroupBox;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    Panel4: TPanel;
    PCDadosTop: TPageControl;
    TabSheet3: TTabSheet;
    Panel6: TPanel;
    GroupBox2: TGroupBox;
    Label12: TLabel;
    LaDtaEntra: TLabel;
    Label15: TLabel;
    Label14: TLabel;
    LaPedidoCli: TLabel;
    TPDtaEmiss: TdmkEditDateTimePicker;
    TPDtaEntra: TdmkEditDateTimePicker;
    EdPrioridade: TdmkEdit;
    TPDtaPrevi: TdmkEditDateTimePicker;
    EdPedidoCli: TdmkEdit;
    Incluinovositensdegrupo1: TMenuItem;
    QrSoliComprGru: TMySQLQuery;
    DsSoliComprGru: TDataSource;
    QrSoliComprGruCodUsu: TIntegerField;
    QrSoliComprGruNome: TWideStringField;
    QrSoliComprGruNivel1: TIntegerField;
    QrSoliComprGruGRATAMCAD: TIntegerField;
    QrSoliComprGruQuantS: TFloatField;
    QrSoliComprGruValLiq: TFloatField;
    AlteraExcluiIncluiitemselecionado1: TMenuItem;
    N1: TMenuItem;
    IncluinovositensporLeitura1: TMenuItem;
    N2: TMenuItem;
    PMCustom: TPopupMenu;
    IncluinovoitemprodutoCustomizvel1: TMenuItem;
    Adicionapartesaoitemselecionado1: TMenuItem;
    QrSoliComprGruItensCustomizados: TFloatField;
    N3: TMenuItem;
    AtualizaValoresdoitem1: TMenuItem;
    ItemprodutoCustomizvel1: TMenuItem;
    Partedoitemprodutoselecionado1: TMenuItem;
    Alteraitemprodutoselecionado1: TMenuItem;
    Excluipartedoprodutoselecionado1: TMenuItem;
    Excluiitemprodutoselecionado1: TMenuItem;
    QrItsN: TmySQLQuery;
    QrItsNCU_NIVEL1: TIntegerField;
    QrItsNNO_NIVEL1: TWideStringField;
    QrItsNNO_TAM: TWideStringField;
    QrItsNNO_COR: TWideStringField;
    QrItsNQuantS: TFloatField;
    QrItsNPrecoR: TFloatField;
    QrItsNDescoP: TFloatField;
    QrItsNPrecoF: TFloatField;
    QrItsNValBru: TFloatField;
    QrItsNValLiq: TFloatField;
    frxDsItsN: TfrxDBDataset;
    QrItsNKGT: TLargeintField;
    frxDsSoliComprCab: TfrxDBDataset;
    QrSoliComprGruFracio: TSmallintField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel11: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel12: TPanel;
    PnSaiDesis: TPanel;
    BtConfirma: TBitBtn;
    BtDesiste: TBitBtn;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    BtSolicita: TBitBtn;
    BtItens: TBitBtn;
    BtFornece: TBitBtn;
    BtVisual: TBitBtn;
    BitBtn1: TBitBtn;
    BtRecalcula: TBitBtn;
    N4: TMenuItem;
    QrSoliComprGruControle: TIntegerField;
    QrSoliComprIts: TMySQLQuery;
    QrSoliComprItsControle: TIntegerField;
    BtGraGruN: TBitBtn;
    BtFisRegCad: TBitBtn;
    QrEntregaCli: TmySQLQuery;
    QrEntregaCliE_ALL: TWideStringField;
    QrEntregaCliCNPJ_TXT: TWideStringField;
    QrEntregaCliNOME_TIPO_DOC: TWideStringField;
    QrEntregaCliTE1_TXT: TWideStringField;
    QrEntregaCliNUMERO_TXT: TWideStringField;
    QrEntregaCliCEP_TXT: TWideStringField;
    QrEntregaCliL_CNPJ: TWideStringField;
    QrEntregaCliL_CPF: TWideStringField;
    QrEntregaCliL_Nome: TWideStringField;
    QrEntregaCliLograd: TSmallintField;
    QrEntregaCliNOMELOGRAD: TWideStringField;
    QrEntregaCliRUA: TWideStringField;
    QrEntregaCliNUMERO: TIntegerField;
    QrEntregaCliCOMPL: TWideStringField;
    QrEntregaCliBAIRRO: TWideStringField;
    QrEntregaCliLCodMunici: TIntegerField;
    QrEntregaCliNO_MUNICI: TWideStringField;
    QrEntregaCliNOMEUF: TWideStringField;
    QrEntregaCliCEP: TIntegerField;
    QrEntregaCliCodiPais: TIntegerField;
    QrEntregaCliNO_PAIS: TWideStringField;
    QrEntregaCliENDEREF: TWideStringField;
    QrEntregaCliTE1: TWideStringField;
    QrEntregaCliEmail: TWideStringField;
    QrEntregaCliL_IE: TWideStringField;
    QrEntregaEnti: TmySQLQuery;
    QrEntregaEntiCodigo: TIntegerField;
    QrEntregaEntiCadastro: TDateField;
    QrEntregaEntiENatal: TDateField;
    QrEntregaEntiPNatal: TDateField;
    QrEntregaEntiTipo: TSmallintField;
    QrEntregaEntiRespons1: TWideStringField;
    QrEntregaEntiRespons2: TWideStringField;
    QrEntregaEntiENumero: TIntegerField;
    QrEntregaEntiPNumero: TIntegerField;
    QrEntregaEntiELograd: TSmallintField;
    QrEntregaEntiPLograd: TSmallintField;
    QrEntregaEntiECEP: TIntegerField;
    QrEntregaEntiPCEP: TIntegerField;
    QrEntregaEntiNOME_ENT: TWideStringField;
    QrEntregaEntiNO_2_ENT: TWideStringField;
    QrEntregaEntiCNPJ_CPF: TWideStringField;
    QrEntregaEntiIE_RG: TWideStringField;
    QrEntregaEntiNIRE_: TWideStringField;
    QrEntregaEntiRUA: TWideStringField;
    QrEntregaEntiSITE: TWideStringField;
    QrEntregaEntiNUMERO: TIntegerField;
    QrEntregaEntiCOMPL: TWideStringField;
    QrEntregaEntiBAIRRO: TWideStringField;
    QrEntregaEntiCIDADE: TWideStringField;
    QrEntregaEntiNOMELOGRAD: TWideStringField;
    QrEntregaEntiNOMEUF: TWideStringField;
    QrEntregaEntiPais: TWideStringField;
    QrEntregaEntiLograd: TSmallintField;
    QrEntregaEntiCEP: TIntegerField;
    QrEntregaEntiTE1: TWideStringField;
    QrEntregaEntiFAX: TWideStringField;
    QrEntregaEntiEMAIL: TWideStringField;
    QrEntregaEntiTRATO: TWideStringField;
    QrEntregaEntiENDEREF: TWideStringField;
    QrEntregaEntiCODMUNICI: TFloatField;
    QrEntregaEntiCODPAIS: TFloatField;
    QrEntregaEntiRG: TWideStringField;
    QrEntregaEntiSSP: TWideStringField;
    QrEntregaEntiDataRG: TDateField;
    QrEntregaEntiIE: TWideStringField;
    QrEntregaEntiE_ALL: TWideStringField;
    QrEntregaEntiCNPJ_TXT: TWideStringField;
    QrEntregaEntiNOME_TIPO_DOC: TWideStringField;
    QrEntregaEntiTE1_TXT: TWideStringField;
    QrEntregaEntiFAX_TXT: TWideStringField;
    QrEntregaEntiNUMERO_TXT: TWideStringField;
    QrEntregaEntiCEP_TXT: TWideStringField;
    PMRegrFiscal: TPopupMenu;
    Estrio1: TMenuItem;
    Cadastro1: TMenuItem;
    QrUsuario: TMySQLQuery;
    QrUsuarioUserCad: TIntegerField;
    QrUsuarioUserAlt: TIntegerField;
    QrUsuarioNO_LANCADOR: TWideStringField;
    QrUsuarioNO_ALTERADOR: TWideStringField;
    frxDsUsuario: TfrxDBDataset;
    TabSheet14: TTabSheet;
    MeObserva: TdmkMemo;
    TabSheet15: TTabSheet;
    DBMemo1: TDBMemo;
    EdReferenPedi: TdmkEdit;
    Label98: TLabel;
    Panel14: TPanel;
    RGEntSai: TdmkRadioGroup;
    QrSoliComprGruValBru: TFloatField;
    QrSumIts: TMySQLQuery;
    DsSumIts: TDataSource;
    QrSumItsQuantS: TFloatField;
    QrSumItsDescoV: TFloatField;
    QrSumItsValLiq: TFloatField;
    QrSumItsValBru: TFloatField;
    QrSumItsItensCustomizados: TFloatField;
    QrSumItsvProd: TFloatField;
    QrSumItsvFrete: TFloatField;
    QrSumItsvSeg: TFloatField;
    QrSumItsvOutro: TFloatField;
    QrSumItsvDesc: TFloatField;
    QrSumItsvBC: TFloatField;
    QrSoliComprGruPrecoF: TFloatField;
    QrItsNvProd: TFloatField;
    QrItsNvFrete: TFloatField;
    QrItsNvSeg: TFloatField;
    QrItsNvOutro: TFloatField;
    QrItsNvDesc: TFloatField;
    QrItsNvBC: TFloatField;
    QrSoliComprCabCodigo: TIntegerField;
    QrSoliComprCabCodUsu: TIntegerField;
    QrSoliComprCabEmpresa: TIntegerField;
    QrSoliComprCabCliente: TIntegerField;
    QrSoliComprCabDtaEmiss: TDateField;
    QrSoliComprCabDtaEntra: TDateField;
    QrSoliComprCabDtaInclu: TDateField;
    QrSoliComprCabDtaPrevi: TDateField;
    QrSoliComprCabPrioridade: TSmallintField;
    QrSoliComprCabCondicaoPG: TIntegerField;
    QrSoliComprCabMoeda: TIntegerField;
    QrSoliComprCabSituacao: TIntegerField;
    QrSoliComprCabTabelaPrc: TIntegerField;
    QrSoliComprCabMotivoSit: TIntegerField;
    QrSoliComprCabLoteProd: TIntegerField;
    QrSoliComprCabPedidoCli: TWideStringField;
    QrSoliComprCabFretePor: TSmallintField;
    QrSoliComprCabTransporta: TIntegerField;
    QrSoliComprCabRedespacho: TIntegerField;
    QrSoliComprCabObserva: TWideMemoField;
    QrSoliComprCabEntSai: TSmallintField;
    QrSoliComprCabReferenPedi: TWideStringField;
    QrSoliComprCabNOMEEMP: TWideStringField;
    QrSoliComprCabNOMETRANSP: TWideStringField;
    QrSoliComprCabNOMEREDESP: TWideStringField;
    QrSoliComprCabFilial: TIntegerField;
    QrSoliComprCabCODUSU_TRA: TIntegerField;
    QrSoliComprCabCODUSU_RED: TIntegerField;
    QrSoliComprCabCODUSU_MOT: TIntegerField;
    QrSoliComprCabCODUSU_FOR: TIntegerField;
    DBGGru: TdmkDBGrid;
    GroupBox10: TGroupBox;
    Label54: TLabel;
    Label55: TLabel;
    Label63: TLabel;
    Label64: TLabel;
    Label65: TLabel;
    DBEdit9: TDBEdit;
    DBEdit10: TDBEdit;
    DBEdit12: TDBEdit;
    DBEdit11: TDBEdit;
    DBEdit19: TDBEdit;
    Panel1: TPanel;
    Label56: TLabel;
    DBEdit3: TDBEdit;
    DBEdit2: TDBEdit;
    Label99: TLabel;
    DBEdit1: TDBEdit;
    DBEdit82: TDBEdit;
    Label100: TLabel;
    Label60: TLabel;
    DBEdit8: TDBEdit;
    DBEdit22: TDBEdit;
    Label61: TLabel;
    Splitter2: TSplitter;
    PageControl3: TPageControl;
    TabSheet5: TTabSheet;
    GradeQ: TStringGrid;
    StaticText1: TStaticText;
    TabSheet4: TTabSheet;
    GradeF: TStringGrid;
    TabSheet6: TTabSheet;
    GradeD: TStringGrid;
    StaticText3: TStaticText;
    TabSheet7: TTabSheet;
    GradeV: TStringGrid;
    TabSheet8: TTabSheet;
    GradeC: TStringGrid;
    StaticText6: TStaticText;
    TabSheet9: TTabSheet;
    GradeA: TStringGrid;
    StaticText2: TStaticText;
    TabSheet10: TTabSheet;
    GradeX: TStringGrid;
    Splitter1: TSplitter;
    DBGSoliComprFor: TDBGrid;
    QrSoliComprFor: TMySQLQuery;
    DsSoliComprFor: TDataSource;
    Incluinovasolicitao1: TMenuItem;
    Alterasolicitaoatual1: TMenuItem;
    Excluisolicitaoatual1: TMenuItem;
    Duplicarsolicitaoatual1: TMenuItem;
    QrSoliComprForCodigo: TIntegerField;
    QrSoliComprForFornece: TIntegerField;
    QrSoliComprForRelevancia: TSmallintField;
    QrSoliComprForNO_FORNECE: TWideStringField;
    PmFornece: TPopupMenu;
    Incluiforneceor1: TMenuItem;
    Removefornecedor1: TMenuItem;
    QrFor: TMySQLQuery;
    QrForE_ALL: TWideStringField;
    QrForCNPJ_TXT: TWideStringField;
    QrForNOME_TIPO_DOC: TWideStringField;
    QrForTE1_TXT: TWideStringField;
    QrForFAX_TXT: TWideStringField;
    QrForNUMERO_TXT: TWideStringField;
    QrForCEP_TXT: TWideStringField;
    QrForCodigo: TIntegerField;
    QrForTipo: TSmallintField;
    QrForCodUsu: TIntegerField;
    QrForNOME_ENT: TWideStringField;
    QrForCNPJ_CPF: TWideStringField;
    QrForIE_RG: TWideStringField;
    QrForRUA: TWideStringField;
    QrForCOMPL: TWideStringField;
    QrForBAIRRO: TWideStringField;
    QrForCIDADE: TWideStringField;
    QrForNOMELOGRAD: TWideStringField;
    QrForNOMEUF: TWideStringField;
    QrForPais: TWideStringField;
    QrForENDEREF: TWideStringField;
    QrForTE1: TWideStringField;
    QrForFAX: TWideStringField;
    QrForIE: TWideStringField;
    QrForCAD_FEDERAL: TWideStringField;
    QrForCAD_ESTADUAL: TWideStringField;
    QrForIE_TXT: TWideStringField;
    QrForNUMERO: TFloatField;
    QrForCEP: TFloatField;
    QrForUF: TFloatField;
    frxDsFor: TfrxDBDataset;
    frxSLC_COMPRA_001_A: TfrxReport;
    QrForRelevancia: TSmallintField;
    QrForL_Ativo: TSmallintField;
    QrForCE1: TWideStringField;
    QrForCE1_TXT: TWideStringField;
    QrCentroCusto: TMySQLQuery;
    QrCentroCustoREF_NIV1: TWideStringField;
    QrCentroCustoCodigo: TIntegerField;
    QrCentroCustoNome: TWideStringField;
    QrCentroCustoAtivo: TSmallintField;
    DsCentroCusto: TDataSource;
    Label29: TLabel;
    DBEdREF_NIV1: TDBEdit;
    EdCentroCusto: TdmkEditCB;
    CBCentroCusto: TdmkDBLookupComboBox;
    SbCentroCusto: TSpeedButton;
    QrSoliComprCabNO_CENTROCUSTO: TWideStringField;
    QrSoliComprCabCentroCusto: TIntegerField;
    Label1: TLabel;
    DBEdit4: TDBEdit;
    DBEdit5: TDBEdit;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrSoliComprCabAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrSoliComprCabBeforeOpen(DataSet: TDataSet);
    procedure EdCodUsuKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSolicitaClick(Sender: TObject);
    procedure PMSolicitaPopup(Sender: TObject);
    procedure QrSoliComprCabBeforeClose(DataSet: TDataSet);
    procedure QrSoliComprCabAfterScroll(DataSet: TDataSet);
    procedure BtItensClick(Sender: TObject);
    procedure SpeedButton6Click(Sender: TObject);
    procedure BtTabelaPrcClick(Sender: TObject);
    procedure SpeedButton9Click(Sender: TObject);
    procedure BtCondicaoPGClick(Sender: TObject);
    procedure EdTabelaPrcChange(Sender: TObject);
    procedure SpeedButton11Click(Sender: TObject);
    procedure SpeedButton12Click(Sender: TObject);
    procedure SpeedButton13Click(Sender: TObject);
    procedure EdDesoAces_PChange(Sender: TObject);
    procedure EdFrete_PChange(Sender: TObject);
    procedure EdSeguro_PChange(Sender: TObject);
    procedure Incluinovositensdegrupo1Click(Sender: TObject);
    procedure QrSoliComprGruAfterScroll(DataSet: TDataSet);
    procedure BtVisualClick(Sender: TObject);
    procedure PageControl1Change(Sender: TObject);
    procedure GradeADrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect;
      State: TGridDrawState);
    procedure GradeCDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect;
      State: TGridDrawState);
    procedure GradeQDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect;
      State: TGridDrawState);
    procedure GradeFDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect;
      State: TGridDrawState);
    procedure GradeDDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect;
      State: TGridDrawState);
    procedure GradeVDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect;
      State: TGridDrawState);
    procedure GradeXDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect;
      State: TGridDrawState);
    procedure BitBtn1Click(Sender: TObject);
    procedure QrSoliComprGruAfterOpen(DataSet: TDataSet);
    procedure GradeQDblClick(Sender: TObject);
    procedure BtRecalculaClick(Sender: TObject);
    procedure AlteraExcluiIncluiitemselecionado1Click(Sender: TObject);
    procedure SbRegrFiscalClick(Sender: TObject);
    procedure EdRegrFiscalChange(Sender: TObject);
    procedure EdRegrFiscalExit(Sender: TObject);
    procedure Adicionapartesaoitemselecionado1Click(Sender: TObject);
    procedure Custumizao1Click(Sender: TObject);
    procedure BtForneceClick(Sender: TObject);
    procedure PMCustomPopup(Sender: TObject);
    procedure QrSoliComprGruBeforeClose(DataSet: TDataSet);
    procedure QrCustomizadosAfterScroll(DataSet: TDataSet);
    procedure QrCustomizadosBeforeClose(DataSet: TDataSet);
    procedure AtualizaValoresdoitem1Click(Sender: TObject);
    procedure Alteraitemprodutoselecionado1Click(Sender: TObject);
    procedure Excluipartedoprodutoselecionado1Click(Sender: TObject);
    procedure Excluiitemprodutoselecionado1Click(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
    procedure QrForCalcFields(DataSet: TDataSet);
    procedure frxPED_VENDA_001_01GetValue(const VarName: string;
      var Value: Variant);
    procedure EdRepresenChange(Sender: TObject);
    procedure N1Click(Sender: TObject);
    procedure EdindPresChange(Sender: TObject);
    procedure EdindPresKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdEmpresaChange(Sender: TObject);
    procedure BtGraGruNClick(Sender: TObject);
    procedure BtFisRegCadClick(Sender: TObject);
    procedure Edide_finNFeChange(Sender: TObject);
    procedure Edide_finNFeKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdEntregaEntiRedefinido(Sender: TObject);
    procedure CkEntregaUsaClick(Sender: TObject);
    procedure SbEntregaEntiClick(Sender: TObject);
    procedure CkRetiradaUsaClick(Sender: TObject);
    procedure EdRetiradaEntiRedefinido(Sender: TObject);
    procedure QrEntregaEntiCalcFields(DataSet: TDataSet);
    procedure QrEntregaCliCalcFields(DataSet: TDataSet);
    procedure Estrio1Click(Sender: TObject);
    procedure Cadastro1Click(Sender: TObject);
    procedure EdRegrFiscalRedefinido(Sender: TObject);
    procedure EdCodUsuEnter(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure RGEntSaiClick(Sender: TObject);
    procedure AplicardescontoGeral1Click(Sender: TObject);
    procedure EdIND_PGTOChange(Sender: TObject);
    procedure EdIND_PGTOKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure Incluinovasolicitao1Click(Sender: TObject);
    procedure Alterasolicitaoatual1Click(Sender: TObject);
    procedure Excluisolicitaoatual1Click(Sender: TObject);
    procedure Duplicarsolicitaoatual1Click(Sender: TObject);
    procedure Incluiforneceor1Click(Sender: TObject);
    procedure Removefornecedor1Click(Sender: TObject);
    procedure SbCentroCustoClick(Sender: TObject);
    procedure EdCentroCustoChange(Sender: TObject);
    procedure IncluinovositensporLeitura1Click(Sender: TObject);
  private
    { Private declarations }
    FindPres, F_finNFe, F_IND_PGTO_EFD, F_IND_FRT_EFD: MyArrayLista;
    FDBGWidth: Integer;

    //FCodUsuTxt_Old: String;
    //
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
   ////Procedures do form
    procedure MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo: Integer);
    procedure MostraEnderecoDeEntrega2();
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    //
    procedure CalculaDesoAces_V();
    procedure CalculaFrete_V();
    procedure CalculaSeguro_V();
    procedure DefineEnderecoDeEntrega();
    procedure DesabilitaComponentes();

    procedure MostraSoliComprGru(SQLType: TSQLType);
    //procedure MostraPediVdaLei(SQLType: TSQLType);
    procedure GradeQDblClick2();
    procedure MostraFormEntidade2(Entidade: Integer; Query: TmySQLQuery;
              EditCliente: TdmkEditCB; ComboCliente: TdmkDBLookupComboBox);
    //function ObtemQtde(var Qtde: Double): Boolean;
    procedure Imprime();
    procedure ReopenFor();
    procedure ReopenCentroCusto();
  public
    { Public declarations }
    procedure LocCod(Atual, Codigo: Integer);
    procedure ReopenSoliComprGru(Nivel1: Integer);
    procedure ReopenSoliComprFor(Nivel1: Integer);
    (*
    procedure ReopenPediVdaCuz(Conta: Integer);
    procedure MostraFmPediVdaCuzParIns(CodUsu, PrdGrupTip, Nivel1, GraGruX, Controle:
              Integer; SQLType: TSQLType;
              QuantS, MedidaC, MedidaL, MedidaA, MedidaE: Double);
    procedure AtualizaItemCustomizado(Controle: Integer);
    *)
    procedure ReopenSumIts();
  end;

var
  FmSoliComprCab: TFmSoliComprCab;
const
  FFormatFloat = '00000';

implementation

uses
  {$IfNDef SemNFe_0000} NFe_PF, {$EndIf}
  {$IfNDef NAO_GFAT}UnGrade_Jan, UnGFat_Jan, {$EndIf}
  {$IfNDef NO_FINANCEIRO}UnFinanceiroJan, {$EndIf}
  UnMyObjects, Module, GraAtrIts, MyDBCheck, ModuleGeral, ModPediVda,
  SoliComprIts,
  Motivos, CambioMda, PediPrzCab1, PediAcc, SoliComprGru, ModProd,
  GetValor, Principal,
  //PediVdaImp,PediVdaLei2, PediVdaCuzIns2, PediVdaCuzUpd2, PediVdaCuzParIns2,
  UnPraz_PF, UnEntities, SoliComprFor, UnCeCuRe_Jan;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmSoliComprCab.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmSoliComprCab.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrSoliComprCabCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmSoliComprCab.DefParams;
begin
  VAR_GOTOTABELA := 'solicomprcab';
  VAR_GOTOMYSQLTABLE := QrSoliComprCab;
  VAR_GOTONEG := gotoAll;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 1;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT pvd.Codigo, pvd.CodUsu, pvd.Empresa, pvd.Cliente,');
  VAR_SQLx.Add('pvd.DtaEmiss, pvd.DtaEntra, pvd.DtaInclu, pvd.DtaPrevi,');
  VAR_SQLx.Add('pvd.Prioridade, pvd.CondicaoPG, pvd.Moeda,');
  VAR_SQLx.Add('pvd.Situacao, pvd.TabelaPrc, pvd.MotivoSit, pvd.LoteProd,');
  VAR_SQLx.Add('pvd.PedidoCli, pvd.FretePor, pvd.Transporta, pvd.Redespacho,');
  VAR_SQLx.Add('pvd.Observa, pvd.EntSai, pvd.ReferenPedi, ');
  VAR_SQLx.Add('IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NOMEEMP,');
  VAR_SQLx.Add('IF(tra.Tipo=0, tra.RazaoSocial, tra.NOME) NOMETRANSP,');
  VAR_SQLx.Add('IF(red.Tipo=0, red.RazaoSocial, red.NOME) NOMEREDESP, ');
  VAR_SQLx.Add('emp.Filial, pvd.CentroCusto, ccr.Nome NO_CENTROCUSTO, ');
  VAR_SQLx.Add('tra.CodUsu CODUSU_TRA, ');
  VAR_SQLx.Add('red.CodUsu CODUSU_RED,');
  VAR_SQLx.Add('mot.CodUsu CODUSU_MOT, ');
  VAR_SQLx.Add('emp.CodUsu CODUSU_FOR');
  VAR_SQLx.Add('');
  VAR_SQLx.Add('FROM solicomprcab     pvd');
  VAR_SQLx.Add('LEFT JOIN entidades   emp ON emp.Codigo=pvd.Empresa');
  VAR_SQLx.Add('LEFT JOIN entidades   tra ON tra.Codigo=pvd.Transporta');
  VAR_SQLx.Add('LEFT JOIN entidades   red ON red.Codigo=pvd.Redespacho');
  VAR_SQLx.Add('LEFT JOIN motivos     mot ON mot.Codigo=pvd.MotivoSit');
  VAR_SQLx.Add('LEFT JOIN centrocusto ccr ON ccr.Codigo=pvd.CentroCusto');
  VAR_SQLx.Add('');
  VAR_SQLx.Add('WHERE pvd.Codigo > -1000');
  VAR_SQLx.Add('AND pvd.CodUsu > 0 ');
  VAR_SQLx.Add('AND pvd.Empresa IN (' + VAR_LIB_EMPRESAS + ')');
  //
  VAR_SQL1.Add('AND pvd.Codigo=:P0');
  //
  VAR_SQL2.Add('AND pvd.CodUsu=:P0');
  //
  //VAR_SQLa.Add('AND pvd.Nome Like :P0');
  //
  VAR_GOTOVAR1 := 'Empresa IN (' + VAR_LIB_EMPRESAS + ') AND CodUsu > 0';
end;

procedure TFmSoliComprCab.DesabilitaComponentes();
var
  Habilita0, Habilita1, Habilita2: Boolean;
begin
  DmodG.ReopenParamsEmp(QrSoliComprCabEmpresa.Value);
  //
  Habilita0 := not ((ImgTipo.SQLType = stUpd) and (QrSoliComprGru.RecordCount > 0));
  Habilita1 := False;
  if not Habilita0 then
  begin
    case DModG.QrParamsEmpPedVdaMudPrazo.Value of
      0: Habilita1 := False;
      1: Habilita1 := VAR_USUARIO < 0;
      2: Habilita1 := True;
    end;
  end else Habilita1 := True;
  //
  Habilita2 := Habilita1 or (QrSoliComprCabCondicaoPG.Value = 0);
{
  LaCondicaoPG.Enabled := Habilita2;
  EdCondicaoPG.Enabled := Habilita2;
  CBCondicaoPG.Enabled := Habilita2;
  BtCondicaoPG.Enabled := Habilita2;
}

  //

  if not Habilita0 then
  begin
    case DModG.QrParamsEmpPedVdaMudLista.Value of
      0: Habilita1 := False;
      1: Habilita1 := VAR_USUARIO < 0;
      2: Habilita1 := True;
    end;
  end else Habilita1 := True;
  Habilita2 := Habilita1 or (QrSoliComprCabTabelaPrc.Value = 0);
{
  LaTabelaPrc.Enabled := Habilita2;
  EdTabelaPrc.Enabled := Habilita2;
  CBTabelaPrc.Enabled := Habilita2;
  BtTabelaPrc.Enabled := Habilita2;
  //
}
end;

procedure TFmSoliComprCab.Duplicarsolicitaoatual1Click(Sender: TObject);
var
  Codigo, Controle, Conta, CodUsu: Integer;
begin
  if (QrSoliComprCab.State = dsInactive) or (QrSoliComprCab.RecordCount = 0) then Exit;
  //
  if Geral.MB_Pergunta('Confirma a duplica��o da solicita��o atual?') = ID_YES then
  begin
    Screen.Cursor := crHourGlass;
    try
      QrSoliComprCab.DisableControls;
      (*
      QrSoliComprCabCuz.DisableControls;
      *)
      //
      //Codigo := UMyMod.BuscaEmLivreY_Def('solicomprcab', 'Codigo', stIns, 0);
      Codigo := UMyMod.BPGS1I32('solicomprcab', 'Codigo', '', '', tsPos, stIns, Codigo);
      //CodUsu := UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, 'solicomprcab', 'CodUsu', [], [], stIns, 0, siPositivo, nil);
      CodUsu := UMyMod.BPGS1I32('solicomprcab', 'Codusu', '', '', tsPos, stIns, Codusu);
      //
      if MyObjects.FIC(Codigo = 0, nil, 'Falha ao definir C�digo para a tabela "solicomprcab"!') then Exit;
      if MyObjects.FIC(CodUsu = 0, nil, 'Falha ao definir CodUsu para a tabela "solicomprcab"!') then Exit;
      //
      if UnDmkDAC_PF.InsereRegistrosPorCopia(DModG.QrUpdPID1, 'solicomprcab', TMeuDB,
        ['Codigo'], [QrSoliComprCabCodigo.Value],
        ['Codigo', 'CodUsu'],
        [Codigo, CodUsu],
        '', True, LaAviso1, LaAviso2) then
      begin
        QrSoliComprIts.Close;
        QrSoliComprIts.Params[0].AsInteger := QrSoliComprCabCodigo.Value;
        UMyMod.AbreQuery(QrSoliComprIts, Dmod.MyDB);
        //
        if QrSoliComprIts.RecordCount > 0 then
        begin
          QrSoliComprIts.First;
          while not QrSoliComprIts.Eof do
          begin
            //Controle := UMyMod.BuscaEmLivreY_Def('pedivdaits', 'Controle', stIns, 0);
            Controle := UMyMod.BPGS1I32('solicomprits', 'Controle', '', '', tsPos, stIns, 0);
            //
            if MyObjects.FIC(Controle = 0, nil, 'Falha ao definir Controle para a tabela "solicomprits"!') then Exit;
            //
            if UnDmkDAC_PF.InsereRegistrosPorCopia(DModG.QrUpdPID1, 'solicompits', TMeuDB,
              ['Controle'], [QrSoliComprItsControle.Value],
              ['Codigo', 'Controle', 'QuantV'],
              [Codigo, Controle, 0],
              '', True, LaAviso1, LaAviso2) then
            begin
              (*
              if (QrSoliComprCabCuz.State <> dsInactive) and (QrSoliComprCabCuz.RecordCount > 0) then
              begin
                QrSoliComprCabCuz.First;
                while not QrSoliComprCabCuz.Eof do
                begin
                  Conta := UMyMod.BuscaEmLivreY_Def('pedivdacuz', 'Conta', stIns, 0);
                  //
                  if MyObjects.FIC(Conta = 0, nil, 'Falha ao definir Conta para a tabela "pedivdacuz"!') then Exit;
                  //
                  if UnDmkDAC_PF.InsereRegistrosPorCopia(DModG.QrUpdPID1, 'pedivdacuz', TMeuDB,
                    ['Conta'], [QrSoliComprCabCuzConta.Value],
                    ['Conta', 'Controle'],
                    [Conta, Controle],
                    '', True, LaAviso1, LaAviso2) then
                  begin
                    //
                  end;
                  QrSoliComprCabCuz.Next;
                end;
              end;
              *)
            end;
            //
            QrSoliComprIts.Next;
          end;
        end;
      end;
    finally
      QrSoliComprCab.EnableControls;
      (*
      QrSoliComprCabCuz.EnableControls;
      *)
      QrSoliComprIts.Close;
      Va(vpLast);
      MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
      //
      Screen.Cursor := crDefault;
      Geral.MB_Aviso('Duplica��o finalizada!');
    end;
  end;
end;

procedure TFmSoliComprCab.EdCentroCustoChange(Sender: TObject);
begin
  if EdCentroCusto.ValueVariant = 0 then
    DBEdREF_NIV1.DataField := ''
  else
    DBEdREF_NIV1.DataField := 'REF_NIV1';
end;

procedure TFmSoliComprCab.EdCodUsuEnter(Sender: TObject);
begin
  //FCodUsuTxt_Old := EdCodUsu.Text;
end;

procedure TFmSoliComprCab.EdCodUsuKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key = VK_F4) and (ImgTipo.SQLType = stIns) then
    UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, 'solicomprcab', 'CodUsu', [], [],
    stIns, 0, siPositivo, EdCodUsu);
end;

procedure TFmSoliComprCab.EdDesoAces_PChange(Sender: TObject);
begin
{
  if EdDesoAces_P.ValueVariant <> 0 then
  begin
    EdDesoAces_V.Enabled := False;
    CalculaDesoAces_V();
  end else EdDesoAces_V.Enabled := True;
}
end;

procedure TFmSoliComprCab.EdEmpresaChange(Sender: TObject);
begin
{$IfNDef SemNFe_0000}
{
  if ImgTipo.SQLType = stIns then
    UnNFe_PF.Configura_idDest(EdEmpresa, RG_idDest);
}
{$EndIf}
  DmPediVda.ReopenCartEmis(VuEmpresa.ValueVariant);
end;

procedure TFmSoliComprCab.EdEntregaEntiRedefinido(Sender: TObject);
begin
{
  PnEntregaEntiViw.Visible := EdEntregaEnti.ValueVariant <> 0;
  DefineEnderecoDeEntrega();
}
end;

procedure TFmSoliComprCab.EdFrete_PChange(Sender: TObject);
begin
{
  if EdFrete_P.ValueVariant <> 0 then
  begin
    EdFrete_V.Enabled := False;
    CalculaFrete_V();
  end else EdFrete_V.Enabled := True;
}
end;

procedure TFmSoliComprCab.Edide_finNFeChange(Sender: TObject);
begin
{
  Edide_finNFe_TXT.Text := UnNFe_PF.TextoDeCodigoNFe(nfeCTide_finNFe,  Edide_finNFe.ValueVariant);
}
end;

procedure TFmSoliComprCab.Edide_finNFeKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  TitCols: array[0..1] of String;
begin
{
  if Key = VK_F4 then
  begin
    TitCols[0] := 'C�d.';
    TitCols[1] := 'Descri��o';
    Edide_finNFe.Text := Geral.SelecionaItem(F_finNFe, 0,
      'SEL-LISTA-000 :: Finalidade de emiss�o da NF-e',
      TitCols, Screen.Width)
  end;
}
end;

procedure TFmSoliComprCab.EdindPresChange(Sender: TObject);
var
  Texto: String;
begin
{
  Geral.DescricaoDeArrStrStr(EdindPres.Text, FindPres, Texto, 0, 1);
  EdindPres_TXT.Text := Texto;
}
end;

procedure TFmSoliComprCab.EdindPresKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  TitCols: array[0..1] of String;
begin
{
  if Key = VK_F4 then
  begin
    TitCols[0] := 'C�d.';
    TitCols[1] := 'Descri��o';
    EdindPres.Text := Geral.SelecionaItem(FindPres, 0,
    'SEL-LISTA-000 :: Indicador de presen�a do comprador no estabelecimento comercial no momento da opera��o',
    TitCols, Screen.Width)
  end;
}
end;

procedure TFmSoliComprCab.EdIND_PGTOChange(Sender: TObject);
begin
{
  EdIND_PGTO_TXT.Text := UnNFe_PF.TextoDeCodigoNFe(nfeIND_PAG_EFD,  EdIND_PGTO.ValueVariant);
}
end;

procedure TFmSoliComprCab.EdIND_PGTOKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  TitCols: array[0..1] of String;
begin
{
  if Key = VK_F4 then
  begin
    TitCols[0] := 'C�d.';
    TitCols[1] := 'Descri��o';
    EdIND_PGTO.Text := Geral.SelecionaItem(F_IND_PGTO_EFD, 0,
      'SEL-LISTA-000 :: Indicador do tipo de pagamento',
      TitCols, Screen.Width)
  end;
}
end;


procedure TFmSoliComprCab.EdRegrFiscalChange(Sender: TObject);
begin
{
  EdModeloNF.Text := '';
  if not EdRegrFiscal.Focused then
    EdModeloNF.Text := DmPediVda.QrFisRegCadNO_MODELO_NF.Value;
}
end;

procedure TFmSoliComprCab.EdRegrFiscalExit(Sender: TObject);
begin
{
  EdModeloNF.Text := DmPediVda.QrFisRegCadNO_MODELO_NF.Value;
}
end;

procedure TFmSoliComprCab.EdRegrFiscalRedefinido(Sender: TObject);
begin
  RGentSai.ItemIndex := DmPediVda.QrFisRegCadTipoMov.Value;
end;

procedure TFmSoliComprCab.EdRepresenChange(Sender: TObject);
begin
{
  EdComisFat.ValueVariant := DmPediVda.QrPediAccPerComissF.Value;
  EdComisRec.ValueVariant := DmPediVda.QrPediAccPerComissR.Value;
}
end;

procedure TFmSoliComprCab.EdRetiradaEntiRedefinido(Sender: TObject);
begin
{
  PnRetiradaEntiViw.Visible := EdRetiradaEnti.ValueVariant <> 0;
}
end;

procedure TFmSoliComprCab.EdSeguro_PChange(Sender: TObject);
begin
{
  if EdSeguro_P.ValueVariant <> 0 then
  begin
    EdSeguro_V.Enabled := False;
    CalculaSeguro_V();
  end else EdSeguro_V.Enabled := True;
}
end;

procedure TFmSoliComprCab.EdTabelaPrcChange(Sender: TObject);
begin
{
  if ImgTipo.SQLType = stIns then
  begin
    EdMoeda.ValueVariant := DmPediVda.QrTabePrcCabMoeda.Value;
    CBMoeda.KeyValue     := DmPediVda.QrTabePrcCabMoeda.Value;
  end;
}
end;

procedure TFmSoliComprCab.Estrio1Click(Sender: TObject);
(*
var
  Entidade: Integer;
*)
begin
(*
  Entidade := EdCliente.ValueVariant;
  if Entidade <> 0 then
    GFat_Jan.MostraFormFisRegEnPsq(Entidade, EdRegrFiscal, CBRegrFiscal)
  else
    Geral.MB_Aviso('Defina o destinat�rio!');
*)
end;

procedure TFmSoliComprCab.Excluiitemprodutoselecionado1Click(Sender: TObject);
begin
(*
  DBCheck.QuaisItens_Exclui(Dmod.QrUpd, QrCustomizados, DBGrid1, 'pedivdaits',
   ['Controle'], ['Controle'], istPergunta, '');
  DmPediVda.AtzSdosPedido(QrSoliComprCabCodigo.Value);
  ReopenSoliComprGru(QrSoliComprGruNivel1.Value);
*)
end;

procedure TFmSoliComprCab.Excluipartedoprodutoselecionado1Click(Sender: TObject);
begin
(*
  DBCheck.QuaisItens_Exclui(Dmod.QrUpd, QrSoliComprCabCuz, DBGrid2, 'pedivdacuz',
   ['Conta'], ['Conta'], istPergunta, '');
  AtualizaItemCustomizado(QrCustomizadosControle.Value);
  DmPediVda.AtzSdosPedido(QrSoliComprCabCodigo.Value);
*)
end;

procedure TFmSoliComprCab.Excluisolicitaoatual1Click(Sender: TObject);
begin
//
end;

procedure TFmSoliComprCab.MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo: Integer);
begin
  case Mostra of
    0:
    begin
      PainelDados.Visible := True;
      PainelEdita.Visible := False;
    end;
    else
      Geral.MB_Aviso('A��o de Inclus�o/altera��o n�o definida!');
  end;
  ImgTipo.SQLType := SQLType;
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmSoliComprCab.MostraEnderecoDeEntrega2();
var
  Entidade: Integer;
begin
(*
  if (QrSoliComprCabL_Ativo.Value = 1) and (QrSoliComprCabEntregaEnti.Value = 0) then
  begin
    QrEntregaCli.Close;
    QrEntregaCli.Params[00].AsInteger := QrSoliComprCabCliente.Value;
    UMyMod.AbreQuery(QrEntregaCli, Dmod.MyDB, 'TFmFatDivGer2.QrCliAfterOpen()');
    //
    //DsEntregaEnti.DataSet := QrEntregaCli;
    MeEnderecoEntrega2.Text := QrEntregaCliE_ALL.Value;
  end else
  begin
    if QrSoliComprCabEntregaEnti.Value = 0 then
      Entidade := QrSoliComprCabCliente.Value
    else
      Entidade := QrSoliComprCabEntregaEnti.Value;
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrEntregaEnti, Dmod.MyDB, [
    'SELECT en.Codigo, en.Cadastro, ENatal, PNatal, Tipo, Respons1, Respons2, ',
    'ENumero, PNumero, ELograd, PLograd, ECEP, PCEP, ',
    'IF(en.Tipo=0, en.RazaoSocial, en.Nome   ) NOME_ENT, ',
    'IF(en.Tipo=0, en.Fantasia   , en.Apelido) NO_2_ENT, ',
    'IF(en.Tipo=0, en.CNPJ       , en.CPF    ) CNPJ_CPF, ',
    'IF(en.Tipo=0, en.IE         , en.RG     ) IE_RG, ',
    'IF(en.Tipo=0, en.NIRE       , ""        ) NIRE_, ',
    'IF(en.Tipo=0, en.ERua       , en.PRua   ) RUA, ',
    'IF(en.Tipo=0, en.ESite       , en.PSite   ) SITE, ',
    'IF(en.Tipo=0, en.ENumero    , en.PNumero) NUMERO, ',
    'IF(en.Tipo=0, en.ECompl     , en.PCompl ) COMPL, ',
    'IF(en.Tipo=0, en.EBairro    , en.PBairro) BAIRRO, ',
    'mun.Nome CIDADE, ',
    'IF(en.Tipo=0, lle.Nome      , llp.Nome  ) NOMELOGRAD, ',
    'IF(en.Tipo=0, ufe.Nome      , ufp.Nome  ) NOMEUF, ',
    'pai.Nome Pais, ',
    'IF(en.Tipo=0, en.ELograd    , en.PLograd) Lograd, ',
    'IF(en.Tipo=0, en.ECEP       , en.PCEP   ) CEP, ',
    'IF(en.Tipo=0, en.ETe1       , en.PTe1   ) TE1, ',
    'IF(en.Tipo=0, en.EFax       , en.PFax   ) FAX, ',
    'IF(en.Tipo=0, en.EEmail     , en.PEmail ) EMAIL, ',
    'IF(en.Tipo=0, lle.Trato     , llp.Trato ) TRATO, ',
    'IF(en.Tipo=0, en.EEndeRef   , en.PEndeRef  ) ENDEREF, ',
    'IF(en.Tipo=0, en.ECodMunici , en.PCodMunici) + 0.000 CODMUNICI, ',
    'IF(en.Tipo=0, en.ECodiPais  , en.PCodiPais ) + 0.000 CODPAIS, ',
    'RG, SSP, DataRG, IE ',
    'FROM entidades en ',
    'LEFT JOIN ufs ufe ON ufe.Codigo=en.EUF ',
    'LEFT JOIN ufs ufp ON ufp.Codigo=en.PUF ',
    'LEFT JOIN listalograd lle ON lle.Codigo=en.ELograd ',
    'LEFT JOIN listalograd llp ON llp.Codigo=en.PLograd ',
    'LEFT JOIN ' + VAR_AllID_DB_NOME + '.dtb_munici mun ON mun.Codigo = IF(en.Tipo=0, en.ECodMunici, en.PCodMunici) ',
    'LEFT JOIN ' + VAR_AllID_DB_NOME + '.bacen_pais pai ON pai.Codigo = IF(en.Tipo=0, en.ECodiPais, en.PCodiPais) ',
    'WHERE en.Codigo=' + Geral.FF0(Entidade),
    'ORDER BY NOME_ENT',
    '']);
    //DsEntregaEnti.DataSet := QrEntregaEnti;
    MeEnderecoEntrega2.Text := QrEntregaEntiE_ALL.Value;
  end;
*)
end;

(*
procedure TFmSoliComprCab.MostraFmPediVdaCuzParIns(CodUsu, PrdGrupTip, Nivel1, GraGruX, Controle:
  Integer; SQLType: TSQLType;
  Quants, MedidaC, MedidaL, MedidaA, MedidaE: Double);
var
  Continuar: Boolean;
  Num: String;
begin
  if DBCheck.CriaFm(TFmPediVdaCuzParIns2, FmPediVdaCuzParIns2, afmoNegarComAviso) then
  begin
    FmPediVdaCuzParIns2.ImgTipo.SQLType := SQLType;
    //FmPediVdaCuzParIns2.FGraGruX := GraGruX;
    FmPediVdaCuzParIns2.ReopenGraGruX(GraGruX);
    FmPediVdaCuzParIns2.FControle := Controle;
    if CodUsu > 0 then
    begin
      DmPediVda.QrPP.Close;
      DmPediVda.QrPP.Params[0].AsInteger := Nivel1;
      UnDmkDAC_PF.AbreQuery(DmPediVda.QrPP, Dmod.MyDB);
      //
      FmPediVdaCuzParIns2.RGGrupTip.ItemIndex       := 3;
      FmPediVdaCuzParIns2.EdMatPartCad.ValueVariant := DmPediVda.QrPPPartePrinc.Value;
      FmPediVdaCuzParIns2.CBMatPartCad.KeyValue     := DmPediVda.QrPPPartePrinc.Value;
      FmPediVdaCuzParIns2.EdGraGru1.ValueVariant    := CodUsu;
      FmPediVdaCuzParIns2.CBGraGru1.KeyValue        := CodUsu;
      FmPediVdaCuzParIns2.EdMedidaC.ValueVariant    := MedidaC;
      FmPediVdaCuzParIns2.EdMedidaL.ValueVariant    := MedidaL;
      FmPediVdaCuzParIns2.EdMedidaA.ValueVariant    := MedidaA;
      FmPediVdaCuzParIns2.EdMedidaE.ValueVariant    := MedidaE;
      FmPediVdaCuzParIns2.EdQuantS.ValueVariant     := 1; //QuantS;  Deve ser s� um !
      //
      Num := FormatFloat('0', GraGruX);
      {
      for C := 1 to GradeC.ColCount - 1 do
        for R := 1 to GradeC.RowCount do
        begin
          Txt := Geral.SoNumero_TT(GradeC.Cells[C,R]);
          if Txt = Num then
          begin
            FmPediVdaCuzParIns2.EdGraGruX.Text := Num;
            Break;
          end;
        end;
      }
      FmPediVdaCuzParIns2.EdGraGruX.Text := Num;
      FmPediVdaCuzParIns2.FLimitaCorTam := False;
    end else begin
      {
      }
    end;
    DmProd.QrNeed1.Close;
    DmProd.QrNeed1.Params[00].AsInteger := GraGruX;
    UnDmkDAC_PF.AbreQuery(DmProd.QrNeed1, Dmod.MyDB);
    FmPediVdaCuzParIns2.ShowModal;
    Continuar :=
      FmPediVdaCuzParIns2.CkContinuar.Checked;
    FmPediVdaCuzParIns2.Destroy;
    //
    if Continuar then
      MostraFmPediVdaCuzParIns(0, 0, 0, GraGruX, Controle, SQLType, 0, 0, 0, 0, 0);
  end;
end;
*)

procedure TFmSoliComprCab.MostraFormEntidade2(Entidade: Integer; Query: TmySQLQuery;
  EditCliente: TdmkEditCB; ComboCliente: TdmkDBLookupComboBox);
begin
  VAR_CADASTRO := 0;
  //
  DModG.CadastroDeEntidade(Entidade, fmcadEntidade2, fmcadEntidade2);
  //
  if VAR_CADASTRO <> 0 then
  begin
    UnDmkDAC_PF.AbreQuery(Query, Dmod.MyDB);
    //
    if Query.Locate('Codigo', VAR_CADASTRO, []) then
    begin
      EditCliente.ValueVariant := VAR_ENTIDADE;
      ComboCliente.KeyValue    := VAR_ENTIDADE;
      EditCliente.SetFocus;
    end;
  end;
end;

procedure TFmSoliComprCab.MostraSoliComprGru(SQLType: TSQLType);
begin
  DmPediVda.ReopenParamsEmp(QrSoliComprCabEmpresa.Value, True);
  if DBCheck.CriaFm(TFmSoliComprGru, FmSoliComprGru, afmoNegarComAviso) then
  begin
    FmSoliComprGru.FPedidoCompra := True;
    FmSoliComprGru.ImgTipo.SQLType := SQLType;
    if SQLType = stUpd then
    begin
      FmSoliComprGru.PnSeleciona.Enabled    := False;
      FmSoliComprGru.EdGraGru1.ValueVariant := QrSoliComprGruNivel1.Value;
      FmSoliComprGru.CBGraGru1.KeyValue     := QrSoliComprGruNivel1.Value;
    end;
    FmSoliComprGru.ShowModal;
    FmSoliComprGru.Destroy;
  end;
end;

(*
procedure TFmSoliComprCab.MostraPediVdaLei(SQLType: TSQLType);
begin
  DmPediVda.ReopenParamsEmp(QrSoliComprCabEmpresa.Value, True);
  if DBCheck.CriaFm(TFmPediVdaLei2, FmPediVdaLei2, afmoNegarComAviso) then
  begin
    FmPediVdaLei2.ImgTipo.SQLType := SQLType;
    FmPediVdaLei2.ShowModal;
    FmPediVdaLei2.Destroy;
    ReopenSoliComprGru(QrSoliComprGruNivel1.Value);
    ReopenSoliComprFor(0);
  end;
end;
*)

procedure TFmSoliComprCab.N1Click(Sender: TObject);
begin

end;

{
function TObtemQtde(var Qtde: Double): Boolean;
var
  ResVar: Variant;
begin
  // N�o pode, j� vem definido
  //Qtde := 0;
  Result := False;
  if MyObjects.GetValorDmk(TFmGetValor, FmGetValor, dmktfDouble,
  0, 3, 0, '', '', True, 'Itens', 'Informe a quantidade de itens: ',
  0, ResVar) then
  begin
    Qtde := Geral.IMV(ResVar);
    Result := True;
  end;
end;
}

procedure TFmSoliComprCab.PMCustomPopup(Sender: TObject);
(*
var
  Habilita: Boolean;
*)
begin
(*
  Habilita :=
    (QrCustomizados.State <> dsInactive)
    and
    (QrCustomizados.RecordCount > 0);
  Adicionapartesaoitemselecionado1.Enabled := Habilita;

  //

  Habilita := Habilita and
    (QrCustomizados.State <> dsInactive)
    and
    (QrCustomizados.RecordCount > 0)
    and
    (QrSoliComprCabCuz.State <> dsInactive)
    and
    (QrSoliComprCabCuz.RecordCount = 0);
  Excluiitemprodutoselecionado1.Enabled := Habilita;

  //

  Habilita :=
    (QrSoliComprCabCuz.State <> dsInactive)
    and
    (QrSoliComprCabCuz.RecordCount > 0);
  Excluipartedoprodutoselecionado1.Enabled := Habilita;
*)
end;

procedure TFmSoliComprCab.PMSolicitaPopup(Sender: TObject);
var
  Habilita: Boolean;
begin
  Habilita := (QrSoliComprCab.State <> dsInactive) and (QrSoliComprCab.RecordCount > 0);
  Alterasolicitaoatual1.Enabled := Habilita;
  Excluisolicitaoatual1.Enabled := Habilita;
  //
end;

procedure TFmSoliComprCab.PageControl1Change(Sender: TObject);
begin
  case PageControl1.ActivePageIndex of
    0: BtVisual.Visible := False;
    1: BtVisual.Visible := True;
  end;
end;

procedure TFmSoliComprCab.Cadastro1Click(Sender: TObject);
{$IfNDef NAO_GFAT}
var
  RegrFiscal: Integer;
begin
{
  VAR_CADASTRO := 0;
  //
  if not UMyMod.ObtemCodigoDeCodUsu(EdRegrFiscal, RegrFiscal,
    'Informe a regra fiscal!', 'Codigo', 'CodUsu') then Exit;
  //
  Grade_Jan.MostraFormFisRegCad(RegrFiscal);
  //
  if VAR_CADASTRO <> 0 then
  begin
    DmPediVda.QrFisRegCad.Close;
    UnDmkDAC_PF.AbreQuery(DmPediVda.QrFisRegCad, Dmod.MyDB);
    if DmPediVda.QrFisRegCad.Locate('Codigo', VAR_CADASTRO, []) then
    begin
      EdRegrFiscal.ValueVariant := DmPediVda.QrFisRegCadCodUsu.Value;
      CBRegrFiscal.KeyValue     := DmPediVda.QrFisRegCadCodUsu.Value;
      EdRegrFiscal.SetFocus;
    end;
  end;
}
{$Else}
  Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappGFat);
{$EndIf}
end;

procedure TFmSoliComprCab.CalculaDesoAces_V;
begin
  // Parei aqui!!! Falta fazer
end;

procedure TFmSoliComprCab.CalculaFrete_V;
begin
  // Parei aqui!!! Falta fazer
end;

procedure TFmSoliComprCab.CalculaSeguro_V;
begin
  // Parei aqui!!! Falta fazer
end;

procedure TFmSoliComprCab.CkEntregaUsaClick(Sender: TObject);
begin
{
  PnEntregaEntiAll.Visible := CkEntregaUsa.Checked;
  DefineEnderecoDeEntrega();
}
end;

procedure TFmSoliComprCab.CkRetiradaUsaClick(Sender: TObject);
begin
{
  PnRetiradaEntiAll.Visible := CkRetiradaUsa.Checked;
}
end;

procedure TFmSoliComprCab.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  //Va(vpLast);
end;

procedure TFmSoliComprCab.Custumizao1Click(Sender: TObject);
begin

end;

{
procedure TAlteraRegistro;
var
  PediVda : Integer;
begin
  PediVda := QrSoliComprCabCodigo.Value;
  if QrSoliComprCabCodigo.Value = 0 then
  begin
    BtAltera.Enabled := False;
    Exit;
  end;
  if not UMyMod.SelLockY(PediVda, Dmod.MyDB, 'solicomprcab', 'Codigo') then
  begin
    try
      UMyMod.UpdLockY(PediVda, Dmod.MyDB, 'solicomprcab', 'Codigo');
      MostraEdicao(1, CO_ALTERACAO, 0);
    finally
      Screen.Cursor := Cursor;
    end;
  end;
end;

procedure TIncluiRegistro;
var
  Cursor : TCursor;
  PediVda : Integer;
begin
  Cursor := Screen.Cursor;
  Screen.Cursor := crHourglass;
  Refresh;
  try
    PediVda := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
    'solicomprcab', 'solicomprcab', 'Codigo');
    if Length(FormatFloat(FFormatFloat, PediVda))>Length(FFormatFloat) then
    begin
      Geral.MB_Erro('Inclus�o cancelada. Limite de cadastros extrapolado');
      Screen.Cursor := Cursor;
      Exit;
    end;
    MostraEdicao(1, stIns, PediVda);
  finally
    Screen.Cursor := Cursor;
  end;
end;
}

procedure TFmSoliComprCab.QueryPrincipalAfterOpen;
begin
end;

procedure TFmSoliComprCab.DefineEnderecoDeEntrega();
(*
var
  Linha1, linha2, linha3: String;
  Entidade: Integer;
*)
begin
(*
  if (CkEntregaUsa.Checked) and (EdEntregaEnti.ValueVariant <> 0) then
    Entidade := EdEntregaEnti.ValueVariant
  else
    Entidade := EdCliente.ValueVariant;
  //
  MeEnderecoEntrega1.Text := DModG.ObtemEnderecoEntrega3Linhas(
    Entidade, Linha1, Linha2, Linha3);
  if EdCliente.ValueVariant = 0 then
  begin
    DBEdCidade.DataField := '';
    DBEdNOMEUF.DataField := '';
  end else begin
    DBEdCidade.DataField := 'CIDADE';
    DBEdNOMEUF.DataField := 'NOMEUF';
  end;
*)
end;

procedure TFmSoliComprCab.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmSoliComprCab.BtVisualClick(Sender: TObject);
begin
  if DBGGru.Align = alLeft then
  begin
    FDBGWidth := DBGGru.Width;
    DBGGru.Align := alTop;
    DBGGru.Height := 68; // 20 + 18 + 20
  end else begin
    DBGGru.Align := alLeft;
    DBGGru.Width := FDBGWidth;
  end;
end;

procedure TFmSoliComprCab.BitBtn1Click(Sender: TObject);
begin
{
  if DBCheck.CriaFm(TFmPediVdaImp, FmPediVdaImp, afmoNegarComAviso) then
  begin
    FmPediVdaImp.ShowModal;
    FmPediVdaImp.Destroy;
  end;
}
end;

procedure TFmSoliComprCab.BtCondicaoPGClick(Sender: TObject);
var
  CondicaoPG: Integer;
begin
{
  VAR_CADASTRO := 0;
  //
  if DmPediVda.QrPediPrzCab.RecordCount > 0 then
  begin
    if not UMyMod.ObtemCodigoDeCodUsu(EdCondicaoPG, CondicaoPG,
      'Informe a condi��o de pagamento!', 'Codigo', 'CodUsu') then Exit;
  end else
    CondicaoPG := 0;
  //
  Praz_PF.MostraFormPediPrzCab1(CondicaoPG);
  //
  if VAR_CADASTRO <> 0 then
  begin
    DmPediVda.QrPediPrzCab.Close;
    UnDmkDAC_PF.AbreQuery(DmPediVda.QrPediPrzCab, Dmod.MyDB);
    if DmPediVda.QrPediPrzCab.Locate('Codigo', VAR_CADASTRO, []) then
    begin
      EdCondicaoPg.ValueVariant := VAR_CADASTRO;
      CBCondicaoPg.KeyValue     := VAR_CADASTRO;
      EdCondicaoPG.SetFocus;
    end;
  end;
}
end;

procedure TFmSoliComprCab.SpeedButton11Click(Sender: TObject);
begin
{
  MostraFormEntidade2(EdRedespacho.ValueVariant, DmPediVda.QrTransportas,
    EdRedespacho, CBRedespacho);
}
end;

procedure TFmSoliComprCab.SpeedButton12Click(Sender: TObject);
var
  Represen: Integer;
begin
{
  VAR_CADASTRO := 0;
  Represen     := EdRepresen.ValueVariant;
  //
  if DBCheck.CriaFm(TFmPediAcc, FmPediAcc, afmoNegarComAviso) then
  begin
    if Represen <> 0 then
      FmPediAcc.LocCod(Represen, Represen);
    FmPediAcc.ShowModal;
    FmPediAcc.Destroy;
    if VAR_CADASTRO <> 0 then
    begin
      DmPediVda.QrPediAcc.Close;
      UnDmkDAC_PF.AbreQuery(DmPediVda.QrPediAcc, Dmod.MyDB);
      //if DmPediVda.QrPediAcc.Locate('Codigo', VAR_CADASTRO, []) then
      //begin
        EdRepresen.ValueVariant := VAR_CADASTRO;
        CBRepresen.KeyValue     := VAR_CADASTRO;
        EdRepresen.SetFocus;
      //end;
    end;
  end;
}
end;

procedure TFmSoliComprCab.SpeedButton13Click(Sender: TObject);
{$IfNDef NO_FINANCEIRO}
var
  CartEmis: Integer;
begin
{
  VAR_CADASTRO := 0;
  CartEmis     := EdCartEmis.ValueVariant;
  //
  FinanceiroJan.CadastroDeCarteiras(CartEmis);
  //
  if VAR_CADASTRO <> 0 then
  begin
    DmPediVda.ReopenCartEmis(VuEmpresa.ValueVariant);
    //
    EdCartEmis.ValueVariant := VAR_CADASTRO;
    CBCartEmis.KeyValue     := VAR_CADASTRO;
    EdCartEmis.SetFocus;
  end;
}
{$Else}
  Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappFinanceiro);
{$EndIf}
end;

procedure TFmSoliComprCab.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmSoliComprCab.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmSoliComprCab.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmSoliComprCab.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmSoliComprCab.SpeedButton6Click(Sender: TObject);
begin
{
  MostraFormEntidade2(EdTransporta.ValueVariant, DmPediVda.QrTransportas,
    EdTransporta, CBTransporta);
}
end;

procedure TFmSoliComprCab.BtTabelaPrcClick(Sender: TObject);
{$IfNDef NAO_GFAT}
var
  TabelaPrc: Integer;
begin
{
  VAR_CADASTRO := 0;
  //
  if not UMyMod.ObtemCodigoDeCodUsu(EdTabelaPrc, TabelaPrc,
    'Informe a tabela de pre�o!', 'Codigo', 'CodUsu') then Exit;
  //
  GFat_Jan.MostraFormTabePrcCab(TabelaPrc);
  //
  if VAR_CADASTRO <> 0 then
  begin
    DmPediVda.QrTabePrcCab.Close;
    UnDmkDAC_PF.AbreQuery(DmPediVda.QrTabePrcCab, Dmod.MyDB);
    if DmPediVda.QrTabePrcCab.Locate('Codigo', VAR_CADASTRO, []) then
    begin
      EdTabelaPrc.ValueVariant := VAR_CADASTRO;
      CBTabelaPrc.KeyValue     := VAR_CADASTRO;
      EdTabelaPrc.SetFocus;
    end;
  end;
}
{$Else}
  Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappGFat);
{$EndIf}
end;

procedure TFmSoliComprCab.SpeedButton9Click(Sender: TObject);
var
  Moeda: Integer;
begin
{
  VAR_CADASTRO := 0;
  //
  if not UMyMod.ObtemCodigoDeCodUsu(EdMoeda, Moeda,
    'Informe a moeda!', 'Codigo', 'CodUsu') then Exit;
  //
  if DBCheck.CriaFm(TFmCambioMda, FmCambioMda, afmoNegarComAviso) then
  begin
    if Moeda <> 0 then
      FmCambioMda.LocCod(Moeda, Moeda);
    FmCambioMda.ShowModal;
    FmCambioMda.Destroy;
    if VAR_CADASTRO <> 0 then
    begin
      DModG.QrCambioMda.Close;
      UnDmkDAC_PF.AbreQuery(DModG.QrCambioMda, Dmod.MyDB);
      if DModG.QrCambioMda.Locate('Codigo', VAR_CADASTRO, []) then
      begin
        EdMoeda.ValueVariant := VAR_CADASTRO;
        CBMoeda.KeyValue     := VAR_CADASTRO;
        EdMoeda.SetFocus;
      end;
    end;
  end;
}
end;

procedure TFmSoliComprCab.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrSoliComprCabCodigo.Value;
  Close;
end;

procedure TFmSoliComprCab.Adicionapartesaoitemselecionado1Click(Sender: TObject);
begin
(*
  MostraFmPediVdaCuzParIns(0, 0, 0, QrCustomizadosGraGruX.Value,
    QrCustomizadosControle.Value, stIns, 0, 0, 0, 0, 0);
*)
end;

procedure TFmSoliComprCab.AlteraExcluiIncluiitemselecionado1Click(Sender: TObject);
begin
  //n�o alterar / excluir quando tiver customiza��o
  GradeQDblClick2();
end;

procedure TFmSoliComprCab.Alteraitemprodutoselecionado1Click(Sender: TObject);
begin
(*
  if DBCheck.CriaFm(TFmPediVdaCuzUpd2, FmPediVdaCuzUpd2, afmoNegarComAviso) then
  begin
    FmPediVdaCuzUpd2.ReopenPediVdaIts(QrCustomizadosControle.Value);
    FmPediVdaCuzUpd2.ShowModal;
    FmPediVdaCuzUpd2.Destroy;
  end;
*)
end;

procedure TFmSoliComprCab.Alterasolicitaoatual1Click(Sender: TObject);
begin
  // ini 2022-04-03
  // Evitar erros
  RGEntSai.ItemIndex := QrSoliComprCabEntSai.Value;
  // fim 2022-04-03
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PainelEdita, QrSoliComprCab, [PainelDados],
    [PainelEdita], nil, ImgTipo, 'solicomprcab');
  //
{
  RGIndSinc.ItemIndex := 1;
  RGIndSinc.Enabled   := False;
}
  //
  DesabilitaComponentes();
end;

procedure TFmSoliComprCab.AplicardescontoGeral1Click(Sender: TObject);
begin
//
end;

(*
procedure TFmSoliComprCab.AtualizaItemCustomizado(Controle: Integer);
var
  PrecoO, PrecoR, PrecoF, ValBru, ValLiq, DescoP, DescoV, PercCustom, QuantS: Double;
  InfAdCuztm, Codigo: Integer;
  Nome: String;
begin
  InfAdCuztm := 0;
  DmPediVda.QrIts.Close;
  DmPediVda.QrIts.Params[0].AsInteger := Controle;
  UnDmkDAC_PF.AbreQuery(DmPediVda.QrIts, Dmod.MyDB);
  //
  DmPediVda.QrSumCuz.Close;
  DmPediVda.QrSumCuz.Params[0].AsInteger := Controle;
  UnDmkDAC_PF.AbreQuery(DmPediVda.QrSumCuz, Dmod.MyDB);
  //
  DmPediVda.QrCustomiz.Close;
  DmPediVda.QrCustomiz.Params[0].AsInteger := Controle;
  UnDmkDAC_PF.AbreQuery(DmPediVda.QrCustomiz, Dmod.MyDB);
  //
  Nome := '';
  while not DmPediVda.QrCustomiz.Eof do
  begin
    Nome := Nome + ' ' + DmPediVda.QrCustomizSiglaCustm.Value;
    DmPediVda.QrCustomiz.Next;
  end;
  Nome := Copy(Trim(Nome), 1, 255);
  if Nome <> '' then
  begin
    DmPediVda.QrPesInfCuz.Close;
    DmPediVda.QrPesInfCuz.Params[0].AsString := Nome;
    UnDmkDAC_PF.AbreQuery(DmPediVda.QrPesInfCuz, Dmod.MyDB);
    if DmPediVda.QrPesInfCuz.RecordCount > 0 then
      InfAdCuztm := DmPediVda.QrPesInfCuzCodigo.Value
    else begin
      InfAdCuztm := DModG.BuscaProximoCodigoInt('nfectrl', 'nfeinfcuz', '', 0);
      UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'nfeinfcuz', False, [
      'Nome'], ['Codigo'], [Nome], [InfAdCuztm], True);
    end;
  end;
  {
  ValLiq := QrSumCuzValLiq.Value;
  DescoV := QrSumCuzDescoV.Value;
  ValBru := ValLiq + DescoV;
  //
  }
  QuantS := DmPediVda.QrItsQuantS.Value;
  ValLiq := DmPediVda.QrSumCuzValLiq.Value * QuantS;
  DescoV := DmPediVda.QrSumCuzDescoV.Value * QuantS;
  ValBru := ValLiq + DescoV;
  if DmPediVda.QrItsQuantS.Value = 0 then
    PrecoO := 0
  else
    PrecoO := Round(ValBru / QuantS * 100) / 100;
  PrecoR := PrecoO;
  //
  if ValBru = 0 then
    DescoP := 0
  else
    DescoP := DescoV / ValBru * 100;
  //
  if QuantS > 0 then
    PrecoF := ValLiq / QuantS// * ((100 - DescoP)/100)
  else
    PrecoF := 0;
  PercCustom :=   DmPediVda.QrSumCuzPercCustom.Value;
  if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'pedivdaits', False, [
    'PrecoO', 'PrecoR', 'ValBru', 'DescoP', 'DescoV',
    'ValLiq', 'PrecoF', 'InfAdCuztm', 'PercCustom'
  ], ['Controle'], [
    PrecoO, PrecoR, ValBru, DescoP, DescoV,
    ValLiq, PrecoF, InfAdCuztm, PercCustom
  ], [Controle], True) then
  begin
    Codigo := QrSoliComprCabCodigo.Value;
    DmPediVda.AtzSdosPedido(Codigo);
    LocCod(Codigo, Codigo);
    QrSoliComprGru.Locate('Nivel1', DmPediVda.QrItsGraGru1.Value, []);
    if QrCustomizados.State <> dsInactive then
      QrCustomizados.Locate('Controle', Controle, []);
  end;
end;
*)

procedure TFmSoliComprCab.AtualizaValoresdoitem1Click(Sender: TObject);
begin
(*
  AtualizaItemCustomizado(QrCustomizadosControle.Value);
*)
end;

procedure TFmSoliComprCab.BtSolicitaClick(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 0;
  MyObjects.MostraPopUpDeBotao(PMSolicita, BtSolicita);
end;

procedure TFmSoliComprCab.BtRecalculaClick(Sender: TObject);
begin
{
  Screen.Cursor := crHourGlass;
  try
    DmPediVda.AtualizaTodosItensPediVda_(QrSoliComprCabCodigo.Value);
    DmPediVda.AtzSdosPedido(QrSoliComprCabCodigo.Value);
    DmPediVda.AtualizaTodosItensPediVda_(QrSoliComprCabCodigo.Value);
    LocCod(QrSoliComprCabCodigo.Value, QrSoliComprCabCodigo.Value);
  finally
    Screen.Cursor := crDefault;
  end;
}
end;

procedure TFmSoliComprCab.BtConfirmaClick(Sender: TObject);
var
  Codigo, RegrFiscal, indPres, finNFe: Integer;
  Mensagem: String;
  FretePor, modFrete: Integer;
  SQLType: TSQLType;
begin
  SQLType := ImgTipo.SQLType;
  //
  if MyObjects.FIC(EdCodUsu.ValueVariant = 0, EdCodUsu,
    'Informe o n�mero da solicita��o!') then Exit;
  if MyObjects.FIC(EdEmpresa.ValueVariant = 0, EdEmpresa,
    'Informe a empresa!') then Exit;
  if MyObjects.FIC(TPDtaPrevi.Date < 2, TPDtaPrevi,
    'Informe a data de previs�o de entrega!') then Exit;
{
  if MyObjects.FIC(EdCondicaoPG.ValueVariant = 0, EdCondicaoPG,
    'Informe a condi��o de pagamento!') then Exit;
  if MyObjects.FIC(EdCartEmis.ValueVariant = 0, EdCartEmis,
    'Informe a carteira!') then Exit;
  if MyObjects.FIC(EdRegrFiscal.ValueVariant = 0, EdRegrFiscal,
    'Informe a movimenta��o (fiscal)!') then Exit;
  DModG.DefineFretePor(EdFretePor.ValueVariant, FretePor, modFrete);
}
  //
{
  if CkEntregaUsa.Checked = False then
  begin
    EdEntregaEnti.ValueVariant := 0;
    CBEntregaEnti.KeyValue := 0;
  end;
  if CkRetiradaUsa.Checked = False then
  begin
    EdRetiradaEnti.ValueVariant := 0;
    CBRetiradaEnti.KeyValue := 0;
  end;
}
  //Valida regra fiscal com os dados do cliente
  (*
  if not UMyMod.ObtemCodigoDeCodUsu(EdRegrFiscal, RegrFiscal,
    'Informe a regra fiscal!', 'Codigo', 'CodUsu') then Exit;
  //
  if DmPediVda.QrParamsEmpPediVdaNElertas.Value = 0 then //Para quem usa o pedido mas n�o usa a nota
  begin
    if MyObjects.FIC(DmPediVda.QrClientesDOCENT.Value = '', EdCliente,
      'O Destinat�rio n�o possui CNPJ / CPF cadastrado!') then
    begin
      if Geral.MB_Pergunta('Deseja editar agora?' + sLineBreak +
        'AVISO: Verifique se os demais campos est�o devidamente preenchidos!') = ID_YES then
      begin
        MostraFormEntidade2(EdCliente.ValueVariant, DmPediVda.QrClientes,
          EdCliente, CBCliente);
      end;
      Exit;
    end;
    // NFe 3.10
    if MyObjects.FIC(RG_idDest.ItemIndex <= 0, RG_idDest,
      'Informe o Local de Destino da Opera��o!') then Exit;
    // NFe 3.10
    if not Entities.ValidaIndicadorDeIEEntidade_2(DmPediVda.QrClientesIE.Value,
      DmPediVda.QrClientesindIEDest.Value, DmPediVda.QrClientesTipo.Value,
       RG_idDest.ItemIndex, RG_indFinal.ItemIndex, False, Mensagem) then
    begin
      Geral.MB_Aviso(Mensagem);
      Exit;
    end;
    if MyObjects.FIC(RG_indFinal.ItemIndex < 0, RG_indFinal,
      'Informe a opera��o com o consumidor final!') then Exit;
    {$IfNDef SemNFe_0000}
      if MyObjects.FIC(EdindPres.Text = '', EdindPres,
        'Informe o indicador de presen�a do comprador!') then Exit;
      indPres := Geral.IMV(EdindPres.Text);
      if MyObjects.FIC(not (indPres in [0,1,2,3,4,9]), EdindPres,
        'Informe o indicador de presen�a do comprador!') then Exit;

      if MyObjects.FIC(Edide_finNFe.Text = '', Edide_finNFe,
        'Informe a finalidade de emiss�o da NF-e!') then Exit;
      finNFe := Geral.IMV(Edide_finNFe.Text);
      if MyObjects.FIC(not (finNFe in [1,2,3,4]), Edide_finNFe,
        'Informe a finalidade de emiss�o da NF-e!') then Exit;
    {$EndIf}
    if not DmPediVda.ValidaRegraFiscalCFOPCliente(RG_idDest.ItemIndex,
      RG_indFinal.ItemIndex, DmPediVda.QrClientesindIEDest.Value, RegrFiscal,
      DmPediVda.QrFisRegCadNome.Value, DmPediVda.QrClientesIE.Value,
      DmPediVda.QrClientesNOMEUF.Value, DmPediVda.QrParamsEmpUF_WebServ.Value,
      Mensagem) then
    begin
      Geral.MB_Info(Mensagem);
      //
      if Geral.MB_Pergunta('Deseja configurar a Regra Fiscal agora?') = ID_YES then
        Grade_Jan.MostraFormFisRegCad(RegrFiscal);
      //
      Exit;
    end;
  end;
  *)
  //
  Codigo := EdCodigo.ValueVariant;
  Codigo := UMyMod.BPGS1I32('solicomprcab', 'Codigo', '', '', tsPos, SQLType, Codigo);
  if UMyMod.ExecSQLInsUpdPanel(SQLType, FmSoliComprCab, PainelEdita,
    'solicomprcab', Codigo, Dmod.QrUpd, [PainelEdita], [PainelDados], ImgTipo, True) then
  begin
    LocCod(Codigo, Codigo);
    RGEntSai.ItemIndex := 0;
  end;
end;

procedure TFmSoliComprCab.BtForneceClick(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 0;
  MyObjects.MostraPopUpDeBotao(PMFornece, BtFornece);
end;

procedure TFmSoliComprCab.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := Geral.IMV(EdCodigo.Text);
(*
  if ImgTipo.SQLType = stIns then UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'solicomprcab', Codigo);
*)
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'solicomprcab', 'Codigo');
  MostraEdicao(0, stLok, 0);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'solicomprcab', 'Codigo');
end;

procedure TFmSoliComprCab.BtFisRegCadClick(Sender: TObject);
{$IfNDef NAO_GFAT}
var
  Codigo, FisRegCad: Integer;
begin
(*
  Codigo    := QrSoliComprCabCodigo.Value;
  FisRegCad := QrSoliComprCabRegrFiscal.Value;
  //
  Grade_Jan.MostraFormFisRegCad(FisRegCad);
  try
    Screen.Cursor := crHourGlass;
    LocCod(Codigo, Codigo);
  finally
    Screen.Cursor := crDefault;
  end;
*)
{$Else}
  Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappGFat);
{$EndIf}
end;

procedure TFmSoliComprCab.BtGraGruNClick(Sender: TObject);
{$IfNDef NAO_GFAT}
var
  Codigo, Nivel1: Integer;
begin
  Codigo := QrSoliComprCabCodigo.Value;
  Nivel1 := QrSoliComprGruNivel1.Value;
  //
  Grade_Jan.MostraFormGraGruN(Nivel1);
  try
    Screen.Cursor := crHourGlass;
    LocCod(Codigo, Codigo);
  finally
    Screen.Cursor := crDefault;
  end;
{$Else}
  Grl_Geral.InfoSemModulo(TDmkModuloApp.mdlappGFat);
{$EndIf}
end;

procedure TFmSoliComprCab.BtItensClick(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 0;
  MyObjects.MostraPopUpDeBotao(PMItens, BtItens);
end;

procedure TFmSoliComprCab.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLtype := stLok;
  //
  CBEmpresa.ListSource     := DModG.DsEmpresas;
  DBEdREF_NIV1.DataField   := '';
{
  CBTabelaPrc.ListSource   := DmPediVda.DsTabePrcCab;
  CBCondicaoPG.ListSource  := DmPediVda.DsPediPrzCab;
  CBCartEmis.ListSource    := DmPediVda.DsCartEmis;
  CBFretePor.ListSource    := DmPediVda.DsFretePor;
  CBTransporta.ListSource  := DmPediVda.DsTransportas;
  CBRedespacho.ListSource  := DmPediVda.DsRedespachos;
  CBRepresen.ListSource    := DmPediVda.DsPediAcc;
  CBRegrFiscal.ListSource  := DmPediVda.DsFisRegCad;
  CBmoeda.ListSource       := DModG.DsCambioMda;
}
  //
  QrSoliComprGruValLiq.DisplayFormat   := Dmod.FStrFmtPrc;
  QrSoliComprGruValLiq.DisplayFormat   := Dmod.FStrFmtPrc;
(*
  QrCustomizadosValLiq.DisplayFormat := Dmod.FStrFmtPrc;
  QrSoliComprCabCuzPrecoR.DisplayFormat   := Dmod.FStrFmtPrc;
  QrSoliComprCabCuzValBru.DisplayFormat   := Dmod.FStrFmtPrc;
  QrSoliComprCabCuzValLiq.DisplayFormat   := Dmod.FStrFmtPrc;
*)
  //
  QrSoliComprCab.Database      := Dmod.MyDB;
  QrSoliComprGru.Database   := Dmod.MyDB;
  QrSoliComprIts.Database   := Dmod.MyDB;
(*
  QrCustomizados.Database := Dmod.MyDB;
  QrSoliComprCabCuz.Database   := Dmod.MyDB;
  QrCli.Database          := Dmod.MyDB;
  QrItsC.Database         := Dmod.MyDB;
  QrItsZ.Database         := Dmod.MyDB;
*)
  QrItsN.Database         := Dmod.MyDB;
  //
  //
  PageControl1.ActivePageIndex := 0;
  PcDadosTop.ActivePageIndex := 0;
  PageControl3.ActivePageIndex := 0;
  PageControl1.Align := alClient;
  PcDadosTop.Align := alClient;
  //
{
  RGIndSinc.ItemIndex := 1;
  RGIndSinc.Enabled   := False;
}
  //
  TPDtaEmiss.Date := Date;
  TPDtaEntra.Date := Date;
  TPDtaInclu.Date := Date;
  TPDtaPrevi.Date := Date;
  CriaOForm;
  GradeA.ColWidths[0] := 128;
  GradeC.ColWidths[0] := 128;
  GradeX.ColWidths[0] := 128;
  GradeQ.ColWidths[0] := 128;
  GradeF.ColWidths[0] := 128;
  GradeD.ColWidths[0] := 128;
  GradeV.ColWidths[0] := 128;
{$IfNDef SemNFe_0000}
  FindPres := UnNFe_PF.ListaIndicadorDePresencaComprador();
  F_finNFe := UnNFe_PF.ListaFinalidadeDeEmiss�oDaNFe(True);
  F_IND_PGTO_EFD := UnNFe_PF.ListaIND_PAG_EFD();
{$Else}
  FindPres := 0;
  F_finNFe := 0;
{$EndIf}
end;


procedure TFmSoliComprCab.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrSoliComprCabCodigo.Value, LaRegistro.Caption);
end;

procedure TFmSoliComprCab.SbCentroCustoClick(Sender: TObject);
var
  CentroCusto: Integer;
begin
  VAR_CADASTRO := 0;
  CentroCusto      := EdCentroCusto.ValueVariant;
  //
  CeCuRe_Jan.MostraFormCentroCusto(CentroCusto);
  if VAR_CADASTRO <> 0 then
  begin
    UMyMod.SetaCodigoPesquisado(EdCentroCusto, CBCentroCusto, QrCentroCusto, VAR_CADASTRO);
    EdCentroCusto.SetFocus;
  end;
end;

procedure TFmSoliComprCab.SbEntregaEntiClick(Sender: TObject);
begin
{
  MostraFormEntidade2(EdEntregaEnti.ValueVariant, DmPediVda.QrEntregaEnti,
    EdEntregaEnti, CBEntregaEnti);
}
end;

procedure TFmSoliComprCab.SbImprimeClick(Sender: TObject);
begin
  if QrSoliComprCab.State <> dsInactive then
    Imprime()
  else
    Geral.MB_Aviso('Nenhuma solicita��o foi aberta (ou pesquisada)!');
end;

procedure TFmSoliComprCab.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmSoliComprCab.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrSoliComprCabCodUsu.Value, LaRegistro.Caption);
end;

procedure TFmSoliComprCab.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmSoliComprCab.QrSoliComprCabAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
  BtItens.Enabled := QrSoliComprCab.RecordCount > 0;
  BtFornece.Enabled := QrSoliComprCab.RecordCount > 0;
  //Geral.MB_Teste(QrSoliComprCab.SQL.Text);
end;

procedure TFmSoliComprCab.QrSoliComprCabAfterScroll(DataSet: TDataSet);
(*
var
  Linha1, Linha2, Linha3: String;
  Entidade: Integer;
*)
begin
  ReopenSumIts();
  ReopenSoliComprGru(0);
  ReopenSoliComprFor(0);
{ ini 2022-04-03
  case QrSoliComprCabEntSai.Value of
    0(*ent*): DmPediVda.ReopenParamsEmp(QrSoliComprCabCliente.Value, True);
    1(*sai*): DmPediVda.ReopenParamsEmp(QrSoliComprCabEmpresa.Value, True);
  end;
}
  DmPediVda.ReopenParamsEmp(QrSoliComprCabEmpresa.Value, True);
  // fim 2022-04-03
  //
  MostraEnderecoDeEntrega2();
end;

procedure TFmSoliComprCab.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmSoliComprCab.SbQueryClick(Sender: TObject);
begin
  LocCod(QrSoliComprCabCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'solicomprcab', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmSoliComprCab.SbRegrFiscalClick(Sender: TObject);
begin
{
  MyObjects.MostraPopupDeBotao(PMRegrFiscal, SbRegrFiscal);
}
end;

procedure TFmSoliComprCab.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmSoliComprCab.FormShow(Sender: TObject);
begin
  //FCodUsuTxt_Old := EdCodUsu.Text;
  ReopenCentroCusto();
end;

procedure TFmSoliComprCab.frxPED_VENDA_001_01GetValue(const VarName: string;
  var Value: Variant);
begin
  if VarName = 'MeuLogo3x1Existe' then
    Value := FileExists(DModG.QrParamsEmpLogo3x1.Value)
  else
  if VarName = 'MeuLogo3x1Caminho' then
    Value := DModG.QrParamsEmpLogo3x1.Value
  else
  if VarName = 'VARF_TIPOMOV_TIPOENTI' then
  begin
    if QrSoliComprCabEntSai.Value = 1 then
      Value := 'Cliente:'
    else
      Value := 'Fornecedor:'
  end else
  if VarName = 'VARF_TIPOMOV_NUMPED' then
  begin
    if QrSoliComprCabEntSai.Value = 1 then
      Value := 'N� DA SOLICITA��O DE COMPRA: ' + QrSoliComprCabPedidoCli.Value
    else
      Value := 'N� DA SOLICITA��O DE VENDA: ' + QrSoliComprCabPedidoCli.Value
  end
  else
  if VarName = 'VARF_TIPOMOV_NOREPRESENT' then
  begin
(*
    if QrSoliComprCabNOMEACC.Value <> EmptyStr then
    begin
      Value := 'Representante:' + QrSoliComprCabNOMEACC.Value
    end else
*)
      Value := '';
  end
  else
end;

procedure TFmSoliComprCab.GradeADrawCell(Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);
begin
  MyObjects.DesenhaGradeA(GradeA, ACol, ARow, Rect, State, True);
end;

procedure TFmSoliComprCab.GradeCDrawCell(Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);
begin
  MyObjects.DesenhaGradeN(GradeC, GradeA, nil, ACol, ARow, Rect, State,
  '0', 0, 0, True);
end;

procedure TFmSoliComprCab.GradeDDrawCell(Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);
begin
  MyObjects.DesenhaGradeN(GradeD, GradeA, nil, ACol, ARow, Rect, State,
  Dmod.FStrFmtPrc, 0, 0, False);
end;

procedure TFmSoliComprCab.GradeFDrawCell(Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);
begin
  MyObjects.DesenhaGradeN(GradeF, GradeA, nil, ACol, ARow, Rect, State,
  Dmod.FStrFmtPrc, 0, 0, False);
end;

procedure TFmSoliComprCab.GradeQDblClick(Sender: TObject);
begin
  GradeQDblClick2();
end;

procedure TFmSoliComprCab.GradeQDblClick2;
var
  GraGruX: Integer;
  SQLType: TSQLType;
begin
  // somente item n�o customizado
  if QrSoliComprGruItensCustomizados.Value = 0 then
  begin
    if (GradeQ.Col > 0) and (GradeQ.Row > 0) then
    begin
      GraGruX := Geral.IMV(GradeC.Cells[GradeQ.Col, GradeQ.Row]);
      if (QrSoliComprGru.RecordCount > 0) and (GraGruX > 0)
      //and (GradeQ.Col > 0) and (GradeQ.Row > 0)
      then
      begin
        DmPediVda.QrItemSCI.Close;
        DmPediVda.QrItemSCI.Params[00].AsInteger := QrSoliComprCabCodigo.Value;
        DmPediVda.QrItemSCI.Params[01].AsInteger := GraGruX;
        UnDmkDAC_PF.AbreQuery(DmPediVda.QrItemSCI, Dmod.MyDB);
        //
  // ini 2022-04-03
        {if DmPediVda.QrItemSCI.RecordCount > 0 then
        begin
          UMyMod.FormInsUpd_Show(TFmPediVdaAlt2, FmPediVdaAlt2, afmoNegarComAviso,
          DmPediVda.QrItemSCI, stUpd);
        end else begin
          UMyMod.FormInsUpd_Show(TFmPediVdaAlt2, FmPediVdaAlt2, afmoNegarComAviso,
          DmPediVda.QrItemSCI, stIns);
        end;}
        if DmPediVda.QrItemSCI.RecordCount > 0 then
          SQLType := stUpd
        else
          SQLType := stIns;
        UMyMod.FormInsUpd_Show(TFmSoliComprIts, FmSoliComprIts,
          afmoNegarComAviso, DmPediVda.QrItemSCI, SQLType);
  // fim 2022-04-03
      end else
        Geral.MB_Aviso('N�o h� item a ser editado/exclu�do!');
    end else
      Geral.MB_Aviso(
        'Antes clicar para editar/excluir, selecione a c�lula do item a ser editado/exclu�do!' +
        sLineBreak +
        'Uma forma direta de editar/excluir � dar um duplo clique direto na c�lula.');
  end else
    Geral.MB_Aviso(
    'Este item � customizado! Para edit�-lo, clique no bot�o pr�prio!');
end;

procedure TFmSoliComprCab.GradeQDrawCell(Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);
begin
  MyObjects.DesenhaGradeN(GradeQ, GradeA, nil, ACol, ARow, Rect, State,
  dmkPF.FormataCasas(QrSoliComprGruFracio.Value), 0, 0, False);
end;

procedure TFmSoliComprCab.GradeVDrawCell(Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);
begin
  MyObjects.DesenhaGradeN(GradeV, GradeA, nil, ACol, ARow, Rect, State,
  Dmod.FStrFmtPrc, 0, 0, true);
end;

procedure TFmSoliComprCab.GradeXDrawCell(Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);
begin
  MyObjects.DesenhaGradeN(GradeX, GradeA, nil, ACol, ARow, Rect, State,
  '0', 0, 0, True);
end;

procedure TFmSoliComprCab.Imprime();
var
  Titulo: String;
begin
  ReopenFor();
  DModG.ReopenParamsEmp(QrSoliComprCabEmpresa.Value);
  DModG.ReopenEndereco(QrSoliComprCabEmpresa.Value);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrItsN, Dmod.MyDB, [
  'SELECT gg1.CodUsu CU_NIVEL1, gg1.Nome NO_NIVEL1,',
  'gti.Nome NO_TAM, gcc.Nome NO_COR,   ',
  'pvi.QuantS, pvi.PrecoR, pvi.DescoP,',
  'pvi.PrecoF, pvi.ValBru, pvi.ValLiq, 0 KGT,',
  '',
  'pvi.vProd, pvi.vFrete, pvi.vSeg, pvi.vOutro,',
  'pvi.vDesc, pvi.vBC',
  '',
  'FROM solicomprits pvi',
  'LEFT JOIN gragrux ggx ON ggx.Controle=pvi.GraGrux',
  'LEFT JOIN gratamits gti ON gti.Controle=ggx.GraTamI',
  'LEFT JOIN gragruc ggc ON ggc.Controle=ggx.GraGruC',
  'LEFT JOIN gracorcad gcc ON gcc.Codigo=ggc.GraCorCad',
  'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1',
  'WHERE pvi.Codigo=' + Geral.FF0(QrSoliComprCabCodigo.Value),
  //'AND pvi.Customizad=0',
  '']);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrUsuario, Dmod.MyDB, [
  'SELECT pvd.UserCad, pvd.UserAlt,',
  'IF(se1.Funcionario<>0, IF(en1.Tipo=0, en1.RazaoSocial, en1.Nome), ',
  '  se1.Login) NO_LANCADOR,',
  'IF(se2.Funcionario<>0, IF(en2.Tipo=0, en2.RazaoSocial, en2.Nome), ',
  '  se2.Login) NO_ALTERADOR',
  'FROM solicomprcab pvd',
  'LEFT JOIN senhas se1 ON se1.Numero=pvd.UserCad',
  'LEFT JOIN entidades en1 ON en1.Codigo=se1.Funcionario',
  'LEFT JOIN senhas se2 ON se2.Numero=pvd.UserAlt',
  'LEFT JOIN entidades en2 ON en2.Codigo=se2.Funcionario',
  'WHERE pvd.Codigo=' + Geral.FF0(QrSoliComprCabCodigo.Value),
  '']);
  //
  Titulo := 'Informe da Solicita��o n� ' + FormatFloat('000000', QrSoliComprCabCodUsu.Value);
  MyObjects.frxMostra(frxSLC_COMPRA_001_A, Titulo);
end;

procedure TFmSoliComprCab.Incluiforneceor1Click(Sender: TObject);
var
  Fornece: Integer;
begin
  if DBCheck.CriaFm(TFmSoliComprFor, FmSoliComprFor, afmoNegarComAviso) then
  begin
    FmSoliComprFor.FCodigo := QrSoliComprCabCodigo.Value;
    FmSoliComprFor.ShowModal;
    Fornece := FmSoliComprFor.FFornece;
    FmSoliComprFor.Destroy;
    //
    ReopenSoliComprFor(0);
  end;
end;

procedure TFmSoliComprCab.Incluinovasolicitao1Click(Sender: TObject);
var
  Agora: TDateTime;
begin
  if not DModG.SoUmaEmpresaLogada(True) then Exit;
  //
  UMyMod.ConfigPanelInsUpd(stIns, Self, PainelEdita, QrSoliComprCab, [PainelDados],
    [PainelEdita], RGEntSai, ImgTipo, 'solicomprcab');
  //
  DmPediVda.ReopenParamsEmp(DmodG.QrFiliLogFilial.Value, True);
  //
{
  RGIndSinc.ItemIndex     := 1;
  RGIndSinc.Enabled       := False;
}
  EdEmpresa.ValueVariant  := DmodG.QrFiliLogFilial.Value;
  CBEmpresa.KeyValue      := DmodG.QrFiliLogFilial.Value;
  //
  DesabilitaComponentes();
  //
  TPDtaEmiss.Date           := 0;
  TPDtaEntra.Date           := 0;
  TPDtaInclu.Date           := Date;
  TPDtaPrevi.Date           := 0;
{
  EdindPres.ValueVariant    := 9;
  Edide_finNFe.ValueVariant := 0;
}
  //
  if (*Grl_Vars*)VAR_INFO_DATA_HOJE then
  begin
    Agora := DmodG.ObtemAgora();
    //
    TPDtaEmiss.Date := Agora;
    TPDtaEntra.Date := Agora;
    TPDtaPrevi.Date := Agora;
  end;
  //
  //EdCodUsu.SetFocus; 2022-04-02
end;

procedure TFmSoliComprCab.Incluinovositensdegrupo1Click(Sender: TObject);
begin
  MostraSoliComprGru(stIns);
end;

procedure TFmSoliComprCab.IncluinovositensporLeitura1Click(Sender: TObject);
begin
//
end;

procedure TFmSoliComprCab.QrSoliComprCabBeforeClose(DataSet: TDataSet);
begin
  BtItens.Enabled := False;
  BtFornece.Enabled := False;
  QrSoliComprGru.Close;
  QrSoliComprFor.Close;
  QrSumIts.Close;
end;

procedure TFmSoliComprCab.QrSoliComprCabBeforeOpen(DataSet: TDataSet);
begin
  QrSoliComprCabCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmSoliComprCab.QrForCalcFields(DataSet: TDataSet);
begin
  QrForTE1_TXT.Value :=
    Geral.FormataTelefone_TT_Curto(QrForTe1.Value);
  QrForTE1_TXT.Value :=
    Geral.FormataTelefone_TT_Curto(QrForTe1.Value);
  QrForFAX_TXT.Value :=
    Geral.FormataTelefone_TT_Curto(QrForFax.Value);
  QrForCNPJ_TXT.Value :=
    Geral.FormataCNPJ_TT(QrForCNPJ_CPF.Value);
  QrForIE_TXT.Value :=
    Geral.Formata_IE(QrForIE_RG.Value, QrForUF.Value, '??', QrForTipo.Value);
  QrForNUMERO_TXT.Value :=
    Geral.FormataNumeroDeRua(QrForRua.Value, QrForNumero.Value, False);
  //
  QrForE_ALL.Value := Uppercase(QrForNOMELOGRAD.Value);
  if Trim(QrForE_ALL.Value) <> '' then QrForE_ALL.Value :=
    QrForE_ALL.Value + ' ';
  QrForE_ALL.Value := QrForE_ALL.Value + Uppercase(QrForRua.Value);
  if Trim(QrForRua.Value) <> '' then QrForE_ALL.Value :=
    QrForE_ALL.Value + ', ' + QrForNUMERO_TXT.Value;
  if Trim(QrForCompl.Value) <>  '' then QrForE_ALL.Value :=
    QrForE_ALL.Value + ' ' + Uppercase(QrForCompl.Value);
  if Trim(QrForBairro.Value) <>  '' then QrForE_ALL.Value :=
    QrForE_ALL.Value + ' - ' + Uppercase(QrForBairro.Value);
  if QrForCEP.Value > 0 then QrForE_ALL.Value :=
    QrForE_ALL.Value + ' CEP ' +Geral.FormataCEP_NT(QrForCEP.Value);
  if Trim(QrForCidade.Value) <>  '' then QrForE_ALL.Value :=
    QrForE_ALL.Value + ' - ' + Uppercase(QrForCidade.Value);
  if Trim(QrForNOMEUF.Value) <>  '' then QrForE_ALL.Value :=
    QrForE_ALL.Value + ', ' + QrForNOMEUF.Value;
  if Trim(QrForPais.Value) <>  '' then QrForE_ALL.Value :=
    QrForE_ALL.Value + ' - ' + QrForPais.Value;
  //
  QrForCEP_TXT.Value := Geral.FormataCEP_NT(QrForCEP.Value);
  //
end;

procedure TFmSoliComprCab.QrCustomizadosAfterScroll(DataSet: TDataSet);
begin
(*
  ReopenPediVdaCuz(0);
*)
end;

procedure TFmSoliComprCab.QrCustomizadosBeforeClose(DataSet: TDataSet);
begin
(*
  QrSoliComprCabCuz.Close;
*)
end;

procedure TFmSoliComprCab.QrEntregaCliCalcFields(DataSet: TDataSet);
begin
  QrEntregaCliTE1_TXT.Value :=
    Geral.FormataTelefone_TT_Curto(QrEntregaCliTe1.Value);
(*
  QrEntregaCliFAX_TXT.Value :=
    Geral.FormataTelefone_TT_Curto(QrEntregaCliFax.Value);
*)
  {
  QrEntregaCliCNPJ_TXT.Value :=
    Geral.FormataCNPJ_TT(QrEntregaCliCNPJ_CPF.Value);
  QrEntregaCliNOME_TIPO_DOC.Value :=
    dmkPF.ObtemTipoDocTxtDeCPFCNPJ(QrEntregaCliCNPJ_CPF.Value) + ':';
  }
  QrEntregaCliNUMERO_TXT.Value :=
    Geral.FormataNumeroDeRua(QrEntregaCliRua.Value, QrEntregaCliNumero.Value, False);
  //
/////////////////////////////////////////////////////////////////////////////////
  QrEntregaCliE_ALL.Value := QrEntregaCliL_Nome.Value;
  if QrEntregaCliL_CNPJ.Value <> '' then
    QrEntregaCliE_ALL.Value := QrEntregaCliE_ALL.Value + ' CNPJ: ' +
    Geral.FormataCNPJ_TT(QrEntregaCliL_CNPJ.Value)
  else
    QrEntregaCliE_ALL.Value := QrEntregaCliE_ALL.Value + ' CPF: ' +
    Geral.FormataCNPJ_TT(QrEntregaCliL_CPF.Value);
  if Trim(QrEntregaCliL_IE.Value) <> '' then QrEntregaCliE_ALL.Value :=
    QrEntregaCliE_ALL.Value + ' I.E. ' + QrEntregaCliL_IE.Value;
  if Trim(QrEntregaCliTE1.Value) <> '' then QrEntregaCliE_ALL.Value :=
    QrEntregaCliE_ALL.Value + ' Fone: ' + QrEntregaCliTE1_TXT.Value;
  if Trim(QrEntregaCliEMAIL.Value) <> '' then QrEntregaCliE_ALL.Value :=
    QrEntregaCliE_ALL.Value + ' Email: ' + QrEntregaCliEMAIL.Value;

  QrEntregaCliE_ALL.Value := QrEntregaCliE_ALL.Value + sLineBreak +
    Uppercase(QrEntregaCliNOMELOGRAD.Value);
  if Trim(QrEntregaCliE_ALL.Value) <> '' then QrEntregaCliE_ALL.Value :=
    QrEntregaCliE_ALL.Value + ' ';
  QrEntregaCliE_ALL.Value := QrEntregaCliE_ALL.Value + Uppercase(QrEntregaCliRua.Value);
  if Trim(QrEntregaCliRua.Value) <> '' then QrEntregaCliE_ALL.Value :=
    QrEntregaCliE_ALL.Value + ', ' + QrEntregaCliNUMERO_TXT.Value;
  if Trim(QrEntregaCliCompl.Value) <>  '' then QrEntregaCliE_ALL.Value :=
    QrEntregaCliE_ALL.Value + ' ' + Uppercase(QrEntregaCliCompl.Value);
  if Trim(QrEntregaCliBairro.Value) <>  '' then QrEntregaCliE_ALL.Value :=
    QrEntregaCliE_ALL.Value + ' - ' + Uppercase(QrEntregaCliBairro.Value);
  if QrEntregaCliCEP.Value > 0 then QrEntregaCliE_ALL.Value :=
    QrEntregaCliE_ALL.Value + ' CEP ' +Geral.FormataCEP_NT(QrEntregaCliCEP.Value);
  if Trim(QrEntregaCliNO_MUNICI.Value) <>  '' then QrEntregaCliE_ALL.Value :=
    QrEntregaCliE_ALL.Value + ' - ' + Uppercase(QrEntregaCliNO_MUNICI.Value);
  if Trim(QrEntregaCliNOMEUF.Value) <>  '' then QrEntregaCliE_ALL.Value :=
    QrEntregaCliE_ALL.Value + ', ' + QrEntregaCliNOMEUF.Value;
  if Trim(QrEntregaCliNO_PAIS.Value) <>  '' then QrEntregaCliE_ALL.Value :=
    QrEntregaCliE_ALL.Value + ' - ' + QrEntregaCliNO_PAIS.Value;
  //
  //QrEntregaCliCEP_TXT.Value :=Geral.FormataCEP_NT(QrEntregaCliCEP.Value);
  //
  //if Trim(QrEntregaCliE_ALL.Value) = '' then
    //QrEntregaCliE_ALL.Value := QrCliE_ALL.Value;
end;

procedure TFmSoliComprCab.QrEntregaEntiCalcFields(DataSet: TDataSet);
begin
  QrEntregaEntiTE1_TXT.Value :=
    Geral.FormataTelefone_TT_Curto(QrEntregaEntiTe1.Value);
  QrEntregaEntiFAX_TXT.Value :=
    Geral.FormataTelefone_TT_Curto(QrEntregaEntiFax.Value);
  {
  QrEntregaEntiCNPJ_TXT.Value :=
    Geral.FormataCNPJ_TT(QrEntregaEntiCNPJ_CPF.Value);
  QrEntregaEntiNOME_TIPO_DOC.Value :=
    dmkPF.ObtemTipoDocTxtDeCPFCNPJ(QrEntregaEntiCNPJ_CPF.Value) + ':';
  }
  QrEntregaEntiNUMERO_TXT.Value :=
    Geral.FormataNumeroDeRua(QrEntregaEntiRua.Value, QrEntregaEntiNumero.Value, False);
  //
  QrEntregaEntiE_ALL.Value := QrEntregaEntiNOME_ENT.Value;
  case QrEntregaEntiTipo.Value of
    0: QrEntregaEntiE_ALL.Value := QrEntregaEntiE_ALL.Value + ' CNPJ: ' +
       Geral.FormataCNPJ_TT(QrEntregaEntiCNPJ_CPF.Value);
    1: QrEntregaEntiE_ALL.Value := QrEntregaEntiE_ALL.Value + ' CPF: ' +
       Geral.FormataCNPJ_TT(QrEntregaEntiCNPJ_CPF.Value);
  end;
  if Trim(QrEntregaEntiIE.Value) <> '' then QrEntregaEntiE_ALL.Value :=
    QrEntregaEntiE_ALL.Value + ' I.E. ' + QrEntregaEntiIE.Value;
  if Trim(QrEntregaEntiTE1.Value) <> '' then QrEntregaEntiE_ALL.Value :=
    QrEntregaEntiE_ALL.Value + ' Fone: ' + QrEntregaEntiTE1_TXT.Value;
  if Trim(QrEntregaEntiEMAIL.Value) <> '' then QrEntregaEntiE_ALL.Value :=
    QrEntregaEntiE_ALL.Value + ' Email: ' + QrEntregaEntiEMAIL.Value;

  QrEntregaEntiE_ALL.Value := QrEntregaEntiE_ALL.Value + sLineBreak +
    Uppercase(QrEntregaEntiNOMELOGRAD.Value);
  if Trim(QrEntregaEntiE_ALL.Value) <> '' then QrEntregaEntiE_ALL.Value :=
    QrEntregaEntiE_ALL.Value + ' ';
  QrEntregaEntiE_ALL.Value := QrEntregaEntiE_ALL.Value + Uppercase(QrEntregaEntiRua.Value);
  if Trim(QrEntregaEntiRua.Value) <> '' then QrEntregaEntiE_ALL.Value :=
    QrEntregaEntiE_ALL.Value + ', ' + QrEntregaEntiNUMERO_TXT.Value;
  if Trim(QrEntregaEntiCompl.Value) <>  '' then QrEntregaEntiE_ALL.Value :=
    QrEntregaEntiE_ALL.Value + ' ' + Uppercase(QrEntregaEntiCompl.Value);
  if Trim(QrEntregaEntiBairro.Value) <>  '' then QrEntregaEntiE_ALL.Value :=
    QrEntregaEntiE_ALL.Value + ' - ' + Uppercase(QrEntregaEntiBairro.Value);
  if QrEntregaEntiCEP.Value > 0 then QrEntregaEntiE_ALL.Value :=
    QrEntregaEntiE_ALL.Value + ' CEP ' +Geral.FormataCEP_NT(QrEntregaEntiCEP.Value);
  if Trim(QrEntregaEntiCidade.Value) <>  '' then QrEntregaEntiE_ALL.Value :=
    QrEntregaEntiE_ALL.Value + ' - ' + Uppercase(QrEntregaEntiCidade.Value);
  if Trim(QrEntregaEntiNOMEUF.Value) <>  '' then QrEntregaEntiE_ALL.Value :=
    QrEntregaEntiE_ALL.Value + ', ' + QrEntregaEntiNOMEUF.Value;
  if Trim(QrEntregaEntiPais.Value) <>  '' then QrEntregaEntiE_ALL.Value :=
    QrEntregaEntiE_ALL.Value + ' - ' + QrEntregaEntiPais.Value;
  //
  //QrEntregaEntiCEP_TXT.Value :=Geral.FormataCEP_NT(QrEntregaEntiCEP.Value);
  //
  //if Trim(QrEntregaEntiE_ALL.Value) = '' then
    //QrEntregaEntiE_ALL.Value := QrCliE_ALL.Value;
end;

procedure TFmSoliComprCab.QrSoliComprGruAfterOpen(DataSet: TDataSet);
begin
  StaticText1.Visible := QrSoliComprGru.RecordCount > 0;
end;

procedure TFmSoliComprCab.QrSoliComprGruAfterScroll(DataSet: TDataSet);
var
  Grade, Nivel1, Solicitacao: Integer;
begin
  Grade       := QrSoliComprGruGRATAMCAD.Value;
  Nivel1      := QrSoliComprGruNivel1.Value;
  Solicitacao := QrSoliComprCabCodigo.Value;
  //
  DmProd.ConfigGrades13(Grade, Nivel1, Solicitacao,
  GradeA, GradeX, GradeC, GradeQ, GradeF, GradeD, GradeV);
  //
end;

procedure TFmSoliComprCab.QrSoliComprGruBeforeClose(DataSet: TDataSet);
begin
  MyObjects.LimpaGrades([GradeQ, GradeF, GradeD, GradeV, GradeC, GradeA, GradeX],
    0, 0, True);
end;

procedure TFmSoliComprCab.Removefornecedor1Click(Sender: TObject);
begin
  DBCheck.QuaisItens_Exclui(Dmod.QrUpd, QrSoliComprFor, TDBGrid(DBGSoliComprFor),
    'SoliComprFor', ['Codigo', 'Fornece'], ['Codigo', 'Fornece'], istPergunta, '');
end;

procedure TFmSoliComprCab.ReopenCentroCusto();
var
  SQL_PagRec: String;
begin
(*
  case FtpPagto of
    TTipoPagto.tpDeb: SQL_PagRec := 'AND niv5.PagRec=1 ';
    TTipoPagto.tpCred: SQL_PagRec := 'AND niv5.PagRec=2 ';
    TTipoPagto.tpAny: SQL_PagRec := 'AND niv5.PagRec in (1, 2) ';
    else
      SQL_PagRec := '';
  end;
*)
  SQL_PagRec := 'AND niv5.PagRec=1 ';
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrCentroCusto, Dmod.MyDB, [
  'SELECT ',
  'CONCAT(',
  '  LPAD(niv4.Ordem, 1, "0"), ".",',
  '  LPAD(niv3.Ordem, 2, "0"), ".",',
  '  LPAD(niv2.Ordem, 3, "0"), ".",',
  '  LPAD(niv1.Ordem, 4, "0") ',
  ') REF_NIV1,',
  'niv1.Codigo, niv1.Nome,',
  'niv1.Ativo ',
  'FROM centrocust5 niv5 ',
  'LEFT JOIN centrocust4 niv4 ON niv5.Codigo=niv4.NivSup',
  'LEFT JOIN centrocust3 niv3 ON niv4.Codigo=niv3.NivSup',
  'LEFT JOIN centrocust2 niv2 ON niv3.Codigo=niv2.NivSup',
  'LEFT JOIN centrocusto niv1 ON niv2.Codigo=niv1.NivSup',
  'WHERE niv1.Ativo=1',
  SQL_PagRec,
  'ORDER BY niv1.Nome',
  '']);
end;

procedure TFmSoliComprCab.ReopenFor();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrFor, Dmod.MyDB, [
  'SELECT scf.Relevancia, en.Codigo, Tipo, en.CodUsu, IE, ',
  'CASE WHEN en.Tipo=0 THEN en.RazaoSocial ELSE en.Nome     END NOME_ENT, ',
  'CASE WHEN en.Tipo=0 THEN en.CNPJ        ELSE en.CPF      END CNPJ_CPF, ',
  'CASE WHEN en.Tipo=0 THEN en.IE          ELSE en.RG       END IE_RG, ',
  'CASE WHEN en.Tipo=0 THEN en.ERua        ELSE en.PRua     END RUA, ',
  'CASE WHEN en.Tipo=0 THEN en.ENumero + 0.000 ELSE en.PNumero + 0.000 END NUMERO,',
  'CASE WHEN en.Tipo=0 THEN en.ECompl      ELSE en.PCompl   END COMPL, ',
  'CASE WHEN en.Tipo=0 THEN en.EBairro     ELSE en.PBairro  END BAIRRO, ',
  'CASE WHEN en.Tipo=0 THEN mue.Nome       ELSE mup.Nome  END CIDADE, ',
  'CASE WHEN en.Tipo=0 THEN en.EUF + 0.000     ELSE en.PUF + 0.000  END UF, ',
  'CASE WHEN en.Tipo=0 THEN lle.Nome       ELSE llp.Nome    END NOMELOGRAD, ',
  'CASE WHEN en.Tipo=0 THEN ufe.Nome       ELSE ufp.Nome    END NOMEUF, ',
  'CASE WHEN en.Tipo=0 THEN pae.Nome       ELSE pap.NOme    END Pais, ',
  '/*CASE WHEN en.Tipo=0 THEN en.ELograd     ELSE en.PLograd END Lograd,*/',
  'CASE WHEN en.Tipo=0 THEN en.ECEP + 0.000 ELSE en.PCEP + 0.000 END CEP,',
  'CASE WHEN en.Tipo=0 THEN en.EEndeRef    ELSE en.PEndeRef END ENDEREF, ',
  'CASE WHEN en.Tipo=0 THEN en.ETe1        ELSE en.PTe1     END TE1, ',
  'CASE WHEN en.Tipo=0 THEN en.ECel        ELSE en.PCel     END CE1, ',
  'CASE WHEN en.Tipo=0 THEN en.EFax        ELSE en.PFax     END FAX, ',
  'IF(en.Tipo=0, "CNPJ", "CPF") CAD_FEDERAL, IF(en.Tipo=0, "I.E.", "RG") CAD_ESTADUAL,',
  'en.L_Ativo',
  'FROM entidades en ',
  'LEFT JOIN locbdermall.dtb_munici mue ON mue.Codigo=en.ECodMunici',
  'LEFT JOIN locbdermall.dtb_munici mup ON mup.Codigo=en.PCodMunici',
  'LEFT JOIN locbdermall.bacen_pais pae ON pae.Codigo=en.ECodiPais',
  'LEFT JOIN locbdermall.bacen_pais pap ON pap.Codigo=en.PCodiPais',
  'LEFT JOIN ufs ufe ON ufe.Codigo=en.EUF',
  'LEFT JOIN ufs ufp ON ufp.Codigo=en.PUF',
  'LEFT JOIN listalograd lle ON lle.Codigo=en.ELograd',
  'LEFT JOIN listalograd llp ON llp.Codigo=en.PLograd',
  'LEFT JOIN solicomprfor scf ON scf.Fornece=en.Codigo',
  'WHERE scf.Codigo=' + Geral.FF0(QrSoliComprCabCodigo.Value),
  'ORDER BY scf.Relevancia DESC, NOME_ENT',
  '']);
end;

procedure TFmSoliComprCab.ReopenSoliComprFor(Nivel1: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrSoliComprFor, Dmod.MyDB, [
  'SELECT scf.*, ',
  'IF(frn.Tipo=0, frn.RazaoSocial, frn.Nome) NO_FORNECE ',
  'FROM solicomprfor scf ',
  'LEFT JOIN entidades frn ON frn.Codigo=scf.Fornece ',
  'WHERE scf.Codigo=' + Geral.FF0(QrSoliComprCabCodigo.Value),
  'ORDER BY scf.Relevancia DESC, NO_FORNECE ',
  '']);
end;

procedure TFmSoliComprCab.ReopenSoliComprGru(Nivel1: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrSoliComprGru, Dmod.MyDB, [
  'SELECT pvi.Controle, SUM(QuantS) QuantS, ',
  'SUM(ValLiq) ValLiq, SUM(ValBru) ValBru, ',
  'SUM(Customizad) ItensCustomizados, ',
  'gti.Codigo GRATAMCAD, ',
  'gg1.CodUsu, gg1.Nome, gg1.Nivel1, pgt.Fracio, ',
  'IF(QuantS <= 0, 0.00, vProd / QuantS) PrecoF ',
  'FROM solicomprits pvi ',
  'LEFT JOIN gragrux   ggx ON ggx.Controle=pvi.GraGruX ',
  'LEFT JOIN gratamits gti ON gti.Controle=ggx.GraTamI ',
  'LEFT JOIN gragru1   gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN prdgruptip   pgt ON pgt.Codigo=gg1.PrdGrupTip ',
  'WHERE pvi.Codigo=' + Geral.FF0(QrSoliComprCabCodigo.Value),
  'GROUP BY gg1.Nivel1 ',
  'ORDER BY pvi.Controle ',
  '']);
  //
  if Nivel1 <> 0 then
  begin
    DmPediVda.QrLocNiv1.Close;
    DmPediVda.QrLocNiv1.Params[0].AsInteger := Nivel1;
    UnDmkDAC_PF.AbreQuery(DmPediVda.QrLocNiv1, Dmod.MyDB);
    //
    QrSoliComprGru.Locate('Nivel1', DmPediVda.QrLocNiv1GraGru1.Value, []);
  end;
end;

procedure TFmSoliComprCab.ReopenSumIts();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrSumIts, Dmod.MyDB, [
  'SELECT SUM(QuantS) QuantS, ',
  'SUM(DescoV) DescoV,',
  'SUM(ValLiq) ValLiq, SUM(ValBru) ValBru, ',
  'SUM(Customizad) ItensCustomizados,',
  '',
  'SUM(vProd) vProd, SUM(vFrete) vFrete, ',
  'SUM(vSeg) vSeg, SUM(vOutro) vOutro, ',
  'SUM(vDesc) vDesc, SUM(vBC) vBC',
  '',
  'FROM solicomprits pvi ',
  'WHERE pvi.Codigo=' + Geral.FF0(QrSoliComprCabCodigo.Value),
  '']);
end;

procedure TFmSoliComprCab.RGEntSaiClick(Sender: TObject);
begin
  {
  GBDadosTop.Visible := RGEntSai.ItemIndex >= 0;
  PCDadosTop.Visible := RGEntSai.ItemIndex >= 0;}
  DmPediVda.ReopenFisRegCad(RGEntSai.ItemIndex);
  GBDadosTop.Visible := RGEntSai.ItemIndex = 0;
  //
  DmPediVda.ReopenClientes(nil, RGEntSai.ItemIndex);
  DmPediVda.ReopenFisRegCad(RGEntSai.ItemIndex);
  //
  LaDtaEntra.Enabled  := RGEntSai.ItemIndex = 1;
  TPDtaEntra.Enabled  := RGEntSai.ItemIndex = 1;
  LaPedidoCli.Enabled := RGEntSai.ItemIndex = 1;
  EdPedidoCli.Enabled := RGEntSai.ItemIndex = 1;
  //
  //if RGEntSai.ItemIndex >= 0 then}
  if RGEntSai.ItemIndex = 0 then
  begin
    GBDadosTop.Visible := RGEntSai.ItemIndex >= 0;
    PCDadosTop.Visible := RGEntSai.ItemIndex >= 0;
    DmPediVda.ReopenClientes(nil, RGEntSai.ItemIndex);
    DmPediVda.ReopenFisRegCad(RGEntSai.ItemIndex);
    //
    try
      if (ImgTipo.SQLType = stIns) and EdEmpresa.Visible then
        EdEmpresa.SetFocus;
    except
      //
    end;
  end else
  if RGEntSai.ItemIndex = 1 then
  begin
    Geral.MB_Info('Solicita��o de venda n�o implementada! SOLICITE � DERMATEK!');
    RGEntSai.ItemIndex := -1;
  end;
end;

end.

