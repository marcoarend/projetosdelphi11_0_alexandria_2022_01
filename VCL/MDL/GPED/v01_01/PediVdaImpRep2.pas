unit PediVdaImpRep2;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DB, DBGrids, (*&&UnDmkABS_PF,*)
  dmkDBGrid, DBCtrls, mySQLDbTables, Menus, ComCtrls, UnDmkProcFunc,
  DmkDAC_PF;

type
  THackDBGrid = class(TDBGrid);
  TFmPediVdaImpRep2 = class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    BtNenhum: TBitBtn;
    BtTodos: TBitBtn;
    BtPesq: TBitBtn;
    PMPesq: TPopupMenu;
    Ativa1: TMenuItem;
    Desativa1: TMenuItem;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    Panel3: TPanel;
    Label1: TLabel;
    EdPesq: TEdit;
    DBGrid1: TdmkDBGrid;
    Panel4: TPanel;
    DBGrid2: TdmkDBGrid;
    RGSelecao: TRadioGroup;
    procedure FormCreate(Sender: TObject);
    procedure DBGrid1CellClick(Column: TColumn);
    procedure FormStartDock(Sender: TObject; var DragObject: TDragDockObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure EdPesqChange(Sender: TObject);
    procedure FormUnDock(Sender: TObject; Client: TControl;
      NewTarget: TWinControl; var Allow: Boolean);
    procedure BtNenhumClick(Sender: TObject);
    procedure BtTodosClick(Sender: TObject);
    procedure BtPesqClick(Sender: TObject);
    procedure Ativa1Click(Sender: TObject);
    procedure Desativa1Click(Sender: TObject);
    procedure PageControl1Change(Sender: TObject);
    procedure FormDeactivate(Sender: TObject);
    procedure RGSelecaoClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    procedure Pesquisar();
    procedure AtivaItens(Status: Integer);
    procedure AtivaSelecionados(Status: Integer);
    procedure HabilitaItemUnico();
  public
    { Public declarations }
    class function CreateDockForm(const aColor: TColor): TCustomForm;
    //class procedure FiltaAtivos();
  end;

var
  FmPediVdaImpRep2: TFmPediVdaImpRep2;

implementation

uses UnMyObjects, Module, UMySQLModule, TabePrcCab, PediVdaImp2, ModPediVda, MyGlyfs;

{$R *.DFM}

///// PROCEDURES E FUNCTIONS DO DOCKFORM

procedure TFmPediVdaImpRep2.Ativa1Click(Sender: TObject);
begin
  AtivaSelecionados(1);
end;

procedure TFmPediVdaImpRep2.AtivaItens(Status: Integer);
begin
(*&&
  Screen.Cursor := crHourGlass;
  DmPediVda.QrPVIRepIts.Close;
  DmPediVda.QrPVIRepIts.SQL.Clear;
  DmPediVda.QrPVIRepIts.SQL.Add('UPDATE pvirep SET Ativo=' + dmkPF.FFP(Status, 0) + ';');
  DmPediVda.QrPVIRepIts.SQL.Add('SELECT * FROM pvirep;');
  DmkABS_PF.AbreQuery(DmPediVda.QrPVIRepIts);
  HabilitaItemUnico();
  Screen.Cursor := crDefault;
*)
end;

procedure TFmPediVdaImpRep2.AtivaSelecionados(Status: Integer);
begin
(*&&
  Screen.Cursor := crHourGlass;
  DmPediVda.QrPVIRepIts.First;
  while not DmPediVda.QrPVIRepIts.Eof do
  begin
    if DmPediVda.QrPVIRepItsAtivo.Value <> Status then
    begin
      DmPediVda.QrPVIRepIts.Edit;
      DmPediVda.QrPVIRepItsAtivo.Value := Status;
      DmPediVda.QrPVIRepIts.Post;
    end;
    DmPediVda.QrPVIRepIts.Next;
  end;
  HabilitaItemUnico();
  Screen.Cursor := crDefault;
*)
end;

procedure TFmPediVdaImpRep2.BtPesqClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMPesq, BtPesq);
end;

procedure TFmPediVdaImpRep2.BtNenhumClick(Sender: TObject);
begin
  AtivaItens(0);
end;

procedure TFmPediVdaImpRep2.BtTodosClick(Sender: TObject);
begin
  AtivaItens(1);
end;

class function TFmPediVdaImpRep2.CreateDockForm(const aColor: TColor): TCustomForm;
begin
  result := TFmPediVdaImpRep2.Create(Application);
  //result.Color := aColor;
  //result.Caption := ColorToString(aColor);
  FmMyGlyfs.ConfiguraFormDock(result);
  result.Show;
end;

procedure TFmPediVdaImpRep2.FormClose(Sender: TObject; var Action: TCloseAction);
begin
(*&&
  DmPediVda.QrPVIRepIts.Close;
  DmPediVda.QrPVIRepIts.SQL.Clear;
  DmPediVda.QrPVIRepIts.SQL.Add('DROP TABLE PVIRep; ');
  DmPediVda.QrPVIRepIts.ExecSQL;
  //
  ManualFloat(Rect(0, 0, 0, 0));
  Action := caFree;
*)
end;

procedure TFmPediVdaImpRep2.FormShow(Sender: TObject);
begin
  FmPediVdaImp2.BtConfirma.Enabled := False;
end;

procedure TFmPediVdaImpRep2.FormStartDock(Sender: TObject;
  var DragObject: TDragDockObject);
begin
  DragObject := TDragDockObjectEx.Create(Self);
  DragObject.Brush.Color := clAqua; // this will display a red outline
end;


procedure TFmPediVdaImpRep2.FormUnDock(Sender: TObject; Client: TControl;
  NewTarget: TWinControl; var Allow: Boolean);
begin
end;

///// FIM DAS PROCEDURES E FUNCTIONS DO DOCKFORM

procedure TFmPediVdaImpRep2.PageControl1Change(Sender: TObject);
begin
  BtPesq.Enabled := PageControl1.ActivePageIndex = 0;
  case PageControl1.ActivePageIndex of
    0: Pesquisar();
    1: RGSelecaoClick(Self);
  end;
end;

procedure TFmPediVdaImpRep2.Pesquisar();
//var
  //Lin: String;
begin
(*&&
  Screen.Cursor := crHourGlass;
  //
  DmPediVda.QrPVIRepIts.Filter   := 'Nome LIKE "%' + EdPesq.Text + '%"';
  DmPediVda.QrPVIRepIts.Filtered := True;
  //
  Screen.Cursor := crDefault;
*)
end;

procedure TFmPediVdaImpRep2.RGSelecaoClick(Sender: TObject);
begin
(*&&
  DmPediVda.QrPVIRepIts.Filtered := False;
  case RGSelecao.ItemIndex of
    0: DmPediVda.QrPVIRepIts.Filter := 'Ativo=0';
    1: DmPediVda.QrPVIRepIts.Filter := 'Ativo=1';
    2: DmPediVda.QrPVIRepIts.Filter := '';
  end;
  if RGSelecao.ItemIndex < 2 then
    DmPediVda.QrPVIRepIts.Filtered := True;
*)
end;

procedure TFmPediVdaImpRep2.DBGrid1CellClick(Column: TColumn);
var
  Status: Integer;
begin
(*&&
  if Column.FieldName = 'Ativo' then
  begin
    if DmPediVda.QrPVIRepIts.FieldByName('Ativo').Value = 1 then
      Status := 0
    else
      Status := 1;
    //
    DmPediVda.QrPVIRepIts.Edit;
    DmPediVda.QrPVIRepIts.FieldByName('Ativo').Value := Status;
    DmPediVda.QrPVIRepIts.Post;
  end;
  HabilitaItemUnico();
*)
end;

procedure TFmPediVdaImpRep2.HabilitaItemUnico();
var
  Habilita: Boolean;
begin
(*&&
  DmPediVda.QrPVIRepAti.Close;
  DmkABS_PF.AbreQuery(DmPediVda.QrPVIRepAti);
  Habilita :=  DmPediVda.QrPVIRepAtiItens.Value = 0;
  FmPediVdaImp2.LaPVIRep.Enabled := Habilita;
  FmPediVdaImp2.EdPVIRep.Enabled := Habilita;
  FmPediVdaImp2.CBPVIRep.Enabled := Habilita;
  if not Habilita then
  begin
    FmPediVdaImp2.EdPVIRep.ValueVariant := 0;
    FmPediVdaImp2.CBPVIRep.KeyValue     := 0;
  end;
*)
end;


procedure TFmPediVdaImpRep2.Desativa1Click(Sender: TObject);
begin
  AtivaSelecionados(0);
end;

procedure TFmPediVdaImpRep2.EdPesqChange(Sender: TObject);
begin
  Pesquisar();
end;

procedure TFmPediVdaImpRep2.FormCreate(Sender: TObject);
const
  Txt1 = 'INSERT INTO pvirep (Codigo,CodUsu,Nome,Ativo) VALUES(';
var
  Txt2: String;
  //i: Integer;
begin
(*&&
  DmPediVda.QrPVIRepIts.Close;
  DmPediVda.QrPVIRepIts.SQL.Clear;
  DmPediVda.QrPVIRepIts.SQL.Add('DROP TABLE PVIRep; ');
  DmPediVda.QrPVIRepIts.SQL.Add('CREATE TABLE PVIRep (');
  DmPediVda.QrPVIRepIts.SQL.Add('  Codigo  integer      ,');
  DmPediVda.QrPVIRepIts.SQL.Add('  CodUsu  integer      ,');
  DmPediVda.QrPVIRepIts.SQL.Add('  Nome    varchar(50)  ,');
  DmPediVda.QrPVIRepIts.SQL.Add('  Ativo   smallint      ');
  DmPediVda.QrPVIRepIts.SQL.Add(');');
  //
  DmPediVda.QrPVIRepCad.Close;
  UnDmkDAC_PF.AbreQuery(DmPediVda.QrPVIRepCad, Dmod.MyDB);
  while not DmPediVda.QrPVIRepCad.Eof do
  begin
    Txt2 :=
      dmkPF.FFP(DmPediVda.QrPVIRepCadCodigo.Value, 0) + ',' +
      dmkPF.FFP(DmPediVda.QrPVIRepCadCodUsu.Value, 0) + ',' +
      '"' + DmPediVda.QrPVIRepCadNOMEENT.Value + '",' +
      '0);';
  //for i  := 1 to 10 do
    DmPediVda.QrPVIRepIts.SQL.Add(Txt1 + Txt2);
    DmPediVda.QrPVIRepCad.Next;
  end;
  //
  DmPediVda.QrPVIRepIts.SQL.Add('SELECT * FROM pvirep;');
  DmkABS_PF.AbreQuery(DmPediVda.QrPVIRepIts);
  PageControl1.ActivePageIndex := 0;
*)
end;

procedure TFmPediVdaImpRep2.FormDeactivate(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 1;
  RGSelecao.ItemIndex := 1;
  //
  if FindWindow('TFmPediVdaImp', nil) > 0 then
    FmPediVdaImp2.AtivaBtConfirma();
end;

end.

