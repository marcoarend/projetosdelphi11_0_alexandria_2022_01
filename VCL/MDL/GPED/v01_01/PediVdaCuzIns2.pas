unit PediVdaCuzIns2;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB,
  DB, mySQLDbTables, Grids, Mask, dmkLabel, dmkImage, UnDmkEnums, DmkDAC_PF;

type
  TFmPediVdaCuzIns2 = class(TForm)
    Panel1: TPanel;
    Panel3: TPanel;
    QrGraGru1: TmySQLQuery;
    QrGraGru1Codusu: TIntegerField;
    QrGraGru1Nivel3: TIntegerField;
    QrGraGru1Nivel2: TIntegerField;
    QrGraGru1Nivel1: TIntegerField;
    QrGraGru1Nome: TWideStringField;
    QrGraGru1PrdGrupTip: TIntegerField;
    QrGraGru1GraTamCad: TIntegerField;
    QrGraGru1NOMEGRATAMCAD: TWideStringField;
    QrGraGru1CODUSUGRATAMCAD: TIntegerField;
    QrGraGru1CST_A: TSmallintField;
    QrGraGru1CST_B: TSmallintField;
    QrGraGru1UnidMed: TIntegerField;
    QrGraGru1NCM: TWideStringField;
    QrGraGru1Peso: TFloatField;
    QrGraGru1SIGLAUNIDMED: TWideStringField;
    QrGraGru1CODUSUUNIDMED: TIntegerField;
    QrGraGru1NOMEUNIDMED: TWideStringField;
    QrGraGru1NOME_EX: TWideStringField;
    DsGraGru1: TDataSource;
    Label1: TLabel;
    EdGraGru1: TdmkEditCB;
    CBGraGru1: TdmkDBLookupComboBox;
    Panel4: TPanel;
    GradeC: TStringGrid;
    GradeA: TStringGrid;
    GradeX: TStringGrid;
    Panel5: TPanel;
    GroupBox2: TGroupBox;
    Label3: TLabel;
    Label4: TLabel;
    EdGraGruX: TdmkEdit;
    EdQuantP: TdmkEdit;
    GroupBox1: TGroupBox;
    EdMedidaC: TdmkEdit;
    EdMedidaL: TdmkEdit;
    EdMedidaA: TdmkEdit;
    QrGraGru1PerCuztMin: TFloatField;
    QrGraGru1PerCuztMax: TFloatField;
    EdMedidaE: TdmkEdit;
    DBText1: TDBText;
    DBText2: TDBText;
    DBText3: TDBText;
    DBText4: TDBText;
    QrGraGru1Medida1: TWideStringField;
    QrGraGru1Medida2: TWideStringField;
    QrGraGru1Medida3: TWideStringField;
    QrGraGru1Medida4: TWideStringField;
    SpeedButton1: TSpeedButton;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel6: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel7: TPanel;
    PnSaiDesis: TPanel;
    BtOK: TBitBtn;
    BtSaida: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure EdGraGru1Change(Sender: TObject);
    procedure GradeCClick(Sender: TObject);
    procedure GradeCDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect;
      State: TGridDrawState);
    procedure QrGraGru1BeforeClose(DataSet: TDataSet);
    procedure EdGraGruXChange(Sender: TObject);
    procedure EdQuantPChange(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure EdGraGru1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure GradeCDblClick(Sender: TObject);
    procedure QrGraGru1AfterScroll(DataSet: TDataSet);
  private
    { Private declarations }
    procedure HabilitaOK();
  public
    { Public declarations }
  end;

  var
  FmPediVdaCuzIns2: TFmPediVdaCuzIns2;

implementation

uses ModProd, UnMyObjects, UMySQLModule, Module, dmkGeral, ModPediVda, PediVda2,
UnMySQLCuringa;

{$R *.DFM}

procedure TFmPediVdaCuzIns2.BtOKClick(Sender: TObject);
const
  Customizad = 1;
var
  Codigo, GraGruX, Controle, GraGru1, CodUsu, PrdGrupTip: Integer;
  QuantP, MedidaC, MedidaL, MedidaA, MedidaE: Double;
begin
  if not UMyMod.ObtemCodigoDeCodUsu(EdGraGru1, GraGru1,
    'Informe o produto!', 'Nivel1') then Exit;
  CodUsu     := EdGraGru1.ValueVariant;
  PrdGrupTip := QrGraGru1PrdGrupTip.Value;
  Codigo     := FmPediVda2.QrPediVdaCodigo.Value;
  GraGruX    := EdGraGruX.ValueVariant;
  QuantP     := EdQuantP.ValueVariant;
  MedidaC    := EdMedidaC.ValueVariant;
  MedidaL    := EdMedidal.ValueVariant;
  MedidaA    := EdMedidaA.ValueVariant;
  MedidaE    := EdMedidaE.ValueVariant;
  //
  Controle := UMyMod.BuscaEmLivreY_Def('pedivdaits', 'Controle', stIns, 0);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'pedivdaits', False, [
    'Codigo', 'GraGruX', 'QuantP',
    'MedidaC', 'MedidaL', 'MedidaA',
    'MedidaE', 'Customizad'
  ], ['Controle'], [
    Codigo, GraGruX, QuantP,
    MedidaC, MedidaL, MedidaA,
    MedidaE, Customizad
  ], [Controle], True) then
  begin
    DmPediVda.AtzSdosPedido(Codigo);
      FmPediVda2.MostraFmPediVdaCuzParIns(CodUsu, PrdGrupTip, GraGru1, GraGruX,
        Controle, stIns, QuantP, MedidaC, MedidaL, MedidaA, MedidaE);
    Close;
  end;
end;

procedure TFmPediVdaCuzIns2.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmPediVdaCuzIns2.EdGraGruXChange(Sender: TObject);
begin
  HabilitaOK();
end;

procedure TFmPediVdaCuzIns2.EdGraGru1Change(Sender: TObject);
begin
  if EdGraGru1.ValueVariant > 0 then
    DmProd.ConfigGrades0(QrGraGru1GraTamCad.Value, QrGraGru1Nivel1.Value,
    GradeA, GradeX, GradeC)
  else MyObjects.LimpaGrades([GradeA, GradeC, GradeX], 0, 0, True);
  EdGraGruX.ValueVariant := 0;
  HabilitaOK();
end;

procedure TFmPediVdaCuzIns2.EdGraGru1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_ESCAPE then Close;
end;

procedure TFmPediVdaCuzIns2.EdQuantPChange(Sender: TObject);
begin
  HabilitaOK();
end;

procedure TFmPediVdaCuzIns2.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmPediVdaCuzIns2.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  UnDmkDAC_PF.AbreQuery(QrGraGru1, Dmod.MyDB);
end;

procedure TFmPediVdaCuzIns2.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmPediVdaCuzIns2.GradeCClick(Sender: TObject);
var
  Ativo: Integer;
  Reduzido: String;
begin
  if (GradeC.Col > 0) and (GradeC.Row > 0) then
  begin
    Ativo := Geral.IMV(GradeA.Cells[GradeC.Col, GradeC.Row]);
    Reduzido := GradeC.Cells[GradeC.Col, GradeC.Row];
    if Ativo < 1 then
    begin
      Geral.MensagemBox('O reduzido (a cor ou o tamanho) ' + Reduzido +
        ' n�o est� ativo!', 'Aviso', MB_OK+MB_ICONERROR);
      EdGraGruX.Text := '0';
      Exit;
    end;
    EdGraGruX.Text := Reduzido;
    EdQuantP.SetFocus;
  end;
end;

procedure TFmPediVdaCuzIns2.GradeCDblClick(Sender: TObject);
begin
  EdQuantP.SetFocus;
end;

procedure TFmPediVdaCuzIns2.GradeCDrawCell(Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);
begin
  MyObjects.DesenhaGradeN(GradeC, GradeA, nil, ACol, ARow, Rect, State,
  '0', 0, 0, False);
end;

procedure TFmPediVdaCuzIns2.HabilitaOK;
var
  Produto, Reduzido, Quantidade: Integer;
begin
  Produto    := Geral.IMV(EdGraGru1.Text);
  Reduzido   := Geral.IMV(EdGraGruX.Text);
  Quantidade := Geral.IMV(EdQuantP.Text);
  BtOK.Enabled := (Produto > 0) and (Reduzido > 0) and (Quantidade > 0);
end;

procedure TFmPediVdaCuzIns2.QrGraGru1AfterScroll(DataSet: TDataSet);
begin
  EdMedidaL.Visible := QrGraGru1Medida2.Value <> '';
  EdMedidaA.Visible := QrGraGru1Medida3.Value <> '';
  EdMedidaE.Visible := QrGraGru1Medida4.Value <> '';
end;

procedure TFmPediVdaCuzIns2.QrGraGru1BeforeClose(DataSet: TDataSet);
begin
  MyObjects.LimpaGrades([GradeA, GradeC, GradeX], 0, 0, True);
end;

procedure TFmPediVdaCuzIns2.SpeedButton1Click(Sender: TObject);
var
  GraGru1: Integer;
begin
  GraGru1 := CuringaLoc.CriaForm('Nivel1', 'Nome', 'GraGru1', Dmod.MyDB, '', True);
  if GraGru1 <> 0 then
  begin
    EdGraGru1.ValueVariant := GraGru1;
    CBGraGru1.KeyValue     := GraGru1;
  end;
end;

end.
