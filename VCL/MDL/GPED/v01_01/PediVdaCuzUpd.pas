unit PediVdaCuzUpd;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DBCtrls, dmkEdit, DB, mySQLDbTables, Mask,
  dmkImage, UnDmkEnums, DmkDAC_PF;

type
  TFmPediVdaCuzUpd = class(TForm)
    Panel1: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    EdMedidaC: TdmkEdit;
    EdMedidaL: TdmkEdit;
    EdMedidaA: TdmkEdit;
    EdMedidaE: TdmkEdit;
    GroupBox2: TGroupBox;
    Label3: TLabel;
    Label4: TLabel;
    EdQuantP: TdmkEdit;
    QrPediVdaIts: TmySQLQuery;
    DsPediVdaIts: TDataSource;
    QrPediVdaItsControle: TIntegerField;
    QrPediVdaItsGraGruX: TIntegerField;
    QrPediVdaItsMedidaC: TFloatField;
    QrPediVdaItsMedidaL: TFloatField;
    QrPediVdaItsMedidaA: TFloatField;
    QrPediVdaItsMedidaE: TFloatField;
    QrPediVdaItsGRATAMCAD: TIntegerField;
    QrPediVdaItsGRATAMITS: TAutoIncField;
    QrPediVdaItsNO_TAM: TWideStringField;
    QrPediVdaItsNO_COR: TWideStringField;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    Label2: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    QrPediVdaItsQuantP: TFloatField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel5: TPanel;
    PnSaiDesis: TPanel;
    BtOK: TBitBtn;
    BtSaida: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure QrPediVdaItsAfterOpen(DataSet: TDataSet);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure ReopenPediVdaIts(Controle: Integer);
  end;

  var
  FmPediVdaCuzUpd: TFmPediVdaCuzUpd;

implementation

uses UnMyObjects, UMySQLModule, dmkGeral, Module, PediVda;

{$R *.DFM}

procedure TFmPediVdaCuzUpd.BtOKClick(Sender: TObject);
var
  Controle: Integer;
  QuantP, MedidaC, MedidaL, MedidaA, MedidaE: Double;
begin
  Controle := QrPediVdaItsControle.Value;
  QuantP   := EdQuantP .ValueVariant;
  MedidaC  := EdMedidaC.ValueVariant;
  MedidaL  := EdMedidaL.ValueVariant;
  MedidaA  := EdMedidaA.ValueVariant;
  MedidaE  := EdMedidaE.ValueVariant;
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'pedivdaits', False, [
    'QuantP', 'MedidaC', 'MedidaL',
    'MedidaA', 'MedidaE'
  ], ['Controle'], [
    QuantP, MedidaC, MedidaL,
    MedidaA, MedidaE
  ], [Controle], True) then
  begin
    FmPediVda.AtualizaItemCustomizado(Controle);
    Close;
  end;
end;

procedure TFmPediVdaCuzUpd.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmPediVdaCuzUpd.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmPediVdaCuzUpd.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
end;

procedure TFmPediVdaCuzUpd.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmPediVdaCuzUpd.QrPediVdaItsAfterOpen(DataSet: TDataSet);
begin
  EdQuantP.ValueVariant := QrPediVdaItsQuantP.Value;
  EdMedidaC.ValueVariant := QrPediVdaItsMedidaC.Value;
  EdMedidaL.ValueVariant := QrPediVdaItsMedidaL.Value;
  EdMedidaA.ValueVariant := QrPediVdaItsMedidaA.Value;
  EdMedidaE.ValueVariant := QrPediVdaItsMedidaE.Value;
end;

procedure TFmPediVdaCuzUpd.ReopenPediVdaIts(Controle: Integer);
begin
  QrPediVdaIts.Close;
  QrPediVdaIts.Params[0].AsInteger := Controle;
  UnDmkDAC_PF.AbreQuery(QrPediVdaIts, Dmod.MyDB);
  //
end;

end.
