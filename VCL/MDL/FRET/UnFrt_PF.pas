unit UnFrt_PF;

interface

uses
  StdCtrls, ExtCtrls, Windows, Messages, SysUtils, Classes, Graphics, Controls,
  Forms, Dialogs, Menus, UnInternalConsts, UnInternalConsts2, Math, UnMsgInt,
  Db, DbCtrls,   Mask, Buttons, ZCF2, (*DBTables,*) mySQLDbTables, ComCtrls,
  (*DBIProcs,*) Registry, Grids, DBGrids, WinSkinStore, WinSkinData, CheckLst,
  printers, CommCtrl, TypInfo, comobj, ShlObj, RichEdit, ShellAPI, Consts,
  ActiveX, OleCtrls, SHDocVw, AdvToolBar, AdvGlowButton, UnDmkProcFunc,
  Variants, MaskUtils, RTLConsts, IniFiles, frxClass, frxPreview,
  mySQLExceptions,
  // Dermatek
  dmkGeral, dmkEdit, dmkDBEdit, dmkEditF7, dmkEditDateTimePicker, dmkCheckGroup,
  dmkDBLookupCombobox, dmkEditCB, dmkDBGrid, dmkDBGridDAC, dmkImage, UnDmkEnums;

type

  TUnFrt_PF = class(TObject)
  private
    { Private declarations }
  public
    { Public declarations }
    function  DesfazEncerramentoFatura(FatID, FatNum, Empresa: Integer): Boolean;
    function  DesfazEncerramentoManifesto(FatID, FatNum, Empresa: Integer): Boolean;
    //
    procedure MostraFormFrtCaracAd();
    procedure MostraFormFrtCaracSer();
    procedure MostraFormFrtFatCab();
    procedure MostraFormFrtMnfCab();
    procedure MostraFormFrtProPred();
    procedure MostraFormFrtPrcCad(Codigo: Integer);
    procedure MostraFormFrtRegFatC(Codigo: Integer);
    procedure MostraFormFrtRegFisC(Codigo: Integer);
    procedure MostraFormFrtSrvCad(Codigo: Integer);
    procedure MostraFormProdutCad(Codigo: Integer);
    procedure MostraFormFrtFatAutXml(Codigo, Tomador: Integer;
              InserePadroes: Boolean);
    //
    procedure CadastroESelecaoDeFrtPrcCad(QrFrtPrcCad: TmySQLQuery; EdFrtPrcCad:
              TdmkEditCB; CBFrtPrcCad: TdmkDBLookupCombobox);
    procedure CadastroESelecaoDeFrtRegFatC(QrFrtRegFatC: TmySQLQuery; EdFrtRegFatC:
              TdmkEditCB; CBFrtRegFatC: TdmkDBLookupCombobox);
    procedure CadastroESelecaoDeFrtRegFisC(QrFrtRegFisC: TmySQLQuery; EdFrtRegFisC:
              TdmkEditCB; CBFrtRegFisC: TdmkDBLookupCombobox);
    procedure CadastroESelecaoDeFrtCaracAd(QrFrtCaracAd: TmySQLQuery; EdFrtCaracAd:
              TdmkEditCB; CBFrtCaracAd: TdmkDBLookupCombobox);
    procedure CadastroESelecaoDeFrtCaracSer(QrFrtCaracSer: TmySQLQuery; EdFrtCaracSer:
              TdmkEditCB; CBFrtCaracSer: TdmkDBLookupCombobox);
    procedure CadastroESelecaoDeFrtProPred(QrFrtProPred: TmySQLQuery; EdFrtProPred:
              TdmkEditCB; CBFrtProPred: TdmkDBLookupCombobox);
    procedure CadastroESelecaoDeProdutCad(QrFrtProdutCad: TmySQLQuery; EdFrtProdutCad:
              TdmkEditCB; CBFrtProdutCad: TdmkDBLookupCombobox);
    procedure CadastroESelecaoDeFrtSrvCad(QrFrtSrvCad: TmySQLQuery; EdFrtSrvCad:
              TdmkEditCB; CBFrtSrvCad: TdmkDBLookupCombobox);

  end;

var
  Frt_PF: TUnFrt_PF;

implementation

uses MyDBCheck, Module, DmkDAC_PF, UMySQLModule, CfgCadLista, ModuleGeral,
  UnFinanceiro,
  FrtRegFisC, FrtRegFatC, ProdutCad, FrtFatAutXml, FrtSrvCad, FrtFatCab,
  FrtMnfCab;

{ TUnFrt_PF }

procedure TUnFrt_PF.CadastroESelecaoDeFrtCaracAd(QrFrtCaracAd: TmySQLQuery;
  EdFrtCaracAd: TdmkEditCB; CBFrtCaracAd: TdmkDBLookupCombobox);
begin
  VAR_CADASTRO := 0;
  MostraFormFrtCaracAd();
  if VAR_CADASTRO <> 0 then
  begin
    if QrFrtCaracAd <> nil then
      UnDmkDAC_PF.AbreQuery(QrFrtCaracAd, QrFrtCaracAd.Database);
    //
    EdFrtCaracAd.ValueVariant := VAR_CADASTRO;
    CBFrtCaracAd.KeyValue     := VAR_CADASTRO;
    EdFrtCaracAd.SetFocus;
  end;
end;

procedure TUnFrt_PF.CadastroESelecaoDeFrtCaracSer(QrFrtCaracSer: TmySQLQuery;
  EdFrtCaracSer: TdmkEditCB; CBFrtCaracSer: TdmkDBLookupCombobox);
begin
  VAR_CADASTRO := 0;
  MostraFormFrtCaracSer();
  if VAR_CADASTRO <> 0 then
  begin
    if QrFrtCaracSer <> nil then
      UnDmkDAC_PF.AbreQuery(QrFrtCaracSer, QrFrtCaracSer.Database);
    //
    EdFrtCaracSer.ValueVariant := VAR_CADASTRO;
    CBFrtCaracSer.KeyValue     := VAR_CADASTRO;
    EdFrtCaracSer.SetFocus;
  end;
end;

procedure TUnFrt_PF.CadastroESelecaoDeFrtPrcCad(QrFrtPrcCad: TmySQLQuery;
  EdFrtPrcCad: TdmkEditCB; CBFrtPrcCad: TdmkDBLookupCombobox);
var
  FrtPrcCad: Integer;
begin
  VAR_CADASTRO := 0;
  FrtPrcCad := EdFrtPrcCad.ValueVariant;
  //
(*
  if DBCheck.CriaFm(TFmFrtPrcCad, FmFrtPrcCad, afmoNegarComAviso) then
  begin
    if FrtPrcCad <> 0 then
      FmFrtPrcCad.LocCod(FrtPrcCad, FrtPrcCad);
    FmFrtPrcCad.ShowModal;
    FmFrtPrcCad.Destroy;
    if VAR_CADASTRO <> 0 then
    begin
      if QrFrtPrcCad <> nil then
        UnDmkDAC_PF.AbreQuery(QrFrtPrcCad, QrFrtPrcCad.Database);
      //
      EdFrtPrcCad.ValueVariant := VAR_CADASTRO;
      CBFrtPrcCad.KeyValue     := VAR_CADASTRO;
      EdFrtPrcCad.SetFocus;
    end;
  end;
*)
end;

procedure TUnFrt_PF.CadastroESelecaoDeProdutCad(QrFrtProdutCad: TmySQLQuery;
  EdFrtProdutCad: TdmkEditCB; CBFrtProdutCad: TdmkDBLookupCombobox);
begin
  VAR_CADASTRO := 0;
  MostraFormProdutCad(EdFrtProdutCad.ValueVariant);
  if VAR_CADASTRO <> 0 then
  begin
    if QrFrtProdutCad <> nil then
      UnDmkDAC_PF.AbreQuery(QrFrtProdutCad, QrFrtProdutCad.Database);
    //
    EdFrtProdutCad.ValueVariant := VAR_CADASTRO;
    CBFrtProdutCad.KeyValue     := VAR_CADASTRO;
    EdFrtProdutCad.SetFocus;
  end;
end;

procedure TUnFrt_PF.CadastroESelecaoDeFrtProPred(QrFrtProPred: TmySQLQuery;
  EdFrtProPred: TdmkEditCB; CBFrtProPred: TdmkDBLookupCombobox);
begin
  VAR_CADASTRO := 0;
  MostraFormFrtProPred();
  if VAR_CADASTRO <> 0 then
  begin
    if QrFrtProPred <> nil then
      UnDmkDAC_PF.AbreQuery(QrFrtProPred, QrFrtProPred.Database);
    //
    EdFrtProPred.ValueVariant := VAR_CADASTRO;
    CBFrtProPred.KeyValue     := VAR_CADASTRO;
    EdFrtProPred.SetFocus;
  end;
end;

procedure TUnFrt_PF.CadastroESelecaoDeFrtRegFatC(QrFrtRegFatC: TmySQLQuery;
  EdFrtRegFatC: TdmkEditCB; CBFrtRegFatC: TdmkDBLookupCombobox);
var
  FrtRegFatC: Integer;
begin
  VAR_CADASTRO := 0;
  FrtRegFatC := EdFrtRegFatC.ValueVariant;
  //
  if DBCheck.CriaFm(TFmFrtRegFatC, FmFrtRegFatC, afmoNegarComAviso) then
  begin
    if FrtRegFatC <> 0 then
      FmFrtRegFatC.LocCod(FrtRegFatC, FrtRegFatC);
    FmFrtRegFatC.ShowModal;
    FmFrtRegFatC.Destroy;
    if VAR_CADASTRO <> 0 then
    begin
      if QrFrtRegFatC <> nil then
        UnDmkDAC_PF.AbreQuery(QrFrtRegFatC, QrFrtRegFatC.Database);
      //
      EdFrtRegFatC.ValueVariant := VAR_CADASTRO;
      CBFrtRegFatC.KeyValue     := VAR_CADASTRO;
      EdFrtRegFatC.SetFocus;
    end;
  end;
end;

procedure TUnFrt_PF.CadastroESelecaoDeFrtRegFisC(QrFrtRegFisC: TmySQLQuery;
  EdFrtRegFisC: TdmkEditCB; CBFrtRegFisC: TdmkDBLookupCombobox);
var
  FrtRegFisC: Integer;
begin
  VAR_CADASTRO := 0;
  FrtRegFisC := EdFrtRegFisC.ValueVariant;
  //
  if DBCheck.CriaFm(TFmFrtRegFisC, FmFrtRegFisC, afmoNegarComAviso) then
  begin
    if FrtRegFisC <> 0 then
      FmFrtRegFisC.LocCod(FrtRegFisC, FrtRegFisC);
    FmFrtRegFisC.ShowModal;
    FmFrtRegFisC.Destroy;
    if VAR_CADASTRO <> 0 then
    begin
      if QrFrtRegFisC <> nil then
        UnDmkDAC_PF.AbreQuery(QrFrtRegFisC, QrFrtRegFisC.Database);
      //
      EdFrtRegFisC.ValueVariant := VAR_CADASTRO;
      CBFrtRegFisC.KeyValue     := VAR_CADASTRO;
      EdFrtRegFisC.SetFocus;
    end;
  end;
end;

procedure TUnFrt_PF.CadastroESelecaoDeFrtSrvCad(QrFrtSrvCad: TmySQLQuery;
  EdFrtSrvCad: TdmkEditCB; CBFrtSrvCad: TdmkDBLookupCombobox);
begin
  VAR_CADASTRO := 0;
  //
  MostraFormFrtSrvCad(EdFrtSrvCad.ValueVariant);
  //
  if VAR_CADASTRO <> 0 then
  begin
    if QrFrtSrvCad <> nil then
      UnDmkDAC_PF.AbreQuery(QrFrtSrvCad, QrFrtSrvCad.Database);
    //
    EdFrtSrvCad.ValueVariant := VAR_CADASTRO;
    CBFrtSrvCad.KeyValue     := VAR_CADASTRO;
    EdFrtSrvCad.SetFocus;
  end;
end;

function TUnFrt_PF.DesfazEncerramentoFatura(FatID, FatNum, Empresa: Integer): Boolean;
var
  Serie, nCT, Filial, Status, SerieDesfe, CTDesfeita, Codigo: Integer;
  TabLctA, Encerrou, DataFat: String;
  Qry: TmySQLQuery;
begin
  Result := False;
  // Parei aqui! Desmarcar!
  //if not DBCheck.LiberaPelaSenhaAdmin then Exit;
  Screen.Cursor := crHourGlass;
  try
    Qry := TmySQLQuery.Create(Dmod);
    try
      UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT ide_Serie, ide_nCT ',
      'FROM ctecaba ',
      'WHERE FatID=' + Geral.FF0(FatID),
      'AND FatNum=' + Geral.FF0(FatNum),
      'AND Empresa=' + Geral.FF0(Empresa),
      '']);
      Serie := Qry.FieldByName('ide_Serie').AsInteger;
      nCT := Qry.FieldByName('ide_nCT').AsInteger;
      // Exclui lançamentos de contas a receber
      Filial := DModG.ObtemFilialDeEntidade(Empresa);
      TabLctA  := DmodG.NomeTab(TMeuDB, ntLct, False, ttA, Filial);
      UFinanceiro.ExcluiLct_FatNum(nil, FatID, FatNum, Empresa, 0,
      320(*CO_MOTVDEL_320_EXCLUILCTFATURAFRETE*), False, TabLctA);
      //
      //
      // Altera status de encerramento
      Status     := Integer(ctemystatusDesencerrada);
      Encerrou   := '0000-00-00';
      DataFat    := '0000-00-00';
      SerieDesfe := Serie;
      CTDesfeita := nCT;
      Codigo     := FatNum;
      //
      Result := UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'frtfatcab', False, [
      'DataFat', 'Encerrou', 'Status',
      'SerieDesfe', 'CTDesfeita'], [
      'Codigo'], [
      DataFat, Encerrou, Status,
      SerieDesfe, CTDesfeita], [
      Codigo], True);
      //
      Result := True;
    finally
      Qry.Free;
    end;
  finally
    Screen.Cursor := crDefault;
  end;
end;

function TUnFrt_PF.DesfazEncerramentoManifesto(FatID, FatNum,
  Empresa: Integer): Boolean;
var
  Serie, nMDF, Filial, Status, SerieDesfe, MDFDesfeita, Codigo: Integer;
  Encerrou: String;
  Qry: TmySQLQuery;
begin
  Result := False;
  Screen.Cursor := crHourGlass;
  try
    Qry := TmySQLQuery.Create(Dmod);
    try
      UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT ide_Serie, ide_nMDF ',
      'FROM mdfecaba ',
      'WHERE FatID=' + Geral.FF0(FatID),
      'AND FatNum=' + Geral.FF0(FatNum),
      'AND Empresa=' + Geral.FF0(Empresa),
      '']);
      Serie := Qry.FieldByName('ide_Serie').AsInteger;
      nMDF := Qry.FieldByName('ide_nMDF').AsInteger;
      //
      //
      // Altera status de encerramento
      Status     := Integer(ctemystatusDesencerrada);
      Encerrou   := '0000-00-00';
      SerieDesfe := Serie;
      MDFDesfeita := nMDF;
      Codigo     := FatNum;
      //
      Result := UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'frtmnfcab', False, [
      'Encerrou', 'Status',
      'SerieDesfe', 'MDFDesfeita'], [
      'Codigo'], [
      Encerrou, Status,
      SerieDesfe, MDFDesfeita], [
      Codigo], True);
      //
      Result := True;
    finally
      Qry.Free;
    end;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TUnFrt_PF.MostraFormFrtCaracAd();
begin
  UnCfgCadLista.MostraCadLista(Dmod.MyDB, 'FrtCaracAd', 60, ncGerlSeq1,
  'Características de Transporte', [], False, Null, [], [], False);
end;

procedure TUnFrt_PF.MostraFormFrtCaracSer();
begin
  UnCfgCadLista.MostraCadLista(Dmod.MyDB, 'FrtCaracSer', 60, ncGerlSeq1,
  'Características do Serviço', [], False, Null, [], [], False);
end;

procedure TUnFrt_PF.MostraFormFrtFatAutXml(Codigo, Tomador: Integer;
  InserePadroes: Boolean);
begin
  if DBCheck.CriaFm(TFmFrtFatAutXml, FmFrtFatAutXml, afmoNegarComAviso) then
  begin
(*
    FmFrtFatAutXml.FFatID         := FatID;
    FmFrtFatAutXml.FFatNum        := FatNum;
    FmFrtFatAutXml.FEmpresa       := Empresa;
    FmFrtFatAutXml.FDestRem       := DestRem;
*)
    FmFrtFatAutXml.FCodigo        := Codigo;
    FmFrtFatAutXml.FTomador       := Tomador;
    FmFrtFatAutXml.FInserePadroes := InserePadroes;
    //
    FmFrtFatAutXml.ReopenFrtFatAutXml(0);
    FmFrtFatAutXml.ShowModal;
    FmFrtFatAutXml.Destroy;
  end;
end;

procedure TUnFrt_PF.MostraFormFrtFatCab;
begin
  if DBCheck.CriaFm(TFmFrtFatCab, FmFrtFatCab, afmoNegarComAviso) then
  begin
    FmFrtFatCab.ShowModal;
    FmFrtFatCab.Destroy;
  end;
end;

procedure TUnFrt_PF.MostraFormFrtMnfCab();
begin
  if DBCheck.CriaFm(TFmFrtMnfCab, FmFrtMnfCab, afmoNegarComAviso) then
  begin
    FmFrtMnfCab.ShowModal;
    FmFrtMnfCab.Destroy;
  end;
end;

procedure TUnFrt_PF.MostraFormFrtPrcCad(Codigo: Integer);
begin
  UnCfgCadLista.MostraCadLista(Dmod.MyDB, 'FrtPrcCad', 100, ncGerlSeq1,
  'Tabelas de Preçode Frete', [], False, Null, [], [], False);
end;

procedure TUnFrt_PF.MostraFormFrtProPred();
begin
  UnCfgCadLista.MostraCadLista(Dmod.MyDB, 'FrtProPred', 60, ncGerlSeq1,
  'Produtos Predominantes', [], False, Null, [], [], False);
end;

procedure TUnFrt_PF.MostraFormFrtRegFatC(Codigo: Integer);
begin
  if DBCheck.CriaFm(TFmFrtRegFatC, FmFrtRegFatC, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
    begin
      FmFrtRegFatC.LocCod(Codigo, Codigo);
      //FmFrtRegFatC.ReopenVSPaClaCab();
    end;
    //
    FmFrtRegFatC.ShowModal;
    FmFrtRegFatC.Destroy;
  end;
end;

procedure TUnFrt_PF.MostraFormFrtRegFisC(Codigo: Integer);
begin
  if DBCheck.CriaFm(TFmFrtRegFisC, FmFrtRegFisC, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
    begin
      FmFrtRegFisC.LocCod(Codigo, Codigo);
      //FmFrtRegFisC.ReopenVSPaClaCab();
    end;
    //
    FmFrtRegFisC.ShowModal;
    FmFrtRegFisC.Destroy;
  end;
end;


procedure TUnFrt_PF.MostraFormFrtSrvCad(Codigo: Integer);
begin
  if DBCheck.CriaFm(TFmFrtSrvCad, FmFrtSrvCad, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
    begin
      FmFrtSrvCad.LocCod(Codigo, Codigo);
      //FmFrtSrvCad.ReopenVSPaClaCab();
    end;
    //
    FmFrtSrvCad.ShowModal;
    FmFrtSrvCad.Destroy;
  end;
end;

procedure TUnFrt_PF.MostraFormProdutCad(Codigo: Integer);
begin
  if DBCheck.CriaFm(TFmProdutCad, FmProdutCad, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
    begin
      FmProdutCad.LocCod(Codigo, Codigo);
      //FmProdutCad.ReopenVSPaClaCab();
    end;
    //
    FmProdutCad.ShowModal;
    FmProdutCad.Destroy;
  end;
end;

end.
