unit FatConRetGru;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, Grids, ComCtrls, DBCtrls,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkGeral, Variants, dmkLabel, Mask,
  dmkImage, UnDmkEnums, UnDmkProcFunc;

type
  TFmFatConRetGru = class(TForm)
    PainelConfirma: TPanel;
    BtOK: TBitBtn;
    PainelTitulo: TPanel;
    Image1: TImage;
    Panel1: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    QrGraGru1: TmySQLQuery;
    DsGraGru1: TDataSource;
    PnSeleciona: TPanel;
    EdGraGru1: TdmkEditCB;
    CBGraGru1: TdmkDBLookupComboBox;
    Label1: TLabel;
    PageControl1: TPageControl;
    TabSheet3: TTabSheet;
    GradeA: TStringGrid;
    TabSheet5: TTabSheet;
    GradeQ: TStringGrid;
    TabSheet6: TTabSheet;
    GradeC: TStringGrid;
    TabSheet9: TTabSheet;
    GradeX: TStringGrid;
    QrLoc: TmySQLQuery;
    QrLocUltimo: TIntegerField;
    PB1: TProgressBar;
    QrLista: TmySQLQuery;
    QrListaGraCusPrc: TIntegerField;
    StaticText1: TStaticText;
    QrGraGru1CodUsu: TIntegerField;
    QrGraGru1Nome: TWideStringField;
    QrGraGru1Nivel1: TIntegerField;
    QrGraGru1Fracio: TSmallintField;
    QrGraGru1GraTamCad: TIntegerField;
    TabSheet1: TTabSheet;
    GradeV: TStringGrid;
    QrVendido: TmySQLQuery;
    QrVendidoQuantV: TFloatField;
    QrVendidoControle: TIntegerField;
    QrVendidoCodigo: TIntegerField;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure EdGraGru1Change(Sender: TObject);
    procedure EdGraGru1Exit(Sender: TObject);
    procedure GradeQDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect;
      State: TGridDrawState);
    procedure GradeADrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect;
      State: TGridDrawState);
    procedure GradeCDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect;
      State: TGridDrawState);
    procedure BtOKClick(Sender: TObject);
    procedure GradeQDblClick(Sender: TObject);
    procedure GradeQKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure EdGraGru1Enter(Sender: TObject);
    procedure GradeXDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect;
      State: TGridDrawState);
    procedure CBGraGru1Exit(Sender: TObject);
    procedure GradeVDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect;
      State: TGridDrawState);
    procedure ImgTipoChange(Sender: TObject);
  private
    { Private declarations }
    FOldNivel1: Integer;
    procedure ReconfiguraGradeQ();
    // Qtde de itens
    function QtdeItemCorTam(GridP, GridC, GridX: TStringGrid): Boolean;
    function QtdeVariosItensCorTam(GridP, GridA, GridC, GridX: TStringGrid): Boolean;
    function ObtemQtde(var Qtde: Double): Boolean;
    //
{
    function DescontoVariosItensCorTam(GridD, GridA, GridC, GridX:
             TStringGrid): Boolean;
    function ObtemDesconto(var Desconto: Double): Boolean;
}
  public
    { Public declarations }
  end;

  var
  FmFatConRetGru: TFmFatConRetGru;

implementation

uses UnMyObjects, Module, ModProd, MyVCLSkin, GetValor, UMySQLModule, UnInternalConsts,
  FatConRet, GraGruVal, MyDBCheck, ModPediVda, ModuleGeral;

{$R *.DFM}

const
  FThisType = 004;

procedure TFmFatConRetGru.BtOKClick(Sender: TObject);
var
  Col, Row, GraGruX, Codigo: Integer;
  Tot, Qtd_Ven, Qtd_Ret: Double;
  ErrRet, QtdRed: Integer;
  //
  OriCtrl, IDCtrl, StqCenCad, OriCodi, Empresa, OriCnta,
  AlterWeb, Ativo: Integer;
  Qtde, Qtd, Baixa: Double;
  DataHora: String;
begin
  if EdGraGru1.ValueVariant = 0 then
  begin
    Application.MessageBox('Defina o produto!', 'Aviso', MB_OK+MB_ICONWARNING);
    Exit;
  end;
  //Nivel1 := QrGraGru1Nivel1.Value;
  Screen.Cursor := crHourGlass;
  try
    //
    DataHora  := FormatDateTime('yyyy-mm-dd hh:nn:ss', DModG.ObtemAgora());
    StqCenCad := FmFatConRet.QrFatConRetStqCenCad.Value;
    OriCodi   := FmFatConRet.QrFatConRetCodigo.Value;
    Empresa   := FmFatConRet.QrFatConRetEmpresa.Value;
    OriCnta   := 0;
    AlterWeb  := 1;
    Ativo     := 1;
    //
    Codigo    := FmFatConRet.QrFatConRetCodigo.Value;
    Tot       := 0;
    ErrRet    := 0;
    QtdRed    := 0;
    for Col := 1 to GradeQ.ColCount -1 do
    begin
      for Row := 1 to GradeQ.RowCount - 1 do
      begin
        Qtd_Ret := Geral.IMV(GradeQ.Cells[Col,Row]);
        Tot := Tot + Qtd_Ret;
        Qtd_Ven := Geral.IMV(GradeV.Cells[Col,Row]);
        if Qtd_Ret > Qtd_Ven then
          ErrRet := ErrRet + 1;
      end;
    end;
    if Tot = 0 then
    begin
      Screen.Cursor := crDefault;
      Application.MessageBox(
        'N�o h� defini��o de quantidades!', 'Aviso', MB_OK+MB_ICONWARNING);
      Exit;
    end
    else if ErrRet > 0 then
    begin
      Screen.Cursor := crDefault;
      Application.MessageBox(PChar('Existem ' + IntToStr(ErrRet) +
      ' itens sem sa�da suficiente para retorno!'), 'Aviso',
      MB_OK+MB_ICONWARNING);
      Exit;
    end;
    PB1.Position := 0;
    PB1.Max := QtdRed;
    for Col := 1 to GradeQ.ColCount -1 do
    begin
      if Geral.IMV(GradeX.Cells[Col,0]) > 0 then
      begin
        for Row := 1 to GradeQ.RowCount - 1 do
        begin
          if Geral.IMV(GradeX.Cells[0,Row]) > 0 then
          begin
            Qtde := Geral.DMV(GradeQ.Cells[Col, Row]);
            if Qtde > 0 then
            begin
              GraGruX := Geral.IMV(GradeC.Cells[Col, Row]);
              QrVendido.Close;
              QrVendido.Params[00].AsInteger := Codigo;
              QrVendido.Params[01].AsInteger := GraGruX;
              QrVendido.Open;
              while not QrVendido.Eof do
              begin
                //Baixa := 0;
                if Qtde = 0 then QrVendido.Last else
                begin
                  if Qtde >= QrVendidoQuantV.Value then
                  begin
                    Baixa := QrVendidoQuantV.Value;
                    Qtde  := Qtde - Baixa;
                  end else begin
                    Baixa := Qtde;
                    Qtde  := 0;
                  end;
                  if Baixa > 0 then
                  begin
                    // Inverter sinal de sa�da !!
                    Qtd       := Baixa * FmFatConRet.QrFatConRetFATOR.Value * -1;
                    OriCtrl   := 0;
                    OriCtrl   := DModG.BuscaProximoCodigoInt('controle', 'stqmov004', '', OriCtrl);
                    //Controle  := OriCtrl;
                    //
                    IDCtrl := UMyMod.Busca_IDCtrl_NFe(stIns, 0);
                    if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'stqmovitsa', False, [
                    'DataHora', 'Tipo', 'OriCodi',
                    'OriCtrl', 'Empresa', 'StqCenCad',
                    'GraGruX', 'Qtde', 'OriCnta',
                    'OriPart', 'AlterWeb', 'Ativo'], [
                    'IDCtrl'], [
                    DataHora, FThisType, OriCodi,
                    OriCtrl, Empresa, StqCenCad,
                    GraGruX, Qtd, OriCnta,
                    OriCtrl, AlterWeb, Ativo], [
                    IDCtrl], False) then
                    begin
                      Dmod.QrUpd.SQL.Clear;
                      Dmod.QrUpd.SQL.Add('UPDATE fatconits SET ');
                      Dmod.QrUpd.SQL.Add('QuantC=QuantC+ :P0, ');
                      Dmod.QrUpd.SQL.Add('QUANTV=QuantV- :P1  ');
                      Dmod.QrUpd.SQL.Add('WHERE Controle=:P2');
                      Dmod.QrUpd.Params[00].AsFloat   := Baixa;
                      Dmod.QrUpd.Params[01].AsFloat   := Baixa;
                      Dmod.QrUpd.Params[02].AsInteger := QrVendidoControle.Value;
                      Dmod.QrUpd.ExecSQL;
                      //
                      FmFatConRet.AtualizaItensFaturados(QrVendidoCodigo.Value);
                    end;
                  end;
                  //
                  QrVendido.Next;
                end;
              end;
            end;
          end;
        end;
      end;
    end;
    EdGraGru1.ValueVariant := 0;
    EdGraGru1.SetFocus;
    DmPediVda.AtzSdosCondicional_Devol(Codigo);
    FmFatConRet.LocCod(Codigo, Codigo);
    FmFatConRet.ReopenGrupos(FmFatConRet.QrGruposNivel1.Value);
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmFatConRetGru.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmFatConRetGru.CBGraGru1Exit(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 0;
  GradeQ.SetFocus;
  GradeQ.Col := 1;
  GradeQ.Row := 1;
end;

{
function TFmFatConRetGru.DescontoVariosItensCorTam(GridD, GridA, GridC,
  GridX: TStringGrid): Boolean;
var
  Coluna, Linha, c, l, Ativos, ColI, ColF, RowI, RowF,
  CountC, CountL: Integer;
  Desconto: Double;
  DescontoTxt, ItensTxt: String;
begin
  Result := False;
  Screen.Cursor := crHourGlass;
  Ativos := 0;
  RowI := 0;
  RowF := 0;
  ColI := 0;
  ColF := 0;
  Coluna := GridD.Col;
  Linha  := GridD.Row;
  if (Coluna = 0) and (Linha = 0) then
  begin
    ColI := 1;
    ColF := GridD.ColCount - 1;
    RowI := 1;
    RowF := GridD.RowCount - 1;
  end else if Coluna = 0 then
  begin
    ColI := 1;
    ColF := GridD.ColCount - 1;
    RowI := Linha;
    RowF := Linha;
  end else if Linha = 0 then
  begin
    ColI := Coluna;
    ColF := Coluna;
    RowI := 1;
    RowF := GridD.RowCount - 1;
  end;
  //
  CountC := 0;
  CountL := 0;
  for c := ColI to ColF do
    for l := RowI to RowF do
  begin
    CountC := CountC + Geral.IMV(GridX.Cells[c, 0]);
    CountL := CountL + Geral.IMV(GridX.Cells[0, l]);
  end;
  if CountC = 0 then
  begin
    Application.MessageBox('Tamanho n�o definido!', 'Aviso', MB_OK+MB_ICONWARNING);
    Screen.Cursor := crDefault;
    Exit;
  end;
  if CountL = 0 then
  begin
    Application.MessageBox('Cor n�o definida!', 'Aviso', MB_OK+MB_ICONWARNING);
    Screen.Cursor := crDefault;
    Exit;
  end;
  //
  //Nivel1 := QrGraGru1Nivel1.Value;
  Desconto  := Geral.DMV(GridD.Cells[ColI, RowI]);
  for c := ColI to ColF do
    for l := RowI to RowF do
      Ativos := Ativos + Geral.IMV(GridA.Cells[c, l]);
  if Ativos = 0 then
  begin
    Application.MessageBox(PChar('N�o h� nenhum item com c�digo na sele��o ' +
    'para que se possa incluir / alterar o pre�o!'), 'Aviso',
    MB_OK+MB_ICONWARNING);
    Screen.Cursor := crDefault;
    Exit;
  end;
  //
  if ObtemDesconto(Desconto) then
  begin
    DescontoTxt := Geral.FFT(Desconto, 2, siNegativo);
    if (Coluna = 0) and (Linha = 0) then
    begin
      ItensTxt := 'todo grupo';
    end else if Coluna = 0 then
    begin
      ItensTxt := 'todos tamanhos da cor ' + GridA.Cells[0, Linha];
    end else if Linha = 0 then
    begin
      ItensTxt := 'todas cores do tamanho ' + GridA.Cells[Coluna, 0];
    end;
    if Application.MessageBox(PChar('Confirma o valor de ' + DescontoTxt + ' para ' +
    ItensTxt + '?'), 'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) <> ID_YES then
    begin
      Screen.Cursor := crDefault;
      Exit;
    end;
    //
    for c := ColI to ColF do
      for l := RowI to RowF do
        GridD.Cells[c, l] := DescontoTxt;
    Result := True;
  end;
  Screen.Cursor := crDefault;
end;
}

procedure TFmFatConRetGru.EdGraGru1Change(Sender: TObject);
begin
  if EdGraGru1.Focused = False then
    ReconfiguraGradeQ();
end;

procedure TFmFatConRetGru.EdGraGru1Enter(Sender: TObject);
begin
  FOldNivel1 := EdGraGru1.ValueVariant;
end;

procedure TFmFatConRetGru.EdGraGru1Exit(Sender: TObject);
begin
  if FOldNivel1 <> EdGraGru1.ValueVariant then
  begin
    FOldNivel1 := EdGraGru1.ValueVariant;
    ReconfiguraGradeQ();
  end;
end;

procedure TFmFatConRetGru.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmFatConRetGru.FormCreate(Sender: TObject);
begin
  {
  ST1.Caption := '';
  ST2.Caption := '';
  ST3.Caption := '';
  ST1.Font.Color := clRed;
  ST2.Font.Color := clRed;
  ST3.Font.Color := clRed;
  ST1.Color := PnSeleciona.Color;
  ST2.Color := PnSeleciona.Color;
  ST3.Color := PnSeleciona.Color;
  }
  //
  PageControl1.ActivePageIndex := 0;
  GradeA.ColWidths[0] := 128;
  GradeC.ColWidths[0] := 128;
  GradeX.ColWidths[0] := 128;
  GradeQ.ColWidths[0] := 128;
  GradeV.ColWidths[0] := 128;
  //
  QrGraGru1.Close;
  QrGraGru1.SQL.Clear;

  QrGraGru1.SQL.Add('SELECT DISTINCT gg1.CodUsu, gg1.Nome, gg1.Nivel1,');
  QrGraGru1.SQL.Add('gg1.GraTamCad, pgt.Fracio');
  QrGraGru1.SQL.Add('FROM fatconits fci');
  QrGraGru1.SQL.Add('LEFT JOIN fatconcad fcc ON fcc.Codigo=fci.Codigo');
  QrGraGru1.SQL.Add('LEFT JOIN gragrux ggx ON ggx.Controle=fci.GraGruX');
  QrGraGru1.SQL.Add('LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1');
  QrGraGru1.SQL.Add('LEFT JOIN prdgruptip pgt ON pgt.Codigo=gg1.PrdGrupTip');
  QrGraGru1.SQL.Add('WHERE fcc.FatConRet=:P0');
//  QrGraGru1.SQL.Add('AND fcc.QuantP > fcc.QuantV + fcc.QuantC');
  QrGraGru1.SQL.Add('AND fci.QuantV > 0');
  QrGraGru1.SQL.Add('ORDER BY gg1.Nome');
  QrGraGru1.Params[0].AsInteger := FmFatConRet.QrFatConRetCodigo.Value;
  QrGraGru1.Open;
  //
end;

procedure TFmFatConRetGru.FormResize(Sender: TObject);
begin
  // M L A G e r a l .LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

procedure TFmFatConRetGru.GradeADrawCell(Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);
begin
  MyObjects.DesenhaGradeA(GradeA, ACol, ARow, Rect, State, True);
end;

procedure TFmFatConRetGru.GradeCDrawCell(Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);
begin
  MyObjects.DesenhaGradeN(GradeC, GradeA, nil, ACol, ARow, Rect, State,
  '0', 0, 0, True);
end;

procedure TFmFatConRetGru.GradeQDblClick(Sender: TObject);
begin
  if (GradeQ.Col = 0) or (GradeQ.Row = 0) then
    QtdeVariosItensCorTam(GradeQ, GradeA, GradeC, GradeX)
  else
    QtdeItemCorTam(GradeQ, GradeC, GradeX);
end;

procedure TFmFatConRetGru.GradeQDrawCell(Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);
begin
  MyObjects.DesenhaGradeN(GradeQ, GradeA, nil, ACol, ARow, Rect, State,
  dmkPF.FormataCasas(QrGraGru1Fracio.Value), 0, 0, False);
end;

procedure TFmFatConRetGru.GradeQKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  MyObjects.PulaCelulaGradeInputEx(GradeQ, GradeC, Key, Shift, BtOK);
end;

procedure TFmFatConRetGru.GradeVDrawCell(Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);
begin
  MyObjects.DesenhaGradeN(GradeV, GradeA, nil, ACol, ARow, Rect, State,
  dmkPF.FormataCasas(QrGraGru1Fracio.Value), 0, 0, False);
end;

procedure TFmFatConRetGru.GradeXDrawCell(Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);
begin
  MyObjects.DesenhaGradeN(GradeX, nil, nil, ACol, ARow, Rect, State,
  '0', 0, 0, True);
end;

procedure TFmFatConRetGru.ImgTipoChange(Sender: TObject);
begin
  if ImgTipo.SQLType in ([stIns, stUpd]) then
  begin
    BtOK.Enabled   := True;
    GradeQ.Options := GradeQ.Options + [goEditing];
    //GradeD.Options := GradeD.Options + [goEditing];
  end else begin
    BtOK.Enabled   := False;
    GradeQ.Options := GradeQ.Options - [goEditing];
    //GradeD.Options := GradeQ.Options - [goEditing];
  end;
end;

procedure TFmFatConRetGru.ReconfiguraGradeQ;
var
  Grade, Nivel1, Codigo: Integer;
begin
  Codigo     := FmFatConRet.QrFatConRetCodigo.Value;
  Grade      := QrGraGru1GraTamCad.Value;
  Nivel1     := QrGraGru1Nivel1.Value;
  PageControl1.Visible := Nivel1 > 0;
  //
  // Selecionar quantidades  // Impress�o de etiquetas
  DmProd.ConfigGrades11(Codigo, Grade, Nivel1,
    GradeA, GradeX, GradeC, GradeQ, GradeV);
end;

{
function TFmFatConRetGru.ObtemDesconto(var Desconto: Double): Boolean;
begin
  Result := False;
  if DBCheck.CriaFm(TFmGraGruVal, FmGraGruVal, afmoNegarComAviso) then
  begin
    FmGraGruVal.EdValor.ValueVariant := Desconto;
    FmGraGruVal.ShowModal;
    Result   := FmGraGruVal.FConfirmou;
    Desconto := FmGraGruVal.EdValor.ValueVariant;
    FmGraGruVal.Destroy;
  end;
end;
}

function TFmFatConRetGru.ObtemQtde(var Qtde: Double): Boolean;
var
  ResVar: Variant;
begin
  Qtde := 0;
  Result := False;
  if MyObjects.GetValorDmk(TFmGetValor, FmGetValor, dmktfDouble,
  0, 3, 0, '', '', True, 'Itens', 'Informe a quantidade de itens: ',
  0, ResVar) then
  begin
    Qtde := Geral.IMV(ResVar);
    Result := True;
  end;
end;

function TFmFatConRetGru.QtdeItemCorTam(GridP, GridC, GridX: TStringGrid): Boolean;
var
  Qtde: Double;
  c, l, GraGruX, Coluna, Linha: Integer;
begin
  Result := False;
  Screen.Cursor := crHourGlass;
  Coluna := GridP.Col;
  Linha  := GridP.Row;
  c := Geral.IMV(GridX.Cells[Coluna, 0]);
  l := Geral.IMV(GridX.Cells[0, Linha]);
  GraGruX := Geral.IMV(GridC.Cells[Coluna, Linha]);
  if c = 0 then
  begin
    Application.MessageBox('Tamanho n�o definido!', 'Aviso', MB_OK+MB_ICONWARNING);
    Screen.Cursor := crDefault;
    Exit;
  end;
  if l = 0 then
  begin
    Application.MessageBox('Cor n�o definida!', 'Aviso', MB_OK+MB_ICONWARNING);
    Screen.Cursor := crDefault;
    Exit;
  end;
  if GraGruX = 0 then
  begin
    Application.MessageBox('O item nunca foi ativado!', 'Aviso', MB_OK+MB_ICONWARNING);
    Screen.Cursor := crDefault;
    Exit;
  end;
  if (c = 0) or (l = 0) or (GraGruX = 0) then Exit;
  //
  Qtde  := Geral.DMV(GridP.Cells[Coluna, Linha]);
  if ObtemQtde(Qtde) then
    GridP.Cells[Coluna, Linha] := FloatToStr(Qtde);
  Result := True;
  Screen.Cursor := crDefault;
end;

function TFmFatConRetGru.QtdeVariosItensCorTam(GridP, GridA, GridC, GridX:
TStringGrid): Boolean;
var
  Coluna, Linha, c, l, Ativos, ColI, ColF, RowI, RowF,
  CountC, CountL, GraGruX: Integer;
  Qtde: Double;
  QtdeTxt: String;
begin
  Result := False;
  Screen.Cursor := crHourGlass;
  Ativos := 0;
  RowI := 0;
  RowF := 0;
  ColI := 0;
  ColF := 0;
  Coluna := GridP.Col;
  Linha  := GridP.Row;
  if (Coluna = 0) and (Linha = 0) then
  begin
    ColI := 1;
    ColF := GridP.ColCount - 1;
    RowI := 1;
    RowF := GridP.RowCount - 1;
  end else if Coluna = 0 then
  begin
    ColI := 1;
    ColF := GridP.ColCount - 1;
    RowI := Linha;
    RowF := Linha;
  end else if Linha = 0 then
  begin
    ColI := Coluna;
    ColF := Coluna;
    RowI := 1;
    RowF := GridP.RowCount - 1;
  end;
  //
  CountC := 0;
  CountL := 0;
  for c := ColI to ColF do
    for l := RowI to RowF do
  begin
    CountC := CountC + Geral.IMV(GridX.Cells[c, 0]);
    CountL := CountL + Geral.IMV(GridX.Cells[0, l]);
  end;
  if CountC = 0 then
  begin
    Application.MessageBox('Tamanho n�o definido!', 'Aviso', MB_OK+MB_ICONWARNING);
    Screen.Cursor := crDefault;
    Exit;
  end;
  if CountL = 0 then
  begin
    Application.MessageBox('Cor n�o definida!', 'Aviso', MB_OK+MB_ICONWARNING);
    Screen.Cursor := crDefault;
    Exit;
  end;
  //
  Qtde   := Geral.IMV(GridP.Cells[ColI, RowI]);
  for c := ColI to ColF do
    for l := RowI to RowF do
      Ativos := Ativos + Geral.IMV(GridA.Cells[c, l]);
  if Ativos = 0 then
  begin
    Application.MessageBox(PChar('N�o h� nenhum item com c�digo na sele��o ' +
    'para que se possa incluir / alterar o pre�o!'), 'Aviso',
    MB_OK+MB_ICONWARNING);
    Screen.Cursor := crDefault;
    Exit;
  end;
  //
  if ObtemQtde(Qtde) then
  begin
    QtdeTxt := FloatToStr(Qtde);
    for c := ColI to ColF do
      for l := RowI to RowF do
    begin
      GraGruX := Geral.IMV(GridC.Cells[c, l]);
      //
      if GraGruX > 0 then
        GradeQ.Cells[c,l] := QtdeTxt;
    end;
    Result := True;
  end;
  Screen.Cursor := crDefault;
end;

end.

