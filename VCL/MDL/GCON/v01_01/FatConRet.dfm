object FmFatConRet: TFmFatConRet
  Left = 368
  Top = 194
  Caption = 'FAT-CONDI-011 :: Acerto de Faturamento Condicional'
  ClientHeight = 582
  ClientWidth = 1008
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelDados: TPanel
    Left = 0
    Top = 48
    Width = 1008
    Height = 534
    Align = alClient
    Color = clAppWorkSpace
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    object Splitter1: TSplitter
      Left = 1
      Top = 301
      Width = 1006
      Height = 3
      Cursor = crVSplit
      Align = alTop
      ExplicitTop = 289
      ExplicitWidth = 52
    end
    object GroupBox5: TGroupBox
      Left = 1
      Top = 161
      Width = 1006
      Height = 64
      Align = alTop
      TabOrder = 4
      object Label17: TLabel
        Left = 548
        Top = 16
        Width = 57
        Height = 13
        Caption = '$ Comiss'#227'o:'
        FocusControl = DBEdit19
      end
      object dmkLabel2: TdmkLabel
        Left = 8
        Top = 16
        Width = 90
        Height = 13
        Caption = 'Vendedor (Cliente):'
        UpdType = utYes
        SQLType = stNil
      end
      object Label21: TLabel
        Left = 8
        Top = 40
        Width = 39
        Height = 13
        Caption = 'Carteira:'
      end
      object Label22: TLabel
        Left = 686
        Top = 16
        Width = 59
        Height = 13
        Caption = '% Comiss'#227'o:'
      end
      object Label23: TLabel
        Left = 822
        Top = 16
        Width = 105
        Height = 13
        Caption = '% Redu'#231#227'o comiss'#227'o:'
      end
      object Label34: TLabel
        Left = 840
        Top = 40
        Width = 75
        Height = 13
        Caption = 'Desconto geral:'
      end
      object DBEdit11: TDBEdit
        Left = 104
        Top = 12
        Width = 56
        Height = 21
        DataField = 'Cliente'
        DataSource = DsFatConRet
        TabOrder = 0
      end
      object DBEdit13: TDBEdit
        Left = 160
        Top = 12
        Width = 385
        Height = 21
        DataField = 'NO_CLIENTE'
        DataSource = DsFatConRet
        TabOrder = 1
      end
      object DBEdit3: TDBEdit
        Left = 96
        Top = 36
        Width = 741
        Height = 21
        DataField = 'NO_CARTEMIS'
        DataSource = DsFatConRet
        TabOrder = 2
      end
      object DBEdit16: TDBEdit
        Left = 50
        Top = 36
        Width = 44
        Height = 21
        DataField = 'CartEmis'
        DataSource = DsFatConRet
        TabOrder = 3
      end
      object DBEdit17: TDBEdit
        Left = 748
        Top = 12
        Width = 61
        Height = 21
        DataField = 'ComisPer'
        DataSource = DsFatConRet
        TabOrder = 4
      end
      object DBEdit18: TDBEdit
        Left = 920
        Top = 36
        Width = 76
        Height = 21
        DataField = 'DescoGer'
        DataSource = DsFatConRet
        TabOrder = 5
      end
      object DBEdit19: TDBEdit
        Left = 612
        Top = 12
        Width = 72
        Height = 21
        DataField = 'ComisVal'
        DataSource = DsFatConRet
        TabOrder = 6
      end
      object DBEdit20: TDBEdit
        Left = 932
        Top = 12
        Width = 64
        Height = 21
        DataField = 'PercRedu'
        DataSource = DsFatConRet
        TabOrder = 7
      end
    end
    object PnCabeca: TPanel
      Left = 1
      Top = 1
      Width = 1006
      Height = 92
      Align = alTop
      BevelOuter = bvNone
      Enabled = False
      TabOrder = 1
      object Label1: TLabel
        Left = 8
        Top = 4
        Width = 14
        Height = 13
        Caption = 'ID:'
        FocusControl = DBEdCodigo
      end
      object Label2: TLabel
        Left = 152
        Top = 4
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
        FocusControl = DBEdNome
      end
      object Label3: TLabel
        Left = 68
        Top = 4
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
        FocusControl = DBEdit1
      end
      object Label4: TLabel
        Left = 588
        Top = 4
        Width = 44
        Height = 13
        Caption = 'Empresa:'
        FocusControl = DBEdit2
      end
      object Label6: TLabel
        Left = 8
        Top = 44
        Width = 90
        Height = 13
        Caption = 'Centro de estoque:'
        FocusControl = DBEdit4
      end
      object Label11: TLabel
        Left = 768
        Top = 44
        Width = 43
        Height = 13
        Caption = 'Abertura:'
      end
      object Label12: TLabel
        Left = 884
        Top = 44
        Width = 69
        Height = 13
        Caption = 'Encerramento:'
      end
      object Label16: TLabel
        Left = 540
        Top = 44
        Width = 51
        Height = 13
        Caption = '$ Vendido:'
        FocusControl = DBEdit15
      end
      object Label19: TLabel
        Left = 464
        Top = 44
        Width = 58
        Height = 13
        Caption = 'Quantidade:'
        FocusControl = DBEdit14
      end
      object Label30: TLabel
        Left = 692
        Top = 44
        Width = 58
        Height = 13
        Caption = '$ Pendente:'
        FocusControl = DBEdValorPen
      end
      object Label31: TLabel
        Left = 616
        Top = 44
        Width = 58
        Height = 13
        Caption = '$ Recebido:'
        FocusControl = DBEdit21
      end
      object DBEdCodigo: TDBEdit
        Left = 8
        Top = 20
        Width = 56
        Height = 21
        Hint = 'N'#186' do banco'
        TabStop = False
        DataField = 'Codigo'
        DataSource = DsFatConRet
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 0
      end
      object DBEdNome: TDBEdit
        Left = 152
        Top = 20
        Width = 432
        Height = 21
        Hint = 'Nome do banco'
        Color = clWhite
        DataField = 'Nome'
        DataSource = DsFatConRet
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
      end
      object DBEdit1: TDBEdit
        Left = 68
        Top = 20
        Width = 80
        Height = 21
        DataField = 'CodUsu'
        DataSource = DsFatConRet
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 2
      end
      object DBEdit2: TDBEdit
        Left = 588
        Top = 20
        Width = 408
        Height = 21
        DataField = 'NOMEFILIAL'
        DataSource = DsFatConRet
        TabOrder = 3
      end
      object DBEdit4: TDBEdit
        Left = 8
        Top = 60
        Width = 452
        Height = 21
        DataField = 'MOMESTQCENCAD'
        DataSource = DsFatConRet
        TabOrder = 4
      end
      object DBEdit5: TDBEdit
        Left = 768
        Top = 60
        Width = 112
        Height = 21
        DataField = 'Abertura'
        DataSource = DsFatConRet
        TabOrder = 5
      end
      object DBEdit6: TDBEdit
        Left = 884
        Top = 60
        Width = 112
        Height = 21
        DataField = 'ENCERROU_TXT'
        DataSource = DsFatConRet
        TabOrder = 6
        OnChange = DBEdit6Change
      end
      object DBEdit15: TDBEdit
        Left = 540
        Top = 60
        Width = 72
        Height = 21
        DataField = 'ValFat'
        DataSource = DsFatConRet
        TabOrder = 7
      end
      object DBEdit14: TDBEdit
        Left = 464
        Top = 60
        Width = 72
        Height = 21
        DataField = 'QuantV'
        DataSource = DsFatConRet
        TabOrder = 8
      end
      object DBEdValorPen: TDBEdit
        Left = 692
        Top = 60
        Width = 72
        Height = 21
        DataField = 'VALORPEN'
        DataSource = DsFatConRet
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 9
      end
      object DBEdit21: TDBEdit
        Left = 616
        Top = 60
        Width = 72
        Height = 21
        DataField = 'ValorRec'
        DataSource = DsFatConRet
        TabOrder = 10
      end
    end
    object PainelControle: TPanel
      Left = 1
      Top = 485
      Width = 1006
      Height = 48
      Align = alBottom
      TabOrder = 0
      object LaRegistro: TdmkLabel
        Left = 173
        Top = 1
        Width = 26
        Height = 13
        Align = alClient
        Caption = '[N]: 0'
        UpdType = utYes
        SQLType = stNil
      end
      object Panel5: TPanel
        Left = 1
        Top = 1
        Width = 172
        Height = 46
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object Panel3: TPanel
        Left = 496
        Top = 1
        Width = 509
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object BtItens: TBitBtn
          Left = 188
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Itens'
          Enabled = False
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtItensClick
        end
        object BtMovim: TBitBtn
          Left = 4
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Movim.'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtMovimClick
        end
        object Panel2: TPanel
          Left = 400
          Top = 0
          Width = 109
          Height = 46
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 2
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 90
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
        object BtLotes: TBitBtn
          Left = 96
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Lotes'
          Enabled = False
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = BtLotesClick
        end
        object BtPagtos: TBitBtn
          Left = 280
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Pagtos'
          Enabled = False
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 4
          OnClick = BtPagtosClick
        end
      end
    end
    object Panel4: TPanel
      Left = 1
      Top = 368
      Width = 1006
      Height = 117
      Align = alBottom
      TabOrder = 2
      object DBGGrupos: TdmkDBGrid
        Left = 1
        Top = 1
        Width = 432
        Height = 115
        Align = alLeft
        Columns = <
          item
            Expanded = False
            FieldName = 'CU_NIVEL1'
            Title.Caption = 'C'#243'digo'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NO_NIVEL1'
            Title.Caption = 'Nome'
            Width = 215
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'QuantV'
            Title.Caption = 'Quant.'
            Width = 38
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Valor'
            Width = 75
            Visible = True
          end>
        Color = clWindow
        DataSource = DsGrupos
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'CU_NIVEL1'
            Title.Caption = 'C'#243'digo'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NO_NIVEL1'
            Title.Caption = 'Nome'
            Width = 215
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'QuantV'
            Title.Caption = 'Quant.'
            Width = 38
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Valor'
            Width = 75
            Visible = True
          end>
      end
      object PageControl1: TPageControl
        Left = 433
        Top = 1
        Width = 572
        Height = 115
        ActivePage = TabSheet6
        Align = alClient
        MultiLine = True
        TabOrder = 1
        object TabSheet5: TTabSheet
          Caption = ' Quantidade '
          ImageIndex = 2
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object GradeQ: TStringGrid
            Left = 0
            Top = 0
            Width = 564
            Height = 70
            Align = alClient
            ColCount = 2
            DefaultColWidth = 65
            DefaultRowHeight = 18
            FixedCols = 0
            RowCount = 2
            FixedRows = 0
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goTabs]
            ParentFont = False
            TabOrder = 0
            OnDrawCell = GradeQDrawCell
          end
          object StaticText1: TStaticText
            Left = 0
            Top = 70
            Width = 507
            Height = 17
            Align = alBottom
            Alignment = taCenter
            BorderStyle = sbsSunken
            Caption = 
              'D'#234' um duplo clique na c'#233'lula, coluna ou linha correspondente par' +
              'a alterar a quantidade.'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlue
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 1
            Visible = False
          end
        end
        object TabSheet4: TTabSheet
          Caption = ' Valor unit'#225'rio '
          ImageIndex = 5
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object GradeF: TStringGrid
            Left = 0
            Top = 0
            Width = 564
            Height = 87
            Align = alClient
            ColCount = 2
            DefaultColWidth = 65
            DefaultRowHeight = 18
            FixedCols = 0
            RowCount = 2
            FixedRows = 0
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goTabs]
            ParentFont = False
            TabOrder = 0
            OnDrawCell = GradeFDrawCell
          end
        end
        object TabSheet6: TTabSheet
          Caption = ' Desconto '
          ImageIndex = 6
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object GradeD: TStringGrid
            Left = 0
            Top = 0
            Width = 564
            Height = 70
            Align = alClient
            ColCount = 2
            DefaultColWidth = 65
            DefaultRowHeight = 18
            FixedCols = 0
            RowCount = 2
            FixedRows = 0
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goTabs]
            ParentFont = False
            TabOrder = 0
            OnDrawCell = GradeDDrawCell
          end
          object StaticText3: TStaticText
            Left = 0
            Top = 70
            Width = 544
            Height = 17
            Align = alBottom
            Alignment = taCenter
            BorderStyle = sbsSunken
            Caption = 
              'D'#234' um duplo clique na c'#233'lula, coluna ou linha correspondente par' +
              'a incluir / alterar o desconto.'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlue
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 1
          end
        end
        object TabSheet7: TTabSheet
          Caption = ' Valor l'#237'quido'
          ImageIndex = 1
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object GradeV: TStringGrid
            Left = 0
            Top = 0
            Width = 564
            Height = 87
            Align = alClient
            ColCount = 2
            DefaultColWidth = 65
            DefaultRowHeight = 18
            FixedCols = 0
            RowCount = 2
            FixedRows = 0
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goTabs]
            ParentFont = False
            TabOrder = 0
            OnDrawCell = GradeVDrawCell
          end
        end
        object TabSheet8: TTabSheet
          Caption = ' C'#243'digos '
          ImageIndex = 3
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object GradeC: TStringGrid
            Left = 0
            Top = 0
            Width = 564
            Height = 70
            Align = alClient
            ColCount = 1
            DefaultColWidth = 65
            DefaultRowHeight = 18
            FixedCols = 0
            RowCount = 1
            FixedRows = 0
            TabOrder = 0
            OnDrawCell = GradeCDrawCell
            RowHeights = (
              18)
          end
          object StaticText6: TStaticText
            Left = 0
            Top = 70
            Width = 502
            Height = 17
            Align = alBottom
            Alignment = taCenter
            BorderStyle = sbsSunken
            Caption = 
              'Para gerar o c'#243'digo, clique na c'#233'lula, coluna ou linha correspon' +
              'dente na guia "Ativos".'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlue
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 1
            Visible = False
          end
        end
        object TabSheet9: TTabSheet
          Caption = ' Ativos '
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object GradeA: TStringGrid
            Left = 0
            Top = 0
            Width = 564
            Height = 70
            Align = alClient
            ColCount = 2
            DefaultColWidth = 65
            DefaultRowHeight = 18
            FixedCols = 0
            RowCount = 2
            FixedRows = 0
            TabOrder = 0
            OnDrawCell = GradeADrawCell
            RowHeights = (
              18
              18)
          end
          object StaticText2: TStaticText
            Left = 0
            Top = 70
            Width = 475
            Height = 17
            Align = alBottom
            Alignment = taCenter
            BorderStyle = sbsSunken
            Caption = 
              'Clique na c'#233'lula, coluna ou linha correspondente para ativar / d' +
              'esativar o produto.'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlue
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 1
            Visible = False
          end
        end
        object TabSheet10: TTabSheet
          Caption = ' X '
          ImageIndex = 6
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object GradeX: TStringGrid
            Left = 0
            Top = 0
            Width = 564
            Height = 87
            Align = alClient
            ColCount = 2
            DefaultColWidth = 65
            DefaultRowHeight = 18
            FixedCols = 0
            RowCount = 2
            FixedRows = 0
            TabOrder = 0
            OnDrawCell = GradeXDrawCell
            RowHeights = (
              18
              18)
          end
        end
      end
    end
    object GroupBox4: TGroupBox
      Left = 1
      Top = 93
      Width = 1006
      Height = 68
      Align = alTop
      Caption = 
        'Regra fiscal (precisa ser aplica'#231#227'o igual a "Condicional" e c'#225'lc' +
        'ulo diferente de "Nulo": '
      TabOrder = 3
      object Label24: TLabel
        Left = 12
        Top = 44
        Width = 55
        Height = 13
        Caption = 'Modelo NF:'
      end
      object Label26: TLabel
        Left = 12
        Top = 20
        Width = 73
        Height = 13
        Caption = 'Movimenta'#231#227'o:'
      end
      object Label27: TLabel
        Left = 600
        Top = 44
        Width = 55
        Height = 13
        Caption = 'Movimento:'
      end
      object Label28: TLabel
        Left = 764
        Top = 44
        Width = 38
        Height = 13
        Caption = 'C'#225'lculo:'
      end
      object Label29: TLabel
        Left = 920
        Top = 44
        Width = 27
        Height = 13
        Caption = 'Fator:'
      end
      object DBEdit8: TDBEdit
        Left = 88
        Top = 16
        Width = 57
        Height = 21
        DataField = 'CU_FisRegCad'
        DataSource = DsFatConRet
        TabOrder = 0
      end
      object DBEdit7: TDBEdit
        Left = 148
        Top = 16
        Width = 837
        Height = 21
        DataField = 'NO_FisRegCad'
        DataSource = DsFatConRet
        TabOrder = 1
      end
      object DBEdit9: TDBEdit
        Left = 656
        Top = 40
        Width = 95
        Height = 21
        TabStop = False
        DataField = 'NO_TipoMov'
        DataSource = DsFatConRet
        TabOrder = 2
      end
      object DBEdit10: TDBEdit
        Left = 808
        Top = 40
        Width = 108
        Height = 21
        TabStop = False
        DataField = 'NO_TipoCalc'
        DataSource = DsFatConRet
        TabOrder = 3
      end
      object DBEdit12: TDBEdit
        Left = 952
        Top = 40
        Width = 33
        Height = 21
        DataField = 'FATOR'
        DataSource = DsFatConRet
        TabOrder = 4
      end
      object DBEdit50: TDBEdit
        Left = 88
        Top = 40
        Width = 505
        Height = 21
        DataField = 'NOMEMODELONF'
        DataSource = DsFatConRet
        TabOrder = 5
      end
    end
    object Panel6: TPanel
      Left = 1
      Top = 225
      Width = 1006
      Height = 76
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 5
      object DBGrid1: TDBGrid
        Left = 0
        Top = 0
        Width = 489
        Height = 76
        Align = alLeft
        DataSource = DsFatConLot
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'CodUsu'
            Title.Caption = 'C'#243'digo'
            Width = 44
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Nome'
            Title.Caption = 'Descri'#231#227'o'
            Width = 179
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Abertura'
            Width = 113
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Encerrou'
            Title.Caption = 'Encerramento'
            Width = 113
            Visible = True
          end>
      end
      object DBGrid2: TDBGrid
        Left = 489
        Top = 0
        Width = 517
        Height = 76
        Align = alClient
        DataSource = DsEmissC
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'SEQ'
            Width = 30
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Vencimento'
            Width = 66
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'VALOR'
            Title.Caption = 'Valor'
            Width = 70
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NOMECARTEIRA'
            Title.Caption = 'Carteira'
            Width = 238
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'COMPENSA_TXT'
            Title.Alignment = taCenter
            Title.Caption = 'Compensado'
            Width = 72
            Visible = True
          end>
      end
    end
  end
  object PainelEdita: TPanel
    Left = 0
    Top = 48
    Width = 1008
    Height = 534
    Align = alClient
    Color = clBtnShadow
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 2
    Visible = False
    object PainelConfirma: TPanel
      Left = 1
      Top = 485
      Width = 1006
      Height = 48
      Align = alBottom
      TabOrder = 3
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 8
        Top = 3
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object Panel1: TPanel
        Left = 897
        Top = 1
        Width = 108
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 7
          Top = 2
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
    end
    object PainelEdit: TPanel
      Left = 1
      Top = 1
      Width = 1006
      Height = 92
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object Label7: TLabel
        Left = 8
        Top = 4
        Width = 14
        Height = 13
        Caption = 'ID:'
        FocusControl = DBEdCodigo
      end
      object Label8: TLabel
        Left = 68
        Top = 4
        Width = 57
        Height = 13
        Caption = 'C'#243'digo: [F4]'
        FocusControl = DBEdit1
      end
      object Label9: TLabel
        Left = 152
        Top = 4
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
        FocusControl = DBEdNome
      end
      object dmkLabel1: TdmkLabel
        Left = 588
        Top = 4
        Width = 44
        Height = 13
        Caption = 'Empresa:'
        UpdType = utYes
        SQLType = stNil
      end
      object dmkLabel3: TdmkLabel
        Left = 8
        Top = 44
        Width = 90
        Height = 13
        Caption = 'Centro de estoque:'
        UpdType = utYes
        SQLType = stNil
      end
      object Label10: TLabel
        Left = 768
        Top = 44
        Width = 43
        Height = 13
        Caption = 'Abertura:'
        Enabled = False
      end
      object EdCodigo: TdmkEdit
        Left = 8
        Top = 20
        Width = 56
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Codigo'
        UpdCampo = 'Codigo'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdNome: TdmkEdit
        Left = 152
        Top = 20
        Width = 433
        Height = 21
        TabOrder = 2
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Nome'
        UpdCampo = 'Nome'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdCodUsu: TdmkEdit
        Left = 68
        Top = 20
        Width = 80
        Height = 21
        Alignment = taRightJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 1
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'CodUsu'
        UpdCampo = 'CodUsu'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnKeyDown = EdCodUsuKeyDown
      end
      object EdFilial: TdmkEditCB
        Left = 588
        Top = 20
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 3
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = EdFilialChange
        DBLookupComboBox = CBFilial
        IgnoraDBLookupComboBox = False
      end
      object CBFilial: TdmkDBLookupComboBox
        Left = 644
        Top = 20
        Width = 352
        Height = 21
        KeyField = 'Filial'
        ListField = 'NOMEFILIAL'
        ListSource = DsFiliais
        TabOrder = 4
        dmkEditCB = EdFilial
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdStqCenCad: TdmkEditCB
        Left = 8
        Top = 60
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 5
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = EdStqCenCadChange
        DBLookupComboBox = CBStqCenCad
        IgnoraDBLookupComboBox = False
      end
      object CBStqCenCad: TdmkDBLookupComboBox
        Left = 68
        Top = 60
        Width = 697
        Height = 21
        KeyField = 'CodUsu'
        ListField = 'Nome'
        ListSource = DsStqCenCad
        TabOrder = 6
        dmkEditCB = EdStqCenCad
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdAbertura: TdmkEdit
        Left = 768
        Top = 60
        Width = 228
        Height = 21
        Enabled = False
        TabOrder = 7
        FormatType = dmktfDateTime
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfLong
        Texto = '30/12/1899 00:00:00'
        QryCampo = 'Abertura'
        UpdCampo = 'Abertura'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
    end
    object GroupBox6: TGroupBox
      Left = 1
      Top = 93
      Width = 1006
      Height = 68
      Align = alTop
      Caption = 
        ' Regra fiscal (precisa ser aplica'#231#227'o igual a "Condicional" e c'#225'l' +
        'culo diferente de "Nulo": '
      TabOrder = 1
      object Label33: TLabel
        Left = 12
        Top = 44
        Width = 55
        Height = 13
        Caption = 'Modelo NF:'
      end
      object Label25: TLabel
        Left = 12
        Top = 20
        Width = 73
        Height = 13
        Caption = 'Movimenta'#231#227'o:'
      end
      object SpeedButton5: TSpeedButton
        Left = 964
        Top = 14
        Width = 21
        Height = 21
        Caption = '...'
        OnClick = SpeedButton5Click
      end
      object Label13: TLabel
        Left = 600
        Top = 44
        Width = 55
        Height = 13
        Caption = 'Movimento:'
        FocusControl = EdDBTipoMov
      end
      object Label14: TLabel
        Left = 764
        Top = 44
        Width = 38
        Height = 13
        Caption = 'C'#225'lculo:'
        FocusControl = EdDBTipoCalc
      end
      object Label15: TLabel
        Left = 920
        Top = 44
        Width = 27
        Height = 13
        Caption = 'Fator:'
      end
      object EdModeloNF: TdmkEdit
        Left = 68
        Top = 40
        Width = 525
        Height = 21
        ReadOnly = True
        TabOrder = 2
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdFisRegCad: TdmkEditCB
        Left = 92
        Top = 14
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = EdFisRegCadChange
        OnExit = EdFisRegCadExit
        DBLookupComboBox = CBFisRegCad
        IgnoraDBLookupComboBox = False
      end
      object CBFisRegCad: TdmkDBLookupComboBox
        Left = 152
        Top = 14
        Width = 809
        Height = 21
        KeyField = 'CodUsu'
        ListField = 'Nome'
        ListSource = DsFisRegCad
        TabOrder = 1
        dmkEditCB = EdFisRegCad
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdDBTipoMov: TDBEdit
        Left = 660
        Top = 40
        Width = 95
        Height = 21
        TabStop = False
        DataField = 'NO_TipoMov'
        DataSource = DsFisRegCad
        TabOrder = 3
      end
      object EdDBTipoCalc: TDBEdit
        Left = 807
        Top = 40
        Width = 108
        Height = 21
        TabStop = False
        DataField = 'NO_TipoCalc'
        DataSource = DsFisRegCad
        TabOrder = 4
      end
      object EdEntraSai: TdmkEdit
        Left = 952
        Top = 40
        Width = 33
        Height = 21
        Alignment = taRightJustify
        ReadOnly = True
        TabOrder = 5
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
    end
    object GroupBox1: TGroupBox
      Left = 1
      Top = 161
      Width = 1006
      Height = 64
      Align = alTop
      TabOrder = 2
      object dmkLabel5: TdmkLabel
        Left = 8
        Top = 16
        Width = 90
        Height = 13
        Caption = 'Vendedor (Cliente):'
        UpdType = utYes
        SQLType = stNil
      end
      object SpeedButton6: TSpeedButton
        Left = 660
        Top = 12
        Width = 21
        Height = 21
        Caption = '...'
        OnClick = SpeedButton6Click
      end
      object SpeedButton13: TSpeedButton
        Left = 816
        Top = 36
        Width = 21
        Height = 21
        Caption = '...'
        OnClick = SpeedButton13Click
      end
      object Label5: TLabel
        Left = 8
        Top = 40
        Width = 39
        Height = 13
        Caption = 'Carteira:'
      end
      object Label18: TLabel
        Left = 686
        Top = 16
        Width = 59
        Height = 13
        Caption = '% Comiss'#227'o:'
      end
      object Label20: TLabel
        Left = 840
        Top = 40
        Width = 75
        Height = 13
        Caption = 'Desconto geral:'
      end
      object Label32: TLabel
        Left = 822
        Top = 16
        Width = 105
        Height = 13
        Caption = '% Redu'#231#227'o comiss'#227'o:'
      end
      object CBCliente: TdmkDBLookupComboBox
        Left = 164
        Top = 12
        Width = 496
        Height = 21
        KeyField = 'CodUsu'
        ListField = 'NOMEENT'
        ListSource = DmPediVda.DsClientes
        TabOrder = 1
        dmkEditCB = EdCliente
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdCliente: TdmkEditCB
        Left = 104
        Top = 12
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBCliente
        IgnoraDBLookupComboBox = False
      end
      object CBCartEmis: TdmkDBLookupComboBox
        Left = 164
        Top = 36
        Width = 652
        Height = 21
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DmPediVda.DsCartEmis
        TabOrder = 4
        dmkEditCB = EdCartEmis
        QryCampo = 'CartEmis'
        UpdType = utNil
        LocF7SQLMasc = '$#'
      end
      object EdCartEmis: TdmkEditCB
        Left = 104
        Top = 36
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 3
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'CartEmis'
        UpdCampo = 'CartEmis'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBCartEmis
        IgnoraDBLookupComboBox = False
      end
      object EdComisPer: TdmkEdit
        Left = 752
        Top = 12
        Width = 64
        Height = 21
        Alignment = taRightJustify
        TabOrder = 2
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 4
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,0000'
        QryCampo = 'ComisPer'
        UpdCampo = 'ComisPer'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdDescoGer: TdmkEdit
        Left = 920
        Top = 36
        Width = 76
        Height = 21
        Alignment = taRightJustify
        TabOrder = 5
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        QryCampo = 'DescoGer'
        UpdCampo = 'DescoGer'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdPercRedu: TdmkEdit
        Left = 932
        Top = 12
        Width = 64
        Height = 21
        Alignment = taRightJustify
        TabOrder = 6
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 4
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,0000'
        QryCampo = 'PercRedu'
        UpdCampo = 'PercRedu'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    Caption = '                              Acerto de Faturamento Condicional'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 1
    object Image1: TImage
      Left = 226
      Top = 1
      Width = 733
      Height = 46
      Align = alClient
      Transparent = True
      ExplicitLeft = 227
      ExplicitTop = 2
      ExplicitWidth = 376
      ExplicitHeight = 44
    end
    object PanelFill2: TPanel
      Left = 1
      Top = 1
      Width = 225
      Height = 46
      Align = alLeft
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
      ParentColor = True
      ParentFont = False
      TabOrder = 0
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 3
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
        OnClick = SbImprimeClick
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 3
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 3
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 3
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 3
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_R: TGroupBox
      Left = 959
      Top = 1
      Width = 48
      Height = 46
      Align = alRight
      TabOrder = 1
      object ImgTipo: TdmkImage
        Left = 8
        Top = 6
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
  end
  object DsFatConRet: TDataSource
    DataSet = QrFatConRet
    Left = 68
    Top = 12
  end
  object QrFatConRet: TmySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrFatConRetBeforeOpen
    AfterOpen = QrFatConRetAfterOpen
    BeforeClose = QrFatConRetBeforeClose
    AfterScroll = QrFatConRetAfterScroll
    OnCalcFields = QrFatConRetCalcFields
    SQL.Strings = (
      'SELECT IF(emp.Tipo=0,emp.RazaoSocial,emp.Nome) NOMEFILIAL,'
      'emp.Filial, IF(cli.Tipo=0,cli.RazaoSocial,cli.Nome) NO_CLIENTE,'
      'par.BalQtdItem, par.FatSemEstq, frc.Nome NO_FisRegCad,'
      'frc.ModeloNF, frc.CodUsu CU_FisRegCad, frm.TipoMov,'
      'ELT(frm.TipoMov+1,"Entrada","Saida","?") NO_TipoMov,'
      'frm.TipoCalc, ELT(frm.TipoCalc+1,"Nulo","Adiciona",'
      '"Subtrai","?") NO_TipoCalc, frm.GraCusPrc,'
      'imp.Nome NOMEMODELONF, scc.Nome MOMESTQCENCAD,'
      ''
      'par.Associada,'
      'par.CtaServico EMP_CtaServico,'
      'par.FaturaSeq EMP_FaturaSeq,'
      'par.FaturaSep EMP_FaturaSep,'
      'par.FaturaDta EMP_FaturaDta,'
      'par.TxtServico EMP_TxtServico,'
      'par.DupServico  EMP_DupServico,'
      'emp.Filial EMP_FILIAL,'
      ''
      'ase.Filial ASS_FILIAL,'
      'IF(ase.Tipo=0, ase.EUF, ase.PUF) ASS_CO_UF,'
      ''
      'ass.CtaServico ASS_CtaServico,'
      'ass.FaturaSeq ASS_FaturaSeq,'
      'ass.FaturaSep ASS_FaturaSep,'
      'ass.FaturaDta ASS_FaturaDta,'
      'ass.TxtServico ASS_TxtServico,'
      'ass.DupServico  ASS_DupServico,'
      ''
      'car.Nome NO_CARTEMIS, car.Tipo TIPOCART, sbc.*'
      ''
      'FROM fatconret sbc'
      'LEFT JOIN entidades emp ON emp.Codigo=sbc.Empresa'
      'LEFT JOIN stqcencad scc ON scc.Codigo=sbc.StqCenCad'
      'LEFT JOIN paramsemp par ON par.Codigo=sbc.Empresa'
      'LEFT JOIN paramsemp ass ON ass.Codigo=par.Associada'
      'LEFT JOIN entidades ase ON ase.Codigo=ass.Codigo'
      'LEFT JOIN fisregcad frc ON frc.Codigo=sbc.FisRegCad'
      'LEFT JOIN entidades cli ON cli.Codigo=sbc.Cliente'
      'LEFT JOIN fisregmvt frm ON frm.Codigo=frc.Codigo'
      '     AND frm.TipoCalc > 0'
      '     AND frm.Empresa=sbc.Empresa'
      'LEFT JOIN imprime    imp ON imp.Codigo=frc.ModeloNF'
      'LEFT JOIN carteiras  car ON car.Codigo=sbc.CartEmis'
      ''
      'WHERE sbc.Codigo > -1000'
      '')
    Left = 40
    Top = 12
    object QrFatConRetCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'fatconret.Codigo'
      Required = True
    end
    object QrFatConRetCodUsu: TIntegerField
      FieldName = 'CodUsu'
      Origin = 'fatconret.CodUsu'
      Required = True
    end
    object QrFatConRetNome: TWideStringField
      FieldName = 'Nome'
      Origin = 'fatconret.Nome'
      Required = True
      Size = 50
    end
    object QrFatConRetEmpresa: TIntegerField
      FieldName = 'Empresa'
      Origin = 'fatconret.Empresa'
      Required = True
    end
    object QrFatConRetStqCenCad: TIntegerField
      FieldName = 'StqCenCad'
      Origin = 'fatconret.StqCenCad'
      Required = True
    end
    object QrFatConRetAbertura: TDateTimeField
      FieldName = 'Abertura'
      Origin = 'fatconret.Abertura'
      Required = True
      DisplayFormat = 'dd/mm/yyyy hh:nn:ss'
    end
    object QrFatConRetEncerrou: TDateTimeField
      FieldName = 'Encerrou'
      Origin = 'fatconret.Encerrou'
      Required = True
      DisplayFormat = 'dd/mm/yyyy hh:nn:ss'
    end
    object QrFatConRetNOMEFILIAL: TWideStringField
      FieldName = 'NOMEFILIAL'
      Size = 100
    end
    object QrFatConRetMOMESTQCENCAD: TWideStringField
      FieldName = 'MOMESTQCENCAD'
      Origin = 'stqcencad.Nome'
      Size = 50
    end
    object QrFatConRetBalQtdItem: TFloatField
      FieldName = 'BalQtdItem'
      Origin = 'paramsemp.BalQtdItem'
    end
    object QrFatConRetENCERROU_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'ENCERROU_TXT'
      Size = 50
      Calculated = True
    end
    object QrFatConRetFisRegCad: TIntegerField
      FieldName = 'FisRegCad'
      Origin = 'fatconret.FisRegCad'
      Required = True
    end
    object QrFatConRetStatus: TSmallintField
      FieldName = 'Status'
      Origin = 'fatconret.Status'
      Required = True
    end
    object QrFatConRetFatSemEstq: TSmallintField
      FieldName = 'FatSemEstq'
      Origin = 'paramsemp.FatSemEstq'
    end
    object QrFatConRetNO_FisRegCad: TWideStringField
      FieldName = 'NO_FisRegCad'
      Origin = 'fisregcad.Nome'
      Size = 50
    end
    object QrFatConRetCU_FisRegCad: TIntegerField
      FieldName = 'CU_FisRegCad'
      Origin = 'fisregcad.CodUsu'
      Required = True
    end
    object QrFatConRetTipoMov: TSmallintField
      FieldName = 'TipoMov'
      Origin = 'fisregmvt.TipoMov'
    end
    object QrFatConRetNO_TipoMov: TWideStringField
      FieldName = 'NO_TipoMov'
      Size = 7
    end
    object QrFatConRetTipoCalc: TSmallintField
      FieldName = 'TipoCalc'
      Origin = 'fisregmvt.TipoCalc'
    end
    object QrFatConRetNO_TipoCalc: TWideStringField
      FieldName = 'NO_TipoCalc'
      Size = 8
    end
    object QrFatConRetEntraSai: TSmallintField
      FieldName = 'EntraSai'
      Origin = 'fatconret.EntraSai'
      Required = True
    end
    object QrFatConRetFATOR: TSmallintField
      FieldKind = fkCalculated
      FieldName = 'FATOR'
      Calculated = True
    end
    object QrFatConRetGraCusPrc: TIntegerField
      FieldName = 'GraCusPrc'
      Origin = 'fisregmvt.GraCusPrc'
    end
    object QrFatConRetCliente: TIntegerField
      FieldName = 'Cliente'
      Origin = 'fatconret.Cliente'
    end
    object QrFatConRetNO_CLIENTE: TWideStringField
      FieldName = 'NO_CLIENTE'
      Size = 100
    end
    object QrFatConRetNOMEMODELONF: TWideStringField
      FieldName = 'NOMEMODELONF'
      Origin = 'imprime.Nome'
      Size = 100
    end
    object QrFatConRetQuantP: TFloatField
      FieldName = 'QuantP'
      Origin = 'fatconret.QuantP'
    end
    object QrFatConRetQuantC: TFloatField
      FieldName = 'QuantC'
      Origin = 'fatconret.QuantC'
    end
    object QrFatConRetQuantV: TFloatField
      FieldName = 'QuantV'
      Origin = 'fatconret.QuantV'
    end
    object QrFatConRetValLiq: TFloatField
      FieldName = 'ValLiq'
      Origin = 'fatconret.ValLiq'
      DisplayFormat = '###,###,##0.00'
    end
    object QrFatConRetModeloNF: TIntegerField
      FieldName = 'ModeloNF'
    end
    object QrFatConRetFilial: TIntegerField
      FieldName = 'Filial'
    end
    object QrFatConRetAssociada: TIntegerField
      FieldName = 'Associada'
    end
    object QrFatConRetEMP_CtaServico: TIntegerField
      FieldName = 'EMP_CtaServico'
    end
    object QrFatConRetEMP_FaturaSeq: TSmallintField
      FieldName = 'EMP_FaturaSeq'
    end
    object QrFatConRetEMP_FaturaSep: TWideStringField
      FieldName = 'EMP_FaturaSep'
      Size = 1
    end
    object QrFatConRetEMP_FaturaDta: TSmallintField
      FieldName = 'EMP_FaturaDta'
    end
    object QrFatConRetEMP_TxtServico: TWideStringField
      FieldName = 'EMP_TxtServico'
      Size = 100
    end
    object QrFatConRetEMP_DupServico: TWideStringField
      FieldName = 'EMP_DupServico'
      Size = 3
    end
    object QrFatConRetEMP_FILIAL: TIntegerField
      FieldName = 'EMP_FILIAL'
    end
    object QrFatConRetASS_FILIAL: TIntegerField
      FieldName = 'ASS_FILIAL'
    end
    object QrFatConRetASS_CO_UF: TLargeintField
      FieldName = 'ASS_CO_UF'
    end
    object QrFatConRetASS_CtaServico: TIntegerField
      FieldName = 'ASS_CtaServico'
    end
    object QrFatConRetASS_FaturaSeq: TSmallintField
      FieldName = 'ASS_FaturaSeq'
    end
    object QrFatConRetASS_FaturaSep: TWideStringField
      FieldName = 'ASS_FaturaSep'
      Size = 1
    end
    object QrFatConRetASS_FaturaDta: TSmallintField
      FieldName = 'ASS_FaturaDta'
    end
    object QrFatConRetASS_TxtServico: TWideStringField
      FieldName = 'ASS_TxtServico'
      Size = 100
    end
    object QrFatConRetASS_DupServico: TWideStringField
      FieldName = 'ASS_DupServico'
      Size = 3
    end
    object QrFatConRetNO_CARTEMIS: TWideStringField
      FieldName = 'NO_CARTEMIS'
      Size = 100
    end
    object QrFatConRetCartEmis: TIntegerField
      FieldName = 'CartEmis'
    end
    object QrFatConRetTIPOCART: TIntegerField
      FieldName = 'TIPOCART'
      Required = True
    end
    object QrFatConRetComisPer: TFloatField
      FieldName = 'ComisPer'
      DisplayFormat = '0.0000'
    end
    object QrFatConRetComisVal: TFloatField
      FieldName = 'ComisVal'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrFatConRetDescoGer: TFloatField
      FieldName = 'DescoGer'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrFatConRetValorRec: TFloatField
      FieldName = 'ValorRec'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrFatConRetValFat: TFloatField
      FieldName = 'ValFat'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrFatConRetVALORPEN: TFloatField
      FieldKind = fkCalculated
      FieldName = 'VALORPEN'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
      Precision = 2
      Calculated = True
    end
    object QrFatConRetDESCOPER: TFloatField
      FieldKind = fkCalculated
      FieldName = 'DESCOPER'
      Calculated = True
    end
    object QrFatConRetPercRedu: TFloatField
      FieldName = 'PercRedu'
      DisplayFormat = '0.0000'
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = BtMovim
    CanUpd01 = BtItens
    Left = 96
    Top = 12
  end
  object PMMovim: TPopupMenu
    OnPopup = PMMovimPopup
    Left = 524
    Top = 484
    object Incluinovomovimento1: TMenuItem
      Caption = '&Inclui novo acerto'
      OnClick = Incluinovomovimento1Click
    end
    object Alteramovimentoatual1: TMenuItem
      Caption = '&Altera acerto atual'
      Enabled = False
      OnClick = Alteramovimentoatual1Click
    end
    object Encerraacertoatual1: TMenuItem
      Caption = '&Encerra acerto atual'
      OnClick = Encerraacertoatual1Click
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object Atualizaclculos1: TMenuItem
      Caption = '&Recalcula acerto atual'
      OnClick = Atualizaclculos1Click
    end
    object N3: TMenuItem
      Caption = '-'
    end
    object EXcluimovimentoatual1: TMenuItem
      Caption = 'E&Xclui acerto atual'
      Enabled = False
    end
  end
  object DsFiliais: TDataSource
    DataSet = QrFiliais
    Left = 356
    Top = 40
  end
  object QrFiliais: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Filial, Codigo, '
      'IF(Tipo=0, RazaoSocial, Nome) NOMEFILIAL'
      'FROM entidades'
      'WHERE Codigo<-10'
      'ORDER BY NOMEFILIAL')
    Left = 328
    Top = 40
    object QrFiliaisFilial: TIntegerField
      FieldName = 'Filial'
    end
    object QrFiliaisCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrFiliaisNOMEFILIAL: TWideStringField
      FieldName = 'NOMEFILIAL'
      Required = True
      Size = 100
    end
  end
  object dmkValUsu1: TdmkValUsu
    dmkEditCB = EdFilial
    Panel = PainelEdita
    QryCampo = 'Empresa'
    UpdCampo = 'Empresa'
    RefCampo = 'Codigo'
    UpdType = utYes
    Left = 124
    Top = 12
  end
  object dmkValUsu3: TdmkValUsu
    dmkEditCB = EdStqCenCad
    Panel = PainelEdita
    QryCampo = 'StqCenCad'
    UpdCampo = 'StqCenCad'
    RefCampo = 'Codigo'
    UpdType = utYes
    Left = 152
    Top = 12
  end
  object QrStqCenCad: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, CodUsu, Nome'
      'FROM stqcencad'
      'ORDER BY Nome')
    Left = 384
    Top = 40
    object QrStqCenCadCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrStqCenCadCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrStqCenCadNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsStqCenCad: TDataSource
    DataSet = QrStqCenCad
    Left = 412
    Top = 40
  end
  object QrNew: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo'
      'FROM fatconret'
      'WHERE Encerrou=0'
      'AND Empresa=:P0'
      'AND PrdGrupTip=:P1'
      'AND StqCenCad=:P2'
      'ORDER BY Codigo')
    Left = 440
    Top = 40
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrNewCodigo: TIntegerField
      FieldName = 'Codigo'
    end
  end
  object QrSoma: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(Qtde) Qtde'
      'FROM stqmovitsa'
      'WHERE Empresa=:P0'
      'AND StqCenCad=:P1'
      'AND GraGruX=:P2'
      '')
    Left = 476
    Top = 40
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrSomaQtde: TFloatField
      FieldName = 'Qtde'
    end
  end
  object QrFisRegCad: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT frc.Codigo, frc.CodUsu, frc.Nome, frm.TipoMov,'
      'ELT(frm.TipoMov+1,"Entrada","Saida","?") NO_TipoMov, '
      'frm.TipoCalc, ELT(frm.TipoCalc+1,"Nulo","Adiciona",'
      '"Subtrai","?") NO_TipoCalc, imp.Nome NO_MODELO_NF'
      'FROM fisregmvt frm'
      'LEFT JOIN fisregcad frc ON frc.Codigo=frm.Codigo'
      'LEFT JOIN imprime imp ON imp.Codigo=frc.ModeloNF'
      'WHERE frc.Aplicacao=2'
      'AND frm.TipoCalc > 0'
      'AND frm.Empresa=:P0'
      'AND frm.StqCenCad=:P1'
      ''
      '')
    Left = 512
    Top = 40
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrFisRegCadCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrFisRegCadCodUsu: TIntegerField
      FieldName = 'CodUsu'
      Required = True
    end
    object QrFisRegCadNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
    object QrFisRegCadTipoMov: TSmallintField
      FieldName = 'TipoMov'
      Required = True
    end
    object QrFisRegCadNO_TipoMov: TWideStringField
      FieldName = 'NO_TipoMov'
      Size = 7
    end
    object QrFisRegCadTipoCalc: TSmallintField
      FieldName = 'TipoCalc'
      Required = True
    end
    object QrFisRegCadNO_TipoCalc: TWideStringField
      FieldName = 'NO_TipoCalc'
      Size = 8
    end
    object QrFisRegCadNO_MODELO_NF: TWideStringField
      FieldName = 'NO_MODELO_NF'
      Size = 100
    end
  end
  object DsFisRegCad: TDataSource
    DataSet = QrFisRegCad
    Left = 540
    Top = 40
  end
  object dmkValUsu4: TdmkValUsu
    dmkEditCB = EdFisRegCad
    Panel = PainelEdita
    QryCampo = 'FisRegCad'
    UpdCampo = 'FisRegCad'
    RefCampo = 'Codigo'
    UpdType = utYes
    Left = 180
    Top = 12
  end
  object QrGrupos: TmySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrGruposAfterOpen
    BeforeClose = QrGruposBeforeClose
    AfterScroll = QrGruposAfterScroll
    SQL.Strings = (
      'SELECT gg1.CodUsu CU_NIVEL1, gg1.Nome NO_NIVEL1,'
      'gg1.Nivel1, gti.Codigo GraTamCad, SUM(fti.ValFat) Valor,'
      'SUM(fti.QuantV) QuantV, fti.Controle ftiCtrl, pgt.Fracio,'
      'fcc.Codigo FatConCad'
      'FROM fatconits fti'
      'LEFT JOIN fatconcad fcc ON fcc.Codigo=fti.Codigo'
      'LEFT JOIN gragrux ggx ON ggx.Controle=fti.GraGruX'
      'LEFT JOIN gragru1   gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN gratamits gti ON gti.Controle=ggx.GraTamI'
      'LEFT JOIN prdgruptip   pgt ON pgt.Codigo=gg1.PrdGrupTip'
      'WHERE fcc.FatConRet=:P0'
      'GROUP BY ggx.GraGru1')
    Left = 268
    Top = 40
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrGruposCU_NIVEL1: TIntegerField
      FieldName = 'CU_NIVEL1'
    end
    object QrGruposNO_NIVEL1: TWideStringField
      FieldName = 'NO_NIVEL1'
      Size = 30
    end
    object QrGruposNivel1: TIntegerField
      FieldName = 'Nivel1'
    end
    object QrGruposGraTamCad: TIntegerField
      FieldName = 'GraTamCad'
    end
    object QrGruposValor: TFloatField
      FieldName = 'Valor'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrGruposftiCtrl: TIntegerField
      FieldName = 'ftiCtrl'
    end
    object QrGruposFracio: TSmallintField
      FieldName = 'Fracio'
    end
    object QrGruposQuantV: TFloatField
      FieldName = 'QuantV'
    end
    object QrGruposFatConCad: TIntegerField
      FieldName = 'FatConCad'
    end
  end
  object DsGrupos: TDataSource
    DataSet = QrGrupos
    Left = 296
    Top = 40
  end
  object dmkValUsu5: TdmkValUsu
    dmkEditCB = EdCliente
    Panel = PainelEdita
    QryCampo = 'Cliente'
    UpdCampo = 'Cliente'
    RefCampo = 'Codigo'
    UpdType = utYes
    Left = 208
    Top = 12
  end
  object PMItens: TPopupMenu
    Left = 704
    Top = 480
    object RetornaitensporGrade1: TMenuItem
      Caption = 'Retorna itens por &Grade'
      OnClick = RetornaitensporGrade1Click
    end
    object RetornaitensporLeitura1: TMenuItem
      Caption = 'Retorna itens por &Leitura'
      OnClick = RetornaitensporLeitura1Click
    end
  end
  object QrFatConLot: TmySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrFatConLotBeforeClose
    AfterScroll = QrFatConLotAfterScroll
    SQL.Strings = (
      'SELECT fcc.*'
      'FROM fatconcad fcc'
      'WHERE fcc.FatConRet=:P0'
      ''
      '')
    Left = 208
    Top = 40
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrFatConLotCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'fatconcad.Codigo'
    end
    object QrFatConLotCodUsu: TIntegerField
      FieldName = 'CodUsu'
      Origin = 'fatconcad.CodUsu'
    end
    object QrFatConLotNome: TWideStringField
      FieldName = 'Nome'
      Origin = 'fatconcad.Nome'
      Size = 50
    end
    object QrFatConLotEmpresa: TIntegerField
      FieldName = 'Empresa'
      Origin = 'fatconcad.Empresa'
    end
    object QrFatConLotCliente: TIntegerField
      FieldName = 'Cliente'
      Origin = 'fatconcad.Cliente'
    end
    object QrFatConLotPrdGrupTip: TIntegerField
      FieldName = 'PrdGrupTip'
      Origin = 'fatconcad.PrdGrupTip'
    end
    object QrFatConLotStqCenCad: TIntegerField
      FieldName = 'StqCenCad'
      Origin = 'fatconcad.StqCenCad'
    end
    object QrFatConLotFisRegCad: TIntegerField
      FieldName = 'FisRegCad'
      Origin = 'fatconcad.FisRegCad'
    end
    object QrFatConLotEntraSai: TSmallintField
      FieldName = 'EntraSai'
      Origin = 'fatconcad.EntraSai'
    end
    object QrFatConLotCasasProd: TSmallintField
      FieldName = 'CasasProd'
      Origin = 'fatconcad.CasasProd'
    end
    object QrFatConLotAbertura: TDateTimeField
      FieldName = 'Abertura'
      Origin = 'fatconcad.Abertura'
    end
    object QrFatConLotEncerrou: TDateTimeField
      FieldName = 'Encerrou'
      Origin = 'fatconcad.Encerrou'
    end
    object QrFatConLotStatus: TSmallintField
      FieldName = 'Status'
      Origin = 'fatconcad.Status'
    end
    object QrFatConLotTabelaPrc: TIntegerField
      FieldName = 'TabelaPrc'
      Origin = 'fatconcad.TabelaPrc'
    end
    object QrFatConLotMoeda: TIntegerField
      FieldName = 'Moeda'
      Origin = 'fatconcad.Moeda'
    end
    object QrFatConLotQuantP: TFloatField
      FieldName = 'QuantP'
      Origin = 'fatconcad.QuantP'
    end
    object QrFatConLotQuantC: TFloatField
      FieldName = 'QuantC'
      Origin = 'fatconcad.QuantC'
    end
    object QrFatConLotQuantV: TFloatField
      FieldName = 'QuantV'
      Origin = 'fatconcad.QuantV'
    end
    object QrFatConLotValLiq: TFloatField
      FieldName = 'ValLiq'
      Origin = 'fatconcad.ValLiq'
    end
    object QrFatConLotLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'fatconcad.Lk'
    end
    object QrFatConLotDataCad: TDateField
      FieldName = 'DataCad'
      Origin = 'fatconcad.DataCad'
    end
    object QrFatConLotDataAlt: TDateField
      FieldName = 'DataAlt'
      Origin = 'fatconcad.DataAlt'
    end
    object QrFatConLotUserCad: TIntegerField
      FieldName = 'UserCad'
      Origin = 'fatconcad.UserCad'
    end
    object QrFatConLotUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Origin = 'fatconcad.UserAlt'
    end
    object QrFatConLotAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Origin = 'fatconcad.AlterWeb'
    end
    object QrFatConLotAtivo: TSmallintField
      FieldName = 'Ativo'
      Origin = 'fatconcad.Ativo'
    end
    object QrFatConLotFatConRet: TIntegerField
      FieldName = 'FatConRet'
      Origin = 'fatconcad.FatConRet'
    end
  end
  object DsFatConLot: TDataSource
    DataSet = QrFatConLot
    Left = 236
    Top = 40
  end
  object PMLotes: TPopupMenu
    Left = 612
    Top = 480
    object Incluinovolote1: TMenuItem
      Caption = '&Inclui novo lote'
      OnClick = Incluinovolote1Click
    end
    object Retiraloteatual1: TMenuItem
      Caption = '&Retira lote atual'
    end
  end
  object QrDisponiv: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, CodUsu, Nome,'
      'Abertura, Encerrou  '
      'FROM fatconcad'
      'WHERE Encerrou > 0  '
      'AND FatConRet=0'
      ''
      'AND Empresa=:P0'
      'AND Cliente=:P1'
      'AND StqCenCad=:P2'
      'AND FisRegCad=:P3'
      '')
    Left = 708
    Top = 40
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
    object QrDisponivCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrDisponivCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrDisponivNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
    object QrDisponivAbertura: TDateTimeField
      FieldName = 'Abertura'
    end
    object QrDisponivEncerrou: TDateTimeField
      FieldName = 'Encerrou'
    end
  end
  object DsEmissC: TDataSource
    DataSet = QrEmissC
    Left = 908
    Top = 332
  end
  object QrEmissC: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrEmissCCalcFields
    SQL.Strings = (
      'SELECT la.Controle+0.000 Controle, la.Vencimento,'
      'la.Credito-la.Debito VALOR, IF(la.Vencimento > 2,'
      '  DATE_FORMAT(la.Vencimento, '#39'%d/%m/%Y'#39'),'
      '  '#39' '#39') VENCIMENTO_TXT, la.Data, la.Compensado,'
      'la.FatParcela, IF(la.Compensado > 2,'
      '  DATE_FORMAT(la.Compensado, '#39'%d/%m/%Y'#39'),'
      '  '#39' '#39') COMPENSADO_TXT, la.Carteira,  la.SerieCH,'
      'la.Documento, ca.Nome NOMECARTEIRA'
      'FROM lanctos la'
      'LEFT JOIN carteiras ca ON ca.Codigo=la.Carteira'
      'WHERE FatID=:P0'
      'AND FatNum=:P1'
      'ORDER BY Vencimento'
      '')
    Left = 880
    Top = 332
    ParamData = <
      item
        DataType = ftInteger
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrEmissCSEQ: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'SEQ'
      DisplayFormat = '000'
      Calculated = True
    end
    object QrEmissCFatParcela: TIntegerField
      FieldName = 'FatParcela'
    end
    object QrEmissCControle: TFloatField
      FieldName = 'Controle'
      Required = True
    end
    object QrEmissCVencimento: TDateField
      FieldName = 'Vencimento'
    end
    object QrEmissCData: TDateField
      FieldName = 'Data'
    end
    object QrEmissCCarteira: TIntegerField
      FieldName = 'Carteira'
    end
    object QrEmissCCompensado: TDateField
      FieldName = 'Compensado'
    end
    object QrEmissCSerieCH: TWideStringField
      FieldName = 'SerieCH'
      Size = 10
    end
    object QrEmissCDocumento: TFloatField
      FieldName = 'Documento'
    end
    object QrEmissCCOMPENSADO_TXT: TWideStringField
      FieldName = 'COMPENSADO_TXT'
      Size = 10
    end
    object QrEmissCVENCIMENTO_TXT: TWideStringField
      FieldName = 'VENCIMENTO_TXT'
      Size = 10
    end
    object QrEmissCNOMECARTEIRA: TWideStringField
      FieldName = 'NOMECARTEIRA'
      Size = 100
    end
    object QrEmissCVALOR: TFloatField
      FieldName = 'VALOR'
      DisplayFormat = '#,###,###,##0.00'
    end
  end
  object PMPagtos: TPopupMenu
    Left = 796
    Top = 480
    object Incluinovospagamentos1: TMenuItem
      Caption = '&Inclui novo(s) pagamento(s)'
      OnClick = Incluinovospagamentos1Click
    end
    object Alterapagamentoatual1: TMenuItem
      Caption = '&Altera pagamento atual'
      OnClick = Alterapagamentoatual1Click
    end
    object N2: TMenuItem
      Caption = '-'
    end
    object Excluipagamentoatual1: TMenuItem
      Caption = '&Exclui pagamento atual'
      OnClick = Excluipagamentoatual1Click
    end
    object Excluitodospagamentos1: TMenuItem
      Caption = 'E&xclui todos pagamentos'
      OnClick = Excluitodospagamentos1Click
    end
  end
end
