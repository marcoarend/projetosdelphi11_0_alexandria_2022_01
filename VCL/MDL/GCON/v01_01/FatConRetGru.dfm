object FmFatConRetGru: TFmFatConRetGru
  Left = 339
  Top = 185
  Caption = 'FAT-CONDI-013 :: Faturamento Condicional - Retorno por Grade'
  ClientHeight = 482
  ClientWidth = 790
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelConfirma: TPanel
    Left = 0
    Top = 434
    Width = 790
    Height = 48
    Align = alBottom
    TabOrder = 1
    object BtOK: TBitBtn
      Tag = 14
      Left = 20
      Top = 4
      Width = 90
      Height = 40
      Caption = '&OK'
      Enabled = False
      NumGlyphs = 2
      TabOrder = 0
      OnClick = BtOKClick
    end
    object Panel2: TPanel
      Left = 678
      Top = 1
      Width = 111
      Height = 46
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 2
        Top = 3
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Hint = 'Sai da janela atual'
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object PB1: TProgressBar
      Left = 120
      Top = 18
      Width = 541
      Height = 17
      TabOrder = 2
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 790
    Height = 48
    Align = alTop
    Caption = 'Faturamento Condicional - Retorno por Grade'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 2
    object Image1: TImage
      Left = 1
      Top = 1
      Width = 740
      Height = 46
      Align = alClient
      Transparent = True
      ExplicitLeft = 2
      ExplicitTop = 2
      ExplicitWidth = 788
      ExplicitHeight = 44
    end
    object GB_R: TGroupBox
      Left = 741
      Top = 1
      Width = 48
      Height = 46
      Align = alRight
      TabOrder = 0
      ExplicitLeft = 736
      ExplicitTop = 0
      ExplicitHeight = 48
      object ImgTipo: TdmkImage
        Left = 8
        Top = 6
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
        OnChange = ImgTipoChange
      end
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 790
    Height = 386
    Align = alClient
    TabOrder = 0
    object PnSeleciona: TPanel
      Left = 1
      Top = 1
      Width = 788
      Height = 48
      Align = alTop
      TabOrder = 0
      object Label1: TLabel
        Left = 12
        Top = 4
        Width = 40
        Height = 13
        Caption = 'Produto:'
      end
      object EdGraGru1: TdmkEditCB
        Left = 12
        Top = 20
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = EdGraGru1Change
        OnEnter = EdGraGru1Enter
        OnExit = EdGraGru1Exit
        DBLookupComboBox = CBGraGru1
        IgnoraDBLookupComboBox = False
      end
      object CBGraGru1: TdmkDBLookupComboBox
        Left = 72
        Top = 20
        Width = 697
        Height = 21
        KeyField = 'CodUsu'
        ListField = 'Nome'
        ListSource = DsGraGru1
        TabOrder = 1
        OnExit = CBGraGru1Exit
        dmkEditCB = EdGraGru1
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
    end
    object PageControl1: TPageControl
      Left = 1
      Top = 49
      Width = 788
      Height = 336
      ActivePage = TabSheet1
      Align = alClient
      MultiLine = True
      TabOrder = 1
      Visible = False
      object TabSheet5: TTabSheet
        Caption = ' Quantidade '
        ImageIndex = 2
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object GradeQ: TStringGrid
          Left = 0
          Top = 0
          Width = 780
          Height = 291
          Align = alClient
          ColCount = 2
          DefaultColWidth = 65
          DefaultRowHeight = 18
          FixedCols = 0
          RowCount = 2
          FixedRows = 0
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goTabs]
          ParentFont = False
          TabOrder = 0
          OnDblClick = GradeQDblClick
          OnDrawCell = GradeQDrawCell
          OnKeyDown = GradeQKeyDown
        end
        object StaticText1: TStaticText
          Left = 0
          Top = 291
          Width = 555
          Height = 17
          Align = alBottom
          Alignment = taCenter
          BorderStyle = sbsSunken
          Caption = 
            'D'#234' um duplo clique na c'#233'lula, coluna ou linha correspondente par' +
            'a incluir / alterar a quantidade.'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlue
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 1
          Visible = False
        end
      end
      object TabSheet6: TTabSheet
        Caption = ' C'#243'digos '
        ImageIndex = 3
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object GradeC: TStringGrid
          Left = 0
          Top = 0
          Width = 780
          Height = 308
          Align = alClient
          ColCount = 1
          DefaultColWidth = 65
          DefaultRowHeight = 18
          FixedCols = 0
          RowCount = 1
          FixedRows = 0
          TabOrder = 0
          OnDrawCell = GradeCDrawCell
          RowHeights = (
            18)
        end
      end
      object TabSheet3: TTabSheet
        Caption = ' Ativos '
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object GradeA: TStringGrid
          Left = 0
          Top = 0
          Width = 780
          Height = 308
          Align = alClient
          ColCount = 2
          DefaultColWidth = 65
          DefaultRowHeight = 18
          FixedCols = 0
          RowCount = 2
          FixedRows = 0
          TabOrder = 0
          OnDrawCell = GradeADrawCell
          RowHeights = (
            18
            18)
        end
      end
      object TabSheet9: TTabSheet
        Caption = ' X '
        ImageIndex = 6
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object GradeX: TStringGrid
          Left = 0
          Top = 0
          Width = 780
          Height = 308
          Align = alClient
          ColCount = 2
          DefaultColWidth = 65
          DefaultRowHeight = 18
          FixedCols = 0
          RowCount = 2
          FixedRows = 0
          TabOrder = 0
          OnDrawCell = GradeXDrawCell
          RowHeights = (
            18
            18)
        end
      end
      object TabSheet1: TTabSheet
        Caption = ' Vendidos '
        ImageIndex = 4
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object GradeV: TStringGrid
          Left = 0
          Top = 0
          Width = 780
          Height = 308
          Align = alClient
          ColCount = 2
          DefaultColWidth = 65
          DefaultRowHeight = 18
          FixedCols = 0
          RowCount = 2
          FixedRows = 0
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goTabs]
          ParentFont = False
          TabOrder = 0
          OnDrawCell = GradeVDrawCell
        end
      end
    end
  end
  object QrGraGru1: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT DISTINCT gg1.CodUsu, gg1.Nome, gg1.Nivel1,'
      'gg1.GraTamCad, pgt.Fracio'
      'FROM fatconits fci'
      'LEFT JOIN fatconcad fcc ON fcc.Codigo=fci.Codigo'
      'LEFT JOIN gragrux ggx ON ggx.Controle=fci.GraGruX'
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN prdgruptip pgt ON pgt.Codigo=gg1.PrdGrupTip'
      'WHERE fcc.FatConRet>0'
      'AND fcc.QuantV > 0'
      'ORDER BY gg1.Nome'
      '')
    Left = 8
    Top = 8
    object QrGraGru1CodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrGraGru1Nome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
    object QrGraGru1Nivel1: TIntegerField
      FieldName = 'Nivel1'
    end
    object QrGraGru1Fracio: TSmallintField
      FieldName = 'Fracio'
    end
    object QrGraGru1GraTamCad: TIntegerField
      FieldName = 'GraTamCad'
    end
  end
  object DsGraGru1: TDataSource
    DataSet = QrGraGru1
    Left = 36
    Top = 8
  end
  object QrLoc: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT MAX(Sequencia) Ultimo'
      'FROM etqgeraits'
      'WHERE GraGruX = :P0'
      '')
    Left = 64
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrLocUltimo: TIntegerField
      FieldName = 'Ultimo'
      Required = True
    end
  end
  object QrLista: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT GraCusPrc'
      'FROM fisregmvt'
      'WHERE TipoMov=1'
      'AND Empresa=:P0'
      'AND Codigo=:P1')
    Left = 96
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrListaGraCusPrc: TIntegerField
      FieldName = 'GraCusPrc'
    end
  end
  object QrVendido: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT fci.Controle, fci.QuantV, fci.Codigo'
      'FROM fatconits fci'
      'LEFT JOIN fatconcad fcc ON fcc.Codigo=fci.Codigo'
      'LEFT JOIN gragrux ggx ON ggx.Controle=fci.GraGruX'
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN prdgruptip pgt ON pgt.Codigo=gg1.PrdGrupTip'
      'WHERE fcc.FatConRet = :P0'
      'AND fci.GraGruX = :P1'
      'ORDER BY fci.QuantV')
    Left = 656
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrVendidoQuantV: TFloatField
      FieldName = 'QuantV'
    end
    object QrVendidoControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrVendidoCodigo: TIntegerField
      FieldName = 'Codigo'
    end
  end
end
