unit FatConRetLei;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB,
  DB, mySQLDbTables, Mask, Variants, Grids, DBGrids, ComCtrls, dmkLabel,
  dmkDBEdit, dmkDBGrid, dmkImage, UnDmkEnums;

type
  TFmFatConRetLei = class(TForm)
    PainelConfirma: TPanel;
    PainelTitulo: TPanel;
    Image1: TImage;
    Panel1: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    QrItem: TmySQLQuery;
    DsItem: TDataSource;
    QrItemNOMENIVEL1: TWideStringField;
    QrItemGraCorCad: TIntegerField;
    QrItemNOMECOR: TWideStringField;
    QrItemNOMETAM: TWideStringField;
    QrItemGraGruX: TIntegerField;
    QrItemGraGru1: TIntegerField;
    BtExclui: TBitBtn;
    CkFixo: TCheckBox;
    QrFator_: TmySQLQuery;
    QrItemCU_Nivel1: TIntegerField;
    QrItemIPI_Alq: TFloatField;
    QrFatConIts: TmySQLQuery;
    QrItemMadeBy: TSmallintField;
    DsFatConIts: TDataSource;
    PnLeitura: TPanel;
    Panel4: TPanel;
    Label3: TLabel;
    LaQtdeLei: TLabel;
    EdLeitura: TEdit;
    EdQtdLei: TdmkEdit;
    BtOK: TBitBtn;
    Panel9: TPanel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    PnSimu: TPanel;
    Label9: TLabel;
    Label8: TLabel;
    Label7: TLabel;
    dmkEdit2: TdmkEdit;
    dmkEdit3: TdmkEdit;
    dmkEdit1: TdmkEdit;
    Button1: TButton;
    DBGFatConIts: TDBGrid;
    QrFatConItsCU_Nivel1: TIntegerField;
    QrFatConItsNO_Nivel1: TWideStringField;
    QrFatConItsCU_Cor: TIntegerField;
    QrFatConItsNO_Cor: TWideStringField;
    QrFatConItsNO_Tam: TWideStringField;
    QrFatConItsGraGruX: TIntegerField;
    QrFatConItsPrecoF: TFloatField;
    QrFatConItsQuantP: TFloatField;
    QrLista_: TmySQLQuery;
    QrLista_GraCusPrc: TIntegerField;
    QrFatConItsControle: TIntegerField;
    QrFatConItsQuantC: TFloatField;
    QrFatConItsQuantV: TFloatField;
    QrFatConItsValLiq: TFloatField;
    QrTotal: TmySQLQuery;
    DsTotal: TDataSource;
    QrSaldo: TmySQLQuery;
    QrVendido: TmySQLQuery;
    QrVendidoQuantV: TFloatField;
    QrVendidoControle: TIntegerField;
    QrVendidoCodigo: TIntegerField;
    QrSaldoQuantV: TFloatField;
    QrTotalQuantC: TFloatField;
    Panel3: TPanel;
    Label1: TLabel;
    DBEdit4: TDBEdit;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure EdLeituraChange(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure EdQtdLeiKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure BtExcluiClick(Sender: TObject);
    procedure EdLeituraKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure QrItemBeforeClose(DataSet: TDataSet);
    procedure EdQtdLeiEnter(Sender: TObject);
    procedure CkFixoClick(Sender: TObject);
  private
    { Private declarations }
    FTam20: Boolean;
    FGraGruX, FSequencia: Integer;
    function ReopenSaldo(): Boolean;
    function ReopenItem(): Boolean;
    procedure InsereItem();
  public
    { Public declarations }
    procedure ReopenFatConIts(GraGruX: Integer);
    procedure AtualizaEReabre(Codigo, Nivel1, GraGruX: Integer);

  end;

  var
  FmFatConRetLei: TFmFatConRetLei;

implementation

uses UnMyObjects, Module, UMySQLModule, dmkGeral, ModuleGeral, UnInternalConsts,
MyDBCheck, GetValor, Principal, ModPediVda,
(*,ModFatuVda*) ModProd, PediVda, FatPedCab, QuaisItens, FatConRet;

{$R *.DFM}

const
  FThisType = 004;

procedure TFmFatConRetLei.AtualizaEReabre(Codigo, Nivel1, GraGruX: Integer);
begin
  DmPediVda.AtzSdosCondicional_Devol(Codigo);
  FmFatConRet.LocCod(Codigo, Codigo);
  FmFatConRet.ReopenGrupos(Nivel1);
  ReopenFatConIts(GraGruX);
end;

procedure TFmFatConRetLei.BtExcluiClick(Sender: TObject);
var
  Codigo, Nivel1, Controle: Integer;
begin
  if QrFatConItsQuantV.Value > 0 then
  begin
    Geral.MensagemBox('Exclus�o negada! J� existe faturamento para este ' +
    'item!', 'Aviso', MB_OK+MB_ICONWARNING);
    Exit;
  end;
  if Geral.MensagemBox('Confirma a exclus�o do item selecionado?', 'Aviso',
  MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
  begin
    Codigo   := FmFatConRet.QrFatConRetCodigo.Value;
    Nivel1   := FmFatConRet.QrGruposNivel1.Value;
    Controle := QrFatConItsControle.Value;
    //
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('DELETE FROM stqmovitsa WHERE Tipo=:P0');
    Dmod.QrUpd.SQL.Add('AND OriCodi=:P1 AND OriCtrl=:P2 ');
    Dmod.QrUpd.Params[00].AsInteger := FThisType;
    Dmod.QrUpd.Params[01].AsInteger := Codigo;
    Dmod.QrUpd.Params[02].AsInteger := Controle;
    Dmod.QrUpd.ExecSQL;
    //
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('DELETE FROM fatconits WHERE Controle=:Pa');
    Dmod.QrUpd.Params[00].AsInteger := Controle;
    Dmod.QrUpd.ExecSQL;
    //
    AtualizaEReabre(Codigo, Nivel1, 0);
  end;
end;

procedure TFmFatConRetLei.BtOKClick(Sender: TObject);
begin
  InsereItem();
end;

procedure TFmFatConRetLei.InsereItem();
var
  GraGruX, Codigo, OriCtrl, IDCtrl, Empresa, AlterWeb, StqCenCad, OriCnta,
  OriCodi, Ativo: Integer;
  Qtd, Qtde: Double;
  //
  Baixa: Double;
  DataHora: String;
begin
  if (FGragruX = 0) then
  begin
    Application.MessageBox('Defina o produto!', 'Aviso', MB_OK+MB_ICONWARNING);
    EdLeitura.SetFocus;
    Exit;
  end;
  //
  DataHora  := FormatDateTime('yyyy-mm-dd hh:nn:ss', DModG.ObtemAgora());
  StqCenCad := FmFatConRet.QrFatConRetStqCenCad.Value;
  OriCodi   := FmFatConRet.QrFatConRetCodigo.Value;
  Empresa   := FmFatConRet.QrFatConRetEmpresa.Value;
  OriCnta   := 0;
  AlterWeb  := 1;
  Ativo     := 1;
  //
  //Nivel1 := QrItemGraGru1.Value;
  Screen.Cursor := crHourGlass;
  try
    Codigo := FmFatConRet.QrFatConRetCodigo.Value;
    //ErrPreco  := 0;
    //AvisoPrc  := 0;
    //QtdRed    := 0;
    Qtd := Geral.IMV(EdQtdLei.Text);
    if Qtd = 0 then
    begin
      Screen.Cursor := crDefault;
      Application.MessageBox(
        'Definina a quantidade!', 'Aviso', MB_OK+MB_ICONWARNING);
      Exit;
    end;
    if Qtd > QrSaldoQuantV.Value then
    begin
      Screen.Cursor := crDefault;
      Application.MessageBox(PChar('Quantidade a cancelar (' + FloatToStr(Qtd) +
      ') excede o saldo de ' + FloatToStr(QrSaldoQuantV.Value) + ' itens !'),
      'Aviso', MB_OK+MB_ICONWARNING);
      Exit;
    end;
    if FGraGruX > 0 then
    begin
      GraGruX := FGraGruX;
      QrVendido.Close;
      QrVendido.Params[00].AsInteger := Codigo;
      QrVendido.Params[01].AsInteger := GraGruX;
      QrVendido.Open;
      while not QrVendido.Eof do
      begin
        //Baixa := 0;
        if Qtd = 0 then QrVendido.Last else
        begin
          if Qtd >= QrVendidoQuantV.Value then
          begin
            Baixa := QrVendidoQuantV.Value;
            Qtd  := Qtd - Baixa;
          end else begin
            Baixa := Qtd;
            Qtd  := 0;
          end;
          if Baixa > 0 then
          begin
            // Inverter sinal de sa�da !!
            Qtde      := Baixa * FmFatConRet.QrFatConRetFATOR.Value * -1;
            OriCtrl   := 0;
            OriCtrl   := DModG.BuscaProximoCodigoInt('controle', 'stqmov004', '', OriCtrl);
            //Controle  := OriCtrl;
            //
            IDCtrl := UMyMod.Busca_IDCtrl_NFe(stIns, 0);
            if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'stqmovitsa', False, [
            'DataHora', 'Tipo', 'OriCodi',
            'OriCtrl', 'Empresa', 'StqCenCad',
            'GraGruX', 'Qtde', 'OriCnta',
            'OriPart', 'AlterWeb', 'Ativo'], [
            'IDCtrl'], [
            DataHora, FThisType, OriCodi,
            OriCtrl, Empresa, StqCenCad,
            GraGruX, Qtde, OriCnta,
            OriCtrl, AlterWeb, Ativo], [
            IDCtrl], False) then
            begin
              Dmod.QrUpd.SQL.Clear;
              Dmod.QrUpd.SQL.Add('UPDATE fatconits SET ');
              Dmod.QrUpd.SQL.Add('QuantC=QuantC+ :P0, ');
              Dmod.QrUpd.SQL.Add('QUANTV=QuantV- :P1  ');
              Dmod.QrUpd.SQL.Add('WHERE Controle=:P2');
              Dmod.QrUpd.Params[00].AsFloat   := Baixa;
              Dmod.QrUpd.Params[01].AsFloat   := Baixa;
              Dmod.QrUpd.Params[02].AsInteger := QrVendidoControle.Value;
              Dmod.QrUpd.ExecSQL;
              //
              FmFatConRet.AtualizaItensFaturados(QrVendidoCodigo.Value);
            end;
          end;
          //
          QrVendido.Next;
        end;
      end;
      {
      GraGruX    := FGraGruX;
      FGraGruX   := 0;
      Sequencia  := FSequencia;
      FSequencia := 0;
      QuantP     := Geral.DMV(EdQtdLei.Text);
      ImgTipo.SQLType := stIns;
      Controle := UMyMod.BuscaEmLivreY_Def('fatconits', 'Controle',
        ImgTipo.SQLType, 0);
      Casas      := Dmod.QrControleCasasProd.Value;
      PrecoO     := Geral.DMV(EdPrecoO.Text);
      ValCal     := PrecoR;
      DescoP     := Geral.DMV(EdDescoP.Text);
      DescoI     := dmkPF.FFF(PrecoR * DescoP / 100, Casas, siPositivo);
      PrecoF     := PrecoR - DescoI;
      DescoV     := DescoI * QuantP;
      ValBru     := PrecoR * QuantP;
      ValLiq     := ValBru - DescoV;
      //
      if PrecoR <> 0 then
      begin
        Qtde      := QuantP * FmFatConRet.QrFatConRetFATOR.Value;
        OriCtrl   := 0;
        OriCtrl   := DModG.BuscaProximoCodigoInt('controle', 'stqmov004', '', OriCtrl);
        Controle  := OriCtrl;
        //
        IDCtrl := UMyMod.Busca_IDCtrl_NFe(stIns, 0);
        if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'stqmovitsa', False, [
        'DataHora', 'Tipo', 'OriCodi',
        'OriCtrl', 'Empresa', 'StqCenCad',
        'GraGruX', 'Qtde', 'OriCnta',
        'OriPart', 'AlterWeb', 'Ativo'], [
        'IDCtrl'], [
        DataHora, Tipo, OriCodi,
        OriCtrl, Empresa, StqCenCad,
        GraGruX, Qtde, OriCnta,
        OriCtrl, AlterWeb, Ativo], [
        IDCtrl], False) then
        begin
          if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'fatconits', False, [
          'Codigo', 'GraGruX', 'PrecoO',
          'PrecoR', 'QuantP',
          'ValBru', 'DescoP',
          'DescoV', 'ValLiq', 'PrecoF'], [
          'Controle'], [
          Codigo, GraGruX, PrecoO,
          PrecoR, QuantP,
          ValBru, DescoP,
          DescoV, ValLiq, PrecoF], [
          Controle], True) then
          begin
          //
          end;
        end;
      end else
        Application.MessageBox(PChar('Valor (ou pre�o) n�o definido ' +
        'para o reduzido ' + IntToStr(GraGruX) +
        '. Ele n�o ser� incluido!'), 'Aviso',
        MB_OK+MB_ICONWARNING);
      }
    end;
    ImgTipo.SQLType := stIns;
    EdLeitura.Text := '';
    EdLeitura.SetFocus;
    //
    FmFatConRet.LocCod(Codigo, Codigo);
    FmFatConRet.ReopenGrupos(FmFatConRet.QrGruposNivel1.Value);
    AtualizaEReabre(Codigo, FmFatConRet.QrGruposNivel1.Value, FGraGruX);
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmFatConRetLei.QrItemBeforeClose(DataSet: TDataSet);
begin
  QrSaldo.Close;
  //#
  PnSimu.Visible := True;
end;

procedure TFmFatConRetLei.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmFatConRetLei.Button1Click(Sender: TObject);
const
  Tick = 25;
var
  i: Integer;
begin
  EdLeitura.Text := '';
  //
  for i := 1 to Length(dmkEdit1.Text) do
  begin
    EdLeitura.Text := EdLeitura.Text + dmkEdit1.Text[i];
    sleep(Tick);
  end;
  //
  for i := 1 to Length(dmkEdit2.Text) do
  begin
    EdLeitura.Text := EdLeitura.Text + dmkEdit2.Text[i];
    sleep(Tick);
  end;
  //
  for i := 1 to Length(dmkEdit3.Text) do
  begin
    EdLeitura.Text := EdLeitura.Text + dmkEdit3.Text[i];
    sleep(Tick);
  end;
end;

procedure TFmFatConRetLei.CkFixoClick(Sender: TObject);
begin
  EdQtdLei.Enabled := not CkFixo.Checked;
end;

procedure TFmFatConRetLei.EdLeituraChange(Sender: TObject);
var
  Tam: Integer;
begin
  Tam := Length(EdLeitura.Text);
  if Tam = 20 then
  begin
    FGraGruX   := Geral.IMV(Copy(EdLeitura.Text, 07, 6));
    FSequencia := Geral.IMV(Copy(EdLeitura.Text, 13, 8));
    if ReopenItem() then
    begin
      if CkFixo.Checked then
      begin
        FTam20 := True;
        InsereItem();
        EdLeitura.SetFocus;
        EdLeitura.Text := '';
      end else EdQtdLei.SetFocus;
    end;
  end else if Tam = 0 then QrItem.Close;
end;

procedure TFmFatConRetLei.EdLeituraKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_RETURN then
  begin
    if Length(EdLeitura.Text) <= 6 then
      ReopenItem()
    else Application.MessageBox(PChar('Quantidade de caracteres inv�lida para ' +
    'localiza��o pelo reduzido!'), 'Aviso', MB_OK+MB_ICONWARNING);
  end;
end;

procedure TFmFatConRetLei.EdQtdLeiEnter(Sender: TObject);
begin
  if CkFixo.Checked and (EdLeitura.Text = '') and FTam20 then
  begin
    FTam20 := False;
    // est� desabilitado
    //EdPrecoR.SetFocus;
  end;
end;

procedure TFmFatConRetLei.EdQtdLeiKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_RETURN then
    InsereItem();
end;

procedure TFmFatConRetLei.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmFatConRetLei.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  if VAR_USUARIO = -1 then
    PnSimu.Visible := True;
  {
  ST1.Caption := '';
  ST2.Caption := '';
  ST3.Caption := '';
  ST1.Font.Color := clRed;
  ST2.Font.Color := clRed;
  ST3.Font.Color := clRed;
  ST1.Color := PnLeitura.Color;
  ST2.Color := PnLeitura.Color;
  ST3.Color := PnLeitura.Color;
  }
  //
  ReopenFatConIts(0);
  EdQtdLei.ValueVariant := 1;
end;

procedure TFmFatConRetLei.FormResize(Sender: TObject);
begin
  // M L A G e r a l .LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

function TFmFatConRetLei.ReopenItem(): Boolean;
begin
  Result := False;
  QrItem.Close;
  if FGraGruX > 0 then
  begin
    QrItem.Params[0].AsInteger := FGraGrux;
    UMyMod.AbreQuery(QrItem, Dmod.MyDB, 'TFmFatConLei.FormResize()');
    //
    if QrItem.RecordCount > 0 then
    begin
      Result := True;
      // est� desabilitado
      //EdPrecoR.SetFocus;
      //# PnLido.Visible := True;
      //#
      PnSimu.Visible := False;
      ReopenSaldo();
    end else
      Application.MessageBox('Reduzido n�o localizado!', 'Aviso',
      MB_OK+MB_ICONWARNING);
  end;
end;

function TFmFatConRetLei.ReopenSaldo: Boolean;
begin
{
SELECT SUM(fci.QuantV) QuantV
FROM fatconits fci
LEFT JOIN fatconcad fcc ON fcc.Codigo=fci.Codigo
WHERE fci.QuantV > 0
AND fcc.FatConRet = :P0
AND fci.GraGruX= :P1
ORDER BY fci.QuantV, fci.Controle DESC
}
  QrSaldo.Close;
  QrSaldo.Params[00].AsInteger := FmFatConRet.QrFatConRetCodigo.Value;
  QrSaldo.Params[01].AsInteger := FGraGruX;
  QrSaldo.Open;
  //
  Result := True;
end;

procedure TFmFatConRetLei.ReopenFatConIts(GraGruX: Integer);
begin
  QrFatConIts.Close;
  QrFatConIts.Params[0].AsInteger := FmFatConRet.QrFatConRetCodigo.Value;
  UMyMod.AbreQuery(QrFatConIts, Dmod.MyDB, 'TFmFatConLei.ReopenFatConIts()');
  //
  if GraGruX <> 0 then
    QrFatConIts.Locate('GraGruX', GraGruX, []);
  //
  {
  QrTotal.Close;
  QrTotal.Params[0].AsInteger := FmFatConRet.QrFatConRetCodigo.Value;
  QrTotal.Open;
  }
end;

end.

