object FmFatConCad: TFmFatConCad
  Left = 368
  Top = 194
  Caption = 'FAT-CONDI-001 :: Faturamento Condicional - Cadastro Retirada'
  ClientHeight = 582
  ClientWidth = 1008
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelEdita: TPanel
    Left = 0
    Top = 48
    Width = 1008
    Height = 534
    Align = alClient
    Color = clBtnShadow
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 2
    Visible = False
    object PainelConfirma: TPanel
      Left = 1
      Top = 485
      Width = 1006
      Height = 48
      Align = alBottom
      TabOrder = 4
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 8
        Top = 3
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object Panel1: TPanel
        Left = 897
        Top = 1
        Width = 108
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 7
          Top = 2
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
    end
    object PainelEdit: TPanel
      Left = 1
      Top = 1
      Width = 1006
      Height = 92
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object Label7: TLabel
        Left = 8
        Top = 4
        Width = 14
        Height = 13
        Caption = 'ID:'
        FocusControl = DBEdCodigo
      end
      object Label8: TLabel
        Left = 68
        Top = 4
        Width = 57
        Height = 13
        Caption = 'C'#243'digo: [F4]'
        FocusControl = DBEdit1
      end
      object Label9: TLabel
        Left = 152
        Top = 4
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
        FocusControl = DBEdNome
      end
      object dmkLabel1: TdmkLabel
        Left = 588
        Top = 4
        Width = 44
        Height = 13
        Caption = 'Empresa:'
        UpdType = utYes
        SQLType = stNil
      end
      object dmkLabel2: TdmkLabel
        Left = 8
        Top = 44
        Width = 126
        Height = 13
        Caption = 'Tipo de Grupo de Produto:'
        UpdType = utYes
        SQLType = stNil
      end
      object dmkLabel3: TdmkLabel
        Left = 336
        Top = 44
        Width = 90
        Height = 13
        Caption = 'Centro de estoque:'
        UpdType = utYes
        SQLType = stNil
      end
      object Label10: TLabel
        Left = 768
        Top = 44
        Width = 43
        Height = 13
        Caption = 'Abertura:'
        Enabled = False
      end
      object EdCodigo: TdmkEdit
        Left = 8
        Top = 20
        Width = 56
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Codigo'
        UpdCampo = 'Codigo'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdNome: TdmkEdit
        Left = 152
        Top = 20
        Width = 433
        Height = 21
        TabOrder = 2
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Nome'
        UpdCampo = 'Nome'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdCodUsu: TdmkEdit
        Left = 68
        Top = 20
        Width = 80
        Height = 21
        Alignment = taRightJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 1
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'CodUsu'
        UpdCampo = 'CodUsu'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnKeyDown = EdCodUsuKeyDown
      end
      object EdFilial: TdmkEditCB
        Left = 588
        Top = 20
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 3
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = EdFilialChange
        DBLookupComboBox = CBFilial
        IgnoraDBLookupComboBox = False
      end
      object CBFilial: TdmkDBLookupComboBox
        Left = 644
        Top = 20
        Width = 352
        Height = 21
        KeyField = 'Filial'
        ListField = 'NOMEFILIAL'
        ListSource = DsFiliais
        TabOrder = 4
        dmkEditCB = EdFilial
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdPrdGrupTip: TdmkEditCB
        Left = 8
        Top = 60
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 5
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = EdPrdGrupTipChange
        DBLookupComboBox = CBPrdGrupTip
        IgnoraDBLookupComboBox = False
      end
      object CBPrdGrupTip: TdmkDBLookupComboBox
        Left = 64
        Top = 60
        Width = 268
        Height = 21
        KeyField = 'CodUsu'
        ListField = 'Nome'
        ListSource = DsPrdGrupTip
        TabOrder = 6
        dmkEditCB = EdPrdGrupTip
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdStqCenCad: TdmkEditCB
        Left = 336
        Top = 60
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 7
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = EdStqCenCadChange
        DBLookupComboBox = CBStqCenCad
        IgnoraDBLookupComboBox = False
      end
      object CBStqCenCad: TdmkDBLookupComboBox
        Left = 396
        Top = 60
        Width = 236
        Height = 21
        KeyField = 'CodUsu'
        ListField = 'Nome'
        ListSource = DsStqCenCad
        TabOrder = 8
        dmkEditCB = EdStqCenCad
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object RGCasasProd: TdmkRadioGroup
        Left = 636
        Top = 44
        Width = 128
        Height = 37
        Caption = ' Casas decimais: '
        Columns = 4
        Enabled = False
        ItemIndex = 0
        Items.Strings = (
          '0'
          '1'
          '2'
          '3')
        TabOrder = 9
        QryCampo = 'CasasProd'
        UpdCampo = 'CasasProd'
        UpdType = utYes
        OldValor = 0
      end
      object EdAbertura: TdmkEdit
        Left = 768
        Top = 60
        Width = 228
        Height = 21
        Enabled = False
        TabOrder = 10
        FormatType = dmktfDateTime
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfLong
        Texto = '30/12/1899 00:00:00'
        QryCampo = 'Abertura'
        UpdCampo = 'Abertura'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
    end
    object GroupBox3: TGroupBox
      Left = 1
      Top = 93
      Width = 1006
      Height = 36
      Align = alTop
      TabOrder = 1
      object BtTabelaPrc: TSpeedButton
        Left = 684
        Top = 8
        Width = 21
        Height = 21
        Caption = '...'
        OnClick = BtTabelaPrcClick
      end
      object LaTabelaPrc: TLabel
        Left = 8
        Top = 13
        Width = 81
        Height = 13
        Caption = 'Tabela de pre'#231'o:'
      end
      object SpeedButton9: TSpeedButton
        Left = 964
        Top = 8
        Width = 21
        Height = 21
        Caption = '...'
        OnClick = SpeedButton9Click
      end
      object Label22: TLabel
        Left = 718
        Top = 13
        Width = 36
        Height = 13
        Caption = 'Moeda:'
      end
      object CBTabelaPrc: TdmkDBLookupComboBox
        Left = 156
        Top = 8
        Width = 525
        Height = 21
        KeyField = 'CodUsu'
        ListField = 'Nome'
        ListSource = DmPediVda.DsTabePrcCab
        TabOrder = 1
        dmkEditCB = EdTabelaPrc
        QryCampo = 'TabelaPrc'
        UpdType = utNil
        LocF7SQLMasc = '$#'
      end
      object EdTabelaPrc: TdmkEditCB
        Left = 96
        Top = 8
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = EdTabelaPrcChange
        DBLookupComboBox = CBTabelaPrc
        IgnoraDBLookupComboBox = False
      end
      object CBMoeda: TdmkDBLookupComboBox
        Left = 804
        Top = 8
        Width = 157
        Height = 21
        KeyField = 'CodUsu'
        ListField = 'Nome'
        ListSource = DModG.DsCambioMda
        TabOrder = 3
        dmkEditCB = EdMoeda
        UpdType = utNil
        LocF7SQLMasc = '$#'
      end
      object EdMoeda: TdmkEditCB
        Left = 758
        Top = 8
        Width = 44
        Height = 21
        Alignment = taRightJustify
        TabOrder = 2
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBMoeda
        IgnoraDBLookupComboBox = False
      end
    end
    object GroupBox6: TGroupBox
      Left = 1
      Top = 129
      Width = 1006
      Height = 68
      Align = alTop
      Caption = 
        'Regra fiscal (precisa ser aplica'#231#227'o igual a "Condicional" e c'#225'lc' +
        'ulo diferente de "Nulo":'
      TabOrder = 2
      object Label33: TLabel
        Left = 12
        Top = 44
        Width = 55
        Height = 13
        Caption = 'Modelo NF:'
      end
      object Label25: TLabel
        Left = 12
        Top = 20
        Width = 73
        Height = 13
        Caption = 'Movimenta'#231#227'o:'
      end
      object SpeedButton5: TSpeedButton
        Left = 964
        Top = 14
        Width = 21
        Height = 21
        Caption = '...'
        OnClick = SpeedButton5Click
      end
      object Label13: TLabel
        Left = 600
        Top = 44
        Width = 55
        Height = 13
        Caption = 'Movimento:'
        FocusControl = EdDBTipoMov
      end
      object Label14: TLabel
        Left = 764
        Top = 44
        Width = 38
        Height = 13
        Caption = 'C'#225'lculo:'
        FocusControl = EdDBTipoCalc
      end
      object Label15: TLabel
        Left = 920
        Top = 44
        Width = 27
        Height = 13
        Caption = 'Fator:'
      end
      object EdModeloNF: TdmkEdit
        Left = 68
        Top = 40
        Width = 525
        Height = 21
        ReadOnly = True
        TabOrder = 2
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdFisRegCad: TdmkEditCB
        Left = 92
        Top = 14
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = EdFisRegCadChange
        OnExit = EdFisRegCadExit
        DBLookupComboBox = CBFisRegCad
        IgnoraDBLookupComboBox = False
      end
      object CBFisRegCad: TdmkDBLookupComboBox
        Left = 152
        Top = 14
        Width = 809
        Height = 21
        KeyField = 'CodUsu'
        ListField = 'Nome'
        ListSource = DsFisRegCad
        TabOrder = 1
        dmkEditCB = EdFisRegCad
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdDBTipoMov: TDBEdit
        Left = 660
        Top = 40
        Width = 95
        Height = 21
        TabStop = False
        DataField = 'NO_TipoMov'
        DataSource = DsFisRegCad
        TabOrder = 3
      end
      object EdDBTipoCalc: TDBEdit
        Left = 807
        Top = 40
        Width = 108
        Height = 21
        TabStop = False
        DataField = 'NO_TipoCalc'
        DataSource = DsFisRegCad
        TabOrder = 4
      end
      object EdEntraSai: TdmkEdit
        Left = 952
        Top = 40
        Width = 33
        Height = 21
        Alignment = taRightJustify
        ReadOnly = True
        TabOrder = 5
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
    end
    object GroupBox1: TGroupBox
      Left = 1
      Top = 197
      Width = 1006
      Height = 44
      Align = alTop
      TabOrder = 3
      object dmkLabel5: TdmkLabel
        Left = 8
        Top = 20
        Width = 90
        Height = 13
        Caption = 'Vendedor (Cliente):'
        UpdType = utYes
        SQLType = stNil
      end
      object SpeedButton6: TSpeedButton
        Left = 964
        Top = 16
        Width = 21
        Height = 21
        Caption = '...'
        OnClick = SpeedButton6Click
      end
      object CBCliente: TdmkDBLookupComboBox
        Left = 164
        Top = 16
        Width = 797
        Height = 21
        KeyField = 'CodUsu'
        ListField = 'NOMEENT'
        ListSource = DmPediVda.DsClientes
        TabOrder = 1
        dmkEditCB = EdCliente
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdCliente: TdmkEditCB
        Left = 104
        Top = 16
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = EdPrdGrupTipChange
        DBLookupComboBox = CBCliente
        IgnoraDBLookupComboBox = False
      end
    end
  end
  object PainelDados: TPanel
    Left = 0
    Top = 48
    Width = 1008
    Height = 534
    Align = alClient
    Color = clAppWorkSpace
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    object PnCabeca: TPanel
      Left = 1
      Top = 1
      Width = 1006
      Height = 92
      Align = alTop
      BevelOuter = bvNone
      Enabled = False
      TabOrder = 1
      object Label1: TLabel
        Left = 8
        Top = 4
        Width = 14
        Height = 13
        Caption = 'ID:'
        FocusControl = DBEdCodigo
      end
      object Label2: TLabel
        Left = 152
        Top = 4
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
        FocusControl = DBEdNome
      end
      object Label3: TLabel
        Left = 68
        Top = 4
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
        FocusControl = DBEdit1
      end
      object Label4: TLabel
        Left = 588
        Top = 4
        Width = 44
        Height = 13
        Caption = 'Empresa:'
        FocusControl = DBEdit2
      end
      object Label5: TLabel
        Left = 8
        Top = 44
        Width = 128
        Height = 13
        Caption = 'Tipo de grupo de produtos:'
        FocusControl = DBEdit3
      end
      object Label6: TLabel
        Left = 336
        Top = 44
        Width = 90
        Height = 13
        Caption = 'Centro de estoque:'
        FocusControl = DBEdit4
      end
      object Label11: TLabel
        Left = 768
        Top = 44
        Width = 43
        Height = 13
        Caption = 'Abertura:'
      end
      object Label12: TLabel
        Left = 884
        Top = 44
        Width = 69
        Height = 13
        Caption = 'Encerramento:'
      end
      object DBEdCodigo: TDBEdit
        Left = 8
        Top = 20
        Width = 56
        Height = 21
        Hint = 'N'#186' do banco'
        TabStop = False
        DataField = 'Codigo'
        DataSource = DsFatConCad
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 0
      end
      object DBEdNome: TDBEdit
        Left = 152
        Top = 20
        Width = 432
        Height = 21
        Hint = 'Nome do banco'
        Color = clWhite
        DataField = 'Nome'
        DataSource = DsFatConCad
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
      end
      object DBEdit1: TDBEdit
        Left = 68
        Top = 20
        Width = 80
        Height = 21
        DataField = 'CodUsu'
        DataSource = DsFatConCad
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 2
      end
      object DBEdit2: TDBEdit
        Left = 588
        Top = 20
        Width = 408
        Height = 21
        DataField = 'NOMEFILIAL'
        DataSource = DsFatConCad
        TabOrder = 3
      end
      object DBEdit3: TDBEdit
        Left = 8
        Top = 60
        Width = 324
        Height = 21
        DataField = 'NOMEPRDGRUPTIP'
        DataSource = DsFatConCad
        TabOrder = 4
      end
      object DBEdit4: TDBEdit
        Left = 336
        Top = 60
        Width = 296
        Height = 21
        DataField = 'MOMESTQCENCAD'
        DataSource = DsFatConCad
        TabOrder = 5
      end
      object DBRadioGroup1: TDBRadioGroup
        Left = 636
        Top = 44
        Width = 128
        Height = 37
        Caption = ' Casas decimais: '
        Columns = 4
        DataField = 'CasasProd'
        DataSource = DsFatConCad
        Enabled = False
        Items.Strings = (
          '0'
          '1'
          '2'
          '3')
        ParentBackground = True
        TabOrder = 6
        Values.Strings = (
          '0'
          '1'
          '2'
          '3')
      end
      object DBEdit5: TDBEdit
        Left = 768
        Top = 60
        Width = 112
        Height = 21
        DataField = 'Abertura'
        DataSource = DsFatConCad
        TabOrder = 7
      end
      object DBEdit6: TDBEdit
        Left = 884
        Top = 60
        Width = 112
        Height = 21
        DataField = 'ENCERROU_TXT'
        DataSource = DsFatConCad
        TabOrder = 8
        OnChange = DBEdit6Change
      end
    end
    object PainelControle: TPanel
      Left = 1
      Top = 485
      Width = 1006
      Height = 48
      Align = alBottom
      TabOrder = 0
      object LaRegistro: TdmkLabel
        Left = 173
        Top = 1
        Width = 363
        Height = 46
        Align = alClient
        Caption = '[N]: 0'
        UpdType = utYes
        SQLType = stNil
        ExplicitWidth = 26
        ExplicitHeight = 13
      end
      object Panel5: TPanel
        Left = 1
        Top = 1
        Width = 172
        Height = 46
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object Panel3: TPanel
        Left = 536
        Top = 1
        Width = 469
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object BtItens: TBitBtn
          Left = 96
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Itens'
          Enabled = False
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtItensClick
        end
        object BtMovim: TBitBtn
          Left = 4
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Movim.'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtMovimClick
        end
        object Panel2: TPanel
          Left = 360
          Top = 0
          Width = 109
          Height = 46
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 2
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 90
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
      end
    end
    object Panel4: TPanel
      Left = 1
      Top = 341
      Width = 1006
      Height = 144
      Align = alBottom
      TabOrder = 2
      object dmkDBGrid1: TdmkDBGrid
        Left = 1
        Top = 1
        Width = 396
        Height = 142
        Align = alLeft
        Columns = <
          item
            Expanded = False
            FieldName = 'CU_NIVEL1'
            Title.Caption = 'C'#243'digo'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NO_NIVEL1'
            Title.Caption = 'Nome'
            Width = 217
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Valor'
            Width = 75
            Visible = True
          end>
        Color = clWindow
        DataSource = DsGrupos
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'CU_NIVEL1'
            Title.Caption = 'C'#243'digo'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NO_NIVEL1'
            Title.Caption = 'Nome'
            Width = 217
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Valor'
            Width = 75
            Visible = True
          end>
      end
      object PageControl1: TPageControl
        Left = 397
        Top = 1
        Width = 608
        Height = 142
        ActivePage = TabSheet10
        Align = alClient
        MultiLine = True
        TabOrder = 1
        object TabSheet5: TTabSheet
          Caption = ' Quantidade '
          ImageIndex = 2
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object GradeQ: TStringGrid
            Left = 0
            Top = 0
            Width = 600
            Height = 97
            Align = alClient
            ColCount = 2
            DefaultColWidth = 65
            DefaultRowHeight = 18
            FixedCols = 0
            RowCount = 2
            FixedRows = 0
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goTabs]
            ParentFont = False
            TabOrder = 0
            OnDrawCell = GradeQDrawCell
          end
          object StaticText1: TStaticText
            Left = 0
            Top = 97
            Width = 507
            Height = 17
            Align = alBottom
            Alignment = taCenter
            BorderStyle = sbsSunken
            Caption = 
              'D'#234' um duplo clique na c'#233'lula, coluna ou linha correspondente par' +
              'a alterar a quantidade.'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlue
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 1
            Visible = False
          end
        end
        object TabSheet4: TTabSheet
          Caption = ' Valor unit'#225'rio '
          ImageIndex = 5
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object GradeF: TStringGrid
            Left = 0
            Top = 0
            Width = 600
            Height = 114
            Align = alClient
            ColCount = 2
            DefaultColWidth = 65
            DefaultRowHeight = 18
            FixedCols = 0
            RowCount = 2
            FixedRows = 0
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goTabs]
            ParentFont = False
            TabOrder = 0
            OnDrawCell = GradeFDrawCell
          end
        end
        object TabSheet6: TTabSheet
          Caption = ' Desconto '
          ImageIndex = 6
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object GradeD: TStringGrid
            Left = 0
            Top = 0
            Width = 600
            Height = 97
            Align = alClient
            ColCount = 2
            DefaultColWidth = 65
            DefaultRowHeight = 18
            FixedCols = 0
            RowCount = 2
            FixedRows = 0
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goTabs]
            ParentFont = False
            TabOrder = 0
            OnDrawCell = GradeDDrawCell
          end
          object StaticText3: TStaticText
            Left = 0
            Top = 97
            Width = 544
            Height = 17
            Align = alBottom
            Alignment = taCenter
            BorderStyle = sbsSunken
            Caption = 
              'D'#234' um duplo clique na c'#233'lula, coluna ou linha correspondente par' +
              'a incluir / alterar o desconto.'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlue
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 1
          end
        end
        object TabSheet7: TTabSheet
          Caption = ' Valor l'#237'quido'
          ImageIndex = 1
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object GradeV: TStringGrid
            Left = 0
            Top = 0
            Width = 600
            Height = 114
            Align = alClient
            ColCount = 2
            DefaultColWidth = 65
            DefaultRowHeight = 18
            FixedCols = 0
            RowCount = 2
            FixedRows = 0
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goTabs]
            ParentFont = False
            TabOrder = 0
            OnDrawCell = GradeVDrawCell
          end
        end
        object TabSheet8: TTabSheet
          Caption = ' C'#243'digos '
          ImageIndex = 3
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object GradeC: TStringGrid
            Left = 0
            Top = 0
            Width = 600
            Height = 97
            Align = alClient
            ColCount = 1
            DefaultColWidth = 65
            DefaultRowHeight = 18
            FixedCols = 0
            RowCount = 1
            FixedRows = 0
            TabOrder = 0
            OnDrawCell = GradeCDrawCell
            RowHeights = (
              18)
          end
          object StaticText6: TStaticText
            Left = 0
            Top = 97
            Width = 502
            Height = 17
            Align = alBottom
            Alignment = taCenter
            BorderStyle = sbsSunken
            Caption = 
              'Para gerar o c'#243'digo, clique na c'#233'lula, coluna ou linha correspon' +
              'dente na guia "Ativos".'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlue
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 1
            Visible = False
          end
        end
        object TabSheet9: TTabSheet
          Caption = ' Ativos '
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object GradeA: TStringGrid
            Left = 0
            Top = 0
            Width = 600
            Height = 97
            Align = alClient
            ColCount = 2
            DefaultColWidth = 65
            DefaultRowHeight = 18
            FixedCols = 0
            RowCount = 2
            FixedRows = 0
            TabOrder = 0
            OnDrawCell = GradeADrawCell
            RowHeights = (
              18
              18)
          end
          object StaticText2: TStaticText
            Left = 0
            Top = 97
            Width = 475
            Height = 17
            Align = alBottom
            Alignment = taCenter
            BorderStyle = sbsSunken
            Caption = 
              'Clique na c'#233'lula, coluna ou linha correspondente para ativar / d' +
              'esativar o produto.'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlue
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 1
            Visible = False
          end
        end
        object TabSheet10: TTabSheet
          Caption = ' X '
          ImageIndex = 6
          object GradeX: TStringGrid
            Left = 0
            Top = 0
            Width = 600
            Height = 114
            Align = alClient
            ColCount = 2
            DefaultColWidth = 65
            DefaultRowHeight = 18
            FixedCols = 0
            RowCount = 2
            FixedRows = 0
            TabOrder = 0
            OnDrawCell = GradeXDrawCell
            RowHeights = (
              18
              18)
          end
        end
      end
    end
    object GroupBox2: TGroupBox
      Left = 1
      Top = 205
      Width = 1006
      Height = 36
      Align = alTop
      TabOrder = 3
      object Label21: TLabel
        Left = 8
        Top = 12
        Width = 81
        Height = 13
        Caption = 'Tabela de pre'#231'o:'
      end
      object Label23: TLabel
        Left = 720
        Top = 12
        Width = 36
        Height = 13
        Caption = 'Moeda:'
      end
      object DBEdit21: TDBEdit
        Left = 128
        Top = 8
        Width = 56
        Height = 21
        DataField = 'CODUSU_TPC'
        DataSource = DsFatConCad
        TabOrder = 0
      end
      object DBEdit24: TDBEdit
        Left = 188
        Top = 8
        Width = 517
        Height = 21
        DataField = 'NOMETABEPRCCAD'
        DataSource = DsFatConCad
        TabOrder = 1
      end
      object DBEdit18: TDBEdit
        Left = 760
        Top = 8
        Width = 44
        Height = 21
        DataField = 'CODUSU_MDA'
        DataSource = DsFatConCad
        TabOrder = 2
      end
      object DBEdit25: TDBEdit
        Left = 804
        Top = 8
        Width = 181
        Height = 21
        DataField = 'NOMEMOEDA'
        DataSource = DsFatConCad
        TabOrder = 3
      end
    end
    object GroupBox4: TGroupBox
      Left = 1
      Top = 93
      Width = 1006
      Height = 68
      Align = alTop
      Caption = 
        'Regra fiscal (precisa ser aplica'#231#227'o igual a "Condicional" e c'#225'lc' +
        'ulo diferente de "Nulo":'
      TabOrder = 4
      object Label24: TLabel
        Left = 12
        Top = 44
        Width = 55
        Height = 13
        Caption = 'Modelo NF:'
      end
      object Label26: TLabel
        Left = 12
        Top = 20
        Width = 73
        Height = 13
        Caption = 'Movimenta'#231#227'o:'
      end
      object Label27: TLabel
        Left = 600
        Top = 44
        Width = 55
        Height = 13
        Caption = 'Movimento:'
      end
      object Label28: TLabel
        Left = 764
        Top = 44
        Width = 38
        Height = 13
        Caption = 'C'#225'lculo:'
      end
      object Label29: TLabel
        Left = 920
        Top = 44
        Width = 27
        Height = 13
        Caption = 'Fator:'
      end
      object DBEdit8: TDBEdit
        Left = 88
        Top = 16
        Width = 57
        Height = 21
        DataField = 'CU_FisRegCad'
        DataSource = DsFatConCad
        TabOrder = 0
      end
      object DBEdit7: TDBEdit
        Left = 148
        Top = 16
        Width = 837
        Height = 21
        DataField = 'NO_FisRegCad'
        DataSource = DsFatConCad
        TabOrder = 1
      end
      object DBEdit9: TDBEdit
        Left = 656
        Top = 40
        Width = 95
        Height = 21
        TabStop = False
        DataField = 'NO_TipoMov'
        DataSource = DsFatConCad
        TabOrder = 2
      end
      object DBEdit10: TDBEdit
        Left = 808
        Top = 40
        Width = 108
        Height = 21
        TabStop = False
        DataField = 'NO_TipoCalc'
        DataSource = DsFatConCad
        TabOrder = 3
      end
      object DBEdit12: TDBEdit
        Left = 952
        Top = 40
        Width = 33
        Height = 21
        DataField = 'FATOR'
        DataSource = DsFatConCad
        TabOrder = 4
      end
      object DBEdit50: TDBEdit
        Left = 88
        Top = 40
        Width = 505
        Height = 21
        DataField = 'NOMEMODELONF'
        DataSource = DsFatConCad
        TabOrder = 5
      end
    end
    object GroupBox5: TGroupBox
      Left = 1
      Top = 161
      Width = 1006
      Height = 44
      Align = alTop
      TabOrder = 5
      object dmkLabel4: TdmkLabel
        Left = 8
        Top = 20
        Width = 90
        Height = 13
        Caption = 'Vendedor (Cliente):'
        UpdType = utYes
        SQLType = stNil
      end
      object DBEdit11: TDBEdit
        Left = 100
        Top = 16
        Width = 56
        Height = 21
        DataField = 'Cliente'
        DataSource = DsFatConCad
        TabOrder = 0
      end
      object DBEdit13: TDBEdit
        Left = 156
        Top = 16
        Width = 829
        Height = 21
        DataField = 'NO_CLIENTE'
        DataSource = DsFatConCad
        TabOrder = 1
      end
    end
    object GroupBox13: TGroupBox
      Left = 1
      Top = 241
      Width = 1006
      Height = 36
      Align = alTop
      TabOrder = 6
      object Label19: TLabel
        Left = 604
        Top = 12
        Width = 93
        Height = 13
        Caption = 'Quantidade pedida:'
        FocusControl = DBEdit14
      end
      object Label16: TLabel
        Left = 784
        Top = 12
        Width = 112
        Height = 13
        Caption = 'Valor l'#237'quido do pedido:'
        FocusControl = DBEdit15
      end
      object DBEdit14: TDBEdit
        Left = 700
        Top = 8
        Width = 77
        Height = 21
        DataField = 'QuantP'
        DataSource = DsFatConCad
        TabOrder = 0
      end
      object DBEdit15: TDBEdit
        Left = 904
        Top = 8
        Width = 80
        Height = 21
        DataField = 'ValLiq'
        DataSource = DsFatConCad
        TabOrder = 1
      end
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    Caption = 
      '                              Faturamento Condicional - Cadastro' +
      ' Retirada'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 1
    object Image1: TImage
      Left = 226
      Top = 1
      Width = 733
      Height = 46
      Align = alClient
      Transparent = True
      ExplicitLeft = 219
      ExplicitWidth = 781
    end
    object PanelFill002: TPanel
      Left = 1
      Top = 1
      Width = 225
      Height = 46
      Align = alLeft
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
      ParentColor = True
      ParentFont = False
      TabOrder = 0
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 3
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
        OnClick = SbImprimeClick
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 3
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 3
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 3
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 3
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_R: TGroupBox
      Left = 959
      Top = 1
      Width = 48
      Height = 46
      Align = alRight
      TabOrder = 1
      object ImgTipo: TdmkImage
        Left = 8
        Top = 6
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
  end
  object DsFatConCad: TDataSource
    DataSet = QrFatConCad
    Left = 68
    Top = 12
  end
  object QrFatConCad: TmySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrFatConCadBeforeOpen
    AfterOpen = QrFatConCadAfterOpen
    BeforeClose = QrFatConCadBeforeClose
    AfterScroll = QrFatConCadAfterScroll
    OnCalcFields = QrFatConCadCalcFields
    SQL.Strings = (
      'SELECT IF(fil.Tipo=0,fil.RazaoSocial,fil.Nome) NOMEFILIAL,'
      'IF(cli.Tipo=0,cli.RazaoSocial,cli.Nome) NO_CLIENTE,'
      'pgt.Nome NOMEPRDGRUPTIP, scc.Nome MOMESTQCENCAD,'
      'pem.BalQtdItem, pem.FatSemEstq, frc.Nome NO_FisRegCad,'
      'frc.CodUsu CU_FisRegCad, frm.TipoMov, '
      'ELT(frm.TipoMov+1,"Entrada","Saida","?") NO_TipoMov,'
      'frm.TipoCalc, ELT(frm.TipoCalc+1,"Nulo","Adiciona",'
      '"Subtrai","?") NO_TipoCalc, frm.GraCusPrc,'
      'imp.Nome NOMEMODELONF, tpc.CodUsu CODUSU_TPC,'
      'tpc.Nome NOMETABEPRCCAD, mda.CodUsu CODUSU_MDA,'
      'mda.Nome NOMEMOEDA, sbc.*'
      'FROM fatconcad sbc'
      'LEFT JOIN entidades fil  ON fil.Codigo=sbc.Empresa'
      'LEFT JOIN prdgruptip pgt ON pgt.Codigo=sbc.PrdGrupTip'
      'LEFT JOIN stqcencad scc  ON scc.Codigo=sbc.StqCenCad'
      'LEFT JOIN paramsemp pem  ON pem.Codigo=sbc.Empresa'
      'LEFT JOIN fisregcad frc ON frc.Codigo=sbc.FisRegCad'
      'LEFT JOIN entidades cli ON cli.Codigo=sbc.Cliente'
      'LEFT JOIN fisregmvt frm ON frm.Codigo=frc.Codigo'
      '     AND frm.TipoCalc > 0'
      '     AND frm.Empresa=sbc.Empresa'
      'LEFT JOIN imprime    imp ON imp.Codigo=frc.ModeloNF'
      'LEFT JOIN tabeprccab tpc ON tpc.Codigo=sbc.TabelaPrc'
      'LEFT JOIN cambiomda  mda ON mda.Codigo=sbc.Moeda'
      '     '
      'WHERE sbc.Codigo > -1000')
    Left = 40
    Top = 12
    object QrFatConCadCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'fatconcad.Codigo'
      Required = True
    end
    object QrFatConCadCodUsu: TIntegerField
      FieldName = 'CodUsu'
      Origin = 'fatconcad.CodUsu'
      Required = True
    end
    object QrFatConCadNome: TWideStringField
      FieldName = 'Nome'
      Origin = 'fatconcad.Nome'
      Required = True
      Size = 50
    end
    object QrFatConCadEmpresa: TIntegerField
      FieldName = 'Empresa'
      Origin = 'fatconcad.Empresa'
      Required = True
    end
    object QrFatConCadPrdGrupTip: TIntegerField
      FieldName = 'PrdGrupTip'
      Origin = 'fatconcad.PrdGrupTip'
      Required = True
    end
    object QrFatConCadStqCenCad: TIntegerField
      FieldName = 'StqCenCad'
      Origin = 'fatconcad.StqCenCad'
      Required = True
    end
    object QrFatConCadCasasProd: TSmallintField
      FieldName = 'CasasProd'
      Origin = 'fatconcad.CasasProd'
      Required = True
    end
    object QrFatConCadAbertura: TDateTimeField
      FieldName = 'Abertura'
      Origin = 'fatconcad.Abertura'
      Required = True
      DisplayFormat = 'dd/mm/yyyy hh:nn:ss'
    end
    object QrFatConCadEncerrou: TDateTimeField
      FieldName = 'Encerrou'
      Origin = 'fatconcad.Encerrou'
      Required = True
      DisplayFormat = 'dd/mm/yyyy hh:nn:ss'
    end
    object QrFatConCadNOMEFILIAL: TWideStringField
      FieldName = 'NOMEFILIAL'
      Size = 100
    end
    object QrFatConCadNOMEPRDGRUPTIP: TWideStringField
      FieldName = 'NOMEPRDGRUPTIP'
      Origin = 'prdgruptip.Nome'
      Size = 30
    end
    object QrFatConCadMOMESTQCENCAD: TWideStringField
      FieldName = 'MOMESTQCENCAD'
      Origin = 'stqcencad.Nome'
      Size = 50
    end
    object QrFatConCadBalQtdItem: TFloatField
      FieldName = 'BalQtdItem'
      Origin = 'paramsemp.BalQtdItem'
    end
    object QrFatConCadENCERROU_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'ENCERROU_TXT'
      Size = 50
      Calculated = True
    end
    object QrFatConCadFisRegCad: TIntegerField
      FieldName = 'FisRegCad'
      Origin = 'fatconcad.FisRegCad'
      Required = True
    end
    object QrFatConCadStatus: TSmallintField
      FieldName = 'Status'
      Origin = 'fatconcad.Status'
      Required = True
    end
    object QrFatConCadFatSemEstq: TSmallintField
      FieldName = 'FatSemEstq'
      Origin = 'paramsemp.FatSemEstq'
    end
    object QrFatConCadNO_FisRegCad: TWideStringField
      FieldName = 'NO_FisRegCad'
      Origin = 'fisregcad.Nome'
      Size = 50
    end
    object QrFatConCadCU_FisRegCad: TIntegerField
      FieldName = 'CU_FisRegCad'
      Origin = 'fisregcad.CodUsu'
      Required = True
    end
    object QrFatConCadTipoMov: TSmallintField
      FieldName = 'TipoMov'
      Origin = 'fisregmvt.TipoMov'
    end
    object QrFatConCadNO_TipoMov: TWideStringField
      FieldName = 'NO_TipoMov'
      Size = 7
    end
    object QrFatConCadTipoCalc: TSmallintField
      FieldName = 'TipoCalc'
      Origin = 'fisregmvt.TipoCalc'
    end
    object QrFatConCadNO_TipoCalc: TWideStringField
      FieldName = 'NO_TipoCalc'
      Size = 8
    end
    object QrFatConCadEntraSai: TSmallintField
      FieldName = 'EntraSai'
      Origin = 'fatconcad.EntraSai'
      Required = True
    end
    object QrFatConCadFATOR: TSmallintField
      FieldKind = fkCalculated
      FieldName = 'FATOR'
      Calculated = True
    end
    object QrFatConCadGraCusPrc: TIntegerField
      FieldName = 'GraCusPrc'
      Origin = 'fisregmvt.GraCusPrc'
    end
    object QrFatConCadCliente: TIntegerField
      FieldName = 'Cliente'
      Origin = 'fatconcad.Cliente'
    end
    object QrFatConCadNO_CLIENTE: TWideStringField
      FieldName = 'NO_CLIENTE'
      Size = 100
    end
    object QrFatConCadTabelaPrc: TIntegerField
      FieldName = 'TabelaPrc'
      Origin = 'fatconcad.TabelaPrc'
    end
    object QrFatConCadMoeda: TIntegerField
      FieldName = 'Moeda'
      Origin = 'fatconcad.Moeda'
    end
    object QrFatConCadNOMEMODELONF: TWideStringField
      FieldName = 'NOMEMODELONF'
      Origin = 'imprime.Nome'
      Size = 100
    end
    object QrFatConCadCODUSU_TPC: TIntegerField
      FieldName = 'CODUSU_TPC'
      Origin = 'tabeprccab.CodUsu'
      Required = True
    end
    object QrFatConCadCODUSU_MDA: TIntegerField
      FieldName = 'CODUSU_MDA'
      Origin = 'cambiomda.CodUsu'
      Required = True
    end
    object QrFatConCadNOMETABEPRCCAD: TWideStringField
      FieldName = 'NOMETABEPRCCAD'
      Origin = 'tabeprccab.Nome'
      Size = 50
    end
    object QrFatConCadNOMEMOEDA: TWideStringField
      FieldName = 'NOMEMOEDA'
      Origin = 'cambiomda.Nome'
      Size = 30
    end
    object QrFatConCadQuantP: TFloatField
      FieldName = 'QuantP'
      Origin = 'fatconcad.QuantP'
    end
    object QrFatConCadQuantC: TFloatField
      FieldName = 'QuantC'
      Origin = 'fatconcad.QuantC'
    end
    object QrFatConCadQuantV: TFloatField
      FieldName = 'QuantV'
      Origin = 'fatconcad.QuantV'
    end
    object QrFatConCadValLiq: TFloatField
      FieldName = 'ValLiq'
      Origin = 'fatconcad.ValLiq'
      DisplayFormat = '###,###,##0.00'
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = BtMovim
    CanUpd01 = BtItens
    Left = 96
    Top = 12
  end
  object PMMovim: TPopupMenu
    OnPopup = PMMovimPopup
    Left = 564
    Top = 492
    object Incluinovomovimento1: TMenuItem
      Caption = '&Inclui novo movimento'
      OnClick = Incluinovomovimento1Click
    end
    object Alteramovimentoatual1: TMenuItem
      Caption = '&Altera movimento atual'
      Enabled = False
      OnClick = Alteramovimentoatual1Click
    end
    object Encerramovimentoatual1: TMenuItem
      Caption = '&Encerra movimento atual'
      Enabled = False
      OnClick = Encerramovimentoatual1Click
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object EXcluimovimentoatual1: TMenuItem
      Caption = 'E&Xclui movimento atual'
      Enabled = False
    end
  end
  object DsFiliais: TDataSource
    DataSet = QrFiliais
    Left = 356
    Top = 40
  end
  object QrFiliais: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Filial, Codigo, '
      'IF(Tipo=0, RazaoSocial, Nome) NOMEFILIAL'
      'FROM entidades'
      'WHERE Codigo<-10'
      'ORDER BY NOMEFILIAL')
    Left = 328
    Top = 40
    object QrFiliaisFilial: TIntegerField
      FieldName = 'Filial'
    end
    object QrFiliaisCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrFiliaisNOMEFILIAL: TWideStringField
      FieldName = 'NOMEFILIAL'
      Required = True
      Size = 100
    end
  end
  object dmkValUsu1: TdmkValUsu
    dmkEditCB = EdFilial
    Panel = PainelEdita
    QryCampo = 'Empresa'
    UpdCampo = 'Empresa'
    RefCampo = 'Codigo'
    UpdType = utYes
    Left = 124
    Top = 12
  end
  object QrPrdGrupTip: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, CodUsu, Nome'
      'FROM prdgruptip'
      'ORDER BY Nome')
    Left = 272
    Top = 40
    object QrPrdGrupTipCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPrdGrupTipCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrPrdGrupTipNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
  end
  object DsPrdGrupTip: TDataSource
    DataSet = QrPrdGrupTip
    Left = 300
    Top = 40
  end
  object dmkValUsu2: TdmkValUsu
    dmkEditCB = EdPrdGrupTip
    Panel = PainelEdita
    QryCampo = 'PrdGrupTip'
    UpdCampo = 'PrdGrupTip'
    RefCampo = 'Codigo'
    UpdType = utYes
    Left = 152
    Top = 12
  end
  object dmkValUsu3: TdmkValUsu
    dmkEditCB = EdStqCenCad
    Panel = PainelEdita
    QryCampo = 'StqCenCad'
    UpdCampo = 'StqCenCad'
    RefCampo = 'Codigo'
    UpdType = utYes
    Left = 180
    Top = 12
  end
  object QrStqCenCad: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, CodUsu, Nome'
      'FROM stqcencad'
      'ORDER BY Nome')
    Left = 384
    Top = 40
    object QrStqCenCadCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrStqCenCadCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrStqCenCadNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsStqCenCad: TDataSource
    DataSet = QrStqCenCad
    Left = 412
    Top = 40
  end
  object QrNew: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo'
      'FROM fatconcad'
      'WHERE Encerrou=0'
      'AND Empresa=:P0'
      'AND PrdGrupTip=:P1'
      'AND StqCenCad=:P2'
      'ORDER BY Codigo')
    Left = 440
    Top = 40
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrNewCodigo: TIntegerField
      FieldName = 'Codigo'
    end
  end
  object QrSoma: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(Qtde) Qtde'
      'FROM stqmovitsa'
      'WHERE Empresa=:P0'
      'AND StqCenCad=:P1'
      'AND GraGruX=:P2'
      '')
    Left = 476
    Top = 40
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrSomaQtde: TFloatField
      FieldName = 'Qtde'
    end
  end
  object QrFisRegCad: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT frc.Codigo, frc.CodUsu, frc.Nome, frm.TipoMov,'
      'ELT(frm.TipoMov+1,"Entrada","Saida","?") NO_TipoMov, '
      'frm.TipoCalc, ELT(frm.TipoCalc+1,"Nulo","Adiciona",'
      '"Subtrai","?") NO_TipoCalc, imp.Nome NO_MODELO_NF'
      'FROM fisregmvt frm'
      'LEFT JOIN fisregcad frc ON frc.Codigo=frm.Codigo'
      'LEFT JOIN imprime imp ON imp.Codigo=frc.ModeloNF'
      'WHERE frc.Aplicacao=2'
      'AND frm.TipoCalc > 0'
      'AND frm.Empresa=:P0'
      'AND frm.StqCenCad=:P1'
      ''
      '')
    Left = 512
    Top = 40
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrFisRegCadCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrFisRegCadCodUsu: TIntegerField
      FieldName = 'CodUsu'
      Required = True
    end
    object QrFisRegCadNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
    object QrFisRegCadTipoMov: TSmallintField
      FieldName = 'TipoMov'
      Required = True
    end
    object QrFisRegCadNO_TipoMov: TWideStringField
      FieldName = 'NO_TipoMov'
      Size = 7
    end
    object QrFisRegCadTipoCalc: TSmallintField
      FieldName = 'TipoCalc'
      Required = True
    end
    object QrFisRegCadNO_TipoCalc: TWideStringField
      FieldName = 'NO_TipoCalc'
      Size = 8
    end
    object QrFisRegCadNO_MODELO_NF: TWideStringField
      FieldName = 'NO_MODELO_NF'
      Size = 100
    end
  end
  object DsFisRegCad: TDataSource
    DataSet = QrFisRegCad
    Left = 540
    Top = 40
  end
  object dmkValUsu4: TdmkValUsu
    dmkEditCB = EdFisRegCad
    Panel = PainelEdita
    QryCampo = 'FisRegCad'
    UpdCampo = 'FisRegCad'
    RefCampo = 'Codigo'
    UpdType = utYes
    Left = 208
    Top = 12
  end
  object QrGrupos: TmySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrGruposBeforeClose
    AfterScroll = QrGruposAfterScroll
    SQL.Strings = (
      'SELECT gg1.CodUsu CU_NIVEL1, gg1.Nome NO_NIVEL1,'
      'gg1.Nivel1, gti.Codigo GraTamCad, SUM(fti.ValLiq) Valor,'
      'fti.Controle ftiCtrl, pgt.Fracio'
      'FROM fatconits fti'
      'LEFT JOIN gragrux ggx ON ggx.Controle=fti.GraGruX'
      'LEFT JOIN gragru1   gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN gratamits gti ON gti.Controle=ggx.GraTamI'
      'LEFT JOIN prdgruptip   pgt ON pgt.Codigo=gg1.PrdGrupTip'
      'WHERE fti.Codigo=:P0'
      'GROUP BY ggx.GraGru1')
    Left = 648
    Top = 40
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrGruposCU_NIVEL1: TIntegerField
      FieldName = 'CU_NIVEL1'
    end
    object QrGruposNO_NIVEL1: TWideStringField
      FieldName = 'NO_NIVEL1'
      Size = 30
    end
    object QrGruposNivel1: TIntegerField
      FieldName = 'Nivel1'
    end
    object QrGruposGraTamCad: TIntegerField
      FieldName = 'GraTamCad'
    end
    object QrGruposValor: TFloatField
      FieldName = 'Valor'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrGruposftiCtrl: TIntegerField
      FieldName = 'ftiCtrl'
    end
    object QrGruposFracio: TSmallintField
      FieldName = 'Fracio'
    end
  end
  object DsGrupos: TDataSource
    DataSet = QrGrupos
    Left = 676
    Top = 40
  end
  object dmkValUsu5: TdmkValUsu
    dmkEditCB = EdCliente
    Panel = PainelEdita
    QryCampo = 'Cliente'
    UpdCampo = 'Cliente'
    RefCampo = 'Codigo'
    UpdType = utYes
    Left = 236
    Top = 12
  end
  object VuTabelaPrc: TdmkValUsu
    dmkEditCB = EdTabelaPrc
    Panel = PainelEdita
    QryCampo = 'TabelaPrc'
    UpdCampo = 'TabelaPrc'
    RefCampo = 'Codigo'
    UpdType = utYes
    Left = 264
    Top = 12
  end
  object VuCambioMda: TdmkValUsu
    dmkEditCB = EdMoeda
    Panel = PainelEdita
    QryCampo = 'Moeda'
    UpdCampo = 'Moeda'
    RefCampo = 'Codigo'
    UpdType = utYes
    Left = 292
    Top = 12
  end
  object PMItens: TPopupMenu
    Left = 656
    Top = 492
    object Incluinovositensdegrupo1: TMenuItem
      Caption = '&Inclui novos itens por &Grade'
      OnClick = Incluinovositensdegrupo1Click
    end
    object IncluinovositensporLeitura1: TMenuItem
      Caption = 'Inclui novos itens por &Leitura (ou exclui item de lista)'
      OnClick = IncluinovositensporLeitura1Click
    end
    object MenuItem1: TMenuItem
      Caption = '-'
      Visible = False
    end
    object AlteraExcluiIncluiitemselecionado1: TMenuItem
      Caption = '&Altera / Exclui / Inclui item selecionado'
      Visible = False
    end
  end
end
