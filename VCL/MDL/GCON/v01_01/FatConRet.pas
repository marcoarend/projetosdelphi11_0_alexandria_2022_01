unit FatConRet;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls, Db, (*DBTables,*) JPEG, ExtDlgs,
  ZCF2, ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, dmkGeral,
  dmkPermissoes, dmkEdit, dmkLabel, dmkRadioGroup, Grids, DBGrids, dmkDBGrid,
  Menus, dmkCheckGroup, dmkDBLookupComboBox, dmkEditCB, dmkValUsu, ComCtrls,
  dmkEditDateTimePicker, frxClass, frxDBSet, Variants, UnDmkProcFunc, dmkImage,
  UnDmkEnums, UnGrade_Jan;

type
  TFmFatConRet = class(TForm)
    PainelDados: TPanel;
    DsFatConRet: TDataSource;
    QrFatConRet: TmySQLQuery;
    PainelTitulo: TPanel;
    Image1: TImage;
    PanelFill2: TPanel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    PainelEdita: TPanel;
    PainelConfirma: TPanel;
    BtConfirma: TBitBtn;
    PainelControle: TPanel;
    LaRegistro: TdmkLabel;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    Panel3: TPanel;
    BtItens: TBitBtn;
    PainelEdit: TPanel;
    EdCodigo: TdmkEdit;
    PnCabeca: TPanel;
    Label1: TLabel;
    DBEdCodigo: TDBEdit;
    DBEdNome: TDBEdit;
    Label2: TLabel;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    EdNome: TdmkEdit;
    dmkPermissoes1: TdmkPermissoes;
    Label3: TLabel;
    DBEdit1: TDBEdit;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    EdCodUsu: TdmkEdit;
    PMMovim: TPopupMenu;
    BtMovim: TBitBtn;
    Panel4: TPanel;
    QrFatConRetCodigo: TIntegerField;
    QrFatConRetCodUsu: TIntegerField;
    QrFatConRetNome: TWideStringField;
    QrFatConRetEmpresa: TIntegerField;
    QrFatConRetStqCenCad: TIntegerField;
    QrFatConRetAbertura: TDateTimeField;
    QrFatConRetEncerrou: TDateTimeField;
    DsFiliais: TDataSource;
    QrFiliais: TmySQLQuery;
    QrFiliaisFilial: TIntegerField;
    QrFiliaisCodigo: TIntegerField;
    QrFiliaisNOMEFILIAL: TWideStringField;
    dmkLabel1: TdmkLabel;
    EdFilial: TdmkEditCB;
    CBFilial: TdmkDBLookupComboBox;
    dmkValUsu1: TdmkValUsu;
    QrFatConRetNOMEFILIAL: TWideStringField;
    Label4: TLabel;
    DBEdit2: TDBEdit;
    QrFatConRetMOMESTQCENCAD: TWideStringField;
    Label6: TLabel;
    DBEdit4: TDBEdit;
    EdStqCenCad: TdmkEditCB;
    dmkLabel3: TdmkLabel;
    CBStqCenCad: TdmkDBLookupComboBox;
    dmkValUsu3: TdmkValUsu;
    QrStqCenCad: TmySQLQuery;
    DsStqCenCad: TDataSource;
    QrStqCenCadCodigo: TIntegerField;
    QrStqCenCadCodUsu: TIntegerField;
    QrStqCenCadNome: TWideStringField;
    Label10: TLabel;
    EdAbertura: TdmkEdit;
    Label11: TLabel;
    DBEdit5: TDBEdit;
    QrFatConRetBalQtdItem: TFloatField;
    N1: TMenuItem;
    QrNew: TmySQLQuery;
    QrNewCodigo: TIntegerField;
    DBEdit6: TDBEdit;
    Label12: TLabel;
    QrFatConRetENCERROU_TXT: TWideStringField;
    QrSoma: TmySQLQuery;
    QrSomaQtde: TFloatField;
    Incluinovomovimento1: TMenuItem;
    Alteramovimentoatual1: TMenuItem;
    EXcluimovimentoatual1: TMenuItem;
    QrFisRegCad: TmySQLQuery;
    DsFisRegCad: TDataSource;
    dmkValUsu4: TdmkValUsu;
    QrFatConRetFisRegCad: TIntegerField;
    QrFisRegCadCodigo: TIntegerField;
    QrFisRegCadCodUsu: TIntegerField;
    QrFisRegCadNome: TWideStringField;
    QrFisRegCadTipoMov: TSmallintField;
    QrFisRegCadNO_TipoMov: TWideStringField;
    QrFisRegCadTipoCalc: TSmallintField;
    QrFisRegCadNO_TipoCalc: TWideStringField;
    QrFatConRetStatus: TSmallintField;
    QrGrupos: TmySQLQuery;
    DsGrupos: TDataSource;
    DBGGrupos: TdmkDBGrid;
    QrFatConRetFatSemEstq: TSmallintField;
    QrFatConRetNO_FisRegCad: TWideStringField;
    QrFatConRetCU_FisRegCad: TIntegerField;
    QrFatConRetTipoMov: TSmallintField;
    QrFatConRetNO_TipoMov: TWideStringField;
    QrFatConRetTipoCalc: TSmallintField;
    QrFatConRetNO_TipoCalc: TWideStringField;
    QrFatConRetEntraSai: TSmallintField;
    QrFatConRetFATOR: TSmallintField;
    QrFatConRetGraCusPrc: TIntegerField;
    QrFatConRetCliente: TIntegerField;
    dmkValUsu5: TdmkValUsu;
    QrFatConRetNO_CLIENTE: TWideStringField;
    GroupBox6: TGroupBox;
    Label33: TLabel;
    EdModeloNF: TdmkEdit;
    Label25: TLabel;
    EdFisRegCad: TdmkEditCB;
    CBFisRegCad: TdmkDBLookupComboBox;
    SpeedButton5: TSpeedButton;
    Label13: TLabel;
    EdDBTipoMov: TDBEdit;
    EdDBTipoCalc: TDBEdit;
    Label14: TLabel;
    Label15: TLabel;
    EdEntraSai: TdmkEdit;
    GroupBox1: TGroupBox;
    CBCliente: TdmkDBLookupComboBox;
    EdCliente: TdmkEditCB;
    dmkLabel5: TdmkLabel;
    QrFisRegCadNO_MODELO_NF: TWideStringField;
    GroupBox4: TGroupBox;
    Label24: TLabel;
    Label26: TLabel;
    Label27: TLabel;
    Label28: TLabel;
    Label29: TLabel;
    GroupBox5: TGroupBox;
    DBEdit8: TDBEdit;
    DBEdit7: TDBEdit;
    DBEdit9: TDBEdit;
    DBEdit10: TDBEdit;
    DBEdit12: TDBEdit;
    DBEdit50: TDBEdit;
    DBEdit11: TDBEdit;
    DBEdit13: TDBEdit;
    QrFatConRetNOMEMODELONF: TWideStringField;
    PMItens: TPopupMenu;
    QrGruposCU_NIVEL1: TIntegerField;
    QrGruposNO_NIVEL1: TWideStringField;
    QrGruposNivel1: TIntegerField;
    QrGruposGraTamCad: TIntegerField;
    QrGruposValor: TFloatField;
    QrGruposftiCtrl: TIntegerField;
    PageControl1: TPageControl;
    TabSheet5: TTabSheet;
    GradeQ: TStringGrid;
    StaticText1: TStaticText;
    TabSheet4: TTabSheet;
    GradeF: TStringGrid;
    TabSheet6: TTabSheet;
    GradeD: TStringGrid;
    StaticText3: TStaticText;
    TabSheet7: TTabSheet;
    GradeV: TStringGrid;
    TabSheet8: TTabSheet;
    GradeC: TStringGrid;
    StaticText6: TStaticText;
    TabSheet9: TTabSheet;
    GradeA: TStringGrid;
    StaticText2: TStaticText;
    TabSheet10: TTabSheet;
    GradeX: TStringGrid;
    QrFatConRetQuantP: TFloatField;
    QrFatConRetQuantC: TFloatField;
    QrFatConRetQuantV: TFloatField;
    QrFatConRetValLiq: TFloatField;
    QrGruposFracio: TSmallintField;
    SpeedButton6: TSpeedButton;
    DBEdit15: TDBEdit;
    Label16: TLabel;
    DBEdit14: TDBEdit;
    Label19: TLabel;
    QrFatConLot: TmySQLQuery;
    DsFatConLot: TDataSource;
    BtLotes: TBitBtn;
    PMLotes: TPopupMenu;
    Incluinovolote1: TMenuItem;
    Retiraloteatual1: TMenuItem;
    QrDisponiv: TmySQLQuery;
    QrDisponivCodigo: TIntegerField;
    QrDisponivCodUsu: TIntegerField;
    QrDisponivNome: TWideStringField;
    QrDisponivAbertura: TDateTimeField;
    QrDisponivEncerrou: TDateTimeField;
    QrFatConLotCodigo: TIntegerField;
    QrFatConLotCodUsu: TIntegerField;
    QrFatConLotNome: TWideStringField;
    QrFatConLotEmpresa: TIntegerField;
    QrFatConLotCliente: TIntegerField;
    QrFatConLotPrdGrupTip: TIntegerField;
    QrFatConLotStqCenCad: TIntegerField;
    QrFatConLotFisRegCad: TIntegerField;
    QrFatConLotEntraSai: TSmallintField;
    QrFatConLotCasasProd: TSmallintField;
    QrFatConLotAbertura: TDateTimeField;
    QrFatConLotEncerrou: TDateTimeField;
    QrFatConLotStatus: TSmallintField;
    QrFatConLotTabelaPrc: TIntegerField;
    QrFatConLotMoeda: TIntegerField;
    QrFatConLotQuantP: TFloatField;
    QrFatConLotQuantC: TFloatField;
    QrFatConLotQuantV: TFloatField;
    QrFatConLotValLiq: TFloatField;
    QrFatConLotLk: TIntegerField;
    QrFatConLotDataCad: TDateField;
    QrFatConLotDataAlt: TDateField;
    QrFatConLotUserCad: TIntegerField;
    QrFatConLotUserAlt: TIntegerField;
    QrFatConLotAlterWeb: TSmallintField;
    QrFatConLotAtivo: TSmallintField;
    QrFatConLotFatConRet: TIntegerField;
    Splitter1: TSplitter;
    RetornaitensporGrade1: TMenuItem;
    RetornaitensporLeitura1: TMenuItem;
    QrGruposQuantV: TFloatField;
    QrGruposFatConCad: TIntegerField;
    QrFatConRetModeloNF: TIntegerField;
    QrFatConRetFilial: TIntegerField;
    QrFatConRetAssociada: TIntegerField;
    QrFatConRetEMP_CtaServico: TIntegerField;
    QrFatConRetEMP_FaturaSeq: TSmallintField;
    QrFatConRetEMP_FaturaSep: TWideStringField;
    QrFatConRetEMP_FaturaDta: TSmallintField;
    QrFatConRetEMP_TxtServico: TWideStringField;
    QrFatConRetEMP_DupServico: TWideStringField;
    QrFatConRetEMP_FILIAL: TIntegerField;
    QrFatConRetASS_FILIAL: TIntegerField;
    QrFatConRetASS_CO_UF: TLargeintField;
    QrFatConRetASS_CtaServico: TIntegerField;
    QrFatConRetASS_FaturaSeq: TSmallintField;
    QrFatConRetASS_FaturaSep: TWideStringField;
    QrFatConRetASS_FaturaDta: TSmallintField;
    QrFatConRetASS_TxtServico: TWideStringField;
    QrFatConRetASS_DupServico: TWideStringField;
    DBEdit3: TDBEdit;
    QrFatConRetNO_CARTEMIS: TWideStringField;
    QrFatConRetCartEmis: TIntegerField;
    DBEdit16: TDBEdit;
    QrFatConRetTIPOCART: TIntegerField;
    CBCartEmis: TdmkDBLookupComboBox;
    SpeedButton13: TSpeedButton;
    EdCartEmis: TdmkEditCB;
    Label5: TLabel;
    Panel6: TPanel;
    DBGrid1: TDBGrid;
    DBGrid2: TDBGrid;
    DsEmissC: TDataSource;
    QrEmissC: TmySQLQuery;
    QrEmissCControle: TFloatField;
    QrEmissCVencimento: TDateField;
    QrEmissCData: TDateField;
    QrEmissCCarteira: TIntegerField;
    QrEmissCNOMECARTEIRA: TWideStringField;
    QrEmissCSEQ: TIntegerField;
    QrEmissCVENCIMENTO_TXT: TWideStringField;
    QrEmissCCompensado: TDateField;
    QrEmissCCOMPENSADO_TXT: TWideStringField;
    BtPagtos: TBitBtn;
    PMPagtos: TPopupMenu;
    Incluinovospagamentos1: TMenuItem;
    Alterapagamentoatual1: TMenuItem;
    N2: TMenuItem;
    Excluipagamentoatual1: TMenuItem;
    Excluitodospagamentos1: TMenuItem;
    QrEmissCFatParcela: TIntegerField;
    EdComisPer: TdmkEdit;
    Label18: TLabel;
    EdDescoGer: TdmkEdit;
    Label20: TLabel;
    QrFatConRetComisPer: TFloatField;
    QrFatConRetComisVal: TFloatField;
    QrFatConRetDescoGer: TFloatField;
    QrFatConRetValorRec: TFloatField;
    QrFatConRetValFat: TFloatField;
    DBEdit17: TDBEdit;
    DBEdit18: TDBEdit;
    Label17: TLabel;
    DBEdit19: TDBEdit;
    DBEdValorPen: TDBEdit;
    Label30: TLabel;
    DBEdit21: TDBEdit;
    Label31: TLabel;
    QrFatConRetVALORPEN: TFloatField;
    N3: TMenuItem;
    Atualizaclculos1: TMenuItem;
    QrEmissCVALOR: TFloatField;
    QrEmissCSerieCH: TWideStringField;
    QrEmissCDocumento: TFloatField;
    Encerraacertoatual1: TMenuItem;
    QrFatConRetDESCOPER: TFloatField;
    Label32: TLabel;
    EdPercRedu: TdmkEdit;
    QrFatConRetPercRedu: TFloatField;
    DBEdit20: TDBEdit;
    dmkLabel2: TdmkLabel;
    Label21: TLabel;
    Label22: TLabel;
    Label23: TLabel;
    Label34: TLabel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrFatConRetAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrFatConRetBeforeOpen(DataSet: TDataSet);
    procedure EdCodUsuKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SbNovoClick(Sender: TObject);
    procedure BtMovimClick(Sender: TObject);
    procedure PMMovimPopup(Sender: TObject);
    procedure QrFatConRetBeforeClose(DataSet: TDataSet);
    procedure QrFatConRetAfterScroll(DataSet: TDataSet);
    procedure BtItensClick(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
    procedure QrFatConRetCalcFields(DataSet: TDataSet);
    procedure DBEdit6Change(Sender: TObject);
    procedure Incluinovomovimento1Click(Sender: TObject);
    procedure Alteramovimentoatual1Click(Sender: TObject);
    procedure EdFisRegCadChange(Sender: TObject);
    procedure EdStqCenCadChange(Sender: TObject);
    procedure EdFilialChange(Sender: TObject);
    procedure GradeQDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect;
      State: TGridDrawState);
    procedure GradeCDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect;
      State: TGridDrawState);
    procedure GradeADrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect;
      State: TGridDrawState);
    procedure QrGruposAfterScroll(DataSet: TDataSet);
    procedure SpeedButton5Click(Sender: TObject);
    procedure QrGruposBeforeClose(DataSet: TDataSet);
    procedure EdFisRegCadExit(Sender: TObject);
    procedure GradeFDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect;
      State: TGridDrawState);
    procedure GradeDDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect;
      State: TGridDrawState);
    procedure GradeVDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect;
      State: TGridDrawState);
    procedure GradeXDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect;
      State: TGridDrawState);
    procedure SpeedButton6Click(Sender: TObject);
    procedure QrFatConLotAfterScroll(DataSet: TDataSet);
    procedure QrFatConLotBeforeClose(DataSet: TDataSet);
    procedure BtLotesClick(Sender: TObject);
    procedure Incluinovolote1Click(Sender: TObject);
    procedure QrGruposAfterOpen(DataSet: TDataSet);
    procedure RetornaitensporGrade1Click(Sender: TObject);
    procedure RetornaitensporLeitura1Click(Sender: TObject);
    procedure QrEmissCCalcFields(DataSet: TDataSet);
    procedure BtPagtosClick(Sender: TObject);
    procedure Incluinovospagamentos1Click(Sender: TObject);
    procedure Alterapagamentoatual1Click(Sender: TObject);
    procedure Excluipagamentoatual1Click(Sender: TObject);
    procedure Excluitodospagamentos1Click(Sender: TObject);
    procedure SpeedButton13Click(Sender: TObject);
    procedure Atualizaclculos1Click(Sender: TObject);
    procedure Encerraacertoatual1Click(Sender: TObject);
    procedure Encerrafaturamento1Click(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
   ////Procedures do form
    procedure MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo: Integer);
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    //
    procedure ReconfiguraGradeQ();
    //
    procedure MostraFatConRetGru(SQLType: TSQLType);
    procedure MostraFatConRetLei(SQLType: TSQLType);
    //
    //  Pagamentos
    procedure ExcluiPagto();
    procedure ExcluiTodosPagtos();
    procedure DefineVarDup();
    procedure ReopenPagtos(FatParcela: Integer);
    //
    procedure RecalculaTodoFatConRet(FatConRet: Integer);
  public
    { Public declarations }
    procedure LocCod(Atual, Codigo: Integer);
    procedure ReopenFisRegCad();
    procedure ReopenFatConLot(Codigo: Integer);
    procedure ReopenGrupos(Nivel1: Integer);
    procedure AtualizaItensFaturados(FatConcad: Integer);

  end;

var
  FmFatConRet: TFmFatConRet;

const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, MyDBCheck, MasterSelFilial, ModuleGeral, ModProd, MyVCLSkin,
Principal, FatPedImp, ModPediVda, Entidade2, CambioMda, TabePrcCab, FatConLot,
FatConRetGru, FatConRetLei, (*NFaEdit,*) UnInternalConsts3, (*UCashier, *)Carteiras;

{$R *.DFM}

const
  FThisFatID = 004; // condicional

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmFatConRet.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmFatConRet.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrFatConRetCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmFatConRet.DefParams;
begin
  VAR_GOTOTABELA := 'FatConRet';
  VAR_GOTOMYSQLTABLE := QrFatConRet;
  VAR_GOTONEG := gotoAll;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT IF(emp.Tipo=0,emp.RazaoSocial,emp.Nome) NOMEFILIAL,');
  VAR_SQLx.Add('emp.Filial, IF(cli.Tipo=0,cli.RazaoSocial,cli.Nome) NO_CLIENTE,');
  VAR_SQLx.Add('par.BalQtdItem, par.FatSemEstq, frc.Nome NO_FisRegCad,');
  VAR_SQLx.Add('frc.ModeloNF, frc.CodUsu CU_FisRegCad, frm.TipoMov,');
  VAR_SQLx.Add('ELT(frm.TipoMov+1,"Entrada","Saida","?") NO_TipoMov,');
  VAR_SQLx.Add('frm.TipoCalc, ELT(frm.TipoCalc+1,"Nulo","Adiciona",');
  VAR_SQLx.Add('"Subtrai","?") NO_TipoCalc, frm.GraCusPrc,');
  VAR_SQLx.Add('imp.Nome NOMEMODELONF, scc.Nome MOMESTQCENCAD,');
  VAR_SQLx.Add('');
  VAR_SQLx.Add('par.Associada,');
  VAR_SQLx.Add('par.CtaServico EMP_CtaServico,');
  VAR_SQLx.Add('par.FaturaSeq EMP_FaturaSeq,');
  VAR_SQLx.Add('par.FaturaSep EMP_FaturaSep,');
  VAR_SQLx.Add('par.FaturaDta EMP_FaturaDta,');
  VAR_SQLx.Add('par.TxtServico EMP_TxtServico,');
  VAR_SQLx.Add('par.DupServico  EMP_DupServico,');
  VAR_SQLx.Add('emp.Filial EMP_FILIAL,');
  VAR_SQLx.Add('');
  VAR_SQLx.Add('ase.Filial ASS_FILIAL,');
  VAR_SQLx.Add('IF(ase.Tipo=0, ase.EUF, ase.PUF) ASS_CO_UF,');
  VAR_SQLx.Add('');
  VAR_SQLx.Add('ass.CtaServico ASS_CtaServico,');
  VAR_SQLx.Add('ass.FaturaSeq ASS_FaturaSeq,');
  VAR_SQLx.Add('ass.FaturaSep ASS_FaturaSep,');
  VAR_SQLx.Add('ass.FaturaDta ASS_FaturaDta,');
  VAR_SQLx.Add('ass.TxtServico ASS_TxtServico,');
  VAR_SQLx.Add('ass.DupServico  ASS_DupServico,');
  VAR_SQLx.Add('');
  VAR_SQLx.Add('car.Nome NO_CARTEMIS, car.Tipo TIPOCART, sbc.*');
  VAR_SQLx.Add('');
  VAR_SQLx.Add('FROM fatconret sbc');
  VAR_SQLx.Add('LEFT JOIN entidades emp ON emp.Codigo=sbc.Empresa');
  VAR_SQLx.Add('LEFT JOIN stqcencad scc ON scc.Codigo=sbc.StqCenCad');
  VAR_SQLx.Add('LEFT JOIN paramsemp par ON par.Codigo=sbc.Empresa');
  VAR_SQLx.Add('LEFT JOIN paramsemp ass ON ass.Codigo=par.Associada');
  VAR_SQLx.Add('LEFT JOIN entidades ase ON ase.Codigo=ass.Codigo');
  VAR_SQLx.Add('LEFT JOIN fisregcad frc ON frc.Codigo=sbc.FisRegCad');
  VAR_SQLx.Add('LEFT JOIN entidades cli ON cli.Codigo=sbc.Cliente');
  VAR_SQLx.Add('LEFT JOIN fisregmvt frm ON frm.Codigo=frc.Codigo');
  VAR_SQLx.Add('     AND frm.TipoCalc > 0');
  VAR_SQLx.Add('     AND frm.Empresa=sbc.Empresa');
  VAR_SQLx.Add('LEFT JOIN imprime    imp ON imp.Codigo=frc.ModeloNF');
  VAR_SQLx.Add('LEFT JOIN carteiras  car ON car.Codigo=sbc.CartEmis');
  VAR_SQLx.Add('     ');
  VAR_SQLx.Add('WHERE sbc.Codigo > -1000');
  //
  VAR_SQL1.Add('AND sbc.Codigo=:P0');
  //
  VAR_SQL2.Add('AND sbc.CodUsu=:P0');
  //
  VAR_SQLa.Add('AND sbc.Nome Like :P0');
  //
end;

procedure TFmFatConRet.EdCodUsuKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key = VK_F4) and (ImgTipo.SQLType = stIns) then
    EdCodUsu.ValueVariant :=
    UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, 'FatConRet', 'CodUsu', [], [],
    stIns, 0, siPositivo, EdCodUsu);
end;

procedure TFmFatConRet.EdFilialChange(Sender: TObject);
begin
  ReopenFisRegCad();
end;

procedure TFmFatConRet.EdFisRegCadChange(Sender: TObject);
var
  FisRegCad: Integer;
begin
  FisRegCad := Geral.IMV(EdFisRegCad.Text);
  //
  if FisRegCad <> 0 then
  begin
    EdDBTipoMov.DataSource := DsFisRegCad;
    EdDBTipoMov.DataSource := DsFisRegCad;
    if ImgTipo.SQLType = stIns then
    begin
      case QrFisRegCadTipoCalc.Value of
        1: EdEntraSai.ValueVariant := 1;
        2: EdEntraSai.ValueVariant := -1;
        else EdEntraSai.ValueVariant := 0;
      end;
    end;
  end else begin
    EdDBTipoMov.DataSource := nil;
    EdDBTipoMov.DataSource := nil;
    EdEntraSai.ValueVariant := 0;
  end;
  //
  EdModeloNF.Text := '';
  if not EdFisRegCad.Focused then
    EdModeloNF.Text := QrFisRegCadNO_MODELO_NF.Value;
end;

procedure TFmFatConRet.EdFisRegCadExit(Sender: TObject);
begin
  EdModeloNF.Text := QrFisRegCadNO_MODELO_NF.Value;
end;

procedure TFmFatConRet.EdStqCenCadChange(Sender: TObject);
begin
  ReopenFisRegCad();
end;

procedure TFmFatConRet.Excluipagamentoatual1Click(Sender: TObject);
begin
  ExcluiPagto();
end;

procedure TFmFatConRet.Excluitodospagamentos1Click(Sender: TObject);
begin
  ExcluiTodosPagtos();
end;

procedure TFmFatConRet.Encerraacertoatual1Click(Sender: TObject);
var
  Codigo: Integer;
  Agora: String;
begin
  if Geral.MensagemBox('Confirma o encerramento do acerto condicional atual?',
  'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
  begin
    Screen.Cursor := crHourGlass;
    try
      Agora := Geral.FDT(DModG.ObtemAgora(), 105);
      Codigo := QrFatConRetCodigo.Value;
      //
      // Ativa itens no movimento
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('UPDATE stqmovitsa SET Ativo=1');
      Dmod.QrUpd.SQL.Add('WHERE Tipo=:P0' );
      Dmod.QrUpd.SQL.Add('AND OriCodi=:P1 ');
      Dmod.QrUpd.Params[00].AsInteger := FThisFatID;
      Dmod.QrUpd.Params[01].AsInteger := Codigo;
      Dmod.QrUpd.ExecSQL;
      //
      // Encerra definivamente movimento manual
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('UPDATE fatconret SET ');
      Dmod.QrUpd.SQL.Add('Status=9, Encerrou=:P0');
      Dmod.QrUpd.SQL.Add('WHERE Codigo=:P1 ');
      Dmod.QrUpd.Params[00].AsString  := Agora;
      Dmod.QrUpd.Params[01].AsInteger := Codigo;
      Dmod.QrUpd.ExecSQL;
      //
      {}
      LocCod(Codigo, Codigo);
    finally
      Screen.Cursor := crDefault;
    end;
  end;
end;

procedure TFmFatConRet.Encerrafaturamento1Click(Sender: TObject);
begin

end;

{
procedure TFmFatConRet.EncerraFaturamento();
var
  Especie, SerieNFTxt: String;
  NumeroNF, Quantidade: Integer;
  //
  //IDCtrl, ID, SeqInReduz,
  Tipo, OriCodi, OriCtrl, OriCnta, Empresa, Associada, Cliente, RegrFiscal,
  StqCenCad, FatSemEstq, GraGruX, InfAdCuztm, Preco_MedOrdem, TipoNF, modNF,
  Serie, nNF, SitDevolu, Servico, AFP_Sit, Cli_Tipo, Cli_UF, EMP_UF, EMP_FILIAL,
  ASS_CO_UF, ASS_FILIAL, Item_MadeBy, PediVda, OriPart: Integer;
  Qtde, Preco_PrecoF, Total, Preco_PercCustom, Preco_MedidaC, Preco_MedidaL,
  Preco_MedidaA, Preco_MedidaE, AFP_Per, Item_IPI_ALq, TotalPreCalc: Double;
  Cli_IE, refNFe, CFOP, NO_tablaPrc: String;
begin
  if Trim(DBEdValorPen.Text) <> '' then
  begin
    Geral.MensagemBox(PChar('Encerramento cancelado! Existe uma diverg�ncia de '
    + DMod.QrControleMoeda.Value + ' ' + DBEdValorPen.Text + '.'),
    'Aviso', MB_OK+MB_ICONWARNING);
    Exit;
  end;
  //
  if DBCheck.CriaFm(TFmNFaEdit, FmNFaEdit, afmoNegarComAviso) then
  begin
    //DmPediVda.ReopenFatPedCab(QrFatPedCabCodigo.Value, True);
    // Configura��o do tipo
    FmNFaEdit.F_Tipo            := FThisFatID; // 004;
    FmNFaEdit.F_OriCodi         := QrFatConRetCodigo        .Value;
    // Integer
    FmNFaEdit.F_Empresa         := QrFatConRetEmpresa       .Value;
    FmNFaEdit.F_ModeloNF        := QrFatConRetModeloNF      .Value;
    FmNFaEdit.F_Cliente         := QrFatConRetCliente       .Value;
    FmNFaEdit.F_EMP_FILIAL      := QrFatConRetFilial        .Value;
    FmNFaEdit.F_AFP_Sit         := 0;//QrFatConRetAFP_Sit       .Value;
    FmNFaEdit.F_Associada       := QrFatConRetAssociada     .Value;
    FmNFaEdit.F_ASS_FILIAL      := QrFatConRetASS_FILIAL    .Value;
    FmNFaEdit.F_EMP_CtaFaturas  := QrFatConRetEMP_CtaServico.Value;
    FmNFaEdit.F_ASS_CtaFaturas  := QrFatConRetASS_CtaServico.Value;

    FmNFaEdit.F_CartEmis        := QrFatConRetCartEmis      .Value;
    FmNFaEdit.F_CondicaoPG      := -1;//QrFatConRetCondicaoPG    .Value;
    FmNFaEdit.F_EMP_FaturaDta   := QrFatConRetEMP_FaturaDta .Value;
    FmNFaEdit.F_EMP_IDDuplicata := QrFatConRetCodigo        .Value;
    FmNFaEdit.F_EMP_FaturaSeq   := QrFatConRetEMP_FaturaSeq .Value;
    FmNFaEdit.F_TIPOCART        := QrFatConRetTIPOCART      .Value;
    FmNFaEdit.F_Represen        := 0;//QrFatConRetRepresen      .Value;
    FmNFaEdit.F_ASS_IDDuplicata := QrFatConRetCodigo        .Value;
    FmNFaEdit.F_ASS_FaturaSeq   := QrFatConRetASS_FaturaSeq .Value;
    // String
    FmNFaEdit.F_EMP_FaturaSep   := QrFatConRetEMP_FaturaSep .Value;
    FmNFaEdit.F_EMP_TxtFaturas  := QrFatConRetEMP_TxtServico.Value;
    FmNFaEdit.F_EMP_TpDuplicata := QrFatConRetEMP_DupServico.Value;
    FmNFaEdit.F_ASS_FaturaSep   := QrFatConRetASS_FaturaSep .Value;
    FmNFaEdit.F_ASS_TxtFaturas  := QrFatConRetASS_TxtServico.Value;
    FmNFaEdit.F_ASS_TpDuplicata := QrFatConRetASS_DupServico.Value;
    // Double
    FmNFaEdit.F_AFP_Per         := 0;//QrFatConRetAFP_Per       .Value;
    // TDateTime
    FmNFaEdit.F_Abertura        := QrFatConRetAbertura      .Value;
    //
    FmNFaEdit.ReopenFatPedNFs(1,0);
    SerieNFTxt := IntToStr(FmNFaEdit.QrImprimeNO_SerieNF.Value);
    NumeroNF   := DModG.BuscaProximoCodigoInt('paramsnfs', 'Sequencial',
    'WHERE Controle=' + dmkPF.FFP(FmNFaEdit.QrImprimeCtrl_nfs.Value, 0), 0,
    FmNFaEdit.QrImprimeMaxSeqLib.Value, 'S�rie: ' + SerieNFTxt + sLineBreak +
    'Filial: ' + dmkPF.FFP(FmNFaEdit.QrFatPedNFsFilial.Value, 0));

    FmNFaEdit.EdNumeroNF.ValueVariant := NumeroNF;
    Especie := '';
    Quantidade := 0;
    //
    (*
    QrVolumes.Close;
    QrVolumes.Params[00].AsInteger := QrFatPedCabCodigo.Value;
    QrVolumes.Open;
    QrVolumes.First;
    while not QrVolumes.Eof do
    begin
      Quantidade := Quantidade + QrVolumesVolumes.Value;
      if Especie <> '' then
        Especie := Especie + ' + ';
      Especie := Especie + ' ' +
        IntToStr(QrVolumesVolumes.Value) +
        QrVolumesNO_UnidMed.Value;
      //
      QrVolumes.Next;
    end;
    *)
    FmNFaEdit.EdQuantidade.ValueVariant := FloatToStr(Quantidade);
    FmNFaEdit.EdEspecie.ValueVariant := Especie;
    //
    FmNFaEdit.EdNumeroNF.Enabled := (FmNFaEdit.QrImprimeIncSeqAuto.Value = 0);
    //
    (*
    FmNFaEdit.QrCFOP.Close;
    FmNFaEdit.QrCFOP.Params[00].AsInteger := FThisFatID;
    FmNFaEdit.QrCFOP.Params[00].AsInteger := FmFatPedCab.QrFatPedCabCodigo.Value;
    FmNFaEdit.QrCFOP.Params[01].AsInteger := FmFatPedCab.QrFatPedCabEmpresa.Value;
    FmNFaEdit.QrCFOP.Open;
    FmNFaEdit.EdCFOP1.Text := FmNFaEdit.QrCFOPCFOP.Value;
    FmNFaEdit.CBCFOP1.KeyValue := FmNFaEdit.QrCFOPCFOP.Value;
    *)
    //
    FmNFaEdit.ReopenStqMovValA();
    FmNFaEdit.ImgTipo.SQLType := stIns;
    FmNFaEdit.ShowModal;
    FmNFaEdit.Destroy;
    // Reabrir de novo!
    LocCod(QrFatConRetCodigo.Value, QrFatConRetCodigo.Value);
    //
(*
    if QrFatPedCabEncerrou.Value > 0 then
    begin
      if DBCheck.CriaFm(TFmFatPedNFs, FmFatPedNFs, afmoNegarComAviso) then
      begin
        FmFatPedNFs.EdFilial.ValueVariant  := QrFatPedCabEMP_FILIAL.Value;
        FmFatPedNFs.CBFilial.KeyValue      := QrFatPedCabEMP_FILIAL.Value;
        FmFatPedNFs.EdCliente.ValueVariant := QrFatPedCabCliente.Value;
        FmFatPedNFs.CBCliente.KeyValue     := QrFatPedCabCliente.Value;
        FmFatPedNFs.EdPedido.ValueVariant  := QrFatPedCabCU_PediVda.Value;
        FmFatPedNFs.ShowModal;
        FmFatPedNFs.Destroy;
      end;
    end;
*)
  end;
end;
}

procedure TFmFatConRet.MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo: Integer);
begin
  case Mostra of
    0:
    begin
      PainelControle.Visible:=True;
      PainelDados.Visible := True;
      PainelEdita.Visible := False;
    end;
    else Application.MessageBox('A��o de Inclus�o/altera��o n�o definida!',
    'Aviso', MB_OK+MB_ICONWARNING);
  end;
  ImgTipo.SQLType := SQLType;
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmFatConRet.MostraFatConRetGru(SQLType: TSQLType);
begin
  DmodG.ReopenParamsEmp(QrFatConRetEmpresa.Value);
  if DBCheck.CriaFm(TFmFatConRetGru, FmFatConRetGru, afmoNegarComAviso) then
  begin
    FmFatConRetGru.ImgTipo.SQLType := SQLType;
    if SQLType = stUpd then
    begin
      FmFatConRetGru.PnSeleciona.Enabled    := False;
    end;
    FmFatConRetGru.ShowModal;
    FmFatConRetGru.Destroy;
  end;
end;

procedure TFmFatConRet.MostraFatConRetLei(SQLType: TSQLType);
begin
  DmodG.ReopenParamsEmp(QrFatConRetEmpresa.Value);
  if DBCheck.CriaFm(TFmFatConRetLei, FmFatConRetLei, afmoNegarComAviso) then
  begin
    FmFatConRetLei.ImgTipo.SQLType := SQLType;
    FmFatConRetLei.ShowModal;
    FmFatConRetLei.Destroy;
    ReopenGrupos(QrGruposNivel1.Value);
  end;
end;

procedure TFmFatConRet.PMMovimPopup(Sender: TObject);
var
  Habil1: Boolean;
begin
  Habil1 :=
    (QrFatConRet.State <> dsInactive) and (QrFatConRet.RecordCount > 0)
    and (QrFatConRetEncerrou.Value = 0);
  Alteramovimentoatual1.Enabled  := Habil1;
  Encerraacertoatual1.Enabled    := Habil1;
  Atualizaclculos1.Enabled       := Habil1;
  //BtEncerra.Enabled := Habil1;
end;

procedure TFmFatConRet.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmFatConRet.QueryPrincipalAfterOpen;
begin
end;

procedure TFmFatConRet.DBEdit6Change(Sender: TObject);
begin
  if DBEdit6.Text = CO_MovimentoAberto then
  begin
    DBEdit6.Font.Color := clRed;
    DBEdit6.Font.Style := [];//[fsBold];
  end else begin
    DBEdit6.Font.Color := clWindowText;
    DBEdit6.Font.Style := [];
  end;
end;

procedure TFmFatConRet.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmFatConRet.SpeedButton13Click(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  if DBCheck.CriaFm(TFmCarteiras, FmCarteiras, afmoNegarComAviso) then
  begin
    FmCarteiras.ShowModal;
    FmCarteiras.Destroy;
    if VAR_CADASTRO <> 0 then
    begin
      DmPediVda.QrCartEmis.Close;
      DmPediVda.QrCartEmis.Open;
      EdCartEmis.ValueVariant := VAR_CADASTRO;
      CBCartEmis.KeyValue     := VAR_CADASTRO;
    end;
  end;
end;

procedure TFmFatConRet.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmFatConRet.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmFatConRet.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmFatConRet.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmFatConRet.SpeedButton5Click(Sender: TObject);
var
  FisRegCad: Integer;
begin
  VAR_CADASTRO := 0;
  FisRegCad    := EdFisRegCad.ValueVariant;
  //
  Grade_Jan.MostraFormFisRegCad(FisRegCad);
  //
  if VAR_CADASTRO <> 0 then
  begin
    QrFisRegCad.Close;
    QrFisRegCad.Open;
    if QrFisRegCad.Locate('Codigo', QrFisRegCadCodigo.Value, []) then
    begin
      EdFisRegCad.ValueVariant := QrFisRegCadCodUsu.Value;
      CBFisRegCad.KeyValue     := QrFisRegCadCodUsu.Value;
    end;
  end;
end;

procedure TFmFatConRet.SpeedButton6Click(Sender: TObject);
begin
  VAR_ENTIDADE := 0;
  if DBCheck.CriaFm(TFmEntidade2, FmEntidade2, afmoNegarComAviso) then
  begin
    FmEntidade2.ShowModal;
    FmEntidade2.Destroy;
    if VAR_ENTIDADE <> 0 then
    begin
      DmPediVda.QrClientes.Close;
      DmPediVda.QrClientes.Open;
      if DmPediVda.QrClientes.Locate('Codigo', VAR_ENTIDADE, []) then
      begin
        EdCliente.ValueVariant := VAR_ENTIDADE;
        CBCliente.KeyValue     := VAR_ENTIDADE;
      end;
    end;
  end;
end;

procedure TFmFatConRet.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrFatConRetCodigo.Value;
  Close;
end;

procedure TFmFatConRet.BtMovimClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMMovim, BtMovim);
end;

procedure TFmFatConRet.BtPagtosClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMPagtos, BtPagtos);
end;

procedure TFmFatConRet.Alterapagamentoatual1Click(Sender: TObject);
(*var
  CliInt, Terceiro, Cod : Integer;
  Valor: Double;
*)
begin
(* Formul�rio n�o utilizado quando usar migrar para a forma de pagamento nova
  IC3_ED_Controle := QrEmissCControle.Value;
  Valor           := QrEmissCVALOR.Value;
  CliInt          := QrFatConRetEmpresa.Value;
  //
  DefineVarDup();
  //
  Cod             := QrFatConRetCodigo.Value;
  Terceiro        := QrFatConRetCliente.Value;
  //
  DmodG.ReopenParamsEmp(CliInt);
  //
  UCash.Pagto2(QrEmissC, tpCred, Cod, Terceiro, FThisFatID, 0, stUpd,
  'Venda Condicionada', Valor, VAR_USUARIO, 0, CliInt, mmNenhum, 0, 0,
  True, False, 0, 0, 0, 0, '');
  //
  RecalculaTodoFatConRet(QrFatConRetCodigo.Value);
  LocCod(Cod, Cod);
*)
end;

procedure TFmFatConRet.Atualizaclculos1Click(Sender: TObject);
begin
  RecalculaTodoFatConRet(QrFatConRetCodigo.Value);
end;

procedure TFmFatConRet.RecalculaTodoFatConRet(FatConRet: Integer);
begin
  Screen.Cursor := crHourGlass;
  try
    DmPediVda.AtzSdosCondicional_Devol(FatConRet);
    DmPediVda.AtzCondicional_Devol_Comis(FatConRet);
    DmPediVda.AtzCondicional_Devol_Pgtos(FatConRet, FThisFatID);
    //
    LocCod(FatConRet, FatConRet);
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmFatConRet.AtualizaItensFaturados(FatConCad: Integer);
begin
  DMod.QrUpd.SQL.Clear;
  DMod.QrUpd.SQL.Add('UPDATE fatconits SET QuantV = ');
  DMod.QrUpd.SQL.Add('     IF(QuantP > QuantC, QuantP - QuantC, 0),');
  DMod.QrUpd.SQL.Add('ValFat = PrecoF * IF(QuantP > QuantC, QuantP - QuantC, 0)');
  DMod.QrUpd.SQL.Add('WHERE Codigo=:P0');
  DMod.QrUpd.Params[00].AsInteger := FatConCad;
  DMod.QrUpd.ExecSQL;
  //
end;

procedure TFmFatConRet.BtConfirmaClick(Sender: TObject);
var
  Cliente, Codigo, Empresa, StqCenCad, FisRegCad: Integer;
  Nome: String;
begin
  if MyObjects.FIC(Geral.IMV(EdFilial.Text) = 0, EdFilial,
    'Informe a empresa!') then Exit;
  {
  if MyObjects.FIC(Geral.IMV(EdPrdGrupTip.Text) = 0, EdPrdGrupTip,
    'Informe o tipo de grupo de produtos!') then Exit;
  }
  if MyObjects.FIC(Geral.IMV(EdStqCenCad.Text) = 0, EdStqCenCad,
    'Informe o centro de estoque!') then Exit;
  if MyObjects.FIC(Geral.IMV(EdFisRegCad.Text) = 0, EdFisRegCad,
    'Informe a regra fiscal!') then Exit;
  if MyObjects.FIC(Geral.IMV(EdCartEmis.Text) = 0, EdCartEmis,
    'Informe a carteira!') then Exit;
  Cliente := Geral.IMV(EdCliente.Text);
  if MyObjects.FIC(Cliente = 0, EdCliente,
    'Informe o vendedor (cliente)!') then Exit;
  //
  Empresa    := QrFiliaisCodigo.Value;
  //PrdGrupTip := QrPrdGrupTipCodigo.Value;
  StqCenCad  := QrStqCenCadCodigo.Value;
  FisRegCad  := QrFisRegCadCodigo.Value;
  // Verifica se existe faturamentos condicionais dispon�veis para retorno
{
SELECT Codigo, CodUsu, Nome,
Abertura, Encerrou
FROM fatconcad
WHERE Encerrou > 0
AND FatConRet=0

AND Empresa=:P0
AND Cliente=:P1
AND StqCenCad=:P2
AND FisRegCad=:P3
}
  if ImgTipo.SQLType = stIns then
  begin
    QrDisponiv.Close;
    QrDisponiv.Params[00].AsInteger := Empresa;
    QrDisponiv.Params[01].AsInteger := Cliente;
    QrDisponiv.Params[02].AsInteger := StqCenCad;
    QrDisponiv.Params[03].AsInteger := FisRegCad;
    QrDisponiv.Open;
    //dmkPF.LeMeuTexto(QrDisponiv.SQL.Text);
    if MyObjects.FIC(QrDisponiv.RecordCount = 0, nil,
      'N�o h� faturamento condicional dispon�vel para acerto!' + sLineBreak +
      'Empresa: ' + IntToStr(Empresa) + ' - ' + CBFilial.Text + sLineBreak +
      'Vendedor Cliente: ' + IntToStr(Cliente) + ' - ' + CBCliente.Text + sLineBreak +
      'Centro de Estoque: ' + IntToStr(StqCenCad) + ' - ' + CBStqCenCad.Text + sLineBreak +
      'Regra Fiscal: ' + IntToStr(FisRegCad) + ' - ' + CBFisRegCad.Text + sLineBreak + sLineBreak +
      'Certifique-se de que estejam encerrados!') then Exit;
  end;    
  //
  {
   Verifica se existe balan�o aberto
   Como fazer se Tipo de Grupo de Produto � definido depois?
  if DmProd.ExisteBalancoAberto_StqCen_Mul(PrdGrupTip, Empresa, FisRegCad) then
    Exit;
  }
  Nome := EdNome.Text;
  if Length(Nome) = 0 then
  begin
    Application.MessageBox('Defina uma descri��o.', 'Erro', MB_OK+MB_ICONERROR);
    Exit;
  end;
  {  precisa?
  QrNew.Close;
  QrNew.Params[00].AsInteger := Empresa;
  QrNew.Params[01].AsInteger := PrdGrupTip;
  QrNew.Params[02].AsInteger := StqCenCad;
  QrNew.Open;
  if MyObjects.FIC(QrNew.RecordCount > 0, nil,
  'Inclus�o abortada!' + sLineBreak + 'Existe um balan�o aberto para esta ' +
  'configura��o (ID n� ' + IntToStr(QrNewCodigo.Value) + ').') then
  begin
    LocCod(QrFatConRetCodigo.Value, QrNewCodigo.Value);
    Exit;
  end;
  }
  //
  Codigo := UMyMod.BuscaEmLivreY_Def('FatConRet', 'Codigo', ImgTipo.SQLType,
    QrFatConRetCodigo.Value);
  //FEmInclusao := True;
  if UMyMod.ExecSQLInsUpdPanel(ImgTipo.SQLType, FmFatConRet, PainelEdita,
    'FatConRet', Codigo, Dmod.QrUpd, [PainelEdita], [PainelDados], ImgTipo, True) then
  begin
    DmPediVda.AtzSdosCondicional_Devol(Codigo);
    DmPediVda.AtzCondicional_Devol_Comis(Codigo);
    DmPediVda.AtzCondicional_Devol_Pgtos(Codigo, FThisFatID);
    //
    LocCod(Codigo, Codigo);
  end;
  //FEmInclusao := False;
end;

procedure TFmFatConRet.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := Geral.IMV(EdCodigo.Text);
  if ImgTipo.SQLType = stIns then UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'FatConRet', Codigo);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'FatConRet', 'Codigo');
  MostraEdicao(0, stLok, 0);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'FatConRet', 'Codigo');
end;

procedure TFmFatConRet.BtItensClick(Sender: TObject);
begin
  if QrFatConRetFATOR.Value = 0 then
  begin
    Geral.MensagemBox(PChar('Fator n�o definido! ' +
    'O c�lculo deve ser diferente de nulo na regra fiscal!'),
    'Mensagem', MB_OK+MB_ICONWARNING);
    Exit;
  end;
  MyObjects.MostraPopUpDeBotao(PMItens, BtItens);
{
  if DBCheck.CriaFm(TFmFatConLei, FmFatConLei, afmoNegarComAviso) then
  begin
    FmFatConLei.QrLidosQtde.DisplayFormat   := FFmtQtde;
    FmFatConLei.QrTotalQtdLei.DisplayFormat := FFmtQtde;
    FmFatConLei.EdQtdLei.DecimalSize        := QrFatConRetCasasProd.Value;
    FmFatConLei.EdQtdLei.ValueVariant       := QrFatConRetBalQtdItem.Value;
    FmFatConLei.EdStqCenCad.ValueVariant    := QrFatConRetStqCenCad.Value;
    FmFatConLei.CBStqCenCad.KeyValue        := QrFatConRetStqCenCad.Value;
    FmFatConLei.ReopenLidos(0);
    FmFatConLei.ShowModal;
    LocCod(QrFatConRetCodigo.Value, QrFatConRetCodigo.Value);
    FmFatConLei.Destroy;
  end;
}
end;

procedure TFmFatConRet.BtLotesClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMLotes, BtLotes);
end;

procedure TFmFatConRet.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  PageControl1.ActivePageIndex := 0;
  Panel4.Align     := alClient;
  QrFiliais.Open;
  QrStqCenCad.Open;
  DmPediVda.ReopenClientes();
  CriaOForm;
  EdDBTipoMov.DataSource := nil;
  EdDBTipoMov.DataSource := nil;
  //
  DmPediVda.QrTabePrcCab.Close;
  DmPediVda.QrTabePrcCab.Params[00].AsInteger := 4; // Condicional
  // o mysql n�o aceita (neste caso) data como yyyy/mm/yy deve ser yyyy-mm-dd
  DmPediVda.QrTabePrcCab.Params[01].AsString  := FormatDateTime('YYYY-MM-DD', Date);
  DmPediVda.QrTabePrcCab.Open;
  //
  DmPediVda.QrCartEmis.Close;
  DmPediVda.QrCartEmis.Open;
  //
  DmodG.QrCambioMda.Close;
  DmodG.QrCambioMda.Open;
  //
end;

procedure TFmFatConRet.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrFatConRetCodigo.Value, LaRegistro.Caption);
end;

procedure TFmFatConRet.SbImprimeClick(Sender: TObject);
begin
  //MyObjects.MostraPopUpDeBotao(PMImprime, SbImprime);
  if DBCheck.CriaFm(TFmFatPedImp, FmFatPedImp, afmoNegarComAviso) then
  begin
    FmFatPedImp.FFatID          := FThisFatID;
    FmFatPedImp.FEmpresa        := QrFatConRetEmpresa.Value;
    FmFatPedImp.FOriCodigo      := QrFatConRetCodigo.Value;
    FmFatPedImp.FTituloPedido   := 'Faturamento Condicional';
    FmFatPedImp.FPedido         := QrFatConRetCodUsu.Value;
    FmFatPedImp.FCliente        := QrFatConRetCliente.Value;
    FmFatPedImp.FNomeCondicaoPg := '';
    FmFatPedImp.FTipoMov        := QrFatConRetTipoMov.Value; // entrada ou saida
    FmFatPedImp.FDescoGeral_Val := QrFatConRetDescoGer.Value;
    FmFatPedImp.FDescoGeral_Per := QrFatConRetDescoPer.Value;
    FmFatPedImp.FComissao_Val   := QrFatConRetComisVal.Value;
    FmFatPedImp.FComissao_Per   := QrFatConRetComisPer.Value;
    FmFatPedImp.FValLiq         := QrFatConRetValorRec.Value;
    //
    FmFatPedImp.ShowModal;
    FmFatPedImp.Destroy;
  end;
end;

procedure TFmFatConRet.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmFatConRet.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrFatConRetCodUsu.Value, LaRegistro.Caption);
end;

procedure TFmFatConRet.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmFatConRet.QrGruposAfterOpen(DataSet: TDataSet);
var
  Habilita: Boolean;
begin
  Habilita := (QrGrupos.RecordCount > 0)
    and (QrFatConRetEncerrou.Value = 0);
  //
  BtItens.Enabled  := Habilita;
  BtPagtos.Enabled := Habilita;
end;

procedure TFmFatConRet.QrGruposAfterScroll(DataSet: TDataSet);
begin
  ReconfiguraGradeQ();
end;

procedure TFmFatConRet.QrGruposBeforeClose(DataSet: TDataSet);
begin
  MyObjects.LimpaGrades([GradeQ, GradeC, GradeA, GradeX], 0, 0, True);
end;

procedure TFmFatConRet.QrEmissCCalcFields(DataSet: TDataSet);
begin
  QrEmissCSEQ.Value := QrEmissC.RecNo;
end;

procedure TFmFatConRet.QrFatConLotAfterScroll(DataSet: TDataSet);
begin
  ReopenGrupos(0);
end;

procedure TFmFatConRet.QrFatConLotBeforeClose(DataSet: TDataSet);
begin
  BtItens.Enabled := False;
  QrGrupos.Close;
end;

procedure TFmFatConRet.QrFatConRetAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
  BtLotes.Enabled := (QrFatConRet.RecordCount > 0)
    and (QrFatConRetEncerrou.Value = 0);
end;

procedure TFmFatConRet.QrFatConRetAfterScroll(DataSet: TDataSet);
begin
  //BtEncerra.Enabled := QrFatConRet.RecordCount > 0;
  ReopenFatConLot(0);
  ReopenPagtos(0);
end;

procedure TFmFatConRet.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmFatConRet.SbQueryClick(Sender: TObject);
begin
  LocCod(QrFatConRetCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'FatConRet', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmFatConRet.FormResize(Sender: TObject);
begin
  // M L A G e r a l.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 30);
end;

procedure TFmFatConRet.GradeADrawCell(Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);
begin
  MyObjects.DesenhaGradeA(GradeA, ACol, ARow, Rect, State, True);
end;

procedure TFmFatConRet.GradeCDrawCell(Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);
begin
  MyObjects.DesenhaGradeN(GradeC, GradeA, nil, ACol, ARow, Rect, State,
  '0', 0, 0, True);
end;

procedure TFmFatConRet.GradeDDrawCell(Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);
begin
  MyObjects.DesenhaGradeN(GradeD, GradeA, nil, ACol, ARow, Rect, State,
  Dmod.FStrFmtPrc, 0, 0, False);
end;

procedure TFmFatConRet.GradeFDrawCell(Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);
begin
  MyObjects.DesenhaGradeN(GradeF, GradeA, nil, ACol, ARow, Rect, State,
  Dmod.FStrFmtPrc, 0, 0, False);
end;

procedure TFmFatConRet.GradeQDrawCell(Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);
begin
  MyObjects.DesenhaGradeN(GradeQ, GradeA, nil, ACol, ARow, Rect, State,
  dmkPF.FormataCasas(QrGruposFracio.Value), 0, 0, False);
end;

procedure TFmFatConRet.GradeVDrawCell(Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);
begin
  MyObjects.DesenhaGradeN(GradeV, GradeA, nil, ACol, ARow, Rect, State,
  Dmod.FStrFmtPrc, 0, 0, true);
end;

procedure TFmFatConRet.GradeXDrawCell(Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);
begin
  MyObjects.DesenhaGradeN(GradeX, GradeA, nil, ACol, ARow, Rect, State,
  '0', 0, 0, True);
end;

procedure TFmFatConRet.Incluinovolote1Click(Sender: TObject);
begin
  QrDisponiv.Close;
  QrDisponiv.Params[00].AsInteger := QrFatConRetEmpresa.Value;
  QrDisponiv.Params[01].AsInteger := QrFatConRetCliente.Value;
  QrDisponiv.Params[02].AsInteger := QrFatConRetStqCenCad.Value;
  QrDisponiv.Params[03].AsInteger := QrFatConRetFisRegCad.Value;
  QrDisponiv.Open;
  if MyObjects.FIC(QrDisponiv.RecordCount = 0, nil,
    'N�o h� faturamento condicional dispon�vel para acerto!' + sLineBreak +
    'Obs: O pedido de faturamento condicional deve estar encerrado!') then Exit;
  //
  if DBCheck.CriaFm(TFmFatConLot, FmFatConLot, afmoNegarComAviso) then
  begin
    FmFatConLot.ShowModal;
    FmFatConLot.Destroy;
  end;
end;

procedure TFmFatConRet.Incluinovomovimento1Click(Sender: TObject);
begin
  //FEmInclusao := False;
  UMyMod.ConfigPanelInsUpd(stIns, Self, PainelEdit, QrFatConRet, [PainelDados],
  [PainelEdita], EdCodUsu, ImgTipo, 'FatConRet');
  EdAbertura.ValueVariant :=  DModG.ObtemAgora();
end;

procedure TFmFatConRet.Incluinovospagamentos1Click(Sender: TObject);
(*
var
  Terceiro, Cod, Genero, CliInt, Carteira: Integer;
  Valor: Double;
*)
begin
(* Formul�rio n�o utilizado quando usar migrar para a forma de pagamento nova
  DefineVarDup();
  VAR_MYPAGTOSCONFIG := 1;
  Cod                := QrFatConRetCodigo.Value;
  Terceiro           := QrFatConRetCliente.Value;
  Carteira           := QrFatConRetCartEmis.Value;
  CliInt             := QrFatConRetEmpresa.Value;
  Valor              := QrFatConRetVALORPEN.Value;
  DmodG.ReopenParamsEmp(CliInt);
  Genero             := DmodG.QrParamsEmpCtaProdVen.Value;
  //
  UCash.Pagto2(QrEmissC, tpCred, Cod, Terceiro, FThisFatID, Genero, stIns,
  'Venda Condicionada', Valor, VAR_USUARIO, 0, CliInt, mmNenhum, 0, 0,
  True, True, Carteira, 0, 0, 0, DModG.QrParamsEmpTxtProdVen.Value);
  //
  DmPediVda.AtzCondicional_Devol_Pgtos(Cod, FThisFatID);
  LocCod(Cod, Cod);
*)
end;

procedure TFmFatConRet.QrFatConRetBeforeClose(DataSet: TDataSet);
begin
  BtLotes.Enabled := False;
  //BtEncerra.Enabled := False;
  QrFatConLot.Close;
end;

procedure TFmFatConRet.QrFatConRetBeforeOpen(DataSet: TDataSet);
begin
  QrFatConRetCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmFatConRet.QrFatConRetCalcFields(DataSet: TDataSet);
begin
  if QrFatConRetEncerrou.Value = 0 then
    QrFatConRetENCERROU_TXT.Value := CO_MovimentoAberto
  else
    QrFatConRetENCERROU_TXT.Value := Geral.FDT(QrFatConRetEncerrou.Value, 0);
  //
  case QrFatConRetTipoCalc.Value of
    1: QrFatConRetFATOR.Value := 1;
    2: QrFatConRetFATOR.Value := -1;
    else QrFatConRetFATOR.Value := 0;
  end;
  //
  QrFatConRetVALORPEN.Value  :=
    QrFatConRetValFat.Value   -
    QrFatConRetComisVal.Value -
    QrFatConRetDescoGer.Value -
    QrFatConRetValorRec.Value;
  //
  if (QrFatConRetDescoGer.Value >= 0.01) and (QrFatConRetValFat.Value > 0) then
    QrFatConRetDESCOPER.Value :=
      QrFatConRetDescoGer.Value / QrFatConRetValFat.Value * 100
  else QrFatConRetDESCOPER.Value := 0;
end;

procedure TFmFatConRet.ReconfiguraGradeQ;
var
  Grade, Nivel1, Condicional: Integer;
begin
  Grade       := QrGruposGRATAMCAD.Value;
  Nivel1      := QrGruposNivel1.Value;
  Condicional := QrFatConRetCodigo.Value;
  DmProd.ConfigGrades6B(Grade, Nivel1, Condicional,
  GradeA, GradeX, GradeC, GradeQ, GradeF, GradeD, GradeV);
end;

procedure TFmFatConRet.ReopenFatConLot(Codigo: Integer);
begin
  QrFatConLot.Close;
  QrFatConLot.Params[0].AsInteger := QrFatConRetCodigo.Value;
  QrFatConLot.Open;
  //
  QrFatConLot.Locate('Codigo', Codigo, []);
end;

procedure TFmFatConRet.ReopenFisRegCad;
var
  Empresa, StqCenCad: Integer;
begin
  //if FEmInclusao then Exit;
  Empresa   := Geral.IMV(EdFilial.Text);
  StqCenCad := Geral.IMV(EdStqCenCad.Text);
  if (Empresa <> 0) and (StqCenCad <> 0) then
  begin
    Empresa   := QrFiliaisCodigo.Value;
    StqCenCad := QrStqCenCadCodigo.Value;
    if //(QrFisRegCad.State = dsInactive) or
    (Empresa <> QrFisRegCad.Params[0].AsInteger) or
    (StqCenCad <> QrFisRegCad.Params[1].AsInteger) then
    begin
      QrFisRegCad.Close;
      QrFisRegCad.Params[00].AsInteger := Empresa;
      QrFisRegCad.Params[01].AsInteger := StqCenCad;
      QrFisRegCad.Open;
      EdFisRegCad.ValueVariant := 0;
      CBFisRegCad.KeyValue     := Null;
    end;
  end else QrFisRegCad.Close;
end;

procedure TFmFatConRet.ReopenGrupos(Nivel1: Integer);
begin
  QrGrupos.Close;
  QrGrupos.Params[00].AsInteger := QrFatConRetCodigo.Value;
  QrGrupos.Open;
  //
  if Nivel1 <> 0 then
    QrGrupos.Locate('Nivel1', Nivel1, []);
end;

procedure TFmFatConRet.RetornaitensporGrade1Click(Sender: TObject);
begin
  MostraFatConRetGru(stIns);
end;

procedure TFmFatConRet.RetornaitensporLeitura1Click(Sender: TObject);
begin
  MostraFatConRetLei(stIns);
end;

procedure TFmFatConRet.Alteramovimentoatual1Click(Sender: TObject);
var
  Habilita: Boolean;
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PainelEdita, QrFatConRet, [PainelDados],
  [PainelEdita], EdCodUsu, ImgTipo, 'FatConRet');
  Habilita := QrGrupos.RecordCount = 0;
  EdFilial.Enabled     := Habilita;
  CBFilial.Enabled     := Habilita;
  //EdPrdGrupTip.Enabled := Habilita;
  //CBPrdGrupTip.Enabled := Habilita;
  EdStqCenCad.Enabled  := Habilita;
  CBStqCenCad.Enabled  := Habilita;
  EdFisRegCad.Enabled  := Habilita;
  CBFisRegCad.Enabled  := Habilita;
end;

procedure TFmFatConRet.ExcluiPagto();
var
  //FatNum: Double;
  FatNum, FatID, FatParcela : Integer;
begin
(* Ver como fazer!
  FatParcela := QrEmissCFatParcela.Value;
  FatNum     := QrFatConRetCodigo.Value;
  FatID      := FThisFatID;
  if  M L A G e r a l.NaoPermiteExclusaoDeLancto(FatParcela, FatNum, FatID) then Exit;
  if Application.MessageBox(PChar(FIN_MSG_ASKESCLUI), 'Pergunta', MB_YESNOCANCEL) = ID_YES then
  begin
    Dmod.QrUpd.Close;
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('DELETE FROM lanctos WHERE FatID=:P0');
    Dmod.QrUpd.SQL.Add('AND FatNum=:P1 AND FatParcela=:P2');
    Dmod.QrUpd.Params[00].AsInteger := FatID;
    Dmod.QrUpd.Params[01].AsFloat   := FatNum;
    Dmod.QrUpd.Params[02].AsInteger := FatParcela;
    Dmod.QrUpd.ExecSQL;
    //
    Dmod.QrUpd.Close;
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('UPDATE lanctos SET FatParcela=FatParcela-1');
    Dmod.QrUpd.SQL.Add('WHERE FatParcela>:P0 AND FatID=:P1 AND FatNum=:P2');
    Dmod.QrUpd.Params[00].AsInteger := FatParcela;
    Dmod.QrUpd.Params[01].AsInteger := FatID;
    Dmod.QrUpd.Params[02].AsFloat   := FatNum;
    Dmod.QrUpd.ExecSQL;
    //
    RecalculaTodoFatConRet(FatNum);
  end;
*)
end;

procedure TFmFatConRet.ExcluiTodosPagtos();
var
  //FatNum: Double;
  FatNum, FatID, FatParcela: Integer;
begin
(* Ver como fazer!
  FatNum     := QrFatConRetCodigo.Value;
  FatID      := FThisFatID;
  FatParcela := 0;
  if  M L A G e r a l.NaoPermiteExclusaoDeLancto(FatParcela, FatNum, FatID) then Exit;
  if Application.MessageBox(PChar('Confirma a EXCLUS�O DE TODOS PAGAMENTOS?'),
  'Pergunta', MB_YESNOCANCEL) = ID_YES then
  begin
    Dmod.QrUpd.Close;
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('DELETE FROM lanctos WHERE FatID=:P0');
    Dmod.QrUpd.SQL.Add('AND FatNum=:P1');
    Dmod.QrUpd.Params[00].AsInteger := FatID;
    Dmod.QrUpd.Params[01].AsFloat   := FatNum;
    Dmod.QrUpd.ExecSQL;
    //
    RecalculaTodoFatConRet(FatNum);
  end;
*)
end;

procedure TFmFatConRet.DefineVarDup;
begin
  IC3_ED_FatNum := QrFatConRetCodigo.Value;
  IC3_ED_NF     := 0;//Geral.IMV(EdNF.Text);
  IC3_ED_Data   := Int(QrFatConRetEncerrou.Value);
end;

procedure TFmFatConRet.ReopenPagtos(FatParcela: Integer);
begin
  QrEmissC.Close;
  QrEmissC.Params[00].AsInteger := FThisFatID;
  QrEmissC.Params[01].AsInteger := QrFatConRetCodigo.Value;
  QrEmissC.Open;
  //
  QrEmissC.Locate('FatParcela', FatParcela, []);
end;

end.

