unit FatConLot;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Mask, DBCtrls, DB, mySQLDbTables, Grids, DBGrids,
  dmkLabel, dmkGeral;

type
  TFmFatConLot = class(TForm)
    PainelConfirma: TPanel;
    BtOK: TBitBtn;
    PainelTitulo: TPanel;
    Image1: TImage;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    DsFatConRet: TDataSource;
    PnEdit: TPanel;
    PnRetorno: TPanel;
    GroupBox5: TGroupBox;
    dmkLabel4: TdmkLabel;
    DBEdit11: TDBEdit;
    DBEdit13: TDBEdit;
    GroupBox1: TGroupBox;
    Label1: TLabel;
    Label3: TLabel;
    Label2: TLabel;
    Label4: TLabel;
    DBEdCodigo: TDBEdit;
    DBEdit1: TDBEdit;
    DBEdNome: TDBEdit;
    DBEdit2: TDBEdit;
    Panel1: TPanel;
    DBGrid1: TDBGrid;
    DsDisponiv: TDataSource;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  var
  FmFatConLot: TFmFatConLot;

implementation

{$R *.DFM}

uses UnMyObjects, FatConRet, Module;

procedure TFmFatConLot.BtOKClick(Sender: TObject);
  procedure AdicionaLoteAtual(FatConRet: Integer);
  var
    FatConcad: Integer;
  begin
    FatConCad := FmFatConRet.QrDisponivCodigo.Value;
    //
    FmFatConRet.AtualizaItensFaturados(FatConcad);
    //
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('UPDATE fatconcad SET ');
    Dmod.QrUpd.SQL.Add('FatConRet=:P0');
    Dmod.QrUpd.SQL.Add('WHERE Codigo=:Pa');
    //
    Dmod.QrUpd.Params[00].AsInteger := FatConRet;
    Dmod.QrUpd.Params[01].AsInteger := FatConCad;
    Dmod.QrUpd.ExecSQL;
  end;
var
  Codigo, i: integer;
begin
  if FmFatConRet.QrDisponiv.RecordCount > 0 then
  begin
    Codigo := FmFatConRet.QrFatConRetCodigo.Value;
    if DBGrid1.SelectedRows.Count > 0 then
    begin
      if Geral.MensagemBox('Confirma a adi��o dos lotes selecionados?',
      'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
      begin
        with DBGrid1.DataSource.DataSet do
        for i:= 0 to DBGrid1.SelectedRows.Count-1 do
        begin
          //GotoBookmark(pointer(DBGrid1.SelectedRows.Items[i]));
          GotoBookmark(DBGrid1.SelectedRows.Items[i]);
          AdicionaLoteAtual(Codigo);
        end;
      end;
    end else begin
      if Application.MessageBox('Confirma a adi��o do lote selecionado?',
      'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
        AdicionaLoteAtual(Codigo);
    end;
    FmFatConRet.LocCod(Codigo, Codigo);
    Close;
  end else Geral.MensagemBox('N�o h� itens para adicionar!', 'Aviso',
    MB_OK+MB_ICONWARNING);
end;

procedure TFmFatConLot.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmFatConLot.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmFatConLot.FormCreate(Sender: TObject);
begin
  DsFatConRet.DataSet := FmFatConRet.QrFatConRet;
end;

procedure TFmFatConLot.FormResize(Sender: TObject);
begin
  // M L A G e r a l .LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

end.
