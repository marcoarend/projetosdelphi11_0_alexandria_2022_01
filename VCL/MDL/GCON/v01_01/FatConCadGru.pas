unit FatConCadGru;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnInternalConsts,
  dmkImage, Mask, UnDmkEnums, UnDmkProcFunc, DmkDAC_PF;

type
  TFmFatConCadGru = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    QrGraGru1: TmySQLQuery;
    QrGraGru1Codusu: TIntegerField;
    QrGraGru1Nivel3: TIntegerField;
    QrGraGru1Nivel2: TIntegerField;
    QrGraGru1Nivel1: TIntegerField;
    QrGraGru1Nome: TWideStringField;
    QrGraGru1PrdGrupTip: TIntegerField;
    QrGraGru1GraTamCad: TIntegerField;
    QrGraGru1NOMEGRATAMCAD: TWideStringField;
    QrGraGru1CODUSUGRATAMCAD: TIntegerField;
    QrGraGru1CST_A: TSmallintField;
    QrGraGru1CST_B: TSmallintField;
    QrGraGru1UnidMed: TIntegerField;
    QrGraGru1NCM: TWideStringField;
    QrGraGru1Peso: TFloatField;
    QrGraGru1SIGLAUNIDMED: TWideStringField;
    QrGraGru1CODUSUUNIDMED: TIntegerField;
    QrGraGru1NOMEUNIDMED: TWideStringField;
    QrGraGru1NOME_EX: TWideStringField;
    QrGraGru1Fracio: TSmallintField;
    DsGraGru1: TDataSource;
    QrLoc: TmySQLQuery;
    QrLocUltimo: TIntegerField;
    QrLista: TmySQLQuery;
    QrListaGraCusPrc: TIntegerField;
    PnSeleciona: TPanel;
    Label1: TLabel;
    EdGraGru1: TdmkEditCB;
    CBGraGru1: TdmkDBLookupComboBox;
    ST3: TStaticText;
    ST2: TStaticText;
    ST1: TStaticText;
    PnJuros: TPanel;
    Label2: TLabel;
    Label3: TLabel;
    DBEdit1: TDBEdit;
    EdCustoFin: TdmkEdit;
    PageControl1: TPageControl;
    TabSheet5: TTabSheet;
    GradeQ: TStringGrid;
    StaticText1: TStaticText;
    TabSheet2: TTabSheet;
    GradeF: TStringGrid;
    TabSheet4: TTabSheet;
    GradeD: TStringGrid;
    StaticText3: TStaticText;
    TabSheet1: TTabSheet;
    GradeL: TStringGrid;
    TabSheet6: TTabSheet;
    GradeC: TStringGrid;
    StaticText6: TStaticText;
    TabSheet3: TTabSheet;
    GradeA: TStringGrid;
    StaticText2: TStaticText;
    TabSheet9: TTabSheet;
    GradeX: TStringGrid;
    PB1: TProgressBar;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure EdGraGru1Change(Sender: TObject);
    procedure EdGraGru1Enter(Sender: TObject);
    procedure EdGraGru1Exit(Sender: TObject);
    procedure CBGraGru1Exit(Sender: TObject);
    procedure GradeQDblClick(Sender: TObject);
    procedure GradeQDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect;
      State: TGridDrawState);
    procedure GradeQKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure GradeFDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect;
      State: TGridDrawState);
    procedure GradeFKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure GradeDClick(Sender: TObject);
    procedure GradeDDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect;
      State: TGridDrawState);
    procedure GradeDKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure GradeDSelectCell(Sender: TObject; ACol, ARow: Integer;
      var CanSelect: Boolean);
    procedure GradeLDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect;
      State: TGridDrawState);
    procedure GradeCDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect;
      State: TGridDrawState);
    procedure GradeADrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect;
      State: TGridDrawState);
    procedure GradeXDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect;
      State: TGridDrawState);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
    FOldNivel1: Integer;
    procedure ReconfiguraGradeQ();
    // Qtde de itens
    function QtdeItemCorTam(GridP, GridC, GridX: TStringGrid): Boolean;
    function QtdeVariosItensCorTam(GridP, GridA, GridC, GridX: TStringGrid): Boolean;
    function ObtemQtde(var Qtde: Double): Boolean;
    //
    function DescontoVariosItensCorTam(GridD, GridA, GridC, GridX:
             TStringGrid): Boolean;
    function ObtemDesconto(var Desconto: Double): Boolean;
  public
    { Public declarations }
  end;

  var
  FmFatConCadGru: TFmFatConCadGru;

implementation

uses UnMyObjects, Module, ModuleGeral, FatConCad, ModPediVda, ModProd, GraGruX,
  GetValor, GraGruVal, UMySQLModule, MyDBCheck;

{$R *.DFM}

procedure TFmFatConCadGru.BtOKClick(Sender: TObject);
const
  Tipo = 003;
var
  Col, Row, i, Nivel1, GraGruX, Codigo, Controle: Integer;
  Tot, Qtd, PrecoO, PrecoR, PrecoF, QuantP, ValCal, ValBru, ValLiq,
  DescoP, DescoV, DescoI: Double;
  Txt1, Txt2: String;
  ErrPreco, AvisoPrc, QtdRed, Casas: Integer;
  //
  OriCtrl, IDCtrl, StqCenCad, OriCodi, Empresa, OriCnta,
  AlterWeb, Ativo: Integer;
  Qtde: Double;
  DataHora: String;
begin
  if EdGraGru1.ValueVariant = 0 then
  begin
    Application.MessageBox('Defina o produto!', 'Aviso', MB_OK+MB_ICONWARNING);
    Exit;
  end;
  Nivel1 := QrGraGru1Nivel1.Value;
  Screen.Cursor := crHourGlass;
  try
    //
    DataHora  := FormatDateTime('yyyy-mm-dd hh:nn:ss', DModG.ObtemAgora());
    StqCenCad := FmFatConCad.QrFatConCadStqCenCad.Value;
    OriCodi   := FmFatConCad.QrFatConCadCodigo.Value;
    Empresa   := FmFatConCad.QrFatConCadEmpresa.Value;
    OriCnta   := 0;
    AlterWeb  := 1;
    Ativo     := 1;
    //
    Codigo    := FmFatConCad.QrFatConCadCodigo.Value;
    Tot       := 0;
    ErrPreco  := 0;
    AvisoPrc  := 0;
    QtdRed    := 0;
    for Col := 1 to GradeQ.ColCount -1 do
    begin
      for Row := 1 to GradeQ.RowCount - 1 do
      begin
        Qtd := Geral.IMV(GradeQ.Cells[Col,Row]);
        Tot := Tot + Qtd;
        if Qtd > 0 then
          QtdRed := QtdRed + 1;
        PrecoO := Geral.DMV(GradeL.Cells[Col,Row]);
        PrecoR := Geral.DMV(GradeF.Cells[Col,Row]);
        if (PrecoO <>  PrecoR) and (Qtd = 0) then
          AvisoPrc := AvisoPrc + 1;
        if (Qtd > 0) and (PrecoR = 0) then
          ErrPreco := ErrPreco + 1;
      end;
    end;
    if Tot = 0 then
    begin
      Screen.Cursor := crDefault;
      Application.MessageBox(
        'N�o h� defini��o de quantidades!', 'Aviso', MB_OK+MB_ICONWARNING);
      Exit;
    end
    else if ErrPreco > 0 then
    begin
      Screen.Cursor := crDefault;
      Application.MessageBox(PChar('Existem ' + IntToStr(ErrPreco) +
      ' itens sem pre�o de faturamento definido!'), 'Aviso',
      MB_OK+MB_ICONWARNING);

      Exit;
    end
    else if AvisoPrc > 0 then
    begin
      if Application.MessageBox(PChar('Existem ' + IntToStr(AvisoPrc) +
      ' itens com pre�o de faturamento alterado, mas n�o h� defini��o de ' +
      'quantidades de itens!' + sLineBreak + 'Deseja continuar assim mesmo?'),
      'Pergunta', MB_YESNOCANCEL+MB_ICONWARNING) <> ID_YES then
      begin
        Screen.Cursor := crDefault;
        Exit;
      end;
    end;
    PB1.Position := 0;
    PB1.Max := QtdRed;
    for Col := 1 to GradeQ.ColCount -1 do
    begin
      if Geral.IMV(GradeX.Cells[Col,0]) > 0 then
      begin
        for Row := 1 to GradeQ.RowCount - 1 do
        begin
          if Geral.IMV(GradeX.Cells[0,Row]) > 0 then
          begin
            Qtd := Geral.DMV(GradeQ.Cells[Col, Row]);
            if Qtd > 0 then
            begin
              GraGruX := Geral.IMV(GradeC.Cells[Col, Row]);
              if GraGruX > 0 then
              begin
                Casas  := Dmod.QrControleCasasProd.Value;
                PrecoO := Geral.DMV(GradeL.Cells[Col, Row]);
                PrecoR := Geral.DMV(GradeF.Cells[Col, Row]);
                QuantP := Geral.DMV(GradeQ.Cells[Col, Row]);
                ValCal := PrecoR;// * QuantP;
                DescoP := Geral.DMV(GradeD.Cells[Col, Row]);
                DescoI := dmkPF.FFF(PrecoR * DescoP / 100, Casas, siPositivo);
                PrecoF := PrecoR - DescoI;
                DescoV := DescoI * QuantP;
                ValBru := PrecoR * QuantP;
                ValLiq := ValBru - DescoV;
                //
                {
                Controle := UMyMod.BuscaEmLivreY_Def('pedivdaits', 'Controle',
                  ImgTipo.SQLType, 0);
                if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'pedivdaits', False,[
                'Codigo', 'GraGruX', 'PrecoO',
                'PrecoR', 'QuantP', 'ValBru',
                'DescoP', 'DescoV', 'ValLiq',
                'PrecoF'  ], [
                'Controle'], [
                Codigo, GraGruX, PrecoO,
                PrecoR, QuantP, ValBru,
                DescoP, DescoV, ValLiq,
                PrecoF], [
                Controle], True) then
                DmPediVda.AtzSdosPedido(Codigo);
                }
                if PrecoR <> 0 then
                begin
                  Qtde      := QuantP * FmFatConCad.QrFatConCadFATOR.Value;
                  {
                  Preco     := PrecoR;
                  Valor     := ValLiq;
                  }
                  OriCtrl   := 0;
                  OriCtrl   := DModG.BuscaProximoCodigoInt('controle', 'stqmov003', '', OriCtrl);
                  Controle  := OriCtrl;
                  //
                  IDCtrl := UMyMod.Busca_IDCtrl_NFe(stIns, 0);
                  if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'stqmovitsa', False, [
                  'DataHora', 'Tipo', 'OriCodi',
                  'OriCtrl', 'Empresa', 'StqCenCad',
                  'GraGruX', 'Qtde', 'OriCnta',
                  'OriPart', 'AlterWeb', 'Ativo'], [
                  'IDCtrl'], [
                  DataHora, Tipo, OriCodi,
                  OriCtrl, Empresa, StqCenCad,
                  GraGruX, Qtde, OriCnta,
                  OriCtrl, AlterWeb, Ativo], [
                  IDCtrl], False) then
                  begin
                    if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'fatconits', False, [
                    'Codigo', 'GraGruX', 'PrecoO',
                    'PrecoR', 'QuantP',
                    'ValBru', 'DescoP',
                    'DescoV', 'ValLiq', 'PrecoF'], [
                    'Controle'], [
                    Codigo, GraGruX, PrecoO,
                    PrecoR, QuantP,
                    ValBru, DescoP,
                    DescoV, ValLiq, PrecoF], [
                    Controle], True) then
                    {
                    if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'fatconits', False, [
                    'Codigo', 'GraGruX', 'Qtde', 'Preco', 'Valor'], ['Controle'], [
                    OriCodi, GraGruX, Qtde, Preco, Valor], [OriCtrl], True) then
                    }
                    begin
                    //
                    end;
                  end;
                end else
                  Application.MessageBox(PChar('Valor (ou pre�o) n�o definido ' +
                  'para o reduzido ' + IntToStr(GraGruX) +
                  '. Ele n�o ser� incluido!'), 'Aviso',
                  MB_OK+MB_ICONWARNING);
              end;
            end;
          end;
        end;
      end;
    end;
    EdGraGru1.ValueVariant := 0;
    EdGraGru1.SetFocus;
    DmPediVda.AtzSdosCondicional_Saida(Codigo);
    FmFatConCad.LocCod(Codigo, Codigo);
    FmFatConCad.ReopenGrupos(FmFatConCad.QrGruposNivel1.Value);
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmFatConCadGru.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmFatConCadGru.CBGraGru1Exit(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 0;
  GradeQ.SetFocus;
  GradeQ.Col := 1;
  GradeQ.Row := 1;
end;

procedure TFmFatConCadGru.EdGraGru1Change(Sender: TObject);
begin
  if EdGraGru1.Focused = False then
    ReconfiguraGradeQ();
end;

procedure TFmFatConCadGru.EdGraGru1Enter(Sender: TObject);
begin
  FOldNivel1 := EdGraGru1.ValueVariant;
end;

procedure TFmFatConCadGru.EdGraGru1Exit(Sender: TObject);
begin
  if FOldNivel1 <> EdGraGru1.ValueVariant then
  begin
    FOldNivel1 := EdGraGru1.ValueVariant;
    ReconfiguraGradeQ();
  end;
end;

procedure TFmFatConCadGru.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmFatConCadGru.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  ST1.Caption := '';
  ST2.Caption := '';
  ST3.Caption := '';
  ST1.Font.Color := clRed;
  ST2.Font.Color := clRed;
  ST3.Font.Color := clRed;
  ST1.Color := PnSeleciona.Color;
  ST2.Color := PnSeleciona.Color;
  ST3.Color := PnSeleciona.Color;
  //
  PageControl1.ActivePageIndex := 0;
  GradeA.ColWidths[0] := 128;
  GradeC.ColWidths[0] := 128;
  GradeX.ColWidths[0] := 128;
  GradeQ.ColWidths[0] := 128;
  GradeL.ColWidths[0] := 128;
  GradeF.ColWidths[0] := 128;
  GradeD.ColWidths[0] := 128;
  //
  QrGraGru1.Close;
  QrGraGru1.SQL.Clear;
  QrGraGru1.SQL.Add('SELECT gg1.Codusu, gg1.Nivel3, gg1.Nivel2, gg1.Nivel1,');
  QrGraGru1.SQL.Add('gg1.Nome, gg1.PrdGrupTip, gg1.GraTamCad,');
  QrGraGru1.SQL.Add('gtc.Nome NOMEGRATAMCAD, gtc.CodUsu CODUSUGRATAMCAD,');
  QrGraGru1.SQL.Add('gg1.CST_A, gg1.CST_B, gg1.UnidMed, gg1.NCM, gg1.Peso,');
  QrGraGru1.SQL.Add('unm.Sigla SIGLAUNIDMED, unm.CodUsu CODUSUUNIDMED,');
  QrGraGru1.SQL.Add('unm.Nome NOMEUNIDMED,  pgt.Fracio,');
  QrGraGru1.SQL.Add('CONCAT(LPAD(gg1.CodUsu, 8, "0"), " - ", gg1.Nome) NOME_EX ');
  QrGraGru1.SQL.Add('FROM gragru1 gg1');
  QrGraGru1.SQL.Add('LEFT JOIN gratamcad gtc ON gtc.Codigo=gg1.GraTamCad');
  QrGraGru1.SQL.Add('LEFT JOIN unidmed   unm ON unm.Codigo=gg1.UnidMed');
  QrGraGru1.SQL.Add('LEFT JOIN prdgruptip pgt ON pgt.Codigo=gg1.PrdGrupTip');
  QrGraGru1.SQL.Add('WHERE pgt.TipPrd=1');
  QrGraGru1.SQL.Add('');
  QrGraGru1.SQL.Add('AND gg1.Nivel1 NOT IN (');
  QrGraGru1.SQL.Add('  SELECT DISTINCT gg1.Nivel1');
  QrGraGru1.SQL.Add('  FROM pedivdaits pvi');
  QrGraGru1.SQL.Add('  LEFT JOIN gragrux   ggx ON ggx.Controle=pvi.GraGruX');
  QrGraGru1.SQL.Add('  LEFT JOIN gragru1   gg1 ON gg1.Nivel1=ggx.GraGru1');
  QrGraGru1.SQL.Add('  WHERE pvi.Codigo=:P0');
  QrGraGru1.SQL.Add(')');
  QrGraGru1.SQL.Add('');
  QrGraGru1.SQL.Add('ORDER BY gg1.Nome');
  QrGraGru1.Params[0].AsInteger := FmFatConCad.QrFatConCadCodigo.Value;
  UnDmkDAC_PF.AbreQuery(QrGraGru1, Dmod.MyDB);
end;

procedure TFmFatConCadGru.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmFatConCadGru.GradeADrawCell(Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);
begin
  MyObjects.DesenhaGradeA(GradeA, ACol, ARow, Rect, State, True);
end;

procedure TFmFatConCadGru.GradeCDrawCell(Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);
begin
  MyObjects.DesenhaGradeN(GradeC, GradeA, nil, ACol, ARow, Rect, State,
  '0', 0, 0, True);
end;

procedure TFmFatConCadGru.GradeDClick(Sender: TObject);
begin
  if (GradeD.Col = 0) or (GradeD.Row = 0) then
    DescontoVariosItensCorTam(GradeD, GradeA, GradeC, GradeX)
end;

procedure TFmFatConCadGru.GradeDDrawCell(Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);
begin
  MyObjects.DesenhaGradeN(GradeD, GradeA, nil, ACol, ARow, Rect, State,
  Dmod.FStrFmtPrc + ' %', 0, 0, False);
end;

procedure TFmFatConCadGru.GradeDKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  MyObjects.PulaCelulaGradeInput(GradeD, GradeC, Key, Shift);
end;

procedure TFmFatConCadGru.GradeDSelectCell(Sender: TObject; ACol, ARow: Integer;
  var CanSelect: Boolean);
begin
  if (ACol = 0) or (ARow = 0) then
    GradeD.Options := GradeD.Options - [goEditing]
  else
    GradeD.Options := GradeD.Options + [goEditing];
end;

procedure TFmFatConCadGru.GradeFDrawCell(Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);
begin
  MyObjects.DesenhaGradeN(GradeF, GradeA, GradeL, ACol, ARow, Rect, State,
  Dmod.FStrFmtPrc, 0, 0, False);
end;

procedure TFmFatConCadGru.GradeFKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  MyObjects.PulaCelulaGradeInput(GradeF, GradeC, Key, Shift);
end;

procedure TFmFatConCadGru.GradeLDrawCell(Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);
begin
  MyObjects.DesenhaGradeN(GradeL, GradeA, nil, ACol, ARow, Rect, State,
  Dmod.FStrFmtPrc, 0, 0, True);
end;

procedure TFmFatConCadGru.GradeQDblClick(Sender: TObject);
begin
  if (GradeQ.Col = 0) or (GradeQ.Row = 0) then
    QtdeVariosItensCorTam(GradeQ, GradeA, GradeC, GradeX)
  else
    QtdeItemCorTam(GradeQ, GradeC, GradeX);
end;

procedure TFmFatConCadGru.GradeQDrawCell(Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);
begin
  MyObjects.DesenhaGradeN(GradeQ, GradeA, nil, ACol, ARow, Rect, State,
  dmkPF.FormataCasas(QrGraGru1Fracio.Value), 0, 0, False);
end;

procedure TFmFatConCadGru.GradeQKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  MyObjects.PulaCelulaGradeInputEx(GradeQ, GradeC, Key, Shift, BtOK);
end;

procedure TFmFatConCadGru.GradeXDrawCell(Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);
begin
  MyObjects.DesenhaGradeN(GradeX, nil, nil, ACol, ARow, Rect, State,
  '0', 0, 0, True);
end;

procedure TFmFatConCadGru.ReconfiguraGradeQ;
var
  MedDDSimpl, MedDDReal, MediaSel, TaxaM, Juros: Double;
  Grade, Nivel1, Tabela, CondicaoPG, Lista, TipMediaDD, FatSemPrcL: Integer;
begin
  PnJuros.Visible := False;
  Tabela     := FmFatConCad.QrFatConCadTabelaPrc.Value;
  MedDDSimpl := 0;//FmFatConCad.QrFatConCadMedDDSimpl.Value;
  MedDDReal  := 0;//FmFatConCad.QrFatConCadMedDDReal.Value;
  Grade      := QrGraGru1GraTamCad.Value;
  Nivel1     := QrGraGru1Nivel1.Value;
  CondicaoPG := 0;//FmFatConCad.QrFatConCadCondicaoPG.Value;
  TipMediaDD := DmPediVda.QrParamsEmpTipMediaDD.Value;
  FatSemPrcL := DmPediVda.QrParamsEmpFatSemPrcL.Value;
  //
  if Tabela > 0 then
  begin
    PageControl1.Visible := True;
    DmProd.ConfigGrades5(Grade, Nivel1, Tabela, CondicaoPG,
    GradeA, GradeX, GradeC, GradeL, GradeF, GradeQ, GradeD,
    MedDDSimpl, MedDDReal, ST1, ST2, ST3, ImgTipo, True,
    TipMediaDD, FatSemPrcL);
    //
  end else begin
    QrLista.Close;
    QrLista.Params[00].AsInteger := FmFatConCad.QrFatConCadEmpresa.Value;
    QrLista.Params[01].AsInteger := FmFatConCad.QrFatConCadFisRegCad.Value;
    UnDmkDAC_PF.AbreQuery(QrLista, Dmod.MyDB);
    if QrLista.RecordCount = 1 then
    begin
      PageControl1.Visible := True;
      Lista := QrListaGraCusPrc.Value;
      case DmPediVda.QrParamsEmpTipMediaDD.Value of
        1: MediaSel := MedDDSimpl;
        2: MediaSel := MedDDReal;
        else MediaSel := 0;
      end;
      TaxaM := 0;//TaxaM := FmFatConCad.QrFatConCadJurosMes.Value;
      case DmPediVda.QrParamsEmpTipCalcJuro.Value of
        1: Juros := dmkPF.CalculaJuroSimples(TaxaM, MediaSel);
        2: Juros := dmkPF.CalculaJuroComposto(TaxaM, MediaSel);
        else Juros := 0;
      end;
      EdCustoFin.ValueVariant := Juros;
      PnJuros.Visible := True;

      DmProd.ConfigGrades9(Grade, Nivel1, Lista, FmFatConCad.QrFatConCadEmpresa.Value,
      GradeA, GradeX, GradeC, GradeL, GradeF, GradeQ, GradeD,
      //Grade0, Grade1, Grade2, Grade3, Grade4,
      nil, nil, nil, nil, nil,
      Juros, ST1, ST2, ST3, ImgTipo);
    end else begin
      PageControl1.Visible := False;
      //
      MyObjects.LimpaGrade(GradeQ, 0, 0, True);
      MyObjects.LimpaGrade(GradeF, 0, 0, True);
      MyObjects.LimpaGrade(GradeD, 0, 0, True);
      MyObjects.LimpaGrade(GradeL, 0, 0, True);
      MyObjects.LimpaGrade(GradeC, 0, 0, True);
      MyObjects.LimpaGrade(GradeA, 0, 0, True);
      MyObjects.LimpaGrade(GradeX, 0, 0, True);
      //
      if QrLista.RecordCount = 0 then
        Application.MessageBox(PChar('Nenhuma lista est� definida com o tipo ' +
        'de movimento "Sa�da" na regra fiscal "' + FmFatConCad.QrFatConCadNO_FisRegCad.Value
        + '" para a empresa ' + FormatFloat('000', FmFatConCad.QrFatConCadEmpresa.Value)
        + '!'), 'Aviso', MB_OK+MB_ICONWARNING)
      else
        Application.MessageBox(PChar('H� ' + IntToStr(QrLista.RecordCount) +
        'listas habilitadas para o movimento "Sa�da" na regra fiscal "' +
        FmFatConCad.QrFatConCadNO_FisRegCad.Value + '" para a empresa ' +
        FormatFloat('000', FmFatConCad.QrFatConCadEmpresa.Value) + '. Nenhuma ser�' +
        'considerada!'), 'Aviso', MB_OK+MB_ICONWARNING);
    end;
  end;
end;

function TFmFatConCadGru.QtdeItemCorTam(GridP, GridC, GridX: TStringGrid): Boolean;
var
  Qtde: Double;
  c, l, GraGruX, Coluna, Linha: Integer;
begin
  Result := False;
  Screen.Cursor := crHourGlass;
  Coluna := GridP.Col;
  Linha  := GridP.Row;
  c := Geral.IMV(GridX.Cells[Coluna, 0]);
  l := Geral.IMV(GridX.Cells[0, Linha]);
  GraGruX := Geral.IMV(GridC.Cells[Coluna, Linha]);
  if c = 0 then
  begin
    Application.MessageBox('Tamanho n�o definido!', 'Aviso', MB_OK+MB_ICONWARNING);
    Screen.Cursor := crDefault;
    Exit;
  end;
  if l = 0 then
  begin
    Application.MessageBox('Cor n�o definida!', 'Aviso', MB_OK+MB_ICONWARNING);
    Screen.Cursor := crDefault;
    Exit;
  end;
  if GraGruX = 0 then
  begin
    Application.MessageBox('O item nunca foi ativado!', 'Aviso', MB_OK+MB_ICONWARNING);
    Screen.Cursor := crDefault;
    Exit;
  end;
  if (c = 0) or (l = 0) or (GraGruX = 0) then Exit;
  //
  Qtde  := Geral.DMV(GridP.Cells[Coluna, Linha]);
  if ObtemQtde(Qtde) then
    GridP.Cells[Coluna, Linha] := FloatToStr(Qtde);
  Result := True;
  Screen.Cursor := crDefault;
end;

function TFmFatConCadGru.QtdeVariosItensCorTam(GridP, GridA, GridC, GridX:
TStringGrid): Boolean;
var
  Coluna, Linha, c, l, Ativos, ColI, ColF, RowI, RowF,
  CountC, CountL, GraGruX: Integer;
  Qtde: Double;
  QtdeTxt: String;
begin
  Result := False;
  Screen.Cursor := crHourGlass;
  Ativos := 0;
  RowI := 0;
  RowF := 0;
  ColI := 0;
  ColF := 0;
  Coluna := GridP.Col;
  Linha  := GridP.Row;
  if (Coluna = 0) and (Linha = 0) then
  begin
    ColI := 1;
    ColF := GridP.ColCount - 1;
    RowI := 1;
    RowF := GridP.RowCount - 1;
  end else if Coluna = 0 then
  begin
    ColI := 1;
    ColF := GridP.ColCount - 1;
    RowI := Linha;
    RowF := Linha;
  end else if Linha = 0 then
  begin
    ColI := Coluna;
    ColF := Coluna;
    RowI := 1;
    RowF := GridP.RowCount - 1;
  end;
  //
  CountC := 0;
  CountL := 0;
  for c := ColI to ColF do
    for l := RowI to RowF do
  begin
    CountC := CountC + Geral.IMV(GridX.Cells[c, 0]);
    CountL := CountL + Geral.IMV(GridX.Cells[0, l]);
  end;
  if CountC = 0 then
  begin
    Application.MessageBox('Tamanho n�o definido!', 'Aviso', MB_OK+MB_ICONWARNING);
    Screen.Cursor := crDefault;
    Exit;
  end;
  if CountL = 0 then
  begin
    Application.MessageBox('Cor n�o definida!', 'Aviso', MB_OK+MB_ICONWARNING);
    Screen.Cursor := crDefault;
    Exit;
  end;
  //
  Qtde   := Geral.IMV(GridP.Cells[ColI, RowI]);
  for c := ColI to ColF do
    for l := RowI to RowF do
      Ativos := Ativos + Geral.IMV(GridA.Cells[c, l]);
  if Ativos = 0 then
  begin
    Application.MessageBox(PChar('N�o h� nenhum item com c�digo na sele��o ' +
    'para que se possa incluir / alterar o pre�o!'), 'Aviso',
    MB_OK+MB_ICONWARNING);
    Screen.Cursor := crDefault;
    Exit;
  end;
  //
  if ObtemQtde(Qtde) then
  begin
    QtdeTxt := FloatToStr(Qtde);
    for c := ColI to ColF do
      for l := RowI to RowF do
    begin
      GraGruX := Geral.IMV(GridC.Cells[c, l]);
      //
      if GraGruX > 0 then
        GradeQ.Cells[c,l] := QtdeTxt;
    end;
    Result := True;
  end;
  Screen.Cursor := crDefault;
end;

function TFmFatConCadGru.ObtemQtde(var Qtde: Double): Boolean;
var
  ResVar: Variant;
begin
  Qtde := 0;
  Result := False;
  if MyObjects.GetValorDmk(TFmGetValor, FmGetValor, dmktfDouble,
  0, 3, 0, '', '', True, 'Itens', 'Informe a quantidade de itens: ',
  0, ResVar) then
  begin
    Qtde := Geral.IMV(ResVar);
    Result := True;
  end;
end;

function TFmFatConCadGru.DescontoVariosItensCorTam(GridD, GridA, GridC,
  GridX: TStringGrid): Boolean;
var
  Coluna, Linha, c, l, Ativos, ColI, ColF, RowI, RowF,
  CountC, CountL: Integer;
  Desconto: Double;
  DescontoTxt, ItensTxt: String;
begin
  Result := False;
  Screen.Cursor := crHourGlass;
  Ativos := 0;
  RowI := 0;
  RowF := 0;
  ColI := 0;
  ColF := 0;
  Coluna := GridD.Col;
  Linha  := GridD.Row;
  if (Coluna = 0) and (Linha = 0) then
  begin
    ColI := 1;
    ColF := GridD.ColCount - 1;
    RowI := 1;
    RowF := GridD.RowCount - 1;
  end else if Coluna = 0 then
  begin
    ColI := 1;
    ColF := GridD.ColCount - 1;
    RowI := Linha;
    RowF := Linha;
  end else if Linha = 0 then
  begin
    ColI := Coluna;
    ColF := Coluna;
    RowI := 1;
    RowF := GridD.RowCount - 1;
  end;
  //
  CountC := 0;
  CountL := 0;
  for c := ColI to ColF do
    for l := RowI to RowF do
  begin
    CountC := CountC + Geral.IMV(GridX.Cells[c, 0]);
    CountL := CountL + Geral.IMV(GridX.Cells[0, l]);
  end;
  if CountC = 0 then
  begin
    Application.MessageBox('Tamanho n�o definido!', 'Aviso', MB_OK+MB_ICONWARNING);
    Screen.Cursor := crDefault;
    Exit;
  end;
  if CountL = 0 then
  begin
    Application.MessageBox('Cor n�o definida!', 'Aviso', MB_OK+MB_ICONWARNING);
    Screen.Cursor := crDefault;
    Exit;
  end;
  //
  //Nivel1 := QrGraGru1Nivel1.Value;
  Desconto  := Geral.DMV(GridD.Cells[ColI, RowI]);
  for c := ColI to ColF do
    for l := RowI to RowF do
      Ativos := Ativos + Geral.IMV(GridA.Cells[c, l]);
  if Ativos = 0 then
  begin
    Application.MessageBox(PChar('N�o h� nenhum item com c�digo na sele��o ' +
    'para que se possa incluir / alterar o pre�o!'), 'Aviso',
    MB_OK+MB_ICONWARNING);
    Screen.Cursor := crDefault;
    Exit;
  end;
  //
  if ObtemDesconto(Desconto) then
  begin
    DescontoTxt := Geral.FFT(Desconto, 2, siNegativo);
    if (Coluna = 0) and (Linha = 0) then
    begin
      ItensTxt := 'todo grupo';
    end else if Coluna = 0 then
    begin
      ItensTxt := 'todos tamanhos da cor ' + GridA.Cells[0, Linha];
    end else if Linha = 0 then
    begin
      ItensTxt := 'todas cores do tamanho ' + GridA.Cells[Coluna, 0];
    end;
    if Application.MessageBox(PChar('Confirma o valor de ' + DescontoTxt + ' para ' +
    ItensTxt + '?'), 'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) <> ID_YES then
    begin
      Screen.Cursor := crDefault;
      Exit;
    end;
    //
    for c := ColI to ColF do
      for l := RowI to RowF do
        GridD.Cells[c, l] := DescontoTxt;
    Result := True;
  end;
  Screen.Cursor := crDefault;
end;

function TFmFatConCadGru.ObtemDesconto(var Desconto: Double): Boolean;
begin
  Result := False;
  if DBCheck.CriaFm(TFmGraGruVal, FmGraGruVal, afmoNegarComAviso) then
  begin
    FmGraGruVal.EdValor.ValueVariant := Desconto;
    FmGraGruVal.ShowModal;
    Result   := FmGraGruVal.FConfirmou;
    Desconto := FmGraGruVal.EdValor.ValueVariant;
    FmGraGruVal.Destroy;
  end;
end;

end.
