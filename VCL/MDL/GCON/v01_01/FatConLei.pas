unit FatConLei;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB,
  DB, mySQLDbTables, Mask, Variants, Grids, DBGrids, ComCtrls, dmkLabel,
  dmkDBEdit, dmkDBGrid, dmkImage, UnDmkEnums, UnDmkProcFunc;

type
  TFmFatConLei = class(TForm)
    PainelConfirma: TPanel;
    PainelTitulo: TPanel;
    Image1: TImage;
    Panel1: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    QrItem: TmySQLQuery;
    DsItem: TDataSource;
    QrItemNOMENIVEL1: TWideStringField;
    QrItemGraCorCad: TIntegerField;
    QrItemNOMECOR: TWideStringField;
    QrItemNOMETAM: TWideStringField;
    QrItemGraGruX: TIntegerField;
    QrItemGraGru1: TIntegerField;
    BtExclui: TBitBtn;
    CkFixo: TCheckBox;
    QrPreco: TmySQLQuery;
    DsPreco: TDataSource;
    QrFator: TmySQLQuery;
    QrItemCU_Nivel1: TIntegerField;
    QrItemIPI_Alq: TFloatField;
    QrFatConIts: TmySQLQuery;
    QrPrecoPrecoF: TFloatField;
    QrPrecoQuantF: TFloatField;
    QrItemMadeBy: TSmallintField;
    DsFatConIts: TDataSource;
    PnLeitura: TPanel;
    Panel4: TPanel;
    Label3: TLabel;
    LaQtdeLei: TLabel;
    EdLeitura: TEdit;
    EdQtdLei: TdmkEdit;
    BtOK: TBitBtn;
    Panel9: TPanel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    PnSimu: TPanel;
    Label9: TLabel;
    Label8: TLabel;
    Label7: TLabel;
    dmkEdit2: TdmkEdit;
    dmkEdit3: TdmkEdit;
    dmkEdit1: TdmkEdit;
    Button1: TButton;
    DBGFatConIts: TDBGrid;
    EdPrecoO: TdmkEdit;
    Label1: TLabel;
    Panel3: TPanel;
    ST1: TStaticText;
    ST2: TStaticText;
    ST3: TStaticText;
    Label2: TLabel;
    EdPrecoR: TdmkEdit;
    EdDescoP: TdmkEdit;
    Label10: TLabel;
    QrFatConItsCU_Nivel1: TIntegerField;
    QrFatConItsNO_Nivel1: TWideStringField;
    QrFatConItsCU_Cor: TIntegerField;
    QrFatConItsNO_Cor: TWideStringField;
    QrFatConItsNO_Tam: TWideStringField;
    QrFatConItsGraGruX: TIntegerField;
    QrFatConItsPrecoF: TFloatField;
    QrFatConItsQuantP: TFloatField;
    QrLista: TmySQLQuery;
    QrListaGraCusPrc: TIntegerField;
    PnJuros: TPanel;
    Label11: TLabel;
    Label12: TLabel;
    DBEdit4: TDBEdit;
    EdCustoFin: TdmkEdit;
    QrFatConItsControle: TIntegerField;
    QrExiste_: TmySQLQuery;
    QrExiste_Controle: TIntegerField;
    QrExiste_QuantP: TFloatField;
    QrFatConItsQuantC: TFloatField;
    QrFatConItsQuantV: TFloatField;
    QrExiste_PrecoR: TFloatField;
    QrExiste_DescoP: TFloatField;
    QrFatConItsValLiq: TFloatField;
    Panel5: TPanel;
    DBEdit5: TDBEdit;
    Label13: TLabel;
    Label14: TLabel;
    DBEdit6: TDBEdit;
    QrTotal: TmySQLQuery;
    DsTotal: TDataSource;
    QrTotalQtdLei: TFloatField;
    QrTotalValor: TFloatField;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure EdLeituraChange(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure EdQtdLeiKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure BtExcluiClick(Sender: TObject);
    procedure EdLeituraKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure QrItemBeforeClose(DataSet: TDataSet);
    procedure EdQtdLeiEnter(Sender: TObject);
    procedure CkFixoClick(Sender: TObject);
  private
    { Private declarations }
    FOriCtrl,
    FTam20: Boolean;
    FGraGruX, FSequencia: Integer;
    function ReopenPreco(): Boolean;
    function ReopenItem(): Boolean;
    procedure InsereItem();
  public
    { Public declarations }
    procedure ReopenFatConIts(GraGruX: Integer);
    procedure AtualizaEReabre(Codigo, Nivel1, GraGruX: Integer);

  end;

  var
  FmFatConLei: TFmFatConLei;

implementation

uses UnMyObjects, Module, UMySQLModule, dmkGeral, ModuleGeral, UnInternalConsts,
MyDBCheck, GetValor, Principal, ModPediVda,
(*,ModFatuVda*) ModProd, (*Tokyo > ?? PediVda,*) FatPedCab, QuaisItens, FatConCad;

{$R *.DFM}

const
  FThisType = 003;

procedure TFmFatConLei.AtualizaEReabre(Codigo, Nivel1, GraGruX: Integer);
begin
  DmPediVda.AtzSdosCondicional_Saida(Codigo);
  FmFatConCad.LocCod(Codigo, Codigo);
  FmFatConCad.ReopenGrupos(Nivel1);
  ReopenFatConIts(GraGruX);
end;

procedure TFmFatConLei.BtExcluiClick(Sender: TObject);
var
  Casas, AExcluir, Codigo, Nivel1, Controle: Integer;
  QuantP, PrecoR, DescoP, DescoI, DescoV, ValBru, ValLiq, Indevidos,
  Excluidos: Double;
begin
  if QrFatConItsQuantV.Value > 0 then
  begin
    Geral.MensagemBox('Exclus�o negada! J� existe faturamento para este ' +
    'item!', 'Aviso', MB_OK+MB_ICONWARNING);
    Exit;
  end;
  if Geral.MensagemBox('Confirma a exclus�o do item selecionado?', 'Aviso',
  MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
  begin
    Codigo   := FmFatConCad.QrFatConCadCodigo.Value;
    Nivel1   := FmFatConCad.QrGruposNivel1.Value;
    Controle := QrFatConItsControle.Value;
    //
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('DELETE FROM stqmovitsa WHERE Tipo=:P0');
    Dmod.QrUpd.SQL.Add('AND OriCodi=:P1 AND OriCtrl=:P2 ');
    Dmod.QrUpd.Params[00].AsInteger := FThisType;
    Dmod.QrUpd.Params[01].AsInteger := Codigo;
    Dmod.QrUpd.Params[02].AsInteger := Controle;
    Dmod.QrUpd.ExecSQL;
    //
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('DELETE FROM fatconits WHERE Controle=:Pa');
    Dmod.QrUpd.Params[00].AsInteger := Controle;
    Dmod.QrUpd.ExecSQL;
    //
    AtualizaEReabre(Codigo, Nivel1, 0);
  end;
end;

procedure TFmFatConLei.BtOKClick(Sender: TObject);
begin
  InsereItem();
end;

procedure TFmFatConLei.InsereItem();
const
  Tipo = 003;
var
  i, Nivel1, GraGruX, Sequencia, Codigo, Controle: Integer;
  Qtd, PrecoO, PrecoR, PrecoF, QuantP, ValCal, ValBru, ValLiq,
  DescoP, DescoV, DescoI: Double;
  Txt1, Txt2: String;
  ErrPreco, AvisoPrc, QtdRed, Casas, Conta: Integer;
  //
  OriCtrl, IDCtrl, StqCenCad, OriCodi, Empresa, OriCnta,
  AlterWeb, Ativo: Integer;
  Qtde: Double;
  DataHora: String;
begin
  if (FGragruX = 0) then
  begin
    Application.MessageBox('Defina o produto!', 'Aviso', MB_OK+MB_ICONWARNING);
    EdLeitura.SetFocus;
    Exit;
  end;
    //
    DataHora  := FormatDateTime('yyyy-mm-dd hh:nn:ss', DModG.ObtemAgora());
    StqCenCad := FmFatConCad.QrFatConCadStqCenCad.Value;
    OriCodi   := FmFatConCad.QrFatConCadCodigo.Value;
    Empresa   := FmFatConCad.QrFatConCadEmpresa.Value;
    OriCnta   := 0;
    AlterWeb  := 1;
    Ativo     := 1;
    //
  Nivel1 := QrItemGraGru1.Value;
  Screen.Cursor := crHourGlass;
  try
    Codigo := FmFatConCad.QrFatConCadCodigo.Value;
    ErrPreco  := 0;
    AvisoPrc  := 0;
    QtdRed    := 0;
    Qtd := Geral.IMV(EdQtdLei.Text);
    if Qtd = 0 then
    begin
      Screen.Cursor := crDefault;
      Application.MessageBox(
        'Definina a quantidade!', 'Aviso', MB_OK+MB_ICONWARNING);
      Exit;
    end;
    PrecoR := Geral.DMV(EdPrecoR.Text);
    if PrecoR = 0 then
    begin
      Screen.Cursor := crDefault;
      Application.MessageBox('Defina o pre�o de faturamento!', 'Aviso',
        MB_OK+MB_ICONWARNING);
      // est� desabilitado
      //EdPrecoR.SetFocus;
      Exit;
    end;
    if FGraGruX > 0 then
    begin
      GraGruX    := FGraGruX;
      FGraGruX   := 0;
      Sequencia  := FSequencia;
      FSequencia := 0;
      QuantP     := Geral.DMV(EdQtdLei.Text);
      //Conta := UMyMod.BuscaEmLivreY_Def('pedivdalei', 'Conta', ImgTipo.SQLType, 0);

      { � complicado fazer por causa do StqMovItsA !!!
      QrExiste.Close;
      QrExiste.Params[00].AsInteger := Codigo;
      QrExiste.Params[01].AsInteger := GraGruX;
      UMyMod.AbreQuery(QrExiste, 'procedure TFmFatConLei.InsereItem()');
      if QrExisteControle.Value > 0 then
      begin
        Controle := QrExisteControle.Value;
        QuantP   := QuantP + QrExisteQuantP.Value;
        ImgTipo.SQLType := stUpd;
      end else begin
      }
        ImgTipo.SQLType := stIns;
        Controle := UMyMod.BuscaEmLivreY_Def('fatconits', 'Controle',
          ImgTipo.SQLType, 0);
      //end;
      Casas      := Dmod.QrControleCasasProd.Value;
      PrecoO     := Geral.DMV(EdPrecoO.Text);
      ValCal     := PrecoR;// * QuantP;
      DescoP     := Geral.DMV(EdDescoP.Text);
      DescoI     := dmkPF.FFF(PrecoR * DescoP / 100, Casas, siPositivo);
      PrecoF     := PrecoR - DescoI;
      DescoV     := DescoI * QuantP;
      ValBru     := PrecoR * QuantP;
      ValLiq     := ValBru - DescoV;
      //
      {
      if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'fatconits', False,[
      'Codigo', 'GraGruX', 'PrecoO', 'PrecoR', 'QuantP', 'ValBru',
      'DescoP', 'DescoV', 'ValLiq', 'PrecoF'], ['Controle'],
      [Codigo, GraGruX, PrecoO, PrecoR, QuantP, ValBru,
      DescoP, DescoV, ValLiq, PrecoF], [Controle],
      True) then
      DmPediVda.AtzSdosPedido(Codigo);
      }
      if PrecoR <> 0 then
      begin
        Qtde      := QuantP * FmFatConCad.QrFatConCadFATOR.Value;
        {
        Preco     := PrecoR;
        Valor     := ValLiq;
        }
        OriCtrl   := 0;
        OriCtrl   := DModG.BuscaProximoCodigoInt('controle', 'stqmov003', '', OriCtrl);
        Controle  := OriCtrl;
        //
        IDCtrl := UMyMod.Busca_IDCtrl_NFe(stIns, 0);
        if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'stqmovitsa', False, [
        'DataHora', 'Tipo', 'OriCodi',
        'OriCtrl', 'Empresa', 'StqCenCad',
        'GraGruX', 'Qtde', 'OriCnta',
        'OriPart', 'AlterWeb', 'Ativo'], [
        'IDCtrl'], [
        DataHora, Tipo, OriCodi,
        OriCtrl, Empresa, StqCenCad,
        GraGruX, Qtde, OriCnta,
        OriCtrl, AlterWeb, Ativo], [
        IDCtrl], False) then
        begin
          if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'fatconits', False, [
          'Codigo', 'GraGruX', 'PrecoO',
          'PrecoR', 'QuantP',
          'ValBru', 'DescoP',
          'DescoV', 'ValLiq', 'PrecoF'], [
          'Controle'], [
          Codigo, GraGruX, PrecoO,
          PrecoR, QuantP,
          ValBru, DescoP,
          DescoV, ValLiq, PrecoF], [
          Controle], True) then
          {
          if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'fatconits', False, [
          'Codigo', 'GraGruX', 'Qtde', 'Preco', 'Valor'], ['Controle'], [
          OriCodi, GraGruX, Qtde, Preco, Valor], [OriCtrl], True) then
          }
          begin
          //
          end;
        end;
      end else
        Application.MessageBox(PChar('Valor (ou pre�o) n�o definido ' +
        'para o reduzido ' + IntToStr(GraGruX) +
        '. Ele n�o ser� incluido!'), 'Aviso',
        MB_OK+MB_ICONWARNING);
    end;
    ImgTipo.SQLType := stIns;
    EdLeitura.Text := '';
    EdLeitura.SetFocus;
    //
    AtualizaEReabre(Codigo, FmFatConCad.QrGruposNivel1.Value, GraGruX);
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmFatConLei.QrItemBeforeClose(DataSet: TDataSet);
begin
  QrPreco.Close;
  //#
  PnSimu.Visible := True;
end;

procedure TFmFatConLei.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmFatConLei.Button1Click(Sender: TObject);
const
  Tick = 25;
var
  i: Integer;
begin
  EdLeitura.Text := '';
  //
  for i := 1 to Length(dmkEdit1.Text) do
  begin
    EdLeitura.Text := EdLeitura.Text + dmkEdit1.Text[i];
    sleep(Tick);
  end;
  //
  for i := 1 to Length(dmkEdit2.Text) do
  begin
    EdLeitura.Text := EdLeitura.Text + dmkEdit2.Text[i];
    sleep(Tick);
  end;
  //
  for i := 1 to Length(dmkEdit3.Text) do
  begin
    EdLeitura.Text := EdLeitura.Text + dmkEdit3.Text[i];
    sleep(Tick);
  end;
end;

procedure TFmFatConLei.CkFixoClick(Sender: TObject);
begin
  EdQtdLei.Enabled := not CkFixo.Checked;
end;

procedure TFmFatConLei.EdLeituraChange(Sender: TObject);
var
  Tam: Integer;
begin
  Tam := Length(EdLeitura.Text);
  if Tam = 20 then
  begin
    FGraGruX   := Geral.IMV(Copy(EdLeitura.Text, 07, 6));
    FSequencia := Geral.IMV(Copy(EdLeitura.Text, 13, 8));
    if ReopenItem() then
    begin
      if CkFixo.Checked then
      begin
        FTam20 := True;
        InsereItem();
        EdLeitura.SetFocus;
        EdLeitura.Text := '';
      end else EdQtdLei.SetFocus;
    end;
  end else if Tam = 0 then QrItem.Close;
end;

procedure TFmFatConLei.EdLeituraKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  GraGruX: Integer;  
begin
  if Key = VK_RETURN then
  begin
    if Length(EdLeitura.Text) <= 6 then
    begin
      GraGruX := Geral.IMV(EdLeitura.Text);
      ReopenItem();
    end else Application.MessageBox(PChar('Quantidade de caracteres inv�lida para ' +
    'localiza��o pelo reduzido!'), 'Aviso', MB_OK+MB_ICONWARNING);
  end;
end;

procedure TFmFatConLei.EdQtdLeiEnter(Sender: TObject);
begin
  if CkFixo.Checked and (EdLeitura.Text = '') and FTam20 then
  begin
    FTam20 := False;
    // est� desabilitado
    //EdPrecoR.SetFocus;
  end;
end;

procedure TFmFatConLei.EdQtdLeiKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_RETURN then
    InsereItem();
end;

procedure TFmFatConLei.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmFatConLei.FormCreate(Sender: TObject);
begin
  if VAR_USUARIO = -1 then
    PnSimu.Visible := True;
  ST1.Caption := '';
  ST2.Caption := '';
  ST3.Caption := '';
  ST1.Font.Color := clRed;
  ST2.Font.Color := clRed;
  ST3.Font.Color := clRed;
  ST1.Color := PnLeitura.Color;
  ST2.Color := PnLeitura.Color;
  ST3.Color := PnLeitura.Color;
  //
  ReopenFatConIts(0);
  EdQtdLei.ValueVariant := 1;
end;

procedure TFmFatConLei.FormResize(Sender: TObject);
begin
  // M L A G e r a l.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

function TFmFatConLei.ReopenItem(): Boolean;
begin
  Result := False;
  QrItem.Close;
  if FGraGruX > 0 then
  begin
    QrItem.Params[0].AsInteger := FGraGrux;
    UMyMod.AbreQuery(QrItem, Dmod.MyDB, 'TFmFatConLei.FormResize()');
    //
    if QrItem.RecordCount > 0 then
    begin
      Result := True;
      // est� desabilitado
      //EdPrecoR.SetFocus;
      //# PnLido.Visible := True;
      //#
      PnSimu.Visible := False;
      ReopenPreco();
    end else
      Application.MessageBox('Reduzido n�o localizado!', 'Aviso',
      MB_OK+MB_ICONWARNING);
  end;
end;

procedure TFmFatConLei.ReopenFatConIts(GraGruX: Integer);
begin
  QrFatConIts.Close;
  QrFatConIts.Params[0].AsInteger := FmFatConCad.QrFatConCadCodigo.Value;
  UMyMod.AbreQuery(QrFatConIts, Dmod.MyDB, 'TFmFatConLei.ReopenFatConIts()');
  //
  if GraGruX <> 0 then
    QrFatConIts.Locate('GraGruX', GraGruX, []);
  //
  QrTotal.Close;
  QrTotal.Params[0].AsInteger := FmFatConCad.QrFatConCadCodigo.Value;
  QrTotal.Open;
end;

function TFmFatConLei.ReopenPreco(): Boolean;
var
  MedDDSimpl, MedDDReal, MediaSel, TaxaM, Juros: Double;
  Tabela, CondicaoPG, Lista, TipMediaDD, FatSemPrcL: Integer;
begin
  PnJuros.Visible := False;
  Tabela     := FmFatConCad.QrFatConCadTabelaPrc.Value;
  MedDDSimpl := 0;//FmFatConCad.QrFatConCadMedDDSimpl.Value;
  MedDDReal  := 0;//FmFatConCad.QrFatConCadMedDDReal.Value;
  CondicaoPG := 0;//FmFatConCad.QrFatConCadCondicaoPG.Value;
  TipMediaDD := DmPediVda.QrParamsEmpTipMediaDD.Value;
  FatSemPrcL := DmPediVda.QrParamsEmpFatSemPrcL.Value;
  //
  if Tabela > 0 then
  begin
    DmProd.Atualiza_dmkEditsPrecos3(FGraGruX, Tabela, CondicaoPG,
      EdPrecoO, EdPrecoR, MedDDSimpl, MedDDReal, ST1, ST2, ST3, ImgTipo, True,
      TipMediaDD, FatSemPrcL);
    //
  end else begin
    QrLista.Close;
    QrLista.Params[00].AsInteger := FmFatConCad.QrFatConCadEmpresa.Value;
    QrLista.Params[01].AsInteger := FmFatConCad.QrFatConCadFisRegCad.Value;
    UMyMod.AbreQuery(QrLista, Dmod.MyDB, 'TFmFatConLei.ReopenPreco()');
    if QrLista.RecordCount = 1 then
    begin
      Lista := QrListaGraCusPrc.Value;
      case DmPediVda.QrParamsEmpTipMediaDD.Value of
        1: MediaSel := MedDDSimpl;
        2: MediaSel := MedDDReal;
        else MediaSel := 0;
      end;
      TaxaM := 0;//FmFatConCad.QrFatConCadJurosMes.Value;
      case DmPediVda.QrParamsEmpTipCalcJuro.Value of
        1: Juros := dmkPF.CalculaJuroSimples(TaxaM, MediaSel);
        2: Juros := dmkPF.CalculaJuroComposto(TaxaM, MediaSel);
        else Juros := 0;
      end;
      EdCustoFin.ValueVariant := Juros;
      PnJuros.Visible := True;
      DmProd.Atualiza_dmkEditsPrecos4(FGraGruX, Lista, EdPrecoO, EdPrecoR, Juros);
    end else begin
      if QrLista.RecordCount = 0 then
        Application.MessageBox(PChar('Nenhuma lista est� definida com o tipo ' +
        'de movimento "Sa�da" na regra fiscal "' + FmFatConCad.QrFatConCadNO_FisRegCad.Value
        + '" para a empresa ' + FormatFloat('000', FmFatConCad.QrFatConCadEmpresa.Value)
        + '!'), 'Aviso', MB_OK+MB_ICONWARNING)
      else
        Application.MessageBox(PChar('H� ' + IntToStr(QrLista.RecordCount) +
        'listas habilitadas para o movimento "Sa�da" na regra fiscal "' +
        FmFatConCad.QrFatConCadNO_FisRegCad.Value + '" para a empresa ' +
        FormatFloat('000', FmFatConCad.QrFatConCadEmpresa.Value) + '. Nenhuma ser�' +
        'considerada!'), 'Aviso', MB_OK+MB_ICONWARNING);
    end;
  end;
end;

end.

