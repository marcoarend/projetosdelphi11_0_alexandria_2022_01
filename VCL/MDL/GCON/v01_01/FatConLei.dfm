object FmFatConLei: TFmFatConLei
  Left = 339
  Top = 185
  Caption = 'FAT-CONDI-004 :: Faturamento Condicional - Adi'#231#227'o por Leitura'
  ClientHeight = 523
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 1008
    Height = 427
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object DBGFatConIts: TDBGrid
      Left = 0
      Top = 105
      Width = 1008
      Height = 269
      Align = alClient
      DataSource = DsFatConIts
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
      TabOrder = 2
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'CU_Nivel1'
          Title.Caption = 'Produto'
          Width = 68
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_Nivel1'
          Title.Caption = 'Descri'#231#227'o'
          Width = 283
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CU_Cor'
          Title.Caption = 'Cor'
          Width = 59
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_Cor'
          Title.Caption = 'Descri'#231#227'o'
          Width = 175
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_Tam'
          Title.Caption = 'Tamanho'
          Width = 78
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'QuantP'
          Title.Caption = 'Quantidade'
          Width = 69
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'PrecoF'
          Title.Caption = 'Pre'#231'o'
          Width = 69
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ValLiq'
          Title.Caption = 'Val.Liq.'
          Visible = True
        end>
    end
    object PnLeitura: TPanel
      Left = 0
      Top = 0
      Width = 1008
      Height = 105
      Align = alTop
      BevelOuter = bvNone
      ParentBackground = False
      TabOrder = 0
      object Panel4: TPanel
        Left = 0
        Top = 0
        Width = 661
        Height = 48
        Align = alLeft
        TabOrder = 0
        object Label3: TLabel
          Left = 4
          Top = 4
          Width = 89
          Height = 13
          Caption = 'Leitura / digita'#231#227'o:'
        end
        object LaQtdeLei: TLabel
          Left = 188
          Top = 4
          Width = 26
          Height = 13
          Caption = 'Qtde:'
        end
        object Label1: TLabel
          Left = 232
          Top = 4
          Width = 63
          Height = 13
          Caption = 'Pre'#231'o tabela:'
          Enabled = False
        end
        object Label2: TLabel
          Left = 304
          Top = 4
          Width = 51
          Height = 13
          Caption = 'Pre'#231'o real:'
          Enabled = False
        end
        object Label10: TLabel
          Left = 376
          Top = 4
          Width = 57
          Height = 13
          Caption = '%Desconto:'
          Enabled = False
        end
        object EdLeitura: TEdit
          Left = 4
          Top = 20
          Width = 180
          Height = 21
          MaxLength = 20
          TabOrder = 0
          OnChange = EdLeituraChange
          OnKeyDown = EdLeituraKeyDown
        end
        object EdQtdLei: TdmkEdit
          Left = 188
          Top = 20
          Width = 40
          Height = 21
          Alignment = taRightJustify
          Enabled = False
          TabOrder = 1
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = True
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '1'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 1
          ValWarn = False
          OnEnter = EdQtdLeiEnter
          OnKeyDown = EdQtdLeiKeyDown
        end
        object BtOK: TBitBtn
          Tag = 14
          Left = 448
          Top = 3
          Width = 90
          Height = 40
          Caption = '&OK'
          NumGlyphs = 2
          TabOrder = 5
          OnClick = BtOKClick
        end
        object EdPrecoO: TdmkEdit
          Left = 232
          Top = 20
          Width = 68
          Height = 21
          Alignment = taRightJustify
          Enabled = False
          TabOrder = 2
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object EdPrecoR: TdmkEdit
          Left = 304
          Top = 20
          Width = 68
          Height = 21
          Alignment = taRightJustify
          Enabled = False
          TabOrder = 3
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object EdDescoP: TdmkEdit
          Left = 376
          Top = 20
          Width = 68
          Height = 21
          Alignment = taRightJustify
          Enabled = False
          TabOrder = 4
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object PnJuros: TPanel
          Left = 544
          Top = 1
          Width = 116
          Height = 46
          Align = alRight
          BevelOuter = bvLowered
          Enabled = False
          TabOrder = 6
          object Label11: TLabel
            Left = 4
            Top = 4
            Width = 52
            Height = 13
            Caption = '% Jur/m'#234's:'
            FocusControl = DBEdit4
          end
          object Label12: TLabel
            Left = 60
            Top = 4
            Width = 33
            Height = 13
            Caption = '% C.F.:'
            FocusControl = DBEdit4
          end
          object DBEdit4: TDBEdit
            Left = 4
            Top = 20
            Width = 53
            Height = 21
            DataField = 'JurosMes'
            TabOrder = 0
          end
          object EdCustoFin: TdmkEdit
            Left = 60
            Top = 20
            Width = 53
            Height = 21
            Alignment = taRightJustify
            TabOrder = 1
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 4
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,0000'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
        end
      end
      object Panel9: TPanel
        Left = 849
        Top = 0
        Width = 159
        Height = 48
        Align = alClient
        Enabled = False
        TabOrder = 1
        object Label4: TLabel
          Left = 4
          Top = 4
          Width = 86
          Height = 13
          Caption = 'Grupo de produto:'
          FocusControl = DBEdit1
        end
        object Label5: TLabel
          Left = 208
          Top = 4
          Width = 19
          Height = 13
          Caption = 'Cor:'
          FocusControl = DBEdit2
        end
        object Label6: TLabel
          Left = 292
          Top = 4
          Width = 48
          Height = 13
          Caption = 'Tamanho:'
          FocusControl = DBEdit3
        end
        object DBEdit1: TDBEdit
          Left = 4
          Top = 20
          Width = 200
          Height = 21
          DataField = 'NOMENIVEL1'
          DataSource = DsItem
          TabOrder = 0
        end
        object DBEdit2: TDBEdit
          Left = 208
          Top = 20
          Width = 80
          Height = 21
          DataField = 'NOMECOR'
          DataSource = DsItem
          TabOrder = 1
        end
        object DBEdit3: TDBEdit
          Left = 292
          Top = 20
          Width = 49
          Height = 21
          DataField = 'NOMETAM'
          DataSource = DsItem
          TabOrder = 2
        end
      end
      object Panel3: TPanel
        Left = 0
        Top = 48
        Width = 1008
        Height = 57
        Align = alBottom
        TabOrder = 2
        object ST1: TStaticText
          Left = 1
          Top = 2
          Width = 24
          Height = 18
          Align = alBottom
          Alignment = taCenter
          Caption = 'ST1'
          Color = clBtnFace
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clRed
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentColor = False
          ParentFont = False
          TabOrder = 0
          Transparent = False
        end
        object ST2: TStaticText
          Left = 1
          Top = 20
          Width = 24
          Height = 18
          Align = alBottom
          Alignment = taCenter
          Caption = 'ST2'
          Color = clBtnFace
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clRed
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentColor = False
          ParentFont = False
          TabOrder = 1
          Transparent = False
        end
        object ST3: TStaticText
          Left = 1
          Top = 38
          Width = 24
          Height = 18
          Align = alBottom
          Alignment = taCenter
          Caption = 'ST3'
          Color = clBtnFace
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clRed
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentColor = False
          ParentFont = False
          TabOrder = 2
          Transparent = False
        end
      end
      object Panel5: TPanel
        Left = 661
        Top = 0
        Width = 188
        Height = 48
        Align = alLeft
        TabOrder = 3
        object Label13: TLabel
          Left = 8
          Top = 4
          Width = 83
          Height = 13
          Caption = 'Total quantidade:'
          FocusControl = DBEdit5
        end
        object Label14: TLabel
          Left = 96
          Top = 4
          Width = 53
          Height = 13
          Caption = 'Total valor:'
          FocusControl = DBEdit6
        end
        object DBEdit5: TDBEdit
          Left = 8
          Top = 20
          Width = 85
          Height = 21
          TabStop = False
          DataField = 'QtdLei'
          DataSource = DsTotal
          TabOrder = 0
        end
        object DBEdit6: TDBEdit
          Left = 96
          Top = 20
          Width = 85
          Height = 21
          TabStop = False
          DataField = 'Valor'
          DataSource = DsTotal
          TabOrder = 1
        end
      end
    end
    object PnSimu: TPanel
      Left = 0
      Top = 374
      Width = 1008
      Height = 53
      Align = alBottom
      TabOrder = 1
      Visible = False
      object Label9: TLabel
        Left = 368
        Top = 4
        Width = 54
        Height = 13
        Caption = 'Sequ'#234'ncia:'
      end
      object Label8: TLabel
        Left = 284
        Top = 4
        Width = 48
        Height = 13
        Caption = 'Reduzido:'
      end
      object Label7: TLabel
        Left = 200
        Top = 4
        Width = 75
        Height = 13
        Caption = 'Ordem de serv.:'
      end
      object dmkEdit2: TdmkEdit
        Left = 284
        Top = 20
        Width = 81
        Height = 21
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 6
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '000010'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 10
        ValWarn = False
        OnChange = EdLeituraChange
      end
      object dmkEdit3: TdmkEdit
        Left = 368
        Top = 20
        Width = 81
        Height = 21
        Alignment = taRightJustify
        TabOrder = 1
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 8
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '00000001'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 1
        ValWarn = False
        OnChange = EdLeituraChange
      end
      object dmkEdit1: TdmkEdit
        Left = 200
        Top = 20
        Width = 81
        Height = 21
        Alignment = taRightJustify
        TabOrder = 2
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 6
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '000000'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = EdLeituraChange
      end
      object Button1: TButton
        Left = 4
        Top = 12
        Width = 185
        Height = 25
        Caption = 'Simula leitura do c'#243'digo de barras'
        TabOrder = 3
        OnClick = Button1Click
      end
    end
  end
  object PainelConfirma: TPanel
    Left = 0
    Top = 475
    Width = 1008
    Height = 48
    Align = alBottom
    TabOrder = 1
    object Panel2: TPanel
      Left = 896
      Top = 1
      Width = 111
      Height = 46
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 0
      object BtSaida: TBitBtn
        Tag = 13
        Left = 2
        Top = 3
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Hint = 'Sai da janela atual'
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object BtExclui: TBitBtn
      Tag = 12
      Left = 453
      Top = 4
      Width = 140
      Height = 40
      Caption = '&Excluir sequ'#234'ncia'
      NumGlyphs = 2
      TabOrder = 1
      OnClick = BtExcluiClick
    end
    object CkFixo: TCheckBox
      Left = 12
      Top = 16
      Width = 125
      Height = 17
      Caption = 'Quantidade fixa.'
      Checked = True
      State = cbChecked
      TabOrder = 2
      OnClick = CkFixoClick
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    Caption = 'Faturamento Condicional - Adi'#231#227'o por Leitura'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 2
    object Image1: TImage
      Left = 1
      Top = 1
      Width = 958
      Height = 46
      Align = alClient
      Transparent = True
      ExplicitLeft = 2
      ExplicitTop = 2
      ExplicitWidth = 788
      ExplicitHeight = 44
    end
    object GB_R: TGroupBox
      Left = 959
      Top = 1
      Width = 48
      Height = 46
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 6
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
  end
  object QrItem: TMySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrItemBeforeClose
    SQL.Strings = (
      'SELECT gg1.Nome NOMENIVEL1, ggc.GraCorCad, '
      'gcc.Nome NOMECOR,  gti.Nome NOMETAM, '
      'ggx.Controle GraGruX, ggx.GraGru1, '
      'gg1.CodUsu CU_Nivel1, gg1.IPI_Alq, pgt.MadeBy'
      'FROM gragrux ggx '
      'LEFT JOIN gragru1   gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN gragruc ggc ON ggc.Controle=ggx.GraGruC'
      'LEFT JOIN gracorcad gcc ON gcc.Codigo=ggc.GraCorCad'
      'LEFT JOIN gratamits gti ON gti.Controle=ggx.GraTamI'
      'LEFT JOIN prdgruptip pgt ON pgt.Codigo=gg1.PrdGrupTip'
      'WHERE ggx.Controle=:P0'
      '')
    Left = 68
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrItemNOMENIVEL1: TWideStringField
      FieldName = 'NOMENIVEL1'
      Origin = 'gragru1.Nome'
      Size = 30
    end
    object QrItemGraCorCad: TIntegerField
      FieldName = 'GraCorCad'
      Origin = 'gragruc.GraCorCad'
    end
    object QrItemNOMECOR: TWideStringField
      FieldName = 'NOMECOR'
      Origin = 'gracorcad.Nome'
      Size = 30
    end
    object QrItemNOMETAM: TWideStringField
      FieldName = 'NOMETAM'
      Origin = 'gratamits.Nome'
      Size = 5
    end
    object QrItemGraGruX: TIntegerField
      FieldName = 'GraGruX'
      Origin = 'gragrux.Controle'
      Required = True
    end
    object QrItemGraGru1: TIntegerField
      FieldName = 'GraGru1'
      Origin = 'gragrux.GraGru1'
      Required = True
    end
    object QrItemCU_Nivel1: TIntegerField
      FieldName = 'CU_Nivel1'
      Required = True
    end
    object QrItemIPI_Alq: TFloatField
      FieldName = 'IPI_Alq'
    end
    object QrItemMadeBy: TSmallintField
      FieldName = 'MadeBy'
    end
  end
  object DsItem: TDataSource
    DataSet = QrItem
    Left = 96
    Top = 8
  end
  object QrPreco: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT pvi.PrecoF, '
      '(QuantP-QuantC-QuantV) QuantF'
      'FROM fatconits pvi'
      'WHERE pvi.Codigo=:P0'
      'AND pvi.GraGruX=:P1'
      ' ')
    Left = 124
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrPrecoPrecoF: TFloatField
      FieldName = 'PrecoF'
      Required = True
    end
    object QrPrecoQuantF: TFloatField
      FieldName = 'QuantF'
      Required = True
    end
  end
  object DsPreco: TDataSource
    DataSet = QrPreco
    Left = 152
    Top = 8
  end
  object QrFator: TMySQLQuery
    Database = Dmod.MyDB
    Left = 180
    Top = 8
  end
  object QrFatConIts: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT gg1.CodUsu CU_Nivel1, gg1.Nome NO_Nivel1,  '
      'gcc.CodUsu CU_Cor, gcc.Nome NO_Cor, '
      'gti.Nome NO_Tam,'
      'pvi.GraGruX, pvi.PrecoF, pvi.QuantP, pvi.QuantC,'
      'pvi.QuantV, pvi.Controle, pvi.ValLiq'
      'FROM fatconits pvi'
      'LEFT JOIN gragrux   ggx ON ggx.Controle=pvi.GraGruX'
      'LEFT JOIN gragru1   gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN gratamits gti ON gti.Controle=ggx.GraTamI'
      'LEFT JOIN gragruc   ggc ON ggc.Controle=ggx.GraGruC'
      'LEFT JOIN gracorcad   gcc ON gcc.Codigo=ggc.GraCorCad'
      'WHERE pvi.Codigo=:P0'
      'ORDER BY Controle DESC'
      '')
    Left = 708
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrFatConItsCU_Nivel1: TIntegerField
      FieldName = 'CU_Nivel1'
    end
    object QrFatConItsNO_Nivel1: TWideStringField
      FieldName = 'NO_Nivel1'
      Size = 30
    end
    object QrFatConItsCU_Cor: TIntegerField
      FieldName = 'CU_Cor'
    end
    object QrFatConItsNO_Cor: TWideStringField
      FieldName = 'NO_Cor'
      Size = 30
    end
    object QrFatConItsNO_Tam: TWideStringField
      FieldName = 'NO_Tam'
      Size = 5
    end
    object QrFatConItsGraGruX: TIntegerField
      FieldName = 'GraGruX'
      Required = True
    end
    object QrFatConItsPrecoF: TFloatField
      FieldName = 'PrecoF'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrFatConItsQuantP: TFloatField
      FieldName = 'QuantP'
    end
    object QrFatConItsQuantC: TFloatField
      FieldName = 'QuantC'
      Required = True
    end
    object QrFatConItsQuantV: TFloatField
      FieldName = 'QuantV'
      Required = True
    end
    object QrFatConItsControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrFatConItsValLiq: TFloatField
      FieldName = 'ValLiq'
      Required = True
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
  end
  object DsFatConIts: TDataSource
    DataSet = QrFatConIts
    Left = 736
    Top = 8
  end
  object QrLista: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT GraCusPrc'
      'FROM fisregmvt'
      'WHERE TipoMov=1'
      'AND Empresa=:P0'
      'AND Codigo=:P1')
    Left = 208
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrListaGraCusPrc: TIntegerField
      FieldName = 'GraCusPrc'
    end
  end
  object QrExiste_: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle, QuantP, PrecoR, DescoP'
      'FROM fatconits'
      'WHERE Codigo=:P0'
      'AND GraGruX=:P1'
      '')
    Left = 764
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrExiste_Controle: TIntegerField
      FieldName = 'Controle'
    end
    object QrExiste_QuantP: TFloatField
      FieldName = 'QuantP'
    end
    object QrExiste_PrecoR: TFloatField
      FieldName = 'PrecoR'
    end
    object QrExiste_DescoP: TFloatField
      FieldName = 'DescoP'
    end
  end
  object QrTotal: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(sbi.QuantP) QtdLei, SUM(sbi.ValLiq) Valor'
      'FROM fatconits sbi'
      'WHERE sbi.Codigo=:P0')
    Left = 824
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrTotalQtdLei: TFloatField
      FieldName = 'QtdLei'
    end
    object QrTotalValor: TFloatField
      FieldName = 'Valor'
    end
  end
  object DsTotal: TDataSource
    DataSet = QrTotal
    Left = 852
    Top = 8
  end
end
