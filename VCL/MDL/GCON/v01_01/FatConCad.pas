unit FatConCad;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls, Db, (*DBTables,*) JPEG, ExtDlgs,
  ZCF2, ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt, Menus,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, dmkGeral,
  dmkPermissoes, dmkEdit, dmkLabel, dmkRadioGroup, Grids, DBGrids, dmkDBGrid,
  dmkCheckGroup, dmkDBLookupComboBox, dmkEditCB, dmkValUsu, ComCtrls,
  dmkEditDateTimePicker, frxClass, frxDBSet, Variants, UnDmkProcFunc, dmkImage,
  UnDmkEnums, DmkDAC_PF;

type
  TFmFatConCad = class(TForm)
    PainelDados: TPanel;
    DsFatConCad: TDataSource;
    QrFatConCad: TmySQLQuery;
    PainelTitulo: TPanel;
    Image1: TImage;
    PanelFill002: TPanel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    PainelEdita: TPanel;
    PainelConfirma: TPanel;
    BtConfirma: TBitBtn;
    PainelControle: TPanel;
    LaRegistro: TdmkLabel;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    Panel3: TPanel;
    BtItens: TBitBtn;
    PainelEdit: TPanel;
    EdCodigo: TdmkEdit;
    PnCabeca: TPanel;
    Label1: TLabel;
    DBEdCodigo: TDBEdit;
    DBEdNome: TDBEdit;
    Label2: TLabel;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    EdNome: TdmkEdit;
    dmkPermissoes1: TdmkPermissoes;
    Label3: TLabel;
    DBEdit1: TDBEdit;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    EdCodUsu: TdmkEdit;
    PMMovim: TPopupMenu;
    BtMovim: TBitBtn;
    Panel4: TPanel;
    QrFatConCadCodigo: TIntegerField;
    QrFatConCadCodUsu: TIntegerField;
    QrFatConCadNome: TWideStringField;
    QrFatConCadEmpresa: TIntegerField;
    QrFatConCadPrdGrupTip: TIntegerField;
    QrFatConCadStqCenCad: TIntegerField;
    QrFatConCadCasasProd: TSmallintField;
    QrFatConCadAbertura: TDateTimeField;
    QrFatConCadEncerrou: TDateTimeField;
    DsFiliais: TDataSource;
    QrFiliais: TmySQLQuery;
    QrFiliaisFilial: TIntegerField;
    QrFiliaisCodigo: TIntegerField;
    QrFiliaisNOMEFILIAL: TWideStringField;
    dmkLabel1: TdmkLabel;
    EdFilial: TdmkEditCB;
    CBFilial: TdmkDBLookupComboBox;
    dmkValUsu1: TdmkValUsu;
    QrFatConCadNOMEFILIAL: TWideStringField;
    Label4: TLabel;
    DBEdit2: TDBEdit;
    QrPrdGrupTip: TmySQLQuery;
    DsPrdGrupTip: TDataSource;
    EdPrdGrupTip: TdmkEditCB;
    CBPrdGrupTip: TdmkDBLookupComboBox;
    dmkLabel2: TdmkLabel;
    dmkValUsu2: TdmkValUsu;
    QrPrdGrupTipCodigo: TIntegerField;
    QrPrdGrupTipCodUsu: TIntegerField;
    QrPrdGrupTipNome: TWideStringField;
    QrFatConCadNOMEPRDGRUPTIP: TWideStringField;
    Label5: TLabel;
    DBEdit3: TDBEdit;
    QrFatConCadMOMESTQCENCAD: TWideStringField;
    Label6: TLabel;
    DBEdit4: TDBEdit;
    EdStqCenCad: TdmkEditCB;
    dmkLabel3: TdmkLabel;
    CBStqCenCad: TdmkDBLookupComboBox;
    dmkValUsu3: TdmkValUsu;
    QrStqCenCad: TmySQLQuery;
    DsStqCenCad: TDataSource;
    QrStqCenCadCodigo: TIntegerField;
    QrStqCenCadCodUsu: TIntegerField;
    QrStqCenCadNome: TWideStringField;
    RGCasasProd: TdmkRadioGroup;
    Label10: TLabel;
    EdAbertura: TdmkEdit;
    Label11: TLabel;
    DBRadioGroup1: TDBRadioGroup;
    DBEdit5: TDBEdit;
    QrFatConCadBalQtdItem: TFloatField;
    N1: TMenuItem;
    QrNew: TmySQLQuery;
    QrNewCodigo: TIntegerField;
    DBEdit6: TDBEdit;
    Label12: TLabel;
    QrFatConCadENCERROU_TXT: TWideStringField;
    QrSoma: TmySQLQuery;
    QrSomaQtde: TFloatField;
    Incluinovomovimento1: TMenuItem;
    Alteramovimentoatual1: TMenuItem;
    Encerramovimentoatual1: TMenuItem;
    EXcluimovimentoatual1: TMenuItem;
    QrFisRegCad: TmySQLQuery;
    DsFisRegCad: TDataSource;
    dmkValUsu4: TdmkValUsu;
    QrFatConCadFisRegCad: TIntegerField;
    QrFisRegCadCodigo: TIntegerField;
    QrFisRegCadCodUsu: TIntegerField;
    QrFisRegCadNome: TWideStringField;
    QrFisRegCadTipoMov: TSmallintField;
    QrFisRegCadNO_TipoMov: TWideStringField;
    QrFisRegCadTipoCalc: TSmallintField;
    QrFisRegCadNO_TipoCalc: TWideStringField;
    QrFatConCadStatus: TSmallintField;
    QrGrupos: TmySQLQuery;
    DsGrupos: TDataSource;
    dmkDBGrid1: TdmkDBGrid;
    QrFatConCadFatSemEstq: TSmallintField;
    QrFatConCadNO_FisRegCad: TWideStringField;
    QrFatConCadCU_FisRegCad: TIntegerField;
    QrFatConCadTipoMov: TSmallintField;
    QrFatConCadNO_TipoMov: TWideStringField;
    QrFatConCadTipoCalc: TSmallintField;
    QrFatConCadNO_TipoCalc: TWideStringField;
    QrFatConCadEntraSai: TSmallintField;
    QrFatConCadFATOR: TSmallintField;
    QrFatConCadGraCusPrc: TIntegerField;
    QrFatConCadCliente: TIntegerField;
    dmkValUsu5: TdmkValUsu;
    QrFatConCadNO_CLIENTE: TWideStringField;
    GroupBox3: TGroupBox;
    BtTabelaPrc: TSpeedButton;
    LaTabelaPrc: TLabel;
    SpeedButton9: TSpeedButton;
    Label22: TLabel;
    CBTabelaPrc: TdmkDBLookupComboBox;
    EdTabelaPrc: TdmkEditCB;
    CBMoeda: TdmkDBLookupComboBox;
    EdMoeda: TdmkEditCB;
    GroupBox6: TGroupBox;
    Label33: TLabel;
    EdModeloNF: TdmkEdit;
    Label25: TLabel;
    EdFisRegCad: TdmkEditCB;
    CBFisRegCad: TdmkDBLookupComboBox;
    SpeedButton5: TSpeedButton;
    Label13: TLabel;
    EdDBTipoMov: TDBEdit;
    EdDBTipoCalc: TDBEdit;
    Label14: TLabel;
    Label15: TLabel;
    EdEntraSai: TdmkEdit;
    GroupBox1: TGroupBox;
    CBCliente: TdmkDBLookupComboBox;
    EdCliente: TdmkEditCB;
    dmkLabel5: TdmkLabel;
    VuTabelaPrc: TdmkValUsu;
    QrFatConCadTabelaPrc: TIntegerField;
    QrFatConCadMoeda: TIntegerField;
    QrFisRegCadNO_MODELO_NF: TWideStringField;
    GroupBox2: TGroupBox;
    Label21: TLabel;
    Label23: TLabel;
    GroupBox4: TGroupBox;
    Label24: TLabel;
    Label26: TLabel;
    Label27: TLabel;
    Label28: TLabel;
    Label29: TLabel;
    GroupBox5: TGroupBox;
    dmkLabel4: TdmkLabel;
    DBEdit8: TDBEdit;
    DBEdit7: TDBEdit;
    DBEdit9: TDBEdit;
    DBEdit10: TDBEdit;
    DBEdit12: TDBEdit;
    DBEdit50: TDBEdit;
    DBEdit11: TDBEdit;
    DBEdit13: TDBEdit;
    DBEdit21: TDBEdit;
    DBEdit24: TDBEdit;
    DBEdit18: TDBEdit;
    DBEdit25: TDBEdit;
    QrFatConCadNOMEMODELONF: TWideStringField;
    QrFatConCadCODUSU_TPC: TIntegerField;
    QrFatConCadCODUSU_MDA: TIntegerField;
    QrFatConCadNOMETABEPRCCAD: TWideStringField;
    QrFatConCadNOMEMOEDA: TWideStringField;
    VuCambioMda: TdmkValUsu;
    PMItens: TPopupMenu;
    Incluinovositensdegrupo1: TMenuItem;
    IncluinovositensporLeitura1: TMenuItem;
    MenuItem1: TMenuItem;
    AlteraExcluiIncluiitemselecionado1: TMenuItem;
    QrGruposCU_NIVEL1: TIntegerField;
    QrGruposNO_NIVEL1: TWideStringField;
    QrGruposNivel1: TIntegerField;
    QrGruposGraTamCad: TIntegerField;
    QrGruposValor: TFloatField;
    QrGruposftiCtrl: TIntegerField;
    GroupBox13: TGroupBox;
    Label19: TLabel;
    Label16: TLabel;
    DBEdit14: TDBEdit;
    DBEdit15: TDBEdit;
    PageControl1: TPageControl;
    TabSheet5: TTabSheet;
    GradeQ: TStringGrid;
    StaticText1: TStaticText;
    TabSheet4: TTabSheet;
    GradeF: TStringGrid;
    TabSheet6: TTabSheet;
    GradeD: TStringGrid;
    StaticText3: TStaticText;
    TabSheet7: TTabSheet;
    GradeV: TStringGrid;
    TabSheet8: TTabSheet;
    GradeC: TStringGrid;
    StaticText6: TStaticText;
    TabSheet9: TTabSheet;
    GradeA: TStringGrid;
    StaticText2: TStaticText;
    TabSheet10: TTabSheet;
    GradeX: TStringGrid;
    QrFatConCadQuantP: TFloatField;
    QrFatConCadQuantC: TFloatField;
    QrFatConCadQuantV: TFloatField;
    QrFatConCadValLiq: TFloatField;
    QrGruposFracio: TSmallintField;
    SpeedButton6: TSpeedButton;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrFatConCadAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrFatConCadBeforeOpen(DataSet: TDataSet);
    procedure EdCodUsuKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SbNovoClick(Sender: TObject);
    procedure BtMovimClick(Sender: TObject);
    procedure PMMovimPopup(Sender: TObject);
    procedure QrFatConCadBeforeClose(DataSet: TDataSet);
    procedure QrFatConCadAfterScroll(DataSet: TDataSet);
    procedure BtItensClick(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
    procedure QrFatConCadCalcFields(DataSet: TDataSet);
    procedure DBEdit6Change(Sender: TObject);
    procedure Incluinovomovimento1Click(Sender: TObject);
    procedure Alteramovimentoatual1Click(Sender: TObject);
    procedure Encerramovimentoatual1Click(Sender: TObject);
    procedure EdFisRegCadChange(Sender: TObject);
    procedure EdPrdGrupTipChange(Sender: TObject);
    procedure EdStqCenCadChange(Sender: TObject);
    procedure EdFilialChange(Sender: TObject);
    procedure GradeQDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect;
      State: TGridDrawState);
    procedure GradeCDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect;
      State: TGridDrawState);
    procedure GradeADrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect;
      State: TGridDrawState);
    procedure QrGruposAfterScroll(DataSet: TDataSet);
    procedure SpeedButton5Click(Sender: TObject);
    procedure QrGruposBeforeClose(DataSet: TDataSet);
    procedure EdTabelaPrcChange(Sender: TObject);
    procedure EdFisRegCadExit(Sender: TObject);
    procedure Incluinovositensdegrupo1Click(Sender: TObject);
    procedure IncluinovositensporLeitura1Click(Sender: TObject);
    procedure GradeFDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect;
      State: TGridDrawState);
    procedure GradeDDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect;
      State: TGridDrawState);
    procedure GradeVDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect;
      State: TGridDrawState);
    procedure GradeXDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect;
      State: TGridDrawState);
    procedure BtTabelaPrcClick(Sender: TObject);
    procedure SpeedButton9Click(Sender: TObject);
    procedure SpeedButton6Click(Sender: TObject);
  private
    FFmtQtde: String;
    //FEmInclusao: Boolean;
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
   ////Procedures do form
    procedure MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo: Integer);
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    //
    procedure ReconfiguraGradeQ();
    //
    procedure MostraFatConCadGru(SQLType: TSQLType);
    procedure MostraFatConCadLei(SQLType: TSQLType);
  public
    { Public declarations }
    procedure LocCod(Atual, Codigo: Integer);
    procedure ReopenFisRegCad();
    procedure ReopenGrupos(Nivel1: Integer);
  end;

var
  FmFatConCad: TFmFatConCad;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, MyDBCheck, MasterSelFilial, FatConCadLei, ModuleGeral,
  ModProd, MyVCLSkin, Principal, FatPedImp, ModPediVda, FatConCadGru, Entidade2,
  CambioMda, TabePrcCab, UnGrade_Jan;

{$R *.DFM}

const
  FThisFatID = 003; // Venda Condicional

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmFatConCad.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmFatConCad.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrFatConCadCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmFatConCad.DefParams;
begin
  VAR_GOTOTABELA := 'FatConCad';
  VAR_GOTOMYSQLTABLE := QrFatConCad;
  VAR_GOTONEG := gotoAll;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;
  VAR_SQLx.Add('SELECT IF(fil.Tipo=0,fil.RazaoSocial,fil.Nome) NOMEFILIAL,');
  VAR_SQLx.Add('IF(cli.Tipo=0,cli.RazaoSocial,cli.Nome) NO_CLIENTE,');
  VAR_SQLx.Add('pgt.Nome NOMEPRDGRUPTIP, scc.Nome MOMESTQCENCAD,');
  VAR_SQLx.Add('pem.BalQtdItem, pem.FatSemEstq, frc.Nome NO_FisRegCad,');
  VAR_SQLx.Add('frc.CodUsu CU_FisRegCad, frm.TipoMov, ');
  VAR_SQLx.Add('ELT(frm.TipoMov+1,"Entrada","Saida","?") NO_TipoMov,');
  VAR_SQLx.Add('frm.TipoCalc, ELT(frm.TipoCalc+1,"Nulo","Adiciona",');
  VAR_SQLx.Add('"Subtrai","?") NO_TipoCalc, frm.GraCusPrc,');
  VAR_SQLx.Add('imp.Nome NOMEMODELONF, tpc.CodUsu CODUSU_TPC,');
  VAR_SQLx.Add('tpc.Nome NOMETABEPRCCAD, mda.CodUsu CODUSU_MDA,');
  VAR_SQLx.Add('mda.Nome NOMEMOEDA, sbc.*');
  VAR_SQLx.Add('FROM fatconcad sbc');
  VAR_SQLx.Add('LEFT JOIN entidades fil  ON fil.Codigo=sbc.Empresa');
  VAR_SQLx.Add('LEFT JOIN prdgruptip pgt ON pgt.Codigo=sbc.PrdGrupTip');
  VAR_SQLx.Add('LEFT JOIN stqcencad scc  ON scc.Codigo=sbc.StqCenCad');
  VAR_SQLx.Add('LEFT JOIN paramsemp pem  ON pem.Codigo=sbc.Empresa');
  VAR_SQLx.Add('LEFT JOIN fisregcad frc ON frc.Codigo=sbc.FisRegCad');
  VAR_SQLx.Add('LEFT JOIN entidades cli ON cli.Codigo=sbc.Cliente');
  VAR_SQLx.Add('LEFT JOIN fisregmvt frm ON frm.Codigo=frc.Codigo');
  VAR_SQLx.Add('     AND frm.TipoCalc > 0');
  VAR_SQLx.Add('     AND frm.Empresa=sbc.Empresa');
  VAR_SQLx.Add('LEFT JOIN imprime    imp ON imp.Codigo=frc.ModeloNF');
  VAR_SQLx.Add('LEFT JOIN tabeprccab tpc ON tpc.Codigo=sbc.TabelaPrc');
  VAR_SQLx.Add('LEFT JOIN cambiomda  mda ON mda.Codigo=sbc.Moeda');
  VAR_SQLx.Add('     ');
  VAR_SQLx.Add('WHERE sbc.Codigo > -1000');
  //
  VAR_SQL1.Add('AND sbc.Codigo=:P0');
  //
  VAR_SQL2.Add('AND sbc.CodUsu=:P0');
  //
  VAR_SQLa.Add('AND sbc.Nome Like :P0');
  //
end;

procedure TFmFatConCad.EdCodUsuKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key = VK_F4) and (ImgTipo.SQLType = stIns) then
    EdCodUsu.ValueVariant :=
    //UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, 'FatConCad', 'CodUsu', [], []);
    UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, 'FatConCad', 'CodUsu', [], [],
    stIns, 0, siPositivo, EdCodUsu);
end;

procedure TFmFatConCad.EdFilialChange(Sender: TObject);
begin
  ReopenFisRegCad();
end;

procedure TFmFatConCad.EdFisRegCadChange(Sender: TObject);
var
  FisRegCad: Integer;
begin
  FisRegCad := Geral.IMV(EdFisRegCad.Text);
  //
  if FisRegCad <> 0 then
  begin
    EdDBTipoMov.DataSource := DsFisRegCad;
    EdDBTipoMov.DataSource := DsFisRegCad;
    if ImgTipo.SQLType = stIns then
    begin
      case QrFisRegCadTipoCalc.Value of
        1: EdEntraSai.ValueVariant := 1;
        2: EdEntraSai.ValueVariant := -1;
        else EdEntraSai.ValueVariant := 0;
      end;
    end;
  end else begin
    EdDBTipoMov.DataSource := nil;
    EdDBTipoMov.DataSource := nil;
    EdEntraSai.ValueVariant := 0;
  end;
  //
  EdModeloNF.Text := '';
  if not EdFisRegCad.Focused then
    EdModeloNF.Text := QrFisRegCadNO_MODELO_NF.Value;
end;

procedure TFmFatConCad.EdFisRegCadExit(Sender: TObject);
begin
  EdModeloNF.Text := QrFisRegCadNO_MODELO_NF.Value;
end;

procedure TFmFatConCad.EdPrdGrupTipChange(Sender: TObject);
begin
  ReopenFisRegCad();
end;

procedure TFmFatConCad.EdStqCenCadChange(Sender: TObject);
begin
  ReopenFisRegCad();
end;

procedure TFmFatConCad.EdTabelaPrcChange(Sender: TObject);
begin
  if ImgTipo.SQLType = stIns then
  begin
    EdMoeda.ValueVariant := DmPediVda.QrTabePrcCabMoeda.Value;
    CBMoeda.KeyValue     := DmPediVda.QrTabePrcCabMoeda.Value;
  end;
end;

procedure TFmFatConCad.Encerramovimentoatual1Click(Sender: TObject);
var
  Codigo: Integer;
  Agora: String;
begin
  if Geral.MensagemBox('Confirma o encerramento do pedido condicional atual?',
  'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
  begin
    Screen.Cursor := crHourGlass;
    try
      Agora := Geral.FDT(DModG.ObtemAgora(), 105);
      Codigo := QrFatConCadCodigo.Value;
      //
      // Ativa itens no movimento
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('UPDATE stqmovitsa SET Ativo=1');
      Dmod.QrUpd.SQL.Add('WHERE Tipo=:P0' );
      Dmod.QrUpd.SQL.Add('AND OriCodi=:P1 ');
      Dmod.QrUpd.Params[00].AsInteger := FThisFatID;
      Dmod.QrUpd.Params[01].AsInteger := Codigo;
      Dmod.QrUpd.ExecSQL;
      //
      // Encerra definivamente movimento manual
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('UPDATE fatconcad SET ');
      Dmod.QrUpd.SQL.Add('Status=9, Encerrou=:P0');
      Dmod.QrUpd.SQL.Add('WHERE Codigo=:P1 ');
      Dmod.QrUpd.Params[00].AsString  := Agora;
      Dmod.QrUpd.Params[01].AsInteger := Codigo;
      Dmod.QrUpd.ExecSQL;
      //
      {}
      LocCod(Codigo, Codigo);
    finally
      Screen.Cursor := crDefault;
    end;
  end;
end;

procedure TFmFatConCad.MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo: Integer);
begin
  case Mostra of
    0:
    begin
      PainelControle.Visible:=True;
      PainelDados.Visible := True;
      PainelEdita.Visible := False;
    end;
    else Application.MessageBox('A��o de Inclus�o/altera��o n�o definida!',
    'Aviso', MB_OK+MB_ICONWARNING);
  end;
  ImgTipo.SQLType := SQLType;
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmFatConCad.MostraFatConCadGru(SQLType: TSQLType);
begin
  DmodG.ReopenParamsEmp(QrFatConCadEmpresa.Value);
  if DBCheck.CriaFm(TFmFatConCadGru, FmFatConCadGru, afmoNegarComAviso) then
  begin
    FmFatConCadGru.ImgTipo.SQLType := SQLType;
    if SQLType = stUpd then
    begin
      FmFatConCadGru.PnSeleciona.Enabled    := False;
      { Parei Aqui!  ver
      FmFatConCadGru.EdGraGru1.ValueVariant := QrFatConCadGruNivel1.Value;
      FmFatConCadGru.CBGraGru1.KeyValue     := QrFatConCadGruNivel1.Value;
      }
    end;
    FmFatConCadGru.ShowModal;
    FmFatConCadGru.Destroy;
  end;
end;

procedure TFmFatConCad.MostraFatConCadLei(SQLType: TSQLType);
begin
  DmodG.ReopenParamsEmp(QrFatConcadEmpresa.Value);
  if DBCheck.CriaFm(TFmFatConCadLei, FmFatConCadLei, afmoNegarComAviso) then
  begin
    FmFatConCadLei.ImgTipo.SQLType := SQLType;
    FmFatConCadLei.ShowModal;
    FmFatConCadLei.Destroy;
    ReopenGrupos(QrGruposNivel1.Value);
  end;
end;

procedure TFmFatConCad.PMMovimPopup(Sender: TObject);
var
  Habil1: Boolean;
begin
  Habil1 :=
    (QrFatConCad.State <> dsInactive) and (QrFatConCad.RecordCount > 0)
    and (QrFatConCadEncerrou.Value = 0);
  Alteramovimentoatual1.Enabled  := Habil1;
  Encerramovimentoatual1.Enabled := Habil1;
end;

procedure TFmFatConCad.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmFatConCad.QueryPrincipalAfterOpen;
begin
end;

procedure TFmFatConCad.DBEdit6Change(Sender: TObject);
begin
  if DBEdit6.Text = CO_MovimentoAberto then
  begin
    DBEdit6.Font.Color := clRed;
    DBEdit6.Font.Style := [];//[fsBold];
  end else begin
    DBEdit6.Font.Color := clWindowText;
    DBEdit6.Font.Style := [];
  end;
end;

procedure TFmFatConCad.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmFatConCad.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmFatConCad.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmFatConCad.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmFatConCad.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmFatConCad.SpeedButton5Click(Sender: TObject);
var
  FisRegCad: Integer;
begin
  VAR_CADASTRO := 0;
  FisRegCad    := EdFisRegCad.ValueVariant;
  //
  Grade_Jan.MostraFormFisRegCad(FisRegCad);
  //
  if VAR_CADASTRO <> 0 then
  begin
    QrFisRegCad.Close;
    UnDmkDAC_PF.AbreQuery(QrFisRegCad, Dmod.MyDB);
    //
    if QrFisRegCad.Locate('Codigo', QrFisRegCadCodigo.Value, []) then
    begin
      EdFisRegCad.ValueVariant := QrFisRegCadCodUsu.Value;
      CBFisRegCad.KeyValue     := QrFisRegCadCodUsu.Value;
    end;
  end;
end;

procedure TFmFatConCad.SpeedButton6Click(Sender: TObject);
begin
  VAR_ENTIDADE := 0;
  if DBCheck.CriaFm(TFmEntidade2, FmEntidade2, afmoNegarComAviso) then
  begin
    FmEntidade2.ShowModal;
    FmEntidade2.Destroy;
    if VAR_ENTIDADE <> 0 then
    begin
      DmPediVda.QrClientes.Close;
      UnDmkDAC_PF.AbreQuery(DmPediVda.QrClientes, Dmod.MyDB);
      if DmPediVda.QrClientes.Locate('Codigo', VAR_ENTIDADE, []) then
      begin
        EdCliente.ValueVariant := VAR_ENTIDADE;
        CBCliente.KeyValue     := VAR_ENTIDADE;
      end;
    end;
  end;
end;

procedure TFmFatConCad.SpeedButton9Click(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  if DBCheck.CriaFm(TFmCambioMda, FmCambioMda, afmoNegarComAviso) then
  begin
    FmCambioMda.ShowModal;
    FmCambioMda.Destroy;
    if VAR_CADASTRO <> 0 then
    begin
      DModG.QrCambioMda.Close;
      UnDmkDAC_PF.AbreQuery(DModG.QrCambioMda, Dmod.MyDB);
      if DModG.QrCambioMda.Locate('Codigo', VAR_CADASTRO, []) then
      begin
        EdMoeda.ValueVariant := VAR_CADASTRO;
        CBMoeda.KeyValue     := VAR_CADASTRO;
      end;
    end;
  end;
end;

procedure TFmFatConCad.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrFatConCadCodigo.Value;
  Close;
end;

procedure TFmFatConCad.BtTabelaPrcClick(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  if DBCheck.CriaFm(TFmTabePrcCab, FmTabePrcCab, afmoNegarComAviso) then
  begin
    FmTabePrcCab.ShowModal;
    FmTabePrcCab.Destroy;
    if VAR_CADASTRO <> 0 then
    begin
      DmPediVda.QrTabePrcCab.Close;
      UnDmkDAC_PF.AbreQuery(DmPediVda.QrTabePrcCab, Dmod.MyDB);
      if DmPediVda.QrTabePrcCab.Locate('Codigo', VAR_CADASTRO, []) then
      begin
        EdTabelaPrc.ValueVariant := VAR_CADASTRO;
        CBTabelaPrc.KeyValue     := VAR_CADASTRO;
      end;
    end;
  end;
end;

procedure TFmFatConCad.BtMovimClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMMovim, BtMovim);
end;

procedure TFmFatConCad.BtConfirmaClick(Sender: TObject);
var
  Codigo, Empresa, PrdGrupTip, FisRegCad: Integer;
  Nome: String;
begin
  if MyObjects.FIC(Geral.IMV(EdFilial.Text) = 0, EdFilial,
    'Informe a empresa!') then Exit;
  if MyObjects.FIC(Geral.IMV(EdPrdGrupTip.Text) = 0, EdPrdGrupTip,
    'Informe o tipo de grupo de produtos!') then Exit;
  if MyObjects.FIC(Geral.IMV(EdStqCenCad.Text) = 0, EdStqCenCad,
    'Informe o centro de estoque!') then Exit;
  if MyObjects.FIC(Geral.IMV(EdFisRegCad.Text) = 0, EdFisRegCad,
    'Informe a regra fiscal!') then Exit;
  if MyObjects.FIC(Geral.IMV(EdCliente.Text) = 0, EdCliente,
    'Informe o vendedor (cliente)!') then Exit;
  //
  Empresa    := QrFiliaisCodigo.Value;
  PrdGrupTip := QrPrdGrupTipCodigo.Value;
  //StqCenCad  := QrStqCenCadCodigo.Value;
  FisRegCad  := QrFisRegCadCodigo.Value;
  // Verifica se existe balan�o aberto
  if DmProd.ExisteBalancoAberto_StqCen_Mul(PrdGrupTip, Empresa, FisRegCad) then
    Exit;
  Nome := EdNome.Text;
  if Length(Nome) = 0 then
  begin
    Application.MessageBox('Defina uma descri��o.', 'Erro', MB_OK+MB_ICONERROR);
    Exit;
  end;
  {  precisa?
  QrNew.Close;
  QrNew.Params[00].AsInteger := Empresa;
  QrNew.Params[01].AsInteger := PrdGrupTip;
  QrNew.Params[02].AsInteger := StqCenCad;
  UnDmkDAC_PF.AbreQuery(QrNew, Dmod.MyDB);
  if MyObjects.FIC(QrNew.RecordCount > 0, nil,
  'Inclus�o abortada!' + sLineBreak + 'Existe um balan�o aberto para esta ' +
  'configura��o (ID n� ' + IntToStr(QrNewCodigo.Value) + ').') then
  begin
    LocCod(QrFatConCadCodigo.Value, QrNewCodigo.Value);
    Exit;
  end;
  }
  //
  Codigo := UMyMod.BuscaEmLivreY_Def('FatConCad', 'Codigo', ImgTipo.SQLType,
    QrFatConCadCodigo.Value);
  //FEmInclusao := True;
  if UMyMod.ExecSQLInsUpdPanel(ImgTipo.SQLType, FmFatConCad, PainelEdit,
    'FatConCad', Codigo, Dmod.QrUpd, [PainelEdita], [PainelDados], ImgTipo, True) then
  begin
    LocCod(Codigo, Codigo);
  end;
  //FEmInclusao := False;
end;

procedure TFmFatConCad.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := Geral.IMV(EdCodigo.Text);
  if ImgTipo.SQLType = stIns then UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'FatConCad', Codigo);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'FatConCad', 'Codigo');
  MostraEdicao(0, stLok, 0);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'FatConCad', 'Codigo');
end;

procedure TFmFatConCad.BtItensClick(Sender: TObject);
begin
  if QrFatConCadFATOR.Value = 0 then
  begin
    Geral.MensagemBox(PChar('Fator n�o definido! ' +
    'O c�lculo deve ser diferente de nulo na regra fiscal!'),
    'Mensagem', MB_OK+MB_ICONWARNING);
    Exit;
  end;
  MyObjects.MostraPopUpDeBotao(PMItens, BtItens);
{
  if DBCheck.CriaFm(TFmFatConCadLei, FmFatConCadLei, afmoNegarComAviso) then
  begin
    FmFatConCadLei.QrLidosQtde.DisplayFormat   := FFmtQtde;
    FmFatConCadLei.QrTotalQtdLei.DisplayFormat := FFmtQtde;
    FmFatConCadLei.EdQtdLei.DecimalSize        := QrFatConCadCasasProd.Value;
    FmFatConCadLei.EdQtdLei.ValueVariant       := QrFatConCadBalQtdItem.Value;
    FmFatConCadLei.EdStqCenCad.ValueVariant    := QrFatConCadStqCenCad.Value;
    FmFatConCadLei.CBStqCenCad.KeyValue        := QrFatConCadStqCenCad.Value;
    FmFatConCadLei.ReopenLidos(0);
    FmFatConCadLei.ShowModal;
    LocCod(QrFatConCadCodigo.Value, QrFatConCadCodigo.Value);
    FmFatConCadLei.Destroy;
  end;
}
end;

procedure TFmFatConCad.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  PageControl1.ActivePageIndex := 0;
  Panel4.Align     := alClient;
  UnDmkDAC_PF.AbreQuery(QrFiliais, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrPrdGrupTip, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrStqCenCad, Dmod.MyDB);
  DmPediVda.ReopenClientes();
  CriaOForm;
  EdDBTipoMov.DataSource := nil;
  EdDBTipoMov.DataSource := nil;
  //
  DmPediVda.QrTabePrcCab.Close;
  DmPediVda.QrTabePrcCab.Params[00].AsInteger := 4; // Condicional
  // o mysql n�o aceita (neste caso) data como yyyy/mm/yy deve ser yyyy-mm-dd
  DmPediVda.QrTabePrcCab.Params[01].AsString  := FormatDateTime('YYYY-MM-DD', Date);
  UnDmkDAC_PF.AbreQuery(DmPediVda.QrTabePrcCab, Dmod.MyDB);
  //
  DmodG.QrCambioMda.Close;
  UnDmkDAC_PF.AbreQuery(DmodG.QrCambioMda, Dmod.MyDB);
end;

procedure TFmFatConCad.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrFatConCadCodigo.Value, LaRegistro.Caption);
end;

procedure TFmFatConCad.SbImprimeClick(Sender: TObject);
begin
  //MyObjects.MostraPopUpDeBotao(PMImprime, SbImprime);
  if DBCheck.CriaFm(TFmFatPedImp, FmFatPedImp, afmoNegarComAviso) then
  begin
    FmFatPedImp.FFatID          := FThisFatID;
    FmFatPedImp.FEmpresa        := QrFatConCadEmpresa.Value;
    FmFatPedImp.FOriCodigo      := QrFatConCadCodigo.Value;
    FmFatPedImp.FTituloPedido   := 'Pedido Condicional';
    FmFatPedImp.FPedido         := QrFatConCadCodUsu.Value;
    FmFatPedImp.FCliente        := QrFatConCadCliente.Value;
    FmFatPedImp.FNomeCondicaoPg := '';
    FmFatPedImp.FTipoMov        := QrFatConCadTipoMov.Value; // entrada ou saida

    FmFatPedImp.ShowModal;
    FmFatPedImp.Destroy;
  end;
end;

procedure TFmFatConCad.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmFatConCad.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrFatConCadCodUsu.Value, LaRegistro.Caption);
end;

procedure TFmFatConCad.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmFatConCad.QrGruposAfterScroll(DataSet: TDataSet);
begin
  ReconfiguraGradeQ();
end;

procedure TFmFatConCad.QrGruposBeforeClose(DataSet: TDataSet);
begin
  MyObjects.LimpaGrades([GradeQ, GradeC, GradeA, GradeX], 0, 0, True);
end;

procedure TFmFatConCad.QrFatConCadAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
  BtItens.Enabled := (QrFatConCad.RecordCount > 0)
    and (QrFatConCadEncerrou.Value = 0);
end;

procedure TFmFatConCad.QrFatConCadAfterScroll(DataSet: TDataSet);
begin
  FFmtQtde := dmkPF.FormataCasas(QrFatConCadCasasProd.Value);
  ReopenGrupos(0);
end;

procedure TFmFatConCad.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmFatConCad.SbQueryClick(Sender: TObject);
begin
  LocCod(QrFatConCadCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'FatConCad', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmFatConCad.FormResize(Sender: TObject);
begin
   //M L A G e r a l.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 30);
end;

procedure TFmFatConCad.GradeADrawCell(Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);
begin
  MyObjects.DesenhaGradeA(GradeA, ACol, ARow, Rect, State, True);
end;

procedure TFmFatConCad.GradeCDrawCell(Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);
begin
  MyObjects.DesenhaGradeN(GradeC, GradeA, nil, ACol, ARow, Rect, State,
  '0', 0, 0, True);
end;

procedure TFmFatConCad.GradeDDrawCell(Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);
begin
  MyObjects.DesenhaGradeN(GradeD, GradeA, nil, ACol, ARow, Rect, State,
  Dmod.FStrFmtPrc, 0, 0, False);
end;

procedure TFmFatConCad.GradeFDrawCell(Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);
begin
  MyObjects.DesenhaGradeN(GradeF, GradeA, nil, ACol, ARow, Rect, State,
  Dmod.FStrFmtPrc, 0, 0, False);
end;

procedure TFmFatConCad.GradeQDrawCell(Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);
begin
  MyObjects.DesenhaGradeN(GradeQ, GradeA, nil, ACol, ARow, Rect, State,
  dmkPF.FormataCasas(QrGruposFracio.Value), 0, 0, False);
end;

procedure TFmFatConCad.GradeVDrawCell(Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);
begin
  MyObjects.DesenhaGradeN(GradeV, GradeA, nil, ACol, ARow, Rect, State,
  Dmod.FStrFmtPrc, 0, 0, true);
end;

procedure TFmFatConCad.GradeXDrawCell(Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);
begin
  MyObjects.DesenhaGradeN(GradeX, GradeA, nil, ACol, ARow, Rect, State,
  '0', 0, 0, True);
end;

procedure TFmFatConCad.Incluinovomovimento1Click(Sender: TObject);
begin
  //FEmInclusao := False;
  UMyMod.ConfigPanelInsUpd(stIns, Self, PainelEdit, QrFatConCad, [PainelDados],
  [PainelEdita], EdCodUsu, ImgTipo, 'FatConCad');
  EdAbertura.ValueVariant :=  DModG.ObtemAgora();

end;

procedure TFmFatConCad.Incluinovositensdegrupo1Click(Sender: TObject);
begin
  MostraFatConCadGru(stIns);
end;

procedure TFmFatConCad.IncluinovositensporLeitura1Click(Sender: TObject);
begin
  MostraFatConCadLei(stIns);
end;

procedure TFmFatConCad.QrFatConCadBeforeClose(DataSet: TDataSet);
begin
  BtItens.Enabled := False;
  QrGrupos.Close;
end;

procedure TFmFatConCad.QrFatConCadBeforeOpen(DataSet: TDataSet);
begin
  QrFatConCadCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmFatConCad.QrFatConCadCalcFields(DataSet: TDataSet);
begin
  if QrFatConCadEncerrou.Value = 0 then
    QrFatConCadENCERROU_TXT.Value := CO_MovimentoAberto
  else
    QrFatConCadENCERROU_TXT.Value := Geral.FDT(QrFatConCadEncerrou.Value, 0);
  //
  case QrFatConCadTipoCalc.Value of
    1: QrFatConCadFATOR.Value := 1;
    2: QrFatConCadFATOR.Value := -1;
    else QrFatConCadFATOR.Value := 0;
  end;
end;

procedure TFmFatConCad.ReconfiguraGradeQ;
var
  Grade, Nivel1, Condicional: Integer;
begin
  Grade       := QrGruposGRATAMCAD.Value;
  Nivel1      := QrGruposNivel1.Value;
  Condicional := QrFatConCadCodigo.Value;
  //
  DmProd.ConfigGrades6A(Grade, Nivel1, Condicional,
  GradeA, GradeX, GradeC, GradeQ, GradeF, GradeD, GradeV);
  //
{
  DmProd.ConfigGrades3(QrGruposGraTamCad.Value, QrGruposNivel1.Value,
    GradeA, GradeX, GradeC, GradeQ);
  DmProd.ConfigGrades8(QrGruposGraTamCad.Value, QrGruposNivel1.Value,
    QrFatConCadCodigo.Value, GradeA, GradeX, GradeC, GradeQ);
}
end;

procedure TFmFatConCad.ReopenFisRegCad;
var
  Empresa, StqCenCad: Integer;
begin
  //if FEmInclusao then Exit;
  Empresa   := Geral.IMV(EdFilial.Text);
  StqCenCad := Geral.IMV(EdStqCenCad.Text);
  if (Empresa <> 0) and (StqCenCad <> 0) then
  begin
    Empresa   := QrFiliaisCodigo.Value;
    StqCenCad := QrStqCenCadCodigo.Value;
    if //(QrFisRegCad.State = dsInactive) or
    (Empresa <> QrFisRegCad.Params[0].AsInteger) or
    (StqCenCad <> QrFisRegCad.Params[1].AsInteger) then
    begin
      QrFisRegCad.Close;
      QrFisRegCad.Params[00].AsInteger := Empresa;
      QrFisRegCad.Params[01].AsInteger := StqCenCad;
      UnDmkDAC_PF.AbreQuery(QrFisRegCad, Dmod.MyDB);
      EdFisRegCad.ValueVariant := 0;
      CBFisRegCad.KeyValue     := Null;
    end;
  end else QrFisRegCad.Close;
end;

procedure TFmFatConCad.ReopenGrupos(Nivel1: Integer);
begin
  QrGrupos.Close;
  QrGrupos.ParamByName('P0').AsInteger := QrFatConCadCodigo.Value;
  UnDmkDAC_PF.AbreQuery(QrGrupos, Dmod.MyDB);
  //
  if Nivel1 <> 0 then
    QrGrupos.Locate('Nivel1', Nivel1, []);
end;

procedure TFmFatConCad.Alteramovimentoatual1Click(Sender: TObject);
var
  Habilita: Boolean;
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PainelEdita, QrFatConCad, [PainelDados],
  [PainelEdita], EdCodUsu, ImgTipo, 'FatConCad');
  Habilita := QrGrupos.RecordCount = 0;
  EdFilial.Enabled     := Habilita;
  CBFilial.Enabled     := Habilita;
  EdPrdGrupTip.Enabled := Habilita;
  CBPrdGrupTip.Enabled := Habilita;
  EdStqCenCad.Enabled  := Habilita;
  CBStqCenCad.Enabled  := Habilita;
  EdFisRegCad.Enabled  := Habilita;
  CBFisRegCad.Enabled  := Habilita;
end;

end.

