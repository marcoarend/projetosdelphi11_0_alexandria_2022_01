object FmFatConLot: TFmFatConLot
  Left = 339
  Top = 185
  Caption = 
    'FAT-CONDI-012 :: Acerto de Faturamento Condicional - Adi'#231#227'o de L' +
    'ote'
  ClientHeight = 494
  ClientWidth = 784
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelConfirma: TPanel
    Left = 0
    Top = 446
    Width = 784
    Height = 48
    Align = alBottom
    TabOrder = 0
    object BtOK: TBitBtn
      Tag = 14
      Left = 20
      Top = 4
      Width = 90
      Height = 40
      Caption = '&OK'
      NumGlyphs = 2
      TabOrder = 0
      OnClick = BtOKClick
    end
    object Panel2: TPanel
      Left = 672
      Top = 1
      Width = 111
      Height = 46
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 2
        Top = 3
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Hint = 'Sai da janela atual'
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 784
    Height = 48
    Align = alTop
    Caption = 'Acerto de Faturamento Condicional - Adi'#231#227'o de Lote'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 1
    object Image1: TImage
      Left = 2
      Top = 2
      Width = 780
      Height = 44
      Align = alClient
      Transparent = True
      ExplicitWidth = 788
    end
  end
  object PnEdit: TPanel
    Left = 0
    Top = 48
    Width = 784
    Height = 398
    Align = alClient
    TabOrder = 2
    ExplicitLeft = 4
    object PnRetorno: TPanel
      Left = 1
      Top = 1
      Width = 782
      Height = 160
      Align = alTop
      TabOrder = 0
      object GroupBox5: TGroupBox
        Left = 1
        Top = 101
        Width = 780
        Height = 44
        Align = alTop
        TabOrder = 0
        object dmkLabel4: TdmkLabel
          Left = 8
          Top = 20
          Width = 90
          Height = 13
          Caption = 'Vendedor (Cliente):'
          UpdType = utYes
          SQLType = stNil
        end
        object DBEdit11: TDBEdit
          Left = 100
          Top = 16
          Width = 56
          Height = 21
          DataField = 'Cliente'
          DataSource = DsFatConRet
          TabOrder = 0
        end
        object DBEdit13: TDBEdit
          Left = 156
          Top = 16
          Width = 617
          Height = 21
          DataField = 'NO_CLIENTE'
          DataSource = DsFatConRet
          TabOrder = 1
        end
      end
      object GroupBox1: TGroupBox
        Left = 1
        Top = 1
        Width = 780
        Height = 100
        Align = alTop
        TabOrder = 1
        object Label1: TLabel
          Left = 8
          Top = 12
          Width = 14
          Height = 13
          Caption = 'ID:'
          FocusControl = DBEdCodigo
        end
        object Label3: TLabel
          Left = 68
          Top = 12
          Width = 36
          Height = 13
          Caption = 'C'#243'digo:'
          FocusControl = DBEdit1
        end
        object Label2: TLabel
          Left = 152
          Top = 12
          Width = 51
          Height = 13
          Caption = 'Descri'#231#227'o:'
          FocusControl = DBEdNome
        end
        object Label4: TLabel
          Left = 8
          Top = 52
          Width = 44
          Height = 13
          Caption = 'Empresa:'
          FocusControl = DBEdit2
        end
        object DBEdCodigo: TDBEdit
          Left = 8
          Top = 28
          Width = 56
          Height = 21
          Hint = 'N'#186' do banco'
          TabStop = False
          DataField = 'Codigo'
          DataSource = DsFatConRet
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ReadOnly = True
          ShowHint = True
          TabOrder = 0
        end
        object DBEdit1: TDBEdit
          Left = 68
          Top = 28
          Width = 80
          Height = 21
          DataField = 'CodUsu'
          DataSource = DsFatConRet
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 1
        end
        object DBEdNome: TDBEdit
          Left = 152
          Top = 28
          Width = 621
          Height = 21
          Hint = 'Nome do banco'
          Color = clWhite
          DataField = 'Nome'
          DataSource = DsFatConRet
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
        end
        object DBEdit2: TDBEdit
          Left = 8
          Top = 68
          Width = 765
          Height = 21
          DataField = 'NOMEFILIAL'
          DataSource = DsFatConRet
          TabOrder = 3
        end
      end
    end
    object Panel1: TPanel
      Left = 1
      Top = 161
      Width = 782
      Height = 236
      Align = alClient
      TabOrder = 1
      ExplicitTop = 158
      ExplicitHeight = 247
      object DBGrid1: TDBGrid
        Left = 1
        Top = 1
        Width = 780
        Height = 234
        Align = alClient
        DataSource = DsDisponiv
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'CodUsu'
            Title.Caption = 'C'#243'digo'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Nome'
            Title.Caption = 'Descri'#231#227'o'
            Width = 377
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Abertura'
            Width = 113
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Encerrou'
            Title.Caption = 'Encerramento'
            Width = 113
            Visible = True
          end>
      end
    end
  end
  object DsFatConRet: TDataSource
    DataSet = FmFatConRet.QrFatConRet
    Left = 8
    Top = 8
  end
  object DsDisponiv: TDataSource
    DataSet = FmFatConRet.QrDisponiv
    Left = 36
    Top = 8
  end
end
