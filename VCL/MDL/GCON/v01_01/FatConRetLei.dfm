object FmFatConRetLei: TFmFatConRetLei
  Left = 339
  Top = 185
  Caption = 'FAT-CONDI-014 :: Faturamento Condicional - Retorno por Leitura'
  ClientHeight = 523
  ClientWidth = 784
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 784
    Height = 427
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object DBGFatConIts: TDBGrid
      Left = 0
      Top = 49
      Width = 784
      Height = 325
      Align = alClient
      DataSource = DsFatConIts
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
      TabOrder = 2
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'CU_Nivel1'
          Title.Caption = 'Produto'
          Width = 68
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_Nivel1'
          Title.Caption = 'Descri'#231#227'o'
          Width = 283
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CU_Cor'
          Title.Caption = 'Cor'
          Width = 59
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_Cor'
          Title.Caption = 'Descri'#231#227'o'
          Width = 187
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_Tam'
          Title.Caption = 'Tamanho'
          Width = 78
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'QuantC'
          Title.Caption = 'Quantidade'
          Width = 69
          Visible = True
        end>
    end
    object PnLeitura: TPanel
      Left = 0
      Top = 0
      Width = 784
      Height = 49
      Align = alTop
      BevelOuter = bvNone
      ParentBackground = False
      TabOrder = 0
      object Panel4: TPanel
        Left = 0
        Top = 0
        Width = 333
        Height = 49
        Align = alLeft
        TabOrder = 0
        object Label3: TLabel
          Left = 4
          Top = 4
          Width = 89
          Height = 13
          Caption = 'Leitura / digita'#231#227'o:'
        end
        object LaQtdeLei: TLabel
          Left = 188
          Top = 4
          Width = 26
          Height = 13
          Caption = 'Qtde:'
        end
        object EdLeitura: TEdit
          Left = 4
          Top = 20
          Width = 180
          Height = 21
          MaxLength = 20
          TabOrder = 0
          OnChange = EdLeituraChange
          OnKeyDown = EdLeituraKeyDown
        end
        object EdQtdLei: TdmkEdit
          Left = 188
          Top = 20
          Width = 40
          Height = 21
          Alignment = taRightJustify
          Enabled = False
          TabOrder = 1
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = True
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '1'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 1
          ValWarn = False
          OnEnter = EdQtdLeiEnter
          OnKeyDown = EdQtdLeiKeyDown
        end
        object BtOK: TBitBtn
          Tag = 14
          Left = 236
          Top = 3
          Width = 90
          Height = 40
          Caption = '&OK'
          NumGlyphs = 2
          TabOrder = 2
          OnClick = BtOKClick
        end
      end
      object Panel9: TPanel
        Left = 333
        Top = 0
        Width = 347
        Height = 49
        Align = alClient
        Enabled = False
        TabOrder = 1
        object Label4: TLabel
          Left = 4
          Top = 4
          Width = 86
          Height = 13
          Caption = 'Grupo de produto:'
          FocusControl = DBEdit1
        end
        object Label5: TLabel
          Left = 208
          Top = 4
          Width = 19
          Height = 13
          Caption = 'Cor:'
          FocusControl = DBEdit2
        end
        object Label6: TLabel
          Left = 292
          Top = 4
          Width = 48
          Height = 13
          Caption = 'Tamanho:'
          FocusControl = DBEdit3
        end
        object DBEdit1: TDBEdit
          Left = 4
          Top = 20
          Width = 200
          Height = 21
          DataField = 'NOMENIVEL1'
          DataSource = DsItem
          TabOrder = 0
        end
        object DBEdit2: TDBEdit
          Left = 208
          Top = 20
          Width = 80
          Height = 21
          DataField = 'NOMECOR'
          DataSource = DsItem
          TabOrder = 1
        end
        object DBEdit3: TDBEdit
          Left = 292
          Top = 20
          Width = 49
          Height = 21
          DataField = 'NOMETAM'
          DataSource = DsItem
          TabOrder = 2
        end
      end
      object Panel3: TPanel
        Left = 680
        Top = 0
        Width = 104
        Height = 49
        Align = alRight
        Caption = 'Panel3'
        TabOrder = 2
        object Label1: TLabel
          Left = 4
          Top = 4
          Width = 80
          Height = 13
          Caption = 'Total cancelado:'
          FocusControl = DBEdit4
        end
        object DBEdit4: TDBEdit
          Left = 4
          Top = 20
          Width = 80
          Height = 21
          DataField = 'QuantC'
          DataSource = DsTotal
          TabOrder = 0
        end
      end
    end
    object PnSimu: TPanel
      Left = 0
      Top = 374
      Width = 784
      Height = 53
      Align = alBottom
      TabOrder = 1
      Visible = False
      object Label9: TLabel
        Left = 368
        Top = 4
        Width = 54
        Height = 13
        Caption = 'Sequ'#234'ncia:'
      end
      object Label8: TLabel
        Left = 284
        Top = 4
        Width = 48
        Height = 13
        Caption = 'Reduzido:'
      end
      object Label7: TLabel
        Left = 200
        Top = 4
        Width = 75
        Height = 13
        Caption = 'Ordem de serv.:'
      end
      object dmkEdit2: TdmkEdit
        Left = 284
        Top = 20
        Width = 81
        Height = 21
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 6
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '000010'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 10
        ValWarn = False
        OnChange = EdLeituraChange
      end
      object dmkEdit3: TdmkEdit
        Left = 368
        Top = 20
        Width = 81
        Height = 21
        Alignment = taRightJustify
        TabOrder = 1
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 8
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '00000001'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 1
        ValWarn = False
        OnChange = EdLeituraChange
      end
      object dmkEdit1: TdmkEdit
        Left = 200
        Top = 20
        Width = 81
        Height = 21
        Alignment = taRightJustify
        TabOrder = 2
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 6
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '000000'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = EdLeituraChange
      end
      object Button1: TButton
        Left = 4
        Top = 12
        Width = 185
        Height = 25
        Caption = 'Simula leitura do c'#243'digo de barras'
        TabOrder = 3
        OnClick = Button1Click
      end
    end
  end
  object PainelConfirma: TPanel
    Left = 0
    Top = 475
    Width = 784
    Height = 48
    Align = alBottom
    TabOrder = 1
    object Panel2: TPanel
      Left = 672
      Top = 1
      Width = 111
      Height = 46
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 0
      object BtSaida: TBitBtn
        Tag = 13
        Left = 2
        Top = 3
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Hint = 'Sai da janela atual'
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object BtExclui: TBitBtn
      Tag = 12
      Left = 453
      Top = 4
      Width = 140
      Height = 40
      Caption = '&Excluir sequ'#234'ncia'
      NumGlyphs = 2
      TabOrder = 1
      OnClick = BtExcluiClick
    end
    object CkFixo: TCheckBox
      Left = 12
      Top = 16
      Width = 125
      Height = 17
      Caption = 'Quantidade fixa.'
      Checked = True
      State = cbChecked
      TabOrder = 2
      OnClick = CkFixoClick
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 784
    Height = 48
    Align = alTop
    Caption = 'Faturamento Condicional - Retorno por Leitura'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 2
    object Image1: TImage
      Left = 1
      Top = 1
      Width = 734
      Height = 46
      Align = alClient
      Transparent = True
      ExplicitLeft = 2
      ExplicitTop = 6
      ExplicitWidth = 922
      ExplicitHeight = 44
    end
    object GB_R: TGroupBox
      Left = 735
      Top = 1
      Width = 48
      Height = 46
      Align = alRight
      TabOrder = 0
      ExplicitLeft = 736
      ExplicitTop = 0
      ExplicitHeight = 48
      object ImgTipo: TdmkImage
        Left = 8
        Top = 6
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
  end
  object QrItem: TmySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrItemBeforeClose
    SQL.Strings = (
      'SELECT gg1.Nome NOMENIVEL1, ggc.GraCorCad, '
      'gcc.Nome NOMECOR,  gti.Nome NOMETAM, '
      'ggx.Controle GraGruX, ggx.GraGru1, '
      'gg1.CodUsu CU_Nivel1, gg1.IPI_Alq, pgt.MadeBy'
      'FROM gragrux ggx '
      'LEFT JOIN gragru1   gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN gragruc ggc ON ggc.Controle=ggx.GraGruC'
      'LEFT JOIN gracorcad gcc ON gcc.Codigo=ggc.GraCorCad'
      'LEFT JOIN gratamits gti ON gti.Controle=ggx.GraTamI'
      'LEFT JOIN prdgruptip pgt ON pgt.Codigo=gg1.PrdGrupTip'
      'WHERE ggx.Controle=:P0'
      '')
    Left = 12
    Top = 140
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrItemNOMENIVEL1: TWideStringField
      FieldName = 'NOMENIVEL1'
      Origin = 'gragru1.Nome'
      Size = 30
    end
    object QrItemGraCorCad: TIntegerField
      FieldName = 'GraCorCad'
      Origin = 'gragruc.GraCorCad'
    end
    object QrItemNOMECOR: TWideStringField
      FieldName = 'NOMECOR'
      Origin = 'gracorcad.Nome'
      Size = 30
    end
    object QrItemNOMETAM: TWideStringField
      FieldName = 'NOMETAM'
      Origin = 'gratamits.Nome'
      Size = 5
    end
    object QrItemGraGruX: TIntegerField
      FieldName = 'GraGruX'
      Origin = 'gragrux.Controle'
      Required = True
    end
    object QrItemGraGru1: TIntegerField
      FieldName = 'GraGru1'
      Origin = 'gragrux.GraGru1'
      Required = True
    end
    object QrItemCU_Nivel1: TIntegerField
      FieldName = 'CU_Nivel1'
      Required = True
    end
    object QrItemIPI_Alq: TFloatField
      FieldName = 'IPI_Alq'
    end
    object QrItemMadeBy: TSmallintField
      FieldName = 'MadeBy'
    end
  end
  object DsItem: TDataSource
    DataSet = QrItem
    Left = 40
    Top = 140
  end
  object QrFator_: TmySQLQuery
    Database = Dmod.MyDB
    Left = 72
    Top = 172
  end
  object QrFatConIts: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT gg1.CodUsu CU_Nivel1, gg1.Nome NO_Nivel1,  '
      'gcc.CodUsu CU_Cor, gcc.Nome NO_Cor, '
      'gti.Nome NO_Tam,'
      'fci.GraGruX, fci.PrecoF, fci.QuantP, fci.QuantC,'
      'fci.QuantV, fci.Controle, fci.ValLiq'
      'FROM fatconits fci'
      'LEFT JOIN fatconcad fcc ON fcc.Codigo=fci.Codigo'
      'LEFT JOIN gragrux   ggx ON ggx.Controle=fci.GraGruX'
      'LEFT JOIN gragru1   gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN gratamits gti ON gti.Controle=ggx.GraTamI'
      'LEFT JOIN gragruc   ggc ON ggc.Controle=ggx.GraGruC'
      'LEFT JOIN gracorcad   gcc ON gcc.Codigo=ggc.GraCorCad'
      'WHERE fci.QuantC > 0 '
      'AND fcc.FatConRet=:P0'
      'ORDER BY Controle DESC'
      '')
    Left = 72
    Top = 140
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrFatConItsCU_Nivel1: TIntegerField
      FieldName = 'CU_Nivel1'
    end
    object QrFatConItsNO_Nivel1: TWideStringField
      FieldName = 'NO_Nivel1'
      Size = 30
    end
    object QrFatConItsCU_Cor: TIntegerField
      FieldName = 'CU_Cor'
    end
    object QrFatConItsNO_Cor: TWideStringField
      FieldName = 'NO_Cor'
      Size = 30
    end
    object QrFatConItsNO_Tam: TWideStringField
      FieldName = 'NO_Tam'
      Size = 5
    end
    object QrFatConItsGraGruX: TIntegerField
      FieldName = 'GraGruX'
      Required = True
    end
    object QrFatConItsPrecoF: TFloatField
      FieldName = 'PrecoF'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrFatConItsQuantP: TFloatField
      FieldName = 'QuantP'
    end
    object QrFatConItsQuantC: TFloatField
      FieldName = 'QuantC'
      Required = True
    end
    object QrFatConItsQuantV: TFloatField
      FieldName = 'QuantV'
      Required = True
    end
    object QrFatConItsControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrFatConItsValLiq: TFloatField
      FieldName = 'ValLiq'
      Required = True
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
  end
  object DsFatConIts: TDataSource
    DataSet = QrFatConIts
    Left = 100
    Top = 140
  end
  object QrLista_: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT GraCusPrc'
      'FROM fisregmvt'
      'WHERE TipoMov=1'
      'AND Empresa=:P0'
      'AND Codigo=:P1')
    Left = 100
    Top = 172
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrLista_GraCusPrc: TIntegerField
      FieldName = 'GraCusPrc'
    end
  end
  object QrTotal: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(fci.QuantC) QuantC'
      'FROM fatconits fci'
      'LEFT JOIN fatconcad fcc ON fcc.Codigo=fci.Codigo'
      'WHERE fci.QuantC > 0 '
      'AND fcc.FatConRet = :P0'
      'ORDER BY Controle DESC')
    Left = 196
    Top = 140
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrTotalQuantC: TFloatField
      FieldName = 'QuantC'
    end
  end
  object DsTotal: TDataSource
    DataSet = QrTotal
    Left = 224
    Top = 140
  end
  object QrSaldo: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(fci.QuantV) QuantV'
      'FROM fatconits fci'
      'LEFT JOIN fatconcad fcc ON fcc.Codigo=fci.Codigo'
      'WHERE fci.QuantV > 0'
      'AND fcc.FatConRet = :P0'
      'AND fci.GraGruX= :P1'
      'ORDER BY fci.QuantV, fci.Controle DESC')
    Left = 132
    Top = 140
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrSaldoQuantV: TFloatField
      FieldName = 'QuantV'
    end
  end
  object QrVendido: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT fci.Controle, fci.QuantV, fci.Codigo'
      'FROM fatconits fci'
      'LEFT JOIN fatconcad fcc ON fcc.Codigo=fci.Codigo'
      'LEFT JOIN gragrux ggx ON ggx.Controle=fci.GraGruX'
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN prdgruptip pgt ON pgt.Codigo=gg1.PrdGrupTip'
      'WHERE fcc.FatConRet = :P0'
      'AND fci.GraGruX = :P1'
      'ORDER BY fci.QuantV')
    Left = 164
    Top = 140
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrVendidoQuantV: TFloatField
      FieldName = 'QuantV'
    end
    object QrVendidoControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrVendidoCodigo: TIntegerField
      FieldName = 'Codigo'
    end
  end
end
