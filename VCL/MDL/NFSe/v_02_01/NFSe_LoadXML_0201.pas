unit NFSe_LoadXML_0201;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, XMLDoc, XMLIntf, xmldom, msxmldom, dmkImage,
  UnDmkProcFunc, DmkDAC_PF, NFSe_PF_0000, UnDmkEnums;

const
  CO_Toma = 'Toma';
  CO_Presta = 'Presta';
  CO_Interme = 'Interme';
type
  TipoDeArraySQL = (tasqlDPS, tasqlNFSe, tasqlRPS);
  PRSSFeedData = ^TRSSFeedData;
  TRSSFeedData = record
    URL : string;
    Description : string;
    Date : string;
  end;

  arrayOfString = array of String;
  arrayOfVariant = array of Variant;

  TFmNFSe_LoadXML_0201 = class(TForm)
    PainelConfirma: TPanel;
    BtOK: TBitBtn;
    Panel1: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    Panel3: TPanel;
    EdArquivo: TdmkEdit;
    Label110: TLabel;
    SpeedButton16: TSpeedButton;
    XMLDocument1: TXMLDocument;
    EdVersion: TdmkEdit;
    Label1: TLabel;
    Label2: TLabel;
    EdEncoding: TdmkEdit;
    EdNodeName: TdmkEdit;
    Label3: TLabel;
    EdLocalName: TdmkEdit;
    Label4: TLabel;
    EdPrefix: TdmkEdit;
    Label5: TLabel;
    Label6: TLabel;
    EdNamespaceURI: TdmkEdit;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    TreeView1: TTreeView;
    MeIgnorados: TMemo;
    QrPsq: TmySQLQuery;
    RGAmbiente: TRadioGroup;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure SpeedButton16Click(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure RGAmbienteClick(Sender: TObject);
  private
    { Private declarations }
    FAmbiente: Integer;
    FNumeroLote: Int64;
    //
    FFoc_Toma,
    FFoc_Presta,
    FFoc_Interme: String;
    {
    FNfse_Numero,
    FNfse_CodigoVerificacao,
    FNfse_DataEmissao: String;
    
    FRPS_RpsIDSerie,
    FRPS_RpsIDNumero,
    FRPS_RpsIDTipo: String;
    
    EntiTipo: String; // CO_Toma, CO_Presta, 'Iterme'
    Fld_Nfse: arrayofString;
    Val_Nfse: arrayofVariant;
    //
    Fld_DPS: arrayofString;
    Val_DPS: arrayofVariant;
    //
    Fld_RPS: arrayofString;
    Val_RPS: arrayofVariant;
    }
    //
    //
    //
    procedure ClearTree();
    procedure ExpandeXmlNoTreeView();
    function  CarregaArquivoNoTempBD(Source: String; XMLDocument: TXMLDocument; FullNameArq: String): Boolean;

{
    procedure AvisaItemDesconhecido(Source, Grupo, NodeName: WideString);
    procedure AddSQLItem(TipoSQL: TipoDeArraySQL; Campo: String; Valor: Variant);
    procedure SetZeroArrs(var Fld: arrayofString; Val: arrayOfVariant);
    // Arquivos
    procedure ImportaGrupo_ConsultarNfseResposta(Source: String; XMLNoSorc: IXMLNode; TipoDoc: TipoDeArraySQL);
    procedure ImportaGrupo_EnviarLoteRpsSincronoResposta(Source: String; XMLNoSorc: IXMLNode; TipoDoc: TipoDeArraySQL);
    // N�s
    procedure ImportaGrupo_ListaNfse(Source: String; XMLNoSorc: IXMLNode; TipoDoc: TipoDeArraySQL);
    procedure ImportaGrupo_CompNfse(Source: String; XMLNoSorc: IXMLNode; TipoDoc: TipoDeArraySQL);
    procedure ImportaGrupo_Nfse(Source: String; XMLNoSorc: IXMLNode; TipoDoc: TipoDeArraySQL);
    procedure ImportaGrupo_InfNfse(Source: String; XMLNoSorc: IXMLNode; TipoDoc: TipoDeArraySQL);
    procedure ImportaGrupo_ValoresNfse(Source: String; XMLNoSorc: IXMLNode; TipoDoc: TipoDeArraySQL);
    procedure ImportaGrupo_PrestadorServico(Source: String; XMLNoSorc: IXMLNode; TipoDoc: TipoDeArraySQL);
    procedure ImportaGrupo_OrgaoGerador(Source: String; XMLNoSorc: IXMLNode; TipoDoc: TipoDeArraySQL);
    procedure ImportaGrupo_DeclaracaoPrestacaoServico(Source: String; XMLNoSorc: IXMLNode; TipoDoc: TipoDeArraySQL);
    procedure ImportaGrupo_InfDeclaracaoPrestacaoServico(Source: String; XMLNoSorc: IXMLNode; TipoDoc: TipoDeArraySQL);
    procedure ImportaGrupo_IdentificacaoPrestador(Source: String; XMLNoSorc: IXMLNode; TipoDoc: TipoDeArraySQL);
    procedure ImportaGrupo_CpfCnpj(Source: String; XMLNoSorc: IXMLNode; TipoDoc: TipoDeArraySQL);
    procedure ImportaGrupo_Endereco(Source: String; XMLNoSorc: IXMLNode; TipoDoc: TipoDeArraySQL);
    procedure ImportaGrupo_Contato(Source: String; XMLNoSorc: IXMLNode; TipoDoc: TipoDeArraySQL);
    procedure ImportaGrupo_Servico(Source: String; XMLNoSorc: IXMLNode; TipoDoc: TipoDeArraySQL);
    procedure ImportaGrupo_Prestador(Source: String; XMLNoSorc: IXMLNode; TipoDoc: TipoDeArraySQL);
    procedure ImportaGrupo_Tomador(Source: String; XMLNoSorc: IXMLNode; TipoDoc: TipoDeArraySQL);
    procedure ImportaGrupo_IdentificacaoTomador(Source: String; XMLNoSorc: IXMLNode; TipoDoc: TipoDeArraySQL);
    procedure ImportaGrupo_Valores(Source: String; XMLNoSorc: IXMLNode; TipoDoc: TipoDeArraySQL);
    //
    procedure ImportaGrupo_Rps(Source: String; XMLNoSorc: IXMLNode; TipoDoc: TipoDeArraySQL);
    procedure ImportaGrupo_IdentificacaoRps(Source: String; XMLNoSorc: IXMLNode; TipoDoc: TipoDeArraySQL);
    //
    procedure SQLInsUpd_Nfse();
    procedure SQLInsUpd_RPS();
    function  NaoPermitePeloAmbiente(): Boolean;
    function  DefineDocumento(EntiTipo, Doc: String): String;
}
    //
    //function  DefineEmpresa_Nfse(): Integer;
  public
    { Public declarations }
  end;

  var
  FmNFSe_LoadXML_0201: TFmNFSe_LoadXML_0201;

implementation

uses UnMyObjects, UMySQLModule, Module, ModuleGeral;

{$R *.DFM}

{
procedure TFmNFSe_LoadXML_0201.AddSQLItem(TipoSQL: TipoDeArraySQL;
  Campo: String; Valor: Variant);
var
  N: Integer;
begin
  case TipoSQL of
    tasqlDPS:
    begin
      N := Length(Fld_Dps);
      SetLength(Fld_Dps, N + 1);
      SetLength(Val_Dps, N + 1);
      Fld_Dps[N] := Campo;
      Val_Dps[N] := Valor;
    end;
    tasqlNFSe:
    begin
      N := Length(Fld_Nfse);
      SetLength(Fld_Nfse, N + 1);
      SetLength(Val_Nfse, N + 1);
      Fld_Nfse[N] := Campo;
      Val_Nfse[N] := Valor;
    end;
    tasqlRPS:
    begin
      N := Length(Fld_Rps);
      SetLength(Fld_Rps, N + 1);
      SetLength(Val_Rps, N + 1);
      Fld_Rps[N] := Campo;
      Val_Rps[N] := Valor;
    end;
  end;
end;
}

{
procedure TFmNFSe_LoadXML_0201.AvisaItemDesconhecido(Source, Grupo,
  NodeName: WideString);
var
  Txt: String;
begin
  if Source <> '' then
    Txt := Source + '.' + Grupo
  else
    Txt := Grupo;
  //
  if Lowercase(NodeName) = LowerCase('xmlns') then
    MeIgnorados.Lines.Add(Txt + ': ' + NodeName)
  else
  if Lowercase(NodeName) = LowerCase('xmlns:xsi') then
    MeIgnorados.Lines.Add(Txt + ': ' + NodeName)
  else
  if Lowercase(NodeName) = LowerCase('xmlns:ds') then
    MeIgnorados.Lines.Add(Txt + ': ' + NodeName)
  else
  if Lowercase(NodeName) = LowerCase('xmlns:xsd') then
    MeIgnorados.Lines.Add(Txt + ': ' + NodeName)
  else
  if Lowercase(NodeName) = LowerCase('xsi:schemaLocation') then
    MeIgnorados.Lines.Add(Txt + ': ' + NodeName)
(*
  else
  if Lowercase(NodeName) = LowerCase('xsi:ns2') then
    MeIgnorados.Lines.Add(Txt + ': ' + NodeName)
*)
  else
  if Lowercase(NodeName) = LowerCase('xmlns:ns2') then
    MeIgnorados.Lines.Add(Txt + ': ' + NodeName)
  else
    Geral.MensagemBox('TAG do XML desconhecido no grupo ' + Txt + ':' +
    #13#10 + NodeName + #13#10 +
    'Avise a Dermatek!', 'Aviso', MB_OK+MB_ICONWARNING);
end;
}

procedure TFmNFSe_LoadXML_0201.BtOKClick(Sender: TObject);
begin
  FFoc_Toma := '';
  FFoc_Presta := '';
  FFoc_Interme := '';

  //
  ExpandeXmlNoTreeView();
  //
  CarregaArquivoNoTempBD('', XMLDocument1, EdArquivo.Text);
end;

procedure TFmNFSe_LoadXML_0201.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

function TFmNFSe_LoadXML_0201.CarregaArquivoNoTempBD(Source: String; XMLDocument: TXMLDocument;
  FullNameArq: String): Boolean;
var
  xmlNodeA: IXMLNode;
  MyCursor: TCursor;
  //Campo: String;
  //cTipoDoc: TNFeCTide_TipoDoc;
  //
  //ArqDir, ArqName, Codificacao, LocalName, Prefix, NamespaceURI, NodeName,
  //xTipoDoc: String;
  //Versao: Double;
  //P: Integer;
begin
  Result := False;
  if RGAmbiente.ItemIndex < 1 then
  begin
    Geral.MB_Aviso('Ambiente n�o informado na importa��o de XML!');
    Exit;
  end;
  MyCursor      := Screen.Cursor;
  Screen.Cursor := crHourGlass;
  try
    //
    FAmbiente := -1;
    FNumeroLote := 0;
    //







{
    // Para garantir que n�o usara do XML do arquivo anterior quando ler v�rios
    FCodInfoEmit := 0;
    //
    FAssinado := 0;
    Campo := '';
    //cTipoDoc := NFeTipoDoc_Desconhecido;
}
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Importando dados carregados');
    // Lote de envio de NF-e(s)
    xmlNodeA := XMLDocument.DocumentElement;
    //Geral.MB_Info(xmlNodeA.NodeName);
    {
    if LowerCase(xmlNodeA.NodeName) = LowerCase('ConsultarNfseServicoPrestadoResposta') then
      ImportaGrupo_ConsultarNfseResposta(Source, xmlNodeA, tasqlNfse)
    else
    if LowerCase(xmlNodeA.NodeName) = LowerCase('EnviarLoteRpsSincronoResposta')  then
      ImportaGrupo_EnviarLoteRpsSincronoResposta(Source, xmlNodeA, tasqlNfse)
    else
    }
      Geral.MB_Aviso('XML com n� principal desconhecido:' + #13#10 + #13#10 +
      xmlNodeA.NodeName);
    //
  finally
    Screen.Cursor := MyCursor;
  end;
end;

procedure TFmNFSe_LoadXML_0201.ClearTree();
var
  I: integer;
begin
  for I := 0 to TreeView1.Items.Count - 1 do
    Dispose(TreeView1.Items[I].Data);
  TreeView1.Items.Clear;
end;

{
function TFmNFSe_LoadXML_0201.DefineDocumento(EntiTipo, Doc: String): String;
begin
  if Trim(Doc) <> '' then
  begin
    if EntiTipo = CO_Toma then
      FFoc_Toma := Doc
    else
    if EntiTipo = CO_Presta then
      FFoc_Presta := Doc
    else
    if EntiTipo = CO_Interme then
      FFoc_Interme := Doc
    ;
  end;
end;
}

procedure TFmNFSe_LoadXML_0201.ExpandeXmlNoTreeView();
  procedure DomToTree (XmlNode: IXMLNode; TreeNode: TTreeNode);
  var
    I: Integer;
    NewTreeNode: TTreeNode;
    NodeText: string;
    AttrNode: IXMLNode;
  begin
    // skip text nodes and other special cases
    if not (XmlNode.NodeType = ntElement) then
    begin
      //NodeText := XmlNode.NodeName + ' = ' + XmlNode.NodeValue;
      //Geral.MensagemBox(NodeText, 'Texto do n� pulado', MB_OK+MB_ICONWARNING);
      Exit;
    end;
    // add the node itself
    NodeText := XmlNode.NodeName;
    if XmlNode.IsTextElement then
      NodeText := NodeText + ' = ' + XmlNode.NodeValue;
    NewTreeNode := TreeView1.Items.AddChild(TreeNode, NodeText);
    // add attributes
    for I := 0 to xmlNode.AttributeNodes.Count - 1 do
    begin
      AttrNode := xmlNode.AttributeNodes.Nodes[I];
      TreeView1.Items.AddChild(NewTreeNode,
        '[' + AttrNode.NodeName + ' = "' + AttrNode.Text + '"]');
    end;
    // add each child node
    if XmlNode.HasChildNodes then
      for I := 0 to xmlNode.ChildNodes.Count - 1 do
        DomToTree (xmlNode.ChildNodes.Nodes [I], NewTreeNode);
  end;
var
  Arquivo: String;
  //Texto: WideString;
  //xmlDocA, xmlDocB: IXMLDocument;
  //xmlNodeA,
  xmlNodeB, xmlNodeC: IXMLNode;
  //xmlList: IXMLNodeList;
  //
  TV1: TTreeNode;
  j(*, N*): Integer;
  X: IXMLNode;
begin
  Arquivo := EdArquivo.Text;
  if not FileExists(Arquivo) then
  begin
    Geral.MensagemBox('Arquivo n�o localizado:' + #13#10 + Arquivo,
    'Aviso', MB_OK+MB_ICONWARNING);
    Exit;
  end;
  ClearTree;
  XMLDocument1.FileName := Arquivo;
  XMLDocument1.Active := True;
  EdVersion.Text := XMLDocument1.Version;
  EdEncoding.Text := XMLDocument1.Encoding;
  // Erro
  //ShowMessage(XMLDocument1.DocumentElement.Text);  // Erro
  // Todo texto do XML  ap�s a declara��o: <?xml version="1.0" encoding="utf-8" ?>
  //ShowMessage(XMLDocument1.DocumentElement.XML);
  EdLocalName.Text    := XMLDocument1.DocumentElement.LocalName;    // Servi�o
  EdPrefix.Text       := XMLDocument1.DocumentElement.Prefix;       // [nada]
  EdNamespaceURI.Text := XMLDocument1.DocumentElement.NamespaceURI; // http://www.portalfiscal.inf.br/nfe
  EdNodeName.Text     := XMLDocument1.DocumentElement.NodeName;     // Servi�o
  //ShowMessage(XMLDocument1.DocumentElement.NodeValue);            // Erro
  xmlNodeB := XMLDocument1.DocumentElement.ChildNodes.First;
  while xmlNodeB <> nil do
  begin
    TV1 := TreeView1.Items.Add(nil, xmlNodeB.NodeName);
    for J := 0 to xmlNodeB.AttributeNodes.Count - 1 do
    begin
      X := xmlNodeB.AttributeNodes.Nodes[J];
      TreeView1.Items.AddChild(TV1, '[' + X.NodeName + ' = "' + X.Text + '"]');
    end;
    //
    xmlNodeC := xmlNodeB.ChildNodes.First;
    while xmlNodeC <> nil do
    begin
      //N := N + 1;
      DomToTree(xmlNodeC, TV1);
      xmlNodeC := xmlNodeC.NextSibling;
    end;
    xmlNodeB := xmlNodeB.NextSibling;
  end;
  //
  // Expande �rvore
  TreeView1.Items.GetFirstNode.Expand(true);
  //
  //  Permitir leitura posterior
  //XMLDocument1.Active := False;
end;

procedure TFmNFSe_LoadXML_0201.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmNFSe_LoadXML_0201.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  PageControl1.ActivePageIndex := 0;
end;

procedure TFmNFSe_LoadXML_0201.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

{
procedure TFmNFSe_LoadXML_0201.ImportaGrupo_ConsultarNfseResposta(Source: String; XMLNoSorc: IXMLNode; TipoDoc: TipoDeArraySQL);
const
  G = '458R';
var
  I: Integer;
  AttrNode, XMLNohSubNivel: IXMLNode;
  Campo: String;
begin
  if not (XMLNoSorc.NodeType = ntElement) then
    Exit;
  if XMLNoSorc.IsTextElement then
  begin
    Geral.MB_Aviso('Elemento de texto desconhecido no Grupo ' + G + ':' +
    #13#10 + XMLNoSorc.Text);
  end;
  for I := 0 to XMLNoSorc.AttributeNodes.Count - 1 do
  begin
    AttrNode := XMLNoSorc.AttributeNodes.Nodes[I];
    Campo := Lowercase(AttrNode.NodeName);
    (*
    if Campo = LowerCase('versao') then
      EdenviNFe_versao.ValueVariant := ValorFlu(AttrNode) else
    //
    *)
    AvisaItemDesconhecido(Source, G, AttrNode.NodeName);
  end;
  // Subn�s
  if XMLNoSorc.HasChildNodes then
  begin
    for I := 0 to XMLNoSorc.ChildNodes.Count - 1 do
    begin
      XMLNohSubNivel := XMLNoSorc.ChildNodes.Nodes[I];
      if (XMLNohSubNivel.NodeType = ntElement) then
      begin
        Campo := Lowercase(XMLNohSubNivel.NodeName);
        if Campo = LowerCase('listanfse') then
          ImportaGrupo_ListaNfse(Source, XMLNohSubNivel, TipoDoc)
        else


        (*
        if Campo = LowerCase('?) then
          EdenviNFe_idLote.ValueVariant := ValorI64(XMLNohSubNivel)
        else
        if Campo = LowerCase('?) then
          ImportaNFe_Grupo_A00(XMLNohSubNivel)
        else
        *)
          AvisaItemDesconhecido(Source, G, XMLNohSubNivel.NodeName);
      end;
    end;
  end;
end;

procedure TFmNFSe_LoadXML_0201.ImportaGrupo_ListaNfse(Source: String; XMLNoSorc: IXMLNode; TipoDoc: TipoDeArraySQL);
const
  G = '458R2';
var
  I: Integer;
  AttrNode, XMLNohSubNivel: IXMLNode;
  Campo: String;
begin
  if not (XMLNoSorc.NodeType = ntElement) then
    Exit;
  if XMLNoSorc.IsTextElement then
  begin
    Geral.MB_Aviso('Elemento de texto desconhecido no Grupo ' + G + ':' +
    #13#10 + XMLNoSorc.Text);
  end;
  for I := 0 to XMLNoSorc.AttributeNodes.Count - 1 do
  begin
    AttrNode := XMLNoSorc.AttributeNodes.Nodes[I];
    Campo := Lowercase(AttrNode.NodeName);
    (*
    if Campo = LowerCase('versao') then
      EdenviNFe_versao.ValueVariant := ValorFlu(AttrNode) else
    //
    *)
    AvisaItemDesconhecido(Source, G, AttrNode.NodeName);
  end;
  // Subn�s
  if XMLNoSorc.HasChildNodes then
  begin
    for I := 0 to XMLNoSorc.ChildNodes.Count - 1 do
    begin
      XMLNohSubNivel := XMLNoSorc.ChildNodes.Nodes[I];
      if (XMLNohSubNivel.NodeType = ntElement) then
      begin
        Campo := Lowercase(XMLNohSubNivel.NodeName);
        if Campo = LowerCase('CompNfse') then
          ImportaGrupo_CompNfse(Source, XMLNohSubNivel, TipoDoc)
        else


        (*
        if Campo = LowerCase('?) then
          EdenviNFe_idLote.ValueVariant := ValorI64(XMLNohSubNivel)
        else
        if Campo = LowerCase('?) then
          ImportaNFe_Grupo_A00(XMLNohSubNivel)
        else
        *)
          AvisaItemDesconhecido(Source, G, XMLNohSubNivel.NodeName);
      end;
    end;
  end;
end;

procedure TFmNFSe_LoadXML_0201.ImportaGrupo_CompNfse(Source: String; XMLNoSorc: IXMLNode; TipoDoc: TipoDeArraySQL);
const
  G = 'CompNfse';
var
  I: Integer;
  AttrNode, XMLNohSubNivel: IXMLNode;
  Campo: String;
begin
  if not (XMLNoSorc.NodeType = ntElement) then
    Exit;
  if XMLNoSorc.IsTextElement then
  begin
    Geral.MB_Aviso('Elemento de texto desconhecido no Grupo ' + G + ':' +
    #13#10 + XMLNoSorc.Text);
  end;
  for I := 0 to XMLNoSorc.AttributeNodes.Count - 1 do
  begin
    AttrNode := XMLNoSorc.AttributeNodes.Nodes[I];
    Campo := Lowercase(AttrNode.NodeName);
    (*
    if Campo = LowerCase('versao') then
      EdenviNFe_versao.ValueVariant := ValorFlu(AttrNode) else
    //
    *)
    AvisaItemDesconhecido(Source, G, AttrNode.NodeName);
  end;
  // Subn�s
  if XMLNoSorc.HasChildNodes then
  begin
    for I := 0 to XMLNoSorc.ChildNodes.Count - 1 do
    begin
      XMLNohSubNivel := XMLNoSorc.ChildNodes.Nodes[I];
      if (XMLNohSubNivel.NodeType = ntElement) then
      begin
        Campo := Lowercase(XMLNohSubNivel.NodeName);
        if Campo = LowerCase('Nfse') then
          ImportaGrupo_Nfse(Source, XMLNohSubNivel, TipoDoc)
        else


        (*
        if Campo = LowerCase('?) then
          EdenviNFe_idLote.ValueVariant := ValorI64(XMLNohSubNivel)
        else
        if Campo = LowerCase('?) then
          ImportaNFe_Grupo_A00(XMLNohSubNivel)
        else
        *)
          AvisaItemDesconhecido(Source, G, XMLNohSubNivel.NodeName);
      end;
    end;
  end;
end;

procedure TFmNFSe_LoadXML_0201.ImportaGrupo_Contato(Source: String;
  XMLNoSorc: IXMLNode; TipoDoc: TipoDeArraySQL);
const
  G = 'Contato';
var
  I: Integer;
  AttrNode, XMLNohSubNivel: IXMLNode;
  Campo: String;
begin
  //? SetZeroArrs(Fld_? > Nfse, Val_? > Nfse);
  if not (XMLNoSorc.NodeType = ntElement) then
    Exit;
  if XMLNoSorc.IsTextElement then
  begin
    Geral.MB_Aviso('Elemento de texto desconhecido no Grupo ' + G + ':' +
    #13#10 + XMLNoSorc.Text);
  end;
  for I := 0 to XMLNoSorc.AttributeNodes.Count - 1 do
  begin
    AttrNode := XMLNoSorc.AttributeNodes.Nodes[I];
    Campo := Lowercase(AttrNode.NodeName);
    (*
    if Campo = LowerCase('versao') then
      AddSQLItem(TipoDoc, Campo, dmkPF.XMLVal?(AttrNode))
    else
    *)
    //
    AvisaItemDesconhecido(Source, G, AttrNode.NodeName);
  end;
  // Subn�s
  if XMLNoSorc.HasChildNodes then
  begin
    for I := 0 to XMLNoSorc.ChildNodes.Count - 1 do
    begin
      XMLNohSubNivel := XMLNoSorc.ChildNodes.Nodes[I];
      if (XMLNohSubNivel.NodeType = ntElement) then
      begin
        Campo := Lowercase(XMLNohSubNivel.NodeName);
        if Campo = LowerCase('Telefone') then
          AddSQLItem(TipoDoc, EntiTipo + G + Campo, dmkPF.XMLValTxt(XMLNohSubNivel))
        (* Maring� Ini*)
        else
        if Campo = LowerCase('Telefonea') then
          AddSQLItem(TipoDoc, EntiTipo + G + 'Telefone', dmkPF.XMLValTxt(XMLNohSubNivel))
        (*Maring� Fim*)
        else
        if Campo = LowerCase('Email') then
          AddSQLItem(TipoDoc, EntiTipo + G + Campo, dmkPF.XMLValTxt(XMLNohSubNivel))
        else
        (*
        if Campo = LowerCase('?) then
          ImportaGrupo_?(Source, XMLNohSubNivel)
        else
        *)
          AvisaItemDesconhecido(Source, G, XMLNohSubNivel.NodeName);
      end;
    end;
  end;
end;

procedure TFmNFSe_LoadXML_0201.ImportaGrupo_CpfCnpj(Source: String;
  XMLNoSorc: IXMLNode; TipoDoc: TipoDeArraySQL);
const
  G = 'CpfCnpj';
var
  I: Integer;
  AttrNode, XMLNohSubNivel: IXMLNode;
  Campo, Doc: String;
begin
  //? SetZeroArrs(Fld_? > Nfse, Val_? > Nfse);
  if not (XMLNoSorc.NodeType = ntElement) then
    Exit;
  if XMLNoSorc.IsTextElement then
  begin
    Geral.MB_Aviso('Elemento de texto desconhecido no Grupo ' + G + ':' +
    #13#10 + XMLNoSorc.Text);
  end;
  for I := 0 to XMLNoSorc.AttributeNodes.Count - 1 do
  begin
    AttrNode := XMLNoSorc.AttributeNodes.Nodes[I];
    Campo := Lowercase(AttrNode.NodeName);
    (*
    if Campo = LowerCase('versao') then
      AddSQLItem(TipoDoc, Campo, dmkPF.XMLVal?(AttrNode))
    else
    *)
    //
    AvisaItemDesconhecido(Source, G, AttrNode.NodeName);
  end;
  // Subn�s
  if XMLNoSorc.HasChildNodes then
  begin
    for I := 0 to XMLNoSorc.ChildNodes.Count - 1 do
    begin
      XMLNohSubNivel := XMLNoSorc.ChildNodes.Nodes[I];
      if (XMLNohSubNivel.NodeType = ntElement) then
      begin
        Campo := Lowercase(XMLNohSubNivel.NodeName);
        if Campo = LowerCase('Cpf') then
        begin
          Doc := dmkPF.XMLValTxt(XMLNohSubNivel);
          AddSQLItem(TipoDoc, EntiTipo + Campo, Doc);
          DefineDocumento(EntiTipo, Doc);
        end else
        if Campo = LowerCase('Cnpj') then
        begin
          Doc := dmkPF.XMLValTxt(XMLNohSubNivel);
          AddSQLItem(TipoDoc, EntiTipo + Campo, Doc);
          DefineDocumento(EntiTipo, Doc);
        end
        //
        (*
        else
        if Campo = LowerCase('?) then
          ImportaGrupo_?(Source, XMLNohSubNivel)
        *)
        else

          AvisaItemDesconhecido(Source, G, XMLNohSubNivel.NodeName);
      end;
    end;
  end;
end;

procedure TFmNFSe_LoadXML_0201.ImportaGrupo_DeclaracaoPrestacaoServico(
  Source: String; XMLNoSorc: IXMLNode; TipoDoc: TipoDeArraySQL);
const
  G = 'DeclaracaoPrestacaoServico';
var
  I: Integer;
  AttrNode, XMLNohSubNivel: IXMLNode;
  Campo: String;
begin
  SetZeroArrs(Fld_RPS, Val_RPS);
  if not (XMLNoSorc.NodeType = ntElement) then
    Exit;
  if XMLNoSorc.IsTextElement then
  begin
    Geral.MB_Aviso('Elemento de texto desconhecido no Grupo ' + G + ':' +
    #13#10 + XMLNoSorc.Text);
  end;
  for I := 0 to XMLNoSorc.AttributeNodes.Count - 1 do
  begin
    AttrNode := XMLNoSorc.AttributeNodes.Nodes[I];
    Campo := Lowercase(AttrNode.NodeName);
    (*
    if Campo = LowerCase('versao') then
      AddSQLItem(TipoDoc, Campo, dmkPF.XMLVal?(AttrNode))
    else
    *)
    //
    AvisaItemDesconhecido(Source, G, AttrNode.NodeName);
  end;
  // Subn�s
  if XMLNoSorc.HasChildNodes then
  begin
    for I := 0 to XMLNoSorc.ChildNodes.Count - 1 do
    begin
      XMLNohSubNivel := XMLNoSorc.ChildNodes.Nodes[I];
      if (XMLNohSubNivel.NodeType = ntElement) then
      begin
        Campo := Lowercase(XMLNohSubNivel.NodeName);
        (*
        if Campo = LowerCase('?) then
          AddSQLItem(TipoDoc, Campo, dmkPF.XMLVal?(XMLNohSubNivel))
        else
*)
        if Campo = LowerCase('InfDeclaracaoPrestacaoServico') then
          ImportaGrupo_InfDeclaracaoPrestacaoServico(Source, XMLNohSubNivel, TipoDoc)
        else
          AvisaItemDesconhecido(Source, G, XMLNohSubNivel.NodeName);
      end;
    end;
  end;

  //

  SQLInsUpd_Rps();
end;

procedure TFmNFSe_LoadXML_0201.ImportaGrupo_Endereco(Source: String;
  XMLNoSorc: IXMLNode; TipoDoc: TipoDeArraySQL);
const
  G = 'Endereco';
var
  I: Integer;
  AttrNode, XMLNohSubNivel: IXMLNode;
  Campo: String;
begin
  //? SetZeroArrs(Fld_? > Nfse, Val_? > Nfse);
  if not (XMLNoSorc.NodeType = ntElement) then
    Exit;
  if XMLNoSorc.IsTextElement then
  begin
    Geral.MB_Aviso('Elemento de texto desconhecido no Grupo ' + G + ':' +
    #13#10 + XMLNoSorc.Text);
  end;
  for I := 0 to XMLNoSorc.AttributeNodes.Count - 1 do
  begin
    AttrNode := XMLNoSorc.AttributeNodes.Nodes[I];
    Campo := Lowercase(AttrNode.NodeName);
    //
    (*
    if Campo = LowerCase('versao') then
      AddSQLItem(TipoDoc, Campo, dmkPF.XMLVal?(AttrNode))
    else
    *)
    //
    AvisaItemDesconhecido(Source, G, AttrNode.NodeName);
  end;
  // Subn�s
  if XMLNoSorc.HasChildNodes then
  begin
    for I := 0 to XMLNoSorc.ChildNodes.Count - 1 do
    begin
      XMLNohSubNivel := XMLNoSorc.ChildNodes.Nodes[I];
      if (XMLNohSubNivel.NodeType = ntElement) then
      begin
        Campo := Lowercase(XMLNohSubNivel.NodeName);
        if Campo = LowerCase('Endereco') then
          AddSQLItem(TipoDoc, EntiTipo + Campo, dmkPF.XMLValTxt(XMLNohSubNivel))
        else
        if Campo = LowerCase('Numero') then
          AddSQLItem(TipoDoc, EntiTipo + Campo, dmkPF.XMLValTxt(XMLNohSubNivel))
        else
        if Campo = LowerCase('Complemento') then
          AddSQLItem(TipoDoc, EntiTipo + Campo, dmkPF.XMLValTxt(XMLNohSubNivel))
        else
        if Campo = LowerCase('Bairro') then
          AddSQLItem(TipoDoc, EntiTipo + Campo, dmkPF.XMLValTxt(XMLNohSubNivel))
        else
        if Campo = LowerCase('CodigoMunicipio') then
          AddSQLItem(TipoDoc, EntiTipo + Campo, dmkPF.XMLValInt(XMLNohSubNivel))
        else
        if Campo = LowerCase('Uf') then
          AddSQLItem(TipoDoc, EntiTipo + Campo, dmkPF.XMLValTxt(XMLNohSubNivel))
        else
        if Campo = LowerCase('Cep') then
          AddSQLItem(TipoDoc, EntiTipo + Campo, dmkPF.XMLValTxt(XMLNohSubNivel))
        else
        if Campo = LowerCase('Telefone') then
          AddSQLItem(TipoDoc, EntiTipo + Campo, dmkPF.XMLValTxt(XMLNohSubNivel))
        else
        if Campo = LowerCase('Email') then
          AddSQLItem(TipoDoc, EntiTipo + Campo, dmkPF.XMLValTxt(XMLNohSubNivel))
        else
        (*
        if Campo = LowerCase('?) then
          ImportaGrupo_?(Source, XMLNohSubNivel)
        else
        *)
          AvisaItemDesconhecido(Source, G, XMLNohSubNivel.NodeName);
      end;
    end;
  end;
end;

procedure TFmNFSe_LoadXML_0201.ImportaGrupo_EnviarLoteRpsSincronoResposta(
  Source: String; XMLNoSorc: IXMLNode; TipoDoc: TipoDeArraySQL);
const
  G = 'EnviarLoteRpsSincronoResposta';
var
  I: Integer;
  AttrNode, XMLNohSubNivel: IXMLNode;
  Campo: String;
  //
  NumeroLote, I64: TtsNumeroLote;
  DataRecebimento: String;
  Protocolo: TtsNumeroProtocolo;
  Ambiente, Status, Codigo: Integer;
begin
  //? SetZeroArrs(Fld_? > Nfse, Val_? > Nfse);
  if not (XMLNoSorc.NodeType = ntElement) then
    Exit;
  if XMLNoSorc.IsTextElement then
  begin
    Geral.MB_Aviso('Elemento de texto desconhecido no Grupo ' + G + ':' +
    #13#10 + XMLNoSorc.Text);
  end;
  for I := 0 to XMLNoSorc.AttributeNodes.Count - 1 do
  begin
    AttrNode := XMLNoSorc.AttributeNodes.Nodes[I];
    Campo := Lowercase(AttrNode.NodeName);
    (*
    if Campo = LowerCase('versao') then
      AddSQLItem(TipoDoc, Campo, dmkPF.XMLVal?(AttrNode))
    else
    *)
    //
    AvisaItemDesconhecido(Source, G, AttrNode.NodeName);
  end;
  // Subn�s
  if XMLNoSorc.HasChildNodes then
  begin
    for I := 0 to XMLNoSorc.ChildNodes.Count - 1 do
    begin
      XMLNohSubNivel := XMLNoSorc.ChildNodes.Nodes[I];
      if (XMLNohSubNivel.NodeType = ntElement) then
      begin
        Campo := Lowercase(XMLNohSubNivel.NodeName);
        if Campo = LowerCase('NumeroLote') then
          //AddSQLItem(TipoDoc, ? + Campo, dmkPF.XMLVal?(XMLNohSubNivel));
          NumeroLote := dmkPF.XMLValInt(XMLNohSubNivel)
        else
        if Campo = LowerCase('DataRecebimento') then
          //AddSQLItem(TipoDoc, ? + Campo, dmkPF.XMLVal?(XMLNohSubNivel));
          DataRecebimento := dmkPF.XMLValDth(XMLNohSubNivel)
        else
        if Campo = LowerCase('Protocolo') then
          //AddSQLItem(TipoDoc, ? + Campo, dmkPF.XMLVal?(XMLNohSubNivel));
          Protocolo := dmkPF.XMLValTxt(XMLNohSubNivel)
        //
        else
        if Campo = LowerCase('ListaNfse') then
        begin
          if NumeroLote <> 0 then
          begin
            UnDmkDAC_PF.AbreMySQLQuery0(QrPsq, Dmod.MyDB, [
            'SELECT Codigo, Ambiente, Status ',
            'FROM nfselrpsc ',
            'WHERE Codigo=' + Geral.FF0(NumeroLote),
            '']);
            Ambiente := QrPsq.FieldByName('Ambiente').AsInteger;
            Codigo := QrPsq.FieldByName('Codigo').AsInteger;
            Status := QrPsq.FieldByName('Status').AsInteger;
            if Codigo <> 0 then
            begin
              (*
              if Trim(Protocolo) <> '' then
                Status := 100;
              *)
              UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'nfselrpsc', False, [
              'DataRecebimento', 'Protocolo'], ['Codigo'], [
              DataRecebimento, Protocolo], [Codigo], True);
              //
            end;
            FAmbiente := Ambiente;
            FNumeroLote := NumeroLote;
            //
            ImportaGrupo_ListaNfse(Source, XMLNohSubNivel, TipoDoc);
          end else
            Geral.MB_Aviso('Lote ' + Geral.FI64(NumeroLote) + ' n�o localizado na base de dados!');
        end
        else
          AvisaItemDesconhecido(Source, G, XMLNohSubNivel.NodeName);
      end;
    end;
    // Atualiza Status Lote:
  end;
end;

procedure TFmNFSe_LoadXML_0201.ImportaGrupo_IdentificacaoPrestador(
  Source: String; XMLNoSorc: IXMLNode; TipoDoc: TipoDeArraySQL);
const
  G = 'IdentificacaoPrestador';
var
  I: Integer;
  AttrNode, XMLNohSubNivel: IXMLNode;
  Campo, Antes: String;
begin
  Antes := EntiTipo;
  EntiTipo := CO_Presta;
  //? SetZeroArrs(Fld_? > Nfse, Val_? > Nfse);
  if not (XMLNoSorc.NodeType = ntElement) then
    Exit;
  if XMLNoSorc.IsTextElement then
  begin
    Geral.MB_Aviso('Elemento de texto desconhecido no Grupo ' + G + ':' +
    #13#10 + XMLNoSorc.Text);
  end;
  for I := 0 to XMLNoSorc.AttributeNodes.Count - 1 do
  begin
    AttrNode := XMLNoSorc.AttributeNodes.Nodes[I];
    Campo := Lowercase(AttrNode.NodeName);
    (*
    if Campo = LowerCase('versao') then
      AddSQLItem(TipoDoc, Campo, dmkPF.XMLVal?(AttrNode))
    else
    *)
    //
    AvisaItemDesconhecido(Source, G, AttrNode.NodeName);
  end;
  // Subn�s
  if XMLNoSorc.HasChildNodes then
  begin
    for I := 0 to XMLNoSorc.ChildNodes.Count - 1 do
    begin
      XMLNohSubNivel := XMLNoSorc.ChildNodes.Nodes[I];
      if (XMLNohSubNivel.NodeType = ntElement) then
      begin
        Campo := Lowercase(XMLNohSubNivel.NodeName);
        if Campo = LowerCase('InscricaoMunicipal') then
          AddSQLItem(TipoDoc, EntiTipo + Campo, dmkPF.XMLValTxt(XMLNohSubNivel))
        else
        if Campo = LowerCase('CpfCnpj') then
          ImportaGrupo_CpfCnpj(Source, XMLNohSubNivel, TipoDoc)
        else
          AvisaItemDesconhecido(Source, G, XMLNohSubNivel.NodeName);
      end;
    end;
  end;
  EntiTipo := Antes;
end;

procedure TFmNFSe_LoadXML_0201.ImportaGrupo_IdentificacaoRps(Source: String;
  XMLNoSorc: IXMLNode; TipoDoc: TipoDeArraySQL);
const
  G = 'IdentificacaoRps';
  Tag = 'RpsID';
var
  I: Integer;
  AttrNode, XMLNohSubNivel: IXMLNode;
  Campo: String;
begin
  //? SetZeroArrs(Fld_? > Nfse, Val_? > Nfse);
  if not (XMLNoSorc.NodeType = ntElement) then
    Exit;
  if XMLNoSorc.IsTextElement then
  begin
    Geral.MB_Aviso('Elemento de texto desconhecido no Grupo ' + G + ':' +
    #13#10 + XMLNoSorc.Text);
  end;
  for I := 0 to XMLNoSorc.AttributeNodes.Count - 1 do
  begin
    AttrNode := XMLNoSorc.AttributeNodes.Nodes[I];
    Campo := Lowercase(AttrNode.NodeName);
    (*
    if Campo = LowerCase('versao') then
      AddSQLItem(TipoDoc, Campo, dmkPF.XMLVal?(AttrNode))
    else
    *)
    //
    AvisaItemDesconhecido(Source, G, AttrNode.NodeName);
  end;
  // Subn�s
  if XMLNoSorc.HasChildNodes then
  begin
    for I := 0 to XMLNoSorc.ChildNodes.Count - 1 do
    begin
      XMLNohSubNivel := XMLNoSorc.ChildNodes.Nodes[I];
      if (XMLNohSubNivel.NodeType = ntElement) then
      begin
        Campo := Lowercase(XMLNohSubNivel.NodeName);
        if Campo = LowerCase('Numero') then
        begin
          FRPS_RpsIDNumero := dmkPF.XMLValTxt(XMLNohSubNivel);
          AddSQLItem(TipoDoc, Tag + Campo, FRPS_RpsIDNumero);
        end else
        if Campo = LowerCase('Serie') then
        begin
          FRPS_RpsIDSerie := dmkPF.XMLValTxt(XMLNohSubNivel);
          AddSQLItem(TipoDoc, Tag + Campo, FRPS_RpsIDSerie);
        end else
        if Campo = LowerCase('Tipo') then
        begin
          FRPS_RpsIDTipo := dmkPF.XMLValTxt(XMLNohSubNivel);
          AddSQLItem(TipoDoc, Tag + Campo, FRPS_RpsIDTipo);
        end else
        //
        (*
        if Campo = LowerCase('?) then
          ImportaGrupo_?(Source, XMLNohSubNivel)
        else
        *)
          AvisaItemDesconhecido(Source, G, XMLNohSubNivel.NodeName);
      end;
    end;
  end;
end;

procedure TFmNFSe_LoadXML_0201.ImportaGrupo_IdentificacaoTomador(Source: String;
  XMLNoSorc: IXMLNode; TipoDoc: TipoDeArraySQL);
const
  G = 'IdentificacaoTomador';
var
  I: Integer;
  AttrNode, XMLNohSubNivel: IXMLNode;
  Campo: String;
begin
  //? SetZeroArrs(Fld_? > Nfse, Val_? > Nfse);
  if not (XMLNoSorc.NodeType = ntElement) then
    Exit;
  if XMLNoSorc.IsTextElement then
  begin
    Geral.MB_Aviso('Elemento de texto desconhecido no Grupo ' + G + ':' +
    #13#10 + XMLNoSorc.Text);
  end;
  for I := 0 to XMLNoSorc.AttributeNodes.Count - 1 do
  begin
    AttrNode := XMLNoSorc.AttributeNodes.Nodes[I];
    Campo := Lowercase(AttrNode.NodeName);
    (*
    if Campo = LowerCase('versao') then
      AddSQLItem(TipoDoc, Campo, dmkPF.XMLVal?(AttrNode))
    else
    *)
    //
    AvisaItemDesconhecido(Source, G, AttrNode.NodeName);
  end;
  // Subn�s
  if XMLNoSorc.HasChildNodes then
  begin
    for I := 0 to XMLNoSorc.ChildNodes.Count - 1 do
    begin
      XMLNohSubNivel := XMLNoSorc.ChildNodes.Nodes[I];
      if (XMLNohSubNivel.NodeType = ntElement) then
      begin
        Campo := Lowercase(XMLNohSubNivel.NodeName);
        (*
        if Campo = LowerCase('?) then
          AddSQLItem(TipoDoc, Campo, dmkPF.XMLVal?(XMLNohSubNivel))
        else
        *)
        //
        if Campo = LowerCase('CpfCnpj') then
          ImportaGrupo_CpfCnpj(Source, XMLNohSubNivel, TipoDoc)
        else
          AvisaItemDesconhecido(Source, G, XMLNohSubNivel.NodeName);
      end;
    end;
  end;
end;

procedure TFmNFSe_LoadXML_0201.ImportaGrupo_InfDeclaracaoPrestacaoServico(
  Source: String; XMLNoSorc: IXMLNode; TipoDoc: TipoDeArraySQL);
const
  G = 'InfDeclaracaoPrestacaoServico';
var
  I: Integer;
  AttrNode, XMLNohSubNivel: IXMLNode;
  Campo: String;
begin
  //? SetZeroArrs(Fld_? > Nfse, Val_? > Nfse);
  if not (XMLNoSorc.NodeType = ntElement) then
    Exit;
  if XMLNoSorc.IsTextElement then
  begin
    Geral.MB_Aviso('Elemento de texto desconhecido no Grupo ' + G + ':' +
    #13#10 + XMLNoSorc.Text);
  end;
  for I := 0 to XMLNoSorc.AttributeNodes.Count - 1 do
  begin
    AttrNode := XMLNoSorc.AttributeNodes.Nodes[I];
    Campo := Lowercase(AttrNode.NodeName);
    (*
    if Campo = LowerCase('versao') then
      AddSQLItem(TipoDoc, Campo, dmkPF.XMLVal?(AttrNode))
    else
    *)
    //
    AvisaItemDesconhecido(Source, G, AttrNode.NodeName);
  end;
  // Subn�s
  if XMLNoSorc.HasChildNodes then
  begin
    for I := 0 to XMLNoSorc.ChildNodes.Count - 1 do
    begin
      XMLNohSubNivel := XMLNoSorc.ChildNodes.Nodes[I];
      if (XMLNohSubNivel.NodeType = ntElement) then
      begin
        Campo := Lowercase(XMLNohSubNivel.NodeName);
        if Campo = LowerCase('Competencia') then
          AddSQLItem(TipoDoc, Campo, dmkPF.XMLValDta(XMLNohSubNivel))
        else
        if Campo = LowerCase('OptanteSimplesNacional') then
          AddSQLItem(TipoDoc, Campo, dmkPF.XMLValInt(XMLNohSubNivel))
        else
        if Campo = LowerCase('IncentivoFiscal') then
          AddSQLItem(TipoDoc, Campo, dmkPF.XMLValInt(XMLNohSubNivel))
        //
        else
        if Campo = LowerCase('Rps') then
          ImportaGrupo_Rps(Source, XMLNohSubNivel, TipoDoc)
        else
        if Campo = LowerCase('Servico') then
          ImportaGrupo_Servico(Source, XMLNohSubNivel, TipoDoc)
        else
        if Campo = LowerCase('Prestador') then
          ImportaGrupo_Prestador(Source, XMLNohSubNivel, TipoDoc)
        else
        if Campo = LowerCase('Tomador') then
          ImportaGrupo_Tomador(Source, XMLNohSubNivel, TipoDoc)
        else
          AvisaItemDesconhecido(Source, G, XMLNohSubNivel.NodeName);
      end;
    end;
  end;
end;

procedure TFmNFSe_LoadXML_0201.ImportaGrupo_InfNfse(Source: String; XMLNoSorc: IXMLNode; TipoDoc: TipoDeArraySQL);
const
  G = 'InfNfse';
var
  I: Integer;
  AttrNode, XMLNohSubNivel: IXMLNode;
  Campo: String;
begin
  if not (XMLNoSorc.NodeType = ntElement) then
    Exit;
  if XMLNoSorc.IsTextElement then
  begin
    Geral.MB_Aviso('Elemento de texto desconhecido no Grupo ' + G + ':' +
    #13#10 + XMLNoSorc.Text);
  end;
  for I := 0 to XMLNoSorc.AttributeNodes.Count - 1 do
  begin
    AttrNode := XMLNoSorc.AttributeNodes.Nodes[I];
    Campo := Lowercase(AttrNode.NodeName);
    (*
    if Campo = LowerCase(') then
      AddSQLItem(TipoDoc, Campo, dmkPF.XMLVal?(AttrNode));
    *)
    //
    AvisaItemDesconhecido(Source, G, AttrNode.NodeName);
  end;
  // Subn�s
  if XMLNoSorc.HasChildNodes then
  begin
    for I := 0 to XMLNoSorc.ChildNodes.Count - 1 do
    begin
      XMLNohSubNivel := XMLNoSorc.ChildNodes.Nodes[I];
      if (XMLNohSubNivel.NodeType = ntElement) then
      begin
        Campo := Lowercase(XMLNohSubNivel.NodeName);
        if Campo = LowerCase('Numero') then
        begin
          FNfse_Numero := dmkPF.XMLValTxt(XMLNohSubNivel);
          AddSQLItem(TipoDoc, Campo, FNfse_Numero);
        end
        else
        if Campo = LowerCase('CodigoVerificacao') then
        begin
          FNfse_CodigoVerificacao := dmkPF.XMLValTxt(XMLNohSubNivel);
          AddSQLItem(TipoDoc, Campo, FNfse_CodigoVerificacao);
        end
        else
        if Campo = LowerCase('DataEmissao') then
        begin
          FNfse_DataEmissao := dmkPF.XMLValDth(XMLNohSubNivel);
          AddSQLItem(TipoDoc, Campo, FNfse_DataEmissao);
        end
        //
        else
        if Campo = LowerCase('ValoresNfse') then
          ImportaGrupo_ValoresNfse(Source, XMLNohSubNivel, TipoDoc)
        else
        if Campo = LowerCase('PrestadorServico') then
          ImportaGrupo_PrestadorServico(Source, XMLNohSubNivel, TipoDoc)
        else
        if Campo = LowerCase('OrgaoGerador') then
          ImportaGrupo_OrgaoGerador(Source, XMLNohSubNivel, TipoDoc)
        else
        if Campo = LowerCase('DeclaracaoPrestacaoServico') then
          ImportaGrupo_DeclaracaoPrestacaoServico(Source, XMLNohSubNivel, tasqlRPS)
        else
          AvisaItemDesconhecido(Source, G, XMLNohSubNivel.NodeName);
      end;
    end;
  end;
end;

procedure TFmNFSe_LoadXML_0201.ImportaGrupo_Nfse(Source: String; XMLNoSorc: IXMLNode; TipoDoc: TipoDeArraySQL);
const
  G = 'Nfse';
var
  I: Integer;
  AttrNode, XMLNohSubNivel: IXMLNode;
  Campo: String;
begin
  SetZeroArrs(Fld_Nfse, Val_Nfse);
////////////////////////////////////////////////////////////////////////////////
  if not (XMLNoSorc.NodeType = ntElement) then
    Exit;
  if XMLNoSorc.IsTextElement then
  begin
    Geral.MB_Aviso('Elemento de texto desconhecido no Grupo ' + G + ':' +
    #13#10 + XMLNoSorc.Text);
  end;
  for I := 0 to XMLNoSorc.AttributeNodes.Count - 1 do
  begin
    AttrNode := XMLNoSorc.AttributeNodes.Nodes[I];
    Campo := Lowercase(AttrNode.NodeName);
    if Campo = LowerCase('versao') then
      AddSQLItem(TipoDoc, Campo, dmkPF.XMLValFlu(AttrNode))
    else
    //
    AvisaItemDesconhecido(Source, G, AttrNode.NodeName);
  end;
  // Subn�s
  if XMLNoSorc.HasChildNodes then
  begin
    for I := 0 to XMLNoSorc.ChildNodes.Count - 1 do
    begin
      XMLNohSubNivel := XMLNoSorc.ChildNodes.Nodes[I];
      if (XMLNohSubNivel.NodeType = ntElement) then
      begin
        Campo := Lowercase(XMLNohSubNivel.NodeName);
        if Campo = LowerCase('InfNfse') then
          ImportaGrupo_InfNfse(Source, XMLNohSubNivel, TipoDoc)
        else


        (*
        if Campo = LowerCase('?) then
          AddSQLItem(TipoDoc, Campo, dmkPF.XMLVal?(AttrNode))
        else
        if Campo = LowerCase('?) then
          ImportaGrupo_?(XMLNohSubNivel)
        else
        *)
          AvisaItemDesconhecido(Source, G, XMLNohSubNivel.NodeName);
      end;
    end;
  end;

  //

  SQLInsUpd_Nfse();
end;

procedure TFmNFSe_LoadXML_0201.ImportaGrupo_OrgaoGerador(Source: String;
  XMLNoSorc: IXMLNode; TipoDoc: TipoDeArraySQL);
const
  G = 'OrgaoGerador';
var
  I: Integer;
  AttrNode, XMLNohSubNivel: IXMLNode;
  Campo: String;
begin
  //? SetZeroArrs(Fld_? > Nfse, Val_? > Nfse);
  if not (XMLNoSorc.NodeType = ntElement) then
    Exit;
  if XMLNoSorc.IsTextElement then
  begin
    Geral.MB_Aviso('Elemento de texto desconhecido no Grupo ' + G + ':' +
    #13#10 + XMLNoSorc.Text);
  end;
  for I := 0 to XMLNoSorc.AttributeNodes.Count - 1 do
  begin
    AttrNode := XMLNoSorc.AttributeNodes.Nodes[I];
    Campo := Lowercase(AttrNode.NodeName);
    (*
    if Campo = LowerCase('versao') then
      AddSQLItem(TipoDoc, Campo, dmkPF.XMLVal?(AttrNode))
    else
    *)
    //
    AvisaItemDesconhecido(Source, G, AttrNode.NodeName);
  end;
  // Subn�s
  if XMLNoSorc.HasChildNodes then
  begin
    for I := 0 to XMLNoSorc.ChildNodes.Count - 1 do
    begin
      XMLNohSubNivel := XMLNoSorc.ChildNodes.Nodes[I];
      if (XMLNohSubNivel.NodeType = ntElement) then
      begin
        Campo := Lowercase(XMLNohSubNivel.NodeName);
        if Campo = LowerCase('CodigoMunicipio') then
          AddSQLItem(TipoDoc, 'OrgaoGeradorCodigoMunicipio', dmkPF.XMLValInt(XMLNohSubNivel))
        else
        if Campo = LowerCase('Uf') then
          AddSQLItem(TipoDoc, 'OrgaoGeradorUf', dmkPF.XMLValTxt(XMLNohSubNivel))
(*
        else
        if Campo = LowerCase('?) then
          ImportaGrupo_?(Source, XMLNohSubNivel)
        *)
        else
          AvisaItemDesconhecido(Source, G, XMLNohSubNivel.NodeName);
      end;
    end;
  end;
end;

procedure TFmNFSe_LoadXML_0201.ImportaGrupo_Prestador(Source: String;
  XMLNoSorc: IXMLNode; TipoDoc: TipoDeArraySQL);
const
  G = 'Prestador';
var
  I: Integer;
  AttrNode, XMLNohSubNivel: IXMLNode;
  Campo: String;
begin
  EntiTipo := CO_Presta;
  //
  //? SetZeroArrs(Fld_? > Nfse, Val_? > Nfse);
  if not (XMLNoSorc.NodeType = ntElement) then
    Exit;
  if XMLNoSorc.IsTextElement then
  begin
    Geral.MB_Aviso('Elemento de texto desconhecido no Grupo ' + G + ':' +
    #13#10 + XMLNoSorc.Text);
  end;
  for I := 0 to XMLNoSorc.AttributeNodes.Count - 1 do
  begin
    AttrNode := XMLNoSorc.AttributeNodes.Nodes[I];
    Campo := Lowercase(AttrNode.NodeName);
    (*
    if Campo = LowerCase('versao') then
      AddSQLItem(TipoDoc, Campo, dmkPF.XMLVal?(AttrNode))
    else
    *)
    //
    AvisaItemDesconhecido(Source, G, AttrNode.NodeName);
  end;
  // Subn�s
  if XMLNoSorc.HasChildNodes then
  begin
    for I := 0 to XMLNoSorc.ChildNodes.Count - 1 do
    begin
      XMLNohSubNivel := XMLNoSorc.ChildNodes.Nodes[I];
      if (XMLNohSubNivel.NodeType = ntElement) then
      begin
        Campo := Lowercase(XMLNohSubNivel.NodeName);
        if Campo = LowerCase('InscricaoMunicipal') then
          AddSQLItem(TipoDoc, EntiTipo + Campo, dmkPF.XMLValTxt(XMLNohSubNivel))
        else
        //
        if Campo = LowerCase('CpfCnpj') then
          ImportaGrupo_CpfCnpj(Source, XMLNohSubNivel, TipoDoc)
        else
          AvisaItemDesconhecido(Source, G, XMLNohSubNivel.NodeName);
      end;
    end;
  end;
end;

procedure TFmNFSe_LoadXML_0201.ImportaGrupo_PrestadorServico(Source: String;
  XMLNoSorc: IXMLNode; TipoDoc: TipoDeArraySQL);
const
  G = 'PrestadorServico';
var
  I: Integer;
  AttrNode, XMLNohSubNivel: IXMLNode;
  Campo: String;
begin
  EntiTipo := CO_Presta;
  //? SetZeroArrs(Fld_? > Nfse, Val_? > Nfse);
  if not (XMLNoSorc.NodeType = ntElement) then
    Exit;
  if XMLNoSorc.IsTextElement then
  begin
    Geral.MB_Aviso('Elemento de texto desconhecido no Grupo ' + G + ':' +
    #13#10 + XMLNoSorc.Text);
  end;
  for I := 0 to XMLNoSorc.AttributeNodes.Count - 1 do
  begin
    AttrNode := XMLNoSorc.AttributeNodes.Nodes[I];
    Campo := Lowercase(AttrNode.NodeName);
    (*
    if Campo = LowerCase('versao') then
      AddSQLItem(TipoDoc, Campo, dmkPF.XMLVal?(AttrNode))
    else
    *)
    //
    AvisaItemDesconhecido(Source, G, AttrNode.NodeName);
  end;
  // Subn�s
  if XMLNoSorc.HasChildNodes then
  begin
    for I := 0 to XMLNoSorc.ChildNodes.Count - 1 do
    begin
      XMLNohSubNivel := XMLNoSorc.ChildNodes.Nodes[I];
      if (XMLNohSubNivel.NodeType = ntElement) then
      begin
        Campo := Lowercase(XMLNohSubNivel.NodeName);
        if Campo = LowerCase('RazaoSocial') then
          AddSQLItem(TipoDoc, EntiTipo + Campo, dmkPF.XMLValTxt(XMLNohSubNivel))
        else
        if Campo = LowerCase('NomeFantasia') then
          AddSQLItem(TipoDoc, EntiTipo + Campo, dmkPF.XMLValTxt(XMLNohSubNivel))
        else
        if Campo = LowerCase('IdentificacaoPrestador') then
          ImportaGrupo_IdentificacaoPrestador(Source, XMLNohSubNivel, TipoDoc)
        else
        if Campo = LowerCase('Endereco') then
          ImportaGrupo_Endereco(Source, XMLNohSubNivel, TipoDoc)
        else
        if Campo = LowerCase('Contato') then
          ImportaGrupo_Contato(Source, XMLNohSubNivel, TipoDoc)
        else
          AvisaItemDesconhecido(Source, G, XMLNohSubNivel.NodeName);
      end;
    end;
  end;
  EntiTipo := '';
end;

procedure TFmNFSe_LoadXML_0201.ImportaGrupo_Rps(Source: String;
  XMLNoSorc: IXMLNode; TipoDoc: TipoDeArraySQL);
const
  G = 'Rps';
var
  I: Integer;
  AttrNode, XMLNohSubNivel: IXMLNode;
  Campo: String;
begin
  SetZeroArrs(Fld_Rps, Val_Rps);
  if not (XMLNoSorc.NodeType = ntElement) then
    Exit;
  if XMLNoSorc.IsTextElement then
  begin
    Geral.MB_Aviso('Elemento de texto desconhecido no Grupo ' + G + ':' +
    #13#10 + XMLNoSorc.Text);
  end;
  for I := 0 to XMLNoSorc.AttributeNodes.Count - 1 do
  begin
    AttrNode := XMLNoSorc.AttributeNodes.Nodes[I];
    Campo := Lowercase(AttrNode.NodeName);
    (*
    if Campo = LowerCase('versao') then
      AddSQLItem(TipoDoc, Campo, dmkPF.XMLVal?(AttrNode))
    else
    *)
    //
    AvisaItemDesconhecido(Source, G, AttrNode.NodeName);
  end;
  // Subn�s
  if XMLNoSorc.HasChildNodes then
  begin
    for I := 0 to XMLNoSorc.ChildNodes.Count - 1 do
    begin
      XMLNohSubNivel := XMLNoSorc.ChildNodes.Nodes[I];
      if (XMLNohSubNivel.NodeType = ntElement) then
      begin
        Campo := Lowercase(XMLNohSubNivel.NodeName);
        if Campo = LowerCase('DataEmissao') then
          AddSQLItem(TipoDoc, G + Campo, dmkPF.XMLValDth(XMLNohSubNivel))
        else
        if Campo = LowerCase('Status') then
          AddSQLItem(TipoDoc, G + Campo, dmkPF.XMLValInt(XMLNohSubNivel))
        //
        else
        if Campo = LowerCase('IdentificacaoRps') then
          ImportaGrupo_IdentificacaoRps(Source, XMLNohSubNivel, TipoDoc)
        else
          AvisaItemDesconhecido(Source, G, XMLNohSubNivel.NodeName);
      end;
    end;
  end;
end;

procedure TFmNFSe_LoadXML_0201.ImportaGrupo_Servico(Source: String;
  XMLNoSorc: IXMLNode; TipoDoc: TipoDeArraySQL);
const
  G = 'Servico';
var
  I: Integer;
  AttrNode, XMLNohSubNivel: IXMLNode;
  Campo: String;
begin
  //? SetZeroArrs(Fld_? > Nfse, Val_? > Nfse);
  if not (XMLNoSorc.NodeType = ntElement) then
    Exit;
  if XMLNoSorc.IsTextElement then
  begin
    Geral.MB_Aviso('Elemento de texto desconhecido no Grupo ' + G + ':' +
    #13#10 + XMLNoSorc.Text);
  end;
  for I := 0 to XMLNoSorc.AttributeNodes.Count - 1 do
  begin
    AttrNode := XMLNoSorc.AttributeNodes.Nodes[I];
    Campo := Lowercase(AttrNode.NodeName);
    (*
    if Campo = LowerCase('versao') then
      AddSQLItem(TipoDoc, Campo, dmkPF.XMLVal?(AttrNode))
    else
    *)
    //
    AvisaItemDesconhecido(Source, G, AttrNode.NodeName);
  end;
  // Subn�s
  if XMLNoSorc.HasChildNodes then
  begin
    for I := 0 to XMLNoSorc.ChildNodes.Count - 1 do
    begin
      XMLNohSubNivel := XMLNoSorc.ChildNodes.Nodes[I];
      if (XMLNohSubNivel.NodeType = ntElement) then
      begin
        Campo := Lowercase(XMLNohSubNivel.NodeName);
        if Campo = LowerCase('IssRetido') then
          AddSQLItem(TipoDoc, Campo, dmkPF.XMLValInt(XMLNohSubNivel))
        else
        if Campo = LowerCase('ResponsavelRetencao') then
          AddSQLItem(TipoDoc, Campo, dmkPF.XMLValInt(XMLNohSubNivel))
        else
        if Campo = LowerCase('ItemListaServico') then
          AddSQLItem(TipoDoc, Campo, dmkPF.XMLValTxt(XMLNohSubNivel))
        else
        if Campo = LowerCase('CodigoCnae') then
          AddSQLItem(TipoDoc, Campo, dmkPF.XMLValInt(XMLNohSubNivel))
        else
        if Campo = LowerCase('CodigoTributacaoMunicipio') then
          AddSQLItem(TipoDoc, Campo, dmkPF.XMLValInt(XMLNohSubNivel))
        else
        if Campo = LowerCase('Discriminacao') then
          AddSQLItem(TipoDoc, Campo, dmkPF.XMLValTxt(XMLNohSubNivel))
        else
        if Campo = LowerCase('CodigoMunicipio') then
          AddSQLItem(TipoDoc, Campo, dmkPF.XMLValInt(XMLNohSubNivel))
        else
        if Campo = LowerCase('CodigoPais') then
          AddSQLItem(TipoDoc, Campo, dmkPF.XMLValTxt(XMLNohSubNivel))
        else
        if Campo = LowerCase('ExigibilidadeISS') then
          AddSQLItem(TipoDoc, Campo, dmkPF.XMLValInt(XMLNohSubNivel))
        else
        if Campo = LowerCase('MunicipioIncidencia') then
          AddSQLItem(TipoDoc, Campo, dmkPF.XMLValInt(XMLNohSubNivel))
        else
        if Campo = LowerCase('NumeroProcesso') then
          AddSQLItem(TipoDoc, Campo, dmkPF.XMLValTxt(XMLNohSubNivel))
        else
        //
        if Campo = LowerCase('Valores') then
          ImportaGrupo_Valores(Source, XMLNohSubNivel, TipoDoc)
        else
          AvisaItemDesconhecido(Source, G, XMLNohSubNivel.NodeName);
      end;
    end;
  end;
end;

procedure TFmNFSe_LoadXML_0201.ImportaGrupo_Tomador(Source: String;
  XMLNoSorc: IXMLNode; TipoDoc: TipoDeArraySQL);
const
  G = 'Tomador';
var
  I: Integer;
  AttrNode, XMLNohSubNivel: IXMLNode;
  Campo: String;
begin
  EntiTipo := CO_Toma;
  //
  //? SetZeroArrs(Fld_? > Nfse, Val_? > Nfse);
  if not (XMLNoSorc.NodeType = ntElement) then
    Exit;
  if XMLNoSorc.IsTextElement then
  begin
    Geral.MB_Aviso('Elemento de texto desconhecido no Grupo ' + G + ':' +
    #13#10 + XMLNoSorc.Text);
  end;
  for I := 0 to XMLNoSorc.AttributeNodes.Count - 1 do
  begin
    AttrNode := XMLNoSorc.AttributeNodes.Nodes[I];
    Campo := Lowercase(AttrNode.NodeName);
    (*
    if Campo = LowerCase('versao') then
      AddSQLItem(TipoDoc, Campo, dmkPF.XMLVal?(AttrNode))
    else
    *)
    //
    AvisaItemDesconhecido(Source, G, AttrNode.NodeName);
  end;
  // Subn�s
  if XMLNoSorc.HasChildNodes then
  begin
    for I := 0 to XMLNoSorc.ChildNodes.Count - 1 do
    begin
      XMLNohSubNivel := XMLNoSorc.ChildNodes.Nodes[I];
      if (XMLNohSubNivel.NodeType = ntElement) then
      begin
        Campo := Lowercase(XMLNohSubNivel.NodeName);
        if Campo = LowerCase('RazaoSocial') then
          AddSQLItem(TipoDoc, EntiTipo + Campo, dmkPF.XMLValTxt(XMLNohSubNivel))
        else
        if Campo = LowerCase('NomeFantasia') then
          AddSQLItem(TipoDoc, EntiTipo + Campo, dmkPF.XMLValTxt(XMLNohSubNivel))
        else
        //
        if Campo = LowerCase('IdentificacaoTomador') then
          ImportaGrupo_IdentificacaoTomador(Source, XMLNohSubNivel, TipoDoc)
        else
        if Campo = LowerCase('Endereco') then
          ImportaGrupo_Endereco(Source, XMLNohSubNivel, TipoDoc)
        else
        if Campo = LowerCase('Contato') then
          ImportaGrupo_Contato(Source, XMLNohSubNivel, TipoDoc)
        else
          AvisaItemDesconhecido(Source, G, XMLNohSubNivel.NodeName);
      end;
    end;
  end;
end;

procedure TFmNFSe_LoadXML_0201.ImportaGrupo_Valores(Source: String;
  XMLNoSorc: IXMLNode; TipoDoc: TipoDeArraySQL);
const
  G = 'Valores';
var
  I: Integer;
  AttrNode, XMLNohSubNivel: IXMLNode;
  Campo: String;
begin
  //? SetZeroArrs(Fld_? > Nfse, Val_? > Nfse);
  if not (XMLNoSorc.NodeType = ntElement) then
    Exit;
  if XMLNoSorc.IsTextElement then
  begin
    Geral.MB_Aviso('Elemento de texto desconhecido no Grupo ' + G + ':' +
    #13#10 + XMLNoSorc.Text);
  end;
  for I := 0 to XMLNoSorc.AttributeNodes.Count - 1 do
  begin
    AttrNode := XMLNoSorc.AttributeNodes.Nodes[I];
    Campo := Lowercase(AttrNode.NodeName);
    (*
    if Campo = LowerCase('versao') then
      AddSQLItem(TipoDoc, Campo, dmkPF.XMLVal?(AttrNode))
    else
    *)
    //
    AvisaItemDesconhecido(Source, G, AttrNode.NodeName);
  end;
  // Subn�s
  if XMLNoSorc.HasChildNodes then
  begin
    for I := 0 to XMLNoSorc.ChildNodes.Count - 1 do
    begin
      XMLNohSubNivel := XMLNoSorc.ChildNodes.Nodes[I];
      if (XMLNohSubNivel.NodeType = ntElement) then
      begin
        Campo := Lowercase(XMLNohSubNivel.NodeName);
        if Campo = LowerCase('ValorServicos') then
          AddSQLItem(TipoDoc, Campo, dmkPF.XMLValFlu(XMLNohSubNivel))
        else
        if Campo = LowerCase('ValorDeducoes') then
          AddSQLItem(TipoDoc, Campo, dmkPF.XMLValFlu(XMLNohSubNivel))
        else
        if Campo = LowerCase('ValorPis') then
          AddSQLItem(TipoDoc, Campo, dmkPF.XMLValFlu(XMLNohSubNivel))
        else
        if Campo = LowerCase('ValorCofins') then
          AddSQLItem(TipoDoc, Campo, dmkPF.XMLValFlu(XMLNohSubNivel))
        else
        if Campo = LowerCase('ValorInss') then
          AddSQLItem(TipoDoc, Campo, dmkPF.XMLValFlu(XMLNohSubNivel))
        else
        if Campo = LowerCase('ValorIr') then
          AddSQLItem(TipoDoc, Campo, dmkPF.XMLValFlu(XMLNohSubNivel))
        else
        if Campo = LowerCase('ValorCsll') then
          AddSQLItem(TipoDoc, Campo, dmkPF.XMLValFlu(XMLNohSubNivel))
        else
        if Campo = LowerCase('OutrasRetencoes') then
          AddSQLItem(TipoDoc, Campo, dmkPF.XMLValFlu(XMLNohSubNivel))
        else
        if Campo = LowerCase('ValorIss') then
          AddSQLItem(TipoDoc, Campo, dmkPF.XMLValFlu(XMLNohSubNivel))
        else
        if Campo = LowerCase('Aliquota') then
          AddSQLItem(TipoDoc, Campo, dmkPF.XMLValFlu(XMLNohSubNivel))
        else
        if Campo = LowerCase('DescontoIncondicionado') then
          AddSQLItem(TipoDoc, Campo, dmkPF.XMLValFlu(XMLNohSubNivel))
        else
        if Campo = LowerCase('DescontoCondicionado') then
          AddSQLItem(TipoDoc, Campo, dmkPF.XMLValFlu(XMLNohSubNivel))
        else
        (*
        if Campo = LowerCase('?) then
          ImportaGrupo_?(Source, XMLNohSubNivel)
        else
        *)
          AvisaItemDesconhecido(Source, G, XMLNohSubNivel.NodeName);
      end;
    end;
  end;
end;

procedure TFmNFSe_LoadXML_0201.ImportaGrupo_ValoresNfse(Source: String; XMLNoSorc: IXMLNode; TipoDoc: TipoDeArraySQL);
const
  G = 'ValoresNfse';
var
  I: Integer;
  AttrNode, XMLNohSubNivel: IXMLNode;
  Campo: String;
begin
  if not (XMLNoSorc.NodeType = ntElement) then
    Exit;
  if XMLNoSorc.IsTextElement then
  begin
    Geral.MB_Aviso('Elemento de texto desconhecido no Grupo ' + G + ':' +
    #13#10 + XMLNoSorc.Text);
  end;
  for I := 0 to XMLNoSorc.AttributeNodes.Count - 1 do
  begin
    AttrNode := XMLNoSorc.AttributeNodes.Nodes[I];
    Campo := Lowercase(AttrNode.NodeName);
    (*
    if Campo = LowerCase('versao') then
      AddSQLItem(TipoDoc, Campo, dmkPF.XMLVal?(AttrNode))
    else
    *)
    //
    AvisaItemDesconhecido(Source, G, AttrNode.NodeName);
  end;
  // Subn�s
  if XMLNoSorc.HasChildNodes then
  begin
    for I := 0 to XMLNoSorc.ChildNodes.Count - 1 do
    begin
      XMLNohSubNivel := XMLNoSorc.ChildNodes.Nodes[I];
      if (XMLNohSubNivel.NodeType = ntElement) then
      begin
        Campo := Lowercase(XMLNohSubNivel.NodeName);
        if Campo = LowerCase('BaseCalculo') then
          AddSQLItem(TipoDoc, Campo, dmkPF.XMLValFlu(XMLNohSubNivel))
        else
        if Campo = LowerCase('Aliquota') then
          AddSQLItem(TipoDoc, Campo, dmkPF.XMLValFlu(XMLNohSubNivel))
        else
        if Campo = LowerCase('ValorIss') then
          AddSQLItem(TipoDoc, Campo, dmkPF.XMLValFlu(XMLNohSubNivel))
        else
        if Campo = LowerCase('ValorLiquidoNfse') then
          AddSQLItem(TipoDoc, Campo, dmkPF.XMLValFlu(XMLNohSubNivel))
(*
        else
        if Campo = LowerCase('?') then
          ImportaGrupo_?(Source, XMLNohSubNivel)
*)
        else
          AvisaItemDesconhecido(Source, G, XMLNohSubNivel.NodeName);
      end;
    end;
  end;
end;
*)

function TFmNFSe_LoadXML_0201.NaoPermitePeloAmbiente(): Boolean;
begin
  Result := True;
  if (FAmbiente <> -1) then
  begin
    if FAmbiente <> RGAmbiente.ItemIndex then
    begin
      Geral.MB_Aviso('Ambiente informado difere da base de dados!');
      Exit;
    end;
  end;
  //
  if RGAmbiente.ItemIndex < 1 then
      Geral.MB_Aviso('Ambiente informado � inv�lido!')
  else
    Result := False;
end;

procedure TFmNFSe_LoadXML_0201.SetZeroArrs(var Fld: arrayofString; Val: arrayOfVariant);
begin
  SetLength(Fld, 0);
  SetLength(Val, 0);
end;

procedure TFmNFSe_LoadXML_0201.SQLInsUpd_Nfse();
var
  I, Empresa, Ambiente: Integer;
  Nome: String;
  Continua: Boolean;
begin
  DModG.DefineEntidade(FFoc_Presta, True, Empresa, Nome);
  //
  if NaoPermitePeloAmbiente() then
    Exit;
  Ambiente := RGAmbiente.ItemIndex;
  //  
  UnDmkDAC_PF.AbreMySQLQuery0(QrPsq, Dmod.MyDB, [
  'SELECT Empresa, Numero ',
  'FROM nfsenfscab ',
  'WHERE Empresa=' + Geral.FF0(Empresa),
  'AND Numero=' + FNfse_Numero,
  '']);
  if QrPsq.RecordCount > 0 then
  begin
    Geral.MB_Aviso('A NFSe n� ' + FNfse_Numero + ' da empresa ' +
    Geral.FF0(Empresa) + ' j� foi registrada anteriormente!');
  end else
  begin
    I := Length(Fld_Nfse);
    SetLength(Fld_Nfse, I + 5);
    SetLength(Val_Nfse, I + 5);
    //
    Fld_Nfse[I + 0] := 'RpsIDSerie';
    Fld_Nfse[I + 1] := 'RpsIDNumero';
    Fld_Nfse[I + 2] := 'RpsIDTipo';
    Fld_Nfse[I + 3] := 'Empresa';
    Fld_Nfse[I + 4] := 'Ambiente';

  (*
    Fld_Nfse[I + 4] := 'Cliente';
    Fld_Nfse[I + 5] := CO_Interme;
  *)
    //
    Val_Nfse[I + 0] := FRPS_RpsIDSerie;
    Val_Nfse[I + 1] := FRPS_RpsIDNumero;
    Val_Nfse[I + 2] := FRPS_RpsIDTipo;
    Val_Nfse[I + 3] := Empresa;
    Val_Nfse[I + 4] := Ambiente;
  (*
    Val_Nfse[I + 4] := Cliente;
    Val_Nfse[I + 5] := Interme;
  *)
    //
    UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'nfsenfscab', False,
     Fld_Nfse, [], Val_NFse, [], True);
  end;
end;

procedure TFmNFSe_LoadXML_0201.SQLInsUpd_RPS();
var
  I, Empresa: Integer;
  Nome: String;
begin
  if Geral.IMV(FRPS_RpsIDNumero) <> 0 then
  begin
    DModG.DefineEntidade(FFoc_Presta, True, Empresa, Nome);
    //
    if NaoPermitePeloAmbiente() then
      Exit;
    UnDmkDAC_PF.AbreMySQLQuery0(QrPsq, Dmod.MyDB, [
    'SELECT Codigo ',
    'FROM nfsenfsrps ',
    'WHERE Empresa=' + Geral.FF0(Empresa),
    'AND RpsIDSerie="' + FRPS_RpsIDSerie + '"',
    'AND RpsIDNumero=' + FRPS_RpsIDNumero,
    'AND RpsIDTipo=' + FRPS_RpsIDTipo,
    '']);
    if QrPsq.RecordCount > 0 then
    begin
      Geral.MB_Aviso('O RPS n� ' + FNfse_Numero + ' da empresa ' +
      Geral.FF0(Empresa) + ' j� foi inclu�do anteriormente!');
    end else
    begin
      I := Length(Fld_Rps);
      SetLength(Fld_Rps, I + 2);
      SetLength(Val_Rps, I + 2);
      //
      Fld_Rps[I + 0] := 'Codigo';
      Fld_Rps[I + 1] := 'Empresa';
      //
      Val_Rps[I + 0] := FNfse_Numero;
      Val_Rps[I + 1] := Empresa;
      //
      UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'nfsenfsrps', False,
       Fld_Rps, [], Val_Rps, [], True);
    end;
  end;
end;

(* Exempolo
procedure TFmNFSe_LoadXML_0201.ImportaGrupo_?(Source: String;
  XMLNoSorc: IXMLNode; TipoDoc: TipoDeArraySQL);
const
  G = > ? 'CpfCnpj';
var
  I: Integer;
  AttrNode, XMLNohSubNivel: IXMLNode;
  Campo: String;
begin
  ? SetZeroArrs(Fld_? > Nfse, Val_? > Nfse);
  if not (XMLNoSorc.NodeType = ntElement) then
    Exit;
  if XMLNoSorc.IsTextElement then
  begin
    Geral.MB_Aviso('Elemento de texto desconhecido no Grupo ' + G + ':' +
    #13#10 + XMLNoSorc.Text);
  end;
  for I := 0 to XMLNoSorc.AttributeNodes.Count - 1 do
  begin
    AttrNode := XMLNoSorc.AttributeNodes.Nodes[I];
    Campo := Lowercase(AttrNode.NodeName);
    (*
    if Campo = LowerCase('versao') then
      AddSQLItem(TipoDoc, Campo, dmkPF.XMLVal?(AttrNode))
    else
    *)
    //
    AvisaItemDesconhecido(Source, G, AttrNode.NodeName);
  end;
  // Subn�s
  if XMLNoSorc.HasChildNodes then
  begin
    for I := 0 to XMLNoSorc.ChildNodes.Count - 1 do
    begin
      XMLNohSubNivel := XMLNoSorc.ChildNodes.Nodes[I];
      if (XMLNohSubNivel.NodeType = ntElement) then
      begin
        Campo := Lowercase(XMLNohSubNivel.NodeName);
        (*
        if Campo = LowerCase('?) then
          AddSQLItem(TipoDoc, ? + Campo, dmkPF.XMLVal?(XMLNohSubNivel));
        //else
        (*
        else
        if Campo = LowerCase('?) then
          ImportaGrupo_?(Source, XMLNohSubNivel)
        else
        *)
          AvisaItemDesconhecido(Source, G, XMLNohSubNivel.NodeName);
      end;
    end;
  end;
end;
Fim Exemplo*)
}

procedure TFmNFSe_LoadXML_0201.SpeedButton16Click(Sender: TObject);
var
  IniDir, Arquivo: String;
begin
  IniDir := ExtractFileDir(EdArquivo.Text);
  Arquivo := ExtractFileName(EdArquivo.Text);
  if MyObjects.FileOpenDialog(Self, IniDir, Arquivo,
  'Selecione o arquivo XML', '', [], Arquivo) then
    EdArquivo.Text := Arquivo;
end;

procedure TFmNFSe_LoadXML_0201.RGAmbienteClick(Sender: TObject);
begin
  BtOK.Enabled := RGAmbiente.ItemIndex > 0;
end;

end.
