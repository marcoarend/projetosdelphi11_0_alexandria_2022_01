
{***********************************************************************************************************}
{                                                                                                           }
{                                             XML Data Binding                                              }
{                                                                                                           }
{         Generated on: 27/09/2012 18:22:30                                                                 }
{       Generated from: C:\Projetos_Aux\_NFSE\Maringa\Schemas XML\Homologação\schemas\NFSe_0201_MyRPS.xsd   }
{   Settings stored in: C:\Projetos_Aux\_NFSE\Maringa\Schemas XML\Homologação\schemas\NFSe_0201_MyRPS.xdb   }
{                                                                                                           }
{***********************************************************************************************************}

unit NFSe_0201_MyRPS;

interface

uses xmldom, XMLDoc, XMLIntf;

type

{ Forward Decls }

  IXMLEnviarLoteRpsEnvio = interface;
  IXMLTcLoteRps = interface;
  IXMLTcCpfCnpj = interface;
  IXMLListaRps = interface;
  IXMLTcDeclaracaoPrestacaoServico = interface;
  IXMLTcInfDeclaracaoPrestacaoServico = interface;
  IXMLTcInfRps = interface;
  IXMLTcIdentificacaoRps = interface;
  IXMLTcDadosServico = interface;
  IXMLTcValoresDeclaracaoServico = interface;
  IXMLTcIdentificacaoPrestador = interface;
  IXMLTcDadosTomador = interface;
  IXMLTcIdentificacaoTomador = interface;
  IXMLTcEndereco = interface;
  IXMLTcContato = interface;
  IXMLTcDadosIntermediario = interface;
  IXMLTcIdentificacaoIntermediario = interface;
  IXMLTcDadosConstrucaoCivil = interface;
  IXMLEnviarLoteRpsSincronoEnvio = interface;
  IXMLMyRPS = interface;
  IXMLTcIdentificacaoOrgaoGerador = interface;
  IXMLTcIdentificacaoConsulente = interface;
  IXMLTcValoresNfse = interface;
  IXMLTcDadosPrestador = interface;

{ IXMLEnviarLoteRpsEnvio }

  IXMLEnviarLoteRpsEnvio = interface(IXMLNode)
    ['{4346A5C2-BBC1-46C3-B850-3E1B51D2910D}']
    { Property Accessors }
    function Get_LoteRps: IXMLTcLoteRps;
    function Get_Signature: WideString;
    procedure Set_Signature(Value: WideString);
    { Methods & Properties }
    property LoteRps: IXMLTcLoteRps read Get_LoteRps;
    property Signature: WideString read Get_Signature write Set_Signature;
  end;

{ IXMLTcLoteRps }

  IXMLTcLoteRps = interface(IXMLNode)
    ['{CC8C8AC5-24AB-4544-AD0A-609F176F3399}']
    { Property Accessors }
    function Get_Id: WideString;
    function Get_Versao: WideString;
    function Get_NumeroLote: LongWord;
    function Get_CpfCnpj: IXMLTcCpfCnpj;
    function Get_InscricaoMunicipal: WideString;
    function Get_QuantidadeRps: Integer;
    function Get_ListaRps: IXMLListaRps;
    procedure Set_Id(Value: WideString);
    procedure Set_Versao(Value: WideString);
    procedure Set_NumeroLote(Value: LongWord);
    procedure Set_InscricaoMunicipal(Value: WideString);
    procedure Set_QuantidadeRps(Value: Integer);
    { Methods & Properties }
    property Id: WideString read Get_Id write Set_Id;
    property Versao: WideString read Get_Versao write Set_Versao;
    property NumeroLote: LongWord read Get_NumeroLote write Set_NumeroLote;
    property CpfCnpj: IXMLTcCpfCnpj read Get_CpfCnpj;
    property InscricaoMunicipal: WideString read Get_InscricaoMunicipal write Set_InscricaoMunicipal;
    property QuantidadeRps: Integer read Get_QuantidadeRps write Set_QuantidadeRps;
    property ListaRps: IXMLListaRps read Get_ListaRps;
  end;

{ IXMLTcCpfCnpj }

  IXMLTcCpfCnpj = interface(IXMLNode)
    ['{5E0D8883-C79A-42EB-A066-7FE688C5C449}']
    { Property Accessors }
    function Get_Cpf: WideString;
    function Get_Cnpj: WideString;
    procedure Set_Cpf(Value: WideString);
    procedure Set_Cnpj(Value: WideString);
    { Methods & Properties }
    property Cpf: WideString read Get_Cpf write Set_Cpf;
    property Cnpj: WideString read Get_Cnpj write Set_Cnpj;
  end;

{ IXMLListaRps }

  IXMLListaRps = interface(IXMLNodeCollection)
    ['{CA67414B-E928-4180-ADF1-64E5201C8206}']
    { Property Accessors }
    function Get_Rps(Index: Integer): IXMLTcDeclaracaoPrestacaoServico;
    { Methods & Properties }
    function Add: IXMLTcDeclaracaoPrestacaoServico;
    function Insert(const Index: Integer): IXMLTcDeclaracaoPrestacaoServico;
    property Rps[Index: Integer]: IXMLTcDeclaracaoPrestacaoServico read Get_Rps; default;
  end;

{ IXMLTcDeclaracaoPrestacaoServico }

  IXMLTcDeclaracaoPrestacaoServico = interface(IXMLNode)
    ['{F782BF01-4CAA-496E-9FAB-E7CABC7462D1}']
    { Property Accessors }
    function Get_InfDeclaracaoPrestacaoServico: IXMLTcInfDeclaracaoPrestacaoServico;
    function Get_Signature: WideString;
    procedure Set_Signature(Value: WideString);
    { Methods & Properties }
    property InfDeclaracaoPrestacaoServico: IXMLTcInfDeclaracaoPrestacaoServico read Get_InfDeclaracaoPrestacaoServico;
    property Signature: WideString read Get_Signature write Set_Signature;
  end;

{ IXMLTcInfDeclaracaoPrestacaoServico }

  IXMLTcInfDeclaracaoPrestacaoServico = interface(IXMLNode)
    ['{73048306-CD55-4A78-9C38-FBFE7DFE5204}']
    { Property Accessors }
    function Get_Id: WideString;
    function Get_Rps: IXMLTcInfRps;
    function Get_Competencia: WideString;
    function Get_Servico: IXMLTcDadosServico;
    function Get_Prestador: IXMLTcIdentificacaoPrestador;
    function Get_Tomador: IXMLTcDadosTomador;
    function Get_Intermediario: IXMLTcDadosIntermediario;
    function Get_ConstrucaoCivil: IXMLTcDadosConstrucaoCivil;
    function Get_RegimeEspecialTributacao: ShortInt;
    function Get_OptanteSimplesNacional: ShortInt;
    function Get_IncentivoFiscal: ShortInt;
    procedure Set_Id(Value: WideString);
    procedure Set_Competencia(Value: WideString);
    procedure Set_RegimeEspecialTributacao(Value: ShortInt);
    procedure Set_OptanteSimplesNacional(Value: ShortInt);
    procedure Set_IncentivoFiscal(Value: ShortInt);
    { Methods & Properties }
    property Id: WideString read Get_Id write Set_Id;
    property Rps: IXMLTcInfRps read Get_Rps;
    property Competencia: WideString read Get_Competencia write Set_Competencia;
    property Servico: IXMLTcDadosServico read Get_Servico;
    property Prestador: IXMLTcIdentificacaoPrestador read Get_Prestador;
    property Tomador: IXMLTcDadosTomador read Get_Tomador;
    property Intermediario: IXMLTcDadosIntermediario read Get_Intermediario;
    property ConstrucaoCivil: IXMLTcDadosConstrucaoCivil read Get_ConstrucaoCivil;
    property RegimeEspecialTributacao: ShortInt read Get_RegimeEspecialTributacao write Set_RegimeEspecialTributacao;
    property OptanteSimplesNacional: ShortInt read Get_OptanteSimplesNacional write Set_OptanteSimplesNacional;
    property IncentivoFiscal: ShortInt read Get_IncentivoFiscal write Set_IncentivoFiscal;
  end;

{ IXMLTcInfRps }

  IXMLTcInfRps = interface(IXMLNode)
    ['{2A13DA43-B7C0-4950-9454-F30150698F73}']
    { Property Accessors }
    function Get_Id: WideString;
    function Get_IdentificacaoRps: IXMLTcIdentificacaoRps;
    function Get_DataEmissao: WideString;
    function Get_Status: ShortInt;
    function Get_RpsSubstituido: IXMLTcIdentificacaoRps;
    procedure Set_Id(Value: WideString);
    procedure Set_DataEmissao(Value: WideString);
    procedure Set_Status(Value: ShortInt);
    { Methods & Properties }
    property Id: WideString read Get_Id write Set_Id;
    property IdentificacaoRps: IXMLTcIdentificacaoRps read Get_IdentificacaoRps;
    property DataEmissao: WideString read Get_DataEmissao write Set_DataEmissao;
    property Status: ShortInt read Get_Status write Set_Status;
    property RpsSubstituido: IXMLTcIdentificacaoRps read Get_RpsSubstituido;
  end;

{ IXMLTcIdentificacaoRps }

  IXMLTcIdentificacaoRps = interface(IXMLNode)
    ['{3C84BAAC-530D-417C-B516-355E06D853F6}']
    { Property Accessors }
    function Get_Numero: LongWord;
    function Get_Serie: WideString;
    function Get_Tipo: ShortInt;
    procedure Set_Numero(Value: LongWord);
    procedure Set_Serie(Value: WideString);
    procedure Set_Tipo(Value: ShortInt);
    { Methods & Properties }
    property Numero: LongWord read Get_Numero write Set_Numero;
    property Serie: WideString read Get_Serie write Set_Serie;
    property Tipo: ShortInt read Get_Tipo write Set_Tipo;
  end;

{ IXMLTcDadosServico }

  IXMLTcDadosServico = interface(IXMLNode)
    ['{FDAA89BA-DB33-44AC-AB5B-2B200C8CB015}']
    { Property Accessors }
    function Get_Valores: IXMLTcValoresDeclaracaoServico;
    function Get_IssRetido: ShortInt;
    function Get_ResponsavelRetencao: ShortInt;
    function Get_ItemListaServico: WideString;
    function Get_CodigoCnae: Integer;
    function Get_CodigoTributacaoMunicipio: WideString;
    function Get_Discriminacao: WideString;
    function Get_CodigoMunicipio: Integer;
    function Get_CodigoPais: WideString;
    function Get_ExigibilidadeISS: ShortInt;
    function Get_MunicipioIncidencia: Integer;
    function Get_NumeroProcesso: WideString;
    procedure Set_IssRetido(Value: ShortInt);
    procedure Set_ResponsavelRetencao(Value: ShortInt);
    procedure Set_ItemListaServico(Value: WideString);
    procedure Set_CodigoCnae(Value: Integer);
    procedure Set_CodigoTributacaoMunicipio(Value: WideString);
    procedure Set_Discriminacao(Value: WideString);
    procedure Set_CodigoMunicipio(Value: Integer);
    procedure Set_CodigoPais(Value: WideString);
    procedure Set_ExigibilidadeISS(Value: ShortInt);
    procedure Set_MunicipioIncidencia(Value: Integer);
    procedure Set_NumeroProcesso(Value: WideString);
    { Methods & Properties }
    property Valores: IXMLTcValoresDeclaracaoServico read Get_Valores;
    property IssRetido: ShortInt read Get_IssRetido write Set_IssRetido;
    property ResponsavelRetencao: ShortInt read Get_ResponsavelRetencao write Set_ResponsavelRetencao;
    property ItemListaServico: WideString read Get_ItemListaServico write Set_ItemListaServico;
    property CodigoCnae: Integer read Get_CodigoCnae write Set_CodigoCnae;
    property CodigoTributacaoMunicipio: WideString read Get_CodigoTributacaoMunicipio write Set_CodigoTributacaoMunicipio;
    property Discriminacao: WideString read Get_Discriminacao write Set_Discriminacao;
    property CodigoMunicipio: Integer read Get_CodigoMunicipio write Set_CodigoMunicipio;
    property CodigoPais: WideString read Get_CodigoPais write Set_CodigoPais;
    property ExigibilidadeISS: ShortInt read Get_ExigibilidadeISS write Set_ExigibilidadeISS;
    property MunicipioIncidencia: Integer read Get_MunicipioIncidencia write Set_MunicipioIncidencia;
    property NumeroProcesso: WideString read Get_NumeroProcesso write Set_NumeroProcesso;
  end;

{ IXMLTcValoresDeclaracaoServico }

  IXMLTcValoresDeclaracaoServico = interface(IXMLNode)
    ['{70F439B2-7282-421D-9369-844424BFAB48}']
    { Property Accessors }
    function Get_ValorServicos: WideString;
    function Get_ValorDeducoes: WideString;
    function Get_ValorPis: WideString;
    function Get_ValorCofins: WideString;
    function Get_ValorInss: WideString;
    function Get_ValorIr: WideString;
    function Get_ValorCsll: WideString;
    function Get_OutrasRetencoes: WideString;
    function Get_ValorIss: WideString;
    function Get_Aliquota: WideString;
    function Get_DescontoIncondicionado: WideString;
    function Get_DescontoCondicionado: WideString;
    procedure Set_ValorServicos(Value: WideString);
    procedure Set_ValorDeducoes(Value: WideString);
    procedure Set_ValorPis(Value: WideString);
    procedure Set_ValorCofins(Value: WideString);
    procedure Set_ValorInss(Value: WideString);
    procedure Set_ValorIr(Value: WideString);
    procedure Set_ValorCsll(Value: WideString);
    procedure Set_OutrasRetencoes(Value: WideString);
    procedure Set_ValorIss(Value: WideString);
    procedure Set_Aliquota(Value: WideString);
    procedure Set_DescontoIncondicionado(Value: WideString);
    procedure Set_DescontoCondicionado(Value: WideString);
    { Methods & Properties }
    property ValorServicos: WideString read Get_ValorServicos write Set_ValorServicos;
    property ValorDeducoes: WideString read Get_ValorDeducoes write Set_ValorDeducoes;
    property ValorPis: WideString read Get_ValorPis write Set_ValorPis;
    property ValorCofins: WideString read Get_ValorCofins write Set_ValorCofins;
    property ValorInss: WideString read Get_ValorInss write Set_ValorInss;
    property ValorIr: WideString read Get_ValorIr write Set_ValorIr;
    property ValorCsll: WideString read Get_ValorCsll write Set_ValorCsll;
    property OutrasRetencoes: WideString read Get_OutrasRetencoes write Set_OutrasRetencoes;
    property ValorIss: WideString read Get_ValorIss write Set_ValorIss;
    property Aliquota: WideString read Get_Aliquota write Set_Aliquota;
    property DescontoIncondicionado: WideString read Get_DescontoIncondicionado write Set_DescontoIncondicionado;
    property DescontoCondicionado: WideString read Get_DescontoCondicionado write Set_DescontoCondicionado;
  end;

{ IXMLTcIdentificacaoPrestador }

  IXMLTcIdentificacaoPrestador = interface(IXMLNode)
    ['{2C497F33-31A8-47D1-ACE9-6EB1980B107A}']
    { Property Accessors }
    function Get_CpfCnpj: IXMLTcCpfCnpj;
    function Get_InscricaoMunicipal: WideString;
    procedure Set_InscricaoMunicipal(Value: WideString);
    { Methods & Properties }
    property CpfCnpj: IXMLTcCpfCnpj read Get_CpfCnpj;
    property InscricaoMunicipal: WideString read Get_InscricaoMunicipal write Set_InscricaoMunicipal;
  end;

{ IXMLTcDadosTomador }

  IXMLTcDadosTomador = interface(IXMLNode)
    ['{328141E6-7D38-41F4-BF22-8F4B4C3441C1}']
    { Property Accessors }
    function Get_IdentificacaoTomador: IXMLTcIdentificacaoTomador;
    function Get_RazaoSocial: WideString;
    function Get_Endereco: IXMLTcEndereco;
    function Get_Contato: IXMLTcContato;
    procedure Set_RazaoSocial(Value: WideString);
    { Methods & Properties }
    property IdentificacaoTomador: IXMLTcIdentificacaoTomador read Get_IdentificacaoTomador;
    property RazaoSocial: WideString read Get_RazaoSocial write Set_RazaoSocial;
    property Endereco: IXMLTcEndereco read Get_Endereco;
    property Contato: IXMLTcContato read Get_Contato;
  end;

{ IXMLTcIdentificacaoTomador }

  IXMLTcIdentificacaoTomador = interface(IXMLNode)
    ['{92B6F884-E66B-4175-9F23-F1C4B03583B6}']
    { Property Accessors }
    function Get_CpfCnpj: IXMLTcCpfCnpj;
    function Get_InscricaoMunicipal: WideString;
    procedure Set_InscricaoMunicipal(Value: WideString);
    { Methods & Properties }
    property CpfCnpj: IXMLTcCpfCnpj read Get_CpfCnpj;
    property InscricaoMunicipal: WideString read Get_InscricaoMunicipal write Set_InscricaoMunicipal;
  end;

{ IXMLTcEndereco }

  IXMLTcEndereco = interface(IXMLNode)
    ['{60425CF7-79AF-46E1-A117-CC88093FED98}']
    { Property Accessors }
    function Get_Endereco: WideString;
    function Get_Numero: WideString;
    function Get_Complemento: WideString;
    function Get_Bairro: WideString;
    function Get_CodigoMunicipio: Integer;
    function Get_Uf: WideString;
    function Get_CodigoPais: WideString;
    function Get_Cep: WideString;
    procedure Set_Endereco(Value: WideString);
    procedure Set_Numero(Value: WideString);
    procedure Set_Complemento(Value: WideString);
    procedure Set_Bairro(Value: WideString);
    procedure Set_CodigoMunicipio(Value: Integer);
    procedure Set_Uf(Value: WideString);
    procedure Set_CodigoPais(Value: WideString);
    procedure Set_Cep(Value: WideString);
    { Methods & Properties }
    property Endereco: WideString read Get_Endereco write Set_Endereco;
    property Numero: WideString read Get_Numero write Set_Numero;
    property Complemento: WideString read Get_Complemento write Set_Complemento;
    property Bairro: WideString read Get_Bairro write Set_Bairro;
    property CodigoMunicipio: Integer read Get_CodigoMunicipio write Set_CodigoMunicipio;
    property Uf: WideString read Get_Uf write Set_Uf;
    property CodigoPais: WideString read Get_CodigoPais write Set_CodigoPais;
    property Cep: WideString read Get_Cep write Set_Cep;
  end;

{ IXMLTcContato }

  IXMLTcContato = interface(IXMLNode)
    ['{F97A7D9B-76B1-4B8D-9CF4-21744FCBA0E2}']
    { Property Accessors }
    function Get_Telefone: WideString;
    function Get_Email: WideString;
    procedure Set_Telefone(Value: WideString);
    procedure Set_Email(Value: WideString);
    { Methods & Properties }
    property Telefone: WideString read Get_Telefone write Set_Telefone;
    property Email: WideString read Get_Email write Set_Email;
  end;

{ IXMLTcDadosIntermediario }

  IXMLTcDadosIntermediario = interface(IXMLNode)
    ['{AFFFCF17-E131-4C49-A8C8-18A1D2AC6D47}']
    { Property Accessors }
    function Get_IdentificacaoIntermediario: IXMLTcIdentificacaoIntermediario;
    function Get_RazaoSocial: WideString;
    procedure Set_RazaoSocial(Value: WideString);
    { Methods & Properties }
    property IdentificacaoIntermediario: IXMLTcIdentificacaoIntermediario read Get_IdentificacaoIntermediario;
    property RazaoSocial: WideString read Get_RazaoSocial write Set_RazaoSocial;
  end;

{ IXMLTcIdentificacaoIntermediario }

  IXMLTcIdentificacaoIntermediario = interface(IXMLNode)
    ['{C94DCABA-C099-4B93-82A9-1E3B6DBB2B2B}']
    { Property Accessors }
    function Get_CpfCnpj: IXMLTcCpfCnpj;
    function Get_InscricaoMunicipal: WideString;
    procedure Set_InscricaoMunicipal(Value: WideString);
    { Methods & Properties }
    property CpfCnpj: IXMLTcCpfCnpj read Get_CpfCnpj;
    property InscricaoMunicipal: WideString read Get_InscricaoMunicipal write Set_InscricaoMunicipal;
  end;

{ IXMLTcDadosConstrucaoCivil }

  IXMLTcDadosConstrucaoCivil = interface(IXMLNode)
    ['{96B8A2BA-7366-47BF-8972-698D59130908}']
    { Property Accessors }
    function Get_CodigoObra: WideString;
    function Get_Art: WideString;
    procedure Set_CodigoObra(Value: WideString);
    procedure Set_Art(Value: WideString);
    { Methods & Properties }
    property CodigoObra: WideString read Get_CodigoObra write Set_CodigoObra;
    property Art: WideString read Get_Art write Set_Art;
  end;

{ IXMLEnviarLoteRpsSincronoEnvio }

  IXMLEnviarLoteRpsSincronoEnvio = interface(IXMLNode)
    ['{BE345AE1-4EA3-4EF1-AD0E-AAB004F8290B}']
    { Property Accessors }
    function Get_LoteRps: IXMLTcLoteRps;
    function Get_Signature: WideString;
    procedure Set_Signature(Value: WideString);
    { Methods & Properties }
    property LoteRps: IXMLTcLoteRps read Get_LoteRps;
    property Signature: WideString read Get_Signature write Set_Signature;
  end;

{ IXMLMyRPS }

  IXMLMyRPS = interface(IXMLNode)
    ['{A4DA454A-9C4E-4C67-ACF1-8FBA4E10A2C3}']
    { Property Accessors }
    function Get_Rps: IXMLTcDeclaracaoPrestacaoServico;
    { Methods & Properties }
    property Rps: IXMLTcDeclaracaoPrestacaoServico read Get_Rps;
  end;

{ IXMLTcIdentificacaoOrgaoGerador }

  IXMLTcIdentificacaoOrgaoGerador = interface(IXMLNode)
    ['{11FBFC4A-B293-4F3E-A1AF-AE7F3770B700}']
    { Property Accessors }
    function Get_CodigoMunicipio: Integer;
    function Get_Uf: WideString;
    procedure Set_CodigoMunicipio(Value: Integer);
    procedure Set_Uf(Value: WideString);
    { Methods & Properties }
    property CodigoMunicipio: Integer read Get_CodigoMunicipio write Set_CodigoMunicipio;
    property Uf: WideString read Get_Uf write Set_Uf;
  end;

{ IXMLTcIdentificacaoConsulente }

  IXMLTcIdentificacaoConsulente = interface(IXMLNode)
    ['{4743633C-2942-4741-B486-9CF0087B4B4C}']
    { Property Accessors }
    function Get_CpfCnpj: IXMLTcCpfCnpj;
    function Get_InscricaoMunicipal: WideString;
    procedure Set_InscricaoMunicipal(Value: WideString);
    { Methods & Properties }
    property CpfCnpj: IXMLTcCpfCnpj read Get_CpfCnpj;
    property InscricaoMunicipal: WideString read Get_InscricaoMunicipal write Set_InscricaoMunicipal;
  end;

{ IXMLTcValoresNfse }

  IXMLTcValoresNfse = interface(IXMLNode)
    ['{8EFBF628-FA09-4F11-938C-2660C815A88E}']
    { Property Accessors }
    function Get_BaseCalculo: WideString;
    function Get_Aliquota: WideString;
    function Get_ValorIss: WideString;
    function Get_ValorLiquidoNfse: WideString;
    procedure Set_BaseCalculo(Value: WideString);
    procedure Set_Aliquota(Value: WideString);
    procedure Set_ValorIss(Value: WideString);
    procedure Set_ValorLiquidoNfse(Value: WideString);
    { Methods & Properties }
    property BaseCalculo: WideString read Get_BaseCalculo write Set_BaseCalculo;
    property Aliquota: WideString read Get_Aliquota write Set_Aliquota;
    property ValorIss: WideString read Get_ValorIss write Set_ValorIss;
    property ValorLiquidoNfse: WideString read Get_ValorLiquidoNfse write Set_ValorLiquidoNfse;
  end;

{ IXMLTcDadosPrestador }

  IXMLTcDadosPrestador = interface(IXMLNode)
    ['{AD128E94-7633-4939-95A8-18D8A357C420}']
    { Property Accessors }
    function Get_IdentificacaoPrestador: IXMLTcIdentificacaoPrestador;
    function Get_RazaoSocial: WideString;
    function Get_NomeFantasia: WideString;
    function Get_Endereco: IXMLTcEndereco;
    function Get_Contato: IXMLTcContato;
    procedure Set_RazaoSocial(Value: WideString);
    procedure Set_NomeFantasia(Value: WideString);
    { Methods & Properties }
    property IdentificacaoPrestador: IXMLTcIdentificacaoPrestador read Get_IdentificacaoPrestador;
    property RazaoSocial: WideString read Get_RazaoSocial write Set_RazaoSocial;
    property NomeFantasia: WideString read Get_NomeFantasia write Set_NomeFantasia;
    property Endereco: IXMLTcEndereco read Get_Endereco;
    property Contato: IXMLTcContato read Get_Contato;
  end;

{ Forward Decls }

  TXMLEnviarLoteRpsEnvio = class;
  TXMLTcLoteRps = class;
  TXMLTcCpfCnpj = class;
  TXMLListaRps = class;
  TXMLTcDeclaracaoPrestacaoServico = class;
  TXMLTcInfDeclaracaoPrestacaoServico = class;
  TXMLTcInfRps = class;
  TXMLTcIdentificacaoRps = class;
  TXMLTcDadosServico = class;
  TXMLTcValoresDeclaracaoServico = class;
  TXMLTcIdentificacaoPrestador = class;
  TXMLTcDadosTomador = class;
  TXMLTcIdentificacaoTomador = class;
  TXMLTcEndereco = class;
  TXMLTcContato = class;
  TXMLTcDadosIntermediario = class;
  TXMLTcIdentificacaoIntermediario = class;
  TXMLTcDadosConstrucaoCivil = class;
  TXMLEnviarLoteRpsSincronoEnvio = class;
  TXMLMyRPS = class;
  TXMLTcIdentificacaoOrgaoGerador = class;
  TXMLTcIdentificacaoConsulente = class;
  TXMLTcValoresNfse = class;
  TXMLTcDadosPrestador = class;

{ TXMLEnviarLoteRpsEnvio }

  TXMLEnviarLoteRpsEnvio = class(TXMLNode, IXMLEnviarLoteRpsEnvio)
  protected
    { IXMLEnviarLoteRpsEnvio }
    function Get_LoteRps: IXMLTcLoteRps;
    function Get_Signature: WideString;
    procedure Set_Signature(Value: WideString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLTcLoteRps }

  TXMLTcLoteRps = class(TXMLNode, IXMLTcLoteRps)
  protected
    { IXMLTcLoteRps }
    function Get_Id: WideString;
    function Get_Versao: WideString;
    function Get_NumeroLote: LongWord;
    function Get_CpfCnpj: IXMLTcCpfCnpj;
    function Get_InscricaoMunicipal: WideString;
    function Get_QuantidadeRps: Integer;
    function Get_ListaRps: IXMLListaRps;
    procedure Set_Id(Value: WideString);
    procedure Set_Versao(Value: WideString);
    procedure Set_NumeroLote(Value: LongWord);
    procedure Set_InscricaoMunicipal(Value: WideString);
    procedure Set_QuantidadeRps(Value: Integer);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLTcCpfCnpj }

  TXMLTcCpfCnpj = class(TXMLNode, IXMLTcCpfCnpj)
  protected
    { IXMLTcCpfCnpj }
    function Get_Cpf: WideString;
    function Get_Cnpj: WideString;
    procedure Set_Cpf(Value: WideString);
    procedure Set_Cnpj(Value: WideString);
  end;

{ TXMLListaRps }

  TXMLListaRps = class(TXMLNodeCollection, IXMLListaRps)
  protected
    { IXMLListaRps }
    function Get_Rps(Index: Integer): IXMLTcDeclaracaoPrestacaoServico;
    function Add: IXMLTcDeclaracaoPrestacaoServico;
    function Insert(const Index: Integer): IXMLTcDeclaracaoPrestacaoServico;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLTcDeclaracaoPrestacaoServico }

  TXMLTcDeclaracaoPrestacaoServico = class(TXMLNode, IXMLTcDeclaracaoPrestacaoServico)
  protected
    { IXMLTcDeclaracaoPrestacaoServico }
    function Get_InfDeclaracaoPrestacaoServico: IXMLTcInfDeclaracaoPrestacaoServico;
    function Get_Signature: WideString;
    procedure Set_Signature(Value: WideString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLTcInfDeclaracaoPrestacaoServico }

  TXMLTcInfDeclaracaoPrestacaoServico = class(TXMLNode, IXMLTcInfDeclaracaoPrestacaoServico)
  protected
    { IXMLTcInfDeclaracaoPrestacaoServico }
    function Get_Id: WideString;
    function Get_Rps: IXMLTcInfRps;
    function Get_Competencia: WideString;
    function Get_Servico: IXMLTcDadosServico;
    function Get_Prestador: IXMLTcIdentificacaoPrestador;
    function Get_Tomador: IXMLTcDadosTomador;
    function Get_Intermediario: IXMLTcDadosIntermediario;
    function Get_ConstrucaoCivil: IXMLTcDadosConstrucaoCivil;
    function Get_RegimeEspecialTributacao: ShortInt;
    function Get_OptanteSimplesNacional: ShortInt;
    function Get_IncentivoFiscal: ShortInt;
    procedure Set_Id(Value: WideString);
    procedure Set_Competencia(Value: WideString);
    procedure Set_RegimeEspecialTributacao(Value: ShortInt);
    procedure Set_OptanteSimplesNacional(Value: ShortInt);
    procedure Set_IncentivoFiscal(Value: ShortInt);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLTcInfRps }

  TXMLTcInfRps = class(TXMLNode, IXMLTcInfRps)
  protected
    { IXMLTcInfRps }
    function Get_Id: WideString;
    function Get_IdentificacaoRps: IXMLTcIdentificacaoRps;
    function Get_DataEmissao: WideString;
    function Get_Status: ShortInt;
    function Get_RpsSubstituido: IXMLTcIdentificacaoRps;
    procedure Set_Id(Value: WideString);
    procedure Set_DataEmissao(Value: WideString);
    procedure Set_Status(Value: ShortInt);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLTcIdentificacaoRps }

  TXMLTcIdentificacaoRps = class(TXMLNode, IXMLTcIdentificacaoRps)
  protected
    { IXMLTcIdentificacaoRps }
    function Get_Numero: LongWord;
    function Get_Serie: WideString;
    function Get_Tipo: ShortInt;
    procedure Set_Numero(Value: LongWord);
    procedure Set_Serie(Value: WideString);
    procedure Set_Tipo(Value: ShortInt);
  end;

{ TXMLTcDadosServico }

  TXMLTcDadosServico = class(TXMLNode, IXMLTcDadosServico)
  protected
    { IXMLTcDadosServico }
    function Get_Valores: IXMLTcValoresDeclaracaoServico;
    function Get_IssRetido: ShortInt;
    function Get_ResponsavelRetencao: ShortInt;
    function Get_ItemListaServico: WideString;
    function Get_CodigoCnae: Integer;
    function Get_CodigoTributacaoMunicipio: WideString;
    function Get_Discriminacao: WideString;
    function Get_CodigoMunicipio: Integer;
    function Get_CodigoPais: WideString;
    function Get_ExigibilidadeISS: ShortInt;
    function Get_MunicipioIncidencia: Integer;
    function Get_NumeroProcesso: WideString;
    procedure Set_IssRetido(Value: ShortInt);
    procedure Set_ResponsavelRetencao(Value: ShortInt);
    procedure Set_ItemListaServico(Value: WideString);
    procedure Set_CodigoCnae(Value: Integer);
    procedure Set_CodigoTributacaoMunicipio(Value: WideString);
    procedure Set_Discriminacao(Value: WideString);
    procedure Set_CodigoMunicipio(Value: Integer);
    procedure Set_CodigoPais(Value: WideString);
    procedure Set_ExigibilidadeISS(Value: ShortInt);
    procedure Set_MunicipioIncidencia(Value: Integer);
    procedure Set_NumeroProcesso(Value: WideString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLTcValoresDeclaracaoServico }

  TXMLTcValoresDeclaracaoServico = class(TXMLNode, IXMLTcValoresDeclaracaoServico)
  protected
    { IXMLTcValoresDeclaracaoServico }
    function Get_ValorServicos: WideString;
    function Get_ValorDeducoes: WideString;
    function Get_ValorPis: WideString;
    function Get_ValorCofins: WideString;
    function Get_ValorInss: WideString;
    function Get_ValorIr: WideString;
    function Get_ValorCsll: WideString;
    function Get_OutrasRetencoes: WideString;
    function Get_ValorIss: WideString;
    function Get_Aliquota: WideString;
    function Get_DescontoIncondicionado: WideString;
    function Get_DescontoCondicionado: WideString;
    procedure Set_ValorServicos(Value: WideString);
    procedure Set_ValorDeducoes(Value: WideString);
    procedure Set_ValorPis(Value: WideString);
    procedure Set_ValorCofins(Value: WideString);
    procedure Set_ValorInss(Value: WideString);
    procedure Set_ValorIr(Value: WideString);
    procedure Set_ValorCsll(Value: WideString);
    procedure Set_OutrasRetencoes(Value: WideString);
    procedure Set_ValorIss(Value: WideString);
    procedure Set_Aliquota(Value: WideString);
    procedure Set_DescontoIncondicionado(Value: WideString);
    procedure Set_DescontoCondicionado(Value: WideString);
  end;

{ TXMLTcIdentificacaoPrestador }

  TXMLTcIdentificacaoPrestador = class(TXMLNode, IXMLTcIdentificacaoPrestador)
  protected
    { IXMLTcIdentificacaoPrestador }
    function Get_CpfCnpj: IXMLTcCpfCnpj;
    function Get_InscricaoMunicipal: WideString;
    procedure Set_InscricaoMunicipal(Value: WideString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLTcDadosTomador }

  TXMLTcDadosTomador = class(TXMLNode, IXMLTcDadosTomador)
  protected
    { IXMLTcDadosTomador }
    function Get_IdentificacaoTomador: IXMLTcIdentificacaoTomador;
    function Get_RazaoSocial: WideString;
    function Get_Endereco: IXMLTcEndereco;
    function Get_Contato: IXMLTcContato;
    procedure Set_RazaoSocial(Value: WideString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLTcIdentificacaoTomador }

  TXMLTcIdentificacaoTomador = class(TXMLNode, IXMLTcIdentificacaoTomador)
  protected
    { IXMLTcIdentificacaoTomador }
    function Get_CpfCnpj: IXMLTcCpfCnpj;
    function Get_InscricaoMunicipal: WideString;
    procedure Set_InscricaoMunicipal(Value: WideString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLTcEndereco }

  TXMLTcEndereco = class(TXMLNode, IXMLTcEndereco)
  protected
    { IXMLTcEndereco }
    function Get_Endereco: WideString;
    function Get_Numero: WideString;
    function Get_Complemento: WideString;
    function Get_Bairro: WideString;
    function Get_CodigoMunicipio: Integer;
    function Get_Uf: WideString;
    function Get_CodigoPais: WideString;
    function Get_Cep: WideString;
    procedure Set_Endereco(Value: WideString);
    procedure Set_Numero(Value: WideString);
    procedure Set_Complemento(Value: WideString);
    procedure Set_Bairro(Value: WideString);
    procedure Set_CodigoMunicipio(Value: Integer);
    procedure Set_Uf(Value: WideString);
    procedure Set_CodigoPais(Value: WideString);
    procedure Set_Cep(Value: WideString);
  end;

{ TXMLTcContato }

  TXMLTcContato = class(TXMLNode, IXMLTcContato)
  protected
    { IXMLTcContato }
    function Get_Telefone: WideString;
    function Get_Email: WideString;
    procedure Set_Telefone(Value: WideString);
    procedure Set_Email(Value: WideString);
  end;

{ TXMLTcDadosIntermediario }

  TXMLTcDadosIntermediario = class(TXMLNode, IXMLTcDadosIntermediario)
  protected
    { IXMLTcDadosIntermediario }
    function Get_IdentificacaoIntermediario: IXMLTcIdentificacaoIntermediario;
    function Get_RazaoSocial: WideString;
    procedure Set_RazaoSocial(Value: WideString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLTcIdentificacaoIntermediario }

  TXMLTcIdentificacaoIntermediario = class(TXMLNode, IXMLTcIdentificacaoIntermediario)
  protected
    { IXMLTcIdentificacaoIntermediario }
    function Get_CpfCnpj: IXMLTcCpfCnpj;
    function Get_InscricaoMunicipal: WideString;
    procedure Set_InscricaoMunicipal(Value: WideString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLTcDadosConstrucaoCivil }

  TXMLTcDadosConstrucaoCivil = class(TXMLNode, IXMLTcDadosConstrucaoCivil)
  protected
    { IXMLTcDadosConstrucaoCivil }
    function Get_CodigoObra: WideString;
    function Get_Art: WideString;
    procedure Set_CodigoObra(Value: WideString);
    procedure Set_Art(Value: WideString);
  end;

{ TXMLEnviarLoteRpsSincronoEnvio }

  TXMLEnviarLoteRpsSincronoEnvio = class(TXMLNode, IXMLEnviarLoteRpsSincronoEnvio)
  protected
    { IXMLEnviarLoteRpsSincronoEnvio }
    function Get_LoteRps: IXMLTcLoteRps;
    function Get_Signature: WideString;
    procedure Set_Signature(Value: WideString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLMyRPS }

  TXMLMyRPS = class(TXMLNode, IXMLMyRPS)
  protected
    { IXMLMyRPS }
    function Get_Rps: IXMLTcDeclaracaoPrestacaoServico;
  public
    procedure AfterConstruction; override;
  end;

{ TXMLTcIdentificacaoOrgaoGerador }

  TXMLTcIdentificacaoOrgaoGerador = class(TXMLNode, IXMLTcIdentificacaoOrgaoGerador)
  protected
    { IXMLTcIdentificacaoOrgaoGerador }
    function Get_CodigoMunicipio: Integer;
    function Get_Uf: WideString;
    procedure Set_CodigoMunicipio(Value: Integer);
    procedure Set_Uf(Value: WideString);
  end;

{ TXMLTcIdentificacaoConsulente }

  TXMLTcIdentificacaoConsulente = class(TXMLNode, IXMLTcIdentificacaoConsulente)
  protected
    { IXMLTcIdentificacaoConsulente }
    function Get_CpfCnpj: IXMLTcCpfCnpj;
    function Get_InscricaoMunicipal: WideString;
    procedure Set_InscricaoMunicipal(Value: WideString);
  public
    procedure AfterConstruction; override;
  end;

{ TXMLTcValoresNfse }

  TXMLTcValoresNfse = class(TXMLNode, IXMLTcValoresNfse)
  protected
    { IXMLTcValoresNfse }
    function Get_BaseCalculo: WideString;
    function Get_Aliquota: WideString;
    function Get_ValorIss: WideString;
    function Get_ValorLiquidoNfse: WideString;
    procedure Set_BaseCalculo(Value: WideString);
    procedure Set_Aliquota(Value: WideString);
    procedure Set_ValorIss(Value: WideString);
    procedure Set_ValorLiquidoNfse(Value: WideString);
  end;

{ TXMLTcDadosPrestador }

  TXMLTcDadosPrestador = class(TXMLNode, IXMLTcDadosPrestador)
  protected
    { IXMLTcDadosPrestador }
    function Get_IdentificacaoPrestador: IXMLTcIdentificacaoPrestador;
    function Get_RazaoSocial: WideString;
    function Get_NomeFantasia: WideString;
    function Get_Endereco: IXMLTcEndereco;
    function Get_Contato: IXMLTcContato;
    procedure Set_RazaoSocial(Value: WideString);
    procedure Set_NomeFantasia(Value: WideString);
  public
    procedure AfterConstruction; override;
  end;

{ Global Functions }

// MyRPS
function GetMyRPS(Doc: IXMLDocument): IXMLMyRPS;
function LoadMyRPS(const FileName: WideString): IXMLMyRPS;
function NewMyRPS: IXMLMyRPS;
// Enviar Lote RPS
function GetEnviarLoteRpsEnvio(Doc: IXMLDocument): IXMLEnviarLoteRpsEnvio;
function LoadEnviarLoteRpsEnvio(const FileName: WideString): IXMLEnviarLoteRpsEnvio;
function NewEnviarLoteRpsEnvio: IXMLEnviarLoteRpsEnvio;
// Enviar Lote RPS Sincrono
function GetEnviarLoteRpsSincronoEnvio(Doc: IXMLDocument): IXMLEnviarLoteRpsSincronoEnvio;
function LoadEnviarLoteRpsSincronoEnvio(const FileName: WideString): IXMLEnviarLoteRpsSincronoEnvio;
function NewEnviarLoteRpsSincronoEnvio: IXMLEnviarLoteRpsSincronoEnvio;

const
  TargetNamespace = 'http://www.abrasf.org.br/nfse.xsd';

implementation

{ Global Functions }

// MyRPS

function GetMyRPS(Doc: IXMLDocument): IXMLMyRPS;
begin
  Result := Doc.GetDocBinding('MyRPS', TXMLMyRPS, TargetNamespace) as IXMLMyRPS;
end;

function LoadMyRPS(const FileName: WideString): IXMLMyRPS;
begin
  Result := LoadXMLDocument(FileName).GetDocBinding('MyRPS', TXMLMyRPS, TargetNamespace) as IXMLMyRPS;
end;

function NewMyRPS: IXMLMyRPS;
begin
  Result := NewXMLDocument.GetDocBinding('MyRPS', TXMLMyRPS, TargetNamespace) as IXMLMyRPS;
end;

// Enviar lote RPS

function GetEnviarLoteRpsEnvio(Doc: IXMLDocument): IXMLEnviarLoteRpsEnvio;
begin
  Result := Doc.GetDocBinding('EnviarLoteRpsEnvio', TXMLEnviarLoteRpsEnvio, TargetNamespace) as IXMLEnviarLoteRpsEnvio;
end;

function LoadEnviarLoteRpsEnvio(const FileName: WideString): IXMLEnviarLoteRpsEnvio;
begin
  Result := LoadXMLDocument(FileName).GetDocBinding('EnviarLoteRpsEnvio', TXMLEnviarLoteRpsEnvio, TargetNamespace) as IXMLEnviarLoteRpsEnvio;
end;

function NewEnviarLoteRpsEnvio: IXMLEnviarLoteRpsEnvio;
begin
  Result := NewXMLDocument.GetDocBinding('EnviarLoteRpsEnvio', TXMLEnviarLoteRpsEnvio, TargetNamespace) as IXMLEnviarLoteRpsEnvio;
end;

// Enviar Lote RPS Sincrono

function GetEnviarLoteRpsSincronoEnvio(Doc: IXMLDocument): IXMLEnviarLoteRpsSincronoEnvio;
begin
  Result := Doc.GetDocBinding('EnviarLoteRpsSincronoEnvio', TXMLEnviarLoteRpsSincronoEnvio, TargetNamespace) as IXMLEnviarLoteRpsSincronoEnvio;
end;

function LoadEnviarLoteRpsSincronoEnvio(const FileName: WideString): IXMLEnviarLoteRpsSincronoEnvio;
begin
  Result := LoadXMLDocument(FileName).GetDocBinding('EnviarLoteRpsSincronoEnvio', TXMLEnviarLoteRpsSincronoEnvio, TargetNamespace) as IXMLEnviarLoteRpsSincronoEnvio;
end;

function NewEnviarLoteRpsSincronoEnvio: IXMLEnviarLoteRpsSincronoEnvio;
begin
  Result := NewXMLDocument.GetDocBinding('EnviarLoteRpsSincronoEnvio', TXMLEnviarLoteRpsSincronoEnvio, TargetNamespace) as IXMLEnviarLoteRpsSincronoEnvio;
end;

{ TXMLEnviarLoteRpsEnvio }

procedure TXMLEnviarLoteRpsEnvio.AfterConstruction;
begin
  RegisterChildNode('LoteRps', TXMLTcLoteRps);
  inherited;
end;

function TXMLEnviarLoteRpsEnvio.Get_LoteRps: IXMLTcLoteRps;
begin
  Result := ChildNodes['LoteRps'] as IXMLTcLoteRps;
end;

function TXMLEnviarLoteRpsEnvio.Get_Signature: WideString;
begin
  Result := ChildNodes['dsig:Signature'].Text;
end;

procedure TXMLEnviarLoteRpsEnvio.Set_Signature(Value: WideString);
begin
  ChildNodes['dsig:Signature'].NodeValue := Value;
end;

{ TXMLTcLoteRps }

procedure TXMLTcLoteRps.AfterConstruction;
begin
  RegisterChildNode('CpfCnpj', TXMLTcCpfCnpj);
  RegisterChildNode('ListaRps', TXMLListaRps);
  inherited;
end;

function TXMLTcLoteRps.Get_Id: WideString;
begin
  Result := AttributeNodes['Id'].Text;
end;

procedure TXMLTcLoteRps.Set_Id(Value: WideString);
begin
  SetAttribute('Id', Value);
end;

function TXMLTcLoteRps.Get_Versao: WideString;
begin
  Result := AttributeNodes['versao'].Text;
end;

procedure TXMLTcLoteRps.Set_Versao(Value: WideString);
begin
  SetAttribute('versao', Value);
end;

function TXMLTcLoteRps.Get_NumeroLote: LongWord;
begin
  Result := ChildNodes['NumeroLote'].NodeValue;
end;

procedure TXMLTcLoteRps.Set_NumeroLote(Value: LongWord);
begin
  ChildNodes['NumeroLote'].NodeValue := Value;
end;

function TXMLTcLoteRps.Get_CpfCnpj: IXMLTcCpfCnpj;
begin
  Result := ChildNodes['CpfCnpj'] as IXMLTcCpfCnpj;
end;

function TXMLTcLoteRps.Get_InscricaoMunicipal: WideString;
begin
  Result := ChildNodes['InscricaoMunicipal'].Text;
end;

procedure TXMLTcLoteRps.Set_InscricaoMunicipal(Value: WideString);
begin
  ChildNodes['InscricaoMunicipal'].NodeValue := Value;
end;

function TXMLTcLoteRps.Get_QuantidadeRps: Integer;
begin
  Result := ChildNodes['QuantidadeRps'].NodeValue;
end;

procedure TXMLTcLoteRps.Set_QuantidadeRps(Value: Integer);
begin
  ChildNodes['QuantidadeRps'].NodeValue := Value;
end;

function TXMLTcLoteRps.Get_ListaRps: IXMLListaRps;
begin
  Result := ChildNodes['ListaRps'] as IXMLListaRps;
end;

{ TXMLTcCpfCnpj }

function TXMLTcCpfCnpj.Get_Cpf: WideString;
begin
  Result := ChildNodes['Cpf'].Text;
end;

procedure TXMLTcCpfCnpj.Set_Cpf(Value: WideString);
begin
  ChildNodes['Cpf'].NodeValue := Value;
end;

function TXMLTcCpfCnpj.Get_Cnpj: WideString;
begin
  Result := ChildNodes['Cnpj'].Text;
end;

procedure TXMLTcCpfCnpj.Set_Cnpj(Value: WideString);
begin
  ChildNodes['Cnpj'].NodeValue := Value;
end;

{ TXMLListaRps }

procedure TXMLListaRps.AfterConstruction;
begin
  RegisterChildNode('Rps', TXMLTcDeclaracaoPrestacaoServico);
  ItemTag := 'Rps';
  ItemInterface := IXMLTcDeclaracaoPrestacaoServico;
  inherited;
end;

function TXMLListaRps.Get_Rps(Index: Integer): IXMLTcDeclaracaoPrestacaoServico;
begin
  Result := List[Index] as IXMLTcDeclaracaoPrestacaoServico;
end;

function TXMLListaRps.Add: IXMLTcDeclaracaoPrestacaoServico;
begin
  Result := AddItem(-1) as IXMLTcDeclaracaoPrestacaoServico;
end;

function TXMLListaRps.Insert(const Index: Integer): IXMLTcDeclaracaoPrestacaoServico;
begin
  Result := AddItem(Index) as IXMLTcDeclaracaoPrestacaoServico;
end;

{ TXMLTcDeclaracaoPrestacaoServico }

procedure TXMLTcDeclaracaoPrestacaoServico.AfterConstruction;
begin
  RegisterChildNode('InfDeclaracaoPrestacaoServico', TXMLTcInfDeclaracaoPrestacaoServico);
  inherited;
end;

function TXMLTcDeclaracaoPrestacaoServico.Get_InfDeclaracaoPrestacaoServico: IXMLTcInfDeclaracaoPrestacaoServico;
begin
  Result := ChildNodes['InfDeclaracaoPrestacaoServico'] as IXMLTcInfDeclaracaoPrestacaoServico;
end;

function TXMLTcDeclaracaoPrestacaoServico.Get_Signature: WideString;
begin
  Result := ChildNodes['dsig:Signature'].Text;
end;

procedure TXMLTcDeclaracaoPrestacaoServico.Set_Signature(Value: WideString);
begin
  ChildNodes['dsig:Signature'].NodeValue := Value;
end;

{ TXMLTcInfDeclaracaoPrestacaoServico }

procedure TXMLTcInfDeclaracaoPrestacaoServico.AfterConstruction;
begin
  RegisterChildNode('Rps', TXMLTcInfRps);
  RegisterChildNode('Servico', TXMLTcDadosServico);
  RegisterChildNode('Prestador', TXMLTcIdentificacaoPrestador);
  RegisterChildNode('Tomador', TXMLTcDadosTomador);
  RegisterChildNode('Intermediario', TXMLTcDadosIntermediario);
  RegisterChildNode('ConstrucaoCivil', TXMLTcDadosConstrucaoCivil);
  inherited;
end;

function TXMLTcInfDeclaracaoPrestacaoServico.Get_Id: WideString;
begin
  Result := AttributeNodes['Id'].Text;
end;

procedure TXMLTcInfDeclaracaoPrestacaoServico.Set_Id(Value: WideString);
begin
  SetAttribute('Id', Value);
end;

function TXMLTcInfDeclaracaoPrestacaoServico.Get_Rps: IXMLTcInfRps;
begin
  Result := ChildNodes['Rps'] as IXMLTcInfRps;
end;

function TXMLTcInfDeclaracaoPrestacaoServico.Get_Competencia: WideString;
begin
  Result := ChildNodes['Competencia'].Text;
end;

procedure TXMLTcInfDeclaracaoPrestacaoServico.Set_Competencia(Value: WideString);
begin
  ChildNodes['Competencia'].NodeValue := Value;
end;

function TXMLTcInfDeclaracaoPrestacaoServico.Get_Servico: IXMLTcDadosServico;
begin
  Result := ChildNodes['Servico'] as IXMLTcDadosServico;
end;

function TXMLTcInfDeclaracaoPrestacaoServico.Get_Prestador: IXMLTcIdentificacaoPrestador;
begin
  Result := ChildNodes['Prestador'] as IXMLTcIdentificacaoPrestador;
end;

function TXMLTcInfDeclaracaoPrestacaoServico.Get_Tomador: IXMLTcDadosTomador;
begin
  Result := ChildNodes['Tomador'] as IXMLTcDadosTomador;
end;

function TXMLTcInfDeclaracaoPrestacaoServico.Get_Intermediario: IXMLTcDadosIntermediario;
begin
  Result := ChildNodes['Intermediario'] as IXMLTcDadosIntermediario;
end;

function TXMLTcInfDeclaracaoPrestacaoServico.Get_ConstrucaoCivil: IXMLTcDadosConstrucaoCivil;
begin
  Result := ChildNodes['ConstrucaoCivil'] as IXMLTcDadosConstrucaoCivil;
end;

function TXMLTcInfDeclaracaoPrestacaoServico.Get_RegimeEspecialTributacao: ShortInt;
begin
  Result := ChildNodes['RegimeEspecialTributacao'].NodeValue;
end;

procedure TXMLTcInfDeclaracaoPrestacaoServico.Set_RegimeEspecialTributacao(Value: ShortInt);
begin
  ChildNodes['RegimeEspecialTributacao'].NodeValue := Value;
end;

function TXMLTcInfDeclaracaoPrestacaoServico.Get_OptanteSimplesNacional: ShortInt;
begin
  Result := ChildNodes['OptanteSimplesNacional'].NodeValue;
end;

procedure TXMLTcInfDeclaracaoPrestacaoServico.Set_OptanteSimplesNacional(Value: ShortInt);
begin
  ChildNodes['OptanteSimplesNacional'].NodeValue := Value;
end;

function TXMLTcInfDeclaracaoPrestacaoServico.Get_IncentivoFiscal: ShortInt;
begin
  Result := ChildNodes['IncentivoFiscal'].NodeValue;
end;

procedure TXMLTcInfDeclaracaoPrestacaoServico.Set_IncentivoFiscal(Value: ShortInt);
begin
  ChildNodes['IncentivoFiscal'].NodeValue := Value;
end;

{ TXMLTcInfRps }

procedure TXMLTcInfRps.AfterConstruction;
begin
  RegisterChildNode('IdentificacaoRps', TXMLTcIdentificacaoRps);
  RegisterChildNode('RpsSubstituido', TXMLTcIdentificacaoRps);
  inherited;
end;

function TXMLTcInfRps.Get_Id: WideString;
begin
  Result := AttributeNodes['Id'].Text;
end;

procedure TXMLTcInfRps.Set_Id(Value: WideString);
begin
  SetAttribute('Id', Value);
end;

function TXMLTcInfRps.Get_IdentificacaoRps: IXMLTcIdentificacaoRps;
begin
  Result := ChildNodes['IdentificacaoRps'] as IXMLTcIdentificacaoRps;
end;

function TXMLTcInfRps.Get_DataEmissao: WideString;
begin
  Result := ChildNodes['DataEmissao'].Text;
end;

procedure TXMLTcInfRps.Set_DataEmissao(Value: WideString);
begin
  ChildNodes['DataEmissao'].NodeValue := Value;
end;

function TXMLTcInfRps.Get_Status: ShortInt;
begin
  Result := ChildNodes['Status'].NodeValue;
end;

procedure TXMLTcInfRps.Set_Status(Value: ShortInt);
begin
  ChildNodes['Status'].NodeValue := Value;
end;

function TXMLTcInfRps.Get_RpsSubstituido: IXMLTcIdentificacaoRps;
begin
  Result := ChildNodes['RpsSubstituido'] as IXMLTcIdentificacaoRps;
end;

{ TXMLTcIdentificacaoRps }

function TXMLTcIdentificacaoRps.Get_Numero: LongWord;
begin
  Result := ChildNodes['Numero'].NodeValue;
end;

procedure TXMLTcIdentificacaoRps.Set_Numero(Value: LongWord);
begin
  ChildNodes['Numero'].NodeValue := Value;
end;

function TXMLTcIdentificacaoRps.Get_Serie: WideString;
begin
  Result := ChildNodes['Serie'].Text;
end;

procedure TXMLTcIdentificacaoRps.Set_Serie(Value: WideString);
begin
  ChildNodes['Serie'].NodeValue := Value;
end;

function TXMLTcIdentificacaoRps.Get_Tipo: ShortInt;
begin
  Result := ChildNodes['Tipo'].NodeValue;
end;

procedure TXMLTcIdentificacaoRps.Set_Tipo(Value: ShortInt);
begin
  ChildNodes['Tipo'].NodeValue := Value;
end;

{ TXMLTcDadosServico }

procedure TXMLTcDadosServico.AfterConstruction;
begin
  RegisterChildNode('Valores', TXMLTcValoresDeclaracaoServico);
  inherited;
end;

function TXMLTcDadosServico.Get_Valores: IXMLTcValoresDeclaracaoServico;
begin
  Result := ChildNodes['Valores'] as IXMLTcValoresDeclaracaoServico;
end;

function TXMLTcDadosServico.Get_IssRetido: ShortInt;
begin
  Result := ChildNodes['IssRetido'].NodeValue;
end;

procedure TXMLTcDadosServico.Set_IssRetido(Value: ShortInt);
begin
  ChildNodes['IssRetido'].NodeValue := Value;
end;

function TXMLTcDadosServico.Get_ResponsavelRetencao: ShortInt;
begin
  Result := ChildNodes['ResponsavelRetencao'].NodeValue;
end;

procedure TXMLTcDadosServico.Set_ResponsavelRetencao(Value: ShortInt);
begin
  ChildNodes['ResponsavelRetencao'].NodeValue := Value;
end;

function TXMLTcDadosServico.Get_ItemListaServico: WideString;
begin
  Result := ChildNodes['ItemListaServico'].Text;
end;

procedure TXMLTcDadosServico.Set_ItemListaServico(Value: WideString);
begin
  ChildNodes['ItemListaServico'].NodeValue := Value;
end;

function TXMLTcDadosServico.Get_CodigoCnae: Integer;
begin
  Result := ChildNodes['CodigoCnae'].NodeValue;
end;

procedure TXMLTcDadosServico.Set_CodigoCnae(Value: Integer);
begin
  ChildNodes['CodigoCnae'].NodeValue := Value;
end;

function TXMLTcDadosServico.Get_CodigoTributacaoMunicipio: WideString;
begin
  Result := ChildNodes['CodigoTributacaoMunicipio'].Text;
end;

procedure TXMLTcDadosServico.Set_CodigoTributacaoMunicipio(Value: WideString);
begin
  ChildNodes['CodigoTributacaoMunicipio'].NodeValue := Value;
end;

function TXMLTcDadosServico.Get_Discriminacao: WideString;
begin
  Result := ChildNodes['Discriminacao'].Text;
end;

procedure TXMLTcDadosServico.Set_Discriminacao(Value: WideString);
begin
  ChildNodes['Discriminacao'].NodeValue := Value;
end;

function TXMLTcDadosServico.Get_CodigoMunicipio: Integer;
begin
  Result := ChildNodes['CodigoMunicipio'].NodeValue;
end;

procedure TXMLTcDadosServico.Set_CodigoMunicipio(Value: Integer);
begin
  ChildNodes['CodigoMunicipio'].NodeValue := Value;
end;

function TXMLTcDadosServico.Get_CodigoPais: WideString;
begin
  Result := ChildNodes['CodigoPais'].Text;
end;

procedure TXMLTcDadosServico.Set_CodigoPais(Value: WideString);
begin
  ChildNodes['CodigoPais'].NodeValue := Value;
end;

function TXMLTcDadosServico.Get_ExigibilidadeISS: ShortInt;
begin
  Result := ChildNodes['ExigibilidadeISS'].NodeValue;
end;

procedure TXMLTcDadosServico.Set_ExigibilidadeISS(Value: ShortInt);
begin
  ChildNodes['ExigibilidadeISS'].NodeValue := Value;
end;

function TXMLTcDadosServico.Get_MunicipioIncidencia: Integer;
begin
  Result := ChildNodes['MunicipioIncidencia'].NodeValue;
end;

procedure TXMLTcDadosServico.Set_MunicipioIncidencia(Value: Integer);
begin
  ChildNodes['MunicipioIncidencia'].NodeValue := Value;
end;

function TXMLTcDadosServico.Get_NumeroProcesso: WideString;
begin
  Result := ChildNodes['NumeroProcesso'].Text;
end;

procedure TXMLTcDadosServico.Set_NumeroProcesso(Value: WideString);
begin
  ChildNodes['NumeroProcesso'].NodeValue := Value;
end;

{ TXMLTcValoresDeclaracaoServico }

function TXMLTcValoresDeclaracaoServico.Get_ValorServicos: WideString;
begin
  Result := ChildNodes['ValorServicos'].Text;
end;

procedure TXMLTcValoresDeclaracaoServico.Set_ValorServicos(Value: WideString);
begin
  ChildNodes['ValorServicos'].NodeValue := Value;
end;

function TXMLTcValoresDeclaracaoServico.Get_ValorDeducoes: WideString;
begin
  Result := ChildNodes['ValorDeducoes'].Text;
end;

procedure TXMLTcValoresDeclaracaoServico.Set_ValorDeducoes(Value: WideString);
begin
  ChildNodes['ValorDeducoes'].NodeValue := Value;
end;

function TXMLTcValoresDeclaracaoServico.Get_ValorPis: WideString;
begin
  Result := ChildNodes['ValorPis'].Text;
end;

procedure TXMLTcValoresDeclaracaoServico.Set_ValorPis(Value: WideString);
begin
  ChildNodes['ValorPis'].NodeValue := Value;
end;

function TXMLTcValoresDeclaracaoServico.Get_ValorCofins: WideString;
begin
  Result := ChildNodes['ValorCofins'].Text;
end;

procedure TXMLTcValoresDeclaracaoServico.Set_ValorCofins(Value: WideString);
begin
  ChildNodes['ValorCofins'].NodeValue := Value;
end;

function TXMLTcValoresDeclaracaoServico.Get_ValorInss: WideString;
begin
  Result := ChildNodes['ValorInss'].Text;
end;

procedure TXMLTcValoresDeclaracaoServico.Set_ValorInss(Value: WideString);
begin
  ChildNodes['ValorInss'].NodeValue := Value;
end;

function TXMLTcValoresDeclaracaoServico.Get_ValorIr: WideString;
begin
  Result := ChildNodes['ValorIr'].Text;
end;

procedure TXMLTcValoresDeclaracaoServico.Set_ValorIr(Value: WideString);
begin
  ChildNodes['ValorIr'].NodeValue := Value;
end;

function TXMLTcValoresDeclaracaoServico.Get_ValorCsll: WideString;
begin
  Result := ChildNodes['ValorCsll'].Text;
end;

procedure TXMLTcValoresDeclaracaoServico.Set_ValorCsll(Value: WideString);
begin
  ChildNodes['ValorCsll'].NodeValue := Value;
end;

function TXMLTcValoresDeclaracaoServico.Get_OutrasRetencoes: WideString;
begin
  Result := ChildNodes['OutrasRetencoes'].Text;
end;

procedure TXMLTcValoresDeclaracaoServico.Set_OutrasRetencoes(Value: WideString);
begin
  ChildNodes['OutrasRetencoes'].NodeValue := Value;
end;

function TXMLTcValoresDeclaracaoServico.Get_ValorIss: WideString;
begin
  Result := ChildNodes['ValorIss'].Text;
end;

procedure TXMLTcValoresDeclaracaoServico.Set_ValorIss(Value: WideString);
begin
  ChildNodes['ValorIss'].NodeValue := Value;
end;

function TXMLTcValoresDeclaracaoServico.Get_Aliquota: WideString;
begin
  Result := ChildNodes['Aliquota'].Text;
end;

procedure TXMLTcValoresDeclaracaoServico.Set_Aliquota(Value: WideString);
begin
  ChildNodes['Aliquota'].NodeValue := Value;
end;

function TXMLTcValoresDeclaracaoServico.Get_DescontoIncondicionado: WideString;
begin
  Result := ChildNodes['DescontoIncondicionado'].Text;
end;

procedure TXMLTcValoresDeclaracaoServico.Set_DescontoIncondicionado(Value: WideString);
begin
  ChildNodes['DescontoIncondicionado'].NodeValue := Value;
end;

function TXMLTcValoresDeclaracaoServico.Get_DescontoCondicionado: WideString;
begin
  Result := ChildNodes['DescontoCondicionado'].Text;
end;

procedure TXMLTcValoresDeclaracaoServico.Set_DescontoCondicionado(Value: WideString);
begin
  ChildNodes['DescontoCondicionado'].NodeValue := Value;
end;

{ TXMLTcIdentificacaoPrestador }

procedure TXMLTcIdentificacaoPrestador.AfterConstruction;
begin
  RegisterChildNode('CpfCnpj', TXMLTcCpfCnpj);
  inherited;
end;

function TXMLTcIdentificacaoPrestador.Get_CpfCnpj: IXMLTcCpfCnpj;
begin
  Result := ChildNodes['CpfCnpj'] as IXMLTcCpfCnpj;
end;

function TXMLTcIdentificacaoPrestador.Get_InscricaoMunicipal: WideString;
begin
  Result := ChildNodes['InscricaoMunicipal'].Text;
end;

procedure TXMLTcIdentificacaoPrestador.Set_InscricaoMunicipal(Value: WideString);
begin
  ChildNodes['InscricaoMunicipal'].NodeValue := Value;
end;

{ TXMLTcDadosTomador }

procedure TXMLTcDadosTomador.AfterConstruction;
begin
  RegisterChildNode('IdentificacaoTomador', TXMLTcIdentificacaoTomador);
  RegisterChildNode('Endereco', TXMLTcEndereco);
  RegisterChildNode('Contato', TXMLTcContato);
  inherited;
end;

function TXMLTcDadosTomador.Get_IdentificacaoTomador: IXMLTcIdentificacaoTomador;
begin
  Result := ChildNodes['IdentificacaoTomador'] as IXMLTcIdentificacaoTomador;
end;

function TXMLTcDadosTomador.Get_RazaoSocial: WideString;
begin
  Result := ChildNodes['RazaoSocial'].Text;
end;

procedure TXMLTcDadosTomador.Set_RazaoSocial(Value: WideString);
begin
  ChildNodes['RazaoSocial'].NodeValue := Value;
end;

function TXMLTcDadosTomador.Get_Endereco: IXMLTcEndereco;
begin
  Result := ChildNodes['Endereco'] as IXMLTcEndereco;
end;

function TXMLTcDadosTomador.Get_Contato: IXMLTcContato;
begin
  Result := ChildNodes['Contato'] as IXMLTcContato;
end;

{ TXMLTcIdentificacaoTomador }

procedure TXMLTcIdentificacaoTomador.AfterConstruction;
begin
  RegisterChildNode('CpfCnpj', TXMLTcCpfCnpj);
  inherited;
end;

function TXMLTcIdentificacaoTomador.Get_CpfCnpj: IXMLTcCpfCnpj;
begin
  Result := ChildNodes['CpfCnpj'] as IXMLTcCpfCnpj;
end;

function TXMLTcIdentificacaoTomador.Get_InscricaoMunicipal: WideString;
begin
  Result := ChildNodes['InscricaoMunicipal'].Text;
end;

procedure TXMLTcIdentificacaoTomador.Set_InscricaoMunicipal(Value: WideString);
begin
  ChildNodes['InscricaoMunicipal'].NodeValue := Value;
end;

{ TXMLTcEndereco }

function TXMLTcEndereco.Get_Endereco: WideString;
begin
  Result := ChildNodes['Endereco'].Text;
end;

procedure TXMLTcEndereco.Set_Endereco(Value: WideString);
begin
  ChildNodes['Endereco'].NodeValue := Value;
end;

function TXMLTcEndereco.Get_Numero: WideString;
begin
  Result := ChildNodes['Numero'].Text;
end;

procedure TXMLTcEndereco.Set_Numero(Value: WideString);
begin
  ChildNodes['Numero'].NodeValue := Value;
end;

function TXMLTcEndereco.Get_Complemento: WideString;
begin
  Result := ChildNodes['Complemento'].Text;
end;

procedure TXMLTcEndereco.Set_Complemento(Value: WideString);
begin
  ChildNodes['Complemento'].NodeValue := Value;
end;

function TXMLTcEndereco.Get_Bairro: WideString;
begin
  Result := ChildNodes['Bairro'].Text;
end;

procedure TXMLTcEndereco.Set_Bairro(Value: WideString);
begin
  ChildNodes['Bairro'].NodeValue := Value;
end;

function TXMLTcEndereco.Get_CodigoMunicipio: Integer;
begin
  Result := ChildNodes['CodigoMunicipio'].NodeValue;
end;

procedure TXMLTcEndereco.Set_CodigoMunicipio(Value: Integer);
begin
  ChildNodes['CodigoMunicipio'].NodeValue := Value;
end;

function TXMLTcEndereco.Get_Uf: WideString;
begin
  Result := ChildNodes['Uf'].Text;
end;

procedure TXMLTcEndereco.Set_Uf(Value: WideString);
begin
  ChildNodes['Uf'].NodeValue := Value;
end;

function TXMLTcEndereco.Get_CodigoPais: WideString;
begin
  Result := ChildNodes['CodigoPais'].Text;
end;

procedure TXMLTcEndereco.Set_CodigoPais(Value: WideString);
begin
  ChildNodes['CodigoPais'].NodeValue := Value;
end;

function TXMLTcEndereco.Get_Cep: WideString;
begin
  Result := ChildNodes['Cep'].Text;
end;

procedure TXMLTcEndereco.Set_Cep(Value: WideString);
begin
  ChildNodes['Cep'].NodeValue := Value;
end;

{ TXMLTcContato }

function TXMLTcContato.Get_Telefone: WideString;
begin
  Result := ChildNodes['Telefone'].Text;
end;

procedure TXMLTcContato.Set_Telefone(Value: WideString);
begin
  ChildNodes['Telefone'].NodeValue := Value;
end;

function TXMLTcContato.Get_Email: WideString;
begin
  Result := ChildNodes['Email'].Text;
end;

procedure TXMLTcContato.Set_Email(Value: WideString);
begin
  ChildNodes['Email'].NodeValue := Value;
end;

{ TXMLTcDadosIntermediario }

procedure TXMLTcDadosIntermediario.AfterConstruction;
begin
  RegisterChildNode('IdentificacaoIntermediario', TXMLTcIdentificacaoIntermediario);
  inherited;
end;

function TXMLTcDadosIntermediario.Get_IdentificacaoIntermediario: IXMLTcIdentificacaoIntermediario;
begin
  Result := ChildNodes['IdentificacaoIntermediario'] as IXMLTcIdentificacaoIntermediario;
end;

function TXMLTcDadosIntermediario.Get_RazaoSocial: WideString;
begin
  Result := ChildNodes['RazaoSocial'].Text;
end;

procedure TXMLTcDadosIntermediario.Set_RazaoSocial(Value: WideString);
begin
  ChildNodes['RazaoSocial'].NodeValue := Value;
end;

{ TXMLTcIdentificacaoIntermediario }

procedure TXMLTcIdentificacaoIntermediario.AfterConstruction;
begin
  RegisterChildNode('CpfCnpj', TXMLTcCpfCnpj);
  inherited;
end;

function TXMLTcIdentificacaoIntermediario.Get_CpfCnpj: IXMLTcCpfCnpj;
begin
  Result := ChildNodes['CpfCnpj'] as IXMLTcCpfCnpj;
end;

function TXMLTcIdentificacaoIntermediario.Get_InscricaoMunicipal: WideString;
begin
  Result := ChildNodes['InscricaoMunicipal'].Text;
end;

procedure TXMLTcIdentificacaoIntermediario.Set_InscricaoMunicipal(Value: WideString);
begin
  ChildNodes['InscricaoMunicipal'].NodeValue := Value;
end;

{ TXMLTcDadosConstrucaoCivil }

function TXMLTcDadosConstrucaoCivil.Get_CodigoObra: WideString;
begin
  Result := ChildNodes['CodigoObra'].Text;
end;

procedure TXMLTcDadosConstrucaoCivil.Set_CodigoObra(Value: WideString);
begin
  ChildNodes['CodigoObra'].NodeValue := Value;
end;

function TXMLTcDadosConstrucaoCivil.Get_Art: WideString;
begin
  Result := ChildNodes['Art'].Text;
end;

procedure TXMLTcDadosConstrucaoCivil.Set_Art(Value: WideString);
begin
  ChildNodes['Art'].NodeValue := Value;
end;

{ TXMLEnviarLoteRpsSincronoEnvio }

procedure TXMLEnviarLoteRpsSincronoEnvio.AfterConstruction;
begin
  RegisterChildNode('LoteRps', TXMLTcLoteRps);
  inherited;
end;

function TXMLEnviarLoteRpsSincronoEnvio.Get_LoteRps: IXMLTcLoteRps;
begin
  Result := ChildNodes['LoteRps'] as IXMLTcLoteRps;
end;

function TXMLEnviarLoteRpsSincronoEnvio.Get_Signature: WideString;
begin
  Result := ChildNodes['dsig:Signature'].Text;
end;

procedure TXMLEnviarLoteRpsSincronoEnvio.Set_Signature(Value: WideString);
begin
  ChildNodes['dsig:Signature'].NodeValue := Value;
end;

{ TXMLMyRPS }

procedure TXMLMyRPS.AfterConstruction;
begin
  RegisterChildNode('Rps', TXMLTcDeclaracaoPrestacaoServico);
  inherited;
end;

function TXMLMyRPS.Get_Rps: IXMLTcDeclaracaoPrestacaoServico;
begin
  Result := ChildNodes['Rps'] as IXMLTcDeclaracaoPrestacaoServico;
end;

{ TXMLTcIdentificacaoOrgaoGerador }

function TXMLTcIdentificacaoOrgaoGerador.Get_CodigoMunicipio: Integer;
begin
  Result := ChildNodes['CodigoMunicipio'].NodeValue;
end;

procedure TXMLTcIdentificacaoOrgaoGerador.Set_CodigoMunicipio(Value: Integer);
begin
  ChildNodes['CodigoMunicipio'].NodeValue := Value;
end;

function TXMLTcIdentificacaoOrgaoGerador.Get_Uf: WideString;
begin
  Result := ChildNodes['Uf'].Text;
end;

procedure TXMLTcIdentificacaoOrgaoGerador.Set_Uf(Value: WideString);
begin
  ChildNodes['Uf'].NodeValue := Value;
end;

{ TXMLTcIdentificacaoConsulente }

procedure TXMLTcIdentificacaoConsulente.AfterConstruction;
begin
  RegisterChildNode('CpfCnpj', TXMLTcCpfCnpj);
  inherited;
end;

function TXMLTcIdentificacaoConsulente.Get_CpfCnpj: IXMLTcCpfCnpj;
begin
  Result := ChildNodes['CpfCnpj'] as IXMLTcCpfCnpj;
end;

function TXMLTcIdentificacaoConsulente.Get_InscricaoMunicipal: WideString;
begin
  Result := ChildNodes['InscricaoMunicipal'].Text;
end;

procedure TXMLTcIdentificacaoConsulente.Set_InscricaoMunicipal(Value: WideString);
begin
  ChildNodes['InscricaoMunicipal'].NodeValue := Value;
end;

{ TXMLTcValoresNfse }

function TXMLTcValoresNfse.Get_BaseCalculo: WideString;
begin
  Result := ChildNodes['BaseCalculo'].Text;
end;

procedure TXMLTcValoresNfse.Set_BaseCalculo(Value: WideString);
begin
  ChildNodes['BaseCalculo'].NodeValue := Value;
end;

function TXMLTcValoresNfse.Get_Aliquota: WideString;
begin
  Result := ChildNodes['Aliquota'].Text;
end;

procedure TXMLTcValoresNfse.Set_Aliquota(Value: WideString);
begin
  ChildNodes['Aliquota'].NodeValue := Value;
end;

function TXMLTcValoresNfse.Get_ValorIss: WideString;
begin
  Result := ChildNodes['ValorIss'].Text;
end;

procedure TXMLTcValoresNfse.Set_ValorIss(Value: WideString);
begin
  ChildNodes['ValorIss'].NodeValue := Value;
end;

function TXMLTcValoresNfse.Get_ValorLiquidoNfse: WideString;
begin
  Result := ChildNodes['ValorLiquidoNfse'].Text;
end;

procedure TXMLTcValoresNfse.Set_ValorLiquidoNfse(Value: WideString);
begin
  ChildNodes['ValorLiquidoNfse'].NodeValue := Value;
end;

{ TXMLTcDadosPrestador }

procedure TXMLTcDadosPrestador.AfterConstruction;
begin
  RegisterChildNode('IdentificacaoPrestador', TXMLTcIdentificacaoPrestador);
  RegisterChildNode('Endereco', TXMLTcEndereco);
  RegisterChildNode('Contato', TXMLTcContato);
  inherited;
end;

function TXMLTcDadosPrestador.Get_IdentificacaoPrestador: IXMLTcIdentificacaoPrestador;
begin
  Result := ChildNodes['IdentificacaoPrestador'] as IXMLTcIdentificacaoPrestador;
end;

function TXMLTcDadosPrestador.Get_RazaoSocial: WideString;
begin
  Result := ChildNodes['RazaoSocial'].Text;
end;

procedure TXMLTcDadosPrestador.Set_RazaoSocial(Value: WideString);
begin
  ChildNodes['RazaoSocial'].NodeValue := Value;
end;

function TXMLTcDadosPrestador.Get_NomeFantasia: WideString;
begin
  Result := ChildNodes['NomeFantasia'].Text;
end;

procedure TXMLTcDadosPrestador.Set_NomeFantasia(Value: WideString);
begin
  ChildNodes['NomeFantasia'].NodeValue := Value;
end;

function TXMLTcDadosPrestador.Get_Endereco: IXMLTcEndereco;
begin
  Result := ChildNodes['Endereco'] as IXMLTcEndereco;
end;

function TXMLTcDadosPrestador.Get_Contato: IXMLTcContato;
begin
  Result := ChildNodes['Contato'] as IXMLTcContato;
end;

end.