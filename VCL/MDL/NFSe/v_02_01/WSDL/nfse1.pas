// ************************************************************************ //
// The types declared in this file were generated from data read from the
// WSDL File described below:
// WSDL     : C:\Projetos_Aux\_NFSE\nfse.wsdl
//  >Import : C:\Projetos_Aux\_NFSE\nfse.wsdl:0
// Encoding : UTF-8
// Version  : 1.0
// (25/09/2012 11:50:12 - - $Rev: 7300 $)
// ************************************************************************ //

unit nfse1;

interface

uses InvokeRegistry, SOAPHTTPClient, Types, XSBuiltIns;

const
  IS_UNQL = $0008;


type

  // ************************************************************************ //
  // The following types, referred to in the WSDL document are not being represented
  // in this file. They are either aliases[@] of other types represented or were referred
  // to but never[!] declared in the document. The types from the latter category
  // typically map to predefined/known XML or Borland types; however, they could also 
  // indicate incorrect WSDL documents that failed to declare or import a schema type.
  // ************************************************************************ //
  // !:string          - "http://www.w3.org/2001/XMLSchema"[Gbl]

  input                = class;                 { "http://nfse.abrasf.org.br"[Lit][GblCplx] }
  output               = class;                 { "http://nfse.abrasf.org.br"[Lit][GblCplx] }
  CancelarNfseRequest  = class;                 { "http://nfse.abrasf.org.br"[Lit][GblElm] }
  CancelarNfseResponse = class;                 { "http://nfse.abrasf.org.br"[Lit][GblElm] }
  ConsultarLoteRpsRequest = class;              { "http://nfse.abrasf.org.br"[Lit][GblElm] }
  ConsultarLoteRpsResponse = class;             { "http://nfse.abrasf.org.br"[Lit][GblElm] }
  ConsultarNfseServicoPrestadoRequest = class;   { "http://nfse.abrasf.org.br"[Lit][GblElm] }
  ConsultarNfseServicoPrestadoResponse = class;   { "http://nfse.abrasf.org.br"[Lit][GblElm] }
  ConsultarNfseServicoTomadoRequest = class;    { "http://nfse.abrasf.org.br"[Lit][GblElm] }
  ConsultarNfseServicoTomadoResponse = class;   { "http://nfse.abrasf.org.br"[Lit][GblElm] }
  ConsultarNfsePorFaixaRequest = class;         { "http://nfse.abrasf.org.br"[Lit][GblElm] }
  ConsultarNfsePorFaixaResponse = class;        { "http://nfse.abrasf.org.br"[Lit][GblElm] }
  ConsultarNfsePorRpsRequest = class;           { "http://nfse.abrasf.org.br"[Lit][GblElm] }
  ConsultarNfsePorRpsResponse = class;          { "http://nfse.abrasf.org.br"[Lit][GblElm] }
  RecepcionarLoteRpsRequest = class;            { "http://nfse.abrasf.org.br"[Lit][GblElm] }
  RecepcionarLoteRpsResponse = class;           { "http://nfse.abrasf.org.br"[Lit][GblElm] }
  GerarNfseRequest     = class;                 { "http://nfse.abrasf.org.br"[Lit][GblElm] }
  GerarNfseResponse    = class;                 { "http://nfse.abrasf.org.br"[Lit][GblElm] }
  SubstituirNfseRequest = class;                { "http://nfse.abrasf.org.br"[Lit][GblElm] }
  SubstituirNfseResponse = class;               { "http://nfse.abrasf.org.br"[Lit][GblElm] }
  RecepcionarLoteRpsSincronoRequest = class;    { "http://nfse.abrasf.org.br"[Lit][GblElm] }
  RecepcionarLoteRpsSincronoResponse = class;   { "http://nfse.abrasf.org.br"[Lit][GblElm] }



  // ************************************************************************ //
  // XML       : input, global, <complexType>
  // Namespace : http://nfse.abrasf.org.br
  // Serializtn: [xoLiteralParam]
  // Info      : Wrapper
  // ************************************************************************ //
  input = class(TRemotable)
  private
    FnfseCabecMsg: WideString;
    FnfseDadosMsg: WideString;
  public
    constructor Create; override;
  published
    property nfseCabecMsg: WideString  Index (IS_UNQL) read FnfseCabecMsg write FnfseCabecMsg;
    property nfseDadosMsg: WideString  Index (IS_UNQL) read FnfseDadosMsg write FnfseDadosMsg;
  end;



  // ************************************************************************ //
  // XML       : output, global, <complexType>
  // Namespace : http://nfse.abrasf.org.br
  // Serializtn: [xoLiteralParam]
  // Info      : Wrapper
  // ************************************************************************ //
  output = class(TRemotable)
  private
    FoutputXML: WideString;
  public
    constructor Create; override;
  published
    property outputXML: WideString  Index (IS_UNQL) read FoutputXML write FoutputXML;
  end;



  // ************************************************************************ //
  // XML       : CancelarNfseRequest, global, <element>
  // Namespace : http://nfse.abrasf.org.br
  // Info      : Wrapper
  // ************************************************************************ //
  CancelarNfseRequest = class(input)
  private
  published
  end;



  // ************************************************************************ //
  // XML       : CancelarNfseResponse, global, <element>
  // Namespace : http://nfse.abrasf.org.br
  // Info      : Wrapper
  // ************************************************************************ //
  CancelarNfseResponse = class(output)
  private
  published
  end;



  // ************************************************************************ //
  // XML       : ConsultarLoteRpsRequest, global, <element>
  // Namespace : http://nfse.abrasf.org.br
  // Info      : Wrapper
  // ************************************************************************ //
  ConsultarLoteRpsRequest = class(input)
  private
  published
  end;



  // ************************************************************************ //
  // XML       : ConsultarLoteRpsResponse, global, <element>
  // Namespace : http://nfse.abrasf.org.br
  // Info      : Wrapper
  // ************************************************************************ //
  ConsultarLoteRpsResponse = class(output)
  private
  published
  end;



  // ************************************************************************ //
  // XML       : ConsultarNfseServicoPrestadoRequest, global, <element>
  // Namespace : http://nfse.abrasf.org.br
  // Info      : Wrapper
  // ************************************************************************ //
  ConsultarNfseServicoPrestadoRequest = class(input)
  private
  published
  end;



  // ************************************************************************ //
  // XML       : ConsultarNfseServicoPrestadoResponse, global, <element>
  // Namespace : http://nfse.abrasf.org.br
  // Info      : Wrapper
  // ************************************************************************ //
  ConsultarNfseServicoPrestadoResponse = class(output)
  private
  published
  end;



  // ************************************************************************ //
  // XML       : ConsultarNfseServicoTomadoRequest, global, <element>
  // Namespace : http://nfse.abrasf.org.br
  // Info      : Wrapper
  // ************************************************************************ //
  ConsultarNfseServicoTomadoRequest = class(input)
  private
  published
  end;



  // ************************************************************************ //
  // XML       : ConsultarNfseServicoTomadoResponse, global, <element>
  // Namespace : http://nfse.abrasf.org.br
  // Info      : Wrapper
  // ************************************************************************ //
  ConsultarNfseServicoTomadoResponse = class(output)
  private
  published
  end;



  // ************************************************************************ //
  // XML       : ConsultarNfsePorFaixaRequest, global, <element>
  // Namespace : http://nfse.abrasf.org.br
  // Info      : Wrapper
  // ************************************************************************ //
  ConsultarNfsePorFaixaRequest = class(input)
  private
  published
  end;



  // ************************************************************************ //
  // XML       : ConsultarNfsePorFaixaResponse, global, <element>
  // Namespace : http://nfse.abrasf.org.br
  // Info      : Wrapper
  // ************************************************************************ //
  ConsultarNfsePorFaixaResponse = class(output)
  private
  published
  end;



  // ************************************************************************ //
  // XML       : ConsultarNfsePorRpsRequest, global, <element>
  // Namespace : http://nfse.abrasf.org.br
  // Info      : Wrapper
  // ************************************************************************ //
  ConsultarNfsePorRpsRequest = class(input)
  private
  published
  end;



  // ************************************************************************ //
  // XML       : ConsultarNfsePorRpsResponse, global, <element>
  // Namespace : http://nfse.abrasf.org.br
  // Info      : Wrapper
  // ************************************************************************ //
  ConsultarNfsePorRpsResponse = class(output)
  private
  published
  end;



  // ************************************************************************ //
  // XML       : RecepcionarLoteRpsRequest, global, <element>
  // Namespace : http://nfse.abrasf.org.br
  // Info      : Wrapper
  // ************************************************************************ //
  RecepcionarLoteRpsRequest = class(input)
  private
  published
  end;



  // ************************************************************************ //
  // XML       : RecepcionarLoteRpsResponse, global, <element>
  // Namespace : http://nfse.abrasf.org.br
  // Info      : Wrapper
  // ************************************************************************ //
  RecepcionarLoteRpsResponse = class(output)
  private
  published
  end;



  // ************************************************************************ //
  // XML       : GerarNfseRequest, global, <element>
  // Namespace : http://nfse.abrasf.org.br
  // Info      : Wrapper
  // ************************************************************************ //
  GerarNfseRequest = class(input)
  private
  published
  end;



  // ************************************************************************ //
  // XML       : GerarNfseResponse, global, <element>
  // Namespace : http://nfse.abrasf.org.br
  // Info      : Wrapper
  // ************************************************************************ //
  GerarNfseResponse = class(output)
  private
  published
  end;



  // ************************************************************************ //
  // XML       : SubstituirNfseRequest, global, <element>
  // Namespace : http://nfse.abrasf.org.br
  // Info      : Wrapper
  // ************************************************************************ //
  SubstituirNfseRequest = class(input)
  private
  published
  end;



  // ************************************************************************ //
  // XML       : SubstituirNfseResponse, global, <element>
  // Namespace : http://nfse.abrasf.org.br
  // Info      : Wrapper
  // ************************************************************************ //
  SubstituirNfseResponse = class(output)
  private
  published
  end;



  // ************************************************************************ //
  // XML       : RecepcionarLoteRpsSincronoRequest, global, <element>
  // Namespace : http://nfse.abrasf.org.br
  // Info      : Wrapper
  // ************************************************************************ //
  RecepcionarLoteRpsSincronoRequest = class(input)
  private
  published
  end;



  // ************************************************************************ //
  // XML       : RecepcionarLoteRpsSincronoResponse, global, <element>
  // Namespace : http://nfse.abrasf.org.br
  // Info      : Wrapper
  // ************************************************************************ //
  RecepcionarLoteRpsSincronoResponse = class(output)
  private
  published
  end;


  // ************************************************************************ //
  // Namespace : http://nfse.abrasf.org.br
  // soapAction: http://nfse.abrasf.org.br/%operationName%
  // transport : http://schemas.xmlsoap.org/soap/http
  // style     : document
  // binding   : nfseSOAP
  // service   : NfseWSService
  // port      : nfseSOAP
  // URL       : http://ws.pbh.gov.br
  // ************************************************************************ //
  nfse = interface(IInvokable)
  ['{E1323A52-80BC-D6B7-7378-415C805ECD3F}']

    // Cannot unwrap: 
    //     - Input element wrapper name does not match operation's name
    function  CancelarNfse(const parameters: CancelarNfseRequest): CancelarNfseResponse; stdcall;

    // Cannot unwrap: 
    //     - Input element wrapper name does not match operation's name
    function  ConsultarLoteRps(const parameters: ConsultarLoteRpsRequest): ConsultarLoteRpsResponse; stdcall;

    // Cannot unwrap: 
    //     - Input element wrapper name does not match operation's name
    function  ConsultarNfseServicoPrestado(const parameters: ConsultarNfseServicoPrestadoRequest): ConsultarNfseServicoPrestadoResponse; stdcall;

    // Cannot unwrap: 
    //     - Input element wrapper name does not match operation's name
    function  ConsultarNfseServicoTomado(const parameters: ConsultarNfseServicoTomadoRequest): ConsultarNfseServicoTomadoResponse; stdcall;

    // Cannot unwrap: 
    //     - Input element wrapper name does not match operation's name
    function  ConsultarNfsePorFaixa(const parameters: ConsultarNfsePorFaixaRequest): ConsultarNfsePorFaixaResponse; stdcall;

    // Cannot unwrap: 
    //     - Input element wrapper name does not match operation's name
    function  ConsultarNfsePorRps(const parameters: ConsultarNfsePorRpsRequest): ConsultarNfsePorRpsResponse; stdcall;

    // Cannot unwrap: 
    //     - Input element wrapper name does not match operation's name
    function  RecepcionarLoteRps(const parameters: RecepcionarLoteRpsRequest): RecepcionarLoteRpsResponse; stdcall;

    // Cannot unwrap: 
    //     - Input element wrapper name does not match operation's name
    function  GerarNfse(const parameters: GerarNfseRequest): GerarNfseResponse; stdcall;

    // Cannot unwrap: 
    //     - Input element wrapper name does not match operation's name
    function  SubstituirNfse(const parameters: SubstituirNfseRequest): SubstituirNfseResponse; stdcall;

    // Cannot unwrap: 
    //     - Input element wrapper name does not match operation's name
    function  RecepcionarLoteRpsSincrono(const parameters: RecepcionarLoteRpsSincronoRequest): RecepcionarLoteRpsSincronoResponse; stdcall;
  end;

function Getnfse(UseWSDL: Boolean=System.False; Addr: string=''; HTTPRIO: THTTPRIO = nil): nfse;


implementation
  uses SysUtils;

function Getnfse(UseWSDL: Boolean; Addr: string; HTTPRIO: THTTPRIO): nfse;
const
  defWSDL = 'C:\Projetos_Aux\_NFSE\nfse.wsdl';
  defURL  = 'http://ws.pbh.gov.br';
  defSvc  = 'NfseWSService';
  defPrt  = 'nfseSOAP';
var
  RIO: THTTPRIO;
begin
  Result := nil;
  if (Addr = '') then
  begin
    if UseWSDL then
      Addr := defWSDL
    else
      Addr := defURL;
  end;
  if HTTPRIO = nil then
    RIO := THTTPRIO.Create(nil)
  else
    RIO := HTTPRIO;
  try
    Result := (RIO as nfse);
    if UseWSDL then
    begin
      RIO.WSDLLocation := Addr;
      RIO.Service := defSvc;
      RIO.Port := defPrt;
    end else
      RIO.URL := Addr;
  finally
    if (Result = nil) and (HTTPRIO = nil) then
      RIO.Free;
  end;
end;


constructor input.Create;
begin
  inherited Create;
  FSerializationOptions := [xoLiteralParam];
end;

constructor output.Create;
begin
  inherited Create;
  FSerializationOptions := [xoLiteralParam];
end;

initialization
  InvRegistry.RegisterInterface(TypeInfo(nfse), 'http://nfse.abrasf.org.br', 'UTF-8');
  InvRegistry.RegisterDefaultSOAPAction(TypeInfo(nfse), 'http://nfse.abrasf.org.br/%operationName%');
  InvRegistry.RegisterInvokeOptions(TypeInfo(nfse), ioDocument);
  InvRegistry.RegisterInvokeOptions(TypeInfo(nfse), ioLiteral);
  InvRegistry.RegisterExternalParamName(TypeInfo(nfse), 'CancelarNfse', 'parameters1', 'parameters');
  InvRegistry.RegisterExternalParamName(TypeInfo(nfse), 'ConsultarLoteRps', 'parameters1', 'parameters');
  InvRegistry.RegisterExternalParamName(TypeInfo(nfse), 'ConsultarNfseServicoPrestado', 'parameters1', 'parameters');
  InvRegistry.RegisterExternalParamName(TypeInfo(nfse), 'ConsultarNfseServicoTomado', 'parameters1', 'parameters');
  InvRegistry.RegisterExternalParamName(TypeInfo(nfse), 'ConsultarNfsePorFaixa', 'parameters1', 'parameters');
  InvRegistry.RegisterExternalParamName(TypeInfo(nfse), 'ConsultarNfsePorRps', 'parameters1', 'parameters');
  InvRegistry.RegisterExternalParamName(TypeInfo(nfse), 'RecepcionarLoteRps', 'parameters1', 'parameters');
  InvRegistry.RegisterExternalParamName(TypeInfo(nfse), 'GerarNfse', 'parameters1', 'parameters');
  InvRegistry.RegisterExternalParamName(TypeInfo(nfse), 'SubstituirNfse', 'parameters1', 'parameters');
  InvRegistry.RegisterExternalParamName(TypeInfo(nfse), 'RecepcionarLoteRpsSincrono', 'parameters1', 'parameters');
  RemClassRegistry.RegisterXSClass(input, 'http://nfse.abrasf.org.br', 'input');
  RemClassRegistry.RegisterSerializeOptions(input, [xoLiteralParam]);
  RemClassRegistry.RegisterXSClass(output, 'http://nfse.abrasf.org.br', 'output');
  RemClassRegistry.RegisterSerializeOptions(output, [xoLiteralParam]);
  RemClassRegistry.RegisterXSClass(CancelarNfseRequest, 'http://nfse.abrasf.org.br', 'CancelarNfseRequest');
  RemClassRegistry.RegisterXSClass(CancelarNfseResponse, 'http://nfse.abrasf.org.br', 'CancelarNfseResponse');
  RemClassRegistry.RegisterXSClass(ConsultarLoteRpsRequest, 'http://nfse.abrasf.org.br', 'ConsultarLoteRpsRequest');
  RemClassRegistry.RegisterXSClass(ConsultarLoteRpsResponse, 'http://nfse.abrasf.org.br', 'ConsultarLoteRpsResponse');
  RemClassRegistry.RegisterXSClass(ConsultarNfseServicoPrestadoRequest, 'http://nfse.abrasf.org.br', 'ConsultarNfseServicoPrestadoRequest');
  RemClassRegistry.RegisterXSClass(ConsultarNfseServicoPrestadoResponse, 'http://nfse.abrasf.org.br', 'ConsultarNfseServicoPrestadoResponse');
  RemClassRegistry.RegisterXSClass(ConsultarNfseServicoTomadoRequest, 'http://nfse.abrasf.org.br', 'ConsultarNfseServicoTomadoRequest');
  RemClassRegistry.RegisterXSClass(ConsultarNfseServicoTomadoResponse, 'http://nfse.abrasf.org.br', 'ConsultarNfseServicoTomadoResponse');
  RemClassRegistry.RegisterXSClass(ConsultarNfsePorFaixaRequest, 'http://nfse.abrasf.org.br', 'ConsultarNfsePorFaixaRequest');
  RemClassRegistry.RegisterXSClass(ConsultarNfsePorFaixaResponse, 'http://nfse.abrasf.org.br', 'ConsultarNfsePorFaixaResponse');
  RemClassRegistry.RegisterXSClass(ConsultarNfsePorRpsRequest, 'http://nfse.abrasf.org.br', 'ConsultarNfsePorRpsRequest');
  RemClassRegistry.RegisterXSClass(ConsultarNfsePorRpsResponse, 'http://nfse.abrasf.org.br', 'ConsultarNfsePorRpsResponse');
  RemClassRegistry.RegisterXSClass(RecepcionarLoteRpsRequest, 'http://nfse.abrasf.org.br', 'RecepcionarLoteRpsRequest');
  RemClassRegistry.RegisterXSClass(RecepcionarLoteRpsResponse, 'http://nfse.abrasf.org.br', 'RecepcionarLoteRpsResponse');
  RemClassRegistry.RegisterXSClass(GerarNfseRequest, 'http://nfse.abrasf.org.br', 'GerarNfseRequest');
  RemClassRegistry.RegisterXSClass(GerarNfseResponse, 'http://nfse.abrasf.org.br', 'GerarNfseResponse');
  RemClassRegistry.RegisterXSClass(SubstituirNfseRequest, 'http://nfse.abrasf.org.br', 'SubstituirNfseRequest');
  RemClassRegistry.RegisterXSClass(SubstituirNfseResponse, 'http://nfse.abrasf.org.br', 'SubstituirNfseResponse');
  RemClassRegistry.RegisterXSClass(RecepcionarLoteRpsSincronoRequest, 'http://nfse.abrasf.org.br', 'RecepcionarLoteRpsSincronoRequest');
  RemClassRegistry.RegisterXSClass(RecepcionarLoteRpsSincronoResponse, 'http://nfse.abrasf.org.br', 'RecepcionarLoteRpsSincronoResponse');

end.