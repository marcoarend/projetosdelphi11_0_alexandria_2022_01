// ************************************************************************ //
// The types declared in this file were generated from data read from the
// WSDL File described below:
// WSDL     : C:\Projetos\Delphi 2007\NFSe\v_02_01\WSDL\Maringa.wsdl
//  >Import : C:\Projetos\Delphi 2007\NFSe\v_02_01\WSDL\Maringa.wsdl:0
// Version  : 1.0
// (25/09/2012 11:51:55 - - $Rev: 7300 $)
// ************************************************************************ //

unit Maringa;

interface

uses InvokeRegistry, SOAPHTTPClient, Types, XSBuiltIns;

type

  // ************************************************************************ //
  // The following types, referred to in the WSDL document are not being represented
  // in this file. They are either aliases[@] of other types represented or were referred
  // to but never[!] declared in the document. The types from the latter category
  // typically map to predefined/known XML or Borland types; however, they could also 
  // indicate incorrect WSDL documents that failed to declare or import a schema type.
  // ************************************************************************ //
  // !:string          - "http://www.w3.org/2001/XMLSchema"[]
  // !:anyType         - "http://www.w3.org/2001/XMLSchema"[]


  // ************************************************************************ //
  // Namespace : https://isseteste.maringa.pr.gov.br/ws/
  // soapAction: https://isseteste.maringa.pr.gov.br/ws/#%operationName%
  // transport : http://schemas.xmlsoap.org/soap/http
  // style     : rpc
  // binding   : NfseServicesBinding
  // service   : NfseServicesService
  // port      : NfseServicesPort
  // URL       : https://isseteste.maringa.pr.gov.br/ws/
  // ************************************************************************ //
  NfseServicesPort = interface(IInvokable)
  ['{F72AEC2A-E227-431F-D706-759622B2FFC5}']
    function  EnviarLoteRps(const xml: WideString): WideString; stdcall;
    function  EnviarLoteRpsSincrono(const xml: WideString): WideString; stdcall;
    function  GerarNfse(const xml: WideString): WideString; stdcall;
    function  CancelarNfse(const xml: WideString): WideString; stdcall;
    function  SubstituirNfse(const xml: WideString): WideString; stdcall;
    function  ConsultarLoteRps(const xml: WideString): WideString; stdcall;
    function  ConsultarNfseRps(const xml: WideString): WideString; stdcall;
    function  ConsultarNfseServicoPrestado(const xml: WideString): WideString; stdcall;
    function  ConsultarNfseServicoTomado(const xml: WideString): WideString; stdcall;
    function  ConsultarNfseFaixa(const xml: WideString): WideString; stdcall;
    function  ValidarXml(const xml: WideString): Variant; stdcall;
    function  Teste(const xml: WideString): Variant; stdcall;
  end;

function GetNfseServicesPort(UseWSDL: Boolean=System.False; Addr: string=''; HTTPRIO: THTTPRIO = nil): NfseServicesPort;


implementation
  uses SysUtils;

function GetNfseServicesPort(UseWSDL: Boolean; Addr: string; HTTPRIO: THTTPRIO): NfseServicesPort;
const
  defWSDL = 'C:\Projetos\Delphi 2007\NFSe\v_02_01\WSDL\Maringa.wsdl';
  defURL  = 'https://isseteste.maringa.pr.gov.br/ws/';
  defSvc  = 'NfseServicesService';
  defPrt  = 'NfseServicesPort';
var
  RIO: THTTPRIO;
begin
  Result := nil;
  if (Addr = '') then
  begin
    if UseWSDL then
      Addr := defWSDL
    else
      Addr := defURL;
  end;
  if HTTPRIO = nil then
    RIO := THTTPRIO.Create(nil)
  else
    RIO := HTTPRIO;
  try
    Result := (RIO as NfseServicesPort);
    if UseWSDL then
    begin
      RIO.WSDLLocation := Addr;
      RIO.Service := defSvc;
      RIO.Port := defPrt;
    end else
      RIO.URL := Addr;
  finally
    if (Result = nil) and (HTTPRIO = nil) then
      RIO.Free;
  end;
end;


initialization
  InvRegistry.RegisterInterface(TypeInfo(NfseServicesPort), 'https://isseteste.maringa.pr.gov.br/ws/', '');
  InvRegistry.RegisterDefaultSOAPAction(TypeInfo(NfseServicesPort), 'https://isseteste.maringa.pr.gov.br/ws/#%operationName%');

end.