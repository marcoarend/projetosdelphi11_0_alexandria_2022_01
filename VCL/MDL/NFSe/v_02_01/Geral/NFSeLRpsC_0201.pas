unit NFSeLRpsC_0201;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls, Db, (*DBTables,*) JPEG, ExtDlgs,
  ZCF2, ResIntStrings,  UnMLAGeral, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, dmkGeral,
  dmkPermissoes, dmkEdit, dmkLabel, dmkRadioGroup, dmkValUsu,
  dmkDBLookupComboBox, dmkEditCB, Menus, Grids, DBGrids, UrlMon, ComCtrls,
  OleCtrls, SHDocVw, UnDmkProcFunc, dmkImage, (*NFeXMLGerencia,*) DmkDAC_PF,
  UnDMkEnums;

type
  TFmNFSeLRpsC_0201 = class(TForm)
    PainelDados: TPanel;
    DsNFSeLRpsC: TDataSource;
    QrNFSeLRpsC: TmySQLQuery;
    PainelEdita: TPanel;
    dmkPermissoes1: TdmkPermissoes;
    VuEmpresa: TdmkValUsu;
    PMLote: TPopupMenu;
    Incluinovolote1: TMenuItem;
    Alteraloteatual1: TMenuItem;
    Excluiloteatual1: TMenuItem;
    PMEvento: TPopupMenu;
    DsNFSeLRpsI: TDataSource;
    QrNFSeLRpsI: TmySQLQuery;
    N1: TMenuItem;
    Envialoteaofisco1: TMenuItem;
    N2: TMenuItem;
    Lerarquivo1: TMenuItem;
    PainelData: TPanel;
    PMXML: TPopupMenu;
    AbrirXMLnonavegadordestajanela1: TMenuItem;
    SelecionaroXML1: TMenuItem;
    Lote1: TMenuItem;
    Assinada1: TMenuItem;
    Gerada2: TMenuItem;
    AbrirXMLdaNFEnonavegadorpadro1: TMenuItem;
    Assina1: TMenuItem;
    Gerada1: TMenuItem;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBEdita: TGroupBox;
    Panel6: TPanel;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    Label8: TLabel;
    EdCodUsu: TdmkEdit;
    Label9: TLabel;
    EdNome: TdmkEdit;
    Label4: TLabel;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    BtLote: TBitBtn;
    BtEvento: TBitBtn;
    BtXML: TBitBtn;
    GroupBox1: TGroupBox;
    Panel4: TPanel;
    Label1: TLabel;
    Label5: TLabel;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    DBEdCodigo: TDBEdit;
    GBAvisos1: TGroupBox;
    Panel7: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    IncluiEventoaoloteatual1: TMenuItem;
    RemoveEventosselecionados1: TMenuItem;
    BtEnvia: TBitBtn;
    Label3: TLabel;
    DBEdit1: TDBEdit;
    Label2: TLabel;
    DBEdit4: TDBEdit;
    Label6: TLabel;
    DBEdit15: TDBEdit;
    Label18: TLabel;
    DBEdit16: TDBEdit;
    DBEdit17: TDBEdit;
    Label19: TLabel;
    DBEdit18: TDBEdit;
    Label20: TLabel;
    DBEdit19: TDBEdit;
    Label21: TLabel;
    DBEdit20: TDBEdit;
    Label22: TLabel;
    DBEdit21: TDBEdit;
    Label10: TLabel;
    DBEdit5: TDBEdit;
    Label11: TLabel;
    DBEdit6: TDBEdit;
    Label12: TLabel;
    DBEdit7: TDBEdit;
    QrNFSeLRpsICodigo: TIntegerField;
    QrNFSeLRpsIControle: TIntegerField;
    QrNFSeLRpsINFSeDPSCab: TIntegerField;
    QrNFSeLRpsIStatus: TIntegerField;
    QrNFSeLRpsCM: TmySQLQuery;
    DSNFSeLRpsCM: TDataSource;
    QrNFSeLRpsCMCodigo: TIntegerField;
    QrNFSeLRpsCMControle: TIntegerField;
    QrNFSeLRpsCMMsgCod: TWideStringField;
    QrNFSeLRpsCMMsgTxt: TWideMemoField;
    GroupBox2: TGroupBox;
    DBGrid1: TDBGrid;
    GroupBox3: TGroupBox;
    DBGrid2: TDBGrid;
    QrNFSeLRpsINO_Status: TWideStringField;
    DBEdit8: TDBEdit;
    DBGrid3: TDBGrid;
    QrNFSeLRpsCMMsgCor: TWideMemoField;
    Panel8: TPanel;
    DBMemo2: TDBMemo;
    DBMemo1: TDBMemo;
    QrNFSeLRpsCNO_Sincronia: TWideStringField;
    QrNFSeLRpsCNO_Ambiente: TWideStringField;
    QrNFSeLRpsCCodigo: TIntegerField;
    QrNFSeLRpsCSincronia: TSmallintField;
    QrNFSeLRpsCNFSeFatCab: TIntegerField;
    QrNFSeLRpsCAmbiente: TSmallintField;
    QrNFSeLRpsCStatus: TIntegerField;
    QrNFSeLRpsCEmpresa: TIntegerField;
    QrNFSeLRpsCNumeroLote: TLargeintField;
    QrNFSeLRpsCCpf: TWideStringField;
    QrNFSeLRpsCCnpj: TWideStringField;
    QrNFSeLRpsCInscricaoMunicipal: TWideStringField;
    QrNFSeLRpsCQuantidadeRps: TIntegerField;
    QrNFSeLRpsCversao: TFloatField;
    QrNFSeLRpsCId: TWideStringField;
    QrNFSeLRpsCLk: TIntegerField;
    QrNFSeLRpsCDataCad: TDateField;
    QrNFSeLRpsCDataAlt: TDateField;
    QrNFSeLRpsCUserCad: TIntegerField;
    QrNFSeLRpsCUserAlt: TIntegerField;
    QrNFSeLRpsCAlterWeb: TSmallintField;
    QrNFSeLRpsCAtivo: TSmallintField;
    QrNFSeLRpsCDataRecebimento: TDateTimeField;
    QrNFSeLRpsCProtocolo: TWideStringField;
    QrNFSeLRpsCNO_Status: TWideStringField;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrNFSeLRpsCAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrNFSeLRpsCBeforeOpen(DataSet: TDataSet);
    procedure BtLoteClick(Sender: TObject);
    procedure EdCodUsuKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SbNovoClick(Sender: TObject);
    procedure Alteraloteatual1Click(Sender: TObject);
    procedure Incluinovolote1Click(Sender: TObject);
    procedure PMLotePopup(Sender: TObject);
    procedure PMEventoPopup(Sender: TObject);
    procedure QrNFSeLRpsCAfterScroll(DataSet: TDataSet);
    procedure Envialoteaofisco1Click(Sender: TObject);
    procedure Verificalotenofisco1Click(Sender: TObject);
    procedure Lote1Click(Sender: TObject);
    procedure BtXMLClick(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
    procedure Lerarquivo1Click(Sender: TObject);
    procedure BtEventoClick(Sender: TObject);
    procedure IncluiEventoaoloteatual1Click(Sender: TObject);
    procedure RemoveEventosselecionados1Click(Sender: TObject);
    procedure QrNFSeLRpsICalcFields(DataSet: TDataSet);
    procedure BtEnviaClick(Sender: TObject);
    procedure QrNFSeLRpsCBeforeClose(DataSet: TDataSet);
    procedure QrNFSeLRpsCCalcFields(DataSet: TDataSet);
  private
    //FDirNFeXML: String;
    //FPreEmail: Integer;
    //
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    //procedure IncluiRegistro;
    //procedure AlteraRegistro;
   ////Procedures do form
    procedure MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo: Integer);
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    //
    //procedure MostraNFeEveEmail();
  public
    { Public declarations }
    procedure LocCod(Atual, Codigo: Integer);
    procedure ReopenNFSeLRpsCM();
    procedure ReopenNFSeLRpsI();
    procedure LocalizaLote(Codigo: Integer);
  end;

var
  FmNFSeLRpsC_0201: TFmNFSeLRpsC_0201;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, ModuleGeral, MyDBCheck, 
  NFSeLRpsI_0201, NFSe_PF_0000;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmNFSeLRpsC_0201.Lerarquivo1Click(Sender: TObject);
begin
{
  if DBCheck.CriaFm(TFmNFeSteps_0200, FmNFeSteps_0200, afmoNegarComAviso) then
  begin
    FmNFeSteps_0200.PnLoteEnv.Visible := True;
    FmNFeSteps_0200.Show;
    //
    FmNFeSteps_0200.RGAcao.ItemIndex := 6; // Enviar lote de Eventos da NFe
    FmNFeSteps_0200.CkSoLer.Checked  := True;
    FmNFeSteps_0200.PreparaEnvioDeLote(QrNFSeLRpsCCodigo.Value, QrNFSeLRpsCEmpresa.Value);
    FmNFeSteps_0200.FFormChamou      := 'FmNFeEveRLoE';
    //
    //
    //FmNFeSteps_0200.Destroy;
    //
    //LocCod(QrNFSeLRpsCCodigo.Value, QrNFSeLRpsCCodigo.Value);
  end;
}
end;

procedure TFmNFSeLRpsC_0201.LocalizaLote(Codigo: Integer);
begin
  LocCod(Codigo, Codigo);
  if QrNFSeLRpsCCodigo.Value <> Codigo then
    Geral.MensagemBox('N�o foi poss�vel localizar o lote ID ' + Geral.FF0(
    Codigo), 'Erro', MB_OK+MB_ICONERROR);
end;

procedure TFmNFSeLRpsC_0201.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmNFSeLRpsC_0201.Lote1Click(Sender: TObject);
//var
  //LoteStr: String;
begin
{
  LoteStr := FormatFloat('000000000', QrNFSeLRpsCCodigo.Value);
  DmNFe_0000.AbreXML_NoTWebBrowser(QrNFSeLRpsCEmpresa.Value, NFE_EXT_ENV_LOT_XML,
    LoteStr, True, WB_XML);
}
end;

procedure TFmNFSeLRpsC_0201.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrNFSeLRpsCCodigo.Value, LaRegistro.Caption[2]);
end;

procedure TFmNFSeLRpsC_0201.Verificalotenofisco1Click(Sender: TObject);
begin
end;

/////////////////////////////////////////////////////////////////////////////////////

procedure TFmNFSeLRpsC_0201.DefParams;
begin
  VAR_GOTOTABELA := 'nfselrpsc';
  VAR_GOTOMYSQLTABLE := QrNFSeLRpsC;
  VAR_GOTONEG := gotoPiZ;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT ELT(lrc.Sincronia+1, "N�o", "Sim") NO_Sincronia, ');
  VAR_SQLx.Add('ELT(lrc.Ambiente, "Produ��o", "Homologa��o") NO_Ambiente, ');
  VAR_SQLx.Add('lrc.*');
  VAR_SQLx.Add('FROM nfselrpsc lrc');
  VAR_SQLx.Add('');
  VAR_SQLx.Add('WHERE lrc.Codigo>-1000');
  VAR_SQLx.Add('');
  //
  VAR_SQL1.Add('AND lrc.Codigo=:P0');
  //
  VAR_SQL2.Add('AND lrc.CodUsu=:P0');
  //
  VAR_SQLa.Add('AND lrc.Nome Like :P0');
  //
end;

procedure TFmNFSeLRpsC_0201.EdCodUsuKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key = VK_F4) and (ImgTipo.SQLType = stIns) then
    UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, 'nfselrpsc', 'CodUsu', [], [],
    stIns, 0, siPositivo, EdCodUsu);
end;

procedure TFmNFSeLRpsC_0201.Envialoteaofisco1Click(Sender: TObject);
begin
{
  if DBCheck.CriaFm(TFmNFeSteps_0200, FmNFeSteps_0200, afmoNegarComAviso) then
  begin
    FmNFeSteps_0200.PnLoteEnv.Visible := True;
    FmNFeSteps_0200.Show;
    //
    FmNFeSteps_0200.RGAcao.ItemIndex := 6; // Enviar lote de Eventos da NFe
    FmNFeSteps_0200.PreparaEnvioDeLoteEvento(
      QrNFSeLRpsCCodigo.Value, QrNFSeLRpsCEmpresa.Value);
    FmNFeSteps_0200.FFormChamou      := 'FmNFeEveRLoE';
    //
    //
    //FmNFeSteps_0200.Destroy;
    //
  end;
}
end;

procedure TFmNFSeLRpsC_0201.MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo: Integer);
begin
  case Mostra of
    0:
    begin
      PainelDados.Visible := True;
      PainelEdita.Visible := False;
    end;
    1:
    begin
      PainelEdita.Visible := True;
      PainelDados.Visible := False;
      if SQLType = stIns then
      begin
        EdCodigo.Text := FormatFloat(FFormatFloat, Codigo);
        EdNome.Text := '';
        //...
      end else begin
        EdCodigo.Text := DBEdCodigo.Text;
        //EdNome.Text := DBEdNome.Text;
        //...
      end;
      EdNome.SetFocus;
    end;
    else Geral.MensagemBox('A��o de Inclus�o/altera��o n�o definida!',
    'Aviso', MB_OK+MB_ICONWARNING);
  end;
  ImgTipo.SQLType := SQLType;
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

{
procedure TFmNFSeLRpsC_0201.MostraNFeEveEmail();
begin
  if DBCheck.CriaFm(TFmNFeEveMail, FmNFeEveMail, afmoNegarComAviso) then
  begin
    Screen.Cursor := crHourGlass;
    FmNFeEveMail.ReopenNFSeLRpsI(QrNFSeLRpsCCodigo.Value);
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrCabA, Dmod.MyDB, [
    'SELECT Id, ide_serie, ide_nNF, ',
    'versao, IDCtrl, ',
    'IF(infCanc_cStat>0, infCanc_cStat, infProt_cStat) cStat, ',
    'IF(infCanc_cStat>0, infCanc_nProt, infProt_nProt) nProt, ',
    'IF(infCanc_cStat>0, infCanc_xMotivo, infProt_xMotivo) xMotivo, ',
    'IF(infCanc_cStat>0, infCanc_dhRecbto, infProt_dhRecbto) dhRecbto, ',
    'emit_xNome, emit_CNPJ, emit_CPF, emit_IE, emit_UF, ',
    'dest_xNome, dest_CNPJ, dest_CPF, dest_IE, dest_UF, ',
    'Transporta_CNPJ, Transporta_CPF, Transporta_xNome, ',
    'Transporta_IE, Transporta_UF ',
    'FROM nfecaba ',
    'WHERE Id="' + QrNFSeLRpsIchNFe.Value + '"',
    '']);

    // NFe
    FmNFeEveMail.EdNFeId.Text := QrCabAId.Value;
    FmNFeEveMail.EdSerie.Text := Geral.FF0(QrCabAide_serie.Value);
    FmNFeEveMail.EdNumeroNFe.Text := Geral.FF0(QrCabAide_nNF.Value);
    FmNFeEveMail.EdNFeStatus.Text := Geral.FF0(QrCabAcStat.Value);
    FmNFeEveMail.EdNFeStatus_TXT.Text := QrCabAxMotivo.Value;
    FmNFeEveMail.EdVersaoNFe.Text := Geral.FFT(QrCabAversao.Value, 2, siNegativo);
    FmNFeEveMail.EdNFeIDCtrl.Text := Geral.FF0(QrCabAIDCtrl.Value);

    // Emitente
    if QrCabAemit_CNPJ.Value <> '' then
      FmNFeEveMail.EdemitCNPJ_CPF.Text := Geral.FormataCNPJ_TT(QrCabAemit_CNPJ.Value)
    else
      FmNFeEveMail.EdemitCNPJ_CPF.Text := Geral.FormataCNPJ_TT(QrCabAemit_CPF.Value);
    FmNFeEveMail.Edemit_xNome.Text := QrCabAemit_xNome.Value;
    FmNFeEveMail.EdemitIE.Text := Geral.Formata_IE(QrCabAemit_IE.Value, QrCabAemit_UF.Value, '', 0);
    FmNFeEveMail.EdemitUF.Text := QrCabAemit_UF.Value;

    // Destinatario
    if QrCabAdest_CNPJ.Value <> '' then
      FmNFeEveMail.EddestCNPJ_CPF.Text := Geral.FormataCNPJ_TT(QrCabAdest_CNPJ.Value)
    else
      FmNFeEveMail.EddestCNPJ_CPF.Text := Geral.FormataCNPJ_TT(QrCabAdest_CPF.Value);
    FmNFeEveMail.Eddest_xNome.Text := QrCabAdest_xNome.Value;
    FmNFeEveMail.EddestIE.Text := Geral.Formata_IE(QrCabAdest_IE.Value, QrCabAdest_UF.Value, '', 0);
    FmNFeEveMail.EddestUF.Text := QrCabAdest_UF.Value;

    // Transportadora
    if QrCabATransporta_CNPJ.Value <> '' then
      FmNFeEveMail.EdtrspCNPJ_CPF.Text := Geral.FormataCNPJ_TT(QrCabATransporta_CNPJ.Value)
    else
      FmNFeEveMail.EdtrspCNPJ_CPF.Text := Geral.FormataCNPJ_TT(QrCabATransporta_CPF.Value);
    FmNFeEveMail.Edtrsp_xNome.Text := QrCabAtransporta_xNome.Value;
    FmNFeEveMail.EdtrspIE.Text := Geral.Formata_IE(QrCabAtransporta_IE.Value, QrCabAtransporta_UF.Value, '', 0);
    FmNFeEveMail.EdtrspUF.Text := QrCabAtransporta_UF.Value;

    // Protocolo
    FmNFeEveMail.EdtpEvento.Text := Geral.FF0(QrNFSeLRpsItpEvento.Value);
    FmNFeEveMail.EddescEvento.Text := QrNFSeLRpsIdescEvento.Value;
    FmNFeEveMail.EddhEvento.Text := Geral.FDT(QrNFSeLRpsIdhEvento.Value, 0);
    FmNFeEveMail.Edret_nProt.Text := QrNFSeLRpsIret_nProt.Value;
    FmNFeEveMail.Edret_dhregEvento.Text := Geral.FDT(QrNFSeLRpsIret_dhRegEvento.Value, 0);
    MyObjects.Informa2(FmNFeEveMail.LaEvento1, FmNFeEveMail.LaEvento2,
      False, 'Controle do evento: ' + Geral.FF0(QrNFSeLRpsIControle.Value));

    //
    FmNFeEveMail.EdNFeXML.Text := FDirNFeXML;
    FmNFeEveMail.EdPreEmail.ValueVariant := FPreEmail;
    FmNFeEveMail.CBPreEmail.KeyValue     := FPreEmail;
    FmNFeEveMail.FXML_Evento := QrNFSeLRpsIXML_Eve.Value;
    FmNFeEveMail.FXML_Protocolo := QrNFSeLRpsIXML_retEve.Value;

    Screen.Cursor := crDefault;
    FmNFeEveMail.ShowModal;

    FmNFeEveMail.Destroy;
  end;
end;
}

procedure TFmNFSeLRpsC_0201.PMLotePopup(Sender: TObject);
{
var
  HabilitaA, HabilitaB: Boolean;
}
begin
{
  HabilitaA := (QrNFSeLRpsC.State = dsBrowse) and (QrNFSeLRpsC.RecordCount > 0);
  // Leituras de arquivos
  LerArquivo1.Enabled      := HabilitaA;
  //
  HabilitaB := HabilitaA and (QrNFSeLRpsC.RecordCount = 0);
  Alteraloteatual1.Enabled := HabilitaB;
  //
  HabilitaB := HabilitaA and (not (QrNFSeLRpsCcStat.Value in ([103,104,105])));
  Envialoteaofisco1.Enabled := HabilitaB;
}
end;

procedure TFmNFSeLRpsC_0201.PMEventoPopup(Sender: TObject);
{
var
  Habilita: Boolean;
}
begin
{
  Habilita := (QrNFSeLRpsC.RecordCount > 0) and (QrNFSeLRpsCcStat.Value <> 103);
  Alteraloteatual1.Enabled := Habilita;
}
end;

procedure TFmNFSeLRpsC_0201.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmNFSeLRpsC_0201.QueryPrincipalAfterOpen;
begin
end;

procedure TFmNFSeLRpsC_0201.RemoveEventosselecionados1Click(Sender: TObject);
{
var
  Status: Integer;
}
begin
{
  if (QrNFSeLRpsCcStat.Value < 100) or (not (QrNFSeLRpsIret_cStat.Value in ([128,129])))
  or ((QrNFSeLRpsCcStat.Value in ([130])) and (QrNFSeLRpsIret_cStat.Value > 200)) then
  begin
    if Application.MessageBox('Deseja realmente tirar o evento deste lote?',
    'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
    begin
      if (QrNFSeLRpsIStatus.Value = 50)
      or ((QrNFSeLRpsCcStat.Value = 130) and (QrNFSeLRpsIret_cStat.Value > 200)) then
        Status := 30
      else
        Status := QrNFSeLRpsIStatus.Value;
      //
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('UPDATE nfeevercab SET EventoLote = 0, Status=:P0 ');
      Dmod.QrUpd.SQL.Add('WHERE FatID=:P1 AND FatNum=:P2 ');
      Dmod.QrUpd.SQL.Add('AND Empresa=:P3 AND Controle=:P4');
      Dmod.QrUpd.Params[00].AsInteger := Status;
      Dmod.QrUpd.Params[01].AsInteger := QrNFSeLRpsIFatID.Value;
      Dmod.QrUpd.Params[02].AsInteger := QrNFSeLRpsIFatNum.Value;
      Dmod.QrUpd.Params[03].AsInteger := QrNFSeLRpsIEmpresa.Value;
      Dmod.QrUpd.Params[04].AsInteger := QrNFSeLRpsIControle.Value;
      Dmod.QrUpd.ExecSQL;
      ReopenNFSeLRpsI();
    end;
  end else
    Geral.MensagemBox('A��o n�o permitida!', 'Aviso', MB_OK+MB_ICONWARNING);
}
end;

procedure TFmNFSeLRpsC_0201.ReopenNFSeLRpsCM;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrNFSeLRpsCM, Dmod.MyDB, [
  'SELECT * ',
  'FROM nfselrpscm ',
  'WHERE Codigo=' + Geral.FF0(QrNFSeLRpsCCodigo.Value),
  'ORDER BY Controle DESC ',
  '']);
end;

procedure TFmNFSeLRpsC_0201.ReopenNFSeLRpsI();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrNFSeLRpsI, Dmod.MyDB, [
  'SELECT * ',
  'FROM nfselrpsi ',
  'WHERE Codigo=' + Geral.FF0(QrNFSeLRpsCCodigo.Value),
  'ORDER BY Controle DESC ',
  '']);
end;

procedure TFmNFSeLRpsC_0201.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmNFSeLRpsC_0201.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmNFSeLRpsC_0201.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmNFSeLRpsC_0201.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmNFSeLRpsC_0201.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmNFSeLRpsC_0201.Alteraloteatual1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PainelEdita, QrNFSeLRpsC, [PainelDados],
  [PainelEdita], EdNome, ImgTipo, 'nfselrpsc');
end;

procedure TFmNFSeLRpsC_0201.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrNFSeLRpsCCodigo.Value;
  Close;
end;

procedure TFmNFSeLRpsC_0201.BtXMLClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMXML, BtXML);
end;

procedure TFmNFSeLRpsC_0201.BtConfirmaClick(Sender: TObject);
var
  Codigo: Integer;
begin
  Codigo := DModG.BuscaProximoCodigoInt('nfectrl', 'nfselrpsc', '', 0, 999999999, '');
  EdCodigo.ValueVariant := Codigo;
  EdCodUsu.ValueVariant := Codigo;
  if UMyMod.ExecSQLInsUpdPanel(ImgTipo.SQLType, Self, PainelEdita,
    'nfselrpsc', Codigo, Dmod.QrUpd, [PainelEdita], [PainelDados], ImgTipo, True) then
  begin
    LocCod(Codigo, Codigo);
  end;
end;

procedure TFmNFSeLRpsC_0201.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := Geral.IMV(EdCodigo.Text);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'nfselrpsc', 'Codigo');
  MostraEdicao(0, stLok, 0);
end;

procedure TFmNFSeLRpsC_0201.BtEnviaClick(Sender: TObject);
{
var
  Msg: String;
}
begin
{
  Msg := '';
  DmNFe_0000.ReopenEmpresa(QrNFSeLRpsCEmpresa.Value);
  DmNFe_0000.ReopenOpcoesNFe(DModG.QrEmpresasCodigo.Value, True);
  ReopenNFSeLRpsI();
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrProt, Dmod.MyDB, [
  'SELECT COUNT(*) Itens ',
  'FROM nfeevercab ',
  'WHERE EventoLote=' + Geral.FF0(QrNFSeLRpsCCodigo.Value),
  'AND Status in (135,136)',
  '']);
  if QrProt.RecordCount = 0 then
    Msg := 'N�o h� item protocolado para envio!'
  else
  if QrProtItens.Value < QrNFSeLRpsI.RecordCount then
  begin
    Msg := 'Somente os itens protocolados ser�o enviados!'
  end;
  if Msg <> '' then
    Geral.MensagemBox(Msg, 'Aviso', MB_OK+MB_ICONWARNING);
  if QrProt.RecordCount = 0 then
    Exit;

  FDirNFeXML := DmNFe_0000.QrFilialDirEveProcCCe.Value;
  if FDirNFeXML = '' then
  begin
    Geral.MensagemBox(
    'Diret�rio do processamento de carta de corre��o n�o definido!',
    'Aviso', MB_OK+MB_ICONWARNING);
    Exit;
  end;
  Geral.VerificaDir(FDirNFeXML, '\', '', True);
  FPreEmail := DmNFe_0000.QrFilialPreMailEveCCe.Value;

  QrNFSeLRpsI.DisableControls;
  try
    QrNFSeLRpsI.First;
    while not QrNFSeLRpsI.Eof do
    begin
      if QrNFSeLRpsIStatus.Value in ([135,136]) then
      begin
        MostraNFeEveEmail();
      end;
      //
      QrNFSeLRpsI.Next;
    end;
  finally
    QrNFSeLRpsI.EnableControls;
  end;
}
end;

procedure TFmNFSeLRpsC_0201.BtEventoClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMEvento, BtEvento);
end;

procedure TFmNFSeLRpsC_0201.BtLoteClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMLote, BtLote);
end;

procedure TFmNFSeLRpsC_0201.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  DModG.ReopenEmpresas(VAR_USUARIO, 0);
  CBEmpresa.ListSource := DModG.DsEmpresas;
  CriaOForm;
end;

procedure TFmNFSeLRpsC_0201.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrNFSeLRpsCCodigo.Value, LaRegistro.Caption);
end;

procedure TFmNFSeLRpsC_0201.SbImprimeClick(Sender: TObject);
begin
//
end;

procedure TFmNFSeLRpsC_0201.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmNFSeLRpsC_0201.SbNovoClick(Sender: TObject);
begin
  //LaRegistro.Caption := GOTOy.CodUsu(QrNFSeLRpsCCodUsu.Value, LaRegistro.Caption);
end;

procedure TFmNFSeLRpsC_0201.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmNFSeLRpsC_0201.QrNFSeLRpsICalcFields(DataSet: TDataSet);
begin
  QrNFSeLRpsINO_Status.Value := UnNFSe_PF_0000.TextoStatusNFSe(
    QrNFSeLRpsIStatus.Value);
end;

procedure TFmNFSeLRpsC_0201.QrNFSeLRpsCAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmNFSeLRpsC_0201.QrNFSeLRpsCAfterScroll(DataSet: TDataSet);
begin
  ReopenNFSeLRpsI();
  ReopenNFSeLRpsCM();
end;

procedure TFmNFSeLRpsC_0201.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmNFSeLRpsC_0201.SbQueryClick(Sender: TObject);
begin
  LocCod(QrNFSeLRpsCCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'nfselrpsc', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmNFSeLRpsC_0201.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmNFSeLRpsC_0201.IncluiEventoaoloteatual1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmNFSeLRpsI_0201, FmNFSeLRpsI_0201, afmoNegarComAviso) then
  begin
{
    FmNFSeLRpsI_0201.QrNFSeLRpsI.Close;
    FmNFSeLRpsI_0201.QrNFSeLRpsI.Params[00].AsInteger := QrNFSeLRpsCEmpresa.Value;
    FmNFSeLRpsI_0201.QrNFSeLRpsI.Params[01].AsInteger := DmNFe_0000.stepNFeAssinada();
    FmNFSeLRpsI_0201.QrNFSeLRpsI.Open;
}
    //
    FmNFSeLRpsI_0201.ShowModal;
    FmNFSeLRpsI_0201.Destroy;
    //
  end;
end;

procedure TFmNFSeLRpsC_0201.Incluinovolote1Click(Sender: TObject);
var
  Filial: Integer;
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PainelEdita, QrNFSeLRpsC, [PainelDados],
  [PainelEdita], EdNome, ImgTipo, 'nfselrpsc');
  UMyMod.ObtemValorDoCampoXDeIndex_Int(VAR_LIB_EMPRESA_SEL, 'Filial', 'Codigo',
    DmodG.QrEmpresas, EdEmpresa, CBEmpresa, Filial);
end;

procedure TFmNFSeLRpsC_0201.QrNFSeLRpsCBeforeClose(DataSet: TDataSet);
begin
  QrNFSeLRpsI.Close;
  QrNFSeLRpsCM.Close;
end;

procedure TFmNFSeLRpsC_0201.QrNFSeLRpsCBeforeOpen(DataSet: TDataSet);
begin
  QrNFSeLRpsCCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmNFSeLRpsC_0201.QrNFSeLRpsCCalcFields(DataSet: TDataSet);
begin
  QrNFSeLRpsCNO_Status.Value := UnNFSe_PF_0000.TextoStatusNFSe(
    QrNFSeLRpsCStatus.Value);
end;

end.

