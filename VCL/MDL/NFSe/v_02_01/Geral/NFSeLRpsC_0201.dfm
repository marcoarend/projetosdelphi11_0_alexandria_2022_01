object FmNFSeLRpsC_0201: TFmNFSeLRpsC_0201
  Left = 368
  Top = 194
  Caption = 'NFS-LOTES-001 :: Lotes de RPS - NFS-e '
  ClientHeight = 592
  ClientWidth = 1008
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelEdita: TPanel
    Left = 0
    Top = 96
    Width = 1008
    Height = 496
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    Visible = False
    object GBEdita: TGroupBox
      Left = 0
      Top = 0
      Width = 1008
      Height = 125
      Align = alTop
      TabOrder = 0
      object Panel6: TPanel
        Left = 2
        Top = 15
        Width = 1004
        Height = 108
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object Label7: TLabel
          Left = 8
          Top = 4
          Width = 14
          Height = 13
          Caption = 'ID:'
        end
        object Label8: TLabel
          Left = 68
          Top = 4
          Width = 57
          Height = 13
          Caption = 'C'#243'digo: [F4]'
        end
        object Label9: TLabel
          Left = 152
          Top = 4
          Width = 51
          Height = 13
          Caption = 'Descri'#231#227'o:'
        end
        object Label4: TLabel
          Left = 8
          Top = 48
          Width = 44
          Height = 13
          Caption = 'Empresa:'
        end
        object EdCodigo: TdmkEdit
          Left = 8
          Top = 20
          Width = 56
          Height = 21
          Alignment = taRightJustify
          Enabled = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ReadOnly = True
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'Codigo'
          UpdCampo = 'Codigo'
          UpdType = utInc
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
        end
        object EdCodUsu: TdmkEdit
          Left = 68
          Top = 20
          Width = 80
          Height = 21
          Alignment = taRightJustify
          Enabled = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 1
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'CodUsu'
          UpdCampo = 'CodUsu'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          OnKeyDown = EdCodUsuKeyDown
        end
        object EdNome: TdmkEdit
          Left = 152
          Top = 20
          Width = 633
          Height = 21
          TabOrder = 2
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          QryCampo = 'Nome'
          UpdCampo = 'Nome'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
        end
        object EdEmpresa: TdmkEditCB
          Left = 8
          Top = 64
          Width = 53
          Height = 21
          Alignment = taRightJustify
          TabOrder = 3
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          DBLookupComboBox = CBEmpresa
          IgnoraDBLookupComboBox = False
        end
        object CBEmpresa: TdmkDBLookupComboBox
          Left = 65
          Top = 64
          Width = 720
          Height = 21
          KeyField = 'Filial'
          ListField = 'NOMEFILIAL'
          ListSource = DModG.DsEmpresas
          TabOrder = 4
          dmkEditCB = EdEmpresa
          UpdType = utYes
          LocF7SQLMasc = '$#'
        end
      end
    end
    object GBConfirma: TGroupBox
      Left = 0
      Top = 433
      Width = 1008
      Height = 63
      Align = alBottom
      TabOrder = 1
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 12
        Top = 17
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object Panel1: TPanel
        Left = 898
        Top = 15
        Width = 108
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 7
          Top = 2
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
    end
  end
  object PainelDados: TPanel
    Left = 0
    Top = 96
    Width = 1008
    Height = 496
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    object PainelData: TPanel
      Left = 0
      Top = 0
      Width = 1008
      Height = 149
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 501
        Height = 149
        Align = alLeft
        Caption = ' Dados do Lote: '
        TabOrder = 0
        object Panel4: TPanel
          Left = 2
          Top = 15
          Width = 497
          Height = 132
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          object Label1: TLabel
            Left = 8
            Top = 4
            Width = 36
            Height = 13
            Caption = 'C'#243'digo:'
          end
          object Label5: TLabel
            Left = 8
            Top = 44
            Width = 44
            Height = 13
            Caption = 'Empresa:'
            FocusControl = DBEdit2
          end
          object Label3: TLabel
            Left = 80
            Top = 4
            Width = 40
            Height = 13
            Caption = 'N'#250'mero:'
          end
          object Label2: TLabel
            Left = 152
            Top = 4
            Width = 45
            Height = 13
            Caption = 'Sincrono:'
            FocusControl = DBEdit4
          end
          object Label6: TLabel
            Left = 208
            Top = 4
            Width = 62
            Height = 13
            Caption = 'Faturamento:'
            FocusControl = DBEdit15
          end
          object Label18: TLabel
            Left = 280
            Top = 4
            Width = 47
            Height = 13
            Caption = 'Ambiente:'
            FocusControl = DBEdit16
          end
          object Label19: TLabel
            Left = 340
            Top = 88
            Width = 33
            Height = 13
            Caption = 'Status:'
            FocusControl = DBEdit18
          end
          object Label20: TLabel
            Left = 436
            Top = 4
            Width = 51
            Height = 13
            Caption = 'Qtde RPS:'
            FocusControl = DBEdit19
          end
          object Label21: TLabel
            Left = 388
            Top = 4
            Width = 36
            Height = 13
            Caption = 'Vers'#227'o:'
            FocusControl = DBEdit20
          end
          object Label22: TLabel
            Left = 388
            Top = 44
            Width = 12
            Height = 13
            Caption = 'Id:'
            FocusControl = DBEdit21
          end
          object Label10: TLabel
            Left = 8
            Top = 88
            Width = 23
            Height = 13
            Caption = 'CPF:'
            FocusControl = DBEdit5
          end
          object Label11: TLabel
            Left = 124
            Top = 88
            Width = 30
            Height = 13
            Caption = 'CNPJ:'
            FocusControl = DBEdit6
          end
          object Label12: TLabel
            Left = 240
            Top = 88
            Width = 94
            Height = 13
            Caption = 'Inscri'#231#227'o Municipal:'
            FocusControl = DBEdit7
          end
          object DBEdit2: TDBEdit
            Left = 8
            Top = 60
            Width = 56
            Height = 21
            DataField = 'Empresa'
            DataSource = DsNFSeLRpsC
            TabOrder = 0
          end
          object DBEdit3: TDBEdit
            Left = 64
            Top = 60
            Width = 321
            Height = 21
            DataSource = DsNFSeLRpsC
            TabOrder = 1
          end
          object DBEdCodigo: TDBEdit
            Left = 8
            Top = 20
            Width = 68
            Height = 21
            DataField = 'Codigo'
            DataSource = DsNFSeLRpsC
            TabOrder = 2
          end
          object DBEdit1: TDBEdit
            Left = 80
            Top = 20
            Width = 68
            Height = 21
            DataField = 'Codigo'
            DataSource = DsNFSeLRpsC
            TabOrder = 3
          end
          object DBEdit4: TDBEdit
            Left = 152
            Top = 20
            Width = 52
            Height = 21
            DataField = 'NO_Sincronia'
            DataSource = DsNFSeLRpsC
            TabOrder = 4
          end
          object DBEdit15: TDBEdit
            Left = 208
            Top = 20
            Width = 68
            Height = 21
            DataField = 'NFSeFatCab'
            DataSource = DsNFSeLRpsC
            TabOrder = 5
          end
          object DBEdit16: TDBEdit
            Left = 280
            Top = 20
            Width = 20
            Height = 21
            DataField = 'Ambiente'
            DataSource = DsNFSeLRpsC
            TabOrder = 6
          end
          object DBEdit17: TDBEdit
            Left = 300
            Top = 20
            Width = 84
            Height = 21
            DataField = 'NO_Ambiente'
            DataSource = DsNFSeLRpsC
            TabOrder = 7
          end
          object DBEdit18: TDBEdit
            Left = 340
            Top = 104
            Width = 32
            Height = 21
            DataField = 'Status'
            DataSource = DsNFSeLRpsC
            TabOrder = 8
          end
          object DBEdit19: TDBEdit
            Left = 436
            Top = 20
            Width = 56
            Height = 21
            DataField = 'QuantidadeRps'
            DataSource = DsNFSeLRpsC
            TabOrder = 9
          end
          object DBEdit20: TDBEdit
            Left = 388
            Top = 20
            Width = 44
            Height = 21
            DataField = 'versao'
            DataSource = DsNFSeLRpsC
            TabOrder = 10
          end
          object DBEdit21: TDBEdit
            Left = 388
            Top = 60
            Width = 105
            Height = 21
            DataField = 'Id'
            DataSource = DsNFSeLRpsC
            TabOrder = 11
          end
          object DBEdit5: TDBEdit
            Left = 8
            Top = 104
            Width = 112
            Height = 21
            DataField = 'Cpf'
            DataSource = DsNFSeLRpsC
            TabOrder = 12
          end
          object DBEdit6: TDBEdit
            Left = 124
            Top = 104
            Width = 112
            Height = 21
            DataField = 'Cnpj'
            DataSource = DsNFSeLRpsC
            TabOrder = 13
          end
          object DBEdit7: TDBEdit
            Left = 240
            Top = 104
            Width = 97
            Height = 21
            DataField = 'InscricaoMunicipal'
            DataSource = DsNFSeLRpsC
            TabOrder = 14
          end
          object DBEdit8: TDBEdit
            Left = 372
            Top = 104
            Width = 121
            Height = 21
            DataField = 'NO_Status'
            DataSource = DsNFSeLRpsC
            TabOrder = 15
          end
        end
      end
      object GroupBox2: TGroupBox
        Left = 501
        Top = 0
        Width = 507
        Height = 149
        Align = alClient
        Caption = ' Erros e Alertas: '
        TabOrder = 1
        object DBGrid1: TDBGrid
          Left = 2
          Top = 15
          Width = 104
          Height = 132
          Align = alLeft
          DataSource = DSNFSeLRpsCM
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'MsgCod'
              Title.Caption = 'C'#243'digo'
              Width = 68
              Visible = True
            end>
        end
        object Panel8: TPanel
          Left = 106
          Top = 15
          Width = 399
          Height = 132
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 1
          object DBMemo2: TDBMemo
            Left = 0
            Top = 62
            Width = 399
            Height = 70
            Align = alClient
            DataField = 'MsgCor'
            DataSource = DSNFSeLRpsCM
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clPurple
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 0
          end
          object DBMemo1: TDBMemo
            Left = 0
            Top = 0
            Width = 399
            Height = 62
            Align = alTop
            DataField = 'MsgTxt'
            DataSource = DSNFSeLRpsCM
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clRed
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 1
          end
        end
      end
    end
    object GBCntrl: TGroupBox
      Left = 0
      Top = 432
      Width = 1008
      Height = 64
      Align = alBottom
      TabOrder = 1
      object Panel5: TPanel
        Left = 2
        Top = 15
        Width = 172
        Height = 47
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 174
        Top = 15
        Width = 311
        Height = 47
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel3: TPanel
        Left = 485
        Top = 15
        Width = 521
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object Panel2: TPanel
          Left = 412
          Top = 0
          Width = 109
          Height = 47
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 0
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 90
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
        object BtLote: TBitBtn
          Left = 4
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Lote'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          Visible = False
          OnClick = BtLoteClick
        end
        object BtEvento: TBitBtn
          Left = 96
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Evento'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          Visible = False
          OnClick = BtEventoClick
        end
        object BtXML: TBitBtn
          Left = 280
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&XML'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          Visible = False
          OnClick = BtXMLClick
        end
        object BtEnvia: TBitBtn
          Tag = 244
          Left = 188
          Top = 4
          Width = 90
          Height = 40
          Caption = '&Envia'
          NumGlyphs = 2
          TabOrder = 4
          Visible = False
          OnClick = BtEnviaClick
        end
      end
    end
    object GroupBox3: TGroupBox
      Left = 0
      Top = 149
      Width = 1008
      Height = 283
      Align = alClient
      Caption = ' Itens do Lote: '
      TabOrder = 2
      object DBGrid2: TDBGrid
        Left = 2
        Top = 15
        Width = 299
        Height = 266
        Align = alLeft
        DataSource = DsNFSeLRpsI
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'Controle'
            Title.Caption = 'Seq. Item'
            Width = 60
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NFSeDPSCab'
            Title.Caption = 'ID RPS'
            Width = 60
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Status'
            Width = 36
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NO_Status'
            Title.Caption = 'Descri'#231#227'o status'
            Width = 100
            Visible = True
          end>
      end
      object DBGrid3: TDBGrid
        Left = 301
        Top = 15
        Width = 705
        Height = 266
        Align = alClient
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 12
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 216
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
        OnClick = SbImprimeClick
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 216
      Top = 0
      Width = 744
      Height = 52
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 279
        Height = 32
        Caption = 'Lotes de RPS - NFS-e '
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 279
        Height = 32
        Caption = 'Lotes de RPS - NFS-e '
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 279
        Height = 32
        Caption = 'Lotes de RPS - NFS-e '
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 52
    Width = 1008
    Height = 44
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel7: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object DsNFSeLRpsC: TDataSource
    DataSet = QrNFSeLRpsC
    Left = 40
    Top = 12
  end
  object QrNFSeLRpsC: TmySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrNFSeLRpsCBeforeOpen
    AfterOpen = QrNFSeLRpsCAfterOpen
    BeforeClose = QrNFSeLRpsCBeforeClose
    AfterScroll = QrNFSeLRpsCAfterScroll
    OnCalcFields = QrNFSeLRpsCCalcFields
    SQL.Strings = (
      'SELECT ELT(lrc.Sincronia+1, '#39'N'#227'o'#39', '#39'Sim'#39') NO_Sincronia, '
      'ELT(lrc.Ambiente, '#39'Produ'#231#227'o'#39', '#39'Homologa'#231#227'o'#39') NO_Ambiente, lrc.*'
      'FROM nfselrpsc lrc'
      ''
      'WHERE lrc.Codigo>-1000'
      '')
    Left = 12
    Top = 12
    object QrNFSeLRpsCNO_Sincronia: TWideStringField
      FieldName = 'NO_Sincronia'
      Size = 3
    end
    object QrNFSeLRpsCNO_Ambiente: TWideStringField
      FieldName = 'NO_Ambiente'
      Size = 11
    end
    object QrNFSeLRpsCCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'nfselrpsc.Codigo'
    end
    object QrNFSeLRpsCSincronia: TSmallintField
      FieldName = 'Sincronia'
      Origin = 'nfselrpsc.Sincronia'
    end
    object QrNFSeLRpsCNFSeFatCab: TIntegerField
      FieldName = 'NFSeFatCab'
      Origin = 'nfselrpsc.NFSeFatCab'
    end
    object QrNFSeLRpsCAmbiente: TSmallintField
      FieldName = 'Ambiente'
      Origin = 'nfselrpsc.Ambiente'
    end
    object QrNFSeLRpsCStatus: TIntegerField
      FieldName = 'Status'
      Origin = 'nfselrpsc.Status'
    end
    object QrNFSeLRpsCEmpresa: TIntegerField
      FieldName = 'Empresa'
      Origin = 'nfselrpsc.Empresa'
    end
    object QrNFSeLRpsCNumeroLote: TLargeintField
      FieldName = 'NumeroLote'
      Origin = 'nfselrpsc.NumeroLote'
    end
    object QrNFSeLRpsCCpf: TWideStringField
      FieldName = 'Cpf'
      Origin = 'nfselrpsc.Cpf'
      Size = 18
    end
    object QrNFSeLRpsCCnpj: TWideStringField
      FieldName = 'Cnpj'
      Origin = 'nfselrpsc.Cnpj'
      Size = 18
    end
    object QrNFSeLRpsCInscricaoMunicipal: TWideStringField
      FieldName = 'InscricaoMunicipal'
      Origin = 'nfselrpsc.InscricaoMunicipal'
      Size = 18
    end
    object QrNFSeLRpsCQuantidadeRps: TIntegerField
      FieldName = 'QuantidadeRps'
      Origin = 'nfselrpsc.QuantidadeRps'
    end
    object QrNFSeLRpsCversao: TFloatField
      FieldName = 'versao'
      Origin = 'nfselrpsc.versao'
    end
    object QrNFSeLRpsCId: TWideStringField
      FieldName = 'Id'
      Origin = 'nfselrpsc.Id'
      Size = 255
    end
    object QrNFSeLRpsCLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'nfselrpsc.Lk'
    end
    object QrNFSeLRpsCDataCad: TDateField
      FieldName = 'DataCad'
      Origin = 'nfselrpsc.DataCad'
    end
    object QrNFSeLRpsCDataAlt: TDateField
      FieldName = 'DataAlt'
      Origin = 'nfselrpsc.DataAlt'
    end
    object QrNFSeLRpsCUserCad: TIntegerField
      FieldName = 'UserCad'
      Origin = 'nfselrpsc.UserCad'
    end
    object QrNFSeLRpsCUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Origin = 'nfselrpsc.UserAlt'
    end
    object QrNFSeLRpsCAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Origin = 'nfselrpsc.AlterWeb'
    end
    object QrNFSeLRpsCAtivo: TSmallintField
      FieldName = 'Ativo'
      Origin = 'nfselrpsc.Ativo'
    end
    object QrNFSeLRpsCDataRecebimento: TDateTimeField
      FieldName = 'DataRecebimento'
      Origin = 'nfselrpsc.DataRecebimento'
    end
    object QrNFSeLRpsCProtocolo: TWideStringField
      FieldName = 'Protocolo'
      Origin = 'nfselrpsc.Protocolo'
      Size = 50
    end
    object QrNFSeLRpsCNO_Status: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NO_Status'
      Size = 100
      Calculated = True
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = BtXML
    CanUpd01 = BtEvento
    Left = 68
    Top = 12
  end
  object VuEmpresa: TdmkValUsu
    dmkEditCB = EdEmpresa
    Panel = PainelEdita
    QryCampo = 'Empresa'
    UpdCampo = 'Empresa'
    RefCampo = 'Codigo'
    UpdType = utYes
    Left = 100
    Top = 12
  end
  object PMLote: TPopupMenu
    OnPopup = PMLotePopup
    Left = 520
    Top = 504
    object Incluinovolote1: TMenuItem
      Caption = '&Inclui novo lote'
      OnClick = Incluinovolote1Click
    end
    object Alteraloteatual1: TMenuItem
      Caption = '&Altera lote atual'
      OnClick = Alteraloteatual1Click
    end
    object Excluiloteatual1: TMenuItem
      Caption = '&Exclui lote atual'
      Enabled = False
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object Envialoteaofisco1: TMenuItem
      Caption = '&Envia lote ao fisco'
      Enabled = False
      OnClick = Envialoteaofisco1Click
    end
    object N2: TMenuItem
      Caption = '-'
    end
    object Lerarquivo1: TMenuItem
      Caption = '&Ler arquivo'
      OnClick = Lerarquivo1Click
    end
  end
  object PMEvento: TPopupMenu
    OnPopup = PMEventoPopup
    Left = 608
    Top = 508
    object IncluiEventoaoloteatual1: TMenuItem
      Caption = '&Inclui Evento ao lote atual'
      OnClick = IncluiEventoaoloteatual1Click
    end
    object RemoveEventosselecionados1: TMenuItem
      Caption = '&Remove Evento(s) selecionado(s)'
      OnClick = RemoveEventosselecionados1Click
    end
  end
  object DsNFSeLRpsI: TDataSource
    DataSet = QrNFSeLRpsI
    Left = 164
    Top = 12
  end
  object QrNFSeLRpsI: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrNFSeLRpsICalcFields
    SQL.Strings = (
      'SELECT * '
      'FROM nfselrpsi'
      'WHERE Codigo>0'
      '')
    Left = 136
    Top = 12
    object QrNFSeLRpsICodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrNFSeLRpsIControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrNFSeLRpsINFSeDPSCab: TIntegerField
      FieldName = 'NFSeDPSCab'
    end
    object QrNFSeLRpsIStatus: TIntegerField
      FieldName = 'Status'
    end
    object QrNFSeLRpsINO_Status: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NO_Status'
      Size = 100
      Calculated = True
    end
  end
  object PMXML: TPopupMenu
    Left = 792
    Top = 512
    object AbrirXMLnonavegadordestajanela1: TMenuItem
      Caption = 'Abrir XML no navegador desta janela'
      object Gerada2: TMenuItem
        Caption = '&Gerada'
      end
      object Assinada1: TMenuItem
        Caption = '&Assinada'
      end
      object Lote1: TMenuItem
        Caption = '&Lote'
        OnClick = Lote1Click
      end
      object SelecionaroXML1: TMenuItem
        Caption = '&Selecionar o XML'
      end
    end
    object AbrirXMLdaNFEnonavegadorpadro1: TMenuItem
      Caption = 'Abrir XML da NFE no navegador padr'#227'o'
      object Gerada1: TMenuItem
        Caption = '&Gerada'
      end
      object Assina1: TMenuItem
        Caption = '&Assina'
      end
    end
  end
  object QrNFSeLRpsCM: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrNFSeLRpsICalcFields
    SQL.Strings = (
      'SELECT * '
      'FROM nfselrpscm'
      'WHERE Codigo>0'
      '')
    Left = 192
    Top = 12
    object QrNFSeLRpsCMCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrNFSeLRpsCMControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrNFSeLRpsCMMsgCod: TWideStringField
      FieldName = 'MsgCod'
      Size = 60
    end
    object QrNFSeLRpsCMMsgTxt: TWideMemoField
      FieldName = 'MsgTxt'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrNFSeLRpsCMMsgCor: TWideMemoField
      FieldName = 'MsgCor'
      BlobType = ftWideMemo
      Size = 4
    end
  end
  object DSNFSeLRpsCM: TDataSource
    DataSet = QrNFSeLRpsCM
    Left = 220
    Top = 12
  end
end
