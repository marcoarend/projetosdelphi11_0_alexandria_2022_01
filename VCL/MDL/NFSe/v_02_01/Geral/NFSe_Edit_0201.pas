unit NFSe_Edit_0201;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnMLAGeral, UnInternalConsts,
  dmkImage, dmkValUsu, dmkLabelRotate, dmkMemo, dmkRadioGroup, DmkDAC_PF,
  NFSe_PF_0000,
  // ini 2023-12-20
  //DMKpnfsConversao,
  // fim 2023-12-20
  dmkCompoStore, UnDmkEnums, dmkCheckBox;

type
  TFmNFSe_Edit_0201 = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    QrClientes: TmySQLQuery;
    QrClientesCodigo: TIntegerField;
    QrClientesNOMEENTIDADE: TWideStringField;
    DsClientes: TDataSource;
    Panel7: TPanel;
    Label9: TLabel;
    Label21: TLabel;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    TPRpsDataEmissao: TdmkEditDateTimePicker;
    EdRpsHoraEmissao: TdmkEdit;
    VUEmpresa: TdmkValUsu;
    Panel11: TPanel;
    QrListServ: TmySQLQuery;
    DsListServ: TDataSource;
    QrListServNome: TWideStringField;
    QrNFSeSrvCad: TmySQLQuery;
    DsNFSeSrvCad: TDataSource;
    QrNFSeSrvCadCodigo: TIntegerField;
    QrNFSeSrvCadNome: TWideStringField;
    Label1: TLabel;
    TPCompetencia: TdmkEditDateTimePicker;
    QrIntermediarios: TmySQLQuery;
    DsIntermediarios: TDataSource;
    QrIntermediariosCodigo: TIntegerField;
    QrIntermediariosNOMEENTIDADE: TWideStringField;
    QrTsTipoRps1: TmySQLQuery;
    DsTsTipoRps1: TDataSource;
    QrTsTipoRps1Codigo: TIntegerField;
    QrTsTipoRps1Nome: TWideStringField;
    QrTsResponsavelRetencao: TmySQLQuery;
    DsTsResponsavelRetencao: TDataSource;
    QrTsResponsavelRetencaoCodigo: TIntegerField;
    QrTsResponsavelRetencaoNome: TWideStringField;
    QrTsExigibilidadeIss: TmySQLQuery;
    DsTsExigibilidadeIss: TDataSource;
    QrTsExigibilidadeIssCodigo: TIntegerField;
    QrTsExigibilidadeIssNome: TWideStringField;
    QrCNAE21Cad: TmySQLQuery;
    DsCNAE21Cad: TDataSource;
    QrMeuServico: TmySQLQuery;
    QrMeuServicoCodigoTributacaoMunicipio: TWideStringField;
    QrMeuServicoResponsavelRetencao: TSmallintField;
    QrMeuServicoItemListaServico: TWideStringField;
    QrMeuServicoCodigoCnae: TWideStringField;
    QrMeuServicoNumeroProcesso: TWideStringField;
    QrMeuServicoExigibilidadeIss: TSmallintField;
    Panel5: TPanel;
    Panel8: TPanel;
    Panel9: TPanel;
    Label16: TLabel;
    Label17: TLabel;
    EdCodigo: TdmkEdit;
    EdIntermediario: TdmkEditCB;
    CBIntermediario: TdmkDBLookupComboBox;
    SbIntermediario: TSpeedButton;
    Label34: TLabel;
    EdCliente: TdmkEditCB;
    CBCliente: TdmkDBLookupComboBox;
    SbCliente: TSpeedButton;
    Label19: TLabel;
    EdNFSeSrvCad: TdmkEditCB;
    CBNFSeSrvCad: TdmkDBLookupComboBox;
    SbNFSeSrvCad: TSpeedButton;
    Label18: TLabel;
    EdItemListaServico: TdmkEditCB;
    CBItemListaServico: TdmkDBLookupComboBox;
    SbItemListaServico: TSpeedButton;
    Label36: TLabel;
    EdCodigoCnae: TdmkEditCB;
    CBCodigoCnae: TdmkDBLookupComboBox;
    SbCodigoCnae: TSpeedButton;
    dmkLabelRotate1: TdmkLabelRotate;
    MeDiscriminacao: TdmkMemo;
    Panel10: TPanel;
    GroupBox3: TGroupBox;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label32: TLabel;
    Label33: TLabel;
    EdValorServicos: TdmkEdit;
    EdValorDeducoes: TdmkEdit;
    EdAliquota: TdmkEdit;
    EdDescontoIncondicionado: TdmkEdit;
    EdDescontoCondicionado: TdmkEdit;
    GroupBox2: TGroupBox;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    SbSubstTipo: TSpeedButton;
    EdSubstNumero: TdmkEdit;
    EdSubstSerie: TdmkEdit;
    EdSubstTipo: TdmkEditCB;
    CBSubstTipo: TdmkDBLookupComboBox;
    GroupBox4: TGroupBox;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    EdValorPis: TdmkEdit;
    EdValorCofins: TdmkEdit;
    EdValorInss: TdmkEdit;
    EdValorIr: TdmkEdit;
    EdValorCsll: TdmkEdit;
    EdOutrasRetencoes: TdmkEdit;
    GroupBox5: TGroupBox;
    SbResponsavelRetencao: TSpeedButton;
    Label35: TLabel;
    EdResponsavelRetencao: TdmkEditCB;
    CBResponsavelRetencao: TdmkDBLookupComboBox;
    GroupBox6: TGroupBox;
    SpeedButton6: TSpeedButton;
    Label39: TLabel;
    Label38: TLabel;
    EdExigibilidadeIss: TdmkEditCB;
    CBExigibilidadeIss: TdmkDBLookupComboBox;
    EdNumeroProcesso: TdmkEdit;
    Panel6: TPanel;
    GroupBox7: TGroupBox;
    SbMunicipioIncidencia: TSpeedButton;
    Label22: TLabel;
    EdQuemPagaISS: TdmkEditCB;
    CBQuemPagaISS: TdmkDBLookupComboBox;
    QrDTB_Munici: TmySQLQuery;
    QrDTB_MuniciCodigo: TIntegerField;
    QrDTB_MuniciNome: TWideStringField;
    DsDTB_Munici: TDataSource;
    QrListServCodAlf: TWideStringField;
    QrCNAE21CadCodAlf: TWideStringField;
    QrCNAE21CadNome: TWideStringField;
    QrMeuServicoCodigo: TIntegerField;
    QrMeuServicoNome: TWideStringField;
    QrMeuServicoDiscriminacao: TWideMemoField;
    GroupBox8: TGroupBox;
    Label23: TLabel;
    Label24: TLabel;
    EdConstrucaoCivilCodigoObra: TdmkEdit;
    EdConstrucaoCivilArt: TdmkEdit;
    Panel12: TPanel;
    QrTsRegimeEspecialTributacao: TmySQLQuery;
    QrTsRegimeEspecialTributacaoCodigo: TIntegerField;
    QrTsRegimeEspecialTributacaoNome: TWideStringField;
    QrTsSimNao2: TmySQLQuery;
    QrTsSimNao2Codigo: TIntegerField;
    QrTsSimNao2Nome: TWideStringField;
    DsTsRegimeEspecialTributacao: TDataSource;
    QrTsSimNao1: TmySQLQuery;
    QrTsSimNao1Codigo: TIntegerField;
    QrTsSimNao1Nome: TWideStringField;
    DsTsSimNao1: TDataSource;
    DsTsSimNao2: TDataSource;
    QrMeuServicoRegimeEspecialTributacao: TSmallintField;
    QrMeuServicoOptanteSimplesNacional: TSmallintField;
    QrMeuServicoIncentivoFiscal: TSmallintField;
    Label28: TLabel;
    EdRpsIDTipo: TdmkEditCB;
    CBRpsIDTipo: TdmkDBLookupComboBox;
    SbRpsIDTipo: TSpeedButton;
    QrTsTipoRps2: TmySQLQuery;
    DsTsTipoRps2: TDataSource;
    QrTsTipoRps2Codigo: TIntegerField;
    QrTsTipoRps2Nome: TWideStringField;
    QrMeuServicoRpsTipo: TSmallintField;
    Label20: TLabel;
    EdIssRetido: TdmkEditCB;
    CBIssRetido: TdmkDBLookupComboBox;
    QrTsSimNao3: TmySQLQuery;
    DsTsSimNao3: TDataSource;
    QrTsSimNao3Codigo: TIntegerField;
    QrTsSimNao3Nome: TWideStringField;
    QrMeuServicoQuemPagaISS: TSmallintField;
    EdValorIss: TdmkEdit;
    Label8: TLabel;
    QrMeuServicoAliquota: TFloatField;
    EdRpsIdNumero: TdmkEdit;
    EdRpsIdSerie: TdmkEdit;
    Label29: TLabel;
    Label30: TLabel;
    RGFormaGeraEEnvia: TRadioGroup;
    QrMeuServicoNaturezaOperacao: TIntegerField;
    QrMeuServicoAlqPIS: TFloatField;
    QrMeuServicoAlqCOFINS: TFloatField;
    QrMeuServicoAlqINSS: TFloatField;
    QrMeuServicoAlqIR: TFloatField;
    QrMeuServicoAlqCSLL: TFloatField;
    QrMeuServicoAlqOutrasRetencoes: TFloatField;
    CSTabSheetChamou: TdmkCompoStore;
    QrMeuServicoLk: TIntegerField;
    QrMeuServicoDataCad: TDateField;
    QrMeuServicoDataAlt: TDateField;
    QrMeuServicoUserCad: TIntegerField;
    QrMeuServicoUserAlt: TIntegerField;
    QrMeuServicoAlterWeb: TSmallintField;
    QrMeuServicoAtivo: TSmallintField;
    QrMeuServicoPDFCompres: TSmallintField;
    QrMeuServicoPDFEmbFont: TSmallintField;
    QrMeuServicoPDFPrnOptm: TSmallintField;
    QrMeuServicoImportado: TIntegerField;
    QrMeuServicoindFinal: TIntegerField;
    GroupBox13: TGroupBox;
    PnTotTrib: TPanel;
    CkiTotTrib: TdmkCheckBox;
    RGFontTotTrib: TRadioGroup;
    RGTabTotTrib: TRadioGroup;
    Panel14: TPanel;
    Label44: TLabel;
    EdVerTotTrib: TdmkEdit;
    RGTpAliqTotTrib: TRadioGroup;
    Panel13: TPanel;
    Label31: TLabel;
    Label40: TLabel;
    Label41: TLabel;
    EdvBasTrib: TdmkEdit;
    EdvTotTrib: TdmkEdit;
    EdpTotTrib: TdmkEdit;
    GroupBox10: TGroupBox;
    Label37: TLabel;
    EdCodigoTributacaoMunicipio: TdmkEdit;
    GroupBox11: TGroupBox;
    SpeedButton8: TSpeedButton;
    Label25: TLabel;
    EdOptanteSimplesNacional: TdmkEditCB;
    CBOptanteSimplesNacional: TdmkDBLookupComboBox;
    GroupBox12: TGroupBox;
    SpeedButton9: TSpeedButton;
    Label26: TLabel;
    EdIncentivoFiscal: TdmkEditCB;
    CBIncentivoFiscal: TdmkDBLookupComboBox;
    GroupBox9: TGroupBox;
    SpeedButton7: TSpeedButton;
    Label27: TLabel;
    EdRegimeEspecialTributacao: TdmkEditCB;
    CBRegimeEspecialTributacao: TdmkDBLookupComboBox;
    QrListServCodigo: TIntegerField;
    QrCNAE21CadCodigo: TIntegerField;
    QrClientesCEP: TIntegerField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbClienteClick(Sender: TObject);
    procedure SbItemListaServicoClick(Sender: TObject);
    procedure SbNFSeSrvCadClick(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure SbIntermediarioClick(Sender: TObject);
    procedure SbSubstTipoClick(Sender: TObject);
    procedure SbCodigoCnaeClick(Sender: TObject);
    procedure EdNFSeSrvCadEnter(Sender: TObject);
    procedure EdNFSeSrvCadExit(Sender: TObject);
    procedure EdNFSeSrvCadChange(Sender: TObject);
    procedure MeDiscriminacaoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SbRpsIDTipoClick(Sender: TObject);
    procedure EdValorServicosChange(Sender: TObject);
    procedure EdAliquotaChange(Sender: TObject);
    procedure EdValorDeducoesChange(Sender: TObject);
    procedure EdDescontoIncondicionadoChange(Sender: TObject);
    procedure RGFormaGeraEEnviaClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure EdItemListaServicoChange(Sender: TObject);
    procedure CkiTotTribClick(Sender: TObject);
    procedure EdEmpresaChange(Sender: TObject);
    procedure RGFontTotTribClick(Sender: TObject);
  private
    { Private declarations }
    FServico: Integer;
    //
    procedure ConfiguraItensMeuServico();
    //procedure ObtemMunicipioDeEntidade(Entidade: Integer);
    //
    procedure CalculaISS();
    procedure CalculaRetencoes();
    procedure FechaForm();
    procedure ReopenMeuServico();
    procedure ConfiguraDiscriminacao();

  public
    { Public declarations }
    FSerie: String;
    FNFSeFatCab, FNumero: Integer;
    FGeraNFSe: Boolean;
    FQrNFSeDPSCab: TmySQLQuery;
    FFormaGerarNFSe: TnfseFormaGerarNFSe;
  end;

  var
  FmNFSe_Edit_0201: TFmNFSe_Edit_0201;

implementation

uses MyDBCheck, UnMyObjects, Module, ModuleGeral, UMySQLModule,
  CNAE21, NFSe_PF_0201, NFSe_0000_Module, MyGlyfs, Principal;

{$R *.DFM}

procedure TFmNFSe_Edit_0201.BtOKClick(Sender: TObject);
  function VerificaCodCidadePais(const Cliente: Integer; var Msg: String): Boolean;
  var
    CodMunici, PaisCod: Integer;
  begin
    Msg    := '';
    Result := False;
    //
    UnDmkDAC_PF.AbreMySQLQuery0(DModG.QrAux, Dmod.MyDB, [
    'SELECT ',
    'IF(Tipo=0, ECodMunici, PCodMunici) Cidade, ',
    'IF(Tipo=0, ECodiPais, PCodiPais) Pais ',
    'FROM entidades ',
    'WHERE Codigo = ' + Geral.FF0(Cliente), 
    '']);
    if DModG.QrAux.RecordCount > 0 then
    begin
      CodMunici := DModG.QrAux.FieldByName('Cidade').AsInteger;
      PaisCod   := DModG.QrAux.FieldByName('Pais').AsInteger;
      //
      if CodMunici = 0 then
      begin
        Msg    := 'C�digo do munic�pio n�o definido no cadastro do cliente!';
        Result := True;
      end else
      if PaisCod = 0 then
      begin
        Msg    := 'C�digo do pa�s n�o definido no cadastro do cliente!';
        Result := True;
      end;
    end;
  end;
var
  RpsDataEmissao, Competencia: TDateTime;
  (*RpsID,*) RpsIDSerie, SubstSerie, ItemListaServico,
  CodigoCnae, CodigoTributacaoMunicipio, Discriminacao, NumeroProcesso,
  (*PrestaCpf, PrestaCnpj, PrestaInscricaoMunicipal, TomaCpf, TomaCnpj,
  TomaInscricaoMunicipal, TomaRazaoSocial, TomaEndereco, TomaNumero,
  TomaComplemento, TomaBairro, TomaUf, IntermeCpf, IntermeCnpj,
  IntermeInscricaoMunicipal, IntermeRazaoSocial,*) ConstrucaoCivilCodigoObra,
  ConstrucaoCivilArt, Id, RpsHoraEmissao, Msg: String;
  CodAtual, Empresa, Cliente, Intermediario, RpsIDNumero, RpsIDTipo, RpsStatus,
  SubstNumero, SubstTipo, IssRetido, ResponsavelRetencao, (*CodigoMunicipio,
  CodigoPais,*) ExigibilidadeISS, QuemPagaIss, MunicipioIncidencia, (*TomaCodigoMunicipio,
  TomaCodigoPais, TomaCep,*) RegimeEspecialTributacao, OptanteSimplesNacional,
  IncentivoFiscal, NFSeSrvCad, NFSeFatCab: Integer;
  ValorServicos, ValorDeducoes, ValorPis, ValorCofins, ValorInss, ValorIr,
  ValorCsll, OutrasRetencoes, ValorIss, Aliquota, DescontoIncondicionado,
  DescontoCondicionado: Double;
  DPS, Codigo: Integer;
  Qry: TmySQLQuery;
  // INICIO LEI N� 12.741 - Lei da Transparencia
  iTotTrib: Integer; vTotTrib, vBasTrib, pTotTrib: Double; fontTotTrib:
  TIBPTaxFont; tabTotTrib: TIBPTaxTabs; verTotTrib: String;
  tpAliqTotTrib: TIBPTaxOrig;
  // FIM LEI N� 12.741 - Lei da Transparencia
begin
  CodAtual       := 0;
  //
  if not DModG.ObtemEmpresaSelecionada(EdEmpresa, Empresa) then Exit;
  if MyObjects.FIC_Edit_Int(EdCliente, 'Informe o tomador (Cliente)!', Cliente) then Exit;
  if VerificaCodCidadePais(EdCliente.ValueVariant, Msg) then
  begin
    Geral.MB_Aviso(Msg);
    EdCliente.SetFocus;
    Exit;
  end;
  if MyObjects.FIC(QrClientesCEP.Value = 0, EdCliente, 'Informe o CEP no cadastro do tomador (Cliente)!') then Exit;
  //
  Intermediario := EdIntermediario.ValueVariant;
  //RpsID          := ;
  RpsIDNumero               := EdRpsIDNumero.ValueVariant;
  RpsIDSerie                := EdRpsIDSerie.ValueVariant;
  RpsIDTipo                 := EdRpsIDTipo.ValueVariant;
  RpsDataEmissao            := TPRpsDataEmissao.Date;
  RpsStatus                 := tsStatusRps_Normal;
  Competencia               := TPCompetencia.Date;
  SubstNumero               := EdSubstNumero.ValueVariant;
  SubstSerie                := EdSubstSerie.Text;
  SubstTipo                 := EdSubstTipo.ValueVariant;
  ValorServicos             := EdValorServicos.ValueVariant;
  ValorDeducoes             := EdValorDeducoes.ValueVariant;
  ValorPis                  := EdValorPis.ValueVariant;
  ValorCofins               := EdValorCofins.ValueVariant;
  ValorInss                 := EdValorInss.ValueVariant;
  ValorIr                   := EdValorIr.ValueVariant;
  ValorCsll                 := EdValorCsll.ValueVariant;
  OutrasRetencoes           := EdOutrasRetencoes.ValueVariant;
  ValorIss                  := EdValorIss.ValueVariant;
  Aliquota                  := EdAliquota.ValueVariant;
  DescontoIncondicionado    := EdDescontoIncondicionado.ValueVariant;
  DescontoCondicionado      := EdDescontoCondicionado.ValueVariant;
  IssRetido                 := EdIssRetido.ValueVariant;
  ResponsavelRetencao       := EdResponsavelRetencao.ValueVariant;
  ItemListaServico          := EdItemListaServico.ValueVariant;
  CodigoCnae                := EdCodigoCnae.ValueVariant;
  CodigoTributacaoMunicipio := EdCodigoTributacaoMunicipio.ValueVariant;
  Discriminacao             := MeDiscriminacao.Text;
  //CodigoMunicipio           := EdCodigoMunicipio.ValueVariant;
  //CodigoPais                := EdCodigoPais         .ValueVariant;
  ExigibilidadeISS          := EdExigibilidadeISS   .ValueVariant;
  QuemPagaISS               := EdQuemPagaISS.ValueVariant;
  MunicipioIncidencia       := -1;
  NumeroProcesso            := EdNumeroProcesso     .ValueVariant;
  //PrestaCpf                 := Ed.ValueVariant;
  //PrestaCnpj                := Ed.ValueVariant;
  //PrestaInscricaoMunicipal  := Ed.ValueVariant;
  //TomaCpf                   := Ed.ValueVariant;
  //TomaCnpj                  := Ed.ValueVariant;
  //TomaInscricaoMunicipal:= ;
  //TomaRazaoSocial:= ;
  //TomaEndereco   := ;
  //TomaNumero     := ;
  //TomaComplemento:= ;
  //TomaBairro     := ;
  //TomaCodigoMunicipio:= ;
  //TomaUf         := ;
  //TomaCodigoPais := ;
  //TomaCep        := ;
  //IntermeCpf     := ;
  //IntermeCnpj    := ;
  //IntermeInscricaoMunicipal := ;
  //IntermeRazaoSocial:= ;
  ConstrucaoCivilCodigoObra := EdConstrucaoCivilCodigoObra.Text;
  ConstrucaoCivilArt        := EdConstrucaoCivilArt.Text;
  RegimeEspecialTributacao  := EdRegimeEspecialTributacao.ValueVariant;
  OptanteSimplesNacional    := EdOptanteSimplesNacional.ValueVariant;
  IncentivoFiscal           := EdIncentivoFiscal.ValueVariant;
  //Id                        := ;
  NFSeSrvCad                := EdNFSeSrvCad.ValueVariant;
  RpsHoraEmissao            := EdRpsHoraEmissao.Text;
  //
  CodAtual := EdCodigo.ValueVariant;
  NFSeFatCab := FNFSeFatCab; // 0 = avulso.
  //
  // INICIO LEI N� 12.741 - Lei da Transparencia
  iTotTrib      := Geral.BoolToInt(CkiTotTrib.Checked);
  if CkiTotTrib.Checked then
  begin
    vTotTrib      := EdvTotTrib.ValueVariant;
    vBasTrib      := EdvBasTrib.ValueVariant;
    pTotTrib      := EdpTotTrib.ValueVariant;
    fontTotTrib   := TIBPTaxFont(RGFontTotTrib.ItemIndex);
    tabTotTrib    := TIBPTaxTabs(RGTabTotTrib.ItemIndex);
    verTotTrib    := EdVerTotTrib.ValueVariant;
    tpAliqTotTrib := TIBPTaxOrig(RGTpAliqTotTrib.ItemIndex);
  end else
  begin
    vTotTrib      := 0;
    vBasTrib      := 0;
    pTotTrib      := 0;
    fontTotTrib   := TIBPTaxFont.ibptaxfontNaoInfo;
    tabTotTrib    := TIBPTaxTabs.ibptaxtabNCM;
    verTotTrib    := '0.00';
    tpAliqTotTrib := TIBPTaxOrig.ibptaxoriNaoInfo;
  end;
  // FIM LEI N� 12.741 - Lei da Transparencia
  //
  Codigo := UnNFSe_PF_0000.InsUpdDeclaracaoPrestacaoServico(ImgTipo.SQLType,
  NFSeFatCab,
  RpsDataEmissao, Competencia, (*RpsID,*) RpsIDSerie, SubstSerie,
  ItemListaServico, CodigoCnae, CodigoTributacaoMunicipio,
  Discriminacao, NumeroProcesso, (*PrestaCpf, PrestaCnpj,
  PrestaInscricaoMunicipal, TomaCpf, TomaCnpj,
  TomaInscricaoMunicipal, TomaRazaoSocial, TomaEndereco, TomaNumero,
  TomaComplemento, TomaBairro, TomaUf, IntermeCpf, IntermeCnpj,
  IntermeInscricaoMunicipal, IntermeRazaoSocial,*)
  ConstrucaoCivilCodigoObra, ConstrucaoCivilArt, Id,
  RpsHoraEmissao,
  CodAtual, Empresa, Cliente, Intermediario, RpsIDNumero, RpsIDTipo,
  RpsStatus, SubstNumero, SubstTipo, IssRetido, ResponsavelRetencao,
  (*CodigoMunicipio, CodigoPais,*) ExigibilidadeISS,
  QuemPagaIss, MunicipioIncidencia, (*TomaCodigoMunicipio, TomaCodigoPais, TomaCep,*)
  RegimeEspecialTributacao, OptanteSimplesNacional, IncentivoFiscal,
  NFSeSrvCad,
  ValorServicos, ValorDeducoes, ValorPis, ValorCofins, ValorInss,
  ValorIr, ValorCsll, OutrasRetencoes, ValorIss, Aliquota,
  DescontoIncondicionado, DescontoCondicionado, 0,
  // INICIO LEI N� 12.741 - Lei da Transparencia
  iTotTrib, vTotTrib, vBasTrib, pTotTrib, fontTotTrib,
  tabTotTrib, verTotTrib, tpAliqTotTrib);
  // FIM LEI N� 12.741 - Lei da Transparencia
  if Codigo <> 0 then
  begin
    try
      DPS := Codigo;
      if FGeraNFSe and (FNFSeFatCab = 0) then
      begin
        if Geral.MB_Pergunta(
        'Confirma o envio da DPS ao fisco para gera��o da NFS-e?') = ID_YES then
        begin
          UnNFSe_PF_0201.GeraNFseUnica(Self, DPS, Empresa, FFormaGerarNFSe,
          FQrNFSeDPSCab, LaAviso1, LaAviso2);
          //
          if DModG.QrParamsEmpCodigo.Value <> Empresa then
            DModG.ReopenParamsEmp(Empresa);
          //
          Qry := TmySQLQuery.Create(Dmod);
          try
            //
            UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
            'SELECT Empresa, Ambiente, RpsIDSerie, ',
            'RpsIDTipo, RpsIDNumero ',
            'FROM nfsedpscab ',
            'WHERE Codigo=' + Geral.FF0(Codigo),
            '']);
            //
            //
            UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
            'SELECT NfsNumero, NfsRpsIDSerie ',
            'FROM nfsenfscab ',
            'WHERE Empresa=' + Geral.FF0(Qry.FieldByName('Empresa').AsInteger),
            'AND Ambiente=' + Geral.FF0(Qry.FieldByName('Ambiente').AsInteger),
            'AND NfsRpsIDSerie="' + Qry.FieldByName('RpsIDSerie').AsString + '" ',
            'AND NfsRpsIDTipo=' + Geral.FF0(Qry.FieldByName('RpsIDTipo').AsInteger),
            'AND NfsRpsIDNumero=' + Geral.FF0(Qry.FieldByName('RpsIDNumero').AsInteger),
            '']);
            FNumero := Qry.FieldByName('NfsNumero').AsInteger;
            FSerie  := Qry.FieldByName('NfsRpsIDSerie').AsString;
            //
          finally
            Qry.Free;
          end;
        end;
      end;
      if FQrNFSeDPSCab <> nil then
      begin
        FQrNFSeDPSCab.Close;
        FQrNFSeDPSCab.Open;
        FQrNFSeDPSCab.Locate('Codigo', Codigo, []);
      end;
    finally
      FechaForm();
    end;
  end;
end;

procedure TFmNFSe_Edit_0201.BtSaidaClick(Sender: TObject);
begin
  FechaForm();
end;

procedure TFmNFSe_Edit_0201.CalculaISS();
var
  Base, Aliq, Desc, Dedu, Brut, Valr: Double;
begin
{ Base de c�lculo:
  P�g 20 Manual 2.01 ABRASF
(Valor dos servi�os - Valor
das dedu��es - descontos
incondicionados)}
  Brut := EdValorServicos.ValueVariant;
  Desc := EdDescontoIncondicionado.ValueVariant;
  Dedu := EdValorDeducoes.ValueVariant;
  Base := (Brut - Dedu - Desc) / 100;
  Aliq := EdAliquota.ValueVariant;
  Valr := Geral.RoundC(Base * Aliq, 2);
  //
  EdValorIss.ValueVariant := Valr;
end;

procedure TFmNFSe_Edit_0201.CalculaRetencoes();
var
  // Aliq, 
  Base, Desc, Dedu, Brut, Valr: Double;
begin
{ Base de c�lculo:
  P�g 20 Manual 2.01 ABRASF
(Valor dos servi�os - Valor
das dedu��es - descontos
incondicionados)}
  Brut := EdValorServicos.ValueVariant;
  Desc := EdDescontoIncondicionado.ValueVariant;
  Dedu := EdValorDeducoes.ValueVariant;
  Base := (Brut - Dedu - Desc) / 100;
  //
  Valr := Geral.RoundC(Base * QrMeuServicoAlqPIS.Value, 2);
  EdValorPis.ValueVariant := Valr;
  //
  Valr := Geral.RoundC(Base * QrMeuServicoAlqCofins.Value, 2);
  EdValorCofins.ValueVariant := Valr;
  //
  Valr := Geral.RoundC(Base * QrMeuServicoAlqINSS.Value, 2);
  EdValorINSS.ValueVariant := Valr;
  //
  Valr := Geral.RoundC(Base * QrMeuServicoAlqIR.Value, 2);
  EdValorIR.ValueVariant := Valr;
  //
  Valr := Geral.RoundC(Base * QrMeuServicoAlqCSLL.Value, 2);
  EdValorCSLL.ValueVariant := Valr;
  //
  Valr := Geral.RoundC(Base * QrMeuServicoAlqOutrasRetencoes.Value, 2);
  EdOutrasRetencoes.ValueVariant := Valr;
  //
end;

procedure TFmNFSe_Edit_0201.CkiTotTribClick(Sender: TObject);
begin
  PnTotTrib.Visible := CkiTotTrib.Checked;
  //
end;

procedure TFmNFSe_Edit_0201.ConfiguraDiscriminacao();
  procedure SemInfoTrib();
  begin
    CkiTotTrib.Checked        := False;
    EdvBasTrib.ValueVariant   := 0.00;
    EdvTotTrib.ValueVariant   := 0.00;
    EdpTotTrib.ValueVariant   := 0.00;
    RGFontTotTrib.ItemIndex   := Integer(TIBPTaxFont.ibptaxfontNaoInfo);
    RGTpAliqTotTrib.ItemIndex := Integer(TIBPTaxOrig.ibptaxoriNaoInfo);
    RGTabTotTrib.ItemIndex    := Integer(TIBPTaxTabs.ibptaxtabNCM);
    EdVerTotTrib.ValueVariant := '0.00';
  end;
var
  InformaTrib: Boolean;
  InfTotTrib, FonteStr, LC116, UF, Chave: String;
  IndFinal: TNFeIndFinal;
  //Qry: TmySQLQuery;
  Importado: Boolean;
  vServ, vDesc: Double;
  //
  // INICIO LEI N� 12.741 - Lei da Transparencia
  iTotTrib: Integer;
  vTotTrib, vBasTrib, pTotTrib: Double;
  fontTotTrib: TIBPTaxFont;
  tabTotTrib: TIBPTaxTabs;
  verTotTrib: String;
  tpAliqTotTrib: TIBPTaxOrig;
  // FIM LEI N� 12.741 - Lei da Transparencia
begin
  // INICIO LEI N� 12.741 - Lei da Transparencia
  //  Informacao do total aproximados dos tributos na Discriminacao:
  InfTotTrib := '';
  InformaTrib := False;
  indFinal := TNFeIndFinal(QrMeuServicoindFinal.Value);
  //
  case TindFinalTribNFe(DModG.QrParamsEmpNFSe_indFinalCpl.Value) of
    infFinTribIndefinido: InformaTrib := False;
    infFinTribSoConsumidorFinal: InformaTrib :=  IndFinal = indfinalConsumidorFinal;
    infFinTribTodos: InformaTrib := True;
    else Geral.MB_Erro(
    '"InformaTribCpl" n�o implementado em "DmNFe_0000.TotaisNFe()"');
  end;
  MeDiscriminacao.Text := QrMeuServicoDiscriminacao.Value;
  if InformaTrib then
  begin
    FonteStr := '';
    //
    LC116 := Geral.SoNumero_TT(EdItemListaServico.Text);
    vServ := EdValorServicos.ValueVariant;
(*
Manual de Integra��o de Olho no Imposto - vers�o 0.0.6
  i) Quando a empresa oferece desconto incondicional, deve considerar este
  desconto para exibir a carga tribut�ria aproximada?
  Sim, deve considerar o desconto incondicional, j� que sobre ele n�o h�
  incid�ncia tribut�ria. Naturalmente, o desconto deve ser calculado item a
  item dos produtos vendidos, j� que o c�lculo da tributa��o para o atendimento
  da lei 12.741/2012 ocorre item a item.
  //
c) Quando temos desconto no valor do item como fica?
O desconto incondicional deve deduzir o valor do produto para s� depois ser
aplicada a al�quota m�dia aproximada.
*)
    vDesc := EdDescontoIncondicionado.ValueVariant + EdValorDeducoes.ValueVariant;
    //
    if (Geral.SoNumero1a9_TT(LC116) = '') or (vServ - vDesc < 0) then
    begin
      SemInfotrib();
      Exit;
    end;
    Importado := QrMeuServicoImportado.Value = 1;
    UF        := DmodG.QrPrmsEmpNFeUF_Servico.Value;
    //
    // ini 2023-12-21
    if RGFontTotTrib.ItemIndex = 1  then
    // fim 2023-12-21
      DmNFSe_0000.ObtemValorAproximadoDeTributos(DModG.QrParamsEmpNFSeCodMunici.Value,
        LC116, Importado, vServ, vDesc, (*var*) vBasTrib, pTotTrib, vTotTrib,
        tabTotTrib, verTotTrib, tpAliqTotTrib, fontTotTrib, Chave);


    CkiTotTrib.Checked        := True;
    EdvBasTrib.ValueVariant   := vBasTrib;
    EdvTotTrib.ValueVariant   := vTotTrib;
    EdpTotTrib.ValueVariant   := pTotTrib;
    RGFontTotTrib.ItemIndex   := Integer(fontTotTrib);
    RGTpAliqTotTrib.ItemIndex := Integer(tpAliqTotTrib);
    RGTabTotTrib.ItemIndex    := Integer(tabTotTrib);
    EdVerTotTrib.ValueVariant := verTotTrib;

    case TIBPTaxFont(fontTotTrib) of
      ibptaxfontNaoInfo: FonteStr := ''; //'Fonte: C�lculo pr�prio';
      ibptaxfontIBPTax: FonteStr := 'Fonte: IBPT ' + Chave;
      ibptaxfontCalculoProprio: FonteStr := 'Fonte: C�lculo pr�prio';
      else Geral.MB_Erro(
      '"fontTotTrib" n�o implementado em "FmNFSe_Edit_0201.ConfiguraDiscriminacao()"');
    end;
    //
    InfTotTrib := 'Val. Aprox. dos Tributos R$ ' +
    Geral.FFT(vTotTrib, 2, siPositivo) + ' (' +
    Geral.FFT(pTotTrib, 2, siPositivo) + '%) ' + FonteStr;
    //
    MeDiscriminacao.Text :=
      QrMeuServicoDiscriminacao.Value + sLineBreak + InftotTrib;
  end else
  begin
    SemInfoTrib();
  end;
  // FIM LEI N� 12.741 - Lei da Transparencia
end;

procedure TFmNFSe_Edit_0201.ConfiguraItensMeuServico();
{
var
  Municipio: Integer;
}
begin
  EdCodigoTributacaoMunicipio.Text := QrMeuServicoCodigoTributacaoMunicipio.Value;
  EdNumeroProcesso.Text := QrMeuServicoNumeroProcesso.Value;

  EdResponsavelRetencao.ValueVariant := QrMeuServicoResponsavelRetencao.Value;
  CBResponsavelRetencao.KeyValue := QrMeuServicoResponsavelRetencao.Value;
  if QrMeuServicoResponsavelRetencao.Value = -1 then
  begin
    EdIssRetido.ValueVariant := 2;
    CBIssRetido.KeyValue := 2;
  end else
  begin
    EdIssRetido.ValueVariant := 1;
    CBIssRetido.KeyValue := 1;
  end;

  EdItemListaServico.ValueVariant := QrMeuServicoItemListaServico.Value;
  CBItemListaServico.KeyValue := QrMeuServicoItemListaServico.Value;

  EdCodigoCnae.ValueVariant := QrMeuServicoCodigoCnae.Value;
  CBCodigoCnae.KeyValue := QrMeuServicoCodigoCnae.Value;

  EdRpsIDTipo.ValueVariant := QrMeuServicoRpsTipo.Value;
  CBRpsIDTipo.KeyValue := QrMeuServicoRpsTipo.Value;

  EdOptanteSimplesNacional.ValueVariant := QrMeuServicoOptanteSimplesNacional.Value;
  CBOptanteSimplesNacional.KeyValue := QrMeuServicoOptanteSimplesNacional.Value;

  EdIncentivoFiscal.ValueVariant := QrMeuServicoIncentivoFiscal.Value;
  CBIncentivoFiscal.KeyValue := QrMeuServicoIncentivoFiscal.Value;

  EdRegimeEspecialTributacao.ValueVariant := QrMeuServicoRegimeEspecialTributacao.Value;
  CBRegimeEspecialTributacao.KeyValue := QrMeuServicoRegimeEspecialTributacao.Value;

{
  case QrMeuServicoQuemPagaISS.Value of
    0: ObtemMunicipioDeEntidade(EdEmpresa.ValueVariant);
    1: ObtemMunicipioDeEntidade(EdCliente.ValueVariant);
    2: ObtemMunicipioDeEntidade(EdIntermediario.ValueVariant);
    9: // A definir;
    else Geral.MensagemBox('Refer�ncia do municipio n�o implementado:' +
    Geral.FF0(QrMeuServicoQuemPagaISS.Value),
    'Aviso', MB_OK+MB_ICONWARNING);
  end;
}

  EdExigibilidadeIss.ValueVariant := QrMeuServicoExigibilidadeIss.Value;
  CBExigibilidadeIss.KeyValue := QrMeuServicoExigibilidadeIss.Value;

  EdAliquota.ValueVariant := QrMeuServicoAliquota.Value;

  //MeDiscriminacao.Text := QrMeuServicoDiscriminacao.Value;
   ConfiguraDiscriminacao();
end;

procedure TFmNFSe_Edit_0201.EdAliquotaChange(Sender: TObject);
begin
  CalculaISS();
end;

procedure TFmNFSe_Edit_0201.EdDescontoIncondicionadoChange(Sender: TObject);
begin
  if EdDescontoIncondicionado.Focused then
  begin
    CalculaISS();
    CalculaRetencoes();
    ConfiguraDiscriminacao();
  end;
end;

procedure TFmNFSe_Edit_0201.EdEmpresaChange(Sender: TObject);
var
  Empresa: Integer;
begin
  (*Empresa*)    DModG.ObtemEmpresaSelecionada(EdEmpresa, Empresa);
  DModG.ReopenParamsEmp(Empresa);
end;

procedure TFmNFSe_Edit_0201.EdItemListaServicoChange(Sender: TObject);
begin
  if CBItemListaServico.Focused then
    ConfiguraDiscriminacao();
end;

procedure TFmNFSe_Edit_0201.EdNFSeSrvCadChange(Sender: TObject);
begin
  ReopenMeuServico();
  if CBNFSeSrvCad.Focused then
    ConfiguraItensMeuServico();
end;

procedure TFmNFSe_Edit_0201.EdNFSeSrvCadEnter(Sender: TObject);
begin
  FServico := EdNFSeSrvCad.ValueVariant;
end;

procedure TFmNFSe_Edit_0201.EdNFSeSrvCadExit(Sender: TObject);
begin
  if EdNFSeSrvCad.ValueVariant <> FServico then
    ConfiguraItensMeuServico();
  CalculaRetencoes();
end;

procedure TFmNFSe_Edit_0201.EdValorDeducoesChange(Sender: TObject);
begin
  if EdValorDeducoes.Focused then
  begin
    CalculaISS();
    ConfiguraDiscriminacao();
  end;
end;

procedure TFmNFSe_Edit_0201.EdValorServicosChange(Sender: TObject);
begin
  if EdValorServicos.Focused then
  begin
    CalculaISS();
    CalculaRetencoes();
    ConfiguraDiscriminacao();
  end;
end;

procedure TFmNFSe_Edit_0201.FechaForm;
begin
  if TFmNFSe_Edit_0201(Self).Owner is TApplication then
    Close
  else
    MyObjects.FormTDIFecha(TFmNFSe_Edit_0201(Self), TTabSheet(CSTabSheetChamou.Component));
end;

procedure TFmNFSe_Edit_0201.FormActivate(Sender: TObject);
begin
  if TFmNFSe_Edit_0201(Self).Owner is TApplication then
  begin
    MyObjects.CorIniComponente();
  end;
end;

procedure TFmNFSe_Edit_0201.FormCreate(Sender: TObject);
var
  Agora: TDateTime;
begin
  ImgTipo.SQLType := stLok;
  CBEmpresa.ListSource := DModG.DsEmpresas;
  FServico := 0;
  FNFSeFatCab := 0;
  FGeraNFSe := True;
  //
  FSerie  := '';
  FNumero := 0;
  //
  UMyMod.AbreQuery(QrClientes, Dmod.MyDB);
  UMyMod.AbreQuery(QrIntermediarios, Dmod.MyDB);
  UMyMod.AbreQuery(QrNFSeSrvCad, Dmod.MyDB);
  //
  UMyMod.AbreQuery(QrListServ, DmodG.AllID_DB);
  UMyMod.AbreQuery(QrCNAE21Cad, DmodG.AllID_DB);
  UMyMod.AbreQuery(QrTsResponsavelRetencao, DmodG.AllID_DB);
  UMyMod.AbreQuery(QrTsTipoRps1, DmodG.AllID_DB);
  UMyMod.AbreQuery(QrTsTipoRps2, DmodG.AllID_DB);
  UMyMod.AbreQuery(QrTsExigibilidadeIss, DmodG.AllID_DB);
  UMyMod.AbreQuery(QrTsSimNao1, DmodG.AllID_DB);
  UMyMod.AbreQuery(QrTsSimNao2, DmodG.AllID_DB);
  UMyMod.AbreQuery(QrTsSimNao3, DmodG.AllID_DB);
  UMyMod.AbreQuery(QrTsRegimeEspecialTributacao, DmodG.AllID_DB);
  //
  Agora := DModG.ObtemAgora();
  TPCompetencia.Date := Agora;
  TPRpsDataEmissao.Date := Agora;
  EdRpsHoraEmissao.Text := Geral.FDT(Agora, 100);
end;

procedure TFmNFSe_Edit_0201.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmNFSe_Edit_0201.FormShow(Sender: TObject);
begin
{$IfNDef cSkinRank} //Berlin
{$IfNDef cAlphaSkin} //Berlin
  FmMyGlyfs.DefineGlyfs(TForm(Sender));
{$EndIf}
{$EndIf}
{$IfDef cSkinRank} //Berlin
  if FmPrincipal.Sd1.Active then
    FmMyGlyfs.DefineGlyfsTDI(FmPrincipal.sd1, Sender);
{$EndIf}
{$IfDef cAlphaSkin} //Berlin
  if FmPrincipal.sSkinManager1.Active then
    FmMyGlyfs.DefineGlyfsTDI2(FmPrincipal.sSkinManager1, Sender);
{$EndIf}
end;

procedure TFmNFSe_Edit_0201.MeDiscriminacaoKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
 if Key in ([9, 10, 13]) then
   Key := 0;
end;

{
procedure TFmNFSe_Edit_0201.ObtemMunicipioDeEntidade(Entidade: Integer);
var
  Municipio: Integer;
begin
  Municipio := DmodG.ObtemMunicipioDeEntidade(Entidade);
  EdQuemPagaISS.ValueVariant := Municipio;
  CBQuemPagaISS.KeyValue := Municipio;
end;
}

procedure TFmNFSe_Edit_0201.ReopenMeuServico();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrMeuServico, Dmod.MyDB, [
  'SELECT * ',
  'FROM nfsesrvcad ',
  'WHERE Codigo=' + Geral.FF0(EdNFSeSrvCad.ValueVariant),
  'ORDER BY Nome ',
  '']);
end;

procedure TFmNFSe_Edit_0201.RGFontTotTribClick(Sender: TObject);
begin
  ConfiguraDiscriminacao();
end;

procedure TFmNFSe_Edit_0201.RGFormaGeraEEnviaClick(Sender: TObject);
begin
  BtOK.Enabled := RGFormaGeraEEnvia.ItemIndex > 0;
end;

procedure TFmNFSe_Edit_0201.SbClienteClick(Sender: TObject);
begin
  DmodG.CadastroDeEntidade(EdCliente.ValueVariant, fmcadEntidade2, fmcadEntidade2);
  //
  UMyMod.AbreQuery(QrClientes, Dmod.MyDB);
  //
  if VAR_CADASTRO > 0 then
  begin
    UMyMod.SetaCodigoPesquisado(EdCliente, CBCliente, QrClientes, VAR_CADASTRO, 'Codigo');
    EdCliente.SetFocus;
  end;
end;

procedure TFmNFSe_Edit_0201.SbCodigoCnaeClick(Sender: TObject);
var
  CNAE: String;
  CNAEInt: Integer;
begin
  VAR_CADASTROX := '';
  CNAE          := EdCodigoCnae.ValueVariant;
  //
  if CNAE <> '' then
    CNAEInt := QrCNAE21CadCodigo.Value
  else
    CNAEInt := 0;
  //
  UnNFSe_PF_0000.MostraFormCNAE21Cad(CNAEInt);
  //
  UMyMod.AbreQuery(QrCNAE21Cad, DmodG.AllID_DB);
  //
  if VAR_CADASTROX <> '' then
  begin
    UMyMod.SetaCodTxtPesquisado(EdCodigoCnae, CBCodigoCnae, QrCNAE21Cad, VAR_CADASTROX, 'CodTxt');
    EdCodigoCnae.SetFocus;
  end;
end;

procedure TFmNFSe_Edit_0201.SbIntermediarioClick(Sender: TObject);
begin
  DmodG.CadastroDeEntidade(EdCliente.ValueVariant, fmcadEntidade2, fmcadEntidade2);
  //
  UMyMod.AbreQuery(QrIntermediarios, Dmod.MyDB);
  //
  if VAR_CADASTRO > 0 then
  begin
    UMyMod.SetaCodigoPesquisado(EdCliente, CBCliente, QrIntermediarios, VAR_CADASTRO, 'Codigo');
    EdCliente.SetFocus;
  end;
end;

procedure TFmNFSe_Edit_0201.SbItemListaServicoClick(Sender: TObject);
var
  Servico: String;
  ServicoInt: Integer;
begin
  VAR_CADASTROX := '';
  Servico       := EdItemListaServico.ValueVariant;
  //
  if Servico <> '' then
    ServicoInt := QrListServCodigo.Value
  else
    ServicoInt := 0;
  //
  UnNFSe_PF_0000.MostraFormListServ(ServicoInt);
  //
  UMyMod.AbreQuery(QrListServ, DModG.AllID_DB);
  //
  if VAR_CADASTROX <> '' then
  begin
    UMyMod.SetaCodTxtPesquisado(EdItemListaServico, CBItemListaServico, QrListServ, VAR_CADASTROX, 'Codigo');
    EdItemListaServico.SetFocus;
  end;
end;

procedure TFmNFSe_Edit_0201.SbNFSeSrvCadClick(Sender: TObject);
begin
  UnNFSe_PF_0000.MostraFormNFSeSrvCad(EdNFSeSrvCad.ValueVariant);
  //
  UMyMod.AbreQuery(QrNFSeSrvCad, Dmod.MyDB);
  //
  if VAR_CADASTRO > 0 then
  begin
    UMyMod.SetaCodigoPesquisado(EdNFSeSrvCad, CBNFSeSrvCad, QrNFSeSrvCad, VAR_CADASTRO, 'Codigo');
    EdNFSeSrvCad.SetFocus;
  end;
end;

procedure TFmNFSe_Edit_0201.SbRpsIDTipoClick(Sender: TObject);
begin
//
end;

procedure TFmNFSe_Edit_0201.SbSubstTipoClick(Sender: TObject);
begin
{
  UnNFSe_PF_0000.MostraFormNFSeSrvCad();
  UMyMod.AbreQuery(QrNFSeSrvCad, Dmod.MyDB);
  if VAR_CADASTRO > 0 then
    UMyMod.SetaCodigoPesquisado(EdNFSeSrvCad, CBNFSeSrvCad, QrNFSeSrvCad, VAR_CADASTRO, 'Codigo');
}
end;

{
procedure TFmNFSe_Edit_0201.SubstituirNFSe(DPS, Empresa: Integer; LaAviso1,
  LaAviso2: TLabel);
begin
/
end;
}

end.
