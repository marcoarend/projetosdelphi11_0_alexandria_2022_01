object FmNFSe_Steps_0201: TFmNFSe_Steps_0201
  Left = 339
  Top = 185
  Caption = 'NFS-STEPS-001 :: Passos da NFS-e'
  ClientHeight = 662
  ClientWidth = 784
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel3: TPanel
    Left = 0
    Top = 48
    Width = 784
    Height = 500
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object PnLoteEnv: TPanel
      Left = 0
      Top = 0
      Width = 784
      Height = 500
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaWait: TLabel
        Left = 0
        Top = 302
        Width = 784
        Height = 24
        Align = alTop
        Alignment = taCenter
        Caption = '...'
        Font.Charset = ANSI_CHARSET
        Font.Color = clGreen
        Font.Height = -21
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        ExplicitWidth = 18
      end
      object LaExpiraCertDigital: TLabel
        Left = 0
        Top = 278
        Width = 784
        Height = 24
        Align = alTop
        Alignment = taCenter
        Caption = 'Expira'#231#227'o do Certificado Digital'
        Font.Charset = ANSI_CHARSET
        Font.Color = clRed
        Font.Height = -21
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        Visible = False
        ExplicitWidth = 316
      end
      object Panel4: TPanel
        Left = 0
        Top = 452
        Width = 784
        Height = 48
        Align = alBottom
        BevelOuter = bvNone
        TabOrder = 0
        object Panel5: TPanel
          Left = 0
          Top = 0
          Width = 268
          Height = 48
          Align = alLeft
          BevelOuter = bvNone
          TabOrder = 0
          object Label3: TLabel
            Left = 8
            Top = 4
            Width = 88
            Height = 13
            Caption = 'Posi'#231#227'o do cursor:'
          end
          object dmkEdit1: TdmkEdit
            Left = 8
            Top = 20
            Width = 80
            Height = 21
            Alignment = taRightJustify
            ReadOnly = True
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
          end
          object dmkEdit2: TdmkEdit
            Left = 88
            Top = 20
            Width = 80
            Height = 21
            Alignment = taRightJustify
            ReadOnly = True
            TabOrder = 1
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
          end
          object dmkEdit3: TdmkEdit
            Left = 180
            Top = 20
            Width = 80
            Height = 21
            Alignment = taRightJustify
            ReadOnly = True
            TabOrder = 2
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
          end
        end
        object REWarning: TRichEdit
          Left = 268
          Top = 0
          Width = 516
          Height = 48
          Align = alClient
          Font.Charset = DEFAULT_CHARSET
          Font.Color = 4227327
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          ReadOnly = True
          TabOrder = 1
        end
      end
      object PnJustificativa: TPanel
        Left = 0
        Top = 233
        Width = 784
        Height = 45
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 1
        Visible = False
        object Label7: TLabel
          Left = 4
          Top = 4
          Width = 169
          Height = 13
          Caption = 'Justificativa (m'#237'nimo 15 caracteres):'
          Color = clYellow
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clFuchsia
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentColor = False
          ParentFont = False
        end
        object EdNFeJust: TdmkEditCB
          Left = 4
          Top = 20
          Width = 56
          Height = 21
          Alignment = taRightJustify
          Color = clYellow
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clFuchsia
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          DBLookupComboBox = CBNFeJust
          IgnoraDBLookupComboBox = False
        end
        object CBNFeJust: TdmkDBLookupComboBox
          Left = 60
          Top = 20
          Width = 705
          Height = 21
          Color = clYellow
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clFuchsia
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          KeyField = 'CodUsu'
          ListField = 'Nome'
          ListSource = DsNFeJust
          ParentFont = False
          TabOrder = 1
          dmkEditCB = EdNFeJust
          UpdType = utYes
        end
      end
      object PnCancInutiliza: TPanel
        Left = 0
        Top = 136
        Width = 784
        Height = 49
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 2
        object PnChaveNFe: TPanel
          Left = 213
          Top = 0
          Width = 288
          Height = 49
          Align = alLeft
          BevelOuter = bvNone
          TabOrder = 0
          Visible = False
          object Label8: TLabel
            Left = 4
            Top = 4
            Width = 127
            Height = 13
            Caption = 'Chave de acesso da NF-e:'
            Color = clYellow
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clFuchsia
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentColor = False
            ParentFont = False
          end
          object EdchNFe: TEdit
            Left = 4
            Top = 20
            Width = 280
            Height = 21
            Color = clYellow
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clFuchsia
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            MaxLength = 255
            ParentFont = False
            ReadOnly = True
            TabOrder = 0
          end
        end
        object Panel8: TPanel
          Left = 697
          Top = 0
          Width = 87
          Height = 49
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 1
          object Label20: TLabel
            Left = 4
            Top = 4
            Width = 36
            Height = 13
            Caption = 'Vers'#227'o:'
          end
          object EdVersaoAcao: TdmkEdit
            Left = 4
            Top = 20
            Width = 69
            Height = 21
            TabOrder = 0
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '2.00'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = '2.00'
          end
        end
        object PnRecibo: TPanel
          Left = 0
          Top = 0
          Width = 213
          Height = 49
          Align = alLeft
          BevelOuter = bvNone
          TabOrder = 2
          Visible = False
          object Label6: TLabel
            Left = 4
            Top = 4
            Width = 37
            Height = 13
            Caption = 'Recibo:'
            Color = clYellow
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clFuchsia
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentColor = False
            ParentFont = False
          end
          object EdRecibo: TdmkEdit
            Left = 4
            Top = 20
            Width = 204
            Height = 21
            Color = clYellow
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clFuchsia
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            ReadOnly = True
            TabOrder = 0
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 9
            NoEnterToTab = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
          end
        end
        object PnProtocolo: TPanel
          Left = 501
          Top = 0
          Width = 116
          Height = 49
          Align = alLeft
          BevelOuter = bvNone
          TabOrder = 3
          object Label9: TLabel
            Left = 4
            Top = 4
            Width = 48
            Height = 13
            Caption = 'Protocolo:'
            Color = clYellow
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clFuchsia
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentColor = False
            ParentFont = False
          end
          object EdnProt: TEdit
            Left = 4
            Top = 20
            Width = 108
            Height = 21
            Color = clYellow
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clFuchsia
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            MaxLength = 255
            ParentFont = False
            ReadOnly = True
            TabOrder = 0
          end
        end
        object PnIDCtrl: TPanel
          Left = 617
          Top = 0
          Width = 80
          Height = 49
          Align = alLeft
          BevelOuter = bvNone
          TabOrder = 4
          object Label16: TLabel
            Left = 4
            Top = 4
            Width = 65
            Height = 13
            Caption = 'Controle NFe:'
            Color = clYellow
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clFuchsia
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentColor = False
            ParentFont = False
          end
          object EdIDCtrl: TdmkEdit
            Left = 5
            Top = 20
            Width = 68
            Height = 21
            Alignment = taRightJustify
            Color = clYellow
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clFuchsia
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            ReadOnly = True
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 9
            NoEnterToTab = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '000000000'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
          end
        end
      end
      object PnInutiliza: TPanel
        Left = 0
        Top = 185
        Width = 784
        Height = 48
        Align = alTop
        BevelOuter = bvNone
        Enabled = False
        TabOrder = 3
        Visible = False
        object Label14: TLabel
          Left = 376
          Top = 4
          Width = 73
          Height = 13
          Caption = 'CNPJ empresa:'
          Color = clYellow
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clFuchsia
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentColor = False
          ParentFont = False
        end
        object Label15: TLabel
          Left = 492
          Top = 4
          Width = 22
          Height = 13
          Caption = 'Ano:'
          Color = clYellow
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clFuchsia
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentColor = False
          ParentFont = False
        end
        object GroupBox1: TGroupBox
          Left = 88
          Top = 0
          Width = 280
          Height = 48
          Align = alLeft
          Caption = ' Intervalo de numera'#231#227'o a ser inutilizado:  '
          Color = clYellow
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clFuchsia
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentBackground = False
          ParentColor = False
          ParentFont = False
          TabOrder = 0
          object Label10: TLabel
            Left = 8
            Top = 20
            Width = 61
            Height = 13
            Caption = 'N'#186' NF inicial:'
          end
          object Label11: TLabel
            Left = 148
            Top = 20
            Width = 54
            Height = 13
            Caption = 'N'#186' NF final:'
          end
          object EdnNFIni: TdmkEdit
            Left = 72
            Top = 16
            Width = 65
            Height = 21
            Alignment = taRightJustify
            Color = clYellow
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            ValMin = '0'
            ValMax = '999999999'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
          end
          object EdnNFFim: TdmkEdit
            Left = 204
            Top = 16
            Width = 65
            Height = 21
            Alignment = taRightJustify
            Color = clYellow
            TabOrder = 1
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            ValMin = '0'
            ValMax = '999999999'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
          end
        end
        object Panel9: TPanel
          Left = 0
          Top = 0
          Width = 88
          Height = 48
          Align = alLeft
          BevelOuter = bvNone
          TabOrder = 1
          object Label12: TLabel
            Left = 4
            Top = 4
            Width = 38
            Height = 13
            Caption = 'Modelo:'
            Color = clYellow
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clFuchsia
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentColor = False
            ParentFont = False
          end
          object Label13: TLabel
            Left = 48
            Top = 4
            Width = 27
            Height = 13
            Caption = 'S'#233'rie:'
            Color = clYellow
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clFuchsia
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentColor = False
            ParentFont = False
          end
          object EdModelo: TdmkEdit
            Left = 4
            Top = 20
            Width = 40
            Height = 21
            Alignment = taRightJustify
            Color = clYellow
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clFuchsia
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 2
            NoEnterToTab = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '55'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 55
          end
          object EdSerie: TdmkEdit
            Left = 48
            Top = 20
            Width = 33
            Height = 21
            Alignment = taRightJustify
            Color = clYellow
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clFuchsia
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 1
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 1
            NoEnterToTab = False
            ValMin = '0'
            ValMax = '899'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
          end
        end
        object EdEmitCNPJ: TdmkEdit
          Left = 376
          Top = 20
          Width = 112
          Height = 21
          Color = clYellow
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clFuchsia
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 2
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 1
          NoEnterToTab = False
          ValMin = '0'
          ValMax = '899'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
        end
        object EdAno: TdmkEdit
          Left = 492
          Top = 20
          Width = 25
          Height = 21
          Alignment = taRightJustify
          Color = clYellow
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clFuchsia
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 3
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 2
          NoEnterToTab = False
          ValMin = '0'
          ValMax = '899'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '00'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
        end
      end
      object PnConfig1: TPanel
        Left = 0
        Top = 72
        Width = 784
        Height = 64
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 4
        object Panel6: TPanel
          Left = 113
          Top = 0
          Width = 404
          Height = 64
          Align = alLeft
          BevelOuter = bvNone
          TabOrder = 0
          object Label5: TLabel
            Left = 4
            Top = 12
            Width = 44
            Height = 13
            Caption = 'Empresa:'
          end
          object Label17: TLabel
            Left = 4
            Top = 40
            Width = 127
            Height = 13
            Caption = 'Serial do Certificado digital:'
          end
          object EdEmpresa: TdmkEdit
            Left = 52
            Top = 8
            Width = 32
            Height = 21
            Alignment = taRightJustify
            Enabled = False
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            OnChange = EdEmpresaChange
          end
          object EdSerialNumber: TdmkEdit
            Left = 136
            Top = 36
            Width = 260
            Height = 21
            Enabled = False
            TabOrder = 1
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            OnChange = EdEmpresaChange
          end
        end
        object Panel7: TPanel
          Left = 0
          Top = 0
          Width = 113
          Height = 64
          Align = alLeft
          BevelOuter = bvNone
          TabOrder = 1
          object RGAmbiente: TRadioGroup
            Left = 0
            Top = 0
            Width = 113
            Height = 64
            Align = alClient
            Caption = ' Ambiente: '
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ItemIndex = 0
            Items.Strings = (
              'Nenhum'
              'Produ'#231#227'o'
              'Homologa'#231#227'o')
            ParentFont = False
            TabOrder = 0
          end
        end
        object Panel10: TPanel
          Left = 593
          Top = 0
          Width = 191
          Height = 64
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 2
          object CkSoLer: TCheckBox
            Left = 8
            Top = 36
            Width = 169
            Height = 17
            Caption = 'Somente ler arquivo j'#225' gravado.'
            Enabled = False
            TabOrder = 0
          end
        end
        object PnLote: TPanel
          Left = 517
          Top = 0
          Width = 76
          Height = 64
          Align = alLeft
          BevelOuter = bvNone
          TabOrder = 3
          object Label4: TLabel
            Left = 4
            Top = 20
            Width = 24
            Height = 13
            Caption = 'Lote:'
            Color = clYellow
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clFuchsia
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentColor = False
            ParentFont = False
          end
          object EdLote: TdmkEdit
            Left = 4
            Top = 36
            Width = 65
            Height = 21
            Alignment = taRightJustify
            Color = clYellow
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clFuchsia
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 9
            NoEnterToTab = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '000000000'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
          end
        end
      end
      object Panel15: TPanel
        Left = 0
        Top = 0
        Width = 784
        Height = 72
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 5
        object RGAcao: TRadioGroup
          Left = 0
          Top = 0
          Width = 668
          Height = 72
          Align = alClient
          Caption = ' A'#231#227'o a realizar: '
          Columns = 3
          Enabled = False
          ItemIndex = 0
          Items.Strings = (
            'Gera'#231#227'o de NFS-e')
          TabOrder = 0
          OnClick = RGAcaoClick
        end
        object PnAbrirXML: TPanel
          Left = 668
          Top = 0
          Width = 116
          Height = 72
          Align = alRight
          BevelOuter = bvNone
          Enabled = False
          TabOrder = 1
          Visible = False
          object BtAbrir: TButton
            Left = 8
            Top = 4
            Width = 100
            Height = 25
            Caption = 'Abrir arquivo XML'
            Enabled = False
            TabOrder = 0
            OnClick = BtAbrirClick
          end
          object Button1: TButton
            Left = 8
            Top = 32
            Width = 100
            Height = 25
            Caption = 'Aviso'
            TabOrder = 1
            OnClick = Button1Click
          end
        end
      end
      object PageControl1: TPageControl
        Left = 0
        Top = 326
        Width = 784
        Height = 97
        ActivePage = TabSheet2
        Align = alClient
        TabOrder = 6
        ExplicitLeft = 4
        object TabSheet1: TTabSheet
          Caption = ' XML de envio '
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object RETxtEnvio: TMemo
            Left = 0
            Top = 0
            Width = 776
            Height = 69
            Align = alClient
            TabOrder = 0
            WordWrap = False
            OnChange = RETxtEnvioChange
          end
        end
        object TabSheet5: TTabSheet
          Caption = 'XML de envio (Formatado)'
          ImageIndex = 1
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object WBEnvio: TWebBrowser
            Left = 0
            Top = 0
            Width = 776
            Height = 69
            Align = alClient
            TabOrder = 0
            ExplicitWidth = 765
            ExplicitHeight = 45
            ControlData = {
              4C00000034500000220700000000000000000000000000000000000000000000
              000000004C000000000000000000000001000000E0D057007335CF11AE690800
              2B2E126208000000000000004C0000000114020000000000C000000000000046
              8000000000000000000000000000000000000000000000000000000000000000
              00000000000000000100000000000000000000000000000000000000}
          end
        end
        object TabSheet2: TTabSheet
          Caption = ' XML Retornado (Texto) '
          ImageIndex = 2
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object RETxtRetorno: TMemo
            Left = 0
            Top = 0
            Width = 776
            Height = 69
            Align = alClient
            TabOrder = 0
            WantReturns = False
            OnChange = RETxtRetornoChange
          end
        end
        object TabSheet3: TTabSheet
          Caption = ' XML Retornado (Formatado) '
          ImageIndex = 3
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object WBResposta: TWebBrowser
            Left = 0
            Top = 0
            Width = 776
            Height = 69
            Align = alClient
            TabOrder = 0
            ExplicitWidth = 765
            ExplicitHeight = 45
            ControlData = {
              4C00000034500000220700000000000000000000000000000000000000000000
              000000004C000000000000000000000001000000E0D057007335CF11AE690800
              2B2E126208000000000000004C0000000114020000000000C000000000000046
              8000000000000000000000000000000000000000000000000000000000000000
              00000000000000000100000000000000000000000000000000000000}
          end
        end
        object TabSheet4: TTabSheet
          Caption = ' Informa'#231#245'es do XML de retorno '
          ImageIndex = 4
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object MeInfo: TMemo
            Left = 0
            Top = 0
            Width = 776
            Height = 69
            Align = alClient
            ScrollBars = ssVertical
            TabOrder = 0
            WordWrap = False
          end
        end
      end
      object Panel11: TPanel
        Left = 0
        Top = 423
        Width = 784
        Height = 29
        Align = alBottom
        BevelOuter = bvNone
        Caption = 'Panel11'
        TabOrder = 7
        object Label19: TLabel
          Left = 4
          Top = 8
          Width = 68
          Height = 13
          Caption = 'Web Service: '
        end
        object EdWebService: TEdit
          Left = 76
          Top = 4
          Width = 693
          Height = 21
          ReadOnly = True
          TabOrder = 0
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 784
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 736
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 688
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 213
        Height = 32
        Caption = 'Passos da NFS-e'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 213
        Height = 32
        Caption = 'Passos da NFS-e'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 213
        Height = 32
        Caption = 'Passos da NFS-e'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 548
    Width = 784
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 780
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object Label1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
        Visible = False
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 592
    Width = 784
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 638
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sair'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
        NumGlyphs = 2
      end
    end
    object PnConfirma: TPanel
      Left = 2
      Top = 15
      Width = 636
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      Visible = False
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        TabOrder = 0
        OnClick = BtOKClick
        NumGlyphs = 2
      end
    end
  end
  object QrNFeCabA1: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM nfecaba'
      'WHERE ID=:P0'
      'AND LoteEnv=:P1'
      '')
    Left = 108
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrNFeCabA1FatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrNFeCabA1FatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrNFeCabA1Empresa: TIntegerField
      FieldName = 'Empresa'
    end
  end
  object QrNFeJust: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM nfejust'
      'WHERE :P0 & Aplicacao > 0'
      'ORDER BY Nome')
    Left = 44
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrNFeJustCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrNFeJustNome: TWideStringField
      FieldName = 'Nome'
      Size = 240
    end
    object QrNFeJustCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrNFeJustAplicacao: TIntegerField
      FieldName = 'Aplicacao'
    end
  end
  object DsNFeJust: TDataSource
    DataSet = QrNFeJust
    Left = 72
    Top = 8
  end
  object QrNFeCabA2: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM nfecaba'
      'WHERE ID=:P0'
      'AND IDCtrl=:P1'
      '')
    Left = 140
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrNFeCabA2FatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrNFeCabA2FatNum: TIntegerField
      FieldName = 'FatNum'
    end
    object QrNFeCabA2Empresa: TIntegerField
      FieldName = 'Empresa'
    end
  end
  object QrCabA: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT IDCtrl, infProt_ID, infProt_nProt'
      'FROM nfecaba '
      'WHERE Empresa=:P0'
      'AND id=:P1')
    Left = 524
    Top = 12
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrCabAIDCtrl: TIntegerField
      FieldName = 'IDCtrl'
    end
    object QrCabAinfProt_ID: TWideStringField
      FieldName = 'infProt_ID'
      Size = 30
    end
    object QrCabAinfProt_nProt: TWideStringField
      FieldName = 'infProt_nProt'
      Size = 15
    end
  end
  object Timer1: TTimer
    Enabled = False
    OnTimer = Timer1Timer
    Left = 16
    Top = 8
  end
end
