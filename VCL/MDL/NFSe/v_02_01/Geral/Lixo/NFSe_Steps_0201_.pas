unit NFSe_Steps_0201_;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, Grids, DBGrids, ComCtrls, dmkEdit,
  DB, DBClient, OmniXML, OmniXMLUtils, mySQLDbTables, DBCtrls,
  dmkDBLookupComboBox, dmkEditCB, Variants, dmkGeral, OleCtrls, SHDocVw,
  UnDmkProcFunc, NFeXMLGerencia, dmkImage, UnDmkEnums;

type
  TTipoNoXML = (tnxAttrStr, tnxTextStr);
  TTipoTagXML = (
     ttx_Id                ,
     ttx_idLote            ,
     ttx_versao            ,
     ttx_tpAmb             ,
     ttx_verAplic          ,
     ttx_cOrgao            ,
     ttx_cStat             ,
     ttx_xMotivo           ,
     ttx_cUF               ,
     ttx_dhRecbto          ,
     ttx_chNFe             ,
     ttx_nProt             ,
     ttx_digVal            ,
     ttx_ano               ,
     ttx_CNPJ              ,
     ttx_mod               ,
     ttx_serie             ,
     ttx_nNFIni            ,
     ttx_nNFFin            ,
     ttx_nRec              ,
     ttx_tMed              ,
     ttx_tpEvento          ,
     ttx_xEvento           ,
     ttx_CNPJDest          ,
     ttx_CPFDest           ,
     ttx_emailDest         ,
     ttx_nSeqEvento        ,
     ttx_dhRegEvento
     );

  TFmNFSe_Steps_0201 = class(TForm)
    QrNFeCabA1: TmySQLQuery;
    QrNFeCabA1FatID: TIntegerField;
    QrNFeCabA1FatNum: TIntegerField;
    QrNFeCabA1Empresa: TIntegerField;
    QrNFeJust: TmySQLQuery;
    DsNFeJust: TDataSource;
    QrNFeJustCodigo: TIntegerField;
    QrNFeJustNome: TWideStringField;
    QrNFeJustCodUsu: TIntegerField;
    QrNFeJustAplicacao: TIntegerField;
    QrNFeCabA2: TmySQLQuery;
    QrNFeCabA2FatID: TIntegerField;
    QrNFeCabA2FatNum: TIntegerField;
    QrNFeCabA2Empresa: TIntegerField;
    Panel3: TPanel;
    PnLoteEnv: TPanel;
    Panel4: TPanel;
    Panel5: TPanel;
    Label3: TLabel;
    dmkEdit1: TdmkEdit;
    dmkEdit2: TdmkEdit;
    dmkEdit3: TdmkEdit;
    REWarning: TRichEdit;
    PnJustificativa: TPanel;
    Label7: TLabel;
    EdNFeJust: TdmkEditCB;
    CBNFeJust: TdmkDBLookupComboBox;
    PnCancInutiliza: TPanel;
    PnChaveNFe: TPanel;
    Label8: TLabel;
    EdchNFe: TEdit;
    Panel8: TPanel;
    PnRecibo: TPanel;
    Label6: TLabel;
    EdRecibo: TdmkEdit;
    PnProtocolo: TPanel;
    Label9: TLabel;
    EdnProt: TEdit;
    PnIDCtrl: TPanel;
    Label16: TLabel;
    EdIDCtrl: TdmkEdit;
    PnInutiliza: TPanel;
    Label14: TLabel;
    Label15: TLabel;
    GroupBox1: TGroupBox;
    Label10: TLabel;
    Label11: TLabel;
    EdnNFIni: TdmkEdit;
    EdnNFFim: TdmkEdit;
    Panel9: TPanel;
    Label12: TLabel;
    Label13: TLabel;
    EdModelo: TdmkEdit;
    EdSerie: TdmkEdit;
    EdEmitCNPJ: TdmkEdit;
    EdAno: TdmkEdit;
    PnConfig1: TPanel;
    Panel6: TPanel;
    Label5: TLabel;
    Label17: TLabel;
    EdEmpresa: TdmkEdit;
    EdSerialNumber: TdmkEdit;
    Panel7: TPanel;
    RGAmbiente: TRadioGroup;
    Panel10: TPanel;
    CkSoLer: TCheckBox;
    PnLote: TPanel;
    Label4: TLabel;
    EdLote: TdmkEdit;
    Panel15: TPanel;
    RGAcao: TRadioGroup;
    PnAbrirXML: TPanel;
    BtAbrir: TButton;
    Button1: TButton;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    RETxtEnvio: TMemo;
    TabSheet2: TTabSheet;
    RETxtRetorno: TMemo;
    TabSheet3: TTabSheet;
    WBResposta: TWebBrowser;
    TabSheet4: TTabSheet;
    MeInfo: TMemo;
    LaWait: TLabel;
    TabSheet5: TTabSheet;
    WBEnvio: TWebBrowser;
    Panel11: TPanel;
    Label19: TLabel;
    EdWebService: TEdit;
    QrCabA: TmySQLQuery;
    QrCabAIDCtrl: TIntegerField;
    QrCabAinfProt_ID: TWideStringField;
    QrCabAinfProt_nProt: TWideStringField;
    Timer1: TTimer;
    Label20: TLabel;
    EdVersaoAcao: TdmkEdit;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel1: TPanel;
    Label1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    PnConfirma: TPanel;
    BtOK: TBitBtn;
    LaExpiraCertDigital: TLabel;
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure RETxtEnvioSelectionChange(Sender: TObject);
    procedure BtAbrirClick(Sender: TObject);
    procedure EdEmpresaChange(Sender: TObject);
    procedure RGAcaoClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure Button1Click(Sender: TObject);
    procedure EdUF_ServicoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure RETxtRetornoChange(Sender: TObject);
    procedure RETxtEnvioChange(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
  private
    { Private declarations }
    FMsg: String;
    FSiglas_WS: MyArrayLista;
    FPathLoteNFe, FPathLoteEvento: String;
    xmlDoc : IXMLDocument;
    xmlNode: IXMLNode;
    xmlList: IXMLNodeList;
    {
    FNomeCertificado, FSiglaUF: String;
    FtpAmb: Integer;
    }
    FAmbiente_Int, FCodigoUF_Int: Integer;
    FAmbiente_Txt, FCodigoUF_Txt: String;
    FWSDL, FURL: String;

    //function TotaisNFe(FatID, FatNum, Empresa: Integer): Boolean;
    function DefX(Codigo, ID: String; Texto: String): String;
    function DefI(Codigo, ID: String; ValMin, ValMax, Numero: Integer): Integer;
    function DefMsg(Codigo, ID, MsgExtra, Valor: String): Boolean;
    function DefineLote(var Lote: Integer): Boolean;
    function DefineEmpresa(var Empresa: Integer): Boolean;
    function DefinechNFe(var chNFe: WideString): Boolean;
    function DefinenProt(var nProt: WideString): Boolean;
    function DefineModelo(var Modelo: WideString): Boolean;
    function DefineSerie(var Serie: WideString): Boolean;
    function DefinenNFIni(var nNFIni: WideString): Boolean;
    function DefinenNFFin(var nNFFim: WideString): Boolean;
    function DefineEmitCNPJ(var EmitCNPJ: WideString): Boolean;
    function DefineIDCtrl(var IDCtrl: Integer): Boolean;
    function DefineXMLDoc(): Boolean;

    //function DefineMeuID_NFe(var Controle: Integer): Boolean;
    procedure UpdateCursorPos(Memo: TMemo);
    {
    procedure GetXMLData(ANode: TxmlNode; Control: TWinControl);
    procedure SetXMLData(ANode: TxmlNode; Control: TWinControl);
    }
    procedure HabilitaBotoes(Visivel: Boolean = True);
    procedure ReopenNFeJust(Aplicacao: Byte);
    procedure MostraTextoRetorno(Texto: WideString);
    function TextoArqDefinido(Texto: WideString): Boolean;
    function LeNoXML(No: IXMLNode; Tipo: TTipoNoXML; Tag: TTipoTagXML): WideString;
    function ObtemNomeAmbiente(Ambiente: WideString): WideString;
    function ObtemNomeDaTag(Tag: TTipoTagXML): String;
    function ObtemDescricaoDaTag(Tag: TTipoTagXML): String;
    procedure VerificaCertificadoDigital(Empresa: Integer);
    procedure ExecutaConsultaLoteNFe();
    procedure LerTextoConsultaLoteNFe();
    function LerTextoEnvioLoteNFe(): Boolean;
  public
    { Public declarations }
    Node: TxmlNode;
    FFormChamou: String;
    FXML_LoteNFe, FXML_LoteEvento: WideString;
    FSegundos, FSecWait: Integer;
    FTextoArq: WideString;
    FNaoExecutaLeitura: Boolean;
    //
    function AbreArquivoSelecionado(Arquivo: String): Boolean;
    function AbreArquivoXML(Arq, Ext: String; Assinado: Boolean): Boolean;
    function  CriaNFeNormal(Recria: Boolean; NFeStatus, FatID, FatNum,
              Empresa, IDCtrl, Cliente, FretePor, modFrete, Transporta,
              ide_indPag, FisRegCad: Integer; FreteVal, Seguro, Outros: Double;
              ide_Serie: Variant; ide_nNF: Integer; ide_dEmi, ide_dSaiEnt: TDateTime;
              ide_tpNF, ide_tpEmis: Integer; infAdic_infAdFisco, infAdic_infCpl,
              VeicTransp_Placa, VeicTransp_UF, VeicTransp_RNTC,
              Exporta_UFEmbarq, Exporta_xLocEmbarq, SQL_ITS_ITS, SQL_ITS_TOT,
              SQL_FAT_ITS, SQL_FAT_TOT, SQL_VOLUMES, SQL_CUSTOMZ: String;
              UF_Emit, UF_Dest: String; GravaCampos, cNF_Atual,
              Financeiro: Integer; ide_hSaiEnt: TTime; ide_dhCont: TDateTime;
              ide_xJust: String; emit_CRT: Integer; dest_email, Vagao, Balsa,
              Compra_XNEmp, Compra_XPed, Compra_XCont: String;
              ApenasCriaXML: Boolean): Boolean;
    function  InsUpdNFeCab(Status: Integer; SQLType: TSQLType;
              Empresa, Cliente, ModFrete, Transporta: Integer; ide_natOp: String;
              ide_indPag: Integer; ide_serie: Variant; ide_nNF: Integer;
              ide_dEmi, ide_dSaiEnt: TDateTime; ide_tpNF, ide_tpEmis, ide_finNFe,
              Retirada, Entrega, FatID, FatNum, IDCtrl: Integer;
              infAdic_infAdFisco, infAdic_infCpl,
              VeicTransp_Placa, VeicTransp_UF, VeicTransp_RNTC,
              Exporta_UFEmbarq, Exporta_xLocEmbarq,
              SQL_FAT_ITS: String; FreteExtra, SegurExtra, DespAcess: Double;
              cNF_Atual: Integer; ide_hSaiEnt: TTime; ide_dhCont:
              TDateTime; ide_xJust: String; emit_CRT: Integer; dest_email,
              Vagao, Balsa, Compra_XNEmp, Compra_XPed, Compra_XCont: String;
              VersaoNFe: Integer; NFeConjugada: Boolean = False): Boolean;
    function  PreparaEnvioDeLoteNFe(Lote, Empresa: Integer): Boolean;
    procedure PreparaVerificacaoStatusServico(Empresa: Integer);
    procedure PreparaConsultaLote(Lote, Empresa: Integer; Recibo: String);
    procedure PreparaCancelamentoDeNFe(Lote, Empresa: Integer; ChaveNFe, Protocolo: String);
    procedure PreparaInutilizaNumerosNF(Empresa, Lote, Ano, Modelo, Serie, nNFIni, nNFFim, Justif: Integer);
    procedure PreparaConsultaNFe(Empresa, IDCtrl: Integer; ChaveNFe: String);
    function  PreparaEnvioDeLoteEvento(Lote, Empresa: Integer): Boolean;

    procedure VerificaStatusServico();

    procedure ExecutaEnvioDeLoteNFe();
    procedure ExecutaEnvioDeLoteEvento();
    procedure ExecutaCancelaNFe();//MeuID_NFe: Integer);
    procedure ExecutaInutilizaNumerosNF();
    procedure ExecutaConsultaNFe();

    procedure LerTextoStatusServico();
    function LerTextoEnvioLoteEvento(): Boolean;
    procedure LerTextoCancelaNFe();//MeuID_NFe: Integer);
    procedure LerTextoInutilizaNumerosNF();
    procedure LerTextoConsultaNFe();

  end;

  var
  FmNFSe_Steps_0201: TFmNFSe_Steps_0201;

implementation

uses RichEdit, ReInit, UnInternalConsts, UnMyObjects, NFeStatusServico,
ModuleGeral, Module, UMySQLModule, NFeGeraXML_0200, ModuleNFe_0000,
NFe_Pesq_0000, NFeInut_0000, NFeLEnC_0200, MyDBCheck, Entidade2, NFeLEnU_0200;

{$R *.DFM}

const
  FXML_Load_Failure = 'Falha ao carregar o XML!';
  FVersaoNFe = 200;
var
  FverXML_versao: String;

procedure TFmNFSe_Steps_0201.BtOKClick(Sender: TObject);
begin
  REWarning.Text     := '';
  RETxtEnvio.Text    := '';
  MeInfo.Text        := '';
  MostraTextoRetorno('');
  PageControl1.ActivePageIndex := 0;
  Update;
  Application.ProcessMessages;
  //
  //FNomeCertificado := DmNFe_0000.QrFilialCerDigital.Value;
  FCodigoUF_Int  := Geral.GetCodigoUF_IBGE_DTB_da_SiglaUF(CBUF.Text);
  FAmbiente_Int  := RGAmbiente.ItemIndex;
  if FAmbiente_Int = 0 then
  begin
    Geral.MensagemBox('Defina o ambiente!', 'Aviso', MB_OK+MB_ICONWARNING);
    Exit;
  end;
  FAmbiente_Txt  := IntToStr(FAmbiente_Int);
  FCodigoUF_Txt  := IntToStr(FCodigoUF_Int);
  FWSDL          := '';
  FURL           := '';
  if FCodigoUF_Int = 0 then
  begin
    Geral.MensagemBox('Defina a UF!', 'Aviso', MB_OK+MB_ICONWARNING);
    Exit;
  end;
  //
  BtOK.Enabled := False;
  case RGAcao.ItemIndex of
    0: VerificaStatusServico();
    1:
    begin
      if CkSoLer.Checked then
        LerTextoEnvioLoteNFe()
      else
        ExecutaEnvioDeLoteNFe();
    end;
    2:
    begin
      if CkSoLer.Checked then
        LerTextoConsultaLoteNFe()
      else
        ExecutaConsultaLoteNFe();
    end;
    3:
    begin
      if CkSoLer.Checked then
        LerTextoCancelaNFe()
      else
        ExecutaCancelaNFe();
    end;
    4:
    begin
      if CkSoLer.Checked then
        LerTextoInutilizaNumerosNF()
      else
        ExecutaInutilizaNumerosNF();
    end;
    5:
    begin
      if CkSoLer.Checked then
        LerTextoConsultaNFe()
      else
        ExecutaConsultaNFe();
    end;
    6:
    begin
      if CkSoLer.Checked then
        LerTextoEnvioLoteEvento()
      else
        ExecutaEnvioDeLoteEvento();
    end;
    else Geral.MensagemBox('A a��o "' + RGAcao.Items[
    RGAcao.ItemIndex] + '" n�o est� implementada! AVISE A DERMATEK!',
    'Aviso', MB_OK+MB_ICONWARNING);
  end;
  if Trim(REWarning.Text) <> '' then
    dmkPF.LeMeuTexto(REWarning.Text);
end;

procedure TFmNFSe_Steps_0201.Button1Click(Sender: TObject);
begin
  dmkPF.LeMeuTexto(FTextoArq, FXML_Load_Failure);
end;

procedure TFmNFSe_Steps_0201.ExecutaEnvioDeLoteEvento();
var
  retWS, rtfDadosMsg, LoteStr: String;
  Lote, Empresa: Integer;
begin
  if not DefineLote(Lote) then Exit;
  if not DefineEmpresa(Empresa) then Exit;
  //
  if not CkSoLer.Checked then
  begin
    MyObjects.Informa(Label1, True, 'Enviando lote ao fisco');
    if not FileExists(FPathLoteEvento) then
    begin
      //Continua := False;
      Geral.MensagemBox('O lote "' + FPathLoteEvento +
      '" n�o foi localizado!', 'Erro', MB_OK+MB_ICONWARNING);
      Exit;
    end;
    rtfDadosMsg := MLAGeral.LoadFileToText(FPathLoteEvento);
    //RETxtEnvio.Text := rtfDadosMsg;
    if rtfDadosMsg = '' then
    begin
      //Continua := False;
      Geral.MensagemBox('O lote de eventos "' + FPathLoteEvento +
      '" foi carregado mas est� vazio!', 'Erro', MB_OK+MB_ICONWARNING);
      Exit;
    end;
    retWS :='';
    FTextoArq :='';
    Screen.Cursor := crHourGlass;
    try
      FTextoArq := FmNFeGeraXML_0200.WS_RecepcaoEvento(EdUF_Servico.Text,
      FAmbiente_Int, FCodigoUF_Int, EdSerialNumber.Text, Lote, Label1, RETxtEnvio,
      EdWebService);
      //Geral.MensagemBox(FTextoArq, 'Texto resultante', MB_OK+MB_ICONWARNING);
      //
      MyObjects.Informa(Label1, False, 'Resposta recebida com Sucesso!');
      MostraTextoRetorno(FTextoArq);
      //
      DmNFe_0000.ReopenEmpresa(Empresa);
      LoteStr := FormatFloat('000000000', Lote);
      DmNFe_0000.SalvaXML(NFE_EXT_EVE_RET_LOT_XML, LoteStr, FTextoArq, RETxtRetorno, False);
      //
      if LerTextoEnvioLoteEvento() then ;//Close;
      //
      if Pos('Erros:', FTextoArq) > 0 then
      begin
        Geral.MensagemBox('Erro na chamada do WS...' + #13+#13 +
        FTextoArq, 'Erro', MB_OK+MB_ICONERROR);
        MyObjects.Informa(Label1, False, 'Resposta recebida com Erros');
      end;
    finally
      Screen.Cursor := crDefault;
    end;
  end;
end;

procedure TFmNFSe_Steps_0201.ExecutaEnvioDeLoteNFe();
var
  retWS, rtfDadosMsg, LoteStr: String;
  Lote, Empresa: Integer;
begin
  if not DefineLote(Lote) then Exit;
  if not DefineEmpresa(Empresa) then Exit;
  //
  if not CkSoLer.Checked then
  begin
    MyObjects.Informa(Label1, True, 'Enviando lote ao fisco');
    if not FileExists(FPathLoteNFe) then
    begin
      //Continua := False;
      Geral.MensagemBox('O lote "' + FPathLoteNFe +
      '" n�o foi localizado!', 'Erro', MB_OK+MB_ICONWARNING);
      Exit;
    end;
    rtfDadosMsg := MLAGeral.LoadFileToText(FPathLoteNFe);
    //RETxtEnvio.Text := rtfDadosMsg;
    if rtfDadosMsg = '' then
    begin
      //Continua := False;
      Geral.MensagemBox('O lote de NFe "' + FPathLoteNFe +
      '" foi carregado mas est� vazio!', 'Erro', MB_OK+MB_ICONWARNING);
      Exit;
    end;
    retWS :='';
    FTextoArq :='';
    Screen.Cursor := crHourGlass;
    try
      FTextoArq := FmNFeGeraXML_0200.WS_NFeRecepcaoLote(EdUF_Servico.Text,
      FAmbiente_Int, FCodigoUF_Int, EdSerialNumber.Text, Lote, Label1, RETxtEnvio,
      EdWebService);
      //
      MyObjects.Informa(Label1, False, 'Resposta recebida com Sucesso!');
      MostraTextoRetorno(FTextoArq);
      //
      DmNFe_0000.ReopenEmpresa(Empresa);
      LoteStr := FormatFloat('000000000', Lote);
      DmNFe_0000.SalvaXML(NFE_EXT_REC_XML, LoteStr, FTextoArq, RETxtRetorno, False);
      //
      Timer1.Enabled := True;
    finally
      Screen.Cursor := crDefault;
    end;
  end;
  if LerTextoEnvioLoteNFe() then ;//Close;
end;

procedure TFmNFSe_Steps_0201.ExecutaInutilizaNumerosNF();
var
  Id, xJust, Modelo, Serie, nNFIni, nNFFin, EmitCNPJ, LoteStr: WideString;
  Empresa, Lote, K: Integer;
begin
  MyObjects.Informa(Label1, True, 'Verificando dados a serem enviados ao servidor do fisco');
  if not DefineEmpresa(Empresa) then Exit;
  if not DefineLote(Lote) then Exit;
  if not DefineModelo(Modelo) then Exit;
  if not DefineSerie(Serie) then Exit;
  if not DefinenNFIni(nNFIni) then Exit;
  if not DefinenNFFin(nNFFin) then Exit;
  if not DefineEmitCNPJ(EmitCNPJ) then Exit;
  //
  xJust := Trim(DmNFe_0000.ValidaTexto_XML(CBNFeJust.Text, 'xJust', 'xJust'));
  K := Length(xJust);
  if K < 15 then
  begin
    Geral.MensagemBox('A justificativa deve ter pelo menos 15 ' +
    'caracteres!'#13#10+'O texto "' + xJust + '" tem apenas ' + IntToStr(K)+'.',
    'Aviso', MB_OK+MB_ICONWARNING);
    Exit;
  end;
  xJust := Geral.TFD(FloatToStr(QrNFeJustCodigo.Value), 10, siPositivo) + ' - ' + xJust;
  Screen.Cursor := CrHourGlass;
  try

    {
    FTextoArq := DmNFe_0000.pInutilizarNFe(FSiglaUF, IntToStr(FtpAmb), FNomeCertificado,
      emitCNPJ, xJust, nNfIni, nNFFim, Modelo, Serie, EdAno.Text, Label1);
    }
    FTextoArq := FmNFeGeraXML_0200.WS_NFeInutilizacaoNFe(EdUF_Servico.Text,
      FAmbiente_Int, FCodigoUF_Int, EdAno.ValueVariant, Id, emitCNPJ, Modelo,
      Serie, nNFIni, nNFFin, XJust, EdSerialNumber.Text, Label1, RETxtEnvio,
      EdWebService);
    MyObjects.Informa(Label1, False, 'Resposta recebido com Sucesso!');
    //
    // Salva arquivo
    MyObjects.Informa(Label1, True, 'Salvando resposta');
    //
    //cUF := IntToStr(Geral.GetCodigoUF_IBGE_DTB_da_SiglaUF(FSiglaUF));
    DmNFe_0000.MontaID_Inutilizacao(FCodigoUF_Txt, EdAno.Text, emitCNPJ, Modelo, Serie,
      nNFIni, nNFFin, Id);
    //
    LoteStr := Id + '_' + FormatFloat('000000000', Lote);
    DmNFe_0000.SalvaXML(NFE_EXT_INU_XML, LoteStr, FTextoArq, RETxtRetorno, False);
    //
    LerTextoInutilizaNumerosNF();
    //
    if Pos('Erros:', FTextoArq) > 0 then
    begin
      Geral.MensagemBox('Erro na chamada do WS...' + #13+#13 +
      FTextoArq, 'Erro', MB_OK+MB_ICONERROR);
      MyObjects.Informa(Label1, False, 'Resposta recebida com Erros');
    end;
  finally
    Screen.Cursor := crDefault;
  end;
end;

function TFmNFSe_Steps_0201.LerTextoEnvioLoteNFe(): Boolean;
var
  Status, Codigo, Controle, Empresa: Integer;
  versao, tpAmb, verAplic, cStat, xMotivo, cUF, nRec, dhRecbto, tMed: WideString;
begin
  FverXML_versao := verEnviNFe_Versao;
  Result := False;
  if not DefineLote(Codigo) then Exit;
  if not DefineEmpresa(Empresa) then Exit;
  if not TextoArqDefinido(FTextoArq) then Exit;
  if DefineXMLDoc() then
  begin
    // Verifica se � envio de lote
    xmlNode := xmlDoc.SelectSingleNode('/retEnviNFe');
    if assigned(xmlNode) then
    begin
      Pagecontrol1.ActivePageIndex := 4;
      versao   := LeNoXML(xmlNode, tnxAttrStr, ttx_versao);
      //
      tpAmb    := LeNoXML(xmlNode, tnxTextStr, ttx_tpAmb);
      verAplic := LeNoXML(xmlNode, tnxTextStr, ttx_verAplic);
      cStat    := LeNoXML(xmlNode, tnxTextStr, ttx_cStat);
      xMotivo  := LeNoXML(xmlNode, tnxTextStr, ttx_xMotivo);
      cUF      := LeNoXML(xmlNode, tnxTextStr, ttx_cUF);
      //
      xmlNode := xmlDoc.SelectSingleNode('/retEnviNFe/infRec');
      if assigned(xmlNode) then
      begin
        nRec      := LeNoXML(xmlNode, tnxTextStr, ttx_nRec);
        dhRecbto  := LeNoXML(xmlNode, tnxTextStr, ttx_dhRecbto);
        tMed      := LeNoXML(xmlNode, tnxTextStr, ttx_tMed);
      end else begin
        nRec      := '';
        dhRecbto  := '';
        tMed      := '';
      end;
      //
      FmNFeGeraXML_0200.Ajusta_dh_NFe(dhRecbto);
      //
      Controle := DModG.BuscaProximoCodigoInt('nfectrl', 'nfelenm', '', 0);
      if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'nfelenm', False, [
        'Codigo', 'versao', 'tpAmb',
        'verAplic', 'cStat', 'xMotivo',
        'cUF', 'nRec', 'dhRecbto', 'tMed',
        'Status'
      ], ['Controle'], [
        Codigo, versao, tpAmb,
        verAplic, cStat, xMotivo,
        cUF, nRec, dhRecbto, tMed,
        DmNFe_0000.stepLoteEnvEnviado()
      ], [Controle], True) then
      begin
        if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'nfelenc', False, [
          'versao', 'tpAmb', 'verAplic',
          'cStat', 'xMotivo', 'cUF',
          'nRec', 'dhRecbto', 'tMed'
        ], ['Codigo'], [
          versao, tpAmb, verAplic,
          cStat, xMotivo, cUF,
          nRec, dhRecbto, tMed
        ], [Codigo], True) then
        begin
          Status := Geral.IMV(cStat);
          if Status <> 103 then
            Status := DmNFe_0000.stepLoteRejeitado;
          UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'nfecaba', False, [
            'Status'], ['LoteEnv', 'Empresa'], [
            Status], [Codigo, Empresa], True);
        end;
      end;
      if FFormChamou = 'FmNFeLEnc_0200' then
        FmNFeLEnc_0200.LocCod(Codigo, Codigo);
      if FFormChamou = 'FmNFeLEnU_0200' then
        FmNFeLEnU_0200.ReabreNFeLEnc(Codigo);
    end else Geral.MensagemBox('Arquivo XML n�o conhecido ou n�o implementado!',
    'Aviso', MB_OK+MB_ICONWARNING);
  end;
  Result := True;
end;

function TFmNFSe_Steps_0201.LerTextoEnvioLoteEvento(): Boolean;
var
  Status, Codigo, Controle, Empresa: Integer;
  //tMed, nRec
  versao, idLote, tpAmb, verAplic, cOrgao, cStat, xMotivo:
  WideString;
  infEvento_Id, infEvento_tpAmb, infEvento_verAplic, infEvento_cOrgao,
  infEvento_cStat, infEvento_xMotivo, infEvento_chNFe, infEvento_tpEvento,
  infEvento_xEvento, infEvento_CNPJDest, infEvento_CPFDest, infEvento_emailDest,
  infEvento_nSeqEvento, infEvento_dhRegEvento, infEvento_nProt, XML_retEve,
  infEvento_versao: WideString;
  ret_TZD_UTC: Double;
  tpEvento, nSeqEvento, SubCtrl: Integer;
  SQLType: TSQLType;
begin
  FverXML_versao := verEnviEvento_Versao;
  Result := False;
  if not DefineLote(Codigo) then Exit;
  if not DefineEmpresa(Empresa) then Exit;
  if not TextoArqDefinido(FTextoArq) then Exit;
  if DefineXMLDoc() then
  begin
    // Verifica se � envio de lote
    xmlNode := xmlDoc.SelectSingleNode('/retEnvEvento');
    if assigned(xmlNode) then
    begin
      Pagecontrol1.ActivePageIndex := 4;
      versao   := LeNoXML(xmlNode, tnxAttrStr, ttx_versao);
      //
      idLote   := LeNoXML(xmlNode, tnxTextStr, ttx_idLote);
      tpAmb    := LeNoXML(xmlNode, tnxTextStr, ttx_tpAmb);
      verAplic := LeNoXML(xmlNode, tnxTextStr, ttx_verAplic);
      cOrgao   := LeNoXML(xmlNode, tnxTextStr, ttx_cOrgao);
      cStat    := LeNoXML(xmlNode, tnxTextStr, ttx_cStat);
      xMotivo  := LeNoXML(xmlNode, tnxTextStr, ttx_xMotivo);
      //
      ///xmlNode := xmlDoc.SelectSingleNode('/retEnvEvento/retEvento');
      Controle := DModG.BuscaProximoCodigoInt('nfectrl', 'nfeeverlor', '', 0);
      if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'nfeeverlor', False, [
        'Codigo', 'versao', 'tpAmb',
        'verAplic', 'cOrgao',
        'cStat', 'xMotivo'
      ], ['Controle'], [
        Codigo, versao, tpAmb,
        verAplic, cOrgao,
        cStat, xMotivo
      ], [Controle], True) then
      begin
        if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'nfeeverloe', False, [
          'versao', 'tpAmb', 'verAplic',
          'cOrgao', 'cStat', 'xMotivo'
        ], ['Codigo'], [
          versao, tpAmb, verAplic,
          cOrgao, cStat, xMotivo
        ], [Codigo], True) then
        begin
          xmlList := xmlDoc.SelectNodes('/retEnvEvento/retEvento');
          //xmlList := xmlDoc.SelectNodes('/retEnvEvento/retEvento/infEvento');
          if xmlList.Length > 0 then
          begin
            //I := -1;
            while xmlList.Length > 0 do
            begin
              //I := I + 1;
              //testar
              infEvento_versao := LeNoXML(xmlList.Item[0], tnxAttrStr, ttx_versao);
{ TODO :       Verificar vers�o e conte�do do XML_retEve! }
              XML_retEve            := xmlList.Item[0].XML;
              //
              //xmlNode := xmlList.Item[0].SelectSingleNode('/retEnvEvento/retEvento/infEvento');
              xmlNode := xmlList.Item[0].FirstChild;
              //xmlList.Item[0].SelectSingleNode('/retEnvEvento/retEvento/infEvento', xmlNode);
              //xmlNode := xmlList.Item[0].SelectSingleNode('/retEvento/infEvento');
              infEvento_Id          := LeNoXML(xmlNode, tnxAttrStr, ttx_Id);
              //
              infEvento_tpAmb       := LeNoXML(xmlNode, tnxTextStr, ttx_tpAmb);
              infEvento_verAplic    := LeNoXML(xmlNode, tnxTextStr, ttx_verAplic);
              infEvento_cOrgao      := LeNoXML(xmlNode, tnxTextStr, ttx_cOrgao);
              infEvento_cStat       := LeNoXML(xmlNode, tnxTextStr, ttx_cStat);
              infEvento_xMotivo     := LeNoXML(xmlNode, tnxTextStr, ttx_xMotivo);
              infEvento_chNFe       := LeNoXML(xmlNode, tnxTextStr, ttx_chNFe);
              infEvento_tpEvento    := LeNoXML(xmlNode, tnxTextStr, ttx_tpEvento);
              infEvento_xEvento     := LeNoXML(xmlNode, tnxTextStr, ttx_xEvento);
              infEvento_CNPJDest    := LeNoXML(xmlNode, tnxTextStr, ttx_CNPJDest);
              infEvento_CPFDest     := LeNoXML(xmlNode, tnxTextStr, ttx_CPFDest);
              infEvento_emailDest   := LeNoXML(xmlNode, tnxTextStr, ttx_emailDest);
              infEvento_nSeqEvento  := LeNoXML(xmlNode, tnxTextStr, ttx_nSeqEvento);
              infEvento_dhRegEvento := LeNoXML(xmlNode, tnxTextStr, ttx_dhRegEvento);
              infEvento_nProt       := LeNoXML(xmlNode, tnxTextStr, ttx_nProt);
              //
              FmNFeGeraXML_0200.Ajusta_dh_NFe_UTC(infEvento_dhRegEvento, ret_TZD_UTC);
              //
              Status := Geral.IMV(infEvento_cStat);

              tpEvento := Geral.IMV(infEvento_tpEvento);
              nSeqEvento := Geral.IMV(infEvento_nSeqEvento);
              //N�O LOCALIZA DIREITO
              Controle := DmNFe_0000.EventoObtemCtrl(Codigo, tpEvento,
                nSeqEvento, infEvento_chNFe);
              //
              if Controle <> 0 then
              begin
                if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'nfeevercab', False, [
                'XML_retEve',
                'Status', 'ret_versao', 'ret_Id',
                'ret_tpAmb', 'ret_verAplic', 'ret_cOrgao',
                'ret_cStat', 'ret_xMotivo', 'ret_chNFe',
                'ret_tpEvento', 'ret_xEvento', 'ret_nSeqEvento',
                'ret_CNPJDest', 'ret_CPFDest', 'ret_emailDest',
                'ret_dhRegEvento', 'ret_TZD_UTC', 'ret_nProt'], [
                (*'FatID', 'FatNum', 'Empresa',*) 'Controle'], [
                Geral.WideStringToSQLString(XML_retEve), Status,
                (*ret_versao*)infEvento_versao, (*ret_Id*)infEvento_Id,
                (*ret_tpAmb*)infEvento_tpAmb, (*ret_verAplic*)infEvento_verAplic,
                (*ret_cOrgao*)infEvento_cOrgao, (*ret_cStat*)infEvento_cStat,
                (*ret_xMotivo*)infEvento_xMotivo, (*ret_chNFe*)infEvento_chNFe,
                (*ret_tpEvento*)infEvento_tpEvento, (*ret_xEvento*)infEvento_xEvento,
                (*ret_nSeqEvento*)infEvento_nSeqEvento, (*ret_CNPJDest*)infEvento_CNPJDest,
                (*ret_CPFDest*)infEvento_CPFDest, (*ret_emailDest*)infEvento_emailDest,
                (*ret_dhRegEvento*)infEvento_dhRegEvento, ret_TZD_UTC,
                (*ret_nProt*)infEvento_nProt], [
                (*FatID, FatNum, Empresa,*) Controle], True) then
                begin
                  // hist�rico
                  //SubCtrl := DModG.BuscaProximoCodigoInt('nfectrl', 'nfeeverret', '', 0);
                  DmNFe_0000.EventoObtemSub(Controle, infEvento_Id, SubCtrl, SQLType);

                  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'nfeeverret', False, [
                  'Controle', 'ret_versao', 'ret_Id', 'ret_tpAmb',
                  'ret_verAplic', 'ret_cOrgao', 'ret_cStat',
                  'ret_xMotivo', 'ret_chNFe', 'ret_tpEvento',
                  'ret_xEvento', 'ret_nSeqEvento', 'ret_CNPJDest',
                  'ret_CPFDest', 'ret_emailDest', 'ret_dhRegEvento',
                  'ret_TZD_UTC', 'ret_nProt'], [
                  'SubCtrl'], [
                  Controle, (*ret_versao*)infEvento_versao, (*ret_Id*)infEvento_Id,
                  (*ret_tpAmb*)infEvento_tpAmb, (*ret_verAplic*)infEvento_verAplic,
                  (*ret_cOrgao*)infEvento_cOrgao, (*ret_cStat*)infEvento_cStat,
                  (*ret_xMotivo*)infEvento_xMotivo, (*ret_chNFe*)infEvento_chNFe,
                  (*ret_tpEvento*)infEvento_tpEvento, (*ret_xEvento*)infEvento_xEvento,
                  (*ret_nSeqEvento*)infEvento_nSeqEvento, (*ret_CNPJDest*)infEvento_CNPJDest,
                  (*ret_CPFDest*)infEvento_CPFDest, (*ret_emailDest*)infEvento_emailDest,
                  (*ret_dhRegEvento*)infEvento_dhRegEvento, ret_TZD_UTC,
                  (*ret_nProt*)infEvento_nProt], [
                  SubCtrl], True) then
                  begin
                    case Geral.IMV(infEvento_tpEvento) of
                      NFe_CodEventoCCe: ;// Carta de corre��o. A princ�pio n�o faz nada!

{
                      NFe_CodEventoCan: // Cancelamento
                      begin
          if cStat = '101' then
          begin
            cJust := Geral.IMV(EdNFeJust.Text);
            if QrNFeJust.Locate('CodUsu', cJust, []) then
            begin
              cJust := QrNFeJustCodigo.Value;
              xJust := CBNFeJust.Text;
            end else begin
              cJust := 0;
              xJust := '';
            end;
            UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'nfecaba', False, [
              'Status', 'infCanc_Id', 'infCanc_tpAmb',
              'infCanc_verAplic', 'infCanc_dhRecbto', 'infCanc_nProt',
              'infCanc_digVal', 'infCanc_cStat', 'infCanc_xMotivo',
              'infCanc_cJust', 'infCanc_xJust', 'retCancNFe_versao'
            ], ['ID', 'LoteEnv'], [
              cStat, Id, tpAmb,
              verAplic, dhRecbto, nProt,
              digVal, cStat, xMotivo,
              cJust, xJust, versao
            ], [chNFe, Codigo], True);
            Geral.MensagemBox(xMotivo, 'Aviso',
              MB_OK+MB_ICONINFORMATION);
            if cJust = 0 then Geral.MensagemBox('cJust n�o definido!',
              'Erro', MB_OK+MB_ICONWARNING);
          end else
                      end;
}
                      else
                      begin
                        Geral.MensagemBox('Tipo de evento n�o implementado na fun��o:' + #13#10 +
                        'TFmNFeSteps_0200.LerTextoEnvioLoteEvento()', 'Aviso', MB_OK+MB_ICONWARNING);
                        Result := False;
                        Exit;
                      end;
                    end;
                  end;
                end;
              end;
              //
              xmlList.Remove(xmlList.Item[0]);
              //
            end;
          end
        end;
      end;
      // N�o precisa! J� Faz direto nas SQL acima!
      //DmNFe_0000.AtualizaXML_No_BD_Tudo(False);
      //
    end else Geral.MensagemBox('Arquivo XML n�o conhecido ou n�o implementado!',
    'Aviso', MB_OK+MB_ICONWARNING);
  end;
end;

procedure TFmNFSe_Steps_0201.LerTextoInutilizaNumerosNF;
var
  Controle, Empresa: Integer;
  versao, tpAmb, verAplic, cStat, xMotivo, cUF, dhRecbto, Id, nProt, xJust, Ano,
  CNPJ, Modelo, Serie, nNFIni, nNFFim: WideString;
  //
  Lote: Integer;
begin
  FverXML_versao := verNFeInutNFe_Versao;
  if not DefineEmpresa(Empresa) then Exit;
  if not TextoArqDefinido(FTextoArq) then Exit;
  if DefineXMLDoc() then
  begin
    // Verifica se � recibo de inutiliza��o de n�meros de NF
    xmlNode := xmlDoc.SelectSingleNode('/retInutNFe');
    if assigned(xmlNode) then
    begin
      versao   := LeNoXML(xmlNode, tnxAttrStr, ttx_versao);
      //
      tpAmb    := '';
      verAplic := '';
      cStat    := '';
      xMotivo  := '';
      cUF      := '';
      xmlNode := xmlDoc.SelectSingleNode('/retInutNFe/infInut');
      if assigned(xmlNode) then
      begin
        Id       := LeNoXML(xmlNode, tnxAttrStr, ttx_Id);
        tpAmb    := LeNoXML(xmlNode, tnxTextStr, ttx_tpAmb);
        verAplic := LeNoXML(xmlNode, tnxTextStr, ttx_verAplic);
        cStat    := LeNoXML(xmlNode, tnxTextStr, ttx_cStat);
        xMotivo  := LeNoXML(xmlNode, tnxTextStr, ttx_xMotivo);
        cUF      := LeNoXML(xmlNode, tnxTextStr, ttx_cUF);
        if not DefineLote(Lote) then
        begin
          FmNFeGeraXML_0200.DesmontaID_Inutilizacao(
            Id, cUF, Ano, CNPJ, Modelo, Serie, nNFIni, nNFFim);
          DmNFe_0000.QrNFeInut.Close;
          DmNFe_0000.QrNFeInut.Params[00].AsInteger := Empresa;
          DmNFe_0000.QrNFeInut.Params[01].AsString := cUF;
          DmNFe_0000.QrNFeInut.Params[02].AsString := ano;
          DmNFe_0000.QrNFeInut.Params[03].AsString := CNPJ;
          DmNFe_0000.QrNFeInut.Params[04].AsString := modelo;
          DmNFe_0000.QrNFeInut.Params[05].AsString := Serie;
          DmNFe_0000.QrNFeInut.Params[06].AsString := nNFIni;
          DmNFe_0000.QrNFeInut.Params[07].AsString := nNFFim;
          DmNFe_0000.QrNFeInut.Open;
          Lote := DmNFe_0000.QrNFeInutCodigo.Value;
          if Lote = 0 then
          begin
            Geral.MensagemBox('N�o foi poss�vel descobrir o Lote pelo ID!',
            'Erro', MB_OK+MB_ICONWARNING);
            Exit;
          end else
            Geral.MensagemBox('O Lote foi encontrado pelo ID!',
            'Mensagem', MB_OK+MB_ICONINFORMATION);
        end;
        if cStat = '102' then
        begin
          ano      := LeNoXML(xmlNode, tnxTextStr, ttx_ano);
          CNPJ     := LeNoXML(xmlNode, tnxTextStr, ttx_CNPJ);
          modelo   := LeNoXML(xmlNode, tnxTextStr, ttx_mod);
          serie    := LeNoXML(xmlNode, tnxTextStr, ttx_serie);
          nNFIni   := LeNoXML(xmlNode, tnxTextStr, ttx_nNFIni);
          nNFFim   := LeNoXML(xmlNode, tnxTextStr, ttx_nNFFin);
          dhRecbto := LeNoXML(xmlNode, tnxTextStr, ttx_dhRecbto);
          nProt    := LeNoXML(xmlNode, tnxTextStr, ttx_nProt);
          //
          FmNFeGeraXML_0200.Ajusta_dh_NFe(dhRecbto);
          //
          UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'nfeinut', False, [
          'versao', 'Id', 'tpAmb',
          'cUF', 'ano', 'CNPJ',
          //ver aqui o que fazer
          'modelo', 'Serie',
          'nNFIni', 'nNFFim', 'xJust',
          'cStat', 'xMotivo', 'dhRecbto',
          'nProt'], ['Codigo'], [
          versao, Id, tpAmb,
          cUF, ano, CNPJ,
          modelo, Serie,
          nNFIni, nNFFim, xJust,
          cStat, xMotivo, dhRecbto,
          nProt], [Lote], True);
        end;
        Controle := DModG.BuscaProximoCodigoInt('nfectrl', 'nfeinutmsg', '', 0);
        UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'nfeinutmsg', False, [
        'Codigo', 'versao', 'Id',
        'tpAmb', 'verAplic', 'cStat',
        'xMotivo', 'cUF', '_Ativo_'], [
        'Controle'], [
        Lote, versao, Id,
        tpAmb, verAplic, cStat,
        xMotivo, cUF, 1], [
        Controle], True);
      end else Geral.MensagemBox(
      'Arquivo XML n�o possui informa��es de Inutiliza��o de numera��o de NF-e!',
      'Aviso', MB_OK+MB_ICONWARNING);
    end else Geral.MensagemBox(
    'Arquivo XML n�o conhecido ou n�o implementado!',
    'Aviso', MB_OK+MB_ICONWARNING);
    //
    DmNFe_0000.AtualizaXML_No_BD_Tudo(False);
    //
  end;
  FmNFeInut_0000.LocCod(Lote, Lote);
  //Close;
end;

procedure TFmNFSe_Steps_0201.LerTextoStatusServico();
begin
  FverXML_versao := verConsStatServ_Versao;
  if not TextoArqDefinido(FTextoArq) then Exit;
  if DefineXMLDoc() then
  begin
    // Verifica se � recibo de consulta de lote
    xmlNode := xmlDoc.SelectSingleNode('/retConsStatServ');
    if assigned(xmlNode) then
    begin
      LeNoXML(xmlNode, tnxAttrStr, ttx_versao);
      LeNoXML(xmlNode, tnxTextStr, ttx_tpAmb);
      LeNoXML(xmlNode, tnxTextStr, ttx_verAplic);
      LeNoXML(xmlNode, tnxTextStr, ttx_cStat);
      LeNoXML(xmlNode, tnxTextStr, ttx_xMotivo);
      LeNoXML(xmlNode, tnxTextStr, ttx_cUF);
      LeNoXML(xmlNode, tnxTextStr, ttx_dhRecbto);
      LeNoXML(xmlNode, tnxTextStr, ttx_tMed);
      //
      Pagecontrol1.ActivePageIndex := 4;
    end else Geral.MensagemBox('Arquivo XML n�o conhecido ou n�o implementado!',
    'Aviso', MB_OK+MB_ICONWARNING);
  end;
end;

procedure TFmNFSe_Steps_0201.MostraTextoRetorno(Texto: WideString);
begin
  RETxtRetorno.Text := Texto;
end;

function TFmNFSe_Steps_0201.ObtemDescricaoDaTag(Tag: TTipoTagXML): String;
begin
  case Tag of
    ttx_Id          : Result := 'ID'                    ;
    ttx_idLote      : Result := 'ID do lote'            ;
    ttx_versao      : Result := 'Vers�o XML'            ;
    ttx_tpAmb       : Result := 'Ambiente'              ;
    ttx_verAplic    : Result := 'Vers�o do aplicativo'  ;
    ttx_cOrgao      : Result := 'Org�o'                 ;
    ttx_cStat       : Result := 'Status'                ;
    ttx_xMotivo     : Result := 'Motivo'                ;
    ttx_cUF         : Result := 'UF'                    ;
    ttx_dhRecbto    : Result := 'Data/hora recibo'      ;
    ttx_chNFe       : Result := 'Chave NF-e'            ;
    ttx_nProt       : Result := 'Protocolo'             ;
    ttx_digVal      : Result := 'Valor "digest"'        ;
    ttx_ano         : Result := 'Ano'                   ;
    ttx_CNPJ        : Result := 'CNPJ'                  ;
    ttx_mod         : Result := 'Modelo NF'             ;
    ttx_serie       : Result := 'S�rie NF'              ;
    ttx_nNFIni      : Result := 'N�mero inicial NF'     ;
    ttx_nNFFin      : Result := 'N�mero final NF'       ;
    ttx_nRec        : Result := 'N�mero do Recibo'      ;
    ttx_tMed        : Result := 'Tempo m�dio'           ;
    ttx_tpEvento    : Result := 'Tipo de evento'        ;
    ttx_xEvento     : Result := 'Descri��o do evento'   ;
    ttx_CNPJDest    : Result := 'CNPJ do destinat�rio'  ;
    ttx_CPFDest     : Result := 'CPF do destinat�rio'   ;
    ttx_emailDest   : Result := 'E-mail do destinat�rio';
    ttx_nSeqEvento  : Result := 'Sequencial do evento'  ;
    ttx_dhRegEvento : Result := 'Data/hora registro'    ;
    //
    else              Result := '? ? ? ? ?';
  end;
end;

function TFmNFSe_Steps_0201.ObtemNomeAmbiente(Ambiente: WideString): WideString;
var
  I: Integer;
begin
  if Ambiente = '' then Result := '? ? ? ? ?' else
    begin
    I := StrToInt(Ambiente);
    case I of
      1: Result := 'Produ��o';
      2: Result := 'Homologa��o';
      else Result := '*** Erro ***';
    end;
  end;
end;

function TFmNFSe_Steps_0201.ObtemNomeDaTag(Tag: TTipoTagXML): String;
begin
  case Tag of
    ttx_Id          : Result := 'Id'                ;
    ttx_idLote      : Result := 'idLote'            ;
    ttx_versao      : Result := 'versao'            ;
    ttx_tpAmb       : Result := 'tpAmb'             ;
    ttx_verAplic    : Result := 'verAplic'          ;
    ttx_cOrgao      : Result := 'cOrgao'            ;
    ttx_cStat       : Result := 'cStat'             ;
    ttx_xMotivo     : Result := 'xMotivo'           ;
    ttx_cUF         : Result := 'cUF'               ;
    ttx_dhRecbto    : Result := 'dhRecbto'          ;
    ttx_chNFe       : Result := 'chNFe'             ;
    ttx_nProt       : Result := 'nProt'             ;
    ttx_digVal      : Result := 'digVal'            ;
    ttx_ano         : Result := 'ano'               ;
    ttx_CNPJ        : Result := 'CNPJ'              ;
    ttx_mod         : Result := 'mod'               ;
    ttx_serie       : Result := 'serie'             ;
    ttx_nNFIni      : Result := 'nNFIni'            ;
    ttx_nNFFin      : Result := 'nNFFin'            ;
    ttx_nRec        : Result := 'nRec'              ;
    ttx_tMed        : Result := 'tMed'              ;
    ttx_tpEvento    : Result := 'tpEvento'          ;
    ttx_xEvento     : Result := 'xEvento'           ;
    ttx_CNPJDest    : Result := 'CNPJDest'          ;
    ttx_CPFDest     : Result := 'CPFDest'           ;
    ttx_emailDest   : Result := 'emailDest'         ;
    ttx_nSeqEvento  : Result := 'nSeqEvento'        ;
    ttx_dhRegEvento : Result := 'dhRegEvento'       ;
    //
    else           Result :=      '???';
  end;
end;

function TFmNFSe_Steps_0201.LeNoXML(No: IXMLNode; Tipo: TTipoNoXML; Tag: TTipoTagXML): WideString;
var
  Texto: WideString;
begin
  Result := '';
  case Tipo of
    tnxTextStr: Result := GetNodeTextStr(No, ObtemNomeDaTag(Tag), '');
    tnxAttrStr: Result := GetNodeAttrStr(No, ObtemNomeDaTag(Tag), '');
    else Result := '???' + ObtemNomeDaTag(Tag) + '???';
  end;
  //
  if (Tag = ttx_Versao) and (Result <> FverXML_versao) then
    Geral.MensagemBox('Vers�o do XML difere do esperado!' +
    #13#10 + 'Vers�o informada: ' + Result + #13#10 +
    'AVISE A DERMATEK!', 'Aviso', MB_OK+MB_ICONWARNING);
  if (Tag = ttx_dhRecbto) then FmNFeGeraXML_0200.Ajusta_dh_NFe(Result);
  if Result <> '' then
  begin
    case Tag of
      ttx_tpAmb: Texto := Result + ' - ' + ObtemNomeAmbiente(Result);
      ttx_tMed : Texto := Result + ' segundos';
      ttx_cUF  : Texto := Result + ' - ' +
                 Geral.GetSiglaUF_do_CodigoUF_IBGE_DTB(StrToInt(Result));
      else Texto := Result;
    end;
  end;
  //
  MeInfo.Lines.Add(ObtemDescricaoDaTag(Tag) + ' = ' + Texto);
end;

procedure TFmNFSe_Steps_0201.LerTextoCancelaNFe();//MeuID_NFe: Integer);
var
  Codigo, Controle, FatID, FatNum, Empresa: Integer;
  versao, tpAmb, verAplic, cStat, xMotivo, cUF, dhRecbto, Id, chNFe, nProt,
  digVal, Chave, xJust: WideString;
  //
  Lote, cJust: Integer;
begin
  Lote := 0;
  FverXML_versao := verCancNFe_Versao;
  if not DefineLote(Codigo) then Exit;
  if not DefineEmpresa(Empresa) then Exit;
  if not TextoArqDefinido(FTextoArq) then Exit;
  if DefineXMLDoc() then
  begin
    // Verifica se � recibo de cancelamento de NFe
    xmlNode := xmlDoc.SelectSingleNode('/retCancNFe');
    if assigned(xmlNode) then
    begin
      Pagecontrol1.ActivePageIndex := 4;
      versao   := LeNoXML(xmlNode, tnxAttrStr, ttx_versao);
      //
      tpAmb    := LeNoXML(xmlNode, tnxTextStr, ttx_tpAmb);
      verAplic := LeNoXML(xmlNode, tnxTextStr, ttx_verAplic);
      cStat    := LeNoXML(xmlNode, tnxTextStr, ttx_cStat);
      xMotivo  := LeNoXML(xmlNode, tnxTextStr, ttx_xMotivo);
      cUF      := LeNoXML(xmlNode, tnxTextStr, ttx_cUF);
      //
      xmlNode := xmlDoc.SelectSingleNode('/retCancNFe/infCanc');
      if assigned(xmlNode) then
      begin
        Id       := LeNoXML(xmlNode, tnxAttrStr, ttx_Id);
        tpAmb    := LeNoXML(xmlNode, tnxTextStr, ttx_tpAmb);
        verAplic := LeNoXML(xmlNode, tnxTextStr, ttx_verAplic);
        chNFe    := LeNoXML(xmlNode, tnxTextStr, ttx_chNFe);
        cStat    := LeNoXML(xmlNode, tnxTextStr, ttx_cStat);
        xMotivo  := LeNoXML(xmlNode, tnxTextStr, ttx_xMotivo);
        cUF      := LeNoXML(xmlNode, tnxTextStr, ttx_cUF);
        if not DefinechNFe(Chave) then Exit;
        if chNFe = Chave then
        begin
          nProt    := LeNoXML(xmlNode, tnxTextStr, ttx_nProt);
          dhRecbto := LeNoXML(xmlNode, tnxTextStr, ttx_dhRecbto);
          //
          FmNFeGeraXML_0200.Ajusta_dh_NFe(dhRecbto);
          //
          // hist�rico da NF
          QrNFeCabA1.Close;
          if DefinechNFe(Chave) then
          begin
            QrNFeCabA1.Params[00].AsString  := Chave;
            QrNFeCabA1.Params[01].AsInteger := Lote;
            QrNFeCabA1.Open;
          end;
          if (QrNFeCabA1.State <> dsInactive) and (QrNFeCabA1.RecordCount > 0) then
          begin
            FatID   := QrNFeCabA1FatID.Value;
            FatNum  := QrNFeCabA1FatNum.Value;
            Empresa := QrNFeCabA1Empresa.Value;
            //
            Controle := DModG.BuscaProximoCodigoInt(
              'nfectrl', 'nfecabamsg', '', 0);
            //
            UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'nfecabamsg', False, [
            'FatID', 'FatNum', 'Empresa', 'Solicit',
            'Id', 'tpAmb', 'verAplic',
            'dhRecbto', 'nProt', 'digVal',
            'cStat', 'xMotivo', '_Ativo_'], [
            'Controle'], [
            FatID, FatNum, Empresa, cStat,
            Id, tpAmb, verAplic,
            dhRecbto, nProt, digVal,
            cStat, xMotivo, 1], [
            Controle], True);
          end else Geral.MensagemBox('A Nota Fiscal de chave "' +
          chNFe + '" n�o foi localizada e ficar� sem o hist�rico ' +
          'desta consulta!', 'Aviso', MB_OK+MB_ICONWARNING);
          if cStat = '101' then
          begin
            cJust := Geral.IMV(EdNFeJust.Text);
            if QrNFeJust.Locate('CodUsu', cJust, []) then
            begin
              cJust := QrNFeJustCodigo.Value;
              xJust := CBNFeJust.Text;
            end else begin
              cJust := 0;
              xJust := '';
            end;
            UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'nfecaba', False, [
              'Status', 'infCanc_Id', 'infCanc_tpAmb',
              'infCanc_verAplic', 'infCanc_dhRecbto', 'infCanc_nProt',
              'infCanc_digVal', 'infCanc_cStat', 'infCanc_xMotivo',
              'infCanc_cJust', 'infCanc_xJust', 'retCancNFe_versao'
            ], ['ID', 'LoteEnv'], [
              cStat, Id, tpAmb,
              verAplic, dhRecbto, nProt,
              digVal, cStat, xMotivo,
              cJust, xJust, versao
            ], [chNFe, Codigo], True);
            Geral.MensagemBox(xMotivo, 'Aviso',
              MB_OK+MB_ICONINFORMATION);
            if cJust = 0 then Geral.MensagemBox('cJust n�o definido!',
              'Erro', MB_OK+MB_ICONWARNING);
          end else
          begin
            // N�o faz nada, NFe continua homologada
            Geral.MensagemBox(xMotivo, 'Erro',
              MB_OK+MB_ICONERROR);
          end;
        end else Geral.MensagemBox('Chave da NFe n�o confere:'+#13+#10+
        chNFe+#13#10+Chave, 'Aviso', MB_OK+MB_ICONWARNING);
      end else Geral.MensagemBox('Arquivo XML n�o possui informa��es de cancelamento!',
      'Aviso', MB_OK+MB_ICONWARNING);
    end else Geral.MensagemBox('Arquivo XML n�o conhecido ou n�o implementado!',
    'Aviso', MB_OK+MB_ICONWARNING);
    //
    DmNFe_0000.AtualizaXML_No_BD_Tudo(False);
    //
  end;
  try
    FmNFe_Pesq_0000.ReopenNFeCabA(FmNFe_Pesq_0000.QrNFeCabAIDCtrl.Value, False);
  except
    Geral.MensagemBox(
    'N�o foi poss�vel reabrir as tabelas da janela de gerenciamento de NF-e(s)',
    'Aviso', MB_OK+MB_ICONWARNING);
  end;
end;

procedure TFmNFSe_Steps_0201.LerTextoConsultaLoteNFe();
var
  Codigo, Controle, FatID, FatNum, Empresa: Integer;
  versao, tpAmb, verAplic, cStat, xMotivo, cUF, nRec, dhRecbto, tMed,
  infProt_Id, infProt_chNFe, infProt_nProt, infProt_digVal, infProt_tpAmb,
  infProt_verAplic, infProt_dhRecbto, infProt_cStat, infProt_xMotivo: WideString;
begin
  FverXML_versao := verConsReciNFe_Versao;
  if not DefineLote(Codigo) then Exit;
  if not DefineEmpresa(Empresa) then Exit;
  if not TextoArqDefinido(FTextoArq) then Exit;
  if DefineXMLDoc() then
  begin
    // Verifica se � recibo de lote de envio
    xmlNode := xmlDoc.SelectSingleNode('/retConsReciNFe');
    if assigned(xmlNode) then
    begin
      Pagecontrol1.ActivePageIndex := 4;
      versao   := LeNoXML(xmlNode, tnxAttrStr, ttx_versao);
      //
      tpAmb    := LeNoXML(xmlNode, tnxTextStr, ttx_tpAmb);
      verAplic := LeNoXML(xmlNode, tnxTextStr, ttx_verAplic);
      nRec     := LeNoXML(xmlNode, tnxTextStr, ttx_nRec);
      cStat    := LeNoXML(xmlNode, tnxTextStr, ttx_cStat);
      xMotivo  := LeNoXML(xmlNode, tnxTextStr, ttx_xMotivo);
      cUF      := LeNoXML(xmlNode, tnxTextStr, ttx_cUF);
      //
      Controle := DModG.BuscaProximoCodigoInt('nfectrl', 'nfelenm', '', 0);
      if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'nfelenm', False, [
        'Codigo', 'versao', 'tpAmb',
        'verAplic', 'cStat', 'xMotivo',
        'cUF', 'nRec', 'dhRecbto', 'tMed',
        'Status'
      ], ['Controle'], [
        Codigo, versao, tpAmb,
        verAplic, cStat, xMotivo,
        cUF, nRec, dhRecbto, tMed,
        DmNFe_0000.stepLoteEnvConsulta()
      ], [Controle], True) then
      begin
        if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'nfelenc', False, [
          'versao', 'tpAmb', 'verAplic',
          'cStat', 'xMotivo', 'cUF',
          'nRec', 'dhRecbto', 'tMed'
        ], ['Codigo'], [
          versao, tpAmb, verAplic,
          cStat, xMotivo, cUF,
          nRec, dhRecbto, tMed
        ], [Codigo], True) then
        begin
          xmlList := xmlDoc.SelectNodes('/retConsReciNFe/protNFe/infProt');
          if xmlList.Length > 0 then
          begin
            while xmlList.Length > 0 do
            begin
              {
              infProt_Id       := GetNodeAttrStr(xmlList.Item[0], 'Id', '');
              infProt_tpAmb    := GetNodeTextStr(xmlList.Item[0], 'tpAmb', '');
              infProt_verAplic := GetNodeTextStr(xmlList.Item[0], 'verAplic', '');
              infProt_chNFe    := GetNodeTextStr(xmlList.Item[0], 'chNFe', '');
              infProt_dhRecbto := GetNodeTextStr(xmlList.Item[0], 'dhRecbto', '');
              infProt_nProt    := GetNodeTextStr(xmlList.Item[0], 'nProt', '');
              infProt_digVal   := GetNodeTextStr(xmlList.Item[0], 'digVal', '');
              infProt_cStat    := GetNodeTextStr(xmlList.Item[0], 'cStat', '');
              infProt_xMotivo  := GetNodeTextStr(xmlList.Item[0], 'xMotivo', '');
              }
              //
              infProt_Id       := LeNoXML(xmlList.Item[0], tnxAttrStr, ttx_Id);
              infProt_tpAmb    := LeNoXML(xmlList.Item[0], tnxTextStr, ttx_tpAmb);
              infProt_verAplic := LeNoXML(xmlList.Item[0], tnxTextStr, ttx_verAplic);
              infProt_chNFe    := LeNoXML(xmlList.Item[0], tnxTextStr, ttx_chNFe);
              infProt_dhRecbto := LeNoXML(xmlList.Item[0], tnxTextStr, ttx_dhRecbto);
              infProt_nProt    := LeNoXML(xmlList.Item[0], tnxTextStr, ttx_nProt);
              infProt_digVal   := LeNoXML(xmlList.Item[0], tnxTextStr, ttx_digVal);
              infProt_cStat    := LeNoXML(xmlList.Item[0], tnxTextStr, ttx_cStat);
              infProt_xMotivo  := LeNoXML(xmlList.Item[0], tnxTextStr, ttx_xMotivo);
              //
              FmNFeGeraXML_0200.Ajusta_dh_NFe(dhRecbto);
              //
              if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'nfecaba', False, [
                'Status', 'infProt_Id', 'infProt_tpAmb',
                'infProt_verAplic', 'infProt_dhRecbto', 'infProt_nProt',
                'infProt_digVal', 'infProt_cStat', 'infProt_xMotivo',
                'protNFe_versao'
              ], ['ID', 'LoteEnv'], [
                infProt_cStat, infProt_Id, infProt_tpAmb,
                infProt_verAplic, infProt_dhRecbto, infProt_nProt,
                infProt_digVal, infProt_cStat, infProt_xMotivo,
                versao
              ], [infProt_chNFe, Codigo], True) then
              begin
                // hist�rico da NF
                QrNFeCabA1.Close;
                QrNFeCabA1.Params[00].AsString  := infProt_chNFe;
                QrNFeCabA1.Params[01].AsInteger := Codigo;
                QrNFeCabA1.Open;
                if QrNFeCabA1.RecordCount > 0 then
                begin
                  FatID   := QrNFeCabA1FatID.Value;
                  FatNum  := QrNFeCabA1FatNum.Value;
                  Empresa := QrNFeCabA1Empresa.Value;
                  //
                  Controle := DModG.BuscaProximoCodigoInt(
                    'nfectrl', 'nfecabamsg', '', 0);
                  //
                  UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'nfecabamsg', False, [
                  'FatID', 'FatNum', 'Empresa', 'Solicit',
                  'Id', 'tpAmb', 'verAplic',
                  'dhRecbto', 'nProt', 'digVal',
                  'cStat', 'xMotivo', '_Ativo_'], [
                  'Controle'], [
                  FatID, FatNum, Empresa, 100(*autoriza��o*),
                  infProt_Id, infProt_tpAmb, infProt_verAplic,
                  infProt_dhRecbto, infProt_nProt, infProt_digVal,
                  infProt_cStat, infProt_xMotivo, 1], [
                  Controle], True);
                end else Geral.MensagemBox('A Nota Fiscal de chave "' +
                infProt_chNFe + '" n�o foi localizada e ficara sem o hist�rico ' +
                'desta consulta!', 'Aviso', MB_OK+MB_ICONWARNING);
              end;
              //
              xmlList.Remove(xmlList.Item[0]);
              //
            end;
          end
        end;
      end;
      //
      DmNFe_0000.AtualizaXML_No_BD_Tudo(False);
      //
    end else Geral.MensagemBox('Arquivo XML n�o conhecido ou n�o implementado!',
    'Aviso', MB_OK+MB_ICONWARNING);
  end;
end;

procedure TFmNFSe_Steps_0201.LerTextoConsultaNFe();
var
  IDCtrl, Controle, FatID, FatNum, Empresa: Integer;
  tpAmb, verAplic, cStat, xMotivo, cUF, dhRecbto, Id, nProt,
  chNFe, digVal: WideString;
  //
  Status: Integer;
begin
  FverXML_versao := EdVersaoAcao.Text;
  if not DefineEmpresa(Empresa) then Exit;
  if not DefinechNFe(chNFe) then Exit;
  if not TextoArqDefinido(FTextoArq) then Exit;
  if DefineXMLDoc() then
  begin
    // Verifica se � recibo de consulta de NFe
    xmlNode := xmlDoc.SelectSingleNode('/retConsSitNFe');
    if assigned(xmlNode) then
    begin
      PageControl1.ActivePageIndex := 4;
      LeNoXML(xmlNode, tnxAttrStr, ttx_versao);
      tpAmb    := '';
      verAplic := '';
      cStat    := '';
      xMotivo  := '';
      cUF      := '';
      chNFe    := '';
      dhRecbto := '';
      nProt    := '';
      digVal   := '';
      xmlNode := xmlDoc.SelectSingleNode('/retConsSitNFe/protNFe/infProt');
      // 2012-03-29
      // Cancelamento
      if not assigned(xmlNode) then
        xmlNode := xmlDoc.SelectSingleNode('/retConsSitNFe/retCancNFe/infCanc');
      // Fim 2012-03-29
      if assigned(xmlNode) then
      begin
        Pagecontrol1.ActivePageIndex := 4;
        Id       := LeNoXML(xmlNode, tnxAttrStr, ttx_Id);
        tpAmb    := LeNoXML(xmlNode, tnxTextStr, ttx_tpAmb);
        verAplic := LeNoXML(xmlNode, tnxTextStr, ttx_verAplic);
        cStat    := LeNoXML(xmlNode, tnxTextStr, ttx_cStat);
        xMotivo  := LeNoXML(xmlNode, tnxTextStr, ttx_xMotivo);
        cUF      := LeNoXML(xmlNode, tnxTextStr, ttx_cUF);
        //
        if Geral.IMV(cStat) in ([100,101,110]) then
        begin
          //tMed     := LeNoXML(xmlNode, tnxTextStr, ttx_tMed);
          chNFe    := LeNoXML(xmlNode, tnxTextStr, ttx_chNFe);
          dhRecbto := LeNoXML(xmlNode, tnxTextStr, ttx_dhRecbto);
          nProt    := LeNoXML(xmlNode, tnxTextStr, ttx_nProt);
          digVal   := LeNoXML(xmlNode, tnxTextStr, ttx_digVal);
          //
          FmNFeGeraXML_0200.Ajusta_dh_NFe(dhRecbto);
        end;
        //
        if DefineIDCtrl(IDCtrl) then
        begin
          QrNFeCabA2.Close;
          QrNFeCabA2.Params[00].AsString  := chNFe;
          QrNFeCabA2.Params[01].AsInteger := IDCtrl;
          QrNFeCabA2.Open;
          if QrNFeCabA2.RecordCount > 0 then
          begin
            Status := Geral.IMV(cStat);
            //
            if (Status in ([100,101,110]))
            //  erro ID fica nulo or (Status > 199)
            then begin
              //if Id = chNFe then
              //begin
                UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'nfecaba', False, [
                  'Status', 'infProt_Id', 'infProt_tpAmb',
                  'infProt_verAplic', 'infProt_dhRecbto', 'infProt_nProt',
                  'infProt_digVal', 'infProt_cStat', 'infProt_xMotivo'
                ], ['ID'], [
                  cStat, Id, tpAmb,
                  verAplic, dhRecbto, nProt,
                  digVal, cStat, xMotivo
                ], [chNFe], True);
              //end;
            end;
            //
            // hist�rico da NF
            FatID   := QrNFeCabA2FatID.Value;
            FatNum  := QrNFeCabA2FatNum.Value;
            Empresa := QrNFeCabA2Empresa.Value;
            //
            Controle := DModG.BuscaProximoCodigoInt(
              'nfectrl', 'nfecabamsg', '', 0);
            //
            UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'nfecabamsg', False, [
            'FatID', 'FatNum', 'Empresa', 'Solicit',
            'Id', 'tpAmb', 'verAplic',
            'dhRecbto', 'nProt', 'digVal',
            'cStat', 'xMotivo', '_Ativo_'], [
            'Controle'], [
            FatID, FatNum, Empresa, 100(*homologa��o*),
            Id, tpAmb, verAplic,
            dhRecbto, nProt, digVal,
            cStat, xMotivo, 1], [
            Controle], True);
          end;
        end else Geral.MensagemBox('A Nota Fiscal de chave "' +
        chNFe + '" n�o foi localizada e ficar� sem o hist�rico ' +
        'desta consulta!', 'Aviso', MB_OK+MB_ICONWARNING);
        //
      end else Geral.MensagemBox('Arquivo XML n�o possui informa��es de Inutiliza��o de numera��o de NF-e!',
      'Aviso', MB_OK+MB_ICONWARNING);
    end else Geral.MensagemBox('Arquivo XML n�o conhecido ou n�o implementado!',
    'Aviso', MB_OK+MB_ICONWARNING);
  end;
  try
    if IDCtrl <> 0 then
      FmNFe_Pesq_0000.ReopenNFeCabA(IDCtrl, False);
  except
    //
  end;
  //Close;
end;

procedure TFmNFSe_Steps_0201.BtAbrirClick(Sender: TObject);
var
  IniDir, Arquivo: String;
begin
  IniDir := ExtractFileDir('C:\Dermatek\NFe\');
  Arquivo := '';
  if MyObjects.FileOpenDialog(Self, IniDir, Arquivo,
  'Selecione o arquivo XML', '', [], Arquivo) then
    AbreArquivoSelecionado(Arquivo);
end;

function TFmNFSe_Steps_0201.AbreArquivoSelecionado(Arquivo: String): Boolean;
begin
  FTextoArq := MLAGeral.LoadFileToText(Arquivo);
  MostraTextoRetorno(FTextoArq);
  if FTextoArq <> '' then
    HabilitaBotoes();
  Result := true;
end;

function TFmNFSe_Steps_0201.AbreArquivoXML(Arq, Ext: String; Assinado: Boolean): Boolean;
var
  Dir, Arquivo: String;
begin
  Result := False;
  FTextoArq := '';
  if not DmNFe_0000.ObtemDirXML(Ext, Dir, Assinado) then
    Exit;
  Arquivo := Dir + Arq + Ext;
  if FileExists(Arquivo) then
  begin
    FTextoArq := MLAGeral.LoadFileToText(Arquivo);
    MostraTextoRetorno(FTextoArq);
    Result := FTextoArq <> '';
    if Result then HabilitaBotoes();
  end else Geral.MensagemBox('Arquivo n�o localizado "' +
  Arquivo + '"!', 'Erro', MB_OK+MB_ICONERROR);
end;

procedure TFmNFSe_Steps_0201.BtSaidaClick(Sender: TObject);
begin
  Close;
  //Destroy;
end;

procedure TFmNFSe_Steps_0201.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmNFSe_Steps_0201.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  {
  if FFormChamou = 'FmNFeLEnc' then
    FmNFeLEnc.LocCod(EdLote.ValueVariant, EdLote.ValueVariant);
  }
end;

procedure TFmNFSe_Steps_0201.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  FTextoArq :='';
  FNaoExecutaLeitura := False;
  FSecWait := 15;
  Timer1.Enabled := False;
  FSegundos := 0;
  //
  PageControl1.ActivePageIndex := 0;
  FSiglas_WS  := Geral.SiglasWebService();
  FFormChamou := '';
  //
  MyObjects.Informa(Label1, True, 'Configurando conforme solicita��o');
  if ((VAR_LOGIN = 'MASTER') and (VAR_SENHA = CO_MASTER)) then
  begin
    EdLote.Enabled     := True;
    EdEmpresa.Enabled  := True;
    EdRecibo.Enabled   := True;
    //
    BtAbrir.Enabled    := True;
  end;
  (*
  Grade.Cells[00,00] := 'Linha';
  Grade.Cells[00,01] := '#';
  Grade.Cells[00,02] := 'ID';
  Grade.Cells[00,03] := 'Campo';
  Grade.Cells[00,04] := 'Descri��o';
  Grade.Cells[00,05] := 'Valor';
  Grade.Cells[00,06] := 'Adic.';
  Grade.Cells[00,07] := 'Observa��o';
  //
  Grade.ColWidths[00] := 40;
  Grade.ColWidths[01] := 40;
  Grade.ColWidths[02] := 40;
  Grade.ColWidths[03] := 80;
  Grade.ColWidths[04] := 300;
  Grade.ColWidths[05] := 300;
  Grade.ColWidths[06] := 28;
  Grade.ColWidths[07] := 600;
  *)
  //
end;

procedure TFmNFSe_Steps_0201.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [Label1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmNFSe_Steps_0201.HabilitaBotoes(Visivel: Boolean = True);
begin
  PnConfirma.Visible := Visivel;
end;

function TFmNFSe_Steps_0201.InsUpdNFeCab(Status: Integer; SQLType: TSQLType;
  Empresa, Cliente, ModFrete, Transporta: Integer; ide_natOp: String;
  ide_indPag: Integer; ide_serie: Variant; ide_nNF: Integer; ide_dEmi,
  ide_dSaiEnt: TDateTime; ide_tpNF, ide_tpEmis, ide_finNFe, Retirada, Entrega,
  FatID, FatNum, IDCtrl: Integer; infAdic_infAdFisco, infAdic_infCpl,
  VeicTransp_Placa, VeicTransp_UF, VeicTransp_RNTC, Exporta_UFEmbarq,
  Exporta_xLocEmbarq, SQL_FAT_ITS: String; FreteExtra, SegurExtra,
  DespAcess: Double; cNF_Atual: Integer; ide_hSaiEnt: TTime; ide_dhCont:
  TDateTime; ide_xJust: String; emit_CRT: Integer; dest_email, Vagao, Balsa,
  Compra_XNEmp, Compra_XPed, Compra_XCont: String; VersaoNFe: Integer;
  NFeConjugada: Boolean = False): Boolean;
var
  NFe_Id: String;
  ide_cNF, ide_cUF, ide_mod: Integer;
  versao: Double;
  //
  ide_tpAmb, ide_cMunFG, ide_tpImp, emit_cMun, emit_cPais, dest_cMun,
  dest_cPais, retirada_cMun, entrega_cMun, _Ativo_: Integer;
  ide_verProc, emit_CNPJ, emit_CPF, emit_xNome, emit_xFant, emit_xLgr, emit_nro,
  emit_xCpl, emit_xBairro, emit_xMun, emit_UF, emit_CEP, emit_xPais, emit_fone,
  emit_IE, emit_IEST, emit_IM, emit_CNAE, dest_CNPJ, dest_CPF, dest_xNome,
  dest_xLgr, dest_nro, dest_xCpl, dest_xBairro, dest_xMun, dest_UF, dest_CEP,
  dest_xPais, dest_fone, dest_IE, dest_ISUF, retirada_CNPJ, retirada_xLgr,
  retirada_nro, retirada_xCpl, retirada_xBairro, retirada_xMun, retirada_UF,
  entrega_CNPJ, entrega_xLgr, entrega_nro, entrega_xCpl, entrega_xBairro,
  entrega_xMun, entrega_UF, ide_cDV, DataFiscal: String;
  retirada_, entrega_: Boolean;
  //
  Transporta_CNPJ, Transporta_CPF, Transporta_XNome, Transporta_IE,
  Transporta_XEnder, Transporta_XMun, Transporta_UF: String;
  RetTransp_vServ, RetTransp_vBCRet, RetTransp_PICMSRet,
  RetTransp_vICMSRet: Double;
  RetTransp_CFOP, RetTransp_CMunFG,
  //VeicTransp_Placa, VeicTransp_UF, VeicTransp_RNTC,
  Cobr_Fat_NFat: String;
  Cobr_Fat_vOrig, Cobr_Fat_vDesc, Cobr_Fat_vLiq: Double;
  CodInfoEmit, CodInfoDest: Integer;
begin
  DmNFe_0000.ReopenNFeLayI();
  Result := False;
  if not DmNFe_0000.ExcluiNfe(Status, FatID, FatNum, Empresa, False) then Exit;
  //
  FMsg := '';
  if not DmNFe_0000.ReopenOpcoesNFe(Empresa, True) then Exit;
  //
  DmNFe_0000.ReopenDest(Cliente);
  if DmNFe_0000.QrDestCodigo.Value <> Cliente then
    FMsg := 'Destinat�rio n�o localizado!';
  //
  DmNFe_0000.ReopenEmpresa(Empresa);
  if DmNFe_0000.QrEmpresaCodigo.Value <> Empresa then
    FMsg := 'Empresa n�o localizada!';
  // S� simples federal! - Parei aqui
  if DmNFe_0000.QrFilialSimplesFed.Value = 0 then
    Geral.MensagemBox('C�lculo de impostos em fase de implementa��o!' + #13#10 +
    'Verifique a exatid�o dos valores e comunique � DERMATEK qualquer erro!',
    'Aviso', MB_OK+MB_ICONWARNING);
  //
  if Transporta <> 0 then
  begin
    DmNFe_0000.ReopenTransporta(Transporta);
    if DmNFe_0000.QrTransportaCodigo.Value <> Transporta then
      FMsg := 'Transportadora n�o localizada!';
  end;
  //
  if Empresa = 0 then
    FMsg := 'Empresa inv�lida!';
  //
  if Cliente = 0 then
    FMsg := 'Cliente inv�lida!';
  //
  versao := DmNFe_0000.QrOpcoesNFeversao.Value;
  if Trunc((versao * 100) + 0.5) <> VersaoNFe then
    FMsg := 'Vers�o NF-e n�o suportada neste aplicativo: ' +
    FloatToStr(Versao);
  //
  ide_cUF := Geral.IMV(DmNFe_0000.QrEmpresaDTB_UF.Value);
  if (ide_cUF < FMinUF_IBGE) or (ide_cUF > FMaxUF_IBGE) then
    FMsg := 'C�digo IBGE da UF inv�lido: ' + DmNFe_0000.QrEmpresaDTB_UF.Value;
  //
  ide_mod     := DefI('10', 'B06', 55, 55, DmNFe_0000.QrOpcoesNFeide_mod.Value);
  {  Errado!
  if ide_tpNF = 0 then // entrada
    ide_cMunFG  := DefI('16', 'B12', 1, 9999999, QrClienteCodMunici.Value)
  else // saida
  }
    ide_cMunFG  := DefI('16', 'B12', 1, 9999999, Trunc(DmNFe_0000.QrEmpresaCodMunici.Value));
  //
  ide_tpImp   := DefI('25', 'B21', 1, 2, DmNFe_0000.QrOpcoesNFeide_tpImp.Value);
  ide_tpAmb   := DefI('28', 'B24', 1, 2, DmNFe_0000.QrOpcoesNFeide_tpAmb.Value);
  ide_verProc := DefX('29b', 'B27', DBCheck.Obtem_verProc);
  //
  //evitar cpf no cnpj
  if DmNFe_0000.QrEmpresaTipo.Value = 0 then
  begin
    emit_CNPJ := Geral.SoNumero_TT(DmNFe_0000.QrEmpresaCNPJ.Value);
    if Length(emit_CNPJ) < 14 then
      FMsg := 'CNPJ com tamanho inv�lido para o emitente!';
    emit_CNPJ := Geral.CompletaString(emit_CNPJ, '0', 14, taRightJustify, False);
  end else begin
    emit_CPF := Geral.SoNumero_TT(DmNFe_0000.QrEmpresaCPF.Value);
    if Length(emit_CPF) < 11 then
      FMsg := 'CPF com tamanho inv�lido para o emitente!';
    emit_CNPJ := Geral.CompletaString(emit_CNPJ, '0', 11, taRightJustify, False);
  end;
  emit_xNome  := DefX('32', 'C03', DmNFe_0000.QrEmpresaNO_ENT.Value);
  emit_xFant  := DefX('33', 'C04', DmNFe_0000.QrEmpresaFANTASIA.Value);
  emit_xLgr   := DefX('35', 'C06', Trim(DmNFe_0000.QrEmpresaNO_LOGRAD.Value + ' ' + DmNFe_0000.QrEmpresaRua.Value));
  emit_nro    := DefX('36', 'C07', Geral.FormataNumeroDeRua(
                 DmNFe_0000.QrEmpresaRua.Value, FormatFloat('0',
                 DmNFe_0000.QrEmpresaNumero.Value), False));
  emit_xCpl   := DefX('37', 'C08', DmNFe_0000.QrEmpresaCOMPL.Value);
  emit_xBairro:= DefX('38', 'C09', DmNFe_0000.QrEmpresaBAIRRO.Value);
  emit_cMun   := DefI('39', 'C10', 1, 9999999, Trunc(DmNFe_0000.QrEmpresaCodMunici.Value));
  emit_xMun   := DefX('40', 'C11', DmNFe_0000.QrEmpresaNO_Munici.Value);
  emit_UF     := DefX('41', 'C12', DmNFe_0000.QrEmpresaNO_UF.Value);
  emit_CEP    := DefX('42', 'C13', Geral.FormataCEP_TT(Geral.FFT(DmNFe_0000.QrEmpresaCEP.Value, 0, siPositivo), '', '00000000'));
  emit_cPais  := DefI('43', 'C14', 1, 9999, Trunc(DmNFe_0000.QrEmpresaCodiPais.Value));
  emit_xPais  := DefX('44', 'C15', DmNFe_0000.QrEmpresaNO_Pais.Value);
  emit_fone   := DefX('45', 'C16', Geral.FormataTelefone_TT_NFe(DmNFe_0000.QrEmpresaTe1.Value));
  emit_IE     := DefX('46', 'C17', Trim(Uppercase(DmNFe_0000.QrEmpresaIE.Value)));
  // evitar erro no XML
  if (DmNFe_0000.QrEmpresaTipo.Value = 0) and (Trim(emit_IE) = '') then
    FMsg := 'Para pessoa jur�dica deve ser informada a sua Inscri��o Estadual. '
    + 'Caso seja isenta, informe a palavra "ISENTO" no cadastro da empresa ' +
    emit_CNPJ + ' - ' + emit_xNome + '.';
  emit_IEST   := DefX('47', 'C18', Trim(Uppercase(DmNFe_0000.QrEmpresaIEST.Value)));
  // 2012-09-16
  if NFeConjugada then
  begin
    emit_IM     := DefX('48', 'C19', DmNFe_0000.QrEmpresaNIRE.Value);
    emit_CNAE   := DefX('49', 'C20', DmNFe_0000.QrEmpresaCNAE.Value);
  end else
  begin
    emit_IM     := '';
    emit_CNAE   := '';
  end;
  // FIM 2012-09-16
  //
  // verificar se o municipio pertence � UF
  if (emit_cMun div 100000) <> Geral.GetCodigoUF_IBGE_DTB_da_SiglaUF(emit_UF) then
    FMsg := 'O munic�pio ' + FormatFloat('0000000', emit_cMun) +
    ' n�o pertence � UF ' + emit_UF + ' (Munic�pio do emitente)!';
  //
  //evitar cpf no cnpj
  if DmNFe_0000.QrDestTipo.Value = 0 then
  begin
    dest_CNPJ := Geral.SoNumero_TT(DmNFe_0000.QrDestCNPJ.Value);
    if Length(dest_CNPJ) < 14 then
      FMsg := 'CNPJ "' + DmNFe_0000.QrDestCNPJ.Value + '" com tamanho inv�lido para o cliente!';
    dest_CNPJ := Geral.CompletaString(dest_CNPJ, '0', 14, taRightJustify, False);
  end else begin
    dest_CPF := Geral.SoNumero_TT(DmNFe_0000.QrDestCPF.Value);
    if Length(dest_CPF) < 11 then
      FMsg := 'CPF "' + DmNFe_0000.QrDestCPF.Value + '" com tamanho inv�lido para o cliente!';
    dest_CPF := Geral.CompletaString(dest_CPF, '0', 11, taRightJustify, False);
  end;
  dest_xNome  := DefX('65', 'E04', DmNFe_0000.QrDestNO_ENT.Value);
  dest_xLgr   := DefX('67', 'E06', Trim(DmNFe_0000.QrDestNO_LOGRAD.Value + ' ' + DmNFe_0000.QrDestRua.Value));
  dest_nro    := DefX('68', 'E07', Geral.FormataNumeroDeRua(
                 DmNFe_0000.QrDestRua.Value, FormatFloat('0', DmNFe_0000.QrDestNumero.Value), False));
  dest_xCpl   := DefX('69', 'E08', DmNFe_0000.QrDestCOMPL.Value);
  dest_xBairro:= DefX('70', 'E09', DmNFe_0000.QrDestBAIRRO.Value);
  dest_cMun   := DefI('71', 'E10', 1, 9999999, Trunc(DmNFe_0000.QrDestCodMunici.Value));
  dest_xMun   := DefX('72', 'E11', DmNFe_0000.QrDestNO_Munici.Value);
  dest_UF     := DefX('73', 'E12', DmNFe_0000.QrDestNO_UF.Value);
  dest_CEP    := DefX('74', 'E13', Geral.FormataCEP_TT(IntToStr(Trunc(DmNFe_0000.QrDestCEP.Value)), '', '00000000'));
  dest_cPais  := DefI('75', 'E14', 1, 9999, Trunc(DmNFe_0000.QrDestCodiPais.Value));//1058; //
  dest_xPais  := DefX('76', 'E15', DmNFe_0000.QrDestNO_Pais.Value);//'BRASIL'; //
  dest_fone   := DefX('77', 'E16', Geral.FormataTelefone_TT_NFe(DmNFe_0000.QrDestTe1.Value));
  dest_IE     := DefX('78', 'E17', Trim(Uppercase(DmNFe_0000.QrDestIE.Value)));
  // evitar erro no XML
  if (DmNFe_0000.QrDestTipo.Value = 0) and (Trim(dest_IE) = '') then
    FMsg := 'Para pessoa jur�dica deve ser informada a sua Inscri��o Estadual. '
    + 'Caso seja isenta, informe a palavra "ISENTO" no cadastro da empresa ' +
    dest_CNPJ + ' - ' + dest_xNome + '.';
  dest_ISUF   := DefX('79', 'E18', Trim(Uppercase(DmNFe_0000.QrDestSUFRAMA.Value)));
  //
  // verificar se o municipio pertence � UF
  if (dest_cMun div 100000) <> Geral.GetCodigoUF_IBGE_DTB_da_SiglaUF(dest_UF) then
    FMsg := 'O munic�pio ' + FormatFloat('0000000', dest_cMun) +
    ' n�o pertence � UF ' + dest_UF + ' (Munic�pio do destinat�rio)!';

  retirada_ := (Retirada <> 0) and (Retirada <> Empresa);
  if retirada_ then
  begin
    retirada_CNPJ     := '';  // Parei Aqui
    retirada_xLgr     := '';  // ...
    retirada_nro      := '';  // ...
    retirada_xCpl     := '';  // ...
    retirada_xBairro  := '';  // ...
    retirada_cMun     := 0;   // ...
    retirada_xMun     := '';  // ...
    retirada_UF       := '';  // ...
  end else begin
    retirada_CNPJ     := '';  // ...
    retirada_xLgr     := '';  // ...
    retirada_nro      := '';  // ...
    retirada_xCpl     := '';  // ...
    retirada_xBairro  := '';  // ...
    retirada_cMun     := 0;   // ...
    retirada_xMun     := '';  // ...
    retirada_UF       := '';  // ...
  end;
  //
  entrega_ := (Entrega <> 0) and (Entrega <> Cliente);
  if entrega_ or (DmNFe_0000.QrDestL_Ativo.Value = 1) then
  begin
    entrega_CNPJ      := Geral.SoNumero_TT(DmNFe_0000.QrDestL_CNPJ.Value);
    entrega_xLgr      := DefX('91', 'G03', Trim(DmNFe_0000.QrDestNO_LLOGRAD.Value + ' ' + DmNFe_0000.QrDestLRua.Value));
    entrega_nro       := DefX('92', 'G04', Geral.FormataNumeroDeRua(
                         DmNFe_0000.QrDestRua.Value, FormatFloat('0', DmNFe_0000.QrDestLNumero.Value), False));
    entrega_xCpl      := DefX('93', 'G05', DmNFe_0000.QrDestLCompl.Value);
    entrega_xBairro   := DefX('94', 'G06', DmNFe_0000.QrDestLBairro.Value);
    entrega_cMun      := DefI('95', 'G07', 1, 9999999, DmNFe_0000.QrDestLCodMunici.Value);
    entrega_xMun      := DefX('96', 'G08', DmNFe_0000.QrDestNO_LMunici.Value);
    entrega_UF        := DefX('97', 'G09', DmNFe_0000.QrDestNO_LUF.Value);
    if Length(entrega_CNPJ) < 14 then
      FMsg := 'CNPJ "' + DmNFe_0000.QrDestCNPJ.Value + '" com tamanho inv�lido para o endere�o de entrega!';
  end else begin
    entrega_CNPJ     := '';
    entrega_xLgr     := '';
    entrega_nro      := '';
    entrega_xCpl     := '';
    entrega_xBairro  := '';
    entrega_cMun     := 0;
    entrega_xMun     := '';
    entrega_UF       := '';
  end;
  //
  //
  //  TOTAIS da NFe somente ap�s inclus�o dos itens
  //
  //evitar cpf no cnpj
  if Transporta <> 0 then
  begin
    if DmNFe_0000.QrTransportaTipo.Value = 0 then
    begin
      transporta_CNPJ := Geral.SoNumero_TT(DmNFe_0000.QrTransportaCNPJ.Value);
      if Length(transporta_CNPJ) < 14 then
        FMsg := 'CNPJ "' + DmNFe_0000.QrTransportaCNPJ.Value +
        '" com tamanho inv�lido para a transportadora "' +
        DmNFe_0000.QrTransportaNO_ENT.Value + '"!';
      transporta_CNPJ := Geral.CompletaString(transporta_CNPJ, '0', 14, taRightJustify, False);
    end else begin
      transporta_CPF := Geral.SoNumero_TT(DmNFe_0000.QrTransportaCPF.Value);
      if Length(transporta_CPF) < 11 then
        FMsg := 'CPF "' + DmNFe_0000.QrTransportaCPF.Value +
        '" com tamanho inv�lido para a transportadora "' +
        DmNFe_0000.QrTransportaNO_ENT.Value + '"!';
      transporta_CNPJ := Geral.CompletaString(transporta_CNPJ, '0', 11, taRightJustify, False);
    end;
    transporta_xNome  := DmNFe_0000.QrTransportaNO_ENT.Value;
    transporta_IE     := Uppercase(DmNFe_0000.QrTransportaIE.Value);
    if transporta_IE <> 'ISENTO' then
      transporta_IE := Geral.SoNumero_TT(transporta_IE);
    transporta_xEnder := DmNFe_0000.QrTransportaENDERECO.Value;
    transporta_xMun   := DmNFe_0000.QrTransportaNO_Munici.Value;
    transporta_UF     := DmNFe_0000.QrTransportaNO_UF.Value;
    //
    RetTransp_vServ     := 0.00;  // Parei aqui
    RetTransp_vBCRet    := 0.00;  // ...
    RetTransp_pICMSRet  := 0.00;  // ...
    RetTransp_vICMSRet  := 0.00;  // ...
    RetTransp_CFOP      := '';    // ...
    RetTransp_CMunFG    := '';    // ...
  end else begin
    transporta_CNPJ     := '';
    transporta_CPF      := '';
    transporta_xNome    := '';
    transporta_IE       := '';
    transporta_xEnder   := '';
    transporta_xMun     := '';
    transporta_UF       := '';
    //
    RetTransp_vServ     := 0.00;
    RetTransp_vBCRet    := 0.00;
    RetTransp_pICMSRet  := 0.00;
    RetTransp_vICMSRet  := 0.00;
    RetTransp_CFOP      := '';
    RetTransp_CMunFG    := '';
  end;
  // Fatura: Deixar assim (� acertado depois nos totais!) -> TDmNFe_0000.TotaisNFe(...
  Cobr_Fat_NFat       := '';
  Cobr_Fat_vOrig      := 0.00;
  Cobr_Fat_vDesc      := 0.00;
  Cobr_Fat_vLiq       := 0.00;
  // Fim Fatura

  Exporta_UFEmbarq    := Exporta_UFEmbarq;
  Exporta_XLocEmbarq  := Exporta_xLocEmbarq;
  //
  _Ativo_           := 1;
  //
  if (emit_CNPJ = '') and (emit_CPF = '') then
    FMsg := 'Emitente sem CNPJ e sem CPF!';
  if (dest_CNPJ = '') and (dest_CPF = '') then
    FMsg := 'Cliente sem CNPJ e sem CPF!';

  if (emit_CNPJ = '') and (DmNFe_0000.QrEmpresaTipo.Value = 0) then
    FMsg := 'Emitente jur�dico sem CNPJ!';
  if (emit_CPF = '') and (DmNFe_0000.QrEmpresaTipo.Value = 1) then
    FMsg := 'Emitente pessoa f�sica sem CPF!';

  if (dest_CNPJ = '') and (DmNFe_0000.QrDestTipo.Value = 0) then
    FMsg := 'Cliente jur�dico sem CNPJ!';
  if (dest_CPF = '') and (DmNFe_0000.QrDestTipo.Value = 1) then
    FMsg := 'Cliente pessoa f�sica sem CPF!';

  if emit_IE <> 'ISENTO' then emit_IE := Geral.SoNumero_TT(emit_IE);
  if Length(emit_IE) = 0 then
    FMsg := 'IE deve ser informado (informe "ISENTO" para empresa sem IE)';
  if (Length(emit_fone) > 0) and (Length(emit_fone) < 10) then
    FMsg := 'Telefone deve ter 10 n�meros: ' + emit_fone;
  if not DmNFe_0000.MontaChaveDeAcesso(ide_cUF, ide_dEmi, emit_CNPJ, ide_mod, ide_Serie,
    ide_nNF, ide_cNF, ide_cDV, NFe_Id, cNF_Atual, ide_tpEmis, versao) then Exit;
  //p�g 97
  //
  if FMsg <> '' then
  begin
    if FMsg <> 'Exit' then
      Geral.MensagemBox(FMsg, 'Aviso', MB_OK+MB_ICONWARNING);
  end else
  begin
    // Natureza da opera��o (txt)
    if ide_tpAmb = 2 then // Homologa��o
    begin
      ide_natOp := 'TESTE EMISSAO NOTA FISCAL ELETRONICA';
      dest_xNome := 'NF-E EMITIDA EM AMBIENTE DE HOMOLOGACAO - SEM VALOR FISCAL';

      // pediu no SCAN !!!!
      if ide_Serie >= 900 then
      begin
        // pediu no SCAN
        // Rejeicao: NF-e emitida em ambiente de homologacao com CNPJ do destinatario diferente de 99999999000191
        // pediu no SCAN
        dest_CPF := '';
        dest_CNPJ := '99999999000191';
      end;
    end;
    //
    DataFiscal  := Geral.FDT(ide_dEmi, 1);
    CodInfoEmit := Empresa;
    CodInfoDest := Cliente;
    Result := UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'nfecaba', False, [
    'versao', 'Id', 'ide_cUF',
    'ide_cNF', 'ide_natOp', 'ide_indPag',
    'ide_mod', 'ide_serie', 'ide_nNF',
    'ide_dEmi', 'ide_dSaiEnt', 'ide_tpNF',
    'ide_cMunFG', 'ide_tpImp', 'ide_tpEmis', 'ide_cDV',
    'ide_tpAmb', 'ide_finNFe', 'ide_verProc', 'emit_CNPJ',
    'emit_CPF', 'emit_xNome', 'emit_xFant',
    'emit_xLgr', 'emit_nro', 'emit_xCpl',
    'emit_xBairro', 'emit_cMun', 'emit_xMun',
    'emit_UF', 'emit_CEP', 'emit_cPais',
    'emit_xPais', 'emit_fone', 'emit_IE',
    'emit_IEST', 'emit_IM', 'emit_CNAE',
    'dest_CNPJ', 'dest_CPF', 'dest_xNome',
    'dest_xLgr', 'dest_nro', 'dest_xCpl',
    'dest_xBairro', 'dest_cMun', 'dest_xMun',
    'dest_UF', 'dest_CEP', 'dest_cPais',
    'dest_xPais', 'dest_fone', 'dest_IE',
    'dest_ISUF',
    'ModFrete', 'Transporta_CNPJ', 'Transporta_CPF',
    'Transporta_XNome', 'Transporta_IE', 'Transporta_XEnder',
    'Transporta_XMun', 'Transporta_UF', 'RetTransp_vServ',
    'RetTransp_vBCRet', 'RetTransp_PICMSRet', 'RetTransp_vICMSRet',
    'RetTransp_CFOP', 'RetTransp_CMunFG', 'VeicTransp_Placa',
    'VeicTransp_UF', 'VeicTransp_RNTC', 'Cobr_Fat_NFat',
    'Cobr_Fat_vOrig', 'Cobr_Fat_vDesc', 'Cobr_Fat_vLiq',
    'InfAdic_InfAdFisco', 'InfAdic_InfCpl', 'Exporta_UFEmbarq',
    'Exporta_XLocEmbarq', 'Compra_XNEmp', 'Compra_XPed',
    'Compra_XCont', 'IDCtrl', 'FreteExtra', 'SegurExtra',
    'DataFiscal', 'CodInfoEmit', 'CodInfoDest', 'ICMSTot_vOutro',
    // NFe 2.00
    'ide_hSaiEnt', 'ide_dhCont', 'ide_xJust',
    'emit_CRT', 'dest_email', 'Vagao', 'Balsa',
    // fim NFe 2.00
    '_Ativo_'], ['FatID', 'FatNum', 'Empresa'], [
    versao, NFe_Id, ide_cUF,
    ide_cNF, ide_natOp, ide_indPag,
    ide_mod, ide_serie, ide_nNF,
    ide_dEmi, ide_dSaiEnt, ide_tpNF,
    ide_cMunFG, ide_tpImp, ide_tpEmis, ide_cDV,
    ide_tpAmb, ide_finNFe, ide_verProc, emit_CNPJ,
    emit_CPF, emit_xNome, emit_xFant,
    emit_xLgr, emit_nro, emit_xCpl,
    emit_xBairro, emit_cMun, emit_xMun,
    emit_UF, emit_CEP, emit_cPais,
    emit_xPais, emit_fone, emit_IE,
    emit_IEST, emit_IM, emit_CNAE,
    dest_CNPJ, dest_CPF, dest_xNome,
    dest_xLgr, dest_nro, dest_xCpl,
    dest_xBairro, dest_cMun, dest_xMun,
    dest_UF, dest_CEP, dest_cPais,
    dest_xPais, dest_fone, dest_IE,
    dest_ISUF,
    ModFrete, Transporta_CNPJ, Transporta_CPF,
    Transporta_XNome, Transporta_IE, Transporta_XEnder,
    Transporta_XMun, Transporta_UF, RetTransp_vServ,
    RetTransp_vBCRet, RetTransp_PICMSRet, RetTransp_vICMSRet,
    RetTransp_CFOP, RetTransp_CMunFG, VeicTransp_Placa,
    VeicTransp_UF, VeicTransp_RNTC, Cobr_Fat_NFat,
    Cobr_Fat_vOrig, Cobr_Fat_vDesc, Cobr_Fat_vLiq,
    InfAdic_InfAdFisco, InfAdic_InfCpl, Exporta_UFEmbarq,
    Exporta_XLocEmbarq, Compra_XNEmp, Compra_XPed,
    Compra_XCont, IDCtrl, FreteExtra, SegurExtra,
    DataFiscal, CodInfoEmit, CodInfoDest, DespAcess,
    // NFe 2.00
    ide_hSaiEnt, ide_dhCont, ide_xJust,
    emit_CRT, dest_email, Vagao, Balsa,
    // fim NFe 2.00
    _Ativo_], [FatID, FatNum, Empresa], True);
    //
    if retirada_ then
    begin
      Result := UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'nfecabf', False, [
      'retirada_CNPJ', 'retirada_xLgr',
      'retirada_nro', 'retirada_xCpl', 'retirada_xBairro',
      'retirada_cMun', 'retirada_xMun', 'retirada_UF', '_Ativo_'], [
      'FatID', 'FatNum', 'Empresa'], [
      retirada_CNPJ, retirada_xLgr,
      retirada_nro, retirada_xCpl, retirada_xBairro,
      retirada_cMun, retirada_xMun, retirada_UF,_Ativo_], [
      FatID, FatNum, Empresa], True);
    end;
    //
    if entrega_ then
    begin
      Result := UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'nfecabg', False, [
      'entrega_CNPJ', 'entrega_xLgr', 'entrega_nro',
      'entrega_xCpl', 'entrega_xBairro', 'entrega_cMun',
      'entrega_xMun', 'entrega_UF', '_Ativo_'], [
      'FatID', 'FatNum', 'Empresa'], [
      entrega_CNPJ, entrega_xLgr, entrega_nro,
      entrega_xCpl, entrega_xBairro, entrega_cMun,
      entrega_xMun, entrega_UF, _Ativo_], [
      FatID, FatNum, Empresa], True);
    end;
  end;
end;

function TFmNFSe_Steps_0201.CriaNFeNormal(Recria: Boolean; NFeStatus, FatID, FatNum,
  Empresa, IDCtrl, Cliente, FretePor, modFrete, Transporta, ide_indPag,
  FisRegCad: Integer; FreteVal, Seguro, Outros: Double;
  ide_Serie: Variant; ide_nNF: Integer; ide_dEmi, ide_dSaiEnt: TDateTime;
  ide_tpNF, ide_tpEmis: Integer; infAdic_infAdFisco, infAdic_infCpl,
  VeicTransp_Placa, VeicTransp_UF, VeicTransp_RNTC,
  Exporta_UFEmbarq, Exporta_xLocEmbarq, SQL_ITS_ITS, SQL_ITS_TOT, SQL_FAT_ITS,
  SQL_FAT_TOT, SQL_VOLUMES, SQL_CUSTOMZ: String; UF_Emit, UF_Dest: String;
  GravaCampos, cNF_Atual, Financeiro: Integer; ide_hSaiEnt: TTime; ide_dhCont:
  TDateTime; ide_xJust: String; emit_CRT: Integer; dest_email, Vagao, Balsa,
  Compra_XNEmp, Compra_XPed, Compra_XCont: String;
  ApenasCriaXML: Boolean): Boolean;
const
  ide_finNFe = 1; //1- NF-e normal/ 2-NF-e complementar / 3 � NF-e de ajuste
var
  Retirada, Entrega, Status: Integer;
  //
  XMLGerado_Arq, XMLGerado_Dir, XMLAssinado_Dir, ide_natOp: String;
  //
  Continua: Boolean;
  ICMS_Usa, PIS_Usa, COFINS_Usa, ISS_Usa, IPI_Usa, II_Usa: Integer;
  ISS_Alq: Double;
begin
  Result := False;
  // FretePor > N�o usa. Por qu�? Parei Aqui!
  Screen.Cursor := crHourGlass;
  MyObjects.Informa(Label1, True, 'Inciando processo');
  //
  Status := 0;
  try
    if not ApenasCriaXML then   // 2010-10-22
    begin
      DmNFe_0000.QrFisRegCad.Close;
      DmNFe_0000.QrFisRegCad.Params[0].AsInteger := FisRegCad;
      DmNFe_0000.QrFisRegCad.Open;
      ide_natOp   := DmNFe_0000.QrFisRegCadide_natOP .Value;
      ICMS_Usa    := DmNFe_0000.QrFisRegCadICMS_Usa  .Value;
      PIS_Usa     := DmNFe_0000.QrFisRegCadPIS_Usa   .Value;
      COFINS_Usa  := DmNFe_0000.QrFisRegCadCOFINS_Usa.Value;
      ISS_Usa     := DmNFe_0000.QrFisRegCadISS_Usa   .Value;
      ISS_Alq     := DmNFe_0000.QrFisRegCadISS_Alq   .Value;
      IPI_Usa     := DmNFe_0000.QrFisRegCadIPI_Usa   .Value;
      II_Usa      := DmNFe_0000.QrFisRegCadII_Usa    .Value;
      //
      XMLGerado_Arq := '';
      XMLGerado_Dir := '';
      XMLAssinado_Dir := '';
      Retirada      := 0;
      Entrega       := 0;
      Continua      := False;
      //
      MyObjects.Informa(Label1, True, 'Obtendo status da NFe');
      Status := DmNFe_0000.ReopenNFeCabA(FatID, FatNum, Empresa);
      if (Status <= 0) or Recria then
      begin
        if Recria then Status := 0;
        //
        MyObjects.Informa(Label1, True, 'Inserindo registro de cabe�alho da NFe no banco de dados');
        if InsUpdNFeCab(NFeStatus, stIns,
        Empresa, Cliente, modFrete, Transporta, ide_natOp,
        ide_indPag, ide_serie, ide_nNF, ide_dEmi, ide_dSaiEnt, ide_tpNF,
        ide_tpEmis, ide_finNFe, Retirada, Entrega, FatID, FatNum, IDCtrl,
        infAdic_infAdFisco, infAdic_infCpl,
        VeicTransp_Placa, VeicTransp_UF, VeicTransp_RNTC,
        Exporta_UFEmbarq, Exporta_xLocEmbarq,
        SQL_FAT_TOT, FreteVal, Seguro, Outros, cNF_Atual,
        ide_hSaiEnt, ide_dhCont, ide_xJust, emit_CRT, dest_email, Vagao, Balsa,
        Compra_XNEmp, Compra_XPed, Compra_XCont, FVersaoNFe) then
        begin
          //if Continua then
          if Financeiro > 0 then
            Continua := DmNFe_0000.InsUpdNFeCabY(FatID, FatNum, Empresa, SQL_FAT_ITS, Label1, REWarning)
            else
              Continua := True;
          if Continua then
            Continua := DmNFe_0000.InsUpdNFeCabXVol(FatID, FatNum, Empresa,
              SQL_VOLUMES, Label1, REWarning);
          if Continua then
          begin
            Continua := DmNFe_0000.InsUpdNFeIts(FatID, FatNum, Empresa, ICMS_Usa,
            IPI_Usa, II_Usa, PIS_Usa, COFINS_Usa, ISS_Usa, ISS_Alq, UF_Emit, UF_Dest,
            FisRegCad, SQL_ITS_ITS, SQL_ITS_TOT, SQL_CUSTOMZ, Label1, emit_CRT);
          end;
          if Continua then
            Continua := DmNFe_0000.TotaisNFe(FatID, FatNum, Empresa,
              Label1, REWarning);
          if Continua then
            Continua := DmNFe_0000.StepNFeCab(FatID, FatNum, Empresa,
            DmNFe_0000.stepNFeDados, Label1);
        end;
      end else
        Continua := True;
      //
    end else Continua := True; // 2010-10-22
    //
    if Continua or (Status = DmNFe_0000.stepNFeDados) then
    begin
      MyObjects.Informa(Label1, True, 'Montando arquivo XML');
      Continua := FmNFeGeraXML_0200.CriarDocumentoNFe(FatID, FatNum, Empresa,
        XMLGerado_Arq, XMLGerado_Dir, Label1, GravaCampos);
      if Continua then
        Continua := DmNFe_0000.StepNFeCab(FatID, FatNum, Empresa,
          DmNFe_0000.stepNFeGerada, Label1);
    end;
    if Continua or (Status = DmNFe_0000.stepNFeGerada) then
    begin
      MyObjects.Informa(Label1, True, 'Verificando dados para assinatura do arquivo XML');
      // Assinar NF-e
      //Continua := False;
      if XMLGerado_Arq = '' then
      begin
        Status := DmNFe_0000.ReopenNFeCabA(FatID, FatNum, Empresa);
        if Status = DmNFe_0000.stepNFeGerada then
        begin
          DmNFe_0000.ReopenEmpresa(Empresa);
          //
          XMLGerado_Arq := DmNFe_0000.QrNFECabAId.Value + NFE_EXT_NFE_XML;
          XMLGerado_Dir := DmNFe_0000.QrFilialDirNFeGer.Value;
        end;
      end;
      if not Geral.VerificaDir(XMLGerado_Dir, '\', 'XML gerado', True) then Exit;
      if XMLGerado_Arq = '' then
        Geral.MensagemBox('Nome do arquivo da ' +
        'NF-e gerada indefinido!', 'Aviso', MB_OK+MB_ICONWARNING)
      else
        MyObjects.Informa(Label1, True, 'Assinando arquivo XML');
        Continua := DmNFe_0000.AssinarArquivoXML(XMLGerado_Arq, XMLGerado_Dir,
          Empresa, 0, DmNFe_0000.QrNFECabAIDCtrl.Value, XMLAssinado_Dir);
      if Continua then
      begin
        //Continua :=
        DmNFe_0000.StepNFeCab(FatID, FatNum, Empresa,
          DmNFe_0000.stepNFeAssinada, Label1);
        Result := True;
      end;
    end;
  finally
    Screen.Cursor := crDefault;
  end;
end;

function TFmNFSe_Steps_0201.DefI(Codigo, ID: String; ValMin, ValMax,
  Numero: Integer): Integer;
  function Txt: String;
  begin
    Result := IntToStr(Numero);
  end;
begin
  Result := Numero;
  if DmNFe_0000.QrNfeLayI.Locate('Codigo;ID', VarArrayOf([Codigo,ID]), [loCaseInsensitive]) then
  begin
    if (DmNFe_0000.QrNFeLayIOcorMin.Value > 0) and (Numero < ValMin) then
    begin
      if FMsg = '' then
        FMsg := DmNFe_0000.MensagemDeID_NFe(Codigo, ID, Txt, 'Tamanho do integer abaixo do m�nimo!', 4);
    end else
    if (DmNFe_0000.QrNFeLayIOcorMin.Value > 0) and (Numero > ValMax) then
    begin
      if FMsg = '' then
        FMsg := DmNFe_0000.MensagemDeID_NFe(Codigo, ID, Txt, 'Tamanho do integer acima do m�ximo!', 5);
    end;
  end else FMsg := 'N�o foi poss�vel definir o item #' + Codigo + '- ID = ' +
    ID + ' pelo layout da NF-e!';
end;

function TFmNFSe_Steps_0201.DefinechNFe(var chNFe: WideString): Boolean;
var
  K: Integer;
begin
  Result := False;
  chNFe := EdchNFe.Text;
  K := Length(chNFe);
  if K <> 44 then
    Geral.MensagemBox('Tamanho da chave difere de 44: tamanho = ' +
    IntToStr(K), 'Erro', MB_OK+MB_ICONERROR)
  else if Geral.SoNumero1a9_TT(chNFe) = '' then
    Geral.MensagemBox('Chave n�o definida!', 'Erro', MB_OK+MB_ICONERROR)
  else
    Result := True;
end;

function TFmNFSe_Steps_0201.DefineEmitCNPJ(var EmitCNPJ: WideString): Boolean;
var
  K: Integer;
begin
  EmitCNPJ := Geral.SoNumero_TT(EdEmitCNPJ.Text);
  k := Length(EmitCNPJ);
  if K = 14 then Result := True
  else begin
    Result := False;
    Geral.MensagemBox('CNPJ da empresa emitente com tamanho incorreto!',
    'Erro', MB_OK+MB_ICONERROR);
  end;
end;

function TFmNFSe_Steps_0201.DefineEmpresa(var Empresa: Integer): Boolean;
begin
  Empresa := EdEmpresa.ValueVariant;
  if Empresa <> 0 then Result := True
  else begin
    Result := False;
    Geral.MensagemBox('Empresa n�o definida!', 'Erro', MB_OK+MB_ICONERROR);
  end;
end;

function TFmNFSe_Steps_0201.DefineIDCtrl(var IDCtrl: Integer): Boolean;
begin
  IDCtrl := EdIDCtrl.ValueVariant;
  if IDCtrl <> 0 then Result := True
  else begin
    Result := False;
    Geral.MensagemBox(
    'IDCtrl n�o definido! A��o/consulta n�o ser� inclu�da no hist�rico da NF!',
    'Erro', MB_OK+MB_ICONERROR);
  end;
end;

function TFmNFSe_Steps_0201.DefineLote(var lote: Integer): Boolean;
begin
  Lote := EdLote.ValueVariant;
  if Lote <> 0 then Result := True
  else begin
    Result := False;
    Geral.MensagemBox('Lote n�o definido!', 'Erro', MB_OK+MB_ICONERROR);
  end;
end;

function TFmNFSe_Steps_0201.DefineModelo(var Modelo: WideString): Boolean;
begin
  Modelo := FormatFloat('00', EdModelo.ValueVariant);
  if Modelo <> '55' then
  begin
    Result := False;
    Geral.MensagemBox('Modelo de NF-e n�o implementado: ' + Modelo,
    'Aviso', MB_OK+MB_ICONERROR);
  end else Result := True;
end;

function TFmNFSe_Steps_0201.DefinenNFFin(var nNFFim: WideString): Boolean;
begin
  if (EdnNFFim.ValueVariant = null) or (EdnNFFim.ValueVariant < 0) then
  begin
    Result := False;
    Geral.MensagemBox('Numera��o final de NF-e inv�lida!',
    'Aviso', MB_OK+MB_ICONERROR);
  end else begin
    nNFFim := FormatFloat('0', EdnNFFim.ValueVariant);
    Result := True;
  end;
end;

function TFmNFSe_Steps_0201.DefinenNFIni(var nNFIni: WideString): Boolean;
begin
  if (EdnNFIni.ValueVariant = null) or (EdnNFIni.ValueVariant < 0) then
  begin
    Result := False;
    Geral.MensagemBox('Numera��o inicial de NF-e inv�lida!',
    'Aviso', MB_OK+MB_ICONERROR);
  end else begin
    nNFIni := FormatFloat('0', EdnNFIni.ValueVariant);
    Result := True;
  end;
end;

function TFmNFSe_Steps_0201.DefinenProt(var nProt: WideString): Boolean;
var
  K: Integer;
begin
  Result := False;
  nProt := EdnProt.Text;
  K := Length(nProt);
  if K <> 15 then
    Geral.MensagemBox('Tamanho do protocolo difere de 15: tamanho = ' +
    IntToStr(K), 'Erro', MB_OK+MB_ICONERROR)
  else if Geral.SoNumero1a9_TT(nProt) = '' then
    Geral.MensagemBox('Protocolo n�o definido!', 'Erro', MB_OK+MB_ICONERROR)
  else
    Result := True;
end;

function TFmNFSe_Steps_0201.DefineSerie(var Serie: WideString): Boolean;
begin
  if (EdSerie.ValueVariant = null) or
  (EdSerie.ValueVariant < 0) or
  (EdSerie.ValueVariant > 899) then
  begin
    Result := False;
    Geral.MensagemBox('N�mero de s�rie inv�lido!', 'Aviso',
      MB_OK+MB_ICONWARNING);
  end else
  begin
    Serie  := FormatFloat('0', EdSerie.ValueVariant);
    Result := True;
  end;
end;

function TFmNFSe_Steps_0201.DefineXMLDoc(): Boolean;
begin
  xmlDoc := CreateXMLDoc;
  if not XMLLoadFromAnsiString(xmlDoc, FTextoArq) then
  begin
    Result := False;
    dmkPF.LeMeuTexto(FTextoArq, FXML_Load_Failure);
  end else Result := True;
end;

function TFmNFSe_Steps_0201.DefMsg(Codigo, ID, MsgExtra, Valor: String): Boolean;
  procedure AlteraDest;
  var
    Dest: Integer;
  begin
    //
    if DmNFe_0000.QrDest.State <> dsInactive then
      Dest := DmNFe_0000.QrDestCodigo.Value
    else
      Dest := 0;
    //
    if DBCheck.CriaFm(TFmEntidade2, FmEntidade2, afmoNegarComAviso) then
    begin
      if Dest <> 0 then
        FmEntidade2.LocCod(Dest, Dest);
      FmEntidade2.ShowModal;
      FmEntidade2.Destroy;
    end;
    if DmNFe_0000.QrDest.State <> dsInactive then
    begin
      DmNFe_0000.QrDest.Close;
      DmNFe_0000.QrDest.Open;
    end;
  end;
var
  Msg1, Msg2: String;
  Cod: String;
begin
  Msg1 := DmNFe_0000.MensagemDeID_NFe(Codigo, ID, Valor, '[Extra] ' + MsgExtra, 6);
  if FMsg = '' then FMsg := Msg1;

  //  Redirecionamento
  if Uppercase(ID[1]) = 'E' then // Emitente
  begin
    Cod := IntToStr(DmNFe_0000.QrDestCodigo.Value);
    Msg2 := 'Desejo alterar agora o cadastro da entidade n� ' + Cod;
    MLAGeral.MessageDlgCheck(Msg1, mtConfirmation, [mbOK], 0, mrOK,
      True, True, Msg2, @AlteraDest);
    FMsg := 'Exit';
  end;
  //
  Result := True;
end;

function TFmNFSe_Steps_0201.DefX(Codigo, ID, Texto: String): String;
var
  t: Integer;
  OK: Boolean;
begin
  //Result := ValidaTexto_XML(Texto, Codigo, ID);
  Result := Texto;
  if DmNFe_0000.QrNfeLayI.Locate('Codigo;ID', VarArrayOf([Codigo,ID]), [loCaseInsensitive]) then
  begin
    t := Length(Result);
    OK := False;
    if (DmNFe_0000.QrNFeLayIOcorMin.Value > 0) or (t > 0) then
    begin
      if DmNFe_0000.QrNFeLayITamVar.Value <> '' then
      begin
        Dmod.QrAux.Close;
        Dmod.QrAux.SQL.Clear;
        Dmod.QrAux.SQL.Add('SELECT ' + FormatFloat('0', T) + ' in (' +
        DmNFe_0000.QrNFeLayITamVar.Value + ') Tem');
        UMyMod.AbreQuery(Dmod.QrAux, Dmod.MyDB, 'TDmNFe_0000.DefX()');
        OK := Dmod.QrAux.FieldByName('Tem').AsInteger = 1;
      end;
      if not OK then
      begin
        if t < DmNFe_0000.QrNFeLayITamMin.Value then
          DefMsg(Codigo, ID, 'Tamanho do texto abaixo do m�nimo!', Result);
        if t > DmNFe_0000.QrNFeLayITamMax.Value then
        begin
          if DmNFe_0000.QrNFeLayITamMin.Value = DmNFe_0000.QrNFeLayITamMax.Value then
            DefMsg(Codigo, ID, 'Tamanho do texto difere do esperado!', Result)
          else
            Result := Copy(Result, 1, DmNFe_0000.QrNFeLayITamMax.Value);
        end;
      end;
    end;
  end else DefMsg(Codigo, ID, 'Item n�o localizado na tabela "NFeLayI"', Texto);
end;

function TFmNFSe_Steps_0201.PreparaEnvioDeLoteEvento(Lote,
  Empresa: Integer): Boolean;
var
  LoteStr: String;
  Continua: Boolean;
begin
  FPathLoteEvento := '';
  EdLote.ValueVariant    := Lote;
  EdEmpresa.ValueVariant := Empresa;
  //
  VerificaCertificadoDigital(Empresa);
  //
  if not CkSoLer.Checked then
  begin
    Continua := FmNFeGeraXML_0200.GerarLoteEvento(Lote, Empresa, FPathLoteEvento, FXML_LoteEvento, Label1);
    if Continua then
    begin
      MyObjects.Informa(Label1, False,
      'Configure a forma de envio e clique em "OK" para enviar o lote ao fisco!');
    end else MyObjects.Informa(Label1, False, 'Envio cancelado!');
  end else
  begin
    LoteStr := FormatFloat('000000000', Lote);
    if AbreArquivoXML(LoteStr, NFE_EXT_EVE_ENV_LOT_XML, False) then
      MyObjects.Informa(Label1, False,
      'Clique em "OK" para ler arquivo selecionado, ou em "Abrir" para selecionar um arquivo!');
  end;
  HabilitaBotoes();
  Result := True;
end;

function TFmNFSe_Steps_0201.PreparaEnvioDeLoteNFe(Lote, Empresa: Integer): Boolean;
var
  LoteStr: String;
  Continua: Boolean;
begin
  FPathLoteNFe := '';
  EdLote.ValueVariant    := Lote;
  EdEmpresa.ValueVariant := Empresa;
  //
  VerificaCertificadoDigital(Empresa);
  //
  if not CkSoLer.Checked then
  begin
    Continua := FmNFeGeraXML_0200.GerarLoteNFe(Lote, Empresa, FPathLoteNFe, FXML_LoteNFe, Label1);
    if Continua then
    begin
      MyObjects.Informa(Label1, False,
      'Configure a forma de envio e clique em "OK" para enviar o lote ao fisco!');
    end else MyObjects.Informa(Label1, False, 'Envio cancelado!');
  end else
  begin
    LoteStr := FormatFloat('000000000', Lote);
    if AbreArquivoXML(LoteStr, NFE_EXT_REC_XML, False) then
      MyObjects.Informa(Label1, False,
      'Clique em "OK" para ler arquivo selecionado, ou em "Abrir" para selecionar um arquivo!');
  end;
  HabilitaBotoes();
  Result := True;
end;

procedure TFmNFSe_Steps_0201.PreparaInutilizaNumerosNF(Empresa, Lote, Ano, Modelo, Serie,
  nNFIni, nNFFim, Justif: Integer);
var
  LoteStr, cUF, Id: WideString;
begin
  EdEmpresa.ValueVariant   := Empresa;
  EdLote.ValueVariant      := Lote;
  EdAno.ValueVariant       := Ano;
  EdModelo.ValueVariant    := Modelo;
  EdSerie.ValueVariant     := Serie;
  EdnNFIni.ValueVariant    := nNFIni;
  EdnNFFim.ValueVariant    := nNFFim;
  EdNFeJust.ValueVariant   := Justif;
  CBNFeJust.KeyValue       := Justif;
  //
  PnJustificativa.Enabled  := False;
  //
  //
  VerificaCertificadoDigital(Empresa);
  //
  //
  if CkSoLer.Checked then
  begin
    cUF := IntToStr(Geral.GetCodigoUF_IBGE_DTB_da_SiglaUF(DmNFe_0000.QrEmpresaNO_UF.Value));
    DmNFe_0000.MontaID_Inutilizacao(cUF, EdAno.Text, EdEmitCNPJ.Text,
      FormatFloat('00', EdModelo.ValueVariant),
      FormatFloat('00', EdSerie.ValueVariant),
      FormatFloat('000000000', nNFIni),
      FormatFloat('000000000', nNFFim), Id);
    LoteStr := Id + '_' + FormatFloat('000000000', Lote);
    //
    if AbreArquivoXML(LoteStr, NFE_EXT_INU_XML, False) then
      MyObjects.Informa(Label1, False,
'Clique em "OK" para ler arquivo selecionado, ou em "Abrir" para selecionar um arquivo!');
  end else
    MyObjects.Informa(Label1, False, 'Configure o modo de solicita��o e clique em "OK"!');
  HabilitaBotoes();
end;

procedure TFmNFSe_Steps_0201.PreparaVerificacaoStatusServico(Empresa: Integer);
begin
  EdEmpresa.ValueVariant := Empresa;
  //
  VerificaCertificadoDigital(Empresa);
  //
  //
  HabilitaBotoes();
  //
  MyObjects.Informa(Label1, False, 'Configure a forma de consulta e clique em "OK"!');
end;

procedure TFmNFSe_Steps_0201.PreparaCancelamentoDeNFe(Lote, Empresa: Integer;
  ChaveNFe, Protocolo: String);
begin
  EdLote.ValueVariant      := Lote;
  EdEmpresa.ValueVariant   := Empresa;
  EdChNFe.Text             := ChaveNFe;
  EdnProt.Text             := Protocolo;
  //
  //
  //
  VerificaCertificadoDigital(Empresa);
  //
  //
  if CkSoLer.Checked then
  begin
    if AbreArquivoXML(ChaveNFe, NFE_EXT_CAN_XML, False) then
      MyObjects.Informa(Label1, False, 'Clique em "OK" para ler arquivo selecionado, ou em "Abrir" para selecionar um arquivo!');
  end else
    MyObjects.Informa(Label1, False, 'Configure o modo de solicita��o e clique em "OK"!');
  HabilitaBotoes();
end;

procedure TFmNFSe_Steps_0201.PreparaConsultaNFe(Empresa, IDCtrl: Integer; ChaveNFe: String);
begin
  EdEmpresa.ValueVariant := Empresa;
  EdchNFe.Text           := ChaveNFe;
  EdIDCtrl.ValueVariant  := IDCtrl;
  //
  //
  VerificaCertificadoDigital(Empresa);
  //
  //
  HabilitaBotoes();
  //
  MyObjects.Informa(Label1, False, 'Configure a forma de consulta e clique em "OK"!');
end;

procedure TFmNFSe_Steps_0201.PreparaConsultaLote(Lote, Empresa: Integer; Recibo: String);
var
  LoteStr: String;
begin
  EdLote.ValueVariant      := Lote;
  EdEmpresa.ValueVariant   := Empresa;
  EdRecibo.ValueVariant    := Recibo;
  //
  VerificaCertificadoDigital(Empresa);
  //
  //
  HabilitaBotoes();

  if CkSoLer.Checked then
  begin
    LoteStr := DmNFe_0000.FormataLoteNFe(Lote);
    if AbreArquivoXML(LoteStr, NFE_EXT_PRO_REC_XML, False) then
      MyObjects.Informa(Label1, False, 'Clique em "OK" para ler arquivo selecionado, ou em "Abrir" para selecionar um arquivo!');
  end else
    MyObjects.Informa(Label1, False, 'Configure a forma de consulta e clique em "OK"!');
end;

procedure TFmNFSe_Steps_0201.EdEmpresaChange(Sender: TObject);
begin
  {
  DmNFe_0000.ReopenEmpresa(Geral.IMV(EdEmpresa.Text));
  //
  CBUF.Text := DmNFe_0000.QrFilialUF_WebServ.Value;
  }
  DmNFe_0000.ReopenOpcoesNFe(EdEmpresa.ValueVariant, True);
  RGAmbiente.ItemIndex := DmNFe_0000.QrOpcoesNFeide_tpAmb.Value;
end;

procedure TFmNFSe_Steps_0201.EdUF_ServicoKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  TitCols: array[0..1] of String;
begin
  if Key = VK_F4 then
  begin
    TitCols[0] := 'C�d.';
    TitCols[1] := 'Descri��o';
    //
    EdUF_Servico.Text := Geral.SelecionaItem(FSiglas_WS, 1,
      'SEL-LISTA-000 :: Sigla da Web Service', TitCols, Screen.Width)
  end;
end;

procedure TFmNFSe_Steps_0201.ExecutaCancelaNFe();//MeuID_NFe: Integer);
var
  chNFe, nProt, xJust: WideString;
  Empresa, K: Integer;
begin
  if not DefinechNFe(chNFe) then Exit;
  if not DefineEmpresa(Empresa) then Exit;
  if not DefinenProt(nProt) then Exit;
  xJust := Trim(DmNFe_0000.ValidaTexto_XML(CBNFeJust.Text, 'xJust', 'xJust'));
  K := Length(xJust);
  if K < 15 then
  begin
    Geral.MensagemBox('A justificativa deve ter pelo menos 15 ' +
    'caracteres!'#13#10+'O texto "' + xJust + '" tem apenas ' + IntToStr(K)+'.',
    'Aviso', MB_OK+MB_ICONWARNING);
    Exit;
  end;
  xJust := Geral.TFD(FloatToStr(QrNFeJustCodigo.Value), 10, siPositivo) + ' - ' + xJust;
  Screen.Cursor := CrHourGlass;
  try
    //
    FTextoArq := FmNFeGeraXML_0200.WS_NFeCancelamentoNFe(EdUF_Servico.Text,
      FAmbiente_Int, FCodigoUF_Int, EdchNFe.Text, EdSerialNumber.Text,
      EdnProt.Text, xJust, Label1, RETxtEnvio, EdWebService);
    MyObjects.Informa(Label1, False, 'Resposta recebido com Sucesso!');
    //
    // Salva arquivo
    if not DmNFe_0000.ReopenEmpresa(Empresa) then Exit;
    DmNFe_0000.SalvaXML(NFE_EXT_CAN_XML, chNFe, FTextoArq, RETxtRetorno, False);
    //
    LerTextoCancelaNFe();//MeuID_NFe);
    //
    if Pos('Erros:', FTextoArq) > 0 then
    begin
      Geral.MensagemBox('Erro na chamada do WS...' + #13+#13 +
      FTextoArq, 'Erro', MB_OK+MB_ICONERROR);
      MyObjects.Informa(Label1, False, 'Resposta recebida com Erros');
    end;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmNFSe_Steps_0201.ExecutaConsultaLoteNFe();
var
  Recibo, LoteStr: String;
  Empresa, Lote: Integer;
begin
  Recibo := EdRecibo.Text;
  if (Recibo <> '')  then
  begin
    MyObjects.Informa(Label1, True, 'Preparando consulta');
    if not DefineEmpresa(Empresa) then Exit;
    if not DefineLote(Lote) then Exit;
    DmNFe_0000.ReopenEmpresa(Empresa);
    FTextoArq :='';
    Screen.Cursor := CrHourGlass;
    try
      MyObjects.Informa(Label1, True, 'Consultando o servidor do fisco');
      //
      FTextoArq := FmNFeGeraXML_0200.WS_NFeRetRecepcao(EdUF_Servico.Text, FAmbiente_Int,
        FCodigoUF_Int, EdRecibo.Text, Label1, RETxtEnvio, EdWebService);
      //
      MyObjects.Informa(Label1, False, 'Resposta recebida com Sucesso!');
      MostraTextoRetorno(FTextoArq);
      if Pos('Erros:', FTextoArq) > 0 then
      begin
        MyObjects.Informa(Label1, False, 'O fisco acusou erros na resposta!');
        Geral.MensagemBox('Resposta recebida com Erros!', 'Aviso',
        MB_OK+MB_ICONWARNING);
      end else
      begin
        DmNFe_0000.ReopenEmpresa(Empresa);
        //
        LoteStr := DmNFe_0000.FormataLoteNFe(Lote);
        DmNFe_0000.SalvaXML(NFE_EXT_PRO_REC_XML, LoteStr, FTextoArq, RETxtRetorno, False);
        //
        LerTextoConsultaLoteNFe();
        try
          if FFormChamou = 'FmNFeLEnc_0200' then
            FmNFeLEnc_0200.LocCod(Lote, Lote);
          if FFormChamou = 'FmNFeLEnU_0200' then
            FmNFeLEnU_0200.ReabreNFeLEnc(Lote);
        except
          Geral.MensagemBox(
          'N�o foi poss�vel localizar o lote de NF-e(s) n�mero ' + EdLote.Text +
          '!', 'Aviso', MB_OK+MB_ICONWARNING);
        end;
      end;
    finally
      Screen.Cursor := crDefault;
    end;
  end
  else
    Geral.MensagemBox('Recibo n�o informado para consulta...', 'Aviso',
    MB_OK+MB_ICONWARNING);
end;

procedure TFmNFSe_Steps_0201.ExecutaConsultaNFe();
var
  Empresa, IDCtrl: Integer;
  chNFe: WideString;
  Id, Dir, Aviso: String;
begin
  if not DefineEmpresa(Empresa) then Exit;
  if not DefinechNFe(chNFe) then Exit;
  //
  Screen.Cursor := crHourGlass;
  try
    {
    FTextoArq := FmNFeGeraXML_0200.WS_NFeConsultaNF(EdUF_Servico.Text, FAmbiente_Int,
      FCodigoUF_Int, EdSerialNumber.Text, chNFe, Label1);
    }
    FTextoArq := FmNFeGeraXML_0200.WS_NFeConsultaNF(EdUF_Servico.Text, FAmbiente_Int,
      FCodigoUF_Int, chNFe, EdVersaoAcao.Text, Label1, RETxtEnvio, EdWebService);
    MostraTextoRetorno(FTextoArq);
    MyObjects.Informa(Label1, True, 'Resposta recebida com Sucesso!');
    //
    DmNFe_0000.SalvaXML(NFE_EXT_SIT_XML, chNFe, FTextoArq, RETxtRetorno, False);
    //
    // 2012-03-29
    if Pos('Erros:', FTextoArq) > 0 then
    begin
      Geral.MensagemBox('Erro na chamada do WS...' + #13+#13 +
      FTextoArq, 'Erro', MB_OK+MB_ICONERROR);
      MyObjects.Informa(Label1, False, 'Resposta recebida com Erros!');
    end;
    if not FNaoExecutaLeitura then
    begin
    // fim 2012-03-29
      LerTextoConsultaNFe();
      //
      // 2010-10-18
      QrCabA.Close;
      QrCabA.Params[00].AsInteger := EdEmpresa.ValueVariant;
      QrCabA.Params[01].AsString  := chNFe;
      QrCabA.Open;
      IDCtrl := QrCabAIDCtrl.Value;
      if IDCtrl > 0 then
      begin
        // 2011-06-08
        Id    := QrCabAinfProt_ID.Value;
        if Trim(Id) = '' then
          Id := QrCabAinfProt_nProt.Value;
        // Fim2011-06-08

        Dir   := DModG.QrParamsEmpDirSit.Value;
        Aviso := '';
        DmNFe_0000.AtualizaXML_No_BD_ConsultaNFe(chNFe, Id, IDCtrl, Dir, Aviso);
        if Aviso <> '' then Geral.MensagemBox(
        'Os arquivos abaixo n�o foram localizados:' + #13#10 + Aviso,
        'Aviso', MB_OK+MB_ICONWARNING);
      end;
    end else
      Close;
    // Fim 2010-10-18
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmNFSe_Steps_0201.RGAcaoClick(Sender: TObject);
begin
  PnLote.Visible               := False;
  PnRecibo.Visible             := False;
  PnJustificativa.Visible      := False;
  PnInutiliza.Visible          := False;
  PnChaveNFe.Visible           := False;
  PnProtocolo.Visible          := False;
  //
  case RGAcao.ItemIndex of
    0: (*Nada*);
    1: 
    begin
      PnLote.Visible               := True;
    end;
    2:
    begin
      PnLote.Visible               := True;
      PnRecibo.Visible             := True;
    end;
    3:
    begin
      ReopenNFeJust(1);
      PnLote.Visible               := True;
      //PnCancInutiliza.Visible      := True;
      PnChaveNFe.Visible           := True;
      PnProtocolo.Visible          := True;
      PnJustificativa.Visible      := True;
    end;
    4: (*Ainda n�o fiz*)
    begin
      ReopenNFeJust(2);
      PnLote.Visible               := True;
      //PnCancInutiliza.Visible      := True;
      PnInutiliza.Visible          := True;
      PnJustificativa.Visible      := True;
    end;
    5:
    begin
      PnChaveNFe.Visible           := True;
    end;
    6:
    begin
      PnLote.Visible               := True;
    end;
  end;
  PnCancInutiliza.Visible := PnRecibo.Visible or PnChaveNFe.Visible or PnProtocolo.Visible;
end;

function TFmNFSe_Steps_0201.TextoArqDefinido(Texto: WideString): Boolean;
begin
  Result := Texto <> '';
  if not Result then
    Geral.MensagemBox('Texto XML n�o definido!', 'Erro', MB_OK+MB_ICONERROR);
end;

procedure TFmNFSe_Steps_0201.Timer1Timer(Sender: TObject);
begin
  FSegundos := FSegundos + 1;
  LaWait.Visible := True;
  MyObjects.Informa(LaWait, True, 'Aguarde o tempo m�nimo de resposta ' +
    FormatFloat('0', FSegundos) + ' de ' + FormatFloat('0', FSecWait));
  //
  if LaWait.Font.Color = clGreen then
    LaWait.Font.Color := clBlue
  else
    LaWait.Font.Color := clGreen;
  //
  if FSegundos = FSecWait then
    Close;
end;

procedure TFmNFSe_Steps_0201.RETxtEnvioChange(Sender: TObject);
begin
  DmNFe_0000.LoadXML(RETxtEnvio, WBEnvio, PageControl1, 1);
end;

procedure TFmNFSe_Steps_0201.RETxtEnvioSelectionChange(Sender: TObject);
begin
  UpdateCursorPos(RETxtEnvio);
end;

procedure TFmNFSe_Steps_0201.RETxtRetornoChange(Sender: TObject);
begin
  DmNFe_0000.LoadXML(RETxtRetorno, WBResposta, PageControl1, 3);
end;

procedure TFmNFSe_Steps_0201.ReopenNFeJust(Aplicacao: Byte);
begin
  QrNFeJust.Close;
  QrNFeJust.Params[0].AsInteger := Aplicacao;
  QrNFeJust.Open;
  //
end;

procedure TFmNFSe_Steps_0201.UpdateCursorPos(Memo: TMemo);
var
  CharPos: TPoint;
begin
  CharPos.Y := SendMessage(Memo.Handle, EM_EXLINEFROMCHAR, 0, Memo.SelStart);
  CharPos.X := (Memo.SelStart - SendMessage(Memo.Handle, EM_LINEINDEX, CharPos.Y, 0));
  Inc(CharPos.Y);
  Inc(CharPos.X);
  //StatusBar.Panels[0].Text := Format(sColRowInfo, [CharPos.Y, CharPos.X]);
  dmkEdit1.ValueVariant := CharPos.Y;
  dmkEdit2.ValueVariant := CharPos.X;
  dmkEdit3.ValueVariant := Memo.SelStart;
end;

procedure TFmNFSe_Steps_0201.VerificaCertificadoDigital(Empresa: Integer);
begin
  DmNFe_0000.ReopenEmpresa(Empresa);
  EdEmitCNPJ.Text          := DmNFe_0000.QrEmpresaCNPJ.Value;
  CBUF.Text                := DmNFe_0000.QrFilialUF_WebServ.Value;
  //2011-08-25
  //EdUF_Servico.Text        := DmNFe_0000.QrFilialUF_Servico.Value;
  case DmNFe_0000.QrFilialNFetpEmis.Value of
    3(*SCAN*): EdUF_Servico.Text := 'SCAN';
    else EdUF_Servico.Text := DmNFe_0000.QrFilialUF_Servico.Value;
  end;
  // Fim 2011-08-25
  EdSerialNumber.Text := DmNFe_0000.QrFilialNFeSerNum.Value;
  LaExpiraCertDigital.Caption := '';
  if DmNFe_0000.QrFilialNFeSerVal.Value < 2 then
    LaExpiraCertDigital.Caption :=
    'N�o h� data de validade cadastrada para seu certificado digital!'
  else
  if DmNFe_0000.QrFilialNFeSerVal.Value < Int(Date) then
    LaExpiraCertDigital.Caption :=
    'Seu certificado digital expirou!'
  else
  if DmNFe_0000.QrFilialNFeSerVal.Value <= (Int(Date) + DmNFe_0000.QrFilialNFeSerAvi.Value) then
    LaExpiraCertDigital.Caption :=
    'Seu certificado digital expira em ' + FormatFloat('0.000',
    DmNFe_0000.QrFilialNFeSerVal.Value - Now + 1) + ' dias!';
  LaExpiraCertDigital.Visible := LaExpiraCertDigital.Caption <> '';
end;

procedure TFmNFSe_Steps_0201.VerificaStatusServico();
begin
  Screen.Cursor := CrHourGlass;
  try
    FTextoArq := FmNFeGeraXML_0200.WS_NFeStatusServico(EdUF_Servico.Text, FAmbiente_Int,
      FCodigoUF_Int, EdSerialNumber.Text, Label1, RETxtEnvio, EdWebService);
    MyObjects.Informa(Label1, False, 'Resposta recebida com sucesso!');
    MostraTextoRetorno(FTextoArq);
    LerTextoStatusServico();
    if Pos('Erros:', FTextoArq) > 0 then
    begin
      Geral.MensagemBox('Erro na chamada do WS...' + #13+#13 +
      FTextoArq, 'Erro', MB_OK+MB_ICONERROR);
      MyObjects.Informa(Label1, False, 'Resposta recebida com Erros');
    end;
  finally
    Screen.Cursor := crDefault;
  end;
end;

end.
