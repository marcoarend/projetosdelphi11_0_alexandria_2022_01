unit NFSe_Steps_0201;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnMLAGeral, UnInternalConsts,
  dmkImage,
  OleCtrls, SHDocVw, XMLIntf, XMLDoc, JwaWinCrypt, SOAPHTTPClient,
  SOAPHTTPTrans, WinInet,
  // Dermatek
  //NfsBR_Configuracoes, NfsBR_Conversao,
  // Microsoft
  CAPICOM_TLB, MSXML2_TLB, ComObj, StrUtils, ACBrNFSe, DMKpnfsConversao;

type
  TFmNFSe_Steps_0201 = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    PnConfirma: TPanel;
    LaTitulo1C: TLabel;
    CkMeuXML: TCheckBox;
    BtOK: TBitBtn;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    Panel1: TPanel;
    GroupBox1: TGroupBox;
    Panel15: TPanel;
    RGAcao: TRadioGroup;
    PnAbrirXML: TPanel;
    BtAbrir: TButton;
    Panel5: TPanel;
    Panel6: TPanel;
    REWarning: TRichEdit;
    Panel7: TPanel;
    TabSheet8: TTabSheet;
    Panel11: TPanel;
    La_T1: TLabel;
    La_T3: TLabel;
    La_T2: TLabel;
    Panel12: TPanel;
    TabSheet9: TTabSheet;
    Panel13: TPanel;
    TabSheet10: TTabSheet;
    Panel14: TPanel;
    Panel9: TPanel;
    Panel8: TPanel;
    RGAmbiente: TRadioGroup;
    Panel10: TPanel;
    Label5: TLabel;
    Label1: TLabel;
    Label17: TLabel;
    Label14: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label6: TLabel;
    Label19: TLabel;
    EdEmpresa: TdmkEdit;
    EdID: TdmkEdit;
    EdSerialNumber: TdmkEdit;
    EdCNPJ: TdmkEdit;
    EdSenha: TdmkEdit;
    EdVersao: TdmkEdit;
    EdCodigoVerificacao: TdmkEdit;
    EdWebService: TEdit;
    CkSoLer: TCheckBox;
    PageControl2: TPageControl;
    TabSheet2: TTabSheet;
    RETxtEnvioNFSe: TMemo;
    TabSheet5: TTabSheet;
    WBEnvio: TWebBrowser;
    TabSheet3: TTabSheet;
    RETxtRetorno: TMemo;
    TabSheet4: TTabSheet;
    WBResposta: TWebBrowser;
    TabSheet6: TTabSheet;
    MeInfo: TMemo;
    TabSheet7: TTabSheet;
    Button1: TButton;
    Button2: TButton;
    Label4: TLabel;
    EdNIRE: TdmkEdit;
    GroupBox2: TGroupBox;
    GroupBox3: TGroupBox;
    TPEmissaoI: TDateTimePicker;
    Label7: TLabel;
    TPEmissaoF: TDateTimePicker;
    Label8: TLabel;
    Label9: TLabel;
    EdPagina: TdmkEdit;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure RETxtRetornoChange(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure BtAbrirClick(Sender: TObject);
    procedure EdEmpresaExit(Sender: TObject);
    procedure RGAmbienteClick(Sender: TObject);
    procedure RGAcaoClick(Sender: TObject);
    procedure EdWebServiceChange(Sender: TObject);
    procedure EdEmpresaChange(Sender: TObject);
    procedure RETxtEnvioNFSeChange(Sender: TObject);
  private
    { Private declarations }
    FAmbiente_Int: Integer;
    FAmbiente_Txt: String;
    FWSDL, FURL: String;
    FPathXML: String;
    FTextoArq: WideString;
    _xmlDoc : IXMLDocument;
    //
    // Defini��es
    function  DefineRPS(var RPS: Integer): Boolean;
    function  DefineEmpresa(var Empresa: Integer): Boolean;

    // Execu��es no WS
    procedure ExecutaGeracaoDeNFSe();
    //
    // Leitura de retorno
    function  LerTextoEnvioRPS(): Boolean;
    //
    // Outros
    function  AbreArquivoSelecionado(Arquivo: String): Boolean;
    procedure ConfiguracoesEmpresa();
    function  DefineXMLDoc(): Boolean;
    procedure HabilitaBotoes(Visivel: Boolean = True);
    procedure MostraTextoRetorno(Texto: WideString);
    function  TextoArqDefinido(Texto: WideString): Boolean;
    procedure PreparaConexaoAoWebService(Empresa: Integer);
    procedure OnBeforePost(const HTTPReqResp: THTTPReqResp;
              Data: Pointer);
    procedure ConfiguraRio(Rio: THTTPRIO);
  public
    { Public declarations }
    // Prepara envio
    function EnviarLoteRpsSincronoEnvio_Prepara(Lote, Empresa: Integer;
             XMLAssinado_Dir: String): Boolean;
    function EnviarLoteRpsSincronoEnvio_Envia(): Boolean;
    function ConsultarNfseServicoPrestado_Envia(): Boolean;
    function EnviarLoteRpsSincronoEnvio_Retorno(Arquivo: String): Boolean;
    function ConsultarNfseFaixa_Envia(): Boolean;

    function PreparaGeracaoDeRPS(RPS, Empresa: Integer;
             XMLAssinado_Dir: String): Boolean;

  end;

  var
  FmNFSe_Steps_0201: TFmNFSe_Steps_0201;

implementation

uses UnMyObjects, Module, UnDmkProcFunc, NFSe_PF_0000, NFSe_0000_Module,
  UMySQLModule, NFSe_PF_0201;

var
  FverXML_versao: String;

{$R *.DFM}

const
  FXML_Load_Failure = 'Falha ao carregar o XML!';
  INTERNET_OPTION_CLIENT_CERT_CONTEXT = 84;


function TFmNFSe_Steps_0201.AbreArquivoSelecionado(Arquivo: String): Boolean;
begin
  FTextoArq := MLAGeral.LoadFileToText(Arquivo);
  MostraTextoRetorno(FTextoArq);
  if FTextoArq <> '' then
    HabilitaBotoes();
  Result := true;
end;

procedure TFmNFSe_Steps_0201.BtAbrirClick(Sender: TObject);
var
  IniDir, Arquivo: String;
begin
  IniDir := ExtractFileDir('C:\Dermatek\NFe\');
  Arquivo := '';
  if MyObjects.FileOpenDialog(Self, IniDir, Arquivo,
  'Selecione o arquivo XML', '', [], Arquivo) then
    AbreArquivoSelecionado(Arquivo);
end;

procedure TFmNFSe_Steps_0201.BtOKClick(Sender: TObject);
begin
  UnNFSe_PF_0201.ConfiguraComponenteACBrNFSe(EdEmpresa.ValueVariant,
    DmNFSe_0000.ACBrNFSe1);
  DmNFSe_0000.ACBrNFSe1.NotasFiscais.Clear;
  //
  REWarning.Text     := '';
  //RETxtEnvioNFSe.Text    := '';
  MeInfo.Text        := '';
  MostraTextoRetorno('');
  PageControl1.ActivePageIndex := 2;
  Update;
  Application.ProcessMessages;
  //
  FAmbiente_Int  := RGAmbiente.ItemIndex;
  if FAmbiente_Int = 0 then
  begin
    Geral.MensagemBox('Defina o ambiente!', 'Aviso', MB_OK+MB_ICONWARNING);
    Exit;
  end;
  FAmbiente_Txt  := IntToStr(FAmbiente_Int);
  FWSDL          := '';
  FURL           := '';
  //
  // 2012-09-22 BtOK.Enabled := False;
  case RGAcao.ItemIndex of
    1: EnviarLoteRpsSincronoEnvio_Envia();
    7: ConsultarNfseServicoPrestado_Envia();
    9: ConsultarNfseFaixa_Envia();










    {
    begin
      if CkSoLer.Checked then
        LerTextoEnvio?()
      else
        ExecutaEnvioDe?();
    end;
    }
    else Geral.MensagemBox('A a��o "' + RGAcao.Items[
    RGAcao.ItemIndex] + '" n�o est� implementada! AVISE A DERMATEK!',
    'Aviso', MB_OK+MB_ICONWARNING);
  end;
  if Trim(REWarning.Text) <> '' then
    dmkPF.LeMeuTexto(REWarning.Text);
end;

procedure TFmNFSe_Steps_0201.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmNFSe_Steps_0201.Button1Click(Sender: TObject);
{
const
  XMLTit = '<?xml version="1.0" encoding="UTF-8"?>';
  XML =
  '<ConsultarNfseRpsEnvio xmlns="http://www.abrasf.org.br/nfse.xsd" xmlns:ds="http://www.w3.org/2000/09/xmldsig#" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.abrasf.org.br/nfse.xsd nfse_v2.01.xsd ">' +
    '<IdentificacaoRps>' +
      '<Numero>1</Numero>' +
      '<Serie>1</Serie>' +
      '<Tipo>1</Tipo>' +
    '</IdentificacaoRps>' +
    '<Prestador>' +
      '<CpfCnpj>'+
        '<Cnpj>03143014000152</Cnpj>' +
      '</CpfCnpj>' +
      '<InscricaoMunicipal>82281</InscricaoMunicipal>' +
    '</Prestador>' +
  '</ConsultarNfseRpsEnvio>';
  XMLCab = '';//'<cabecalho versao="2.01" xmlns="https://isseteste.maringa.pr.gov.br/ws/"><versaoDados>2.01</versaoDados></cabecalho>';
var
  Res: WideString;
  Nota: NfseServicesPort;
  sAviso: String;
  Rio: THTTPRIO;
}
begin
{
  FURL := 'https://isseteste.maringa.pr.gov.br/ws/';
  //
  Rio := THTTPRIO.Create(nil);
  ConfiguraRio(Rio);
  Nota      := GetNfseServicesPort(False, FURL, Rio);
  RETxtEnvioNFSe.Text :=  'URL: ' + FURL + #13#10 + XMLCab + #13#10 + XML;
  Res := Nota.ConsultarNfseRps(XMLTit + XMLCab + XML);
  //
  Geral.MensagemBox(Res, 'Aviso', MB_OK+MB_ICONWARNING);
}
end;

procedure TFmNFSe_Steps_0201.Button2Click(Sender: TObject);
  procedure ConfiguraRio( Rio : THTTPRIO);
  begin
    (*
    if FConfiguracoes.WebServices.ProxyHost <> '' then
     begin
       Rio.HTTPWebNode.Proxy        := FConfiguracoes.WebServices.ProxyHost+':'+FConfiguracoes.WebServices.ProxyPort;
       Rio.HTTPWebNode.UserName     := FConfiguracoes.WebServices.ProxyUser;
       Rio.HTTPWebNode.Password     := FConfiguracoes.WebServices.ProxyPass;
     end;
    *)
    Rio.HTTPWebNode.OnBeforePost := OnBeforePost;
  end;
var
  Res: WideString;
  Nota: NfseServicesPort;
  sAviso: String;
  Rio: THTTPRIO;
begin
(*
  FURL := 'https://isseteste.maringa.pr.gov.br/ws/';
  //
  Rio := THTTPRIO.Create(nil);
  ConfiguraRio(Rio);
  Nota      := GetNfseServicesPort(False, FURL, Rio);
  RETxtEnvioNFSe.Text :=  'URL: ' + FURL + #13#10 + XML;
  Res := Nota.GerarNfse(XML);
  //
  RETxtRetorno.Text := Res;
*)
end;

procedure TFmNFSe_Steps_0201.ConfiguracoesEmpresa();
begin
  PreparaConexaoAoWebService(EdEmpresa.ValueVariant);
  if RGAmbiente.ItemIndex <> DmNFSe_0000.QrFilialNFSeAmbiente.Value then
    RGAmbiente.ItemIndex := DmNFSe_0000.QrFilialNFSeAmbiente.Value;
end;

procedure TFmNFSe_Steps_0201.ConfiguraRio(Rio: THTTPRIO);
begin
  {
  if FConfiguracoes.WebServices.ProxyHost <> '' then
   begin
     Rio.HTTPWebNode.Proxy        := FConfiguracoes.WebServices.ProxyHost+':'+FConfiguracoes.WebServices.ProxyPort;
     Rio.HTTPWebNode.UserName     := FConfiguracoes.WebServices.ProxyUser;
     Rio.HTTPWebNode.Password     := FConfiguracoes.WebServices.ProxyPass;
   end;
  }
  Rio.HTTPWebNode.OnBeforePost := OnBeforePost;
end;

function TFmNFSe_Steps_0201.ConsultarNfseFaixa_Envia: Boolean;
{
var
  Res: WideString;
  Nota: NfseServicesPort;
  sAviso: String;
  Rio: THTTPRIO;
  XML, Arquivo: String;
  //
  Status, Codigo: Integer;
begin
  //
  //FPathXML := 'C:\Dermatek\NFSe\CNPJ_03143014000152\RPSEnvLot\00000071-env-lot-rps.xml';
  //FPathXML := 'C:\Projetos_Aux\_NFSE\Maringa\Exemplos\Homologa��o\exemplos\EnviarLoteRpsSincronoEnvio.xml';
  //FPathXML := 'C:\Dermatek\NFSe\CNPJ_03143014000152\DPSAss\00000184-dps.xml';
  // Fim s� teste
  //
  Rio := THTTPRIO.Create(nil);
  ConfiguraRio(Rio);
  Nota      := GetNfseServicesPort(False, FURL, Rio);
  XML := MLAGeral.LoadFileToText(FPathXML);
  if XML = '' then
  begin
    Geral.MensagemBox('O lote de NFSe "' + FPathXML +
    '" foi carregado mas est� vazio!', 'Erro', MB_OK+MB_ICONWARNING);
    Exit;
  end;
  RETxtEnvioNFSe.Text :=  'URL: ' + FURL + #13#10 + XML;
  Res := Nota.EnviarLoteRpsSincrono(XML);
  //Res := Nota.EnviarLoteRps(XML);
  //
  RETxtRetorno.Text := Res;
  Arquivo := DmNFSe_0000.SalvaXML(NFSE_RPS_REC_LOT,
    UnNFSe_PF_0000.FormataRPSNumero(EdId.ValueVariant), Res, nil, False);
  //
  // Alterar no BD para enviado
  Status := DmNFSe_0000.stepLoteEnvEnviado();
  Codigo := EdId.ValueVariant;
  UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'nfselrpsc', False, [
  'Status'], ['Codigo'], [Status], [Codigo], True);
  //
  // Verificar retorno (Sincrono)
  EnviarLoteRpsSincronoEnvio_Retorno(Arquivo);
}
(****
var
  NFSeServices: TWebServicesConf;
  ****)
begin
(****
  NFSeServices := TWebServicesConf.Create(Self);
  //
  NFSeServices.CodigoMunicipio := 4115200;
  ShowMessage(IntToStr(NFSeServices.CodigoMunicipio));
  //
****)
{
  NFSeServices.AmbienteTyp := ambHomologacao;
  ShowMessage(IntToStr(NFSeServices.AmbienteNum));
  //
  NFSeServices.AmbienteNum := 1;
  ShowMessage(IntToStr(Integer(NFSeServices.AmbienteTyp)));
}
  //
end;

function TFmNFSe_Steps_0201.ConsultarNfseServicoPrestado_Envia: Boolean;
var
  CNPJ, NIRE: String;
  Pagina: Integer;
begin
  CNPJ := Geral.SoNumero_TT(EdCNPJ.Text);
  NIRE := Geral.SoNumero_TT(EdNIRE.Text);

  Pagina := EdPagina.ValueVariant;

  if Pagina > 0 then
    DmNFSe_0000.ACBrNFSe1.ConsutarNFSePresta(CNPJ, NIRE, 0, dtpsqNenhuma,
      TPEmissaoI.Date, TPEmissaoF.Date, '', '', '', '', Pagina)
  else
    DmNFSe_0000.ACBrNFSe1.ConsutarNFSe(CNPJ, NIRE, TPEmissaoI.Date, TPEmissaoF.Date);
  //

  ReTxtRetorno.Lines.Text   := UTF8Encode(DmNFSe_0000.ACBrNFSe1.WebServices.ConsNfse.RetWS);
end;

function TFmNFSe_Steps_0201.DefineEmpresa(var Empresa: Integer): Boolean;
begin
  Empresa := EdEmpresa.ValueVariant;
  if Empresa <> 0 then Result := True
  else begin
    Result := False;
    Geral.MensagemBox('Empresa n�o definida!', 'Erro', MB_OK+MB_ICONERROR);
  end;
end;

function TFmNFSe_Steps_0201.DefineRPS(var RPS: Integer): Boolean;
begin
  RPS := EdID.ValueVariant;
  if RPS <> 0 then Result := True
  else begin
    Result := False;
    Geral.MensagemBox('N�mero do RPS n�o definido!', 'Erro', MB_OK+MB_ICONERROR);
  end;
end;

function TFmNFSe_Steps_0201.DefineXMLDoc: Boolean;
begin
{
  (*
  xmlDoc := TXMLDocument.Create;
  if not XMLLoadFromAnsiString(xmlDoc, FTextoArq) then
  begin
    Result := False;
    dmkPF.LeMeuTexto(FTextoArq, FXML_Load_Failure);
  end else Result := True;
  *)
  try
    _xmlDoc := LoadXMLDocument(FTextoArq);
    Result := True;
  except
    Result := False;
    dmkPF.LeMeuTexto(FTextoArq, FXML_Load_Failure);
  end;
}
end;

procedure TFmNFSe_Steps_0201.EdEmpresaChange(Sender: TObject);
begin
  if not EdEmpresa.Focused then
    ConfiguracoesEmpresa();
end;

procedure TFmNFSe_Steps_0201.EdEmpresaExit(Sender: TObject);
begin
  ConfiguracoesEmpresa();
end;

procedure TFmNFSe_Steps_0201.EdWebServiceChange(Sender: TObject);
begin
  //FURL := 'https://isseteste.maringa.pr.gov.br/ws/';
  FURL := EdWebService.Text;
end;

function TFmNFSe_Steps_0201.EnviarLoteRpsSincronoEnvio_Envia(): Boolean;
var
  Res: WideString;
  Nota: NfseServicesPort;
  sAviso: String;
  Rio: THTTPRIO;
  XML, Arquivo: String;
  //
  Status, Codigo: Integer;
begin
  //
  //FPathXML := 'C:\Dermatek\NFSe\CNPJ_03143014000152\RPSEnvLot\00000071-env-lot-rps.xml';
  //FPathXML := 'C:\Projetos_Aux\_NFSE\Maringa\Exemplos\Homologa��o\exemplos\EnviarLoteRpsSincronoEnvio.xml';
  //FPathXML := 'C:\Dermatek\NFSe\CNPJ_03143014000152\DPSAss\00000184-dps.xml';
  // Fim s� teste
  //
  Rio := THTTPRIO.Create(nil);
  ConfiguraRio(Rio);
  Nota      := GetNfseServicesPort(False, FURL, Rio);
  XML := MLAGeral.LoadFileToText(FPathXML);
  if XML = '' then
  begin
    Geral.MensagemBox('O lote de NFSe "' + FPathXML +
    '" foi carregado mas est� vazio!', 'Erro', MB_OK+MB_ICONWARNING);
    Exit;
  end;
  RETxtEnvioNFSe.Text :=  'URL: ' + FURL + #13#10 + XML;
  Res := Nota.EnviarLoteRpsSincrono(XML);
  //Res := Nota.EnviarLoteRps(XML);
  //
  RETxtRetorno.Text := Res;
  Arquivo := DmNFSe_0000.SalvaXML(NFSE_RPS_REC_LOT,
    UnNFSe_PF_0000.FormataRPSNumero(EdId.ValueVariant), Res, nil, False);
  //
  // Alterar no BD para enviado
  Status := DmNFSe_0000.stepLoteEnvEnviado();
  Codigo := EdId.ValueVariant;
  UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'nfselrpsc', False, [
  'Status'], ['Codigo'], [Status], [Codigo], True);
  //
  // Verificar retorno (Sincrono)
  EnviarLoteRpsSincronoEnvio_Retorno(Arquivo);
end;

function TFmNFSe_Steps_0201.EnviarLoteRpsSincronoEnvio_Prepara(Lote,
  Empresa: Integer; XMLAssinado_Dir: String): Boolean;
var
  LoteStr: String;
  Continua: Boolean;
begin
  RGAcao.ItemIndex := 1;
  //
  FPathXML := XMLAssinado_Dir;
  RETxtEnvioNFSe.Lines.LoadFromFile(FPathXML);
  EdID.ValueVariant    := Lote;
  EdEmpresa.ValueVariant := Empresa;
  //
  PreparaConexaoAoWebService(Empresa);
  //
  HabilitaBotoes();
  Result := True;
end;

function TFmNFSe_Steps_0201.EnviarLoteRpsSincronoEnvio_Retorno(
  Arquivo: String): Boolean;
var
  fEntrada: file;
  NumRead, i: integer;
  buf: char;
  xmldoc: Widestring;
  //
  ListaMensagens: array of String;
begin
  AssignFile(fEntrada, Arquivo);
  Reset(fEntrada, 1);
  xmlDoc := '';

  for i := 1 to FileSize(fEntrada) do
  begin
    Blockread(fEntrada, buf, 1, NumRead);
    xmlDoc := xmlDoc + buf;
  end;

  CloseFile(fEntrada);

  //
  // HOJ2410
  if xmlDoc <> '' then
  begin
    if pos('<ListaNfse', xmlDoc) > 0 then
    begin
      Geral.MensagemBox('Tratamento de mensagem n�o implementado:' + #13#10 +
      '<ListaNfse', 'Aviso', MB_OK+MB_ICONWARNING);
    end else
    if pos('<ListaMensagemRetorno', xmlDoc) > 0 then
    begin
      DmNFSe_0000.StepNFSeRPS_Lote(EdID.ValueVariant,
        DmNFSe_0000.stepLoteRejeitado(), [], [], LaAviso1, LaAviso2);
      //
      DmNFSe_0000.ObtemMensagensRetornoLote(xmlDoc, EdID.ValueVariant, 'MensagemRetorno');
    end else
    if pos('<ListaMensagemRetornoLote', xmlDoc) > 0 then
    begin
      DmNFSe_0000.StepNFSeRPS_Lote(EdID.ValueVariant,
        DmNFSe_0000.stepLoteRejeitado(), [], [], LaAviso1, LaAviso2);
      //
      Geral.MensagemBox('Tratamento de mensagem n�o implementado:' + #13#10 +
      'ListaMensagemRetornoLote', 'Aviso', MB_OK+MB_ICONWARNING);
    end;
  end
  else
    Geral.MensagemBox('N�o foi poss�vel carregar o Documento XML de retorno!',
    'Aviso', MB_OK+MB_ICONWARNING);
  Result := True;
end;

procedure TFmNFSe_Steps_0201.ExecutaGeracaoDeNFSe();
var
  (*retWS,*) rtfDadosMsg, RPSStr: String;
var
  Empresa, RPS: Integer;
  VersaoLayOut, VersaoDados: String;
begin
  if not DefineRPS(RPS) then Exit;
  if not DefineEmpresa(Empresa) then Exit;
  //
  if not CkSoLer.Checked then
  begin
    MyObjects.Informa(Label1, True, 'Enviando RPS ao fisco');
    if not FileExists(FPathXML) then
    begin
      //Continua := False;
      Geral.MensagemBox('O RPS "' + FPathXML +
      '" n�o foi localizado!', 'Erro', MB_OK+MB_ICONWARNING);
      Exit;
    end;

    rtfDadosMsg := MLAGeral.LoadFileToText(FPathXML);
    //RETxtEnvioNFSe.Text := rtfDadosMsg;
    if rtfDadosMsg = '' then
    begin
      //Continua := False;
      Geral.MensagemBox('O RPS "' + FPathXML +
      '" foi carregado mas est� vazio!', 'Erro', MB_OK+MB_ICONWARNING);
      Exit;
    end;
    VersaoLayOut := Geral.Substitui(EdVersao.Text, ',', '.');
    VersaoDados  := VersaoLayOut;
    //
    //retWS :='';
    FTextoArq :='';
    Screen.Cursor := crHourGlass;
    try
      {
      FTextoArq := FmNFeGeraXML_0200.WS_NFeRecepcaoLote(EdUF_Servico.Text,
      FAmbiente_Int, FCodigoUF_Int, EdSerialNumber.Text, Lote, Label1, RETxtEnvio,
      EdWebService);}
      if UnNFSe_PF_0000.Executar(Geral.SoNumero_TT(EdCNPJ.Text), EdSenha.Text,
      VersaoLayOut, VersaoDados, rtfDadosMsg, FTextoArq) then
      begin
        //
        MyObjects.Informa(Label1, False, 'Resposta recebida com Sucesso!');
        MostraTextoRetorno(FTextoArq);
        //
        DmNFSe_0000.ReopenEmpresa(Empresa);
        RPSStr := FormatFloat('000000000', RPS);
        //DmNFSe_0000.SalvaXML(NFSE_???, RPSStr, FTextoArq, RETxtRetorno, False);
        //
        //Timer1.Enabled := True;
      end;
    finally
      Screen.Cursor := crDefault;
    end;
  end;
  if LerTextoEnvioRPS() then ;//Close;
end;

procedure TFmNFSe_Steps_0201.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmNFSe_Steps_0201.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  PageControl1.ActivePageIndex := 2;
  //
  if ((VAR_LOGIN = 'MASTER') and (VAR_SENHA = CO_MASTER)) then
  begin
    EdID.Enabled                := True;
    EdEmpresa.Enabled           := True;
    EdCodigoVerificacao.Enabled := True;
    //
    BtAbrir.Enabled             := True;
  end;
  //
  TPEmissaoI.Date := Date - 30;
  TPEmissaoF.Date := Date;
end;

procedure TFmNFSe_Steps_0201.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmNFSe_Steps_0201.HabilitaBotoes(Visivel: Boolean);
begin
  PnConfirma.Visible := Visivel;
end;

function TFmNFSe_Steps_0201.LerTextoEnvioRPS(): Boolean;
var
  RPS, Empresa: Integer;
begin
  FverXML_versao := verGeraNFSe_Versao;
  Result := False;
  if not DefineRPS(RPS) then Exit;
  if not DefineEmpresa(Empresa) then Exit;
  if not TextoArqDefinido(FTextoArq) then Exit;
  if DefineXMLDoc() then
  begin
    // Parei Aqui
    Geral.MensagemBox(FTextoArq, 'Mensagem', MB_OK+MB_ICONINFORMATION);
  end;
end;

procedure TFmNFSe_Steps_0201.MostraTextoRetorno(Texto: WideString);
begin
  RETxtRetorno.Text := Texto;
end;

procedure TFmNFSe_Steps_0201.OnBeforePost(const HTTPReqResp: THTTPReqResp;
  Data: Pointer);
{
var
  Cert         : ICertificate2;
  CertContext  : ICertContext;
  PCertContext : Pointer;
begin
  Cert := FConfiguracoes.Certificados.GetCertificado;
  CertContext :=  Cert as ICertContext;
  CertContext.Get_CertContext(Integer(PCertContext));

  if not InternetSetOption(Data, INTERNET_OPTION_CLIENT_CERT_CONTEXT, PCertContext,sizeof(CertContext)*5) then
   begin
     if Assigned(TACBrNFe( FACBrNFe ).OnGerarLog) then
        TACBrNFe( FACBrNFe ).OnGerarLog('ERRO: Erro OnBeforePost: ' + IntToStr(GetLastError));
     raise Exception.Create( 'Erro OnBeforePost: ' + IntToStr(GetLastError) );
   end;
}
var
  Store        : IStore;
  Certs        : ICertificates;
  Cert         : ICertificate2;
  CertContext  : ICertContext;
  PCertContext : PCCERT_CONTEXT;
  i : Integer;
begin
  Store := CoStore.Create;
  //Reposit�rios de Certifcados da M�quina

  Store.Open( CAPICOM_CURRENT_USER_STORE, 'MY',    CAPICOM_STORE_OPEN_MAXIMUM_ALLOWED );
  //Abre a lista de certificados

  Certs := Store.Certificates ;
  //Aloca todos os certificados instalados na m�quia

  i := 0;
  //loop de procura ao certificado requerido pelo n�mero serial
  while i < Certs.Count do
   begin
     Cert := IInterface( Certs.Item[ i+1 ] ) as ICertificate2;
     //Cria objeto para acesso a leitura do certificado
     if UpperCase(Cert.SerialNumber ) = UpperCase(EdSerialNumber.Text) then
     //se o n�mero do serial for igual ao que queremos utilizar
      begin
        //carrega informa��es do certificado
        CertContext := Cert as ICertContext;
        CertContext.Get_CertContext( Integer( PCertContext ) );

        //if not (InternetSetOption(Data, 84, PCertContext, Sizeof(CERT_CONTEXT))) then
        if not InternetSetOption(Data, INTERNET_OPTION_CLIENT_CERT_CONTEXT, PCertContext, sizeof(CertContext)*5) then
        begin
          Geral.MensagemBox('Falha ao selecionar o certificado.', 'Aviso', MB_OK+MB_ICONWARNING);
        end;

        i := Certs.Count;
        //encerra o loop
      end;
     i := i + 1;
  end;
end;

procedure TFmNFSe_Steps_0201.PreparaConexaoAoWebService(Empresa: Integer);
begin
  DmNFSe_0000.ReopenEmpresa(Empresa);
  EdCNPJ.Text         := DmNFSe_0000.QrEmpresaCNPJ.Value;
  EdNIRE.Text         := DmNFSe_0000.QrEmpresaNIRE.Value;
  EdSerialNumber.Text := DmNFSe_0000.QrFilialNFSeCertDigital.Value;
  MyObjects.InformaN([La_T1, La_T2, La_T3], False, '...');
  if DmNFSe_0000.QrFilialNFSeCertValidad.Value < 2 then
    MyObjects.InformaN([La_T1, La_T2, La_T3], False,
    'N�o h� data de validade cadastrada para seu certificado digital!')
  else
  if DmNFSe_0000.QrFilialNFSeCertValidad.Value < Int(Date) then
    MyObjects.InformaN([La_T1, La_T2, La_T3], False,
    'Seu certificado digital expirou!')
  else
  if DmNFSe_0000.QrFilialNFSeCertValidad.Value <= (Int(Date) + DmNFSe_0000.QrFilialNFSeCertAviExpi.Value) then
    MyObjects.InformaN([La_T1, La_T2, La_T3], False,
    'Seu certificado digital expira em ' + FormatFloat('0.000',
    DmNFSe_0000.QrFilialNFSeCertValidad.Value - Now + 1) + ' dias!');
  //
  EdVersao.ValueVariant := DmNFSe_0000.QrFilialNFSeVersao.Value;
end;

function TFmNFSe_Steps_0201.PreparaGeracaoDeRPS(RPS, Empresa: Integer;
  XMLAssinado_Dir: String): Boolean;
var
  LoteStr: String;
  Continua: Boolean;
begin
  FPathXML := '';
  EdID.ValueVariant    := RPS;
  EdEmpresa.ValueVariant := Empresa;
  //EdSenha.Text := Senha;
  //EdVersao.Text := Versao;
  FPathXML := XMLAssinado_Dir;
  //
  PreparaConexaoAoWebService(Empresa);
  //
{
  if not CkSoLer.Checked then
  begin
    Continua := FmNFSeGeraXML_0200.GerarLoteNFe(Lote, Empresa, FPathXML, FXML_LoteNFe, Label1);
    if Continua then
    begin
      MyObjects.Informa(Label1, False,
      'Configure a forma de envio e clique em "OK" para enviar o lote ao fisco!');
    end else MyObjects.Informa(Label1, False, 'Envio cancelado!');
  end else
  begin
    LoteStr := FormatFloat('000000000', Lote);
    if AbreArquivoXML(LoteStr, NFE_EXT_REC_XML, False) then
      MyObjects.Informa(Label1, False,
      'Clique em "OK" para ler arquivo selecionado, ou em "Abrir" para selecionar um arquivo!');
  end;
}
  HabilitaBotoes();
  Result := True;
end;

procedure TFmNFSe_Steps_0201.RETxtEnvioNFSeChange(Sender: TObject);
begin
  DmNFSe_0000.LoadXML(RETxtEnvioNFSe, WBEnvio, PageControl1, 1);
end;

procedure TFmNFSe_Steps_0201.RETxtRetornoChange(Sender: TObject);
begin
  DmNFSe_0000.LoadXML(RETxtRetorno, WBResposta, PageControl1, 3);
end;

procedure TFmNFSe_Steps_0201.RGAcaoClick(Sender: TObject);
begin
  MyObjects.InformaN([LaTitulo1A, LaTitulo1B, LaTitulo1C], False,
    RGAcao.Items[RGAcao.ItemIndex]);
end;

procedure TFmNFSe_Steps_0201.RGAmbienteClick(Sender: TObject);
begin
  PreparaConexaoAoWebService(EdEmpresa.ValueVariant);
  case RGAmbiente.ItemIndex of
    1: EdWebService.Text := DmNFSe_0000.QrFilialNFSeWSProducao.Value;
    2: EdWebService.Text := DmNFSe_0000.QrFilialNFSeWSHomologa.Value;
    else EdWebService.Text := '';
  end;
end;

function TFmNFSe_Steps_0201.TextoArqDefinido(Texto: WideString): Boolean;
begin
  Result := Texto <> '';
  if not Result then
    Geral.MensagemBox('Texto XML n�o definido!', 'Erro', MB_OK+MB_ICONERROR);
end;

end.
