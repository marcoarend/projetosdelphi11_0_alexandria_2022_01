object FmNFSe_Steps_0201: TFmNFSe_Steps_0201
  Left = 339
  Top = 185
  Caption = 'NFS-STEPS-001 :: Passos da NFS-e'
  ClientHeight = 623
  ClientWidth = 784
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 784
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 736
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 688
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 213
        Height = 32
        Caption = 'Passos da NFS-e'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 213
        Height = 32
        Caption = 'Passos da NFS-e'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 213
        Height = 32
        Caption = 'Passos da NFS-e'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 784
    Height = 461
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 784
      Height = 421
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object PageControl1: TPageControl
        Left = 0
        Top = 0
        Width = 784
        Height = 421
        ActivePage = TabSheet1
        Align = alClient
        TabOrder = 0
        object TabSheet1: TTabSheet
          Caption = ' Servi'#231'o '
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object Panel1: TPanel
            Left = 0
            Top = 0
            Width = 776
            Height = 393
            Align = alClient
            ParentBackground = False
            TabOrder = 0
            object GroupBox1: TGroupBox
              Left = 1
              Top = 1
              Width = 774
              Height = 391
              Align = alClient
              TabOrder = 0
              object Panel15: TPanel
                Left = 2
                Top = 15
                Width = 770
                Height = 126
                Align = alTop
                BevelOuter = bvNone
                TabOrder = 0
                object RGAcao: TRadioGroup
                  Left = 0
                  Top = 0
                  Width = 654
                  Height = 126
                  Align = alClient
                  Caption = ' A'#231#227'o a realizar: '
                  Columns = 2
                  Enabled = False
                  Items.Strings = (
                    'Enviar lote de RPS'
                    'Enviar lote de RPS S'#237'ncrono'
                    'Gera'#231#227'o de NFS-e'
                    'Cancelamento de NFS-e'
                    'Substitui'#231'ao de NFS-e'
                    'Consulta de Lote de RPS'
                    'Consulta de NFS-e por RPS'
                    'Consulta de NFS-e - Servi'#231'os Prestados'
                    'Consulta de NFS-e - Servi'#231'os Tomados ou Intermediados'
                    'Consulta de NFS-e por Faixa')
                  TabOrder = 0
                  OnClick = RGAcaoClick
                end
                object PnAbrirXML: TPanel
                  Left = 654
                  Top = 0
                  Width = 116
                  Height = 126
                  Align = alRight
                  BevelOuter = bvNone
                  Enabled = False
                  TabOrder = 1
                  Visible = False
                  object BtAbrir: TButton
                    Left = 8
                    Top = 4
                    Width = 100
                    Height = 25
                    Caption = 'Abrir arquivo XML'
                    Enabled = False
                    TabOrder = 0
                    OnClick = BtAbrirClick
                  end
                end
              end
              object Panel5: TPanel
                Left = 2
                Top = 341
                Width = 770
                Height = 48
                Align = alBottom
                BevelOuter = bvNone
                TabOrder = 1
                object Panel6: TPanel
                  Left = 0
                  Top = 0
                  Width = 268
                  Height = 48
                  Align = alLeft
                  BevelOuter = bvNone
                  TabOrder = 0
                end
                object REWarning: TRichEdit
                  Left = 268
                  Top = 0
                  Width = 502
                  Height = 48
                  Align = alClient
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = 4227327
                  Font.Height = -11
                  Font.Name = 'MS Sans Serif'
                  Font.Style = [fsBold]
                  ParentFont = False
                  ReadOnly = True
                  TabOrder = 1
                end
              end
              object Panel7: TPanel
                Left = 2
                Top = 141
                Width = 770
                Height = 192
                Align = alTop
                TabOrder = 2
                object CkSoLer: TCheckBox
                  Left = 4
                  Top = 4
                  Width = 169
                  Height = 17
                  Caption = 'Somente ler arquivo j'#225' gravado.'
                  Enabled = False
                  TabOrder = 0
                end
              end
            end
          end
        end
        object TabSheet8: TTabSheet
          Caption = ' Configura'#231#227'o '
          ImageIndex = 1
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object Panel12: TPanel
            Left = 0
            Top = 0
            Width = 776
            Height = 393
            Align = alClient
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 0
            object Panel9: TPanel
              Left = 0
              Top = 0
              Width = 776
              Height = 209
              Align = alTop
              ParentBackground = False
              TabOrder = 0
              object Panel8: TPanel
                Left = 1
                Top = 1
                Width = 113
                Height = 207
                Align = alLeft
                BevelOuter = bvNone
                TabOrder = 0
                object RGAmbiente: TRadioGroup
                  Left = 0
                  Top = 0
                  Width = 113
                  Height = 207
                  Align = alClient
                  Caption = ' Ambiente: '
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'MS Sans Serif'
                  Font.Style = [fsBold]
                  ItemIndex = 0
                  Items.Strings = (
                    'Nenhum'
                    'Produ'#231#227'o'
                    'Homologa'#231#227'o')
                  ParentFont = False
                  TabOrder = 0
                  OnClick = RGAmbienteClick
                end
              end
              object Panel10: TPanel
                Left = 114
                Top = 1
                Width = 661
                Height = 207
                Align = alClient
                BevelOuter = bvNone
                TabOrder = 1
                object Label5: TLabel
                  Left = 4
                  Top = 8
                  Width = 44
                  Height = 13
                  Caption = 'Empresa:'
                end
                object Label1: TLabel
                  Left = 100
                  Top = 8
                  Width = 14
                  Height = 13
                  Caption = 'ID:'
                end
                object Label17: TLabel
                  Left = 4
                  Top = 36
                  Width = 127
                  Height = 13
                  Caption = 'Serial do Certificado digital:'
                end
                object Label14: TLabel
                  Left = 400
                  Top = 16
                  Width = 73
                  Height = 13
                  Caption = 'CNPJ empresa:'
                end
                object Label2: TLabel
                  Left = 516
                  Top = 16
                  Width = 34
                  Height = 13
                  Caption = 'Senha:'
                end
                object Label3: TLabel
                  Left = 616
                  Top = 16
                  Width = 36
                  Height = 13
                  Caption = 'Vers'#227'o:'
                end
                object Label6: TLabel
                  Left = 8
                  Top = 60
                  Width = 106
                  Height = 13
                  Caption = 'C'#243'digo de verifica'#231#227'o:'
                end
                object Label19: TLabel
                  Left = 4
                  Top = 100
                  Width = 68
                  Height = 13
                  Caption = 'Web Service: '
                end
                object Label4: TLabel
                  Left = 340
                  Top = 60
                  Width = 93
                  Height = 13
                  Caption = 'Inscri'#231#227'o municipal:'
                end
                object EdEmpresa: TdmkEdit
                  Left = 52
                  Top = 4
                  Width = 32
                  Height = 21
                  Alignment = taRightJustify
                  Enabled = False
                  TabOrder = 0
                  FormatType = dmktfInteger
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0
                  OnChange = EdEmpresaChange
                  OnExit = EdEmpresaExit
                end
                object EdID: TdmkEdit
                  Left = 128
                  Top = 4
                  Width = 69
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 1
                  FormatType = dmktfInteger
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 9
                  NoEnterToTab = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '000000000'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0
                end
                object EdSerialNumber: TdmkEdit
                  Left = 136
                  Top = 32
                  Width = 260
                  Height = 21
                  Enabled = False
                  TabOrder = 2
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = ''
                end
                object EdCNPJ: TdmkEdit
                  Left = 400
                  Top = 32
                  Width = 112
                  Height = 21
                  TabOrder = 3
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 1
                  NoEnterToTab = False
                  ValMin = '0'
                  ValMax = '899'
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = ''
                end
                object EdSenha: TdmkEdit
                  Left = 516
                  Top = 32
                  Width = 97
                  Height = 21
                  TabOrder = 4
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 1
                  NoEnterToTab = False
                  ValMin = '0'
                  ValMax = '899'
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = ''
                end
                object EdVersao: TdmkEdit
                  Left = 616
                  Top = 32
                  Width = 40
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 5
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 2
                  LeftZeros = 1
                  NoEnterToTab = False
                  ValMin = '0'
                  ValMax = '899'
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0,00'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                end
                object EdCodigoVerificacao: TdmkEdit
                  Left = 136
                  Top = 56
                  Width = 197
                  Height = 21
                  ReadOnly = True
                  TabOrder = 6
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 9
                  NoEnterToTab = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = ''
                end
                object EdWebService: TEdit
                  Left = 74
                  Top = 96
                  Width = 583
                  Height = 21
                  ReadOnly = True
                  TabOrder = 7
                  OnChange = EdWebServiceChange
                end
                object EdNIRE: TdmkEdit
                  Left = 436
                  Top = 56
                  Width = 77
                  Height = 21
                  ReadOnly = True
                  TabOrder = 8
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 9
                  NoEnterToTab = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = ''
                end
                object GroupBox2: TGroupBox
                  Left = 4
                  Top = 120
                  Width = 477
                  Height = 77
                  Caption = ' Pesquisa'
                  TabOrder = 9
                  object GroupBox3: TGroupBox
                    Left = 2
                    Top = 15
                    Width = 375
                    Height = 60
                    Align = alLeft
                    Caption = ' Periodo emiss'#227'o: '
                    TabOrder = 0
                    object Label7: TLabel
                      Left = 12
                      Top = 16
                      Width = 55
                      Height = 13
                      Caption = 'Data inicial:'
                    end
                    object Label8: TLabel
                      Left = 128
                      Top = 16
                      Width = 51
                      Height = 13
                      Caption = 'Data Final:'
                    end
                    object Label9: TLabel
                      Left = 244
                      Top = 16
                      Width = 36
                      Height = 13
                      Caption = 'P'#225'gina:'
                    end
                    object TPEmissaoI: TDateTimePicker
                      Left = 12
                      Top = 32
                      Width = 112
                      Height = 21
                      Date = 41184.821911446760000000
                      Time = 41184.821911446760000000
                      TabOrder = 0
                    end
                    object TPEmissaoF: TDateTimePicker
                      Left = 128
                      Top = 32
                      Width = 112
                      Height = 21
                      Date = 41184.821911446760000000
                      Time = 41184.821911446760000000
                      TabOrder = 1
                    end
                    object EdPagina: TdmkEdit
                      Left = 244
                      Top = 32
                      Width = 80
                      Height = 21
                      Alignment = taRightJustify
                      TabOrder = 2
                      FormatType = dmktfInteger
                      MskType = fmtNone
                      DecimalSize = 0
                      LeftZeros = 0
                      NoEnterToTab = False
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      Texto = '1'
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = 1
                    end
                  end
                end
              end
            end
          end
        end
        object TabSheet9: TTabSheet
          Caption = ' Mensagens '
          ImageIndex = 2
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object Panel13: TPanel
            Left = 0
            Top = 0
            Width = 776
            Height = 393
            Align = alClient
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 0
            object PageControl2: TPageControl
              Left = 0
              Top = 0
              Width = 776
              Height = 393
              ActivePage = TabSheet3
              Align = alClient
              TabOrder = 0
              object TabSheet2: TTabSheet
                Caption = ' XML de envio '
                ExplicitLeft = 0
                ExplicitTop = 0
                ExplicitWidth = 0
                ExplicitHeight = 0
                object RETxtEnvioNFSe: TMemo
                  Left = 0
                  Top = 0
                  Width = 768
                  Height = 365
                  Align = alClient
                  TabOrder = 0
                  WordWrap = False
                  OnChange = RETxtEnvioNFSeChange
                end
              end
              object TabSheet5: TTabSheet
                Caption = 'XML de envio (Formatado)'
                ImageIndex = 1
                ExplicitLeft = 0
                ExplicitTop = 0
                ExplicitWidth = 0
                ExplicitHeight = 0
                object WBEnvio: TWebBrowser
                  Left = 0
                  Top = 0
                  Width = 768
                  Height = 365
                  Align = alClient
                  TabOrder = 0
                  ExplicitWidth = 776
                  ExplicitHeight = 69
                  ControlData = {
                    4C000000604F0000B92500000000000000000000000000000000000000000000
                    000000004C000000000000000000000001000000E0D057007335CF11AE690800
                    2B2E126208000000000000004C0000000114020000000000C000000000000046
                    8000000000000000000000000000000000000000000000000000000000000000
                    00000000000000000100000000000000000000000000000000000000}
                end
              end
              object TabSheet3: TTabSheet
                Caption = ' XML Retornado (Texto) '
                ImageIndex = 2
                ExplicitLeft = 0
                ExplicitTop = 0
                ExplicitWidth = 0
                ExplicitHeight = 0
                object RETxtRetorno: TMemo
                  Left = 0
                  Top = 0
                  Width = 768
                  Height = 365
                  Align = alClient
                  TabOrder = 0
                  WantReturns = False
                  OnChange = RETxtRetornoChange
                end
              end
              object TabSheet4: TTabSheet
                Caption = ' XML Retornado (Formatado) '
                ImageIndex = 3
                ExplicitLeft = 0
                ExplicitTop = 0
                ExplicitWidth = 0
                ExplicitHeight = 0
                object WBResposta: TWebBrowser
                  Left = 0
                  Top = 0
                  Width = 768
                  Height = 365
                  Align = alClient
                  TabOrder = 0
                  ExplicitWidth = 762
                  ExplicitHeight = 0
                  ControlData = {
                    4C000000604F0000B92500000000000000000000000000000000000000000000
                    000000004C000000000000000000000001000000E0D057007335CF11AE690800
                    2B2E126208000000000000004C0000000114020000000000C000000000000046
                    8000000000000000000000000000000000000000000000000000000000000000
                    00000000000000000100000000000000000000000000000000000000}
                end
              end
              object TabSheet6: TTabSheet
                Caption = ' Informa'#231#245'es do XML de retorno '
                ImageIndex = 4
                ExplicitLeft = 0
                ExplicitTop = 0
                ExplicitWidth = 0
                ExplicitHeight = 0
                object MeInfo: TMemo
                  Left = 0
                  Top = 0
                  Width = 772
                  Height = 145
                  Align = alClient
                  ScrollBars = ssVertical
                  TabOrder = 0
                  WordWrap = False
                end
              end
              object TabSheet7: TTabSheet
                Caption = 'TabSheet6'
                ImageIndex = 5
                ExplicitLeft = 0
                ExplicitTop = 0
                ExplicitWidth = 0
                ExplicitHeight = 0
                object Button1: TButton
                  Left = 140
                  Top = 60
                  Width = 141
                  Height = 25
                  Caption = 'ConsultarNfseRps'
                  TabOrder = 0
                  OnClick = Button1Click
                end
                object Button2: TButton
                  Left = 288
                  Top = 60
                  Width = 185
                  Height = 25
                  Caption = 'Button2'
                  TabOrder = 1
                  OnClick = Button2Click
                end
              end
            end
          end
        end
        object TabSheet10: TTabSheet
          Caption = 'TabSheet10'
          ImageIndex = 3
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object Panel14: TPanel
            Left = 0
            Top = 0
            Width = 776
            Height = 393
            Align = alClient
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 0
          end
        end
      end
    end
    object Panel11: TPanel
      Left = 0
      Top = 421
      Width = 784
      Height = 40
      Align = alBottom
      Color = 12171705
      ParentBackground = False
      TabOrder = 1
      object La_T1: TLabel
        Left = 7
        Top = 2
        Width = 24
        Height = 33
        Caption = '...'
        Font.Charset = ANSI_CHARSET
        Font.Color = 8889019
        Font.Height = -27
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object La_T3: TLabel
        Left = 9
        Top = 4
        Width = 24
        Height = 33
        Caption = '...'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWhite
        Font.Height = -27
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object La_T2: TLabel
        Left = 8
        Top = 3
        Width = 24
        Height = 33
        Caption = '...'
        Font.Charset = ANSI_CHARSET
        Font.Color = 7681805
        Font.Height = -27
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 509
    Width = 784
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 780
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 553
    Width = 784
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 638
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
        NumGlyphs = 2
      end
    end
    object PnConfirma: TPanel
      Left = 2
      Top = 15
      Width = 636
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object CkMeuXML: TCheckBox
        Left = 192
        Top = 8
        Width = 217
        Height = 29
        Caption = 'Enviar meu xml informado.'
        TabOrder = 0
      end
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        TabOrder = 1
        OnClick = BtOKClick
        NumGlyphs = 2
      end
    end
  end
end
