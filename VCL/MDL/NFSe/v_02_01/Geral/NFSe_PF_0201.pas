unit NFSe_PF_0201;

interface

uses
  Forms, StdCtrls, ComCtrls, ExtCtrls, Windows, Messages, SysUtils, Classes,
  Graphics, Controls, Dialogs, Variants, MaskUtils, mySQLDbTables, UnGrl_Vars,

  //Registry, ShellAPI, TypInfo, comobj, ShlObj, Winsock, Math, About,
  //mySQLDbTables, DB,(* DbTables,*) mySQLExceptions,
  XMLDoc,
  // Ver o que fazer!
  //NFSe_0201_MyRPS,
  // FIM NFS-e

  // ini 2023-12-20
  //DmkACBrNFSe, DMKpcnConversao, DMKpnfsConversao, DmkACBrNFSeUtil,
  ACBrNFSe, pcnConversao, pnfsConversao, ACBrBase,
  ACBrDFeSSL, synacode, blckSock,
  // fim 2023-12-20
  //
  UnMsgInt, dmkGeral, UnInternalConsts, NFSe_PF_0000, UnDmkProcFunc, UnDMkEnums;

type
  TNFSe_PF_0201 = class(TObject)
  private
    {private declaration}
    //FGravaCampo: Boolean;
    //Ok: Boolean;

  public
    {public declaration}
    function  CI(Valor, Alq: Double): Double;
    procedure ConfiguraComponenteACBrNFSe(Empresa: Integer; ACBrNFSeX: TACBrNFSe(*TdmkACBrNFSe*));
    procedure ConfiguraComponenteACBrNFSe_Antigo(Empresa: Integer; ACBrNFSeX: TACBrNFSe(*TdmkACBrNFSe*));
    procedure ConfiguraComponenteACBrNFSe_Novo(Empresa: Integer; ACBrNFSeX: TACBrNFSe(*TdmkACBrNFSe*));
    procedure MostraFormNFSe(SQLType: TSQLType;
              Prestador, Tomador, Intermediario, MeuServico, ItemListSrv:
              Integer; Discriminacao: String; GeraNFSe: Boolean; NFSeFatCab,
              DPS: Integer; Servico: TnfseFormaGerarNFSe;
              QrNFSeDPSCab: TmySQLQuery; Valor: Double;
              var Serie: String; var Numero: Integer; TabedForm: TForm);
    // Faturamento de servi�os
    function  InsUpdNFSeFatCab(Codigo, Empresa, Ambient: Integer; Competen,
              Nome: String; DtFatIni, HraFatIni: TDateTime; SQLTipo: TSQLType): Integer;
    function  CalculaDiscriminacao(const ValorServico, ValorDeducoes,
              DescontoIncondicionado: Double; const SrvindFinal, SrvImportado: Integer;
              const ItemListaServico, Discriminacao: String; var iTotTrib: Integer;
              var vTotTrib, vBasTrib, pTotTrib: Double; var fontTotTrib: TIBPTaxFont;
              var tabTotTrib: TIBPTaxTabs; var verTotTrib: String;
              var tpAliqTotTrib: TIBPTaxOrig; var Desistiu: Boolean): String;
    // Servi�os WS
    function  EnviaRPS_Unica(Form: TForm; DPS, LoteRPS: Integer;
              QryDPS: TmySQLQuery; LaAviso1, LaAviso2: TLabel): Boolean;
    function  SubstituiNFSe(Form: TForm; DPS, LoteRPS: Integer;
              QryDPS: TmySQLQuery; LaAviso1, LaAviso2: TLabel): Boolean;
    //
    function  MontaXML_RPS(const Empresa: Integer; const ACBrNFSeX: TACBrNFSe(*TdmkACBrNFSe*);
              const LoteRPS: Integer; var RPSTipo: Integer;
              var RPSSerie: String; var NumeroRPS: Integer): Boolean;

    function  LoadCanc(ACBrNFSeX: TACBrNFSe(*TdmkACBrNFSe*)): Boolean;
    //
{
    function  MontaXML_MyRPS_Dermatek(Codigo: Integer; LaAviso1, LaAviso2: TLabel): Boolean;
    function  GerarXML_RPS(): Boolean;
    // Recep��o de Lote de RPS
    // 4.5.1 (p�g 36 ABRASF 2.01)
    // Enviar Lote de RPS S�ncrono
    // 4.5.2 (p�g 36 ABRASF 2.01)
    function  MontaXML_LoteRPS(Codigo: Integer; Sincroniza: Boolean;
              LaAviso1, LaAviso2: TLabel;
              var XMLAssinado_Dir: String): Boolean;
}

{
    function  GerarXML_LoteRPS(LoteRPS: IXMLTcLoteRps): Boolean;
}

{
    // Consulta de NFS-e por RPS
    // 4.5.7 (p�g 39 ABRASF 2.01)
    function  MontaXML_CosultarNfseRpsEnvio(): Boolean;
}

    function  Def(const Tipo: String; Tam, Casas: Integer; Nulo: Boolean;
              var Dest: String; const Source: Variant;
              Obrigatorio: Boolean = False): Boolean;
    function  DefA(const Tipo: String; Tam, Casas: Integer; Nulo: Boolean;
              var Dest: String; const Query: TmySQLQuery; const Campo: String;
              Obrigatorio: Boolean = False): Boolean;
    function  GeraNFseUnica(Form: TForm; DPS, Empresa: Integer;
              FormaGerarNFSe: TnfseFormaGerarNFSe; QrDPS: TmySQLQuery;
              LaAviso1, LaAviso2: TLabel): Boolean;
    procedure ConsultaRPSselecionado_Novo(Form: TForm; Empresa, Ambiente,
              Codigo, RpsIDNumero: Integer; RpsIDSerie: String; RpsIDTipo:
              Integer; Caption: String; QrNFSeDPSCab: TmySQLQuery;
              LaAviso1, LaAviso2: TLabel);
  end;

var
  UnNFSe_PF_0201: TNFSe_PF_0201;


implementation

uses MyDBCheck, NFSe_Edit_0201, NFSe_0000_Module, ModuleGeral, (*NFSeABRASF_0201,*)
  UnMyObjects, UMySQLModule, Module, DmkDAC_PF, NFSeABRASF_0201;

{
var
  cXMLGerarNfseEnvio: IXMLMyRPS;
  cXMLEnviarLoteRpsEnvio: IXMLEnviarLoteRpsEnvio;
  cXMLEnviarLoteRpsSincronoEnvio: IXMLEnviarLoteRpsSincronoEnvio;
  cXMLTcDeclaracaoPrestacaoServico: IXMLTcDeclaracaoPrestacaoServico;
  arqXML: TXMLDocument;
  (* Objetos do Documento XML... *)
  DPS: IXMLTcInfDeclaracaoPrestacaoServico;
  LoteRPS: IXMLTcLoteRps;
}


//const
  //CO_XML_VERSION_E_ENCODING = '<?xml version="1.0" encoding="UTF-8"?>';

{ TNFSe_PF }

function TNFSe_PF_0201.Def(const Tipo: String; Tam, Casas: Integer; Nulo:
  Boolean; var Dest: String; const Source: Variant; Obrigatorio: Boolean): Boolean;
var
  //Continua, Avisa: Boolean;
  //T: Integer;
  Fmt: String;
begin
  Result := False;
  Dest   := '';
  // Integer, Float
  if Tipo = 'C' then
  begin
    Dest := String(Source);
    if Nulo then
      Result := True
    else
      Result := Trim(Dest) <> '';
  end else
  if Tipo = 'N' then
  begin
    Dest := Geral.DecimalPonto(FloatToStrF(Source, ffFixed, Tam, Casas));
    (*
    Tam := Length(Dest);
    if Copy(Dest, Tam - 2) = '.00' then
      Dest := Copy(Dest, 1, Tam - 3)
    else
    if Copy(Dest, Tam) = '0' then
      Dest := Copy(Dest, 1, Tam - 1);
    *)
    //
    if Nulo then
      Result := True
    else
      Result := Source >= 0.01;
  end else
  // data
  if Tipo = 'D' then
  begin
    case Tam of
      8:  Fmt := 'YYYY-MM-DD';
      14: Fmt := 'YYYY-MM-DD''T''HH:NN:SS';
      else begin
        Result := False;
        Geral.MB_Aviso('Formato de DATA n�o implementado: ' + Geral.FF0(Tam) +
          '.' + Geral.FF0(Casas) + sLineBreak + 'Informe a DERMATEK!');
        Exit;
      end;
    end;
    Dest := FormatDateTime(Fmt, Source);
    if Nulo then
      Result := True
    else
      Result := Source > 2;
  end else
  // hora
  if Tipo = 'H' then
  begin
    case Tam of
      6:  Fmt := 'hh:nn:ss';
      else begin
        Result := False;
        Geral.MB_Aviso('Formato de HORA n�o implementado: ' + Geral.FF0(Tam) +
          '.' + Geral.FF0(Casas) + sLineBreak + 'Informe a DERMATEK!');
        Exit;
      end;
    end;
    Dest := FormatDateTime(Fmt, Source);
    if Nulo then
      Result := True
    else
      Result := Source > 2;
  end else
    Geral.MB_Erro('NFS-e:' + sLineBreak +  'Valor do tipo "' + Tipo + '"'
      + sLineBreak + 'N�o foi poss�vel definir o valor');
  //
end;

function TNFSe_PF_0201.DefA(const Tipo: String; Tam, Casas: Integer;
  Nulo: Boolean; var Dest: String; const Query: TmySQLQuery; const Campo: String;
  Obrigatorio: Boolean): Boolean;
var
  Source: Variant;
  Msg: String;
begin
  Source := Query.FieldByName(Campo).AsVariant;
  Result := Def(Tipo, Tam, Casas, Nulo, Dest, Source, Obrigatorio);
  if Obrigatorio and not Result then
  begin
    Msg := 'Gera��o de XML inv�lido!' + sLineBreak +
    'Campo obrigat�rio: ' + Campo;
    Geral.MB_Erro(Msg);
    raise Exception.Create(Msg);
    Exit;
  end;
end;

function TNFSe_PF_0201.GeraNFseUnica(Form: TForm; DPS, Empresa: Integer;
  FormaGerarNFSe: TnfseFormaGerarNFSe; QrDPS: TmySQLQuery;
  LaAviso1, LaAviso2: TLabel): Boolean;
const
  TipoDoc = tdnfseDPS;
  NFSeFatCab = 0;
  QuantidadeRps = 1;
  Sincroniza = True;
var
  //XMLGerado_Arq, XMLGerado_Dir, XMLAssinado_Dir: String;
  NFSeLRpsC: Integer;
begin
  Result := False;
  // Cria novo lote de RPS
  NFSeLRpsC := DmNFSe_0000.InsUpdLoteRPScab(Empresa, NFSeFatCab, QuantidadeRps, Sincroniza);
  if NFSeLRpsC <> 0 then
  begin
    if DmNFSe_0000.InsUpdLoteRPSIts(NFSeLRpsC, DPS, LaAviso1, LaAviso2) then
    begin
      if FormaGerarNFSe = fgnSubstituicaoNFSe then
      begin
        UnNFSe_PF_0201.SubstituiNFSe(Form, DPS, NFSeLRpsC,
           QrDPS, LaAviso1, LaAviso2);
      end else
      begin
        (*1:*) Result := UnNFSe_PF_0201.EnviaRPS_Unica(Form, DPS, NFSeLRpsC,
                           QrDPS, LaAviso1, LaAviso2);

        (* C�digo funciona, mas desativei para n�o confundir
        case RGFormaGeraEEnvia.ItemIndex of
          2:
          begin
            UnNFSe_PF_0201.MontaXML_MyRPS_Dermatek(DPS, LaAviso1, LaAviso2);
            DmNFSe_0000.ReopenEmpresa(Empresa);
            DModG.ReopenParamsEmp(Empresa);
            XMLGerado_Arq := Geral.FFN(DPS, 8) + NFSE_DPS_GER;
            XMLGerado_Dir := DmNFSe_0000.QrFilialDirNFSeDPSGer.Value;
            //
            //  Assinar o RPS
            DmNFSe_0000.AssinarArquivoXML_NFSe(XMLGerado_Arq, XMLGerado_Dir,
            Empresa, TipoDoc, DPS, LaAviso1, LaAviso2, XMLAssinado_Dir);
            //
            // Cria novo lote de RPS
            NFSeLRpsC := DmNFSe_0000.InsUpdLoteRPScab(Empresa, NFSeFatCab, QuantidadeRps, Sincroniza);
            if NFSeLRpsC <> 0 then
            begin
              if DmNFSe_0000.InsUpdLoteRPSIts(NFSeLRpsC, DPS, LaAviso1, LaAviso2) then
              begin
                if UnNFSe_PF_0201.MontaXML_LoteRPS(NFSeLRpsC, Sincroniza,
                LaAviso1, LaAviso2, XMLAssinado_Dir) then
                begin
                  if DBCheck.CriaFm(TFmNFSe_Steps_0201, FmNFSe_Steps_0201, afmoNegarComAviso) then
                  begin
                    FmNFSe_Steps_0201.EnviarLoteRpsSincronoEnvio_Prepara(
                      NFSeLRpsC, Empresa, XMLAssinado_Dir);
                    FmNFSe_Steps_0201.ShowModal;
                    FmNFSe_Steps_0201.Destroy;
                  end;
                end;
              end;
            end;
          end;
        end;
        *)
      end;
    end;
  end;
end;

function TNFSe_PF_0201.InsUpdNFSeFatCab(Codigo, Empresa, Ambient: Integer;
  Competen, Nome: String; DtFatIni, HraFatIni: TDateTime; SQLTipo: TSQLType): Integer;
var
  DataFatIni, HoraFatIni, m: String;
  Competenci, Ambiente: Integer;
begin
  Result := 0;
  //
  if MyObjects.FIC(Empresa = 0, nil, 'Informe a empresa!') then Exit;
  //
  if SQLTipo = stIns then
  begin
    DataFatIni := Geral.FDT(Date, 1);
    HoraFatIni := Geral.FDT(Time, 100);
  end else
  begin
    DataFatIni := Geral.FDT(DtFatIni, 1);
    HoraFatIni := Geral.FDT(DtFatIni, 100);
  end;
  //
  m := Competen;
  //
  if Length(m) = 7 then
    Competenci := Geral.IMV(Copy(m, 4, 4) + Copy(m, 1, 2))
  else
    Competenci := 0;
  //
  if SQLTipo = stIns then
  begin
    DModG.ReopenParamsEmp(Empresa);
    Ambiente := DmodG.QrParamsEmpNFSeAmbiente.Value
  end else
    Ambiente := Ambient;
  //
  Codigo := UMyMod.BPGS1I32('nfsefatcab', 'Codigo', '', '', tsPos, SQLTipo, Codigo);
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLTipo, 'nfsefatcab', False,
    [
    'Nome', 'Empresa', 'DataFatIni', 'HoraFatIni', (*'DataFatFim', 'HoraFatFim',*)
    'Competenci', 'Ambiente'
    ], ['Codigo'],
    [
    Nome, Empresa, DataFatIni,
    HoraFatIni, (*DataFatFim, HoraFatFim,*)
    Competenci, Ambiente
    ], [Codigo], True) then
  begin
    Result := Codigo;
  end;
end;

{
function TNFSe_PF_0201.GerarXML_LoteRPS(LoteRPS: IXMLTcLoteRps): Boolean;
var
  K, T, M, pIni, pFim: Integer;
  Valor, IM, x: String;
  infAdProd, XML_DPS: WideString;
begin
  Result := False;

  //
  if Def('C', 00, 00, False, x, DmNFSe_0000.QRNFSeLRpsCId.Value, True) then
    LoteRPS.Id := x;
  //
  if Def('C', 00, 00, False, x, DmNFSe_0000.QRNFSeLRpsCNumeroLote.Value, True) then
    LoteRPS.NumeroLote := StrToInt(x);
  //  PRESTADOR
  if DmNFSe_0000.QRNFSeLRpsCCnpj.Value <> '' then
  begin
    if Def('C', 14, 00, False, x, DmNFSe_0000.QRNFSeLRpsCCnpj.Value, True) then
      LoteRPS.CpfCnpj.Cnpj := x;
  end else
  begin
    if Def('C', 11, 00, False, x, DmNFSe_0000.QRNFSeLRpsCCpf.Value, True) then
      LoteRPS.CpfCnpj.Cpf := x;
  end;
  //
  if Def('C', 00, 00, False, x, DmNFSe_0000.QRNFSeLRpsCInscricaoMunicipal.Value) then
    LoteRPS.InscricaoMunicipal := x;
  if Def('C', 00, 00, False, x, DmNFSe_0000.QRNFSeLRpsCQuantidadeRps.Value, True) then
    LoteRPS.QuantidadeRps := StrToInt(x);
  //

      LoteRPS.ListaRps.Add;
      LoteRPS.ListaRps.Rps[0].Text := 'MINHA_LISTA_RPS';

  (* N�o funciona troca '<' por '&lt;'  e '>' por '&gt;'
  DmNFSe_0000.ReopenNFSeLRpsCIts(Codigo);
  DmNFSe_0000.QrNFSeLRpsI.First;
  while not DmNFSe_0000.QrNFSeLRpsI.Eof do
  begin
    DmNFSe_0000.ReopenNFSeArq(DmNFSe_0000.QRNFSeLRpsINFSeDPSCab.Value);
    if DmNFSe_0000.QrNFSeArqXML_DPS.Value <> '' then
    begin
      LoteRPS.ListaRps.Add;
      T := Length('<Rps');
      XML_DPS := DmNFSe_0000.QrNFSeArqXML_DPS.Value;
      XML_DPS := Geral.Substitui(XML_DPS, '""', '"');
      pIni := pos('<InfDeclaracaoPrestacaoServico', XML_DPS);
      XML_DPS := Copy(XML_DPS, pIni);
      T := Length('</Rps>');
      for M := Length(XML_DPS) - T + 1 downto 0 do
      begin
        if Copy(XML_DPS, M, T) = '</Rps>' then
        begin
          pFim := M;
          Break;
        end;
      end;
      XML_DPS := Copy(XML_DPS, 1, pFim - 1);
      LoteRPS.ListaRps.Rps[K].Text := XML_DPS;
    end else
    begin
      Geral.MB_Erro('N�o foi poss�vel obter o XML da DPS n� ' +
        Geral.FF0(DmNFSe_0000.QRNFSeLRpsINFSeDPSCab.Value));
      //
      Exit;
    end;
    //
    DmNFSe_0000.QrNFSeLRpsI.Next;
  end;
  *)
  if Def('N', 04, 02, False, x, DmNFSe_0000.QRNFSeLRpsCversao.Value) then
    LoteRPS.Versao := x;
  //
  Result := True;
end;
}

{
function TNFSe_PF_0201.GerarXML_RPS(): Boolean;
var
  i, h, j: Integer;
  Valor, IM, x: String;
  infAdProd: WideString;
begin
  Result := False;
  (*
  Geral.MB_Aviso(cXMLGerarNfseEnvio.Rps.NamespaceURI);
  Geral.MB_Aviso(cXMLGerarNfseEnvio.Rps.LocalName);
  Geral.MB_Aviso(cXMLGerarNfseEnvio.Rps.NodeName);
  Geral.MB_Aviso(cXMLGerarNfseEnvio.Rps.Prefix);
  Geral.MB_Aviso(cXMLGerarNfseEnvio.Rps.Signature);
  //Geral.MB_Aviso(cXMLGerarNfseEnvio.Rps.Text);
  //Geral.MB_Aviso(cXMLGerarNfseEnvio.Rps.XML);
  *)
  //
  DPS := cXMLGerarNfseEnvio.Rps.InfDeclaracaoPrestacaoServico;
  //
  // N�o Usa? DPS.Id := '';
  //
  // RPS
  if DefA('C', 00, 00, False, x, DmNFSe_0000.QrNFSeDPSCab, 'RpsID') then
    DPS.Rps.Id := x;
  if DefA('N', 15, 00, False, x, DmNFSe_0000.QrNFSeDPSCab, 'RpsIDNumero', True) then
    DPS.Rps.IdentificacaoRps.Numero := StrToInt(x);
  if DefA('N', 05, 00, False, x, DmNFSe_0000.QrNFSeDPSCab, 'RpsIDSerie', True) then
    DPS.Rps.IdentificacaoRps.Serie := x;
  if DefA('N', 01, 00, False, x, DmNFSe_0000.QrNFSeDPSCab, 'RpsIDTipo', True) then
    DPS.Rps.IdentificacaoRps.Tipo := StrToInt(x);
  //
  if DefA('D', 08, 00, False, x, DmNFSe_0000.QrNFSeDPSCab, 'RpsDataEmissao', True) then
    DPS.Rps.DataEmissao := x;
  if DefA('N', 01, 00, False, x, DmNFSe_0000.QrNFSeDPSCab, 'RpsStatus', True) then
    DPS.Rps.Status := StrToInt(x);
  //
  // RPS SUBSTITUIDA
  if (DmNFSe_0000.QrNFSeDPSCabSubstNumero.Value > 0)
  or (DmNFSe_0000.QrNFSeDPSCabSubstSerie.Value <> '')
  or (DmNFSe_0000.QrNFSeDPSCabSubstTipo.Value <> 0) then
  begin
    if DefA('N', 15, 00, False, x, DmNFSe_0000.QrNFSeDPSCab, 'SubstNumero', True) then
      DPS.Rps.RpsSubstituido.Numero := StrToInt(x);
    if DefA('N', 05, 00, False, x, DmNFSe_0000.QrNFSeDPSCab, 'SubstSerie', True) then
      DPS.Rps.RpsSubstituido.Serie := x;
    if DefA('N', 01, 00, False, x, DmNFSe_0000.QrNFSeDPSCab, 'SubstTipo', True) then
      DPS.Rps.RpsSubstituido.Tipo := StrToInt(x);
  end;
  //
  // COMPETENCIA (Data da realiza��o do servico)
  if DefA('D', 08, 00, False, x, DmNFSe_0000.QrNFSeDPSCab, 'Competencia', True) then
    DPS.Competencia := x;
  //
  //
  //  VALORES SERVICO
  if DefA('N', 15, 02, False, x, DmNFSe_0000.QrNFSeDPSCab, 'ValorServicos', True) then
    DPS.Servico.Valores.ValorServicos := x;
  if DefA('N', 15, 02, False, x, DmNFSe_0000.QrNFSeDPSCab, 'ValorDeducoes') then
    DPS.Servico.Valores.ValorDeducoes := x;
  if DefA('N', 15, 02, False, x, DmNFSe_0000.QrNFSeDPSCab, 'ValorPis') then
    DPS.Servico.Valores.ValorPis := x;
  if DefA('N', 15, 02, False, x, DmNFSe_0000.QrNFSeDPSCab, 'ValorCofins') then
    DPS.Servico.Valores.ValorCofins := x;
  if DefA('N', 15, 02, False, x, DmNFSe_0000.QrNFSeDPSCab, 'ValorInss') then
    DPS.Servico.Valores.ValorInss := x;
  if DefA('N', 15, 02, False, x, DmNFSe_0000.QrNFSeDPSCab, 'ValorIr') then
    DPS.Servico.Valores.ValorIr := x;
  if DefA('N', 15, 02, False, x, DmNFSe_0000.QrNFSeDPSCab, 'ValorCsll') then
    DPS.Servico.Valores.ValorCsll := x;
  if DefA('N', 15, 02, False, x, DmNFSe_0000.QrNFSeDPSCab, 'OutrasRetencoes') then
    DPS.Servico.Valores.OutrasRetencoes := x;
  if DefA('N', 15, 02, False, x, DmNFSe_0000.QrNFSeDPSCab, 'ValorIss') then
    DPS.Servico.Valores.ValorIss := x;
  if DefA('N', 15, 02, False, x, DmNFSe_0000.QrNFSeDPSCab, 'Aliquota') then
    DPS.Servico.Valores.Aliquota := x;
  if DefA('N', 15, 02, False, x, DmNFSe_0000.QrNFSeDPSCab, 'DescontoIncondicionado') then
    DPS.Servico.Valores.DescontoIncondicionado := x;
  if DefA('N', 15, 02, False, x, DmNFSe_0000.QrNFSeDPSCab, 'DescontoCondicionado') then
    DPS.Servico.Valores.DescontoCondicionado := x;
  // DADOS SERVICO
  if DefA('N', 01, 00, False, x, DmNFSe_0000.QrNFSeDPSCab, 'IssRetido', True) then
    DPS.Servico.IssRetido := StrToInt(x);
  if DefA('N', 01, 00, False, x, DmNFSe_0000.QrNFSeDPSCab, 'ResponsavelRetencao') then
    DPS.Servico.ResponsavelRetencao := StrToInt(x);
  if DefA('C', 05, 00, False, x, DmNFSe_0000.QrNFSeDPSCab, 'ItemListaServico', True) then
    DPS.Servico.ItemListaServico := x;
  if DefA('N', 07, 00, False, x, DmNFSe_0000.QrNFSeDPSCab, 'CodigoCnae', True) then
    DPS.Servico.CodigoCnae := StrToInt(x);
  if DefA('C', 20, 00, False, x, DmNFSe_0000.QrNFSeDPSCab, 'CodigoTributacaoMunicipio') then
    DPS.Servico.CodigoTributacaoMunicipio := x;
  if DefA('C', 2000, 0, False, x, DmNFSe_0000.QrNFSeDPSCab, 'Discriminacao', True) then
    DPS.Servico.Discriminacao := x;
  if DefA('N', 07, 00, False, x, DmNFSe_0000.QrNFSeDPSCab, 'CodigoMunicipio', True) then
    DPS.Servico.CodigoMunicipio := StrToInt(x);
  if DefA('C', 04, 00, False, x, DmNFSe_0000.QrNFSeDPSCab, 'CodigoPais') then
    DPS.Servico.CodigoPais := x;
  if DefA('N', 02, 00, False, x, DmNFSe_0000.QrNFSeDPSCab, 'ExigibilidadeISS', True) then
    DPS.Servico.ExigibilidadeISS := StrToInt(x);
  if DefA('N', 07, 00, False, x, DmNFSe_0000.QrNFSeDPSCab, 'MunicipioIncidencia') then
    DPS.Servico.MunicipioIncidencia := StrToInt(x);
  if DefA('C', 30, 00, False, x, DmNFSe_0000.QrNFSeDPSCab, 'NumeroProcesso') then
    DPS.Servico.NumeroProcesso := x;
  //
  if DefA('C', 30, 00, False, x, DmNFSe_0000.QrNFSeDPSCab, 'NumeroProcesso') then
    DPS.Servico.NumeroProcesso := x;
  //
  //
  //  PRESTADOR
  if DmNFSe_0000.QrNFSeDPSCabPrestaCnpj.Value <> '' then
  begin
    if DefA('C', 14, 00, False, x, DmNFSe_0000.QrNFSeDPSCab, 'PrestaCnpj', True) then
      DPS.Prestador.CpfCnpj.Cnpj := x;
  end else
  begin
    if DefA('C', 11, 00, False, x, DmNFSe_0000.QrNFSeDPSCab, 'PrestaCpf', True) then
      DPS.Prestador.CpfCnpj.Cpf := x;
  end;
  if DefA('C', 15, 00, False, x, DmNFSe_0000.QrNFSeDPSCab, 'PrestaInscricaoMunicipal') then
    DPS.Prestador.InscricaoMunicipal := x;
  //
  //
  // TOMADOR
  if DmNFSe_0000.QrNFSeDPSCabTomaCnpj.Value <> '' then
  begin
    if DefA('C', 14, 00, False, x, DmNFSe_0000.QrNFSeDPSCab, 'TomaCnpj') then
      DPS.Tomador.IdentificacaoTomador.CpfCnpj.Cnpj := x;
  end else
  begin
    if DefA('C', 11, 00, False, x, DmNFSe_0000.QrNFSeDPSCab, 'TomaCpf') then
      DPS.Tomador.IdentificacaoTomador.CpfCnpj.Cpf := x;
  end;
  if DefA('C', 15, 00, False, x, DmNFSe_0000.QrNFSeDPSCab, 'TomaInscricaoMunicipal') then
    DPS.Tomador.IdentificacaoTomador.InscricaoMunicipal := x;
  if DefA('C', 150, 01, False, x, DmNFSe_0000.QrNFSeDPSCab, 'TomaRazaoSocial') then
    DPS.Tomador.RazaoSocial := x;
  // Endereco
  if DefA('C', 125, 00, False, x, DmNFSe_0000.QrNFSeDPSCab, 'TomaEndereco') then
    DPS.Tomador.Endereco.Endereco := x;
  if DefA('C', 10, 00, False, x, DmNFSe_0000.QrNFSeDPSCab, 'TomaNumero') then
    DPS.Tomador.Endereco.Numero := x;
  if DefA('C', 60, 00, False, x, DmNFSe_0000.QrNFSeDPSCab, 'TomaComplemento') then
    DPS.Tomador.Endereco.Complemento := x;
  if DefA('C', 60, 00, False, x, DmNFSe_0000.QrNFSeDPSCab, 'TomaBairro') then
    DPS.Tomador.Endereco.Bairro := x;
  if DefA('N', 07, 00, False, x, DmNFSe_0000.QrNFSeDPSCab, 'TomaCodigoMunicipio') then
    DPS.Tomador.Endereco.CodigoMunicipio := StrToInt(x);
  if DefA('C', 02, 00, False, x, DmNFSe_0000.QrNFSeDPSCab, 'TomaUf') then
    DPS.Tomador.Endereco.Uf := x;
  if DefA('C', 04, 00, False, x, DmNFSe_0000.QrNFSeDPSCab, 'TomaCodigoPais') then
    DPS.Tomador.Endereco.CodigoPais := x;
  if DefA('C', 08, 00, False, x, DmNFSe_0000.QrNFSeDPSCab, 'TomaCep') then
    DPS.Tomador.Endereco.Cep := x;
  // Contato
  if DefA('C', 80, 00, False, x, DmNFSe_0000.QrNFSeDPSCab, 'TomaContatoEmail') then
    DPS.Tomador.Contato.Email := x;
  (*
    Maring� n�o tem!
  if DefA('C', 80, 00, False, x, DmNFSe_0000.QrNFSeDPSCab, 'TomaContatoTelefone') then
    DPS.Tomador.Contato.Telefone := x;
  *)
  //
  //
  // INTERMEDIARIO
  if DmNFSe_0000.QrNFSeDPSCabIntermediario.Value <> 0 then
  begin
    if DmNFSe_0000.QrNFSeDPSCabIntermeCnpj.Value <> '' then
    begin
      if DefA('C', 14, 00, False, x, DmNFSe_0000.QrNFSeDPSCab, 'IntermeCnpj') then
        DPS.Intermediario.IdentificacaoIntermediario.CpfCnpj.Cnpj := x;
    end else
    begin
      if DefA('C', 11, 00, False, x, DmNFSe_0000.QrNFSeDPSCab, 'IntermeCpf') then
        DPS.Intermediario.IdentificacaoIntermediario.CpfCnpj.Cpf := x;
    end;
    if DefA('C', 15, 00, False, x, DmNFSe_0000.QrNFSeDPSCab, 'IntermeInscricaoMunicipal') then
      DPS.Intermediario.IdentificacaoIntermediario.InscricaoMunicipal := x;
    if DefA('C', 150, 00, False, x, DmNFSe_0000.QrNFSeDPSCab, 'IntermeRazaoSocial') then
      DPS.Intermediario.RazaoSocial := x;
  end;
  //
  //
  // Constru��o civil
  if (DmNFSe_0000.QrNFSeDPSCabConstrucaoCivilArt.Value <> '')
  or (DmNFSe_0000.QrNFSeDPSCabConstrucaoCivilCodigoObra.Value <> '') then
  begin
    if DefA('C', 15, 00, False, x, DmNFSe_0000.QrNFSeDPSCab, 'ConstrucaoCivilCodigoObra') then
      DPS.ConstrucaoCivil.CodigoObra := x;
    if DefA('C', 15, 00, False, x, DmNFSe_0000.QrNFSeDPSCab, 'ConstrucaoCivilArt') then
      DPS.ConstrucaoCivil.Art := x;
  end;
  //  Outros tributa��o
  if DefA('N', 02, 00, False, x, DmNFSe_0000.QrNFSeDPSCab, 'RegimeEspecialTributacao') then
    DPS.RegimeEspecialTributacao := StrToInt(x);
  if DefA('N', 01, 00, False, x, DmNFSe_0000.QrNFSeDPSCab, 'OptanteSimplesNacional', True) then
    DPS.OptanteSimplesNacional := StrToInt(x);
  if DefA('N', 01, 00, False, x, DmNFSe_0000.QrNFSeDPSCab, 'IncentivoFiscal', True) then
    DPS.IncentivoFiscal := StrToInt(x);
  //
  Result := True;
end;
}

function TNFSe_PF_0201.LoadCanc(ACBrNFSeX: TACBrNFSe(*TdmkACBrNFSe*)): Boolean;
begin
  // ini 2023-12-20 Parei Aqui!
  Geral.MB_Aviso('Falta implementar 2023-12-20!');
{
  with ACBrNFSeX.NotasFiscais.Items[0].NFSe do
  begin
    PrestadorServico.IdentificacaoPrestador.Cnpj :=
      Geral.SoNumero_TT(DmNFSe_0000.QrNFSeDPSCabPrestaCnpj.Value);
    PrestadorServico.IdentificacaoPrestador.Cpf :=
      Geral.SoNumero_TT(DmNFSe_0000.QrNFSeDPSCabPrestaCpf.Value);
    PrestadorServico.IdentificacaoPrestador.InscricaoMunicipal :=
      Geral.SoNumero_TT(DmNFSe_0000.QrNFSeDPSCabPrestaInscricaoMunicipal.Value);
    PrestadorServico.Endereco.CodigoMunicipio :=
      Geral.FF0(DmNFSe_0000.QrNFSeDPSCabCodigoMunicipio.Value);
  end;
  Result := True;
}
  // fim 2023-12-20 Parei Aqui!
end;

{
function TNFSe_PF_0201.MontaXML_CosultarNfseRpsEnvio: Boolean;
var
  XML: WideString;
begin
(*
  XML :=
  CO_XML_VERSION_E_ENCODING +
  <ConsultarNfseRpsEnvio xmlns="http://www.abrasf.org.br/nfse.xsd"
  xmlns:ds="http://www.w3.org/2000/09/xmldsig#"
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xsi:schemaLocation="http://www.abrasf.org.br/nfse.xsd nfse_v2.01.xsd ">
  <IdentificacaoRps>
  <Numero>1</Numero>
  <Serie>1</Serie>
  <Tipo>1</Tipo>
  </IdentificacaoRps>
  <Prestador>
  <CpfCnpj>
  <Cnpj>99999999999999</Cnpj>
  </CpfCnpj>
  <InscricaoMunicipal>999999</InscricaoMunicipal>
  </Prestador>
  </ConsultarNfseRpsEnvio>
*)

end;
}

{
function TNFSe_PF_0201.MontaXML_LoteRPS(Codigo: Integer; Sincroniza: Boolean;
LaAviso1, LaAviso2: TLabel; var XMLAssinado_Dir: String): Boolean;
var
  RpsId: String;
  XML_DPS: AnsiString;
  XML_LOT, XML: WideString;
  P, M, T, pIni, pFim: Integer;
  XMLGerado_Arq, XMLGerado_Dir: String;
begin
  Result := False;
  //
  DmNFSe_0000.ReopenNFSeLRpsC(Codigo);
  //Empresa := DmNFSe_0000.QrNFSeLRpsCEmpresa.Value;
  RpsId := UnNFSe_PF_0000.FormataRPSNumero(Codigo);
(* Criando o Documento XML e Gravando cabe�alho... *)
  arqXML := TXMLDocument.Create(nil);
  arqXML.Active := False;
  arqXML.FileName := '';
  if Sincroniza then
  begin
    cXMLEnviarLoteRpsSincronoEnvio := GetEnviarLoteRpsSincronoEnvio(arqXML);
    LoteRPS := cXMLEnviarLoteRpsSincronoEnvio.LoteRps;
  end else
  begin
    cXMLEnviarLoteRpsEnvio := GetEnviarLoteRpsEnvio(arqXML);
    LoteRPS := cXMLEnviarLoteRpsEnvio.LoteRps;
  end;
  arqXML.Version := '1.0';
  arqXML.Encoding := 'UTF-8';
  arqXML.DocumentElement.Attributes['xmlns:ds'] := 'http://www.w3.org/2000/09/xmldsig#';
  arqXML.DocumentElement.Attributes['xmlns:xsi'] := 'http://www.w3.org/2001/XMLSchema-instance';
  arqXML.DocumentElement.Attributes['xsi:schemaLocation'] := 'http://www.abrasf.org.br/nfse.xsd nfse_v2.01.xsd';
  if GerarXML_LoteRPS(LoteRps) then
  begin
    XML := '';
    DmNFSe_0000.ReopenNFSeLRpsCIts(Codigo);
    DmNFSe_0000.QrNFSeLRpsI.First;
    while not DmNFSe_0000.QrNFSeLRpsI.Eof do
    begin
      XML_DPS := '';
      if DmNFSe_0000.ReopenNFSeArq(txmRPS,
      DmNFSe_0000.QRNFSeLRpsIEmpresa.Value,
      DmNFSe_0000.QRNFSeLRpsIAmbiente.Value,
      DmNFSe_0000.QRNFSeLRpsINFSeDPSCab.Value,
      XML_DPS, 'NFSe_PF_0201.MontaXML_LoteRPS') then
      begin
        T := Length('<Rps');
        XML_DPS := DmNFSe_0000.QrNFSeArqXML.Value;
        XML_DPS := Geral.Substitui(XML_DPS, '""', '"');
        // Exigencia Maring�
        XML_DPS := Geral.Substitui(XML_DPS,
          '<Rps xmlns="http://www.abrasf.org.br/">',
          '<Rps xmlns="http://www.abrasf.org.br/nfse.xsd">');
        //
        pIni := pos('<Rps', XML_DPS);
        XML_DPS := Copy(XML_DPS, pIni);
        T := Length('</Rps>');
        for M := Length(XML_DPS) - T + 1 downto 0 do
        begin
          if Copy(XML_DPS, M, T) = '</Rps>' then
          begin
            pFim := M;
            Break;
          end;
        end;
        XML_DPS := Copy(XML_DPS, 1, pFim - 1 + T);
        XML := XML + XML_DPS;
      end else
      begin
        Geral.MB_Erro('N�o foi poss�vel obter o XML da DPS n� ' +
          Geral.FF0(DmNFSe_0000.QRNFSeLRpsINFSeDPSCab.Value));
        //
        Exit;
      end;
      //
      DmNFSe_0000.QrNFSeLRpsI.Next;
    end;
    //
    XML_DPS := XML;
    XML_LOT := arqXML.XML.Text;
    //P := pos('<EnviarLoteRps', XML_LOT);
    //XML_LOT := CO_XML_VERSION_E_ENCODING + Copy(XML_LOT, P);
    XML := Geral.Substitui(XML_LOT, '<Rps>MINHA_LISTA_RPS</Rps>', XML_DPS);
    // Salvar XML Sem Assinar
    Result := DmNFSe_0000.SalvaXML(
      NFSE_RPS_ENV_LOT, RpsId, XML, nil, False) <> '';
    XMLGerado_Arq := UnNFSe_PF_0000.FormataRPSNumero(Codigo) + NFSE_RPS_ENV_LOT;
    XMLGerado_Dir := DmNFSe_0000.QrFilialDirNFSeRPSEnvLot.Value;
    //
    DmNFSe_0000.AssinarArquivoXML_NFSe(XMLGerado_Arq, XMLGerado_Dir,
    DmNFSe_0000.QrNFSeLRpsCEmpresa.Value, tdnfseLoteRps, Codigo,
      LaAviso1, LaAviso2, XMLAssinado_Dir);
    //
    Result := XMLAssinado_Dir <> '';
  end;
  arqXML := nil;
end;
}

{
function TNFSe_PF_0201.MontaXML_MyRPS_Dermatek(Codigo: Integer; LaAviso1, LaAviso2: TLabel): Boolean;
var
  RpsId: String;
  Empresa, pI, pF: Integer;
  XML: WideString;
begin
  Result := False;
  //
  DmNFSe_0000.ReopenNFSeDPSCab(Codigo);
  Empresa := DmNFSe_0000.QrNFSeDPSCabEmpresa.Value;
  //RpsId := Geral.FFN(Codigo, 8);
  RpsId := UnNFSe_PF_0000.FormataRPSNumero(Codigo);
(* Criando o Documento XML e Gravando cabe�alho... *)
  arqXML := TXMLDocument.Create(nil);
  arqXML.Active := False;
  arqXML.FileName := '';
  cXMLGerarNfseEnvio := GetMyRPS(arqXML);
  arqXML.Version := '1.0';
  arqXML.Encoding := 'UTF-8';
  arqXML.DocumentElement.Attributes['xmlns:ds'] := 'http://www.w3.org/2000/09/xmldsig#';
  arqXML.DocumentElement.Attributes['xmlns:xsi'] := 'http://www.w3.org/2001/XMLSchema-instance';
  arqXML.DocumentElement.Attributes['xsi:schemaLocation'] := 'http://www.abrasf.org.br/nfse.xsd nfse_v2.01.xsd';
  //xmlns="http://www.abrasf.org.br/nfse.xsd"
  if GerarXML_RPS() then
  begin
    pI := pos('<Rps', arqXML.XML.Text);
    pF := pos('</Rps></MyRPS>', arqXML.XML.Text);
    XML := copy(arqXML.XML.Text, pI, pF - pI + Length('</Rps>'));
    //XML := copy(arqXML.XML.Text, pI, pF - pI + Length('</Rps>'));
    //XML := CO_XML_VERSION_E_ENCODING + XML;
    DmNFSe_0000.ReopenEmpresa(Empresa);
    //
    if DmNFSe_0000.SalvaXML(NFSE_DPS_GER, RpsId, XML, nil, False) <> '' then
    begin
      DmNFSe_0000.StepNFSeRPS_Solo(Codigo, DmNFSe_0000.stepGerada(), [], [],
        LaAviso1, LaAviso2);
    end;
  end;
  arqXML := nil;
end;
}


function TNFSe_PF_0201.CalculaDiscriminacao(const ValorServico, ValorDeducoes,
  DescontoIncondicionado: Double; const SrvindFinal, SrvImportado: Integer;
  const ItemListaServico, Discriminacao: String; var iTotTrib: Integer;
  var vTotTrib, vBasTrib, pTotTrib: Double; var fontTotTrib: TIBPTaxFont;
  var tabTotTrib: TIBPTaxTabs; var verTotTrib: String;
  var tpAliqTotTrib: TIBPTaxOrig; var Desistiu: Boolean): String;
var
  InformaTrib: Boolean;
  InfTotTrib, FonteStr, LC116, Chave: String;
  IndFinal: TNFeIndFinal;
  //Qry: TmySQLQuery;
  Importado: Boolean;
  vServ, vDesc: Double;
  //
begin
  // INICIO LEI N� 12.741 - Lei da Transparencia
  //  Informacao do total aproximados dos tributos na Discriminacao:
  Desistiu    := False;
  InfTotTrib  := '';
  InformaTrib := False;
  indFinal    := TNFeIndFinal(SrvindFinal);
  //
  case TindFinalTribNFe(DModG.QrParamsEmpNFSe_indFinalCpl.Value) of
    infFinTribIndefinido: InformaTrib := False;
    infFinTribSoConsumidorFinal: InformaTrib :=  IndFinal = indfinalConsumidorFinal;
    infFinTribTodos: InformaTrib := True;
    else Geral.MB_Erro(
    '"InformaTribCpl" n�o implementado em "DmNFe_0000.TotaisNFe()"');
  end;
  if InformaTrib then
  begin
    FonteStr := '';
    //
    LC116 := Geral.SoNumero_TT(ItemListaServico);
    vServ := ValorServico;
(*
Manual de Integra��o de Olho no Imposto - vers�o 0.0.6
  i) Quando a empresa oferece desconto incondicional, deve considerar este
  desconto para exibir a carga tribut�ria aproximada?
  Sim, deve considerar o desconto incondicional, j� que sobre ele n�o h�
  incid�ncia tribut�ria. Naturalmente, o desconto deve ser calculado item a
  item dos produtos vendidos, j� que o c�lculo da tributa��o para o atendimento
  da lei 12.741/2012 ocorre item a item.
  //
c) Quando temos desconto no valor do item como fica?
O desconto incondicional deve deduzir o valor do produto para s� depois ser
aplicada a al�quota m�dia aproximada.
*)
    vDesc := ValorDeducoes - DescontoIncondicionado;
    //
    if (Geral.SoNumero1a9_TT(LC116) = '') or (vServ - vDesc < 0) then
    begin
      Exit;
    end;
    Importado := SrvImportado = 1;
    //
    if not DmNFSe_0000.ObtemValorAproximadoDeTributos(DmodG.QrParamsEmpNFSeCodMunici.Value,
      LC116, Importado, vServ, vDesc, (*var*) vBasTrib, pTotTrib, vTotTrib, tabTotTrib,
      verTotTrib, tpAliqTotTrib, fontTotTrib, Chave) then
    begin
      if Geral.MB_Pergunta('Deseja continuar mesmo assim?') <> ID_YES then
      begin
        Desistiu := True;
        Exit;
      end;
    end;

    case TIBPTaxFont(fontTotTrib) of
      ibptaxfontNaoInfo:
        FonteStr := ''; //'Fonte: C�lculo pr�prio';
      ibptaxfontIBPTax:
        FonteStr := 'Fonte: IBPT ' + Chave;
      ibptaxfontCalculoProprio:
        FonteStr := 'Fonte: C�lculo pr�prio';
      else
      begin
        Geral.MB_Erro(
          '"fontTotTrib" n�o implementado em "FmNFSe_Edit_0201.ConfiguraDiscriminacao()"');
        if Geral.MB_Pergunta('Deseja continuar mesmo assim?') <> ID_YEs then Exit;
      end;
    end;
    //
    iTotTrib := 1;
    InfTotTrib := 'Val. Aprox. dos Tributos R$ ' +
    Geral.FFT(vTotTrib, 2, siPositivo) + ' (' +
    Geral.FFT(pTotTrib, 2, siPositivo) + '%) ' + FonteStr;
    //
    Result := Discriminacao + sLineBreak + InftotTrib;
  end else
  begin
    iTotTrib := 0;
    Result := Discriminacao;
  end;
  // FIM LEI N� 12.741 - Lei da Transparencia
end;

function TNFSe_PF_0201.CI(Valor, Alq: Double): Double;
begin
  Result := Geral.RoundC((Valor * Alq) / 100, 2);
end;

procedure TNFSe_PF_0201.ConfiguraComponenteACBrNFSe(Empresa: Integer; ACBrNFSeX: TACBrNFSe(*TdmkACBrNFSe*));
begin
  ConfiguraComponenteACBrNFSe_Novo(Empresa, ACBrNFSeX);
end;

procedure TNFSe_PF_0201.ConfiguraComponenteACBrNFSe_Antigo(Empresa: Integer;
  ACBrNFSeX: TACBrNFSe);
var
 PathMensal: String;
 //Cam,
 ProxServ, ProxUser, ProxPass: String;
 //ProxBaAu,
 ProxEnab, ProxPort: Integer;
 Ok: Boolean;
begin
(*
  2023-12-20 - Deprecado. C�digo que usava com o componente DmkACBrNFSe!

  DmNFSe_0000.ReopenEmpresa(Empresa);

  //

 {$IFDEF ACBrNFSeOpenSSL}
   ACBrNFSeX.Configuracoes.Certificados.Certificado := Caminho.Text;
   ACBrNFSeX.Configuracoes.Certificados.Senha       := edtSenha.Text;
 {$ELSE}
   ACBrNFSeX.Configuracoes.Certificados.NumeroSerie := DmNFSe_0000.QrFilialNFSeCertDigital.Value;
 {$ENDIF}

 ACBrNFSeX.Configuracoes.WebServices.Salvar := True;

 // adi��o de posfixo nos nomes dos arquivos -?-?-.xml
 ACBrNFSeX.Configuracoes.Arquivos.AdicionarLiteral := True;
 // ??
 ACBrNFSeX.Configuracoes.Arquivos.EmissaoPathNFSe := True;
 //
 //ACBrNFSeX.Configuracoes.Arquivos.PastaMensal := True;
 ACBrNFSeX.Configuracoes.Arquivos.SepararPorMes := True;
 //
 ACBrNFSeX.Configuracoes.Arquivos.PathCan  := DmNFSe_0000.QrFilialDirNFSeLogs.Value;
 ACBrNFSeX.Configuracoes.Arquivos.PathNFSe := DmNFSe_0000.QrFilialDirNFSeLogs.Value;
 ACBrNFSeX.Configuracoes.Arquivos.Salvar:=True;

 PathMensal := ACBrNFSeX.Configuracoes.Arquivos.GetPathNFSe(0);

 //ACBrNFSeX.Configuracoes.Geral.PathSchemas := DmNFSe_0000.QrFilialDirNFSeSchema.Value;
 ACBrNFSeX.Configuracoes.Arquivos.PathSchemas := DmNFSe_0000.QrFilialDirNFSeSchema.Value;
 ACBrNFSeX.Configuracoes.Geral.Salvar      := True;
 //ACBrNFSeX.Configuracoes.Geral.PathSalvar  := DmNFSe_0000.QrFilialDirNFSeLogs.Value;
 ACBrNFSeX.Configuracoes.Arquivos.PathSalvar  := DmNFSe_0000.QrFilialDirNFSeLogs.Value;

 ACBrNFSeX.Configuracoes.WebServices.CodigoMunicipio := DmNFSe_0000.QrFilialNFSeCodMunici.Value;
 ACBrNFSeX.Configuracoes.WebServices.Ambiente        := StrToTpAmb(Ok, IntToStr(DmNFSe_0000.QrFilialNFSeAmbiente.Value));
 ACBrNFSeX.Configuracoes.WebServices.Ambiente        := TpcnTipoAmbiente(DmNFSe_0000.QrFilialNFSeAmbiente.Value);
 ACBrNFSeX.Configuracoes.WebServices.Visualizar      := DmNFSe_0000.QrFilialNFSeMsgVisu.Value = 1;
 ACBrNFSeX.Configuracoes.WebServices.SenhaWeb        := DmNFSe_0000.QrFilialNFSeSenhaWeb.Value;
 ACBrNFSeX.Configuracoes.WebServices.UserWeb         := DmNFSe_0000.QrFilialNFSeUserWeb.Value;

 ProxEnab := Geral.ReadAppKeyCU('ProxyEnable', 'Dermatek', ktInteger, 0);
 if ProxEnab = 1 then
 begin
   ProxUser := Geral.ReadAppKeyCU('ProxyUser', 'Dermatek', ktString, '');
   ProxPass := Geral.ReadAppKeyCU('ProxyPass', 'Dermatek', ktString, '');
   //ProxBaAu := Geral.ReadAppKeyCU('ProxyBasicAuth', 'Dermatek', ktInteger, 0);
   ProxServ := Geral.ReadAppKeyCU('ProxyServ', 'Dermatek', ktString, '');
   ProxPort := Geral.ReadAppKeyCU('ProxyPort', 'Dermatek', ktInteger, 0);
 end else
 begin
   ProxUser := '';
   ProxPass := '';
   //ProxBaAu := '';
   ProxServ := '';
   ProxPort := 0;
 end;
 //
 ACBrNFSeX.Configuracoes.WebServices.ProxyHost := ProxServ;
 ACBrNFSeX.Configuracoes.WebServices.ProxyPort := Geral.FF0(ProxPort);
 ACBrNFSeX.Configuracoes.WebServices.ProxyUser := ProxUser;
 ACBrNFSeX.Configuracoes.WebServices.ProxyPass := ProxPass;
 //
 ACBrNFSeX.Configuracoes.WebServices.Visualizar := VAR_VISUALIZAR_MSG_TdmkACBrNFSe;
{ N�o uso - DANFSe
 if ACBrNFSeX.DANFSe <> nil then
  begin
   ACBrNFSeX.DANFSe.Logo       := edtLogoMarca.Text;
   ACBrNFSeX.DANFSe.PrestLogo  := edtPrestLogo.Text;
   ACBrNFSeX.DANFSe.Prefeitura := edtPrefeitura.Text;
  end;
}
*)
end;

procedure TNFSe_PF_0201.ConfiguraComponenteACBrNFSe_Novo(Empresa: Integer;
  ACBrNFSeX: TACBrNFSe);
// ini 2023-12-20
var
 PathMensal: String;
 //Cam,
 ProxServ, ProxUser, ProxPass: String;
 //ProxBaAu,
 ProxEnab, ProxPort: Integer;
 Ok: Boolean;
(*
var
  Ok: Boolean;
  PathMensal: String;
*)
begin
  DmNFSe_0000.ReopenEmpresa(Empresa);
  DModG.ReopenParamsEmp(Empresa);
  DModG.ReopenParamsACBr();

  //
  ACBrNFSeX.Configuracoes.Certificados.DadosPFX    := '';
  ACBrNFSeX.Configuracoes.Certificados.ArquivoPFX  := VAR_CertDigPfxCam; // edtCaminho.Text;  // 'C:\Executaveis\22.0\Testes\NFSe\Certificado\Dermatek23.pfx'
  ACBrNFSeX.Configuracoes.Certificados.Senha       := VAR_CertDigPfxPwd; // edtSenha.Text;     // 'Dermatek#2023@'
  ACBrNFSeX.Configuracoes.Certificados.NumeroSerie := VAR_CertNFeSerNum; // edtNumSerie.Text;  // '4F2A17B053AB825D'
  ACBrNFSeX.SSL.DescarregarCertificado;

  with ACBrNFSeX.Configuracoes.Geral do
  begin
    SSLLib        := TSSLLib(DModG.QrParamsACBrCertificado_SSLLib.Value); //TSSLLib(cbSSLLib.ItemIndex);   //Marco 4   libWinCript
    SSLCryptLib   := TSSLCryptLib(DModG.QrParamsACBrCertificado_CryptLib.Value); //TSSLCryptLib(cbCryptLib.ItemIndex);  //Marco 3 cryWinCrypt
    SSLHttpLib    := TSSLHttpLib(DModG.QrParamsACBrCertificado_HttpLib.Value); //TSSLHttpLib(cbHttpLib.ItemIndex);    //Marco 2 httpWinHttp
    SSLXmlSignLib := TSSLXmlSignLib(DModG.QrParamsACBrCertificado_XmlSignLib.Value); //TSSLXmlSignLib(cbXmlSignLib.ItemIndex);  //Marco 4 xsLibXml2

    //AtualizarSSLLibsCombo;

    Salvar           := Geral.IntToBool(DModG.QrParamsACBrGeral_Salvar.Value); // ckSalvar.Checked;  // True
    ExibirErroSchema := Geral.IntToBool(DModG.QrParamsACBrGeral_ExibirErroSchema.Value); // cbxExibirErroSchema.Checked;  // True
    // For�ar retirar acentos. Erro de XML!
    RetirarAcentos   := True; //Geral.IntToBool(DModG.QrParamsACBrGeral_RetirarAcentos.Value); // cbxRetirarAcentos.Checked;  // True
    FormatoAlerta    := DModG.QrParamsACBrGeral_FormatoAlerta.Value; // edtFormatoAlerta.Text; // 'TAG:%TAGNIVEL% ID:%ID%/%TAG%(%DESCRICAO%) - %MSG%.'
    FormaEmissao     := TpcnTipoEmissao.teNormal; // TpcnTipoEmissao(cbFormaEmissao.ItemIndex);  // 0
    //
    if DmNFSe_0000.QrFilialDirNFSeIniFiles.Value = EmptyStr then
      Geral.MB_Aviso('Pasta de arquivos ini n�o definida!' + sLineBreak + 'Defina nas configura��es NFSe da janela de op��es da filial');
    //ver o que fazer
    PathIniCidades  := DmNFSe_0000.QrFilialDirNFSeIniFiles.Value; // 'C:\Dermatek\ACBr\ACBrDFe\ACBrNFSe\Delphi\Ini'; // edtArqINI.Text;  // 'C:\Executaveis\22.0\Testes\NFSe'
    //ver o que fazer
    PathIniProvedor := DmNFSe_0000.QrFilialDirNFSeIniFiles.Value; // 'C:\Dermatek\ACBr\ACBrDFe\ACBrNFSe\Delphi\Ini'; // edtArqINI.Text;  // 'C:\Executaveis\22.0\Testes\NFSe'
    CodigoMunicipio := DmNFSe_0000.QrFilialNFSeCodMunici.Value; // StrToIntDef(edtCodCidade.Text, 0);   // 4115200
    SenhaWeb        := DmNFSe_0000.QrFilialNFSeSenhaWeb.Value; // edtSenhaWeb.Text;  // 'Ml03143014'
    UserWeb         := DmNFSe_0000.QrFilialNFSeUserWeb.Value; // edtUserWeb.Text;   // '03143014000152'

    Emitente.CNPJ         := Geral.SoNumero_TT(DModG.QrEmpresasCNPJ_CPF.Value); //edtEmitCNPJ.Text;  // '03143014000152'
    Emitente.InscMun      := Geral.SoNumero_TT(DModG.QrEmpresasNIRE.Value); // edtEmitIM.Text;    // 82281
    Emitente.RazSocial    := DModG.QrEmpresasNOMEFILIAL.Value; //edtEmitRazao.Text; // 'M L Arend & Cia Ltada - ME'
    Emitente.WebUser      := DmNFSe_0000.QrFilialNFSeUserWeb.Value; // edtUserWeb.Text; // '03143014000152'
    Emitente.WebSenha     := DmNFSe_0000.QrFilialNFSeSenhaWeb.Value; // edtSenhaWeb.Text;  // 'Ml03143014'
    //ver o que fazer
    Emitente.WebFraseSecr := ''; //NFSeWebFraseSecr //edtFraseSecWeb.Text;    // ''

    // Provedor ISSNet sem certificado
    // Emitente.WebChaveAcesso := 'A001.B0001.C0001-1';

    // Provedor Sigep sem certificado
    // Emitente.WebChaveAcesso := 'A001.B0001.C0001';

    // Provedor iiBrasil token = WebChaveAcesso para homologa��o
    Emitente.WebChaveAcesso := 'TLXX4JN38KXTRNSE';

    if Provedor = proAgili then
      Emitente.WebChaveAcesso := 'TLXX4JN38KXTRNSETLXX4JN38KXTRNSE';

    if Provedor = proIPM then
      Emitente.WebChaveAcesso := string(EncodeBase64(Emitente.WebUser + ':' +
                                                     Emitente.WebSenha));

    with Emitente.DadosSenhaParams.Add do
    begin
      Param := 'ChaveAutorizacao';
      Conteudo := 'A001.B0001.C0001-1';
    end;

    SetConfigMunicipio;
  end;
  //


  with ACBrNFSeX.Configuracoes.WebServices do
  begin
    Ambiente   := StrToTpAmb(Ok, IntToStr(DmNFSe_0000.QrFilialNFSeAmbiente.Value)); // StrToTpAmb(Ok,IntToStr(rgTipoAmb.ItemIndex+1));  // 2
    Visualizar := DmNFSe_0000.QrFilialNFSeMsgVisu.Value = 1; // cbxVisualizar.Checked; // False
    Salvar     := True; // cbxSalvarSOAP.Checked; // False
    //ver o que fazer
    UF         := ''; //edtEmitUF.Text; // PR
    //ver o que fazer
    AjustaAguardaConsultaRet := False; // cbxAjustarAut.Checked;  // False
    //ver o que fazer
(*
    if NaoEstaVazio(edtAguardar.Text)then   // 0
      AguardarConsultaRet := ifThen(StrToInt(edtAguardar.Text) < 1000, StrToInt(edtAguardar.Text) * 1000, StrToInt(edtAguardar.Text))
    else
      edtAguardar.Text := IntToStr(AguardarConsultaRet);
*)
    AguardarConsultaRet := 0;

    //ver o que fazer
(*
    if NaoEstaVazio(edtTentativas.Text) then   // 5
      Tentativas := StrToInt(edtTentativas.Text)
    else
      edtTentativas.Text := IntToStr(Tentativas);
*)
    Tentativas := 5;

    //ver o que fazer
(*
    if NaoEstaVazio(edtIntervalo.Text) then  // 0
      IntervaloTentativas := ifThen(StrToInt(edtIntervalo.Text) < 1000, StrToInt(edtIntervalo.Text) * 1000, StrToInt(edtIntervalo.Text))
    else
      edtIntervalo.Text := IntToStr(ACBrNFSe1.Configuracoes.WebServices.IntervaloTentativas);
*)
    IntervaloTentativas := 0;

    //ver o que fazer
    TimeOut   := 5000; // seTimeOut.Value;    // 5000
    ProxyHost := ''; // edtProxyHost.Text;   // ''
    ProxyPort := ''; // edtProxyPorta.Text;  // ''
    ProxyUser := ''; // edtProxyUser.Text;   // ''
    ProxyPass := ''; // edtProxySenha.Text;  // ''
  end;

    //ver o que fazer
  ACBrNFSeX.SSL.SSLType := TSSLType(0); //TSSLType(cbSSLType.ItemIndex);  // 0


  //ver o que fazer
  with ACBrNFSex.Configuracoes.Arquivos do
  begin
    NomeLongoNFSe    := True;
    Salvar           := True; // cbxSalvarArqs.Checked;           // False
    SepararPorMes    := True; // cbxPastaMensal.Checked;          // False
    AdicionarLiteral := False; //cbxAdicionaLiteral.Checked;      // False
    EmissaoPathNFSe  := False; //cbxEmissaoPathNFSe.Checked;      // False
    SepararPorCNPJ   := True; // cbxSepararPorCNPJ.Checked;       // False
    PathSalvar       := DmNFSe_0000.QrFilialDirNFSeLogs.Value; // PathMensal; // ''
    PathSchemas      := DmNFSe_0000.QrFilialDirNFSeSchema.Value; // edtPathSchemas.Text;  // ''
    PathNFSe         := DModG.QrParamsEmpDirNFSeNFSAut.Value; // edtPathNFSe.Text; // 'C:\Executaveis\22.0\Testes\NFSe\XML'
    PathGer          := DModG.QrParamsEmpDirNFSeDPSGer.Value; // ;   // 'C:\Executaveis\22.0\Testes\NFSe\Logs'
    PathMensal       := GetPathGer(0);
    PathCan          := DModG.QrParamsEmpDirNFSeNFSCan.Value; // PathMensal; // ''
  end;

(* N�o usa
  if ACBrNFSe1.DANFSe <> nil then
  begin
    // TTipoDANFSE = ( tpPadrao, tpIssDSF, tpFiorilli );
    ACBrNFSe1.DANFSe.TipoDANFSE := tpPadrao;
    ACBrNFSe1.DANFSe.Logo       := edtLogoMarca.Text;
    ACBrNFSe1.DANFSe.PrestLogo  := edtPrestLogo.Text;
    ACBrNFSe1.DANFSe.Prefeitura := edtPrefeitura.Text;
    ACBrNFSe1.DANFSe.PathPDF    := PathMensal;

    ACBrNFSe1.DANFSe.MargemDireita  := 5;
    ACBrNFSe1.DANFSe.MargemEsquerda := 5;
    ACBrNFSe1.DANFSe.MargemSuperior := 5;
    ACBrNFSe1.DANFSe.MargemInferior := 5;

    // Para os provedores que possuem uma lista de servi�os como � o caso do Infisc
    // devemos atribuir o valor True a propriedade DetalharServico
//    ACBrNFSeDANFSeRL1.DetalharServico := (ACBrNFSe1.Configuracoes.Geral.Provedor = proInfisc);
  end;

  with ACBrNFSe1.MAIL do
  begin
    Host      := edtSmtpHost.Text;
    Port      := edtSmtpPort.Text;
    Username  := edtSmtpUser.Text;
    Password  := edtSmtpPass.Text;
    From      := edtEmailRemetente.Text;
    FromName  := edtEmitRazao.Text;
    SetTLS    := cbEmailTLS.Checked;
    SetSSL    := cbEmailSSL.Checked;
    UseThread := False;

    ReadingConfirmation := False;
  end;
*)

  //ver o que fazer
  //lblSchemas.Caption := ACBrNFSe1.Configuracoes.Geral.xProvedor;  // ISSe
  //Geral.MB_Teste(ACBrNFSeX.Configuracoes.Geral.xProvedor);  // ISSe
  // fim 2023-12-20
end;

procedure TNFSe_PF_0201.ConsultaRPSselecionado_Novo(Form: TForm; Empresa,
  Ambiente, Codigo, RpsIDNumero: Integer; RpsIDSerie: String; RpsIDTipo:
  Integer; Caption: String; QrNFSeDPSCab: TmySQLQuery;
  LaAviso1, LaAviso2: TLabel);
var
  //Retorno: String;

  // ini 2023-12-20
  //Nota: TNfse;
  // fim 2023-12-20

  Mem: TStringStream;
  //Status, Codigo,
  //NFSe: Integer;
  //
  Arq: String;
  //Ok, Continua: Boolean;
  XML: AnsiString;
  //
  //StatusRps: TnfseStatusRPS;
  //Empresa, Ambiente, Codigo: Integer;
begin
  // ini 2023-12-20 Parei Aqui!
  try
    (*
    Screen.Cursor := crHourGlass;
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Preparando consulta de NFSe por RPS');
    if not DmNFSe_0000.ReopenNfseArq(txmRPS, Empresa, Ambiente, Codigo, XML, Caption) then
    begin
      if Geral.MB_Pergunta('Deseja carregar o XML de um arquivo no banco de dados?') = ID_YES then
      begin
        if MyObjects.FileOpenDialog(Form, DmNFSe_0000.QrFilialDirNFSeDPSGer.Value,
        '', 'Arquivo XML', '*.xml', [], Arq) then
        begin
          if FileExists(Arq) then
          begin
            if UnNFSeABRASF_0201.LerXML_Arquivo(DmNFSe_0000.XMLDocument1, Ambiente, Arq) then
            Geral.MB_Aviso('Tente executar a a��o novamente!');
          end;
        end;
      end;
      //
      Exit;
    end;
    //
    UnNFSe_PF_0201.ConfiguraComponenteACBrNFSe(DmodG.QrEmpresasCodigo.Value,
      DmNFSe_0000.ACBrNFSe1);
    DmNFSe_0000.ACBrNFSe1.NotasFiscais.Clear;
    //
    Mem := TStringStream.Create(XML);
    DmNFSe_0000.ACBrNFSe1.NotasFiscais.LoadFromStream(Mem);
    *)
    if not DmNFSe_0000.CarregaXML_RPS(Empresa, (*RpsIDNumero,
    RpsIDSerie, RpsIDTipo,*) Ambiente, LaAviso1, LaAviso2, Form) then
      Exit;

    if DmNFSe_0000.ACBrNFSe1.ConsultarNFSeporRps(
      DmNFSe_0000.ACBrNFSe1.NotasFiscais.Items[0].NFSe.IdentificacaoRps.Numero,
      DmNFSe_0000.ACBrNFSe1.NotasFiscais.Items[0].NFSe.IdentificacaoRps.Serie,
      TipoRPSToStr(DmNFSe_0000.ACBrNFSe1.NotasFiscais.Items[0].NFSe.IdentificacaoRps.Tipo)) then
    begin
      (*Arq := DmNFSe_0000.ACBrNFSe1.Configuracoes.Arquivos.GetPathGer() + '\' +
      DmNFSe_0000.ACBrNFSe1.NotasFiscais.Items[0].NFSe.IdentificacaoRps.Numero +
      DmNFSe_0000.ACBrNFSe1.NotasFiscais.Items[0].NFSe.IdentificacaoRps.Serie  +
       '-comp-nfse.xml';*)
      Arq := DmNFSe_0000.ACBrNFSe1.NotasFiscais.Items[0].NomeArq;
      //StatusRps := DmNFSe_0000.ACBrNFSe1.NotasFiscais.items[0].NFSe.Status;
      //
      DmNFSe_0000.AtualizaStatusRPS(LaAviso1, LaAviso2, Ambiente, Codigo, Arq,
        //QrNFSeDPSCabAmbiente.Value, QrNFSeDPSCabCodigo.Value, Arq,
        'Lendo XML de consulta de NFSe por RPS', QrNFSeDPSCab);
    end
    else Geral.MB_Info(UTF8Encode(DmNFSe_0000.ACBrNFSe1.WebServices.ConsNfseRps.RetWS));
  finally
    Screen.Cursor := crDefault;
  end;
  // fim 2023-12-20 Parei Aqui!
end;

function TNFSe_PF_0201.MontaXML_RPS(const Empresa: Integer; const ACBrNFSeX:
TACBrNFSe(*TdmkACBrNFSe*); const LoteRPS: Integer; var RPSTipo: Integer; var RPSSerie: String;
var NumeroRPS: Integer): Boolean;
var
  ValorISS: Double;
  Ok: Boolean;
  Ambiente, RPSNumero, RPSNumHom: Integer;
begin
  // ini 2023-12-20 Parei Aqui!
  //Geral.MB_Aviso('Falta implementar 2023-12-20!');
  Result := False;
  //
  ACBrNFSeX.NotasFiscais.Clear;
  //
  RPSTipo   := DmNFSe_0000.QrFilialNFSeTipoRps.Value;
  RPSSerie  := DmNFSe_0000.QrFilialNFSeSerieRps.Value;
  Ambiente  := DmNFSe_0000.QrFilialNFSeAmbiente.Value;
  // ini 2023-12-22
  RPSNumero := DmNFSe_0000.QrFilialDPSNumero.Value;
  RPSNumHom := DmNFSe_0000.QrFilialDPSNumHom.Value;
  NumeroRPS := UnNFSe_PF_0000.ObtemProximaChaveRPS(Empresa, Ambiente, RPSTipo, RPSSerie, RPSNumero, RPSNumHom);
  //
  if MyObjects.FIC(RpsSerie = '', nil, 'S�rie da NFS-e n�o definida!') then Exit;
  if MyObjects.FIC(RpsTipo = 0, nil, 'Tipo de RPS n�o definido!') then Exit;
  if MyObjects.FIC(NumeroRPS = 0, nil, 'N�mero do RPS n�o definido!') then Exit;
  // fim 2023-12-22
  //
  with ACBrNFSeX do
  begin
   NotasFiscais.NumeroLote := UnNFSe_PF_0000.FormataRPSNumero(LoteRPS);
   with NotasFiscais.Add.NFSe do
   begin
     //IdentificacaoRps.Numero := FormatFloat('#########0', DmNFSe_0000.QrNFSeDPSCabRpsIDNumero.Value);
  // ini 2023-12-22
     //IdentificacaoRps.Numero := FormatFloat('#########0', RPSNumero);
     IdentificacaoRps.Numero := FormatFloat('#########0', NumeroRPS);
  // fim 2023-12-22

     // Para o provedor ISS.NET em ambiente de Homologa��o mudar a s�rie para '8'
     //IdentificacaoRps.Serie := DmNFSe_0000.QrNFSeDPSCabRpsIDSerie.Value;
     IdentificacaoRps.Serie := RPSSerie;

     // TnfseTipoRPS = ( trRPS, trNFConjugada, trCupom );
     //IdentificacaoRps.Tipo := TnfseTipoRPS(DmNFSe_0000.QrNFSeDPSCabRpsIDTipo.Value);
     //IdentificacaoRps.Tipo := StrToTipoRPS(Ok, Geral.FF0(DmNFSe_0000.QrNFSeDPSCabRpsIDTipo.Value));
     IdentificacaoRps.Tipo := StrToTipoRPS(Ok, Geral.FF0(RPSTipo));

     DataEmissao := DmNFSe_0000.QrNFSeDPSCabRpsDataEmissao.Value;

     // TnfseStatusRPS = ( srNormal, srCancelado );
     //Status := TnfseStatusRPS(DmNFSe_0000.QrNFSeDPSCabRpsStatus.Value);
     Status := StrToStatusRPS(Ok, Geral.FF0(DmNFSe_0000.QrNFSeDPSCabRpsStatus.Value));

     // TnfseNaturezaOperacao = ( noTributacaoNoMunicipio, noTributacaoForaMunicipio, noIsencao, noImune, noSuspensaDecisaoJudicial, noSuspensaProcedimentoAdministrativo );
     // Maring� N�o usa. Usa Exigibilidade
     //NaturezaOperacao := TnfseNaturezaOperacao(DmNFSe_0000.QrNFSeDPSCabNaturezaOperacao.Value);
     NaturezaOperacao := StrToNaturezaOperacao(Ok, Geral.FF0(DmNFSe_0000.QrNFSeDPSCabNaturezaOperacao.Value));


     // TnfseRegimeEspecialTributacao = ( retNenhum, retMicroempresaMunicipal, retEstimativa, retSociedadeProfissionais, retCooperativa, retMicroempresarioIndividual, retMicroempresarioEmpresaPP );
     //RegimeEspecialTributacao := retMicroempresaMunicipal;
     //RegimeEspecialTributacao := TnfseRegimeEspecialTributacao(DmNFSe_0000.QrNFSeDPSCabRegimeEspecialTributacao.Value);
     RegimeEspecialTributacao := StrToRegimeEspecialTributacao(Ok, Geral.FF0(DmNFSe_0000.QrNFSeDPSCabRegimeEspecialTributacao.Value));

     // TnfseSimNao = ( snSim, snNao );
     //OptanteSimplesNacional := TnfseSimNao(DmNFSe_0000.QrNFSeDPSCabOptanteSimplesNacional.Value - 1);
     OptanteSimplesNacional := StrToSimNao(Ok, Geral.FF0(DmNFSe_0000.QrNFSeDPSCabOptanteSimplesNacional.Value));

     // TnfseSimNao = ( snSim, snNao );
     // Maring� usa IncentivoFiscal (Na verdade n�o usa nada, pelos menos hoje 2012-10-02)
     //IncentivadorCultural := TnfseSimNao(DmNFSe_0000.QrNFSeDPSCabIncentivoFiscal.Value - 1);
     IncentivadorCultural := StrToSimNao(Ok, Geral.FF0(DmNFSe_0000.QrNFSeDPSCabIncentivoFiscal.Value));

     (* Usando quando o RPS for substituir outro*)
     if DmNFSe_0000.QrNFSeDPSCabSubstNumero.Value > 0 then
     begin
       RpsSubstituido.Numero := Geral.FF0(DmNFSe_0000.QrNFSeDPSCabSubstNumero.Value);
       RpsSubstituido.Serie  := DmNFSe_0000.QrNFSeDPSCabSubstSerie.Value;
       // TnfseTipoRPS = ( trRPS, trNFConjugada, trCupom );
       //RpsSubstituido.Tipo   := TnfseTipoRPS(DmNFSe_0000.QrNFSeDPSCabSubstTipo.Value);
       RpsSubstituido.Tipo   := StrToTipoRPS(Ok, Geral.FF0(DmNFSe_0000.QrNFSeDPSCabSubstTipo.Value));
     end;

     Competencia := FormatDateTime('YYYY-MM-DD', DmNFSe_0000.QrNFSeDPSCabCompetencia.Value);


  // ini 2023-12-20
     Servico.CodigoMunicipio := Geral.FF0(DmNFSe_0000.QrFilialNFSeCodMunici.Value);
  // fim 2023-12-20


     Servico.Valores.ValorServicos          := DmNFSe_0000.QrNFSeDPSCabValorServicos.Value;
     Servico.Valores.ValorDeducoes          := DmNFSe_0000.QrNFSeDPSCabValorDeducoes.Value;
     Servico.Valores.ValorPis               := DmNFSe_0000.QrNFSeDPSCabValorPis.Value;
     Servico.Valores.ValorCofins            := DmNFSe_0000.QrNFSeDPSCabValorCofins.Value;
     Servico.Valores.ValorInss              := DmNFSe_0000.QrNFSeDPSCabValorInss.Value;
     Servico.Valores.ValorIr                := DmNFSe_0000.QrNFSeDPSCabValorIr.Value;
     Servico.Valores.ValorCsll              := DmNFSe_0000.QrNFSeDPSCabValorCsll.Value;

     // TnfseSituacaoTributaria = ( stRetencao, stNormal, stSubstituicao );
     // stRetencao = snSim
     // stNormal   = snNao
     //Servico.Valores.IssRetido              := TnfseSituacaoTributaria(DmNFSe_0000.QrNFSeDPSCabIssRetido.Value - 1);
     Servico.Valores.IssRetido := StrToSituacaoTributaria(Ok, Geral.FF0(DmNFSe_0000.QrNFSeDPSCabIssRetido.Value));

     Servico.Valores.OutrasRetencoes        := DmNFSe_0000.QrNFSeDPSCabOutrasRetencoes.Value;
     Servico.Valores.DescontoIncondicionado := DmNFSe_0000.QrNFSeDPSCabDescontoIncondicionado.Value;
     Servico.Valores.DescontoCondicionado   := DmNFSe_0000.QrNFSeDPSCabDescontoCondicionado.Value;

     Servico.Valores.BaseCalculo            := Servico.Valores.ValorServicos -
                                               Servico.Valores.ValorDeducoes -
                                               Servico.Valores.DescontoIncondicionado;
     Servico.Valores.Aliquota               := DmNFSe_0000.QrNFSeDPSCabAliquota.Value;

     if Servico.Valores.IssRetido = stNormal then
     begin
       ValorISS := Servico.Valores.BaseCalculo * Servico.Valores.Aliquota;

       // A fun��o RoundTo5 � usada para arredondar valores, sendo que o segundo
       // parametro se refere ao numero de casas decimais.
       // exemplos: RoundTo5(50.532, -2) ==> 50.53
       // exemplos: RoundTo5(50.535, -2) ==> 50.54
       // exemplos: RoundTo5(50.536, -2) ==> 50.54

       Servico.Valores.ValorIss       := Geral.RoundC(ValorISS, 2);
       Servico.Valores.ValorIssRetido := 0.00;
      end
      else begin
       ValorISS := Servico.Valores.BaseCalculo * Servico.Valores.Aliquota;

       Servico.Valores.ValorIss       := 0.00;
       Servico.Valores.ValorIssRetido := Geral.RoundC(ValorISS, 2);
      end;

     Servico.Valores.ValorLiquidoNfse := Servico.Valores.ValorServicos -
                                         Servico.Valores.ValorPis -
                                         Servico.Valores.ValorCofins -
                                         Servico.Valores.ValorInss -
                                         Servico.Valores.ValorIr -
                                         Servico.Valores.ValorCsll -
                                         Servico.Valores.OutrasRetencoes -
                                         Servico.Valores.ValorIssRetido -
                                         Servico.Valores.DescontoIncondicionado -
                                         Servico.Valores.DescontoCondicionado;

     //Servico.ResponsavelRetencao := TnfseResponsavelRetencao(DmNFSe_0000.QrNFSeDPSCabResponsavelRetencao.Value);
     Servico.ResponsavelRetencao := StrToResponsavelRetencao(Ok, Geral.FF0(DmNFSe_0000.QrNFSeDPSCabResponsavelRetencao.Value));

     Servico.ItemListaServico         := DmNFSe_0000.QrNFSeDPSCabItemListaServico.Value;

     // Para o provedor ISS.NET em ambiente de Homologa��o
     // o Codigo CNAE tem que ser '6511102'
     // Servico.CodigoCnae                := '123'; // Informa��o Opcional

     Servico.CodigoCnae := DmNFSe_0000.QrNFSeDPSCabCodigoCnae.Value;
     Servico.CodigoTributacaoMunicipio := DmNFSe_0000.QrNFSeDPSCabCodigoTributacaoMunicipio.Value;
     Servico.Discriminacao             := DmNFSe_0000.QrNFSeDPSCabDiscriminacao.Value;

     // Para o provedor ISS.NET em ambiente de Homologa��o
     // o Codigo do Municipio tem que ser '999'
     Servico.CodigoMunicipio := Geral.FF0(DmNFSe_0000.QrNFSeDPSCabCodigoMunicipio.Value);

     // Informar A Exigibilidade ISS para fintelISS / Maringa[1/2/3/4/5/6/7]
     //Servico.ExigibilidadeISS := TnfseExigibilidadeISS(DmNFSe_0000.QrNFSeDPSCabExigibilidadeISS.Value);
     Servico.ExigibilidadeISS := StrToExigibilidadeISS(Ok, Geral.FF0(DmNFSe_0000.QrNFSeDPSCabExigibilidadeISS.Value));

     // Informar para Saatri / Maringa
     Servico.CodigoPais := DmNFSe_0000.QrNFSeDPSCabCodigoPais.Value;
     Servico.MunicipioIncidencia := DmNFSe_0000.QrNFSeDPSCabMunicipioIncidencia.Value;

     // Informar Maring�?
     Servico.NumeroProcesso := DmNFSe_0000.QrNFSeDPSCabNumeroProcesso.Value;

     with Servico.ItemServico.Add do
      begin
       Descricao     := 'SERVICO 1';
       Quantidade    := 1;
       ValorUnitario := 15.00;
      end;

     Prestador.Cnpj               := Geral.SoNumero_TT(DmNFSe_0000.QrNFSeDPSCabPRESTA_CNPJ_CPF.Value);
     Prestador.InscricaoMunicipal := DmNFSe_0000.QrNFSeDPSCabPrestaInscricaoMunicipal.Value;

     // Para o provedor ISSDigital deve-se informar tamb�m:
     // ini 2023-12-20
      Prestador.Senha := DmNFSe_0000.QrFilialNFSeSenhaWeb.Value;
 //ACBrNFSeX.Configuracoes.WebServices.SenhaWeb        := DmNFSe_0000.QrFilialNFSeSenhaWeb.Value;
 //ACBrNFSeX.Configuracoes.WebServices.UserWeb         := DmNFSe_0000.QrFilialNFSeUserWeb.Value;
      //Prestador.FraseSecreta := '';
      //Prestador.cUF := 33;
      // Provedor WebFisco
      Prestador.Usuario := StrToIntDef(ACBrNFSeX.Configuracoes.Geral.Emitente.WebUser, 0);
     // fim 2023-12-20


     Tomador.IdentificacaoTomador.CpfCnpj            := Geral.SoNumero_TT(DmNFSe_0000.QrNFSeDPSCabTOMA_CNPJ_CPF.Value);
     Tomador.IdentificacaoTomador.InscricaoMunicipal := DmNFSe_0000.QrNFSeDPSCabTomaInscricaoMunicipal.Value;

     Tomador.RazaoSocial := DmNFSe_0000.QrNFSeDPSCabTomaRazaoSocial.Value;

     Tomador.Endereco.Endereco        := DmNFSe_0000.QrNFSeDPSCabTomaEndereco.Value;
     Tomador.Endereco.Numero          := DmNFSe_0000.QrNFSeDPSCabTomaNumero.Value;
     Tomador.Endereco.Complemento     := DmNFSe_0000.QrNFSeDPSCabTomaComplemento.Value;
     Tomador.Endereco.Bairro          := DmNFSe_0000.QrNFSeDPSCabTomaBairro.Value;
     Tomador.Endereco.CodigoMunicipio := Geral.FF0(DmNFSe_0000.QrNFSeDPSCabTomaCodigoMunicipio.Value);
     Tomador.Endereco.UF              := DmNFSe_0000.QrNFSeDPSCabTomaUf.Value;
     Tomador.Endereco.CodigoPais      := DmNFSe_0000.QrNFSeDPSCabTomaCodigoPais.Value;
     Tomador.Endereco.CEP             := Geral.FF0(DmNFSe_0000.QrNFSeDPSCabTomaCep.Value);

     Tomador.Contato.Telefone := DmNFSe_0000.QrNFSeDPSCabTomaContatoTelefone.Value;
     Tomador.Contato.Email    := DmNFSe_0000.QrNFSeDPSCabTomaContatoEmail.Value;

     (* Usando quando houver um intermediario na presta��o do servi�o*)
     if DmNFSe_0000.QrNFSeDPSCabINTERME_CNPJ_CPF.Value <> '' then
     begin
       IntermediarioServico.RazaoSocial        := DmNFSe_0000.QrNFSeDPSCabIntermeRazaoSocial.Value;
       IntermediarioServico.CpfCnpj            := DmNFSe_0000.QrNFSeDPSCabINTERME_CNPJ_CPF.Value;
       IntermediarioServico.InscricaoMunicipal := DmNFSe_0000.QrNFSeDPSCabIntermeInscricaoMunicipal.Value;
     end;

     (* Usando quando o servi�o for uma obra*)
     if DmNFSe_0000.QrNFSeDPSCabConstrucaoCivilCodigoObra.Value <> '' then
     begin
       ConstrucaoCivil.CodigoObra := DmNFSe_0000.QrNFSeDPSCabConstrucaoCivilCodigoObra.Value;
       ConstrucaoCivil.Art        := DmNFSe_0000.QrNFSeDPSCabConstrucaoCivilArt.Value;
     end;
    end;
  end;
  Result := True;
  // fim 2023-12-20 Parei Aqui!
end;

function TNFSe_PF_0201.EnviaRPS_Unica(Form: TForm; DPS, LoteRPS: Integer;
  QryDPS: TmySQLQuery; LaAviso1, LaAviso2: TLabel): Boolean;
var
  RpsId: String;
  //pI, pF,
  Empresa, Ambiente, Codigo: Integer;
  XML: String;
  ACBrNFSeX: TACBrNFSe(*TdmkACBrNFSe*);
  //
  Arquivo, Texto: String;
  //StatusRPS: TnfseStatusRPS;
  //
  RPSIDSerie: String;
  RPSIDTipo, RPSIDNumero: Integer;

  Campo: String;
begin
  Result := False;
  //
  MyObjects.Informa2(LaAviso1, laAviso2, True, 'Gerando NFSe');
  ///
  RpsId := UnNFSe_PF_0000.FormataRPSNumero(DPS);
  DmNFSe_0000.ReopenNFSeDPSCab(DPS);
  Empresa := DmNFSe_0000.QrNFSeDPSCabEmpresa.Value;
  Codigo := DmNFSe_0000.QrNFSeDPSCabCodigo.Value;
  Ambiente := DmNFSe_0000.QrFilialNFSeAmbiente.Value;
  //
  ACBrNFSeX := TACBrNFSe(*TdmkACBrNFSe*).Create(Form);
  ConfiguraComponenteACBrNFSe(Empresa, ACBrNFSeX);
  ACBrNFSeX.NotasFiscais.Clear;
  RPSIDTipo := 0;
  RPSIDSerie := '';
  RPSIDNumero := 0;
  if MontaXML_RPS(Empresa, ACBrNFSeX, LoteRPS, RPSIDTipo, RPSIDSerie, RPSIDNumero) then
  begin
    // ini 2023-12-22
    //XML := ACBrNFSeX.NotasFiscais.Items[0].XML;
    //DmNFSe_0000.AtualizaXML_No_BD(txmRPS, Empresa, Ambiente, Codigo, XML);
    // fim 2023-12-22
    try
      MyObjects.Informa2(LaAviso1, laAviso2, True, 'Enviando NFSe');
      ///
      case DmNFSe_0000.QrFilialNFSeMetodEnvRPS.Value of
        1: Result := ACBrNFSeX.Enviar(LoteRps);
        2: Result := ACBrNFSeX.EnviarSincrono(LoteRps);
        else
        begin
          Result := False;
          Geral.MB_Erro('M�todo de envio de NFSe n�o implementado!');
        end;
      end;
      if Result then
      begin
        MyObjects.Informa2(LaAviso1, laAviso2, True, 'Confirmando Chave RPS');
        UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'nfsedpscab', False, [
        'RpsID', 'RpsIDNumero', 'RpsIDSerie',
        'RpsIDTipo'], ['Codigo'
        ], [
        RpsID, RpsIDNumero, RpsIDSerie,
        RpsIDTipo], [Codigo
        ], True);
        // ini 2023-12-22
        case Ambiente of
          1: Campo := 'DPSNumero';
          2: Campo := 'DPSNumHom';
        end;
        MyObjects.Informa2(LaAviso1, laAviso2, True, 'Confirmando �ltima DPS/RPS na configural��o da filial');
        UnDmkDAC_PF.ExecutaDB(Dmod.MyDB, 'UPDATE paramsemp SET ' + Campo + '=' +
        Geral.FF0(RpsIDNumero) + ' WHERE Codigo=' + Geral.FF0(Empresa));
        XML := ACBrNFSeX.NotasFiscais.Items[0].XML;
        DmNFSe_0000.AtualizaXML_No_BD(txmRPS, Empresa, Ambiente, Codigo, XML);
        // fim 2023-12-22
        //
        MyObjects.Informa2(LaAviso1, laAviso2, True, 'Lendo retorno NFSe');
        // ini 2023-12-22
        //Arquivo := ACBrNFSeX.Configuracoes.Arquivos.GetPathGer + '\' + Geral.FF0(LoteRps) + '-rec.xml';
        Arquivo := ACBrNFSeX.NotasFiscais.Items[0].NomeArqRps;
        // ini 2023-12-22
        //StatusRPS := ACBrNFSeX.NotasFiscais.Items[0].NFSe.Status;
        DmNFSe_0000.AtualizaStatusRPS(LaAviso1, LaAviso2, Ambiente, DPS,
          Arquivo, 'Lendo XML de emiss�o de NFSe', QryDPS);
        case DmNFSe_0000.QrFilialNFSeMetodEnvRPS.Value of
          1:// Assincrono
          begin
            if Geral.MB_Pergunta('NFSe emitida em modo asincrono.' +
            '� necess�rio consultar a DPS para obter a NFSe!' + sLineBreak +
            'Deseja consult�-la agora?') = ID_YES then
            begin
              UnNFSe_PF_0201.ConsultaRPSselecionado_Novo(Form, Empresa, Ambiente,
              Codigo, RpsIDNumero, RpsIDSerie, RpsIDTipo,
              Form.Caption, DmNFSe_0000.QrNFSeDPSCab, LaAviso1, LaAviso2);
            end;
          end;
          2:
          begin
            Arquivo := ACBrNFSeX.NotasFiscais.Items[0].NomeArq;
            //
            DmNFSe_0000.AtualizaStatusRPS(LaAviso1, LaAviso2, Ambiente, Codigo,
            Arquivo, 'Lendo XML de retorno do RPS', DmNFSe_0000.QrNFSeDPSCab);
          end;
        end;
      end;
    except
      on E: Exception do
      begin
        Geral.MB_Erro('Falha ao enviar NFS-e' + sLineBreak + E.Message);
        //
        MyObjects.Informa2(LaAviso1, laAviso2, True, 'Lendo retorno erro');
        ///
        DmNFSe_0000.StepNFSeRPS_Lote(LoteRPS,
          DmNFSe_0000.stepLoteRejeitado(), [], [], LaAviso1, LaAviso2);
        Arquivo := ACBrNFSeX.Configuracoes.Arquivos.GetPathGer + '\' +
         Geral.FF0(LoteRps) + '-rec-c.xml';
        if FileExists(Arquivo) then
        begin
          if dmkPF.LoadArquivoToText(Arquivo, Texto) then
            DmNFSe_0000.ObtemMensagensRetornoLote(Texto, LoteRPS, 'MensagemRetorno');
        end else
         Geral.MB_Erro('Arquivo de log de erro n�o localizado: ' + sLineBreak +
           Arquivo);
      end;
    end;
    //
    ACBrNFSeX.NotasFiscais.Clear;
    MyObjects.Informa2(LaAviso1, laAviso2, False, '...');
  end;
end;

{ Original
procedure TNFSe_PF_0201.MostraFormNFSe(SQLType: TSQLType;
Prestador, Tomador, Intermediario, MeuServico, ItemListSrv: Integer;
Discriminacao: String; GeraNFSe: Boolean; NFSeFatCab, DPS: Integer;
Servico: TnfseFormaGerarNFSe; QrNFSeDPSCab: TmySQLQuery; Valor: Double;
var Serie: String; var Numero: Integer);
var
  Filial: Integer;
  Continua: Boolean;
  //
begin
  Serie  := '';
  Numero := 0;
  //
  if SQLType = stUpd then
  begin
    DmNFSe_0000.ReopenNFSeDPSCab(DPS);
    if ((DmNFSe_0000.QrNFSeDPSCabStatus.Value <= DmNFSe_0000.stepAdedLote())
    and (SQLType = stUpd)) or (Servico = fgnSubstituicaoNFSe) then
      Continua := True
    else
    begin
      Continua := False;
      Geral.MB_Aviso(
      'Status do RPS deve ser menor que 50 para poder ser editado!');
    end;
  end else
    Continua := True;
  //
  if not Continua then
    Exit;
  if DBCheck.CriaFm(TFmNFSe_Edit_0201, FmNFSe_Edit_0201, afmoNegarComAviso) then
  begin
    FmNFSe_Edit_0201.ImgTipo.SQLType := SQLType;
    FmNFSe_Edit_0201.FGeraNFse := GeraNFSe;
    FmNFSe_Edit_0201.FNFSeFatCab := NFSeFatCab;
    //
    if SQLType = stIns then
    begin
      FmNFSe_Edit_0201.EdEmpresa.ValueVariant       := Prestador;
      FmNFSe_Edit_0201.CBEmpresa.KeyValue           := Prestador;

      FmNFSe_Edit_0201.EdValorServicos.ValueVariant := Valor;

      FmNFSe_Edit_0201.EdCliente.ValueVariant       := Tomador;
      FmNFSe_Edit_0201.CBCliente.KeyValue           := Tomador;
      //
    end else
    begin
      Filial := DModG.ObtemFilialDeEntidade(DmNFSe_0000.QrNFSeDPSCabEmpresa.Value);
      with FmNFSe_Edit_0201 do
      begin
        ImgTipo.SQLType := SQLType;
        FQrNFSeDPSCab := QrNFSeDPSCab;
        FFormaGerarNFSe := Servico;
        //
        if (SQLType = stIns) and (Servico = fgnSubstituicaoNFSe) then
        begin
          EdRpsIDNumero.ValueVariant               := 0;
          EdRpsIDSerie.ValueVariant                := DmNFSe_0000.QrNFSeDPSCabRpsIDSerie.Value;
          EdRpsIDTipo.ValueVariant                 := DmNFSe_0000.QrNFSeDPSCabRpsIDTipo.Value;
          CBRpsIDTipo.KeyValue                     := DmNFSe_0000.QrNFSeDPSCabRpsIDTipo.Value;
          EdCodigo.ValueVariant                    := 0;
          //
          EdSubstNumero.ValueVariant               := DmNFSe_0000.QrNFSeDPSCabRpsIDNumero.Value;
          EdSubstSerie.Text                        := DmNFSe_0000.QrNFSeDPSCabRpsIDSerie.Value;
          EdSubstTipo.ValueVariant                 := DmNFSe_0000.QrNFSeDPSCabRpsIDTipo.Value;
          CBSubstTipo.KeyValue                     := DmNFSe_0000.QrNFSeDPSCabRpsIDTipo.Value;
        end else
        if (SQLType = stUpd) and (Servico = fgnSubstituicaoNFSe) then
        begin
          EdRpsIDNumero.ValueVariant               := DmNFSe_0000.QrNFSeDPSCabRpsIDNumero.Value;
          EdRpsIDSerie.ValueVariant                := DmNFSe_0000.QrNFSeDPSCabRpsIDSerie.Value;
          EdRpsIDTipo.ValueVariant                 := DmNFSe_0000.QrNFSeDPSCabRpsIDTipo.Value;
          CBRpsIDTipo.KeyValue                     := DmNFSe_0000.QrNFSeDPSCabRpsIDTipo.Value;
          EdCodigo.ValueVariant                    := DmNFSe_0000.QrNFSeDPSCabCodigo.Value;
          //
          EdSubstNumero.ValueVariant               := DmNFSe_0000.QrNFSeDPSCabSubstNumero.Value;
          EdSubstSerie.Text                        := DmNFSe_0000.QrNFSeDPSCabSubstSerie.Value;
          EdSubstTipo.ValueVariant                 := DmNFSe_0000.QrNFSeDPSCabSubstTipo.Value;
          CBSubstTipo.KeyValue                     := DmNFSe_0000.QrNFSeDPSCabSubstTipo.Value;
        end else
        if (SQLType = stUpd) and ((Servico = fgnLoteRPSSincrono) or
        (Servico = fgnLoteRPS)) then
        begin
          EdRpsIDNumero.ValueVariant               := DmNFSe_0000.QrNFSeDPSCabRpsIDNumero.Value;
          EdRpsIDSerie.ValueVariant                := DmNFSe_0000.QrNFSeDPSCabRpsIDSerie.Value;
          EdRpsIDTipo.ValueVariant                 := DmNFSe_0000.QrNFSeDPSCabRpsIDTipo.Value;
          CBRpsIDTipo.KeyValue                     := DmNFSe_0000.QrNFSeDPSCabRpsIDTipo.Value;
          EdCodigo.ValueVariant                    := DmNFSe_0000.QrNFSeDPSCabCodigo.Value;
          //
        end else
        begin
          Geral.MB_Erro('Forma de emiss�o de NFSe n�o implemetada!');
          Destroy;
          //
          Exit;
        end;

        EdEmpresa.ValueVariant                   := Filial;
        CBEmpresa.KeyValue                       := Filial;
        TPRpsDataEmissao.Date                    := DmNFSe_0000.QrNFSeDPSCabRpsDataEmissao.Value;
        EdRpsHoraEmissao.Text                    := Geral.FDT(DmNFSe_0000.QrNFSeDPSCabRpsHoraEmissao.Value, 100);
        TPCompetencia.Date                       := DmNFSe_0000.QrNFSeDPSCabCompetencia.Value;
        EdIntermediario.ValueVariant             := DmNFSe_0000.QrNFSeDPSCabIntermediario.Value;
        CBIntermediario.KeyValue                 := DmNFSe_0000.QrNFSeDPSCabIntermediario.Value;
        EdCliente.ValueVariant                   := DmNFSe_0000.QrNFSeDPSCabCliente.Value;
        CBCliente.KeyValue                       := DmNFSe_0000.QrNFSeDPSCabCliente.Value;
        EdNFSeSrvCad.ValueVariant                := DmNFSe_0000.QrNFSeDPSCabNFSeSrvCad.Value;
        CBNFSeSrvCad.KeyValue                    := DmNFSe_0000.QrNFSeDPSCabNFSeSrvCad.Value;
        //
        EdItemListaServico.ValueVariant          := DmNFSe_0000.QrNFSeDPSCabItemListaServico.Value;
        CBItemListaServico.KeyValue              := DmNFSe_0000.QrNFSeDPSCabItemListaServico.Value;
        EdCodigoCnae.ValueVariant                := DmNFSe_0000.QrNFSeDPSCabCodigoCnae.Value;
        CBCodigoCnae.KeyValue                    := DmNFSe_0000.QrNFSeDPSCabCodigoCnae.Value;
        MeDiscriminacao.Text                     := DmNFSe_0000.QrNFSeDPSCabDiscriminacao.Value;
        EdValorServicos.ValueVariant             := DmNFSe_0000.QrNFSeDPSCabValorServicos.Value;
        EdValorDeducoes.ValueVariant             := DmNFSe_0000.QrNFSeDPSCabValorDeducoes.Value;
        EdAliquota.ValueVariant                  := DmNFSe_0000.QrNFSeDPSCabAliquota.Value;
        EdDescontoIncondicionado.ValueVariant    := DmNFSe_0000.QrNFSeDPSCabDescontoIncondicionado.Value;
        EdValorIss.ValueVariant                  := DmNFSe_0000.QrNFSeDPSCabValorIss.Value;
        EdDescontoCondicionado.ValueVariant      := DmNFSe_0000.QrNFSeDPSCabDescontoCondicionado.Value;
        EdValorPis.ValueVariant                  := DmNFSe_0000.QrNFSeDPSCabValorPis.Value;
        EdValorCofins.ValueVariant               := DmNFSe_0000.QrNFSeDPSCabValorCofins.Value;
        EdValorInss.ValueVariant                 := DmNFSe_0000.QrNFSeDPSCabValorInss.Value;
        EdValorIr.ValueVariant                   := DmNFSe_0000.QrNFSeDPSCabValorIr.Value;
        EdValorCsll.ValueVariant                 := DmNFSe_0000.QrNFSeDPSCabValorCsll.Value;
        EdOutrasRetencoes.ValueVariant           := DmNFSe_0000.QrNFSeDPSCabOutrasRetencoes.Value;
        EdIssRetido.ValueVariant                 := DmNFSe_0000.QrNFSeDPSCabIssRetido.Value;
        CBIssRetido.KeyValue                     := DmNFSe_0000.QrNFSeDPSCabIssRetido.Value;
        EdResponsavelRetencao.ValueVariant       := DmNFSe_0000.QrNFSeDPSCabResponsavelRetencao.Value;
        CBResponsavelRetencao.KeyValue           := DmNFSe_0000.QrNFSeDPSCabResponsavelRetencao.Value;
        EdExigibilidadeISS.ValueVariant          := DmNFSe_0000.QrNFSeDPSCabExigibilidadeISS.Value;
        CBExigibilidadeISS.KeyValue              := DmNFSe_0000.QrNFSeDPSCabExigibilidadeISS.Value;
        EdNumeroProcesso.ValueVariant            := DmNFSe_0000.QrNFSeDPSCabNumeroProcesso.Value;
        EdConstrucaoCivilCodigoObra.Text         := DmNFSe_0000.QrNFSeDPSCabConstrucaoCivilCodigoObra.Value;
        EdConstrucaoCivilArt.Text                := DmNFSe_0000.QrNFSeDPSCabConstrucaoCivilArt.Value;
        EdCodigoTributacaoMunicipio.ValueVariant := DmNFSe_0000.QrNFSeDPSCabCodigoTributacaoMunicipio.Value;
        EdQuemPagaISS.ValueVariant               := DmNFSe_0000.QrNFSeDPSCabQuemPagaISS.Value;
        CBQuemPagaISS.KeyValue                   := DmNFSe_0000.QrNFSeDPSCabQuemPagaISS.Value;
        EdOptanteSimplesNacional.ValueVariant    := DmNFSe_0000.QrNFSeDPSCabOptanteSimplesNacional.Value;
        CBOptanteSimplesNacional.KeyValue        := DmNFSe_0000.QrNFSeDPSCabOptanteSimplesNacional.Value;
        EdIncentivoFiscal.ValueVariant           := DmNFSe_0000.QrNFSeDPSCabIncentivoFiscal.Value;
        CBIncentivoFiscal.KeyValue               := DmNFSe_0000.QrNFSeDPSCabIncentivoFiscal.Value;
        EdRegimeEspecialTributacao.ValueVariant  := DmNFSe_0000.QrNFSeDPSCabRegimeEspecialTributacao.Value;
        CBRegimeEspecialTributacao.KeyValue      := DmNFSe_0000.QrNFSeDPSCabRegimeEspecialTributacao.Value;
      end;
    end;
    FmNFSe_Edit_0201.ShowModal;
    //
    Numero := FmNFSe_Edit_0201.FNumero;
    Serie  := FmNFSe_Edit_0201.FSerie;
    //
    FmNFSe_Edit_0201.Destroy;
  end
end;
}

procedure TNFSe_PF_0201.MostraFormNFSe(SQLType: TSQLType;
Prestador, Tomador, Intermediario, MeuServico, ItemListSrv: Integer;
Discriminacao: String; GeraNFSe: Boolean; NFSeFatCab, DPS: Integer;
Servico: TnfseFormaGerarNFSe; QrNFSeDPSCab: TmySQLQuery; Valor: Double;
var Serie: String; var Numero: Integer; TabedForm: TForm);
var
  Filial: Integer;
  Continua: Boolean;
  //
  MyForm: TForm;
begin
  Serie  := '';
  Numero := 0;
  //
  if SQLType = stUpd then
  begin
    DmNFSe_0000.ReopenNFSeDPSCab(DPS);
    if ((DmNFSe_0000.QrNFSeDPSCabStatus.Value <= DmNFSe_0000.stepAdedLote())
    and (SQLType = stUpd)) or (Servico = fgnSubstituicaoNFSe) then
      Continua := True
    else
    begin
      Continua := False;
      Geral.MB_Aviso(
      'Status do RPS deve ser menor que 50 para poder ser editado!');
    end;
  end else
    Continua := True;
  //
  if not Continua then
    Exit;
  if TabedForm <> nil then
  begin
    MyForm := TabedForm;
  end else
  begin
    if DBCheck.CriaFm(TFmNFSe_Edit_0201, FmNFSe_Edit_0201, afmoNegarComAviso) then
      MyForm := TForm(FmNFSe_Edit_0201);
  end;
  if MyForm <> nil then
  begin
    TFmNFSe_Edit_0201(MyForm).ImgTipo.SQLType := SQLType;
    TFmNFSe_Edit_0201(MyForm).FGeraNFse := GeraNFSe;
    TFmNFSe_Edit_0201(MyForm).FNFSeFatCab := NFSeFatCab;
    //
    if SQLType = stIns then
    begin
      TFmNFSe_Edit_0201(MyForm).EdEmpresa.ValueVariant       := Prestador;
      TFmNFSe_Edit_0201(MyForm).CBEmpresa.KeyValue           := Prestador;

      TFmNFSe_Edit_0201(MyForm).EdValorServicos.ValueVariant := Valor;

      TFmNFSe_Edit_0201(MyForm).EdCliente.ValueVariant       := Tomador;
      TFmNFSe_Edit_0201(MyForm).CBCliente.KeyValue           := Tomador;
      //
    end else
    begin
      Filial := DModG.ObtemFilialDeEntidade(DmNFSe_0000.QrNFSeDPSCabEmpresa.Value);
      with TFmNFSe_Edit_0201(MyForm) do
      begin
        ImgTipo.SQLType := SQLType;
        FQrNFSeDPSCab := QrNFSeDPSCab;
        FFormaGerarNFSe := Servico;
        //
        if (SQLType = stIns) and (Servico = fgnSubstituicaoNFSe) then
        begin
          EdRpsIDNumero.ValueVariant               := 0;
          EdRpsIDSerie.ValueVariant                := DmNFSe_0000.QrNFSeDPSCabRpsIDSerie.Value;
          EdRpsIDTipo.ValueVariant                 := DmNFSe_0000.QrNFSeDPSCabRpsIDTipo.Value;
          CBRpsIDTipo.KeyValue                     := DmNFSe_0000.QrNFSeDPSCabRpsIDTipo.Value;
          EdCodigo.ValueVariant                    := 0;
          //
          EdSubstNumero.ValueVariant               := DmNFSe_0000.QrNFSeDPSCabRpsIDNumero.Value;
          EdSubstSerie.Text                        := DmNFSe_0000.QrNFSeDPSCabRpsIDSerie.Value;
          EdSubstTipo.ValueVariant                 := DmNFSe_0000.QrNFSeDPSCabRpsIDTipo.Value;
          CBSubstTipo.KeyValue                     := DmNFSe_0000.QrNFSeDPSCabRpsIDTipo.Value;
        end else
        if (SQLType = stUpd) and (Servico = fgnSubstituicaoNFSe) then
        begin
          EdRpsIDNumero.ValueVariant               := DmNFSe_0000.QrNFSeDPSCabRpsIDNumero.Value;
          EdRpsIDSerie.ValueVariant                := DmNFSe_0000.QrNFSeDPSCabRpsIDSerie.Value;
          EdRpsIDTipo.ValueVariant                 := DmNFSe_0000.QrNFSeDPSCabRpsIDTipo.Value;
          CBRpsIDTipo.KeyValue                     := DmNFSe_0000.QrNFSeDPSCabRpsIDTipo.Value;
          EdCodigo.ValueVariant                    := DmNFSe_0000.QrNFSeDPSCabCodigo.Value;
          //
          EdSubstNumero.ValueVariant               := DmNFSe_0000.QrNFSeDPSCabSubstNumero.Value;
          EdSubstSerie.Text                        := DmNFSe_0000.QrNFSeDPSCabSubstSerie.Value;
          EdSubstTipo.ValueVariant                 := DmNFSe_0000.QrNFSeDPSCabSubstTipo.Value;
          CBSubstTipo.KeyValue                     := DmNFSe_0000.QrNFSeDPSCabSubstTipo.Value;
        end else
        if (SQLType = stUpd) and ((Servico = fgnLoteRPSSincrono) or
        (Servico = fgnLoteRPS)) then
        begin
          EdRpsIDNumero.ValueVariant               := DmNFSe_0000.QrNFSeDPSCabRpsIDNumero.Value;
          EdRpsIDSerie.ValueVariant                := DmNFSe_0000.QrNFSeDPSCabRpsIDSerie.Value;
          EdRpsIDTipo.ValueVariant                 := DmNFSe_0000.QrNFSeDPSCabRpsIDTipo.Value;
          CBRpsIDTipo.KeyValue                     := DmNFSe_0000.QrNFSeDPSCabRpsIDTipo.Value;
          EdCodigo.ValueVariant                    := DmNFSe_0000.QrNFSeDPSCabCodigo.Value;
          //
        end else
        begin
          Geral.MB_Erro('Forma de emiss�o de NFSe n�o implemetada!');
          Destroy;
          //
          Exit;
        end;

        EdEmpresa.ValueVariant                   := Filial;
        CBEmpresa.KeyValue                       := Filial;
        TPRpsDataEmissao.Date                    := DmNFSe_0000.QrNFSeDPSCabRpsDataEmissao.Value;
        EdRpsHoraEmissao.Text                    := Geral.FDT(DmNFSe_0000.QrNFSeDPSCabRpsHoraEmissao.Value, 100);
        TPCompetencia.Date                       := DmNFSe_0000.QrNFSeDPSCabCompetencia.Value;
        EdIntermediario.ValueVariant             := DmNFSe_0000.QrNFSeDPSCabIntermediario.Value;
        CBIntermediario.KeyValue                 := DmNFSe_0000.QrNFSeDPSCabIntermediario.Value;
        EdCliente.ValueVariant                   := DmNFSe_0000.QrNFSeDPSCabCliente.Value;
        CBCliente.KeyValue                       := DmNFSe_0000.QrNFSeDPSCabCliente.Value;
        EdNFSeSrvCad.ValueVariant                := DmNFSe_0000.QrNFSeDPSCabNFSeSrvCad.Value;
        CBNFSeSrvCad.KeyValue                    := DmNFSe_0000.QrNFSeDPSCabNFSeSrvCad.Value;
        //
        EdItemListaServico.ValueVariant          := DmNFSe_0000.QrNFSeDPSCabItemListaServico.Value;
        CBItemListaServico.KeyValue              := DmNFSe_0000.QrNFSeDPSCabItemListaServico.Value;
        EdCodigoCnae.ValueVariant                := DmNFSe_0000.QrNFSeDPSCabCodigoCnae.Value;
        CBCodigoCnae.KeyValue                    := DmNFSe_0000.QrNFSeDPSCabCodigoCnae.Value;
        MeDiscriminacao.Text                     := DmNFSe_0000.QrNFSeDPSCabDiscriminacao.Value;
        EdValorServicos.ValueVariant             := DmNFSe_0000.QrNFSeDPSCabValorServicos.Value;
        EdValorDeducoes.ValueVariant             := DmNFSe_0000.QrNFSeDPSCabValorDeducoes.Value;
        EdAliquota.ValueVariant                  := DmNFSe_0000.QrNFSeDPSCabAliquota.Value;
        EdDescontoIncondicionado.ValueVariant    := DmNFSe_0000.QrNFSeDPSCabDescontoIncondicionado.Value;
        EdValorIss.ValueVariant                  := DmNFSe_0000.QrNFSeDPSCabValorIss.Value;
        EdDescontoCondicionado.ValueVariant      := DmNFSe_0000.QrNFSeDPSCabDescontoCondicionado.Value;
        EdValorPis.ValueVariant                  := DmNFSe_0000.QrNFSeDPSCabValorPis.Value;
        EdValorCofins.ValueVariant               := DmNFSe_0000.QrNFSeDPSCabValorCofins.Value;
        EdValorInss.ValueVariant                 := DmNFSe_0000.QrNFSeDPSCabValorInss.Value;
        EdValorIr.ValueVariant                   := DmNFSe_0000.QrNFSeDPSCabValorIr.Value;
        EdValorCsll.ValueVariant                 := DmNFSe_0000.QrNFSeDPSCabValorCsll.Value;
        EdOutrasRetencoes.ValueVariant           := DmNFSe_0000.QrNFSeDPSCabOutrasRetencoes.Value;
        EdIssRetido.ValueVariant                 := DmNFSe_0000.QrNFSeDPSCabIssRetido.Value;
        CBIssRetido.KeyValue                     := DmNFSe_0000.QrNFSeDPSCabIssRetido.Value;
        EdResponsavelRetencao.ValueVariant       := DmNFSe_0000.QrNFSeDPSCabResponsavelRetencao.Value;
        CBResponsavelRetencao.KeyValue           := DmNFSe_0000.QrNFSeDPSCabResponsavelRetencao.Value;
        EdExigibilidadeISS.ValueVariant          := DmNFSe_0000.QrNFSeDPSCabExigibilidadeISS.Value;
        CBExigibilidadeISS.KeyValue              := DmNFSe_0000.QrNFSeDPSCabExigibilidadeISS.Value;
        EdNumeroProcesso.ValueVariant            := DmNFSe_0000.QrNFSeDPSCabNumeroProcesso.Value;
        EdConstrucaoCivilCodigoObra.Text         := DmNFSe_0000.QrNFSeDPSCabConstrucaoCivilCodigoObra.Value;
        EdConstrucaoCivilArt.Text                := DmNFSe_0000.QrNFSeDPSCabConstrucaoCivilArt.Value;
        EdCodigoTributacaoMunicipio.ValueVariant := DmNFSe_0000.QrNFSeDPSCabCodigoTributacaoMunicipio.Value;
        EdQuemPagaISS.ValueVariant               := DmNFSe_0000.QrNFSeDPSCabQuemPagaISS.Value;
        CBQuemPagaISS.KeyValue                   := DmNFSe_0000.QrNFSeDPSCabQuemPagaISS.Value;
        EdOptanteSimplesNacional.ValueVariant    := DmNFSe_0000.QrNFSeDPSCabOptanteSimplesNacional.Value;
        CBOptanteSimplesNacional.KeyValue        := DmNFSe_0000.QrNFSeDPSCabOptanteSimplesNacional.Value;
        EdIncentivoFiscal.ValueVariant           := DmNFSe_0000.QrNFSeDPSCabIncentivoFiscal.Value;
        CBIncentivoFiscal.KeyValue               := DmNFSe_0000.QrNFSeDPSCabIncentivoFiscal.Value;
        EdRegimeEspecialTributacao.ValueVariant  := DmNFSe_0000.QrNFSeDPSCabRegimeEspecialTributacao.Value;
        CBRegimeEspecialTributacao.KeyValue      := DmNFSe_0000.QrNFSeDPSCabRegimeEspecialTributacao.Value;
      end;
    end;
    if TabedForm = nil then
    begin
      TFmNFSe_Edit_0201(MyForm).ShowModal;
      //
      Numero := TFmNFSe_Edit_0201(MyForm).FNumero;
      Serie  := TFmNFSe_Edit_0201(MyForm).FSerie;
      //
      TFmNFSe_Edit_0201(MyForm).Destroy;
    end;
  end
end;

function TNFSe_PF_0201.SubstituiNFSe(Form: TForm; DPS, LoteRPS: Integer;
  QryDPS: TmySQLQuery; LaAviso1, LaAviso2: TLabel): Boolean;
{
var
  RpsId: String;
  Empresa, Ambiente, Codigo, pI, pF: Integer;
  XML: String;
  ACBrNFSeX: TACBrNFSe(*TdmkACBrNFSe*);
  //
  Arquivo, Texto: String;
  StatusRPS: TnfseStatusRPS;
}
begin
  Result := False;
  //
  Geral.MB_Info('Substitui��o de NFS-e n�o implementada!');
{
  MyObjects.Informa2(LaAviso1, laAviso2, True, 'Gerando NFSe');
  ///
  RpsId := UnNFSe_PF_0000.FormataRPSNumero(DPS);
  DmNFSe_0000.ReopenNFSeDPSCab(DPS);
  Empresa := DmNFSe_0000.QrNFSeDPSCabEmpresa.Value;
  Codigo := DmNFSe_0000.QrNFSeDPSCabCodigo.Value;
  Ambiente := DmNFSe_0000.QrFilialNFSeAmbiente.Value;
  //
  ACBrNFSeX := TACBrNFSe(*TdmkACBrNFSe*).Create(Form);
  ConfiguraComponenteACBrNFSe(Empresa, ACBrNFSeX);
  ACBrNFSeX.NotasFiscais.Clear;
  GerarNFSe(ACBrNFSeX, LoteRPS);
  LoadCanc(ACBrNFSeX);

  //Geral.MB_Info(ACBrNFSeX.NotasFiscais.Items[0].XML);
(*
 if (TNFSeCancelarNfse(Self).FNumeroRPS = '') then
    TNFSeCancelarNfse(Self).FNumeroRPS:=TNFSeCancelarNfse(Self).FNotasFiscais.Items[0].NFSe.Numero;
 if (TNFSeCancelarNfse(Self).FCNPJ = '') then
    TNFSeCancelarNfse(Self).FCNPJ:=SomenteNumeros(TNFSeCancelarNfse(Self).FNotasFiscais.Items[0].NFSe.PrestadorServico.IdentificacaoPrestador.Cnpj);
 if (TNFSeCancelarNfse(Self).FIM = '') then
    TNFSeCancelarNfse(Self).FIM:=TNFSeCancelarNfse(Self).FNotasFiscais.Items[0].NFSe.PrestadorServico.IdentificacaoPrestador.InscricaoMunicipal;
 if (TNFSeCancelarNfse(Self).FCodigoMunicipio = '') then
    TNFSeCancelarNfse(Self).FCodigoMunicipio:=TNFSeCancelarNfse(Self).FNotasFiscais.Items[0].NFSe.PrestadorServico.Endereco.CodigoMunicipio;
*)

  //ACBrNFSeX.NotasFiscais.Items[0].






  //
  XML := ACBrNFSeX.NotasFiscais.Items[0].XML;
  DmNFSe_0000.AtualizaXML_No_BD(txmRPS, Empresa, Ambiente, Codigo, XML);
  try
    MyObjects.Informa2(LaAviso1, laAviso2, True, 'Enviando NFSe substituta');
    ///
    if ACBrNFSeX.SubstituiNFSe('1', DPS) then
    begin
      MyObjects.Informa2(LaAviso1, laAviso2, True, 'Lendo retorno NFSe substituta');
      ///
      Arquivo := ACBrNFSeX.Configuracoes.Arquivos.GetPathGer + '\' +
        Geral.FF0(DPS) + '-subst.xml';
      StatusRPS := ACBrNFSeX.NotasFiscais.Items[0].NFSe.Status;
      DmNFSe_0000.AtualizaStatusRPS(LaAviso1, LaAviso2, Ambiente, DPS,
        Arquivo, 'Lendo XML de emiss�o de NFSe', QryDPS);
    end;
  except
    MyObjects.Informa2(LaAviso1, laAviso2, True, 'Lendo retorno erro');
    ///
    DmNFSe_0000.StepNFSeRPS_Lote(LoteRPS,
      DmNFSe_0000.stepLoteRejeitado(), [], [], LaAviso1, LaAviso2);
    Arquivo := ACBrNFSeX.Configuracoes.Arquivos.GetPathGer + '\' +
     Geral.FF0(DPS) + '-subst-c.xml';
    if FileExists(Arquivo) then
    begin
      if dmkPF.LoadArquivoToText(Arquivo, Texto) then
        DmNFSe_0000.ObtemMensagensRetornoLote(Texto, LoteRPS, 'MensagemRetorno');
    end else
     Geral.MB_Erro('Arquivo de log de erro n�o localizado: ' + sLineBreak +
       Arquivo);
  end;
  //
  ACBrNFSeX.NotasFiscais.Clear;
  MyObjects.Informa2(LaAviso1, laAviso2, False, '...');
}
end;

{
melhorar iss
desfazer texto no REEnviaTXT
mostrat PageControl: 1>2, 2>1
tirar MensagemBox('' 'Teste'
nome da empresa no form do lote
}

end.
