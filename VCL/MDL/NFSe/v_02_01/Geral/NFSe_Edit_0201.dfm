object FmNFSe_Edit_0201: TFmNFSe_Edit_0201
  Left = 339
  Top = 185
  Caption = 'NFS-ENVIO-001 :: Declara'#231#227'o de Presta'#231#227'o de Servi'#231'o'
  ClientHeight = 741
  ClientWidth = 992
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel2: TPanel
    Left = 0
    Top = 47
    Width = 992
    Height = 589
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 992
      Height = 589
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 992
        Height = 589
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        TabOrder = 0
        object Panel7: TPanel
          Left = 2
          Top = 15
          Width = 988
          Height = 26
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alTop
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          object Label9: TLabel
            Left = 10
            Top = 5
            Width = 44
            Height = 13
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Empresa:'
          end
          object Label21: TLabel
            Left = 575
            Top = 5
            Width = 42
            Height = 13
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Emiss'#227'o:'
          end
          object Label1: TLabel
            Left = 800
            Top = 5
            Width = 65
            Height = 13
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Compet'#234'ncia:'
          end
          object EdEmpresa: TdmkEditCB
            Left = 73
            Top = 0
            Width = 54
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            OnChange = EdEmpresaChange
            DBLookupComboBox = CBEmpresa
            IgnoraDBLookupComboBox = False
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
          object CBEmpresa: TdmkDBLookupComboBox
            Left = 128
            Top = 0
            Width = 437
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            KeyField = 'Filial'
            ListField = 'NOMEFILIAL'
            ListSource = DModG.DsEmpresas
            TabOrder = 1
            dmkEditCB = EdEmpresa
            UpdType = utYes
            LocF7SQLMasc = '$#'
            LocF7PreDefProc = f7pNone
          end
          object TPRpsDataEmissao: TdmkEditDateTimePicker
            Left = 622
            Top = 0
            Width = 112
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Date = 41131.000000000000000000
            Time = 0.724786689817847200
            TabOrder = 2
            ReadOnly = False
            DefaultEditMask = '!99/99/99;1;_'
            AutoApplyEditMask = True
            UpdType = utYes
            DatePurpose = dmkdpNone
          end
          object EdRpsHoraEmissao: TdmkEdit
            Left = 736
            Top = 0
            Width = 61
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            TabOrder = 3
            FormatType = dmktfTime
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfLong
            HoraFormat = dmkhfLong
            Texto = '00:00:00'
            UpdCampo = 'HoraIni'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
          object TPCompetencia: TdmkEditDateTimePicker
            Left = 868
            Top = 0
            Width = 112
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Date = 41131.000000000000000000
            Time = 0.724786689817847200
            TabOrder = 4
            ReadOnly = False
            DefaultEditMask = '!99/99/99;1;_'
            AutoApplyEditMask = True
            UpdType = utYes
            DatePurpose = dmkdpNone
          end
        end
        object Panel11: TPanel
          Left = 2
          Top = 453
          Width = 988
          Height = 0
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alTop
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 1
        end
        object Panel5: TPanel
          Left = 2
          Top = 41
          Width = 988
          Height = 142
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 2
          object Panel8: TPanel
            Left = 0
            Top = 0
            Width = 569
            Height = 142
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alLeft
            BevelOuter = bvNone
            TabOrder = 0
            object Label16: TLabel
              Left = 10
              Top = 5
              Width = 36
              Height = 13
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'C'#243'digo:'
            end
            object Label17: TLabel
              Left = 121
              Top = 5
              Width = 63
              Height = 13
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Intermedi'#225'rio:'
            end
            object SbIntermediario: TSpeedButton
              Left = 542
              Top = 0
              Width = 23
              Height = 23
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = '...'
              OnClick = SbIntermediarioClick
            end
            object Label34: TLabel
              Left = 10
              Top = 24
              Width = 97
              Height = 13
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Tomador do servi'#231'o:'
            end
            object SbCliente: TSpeedButton
              Left = 542
              Top = 24
              Width = 23
              Height = 23
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = '...'
              OnClick = SbClienteClick
            end
            object Label19: TLabel
              Left = 10
              Top = 48
              Width = 94
              Height = 13
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Minha Regra Fiscal:'
            end
            object SbNFSeSrvCad: TSpeedButton
              Left = 542
              Top = 48
              Width = 23
              Height = 23
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = '...'
              OnClick = SbNFSeSrvCadClick
            end
            object Label18: TLabel
              Left = 10
              Top = 72
              Width = 99
              Height = 13
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Servi'#231'o (LC 116/03):'
            end
            object SbItemListaServico: TSpeedButton
              Left = 542
              Top = 72
              Width = 23
              Height = 23
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = '...'
              OnClick = SbItemListaServicoClick
            end
            object Label36: TLabel
              Left = 10
              Top = 96
              Width = 83
              Height = 13
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'C'#243'digo do CNAE:'
            end
            object SbCodigoCnae: TSpeedButton
              Left = 542
              Top = 96
              Width = 23
              Height = 23
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = '...'
              OnClick = SbCodigoCnaeClick
            end
            object Label28: TLabel
              Left = 10
              Top = 120
              Width = 64
              Height = 13
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Tipo de RPS:'
              Enabled = False
            end
            object SbRpsIDTipo: TSpeedButton
              Left = 346
              Top = 120
              Width = 23
              Height = 23
              Hint = 'Inclui item de carteira'
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = '...'
              Enabled = False
              OnClick = SbRpsIDTipoClick
            end
            object Label29: TLabel
              Left = 372
              Top = 120
              Width = 40
              Height = 13
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'N'#250'mero:'
              Enabled = False
            end
            object Label30: TLabel
              Left = 486
              Top = 120
              Width = 27
              Height = 13
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'S'#233'rie:'
              Enabled = False
            end
            object EdCodigo: TdmkEdit
              Left = 59
              Top = 0
              Width = 54
              Height = 21
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Alignment = taRightJustify
              Enabled = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -15
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
              ReadOnly = True
              TabOrder = 0
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utInc
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
            end
            object EdIntermediario: TdmkEditCB
              Left = 188
              Top = 0
              Width = 56
              Height = 21
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Alignment = taRightJustify
              TabOrder = 1
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              DBLookupComboBox = CBIntermediario
              IgnoraDBLookupComboBox = False
              AutoSetIfOnlyOneReg = setregOnlyManual
            end
            object CBIntermediario: TdmkDBLookupComboBox
              Left = 245
              Top = 0
              Width = 296
              Height = 21
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              KeyField = 'Codigo'
              ListField = 'NOMEENTIDADE'
              ListSource = DsIntermediarios
              TabOrder = 2
              dmkEditCB = EdIntermediario
              UpdType = utYes
              LocF7SQLMasc = '$#'
              LocF7PreDefProc = f7pNone
            end
            object EdCliente: TdmkEditCB
              Left = 114
              Top = 24
              Width = 56
              Height = 21
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Alignment = taRightJustify
              TabOrder = 3
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              DBLookupComboBox = CBCliente
              IgnoraDBLookupComboBox = False
              AutoSetIfOnlyOneReg = setregOnlyManual
            end
            object CBCliente: TdmkDBLookupComboBox
              Left = 171
              Top = 24
              Width = 371
              Height = 21
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              KeyField = 'Codigo'
              ListField = 'NOMEENTIDADE'
              ListSource = DsClientes
              TabOrder = 4
              dmkEditCB = EdCliente
              UpdType = utYes
              LocF7SQLMasc = '$#'
              LocF7PreDefProc = f7pNone
            end
            object EdNFSeSrvCad: TdmkEditCB
              Left = 114
              Top = 48
              Width = 56
              Height = 21
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Alignment = taRightJustify
              TabOrder = 5
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              OnChange = EdNFSeSrvCadChange
              OnEnter = EdNFSeSrvCadEnter
              OnExit = EdNFSeSrvCadExit
              DBLookupComboBox = CBNFSeSrvCad
              IgnoraDBLookupComboBox = False
              AutoSetIfOnlyOneReg = setregOnlyManual
            end
            object CBNFSeSrvCad: TdmkDBLookupComboBox
              Left = 171
              Top = 48
              Width = 371
              Height = 21
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              KeyField = 'Codigo'
              ListField = 'Nome'
              ListSource = DsNFSeSrvCad
              TabOrder = 6
              dmkEditCB = EdNFSeSrvCad
              UpdType = utYes
              LocF7SQLMasc = '$#'
              LocF7PreDefProc = f7pNone
            end
            object EdItemListaServico: TdmkEditCB
              Left = 114
              Top = 72
              Width = 56
              Height = 21
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              TabOrder = 7
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
              OnChange = EdItemListaServicoChange
              DBLookupComboBox = CBItemListaServico
              IgnoraDBLookupComboBox = False
              AutoSetIfOnlyOneReg = setregOnlyManual
            end
            object CBItemListaServico: TdmkDBLookupComboBox
              Left = 171
              Top = 72
              Width = 371
              Height = 21
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              KeyField = 'CodAlf'
              ListField = 'Nome'
              ListSource = DsListServ
              TabOrder = 8
              dmkEditCB = EdItemListaServico
              UpdType = utYes
              LocF7SQLMasc = '$#'
              LocF7PreDefProc = f7pNone
            end
            object EdCodigoCnae: TdmkEditCB
              Left = 114
              Top = 96
              Width = 56
              Height = 21
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              TabOrder = 9
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              QryCampo = 'CodigoCnae'
              UpdCampo = 'CodigoCnae'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
              DBLookupComboBox = CBCodigoCnae
              IgnoraDBLookupComboBox = False
              AutoSetIfOnlyOneReg = setregOnlyManual
            end
            object CBCodigoCnae: TdmkDBLookupComboBox
              Left = 171
              Top = 96
              Width = 371
              Height = 21
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              KeyField = 'CodAlf'
              ListField = 'Nome'
              ListSource = DsCNAE21Cad
              TabOrder = 10
              dmkEditCB = EdCodigoCnae
              QryCampo = 'CodigoCnae'
              UpdType = utYes
              LocF7SQLMasc = '$#'
              LocF7PreDefProc = f7pNone
            end
            object EdRpsIDTipo: TdmkEditCB
              Left = 114
              Top = 120
              Width = 24
              Height = 21
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Alignment = taRightJustify
              Enabled = False
              TabOrder = 11
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              DBLookupComboBox = CBRpsIDTipo
              IgnoraDBLookupComboBox = False
              AutoSetIfOnlyOneReg = setregOnlyManual
            end
            object CBRpsIDTipo: TdmkDBLookupComboBox
              Left = 138
              Top = 120
              Width = 208
              Height = 21
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Enabled = False
              KeyField = 'Codigo'
              ListField = 'Nome'
              ListSource = DsTsTipoRps1
              TabOrder = 12
              dmkEditCB = EdRpsIDTipo
              UpdType = utYes
              LocF7SQLMasc = '$#'
              LocF7PreDefProc = f7pNone
            end
            object EdRpsIdNumero: TdmkEdit
              Left = 415
              Top = 120
              Width = 64
              Height = 21
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Alignment = taRightJustify
              Enabled = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -15
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
              TabOrder = 13
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utInc
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
            end
            object EdRpsIdSerie: TdmkEdit
              Left = 517
              Top = 120
              Width = 48
              Height = 21
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Enabled = False
              TabOrder = 14
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              QryCampo = 'NumOC'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
          end
          object Panel9: TPanel
            Left = 569
            Top = 0
            Width = 419
            Height = 142
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alClient
            BevelOuter = bvNone
            Caption = 'Panel9'
            TabOrder = 1
            object dmkLabelRotate1: TdmkLabelRotate
              Left = 0
              Top = 0
              Width = 21
              Height = 142
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Angle = ag90
              Caption = 'Discrimina'#231#227'o:'
              Font.Charset = ANSI_CHARSET
              Font.Color = clBlack
              Font.Height = -15
              Font.Name = 'Arial'
              Font.Style = []
              Align = alLeft
              ExplicitHeight = 177
            end
            object MeDiscriminacao: TdmkMemo
              Left = 21
              Top = 0
              Width = 398
              Height = 142
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              TabStop = False
              Align = alClient
              TabOrder = 0
              WantReturns = False
              OnKeyDown = MeDiscriminacaoKeyDown
              UpdType = utYes
            end
          end
        end
        object Panel10: TPanel
          Left = 2
          Top = 183
          Width = 988
          Height = 206
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 3
          object GroupBox3: TGroupBox
            Left = 10
            Top = 5
            Width = 555
            Height = 60
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = ' Valor do Servi'#231'o e ISS: '
            TabOrder = 0
            object Label5: TLabel
              Left = 10
              Top = 16
              Width = 48
              Height = 13
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = '$ Servi'#231'o:'
            end
            object Label6: TLabel
              Left = 119
              Top = 16
              Width = 61
              Height = 13
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = '$ Dedu'#231#245'es:'
            end
            object Label7: TLabel
              Left = 219
              Top = 16
              Width = 52
              Height = 13
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = '% Aliquota:'
            end
            object Label32: TLabel
              Left = 277
              Top = 16
              Width = 81
              Height = 13
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = '$ Desco. incond:'
            end
            object Label33: TLabel
              Left = 460
              Top = 16
              Width = 81
              Height = 13
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = '$ Desco. condic:'
            end
            object Label8: TLabel
              Left = 371
              Top = 16
              Width = 29
              Height = 13
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = '$ ISS:'
            end
            object EdValorServicos: TdmkEdit
              Left = 10
              Top = 31
              Width = 107
              Height = 21
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Alignment = taRightJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -15
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
              TabOrder = 0
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              UpdType = utInc
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
              OnChange = EdValorServicosChange
            end
            object EdValorDeducoes: TdmkEdit
              Left = 120
              Top = 31
              Width = 95
              Height = 21
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Alignment = taRightJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -15
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
              TabOrder = 1
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              UpdType = utInc
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
              OnChange = EdValorDeducoesChange
            end
            object EdAliquota: TdmkEdit
              Left = 219
              Top = 31
              Width = 55
              Height = 21
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Alignment = taRightJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -15
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
              TabOrder = 2
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              UpdType = utInc
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
              OnChange = EdAliquotaChange
            end
            object EdDescontoIncondicionado: TdmkEdit
              Left = 277
              Top = 31
              Width = 91
              Height = 21
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Alignment = taRightJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -15
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
              TabOrder = 3
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              UpdType = utInc
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
              OnChange = EdDescontoIncondicionadoChange
            end
            object EdDescontoCondicionado: TdmkEdit
              Left = 460
              Top = 31
              Width = 91
              Height = 21
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Alignment = taRightJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -15
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
              TabOrder = 5
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              UpdType = utInc
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
            end
            object EdValorIss: TdmkEdit
              Left = 371
              Top = 31
              Width = 87
              Height = 21
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Alignment = taRightJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -15
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
              TabOrder = 4
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              UpdType = utInc
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
            end
          end
          object GroupBox2: TGroupBox
            Left = 574
            Top = 5
            Width = 513
            Height = 60
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = ' Recibo Provis'#243'rio de Servi'#231'o (RPS) Substitu'#237'do: '
            TabOrder = 1
            object Label2: TLabel
              Left = 10
              Top = 16
              Width = 40
              Height = 13
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'N'#250'mero:'
            end
            object Label3: TLabel
              Left = 109
              Top = 16
              Width = 27
              Height = 13
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'S'#233'rie:'
            end
            object Label4: TLabel
              Left = 172
              Top = 16
              Width = 64
              Height = 13
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Tipo de RPS:'
            end
            object SbSubstTipo: TSpeedButton
              Left = 385
              Top = 27
              Width = 23
              Height = 23
              Hint = 'Inclui item de carteira'
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = '...'
              Enabled = False
              OnClick = SbSubstTipoClick
            end
            object EdSubstNumero: TdmkEdit
              Left = 10
              Top = 31
              Width = 96
              Height = 21
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Alignment = taRightJustify
              Enabled = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -15
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
              TabOrder = 0
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utInc
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
            end
            object EdSubstSerie: TdmkEdit
              Left = 109
              Top = 31
              Width = 59
              Height = 21
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Enabled = False
              TabOrder = 1
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              QryCampo = 'NumOC'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object EdSubstTipo: TdmkEditCB
              Left = 173
              Top = 31
              Width = 24
              Height = 21
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Alignment = taRightJustify
              Enabled = False
              TabOrder = 2
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              DBLookupComboBox = CBSubstTipo
              IgnoraDBLookupComboBox = False
              AutoSetIfOnlyOneReg = setregOnlyManual
            end
            object CBSubstTipo: TdmkDBLookupComboBox
              Left = 197
              Top = 31
              Width = 187
              Height = 21
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Enabled = False
              KeyField = 'Codigo'
              ListField = 'Nome'
              ListSource = DsTsTipoRps2
              TabOrder = 3
              dmkEditCB = EdSubstTipo
              UpdType = utYes
              LocF7SQLMasc = '$#'
              LocF7PreDefProc = f7pNone
            end
          end
          object GroupBox4: TGroupBox
            Left = 10
            Top = 64
            Width = 555
            Height = 61
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = ' Tributos Federais:  '
            TabOrder = 2
            object Label10: TLabel
              Left = 10
              Top = 16
              Width = 29
              Height = 13
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = '$ PIS:'
            end
            object Label11: TLabel
              Left = 93
              Top = 16
              Width = 51
              Height = 13
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = '$ COFINS:'
            end
            object Label12: TLabel
              Left = 177
              Top = 16
              Width = 37
              Height = 13
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = '$ INSS:'
            end
            object Label13: TLabel
              Left = 260
              Top = 16
              Width = 23
              Height = 13
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = '$ IR:'
            end
            object Label14: TLabel
              Left = 343
              Top = 16
              Width = 38
              Height = 13
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = '$ CSLL:'
            end
            object Label15: TLabel
              Left = 427
              Top = 16
              Width = 93
              Height = 13
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = '$ Outras reten'#231#245'es:'
            end
            object EdValorPis: TdmkEdit
              Left = 10
              Top = 31
              Width = 79
              Height = 21
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Alignment = taRightJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -15
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
              TabOrder = 0
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              UpdType = utInc
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
            end
            object EdValorCofins: TdmkEdit
              Left = 93
              Top = 31
              Width = 79
              Height = 21
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Alignment = taRightJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -15
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
              TabOrder = 1
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              UpdType = utInc
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
            end
            object EdValorInss: TdmkEdit
              Left = 177
              Top = 31
              Width = 79
              Height = 21
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Alignment = taRightJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -15
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
              TabOrder = 2
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              UpdType = utInc
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
            end
            object EdValorIr: TdmkEdit
              Left = 260
              Top = 31
              Width = 79
              Height = 21
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Alignment = taRightJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -15
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
              TabOrder = 3
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              UpdType = utInc
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
            end
            object EdValorCsll: TdmkEdit
              Left = 343
              Top = 31
              Width = 79
              Height = 21
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Alignment = taRightJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -15
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
              TabOrder = 4
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              UpdType = utInc
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
            end
            object EdOutrasRetencoes: TdmkEdit
              Left = 427
              Top = 31
              Width = 95
              Height = 21
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Alignment = taRightJustify
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -15
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
              TabOrder = 5
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              UpdType = utInc
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
            end
          end
          object GroupBox5: TGroupBox
            Left = 574
            Top = 64
            Width = 513
            Height = 61
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = ' Reten'#231#227'o do ISS: '
            TabOrder = 3
            object SbResponsavelRetencao: TSpeedButton
              Left = 385
              Top = 31
              Width = 23
              Height = 23
              Hint = 'Inclui item de carteira'
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = '...'
              OnClick = SbNFSeSrvCadClick
            end
            object Label35: TLabel
              Left = 139
              Top = 16
              Width = 65
              Height = 13
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Respons'#225'vel:'
            end
            object Label20: TLabel
              Left = 10
              Top = 16
              Width = 37
              Height = 13
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Retido?'
            end
            object EdResponsavelRetencao: TdmkEditCB
              Left = 139
              Top = 31
              Width = 56
              Height = 21
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Alignment = taRightJustify
              TabOrder = 2
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              DBLookupComboBox = CBResponsavelRetencao
              IgnoraDBLookupComboBox = False
              AutoSetIfOnlyOneReg = setregOnlyManual
            end
            object CBResponsavelRetencao: TdmkDBLookupComboBox
              Left = 196
              Top = 31
              Width = 187
              Height = 21
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              KeyField = 'Codigo'
              ListField = 'Nome'
              ListSource = DsTsResponsavelRetencao
              TabOrder = 3
              dmkEditCB = EdResponsavelRetencao
              UpdType = utYes
              LocF7SQLMasc = '$#'
              LocF7PreDefProc = f7pNone
            end
            object EdIssRetido: TdmkEditCB
              Left = 10
              Top = 31
              Width = 29
              Height = 21
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Alignment = taRightJustify
              TabOrder = 0
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              QryCampo = 'OptanteSimplesNacional'
              UpdCampo = 'OptanteSimplesNacional'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              DBLookupComboBox = CBIssRetido
              IgnoraDBLookupComboBox = False
              AutoSetIfOnlyOneReg = setregOnlyManual
            end
            object CBIssRetido: TdmkDBLookupComboBox
              Left = 39
              Top = 31
              Width = 96
              Height = 21
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              KeyField = 'Codigo'
              ListField = 'Nome'
              ListSource = DsTsSimNao3
              TabOrder = 1
              dmkEditCB = EdIssRetido
              QryCampo = 'OptanteSimplesNacional'
              UpdType = utYes
              LocF7SQLMasc = '$#'
              LocF7PreDefProc = f7pNone
            end
          end
          object GroupBox6: TGroupBox
            Left = 10
            Top = 126
            Width = 679
            Height = 75
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = ' Exigibilidade do ISS:'
            TabOrder = 4
            object SpeedButton6: TSpeedButton
              Left = 649
              Top = 20
              Width = 23
              Height = 23
              Hint = 'Inclui item de carteira'
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = '...'
            end
            object Label39: TLabel
              Left = 10
              Top = 25
              Width = 158
              Height = 13
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'C'#243'digo de natureza da opera'#231#227'o:'
            end
            object Label38: TLabel
              Left = 6
              Top = 50
              Width = 362
              Height = 13
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 
                'N'#250'mero do processo judicial ou administrativo de suspens'#227'o da ex' +
                'igibilidade:'
            end
            object EdExigibilidadeIss: TdmkEditCB
              Left = 172
              Top = 20
              Width = 29
              Height = 21
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Alignment = taRightJustify
              TabOrder = 0
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              QryCampo = 'ExigibilidadeIss'
              UpdCampo = 'ExigibilidadeIss'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              DBLookupComboBox = CBExigibilidadeIss
              IgnoraDBLookupComboBox = False
              AutoSetIfOnlyOneReg = setregOnlyManual
            end
            object CBExigibilidadeIss: TdmkDBLookupComboBox
              Left = 201
              Top = 20
              Width = 448
              Height = 21
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              KeyField = 'Codigo'
              ListField = 'Nome'
              ListSource = DsTsExigibilidadeIss
              TabOrder = 1
              dmkEditCB = EdExigibilidadeIss
              QryCampo = 'ExigibilidadeIss'
              UpdType = utYes
              LocF7SQLMasc = '$#'
              LocF7PreDefProc = f7pNone
            end
            object EdNumeroProcesso: TdmkEdit
              Left = 371
              Top = 45
              Width = 300
              Height = 21
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              TabOrder = 2
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              QryCampo = 'NumeroProcesso'
              UpdCampo = 'NumeroProcesso'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
          end
          object GroupBox8: TGroupBox
            Left = 695
            Top = 126
            Width = 290
            Height = 75
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = ' Constru'#231#227'o Civil: '
            TabOrder = 5
            object Label23: TLabel
              Left = 10
              Top = 25
              Width = 77
              Height = 13
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'C'#243'digo de Obra:'
            end
            object Label24: TLabel
              Left = 10
              Top = 49
              Width = 61
              Height = 13
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'C'#243'digo ART:'
            end
            object EdConstrucaoCivilCodigoObra: TdmkEdit
              Left = 92
              Top = 18
              Width = 160
              Height = 21
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              TabOrder = 0
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              QryCampo = 'ConstrucaoCivilCodigoObra'
              UpdCampo = 'ConstrucaoCivilCodigoObra'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object EdConstrucaoCivilArt: TdmkEdit
              Left = 92
              Top = 44
              Width = 160
              Height = 21
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              TabOrder = 1
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              QryCampo = 'ConstrucaoCivilArt'
              UpdCampo = 'ConstrucaoCivilArt'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
          end
        end
        object Panel6: TPanel
          Left = 2
          Top = 389
          Width = 988
          Height = 64
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 4
          object GroupBox7: TGroupBox
            Left = 10
            Top = 0
            Width = 335
            Height = 61
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = ' Munic'#237'pio de incid'#234'ncia do ISS: '
            TabOrder = 0
            object SbMunicipioIncidencia: TSpeedButton
              Left = 306
              Top = 31
              Width = 23
              Height = 23
              Hint = 'Inclui item de carteira'
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = '...'
              Enabled = False
            end
            object Label22: TLabel
              Left = 10
              Top = 16
              Width = 110
              Height = 13
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'C'#243'digo DTB munic'#237'pio:'
            end
            object EdQuemPagaISS: TdmkEditCB
              Left = 9
              Top = 31
              Width = 56
              Height = 21
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Alignment = taRightJustify
              TabOrder = 0
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              DBLookupComboBox = CBQuemPagaISS
              IgnoraDBLookupComboBox = False
              AutoSetIfOnlyOneReg = setregOnlyManual
            end
            object CBQuemPagaISS: TdmkDBLookupComboBox
              Left = 67
              Top = 31
              Width = 240
              Height = 21
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              KeyField = 'Codigo'
              ListField = 'Nome'
              ListSource = DsTsResponsavelRetencao
              TabOrder = 1
              dmkEditCB = EdQuemPagaISS
              UpdType = utYes
              LocF7SQLMasc = '$#'
              LocF7PreDefProc = f7pNone
            end
          end
          object GroupBox11: TGroupBox
            Left = 350
            Top = 0
            Width = 171
            Height = 61
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = ' Optante pelo Simples Nacional: '
            TabOrder = 1
            object SpeedButton8: TSpeedButton
              Left = 136
              Top = 31
              Width = 23
              Height = 23
              Hint = 'Inclui item de carteira'
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = '...'
              Enabled = False
            end
            object Label25: TLabel
              Left = 10
              Top = 16
              Width = 35
              Height = 13
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Optou?'
            end
            object EdOptanteSimplesNacional: TdmkEditCB
              Left = 10
              Top = 31
              Width = 29
              Height = 21
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Alignment = taRightJustify
              TabOrder = 0
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              QryCampo = 'OptanteSimplesNacional'
              UpdCampo = 'OptanteSimplesNacional'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              DBLookupComboBox = CBOptanteSimplesNacional
              IgnoraDBLookupComboBox = False
              AutoSetIfOnlyOneReg = setregOnlyManual
            end
            object CBOptanteSimplesNacional: TdmkDBLookupComboBox
              Left = 39
              Top = 31
              Width = 94
              Height = 21
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              KeyField = 'Codigo'
              ListField = 'Nome'
              ListSource = DsTsSimNao1
              TabOrder = 1
              dmkEditCB = EdOptanteSimplesNacional
              QryCampo = 'OptanteSimplesNacional'
              UpdType = utYes
              LocF7SQLMasc = '$#'
              LocF7PreDefProc = f7pNone
            end
          end
          object GroupBox12: TGroupBox
            Left = 526
            Top = 0
            Width = 167
            Height = 61
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = ' Incentivo Fiscal: '
            TabOrder = 2
            object SpeedButton9: TSpeedButton
              Left = 136
              Top = 31
              Width = 23
              Height = 23
              Hint = 'Inclui item de carteira'
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = '...'
              Enabled = False
            end
            object Label26: TLabel
              Left = 10
              Top = 16
              Width = 27
              Height = 13
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Tem?'
            end
            object EdIncentivoFiscal: TdmkEditCB
              Left = 10
              Top = 31
              Width = 29
              Height = 21
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Alignment = taRightJustify
              TabOrder = 0
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              QryCampo = 'IncentivoFiscal'
              UpdCampo = 'IncentivoFiscal'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              DBLookupComboBox = CBIncentivoFiscal
              IgnoraDBLookupComboBox = False
              AutoSetIfOnlyOneReg = setregOnlyManual
            end
            object CBIncentivoFiscal: TdmkDBLookupComboBox
              Left = 39
              Top = 31
              Width = 94
              Height = 21
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              KeyField = 'Codigo'
              ListField = 'Nome'
              ListSource = DsTsSimNao2
              TabOrder = 1
              dmkEditCB = EdIncentivoFiscal
              QryCampo = 'IncentivoFiscal'
              UpdType = utYes
              LocF7SQLMasc = '$#'
              LocF7PreDefProc = f7pNone
            end
          end
          object GroupBox9: TGroupBox
            Left = 694
            Top = 0
            Width = 291
            Height = 61
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = ' Regime Especial de Tributa'#231#227'o:'
            TabOrder = 3
            object SpeedButton7: TSpeedButton
              Left = 266
              Top = 31
              Width = 23
              Height = 23
              Hint = 'Inclui item de carteira'
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = '...'
              Enabled = False
            end
            object Label27: TLabel
              Left = 10
              Top = 16
              Width = 114
              Height = 13
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'C'#243'digo da identifica'#231#227'o:'
            end
            object EdRegimeEspecialTributacao: TdmkEditCB
              Left = 10
              Top = 31
              Width = 29
              Height = 21
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Alignment = taRightJustify
              TabOrder = 0
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              QryCampo = 'RegimeEspecialTributacao'
              UpdCampo = 'RegimeEspecialTributacao'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              DBLookupComboBox = CBRegimeEspecialTributacao
              IgnoraDBLookupComboBox = False
              AutoSetIfOnlyOneReg = setregOnlyManual
            end
            object CBRegimeEspecialTributacao: TdmkDBLookupComboBox
              Left = 39
              Top = 31
              Width = 223
              Height = 21
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              KeyField = 'Codigo'
              ListField = 'Nome'
              ListSource = DsTsRegimeEspecialTributacao
              TabOrder = 1
              dmkEditCB = EdRegimeEspecialTributacao
              QryCampo = 'RegimeEspecialTributacao'
              UpdType = utYes
              LocF7SQLMasc = '$#'
              LocF7PreDefProc = f7pNone
            end
          end
        end
        object Panel12: TPanel
          Left = 2
          Top = 453
          Width = 988
          Height = 134
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 5
          object GroupBox13: TGroupBox
            Left = 251
            Top = 1
            Width = 838
            Height = 129
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = '  Lei da Transpar'#234'ncia dos Tributos:'
            TabOrder = 0
            object PnTotTrib: TPanel
              Left = 2
              Top = 36
              Width = 834
              Height = 91
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alClient
              BevelOuter = bvNone
              TabOrder = 0
              Visible = False
              object RGFontTotTrib: TRadioGroup
                Left = 313
                Top = 0
                Width = 104
                Height = 91
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Align = alLeft
                Caption = ' Fonte: '
                ItemIndex = 0
                Items.Strings = (
                  'N'#227'o informada'
                  'IBPTax'
                  'C'#225'lculo pr'#243'prio')
                TabOrder = 0
                OnClick = RGFontTotTribClick
              end
              object RGTabTotTrib: TRadioGroup
                Left = 525
                Top = 0
                Width = 89
                Height = 91
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Align = alLeft
                Caption = ' Tabela: '
                Enabled = False
                ItemIndex = 2
                Items.Strings = (
                  'NCM'
                  'NBS'
                  'LC116')
                TabOrder = 1
              end
              object Panel14: TPanel
                Left = 614
                Top = 0
                Width = 220
                Height = 91
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Align = alClient
                BevelOuter = bvNone
                TabOrder = 2
                object Label44: TLabel
                  Left = 10
                  Top = 5
                  Width = 68
                  Height = 13
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Caption = 'Vers'#227'o tabela:'
                end
                object EdVerTotTrib: TdmkEdit
                  Left = 10
                  Top = 25
                  Width = 88
                  Height = 21
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -15
                  Font.Name = 'MS Sans Serif'
                  Font.Style = []
                  ParentFont = False
                  TabOrder = 0
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 4
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0.0.0'
                  UpdType = utInc
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = '0.0.0'
                  ValWarn = False
                  OnChange = EdAliquotaChange
                end
              end
              object RGTpAliqTotTrib: TRadioGroup
                Left = 417
                Top = 0
                Width = 108
                Height = 91
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Align = alLeft
                Caption = ' Origem do servi'#231'o: '
                Enabled = False
                ItemIndex = 0
                Items.Strings = (
                  'Indefinido'
                  'Nacional'
                  'Importado')
                TabOrder = 3
              end
              object Panel13: TPanel
                Left = 0
                Top = 0
                Width = 313
                Height = 91
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Align = alLeft
                BevelOuter = bvNone
                TabOrder = 4
                object Label31: TLabel
                  Left = 15
                  Top = 1
                  Width = 73
                  Height = 13
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Caption = '$ Base c'#225'lculo:'
                end
                object Label40: TLabel
                  Left = 124
                  Top = 1
                  Width = 63
                  Height = 13
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Caption = '$ Val. Aprox.:'
                end
                object Label41: TLabel
                  Left = 236
                  Top = 1
                  Width = 57
                  Height = 13
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Caption = '% Aliq. aprx:'
                end
                object EdvBasTrib: TdmkEdit
                  Left = 15
                  Top = 17
                  Width = 107
                  Height = 21
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Alignment = taRightJustify
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -15
                  Font.Name = 'MS Sans Serif'
                  Font.Style = []
                  ParentFont = False
                  TabOrder = 0
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 2
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0,00'
                  UpdType = utInc
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  ValWarn = False
                  OnChange = EdValorServicosChange
                end
                object EdvTotTrib: TdmkEdit
                  Left = 125
                  Top = 17
                  Width = 107
                  Height = 21
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Alignment = taRightJustify
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -15
                  Font.Name = 'MS Sans Serif'
                  Font.Style = []
                  ParentFont = False
                  TabOrder = 1
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 2
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0,00'
                  UpdType = utInc
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  ValWarn = False
                  OnChange = EdValorDeducoesChange
                end
                object EdpTotTrib: TdmkEdit
                  Left = 236
                  Top = 17
                  Width = 71
                  Height = 21
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Alignment = taRightJustify
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -15
                  Font.Name = 'MS Sans Serif'
                  Font.Style = []
                  ParentFont = False
                  TabOrder = 2
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 4
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0,0000'
                  UpdType = utInc
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  ValWarn = False
                  OnChange = EdAliquotaChange
                end
              end
            end
            object CkiTotTrib: TdmkCheckBox
              Left = 2
              Top = 15
              Width = 834
              Height = 21
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alTop
              Caption = 'Informar na discrimina'#231#227'o.'
              TabOrder = 1
              OnClick = CkiTotTribClick
              UpdType = utYes
              ValCheck = '1'
              ValUncheck = '0'
              OldValor = #0
            end
          end
          object GroupBox10: TGroupBox
            Left = 10
            Top = 0
            Width = 237
            Height = 61
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = ' Tributa'#231#227'o: '
            TabOrder = 1
            object Label37: TLabel
              Left = 15
              Top = 14
              Width = 165
              Height = 13
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Codigo de tributa'#231#227'o do munic'#237'pio:'
            end
            object EdCodigoTributacaoMunicipio: TdmkEdit
              Left = 15
              Top = 30
              Width = 213
              Height = 21
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              TabOrder = 0
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              QryCampo = 'CodigoTributacaoMunicipio'
              UpdCampo = 'CodigoTributacaoMunicipio'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
          end
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 992
    Height = 47
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 940
      Top = 0
      Width = 52
      Height = 47
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 10
        Top = 10
        Width = 32
        Height = 32
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 59
      Height = 47
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 59
      Top = 0
      Width = 881
      Height = 47
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 9
        Top = 11
        Width = 449
        Height = 32
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Declara'#231#227'o de Presta'#231#227'o de Servi'#231'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -28
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 11
        Top = 14
        Width = 449
        Height = 32
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Declara'#231#227'o de Presta'#231#227'o de Servi'#231'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -28
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 10
        Top = 12
        Width = 449
        Height = 32
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Declara'#231#227'o de Presta'#231#227'o de Servi'#231'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -28
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 636
    Width = 992
    Height = 36
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 988
      Height = 19
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 16
        Top = 2
        Width = 90
        Height = 14
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 15
        Top = 1
        Width = 90
        Height = 14
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 672
    Width = 992
    Height = 69
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 843
      Top = 15
      Width = 147
      Height = 52
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 15
        Top = 4
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 841
      Height = 52
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 15
        Top = 4
        Width = 120
        Height = 40
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
      object RGFormaGeraEEnvia: TRadioGroup
        Left = 172
        Top = 0
        Width = 282
        Height = 60
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = ' Forma de gera'#231#227'o e envio:'
        Columns = 3
        ItemIndex = 1
        Items.Strings = (
          'Nenhum'
          'ACBr'
          'Dermatek')
        TabOrder = 1
        Visible = False
        OnClick = RGFormaGeraEEnviaClick
      end
    end
  end
  object QrClientes: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, '
      'IF(Tipo=0, RazaoSocial, Nome) NOMEENTIDADE,'
      'IF(Tipo=0, ECep, PCep) CEP'
      'FROM entidades'
      'ORDER BY NOMEENTIDADE'
      '')
    Left = 504
    Top = 12
    object QrClientesCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrClientesNOMEENTIDADE: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
    object QrClientesCEP: TIntegerField
      FieldName = 'CEP'
    end
  end
  object DsClientes: TDataSource
    DataSet = QrClientes
    Left = 532
    Top = 12
  end
  object VUEmpresa: TdmkValUsu
    dmkEditCB = EdEmpresa
    Panel = Panel3
    QryCampo = 'Empresa'
    UpdCampo = 'Empresa'
    RefCampo = 'Codigo'
    UpdType = utYes
    Left = 16
    Top = 16
  end
  object QrListServ: TMySQLQuery
    Database = Dmod.ZZDB
    SQL.Strings = (
      'SELECT Codigo, CodAlf, Nome'
      'FROM listserv'
      'ORDER BY Nome')
    Left = 560
    Top = 12
    object QrListServNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 255
    end
    object QrListServCodAlf: TWideStringField
      FieldName = 'CodAlf'
      Required = True
    end
    object QrListServCodigo: TIntegerField
      FieldName = 'Codigo'
    end
  end
  object DsListServ: TDataSource
    DataSet = QrListServ
    Left = 588
    Top = 12
  end
  object QrNFSeSrvCad: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM nfsesrvcad'
      'ORDER BY Nome')
    Left = 616
    Top = 12
    object QrNFSeSrvCadCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrNFSeSrvCadNome: TWideStringField
      FieldName = 'Nome'
      Size = 255
    end
  end
  object DsNFSeSrvCad: TDataSource
    DataSet = QrNFSeSrvCad
    Left = 644
    Top = 12
  end
  object QrIntermediarios: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, IF(Tipo=0, RazaoSocial, Nome) NOMEENTIDADE'
      'FROM entidades'
      'ORDER BY NOMEENTIDADE'
      '')
    Left = 672
    Top = 12
    object QrIntermediariosCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrIntermediariosNOMEENTIDADE: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object DsIntermediarios: TDataSource
    DataSet = QrIntermediarios
    Left = 700
    Top = 12
  end
  object QrTsTipoRps1: TMySQLQuery
    Database = Dmod.ZZDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM tstiporps'
      'ORDER BY Nome')
    Left = 836
    Top = 128
    object QrTsTipoRps1Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrTsTipoRps1Nome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
  end
  object DsTsTipoRps1: TDataSource
    DataSet = QrTsTipoRps1
    Left = 864
    Top = 128
  end
  object QrTsResponsavelRetencao: TMySQLQuery
    Database = Dmod.ZZDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM tsresponsavelretencao'
      'ORDER BY Nome')
    Left = 836
    Top = 156
    object QrTsResponsavelRetencaoCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrTsResponsavelRetencaoNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
  end
  object DsTsResponsavelRetencao: TDataSource
    DataSet = QrTsResponsavelRetencao
    Left = 864
    Top = 156
  end
  object QrTsExigibilidadeIss: TMySQLQuery
    Database = Dmod.ZZDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM tsexigibilidadeiss'
      'ORDER BY Nome')
    Left = 836
    Top = 184
    object QrTsExigibilidadeIssCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrTsExigibilidadeIssNome: TWideStringField
      FieldName = 'Nome'
      Size = 120
    end
  end
  object DsTsExigibilidadeIss: TDataSource
    DataSet = QrTsExigibilidadeIss
    Left = 864
    Top = 184
  end
  object QrCNAE21Cad: TMySQLQuery
    Database = Dmod.ZZDB
    SQL.Strings = (
      'SELECT Codigo, CodAlf, Nome'
      'FROM cnae21Cad'
      'ORDER BY Nome')
    Left = 728
    Top = 12
    object QrCNAE21CadCodAlf: TWideStringField
      FieldName = 'CodAlf'
    end
    object QrCNAE21CadNome: TWideStringField
      FieldName = 'Nome'
      Size = 255
    end
    object QrCNAE21CadCodigo: TIntegerField
      FieldName = 'Codigo'
    end
  end
  object DsCNAE21Cad: TDataSource
    DataSet = QrCNAE21Cad
    Left = 756
    Top = 12
  end
  object QrMeuServico: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *'
      'FROM nfsesrvcad'
      'ORDER BY Nome')
    Left = 948
    Top = 128
    object QrMeuServicoCodigoTributacaoMunicipio: TWideStringField
      FieldName = 'CodigoTributacaoMunicipio'
      Origin = 'nfsesrvcad.CodigoTributacaoMunicipio'
    end
    object QrMeuServicoResponsavelRetencao: TSmallintField
      FieldName = 'ResponsavelRetencao'
      Origin = 'nfsesrvcad.ResponsavelRetencao'
    end
    object QrMeuServicoItemListaServico: TWideStringField
      FieldName = 'ItemListaServico'
      Origin = 'nfsesrvcad.ItemListaServico'
      Size = 5
    end
    object QrMeuServicoCodigoCnae: TWideStringField
      FieldName = 'CodigoCnae'
      Origin = 'nfsesrvcad.CodigoCnae'
    end
    object QrMeuServicoNumeroProcesso: TWideStringField
      FieldName = 'NumeroProcesso'
      Origin = 'nfsesrvcad.NumeroProcesso'
      Size = 30
    end
    object QrMeuServicoExigibilidadeIss: TSmallintField
      FieldName = 'ExigibilidadeIss'
      Origin = 'nfsesrvcad.ExigibilidadeIss'
    end
    object QrMeuServicoCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'nfsesrvcad.Codigo'
    end
    object QrMeuServicoNome: TWideStringField
      FieldName = 'Nome'
      Origin = 'nfsesrvcad.Nome'
      Size = 255
    end
    object QrMeuServicoDiscriminacao: TWideMemoField
      FieldName = 'Discriminacao'
      Required = True
      BlobType = ftWideMemo
      Size = 4
    end
    object QrMeuServicoRegimeEspecialTributacao: TSmallintField
      FieldName = 'RegimeEspecialTributacao'
    end
    object QrMeuServicoOptanteSimplesNacional: TSmallintField
      FieldName = 'OptanteSimplesNacional'
    end
    object QrMeuServicoIncentivoFiscal: TSmallintField
      FieldName = 'IncentivoFiscal'
    end
    object QrMeuServicoRpsTipo: TSmallintField
      FieldName = 'RpsTipo'
    end
    object QrMeuServicoQuemPagaISS: TSmallintField
      FieldName = 'QuemPagaISS'
    end
    object QrMeuServicoAliquota: TFloatField
      FieldName = 'Aliquota'
    end
    object QrMeuServicoNaturezaOperacao: TIntegerField
      FieldName = 'NaturezaOperacao'
    end
    object QrMeuServicoAlqPIS: TFloatField
      FieldName = 'AlqPIS'
    end
    object QrMeuServicoAlqCOFINS: TFloatField
      FieldName = 'AlqCOFINS'
    end
    object QrMeuServicoAlqINSS: TFloatField
      FieldName = 'AlqINSS'
    end
    object QrMeuServicoAlqIR: TFloatField
      FieldName = 'AlqIR'
    end
    object QrMeuServicoAlqCSLL: TFloatField
      FieldName = 'AlqCSLL'
    end
    object QrMeuServicoAlqOutrasRetencoes: TFloatField
      FieldName = 'AlqOutrasRetencoes'
    end
    object QrMeuServicoLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrMeuServicoDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrMeuServicoDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrMeuServicoUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrMeuServicoUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrMeuServicoAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrMeuServicoAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrMeuServicoPDFCompres: TSmallintField
      FieldName = 'PDFCompres'
    end
    object QrMeuServicoPDFEmbFont: TSmallintField
      FieldName = 'PDFEmbFont'
    end
    object QrMeuServicoPDFPrnOptm: TSmallintField
      FieldName = 'PDFPrnOptm'
    end
    object QrMeuServicoImportado: TIntegerField
      FieldName = 'Importado'
    end
    object QrMeuServicoindFinal: TIntegerField
      FieldName = 'indFinal'
    end
  end
  object QrDTB_Munici: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM dtb_munici'
      'ORDER BY Nome')
    Left = 836
    Top = 212
    object QrDTB_MuniciCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrDTB_MuniciNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
  object DsDTB_Munici: TDataSource
    DataSet = QrDTB_Munici
    Left = 864
    Top = 212
  end
  object QrTsRegimeEspecialTributacao: TMySQLQuery
    Database = Dmod.ZZDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM tsregimeespecialtributacao'
      'ORDER BY Nome')
    Left = 780
    Top = 128
    object QrTsRegimeEspecialTributacaoCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrTsRegimeEspecialTributacaoNome: TWideStringField
      FieldName = 'Nome'
      Size = 120
    end
  end
  object QrTsSimNao2: TMySQLQuery
    Database = Dmod.ZZDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM tssimnao'
      'ORDER BY Nome')
    Left = 892
    Top = 184
    object QrTsSimNao2Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrTsSimNao2Nome: TWideStringField
      FieldName = 'Nome'
      Size = 6
    end
  end
  object DsTsRegimeEspecialTributacao: TDataSource
    DataSet = QrTsRegimeEspecialTributacao
    Left = 808
    Top = 128
  end
  object QrTsSimNao1: TMySQLQuery
    Database = Dmod.ZZDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM tssimnao'
      'ORDER BY Nome')
    Left = 892
    Top = 156
    object QrTsSimNao1Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrTsSimNao1Nome: TWideStringField
      FieldName = 'Nome'
      Size = 6
    end
  end
  object DsTsSimNao1: TDataSource
    DataSet = QrTsSimNao1
    Left = 920
    Top = 156
  end
  object DsTsSimNao2: TDataSource
    DataSet = QrTsSimNao2
    Left = 920
    Top = 184
  end
  object QrTsTipoRps2: TMySQLQuery
    Database = Dmod.ZZDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM tstiporps'
      'ORDER BY Nome')
    Left = 892
    Top = 128
    object QrTsTipoRps2Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrTsTipoRps2Nome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
  end
  object DsTsTipoRps2: TDataSource
    DataSet = QrTsTipoRps2
    Left = 920
    Top = 128
  end
  object QrTsSimNao3: TMySQLQuery
    Database = Dmod.ZZDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM tssimnao'
      'ORDER BY Nome')
    Left = 892
    Top = 212
    object QrTsSimNao3Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrTsSimNao3Nome: TWideStringField
      FieldName = 'Nome'
      Size = 6
    end
  end
  object DsTsSimNao3: TDataSource
    DataSet = QrTsSimNao3
    Left = 920
    Top = 212
  end
  object CSTabSheetChamou: TdmkCompoStore
    Left = 16
    Top = 16
  end
end
