object FmNFSe_LoadXML_0201: TFmNFSe_LoadXML_0201
  Left = 339
  Top = 185
  Caption = 'XXX-XXXXX-999 :: ???? ???? ????'
  ClientHeight = 502
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelConfirma: TPanel
    Left = 0
    Top = 454
    Width = 1008
    Height = 48
    Align = alBottom
    TabOrder = 1
    object BtOK: TBitBtn
      Tag = 14
      Left = 20
      Top = 4
      Width = 90
      Height = 40
      Caption = '&Abre'
      Enabled = False
      NumGlyphs = 2
      TabOrder = 0
      OnClick = BtOKClick
    end
    object Panel2: TPanel
      Left = 896
      Top = 1
      Width = 111
      Height = 46
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 2
        Top = 3
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 1008
    Height = 362
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 1008
      Height = 88
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object Label110: TLabel
        Left = 8
        Top = 4
        Width = 39
        Height = 13
        Caption = 'Arquivo:'
      end
      object SpeedButton16: TSpeedButton
        Left = 755
        Top = 20
        Width = 21
        Height = 21
        Caption = '...'
        OnClick = SpeedButton16Click
      end
      object Label1: TLabel
        Left = 8
        Top = 44
        Width = 47
        Height = 13
        Caption = 'Ver. XML:'
      end
      object Label2: TLabel
        Left = 64
        Top = 44
        Width = 59
        Height = 13
        Caption = 'Codifica'#231#227'o:'
      end
      object Label3: TLabel
        Left = 644
        Top = 44
        Width = 17
        Height = 13
        Caption = 'N'#243':'
      end
      object Label4: TLabel
        Left = 128
        Top = 44
        Width = 29
        Height = 13
        Caption = 'Local:'
      end
      object Label5: TLabel
        Left = 260
        Top = 44
        Width = 35
        Height = 13
        Caption = 'Prefixo:'
      end
      object Label6: TLabel
        Left = 308
        Top = 44
        Width = 221
        Height = 13
        Caption = 'Declara'#231#227'o de Namespace (Namespace URI):'
      end
      object EdArquivo: TdmkEdit
        Left = 8
        Top = 20
        Width = 745
        Height = 21
        TabOrder = 0
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = 'C:\Dermatek\NFSe\CNPJ_03143014000152\XML\201210\Ger\173-rec.xml'
        QryCampo = 'DirSchema'
        UpdCampo = 'DirSchema'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 'C:\Dermatek\NFSe\CNPJ_03143014000152\XML\201210\Ger\173-rec.xml'
      end
      object EdVersion: TdmkEdit
        Left = 8
        Top = 60
        Width = 52
        Height = 21
        ReadOnly = True
        TabOrder = 1
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
      end
      object EdEncoding: TdmkEdit
        Left = 64
        Top = 60
        Width = 60
        Height = 21
        ReadOnly = True
        TabOrder = 2
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
      end
      object EdNodeName: TdmkEdit
        Left = 644
        Top = 60
        Width = 133
        Height = 21
        ReadOnly = True
        TabOrder = 3
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
      end
      object EdLocalName: TdmkEdit
        Left = 128
        Top = 60
        Width = 129
        Height = 21
        ReadOnly = True
        TabOrder = 4
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
      end
      object EdPrefix: TdmkEdit
        Left = 260
        Top = 60
        Width = 44
        Height = 21
        ReadOnly = True
        TabOrder = 5
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
      end
      object EdNamespaceURI: TdmkEdit
        Left = 308
        Top = 60
        Width = 333
        Height = 21
        ReadOnly = True
        TabOrder = 6
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
      end
      object RGAmbiente: TRadioGroup
        Left = 900
        Top = 0
        Width = 108
        Height = 88
        Align = alRight
        Caption = ' Ambiente: '
        ItemIndex = 0
        Items.Strings = (
          'N'#227'o sei'
          'Produ'#231#227'o'
          'Homologa'#231#227'o')
        TabOrder = 7
        OnClick = RGAmbienteClick
      end
    end
    object PageControl1: TPageControl
      Left = 0
      Top = 88
      Width = 1008
      Height = 274
      ActivePage = TabSheet1
      Align = alClient
      TabOrder = 1
      object TabSheet1: TTabSheet
        Caption = ' '#193'rvore '
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object TreeView1: TTreeView
          Left = 0
          Top = 0
          Width = 1000
          Height = 246
          Align = alClient
          Indent = 19
          TabOrder = 0
        end
      end
      object TabSheet2: TTabSheet
        Caption = 'Campos ignorados'
        ImageIndex = 1
        ExplicitLeft = 8
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object MeIgnorados: TMemo
          Left = 0
          Top = 0
          Width = 776
          Height = 246
          Align = alClient
          ReadOnly = True
          TabOrder = 0
          ExplicitWidth = 998
          ExplicitHeight = 509
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 912
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 427
        Height = 32
        Caption = 'XXX-XXXXX-999 :: ???? ???? ????'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 427
        Height = 32
        Caption = 'XXX-XXXXX-999 :: ???? ???? ????'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 427
        Height = 32
        Caption = 'XXX-XXXXX-999 :: ???? ???? ????'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 410
    Width = 1008
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object XMLDocument1: TXMLDocument
    Left = 12
    Top = 12
    DOMVendorDesc = 'MSXML'
  end
  object QrPsq: TmySQLQuery
    Database = Dmod.MyDB
    Left = 380
    Top = 148
  end
end
