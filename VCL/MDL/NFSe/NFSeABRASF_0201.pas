unit NFSeABRASF_0201;

interface

uses
  SysUtils, Classes, Forms,
  ComCtrls, XMLDoc, XMLIntf,
  //
  dmkGeral, Windows, Controls, UnMyObjects, UnDmkProcFunc, DmkDAC_PF,
  mySQLDbTables, Module, UMySQLModule, ModuleGeral, NFse_0000_Module,
  //
{$IFNDEF VER130}
  Variants,
{$ENDIF}
  // ini 2023-12-20
  //DMKpcnAuxiliar, DMKpcnConversao, DMKpcnLeitor, DMKpnfsNFSe, DMKpnfsConversao,
  //DmkACBrUtil, DmkACBrNFSeUtil,
  // ini 2023-12-20
  UnDMkEnums;

type

  arrayOfString = array of String;
  arrayOfVariant = array of Variant;

  TipoDeArraySQL = (tasqlDPS, tasqlRPS, tasqlNFSe, tasqlCan);
  TNFSeABRASF_0201 = class(TObject)
  private
    // Arquivos
    //procedure Le_ConsultarNfseResposta(Source: String; XMLNoSorc: IXMLNode; TipoDoc: TipoDeArraySQL);
    procedure Le_EnviarLoteRpsSincronoResposta(Source: String; XMLNoSorc: IXMLNode; TipoDoc: TipoDeArraySQL);
    procedure Le_ConsultarNfseRpsResposta(Source: String; XMLNoSorc: IXMLNode; TipoDoc: TipoDeArraySQL);
    procedure Le_Rps(Source: String; XMLNoSorc: IXMLNode; TipoDoc: TipoDeArraySQL);
    procedure Le_CompNfse(Source: String; XMLNoSorc: IXMLNode; TipoDoc: TipoDeArraySQL);
    procedure Le_CancelarNfseResposta(Source: String; XMLNoSorc: IXMLNode; TipoDoc: TipoDeArraySQL);
    // N�s
    procedure ImportaGrupo_ListaNfse(Source: String; XMLNoSorc: IXMLNode; TipoDoc: TipoDeArraySQL);
    procedure ImportaGrupo_CompNfse(Source: String; XMLNoSorc: IXMLNode; TipoDoc: TipoDeArraySQL);
    procedure ImportaGrupo_Nfse(Source: String; XMLNoSorc: IXMLNode; TipoDoc: TipoDeArraySQL);
    procedure ImportaGrupo_InfNfse(Source: String; XMLNoSorc: IXMLNode; TipoDoc: TipoDeArraySQL);
    procedure ImportaGrupo_ValoresNfse(Source: String; XMLNoSorc: IXMLNode; TipoDoc: TipoDeArraySQL);
    procedure ImportaGrupo_PrestadorServico(Source: String; XMLNoSorc: IXMLNode; TipoDoc: TipoDeArraySQL);
    procedure ImportaGrupo_OrgaoGerador(Source: String; XMLNoSorc: IXMLNode; TipoDoc: TipoDeArraySQL);
    procedure ImportaGrupo_DeclaracaoPrestacaoServico(Source: String; XMLNoSorc: IXMLNode; TipoDoc: TipoDeArraySQL);
    procedure ImportaGrupo_InfDeclaracaoPrestacaoServico(Source: String; XMLNoSorc: IXMLNode; TipoDoc: TipoDeArraySQL);
    procedure ImportaGrupo_IdentificacaoPrestador(Source: String; XMLNoSorc: IXMLNode; TipoDoc: TipoDeArraySQL);
    procedure ImportaGrupo_CpfCnpj(Source: String; XMLNoSorc: IXMLNode; TipoDoc: TipoDeArraySQL);
    procedure ImportaGrupo_Endereco(Source: String; XMLNoSorc: IXMLNode; TipoDoc: TipoDeArraySQL);
    procedure ImportaGrupo_Contato(Source: String; XMLNoSorc: IXMLNode; TipoDoc: TipoDeArraySQL);
    procedure ImportaGrupo_Servico(Source: String; XMLNoSorc: IXMLNode; TipoDoc: TipoDeArraySQL);
    procedure ImportaGrupo_Prestador(Source: String; XMLNoSorc: IXMLNode; TipoDoc: TipoDeArraySQL);
    procedure ImportaGrupo_Tomador(Source: String; XMLNoSorc: IXMLNode; TipoDoc: TipoDeArraySQL);
    procedure ImportaGrupo_IdentificacaoTomador(Source: String; XMLNoSorc: IXMLNode; TipoDoc: TipoDeArraySQL);
    procedure ImportaGrupo_Valores(Source: String; XMLNoSorc: IXMLNode; TipoDoc: TipoDeArraySQL);
    //
    procedure ImportaGrupo_Rps(Source: String; XMLNoSorc: IXMLNode; TipoDoc: TipoDeArraySQL);
    procedure ImportaGrupo_IdentificacaoRps(Source: String; XMLNoSorc: IXMLNode; TipoDoc: TipoDeArraySQL);
    //
    procedure ImportaGrupo_RetCancelamento(Source: String; XMLNoSorc: IXMLNode; TipoDoc: TipoDeArraySQL);
    procedure ImportaGrupo_NfseCancelamento(Source: String; XMLNoSorc: IXMLNode; TipoDoc: TipoDeArraySQL);
    procedure ImportaGrupo_Confirmacao(Source: String; XMLNoSorc: IXMLNode; TipoDoc: TipoDeArraySQL);
    procedure ImportaGrupo_Pedido(Source: String; XMLNoSorc: IXMLNode; TipoDoc: TipoDeArraySQL);
    procedure ImportaGrupo_InfPedidoCancelamento(Source: String; XMLNoSorc: IXMLNode; TipoDoc: TipoDeArraySQL);
    procedure ImportaGrupo_IdentificacaoNfse(Source: String; XMLNoSorc: IXMLNode; TipoDoc: TipoDeArraySQL);
    //
    procedure AddSQLItem(TipoSQL: TipoDeArraySQL; Campo: String; Valor: Variant);
    procedure AvisaItemDesconhecido(Source, Grupo, NodeName: WideString);
    function  DefineDocumento(EntiTipo, Doc: String): String;
    function  NaoPermitePeloAmbiente(): Boolean;
    procedure SetZeroArrs(var Fld: arrayofString; Val: arrayOfVariant);

    procedure SQLInsUpd_Nfse();
    procedure SQLInsUpd_Rps();
    procedure SQLInsUpd_Can();

    function LerXML_NFSe(XMLDocument1: TXMLDocument; Ambiente: Integer): Boolean;
  public
    function LerXML_Arquivo(XMLDocument1: TXMLDocument; Ambiente: Integer; Arquivo: String): Boolean;
    function LerXML_DoXML(XMLDocument1: TXMLDocument; Ambiente: Integer; XML: AnsiString): Boolean;
    function ObtemDiretorioXML_Padrao(const Tabela: TTabelaArqXML;
             const Empresa: Integer; Data: TDateTime): String;
    function ObtemNomeArquivoXML_RPS(const Empresa: Integer; Data: TDateTime;
             Codigo: Integer): String;

  end;

var
  UnNFSeABRASF_0201: TNFSeABRASF_0201;


implementation

{ TNFSeABRASF_0201 }

const
  CO_Toma = 'Toma';
  CO_Presta = 'Presta';
  CO_Interme = 'Interme';

var
  //XMLDocument1: TXMLDocument;
  FXDoc_Version,
  FXDoc_Encoding,
  FXDoc_LocalName,
  FXDoc_Prefix,
  FXDoc_NamespaceURI,
  FXDoc_NodeName: String;
  //
  RGAmbienteItemIndex,
  FAmbiente: Integer;
  FNumeroLote: Integer;
  //
  Fld_Nfse: arrayofString;
  Val_Nfse: arrayofVariant;
  //
  Fld_Can: arrayofString;
  Val_Can: arrayofVariant;
  //
  EntiTipo: String; // CO_Toma, CO_Presta, 'Iterme'
  FFoc_Toma,
  FFoc_Presta,
  FFoc_Interme,
  FFoc_Can,

  FNfse_XML, FRps_XML, FCan_XML: WideString;
  FNfse_Numero,
  FNfse_CodigoVerificacao,
  FNfse_DataEmissao: String;
  //
  FRPS_RpsIDSerie,
  FRPS_RpsIDNumero,
  FRPS_RpsIDTipo: String;

  FCan_NFSeNumero,
  FCan_InscricaoMunicipal,
  FCan_CodigoMunicipio: String;
  //
  FTag_Pre: String;

function TNFSeABRASF_0201.LerXML_Arquivo(XMLDocument1: TXMLDocument; Ambiente: Integer; Arquivo: String): Boolean;
begin
  Result := False;
  XMLDocument1.Active := False;
  if not FileExists(Arquivo) then
  begin
    Geral.MensagemBox('Arquivo n�o localizado:' + #13#10 + Arquivo,
    'Aviso', MB_OK+MB_ICONWARNING);
    Exit;
  end;
  XMLDocument1.FileName := Arquivo;
  XMLDocument1.Active := True;
  //
  Result := LerXML_NFSe(XMLDocument1, Ambiente);
end;

function TNFSeABRASF_0201.LerXML_DoXML(XMLDocument1: TXMLDocument;
  Ambiente: Integer; XML: AnsiString): Boolean;
begin
  Result := False;
  XMLDocument1.Active := False;
  XMLDocument1.LoadFromXML(XML);
  //
  Result := LerXML_NFSe(XMLDocument1, Ambiente);
end;

function TNFSeABRASF_0201.LerXML_NFSe(XMLDocument1: TXMLDocument;  Ambiente:
Integer): Boolean;
var
  xmlNodeA: IXMLNode;
  MyCursor: TCursor;
  Source, NomeNoh: String;
begin
  FTag_Pre := '';
  Source := '';
  XMLDocument1.Active := True;
  //
  FXDoc_Version      := XMLDocument1.Version;
  FXDoc_Encoding     := XMLDocument1.Encoding;
  FXDoc_LocalName    := XMLDocument1.DocumentElement.LocalName;    // Servi�o
  FXDoc_Prefix       := XMLDocument1.DocumentElement.Prefix;       // [nada]
  FXDoc_NamespaceURI := XMLDocument1.DocumentElement.NamespaceURI; // http://www....
  FXDoc_NodeName     := XMLDocument1.DocumentElement.NodeName;     // Servi�o
  //
  Result := False;
  if Ambiente < 1 then
  begin
    Geral.MB_Aviso('Ambiente n�o informado na importa��o de XML!');
    Exit;
  end;
  RGAmbienteItemIndex := Ambiente;
  MyCursor      := Screen.Cursor;
  Screen.Cursor := crHourGlass;
  try
    //
    FAmbiente := -1;
    FNumeroLote := 0;
    //
    //MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Importando dados carregados');
    xmlNodeA := XMLDocument1.DocumentElement;
    NomeNoh  := xmlNodeA.NodeName;
    if LowerCase(NomeNoh) = LowerCase('ConsultarNfseRpsResposta') then
      Le_ConsultarNfseRpsResposta(Source, xmlNodeA, tasqlNfse)
    else
    if LowerCase(NomeNoh) = LowerCase('Rps') then
      Le_Rps(Source, xmlNodeA, tasqlRPS)
    else
    if LowerCase(NomeNoh) = LowerCase('CompNfse') then
      Le_CompNfse(Source, xmlNodeA, tasqlRPS)
    else
    if LowerCase(NomeNoh) = LowerCase('EnviarLoteRpsSincronoResposta') then
      Le_EnviarLoteRpsSincronoResposta(Source, xmlNodeA, tasqlNfse)
    else
    if LowerCase(NomeNoh) = LowerCase('CancelarNfseResposta') then
      Le_CancelarNfseResposta(Source, xmlNodeA, tasqlCan)
    (*
    else
    if LowerCase(xmlNodeA.NodeName) = LowerCase('ConsultarNfseServicoPrestadoResposta') then
      Le_ConsultarNfseResposta(Source, xmlNodeA, tasqlNfse)
    *)
    else
      Geral.MB_Aviso('XML com n� principal desconhecido:' + #13#10 + #13#10 +
      xmlNodeA.NodeName);
    //
    Result := True;
  finally
    Screen.Cursor := MyCursor;
  end;
end;

procedure TNFSeABRASF_0201.Le_CancelarNfseResposta(Source: String;
  XMLNoSorc: IXMLNode; TipoDoc: TipoDeArraySQL);
const
  G = '454R';
var
  I: Integer;
  AttrNode, XMLNohSubNivel: IXMLNode;
  Campo: String;
begin
  if not (XMLNoSorc.NodeType = ntElement) then
    Exit;
  if XMLNoSorc.IsTextElement then
  begin
    Geral.MB_Aviso('Elemento de texto desconhecido no Grupo ' + G + ':' +
    #13#10 + XMLNoSorc.Text);
  end;
  for I := 0 to XMLNoSorc.AttributeNodes.Count - 1 do
  begin
    AttrNode := XMLNoSorc.AttributeNodes.Nodes[I];
    Campo := Lowercase(AttrNode.NodeName);
    {
    if Campo = LowerCase('versao') then
      EdenviNFe_versao.ValueVariant := ValorFlu(AttrNode) else
    //
    }
    AvisaItemDesconhecido(Source, G, AttrNode.NodeName);
  end;
  // Subn�s
  if XMLNoSorc.HasChildNodes then
  begin
    for I := 0 to XMLNoSorc.ChildNodes.Count - 1 do
    begin
      XMLNohSubNivel := XMLNoSorc.ChildNodes.Nodes[I];
      if (XMLNohSubNivel.NodeType = ntElement) then
      begin
        Campo := Lowercase(XMLNohSubNivel.NodeName);
        if Campo = LowerCase('RetCancelamento') then
          ImportaGrupo_RetCancelamento(Source, XMLNohSubNivel, TipoDoc)
        else
          AvisaItemDesconhecido(Source, G, XMLNohSubNivel.NodeName);
      end;
    end;
  end;
end;

procedure TNFSeABRASF_0201.Le_CompNfse(Source: String; XMLNoSorc: IXMLNode;
  TipoDoc: TipoDeArraySQL);
const
  G = 'CompNfse';
var
  I: Integer;
  AttrNode, XMLNohSubNivel: IXMLNode;
  Tag_Antes, Campo: String;
begin
  Tag_Antes := FTag_Pre;
  FTag_Pre := 'NFS';
  FNfse_XML := XMLNoSorc.XML;
  //
  if not (XMLNoSorc.NodeType = ntElement) then
    Exit;
  if XMLNoSorc.IsTextElement then
  begin
    Geral.MB_Aviso('Elemento de texto desconhecido no Grupo ' + G + ':' +
    #13#10 + XMLNoSorc.Text);
  end;
  for I := 0 to XMLNoSorc.AttributeNodes.Count - 1 do
  begin
    AttrNode := XMLNoSorc.AttributeNodes.Nodes[I];
    Campo := Lowercase(AttrNode.NodeName);
    {
    if Campo = LowerCase('versao') then
      EdenviNFe_versao.ValueVariant := ValorFlu(AttrNode) else
    //
    }
    AvisaItemDesconhecido(Source, G, AttrNode.NodeName);
  end;
  // Subn�s
  if XMLNoSorc.HasChildNodes then
  begin
    for I := 0 to XMLNoSorc.ChildNodes.Count - 1 do
    begin
      XMLNohSubNivel := XMLNoSorc.ChildNodes.Nodes[I];
      if (XMLNohSubNivel.NodeType = ntElement) then
      begin
        Campo := Lowercase(XMLNohSubNivel.NodeName);
        //if Campo = LowerCase('Id') then
          //AddSQLItem(TipoDoc, Campo, dmkPF.XMLValTxt(XMLNohSubNivel))
        //else
        if Campo = LowerCase('Nfse') then
          ImportaGrupo_Nfse(Source, XMLNohSubNivel, TipoDoc)
        else
        if Campo = LowerCase('NfseCancelamento') then
          ImportaGrupo_NfseCancelamento(Source, XMLNohSubNivel, TipoDoc)
        else
          AvisaItemDesconhecido(Source, G, XMLNohSubNivel.NodeName);
      end;
    end;
  end;


  //

  SQLInsUpd_Nfse();
  FTag_Pre := Tag_Antes;
end;

{
procedure TNFSeABRASF_0201.Le_ConsultarNfseResposta(Source: String; XMLNoSorc: IXMLNode; TipoDoc: TipoDeArraySQL);
const
  G = '458R';
var
  I: Integer;
  AttrNode, XMLNohSubNivel: IXMLNode;
  Campo: String;
begin
  if not (XMLNoSorc.NodeType = ntElement) then
    Exit;
  if XMLNoSorc.IsTextElement then
  begin
    Geral.MB_Aviso('Elemento de texto desconhecido no Grupo ' + G + ':' +
    #13#10 + XMLNoSorc.Text);
  end;
  for I := 0 to XMLNoSorc.AttributeNodes.Count - 1 do
  begin
    AttrNode := XMLNoSorc.AttributeNodes.Nodes[I];
    Campo := Lowercase(AttrNode.NodeName);
    (*
    if Campo = LowerCase('versao') then
      EdenviNFe_versao.ValueVariant := ValorFlu(AttrNode) else
    //
    *)
    AvisaItemDesconhecido(Source, G, AttrNode.NodeName);
  end;
  // Subn�s
  if XMLNoSorc.HasChildNodes then
  begin
    for I := 0 to XMLNoSorc.ChildNodes.Count - 1 do
    begin
      XMLNohSubNivel := XMLNoSorc.ChildNodes.Nodes[I];
      if (XMLNohSubNivel.NodeType = ntElement) then
      begin
        Campo := Lowercase(XMLNohSubNivel.NodeName);
        if Campo = LowerCase('listanfse') then
          ImportaGrupo_ListaNfse(Source, XMLNohSubNivel, TipoDoc)
        else
          AvisaItemDesconhecido(Source, G, XMLNohSubNivel.NodeName);
      end;
    end;
  end;
end;
}

procedure TNFSeABRASF_0201.Le_ConsultarNfseRpsResposta(Source: String;
  XMLNoSorc: IXMLNode; TipoDoc: TipoDeArraySQL);
const
  G = '457R';
var
  I: Integer;
  AttrNode, XMLNohSubNivel: IXMLNode;
  Campo: String;
begin
  if not (XMLNoSorc.NodeType = ntElement) then
    Exit;
  if XMLNoSorc.IsTextElement then
  begin
    Geral.MB_Aviso('Elemento de texto desconhecido no Grupo ' + G + ':' +
    #13#10 + XMLNoSorc.Text);
  end;
  for I := 0 to XMLNoSorc.AttributeNodes.Count - 1 do
  begin
    AttrNode := XMLNoSorc.AttributeNodes.Nodes[I];
    Campo := Lowercase(AttrNode.NodeName);
    {
    if Campo = LowerCase('versao') then
      EdenviNFe_versao.ValueVariant := ValorFlu(AttrNode) else
    //
    }
    AvisaItemDesconhecido(Source, G, AttrNode.NodeName);
  end;
  // Subn�s
  if XMLNoSorc.HasChildNodes then
  begin
    for I := 0 to XMLNoSorc.ChildNodes.Count - 1 do
    begin
      XMLNohSubNivel := XMLNoSorc.ChildNodes.Nodes[I];
      if (XMLNohSubNivel.NodeType = ntElement) then
      begin
        Campo := Lowercase(XMLNohSubNivel.NodeName);
        if Campo = LowerCase('CompNfse') then
          ImportaGrupo_CompNfse(Source, XMLNohSubNivel, TipoDoc)
        else
          AvisaItemDesconhecido(Source, G, XMLNohSubNivel.NodeName);
      end;
    end;
  end;
end;

procedure TNFSeABRASF_0201.ImportaGrupo_ListaNfse(Source: String; XMLNoSorc: IXMLNode; TipoDoc: TipoDeArraySQL);
const
  G = '458R2';
var
  I: Integer;
  AttrNode, XMLNohSubNivel: IXMLNode;
  Campo: String;
begin
  if not (XMLNoSorc.NodeType = ntElement) then
    Exit;
  if XMLNoSorc.IsTextElement then
  begin
    Geral.MB_Aviso('Elemento de texto desconhecido no Grupo ' + G + ':' +
    #13#10 + XMLNoSorc.Text);
  end;
  for I := 0 to XMLNoSorc.AttributeNodes.Count - 1 do
  begin
    AttrNode := XMLNoSorc.AttributeNodes.Nodes[I];
    Campo := Lowercase(AttrNode.NodeName);
    {
    if Campo = LowerCase('versao') then
      EdenviNFe_versao.ValueVariant := ValorFlu(AttrNode) else
    //
    }
    AvisaItemDesconhecido(Source, G, AttrNode.NodeName);
  end;
  // Subn�s
  if XMLNoSorc.HasChildNodes then
  begin
    for I := 0 to XMLNoSorc.ChildNodes.Count - 1 do
    begin
      XMLNohSubNivel := XMLNoSorc.ChildNodes.Nodes[I];
      if (XMLNohSubNivel.NodeType = ntElement) then
      begin
        Campo := Lowercase(XMLNohSubNivel.NodeName);
        if Campo = LowerCase('CompNfse') then
          ImportaGrupo_CompNfse(Source, XMLNohSubNivel, TipoDoc)
        else
          AvisaItemDesconhecido(Source, G, XMLNohSubNivel.NodeName);
      end;
    end;
  end;
end;

procedure TNFSeABRASF_0201.AddSQLItem(TipoSQL: TipoDeArraySQL; Campo: String;
  Valor: Variant);
var
  N: Integer;
begin
{
  case TipoSQL of
    tasqlDPS:
    begin
      N := Length(Fld_Dps);
      SetLength(Fld_Dps, N + 1);
      SetLength(Val_Dps, N + 1);
      Fld_Dps[N] := Campo;
      Val_Dps[N] := Valor;
    end;
    tasqlNFSe:
    begin
}
      N := Length(Fld_Nfse);
      SetLength(Fld_Nfse, N + 1);
      SetLength(Val_Nfse, N + 1);
      Fld_Nfse[N] := FTag_Pre + Campo;
      Val_Nfse[N] := Valor;
{
    end;
    tasqlRPS:
    begin
      N := Length(Fld_Rps);
      SetLength(Fld_Rps, N + 1);
      SetLength(Val_Rps, N + 1);
      Fld_Rps[N] := Campo;
      Val_Rps[N] := Valor;
    end;
  end;
}
end;

procedure TNFSeABRASF_0201.AvisaItemDesconhecido(Source, Grupo,
  NodeName: WideString);
var
  Txt: String;
begin
  if Source <> '' then
    Txt := Source + '.' + Grupo
  else
    Txt := Grupo;
  //
  if Lowercase(NodeName) = LowerCase('xmlns') then
    //MeIgnorados.Lines.Add(Txt + ': ' + NodeName)
  else
  if Lowercase(NodeName) = LowerCase('xmlns:xsi') then
    //MeIgnorados.Lines.Add(Txt + ': ' + NodeName)
  else
  if Lowercase(NodeName) = LowerCase('xmlns:ds') then
    //MeIgnorados.Lines.Add(Txt + ': ' + NodeName)
  else
  if Lowercase(NodeName) = LowerCase('xmlns:xsd') then
    //MeIgnorados.Lines.Add(Txt + ': ' + NodeName)
  else
  if Lowercase(NodeName) = LowerCase('xsi:schemaLocation') then
    //MeIgnorados.Lines.Add(Txt + ': ' + NodeName)
{
  else
  if Lowercase(NodeName) = LowerCase('xsi:ns2') then
    //MeIgnorados.Lines.Add(Txt + ': ' + NodeName)
}
  else
  if Lowercase(NodeName) = LowerCase('xmlns:ns2') then
    //MeIgnorados.Lines.Add(Txt + ': ' + NodeName)
  else
    Geral.MensagemBox('TAG do XML desconhecido no grupo ' + Txt + ':' +
    #13#10 + NodeName + #13#10 +
    'Avise a Dermatek!', 'Aviso', MB_OK+MB_ICONWARNING);
end;

function TNFSeABRASF_0201.DefineDocumento(EntiTipo, Doc: String): String;
begin
  if Trim(Doc) <> '' then
  begin
    if EntiTipo = CO_Toma then
      FFoc_Toma := Doc
    else
    if EntiTipo = CO_Presta then
      FFoc_Presta := Doc
    else
    if EntiTipo = CO_Interme then
      FFoc_Interme := Doc
    else
    //if EntiTipo = CO_Presta then
      FFoc_Presta := Doc
    ;
  end;
end;

procedure TNFSeABRASF_0201.ImportaGrupo_CompNfse(Source: String; XMLNoSorc: IXMLNode; TipoDoc: TipoDeArraySQL);
const
  G = 'CompNfse';
var
  I: Integer;
  AttrNode, XMLNohSubNivel: IXMLNode;
  Campo: String;
begin
  //Geral.MB_Info(XMLNoSorc.XML);
  FNfse_XML := XMLNoSorc.XML;
  //
  if not (XMLNoSorc.NodeType = ntElement) then
    Exit;
  if XMLNoSorc.IsTextElement then
  begin
    Geral.MB_Aviso('Elemento de texto desconhecido no Grupo ' + G + ':' +
    #13#10 + XMLNoSorc.Text);
  end;
  for I := 0 to XMLNoSorc.AttributeNodes.Count - 1 do
  begin
    AttrNode := XMLNoSorc.AttributeNodes.Nodes[I];
    Campo := Lowercase(AttrNode.NodeName);
    {
    if Campo = LowerCase('versao') then
      EdenviNFe_versao.ValueVariant := ValorFlu(AttrNode) else
    //
    }
    AvisaItemDesconhecido(Source, G, AttrNode.NodeName);
  end;
  // Subn�s
  if XMLNoSorc.HasChildNodes then
  begin
    for I := 0 to XMLNoSorc.ChildNodes.Count - 1 do
    begin
      XMLNohSubNivel := XMLNoSorc.ChildNodes.Nodes[I];
      if (XMLNohSubNivel.NodeType = ntElement) then
      begin
        Campo := Lowercase(XMLNohSubNivel.NodeName);
        if Campo = LowerCase('Nfse') then
          ImportaGrupo_Nfse(Source, XMLNohSubNivel, TipoDoc)
        else
        if Campo = LowerCase('NfseCancelamento') then
          ImportaGrupo_NfseCancelamento(Source, XMLNohSubNivel, TipoDoc)
        else
          AvisaItemDesconhecido(Source, G, XMLNohSubNivel.NodeName);
      end;
    end;
  end;
end;

procedure TNFSeABRASF_0201.ImportaGrupo_Confirmacao(Source: String;
  XMLNoSorc: IXMLNode; TipoDoc: TipoDeArraySQL);
const
  G = 'Confirmacao';
  Prefixo = 'Canc_';
var
  I: Integer;
  AttrNode, XMLNohSubNivel: IXMLNode;
  Campo: String;
begin
  //? SetZeroArrs(Fld_? > Nfse, Val_? > Nfse);
  if not (XMLNoSorc.NodeType = ntElement) then
    Exit;
  if XMLNoSorc.IsTextElement then
  begin
    Geral.MB_Aviso('Elemento de texto desconhecido no Grupo ' + G + ':' +
    #13#10 + XMLNoSorc.Text);
  end;
  for I := 0 to XMLNoSorc.AttributeNodes.Count - 1 do
  begin
    AttrNode := XMLNoSorc.AttributeNodes.Nodes[I];
    Campo := Lowercase(AttrNode.NodeName);
    (*
    if Campo = LowerCase('versao') then
      AddSQLItem(TipoDoc, Campo, dmkPF.XMLVal?(AttrNode))
    else
    *)
    //
    AvisaItemDesconhecido(Source, G, AttrNode.NodeName);
  end;
  // Subn�s
  if XMLNoSorc.HasChildNodes then
  begin
    for I := 0 to XMLNoSorc.ChildNodes.Count - 1 do
    begin
      XMLNohSubNivel := XMLNoSorc.ChildNodes.Nodes[I];
      if (XMLNohSubNivel.NodeType = ntElement) then
      begin
        Campo := Lowercase(XMLNohSubNivel.NodeName);
        if Campo = LowerCase('DataHora') then
          AddSQLItem(TipoDoc, Prefixo + Campo, dmkPF.XMLValDth(XMLNohSubNivel))
        else
        if Campo = LowerCase('Pedido') then
          ImportaGrupo_Pedido(Source, XMLNohSubNivel, TipoDoc)
        else
          AvisaItemDesconhecido(Source, G, XMLNohSubNivel.NodeName);
      end;
    end;
  end;
end;

procedure TNFSeABRASF_0201.ImportaGrupo_Contato(Source: String;
  XMLNoSorc: IXMLNode; TipoDoc: TipoDeArraySQL);
const
  G = 'Contato';
var
  I: Integer;
  AttrNode, XMLNohSubNivel: IXMLNode;
  Campo: String;
begin
  //? SetZeroArrs(Fld_? > Nfse, Val_? > Nfse);
  if not (XMLNoSorc.NodeType = ntElement) then
    Exit;
  if XMLNoSorc.IsTextElement then
  begin
    Geral.MB_Aviso('Elemento de texto desconhecido no Grupo ' + G + ':' +
    #13#10 + XMLNoSorc.Text);
  end;
  for I := 0 to XMLNoSorc.AttributeNodes.Count - 1 do
  begin
    AttrNode := XMLNoSorc.AttributeNodes.Nodes[I];
    Campo := Lowercase(AttrNode.NodeName);
    (*
    if Campo = LowerCase('versao') then
      AddSQLItem(TipoDoc, Campo, dmkPF.XMLVal?(AttrNode))
    else
    *)
    //
    AvisaItemDesconhecido(Source, G, AttrNode.NodeName);
  end;
  // Subn�s
  if XMLNoSorc.HasChildNodes then
  begin
    for I := 0 to XMLNoSorc.ChildNodes.Count - 1 do
    begin
      XMLNohSubNivel := XMLNoSorc.ChildNodes.Nodes[I];
      if (XMLNohSubNivel.NodeType = ntElement) then
      begin
        Campo := Lowercase(XMLNohSubNivel.NodeName);
        if Campo = LowerCase('Telefone') then
          AddSQLItem(TipoDoc, EntiTipo + G + Campo, dmkPF.XMLValTxt(XMLNohSubNivel))
        (* Maring� Ini*)
        else
        if Campo = LowerCase('Telefonea') then
          AddSQLItem(TipoDoc, EntiTipo + G + 'Telefone', dmkPF.XMLValTxt(XMLNohSubNivel))
        (*Maring� Fim*)
        else
        if Campo = LowerCase('Email') then
          AddSQLItem(TipoDoc, EntiTipo + G + Campo, dmkPF.XMLValTxt(XMLNohSubNivel))
        else
        (*
        if Campo = LowerCase('?) then
          ImportaGrupo_?(Source, XMLNohSubNivel)
        else
        *)
          AvisaItemDesconhecido(Source, G, XMLNohSubNivel.NodeName);
      end;
    end;
  end;
end;

procedure TNFSeABRASF_0201.ImportaGrupo_CpfCnpj(Source: String;
  XMLNoSorc: IXMLNode; TipoDoc: TipoDeArraySQL);
const
  G = 'CpfCnpj';
var
  I: Integer;
  AttrNode, XMLNohSubNivel: IXMLNode;
  Campo, Doc: String;
begin
  //? SetZeroArrs(Fld_? > Nfse, Val_? > Nfse);
  if not (XMLNoSorc.NodeType = ntElement) then
    Exit;
  if XMLNoSorc.IsTextElement then
  begin
    Geral.MB_Aviso('Elemento de texto desconhecido no Grupo ' + G + ':' +
    #13#10 + XMLNoSorc.Text);
  end;
  for I := 0 to XMLNoSorc.AttributeNodes.Count - 1 do
  begin
    AttrNode := XMLNoSorc.AttributeNodes.Nodes[I];
    Campo := Lowercase(AttrNode.NodeName);
    (*
    if Campo = LowerCase('versao') then
      AddSQLItem(TipoDoc, Campo, dmkPF.XMLVal?(AttrNode))
    else
    *)
    //
    AvisaItemDesconhecido(Source, G, AttrNode.NodeName);
  end;
  // Subn�s
  if XMLNoSorc.HasChildNodes then
  begin
    for I := 0 to XMLNoSorc.ChildNodes.Count - 1 do
    begin
      XMLNohSubNivel := XMLNoSorc.ChildNodes.Nodes[I];
      if (XMLNohSubNivel.NodeType = ntElement) then
      begin
        Campo := Lowercase(XMLNohSubNivel.NodeName);
        if Campo = LowerCase('Cpf') then
        begin
          Doc := dmkPF.XMLValTxt(XMLNohSubNivel);
          AddSQLItem(TipoDoc, EntiTipo + Campo, Doc);
          DefineDocumento(EntiTipo, Doc);
        end else
        if Campo = LowerCase('Cnpj') then
        begin
          Doc := dmkPF.XMLValTxt(XMLNohSubNivel);
          AddSQLItem(TipoDoc, EntiTipo + Campo, Doc);
          DefineDocumento(EntiTipo, Doc);
        end
        //
        (*
        else
        if Campo = LowerCase('?) then
          ImportaGrupo_?(Source, XMLNohSubNivel)
        *)
        else

          AvisaItemDesconhecido(Source, G, XMLNohSubNivel.NodeName);
      end;
    end;
  end;
end;

procedure TNFSeABRASF_0201.ImportaGrupo_DeclaracaoPrestacaoServico(
  Source: String; XMLNoSorc: IXMLNode; TipoDoc: TipoDeArraySQL);
const
  G = 'DeclaracaoPrestacaoServico';
var
  I: Integer;
  AttrNode, XMLNohSubNivel: IXMLNode;
  Campo: String;
  Tag_Antes: String;
begin
  //SetZeroArrs(Fld_RPS, Val_RPS);
  Tag_Antes := FTag_Pre;
  FTag_Pre := 'DPS';
  if not (XMLNoSorc.NodeType = ntElement) then
    Exit;
  if XMLNoSorc.IsTextElement then
  begin
    Geral.MB_Aviso('Elemento de texto desconhecido no Grupo ' + G + ':' +
    #13#10 + XMLNoSorc.Text);
  end;
  for I := 0 to XMLNoSorc.AttributeNodes.Count - 1 do
  begin
    AttrNode := XMLNoSorc.AttributeNodes.Nodes[I];
    Campo := Lowercase(AttrNode.NodeName);
    (*
    if Campo = LowerCase('versao') then
      AddSQLItem(TipoDoc, Campo, dmkPF.XMLVal?(AttrNode))
    else
    *)
    //
    AvisaItemDesconhecido(Source, G, AttrNode.NodeName);
  end;
  // Subn�s
  if XMLNoSorc.HasChildNodes then
  begin
    for I := 0 to XMLNoSorc.ChildNodes.Count - 1 do
    begin
      XMLNohSubNivel := XMLNoSorc.ChildNodes.Nodes[I];
      if (XMLNohSubNivel.NodeType = ntElement) then
      begin
        Campo := Lowercase(XMLNohSubNivel.NodeName);
        (*
        if Campo = LowerCase('?) then
          AddSQLItem(TipoDoc, Campo, dmkPF.XMLVal?(XMLNohSubNivel))
        else
*)
        if Campo = LowerCase('InfDeclaracaoPrestacaoServico') then
          ImportaGrupo_InfDeclaracaoPrestacaoServico(Source, XMLNohSubNivel, TipoDoc)
        else
          AvisaItemDesconhecido(Source, G, XMLNohSubNivel.NodeName);
      end;
    end;
  end;

  //

  //SQLInsUpd_Rps();
  FTag_Pre := Tag_Antes;
end;

procedure TNFSeABRASF_0201.ImportaGrupo_Endereco(Source: String;
  XMLNoSorc: IXMLNode; TipoDoc: TipoDeArraySQL);
const
  G = 'Endereco';
var
  I: Integer;
  AttrNode, XMLNohSubNivel: IXMLNode;
  Campo: String;
begin
  //? SetZeroArrs(Fld_? > Nfse, Val_? > Nfse);
  if not (XMLNoSorc.NodeType = ntElement) then
    Exit;
  if XMLNoSorc.IsTextElement then
  begin
    Geral.MB_Aviso('Elemento de texto desconhecido no Grupo ' + G + ':' +
    #13#10 + XMLNoSorc.Text);
  end;
  for I := 0 to XMLNoSorc.AttributeNodes.Count - 1 do
  begin
    AttrNode := XMLNoSorc.AttributeNodes.Nodes[I];
    Campo := Lowercase(AttrNode.NodeName);
    //
    (*
    if Campo = LowerCase('versao') then
      AddSQLItem(TipoDoc, Campo, dmkPF.XMLVal?(AttrNode))
    else
    *)
    //
    AvisaItemDesconhecido(Source, G, AttrNode.NodeName);
  end;
  // Subn�s
  if XMLNoSorc.HasChildNodes then
  begin
    for I := 0 to XMLNoSorc.ChildNodes.Count - 1 do
    begin
      XMLNohSubNivel := XMLNoSorc.ChildNodes.Nodes[I];
      if (XMLNohSubNivel.NodeType = ntElement) then
      begin
        Campo := Lowercase(XMLNohSubNivel.NodeName);
        if Campo = LowerCase('Endereco') then
          AddSQLItem(TipoDoc, EntiTipo + Campo, dmkPF.XMLValTxt(XMLNohSubNivel))
        else
        if Campo = LowerCase('Numero') then
          AddSQLItem(TipoDoc, EntiTipo + Campo, dmkPF.XMLValTxt(XMLNohSubNivel))
        else
        if Campo = LowerCase('Complemento') then
          AddSQLItem(TipoDoc, EntiTipo + Campo, dmkPF.XMLValTxt(XMLNohSubNivel))
        else
        if Campo = LowerCase('Bairro') then
          AddSQLItem(TipoDoc, EntiTipo + Campo, dmkPF.XMLValTxt(XMLNohSubNivel))
        else
        if Campo = LowerCase('CodigoMunicipio') then
          AddSQLItem(TipoDoc, EntiTipo + Campo, dmkPF.XMLValInt(XMLNohSubNivel))
        else
        if Campo = LowerCase('Uf') then
          AddSQLItem(TipoDoc, EntiTipo + Campo, dmkPF.XMLValTxt(XMLNohSubNivel))
        else
        if Campo = LowerCase('CodigoPais') then
          AddSQLItem(TipoDoc, EntiTipo + Campo, dmkPF.XMLValTxt(XMLNohSubNivel))
        else
        if Campo = LowerCase('Cep') then
          AddSQLItem(TipoDoc, EntiTipo + Campo, dmkPF.XMLValTxt(XMLNohSubNivel))
        else
        if Campo = LowerCase('Telefone') then
          AddSQLItem(TipoDoc, EntiTipo + Campo, dmkPF.XMLValTxt(XMLNohSubNivel))
        else
        if Campo = LowerCase('Email') then
          AddSQLItem(TipoDoc, EntiTipo + Campo, dmkPF.XMLValTxt(XMLNohSubNivel))
        else
        (*
        if Campo = LowerCase('?) then
          ImportaGrupo_?(Source, XMLNohSubNivel)
        else
        *)
          AvisaItemDesconhecido(Source, G, XMLNohSubNivel.NodeName);
      end;
    end;
  end;
end;

procedure TNFSeABRASF_0201.Le_EnviarLoteRpsSincronoResposta(
  Source: String; XMLNoSorc: IXMLNode; TipoDoc: TipoDeArraySQL);
const
  G = 'EnviarLoteRpsSincronoResposta';
var
  I: Integer;
  AttrNode, XMLNohSubNivel: IXMLNode;
  Campo: String;
  //
  //I64
  NumeroLote: Int64;  // TtsNumeroLote;
  DataRecebimento: String;
  Protocolo: String[0];    // TtsNumeroProtocolo;
  //Status, 
  Ambiente, Codigo: Integer;
begin
  NumeroLote := 0;
  //? SetZeroArrs(Fld_? > Nfse, Val_? > Nfse);
  if not (XMLNoSorc.NodeType = ntElement) then
    Exit;
  if XMLNoSorc.IsTextElement then
  begin
    Geral.MB_Aviso('Elemento de texto desconhecido no Grupo ' + G + ':' +
    #13#10 + XMLNoSorc.Text);
  end;
  for I := 0 to XMLNoSorc.AttributeNodes.Count - 1 do
  begin
    AttrNode := XMLNoSorc.AttributeNodes.Nodes[I];
    Campo := Lowercase(AttrNode.NodeName);
    (*
    if Campo = LowerCase('versao') then
      AddSQLItem(TipoDoc, Campo, dmkPF.XMLVal?(AttrNode))
    else
    *)
    //
    AvisaItemDesconhecido(Source, G, AttrNode.NodeName);
  end;
  // Subn�s
  if XMLNoSorc.HasChildNodes then
  begin
    for I := 0 to XMLNoSorc.ChildNodes.Count - 1 do
    begin
      XMLNohSubNivel := XMLNoSorc.ChildNodes.Nodes[I];
      if (XMLNohSubNivel.NodeType = ntElement) then
      begin
        Campo := Lowercase(XMLNohSubNivel.NodeName);
        if Campo = LowerCase('NumeroLote') then
          //AddSQLItem(TipoDoc, ? + Campo, dmkPF.XMLVal?(XMLNohSubNivel));
          NumeroLote := dmkPF.XMLValInt(XMLNohSubNivel)
        else
        if Campo = LowerCase('DataRecebimento') then
          //AddSQLItem(TipoDoc, ? + Campo, dmkPF.XMLVal?(XMLNohSubNivel));
          DataRecebimento := dmkPF.XMLValDth(XMLNohSubNivel)
        else
        if Campo = LowerCase('Protocolo') then
          //AddSQLItem(TipoDoc, ? + Campo, dmkPF.XMLVal?(XMLNohSubNivel));
          Protocolo := dmkPF.XMLValTxt(XMLNohSubNivel)
        //
        else
        if Campo = LowerCase('ListaNfse') then
        begin
          if NumeroLote <> 0 then
          begin
{}
            UnDmkDAC_PF.AbreMySQLQuery0(DmNFse_0000.QrPsq, Dmod.MyDB, [
            'SELECT Codigo, Ambiente, Status ',
            'FROM nfselrpsc ',
            'WHERE Codigo=' + Geral.FF0(NumeroLote),
            '']);
            Ambiente := DmNFse_0000.QrPsq.FieldByName('Ambiente').AsInteger;
            Codigo := DmNFse_0000.QrPsq.FieldByName('Codigo').AsInteger;
            //Status := DmNFse_0000.QrPsq.FieldByName('Status').AsInteger;
            if Codigo <> 0 then
            begin
              (*
              if Trim(Protocolo) <> '' then
                Status := 100;
              *)
              UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'nfselrpsc', False, [
              'DataRecebimento', 'Protocolo'], ['Codigo'], [
              DataRecebimento, Protocolo], [Codigo], True);
              //
            end;
{}            
            FAmbiente := Ambiente;
            FNumeroLote := NumeroLote;
            //
            ImportaGrupo_ListaNfse(Source, XMLNohSubNivel, TipoDoc);
          end else
            Geral.MB_Aviso('Lote ' + Geral.FI64(NumeroLote) + ' n�o localizado na base de dados!');
        end
        else
          AvisaItemDesconhecido(Source, G, XMLNohSubNivel.NodeName);
      end;
    end;
    // Atualiza Status Lote:
  end;
end;

procedure TNFSeABRASF_0201.Le_Rps(Source: String; XMLNoSorc: IXMLNode;
  TipoDoc: TipoDeArraySQL);
const
  G = 'Rps';
var
  I: Integer;
  AttrNode, XMLNohSubNivel: IXMLNode;
  Tag_Antes, Campo: String;
begin
  Tag_Antes := FTag_Pre;
  FTag_Pre := 'DPS';
  FRps_XML := XMLNoSorc.XML;
  if not (XMLNoSorc.NodeType = ntElement) then
    Exit;
  if XMLNoSorc.IsTextElement then
  begin
    Geral.MB_Aviso('Elemento de texto desconhecido no Grupo ' + G + ':' +
    #13#10 + XMLNoSorc.Text);
  end;
  for I := 0 to XMLNoSorc.AttributeNodes.Count - 1 do
  begin
    AttrNode := XMLNoSorc.AttributeNodes.Nodes[I];
    Campo := Lowercase(AttrNode.NodeName);
    {
    if Campo = LowerCase('versao') then
      EdenviNFe_versao.ValueVariant := ValorFlu(AttrNode) else
    //
    }
    AvisaItemDesconhecido(Source, G, AttrNode.NodeName);
  end;
  // Subn�s
  if XMLNoSorc.HasChildNodes then
  begin
    for I := 0 to XMLNoSorc.ChildNodes.Count - 1 do
    begin
      XMLNohSubNivel := XMLNoSorc.ChildNodes.Nodes[I];
      if (XMLNohSubNivel.NodeType = ntElement) then
      begin
        Campo := Lowercase(XMLNohSubNivel.NodeName);
        //if Campo = LowerCase('Id') then
          //AddSQLItem(TipoDoc, Campo, dmkPF.XMLValTxt(XMLNohSubNivel))
        //else
        if Campo = LowerCase('InfDeclaracaoPrestacaoServico') then
          ImportaGrupo_InfDeclaracaoPrestacaoServico(Source, XMLNohSubNivel, TipoDoc)
        else
          AvisaItemDesconhecido(Source, G, XMLNohSubNivel.NodeName);
      end;
    end;
  end;


  //

  SQLInsUpd_Rps();
  FTag_Pre := Tag_Antes;
end;

function TNFSeABRASF_0201.NaoPermitePeloAmbiente: Boolean;
begin
  Result := True;
  if (FAmbiente <> -1) then
  begin
    if FAmbiente <> RGAmbienteItemIndex then
    begin
      Geral.MB_Aviso('Ambiente informado difere da base de dados!');
      Exit;
    end;
  end;
  //
  if RGAmbienteItemIndex < 1 then
      Geral.MB_Aviso('Ambiente informado � inv�lido!')
  else
    Result := False;
end;

function TNFSeABRASF_0201.ObtemDiretorioXML_Padrao(const Tabela: TTabelaArqXML;
const Empresa: Integer; Data: TDateTime): String;
const
  sProcFunc = 'TNFSeABRASF_0201.ObtemDiretorioXML_Padrao()';
var
  emit_CNPJ, emit_CPF, emit_IE, xDoc, AnoMes, Base: String;
begin
  DModG.ObtemCNPJ_CPF_IE_DeEntidade(Empresa, emit_CNPJ, emit_CPF, emit_IE);
  if emit_CNPJ <> EmptyStr then
    xDoc := Geral.SoNumero_TT(emit_CNPJ)
  else
  if emit_CPF <> EmptyStr then
    xDoc := Geral.SoNumero_TT(emit_CPF)
  else
  if emit_IE <> EmptyStr then
    xDoc := Geral.SoNumero_TT(emit_IE)
  else
    xDoc := '00000000000000';
  //
  AnoMes := FormatDateTime('YYYYMM', Data);
  case Tabela of
    TTabelaArqXML.txmRPS: Base := DModG.QrParamsEmpDirNFSeDPSGer.Value;
    TTabelaArqXML.txmNFS: Base := DModG.QrParamsEmpDirNFSeNFSAut.Value;
    TTabelaArqXML.txmCan: Base := DModG.QrParamsEmpDirNFSeNFSCan.Value;
    else
    begin
       //
       Geral.MB_Erro('"TTabelaArqXML" n�o definida em ' + sProcFunc);
       Result := '';
    end
  end;
  Result := Base + '\' + emit_CNPJ + '\' + AnoMes + '\';
  Result := StringReplace(Result, '\\', '\', [rfReplaceAll, rfIgnoreCase]);
end;

function TNFSeABRASF_0201.ObtemNomeArquivoXML_RPS(const Empresa: Integer; Data: TDateTime;
             Codigo: Integer): String;
const
  sProcFunc = 'TNFSeABRASF_0201.ObtemNomeArquivoXML_Padrao()';
var
  Dir, Arq: String;
begin
  // Parei Aqui! 2023-12-20 ver o que fazer
  Dir := ObtemDiretorioXML_Padrao(TTabelaArqXML.txmRPS, Empresa, Data);
  //
(*
  case Tabela of
    TTabelaArqXML.txmRPS: Arq := DmNFSe_0000.QrFilialDirNFSeDPSGer.Value;
    TTabelaArqXML.txmNFS: Arq := DmNFSe_0000.QrFilialDirNFSeNFSAut.Value;
    TTabelaArqXML.txmCan: Arq := DmNFSe_0000.QrFilialDirNFSeNFSCan.Value;
    else
    begin
       //
       Geral.MB_Erro('"TTabelaArqXML" n�o definida em ' + sProcFunc);
       Result := '';
    end
  end;
*)
end;

procedure TNFSeABRASF_0201.SetZeroArrs(var Fld: arrayofString;
  Val: arrayOfVariant);
begin
  SetLength(Fld, 0);
  SetLength(Val, 0);
end;

procedure TNFSeABRASF_0201.SQLInsUpd_Can();
var
  //I,
  Empresa, Ambiente: Integer;
  Nome, NfsNumero: String;
  //Continua: Boolean;
  //
  XML: String;
  Codigo: Integer;
  //
  RpsIDSerie: String;
  RpsIDTipo, RpsIDNumero: Integer;
begin
  if not DModG.DefineEntidade(FFoc_Presta, True, Empresa, Nome) then
    Exit;
  //
  if NaoPermitePeloAmbiente() then
    Exit;
  Ambiente := RGAmbienteItemIndex;
  //
  NfsNumero := FCan_NFSeNumero;
  UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'nfsenfscab', False,
   Fld_Nfse, [
   'Empresa', 'Ambiente', 'NfsNumero'], Val_NFse, [
   Empresa, Ambiente, NfsNumero], True);
  //
  if DmNFSe_0000.ObtemNumeroRPS_PeloNumeroNFSe(Empresa, Ambiente, NfsNumero,
  RpsIDSerie, RpsIDTipo, RpsIDNumero) then
  begin
    UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'nfsedpscab', False,
     ['Status'], [
     'Empresa', 'Ambiente', 'RpsIDSerie', 'RpsIDTipo', 'RpsIDNumero'], [
     101], [
     Empresa, Ambiente, RpsIDSerie, RpsIDTipo, RpsIDNumero], True);
    //
  end;
  Codigo := Geral.IMV(Geral.SoNumero_TT(FNfse_Numero));
  XML := FCan_XML;
  DmNFSe_0000.AtualizaXML_No_BD(txmCan, Empresa, Ambiente, Codigo, XML);
end;

procedure TNFSeABRASF_0201.SQLInsUpd_Nfse();
var
  I, Empresa, Ambiente: Integer;
  Nome: String;
  //Continua: Boolean;
  //
  Erros: String;
  XML: String;
  Codigo: Integer;
begin
  if not DModG.DefineEntidade(FFoc_Presta, True, Empresa, Nome) then
    Exit;
  //
  if NaoPermitePeloAmbiente() then
    Exit;
  Ambiente := RGAmbienteItemIndex;
  //
  UMyMod.ExecutaMySQLQuery1(Dmod.QrUpd, [
  'DELETE ',
  'FROM nfsenfscab ',
  'WHERE Empresa=' + Geral.FF0(Empresa),
  'AND NfsNumero=' + FNfse_Numero,
  'AND Ambiente='  + Geral.FF0(Ambiente),
  '']);
  //
  I := Length(Fld_Nfse);
  SetLength(Fld_Nfse, I + 5);
  SetLength(Val_Nfse, I + 5);
  //

  Fld_Nfse[I + 0] := 'NfsRpsIDSerie';
  Fld_Nfse[I + 1] := 'NfsRpsIDNumero';
  Fld_Nfse[I + 2] := 'NfsRpsIDTipo';
  Fld_Nfse[I + 3] := 'Empresa';
  Fld_Nfse[I + 4] := 'Ambiente';
  //
  Val_Nfse[I + 0] := FRPS_RpsIDSerie;
  Val_Nfse[I + 1] := FRPS_RpsIDNumero;
  Val_Nfse[I + 2] := FRPS_RpsIDTipo;
  Val_Nfse[I + 3] := Empresa;
  Val_Nfse[I + 4] := Ambiente;
  //
  UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'nfsenfscab', False,
   Fld_Nfse, [], Val_NFse, [], True);
  Erros := '';
  for I := Low(Fld_Nfse) to High(Fld_Nfse) - 1 do
  begin
    try
      UnDmkDAC_PF.AbreMySQLQuery0(DmNFse_0000.QrPsq, Dmod.MyDB, [
      'SELECT ' + Fld_Nfse[I] ,
      'FROM nfsenfscab ',
      'LIMIT 1,1 ',
      '']);

    except
      Erros := Erros + Fld_Nfse[I] + #13#10;
    end;
  end;
  if Erros <> '' then
    Geral.MB_Erro(Erros);
  //
  UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'nfsedpscab', False,
  ['Status'], [
  'Empresa', 'Ambiente', 'RpsIDSerie', 'RpsIDTipo', 'RpsIDNumero'], [
  100], [
  Empresa, Ambiente, FRPS_RpsIDSerie, FRPS_RpsIDTipo, FRPS_RpsIDNumero], True);
  //
  Codigo := Geral.IMV(Geral.SoNumero_TT(FNfse_Numero));
  XML := FNfse_XML;
  DmNFSe_0000.AtualizaXML_No_BD(txmNFS, Empresa, Ambiente, Codigo, XML);
end;

procedure TNFSeABRASF_0201.SQLInsUpd_Rps;
var
  //I,
  Empresa, Ambiente: Integer;
  Nome: String;
  //Continua: Boolean;
  //
  //Erros: String;
  XML: String;
  Codigo: Integer;
begin
  if not DModG.DefineEntidade(FFoc_Presta, True, Empresa, Nome) then
    Exit;
  //
  if NaoPermitePeloAmbiente() then
    Exit;
  Ambiente := RGAmbienteItemIndex;
  //
  Codigo := DmNFSe_0000.ObtemCodigoRPS(Empresa, Geral.IMV(FRPS_RpsIDTipo),
    FRPS_RpsIDSerie, Geral.IMV(FRPS_RpsIDNumero));
  if Codigo <> 0 then
  begin
    XML := FRps_XML;
    DmNFSe_0000.AtualizaXML_No_BD(txmRPS, Empresa, Ambiente, Codigo, XML);
  end;
end;

procedure TNFSeABRASF_0201.ImportaGrupo_IdentificacaoNfse(Source: String;
  XMLNoSorc: IXMLNode; TipoDoc: TipoDeArraySQL);
const
  G = 'IdentificacaoNfse';
var
  I: Integer;
  AttrNode, XMLNohSubNivel: IXMLNode;
  Campo: String;
begin
  //? SetZeroArrs(Fld_? > Nfse, Val_? > Nfse);
  if not (XMLNoSorc.NodeType = ntElement) then
    Exit;
  if XMLNoSorc.IsTextElement then
  begin
    Geral.MB_Aviso('Elemento de texto desconhecido no Grupo ' + G + ':' +
    #13#10 + XMLNoSorc.Text);
  end;
  for I := 0 to XMLNoSorc.AttributeNodes.Count - 1 do
  begin
    AttrNode := XMLNoSorc.AttributeNodes.Nodes[I];
    Campo := Lowercase(AttrNode.NodeName);
    (*
    if Campo = LowerCase('versao') then
      AddSQLItem(TipoDoc, Campo, dmkPF.XMLVal?(AttrNode))
    else
    *)
    //
    AvisaItemDesconhecido(Source, G, AttrNode.NodeName);
  end;
  // Subn�s
  if XMLNoSorc.HasChildNodes then
  begin
    for I := 0 to XMLNoSorc.ChildNodes.Count - 1 do
    begin
      XMLNohSubNivel := XMLNoSorc.ChildNodes.Nodes[I];
      if (XMLNohSubNivel.NodeType = ntElement) then
      begin
        Campo := Lowercase(XMLNohSubNivel.NodeName);
        if Campo = LowerCase('Numero') then
        begin
          FCan_NFSeNumero := Geral.SoNumero_TT(dmkPF.XMLValTxt(XMLNohSubNivel));
          AddSQLItem(TipoDoc, EntiTipo + Campo, FCan_NFSeNumero);
        end else
        if Campo = LowerCase('InscricaoMunicipal') then
        begin
          FCan_InscricaoMunicipal := Geral.SoNumero_TT(dmkPF.XMLValTxt(XMLNohSubNivel));
          AddSQLItem(TipoDoc, EntiTipo + Campo, FCan_InscricaoMunicipal);
        end else
        if Campo = LowerCase('CodigoMunicipio') then
        begin
          FCan_CodigoMunicipio := Geral.SoNumero_TT(dmkPF.XMLValTxt(XMLNohSubNivel));
          AddSQLItem(TipoDoc, EntiTipo + Campo, FCan_CodigoMunicipio);
        end else
        //
        if Campo = LowerCase('CpfCnpj') then
          ImportaGrupo_CpfCnpj(Source, XMLNohSubNivel, TipoDoc)
        else
          AvisaItemDesconhecido(Source, G, XMLNohSubNivel.NodeName);
      end;
    end;
  end;
end;

procedure TNFSeABRASF_0201.ImportaGrupo_IdentificacaoPrestador(
  Source: String; XMLNoSorc: IXMLNode; TipoDoc: TipoDeArraySQL);
const
  G = 'IdentificacaoPrestador';
var
  I: Integer;
  AttrNode, XMLNohSubNivel: IXMLNode;
  Campo, Antes: String;
begin
  Antes := EntiTipo;
  EntiTipo := CO_Presta;
  //? SetZeroArrs(Fld_? > Nfse, Val_? > Nfse);
  if not (XMLNoSorc.NodeType = ntElement) then
    Exit;
  if XMLNoSorc.IsTextElement then
  begin
    Geral.MB_Aviso('Elemento de texto desconhecido no Grupo ' + G + ':' +
    #13#10 + XMLNoSorc.Text);
  end;
  for I := 0 to XMLNoSorc.AttributeNodes.Count - 1 do
  begin
    AttrNode := XMLNoSorc.AttributeNodes.Nodes[I];
    Campo := Lowercase(AttrNode.NodeName);
    (*
    if Campo = LowerCase('versao') then
      AddSQLItem(TipoDoc, Campo, dmkPF.XMLVal?(AttrNode))
    else
    *)
    //
    AvisaItemDesconhecido(Source, G, AttrNode.NodeName);
  end;
  // Subn�s
  if XMLNoSorc.HasChildNodes then
  begin
    for I := 0 to XMLNoSorc.ChildNodes.Count - 1 do
    begin
      XMLNohSubNivel := XMLNoSorc.ChildNodes.Nodes[I];
      if (XMLNohSubNivel.NodeType = ntElement) then
      begin
        Campo := Lowercase(XMLNohSubNivel.NodeName);
        if Campo = LowerCase('InscricaoMunicipal') then
          AddSQLItem(TipoDoc, EntiTipo + Campo, dmkPF.XMLValTxt(XMLNohSubNivel))
        else
        if Campo = LowerCase('CpfCnpj') then
          ImportaGrupo_CpfCnpj(Source, XMLNohSubNivel, TipoDoc)
        else
          AvisaItemDesconhecido(Source, G, XMLNohSubNivel.NodeName);
      end;
    end;
  end;
  EntiTipo := Antes;
end;

procedure TNFSeABRASF_0201.ImportaGrupo_IdentificacaoRps(Source: String;
  XMLNoSorc: IXMLNode; TipoDoc: TipoDeArraySQL);
const
  G = 'IdentificacaoRps';
  Tag = 'RpsID';
var
  I: Integer;
  AttrNode, XMLNohSubNivel: IXMLNode;
  Campo: String;
begin
  //? SetZeroArrs(Fld_? > Nfse, Val_? > Nfse);
  if not (XMLNoSorc.NodeType = ntElement) then
    Exit;
  if XMLNoSorc.IsTextElement then
  begin
    Geral.MB_Aviso('Elemento de texto desconhecido no Grupo ' + G + ':' +
    #13#10 + XMLNoSorc.Text);
  end;
  for I := 0 to XMLNoSorc.AttributeNodes.Count - 1 do
  begin
    AttrNode := XMLNoSorc.AttributeNodes.Nodes[I];
    Campo := Lowercase(AttrNode.NodeName);
    (*
    if Campo = LowerCase('versao') then
      AddSQLItem(TipoDoc, Campo, dmkPF.XMLVal?(AttrNode))
    else
    *)
    //
    AvisaItemDesconhecido(Source, G, AttrNode.NodeName);
  end;
  // Subn�s
  if XMLNoSorc.HasChildNodes then
  begin
    for I := 0 to XMLNoSorc.ChildNodes.Count - 1 do
    begin
      XMLNohSubNivel := XMLNoSorc.ChildNodes.Nodes[I];
      if (XMLNohSubNivel.NodeType = ntElement) then
      begin
        Campo := Lowercase(XMLNohSubNivel.NodeName);
        if Campo = LowerCase('Numero') then
        begin
          FRPS_RpsIDNumero := dmkPF.XMLValTxt(XMLNohSubNivel);
          AddSQLItem(TipoDoc, Tag + Campo, FRPS_RpsIDNumero);
        end else
        if Campo = LowerCase('Serie') then
        begin
          FRPS_RpsIDSerie := dmkPF.XMLValTxt(XMLNohSubNivel);
          AddSQLItem(TipoDoc, Tag + Campo, FRPS_RpsIDSerie);
        end else
        if Campo = LowerCase('Tipo') then
        begin
          FRPS_RpsIDTipo := dmkPF.XMLValTxt(XMLNohSubNivel);
          AddSQLItem(TipoDoc, Tag + Campo, FRPS_RpsIDTipo);
        end else
        //
        (*
        if Campo = LowerCase('?) then
          ImportaGrupo_?(Source, XMLNohSubNivel)
        else
        *)
          AvisaItemDesconhecido(Source, G, XMLNohSubNivel.NodeName);
      end;
    end;
  end;
end;

procedure TNFSeABRASF_0201.ImportaGrupo_IdentificacaoTomador(Source: String;
  XMLNoSorc: IXMLNode; TipoDoc: TipoDeArraySQL);
const
  G = 'IdentificacaoTomador';
var
  I: Integer;
  AttrNode, XMLNohSubNivel: IXMLNode;
  Campo: String;
begin
  //? SetZeroArrs(Fld_? > Nfse, Val_? > Nfse);
  if not (XMLNoSorc.NodeType = ntElement) then
    Exit;
  if XMLNoSorc.IsTextElement then
  begin
    Geral.MB_Aviso('Elemento de texto desconhecido no Grupo ' + G + ':' +
    #13#10 + XMLNoSorc.Text);
  end;
  for I := 0 to XMLNoSorc.AttributeNodes.Count - 1 do
  begin
    AttrNode := XMLNoSorc.AttributeNodes.Nodes[I];
    Campo := Lowercase(AttrNode.NodeName);
    (*
    if Campo = LowerCase('versao') then
      AddSQLItem(TipoDoc, Campo, dmkPF.XMLVal?(AttrNode))
    else
    *)
    //
    AvisaItemDesconhecido(Source, G, AttrNode.NodeName);
  end;
  // Subn�s
  if XMLNoSorc.HasChildNodes then
  begin
    for I := 0 to XMLNoSorc.ChildNodes.Count - 1 do
    begin
      XMLNohSubNivel := XMLNoSorc.ChildNodes.Nodes[I];
      if (XMLNohSubNivel.NodeType = ntElement) then
      begin
        Campo := Lowercase(XMLNohSubNivel.NodeName);
        if Campo = LowerCase('InscricaoMunicipal') then
          AddSQLItem(TipoDoc, Campo, dmkPF.XMLValTxt(XMLNohSubNivel))
        else
        //
        if Campo = LowerCase('CpfCnpj') then
          ImportaGrupo_CpfCnpj(Source, XMLNohSubNivel, TipoDoc)
        else
          AvisaItemDesconhecido(Source, G, XMLNohSubNivel.NodeName);
      end;
    end;
  end;
end;

procedure TNFSeABRASF_0201.ImportaGrupo_InfDeclaracaoPrestacaoServico(
  Source: String; XMLNoSorc: IXMLNode; TipoDoc: TipoDeArraySQL);
const
  G = 'InfDeclaracaoPrestacaoServico';
var
  I: Integer;
  AttrNode, XMLNohSubNivel: IXMLNode;
  Campo: String;
begin
  //? SetZeroArrs(Fld_? > Nfse, Val_? > Nfse);
  if not (XMLNoSorc.NodeType = ntElement) then
    Exit;
  if XMLNoSorc.IsTextElement then
  begin
    Geral.MB_Aviso('Elemento de texto desconhecido no Grupo ' + G + ':' +
    #13#10 + XMLNoSorc.Text);
  end;
  for I := 0 to XMLNoSorc.AttributeNodes.Count - 1 do
  begin
    AttrNode := XMLNoSorc.AttributeNodes.Nodes[I];
    Campo := Lowercase(AttrNode.NodeName);
    //
    if Campo = LowerCase('Id') then
      AddSQLItem(TipoDoc, Campo, dmkPF.XMLValTxt(AttrNode))
    else
    (*
    if Campo = LowerCase('versao') then
      AddSQLItem(TipoDoc, Campo, dmkPF.XMLVal?(AttrNode))
    else
    *)
    //
    AvisaItemDesconhecido(Source, G, AttrNode.NodeName);
  end;
  // Subn�s
  if XMLNoSorc.HasChildNodes then
  begin
    for I := 0 to XMLNoSorc.ChildNodes.Count - 1 do
    begin
      XMLNohSubNivel := XMLNoSorc.ChildNodes.Nodes[I];
      if (XMLNohSubNivel.NodeType = ntElement) then
      begin
        Campo := Lowercase(XMLNohSubNivel.NodeName);
        if Campo = LowerCase('Competencia') then
          AddSQLItem(TipoDoc, Campo, dmkPF.XMLValDta(XMLNohSubNivel))
        else
        if Campo = LowerCase('OptanteSimplesNacional') then
          AddSQLItem(TipoDoc, Campo, dmkPF.XMLValInt(XMLNohSubNivel))
        else
        if Campo = LowerCase('IncentivoFiscal') then
          AddSQLItem(TipoDoc, Campo, dmkPF.XMLValInt(XMLNohSubNivel))
        //
        else
        if Campo = LowerCase('Rps') then
          ImportaGrupo_Rps(Source, XMLNohSubNivel, TipoDoc)
        else
        if Campo = LowerCase('Servico') then
          ImportaGrupo_Servico(Source, XMLNohSubNivel, TipoDoc)
        else
        if Campo = LowerCase('Prestador') then
          ImportaGrupo_Prestador(Source, XMLNohSubNivel, TipoDoc)
        else
        if Campo = LowerCase('Tomador') then
          ImportaGrupo_Tomador(Source, XMLNohSubNivel, TipoDoc)
        else
          AvisaItemDesconhecido(Source, G, XMLNohSubNivel.NodeName);
      end;
    end;
  end;
end;

procedure TNFSeABRASF_0201.ImportaGrupo_InfNfse(Source: String; XMLNoSorc: IXMLNode; TipoDoc: TipoDeArraySQL);
const
  G = 'InfNfse';
var
  I: Integer;
  AttrNode, XMLNohSubNivel: IXMLNode;
  Campo: String;
begin
  if not (XMLNoSorc.NodeType = ntElement) then
    Exit;
  if XMLNoSorc.IsTextElement then
  begin
    Geral.MB_Aviso('Elemento de texto desconhecido no Grupo ' + G + ':' +
    #13#10 + XMLNoSorc.Text);
  end;
  for I := 0 to XMLNoSorc.AttributeNodes.Count - 1 do
  begin
    AttrNode := XMLNoSorc.AttributeNodes.Nodes[I];
    Campo := Lowercase(AttrNode.NodeName);
    {
    if Campo = LowerCase(') then
      AddSQLItem(TipoDoc, Campo, dmkPF.XMLVal?(AttrNode));
    }
    //
    AvisaItemDesconhecido(Source, G, AttrNode.NodeName);
  end;
  // Subn�s
  if XMLNoSorc.HasChildNodes then
  begin
    for I := 0 to XMLNoSorc.ChildNodes.Count - 1 do
    begin
      XMLNohSubNivel := XMLNoSorc.ChildNodes.Nodes[I];
      if (XMLNohSubNivel.NodeType = ntElement) then
      begin
        Campo := Lowercase(XMLNohSubNivel.NodeName);
        if Campo = LowerCase('Numero') then
        begin
          FNfse_Numero := dmkPF.XMLValTxt(XMLNohSubNivel);
          AddSQLItem(TipoDoc, Campo, FNfse_Numero);
        end
        else
        if Campo = LowerCase('CodigoVerificacao') then
        begin
          FNfse_CodigoVerificacao := dmkPF.XMLValTxt(XMLNohSubNivel);
          AddSQLItem(TipoDoc, Campo, FNfse_CodigoVerificacao);
        end
        else
        if Campo = LowerCase('DataEmissao') then
        begin
          FNfse_DataEmissao := dmkPF.XMLValDth(XMLNohSubNivel);
          AddSQLItem(TipoDoc, Campo, FNfse_DataEmissao);
        end
        //
        else
        if Campo = LowerCase('ValoresNfse') then
          ImportaGrupo_ValoresNfse(Source, XMLNohSubNivel, TipoDoc)
        else
        if Campo = LowerCase('PrestadorServico') then
          ImportaGrupo_PrestadorServico(Source, XMLNohSubNivel, TipoDoc)
        else
        if Campo = LowerCase('OrgaoGerador') then
          ImportaGrupo_OrgaoGerador(Source, XMLNohSubNivel, TipoDoc)
        else
        if Campo = LowerCase('DeclaracaoPrestacaoServico') then
          ImportaGrupo_DeclaracaoPrestacaoServico(Source, XMLNohSubNivel, tasqlRPS)
        else
        if Campo = LowerCase('OutrasInformacoes') then
          AddSQLItem(TipoDoc, Campo, dmkPF.XMLValTxt(XMLNohSubNivel))
        else
          AvisaItemDesconhecido(Source, G, XMLNohSubNivel.NodeName);
      end;
    end;
  end;
end;

procedure TNFSeABRASF_0201.ImportaGrupo_InfPedidoCancelamento(Source: String;
  XMLNoSorc: IXMLNode; TipoDoc: TipoDeArraySQL);
const
  G = 'NfseCancelamento';
var
  I: Integer;
  AttrNode, XMLNohSubNivel: IXMLNode;
  Campo: String;
begin
  if not (XMLNoSorc.NodeType = ntElement) then
    Exit;
  if XMLNoSorc.IsTextElement then
  begin
    Geral.MB_Aviso('Elemento de texto desconhecido no Grupo ' + G + ':' +
    #13#10 + XMLNoSorc.Text);
  end;
  for I := 0 to XMLNoSorc.AttributeNodes.Count - 1 do
  begin
    AttrNode := XMLNoSorc.AttributeNodes.Nodes[I];
    Campo := Lowercase(AttrNode.NodeName);
    {
    if Campo = LowerCase(') then
      AddSQLItem(TipoDoc, Campo, dmkPF.XMLVal?(AttrNode));
    }
    if Campo = LowerCase('Id') then
      AddSQLItem(TipoDoc, 'Canc_ID', dmkPF.XMLValTxt(AttrNode))
    else
    if Campo = LowerCase('IdentificacaoNfse') then
      ImportaGrupo_IdentificacaoNfse(Source, XMLNohSubNivel, TipoDoc)
    else
    //
    AvisaItemDesconhecido(Source, G, AttrNode.NodeName);
  end;
  // Subn�s
  if XMLNoSorc.HasChildNodes then
  begin
    for I := 0 to XMLNoSorc.ChildNodes.Count - 1 do
    begin
      XMLNohSubNivel := XMLNoSorc.ChildNodes.Nodes[I];
      if (XMLNohSubNivel.NodeType = ntElement) then
      begin
        Campo := Lowercase(XMLNohSubNivel.NodeName);
        if Campo = LowerCase('CodigoCancelamento') then
          AddSQLItem(TipoDoc, EntiTipo + Campo, dmkPF.XMLValInt(XMLNohSubNivel))
        else
        //
        if Campo = LowerCase('IdentificacaoNfse') then
          ImportaGrupo_IdentificacaoNfse(Source, XMLNohSubNivel, TipoDoc)
        else
          AvisaItemDesconhecido(Source, G, XMLNohSubNivel.NodeName);
      end;
    end;
  end;
end;

procedure TNFSeABRASF_0201.ImportaGrupo_Nfse(Source: String; XMLNoSorc: IXMLNode; TipoDoc: TipoDeArraySQL);
const
  G = 'Nfse';
var
  I: Integer;
  AttrNode, XMLNohSubNivel: IXMLNode;
  Campo: String;
  Tag_Antes: String;
begin
  //SetZeroArrs(Fld_RPS, Val_RPS);
////////////////////////////////////////////////////////////////////////////////
  SetZeroArrs(Fld_Nfse, Val_Nfse);
////////////////////////////////////////////////////////////////////////////////
  Tag_Antes := FTag_Pre;
  FTag_Pre := 'NFS';
////////////////////////////////////////////////////////////////////////////////
  if not (XMLNoSorc.NodeType = ntElement) then
    Exit;
  if XMLNoSorc.IsTextElement then
  begin
    Geral.MB_Aviso('Elemento de texto desconhecido no Grupo ' + G + ':' +
    #13#10 + XMLNoSorc.Text);
  end;
  for I := 0 to XMLNoSorc.AttributeNodes.Count - 1 do
  begin
    AttrNode := XMLNoSorc.AttributeNodes.Nodes[I];
    Campo := Lowercase(AttrNode.NodeName);
    if Campo = LowerCase('versao') then
      AddSQLItem(TipoDoc, Campo, dmkPF.XMLValFlu(AttrNode))
    else
    //
    AvisaItemDesconhecido(Source, G, AttrNode.NodeName);
  end;
  // Subn�s
  if XMLNoSorc.HasChildNodes then
  begin
    for I := 0 to XMLNoSorc.ChildNodes.Count - 1 do
    begin
      XMLNohSubNivel := XMLNoSorc.ChildNodes.Nodes[I];
      if (XMLNohSubNivel.NodeType = ntElement) then
      begin
        Campo := Lowercase(XMLNohSubNivel.NodeName);
        if Campo = LowerCase('InfNfse') then
          ImportaGrupo_InfNfse(Source, XMLNohSubNivel, TipoDoc)
        else


        {
        if Campo = LowerCase('?) then
          AddSQLItem(TipoDoc, Campo, dmkPF.XMLVal?(AttrNode))
        else
        if Campo = LowerCase('?) then
          ImportaGrupo_?(XMLNohSubNivel)
        else
        }
          AvisaItemDesconhecido(Source, G, XMLNohSubNivel.NodeName);
      end;
    end;
  end;

  //
  // ini 2023-12-22
  // desabilitei aqui pois vai executar depois na procedure "Le_CompNfse(..."
  //SQLInsUpd_Nfse();
  //FTag_Pre := Tag_Antes;
  // ini 2023-12-22
end;

procedure TNFSeABRASF_0201.ImportaGrupo_NfseCancelamento(Source: String;
  XMLNoSorc: IXMLNode; TipoDoc: TipoDeArraySQL);
const
  G = 'NfseCancelamento';
  Tag = 'Canc_';
var
  I: Integer;
  AttrNode, XMLNohSubNivel: IXMLNode;
  Campo: String;
  //
  Tag_Antes: String;
begin
  //SetZeroArrs(Fld_RPS, Val_RPS);
////////////////////////////////////////////////////////////////////////////////
  SetZeroArrs(Fld_Nfse, Val_Nfse);
////////////////////////////////////////////////////////////////////////////////
  Tag_Antes := FTag_Pre;
  FTag_Pre := 'Can'; // 'NFS'
////////////////////////////////////////////////////////////////////////////////
  FCan_XML := XMLNoSorc.XML;
////////////////////////////////////////////////////////////////////////////////

  //? SetZeroArrs(Fld_? > Nfse, Val_? > Nfse);
  if not (XMLNoSorc.NodeType = ntElement) then
    Exit;
  if XMLNoSorc.IsTextElement then
  begin
    Geral.MB_Aviso('Elemento de texto desconhecido no Grupo ' + G + ':' +
    #13#10 + XMLNoSorc.Text);
  end;
  for I := 0 to XMLNoSorc.AttributeNodes.Count - 1 do
  begin
    AttrNode := XMLNoSorc.AttributeNodes.Nodes[I];
    Campo := Lowercase(AttrNode.NodeName);
    if Campo = LowerCase('versao') then
      AddSQLItem(TipoDoc, Tag + Campo, dmkPF.XMLValTxt(AttrNode))
    else
    //
    AvisaItemDesconhecido(Source, G, AttrNode.NodeName);
  end;
  // Subn�s
  if XMLNoSorc.HasChildNodes then
  begin
    for I := 0 to XMLNoSorc.ChildNodes.Count - 1 do
    begin
      XMLNohSubNivel := XMLNoSorc.ChildNodes.Nodes[I];
      if (XMLNohSubNivel.NodeType = ntElement) then
      begin
        Campo := Lowercase(XMLNohSubNivel.NodeName);
(*
        if Campo = LowerCase('CodigoMunicipio') then
          AddSQLItem(TipoDoc, 'OrgaoGeradorCodigoMunicipio', dmkPF.XMLValInt(XMLNohSubNivel))
        else
        if Campo = LowerCase('Uf') then
          AddSQLItem(TipoDoc, 'OrgaoGeradorUf', dmkPF.XMLValTxt(XMLNohSubNivel))
        else
*)
        if Campo = LowerCase('Confirmacao') then
          ImportaGrupo_Confirmacao(Source, XMLNohSubNivel, TipoDoc)
        else
          AvisaItemDesconhecido(Source, G, XMLNohSubNivel.NodeName);
      end;
    end;
  end;

  //

  SQLInsUpd_Can();
  FTag_Pre := Tag_Antes;
end;

procedure TNFSeABRASF_0201.ImportaGrupo_OrgaoGerador(Source: String;
  XMLNoSorc: IXMLNode; TipoDoc: TipoDeArraySQL);
const
  G = 'OrgaoGerador';
var
  I: Integer;
  AttrNode, XMLNohSubNivel: IXMLNode;
  Campo: String;
begin
  //? SetZeroArrs(Fld_? > Nfse, Val_? > Nfse);
  if not (XMLNoSorc.NodeType = ntElement) then
    Exit;
  if XMLNoSorc.IsTextElement then
  begin
    Geral.MB_Aviso('Elemento de texto desconhecido no Grupo ' + G + ':' +
    #13#10 + XMLNoSorc.Text);
  end;
  for I := 0 to XMLNoSorc.AttributeNodes.Count - 1 do
  begin
    AttrNode := XMLNoSorc.AttributeNodes.Nodes[I];
    Campo := Lowercase(AttrNode.NodeName);
    (*
    if Campo = LowerCase('versao') then
      AddSQLItem(TipoDoc, Campo, dmkPF.XMLVal?(AttrNode))
    else
    *)
    //
    AvisaItemDesconhecido(Source, G, AttrNode.NodeName);
  end;
  // Subn�s
  if XMLNoSorc.HasChildNodes then
  begin
    for I := 0 to XMLNoSorc.ChildNodes.Count - 1 do
    begin
      XMLNohSubNivel := XMLNoSorc.ChildNodes.Nodes[I];
      if (XMLNohSubNivel.NodeType = ntElement) then
      begin
        Campo := Lowercase(XMLNohSubNivel.NodeName);
        if Campo = LowerCase('CodigoMunicipio') then
          AddSQLItem(TipoDoc, 'OrgaoGeradorCodigoMunicipio', dmkPF.XMLValInt(XMLNohSubNivel))
        else
        if Campo = LowerCase('Uf') then
          AddSQLItem(TipoDoc, 'OrgaoGeradorUf', dmkPF.XMLValTxt(XMLNohSubNivel))
(*
        else
        if Campo = LowerCase('?) then
          ImportaGrupo_?(Source, XMLNohSubNivel)
        *)
        else
          AvisaItemDesconhecido(Source, G, XMLNohSubNivel.NodeName);
      end;
    end;
  end;
end;

procedure TNFSeABRASF_0201.ImportaGrupo_Pedido(Source: String;
  XMLNoSorc: IXMLNode; TipoDoc: TipoDeArraySQL);
const
  G = 'Pedido';
var
  I: Integer;
  AttrNode, XMLNohSubNivel: IXMLNode;
  Campo: String;
begin
  EntiTipo := '';
  //
  //? SetZeroArrs(Fld_? > Nfse, Val_? > Nfse);
  if not (XMLNoSorc.NodeType = ntElement) then
    Exit;
  if XMLNoSorc.IsTextElement then
  begin
    Geral.MB_Aviso('Elemento de texto desconhecido no Grupo ' + G + ':' +
    #13#10 + XMLNoSorc.Text);
  end;
  for I := 0 to XMLNoSorc.AttributeNodes.Count - 1 do
  begin
    AttrNode := XMLNoSorc.AttributeNodes.Nodes[I];
    Campo := Lowercase(AttrNode.NodeName);
    (*
    if Campo = LowerCase('versao') then
      AddSQLItem(TipoDoc, Campo, dmkPF.XMLVal?(AttrNode))
    else
    *)
    //
    AvisaItemDesconhecido(Source, G, AttrNode.NodeName);
  end;
  // Subn�s
  if XMLNoSorc.HasChildNodes then
  begin
    for I := 0 to XMLNoSorc.ChildNodes.Count - 1 do
    begin
      XMLNohSubNivel := XMLNoSorc.ChildNodes.Nodes[I];
      if (XMLNohSubNivel.NodeType = ntElement) then
      begin
        Campo := Lowercase(XMLNohSubNivel.NodeName);
        (*
        if Campo = LowerCase('InscricaoMunicipal') then
          AddSQLItem(TipoDoc, EntiTipo + Campo, dmkPF.XMLValTxt(XMLNohSubNivel))
        else
        *)
        //
        if Campo = LowerCase('InfPedidoCancelamento') then
          ImportaGrupo_InfPedidoCancelamento(Source, XMLNohSubNivel, TipoDoc)
        else
        if Campo = LowerCase('Signature') then
          // nada. Assinatura
        else
          AvisaItemDesconhecido(Source, G, XMLNohSubNivel.NodeName);
      end;
    end;
  end;
end;

procedure TNFSeABRASF_0201.ImportaGrupo_Prestador(Source: String;
  XMLNoSorc: IXMLNode; TipoDoc: TipoDeArraySQL);
const
  G = 'Prestador';
var
  I: Integer;
  AttrNode, XMLNohSubNivel: IXMLNode;
  Campo: String;
begin
  EntiTipo := CO_Presta;
  //
  //? SetZeroArrs(Fld_? > Nfse, Val_? > Nfse);
  if not (XMLNoSorc.NodeType = ntElement) then
    Exit;
  if XMLNoSorc.IsTextElement then
  begin
    Geral.MB_Aviso('Elemento de texto desconhecido no Grupo ' + G + ':' +
    #13#10 + XMLNoSorc.Text);
  end;
  for I := 0 to XMLNoSorc.AttributeNodes.Count - 1 do
  begin
    AttrNode := XMLNoSorc.AttributeNodes.Nodes[I];
    Campo := Lowercase(AttrNode.NodeName);
    (*
    if Campo = LowerCase('versao') then
      AddSQLItem(TipoDoc, Campo, dmkPF.XMLVal?(AttrNode))
    else
    *)
    //
    AvisaItemDesconhecido(Source, G, AttrNode.NodeName);
  end;
  // Subn�s
  if XMLNoSorc.HasChildNodes then
  begin
    for I := 0 to XMLNoSorc.ChildNodes.Count - 1 do
    begin
      XMLNohSubNivel := XMLNoSorc.ChildNodes.Nodes[I];
      if (XMLNohSubNivel.NodeType = ntElement) then
      begin
        Campo := Lowercase(XMLNohSubNivel.NodeName);
        if Campo = LowerCase('InscricaoMunicipal') then
          AddSQLItem(TipoDoc, EntiTipo + Campo, dmkPF.XMLValTxt(XMLNohSubNivel))
        else
        //
        if Campo = LowerCase('CpfCnpj') then
          ImportaGrupo_CpfCnpj(Source, XMLNohSubNivel, TipoDoc)
        else
          AvisaItemDesconhecido(Source, G, XMLNohSubNivel.NodeName);
      end;
    end;
  end;
end;

procedure TNFSeABRASF_0201.ImportaGrupo_PrestadorServico(Source: String;
  XMLNoSorc: IXMLNode; TipoDoc: TipoDeArraySQL);
const
  G = 'PrestadorServico';
var
  I: Integer;
  AttrNode, XMLNohSubNivel: IXMLNode;
  Campo: String;
begin
  EntiTipo := CO_Presta;
  //? SetZeroArrs(Fld_? > Nfse, Val_? > Nfse);
  if not (XMLNoSorc.NodeType = ntElement) then
    Exit;
  if XMLNoSorc.IsTextElement then
  begin
    Geral.MB_Aviso('Elemento de texto desconhecido no Grupo ' + G + ':' +
    #13#10 + XMLNoSorc.Text);
  end;
  for I := 0 to XMLNoSorc.AttributeNodes.Count - 1 do
  begin
    AttrNode := XMLNoSorc.AttributeNodes.Nodes[I];
    Campo := Lowercase(AttrNode.NodeName);
    (*
    if Campo = LowerCase('versao') then
      AddSQLItem(TipoDoc, Campo, dmkPF.XMLVal?(AttrNode))
    else
    *)
    //
    AvisaItemDesconhecido(Source, G, AttrNode.NodeName);
  end;
  // Subn�s
  if XMLNoSorc.HasChildNodes then
  begin
    for I := 0 to XMLNoSorc.ChildNodes.Count - 1 do
    begin
      XMLNohSubNivel := XMLNoSorc.ChildNodes.Nodes[I];
      if (XMLNohSubNivel.NodeType = ntElement) then
      begin
        Campo := Lowercase(XMLNohSubNivel.NodeName);
        if Campo = LowerCase('RazaoSocial') then
          AddSQLItem(TipoDoc, EntiTipo + Campo, dmkPF.XMLValTxt(XMLNohSubNivel))
        else
        if Campo = LowerCase('NomeFantasia') then
          AddSQLItem(TipoDoc, EntiTipo + Campo, dmkPF.XMLValTxt(XMLNohSubNivel))
        else
        if Campo = LowerCase('IdentificacaoPrestador') then
          ImportaGrupo_IdentificacaoPrestador(Source, XMLNohSubNivel, TipoDoc)
        else
        if Campo = LowerCase('Endereco') then
          ImportaGrupo_Endereco(Source, XMLNohSubNivel, TipoDoc)
        else
        if Campo = LowerCase('Contato') then
          ImportaGrupo_Contato(Source, XMLNohSubNivel, TipoDoc)
        else
          AvisaItemDesconhecido(Source, G, XMLNohSubNivel.NodeName);
      end;
    end;
  end;
  EntiTipo := '';
end;

procedure TNFSeABRASF_0201.ImportaGrupo_RetCancelamento(Source: String;
  XMLNoSorc: IXMLNode; TipoDoc: TipoDeArraySQL);
const
  G = 'RetCancelamento';
var
  I: Integer;
  AttrNode, XMLNohSubNivel: IXMLNode;
  Campo: String;
begin
  //SetZeroArrs(Fld_Rps, Val_Rps);
  if not (XMLNoSorc.NodeType = ntElement) then
    Exit;
  if XMLNoSorc.IsTextElement then
  begin
    Geral.MB_Aviso('Elemento de texto desconhecido no Grupo ' + G + ':' +
    #13#10 + XMLNoSorc.Text);
  end;
  for I := 0 to XMLNoSorc.AttributeNodes.Count - 1 do
  begin
    AttrNode := XMLNoSorc.AttributeNodes.Nodes[I];
    Campo := Lowercase(AttrNode.NodeName);
    (*
    if Campo = LowerCase('Id') then
      AddSQLItem(TipoDoc, Campo, dmkPF.XMLValTxt(AttrNode))
    else
    *)
    //
    AvisaItemDesconhecido(Source, G, AttrNode.NodeName);
  end;
  // Subn�s
  if XMLNoSorc.HasChildNodes then
  begin
    for I := 0 to XMLNoSorc.ChildNodes.Count - 1 do
    begin
      XMLNohSubNivel := XMLNoSorc.ChildNodes.Nodes[I];
      if (XMLNohSubNivel.NodeType = ntElement) then
      begin
        Campo := Lowercase(XMLNohSubNivel.NodeName);
        (*
        if Campo = LowerCase('??') then
          AddSQLItem(TipoDoc, Campo, dmkPF.XMLValDth(XMLNohSubNivel))
        else
        *)
        //
        if Campo = LowerCase('NfseCancelamento') then
          ImportaGrupo_NfseCancelamento(Source, XMLNohSubNivel, TipoDoc)
        else
          AvisaItemDesconhecido(Source, G, XMLNohSubNivel.NodeName);
      end;
    end;
  end;
end;

procedure TNFSeABRASF_0201.ImportaGrupo_Rps(Source: String;
  XMLNoSorc: IXMLNode; TipoDoc: TipoDeArraySQL);
const
  G = 'Rps';
var
  I: Integer;
  AttrNode, XMLNohSubNivel: IXMLNode;
  Campo: String;
begin
  //SetZeroArrs(Fld_Rps, Val_Rps);
  if not (XMLNoSorc.NodeType = ntElement) then
    Exit;
  if XMLNoSorc.IsTextElement then
  begin
    Geral.MB_Aviso('Elemento de texto desconhecido no Grupo ' + G + ':' +
    #13#10 + XMLNoSorc.Text);
  end;
  for I := 0 to XMLNoSorc.AttributeNodes.Count - 1 do
  begin
    AttrNode := XMLNoSorc.AttributeNodes.Nodes[I];
    Campo := Lowercase(AttrNode.NodeName);
    if Campo = LowerCase('Id') then
      AddSQLItem(TipoDoc, Campo, dmkPF.XMLValTxt(AttrNode))
    else
    //
    AvisaItemDesconhecido(Source, G, AttrNode.NodeName);
  end;
  // Subn�s
  if XMLNoSorc.HasChildNodes then
  begin
    for I := 0 to XMLNoSorc.ChildNodes.Count - 1 do
    begin
      XMLNohSubNivel := XMLNoSorc.ChildNodes.Nodes[I];
      if (XMLNohSubNivel.NodeType = ntElement) then
      begin
        Campo := Lowercase(XMLNohSubNivel.NodeName);
        if Campo = LowerCase('DataEmissao') then
          AddSQLItem(TipoDoc, G + Campo, dmkPF.XMLValDth(XMLNohSubNivel))
        else
        if Campo = LowerCase('Status') then
          AddSQLItem(TipoDoc, G + Campo, dmkPF.XMLValInt(XMLNohSubNivel))
        //
        else
        if Campo = LowerCase('IdentificacaoRps') then
          ImportaGrupo_IdentificacaoRps(Source, XMLNohSubNivel, TipoDoc)
        else
          AvisaItemDesconhecido(Source, G, XMLNohSubNivel.NodeName);
      end;
    end;
  end;
end;

procedure TNFSeABRASF_0201.ImportaGrupo_Servico(Source: String;
  XMLNoSorc: IXMLNode; TipoDoc: TipoDeArraySQL);
const
  G = 'Servico';
var
  I: Integer;
  AttrNode, XMLNohSubNivel: IXMLNode;
  Campo: String;
begin
  //? SetZeroArrs(Fld_? > Nfse, Val_? > Nfse);
  if not (XMLNoSorc.NodeType = ntElement) then
    Exit;
  if XMLNoSorc.IsTextElement then
  begin
    Geral.MB_Aviso('Elemento de texto desconhecido no Grupo ' + G + ':' +
    #13#10 + XMLNoSorc.Text);
  end;
  for I := 0 to XMLNoSorc.AttributeNodes.Count - 1 do
  begin
    AttrNode := XMLNoSorc.AttributeNodes.Nodes[I];
    Campo := Lowercase(AttrNode.NodeName);
    (*
    if Campo = LowerCase('versao') then
      AddSQLItem(TipoDoc, Campo, dmkPF.XMLVal?(AttrNode))
    else
    *)
    //
    AvisaItemDesconhecido(Source, G, AttrNode.NodeName);
  end;
  // Subn�s
  if XMLNoSorc.HasChildNodes then
  begin
    for I := 0 to XMLNoSorc.ChildNodes.Count - 1 do
    begin
      XMLNohSubNivel := XMLNoSorc.ChildNodes.Nodes[I];
      if (XMLNohSubNivel.NodeType = ntElement) then
      begin
        Campo := Lowercase(XMLNohSubNivel.NodeName);
        if Campo = LowerCase('IssRetido') then
          AddSQLItem(TipoDoc, Campo, dmkPF.XMLValInt(XMLNohSubNivel))
        else
        if Campo = LowerCase('ResponsavelRetencao') then
          AddSQLItem(TipoDoc, Campo, dmkPF.XMLValInt(XMLNohSubNivel))
        else
        if Campo = LowerCase('ItemListaServico') then
          AddSQLItem(TipoDoc, Campo, dmkPF.XMLValTxt(XMLNohSubNivel))
        else
        if Campo = LowerCase('CodigoCnae') then
          AddSQLItem(TipoDoc, Campo, dmkPF.XMLValInt(XMLNohSubNivel))
        else
        if Campo = LowerCase('CodigoTributacaoMunicipio') then
          AddSQLItem(TipoDoc, Campo, dmkPF.XMLValInt(XMLNohSubNivel))
        else
        if Campo = LowerCase('Discriminacao') then
          AddSQLItem(TipoDoc, Campo, dmkPF.XMLValTxt(XMLNohSubNivel))
        else
        if Campo = LowerCase('CodigoMunicipio') then
          AddSQLItem(TipoDoc, Campo, dmkPF.XMLValInt(XMLNohSubNivel))
        else
        if Campo = LowerCase('CodigoPais') then
          AddSQLItem(TipoDoc, Campo, dmkPF.XMLValTxt(XMLNohSubNivel))
        else
        if Campo = LowerCase('ExigibilidadeISS') then
          AddSQLItem(TipoDoc, Campo, dmkPF.XMLValInt(XMLNohSubNivel))
        else
        if Campo = LowerCase('MunicipioIncidencia') then
          AddSQLItem(TipoDoc, Campo, dmkPF.XMLValInt(XMLNohSubNivel))
        else
        if Campo = LowerCase('NumeroProcesso') then
          AddSQLItem(TipoDoc, Campo, dmkPF.XMLValTxt(XMLNohSubNivel))
        else
        //
        if Campo = LowerCase('Valores') then
          ImportaGrupo_Valores(Source, XMLNohSubNivel, TipoDoc)
        else
          AvisaItemDesconhecido(Source, G, XMLNohSubNivel.NodeName);
      end;
    end;
  end;
end;

procedure TNFSeABRASF_0201.ImportaGrupo_Tomador(Source: String;
  XMLNoSorc: IXMLNode; TipoDoc: TipoDeArraySQL);
const
  G = 'Tomador';
var
  I: Integer;
  AttrNode, XMLNohSubNivel: IXMLNode;
  Campo: String;
begin
  EntiTipo := CO_Toma;
  //
  //? SetZeroArrs(Fld_? > Nfse, Val_? > Nfse);
  if not (XMLNoSorc.NodeType = ntElement) then
    Exit;
  if XMLNoSorc.IsTextElement then
  begin
    Geral.MB_Aviso('Elemento de texto desconhecido no Grupo ' + G + ':' +
    #13#10 + XMLNoSorc.Text);
  end;
  for I := 0 to XMLNoSorc.AttributeNodes.Count - 1 do
  begin
    AttrNode := XMLNoSorc.AttributeNodes.Nodes[I];
    Campo := Lowercase(AttrNode.NodeName);
    (*
    if Campo = LowerCase('versao') then
      AddSQLItem(TipoDoc, Campo, dmkPF.XMLVal?(AttrNode))
    else
    *)
    //
    AvisaItemDesconhecido(Source, G, AttrNode.NodeName);
  end;
  // Subn�s
  if XMLNoSorc.HasChildNodes then
  begin
    for I := 0 to XMLNoSorc.ChildNodes.Count - 1 do
    begin
      XMLNohSubNivel := XMLNoSorc.ChildNodes.Nodes[I];
      if (XMLNohSubNivel.NodeType = ntElement) then
      begin
        Campo := Lowercase(XMLNohSubNivel.NodeName);
        if Campo = LowerCase('RazaoSocial') then
          AddSQLItem(TipoDoc, EntiTipo + Campo, dmkPF.XMLValTxt(XMLNohSubNivel))
        else
        if Campo = LowerCase('NomeFantasia') then
          AddSQLItem(TipoDoc, EntiTipo + Campo, dmkPF.XMLValTxt(XMLNohSubNivel))
        else
        if Campo = LowerCase('IdentificacaoTomador') then
          ImportaGrupo_IdentificacaoTomador(Source, XMLNohSubNivel, TipoDoc)
        else
        if Campo = LowerCase('Endereco') then
          ImportaGrupo_Endereco(Source, XMLNohSubNivel, TipoDoc)
        else
        if Campo = LowerCase('Contato') then
          ImportaGrupo_Contato(Source, XMLNohSubNivel, TipoDoc)
        else
          AvisaItemDesconhecido(Source, G, XMLNohSubNivel.NodeName);
      end;
    end;
  end;
end;

procedure TNFSeABRASF_0201.ImportaGrupo_Valores(Source: String;
  XMLNoSorc: IXMLNode; TipoDoc: TipoDeArraySQL);
const
  G = 'Valores';
var
  I: Integer;
  AttrNode, XMLNohSubNivel: IXMLNode;
  Campo: String;
begin
  //? SetZeroArrs(Fld_? > Nfse, Val_? > Nfse);
  if not (XMLNoSorc.NodeType = ntElement) then
    Exit;
  if XMLNoSorc.IsTextElement then
  begin
    Geral.MB_Aviso('Elemento de texto desconhecido no Grupo ' + G + ':' +
    #13#10 + XMLNoSorc.Text);
  end;
  for I := 0 to XMLNoSorc.AttributeNodes.Count - 1 do
  begin
    AttrNode := XMLNoSorc.AttributeNodes.Nodes[I];
    Campo := Lowercase(AttrNode.NodeName);
    (*
    if Campo = LowerCase('versao') then
      AddSQLItem(TipoDoc, Campo, dmkPF.XMLVal?(AttrNode))
    else
    *)
    //
    AvisaItemDesconhecido(Source, G, AttrNode.NodeName);
  end;
  // Subn�s
  if XMLNoSorc.HasChildNodes then
  begin
    for I := 0 to XMLNoSorc.ChildNodes.Count - 1 do
    begin
      XMLNohSubNivel := XMLNoSorc.ChildNodes.Nodes[I];
      if (XMLNohSubNivel.NodeType = ntElement) then
      begin
        Campo := Lowercase(XMLNohSubNivel.NodeName);
        if Campo = LowerCase('ValorServicos') then
          AddSQLItem(TipoDoc, Campo, dmkPF.XMLValFlu(XMLNohSubNivel))
        else
        if Campo = LowerCase('ValorDeducoes') then
          AddSQLItem(TipoDoc, Campo, dmkPF.XMLValFlu(XMLNohSubNivel))
        else
        if Campo = LowerCase('ValorPis') then
          AddSQLItem(TipoDoc, Campo, dmkPF.XMLValFlu(XMLNohSubNivel))
        else
        if Campo = LowerCase('ValorCofins') then
          AddSQLItem(TipoDoc, Campo, dmkPF.XMLValFlu(XMLNohSubNivel))
        else
        if Campo = LowerCase('ValorInss') then
          AddSQLItem(TipoDoc, Campo, dmkPF.XMLValFlu(XMLNohSubNivel))
        else
        if Campo = LowerCase('ValorIr') then
          AddSQLItem(TipoDoc, Campo, dmkPF.XMLValFlu(XMLNohSubNivel))
        else
        if Campo = LowerCase('ValorCsll') then
          AddSQLItem(TipoDoc, Campo, dmkPF.XMLValFlu(XMLNohSubNivel))
        else
        if Campo = LowerCase('OutrasRetencoes') then
          AddSQLItem(TipoDoc, Campo, dmkPF.XMLValFlu(XMLNohSubNivel))
        else
        if Campo = LowerCase('ValorIss') then
          AddSQLItem(TipoDoc, Campo, dmkPF.XMLValFlu(XMLNohSubNivel))
        else
        if Campo = LowerCase('Aliquota') then
          AddSQLItem(TipoDoc, Campo, dmkPF.XMLValFlu(XMLNohSubNivel))
        else
        if Campo = LowerCase('DescontoIncondicionado') then
          AddSQLItem(TipoDoc, Campo, dmkPF.XMLValFlu(XMLNohSubNivel))
        else
        if Campo = LowerCase('DescontoCondicionado') then
          AddSQLItem(TipoDoc, Campo, dmkPF.XMLValFlu(XMLNohSubNivel))
        else
        (*
        if Campo = LowerCase('?) then
          ImportaGrupo_?(Source, XMLNohSubNivel)
        else
        *)
          AvisaItemDesconhecido(Source, G, XMLNohSubNivel.NodeName);
      end;
    end;
  end;
end;

procedure TNFSeABRASF_0201.ImportaGrupo_ValoresNfse(Source: String; XMLNoSorc: IXMLNode; TipoDoc: TipoDeArraySQL);
const
  G = 'ValoresNfse';
var
  I: Integer;
  AttrNode, XMLNohSubNivel: IXMLNode;
  Campo: String;
begin
  if not (XMLNoSorc.NodeType = ntElement) then
    Exit;
  if XMLNoSorc.IsTextElement then
  begin
    Geral.MB_Aviso('Elemento de texto desconhecido no Grupo ' + G + ':' +
    #13#10 + XMLNoSorc.Text);
  end;
  for I := 0 to XMLNoSorc.AttributeNodes.Count - 1 do
  begin
    AttrNode := XMLNoSorc.AttributeNodes.Nodes[I];
    Campo := Lowercase(AttrNode.NodeName);
    {
    if Campo = LowerCase('versao') then
      AddSQLItem(TipoDoc, Campo, dmkPF.XMLVal?(AttrNode))
    else
    }
    //
    AvisaItemDesconhecido(Source, G, AttrNode.NodeName);
  end;
  // Subn�s
  if XMLNoSorc.HasChildNodes then
  begin
    for I := 0 to XMLNoSorc.ChildNodes.Count - 1 do
    begin
      XMLNohSubNivel := XMLNoSorc.ChildNodes.Nodes[I];
      if (XMLNohSubNivel.NodeType = ntElement) then
      begin
        Campo := Lowercase(XMLNohSubNivel.NodeName);
        if Campo = LowerCase('BaseCalculo') then
          AddSQLItem(TipoDoc, Campo, dmkPF.XMLValFlu(XMLNohSubNivel))
        else
        if Campo = LowerCase('Aliquota') then
          AddSQLItem(TipoDoc, Campo, dmkPF.XMLValFlu(XMLNohSubNivel))
        else
        if Campo = LowerCase('ValorIss') then
          AddSQLItem(TipoDoc, Campo, dmkPF.XMLValFlu(XMLNohSubNivel))
        else
        if Campo = LowerCase('ValorLiquidoNfse') then
          AddSQLItem(TipoDoc, Campo, dmkPF.XMLValFlu(XMLNohSubNivel))
{
        else
        if Campo = LowerCase('?') then
          ImportaGrupo_?(Source, XMLNohSubNivel)
}
        else
          AvisaItemDesconhecido(Source, G, XMLNohSubNivel.NodeName);
      end;
    end;
  end;
end;

end.
