object FmNFSeFatAdd: TFmNFSeFatAdd
  Left = 339
  Top = 185
  Caption = 'NFS-FATUR-003 :: Adi'#231#227'o de Itens de Faturamento de Servi'#231'os'
  ClientHeight = 492
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 912
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 539
        Height = 32
        Caption = 'Adi'#231#227'o de Itens de Faturamento de Servi'#231'os'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 539
        Height = 32
        Caption = 'Adi'#231#227'o de Itens de Faturamento de Servi'#231'os'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 539
        Height = 32
        Caption = 'Adi'#231#227'o de Itens de Faturamento de Servi'#231'os'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 1008
    Height = 330
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 1008
      Height = 330
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 1008
        Height = 330
        Align = alClient
        TabOrder = 0
        object LaTotal: TLabel
          Left = 2
          Top = 315
          Width = 1004
          Height = 13
          Align = alBottom
          ExplicitWidth = 3
        end
        object DBGrid1: TDBGrid
          Left = 2
          Top = 32
          Width = 1004
          Height = 283
          Align = alClient
          DataSource = DsNFSeMenCab
          TabOrder = 0
          TitleFont.Charset = ANSI_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          OnCellClick = DBGrid1CellClick
          OnDrawColumnCell = DBGrid1DrawColumnCell
          Columns = <
            item
              Expanded = False
              FieldName = 'Ativo'
              Title.Caption = 'Ok'
              Width = 17
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Codigo'
              Title.Caption = 'C'#243'digo'
              Width = 44
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Cliente'
              Width = 44
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_CLI'
              Title.Caption = 'Nome cliente (Tomador)'
              Width = 200
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Contrato'
              Width = 48
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_SRV'
              Title.Caption = 'Regra Fiscal'
              Width = 200
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'TXT_GeraLct'
              Title.Caption = 'Lct?'
              Width = 32
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'TXT_GeraBol'
              Title.Caption = 'Bol?'
              Width = 32
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'GLCarteira'
              Title.Caption = 'Carteira'
              Width = 40
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'GLConta'
              Title.Caption = 'Conta'
              Width = 40
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'IncreCompet'
              Title.Caption = 'IC'
              Width = 20
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DiaFat'
              Title.Caption = 'Dia'
              Width = 28
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Valor'
              Width = 72
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'TXT_VIGEN_INI'
              Title.Caption = 'Dt vig'#234'n.ini'
              Width = 60
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'TXT_VIGEN_FIM'
              Title.Caption = 'Dt vig'#234'n.fim'
              Width = 60
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_INT'
              Title.Caption = 'Intermedi'#225'rio'
              Width = 120
              Visible = True
            end>
        end
        object PB1: TProgressBar
          Left = 2
          Top = 15
          Width = 1004
          Height = 17
          Align = alTop
          TabOrder = 1
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 378
    Width = 1008
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 422
    Width = 1008
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 862
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 860
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
      object BtTodos: TBitBtn
        Tag = 127
        Left = 217
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Hint = 'Sai da janela atual'
        Caption = '&Todos'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        OnClick = BtTodosClick
      end
      object BtNenhum: TBitBtn
        Tag = 128
        Left = 341
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Hint = 'Sai da janela atual'
        Caption = '&Nenhum'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 2
        OnClick = BtNenhumClick
      end
    end
  end
  object QrNFSeMenCab: TmySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrNFSeMenCabBeforeClose
    AfterScroll = QrNFSeMenCabAfterScroll
    OnCalcFields = QrNFSeMenCabCalcFields
    SQL.Strings = (
      'SELECT nmc.*, nsc.Nome NO_SRV, '
      'car.Nome NO_CRT, cta.Nome NO_CTA,'
      'IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NO_CLI,'
      'IF(ntm.Tipo=0, ntm.RazaoSocial, ntm.Nome) NO_INT'
      'FROM nfsemencab nmc'
      'LEFT JOIN entidades cli ON cli.Codigo=nmc.Cliente '
      'LEFT JOIN entidades ntm ON ntm.Codigo=nmc.Intermed'
      'LEFT JOIN nfsesrvcad nsc ON nsc.Codigo=nmc.NFSeSrvCad'
      'LEFT JOIN carteiras car ON car.Codigo=nmc.GLCarteira'
      'LEFT JOIN contas cta ON cta.Codigo=nmc.GLConta')
    Left = 152
    Top = 120
    object QrNFSeMenCabCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'nfsemencab.Codigo'
    end
    object QrNFSeMenCabEmpresa: TIntegerField
      FieldName = 'Empresa'
      Origin = 'nfsemencab.Empresa'
    end
    object QrNFSeMenCabCliente: TIntegerField
      FieldName = 'Cliente'
      Origin = 'nfsemencab.Cliente'
    end
    object QrNFSeMenCabContrato: TIntegerField
      FieldName = 'Contrato'
      Origin = 'nfsemencab.Contrato'
    end
    object QrNFSeMenCabIntermed: TIntegerField
      FieldName = 'Intermed'
      Origin = 'nfsemencab.Intermed'
    end
    object QrNFSeMenCabNFSeSrvCad: TIntegerField
      FieldName = 'NFSeSrvCad'
      Origin = 'nfsemencab.NFSeSrvCad'
    end
    object QrNFSeMenCabDiaFat: TSmallintField
      FieldName = 'DiaFat'
      Origin = 'nfsemencab.DiaFat'
    end
    object QrNFSeMenCabIncreCompet: TSmallintField
      FieldName = 'IncreCompet'
      Origin = 'nfsemencab.IncreCompet'
    end
    object QrNFSeMenCabValor: TFloatField
      FieldName = 'Valor'
      Origin = 'nfsemencab.Valor'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrNFSeMenCabGLGerarLct: TSmallintField
      FieldName = 'GLGerarLct'
      Origin = 'nfsemencab.GLGerarLct'
    end
    object QrNFSeMenCabGLCarteira: TIntegerField
      FieldName = 'GLCarteira'
      Origin = 'nfsemencab.GLCarteira'
    end
    object QrNFSeMenCabGLConta: TIntegerField
      FieldName = 'GLConta'
      Origin = 'nfsemencab.GLConta'
    end
    object QrNFSeMenCabVigenDtIni: TDateField
      FieldName = 'VigenDtIni'
      Origin = 'nfsemencab.VigenDtIni'
    end
    object QrNFSeMenCabVigenDtFim: TDateField
      FieldName = 'VigenDtFim'
      Origin = 'nfsemencab.VigenDtFim'
    end
    object QrNFSeMenCabAtivo: TSmallintField
      FieldName = 'Ativo'
      Origin = 'nfsemencab.Ativo'
      MaxValue = 1
    end
    object QrNFSeMenCabNO_SRV: TWideStringField
      FieldName = 'NO_SRV'
      Origin = 'nfsesrvcad.Nome'
      Size = 255
    end
    object QrNFSeMenCabNO_CRT: TWideStringField
      FieldName = 'NO_CRT'
      Origin = 'carteiras.Nome'
      Size = 100
    end
    object QrNFSeMenCabNO_CTA: TWideStringField
      FieldName = 'NO_CTA'
      Origin = 'contas.Nome'
      Size = 50
    end
    object QrNFSeMenCabNO_CLI: TWideStringField
      FieldName = 'NO_CLI'
      Size = 100
    end
    object QrNFSeMenCabNO_INT: TWideStringField
      FieldName = 'NO_INT'
      Size = 100
    end
    object QrNFSeMenCabTXT_VIGEN_INI: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TXT_VIGEN_INI'
      Size = 10
      Calculated = True
    end
    object QrNFSeMenCabTXT_VIGEN_FIM: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TXT_VIGEN_FIM'
      Size = 10
      Calculated = True
    end
    object QrNFSeMenCabGBGerarBol: TSmallintField
      FieldName = 'GBGerarBol'
      Origin = 'nfsemencab.GBGerarBol'
    end
    object QrNFSeMenCabTXT_GeraLct: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TXT_GeraLct'
      Size = 3
      Calculated = True
    end
    object QrNFSeMenCabTXT_GeraBol: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TXT_GeraBol'
      Size = 3
      Calculated = True
    end
  end
  object DsNFSeMenCab: TDataSource
    DataSet = QrNFSeMenCab
    Left = 152
    Top = 168
  end
  object QrNFSeSrvCad: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *'
      'FROM nfsesrvcad')
    Left = 244
    Top = 120
    object QrNFSeSrvCadCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrNFSeSrvCadNome: TWideStringField
      FieldName = 'Nome'
      Size = 255
    end
    object QrNFSeSrvCadCodigoTributacaoMunicipio: TWideStringField
      FieldName = 'CodigoTributacaoMunicipio'
    end
    object QrNFSeSrvCadResponsavelRetencao: TSmallintField
      FieldName = 'ResponsavelRetencao'
    end
    object QrNFSeSrvCadItemListaServico: TWideStringField
      FieldName = 'ItemListaServico'
      Size = 5
    end
    object QrNFSeSrvCadCodigoCnae: TWideStringField
      FieldName = 'CodigoCnae'
    end
    object QrNFSeSrvCadNumeroProcesso: TWideStringField
      FieldName = 'NumeroProcesso'
      Size = 30
    end
    object QrNFSeSrvCadExigibilidadeIss: TSmallintField
      FieldName = 'ExigibilidadeIss'
    end
    object QrNFSeSrvCadDiscriminacao: TWideMemoField
      FieldName = 'Discriminacao'
      Required = True
      BlobType = ftWideMemo
      Size = 4
    end
    object QrNFSeSrvCadRegimeEspecialTributacao: TSmallintField
      FieldName = 'RegimeEspecialTributacao'
    end
    object QrNFSeSrvCadOptanteSimplesNacional: TSmallintField
      FieldName = 'OptanteSimplesNacional'
    end
    object QrNFSeSrvCadIncentivoFiscal: TSmallintField
      FieldName = 'IncentivoFiscal'
    end
    object QrNFSeSrvCadRpsTipo: TSmallintField
      FieldName = 'RpsTipo'
    end
    object QrNFSeSrvCadQuemPagaISS: TSmallintField
      FieldName = 'QuemPagaISS'
    end
    object QrNFSeSrvCadAliquota: TFloatField
      FieldName = 'Aliquota'
    end
    object QrNFSeSrvCadNaturezaOperacao: TIntegerField
      FieldName = 'NaturezaOperacao'
    end
    object QrNFSeSrvCadAlqPIS: TFloatField
      FieldName = 'AlqPIS'
    end
    object QrNFSeSrvCadAlqCOFINS: TFloatField
      FieldName = 'AlqCOFINS'
    end
    object QrNFSeSrvCadAlqINSS: TFloatField
      FieldName = 'AlqINSS'
    end
    object QrNFSeSrvCadAlqIR: TFloatField
      FieldName = 'AlqIR'
    end
    object QrNFSeSrvCadAlqCSLL: TFloatField
      FieldName = 'AlqCSLL'
    end
    object QrNFSeSrvCadAlqOutrasRetencoes: TFloatField
      FieldName = 'AlqOutrasRetencoes'
    end
    object QrNFSeSrvCadPDFCompres: TSmallintField
      FieldName = 'PDFCompres'
    end
    object QrNFSeSrvCadPDFEmbFont: TSmallintField
      FieldName = 'PDFEmbFont'
    end
    object QrNFSeSrvCadPDFPrnOptm: TSmallintField
      FieldName = 'PDFPrnOptm'
    end
    object QrNFSeSrvCadImportado: TIntegerField
      FieldName = 'Importado'
    end
    object QrNFSeSrvCadindFinal: TIntegerField
      FieldName = 'indFinal'
    end
  end
end
