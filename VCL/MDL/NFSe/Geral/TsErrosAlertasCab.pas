unit TsErrosAlertasCab;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2, dmkGeral,
  ResIntStrings, UnMLAGeral, UnGOTOy, UnInternalConsts, UnMsgInt, UMySQLModule,
  UnInternalConsts2, mySQLDbTables, UnMySQLCuringa, dmkPermissoes, dmkEdit,
  dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup, Menus, Grids, DBGrids,
  DmkDAC_PF, UnDmkProcFunc, ComCtrls, UnDMkEnums, Variants, UnDmkWeb;

type
  TFmTsErrosAlertasCab = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    Label9: TLabel;
    EdNome: TdmkEdit;
    Label1: TLabel;
    DBEdCodigo: TdmkDBEdit;
    Label2: TLabel;
    DBEdNome: TdmkDBEdit;
    QrTsErrosAlertasCab: TmySQLQuery;
    DsTsErrosAlertasCab: TDataSource;
    QrTsErrosAlertasIts: TmySQLQuery;
    DsTsErrosAlertasIts: TDataSource;
    PMIts: TPopupMenu;
    ItsInclui1: TMenuItem;
    ItsExclui1: TMenuItem;
    ItsAltera1: TMenuItem;
    N1: TMenuItem;
    PMCab: TPopupMenu;
    CabInclui1: TMenuItem;
    CabAltera1: TMenuItem;
    CabExclui1: TMenuItem;
    BtCab: TBitBtn;
    BtIts: TBitBtn;
    DGDados: TDBGrid;
    QrTsErrosAlertasCabCodigo: TIntegerField;
    QrTsErrosAlertasCabNome: TWideStringField;
    QrTsErrosAlertasItsCodigo: TIntegerField;
    QrTsErrosAlertasItsCodTxt: TWideStringField;
    QrTsErrosAlertasItsNome: TWideStringField;
    QrTsErrosAlertasItsSolucao: TWideMemoField;
    QrTsErrosAlertasMun: TmySQLQuery;
    DsTsErrosAlertasMun: TDataSource;
    Importa1: TMenuItem;
    XT3linhasporitem1: TMenuItem;
    QrPsq: TmySQLQuery;
    QrPsqItens: TLargeintField;
    PB1: TProgressBar;
    PnData: TPanel;
    Label3: TLabel;
    DBMemo1: TDBMemo;
    Label4: TLabel;
    DBMemo2: TDBMemo;
    Itemselecionado1: TMenuItem;
    odositensdogrupo1: TMenuItem;
    BtCidades: TBitBtn;
    PMCidades: TPopupMenu;
    Adiciona1: TMenuItem;
    Remove1: TMenuItem;
    QrTsErrosAlertasMunCodigo: TIntegerField;
    QrTsErrosAlertasMunCidade: TIntegerField;
    DBGrid1: TDBGrid;
    QrTsErrosAlertasMunNO_CIDADE: TWideStringField;
    Panel6: TPanel;
    Label27: TLabel;
    SpeedButton8: TSpeedButton;
    EdArq: TdmkEdit;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrTsErrosAlertasCabAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrTsErrosAlertasCabBeforeOpen(DataSet: TDataSet);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure QrTsErrosAlertasCabAfterScroll(DataSet: TDataSet);
    procedure CabInclui1Click(Sender: TObject);
    procedure CabAltera1Click(Sender: TObject);
    procedure CabExclui1Click(Sender: TObject);
    procedure BtCabClick(Sender: TObject);
    procedure BtItsClick(Sender: TObject);
    procedure ItsInclui1Click(Sender: TObject);
    procedure ItsExclui1Click(Sender: TObject);
    procedure ItsAltera1Click(Sender: TObject);
    procedure PMCabPopup(Sender: TObject);
    procedure PMItsPopup(Sender: TObject);
    procedure XT3linhasporitem1Click(Sender: TObject);
    procedure odositensdogrupo1Click(Sender: TObject);
    procedure Itemselecionado1Click(Sender: TObject);
    procedure BtCidadesClick(Sender: TObject);
    procedure Adiciona1Click(Sender: TObject);
    procedure Remove1Click(Sender: TObject);
    procedure SpeedButton8Click(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure LocCod(Atual, Codigo: Integer);
    procedure Va(Para: TVaiPara);
    procedure MostraTsErrosAlertasIts(SQLType: TSQLType);

  public
    { Public declarations }
    FSeq, FCabIni: Integer;
    FLocIni: Boolean;
    //
    procedure ReopenTsErrosAlertasIts(CodTxt: String);
    procedure ReopenTsErrosAlertasMun(Cidade: Integer);

  end;

var
  FmTsErrosAlertasCab: TFmTsErrosAlertasCab;
const
  FFormatFloat = '00000';
  FURL = 'http://www.dermatek.com.br/Tabelas_Publicas/Codigos_erros_Maringa.txt';
  FDestFile = 'C:\Dermatek\NFSe\Tabelas\Codigos_erros_Maringa.txt';

implementation

uses UnMyObjects, ModuleGeral, MyDBCheck, TsErrosAlertasIts, SelCod;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmTsErrosAlertasCab.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmTsErrosAlertasCab.MostraTsErrosAlertasIts(SQLType: TSQLType);
begin
  if DBCheck.CriaFm(TFmTsErrosAlertasIts, FmTsErrosAlertasIts, afmoNegarComAviso) then
  begin
    FmTsErrosAlertasIts.ImgTipo.SQLType := SQLType;
    FmTsErrosAlertasIts.FDsCab := DsTsErrosAlertasCab;
    FmTsErrosAlertasIts.FQrCab := QrTsErrosAlertasCab;
    FmTsErrosAlertasIts.FQrIts := QrTsErrosAlertasIts;
    if SQLType = stIns then
      FmTsErrosAlertasIts.EdCodTxt.Enabled := True
    else
    begin
      FmTsErrosAlertasIts.EdCodTxt.Enabled := False;
      FmTsErrosAlertasIts.EdCodTxt.Text := QrTsErrosAlertasItsCodTxt.Value;
      FmTsErrosAlertasIts.MeNome.Text := QrTsErrosAlertasItsNome.Value;
      FmTsErrosAlertasIts.MeSolucao.Text := QrTsErrosAlertasItsSolucao.Value;
    end;
    FmTsErrosAlertasIts.ShowModal;
    FmTsErrosAlertasIts.Destroy;
  end;
end;

procedure TFmTsErrosAlertasCab.odositensdogrupo1Click(Sender: TObject);
begin
  if Application.MessageBox(PChar(
  'Confirma a exclus�o de TODOS itens deste grupo?'), 'Pergunta',
  MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
  begin
    Screen.Cursor := crHourGlass;
    UMyMod.ExecutaMySQLQuery1(DModG.QrAllUpd, [
    'DELETE FROM tserrosalertasits ',
    'WHERE Codigo=' + Geral.FF0(QrTsErrosAlertasCabCodigo.Value),
    '']);
    //
    ReopenTsErrosAlertasIts('');
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmTsErrosAlertasCab.PMCabPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemCabUpd(CabAltera1, QrTsErrosAlertasCab);
  MyObjects.HabilitaMenuItemCabDel(CabExclui1, QrTsErrosAlertasCab, QrTsErrosAlertasIts);
end;

procedure TFmTsErrosAlertasCab.PMItsPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(ItsInclui1, QrTsErrosAlertasCab);
  MyObjects.HabilitaMenuItemItsUpd(ItsAltera1, QrTsErrosAlertasIts);
  MyObjects.HabilitaMenuItemItsDel(ItsExclui1, QrTsErrosAlertasIts);
  //
  MyObjects.HabilitaMenuItemCabUpd(Importa1, QrTsErrosAlertasCab);
end;

procedure TFmTsErrosAlertasCab.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrTsErrosAlertasCabCodigo.Value, LaRegistro.Caption[2]);
end;

procedure TFmTsErrosAlertasCab.XT3linhasporitem1Click(Sender: TObject);
const
  IDArq = '[ERROS_NFS_3_LINHAS]';
  Titulo = 'C�digos de Erros Retorno Web Service Municipal';
  Filtro = '';
  procedure ImportaDeArquivo(Arquivo: String);
  var
    Lista: TStringList;
    Codigo, I, N, K: Integer;
    CodTxt, Nome, Solucao: String;
  begin
    Codigo := QrTsErrosAlertasCabCodigo.Value;
    Lista := TStringList.Create;
    try
      Lista.LoadFromFile(Arquivo);
      if Uppercase(Lista[0]) <> IDArq then
      begin
        Geral.MensagemBox('Identificador do arquivo difere do espoerado!' +
        #13#13 + 'ID encontrado: "' + Lista[0] + '"' + #13#10 +
        'ID esperado: "' + IDArq + '"', 'Aviso', MB_OK+MB_ICONWARNING);
        //
        Exit;
      end;
      K := Lista.Count div 3;
      PB1.Max := K;
      for I := 0 to K - 1 do
      begin
        N := ((I*3) + 1);
        CodTxt         := Lista[N];
        MyObjects.Informa2EUpdPB(PB1, LaAviso1, LaAviso2, True,
        'Verificando c�digo: "' +  CodTxt + '"');
        //
        UnDmkDAC_PF.AbreMySQLQuery0(QrPsq, DModG.AllID_DB, [
        'SELECT COUNT(CodTxt) Itens ',
        'FROM tserrosalertasits ',
        'WHERE Codigo=' + Geral.FF0(Codigo),
        'AND CodTxt="' + CodTxt + '"',
        '']);
        if  QrPsqItens.Value = 0 then
        begin
          Nome    := Lista[N + 1];
          Solucao := Lista[N + 2];
          //if
          UMyMod.SQLInsUpd(DModG.QrAllUpd, stIns, 'tserrosalertasits', False, [
          'Codigo', 'Nome', 'Solucao'], [
          'CodTxt'], [
          Codigo, Nome, Solucao], [
          CodTxt], True);
        end;
      end;
      MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
      //
      ReopenTsErrosAlertasIts('');
      //
      Screen.Cursor := crDefault;
    finally
      Screen.Cursor := crDefault;
      Lista.Free;
    end;
  end;
var
  Fonte: String;
begin
  if Lowercase(Copy(EdArq.Text, 1, 4)) = 'http' then
  begin
    Fonte := EdArq.Text;
    if FileExists(FDestFile) then DeleteFile(FDestFile);
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Aguarde... Baixando arquivo!');
    Application.ProcessMessages;
    //
    if not DirectoryExists(ExtractFileDir(FDestFile)) then
      ForceDirectories(ExtractFileDir(FDestFile));
    //
    if DmkWeb.DownloadFile(Fonte, FDestFile) then
      ImportaDeArquivo(FDestFile);
  end else
    ImportaDeArquivo(EdArq.Text);
end;

/////////////////////////////////////////////////////////////////////////////////////

procedure TFmTsErrosAlertasCab.DefParams;
begin
  VAR_GOTOTABELA := 'tserrosalertascab';
  VAR_GOTOMYSQLTABLE := QrTsErrosAlertasCab;
  VAR_GOTONEG := gotoPiZ;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := DModG.AllID_DB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT *');
  VAR_SQLx.Add('FROM tserrosalertascab');
  VAR_SQLx.Add('WHERE Codigo > 0');
  //
  VAR_SQL1.Add('AND Codigo=:P0');
  //
  //VAR_SQL2.Add('AND CodUsu=:P0');
  //
  VAR_SQLa.Add('AND Nome Like :P0');
  //
end;

procedure TFmTsErrosAlertasCab.Itemselecionado1Click(Sender: TObject);
begin
  UMyMod.ExcluiRegistroTxt1('Confirma a exclus�o do item selecionado?',
    'tsErrosAlertasIts', 'CodTxt', QrTsErrosAlertasItsCodTxt.Value,
    'AND Codigo=' + Geral.FF0(QrTsErrosAlertasCabCodigo.Value),
    DModG.AllID_DB);
  QrTsErrosAlertasCab.Next;
  ReopenTsErrosAlertasIts(QrTsErrosAlertasItsCodTxt.Value);
end;

procedure TFmTsErrosAlertasCab.ItsAltera1Click(Sender: TObject);
begin
  MostraTsErrosAlertasIts(stUpd);
end;

procedure TFmTsErrosAlertasCab.CabExclui1Click(Sender: TObject);
begin
  Geral.MensagemBox('A��o n�o implementada! Solicite � Dermatek:' + #13#10 +
  Caption + #13#10 + TMenuItem(Sender).Name, 'Mensagem', MB_OK+MB_ICONINFORMATION);
end;

procedure TFmTsErrosAlertasCab.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmTsErrosAlertasCab.QueryPrincipalAfterOpen;
begin
end;

procedure TFmTsErrosAlertasCab.ItsExclui1Click(Sender: TObject);
{
var
  Controle: Integer;
}
begin
{
  if UMyMod.ExcluiRegistroInt1('Confirma a exclus�o do item selecionado?',
  'Cadastro_Com_Itens_ITS', 'Controle', QrTsErrosAlertasItsControle.Value) = ID_YES then
  begin
    Controle := GOTOy.LocalizaPriorNextIntQr(QrTsErrosAlertasIts,
      QrTsErrosAlertasItsControle, QrTsErrosAlertasItsControle.Value);
    ReopenCadastro_Com_Itens_ITS(Controle);
  end;
}
end;

procedure TFmTsErrosAlertasCab.Remove1Click(Sender: TObject);
begin
  UMyMod.ExcluiRegistroInt1('Confirma a retirada da cidade selecionada?',
    'tsErrosAlertasMun', 'Cidade', QrTsErrosAlertasMunCidade.Value, DModG.AllID_DB);
  QrTsErrosAlertasCab.Next;
  ReopenTsErrosAlertasIts(QrTsErrosAlertasItsCodTxt.Value);
end;

procedure TFmTsErrosAlertasCab.ReopenTsErrosAlertasIts(CodTxt: String);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrTsErrosAlertasIts, DModG.AllID_DB, [
  'SELECT * ',
  'FROM tserrosalertasits',
  'WHERE Codigo=' + Geral.FF0(QrTsErrosAlertasCabCodigo.Value),
  '']);
  //
  QrTsErrosAlertasIts.Locate('CodTxt', Codtxt, []);
end;


procedure TFmTsErrosAlertasCab.ReopenTsErrosAlertasMun(Cidade: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrTsErrosAlertasMun, DModG.AllID_DB, [
  'SELECT eam.*, dtm.Nome NO_CIDADE ',
  'FROM tserrosalertasmun eam ',
  'LEFT JOIN dtb_munici dtm ON dtm.Codigo=eam.Cidade ',
  'WHERE eam.Codigo=' + Geral.FF0(QrTsErrosAlertasCabCodigo.Value),
  '']);
  //
  QrTsErrosAlertasMun.Locate('Cidade', Cidade, []);
end;


procedure TFmTsErrosAlertasCab.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmTsErrosAlertasCab.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmTsErrosAlertasCab.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmTsErrosAlertasCab.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmTsErrosAlertasCab.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmTsErrosAlertasCab.SpeedButton8Click(Sender: TObject);
var
  IniDir, Arquivo: String;
begin
  IniDir := ExtractFileDir(FDestFile);
  Arquivo := ExtractFileName(FDestFile);
  //
  if MyObjects.FileOpenDialog(Self, IniDir, Arquivo,
  'Selecione o arquivo', '', [], Arquivo) then
    EdArq.Text := Arquivo;
end;

procedure TFmTsErrosAlertasCab.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmTsErrosAlertasCab.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrTsErrosAlertasCabCodigo.Value;
  Close;
end;

procedure TFmTsErrosAlertasCab.ItsInclui1Click(Sender: TObject);
begin
  MostraTsErrosAlertasIts(stIns);
end;

procedure TFmTsErrosAlertasCab.CabAltera1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrTsErrosAlertasCab, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'tserrosalertascab');
end;

procedure TFmTsErrosAlertasCab.BtCidadesClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMCidades, BtCidades);
end;

procedure TFmTsErrosAlertasCab.BtConfirmaClick(Sender: TObject);
var
  Codigo: Integer;
  Nome: String;
begin
  Nome := EdNome.ValueVariant;
  if MyObjects.FIC(Length(Nome) = 0, EdNome, 'Defina uma descri��o!') then Exit;
  //
  Codigo := UMyMod.BPGS1I32('tserrosalertascab', 'Codigo', '', '', tsPos,
    ImgTipo.SQLType, QrTsErrosAlertasCabCodigo.Value, DModG.AllID_DB);
  if UMyMod.ExecSQLInsUpdPanel(ImgTipo.SQLType, Self, PnEdita, 'tserrosalertascab',
  Codigo, DModG.QrAllUpd, [PnEdita], [PnDados], ImgTipo, True) then
  begin
    LocCod(Codigo, Codigo);
    if FSeq = 1 then Close;
  end;
end;

procedure TFmTsErrosAlertasCab.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := EdCodigo.ValueVariant;
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  UMyMod.UpdUnlockY(Codigo, DModG.AllID_DB, 'tserrosalertascab', 'Codigo');
end;

procedure TFmTsErrosAlertasCab.BtItsClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMIts, BtIts);
end;

procedure TFmTsErrosAlertasCab.Adiciona1Click(Sender: TObject);
var
  Cidade: Integer;
  Codigo: Variant;
const
  Aviso  = '...';
  Titulo = 'Adi��o de Cidade';
  Prompt = 'Informe a cidade:';
  Campo  = 'Descricao';
begin
{
  Application.CreateForm(TFm SelCod, Fm SelCod);
  Fm SelCod.Caption := 'Adi��o de Cidade';
  Fm SelCod.LaPrompt.Caption := 'Informe a cidade';
  Fm SelCod.QrSel.Close;
  Fm SelCod.QrSel.Database := DModG.AllID_DB;
  Fm SelCod.QrSel.SQL.Clear;
  Fm SelCod.QrSel.SQL.Add('SELECT Nome Descricao, Codigo');
  Fm SelCod.QrSel.SQL.Add('FROM dtb_munici');
  Fm SelCod.QrSel.SQL.Add('ORDER BY Descricao');
  Fm SelCod.QrSel.Open;
  //
  Fm SelCod.EdSel.ValueVariant := 0;
  Fm SelCod.CBSel.KeyValue := 0;
  Fm SelCod.ShowModal;
  Fm SelCod.Destroy;
  if VAR_SELCOD > 0 then
}
  Codigo := DBCheck.EscolheCodigoUnico(Aviso, Titulo, Prompt, nil, nil, Campo, 0, [
  'SELECT Codigo, Nome ' + Campo,
  'FROM dtb_munici ',
  'ORDER BY ' + Campo,
  ''], DModG.AllID_DB, False);
  if Codigo <> Null then
  begin
    Codigo := QrTsErrosAlertasCabCodigo.Value;
    Cidade := VAR_SELCOD;
    //
    if UMyMod.SQLInsUpd(DModG.QrAllUpd, stIns, 'tserrosalertasmun', False, [
    'Codigo'], ['Cidade'], [
     Codigo], [Cidade], True) then
       ReopenTsErrosAlertasMun(Cidade);
  end;
end;

procedure TFmTsErrosAlertasCab.BtCabClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMCab, BtCab);
end;

procedure TFmTsErrosAlertasCab.FormCreate(Sender: TObject);
begin
  EdArq.ValueVariant := FURL;
  //
  ImgTipo.SQLType := stLok;
  GBEdita.Align   := alClient;
  PnData.Align    := alClient;
  CriaOForm;
  FSeq := 0;
  //
  QrTsErrosAlertasCab.Database := DModG.AllID_DB;
  QrTsErrosAlertasIts.Database := DModG.AllID_DB;
  QrTsErrosAlertasMun.Database := DModG.AllID_DB;
  QrPsq.Database               := DModG.AllID_DB;
end;

procedure TFmTsErrosAlertasCab.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrTsErrosAlertasCabCodigo.Value, LaRegistro.Caption);
end;

procedure TFmTsErrosAlertasCab.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmTsErrosAlertasCab.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrTsErrosAlertasCabCodigo.Value, LaRegistro.Caption);
end;

procedure TFmTsErrosAlertasCab.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmTsErrosAlertasCab.QrTsErrosAlertasCabAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmTsErrosAlertasCab.QrTsErrosAlertasCabAfterScroll(DataSet: TDataSet);
begin
  ReopenTsErrosAlertasIts('');
  ReopenTsErrosAlertasMun(0);
end;

procedure TFmTsErrosAlertasCab.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //
  if FSeq = 1 then
    CabInclui1Click(Self)
  else
  if (not FLocIni) and (FCabIni <> 0)  then
  begin
    LocCod(FCabIni, FCabIni);
    if QrTsErrosAlertasCabCodigo.Value <> FCabIni then Geral.MensagemBox(
    'Grupo n�o localizado!', 'Aviso', MB_OK+MB_ICONWARNING);
    FLocIni := True;
  end;
  //
  EdArq.CharCase     := ecNormal;
  EdArq.ValueVariant := FURL;
end;

procedure TFmTsErrosAlertasCab.SbQueryClick(Sender: TObject);
begin
  LocCod(QrTsErrosAlertasCabCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'tserrosalertascab', DModG.AllID_DB, CO_VAZIO));
end;

procedure TFmTsErrosAlertasCab.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmTsErrosAlertasCab.CabInclui1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrTsErrosAlertasCab, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'tserrosalertascab');
end;

procedure TFmTsErrosAlertasCab.QrTsErrosAlertasCabBeforeOpen(DataSet: TDataSet);
begin
  QrTsErrosAlertasCabCodigo.DisplayFormat := FFormatFloat;
end;

end.

