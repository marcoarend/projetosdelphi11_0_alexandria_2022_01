unit TsErrosAlertasIts;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, DBCtrls, dmkLabel, Mask,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkDBEdit, dmkGeral, Variants,
  dmkValUsu, dmkImage, dmkMemo, UnDMkEnums;

type
  TFmTsErrosAlertasIts = class(TForm)
    Panel1: TPanel;
    CkContinuar: TCheckBox;
    GroupBox1: TGroupBox;
    Label4: TLabel;
    DBEdCodigo: TDBEdit;
    DBEdNome: TDBEdit;
    Label3: TLabel;
    GroupBox2: TGroupBox;
    EdCodTxt: TdmkEdit;
    Label6: TLabel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    Label1: TLabel;
    MeNome: TdmkMemo;
    Label2: TLabel;
    MeSolucao: TdmkMemo;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    FQrCab, FQrIts: TmySQLQuery;
    FDsCab: TDataSource;
  end;

  var
  FmTsErrosAlertasIts: TFmTsErrosAlertasIts;

implementation

uses UnMyObjects, ModuleGeral, UMySQLModule, UnInternalConsts, MyDBCheck,
  TsErrosAlertasCab;

{$R *.DFM}

procedure TFmTsErrosAlertasIts.BtOKClick(Sender: TObject);
var
  CodTxt, Nome, Solucao: String;
  Codigo: Integer;
begin
  Codigo         := Geral.IMV(DBEdCodigo.Text);
  CodTxt         := EdCodTxt.Text;
  Nome           := MeNome.Text;
  Solucao        := MeSolucao.Text;
  //
  if UMyMod.SQLInsUpd(DModG.QrAllUpd, ImgTipo.SQLType, 'tserrosalertasits', False, [
  'Nome', 'Solucao'], [
  'Codigo', 'CodTxt'], [
  Nome, Solucao], [
  Codigo, CodTxt], True) then
  begin
    FmTsErrosAlertasCab.ReopenTsErrosAlertasIts(CodTxt);
    Close;
  end;
end;

procedure TFmTsErrosAlertasIts.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmTsErrosAlertasIts.FormActivate(Sender: TObject);
begin
  DBEdCodigo.DataSource := FDsCab;
  DBEdNome.DataSource := FDsCab;
  MyObjects.CorIniComponente();
end;

procedure TFmTsErrosAlertasIts.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
end;

procedure TFmTsErrosAlertasIts.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

end.
