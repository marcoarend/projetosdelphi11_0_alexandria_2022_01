object FmNFSeSrvCad: TFmNFSeSrvCad
  Left = 368
  Top = 194
  Caption = 'NFS-SERVI-001 :: Regras Fiscais NFS-e'
  ClientHeight = 679
  ClientWidth = 784
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnDados: TPanel
    Left = 0
    Top = 96
    Width = 784
    Height = 583
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    object GBDados: TGroupBox
      Left = 0
      Top = 0
      Width = 784
      Height = 377
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 1
      object Label1: TLabel
        Left = 16
        Top = 16
        Width = 14
        Height = 13
        Caption = 'ID:'
        FocusControl = DBEdCodigo
      end
      object Label2: TLabel
        Left = 76
        Top = 16
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
        FocusControl = DBEdNome
      end
      object Label11: TLabel
        Left = 16
        Top = 56
        Width = 150
        Height = 13
        Caption = 'C'#243'digo do Servi'#231'o (LC 116/03):'
      end
      object Label12: TLabel
        Left = 16
        Top = 96
        Width = 83
        Height = 13
        Caption = 'C'#243'digo do CNAE:'
      end
      object Label13: TLabel
        Left = 16
        Top = 136
        Width = 64
        Height = 13
        Caption = 'Tipo de RPS:'
      end
      object Label17: TLabel
        Left = 380
        Top = 136
        Width = 246
        Height = 13
        Caption = 'N'#186' processo judicial/admin. suspens'#227'o exigibilidade:'
      end
      object Label25: TLabel
        Left = 636
        Top = 136
        Width = 139
        Height = 13
        Caption = 'C'#243'd. de tributa'#231#227'o munic'#237'pio:'
      end
      object DBEdCodigo: TdmkDBEdit
        Left = 16
        Top = 32
        Width = 56
        Height = 21
        Hint = 'N'#186' do banco'
        TabStop = False
        DataField = 'Codigo'
        DataSource = DsNFSeSrvCad
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 0
        UpdType = utYes
        Alignment = taRightJustify
      end
      object DBEdNome: TdmkDBEdit
        Left = 76
        Top = 32
        Width = 697
        Height = 21
        Hint = 'Nome do banco'
        Color = clWhite
        DataField = 'Nome'
        DataSource = DsNFSeSrvCad
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        UpdType = utYes
        Alignment = taLeftJustify
      end
      object GroupBox3: TGroupBox
        Left = 15
        Top = 176
        Width = 170
        Height = 61
        Caption = ' Reten'#231#227'o do ISS: '
        TabOrder = 2
        object Label14: TLabel
          Left = 8
          Top = 16
          Width = 65
          Height = 13
          Caption = 'Respons'#225'vel:'
        end
        object DBEdit8: TDBEdit
          Left = 8
          Top = 32
          Width = 24
          Height = 21
          DataField = 'ResponsavelRetencao'
          DataSource = DsNFSeSrvCad
          TabOrder = 0
        end
        object DBEdit11: TDBEdit
          Left = 32
          Top = 32
          Width = 132
          Height = 21
          DataField = 'NO_ResponsavelRetencao'
          DataSource = DsNFSeSrvCad
          TabOrder = 1
        end
      end
      object GroupBox4: TGroupBox
        Left = 187
        Top = 176
        Width = 170
        Height = 61
        Caption = ' Munic'#237'pio de incid'#234'ncia do ISS:'
        TabOrder = 3
        object Label15: TLabel
          Left = 8
          Top = 16
          Width = 65
          Height = 13
          Caption = 'Respons'#225'vel:'
        end
        object DBEdit9: TDBEdit
          Left = 8
          Top = 32
          Width = 24
          Height = 21
          DataField = 'QuemPagaISS'
          DataSource = DsNFSeSrvCad
          TabOrder = 0
        end
        object DBEdit12: TDBEdit
          Left = 32
          Top = 32
          Width = 132
          Height = 21
          DataField = 'NO_QuemPagaISS'
          DataSource = DsNFSeSrvCad
          TabOrder = 1
        end
      end
      object GroupBox6: TGroupBox
        Left = 360
        Top = 176
        Width = 417
        Height = 61
        Caption = ' Exigibilidade do ISS:'
        TabOrder = 4
        object Label16: TLabel
          Left = 8
          Top = 16
          Width = 158
          Height = 13
          Caption = 'C'#243'digo de natureza da opera'#231#227'o:'
        end
        object Label26: TLabel
          Left = 352
          Top = 16
          Width = 43
          Height = 13
          Caption = 'Al'#237'quota:'
          Color = clBtnFace
          ParentColor = False
        end
        object DBEdit10: TDBEdit
          Left = 8
          Top = 32
          Width = 24
          Height = 21
          DataField = 'ExigibilidadeIss'
          DataSource = DsNFSeSrvCad
          TabOrder = 0
        end
        object DBEdit13: TDBEdit
          Left = 32
          Top = 32
          Width = 317
          Height = 21
          DataField = 'NO_ExibilidadeIss'
          DataSource = DsNFSeSrvCad
          TabOrder = 1
        end
        object DBEdit1: TDBEdit
          Left = 352
          Top = 32
          Width = 56
          Height = 21
          DataField = 'Aliquota'
          DataSource = DsNFSeSrvCad
          TabOrder = 2
        end
      end
      object DBEdit2: TDBEdit
        Left = 16
        Top = 72
        Width = 68
        Height = 21
        DataField = 'ItemListaServico'
        DataSource = DsNFSeSrvCad
        TabOrder = 5
      end
      object DBEdit3: TDBEdit
        Left = 84
        Top = 72
        Width = 688
        Height = 21
        DataField = 'NO_ItemListaServico'
        DataSource = DsNFSeSrvCad
        TabOrder = 6
      end
      object DBEdit4: TDBEdit
        Left = 16
        Top = 112
        Width = 68
        Height = 21
        DataField = 'CodigoCnae'
        DataSource = DsNFSeSrvCad
        TabOrder = 7
      end
      object DBEdit5: TDBEdit
        Left = 84
        Top = 112
        Width = 688
        Height = 21
        DataField = 'NO_CodigoCnae'
        DataSource = DsNFSeSrvCad
        TabOrder = 8
      end
      object DBEdit6: TDBEdit
        Left = 636
        Top = 152
        Width = 136
        Height = 21
        DataField = 'CodigoTributacaoMunicipio'
        DataSource = DsNFSeSrvCad
        TabOrder = 9
      end
      object DBEdit7: TDBEdit
        Left = 380
        Top = 152
        Width = 253
        Height = 21
        DataField = 'NumeroProcesso'
        DataSource = DsNFSeSrvCad
        TabOrder = 10
      end
      object GroupBox8: TGroupBox
        Left = 15
        Top = 240
        Width = 170
        Height = 61
        Caption = ' Optante pelo Simples Nacional: '
        TabOrder = 11
        object Label20: TLabel
          Left = 8
          Top = 16
          Width = 35
          Height = 13
          Caption = 'Optou?'
        end
        object DBEdit14: TDBEdit
          Left = 8
          Top = 32
          Width = 24
          Height = 21
          DataField = 'OptanteSimplesNacional'
          DataSource = DsNFSeSrvCad
          TabOrder = 0
        end
        object DBEdit15: TDBEdit
          Left = 32
          Top = 32
          Width = 132
          Height = 21
          DataField = 'NO_OptanteSimplesNacional'
          DataSource = DsNFSeSrvCad
          TabOrder = 1
        end
      end
      object GroupBox9: TGroupBox
        Left = 187
        Top = 240
        Width = 170
        Height = 61
        Caption = ' Incentivo Fiscal:'
        TabOrder = 12
        object Label21: TLabel
          Left = 8
          Top = 16
          Width = 27
          Height = 13
          Caption = 'Tem?'
        end
        object DBEdit16: TDBEdit
          Left = 8
          Top = 32
          Width = 24
          Height = 21
          DataField = 'IncentivoFiscal'
          DataSource = DsNFSeSrvCad
          TabOrder = 0
        end
        object DBEdit17: TDBEdit
          Left = 32
          Top = 32
          Width = 132
          Height = 21
          DataField = 'NO_IncentivoFiscal'
          DataSource = DsNFSeSrvCad
          TabOrder = 1
        end
      end
      object GroupBox10: TGroupBox
        Left = 360
        Top = 240
        Width = 417
        Height = 61
        Caption = ' Regime Especial de Tributa'#231#227'o:'
        TabOrder = 13
        object Label22: TLabel
          Left = 8
          Top = 16
          Width = 114
          Height = 13
          Caption = 'C'#243'digo da identifica'#231#227'o:'
        end
        object DBEdit18: TDBEdit
          Left = 8
          Top = 32
          Width = 24
          Height = 21
          DataField = 'RegimeEspecialTributacao'
          DataSource = DsNFSeSrvCad
          TabOrder = 0
        end
        object DBEdit19: TDBEdit
          Left = 32
          Top = 32
          Width = 376
          Height = 21
          DataField = 'NO_RegimeEspecialTributacao'
          DataSource = DsNFSeSrvCad
          TabOrder = 1
        end
      end
      object DBEdit20: TDBEdit
        Left = 16
        Top = 152
        Width = 20
        Height = 21
        DataField = 'RpsTipo'
        DataSource = DsNFSeSrvCad
        TabOrder = 14
      end
      object DBEdit21: TDBEdit
        Left = 36
        Top = 152
        Width = 341
        Height = 21
        DataField = 'NO_RpsTipo'
        DataSource = DsNFSeSrvCad
        TabOrder = 15
      end
      object GroupBox14: TGroupBox
        Left = 16
        Top = 304
        Width = 533
        Height = 65
        Caption = ' Reten'#231#245'es: '
        TabOrder = 16
        object Panel7: TPanel
          Left = 2
          Top = 15
          Width = 529
          Height = 48
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          object Label34: TLabel
            Left = 8
            Top = 4
            Width = 43
            Height = 13
            Caption = 'Aliq. PIS:'
          end
          object Label36: TLabel
            Left = 92
            Top = 4
            Width = 68
            Height = 13
            Caption = 'Aliq.  COFINS:'
          end
          object Label37: TLabel
            Left = 176
            Top = 4
            Width = 54
            Height = 13
            Caption = 'Aliq.  INSS:'
          end
          object Label38: TLabel
            Left = 260
            Top = 4
            Width = 40
            Height = 13
            Caption = 'Aliq.  IR:'
          end
          object Label39: TLabel
            Left = 344
            Top = 4
            Width = 55
            Height = 13
            Caption = 'Aliq.  CSLL:'
          end
          object Label40: TLabel
            Left = 428
            Top = 4
            Width = 83
            Height = 13
            Caption = 'Aliq.  Outras Ret.:'
          end
          object DBEdit22: TDBEdit
            Left = 8
            Top = 20
            Width = 80
            Height = 21
            DataField = 'AlqPIS'
            DataSource = DsNFSeSrvCad
            TabOrder = 0
          end
          object DBEdit23: TDBEdit
            Left = 92
            Top = 20
            Width = 80
            Height = 21
            DataField = 'AlqCOFINS'
            DataSource = DsNFSeSrvCad
            TabOrder = 1
          end
          object DBEdit24: TDBEdit
            Left = 176
            Top = 20
            Width = 80
            Height = 21
            DataField = 'AlqINSS'
            DataSource = DsNFSeSrvCad
            TabOrder = 2
          end
          object DBEdit25: TDBEdit
            Left = 260
            Top = 20
            Width = 80
            Height = 21
            DataField = 'AlqIR'
            DataSource = DsNFSeSrvCad
            TabOrder = 3
          end
          object DBEdit26: TDBEdit
            Left = 344
            Top = 20
            Width = 80
            Height = 21
            DataField = 'AlqCSLL'
            DataSource = DsNFSeSrvCad
            TabOrder = 4
          end
          object DBEdit27: TDBEdit
            Left = 428
            Top = 20
            Width = 96
            Height = 21
            DataField = 'AlqOutrasRetencoes'
            DataSource = DsNFSeSrvCad
            TabOrder = 5
          end
        end
      end
      object GroupBox16: TGroupBox
        Left = 552
        Top = 304
        Width = 225
        Height = 65
        Caption = ' Exporta'#231#227'o para PDF: '
        TabOrder = 17
        object Panel9: TPanel
          Left = 2
          Top = 15
          Width = 221
          Height = 48
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          object dmkCheckBox1: TDBCheckBox
            Left = 8
            Top = 7
            Width = 81
            Height = 17
            Caption = 'Comprimido'
            DataField = 'PDFCompres'
            DataSource = DsNFSeSrvCad
            TabOrder = 0
            ValueChecked = '1'
            ValueUnchecked = '0'
          end
          object dmkCheckBox2: TDBCheckBox
            Left = 8
            Top = 25
            Width = 121
            Height = 17
            Caption = 'Incorporar Fontes'
            DataField = 'PDFEmbFont'
            DataSource = DsNFSeSrvCad
            TabOrder = 1
            ValueChecked = '1'
            ValueUnchecked = '0'
          end
          object dmkCheckBox3: TDBCheckBox
            Left = 96
            Top = 7
            Width = 117
            Height = 17
            Caption = 'Impress'#227'o otimizada'
            DataField = 'PDFPrnOptm'
            DataSource = DsNFSeSrvCad
            TabOrder = 2
            ValueChecked = '1'
            ValueUnchecked = '0'
          end
        end
      end
    end
    object GBCntrl: TGroupBox
      Left = 0
      Top = 519
      Width = 784
      Height = 64
      Align = alBottom
      TabOrder = 0
      object Panel5: TPanel
        Left = 2
        Top = 15
        Width = 172
        Height = 47
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 174
        Top = 15
        Width = 87
        Height = 47
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel3: TPanel
        Left = 261
        Top = 15
        Width = 521
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object BtExclui: TBitBtn
          Tag = 12
          Left = 188
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Exclui'
          Enabled = False
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
        end
        object BtAltera: TBitBtn
          Tag = 11
          Left = 96
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Altera'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtAlteraClick
        end
        object BtInclui: TBitBtn
          Tag = 10
          Left = 4
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Inclui'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtIncluiClick
        end
        object Panel2: TPanel
          Left = 412
          Top = 0
          Width = 109
          Height = 47
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 3
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 90
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
      end
    end
    object PnDBDiscriminacao: TPanel
      Left = 0
      Top = 377
      Width = 784
      Height = 32
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 2
      object dmkLabelRotate2: TdmkLabelRotate
        Left = 0
        Top = 0
        Width = 17
        Height = 32
        Angle = ag90
        Caption = 'Discrimina'#231#227'o:'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        Align = alLeft
        ExplicitLeft = 255
        ExplicitTop = 16
        ExplicitHeight = 181
      end
      object DBMemo1: TDBMemo
        Left = 17
        Top = 0
        Width = 767
        Height = 32
        Align = alClient
        DataField = 'Discriminacao'
        DataSource = DsNFSeSrvCad
        TabOrder = 0
      end
    end
    object Panel10: TPanel
      Left = 0
      Top = 472
      Width = 784
      Height = 47
      Align = alBottom
      BevelOuter = bvNone
      TabOrder = 3
      object DBRadioGroup1: TDBRadioGroup
        Left = 16
        Top = 4
        Width = 533
        Height = 39
        Caption = ' Opera'#231#227'o com consumidor: '
        Columns = 2
        DataField = 'indFinal'
        DataSource = DsNFSeSrvCad
        Items.Strings = (
          '0 - Normal'
          '1 - Consumidor final')
        ParentBackground = True
        TabOrder = 0
        Values.Strings = (
          '0'
          '1'
          '2'
          '3'
          '4'
          '5'
          '6'
          '7'
          '8'
          '9')
      end
      object DBRadioGroup2: TDBRadioGroup
        Left = 556
        Top = 4
        Width = 221
        Height = 39
        Caption = ' Servi'#231'o importado: '
        Columns = 2
        DataField = 'Importado'
        DataSource = DsNFSeSrvCad
        Items.Strings = (
          'N'#227'o'
          'Sim')
        ParentBackground = True
        TabOrder = 1
        Values.Strings = (
          '0'
          '1'
          '2'
          '3'
          '4'
          '5'
          '6'
          '7'
          '8'
          '9')
      end
    end
  end
  object PnEdita: TPanel
    Left = 0
    Top = 96
    Width = 784
    Height = 583
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    Visible = False
    object GBConfirma: TGroupBox
      Left = 0
      Top = 520
      Width = 784
      Height = 63
      Align = alBottom
      TabOrder = 1
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 12
        Top = 17
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object Panel1: TPanel
        Left = 644
        Top = 15
        Width = 138
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 7
          Top = 2
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
    end
    object GBEdita: TGroupBox
      Left = 0
      Top = 0
      Width = 784
      Height = 377
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 0
      object Label7: TLabel
        Left = 16
        Top = 16
        Width = 14
        Height = 13
        Caption = 'ID:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label9: TLabel
        Left = 76
        Top = 16
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label4: TLabel
        Left = 636
        Top = 136
        Width = 139
        Height = 13
        Caption = 'C'#243'd. de tributa'#231#227'o munic'#237'pio:'
      end
      object Label18: TLabel
        Left = 16
        Top = 56
        Width = 150
        Height = 13
        Caption = 'C'#243'digo do Servi'#231'o (LC 116/03):'
      end
      object SbListServN: TSpeedButton
        Left = 756
        Top = 72
        Width = 21
        Height = 21
        Caption = '...'
        OnClick = SbListServNClick
      end
      object Label5: TLabel
        Left = 16
        Top = 96
        Width = 83
        Height = 13
        Caption = 'C'#243'digo do CNAE:'
      end
      object SbCodigoCnae: TSpeedButton
        Left = 756
        Top = 112
        Width = 21
        Height = 21
        Caption = '...'
        OnClick = SbCodigoCnaeClick
      end
      object Label6: TLabel
        Left = 380
        Top = 136
        Width = 246
        Height = 13
        Caption = 'N'#186' processo judicial/admin. suspens'#227'o exigibilidade:'
      end
      object Label28: TLabel
        Left = 16
        Top = 136
        Width = 64
        Height = 13
        Caption = 'Tipo de RPS:'
      end
      object SbRpsIDTipo: TSpeedButton
        Left = 356
        Top = 152
        Width = 21
        Height = 21
        Caption = '...'
        Enabled = False
      end
      object EdCodigo: TdmkEdit
        Left = 16
        Top = 32
        Width = 56
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Codigo'
        UpdCampo = 'Codigo'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdNome: TdmkEdit
        Left = 76
        Top = 32
        Width = 701
        Height = 21
        TabOrder = 1
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Nome'
        UpdCampo = 'Nome'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdCodigoTributacaoMunicipio: TdmkEdit
        Left = 636
        Top = 152
        Width = 141
        Height = 21
        TabOrder = 9
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'CodigoTributacaoMunicipio'
        UpdCampo = 'CodigoTributacaoMunicipio'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdItemListaServico: TdmkEditCB
        Left = 16
        Top = 72
        Width = 68
        Height = 21
        TabOrder = 2
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'ItemListaServico'
        UpdCampo = 'ItemListaServico'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
        DBLookupComboBox = CBItemListaServico
        IgnoraDBLookupComboBox = False
      end
      object CBItemListaServico: TdmkDBLookupComboBox
        Left = 84
        Top = 72
        Width = 669
        Height = 21
        KeyField = 'CodAlf'
        ListField = 'Nome'
        ListSource = DsListServ
        TabOrder = 3
        dmkEditCB = EdItemListaServico
        QryCampo = 'ItemListaServico'
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdCodigoCnae: TdmkEditCB
        Left = 16
        Top = 112
        Width = 68
        Height = 21
        TabOrder = 4
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'CodigoCnae'
        UpdCampo = 'CodigoCnae'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
        DBLookupComboBox = CBCodigoCnae
        IgnoraDBLookupComboBox = False
      end
      object CBCodigoCnae: TdmkDBLookupComboBox
        Left = 84
        Top = 112
        Width = 669
        Height = 21
        KeyField = 'CodAlf'
        ListField = 'Nome'
        ListSource = DsCNAE21Cad
        TabOrder = 5
        dmkEditCB = EdCodigoCnae
        QryCampo = 'CodigoCnae'
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object GroupBox5: TGroupBox
        Left = 15
        Top = 176
        Width = 170
        Height = 61
        Caption = ' Reten'#231#227'o do ISS: '
        TabOrder = 10
        object SbResponsavelRetencao: TSpeedButton
          Left = 140
          Top = 32
          Width = 21
          Height = 21
          Hint = 'Inclui item de carteira'
          Caption = '...'
          Enabled = False
        end
        object Label35: TLabel
          Left = 8
          Top = 16
          Width = 65
          Height = 13
          Caption = 'Respons'#225'vel:'
        end
        object EdResponsavelRetencao: TdmkEditCB
          Left = 8
          Top = 32
          Width = 24
          Height = 21
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'ResponsavelRetencao'
          UpdCampo = 'ResponsavelRetencao'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          DBLookupComboBox = CBResponsavelRetencao
          IgnoraDBLookupComboBox = False
        end
        object CBResponsavelRetencao: TdmkDBLookupComboBox
          Left = 32
          Top = 32
          Width = 108
          Height = 21
          KeyField = 'Codigo'
          ListField = 'Nome'
          ListSource = DsTsResponsavelRetencao
          TabOrder = 1
          dmkEditCB = EdResponsavelRetencao
          QryCampo = 'ResponsavelRetencao'
          UpdType = utYes
          LocF7SQLMasc = '$#'
        end
      end
      object EdNumeroProcesso: TdmkEdit
        Left = 380
        Top = 152
        Width = 249
        Height = 21
        TabOrder = 8
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'NumeroProcesso'
        UpdCampo = 'NumeroProcesso'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object GroupBox1: TGroupBox
        Left = 187
        Top = 176
        Width = 170
        Height = 61
        Caption = ' Munic'#237'pio de incid'#234'ncia do ISS:'
        TabOrder = 11
        object SpeedButton5: TSpeedButton
          Left = 140
          Top = 32
          Width = 21
          Height = 21
          Hint = 'Inclui item de carteira'
          Caption = '...'
          Enabled = False
        end
        object Label8: TLabel
          Left = 8
          Top = 16
          Width = 55
          Height = 13
          Caption = 'Refer'#234'ncia:'
        end
        object EdQuemPagaISS: TdmkEditCB
          Left = 8
          Top = 32
          Width = 24
          Height = 21
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'QuemPagaISS'
          UpdCampo = 'QuemPagaISS'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          DBLookupComboBox = CBQuemPagaISS
          IgnoraDBLookupComboBox = False
        end
        object CBQuemPagaISS: TdmkDBLookupComboBox
          Left = 32
          Top = 32
          Width = 108
          Height = 21
          KeyField = 'Codigo'
          ListField = 'Nome'
          ListSource = DsMyMunicipioIncidencia
          TabOrder = 1
          dmkEditCB = EdQuemPagaISS
          QryCampo = 'QuemPagaISS'
          UpdType = utYes
          LocF7SQLMasc = '$#'
        end
      end
      object GroupBox2: TGroupBox
        Left = 360
        Top = 176
        Width = 417
        Height = 61
        Caption = ' Exigibilidade do ISS:'
        TabOrder = 12
        object SpeedButton6: TSpeedButton
          Left = 328
          Top = 32
          Width = 21
          Height = 21
          Caption = '...'
          Enabled = False
        end
        object Label10: TLabel
          Left = 8
          Top = 16
          Width = 158
          Height = 13
          Caption = 'C'#243'digo de natureza da opera'#231#227'o:'
        end
        object Label3: TLabel
          Left = 352
          Top = 16
          Width = 43
          Height = 13
          Caption = 'Al'#237'quota:'
          Color = clBtnFace
          ParentColor = False
        end
        object EdExigibilidadeIss: TdmkEditCB
          Left = 8
          Top = 32
          Width = 24
          Height = 21
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'ExigibilidadeIss'
          UpdCampo = 'ExigibilidadeIss'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          DBLookupComboBox = CBExigibilidadeIss
          IgnoraDBLookupComboBox = False
        end
        object CBExigibilidadeIss: TdmkDBLookupComboBox
          Left = 32
          Top = 32
          Width = 293
          Height = 21
          KeyField = 'Codigo'
          ListField = 'Nome'
          ListSource = DsTsExigibilidadeIss
          TabOrder = 1
          dmkEditCB = EdExigibilidadeIss
          QryCampo = 'ExigibilidadeIss'
          UpdType = utYes
          LocF7SQLMasc = '$#'
        end
        object EdAliquota: TdmkEdit
          Left = 352
          Top = 32
          Width = 56
          Height = 21
          Alignment = taRightJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 2
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          QryCampo = 'Aliquota'
          UpdCampo = 'Aliquota'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
      end
      object GroupBox7: TGroupBox
        Left = 360
        Top = 240
        Width = 417
        Height = 61
        Caption = ' Regime Especial de Tributa'#231#227'o:'
        TabOrder = 15
        object SpeedButton7: TSpeedButton
          Left = 388
          Top = 32
          Width = 21
          Height = 21
          Caption = '...'
          Enabled = False
        end
        object Label19: TLabel
          Left = 8
          Top = 16
          Width = 114
          Height = 13
          Caption = 'C'#243'digo da identifica'#231#227'o:'
        end
        object EdRegimeEspecialTributacao: TdmkEditCB
          Left = 8
          Top = 32
          Width = 24
          Height = 21
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'RegimeEspecialTributacao'
          UpdCampo = 'RegimeEspecialTributacao'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          DBLookupComboBox = CBRegimeEspecialTributacao
          IgnoraDBLookupComboBox = False
        end
        object CBRegimeEspecialTributacao: TdmkDBLookupComboBox
          Left = 32
          Top = 32
          Width = 353
          Height = 21
          KeyField = 'Codigo'
          ListField = 'Nome'
          ListSource = DsTsRegimeEspecialTributacao
          TabOrder = 1
          dmkEditCB = EdRegimeEspecialTributacao
          QryCampo = 'RegimeEspecialTributacao'
          UpdType = utYes
          LocF7SQLMasc = '$#'
        end
      end
      object GroupBox11: TGroupBox
        Left = 15
        Top = 240
        Width = 170
        Height = 61
        Caption = ' Optante pelo Simples Nacional: '
        TabOrder = 13
        object SpeedButton8: TSpeedButton
          Left = 140
          Top = 32
          Width = 21
          Height = 21
          Hint = 'Inclui item de carteira'
          Caption = '...'
          Enabled = False
        end
        object Label23: TLabel
          Left = 8
          Top = 16
          Width = 35
          Height = 13
          Caption = 'Optou?'
        end
        object EdOptanteSimplesNacional: TdmkEditCB
          Left = 8
          Top = 32
          Width = 24
          Height = 21
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'OptanteSimplesNacional'
          UpdCampo = 'OptanteSimplesNacional'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          DBLookupComboBox = CBOptanteSimplesNacional
          IgnoraDBLookupComboBox = False
        end
        object CBOptanteSimplesNacional: TdmkDBLookupComboBox
          Left = 32
          Top = 32
          Width = 108
          Height = 21
          KeyField = 'Codigo'
          ListField = 'Nome'
          ListSource = DsTsSimNao1
          TabOrder = 1
          dmkEditCB = EdOptanteSimplesNacional
          QryCampo = 'OptanteSimplesNacional'
          UpdType = utYes
          LocF7SQLMasc = '$#'
        end
      end
      object GroupBox12: TGroupBox
        Left = 187
        Top = 240
        Width = 170
        Height = 61
        Caption = ' Incentivo Fiscal: '
        TabOrder = 14
        object SpeedButton9: TSpeedButton
          Left = 140
          Top = 32
          Width = 21
          Height = 21
          Hint = 'Inclui item de carteira'
          Caption = '...'
          Enabled = False
        end
        object Label24: TLabel
          Left = 8
          Top = 16
          Width = 27
          Height = 13
          Caption = 'Tem?'
        end
        object EdIncentivoFiscal: TdmkEditCB
          Left = 8
          Top = 32
          Width = 24
          Height = 21
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'IncentivoFiscal'
          UpdCampo = 'IncentivoFiscal'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          DBLookupComboBox = CBIncentivoFiscal
          IgnoraDBLookupComboBox = False
        end
        object CBIncentivoFiscal: TdmkDBLookupComboBox
          Left = 32
          Top = 32
          Width = 108
          Height = 21
          KeyField = 'Codigo'
          ListField = 'Nome'
          ListSource = DsTsSimNao2
          TabOrder = 1
          dmkEditCB = EdIncentivoFiscal
          QryCampo = 'IncentivoFiscal'
          UpdType = utYes
          LocF7SQLMasc = '$#'
        end
      end
      object EdRpsTipo: TdmkEditCB
        Left = 16
        Top = 152
        Width = 20
        Height = 21
        Alignment = taRightJustify
        TabOrder = 6
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'RpsTipo'
        UpdCampo = 'RpsTipo'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBRpsTipo
        IgnoraDBLookupComboBox = False
      end
      object CBRpsTipo: TdmkDBLookupComboBox
        Left = 36
        Top = 152
        Width = 321
        Height = 21
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsTsTipoRps
        TabOrder = 7
        dmkEditCB = EdRpsTipo
        QryCampo = 'RpsTipo'
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object GroupBox13: TGroupBox
        Left = 16
        Top = 304
        Width = 533
        Height = 65
        Caption = ' Reten'#231#245'es: '
        TabOrder = 16
        object Panel6: TPanel
          Left = 2
          Top = 15
          Width = 529
          Height = 48
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          object Label27: TLabel
            Left = 8
            Top = 4
            Width = 43
            Height = 13
            Caption = 'Aliq. PIS:'
          end
          object Label29: TLabel
            Left = 92
            Top = 4
            Width = 68
            Height = 13
            Caption = 'Aliq.  COFINS:'
          end
          object Label30: TLabel
            Left = 176
            Top = 4
            Width = 54
            Height = 13
            Caption = 'Aliq.  INSS:'
          end
          object Label31: TLabel
            Left = 260
            Top = 4
            Width = 40
            Height = 13
            Caption = 'Aliq.  IR:'
          end
          object Label32: TLabel
            Left = 344
            Top = 4
            Width = 55
            Height = 13
            Caption = 'Aliq.  CSLL:'
          end
          object Label33: TLabel
            Left = 428
            Top = 4
            Width = 83
            Height = 13
            Caption = 'Aliq.  Outras Ret.:'
          end
          object EdAlqPis: TdmkEdit
            Left = 8
            Top = 20
            Width = 80
            Height = 21
            Alignment = taRightJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 0
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            QryCampo = 'AlqPis'
            UpdCampo = 'AlqPis'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
          object EdAlqCofins: TdmkEdit
            Left = 92
            Top = 20
            Width = 80
            Height = 21
            Alignment = taRightJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 1
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            QryCampo = 'AlqCofins'
            UpdCampo = 'AlqCofins'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
          object EdAlqInss: TdmkEdit
            Left = 176
            Top = 20
            Width = 80
            Height = 21
            Alignment = taRightJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 2
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            QryCampo = 'AlqInss'
            UpdCampo = 'AlqInss'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
          object EdAlqIr: TdmkEdit
            Left = 260
            Top = 20
            Width = 80
            Height = 21
            Alignment = taRightJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 3
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            QryCampo = 'AlqIr'
            UpdCampo = 'AlqIr'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
          object EdAlqCsll: TdmkEdit
            Left = 344
            Top = 20
            Width = 80
            Height = 21
            Alignment = taRightJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 4
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            QryCampo = 'AlqCsll'
            UpdCampo = 'AlqCsll'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
          object EdAlqOutrasRetencoes: TdmkEdit
            Left = 428
            Top = 20
            Width = 96
            Height = 21
            Alignment = taRightJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 5
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            QryCampo = 'AlqOutrasRetencoes'
            UpdCampo = 'AlqOutrasRetencoes'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
        end
      end
      object GroupBox15: TGroupBox
        Left = 552
        Top = 304
        Width = 225
        Height = 65
        Caption = ' Exporta'#231#227'o para PDF: '
        TabOrder = 17
        object Panel8: TPanel
          Left = 2
          Top = 15
          Width = 221
          Height = 48
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          object CkPDFCompres: TdmkCheckBox
            Left = 8
            Top = 7
            Width = 81
            Height = 17
            Caption = 'Comprimido'
            TabOrder = 0
            QryCampo = 'PDFCompres'
            UpdCampo = 'PDFCompres'
            UpdType = utYes
            ValCheck = '1'
            ValUncheck = '0'
            OldValor = #0
          end
          object CkPDFEmbFont: TdmkCheckBox
            Left = 8
            Top = 25
            Width = 121
            Height = 17
            Caption = 'Incorporar Fontes'
            TabOrder = 1
            QryCampo = 'PDFEmbFont'
            UpdCampo = 'PDFEmbFont'
            UpdType = utYes
            ValCheck = '1'
            ValUncheck = '0'
            OldValor = #0
          end
          object CkPDFPrnOptm: TdmkCheckBox
            Left = 96
            Top = 7
            Width = 117
            Height = 17
            Caption = 'Impress'#227'o otimizada'
            TabOrder = 2
            QryCampo = 'PDFPrnOptm'
            UpdCampo = 'PDFPrnOptm'
            UpdType = utYes
            ValCheck = '1'
            ValUncheck = '0'
            OldValor = #0
          end
        end
      end
    end
    object PnDiscriminacao: TPanel
      Left = 0
      Top = 377
      Width = 784
      Height = 40
      Align = alTop
      BevelOuter = bvNone
      Caption = 'PnDiscriminacao'
      TabOrder = 2
      object dmkLabelRotate1: TdmkLabelRotate
        Left = 0
        Top = 0
        Width = 17
        Height = 40
        Angle = ag90
        Caption = 'Discrimina'#231#227'o:'
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        Align = alLeft
        ExplicitTop = 4
        ExplicitHeight = 88
      end
      object MeDiscriminacao: TdmkMemo
        Left = 17
        Top = 0
        Width = 767
        Height = 40
        TabStop = False
        Align = alClient
        TabOrder = 0
        WantReturns = False
        OnKeyDown = MeDiscriminacaoKeyDown
        QryCampo = 'Discriminacao'
        UpdCampo = 'Discriminacao'
        UpdType = utYes
      end
    end
    object Panel11: TPanel
      Left = 0
      Top = 472
      Width = 784
      Height = 48
      Align = alBottom
      BevelOuter = bvNone
      TabOrder = 3
      object RG_indFinal: TdmkRadioGroup
        Left = 16
        Top = 2
        Width = 533
        Height = 39
        Caption = ' Opera'#231#227'o com consumidor: '
        Columns = 2
        ItemIndex = 0
        Items.Strings = (
          '0 - Normal'
          '1 - Consumidor final')
        TabOrder = 0
        QryCampo = 'indFinal'
        UpdCampo = 'indFinal'
        UpdType = utYes
        OldValor = 0
      end
      object RGImportado: TdmkRadioGroup
        Left = 556
        Top = 2
        Width = 221
        Height = 39
        Caption = ' Servi'#231'o importado: '
        Columns = 2
        ItemIndex = 0
        Items.Strings = (
          'N'#227'o'
          'Sim')
        TabOrder = 1
        QryCampo = 'Importado'
        UpdCampo = 'Importado'
        UpdType = utYes
        OldValor = 0
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 784
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 736
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 12
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 216
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
        OnClick = SbImprimeClick
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 216
      Top = 0
      Width = 520
      Height = 52
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 268
        Height = 32
        Caption = 'Regras Fiscais NFS-e'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 268
        Height = 32
        Caption = 'Regras Fiscais NFS-e'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 268
        Height = 32
        Caption = 'Regras Fiscais NFS-e'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 52
    Width = 784
    Height = 44
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 780
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object QrNFSeSrvCad: TmySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrNFSeSrvCadBeforeOpen
    AfterOpen = QrNFSeSrvCadAfterOpen
    SQL.Strings = (
      'SELECT mmi.Nome NO_QuemPagaISS,'
      'rre.Nome NO_ResponsavelRetencao,'
      'lsv.Nome NO_ItemListaServico,'
      'c21.Nome NO_CodigoCnae,'
      'eis.Nome NO_ExibilidadeIss, '
      'ret.Nome NO_RegimeEspecialTributacao,'
      'sn1.Nome NO_OptanteSimplesNacional,'
      'sn2.Nome NO_IncentivoFiscal, '
      'rpt.Nome NO_RpsTipo, nsc.*'
      'FROM toolrent.nfsesrvcad nsc'
      'LEFT JOIN loctoolrall.mymunicipioincidencia mmi'
      '  ON mmi.Codigo=nsc.QuemPagaISS'
      'LEFT JOIN loctoolrall.tsresponsavelretencao rre'
      '  ON rre.Codigo=nsc.ResponsavelRetencao'
      'LEFT JOIN loctoolrall.tsexigibilidadeiss eis'
      '  ON eis.Codigo=nsc.ExigibilidadeIss'
      'LEFT JOIN loctoolrall.listserv lsv'
      '  ON lsv.CodAlf=nsc.ItemListaServico'
      'LEFT JOIN loctoolrall.cnae21cad c21'
      '  ON c21.CodAlf=nsc.CodigoCnae'
      'LEFT JOIN loctoolrall.tsregimeespecialtributacao ret'
      '  ON ret.Codigo=nsc.RegimeEspecialTributacao'
      'LEFT JOIN loctoolrall.tssimnao sn1'
      '  ON sn1.Codigo=nsc.OptanteSimplesNacional'
      'LEFT JOIN loctoolrall.tssimnao sn2'
      '  ON sn2.Codigo=nsc.IncentivoFiscal'
      'LEFT JOIN loctoolrall.tstiporps rpt'
      '  ON rpt.Codigo=nsc.RpsTipo'
      ''
      'WHERE nsc.Codigo > 0'
      'AND nsc.Codigo=:P0')
    Left = 64
    Top = 64
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrNFSeSrvCadCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'nfsesrvcad.Codigo'
    end
    object QrNFSeSrvCadNome: TWideStringField
      FieldName = 'Nome'
      Origin = 'nfsesrvcad.Nome'
      Size = 255
    end
    object QrNFSeSrvCadLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'nfsesrvcad.Lk'
    end
    object QrNFSeSrvCadDataCad: TDateField
      FieldName = 'DataCad'
      Origin = 'nfsesrvcad.DataCad'
    end
    object QrNFSeSrvCadDataAlt: TDateField
      FieldName = 'DataAlt'
      Origin = 'nfsesrvcad.DataAlt'
    end
    object QrNFSeSrvCadUserCad: TIntegerField
      FieldName = 'UserCad'
      Origin = 'nfsesrvcad.UserCad'
    end
    object QrNFSeSrvCadUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Origin = 'nfsesrvcad.UserAlt'
    end
    object QrNFSeSrvCadAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Origin = 'nfsesrvcad.AlterWeb'
    end
    object QrNFSeSrvCadAtivo: TSmallintField
      FieldName = 'Ativo'
      Origin = 'nfsesrvcad.Ativo'
    end
    object QrNFSeSrvCadCodigoTributacaoMunicipio: TWideStringField
      FieldName = 'CodigoTributacaoMunicipio'
      Origin = 'nfsesrvcad.CodigoTributacaoMunicipio'
    end
    object QrNFSeSrvCadResponsavelRetencao: TSmallintField
      FieldName = 'ResponsavelRetencao'
      Origin = 'nfsesrvcad.ResponsavelRetencao'
    end
    object QrNFSeSrvCadItemListaServico: TWideStringField
      FieldName = 'ItemListaServico'
      Origin = 'nfsesrvcad.ItemListaServico'
      Size = 5
    end
    object QrNFSeSrvCadCodigoCnae: TWideStringField
      DisplayWidth = 20
      FieldName = 'CodigoCnae'
      Origin = 'nfsesrvcad.CodigoCnae'
    end
    object QrNFSeSrvCadNumeroProcesso: TWideStringField
      FieldName = 'NumeroProcesso'
      Origin = 'nfsesrvcad.NumeroProcesso'
      Size = 30
    end
    object QrNFSeSrvCadQuemPagaISS: TSmallintField
      FieldName = 'QuemPagaISS'
    end
    object QrNFSeSrvCadExigibilidadeIss: TSmallintField
      FieldName = 'ExigibilidadeIss'
      Origin = 'nfsesrvcad.ExigibilidadeIss'
    end
    object QrNFSeSrvCadNO_QuemPagaISS: TWideStringField
      FieldName = 'NO_QuemPagaISS'
      Size = 60
    end
    object QrNFSeSrvCadNO_ResponsavelRetencao: TWideStringField
      FieldName = 'NO_ResponsavelRetencao'
      Origin = 'tsresponsavelretencao.Nome'
      Size = 60
    end
    object QrNFSeSrvCadNO_ItemListaServico: TWideStringField
      FieldName = 'NO_ItemListaServico'
      Origin = 'listserv.Nome'
      Size = 255
    end
    object QrNFSeSrvCadNO_CodigoCnae: TWideStringField
      FieldName = 'NO_CodigoCnae'
      Origin = 'cnae21cad.Nome'
      Size = 255
    end
    object QrNFSeSrvCadNO_ExibilidadeIss: TWideStringField
      FieldName = 'NO_ExibilidadeIss'
      Origin = 'tsexigibilidadeiss.Nome'
      Size = 120
    end
    object QrNFSeSrvCadDiscriminacao: TWideMemoField
      FieldName = 'Discriminacao'
      Origin = 'nfsesrvcad.Discriminacao'
      Required = True
      BlobType = ftWideMemo
      Size = 4
    end
    object QrNFSeSrvCadRegimeEspecialTributacao: TSmallintField
      FieldName = 'RegimeEspecialTributacao'
    end
    object QrNFSeSrvCadOptanteSimplesNacional: TSmallintField
      FieldName = 'OptanteSimplesNacional'
    end
    object QrNFSeSrvCadIncentivoFiscal: TSmallintField
      FieldName = 'IncentivoFiscal'
    end
    object QrNFSeSrvCadNO_RegimeEspecialTributacao: TWideStringField
      FieldName = 'NO_RegimeEspecialTributacao'
      Size = 120
    end
    object QrNFSeSrvCadNO_OptanteSimplesNacional: TWideStringField
      FieldName = 'NO_OptanteSimplesNacional'
      Size = 6
    end
    object QrNFSeSrvCadNO_IncentivoFiscal: TWideStringField
      FieldName = 'NO_IncentivoFiscal'
      Size = 6
    end
    object QrNFSeSrvCadRpsTipo: TSmallintField
      FieldName = 'RpsTipo'
    end
    object QrNFSeSrvCadNO_RpsTipo: TWideStringField
      FieldName = 'NO_RpsTipo'
      Size = 60
    end
    object QrNFSeSrvCadAliquota: TFloatField
      FieldName = 'Aliquota'
      DisplayFormat = '0.00'
    end
    object QrNFSeSrvCadNaturezaOperacao: TIntegerField
      FieldName = 'NaturezaOperacao'
    end
    object QrNFSeSrvCadAlqPIS: TFloatField
      FieldName = 'AlqPIS'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrNFSeSrvCadAlqCOFINS: TFloatField
      FieldName = 'AlqCOFINS'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrNFSeSrvCadAlqINSS: TFloatField
      FieldName = 'AlqINSS'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrNFSeSrvCadAlqIR: TFloatField
      FieldName = 'AlqIR'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrNFSeSrvCadAlqCSLL: TFloatField
      FieldName = 'AlqCSLL'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrNFSeSrvCadAlqOutrasRetencoes: TFloatField
      FieldName = 'AlqOutrasRetencoes'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrNFSeSrvCadPDFCompres: TSmallintField
      FieldName = 'PDFCompres'
    end
    object QrNFSeSrvCadPDFEmbFont: TSmallintField
      FieldName = 'PDFEmbFont'
    end
    object QrNFSeSrvCadPDFPrnOptm: TSmallintField
      FieldName = 'PDFPrnOptm'
    end
    object QrNFSeSrvCadindFinal: TSmallintField
      FieldName = 'indFinal'
    end
    object QrNFSeSrvCadImportado: TSmallintField
      FieldName = 'Importado'
    end
  end
  object DsNFSeSrvCad: TDataSource
    DataSet = QrNFSeSrvCad
    Left = 92
    Top = 64
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = BtInclui
    CanUpd01 = BtAltera
    CanDel01 = BtExclui
    Left = 120
    Top = 64
  end
  object QrListServ: TmySQLQuery
    Database = DModG.AllID_DB
    SQL.Strings = (
      'SELECT Codigo, CodAlf, Nome'
      'FROM listserv'
      'ORDER BY Nome')
    Left = 560
    Top = 12
    object QrListServCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrListServCodAlf: TWideStringField
      FieldName = 'CodAlf'
      Required = True
    end
    object QrListServNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 255
    end
  end
  object DsListServ: TDataSource
    DataSet = QrListServ
    Left = 588
    Top = 12
  end
  object QrCNAE21Cad: TmySQLQuery
    Database = DModG.AllID_DB
    SQL.Strings = (
      'SELECT Codigo, CodAlf, Nome'
      'FROM cnae21cad'
      'WHERE Nivel=1'
      'ORDER BY Nome')
    Left = 504
    Top = 12
    object QrCNAE21CadNome: TWideStringField
      FieldName = 'Nome'
      Size = 255
    end
    object QrCNAE21CadCodAlf: TWideStringField
      FieldName = 'CodAlf'
    end
    object QrCNAE21CadCodigo: TIntegerField
      FieldName = 'Codigo'
    end
  end
  object DsCNAE21Cad: TDataSource
    DataSet = QrCNAE21Cad
    Left = 532
    Top = 12
  end
  object QrTsResponsavelRetencao: TmySQLQuery
    Database = DModG.AllID_DB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM tsresponsavelretencao'
      'ORDER BY Nome')
    Left = 616
    Top = 12
    object QrTsResponsavelRetencaoCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrTsResponsavelRetencaoNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
  end
  object DsTsResponsavelRetencao: TDataSource
    DataSet = QrTsResponsavelRetencao
    Left = 644
    Top = 12
  end
  object DsTsExigibilidadeIss: TDataSource
    DataSet = QrTsExigibilidadeIss
    Left = 532
    Top = 40
  end
  object QrTsExigibilidadeIss: TmySQLQuery
    Database = DModG.AllID_DB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM tsexigibilidadeiss'
      'ORDER BY Nome')
    Left = 504
    Top = 40
    object QrTsExigibilidadeIssCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrTsExigibilidadeIssNome: TWideStringField
      FieldName = 'Nome'
      Size = 120
    end
  end
  object QrMyMunicipioIncidencia: TmySQLQuery
    Database = DModG.AllID_DB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM mymunicipioincidencia'
      'ORDER BY Nome')
    Left = 560
    Top = 40
    object QrMyMunicipioIncidenciaCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrMyMunicipioIncidenciaNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
  end
  object DsMyMunicipioIncidencia: TDataSource
    DataSet = QrMyMunicipioIncidencia
    Left = 588
    Top = 40
  end
  object QrTsRegimeEspecialTributacao: TmySQLQuery
    Database = DModG.AllID_DB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM tsregimeespecialtributacao'
      'ORDER BY Nome')
    Left = 616
    Top = 40
    object QrTsRegimeEspecialTributacaoCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrTsRegimeEspecialTributacaoNome: TWideStringField
      FieldName = 'Nome'
      Size = 120
    end
  end
  object DsTsRegimeEspecialTributacao: TDataSource
    DataSet = QrTsRegimeEspecialTributacao
    Left = 644
    Top = 40
  end
  object QrTsSimNao1: TmySQLQuery
    Database = DModG.AllID_DB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM tssimnao'
      'ORDER BY Nome')
    Left = 672
    Top = 12
    object QrTsSimNao1Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrTsSimNao1Nome: TWideStringField
      FieldName = 'Nome'
      Size = 6
    end
  end
  object DsTsSimNao1: TDataSource
    DataSet = QrTsSimNao1
    Left = 700
    Top = 12
  end
  object QrTsSimNao2: TmySQLQuery
    Database = DModG.AllID_DB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM tssimnao'
      'ORDER BY Nome')
    Left = 672
    Top = 40
    object QrTsSimNao2Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrTsSimNao2Nome: TWideStringField
      FieldName = 'Nome'
      Size = 6
    end
  end
  object DsTsSimNao2: TDataSource
    DataSet = QrTsSimNao2
    Left = 700
    Top = 40
  end
  object QrTsTipoRps: TmySQLQuery
    Database = DModG.AllID_DB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM tstiporps'
      'ORDER BY Nome')
    Left = 504
    Top = 68
    object QrTsTipoRpsCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrTsTipoRpsNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
  end
  object DsTsTipoRps: TDataSource
    DataSet = QrTsTipoRps
    Left = 532
    Top = 68
  end
end
