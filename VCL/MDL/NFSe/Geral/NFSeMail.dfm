object FmNFSeMail: TFmNFSeMail
  Left = 339
  Top = 185
  Caption = 'NFS-EMAIL-001 :: Envio de NFS-es por e-mail'
  ClientHeight = 629
  ClientWidth = 919
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 919
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 871
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 88
      Height = 48
      Align = alLeft
      TabOrder = 1
      object BtProPad: TBitBtn
        Tag = 334
        Left = 4
        Top = 4
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtProPadClick
      end
      object BtEntidades: TBitBtn
        Tag = 132
        Left = 45
        Top = 4
        Width = 40
        Height = 40
        Cursor = crHandPoint
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        OnClick = BtEntidadesClick
      end
    end
    object GB_M: TGroupBox
      Left = 88
      Top = 0
      Width = 783
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 333
        Height = 32
        Caption = 'Envio de NFS-es por e-mail'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 333
        Height = 32
        Caption = 'Envio de NFS-es por e-mail'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 333
        Height = 32
        Caption = 'Envio de NFS-es por e-mail'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 919
    Height = 467
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 919
      Height = 467
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 919
        Height = 467
        Align = alClient
        TabOrder = 0
        object LaTotal: TLabel
          Left = 2
          Top = 452
          Width = 915
          Height = 13
          Align = alBottom
          ExplicitWidth = 3
        end
        object PnMenu: TPanel
          Left = 2
          Top = 15
          Width = 915
          Height = 47
          Align = alTop
          ParentBackground = False
          TabOrder = 0
          ExplicitTop = 12
          object BtPEBProtocolo: TBitBtn
            Tag = 14
            Left = 5
            Top = 2
            Width = 120
            Height = 40
            Caption = '&Protocolo'
            NumGlyphs = 2
            TabOrder = 0
            OnClick = BtPEBProtocoloClick
          end
          object BtPEBTodos: TBitBtn
            Tag = 127
            Left = 130
            Top = 2
            Width = 120
            Height = 40
            Caption = '&Todos'
            NumGlyphs = 2
            TabOrder = 1
            OnClick = BtPEBTodosClick
          end
          object BtEnvia: TBitBtn
            Tag = 244
            Left = 380
            Top = 2
            Width = 120
            Height = 40
            Caption = '&Envia'
            NumGlyphs = 2
            TabOrder = 3
            OnClick = BtEnviaClick
          end
          object BtPEBNenhum: TBitBtn
            Tag = 128
            Left = 255
            Top = 2
            Width = 120
            Height = 40
            Caption = '&Nenhum'
            NumGlyphs = 2
            TabOrder = 2
            OnClick = BtPEBNenhumClick
          end
        end
        object PB1: TProgressBar
          Left = 2
          Top = 62
          Width = 915
          Height = 17
          Align = alTop
          TabOrder = 1
          ExplicitTop = 59
        end
        object TBProtocolos: TTabControl
          Left = 2
          Top = 79
          Width = 915
          Height = 373
          Align = alClient
          TabHeight = 25
          TabOrder = 2
          Tabs.Strings = (
            'NFS-es Autorizadas'
            'NFS-es Canceladas')
          TabIndex = 0
          OnChange = TBProtocolosChange
          ExplicitTop = 76
          ExplicitHeight = 379
          object DBGrid1: TDBGrid
            Left = 4
            Top = 31
            Width = 907
            Height = 65
            Align = alTop
            DataSource = DsProt1
            TabOrder = 0
            TitleFont.Charset = ANSI_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            Columns = <
              item
                Expanded = False
                FieldName = 'Tarefa'
                Width = 70
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Nome'
                Width = 720
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Codigo'
                Title.Caption = 'ID'
                Width = 70
                Visible = True
              end>
          end
          object DBG_ProtocoNFSe: TdmkDBGridZTO
            Left = 4
            Top = 96
            Width = 907
            Height = 273
            Align = alClient
            DataSource = DsProtocoNFSe
            Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
            TabOrder = 1
            TitleFont.Charset = ANSI_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            RowColors = <>
            Columns = <
              item
                Expanded = False
                FieldName = 'LOTE'
                Title.Caption = 'Lote'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'PROTOCOLO'
                Title.Caption = 'Protocolo'
                Title.Font.Charset = DEFAULT_CHARSET
                Title.Font.Color = clWindowText
                Title.Font.Height = -11
                Title.Font.Name = 'MS Sans Serif'
                Title.Font.Style = [fsBold]
                Width = 75
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'DataE_Txt'
                Title.Caption = 'Envio'
                Title.Font.Charset = DEFAULT_CHARSET
                Title.Font.Color = clWindowText
                Title.Font.Height = -11
                Title.Font.Name = 'MS Sans Serif'
                Title.Font.Style = [fsBold]
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'RpsIDTipo'
                Title.Caption = 'Tipo'
                Width = 28
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'RpsIDSerie'
                Title.Caption = 'S'#233'rie'
                Width = 32
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'RpsIDNumero'
                Title.Caption = 'N'#186' RPS'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NfsNumero'
                Font.Charset = ANSI_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                Title.Caption = 'N'#186' NFS-e'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ValorServicos'
                Title.Caption = '$ Servi'#231'os'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'RpsDataEmissao'
                Title.Caption = 'Data emis.'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'RpsHoraEmissao'
                Title.Caption = 'Hora emis.'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NOMEENT'
                Title.Caption = 'Entidade'
                Width = 215
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Email'
                Title.Caption = 'E-mail'
                Width = 200
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Contato'
                Width = 150
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Telefone_TXT'
                Title.Caption = 'Telefone'
                Width = 120
                Visible = True
              end>
          end
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 515
    Width = 919
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 915
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 17
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 559
    Width = 919
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 773
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 771
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
    end
  end
  object QrProt1: TMySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrProt1BeforeClose
    AfterScroll = QrProt1AfterScroll
    SQL.Strings = (
      'SELECT pep.Codigo, pro.Codigo Tarefa, pro.Nome, '
      'pro.PreEmeio, mai.NaoEnvBloq'
      'FROM protocolos pro'
      'LEFT JOIN preemail mai ON mai.Codigo = pro.PreEmeio'
      'WHERE pro.Tipo=1'
      'AND pro.def_client=:P0')
    Left = 286
    Top = 334
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrProt1Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrProt1Nome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QrProt1PreEmeio: TIntegerField
      FieldName = 'PreEmeio'
    end
    object QrProt1NaoEnvBloq: TIntegerField
      FieldName = 'NaoEnvBloq'
    end
    object QrProt1Tarefa: TIntegerField
      FieldName = 'Tarefa'
    end
  end
  object DsProt1: TDataSource
    DataSet = QrProt1
    Left = 314
    Top = 334
  end
  object QrProtocoNFSe: TMySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrProtocoNFSeBeforeClose
    AfterScroll = QrProtocoNFSeAfterScroll
    OnCalcFields = QrProtocoNFSeCalcFields
    SQL.Strings = (
      'SELECT ari.Vencto, ari.Entidade, SUM(ari.Valor) Valor,'
      'cna.Nome NOMECNAB_Cfg, CASE WHEN ent.Tipo=0'
      'THEN ent.RazaoSocial ELSE ent.Nome END NOMEENT,'
      'ari.Boleto, ari.CNAB_Cfg, ari.Protocolo PROTOCOD,'
      'ppi.Conta PROTOCOLO, ptc.Nome TAREFA,'
      'IF(snd.Tipo=0, snd.RazaoSocial, snd.Nome) DELIVER,'
      'ppi.Controle LOTE, ppi.Vencto VENCTO_PROT, '
      'ppi.Valor VALOR_PROT, ppi.MoraDiaVal MORADIAVAL, '
      'ppi.MultaVal MULTAVAL,  pre.Codigo PREVCOD, pre.Periodo'
      'FROM arreits ari'
      'LEFT JOIN prev pre ON pre.Codigo=ari.Codigo'
      'LEFT JOIN entidades ent ON ent.Codigo=ari.Entidade'
      'LEFT JOIN cnab_cfg cna ON cna.Codigo=ari.CNAB_Cfg'
      'LEFT JOIN protocolos ptc ON ptc.Codigo=ari.Protocolo'
      'LEFT JOIN protpakits ppi ON ppi.Docum=ari.Boleto'
      'LEFT JOIN entidades snd ON snd.Codigo=ptc.Def_Sender'
      'WHERE ari.Codigo=:P0'
      'AND ptc.Def_Client=:P1'
      'GROUP BY ari.CNAB_Cfg, ari.Entidade, ari.Vencto'
      'ORDER BY  NOMECNAB_Cfg, NOMEENT')
    Left = 404
    Top = 332
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrProtocoNFSeCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrProtocoNFSeRpsIDTipo: TSmallintField
      FieldName = 'RpsIDTipo'
    end
    object QrProtocoNFSeRpsIDSerie: TWideStringField
      FieldName = 'RpsIDSerie'
      Size = 5
    end
    object QrProtocoNFSeRpsIDNumero: TIntegerField
      FieldName = 'RpsIDNumero'
    end
    object QrProtocoNFSeRpsDataEmissao: TDateField
      FieldName = 'RpsDataEmissao'
    end
    object QrProtocoNFSeRpsHoraEmissao: TTimeField
      FieldName = 'RpsHoraEmissao'
    end
    object QrProtocoNFSeValorServicos: TFloatField
      FieldName = 'ValorServicos'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrProtocoNFSeNOMEENT: TWideStringField
      FieldName = 'NOMEENT'
      Size = 100
    end
    object QrProtocoNFSeLOTE: TIntegerField
      FieldName = 'LOTE'
      DisplayFormat = '000000;-000000;'
    end
    object QrProtocoNFSePROTOCOLO: TIntegerField
      FieldName = 'PROTOCOLO'
      DisplayFormat = '000000;-000000;'
    end
    object QrProtocoNFSeDataE_Txt: TWideStringField
      FieldName = 'DataE_Txt'
      Size = 10
    end
    object QrProtocoNFSeEmail: TWideStringField
      FieldName = 'Email'
      Size = 255
    end
    object QrProtocoNFSeTAREFA: TWideStringField
      FieldName = 'TAREFA'
      Size = 50
    end
    object QrProtocoNFSeProtocoPak: TIntegerField
      FieldName = 'ProtocoPak'
    end
    object QrProtocoNFSeDataD: TDateField
      FieldName = 'DataD'
    end
    object QrProtocoNFSeEMeio_ID: TFloatField
      FieldName = 'EMeio_ID'
    end
    object QrProtocoNFSeDataD_Txt: TWideStringField
      FieldName = 'DataD_Txt'
      Size = 16
    end
    object QrProtocoNFSeAmbiente: TSmallintField
      FieldName = 'Ambiente'
    end
    object QrProtocoNFSePROTOCOD: TIntegerField
      FieldName = 'PROTOCOD'
    end
    object QrProtocoNFSeTipoProt: TIntegerField
      FieldName = 'TipoProt'
    end
    object QrProtocoNFSeEntidade: TIntegerField
      FieldName = 'Entidade'
    end
    object QrProtocoNFSeNfsNumero: TLargeintField
      FieldName = 'NfsNumero'
    end
    object QrProtocoNFSeStatus: TIntegerField
      FieldName = 'Status'
    end
    object QrProtocoNFSePrestaCnpj: TWideStringField
      FieldName = 'PrestaCnpj'
      Size = 18
    end
    object QrProtocoNFSePrestaCpf: TWideStringField
      FieldName = 'PrestaCpf'
      Size = 18
    end
    object QrProtocoNFSePrestaInscricaoMunicipal: TWideStringField
      FieldName = 'PrestaInscricaoMunicipal'
      Size = 18
    end
    object QrProtocoNFSeTomaRazaoSocial: TWideStringField
      FieldName = 'TomaRazaoSocial'
      Size = 150
    end
    object QrProtocoNFSeNOMEENT_MAIL: TWideStringField
      FieldName = 'NOMEENT_MAIL'
      Size = 100
    end
    object QrProtocoNFSeId_Contato: TIntegerField
      FieldName = 'Id_Contato'
    end
    object QrProtocoNFSeTelefone: TWideStringField
      FieldName = 'Telefone'
    end
    object QrProtocoNFSeContato: TWideStringField
      FieldName = 'Contato'
      Size = 30
    end
    object QrProtocoNFSeTelefone_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'Telefone_TXT'
      Size = 30
      Calculated = True
    end
    object QrProtocoNFSeNfsCodigoVerificacao: TWideStringField
      FieldName = 'NfsCodigoVerificacao'
      Size = 9
    end
  end
  object DsProtocoNFSe: TDataSource
    DataSet = QrProtocoNFSe
    Left = 432
    Top = 332
  end
  object CSTabSheetChamou: TdmkCompoStore
    Left = 612
    Top = 331
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 640
    Top = 331
  end
  object PMProtocolo: TPopupMenu
    OnPopup = PMProtocoloPopup
    Left = 90
    Top = 346
    object Gera1: TMenuItem
      Caption = '&Gera protocolo'
      object Todosabertos1: TMenuItem
        Caption = '&Todos abertos'
        OnClick = Todosabertos1Click
      end
      object Abertosseleciondos1: TMenuItem
        Caption = '&Aberto(s) seleciondo(s)'
        OnClick = Abertosseleciondos1Click
      end
    end
    object Desfazprotocolo1: TMenuItem
      Caption = '&Desfaz protocolo(s) selecionado(s)'
      OnClick = Desfazprotocolo1Click
    end
    object MenuItem1: TMenuItem
      Caption = '-'
    end
    object Localizaprotocolo1: TMenuItem
      Caption = '&Localiza lote'
      OnClick = Localizaprotocolo1Click
    end
    object N3: TMenuItem
      Caption = '-'
    end
    object Visualizaconfirmaoderecebimentodanotafiscalatual1: TMenuItem
      Caption = '&Visualiza confirma'#231#227'o de recebimento da nota fiscal atual'
    end
  end
  object QrNFSeCodVerifi: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT NfsCodigoVerificacao'
      'FROM nfsenfscab'
      'WHERE Empresa=:P0'
      'AND Ambiente=:P1'
      'AND NfsNumero=:P2'
      'AND NfsRpsIDSerie=:P3')
    Left = 736
    Top = 260
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
    object QrNFSeCodVerifiNfsCodigoVerificacao: TWideStringField
      FieldName = 'NfsCodigoVerificacao'
      Size = 9
    end
  end
  object PMEnvia: TPopupMenu
    Left = 560
    Top = 400
    object SelecionadoNOenviados1: TMenuItem
      Caption = 'Selecionado &n'#227'o enviados por e-mail'
      OnClick = SelecionadoNOenviados1Click
    end
    object Selecionados1: TMenuItem
      Caption = '&Selecionados por e-mail'
      OnClick = Selecionados1Click
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object SelecionadoporWhatsApp1: TMenuItem
      Caption = 'Selecionado por &WhatsApp'
      OnClick = SelecionadoporWhatsApp1Click
    end
    object N2: TMenuItem
      Caption = '-'
    end
    object Abrircadastrodocliente1: TMenuItem
      Caption = '&Abrir cadastro do cliente'
      OnClick = Abrircadastrodocliente1Click
    end
  end
end
