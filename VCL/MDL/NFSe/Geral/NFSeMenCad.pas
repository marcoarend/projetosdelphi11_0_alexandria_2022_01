unit NFSeMenCad;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, Variants,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnMLAGeral, UnInternalConsts,
  dmkImage, dmkCheckBox, UnDmkEnums, UnProjGroup_Consts;

type
  TFmNFSeMenCad = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    QrClientes: TmySQLQuery;
    QrClientesCodigo: TIntegerField;
    QrClientesNOMEENTIDADE: TWideStringField;
    DsClientes: TDataSource;
    QrNFSeSrvCad: TmySQLQuery;
    QrNFSeSrvCadCodigo: TIntegerField;
    QrNFSeSrvCadNome: TWideStringField;
    DsNFSeSrvCad: TDataSource;
    QrIntermediarios: TmySQLQuery;
    QrIntermediariosCodigo: TIntegerField;
    QrIntermediariosNOMEENTIDADE: TWideStringField;
    DsIntermediarios: TDataSource;
    Panel5: TPanel;
    Label9: TLabel;
    Label17: TLabel;
    Label34: TLabel;
    Label19: TLabel;
    LaContrato: TLabel;
    Label16: TLabel;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    EdIntermed: TdmkEditCB;
    CBIntermed: TdmkDBLookupComboBox;
    CBCliente: TdmkDBLookupComboBox;
    EdCliente: TdmkEditCB;
    EdNFSeSrvCad: TdmkEditCB;
    CBNFSeSrvCad: TdmkDBLookupComboBox;
    EdContrato: TdmkEditCB;
    CBContrato: TdmkDBLookupComboBox;
    EdCodigo: TdmkEdit;
    Panel6: TPanel;
    GroupBox2: TGroupBox;
    PanelFinanceiro: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    EdGLCarteira: TdmkEditCB;
    EdGLConta: TdmkEditCB;
    CBGLConta: TdmkDBLookupComboBox;
    CBGLCarteira: TdmkDBLookupComboBox;
    QrContratos: TmySQLQuery;
    DsContratos: TDataSource;
    QrContratosCODIGO: TIntegerField;
    QrContratosNOME: TWideStringField;
    Panel8: TPanel;
    QrCarteiras: TmySQLQuery;
    DsCarteiras: TDataSource;
    DsContas: TDataSource;
    QrContas: TmySQLQuery;
    QrContasCodigo: TIntegerField;
    QrContasNome: TWideStringField;
    QrCarteirasCodigo: TIntegerField;
    QrCarteirasNome: TWideStringField;
    CkGBGerarBol: TdmkCheckBox;
    Label5: TLabel;
    CkContinuar: TCheckBox;
    GroupBox3: TGroupBox;
    Panel9: TPanel;
    LaDiaFat: TLabel;
    LaValor: TLabel;
    EdDiaFat: TdmkEdit;
    EdValor: TdmkEdit;
    RGIncreCompet: TRadioGroup;
    GroupBox4: TGroupBox;
    Panel10: TPanel;
    Label7: TLabel;
    Label8: TLabel;
    TPVigenDtIni: TdmkEditDateTimePicker;
    TPVigenDtFim: TdmkEditDateTimePicker;
    SpeedButton1: TSpeedButton;
    QrLoc: TmySQLQuery;
    CkGLGerarLct: TdmkCheckBox;
    CkAtivo: TdmkCheckBox;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure CkGLGerarLctClick(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure EdContratoChange(Sender: TObject);
    procedure EdEmpresaChange(Sender: TObject);
    procedure EdClienteChange(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
  private
    { Private declarations }
    function  VerificaConfiguracaoExiste(Empresa, Intermed, Tomador,
                RegraFiscal, Contrato: Integer): Integer;
    procedure ConfiguraContrato(Contrato: Integer);
    {$IfNDef sCNTR}
    procedure ReopenContratos(Contratada, Contratante: Integer);
    {$EndIf}
  public
    { Public declarations }
    FQrNFSEMenCab: TmySQLQuery
  end;

  var
  FmNFSeMenCad: TFmNFSeMenCad;

implementation

uses UnMyObjects, Module, ModuleGeral, UMySQLModule, DmkDAC_PF, NFSe_PF_0000,
  MyDBCheck, MyListas, UnDmkProcFunc, UnGrl_Geral;

{$R *.DFM}

procedure TFmNFSeMenCad.BtOKClick(Sender: TObject);
var
  VigenDtIni, VigenDtFim: String;
  Codigo, Empresa, Cliente, Contrato, Intermed, NFSeSrvCad, DiaFat, IncreCompet,
  GLGerarLct, GLCarteira, GLConta, GBGerarBol, Existe, Ativo: Integer;
  Valor: Double;
begin
  if MyObjects.FIC(EdEmpresa.ValueVariant=0, EdEmpresa, 'Informe a empresa!') then
    Exit;
  Empresa    := DModG.QrEmpresasCodigo.Value;
  Codigo     := EdCodigo.ValueVariant;
  Cliente    := EdCliente.ValueVariant;
  Contrato   := EdContrato.ValueVariant;
  Intermed   := EdIntermed.ValueVariant;
  NFSeSrvCad := EdNFSeSrvCad.ValueVariant;
  DiaFat     := EdDiaFat.ValueVariant;
  GLGerarLct := Geral.BoolToInt(CkGLGerarLct.Checked);
  GBGerarBol := Geral.BoolToInt(CkGBGerarBol.Checked);
  GLCarteira := EdGLCarteira.ValueVariant;
  GLConta    := EdGLConta.ValueVariant;
  VigenDtIni := Geral.FDT(TPVigenDtIni.Date, 1);
  VigenDtFim := Geral.FDT(TPVigenDtFim.Date, 1);
  Ativo      := Geral.BoolToInt(CkAtivo.Checked);
  //
  if MyObjects.FIC(Cliente = 0, EdCliente, 'Informe o cliente!') then Exit;
  if MyObjects.FIC(NFSeSrvCad = 0, EdNFSeSrvCad, 'Informe a regra fiscal!') then Exit;
  if MyObjects.FIC(VigenDtIni = '1899-12-30', TPVigenDtIni, 'Informe a data inicial!') then Exit;
  //
  case RGIncreCompet.ItemIndex of
      0: IncreCompet := -1;
      1: IncreCompet := 0;
    else IncreCompet := 1;
  end;
  //
  if Contrato <> 0 then
    Valor := 0
  else
    Valor := EdValor.ValueVariant;
  //
  if MyObjects.FIC(NFSeSrvCad = 0, EdNFSeSrvCad, 'Informe a regra fiscal!') then
    Exit;
  if MyObjects.FIC(Cliente = 0, EdCliente, 'Informe o cliente!') then
    Exit;
  if GLGerarLct = 1 then
  begin
    if MyObjects.FIC(GLCarteira = 0, EdGLCarteira,
    'Informe a carteira para emiss�o dos lan�amentos!') then
      Exit;
    if MyObjects.FIC(GLConta = 0, EdGLConta,
    'Informe a conta (plano de contas) para emiss�o dos lan�amentos!') then
      Exit;
  end;
  //
  Codigo := UMyMod.BPGS1I32(
    'nfsemencab', 'Codigo', '', '', tsPos, ImgTipo.SQLType, Codigo);
  //
  Existe := VerificaConfiguracaoExiste(Empresa, Intermed, Cliente, NFSeSrvCad, Contrato);
  //
  if (Existe <> 0) and (Existe <> Codigo) then
  begin
    if Geral.MB_Pergunta('Existe uma configura��o ID n�mero ' +
      Geral.FF0(Existe) + ' com os seguintes par�metros:' + sLineBreak +
      'Empresa = ' + Geral.FF0(DModG.QrEmpresasFilial.Value) + sLineBreak +
      'Intermedi�rio = ' + Geral.FF0(Intermed) + sLineBreak +
      'Tomador = ' + Geral.FF0(Cliente) + sLineBreak +
      'Regra fiscal = ' + Geral.FF0(NFSeSrvCad) + sLineBreak +
      'Contrato = ' + Geral.FF0(Contrato) + sLineBreak + sLineBreak +
      'Deseja continuar mesmo assim?') <> ID_YES then
    begin
      Exit;
    end;
  end;
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'nfsemencab', False, [
  'Empresa', 'Cliente', 'Contrato',
  'Intermed', 'NFSeSrvCad', 'DiaFat',
  'IncreCompet', 'Valor', 'GLGerarLct',
  'GLCarteira', 'GLConta', 'VigenDtIni',
  'VigenDtFim', 'GBGerarBol', 'Ativo'], [
  'Codigo'], [
  Empresa, Cliente, Contrato,
  Intermed, NFSeSrvCad, DiaFat,
  IncreCompet, Valor, GLGerarLct,
  GLCarteira, GLConta, VigenDtIni,
  VigenDtFim, GBGerarBol, Ativo], [
  Codigo], True) then
  begin
    if FQrNFSEMenCab <> nil then
    begin
      if (FQrNFSEMenCab.State <> dsInactive) and (FQrNFSEMenCab.RecordCount > 0) then
      begin
        UnDmkDAC_PF.AbreQuery(FQrNFSEMenCab, Dmod.MyDB);
        FQrNFSEMenCab.Locate('Codigo', Codigo, []);
      end;
    end;
    //
    if CkContinuar.Checked then
    begin
      ImgTipo.SQLType           := stIns;
      EdIntermed.ValueVariant   := 0;
      CBIntermed.KeyValue       := Null;
      EdCliente.ValueVariant    := 0;
      CBCliente.KeyValue        := Null;
      EdNFSeSrvCad.ValueVariant := 0;
      CBNFSeSrvCad.KeyValue     := Null;
      EdContrato.ValueVariant   := 0;
      CBContrato.KeyValue       := Null;
      EdValor.ValueVariant      := 0;
      TPVigenDtIni.Date         := 0;
      TPVigenDtFim.Date         := 0;
      CkAtivo.Checked           := True;
      //
      EdEmpresa.SetFocus;
    end else
      Close;
  end;
end;

procedure TFmNFSeMenCad.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmNFSeMenCad.CkGLGerarLctClick(Sender: TObject);
begin
  PanelFinanceiro.Visible := CkGLGerarLct.Checked;
end;

procedure TFmNFSeMenCad.ConfiguraContrato(Contrato: Integer);
begin
  if Contrato <> 0 then
  begin
    LaDiaFat.Visible := False;
    EdDiaFat.Visible := False;
    LaValor.Visible  := False;
    EdValor.Visible  := False;
  end else
  begin
    LaDiaFat.Visible := False; //N�o usa
    EdDiaFat.Visible := False; //N�o usa
    LaValor.Visible  := True;
    EdValor.Visible  := True;
  end;
end;

procedure TFmNFSeMenCad.EdClienteChange(Sender: TObject);
begin
  {$IfNDef sCNTR}
    ReopenContratos(DModG.QrEmpresasCodigo.Value, EdCliente.ValueVariant);
  {$EndIf}
end;

procedure TFmNFSeMenCad.EdContratoChange(Sender: TObject);
begin
  ConfiguraContrato(EdContrato.ValueVariant);
end;

procedure TFmNFSeMenCad.EdEmpresaChange(Sender: TObject);
begin
  {$IfNDef sCNTR}
    ReopenContratos(DModG.QrEmpresasCodigo.Value, EdCliente.ValueVariant);
  {$EndIf}
end;

procedure TFmNFSeMenCad.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

{$IfNDef sCNTR}
procedure TFmNFSeMenCad.ReopenContratos(Contratada, Contratante: Integer);
var
  Fld_Codigo, Fld_Nome, Fld_Tabela: String;
begin
  if VAR_TemContratoMensalidade_FldCodigo <> '' then
    Fld_Codigo := VAR_TemContratoMensalidade_FldCodigo
  else
    Fld_Codigo := 'Codigo';
  if VAR_TemContratoMensalidade_FldNome <> '' then
    Fld_Nome := VAR_TemContratoMensalidade_FldNome
  else
    Fld_Nome := 'Nome';
  if VAR_TemContratoMensalidade_TabNome <> '' then
    Fld_Tabela := VAR_TemContratoMensalidade_TabNome
  else
    Fld_Tabela := 'contratos';
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrContratos, Dmod.MyDB, [
  'SELECT ' + Fld_Codigo + ' CODIGO, ',
  Fld_Nome + ' NOME ',
  'FROM ' + Fld_Tabela,
  'WHERE Ativo = 1 ',
  'AND Contratada=' + Geral.FF0(Contratada),
  'AND Contratante=' + Geral.FF0(Contratante),
  'ORDER BY NOME ',
  '']);
end;
{$EndIf}

function TFmNFSeMenCad.VerificaConfiguracaoExiste(Empresa, Intermed, Tomador,
  RegraFiscal, Contrato: Integer): Integer;
begin
  Result := 0;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrLoc, Dmod.MyDB, [
    'SELECT Codigo ',
    'FROM nfsemencab ',
    'WHERE Empresa=' + Geral.FF0(Empresa),
    'AND Intermed=' + Geral.FF0(Intermed),
    'AND Cliente=' + Geral.FF0(Tomador),
    'AND NFSeSrvCad=' + Geral.FF0(RegraFiscal),
    'AND Contrato=' + Geral.FF0(Contrato),
    '']);
  if QrLoc.RecordCount > 0 then
    Result := QrLoc.FieldByName('Codigo').AsInteger;
end;

procedure TFmNFSeMenCad.FormCreate(Sender: TObject);
var
  TemCNTR: Boolean;
begin
  ImgTipo.SQLType := stLok;
  //
  CBEmpresa.ListSource := DModG.DsEmpresas;
  EdEmpresa.ValueVariant  := DmodG.QrFiliLogFilial.Value;
  CBEmpresa.KeyValue      := DmodG.QrFiliLogFilial.Value;
  //
  UMyMod.AbreQuery(QrClientes, Dmod.MyDB);
  UMyMod.AbreQuery(QrIntermediarios, Dmod.MyDB);
  UMyMod.AbreQuery(QrNFSeSrvCad, Dmod.MyDB);
  UMyMod.AbreQuery(QrCarteiras, Dmod.MyDB);
  UMyMod.AbreQuery(QrContas, Dmod.MyDB);
  //
  QrContratos.Close;
  //
  {$IfNDef sCNTR}
    TemCNTR := Grl_Geral.LiberaModulo(CO_DMKID_APP, Grl_Geral.ObtemSiglaModulo(mdlappContratos),
                 DModG.QrMaster.FieldByName('HabilModulos').AsString);
  {$Else}
    TemCNTR := False;
  {$EndIf}
  LaContrato.Visible := TemCNTR;
  EdContrato.Visible := TemCNTR;
  CBContrato.Visible := TemCNTR;
  TPVigenDtIni.date  := 0;
  TPVigenDtFim.date  := 0;
  //
  //N�o pode escolher o dia no hora de gerar a nota fiscal
  LaDiaFat.Visible := False;
  EdDiaFat.Visible := False;
  //N�o est� sendo usado
  CkGBGerarBol.Visible := False;
end;

procedure TFmNFSeMenCad.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmNFSeMenCad.SpeedButton1Click(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  //
  UnNFSe_PF_0000.MostraFormNFSeSrvCad(EdNFSeSrvCad.ValueVariant);
  //
  if VAR_CADASTRO <> 0 then
  begin
    UMyMod.SetaCodigoPesquisado(EdNFSeSrvCad, CBNFSeSrvCad, QrNFSeSrvCad, VAR_CADASTRO);
    //
    EdNFSeSrvCad.SetFocus;
  end;
end;

end.
