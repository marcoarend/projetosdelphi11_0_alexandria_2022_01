unit NFSeMenRnw;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkDBGridZTO, mySQLDbTables;

type
  TFmNFSeMenRnw = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    QrNFSeMenCab: TmySQLQuery;
    QrNFSeMenCabCodigo: TIntegerField;
    QrNFSeMenCabEmpresa: TIntegerField;
    QrNFSeMenCabCliente: TIntegerField;
    QrNFSeMenCabContrato: TIntegerField;
    QrNFSeMenCabIntermed: TIntegerField;
    QrNFSeMenCabNFSeSrvCad: TIntegerField;
    QrNFSeMenCabDiaFat: TSmallintField;
    QrNFSeMenCabIncreCompet: TSmallintField;
    QrNFSeMenCabValor: TFloatField;
    QrNFSeMenCabGLGerarLct: TSmallintField;
    QrNFSeMenCabGLCarteira: TIntegerField;
    QrNFSeMenCabGLConta: TIntegerField;
    QrNFSeMenCabVigenDtIni: TDateField;
    QrNFSeMenCabVigenDtFim: TDateField;
    QrNFSeMenCabLk: TIntegerField;
    QrNFSeMenCabDataCad: TDateField;
    QrNFSeMenCabDataAlt: TDateField;
    QrNFSeMenCabUserCad: TIntegerField;
    QrNFSeMenCabUserAlt: TIntegerField;
    QrNFSeMenCabAlterWeb: TSmallintField;
    QrNFSeMenCabAtivo: TSmallintField;
    QrNFSeMenCabNO_SRV: TWideStringField;
    QrNFSeMenCabNO_CRT: TWideStringField;
    QrNFSeMenCabNO_CTA: TWideStringField;
    QrNFSeMenCabNO_CLI: TWideStringField;
    QrNFSeMenCabNO_INT: TWideStringField;
    QrNFSeMenCabGBGerarBol: TSmallintField;
    QrNFSeMenCabSN_LCT_TXT: TWideStringField;
    QrNFSeMenCabSN_BOL_TXT: TWideStringField;
    DsNFSeMenCab: TDataSource;
    DBGrid1: TdmkDBGridZTO;
    LaTotal: TLabel;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure DBGrid1DblClick(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure QrNFSeMenCabAfterScroll(DataSet: TDataSet);
    procedure QrNFSeMenCabBeforeClose(DataSet: TDataSet);
  private
    { Private declarations }
    procedure MostraNFSeMenCad();
  public
    { Public declarations }
    FHoje: TDateTime;
    procedure ReopenNFSeMenCab(Hoje: TDateTime);
  end;

  var
  FmNFSeMenRnw: TFmNFSeMenRnw;

implementation

uses UnMyObjects, Module, ModuleGeral, DmkDAC_PF, UnDmkProcFunc, MyDBCheck,
  {$IfNDef SNoti} UnitNotificacoesEdit, {$EndIf}
  NFSeMenCad;

{$R *.DFM}

procedure TFmNFSeMenRnw.BtOKClick(Sender: TObject);
begin
  MostraNFSeMenCad();
end;

procedure TFmNFSeMenRnw.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmNFSeMenRnw.DBGrid1DblClick(Sender: TObject);
begin
  MostraNFSeMenCad();
end;

procedure TFmNFSeMenRnw.MostraNFSeMenCad();
const
  SQLType = stUpd;
begin
  if DBCheck.CriaFm(TFmNFSeMenCad, FmNFSeMenCad, afmoNegarComAviso) then
  begin
    FmNFSeMenCad.ImgTipo.SQLType := SQLType;
    with FmNFSeMenCad do
    begin
      FQrNFSEMenCab := QrNFSEMenCab;
      //
      if SQLType = stUpd then
      begin
        EdCodigo.ValueVariant     := QrNFSEMenCabCodigo.Value;
        EdEmpresa.ValueVariant    := DModG.ObtemFilialDeEntidade(QrNFSEMenCabEmpresa.Value);
        EdCliente.ValueVariant    := QrNFSEMenCabCliente.Value;
        EdContrato.ValueVariant   := QrNFSEMenCabContrato.Value;
        EdIntermed.ValueVariant   := QrNFSEMenCabIntermed.Value;
        EdNFSeSrvCad.ValueVariant := QrNFSEMenCabNFSeSrvCad.Value;
        EdDiaFat.ValueVariant     := QrNFSEMenCabDiaFat.Value;
        EdValor.ValueVariant      := QrNFSEMenCabValor.Value;
        CkGLGerarLct.Checked      := Geral.IntToBool(QrNFSEMenCabGLGerarLct.Value);
        EdGLCarteira.ValueVariant := QrNFSEMenCabGLCarteira.Value;
        EdGLConta.ValueVariant    := QrNFSEMenCabGLConta.Value;
        TPVigenDtIni.Date         := QrNFSEMenCabVigenDtIni.Value;
        TPVigenDtFim.Date         := QrNFSEMenCabVigenDtFim.Value;
        CkGBGerarBol.Checked      := Geral.IntToBool(QrNFSEMenCabGBGerarBol.Value);
        CkAtivo.Checked           := Geral.IntToBool(QrNFSEMenCabAtivo.Value);
      end;
      ShowModal;
      Destroy;
      //
      if FHoje <> 0 then
        ReopenNFSeMenCab(FHoje);
    end;
  end;
end;

procedure TFmNFSeMenRnw.QrNFSeMenCabAfterScroll(DataSet: TDataSet);
begin
  LaTotal.Caption := 'Total de itens: ' + Geral.FF0(QrNFSeMenCab.RecordCount)
end;

procedure TFmNFSeMenRnw.QrNFSeMenCabBeforeClose(DataSet: TDataSet);
begin
  LaTotal.Caption := '';
end;

procedure TFmNFSeMenRnw.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmNFSeMenRnw.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  FHoje := 0;
end;

procedure TFmNFSeMenRnw.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmNFSeMenRnw.ReopenNFSeMenCab(Hoje: TDateTime);
var
  Data: TDateTime;
  SN_LCT_TXT, SN_BOL_TXT,
  DataFim: String;
begin
  Data := Geral.UltimoDiaDoMes(Hoje);
  DataFim := Geral.FDT(Data + 1, 1);
  //
  SN_LCT_TXT := dmkPF.ArrayToTexto('GLGerarLct', 'SN_LCT_TXT', pvPos, True,
    ['N�O', 'SIM', '???']);
  SN_BOL_TXT := dmkPF.ArrayToTexto('GBGerarBol', 'SN_BOL_TXT', pvPos, True,
    ['N�O', 'SIM', '???']);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrNFSeMenCab, Dmod.MyDB, [
    'SELECT nmc.*, nsc.Nome NO_SRV, ',
    'car.Nome NO_CRT, cta.Nome NO_CTA, ',
    SN_LCT_TXT,
    SN_BOL_TXT,
    'IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NO_CLI, ',
    'IF(ntm.Tipo=0, ntm.RazaoSocial, ntm.Nome) NO_INT ',
    'FROM nfsemencab nmc ',
    'LEFT JOIN entidades cli ON cli.Codigo=nmc.Cliente ',
    'LEFT JOIN entidades ntm ON ntm.Codigo=nmc.Intermed ',
    'LEFT JOIN nfsesrvcad nsc ON nsc.Codigo=nmc.NFSeSrvCad ',
    'LEFT JOIN carteiras car ON car.Codigo=nmc.GLCarteira ',
    'LEFT JOIN contas cta ON cta.Codigo=nmc.GLConta ',
    'WHERE nmc.Ativo=1 ',
    'AND nmc.VigenDtIni < SYSDATE() ',
    'AND nmc.VigenDtFim BETWEEN "1900-01-01" AND "' + DataFim + '" ',
    '']);
  {$IfNDef SNoti}
  if QrNFSeMenCab.RecordCount > 0 then
    UnNotificacoesEdit.CriaNotificacaoPublica(Dmod.MyDB, nftNFSeMenVencid, DmodG.ObtemAgora(True))
  else
    UnNotificacoesEdit.ExcluiNotificacao(Dmod.MyDB, Integer(nftNFSeMenVencid));
  {$EndIf}
end;

end.
