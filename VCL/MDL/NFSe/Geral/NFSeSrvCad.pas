unit NFSeSrvCad;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2,
  ResIntStrings, UnMLAGeral, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, dmkGeral,
  dmkPermissoes, dmkEdit, dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup,
  unDmkProcFunc, dmkDBLookupComboBox, dmkEditCB, dmkMemo, dmkLabelRotate,
  dmkCheckBox, UnDMkEnums;

type
  TFmNFSeSrvCad = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    BtExclui: TBitBtn;
    BtAltera: TBitBtn;
    BtInclui: TBitBtn;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    QrNFSeSrvCad: TmySQLQuery;
    DsNFSeSrvCad: TDataSource;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    Label9: TLabel;
    EdNome: TdmkEdit;
    Label1: TLabel;
    DBEdCodigo: TdmkDBEdit;
    Label2: TLabel;
    DBEdNome: TdmkDBEdit;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    QrNFSeSrvCadCodigo: TIntegerField;
    QrNFSeSrvCadNome: TWideStringField;
    QrNFSeSrvCadLk: TIntegerField;
    QrNFSeSrvCadDataCad: TDateField;
    QrNFSeSrvCadDataAlt: TDateField;
    QrNFSeSrvCadUserCad: TIntegerField;
    QrNFSeSrvCadUserAlt: TIntegerField;
    QrNFSeSrvCadAlterWeb: TSmallintField;
    QrNFSeSrvCadAtivo: TSmallintField;
    QrNFSeSrvCadCodigoTributacaoMunicipio: TWideStringField;
    Label4: TLabel;
    EdCodigoTributacaoMunicipio: TdmkEdit;
    QrNFSeSrvCadResponsavelRetencao: TSmallintField;
    QrNFSeSrvCadItemListaServico: TWideStringField;
    QrNFSeSrvCadCodigoCnae: TWideStringField;
    QrNFSeSrvCadNumeroProcesso: TWideStringField;
    QrNFSeSrvCadQuemPagaISS: TSmallintField;
    QrNFSeSrvCadExigibilidadeIss: TSmallintField;
    QrListServ: TmySQLQuery;
    QrListServNome: TWideStringField;
    DsListServ: TDataSource;
    Label18: TLabel;
    EdItemListaServico: TdmkEditCB;
    CBItemListaServico: TdmkDBLookupComboBox;
    SbListServN: TSpeedButton;
    QrCNAE21Cad: TmySQLQuery;
    DsCNAE21Cad: TDataSource;
    Label5: TLabel;
    EdCodigoCnae: TdmkEditCB;
    CBCodigoCnae: TdmkDBLookupComboBox;
    SbCodigoCnae: TSpeedButton;
    QrTsResponsavelRetencao: TmySQLQuery;
    QrTsResponsavelRetencaoCodigo: TIntegerField;
    QrTsResponsavelRetencaoNome: TWideStringField;
    DsTsResponsavelRetencao: TDataSource;
    GroupBox5: TGroupBox;
    SbResponsavelRetencao: TSpeedButton;
    Label35: TLabel;
    EdResponsavelRetencao: TdmkEditCB;
    CBResponsavelRetencao: TdmkDBLookupComboBox;
    DsTsExigibilidadeIss: TDataSource;
    QrTsExigibilidadeIss: TmySQLQuery;
    QrTsExigibilidadeIssCodigo: TIntegerField;
    QrTsExigibilidadeIssNome: TWideStringField;
    Label6: TLabel;
    EdNumeroProcesso: TdmkEdit;
    GroupBox1: TGroupBox;
    SpeedButton5: TSpeedButton;
    Label8: TLabel;
    EdQuemPagaISS: TdmkEditCB;
    CBQuemPagaISS: TdmkDBLookupComboBox;
    GroupBox2: TGroupBox;
    SpeedButton6: TSpeedButton;
    Label10: TLabel;
    EdExigibilidadeIss: TdmkEditCB;
    CBExigibilidadeIss: TdmkDBLookupComboBox;
    QrMyMunicipioIncidencia: TmySQLQuery;
    DsMyMunicipioIncidencia: TDataSource;
    QrMyMunicipioIncidenciaCodigo: TIntegerField;
    QrMyMunicipioIncidenciaNome: TWideStringField;
    Label11: TLabel;
    Label12: TLabel;
    GroupBox3: TGroupBox;
    Label14: TLabel;
    GroupBox4: TGroupBox;
    Label15: TLabel;
    GroupBox6: TGroupBox;
    Label16: TLabel;
    QrNFSeSrvCadNO_QuemPagaISS: TWideStringField;
    QrNFSeSrvCadNO_ResponsavelRetencao: TWideStringField;
    QrNFSeSrvCadNO_ItemListaServico: TWideStringField;
    QrNFSeSrvCadNO_CodigoCnae: TWideStringField;
    QrNFSeSrvCadNO_ExibilidadeIss: TWideStringField;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    DBEdit4: TDBEdit;
    DBEdit5: TDBEdit;
    DBEdit6: TDBEdit;
    DBEdit7: TDBEdit;
    DBEdit8: TDBEdit;
    DBEdit9: TDBEdit;
    DBEdit10: TDBEdit;
    DBEdit11: TDBEdit;
    DBEdit12: TDBEdit;
    DBEdit13: TDBEdit;
    QrCNAE21CadNome: TWideStringField;
    QrCNAE21CadCodAlf: TWideStringField;
    QrListServCodAlf: TWideStringField;
    PnDiscriminacao: TPanel;
    dmkLabelRotate1: TdmkLabelRotate;
    MeDiscriminacao: TdmkMemo;
    PnDBDiscriminacao: TPanel;
    dmkLabelRotate2: TdmkLabelRotate;
    DBMemo1: TDBMemo;
    QrNFSeSrvCadDiscriminacao: TWideMemoField;
    GroupBox7: TGroupBox;
    SpeedButton7: TSpeedButton;
    Label19: TLabel;
    EdRegimeEspecialTributacao: TdmkEditCB;
    CBRegimeEspecialTributacao: TdmkDBLookupComboBox;
    QrTsRegimeEspecialTributacao: TmySQLQuery;
    DsTsRegimeEspecialTributacao: TDataSource;
    QrTsRegimeEspecialTributacaoCodigo: TIntegerField;
    QrTsRegimeEspecialTributacaoNome: TWideStringField;
    QrNFSeSrvCadRegimeEspecialTributacao: TSmallintField;
    QrNFSeSrvCadOptanteSimplesNacional: TSmallintField;
    QrNFSeSrvCadIncentivoFiscal: TSmallintField;
    GroupBox8: TGroupBox;
    Label20: TLabel;
    DBEdit14: TDBEdit;
    DBEdit15: TDBEdit;
    GroupBox9: TGroupBox;
    Label21: TLabel;
    DBEdit16: TDBEdit;
    DBEdit17: TDBEdit;
    GroupBox10: TGroupBox;
    Label22: TLabel;
    DBEdit18: TDBEdit;
    DBEdit19: TDBEdit;
    GroupBox11: TGroupBox;
    SpeedButton8: TSpeedButton;
    Label23: TLabel;
    EdOptanteSimplesNacional: TdmkEditCB;
    CBOptanteSimplesNacional: TdmkDBLookupComboBox;
    GroupBox12: TGroupBox;
    SpeedButton9: TSpeedButton;
    Label24: TLabel;
    EdIncentivoFiscal: TdmkEditCB;
    CBIncentivoFiscal: TdmkDBLookupComboBox;
    QrTsSimNao1: TmySQLQuery;
    DsTsSimNao1: TDataSource;
    QrTsSimNao1Codigo: TIntegerField;
    QrTsSimNao1Nome: TWideStringField;
    QrTsSimNao2: TmySQLQuery;
    DsTsSimNao2: TDataSource;
    QrTsSimNao2Codigo: TIntegerField;
    QrTsSimNao2Nome: TWideStringField;
    QrNFSeSrvCadNO_RegimeEspecialTributacao: TWideStringField;
    QrNFSeSrvCadNO_OptanteSimplesNacional: TWideStringField;
    QrNFSeSrvCadNO_IncentivoFiscal: TWideStringField;
    QrTsTipoRps: TmySQLQuery;
    QrTsTipoRpsCodigo: TIntegerField;
    QrTsTipoRpsNome: TWideStringField;
    DsTsTipoRps: TDataSource;
    Label28: TLabel;
    EdRpsTipo: TdmkEditCB;
    CBRpsTipo: TdmkDBLookupComboBox;
    SbRpsIDTipo: TSpeedButton;
    QrNFSeSrvCadRpsTipo: TSmallintField;
    QrNFSeSrvCadNO_RpsTipo: TWideStringField;
    Label13: TLabel;
    Label17: TLabel;
    Label25: TLabel;
    DBEdit20: TDBEdit;
    DBEdit21: TDBEdit;
    EdAliquota: TdmkEdit;
    Label3: TLabel;
    QrNFSeSrvCadAliquota: TFloatField;
    Label26: TLabel;
    DBEdit1: TDBEdit;
    QrNFSeSrvCadNaturezaOperacao: TIntegerField;
    QrNFSeSrvCadAlqPIS: TFloatField;
    QrNFSeSrvCadAlqCOFINS: TFloatField;
    QrNFSeSrvCadAlqINSS: TFloatField;
    QrNFSeSrvCadAlqIR: TFloatField;
    QrNFSeSrvCadAlqCSLL: TFloatField;
    QrNFSeSrvCadAlqOutrasRetencoes: TFloatField;
    GroupBox13: TGroupBox;
    Panel6: TPanel;
    Label27: TLabel;
    EdAlqPis: TdmkEdit;
    Label29: TLabel;
    EdAlqCofins: TdmkEdit;
    Label30: TLabel;
    EdAlqInss: TdmkEdit;
    Label31: TLabel;
    EdAlqIr: TdmkEdit;
    Label32: TLabel;
    EdAlqCsll: TdmkEdit;
    Label33: TLabel;
    EdAlqOutrasRetencoes: TdmkEdit;
    GroupBox14: TGroupBox;
    Panel7: TPanel;
    Label34: TLabel;
    Label36: TLabel;
    Label37: TLabel;
    Label38: TLabel;
    Label39: TLabel;
    Label40: TLabel;
    DBEdit22: TDBEdit;
    DBEdit23: TDBEdit;
    DBEdit24: TDBEdit;
    DBEdit25: TDBEdit;
    DBEdit26: TDBEdit;
    DBEdit27: TDBEdit;
    QrNFSeSrvCadPDFCompres: TSmallintField;
    QrNFSeSrvCadPDFEmbFont: TSmallintField;
    QrNFSeSrvCadPDFPrnOptm: TSmallintField;
    GroupBox15: TGroupBox;
    Panel8: TPanel;
    CkPDFCompres: TdmkCheckBox;
    CkPDFEmbFont: TdmkCheckBox;
    CkPDFPrnOptm: TdmkCheckBox;
    GroupBox16: TGroupBox;
    Panel9: TPanel;
    dmkCheckBox1: TDBCheckBox;
    dmkCheckBox2: TDBCheckBox;
    dmkCheckBox3: TDBCheckBox;
    QrNFSeSrvCadindFinal: TSmallintField;
    QrNFSeSrvCadImportado: TSmallintField;
    Panel10: TPanel;
    DBRadioGroup1: TDBRadioGroup;
    DBRadioGroup2: TDBRadioGroup;
    Panel11: TPanel;
    RG_indFinal: TdmkRadioGroup;
    RGImportado: TdmkRadioGroup;
    QrListServCodigo: TIntegerField;
    QrCNAE21CadCodigo: TIntegerField;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrNFSeSrvCadAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrNFSeSrvCadBeforeOpen(DataSet: TDataSet);
    procedure BtIncluiClick(Sender: TObject);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure SbCodigoCnaeClick(Sender: TObject);
    procedure SbListServNClick(Sender: TObject);
    procedure MeDiscriminacaoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SbImprimeClick(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure Va(Para: TVaiPara);
  public
    { Public declarations }
    procedure LocCod(Atual, Codigo: Integer);
  end;

var
  FmNFSeSrvCad: TFmNFSeSrvCad;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, CNAE21cad, MyDBCheck, ModuleGeral, NFSe_PF_0000;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmNFSeSrvCad.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmNFSeSrvCad.MeDiscriminacaoKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
 if Key in ([9, 10, 13]) then
   Key := 0;
end;

procedure TFmNFSeSrvCad.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrNFSeSrvCadCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmNFSeSrvCad.DefParams;
begin
  VAR_GOTOTABELA := 'nfsesrvcad';
  VAR_GOTOMYSQLTABLE := QrNFSeSrvCad;
  VAR_GOTONEG := gotoPos;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT mmi.Nome NO_QuemPagaISS,');
  VAR_SQLx.Add('rre.Nome NO_ResponsavelRetencao,');
  VAR_SQLx.Add('lsv.Nome NO_ItemListaServico,');
  VAR_SQLx.Add('c21.Nome NO_CodigoCnae,');
  VAR_SQLx.Add('eis.Nome NO_ExibilidadeIss, ');
  VAR_SQLx.Add('ret.Nome NO_RegimeEspecialTributacao,');
  VAR_SQLx.Add('sn1.Nome NO_OptanteSimplesNacional,');
  VAR_SQLx.Add('sn2.Nome NO_IncentivoFiscal, ');
  VAR_SQLx.Add('rpt.Nome NO_RpsTipo, nsc.*');
  VAR_SQLx.Add('FROM '+ TMeuDB +'.nfsesrvcad nsc');
  VAR_SQLx.Add('LEFT JOIN '+ DModG.AllID_DB.DatabaseName +'.mymunicipioincidencia mmi');
  VAR_SQLx.Add('  ON mmi.Codigo=nsc.QuemPagaISS');
  VAR_SQLx.Add('LEFT JOIN '+ DModG.AllID_DB.DatabaseName +'.tsresponsavelretencao rre');
  VAR_SQLx.Add('  ON rre.Codigo=nsc.ResponsavelRetencao');
  VAR_SQLx.Add('LEFT JOIN '+ DModG.AllID_DB.DatabaseName +'.tsexigibilidadeiss eis');
  VAR_SQLx.Add('  ON eis.Codigo=nsc.ExigibilidadeIss');
  VAR_SQLx.Add('LEFT JOIN '+ DModG.AllID_DB.DatabaseName +'.listserv lsv');
  VAR_SQLx.Add('  ON lsv.CodAlf=nsc.ItemListaServico');
  VAR_SQLx.Add('LEFT JOIN '+ DModG.AllID_DB.DatabaseName +'.cnae21cad c21');
  VAR_SQLx.Add('  ON c21.CodAlf=nsc.CodigoCnae');
  VAR_SQLx.Add('LEFT JOIN '+ DModG.AllID_DB.DatabaseName +'.tsregimeespecialtributacao ret');
  VAR_SQLx.Add('  ON ret.Codigo=nsc.RegimeEspecialTributacao');
  VAR_SQLx.Add('LEFT JOIN '+ DModG.AllID_DB.DatabaseName +'.tssimnao sn1');
  VAR_SQLx.Add('  ON sn1.Codigo=nsc.OptanteSimplesNacional');
  VAR_SQLx.Add('LEFT JOIN '+ DModG.AllID_DB.DatabaseName +'.tssimnao sn2');
  VAR_SQLx.Add('  ON sn2.Codigo=nsc.IncentivoFiscal');
  VAR_SQLx.Add('LEFT JOIN '+ DModG.AllID_DB.DatabaseName +'.tstiporps rpt');
  VAR_SQLx.Add('  ON rpt.Codigo=nsc.RpsTipo');
  VAR_SQLx.Add('');
  VAR_SQLx.Add('WHERE nsc.Codigo > 0');
  //
  VAR_SQL1.Add('AND nsc.Codigo=:P0');
  //
  //VAR_SQL2.Add('AND CodUsu=:P0');
  //
  VAR_SQLa.Add('AND nsc.Nome Like :P0');
  //
end;

procedure TFmNFSeSrvCad.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmNFSeSrvCad.QueryPrincipalAfterOpen;
begin
end;

procedure TFmNFSeSrvCad.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmNFSeSrvCad.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmNFSeSrvCad.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmNFSeSrvCad.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmNFSeSrvCad.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmNFSeSrvCad.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmNFSeSrvCad.BtAlteraClick(Sender: TObject);
begin
  if (QrNFSeSrvCad.State = dsInactive) or (QrNFSeSrvCad.RecordCount = 0) then Exit;
  //
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrNFSeSrvCad, [PnDados],
    [PnEdita], EdNome, ImgTipo, 'nfsesrvcad');
end;

procedure TFmNFSeSrvCad.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrNFSeSrvCadCodigo.Value;
  Close;
end;

procedure TFmNFSeSrvCad.BtConfirmaClick(Sender: TObject);
var
  Codigo: Integer;
  Nome: String;
begin
  Nome := EdNome.ValueVariant;
  if MyObjects.FIC(Length(Nome) = 0, EdNome, 'Defina uma descri��o!') then
    Exit;
  //
  if MyObjects.FIC(RG_indFinal.ItemIndex < 0, RG_indFinal,
    'Informe a opera��o com o consumidor final!') then Exit;
  //
  Codigo := UMyMod.BPGS1I32('nfsesrvcad', 'Codigo', '', '',
    tsPos, ImgTipo.SQLType, QrNFSeSrvCadCodigo.Value);
  if UMyMod.ExecSQLInsUpdPanel(ImgTipo.SQLType, Self, PnEdita,
    'nfsesrvcad', Codigo, Dmod.QrUpd, [PnEdita], [PnDados], ImgTipo, True) then
  begin
    LocCod(Codigo, Codigo);
  end;
end;

procedure TFmNFSeSrvCad.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := EdCodigo.ValueVariant;
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'nfsesrvcad', 'Codigo');
end;

procedure TFmNFSeSrvCad.BtIncluiClick(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrNFSeSrvCad, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'nfsesrvcad');
end;

procedure TFmNFSeSrvCad.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  PnDiscriminacao.Align := alClient;
  PnDBDiscriminacao.Align := alClient;
  CriaOForm;
  //
  UMyMod.AbreQuery(QrCNAE21cad, DmodG.AllID_DB);
  UMyMod.AbreQuery(QrTsTipoRps, DmodG.AllID_DB);
  UMyMod.AbreQuery(QrListServ, DmodG.AllID_DB);
  UMyMod.AbreQuery(QrTsResponsavelRetencao, DmodG.AllID_DB);
  UMyMod.AbreQuery(QrTsExigibilidadeIss, DmodG.AllID_DB);
  UMyMod.AbreQuery(QrTsRegimeEspecialTributacao, DmodG.AllID_DB);
  UMyMod.AbreQuery(QrMyMunicipioIncidencia, DmodG.AllID_DB);
  UMyMod.AbreQuery(QrTsSimNao1, DmodG.AllID_DB);
  UMyMod.AbreQuery(QrTsSimNao2, DmodG.AllID_DB);
end;

procedure TFmNFSeSrvCad.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrNFSeSrvCadCodigo.Value, LaRegistro.Caption);
end;

procedure TFmNFSeSrvCad.SbCodigoCnaeClick(Sender: TObject);
var
  CodAlf: String;
  Codigo: Integer;
begin
  VAR_CADASTROX := '';
  CodAlf        := EdCodigoCnae.ValueVariant;

  if CodAlf <> '' then
    Codigo := QrCNAE21CadCodigo.Value
  else
    Codigo := 0;

  UnNFSe_PF_0000.MostraFormCNAE21Cad(Codigo);

  UMyMod.AbreQuery(QrCNAE21Cad, DmodG.AllID_DB);

  if VAR_CADASTROX <> '' then
  begin
    UMyMod.SetaCodTxtPesquisado(EdCodigoCnae, CBCodigoCnae, QrCNAE21Cad, VAR_CADASTROX, 'CodAlf');
    EdCodigoCnae.SetFocus;
  end;
end;

procedure TFmNFSeSrvCad.SbImprimeClick(Sender: TObject);
begin
  Geral.MB_Aviso('Impress�o n�o dispon�vel para esta janela!');
end;

procedure TFmNFSeSrvCad.SbListServNClick(Sender: TObject);
var
  CodAlf: String;
  Codigo: Integer;
begin
  VAR_CADASTROX := '';
  CodAlf        := EdItemListaServico.ValueVariant;

  if CodAlf <> '' then
    Codigo := QrListServCodigo.Value
  else
    Codigo := 0;

  UnNFSe_PF_0000.MostraFormListServ(Codigo);

  UMyMod.AbreQuery(QrListServ, DmodG.AllID_DB);

  if VAR_CADASTROX <> '' then
  begin
    UMyMod.SetaCodTxtPesquisado(EdItemListaServico, CBItemListaServico,
      QrListServ, VAR_CADASTROX, 'CodAlf');
    EdItemListaServico.SetFocus;
  end;
end;

procedure TFmNFSeSrvCad.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmNFSeSrvCad.SbNovoClick(Sender: TObject);
begin
  //LaRegistro.Caption := GOTOy.CodUsu(QrNFSeSrvCadCodigo.Value, LaRegistro.Caption);
  Geral.MB_Aviso('Pesquisa n�o dispon�vel para esta janela!');
end;

procedure TFmNFSeSrvCad.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmNFSeSrvCad.QrNFSeSrvCadAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmNFSeSrvCad.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmNFSeSrvCad.SbQueryClick(Sender: TObject);
begin
  LocCod(QrNFSeSrvCadCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'nfsesrvcad', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmNFSeSrvCad.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmNFSeSrvCad.QrNFSeSrvCadBeforeOpen(DataSet: TDataSet);
begin
  QrNFSeSrvCadCodigo.DisplayFormat := FFormatFloat;
end;

end.

