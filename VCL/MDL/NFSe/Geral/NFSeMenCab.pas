unit NFSeMenCab;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, Variants,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnMLAGeral, UnInternalConsts,
  dmkImage, dmkDBGridZTO, dmkCompoStore, UnDmkEnums, frxClass, frxDBSet;

type
  TFmNFSeMenCab = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    LaTitulo1C: TLabel;
    Panel5: TPanel;
    Label9: TLabel;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    QrClientes: TmySQLQuery;
    QrClientesCodigo: TIntegerField;
    QrClientesNOMEENTIDADE: TWideStringField;
    DsClientes: TDataSource;
    QrListServ: TmySQLQuery;
    QrListServNome: TWideStringField;
    QrListServCodAlf: TWideStringField;
    DsListServ: TDataSource;
    QrNFSeSrvCad: TmySQLQuery;
    QrNFSeSrvCadCodigo: TIntegerField;
    QrNFSeSrvCadNome: TWideStringField;
    DsNFSeSrvCad: TDataSource;
    QrIntermediarios: TmySQLQuery;
    QrIntermediariosCodigo: TIntegerField;
    QrIntermediariosNOMEENTIDADE: TWideStringField;
    DsIntermediarios: TDataSource;
    Label17: TLabel;
    EdIntermediario: TdmkEditCB;
    CBIntermediario: TdmkDBLookupComboBox;
    Label34: TLabel;
    CBCliente: TdmkDBLookupComboBox;
    EdCliente: TdmkEditCB;
    Label19: TLabel;
    EdNFSeSrvCad: TdmkEditCB;
    CBNFSeSrvCad: TdmkDBLookupComboBox;
    Label18: TLabel;
    EdItemListaServico: TdmkEditCB;
    CBItemListaServico: TdmkDBLookupComboBox;
    BtReabre: TBitBtn;
    QrNFSeMenCab: TmySQLQuery;
    DsNFSeMenCab: TDataSource;
    BtInclui: TBitBtn;
    BtAltera: TBitBtn;
    BtExclui: TBitBtn;
    DBGrid1: TdmkDBGridZTO;
    CSTabSheetChamou: TdmkCompoStore;
    frxNFS_MENSA_001_01A: TfrxReport;
    SbImprime: TBitBtn;
    frxDsNFSeMenCab: TfrxDBDataset;
    CkVigenteEm: TCheckBox;
    TPVigenteEm: TdmkEditDateTimePicker;
    QrNFSeMenCabCodigo: TIntegerField;
    QrNFSeMenCabEmpresa: TIntegerField;
    QrNFSeMenCabCliente: TIntegerField;
    QrNFSeMenCabContrato: TIntegerField;
    QrNFSeMenCabIntermed: TIntegerField;
    QrNFSeMenCabNFSeSrvCad: TIntegerField;
    QrNFSeMenCabDiaFat: TSmallintField;
    QrNFSeMenCabIncreCompet: TSmallintField;
    QrNFSeMenCabValor: TFloatField;
    QrNFSeMenCabGLGerarLct: TSmallintField;
    QrNFSeMenCabGLCarteira: TIntegerField;
    QrNFSeMenCabGLConta: TIntegerField;
    QrNFSeMenCabVigenDtIni: TDateField;
    QrNFSeMenCabVigenDtFim: TDateField;
    QrNFSeMenCabLk: TIntegerField;
    QrNFSeMenCabDataCad: TDateField;
    QrNFSeMenCabDataAlt: TDateField;
    QrNFSeMenCabUserCad: TIntegerField;
    QrNFSeMenCabUserAlt: TIntegerField;
    QrNFSeMenCabAlterWeb: TSmallintField;
    QrNFSeMenCabAtivo: TSmallintField;
    QrNFSeMenCabNO_SRV: TWideStringField;
    QrNFSeMenCabNO_CRT: TWideStringField;
    QrNFSeMenCabNO_CTA: TWideStringField;
    QrNFSeMenCabNO_CLI: TWideStringField;
    QrNFSeMenCabNO_INT: TWideStringField;
    QrNFSeMenCabGBGerarBol: TSmallintField;
    QrNFSeMenCabSN_LCT_TXT: TWideStringField;
    QrNFSeMenCabSN_BOL_TXT: TWideStringField;
    QrNFSeMenCabValorNFSe: TFloatField;
    LaTotal: TLabel;
    BtSoma: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure EdEmpresaChange(Sender: TObject);
    procedure EdIntermediarioChange(Sender: TObject);
    procedure EdClienteChange(Sender: TObject);
    procedure EdNFSeSrvCadChange(Sender: TObject);
    procedure EdItemListaServicoChange(Sender: TObject);
    procedure BtReabreClick(Sender: TObject);
    procedure BtIncluiClick(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure QrNFSeMenCabCalcFields(DataSet: TDataSet);
    procedure QrNFSeMenCabAfterOpen(DataSet: TDataSet);
    procedure QrNFSeMenCabBeforeClose(DataSet: TDataSet);
    procedure BtExcluiClick(Sender: TObject);
    procedure QrNFSeMenCabVigenDtIniGetText(Sender: TField; var Text: string;
      DisplayText: Boolean);
    procedure QrNFSeMenCabVigenDtFimGetText(Sender: TField; var Text: string;
      DisplayText: Boolean);
    procedure FormShow(Sender: TObject);
    procedure frxNFS_MENSA_001_01AGetValue(const VarName: string;
      var Value: Variant);
    procedure SbImprimeClick(Sender: TObject);
    procedure QrNFSeMenCabAfterScroll(DataSet: TDataSet);
    procedure BtSomaClick(Sender: TObject);
  private
    { Private declarations }
    procedure FechaPesquisa();
    procedure MostraFormNFSEMensCad(SQLType: TSQLType);
    procedure ReopenNFSeMenCab(Codigo: Integer);

  public
    { Public declarations }
  end;

  var
  FmNFSeMenCab: TFmNFSeMenCab;

implementation

uses UnMyObjects, Module, ModuleGeral, UMySQLModule, DmkDAC_PF, UnDmkProcFunc,
  MyDBCheck, NFSeMenCad, UnGOTOy, MyGlyfs, Principal;

{$R *.DFM}

procedure TFmNFSeMenCab.BtAlteraClick(Sender: TObject);
begin
  if (QrNFSeMenCab.State <> dsInactive) and (QrNFSeMenCab.RecordCount > 0) then
    MostraFormNFSEMensCad(stUpd);
end;

procedure TFmNFSeMenCab.BtExcluiClick(Sender: TObject);
var
  Codigo: Integer;
begin
  if (QrNFSeMenCab.State <> dsInactive) and (QrNFSeMenCab.RecordCount > 0) then
  begin
    if UMyMod.ExcluiRegistroInt1('Confirma a exclus�o da configura��o de mensalidade de NFS-e selecionada?',
      'nfsemencab', 'Codigo', QrNFSeMenCabCodigo.Value, Dmod.MyDB) = ID_YES then
    begin
      Codigo := GOTOy.LocalizaPriorNextIntQr(QrNFSeMenCab, QrNFSeMenCabCodigo,
      //
      QrNFSeMenCabCodigo.Value);
      ReopenNFseMenCab(Codigo);
    end;
  end;
end;

procedure TFmNFSeMenCab.BtIncluiClick(Sender: TObject);
begin
  MostraFormNFSEMensCad(stIns);
end;

procedure TFmNFSeMenCab.BtReabreClick(Sender: TObject);
begin
  ReopenNFSeMenCab(0);
end;

procedure TFmNFSeMenCab.BtSaidaClick(Sender: TObject);
begin
  if TFmNFSeMenCab(Self).Owner is TApplication then
    Close
  else
    MyObjects.FormTDIFecha(TFmNFSeMenCab(Self), TTabSheet(CSTabSheetChamou.Component));
end;

procedure TFmNFSeMenCab.BtSomaClick(Sender: TObject);
var
  Valor: Double;
  i: Integer;
begin
  if (QrNFSeMenCab.State <> dsInactive) and (QrNFSeMenCab.RecordCount > 0) then
  begin
    Valor := 0;
    //
    with DBGrid1.DataSource.DataSet do
    begin
      for i:= 0 to DBGrid1.SelectedRows.Count - 1 do
      begin
        GotoBookmark(DBGrid1.SelectedRows.Items[i]);
        //
        Valor := Valor + QrNFSeMenCabValorNFSe.Value;
      end;
    end;
    Geral.MB_Aviso('Resultado:' + sLineBreak +
      'Total de itens selecionados: ' + Geral.FF0(DBGrid1.SelectedRows.Count) + sLineBreak +
      'Soma do valor: ' + Geral.TFT(FloatToStr(Valor), 2, siNegativo));
  end;
end;

procedure TFmNFSeMenCab.EdClienteChange(Sender: TObject);
begin
  FechaPesquisa();
end;

procedure TFmNFSeMenCab.EdEmpresaChange(Sender: TObject);
begin
  FechaPesquisa();
end;

procedure TFmNFSeMenCab.EdIntermediarioChange(Sender: TObject);
begin
  FechaPesquisa();
end;

procedure TFmNFSeMenCab.EdItemListaServicoChange(Sender: TObject);
begin
  FechaPesquisa();
end;

procedure TFmNFSeMenCab.EdNFSeSrvCadChange(Sender: TObject);
begin
  FechaPesquisa();
end;

procedure TFmNFSeMenCab.FechaPesquisa();
begin
  QrNFSeMenCab.Close;
end;

procedure TFmNFSeMenCab.FormActivate(Sender: TObject);
begin
  if TFmNFSeMenCab(Self).Owner is TApplication then
  begin
    MyObjects.CorIniComponente();
  end;
end;

procedure TFmNFSeMenCab.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  TPVigenteEm.Date := Date;
  //
  CBEmpresa.ListSource := DModG.DsEmpresas;
  EdEmpresa.ValueVariant  := DmodG.QrFiliLogFilial.Value;
  CBEmpresa.KeyValue      := DmodG.QrFiliLogFilial.Value;
  //
  UMyMod.AbreQuery(QrClientes, Dmod.MyDB);
  UMyMod.AbreQuery(QrIntermediarios, Dmod.MyDB);
  UMyMod.AbreQuery(QrNFSeSrvCad, Dmod.MyDB);
  UMyMod.AbreQuery(QrListServ, DmodG.AllID_DB);
  //
end;

procedure TFmNFSeMenCab.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmNFSeMenCab.FormShow(Sender: TObject);
begin
{$IfNDef cSkinRank} //Berlin
{$IfNDef cAlphaSkin} //Berlin
  FmMyGlyfs.DefineGlyfs(TForm(Sender));
{$EndIf}
{$EndIf}
{$IfDef cSkinRank} //Berlin
  if FmPrincipal.Sd1.Active then
    FmMyGlyfs.DefineGlyfsTDI(FmPrincipal.sd1, Sender);
{$EndIf}
{$IfDef cAlphaSkin} //Berlin
  if FmPrincipal.sSkinManager1.Active then
    FmMyGlyfs.DefineGlyfsTDI2(FmPrincipal.sSkinManager1, Sender);
{$EndIf}
end;

procedure TFmNFSeMenCab.frxNFS_MENSA_001_01AGetValue(const VarName: string;
  var Value: Variant);
var
  Ord, Txt: String;
  Nivel: Integer;
begin
  if VarName = 'VARF_NO_EMPRESA' then Value := CBEmpresa.Text
  else if VarName = 'VARF_VIGENTEEM' then
  begin
    if CkVigenteEm.Checked then
      Txt := Geral.FDT(TPVigenteEm.Date, 2)
    else
      Txt := '';
    Value := dmkPF.ParValueCodTxt('', Txt, Null, 'Qualquer data');
  end
  else if VarName = 'VARF_TOMADOR' then
    Value := dmkPF.ParValueCodTxt('', CBCliente.Text, EdCliente.ValueVariant)
  else if VarName = 'VARF_MinhaRegraFiscal' then
    Value := dmkPF.ParValueCodTxt('', CBNFSeSrvCad.Text, EdNFSeSrvCad.ValueVariant)
  else if VarName = 'VARF_LC_116_03' then
    Value := dmkPF.ParValueCodTxt('', CBItemListaServico.Text, EdItemListaServico.ValueVariant)
  else if VarName = 'VARF_INTERMEDIARIO' then
    Value := dmkPF.ParValueCodTxt('', CBIntermediario.Text, EdIntermediario.ValueVariant)
{
  else if VarName = 'VARF_CpaLin1MrgSup' then Value := Int(QrEntiCfgRel_01CpaLin1MrgSup.Value / VAR_frCM)
  else if VarName = 'VARF_CpaLin1AltLin' then Value := Int(QrEntiCfgRel_01CpaLin1AltLin.Value / VAR_frCM)
  else if VarName = 'VARF_CpaLin1AliTex' then Value := QrEntiCfgRel_01CpaLin1AliTex.Value
  //Linha 2
  else if VarName = 'VARF_CpaLin2FonNom' then Value := QrEntiCfgRel_01CpaLin2FonNom.Value
  else if VarName = 'VARF_CpaLin2FonTam' then Value := QrEntiCfgRel_01CpaLin2FonTam.Value
  else if VarName = 'VARF_CpaLin2MrgSup' then Value := Int(QrEntiCfgRel_01CpaLin2MrgSup.Value / VAR_frCM)
  else if VarName = 'VARF_CpaLin2AltLin' then Value := Int(QrEntiCfgRel_01CpaLin2AltLin.Value / VAR_frCM)
  else if VarName = 'VARF_CpaLin2AliTex' then Value := QrEntiCfgRel_01CpaLin2AliTex.Value
  //Linha 3
  else if VarName = 'VARF_CpaLin3FonNom' then Value := QrEntiCfgRel_01CpaLin3FonNom.Value
  else if VarName = 'VARF_CpaLin3FonTam' then Value := QrEntiCfgRel_01CpaLin3FonTam.Value
  else if VarName = 'VARF_CpaLin3MrgSup' then Value := Int(QrEntiCfgRel_01CpaLin3MrgSup.Value / VAR_frCM)
  else if VarName = 'VARF_CpaLin3AltLin' then Value := Int(QrEntiCfgRel_01CpaLin3AltLin.Value / VAR_frCM)
  else if VarName = 'VARF_CpaLin3AliTex' then Value := QrEntiCfgRel_01CpaLin3AliTex.Value
  //Logo
  else if VarName = 'VARF_CpaImgMrgSup'  then Value := Int(QrEntiCfgRel_01CpaImgMrgSup.Value / VAR_frCM)
  else if VarName = 'VARF_CpaImgMrgEsq'  then Value := Int(QrEntiCfgRel_01CpaImgMrgEsq.Value / VAR_frCM)
  else if VarName = 'VARF_CpaImgAlt'     then Value := Int(QrEntiCfgRel_01CpaImgAlt.Value / VAR_frCM)
  else if VarName = 'VARF_CpaImgLar'     then Value := Int(QrEntiCfgRel_01CpaImgLar.Value / VAR_frCM)
  else if VarName = 'VARF_CpaImgStre'    then Value := QrEntiCfgRel_01CpaImgStre.Value
  else if VarName = 'VARF_CpaImgProp'    then Value := QrEntiCfgRel_01CpaImgProp.Value
  else if VarName = 'VARF_CpaImgTran'    then Value := QrEntiCfgRel_01CpaImgTran.Value
  else if VarName = 'VARF_CpaImgTranCol' then Value := QrEntiCfgRel_01CpaImgTranCol.Value
  //Capa Fim
  else if VarName = 'VARF_PERIODO_TXT' then
    Value := Geral.Maiusculas(MyObjects.CBAnoECBMesToPeriodoTxt(CBAno, CBMes), False)
  else if VarName = 'VARF_PAGINAR' then
    Value := CkPaginar.Checked
  else if AnsiCompareText(VarName, 'VARF_ULTIMODIA') = 0 then Value :=
    FormatDateTime(VAR_FORMATDATE3,
    MLAGeral.UltimoDiaDoPeriodo_Date(FPeriodoAtu))
  else if AnsiCompareText(VarName, 'VARF_PERIODO') = 0 then Value :=
    FormatDateTime(VAR_FORMATDATE6,
    MLAGeral.PrimeiroDiaDoPeriodo_Date(FPeriodoAtu)) + ' at� ' +
    FormatDateTime(VAR_FORMATDATE6,
    MLAGeral.UltimoDiaDoPeriodo_Date(FPeriodoAtu))
  else if AnsiCompareText(VarName, 'MES_ANO ') = 0 then Value :=
    dmkPF.MesEAnoDoPeriodoLongo(FPeriodoAtu)
  else if AnsiCompareText(VarName, 'CIDADE  ') = 0 then Value :=
    QrEmpCIDADE.Value
  else if AnsiCompareText(VarName, 'SIGLA_UF') = 0 then Value :=
    QrEmpNO_UF.Value
  else if AnsiCompareText(VarName, 'DATA_ATU') = 0 then Value :=
    Geral.FDT(Date, 6)
  else if AnsiCompareText(VarName, 'DATA_FIM') = 0 then Value :=
    FormatDateTime(VAR_FORMATDATE6,
    MLAGeral.UltimoDiaDoPeriodo_Date(FPeriodoAtu))
  //
  //Evitar erro de usu�rio ( ou pr�prio)
  //
  else if AnsiCompareText(VarName, 'VAR_DATA_PER_ANT') = 0 then Value :=
    FormatDateTime(VAR_FORMATDATE2,
    MLAGeral.UltimoDiaDoPeriodo_Date(FPeriodoAtu-1))
  else if AnsiCompareText(VarName, 'VAR_DATA_PER_FIM') = 0 then Value :=
    FormatDateTime(VAR_FORMATDATE2,
    MLAGeral.UltimoDiaDoPeriodo_Date(FPeriodoAtu))
  else if Copy(VarName, 1, 5) = 'VMES_' then
    Value := Copy(VarName, 6) + ' / ' + CBAno.Text
  else if VarName = 'VARF_ANO1' then
    Value := 'Total ' + CBAno.Text
  else if VarName = 'VARF_ANO0' then
    Value := 'Total ' + FormatFloat('0', Geral.IMV(CBAno.Text) -1)
  else if VarName = 'VARF_MID1' then
    Value := 'M�dia ' + CBAno.Text
  else if VarName = 'VARF_MID0' then
    Value := 'M�dia ' + FormatFloat('0', Geral.IMV(CBAno.Text) -1)
  else if VarName = 'LogoBalanceteExiste' then
    Value := FileExists(QrEntiCfgRel_01LogoPath.Value)
  else if VarName = 'LogoBalancetePath' then
    Value := QrEntiCfgRel_01LogoPath.Value
  else if VarName = 'VARF_EMPRESA' then
    Value := CBEmpresa.Text
  else if VarName = 'VARF_RESUMIDO_01' then Value := FResumido_01
  else if VarName = 'VARF_RESUMIDO_02' then Value := FResumido_02
  else if VarName = 'VARF_RESUMIDO_03' then Value := FResumido_03
  else if VarName = 'VARF_RESUMIDO_04' then Value := FResumido_04
  else if VarName = 'VARF_RESUMIDO_05' then Value := FResumido_05
  else if VarName = 'VARF_RESUMIDO_06' then Value := FResumido_06
  else if VarName = 'VARF_RESUMIDO_07' then Value := FResumido_07
  else if VarName ='VARF_DATA' then Value := TPData.Date
  else if VarName = 'VARF_CTACOCODNOM' then
  begin
    Nivel := DModFin.QrSaldosNivNivel.Value;
    Ord   := FormatFloat('0', DModFin.QrSaldosNivCO_Pla.Value);

    if Nivel < 5 then Ord := Ord + '.' + FormatFloat('00', DModFin.QrSaldosNivCO_Cjt.Value);
    if Nivel < 4 then Ord := Ord + '.' + FormatFloat('000', DModFin.QrSaldosNivCO_Gru.Value);
    if Nivel < 3 then Ord := Ord + '.' + FormatFloat('0000', DModFin.QrSaldosNivCO_SGr.Value);
    if Nivel < 2 then Ord := Ord + '.' + FormatFloat('00000', DModFin.QrSaldosNivCO_Cta.Value);

    case DModFin.QrSaldosNivNivel.Value of
      1: Value := Ord + ' - ' + DModFin.QrSaldosNivNO_Cta.Value;
      2: Value := Ord + ' - ' + DModFin.QrSaldosNivNO_SGr.Value;
      3: Value := Ord + ' - ' + DModFin.QrSaldosNivNO_Gru.Value;
      4: Value := Ord + ' - ' + DModFin.QrSaldosNivNO_Cjt.Value;
      5: Value := Ord + ' - ' + DModFin.QrSaldosNivNO_Pla.Value;
    end;
  end;
}
end;

procedure TFmNFSeMenCab.MostraFormNFSEMensCad(SQLType: TSQLType);
begin
  if DBCheck.CriaFm(TFmNFSeMenCad, FmNFSeMenCad, afmoNegarComAviso) then
  begin
    FmNFSeMenCad.ImgTipo.SQLType := SQLType;
    with FmNFSeMenCad do
    begin
      FQrNFSEMenCab := QrNFSEMenCab;
      //
      if SQLType = stUpd then
      begin
        EdCodigo.ValueVariant     := QrNFSEMenCabCodigo.Value;
        EdEmpresa.ValueVariant    := DModG.ObtemFilialDeEntidade(QrNFSEMenCabEmpresa.Value);
        EdCliente.ValueVariant    := QrNFSEMenCabCliente.Value;
        EdContrato.ValueVariant   := QrNFSEMenCabContrato.Value;
        EdIntermed.ValueVariant   := QrNFSEMenCabIntermed.Value;
        EdNFSeSrvCad.ValueVariant := QrNFSEMenCabNFSeSrvCad.Value;
        EdDiaFat.ValueVariant     := QrNFSEMenCabDiaFat.Value;
        EdValor.ValueVariant      := QrNFSEMenCabValor.Value;
        CkGLGerarLct.Checked      := Geral.IntToBool(QrNFSEMenCabGLGerarLct.Value);
        EdGLCarteira.ValueVariant := QrNFSEMenCabGLCarteira.Value;
        EdGLConta.ValueVariant    := QrNFSEMenCabGLConta.Value;
        TPVigenDtIni.Date         := QrNFSEMenCabVigenDtIni.Value;
        TPVigenDtFim.Date         := QrNFSEMenCabVigenDtFim.Value;
        CkGBGerarBol.Checked      := Geral.IntToBool(QrNFSEMenCabGBGerarBol.Value);
        CkAtivo.Checked           := Geral.IntToBool(QrNFSEMenCabAtivo.Value);
        CkContinuar.Checked       := False;
        CkContinuar.Visible       := False;
        //
        case QrNFSEMenCabIncreCompet.Value of
           -1: RGIncreCompet.ItemIndex := 0;
            0: RGIncreCompet.ItemIndex := 1;
          else RGIncreCompet.ItemIndex := 2;
        end;
      end else
      begin
        CkAtivo.Checked     := True;
        CkContinuar.Checked := False;
        CkContinuar.Visible := True;
      end;
      ShowModal;
      Destroy;
    end;
  end;
end;

procedure TFmNFSeMenCab.QrNFSeMenCabAfterOpen(DataSet: TDataSet);
begin
  BtAltera.Enabled  := QrNFSeMenCab.RecordCount > 0;
  BtExclui.Enabled  := QrNFSeMenCab.RecordCount > 0;
end;

procedure TFmNFSeMenCab.QrNFSeMenCabAfterScroll(DataSet: TDataSet);
begin
  LaTotal.Caption := 'Total de itens: ' + Geral.FF0(QrNFSeMenCab.RecordCount)
end;

procedure TFmNFSeMenCab.QrNFSeMenCabBeforeClose(DataSet: TDataSet);
begin
  LaTotal.Caption := '';
  //
  BtAltera.Enabled  := False;
  BtExclui.Enabled  := False;
end;

procedure TFmNFSeMenCab.QrNFSeMenCabCalcFields(DataSet: TDataSet);
begin
{
  QrNFSeMenCabTXT_VIGEN_INI.Value :=
    Geral.FDT(QrNFSeMenCabVigenDtIni.Value, 3, True);
  QrNFSeMenCabTXT_VIGEN_FIM.Value :=
    Geral.FDT(QrNFSeMenCabVigenDtFim.Value, 3, True);
  //
  QrNFSeMenCabTXT_GeraLct.Value := dmkPF.IntToSimNao(QrNFSeMenCabGLGerarLct.Value);
  QrNFSeMenCabTXT_GeraBol.Value := dmkPF.IntToSimNao(QrNFSeMenCabGBGerarBol.Value);

  //

object QrNFSeMenCabTXT_VIGEN_INI: TWideStringField
  FieldKind = fkCalculated
  FieldName = 'TXT_VIGEN_INI'
  Size = 10
  Calculated = True
end
object QrNFSeMenCabTXT_VIGEN_FIM: TWideStringField
  FieldKind = fkCalculated
  FieldName = 'TXT_VIGEN_FIM'
  Size = 10
  Calculated = True
end
object QrNFSeMenCabTXT_GeraLct: TWideStringField
  FieldKind = fkCalculated
  FieldName = 'TXT_GeraLct'
  Size = 3
  Calculated = True
end
object QrNFSeMenCabTXT_GeraBol: TWideStringField
  FieldKind = fkCalculated
  FieldName = 'TXT_GeraBol'
  Size = 3
  Calculated = True
end

}
end;

procedure TFmNFSeMenCab.QrNFSeMenCabVigenDtFimGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  Text := Geral.FDT(QrNFSeMenCabVigenDtFim.Value, 3);
end;

procedure TFmNFSeMenCab.QrNFSeMenCabVigenDtIniGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  Text := Geral.FDT(QrNFSeMenCabVigenDtIni.Value, 3);
end;

procedure TFmNFSeMenCab.ReopenNFSeMenCab(Codigo: Integer);
var
  CodEmpresa, CodCliente, CodInterme, CodRegra: Integer;
  CodServico, SQLEmpresa, SQLCliente, SQLInterme, SQLRegra, SQLServico,
  SN_LCT_TXT, SN_BOL_TXT, SQLVigente: String;
begin
  CodEmpresa := EdEmpresa.ValueVariant;
  if MyObjects.FIC(CodEmpresa=0, EdEmpresa, 'Informe a empresa!') then
    Exit;
////////////////////////////////////////////////////////////////////////////////
  SQLEmpresa := 'WHERE nmc.Empresa=' + Geral.FF0(DModG.QrEmpresasCodigo.Value);
  //
  CodCliente := EdCliente.ValueVariant;
  if CodCliente = 0 then
    SQLCliente := ''
  else
    SQLCliente := 'AND nmc.Cliente=' + Geral.FF0(CodCliente);
  //
  CodInterme := EdIntermediario.ValueVariant;
  if CodInterme = 0 then
    SQLInterme := ''
  else
    SQLInterme := 'AND nmc.Intermed=' + Geral.FF0(CodInterme);
  //
  CodRegra   := EdNFSeSrvCad.ValueVariant;
  if CodRegra = 0 then
    SQLRegra := ''
  else
    SQLRegra := 'AND nmc.nfsesrvcad=' + Geral.FF0(CodRegra);
  //
  CodServico := EdItemListaServico.Text;
  if CodServico = '' then
    SQLServico := ''
  else
    SQLServico := 'AND nsc.ItemListaServico="' + CodServico + '"';
  //
  SN_LCT_TXT := dmkPF.ArrayToTexto('GLGerarLct', 'SN_LCT_TXT', pvPos, True,
    ['N�O', 'SIM', '???']);
  SN_BOL_TXT := dmkPF.ArrayToTexto('GBGerarBol', 'SN_BOL_TXT', pvPos, True,
    ['N�O', 'SIM', '???']);
  //
  if CkVigenteEm.Checked then
    SQLVigente := 'AND "' +
      Geral.FDT(TPVigenteEm.Date, 1) + '" BETWEEN VigenDtIni AND VigenDtFim '
  else
    SQLVigente := '';
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrNFSeMenCab, Dmod.MyDB, [
    'SELECT nmc.*, nsc.Nome NO_SRV, ',
    'car.Nome NO_CRT, cta.Nome NO_CTA, ',
    SN_LCT_TXT,
    SN_BOL_TXT,
    'IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NO_CLI, ',
    'IF(ntm.Tipo=0, ntm.RazaoSocial, ntm.Nome) NO_INT, ',
    {$IfNDef sCNTR}
    'IF(nmc.Contrato = 0, nmc.Valor, con.ValorMes) ValorNFSe ',
    {$Else}
    'nmc.Valor ValorNFSe ',
    {$EndIf}
    'FROM nfsemencab nmc ',
    'LEFT JOIN entidades cli ON cli.Codigo=nmc.Cliente ',
    'LEFT JOIN entidades ntm ON ntm.Codigo=nmc.Intermed ',
    'LEFT JOIN nfsesrvcad nsc ON nsc.Codigo=nmc.NFSeSrvCad ',
    'LEFT JOIN carteiras car ON car.Codigo=nmc.GLCarteira ',
    'LEFT JOIN contas cta ON cta.Codigo=nmc.GLConta ',
    {$IfNDef sCNTR}
    'LEFT JOIN contratos con ON con.Codigo=nmc.Contrato ',
    {$EndIf}
    SQLEmpresa,
    SQLCliente,
    SQLInterme,
    SQLRegra,
    SQLServico,
    SQLVigente,
    '']);
end;

procedure TFmNFSeMenCab.SbImprimeClick(Sender: TObject);
begin
  if (QrNFSeMenCab.State <> dsInactive) and (QrNFSeMenCab.RecordCount > 0) then
  begin
    MyObjects.frxDefineDataSets(frxNFS_MENSA_001_01A, [
      DmodG.frxDsDono,
      frxDsNFSeMenCab
    ]);
    //
    MyObjects.frxMostra(frxNFS_MENSA_001_01A, 'Configura��es mensais de NFS-e');
  end;
end;

end.
