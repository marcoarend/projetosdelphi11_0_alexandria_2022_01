unit NFSeFatAdd;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnMLAGeral, UnInternalConsts,
  dmkImage, UnDmkEnums;

type
  TFmNFSeFatAdd = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    DBGrid1: TDBGrid;
    QrNFSeMenCab: TmySQLQuery;
    QrNFSeMenCabCodigo: TIntegerField;
    QrNFSeMenCabEmpresa: TIntegerField;
    QrNFSeMenCabCliente: TIntegerField;
    QrNFSeMenCabContrato: TIntegerField;
    QrNFSeMenCabIntermed: TIntegerField;
    QrNFSeMenCabNFSeSrvCad: TIntegerField;
    QrNFSeMenCabDiaFat: TSmallintField;
    QrNFSeMenCabIncreCompet: TSmallintField;
    QrNFSeMenCabValor: TFloatField;
    QrNFSeMenCabGLGerarLct: TSmallintField;
    QrNFSeMenCabGLCarteira: TIntegerField;
    QrNFSeMenCabGLConta: TIntegerField;
    QrNFSeMenCabVigenDtIni: TDateField;
    QrNFSeMenCabVigenDtFim: TDateField;
    QrNFSeMenCabAtivo: TSmallintField;
    QrNFSeMenCabNO_SRV: TWideStringField;
    QrNFSeMenCabNO_CRT: TWideStringField;
    QrNFSeMenCabNO_CTA: TWideStringField;
    QrNFSeMenCabNO_CLI: TWideStringField;
    QrNFSeMenCabNO_INT: TWideStringField;
    QrNFSeMenCabTXT_VIGEN_INI: TWideStringField;
    QrNFSeMenCabTXT_VIGEN_FIM: TWideStringField;
    QrNFSeMenCabGBGerarBol: TSmallintField;
    QrNFSeMenCabTXT_GeraLct: TWideStringField;
    QrNFSeMenCabTXT_GeraBol: TWideStringField;
    DsNFSeMenCab: TDataSource;
    BtTodos: TBitBtn;
    BtNenhum: TBitBtn;
    QrNFSeSrvCad: TmySQLQuery;
    QrNFSeSrvCadCodigo: TIntegerField;
    QrNFSeSrvCadNome: TWideStringField;
    QrNFSeSrvCadCodigoTributacaoMunicipio: TWideStringField;
    QrNFSeSrvCadResponsavelRetencao: TSmallintField;
    QrNFSeSrvCadItemListaServico: TWideStringField;
    QrNFSeSrvCadCodigoCnae: TWideStringField;
    QrNFSeSrvCadNumeroProcesso: TWideStringField;
    QrNFSeSrvCadExigibilidadeIss: TSmallintField;
    QrNFSeSrvCadDiscriminacao: TWideMemoField;
    QrNFSeSrvCadRegimeEspecialTributacao: TSmallintField;
    QrNFSeSrvCadOptanteSimplesNacional: TSmallintField;
    QrNFSeSrvCadIncentivoFiscal: TSmallintField;
    QrNFSeSrvCadRpsTipo: TSmallintField;
    QrNFSeSrvCadQuemPagaISS: TSmallintField;
    QrNFSeSrvCadAliquota: TFloatField;
    QrNFSeSrvCadNaturezaOperacao: TIntegerField;
    QrNFSeSrvCadAlqPIS: TFloatField;
    QrNFSeSrvCadAlqCOFINS: TFloatField;
    QrNFSeSrvCadAlqINSS: TFloatField;
    QrNFSeSrvCadAlqIR: TFloatField;
    QrNFSeSrvCadAlqCSLL: TFloatField;
    QrNFSeSrvCadAlqOutrasRetencoes: TFloatField;
    PB1: TProgressBar;
    LaTotal: TLabel;
    QrNFSeSrvCadPDFCompres: TSmallintField;
    QrNFSeSrvCadPDFEmbFont: TSmallintField;
    QrNFSeSrvCadPDFPrnOptm: TSmallintField;
    QrNFSeSrvCadImportado: TIntegerField;
    QrNFSeSrvCadindFinal: TIntegerField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure DBGrid1DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure BtTodosClick(Sender: TObject);
    procedure BtNenhumClick(Sender: TObject);
    procedure DBGrid1CellClick(Column: TColumn);
    procedure QrNFSeMenCabCalcFields(DataSet: TDataSet);
    procedure BtOKClick(Sender: TObject);
    procedure QrNFSeMenCabAfterScroll(DataSet: TDataSet);
    procedure QrNFSeMenCabBeforeClose(DataSet: TDataSet);
  private
    { Private declarations }
    procedure AtivaTodos(Ativo: Integer);
    (* Ini - 20160412 => Movido para o NFSe_PF_0201
    function  CalculaDiscriminacao(const ValorServico, ValorDeducoes,
              DescontoIncondicionado: Double; var iTotTrib: Integer; var
              vTotTrib, vBasTrib, pTotTrib: Double; var fontTotTrib:
              TIBPTaxFont; var tabTotTrib: TIBPTaxTabs; var verTotTrib: String;
              var tpAliqTotTrib: TIBPTaxOrig; var Desistiu: Boolean): String;
    Fim - 20160412 *)
  public
    { Public declarations }
    FNFSeMenCab: String;
    FNFSeFatCab: Integer;
    FLastDPS: Integer;
    //
    procedure ReopenNFSeDPSCab(Codigo: Integer);
    function  CriaRPSItemAtual(): Boolean;
    procedure ReopenNFSeSrvCad(Regra: Integer);
   (* Ini - 20160412 => Movido para o NFSe_PF_0201
    function  CI(Valor, Alq: Double): Double;
    Fim - 20160412 *)
  end;

  var
  FmNFSeFatAdd: TFmNFSeFatAdd;

implementation

uses UnMyObjects, Module, ModuleGeral,  MyVCLSkin, DmkDAC_PF, UMySQLModule,
  GetValor, UnDmkProcFunc, NFSe_PF_0000, NFSe_0000_Module, NFSe_PF_0201;

{$R *.DFM}

procedure TFmNFSeFatAdd.AtivaTodos(Ativo: Integer);
var
  Codigo: Integer;
begin
  Codigo := QrNFSeMenCabCodigo.Value;
  //
  UnDmkDAC_PF.ExecutaMySQLQuery0(DModG.QrUpdPID1, DmodG.MyPID_DB, [
  'UPDATE ' + FNFSeMenCab,
  'SET Ativo=' + Geral.FF0(Ativo),
  '']);
  //
  UnDmkDAC_PF.AbreQuery(QrNFSeMenCab, DModG.MyPID_DB);
  QrNFSeMenCab.Locate('Codigo', Codigo, []);
end;

procedure TFmNFSeFatAdd.BtNenhumClick(Sender: TObject);
begin
  AtivaTodos(0);
end;

procedure TFmNFSeFatAdd.BtOKClick(Sender: TObject);
var
  Itens: Integer;
begin
  try
    BtOK.Enabled := False;
    //
    QrNFSeMenCab.DisableControls;
    QrNFSeMenCab.First;
    //
    PB1.Position := 0;
    PB1.Max      := QrNFSeMenCab.RecordCount;
    Itens        := 0;
    //
    while not QrNFSeMenCab.Eof do
    begin
      if QrNFSeMenCabAtivo.Value = 1 then
      begin
        try
          if CriaRPSItemAtual() then
            Itens := Itens + 1
          else
            Exit;
        except
          Geral.MB_Aviso('Falha ao gerar RPS!');
          Exit;
        end;
      end;
      PB1.Position := PB1.Position + 1;
      PB1.Update;
      Application.ProcessMessages;
      //
      QrNFSeMenCab.Next;
    end;
  finally
    PB1.Position := 0;
    BtOK.Enabled := True;
    //
    QrNFSeMenCab.EnableControls;
    Geral.MB_Info(IntToStr(Itens) + ' itens foram inclu�dos!');
    Close;
  end;
end;

procedure TFmNFSeFatAdd.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmNFSeFatAdd.BtTodosClick(Sender: TObject);
begin
  AtivaTodos(1);
end;

(* Ini - 20160412 => Movido para o NFSe_PF_0201
function TFmNFSeFatAdd.CI(Valor, Alq: Double): Double;
begin
  Result := Geral.RoundC((Valor * Alq) / 100, 2);
end;
Fim - 20160412 *)

{ Ini - 20160412 => Movido para o NFSe_PF_0201
function TFmNFSeFatAdd.CalculaDiscriminacao(const ValorServico, ValorDeducoes,
  DescontoIncondicionado: Double; var iTotTrib: Integer; var vTotTrib, vBasTrib,
  pTotTrib: Double; var fontTotTrib: TIBPTaxFont; var tabTotTrib: TIBPTaxTabs;
  var verTotTrib: String; var tpAliqTotTrib: TIBPTaxOrig;
  var Desistiu: Boolean): String;
var
  InformaTrib: Boolean;
  InfTotTrib, FonteStr, LC116: String;
  IndFinal: TNFeIndFinal;
  //Qry: TmySQLQuery;
  Importado: Boolean;
  vServ, vDesc: Double;
  //
begin
  // INICIO LEI N� 12.741 - Lei da Transparencia
  //  Informacao do total aproximados dos tributos na Discriminacao:
  Desistiu    := False;
  InfTotTrib  := '';
  InformaTrib := False;
  indFinal := TNFeIndFinal(QrNFSeSrvCadindFinal.Value);
  //
  case TindFinalTribNFe(DModG.QrParamsEmpNFSe_indFinalCpl.Value) of
    infFinTribIndefinido: InformaTrib := False;
    infFinTribSoConsumidorFinal: InformaTrib :=  IndFinal = indfinalConsumidorFinal;
    infFinTribTodos: InformaTrib := True;
    else Geral.MB_Erro(
    '"InformaTribCpl" n�o implementado em "DmNFe_0000.TotaisNFe()"');
  end;
  if InformaTrib then
  begin
    FonteStr := '';
    //
    LC116 := Geral.SoNumero_TT(QrNFSeSrvCadItemListaServico.Value);
    vServ := ValorServico;
(*
Manual de Integra��o de Olho no Imposto - vers�o 0.0.6
  i) Quando a empresa oferece desconto incondicional, deve considerar este
  desconto para exibir a carga tribut�ria aproximada?
  Sim, deve considerar o desconto incondicional, j� que sobre ele n�o h�
  incid�ncia tribut�ria. Naturalmente, o desconto deve ser calculado item a
  item dos produtos vendidos, j� que o c�lculo da tributa��o para o atendimento
  da lei 12.741/2012 ocorre item a item.
  //
c) Quando temos desconto no valor do item como fica?
O desconto incondicional deve deduzir o valor do produto para s� depois ser
aplicada a al�quota m�dia aproximada.
*)
    vDesc := ValorDeducoes - DescontoIncondicionado;
    //
    if (Geral.SoNumero1a9_TT(LC116) = '') or (vServ - vDesc < 0) then
    begin
      Exit;
    end;
    Importado := QrNFSeSrvCadImportado.Value = 1;
    //
    if not DmNFSe_0000.ObtemValorAproximadoDeTributos(DmodG.QrParamsEmpNFSeCodMunici.Value,
      LC116, Importado, vServ, vDesc, (*var*) vBasTrib, pTotTrib, vTotTrib, tabTotTrib,
      verTotTrib, tpAliqTotTrib, fontTotTrib) then
    begin
      if Geral.MB_Pergunta('Deseja continuar mesmo assim?') <> ID_YES then
      begin
        Desistiu := True;
        Exit;
      end;
    end;

    case TIBPTaxFont(fontTotTrib) of
      ibptaxfontNaoInfo:
        FonteStr := ''; //'Fonte: C�lculo pr�prio';
      ibptaxfontIBPTax:
        FonteStr := 'Fonte: IBPT';
      ibptaxfontCalculoProprio:
        FonteStr := 'Fonte: C�lculo pr�prio';
      else
      begin
        Geral.MB_Erro(
          '"fontTotTrib" n�o implementado em "FmNFSe_Edit_0201.ConfiguraDiscriminacao()"');
        if Geral.MB_Pergunta('Deseja continuar mesmo assim?') <> ID_YEs then Exit;
      end;
    end;
    //
    iTotTrib := 1;
    InfTotTrib := 'Val. Aprox. dos Tributos R$ ' +
    Geral.FFT(vTotTrib, 2, siPositivo) + ' (' +
    Geral.FFT(pTotTrib, 2, siPositivo) + '%) ' + FonteStr;
    //
    Result := QrNFSeSrvCadDiscriminacao.Value + sLineBreak + InftotTrib;
  end else
  begin
    iTotTrib := 0;
    Result := QrNFSeSrvCadDiscriminacao.Value;
  end;
  // FIM LEI N� 12.741 - Lei da Transparencia
end;
Fim - 20160412 }

function TFmNFSeFatAdd.CriaRPSItemAtual: Boolean;
var
  RpsDataEmissao, Competencia: TDateTime;
  (*RpsID,*) RpsIDSerie, SubstSerie, ItemListaServico,
  CodigoCnae, CodigoTributacaoMunicipio, Discriminacao, NumeroProcesso,
  (*PrestaCpf, PrestaCnpj, PrestaInscricaoMunicipal, TomaCpf, TomaCnpj,
  TomaInscricaoMunicipal, TomaRazaoSocial, TomaEndereco, TomaNumero,
  TomaComplemento, TomaBairro, TomaUf, IntermeCpf, IntermeCnpj,
  IntermeInscricaoMunicipal, IntermeRazaoSocial,*) ConstrucaoCivilCodigoObra,
  ConstrucaoCivilArt, Id, RpsHoraEmissao: String;
  CodAtual, Empresa, Cliente, Intermediario, RpsIDNumero, RpsIDTipo, RpsStatus,
  SubstNumero, SubstTipo, IssRetido, ResponsavelRetencao, (*CodigoMunicipio,
  CodigoPais,*) ExigibilidadeISS, QuemPagaIss, MunicipioIncidencia, (*TomaCodigoMunicipio,
  TomaCodigoPais, TomaCep,*) RegimeEspecialTributacao, OptanteSimplesNacional,
  IncentivoFiscal, NFSeSrvCad, NFSeFatCab: Integer;
  ValorServicos, ValorDeducoes, ValorPis, ValorCofins, ValorInss, ValorIr,
  ValorCsll, OutrasRetencoes, ValorIss, Aliquota, DescontoIncondicionado,
  DescontoCondicionado: Double;
  //
  //DPS,
  Codigo, NfseMenCab: Integer;
  VS: Double;
  Hoje: TDateTime;
  Agora: TTime;
  // INICIO LEI N� 12.741 - Lei da Transparencia
  iTotTrib: Integer;
  vTotTrib, vBasTrib, pTotTrib: Double;
  fontTotTrib: TIBPTaxFont;
  tabTotTrib: TIBPTaxTabs;
  verTotTrib: String;
  tpAliqTotTrib: TIBPTaxOrig;
  Desistiu: Boolean;
  // FIM LEI N� 12.741 - Lei da Transparencia
begin
  Result := False;
  DmodG.ObtemDataHora(Hoje, Agora);
  ReopenNFSeSrvCad(QrNFSeMenCabNFSeSrvCad.Value);
  CodAtual       := 0;
  //
  Desistiu                  := False;
  Empresa                   := QrNFSeMenCabEmpresa.Value;
  Cliente                   := QrNFSeMenCabCliente.Value;
  Intermediario             := QrNFSeMenCabIntermed.Value;
  //RpsID          := ;
  RpsIDNumero               := 0;
  RpsIDSerie                := '';
  RpsIDTipo                 := QrNFSeSrvCadRpsTipo.Value;
  RpsDataEmissao            := Hoje;
  RpsStatus                 := tsStatusRps_Normal;
  Competencia               := IncMonth(RpsDataEmissao, QrNFSeMenCabIncreCompet.Value);
  SubstNumero               := 0;
  SubstSerie                := '';
  SubstTipo                 := 0;
  ValorServicos             := QrNFSeMenCabValor.Value;
  VS                        := ValorServicos;
  ValorDeducoes             := 0;
  ValorPis                  := UnNFSe_PF_0201.CI(VS, QrNFSeSrvCadAlqPIS.Value);
  ValorCofins               := UnNFSe_PF_0201.CI(VS, QrNFSeSrvCadAlqCOFINS.Value);
  ValorInss                 := UnNFSe_PF_0201.CI(VS, QrNFSeSrvCadAlqINSS.Value);
  ValorIr                   := UnNFSe_PF_0201.CI(VS, QrNFSeSrvCadAlqIR.Value);
  ValorCsll                 := UnNFSe_PF_0201.CI(VS, QrNFSeSrvCadAlqCSLL.Value);
  OutrasRetencoes           := UnNFSe_PF_0201.CI(VS, QrNFSeSrvCadAlqOutrasRetencoes.Value);
  ValorIss                  := UnNFSe_PF_0201.CI(VS, QrNFSeSrvCadAliquota.Value);
  Aliquota                  := QrNFSeSrvCadAliquota.Value;
  DescontoIncondicionado    := 0;
  DescontoCondicionado      := 0;
  ResponsavelRetencao       := QrNFSeSrvCadResponsavelRetencao.Value;
  //IssRetido                 := 0;
  if ResponsavelRetencao = -1 then
    IssRetido := 2
  else
    IssRetido := 1;
  ItemListaServico          := QrNFSeSrvCadItemListaServico.Value;
  CodigoCnae                := QrNFSeSrvCadCodigoCnae.Value;
  CodigoTributacaoMunicipio := QrNFSeSrvCadCodigoTributacaoMunicipio.Value;
  //Discriminacao             := QrNFSeSrvCadDiscriminacao.Value;
  //CodigoMunicipio           := EdCodigoMunicipio.ValueVariant;
  //CodigoPais                := EdCodigoPais         .ValueVariant;
  ExigibilidadeISS          := QrNFSeSrvCadExigibilidadeIss.Value;
  QuemPagaISS               := QrNFSeSrvCadQuemPagaISS.Value;
  MunicipioIncidencia       := -1;
  NumeroProcesso            := QrNFSeSrvCadNumeroProcesso.Value;
  //PrestaCpf                 := Ed.ValueVariant;
  //PrestaCnpj                := Ed.ValueVariant;
  //PrestaInscricaoMunicipal  := Ed.ValueVariant;
  //TomaCpf                   := Ed.ValueVariant;
  //TomaCnpj                  := Ed.ValueVariant;
  //TomaInscricaoMunicipal:= ;
  //TomaRazaoSocial:= ;
  //TomaEndereco   := ;
  //TomaNumero     := ;
  //TomaComplemento:= ;
  //TomaBairro     := ;
  //TomaCodigoMunicipio:= ;
  //TomaUf         := ;
  //TomaCodigoPais := ;
  //TomaCep        := ;
  //IntermeCpf     := ;
  //IntermeCnpj    := ;
  //IntermeInscricaoMunicipal := ;
  //IntermeRazaoSocial:= ;
  ConstrucaoCivilCodigoObra := ''; //QrNFSeSrvCad.Value;
  ConstrucaoCivilArt        := ''; //QrNFSeSrvCad.Value;
  RegimeEspecialTributacao  := QrNFSeSrvCadRegimeEspecialTributacao.Value;
  OptanteSimplesNacional    := QrNFSeSrvCadOptanteSimplesNacional.Value;
  IncentivoFiscal           := QrNFSeSrvCadIncentivoFiscal.Value;
  //Id                        := ;
  NFSeSrvCad                := QrNFSeSrvCadCodigo.Value;
  RpsHoraEmissao            := Geral.FDT(Agora, 100);
  //
  NfseMenCab                := QrNFSeMenCabCodigo.Value;
  //
  CodAtual := 0;
  NFSeFatCab                := FNFSeFatCab;
  //
  // INICIO LEI N� 12.741 - Lei da Transparencia
  Discriminacao := UnNFSe_PF_0201.CalculaDiscriminacao(VS, ValorDeducoes,
                     DescontoIncondicionado, QrNFSeSrvCadindFinal.Value,
                     QrNFSeSrvCadImportado.Value, QrNFSeSrvCadItemListaServico.Value,
                     QrNFSeSrvCadDiscriminacao.Value, iTotTrib, vTotTrib, vBasTrib,
                     pTotTrib, fontTotTrib, tabTotTrib, verTotTrib, tpAliqTotTrib,
                     Desistiu);
  // FIM LEI N� 12.741 - Lei da Transparencia
  //
  if Desistiu then
    Exit;

  Codigo := UnNFSe_PF_0000.InsUpdDeclaracaoPrestacaoServico(stIns,
  NFSeFatCab,
  RpsDataEmissao, Competencia, (*RpsID,*) RpsIDSerie, SubstSerie,
  ItemListaServico, CodigoCnae, CodigoTributacaoMunicipio,
  Discriminacao, NumeroProcesso, (*PrestaCpf, PrestaCnpj,
  PrestaInscricaoMunicipal, TomaCpf, TomaCnpj,
  TomaInscricaoMunicipal, TomaRazaoSocial, TomaEndereco, TomaNumero,
  TomaComplemento, TomaBairro, TomaUf, IntermeCpf, IntermeCnpj,
  IntermeInscricaoMunicipal, IntermeRazaoSocial,*)
  ConstrucaoCivilCodigoObra, ConstrucaoCivilArt, Id,
  RpsHoraEmissao,
  CodAtual, Empresa, Cliente, Intermediario, RpsIDNumero, RpsIDTipo,
  RpsStatus, SubstNumero, SubstTipo, IssRetido, ResponsavelRetencao,
  (*CodigoMunicipio, CodigoPais,*) ExigibilidadeISS,
  QuemPagaIss, MunicipioIncidencia, (*TomaCodigoMunicipio, TomaCodigoPais, TomaCep,*)
  RegimeEspecialTributacao, OptanteSimplesNacional, IncentivoFiscal,
  NFSeSrvCad,
  ValorServicos, ValorDeducoes, ValorPis, ValorCofins, ValorInss,
  ValorIr, ValorCsll, OutrasRetencoes, ValorIss, Aliquota,
  DescontoIncondicionado, DescontoCondicionado, NfseMenCab,
  // INICIO LEI N� 12.741 - Lei da Transparencia
  iTotTrib, vTotTrib, vBasTrib, pTotTrib, fontTotTrib,
  tabTotTrib, verTotTrib, tpAliqTotTrib
  // FIM LEI N� 12.741 - Lei da Transparencia
  );
  if Codigo <> 0 then
  begin
    FLastDPS := Codigo;
    Result := True;
  end;
end;

procedure TFmNFSeFatAdd.DBGrid1CellClick(Column: TColumn);
var
  Valor: Variant;
  Codigo, Ativo: Integer;
begin
  if Column.FieldName = 'Valor' then
  begin
    Codigo := QrNFSeMenCabCodigo.Value;
    //
    if MyObjects.GetValorDmk(TFmGetValor, FmGetValor, dmktfDouble,
    QrNFSeMenCabValor.Value, 2, 0, '', '', False, 'Mensalidade',
    'Informe o valor:', 80, Valor) then
    begin
      if UMyMod.SQLInsUpd(DModG.QrUpdPID1, stUpd, FNFSeMenCab, False, [
      'Valor'], ['Codigo'], [
      Valor], [Codigo], False) then
        UnDmkDAC_PF.AbreQuery(QrNFSeMenCab, DModG.MyPID_DB);
        QrNFSeMenCab.Locate('Codigo', Codigo, []);
    end;
  end;
  if Column.FieldName = 'Ativo' then
  begin
    Codigo := QrNFSeMenCabCodigo.Value;
    Ativo  := QrNFSeMenCabAtivo.Value;
    //
    if Ativo = 1 then
      Ativo := 0
    else
      Ativo := 1;
    //
    if UMyMod.SQLInsUpd(DModG.QrUpdPID1, stUpd, FNFSeMenCab, False, [
    'Ativo'], ['Codigo'], [
    Ativo], [Codigo], False) then
      UnDmkDAC_PF.AbreQuery(QrNFSeMenCab, DModG.MyPID_DB);
      QrNFSeMenCab.Locate('Codigo', Codigo, []);
  end;
end;

procedure TFmNFSeFatAdd.DBGrid1DrawColumnCell(Sender: TObject; const Rect: TRect;
  DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
  if Column.FieldName = 'Ativo' then
    MeuVCLSkin.DrawGrid(DBGrid1, Rect, 1, QrNFSeMenCabAtivo.Value);
end;

procedure TFmNFSeFatAdd.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmNFSeFatAdd.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  FLastDPS := 0;
end;

procedure TFmNFSeFatAdd.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmNFSeFatAdd.QrNFSeMenCabAfterScroll(DataSet: TDataSet);
begin
  LaTotal.Caption := 'Total de itens: ' + Geral.FF0(QrNFSeMenCab.RecordCount);
end;

procedure TFmNFSeFatAdd.QrNFSeMenCabBeforeClose(DataSet: TDataSet);
begin
  LaTotal.Caption := '';
end;

procedure TFmNFSeFatAdd.QrNFSeMenCabCalcFields(DataSet: TDataSet);
begin
  QrNFSeMenCabTXT_VIGEN_INI.Value :=
    Geral.FDT(QrNFSeMenCabVigenDtIni.Value, 3, True);
  QrNFSeMenCabTXT_VIGEN_FIM.Value :=
    Geral.FDT(QrNFSeMenCabVigenDtFim.Value, 3, True);
  //
  QrNFSeMenCabTXT_GeraLct.Value := dmkPF.IntToSimNao(QrNFSeMenCabGLGerarLct.Value);
  QrNFSeMenCabTXT_GeraBol.Value := dmkPF.IntToSimNao(QrNFSeMenCabGBGerarBol.Value);
end;

procedure TFmNFSeFatAdd.ReopenNFSeDPSCab(Codigo: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrNFSeMenCab, DModG.MyPID_DB, [
  'SELECT nmc.*, nsc.Nome NO_SRV, ',
  'car.Nome NO_CRT, cta.Nome NO_CTA, ',
  'IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NO_CLI, ',
  'IF(ntm.Tipo=0, ntm.RazaoSocial, ntm.Nome) NO_INT ',
  'FROM ' + FNFSeMenCab + ' nmc ',
  'LEFT JOIN ' + TMeuDB + '.entidades cli ON cli.Codigo=nmc.Cliente ',
  'LEFT JOIN ' + TMeuDB + '.entidades ntm ON ntm.Codigo=nmc.Intermed ',
  'LEFT JOIN ' + TMeuDB + '.nfsesrvcad nsc ON nsc.Codigo=nmc.NFSeSrvCad ',
  'LEFT JOIN ' + TMeuDB + '.carteiras car ON car.Codigo=nmc.GLCarteira ',
  'LEFT JOIN ' + TMeuDB + '.contas cta ON cta.Codigo=nmc.GLConta ',
  'ORDER BY NO_CLI',
  '']);
end;

procedure TFmNFSeFatAdd.ReopenNFSeSrvCad(Regra: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrNFSeSrvCad, Dmod.MyDB, [
  'SELECT * ',
  'FROM nfsesrvcad ',
  'WHERE Codigo=' + Geral.FF0(Regra),
  '']);
end;

end.
