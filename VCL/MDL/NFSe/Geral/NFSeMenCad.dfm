object FmNFSeMenCad: TFmNFSeMenCad
  Left = 339
  Top = 185
  Caption = 'NFS-MENSA-002 :: Edi'#231#227'o de Configura'#231#227'o de Mensalidade de NFS-e'
  ClientHeight = 504
  ClientWidth = 772
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 772
    Height = 47
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 725
      Top = 0
      Width = 47
      Height = 47
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 31
        Height = 31
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 47
      Height = 47
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 47
      Top = 0
      Width = 678
      Height = 47
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 582
        Height = 31
        Caption = 'Edi'#231#227'o de Configura'#231#227'o de Mensalidade de NFS-e'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -26
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 582
        Height = 31
        Caption = 'Edi'#231#227'o de Configura'#231#227'o de Mensalidade de NFS-e'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -26
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 582
        Height = 31
        Caption = 'Edi'#231#227'o de Configura'#231#227'o de Mensalidade de NFS-e'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -26
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 47
    Width = 772
    Height = 345
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 772
      Height = 345
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 772
        Height = 345
        Align = alClient
        TabOrder = 0
        object Panel5: TPanel
          Left = 2
          Top = 15
          Width = 768
          Height = 139
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 0
          ExplicitTop = 14
          object Label9: TLabel
            Left = 8
            Top = 8
            Width = 44
            Height = 13
            Caption = 'Empresa:'
          end
          object Label17: TLabel
            Left = 114
            Top = 31
            Width = 63
            Height = 13
            Caption = 'Intermedi'#225'rio:'
          end
          object Label34: TLabel
            Left = 8
            Top = 55
            Width = 97
            Height = 13
            Caption = 'Tomador do servi'#231'o:'
          end
          object Label19: TLabel
            Left = 8
            Top = 78
            Width = 94
            Height = 13
            Caption = 'Minha Regra Fiscal:'
          end
          object LaContrato: TLabel
            Left = 8
            Top = 102
            Width = 43
            Height = 13
            Caption = 'Contrato:'
          end
          object Label16: TLabel
            Left = 8
            Top = 31
            Width = 36
            Height = 13
            Caption = 'C'#243'digo:'
          end
          object Label5: TLabel
            Left = 295
            Top = 123
            Width = 461
            Height = 13
            Alignment = taRightJustify
            Caption = 
              'Antes de selecionar o contrato '#233' necess'#225'rio selecionar a empresa' +
              ' e o tomador do servi'#231'o (cliente)'
          end
          object SpeedButton1: TSpeedButton
            Left = 735
            Top = 75
            Width = 21
            Height = 20
            Caption = '...'
            OnClick = SpeedButton1Click
          end
          object EdEmpresa: TdmkEditCB
            Left = 54
            Top = 4
            Width = 44
            Height = 21
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'Empresa'
            UpdCampo = 'Empresa'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            OnChange = EdEmpresaChange
            DBLookupComboBox = CBEmpresa
            IgnoraDBLookupComboBox = False
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
          object CBEmpresa: TdmkDBLookupComboBox
            Left = 98
            Top = 4
            Width = 658
            Height = 21
            KeyField = 'Filial'
            ListField = 'NOMEFILIAL'
            ListSource = DModG.DsEmpresas
            TabOrder = 1
            dmkEditCB = EdEmpresa
            QryCampo = 'Empresa'
            UpdType = utYes
            LocF7SQLMasc = '$#'
            LocF7PreDefProc = f7pNone
          end
          object EdIntermed: TdmkEditCB
            Left = 178
            Top = 27
            Width = 54
            Height = 21
            Alignment = taRightJustify
            TabOrder = 3
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'Intermed'
            UpdCampo = 'Intermed'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            DBLookupComboBox = CBIntermed
            IgnoraDBLookupComboBox = False
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
          object CBIntermed: TdmkDBLookupComboBox
            Left = 232
            Top = 27
            Width = 524
            Height = 21
            KeyField = 'Codigo'
            ListField = 'NOMEENTIDADE'
            ListSource = DsIntermediarios
            TabOrder = 4
            dmkEditCB = EdIntermed
            QryCampo = 'Intermed'
            UpdType = utYes
            LocF7SQLMasc = '$#'
            LocF7PreDefProc = f7pNone
          end
          object CBCliente: TdmkDBLookupComboBox
            Left = 166
            Top = 51
            Width = 590
            Height = 21
            KeyField = 'Codigo'
            ListField = 'NOMEENTIDADE'
            ListSource = DsClientes
            TabOrder = 6
            dmkEditCB = EdCliente
            UpdType = utYes
            LocF7SQLMasc = '$#'
            LocF7PreDefProc = f7pNone
          end
          object EdCliente: TdmkEditCB
            Left = 110
            Top = 51
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 5
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'Cliente'
            UpdCampo = 'Cliente'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            OnChange = EdClienteChange
            DBLookupComboBox = CBCliente
            IgnoraDBLookupComboBox = False
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
          object EdNFSeSrvCad: TdmkEditCB
            Left = 110
            Top = 75
            Width = 56
            Height = 20
            Alignment = taRightJustify
            TabOrder = 7
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'NFSeSrvCad'
            UpdCampo = 'NFSeSrvCad'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            DBLookupComboBox = CBNFSeSrvCad
            IgnoraDBLookupComboBox = False
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
          object CBNFSeSrvCad: TdmkDBLookupComboBox
            Left = 166
            Top = 75
            Width = 565
            Height = 21
            KeyField = 'Codigo'
            ListField = 'Nome'
            ListSource = DsNFSeSrvCad
            TabOrder = 8
            dmkEditCB = EdNFSeSrvCad
            QryCampo = 'NFSeSrvCad'
            UpdType = utYes
            LocF7SQLMasc = '$#'
            LocF7PreDefProc = f7pNone
          end
          object EdContrato: TdmkEditCB
            Left = 110
            Top = 98
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 9
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'Contrato'
            UpdCampo = 'Contrato'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            OnChange = EdContratoChange
            DBLookupComboBox = CBContrato
            IgnoraDBLookupComboBox = False
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
          object CBContrato: TdmkDBLookupComboBox
            Left = 166
            Top = 98
            Width = 590
            Height = 21
            KeyField = 'CODIGO'
            ListField = 'NOME'
            ListSource = DsContratos
            TabOrder = 10
            dmkEditCB = EdContrato
            QryCampo = 'Contrato'
            UpdType = utYes
            LocF7SQLMasc = '$#'
            LocF7PreDefProc = f7pNone
          end
          object EdCodigo: TdmkEdit
            Left = 55
            Top = 27
            Width = 55
            Height = 21
            TabStop = False
            Alignment = taRightJustify
            Enabled = False
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            ReadOnly = True
            TabOrder = 2
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'Codigo'
            UpdCampo = 'Codigo'
            UpdType = utInc
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
        end
        object Panel6: TPanel
          Left = 2
          Top = 154
          Width = 768
          Height = 189
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 1
          ExplicitTop = 153
          ExplicitHeight = 190
          object GroupBox2: TGroupBox
            Left = 0
            Top = 0
            Width = 768
            Height = 89
            Align = alTop
            Caption = '                                                      '
            TabOrder = 0
            ExplicitWidth = 769
            object PanelFinanceiro: TPanel
              Left = 2
              Top = 15
              Width = 764
              Height = 72
              Align = alClient
              BevelOuter = bvNone
              TabOrder = 0
              Visible = False
              ExplicitTop = 14
              ExplicitWidth = 765
              ExplicitHeight = 73
              object Label1: TLabel
                Left = 4
                Top = 8
                Width = 39
                Height = 13
                Caption = 'Carteira:'
              end
              object Label2: TLabel
                Left = 4
                Top = 31
                Width = 31
                Height = 13
                Caption = 'Conta:'
              end
              object EdGLCarteira: TdmkEditCB
                Left = 106
                Top = 4
                Width = 56
                Height = 21
                Alignment = taRightJustify
                TabOrder = 0
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                QryCampo = 'GLCarteira'
                UpdCampo = 'GLCarteira'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
                DBLookupComboBox = CBGLCarteira
                IgnoraDBLookupComboBox = False
                AutoSetIfOnlyOneReg = setregOnlyManual
              end
              object EdGLConta: TdmkEditCB
                Left = 106
                Top = 27
                Width = 56
                Height = 21
                Alignment = taRightJustify
                TabOrder = 2
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                QryCampo = 'GLConta'
                UpdCampo = 'GLConta'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
                DBLookupComboBox = CBGLConta
                IgnoraDBLookupComboBox = False
                AutoSetIfOnlyOneReg = setregOnlyManual
              end
              object CBGLConta: TdmkDBLookupComboBox
                Left = 162
                Top = 27
                Width = 590
                Height = 21
                KeyField = 'Codigo'
                ListField = 'Nome'
                ListSource = DsContas
                TabOrder = 3
                dmkEditCB = EdGLConta
                QryCampo = 'GLConta'
                UpdType = utYes
                LocF7SQLMasc = '$#'
                LocF7PreDefProc = f7pNone
              end
              object CBGLCarteira: TdmkDBLookupComboBox
                Left = 162
                Top = 4
                Width = 590
                Height = 21
                KeyField = 'Codigo'
                ListField = 'Nome'
                ListSource = DsCarteiras
                TabOrder = 1
                dmkEditCB = EdGLCarteira
                QryCampo = 'GLCarteira'
                UpdType = utYes
                LocF7SQLMasc = '$#'
                LocF7PreDefProc = f7pNone
              end
              object CkGBGerarBol: TdmkCheckBox
                Left = 106
                Top = 51
                Width = 155
                Height = 17
                Caption = 'Gerar boleto de cobran'#231'a'
                TabOrder = 4
                OnClick = CkGLGerarLctClick
                QryCampo = 'GBGerarBol'
                UpdCampo = 'GBGerarBol'
                UpdType = utYes
                ValCheck = #0
                ValUncheck = #0
                OldValor = #0
              end
            end
            object CkGLGerarLct: TdmkCheckBox
              Left = 12
              Top = 0
              Width = 154
              Height = 17
              Caption = 'Emitir lan'#231'amento financeiro: '
              TabOrder = 1
              OnClick = CkGLGerarLctClick
              QryCampo = 'GLGerarLct'
              UpdCampo = 'GLGerarLct'
              UpdType = utYes
              ValCheck = #0
              ValUncheck = #0
              OldValor = #0
            end
          end
          object Panel8: TPanel
            Left = 0
            Top = 89
            Width = 768
            Height = 73
            Align = alTop
            BevelOuter = bvNone
            TabOrder = 1
            ExplicitWidth = 769
            object GroupBox3: TGroupBox
              Left = 0
              Top = 0
              Width = 768
              Height = 65
              Align = alTop
              Caption = ' Mensalidade: '
              TabOrder = 0
              ExplicitWidth = 769
              object Panel9: TPanel
                Left = 2
                Top = 15
                Width = 400
                Height = 48
                Align = alLeft
                BevelOuter = bvNone
                TabOrder = 0
                ExplicitTop = 18
                ExplicitHeight = 45
                object LaDiaFat: TLabel
                  Left = 7
                  Top = 16
                  Width = 19
                  Height = 13
                  Caption = 'Dia:'
                end
                object LaValor: TLabel
                  Left = 71
                  Top = 16
                  Width = 27
                  Height = 13
                  Caption = 'Valor:'
                end
                object EdDiaFat: TdmkEdit
                  Left = 31
                  Top = 12
                  Width = 32
                  Height = 21
                  Alignment = taRightJustify
                  Enabled = False
                  TabOrder = 0
                  FormatType = dmktfInteger
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ValMin = '1'
                  ValMax = '30'
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '1'
                  QryCampo = 'DiaFat'
                  UpdCampo = 'DiaFat'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 1
                  ValWarn = False
                end
                object EdValor: TdmkEdit
                  Left = 102
                  Top = 12
                  Width = 79
                  Height = 21
                  Alignment = taRightJustify
                  TabOrder = 1
                  FormatType = dmktfDouble
                  MskType = fmtNone
                  DecimalSize = 2
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ValMin = '0'
                  ValMax = '0'
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0,00'
                  QryCampo = 'Valor'
                  UpdCampo = 'Valor'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0.000000000000000000
                  ValWarn = False
                end
                object RGIncreCompet: TRadioGroup
                  Left = 189
                  Top = 0
                  Width = 206
                  Height = 46
                  Caption = 'Incremento no m'#234's de compet'#234'ncia:'
                  Columns = 3
                  ItemIndex = 1
                  Items.Strings = (
                    'Anterior'
                    'Atual'
                    'Pr'#243'ximo')
                  TabOrder = 2
                end
              end
              object GroupBox4: TGroupBox
                Left = 402
                Top = 15
                Width = 364
                Height = 48
                Align = alClient
                Caption = ' Vig'#234'ncia de emiss'#227'o: '
                TabOrder = 1
                ExplicitTop = 14
                ExplicitWidth = 365
                ExplicitHeight = 49
                object Panel10: TPanel
                  Left = 2
                  Top = 15
                  Width = 360
                  Height = 31
                  Align = alClient
                  BevelOuter = bvNone
                  TabOrder = 0
                  ExplicitTop = 14
                  ExplicitWidth = 361
                  ExplicitHeight = 33
                  object Label7: TLabel
                    Left = 8
                    Top = 8
                    Width = 55
                    Height = 13
                    Caption = 'Data inicial:'
                  end
                  object Label8: TLabel
                    Left = 189
                    Top = 8
                    Width = 48
                    Height = 13
                    Caption = 'Data final:'
                  end
                  object TPVigenDtIni: TdmkEditDateTimePicker
                    Left = 67
                    Top = 4
                    Width = 111
                    Height = 21
                    Date = 41197.000000000000000000
                    Time = 0.436966192130057600
                    TabOrder = 0
                    ReadOnly = False
                    DefaultEditMask = '!99/99/99;1;_'
                    AutoApplyEditMask = True
                    QryCampo = 'VigenDtIni'
                    UpdCampo = 'VigenDtIni'
                    UpdType = utYes
                    DatePurpose = dmkdpNone
                  end
                  object TPVigenDtFim: TdmkEditDateTimePicker
                    Left = 240
                    Top = 4
                    Width = 110
                    Height = 21
                    Date = 41197.000000000000000000
                    Time = 0.436966192130057600
                    TabOrder = 1
                    ReadOnly = False
                    DefaultEditMask = '!99/99/99;1;_'
                    AutoApplyEditMask = True
                    QryCampo = 'VigenDtFim'
                    UpdCampo = 'VigenDtFim'
                    UpdType = utYes
                    DatePurpose = dmkdpNone
                  end
                end
              end
            end
          end
          object CkContinuar: TCheckBox
            Left = 89
            Top = 166
            Width = 134
            Height = 16
            Caption = 'Continuar inserindo'
            TabOrder = 3
          end
          object CkAtivo: TdmkCheckBox
            Left = 8
            Top = 166
            Width = 74
            Height = 16
            Caption = 'Ativo'
            TabOrder = 2
            QryCampo = 'Ativo'
            UpdCampo = 'Ativo'
            UpdType = utYes
            ValCheck = '1'
            ValUncheck = '0'
            OldValor = #0
          end
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 392
    Width = 772
    Height = 43
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 14
      Width = 768
      Height = 28
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -14
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -14
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 435
    Width = 772
    Height = 69
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 628
      Top = 14
      Width = 142
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 118
        Height = 39
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 14
      Width = 626
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 3
        Width = 118
        Height = 39
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object QrClientes: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, IF(Tipo=0, RazaoSocial, Nome) NOMEENTIDADE'
      'FROM entidades'
      'ORDER BY NOMEENTIDADE'
      '')
    Left = 520
    Top = 56
    object QrClientesCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrClientesNOMEENTIDADE: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object DsClientes: TDataSource
    DataSet = QrClientes
    Left = 548
    Top = 56
  end
  object QrNFSeSrvCad: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM nfsesrvcad'
      'ORDER BY Nome')
    Left = 576
    Top = 56
    object QrNFSeSrvCadCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrNFSeSrvCadNome: TWideStringField
      FieldName = 'Nome'
      Size = 255
    end
  end
  object DsNFSeSrvCad: TDataSource
    DataSet = QrNFSeSrvCad
    Left = 604
    Top = 56
  end
  object QrIntermediarios: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, IF(Tipo=0, RazaoSocial, Nome) NOMEENTIDADE'
      'FROM entidades'
      'ORDER BY NOMEENTIDADE'
      '')
    Left = 632
    Top = 56
    object QrIntermediariosCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrIntermediariosNOMEENTIDADE: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object DsIntermediarios: TDataSource
    DataSet = QrIntermediarios
    Left = 660
    Top = 56
  end
  object QrContratos: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT CODIGO, NOME'
      'FROM contratos')
    Left = 520
    Top = 84
    object QrContratosCODIGO: TIntegerField
      FieldName = 'CODIGO'
    end
    object QrContratosNOME: TWideStringField
      FieldName = 'NOME'
      Size = 255
    end
  end
  object DsContratos: TDataSource
    DataSet = QrContratos
    Left = 548
    Top = 84
  end
  object QrCarteiras: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM carteiras'
      'ORDER BY Nome'
      '')
    Left = 576
    Top = 84
    object QrCarteirasCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCarteirasNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
  object DsCarteiras: TDataSource
    DataSet = QrCarteiras
    Left = 604
    Top = 84
  end
  object DsContas: TDataSource
    DataSet = QrContas
    Left = 660
    Top = 84
  end
  object QrContas: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM contas'
      'ORDER BY Nome'
      '')
    Left = 632
    Top = 84
    object QrContasCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrContasNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object QrLoc: TMySQLQuery
    Database = Dmod.MyDB
    Left = 352
    Top = 356
  end
end
