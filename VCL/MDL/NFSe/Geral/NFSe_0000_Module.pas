unit NFSe_0000_Module;

interface

uses
  Windows, Forms, SysUtils, Classes, dmkGeral, DB, mySQLDbTables, Controls,
  StdCtrls, Variants, Dialogs, ComCtrls, UrlMon, InvokeRegistry, Rio, dmkEdit,
  SOAPHTTPClient, CAPICOM_TLB, SOAPHTTPTrans, WinInet, OleCtrls,
  SHDocVw, Grids, DBGrids, Graphics, DmkDAC_PF, UnInternalConsts, NFSe_PF_0000,
  UnDmkProcFunc,
  // ini 2023-12-20
  //JwaWinCrypt, DmkACBrNFSe, DMKpnfsConversao,
  ACBrBase, ACBrDFe, ACBrNFSe,
  // fim 2023-12-20
  xmldom, XMLIntf, msxmldom, XMLDoc,
  frxClass, frxDBSet, frxExportPDF,
  {$IfNDef VER320} frxExportBaseDialog, {$EndIf}
  UnDmkEnums, ACBrNFSeX;

type
  TTabelaArqXML = (txmRPS, txmNFS, txmCan);
  TArrInt = array of Integer;
  TArrStr = array of String;
  TDmNFSe_0000 = class(TDataModule)
    QrNFSeDPSCab: TmySQLQuery;
    QrNFSeDPSCabCodigo: TIntegerField;
    QrNFSeDPSCabAmbiente: TSmallintField;
    QrNFSeDPSCabEmpresa: TIntegerField;
    QrNFSeDPSCabCliente: TIntegerField;
    QrNFSeDPSCabIntermediario: TIntegerField;
    QrNFSeDPSCabNFSeSrvCad: TIntegerField;
    QrNFSeDPSCabRpsID: TWideStringField;
    QrNFSeDPSCabRpsIDNumero: TIntegerField;
    QrNFSeDPSCabRpsIDSerie: TWideStringField;
    QrNFSeDPSCabRpsIDTipo: TSmallintField;
    QrNFSeDPSCabRpsDataEmissao: TDateField;
    QrNFSeDPSCabRpsHoraEmissao: TTimeField;
    QrNFSeDPSCabRpsStatus: TIntegerField;
    QrNFSeDPSCabCompetencia: TDateField;
    QrNFSeDPSCabSubstNumero: TIntegerField;
    QrNFSeDPSCabSubstSerie: TWideStringField;
    QrNFSeDPSCabSubstTipo: TSmallintField;
    QrNFSeDPSCabValorServicos: TFloatField;
    QrNFSeDPSCabValorDeducoes: TFloatField;
    QrNFSeDPSCabValorPis: TFloatField;
    QrNFSeDPSCabValorCofins: TFloatField;
    QrNFSeDPSCabValorInss: TFloatField;
    QrNFSeDPSCabValorIr: TFloatField;
    QrNFSeDPSCabValorCsll: TFloatField;
    QrNFSeDPSCabOutrasRetencoes: TFloatField;
    QrNFSeDPSCabValorIss: TFloatField;
    QrNFSeDPSCabAliquota: TFloatField;
    QrNFSeDPSCabDescontoIncondicionado: TFloatField;
    QrNFSeDPSCabDescontoCondicionado: TFloatField;
    QrNFSeDPSCabIssRetido: TSmallintField;
    QrNFSeDPSCabResponsavelRetencao: TSmallintField;
    QrNFSeDPSCabItemListaServico: TWideStringField;
    QrNFSeDPSCabCodigoCnae: TWideStringField;
    QrNFSeDPSCabCodigoTributacaoMunicipio: TWideStringField;
    QrNFSeDPSCabDiscriminacao: TWideMemoField;
    QrNFSeDPSCabCodigoMunicipio: TIntegerField;
    QrNFSeDPSCabCodigoPais: TIntegerField;
    QrNFSeDPSCabExigibilidadeISS: TSmallintField;
    QrNFSeDPSCabMunicipioIncidencia: TIntegerField;
    QrNFSeDPSCabNumeroProcesso: TWideStringField;
    QrNFSeDPSCabPrestaCpf: TWideStringField;
    QrNFSeDPSCabPrestaCnpj: TWideStringField;
    QrNFSeDPSCabPrestaInscricaoMunicipal: TWideStringField;
    QrNFSeDPSCabTomaCpf: TWideStringField;
    QrNFSeDPSCabTomaCnpj: TWideStringField;
    QrNFSeDPSCabTomaInscricaoMunicipal: TWideStringField;
    QrNFSeDPSCabTomaRazaoSocial: TWideStringField;
    QrNFSeDPSCabTomaEndereco: TWideStringField;
    QrNFSeDPSCabTomaNumero: TWideStringField;
    QrNFSeDPSCabTomaComplemento: TWideStringField;
    QrNFSeDPSCabTomaBairro: TWideStringField;
    QrNFSeDPSCabTomaCodigoMunicipio: TIntegerField;
    QrNFSeDPSCabTomaUf: TWideStringField;
    QrNFSeDPSCabTomaCodigoPais: TIntegerField;
    QrNFSeDPSCabTomaCep: TIntegerField;
    QrNFSeDPSCabTomaContatoTelefone: TWideStringField;
    QrNFSeDPSCabTomaContatoEmail: TWideStringField;
    QrNFSeDPSCabIntermeCpf: TWideStringField;
    QrNFSeDPSCabIntermeCnpj: TWideStringField;
    QrNFSeDPSCabIntermeInscricaoMunicipal: TWideStringField;
    QrNFSeDPSCabIntermeRazaoSocial: TWideStringField;
    QrNFSeDPSCabConstrucaoCivilCodigoObra: TWideStringField;
    QrNFSeDPSCabConstrucaoCivilArt: TWideStringField;
    QrNFSeDPSCabRegimeEspecialTributacao: TSmallintField;
    QrNFSeDPSCabOptanteSimplesNacional: TSmallintField;
    QrNFSeDPSCabIncentivoFiscal: TSmallintField;
    QrNFSeDPSCabId: TWideStringField;
    QrNFSeDPSCabLk: TIntegerField;
    QrNFSeDPSCabDataCad: TDateField;
    QrNFSeDPSCabDataAlt: TDateField;
    QrNFSeDPSCabUserCad: TIntegerField;
    QrNFSeDPSCabUserAlt: TIntegerField;
    QrNFSeDPSCabAlterWeb: TSmallintField;
    QrNFSeDPSCabAtivo: TSmallintField;
    QrEmpresa: TmySQLQuery;
    QrEmpresaCodigo: TIntegerField;
    QrEmpresaTipo: TSmallintField;
    QrEmpresaCNPJ: TWideStringField;
    QrEmpresaCPF: TWideStringField;
    QrEmpresaIE: TWideStringField;
    QrEmpresaRG: TWideStringField;
    QrEmpresaCNAE: TWideStringField;
    QrEmpresaNO_ENT: TWideStringField;
    QrEmpresaFANTASIA: TWideStringField;
    QrEmpresaRUA: TWideStringField;
    QrEmpresaCOMPL: TWideStringField;
    QrEmpresaBAIRRO: TWideStringField;
    QrEmpresaTe1: TWideStringField;
    QrEmpresaNO_LOGRAD: TWideStringField;
    QrEmpresaNO_Munici: TWideStringField;
    QrEmpresaNO_UF: TWideStringField;
    QrEmpresaDTB_UF: TWideStringField;
    QrEmpresaNO_Pais: TWideStringField;
    QrEmpresaIEST: TWideStringField;
    QrEmpresaSUFRAMA: TWideStringField;
    QrEmpresaFilial: TIntegerField;
    QrEmpresaNIRE: TWideStringField;
    QrEmpresaNumero: TFloatField;
    QrEmpresaCEP: TFloatField;
    QrEmpresaCodiPais: TFloatField;
    QrEmpresaCodMunici: TFloatField;
    QrEmpresaUF: TFloatField;
    QrFilial: TmySQLQuery;
    QrFilialSimplesFed: TSmallintField;
    QrFilialUF_WebServ: TWideStringField;
    QrFilialDirNFeGer: TWideStringField;
    QrFilialDirNFeAss: TWideStringField;
    QrFilialDirEnvLot: TWideStringField;
    QrFilialDirRec: TWideStringField;
    QrFilialDirPedRec: TWideStringField;
    QrFilialDirProRec: TWideStringField;
    QrFilialDirDen: TWideStringField;
    QrFilialDirPedCan: TWideStringField;
    QrFilialDirCan: TWideStringField;
    QrFilialDirPedInu: TWideStringField;
    QrFilialDirInu: TWideStringField;
    QrFilialDirPedSit: TWideStringField;
    QrFilialDirSit: TWideStringField;
    QrFilialDirPedSta: TWideStringField;
    QrFilialDirSta: TWideStringField;
    QrFilialSiglaCustm: TWideStringField;
    QrFilialInfoPerCuz: TSmallintField;
    QrFilialUF_Servico: TWideStringField;
    QrFilialNFeSerNum: TWideStringField;
    QrFilialSINTEGRA_Path: TWideStringField;
    QrFilialDirDANFEs: TWideStringField;
    QrFilialDirNFeProt: TWideStringField;
    QrFilialNFeSerVal: TDateField;
    QrFilialNFeSerAvi: TSmallintField;
    QrFilialNFetpEmis: TSmallintField;
    QrFilialSCAN_Ser: TIntegerField;
    QrFilialSCAN_nNF: TIntegerField;
    QrFilialDirNFeRWeb: TWideStringField;
    QrFilialDirEveEnvLot: TWideStringField;
    QrFilialDirEveRetLot: TWideStringField;
    QrFilialDirEvePedCCe: TWideStringField;
    QrFilialDirEvePedCan: TWideStringField;
    QrFilialDirEveRetCan: TWideStringField;
    QrFilialDirEveRetCCe: TWideStringField;
    QrFilialDirEveProcCCe: TWideStringField;
    QrFilialPreMailEveCCe: TIntegerField;
    QRNFSeLRpsC: TmySQLQuery;
    QRNFSeLRpsCCodigo: TIntegerField;
    QRNFSeLRpsCSincronia: TSmallintField;
    QRNFSeLRpsCNFSeFatCab: TIntegerField;
    QRNFSeLRpsCAmbiente: TSmallintField;
    QRNFSeLRpsCStatus: TIntegerField;
    QRNFSeLRpsCEmpresa: TIntegerField;
    QRNFSeLRpsCCpf: TWideStringField;
    QRNFSeLRpsCCnpj: TWideStringField;
    QRNFSeLRpsCInscricaoMunicipal: TWideStringField;
    QRNFSeLRpsCQuantidadeRps: TIntegerField;
    QRNFSeLRpsCversao: TFloatField;
    QRNFSeLRpsCId: TWideStringField;
    QrNFSeArqRPS: TmySQLQuery;
    QrNFSeArqRPSCodigo: TIntegerField;
    QRNFSeLRpsI: TmySQLQuery;
    QrFilialNFSeMetodo: TIntegerField;
    QrFilialNFSeVersao: TFloatField;
    QrFilialNFSeSerieRps: TWideStringField;
    QrFilialNFSeWSProducao: TWideStringField;
    QrFilialNFSeWSHomologa: TWideStringField;
    QrFilialNFSeAmbiente: TSmallintField;
    QrFilialNFSeTipCtoMail: TIntegerField;
    QrFilialNFSeCertDigital: TWideStringField;
    QrFilialNFSeCertValidad: TDateField;
    QrFilialNFSeCertAviExpi: TSmallintField;
    QrFilialDpsNumero: TIntegerField;
    QrFilialDirNFSeDPSGer: TWideStringField;
    QrFilialRPSNumLote: TIntegerField;
    QrFilialDirNFSeLogs: TWideStringField;
    QrFilialDirNFSeSchema: TWideStringField;
    QrFilialNFSeLogoPref: TWideStringField;
    QrFilialNFSeLogoFili: TWideStringField;
    QrFilialNFSeUserWeb: TWideStringField;
    QrFilialNFSeSenhaWeb: TWideStringField;
    QrFilialNFSeCodMunici: TIntegerField;
    QrFilialNFSePrefeitura1: TWideStringField;
    QrFilialNFSePrefeitura2: TWideStringField;
    QrFilialNFSePrefeitura3: TWideStringField;
    QrFilialNFSeMsgVisu: TSmallintField;
    QrNFSeDPSCabPRESTA_CNPJ_CPF: TWideStringField;
    QrNFSeDPSCabTOMA_CNPJ_CPF: TWideStringField;
    QrNFSeDPSCabINTERME_CNPJ_CPF: TWideStringField;
    QrNFSeDPSCabStatus: TIntegerField;
    QrNFSeDPSCabLastLRpsC: TIntegerField;
    QrNFSeDPSCabQuemPagaISS: TSmallintField;
    QrNFSeDPSCabNaturezaOperacao: TIntegerField;
    QrNFSeArqRPSXML: TWideMemoField;
    QrNFSeArq: TmySQLQuery;
    QrPsq: TmySQLQuery;
    //ACBrNFSe1: TDmkACBrNFSe;
    QrNFSeArqCodigo: TIntegerField;
    QrNFSeArqEmpresa: TIntegerField;
    QrNFSeArqAmbiente: TSmallintField;
    QrNFSeArqXML: TWideMemoField;
    QrItensLoteRPS: TmySQLQuery;
    QrItensLoteRPSNFSeDPSCab: TIntegerField;
    QRNFSeLRpsICodigo: TIntegerField;
    QRNFSeLRpsINFSeDPSCab: TIntegerField;
    QRNFSeLRpsIEmpresa: TIntegerField;
    QRNFSeLRpsIAmbiente: TSmallintField;
    QRNFSeLRpsCNumeroLote: TLargeintField;
    QRNFSeLRpsCDataRecebimento: TDateTimeField;
    QRNFSeLRpsCProtocolo: TWideStringField;
    QrGetCodRps: TmySQLQuery;
    QrGetCodRpsCodigo: TIntegerField;
    XMLDocument1: TXMLDocument;
    QrIDRps: TmySQLQuery;
    QrIDRpsNfsRpsIDSerie: TWideStringField;
    QrIDRpsNfsRpsIDTipo: TSmallintField;
    QrIDRpsNfsRpsIDNumero: TIntegerField;
    QrIDNFSe: TmySQLQuery;
    QrIDNFSeNfsNumero: TLargeintField;
    QrFilialNFSeMetodEnvRPS: TSmallintField;
    QrNextID: TmySQLQuery;
    QrNextIDRPSIDNumero: TIntegerField;
    QrFilialNFSeTipoRps: TSmallintField;
    QrNFS: TmySQLQuery;
    QrNFSEmpresa: TIntegerField;
    QrNFSAmbiente: TSmallintField;
    QrNFSNfsNumero: TLargeintField;
    QrNFSNfsRpsIDSerie: TWideStringField;
    QrNFSNfsRpsIDTipo: TSmallintField;
    QrNFSNfsRpsIDNumero: TIntegerField;
    QrNFSNfsVersao: TFloatField;
    QrNFSNfsCodigoVerificacao: TWideStringField;
    QrNFSNfsDataEmissao: TDateTimeField;
    QrNFSNfsNfseSubstituida: TLargeintField;
    QrNFSNfsOutrasInformacoes: TWideStringField;
    QrNFSNfsBaseCalculo: TFloatField;
    QrNFSNfsAliquota: TFloatField;
    QrNFSNfsValorIss: TFloatField;
    QrNFSNfsValorLiquidoNfse: TFloatField;
    QrNFSNfsValorCredito: TFloatField;
    QrNFSNfsPrestaCnpj: TWideStringField;
    QrNFSNfsPrestaCpf: TWideStringField;
    QrNFSNfsPrestaInscricaoMunicipal: TWideStringField;
    QrNFSNfsPrestaRazaoSocial: TWideStringField;
    QrNFSNfsPrestaNomeFantasia: TWideStringField;
    QrNFSNfsPrestaEndereco: TWideStringField;
    QrNFSNfsPrestaNumero: TWideStringField;
    QrNFSNfsPrestaComplemento: TWideStringField;
    QrNFSNfsPrestaBairro: TWideStringField;
    QrNFSNfsPrestaCodigoMunicipio: TIntegerField;
    QrNFSNfsPrestaUf: TWideStringField;
    QrNFSNfsPrestaCodigoPais: TWideStringField;
    QrNFSNfsPrestaCep: TWideStringField;
    QrNFSNfsPrestaContatoTelefone: TWideStringField;
    QrNFSNfsPrestaContatoEmail: TWideStringField;
    QrNFSNfsOrgaoGeradorCodigoMunicipio: TIntegerField;
    QrNFSNfsOrgaoGeradorUf: TWideStringField;
    QrNFSDpsRpsIDNumero: TIntegerField;
    QrNFSDpsRpsIDSerie: TWideStringField;
    QrNFSDpsRpsIDTipo: TSmallintField;
    QrNFSDpsRpsDataEmissao: TDateTimeField;
    QrNFSDpsRpsStatus: TIntegerField;
    QrNFSDpsSubstNumero: TIntegerField;
    QrNFSDpsSubstSerie: TWideStringField;
    QrNFSDpsSubstTipo: TSmallintField;
    QrNFSDpsCompetencia: TDateField;
    QrNFSDpsValorServicos: TFloatField;
    QrNFSDpsValorDeducoes: TFloatField;
    QrNFSDpsValorPis: TFloatField;
    QrNFSDpsValorCofins: TFloatField;
    QrNFSDpsValorInss: TFloatField;
    QrNFSDpsValorIr: TFloatField;
    QrNFSDpsValorCsll: TFloatField;
    QrNFSDpsOutrasRetencoes: TFloatField;
    QrNFSDpsValorIss: TFloatField;
    QrNFSDpsAliquota: TFloatField;
    QrNFSDpsDescontoIncondicionado: TFloatField;
    QrNFSDpsDescontoCondicionado: TFloatField;
    QrNFSDpsIssRetido: TSmallintField;
    QrNFSDpsResponsavelRetencao: TSmallintField;
    QrNFSDpsItemListaServico: TWideStringField;
    QrNFSDpsCodigoCnae: TWideStringField;
    QrNFSDpsCodigoTributacaoMunicipio: TWideStringField;
    QrNFSDpsDiscriminacao: TWideMemoField;
    QrNFSDpsCodigoMunicipio: TIntegerField;
    QrNFSDpsCodigoPais: TIntegerField;
    QrNFSDpsExigibilidadeIss: TSmallintField;
    QrNFSDpsNaturezaOperacao: TIntegerField;
    QrNFSDpsMunicipioIncidencia: TIntegerField;
    QrNFSDpsNumeroProcesso: TWideStringField;
    QrNFSDpsTomaCpf: TWideStringField;
    QrNFSDpsTomaCnpj: TWideStringField;
    QrNFSDpsTomaInscricaoMunicipal: TWideStringField;
    QrNFSDpsPrestaCpf: TWideStringField;
    QrNFSDpsPrestaCnpj: TWideStringField;
    QrNFSDpsPrestaInscricaoMunicipal: TWideStringField;
    QrNFSDpsTomaRazaoSocial: TWideStringField;
    QrNFSDpsTomaNomeFantasia: TWideStringField;
    QrNFSDpsTomaEndereco: TWideStringField;
    QrNFSDpsTomaNumero: TWideStringField;
    QrNFSDpsTomaComplemento: TWideStringField;
    QrNFSDpsTomaBairro: TWideStringField;
    QrNFSDpsTomaCodigoMunicipio: TIntegerField;
    QrNFSDpsTomaUf: TWideStringField;
    QrNFSDpsTomaCodigoPais: TIntegerField;
    QrNFSDpsTomaCep: TIntegerField;
    QrNFSDpsTomaContatoTelefone: TWideStringField;
    QrNFSDpsTomaContatoEmail: TWideStringField;
    QrNFSDpsIntermeCpf: TWideStringField;
    QrNFSDpsIntermeCnpj: TWideStringField;
    QrNFSDpsIntermeInscricaoMunicipal: TWideStringField;
    QrNFSDpsIntermeRazaoSocial: TWideStringField;
    QrNFSDpsConstrucaoCivilCodigoObra: TWideStringField;
    QrNFSDpsConstrucaoCivilArt: TWideStringField;
    QrNFSDpsRegimeEspecialTributacao: TSmallintField;
    QrNFSDpsOptanteSimplesNacional: TSmallintField;
    QrNFSDpsIncentivoFiscal: TSmallintField;
    QrNFSDpsInfID_ID: TWideStringField;
    QrNFSCanCodigoCancelamento: TSmallintField;
    QrNFSCanCanc_DataHora: TDateTimeField;
    QrNFSCanCanc_ID: TWideStringField;
    QrNFSCanCanc_Versao: TFloatField;
    QrNFSSubNfseSubstituidora: TLargeintField;
    QrNFSSubSubst_ID: TWideStringField;
    QrNFSSubSubst_Versao: TFloatField;
    QrNFSLk: TIntegerField;
    QrNFSDataCad: TDateField;
    QrNFSDataAlt: TDateField;
    QrNFSUserCad: TIntegerField;
    QrNFSUserAlt: TIntegerField;
    QrNFSAlterWeb: TSmallintField;
    QrNFSAtivo: TSmallintField;
    QrNFSCanNumero: TLargeintField;
    QrNFSCanCpf: TWideStringField;
    QrNFSCanInscricaomunicipal: TWideStringField;
    QrNFSCanCodigoMunicipio: TIntegerField;
    frxDsNFS: TfrxDBDataset;
    frxNFSe: TfrxReport;
    frxDsFilial: TfrxDBDataset;
    QrNFSDADOS_RPS: TWideStringField;
    QrNFSTXT_NFS_PRESTA_CNPJ_CPF: TWideStringField;
    QrNFSTXT_DPS_TOMA_CNPJ_CPF: TWideStringField;
    QrNFSTXT_NFS_PRESTA_CEP: TWideStringField;
    QrNFSTXT_NFS_PRESTA_ENDERECO: TWideStringField;
    QrNFSTXT_NFS_PRESTA_CIDADE: TWideStringField;
    QrNFSTXT_DPS_TOMA_ENDERECO: TWideStringField;
    QrNFSTXT_DPS_TOMA_CEP: TWideStringField;
    QrNFSTXT_DPS_TOMA_CIDADE: TWideStringField;
    QrDPS: TmySQLQuery;
    QrDPSCodigo: TIntegerField;
    QrDPSAmbiente: TSmallintField;
    QrDPSEmpresa: TIntegerField;
    QrDPSCliente: TIntegerField;
    QrDPSIntermediario: TIntegerField;
    QrDPSNFSeSrvCad: TIntegerField;
    QrDPSRpsID: TWideStringField;
    QrDPSRpsIDNumero: TIntegerField;
    QrDPSRpsIDSerie: TWideStringField;
    QrDPSRpsIDTipo: TSmallintField;
    QrDPSRpsDataEmissao: TDateField;
    QrDPSRpsHoraEmissao: TTimeField;
    QrDPSRpsStatus: TIntegerField;
    QrDPSCompetencia: TDateField;
    QrDPSSubstNumero: TIntegerField;
    QrDPSSubstSerie: TWideStringField;
    QrDPSSubstTipo: TSmallintField;
    QrDPSValorServicos: TFloatField;
    QrDPSValorDeducoes: TFloatField;
    QrDPSValorPis: TFloatField;
    QrDPSValorCofins: TFloatField;
    QrDPSValorInss: TFloatField;
    QrDPSValorIr: TFloatField;
    QrDPSValorCsll: TFloatField;
    QrDPSOutrasRetencoes: TFloatField;
    QrDPSValorIss: TFloatField;
    QrDPSAliquota: TFloatField;
    QrDPSDescontoIncondicionado: TFloatField;
    QrDPSDescontoCondicionado: TFloatField;
    QrDPSIssRetido: TSmallintField;
    QrDPSResponsavelRetencao: TSmallintField;
    QrDPSItemListaServico: TWideStringField;
    QrDPSCodigoCnae: TWideStringField;
    QrDPSCodigoTributacaoMunicipio: TWideStringField;
    QrDPSDiscriminacao: TWideMemoField;
    QrDPSCodigoMunicipio: TIntegerField;
    QrDPSCodigoPais: TIntegerField;
    QrDPSExigibilidadeISS: TSmallintField;
    QrDPSMunicipioIncidencia: TIntegerField;
    QrDPSNumeroProcesso: TWideStringField;
    QrDPSPrestaCpf: TWideStringField;
    QrDPSPrestaCnpj: TWideStringField;
    QrDPSPrestaInscricaoMunicipal: TWideStringField;
    QrDPSTomaCpf: TWideStringField;
    QrDPSTomaCnpj: TWideStringField;
    QrDPSTomaInscricaoMunicipal: TWideStringField;
    QrDPSTomaRazaoSocial: TWideStringField;
    QrDPSTomaEndereco: TWideStringField;
    QrDPSTomaNumero: TWideStringField;
    QrDPSTomaComplemento: TWideStringField;
    QrDPSTomaBairro: TWideStringField;
    QrDPSTomaCodigoMunicipio: TIntegerField;
    QrDPSTomaUf: TWideStringField;
    QrDPSTomaCodigoPais: TIntegerField;
    QrDPSTomaCep: TIntegerField;
    QrDPSIntermeCpf: TWideStringField;
    QrDPSIntermeCnpj: TWideStringField;
    QrDPSIntermeInscricaoMunicipal: TWideStringField;
    QrDPSIntermeRazaoSocial: TWideStringField;
    QrDPSConstrucaoCivilCodigoObra: TWideStringField;
    QrDPSConstrucaoCivilArt: TWideStringField;
    QrDPSRegimeEspecialTributacao: TSmallintField;
    QrDPSOptanteSimplesNacional: TSmallintField;
    QrDPSIncentivoFiscal: TSmallintField;
    QrDPSId: TWideStringField;
    QrDPSStatus: TIntegerField;
    QrDPSLastLRpsC: TIntegerField;
    QrDPSQuemPagaISS: TSmallintField;
    QrDPSNaturezaOperacao: TIntegerField;
    QrDPSTomaContatoTelefone: TWideStringField;
    QrDPSTomaContatoEmail: TWideStringField;
    QrDPSTomaNomeFantasia: TWideStringField;
    QrDPSNFSeFatCab: TIntegerField;
    frxDsDPS: TfrxDBDataset;
    QrNFSTXT_NFS_PRESTA_TELEFONE: TWideStringField;
    QrNFSTXT_DPS_TOMA_TELEFONE: TWideStringField;
    QrDPSTXT_DPS_TOMA_CNPJ_CPF: TWideStringField;
    QrDPSTXT_DPS_TOMA_ENDERECO: TWideStringField;
    QrDPSTXT_DPS_TOMA_CEP: TWideStringField;
    QrDPSTXT_DPS_TOMA_CIDADE: TWideStringField;
    QrDPSTXT_DPS_TOMA_TELEFONE: TWideStringField;
    QrILS: TmySQLQuery;
    QrILSNome: TWideStringField;
    QrDPSTXT_ItemListaServico: TWideStringField;
    QrNFSTXT_ItemListaServico: TWideStringField;
    QrILSCodTxt: TWideStringField;
    QrNFSTXT_RECOLHIMENTO: TWideStringField;
    QrDPSTXT_RECOLHIMENTO: TWideStringField;
    QrNFSTXT_MUNICIPIOCREDOR: TWideStringField;
    QrDPSTXT_MUNICIPIOCREDOR: TWideStringField;
    frxPDFExport: TfrxPDFExport;
    QrDPSPDFCompres: TSmallintField;
    QrDPSPDFEmbFont: TSmallintField;
    QrDPSPDFPrnOptm: TSmallintField;
    QrNFSeMenCab: TmySQLQuery;
    QrIBPTax: TmySQLQuery;
    QrIBPTaxCodigo: TLargeintField;
    QrIBPTaxEx: TIntegerField;
    QrIBPTaxTabela: TIntegerField;
    QrIBPTaxNome: TWideStringField;
    QrIBPTaxDescricao: TWideMemoField;
    QrIBPTaxAliqNac: TFloatField;
    QrIBPTaxAliqImp: TFloatField;
    QrIBPTaxVersao: TWideStringField;
    QrIBPTaxLk: TIntegerField;
    QrIBPTaxDataCad: TDateField;
    QrIBPTaxDataAlt: TDateField;
    QrIBPTaxUserCad: TIntegerField;
    QrIBPTaxUserAlt: TIntegerField;
    QrIBPTaxAlterWeb: TSmallintField;
    QrIBPTaxAtivo: TSmallintField;
    QrIBPTaxChave: TWideStringField;
    ACBrNFSe1: TACBrNFSe;
    QrFilialDirNFSeIniFiles: TWideStringField;
    QrFilialNFSeWebFraseSecr: TWideStringField;
    QrFilialDPSNumHom: TIntegerField;
    OpenDialog1: TOpenDialog;
    QrFilialDirNFSeNFSAut: TWideStringField;
    QrFilialDirNFSeNFSCan: TWideStringField;
    QrPsqNfsePorRPS: TMySQLQuery;
    QrPsqNfsePorRPSNfsNumero: TLargeintField;
    QrPsqNfsePorRPSEmpresa: TIntegerField;
    QrPsqNfsePorRPSAmbiente: TSmallintField;
    ACBrNFSeX1: TACBrNFSeX;
    procedure QrNFSeDPSCabCalcFields(DataSet: TDataSet);
    procedure frxNFSeGetValue(const VarName: string; var Value: Variant);
    procedure QrNFSCalcFields(DataSet: TDataSet);
    procedure QrNFSAfterScroll(DataSet: TDataSet);
    procedure QrDPSCalcFields(DataSet: TDataSet);
  private
    { Private declarations }
    FPixelsPerInch: Integer;  // Alexandria -> Tokyo
    //
    procedure ReadPixelsPerInch(Reader: TReader);  // Alexandria -> Tokyo
    procedure WritePixelsPerInch(Writer: TWriter);  // Alexandria -> Tokyo
    //
  protected  // Alexandria -> Tokyo
    { Protected declarations }
    procedure DefineProperties(Filer: TFiler); override;  // Alexandria -> Tokyo
    //
  public
    { Public declarations }
    property PixelsPerInch: Integer read FPixelsPerInch write FPixelsPerInch;  // Alexandria -> Tokyo
    //
    procedure ReopenNFSeDPSCab(Codigo: Integer);
    procedure ReopenNFSeLRpsC(Codigo: Integer);
    procedure ReopenNFSeLRpsCIts(Codigo: Integer);
    function  ReopenEmpresa(Empresa: Integer): Boolean;
{
    function  AssinarArquivoXML_NFSe(XMLGerado_Arq, XMLGerado_Dir: String;
              Empresa: Integer; TipoDoc: TNFSeTipoDoc; RPS: Integer;
              LaAviso1, LaAviso2: TLabel; var XMLAssinado_Dir: String): Boolean;
}
    function  TituloArqXML(const Ext: String; var Titulo, Descri: String): Boolean;
    //
    procedure LoadXML(MyMemo: TMemo; MyWebBrowser: TWebBrowser;
              PageControl: TPageControl; ActivePageIndex: Integer);
    function  InsUpdLoteRPSCab(Empresa, NFSeFatCab, QuantidadeRps: Integer;
              Sincroniza: Boolean): Integer;
    function  InsUpdLoteRPSIts(Lote, DPS: Integer; LaAviso1, LaAviso2: TLabel): Boolean;
    //  Step RPS:
    function  StepNFSeRPS_Solo(Codigo, Status: Integer;
              Campos: Array of String; Valores: array of Variant;
              LaAviso1, LaAviso2: TLabel): Boolean;
    function  StepNFSeRPS_Lote(Lote, Status: Integer;
              Campos: Array of String; Valores: array of Variant;
              LaAviso1, LaAviso2: TLabel): Boolean;
    function {10}stepDados(): Integer;
    function {20}stepGerada(): Integer;
    function {30}stepAssinado(): Integer;
    function {40}stepLoteRejeitado(): Integer;
    function {50}stepAdedLote(): Integer;
    function {60}stepLoteEnvEnviado(): Integer;
    function {70}stepLoteEnvConsulta(): Integer;
    function {100}stepNfseEmitida(): Integer;
    //  Fim Step RPS
    procedure ObtemMensagensRetornoLote(XML: WideString; Lote: Integer;
              Tag: String);
    procedure ColoreStatusNFeEmDBGrid(const DBGrid: TDBGrid;
              const Rect: TRect; DataCol: Integer; Column: TColumn;
              State: TGridDrawState; const Status: Integer;
              const Campo: String = 'STATUS');
    function  ReopenNfseArq(const Tabela: TTabelaArqXML; const Empresa,
              Ambiente, Codigo: Integer; var XML: AnsiString;
              const FormCaption: String): Boolean;
    function  AtualizaXML_No_BD(Tabela: TTabelaArqXML; Empresa, Ambiente,
              Codigo: Integer; XML: AnsiString): Boolean;
    function  ObtemCodigoRPS(Empresa, Tipo: Integer; Serie: String;
              Numero: Integer): Integer;
    procedure AtualizaStatusRPS(LaAviso1, LaAviso2: TLabel;
              Ambiente, Codigo: Integer; Arquivo: String;
              Aviso: String; Qry: TmySQLQuery);
    function  ObtemNumeroRPS_PeloNumeroNFSe(const Empresa, Ambiente: Integer;
              const NFSe: String; var Serie: String; var Tipo, Numero: Integer): Boolean;
    function  ObtemNumeroNFSe_PeloRPS(const Empresa, Ambiente: Integer;
              const RPSTipo, RPSNumero: Integer; const RPSSerie: String;
              var NFSeNumero: Integer): Boolean;
    function  ExcluiRPS_PeloNumero(Empresa, Ambiente: Integer;
              RPSTipo, RPSNumero: Integer; RPSSerie: String): Boolean;
    function  ExportaPDFNFSe_PorNFS(Empresa, Ambiente: Integer; Numero: Largeint): String;
    function  ExportaXMLNFSe_PorRps(PreCNPJ, PreCPF, PreInscMun: String; NumeroNFSe,
              TipoNFSe: Integer; SerieNFSe: String; MostraMsg: Boolean = True): String;
    procedure ImpressaoNFSe_Mostra(Empresa: Integer);
    procedure ImpressaoNFSe_PorNFS(Empresa, Ambiente: Integer; Numeros: TArrInt);
    procedure ImpressaoNFSe_PorRPS_Mul(Empresa, Ambiente: Integer; Serie: TArrStr;
              Tipo, Numero: TArrInt);
    procedure ImpressaoNFSe_PorRPS_Uni(Empresa, Ambiente: Integer; Serie: String;
              Tipo, Numero: Integer);
    function  FormataCodigoVerificacao(Codigo: String): String;
    function  ObtemDescricaoItemListaServico(ItemListaServico: String): String;
    function  EmissoesMensaisVencidas(Hoje: TDateTime; MostraForm: Boolean): Integer;
    function  ObtemValorAproximadoDeTributos(const CodMunici: Integer;
              const LC116: String; Importado: Boolean; const vServ, vDesc: Double;
              var vBasTrib, pTotTrib, vTotTrib: Double; var tabTotTrib: TIBPTaxTabs;
              var verTotTrib: String; var tpAliqTotTrib: TIBPTaxOrig;
              var fontTotTrib: TIBPTaxFont; var Chave: String): Boolean;
    function  CarregaXML_RPS(Empresa, (*RpsIDNumero: Integer; RpsIDSerie: String;
              RpsIDTipo,*) Ambiente: Integer; LaAviso1, LaAviso2: TLabel; Form:
              TForm): Boolean;



  end;

var
  DmNFSe_0000: TDmNFSe_0000;
const
  FCaminhoArquivos = 'C:\Dermatek\EMails\';

implementation

uses Module, UMySQLModule, ModuleGeral, UnMyObjects, NFSeABRASF_0201,
NFSe_PF_0201, MyDBCheck, NFSeMenRnw;

{$R *.dfm}

{ TDmNFSe_Module }

procedure TDmNFSe_0000.AtualizaStatusRPS(LaAviso1, LaAviso2: TLabel;
  Ambiente, Codigo: Integer; Arquivo: String;
  Aviso: String; Qry: TmySQLQuery);
//var
  //Status: Integer;
begin
  if FileExists(Arquivo) then
  begin
    MyObjects.Informa2(LaAviso1, LaAviso2, True, Aviso);
    UnNFSeABRASF_0201.LerXML_Arquivo(DmNFSe_0000.XMLDocument1, Ambiente, Arquivo);
(*   Erro! � StatusRPS
    if QrNFSeDPSCabStatus.Value < 100 then
    begin
      case StatusNfse of
        srNormal: Status := 100;
        srCancelado: Status := 101;
        else Status := CodStatAtual;
      end;
      //
      if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'nfsedpscab', False, [
      'Status'], ['Codigo'], [Status], [Codigo], True) then
      begin
*)
        if Qry <> nil then
        begin
          Qry.Close;
          Qry.Open;
          Qry.Locate('Codigo', Codigo, []);
        end;
(*
      end;
    end;
*)
  end else
    Geral.MB_Erro('Arquivo n�o localizado para importa��o de XML:' + #13#10 +
    Arquivo);
  MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
end;

function TDmNFSe_0000.AtualizaXML_No_BD(Tabela: TTabelaArqXML; Empresa,
  Ambiente, Codigo: Integer; XML: AnsiString): Boolean;
var
  TabNome: String;
begin
  case Tabela of
    txmRPS: TabNome := 'nfsearqrps';
    txmNFS: TabNome := 'nfsearqnfs';
    txmCan: TabNome := 'nfsearqcan';
    else TabNome := '??tab??';
  end;
  //
  UMyMod.ExecutaMySQLQuery1(Dmod.QrUpd, [
  'DELETE FROM ' + TabNome,
  'WHERE Codigo=' + Geral.FF0(Codigo),
  'AND Empresa=' + Geral.FF0(Empresa),
  'AND Ambiente=' + Geral.FF0(Ambiente),
  '']);
  //
  Result := UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, TabNome, False, [
  'XML'], ['Codigo', 'Empresa', 'Ambiente'], [
  XML], [Codigo, Empresa, Ambiente], True);
end;

{
function TDmNFSe_0000.AtualizaXML_No_BD_NFSe(NFSe: Integer;
  XML: String): Boolean;
var
  Codigo: Integer;
  //
  SQLType: TSQLType;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrNFSeArqNFS, Dmod.MyDB, [
  'SELECT * FROM nfsearqnfs ',
  'WHERE Codigo=' + Geral.FF0(NFSe),
  '']);
  //
  if QrNFSeArqNFSCodigo.Value = 0 then
  begin
    SQLType := stIns;
  end else begin
    SQLType := stUpd;
  end;
  //
  Codigo := NFSe;
  UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'nfsearqnfs', False, [
  'XML'], ['Codigo'], [
   XML ], [ Codigo ], True);
  //
  Result := True;
end;

function TDmNFSe_0000.AtualizaXML_No_BD_RPS(const RPS: Integer; var Dir,
  Aviso: String): Boolean;
var
  Arq, DirArq, Buf: String;
  XML: WideString;
  Texto: TStringList;
  XML_Assinado: PChar;
  Codigo, Empresa: Integer;
  //
  SQLType: TSQLType;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrNFSeArqRPS, Dmod.MyDB, [
  'SELECT * FROM nfsearqrps ',
  'WHERE Codigo=' + Geral.FF0(RPS),
  '']);
  //
  Arq := UnNFSe_PF_0000.FormataRPSNumero(RPS) + NFSE_DPS_GER;
  Geral.VerificaDir(Dir, '\', 'NFse assinada', True);
  DirArq := Dir + Arq;
  if not FileExists(DirArq) then
  begin
    Aviso := Aviso + DirArq + #13#10;
  end else begin
    buf := '';
    Texto := TStringList.Create;
    Texto.Clear;
    Texto.LoadFromFile(DirArq);
    XML_Assinado := PChar(Texto.Text);
    buf          := Copy(XML_Assinado, 1);
    XML           := Geral.WideStringToSQLString(buf);
    //
    if QrNFSeArqRPSCodigo.Value = 0 then
    begin
      SQLType := stIns;
    end else begin
      SQLType := stUpd;
    end;
    //
    Codigo := RPS;
    UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'nfsearqrps', False, [
    'XML'], ['Codigo'], [
     XML ], [ Codigo ], True);
    Texto.Free;
  end;
  Result := True;
end;

function TDmNFSe_0000.AtualizaXML_No_BD_RPS_XML(RPS: Integer; XML: String):
  Boolean;
var
  Codigo: Integer;
  //
  SQLType: TSQLType;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrNFSeArqRPS, Dmod.MyDB, [
  'SELECT * FROM nfsearqrps ',
  'WHERE Codigo=' + Geral.FF0(RPS),
  '']);
  //
  if QrNFSeArqRPSCodigo.Value = 0 then
  begin
    SQLType := stIns;
  end else begin
    SQLType := stUpd;
  end;
  //
  Codigo := RPS;
  UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'nfsearqrps', False, [
  'XML'], ['Codigo'], [
   XML ], [ Codigo ], True);
  //
  Result := True;
end;
}

function TDmNFSe_0000.CarregaXML_RPS(Empresa, (*RpsIDNumero: Integer;
  RpsIDSerie: String; RpsIDTipo,*) Ambiente: Integer; LaAviso1,
  LaAviso2: TLabel; Form: TForm): Boolean;
var
  XML: AnsiString;
  Arq: String;
  Mem: TStringStream;
begin
  Result := False;
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Carregando dados do RPS / NFSe selecionado');
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrPsqNfsePorRPS, Dmod.MyDB, [
  'SELECT NfsNumero, Empresa, Ambiente ',
  'FROM nfsenfscab ',
  'WHERE Empresa=' + Geral.FF0(QrNFSeDPSCabEmpresa.Value),
  'AND DpsRpsIdNumero=' + Geral.FF0(QrNFSeDPSCabRpsIDNumero.Value),
  'AND DpsRpsIdSerie="' + QrNFSeDPSCabRpsIDSerie.Value + '" ',
  'AND DpsRpsIdTipo=' + Geral.FF0(QrNFSeDPSCabRpsIDTipo.Value),
  'AND Ambiente=' + Geral.FF0(QrNFSeDPSCabAmbiente.Value),
  '']);

  if QrNFSeDPSCabAmbiente.Value <> QrPsqNfsePorRPSAmbiente.Value then
  begin
    Geral.MB_Erro('Ambiente do RPS: ' + Geral.FF0(QrNFSeDPSCabAmbiente.Value) +
    ' n�o confere com o ambiente da NFSe: ' + Geral.FF0(QrPsqNfsePorRPSAmbiente.Value));
    Screen.Cursor := crDefault;
    Exit;
  end;

  if not DmNFSe_0000.ReopenNfseArq(txmNFS, QrPsqNfsePorRPSEmpresa.Value,
  Ambiente, QrPsqNfsePorRPSNfsNumero.Value, XML, Form.Caption) then
  begin
    if Geral.MB_Pergunta('Deseja carregar o XML de um arquivo no banco de dados?') = ID_YES then
    begin
      if MyObjects.FileOpenDialog(Form, DmNFSe_0000.QrFilialDirNFSeDPSGer.Value,
      '', 'Arquivo XML', '*.xml', [], Arq) then
      begin
        if FileExists(Arq) then
        begin
          if UnNFSeABRASF_0201.LerXML_Arquivo(DmNFSe_0000.XMLDocument1, Ambiente, Arq) then
          Geral.MB_Aviso('Tente executar a a��o novamente!');
        end;
      end;
    end;
    Exit;
  end;
  //
  UnNFSe_PF_0201.ConfiguraComponenteACBrNFSe(DmodG.QrEmpresasCodigo.Value,
    DmNFSe_0000.ACBrNFSe1);
  DmNFSe_0000.ACBrNFSe1.NotasFiscais.Clear;
  //
  Mem := TStringStream.Create(XML);
  DmNFSe_0000.ACBrNFSe1.NotasFiscais.LoadFromStream(Mem);

  if DmNFSe_0000.ACBrNFSe1.NotasFiscais.Items[0].NFSe.
  PrestadorServico.IdentificacaoPrestador.InscricaoMunicipal = '' then
    DmNFSe_0000.ACBrNFSe1.NotasFiscais.Items[0].NFSe.
    PrestadorServico.IdentificacaoPrestador.InscricaoMunicipal :=
    Geral.SoNumero_TT(DModG.QrEmpresasNIRE.Value);
  if DmNFSe_0000.ACBrNFSe1.NotasFiscais.Items[0].NFSe.Numero = '' then
  begin
    //Result := False;
    Geral.MB_Aviso('XML da NFSe n�o foi carregado ou n�o est� na base de dados!');
    Screen.Cursor := crDefault;
  end else
    Result := true;
end;

procedure TDmNFSe_0000.ColoreStatusNFeEmDBGrid(const DBGrid: TDBGrid;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState;
  const Status: Integer; const Campo: String);
var
  Txt, Bak: TColor;
begin
  if (Uppercase(Column.FieldName) = Uppercase(Campo)) then
  begin
    Txt := clWindow;
    Bak := clBlack;
    case Status of
      100:      begin Txt := clBlue   ; Bak := clWindow end;
      101..102: begin Txt := clFuchsia; Bak := clWindow end;
      103..199: begin Txt := clBlack  ; Bak := clWindow end;
      201..299: begin Txt := clRed    ; Bak := clWindow end;
      301..399: begin Txt := clWindow ; Bak := clRed;   end;
      401..999: begin Txt := clRed    ; Bak := clWindow end;
    end;
    MyObjects.DesenhaTextoEmDBGrid(DBGrid, Rect,
      Txt, Bak, Column.Alignment, Column.Field.DisplayText);
  end;
end;

procedure TDmNFSe_0000.DefineProperties(Filer: TFiler);
var
  Ancestor: TDataModule;
begin
  inherited;
  Ancestor := TDataModule(Filer.Ancestor);
  Filer.DefineProperty('PixelsPerInch', ReadPixelsPerInch, WritePixelsPerInch, True);
end;

function TDmNFSe_0000.EmissoesMensaisVencidas(Hoje: TDateTime; MostraForm: Boolean): Integer;
var
  Data: TDateTime;
  //SN_LCT_TXT, SN_BOL_TXT,
  DataFim: String;
begin
  Data    := Geral.UltimoDiaDoMes(Hoje);
  DataFim := Geral.FDT(Data + 1, 1);
  //
  if MostraForm = False then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrNFSeMenCab, Dmod.MyDB, [
      'SELECT * ',
      'FROM nfsemencab ',
      'WHERE Ativo=1 ',
      'AND VigenDtIni < SYSDATE() ',
      'AND VigenDtFim BETWEEN "1900-01-01" AND "' + DataFim + '" ',
      '']);
    Result := QrNFSeMenCab.RecordCount;
  end else
    Result := 1;
  //
  if MostraForm and (Result > 0) then
  begin
    if DBCheck.CriaFm(TFmNFSeMenRnw, FmNFSeMenRnw, afmoNegarComAviso) then
    begin
      FmNFSeMenRnw.FHoje := Hoje;
      FmNFSeMenRnw.ReopenNFSeMenCab(Hoje);
      FmNFSeMenRnw.ShowModal;
      FmNFSeMenRnw.Destroy;
    end;
  end;
end;

function TDmNFSe_0000.ExcluiRPS_PeloNumero(Empresa, Ambiente: Integer; RPSTipo,
  RPSNumero: Integer; RPSSerie: String): Boolean;
begin
  Result := UMyMod.ExecutaMySQLQuery1(Dmod.QrUpd, [
  'DELETE FROM nfsedpscab ',
  'WHERE Empresa=' + Geral.FF0(Empresa),
  'AND Ambiente=' + Geral.FF0(Ambiente),
  'AND RpsIDSerie="' + RPSSerie + '"',
  'AND RpsIDTipo=' + Geral.FF0(RPSTipo),
  'AND RpsIDNumero=' + Geral.FF0(RPSNumero),
  '']);
end;

function TDmNFSe_0000.ExportaPDFNFSe_PorNFS(Empresa, Ambiente: Integer;
  Numero: Largeint): String;
var
  Id, DANFSE: String;
  //I: Integer;
begin
  Result  := '';
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrNFS, Dmod.MyDB, [
    'SELECT * FROM nfsenfscab ',
    'WHERE Empresa=' + Geral.FF0(Empresa),
    'AND Ambiente=' + Geral.FF0(Ambiente),
    'AND DpsRpsIDNumero=' + Geral.FF0(Numero),
    '']);
  //
  try
    Screen.Cursor := crHourGlass;
    Id            := Geral.FF0(Numero);
    DANFSE        := FCaminhoArquivos;
    //
    Geral.VerificaDir(DANFSE, '\', 'Diret�rio tempor�rio do DANFE', True);
    DANFSE := DANFSE + Id + '.pdf';
    ReopenEmpresa(Empresa);
    MyObjects.frxDefineDataSets(frxNFSe, [frxDsFilial, frxDsNFS, frxDsDPS]);
    //
    if FileExists(DANFSE) then
      DeleteFile(DANFSE);
    // 2013-04-16 INI
    frxPDFExport.Compressed := QrDPSPDFCompres.Value = 1;
    frxPDFExport.EmbeddedFonts := QrDPSPDFEmbFont.Value = 1;
    frxPDFExport.PrintOptimized := QrDPSPDFPrnOptm.Value = 1;
    // 2013-04-16 FIM
    frxPDFExport.FileName := DANFSE;
    //
    //
    MyObjects.frxPrepara(frxNFSe, 'NFe' + Id);
    DmNFSe_0000.frxNFSe.Export(frxPDFExport);
    //
    Result := DANFSE;
  finally
    Screen.Cursor := crDefault;
  end;
end;

function TDmNFSe_0000.ExportaXMLNFSe_PorRps(PreCNPJ, PreCPF, PreInscMun: String;
  NumeroNFSe, TipoNFSe: Integer; SerieNFSe: String; MostraMsg: Boolean = True): String;
var
  Id, XML, DocPre(*, DocTom, DocInt*): String;
  TxtFile: TextFile;
  XML_DB: AnsiString;
begin
  try
    Screen.Cursor := crHourGlass;
    //
    Id  := Geral.FF0(NumeroNFSe);
    XML := FCaminhoArquivos;
    //
    Geral.VerificaDir(XML, '\', 'Diret�rio tempor�rio do XML NFS-e', True);
    XML := XML + Id + '.xml';
    //
    if FileExists(XML) then
      DeleteFile(XML);
    //
    //Prestador
    DocPre := PreCNPJ;
    if DocPre = '' then
      DocPre := PreCPF;
    //
    (*
    //Tomador
    DocTom := TomCNPJ;
    if DocTom = '' then
      DocTom  := TomCPF;
    //
    //Intermedi�rio
    DocInt := IntCNPJ;
    if DocInt = '' then
      DocInt  := IntCPF;
    //
    *)
    UnNFSe_PF_0201.ConfiguraComponenteACBrNFSe(DmodG.QrEmpresasCodigo.Value,
      DmNFSe_0000.ACBrNFSe1);
    DmNFSe_0000.ACBrNFSe1.NotasFiscais.Clear;
    //
    DmNFSe_0000.ACBrNFSe1.NotasFiscais.Add;
  // ini 2023-12-20
    //if ACBrNFSe1.ConsultarNFSeporRps(Geral.FF0(NumeroNFSe), SerieNFSe, Geral.FF0(TipoNFSe), DocPre, PreInscMun) then
    if ACBrNFSe1.ConsultarNFSeporRps(Geral.FF0(NumeroNFSe), SerieNFSe, Geral.FF0(TipoNFSe)) then
  // fim 2023-12-20 Parei Aqui!
    begin
      //XML_DB := DmNFSe_0000.ACBrNFSe1.WebServices.ConsutaNFSeporRps.ArquivoRetorno;
      XML_DB := DmNFSe_0000.ACBrNFSe1.WebServices.ConsNfseRps.RetWS;
      if MostraMsg then
        Geral.MB_Info(XML_DB);
    end;
    //
    AssignFile(TxtFile, XML);
    Rewrite(TxtFile);
    Write(TxtFile, XML_DB);
    CloseFile(TxtFile);
    //
    Result := XML;
  finally
    Screen.Cursor := crDefault;
  end;
end;

function TDmNFSe_0000.FormataCodigoVerificacao(Codigo: String): String;
const
  Bloco = 3;
var
  K, I: Integer;
begin
  Result := '';
  I := 0;
  K := Length(Codigo);
  while I < K do
  begin
    if I > 0 then
      Result := Result + '-';
    Result := Result + Copy(Codigo, I + 1, Bloco);
    I := I + Bloco;
  end;
end;

procedure TDmNFSe_0000.frxNFSeGetValue(const VarName: string;
  var Value: Variant);
begin
  if VarName = 'VARF_CODIGO_VERIFICACAO' then
    Value := FormataCodigoVerificacao(QrNFSNfsCodigoVerificacao.Value)
  else  
  if VarName = 'NFSeLogoFili_Find' then
    Value := FileExists(QrFilialNFSeLogoFili.Value)
  else
  if VarName = 'NFSeLogoFili_Path' then
    Value := QrFilialNFSeLogoFili.Value
  else
  if VarName = 'NFSeLogoPref_Find' then
    Value := FileExists(QrFilialNFSeLogoPref.Value)
  else
  if VarName = 'NFSeLogoPref_Path' then
    Value := QrFilialNFSeLogoPref.Value
  else
  if VarName = 'VARF_AMBIENTE_TESTE' then
    Value := QrNFSAmbiente.Value
  else
  // ...
  if Copy(VarName, 1, 7) = 'CHOICE_' then
  begin
    //21/10/2013 14:41 :: O Bug no site da Prefeitura foi corrigido!
    (*
    // BUG SERVIDOR MARING� - N�o retorna v�rios dados do RPS
    if QrDPS.RecordCount > 0 then
    begin
      // PRESTADOR
      if VarName = 'CHOICE_PRESTA_IM' then
        Value := QrDPSPrestaInscricaoMunicipal.Value
      else
      // TOMADOR
      if VarName = 'CHOICE_TOMA_RAZAOSOCIAL' then
        Value := QrDPSTomaRazaoSocial.Value
      else
      if VarName = 'CHOICE_TOMA_CNPJ_CPF' then
        Value := QrDPSTXT_DPS_TOMA_CNPJ_CPF.Value
      else
      if VarName = 'CHOICE_TOMA_IM' then
        Value := QrDPSTomaInscricaoMunicipal.Value
      else
      if VarName = 'CHOICE_TOMA_ENDERECO' then
        Value := QrDPSTXT_DPS_TOMA_ENDERECO.Value
      else
      if VarName = 'CHOICE_TOMA_BAIRRO' then
        Value := QrDPSTomaBairro.Value
      else
      if VarName = 'CHOICE_TOMA_CEP' then
        Value := QrDPSTXT_DPS_TOMA_CEP.Value
      else
      if VarName = 'CHOICE_TOMA_UF' then
        Value := QrDPSTomaUF.Value
      else
      if VarName = 'CHOICE_TOMA_CONTATOEMAIL' then
        Value := QrDPSTomaContatoEmail.Value
      else
      if VarName = 'CHOICE_TOMA_CONTATOTELEFONE' then
        Value := QrDPSTXT_DPS_TOMA_TELEFONE.Value
      else
      if VarName = 'CHOICE_TOMA_CIDADE' then
        Value := QrDPSTXT_DPS_TOMA_CIDADE.Value
      else
      if VarName = 'CHOICE_DISCRIMINACAO' then
        Value := QrDPSDiscriminacao.Value
      else
      if VarName = 'CHOICE_VALORSERVICOS' then
        Value := Geral.FFT(QrDPSValorServicos.Value, 2, siPositivo)
      else
      if VarName = 'CHOICE_ITEMLISTASERVICO' then
        Value := Geral.FormataLC11603(QrDPSItemListaServico.Value) + ' - ' +
                 QrDPSTXT_ItemListaServico.Value
      else
      if VarName = 'CHOICE_VALORDEDUCOES' then
        Value := Geral.FFT(QrDPSValorDeducoes.Value, 2, siPositivo)
      else
      if VarName = 'CHOICE_RECOLHIMENTO' then
        Value := QrDPSTXT_RECOLHIMENTO.Value
      else

      //

      if VarName = 'CHOICE_VALORPIS' then
        Value := Geral.FFT(QrDPSValorPis.Value, 2, siPositivo)
      else
      if VarName = 'CHOICE_VALORCOFINS' then
        Value := Geral.FFT(QrDPSValorCofins.Value, 2, siPositivo)
      else
      if VarName = 'CHOICE_VALORINSS' then
        Value := Geral.FFT(QrDPSValorInss.Value, 2, siPositivo)
      else
      if VarName = 'CHOICE_VALORIR' then
        Value := Geral.FFT(QrDPSValorIr.Value, 2, siPositivo)
      else
      if VarName = 'CHOICE_VALORCSLL' then
        Value := Geral.FFT(QrDPSValorCsll.Value, 2, siPositivo)
      else
      if VarName = 'CHOICE_DESCONTOCONDICIONADO' then
        Value := Geral.FFT(QrDPSDescontoCondicionado.Value, 2, siPositivo)
      else
      if VarName = 'CHOICE_DESCONTOINCONDICIONADO' then
        Value := Geral.FFT(QrDPSDescontoIncondicionado.Value, 2, siPositivo)
      else
      if VarName = 'CHOICE_MUNICIPIOCREDOR' then
        Value := QrDPSTXT_MUNICIPIOCREDOR.Value
      else
      if VarName = 'CHOICE_OUTRASRETENCOES' then
        Value := Geral.FFT(QrDPSOutrasRetencoes.Value, 2, siPositivo)
      else
      if VarName = 'CHOICE_CONSTRUCAOCIVILART' then
        Value := QrDPSConstrucaoCivilCodigoObra.Value
      else
      if VarName = 'CHOICE_CONSTRUCAOCIVILCODIGOOBRA' then
        Value := QrDPSConstrucaoCivilArt.Value
      else
      if VarName = 'CHOICE_DPSOPTANTESIMPLESNACIONAL' then
      begin
        if QrDPSOptanteSimplesNacional.Value = 1 then
          Value := 'Sim'
        else
          Value := 'N�o';
      end
      else
    end else
    begin
    *)
      // PRESTADOR
      if VarName = 'CHOICE_PRESTA_IM' then
        Value := QrNFSNfsPrestaInscricaoMunicipal.Value
      else
      // TOMADOR
      if VarName = 'CHOICE_TOMA_RAZAOSOCIAL' then
        Value := QrNFSDpsTomaRazaoSocial.Value
      else
      if VarName = 'CHOICE_TOMA_CNPJ_CPF' then
        Value := QrNFSTXT_DPS_TOMA_CNPJ_CPF.Value
      else
      if VarName = 'CHOICE_TOMA_IM' then
        Value := QrNFSDpsTomaInscricaoMunicipal.Value
      else
      if VarName = 'CHOICE_TOMA_ENDERECO' then
        Value := QrNFSTXT_DPS_TOMA_ENDERECO.Value
      else
      if VarName = 'CHOICE_TOMA_BAIRRO' then
        Value := QrNFSDpsTomaBairro.Value
      else
      if VarName = 'CHOICE_TOMA_CEP' then
        Value := QrNFSTXT_DPS_TOMA_CEP.Value
      else
      if VarName = 'CHOICE_TOMA_UF' then
        Value := QrNFSDpsTomaUF.Value
      else
      if VarName = 'CHOICE_TOMA_CONTATOEMAIL' then
        Value := QrNFSDpsTomaContatoEmail.Value
      else
      if VarName = 'CHOICE_TOMA_CONTATOTELEFONE' then
        Value := QrNFSTXT_DPS_TOMA_TELEFONE.Value
      else
      if VarName = 'CHOICE_TOMA_CIDADE' then
        Value := QrNFSTXT_DPS_TOMA_CIDADE.Value
      else
      if VarName = 'CHOICE_DISCRIMINACAO' then
        Value := QrNFSDpsDiscriminacao.Value
      else
      if VarName = 'CHOICE_VALORSERVICOS' then
        Value := Geral.FFT(QrNFSDpsValorServicos.Value, 2, siPositivo)
      else
      if VarName = 'CHOICE_ITEMLISTASERVICO' then
        Value := Geral.FormataLC11603(QrNFSDpsItemListaServico.Value) + ' - ' +
                 QrNFSTXT_ItemListaServico.Value
      else
      if VarName = 'CHOICE_VALORDEDUCOES' then
        Value := Geral.FFT(QrNFSDpsValorDeducoes.Value, 2, siPositivo)
      else
      if VarName = 'CHOICE_RECOLHIMENTO' then
        Value := QrNFSTXT_RECOLHIMENTO.Value
      else

      //

      if VarName = 'CHOICE_VALORPIS' then
        Value := Geral.FFT(QrNFSDpsValorPis.Value, 2, siPositivo)
      else
      if VarName = 'CHOICE_VALORCOFINS' then
        Value := Geral.FFT(QrNFSDpsValorCofins.Value, 2, siPositivo)
      else
      if VarName = 'CHOICE_VALORINSS' then
        Value := Geral.FFT(QrNFSDpsValorInss.Value, 2, siPositivo)
      else
      if VarName = 'CHOICE_VALORIR' then
        Value := Geral.FFT(QrNFSDpsValorIr.Value, 2, siPositivo)
      else
      if VarName = 'CHOICE_VALORCSLL' then
        Value := Geral.FFT(QrNFSDpsValorCsll.Value, 2, siPositivo)
      else
      if VarName = 'CHOICE_DESCONTOCONDICIONADO' then
        Value := Geral.FFT(QrNFSDpsDescontoCondicionado.Value, 2, siPositivo)
      else
      if VarName = 'CHOICE_DESCONTOINCONDICIONADO' then
        Value := Geral.FFT(QrNFSDpsDescontoIncondicionado.Value, 2, siPositivo)
      else
      if VarName = 'CHOICE_MUNICIPIOCREDOR' then
        Value := QrNFSTXT_MUNICIPIOCREDOR.Value
      else
      if VarName = 'CHOICE_OUTRASRETENCOES' then
        Value := Geral.FFT(QrNFSDpsOutrasRetencoes.Value, 2, siPositivo)
      else
      if VarName = 'CHOICE_CONSTRUCAOCIVILART' then
        Value := QrNFSDpsConstrucaoCivilCodigoObra.Value
      else
      if VarName = 'CHOICE_CONSTRUCAOCIVILCODIGOOBRA' then
        Value := QrNFSDpsConstrucaoCivilArt.Value
      else
      if VarName = 'CHOICE_DPSOPTANTESIMPLESNACIONAL' then
      begin
        if QrNFSDpsOptanteSimplesNacional.Value = 1 then
          Value := 'Sim'
        else
          Value := 'N�o';
      end
      else
    //end;
  end else
  //...
  if VarName = 'VARF_BASECALCULO' then
    Value := Geral.FFT(QrNFSNfsBaseCalculo.Value, 2, siPositivo)
  else
  if VarName = 'VARF_ALIQUOTA' then
    Value := Geral.FFT(QrNFSNfsAliquota.Value, 2, siPositivo)
  else
  if VarName = 'VARF_VALORISS' then
    Value := Geral.FFT(QrNFSNfsValorIss.Value, 2, siPositivo)
  else
  if VarName = 'VARF_COMPETENCIA' then
  begin
    if QrNFSDpsCompetencia.Value < 2 then
      Value := ' '
    else
      Value := Geral.FDT(QrNFSDpsCompetencia.Value, 14);
  end
  else
  if VarName = 'VARF_SITUACAO' then
  begin
    if QrNFSCanCodigoCancelamento.Value = 0 then
      Value := 'NORMAL'
    else
      Value := 'CANCELADO'
  end
end;

procedure TDmNFSe_0000.ImpressaoNFSe_Mostra(Empresa: Integer);
begin
  ReopenEmpresa(Empresa);
  MyObjects.frxDefineDataSets(frxNFSe, [frxDsFilial, frxDsNFS, frxDsDPS]);
  MyObjects.frxMostra(frxNFSe, 'Nota Fiscal de Servi�os Eletr�nica - NFS-e');
end;

procedure TDmNFSe_0000.ImpressaoNFSe_PorNFS(Empresa, Ambiente: Integer;
  Numeros: TArrInt);
 //Numeros: array of Integer);
var
  SQLNums: String;
  I: Integer;
begin
  SQLNums := '';
  for I := Low(Numeros) to High(Numeros) do
    SQLNums := SQLNums + ',' + Geral.FF0(Numeros[I]);
  if Length(SQLNums) > 0 then
    SQLNums := Copy(SQLNums, 2);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrNFS, Dmod.MyDB, [
  'SELECT * FROM nfsenfscab ',
  'WHERE Empresa=' + Geral.FF0(Empresa),
  'AND Ambiente=' + Geral.FF0(Ambiente),
  'AND NfsNumero IN (' + SQLNums + ') ',
  '']);
  ImpressaoNFSe_Mostra(Empresa);
end;

procedure TDmNFSe_0000.ImpressaoNFSe_PorRPS_Mul(Empresa, Ambiente: Integer;
  Serie: TArrStr; Tipo, Numero: TArrInt);
var
  SQL: String;
  I: Integer;
begin
  SQL := '';
  for I := Low(Serie) to High(Serie) do
  begin
    if I > 0 then
      SQL := SQL + '  OR ' + #13#10;
    SQL := SQL +
    '  (NfsRpsIDSerie="' + Serie[I] + '" ' + #13#10 +
    '   AND NfsRpsIDTipo=' + Geral.FF0(Tipo[I]) + #13#10 +
    '   AND NfsRpsIDNumero=' + Geral.FF0(Numero[I]) + #13#10 +
    '  )' + #13#10;
  end;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrNFS, Dmod.MyDB, [
  'SELECT * FROM nfsenfscab ',
  'WHERE Empresa=' + Geral.FF0(Empresa),
  'AND Ambiente=' + Geral.FF0(Ambiente),
  'AND (',
  SQL + ')',
  '']);
  ImpressaoNFSe_Mostra(Empresa);
end;

procedure TDmNFSe_0000.ImpressaoNFSe_PorRPS_Uni(Empresa, Ambiente: Integer;
  Serie: String; Tipo, Numero: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrNFS, Dmod.MyDB, [
  'SELECT * FROM nfsenfscab ',
  'WHERE Empresa=' + Geral.FF0(Empresa),
  'AND Ambiente=' + Geral.FF0(Ambiente),
  'AND NfsRpsIDSerie="' + Serie + '" ',
  'AND NfsRpsIDTipo=' + Geral.FF0(Tipo),
  'AND NfsRpsIDNumero=' + Geral.FF0(Numero),
  '']);
  ImpressaoNFSe_Mostra(Empresa);
end;

function TDmNFSe_0000.InsUpdLoteRPSCab(Empresa, NFSeFatCab, QuantidadeRps: Integer;
  Sincroniza: Boolean): Integer;
var
  Cpf, Cnpj, InscricaoMunicipal, Id: String;
  Codigo, Sincronia, Ambiente, Status, NumeroLote: Integer;
  versao: Double;
begin
  Result := 0;
  DModG.ReopenEntNFS(Empresa);
  ReopenEmpresa(Empresa);
  //Codigo         := 0;
  //QuantidadeRps  := ;
  //NFSeFatCab     := ;
  //Empresa        := ;
  Status         := 0;
  Sincronia      := dmkPF.EscolhaDe2Int(Sincroniza, 1, 0);
  Cpf            := Geral.SoNumero_TT(DModG.QrEntNFSCPF.Value);
  Cnpj           := Geral.SoNumero_TT(DModG.QrEntNFSCNPJ.Value);
  InscricaoMunicipal:= Geral.SoNumero_TT(DModG.QrEntNFSNIRE.Value);
  //

  Ambiente       := QrFilialNFSeAmbiente.Value;
  versao         := QrFilialNFSeVersao.Value;

  //
  Codigo := UMyMod.BPGS1I32('nfselrpsc', 'Codigo', '', '', tsPos, stIns, 0);
  // Id             := 'Id' + Geral.FF0(Codigo);
  Id             := UnNFSe_PF_0000.FormataRPSNumero(Codigo);
  NumeroLote     := UnNFSe_PF_0000.ObtemProximoLoteRPS(Empresa, Ambiente);
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'nfselrpsc', False, [
  'Sincronia', 'NFSeFatCab', 'Ambiente',
  'Status', 'Empresa', 'NumeroLote',
  'Cpf', 'Cnpj', 'InscricaoMunicipal',
  'QuantidadeRps', 'versao', 'Id'], [
  'Codigo'], [
  Sincronia, NFSeFatCab, Ambiente,
  Status, Empresa, NumeroLote,
  Cpf, Cnpj, InscricaoMunicipal,
  QuantidadeRps, versao, Id], [
  Codigo], True) then
    Result := Codigo;
end;

function TDmNFSe_0000.InsUpdLoteRPSIts(Lote, DPS: Integer;
  LaAviso1, LaAviso2: TLabel): Boolean;
var
  Codigo, Controle, NFSeDPSCab, Status: Integer;
begin
  Codigo         := Lote;
  //Controle       := 0;
  NFSeDPSCab     := DPS;
  Status         := 0;
  //
  Controle := UMyMod.BPGS1I32('nfselrpsi', 'Controle', '', '', tsPos, stIns, 0);
  Result := UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'nfselrpsi', False, [
  'Codigo', 'NFSeDPSCab', 'Status'], [
  'Controle'], [
  Codigo, NFSeDPSCab, Status], [
  Controle], True);
  //
  DmNFSe_0000.StepNFSeRPS_Solo(DPS, DmNFSe_0000.stepAdedLote(),
    ['LastLRpsC'], [Lote], LaAviso1, LaAviso2);
end;

procedure TDmNFSe_0000.LoadXML(MyMemo: TMemo; MyWebBrowser: TWebBrowser;
  PageControl: TPageControl; ActivePageIndex: Integer);
  //
  function TextEh_ISO_8859_1(Texto: WideString): Boolean;
  var
    I, P: Integer;
  begin
    P := 0;
    for I := 1 to Length(Texto) do
    begin
      P := P + pos('�', Texto);
      P := P + pos('�', Texto);
      P := P + pos('�', Texto);
      P := P + pos('�', Texto);
      P := P + pos('�', Texto);
      P := P + pos('�', Texto);
      P := P + pos('�', Texto);
      P := P + pos('�', Texto);
      P := P + pos('�', Texto);
      P := P + pos('�', Texto);
      P := P + pos('�', Texto);
      P := P + pos('�', Texto);
      P := P + pos('�', Texto);
      P := P + pos('�', Texto);
      P := P + pos('�', Texto);
      P := P + pos('�', Texto);
      P := P + pos('�', Texto);
      P := P + pos('�', Texto);
      P := P + pos('�', Texto);
      P := P + pos('�', Texto);
      P := P + pos('�', Texto);
      P := P + pos('�', Texto);
      P := P + pos('�', Texto);
      P := P + pos('�', Texto);
      P := P + pos('�', Texto);
      P := P + pos('�', Texto);
    end;
    Result := P > 0;
  end;
var
  Arq: String;
  Texto: WideString;
begin
  Arq := ExtractFileDir(application.ExeName)+'temp.xml';
  //
  Texto := MyMemo.Lines.Text;
  if TextEh_ISO_8859_1(Texto) then
      Texto :=
        Geral.Substitui(Texto, 'encoding="UTF-8"', 'encoding="ISO-8859-1"');
  MyWebBrowser.Visible := True;
  //
  Geral.SalvaTextoEmArquivo(Arq, Texto, True);
  MyWebBrowser.Navigate(Arq);
  DeleteFile(Arq);
  if (PageControl <> nil) and (ActivePageIndex > -1) then
    PageControl.ActivePageIndex := ActivePageIndex;
  Application.ProcessMessages;
end;

function TDmNFSe_0000.ObtemCodigoRPS(Empresa, Tipo: Integer; Serie: String;
  Numero: Integer): Integer;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrGetCodRps, Dmod.MyDB, [
  'SELECT Codigo ',
  'FROM nfsedpscab ',
  'WHERE Empresa=' + Geral.FF0(Empresa),
  'AND RpsIDTipo=' + Geral.FF0(Tipo),
  'AND RpsIDSerie="' + Serie + '" ',
  'AND RpsIDNumero=' + Geral.FF0(Numero),
  '']);
  Result := QrGetCodRpsCodigo.Value;

  if Result = 0 then
  begin
    Geral.MB_Erro('N�o foi poss�vel localizar o cadastro do RPS:' + #13#10 +
    'Empresa: ' + Geral.FF0(Empresa) + #13#10 +
    'Tipo :' + Geral.FF0(Tipo) + #13#10 +
    'S�rie: ' + Serie + #13#10 +
    'N�mero: ' + Geral.FF0(Numero));
  end;
end;

function TDmNFSe_0000.ObtemDescricaoItemListaServico(
  ItemListaServico: String): String;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrILS, DModG.AllID_DB, [
  'SELECT CodTxt, Nome ',
  'FROM listserv ',
  'WHERE CodAlf="' + ItemListaServico + '"',
  '']);
  Result := QrILSNome.Value;
end;

procedure TDmNFSe_0000.ObtemMensagensRetornoLote(XML: WideString; Lote: Integer;
  Tag: String);
const
  TagC = 'Codigo';
  TagX = 'Mensagem';
  TagR = 'Correcao';
var
  Lista: TStringList;
  Str: String;
  posI, posF: Integer;
  Resto: AnsiString;
  TagI, TagF: String;
  //
  I: Integer;
  //StrCod, StrTxt,
  Txt: String;
var
  MsgCod, MsgTxt, MsgCor: String;
  Codigo, Controle: Integer;
begin
  Lista := TStringList.Create;
  Resto := XML;
  TagI := '<' + Tag;
  TagF := '</' + Tag + '>';
  try
    while pos(TagI, Resto)  > 0 do
    begin
      posI := pos(TagI, Resto);
      posF := pos(TagF, Resto);
      Str := Copy(Resto, posI, posF + Length(TagF) - posI);
      Lista.Add(Str);
      Resto := Copy(Resto, posI + Length(Str));
    end;
    //
    for I := 0 to Lista.Count - 1 do
    begin
      Str := Lista[I];
      if dmkPF.ObtemValorDeTag(Tag, Str, Txt) then
      if dmkPF.ObtemValorDeTag(TagC, Txt, MsgCod) then
      if dmkPF.ObtemValorDeTag(TagX, Txt, MsgTxt) then
      if dmkPF.ObtemValorDeTag(TagR, Txt, MsgCor) then
      // caso tenha mais texto:
      if Txt <> '' then
      Geral.MB_Erro('Tag/texto indefnido em mensagem de retorno: ' + #13#10 + Txt);
      //
      Codigo         := Lote;
      Controle := UMyMod.BPGS1I32('nfselrpscm', 'Controle', '', '', tsPos, stIns, 0);
      //
      UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'nfselrpscm', False, [
      'Codigo', 'MsgCod', 'MsgTxt', 'MsgCor'], [
      'Controle'], [
      Codigo, MsgCod, MsgTxt, MsgCor], [
      Controle], True);
    end;
  finally
    Lista.Free;
  end;
  //
  Geral.MensagemBox('ATEN��O!!' + #13#10 + 'Lote n�o aceito pelo web service!' +
  #13#10 + 'Lote n� ' + Geral.FF0(Lote), 'Aviso', MB_OK+MB_ICONWARNING);
  UnNFSe_PF_0000.MostraFormNFSeLRpsC_0201(Lote);
end;

function TDmNFSe_0000.ObtemNumeroNFSe_PeloRPS(const Empresa, Ambiente: Integer;
  const RPSTipo, RPSNumero: Integer; const RPSSerie: String;
  var NFSeNumero: Integer): Boolean;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrIDNFSe, Dmod.MyDB, [
  'SELECT NfsNumero ',
  'FROM nfsenfscab ',
  'WHERE Empresa=' + Geral.FF0(Empresa),
  'AND Ambiente=' + Geral.FF0(Ambiente),
  'AND NfsRpsIDSerie="' + RPSSerie + '"',
  'AND NfsRpsIDTipo=' + Geral.FF0(RPSTipo),
  'AND NfsRpsIDNumero=' + Geral.FF0(RPSNumero),
  '']);
  NFSeNumero := QrIDNFSeNfsNumero.Value;
  //
  Result := NFSeNumero <> 0;
end;

function TDmNFSe_0000.ObtemNumeroRPS_PeloNumeroNFSe(const Empresa, Ambiente:
  Integer; const NFSe: String; var Serie: String; var Tipo, Numero: Integer): Boolean;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrIDRps, Dmod.MyDB, [
  'SELECT NfsRpsIDSerie, NfsRpsIDTipo, NfsRpsIDNumero ',
  'FROM nfsenfscab ',
  'WHERE Empresa=' + Geral.FF0(Empresa),
  'AND Ambiente=' + Geral.FF0(Ambiente),
  'AND NfsNumero=' + Geral.SoNumero_TT(NFSe),
  '']);
  Serie := QrIDRpsNfsRpsIDSerie.Value;
  Tipo := QrIDRpsNfsRpsIDTipo.Value;
  Numero := QrIDRpsNfsRpsIDNumero.Value;
  //
  Result := Numero > 0;
end;

function TDmNFSe_0000.ObtemValorAproximadoDeTributos(
  const CodMunici: Integer; const LC116: String; Importado: Boolean;
  const vServ, vDesc: Double; var vBasTrib, pTotTrib, vTotTrib: Double;
  var tabTotTrib: TIBPTaxTabs; var verTotTrib: String;
  var tpAliqTotTrib: TIBPTaxOrig; var fontTotTrib: TIBPTaxFont;
  var Chave: String): Boolean;

  function ObtemUFDeCodMunici(CodMunici: Integer): String;
  var
    CodUF: String;
  begin
    CodUF  := Copy(Geral.FF0(CodMunici), 0, 2);
    Result := Geral.GetSiglaUF_do_CodigoUF_IBGE_DTB(Geral.IMV(CodUF));
  end;

const
  EXTIPI = '';
var
  EX: String;
begin
  Result        := False;
  pTotTrib      := 0;
  vTotTrib      := 0.0000;
  tabTotTrib    := TIBPTaxTabs.ibptaxtabNCM;
  verTotTrib    := '';
  tpAliqTotTrib := TIBPTaxOrig.ibptaxoriNaoInfo;
  fontTotTrib   := TIBPTaxFont.ibptaxfontNaoInfo;
  Chave         := '';
  //
  if LC116 = '' then
  begin
    Geral.MB_Aviso('N�o foi poss�vel obter o % de impostos da tabela IBPTax!' +
    slineBreak + 'C�digo do Servi�o (LC 116/03): n�o informado!');
    Exit;
  end else
  begin
    if EXTIPI = '' then
      EX := '0'
    else
      EX := Geral.SoNumero_TT(EXTIPI);
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrIBPTax, DModG.AllID_DB, [
      'SELECT * ',
      'FROM ibptax ',
      'WHERE Tabela=' + Geral.FF0(Integer(TIBPTaxTabs.ibptaxtabLC116)),
      'AND Codigo=' + Geral.SoNumero_TT(LC116),
      'AND UF="' + ObtemUFDeCodMunici(CodMunici) + '"',
      'AND Ex=' + EX,
      'AND VigenFim >= "' + Geral.FDT(DModG.ObtemAgora, 1) + '"',
      '']);
    if QrIBPTax.RecordCount > 0 then
    begin
      if not Importado then
      begin
        pTotTrib      := QrIBPTaxAliqNac.Value;
        tpAliqTotTrib := TIBPTaxOrig.ibptaxoriNacional;
      end else
      begin
        pTotTrib      := QrIBPTaxAliqImp.Value;
        tpAliqTotTrib := TIBPTaxOrig.ibptaxoriImportado;
      end;
(*==============================================================================
LEI N� 12.741, DE 8 DE DEZEMBRO DE 2012.
DECRETO N� 8.264, DE 5 DE JUNHO DE 2014
� 1�  Em rela��o � estimativa do valor dos tributos referidos no caput, n�o
ser�o computados valores que tenham sido eximidos por for�a de imunidades,
isen��es, redu��es e n�o incid�ncias eventualmente ocorrentes.
================================================================================
Manual de Integra��o de Olho no Imposto - vers�o 0.0.6
b) Quando temos desconto no valor total do cupom como fica?
- No caso de desconto pelo total, para efeitos de c�lculo do valor do imposto o
valor total descontado deve ser atribu�do item a item, e, sobre o valor com
desconto incondicional deve ser aplicada a al�quota aproximada disponibilizada
pelo IBPT
==============================================================================*)
      vBasTrib    := vServ - vDesc;
      vTotTrib    := vBasTrib * pTotTrib / 100;
      tabTotTrib  := TIBPTaxTabs.ibptaxtabLC116;
      verTotTrib  := QrIBPTaxVersao.Value;
      fontTotTrib := TIBPTaxFont.ibptaxfontIBPTax;
      Chave       := QrIBPTaxChave.Value;
      //
      Result := True;
    end else
      Geral.MB_Aviso('N�o foi poss�vel obter o % de impostos da tabela IBPTax!' +
        slineBreak + 'Ou a tabela pode estar desatualizada!' +
        slineBreak + 'LC 116/03: ' + LC116 + sLineBreak + 'EX: ' + EX);
  end;
end;

procedure TDmNFSe_0000.QrDPSCalcFields(DataSet: TDataSet);
var
  //Credor,
  Num: String;
begin
  if QrDPSTomaCnpj.Value <> '' then
    QrDPSTXT_DPS_TOMA_CNPJ_CPF.Value :=
    Geral.FormataCNPJ_TT(QrDPSTomaCnpj.Value)
  else
    QrDPSTXT_DPS_TOMA_CNPJ_CPF.Value :=
    Geral.FormataCNPJ_TT(QrDPSTomaCpf.Value);

  QrDPSTXT_DPS_TOMA_CEP.Value :=
    Geral.FormataCEP_TT(Geral.FF0(QrDPSTomaCep.Value));

  //

  Num := Geral.FormataNumeroDeRua(QrDPSTomaEndereco.Value,
    QrDPSTomaNumero.Value, False);
  QrDPSTXT_DPS_TOMA_ENDERECO.Value :=
    QrDPSTomaEndereco.Value + ', ' + Num;
  if QrDPSTomaComplemento.Value <> '' then
    QrDPSTXT_DPS_TOMA_ENDERECO.Value :=
    QrDPSTXT_DPS_TOMA_ENDERECO.Value + ' ' +
    QrDPSTomaComplemento.Value;
  //

  QrDPSTXT_DPS_TOMA_CIDADE.Value := DModG.ObtemNomeCidade(
    QrDPSTomaCodigoMunicipio.Value);
  if Dmod.QrControle.FieldByName('SoMaiusculas').AsString = 'V' then
    QrDPSTXT_DPS_TOMA_CIDADE.Value :=
    AnsiUppercase(QrDPSTXT_DPS_TOMA_CIDADE.Value);

  QrDPSTXT_DPS_TOMA_TELEFONE.Value := Geral.FormataTelefone_TT_Curto(
    QrDPSTomaContatoTelefone.Value);

  // O U T R O S

  QrDPSTXT_ItemListaServico.Value :=
    ObtemDescricaoItemListaServico(QrDPSItemListaServico.Value);


  case QrDPSResponsavelRetencao.Value of
      0: QrDPSTXT_RECOLHIMENTO.Value := 'Prestador';
      1: QrDPSTXT_RECOLHIMENTO.Value := 'Tomador';
      2: QrDPSTXT_RECOLHIMENTO.Value := 'Intermedi�rio';
    else QrDPSTXT_RECOLHIMENTO.Value := '';
  end;
  if QrDPSMunicipioIncidencia.Value > 0 then
    QrDPSTXT_MUNICIPIOCREDOR.Value :=
      DModG.ObtemNomeCidade_e_UF(QrDPSMunicipioIncidencia.Value)
  else
    QrDPSTXT_MUNICIPIOCREDOR.Value := DModG.ObtemNomeCidade_e_UF(
    QrDPSCodigoMunicipio.Value);
end;

procedure TDmNFSe_0000.QrNFSAfterScroll(DataSet: TDataSet);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrDPS, Dmod.MyDB, [
    // 2013-04-16 INI
{
  'SELECT * ',
  'FROM nfsedpscab ',
  'WHERE Empresa=' + Geral.FF0(QrNFSEmpresa.Value),
  'AND Ambiente=' + Geral.FF0(QrNFSAmbiente.Value),
  'AND RpsIDTipo=' + Geral.FF0(QrNFSNfsRpsIDTipo.Value),
  'AND RpsIDSerie="' + QrNFSNfsRpsIDSerie.Value + '" ',
  'AND RpsIDNumero=' + Geral.FF0(QrNFSNfsRpsIDNumero.Value),
}
  'SELECT nsc.PDFCompres, nsc.PDFEmbFont, ',
  'nsc.PDFPrnOptm, ndc.* ',
  'FROM nfsedpscab ndc',
  'LEFT JOIN nfsesrvcad nsc ON nsc.Codigo=ndc.NFSeSrvCad',
  'WHERE ndc.Empresa=' + Geral.FF0(QrNFSEmpresa.Value),
  'AND ndc.Ambiente=' + Geral.FF0(QrNFSAmbiente.Value),
  'AND ndc.RpsIDTipo=' + Geral.FF0(QrNFSNfsRpsIDTipo.Value),
  'AND ndc.RpsIDSerie="' + QrNFSNfsRpsIDSerie.Value + '" ',
  'AND ndc.RpsIDNumero=' + Geral.FF0(QrNFSNfsRpsIDNumero.Value),
    // 2013-04-16 FIM
  '']);
end;

procedure TDmNFSe_0000.QrNFSCalcFields(DataSet: TDataSet);
var
  Txt, Num: String;
begin
  if QrNFSDpsRpsIDTipo.Value > 0 then
  begin
    Txt := DModG.DefineTextoTipoRPS(QrNFSDpsRpsIDTipo.Value) + ' N� ' +
    Geral.FF0(QrNFSDpsRpsIDNumero.Value);
    if QrNFSDpsRpsIDSerie.Value <> '' then
      Txt := Txt + ' S�rie ' + QrNFSDpsRpsIDSerie.Value;
    Txt := Txt + ' de ' + Geral.FDT(QrNFSDpsRpsDataEmissao.Value, 2);
  end else
    Txt := '';
  QrNFSDADOS_RPS.Value := Txt;

  //

  if QrNFSNfsPrestaCnpj.Value <> '' then
    QrNFSTXT_NFS_PRESTA_CNPJ_CPF.Value :=
    Geral.FormataCNPJ_TT(QrNFSNfsPrestaCnpj.Value)
  else
    QrNFSTXT_NFS_PRESTA_CNPJ_CPF.Value :=
    Geral.FormataCNPJ_TT(QrNFSNfsPrestaCpf.Value);

  QrNFSTXT_NFS_PRESTA_CEP.Value :=
    Geral.FormataCEP_TT(QrNFSNfsPrestaCep.Value);

  //

  Num := Geral.FormataNumeroDeRua(QrNFSNfsPrestaEndereco.Value,
    QrNFSNfsPrestaNumero.Value, False);
  QrNFSTXT_NFS_PRESTA_ENDERECO.Value :=
    QrNFSNfsPrestaEndereco.Value + ',' + Num;
  if QrNFSNfsPrestaComplemento.Value <> '' then
    QrNFSTXT_NFS_PRESTA_ENDERECO.Value :=
    QrNFSTXT_NFS_PRESTA_ENDERECO.Value + ' ' +
    QrNFSNfsPrestaComplemento.Value;
  //

  QrNFSTXT_NFS_PRESTA_CIDADE.Value := DModG.ObtemNomeCidade(
    QrNFSNfsPrestaCodigoMunicipio.Value);
  if Dmod.QrControle.FieldByName('SoMaiusculas').AsString = 'V' then
    QrNFSTXT_DPS_TOMA_CIDADE.Value :=
    AnsiUppercase(QrNFSTXT_DPS_TOMA_CIDADE.Value);

  QrNFSTXT_NFS_PRESTA_TELEFONE.Value := Geral.FormataTelefone_TT_Curto(
    QrNFSNfsPrestaContatoTelefone.Value);



   // T O M A D O R



  if QrNFSDpsTomaCnpj.Value <> '' then
    QrNFSTXT_DPS_TOMA_CNPJ_CPF.Value :=
    Geral.FormataCNPJ_TT(QrNFSDpsTomaCnpj.Value)
  else
    QrNFSTXT_DPS_TOMA_CNPJ_CPF.Value :=
    Geral.FormataCNPJ_TT(QrNFSDpsTomaCpf.Value);

  QrNFSTXT_DPS_TOMA_CEP.Value :=
    Geral.FormataCEP_TT(Geral.FF0(QrNFSDpsTomaCep.Value));

  //

  Num := Geral.FormataNumeroDeRua(QrNFSDpsTomaEndereco.Value,
    QrNFSDpsTomaNumero.Value, False);
  QrNFSTXT_DPS_TOMA_ENDERECO.Value :=
    QrNFSDpsTomaEndereco.Value + Num;
  if QrNFSDpsTomaComplemento.Value <> '' then
    QrNFSTXT_DPS_TOMA_ENDERECO.Value :=
    QrNFSTXT_DPS_TOMA_ENDERECO.Value + ' ' +
    QrNFSDpsTomaComplemento.Value;
  //

  QrNFSTXT_DPS_TOMA_CIDADE.Value := DModG.ObtemNomeCidade(
    QrNFSDpsTomaCodigoMunicipio.Value);

  QrNFSTXT_DPS_TOMA_TELEFONE.Value := Geral.FormataTelefone_TT_Curto(
    QrNFSDpsTomaContatoTelefone.Value);

  // O U T R O S

  QrNFSTXT_ItemListaServico.Value :=
    ObtemDescricaoItemListaServico(QrNFSDpsItemListaServico.Value);

  case QrNFSDpsResponsavelRetencao.Value of
      0: QrNFSTXT_RECOLHIMENTO.Value := 'Prestador';
      1: QrNFSTXT_RECOLHIMENTO.Value := 'Tomador';
      2: QrNFSTXT_RECOLHIMENTO.Value := 'Intermedi�rio';
    else QrNFSTXT_RECOLHIMENTO.Value := '';
  end;

  if QrNFSDpsMunicipioIncidencia.Value > 0 then
    QrNFSTXT_MUNICIPIOCREDOR.Value :=
      DModG.ObtemNomeCidade_e_UF(QrNFSDpsMunicipioIncidencia.Value)
  else
    QrNFSTXT_MUNICIPIOCREDOR.Value := DModG.ObtemNomeCidade_e_UF(
    QrNFSDpsCodigoMunicipio.Value);
end;

procedure TDmNFSe_0000.QrNFSeDPSCabCalcFields(DataSet: TDataSet);
begin
  if QrNFSeDPSCabPrestaCnpj.Value <> '' then
    QrNFSeDPSCabPRESTA_CNPJ_CPF.Value := QrNFSeDPSCabPrestaCnpj.Value
  else
    QrNFSeDPSCabPRESTA_CNPJ_CPF.Value := QrNFSeDPSCabPrestaCpf.Value;

  //

  if QrNFSeDPSCabTomaCnpj.Value <> '' then
    QrNFSeDPSCabTOMA_CNPJ_CPF.Value := QrNFSeDPSCabTomaCnpj.Value
  else
    QrNFSeDPSCabTOMA_CNPJ_CPF.Value := QrNFSeDPSCabTomaCpf.Value;

  //

  if QrNFSeDPSCabIntermeCnpj.Value <> '' then
    QrNFSeDPSCabINTERME_CNPJ_CPF.Value := QrNFSeDPSCabIntermeCnpj.Value
  else
    QrNFSeDPSCabINTERME_CNPJ_CPF.Value := QrNFSeDPSCabIntermeCpf.Value;

  //

end;

procedure TDmNFSe_0000.ReadPixelsPerInch(Reader: TReader);
begin
  FPixelsPerInch := Reader.ReadInteger;
end;

function TDmNFSe_0000.ReopenEmpresa(Empresa: Integer): Boolean;
begin
{
  QrEmpresa.Close;
  QrEmpresa.Params[0].AsInteger := Empresa;
  UMyMod.AbreQuery(QrEmpresa, Dmod.MyDB, 'TDmNFe_0000.ReopenEmpresa()');
}
  UnDmkDAC_PF.AbreMySQLQuery0(QrEmpresa, Dmod.MyDB, [
  'SELECT en.Codigo, en.Tipo, en.CNPJ, en.CPF,  ',
  'en.IE, en.IEST, en.RG, en.NIRE, en.CNAE,  uf.DTB DTB_UF, ',
  'IF(en.Tipo=0,en.RazaoSocial,en.Nome) NO_ENT, ',
  'IF(en.Tipo=0,en.Fantasia,en.Apelido) FANTASIA, ',
  'IF(en.Tipo=0,en.ERua,en.PRua) RUA, ',
  'IF(en.Tipo=0,en.ENumero,en.PNumero) + 0.000 Numero, ',
  'IF(en.Tipo=0,en.ECompl,en.PCompl) COMPL, ',
  'IF(en.Tipo=0,en.EBairro,en.PBairro) BAIRRO, ',
  'IF(en.Tipo=0,en.ECEP,en.PCEP) + 0.000 CEP, ',
  'IF(en.Tipo=0,en.ETe1,en.PTe1) Te1, ',
  'IF(en.Tipo=0,en.EUF,en.PUF) + 0.000 UF, ',
  'IF(en.Tipo=0,en.ECodiPais,en.PCodiPais) + 0.000 CodiPais, ',
  ' ',
  'IF(en.Tipo=0,en.ECodMunici,en.PCodMunici) + 0.000 CodMunici, ',
  'll.Nome NO_LOGRAD, mu.Nome NO_Munici, uf.Nome NO_UF, ',
  'bpa.Nome NO_Pais, en.SUFRAMA, en.Filial ',
  ' ',
  'FROM entidades en ',
  'LEFT JOIN listalograd ll ON ll.Codigo=IF(en.Tipo=0,en.ELograd,en.PLograd)  ',
  'LEFT JOIN ' + VAR_AllID_DB_NOME + '.dtb_munici mu ON mu.Codigo=IF(en.Tipo=0,en.ECodMunici,en.PCodMunici) ',
  'LEFT JOIN ufs uf ON uf.Codigo=IF(en.Tipo=0,en.EUF,en.PUF) ',
  'LEFT JOIN ' + VAR_AllID_DB_NOME + '.bacen_pais bpa ON bpa.Codigo=IF(en.Tipo=0,en.ECodiPais,en.PCodiPais) ',
  'WHERE en.Codigo=' + Geral.FF0(Empresa),
  '']);
  //
  QrFilial.Close;
  QrFilial.Params[0].AsInteger := Empresa;
  UMyMod.AbreQuery(QrFilial, Dmod.MyDB, 'TDmNFe_0000.ReopenEmpresa()');
  //
  Result := (QrEmpresa.RecordCount > 0) and (QrFilial.RecordCount > 0);
  if not Result then Geral.MensagemBox(
  'N�o foi poss�vel reabrir as tabelas da empresa ' + IntToStr(Empresa) + '!',
  'Aviso', MB_OK+MB_ICONWARNING);
end;

function TDmNFSe_0000.ReopenNFSeArq(const Tabela: TTabelaArqXML; const Empresa,
  Ambiente, Codigo: Integer; var XML: AnsiString; const FormCaption: String):
  Boolean;
var
  TabNome: String;
  Arq: TStringList;
begin
  Result := False;
  case Tabela of
    txmRPS: TabNome := 'nfsearqrps';
    txmNFS: TabNome := 'nfsearqnfs';
    else TabNome := '??tab??';
  end;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrNFSeArq, Dmod.MyDB, [
  'SELECT * ',
  'FROM ' + TabNome,
  'WHERE Codigo=' + Geral.FF0(Codigo),
  'AND Empresa=' + Geral.FF0(Empresa),
  'AND Ambiente=' + Geral.FF0(Ambiente),
  '']);
  //

  XML := QrNFSeArqXML.Value;
  // ini 2023-12-22
  //Result := QrNFSeArq.RecordCount > 0;
  Result := XML <> EmptyStr;
  if not Result and (FormCaption  <> '') then
  begin
    if QrNFSeArq.RecordCount = 0 then
      Geral.MB_Aviso('XML vazio. Registro n�o localizado!' + #13#10 +
      'Tabela: ' + TabNome + #13#10 +
      'Empresa: ' + Geral.FF0(Empresa) + #13#10 +
      'Ambiente:' + Geral.FF0(Ambiente) + #13#10 +
      'ID: ' + Geral.FF0(Codigo) + #13#10 +
      'Janela: ' + Copy(FormCaption, 1, 13))
    else
      Geral.MB_Aviso('Registro localizado, mas o XML est� vazio!' + #13#10 +
      'Tabela: ' + TabNome + #13#10 +
      'Empresa: ' + Geral.FF0(Empresa) + #13#10 +
      'Ambiente:' + Geral.FF0(Ambiente) + #13#10 +
      'ID: ' + Geral.FF0(Codigo) + #13#10 +
      'Janela: ' + Copy(FormCaption, 1, 13));
    //
    Screen.Cursor := crDefault;
    {
    OpenDialog1.Title := 'Selecione o Rps';
    OpenDialog1.DefaultExt := '*-Rps.xml';
    OpenDialog1.Filter :=
      'Arquivos Rps (*-Rps.xml)|*-Rps.xml|Arquivos XML (*.xml)|*.xml|Todos os Arquivos (*.*)|*.*';
    OpenDialog1.InitialDir := ACBrNFSe1.Configuracoes.Arquivos.PathSalvar;

    if OpenDialog1.Execute then
    begin
      Arq := TStringList.Create;
      try
        Arq.LoadFromFile(OpenDialog1.FileName);
        XML := Arq.Text;
        Result := XML <> EmptyStr;
      finally
        if Arq <> nil then
          Arq.Free;
      end;
    end;
    }
  end;
end;

procedure TDmNFSe_0000.ReopenNFSeDPSCab(Codigo: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrNFSeDPSCab, Dmod.MyDB, [
  'SELECT * ',
  'FROM nfsedpscab ',
  'WHERE Codigo=' + Geral.FF0(Codigo),
  '']);
end;

procedure TDmNFSe_0000.ReopenNFSeLRpsC(Codigo: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrNFSeLRpsC, Dmod.MyDB, [
  'SELECT * ',
  'FROM nfselrpsc ',
  'WHERE Codigo=' + Geral.FF0(Codigo),
  '']);
end;

procedure TDmNFSe_0000.ReopenNFSeLRpsCIts(Codigo: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrNFSeLRpsI, Dmod.MyDB, [
  'SELECT nri.Codigo, nri.NFSeDPSCab, dps.Empresa, dps.Ambiente ',
  'FROM nfselrpsi nri ',
  'LEFT JOIN nfsedpscab dps ON dps.Codigo=nri.NFSeDPSCab ',
  'WHERE nri.Codigo=' + Geral.FF0(Codigo),
  '']);
end;

function TDmNFSe_0000.stepDados: Integer;
begin
  Result := 10;
end;

function TDmNFSe_0000.stepGerada: Integer;
begin
  Result := 20;
end;

function TDmNFSe_0000.stepAssinado: Integer;
begin
  Result := 30;
end;

function TDmNFSe_0000.stepLoteRejeitado: Integer;
begin
  Result := 40;
end;

function TDmNFSe_0000.stepAdedLote: Integer;
begin
  Result := 50;
end;

function TDmNFSe_0000.stepLoteEnvEnviado: Integer;
begin
  Result := 60;
end;

function TDmNFSe_0000.stepLoteEnvConsulta: Integer;
begin
  Result := 70;
end;

function TDmNFSe_0000.stepNfseEmitida: Integer;
begin
  Result := 100;
end;

function TDmNFSe_0000.StepNFSeRPS_Lote(Lote, Status: Integer;
  Campos: array of String; Valores: array of Variant; LaAviso1,
  LaAviso2: TLabel): Boolean;
var
  Codigo: Integer;
begin
  Result := False;
  Codigo := Lote;
  UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'nfselrpsc', False, [
  'Status'], ['Codigo'], [Status], [Codigo], True);
  //
  UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'nfselrpsi', False, [
  'Status'], ['Codigo'], [Status], [Codigo], True);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrItensLoteRPS, Dmod.MyDB, [
  'SELECT NFSeDPSCab ',
  'FROM nfselrpsi ',
  'WHERE Codigo=' + Geral.FF0(Lote),
  '']);
  //
  QrItensLoteRPS.First;
  while not QrItensLoteRPS.Eof do
  begin
    // Atualiza RPS no BD
    Codigo := QrItensLoteRPSNFSeDPSCab.Value;
    StepNFSeRPS_Solo(Codigo, Status, Campos, Valores, LaAviso1, LaAviso2);
    //
    QrItensLoteRPS.Next;
  end;
end;

function TDmNFSe_0000.StepNFSeRPS_Solo(Codigo, Status: Integer;
Campos: array of String; Valores: array of Variant;
LaAviso1, LaAviso2: TLabel): Boolean;
var
  ComplementoSQL: String;
  I: Integer;
begin
  ComplementoSQL := '';
  for I := Low(Campos) to High(Campos) do
  begin
    ComplementoSQL := ComplementoSQL + #13#10 +
      ', ' + Campos[I] + '=' + Geral.VariavelToString(Valores[I]);
  end;
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Atualizando status da NFSe para ' + IntToStr(Status));
  UMyMod.ExecutaMySQLQuery1(Dmod.QrUpd, [
  'UPDATE nfsedpscab SET Status=' + Geral.FF0(Status),
  ComplementoSQL,
  'WHERE Codigo=' + Geral.FF0(Codigo),
  '']);
  //
  Result := True;
end;

function TDmNFSe_0000.TituloArqXML(const Ext: String; var Titulo,
  Descri: String): Boolean;
begin
  Result := True;
  if Ext = NFSE_DPS_GER then
  begin
    Titulo := 'RPS';
    Descri := 'O nome do arquivo ser� o c�digo da DPS com extens�o �-dps.xml�';
  end else
  begin
    Result := False;
    Titulo := 'Tipo de arquivo XML desconhecido para NFS-e';
    Descri := 'AVISE A DERMATEK!';
  end;
end;

procedure TDmNFSe_0000.WritePixelsPerInch(Writer: TWriter);
begin
  Writer.WriteInteger(FPixelsPerInch);
end;

end.
