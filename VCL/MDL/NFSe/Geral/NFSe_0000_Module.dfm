object DmNFSe_0000: TDmNFSe_0000
  Height = 420
  Width = 701
  PixelsPerInch = 96
  object QrItensLoteRPS: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT NFSeDPSCab '
      'FROM nfselrpsi'
      'WHERE Codigo>0')
    Left = 212
    Top = 52
    object QrItensLoteRPSNFSeDPSCab: TIntegerField
      FieldName = 'NFSeDPSCab'
    end
  end
  object QrNFSeDPSCab: TMySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrNFSeDPSCabCalcFields
    SQL.Strings = (
      'SELECT *'
      'FROM nfsedpscab'
      'WHERE Codigo=:P0')
    Left = 32
    Top = 4
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrNFSeDPSCabCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrNFSeDPSCabAmbiente: TSmallintField
      FieldName = 'Ambiente'
    end
    object QrNFSeDPSCabEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrNFSeDPSCabCliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrNFSeDPSCabIntermediario: TIntegerField
      FieldName = 'Intermediario'
    end
    object QrNFSeDPSCabNFSeSrvCad: TIntegerField
      FieldName = 'NFSeSrvCad'
    end
    object QrNFSeDPSCabRpsID: TWideStringField
      FieldName = 'RpsID'
      Size = 255
    end
    object QrNFSeDPSCabRpsIDNumero: TIntegerField
      FieldName = 'RpsIDNumero'
    end
    object QrNFSeDPSCabRpsIDSerie: TWideStringField
      FieldName = 'RpsIDSerie'
      Size = 5
    end
    object QrNFSeDPSCabRpsIDTipo: TSmallintField
      FieldName = 'RpsIDTipo'
    end
    object QrNFSeDPSCabRpsDataEmissao: TDateField
      FieldName = 'RpsDataEmissao'
    end
    object QrNFSeDPSCabRpsHoraEmissao: TTimeField
      FieldName = 'RpsHoraEmissao'
    end
    object QrNFSeDPSCabRpsStatus: TIntegerField
      FieldName = 'RpsStatus'
    end
    object QrNFSeDPSCabCompetencia: TDateField
      FieldName = 'Competencia'
    end
    object QrNFSeDPSCabSubstNumero: TIntegerField
      FieldName = 'SubstNumero'
    end
    object QrNFSeDPSCabSubstSerie: TWideStringField
      FieldName = 'SubstSerie'
      Size = 5
    end
    object QrNFSeDPSCabSubstTipo: TSmallintField
      FieldName = 'SubstTipo'
    end
    object QrNFSeDPSCabValorServicos: TFloatField
      FieldName = 'ValorServicos'
    end
    object QrNFSeDPSCabValorDeducoes: TFloatField
      FieldName = 'ValorDeducoes'
    end
    object QrNFSeDPSCabValorPis: TFloatField
      FieldName = 'ValorPis'
    end
    object QrNFSeDPSCabValorCofins: TFloatField
      FieldName = 'ValorCofins'
    end
    object QrNFSeDPSCabValorInss: TFloatField
      FieldName = 'ValorInss'
    end
    object QrNFSeDPSCabValorIr: TFloatField
      FieldName = 'ValorIr'
    end
    object QrNFSeDPSCabValorCsll: TFloatField
      FieldName = 'ValorCsll'
    end
    object QrNFSeDPSCabOutrasRetencoes: TFloatField
      FieldName = 'OutrasRetencoes'
    end
    object QrNFSeDPSCabValorIss: TFloatField
      FieldName = 'ValorIss'
    end
    object QrNFSeDPSCabAliquota: TFloatField
      FieldName = 'Aliquota'
    end
    object QrNFSeDPSCabDescontoIncondicionado: TFloatField
      FieldName = 'DescontoIncondicionado'
    end
    object QrNFSeDPSCabDescontoCondicionado: TFloatField
      FieldName = 'DescontoCondicionado'
    end
    object QrNFSeDPSCabIssRetido: TSmallintField
      FieldName = 'IssRetido'
    end
    object QrNFSeDPSCabResponsavelRetencao: TSmallintField
      FieldName = 'ResponsavelRetencao'
    end
    object QrNFSeDPSCabItemListaServico: TWideStringField
      FieldName = 'ItemListaServico'
      Size = 5
    end
    object QrNFSeDPSCabCodigoCnae: TWideStringField
      FieldName = 'CodigoCnae'
    end
    object QrNFSeDPSCabCodigoTributacaoMunicipio: TWideStringField
      FieldName = 'CodigoTributacaoMunicipio'
    end
    object QrNFSeDPSCabDiscriminacao: TWideMemoField
      FieldName = 'Discriminacao'
      Required = True
      BlobType = ftWideMemo
      Size = 4
    end
    object QrNFSeDPSCabCodigoMunicipio: TIntegerField
      FieldName = 'CodigoMunicipio'
    end
    object QrNFSeDPSCabCodigoPais: TIntegerField
      FieldName = 'CodigoPais'
    end
    object QrNFSeDPSCabExigibilidadeISS: TSmallintField
      FieldName = 'ExigibilidadeISS'
    end
    object QrNFSeDPSCabMunicipioIncidencia: TIntegerField
      FieldName = 'MunicipioIncidencia'
    end
    object QrNFSeDPSCabNumeroProcesso: TWideStringField
      FieldName = 'NumeroProcesso'
      Size = 30
    end
    object QrNFSeDPSCabPrestaCpf: TWideStringField
      FieldName = 'PrestaCpf'
      Size = 18
    end
    object QrNFSeDPSCabPrestaCnpj: TWideStringField
      FieldName = 'PrestaCnpj'
      Size = 18
    end
    object QrNFSeDPSCabPrestaInscricaoMunicipal: TWideStringField
      FieldName = 'PrestaInscricaoMunicipal'
      Size = 18
    end
    object QrNFSeDPSCabTomaCpf: TWideStringField
      FieldName = 'TomaCpf'
      Size = 18
    end
    object QrNFSeDPSCabTomaCnpj: TWideStringField
      FieldName = 'TomaCnpj'
      Size = 18
    end
    object QrNFSeDPSCabTomaInscricaoMunicipal: TWideStringField
      FieldName = 'TomaInscricaoMunicipal'
      Size = 18
    end
    object QrNFSeDPSCabTomaRazaoSocial: TWideStringField
      FieldName = 'TomaRazaoSocial'
      Size = 150
    end
    object QrNFSeDPSCabTomaEndereco: TWideStringField
      FieldName = 'TomaEndereco'
      Size = 125
    end
    object QrNFSeDPSCabTomaNumero: TWideStringField
      FieldName = 'TomaNumero'
      Size = 10
    end
    object QrNFSeDPSCabTomaComplemento: TWideStringField
      FieldName = 'TomaComplemento'
      Size = 60
    end
    object QrNFSeDPSCabTomaBairro: TWideStringField
      FieldName = 'TomaBairro'
      Size = 60
    end
    object QrNFSeDPSCabTomaCodigoMunicipio: TIntegerField
      FieldName = 'TomaCodigoMunicipio'
    end
    object QrNFSeDPSCabTomaUf: TWideStringField
      FieldName = 'TomaUf'
      Size = 125
    end
    object QrNFSeDPSCabTomaCodigoPais: TIntegerField
      FieldName = 'TomaCodigoPais'
    end
    object QrNFSeDPSCabTomaCep: TIntegerField
      FieldName = 'TomaCep'
    end
    object QrNFSeDPSCabTomaContatoTelefone: TWideStringField
      FieldName = 'TomaContatoTelefone'
    end
    object QrNFSeDPSCabTomaContatoEmail: TWideStringField
      FieldName = 'TomaContatoEmail'
      Size = 80
    end
    object QrNFSeDPSCabIntermeCpf: TWideStringField
      FieldName = 'IntermeCpf'
      Size = 18
    end
    object QrNFSeDPSCabIntermeCnpj: TWideStringField
      FieldName = 'IntermeCnpj'
      Size = 18
    end
    object QrNFSeDPSCabIntermeInscricaoMunicipal: TWideStringField
      FieldName = 'IntermeInscricaoMunicipal'
      Size = 18
    end
    object QrNFSeDPSCabIntermeRazaoSocial: TWideStringField
      FieldName = 'IntermeRazaoSocial'
      Size = 150
    end
    object QrNFSeDPSCabConstrucaoCivilCodigoObra: TWideStringField
      FieldName = 'ConstrucaoCivilCodigoObra'
      Size = 15
    end
    object QrNFSeDPSCabConstrucaoCivilArt: TWideStringField
      FieldName = 'ConstrucaoCivilArt'
      Size = 15
    end
    object QrNFSeDPSCabRegimeEspecialTributacao: TSmallintField
      FieldName = 'RegimeEspecialTributacao'
    end
    object QrNFSeDPSCabOptanteSimplesNacional: TSmallintField
      FieldName = 'OptanteSimplesNacional'
    end
    object QrNFSeDPSCabIncentivoFiscal: TSmallintField
      FieldName = 'IncentivoFiscal'
    end
    object QrNFSeDPSCabId: TWideStringField
      FieldName = 'Id'
      Size = 255
    end
    object QrNFSeDPSCabLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrNFSeDPSCabDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrNFSeDPSCabDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrNFSeDPSCabUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrNFSeDPSCabUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrNFSeDPSCabAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrNFSeDPSCabAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrNFSeDPSCabPRESTA_CNPJ_CPF: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'PRESTA_CNPJ_CPF'
      Size = 30
      Calculated = True
    end
    object QrNFSeDPSCabTOMA_CNPJ_CPF: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TOMA_CNPJ_CPF'
      Size = 30
      Calculated = True
    end
    object QrNFSeDPSCabINTERME_CNPJ_CPF: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'INTERME_CNPJ_CPF'
      Size = 30
      Calculated = True
    end
    object QrNFSeDPSCabStatus: TIntegerField
      FieldName = 'Status'
    end
    object QrNFSeDPSCabLastLRpsC: TIntegerField
      FieldName = 'LastLRpsC'
    end
    object QrNFSeDPSCabQuemPagaISS: TSmallintField
      FieldName = 'QuemPagaISS'
    end
    object QrNFSeDPSCabNaturezaOperacao: TIntegerField
      FieldName = 'NaturezaOperacao'
    end
  end
  object QrEmpresa: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT en.Codigo, en.Tipo, en.CNPJ, en.CPF, '
      'en.IE, en.IEST, en.RG, en.NIRE, en.CNAE,  uf.DTB DTB_UF,'
      'IF(en.Tipo=0,en.RazaoSocial,en.Nome) NO_ENT,'
      'IF(en.Tipo=0,en.Fantasia,en.Apelido) FANTASIA,'
      'IF(en.Tipo=0,en.ERua,en.PRua) RUA,'
      'IF(en.Tipo=0,en.ENumero,en.PNumero) + 0.000 Numero,'
      'IF(en.Tipo=0,en.ECompl,en.PCompl) COMPL,'
      'IF(en.Tipo=0,en.EBairro,en.PBairro) BAIRRO,'
      'IF(en.Tipo=0,en.ECEP,en.PCEP) + 0.000 CEP,'
      'IF(en.Tipo=0,en.ETe1,en.PTe1) Te1,'
      'IF(en.Tipo=0,en.EUF,en.PUF) + 0.000 UF,'
      'IF(en.Tipo=0,en.ECodiPais,en.PCodiPais) + 0.000 CodiPais,'
      ''
      'IF(en.Tipo=0,en.ECodMunici,en.PCodMunici) + 0.000 CodMunici,'
      'll.Nome NO_LOGRAD, mu.Nome NO_Munici, uf.Nome NO_UF,'
      'bpa.Nome NO_Pais, en.SUFRAMA, en.Filial'
      ''
      'FROM entidades en'
      
        'LEFT JOIN listalograd ll ON ll.Codigo=IF(en.Tipo=0,en.ELograd,en' +
        '.PLograd) '
      
        'LEFT JOIN dtb_munici mu ON mu.Codigo=IF(en.Tipo=0,en.ECodMunici,' +
        'en.PCodMunici)'
      'LEFT JOIN ufs uf ON uf.Codigo=IF(en.Tipo=0,en.EUF,en.PUF)'
      
        'LEFT JOIN bacen_pais bpa ON bpa.Codigo=IF(en.Tipo=0,en.ECodiPais' +
        ',en.PCodiPais)'
      'WHERE en.Codigo=:P0'
      ''
      '')
    Left = 32
    Top = 52
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrEmpresaCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEmpresaTipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrEmpresaCNPJ: TWideStringField
      FieldName = 'CNPJ'
      Size = 18
    end
    object QrEmpresaCPF: TWideStringField
      FieldName = 'CPF'
      Size = 18
    end
    object QrEmpresaIE: TWideStringField
      FieldName = 'IE'
    end
    object QrEmpresaRG: TWideStringField
      FieldName = 'RG'
      Size = 15
    end
    object QrEmpresaCNAE: TWideStringField
      FieldName = 'CNAE'
      Size = 7
    end
    object QrEmpresaNO_ENT: TWideStringField
      FieldName = 'NO_ENT'
      Required = True
      Size = 100
    end
    object QrEmpresaFANTASIA: TWideStringField
      FieldName = 'FANTASIA'
      Required = True
      Size = 60
    end
    object QrEmpresaRUA: TWideStringField
      FieldName = 'RUA'
      Size = 30
    end
    object QrEmpresaCOMPL: TWideStringField
      FieldName = 'COMPL'
      Size = 30
    end
    object QrEmpresaBAIRRO: TWideStringField
      FieldName = 'BAIRRO'
      Size = 30
    end
    object QrEmpresaTe1: TWideStringField
      FieldName = 'Te1'
    end
    object QrEmpresaNO_LOGRAD: TWideStringField
      FieldName = 'NO_LOGRAD'
      Size = 10
    end
    object QrEmpresaNO_Munici: TWideStringField
      FieldName = 'NO_Munici'
      Size = 100
    end
    object QrEmpresaNO_UF: TWideStringField
      FieldName = 'NO_UF'
      Required = True
      Size = 2
    end
    object QrEmpresaDTB_UF: TWideStringField
      FieldName = 'DTB_UF'
      Required = True
      Size = 2
    end
    object QrEmpresaNO_Pais: TWideStringField
      FieldName = 'NO_Pais'
      Size = 100
    end
    object QrEmpresaIEST: TWideStringField
      FieldName = 'IEST'
    end
    object QrEmpresaSUFRAMA: TWideStringField
      FieldName = 'SUFRAMA'
      Size = 9
    end
    object QrEmpresaFilial: TIntegerField
      FieldName = 'Filial'
    end
    object QrEmpresaNIRE: TWideStringField
      FieldName = 'NIRE'
      Size = 15
    end
    object QrEmpresaNumero: TFloatField
      FieldName = 'Numero'
    end
    object QrEmpresaCEP: TFloatField
      FieldName = 'CEP'
    end
    object QrEmpresaCodiPais: TFloatField
      FieldName = 'CodiPais'
    end
    object QrEmpresaCodMunici: TFloatField
      FieldName = 'CodMunici'
    end
    object QrEmpresaUF: TFloatField
      FieldName = 'UF'
    end
  end
  object QrFilial: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM paramsemp'
      'WHERE Codigo=:P0')
    Left = 32
    Top = 100
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrFilialSimplesFed: TSmallintField
      FieldName = 'SimplesFed'
      Origin = 'paramsemp.SimplesFed'
    end
    object QrFilialUF_WebServ: TWideStringField
      FieldName = 'UF_WebServ'
      Origin = 'paramsemp.UF_WebServ'
      Size = 2
    end
    object QrFilialDirNFeGer: TWideStringField
      FieldName = 'DirNFeGer'
      Origin = 'paramsemp.DirNFeGer'
      Size = 255
    end
    object QrFilialDirNFeAss: TWideStringField
      FieldName = 'DirNFeAss'
      Origin = 'paramsemp.DirNFeAss'
      Size = 255
    end
    object QrFilialDirEnvLot: TWideStringField
      FieldName = 'DirEnvLot'
      Origin = 'paramsemp.DirEnvLot'
      Size = 255
    end
    object QrFilialDirRec: TWideStringField
      FieldName = 'DirRec'
      Origin = 'paramsemp.DirRec'
      Size = 255
    end
    object QrFilialDirPedRec: TWideStringField
      FieldName = 'DirPedRec'
      Origin = 'paramsemp.DirPedRec'
      Size = 255
    end
    object QrFilialDirProRec: TWideStringField
      FieldName = 'DirProRec'
      Origin = 'paramsemp.DirProRec'
      Size = 255
    end
    object QrFilialDirDen: TWideStringField
      FieldName = 'DirDen'
      Origin = 'paramsemp.DirDen'
      Size = 255
    end
    object QrFilialDirPedCan: TWideStringField
      FieldName = 'DirPedCan'
      Origin = 'paramsemp.DirPedCan'
      Size = 255
    end
    object QrFilialDirCan: TWideStringField
      FieldName = 'DirCan'
      Origin = 'paramsemp.DirCan'
      Size = 255
    end
    object QrFilialDirPedInu: TWideStringField
      FieldName = 'DirPedInu'
      Origin = 'paramsemp.DirPedInu'
      Size = 255
    end
    object QrFilialDirInu: TWideStringField
      FieldName = 'DirInu'
      Origin = 'paramsemp.DirInu'
      Size = 255
    end
    object QrFilialDirPedSit: TWideStringField
      FieldName = 'DirPedSit'
      Origin = 'paramsemp.DirPedSit'
      Size = 255
    end
    object QrFilialDirSit: TWideStringField
      FieldName = 'DirSit'
      Origin = 'paramsemp.DirSit'
      Size = 255
    end
    object QrFilialDirPedSta: TWideStringField
      FieldName = 'DirPedSta'
      Origin = 'paramsemp.DirPedSta'
      Size = 255
    end
    object QrFilialDirSta: TWideStringField
      FieldName = 'DirSta'
      Origin = 'paramsemp.DirSta'
      Size = 255
    end
    object QrFilialSiglaCustm: TWideStringField
      FieldName = 'SiglaCustm'
      Origin = 'paramsemp.SiglaCustm'
      Size = 15
    end
    object QrFilialInfoPerCuz: TSmallintField
      FieldName = 'InfoPerCuz'
      Origin = 'paramsemp.InfoPerCuz'
    end
    object QrFilialUF_Servico: TWideStringField
      FieldName = 'UF_Servico'
      Origin = 'paramsemp.UF_Servico'
      Size = 4
    end
    object QrFilialNFeSerNum: TWideStringField
      FieldName = 'NFeSerNum'
      Origin = 'paramsemp.NFeSerNum'
      Size = 255
    end
    object QrFilialSINTEGRA_Path: TWideStringField
      FieldName = 'SINTEGRA_Path'
      Origin = 'paramsemp.SINTEGRA_Path'
      Size = 255
    end
    object QrFilialDirDANFEs: TWideStringField
      FieldName = 'DirDANFEs'
      Origin = 'paramsemp.DirDANFEs'
      Size = 255
    end
    object QrFilialDirNFeProt: TWideStringField
      FieldName = 'DirNFeProt'
      Origin = 'paramsemp.DirNFeProt'
      Size = 255
    end
    object QrFilialNFeSerVal: TDateField
      FieldName = 'NFeSerVal'
      Origin = 'paramsemp.NFeSerVal'
    end
    object QrFilialNFeSerAvi: TSmallintField
      FieldName = 'NFeSerAvi'
      Origin = 'paramsemp.NFeSerAvi'
    end
    object QrFilialNFetpEmis: TSmallintField
      FieldName = 'NFetpEmis'
      Origin = 'paramsemp.NFetpEmis'
    end
    object QrFilialSCAN_Ser: TIntegerField
      FieldName = 'SCAN_Ser'
      Origin = 'paramsemp.SCAN_Ser'
    end
    object QrFilialSCAN_nNF: TIntegerField
      FieldName = 'SCAN_nNF'
      Origin = 'paramsemp.SCAN_nNF'
    end
    object QrFilialDirNFeRWeb: TWideStringField
      FieldName = 'DirNFeRWeb'
      Origin = 'paramsemp.DirNFeRWeb'
      Size = 255
    end
    object QrFilialDirEveEnvLot: TWideStringField
      FieldName = 'DirEveEnvLot'
      Origin = 'paramsemp.DirEveEnvLot'
      Size = 255
    end
    object QrFilialDirEveRetLot: TWideStringField
      FieldName = 'DirEveRetLot'
      Origin = 'paramsemp.DirEveRetLot'
      Size = 255
    end
    object QrFilialDirEvePedCCe: TWideStringField
      FieldName = 'DirEvePedCCe'
      Origin = 'paramsemp.DirEvePedCCe'
      Size = 255
    end
    object QrFilialDirEvePedCan: TWideStringField
      FieldName = 'DirEvePedCan'
      Origin = 'paramsemp.DirEvePedCan'
      Size = 255
    end
    object QrFilialDirEveRetCan: TWideStringField
      FieldName = 'DirEveRetCan'
      Origin = 'paramsemp.DirEveRetCan'
      Size = 255
    end
    object QrFilialDirEveRetCCe: TWideStringField
      FieldName = 'DirEveRetCCe'
      Origin = 'paramsemp.DirEveRetCCe'
      Size = 255
    end
    object QrFilialDirEveProcCCe: TWideStringField
      FieldName = 'DirEveProcCCe'
      Origin = 'paramsemp.DirEveProcCCe'
      Size = 255
    end
    object QrFilialPreMailEveCCe: TIntegerField
      FieldName = 'PreMailEveCCe'
      Origin = 'paramsemp.PreMailEveCCe'
    end
    object QrFilialNFSeMetodo: TIntegerField
      FieldName = 'NFSeMetodo'
    end
    object QrFilialNFSeVersao: TFloatField
      FieldName = 'NFSeVersao'
    end
    object QrFilialNFSeSerieRps: TWideStringField
      FieldName = 'NFSeSerieRps'
      Size = 5
    end
    object QrFilialNFSeWSProducao: TWideStringField
      FieldName = 'NFSeWSProducao'
      Size = 255
    end
    object QrFilialNFSeWSHomologa: TWideStringField
      FieldName = 'NFSeWSHomologa'
      Size = 255
    end
    object QrFilialNFSeAmbiente: TSmallintField
      FieldName = 'NFSeAmbiente'
    end
    object QrFilialNFSeTipCtoMail: TIntegerField
      FieldName = 'NFSeTipCtoMail'
    end
    object QrFilialNFSeCertDigital: TWideStringField
      FieldName = 'NFSeCertDigital'
      Size = 255
    end
    object QrFilialNFSeCertValidad: TDateField
      FieldName = 'NFSeCertValidad'
    end
    object QrFilialNFSeCertAviExpi: TSmallintField
      FieldName = 'NFSeCertAviExpi'
    end
    object QrFilialDpsNumero: TIntegerField
      FieldName = 'DpsNumero'
    end
    object QrFilialDirNFSeDPSGer: TWideStringField
      FieldName = 'DirNFSeDPSGer'
      Size = 255
    end
    object QrFilialRPSNumLote: TIntegerField
      FieldName = 'RPSNumLote'
    end
    object QrFilialDirNFSeLogs: TWideStringField
      FieldName = 'DirNFSeLogs'
      Size = 255
    end
    object QrFilialDirNFSeSchema: TWideStringField
      FieldName = 'DirNFSeSchema'
      Size = 255
    end
    object QrFilialNFSeLogoPref: TWideStringField
      FieldName = 'NFSeLogoPref'
      Size = 255
    end
    object QrFilialNFSeLogoFili: TWideStringField
      FieldName = 'NFSeLogoFili'
      Size = 255
    end
    object QrFilialNFSeUserWeb: TWideStringField
      FieldName = 'NFSeUserWeb'
      Size = 60
    end
    object QrFilialNFSeSenhaWeb: TWideStringField
      FieldName = 'NFSeSenhaWeb'
      Size = 60
    end
    object QrFilialNFSeCodMunici: TIntegerField
      FieldName = 'NFSeCodMunici'
    end
    object QrFilialNFSePrefeitura1: TWideStringField
      FieldName = 'NFSePrefeitura1'
      Size = 100
    end
    object QrFilialNFSePrefeitura2: TWideStringField
      FieldName = 'NFSePrefeitura2'
      Size = 100
    end
    object QrFilialNFSePrefeitura3: TWideStringField
      FieldName = 'NFSePrefeitura3'
      Size = 100
    end
    object QrFilialNFSeMsgVisu: TSmallintField
      FieldName = 'NFSeMsgVisu'
    end
    object QrFilialNFSeMetodEnvRPS: TSmallintField
      FieldName = 'NFSeMetodEnvRPS'
    end
    object QrFilialNFSeTipoRps: TSmallintField
      FieldName = 'NFSeTipoRps'
    end
    object QrFilialDirNFSeIniFiles: TWideStringField
      FieldName = 'DirNFSeIniFiles'
      Size = 255
    end
    object QrFilialNFSeWebFraseSecr: TWideStringField
      FieldName = 'NFSeWebFraseSecr'
      Size = 60
    end
    object QrFilialDPSNumHom: TIntegerField
      FieldName = 'DPSNumHom'
    end
    object QrFilialDirNFSeNFSAut: TWideStringField
      FieldName = 'DirNFSeNFSAut'
      Size = 255
    end
    object QrFilialDirNFSeNFSCan: TWideStringField
      FieldName = 'DirNFSeNFSCan'
      Size = 255
    end
  end
  object QRNFSeLRpsC: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM nfselrpsc')
    Left = 120
    Top = 4
    object QRNFSeLRpsCCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QRNFSeLRpsCSincronia: TSmallintField
      FieldName = 'Sincronia'
    end
    object QRNFSeLRpsCNFSeFatCab: TIntegerField
      FieldName = 'NFSeFatCab'
    end
    object QRNFSeLRpsCAmbiente: TSmallintField
      FieldName = 'Ambiente'
    end
    object QRNFSeLRpsCStatus: TIntegerField
      FieldName = 'Status'
    end
    object QRNFSeLRpsCEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QRNFSeLRpsCCpf: TWideStringField
      FieldName = 'Cpf'
      Size = 18
    end
    object QRNFSeLRpsCCnpj: TWideStringField
      FieldName = 'Cnpj'
      Size = 18
    end
    object QRNFSeLRpsCInscricaoMunicipal: TWideStringField
      FieldName = 'InscricaoMunicipal'
      Size = 18
    end
    object QRNFSeLRpsCQuantidadeRps: TIntegerField
      FieldName = 'QuantidadeRps'
    end
    object QRNFSeLRpsCversao: TFloatField
      FieldName = 'versao'
    end
    object QRNFSeLRpsCId: TWideStringField
      FieldName = 'Id'
      Size = 255
    end
    object QRNFSeLRpsCNumeroLote: TLargeintField
      FieldName = 'NumeroLote'
    end
    object QRNFSeLRpsCDataRecebimento: TDateTimeField
      FieldName = 'DataRecebimento'
    end
    object QRNFSeLRpsCProtocolo: TWideStringField
      FieldName = 'Protocolo'
      Size = 50
    end
  end
  object QrNFSeArqRPS: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM nfsearqrps'
      'WHERE Codigo=:P0')
    Left = 120
    Top = 52
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrNFSeArqRPSCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrNFSeArqRPSXML: TWideMemoField
      FieldName = 'XML'
      BlobType = ftWideMemo
      Size = 4
    end
  end
  object QRNFSeLRpsI: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT nri.Codigo, nri.NFSeDPSCab, dps.Empresa, dps.Ambiente '
      'FROM nfselrpsi nri'
      'LEFT JOIN nfsedpscab dps ON dps.Codigo=nri.NFSeDPSCab'
      'WHERE nri.Codigo=0')
    Left = 120
    Top = 100
    object QRNFSeLRpsICodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QRNFSeLRpsINFSeDPSCab: TIntegerField
      FieldName = 'NFSeDPSCab'
    end
    object QRNFSeLRpsIEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QRNFSeLRpsIAmbiente: TSmallintField
      FieldName = 'Ambiente'
    end
  end
  object QrNFSeArq: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM nfsearqrps'
      'WHERE Codigo=:P0')
    Left = 212
    Top = 4
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrNFSeArqCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrNFSeArqEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrNFSeArqAmbiente: TSmallintField
      FieldName = 'Ambiente'
    end
    object QrNFSeArqXML: TWideMemoField
      FieldName = 'XML'
      BlobType = ftWideMemo
      Size = 4
    end
  end
  object QrPsq: TMySQLQuery
    Database = Dmod.MyDB
    Left = 380
    Top = 148
  end
  object QrGetCodRps: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo '
      'FROM nfsedpscab '
      'WHERE Empresa=:p0'
      'AND RpsIDTipo=:P1'
      'AND RpsIDSerie=:P2'
      'AND RpsIDNumero=:P3')
    Left = 396
    Top = 264
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'p0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
    object QrGetCodRpsCodigo: TIntegerField
      FieldName = 'Codigo'
    end
  end
  object XMLDocument1: TXMLDocument
    Left = 620
    Top = 52
    DOMVendorDesc = 'MSXML'
  end
  object QrIDRps: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT NfsRpsIDSerie, NfsRpsIDTipo, NfsRpsIDNumero'
      'FROM nfsenfscab'
      'WHERE Empresa=:P0'
      'AND Ambiente=:P1'
      'AND NfsNumero=:P2')
    Left = 396
    Top = 312
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrIDRpsNfsRpsIDSerie: TWideStringField
      FieldName = 'NfsRpsIDSerie'
      Size = 5
    end
    object QrIDRpsNfsRpsIDTipo: TSmallintField
      FieldName = 'NfsRpsIDTipo'
    end
    object QrIDRpsNfsRpsIDNumero: TIntegerField
      FieldName = 'NfsRpsIDNumero'
    end
  end
  object QrIDNFSe: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT NfsNumero '
      'FROM nfsenfscab'
      'WHERE Empresa=:P0'
      'AND Ambiente=:P1'
      'AND NfsRpsIDSerie=:P2'
      'AND NfsRpsIDTipo=:P3'
      'AND NfsRpsIDNumero=:P4')
    Left = 396
    Top = 360
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P4'
        ParamType = ptUnknown
      end>
    object QrIDNFSeNfsNumero: TLargeintField
      FieldName = 'NfsNumero'
    end
  end
  object QrNextID: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT MAX(RpsIDNumero) RPSIDNumero'
      'FROM nfsedpscab'
      'WHERE Empresa=:P0'
      'AND RpsIDSerie=:P1'
      'AND RpsIDTipo=:P2')
    Left = 620
    Top = 100
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrNextIDRPSIDNumero: TIntegerField
      FieldName = 'RPSIDNumero'
    end
  end
  object QrNFS: TMySQLQuery
    Database = Dmod.MyDB
    AfterScroll = QrNFSAfterScroll
    OnCalcFields = QrNFSCalcFields
    SQL.Strings = (
      'SELECT * FROM nfsenfscab'
      'WHERE ?')
    Left = 32
    Top = 148
    object QrNFSEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrNFSAmbiente: TSmallintField
      FieldName = 'Ambiente'
    end
    object QrNFSNfsNumero: TLargeintField
      FieldName = 'NfsNumero'
    end
    object QrNFSNfsRpsIDSerie: TWideStringField
      FieldName = 'NfsRpsIDSerie'
      Size = 5
    end
    object QrNFSNfsRpsIDTipo: TSmallintField
      FieldName = 'NfsRpsIDTipo'
    end
    object QrNFSNfsRpsIDNumero: TIntegerField
      FieldName = 'NfsRpsIDNumero'
    end
    object QrNFSNfsVersao: TFloatField
      FieldName = 'NfsVersao'
    end
    object QrNFSNfsCodigoVerificacao: TWideStringField
      FieldName = 'NfsCodigoVerificacao'
      Size = 9
    end
    object QrNFSNfsDataEmissao: TDateTimeField
      FieldName = 'NfsDataEmissao'
    end
    object QrNFSNfsNfseSubstituida: TLargeintField
      FieldName = 'NfsNfseSubstituida'
    end
    object QrNFSNfsOutrasInformacoes: TWideStringField
      FieldName = 'NfsOutrasInformacoes'
      Size = 255
    end
    object QrNFSNfsBaseCalculo: TFloatField
      FieldName = 'NfsBaseCalculo'
    end
    object QrNFSNfsAliquota: TFloatField
      FieldName = 'NfsAliquota'
    end
    object QrNFSNfsValorIss: TFloatField
      FieldName = 'NfsValorIss'
    end
    object QrNFSNfsValorLiquidoNfse: TFloatField
      FieldName = 'NfsValorLiquidoNfse'
    end
    object QrNFSNfsValorCredito: TFloatField
      FieldName = 'NfsValorCredito'
    end
    object QrNFSNfsPrestaCnpj: TWideStringField
      FieldName = 'NfsPrestaCnpj'
      Size = 18
    end
    object QrNFSNfsPrestaCpf: TWideStringField
      FieldName = 'NfsPrestaCpf'
      Size = 18
    end
    object QrNFSNfsPrestaInscricaoMunicipal: TWideStringField
      FieldName = 'NfsPrestaInscricaoMunicipal'
      Size = 18
    end
    object QrNFSNfsPrestaRazaoSocial: TWideStringField
      FieldName = 'NfsPrestaRazaoSocial'
      Size = 150
    end
    object QrNFSNfsPrestaNomeFantasia: TWideStringField
      FieldName = 'NfsPrestaNomeFantasia'
      Size = 60
    end
    object QrNFSNfsPrestaEndereco: TWideStringField
      FieldName = 'NfsPrestaEndereco'
      Size = 125
    end
    object QrNFSNfsPrestaNumero: TWideStringField
      FieldName = 'NfsPrestaNumero'
      Size = 10
    end
    object QrNFSNfsPrestaComplemento: TWideStringField
      FieldName = 'NfsPrestaComplemento'
      Size = 60
    end
    object QrNFSNfsPrestaBairro: TWideStringField
      FieldName = 'NfsPrestaBairro'
      Size = 60
    end
    object QrNFSNfsPrestaCodigoMunicipio: TIntegerField
      FieldName = 'NfsPrestaCodigoMunicipio'
    end
    object QrNFSNfsPrestaUf: TWideStringField
      FieldName = 'NfsPrestaUf'
      Size = 2
    end
    object QrNFSNfsPrestaCodigoPais: TWideStringField
      FieldName = 'NfsPrestaCodigoPais'
      Size = 4
    end
    object QrNFSNfsPrestaCep: TWideStringField
      FieldName = 'NfsPrestaCep'
      Size = 10
    end
    object QrNFSNfsPrestaContatoTelefone: TWideStringField
      FieldName = 'NfsPrestaContatoTelefone'
      Size = 30
    end
    object QrNFSNfsPrestaContatoEmail: TWideStringField
      FieldName = 'NfsPrestaContatoEmail'
      Size = 80
    end
    object QrNFSNfsOrgaoGeradorCodigoMunicipio: TIntegerField
      FieldName = 'NfsOrgaoGeradorCodigoMunicipio'
    end
    object QrNFSNfsOrgaoGeradorUf: TWideStringField
      FieldName = 'NfsOrgaoGeradorUf'
      Size = 2
    end
    object QrNFSDpsRpsIDNumero: TIntegerField
      FieldName = 'DpsRpsIDNumero'
    end
    object QrNFSDpsRpsIDSerie: TWideStringField
      FieldName = 'DpsRpsIDSerie'
      Size = 5
    end
    object QrNFSDpsRpsIDTipo: TSmallintField
      FieldName = 'DpsRpsIDTipo'
    end
    object QrNFSDpsRpsDataEmissao: TDateTimeField
      FieldName = 'DpsRpsDataEmissao'
    end
    object QrNFSDpsRpsStatus: TIntegerField
      FieldName = 'DpsRpsStatus'
    end
    object QrNFSDpsSubstNumero: TIntegerField
      FieldName = 'DpsSubstNumero'
    end
    object QrNFSDpsSubstSerie: TWideStringField
      FieldName = 'DpsSubstSerie'
      Size = 5
    end
    object QrNFSDpsSubstTipo: TSmallintField
      FieldName = 'DpsSubstTipo'
    end
    object QrNFSDpsCompetencia: TDateField
      FieldName = 'DpsCompetencia'
    end
    object QrNFSDpsValorServicos: TFloatField
      FieldName = 'DpsValorServicos'
    end
    object QrNFSDpsValorDeducoes: TFloatField
      FieldName = 'DpsValorDeducoes'
    end
    object QrNFSDpsValorPis: TFloatField
      FieldName = 'DpsValorPis'
    end
    object QrNFSDpsValorCofins: TFloatField
      FieldName = 'DpsValorCofins'
    end
    object QrNFSDpsValorInss: TFloatField
      FieldName = 'DpsValorInss'
    end
    object QrNFSDpsValorIr: TFloatField
      FieldName = 'DpsValorIr'
    end
    object QrNFSDpsValorCsll: TFloatField
      FieldName = 'DpsValorCsll'
    end
    object QrNFSDpsOutrasRetencoes: TFloatField
      FieldName = 'DpsOutrasRetencoes'
    end
    object QrNFSDpsValorIss: TFloatField
      FieldName = 'DpsValorIss'
    end
    object QrNFSDpsAliquota: TFloatField
      FieldName = 'DpsAliquota'
    end
    object QrNFSDpsDescontoIncondicionado: TFloatField
      FieldName = 'DpsDescontoIncondicionado'
    end
    object QrNFSDpsDescontoCondicionado: TFloatField
      FieldName = 'DpsDescontoCondicionado'
    end
    object QrNFSDpsIssRetido: TSmallintField
      FieldName = 'DpsIssRetido'
    end
    object QrNFSDpsResponsavelRetencao: TSmallintField
      FieldName = 'DpsResponsavelRetencao'
    end
    object QrNFSDpsItemListaServico: TWideStringField
      FieldName = 'DpsItemListaServico'
      Size = 5
    end
    object QrNFSDpsCodigoCnae: TWideStringField
      FieldName = 'DpsCodigoCnae'
    end
    object QrNFSDpsCodigoTributacaoMunicipio: TWideStringField
      FieldName = 'DpsCodigoTributacaoMunicipio'
    end
    object QrNFSDpsDiscriminacao: TWideMemoField
      FieldName = 'DpsDiscriminacao'
      Required = True
      BlobType = ftWideMemo
      Size = 4
    end
    object QrNFSDpsCodigoMunicipio: TIntegerField
      FieldName = 'DpsCodigoMunicipio'
    end
    object QrNFSDpsCodigoPais: TIntegerField
      FieldName = 'DpsCodigoPais'
    end
    object QrNFSDpsExigibilidadeIss: TSmallintField
      FieldName = 'DpsExigibilidadeIss'
    end
    object QrNFSDpsNaturezaOperacao: TIntegerField
      FieldName = 'DpsNaturezaOperacao'
    end
    object QrNFSDpsMunicipioIncidencia: TIntegerField
      FieldName = 'DpsMunicipioIncidencia'
    end
    object QrNFSDpsNumeroProcesso: TWideStringField
      FieldName = 'DpsNumeroProcesso'
      Size = 30
    end
    object QrNFSDpsTomaCpf: TWideStringField
      FieldName = 'DpsTomaCpf'
      Size = 18
    end
    object QrNFSDpsTomaCnpj: TWideStringField
      FieldName = 'DpsTomaCnpj'
      Size = 18
    end
    object QrNFSDpsTomaInscricaoMunicipal: TWideStringField
      FieldName = 'DpsTomaInscricaoMunicipal'
      Size = 18
    end
    object QrNFSDpsPrestaCpf: TWideStringField
      FieldName = 'DpsPrestaCpf'
      Size = 18
    end
    object QrNFSDpsPrestaCnpj: TWideStringField
      FieldName = 'DpsPrestaCnpj'
      Size = 18
    end
    object QrNFSDpsPrestaInscricaoMunicipal: TWideStringField
      FieldName = 'DpsPrestaInscricaoMunicipal'
      Size = 18
    end
    object QrNFSDpsTomaRazaoSocial: TWideStringField
      FieldName = 'DpsTomaRazaoSocial'
      Size = 150
    end
    object QrNFSDpsTomaNomeFantasia: TWideStringField
      FieldName = 'DpsTomaNomeFantasia'
      Size = 60
    end
    object QrNFSDpsTomaEndereco: TWideStringField
      FieldName = 'DpsTomaEndereco'
      Size = 125
    end
    object QrNFSDpsTomaNumero: TWideStringField
      FieldName = 'DpsTomaNumero'
      Size = 10
    end
    object QrNFSDpsTomaComplemento: TWideStringField
      FieldName = 'DpsTomaComplemento'
      Size = 60
    end
    object QrNFSDpsTomaBairro: TWideStringField
      FieldName = 'DpsTomaBairro'
      Size = 60
    end
    object QrNFSDpsTomaCodigoMunicipio: TIntegerField
      FieldName = 'DpsTomaCodigoMunicipio'
    end
    object QrNFSDpsTomaUf: TWideStringField
      FieldName = 'DpsTomaUf'
      Size = 125
    end
    object QrNFSDpsTomaCodigoPais: TIntegerField
      FieldName = 'DpsTomaCodigoPais'
    end
    object QrNFSDpsTomaCep: TIntegerField
      FieldName = 'DpsTomaCep'
    end
    object QrNFSDpsTomaContatoTelefone: TWideStringField
      FieldName = 'DpsTomaContatoTelefone'
    end
    object QrNFSDpsTomaContatoEmail: TWideStringField
      FieldName = 'DpsTomaContatoEmail'
      Size = 80
    end
    object QrNFSDpsIntermeCpf: TWideStringField
      FieldName = 'DpsIntermeCpf'
      Size = 18
    end
    object QrNFSDpsIntermeCnpj: TWideStringField
      FieldName = 'DpsIntermeCnpj'
      Size = 18
    end
    object QrNFSDpsIntermeInscricaoMunicipal: TWideStringField
      FieldName = 'DpsIntermeInscricaoMunicipal'
      Size = 18
    end
    object QrNFSDpsIntermeRazaoSocial: TWideStringField
      FieldName = 'DpsIntermeRazaoSocial'
      Size = 150
    end
    object QrNFSDpsConstrucaoCivilCodigoObra: TWideStringField
      FieldName = 'DpsConstrucaoCivilCodigoObra'
      Size = 15
    end
    object QrNFSDpsConstrucaoCivilArt: TWideStringField
      FieldName = 'DpsConstrucaoCivilArt'
      Size = 15
    end
    object QrNFSDpsRegimeEspecialTributacao: TSmallintField
      FieldName = 'DpsRegimeEspecialTributacao'
    end
    object QrNFSDpsOptanteSimplesNacional: TSmallintField
      FieldName = 'DpsOptanteSimplesNacional'
    end
    object QrNFSDpsIncentivoFiscal: TSmallintField
      FieldName = 'DpsIncentivoFiscal'
    end
    object QrNFSDpsInfID_ID: TWideStringField
      FieldName = 'DpsInfID_ID'
      Size = 255
    end
    object QrNFSCanCodigoCancelamento: TSmallintField
      FieldName = 'CanCodigoCancelamento'
    end
    object QrNFSCanCanc_DataHora: TDateTimeField
      FieldName = 'CanCanc_DataHora'
    end
    object QrNFSCanCanc_ID: TWideStringField
      FieldName = 'CanCanc_ID'
      Size = 255
    end
    object QrNFSCanCanc_Versao: TFloatField
      FieldName = 'CanCanc_Versao'
    end
    object QrNFSSubNfseSubstituidora: TLargeintField
      FieldName = 'SubNfseSubstituidora'
    end
    object QrNFSSubSubst_ID: TWideStringField
      FieldName = 'SubSubst_ID'
      Size = 255
    end
    object QrNFSSubSubst_Versao: TFloatField
      FieldName = 'SubSubst_Versao'
    end
    object QrNFSLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrNFSDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrNFSDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrNFSUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrNFSUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrNFSAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrNFSAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrNFSCanNumero: TLargeintField
      FieldName = 'CanNumero'
    end
    object QrNFSCanCpf: TWideStringField
      FieldName = 'CanCpf'
      Size = 18
    end
    object QrNFSCanInscricaomunicipal: TWideStringField
      FieldName = 'CanInscricaomunicipal'
      Size = 18
    end
    object QrNFSCanCodigoMunicipio: TIntegerField
      FieldName = 'CanCodigoMunicipio'
    end
    object QrNFSDADOS_RPS: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DADOS_RPS'
      Size = 255
      Calculated = True
    end
    object QrNFSTXT_NFS_PRESTA_CNPJ_CPF: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TXT_NFS_PRESTA_CNPJ_CPF'
      Size = 30
      Calculated = True
    end
    object QrNFSTXT_NFS_PRESTA_ENDERECO: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TXT_NFS_PRESTA_ENDERECO'
      Size = 255
      Calculated = True
    end
    object QrNFSTXT_NFS_PRESTA_CEP: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TXT_NFS_PRESTA_CEP'
      Size = 15
      Calculated = True
    end
    object QrNFSTXT_NFS_PRESTA_CIDADE: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TXT_NFS_PRESTA_CIDADE'
      Size = 255
      Calculated = True
    end
    object QrNFSTXT_NFS_PRESTA_TELEFONE: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TXT_NFS_PRESTA_TELEFONE'
      Size = 50
      Calculated = True
    end
    object QrNFSTXT_DPS_TOMA_CNPJ_CPF: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TXT_DPS_TOMA_CNPJ_CPF'
      Size = 30
      Calculated = True
    end
    object QrNFSTXT_DPS_TOMA_ENDERECO: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TXT_DPS_TOMA_ENDERECO'
      Size = 255
      Calculated = True
    end
    object QrNFSTXT_DPS_TOMA_CEP: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TXT_DPS_TOMA_CEP'
      Size = 15
      Calculated = True
    end
    object QrNFSTXT_DPS_TOMA_CIDADE: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TXT_DPS_TOMA_CIDADE'
      Size = 255
      Calculated = True
    end
    object QrNFSTXT_DPS_TOMA_TELEFONE: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TXT_DPS_TOMA_TELEFONE'
      Size = 50
      Calculated = True
    end
    object QrNFSTXT_ItemListaServico: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TXT_ItemListaServico'
      Size = 255
      Calculated = True
    end
    object QrNFSTXT_RECOLHIMENTO: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TXT_RECOLHIMENTO'
      Size = 50
      Calculated = True
    end
    object QrNFSTXT_MUNICIPIOCREDOR: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TXT_MUNICIPIOCREDOR'
      Size = 255
      Calculated = True
    end
  end
  object frxDsNFS: TfrxDBDataset
    UserName = 'frxDsNFS'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Empresa=Empresa'
      'Ambiente=Ambiente'
      'NfsNumero=NfsNumero'
      'NfsRpsIDSerie=NfsRpsIDSerie'
      'NfsRpsIDTipo=NfsRpsIDTipo'
      'NfsRpsIDNumero=NfsRpsIDNumero'
      'NfsVersao=NfsVersao'
      'NfsCodigoVerificacao=NfsCodigoVerificacao'
      'NfsDataEmissao=NfsDataEmissao'
      'NfsNfseSubstituida=NfsNfseSubstituida'
      'NfsOutrasInformacoes=NfsOutrasInformacoes'
      'NfsBaseCalculo=NfsBaseCalculo'
      'NfsAliquota=NfsAliquota'
      'NfsValorIss=NfsValorIss'
      'NfsValorLiquidoNfse=NfsValorLiquidoNfse'
      'NfsValorCredito=NfsValorCredito'
      'NfsPrestaCnpj=NfsPrestaCnpj'
      'NfsPrestaCpf=NfsPrestaCpf'
      'NfsPrestaInscricaoMunicipal=NfsPrestaInscricaoMunicipal'
      'NfsPrestaRazaoSocial=NfsPrestaRazaoSocial'
      'NfsPrestaNomeFantasia=NfsPrestaNomeFantasia'
      'NfsPrestaEndereco=NfsPrestaEndereco'
      'NfsPrestaNumero=NfsPrestaNumero'
      'NfsPrestaComplemento=NfsPrestaComplemento'
      'NfsPrestaBairro=NfsPrestaBairro'
      'NfsPrestaCodigoMunicipio=NfsPrestaCodigoMunicipio'
      'NfsPrestaUf=NfsPrestaUf'
      'NfsPrestaCodigoPais=NfsPrestaCodigoPais'
      'NfsPrestaCep=NfsPrestaCep'
      'NfsPrestaContatoTelefone=NfsPrestaContatoTelefone'
      'NfsPrestaContatoEmail=NfsPrestaContatoEmail'
      'NfsOrgaoGeradorCodigoMunicipio=NfsOrgaoGeradorCodigoMunicipio'
      'NfsOrgaoGeradorUf=NfsOrgaoGeradorUf'
      'DpsRpsIDNumero=DpsRpsIDNumero'
      'DpsRpsIDSerie=DpsRpsIDSerie'
      'DpsRpsIDTipo=DpsRpsIDTipo'
      'DpsRpsDataEmissao=DpsRpsDataEmissao'
      'DpsRpsStatus=DpsRpsStatus'
      'DpsSubstNumero=DpsSubstNumero'
      'DpsSubstSerie=DpsSubstSerie'
      'DpsSubstTipo=DpsSubstTipo'
      'DpsCompetencia=DpsCompetencia'
      'DpsValorServicos=DpsValorServicos'
      'DpsValorDeducoes=DpsValorDeducoes'
      'DpsValorPis=DpsValorPis'
      'DpsValorCofins=DpsValorCofins'
      'DpsValorInss=DpsValorInss'
      'DpsValorIr=DpsValorIr'
      'DpsValorCsll=DpsValorCsll'
      'DpsOutrasRetencoes=DpsOutrasRetencoes'
      'DpsValorIss=DpsValorIss'
      'DpsAliquota=DpsAliquota'
      'DpsDescontoIncondicionado=DpsDescontoIncondicionado'
      'DpsDescontoCondicionado=DpsDescontoCondicionado'
      'DpsIssRetido=DpsIssRetido'
      'DpsResponsavelRetencao=DpsResponsavelRetencao'
      'DpsItemListaServico=DpsItemListaServico'
      'DpsCodigoCnae=DpsCodigoCnae'
      'DpsCodigoTributacaoMunicipio=DpsCodigoTributacaoMunicipio'
      'DpsDiscriminacao=DpsDiscriminacao'
      'DpsCodigoMunicipio=DpsCodigoMunicipio'
      'DpsCodigoPais=DpsCodigoPais'
      'DpsExigibilidadeIss=DpsExigibilidadeIss'
      'DpsNaturezaOperacao=DpsNaturezaOperacao'
      'DpsMunicipioIncidencia=DpsMunicipioIncidencia'
      'DpsNumeroProcesso=DpsNumeroProcesso'
      'DpsTomaCpf=DpsTomaCpf'
      'DpsTomaCnpj=DpsTomaCnpj'
      'DpsTomaInscricaoMunicipal=DpsTomaInscricaoMunicipal'
      'DpsPrestaCpf=DpsPrestaCpf'
      'DpsPrestaCnpj=DpsPrestaCnpj'
      'DpsPrestaInscricaoMunicipal=DpsPrestaInscricaoMunicipal'
      'DpsTomaRazaoSocial=DpsTomaRazaoSocial'
      'DpsTomaNomeFantasia=DpsTomaNomeFantasia'
      'DpsTomaEndereco=DpsTomaEndereco'
      'DpsTomaNumero=DpsTomaNumero'
      'DpsTomaComplemento=DpsTomaComplemento'
      'DpsTomaBairro=DpsTomaBairro'
      'DpsTomaCodigoMunicipio=DpsTomaCodigoMunicipio'
      'DpsTomaUf=DpsTomaUf'
      'DpsTomaCodigoPais=DpsTomaCodigoPais'
      'DpsTomaCep=DpsTomaCep'
      'DpsTomaContatoTelefone=DpsTomaContatoTelefone'
      'DpsTomaContatoEmail=DpsTomaContatoEmail'
      'DpsIntermeCpf=DpsIntermeCpf'
      'DpsIntermeCnpj=DpsIntermeCnpj'
      'DpsIntermeInscricaoMunicipal=DpsIntermeInscricaoMunicipal'
      'DpsIntermeRazaoSocial=DpsIntermeRazaoSocial'
      'DpsConstrucaoCivilCodigoObra=DpsConstrucaoCivilCodigoObra'
      'DpsConstrucaoCivilArt=DpsConstrucaoCivilArt'
      'DpsRegimeEspecialTributacao=DpsRegimeEspecialTributacao'
      'DpsOptanteSimplesNacional=DpsOptanteSimplesNacional'
      'DpsIncentivoFiscal=DpsIncentivoFiscal'
      'DpsInfID_ID=DpsInfID_ID'
      'CanCodigoCancelamento=CanCodigoCancelamento'
      'CanCanc_DataHora=CanCanc_DataHora'
      'CanCanc_ID=CanCanc_ID'
      'CanCanc_Versao=CanCanc_Versao'
      'SubNfseSubstituidora=SubNfseSubstituidora'
      'SubSubst_ID=SubSubst_ID'
      'SubSubst_Versao=SubSubst_Versao'
      'Lk=Lk'
      'DataCad=DataCad'
      'DataAlt=DataAlt'
      'UserCad=UserCad'
      'UserAlt=UserAlt'
      'AlterWeb=AlterWeb'
      'Ativo=Ativo'
      'CanNumero=CanNumero'
      'CanCpf=CanCpf'
      'CanInscricaomunicipal=CanInscricaomunicipal'
      'CanCodigoMunicipio=CanCodigoMunicipio'
      'DADOS_RPS=DADOS_RPS'
      'TXT_NFS_PRESTA_CNPJ_CPF=TXT_NFS_PRESTA_CNPJ_CPF'
      'TXT_NFS_PRESTA_ENDERECO=TXT_NFS_PRESTA_ENDERECO'
      'TXT_NFS_PRESTA_CEP=TXT_NFS_PRESTA_CEP'
      'TXT_NFS_PRESTA_CIDADE=TXT_NFS_PRESTA_CIDADE'
      'TXT_NFS_PRESTA_TELEFONE=TXT_NFS_PRESTA_TELEFONE'
      'TXT_DPS_TOMA_CNPJ_CPF=TXT_DPS_TOMA_CNPJ_CPF'
      'TXT_DPS_TOMA_ENDERECO=TXT_DPS_TOMA_ENDERECO'
      'TXT_DPS_TOMA_CEP=TXT_DPS_TOMA_CEP'
      'TXT_DPS_TOMA_CIDADE=TXT_DPS_TOMA_CIDADE'
      'TXT_DPS_TOMA_TELEFONE=TXT_DPS_TOMA_TELEFONE'
      'TXT_ItemListaServico=TXT_ItemListaServico'
      'TXT_RECOLHIMENTO=TXT_RECOLHIMENTO'
      'TXT_MUNICIPIOCREDOR=TXT_MUNICIPIOCREDOR'
      'TXT_DPSNATUREZAOPERACAO=TXT_DPSNATUREZAOPERACAO')
    DataSet = QrNFS
    BCDToCurrency = False
    DataSetOptions = []
    Left = 120
    Top = 148
  end
  object frxNFSe: TfrxReport
    Version = '2022.1'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 41202.775958854200000000
    ReportOptions.LastChange = 41568.665767048610000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'procedure MeSemValorOnBeforePrint(Sender: TfrxComponent);'
      'begin'
      
        '  MeSemValor.Visible := <VARF_AMBIENTE_TESTE> <> 1;             ' +
        '                     '
      'end;'
      ''
      'begin'
      '  if <NFSeLogoFili_Find> = True then'
      '    Picture2.LoadFromFile(<NFSeLogoFili_Path>);'
      '  if <NFSeLogoPref_Find> = True then'
      '    Picture1.LoadFromFile(<NFSeLogoPref_Path>);'
      'end.')
    OnGetValue = frxNFSeGetValue
    Left = 212
    Top = 148
    Datasets = <
      item
        DataSet = frxDsDPS
        DataSetName = 'frxDsDPS'
      end
      item
        DataSet = frxDsFilial
        DataSetName = 'frxDsFilial'
      end
      item
        DataSet = frxDsNFS
        DataSetName = 'frxDsNFS'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
      LargeDesignHeight = True
      MirrorMode = []
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 1050.708666300000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        DataSet = frxDsNFS
        DataSetName = 'frxDsNFS'
        RowCount = 0
        object MeSemValor: TfrxMemoView
          Tag = 2
          AllowVectorExport = True
          Width = 680.314960630000000000
          Height = 1046.929810000000000000
          Visible = False
          OnBeforePrint = 'MeSemValorOnBeforePrint'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = 15000804
          Font.Height = -69
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'TESTE SEM VALOR'
            'TESTE SEM VALOR'
            'TESTE SEM VALOR')
          ParentFont = False
          Rotation = 45
          VAlign = vaCenter
        end
        object Shape1: TfrxShapeView
          AllowVectorExport = True
          Width = 680.315400000000000000
          Height = 109.606370000000000000
          Frame.Typ = []
          Frame.Width = 0.500000000000000000
          Shape = skRoundRectangle
        end
        object Picture1: TfrxPictureView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Top = 3.779530000000001000
          Width = 76.724409450000000000
          Height = 102.047310000000000000
          Center = True
          Frame.Typ = []
          Frame.Width = 0.500000000000000000
          HightQuality = True
          Transparent = False
          TransparentColor = clWhite
        end
        object Memo1: TfrxMemoView
          AllowVectorExport = True
          Left = 90.708720000000000000
          Top = 7.559059999999999000
          Width = 317.480349130000000000
          Height = 37.795300000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsFilial."NFSePrefeitura1"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Picture2: TfrxPictureView
          AllowVectorExport = True
          Left = 408.189240000000000000
          Top = 3.779530000000000000
          Width = 136.063080000000000000
          Height = 102.047310000000000000
          Center = True
          Frame.Typ = []
          Frame.Width = 0.500000000000000000
          HightQuality = True
          Transparent = False
          TransparentColor = clWhite
        end
        object Memo4: TfrxMemoView
          AllowVectorExport = True
          Left = 548.031850000000000000
          Width = 132.283550000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'N'#250'mero da NFS-e:')
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          AllowVectorExport = True
          Left = 548.031850000000000000
          Top = 15.118120000000000000
          Width = 132.283550000000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[FormatFloat('#39'000000'#39', <frxDsNFS."NfsNumero">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo2: TfrxMemoView
          AllowVectorExport = True
          Left = 90.708720000000000000
          Top = 45.354360000000000000
          Width = 317.480349130000000000
          Height = 30.236240000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsFilial."NFSePrefeitura2"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo3: TfrxMemoView
          AllowVectorExport = True
          Left = 90.708720000000000000
          Top = 75.590600000000000000
          Width = 317.480349130000000000
          Height = 30.236240000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsFilial."NFSePrefeitura3"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo6: TfrxMemoView
          AllowVectorExport = True
          Left = 548.031850000000000000
          Top = 37.795300000000000000
          Width = 132.283550000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftTop]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'C'#243'digo de Verifica'#231#227'o:')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          AllowVectorExport = True
          Left = 548.031850000000000000
          Top = 52.913420000000000000
          Width = 132.283550000000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[VARF_CODIGO_VERIFICACAO]')
          ParentFont = False
          WordWrap = False
        end
        object Memo8: TfrxMemoView
          AllowVectorExport = True
          Left = 548.031849999999900000
          Top = 75.590600000000000000
          Width = 132.283550000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftTop]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Data emiss'#227'o:')
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          AllowVectorExport = True
          Left = 548.031850000000000000
          Top = 90.708720000000000000
          Width = 132.283550000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsNFS."NfsDataEmissao"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo10: TfrxMemoView
          AllowVectorExport = True
          Left = 18.897650000000000000
          Top = 109.606370000000000000
          Width = 642.520100000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsNFS."DADOS_RPS"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Shape3: TfrxShapeView
          AllowVectorExport = True
          Top = 128.504020000000000000
          Width = 680.315400000000000000
          Height = 118.299212600000000000
          Frame.Typ = []
          Frame.Width = 0.500000000000000000
          Shape = skRoundRectangle
        end
        object Me_E04_Titu: TfrxMemoView
          Tag = 1
          AllowVectorExport = True
          Top = 148.157565750000000000
          Width = 559.370169060000000000
          Height = 24.566924250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'NOME / RAZ'#195'O SOCIAL')
          ParentFont = False
        end
        object Memo17: TfrxMemoView
          Tag = 1
          AllowVectorExport = True
          Left = 18.897650000000000000
          Top = 128.504020000000000000
          Width = 642.520019450000000000
          Height = 19.653545750000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'PRESTADOR DE SERVI'#199'OS')
          ParentFont = False
          VAlign = vaCenter
        end
        object Me_E02_Titu: TfrxMemoView
          Tag = 1
          AllowVectorExport = True
          Left = 559.370149530000000000
          Top = 148.157565750000000000
          Width = 120.944828190000000000
          Height = 24.566924250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'CNPJ / CPF')
          ParentFont = False
        end
        object Me_E04_Dado: TfrxMemoView
          Tag = 1
          AllowVectorExport = True
          Left = 11.338590000000000000
          Top = 156.228422130000000000
          Width = 544.252049060000000000
          Height = 16.629916380000000000
          DataField = 'NfsPrestaRazaoSocial'
          DataSet = frxDsNFS
          DataSetName = 'frxDsNFS'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsNFS."NfsPrestaRazaoSocial"]')
          ParentFont = False
          WordBreak = True
          WordWrap = False
          VAlign = vaCenter
        end
        object Me_E02_Dado: TfrxMemoView
          Tag = 1
          AllowVectorExport = True
          Left = 559.370149530000000000
          Top = 156.228422130000000000
          Width = 117.165298190000000000
          Height = 16.629916380000000000
          DataField = 'TXT_NFS_PRESTA_CNPJ_CPF'
          DataSet = frxDsNFS
          DataSetName = 'frxDsNFS'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsNFS."TXT_NFS_PRESTA_CNPJ_CPF"]')
          ParentFont = False
          WordBreak = True
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo27: TfrxMemoView
          Tag = 1
          AllowVectorExport = True
          Top = 172.724490000000000000
          Width = 680.315172990000000000
          Height = 24.566924250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'ENDERE'#199'O')
          ParentFont = False
        end
        object Memo28: TfrxMemoView
          Tag = 1
          AllowVectorExport = True
          Left = 11.338590000000000000
          Top = 180.795346380000000000
          Width = 665.197052990000000000
          Height = 16.629916380000000000
          DataField = 'TXT_NFS_PRESTA_ENDERECO'
          DataSet = frxDsNFS
          DataSetName = 'frxDsNFS'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsNFS."TXT_NFS_PRESTA_ENDERECO"]')
          ParentFont = False
          WordBreak = True
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo29: TfrxMemoView
          Tag = 1
          AllowVectorExport = True
          Top = 197.291338580000000000
          Width = 185.196850390000000000
          Height = 24.566924250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'BAIRRO / DISTRITO')
          ParentFont = False
        end
        object Memo30: TfrxMemoView
          Tag = 1
          AllowVectorExport = True
          Left = 11.338343460000000000
          Top = 205.228346460000000000
          Width = 173.858260390000000000
          Height = 16.629916380000000000
          DataField = 'NfsPrestaBairro'
          DataSet = frxDsNFS
          DataSetName = 'frxDsNFS'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsNFS."NfsPrestaBairro"]')
          ParentFont = False
          WordBreak = True
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo31: TfrxMemoView
          Tag = 1
          AllowVectorExport = True
          Left = 185.196603860000000000
          Top = 197.291338582677000000
          Width = 86.929133860000000000
          Height = 24.566924250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'CEP')
          ParentFont = False
        end
        object Memo32: TfrxMemoView
          Tag = 1
          AllowVectorExport = True
          Left = 185.196603860000000000
          Top = 205.228346460000000000
          Width = 86.929133860000000000
          Height = 16.629916380000000000
          DataSet = frxDsNFS
          DataSetName = 'frxDsNFS'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsNFS."TXT_NFS_PRESTA_CEP"]')
          ParentFont = False
          WordBreak = True
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo35: TfrxMemoView
          Tag = 1
          AllowVectorExport = True
          Left = 272.126160000000000000
          Top = 197.291414250000000000
          Width = 408.567019450000000000
          Height = 24.566929130000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'MUNIC'#205'PIO')
          ParentFont = False
        end
        object Memo36: TfrxMemoView
          Tag = 1
          AllowVectorExport = True
          Left = 272.126160000000000000
          Top = 205.362270630000000000
          Width = 404.787489450000000000
          Height = 16.629921260000000000
          DataField = 'TXT_NFS_PRESTA_CIDADE'
          DataSet = frxDsNFS
          DataSetName = 'frxDsNFS'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsNFS."TXT_NFS_PRESTA_CIDADE"]')
          ParentFont = False
          WordBreak = True
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo37: TfrxMemoView
          Tag = 1
          AllowVectorExport = True
          Left = 0.377779450000000000
          Top = 221.968594250000000000
          Width = 134.551168900000000000
          Height = 24.566929130000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftRight, ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'FONE / FAX')
          ParentFont = False
        end
        object Memo38: TfrxMemoView
          Tag = 1
          AllowVectorExport = True
          Left = 11.716369450000000000
          Top = 229.795275590000000000
          Width = 123.212578900000000000
          Height = 16.629921260000000000
          DataSet = frxDsNFS
          DataSetName = 'frxDsNFS'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsNFS."TXT_NFS_PRESTA_TELEFONE"]')
          ParentFont = False
          WordBreak = True
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo39: TfrxMemoView
          Tag = 1
          AllowVectorExport = True
          Left = 134.928948340000000000
          Top = 221.968594250000000000
          Width = 27.590551180000000000
          Height = 24.566929130000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'UF')
          ParentFont = False
        end
        object Memo40: TfrxMemoView
          Tag = 1
          AllowVectorExport = True
          Left = 134.928948340000000000
          Top = 229.795275590000000000
          Width = 27.590551180000000000
          Height = 16.629921260000000000
          DataSet = frxDsNFS
          DataSetName = 'frxDsNFS'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsNFS."NfsPrestaUf"]')
          ParentFont = False
          WordBreak = True
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo11: TfrxMemoView
          Tag = 1
          AllowVectorExport = True
          Left = 309.921460000000000000
          Top = 221.858267720000000000
          Width = 370.393632440000000000
          Height = 24.566929130000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'E-MAIL:')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          Tag = 1
          AllowVectorExport = True
          Left = 309.921460000000000000
          Top = 229.795275590000000000
          Width = 362.834572440000000000
          Height = 16.629921260000000000
          DataField = 'NfsPrestaContatoEmail'
          DataSet = frxDsNFS
          DataSetName = 'frxDsNFS'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsNFS."NfsPrestaContatoEmail"]')
          ParentFont = False
          WordBreak = True
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo13: TfrxMemoView
          Tag = 1
          AllowVectorExport = True
          Left = 162.519790000000000000
          Top = 221.968518580000000000
          Width = 147.401538190000000000
          Height = 24.566929130000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'INSCRI'#199#195'O MUNICIPAL')
          ParentFont = False
        end
        object Memo14: TfrxMemoView
          Tag = 1
          AllowVectorExport = True
          Left = 162.519790000000000000
          Top = 229.795275590000000000
          Width = 143.622008190000000000
          Height = 16.629921260000000000
          DataSet = frxDsNFS
          DataSetName = 'frxDsNFS'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[CHOICE_PRESTA_IM]')
          ParentFont = False
          WordBreak = True
          WordWrap = False
          VAlign = vaCenter
        end
        object Shape4: TfrxShapeView
          AllowVectorExport = True
          Left = 0.000246540000000000
          Top = 254.228510000000000000
          Width = 680.315400000000000000
          Height = 118.299212600000000000
          Frame.Typ = []
          Frame.Width = 0.500000000000000000
          Shape = skRoundRectangle
        end
        object Memo15: TfrxMemoView
          Tag = 1
          AllowVectorExport = True
          Left = 0.000246540000000000
          Top = 273.882055750000000000
          Width = 559.370169060000000000
          Height = 24.566924250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'NOME / RAZ'#195'O SOCIAL')
          ParentFont = False
        end
        object Memo16: TfrxMemoView
          Tag = 1
          AllowVectorExport = True
          Left = 18.897896540000000000
          Top = 254.228510000000000000
          Width = 642.520019450000000000
          Height = 19.653545750000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'TOMADOR DE SERVI'#199'OS')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo18: TfrxMemoView
          Tag = 1
          AllowVectorExport = True
          Left = 559.370396070000000000
          Top = 273.882055750000000000
          Width = 120.944828190000000000
          Height = 24.566924250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'CNPJ / CPF')
          ParentFont = False
        end
        object Memo19: TfrxMemoView
          Tag = 1
          AllowVectorExport = True
          Left = 11.338836540000000000
          Top = 281.952912130000000000
          Width = 544.252049060000000000
          Height = 16.629916380000000000
          DataSet = frxDsNFS
          DataSetName = 'frxDsNFS'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[CHOICE_TOMA_RAZAOSOCIAL]')
          ParentFont = False
          WordBreak = True
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo20: TfrxMemoView
          Tag = 1
          AllowVectorExport = True
          Left = 559.370396070000000000
          Top = 281.952912130000000000
          Width = 120.944828190000000000
          Height = 16.629916380000000000
          DataSet = frxDsNFS
          DataSetName = 'frxDsNFS'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[CHOICE_TOMA_CNPJ_CPF]')
          ParentFont = False
          WordBreak = True
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo21: TfrxMemoView
          Tag = 1
          AllowVectorExport = True
          Left = 0.000246540000000000
          Top = 298.448980000000000000
          Width = 680.315172990000000000
          Height = 24.566924250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'ENDERE'#199'O')
          ParentFont = False
        end
        object Memo22: TfrxMemoView
          Tag = 1
          AllowVectorExport = True
          Left = 11.338836540000000000
          Top = 306.519836380000000000
          Width = 665.197052990000000000
          Height = 16.629916380000000000
          DataSet = frxDsNFS
          DataSetName = 'frxDsNFS'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[CHOICE_TOMA_ENDERECO]')
          ParentFont = False
          WordBreak = True
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo23: TfrxMemoView
          Tag = 1
          AllowVectorExport = True
          Top = 323.015828580000000000
          Width = 185.196850390000000000
          Height = 24.566924250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'BAIRRO / DISTRITO')
          ParentFont = False
        end
        object Memo24: TfrxMemoView
          Tag = 1
          AllowVectorExport = True
          Left = 11.338590000000000000
          Top = 330.952836460000000000
          Width = 173.858260390000000000
          Height = 16.629916380000000000
          DataSet = frxDsNFS
          DataSetName = 'frxDsNFS'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[CHOICE_TOMA_BAIRRO]')
          ParentFont = False
          WordBreak = True
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo25: TfrxMemoView
          Tag = 1
          AllowVectorExport = True
          Left = 185.196850400000000000
          Top = 323.015828580000000000
          Width = 86.929133860000000000
          Height = 24.566924250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'CEP')
          ParentFont = False
        end
        object Memo26: TfrxMemoView
          Tag = 1
          AllowVectorExport = True
          Left = 185.196850400000000000
          Top = 330.952836460000000000
          Width = 86.929133860000000000
          Height = 16.629916380000000000
          DataSet = frxDsNFS
          DataSetName = 'frxDsNFS'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[CHOICE_TOMA_CEP]')
          ParentFont = False
          WordBreak = True
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo33: TfrxMemoView
          Tag = 1
          AllowVectorExport = True
          Left = 272.126406540000000000
          Top = 323.015904250000000000
          Width = 408.567019450000000000
          Height = 24.566929130000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'MUNIC'#205'PIO')
          ParentFont = False
        end
        object Memo34: TfrxMemoView
          Tag = 1
          AllowVectorExport = True
          Left = 272.126406540000000000
          Top = 331.086760630000000000
          Width = 404.787489450000000000
          Height = 16.629921260000000000
          DataSet = frxDsNFS
          DataSetName = 'frxDsNFS'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[CHOICE_TOMA_CIDADE]')
          ParentFont = False
          WordBreak = True
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo41: TfrxMemoView
          Tag = 1
          AllowVectorExport = True
          Left = 0.378025990000000000
          Top = 347.693084250000000000
          Width = 134.551168900000000000
          Height = 24.566929130000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftRight, ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'FONE / FAX')
          ParentFont = False
        end
        object Memo42: TfrxMemoView
          Tag = 1
          AllowVectorExport = True
          Left = 11.716615990000000000
          Top = 355.519765590000000000
          Width = 123.212578900000000000
          Height = 16.629921260000000000
          DataSet = frxDsNFS
          DataSetName = 'frxDsNFS'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[CHOICE_TOMA_CONTATOTELEFONE]')
          ParentFont = False
          WordBreak = True
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo43: TfrxMemoView
          Tag = 1
          AllowVectorExport = True
          Left = 134.929194880000000000
          Top = 347.693084250000000000
          Width = 27.590551180000000000
          Height = 24.566929130000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'UF')
          ParentFont = False
        end
        object Memo44: TfrxMemoView
          Tag = 1
          AllowVectorExport = True
          Left = 134.929194880000000000
          Top = 355.519765590000000000
          Width = 27.590551180000000000
          Height = 16.629921260000000000
          DataSet = frxDsNFS
          DataSetName = 'frxDsNFS'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[CHOICE_TOMA_UF]')
          ParentFont = False
          WordBreak = True
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo45: TfrxMemoView
          Tag = 1
          AllowVectorExport = True
          Left = 309.921706540000000000
          Top = 347.582757720000000000
          Width = 370.393632440000000000
          Height = 24.566929130000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'E-MAIL:')
          ParentFont = False
        end
        object Memo46: TfrxMemoView
          Tag = 1
          AllowVectorExport = True
          Left = 309.921706540000000000
          Top = 355.519765590000000000
          Width = 362.834572440000000000
          Height = 16.629921260000000000
          DataSet = frxDsNFS
          DataSetName = 'frxDsNFS'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[CHOICE_TOMA_CONTATOEMAIL]')
          ParentFont = False
          WordBreak = True
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo47: TfrxMemoView
          Tag = 1
          AllowVectorExport = True
          Left = 162.520036540000000000
          Top = 347.693008580000000000
          Width = 147.401538190000000000
          Height = 24.566929130000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'INSCRI'#199#195'O MUNICIPAL')
          ParentFont = False
        end
        object Memo48: TfrxMemoView
          Tag = 1
          AllowVectorExport = True
          Left = 162.520036540000000000
          Top = 355.519765590000000000
          Width = 143.622008190000000000
          Height = 16.629921260000000000
          DataSet = frxDsNFS
          DataSetName = 'frxDsNFS'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[CHOICE_TOMA_IM]')
          ParentFont = False
          WordBreak = True
          WordWrap = False
          VAlign = vaCenter
        end
        object Shape2: TfrxShapeView
          AllowVectorExport = True
          Top = 377.953000000000000000
          Width = 680.315400000000000000
          Height = 396.850650000000000000
          Frame.Typ = []
          Frame.Width = 0.500000000000000000
        end
        object Memo49: TfrxMemoView
          AllowVectorExport = True
          Top = 393.071120000000000000
          Width = 680.315400000000000000
          Height = 381.732530000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[CHOICE_DISCRIMINACAO]')
          ParentFont = False
        end
        object Memo50: TfrxMemoView
          AllowVectorExport = True
          Top = 377.953000000000000000
          Width = 680.315400000000000000
          Height = 15.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Discrimina'#231#227'o dos Servi'#231'os:')
          ParentFont = False
        end
        object Memo51: TfrxMemoView
          AllowVectorExport = True
          Top = 774.803650000000000000
          Width = 377.953000000000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            ' Valor Total da NFS-e')
          ParentFont = False
        end
        object Memo52: TfrxMemoView
          AllowVectorExport = True
          Left = 377.953000000000000000
          Top = 774.803650000000000000
          Width = 302.362400000000000000
          Height = 22.677180000000000000
          DataSet = frxDsNFS
          DataSetName = 'frxDsNFS'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'R$ [CHOICE_VALORSERVICOS] ')
          ParentFont = False
        end
        object Memo53: TfrxMemoView
          Tag = 1
          AllowVectorExport = True
          Top = 797.480830000000000000
          Width = 680.315280390000000000
          Height = 34.015748030000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'ITEM DA LISTA DE SERVI'#199'OS:')
          ParentFont = False
        end
        object Memo54: TfrxMemoView
          Tag = 1
          AllowVectorExport = True
          Left = 3.779530000000000000
          Top = 805.417837880000000000
          Width = 672.756220390000000000
          Height = 26.456692910000000000
          DataSet = frxDsNFS
          DataSetName = 'frxDsNFS'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[CHOICE_ITEMLISTASERVICO]')
          ParentFont = False
          WordBreak = True
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo55: TfrxMemoView
          Tag = 1
          AllowVectorExport = True
          Top = 831.496600000000000000
          Width = 113.385826770000000000
          Height = 24.566924250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'VALOR TOTAL DEDU'#199#213'ES:')
          ParentFont = False
        end
        object Memo56: TfrxMemoView
          Tag = 1
          AllowVectorExport = True
          Left = 3.779530000000000000
          Top = 839.433607880000000000
          Width = 105.826720390000000000
          Height = 16.629921260000000000
          DataSet = frxDsNFS
          DataSetName = 'frxDsNFS'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[CHOICE_VALORDEDUCOES]')
          ParentFont = False
          WordBreak = True
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo57: TfrxMemoView
          Tag = 1
          AllowVectorExport = True
          Left = 113.385900000000000000
          Top = 831.496600000000000000
          Width = 113.385826770000000000
          Height = 24.566924250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'BASE DE C'#193'LCULO:')
          ParentFont = False
        end
        object Memo58: TfrxMemoView
          Tag = 1
          AllowVectorExport = True
          Left = 117.165430000000000000
          Top = 839.433607880000000000
          Width = 105.826720390000000000
          Height = 16.629921260000000000
          DataSet = frxDsNFS
          DataSetName = 'frxDsNFS'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_BASECALCULO]')
          ParentFont = False
          WordBreak = True
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo59: TfrxMemoView
          Tag = 1
          AllowVectorExport = True
          Left = 226.771800000000000000
          Top = 831.496600000000000000
          Width = 113.385826770000000000
          Height = 24.566924250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'AL'#205'QUOTA (%):')
          ParentFont = False
        end
        object Memo60: TfrxMemoView
          Tag = 1
          AllowVectorExport = True
          Left = 230.551330000000000000
          Top = 839.433607880000000000
          Width = 105.826720390000000000
          Height = 16.629921260000000000
          DataSet = frxDsNFS
          DataSetName = 'frxDsNFS'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_ALIQUOTA]')
          ParentFont = False
          WordBreak = True
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo61: TfrxMemoView
          Tag = 1
          AllowVectorExport = True
          Left = 340.157700000000000000
          Top = 831.496600000000000000
          Width = 113.385826770000000000
          Height = 24.566924250000000000
          DataSet = frxDsNFS
          DataSetName = 'frxDsNFS'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'VALOR DO ISSQN:')
          ParentFont = False
        end
        object Memo62: TfrxMemoView
          Tag = 1
          AllowVectorExport = True
          Left = 343.937230000000000000
          Top = 839.433607880000000000
          Width = 105.826720390000000000
          Height = 16.629921260000000000
          DataSet = frxDsNFS
          DataSetName = 'frxDsNFS'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_VALORISS]')
          ParentFont = False
          WordBreak = True
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo63: TfrxMemoView
          Tag = 1
          AllowVectorExport = True
          Left = 453.543600000000000000
          Top = 831.496600000000000000
          Width = 113.385826770000000000
          Height = 24.566924250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'RESP. RECOLHIMENTO')
          ParentFont = False
        end
        object Memo64: TfrxMemoView
          Tag = 1
          AllowVectorExport = True
          Left = 457.323130000000000000
          Top = 839.433607880000000000
          Width = 105.826720390000000000
          Height = 16.629921260000000000
          DataSet = frxDsNFS
          DataSetName = 'frxDsNFS'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[CHOICE_RECOLHIMENTO]')
          ParentFont = False
          WordBreak = True
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo67: TfrxMemoView
          Tag = 1
          AllowVectorExport = True
          Top = 855.953310000000000000
          Width = 113.385826770000000000
          Height = 24.566924250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'VALOR DO PIS:')
          ParentFont = False
        end
        object Memo68: TfrxMemoView
          Tag = 1
          AllowVectorExport = True
          Left = 3.779530000000000000
          Top = 863.890317880000000000
          Width = 105.826720390000000000
          Height = 16.629921260000000000
          DataSet = frxDsNFS
          DataSetName = 'frxDsNFS'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[CHOICE_VALORPIS]')
          ParentFont = False
          WordBreak = True
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo69: TfrxMemoView
          Tag = 1
          AllowVectorExport = True
          Left = 113.385900000000000000
          Top = 855.953310000000000000
          Width = 113.385826770000000000
          Height = 24.566924250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'VALOR DO COFINS:')
          ParentFont = False
        end
        object Memo70: TfrxMemoView
          Tag = 1
          AllowVectorExport = True
          Left = 117.165430000000000000
          Top = 863.890317880000000000
          Width = 105.826720390000000000
          Height = 16.629921260000000000
          DataSet = frxDsNFS
          DataSetName = 'frxDsNFS'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[CHOICE_VALORCOFINS]')
          ParentFont = False
          WordBreak = True
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo71: TfrxMemoView
          Tag = 1
          AllowVectorExport = True
          Left = 226.771800000000000000
          Top = 855.953310000000000000
          Width = 113.385826770000000000
          Height = 24.566924250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'VALOR DO INSS:')
          ParentFont = False
        end
        object Memo72: TfrxMemoView
          Tag = 1
          AllowVectorExport = True
          Left = 230.551330000000000000
          Top = 863.890317880000000000
          Width = 105.826720390000000000
          Height = 16.629921260000000000
          DataSet = frxDsNFS
          DataSetName = 'frxDsNFS'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[CHOICE_VALORINSS]')
          ParentFont = False
          WordBreak = True
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo73: TfrxMemoView
          Tag = 1
          AllowVectorExport = True
          Left = 340.157700000000000000
          Top = 855.953310000000000000
          Width = 113.385826770000000000
          Height = 24.566924250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'VALOR DO IRRF:')
          ParentFont = False
        end
        object Memo74: TfrxMemoView
          Tag = 1
          AllowVectorExport = True
          Left = 343.937230000000000000
          Top = 863.890317880000000000
          Width = 105.826720390000000000
          Height = 16.629921260000000000
          DataSet = frxDsNFS
          DataSetName = 'frxDsNFS'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[CHOICE_VALORIR]')
          ParentFont = False
          WordBreak = True
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo75: TfrxMemoView
          Tag = 1
          AllowVectorExport = True
          Left = 453.543600000000000000
          Top = 855.953310000000000000
          Width = 113.385826770000000000
          Height = 24.566924250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'VALOR DO CSLL:')
          ParentFont = False
        end
        object Memo76: TfrxMemoView
          Tag = 1
          AllowVectorExport = True
          Left = 457.323130000000000000
          Top = 863.890317880000000000
          Width = 105.826720390000000000
          Height = 16.629921260000000000
          DataSet = frxDsNFS
          DataSetName = 'frxDsNFS'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[CHOICE_VALORCSLL]')
          ParentFont = False
          WordBreak = True
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo77: TfrxMemoView
          Tag = 1
          AllowVectorExport = True
          Left = 566.929500000000000000
          Top = 855.953310000000000000
          Width = 113.385826770000000000
          Height = 24.566924250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'VALOR OUTRAS RETEN'#199#213'ES:')
          ParentFont = False
        end
        object Memo78: TfrxMemoView
          Tag = 1
          AllowVectorExport = True
          Left = 570.709030000000000000
          Top = 863.890317880000000000
          Width = 105.826720390000000000
          Height = 16.629921260000000000
          DataSet = frxDsNFS
          DataSetName = 'frxDsNFS'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[CHOICE_OUTRASRETENCOES]')
          ParentFont = False
          WordBreak = True
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo83: TfrxMemoView
          Tag = 1
          AllowVectorExport = True
          Left = 189.086643460000000000
          Top = 880.630490000000000000
          Width = 75.590526770000000000
          Height = 24.566924250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'SITUA'#199#195'O:')
          ParentFont = False
        end
        object Memo84: TfrxMemoView
          Tag = 1
          AllowVectorExport = True
          Left = 190.755912830000000000
          Top = 888.567497880000000000
          Width = 71.810950390000000000
          Height = 16.629921260000000000
          DataSet = frxDsNFS
          DataSetName = 'frxDsNFS'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[VARF_SITUACAO]')
          ParentFont = False
          WordBreak = True
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo85: TfrxMemoView
          Tag = 1
          AllowVectorExport = True
          Left = 566.929499999999900000
          Top = 831.496600000000000000
          Width = 64.251968500000000000
          Height = 24.566924250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'C'#211'DIGO DA OBRA:')
          ParentFont = False
        end
        object Memo86: TfrxMemoView
          Tag = 1
          AllowVectorExport = True
          Left = 570.709030000000000000
          Top = 839.433607880000000000
          Width = 56.692913385826800000
          Height = 16.629921260000000000
          DataSet = frxDsNFS
          DataSetName = 'frxDsNFS'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[CHOICE_CONSTRUCAOCIVILCODIGOOBRA]')
          ParentFont = False
          WordBreak = True
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo87: TfrxMemoView
          Tag = 1
          AllowVectorExport = True
          Left = 529.133858270000000000
          Top = 880.630490000000000000
          Width = 151.181102360000000000
          Height = 24.566924250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'MUNI'#141'C'#205'PIO CREDOR:')
          ParentFont = False
        end
        object Memo88: TfrxMemoView
          Tag = 1
          AllowVectorExport = True
          Left = 532.354670000000000000
          Top = 888.567497880000000000
          Width = 143.622042360000000000
          Height = 16.629921260000000000
          DataSet = frxDsNFS
          DataSetName = 'frxDsNFS'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[CHOICE_MUNICIPIOCREDOR]')
          ParentFont = False
          WordBreak = True
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo89: TfrxMemoView
          Tag = 1
          AllowVectorExport = True
          Top = 905.087200000000000000
          Width = 680.315280390000000000
          Height = 128.503998030000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'OUTRAS INFORMA'#199#213'ES:')
          ParentFont = False
        end
        object Memo90: TfrxMemoView
          Tag = 1
          AllowVectorExport = True
          Left = 3.779530000000000000
          Top = 916.803737880000000000
          Width = 672.756220390000000000
          Height = 113.385882910000000000
          DataField = 'NfsOutrasInformacoes'
          DataSet = frxDsNFS
          DataSetName = 'frxDsNFS'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsNFS."NfsOutrasInformacoes"]')
          ParentFont = False
          WordBreak = True
          WordWrap = False
        end
        object Memo91: TfrxMemoView
          Tag = 1
          AllowVectorExport = True
          Left = 631.181102362205100000
          Top = 831.496600000000000000
          Width = 49.133858270000000000
          Height = 24.566924250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'ART:')
          ParentFont = False
        end
        object Memo92: TfrxMemoView
          Tag = 1
          AllowVectorExport = True
          Left = 634.960629921260000000
          Top = 839.433607880000000000
          Width = 41.574710390000000000
          Height = 16.629921260000000000
          DataSet = frxDsNFS
          DataSetName = 'frxDsNFS'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[CHOICE_CONSTRUCAOCIVILART]')
          ParentFont = False
          WordBreak = True
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo93: TfrxMemoView
          Tag = 1
          AllowVectorExport = True
          Left = 120.944881890000000000
          Top = 880.630490000000000000
          Width = 68.031496060000000000
          Height = 24.566924250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'OPTANTE SIMPLES:')
          ParentFont = False
        end
        object Memo94: TfrxMemoView
          Tag = 1
          AllowVectorExport = True
          Left = 123.504020000000000000
          Top = 888.567497880000000000
          Width = 62.362202280000000000
          Height = 16.629921260000000000
          DataSet = frxDsNFS
          DataSetName = 'frxDsNFS'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[CHOICE_DPSOPTANTESIMPLESNACIONAL]')
          ParentFont = False
          WordBreak = True
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo65: TfrxMemoView
          Tag = 1
          AllowVectorExport = True
          Top = 880.630490000000000000
          Width = 120.944881890000000000
          Height = 24.566924250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'COMPET'#202'NCIA:')
          ParentFont = False
        end
        object Memo66: TfrxMemoView
          Tag = 1
          AllowVectorExport = True
          Left = 3.779530000000000000
          Top = 888.567497880000000000
          Width = 113.385780390000000000
          Height = 16.629921260000000000
          DataSet = frxDsNFS
          DataSetName = 'frxDsNFS'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[VARF_COMPETENCIA]')
          ParentFont = False
          WordBreak = True
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo81: TfrxMemoView
          Tag = 1
          AllowVectorExport = True
          Left = 396.960671420000000000
          Top = 880.629921260000000000
          Width = 132.283464570000000000
          Height = 24.566924250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'DESCONTO INCONDICIONADO:')
          ParentFont = False
        end
        object Memo82: TfrxMemoView
          Tag = 1
          AllowVectorExport = True
          Left = 400.630180000000000000
          Top = 888.347027880000000000
          Width = 124.724370390000000000
          Height = 16.629921260000000000
          DataSet = frxDsNFS
          DataSetName = 'frxDsNFS'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[CHOICE_DESCONTOINCONDICIONADO]')
          ParentFont = False
          WordBreak = True
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo79: TfrxMemoView
          Tag = 1
          AllowVectorExport = True
          Left = 264.677194650000000000
          Top = 880.630490000000000000
          Width = 132.283464570000000000
          Height = 24.566924250000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Times New Roman'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'DESCONTO CONDICIONADO:')
          ParentFont = False
        end
        object Memo80: TfrxMemoView
          Tag = 1
          AllowVectorExport = True
          Left = 267.567100000000000000
          Top = 888.567497880000000000
          Width = 124.724370390000000000
          Height = 16.629921260000000000
          DataSet = frxDsNFS
          DataSetName = 'frxDsNFS'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[CHOICE_DESCONTOCONDICIONADO]')
          ParentFont = False
          WordBreak = True
          WordWrap = False
          VAlign = vaCenter
        end
      end
    end
  end
  object frxDsFilial: TfrxDBDataset
    UserName = 'frxDsFilial'
    CloseDataSource = False
    FieldAliases.Strings = (
      'SimplesFed=SimplesFed'
      'CerDigital=CerDigital'
      'UF_WebServ=UF_WebServ'
      'DirNFeGer=DirNFeGer'
      'DirNFeAss=DirNFeAss'
      'DirEnvLot=DirEnvLot'
      'DirRec=DirRec'
      'DirPedRec=DirPedRec'
      'DirProRec=DirProRec'
      'DirDen=DirDen'
      'DirPedCan=DirPedCan'
      'DirCan=DirCan'
      'DirPedInu=DirPedInu'
      'DirInu=DirInu'
      'DirPedSit=DirPedSit'
      'DirSit=DirSit'
      'DirPedSta=DirPedSta'
      'DirSta=DirSta'
      'SiglaCustm=SiglaCustm'
      'InfoPerCuz=InfoPerCuz'
      'UF_Servico=UF_Servico'
      'NFeSerNum=NFeSerNum'
      'SINTEGRA_Path=SINTEGRA_Path'
      'DirDANFEs=DirDANFEs'
      'DirNFeProt=DirNFeProt'
      'NFeSerVal=NFeSerVal'
      'NFeSerAvi=NFeSerAvi'
      'NFetpEmis=NFetpEmis'
      'SCAN_Ser=SCAN_Ser'
      'SCAN_nNF=SCAN_nNF'
      'DirNFeRWeb=DirNFeRWeb'
      'DirEveEnvLot=DirEveEnvLot'
      'DirEveRetLot=DirEveRetLot'
      'DirEvePedCCe=DirEvePedCCe'
      'DirEvePedCan=DirEvePedCan'
      'DirEveRetCan=DirEveRetCan'
      'DirEveRetCCe=DirEveRetCCe'
      'DirEveProcCCe=DirEveProcCCe'
      'PreMailEveCCe=PreMailEveCCe'
      'NFSeMetodo=NFSeMetodo'
      'NFSeVersao=NFSeVersao'
      'NFSeSerieRps=NFSeSerieRps'
      'NFSeWSProducao=NFSeWSProducao'
      'NFSeWSHomologa=NFSeWSHomologa'
      'NFSeAmbiente=NFSeAmbiente'
      'NFSeTipCtoMail=NFSeTipCtoMail'
      'NFSeCertDigital=NFSeCertDigital'
      'NFSeCertValidad=NFSeCertValidad'
      'NFSeCertAviExpi=NFSeCertAviExpi'
      'DpsNumero=DpsNumero'
      'DirNFSeDPSGer=DirNFSeDPSGer'
      'DirNFSeDPSAss=DirNFSeDPSAss'
      'RPSNumLote=RPSNumLote'
      'DirNFSeRPSEnvLot=DirNFSeRPSEnvLot'
      'DirNFSeRPSRecLot=DirNFSeRPSRecLot'
      'DirNFSeLogs=DirNFSeLogs'
      'DirNFSeSchema=DirNFSeSchema'
      'NFSeLogoPref=NFSeLogoPref'
      'NFSeLogoFili=NFSeLogoFili'
      'NFSeUserWeb=NFSeUserWeb'
      'NFSeSenhaWeb=NFSeSenhaWeb'
      'NFSeCodMunici=NFSeCodMunici'
      'NFSePrefeitura1=NFSePrefeitura1'
      'NFSePrefeitura2=NFSePrefeitura2'
      'NFSePrefeitura3=NFSePrefeitura3'
      'NFSeMsgVisu=NFSeMsgVisu'
      'NFSeMetodEnvRPS=NFSeMetodEnvRPS'
      'NFSeTipoRps=NFSeTipoRps')
    DataSet = QrFilial
    BCDToCurrency = False
    DataSetOptions = []
    Left = 212
    Top = 100
  end
  object QrDPS: TMySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrDPSCalcFields
    SQL.Strings = (
      'SELECT nsc.PDFCompres, nsc.PDFEmbFont, '
      'nsc.PDFPrnOptm, ndc.* '
      'FROM nfsedpscab ndc'
      'LEFT JOIN nfsesrvcad nsc ON nsc.Codigo=ndc.NFSeSrvCad'
      'WHERE ndc.Empresa=-11'
      'AND ndc.Ambiente=1'
      'AND ndc.RpsIDTipo=1'
      'AND ndc.RpsIDSerie="1" '
      'AND ndc.RpsIDNumero=44')
    Left = 32
    Top = 196
    object QrDPSCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrDPSAmbiente: TSmallintField
      FieldName = 'Ambiente'
    end
    object QrDPSEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrDPSCliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrDPSIntermediario: TIntegerField
      FieldName = 'Intermediario'
    end
    object QrDPSNFSeSrvCad: TIntegerField
      FieldName = 'NFSeSrvCad'
    end
    object QrDPSRpsID: TWideStringField
      FieldName = 'RpsID'
      Size = 255
    end
    object QrDPSRpsIDNumero: TIntegerField
      FieldName = 'RpsIDNumero'
    end
    object QrDPSRpsIDSerie: TWideStringField
      FieldName = 'RpsIDSerie'
      Size = 5
    end
    object QrDPSRpsIDTipo: TSmallintField
      FieldName = 'RpsIDTipo'
    end
    object QrDPSRpsDataEmissao: TDateField
      FieldName = 'RpsDataEmissao'
    end
    object QrDPSRpsHoraEmissao: TTimeField
      FieldName = 'RpsHoraEmissao'
    end
    object QrDPSRpsStatus: TIntegerField
      FieldName = 'RpsStatus'
    end
    object QrDPSCompetencia: TDateField
      FieldName = 'Competencia'
    end
    object QrDPSSubstNumero: TIntegerField
      FieldName = 'SubstNumero'
    end
    object QrDPSSubstSerie: TWideStringField
      FieldName = 'SubstSerie'
      Size = 5
    end
    object QrDPSSubstTipo: TSmallintField
      FieldName = 'SubstTipo'
    end
    object QrDPSValorServicos: TFloatField
      FieldName = 'ValorServicos'
    end
    object QrDPSValorDeducoes: TFloatField
      FieldName = 'ValorDeducoes'
    end
    object QrDPSValorPis: TFloatField
      FieldName = 'ValorPis'
    end
    object QrDPSValorCofins: TFloatField
      FieldName = 'ValorCofins'
    end
    object QrDPSValorInss: TFloatField
      FieldName = 'ValorInss'
    end
    object QrDPSValorIr: TFloatField
      FieldName = 'ValorIr'
    end
    object QrDPSValorCsll: TFloatField
      FieldName = 'ValorCsll'
    end
    object QrDPSOutrasRetencoes: TFloatField
      FieldName = 'OutrasRetencoes'
    end
    object QrDPSValorIss: TFloatField
      FieldName = 'ValorIss'
    end
    object QrDPSAliquota: TFloatField
      FieldName = 'Aliquota'
    end
    object QrDPSDescontoIncondicionado: TFloatField
      FieldName = 'DescontoIncondicionado'
    end
    object QrDPSDescontoCondicionado: TFloatField
      FieldName = 'DescontoCondicionado'
    end
    object QrDPSIssRetido: TSmallintField
      FieldName = 'IssRetido'
    end
    object QrDPSResponsavelRetencao: TSmallintField
      FieldName = 'ResponsavelRetencao'
    end
    object QrDPSItemListaServico: TWideStringField
      FieldName = 'ItemListaServico'
      Size = 5
    end
    object QrDPSCodigoCnae: TWideStringField
      FieldName = 'CodigoCnae'
    end
    object QrDPSCodigoTributacaoMunicipio: TWideStringField
      FieldName = 'CodigoTributacaoMunicipio'
    end
    object QrDPSDiscriminacao: TWideMemoField
      FieldName = 'Discriminacao'
      Required = True
      BlobType = ftWideMemo
      Size = 4
    end
    object QrDPSCodigoMunicipio: TIntegerField
      FieldName = 'CodigoMunicipio'
    end
    object QrDPSCodigoPais: TIntegerField
      FieldName = 'CodigoPais'
    end
    object QrDPSExigibilidadeISS: TSmallintField
      FieldName = 'ExigibilidadeISS'
    end
    object QrDPSMunicipioIncidencia: TIntegerField
      FieldName = 'MunicipioIncidencia'
    end
    object QrDPSNumeroProcesso: TWideStringField
      FieldName = 'NumeroProcesso'
      Size = 30
    end
    object QrDPSPrestaCpf: TWideStringField
      FieldName = 'PrestaCpf'
      Size = 18
    end
    object QrDPSPrestaCnpj: TWideStringField
      FieldName = 'PrestaCnpj'
      Size = 18
    end
    object QrDPSPrestaInscricaoMunicipal: TWideStringField
      FieldName = 'PrestaInscricaoMunicipal'
      Size = 18
    end
    object QrDPSTomaCpf: TWideStringField
      FieldName = 'TomaCpf'
      Size = 18
    end
    object QrDPSTomaCnpj: TWideStringField
      FieldName = 'TomaCnpj'
      Size = 18
    end
    object QrDPSTomaInscricaoMunicipal: TWideStringField
      FieldName = 'TomaInscricaoMunicipal'
      Size = 18
    end
    object QrDPSTomaRazaoSocial: TWideStringField
      FieldName = 'TomaRazaoSocial'
      Size = 150
    end
    object QrDPSTomaEndereco: TWideStringField
      FieldName = 'TomaEndereco'
      Size = 125
    end
    object QrDPSTomaNumero: TWideStringField
      FieldName = 'TomaNumero'
      Size = 10
    end
    object QrDPSTomaComplemento: TWideStringField
      FieldName = 'TomaComplemento'
      Size = 60
    end
    object QrDPSTomaBairro: TWideStringField
      FieldName = 'TomaBairro'
      Size = 60
    end
    object QrDPSTomaCodigoMunicipio: TIntegerField
      FieldName = 'TomaCodigoMunicipio'
    end
    object QrDPSTomaUf: TWideStringField
      FieldName = 'TomaUf'
      Size = 125
    end
    object QrDPSTomaCodigoPais: TIntegerField
      FieldName = 'TomaCodigoPais'
    end
    object QrDPSTomaCep: TIntegerField
      FieldName = 'TomaCep'
    end
    object QrDPSIntermeCpf: TWideStringField
      FieldName = 'IntermeCpf'
      Size = 18
    end
    object QrDPSIntermeCnpj: TWideStringField
      FieldName = 'IntermeCnpj'
      Size = 18
    end
    object QrDPSIntermeInscricaoMunicipal: TWideStringField
      FieldName = 'IntermeInscricaoMunicipal'
      Size = 18
    end
    object QrDPSIntermeRazaoSocial: TWideStringField
      FieldName = 'IntermeRazaoSocial'
      Size = 150
    end
    object QrDPSConstrucaoCivilCodigoObra: TWideStringField
      FieldName = 'ConstrucaoCivilCodigoObra'
      Size = 15
    end
    object QrDPSConstrucaoCivilArt: TWideStringField
      FieldName = 'ConstrucaoCivilArt'
      Size = 15
    end
    object QrDPSRegimeEspecialTributacao: TSmallintField
      FieldName = 'RegimeEspecialTributacao'
    end
    object QrDPSOptanteSimplesNacional: TSmallintField
      FieldName = 'OptanteSimplesNacional'
    end
    object QrDPSIncentivoFiscal: TSmallintField
      FieldName = 'IncentivoFiscal'
    end
    object QrDPSId: TWideStringField
      FieldName = 'Id'
      Size = 255
    end
    object QrDPSStatus: TIntegerField
      FieldName = 'Status'
    end
    object QrDPSLastLRpsC: TIntegerField
      FieldName = 'LastLRpsC'
    end
    object QrDPSQuemPagaISS: TSmallintField
      FieldName = 'QuemPagaISS'
    end
    object QrDPSNaturezaOperacao: TIntegerField
      FieldName = 'NaturezaOperacao'
    end
    object QrDPSTomaContatoTelefone: TWideStringField
      FieldName = 'TomaContatoTelefone'
    end
    object QrDPSTomaContatoEmail: TWideStringField
      FieldName = 'TomaContatoEmail'
      Size = 80
    end
    object QrDPSTomaNomeFantasia: TWideStringField
      FieldName = 'TomaNomeFantasia'
      Size = 60
    end
    object QrDPSNFSeFatCab: TIntegerField
      FieldName = 'NFSeFatCab'
    end
    object QrDPSTXT_DPS_TOMA_CNPJ_CPF: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TXT_DPS_TOMA_CNPJ_CPF'
      Size = 30
      Calculated = True
    end
    object QrDPSTXT_DPS_TOMA_ENDERECO: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TXT_DPS_TOMA_ENDERECO'
      Size = 255
      Calculated = True
    end
    object QrDPSTXT_DPS_TOMA_CEP: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TXT_DPS_TOMA_CEP'
      Size = 15
      Calculated = True
    end
    object QrDPSTXT_DPS_TOMA_CIDADE: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TXT_DPS_TOMA_CIDADE'
      Size = 255
      Calculated = True
    end
    object QrDPSTXT_DPS_TOMA_TELEFONE: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TXT_DPS_TOMA_TELEFONE'
      Size = 50
      Calculated = True
    end
    object QrDPSTXT_ItemListaServico: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TXT_ItemListaServico'
      Size = 255
      Calculated = True
    end
    object QrDPSTXT_RECOLHIMENTO: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TXT_RECOLHIMENTO'
      Size = 50
      Calculated = True
    end
    object QrDPSTXT_MUNICIPIOCREDOR: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TXT_MUNICIPIOCREDOR'
      Size = 255
      Calculated = True
    end
    object QrDPSPDFCompres: TSmallintField
      FieldName = 'PDFCompres'
    end
    object QrDPSPDFEmbFont: TSmallintField
      FieldName = 'PDFEmbFont'
    end
    object QrDPSPDFPrnOptm: TSmallintField
      FieldName = 'PDFPrnOptm'
    end
  end
  object frxDsDPS: TfrxDBDataset
    UserName = 'frxDsDPS'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Codigo=Codigo'
      'Ambiente=Ambiente'
      'Empresa=Empresa'
      'Cliente=Cliente'
      'Intermediario=Intermediario'
      'NFSeSrvCad=NFSeSrvCad'
      'RpsID=RpsID'
      'RpsIDNumero=RpsIDNumero'
      'RpsIDSerie=RpsIDSerie'
      'RpsIDTipo=RpsIDTipo'
      'RpsDataEmissao=RpsDataEmissao'
      'RpsHoraEmissao=RpsHoraEmissao'
      'RpsStatus=RpsStatus'
      'Competencia=Competencia'
      'SubstNumero=SubstNumero'
      'SubstSerie=SubstSerie'
      'SubstTipo=SubstTipo'
      'ValorServicos=ValorServicos'
      'ValorDeducoes=ValorDeducoes'
      'ValorPis=ValorPis'
      'ValorCofins=ValorCofins'
      'ValorInss=ValorInss'
      'ValorIr=ValorIr'
      'ValorCsll=ValorCsll'
      'OutrasRetencoes=OutrasRetencoes'
      'ValorIss=ValorIss'
      'Aliquota=Aliquota'
      'DescontoIncondicionado=DescontoIncondicionado'
      'DescontoCondicionado=DescontoCondicionado'
      'IssRetido=IssRetido'
      'ResponsavelRetencao=ResponsavelRetencao'
      'ItemListaServico=ItemListaServico'
      'CodigoCnae=CodigoCnae'
      'CodigoTributacaoMunicipio=CodigoTributacaoMunicipio'
      'Discriminacao=Discriminacao'
      'CodigoMunicipio=CodigoMunicipio'
      'CodigoPais=CodigoPais'
      'ExigibilidadeISS=ExigibilidadeISS'
      'MunicipioIncidencia=MunicipioIncidencia'
      'NumeroProcesso=NumeroProcesso'
      'PrestaCpf=PrestaCpf'
      'PrestaCnpj=PrestaCnpj'
      'PrestaInscricaoMunicipal=PrestaInscricaoMunicipal'
      'TomaCpf=TomaCpf'
      'TomaCnpj=TomaCnpj'
      'TomaInscricaoMunicipal=TomaInscricaoMunicipal'
      'TomaRazaoSocial=TomaRazaoSocial'
      'TomaEndereco=TomaEndereco'
      'TomaNumero=TomaNumero'
      'TomaComplemento=TomaComplemento'
      'TomaBairro=TomaBairro'
      'TomaCodigoMunicipio=TomaCodigoMunicipio'
      'TomaUf=TomaUf'
      'TomaCodigoPais=TomaCodigoPais'
      'TomaCep=TomaCep'
      'IntermeCpf=IntermeCpf'
      'IntermeCnpj=IntermeCnpj'
      'IntermeInscricaoMunicipal=IntermeInscricaoMunicipal'
      'IntermeRazaoSocial=IntermeRazaoSocial'
      'ConstrucaoCivilCodigoObra=ConstrucaoCivilCodigoObra'
      'ConstrucaoCivilArt=ConstrucaoCivilArt'
      'RegimeEspecialTributacao=RegimeEspecialTributacao'
      'OptanteSimplesNacional=OptanteSimplesNacional'
      'IncentivoFiscal=IncentivoFiscal'
      'Id=Id'
      'Status=Status'
      'LastLRpsC=LastLRpsC'
      'QuemPagaISS=QuemPagaISS'
      'NaturezaOperacao=NaturezaOperacao'
      'TomaContatoTelefone=TomaContatoTelefone'
      'TomaContatoEmail=TomaContatoEmail'
      'TomaNomeFantasia=TomaNomeFantasia'
      'NFSeFatCab=NFSeFatCab'
      'TXT_DPS_TOMA_CNPJ_CPF=TXT_DPS_TOMA_CNPJ_CPF'
      'TXT_DPS_TOMA_ENDERECO=TXT_DPS_TOMA_ENDERECO'
      'TXT_DPS_TOMA_CEP=TXT_DPS_TOMA_CEP'
      'TXT_DPS_TOMA_CIDADE=TXT_DPS_TOMA_CIDADE'
      'TXT_DPS_TOMA_TELEFONE=TXT_DPS_TOMA_TELEFONE'
      'TXT_ItemListaServico=TXT_ItemListaServico'
      'TXT_RECOLHIMENTO=TXT_RECOLHIMENTO'
      'TXT_MUNICIPIOCREDOR=TXT_MUNICIPIOCREDOR'
      'PDFCompres=PDFCompres'
      'PDFEmbFont=PDFEmbFont'
      'PDFPrnOptm=PDFPrnOptm'
      'TXT_DPSNATUREZAOPERACAO=TXT_DPSNATUREZAOPERACAO')
    DataSet = QrDPS
    BCDToCurrency = False
    DataSetOptions = []
    Left = 120
    Top = 196
  end
  object QrILS: TMySQLQuery
    Database = DModG.AllID_DB
    SQL.Strings = (
      'SELECT CodTxt, Nome'
      'FROM listserv'
      'WHERE CodAlf=:P0')
    Left = 212
    Top = 196
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrILSNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 255
    end
    object QrILSCodTxt: TWideStringField
      FieldName = 'CodTxt'
      Required = True
    end
  end
  object frxPDFExport: TfrxPDFExport
    ShowDialog = False
    UseFileCache = True
    ShowProgress = False
    OverwritePrompt = False
    DataOnly = False
    EmbeddedFonts = True
    EmbedFontsIfProtected = False
    InteractiveFormsFontSubset = 'A-Z,a-z,0-9,#43-#47 '
    OpenAfterExport = False
    PrintOptimized = True
    Outline = False
    Background = False
    HTMLTags = True
    Quality = 95
    Transparency = False
    Author = 'Dermatek'
    Subject = 'FastReport PDF export'
    Creator = 'FastReport'
    ProtectionFlags = [ePrint, eModify, eCopy, eAnnot]
    HideToolbar = False
    HideMenubar = False
    HideWindowUI = False
    FitWindow = False
    CenterWindow = False
    PrintScaling = False
    PdfA = False
    PDFStandard = psNone
    PDFVersion = pv17
    Left = 440
    Top = 44
  end
  object QrNFSeMenCab: TMySQLQuery
    Database = Dmod.MyDB
    Left = 312
    Top = 4
  end
  object QrIBPTax: TMySQLQuery
    Database = Dmod.ZZDB
    SQL.Strings = (
      'SELECT * '
      'FROM ibptax'
      'WHERE Tabela=0'
      'AND Codigo=5021019'
      'AND Ex=0')
    Left = 312
    Top = 52
    object QrIBPTaxCodigo: TLargeintField
      FieldName = 'Codigo'
    end
    object QrIBPTaxEx: TIntegerField
      FieldName = 'Ex'
    end
    object QrIBPTaxTabela: TIntegerField
      FieldName = 'Tabela'
    end
    object QrIBPTaxNome: TWideStringField
      FieldName = 'Nome'
      Size = 255
    end
    object QrIBPTaxDescricao: TWideMemoField
      FieldName = 'Descricao'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrIBPTaxAliqNac: TFloatField
      FieldName = 'AliqNac'
    end
    object QrIBPTaxAliqImp: TFloatField
      FieldName = 'AliqImp'
    end
    object QrIBPTaxVersao: TWideStringField
      FieldName = 'Versao'
    end
    object QrIBPTaxLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrIBPTaxDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrIBPTaxDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrIBPTaxUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrIBPTaxUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrIBPTaxAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrIBPTaxAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrIBPTaxChave: TWideStringField
      FieldName = 'Chave'
    end
  end
  object ACBrNFSe1: TACBrNFSe
    Configuracoes.Geral.SSLLib = libNone
    Configuracoes.Geral.SSLCryptLib = cryNone
    Configuracoes.Geral.SSLHttpLib = httpNone
    Configuracoes.Geral.SSLXmlSignLib = xsNone
    Configuracoes.Geral.FormatoAlerta = 'TAG:%TAGNIVEL% ID:%ID%/%TAG%(%DESCRICAO%) - %MSG%.'
    Configuracoes.Geral.CodigoMunicipio = 0
    Configuracoes.Geral.ConsultaLoteAposEnvio = False
    Configuracoes.Geral.Emitente.DadosSenhaParams = <>
    Configuracoes.Geral.Resposta = 0
    Configuracoes.Arquivos.OrdenacaoPath = <>
    Configuracoes.WebServices.UF = 'SP'
    Configuracoes.WebServices.AguardarConsultaRet = 0
    Configuracoes.WebServices.QuebradeLinha = '|'
    Left = 68
    Top = 292
  end
  object OpenDialog1: TOpenDialog
    Left = 236
    Top = 332
  end
  object QrPsqNfsePorRPS: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT NfsNumero, Empresa, Ambiente'
      'FROM nfsenfscab'
      'WHERE Empresa=-11'
      'AND DpsRpsIdNumero=1'
      'AND DpsRpsIdSerie="1"'
      'AND DpsRpsIdTipo=1'
      'AND Ambiente=2'
      '')
    Left = 613
    Top = 344
    object QrPsqNfsePorRPSNfsNumero: TLargeintField
      FieldName = 'NfsNumero'
    end
    object QrPsqNfsePorRPSEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrPsqNfsePorRPSAmbiente: TSmallintField
      FieldName = 'Ambiente'
    end
  end
  object ACBrNFSeX1: TACBrNFSeX
    Configuracoes.Geral.SSLLib = libNone
    Configuracoes.Geral.SSLCryptLib = cryNone
    Configuracoes.Geral.SSLHttpLib = httpNone
    Configuracoes.Geral.SSLXmlSignLib = xsNone
    Configuracoes.Geral.FormatoAlerta = 'TAG:%TAGNIVEL% ID:%ID%/%TAG%(%DESCRICAO%) - %MSG%.'
    Configuracoes.Geral.CodigoMunicipio = 0
    Configuracoes.Geral.Provedor = proNenhum
    Configuracoes.Geral.Versao = ve100
    Configuracoes.Arquivos.OrdenacaoPath = <>
    Configuracoes.WebServices.UF = 'SP'
    Configuracoes.WebServices.AguardarConsultaRet = 0
    Configuracoes.WebServices.QuebradeLinha = '|'
    Left = 68
    Top = 344
  end
end
