object FmTsErrosAlertasCab: TFmTsErrosAlertasCab
  Left = 368
  Top = 194
  Caption = 'NFS-TBTER-001 :: Tabelas de Erros e Altertas da NFS-e'
  ClientHeight = 451
  ClientWidth = 784
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnEdita: TPanel
    Left = 0
    Top = 105
    Width = 784
    Height = 346
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    Visible = False
    object GBEdita: TGroupBox
      Left = 0
      Top = 0
      Width = 784
      Height = 125
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 0
      object Label7: TLabel
        Left = 16
        Top = 16
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label9: TLabel
        Left = 76
        Top = 16
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
        Color = clBtnFace
        ParentColor = False
      end
      object EdCodigo: TdmkEdit
        Left = 16
        Top = 32
        Width = 56
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Codigo'
        UpdCampo = 'Codigo'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdNome: TdmkEdit
        Left = 76
        Top = 32
        Width = 633
        Height = 21
        TabOrder = 1
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Nome'
        UpdCampo = 'Nome'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
    end
    object GBConfirma: TGroupBox
      Left = 0
      Top = 283
      Width = 784
      Height = 63
      Align = alBottom
      TabOrder = 1
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 12
        Top = 17
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object Panel1: TPanel
        Left = 674
        Top = 15
        Width = 108
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 7
          Top = 2
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
    end
  end
  object PnDados: TPanel
    Left = 0
    Top = 105
    Width = 784
    Height = 346
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    object GBDados: TGroupBox
      Left = 0
      Top = 49
      Width = 784
      Height = 65
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 1
      object Label1: TLabel
        Left = 16
        Top = 16
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
        FocusControl = DBEdCodigo
      end
      object Label2: TLabel
        Left = 76
        Top = 16
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
        FocusControl = DBEdNome
      end
      object DBEdCodigo: TdmkDBEdit
        Left = 16
        Top = 32
        Width = 56
        Height = 21
        TabStop = False
        DataField = 'Codigo'
        DataSource = DsTsErrosAlertasCab
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 0
        UpdType = utYes
        Alignment = taRightJustify
      end
      object DBEdNome: TdmkDBEdit
        Left = 76
        Top = 32
        Width = 689
        Height = 21
        Color = clWhite
        DataField = 'Nome'
        DataSource = DsTsErrosAlertasCab
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        UpdType = utYes
        Alignment = taLeftJustify
      end
    end
    object GBCntrl: TGroupBox
      Left = 0
      Top = 282
      Width = 784
      Height = 64
      Align = alBottom
      TabOrder = 0
      object Panel5: TPanel
        Left = 2
        Top = 15
        Width = 172
        Height = 47
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 174
        Top = 15
        Width = 30
        Height = 17
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel3: TPanel
        Left = 261
        Top = 15
        Width = 521
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object Panel2: TPanel
          Left = 388
          Top = 0
          Width = 133
          Height = 47
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 0
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
        object BtCab: TBitBtn
          Tag = 350
          Left = 4
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Tabela'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtCabClick
        end
        object BtIts: TBitBtn
          Tag = 351
          Left = 128
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Itens'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = BtItsClick
        end
        object BtCidades: TBitBtn
          Tag = 531
          Left = 252
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Cidade'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = BtCidadesClick
        end
      end
    end
    object DGDados: TDBGrid
      Left = 0
      Top = 114
      Width = 165
      Height = 168
      Align = alLeft
      DataSource = DsTsErrosAlertasIts
      TabOrder = 2
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'CodTxt'
          Width = 123
          Visible = True
        end>
    end
    object PnData: TPanel
      Left = 165
      Top = 65
      Width = 619
      Height = 168
      BevelOuter = bvNone
      TabOrder = 3
      object Label3: TLabel
        Left = 0
        Top = 0
        Width = 619
        Height = 17
        Align = alTop
        Alignment = taCenter
        AutoSize = False
        Caption = 'Explica'#231#227'o:'
        ExplicitLeft = 12
        ExplicitTop = -7
      end
      object Label4: TLabel
        Left = 0
        Top = 106
        Width = 619
        Height = 15
        Align = alTop
        Alignment = taCenter
        AutoSize = False
        Caption = 'Solu'#231#227'o:'
        ExplicitTop = 91
      end
      object DBMemo1: TDBMemo
        Left = 0
        Top = 17
        Width = 619
        Height = 89
        Align = alTop
        DataField = 'Nome'
        DataSource = DsTsErrosAlertasIts
        TabOrder = 0
      end
      object DBMemo2: TDBMemo
        Left = 0
        Top = 121
        Width = 619
        Height = 47
        Align = alClient
        DataField = 'Solucao'
        DataSource = DsTsErrosAlertasIts
        TabOrder = 1
      end
    end
    object DBGrid1: TDBGrid
      Left = 556
      Top = 114
      Width = 228
      Height = 168
      Align = alRight
      DataSource = DsTsErrosAlertasMun
      TabOrder = 4
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'Cidade'
          Title.Caption = 'C'#243'digo'
          Width = 50
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_CIDADE'
          Title.Caption = 'Cidade'
          Width = 138
          Visible = True
        end>
    end
    object Panel6: TPanel
      Left = 0
      Top = 0
      Width = 784
      Height = 49
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 5
      object Label27: TLabel
        Left = 8
        Top = 7
        Width = 114
        Height = 13
        Caption = 'Arquivo a ser importado:'
      end
      object SpeedButton8: TSpeedButton
        Left = 754
        Top = 23
        Width = 21
        Height = 21
        OnClick = SpeedButton8Click
      end
      object EdArq: TdmkEdit
        Left = 8
        Top = 23
        Width = 745
        Height = 21
        TabOrder = 0
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = 
          'http://www.dermatek.com.br/Instaladores/LC 116 03 _ ListaServ.tx' +
          't'
        QryCampo = 'DirNFeGer'
        UpdCampo = 'DirNFeGer'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 
          'http://www.dermatek.com.br/Instaladores/LC 116 03 _ ListaServ.tx' +
          't'
        ValWarn = False
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 784
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 736
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 12
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 216
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 216
      Top = 0
      Width = 520
      Height = 52
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 450
        Height = 32
        Caption = 'Tabelas de Erros e Altertas da NFS-e'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 450
        Height = 32
        Caption = 'Tabelas de Erros e Altertas da NFS-e'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 450
        Height = 32
        Caption = 'Tabelas de Erros e Altertas da NFS-e'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 52
    Width = 784
    Height = 53
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 780
      Height = 36
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object PB1: TProgressBar
        Left = 0
        Top = 19
        Width = 780
        Height = 17
        Align = alBottom
        TabOrder = 0
      end
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = BtCab
    Left = 120
    Top = 64
  end
  object QrTsErrosAlertasCab: TmySQLQuery
    Database = DModG.AllID_DB
    BeforeOpen = QrTsErrosAlertasCabBeforeOpen
    AfterOpen = QrTsErrosAlertasCabAfterOpen
    AfterScroll = QrTsErrosAlertasCabAfterScroll
    SQL.Strings = (
      'SELECT *'
      'FROM tserrosalertascab')
    Left = 64
    Top = 65
    object QrTsErrosAlertasCabCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrTsErrosAlertasCabNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
  object DsTsErrosAlertasCab: TDataSource
    DataSet = QrTsErrosAlertasCab
    Left = 92
    Top = 65
  end
  object QrTsErrosAlertasIts: TmySQLQuery
    Database = DModG.AllID_DB
    SQL.Strings = (
      'SELECT * '
      'FROM tserrosalertasits'
      'WHERE Codigo =:P0'
      'ORDER BY Nome')
    Left = 148
    Top = 65
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrTsErrosAlertasItsCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrTsErrosAlertasItsCodTxt: TWideStringField
      FieldName = 'CodTxt'
      Size = 12
    end
    object QrTsErrosAlertasItsNome: TWideStringField
      FieldName = 'Nome'
      Size = 255
    end
    object QrTsErrosAlertasItsSolucao: TWideMemoField
      FieldName = 'Solucao'
      BlobType = ftWideMemo
      Size = 4
    end
  end
  object DsTsErrosAlertasIts: TDataSource
    DataSet = QrTsErrosAlertasIts
    Left = 176
    Top = 65
  end
  object PMIts: TPopupMenu
    OnPopup = PMItsPopup
    Left = 436
    Top = 372
    object ItsInclui1: TMenuItem
      Caption = '&Inclui'
      Enabled = False
      OnClick = ItsInclui1Click
    end
    object ItsAltera1: TMenuItem
      Caption = '&Edita'
      Enabled = False
      OnClick = ItsAltera1Click
    end
    object ItsExclui1: TMenuItem
      Caption = '&Exclui'
      Enabled = False
      OnClick = ItsExclui1Click
      object Itemselecionado1: TMenuItem
        Caption = '&Item selecionado'
        OnClick = Itemselecionado1Click
      end
      object odositensdogrupo1: TMenuItem
        Caption = '&Todos itens do grupo'
        OnClick = odositensdogrupo1Click
      end
    end
    object N1: TMenuItem
      Caption = '-'
      Enabled = False
    end
    object Importa1: TMenuItem
      Caption = '&Importa'
      object XT3linhasporitem1: TMenuItem
        Caption = 'TXT (3 linhas por item)'
        OnClick = XT3linhasporitem1Click
      end
    end
  end
  object PMCab: TPopupMenu
    OnPopup = PMCabPopup
    Left = 300
    Top = 372
    object CabInclui1: TMenuItem
      Caption = '&Inclui'
      OnClick = CabInclui1Click
    end
    object CabAltera1: TMenuItem
      Caption = '&Altera'
      Enabled = False
      OnClick = CabAltera1Click
    end
    object CabExclui1: TMenuItem
      Caption = '&Exclui'
      Enabled = False
      OnClick = CabExclui1Click
    end
  end
  object QrTsErrosAlertasMun: TmySQLQuery
    Database = DModG.AllID_DB
    SQL.Strings = (
      'SELECT eam.*, dtm.Nome NO_CIDADE '
      'FROM tserrosalertasmun eam'
      'LEFT JOIN dtb_munici dtm ON dtm.Codigo=eam.Cidade'
      'WHERE eam.Codigo =:P0')
    Left = 204
    Top = 65
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrTsErrosAlertasMunCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrTsErrosAlertasMunCidade: TIntegerField
      FieldName = 'Cidade'
    end
    object QrTsErrosAlertasMunNO_CIDADE: TWideStringField
      FieldName = 'NO_CIDADE'
      Size = 100
    end
  end
  object DsTsErrosAlertasMun: TDataSource
    DataSet = QrTsErrosAlertasMun
    Left = 232
    Top = 65
  end
  object QrPsq: TmySQLQuery
    Database = DModG.AllID_DB
    SQL.Strings = (
      'SELECT COUNT(CodTxt) Itens'
      'FROM tserrosalertas'
      'WHERE CodTxt=""')
    Left = 260
    Top = 64
    object QrPsqItens: TLargeintField
      FieldName = 'Itens'
      Required = True
    end
  end
  object PMCidades: TPopupMenu
    Left = 520
    Top = 372
    object Adiciona1: TMenuItem
      Caption = '&Adiciona'
      OnClick = Adiciona1Click
    end
    object Remove1: TMenuItem
      Caption = '&Remove'
      OnClick = Remove1Click
    end
  end
end
