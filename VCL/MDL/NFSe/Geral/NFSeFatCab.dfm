object FmNFSeFatCab: TFmNFSeFatCab
  Left = 368
  Top = 194
  Caption = 'NFS-FATUR-001 :: Faturamento de Servi'#231'os'
  ClientHeight = 347
  ClientWidth = 791
  Color = clBtnFace
  Constraints.MinHeight = 256
  Constraints.MinWidth = 630
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object PnEdita: TPanel
    Left = 0
    Top = 94
    Width = 791
    Height = 253
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    Visible = False
    ExplicitWidth = 993
    ExplicitHeight = 350
    object GBEdita: TGroupBox
      Left = 0
      Top = 0
      Width = 791
      Height = 154
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 0
      ExplicitWidth = 1241
      object Label7: TLabel
        Left = 20
        Top = 20
        Width = 36
        Height = 13
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'C'#243'digo:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label9: TLabel
        Left = 20
        Top = 69
        Width = 51
        Height = 13
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Descri'#231#227'o:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label3: TLabel
        Left = 94
        Top = 20
        Width = 44
        Height = 13
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Empresa:'
      end
      object LaMes: TLabel
        Left = 734
        Top = 20
        Width = 44
        Height = 13
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'M'#234's [F5]:'
      end
      object EdCodigo: TdmkEdit
        Left = 20
        Top = 39
        Width = 69
        Height = 26
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Codigo'
        UpdCampo = 'Codigo'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdNome: TdmkEdit
        Left = 20
        Top = 89
        Width = 779
        Height = 25
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        TabOrder = 4
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Nome'
        UpdCampo = 'Nome'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdEmpresa: TdmkEditCB
        Left = 92
        Top = 39
        Width = 54
        Height = 26
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        TabOrder = 1
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Filial'
        UpdCampo = 'Empresa'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBEmpresa
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBEmpresa: TdmkDBLookupComboBox
        Left = 148
        Top = 39
        Width = 582
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        KeyField = 'Filial'
        ListField = 'NOMEFILIAL'
        ListSource = DModG.DsEmpresas
        TabOrder = 2
        dmkEditCB = EdEmpresa
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object EdCompetenci: TdmkEdit
        Left = 734
        Top = 39
        Width = 66
        Height = 26
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taCenter
        TabOrder = 3
        FormatType = dmktfMesAno
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfLong
        HoraFormat = dmkhfShort
        QryCampo = 'Competenci'
        UpdCampo = 'Competenci'
        UpdType = utYes
        Obrigatorio = True
        PermiteNulo = False
        ValueVariant = Null
        ValWarn = False
      end
    end
    object GBConfirma: TGroupBox
      Left = 0
      Top = 175
      Width = 791
      Height = 78
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alBottom
      TabOrder = 1
      ExplicitTop = 359
      ExplicitWidth = 1241
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 15
        Top = 21
        Width = 147
        Height = 49
        Cursor = crHandPoint
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object Panel1: TPanel
        Left = 1073
        Top = 18
        Width = 166
        Height = 58
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 9
          Top = 2
          Width = 147
          Height = 50
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
    end
  end
  object PnDados: TPanel
    Left = 0
    Top = 94
    Width = 791
    Height = 253
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    ExplicitWidth = 993
    ExplicitHeight = 350
    object LaTotal: TLabel
      Left = 0
      Top = 342
      Width = 3
      Height = 13
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alBottom
    end
    object GBDados: TGroupBox
      Left = 0
      Top = 21
      Width = 1241
      Height = 144
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 1
      object Label1: TLabel
        Left = 20
        Top = 20
        Width = 36
        Height = 13
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'C'#243'digo:'
        FocusControl = DBEdCodigo
      end
      object Label2: TLabel
        Left = 20
        Top = 69
        Width = 51
        Height = 13
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Descri'#231#227'o:'
        FocusControl = DBEdNome
      end
      object Label4: TLabel
        Left = 94
        Top = 20
        Width = 44
        Height = 13
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Empresa:'
        FocusControl = DBEdit1
      end
      object Label5: TLabel
        Left = 734
        Top = 20
        Width = 44
        Height = 13
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'M'#234's [F5]:'
      end
      object Label6: TLabel
        Left = 802
        Top = 20
        Width = 43
        Height = 13
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Abertura:'
        FocusControl = DBEdit4
      end
      object Label8: TLabel
        Left = 802
        Top = 69
        Width = 73
        Height = 13
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = #218'ltima emiss'#227'o:'
        FocusControl = DBEdit5
      end
      object Label10: TLabel
        Left = 734
        Top = 69
        Width = 26
        Height = 13
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Itens:'
      end
      object DBEdCodigo: TdmkDBEdit
        Left = 20
        Top = 39
        Width = 69
        Height = 24
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        TabStop = False
        DataField = 'Codigo'
        DataSource = DsNFSeFatCab
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 0
        UpdType = utYes
        Alignment = taRightJustify
      end
      object DBEdNome: TdmkDBEdit
        Left = 20
        Top = 89
        Width = 710
        Height = 24
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Color = clWhite
        DataField = 'Nome'
        DataSource = DsNFSeFatCab
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        UpdType = utYes
        Alignment = taLeftJustify
      end
      object DBEdit1: TDBEdit
        Left = 94
        Top = 39
        Width = 49
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        DataField = 'Empresa'
        DataSource = DsNFSeFatCab
        TabOrder = 2
      end
      object DBEdit2: TDBEdit
        Left = 143
        Top = 39
        Width = 587
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        DataField = 'NO_EMP'
        DataSource = DsNFSeFatCab
        TabOrder = 3
      end
      object DBEdit3: TDBEdit
        Left = 734
        Top = 39
        Width = 66
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        DataField = 'TXT_COMPETENCI'
        DataSource = DsNFSeFatCab
        TabOrder = 4
      end
      object DBEdit4: TDBEdit
        Left = 802
        Top = 39
        Width = 153
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        DataField = 'TXT_INICIO'
        DataSource = DsNFSeFatCab
        TabOrder = 5
      end
      object DBEdit5: TDBEdit
        Left = 802
        Top = 89
        Width = 153
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        DataField = 'TXT_FINAL'
        DataSource = DsNFSeFatCab
        TabOrder = 6
      end
      object EdItens: TdmkEdit
        Left = 734
        Top = 89
        Width = 65
        Height = 25
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        ReadOnly = True
        TabOrder = 7
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
    end
    object GBCntrl: TGroupBox
      Left = 0
      Top = 358
      Width = 1241
      Height = 79
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alBottom
      TabOrder = 0
      object Panel5: TPanel
        Left = 2
        Top = 18
        Width = 212
        Height = 59
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 158
          Top = 5
          Width = 49
          Height = 49
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 108
          Top = 5
          Width = 50
          Height = 49
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 59
          Top = 5
          Width = 49
          Height = 49
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 10
          Top = 5
          Width = 49
          Height = 49
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 214
        Top = 18
        Width = 30
        Height = 17
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel3: TPanel
        Left = 598
        Top = 18
        Width = 641
        Height = 59
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object Panel2: TPanel
          Left = 478
          Top = 0
          Width = 163
          Height = 59
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 0
          object BtSaida: TBitBtn
            Tag = 13
            Left = 5
            Top = 5
            Width = 148
            Height = 49
            Cursor = crHandPoint
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
        object BtCab: TBitBtn
          Tag = 546
          Left = 5
          Top = 5
          Width = 148
          Height = 49
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Malote'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtCabClick
        end
        object BtIts: TBitBtn
          Tag = 543
          Left = 158
          Top = 5
          Width = 147
          Height = 49
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&DPS'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = BtItsClick
        end
        object BtRPS: TBitBtn
          Tag = 542
          Left = 310
          Top = 5
          Width = 148
          Height = 49
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&RPS'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = BtRPSClick
        end
      end
    end
    object DGDados: TdmkDBGrid
      Left = 37
      Top = 172
      Width = 1241
      Height = 142
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Columns = <
        item
          Expanded = False
          FieldName = 'Codigo'
          Title.Caption = 'DPS'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Ambiente'
          Title.Caption = 'Amb'
          Width = 28
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Status'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          Width = 36
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'LastLRpsC'
          Title.Caption = 'Lote'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Cliente'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_CLI'
          Title.Caption = 'Nome cliente (Tomador)'
          Width = 140
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Intermediario'
          Title.Caption = 'Intermed.'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_INT'
          Title.Caption = 'Nome intermedi'#225'rio'
          Width = 140
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'RpsIDTipo'
          Title.Caption = 'Tipo'
          Width = 28
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'RpsIDSerie'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          Title.Caption = 'S'#233'rie'
          Width = 32
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'RpsIDNumero'
          Title.Caption = 'N'#186' RPS'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NfsNumero'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          Title.Caption = 'N'#186' NFS-e'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'RpsDataEmissao'
          Title.Caption = 'Data emis.'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'RpsHoraEmissao'
          Title.Caption = 'Hora emis.'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'RpsStatus'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ValorServicos'
          Title.Caption = '$ Servi'#231'os'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ValorIss'
          Title.Caption = '$ ISS'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Aliquota'
          Title.Caption = 'Al'#237'quota'
          Width = 48
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'IssRetido'
          Width = 50
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ResponsavelRetencao'
          Title.Caption = 'Resp.ret.'
          Width = 48
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ItemListaServico'
          Title.Caption = 'LC 116/03'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Discriminacao'
          Width = 600
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ValorPis'
          Width = 60
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ValorCofins'
          Width = 60
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ValorInss'
          Width = 60
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ValorIr'
          Width = 60
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ValorCsll'
          Width = 60
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'OutrasRetencoes'
          Width = 60
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'FatID'
          Title.Caption = 'Fat ID'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Lancto'
          Title.Caption = 'Lct. Financ.'
          Width = 75
          Visible = True
        end>
      Color = clWindow
      DataSource = DsNFSeDPSCab
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
      TabOrder = 2
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -12
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      OnDrawColumnCell = DGDadosDrawColumnCell
      Columns = <
        item
          Expanded = False
          FieldName = 'Codigo'
          Title.Caption = 'DPS'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Ambiente'
          Title.Caption = 'Amb'
          Width = 28
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Status'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          Width = 36
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'LastLRpsC'
          Title.Caption = 'Lote'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Cliente'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_CLI'
          Title.Caption = 'Nome cliente (Tomador)'
          Width = 140
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Intermediario'
          Title.Caption = 'Intermed.'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_INT'
          Title.Caption = 'Nome intermedi'#225'rio'
          Width = 140
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'RpsIDTipo'
          Title.Caption = 'Tipo'
          Width = 28
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'RpsIDSerie'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          Title.Caption = 'S'#233'rie'
          Width = 32
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'RpsIDNumero'
          Title.Caption = 'N'#186' RPS'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NfsNumero'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          Title.Caption = 'N'#186' NFS-e'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'RpsDataEmissao'
          Title.Caption = 'Data emis.'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'RpsHoraEmissao'
          Title.Caption = 'Hora emis.'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'RpsStatus'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ValorServicos'
          Title.Caption = '$ Servi'#231'os'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ValorIss'
          Title.Caption = '$ ISS'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Aliquota'
          Title.Caption = 'Al'#237'quota'
          Width = 48
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'IssRetido'
          Width = 50
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ResponsavelRetencao'
          Title.Caption = 'Resp.ret.'
          Width = 48
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ItemListaServico'
          Title.Caption = 'LC 116/03'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Discriminacao'
          Width = 600
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ValorPis'
          Width = 60
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ValorCofins'
          Width = 60
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ValorInss'
          Width = 60
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ValorIr'
          Width = 60
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ValorCsll'
          Width = 60
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'OutrasRetencoes'
          Width = 60
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'FatID'
          Title.Caption = 'Fat ID'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Lancto'
          Title.Caption = 'Lct. Financ.'
          Width = 75
          Visible = True
        end>
    end
    object PB1: TProgressBar
      Left = 0
      Top = 0
      Width = 1241
      Height = 21
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alTop
      TabOrder = 3
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 791
    Height = 51
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    ExplicitWidth = 993
    object GB_R: TGroupBox
      Left = 1182
      Top = 0
      Width = 59
      Height = 64
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 10
        Top = 15
        Width = 39
        Height = 39
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 318
      Height = 64
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 5
        Top = 10
        Width = 49
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 0
        OnClick = SbImprimeClick
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 57
        Top = 10
        Width = 49
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 108
        Top = 10
        Width = 50
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 160
        Top = 10
        Width = 49
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 212
        Top = 10
        Width = 49
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
      object SbEmail: TBitBtn
        Tag = 244
        Left = 263
        Top = 10
        Width = 49
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 5
        OnClick = SbEmailClick
      end
    end
    object GB_M: TGroupBox
      Left = 318
      Top = 0
      Width = 864
      Height = 64
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 9
        Top = 11
        Width = 356
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Faturamento de Servi'#231'os'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 11
        Top = 14
        Width = 356
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Faturamento de Servi'#231'os'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 10
        Top = 12
        Width = 356
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Faturamento de Servi'#231'os'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 51
    Width = 791
    Height = 43
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    ExplicitWidth = 993
    object Panel4: TPanel
      Left = 2
      Top = 18
      Width = 1237
      Height = 34
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 16
        Top = 2
        Width = 15
        Height = 19
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -17
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 15
        Top = 1
        Width = 15
        Height = 19
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -17
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = BtCab
    Left = 120
    Top = 64
  end
  object QrNFSeFatCab: TMySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrNFSeFatCabBeforeOpen
    AfterOpen = QrNFSeFatCabAfterOpen
    AfterScroll = QrNFSeFatCabAfterScroll
    OnCalcFields = QrNFSeFatCabCalcFields
    SQL.Strings = (
      'SELECT nfc.*, emp.Filial, '
      'IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NO_EMP '
      'FROM nfsefatcab nfc'
      'LEFT JOIN entidades emp ON emp.Codigo=nfc.Empresa'
      'WHERE nfc.Codigo > 0')
    Left = 64
    Top = 65
    object QrNFSeFatCabCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'nfsefatcab.Codigo'
    end
    object QrNFSeFatCabNome: TWideStringField
      FieldName = 'Nome'
      Origin = 'nfsefatcab.Nome'
      Size = 60
    end
    object QrNFSeFatCabEmpresa: TIntegerField
      FieldName = 'Empresa'
      Origin = 'nfsefatcab.Empresa'
    end
    object QrNFSeFatCabDataFatIni: TDateField
      FieldName = 'DataFatIni'
      Origin = 'nfsefatcab.DataFatIni'
    end
    object QrNFSeFatCabHoraFatIni: TTimeField
      FieldName = 'HoraFatIni'
      Origin = 'nfsefatcab.HoraFatIni'
    end
    object QrNFSeFatCabDataFatFim: TDateField
      FieldName = 'DataFatFim'
      Origin = 'nfsefatcab.DataFatFim'
    end
    object QrNFSeFatCabHoraFatFim: TTimeField
      FieldName = 'HoraFatFim'
      Origin = 'nfsefatcab.HoraFatFim'
    end
    object QrNFSeFatCabCompetenci: TIntegerField
      FieldName = 'Competenci'
      Origin = 'nfsefatcab.Competenci'
    end
    object QrNFSeFatCabNO_EMP: TWideStringField
      FieldName = 'NO_EMP'
      Size = 100
    end
    object QrNFSeFatCabTXT_INICIO: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TXT_INICIO'
      Calculated = True
    end
    object QrNFSeFatCabTXT_FINAL: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TXT_FINAL'
      Calculated = True
    end
    object QrNFSeFatCabTXT_COMPETENCI: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TXT_COMPETENCI'
      Size = 7
      Calculated = True
    end
    object QrNFSeFatCabFilial: TIntegerField
      FieldName = 'Filial'
    end
    object QrNFSeFatCabAmbiente: TIntegerField
      FieldName = 'Ambiente'
    end
  end
  object DsNFSeFatCab: TDataSource
    DataSet = QrNFSeFatCab
    Left = 92
    Top = 65
  end
  object PMIts: TPopupMenu
    OnPopup = PMItsPopup
    Left = 636
    Top = 372
    object ItsInclui1: TMenuItem
      Caption = '&Inclui'
      Enabled = False
      OnClick = ItsInclui1Click
    end
    object ItsAltera1: TMenuItem
      Caption = '&Altera'
      Enabled = False
      OnClick = ItsAltera1Click
    end
    object ItsExclui1: TMenuItem
      Caption = '&Exclui'
      Enabled = False
      OnClick = ItsExclui1Click
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object Visualizaconfirmaoderecebimentodafaturaatual1: TMenuItem
      Caption = '&Visualiza confirma'#231#227'o de recebimento da fatura atual'
      OnClick = Visualizaconfirmaoderecebimentodafaturaatual1Click
    end
  end
  object PMCab: TPopupMenu
    OnPopup = PMCabPopup
    Left = 548
    Top = 368
    object CabInclui1: TMenuItem
      Caption = '&Inclui'
      OnClick = CabInclui1Click
    end
    object CabAltera1: TMenuItem
      Caption = '&Altera'
      Enabled = False
      OnClick = CabAltera1Click
    end
    object CabExclui1: TMenuItem
      Caption = '&Exclui'
      Enabled = False
      OnClick = CabExclui1Click
    end
  end
  object QrNFSeDPSCab: TMySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrNFSeDPSCabBeforeClose
    AfterScroll = QrNFSeDPSCabAfterScroll
    SQL.Strings = (
      'SELECT dps.*, '
      'IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NO_CLI, '
      'IF(ntm.Tipo=0, ntm.RazaoSocial, ntm.Nome) NO_INT '
      'FROM nfsedpscab dps'
      'LEFT JOIN entidades cli ON cli.Codigo=dps.Cliente '
      'LEFT JOIN entidades ntm ON ntm.Codigo=dps.Intermediario'
      'WHERE NFSeFatCab=1')
    Left = 148
    Top = 64
    object QrNFSeDPSCabCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrNFSeDPSCabAmbiente: TSmallintField
      FieldName = 'Ambiente'
    end
    object QrNFSeDPSCabEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrNFSeDPSCabCliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrNFSeDPSCabIntermediario: TIntegerField
      FieldName = 'Intermediario'
    end
    object QrNFSeDPSCabNFSeSrvCad: TIntegerField
      FieldName = 'NFSeSrvCad'
    end
    object QrNFSeDPSCabRpsID: TWideStringField
      FieldName = 'RpsID'
      Size = 255
    end
    object QrNFSeDPSCabRpsIDNumero: TIntegerField
      FieldName = 'RpsIDNumero'
    end
    object QrNFSeDPSCabRpsIDSerie: TWideStringField
      FieldName = 'RpsIDSerie'
      Size = 5
    end
    object QrNFSeDPSCabRpsIDTipo: TSmallintField
      FieldName = 'RpsIDTipo'
    end
    object QrNFSeDPSCabRpsDataEmissao: TDateField
      FieldName = 'RpsDataEmissao'
    end
    object QrNFSeDPSCabRpsHoraEmissao: TTimeField
      FieldName = 'RpsHoraEmissao'
    end
    object QrNFSeDPSCabRpsStatus: TIntegerField
      FieldName = 'RpsStatus'
    end
    object QrNFSeDPSCabCompetencia: TDateField
      FieldName = 'Competencia'
    end
    object QrNFSeDPSCabSubstNumero: TIntegerField
      FieldName = 'SubstNumero'
    end
    object QrNFSeDPSCabSubstSerie: TWideStringField
      FieldName = 'SubstSerie'
      Size = 5
    end
    object QrNFSeDPSCabSubstTipo: TSmallintField
      FieldName = 'SubstTipo'
    end
    object QrNFSeDPSCabValorServicos: TFloatField
      FieldName = 'ValorServicos'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrNFSeDPSCabValorDeducoes: TFloatField
      FieldName = 'ValorDeducoes'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrNFSeDPSCabValorPis: TFloatField
      FieldName = 'ValorPis'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrNFSeDPSCabValorCofins: TFloatField
      FieldName = 'ValorCofins'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrNFSeDPSCabValorInss: TFloatField
      FieldName = 'ValorInss'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrNFSeDPSCabValorIr: TFloatField
      FieldName = 'ValorIr'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrNFSeDPSCabValorCsll: TFloatField
      FieldName = 'ValorCsll'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrNFSeDPSCabOutrasRetencoes: TFloatField
      FieldName = 'OutrasRetencoes'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrNFSeDPSCabValorIss: TFloatField
      FieldName = 'ValorIss'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrNFSeDPSCabAliquota: TFloatField
      FieldName = 'Aliquota'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrNFSeDPSCabDescontoIncondicionado: TFloatField
      FieldName = 'DescontoIncondicionado'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrNFSeDPSCabDescontoCondicionado: TFloatField
      FieldName = 'DescontoCondicionado'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrNFSeDPSCabIssRetido: TSmallintField
      FieldName = 'IssRetido'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrNFSeDPSCabResponsavelRetencao: TSmallintField
      FieldName = 'ResponsavelRetencao'
    end
    object QrNFSeDPSCabItemListaServico: TWideStringField
      FieldName = 'ItemListaServico'
      Size = 5
    end
    object QrNFSeDPSCabCodigoCnae: TWideStringField
      FieldName = 'CodigoCnae'
    end
    object QrNFSeDPSCabCodigoTributacaoMunicipio: TWideStringField
      FieldName = 'CodigoTributacaoMunicipio'
    end
    object QrNFSeDPSCabDiscriminacao: TWideMemoField
      FieldName = 'Discriminacao'
      Required = True
      BlobType = ftWideMemo
      Size = 4
    end
    object QrNFSeDPSCabCodigoMunicipio: TIntegerField
      FieldName = 'CodigoMunicipio'
    end
    object QrNFSeDPSCabCodigoPais: TIntegerField
      FieldName = 'CodigoPais'
    end
    object QrNFSeDPSCabExigibilidadeISS: TSmallintField
      FieldName = 'ExigibilidadeISS'
    end
    object QrNFSeDPSCabMunicipioIncidencia: TIntegerField
      FieldName = 'MunicipioIncidencia'
    end
    object QrNFSeDPSCabNumeroProcesso: TWideStringField
      FieldName = 'NumeroProcesso'
      Size = 30
    end
    object QrNFSeDPSCabPrestaCpf: TWideStringField
      FieldName = 'PrestaCpf'
      Size = 18
    end
    object QrNFSeDPSCabPrestaCnpj: TWideStringField
      FieldName = 'PrestaCnpj'
      Size = 18
    end
    object QrNFSeDPSCabPrestaInscricaoMunicipal: TWideStringField
      FieldName = 'PrestaInscricaoMunicipal'
      Size = 18
    end
    object QrNFSeDPSCabTomaCpf: TWideStringField
      FieldName = 'TomaCpf'
      Size = 18
    end
    object QrNFSeDPSCabTomaCnpj: TWideStringField
      FieldName = 'TomaCnpj'
      Size = 18
    end
    object QrNFSeDPSCabTomaInscricaoMunicipal: TWideStringField
      FieldName = 'TomaInscricaoMunicipal'
      Size = 18
    end
    object QrNFSeDPSCabTomaRazaoSocial: TWideStringField
      FieldName = 'TomaRazaoSocial'
      Size = 150
    end
    object QrNFSeDPSCabTomaEndereco: TWideStringField
      FieldName = 'TomaEndereco'
      Size = 125
    end
    object QrNFSeDPSCabTomaNumero: TWideStringField
      FieldName = 'TomaNumero'
      Size = 10
    end
    object QrNFSeDPSCabTomaComplemento: TWideStringField
      FieldName = 'TomaComplemento'
      Size = 60
    end
    object QrNFSeDPSCabTomaBairro: TWideStringField
      FieldName = 'TomaBairro'
      Size = 60
    end
    object QrNFSeDPSCabTomaCodigoMunicipio: TIntegerField
      FieldName = 'TomaCodigoMunicipio'
    end
    object QrNFSeDPSCabTomaUf: TWideStringField
      FieldName = 'TomaUf'
      Size = 125
    end
    object QrNFSeDPSCabTomaCodigoPais: TIntegerField
      FieldName = 'TomaCodigoPais'
    end
    object QrNFSeDPSCabTomaCep: TIntegerField
      FieldName = 'TomaCep'
    end
    object QrNFSeDPSCabIntermeCpf: TWideStringField
      FieldName = 'IntermeCpf'
      Size = 18
    end
    object QrNFSeDPSCabIntermeCnpj: TWideStringField
      FieldName = 'IntermeCnpj'
      Size = 18
    end
    object QrNFSeDPSCabIntermeInscricaoMunicipal: TWideStringField
      FieldName = 'IntermeInscricaoMunicipal'
      Size = 18
    end
    object QrNFSeDPSCabIntermeRazaoSocial: TWideStringField
      FieldName = 'IntermeRazaoSocial'
      Size = 150
    end
    object QrNFSeDPSCabConstrucaoCivilCodigoObra: TWideStringField
      FieldName = 'ConstrucaoCivilCodigoObra'
      Size = 15
    end
    object QrNFSeDPSCabConstrucaoCivilArt: TWideStringField
      FieldName = 'ConstrucaoCivilArt'
      Size = 15
    end
    object QrNFSeDPSCabRegimeEspecialTributacao: TSmallintField
      FieldName = 'RegimeEspecialTributacao'
    end
    object QrNFSeDPSCabOptanteSimplesNacional: TSmallintField
      FieldName = 'OptanteSimplesNacional'
    end
    object QrNFSeDPSCabIncentivoFiscal: TSmallintField
      FieldName = 'IncentivoFiscal'
    end
    object QrNFSeDPSCabId: TWideStringField
      FieldName = 'Id'
      Size = 255
    end
    object QrNFSeDPSCabLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrNFSeDPSCabDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrNFSeDPSCabDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrNFSeDPSCabUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrNFSeDPSCabUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrNFSeDPSCabAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrNFSeDPSCabAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrNFSeDPSCabStatus: TIntegerField
      FieldName = 'Status'
    end
    object QrNFSeDPSCabLastLRpsC: TIntegerField
      FieldName = 'LastLRpsC'
    end
    object QrNFSeDPSCabQuemPagaISS: TSmallintField
      FieldName = 'QuemPagaISS'
    end
    object QrNFSeDPSCabNaturezaOperacao: TIntegerField
      FieldName = 'NaturezaOperacao'
    end
    object QrNFSeDPSCabTomaContatoTelefone: TWideStringField
      FieldName = 'TomaContatoTelefone'
    end
    object QrNFSeDPSCabTomaContatoEmail: TWideStringField
      FieldName = 'TomaContatoEmail'
      Size = 80
    end
    object QrNFSeDPSCabTomaNomeFantasia: TWideStringField
      FieldName = 'TomaNomeFantasia'
      Size = 60
    end
    object QrNFSeDPSCabNFSeFatCab: TIntegerField
      FieldName = 'NFSeFatCab'
    end
    object QrNFSeDPSCabNO_CLI: TWideStringField
      FieldName = 'NO_CLI'
      Size = 100
    end
    object QrNFSeDPSCabNO_INT: TWideStringField
      FieldName = 'NO_INT'
      Size = 100
    end
    object QrNFSeDPSCabNfsNumero: TLargeintField
      FieldName = 'NfsNumero'
    end
    object QrNFSeDPSCabFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrNFSeDPSCabLancto: TIntegerField
      FieldName = 'Lancto'
    end
    object QrNFSeDPSCabGLGerarLct: TSmallintField
      FieldName = 'GLGerarLct'
    end
    object QrNFSeDPSCabGLCarteira: TIntegerField
      FieldName = 'GLCarteira'
    end
    object QrNFSeDPSCabGLConta: TIntegerField
      FieldName = 'GLConta'
    end
    object QrNFSeDPSCabGLConta_TXT: TWideStringField
      FieldName = 'GLConta_TXT'
      Size = 50
    end
  end
  object DsNFSeDPSCab: TDataSource
    DataSet = QrNFSeDPSCab
    Left = 176
    Top = 64
  end
  object QrEmitir: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT dps.*, '
      'IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NO_CLI, '
      'IF(ntm.Tipo=0, ntm.RazaoSocial, ntm.Nome) NO_INT '
      'FROM nfsedpscab dps'
      'LEFT JOIN entidades cli ON cli.Codigo=dps.Cliente '
      'LEFT JOIN entidades ntm ON ntm.Codigo=dps.Intermediario'
      'WHERE RpsIDNumero=0'
      'AND NFSeFatCab=1'
      'ORDER BY Codigo')
    Left = 236
    Top = 64
    object QrEmitirCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEmitirAmbiente: TSmallintField
      FieldName = 'Ambiente'
    end
    object QrEmitirEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrEmitirCliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrEmitirIntermediario: TIntegerField
      FieldName = 'Intermediario'
    end
    object QrEmitirNFSeSrvCad: TIntegerField
      FieldName = 'NFSeSrvCad'
    end
    object QrEmitirRpsID: TWideStringField
      FieldName = 'RpsID'
      Size = 255
    end
    object QrEmitirRpsIDNumero: TIntegerField
      FieldName = 'RpsIDNumero'
    end
    object QrEmitirRpsIDSerie: TWideStringField
      FieldName = 'RpsIDSerie'
      Size = 5
    end
    object QrEmitirRpsIDTipo: TSmallintField
      FieldName = 'RpsIDTipo'
    end
    object QrEmitirRpsDataEmissao: TDateField
      FieldName = 'RpsDataEmissao'
    end
    object QrEmitirRpsHoraEmissao: TTimeField
      FieldName = 'RpsHoraEmissao'
    end
    object QrEmitirRpsStatus: TIntegerField
      FieldName = 'RpsStatus'
    end
    object QrEmitirCompetencia: TDateField
      FieldName = 'Competencia'
    end
    object QrEmitirSubstNumero: TIntegerField
      FieldName = 'SubstNumero'
    end
    object QrEmitirSubstSerie: TWideStringField
      FieldName = 'SubstSerie'
      Size = 5
    end
    object QrEmitirSubstTipo: TSmallintField
      FieldName = 'SubstTipo'
    end
    object QrEmitirValorServicos: TFloatField
      FieldName = 'ValorServicos'
    end
    object QrEmitirValorDeducoes: TFloatField
      FieldName = 'ValorDeducoes'
    end
    object QrEmitirValorPis: TFloatField
      FieldName = 'ValorPis'
    end
    object QrEmitirValorCofins: TFloatField
      FieldName = 'ValorCofins'
    end
    object QrEmitirValorInss: TFloatField
      FieldName = 'ValorInss'
    end
    object QrEmitirValorIr: TFloatField
      FieldName = 'ValorIr'
    end
    object QrEmitirValorCsll: TFloatField
      FieldName = 'ValorCsll'
    end
    object QrEmitirOutrasRetencoes: TFloatField
      FieldName = 'OutrasRetencoes'
    end
    object QrEmitirValorIss: TFloatField
      FieldName = 'ValorIss'
    end
    object QrEmitirAliquota: TFloatField
      FieldName = 'Aliquota'
    end
    object QrEmitirDescontoIncondicionado: TFloatField
      FieldName = 'DescontoIncondicionado'
    end
    object QrEmitirDescontoCondicionado: TFloatField
      FieldName = 'DescontoCondicionado'
    end
    object QrEmitirIssRetido: TSmallintField
      FieldName = 'IssRetido'
    end
    object QrEmitirResponsavelRetencao: TSmallintField
      FieldName = 'ResponsavelRetencao'
    end
    object QrEmitirItemListaServico: TWideStringField
      FieldName = 'ItemListaServico'
      Size = 5
    end
    object QrEmitirCodigoCnae: TWideStringField
      FieldName = 'CodigoCnae'
    end
    object QrEmitirCodigoTributacaoMunicipio: TWideStringField
      FieldName = 'CodigoTributacaoMunicipio'
    end
    object QrEmitirDiscriminacao: TWideMemoField
      FieldName = 'Discriminacao'
      Required = True
      BlobType = ftWideMemo
      Size = 4
    end
    object QrEmitirCodigoMunicipio: TIntegerField
      FieldName = 'CodigoMunicipio'
    end
    object QrEmitirCodigoPais: TIntegerField
      FieldName = 'CodigoPais'
    end
    object QrEmitirExigibilidadeISS: TSmallintField
      FieldName = 'ExigibilidadeISS'
    end
    object QrEmitirMunicipioIncidencia: TIntegerField
      FieldName = 'MunicipioIncidencia'
    end
    object QrEmitirNumeroProcesso: TWideStringField
      FieldName = 'NumeroProcesso'
      Size = 30
    end
    object QrEmitirPrestaCpf: TWideStringField
      FieldName = 'PrestaCpf'
      Size = 18
    end
    object QrEmitirPrestaCnpj: TWideStringField
      FieldName = 'PrestaCnpj'
      Size = 18
    end
    object QrEmitirPrestaInscricaoMunicipal: TWideStringField
      FieldName = 'PrestaInscricaoMunicipal'
      Size = 18
    end
    object QrEmitirTomaCpf: TWideStringField
      FieldName = 'TomaCpf'
      Size = 18
    end
    object QrEmitirTomaCnpj: TWideStringField
      FieldName = 'TomaCnpj'
      Size = 18
    end
    object QrEmitirTomaInscricaoMunicipal: TWideStringField
      FieldName = 'TomaInscricaoMunicipal'
      Size = 18
    end
    object QrEmitirTomaRazaoSocial: TWideStringField
      FieldName = 'TomaRazaoSocial'
      Size = 150
    end
    object QrEmitirTomaEndereco: TWideStringField
      FieldName = 'TomaEndereco'
      Size = 125
    end
    object QrEmitirTomaNumero: TWideStringField
      FieldName = 'TomaNumero'
      Size = 10
    end
    object QrEmitirTomaComplemento: TWideStringField
      FieldName = 'TomaComplemento'
      Size = 60
    end
    object QrEmitirTomaBairro: TWideStringField
      FieldName = 'TomaBairro'
      Size = 60
    end
    object QrEmitirTomaCodigoMunicipio: TIntegerField
      FieldName = 'TomaCodigoMunicipio'
    end
    object QrEmitirTomaUf: TWideStringField
      FieldName = 'TomaUf'
      Size = 125
    end
    object QrEmitirTomaCodigoPais: TIntegerField
      FieldName = 'TomaCodigoPais'
    end
    object QrEmitirTomaCep: TIntegerField
      FieldName = 'TomaCep'
    end
    object QrEmitirIntermeCpf: TWideStringField
      FieldName = 'IntermeCpf'
      Size = 18
    end
    object QrEmitirIntermeCnpj: TWideStringField
      FieldName = 'IntermeCnpj'
      Size = 18
    end
    object QrEmitirIntermeInscricaoMunicipal: TWideStringField
      FieldName = 'IntermeInscricaoMunicipal'
      Size = 18
    end
    object QrEmitirIntermeRazaoSocial: TWideStringField
      FieldName = 'IntermeRazaoSocial'
      Size = 150
    end
    object QrEmitirConstrucaoCivilCodigoObra: TWideStringField
      FieldName = 'ConstrucaoCivilCodigoObra'
      Size = 15
    end
    object QrEmitirConstrucaoCivilArt: TWideStringField
      FieldName = 'ConstrucaoCivilArt'
      Size = 15
    end
    object QrEmitirRegimeEspecialTributacao: TSmallintField
      FieldName = 'RegimeEspecialTributacao'
    end
    object QrEmitirOptanteSimplesNacional: TSmallintField
      FieldName = 'OptanteSimplesNacional'
    end
    object QrEmitirIncentivoFiscal: TSmallintField
      FieldName = 'IncentivoFiscal'
    end
    object QrEmitirId: TWideStringField
      FieldName = 'Id'
      Size = 255
    end
    object QrEmitirLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrEmitirDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrEmitirDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrEmitirUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrEmitirUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrEmitirAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrEmitirAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrEmitirStatus: TIntegerField
      FieldName = 'Status'
    end
    object QrEmitirLastLRpsC: TIntegerField
      FieldName = 'LastLRpsC'
    end
    object QrEmitirQuemPagaISS: TSmallintField
      FieldName = 'QuemPagaISS'
    end
    object QrEmitirNaturezaOperacao: TIntegerField
      FieldName = 'NaturezaOperacao'
    end
    object QrEmitirTomaContatoTelefone: TWideStringField
      FieldName = 'TomaContatoTelefone'
    end
    object QrEmitirTomaContatoEmail: TWideStringField
      FieldName = 'TomaContatoEmail'
      Size = 80
    end
    object QrEmitirTomaNomeFantasia: TWideStringField
      FieldName = 'TomaNomeFantasia'
      Size = 60
    end
    object QrEmitirNFSeFatCab: TIntegerField
      FieldName = 'NFSeFatCab'
    end
    object QrEmitirNO_CLI: TWideStringField
      FieldName = 'NO_CLI'
      Size = 100
    end
    object QrEmitirNO_INT: TWideStringField
      FieldName = 'NO_INT'
      Size = 100
    end
  end
  object PMImprime: TPopupMenu
    Left = 16
    Top = 28
    object odasNFSeemitidas1: TMenuItem
      Caption = '&Todas NFS-e emitidas'
      OnClick = odasNFSeemitidas1Click
    end
    object NFSedaDPSatual1: TMenuItem
      Caption = 'NFS-e da DPS atual'
      OnClick = NFSedaDPSatual1Click
    end
    object NFSedasDPSSelecionadas1: TMenuItem
      Caption = 'NFS-e das DPS Selecionadas'
      OnClick = NFSedasDPSSelecionadas1Click
    end
  end
  object QrNFSeCodVerifi: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT NfsCodigoVerificacao'
      'FROM nfsenfscab'
      'WHERE Empresa=:P0'
      'AND Ambiente=:P1'
      'AND NfsNumero=:P2'
      'AND NfsRpsIDSerie=:P3')
    Left = 624
    Top = 12
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
    object QrNFSeCodVerifiNfsCodigoVerificacao: TWideStringField
      FieldName = 'NfsCodigoVerificacao'
      Size = 9
    end
  end
  object CSTabSheetChamou: TdmkCompoStore
    Left = 16
    Top = 16
  end
end
