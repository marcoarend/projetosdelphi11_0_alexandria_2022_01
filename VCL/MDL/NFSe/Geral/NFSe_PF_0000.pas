unit NFSe_PF_0000;

interface

uses
  Forms, StdCtrls, ComCtrls, ExtCtrls, Windows, Messages, SysUtils, Classes,
  Graphics, Controls, Dialogs, Variants, MaskUtils, UnMsgInt, dmkGeral,
  (*Certificado*)CAPICOM_TLB,(*20230320 JwaWinCrypt,*) WinInet, SOAPConst,
  (*HTTP*)SOAPHTTPTrans, UnDmkProcFunc, UnInternalConsts, NFSe_TbTerc,
  UnDMkEnums, UnProjGroup_Consts;

  //Buttons, UnMLAGeral, Math,
  //IBCustomDataSet, IBQuery, Clipbrd, DB, DBClient, UrlMon,
  //InvokeRegistry,
  //SOAPHTTPClient,
  //WinInet, XMLDoc, xmldom, XMLIntf, msxmldom;

const
  tsStatusRps_Normal = 1;
  tsStatusRps_Cnacelado = 2;

type
  TtsStatusRps = (tssrZero, tssrNormal, tssrCancelado);
  //
  TtsNumeroNfse = Int64;
  TtsCodigoVerificacao = String[9];
  TtstatusRps = (csrpsZero, csrpsNormal, csrpsCancelado);
  TtstatusNfse = (csnfsZero, csnfsNormal, csnfsCancelado);

  TtsNumeroLote = Int64;
  TtsNumeroProtocolo = String[50];
  TtsOutrasInformacoes = String[255];
  //
  TNFSe_PF_0000 = class(TObject)
  private
    {private declaration}
  public
    {public declaration}
    // Usa mesmo sem emiss�o de NFSe
    procedure MostraFormListServ(Codigo: Integer = 0);

{$IfNDef sNFSe}
    procedure MostraFormNFSeSrvCad(Codigo: Integer = 0);
    procedure MostraFormNFSeParams();
    procedure MostraFormCNAE21Cad(Codigo: Integer = 0);
    procedure MostraFormTsErrosAlertasCab();
    procedure MostraFormNFSeLRpsC_0201(Lote: Integer);
    procedure MostraFormNFSe_RPSPesq(AbrirEmAba: Boolean; InOwner: TWincontrol;
                Pager: TWinControl);
    procedure MostraFormNFSeMail(AbrirEmAba: Boolean; InOwner: TWincontrol;
                Pager: TWinControl; CliInt, NFSeFatCab,
                NFSeDPSCabCod: Integer; Avulso: Boolean);
    procedure MostraFormNFSe_NFSePesq(AbrirEmAba: Boolean;
                InOwner: TWincontrol; Pager: TWinControl);
    procedure MostraFormNFSeMenCab(AbrirEmAba: Boolean;
                InOwner: TWincontrol; Pager: TWinControl);
    procedure MostraFormNFSeFatCab(AbrirEmAba: Boolean; InOwner: TWincontrol;
                Pager: TWinControl; Codigo: Integer = 0);
    //
    function  InsUpdDeclaracaoPrestacaoServico(SQLType: TSQLType;
              const NFSeFatCab: Integer;
              var RpsDataEmissao, Competencia: TDateTime;
              var(*RpsID,*) RpsIDSerie, SubstSerie,
              ItemListaServico, CodigoCnae, CodigoTributacaoMunicipio,
              Discriminacao, NumeroProcesso, (*PrestaCpf, PrestaCnpj,
              PrestaInscricaoMunicipal, TomaCpf, TomaCnpj,
              TomaInscricaoMunicipal, TomaRazaoSocial, TomaEndereco, TomaNumero,
              TomaComplemento, TomaBairro, TomaUf, IntermeCpf, IntermeCnpj,
              IntermeInscricaoMunicipal, IntermeRazaoSocial,*)
              ConstrucaoCivilCodigoObra, ConstrucaoCivilArt, Id,
              RpsHoraEmissao: String;
              var CodAtual, Empresa, Cliente, Intermediario, RpsIDNumero,
              RpsIDTipo, RpsStatus, SubstNumero, SubstTipo, IssRetido, ResponsavelRetencao,
              (*CodigoMunicipio, CodigoPais,*) ExigibilidadeISS,
              QuemPagaIss, MunicipioIncidencia_, (*TomaCodigoMunicipio, TomaCodigoPais, TomaCep,*)
              RegimeEspecialTributacao, OptanteSimplesNacional, IncentivoFiscal,
              NFSeSrvCad: Integer;
              var ValorServicos, ValorDeducoes, ValorPis, ValorCofins, ValorInss,
              ValorIr, ValorCsll, OutrasRetencoes, ValorIss, Aliquota,
              DescontoIncondicionado, DescontoCondicionado: Double;
              NfseMenCab: Integer;
              // INICIO LEI N� 12.741 - Lei da Transparencia
              iTotTrib: Integer; vTotTrib, vBasTrib, pTotTrib: Double; fontTotTrib:
              TIBPTaxFont; tabTotTrib: TIBPTaxTabs; verTotTrib: String;
              tpAliqTotTrib: TIBPTaxOrig): Integer;
    //function  ObtemProximoDPS(Empresa: Integer): Integer;
    function  ObtemProximaChaveRPS(const Empresa, Ambiente: Integer; const Tipo:
              Integer; const Serie: String; var Numero, NumHom: Integer): Integer;
    function  ObtemProximoLoteRPS(Empresa, Ambiente: Integer): Integer;
    function  DefineDadosPrestador(const Entidade: Integer;
              var CPF, CNPJ, IM: String; var Municipio, Pais: Integer): Boolean;
    function  DefineDadosTomador(const Entidade: Integer; var CPF, CNPJ, IM,
              Nome, Endereco, Numero, Compl, Bairro, UF, Telefone: String;
              var Municipio, Pais, Cep: Integer): Boolean;
    function  DefineDadosIntermediario(const Entidade: Integer;
              var CPF, CNPJ, IM, Nome: String): Boolean;
    function  FIC_PS(FaltaInfo: Boolean; Msg: String): Boolean;
    function  VerificaDocumento(EntiTipo: String; const Tipo: Integer; const
              CNPJ_BD, CPF_BD: String; var CNPJ_NF, CPF_NF: String): Boolean;
    //
    function  WS_NFeRecepcaoLote(UFServico: String; Ambiente,
              CodigoUF: Byte; NumeroSerial: String; Lote: Integer; Label1: TLabel;
              RETxtEnvio: TMemo; EdWS: TEdit): WideString;
{
    function  Executar(const CNPJ, Senha, VersaoLayOut, VersaoDados: String;
              const DadosMsg: WideString; var Retorno: WideString): Boolean;
    procedure OnBeforePost(const HTTPReqResp: THTTPReqResp;
              Data: Pointer);
    procedure OnBeforePost2(const HTTPReqResp: THTTPReqResp;
              Data: Pointer);
}
    function  FormataRPSNumero(RPS: Integer): String;
    function  TextoStatusNFSe(Codigo: Integer): String;
    function  InsereLancto(Vencto: TDateTime; NfsNumero: Int64; Valor: Double;
              Empresa, Entidade, Periodo, CartEmiss, Genero,
              NfseDpsCab: Integer; IDSerie, Descri, TabLctA: String): Integer;
    function  ObtemUrlNfse(IdMunicipio: Integer): String;
    function  MontaUrlNfse(IdMunicipio: Integer; Url, CNPJ: String;
              NumNfse: Integer; CodVal: String): String;
    procedure MostraFormNfse_ConfReceb(Form: TForm; Id_Cliente, Ambiente,
              RpsIDNumero: Integer; RpsIDSerie: String);
{$EndIf}
  end;

var
  UnNFSe_PF_0000: TNFSe_PF_0000;

const
  verGeraNFSe_Versao          = '2.01'; // Gera NFS-e
  INTERNET_OPTION_CLIENT_CERT_CONTEXT = 84;

implementation

uses MyDBCheck, CfgCadLista, Module, CNAE21Cad,
{$IfNDef sNFSe}NFSeSrvCad, TsErrosAlertasCab,

  UnMyObjects, DmkDAC_PF, UMySQLModule, ModuleGeral,
  (*NFSe_Steps_0201,*) NFSeLRpsC_0201, NFSe_RPSPesq, NFSe_NFSPesq, MyListas,
  {$IfNDef SemProtocolo} NFSeMail, {$EndIf}
  {$IfNDef NO_FINANCEIRO} UnFinanceiro, {$EndIf}
  NFSE_0000_Module, NFSeMenCab, NFSeFatCab, NFSeParams, UnGrl_Geral,
  NFSe_ConfReceb, UnGrlUsuarios,
{$EndIf}
 ListServ;

{ TNFSe_PF }

procedure TNFSe_PF_0000.MostraFormListServ(Codigo: Integer = 0);
begin
  if DBCheck.CriaFm(TFmListServ, FmListServ, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
      FmListServ.LocCod(Codigo, Codigo);
    FmListServ.ShowModal;
    FmListServ.Destroy;
  end;
end;


{$IfNDef sNFSe}

procedure TNFSe_PF_0000.MostraFormNFSeSrvCad(Codigo: Integer = 0);
begin
  if DBCheck.CriaFm(TFmNFSeSrvCad, FmNFSeSrvCad, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
      FmNFSeSrvCad.LocCod(Codigo, Codigo);
    FmNFSeSrvCad.ShowModal;
    FmNFSeSrvCad.Destroy;
  end;
end;

procedure TNFSe_PF_0000.MostraFormNfse_ConfReceb(Form: TForm; Id_Cliente, Ambiente,
              RpsIDNumero: Integer; RpsIDSerie: String);
begin
  if GrlUsuarios.Efetuar_Login(Form, stDesktop, Dmod.MyDB, Dmod.QrUpd,
    Dmod.QrAux, VAR_USUARIO) then
  begin
    if DBCheck.CriaFm(TFmNFSe_ConfReceb, FmNFSe_ConfReceb, afmoNegarComAviso) then
    begin
      FmNFSe_ConfReceb.FId_Cliente := Id_Cliente;
      FmNFSe_ConfReceb.FAmbiente   := Ambiente;
      FmNFSe_ConfReceb.FNumero_NF  := RpsIDNumero;
      FmNFSe_ConfReceb.FSerie_NF   := RpsIDSerie;
      FmNFSe_ConfReceb.ShowModal;
      FmNFSe_ConfReceb.Destroy;
    end;
  end;
end;

function TNFSe_PF_0000.DefineDadosIntermediario(const Entidade: Integer;
  var CPF, CNPJ, IM, Nome: String): Boolean;
begin
  if Entidade <> 0 then
  begin
    DModG.ReopenEntNFS(Entidade);
    Result := VerificaDocumento('tomador',  DModG.QrEntNFSTipo.Value,
    DModG.QrEntNFSCNPJ.Value, DModG.QrEntNFSCPF.Value, CNPJ, CPF);
    IM := Geral.SoNumero_TT(DModG.QrEntNFSNIRE.Value);
    Nome := DModG.QrEntNFSNO_ENT.Value;
  end else
  begin
    Result := True;
    CNPJ := '';
    CPF := '';
    IM := '';
    Nome := '';
  end;
end;

function TNFSe_PF_0000.DefineDadosPrestador(const Entidade: Integer;
  var CPF, CNPJ, IM: String; var Municipio, Pais: Integer): Boolean;
begin
  DModG.ReopenEntNFS(Entidade);
  Result := VerificaDocumento('prestador',  DModG.QrEntNFSTipo.Value,
  DModG.QrEntNFSCNPJ.Value, DModG.QrEntNFSCPF.Value, CNPJ, CPF);
  IM := Geral.SoNumero_TT(DModG.QrEntNFSNIRE.Value);
  //
  Municipio := Trunc(DModG.QrEntNFSCodMunici.Value);
  Pais := Trunc(DModG.QrEntNFSCodiPais.Value);
end;

function TNFSe_PF_0000.DefineDadosTomador(const Entidade: Integer; var CPF,
  CNPJ, IM, Nome, Endereco, Numero, Compl, Bairro, UF, Telefone: String;
  var Municipio, Pais, Cep: Integer): Boolean;
begin
  DModG.ReopenEntNFS(Entidade);
  Result := VerificaDocumento('tomador',  DModG.QrEntNFSTipo.Value,
  DModG.QrEntNFSCNPJ.Value, DModG.QrEntNFSCPF.Value, CNPJ, CPF);
  IM := Geral.SoNumero_TT(DModG.QrEntNFSNIRE.Value);
  Nome := DModG.QrEntNFSNO_ENT.Value;
  Endereco := Trim(DModG.QrEntNFSNO_Lograd.Value + ' ' + DModG.QrEntNFSRua.Value);
  Numero := Geral.FF0(Trunc(DModG.QrEntNFSNumero.Value));
  Compl := DModG.QrEntNFSCompl.Value;
  Bairro := DModG.QrEntNFSBairro.Value;
  Municipio := Trunc(DModG.QrEntNFSCodMunici.Value);
  UF := Geral.GetSiglaUF_do_CodigoUF(Trunc(DModG.QrEntNFSUF_Cod.Value));
  Pais := Trunc(DModG.QrEntNFSCodiPais.Value);
  Cep := Trunc(DModG.QrEntNFSCEP.Value);
  //
  Telefone := DModG.QrEntNFSTE1.Value;
end;

{
function TNFSe_PF_0000.Executar(const CNPJ, Senha, VersaoLayOut, VersaoDados: String;
  const DadosMsg: WideString; var Retorno: WideString): Boolean;
  //
  function DadosSenha(CNPJ, Senha: String): AnsiString;
  begin
   Result := '<wsse:Security S:mustUnderstand="1"' +
                 ' xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd"' +
                 ' xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd">' +
              '<wsse:UsernameToken wsu:Id="UsernameToken-18">' +
               '<wsse:Username>' +
                 CNPJ +
               '</wsse:Username>' +
               '<wsse:Password Type="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText">' +
                Senha +
               '</wsse:Password>' +
              '</wsse:UsernameToken>' +
             '</wsse:Security>';
  end;
  function CabMsg(Prefixo2, VersaoLayOut, VersaoDados,
    NameSpaceCab: String): AnsiString;
  begin
   Result := '<' + Prefixo2 + 'cabecalho' +
              ' versao="'  + VersaoLayOut + '"' +
              ' xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"' +
              ' xmlns:xsd="http://www.w3.org/2001/XMLSchema"' + NameSpaceCab +
              '<versaoDados>' + VersaoDados + '</versaoDados>'+
             '</' + Prefixo2 + 'cabecalho>';
  end;


var
 aMsg        : String;
 Texto       : String;
 Acao        : TStringList;
 Stream      : TMemoryStream;
 StrStream   : TStringStream;

 ReqResp : THTTPReqResp;

 Prefixo3      : String;
 Prefixo4      : String;
 FRetListaNfse : AnsiString;
 FRetNfse      : AnsiString;
 i, j, k, p, ii: Integer;
 PathSalvar    : String;
 //
  FRetornoWS: AnsiString;
  FRetWS: AnsiString;

const
  //SoapAction = 'http://nfse.abrasf.org.br/Infse/GerarNfse';
  //SoapAction = 'https://isseteste.maringa.pr.gov.br/ws/#GerarNfse';
  SoapAction = 'https://isseteste.maringa.pr.gov.br/ws/#GerarNfse';
  //FURL = 'https://isseteste.maringa.pr.gov.br/ws';
  FURL = 'https://isseteste.maringa.pr.gov.br/ws/';
begin
  Retorno := '';
  Geral.MensagemBox(DadosMsg, 'XML', MB_OK+MB_ICONINFORMATION);
  Texto := DadosMsg;

 Acao      := TStringList.Create;
 Stream    := TMemoryStream.Create;
 Acao.Text := Texto;

 ReqResp := THTTPReqResp.Create(nil);
 ReqResp.OnBeforePost := OnBeforePost;

 // Proxy
 //ConfiguraReqResp( ReqResp );

 ReqResp.URL := FURL;
 ReqResp.UseUTF8InHeader := True;

 //ReqResp.SoapAction := FProvedorClass.GetSoapAction(acGerar, FNomeCidade);
 ReqResp.SoapAction := SoapAction;

 try
  if FmNFSe_Steps_0201.CkMeuXML.Checked then
    Acao.Text := FmNFSe_Steps_0201.RETxtEnvioNFSe.Text
  else
    FmNFSe_Steps_0201.RETxtEnvioNFSe.Text := Acao.Text;
  ReqResp.Execute(Acao.Text, Stream);
  StrStream := TStringStream.Create('');
  StrStream.CopyFrom(Stream, 0);

  //FRetornoWS := TiraAcentos(ParseText(StrStream.DataString, True));
  FRetornoWS := StrStream.DataString;
  //FRetWS     := FProvedorClass.GetRetornoWS(acGerar, FRetornoWS);
  FRetWS     := FRetornoWS;
  StrStream.Free;


  Geral.MensagemBox(FRetWS, 'XML Retorno', MB_OK+MB_ICONWARNING);
  //Result := (FMsg = '');
  Retorno := FRetWS;
  Result := Retorno <> '';
 finally
  ReqResp.Free;
  Acao.Free;
  Stream.Free;

 end;
end;
}

function TNFSe_PF_0000.FIC_PS(FaltaInfo: Boolean; Msg: String): Boolean;
begin
  Result := MyObjects.FIC(FaltaInfo, nil,
  'NFSe_PF_0000.InsUpdDeclaracaoPrestacaoServico' + sLineBreak + Msg);
end;

function TNFSe_PF_0000.FormataRPSNumero(RPS: Integer): String;
begin
  Result := Geral.FFN(RPS, 8);
end;

function TNFSe_PF_0000.InsUpdDeclaracaoPrestacaoServico(
  SQLType: TSQLType;
  const NFSeFatCab: Integer;
  var RpsDataEmissao, Competencia: TDateTime;
  var(*RpsID,*) RpsIDSerie, SubstSerie,
  ItemListaServico, CodigoCnae, CodigoTributacaoMunicipio,
  Discriminacao, NumeroProcesso, (*PrestaCpf, PrestaCnpj,
  PrestaInscricaoMunicipal, TomaCpf, TomaCnpj,
  TomaInscricaoMunicipal, TomaRazaoSocial, TomaEndereco, TomaNumero,
  TomaComplemento, TomaBairro, TomaUf, IntermeCpf, IntermeCnpj,
  IntermeInscricaoMunicipal, IntermeRazaoSocial,*)
  ConstrucaoCivilCodigoObra, ConstrucaoCivilArt, Id,
  RpsHoraEmissao: String;
  var CodAtual, Empresa, Cliente, Intermediario, RpsIDNumero,
  RpsIDTipo, RpsStatus, SubstNumero, SubstTipo, IssRetido, ResponsavelRetencao,
  (*CodigoMunicipio, CodigoPais,*) ExigibilidadeISS,
  QuemPagaIss, MunicipioIncidencia_, (*TomaCodigoMunicipio, TomaCodigoPais, TomaCep,*)
  RegimeEspecialTributacao, OptanteSimplesNacional, IncentivoFiscal,
  NFSeSrvCad: Integer;
  var ValorServicos, ValorDeducoes, ValorPis, ValorCofins, ValorInss,
  ValorIr, ValorCsll, OutrasRetencoes, ValorIss, Aliquota,
  DescontoIncondicionado, DescontoCondicionado: Double;
  NfseMenCab: Integer;
  // INICIO LEI N� 12.741 - Lei da Transparencia
  iTotTrib: Integer;
  vTotTrib, vBasTrib, pTotTrib: Double;
  fontTotTrib: TIBPTaxFont;
  tabTotTrib: TIBPTaxTabs; verTotTrib: String;
  tpAliqTotTrib: TIBPTaxOrig): Integer;
  // FIM LEI N� 12.741 - Lei da Transparencia
var
  Codigo, TipoPresta, TipoToma, TipoIntermed, MunicipioIncidencia: Integer;
  //
  RpsID, PrestaCpf, PrestaCnpj,
  PrestaInscricaoMunicipal, TomaCpf, TomaCnpj,
  TomaInscricaoMunicipal, TomaRazaoSocial, TomaEndereco, TomaNumero,
  TomaComplemento, TomaBairro, TomaUf, TomaContatoTelefone, TomaContatoEmail,
  IntermeCpf, IntermeCnpj, IntermeInscricaoMunicipal, IntermeRazaoSocial: String;
  //
  CodigoMunicipio, CodigoPais, TomaCodigoMunicipio,
  TomaCodigoPais, TomaCep, Ambiente: Integer;
begin
  Result := 0;
  //
  Id := '';
  //
  if FIC_PS(Empresa=0, 'Empresa n�o definida!') then Exit;
  DModG.ReopenParamsEmp(Empresa);
  //
  if not DefineDadosPrestador(Empresa, PrestaCpf, PrestaCnpj,
    PrestaInscricaoMunicipal, CodigoMunicipio, CodigoPais) then Exit;

  if not DefineDadosTomador(Cliente, TomaCpf, TomaCnpj,
    TomaInscricaoMunicipal, TomaRazaoSocial, TomaEndereco, TomaNumero,
    TomaComplemento, TomaBairro, TomaUf, TomaContatoTelefone, TomaCodigoMunicipio,
    TomaCodigoPais, TomaCep) then Exit;
    TomaContatoEmail := DModG.ObtemPrimeiroEMail_NFSe(Empresa, Cliente);

  if not DefineDadosIntermediario(Intermediario, IntermeCpf, IntermeCnpj,
    IntermeInscricaoMunicipal, IntermeRazaoSocial) then Exit;

  if SQLtype = stIns then
  begin

////////////////////////////////////////////////////////////////////////////////
  { N�o fazer aqui! pode dar confus�o se n�o for tranformada em nfse
    RpsIDNumero := ObtemProximoDPS(Empresa);
    RpsIDSerie  := DModG.QrParamsEmpNFSeSerieRps.Value;
  }
    RpsIDNumero := 0;  // 2023-12-22
    RpsIDSerie  := ''; // 2023-12-22
////////////////////////////////////////////////////////////////////////////////

  end;
  Ambiente      := DModG.QrParamsEmpNFSeAmbiente.Value;

  if FIC_PS(Ambiente=0, 'Ambiente inv�lido!') then Exit;
  // Empresa, Cliente, Intermediario,
  if FIC_PS(NFSeSrvCad=0, 'Regra Fiscal n�o definida!') then Exit;
  // NFSeSrvCad, RpsID, RpsIDNumero,
  //if FIC_PS(RpsIDSerie='', 'S�rie da NFS-e n�o definida!') then Exit;
  //if FIC_PS(RpsIDTipo=0, 'Tipo de RPS n�o definido!') then Exit;
  // RpsDataEmissao, RpsHoraEmissao,
  if FIC_PS(RpsStatus=0, 'Status do RPS n�o definido!') then Exit;
  // Competencia, SubstNumero, SubstSerie, SubstTipo,
  // ValorServicos, ValorDeducoes, ValorPis, ValorCofins, ValorInss,
  // ValorIr, ValorCsll, OutrasRetencoes, ValorIss, Aliquota, DescontoIncondicionado,
  // DescontoCondicionado, IssRetido, ResponsavelRetencao,
  if FIC_PS(ItemListaServico='', 'C�digo Servi�o LC 116/03 n�o definido!') then Exit;
  //CodigoCnae, CodigoTributacaoMunicipio,
  if FIC_PS(Trim(Discriminacao)='', 'Discrimina��o do servi�o n�o informada!') then Exit;
  if FIC_PS(CodigoMunicipio=0, 'C�digo do munic�pio (servi�o) n�o informado!') then Exit;
  if FIC_PS(CodigoPais=0, 'C�digo do pa�s (servi�o) n�o informado!') then Exit;
  if FIC_PS(ExigibilidadeISS=0, 'Exigibilidade do ISS n�o definido!') then Exit;
  //MunicipioIncidencia, NumeroProcesso,
  if FIC_PS(Trim(PrestaCpf + PrestaCnpj)='', 'CNPJ/CPF do prestador n�o definido!') then Exit;
  if FIC_PS(PrestaInscricaoMunicipal='', 'Inscri��o Municipal n�o definido!') then Exit;
  if FIC_PS(Trim(TomaCpf + TomaCnpj)='', 'CNPJ/CPF do tomador n�o definido!') then Exit;
  //if FIC_PS(TomaInscricaoMunicipal='', 'Inscri��o Municipal do tomador n�o definido!') then Exit;
  if FIC_PS(TomaRazaoSocial='', 'Raz�o Social do tomador n�o definido!') then Exit;
  //TomaEndereco, TomaNumero, TomaComplemento, TomaBairro,
  if FIC_PS(TomaCodigoMunicipio = 0, 'C�digo do munic�pio do tomador do servi�o n�o informado!') then Exit;
  //TomaUf, TomaCodigoPais, TomaCep, TomaContatoTelefone, TomaContatoEmail,
  //IntermeCpf, IntermeCnpj, IntermeInscricaoMunicipal, IntermeRazaoSocial,
  // ConstrucaoCivilCodigoObra, ConstrucaoCivilArt,
  //RegimeEspecialTributacao, OptanteSimplesNacional, IncentivoFiscal, Id

{ 2013-06-20
  Codigo := UMyMod.BPGS1I32('nfsedpscab', 'Codigo', '', '',
    tsPos, SQLType, CodAtual);
}
  Codigo := UMyMod.BPGS1I32('nfsedpscab', 'Codigo', '', '',
    tsPos, SQLType, CodAtual);
// FIM 2013-06-20

  RpsID := Geral.FF0(Codigo);
  if MunicipioIncidencia_ = -1 then
  begin
    case QuemPagaIss of
      NFE_CO_PRESTADOR: MunicipioIncidencia := CodigoMunicipio;
      NFE_CO_TOMADOR: MunicipioIncidencia := TomaCodigoMunicipio;
      else
      begin
        { Ver o que fazer!
        NFE_CO_INTERMEDIARIO:
        NFE_CO_ADEFINIR = 9;
        }
        MunicipioIncidencia := 0;
      end;
    end;
  end else
    MunicipioIncidencia := MunicipioIncidencia_;

  if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'nfsedpscab', False, [
  'NFSeFatCab',
  'Ambiente', 'Empresa', 'Cliente',
  'Intermediario', 'NFSeSrvCad', 'RpsID',
  'RpsIDNumero', 'RpsIDSerie', 'RpsIDTipo',
  'RpsDataEmissao', 'RpsHoraEmissao', 'RpsStatus',
  'Competencia', 'SubstNumero', 'SubstSerie',
  'SubstTipo', 'ValorServicos', 'ValorDeducoes',
  'ValorPis', 'ValorCofins', 'ValorInss',
  'ValorIr', 'ValorCsll', 'OutrasRetencoes',
  'ValorIss', 'Aliquota', 'DescontoIncondicionado',
  'DescontoCondicionado', 'IssRetido', 'ResponsavelRetencao',
  'ItemListaServico', 'CodigoCnae', 'CodigoTributacaoMunicipio',
  'Discriminacao', 'CodigoMunicipio', 'CodigoPais',
  'ExigibilidadeISS', 'QuemPagaISS', 'MunicipioIncidencia', 'NumeroProcesso',
  'PrestaCpf', 'PrestaCnpj', 'PrestaInscricaoMunicipal',
  'TomaCpf', 'TomaCnpj', 'TomaInscricaoMunicipal',
  'TomaRazaoSocial', 'TomaEndereco', 'TomaNumero',
  'TomaComplemento', 'TomaBairro', 'TomaCodigoMunicipio',
  'TomaUf', 'TomaCodigoPais', 'TomaCep',
  'TomaContatoTelefone', 'TomaContatoEmail', 'IntermeCpf',
  'IntermeCnpj', 'IntermeInscricaoMunicipal', 'IntermeRazaoSocial',
  'ConstrucaoCivilCodigoObra', 'ConstrucaoCivilArt', 'RegimeEspecialTributacao',
  'OptanteSimplesNacional', 'IncentivoFiscal', 'NfseMenCab',
  // INICIO LEI N� 12.741 - Lei da Transparencia
  'iTotTrib', 'vTotTrib',
  'vBasTrib', 'pTotTrib', 'fontTotTrib',
  'tabTotTrib', 'verTotTrib', 'tpAliqTotTrib',
  // FIM LEI N� 12.741 - Lei da Transparencia
  'Id'], [
  'Codigo'], [
  NFSeFatCab,
  Ambiente, Empresa, Cliente,
  Intermediario, NFSeSrvCad, RpsID,
  RpsIDNumero, RpsIDSerie, RpsIDTipo,
  RpsDataEmissao, RpsHoraEmissao, RpsStatus,
  Competencia, SubstNumero, SubstSerie,
  SubstTipo, ValorServicos, ValorDeducoes,
  ValorPis, ValorCofins, ValorInss,
  ValorIr, ValorCsll, OutrasRetencoes,
  ValorIss, Aliquota, DescontoIncondicionado,
  DescontoCondicionado, IssRetido, ResponsavelRetencao,
  ItemListaServico, CodigoCnae, CodigoTributacaoMunicipio,
  Discriminacao, CodigoMunicipio, CodigoPais,
  ExigibilidadeISS, QuemPagaISS, MunicipioIncidencia, NumeroProcesso,
  PrestaCpf, PrestaCnpj, PrestaInscricaoMunicipal,
  TomaCpf, TomaCnpj, TomaInscricaoMunicipal,
  TomaRazaoSocial, TomaEndereco, TomaNumero,
  TomaComplemento, TomaBairro, TomaCodigoMunicipio,
  TomaUf, TomaCodigoPais, TomaCep,
  TomaContatoTelefone, TomaContatoEmail, IntermeCpf,
  IntermeCnpj, IntermeInscricaoMunicipal, IntermeRazaoSocial,
  ConstrucaoCivilCodigoObra, ConstrucaoCivilArt, RegimeEspecialTributacao,
  OptanteSimplesNacional, IncentivoFiscal, NfseMenCab,
  // INICIO LEI N� 12.741 - Lei da Transparencia
  iTotTrib, vTotTrib,
  vBasTrib, pTotTrib, fontTotTrib,
  tabTotTrib, verTotTrib, tpAliqTotTrib,
  // FIM LEI N� 12.741 - Lei da Transparencia
  Id], [
  Codigo], True) then
  begin
    Result := Codigo;
  end;
end;

function TNFSe_PF_0000.InsereLancto(Vencto: TDateTime; NfsNumero: Int64;
  Valor: Double; Empresa, Entidade, Periodo, CartEmiss, Genero,
  NfseDpsCab: Integer; IDSerie, Descri, TabLctA: String): Integer;
{$IfNDef NO_FINANCEIRO}
var
  Res: Boolean;
begin
  Result := 0;
  //
  UFinanceiro.LancamentoDefaultVARS;
  //
  FLAN_Data       := Geral.FDT(Date, 1);
  FLAN_Tipo       := 2;
  FLAN_SerieNF    := IDSerie;
  FLAN_NotaFiscal := NfsNumero;
  FLAN_Credito    := Valor;
  FLAN_MoraDia    := 0;
  FLAN_Multa      := 0;
  FLAN_Carteira   := CartEmiss;
  FLAN_Genero     := Genero;
  FLAN_Cliente    := Entidade;
  FLAN_CliInt     := Empresa;
  FLAN_Vencimento := Geral.FDT(Vencto, 1);
  {$IFDEF DEFINE_VARLCT}
  FLAN_Mez        := dmkPF.PeriodoToAnoMes(Periodo);
  {$ELSE}
  FLAN_Mez        := dmkPF.ITS_NULL(Periodo);
  {$ENDIF}
  FLAN_FatID      := VAR_FATID_0201;
  FLAN_FatNum     := NfsNumero;
  FLAN_FatParcela := 1;
  //
  FLAN_Descricao := Descri + ' - ' + dmkPF.MesEAnoDoPeriodo(Periodo);
  //
  FLAN_Controle := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle', TabLctA, LAN_CTOS, 'Controle');
  //
  {$IFDEF DEFINE_VARLCT}
    Res := UFinanceiro.InsereLancamento(TabLctA);
  {$ELSE}
    Res := UFinanceiro.InsereLancamento;
  {$ENDIF}
  //
  if Res then
  begin
    if UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
      'UPDATE nfsedpscab SET ',
      'FatID=' + Geral.FF0(VAR_FATID_0201) + ', ',
      'Lancto=' + Geral.FF0(FLAN_Controle),
      'WHERE Codigo=' + Geral.FF0(NfseDpsCab),
      '']) then
    begin
      Result := FLAN_Controle;
    end;
  end;
{$Else}
begin
  Grl_Geral.InfoSemModulo(mdlappFinanceiro);
{$EndIf}
end;

procedure TNFSe_PF_0000.MostraFormCNAE21Cad(Codigo: Integer = 0);
begin
  if DBCheck.CriaFm(TFmCNAE21Cad, FmCNAE21Cad, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
      FmCNAE21Cad.LocCod(Codigo, Codigo);
    FmCNAE21Cad.ShowModal;
    FmCNAE21Cad.Destroy;
  end;
end;

procedure TNFSe_PF_0000.MostraFormTsErrosAlertasCab();
begin
  if DBCheck.CriaFm(TFmTsErrosAlertasCab, FmTsErrosAlertasCab, afmoNegarComAviso) then
  begin
    FmTsErrosAlertasCab.ShowModal;
    FmTsErrosAlertasCab.Destroy;
  end;
end;

{
function TNFSe_PF_0000.ObtemProximoDPS(Empresa: Integer): Integer;
begin
  Result := DModG.BuscaProximoCodigoInt('paramsemp', 'DpsNumero',
    'WHERE Codigo=' + Geral.FF0(Empresa), 0);
end;
}

function TNFSe_PF_0000.ObtemProximoLoteRPS(Empresa, Ambiente: Integer): Integer;
var
  Campo: String;
begin
{
  Result := DModG.BuscaProximoCodigoInt('paramsemp', 'RPSNumLote',
    'WHERE Codigo=' + Geral.FF0(Empresa), 0);
}
  case Ambiente of
    1: Campo := 'RPSNumLote';
    2: Campo := 'HomNumLote';
    else Campo := '???NumLote';
  end;
  Result := DModG.BuscaProximoCodigoInt('paramsemp', Campo,
    'WHERE Codigo=' + Geral.FF0(Empresa), 0);
    //and amb = ?
end;

function TNFSe_PF_0000.MontaUrlNfse(IdMunicipio: Integer; Url, CNPJ: String;
  NumNfse: Integer; CodVal: String): String;
var
  Res: String;
begin
  /////////////////////////////////////////////////////////////////////////////
  ///  Sempre que mudar esta fun��o mudar na WEB em:                        ///
  ///  helper -> Dmk_nfse_helper.php -> monta_url_nfse                      ///
  /////////////////////////////////////////////////////////////////////////////

  if IdMunicipio = 4115200 then //Maring�
  begin
    Res := Url;
    Res := Geral.Substitui(Res, '[CNPJ]', CNPJ);
    Res := Geral.Substitui(Res, '[NUM_NFSE]', Geral.FF0(NumNfse));
    Res := Geral.Substitui(Res, '[COD_VAL]', CodVal);
  end else
		Res := '';
  //
  Result := Res;
end;

function TNFSe_PF_0000.ObtemUrlNfse(IdMunicipio: Integer): String;
var
  Site, Url: String;
begin
  /////////////////////////////////////////////////////////////////////////////
  ///  Sempre que mudar esta fun��o mudar na WEB em:                        ///
  ///  helper -> Dmk_nfse_helper.php -> obtem_url_nfse                      ///
  /////////////////////////////////////////////////////////////////////////////

  if IdMunicipio = 4115200 then
  begin
    Site := 'http://isse.maringa.pr.gov.br/print/nfse';
    Url  := Site + '/?cnpj=[CNPJ]&numnfe=[NUM_NFSE]&codval=[COD_VAL]';
  end else
    Url := '';
  //
  Result := Url;
end;

function TNFSe_PF_0000.ObtemProximaChaveRPS(const Empresa, Ambiente: Integer; const
  Tipo: Integer; const Serie: String; var Numero, NumHom: Integer): Integer;
var
  Campo: String;
begin
// E10
// RPS j� informado.
// Para essa Inscri��o Municipal/CNPJ j� existe um RPS informado com o mesmo n�mero, s�rie e tipo.
  //
  // ini 2023-12-22
(*
  UnDmkDAC_PF.AbreMySQLQuery0(DmNFSe_0000.QrNextID, Dmod.MyDB, [
  'SELECT MAX(RpsIDNumero) RPSIDNumero ',
  'FROM nfsedpscab ',
  'WHERE Empresa=' + Geral.FF0(Empresa),
  'AND Ambiente=' + Geral.FF0(Ambiente),
  'AND RpsIDSerie="' + Serie + '" ',
  'AND RpsIDTipo=' + Geral.FF0(Tipo),
  '']);
  Numero := DmNFSe_0000.QrNextIDRPSIDNumero.Value + 1;
  //
  Result := Numero > 0;
  //
*)
  Result := 0;
  case Ambiente of
    1: Result := Numero + 1;
    2: Result := NumHom + 1;
  end;
  // fim 2023-12-22
end;

{
procedure TNFSe_PF_0000.OnBeforePost(const HTTPReqResp: THTTPReqResp;
  Data: Pointer);
var
  Store        : IStore;
  Certs        : ICertificates;
  Cert         : ICertificate2;
  CertContext  : ICertContext;
  PCertContext : PCCERT_CONTEXT;
  i : Integer;
  ContentHeader: string;
begin
  
  Store := CoStore.Create;
  //Reposit�rios de Certifcados da M�quina

  Store.Open( CAPICOM_CURRENT_USER_STORE, 'MY',    CAPICOM_STORE_OPEN_MAXIMUM_ALLOWED );
  //Abre a lista de certificados

  Certs := Store.Certificates ;
  //Aloca todos os certificados instalados na m�quia

  i := 0;
  //loop de procura ao certificado requerido pelo n�mero serial
  while i < Certs.Count do
   begin
     Cert := IInterface( Certs.Item[ i+1 ] ) as ICertificate2;
     //Cria objeto para acesso a leitura do certificado
     if UpperCase(Cert.SerialNumber ) = UpperCase(FmNFSe_Steps_0201.EdSerialNumber.Text) then
     //se o n�mero do serial for igual ao que queremos utilizar
      begin
        //carrega informa��es do certificado
        CertContext := Cert as ICertContext;
        CertContext.Get_CertContext( Integer( PCertContext ) );

        //if not (InternetSetOption(Data, 84, PCertContext, Sizeof(CERT_CONTEXT))) then
        if not InternetSetOption(Data, INTERNET_OPTION_CLIENT_CERT_CONTEXT, PCertContext, sizeof(CertContext)*5) then
        begin
          Geral.MensagemBox('Falha ao selecionar o certificado.', 'Aviso', MB_OK+MB_ICONWARNING);
        end;

(*
  ContentHeader := Format(ContentTypeTemplate, ['application/soap+xml; charset=utf-8']);
  HttpAddRequestHeaders(Data, PChar(ContentHeader), Length(ContentHeader), HTTP_ADDREQ_FLAG_REPLACE);
*)
        //if FConfiguracoes.WebServices.VersaoSoap = '1.2'
        //then
          ContentHeader := Format(ContentTypeTemplate, ['application/soap+xml; charset=utf-8']);
        //else
          //ContentHeader := Format(ContentTypeTemplate, ['text/xml; charset=utf-8']);


        HttpAddRequestHeaders(Data, PChar(ContentHeader), Length(ContentHeader), HTTP_ADDREQ_FLAG_REPLACE);
        i := Certs.Count;
        //encerra o loop
      end;
     i := i + 1;
  end;
  HTTPReqResp.CheckContentType;
end;
}

function TNFSe_PF_0000.TextoStatusNFSe(Codigo: Integer): String;
begin
  case Codigo of
    0  : Result := '';
    //
    10 : Result := 'Registros do DPS inclu�dos na Base da Dados';
    20 : Result := 'XML do RPS foi gerada';
    30 : Result := 'XML do RPS est� assinado';
    40 : Result := 'Lote de RPS(s) rejeitado';
    50 : Result := 'RPS adicionada ao lote';
    60 : Result := 'Lote ao qual o RPS pertence foi enviado';
    70 : Result := 'Lote ao qual o RPS pertence em fase de consulta';
    //
    100: Result := 'RPS convertido em NFS-e';
    {
    101: Result := 'Cancelamento de NF-e homologado';
    102: Result := 'Inutiliza��o de n�mero homologado';
    }
    //
    else Result := 'Status n�o implementado';
  end;
end;

(*
procedure TNFSe_PF_0000.OnBeforePost2(const HTTPReqResp: THTTPReqResp;
  Data: Pointer);

function GetLastErrorText: string;
var
 aMsg: String;
begin
 case GetLastError of
  12030: aMsg := 'A conex�o com o servidor foi finalizada.';
  12044: aMsg := 'O Servidor est� solicitando autentica��o do cliente.';
  12046: aMsg := 'Autoriza��o do cliente n�o est� configurado neste computador.';
  else aMsg := IntToStr(GetLastError);
 end;
 Result := aMsg;
end;

var
 Cert         : ICertificate2;
 CertContext  : ICertContext;
 PCertContext : Pointer;
 ContentHeader: string;
begin
 Cert        := FConfiguracoes.Certificados.GetCertificado;
 CertContext := Cert as ICertContext;
 CertContext.Get_CertContext(Integer(PCertContext));

 if (FProvedor <> proGovBr) and
    (FProvedor <> proSimplISS)
  then begin
   if not InternetSetOption(Data, INTERNET_OPTION_CLIENT_CERT_CONTEXT, PCertContext, Sizeof(CERT_CONTEXT)*5)
    then begin
     if Assigned(TDmkACBrNFSe( FACBrNFSe ).OnGerarLog)
      then TDmkACBrNFSe( FACBrNFSe ).OnGerarLog('ERRO: Erro OnBeforePost: ' + IntToStr(GetLastError));
     raise Exception.Create( 'Erro OnBeforePost: ' + GetLastErrorText {IntToStr(GetLastError)} );
    end;
  end;

 if trim(FConfiguracoes.WebServices.ProxyUser) <> ''
  then begin
   if not InternetSetOption(Data, INTERNET_OPTION_PROXY_USERNAME, PChar(FConfiguracoes.WebServices.ProxyUser), Length(FConfiguracoes.WebServices.ProxyUser))
    then raise Exception.Create( 'Erro OnBeforePost: ' + IntToStr(GetLastError) );
   end;

 if trim(FConfiguracoes.WebServices.ProxyPass) <> ''
  then begin
   if not InternetSetOption(Data, INTERNET_OPTION_PROXY_PASSWORD, PChar(FConfiguracoes.WebServices.ProxyPass),Length (FConfiguracoes.WebServices.ProxyPass))
    then raise Exception.Create( 'Erro OnBeforePost: ' + IntToStr(GetLastError) );
   end;

  if (pos('SCERECEPCAORFB',UpperCase(FURL)) <= 0) and
     (pos('SCECONSULTARFB',UpperCase(FURL)) <= 0) then
   begin

     if FConfiguracoes.WebServices.VersaoSoap = '1.2'
      then ContentHeader := Format(ContentTypeTemplate, ['application/soap+xml; charset=utf-8'])
      else ContentHeader := Format(ContentTypeTemplate, ['text/xml; charset=utf-8']);

     HttpAddRequestHeaders(Data, PChar(ContentHeader), Length(ContentHeader), HTTP_ADDREQ_FLAG_REPLACE);
   end;
  HTTPReqResp.CheckContentType;
end;
*)

function TNFSe_PF_0000.VerificaDocumento(EntiTipo: String; const Tipo: Integer;
  const CNPJ_BD, CPF_BD: String; var CNPJ_NF, CPF_NF: String): Boolean;
begin
  Result := False;
  CNPJ_NF := '';
  CPF_NF := '';
  case Tipo of
    0: CNPJ_NF := Geral.SoNumero_TT(CNPJ_BD);
    1: CPF_NF := Geral.SoNumero_TT(CPF_BD);
  end;
  case Tipo of
    0: Result := Length(CNPJ_NF) = 14;
    1: Result := Length(CPF_NF) = 11;
  end;
  if not Result then
  begin
    case Tipo of
      0: FIC_PS(True, 'CNPJ do ' + EntiTipo + ' inv�lido: "' + CNPJ_BD + '"!');
      1: FIC_PS(True, 'CPF do ' + EntiTipo + ' inv�lido: "' + CPF_BD + '"!');
    end;
  end;
end;

function TNFSe_PF_0000.WS_NFeRecepcaoLote(UFServico: String; Ambiente,
  CodigoUF: Byte; NumeroSerial: String; Lote: Integer; Label1: TLabel;
  RETxtEnvio: TMemo; EdWS: TEdit): WideString;
begin

end;

{
function TNFSe_PF_0000.WS_NFeRecepcaoLote(Ambiente: Byte;
  NumeroSerial: String; RPS: Integer; Label1: TLabel;
  RETxtEnvio: TMemo; EdWS: TEdit): WideString;
function TProvedorGovBR.Gera_CabMsg(Prefixo2, VersaoLayOut, VersaoDados,
  NameSpaceCab: String): AnsiString;
begin
 Result := '<' + Prefixo2 + 'cabecalho versao="'  + VersaoLayOut + '"' + NameSpaceCab +
            '<versaoDados>' + VersaoDados + '</versaoDados>'+
           '</' + Prefixo2 + 'cabecalho>';
end;

const
  TipoConsumo = tcwsEnviaLoteNF;
var
  sAviso: String;
var
  Texto : String;
  Acao  : TStringList ;
  Stream: TMemoryStream;
  StrStream: TStringStream;
  ReqResp: THTTPReqResp;
  I: Integer;
  XML_STR: String;
begin
(*
"<?xml version=\"1.0\" encoding=\"UTF-8\"?><cabecalho xmlns=\"http://www.abrasf.org.br/nfse.xsd\" versao=\"1.00\"><versaoDados>1.00</versaoDados></cabecalho>";
*)

  Screen.Cursor := crHourGlass;
  if not ObtemWebServer(UFServico, Ambiente, CodigoUF, TipoConsumo,  sAviso, Label1) then Exit;
  FDadosTxt := FmNFeSteps_0200.FXML_LoteNFe;
  FDadosTxt := StringReplace( FDadosTxt, '<'+ENCODING_UTF8_STD+'>', '', [rfReplaceAll] ) ;
  FDadosTxt := StringReplace( FDadosTxt, '<'+ENCODING_UTF8+'>', '', [rfReplaceAll] ) ;
  FDadosTxt := StringReplace( FDadosTxt, '<?xml version="1.0"?>', '', [rfReplaceAll] ) ;

  Acao := TStringList.Create;
  Stream := TMemoryStream.Create;

  Texto := '<?xml version="1.0" encoding="utf-8"?>';
  Texto := Texto + '<soap12:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap12="http://www.w3.org/2003/05/soap-envelope">';
  Texto := Texto +   '<soap12:Header>';
  Texto := Texto +     '<nfeCabecMsg xmlns="http://www.portalfiscal.inf.br/nfe/wsdl/NfeRecepcao2">';
  Texto := Texto +       '<cUF>' + FormatFloat('00', CodigoUF) + '</cUF>';
  Texto := Texto +       '<versaoDados>' + verEnviNFe_Versao + '</versaoDados>';
  Texto := Texto +     '</nfeCabecMsg>';
  Texto := Texto +   '</soap12:Header>';
  Texto := Texto +   '<soap12:Body>';
  Texto := Texto +     '<nfeDadosMsg xmlns="http://www.portalfiscal.inf.br/nfe/wsdl/NfeRecepcao2">';
  Texto := Texto + FDadosTxt;
  Texto := Texto +     '</nfeDadosMsg>';
  Texto := Texto +   '</soap12:Body>';
  Texto := Texto +'</soap12:Envelope>';


  XML_STR := '';
  for i := 1 to Length(Texto) do
  begin
    if Ord(Texto[I]) > 31 then
      XML_STR := XML_STR + Texto[I]
  end;
  Texto := XML_STR;
  //
  Acao.Text := Texto;
  Acao.SaveToStream(Stream);

  ReqResp := THTTPReqResp.Create(nil);
  ConfiguraReqResp( ReqResp );
  ReqResp.URL := FURL;
  ReqResp.UseUTF8InHeader := True;
  ReqResp.SoapAction := 'http://www.portalfiscal.inf.br/nfe/wsdl/NfeRecepcao2';
  try
    //RETxtEnvio.Text :=  'URL: ' + FURL + sLineBreak + FCabecTxt + sLineBreak + FDadosTxt;
    RETxtEnvio.Text :=  Acao.Text;
    EdWS.Text := FURL;
    ReqResp.Execute(Acao.Text, Stream);

    StrStream := TStringStream.Create('');
    StrStream.CopyFrom(Stream, 0);

    Result := SeparaDados(StrStream.DataString,'nfeRecepcaoLote2Result');
    //
    StrStream.Free;
  finally
    Acao.Free;
    Stream.Free;
  end;
end;
}

procedure TNFSe_PF_0000.MostraFormNFSeFatCab(AbrirEmAba: Boolean;
  InOwner: TWincontrol; Pager: TWinControl; Codigo: Integer = 0);
var
  {$IFDEF DEFINE_VARLCT}
  DtEncer, DtMorto: TDateTime;
  TbLctB, TbLctD: String;
  Enti, CliIn: Integer;
  SelecionouEmpresa: Boolean;
  {$ENDIF}
  TbLctA: String;
  Form: TForm;
begin
  {$IFDEF DEFINE_VARLCT}
  SelecionouEmpresa := DModG.SelecionaEmpresa(sllLivre);
  Enti              := DModG.EmpresaAtual_ObtemCodigo(tecEntidade);
  CliIn             := DModG.EmpresaAtual_ObtemCodigo(tecCliInt);
  //
  if (Enti <> 0) and (CliIn <> 0) then
    DModG.Def_EM_ABD(TMeuDB, Enti, CliIn, DtEncer, DtMorto, TbLctA, TbLctB, TbLctD);
  {$ELSE}
  TbLctA := VAR_LCT;
  {$ENDIF}
  //
  if TbLctA = '' then
  begin
    Geral.MB_Erro('Falha ao obter tabela de lan�amentos financeiros!');
    Exit;
  end;
  //
  if AbrirEmAba then
  begin
    Form := MyObjects.FormTDICria(TFmNFSeFatCab, InOwner, Pager, True, True);
    //
    if Form <> nil then
    begin
      TFmNFSeFatCab(Form).FTbLctA := TbLctA;
      //
      if Codigo <> 0 then
        TFmNFSeFatCab(Form).LocCod(Codigo, Codigo);
    end;
  end else
  begin
    if DBCheck.CriaFm(TFmNFSeFatCab, FmNFSeFatCab, afmoNegarComAviso) then
    begin
      FmNFSeFatCab.FTbLctA := TbLctA;
      //
      if Codigo <> 0 then
        FmNFSeFatCab.LocCod(Codigo, Codigo);
      //
      FmNFSeFatCab.ShowModal;
      FmNFSeFatCab.Destroy;
      //
      FmNFSeFatCab := nil;
    end;
  end;
end;

procedure TNFSe_PF_0000.MostraFormNFSeLRpsC_0201(Lote: Integer);
begin
  if DBCheck.CriaFm(TFmNFSeLRpsC_0201, FmNFSeLRpsC_0201, afmoNegarComAviso) then
  begin
    if Lote <> 0 then
      FmNFSeLRpsC_0201.LocalizaLote(Lote);
    FmNFSeLRpsC_0201.ShowModal;
    FmNFSeLRpsC_0201.Destroy;
  end;
end;

procedure TNFSe_PF_0000.MostraFormNFSeMail(AbrirEmAba: Boolean;
  InOwner: TWincontrol; Pager: TWinControl; CliInt, NFSeFatCab,
  NFSeDPSCabCod: Integer; Avulso: Boolean);
var
  Form: TForm;
begin
  {$IfNDef SemProtocolo}
    if AbrirEmAba then
    begin
      Form := MyObjects.FormTDICria(TFmNFSeMail, InOwner, Pager, True, True);
      //
      if Form <> nil then
      begin
        TFmNFSeMail(Form).FCliInt        := CliInt;
        TFmNFSeMail(Form).FNFSeFatCab    := NFSeFatCab;
        TFmNFSeMail(Form).FNFSeDPSCabCod := NFSeDPSCabCod;
        TFmNFSeMail(Form).FAvulso        := Avulso;
        TFmNFSeMail(Form).ReopenQuerys(-1);
      end;
    end else
    begin
      if DBCheck.CriaFm(TFmNFSeMail, FmNFSeMail, afmoNegarComAviso) then
      begin
        FmNFSeMail.FCliInt        := CliInt;
        FmNFSeMail.FNFSeFatCab    := NFSeFatCab;
        FmNFSeMail.FNFSeDPSCabCod := NFSeDPSCabCod;
        FmNFSeMail.FAvulso        := Avulso;
        FmNFSeMail.ReopenQuerys(-1);
        FmNFSeMail.ShowModal;
        FmNFSeMail.Destroy;
      end;
    end;
  {$Else}
    Grl_Geral.InfoSemModulo(mdlappProtocolos);
  {$EndIf}
end;

procedure TNFSe_PF_0000.MostraFormNFSeMenCab(AbrirEmAba: Boolean;
  InOwner: TWincontrol; Pager: TWinControl);
begin
  if AbrirEmAba then
  begin
    if FmNFSeFatCab = nil then
      MyObjects.FormTDICria(TFmNFSeMenCab, InOwner, Pager, False, True);
  end else
  begin
    if DBCheck.CriaFm(TFmNFSeMenCab, FmNFSeMenCab, afmoNegarComAviso) then
    begin
      FmNFSeMenCab.ShowModal;
      FmNFSeMenCab.Destroy;
      //
      FmNFSeMenCab := nil;
    end;
  end;
end;

procedure TNFSe_PF_0000.MostraFormNFSeParams();
var
  Empresa: Integer;
begin
  if not DmodG.SoUmaEmpresaLogada(True) then Exit;
  //
  if Grl_Geral.LiberaModulo(CO_DMKID_APP, Grl_Geral.ObtemSiglaModulo(mdlappNFSe),
    DModG.QrMasterHabilModulos.Value, True) then
  begin
    Empresa := DmodG.QrFiliLogCodigo.Value;
    //
    DmodG.ReopenParamsEmp(Empresa);
    //
    if DBCheck.CriaFm(TFmNFSeParams, FmNFSeParams, afmoNegarComAviso) then
    begin
      FmNFSeParams.FEmpresa := Empresa;
      FmNFSeParams.ShowModal;
      FmNFSeParams.Destroy;
    end;
  end;
end;

procedure TNFSe_PF_0000.MostraFormNFSe_NFSePesq(AbrirEmAba: Boolean;
  InOwner: TWincontrol; Pager: TWinControl);
begin
  if AbrirEmAba then
  begin
    if FmNFSeFatCab = nil then
      MyObjects.FormTDICria(TFmNFSe_NFSPesq, InOwner, Pager, True, True);
  end else
  begin
    if DBCheck.CriaFm(TFmNFSe_NFSPesq, FmNFSe_NFSPesq, afmoNegarComAviso) then
    begin
      FmNFSe_NFSPesq.ShowModal;
      FmNFSe_NFSPesq.Destroy;
      //
      FmNFSe_NFSPesq := nil;
    end;
  end;
end;

procedure TNFSe_PF_0000.MostraFormNFSe_RPSPesq(AbrirEmAba: Boolean;
  InOwner: TWincontrol; Pager: TWinControl);
begin
  if AbrirEmAba then
  begin
    if FmNFSeFatCab = nil then
      MyObjects.FormTDICria(TFmNFSe_RPSPesq, InOwner, Pager, False, True);
  end else
  begin
    if DBCheck.CriaFm(TFmNFSe_RPSPesq, FmNFSe_RPSPesq, afmoNegarComAviso) then
    begin
      FmNFSe_RPSPesq.ShowModal;
      FmNFSe_RPSPesq.Destroy;
      //
      FmNFSe_RPSPesq := nil;
    end;
  end;
end;
{$EndIf}


end.

