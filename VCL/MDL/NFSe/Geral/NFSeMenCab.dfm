object FmNFSeMenCab: TFmNFSeMenCab
  Left = 339
  Top = 185
  Caption = 'NFS-MENSA-001 :: Configura'#231#227'o de Mensalidades NFS-e'
  ClientHeight = 562
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
        OnClick = SbImprimeClick
      end
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 912
      Height = 52
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 461
        Height = 32
        Caption = 'Configura'#231#227'o de Mensalidades NFS-e'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 461
        Height = 32
        Caption = 'Configura'#231#227'o de Mensalidades NFS-e'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 461
        Height = 32
        Caption = 'Configura'#231#227'o de Mensalidades NFS-e'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 52
    Width = 1008
    Height = 396
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 1008
      Height = 396
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaTotal: TLabel
        Left = 0
        Top = 383
        Width = 1008
        Height = 13
        Align = alBottom
        ExplicitWidth = 3
      end
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 1008
        Height = 117
        Align = alTop
        Caption = ' Filtro de pesquisa: '
        TabOrder = 0
        object Panel5: TPanel
          Left = 2
          Top = 15
          Width = 1004
          Height = 100
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          object Label9: TLabel
            Left = 8
            Top = 8
            Width = 44
            Height = 13
            Caption = 'Empresa:'
          end
          object Label17: TLabel
            Left = 600
            Top = 8
            Width = 63
            Height = 13
            Caption = 'Intermedi'#225'rio:'
          end
          object Label34: TLabel
            Left = 8
            Top = 32
            Width = 97
            Height = 13
            Caption = 'Tomador do servi'#231'o:'
          end
          object Label19: TLabel
            Left = 8
            Top = 56
            Width = 94
            Height = 13
            Caption = 'Minha Regra Fiscal:'
          end
          object Label18: TLabel
            Left = 8
            Top = 80
            Width = 99
            Height = 13
            Caption = 'Servi'#231'o (LC 116/03):'
          end
          object EdEmpresa: TdmkEditCB
            Left = 55
            Top = 4
            Width = 44
            Height = 21
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            OnChange = EdEmpresaChange
            DBLookupComboBox = CBEmpresa
            IgnoraDBLookupComboBox = False
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
          object CBEmpresa: TdmkDBLookupComboBox
            Left = 100
            Top = 4
            Width = 497
            Height = 21
            KeyField = 'Filial'
            ListField = 'NOMEFILIAL'
            ListSource = DModG.DsEmpresas
            TabOrder = 1
            dmkEditCB = EdEmpresa
            UpdType = utYes
            LocF7SQLMasc = '$#'
            LocF7PreDefProc = f7pNone
          end
          object EdIntermediario: TdmkEditCB
            Left = 664
            Top = 4
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 2
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            OnChange = EdIntermediarioChange
            DBLookupComboBox = CBIntermediario
            IgnoraDBLookupComboBox = False
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
          object CBIntermediario: TdmkDBLookupComboBox
            Left = 720
            Top = 4
            Width = 273
            Height = 21
            KeyField = 'Codigo'
            ListField = 'NOMEENTIDADE'
            ListSource = DsIntermediarios
            TabOrder = 3
            dmkEditCB = EdIntermediario
            UpdType = utYes
            LocF7SQLMasc = '$#'
            LocF7PreDefProc = f7pNone
          end
          object CBCliente: TdmkDBLookupComboBox
            Left = 168
            Top = 28
            Width = 825
            Height = 21
            KeyField = 'Codigo'
            ListField = 'NOMEENTIDADE'
            ListSource = DsClientes
            TabOrder = 4
            dmkEditCB = EdCliente
            UpdType = utYes
            LocF7SQLMasc = '$#'
            LocF7PreDefProc = f7pNone
          end
          object EdCliente: TdmkEditCB
            Left = 112
            Top = 28
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 5
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            OnChange = EdClienteChange
            DBLookupComboBox = CBCliente
            IgnoraDBLookupComboBox = False
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
          object EdNFSeSrvCad: TdmkEditCB
            Left = 112
            Top = 52
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 6
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            OnChange = EdNFSeSrvCadChange
            DBLookupComboBox = CBNFSeSrvCad
            IgnoraDBLookupComboBox = False
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
          object CBNFSeSrvCad: TdmkDBLookupComboBox
            Left = 168
            Top = 52
            Width = 493
            Height = 21
            KeyField = 'Codigo'
            ListField = 'Nome'
            ListSource = DsNFSeSrvCad
            TabOrder = 7
            dmkEditCB = EdNFSeSrvCad
            UpdType = utYes
            LocF7SQLMasc = '$#'
            LocF7PreDefProc = f7pNone
          end
          object EdItemListaServico: TdmkEditCB
            Left = 112
            Top = 76
            Width = 56
            Height = 21
            TabOrder = 8
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
            OnChange = EdItemListaServicoChange
            DBLookupComboBox = CBItemListaServico
            IgnoraDBLookupComboBox = False
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
          object CBItemListaServico: TdmkDBLookupComboBox
            Left = 168
            Top = 76
            Width = 705
            Height = 21
            KeyField = 'CodAlf'
            ListField = 'Nome'
            ListSource = DsListServ
            TabOrder = 9
            dmkEditCB = EdItemListaServico
            UpdType = utYes
            LocF7SQLMasc = '$#'
            LocF7PreDefProc = f7pNone
          end
          object BtReabre: TBitBtn
            Tag = 18
            Left = 876
            Top = 56
            Width = 120
            Height = 40
            Caption = '&Reabre'
            NumGlyphs = 2
            TabOrder = 10
            OnClick = BtReabreClick
          end
          object CkVigenteEm: TCheckBox
            Left = 668
            Top = 54
            Width = 89
            Height = 17
            Caption = 'Vigente em: '
            TabOrder = 11
          end
          object TPVigenteEm: TdmkEditDateTimePicker
            Left = 760
            Top = 52
            Width = 112
            Height = 21
            Date = 41580.437198923610000000
            Time = 41580.437198923610000000
            TabOrder = 12
            ReadOnly = False
            DefaultEditMask = '!99/99/99;1;_'
            AutoApplyEditMask = True
            UpdType = utYes
            DatePurpose = dmkdpNone
          end
        end
      end
      object DBGrid1: TdmkDBGridZTO
        Left = 0
        Top = 117
        Width = 1008
        Height = 266
        Align = alClient
        DataSource = DsNFSeMenCab
        Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect, dgTitleClick, dgTitleHotTrack]
        TabOrder = 1
        TitleFont.Charset = ANSI_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        RowColors = <>
        Columns = <
          item
            Expanded = False
            FieldName = 'Codigo'
            Title.Caption = 'C'#243'digo'
            Width = 44
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Ativo'
            Width = 45
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Cliente'
            Width = 44
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NO_CLI'
            Title.Caption = 'Nome cliente (Tomador)'
            Width = 200
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Contrato'
            Width = 48
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NFSeSrvCad'
            Title.Caption = 'Regra'
            Width = 36
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NO_SRV'
            Title.Caption = 'Descri'#231#227'o da Regra Fiscal'
            Width = 200
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'SN_LCT_TXT'
            Title.Caption = 'Lct?'
            Width = 32
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'SN_BOL_TXT'
            Title.Caption = 'Bol?'
            Width = 32
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'GLCarteira'
            Title.Caption = 'Carteira'
            Width = 40
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'GLConta'
            Title.Caption = 'Conta'
            Width = 40
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'IncreCompet'
            Title.Caption = 'IC'
            Width = 20
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'ValorNFSe'
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            Title.Caption = 'Valor'
            Width = 72
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'VigenDtIni'
            Title.Caption = 'Dt vig'#234'n.ini'
            Width = 60
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'VigenDtFim'
            Title.Caption = 'Dt vig'#234'n.fim'
            Width = 60
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NO_INT'
            Title.Caption = 'Intermedi'#225'rio'
            Width = 120
            Visible = True
          end>
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 448
    Width = 1008
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 492
    Width = 1008
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 746
      Top = 15
      Width = 260
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 130
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sair'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
      object BtSoma: TBitBtn
        Tag = 254
        Left = 6
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Hint = 'Altera banco atual'
        Caption = '&Soma'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        OnClick = BtSomaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 744
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtInclui: TBitBtn
        Tag = 10
        Left = 6
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Inclui'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtIncluiClick
      end
      object BtAltera: TBitBtn
        Tag = 11
        Left = 128
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Altera'
        Enabled = False
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        OnClick = BtAlteraClick
      end
      object BtExclui: TBitBtn
        Tag = 12
        Left = 252
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Exclui'
        Enabled = False
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 2
        OnClick = BtExcluiClick
      end
    end
  end
  object QrClientes: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, IF(Tipo=0, RazaoSocial, Nome) NOMEENTIDADE'
      'FROM entidades'
      'ORDER BY NOMEENTIDADE'
      '')
    Left = 520
    Top = 16
    object QrClientesCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrClientesNOMEENTIDADE: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object DsClientes: TDataSource
    DataSet = QrClientes
    Left = 548
    Top = 16
  end
  object QrListServ: TMySQLQuery
    Database = Dmod.ZZDB
    SQL.Strings = (
      'SELECT CodAlf, Nome'
      'FROM listserv'
      'ORDER BY Nome')
    Left = 576
    Top = 16
    object QrListServNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 255
    end
    object QrListServCodAlf: TWideStringField
      FieldName = 'CodAlf'
      Required = True
    end
  end
  object DsListServ: TDataSource
    DataSet = QrListServ
    Left = 604
    Top = 16
  end
  object QrNFSeSrvCad: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM nfsesrvcad'
      'ORDER BY Nome')
    Left = 632
    Top = 16
    object QrNFSeSrvCadCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrNFSeSrvCadNome: TWideStringField
      FieldName = 'Nome'
      Size = 255
    end
  end
  object DsNFSeSrvCad: TDataSource
    DataSet = QrNFSeSrvCad
    Left = 660
    Top = 16
  end
  object QrIntermediarios: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, IF(Tipo=0, RazaoSocial, Nome) NOMEENTIDADE'
      'FROM entidades'
      'ORDER BY NOMEENTIDADE'
      '')
    Left = 688
    Top = 16
    object QrIntermediariosCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrIntermediariosNOMEENTIDADE: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object DsIntermediarios: TDataSource
    DataSet = QrIntermediarios
    Left = 716
    Top = 16
  end
  object QrNFSeMenCab: TMySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrNFSeMenCabAfterOpen
    BeforeClose = QrNFSeMenCabBeforeClose
    AfterScroll = QrNFSeMenCabAfterScroll
    OnCalcFields = QrNFSeMenCabCalcFields
    SQL.Strings = (
      'SELECT nmc.*, nsc.Nome NO_SRV, '
      'car.Nome NO_CRT, cta.Nome NO_CTA,'
      'IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NO_CLI,'
      'IF(ntm.Tipo=0, ntm.RazaoSocial, ntm.Nome) NO_INT'
      'FROM nfsemencab nmc'
      'LEFT JOIN entidades cli ON cli.Codigo=nmc.Cliente '
      'LEFT JOIN entidades ntm ON ntm.Codigo=nmc.Intermed'
      'LEFT JOIN nfsesrvcad nsc ON nsc.Codigo=nmc.NFSeSrvCad'
      'LEFT JOIN carteiras car ON car.Codigo=nmc.GLCarteira'
      'LEFT JOIN contas cta ON cta.Codigo=nmc.GLConta')
    Left = 328
    Top = 216
    object QrNFSeMenCabCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'nfsemencab.Codigo'
    end
    object QrNFSeMenCabEmpresa: TIntegerField
      FieldName = 'Empresa'
      Origin = 'nfsemencab.Empresa'
    end
    object QrNFSeMenCabCliente: TIntegerField
      FieldName = 'Cliente'
      Origin = 'nfsemencab.Cliente'
    end
    object QrNFSeMenCabContrato: TIntegerField
      FieldName = 'Contrato'
      Origin = 'nfsemencab.Contrato'
    end
    object QrNFSeMenCabIntermed: TIntegerField
      FieldName = 'Intermed'
      Origin = 'nfsemencab.Intermed'
    end
    object QrNFSeMenCabNFSeSrvCad: TIntegerField
      FieldName = 'NFSeSrvCad'
      Origin = 'nfsemencab.NFSeSrvCad'
    end
    object QrNFSeMenCabDiaFat: TSmallintField
      FieldName = 'DiaFat'
      Origin = 'nfsemencab.DiaFat'
    end
    object QrNFSeMenCabIncreCompet: TSmallintField
      FieldName = 'IncreCompet'
      Origin = 'nfsemencab.IncreCompet'
    end
    object QrNFSeMenCabValor: TFloatField
      FieldName = 'Valor'
      Origin = 'nfsemencab.Valor'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrNFSeMenCabGLGerarLct: TSmallintField
      FieldName = 'GLGerarLct'
      Origin = 'nfsemencab.GLGerarLct'
    end
    object QrNFSeMenCabGLCarteira: TIntegerField
      FieldName = 'GLCarteira'
      Origin = 'nfsemencab.GLCarteira'
    end
    object QrNFSeMenCabGLConta: TIntegerField
      FieldName = 'GLConta'
      Origin = 'nfsemencab.GLConta'
    end
    object QrNFSeMenCabVigenDtIni: TDateField
      FieldName = 'VigenDtIni'
      Origin = 'nfsemencab.VigenDtIni'
      OnGetText = QrNFSeMenCabVigenDtIniGetText
    end
    object QrNFSeMenCabVigenDtFim: TDateField
      FieldName = 'VigenDtFim'
      Origin = 'nfsemencab.VigenDtFim'
      OnGetText = QrNFSeMenCabVigenDtFimGetText
    end
    object QrNFSeMenCabLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'nfsemencab.Lk'
    end
    object QrNFSeMenCabDataCad: TDateField
      FieldName = 'DataCad'
      Origin = 'nfsemencab.DataCad'
    end
    object QrNFSeMenCabDataAlt: TDateField
      FieldName = 'DataAlt'
      Origin = 'nfsemencab.DataAlt'
    end
    object QrNFSeMenCabUserCad: TIntegerField
      FieldName = 'UserCad'
      Origin = 'nfsemencab.UserCad'
    end
    object QrNFSeMenCabUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Origin = 'nfsemencab.UserAlt'
    end
    object QrNFSeMenCabAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Origin = 'nfsemencab.AlterWeb'
    end
    object QrNFSeMenCabAtivo: TSmallintField
      FieldName = 'Ativo'
      Origin = 'nfsemencab.Ativo'
      MaxValue = 1
    end
    object QrNFSeMenCabNO_SRV: TWideStringField
      FieldName = 'NO_SRV'
      Origin = 'nfsesrvcad.Nome'
      Size = 255
    end
    object QrNFSeMenCabNO_CRT: TWideStringField
      FieldName = 'NO_CRT'
      Origin = 'carteiras.Nome'
      Size = 100
    end
    object QrNFSeMenCabNO_CTA: TWideStringField
      FieldName = 'NO_CTA'
      Origin = 'contas.Nome'
      Size = 50
    end
    object QrNFSeMenCabNO_CLI: TWideStringField
      FieldName = 'NO_CLI'
      Size = 100
    end
    object QrNFSeMenCabNO_INT: TWideStringField
      FieldName = 'NO_INT'
      Size = 100
    end
    object QrNFSeMenCabGBGerarBol: TSmallintField
      FieldName = 'GBGerarBol'
      Origin = 'nfsemencab.GBGerarBol'
    end
    object QrNFSeMenCabSN_LCT_TXT: TWideStringField
      FieldName = 'SN_LCT_TXT'
      Size = 6
    end
    object QrNFSeMenCabSN_BOL_TXT: TWideStringField
      FieldName = 'SN_BOL_TXT'
      Size = 6
    end
    object QrNFSeMenCabValorNFSe: TFloatField
      FieldName = 'ValorNFSe'
      DisplayFormat = '#,###,###,##0.00'
    end
  end
  object DsNFSeMenCab: TDataSource
    DataSet = QrNFSeMenCab
    Left = 328
    Top = 264
  end
  object CSTabSheetChamou: TdmkCompoStore
    Left = 796
    Top = 20
  end
  object frxNFS_MENSA_001_01A: TfrxReport
    Version = '5.6.18'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39951.885535092600000000
    ReportOptions.LastChange = 39951.885535092600000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      'end.')
    OnGetValue = frxNFS_MENSA_001_01AGetValue
    Left = 436
    Top = 264
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsNFSeMenCab
        DataSetName = 'frxDsNFSeMenCab'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      Orientation = poLandscape
      PaperWidth = 297.000000000000000000
      PaperHeight = 210.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 20.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      LargeDesignHeight = True
      object PageHeader4: TfrxPageHeader
        FillType = ftBrush
        Height = 139.842600240000000000
        Top = 18.897650000000000000
        Width = 971.339210000000000000
        object Shape3: TfrxShapeView
          Width = 971.339210000000000000
          Height = 37.795300000000000000
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo46: TfrxMemoView
          Left = 7.559060000000000000
          Width = 956.221090000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line3: TfrxLineView
          Top = 18.897650000000000000
          Width = 971.339210000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo47: TfrxMemoView
          Left = 151.181200000000000000
          Top = 18.897650000000000000
          Width = 668.976409690000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'CONFIGURA'#199#213'ES DE EMISS'#213'ES MENSAIS')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo48: TfrxMemoView
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 143.622076540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[Now]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MePagina: TfrxMemoView
          Left = 820.158010000000000000
          Top = 18.897650000000000000
          Width = 143.622047240000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo50: TfrxMemoView
          Top = 41.574830000000000000
          Width = 971.339210000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_NO_EMPRESA]')
          ParentFont = False
        end
        object Memo86: TfrxMemoView
          Top = 64.252010000000000000
          Width = 302.362400000000000000
          Height = 15.118110236220470000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Vigente em: [VARF_VIGENTEEM]')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          Top = 124.724490000000000000
          Width = 45.354360000000000000
          Height = 15.118110240000000000
          DataSet = frxDsNFSeMenCab
          DataSetName = 'frxDsNFSeMenCab'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'C'#243'digo')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          Top = 79.370130000000000000
          Width = 971.338582677165300000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Tomador do servi'#231'o: [VARF_TOMADOR]')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          Top = 94.488250000000000000
          Width = 971.338582677165300000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Minha Regra Fiscal: [VARF_MinhaRegraFiscal]')
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          Left = 181.417440000000000000
          Top = 64.252010000000000000
          Width = 789.921770000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Intermedi'#225'rio: [VARF_INTERMEDIARIO]')
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          Top = 109.606370000000000000
          Width = 971.338582677165300000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Servi'#231'o (LC 116/03): [VARF_LC_116_03]')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          Left = 45.354360000000000000
          Top = 124.724490000000000000
          Width = 272.126160000000000000
          Height = 15.118110240000000000
          DataSet = frxDsNFSeMenCab
          DataSetName = 'frxDsNFSeMenCab'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Cliente')
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          Left = 317.480520000000000000
          Top = 124.724490000000000000
          Width = 45.354360000000000000
          Height = 15.118110240000000000
          DataSet = frxDsNFSeMenCab
          DataSetName = 'frxDsNFSeMenCab'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Contrato')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          Left = 362.834880000000000000
          Top = 124.724490000000000000
          Width = 196.535560000000000000
          Height = 15.118110240000000000
          DataSet = frxDsNFSeMenCab
          DataSetName = 'frxDsNFSeMenCab'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Regra')
          ParentFont = False
        end
        object Memo11: TfrxMemoView
          Left = 559.370440000000000000
          Top = 124.724490000000000000
          Width = 18.897650000000000000
          Height = 15.118110240000000000
          DataSet = frxDsNFSeMenCab
          DataSetName = 'frxDsNFSeMenCab'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Lct?')
          ParentFont = False
        end
        object Memo16: TfrxMemoView
          Left = 578.268090000000000000
          Top = 124.724490000000000000
          Width = 18.897650000000000000
          Height = 15.118110240000000000
          DataSet = frxDsNFSeMenCab
          DataSetName = 'frxDsNFSeMenCab'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Bol?')
          ParentFont = False
        end
        object Memo19: TfrxMemoView
          Left = 597.165740000000000000
          Top = 124.724490000000000000
          Width = 45.354360000000000000
          Height = 15.118110240000000000
          DataSet = frxDsNFSeMenCab
          DataSetName = 'frxDsNFSeMenCab'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Carteira')
          ParentFont = False
        end
        object Memo20: TfrxMemoView
          Left = 642.520100000000000000
          Top = 124.724490000000000000
          Width = 45.354360000000000000
          Height = 15.118110240000000000
          DataSet = frxDsNFSeMenCab
          DataSetName = 'frxDsNFSeMenCab'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Conta')
          ParentFont = False
        end
        object Memo23: TfrxMemoView
          Left = 687.874460000000000000
          Top = 124.724490000000000000
          Width = 18.897650000000000000
          Height = 15.118110240000000000
          DataSet = frxDsNFSeMenCab
          DataSetName = 'frxDsNFSeMenCab'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'IC')
          ParentFont = False
        end
        object Memo24: TfrxMemoView
          Left = 706.772110000000000000
          Top = 124.724490000000000000
          Width = 18.897650000000000000
          Height = 15.118110240000000000
          DataSet = frxDsNFSeMenCab
          DataSetName = 'frxDsNFSeMenCab'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Dia')
          ParentFont = False
        end
        object Memo25: TfrxMemoView
          Left = 725.669760000000000000
          Top = 124.724490000000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          DataSet = frxDsNFSeMenCab
          DataSetName = 'frxDsNFSeMenCab'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Valor')
          ParentFont = False
        end
        object Memo29: TfrxMemoView
          Left = 786.142240000000000000
          Top = 124.724490000000000000
          Width = 90.708720000000000000
          Height = 15.118110240000000000
          DataSet = frxDsNFSeMenCab
          DataSetName = 'frxDsNFSeMenCab'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Vig'#234'ncia')
          ParentFont = False
        end
        object Memo30: TfrxMemoView
          Left = 876.850960000000000000
          Top = 124.724490000000000000
          Width = 94.488250000000000000
          Height = 15.118110240000000000
          DataSet = frxDsNFSeMenCab
          DataSetName = 'frxDsNFSeMenCab'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Intermediario')
          ParentFont = False
        end
      end
      object PageFooter2: TfrxPageFooter
        FillType = ftBrush
        Height = 15.118110240000000000
        Top = 294.803340000000000000
        Width = 971.339210000000000000
        object Memo93: TfrxMemoView
          Width = 676.535870000000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
        object MeVARF_CODI_FRX: TfrxMemoView
          Left = 653.858690000000000000
          Width = 321.260050000000000000
          Height = 13.228346460000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_CODI_FRX]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        Height = 15.118110240000000000
        Top = 219.212740000000000000
        Width = 971.339210000000000000
        DataSet = frxDsNFSeMenCab
        DataSetName = 'frxDsNFSeMenCab'
        RowCount = 0
        object Memo1: TfrxMemoView
          Width = 45.354360000000000000
          Height = 15.118110240000000000
          DataField = 'Codigo'
          DataSet = frxDsNFSeMenCab
          DataSetName = 'frxDsNFSeMenCab'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsNFSeMenCab."Codigo"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo12: TfrxMemoView
          Left = 45.354360000000000000
          Width = 45.354360000000000000
          Height = 15.118110240000000000
          DataField = 'Cliente'
          DataSet = frxDsNFSeMenCab
          DataSetName = 'frxDsNFSeMenCab'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsNFSeMenCab."Cliente"]')
          ParentFont = False
        end
        object Memo13: TfrxMemoView
          Left = 90.708720000000000000
          Width = 226.771653543307100000
          Height = 15.118110240000000000
          DataField = 'NO_CLI'
          DataSet = frxDsNFSeMenCab
          DataSetName = 'frxDsNFSeMenCab'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsNFSeMenCab."NO_CLI"]')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          Left = 317.480520000000000000
          Width = 45.354360000000000000
          Height = 15.118110240000000000
          DataField = 'Contrato'
          DataSet = frxDsNFSeMenCab
          DataSetName = 'frxDsNFSeMenCab'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsNFSeMenCab."Contrato"]')
          ParentFont = False
        end
        object Memo14: TfrxMemoView
          Left = 362.834880000000000000
          Width = 45.354360000000000000
          Height = 15.118110240000000000
          DataField = 'NFSeSrvCad'
          DataSet = frxDsNFSeMenCab
          DataSetName = 'frxDsNFSeMenCab'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsNFSeMenCab."NFSeSrvCad"]')
          ParentFont = False
        end
        object Memo15: TfrxMemoView
          Left = 408.189240000000000000
          Width = 151.181102362204700000
          Height = 15.118110240000000000
          DataField = 'NO_SRV'
          DataSet = frxDsNFSeMenCab
          DataSetName = 'frxDsNFSeMenCab'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsNFSeMenCab."NO_SRV"]')
          ParentFont = False
        end
        object Memo17: TfrxMemoView
          Left = 559.370440000000000000
          Width = 18.897650000000000000
          Height = 15.118110240000000000
          DataField = 'SN_LCT_TXT'
          DataSet = frxDsNFSeMenCab
          DataSetName = 'frxDsNFSeMenCab'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsNFSeMenCab."SN_LCT_TXT"]')
          ParentFont = False
        end
        object Memo18: TfrxMemoView
          Left = 578.268090000000000000
          Width = 18.897650000000000000
          Height = 15.118110240000000000
          DataField = 'SN_BOL_TXT'
          DataSet = frxDsNFSeMenCab
          DataSetName = 'frxDsNFSeMenCab'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsNFSeMenCab."SN_BOL_TXT"]')
          ParentFont = False
        end
        object Memo21: TfrxMemoView
          Left = 597.165740000000000000
          Width = 45.354360000000000000
          Height = 15.118110240000000000
          DataField = 'GLCarteira'
          DataSet = frxDsNFSeMenCab
          DataSetName = 'frxDsNFSeMenCab'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsNFSeMenCab."GLCarteira"]')
          ParentFont = False
        end
        object Memo22: TfrxMemoView
          Left = 642.520100000000000000
          Width = 45.354360000000000000
          Height = 15.118110240000000000
          DataField = 'GLConta'
          DataSet = frxDsNFSeMenCab
          DataSetName = 'frxDsNFSeMenCab'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsNFSeMenCab."GLConta"]')
          ParentFont = False
        end
        object Memo26: TfrxMemoView
          Left = 687.874460000000000000
          Width = 18.897650000000000000
          Height = 15.118110240000000000
          DataField = 'IncreCompet'
          DataSet = frxDsNFSeMenCab
          DataSetName = 'frxDsNFSeMenCab'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsNFSeMenCab."IncreCompet"]')
          ParentFont = False
        end
        object Memo27: TfrxMemoView
          Left = 706.772110000000000000
          Width = 18.897650000000000000
          Height = 15.118110240000000000
          DataField = 'DiaFat'
          DataSet = frxDsNFSeMenCab
          DataSetName = 'frxDsNFSeMenCab'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsNFSeMenCab."DiaFat"]')
          ParentFont = False
        end
        object Memo28: TfrxMemoView
          Left = 725.669760000000000000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          DataField = 'Valor'
          DataSet = frxDsNFSeMenCab
          DataSetName = 'frxDsNFSeMenCab'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsNFSeMenCab."Valor"]')
          ParentFont = False
        end
        object Memo31: TfrxMemoView
          Left = 786.142240000000000000
          Width = 45.354360000000000000
          Height = 15.118110240000000000
          DataField = 'VigenDtIni'
          DataSet = frxDsNFSeMenCab
          DataSetName = 'frxDsNFSeMenCab'
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsNFSeMenCab."VigenDtIni"]')
          ParentFont = False
        end
        object Memo32: TfrxMemoView
          Left = 831.496600000000000000
          Width = 45.354360000000000000
          Height = 15.118110240000000000
          DataField = 'VigenDtFim'
          DataSet = frxDsNFSeMenCab
          DataSetName = 'frxDsNFSeMenCab'
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsNFSeMenCab."VigenDtFim"]')
          ParentFont = False
        end
        object Memo33: TfrxMemoView
          Left = 876.850960000000000000
          Width = 94.488250000000000000
          Height = 15.118110240000000000
          DataField = 'NO_INT'
          DataSet = frxDsNFSeMenCab
          DataSetName = 'frxDsNFSeMenCab'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsNFSeMenCab."NO_INT"]')
          ParentFont = False
        end
      end
    end
  end
  object frxDsNFSeMenCab: TfrxDBDataset
    UserName = 'frxDsNFSeMenCab'
    CloseDataSource = False
    DataSet = QrNFSeMenCab
    BCDToCurrency = False
    Left = 436
    Top = 216
  end
end
