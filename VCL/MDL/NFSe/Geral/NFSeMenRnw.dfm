object FmNFSeMenRnw: TFmNFSeMenRnw
  Left = 339
  Top = 185
  Caption = 'ACT-RENEW-003 :: Emiss'#245'es Mensais de NFS-e Vencidas'
  ClientHeight = 629
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 912
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 470
        Height = 32
        Caption = 'Emiss'#245'es Mensais de NFS-e Vencidas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 470
        Height = 32
        Caption = 'Emiss'#245'es Mensais de NFS-e Vencidas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 470
        Height = 32
        Caption = 'Emiss'#245'es Mensais de NFS-e Vencidas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 1008
    Height = 467
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 1008
      Height = 467
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 1008
        Height = 467
        Align = alClient
        TabOrder = 0
        object LaTotal: TLabel
          Left = 2
          Top = 452
          Width = 3
          Height = 13
          Align = alBottom
        end
        object DBGrid1: TdmkDBGridZTO
          Left = 2
          Top = 15
          Width = 1004
          Height = 437
          Align = alClient
          DataSource = DsNFSeMenCab
          Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
          TabOrder = 0
          TitleFont.Charset = ANSI_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          RowColors = <>
          OnDblClick = DBGrid1DblClick
          Columns = <
            item
              Expanded = False
              FieldName = 'Codigo'
              Title.Caption = 'C'#243'digo'
              Width = 44
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Cliente'
              Width = 44
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_CLI'
              Title.Caption = 'Nome cliente (Tomador)'
              Width = 200
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Contrato'
              Width = 48
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NFSeSrvCad'
              Title.Caption = 'Regra'
              Width = 36
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_SRV'
              Title.Caption = 'Descri'#231#227'o da Regra Fiscal'
              Width = 200
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'SN_LCT_TXT'
              Title.Caption = 'Lct?'
              Width = 32
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'SN_BOL_TXT'
              Title.Caption = 'Bol?'
              Width = 32
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'GLCarteira'
              Title.Caption = 'Carteira'
              Width = 40
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'GLConta'
              Title.Caption = 'Conta'
              Width = 40
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'IncreCompet'
              Title.Caption = 'IC'
              Width = 20
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DiaFat'
              Title.Caption = 'Dia'
              Width = 28
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Valor'
              Font.Charset = ANSI_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsBold]
              Width = 72
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'VigenDtIni'
              Title.Caption = 'Dt vig'#234'n.ini'
              Width = 60
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'VigenDtFim'
              Title.Caption = 'Dt vig'#234'n.fim'
              Width = 60
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_INT'
              Title.Caption = 'Intermedi'#225'rio'
              Width = 120
              Visible = True
            end>
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 515
    Width = 1008
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 559
    Width = 1008
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 862
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 860
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object QrNFSeMenCab: TmySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrNFSeMenCabBeforeClose
    AfterScroll = QrNFSeMenCabAfterScroll
    SQL.Strings = (
      'SELECT nmc.*, nsc.Nome NO_SRV, '
      'car.Nome NO_CRT, cta.Nome NO_CTA,'
      'IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NO_CLI,'
      'IF(ntm.Tipo=0, ntm.RazaoSocial, ntm.Nome) NO_INT'
      'FROM nfsemencab nmc'
      'LEFT JOIN entidades cli ON cli.Codigo=nmc.Cliente '
      'LEFT JOIN entidades ntm ON ntm.Codigo=nmc.Intermed'
      'LEFT JOIN nfsesrvcad nsc ON nsc.Codigo=nmc.NFSeSrvCad'
      'LEFT JOIN carteiras car ON car.Codigo=nmc.GLCarteira'
      'LEFT JOIN contas cta ON cta.Codigo=nmc.GLConta')
    Left = 328
    Top = 216
    object QrNFSeMenCabCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'nfsemencab.Codigo'
    end
    object QrNFSeMenCabEmpresa: TIntegerField
      FieldName = 'Empresa'
      Origin = 'nfsemencab.Empresa'
    end
    object QrNFSeMenCabCliente: TIntegerField
      FieldName = 'Cliente'
      Origin = 'nfsemencab.Cliente'
    end
    object QrNFSeMenCabContrato: TIntegerField
      FieldName = 'Contrato'
      Origin = 'nfsemencab.Contrato'
    end
    object QrNFSeMenCabIntermed: TIntegerField
      FieldName = 'Intermed'
      Origin = 'nfsemencab.Intermed'
    end
    object QrNFSeMenCabNFSeSrvCad: TIntegerField
      FieldName = 'NFSeSrvCad'
      Origin = 'nfsemencab.NFSeSrvCad'
    end
    object QrNFSeMenCabDiaFat: TSmallintField
      FieldName = 'DiaFat'
      Origin = 'nfsemencab.DiaFat'
    end
    object QrNFSeMenCabIncreCompet: TSmallintField
      FieldName = 'IncreCompet'
      Origin = 'nfsemencab.IncreCompet'
    end
    object QrNFSeMenCabValor: TFloatField
      FieldName = 'Valor'
      Origin = 'nfsemencab.Valor'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrNFSeMenCabGLGerarLct: TSmallintField
      FieldName = 'GLGerarLct'
      Origin = 'nfsemencab.GLGerarLct'
    end
    object QrNFSeMenCabGLCarteira: TIntegerField
      FieldName = 'GLCarteira'
      Origin = 'nfsemencab.GLCarteira'
    end
    object QrNFSeMenCabGLConta: TIntegerField
      FieldName = 'GLConta'
      Origin = 'nfsemencab.GLConta'
    end
    object QrNFSeMenCabVigenDtIni: TDateField
      FieldName = 'VigenDtIni'
      Origin = 'nfsemencab.VigenDtIni'
    end
    object QrNFSeMenCabVigenDtFim: TDateField
      FieldName = 'VigenDtFim'
      Origin = 'nfsemencab.VigenDtFim'
    end
    object QrNFSeMenCabLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'nfsemencab.Lk'
    end
    object QrNFSeMenCabDataCad: TDateField
      FieldName = 'DataCad'
      Origin = 'nfsemencab.DataCad'
    end
    object QrNFSeMenCabDataAlt: TDateField
      FieldName = 'DataAlt'
      Origin = 'nfsemencab.DataAlt'
    end
    object QrNFSeMenCabUserCad: TIntegerField
      FieldName = 'UserCad'
      Origin = 'nfsemencab.UserCad'
    end
    object QrNFSeMenCabUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Origin = 'nfsemencab.UserAlt'
    end
    object QrNFSeMenCabAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Origin = 'nfsemencab.AlterWeb'
    end
    object QrNFSeMenCabAtivo: TSmallintField
      FieldName = 'Ativo'
      Origin = 'nfsemencab.Ativo'
    end
    object QrNFSeMenCabNO_SRV: TWideStringField
      FieldName = 'NO_SRV'
      Origin = 'nfsesrvcad.Nome'
      Size = 255
    end
    object QrNFSeMenCabNO_CRT: TWideStringField
      FieldName = 'NO_CRT'
      Origin = 'carteiras.Nome'
      Size = 100
    end
    object QrNFSeMenCabNO_CTA: TWideStringField
      FieldName = 'NO_CTA'
      Origin = 'contas.Nome'
      Size = 50
    end
    object QrNFSeMenCabNO_CLI: TWideStringField
      FieldName = 'NO_CLI'
      Size = 100
    end
    object QrNFSeMenCabNO_INT: TWideStringField
      FieldName = 'NO_INT'
      Size = 100
    end
    object QrNFSeMenCabGBGerarBol: TSmallintField
      FieldName = 'GBGerarBol'
      Origin = 'nfsemencab.GBGerarBol'
    end
    object QrNFSeMenCabSN_LCT_TXT: TWideStringField
      FieldName = 'SN_LCT_TXT'
      Size = 6
    end
    object QrNFSeMenCabSN_BOL_TXT: TWideStringField
      FieldName = 'SN_BOL_TXT'
      Size = 6
    end
  end
  object DsNFSeMenCab: TDataSource
    DataSet = QrNFSeMenCab
    Left = 328
    Top = 264
  end
end
