unit NFSeFatCab;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2, dmkGeral,
  ResIntStrings, UnMLAGeral, UnGOTOy, UnInternalConsts, UnMsgInt, UMySQLModule,
  UnInternalConsts2, mySQLDbTables, UnMySQLCuringa, dmkPermissoes, dmkEdit,
  dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup, Menus, Grids, DBGrids,
  UnDmkProcFunc, dmkDBLookupComboBox, dmkEditCB, dmkDBGrid,
  // ini 2023-12-20
  //DMKpnfsConversao,
  // fim 2023-12-20
  ComCtrls, dmkCompoStore, UnDmkEnums;

type
  TFmNFSeFatCab = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    Label9: TLabel;
    EdNome: TdmkEdit;
    Label1: TLabel;
    DBEdCodigo: TdmkDBEdit;
    Label2: TLabel;
    DBEdNome: TdmkDBEdit;
    QrNFSeFatCab: TmySQLQuery;
    DsNFSeFatCab: TDataSource;
    PMIts: TPopupMenu;
    ItsInclui1: TMenuItem;
    ItsExclui1: TMenuItem;
    ItsAltera1: TMenuItem;
    PMCab: TPopupMenu;
    CabInclui1: TMenuItem;
    CabAltera1: TMenuItem;
    CabExclui1: TMenuItem;
    BtCab: TBitBtn;
    BtIts: TBitBtn;
    Label3: TLabel;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    LaMes: TLabel;
    EdCompetenci: TdmkEdit;
    QrNFSeFatCabCodigo: TIntegerField;
    QrNFSeFatCabNome: TWideStringField;
    QrNFSeFatCabEmpresa: TIntegerField;
    QrNFSeFatCabDataFatIni: TDateField;
    QrNFSeFatCabHoraFatIni: TTimeField;
    QrNFSeFatCabDataFatFim: TDateField;
    QrNFSeFatCabHoraFatFim: TTimeField;
    QrNFSeFatCabCompetenci: TIntegerField;
    QrNFSeFatCabNO_EMP: TWideStringField;
    Label4: TLabel;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    Label5: TLabel;
    DBEdit3: TDBEdit;
    QrNFSeFatCabTXT_FINAL: TWideStringField;
    QrNFSeFatCabTXT_INICIO: TWideStringField;
    QrNFSeFatCabTXT_COMPETENCI: TWideStringField;
    Label6: TLabel;
    DBEdit4: TDBEdit;
    Label8: TLabel;
    DBEdit5: TDBEdit;
    QrNFSeFatCabFilial: TIntegerField;
    QrNFSeDPSCab: TmySQLQuery;
    QrNFSeDPSCabCodigo: TIntegerField;
    QrNFSeDPSCabAmbiente: TSmallintField;
    QrNFSeDPSCabEmpresa: TIntegerField;
    QrNFSeDPSCabCliente: TIntegerField;
    QrNFSeDPSCabIntermediario: TIntegerField;
    QrNFSeDPSCabNFSeSrvCad: TIntegerField;
    QrNFSeDPSCabRpsID: TWideStringField;
    QrNFSeDPSCabRpsIDNumero: TIntegerField;
    QrNFSeDPSCabRpsIDSerie: TWideStringField;
    QrNFSeDPSCabRpsIDTipo: TSmallintField;
    QrNFSeDPSCabRpsDataEmissao: TDateField;
    QrNFSeDPSCabRpsHoraEmissao: TTimeField;
    QrNFSeDPSCabRpsStatus: TIntegerField;
    QrNFSeDPSCabCompetencia: TDateField;
    QrNFSeDPSCabSubstNumero: TIntegerField;
    QrNFSeDPSCabSubstSerie: TWideStringField;
    QrNFSeDPSCabSubstTipo: TSmallintField;
    QrNFSeDPSCabValorServicos: TFloatField;
    QrNFSeDPSCabValorDeducoes: TFloatField;
    QrNFSeDPSCabValorPis: TFloatField;
    QrNFSeDPSCabValorCofins: TFloatField;
    QrNFSeDPSCabValorInss: TFloatField;
    QrNFSeDPSCabValorIr: TFloatField;
    QrNFSeDPSCabValorCsll: TFloatField;
    QrNFSeDPSCabOutrasRetencoes: TFloatField;
    QrNFSeDPSCabValorIss: TFloatField;
    QrNFSeDPSCabAliquota: TFloatField;
    QrNFSeDPSCabDescontoIncondicionado: TFloatField;
    QrNFSeDPSCabDescontoCondicionado: TFloatField;
    QrNFSeDPSCabIssRetido: TSmallintField;
    QrNFSeDPSCabResponsavelRetencao: TSmallintField;
    QrNFSeDPSCabItemListaServico: TWideStringField;
    QrNFSeDPSCabCodigoCnae: TWideStringField;
    QrNFSeDPSCabCodigoTributacaoMunicipio: TWideStringField;
    QrNFSeDPSCabDiscriminacao: TWideMemoField;
    QrNFSeDPSCabCodigoMunicipio: TIntegerField;
    QrNFSeDPSCabCodigoPais: TIntegerField;
    QrNFSeDPSCabExigibilidadeISS: TSmallintField;
    QrNFSeDPSCabMunicipioIncidencia: TIntegerField;
    QrNFSeDPSCabNumeroProcesso: TWideStringField;
    QrNFSeDPSCabPrestaCpf: TWideStringField;
    QrNFSeDPSCabPrestaCnpj: TWideStringField;
    QrNFSeDPSCabPrestaInscricaoMunicipal: TWideStringField;
    QrNFSeDPSCabTomaCpf: TWideStringField;
    QrNFSeDPSCabTomaCnpj: TWideStringField;
    QrNFSeDPSCabTomaInscricaoMunicipal: TWideStringField;
    QrNFSeDPSCabTomaRazaoSocial: TWideStringField;
    QrNFSeDPSCabTomaEndereco: TWideStringField;
    QrNFSeDPSCabTomaNumero: TWideStringField;
    QrNFSeDPSCabTomaComplemento: TWideStringField;
    QrNFSeDPSCabTomaBairro: TWideStringField;
    QrNFSeDPSCabTomaCodigoMunicipio: TIntegerField;
    QrNFSeDPSCabTomaUf: TWideStringField;
    QrNFSeDPSCabTomaCodigoPais: TIntegerField;
    QrNFSeDPSCabTomaCep: TIntegerField;
    QrNFSeDPSCabIntermeCpf: TWideStringField;
    QrNFSeDPSCabIntermeCnpj: TWideStringField;
    QrNFSeDPSCabIntermeInscricaoMunicipal: TWideStringField;
    QrNFSeDPSCabIntermeRazaoSocial: TWideStringField;
    QrNFSeDPSCabConstrucaoCivilCodigoObra: TWideStringField;
    QrNFSeDPSCabConstrucaoCivilArt: TWideStringField;
    QrNFSeDPSCabRegimeEspecialTributacao: TSmallintField;
    QrNFSeDPSCabOptanteSimplesNacional: TSmallintField;
    QrNFSeDPSCabIncentivoFiscal: TSmallintField;
    QrNFSeDPSCabId: TWideStringField;
    QrNFSeDPSCabLk: TIntegerField;
    QrNFSeDPSCabDataCad: TDateField;
    QrNFSeDPSCabDataAlt: TDateField;
    QrNFSeDPSCabUserCad: TIntegerField;
    QrNFSeDPSCabUserAlt: TIntegerField;
    QrNFSeDPSCabAlterWeb: TSmallintField;
    QrNFSeDPSCabAtivo: TSmallintField;
    QrNFSeDPSCabStatus: TIntegerField;
    QrNFSeDPSCabLastLRpsC: TIntegerField;
    QrNFSeDPSCabQuemPagaISS: TSmallintField;
    QrNFSeDPSCabNaturezaOperacao: TIntegerField;
    QrNFSeDPSCabTomaContatoTelefone: TWideStringField;
    QrNFSeDPSCabTomaContatoEmail: TWideStringField;
    QrNFSeDPSCabTomaNomeFantasia: TWideStringField;
    DsNFSeDPSCab: TDataSource;
    DGDados: TdmkDBGrid;
    QrNFSeDPSCabNFSeFatCab: TIntegerField;
    QrNFSeDPSCabNO_CLI: TWideStringField;
    QrNFSeDPSCabNO_INT: TWideStringField;
    BtRPS: TBitBtn;
    QrEmitir: TmySQLQuery;
    QrEmitirCodigo: TIntegerField;
    QrEmitirAmbiente: TSmallintField;
    QrEmitirEmpresa: TIntegerField;
    QrEmitirCliente: TIntegerField;
    QrEmitirIntermediario: TIntegerField;
    QrEmitirNFSeSrvCad: TIntegerField;
    QrEmitirRpsID: TWideStringField;
    QrEmitirRpsIDNumero: TIntegerField;
    QrEmitirRpsIDSerie: TWideStringField;
    QrEmitirRpsIDTipo: TSmallintField;
    QrEmitirRpsDataEmissao: TDateField;
    QrEmitirRpsHoraEmissao: TTimeField;
    QrEmitirRpsStatus: TIntegerField;
    QrEmitirCompetencia: TDateField;
    QrEmitirSubstNumero: TIntegerField;
    QrEmitirSubstSerie: TWideStringField;
    QrEmitirSubstTipo: TSmallintField;
    QrEmitirValorServicos: TFloatField;
    QrEmitirValorDeducoes: TFloatField;
    QrEmitirValorPis: TFloatField;
    QrEmitirValorCofins: TFloatField;
    QrEmitirValorInss: TFloatField;
    QrEmitirValorIr: TFloatField;
    QrEmitirValorCsll: TFloatField;
    QrEmitirOutrasRetencoes: TFloatField;
    QrEmitirValorIss: TFloatField;
    QrEmitirAliquota: TFloatField;
    QrEmitirDescontoIncondicionado: TFloatField;
    QrEmitirDescontoCondicionado: TFloatField;
    QrEmitirIssRetido: TSmallintField;
    QrEmitirResponsavelRetencao: TSmallintField;
    QrEmitirItemListaServico: TWideStringField;
    QrEmitirCodigoCnae: TWideStringField;
    QrEmitirCodigoTributacaoMunicipio: TWideStringField;
    QrEmitirDiscriminacao: TWideMemoField;
    QrEmitirCodigoMunicipio: TIntegerField;
    QrEmitirCodigoPais: TIntegerField;
    QrEmitirExigibilidadeISS: TSmallintField;
    QrEmitirMunicipioIncidencia: TIntegerField;
    QrEmitirNumeroProcesso: TWideStringField;
    QrEmitirPrestaCpf: TWideStringField;
    QrEmitirPrestaCnpj: TWideStringField;
    QrEmitirPrestaInscricaoMunicipal: TWideStringField;
    QrEmitirTomaCpf: TWideStringField;
    QrEmitirTomaCnpj: TWideStringField;
    QrEmitirTomaInscricaoMunicipal: TWideStringField;
    QrEmitirTomaRazaoSocial: TWideStringField;
    QrEmitirTomaEndereco: TWideStringField;
    QrEmitirTomaNumero: TWideStringField;
    QrEmitirTomaComplemento: TWideStringField;
    QrEmitirTomaBairro: TWideStringField;
    QrEmitirTomaCodigoMunicipio: TIntegerField;
    QrEmitirTomaUf: TWideStringField;
    QrEmitirTomaCodigoPais: TIntegerField;
    QrEmitirTomaCep: TIntegerField;
    QrEmitirIntermeCpf: TWideStringField;
    QrEmitirIntermeCnpj: TWideStringField;
    QrEmitirIntermeInscricaoMunicipal: TWideStringField;
    QrEmitirIntermeRazaoSocial: TWideStringField;
    QrEmitirConstrucaoCivilCodigoObra: TWideStringField;
    QrEmitirConstrucaoCivilArt: TWideStringField;
    QrEmitirRegimeEspecialTributacao: TSmallintField;
    QrEmitirOptanteSimplesNacional: TSmallintField;
    QrEmitirIncentivoFiscal: TSmallintField;
    QrEmitirId: TWideStringField;
    QrEmitirLk: TIntegerField;
    QrEmitirDataCad: TDateField;
    QrEmitirDataAlt: TDateField;
    QrEmitirUserCad: TIntegerField;
    QrEmitirUserAlt: TIntegerField;
    QrEmitirAlterWeb: TSmallintField;
    QrEmitirAtivo: TSmallintField;
    QrEmitirStatus: TIntegerField;
    QrEmitirLastLRpsC: TIntegerField;
    QrEmitirQuemPagaISS: TSmallintField;
    QrEmitirNaturezaOperacao: TIntegerField;
    QrEmitirTomaContatoTelefone: TWideStringField;
    QrEmitirTomaContatoEmail: TWideStringField;
    QrEmitirTomaNomeFantasia: TWideStringField;
    QrEmitirNFSeFatCab: TIntegerField;
    QrEmitirNO_CLI: TWideStringField;
    QrEmitirNO_INT: TWideStringField;
    QrNFSeFatCabAmbiente: TIntegerField;
    PMImprime: TPopupMenu;
    odasNFSeemitidas1: TMenuItem;
    NFSedaDPSatual1: TMenuItem;
    NFSedasDPSSelecionadas1: TMenuItem;
    SbEmail: TBitBtn;
    PB1: TProgressBar;
    QrNFSeCodVerifi: TmySQLQuery;
    QrNFSeCodVerifiNfsCodigoVerificacao: TWideStringField;
    CSTabSheetChamou: TdmkCompoStore;
    LaTotal: TLabel;
    QrNFSeDPSCabNfsNumero: TLargeintField;
    QrNFSeDPSCabFatID: TIntegerField;
    QrNFSeDPSCabLancto: TIntegerField;
    QrNFSeDPSCabGLGerarLct: TSmallintField;
    QrNFSeDPSCabGLCarteira: TIntegerField;
    QrNFSeDPSCabGLConta: TIntegerField;
    QrNFSeDPSCabGLConta_TXT: TWideStringField;
    N1: TMenuItem;
    Visualizaconfirmaoderecebimentodafaturaatual1: TMenuItem;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrNFSeFatCabAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrNFSeFatCabBeforeOpen(DataSet: TDataSet);
    procedure SbNovoClick(Sender: TObject);
    procedure QrNFSeFatCabAfterScroll(DataSet: TDataSet);
    procedure CabInclui1Click(Sender: TObject);
    procedure CabAltera1Click(Sender: TObject);
    procedure CabExclui1Click(Sender: TObject);
    procedure BtCabClick(Sender: TObject);
    procedure BtItsClick(Sender: TObject);
    procedure ItsInclui1Click(Sender: TObject);
    procedure ItsExclui1Click(Sender: TObject);
    procedure ItsAltera1Click(Sender: TObject);
    procedure PMCabPopup(Sender: TObject);
    procedure PMItsPopup(Sender: TObject);
    procedure QrNFSeFatCabCalcFields(DataSet: TDataSet);
    procedure BtRPSClick(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
    procedure odasNFSeemitidas1Click(Sender: TObject);
    procedure NFSedaDPSatual1Click(Sender: TObject);
    procedure NFSedasDPSSelecionadas1Click(Sender: TObject);
    procedure DGDadosDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure SbEmailClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure QrNFSeDPSCabAfterScroll(DataSet: TDataSet);
    procedure QrNFSeDPSCabBeforeClose(DataSet: TDataSet);
    procedure Visualizaconfirmaoderecebimentodafaturaatual1Click(
      Sender: TObject);
  private
    FNFSeMenCab: String;
    //
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    procedure MostraNFSeFatAdd(SQLType: TSQLType);
    procedure InsereLanctosFinanceiros();
    procedure AtualizaLanctosFinanceiros();
  public
    { Public declarations }
    FSeq, FCabIni: Integer;
    FLocIni: Boolean;
    FTbLctA: String;
    //
    procedure ReopenNFSeDPSCab(Codigo: Integer);
    procedure LocCod(Atual, Codigo: Integer);
  end;

var
  FmNFSeFatCab: TFmNFSeFatCab;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, MyDBCheck, DmkDAC_PF, ModuleGeral, NFSe_Create,
  NFSeFatAdd, NFSe_PF_0201, NFSe_0000_Module, MyGlyfs, Principal, NFSe_PF_0000,
  UnGrl_Geral;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmNFSeFatCab.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmNFSeFatCab.MostraNFSeFatAdd(SQLType: TSQLType);
begin
  FNFSeMenCab := UnNFSe_Create.RecriaTempTableNovo(ntrtt_NFSeMenCab, DModG.QrUpdPID1, False);
  //
  {$IfNDef sCNTR}
    UMyMod.ExecutaMySQLQuery1(DModG.QrUpdPID1, [
      'DELETE FROM ' + FNFSeMenCab + '; ',
      'INSERT INTO ' + FNFSeMenCab,
      'SELECT cab.Codigo, ',
      'cab.Empresa, cab.Cliente, cab.Contrato, ',
      'cab.Intermed, cab.NFSeSrvCad, cab.DiaFat, ',
      'cab.IncreCompet, ',
      'IF(cab.Contrato=0, cab.Valor, con.ValorMes) Valor, cab.GLGerarLct, ',
      'cab.GLCarteira, cab.GLConta, cab.VigenDtIni, ',
      'cab.VigenDtFim, cab.GBGerarBol, 1 Ativo ',
      'FROM ' + TMeuDB + '.nfsemencab cab ',
      'LEFT JOIN ' + TMeuDB + '.contratos con ON con.Codigo = cab.Contrato ',
      'WHERE cab.Empresa=' + Geral.FF0(QrNFSeFatCabEmpresa.Value),
      'AND Date_Add(SYSDATE(), Interval cab.IncreCompet Month) >= cab.VigenDtIni ',
      'AND ( ',
      '     (Date_Add(SYSDATE(), Interval cab.IncreCompet Month) <= cab.VigenDtFim) ',
      '     OR ',
      '     (cab.VigenDtFim < "1900-01-01") ',
      ') ',
      'AND con.Status NOT IN (0,1,4) ',
      'AND con.DtaCntrFim < "1900-01-01" ',
      'AND cab.Ativo = 1 ',
      'AND cab.Codigo NOT IN ',
      '( ',
      'SELECT NfseMenCab ',
      'FROM ' + TMeuDB + '.nfsedpscab ',
      'WHERE NFSeFatCab=' + Geral.FF0(QrNFSeFatCabCodigo.Value),
      ') ',
      '']);
  {$Else}
    UMyMod.ExecutaMySQLQuery1(DModG.QrUpdPID1, [
      'DELETE FROM ' + FNFSeMenCab + '; ',
      'INSERT INTO ' + FNFSeMenCab,
      'SELECT Codigo, ',
      'Empresa, Cliente, Contrato, ',
      'Intermed, NFSeSrvCad, DiaFat, ',
      'IncreCompet, Valor, GLGerarLct, ',
      'GLCarteira, GLConta, VigenDtIni, ',
      'VigenDtFim, GBGerarBol, ',
      '1 Ativo ',
      'FROM ' + TMeuDB + '.nfsemencab ',
      'WHERE Empresa=' + Geral.FF0(QrNFSeFatCabEmpresa.Value),
      'AND cab.Ativo = 1 ',
      'AND Date_Add(SYSDATE(), Interval IncreCompet Month) >= VigenDtIni ',
      'AND ( ',
      '     (Date_Add(SYSDATE(), Interval IncreCompet Month) <= VigenDtFim) ',
      '     OR ',
      '     (VigenDtFim < "1900-01-01") ',
      ') ',
      'AND Codigo NOT IN ',
      '( ',
      'SELECT NfseMenCab ',
      'FROM ' + TMeuDB + '.nfsedpscab ',
      'WHERE NFSeFatCab=' + Geral.FF0(QrNFSeFatCabCodigo.Value),
      ') ',
      '']);
  {$EndIf}
  //
  if DBCheck.CriaFm(TFmNFSeFatAdd, FmNFSeFatAdd, afmoNegarComAviso) then
  begin
    FmNFSeFatAdd.FNFSeMenCab := FNFSeMenCab;
    FmNFSeFatAdd.FNFSeFatCab := QrNFSeFatCabCodigo.Value;
    FmNFSeFatAdd.ReopenNFSeDPSCab(0);
    //
    FmNFSeFatAdd.ShowModal;
    ReopenNFSeDPSCab(FmNFSeFatAdd.FLastDPS);
    FmNFSeFatAdd.Destroy;
    //
  end;
end;

procedure TFmNFSeFatCab.NFSedaDPSatual1Click(Sender: TObject);
begin
  DmNFSe_0000.ImpressaoNFSe_PorRPS_Uni(QrNFSeDPSCabEmpresa.Value,
    QrNFSeDPSCabAmbiente.Value, QrNFSeDPSCabRpsIDSerie.Value,
    QrNFSeDPSCabRpsIDTipo.Value, QrNFSeDPSCabRpsIDNumero.Value);
end;

procedure TFmNFSeFatCab.NFSedasDPSSelecionadas1Click(Sender: TObject);
var
  Ser: TArrStr;
  Tip, Num: TArrInt;
  I, N: Integer;
begin
  N := DGDados.SelectedRows.Count;
  if N < 2 then
    NFSedaDPSatual1Click(Self)
  else begin
    SetLength(Ser, N);
    SetLength(Tip, N);
    SetLength(Num, N);
    //
    with DGDados.DataSource.DataSet do
    for I := 0 to N - 1 do
    begin
      GotoBookmark(DGDados.SelectedRows.Items[I]);
      //
      {
      Ser[QrNFSeDPSCab.RecNo -1] := QrNFSeDPSCabRpsIDSerie.Value;
      Tip[QrNFSeDPSCab.RecNo -1] := QrNFSeDPSCabRpsIDTipo.Value;
      Num[QrNFSeDPSCab.RecNo -1] := QrNFSeDPSCabRpsIDNumero.Value;
      }
      Ser[I] := QrNFSeDPSCabRpsIDSerie.Value;
      Tip[I] := QrNFSeDPSCabRpsIDTipo.Value;
      Num[I] := QrNFSeDPSCabRpsIDNumero.Value;
    end;
    DmNFSe_0000.ImpressaoNFSe_PorRPS_Mul(QrNFSeDPSCabEmpresa.Value,
      QrNFSeDPSCabAmbiente.Value, Ser, Tip, Num);
  end;
end;

procedure TFmNFSeFatCab.odasNFSeemitidas1Click(Sender: TObject);
var
  Ser: TArrStr;
  Tip, Num: TArrInt;
begin
  SetLength(Ser, QrNFSeDPSCab.RecordCount);
  SetLength(Tip, QrNFSeDPSCab.RecordCount);
  SetLength(Num, QrNFSeDPSCab.RecordCount);
  QrNFSeDPSCab.DisableControls;
  try
    QrNFSeDPSCab.First;
    while not QrNFSeDPSCab.Eof do
    begin
      Ser[QrNFSeDPSCab.RecNo -1] := QrNFSeDPSCabRpsIDSerie.Value;
      Tip[QrNFSeDPSCab.RecNo -1] := QrNFSeDPSCabRpsIDTipo.Value;
      Num[QrNFSeDPSCab.RecNo -1] := QrNFSeDPSCabRpsIDNumero.Value;
      //
      QrNFSeDPSCab.Next;
    end;
    DmNFSe_0000.ImpressaoNFSe_PorRPS_Mul(QrNFSeDPSCabEmpresa.Value,
      QrNFSeDPSCabAmbiente.Value, Ser, Tip, Num);
  finally
    QrNFSeDPSCab.EnableControls;
  end;
end;

procedure TFmNFSeFatCab.PMCabPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemCabUpd(CabAltera1, QrNFSeFatCab);
  MyObjects.HabilitaMenuItemCabDel(CabExclui1, QrNFSeFatCab, QrNFSeDPSCab);
end;

procedure TFmNFSeFatCab.PMItsPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(ItsInclui1, QrNFSeFatCab);
  MyObjects.HabilitaMenuItemItsUpd(ItsAltera1, QrNFSeDPSCab);
  MyObjects.HabilitaMenuItemItsDel(ItsExclui1, QrNFSeDPSCab);
  MyObjects.HabilitaMenuItemItsDel(Visualizaconfirmaoderecebimentodafaturaatual1, QrNFSeDPSCab);
end;

procedure TFmNFSeFatCab.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrNFSeFatCabCodigo.Value, LaRegistro.Caption[2]);
end;
procedure TFmNFSeFatCab.Visualizaconfirmaoderecebimentodafaturaatual1Click(
  Sender: TObject);
begin
  UnNFSe_PF_0000.MostraFormNfse_ConfReceb(Self, QrNFSeDPSCabCliente.Value,
    QrNFSeDPSCabAmbiente.Value, QrNFSeDPSCabRpsIDNumero.Value,
    QrNFSeDPSCabRpsIDSerie.Value);
end;

/////////////////////////////////////////////////////////////////////////////////////

procedure TFmNFSeFatCab.DefParams;
begin
  VAR_GOTOTABELA := 'nfsefatcab';
  VAR_GOTOMYSQLTABLE := QrNFSeFatCab;
  VAR_GOTONEG := gotoPiZ;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT nfc.*, emp.Filial, ');
  VAR_SQLx.Add('IF(emp.Tipo=0, emp.RazaoSocial, emp.Nome) NO_EMP');
  VAR_SQLx.Add('FROM nfsefatcab nfc');
  VAR_SQLx.Add('LEFT JOIN entidades emp ON emp.Codigo=nfc.Empresa');
  VAR_SQLx.Add('WHERE nfc.Codigo > 0');
  VAR_SQLx.Add('AND nfc.Empresa IN (' + VAR_LIB_EMPRESAS + ')');
  //
  VAR_SQL1.Add('AND nfc.Codigo=:P0');
  //
  //VAR_SQL2.Add('AND CodUsu=:P0');
  //
  VAR_SQLa.Add('AND nfc.Nome LIKE :P0');
  //
  VAR_GOTOVAR1 := 'Codigo IN (SELECT Codigo FROM nfsefatcab WHERE Empresa IN (' + VAR_LIB_EMPRESAS + '))';
end;

procedure TFmNFSeFatCab.DGDadosDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
  DmNFSe_0000.ColoreStatusNFeEmDBGrid(TDBGrid(DGDados), Rect, DataCol, Column,
    State, Trunc(QrNFSeDPSCabStatus.Value));
end;

procedure TFmNFSeFatCab.InsereLanctosFinanceiros;
begin
  if (QrNFSeDPSCab.State <> dsInactive) and (QrNFSeDPSCab.RecordCount > 0) then
  begin
    try
      QrNFSeDPSCab.DisableControls;
      DGDados.Enabled := False;
      //
      PB1.Position := 0;
      PB1.Max      := QrNFSeDPSCab.RecordCount;
      //
      MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Inserindo lan�amentos financeiros');
      //
      if FTbLctA = '' then
      begin
        Geral.MB_Erro('Falha ao obter tabela de lan�amentos financeiros!');
        Exit;
      end;
      //
      QrNFSeDPSCab.First;
      while not QrNFSeDPSCab.EOF do
      begin
        if (QrNFSeDPSCabGLGerarLct.Value <> 0) and
          (QrNFSeDPSCabGLCarteira.Value <> 0) and
          (QrNFSeDPSCabGLConta.Value <> 0) and
          (QrNFSeDPSCabLancto.Value = 0) and
          (QrNFSeDPSCabStatus.Value in [100, 101]) then
        begin
          //Atualiza notas geradas atrav�s de emiss�o mensal (sem boleto)
          UnNFSe_PF_0000.InsereLancto(QrNFSeDPSCabRpsDataEmissao.Value,
            QrNFSeDPSCabNfsNumero.Value, QrNFSeDPSCabValorServicos.Value,
            QrNFSeDPSCabEmpresa.Value, QrNFSeDPSCabCliente.Value,
            QrNFSeFatCabCompetenci.Value, QrNFSeDPSCabGLCarteira.Value,
            QrNFSeDPSCabGLConta.Value, QrNFSeDPSCabCodigo.Value,
            QrNFSeDPSCabRpsIDSerie.Value, QrNFSeDPSCabGLConta_TXT.Value, FTbLctA);
        end else
        if (QrNFSeDPSCabFatID.Value = VAR_FATID_0202) and
          (QrNFSeDPSCabLancto.Value <> 0) and
          (QrNFSeDPSCabStatus.Value in [100, 101]) then
        begin
          //Atualiza notas geradas atrav�s de boletos (sem emiss�o mensal)
          UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
            'UPDATE ' + FTbLctA + ' SET ',
            'SerieNF=' + QrNFSeDPSCabRpsIDSerie.Value + ', ',
            'NotaFiscal=' + Geral.FF0(QrNFSeDPSCabNfsNumero.Value),
            'WHERE Controle=' + Geral.FF0(QrNFSeDPSCabLancto.Value),
            '']);
        end;
        PB1.Position := PB1.Position + 1;
        PB1.Update;
        Application.ProcessMessages;
        //
        QrNFSeDPSCab.Next;
      end;
    finally
      MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
      //
      PB1.Position := 0;
      //
      DGDados.Enabled := True;
      QrNFSeDPSCab.EnableControls;
    end;
  end;
  ReopenNFSeDPSCab(0);
end;

procedure TFmNFSeFatCab.ItsAltera1Click(Sender: TObject);
const
  Prestador     = 0;
  Tomador       = 0;
  Intermediario = 0;
  MeuServico    = 0;
  ItemListSrv   = 0;
  Discriminacao = '';
  Valor         = 0;
  GeraNFSe      = False;
  Servico       = fgnLoteRPS;
  //
  TabedForm = nil;
var
  DPS, NFSeFatCab: Integer;
  SerieNF: String;
  NumNF: Integer;
begin
  NFSeFatCab := QrNFSeFatCabCodigo.Value;
  DPS := QrNFSeDPSCabCodigo.Value;
  //
  UnNFSe_PF_0201.MostraFormNFSe(stUpd,
  Prestador, Tomador, Intermediario, MeuServico, ItemListSrv,
  Discriminacao, GeraNFSe, NFSeFatCab, DPS, Servico, QrNFSeDPSCab,
  Valor, SerieNF, NumNF, TabedForm);
end;

procedure TFmNFSeFatCab.CabExclui1Click(Sender: TObject);
var
  Qry: TmySQLQuery;
  Codigo: Integer;
begin
  if (QrNFSeFatCab.State <> dsInactive) and (QrNFSeFatCab.RecordCount > 0) then
  begin
    Qry := TmySQLQuery.Create(TDataModule(Dmod.MyDB.Owner));
    try
      Codigo := QrNFSeFatCabCodigo.Value;
      //
      UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
        'SELECT * ',
        'FROM nfsedpscab ',
        'WHERE NFSeFatCab=' + Geral.FF0(Codigo),
        '']);
      if Qry.RecordCount =  0 then
      begin
        if UMyMod.ExcluiRegistroInt1('Confirma a exclus�o do faturamento ID n�mero ' +
          Geral.FF0(Codigo) + '?', 'nfsefatcab', 'Codigo', Codigo, Dmod.MyDB) = ID_YES
        then
          Va(vpLast);
      end;
    finally
      Qry.Free;
    end;
  end;
end;

procedure TFmNFSeFatCab.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmNFSeFatCab.QueryPrincipalAfterOpen;
begin
end;

procedure TFmNFSeFatCab.ItsExclui1Click(Sender: TObject);
var
  Codigo, i: Integer;
begin
  if DGDados.SelectedRows.Count > 1 then
  begin
    if Geral.MensagemBox('Confirma a exclus�o dos DPS"s selecionados?',
      'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) <> ID_YES
    then
      Exit;
    //
    with DGDados.DataSource.DataSet do
    for i:= 0 to DGDados.SelectedRows.Count-1 do
    begin
      GotoBookmark(DGDados.SelectedRows.Items[i]);
      try
        if QrNFSeDPSCabStatus.Value <= DmNFSe_0000.stepAdedLote() then
        begin
          UMyMod.ExcluiRegistroInt1('', 'NFSeDPSCab', 'Codigo',
            QrNFSeDPSCabCodigo.Value, Dmod.MyDB);
          AtualizaLanctosFinanceiros();
        end;
      except
        Geral.MB_Aviso('Falha ao excluir DPF n�mero ' +
          Geral.FF0(QrNFSeFatCabCodigo.Value) + '!');
      end;
    end;
    Codigo := GOTOy.LocalizaPriorNextIntQr(QrNFSeDPSCab,
                QrNFSeDPSCabCodigo, QrNFSeDPSCabCodigo.Value);
    ReopenNFSeDPSCab(Codigo);
  end else
  begin
    if QrNFSeDPSCabStatus.Value <= DmNFSe_0000.stepAdedLote() then
    begin
      if UMyMod.ExcluiRegistroInt1('Confirma a exclus�o do DPS selecionado?',
      'NFSeDPSCab', 'Codigo', QrNFSeDPSCabCodigo.Value, Dmod.MyDB) = ID_YES then
      begin
        AtualizaLanctosFinanceiros();
        Codigo := GOTOy.LocalizaPriorNextIntQr(QrNFSeDPSCab,
          QrNFSeDPSCabCodigo, QrNFSeDPSCabCodigo.Value);
        ReopenNFSeDPSCab(Codigo);
      end;
    end else
      Geral.MB_Aviso(
        'Status do RPS deve ser menor ou igual a 50 para poder ser exclu�do!');
  end;
end;

procedure TFmNFSeFatCab.ReopenNFSeDPSCab(Codigo: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrNFSeDPSCab, Dmod.MyDB, [
    'SELECT dps.*, nfs.NfsNumero, ',
    'IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NO_CLI, ',
    'IF(ntm.Tipo=0, ntm.RazaoSocial, ntm.Nome) NO_INT, ',
    'nmc.GLGerarLct, nmc.GLCarteira, nmc.GLConta, con.Nome GLConta_TXT ',
    'FROM nfsedpscab dps ',
    'LEFT JOIN nfsemencab nmc ON nmc.Codigo = dps.NfseMenCab ',
    'LEFT JOIN contas con ON con.Codigo = nmc.GLConta ',
    'LEFT JOIN nfsenfscab nfs ON (nfs.Empresa = dps.Empresa ',
    'AND nfs.Ambiente = dps.Ambiente ',
    'AND nfs.NfsRpsIDSerie = dps.RpsIDSerie ',
    'AND nfs.NfsRpsIDTipo = dps.RpsIDTipo  ',
    'AND nfs.NfsRpsIDNumero = dps.RpsIDNumero) ',
    'LEFT JOIN entidades cli ON cli.Codigo=dps.Cliente ',
    'LEFT JOIN entidades ntm ON ntm.Codigo=dps.Intermediario ',
    'WHERE NFSeFatCab=' + Geral.FF0(QrNFSeFatCabCodigo.Value),
    '']);
  if Codigo <> 0 then
    QrNFSeDPSCab.Locate('Codigo', Codigo, []);
end;


procedure TFmNFSeFatCab.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmNFSeFatCab.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmNFSeFatCab.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmNFSeFatCab.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmNFSeFatCab.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmNFSeFatCab.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrNFSeFatCabCodigo.Value;
  if TFmNFSeFatCab(Self).Owner is TApplication then
    Close
  else
    MyObjects.FormTDIFecha(TFmNFSeFatCab(Self), TTabSheet(CSTabSheetChamou.Component));
end;

procedure TFmNFSeFatCab.ItsInclui1Click(Sender: TObject);
begin
  MostraNFSeFatAdd(stIns);
end;

procedure TFmNFSeFatCab.CabAltera1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrNFSeFatCab, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'nfsefatcab');
end;

procedure TFmNFSeFatCab.BtConfirmaClick(Sender: TObject);
var
{
  DataFatIni, HoraFatIni, m: String;
  Competenci, Ambiente: Integer;
}
  Nome, Competen: String;
  Codigo, Empresa: Integer;
begin
  {
  if MyObjects.FIC(EdEmpresa.ValueVariant = 0, EdEmpresa, 'Informe a empresa!') then
    Exit;
  Empresa        := DModG.QrEmpresasCodigo.Value;
  Codigo         := EdCodigo.ValueVariant;
  Nome           := EdNome.ValueVariant;
  //if MyObjects.FIC(Length(Nome) = 0, EdNome, 'Defina uma descri��o!') then Exit;
  if ImgTipo.SQLType = stIns then
  begin
    DataFatIni     := Geral.FDT(Date, 1);
    HoraFatIni     := Geral.FDT(Time, 100);
  end else
  begin
    DataFatIni     := Geral.FDT(QrNFSeFatCabDataFatIni.Value, 1);
    HoraFatIni     := Geral.FDT(QrNFSeFatCabHoraFatIni.Value, 100);
  end;
  //DataFatFim     := ;
  //HoraFatFim     := ;
  m := EdCompetenci.Text;
  if Length(m) = 7 then
    Competenci := Geral.IMV(Copy(m, 4, 4) + Copy(m, 1, 2))
  else
    Competenci := 0;
  //
  if ImgTipo.SQLType = stIns then
  begin
    DModG.ReopenParamsEmp(Empresa);
    Ambiente := DmodG.QrParamsEmpNFSeAmbiente.Value
  end else
    Ambiente := QrNFSeFatCabAmbiente.Value;
  //
  Codigo := UMyMod.BPGS1I32(
    'nfsefatcab', 'Codigo', '', '', tsPos, ImgTipo.SQLType, Codigo);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'nfsefatcab', False, [
  'Nome', 'Empresa', 'DataFatIni',
  'HoraFatIni', (*'DataFatFim', 'HoraFatFim',*)
  'Competenci', 'Ambiente'], [
  'Codigo'], [
  Nome, Empresa, DataFatIni,
  HoraFatIni, (*DataFatFim, HoraFatFim,*)
  Competenci, Ambiente], [
  Codigo], True) then
  begin
    LocCod(Codigo, Codigo);
    PnDados.Visible := True;
    PnEdita.Visible := False;
    ImgTipo.SQLType := stLok;
    //
    if FSeq = 1 then Close;
  end;
  }
  if MyObjects.FIC(EdEmpresa.ValueVariant = 0, EdEmpresa, 'Informe a empresa!') then Exit;
  //
  Empresa  := DModG.QrEmpresasCodigo.Value;
  Codigo   := EdCodigo.ValueVariant;
  Nome     := EdNome.ValueVariant;
  Competen := EdCompetenci.Text;
  //
  if MyObjects.FIC(Competen = '', EdCompetenci, 'Informe o m�s!') then Exit;
  //
  Codigo := UnNFSe_PF_0201.InsUpdNFSeFatCab(Codigo, Empresa,
              QrNFSeFatCabAmbiente.Value, Competen, Nome,
              QrNFSeFatCabDataFatIni.Value, QrNFSeFatCabHoraFatIni.Value,
              ImgTipo.SQLType);
  //
  if Codigo <> 0 then
  begin
    LocCod(Codigo, Codigo);
    PnDados.Visible := True;
    PnEdita.Visible := False;
    ImgTipo.SQLType := stLok;
    //
    if FSeq = 1 then Close;
  end;
end;

procedure TFmNFSeFatCab.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := EdCodigo.ValueVariant;
  if ImgTipo.SQLType = stIns then
    UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'nfsefatcab', Codigo);
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'nfsefatcab', 'Codigo');
end;

procedure TFmNFSeFatCab.BtItsClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMIts, BtIts);
end;

procedure TFmNFSeFatCab.BtRPSClick(Sender: TObject);
var
  DPS: Integer;
  VerMsg: Boolean;
begin
  DPS    := 0;
  VerMsg := False;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrEmitir, Dmod.MyDB, [
    'SELECT dps.*, ',
    'IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NO_CLI, ',
    'IF(ntm.Tipo=0, ntm.RazaoSocial, ntm.Nome) NO_INT ',
    'FROM nfsedpscab dps ',
    'LEFT JOIN entidades cli ON cli.Codigo=dps.Cliente ',
    'LEFT JOIN entidades ntm ON ntm.Codigo=dps.Intermediario ',
    'WHERE RpsIDNumero=0 ',
    'AND NFSeFatCab=' + Geral.FF0(QrNFSeFatCabCodigo.Value),
    'ORDER BY Codigo ',
    '']);
  if QrEmitir.RecordCount > 0 then
  begin
    if Geral.MB_Pergunta('Confirma o envio de ' + Geral.FF0(QrEmitir.RecordCount)
      + ' DPSs ao Fisco para gera��o de NFS-es?') = ID_YES then
    begin
      Screen.Cursor := crHourGlass;
      try
        QrEmitir.DisableControls;
        PB1.Position := 0;
        PB1.Max      := QrEmitir.RecordCount;
        //
        VerMsg := VAR_VISUALIZAR_MSG_TDmkACBrNFSe;
        VAR_VISUALIZAR_MSG_TDmkACBrNFSe := False;
        QrEmitir.First;
        while not QrEmitir.Eof do
        begin
          DPS := QrEmitirCodigo.Value;
          if QrEmitirEmpresa.Value = QrNFSeDPSCabEmpresa.Value then
          begin
            MyObjects.Informa2(LaAviso1, LaAviso2, True,
              'Gerando NFS-e do RPS ' + Geral.FF0(QrEmitirCodigo.Value));
            //
            try
              if not UnNFSe_PF_0201.GeraNFseUnica(Self, DPS,
                QrEmitirEmpresa.Value, fgnLoteRPS, nil, LaAviso1, LaAviso2) then
              begin
                Geral.MB_Aviso('Falha ao gerar NFS-e!');
                Exit;
              end;
            except
              Geral.MB_Aviso('Falha ao converter DPS em NFS-e' + sLineBreak + 'Processo abortado!');
              Exit;
            end;
          end else
          begin
            raise Exception.Create('A DPS ' + Geral.FF0(DPS) +
            ' est� com a empresa inconsistente com o malote!');
            Exit;
          end;
          PB1.Position := PB1.Position + 1;
          PB1.Update;
          Application.ProcessMessages;
          //
          QrEmitir.Next;
        end;
        PB1.Position := 0;
        QrEmitir.EnableControls;
        //
        ReopenNFSeDPSCab(DPS);
        Screen.Cursor := crDefault;
        //
        MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
        //
        Geral.MB_Info('Envio finalizado!');
      except
        // Reabrir caso conseguiu emitir pelo menos uma NFSe
        VAR_VISUALIZAR_MSG_TDmkACBrNFSe := VerMsg;
        UnDmkDAC_PF.AbreQuery(QrNFSeDPSCab, Dmod.MyDB);
        Screen.Cursor := crDefault;
      end;
    end;
  end else
    Geral.MB_Aviso('N�o h� DPS neste malote para converter em NFS-e!');
  InsereLanctosFinanceiros;
end;

procedure TFmNFSeFatCab.AtualizaLanctosFinanceiros;
begin
  //Atualiza notas geradas atrav�s de boletos (sem emiss�o mensal)
  UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
    'UPDATE ' + FTbLctA + ' SET ',
    'SerieNF = "", ',
    'NotaFiscal = 0 ',
    'WHERE Controle=' + Geral.FF0(QrNFSeDPSCabLancto.Value),
    '']);
end;

procedure TFmNFSeFatCab.BtCabClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMCab, BtCab);
end;

procedure TFmNFSeFatCab.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  GBEdita.Align := alClient;
  DGDados.Align := alClient;
  CriaOForm;
  FSeq := 0;
  EdCompetenci.Texto := Geral.FDT(Date, 14);
  //
  CBEmpresa.ListSource   := DModG.DsEmpresas;
  EdEmpresa.ValueVariant := DmodG.QrFiliLogFilial.Value;
  CBEmpresa.KeyValue     := DmodG.QrFiliLogFilial.Value;
  //
  PB1.Visible       := True;
  DGDados.PopupMenu := PMIts;
end;

procedure TFmNFSeFatCab.SbNumeroClick(Sender: TObject);
begin
  DefParams; //<- evitar erro em form tabbed ou paneled! Colocar no in�cio da procedure!!!
  LaRegistro.Caption := GOTOy.Codigo(QrNFSeFatCabCodigo.Value, LaRegistro.Caption);
end;

procedure TFmNFSeFatCab.SbEmailClick(Sender: TObject);
{$IfNDef SemProtocolo}
{$IfDef TEM_DBWEB}
var
  Aba, Enab, Enab2: Boolean;
begin
  Enab  := (QrNFSeFatCab.State <> dsInactive) and (QrNFSeFatCab.RecordCount > 0);
  Enab2 := (QrNFSeDPSCab.State <> dsInactive) and (QrNFSeDPSCab.RecordCount > 0);
  //
  if Enab and Enab2 then
  begin
    if TFmNFSeFatCab(Self).Owner is TApplication then
      Aba := False
    else
      Aba := True;
    UnNFSe_PF_0000.MostraFormNFSeMail(Aba, FmPrincipal.PageControl1,
      FmPrincipal.AdvToolBarPagerNovo, QrNFSeFatCabEmpresa.Value,
      QrNFSeFatCabCodigo.Value, QrNFSeDPSCabCodigo.Value, False);
  end;
{$Else}
begin
  Geral.MB_Aviso('Para utilizar o envio de NFS-es protocolodas por lote o m�dulo WEB deve est� habilitado!');
{$EndIf}
{$Else}
begin
  Grl_Geral.InfoSemModulo(mdlappProtocolos);
{$EndIf}
end;

procedure TFmNFSeFatCab.SbImprimeClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMImprime, SbImprime);
end;

procedure TFmNFSeFatCab.SbNomeClick(Sender: TObject);
begin
  DefParams; //<- evitar erro em form tabbed ou paneled! Colocar no in�cio da procedure!!!
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmNFSeFatCab.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrNFSeFatCabCodigo.Value, LaRegistro.Caption);
end;

procedure TFmNFSeFatCab.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmNFSeFatCab.QrNFSeDPSCabAfterScroll(DataSet: TDataSet);
begin
  LaTotal.Caption := 'Total de itens: ' + Geral.FF0(QrNFSeDPSCab.RecordCount);
end;

procedure TFmNFSeFatCab.QrNFSeDPSCabBeforeClose(DataSet: TDataSet);
begin
  LaTotal.Caption := '';
end;

procedure TFmNFSeFatCab.QrNFSeFatCabAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmNFSeFatCab.QrNFSeFatCabAfterScroll(DataSet: TDataSet);
begin
  ReopenNFSeDPSCab(0);
end;

procedure TFmNFSeFatCab.FormActivate(Sender: TObject);
begin
  if TFmNFSeFatCab(Self).Owner is TApplication then
  begin
    MyObjects.CorIniComponente();
    //
    if FSeq = 1 then
      CabInclui1Click(Self)
    else
    if (not FLocIni) and (FCabIni <> 0)  then
    begin
      LocCod(FCabIni, FCabIni);
      if QrNFSeFatCabCodigo.Value <> FCabIni then Geral.MensagemBox(
      'Grupo n�o localizado!', 'Aviso', MB_OK+MB_ICONWARNING);
      FLocIni := True;
    end;
  end;
end;

procedure TFmNFSeFatCab.SbQueryClick(Sender: TObject);
begin
  DefParams; //<- evitar erro em form tabbed ou paneled! Colocar no in�cio da procedure!!!
  LocCod(QrNFSeFatCabCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'nfsefatcab', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmNFSeFatCab.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmNFSeFatCab.FormShow(Sender: TObject);
begin
  if not (TFmNFSeFatCab(Self).Owner is TApplication) then
  begin
    {$IfNDef cSkinRank} //Berlin
    {$IfNDef cAlphaSkin} //Berlin
      FmMyGlyfs.DefineGlyfs(TForm(Sender));
    {$EndIf}
    {$EndIf}
    {$IfDef cSkinRank} //Berlin
      if FmPrincipal.Sd1.Active then
        FmMyGlyfs.DefineGlyfsTDI(FmPrincipal.sd1, Sender);
    {$EndIf}
    {$IfDef cAlphaSkin} //Berlin
      if FmPrincipal.sSkinManager1.Active then
        FmMyGlyfs.DefineGlyfsTDI2(FmPrincipal.sSkinManager1, Sender);
    {$EndIf}
    //
    if FSeq = 1 then
      CabInclui1Click(Self)
    else
    if (not FLocIni) and (FCabIni <> 0)  then
    begin
      LocCod(FCabIni, FCabIni);
      if QrNFSeFatCabCodigo.Value <> FCabIni then Geral.MensagemBox(
      'Grupo n�o localizado!', 'Aviso', MB_OK+MB_ICONWARNING);
      FLocIni := True;
    end;
  end;

end;

procedure TFmNFSeFatCab.CabInclui1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrNFSeFatCab, [PnDados],
    [PnEdita], EdNome, ImgTipo, 'nfsefatcab');
  //
  DModG.ReopenEmpresas(VAR_USUARIO, 0, EdEmpresa, CBEmpresa);
  EdCompetenci.ValueVariant := Geral.FDT(DModG.ObtemAgora(), 14);
end;

procedure TFmNFSeFatCab.QrNFSeFatCabBeforeOpen(DataSet: TDataSet);
begin
  QrNFSeFatCabCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmNFSeFatCab.QrNFSeFatCabCalcFields(DataSet: TDataSet);
var
  m: String;
begin
  // 2013-07-28
  // Erro no XE2 > mySQLAccess.pas linha 895 quando Hora  � nula
  if QrNFSeFatCabDataFatIni.Value > 1 then
(*
  QrNFSeFatCabTXT_FINAL.Text :=
    Geral.FDT(QrNFSeFatCabDataFatFim.Value, 2) + ' ' +
    Geral.FDT(QrNFSeFatCabHoraFatFim.Value, 100);
*)
  if QrNFSeFatCabDataFatFim.Value > 1 then
  QrNFSeFatCabTXT_FINAL.Text :=
    Geral.FDT(QrNFSeFatCabDataFatFim.Value, 2) + ' ' +
    Geral.FDT(QrNFSeFatCabHoraFatFim.Value, 100)
  else
    QrNFSeFatCabTXT_FINAL.Text := '';
  // FIM 2013-07-28
  //
  if QrNFSeFatCabDataFatIni.Value > 1 then
  QrNFSeFatCabTXT_INICIO.Text :=
    Geral.FDT(QrNFSeFatCabDataFatIni.Value, 2) + ' ' +
    Geral.FDT(QrNFSeFatCabHoraFatIni.Value, 100)
  else
    QrNFSeFatCabTXT_INICIO.Text := '';
  //
  m := Geral.FFN(QrNFSeFatCabCompetenci.Value, 6);
  QrNFSeFatCabTXT_COMPETENCI.Text :=
    Copy(m, 5, 2) + '/' + Copy(m, 1, 4);
end;

end.

