unit NFSe_Create;

interface

uses
  StdCtrls, ExtCtrls, Windows, Messages, SysUtils, Classes, Graphics, Controls,
  Forms, Dialogs, Menus, UnMsgInt, UnInternalConsts, Db, DbCtrls, UnMLAGeral,
  UnInternalConsts2, ComCtrls, Registry, mySQLDbTables,(* DbTables,*) dmkGeral;

type
  TNomeTabRecriaTempTable = (ntrtt_NFSeMenCab(*, ntrtt_NFSeNfsCab*));
  TAcaoCreate = (acDrop, acCreate, acFind);
  TNFSe_Create = class(TObject)

  private
    { Private declarations }
    procedure Cria_ntrtt_NFSeMenCab(Qry: TmySQLQuery);
    //procedure Cria_ntrtt_NFSeNfsCab(Qry: TmySQLQuery);

  public
    { Public declarations }
    function RecriaTempTableNovo(Tabela: TNomeTabRecriaTempTable;
             Qry: TmySQLQuery; UniqueTableName: Boolean; Repeticoes:
             Integer = 1; NomeTab: String = ''): String;
  end;

var
  UnNFSe_Create: TNFSe_Create;

implementation

uses UnMyObjects, Module, ModuleGeral;

procedure TNFSe_Create.Cria_ntrtt_NFSeMenCab(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  Codigo               int(11)      NOT NULL                      ,');
  Qry.SQL.Add('  Empresa              int(11)      NOT NULL                      ,');
  Qry.SQL.Add('  Cliente              int(11)      NOT NULL                      ,');
  Qry.SQL.Add('  Contrato             int(11)      NOT NULL                      ,');
  Qry.SQL.Add('  Intermed             int(11)      NOT NULL                      ,');
  Qry.SQL.Add('  NFSeSrvCad           int(11)      NOT NULL                      ,');
  Qry.SQL.Add('  DiaFat               tinyint(2)   NOT NULL                      ,');
  Qry.SQL.Add('  IncreCompet          tinyint(1)   NOT NULL                      ,');
  Qry.SQL.Add('  Valor                double(15,2) NOT NULL                      ,');
  Qry.SQL.Add('  GLGerarLct           tinyint(1)   NOT NULL                      ,');
  Qry.SQL.Add('  GLCarteira           int(11)      NOT NULL                      ,');
  Qry.SQL.Add('  GLConta              int(11)      NOT NULL                      ,');
  Qry.SQL.Add('  VigenDtIni           date         NOT NULL                      ,');
  Qry.SQL.Add('  VigenDtFim           date         NOT NULL                      ,');
  Qry.SQL.Add('  GBGerarBol           tinyint(1)   NOT NULL                      ,');
  //
  Qry.SQL.Add('  Ativo                tinyint(1)   NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  PRIMARY KEY (Codigo)');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

(*
procedure TNFSe_Create.Cria_ntrtt_NFSeNfsCab(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  Empresa                        int(11)      NOT NULL                      ,');
  Qry.SQL.Add('  Ambiente                       tinyint(1)   NOT NULL                      ,');
  Qry.SQL.Add('  NfsNumero                      bigint(20)   NOT NULL                      ,');
  Qry.SQL.Add('  NfsRpsIDSerie                  varchar(5)   NOT NULL                      ,');
  Qry.SQL.Add('  NfsRpsIDTipo                   tinyint(1)   NOT NULL                      ,');
  Qry.SQL.Add('  NfsRpsIDNumero                 int(11)      NOT NULL                      ,');
  Qry.SQL.Add('  NfsVersao                      double(15,2) NOT NULL                      ,');
  Qry.SQL.Add('  NfsCodigoVerificacao           varchar(9)   NOT NULL                      ,');
  Qry.SQL.Add('  NfsDataEmissao                 datetime     NOT NULL                      ,');
  Qry.SQL.Add('  NfsNfseSubstituida             bigint(20)   NOT NULL                      ,');
  Qry.SQL.Add('  NfsOutrasInformacoes           varchar(255) NOT NULL                      ,');
  Qry.SQL.Add('  NfsBaseCalculo                 double(15,2) NOT NULL                      ,');
  Qry.SQL.Add('  NfsAliquota                    double(15,2) NOT NULL                      ,');
  Qry.SQL.Add('  NfsValorIss                    double(15,2) NOT NULL                      ,');
  Qry.SQL.Add('  NfsValorLiquidoNfse            double(15,2) NOT NULL                      ,');
  Qry.SQL.Add('  NfsValorCredito                double(15,2) NOT NULL                      ,');
  Qry.SQL.Add('  NfsPrestaCnpj                  varchar(18)  NOT NULL                      ,');
  Qry.SQL.Add('  NfsPrestaCpf                   varchar(18)  NOT NULL                      ,');
  Qry.SQL.Add('  NfsPrestaInscricaoMunicipal    varchar(18)  NOT NULL                      ,');
  Qry.SQL.Add('  NfsPrestaRazaoSocial           varchar(150) NOT NULL                      ,');
  Qry.SQL.Add('  NfsPrestaNomeFantasia          varchar(60)  NOT NULL                      ,');
  Qry.SQL.Add('  NfsPrestaEndereco              varchar(125) NOT NULL                      ,');
  Qry.SQL.Add('  NfsPrestaNumero                varchar(10)  NOT NULL                      ,');
  Qry.SQL.Add('  NfsPrestaComplemento           varchar(60)  NOT NULL                      ,');
  Qry.SQL.Add('  NfsPrestaBairro                varchar(60)  NOT NULL                      ,');
  Qry.SQL.Add('  NfsPrestaCodigoMunicipio       int(11)      NOT NULL                      ,');
  Qry.SQL.Add('  NfsPrestaUf                    char(2)      NOT NULL                      ,');
  Qry.SQL.Add('  NfsPrestaCodigoPais            varchar(4)   NOT NULL                      ,');
  Qry.SQL.Add('  NfsPrestaCep                   varchar(10)  NOT NULL                      ,');
  Qry.SQL.Add('  NfsPrestaContatoTelefone       varchar(30)  NOT NULL                      ,');
  Qry.SQL.Add('  NfsPrestaContatoEmail          varchar(80)  NOT NULL                      ,');
  Qry.SQL.Add('  NfsOrgaoGeradorCodigoMunicipio int(11)      NOT NULL                      ,');
  Qry.SQL.Add('  NfsOrgaoGeradorUf              varchar(2)   NOT NULL                      ,');
  Qry.SQL.Add('  DpsRpsIDNumero                 int(11)      NOT NULL                      ,');
  Qry.SQL.Add('  DpsRpsIDSerie                  varchar(5)   NOT NULL                      ,');
  Qry.SQL.Add('  DpsRpsIDTipo                   tinyint(1)   NOT NULL                      ,');
  Qry.SQL.Add('  DpsRpsDataEmissao              datetime     NOT NULL                      ,');
  Qry.SQL.Add('  DpsRpsStatus                   int(11)      NOT NULL                      ,');
  Qry.SQL.Add('  DpsSubstNumero                 int(11)      NOT NULL                      ,');
  Qry.SQL.Add('  DpsSubstSerie                  varchar(5)   NOT NULL                      ,');
  Qry.SQL.Add('  DpsSubstTipo                   tinyint(1)   NOT NULL                      ,');
  Qry.SQL.Add('  DpsCompetencia                 date         NOT NULL                      ,');
  Qry.SQL.Add('  DpsValorServicos               double(15,2) NOT NULL                      ,');
  Qry.SQL.Add('  DpsValorDeducoes               double(15,2) NOT NULL                      ,');
  Qry.SQL.Add('  DpsValorPis                    double(15,2) NOT NULL                      ,');
  Qry.SQL.Add('  DpsValorCofins                 double(15,2) NOT NULL                      ,');
  Qry.SQL.Add('  DpsValorInss                   double(15,2) NOT NULL                      ,');
  Qry.SQL.Add('  DpsValorIr                     double(15,2) NOT NULL                      ,');
  Qry.SQL.Add('  DpsValorCsll                   double(15,2) NOT NULL                      ,');
  Qry.SQL.Add('  DpsOutrasRetencoes             double(15,2) NOT NULL                      ,');
  Qry.SQL.Add('  DpsValorIss                    double(15,2) NOT NULL                      ,');
  Qry.SQL.Add('  DpsAliquota                    float(4,2)   NOT NULL                      ,');
  Qry.SQL.Add('  DpsDescontoIncondicionado      double(15,2) NOT NULL                      ,');
  Qry.SQL.Add('  DpsDescontoCondicionado        double(15,2) NOT NULL                      ,');
  Qry.SQL.Add('  DpsIssRetido                   tinyint(1)   NOT NULL                      ,');
  Qry.SQL.Add('  DpsResponsavelRetencao         tinyint(1)   NOT NULL                      ,');
  Qry.SQL.Add('  DpsItemListaServico            varchar(5)   NOT NULL                      ,');
  Qry.SQL.Add('  DpsCodigoCnae                  varchar(20)  NOT NULL                      ,');
  Qry.SQL.Add('  DpsCodigoTributacaoMunicipio   varchar(20)  NOT NULL                      ,');
  Qry.SQL.Add('  DpsDiscriminacao               text         NOT NULL                      ,');
  Qry.SQL.Add('  DpsCodigoMunicipio             int(11)      NOT NULL                      ,');
  Qry.SQL.Add('  DpsCodigoPais                  int(11)      NOT NULL                      ,');
  Qry.SQL.Add('  DpsExigibilidadeIss            tinyint(2)   NOT NULL                      ,');
  Qry.SQL.Add('  DpsNaturezaOperacao            int(11)      NOT NULL                      ,');
  Qry.SQL.Add('  DpsMunicipioIncidencia         int(11)      NOT NULL                      ,');
  Qry.SQL.Add('  DpsNumeroProcesso              varchar(30)  NOT NULL                      ,');
  Qry.SQL.Add('  DpsTomaCpf                     varchar(18)  NOT NULL                      ,');
  Qry.SQL.Add('  DpsTomaCnpj                    varchar(18)  NOT NULL                      ,');
  Qry.SQL.Add('  DpsTomaInscricaoMunicipal      varchar(18)  NOT NULL                      ,');
  Qry.SQL.Add('  DpsPrestaCpf                   varchar(18)  NOT NULL                      ,');
  Qry.SQL.Add('  DpsPrestaCnpj                  varchar(18)  NOT NULL                      ,');
  Qry.SQL.Add('  DpsPrestaInscricaoMunicipal    varchar(18)  NOT NULL                      ,');
  Qry.SQL.Add('  DpsTomaRazaoSocial             varchar(150) NOT NULL                      ,');
  Qry.SQL.Add('  DpsTomaNomeFantasia            varchar(60)  NOT NULL                      ,');
  Qry.SQL.Add('  DpsTomaEndereco                varchar(125) NOT NULL                      ,');
  Qry.SQL.Add('  DpsTomaNumero                  varchar(10)  NOT NULL                      ,');
  Qry.SQL.Add('  DpsTomaComplemento             varchar(60)  NOT NULL                      ,');
  Qry.SQL.Add('  DpsTomaBairro                  varchar(60)  NOT NULL                      ,');
  Qry.SQL.Add('  DpsTomaCodigoMunicipio         int(11)      NOT NULL                      ,');
  Qry.SQL.Add('  DpsTomaUf                      varchar(125) NOT NULL                      ,');
  Qry.SQL.Add('  DpsTomaCodigoPais              int(11)      NOT NULL                      ,');
  Qry.SQL.Add('  DpsTomaCep                     int(11)      NOT NULL                      ,');
  Qry.SQL.Add('  DpsTomaContatoTelefone         varchar(20)  NOT NULL                      ,');
  Qry.SQL.Add('  DpsTomaContatoEmail            varchar(80)  NOT NULL                      ,');
  Qry.SQL.Add('  DpsIntermeCpf                  varchar(18)  NOT NULL                      ,');
  Qry.SQL.Add('  DpsIntermeCnpj                 varchar(18)  NOT NULL                      ,');
  Qry.SQL.Add('  DpsIntermeInscricaoMunicipal   varchar(18)  NOT NULL                      ,');
  Qry.SQL.Add('  DpsIntermeRazaoSocial          varchar(150) NOT NULL                      ,');
  Qry.SQL.Add('  DpsConstrucaoCivilCodigoObra   varchar(15)  NOT NULL                      ,');
  Qry.SQL.Add('  DpsConstrucaoCivilArt          varchar(15)  NOT NULL                      ,');
  Qry.SQL.Add('  DpsRegimeEspecialTributacao    tinyint(2)   NOT NULL                      ,');
  Qry.SQL.Add('  DpsOptanteSimplesNacional      tinyint(1)   NOT NULL                      ,');
  Qry.SQL.Add('  DpsIncentivoFiscal             tinyint(1)   NOT NULL                      ,');
  Qry.SQL.Add('  DpsInfID_ID                    varchar(255) NOT NULL                      ,');
  Qry.SQL.Add('  CanCodigoCancelamento          tinyint(1)   NOT NULL                      ,');
  Qry.SQL.Add('  CanCanc_DataHora               datetime     NOT NULL                      ,');
  Qry.SQL.Add('  CanCanc_ID                     varchar(255) NOT NULL                      ,');
  Qry.SQL.Add('  CanCanc_Versao                 double(15,2) NOT NULL                      ,');
  Qry.SQL.Add('  SubNfseSubstituidora           bigint(20)   NOT NULL                      ,');
  Qry.SQL.Add('  SubSubst_ID                    varchar(255) NOT NULL                      ,');
  Qry.SQL.Add('  SubSubst_Versao                double(15,2) NOT NULL                      ,');
  Qry.SQL.Add('  CanNumero                      bigint(20)   NOT NULL                      ,');
  Qry.SQL.Add('  CanCpf                         varchar(18)  NOT NULL                      ,');
  Qry.SQL.Add('  CanInscricaomunicipal          varchar(18)  NOT NULL                      ,');
  Qry.SQL.Add('  CanCodigoMunicipio             int(11)      NOT NULL                      ,');

  Qry.SQL.Add('  Ativo                          tinyint(1)   NOT NULL                      ');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;
*)

function TNFSe_Create.RecriaTempTableNovo(Tabela: TNomeTabRecriaTempTable; Qry: TmySQLQuery;
UniqueTableName: Boolean; Repeticoes: Integer = 1; NomeTab: String = ''): String;
var
  Nome, TabNo: String;
  P: Integer;
begin
  TabNo := '';
  if NomeTab = '' then
  begin
    case Tabela of
      ntrtt_NFSeMenCab:               Nome := Lowercase('_nfsemencab_');
      //ntrtt_NFSeNfsCab:               Nome := Lowercase('_nfsenfscab_');
      //
      // ...
      else Nome := '';
    end;
  end else
    Nome := Lowercase(NomeTab);
  //
  if Nome = '' then
  begin
    Geral.MensagemBox(
    'Tabela tempor�ria sem nome definido! (RecriaTemTableNovo)',
    'Erro', MB_OK+MB_ICONERROR);
    Result := '';
    Exit;
  end;
  if UniqueTableName then
  begin
    TabNo := '_' + FormatFloat('0', VAR_USUARIO) + '_' + Nome;
    //  caso for Master ou Admin (n�meros negativos)
    P := pos('-', TabNo);
    if P > 0 then
      TabNo[P] := '_';
  end else TabNo := Nome;
  Qry.Close;
  Qry.SQL.Clear;
  Qry.SQL.Add('DROP TABLE IF EXISTS ' + TabNo);
  Qry.ExecSQL;
  //
  Qry.Close;
  Qry.SQL.Clear;
  Qry.SQL.Add('CREATE TABLE ' + TabNo +' (');
  //
  case Tabela of
    ntrtt_NFSeMenCab        : Cria_ntrtt_NFSeMenCab(Qry);
    //ntrtt_NFSeNfsCab        : Cria_ntrtt_NFSeNfsCab(Qry);
    //
    else Geral.MensagemBox('N�o foi poss�vel criar a tabela tempor�ria "' +
    Nome + '" por falta de implementa��o!', 'Erro', MB_OK+MB_ICONERROR);
  end;
  Result := TabNo;
end;

end.

