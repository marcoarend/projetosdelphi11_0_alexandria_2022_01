unit NFSe_RPSPesq;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, dmkLabel, dmkEdit, dmkEditCB,
  DBCtrls, dmkDBLookupComboBox, DB, mySQLDbTables, ComCtrls,
  dmkEditDateTimePicker, Grids, DBGrids, dmkDBGrid, Variants, Menus, dmkMemo,
  frxClass, frxDBSet, dmkGeral, frxBarcode, UnDmkProcFunc, dmkImage,
  dmkCompoStore, dmkPermissoes,
  // ini 2023-12-20
  (*
  DmkACBrNFSe,
  // ACBr
  DMKpnfsConversao, DMKpnfsNFSe,
  *)
  ACBrNFSe, pnfsConversao,
  // fim 2023-12-20
  //
  NFSe_PF_0000, NFSe_PF_0201, xmldom, XMLIntf, msxmldom, XMLDoc, UnDmkEnums;

type
  TFmNFSe_RPSPesq = class(TForm)
    PnGeral: TPanel;
    QrClientes: TmySQLQuery;
    QrClientesCodigo: TIntegerField;
    QrClientesNO_Enti: TWideStringField;
    DsClientes: TDataSource;
    QrClientesTipo: TSmallintField;
    QrClientesCNPJ: TWideStringField;
    QrClientesCPF: TWideStringField;
    PMLeArq: TPopupMenu;
    PMImprime: TPopupMenu;
    CampospreenchidosnoXMLdaNFe1: TMenuItem;
    QrNFSeDPSCab: TmySQLQuery;
    DsNFSeDPSCab: TDataSource;
    QrNFeXMLI: TmySQLQuery;
    QrNFeXMLICodigo: TWideStringField;
    QrNFeXMLIID: TWideStringField;
    QrNFeXMLIValor: TWideStringField;
    QrNFeXMLIPai: TWideStringField;
    QrNFeXMLIDescricao: TWideStringField;
    QrNFeXMLICampo: TWideStringField;
    frxDsNFeXMLI: TfrxDBDataset;
    frxCampos: TfrxReport;
    QrArq: TmySQLQuery;
    QrArqFatID: TIntegerField;
    QrArqFatNum: TIntegerField;
    QrArqEmpresa: TIntegerField;
    QrArqIDCtrl: TIntegerField;
    QrArqXML_NFe: TWideMemoField;
    QrArqXML_Aut: TWideMemoField;
    QrArqXML_Can: TWideMemoField;
    QrArqLk: TIntegerField;
    QrArqDataCad: TDateField;
    QrArqDataAlt: TDateField;
    QrArqUserCad: TIntegerField;
    QrArqUserAlt: TIntegerField;
    QrArqAlterWeb: TSmallintField;
    QrArqAtivo: TSmallintField;
    PMArq: TPopupMenu;
    NFe1: TMenuItem;
    Autorizao1: TMenuItem;
    Cancelamento2: TMenuItem;
    N1: TMenuItem;
    DiretriodoarquivoXML1: TMenuItem;
    frxListaNFes: TfrxReport;
    N2: TMenuItem;
    Listadasnotaspesquisadas1: TMenuItem;
    QrNFe_100: TmySQLQuery;
    frxDsNFe_100: TfrxDBDataset;
    QrNFe_XXX: TmySQLQuery;
    frxDsNFe_XXX: TfrxDBDataset;
    QrNFe_XXXOrdem: TIntegerField;
    QrNFe_XXXcStat: TIntegerField;
    QrNFe_XXXide_nNF: TIntegerField;
    QrNFe_XXXide_serie: TIntegerField;
    QrNFe_XXXide_AAMM_AA: TWideStringField;
    QrNFe_XXXide_AAMM_MM: TWideStringField;
    QrNFe_XXXide_dEmi: TDateField;
    QrNFe_XXXNOME_tpEmis: TWideStringField;
    QrNFe_XXXNOME_tpNF: TWideStringField;
    QrNFe_XXXStatus: TIntegerField;
    QrNFe_XXXMotivo: TWideStringField;
    QrNFe_100ide_nNF: TIntegerField;
    QrNFe_100ide_serie: TIntegerField;
    QrNFe_100ide_dEmi: TDateField;
    QrNFe_100ICMSTot_vProd: TFloatField;
    QrNFe_100ICMSTot_vST: TFloatField;
    QrNFe_100ICMSTot_vFrete: TFloatField;
    QrNFe_100ICMSTot_vSeg: TFloatField;
    QrNFe_100ICMSTot_vIPI: TFloatField;
    QrNFe_100ICMSTot_vOutro: TFloatField;
    QrNFe_100ICMSTot_vDesc: TFloatField;
    QrNFe_100ICMSTot_vNF: TFloatField;
    QrNFe_100ICMSTot_vBC: TFloatField;
    QrNFe_100ICMSTot_vICMS: TFloatField;
    QrNFe_100NOME_tpEmis: TWideStringField;
    QrNFe_100NOME_tpNF: TWideStringField;
    QrNFe_100Ordem: TIntegerField;
    QrNFe_XXXNOME_ORDEM: TWideStringField;
    QrNFe_101: TmySQLQuery;
    frxDsNFe_101: TfrxDBDataset;
    QrNFe_101Ordem: TIntegerField;
    QrNFe_101ide_nNF: TIntegerField;
    QrNFe_101ide_serie: TIntegerField;
    QrNFe_101ide_AAMM_AA: TWideStringField;
    QrNFe_101ide_AAMM_MM: TWideStringField;
    QrNFe_101ide_dEmi: TDateField;
    QrNFe_101NOME_tpEmis: TWideStringField;
    QrNFe_101NOME_tpNF: TWideStringField;
    QrNFe_101infCanc_dhRecbto: TDateTimeField;
    QrNFe_101infCanc_nProt: TWideStringField;
    QrNFe_101Motivo: TWideStringField;
    QrNFe_101Id: TWideStringField;
    QrNFe_101Id_TXT: TWideStringField;
    PreviewdaNFe1: TMenuItem;
    QrNFe_100ICMSTot_vPIS: TFloatField;
    QrNFe_100ICMSTot_vCOFINS: TFloatField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel1: TPanel;
    BtImprime: TBitBtn;
    BtEnvia: TBitBtn;
    BtEvento: TBitBtn;
    BtCancela: TBitBtn;
    BtSubstitui: TBitBtn;
    BtLeArq: TBitBtn;
    BtArq: TBitBtn;
    BtConsulta: TBitBtn;
    BtAltera: TBitBtn;
    BtExclui: TBitBtn;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    Panel5: TPanel;
    Panel6: TPanel;
    Panel7: TPanel;
    dmkLabel1: TdmkLabel;
    Label14: TLabel;
    Label3: TLabel;
    EdFilial: TdmkEditCB;
    CBFilial: TdmkDBLookupComboBox;
    EdCliente: TdmkEditCB;
    CBCliente: TdmkDBLookupComboBox;
    EdIntermediario: TdmkEditCB;
    CBIntermediario: TdmkDBLookupComboBox;
    GroupBox1: TGroupBox;
    Label2: TLabel;
    Label1: TLabel;
    TPDataF: TdmkEditDateTimePicker;
    TPDataI: TdmkEditDateTimePicker;
    GroupBox2: TGroupBox;
    Label4: TLabel;
    Label5: TLabel;
    dmkEditDateTimePicker1: TdmkEditDateTimePicker;
    dmkEditDateTimePicker2: TdmkEditDateTimePicker;
    Panel8: TPanel;
    BtReabre: TBitBtn;
    Ck100e101: TCheckBox;
    RGOrdem2: TRadioGroup;
    RGAmbiente: TRadioGroup;
    RGQuemEmit: TRadioGroup;
    RGOrdem1: TRadioGroup;
    PnDados: TPanel;
    dmkDBGrid1: TdmkDBGrid;
    Splitter1: TSplitter;
    DBGrid1: TDBGrid;
    QrNFSeDPSCabCodigo: TIntegerField;
    QrNFSeDPSCabAmbiente: TSmallintField;
    QrNFSeDPSCabEmpresa: TIntegerField;
    QrNFSeDPSCabCliente: TIntegerField;
    QrNFSeDPSCabIntermediario: TIntegerField;
    QrNFSeDPSCabNFSeSrvCad: TIntegerField;
    QrNFSeDPSCabRpsID: TWideStringField;
    QrNFSeDPSCabRpsIDNumero: TIntegerField;
    QrNFSeDPSCabRpsIDSerie: TWideStringField;
    QrNFSeDPSCabRpsIDTipo: TSmallintField;
    QrNFSeDPSCabRpsDataEmissao: TDateField;
    QrNFSeDPSCabRpsHoraEmissao: TTimeField;
    QrNFSeDPSCabRpsStatus: TIntegerField;
    QrNFSeDPSCabCompetencia: TDateField;
    QrNFSeDPSCabSubstNumero: TIntegerField;
    QrNFSeDPSCabSubstSerie: TWideStringField;
    QrNFSeDPSCabSubstTipo: TSmallintField;
    QrNFSeDPSCabValorServicos: TFloatField;
    QrNFSeDPSCabValorDeducoes: TFloatField;
    QrNFSeDPSCabValorPis: TFloatField;
    QrNFSeDPSCabValorCofins: TFloatField;
    QrNFSeDPSCabValorInss: TFloatField;
    QrNFSeDPSCabValorIr: TFloatField;
    QrNFSeDPSCabValorCsll: TFloatField;
    QrNFSeDPSCabOutrasRetencoes: TFloatField;
    QrNFSeDPSCabValorIss: TFloatField;
    QrNFSeDPSCabAliquota: TFloatField;
    QrNFSeDPSCabDescontoIncondicionado: TFloatField;
    QrNFSeDPSCabDescontoCondicionado: TFloatField;
    QrNFSeDPSCabIssRetido: TSmallintField;
    QrNFSeDPSCabResponsavelRetencao: TSmallintField;
    QrNFSeDPSCabItemListaServico: TWideStringField;
    QrNFSeDPSCabCodigoCnae: TWideStringField;
    QrNFSeDPSCabCodigoTributacaoMunicipio: TWideStringField;
    QrNFSeDPSCabDiscriminacao: TWideMemoField;
    QrNFSeDPSCabCodigoMunicipio: TIntegerField;
    QrNFSeDPSCabCodigoPais: TIntegerField;
    QrNFSeDPSCabExigibilidadeISS: TSmallintField;
    QrNFSeDPSCabMunicipioIncidencia: TIntegerField;
    QrNFSeDPSCabNumeroProcesso: TWideStringField;
    QrNFSeDPSCabPrestaCpf: TWideStringField;
    QrNFSeDPSCabPrestaCnpj: TWideStringField;
    QrNFSeDPSCabPrestaInscricaoMunicipal: TWideStringField;
    QrNFSeDPSCabTomaCpf: TWideStringField;
    QrNFSeDPSCabTomaCnpj: TWideStringField;
    QrNFSeDPSCabTomaInscricaoMunicipal: TWideStringField;
    QrNFSeDPSCabTomaRazaoSocial: TWideStringField;
    QrNFSeDPSCabTomaEndereco: TWideStringField;
    QrNFSeDPSCabTomaNumero: TWideStringField;
    QrNFSeDPSCabTomaComplemento: TWideStringField;
    QrNFSeDPSCabTomaBairro: TWideStringField;
    QrNFSeDPSCabTomaCodigoMunicipio: TIntegerField;
    QrNFSeDPSCabTomaUf: TWideStringField;
    QrNFSeDPSCabTomaCodigoPais: TIntegerField;
    QrNFSeDPSCabTomaCep: TIntegerField;
    QrNFSeDPSCabIntermeCpf: TWideStringField;
    QrNFSeDPSCabIntermeCnpj: TWideStringField;
    QrNFSeDPSCabIntermeInscricaoMunicipal: TWideStringField;
    QrNFSeDPSCabIntermeRazaoSocial: TWideStringField;
    QrNFSeDPSCabConstrucaoCivilCodigoObra: TWideStringField;
    QrNFSeDPSCabConstrucaoCivilArt: TWideStringField;
    QrNFSeDPSCabRegimeEspecialTributacao: TSmallintField;
    QrNFSeDPSCabOptanteSimplesNacional: TSmallintField;
    QrNFSeDPSCabIncentivoFiscal: TSmallintField;
    QrNFSeDPSCabId: TWideStringField;
    QrNFSeDPSCabLk: TIntegerField;
    QrNFSeDPSCabDataCad: TDateField;
    QrNFSeDPSCabDataAlt: TDateField;
    QrNFSeDPSCabUserCad: TIntegerField;
    QrNFSeDPSCabUserAlt: TIntegerField;
    QrNFSeDPSCabAlterWeb: TSmallintField;
    QrNFSeDPSCabAtivo: TSmallintField;
    QrNFSeDPSCabLastLRpsC: TIntegerField;
    QrNFSeDPSCabQuemPagaISS: TSmallintField;
    QrNFSeDPSCabNaturezaOperacao: TIntegerField;
    QrNFSeDPSCabTomaContatoTelefone: TWideStringField;
    QrNFSeDPSCabTomaContatoEmail: TWideStringField;
    QrNFSeDPSCabTomaNomeFantasia: TWideStringField;
    QrNFSe: TmySQLQuery;
    QrNFSeNumero: TLargeintField;
    PMConsulta: TPopupMenu;
    RPSselecionado1: TMenuItem;
    PorfaixadeNFSe1: TMenuItem;
    Servicosprestados1: TMenuItem;
    Serviostomados1: TMenuItem;
    QrPsqNfse: TmySQLQuery;
    QrPsqNfseNfsNumero: TLargeintField;
    QrPsqNfseEmpresa: TIntegerField;
    QrPsqNfseAmbiente: TSmallintField;
    PMCancela: TPopupMenu;
    Erronaemisso1: TMenuItem;
    N4Duplicidadedanota1: TMenuItem;
    N2Servionoprestado1: TMenuItem;
    N3Errodeassinatura1: TMenuItem;
    N5Errodeprocessamento1: TMenuItem;
    QrNFSeDPSCabNFSeFatCab: TIntegerField;
    QrIntermediario: TmySQLQuery;
    QrIntermediarioCodigo: TIntegerField;
    QrIntermediarioNO_Enti: TWideStringField;
    QrIntermediarioCNPJ: TWideStringField;
    QrIntermediarioCPF: TWideStringField;
    QrIntermediarioTipo: TSmallintField;
    DsIntermediario: TDataSource;
    QrNFSeDPSCabNO_CLI: TWideStringField;
    QrNFSeDPSCabNO_INT: TWideStringField;
    QrClientesNIRE: TWideStringField;
    QrIntermediarioNIRE: TWideStringField;
    QrLoc: TmySQLQuery;
    QrNFSeDPSCabStatusDPS: TIntegerField;
    dmkPermissoes1: TdmkPermissoes;
    CSTabSheetChamou: TdmkCompoStore;
    QrNFSeDPSCabTomaCnpjCpf: TWideStringField;
    QrNFSeDPSCabTomaCnpjCpf_TXT: TWideStringField;
    QrNFSeDPSCabNfsNumero: TLargeintField;
    LdoarquivoXML1: TMenuItem;
    LXMLdoBD1: TMenuItem;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure EdFilialExit(Sender: TObject);
    procedure EdFilialChange(Sender: TObject);
    procedure EdClienteChange(Sender: TObject);
    procedure EdClienteExit(Sender: TObject);
    procedure TPDataIChange(Sender: TObject);
    procedure TPDataIClick(Sender: TObject);
    procedure BtCancelaClick(Sender: TObject);
    procedure QrClientesBeforeClose(DataSet: TDataSet);
    procedure BtLeArqClick(Sender: TObject);
    procedure dmkDBGrid1DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure CampospreenchidosnoXMLdaNFe1Click(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
    procedure QrNFSeDPSCabAfterOpen(DataSet: TDataSet);
    procedure QrNFSeDPSCabAfterScroll(DataSet: TDataSet);
    procedure QrNFSeDPSCabBeforeClose(DataSet: TDataSet);
    procedure BtArqClick(Sender: TObject);
    procedure PMArqPopup(Sender: TObject);
    procedure NFe1Click(Sender: TObject);
    procedure Autorizao1Click(Sender: TObject);
    procedure Cancelamento2Click(Sender: TObject);
    procedure BtEnviaClick(Sender: TObject);
    procedure BtReabreClick(Sender: TObject);
    procedure CkEmitClick(Sender: TObject);
    procedure RGAmbienteClick(Sender: TObject);
    procedure DiretriodoarquivoXML1Click(Sender: TObject);
    procedure BtExcluiClick(Sender: TObject);
    procedure RGOrdem1Click(Sender: TObject);
    procedure RGOrdem2Click(Sender: TObject);
    procedure Listadasnotaspesquisadas1Click(Sender: TObject);
    procedure QrNFe_XXXCalcFields(DataSet: TDataSet);
    procedure frxListaNFesGetValue(const VarName: string; var Value: Variant);
    procedure PreviewdaNFe1Click(Sender: TObject);
    procedure BtConsultaClick(Sender: TObject);
    procedure RPSselecionado1Click(Sender: TObject);
    procedure PorfaixadeNFSe1Click(Sender: TObject);
    procedure Servicosprestados1Click(Sender: TObject);
    procedure Serviostomados1Click(Sender: TObject);
    procedure Erronaemisso1Click(Sender: TObject);
    procedure N2Servionoprestado1Click(Sender: TObject);
    procedure N4Duplicidadedanota1Click(Sender: TObject);
    procedure N3Errodeassinatura1Click(Sender: TObject);
    procedure N5Errodeprocessamento1Click(Sender: TObject);
    procedure BtSubstituiClick(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure QrNFSeDPSCabCalcFields(DataSet: TDataSet);
    procedure FormShow(Sender: TObject);
    procedure LdoarquivoXML1Click(Sender: TObject);
    procedure LXMLdoBD1Click(Sender: TObject);
  private
    { Private declarations }
    //FLote, FEmpresa: Integer;
    // Impress�o de lista de NFes
    //F_NFe_100, F_NFe_101, F_NFe_XXX: String;
    //
    procedure ReopenNFSeDPSCab(Codigo: Integer; So_O_ID: Boolean);
    procedure FechaNFSeDPSCab();
    //procedure InsUpdNFSe(Nota: TNfse);
    procedure ReopenPesqAtual();
    procedure CancelaNFSe(Motivo: Integer);
    procedure MostraFormNFSe_Edit_0201(SQLType: TSQLType; Servico: TnfseFormaGerarNFSe);
    function  CarregaXML(): Boolean;
    //
    //procedure ConsultaRPSselecionado_Antigo();
    procedure ConsultaRPSselecionado_Novo();
  public
    { Public declarations }
  end;

  var
  FmNFSe_RPSPesq: TFmNFSe_RPSPesq;

implementation

uses UnMyObjects, ModuleGeral, UnInternalConsts, MyDBCheck, UCreate,
  UMySQLModule, Module, DmkDAC_PF, NFSe_0000_Module, MyGlyfs, NFSeABRASF_0201,
  NFSe_Edit_0201, Principal;

{$R *.DFM}

procedure TFmNFSe_RPSPesq.Autorizao1Click(Sender: TObject);
begin
  Geral.MensagemBox(QrArqXML_Aut.Value, 'XML Autoriza��o', MB_OK+MB_ICONINFORMATION);
end;

procedure TFmNFSe_RPSPesq.BtReabreClick(Sender: TObject);
begin
  ReopenPesqAtual();
end;

procedure TFmNFSe_RPSPesq.BtEnviaClick(Sender: TObject);
begin
  Geral.MB_Info('N�o implementado!' + #13#10 + 'Em implementa��o!');
  Exit;
end;

procedure TFmNFSe_RPSPesq.BtExcluiClick(Sender: TObject);
var
  Codigo, NFSeNumero: Integer;
begin
  DmNFSe_0000.ObtemNumeroNFSe_PeloRPS(QrNFSeDPSCabEmpresa.Value,
    QrNFSeDPSCabAmbiente.Value, QrNFSeDPSCabRpsIDTipo.Value,
    QrNFSeDPSCabRPSIDNumero.Value, QrNFSeDPSCabRPSIDSerie.Value,
    NFSeNumero);
  if NFSeNumero = 0 then
  begin
    if Geral.MensagemBox('Deseja realmente excluir o RPS:' + #13#10 +
    'Tipo: ' + FormatFloat('000', QrNFSeDPSCabRPSIDTipo.Value) + #13#10 +
    'S�rie: ' + QrNFSeDPSCabRPSIDSerie.Value + #13#10 +
    'N�: ' + FormatFloat('000', QrNFSeDPSCabRPSIDNumero.Value) + #13#10 + #13#10 +
    'ESTE PROCEDIMENTO N�O PODER� SER REVERTIDO!',
    'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
    begin
      if DmNFSe_0000.ExcluiRPS_PeloNumero(QrNFSeDPSCabEmpresa.Value,
      QrNFSeDPSCabAmbiente.Value, QrNFSeDPSCabRpsIDTipo.Value,
      QrNFSeDPSCabRPSIDNumero.Value, QrNFSeDPSCabRPSIDSerie.Value) then
      begin
        QrNFSeDPSCab.Next;
        Codigo := UMyMod.ProximoRegistro(QrNFSeDPSCab, 'Codigo', QrNFSeDPSCabCodigo.Value);
        //
        BtReabreClick(Self);
        QrNFSeDPSCab.Locate('Codigo', Codigo, []);
        Geral.MensagemBox('RPS exclu�do com sucesso!',
        'Informa��o', MB_OK+MB_ICONINFORMATION);
      end;
    end;
  end else Geral.MB_Aviso('Exclus�o cancelada! ' + #13#10 +
  'O registro selecionado � o RPS da NFSE ' + Geral.FF0(NFSeNumero));
end;

procedure TFmNFSe_RPSPesq.BtAlteraClick(Sender: TObject);
begin
  if QrNFSeDPSCabSubstNumero.Value > 0 then
    MostraFormNFSe_Edit_0201(stUpd, fgnSubstituicaoNFSe)
  else
    MostraFormNFSe_Edit_0201(stUpd, fgnLoteRPSSincrono);
end;

procedure TFmNFSe_RPSPesq.BtArqClick(Sender: TObject);
begin
  QrArq.Close;
  QrArq.Params[0].AsInteger := QrNFSeDPSCabCodigo.Value;
  QrArq.Open;
  //
  MyObjects.MostraPopUpDeBotao(PMArq, BtArq);
end;

procedure TFmNFSe_RPSPesq.BtCancelaClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMCancela, BtCancela);
end;

procedure TFmNFSe_RPSPesq.BtConsultaClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMConsulta, BtConsulta);
end;

procedure TFmNFSe_RPSPesq.BtLeArqClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMLeArq, BtLeArq);
end;

procedure TFmNFSe_RPSPesq.BtSaidaClick(Sender: TObject);
begin
  if TFmNFSe_RPSPesq(Self).Owner is TApplication then
    Close
  else
    MyObjects.FormTDIFecha(TFmNFSe_RPSPesq(Self), TTabSheet(CSTabSheetChamou.Component));
end;

procedure TFmNFSe_RPSPesq.BtSubstituiClick(Sender: TObject);
begin
  Geral.MB_Info('Substitui��o de NFS-e n�o implementada!');
  //MostraFormNFSe_Edit_0201(stIns, fgnSubstituicaoNFSe);
end;

procedure TFmNFSe_RPSPesq.CampospreenchidosnoXMLdaNFe1Click(Sender: TObject);
begin
{
  QrNFeXMLI.Close;
  QrNFeXMLI.Params[00].AsInteger := QrNFSeDPSCabFatID.Value;
  QrNFeXMLI.Params[01].AsInteger := QrNFSeDPSCabFatNum.Value;
  QrNFeXMLI.Params[02].AsInteger := QrNFSeDPSCabEmpresa.Value;
  QrNFeXMLI.Open;
  //
  MyObjects.frxMostra(frxCampos, 'Campos preenchidos no XML');
}
end;

procedure TFmNFSe_RPSPesq.Cancelamento2Click(Sender: TObject);
begin
  Geral.MensagemBox(QrArqXML_Can.Value, 'XML Cancelamento', MB_OK+MB_ICONINFORMATION);
end;

procedure TFmNFSe_RPSPesq.CancelaNFSe(Motivo: Integer);
var
  //Status, NFSe: Integer;
  //
  //Ok, Continua: Boolean;
  Arq: String;
begin
  if Geral.MB_Pergunta(
  'Confirma a solicita��o de cancelamento da NFS-e selecionada?') <> ID_YES then
    Exit;
  //
  Screen.Cursor := crHourGlass;
  try
//    if not CarregaXML() then
    DmNFSe_0000.ReopenNFSeDPSCab(QrNFSeDPSCabCodigo.Value);
    if not DmNFSe_0000.CarregaXML_RPS(QrNFSeDPSCabEmpresa.Value,
    (*QrNFSeDPSCabRPSIdNumero.Value, QrNFSeDPSCabRpsIDSerie.Value,
    QrNFSeDPSCabRpsIDTipo.Value,*) QrNFSeDPSCabAmbiente.Value, LaAviso1, LaAviso2,
    Self) then
      Exit;
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Cancelando NFSe');
    if DmNFSe_0000.ACBrNFSe1.CancelarNFSe(Geral.FF0(Motivo)) then
    begin
      // Erro??? no ACBr
      //Arq := DmNFSe_0000.ACBrNFSe1.Configuracoes.Arquivos.GetPathCan + '\' +
      //DmNFSe_0000.ACBrNFSe1.NotasFiscais.Items[0].NFSe.Numero + '-can.xml';
(*
////////////////////////////////////////////////////////////////////////////////
ERRO NO ARQUIVO : C:\ACBr\Fontes\ACBrDFe\ACBrNFSe\ACBrNFSeWebServices.pas
         LINHA: 1655 (2023-12-24)
         em vez de usar  GetPathCan � usado GetPathNFSe
////////////////////////////////////////////////////////////////////////////////
procedure TNFSeCancelarNFSe.SalvarResposta;
var
  aPath: String;
begin
  inherited SalvarResposta;

  if FPConfiguracoesNFSe.Arquivos.Salvar then
  begin
    aPath := PathWithDelim(FPConfiguracoesNFSe.Arquivos.GetPathNFSe(0, ''{xData, xCNPJ}));
    FPDFeOwner.Gravar(GerarPrefixoArquivo + '-' + ArqResp + '.xml', FPRetWS, aPath);
  end;
end;

*)
      Arq := DmNFSe_0000.ACBrNFSe1.Configuracoes.Arquivos.GetPathNFSe() + '\' +
      DmNFSe_0000.ACBrNFSe1.NotasFiscais.Items[0].NFSe.Numero + '-can.xml';
      //
      DmNFSe_0000.AtualizaStatusRPS(LaAviso1, LaAviso2,
        QrNFSeDPSCabAmbiente.Value, QrNFSeDPSCabCodigo.Value, Arq,
        'Lendo XML de cancelamento de NFSe / RPS', QrNFSeDPSCab);
    end else
      Geral.MB_Info(UTF8Encode(DmNFSe_0000.ACBrNFSe1.WebServices.ConsNfseRps.RetWS));
  finally
    Screen.Cursor := crDefault;
  end;
end;

function TFmNFSe_RPSPesq.CarregaXML(): Boolean;
var
  Ambiente: Integer;
  XML: AnsiString;
  Arq: String;
  Mem: TStringStream;
  // ini 2023-12-20
  //Nota: TNfse;
  // ini 2023-12-20
begin
  Result := False;
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Carregando dados do RPS / NFSe selecionado');
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrPsqNfse, Dmod.MyDB, [
  'SELECT NfsNumero, Empresa, Ambiente ',
  'FROM nfsenfscab ',
  'WHERE Empresa=' + Geral.FF0(QrNFSeDPSCabEmpresa.Value),
  'AND DpsRpsIdNumero=' + Geral.FF0(QrNFSeDPSCabRpsIDNumero.Value),
  'AND DpsRpsIdSerie="' + QrNFSeDPSCabRpsIDSerie.Value + '" ',
  'AND DpsRpsIdTipo=' + Geral.FF0(QrNFSeDPSCabRpsIDTipo.Value),
  'AND Ambiente=' + Geral.FF0(QrNFSeDPSCabAmbiente.Value),
  '']);

  if QrNFSeDPSCabAmbiente.Value <> QrPsqNfseAmbiente.Value then
  begin
    Geral.MB_Erro('Ambiente do RPS: ' + Geral.FF0(QrNFSeDPSCabAmbiente.Value) +
    ' n�o confere com o ambiente da NFSe: ' + Geral.FF0(QrPsqNfseAmbiente.Value));
    Screen.Cursor := crDefault;
    Exit;
  end else Ambiente := QrNFSeDPSCabAmbiente.Value;

  if not DmNFSe_0000.ReopenNfseArq(txmNFS, QrPsqNfseEmpresa.Value,
  Ambiente, QrPsqNfseNfsNumero.Value, XML, Caption) then
  begin
    if Geral.MB_Pergunta('Deseja carregar o XML de um arquivo no banco de dados?') = ID_YES then
    begin
      if MyObjects.FileOpenDialog(Self, DmNFSe_0000.QrFilialDirNFSeDPSGer.Value,
      '', 'Arquivo XML', '*.xml', [], Arq) then
      begin
        if FileExists(Arq) then
        begin
          if UnNFSeABRASF_0201.LerXML_Arquivo(DmNFSe_0000.XMLDocument1, Ambiente, Arq) then
          Geral.MB_Aviso('Tente executar a a��o novamente!');
        end;
      end;
    end;
    Exit;
  end;
  //
  UnNFSe_PF_0201.ConfiguraComponenteACBrNFSe(DmodG.QrEmpresasCodigo.Value,
    DmNFSe_0000.ACBrNFSe1);
  DmNFSe_0000.ACBrNFSe1.NotasFiscais.Clear;
  //
  Mem := TStringStream.Create(XML);
  DmNFSe_0000.ACBrNFSe1.NotasFiscais.LoadFromStream(Mem);
  // ini 2023-12-20 Parei Aqui!
  //Nota := DmNFSe_0000.ACBrNFSe1.NotasFiscais.Items[0].NFSe;

  if DmNFSe_0000.ACBrNFSe1.NotasFiscais.Items[0].NFSe.
  PrestadorServico.IdentificacaoPrestador.InscricaoMunicipal = '' then
    DmNFSe_0000.ACBrNFSe1.NotasFiscais.Items[0].NFSe.
    PrestadorServico.IdentificacaoPrestador.InscricaoMunicipal :=
    Geral.SoNumero_TT(DModG.QrEmpresasNIRE.Value);
  //
  //if Nota.Numero = '' then
  if DmNFSe_0000.ACBrNFSe1.NotasFiscais.Items[0].NFSe.Numero = '' then
  // fim 2023-12-20 Parei Aqui!
  begin
    //Result := False;
    Geral.MB_Aviso('XML da NFSe n�o foi carregado ou n�o est� na base de dados!');
    Screen.Cursor := crDefault;
  end else
    Result := true;
end;

procedure TFmNFSe_RPSPesq.CkEmitClick(Sender: TObject);
begin
  FechaNFSeDPSCab;
end;

{
procedure TFmNFSe_RPSPesq.ConsultaRPSselecionado_Antigo();
var
  //Retorno: String;

  // ini 2023-12-20
  //Nota: TNfse;
  // fim 2023-12-20

  Mem: TStringStream;
  //Status, Codigo,
  //NFSe: Integer;
  //
  Arq: String;
  //Ok, Continua: Boolean;
  XML: AnsiString;
  //
  //StatusRps: TnfseStatusRPS;
begin

  // ini 2023-12-20 Parei Aqui!
  Geral.MB_Aviso('Falta implementar 2023-12-20!');
  try
    Screen.Cursor := crHourGlass;
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Preparando consulta de NFSe por RPS');

    if not DmNFSe_0000.ReopenNfseArq(txmRPS, QrNFSeDPSCabEmpresa.Value,
    QrNFSeDPSCabAmbiente.Value, QrNFSeDPSCabCodigo.Value, XML, Caption) then
      Exit;
    //
    UnNFSe_PF_0201.ConfiguraComponenteACBrNFSe(DmodG.QrEmpresasCodigo.Value,
      DmNFSe_0000.ACBrNFSe1);
    DmNFSe_0000.ACBrNFSe1.NotasFiscais.Clear;
    //

    (*
    DmNFSe_0000.ACBrNFSe1.NotasFiscais.Add;
    DmNFSe_0000.ACBrNFSe1.NotasFiscais.Items[0].NFSe.IdentificacaoRps.Numero := Geral.FF0(QrNFSeDPSCabRpsIDNumero.Value);
    DmNFSe_0000.ACBrNFSe1.NotasFiscais.Items[0].NFSe.IdentificacaoRps.Serie := QrNFSeDPSCabRpsIDSerie.Value;
    DmNFSe_0000.ACBrNFSe1.NotasFiscais.Items[0].NFSe.IdentificacaoRps.Tipo := StrToTipoRPS(Ok, Geral.FF0(QrNFSeDPSCabRpsIDTipo.Value));
    DmNFSe_0000.ACBrNFSe1.NotasFiscais.Items[0].NFSe.Prestador.Cnpj := Geral.SoNumero_TT(QrNFSeDPSCabPrestaCnpj.Value);
    DmNFSe_0000.ACBrNFSe1.NotasFiscais.Items[0].NFSe.Prestador.InscricaoMunicipal := Geral.SoNumero_TT(QrNFSeDPSCabPrestaInscricaoMunicipal.Value);
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Consultando de NFSe por RPS');
    if DmNFSe_0000.ACBrNFSe1.ConsutarNFSeporRps(DmNFSe_0000.ACBrNFSe1.NotasFiscais.Items[0].NFSe.IdentificacaoRps.Numero,
                                  DmNFSe_0000.ACBrNFSe1.NotasFiscais.Items[0].NFSe.IdentificacaoRps.Serie,
                                  TipoRPSToStr(DmNFSe_0000.ACBrNFSe1.NotasFiscais.Items[0].NFSe.IdentificacaoRps.Tipo),
                                  DmNFSe_0000.ACBrNFSe1.NotasFiscais.Items[0].NFSe.Prestador.Cnpj,
                                  DmNFSe_0000.ACBrNFSe1.NotasFiscais.Items[0].NFSe.Prestador.InscricaoMunicipal) then ...
    *)
    //pelo XML do BD tamb�m funciona!
    Mem := TStringStream.Create(XML);
    DmNFSe_0000.ACBrNFSe1.NotasFiscais.LoadFromStream(Mem);
  // ini 2023-12-20
    //Nota := DmNFSe_0000.ACBrNFSe1.NotasFiscais.Items[0].NFSe;
   // fim 2023-12-20
  //fazer pesquisa por xml de arquivo tambem?


    if DmNFSe_0000.ACBrNFSe1.ConsutarNFSeporRps(Nota.IdentificacaoRps.Numero,
                                  Nota.IdentificacaoRps.Serie,
                                  TipoRPSToStr(Nota.IdentificacaoRps.Tipo),
                                  Nota.Prestador.Cnpj,
                                  Nota.Prestador.InscricaoMunicipal) then
    begin
      Arq := DmNFSe_0000.ACBrNFSe1.Configuracoes.Arquivos.GetPathGer + '\' +
      DmNFSe_0000.ACBrNFSe1.NotasFiscais.Items[0].NFSe.IdentificacaoRps.Numero +
      DmNFSe_0000.ACBrNFSe1.NotasFiscais.Items[0].NFSe.IdentificacaoRps.Serie  +
       '-comp-nfse.xml';
      //StatusRps := DmNFSe_0000.ACBrNFSe1.NotasFiscais.items[0].NFSe.Status;
      //
      DmNFSe_0000.AtualizaStatusRPS(LaAviso1, LaAviso2,
        QrNFSeDPSCabAmbiente.Value, QrNFSeDPSCabCodigo.Value, Arq,
        'Lendo XML de consulta de NFSe por RPS', QrNFSeDPSCab);
    end
    else Geral.MB_Info(UTF8Encode(DmNFSe_0000.ACBrNFSe1.WebServices.ConsNfseRps.RetWS));
  finally
    Screen.Cursor := crDefault;
  end;
*)
  // fim 2023-12-20 Parei Aqui!
end;
}

procedure TFmNFSe_RPSPesq.ConsultaRPSselecionado_Novo();
begin
  DmNFSe_0000.ReopenNFSeDPSCab(QrNFSeDPSCabCodigo.Value);
  //
  UnNFSe_PF_0201.ConsultaRPSselecionado_Novo(Self, QrNFSeDPSCabEmpresa.Value,
  QrNFSeDPSCabAmbiente.Value, QrNFSeDPSCabCodigo.Value,
  QrNFSeDPSCabRpsIDNumero.Value, QrNFSeDPSCabRpsIDSerie.Value,
  QrNFSeDPSCabRpsIDTipo.Value, Self.Caption, QrNFSeDPSCab, LaAviso1, LaAviso2);
end;

procedure TFmNFSe_RPSPesq.DiretriodoarquivoXML1Click(Sender: TObject);
{
var
  Dir, Arq, Ext, Arquivo: String;
}
begin
{
  if not DmNFe_0000.ReopenEmpresa(DModG.QrEmpresasCodigo.Value) then Exit;
  //
  Arq := QrNFSeDPSCabId.Value;
  Ext := NFE_EXT_NFE_XML;
  //
  if DmNFe_0000.ObtemDirXML(Ext, Dir, True) then
  begin
    Arquivo := Dir + Arq + Ext;
    Geral.MensagemBox(Arquivo, 'Caminho do arquivo XML', MB_OK+MB_ICONWARNING);
  end;
}
end;

procedure TFmNFSe_RPSPesq.dmkDBGrid1DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
  DmNFSe_0000.ColoreStatusNFeEmDBGrid(TDBGrid(dmkDBGrid1), Rect, DataCol, Column,
    State, Trunc(QrNFSeDPSCabStatusDPS.Value), 'StatusDPS');
end;

procedure TFmNFSe_RPSPesq.EdClienteChange(Sender: TObject);
begin
  if not EdFilial.Focused then
    FechaNFSeDPSCab;
end;

procedure TFmNFSe_RPSPesq.EdClienteExit(Sender: TObject);
begin
  FechaNFSeDPSCab;
end;

procedure TFmNFSe_RPSPesq.EdFilialChange(Sender: TObject);
begin
  //if not EdFilial.Focused then
    FechaNFSeDPSCab;
end;

procedure TFmNFSe_RPSPesq.EdFilialExit(Sender: TObject);
begin
  //FechaNFSeDPSCab;
end;

procedure TFmNFSe_RPSPesq.Erronaemisso1Click(Sender: TObject);
begin
  CancelaNFSe(1);
end;

procedure TFmNFSe_RPSPesq.FormActivate(Sender: TObject);
begin
  if TFmNFSe_RPSPesq(Self).Owner is TApplication then
  begin
    MyObjects.CorIniComponente();
  end;
end;

procedure TFmNFSe_RPSPesq.FormCreate(Sender: TObject);
begin
  try
    Screen.Cursor       := crHourGlass;
    ImgTipo.SQLType     := stPsq;
    CBFilial.ListSource := DModG.DsEmpresas;
    //
    DModG.ReopenEmpresas(VAR_USUARIO, 0, EdFilial, CBFilial);
    QrClientes.Open;
    QrIntermediario.Open;
    TPDataI.Date := Date - 10; // m�x permitido pelo fisco
    TPDataF.Date := Date;
    //
    ReopenNFSeDPSCab(0, False);
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmNFSe_RPSPesq.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmNFSe_RPSPesq.FormShow(Sender: TObject);
begin
{$IfNDef cSkinRank} //Berlin
{$IfNDef cAlphaSkin} //Berlin
  FmMyGlyfs.DefineGlyfs(TForm(Sender));
{$EndIf}
{$EndIf}
{$IfDef cSkinRank} //Berlin
  if FmPrincipal.Sd1.Active then
    FmMyGlyfs.DefineGlyfsTDI(FmPrincipal.sd1, Sender);
{$EndIf}
{$IfDef cAlphaSkin} //Berlin
  if FmPrincipal.sSkinManager1.Active then
    FmMyGlyfs.DefineGlyfsTDI2(FmPrincipal.sSkinManager1, Sender);
{$EndIf}
end;

procedure TFmNFSe_RPSPesq.frxListaNFesGetValue(const VarName: string;
  var Value: Variant);
begin
  if VarName = 'VARF_EMPRESA' then
    Value := CBFilial.Text
  else
  if VarName = 'VARF_TERCEIRO' then
    Value := dmkPF.ParValueCodTxt('', CBCliente.Text, EdCliente.ValueVariant)
  else
  if VarName = 'VARF_PERIODO' then
    Value := dmkPF.PeriodoImp2(
    TPDataI.Date, TPDataF.Date, True, True, '', '', '')
  else
  if VarName = 'VARF_STATUS' then
  begin
    if Ck100e101.Checked then
      Value := 'AUTORIZADAS E CANCELADAS'
    else
      Value := 'QUALQUER';
  end
  else
  if VarName = 'VARF_APENAS_EMP' then
  begin
    case RGQuemEmit.ItemIndex of
      0: Value := 'Notas Fiscais Emitidas';
      1: Value := 'Notas Fiscais Recebidas';
      2: Value := 'Notas Fiscais Emitidas e Recebidas';
      else Value := 'Notas Fiscais ????';
    end;
  end
  else
  if VarName = 'VARF_AMBIENTE' then
  begin
    case RGAmbiente.ItemIndex of
      0: Value := 'AMBOS';
      1: Value := 'PRODU��O';
      2: Value := 'HOMOLOGA��O';
    end;
  end
end;

procedure TFmNFSe_RPSPesq.LdoarquivoXML1Click(Sender: TObject);
var
  Empresa, Ambiente, Codigo: Integer;
  Data: TDateTime;
  XML: AnsiString;
  Dir, Arq: String;
begin
  if Geral.MB_Pergunta(
  'Confirma a leitura do arquivo XML da NFS-e selecionada do diret�rio padr�o?') <> ID_YES then
    Exit;
  //
  Screen.Cursor := crHourGlass;
  try
    Empresa  := QrNFSeDPSCabEmpresa.Value;
    Ambiente := QrNFSeDPSCabAmbiente.Value;
    Codigo   := QrNFSeDPSCabCodigo.Value;
    Data     := QrNFSeDPSCabRpsDataEmissao.Value;
    //
    // Carregar arquivo de emiss�o de RPS para saber o diret�rio padr�o
    Dir := UnNFSeABRASF_0201.ObtemDiretorioXML_Padrao(TTabelaArqXML.txmRPS,
      Empresa, Data);
    if MyObjects.FileOpenDialog(Self, Dir, '', 'Arquivo XML', '*.xml', [], Arq) then
    begin
      if FileExists(Arq) then
      begin
        if UnNFSeABRASF_0201.LerXML_Arquivo(DmNFSe_0000.XMLDocument1, Ambiente, Arq) then
        //Geral.MB_Aviso('Tente executar a a��o novamente!');
        begin
          ReopenPesqAtual();
          QrNFSeDPSCab.Locate('Codigo', Codigo, []);
        end;
      end;
    end;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmNFSe_RPSPesq.Listadasnotaspesquisadas1Click(Sender: TObject);
{
var
  ide_nNF, ide_serie, cStat, Ordem, Status: Integer;
  ide_dEmi: String;
  ICMSTot_vProd, ICMSTot_vST, ICMSTot_vFrete,
  ICMSTot_vSeg, ICMSTot_vIPI, ICMSTot_vOutro,
  ICMSTot_vDesc, ICMSTot_vNF, ICMSTot_vBC,
  ICMSTot_vICMS, ICMSTot_vPIS, ICMSTot_vCOFINS: Double;
  NOME_tpEmis, NOME_tpNF, Motivo,
  ide_AAMM_AA, ide_AAMM_MM, Id, infCanc_dhRecbto, infCanc_nProt: String;
}
begin
{
  Screen.Cursor := crHourGlass;
  try
    F_NFe_100 := GradeCriar.RecriaTempTableNovo(ntrttNFe_100, DmodG.QrUpdPID1, False);
    F_NFe_101 := GradeCriar.RecriaTempTableNovo(ntrttNFe_101, DmodG.QrUpdPID1, False);
    F_NFe_XXX := GradeCriar.RecriaTempTableNovo(ntrttNFe_XXX, DmodG.QrUpdPID1, False);
    //
    QrNFSeDPSCab.First;
    while not QrNFSeDPSCab.Eof do
    begin
      // 0..999
      cStat            := Trunc(QrNFSeDPSCabcStat.Value);
      ide_nNF          := QrNFSeDPSCabide_nNF.Value;
      ide_serie        := QrNFSeDPSCabide_serie.Value;
      ide_dEmi         := Geral.FDT(QrNFSeDPSCabide_dEmi.Value, 1);
      // 100
      ICMSTot_vProd    := QrNFSeDPSCabICMSTot_vProd.Value;
      ICMSTot_vST      := QrNFSeDPSCabICMSTot_vST.Value;
      ICMSTot_vFrete   := QrNFSeDPSCabICMSTot_vFrete.Value;
      ICMSTot_vSeg     := QrNFSeDPSCabICMSTot_vSeg.Value;
      ICMSTot_vIPI     := QrNFSeDPSCabICMSTot_vIPI.Value;
      ICMSTot_vOutro   := QrNFSeDPSCabICMSTot_vOutro.Value;
      ICMSTot_vDesc    := QrNFSeDPSCabICMSTot_vDesc.Value;
      ICMSTot_vNF      := QrNFSeDPSCabICMSTot_vNF.Value;
      ICMSTot_vBC      := QrNFSeDPSCabICMSTot_vBC.Value;
      ICMSTot_vICMS    := QrNFSeDPSCabICMSTot_vICMS.Value;
      ICMSTot_vPIS     := QrNFSeDPSCabICMSTot_vPIS.Value;
      ICMSTot_vCOFINS  := QrNFSeDPSCabICMSTot_vCOFINS.Value;
      NOME_tpEmis      := QrNFSeDPSCabNOME_tpEmis.Value;
      NOME_tpNF        := QrNFSeDPSCabNOME_tpNF.Value;
      // 101
      ide_AAMM_AA      := Geral.FDT(QrNFSeDPSCabide_dEmi.Value, 21);
      ide_AAMM_MM      := Geral.FDT(QrNFSeDPSCabide_dEmi.Value, 22);
      Motivo           := QrNFSeDPSCabinfCanc_xJust.Value;
      Id               := QrNFSeDPSCabId.Value;
      infCanc_dhRecbto := Geral.FDT(QrNFSeDPSCabinfCanc_dhRecbto.Value, 9);
      infCanc_nProt    := QrNFSeDPSCabinfCanc_nProt.Value;
      // Outros
      Status         := QrNFSeDPSCabStatus.Value;
      //
      case cStat of
        100:
        begin
          UMyMod.SQLInsUpd(DModG.QrUpdPID1, stIns, F_NFe_100, True, [
          'Ordem',
          'ide_nNF', 'ide_serie', 'ide_dEmi',
          'ICMSTot_vProd', 'ICMSTot_vST', 'ICMSTot_vFrete',
          'ICMSTot_vSeg', 'ICMSTot_vIPI', 'ICMSTot_vOutro',
          'ICMSTot_vDesc', 'ICMSTot_vNF', 'ICMSTot_vBC',
          'ICMSTot_vICMS', 'ICMSTot_vPIS', 'ICMSTot_vCOFINS',
          'NOME_tpEmis', 'NOME_tpNF'], [
          ], [
          0,
          ide_nNF, ide_serie, ide_dEmi,
          ICMSTot_vProd, ICMSTot_vST, ICMSTot_vFrete,
          ICMSTot_vSeg, ICMSTot_vIPI, ICMSTot_vOutro,
          ICMSTot_vDesc, ICMSTot_vNF, ICMSTot_vBC,
          ICMSTot_vICMS, ICMSTot_vPIS, ICMSTot_vCOFINS,
          NOME_tpEmis, NOME_tpNF], [
          ], False);
        end;
        101:
        begin
          Ordem := 0;
          UMyMod.SQLInsUpd(DModG.QrUpdPID1, stIns, F_NFe_101, False, [
          'Ordem', 'ide_nNF',
          'ide_serie', 'ide_AAMM_AA', 'ide_AAMM_MM',
          'ide_dEmi', 'NOME_tpEmis', 'NOME_tpNF',
          'Id', 'infCanc_dhRecbto', 'infCanc_nProt', 'Motivo'], [
          ], [
          Ordem, ide_nNF,
          ide_serie, ide_AAMM_AA, ide_AAMM_MM,
          ide_dEmi, NOME_tpEmis, NOME_tpNF,
          Id, infCanc_dhRecbto, infCanc_nProt, Motivo], [
          ], False);
        end;
        else begin
          if Status < 100 then
            Ordem := 2
          else
            Ordem := 1;
          Motivo := NFeXMLGeren.Texto_StatusNFe(Status, QrNFSeDPSCabversao.Value);
          //
          UMyMod.SQLInsUpd(DModG.QrUpdPID1, stIns, F_NFe_XXX, False, [
          'Ordem', 'cStat', 'ide_nNF',
          'ide_serie', 'ide_AAMM_AA', 'ide_AAMM_MM',
          'ide_dEmi', 'NOME_tpEmis', 'NOME_tpNF',
          'Status', 'Motivo'], [
          ], [
          Ordem, cStat, ide_nNF,
          ide_serie, ide_AAMM_AA, ide_AAMM_MM,
          ide_dEmi, NOME_tpEmis, NOME_tpNF,
          Status, Motivo], [
          ], False);
        end;
      end;
      //
      QrNFSeDPSCab.Next;
    end;
    //
    QrNFe_100.Close;
    QrNFe_100.Database := DModG.MyPID_DB;
    QrNFe_100.Open;
    //
    QrNFe_101.Close;
    QrNFe_101.Database := DModG.MyPID_DB;
    QrNFe_101.Open;
    //
    QrNFe_XXX.Database := DModG.MyPID_DB;
    MyObjects.frxMostra(frxListaNFes, 'Lista de NF-e(s)');
  finally
    Screen.Cursor := crDefault;
  end;
}
end;

procedure TFmNFSe_RPSPesq.LXMLdoBD1Click(Sender: TObject);
var
  Empresa, Ambiente, Codigo: Integer;
  XML: AnsiString;
  Arq: String;
begin
  if Geral.MB_Pergunta(
  'Confirma a leitura do arquivo XML da NFS-e selecionada do banco de dados?') <> ID_YES then
    Exit;
  Empresa  := QrNFSeDPSCabEmpresa.Value;
  Ambiente := QrNFSeDPSCabAmbiente.Value;
  Codigo   := QrNFSeDPSCabCodigo.Value;
  //
  if DmNFSe_0000.ReopenNfseArq(txmRPS, Empresa, Ambiente, Codigo, XML, Caption) then
    UnNFSeABRASF_0201.LerXML_DoXML(DmNFSe_0000.XMLDocument1, Ambiente, XML);
end;

procedure TFmNFSe_RPSPesq.MostraFormNFSe_Edit_0201(SQLType: TSQLType;
  Servico: TnfseFormaGerarNFSe);
const
  Prestador     = 0;
  Tomador       = 0;
  Intermediario = 0;
  MeuServico    = 0;
  ItemListSrv   = 0;
  Valor         = 0;
  Discriminacao = '';
  GeraNFSe = True;
  //
  TabedForm = nil;
var
  DPS, NFSeFatCab: Integer;
  SerieNF: String;
  NumNF: Integer;
begin
  NFSeFatCab := QrNFSeDPSCabNFSeFatCab.Value;
  DPS := QrNFSeDPSCabCodigo.Value;
  //
  UnNFSe_PF_0201.MostraFormNFSe(SQLType,
  Prestador, Tomador, Intermediario, MeuServico, ItemListSrv,
  Discriminacao, GeraNFSe, NFSeFatCab, DPS, Servico, QrNFSeDPSCab, Valor,
  SerieNF, NumNF, TabedForm);
{
  if ((QrNFSeDPSCabStatus.Value <= DmNFSe_0000.stepAdedLote())
  and (SQLType = stUpd)) or (Servico = fgnSubstituicaoNFSe) then
  begin
    Filial := DModG.ObtemFilialDeEntidade(QrNFSeDPSCabEmpresa.Value);
    if DBCheck.CriaFm(TFmNFSe_Edit_0201, FmNFSe_Edit_0201, afmoNegarComAviso) then
    begin
      with FmNFSe_Edit_0201 do
      begin
        ImgTipo.SQLType := SQLType;
        FQrNFSeDPSCab := QrNFSeDPSCab;
        FFormaGerarNFSe := Servico;
        //
        if (SQLType = stIns) and (Servico = fgnSubstituicaoNFSe) then
        begin
          EdRpsIDNumero.ValueVariant               := 0;
          EdRpsIDSerie.ValueVariant                := QrNFSeDPSCabRpsIDSerie.Value;
          EdRpsIDTipo.ValueVariant                 := QrNFSeDPSCabRpsIDTipo.Value;
          CBRpsIDTipo.KeyValue                     := QrNFSeDPSCabRpsIDTipo.Value;
          EdCodigo.ValueVariant                    := 0;
          //
          EdSubstNumero.ValueVariant               := QrNFSeDPSCabRpsIDNumero.Value;
          EdSubstSerie.Text                        := QrNFSeDPSCabRpsIDSerie.Value;
          EdSubstTipo.ValueVariant                 := QrNFSeDPSCabRpsIDTipo.Value;
          CBSubstTipo.KeyValue                     := QrNFSeDPSCabRpsIDTipo.Value;
        end else
        if (SQLType = stUpd) and (Servico = fgnSubstituicaoNFSe) then
        begin
          EdRpsIDNumero.ValueVariant               := QrNFSeDPSCabRpsIDNumero.Value;
          EdRpsIDSerie.ValueVariant                := QrNFSeDPSCabRpsIDSerie.Value;
          EdRpsIDTipo.ValueVariant                 := QrNFSeDPSCabRpsIDTipo.Value;
          CBRpsIDTipo.KeyValue                     := QrNFSeDPSCabRpsIDTipo.Value;
          EdCodigo.ValueVariant                    := QrNFSeDPSCabCodigo.Value;
          //
          EdSubstNumero.ValueVariant               := QrNFSeDPSCabSubstNumero.Value;
          EdSubstSerie.Text                        := QrNFSeDPSCabSubstSerie.Value;
          EdSubstTipo.ValueVariant                 := QrNFSeDPSCabSubstTipo.Value;
          CBSubstTipo.KeyValue                     := QrNFSeDPSCabSubstTipo.Value;
        end else
        if (SQLType = stUpd) and (Servico = fgnLoteRPSSincrono) then
        begin
          EdRpsIDNumero.ValueVariant               := QrNFSeDPSCabRpsIDNumero.Value;
          EdRpsIDSerie.ValueVariant                := QrNFSeDPSCabRpsIDSerie.Value;
          EdRpsIDTipo.ValueVariant                 := QrNFSeDPSCabRpsIDTipo.Value;
          CBRpsIDTipo.KeyValue                     := QrNFSeDPSCabRpsIDTipo.Value;
          EdCodigo.ValueVariant                    := QrNFSeDPSCabCodigo.Value;
          //
        end else
        begin
          Geral.MB_Erro('Forma de emiss�o de NFSe n�o implemetada!');
          Destroy;
          //
          Exit;
        end;

        EdEmpresa.ValueVariant                   := Filial;
        CBEmpresa.KeyValue                       := Filial;
        TPRpsDataEmissao.Date                    := QrNFSeDPSCabRpsDataEmissao.Value;
        EdRpsHoraEmissao.Text                    := Geral.FDT(QrNFSeDPSCabRpsHoraEmissao.Value, 100);
        TPCompetencia.Date                       := QrNFSeDPSCabCompetencia.Value;
        EdIntermediario.ValueVariant             := QrNFSeDPSCabIntermediario.Value;
        CBIntermediario.KeyValue                 := QrNFSeDPSCabIntermediario.Value;
        EdCliente.ValueVariant                   := QrNFSeDPSCabCliente.Value;
        CBCliente.KeyValue                       := QrNFSeDPSCabCliente.Value;
        EdNFSeSrvCad.ValueVariant                := QrNFSeDPSCabNFSeSrvCad.Value;
        CBNFSeSrvCad.KeyValue                    := QrNFSeDPSCabNFSeSrvCad.Value;
        //
        EdItemListaServico.ValueVariant          := QrNFSeDPSCabItemListaServico.Value;
        CBItemListaServico.KeyValue              := QrNFSeDPSCabItemListaServico.Value;
        EdCodigoCnae.ValueVariant                := QrNFSeDPSCabCodigoCnae.Value;
        CBCodigoCnae.KeyValue                    := QrNFSeDPSCabCodigoCnae.Value;
        MeDiscriminacao.Text                     := QrNFSeDPSCabDiscriminacao.Value;
        EdValorServicos.ValueVariant             := QrNFSeDPSCabValorServicos.Value;
        EdValorDeducoes.ValueVariant             := QrNFSeDPSCabValorDeducoes.Value;
        EdAliquota.ValueVariant                  := QrNFSeDPSCabAliquota.Value;
        EdDescontoIncondicionado.ValueVariant    := QrNFSeDPSCabDescontoIncondicionado.Value;
        EdValorIss.ValueVariant                  := QrNFSeDPSCabValorIss.Value;
        EdDescontoCondicionado.ValueVariant      := QrNFSeDPSCabDescontoCondicionado.Value;
        EdValorPis.ValueVariant                  := QrNFSeDPSCabValorPis.Value;
        EdValorCofins.ValueVariant               := QrNFSeDPSCabValorCofins.Value;
        EdValorInss.ValueVariant                 := QrNFSeDPSCabValorInss.Value;
        EdValorIr.ValueVariant                   := QrNFSeDPSCabValorIr.Value;
        EdValorCsll.ValueVariant                 := QrNFSeDPSCabValorCsll.Value;
        EdOutrasRetencoes.ValueVariant           := QrNFSeDPSCabOutrasRetencoes.Value;
        EdIssRetido.ValueVariant                 := QrNFSeDPSCabIssRetido.Value;
        CBIssRetido.KeyValue                     := QrNFSeDPSCabIssRetido.Value;
        EdResponsavelRetencao.ValueVariant       := QrNFSeDPSCabResponsavelRetencao.Value;
        CBResponsavelRetencao.KeyValue           := QrNFSeDPSCabResponsavelRetencao.Value;
        EdExigibilidadeISS.ValueVariant          := QrNFSeDPSCabExigibilidadeISS.Value;
        CBExigibilidadeISS.KeyValue              := QrNFSeDPSCabExigibilidadeISS.Value;
        EdNumeroProcesso.ValueVariant            := QrNFSeDPSCabNumeroProcesso.Value;
        EdConstrucaoCivilCodigoObra.Text         := QrNFSeDPSCabConstrucaoCivilCodigoObra.Value;
        EdConstrucaoCivilArt.Text                := QrNFSeDPSCabConstrucaoCivilArt.Value;
        EdCodigoTributacaoMunicipio.ValueVariant := QrNFSeDPSCabCodigoTributacaoMunicipio.Value;
        EdQuemPagaISS.ValueVariant               := QrNFSeDPSCabQuemPagaISS.Value;
        CBQuemPagaISS.KeyValue                   := QrNFSeDPSCabQuemPagaISS.Value;
        EdOptanteSimplesNacional.ValueVariant    := QrNFSeDPSCabOptanteSimplesNacional.Value;
        CBOptanteSimplesNacional.KeyValue        := QrNFSeDPSCabOptanteSimplesNacional.Value;
        EdIncentivoFiscal.ValueVariant           := QrNFSeDPSCabIncentivoFiscal.Value;
        CBIncentivoFiscal.KeyValue               := QrNFSeDPSCabIncentivoFiscal.Value;
        EdRegimeEspecialTributacao.ValueVariant  := QrNFSeDPSCabRegimeEspecialTributacao.Value;
        CBRegimeEspecialTributacao.KeyValue      := QrNFSeDPSCabRegimeEspecialTributacao.Value;
        //
        ShowModal;
        Destroy;
      end;
    end;
  end else Geral.MB_Aviso(
  'Status do RPS deve ser menor que 50 para poder ser editado!')
}
end;

procedure TFmNFSe_RPSPesq.N2Servionoprestado1Click(Sender: TObject);
begin
  CancelaNFSe(2);
end;

procedure TFmNFSe_RPSPesq.N3Errodeassinatura1Click(Sender: TObject);
begin
  Geral.MB_Erro('Uso restrito da Administra��o Tribut�ria Municipal');
end;

procedure TFmNFSe_RPSPesq.N4Duplicidadedanota1Click(Sender: TObject);
begin
  CancelaNFSe(4);
end;

procedure TFmNFSe_RPSPesq.N5Errodeprocessamento1Click(Sender: TObject);
begin
  Geral.MB_Erro('Uso restrito da Administra��o Tribut�ria Municipal');
end;

procedure TFmNFSe_RPSPesq.NFe1Click(Sender: TObject);
begin
  Geral.MensagemBox(QrArqXML_NFe.Value, 'XML NFe', MB_OK+MB_ICONINFORMATION);
end;

procedure TFmNFSe_RPSPesq.PMArqPopup(Sender: TObject);
begin
  NFe1         .Enabled := QrArqXML_NFe.Value <> '';
  Autorizao1   .Enabled := QrArqXML_Aut.Value <> '';
  Cancelamento2.Enabled := QrArqXML_Can.Value <> '';
end;

procedure TFmNFSe_RPSPesq.PorfaixadeNFSe1Click(Sender: TObject);
{
var
  Retorno, DocPres, Txt: String;
  (*Nota: TNfse;
  I,*) NFSeIni, NFSeFim: Integer;
}
begin
  (*
  // ini 2023-12-20 Parei Aqui!

  //////////////////////////////////////////////////////////////////////////////
   Usar DmNFSe_0000.ACBrNFSeX1
  //////////////////////////////////////////////////////////////////////////////
  Geral.MB_Aviso('Falta implementar 2023-12-20!');
  NFSeIni := 0;
  NFSeFim := 0;
  //Obtem NFSe inicial
  if InputQuery('N�mero inicial da NFS-e', 'N�mero inicial da NFS-e', Txt) then
    NFSeIni := Geral.IMV(Txt);
  //Obtem NFSe final
  if InputQuery('N�mero final da NFS-e', 'N�mero final da NFS-e', Txt) then
    NFSeFim := Geral.IMV(Txt);
  //
  if (NFSeIni = 0) or (NFSeFim = 0) then
  begin
    Geral.MensagemBox('Numera��o inv�lida!', 'Aviso', MB_OK+MB_ICONWARNING);
    Exit;
  end;
  try
    Screen.Cursor := crHourGlass;
    //
    UnNFSe_PF_0201.ConfiguraComponenteACBrNFSe(DmodG.QrEmpresasCodigo.Value,
      DmNFSe_0000.ACBrNFSe1);
    DmNFSe_0000.ACBrNFSe1.NotasFiscais.Clear;
    //
    DmNFSe_0000.ACBrNFSe1.NotasFiscais.Add;
    { TODO -oMarco Luciano Arend -cConsulta de NFSe :           NFSe: Consulta de NFSe por Faixa }
    {
    function TDmkACBrNFSe.ConsutarNFSeFaixa(ACnpj, AInscricaoMunicipal: String;
    ANumeroNFSeInicial, ANumeroNFSeFinal, APagina: Integer): Boolean;
    }
    DocPres := QrNFSeDPSCabPrestaCnpj.Value;
    if DocPres = '' then
      DocPres := QrNFSeDPSCabPrestaCpf.Value;
    //
    if DmNFSe_0000.ACBrNFSe1.ConsultarNFSeFaixa(DocPres,
      QrNFSeDPSCabPrestaInscricaoMunicipal.Value, NFSeIni, NFSeFim, 1) then
    begin
      Retorno := DmNFSe_0000.ACBrNFSe1.WebServices.ConsNfseFaixa.ArquivoRetorno;
      {
      for I := 0 to DmNFSe_0000.ACBrNFSe1.WebServices.ConsNfseFaixa.NFSeRetorno.ListaNfse.CompNfse.Count - 1 do
      begin
        Nota := DmNFSe_0000.ACBrNFSe1.WebServices.ConsNfseFaixa.NFSeRetorno.ListaNfse.CompNfse.Items[I].Nfse;
        ShowMessage(Nota.Numero);
      end;
      }
      Geral.MB_Info(Retorno);
      //ShowMessage('OK');
    end;
  finally
    Screen.Cursor := crDefault;
  end;
*)
  // fim 2023-12-20 Parei Aqui!
end;

procedure TFmNFSe_RPSPesq.PreviewdaNFe1Click(Sender: TObject);
{
var
  I: Integer;
  Report: TfrxReport;
  Memo: TfrxMemoView;
}
begin
{
  Report := frxA4A_002;
  try
    for I := 0 to Report.ComponentCount - 1 do
    begin
      if Report.Components[I] is TfrxMemoView then
      begin
        Memo := TfrxMemoView(Report.Components[I]);
        //Memo.Frame.Typ := [];
        case Memo.Tag of
          0: Memo.Visible := False;
          1: ; // Deixa como est�
          2: Memo.Visible := True;
        end;
      end
      else if Report.Components[I] is TfrxLineView then
        TfrxLineView(Report.Components[I]).Visible := False
      else if Report.Components[I] is TfrxBarCodeView then
        TfrxBarCodeView(Report.Components[I]).Visible := False
    end;
    DefineFrx(Report, ficMostra);
  finally
    //
  end;
}
end;

procedure TFmNFSe_RPSPesq.QrClientesBeforeClose(DataSet: TDataSet);
begin
  BtLeArq.Enabled     := False;
  BtImprime.Enabled   := False;
  BtEnvia.Enabled     := False;
  BtEvento.Enabled    := False;
  BtCancela.Enabled   := False;
  BtSubstitui.Enabled := False;
  BtExclui.Enabled    := False;
end;

procedure TFmNFSe_RPSPesq.QrNFSeDPSCabAfterOpen(DataSet: TDataSet);
begin
  PnDados.Visible    := True;
  BtAltera.Enabled   := QrNFSeDPSCab.RecordCount > 0;
  BtConsulta.Enabled := QrNFSeDPSCab.RecordCount > 0;
  SbImprime.Enabled  := QrNFSeDPSCab.RecordCount > 0;
  SbNovo.Enabled     := QrNFSeDPSCab.RecordCount > 0;
end;

procedure TFmNFSe_RPSPesq.QrNFSeDPSCabAfterScroll(DataSet: TDataSet);
begin
  BtExclui.Enabled    := False;
  BtCancela.Enabled   := False;
  BtSubstitui.Enabled := False;
  BtAltera.Enabled    := False;
  BtImprime.Enabled   := False;
  BtEnvia.Enabled     := False;
  BtEvento.Enabled    := False;
  //
  BtConsulta.Enabled  := True;
  BtLeArq.Enabled     := True; // 2023-12-23

  //
  case Trunc(QrNFSeDPSCabStatusDPS.Value) of
    0..99,201..999:
    begin
      BtExclui.Enabled := True;
      BtAltera.Enabled := True;
    end;
    100:
    begin
      BtEvento.Enabled  := False;
      //BtArq.Enabled     := False;
      BtCancela.Enabled := True;
      //BtImprime.Enabled := True;
    end;
  end;
  //ReopenNFSeDPSCabMsg();
end;

procedure TFmNFSe_RPSPesq.QrNFSeDPSCabBeforeClose(DataSet: TDataSet);
begin
  PnDados.Visible     := False;
  SbImprime.Enabled   := False;
  SbNovo.Enabled      := False;
  //
  BtExclui.Enabled    := False;
  BtCancela.Enabled   := False;
  BtSubstitui.Enabled := False;
  BtConsulta.Enabled  := False;
  BtAltera.Enabled    := False;
end;

procedure TFmNFSe_RPSPesq.QrNFSeDPSCabCalcFields(DataSet: TDataSet);
var
  Doc: String;
begin
  Doc := Geral.FormataCNPJ_TT(QrNFSeDPSCabTomaCnpjCpf.Value);
  //
  QrNFSeDPSCabTomaCnpjCpf_TXT.Value := Doc;
end;

procedure TFmNFSe_RPSPesq.QrNFe_XXXCalcFields(DataSet: TDataSet);
begin
  case QrNFe_XXXOrdem.Value of
    0: QrNFe_XXXNOME_ORDEM.Value := 'CANCELADA';
    1: QrNFe_XXXNOME_ORDEM.Value := 'REJEITADA';
    2: QrNFe_XXXNOME_ORDEM.Value := 'N�O ENVIADA';
    else QrNFe_XXXNOME_ORDEM.Value := '? ? ? ? ?';
  end;
end;

procedure TFmNFSe_RPSPesq.FechaNFSeDPSCab();
begin
  QrNFSeDPSCab.Close;
end;

procedure TFmNFSe_RPSPesq.RGOrdem1Click(Sender: TObject);
begin
  FechaNFSeDPSCab;
end;

procedure TFmNFSe_RPSPesq.RGOrdem2Click(Sender: TObject);
begin
  FechaNFSeDPSCab;
end;

procedure TFmNFSe_RPSPesq.RPSselecionado1Click(Sender: TObject);
begin
  ConsultaRPSSelecionado_Novo();
end;

procedure TFmNFSe_RPSPesq.ReopenNFSeDPSCab(Codigo: Integer; So_O_ID: Boolean);
var
  //DOC1,
  Empresa, Ord2: String;
  SQL_GERAL, SQL_WHERE, SQL_EMPRESA, SQL_AMBIENTE, SQL_CLIENTE, SQL_INTERMED,
  SQL_QUEMEMIT, SQL_ORDEM: String;
begin
  Screen.Cursor := crHourGlass;
  if (EdFilial.ValueVariant <> Null) and (EdFilial.ValueVariant <> 0) then
    Empresa := FormatFloat('0', DModG.QrEmpresasCodigo.Value)
  else Empresa := '';
  QrNFSeDPSCab.Close;
  if Empresa <> '' then
  begin
    SQL_GERAL := Geral.ATS([
    'SELECT IF (cli.Tipo=0, cli.RazaoSocial, cli.Nome) NO_CLI, dps.Status StatusDPS, ',
    'IF(dps.TomaCnpj <> "", dps.TomaCnpj, dps.TomaCpf) TomaCnpjCpf, ',
    'IF (ine.Tipo=0, ine.RazaoSocial, ine.Nome) NO_INT, nfs.NfsNumero, dps.* ',
    'FROM nfsedpscab dps ',
    'LEFT JOIN nfsenfscab nfs ON nfs.Empresa = dps.Empresa ',
    'AND nfs.Ambiente = dps.Ambiente ',
    'AND nfs.NfsRpsIDSerie = dps.RpsIDSerie ',
    'AND nfs.NfsRpsIDTipo = dps.RpsIDTipo  ',
    'AND nfs.NfsRpsIDNumero = dps.RpsIDNumero ',
    'LEFT JOIN entidades cli ON cli.Codigo = dps.Cliente ',
    'LEFT JOIN entidades ine ON ine.Codigo = dps.Intermediario ',
    '']);
    if So_O_ID then
    begin
      SQL_WHERE := 'WHERE dps.Codigo=' + Geral.FF0(Codigo);
    end else begin
      SQL_WHERE := dmkPF.SQL_Periodo('WHERE dps.RpsDataEmissao ',
        TPDataI.Date, TPDataF.Date, True, True);
      SQL_EMPRESA := 'AND dps.Empresa = ' + Empresa;
      case RGAmbiente.ItemIndex of
        0: SQL_AMBIENTE := ''; // Tudo
        1: SQL_AMBIENTE := 'AND dps.Ambiente=1';
        2: SQL_AMBIENTE := 'AND dps.Ambiente=2';
      end;
      if EdCliente.ValueVariant > 0 then
      begin
        if QrClientesTipo.Value = 0 then
          SQL_CLIENTE := 'AND dps.TomaCnpj=' + Geral.TFD(QrClientesCNPJ.Value, 14, siPositivo)
        else
          SQL_CLIENTE := 'AND dps.TomaCpf=' + Geral.TFD(QrClientesCPF.Value, 11, siPositivo);
      end;
      if EdIntermediario.ValueVariant <> 0 then
      begin
        if QrIntermediarioTipo.Value = 0 then
          SQL_INTERMED := 'AND dps.IntermeCnpj=' + Geral.TFD(QrIntermediarioCNPJ.Value, 14, siPositivo)
        else
          SQL_INTERMED := 'AND dps.IntermeCpf=' + Geral.TFD(QrIntermediarioCPF.Value, 11, siPositivo);
      end;
      case RGQuemEmit.ItemIndex of
        0: SQL_QUEMEMIT := 'AND dps.PrestaCnpj="' + DModG.QrEmpresasCNPJ_CPF.Value + '"';
        1: SQL_QUEMEMIT := 'AND (dps.PrestaCnpj<>"' + DModG.QrEmpresasCNPJ_CPF.Value + '" OR nfs.DpsPrestaCnpj IS NULL)';
      end;
      case RGOrdem2.ItemIndex of
        0: Ord2 := '';
        1: Ord2 := 'DESC';
      end;
      case RGOrdem1.ItemIndex of
        0: SQL_ORDEM := 'ORDER BY dps.RpsDataEmissao ' + Ord2 + ', dps.RpsIDNumero ' + Ord2;
        1: SQL_ORDEM := 'ORDER BY dps.RpsIDNumero ' + Ord2 + ', dps.RpsDataEmissao ' + Ord2;
        2: SQL_ORDEM := 'ORDER BY dps.Status ' + Ord2 + ', dps.RpsIDNumero ' + Ord2;
        3: SQL_ORDEM := 'ORDER BY dps.Status ' + Ord2 + ', dps.RpsDataEmissao ' + Ord2;
      end;
      //
      UnDmkDAC_PF.AbreMySQLQuery0(QrNFSeDPSCab, Dmod.MyDB, [
      SQL_GERAL, SQL_WHERE, SQL_EMPRESA, SQL_AMBIENTE, SQL_CLIENTE, SQL_INTERMED, SQL_ORDEM,
      '']);
      QrNFSeDPSCab.Locate('Codigo', Codigo, []);
    end;
  end;
  Screen.Cursor := crDefault;
end;

procedure TFmNFSe_RPSPesq.ReopenPesqAtual();
var
  Codigo: Integer;
begin
  Codigo := 0;
  if (QrNFSeDPSCab.State <> dsInactive) then Codigo := QrNFSeDPSCabCodigo.Value;
  ReopenNFSeDPSCab(Codigo, False);
end;

procedure TFmNFSe_RPSPesq.RGAmbienteClick(Sender: TObject);
begin
  FechaNFSeDPSCab;
end;

procedure TFmNFSe_RPSPesq.SbImprimeClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMImprime, SbImprime);
end;

procedure TFmNFSe_RPSPesq.Servicosprestados1Click(Sender: TObject);
(*
var
  Retorno, DocPres, DocTom, DocInt, NireTom, NireInt: String;
  (*
  I: Integer;
  Nota: TNfse;
  *)
begin
(*
  // ini 2023-12-20 Parei Aqui!
  Geral.MB_Aviso('Falta implementar 2023-12-20!');
  // fim 2023-12-20 Parei Aqui!
  try
    Screen.Cursor := crHourGlass;
    //
    UnNFSe_PF_0201.ConfiguraComponenteACBrNFSe(DmodG.QrEmpresasCodigo.Value,
      DmNFSe_0000.ACBrNFSe1);
    DmNFSe_0000.ACBrNFSe1.NotasFiscais.Clear;
    //
    //Prestador
    DocPres := QrNFSeDPSCabPrestaCnpj.Value;
    if DocPres = '' then
      DocPres := QrNFSeDPSCabPrestaCpf.Value;
    //Tomador
    if EdCliente.ValueVariant <> 0 then
    begin
      NireTom := QrClientesNIRE.Value;
      DocTom  := QrClientesCNPJ.Value;
      if DocTom = '' then
        DocTom := QrClientesCPF.Value;
    end else
    begin
      NireTom := '';
      DocTom  := '';
    end;
    //Intermedi�rio
    if EdIntermediario.ValueVariant <> 0 then
    begin
      NireInt := QrIntermediarioNIRE.Value;
      DocInt  := QrIntermediarioCNPJ.Value;
      if DocInt = '' then
        DocInt := QrIntermediarioCPF.Value;
    end else
    begin
      NireInt := '';
      DocInt  := '';
    end;
    //
    DmNFSe_0000.ACBrNFSe1.NotasFiscais.Add;
    if DmNFSe_0000.ACBrNFSeX1.ConsutarNFSePresta(DocPres, QrNFSeDPSCabPrestaInscricaoMunicipal.Value,
      0, dtpsqNenhuma, TPDataI.Date, TPDataF.Date, DocTom, NireTom, DocInt, NireInt, 1) then
    begin
      {
      for I := 0 to DmNFSe_0000.ACBrNFSe1.WebServices.ConsNfsePresta.NFSeRetorno.ListaNfse.CompNfse.Count - 1 do
      begin
        Nota := DmNFSe_0000.ACBrNFSe1.WebServices.ConsNfsePresta.NFSeRetorno.ListaNfse.CompNfse.Items[I].Nfse;
        ShowMessage(Nota.Numero);
      end;
      }
      Retorno := DmNFSe_0000.ACBrNFSe1.WebServices.ConsNfsePresta.ArquivoRetorno;
      Geral.MB_Info(Retorno);
      //ShowMessage('OK');
    end;
  finally
    Screen.Cursor := crDefault;
  end;
*)
  // fim 2023-12-20 Parei Aqui!
end;

procedure TFmNFSe_RPSPesq.Serviostomados1Click(Sender: TObject);
(*
var
  Retorno, DocPres, DocTom, DocInt, NireTom, NireInt: String;
  I: Integer;
  // ini 2023-12-20
  //Nota: TNfse;
  Numero: Integer;
  // fim 2023-12-20
*)
begin
(*
  // ini 2023-12-20 Parei Aqui!
  Geral.MB_Aviso('Falta implementar 2023-12-20!');
  try
    Screen.Cursor := crHourGlass;
    //
    UnNFSe_PF_0201.ConfiguraComponenteACBrNFSe(DmodG.QrEmpresasCodigo.Value,
      DmNFSe_0000.ACBrNFSe1);
    DmNFSe_0000.ACBrNFSe1.NotasFiscais.Clear;
    //
    DmNFSe_0000.ACBrNFSe1.NotasFiscais.Add;
    //
    //Prestador
    DocPres := QrNFSeDPSCabPrestaCnpj.Value;
    if DocPres = '' then
      DocPres := QrNFSeDPSCabPrestaCpf.Value;
    //Tomador
    if EdCliente.ValueVariant <> 0 then
    begin
      NireTom := QrClientesNIRE.Value;
      DocTom  := QrClientesCNPJ.Value;
      if DocTom = '' then
        DocTom := QrClientesCPF.Value;
    end else
    begin
      NireTom := '';
      DocTom  := '';
    end;
    //Intermedi�rio
    if EdIntermediario.ValueVariant <> 0 then
    begin
      NireInt := QrIntermediarioNIRE.Value;
      DocInt  := QrIntermediarioCNPJ.Value;
      if DocInt = '' then
        DocInt := QrIntermediarioCPF.Value;
    end else
    begin
      NireInt := '';
      DocInt  := '';
    end;
    //
    if DmNFSe_0000.ACBrNFSe1.ConsutarNFSeToma(DocPres, QrNFSeDPSCabPrestaInscricaoMunicipal.Value,
      0, dtpsqNenhuma, TPDataI.Date, TPDataF.Date, DocPres, QrNFSeDPSCabPrestaInscricaoMunicipal.Value,
      DocTom, NireTom, DocInt, NireInt, 1) then
    begin
      for I := 0 to DmNFSe_0000.ACBrNFSe1.WebServices.ConsNfseToma.NFSeRetorno.ListaNfse.CompNfse.Count - 1 do
      begin
  // ini 2023-12-20 Parei Aqui!
  Geral.MB_Aviso('Falta implementar 2023-12-20!');
        //Nota := DmNFSe_0000.ACBrNFSe1.WebServices.ConsNfseToma.NFSeRetorno.ListaNfse.CompNfse.Items[I].Nfse;
        //ShowMessage(Nota.Numero);
        Numero := DmNFSe_0000.ACBrNFSe1.WebServices.ConsNfseToma.NFSeRetorno.ListaNfse.CompNfse.Items[I].Nfse.Numero;
        ShowMessage(Numero);
  // fim 2023-12-20 Parei Aqui!
      end;
      Retorno := DmNFSe_0000.ACBrNFSe1.WebServices.ConsNfseToma.ArquivoRetorno;
      Geral.MB_Info(Retorno);
      ShowMessage('OK');
    end;
  finally
    Screen.Cursor := crDefault;
  end;
*)
  // fim 2023-12-20 Parei Aqui!
end;

procedure TFmNFSe_RPSPesq.TPDataIChange(Sender: TObject);
begin
  FechaNFSeDPSCab;
end;

procedure TFmNFSe_RPSPesq.TPDataIClick(Sender: TObject);
begin
  FechaNFSeDPSCab;
end;

{
procedure TFmNFSe_RPSPesq.InsUpdNFSe(Nota: TNfse);
var
  // Nfse
  Versao,
  Numero,
  CodigoVerificacao,
  DataEmissao,
  NfseSubstituida,
  OutrasInformacoes: String;
  BaseCalculo,
  Aliquota,
  ValorIss,
  ValorLiquidoNfse,
  ValorCredito: Currency;
  PrestaCnpj,
  PrestaCpf,
  PrestaInscricaoMunicipal,
  PrestaRazaoSocial,
  PrestaNomeFantasia,
  PrestaEndereco,
  PrestaNumero,
  PrestaComplemento,
  PrestaBairro,
  PrestaCodigoMunicipio,
  PrestaUf: String;
  PrestaCodigoPais: Integer;
  PrestaCep,
  PrestaContatoTelefone,
  PrestaContatoEmail,
  OrgaoGeradorCodigoMunicipio,
  OrgaoGeradorUf,
  //
  //RPS
  RpsIDNumero,
  RpsIDSerie,
  RpsIDTipo,
  RpsDataEmissao,
  RpsStatus,
  RpsSubstNumero,
  RpsSubstSerie,
  RpsSubstTipo,
  Competencia: String;
  ValorServicos,
  ValorDeducoes,
  ValorPis,
  ValorCofins,
  ValorInss,
  ValorIr,
  ValorCsll,
  OutrasRetencoes,
  DescontoIncondicionado,
  DescontoCondicionado: Double;
  IssRetido,
  ResponsavelRetencao,
  ItemListaServico,
  CodigoCnae,
  CodigoTributacaoMunicipio,
  Discriminacao,
  CodigoMunicipio,
  CodigoPais,
  ExigibilidadeISS,
  NaturezaOperacao,  // Compatibilidade com  ACBr
  MunicipioIncidencia,
  NumeroProcesso,
(*Servico tcDadosServico
  Prestador tcIdentificacaoPrestador*)
  TomaCnpj,
  TomaCpf,
  TomaInscricaoMunicipal,
  TomaRazaoSocial,
  TomaNomeFantasia,
  TomaEndereco,
  TomaNumero,
  TomaComplemento,
  TomaBairro,
  TomaCodigoMunicipio,
  TomaUf,
  TomaCodigoPais,
  TomaCep,
  TomaContatoTelefone,
  TomaContatoEmail,
  IntermeCnpj,
  IntermeCpf,
  IntermeInscricaoMunicipal,
  IntermeRazaoSocial,
  ConstrucaoCivilCodigoObra,
  ConstrucaoCivilArt,
  RegimeEspecialTributacao,
  OptanteSimplesNacional,
  IncentivoFiscal,
  InfID_ID: String;
  //Cancelamento
  CodigoCancelamento,
  Canc_DataHora,
  Canc_ID,
  Canc_Versao,
  NfseSubstituidora,
  Subst_ID,
  Subst_Versao: String;
  //
  Status: Integer;
  Empresa, Ambiente: Integer;
  SQLType: TSQLType;
begin
//tcCompNfse
//Representa a estrutura de compartilhamento de dados de uma NFS-e.
//Nome Tipo Ocorr�ncia Descri��o
//Nfse tcNfse 1-1
//NfseCancelamento tcCancelamentoNfse 0-1
//NfseSubstituicao tcSubstituicaoNfse 0-1
  //
  (********
  tcNfse
  ********)
  //Nome Tipo Ocorr�ncia Descri��o
  //InfNfse
    (********
    tcInfNfse
    ********)
    Numero            := Nota.Numero;
    CodigoVerificacao := Nota.CodigoVerificacao;
    DataEmissao       := Geral.FDT(Nota.DataEmissao, 109);
    NfseSubstituida   := Nota.NfseSubstituida;
    OutrasInformacoes := Nota.OutrasInformacoes;
    // ValoresNfse
      (********
      tcInfNfse
      ********)
      BaseCalculo       := Nota.Servico.Valores.BaseCalculo;
      Aliquota          := Nota.Servico.Valores.Aliquota;
      ValorIss          := Nota.Servico.Valores.ValorIss;
      ValorLiquidoNfse  := Nota.Servico.Valores.ValorLiquidoNfse;
    // Fim ValoresNfse
    ValorCredito := Nota.ValorCredito;
    //PrestadorServico
      (********
      tcDadosPrestador
      ********)
      //IdentificacaoPrestador
        (********
        tcIdentificacaoPrestador
        ********)
        //CpfCnpj
          (********
          tcCpfCnpj 1-1
        ********)
        PrestaCnpj := Nota.Prestador.Cnpj;
        PrestaCpf  := Nota.Prestador.Cpf;
        PrestaInscricaoMunicipal := Nota.Prestador.InscricaoMunicipal;
      //RazaoSocial
        (********
        tsRazaoSocial
        ********)
        PrestaRazaoSocial := Nota.PrestadorServico.RazaoSocial;
      //NomeFantasia
        (********
        tsNomeFantasia
        ********)
        PrestaNomeFantasia := Nota.PrestadorServico.NomeFantasia;
      //Endereco
        (********
        tcEndereco
        ********)
        PrestaEndereco        := Nota.PrestadorServico.Endereco.Endereco;
        PrestaNumero          := Nota.PrestadorServico.Endereco.Numero;
        PrestaComplemento     := Nota.PrestadorServico.Endereco.Complemento;
        PrestaBairro          := Nota.PrestadorServico.Endereco.Bairro;
        PrestaCodigoMunicipio := Nota.PrestadorServico.Endereco.CodigoMunicipio;
        PrestaUf              := Nota.PrestadorServico.Endereco.Uf;
        PrestaCodigoPais      := Nota.PrestadorServico.Endereco.CodigoPais;
        PrestaCep             := Nota.PrestadorServico.Endereco.Cep;
      //Contato
        (********
        tcContato
        ********)
        PrestaContatoTelefone := Nota.PrestadorServico.Contato.Telefone;
        PrestaContatoEmail    := Nota.PrestadorServico.Contato.Email;
    // Fim PrestadorServico
    //OrgaoGerador
      (********
      tcIdentificacaoOrgaoGerador
      ********)
      OrgaoGeradorCodigoMunicipio := Nota.OrgaoGerador.CodigoMunicipio;
      OrgaoGeradorUf              := Nota.OrgaoGerador.Uf;
    //DeclaracaoPrestacaoServico
      (********
      tcDeclaracaoPrestacaoServico
      ********)
        //InfDeclaracaoPrestacaoServico
        (********
        tcInfDeclaracaoPrestacaoServico
        ********)
        //Rps
          (********
          tcInfRps
          ********)
          //IdentificacaoRps
            (********
            tcIdentificacaoRps
            ********)
            RpsIDNumero := Nota.IdentificacaoRps.Numero;
            RpsIDSerie  := Nota.IdentificacaoRps.Serie;
            RpsIDTipo   := TipoRpsToStr(Nota.IdentificacaoRps.Tipo);
          // FIM IdentificacaoRps
          RpsDataEmissao := Geral.FDT(Nota.DataEmissaoRps, 109);
          RpsStatus      := StatusRpsToStr(Nota.Status);
          //RpsSubstituido
            (********
            tcIdentificacaoRps
            ********)
            RpsSubstNumero := Nota.RpsSubstituido.Numero;
            RpsSubstSerie  := Nota.RpsSubstituido.Serie;
            RpsSubstTipo   := TipoRpsToStr(Nota.RpsSubstituido.Tipo);
          //Id tsIdTag
          // N�o fazer, � igual ao numero
        // FIM Rps
        Competencia := Nota.Competencia;
        //Servico
          (********
          tcDadosServico
          ********)
          //Valores
            (********
            tcValoresDeclaracaoServico
            ********)
            ValorServicos          := Nota.Servico.Valores.ValorServicos;
            ValorDeducoes          := Nota.Servico.Valores.ValorDeducoes;
            ValorPis               := Nota.Servico.Valores.ValorPis;
            ValorCofins            := Nota.Servico.Valores.ValorCofins;
            ValorInss              := Nota.Servico.Valores.ValorInss;
            ValorIr                := Nota.Servico.Valores.ValorIr;
            ValorCsll              := Nota.Servico.Valores.ValorCsll;
            OutrasRetencoes        := Nota.Servico.Valores.OutrasRetencoes;
            ValorIss               := Nota.Servico.Valores.ValorIss;
            Aliquota               := Nota.Servico.Valores.Aliquota;
            DescontoIncondicionado := Nota.Servico.Valores.DescontoIncondicionado;
            DescontoCondicionado   := Nota.Servico.Valores.DescontoCondicionado;
          //FIM Valores
          IssRetido                 := SituacaoTributariaToStr(Nota.Servico.Valores.IssRetido);
          ResponsavelRetencao       := ResponsavelRetencaoToStr(Nota.Servico.ResponsavelRetencao);
          ItemListaServico          := Nota.Servico.ItemListaServico;
          CodigoCnae                := Nota.Servico.CodigoCnae;
          CodigoTributacaoMunicipio := Nota.Servico.CodigoTributacaoMunicipio;
          Discriminacao             := Nota.Servico.Discriminacao;
          CodigoMunicipio           := Nota.Servico.CodigoMunicipio;
          CodigoPais                := Geral.FF0(Nota.Servico.CodigoPais);
          ExigibilidadeISS          := ExigibilidadeISSToStr(Nota.Servico.ExigibilidadeISS);
          MunicipioIncidencia       := Geral.FF0(Nota.Servico.MunicipioIncidencia);
          NumeroProcesso            := Nota.Servico.NumeroProcesso;
        //FIM Servico
        //Prestador
          (********
          tcIdentificacaoPrestador
          ********)
          // J� feito na NFS acima!!!
        // FIM Prestador
        //TomadorServico
          (********
          tcDadosTomador
          ********)
          //IdentificacaoTomador
            (********
            tcIdentificacaoTomador 0-1
            ********)
            if Length(Nota.Tomador.IdentificacaoTomador.CpfCnpj) > 11 then
            begin
              TomaCnpj := Nota.Tomador.IdentificacaoTomador.CpfCnpj;
              TomaCpf  := '';
            end else
            begin
              TomaCnpj := '';
              TomaCpf  := Nota.Tomador.IdentificacaoTomador.CpfCnpj;
            end;
            TomaInscricaoMunicipal := Nota.Tomador.IdentificacaoTomador.InscricaoMunicipal;
          // FIM IdentificacaoTomador
          TomaRazaoSocial := Nota.Tomador.RazaoSocial;
          //Endereco
            (********
            tcEndereco
            ********)
            TomaEndereco        := Nota.Tomador.Endereco.Endereco;
            TomaNumero          := Nota.Tomador.Endereco.Numero;
            TomaComplemento     := Nota.Tomador.Endereco.Complemento;
            TomaBairro          := Nota.Tomador.Endereco.Bairro;
            TomaCodigoMunicipio := Nota.Tomador.Endereco.CodigoMunicipio;
            TomaUf              := Nota.Tomador.Endereco.UF;
            TomaCodigoPais      := Geral.FF0(Nota.Tomador.Endereco.CodigoPais);
            TomaCep             := Nota.Tomador.Endereco.CEP;
          // FIM Endereco
          //Contato
            (********
            tcContato
            ********)
            TomaContatoTelefone := Nota.Tomador.Contato.Telefone;
            TomaContatoEmail    := Nota.Tomador.Contato.Email;
          // FIM Contato
        // Fim Tomador
        //Intermediario
          (********
          //DadosIntermediario
            tcDadosIntermediario
            ********)
            //IdentificacaoIntermediario
              (********
              tcIdentificacaoIntermediario
              ********)
              //CpfCnpj
                (********
                tcCpfCnpj
                ********)
                if Length(Nota.IntermediarioServico.CpfCnpj) > 11 then
                begin
                  IntermeCnpj := Nota.IntermediarioServico.CpfCnpj;
                  IntermeCpf  := '';
                end else
                begin
                  IntermeCnpj := '';
                  IntermeCpf  := Nota.IntermediarioServico.CpfCnpj;
                end;
              // FIM CpfCnpj
              IntermeInscricaoMunicipal := Nota.IntermediarioServico.InscricaoMunicipal;
            // FIM IdentificacaoIntermediario
          IntermeRazaoSocial := Nota.IntermediarioServico.RazaoSocial;
          // FIM DadosIntermediario
        // FIM Intermediario
        //ConstrucaoCivil
          (********
          tcDadosConstrucaoCivil
          ********)
          ConstrucaoCivilCodigoObra := Nota.ConstrucaoCivil.CodigoObra;
          ConstrucaoCivilArt := Nota.ConstrucaoCivil.Art;
        // FIM ConstrucaoCivil
        RegimeEspecialTributacao := RegimeEspecialTributacaoToStr(Nota.RegimeEspecialTributacao);
        OptanteSimplesNacional := SimNaoToStr(Nota.OptanteSimplesNacional);
        IncentivoFiscal := SimNaoToStr(Nota.IncentivadorCultural);
        //Id tsIdTag Identificador da TAG a ser assinada
        // N�o Fazer?
        InfID_ID := Nota.InfID.ID;



        //FIM InfDeclaracaoPrestacaoServico
        //Signature dsig:Signature
        // Assinatura n�o fazer!
      //FIM DeclaracaoPrestacaoServico
  // FIM InfNfse
  //Signature Dsig:Signature
    // Assinatura n�o fazer
  //versao tsVersao 1-1
  //versao := 2.01;  // Ver!!! Parei Aqui!!  DmNFSe_0000.ACBrNFSe1.NotasFiscais.Items[0].
  //versao := Nota.Versao;
  //NfseCancelamento
    (********
    tcCancelamentoNfse
    ********)
    //Confirmacao
      (********
      tcConfirmacaoCancelamento
      ********)
      //Pedido
        (********
        tcPedidoCancelamento
        ********)
        //InfPedidoCancelamento
          (********
          tcInfPedidoCancelamento
          ********)
            //IdentificacaoNfse tcIdentificacaoNfse 1-1
            // N�o fazer. J� tem dados acima!
            CodigoCancelamento := Nota.NfseCancelamento.Pedido.CodigoCancelamento;
        //Signature Dsig:Signature  > n�o fazer
      // FIM Pedido
      Canc_DataHora := Geral.FDT(Nota.NfseCancelamento.DataHora, 109);
      Canc_Id := Nota.NfseCancelamento.InfID.ID;
    // FIM Confirmacao
    //Signature Dsig:Signature 0-1
    // N�o fazer assinatura
    Canc_versao := '2.01?'; //Nota.NfseCancelamento.?
  // FIM NfseCancelamento
  //NfseSubstituicao
    (********
    tcSubstituicaoNfse
    ********)
    //SubstituicaoNfse
      (********
      tcInfSubstituicaoNfse
      ********)
      NfseSubstituidora := Nota.NfseSubstituidora;
      Subst_ID          := '';
    // FIM SubstituicaoNfse
    //Signature dsig:Signature 0-2 Signature > N�o Fazer
    Subst_versao := '2.01?'; //tsVersao

//FIM CompNfse

  if DModG.ObtemEntidadeDeCNPJCFP(PrestaCnpj + PrestaCpf, Empresa) then
  begin
    Ambiente := DmNFSe_0000.QrFilialNFSeAmbiente.Value;
    UnDmkDAC_PF.AbreMySQLQuery0(QrNFSe, Dmod.MyDB, [
    'SELECT * ',
    'FROM nfsenfscab ',
    'WHERE Empresa=' + Geral.FF0(Empresa),
    'AND Numero="' + Numero + '" ',
    'AND Ambiente=' + Geral.FF0(Ambiente),
    '']);
    //
    if QrNFSe.RecordCount > 0 then
      SQLType := stUpd
    else
      SQLType := stIns;
    //
    //if
    UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'nfsenfscab', False, [
    'RpsIDSerie', 'RpsIDTipo', 'RpsIDNumero',
    'Versao', 'CodigoVerificacao', 'DataEmissao',
    'NfseSubstituida', 'OutrasInformacoes', 'BaseCalculo',
    'Aliquota', 'ValorIss', 'ValorLiquidoNfse',
    'ValorCredito', 'PrestaCnpj', 'PrestaCpf',
    'PrestaInscricaoMunicipal', 'PrestaRazaoSocial', 'PrestaNomeFantasia',
    'PrestaEndereco', 'PrestaNumero', 'PrestaComplemento',
    'PrestaBairro', 'PrestaCodigoMunicipio', 'PrestaUf',
    'PrestaCodigoPais', 'PrestaCep', 'PrestaContatoTelefone',
    'PrestaContatoEmail', 'OrgaoGeradorCodigoMunicipio', 'OrgaoGeradorUf',
    (*'Status',*) 'RpsDataEmissao', 'RpsStatus',
    'RpsSubstNumero', 'RpsSubstSerie', 'RpsSubstTipo',
    'Competencia', 'ValorServicos', 'ValorDeducoes',
    'ValorPis', 'ValorCofins', 'ValorInss',
    'ValorIr', 'ValorCsll', 'OutrasRetencoes',
    'DescontoIncondicionado', 'DescontoCondicionado', 'IssRetido',
    'ResponsavelRetencao', 'ItemListaServico', 'CodigoCnae',
    'CodigoTributacaoMunicipio', 'Discriminacao', 'CodigoMunicipio',
    'CodigoPais', 'ExigibilidadeIss', 'NaturezaOperacao',
    (*'QuemPagaISS',*) 'MunicipioIncidencia', 'NumeroProcesso',
    'TomaCpf', 'TomaCnpj', 'TomaInscricaoMunicipal',
    'TomaRazaoSocial', 'TomaNomeFantasia', 'TomaEndereco',
    'TomaNumero', 'TomaComplemento', 'TomaBairro',
    'TomaCodigoMunicipio', 'TomaUf', 'TomaCodigoPais',
    'TomaCep', 'TomaContatoTelefone', 'TomaContatoEmail',
    'IntermeCpf', 'IntermeCnpj', 'IntermeInscricaoMunicipal',
    'IntermeRazaoSocial', 'ConstrucaoCivilCodigoObra', 'ConstrucaoCivilArt',
    'RegimeEspecialTributacao', 'OptanteSimplesNacional', 'IncentivoFiscal',
    'InfID_ID', 'CodigoCancelamento', 'Canc_DataHora',
    'Canc_ID', 'Canc_Versao', 'NfseSubstituidora',
    'Subst_ID', 'Subst_Versao'], [
    'Empresa', 'Numero', 'Ambiente'], [
    RpsIDSerie, RpsIDTipo, RpsIDNumero,
    Versao, CodigoVerificacao, DataEmissao,
    NfseSubstituida, OutrasInformacoes, BaseCalculo,
    Aliquota, ValorIss, ValorLiquidoNfse,
    ValorCredito, PrestaCnpj, PrestaCpf,
    PrestaInscricaoMunicipal, PrestaRazaoSocial, PrestaNomeFantasia,
    PrestaEndereco, PrestaNumero, PrestaComplemento,
    PrestaBairro, PrestaCodigoMunicipio, PrestaUf,
    PrestaCodigoPais, PrestaCep, PrestaContatoTelefone,
    PrestaContatoEmail, OrgaoGeradorCodigoMunicipio, OrgaoGeradorUf,
    (*Status,*) RpsDataEmissao, RpsStatus,
    RpsSubstNumero, RpsSubstSerie, RpsSubstTipo,
    Competencia, ValorServicos, ValorDeducoes,
    ValorPis, ValorCofins, ValorInss,
    ValorIr, ValorCsll, OutrasRetencoes,
    DescontoIncondicionado, DescontoCondicionado, IssRetido,
    ResponsavelRetencao, ItemListaServico, CodigoCnae,
    CodigoTributacaoMunicipio, Discriminacao, CodigoMunicipio,
    CodigoPais, ExigibilidadeIss, NaturezaOperacao,
    (*QuemPagaISS,*) MunicipioIncidencia, NumeroProcesso,
    TomaCpf, TomaCnpj, TomaInscricaoMunicipal,
    TomaRazaoSocial, TomaNomeFantasia, TomaEndereco,
    TomaNumero, TomaComplemento, TomaBairro,
    TomaCodigoMunicipio, TomaUf, TomaCodigoPais,
    TomaCep, TomaContatoTelefone, TomaContatoEmail,
    IntermeCpf, IntermeCnpj, IntermeInscricaoMunicipal,
    IntermeRazaoSocial, ConstrucaoCivilCodigoObra, ConstrucaoCivilArt,
    RegimeEspecialTributacao, OptanteSimplesNacional, IncentivoFiscal,
    InfID_ID, CodigoCancelamento, Canc_DataHora,
    Canc_ID, Canc_Versao, NfseSubstituidora,
    Subst_ID, Subst_Versao], [
    Empresa, Numero, Ambiente], True);
  end else Geral.MB_Erro('Empresa n�o definida em atualiza��o de NFS-e!');
end;
}

end.
