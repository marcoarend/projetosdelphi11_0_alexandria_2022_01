object FmNFSe_RPSPesq: TFmNFSe_RPSPesq
  Left = 339
  Top = 185
  Caption = 'NFS-_RPS_-001 :: Gerenciamento de RPS'
  ClientHeight = 580
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object PnGeral: TPanel
    Left = 0
    Top = 96
    Width = 1008
    Height = 414
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object Panel5: TPanel
      Left = 0
      Top = 0
      Width = 1008
      Height = 165
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object Panel6: TPanel
        Left = 0
        Top = 0
        Width = 1008
        Height = 124
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 0
        object Panel7: TPanel
          Left = 0
          Top = 0
          Width = 468
          Height = 124
          Align = alLeft
          BevelOuter = bvNone
          TabOrder = 0
          object dmkLabel1: TdmkLabel
            Left = 8
            Top = 0
            Width = 102
            Height = 13
            Caption = 'Empresa (obrigat'#243'rio):'
            UpdType = utYes
            SQLType = stNil
          end
          object Label14: TLabel
            Left = 8
            Top = 40
            Width = 45
            Height = 13
            Caption = 'Tomador:'
          end
          object Label3: TLabel
            Left = 8
            Top = 80
            Width = 63
            Height = 13
            Caption = 'Intermedi'#225'rio:'
          end
          object EdFilial: TdmkEditCB
            Left = 8
            Top = 16
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            OnChange = EdFilialChange
            OnExit = EdFilialExit
            DBLookupComboBox = CBFilial
            IgnoraDBLookupComboBox = False
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
          object CBFilial: TdmkDBLookupComboBox
            Left = 64
            Top = 16
            Width = 400
            Height = 21
            KeyField = 'Filial'
            ListField = 'NOMEFILIAL'
            ListSource = DModG.DsEmpresas
            TabOrder = 1
            dmkEditCB = EdFilial
            UpdType = utYes
            LocF7SQLMasc = '$#'
            LocF7PreDefProc = f7pNone
          end
          object EdCliente: TdmkEditCB
            Left = 8
            Top = 56
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 2
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'Cliente'
            UpdCampo = 'Cliente'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            OnChange = EdClienteChange
            OnExit = EdClienteExit
            DBLookupComboBox = CBCliente
            IgnoraDBLookupComboBox = False
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
          object CBCliente: TdmkDBLookupComboBox
            Left = 64
            Top = 56
            Width = 400
            Height = 21
            KeyField = 'Codigo'
            ListField = 'NO_Enti'
            ListSource = DsClientes
            TabOrder = 3
            dmkEditCB = EdCliente
            QryCampo = 'Cliente'
            UpdType = utNil
            LocF7SQLMasc = '$#'
            LocF7PreDefProc = f7pNone
          end
          object EdIntermediario: TdmkEditCB
            Left = 8
            Top = 96
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 4
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'Cliente'
            UpdCampo = 'Cliente'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            OnChange = EdClienteChange
            OnExit = EdClienteExit
            DBLookupComboBox = CBIntermediario
            IgnoraDBLookupComboBox = False
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
          object CBIntermediario: TdmkDBLookupComboBox
            Left = 64
            Top = 96
            Width = 400
            Height = 21
            KeyField = 'Codigo'
            ListField = 'NO_Enti'
            ListSource = DsIntermediario
            TabOrder = 5
            dmkEditCB = EdIntermediario
            QryCampo = 'Cliente'
            UpdType = utNil
            LocF7SQLMasc = '$#'
            LocF7PreDefProc = f7pNone
          end
        end
        object GroupBox1: TGroupBox
          Left = 468
          Top = 0
          Width = 132
          Height = 124
          Align = alLeft
          Caption = ' Emiss'#227'o: '
          TabOrder = 1
          object Label2: TLabel
            Left = 8
            Top = 60
            Width = 48
            Height = 13
            Caption = 'Data final:'
          end
          object Label1: TLabel
            Left = 8
            Top = 20
            Width = 55
            Height = 13
            Caption = 'Data inicial:'
          end
          object TPDataF: TdmkEditDateTimePicker
            Left = 8
            Top = 76
            Width = 115
            Height = 21
            Date = 40017.000000000000000000
            Time = 0.908141423620691100
            TabOrder = 1
            ReadOnly = False
            DefaultEditMask = '!99/99/99;1;_'
            AutoApplyEditMask = True
            UpdType = utYes
            DatePurpose = dmkdpNone
          end
          object TPDataI: TdmkEditDateTimePicker
            Left = 8
            Top = 36
            Width = 115
            Height = 21
            Date = 40017.000000000000000000
            Time = 0.908086238428950300
            TabOrder = 0
            OnClick = TPDataIClick
            OnChange = TPDataIChange
            ReadOnly = False
            DefaultEditMask = '!99/99/99;1;_'
            AutoApplyEditMask = True
            UpdType = utYes
            DatePurpose = dmkdpNone
          end
        end
        object GroupBox2: TGroupBox
          Left = 600
          Top = 0
          Width = 132
          Height = 124
          Align = alLeft
          Caption = ' Compet'#234'ncia: '
          TabOrder = 2
          object Label4: TLabel
            Left = 8
            Top = 60
            Width = 48
            Height = 13
            Caption = 'Data final:'
          end
          object Label5: TLabel
            Left = 8
            Top = 20
            Width = 55
            Height = 13
            Caption = 'Data inicial:'
          end
          object dmkEditDateTimePicker1: TdmkEditDateTimePicker
            Left = 8
            Top = 76
            Width = 115
            Height = 21
            Date = 40017.000000000000000000
            Time = 0.908141423620691100
            TabOrder = 1
            ReadOnly = False
            DefaultEditMask = '!99/99/99;1;_'
            AutoApplyEditMask = True
            UpdType = utYes
            DatePurpose = dmkdpNone
          end
          object dmkEditDateTimePicker2: TdmkEditDateTimePicker
            Left = 8
            Top = 36
            Width = 115
            Height = 21
            Date = 40017.000000000000000000
            Time = 0.908086238428950300
            TabOrder = 0
            OnClick = TPDataIClick
            OnChange = TPDataIChange
            ReadOnly = False
            DefaultEditMask = '!99/99/99;1;_'
            AutoApplyEditMask = True
            UpdType = utYes
            DatePurpose = dmkdpNone
          end
        end
        object Panel8: TPanel
          Left = 732
          Top = 0
          Width = 276
          Height = 124
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 3
          object BtReabre: TBitBtn
            Tag = 18
            Left = 104
            Top = 8
            Width = 90
            Height = 40
            Caption = '&Reabre'
            NumGlyphs = 2
            TabOrder = 0
            OnClick = BtReabreClick
          end
          object Ck100e101: TCheckBox
            Left = 4
            Top = 50
            Width = 189
            Height = 17
            Caption = 'Somente autorizadas e canceladas.'
            TabOrder = 1
            Visible = False
            OnClick = CkEmitClick
          end
          object RGOrdem2: TRadioGroup
            Left = 4
            Top = 6
            Width = 93
            Height = 39
            Caption = ' Sentido: '
            Columns = 2
            ItemIndex = 1
            Items.Strings = (
              'ASC'
              'DES')
            TabOrder = 2
            OnClick = RGOrdem2Click
          end
        end
      end
      object RGAmbiente: TRadioGroup
        Left = 0
        Top = 124
        Width = 189
        Height = 41
        Align = alLeft
        Caption = ' Ambiente: '
        Columns = 3
        ItemIndex = 1
        Items.Strings = (
          'Ambos'
          'Produ.'
          'Homol.')
        TabOrder = 1
        OnClick = RGAmbienteClick
      end
      object RGQuemEmit: TRadioGroup
        Left = 189
        Top = 124
        Width = 271
        Height = 41
        Align = alLeft
        Caption = ' Servi'#231'os:  '
        Columns = 3
        ItemIndex = 0
        Items.Strings = (
          'Prestados'
          'Tomados '
          'Ambos')
        TabOrder = 2
        OnClick = RGAmbienteClick
      end
      object RGOrdem1: TRadioGroup
        Left = 460
        Top = 124
        Width = 548
        Height = 41
        Align = alClient
        Caption = ' Ordem: '
        Columns = 4
        ItemIndex = 0
        Items.Strings = (
          'Data, RPS'
          'RPS, Data'
          'Status, RPS'
          'Status, Data')
        TabOrder = 3
        OnClick = RGOrdem1Click
      end
    end
    object PnDados: TPanel
      Left = 0
      Top = 165
      Width = 1008
      Height = 249
      Align = alClient
      Caption = 'PnDados'
      TabOrder = 1
      Visible = False
      object Splitter1: TSplitter
        Left = 1
        Top = 123
        Width = 1006
        Height = 5
        Cursor = crVSplit
        Align = alBottom
        ExplicitTop = 62
      end
      object dmkDBGrid1: TdmkDBGrid
        Left = 1
        Top = 1
        Width = 1006
        Height = 122
        Align = alClient
        Columns = <
          item
            Expanded = False
            FieldName = 'Codigo'
            Title.Caption = 'Meu ID'
            Width = 56
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Ambiente'
            Title.Caption = 'Amb'
            Width = 28
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'StatusDPS'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            Width = 36
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'LastLRpsC'
            Title.Caption = 'Lote'
            Width = 56
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Cliente'
            Title.Caption = 'Tomador'
            Width = 65
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'TomaCnpjCpf_TXT'
            Title.Caption = 'Tomador CNPJ / CPF'
            Width = 130
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NO_CLI'
            Title.Caption = 'Tomador do Servi'#231'o'
            Width = 200
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Intermediario'
            Title.Caption = 'Intermed.'
            Width = 56
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NO_INT'
            Title.Caption = 'Nome intermedi'#225'rio'
            Width = 140
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'RpsIDTipo'
            Title.Caption = 'Tipo'
            Width = 28
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'RpsIDSerie'
            Title.Caption = 'S'#233'rie'
            Width = 32
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'RpsIDNumero'
            Title.Caption = 'N'#186' RPS'
            Width = 56
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NfsNumero'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            Title.Caption = 'N'#186' NFS-e'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'RpsDataEmissao'
            Title.Caption = 'Data emis.'
            Width = 56
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'RpsHoraEmissao'
            Title.Caption = 'Hora emis.'
            Width = 56
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'RpsStatus'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'ValorServicos'
            Title.Caption = '$ Servi'#231'os'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'ValorIss'
            Title.Caption = '$ ISS'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Aliquota'
            Title.Caption = 'Al'#237'quota'
            Width = 48
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'IssRetido'
            Width = 50
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'ResponsavelRetencao'
            Title.Caption = 'Resp.ret.'
            Width = 48
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'ItemListaServico'
            Title.Caption = 'LC 116/03'
            Width = 56
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Discriminacao'
            Width = 600
            Visible = True
          end>
        Color = clWindow
        DataSource = DsNFSeDPSCab
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        OnDrawColumnCell = dmkDBGrid1DrawColumnCell
        FieldsCalcToOrder.Strings = (
          'TomaCnpjCpf_TXT=TomaCnpjCpf')
        Columns = <
          item
            Expanded = False
            FieldName = 'Codigo'
            Title.Caption = 'Meu ID'
            Width = 56
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Ambiente'
            Title.Caption = 'Amb'
            Width = 28
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'StatusDPS'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            Width = 36
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'LastLRpsC'
            Title.Caption = 'Lote'
            Width = 56
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Cliente'
            Title.Caption = 'Tomador'
            Width = 65
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'TomaCnpjCpf_TXT'
            Title.Caption = 'Tomador CNPJ / CPF'
            Width = 130
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NO_CLI'
            Title.Caption = 'Tomador do Servi'#231'o'
            Width = 200
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Intermediario'
            Title.Caption = 'Intermed.'
            Width = 56
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NO_INT'
            Title.Caption = 'Nome intermedi'#225'rio'
            Width = 140
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'RpsIDTipo'
            Title.Caption = 'Tipo'
            Width = 28
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'RpsIDSerie'
            Title.Caption = 'S'#233'rie'
            Width = 32
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'RpsIDNumero'
            Title.Caption = 'N'#186' RPS'
            Width = 56
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NfsNumero'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            Title.Caption = 'N'#186' NFS-e'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'RpsDataEmissao'
            Title.Caption = 'Data emis.'
            Width = 56
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'RpsHoraEmissao'
            Title.Caption = 'Hora emis.'
            Width = 56
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'RpsStatus'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'ValorServicos'
            Title.Caption = '$ Servi'#231'os'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'ValorIss'
            Title.Caption = '$ ISS'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Aliquota'
            Title.Caption = 'Al'#237'quota'
            Width = 48
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'IssRetido'
            Width = 50
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'ResponsavelRetencao'
            Title.Caption = 'Resp.ret.'
            Width = 48
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'ItemListaServico'
            Title.Caption = 'LC 116/03'
            Width = 56
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Discriminacao'
            Width = 600
            Visible = True
          end>
      end
      object DBGrid1: TDBGrid
        Left = 1
        Top = 128
        Width = 1006
        Height = 120
        Align = alBottom
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 217
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
        OnClick = SbImprimeClick
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 8
        Width = 40
        Height = 40
        Enabled = False
        NumGlyphs = 2
        TabOrder = 1
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 8
        Width = 40
        Height = 40
        Enabled = False
        NumGlyphs = 2
        TabOrder = 2
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 8
        Width = 40
        Height = 40
        Enabled = False
        NumGlyphs = 2
        TabOrder = 3
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 8
        Width = 40
        Height = 40
        Enabled = False
        NumGlyphs = 2
        TabOrder = 4
      end
    end
    object GB_M: TGroupBox
      Left = 217
      Top = 0
      Width = 743
      Height = 52
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 286
        Height = 32
        Caption = 'Gerenciamento de RPS'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 286
        Height = 32
        Caption = 'Gerenciamento de RPS'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 286
        Height = 32
        Caption = 'Gerenciamento de RPS'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 52
    Width = 1008
    Height = 44
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 510
    Width = 1008
    Height = 70
    Align = alBottom
    TabOrder = 3
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 4
        Width = 90
        Height = 40
        Caption = '&Imprime'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 0
      end
      object BtEnvia: TBitBtn
        Tag = 244
        Left = 94
        Top = 4
        Width = 90
        Height = 40
        Caption = '&Envia'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtEnviaClick
      end
      object BtEvento: TBitBtn
        Tag = 526
        Left = 184
        Top = 4
        Width = 90
        Height = 40
        Caption = 'E&vento'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 2
      end
      object BtCancela: TBitBtn
        Tag = 455
        Left = 274
        Top = 4
        Width = 90
        Height = 40
        Caption = 'Ca&ncela'
        NumGlyphs = 2
        TabOrder = 3
        OnClick = BtCancelaClick
      end
      object BtSubstitui: TBitBtn
        Tag = 541
        Left = 364
        Top = 4
        Width = 90
        Height = 40
        Caption = 'S&ubstitui'
        NumGlyphs = 2
        TabOrder = 4
        OnClick = BtSubstituiClick
      end
      object BtLeArq: TBitBtn
        Tag = 40
        Left = 454
        Top = 4
        Width = 90
        Height = 40
        Caption = '&L'#234' arquivo'
        NumGlyphs = 2
        TabOrder = 5
        OnClick = BtLeArqClick
      end
      object BtArq: TBitBtn
        Tag = 512
        Left = 544
        Top = 4
        Width = 90
        Height = 40
        Caption = 'L'#234' &XML/BD'
        NumGlyphs = 2
        TabOrder = 6
        OnClick = BtArqClick
      end
      object BtConsulta: TBitBtn
        Tag = 528
        Left = 634
        Top = 4
        Width = 90
        Height = 40
        Caption = '&Consulta'
        NumGlyphs = 2
        TabOrder = 7
        OnClick = BtConsultaClick
      end
      object BtAltera: TBitBtn
        Tag = 11
        Left = 724
        Top = 4
        Width = 90
        Height = 40
        Caption = 'Altera'
        NumGlyphs = 2
        TabOrder = 8
        OnClick = BtAlteraClick
      end
      object BtExclui: TBitBtn
        Tag = 12
        Left = 814
        Top = 4
        Width = 90
        Height = 40
        Caption = 'Exclui'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 9
        OnClick = BtExcluiClick
      end
      object Panel2: TPanel
        Left = 909
        Top = 0
        Width = 95
        Height = 53
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 10
        object BtSaida: TBitBtn
          Tag = 13
          Left = 2
          Top = 3
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Hint = 'Sai da janela atual'
          Caption = '&Sa'#237'da'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
        end
      end
    end
  end
  object QrClientes: TMySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrClientesBeforeClose
    SQL.Strings = (
      'SELECT Codigo, Tipo, CNPJ, CPF, NIRE,'
      'IF(Tipo=0, RazaoSocial, Nome) NO_Enti'
      'FROM Entidades'
      'WHERE Cliente1="V"'
      'OR Cliente2="V"'
      'OR Cliente3="V"'
      'OR Cliente4="V"'
      'ORDER BY NO_Enti')
    Left = 620
    Top = 12
    object QrClientesCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrClientesNO_Enti: TWideStringField
      FieldName = 'NO_Enti'
      Required = True
      Size = 100
    end
    object QrClientesTipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrClientesCNPJ: TWideStringField
      FieldName = 'CNPJ'
      Size = 18
    end
    object QrClientesCPF: TWideStringField
      FieldName = 'CPF'
      Size = 18
    end
    object QrClientesNIRE: TWideStringField
      FieldName = 'NIRE'
      Size = 15
    end
  end
  object DsClientes: TDataSource
    DataSet = QrClientes
    Left = 616
    Top = 60
  end
  object PMLeArq: TPopupMenu
    Left = 420
    Top = 432
    object LdoarquivoXML1: TMenuItem
      Caption = 'L'#234' XML do arquivo XML'
      OnClick = LdoarquivoXML1Click
    end
    object LXMLdoBD1: TMenuItem
      Caption = 'L'#234' XML do Banco de dados'
      OnClick = LXMLdoBD1Click
    end
  end
  object PMImprime: TPopupMenu
    Left = 8
    Top = 8
    object CampospreenchidosnoXMLdaNFe1: TMenuItem
      Caption = '&Campos preenchidos no XML da NF-e'
      OnClick = CampospreenchidosnoXMLdaNFe1Click
    end
    object N2: TMenuItem
      Caption = '-'
    end
    object Listadasnotaspesquisadas1: TMenuItem
      Caption = '&Lista das notas pesquisadas'
      OnClick = Listadasnotaspesquisadas1Click
    end
    object PreviewdaNFe1: TMenuItem
      Caption = 'Preview da NF-e'
      OnClick = PreviewdaNFe1Click
    end
  end
  object QrNFSeDPSCab: TMySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrNFSeDPSCabAfterOpen
    BeforeClose = QrNFSeDPSCabBeforeClose
    AfterScroll = QrNFSeDPSCabAfterScroll
    OnCalcFields = QrNFSeDPSCabCalcFields
    SQL.Strings = (
      'SELECT IF (cli.Tipo=0, cli.RazaoSocial, cli.Nome) NO_CLI, '
      'IF (ine.Tipo=0, ine.RazaoSocial, ine.Nome) NO_INT, '
      'dps.Status StatusDPS, dps.* '
      'FROM nfsedpscab dps '
      'LEFT JOIN entidades cli ON cli.Codigo = dps.Cliente '
      'LEFT JOIN entidades ine ON ine.Codigo = dps.Intermediario ')
    Left = 164
    Top = 40
    object QrNFSeDPSCabCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrNFSeDPSCabAmbiente: TSmallintField
      FieldName = 'Ambiente'
    end
    object QrNFSeDPSCabEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrNFSeDPSCabCliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrNFSeDPSCabIntermediario: TIntegerField
      FieldName = 'Intermediario'
    end
    object QrNFSeDPSCabNFSeSrvCad: TIntegerField
      FieldName = 'NFSeSrvCad'
    end
    object QrNFSeDPSCabRpsID: TWideStringField
      FieldName = 'RpsID'
      Size = 255
    end
    object QrNFSeDPSCabRpsIDNumero: TIntegerField
      FieldName = 'RpsIDNumero'
    end
    object QrNFSeDPSCabRpsIDSerie: TWideStringField
      FieldName = 'RpsIDSerie'
      Size = 5
    end
    object QrNFSeDPSCabRpsIDTipo: TSmallintField
      FieldName = 'RpsIDTipo'
    end
    object QrNFSeDPSCabRpsDataEmissao: TDateField
      FieldName = 'RpsDataEmissao'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrNFSeDPSCabRpsHoraEmissao: TTimeField
      FieldName = 'RpsHoraEmissao'
    end
    object QrNFSeDPSCabRpsStatus: TIntegerField
      FieldName = 'RpsStatus'
    end
    object QrNFSeDPSCabCompetencia: TDateField
      FieldName = 'Competencia'
    end
    object QrNFSeDPSCabSubstNumero: TIntegerField
      FieldName = 'SubstNumero'
    end
    object QrNFSeDPSCabSubstSerie: TWideStringField
      FieldName = 'SubstSerie'
      Size = 5
    end
    object QrNFSeDPSCabSubstTipo: TSmallintField
      FieldName = 'SubstTipo'
    end
    object QrNFSeDPSCabValorServicos: TFloatField
      FieldName = 'ValorServicos'
      DisplayFormat = '#,###,##0.00'
    end
    object QrNFSeDPSCabValorDeducoes: TFloatField
      FieldName = 'ValorDeducoes'
    end
    object QrNFSeDPSCabValorPis: TFloatField
      FieldName = 'ValorPis'
    end
    object QrNFSeDPSCabValorCofins: TFloatField
      FieldName = 'ValorCofins'
    end
    object QrNFSeDPSCabValorInss: TFloatField
      FieldName = 'ValorInss'
    end
    object QrNFSeDPSCabValorIr: TFloatField
      FieldName = 'ValorIr'
    end
    object QrNFSeDPSCabValorCsll: TFloatField
      FieldName = 'ValorCsll'
    end
    object QrNFSeDPSCabOutrasRetencoes: TFloatField
      FieldName = 'OutrasRetencoes'
    end
    object QrNFSeDPSCabValorIss: TFloatField
      FieldName = 'ValorIss'
      DisplayFormat = '#,###,##0.00'
    end
    object QrNFSeDPSCabAliquota: TFloatField
      FieldName = 'Aliquota'
      DisplayFormat = '#,###,##0.00'
    end
    object QrNFSeDPSCabDescontoIncondicionado: TFloatField
      FieldName = 'DescontoIncondicionado'
    end
    object QrNFSeDPSCabDescontoCondicionado: TFloatField
      FieldName = 'DescontoCondicionado'
    end
    object QrNFSeDPSCabIssRetido: TSmallintField
      FieldName = 'IssRetido'
      DisplayFormat = '#,###,##0.00'
    end
    object QrNFSeDPSCabResponsavelRetencao: TSmallintField
      FieldName = 'ResponsavelRetencao'
    end
    object QrNFSeDPSCabItemListaServico: TWideStringField
      FieldName = 'ItemListaServico'
      Size = 5
    end
    object QrNFSeDPSCabCodigoCnae: TWideStringField
      FieldName = 'CodigoCnae'
    end
    object QrNFSeDPSCabCodigoTributacaoMunicipio: TWideStringField
      FieldName = 'CodigoTributacaoMunicipio'
    end
    object QrNFSeDPSCabDiscriminacao: TWideMemoField
      FieldName = 'Discriminacao'
      Required = True
      BlobType = ftWideMemo
      Size = 4
    end
    object QrNFSeDPSCabCodigoMunicipio: TIntegerField
      FieldName = 'CodigoMunicipio'
    end
    object QrNFSeDPSCabCodigoPais: TIntegerField
      FieldName = 'CodigoPais'
    end
    object QrNFSeDPSCabExigibilidadeISS: TSmallintField
      FieldName = 'ExigibilidadeISS'
    end
    object QrNFSeDPSCabMunicipioIncidencia: TIntegerField
      FieldName = 'MunicipioIncidencia'
    end
    object QrNFSeDPSCabNumeroProcesso: TWideStringField
      FieldName = 'NumeroProcesso'
      Size = 30
    end
    object QrNFSeDPSCabPrestaCpf: TWideStringField
      FieldName = 'PrestaCpf'
      Size = 18
    end
    object QrNFSeDPSCabPrestaCnpj: TWideStringField
      FieldName = 'PrestaCnpj'
      Size = 18
    end
    object QrNFSeDPSCabPrestaInscricaoMunicipal: TWideStringField
      FieldName = 'PrestaInscricaoMunicipal'
      Size = 18
    end
    object QrNFSeDPSCabTomaCpf: TWideStringField
      FieldName = 'TomaCpf'
      Size = 18
    end
    object QrNFSeDPSCabTomaCnpj: TWideStringField
      FieldName = 'TomaCnpj'
      Size = 18
    end
    object QrNFSeDPSCabTomaInscricaoMunicipal: TWideStringField
      FieldName = 'TomaInscricaoMunicipal'
      Size = 18
    end
    object QrNFSeDPSCabTomaRazaoSocial: TWideStringField
      FieldName = 'TomaRazaoSocial'
      Size = 150
    end
    object QrNFSeDPSCabTomaEndereco: TWideStringField
      FieldName = 'TomaEndereco'
      Size = 125
    end
    object QrNFSeDPSCabTomaNumero: TWideStringField
      FieldName = 'TomaNumero'
      Size = 10
    end
    object QrNFSeDPSCabTomaComplemento: TWideStringField
      FieldName = 'TomaComplemento'
      Size = 60
    end
    object QrNFSeDPSCabTomaBairro: TWideStringField
      FieldName = 'TomaBairro'
      Size = 60
    end
    object QrNFSeDPSCabTomaCodigoMunicipio: TIntegerField
      FieldName = 'TomaCodigoMunicipio'
    end
    object QrNFSeDPSCabTomaUf: TWideStringField
      FieldName = 'TomaUf'
      Size = 125
    end
    object QrNFSeDPSCabTomaCodigoPais: TIntegerField
      FieldName = 'TomaCodigoPais'
    end
    object QrNFSeDPSCabTomaCep: TIntegerField
      FieldName = 'TomaCep'
    end
    object QrNFSeDPSCabIntermeCpf: TWideStringField
      FieldName = 'IntermeCpf'
      Size = 18
    end
    object QrNFSeDPSCabIntermeCnpj: TWideStringField
      FieldName = 'IntermeCnpj'
      Size = 18
    end
    object QrNFSeDPSCabIntermeInscricaoMunicipal: TWideStringField
      FieldName = 'IntermeInscricaoMunicipal'
      Size = 18
    end
    object QrNFSeDPSCabIntermeRazaoSocial: TWideStringField
      FieldName = 'IntermeRazaoSocial'
      Size = 150
    end
    object QrNFSeDPSCabConstrucaoCivilCodigoObra: TWideStringField
      FieldName = 'ConstrucaoCivilCodigoObra'
      Size = 15
    end
    object QrNFSeDPSCabConstrucaoCivilArt: TWideStringField
      FieldName = 'ConstrucaoCivilArt'
      Size = 15
    end
    object QrNFSeDPSCabRegimeEspecialTributacao: TSmallintField
      FieldName = 'RegimeEspecialTributacao'
    end
    object QrNFSeDPSCabOptanteSimplesNacional: TSmallintField
      FieldName = 'OptanteSimplesNacional'
    end
    object QrNFSeDPSCabIncentivoFiscal: TSmallintField
      FieldName = 'IncentivoFiscal'
    end
    object QrNFSeDPSCabId: TWideStringField
      FieldName = 'Id'
      Size = 255
    end
    object QrNFSeDPSCabLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrNFSeDPSCabDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrNFSeDPSCabDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrNFSeDPSCabUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrNFSeDPSCabUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrNFSeDPSCabAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrNFSeDPSCabAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrNFSeDPSCabLastLRpsC: TIntegerField
      FieldName = 'LastLRpsC'
    end
    object QrNFSeDPSCabQuemPagaISS: TSmallintField
      FieldName = 'QuemPagaISS'
    end
    object QrNFSeDPSCabNaturezaOperacao: TIntegerField
      FieldName = 'NaturezaOperacao'
    end
    object QrNFSeDPSCabTomaContatoTelefone: TWideStringField
      FieldName = 'TomaContatoTelefone'
    end
    object QrNFSeDPSCabTomaContatoEmail: TWideStringField
      FieldName = 'TomaContatoEmail'
      Size = 80
    end
    object QrNFSeDPSCabTomaNomeFantasia: TWideStringField
      FieldName = 'TomaNomeFantasia'
      Size = 60
    end
    object QrNFSeDPSCabNFSeFatCab: TIntegerField
      FieldName = 'NFSeFatCab'
    end
    object QrNFSeDPSCabNO_CLI: TWideStringField
      FieldName = 'NO_CLI'
      Size = 100
    end
    object QrNFSeDPSCabNO_INT: TWideStringField
      FieldName = 'NO_INT'
      Size = 100
    end
    object QrNFSeDPSCabStatusDPS: TIntegerField
      FieldName = 'StatusDPS'
    end
    object QrNFSeDPSCabTomaCnpjCpf: TWideStringField
      FieldName = 'TomaCnpjCpf'
      Size = 18
    end
    object QrNFSeDPSCabTomaCnpjCpf_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TomaCnpjCpf_TXT'
      Size = 30
      Calculated = True
    end
    object QrNFSeDPSCabNfsNumero: TLargeintField
      FieldName = 'NfsNumero'
    end
  end
  object DsNFSeDPSCab: TDataSource
    DataSet = QrNFSeDPSCab
    Left = 168
    Top = 88
  end
  object QrNFeXMLI: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      
        'SELECT xml.Codigo, xml.ID, xml.Valor, lay.Pai, lay.Descricao, la' +
        'y.Campo'
      'FROM nfexmli xml'
      'LEFT JOIN nfelayi lay ON lay.ID=xml.ID AND lay.Codigo=xml.Codigo'
      'WHERE xml.FatID=:P0'
      'AND xml.FatNum=:P1'
      'AND xml.Empresa=:P2')
    Left = 284
    Top = 424
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrNFeXMLICodigo: TWideStringField
      FieldName = 'Codigo'
      Size = 5
    end
    object QrNFeXMLIID: TWideStringField
      FieldName = 'ID'
      Size = 6
    end
    object QrNFeXMLIValor: TWideStringField
      FieldName = 'Valor'
      Size = 60
    end
    object QrNFeXMLIPai: TWideStringField
      FieldName = 'Pai'
      Size = 10
    end
    object QrNFeXMLIDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 100
    end
    object QrNFeXMLICampo: TWideStringField
      FieldName = 'Campo'
      Size = 30
    end
  end
  object frxDsNFeXMLI: TfrxDBDataset
    UserName = 'frxDsNFeXMLI'
    CloseDataSource = False
    DataSet = QrNFeXMLI
    BCDToCurrency = False
    DataSetOptions = []
    Left = 312
    Top = 424
  end
  object frxCampos: TfrxReport
    Version = '2022.1'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 40025.074573773150000000
    ReportOptions.LastChange = 40025.074573773150000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    Left = 340
    Top = 424
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsNFeXMLI
        DataSetName = 'frxDsNFeXMLI'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 15.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 15.000000000000000000
      Frame.Typ = []
      MirrorMode = []
      object PageHeader9: TfrxPageHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 58.582701570000000000
        Top = 79.370130000000000000
        Width = 699.213050000000000000
        object Shape7: TfrxShapeView
          AllowVectorExport = True
          Width = 699.213050000000000000
          Height = 37.795300000000000000
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Line24: TfrxLineView
          AllowVectorExport = True
          Top = 18.897650000000000000
          Width = 699.213050000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo215: TfrxMemoView
          AllowVectorExport = True
          Left = 151.181200000000000000
          Width = 396.850650000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'Campos preenchidos no XML')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo293: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 684.094930000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            
              'NF N'#186' [frxDsNFeCabA."ide_nNF"]   -   Chave NF-e: [frxDsNFeCabA."' +
              'Id"]')
          ParentFont = False
        end
        object Memo1: TfrxMemoView
          AllowVectorExport = True
          Top = 37.795300000000000000
          Width = 18.897650000000000000
          Height = 20.787401570000000000
          DataSet = frxDsNFeXMLI
          DataSetName = 'frxDsNFeXMLI'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'C'#243'd.'
            'ID')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          AllowVectorExport = True
          Left = 18.897650000000000000
          Top = 37.795300000000000000
          Width = 52.913420000000000000
          Height = 20.787401570000000000
          DataSet = frxDsNFeXMLI
          DataSetName = 'frxDsNFeXMLI'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Pai'
            'Campo')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          AllowVectorExport = True
          Left = 71.811070000000000000
          Top = 37.795300000000000000
          Width = 627.401980000000000000
          Height = 20.787401570000000000
          DataSet = frxDsNFeXMLI
          DataSetName = 'frxDsNFeXMLI'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Descri'#231#227'o do campo'
            'Valor da campo no XML')
          ParentFont = False
        end
        object Memo216: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[Date]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo5: TfrxMemoView
          AllowVectorExport = True
          Left = 548.031850000000000000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'p'#225'g. [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object MasterData12: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 20.787401574803150000
        Top = 200.315090000000000000
        Width = 699.213050000000000000
        DataSet = frxDsNFeXMLI
        DataSetName = 'frxDsNFeXMLI'
        RowCount = 0
        object Memo301: TfrxMemoView
          AllowVectorExport = True
          Width = 18.897650000000000000
          Height = 20.787401574803150000
          DataSet = frxDsNFeXMLI
          DataSetName = 'frxDsNFeXMLI'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsNFeXMLI."Codigo"]'
            '[frxDsNFeXMLI."ID"]')
          ParentFont = False
        end
        object Memo14: TfrxMemoView
          AllowVectorExport = True
          Left = 18.897650000000000000
          Width = 52.913420000000000000
          Height = 20.787401570000000000
          DataSet = frxDsNFeXMLI
          DataSetName = 'frxDsNFeXMLI'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsNFeXMLI."Pai"]'
            '[frxDsNFeXMLI."Campo"]')
          ParentFont = False
        end
        object Memo15: TfrxMemoView
          AllowVectorExport = True
          Left = 71.811070000000000000
          Width = 627.401980000000000000
          Height = 20.787401574803150000
          DataSet = frxDsNFeXMLI
          DataSetName = 'frxDsNFeXMLI'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsNFeXMLI."Descricao"]'
            '[frxDsNFeXMLI."Valor"]')
          ParentFont = False
        end
      end
      object ReportTitle1: TfrxReportTitle
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 37.795300000000000000
        Top = 18.897650000000000000
        Width = 699.213050000000000000
        object Shape1: TfrxShapeView
          AllowVectorExport = True
          Width = 699.213050000000000000
          Height = 37.795300000000000000
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo8: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 684.094930000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsNFeCabA."NO_Cli"]')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Width = 684.094930000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line1: TfrxLineView
          AllowVectorExport = True
          Top = 18.897650000000000000
          Width = 699.213050000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
      end
    end
  end
  object QrArq: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM nfearq'
      'WHERE IDCtrl=:P0'
      ''
      '')
    Left = 288
    Top = 368
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrArqFatID: TIntegerField
      FieldName = 'FatID'
      Origin = 'nfearq.FatID'
    end
    object QrArqFatNum: TIntegerField
      FieldName = 'FatNum'
      Origin = 'nfearq.FatNum'
    end
    object QrArqEmpresa: TIntegerField
      FieldName = 'Empresa'
      Origin = 'nfearq.Empresa'
    end
    object QrArqIDCtrl: TIntegerField
      FieldName = 'IDCtrl'
      Origin = 'nfearq.IDCtrl'
    end
    object QrArqXML_NFe: TWideMemoField
      FieldName = 'XML_NFe'
      Origin = 'nfearq.XML_NFe'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrArqXML_Aut: TWideMemoField
      FieldName = 'XML_Aut'
      Origin = 'nfearq.XML_Aut'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrArqXML_Can: TWideMemoField
      FieldName = 'XML_Can'
      Origin = 'nfearq.XML_Can'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrArqLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'nfearq.Lk'
    end
    object QrArqDataCad: TDateField
      FieldName = 'DataCad'
      Origin = 'nfearq.DataCad'
    end
    object QrArqDataAlt: TDateField
      FieldName = 'DataAlt'
      Origin = 'nfearq.DataAlt'
    end
    object QrArqUserCad: TIntegerField
      FieldName = 'UserCad'
      Origin = 'nfearq.UserCad'
    end
    object QrArqUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Origin = 'nfearq.UserAlt'
    end
    object QrArqAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Origin = 'nfearq.AlterWeb'
    end
    object QrArqAtivo: TSmallintField
      FieldName = 'Ativo'
      Origin = 'nfearq.Ativo'
    end
  end
  object PMArq: TPopupMenu
    OnPopup = PMArqPopup
    Left = 492
    Top = 436
    object NFe1: TMenuItem
      Caption = '&NFe'
      Enabled = False
      OnClick = NFe1Click
    end
    object Autorizao1: TMenuItem
      Caption = '&Autoriza'#231#227'o'
      Enabled = False
      OnClick = Autorizao1Click
    end
    object Cancelamento2: TMenuItem
      Caption = '&Cancelamento'
      Enabled = False
      OnClick = Cancelamento2Click
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object DiretriodoarquivoXML1: TMenuItem
      Caption = '&Diret'#243'rio do arquivo XML'
      OnClick = DiretriodoarquivoXML1Click
    end
  end
  object frxListaNFes: TfrxReport
    Version = '2022.1'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 40025.074573773150000000
    ReportOptions.LastChange = 40025.074573773150000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    OnGetValue = frxListaNFesGetValue
    Left = 64
    Top = 296
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsNFe_100
        DataSetName = 'frxDsNFe_100'
      end
      item
        DataSet = frxDsNFe_101
        DataSetName = 'frxDsNFe_101'
      end
      item
        DataSet = frxDsNFe_XXX
        DataSetName = 'frxDsNFe_XXX'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      Orientation = poLandscape
      PaperWidth = 297.000000000000000000
      PaperHeight = 210.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 15.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
      MirrorMode = []
      object PageHeader9: TfrxPageHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 60.472480000000000000
        Top = 79.370130000000000000
        Width = 990.236860000000000000
        object Shape7: TfrxShapeView
          AllowVectorExport = True
          Width = 990.236860000000000000
          Height = 37.795300000000000000
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Line24: TfrxLineView
          AllowVectorExport = True
          Top = 18.897650000000000000
          Width = 990.236860000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo293: TfrxMemoView
          AllowVectorExport = True
          Left = 151.181200000000000000
          Width = 687.874460000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'Per'#237'odo da pesquisa: [VARF_PERIODO]')
          ParentFont = False
        end
        object Memo216: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[Date]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo5: TfrxMemoView
          AllowVectorExport = True
          Left = 839.055660000000000000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'p'#225'g. [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo8: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 264.566929130000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            'Status: [VARF_STATUS]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo42: TfrxMemoView
          AllowVectorExport = True
          Left = 272.126160000000000000
          Top = 18.897650000000000000
          Width = 445.984540000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_APENAS_EMP]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo43: TfrxMemoView
          AllowVectorExport = True
          Left = 718.110700000000000000
          Top = 18.897650000000000000
          Width = 264.567100000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'Ambiente: [VARF_AMBIENTE]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo44: TfrxMemoView
          AllowVectorExport = True
          Top = 41.574830000000000000
          Width = 495.118430000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            'Empresa: [VARF_EMPRESA]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo45: TfrxMemoView
          AllowVectorExport = True
          Left = 495.118430000000000000
          Top = 41.574830000000000000
          Width = 495.118430000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'Terceiro: [VARF_TERCEIRO]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object MasterData12: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 13.228346460000000000
        Top = 260.787570000000000000
        Width = 990.236860000000000000
        DataSet = frxDsNFe_100
        DataSetName = 'frxDsNFe_100'
        RowCount = 0
        object Memo14: TfrxMemoView
          AllowVectorExport = True
          Width = 64.252010000000000000
          Height = 13.228346460000000000
          DataSet = frxDsNFe_100
          DataSetName = 'frxDsNFe_100'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[FormatFloat('#39'000,000,000'#39', <frxDsNFe_100."ide_nNF">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo15: TfrxMemoView
          AllowVectorExport = True
          Left = 64.252010000000000000
          Width = 22.677180000000000000
          Height = 13.228346460000000000
          DataSet = frxDsNFe_100
          DataSetName = 'frxDsNFe_100'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[FormatFloat('#39'000'#39', <frxDsNFe_100."ide_serie">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo6: TfrxMemoView
          AllowVectorExport = True
          Left = 86.929190000000000000
          Width = 45.354330710000000000
          Height = 13.228346460000000000
          DataField = 'ide_dEmi'
          DataSet = frxDsNFe_100
          DataSetName = 'frxDsNFe_100'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsNFe_100."ide_dEmi"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo9: TfrxMemoView
          AllowVectorExport = True
          Left = 132.283550000000000000
          Width = 68.031496062992130000
          Height = 13.228346460000000000
          DataField = 'ICMSTot_vProd'
          DataSet = frxDsNFe_100
          DataSetName = 'frxDsNFe_100'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsNFe_100."ICMSTot_vProd"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo11: TfrxMemoView
          AllowVectorExport = True
          Left = 566.929500000000000000
          Width = 68.031496060000000000
          Height = 13.228346460000000000
          DataField = 'ICMSTot_vNF'
          DataSet = frxDsNFe_100
          DataSetName = 'frxDsNFe_100'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsNFe_100."ICMSTot_vNF"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo13: TfrxMemoView
          AllowVectorExport = True
          Left = 200.315090000000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          DataField = 'ICMSTot_vST'
          DataSet = frxDsNFe_100
          DataSetName = 'frxDsNFe_100'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsNFe_100."ICMSTot_vST"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo17: TfrxMemoView
          AllowVectorExport = True
          Left = 260.787570000000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          DataField = 'ICMSTot_vFrete'
          DataSet = frxDsNFe_100
          DataSetName = 'frxDsNFe_100'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsNFe_100."ICMSTot_vFrete"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo19: TfrxMemoView
          AllowVectorExport = True
          Left = 321.260050000000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          DataField = 'ICMSTot_vSeg'
          DataSet = frxDsNFe_100
          DataSetName = 'frxDsNFe_100'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsNFe_100."ICMSTot_vSeg"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo24: TfrxMemoView
          AllowVectorExport = True
          Left = 381.732530000000000000
          Width = 64.251968503937010000
          Height = 13.228346460000000000
          DataField = 'ICMSTot_vIPI'
          DataSet = frxDsNFe_100
          DataSetName = 'frxDsNFe_100'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsNFe_100."ICMSTot_vIPI"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo25: TfrxMemoView
          AllowVectorExport = True
          Left = 445.984540000000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          DataField = 'ICMSTot_vOutro'
          DataSet = frxDsNFe_100
          DataSetName = 'frxDsNFe_100'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsNFe_100."ICMSTot_vOutro"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo26: TfrxMemoView
          AllowVectorExport = True
          Left = 506.457020000000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          DataField = 'ICMSTot_vDesc'
          DataSet = frxDsNFe_100
          DataSetName = 'frxDsNFe_100'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsNFe_100."ICMSTot_vDesc"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo27: TfrxMemoView
          AllowVectorExport = True
          Left = 634.961040000000000000
          Width = 68.031500940000000000
          Height = 13.228346460000000000
          DataField = 'ICMSTot_vBC'
          DataSet = frxDsNFe_100
          DataSetName = 'frxDsNFe_100'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsNFe_100."ICMSTot_vBC"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo29: TfrxMemoView
          AllowVectorExport = True
          Left = 702.992580000000000000
          Width = 64.251968500000000000
          Height = 13.228346460000000000
          DataField = 'ICMSTot_vICMS'
          DataSet = frxDsNFe_100
          DataSetName = 'frxDsNFe_100'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsNFe_100."ICMSTot_vICMS"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo63: TfrxMemoView
          AllowVectorExport = True
          Left = 888.189550000000000000
          Width = 86.929141180000000000
          Height = 13.228346460000000000
          DataField = 'NOME_tpEmis'
          DataSet = frxDsNFe_100
          DataSetName = 'frxDsNFe_100'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsNFe_100."NOME_tpEmis"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo64: TfrxMemoView
          AllowVectorExport = True
          Left = 975.118740000000000000
          Width = 15.118071180000000000
          Height = 13.228346460000000000
          DataField = 'NOME_tpNF'
          DataSet = frxDsNFe_100
          DataSetName = 'frxDsNFe_100'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsNFe_100."NOME_tpNF"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo93: TfrxMemoView
          AllowVectorExport = True
          Left = 767.244590000000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          DataField = 'ICMSTot_vPIS'
          DataSet = frxDsNFe_100
          DataSetName = 'frxDsNFe_100'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsNFe_100."ICMSTot_vPIS"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo96: TfrxMemoView
          AllowVectorExport = True
          Left = 827.717070000000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          DataField = 'ICMSTot_vCOFINS'
          DataSet = frxDsNFe_100
          DataSetName = 'frxDsNFe_100'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsNFe_100."ICMSTot_vCOFINS"]')
          ParentFont = False
          WordWrap = False
        end
      end
      object ReportTitle1: TfrxReportTitle
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 37.795300000000000000
        Top = 18.897650000000000000
        Width = 990.236860000000000000
        object Shape1: TfrxShapeView
          AllowVectorExport = True
          Width = 990.236860000000000000
          Height = 37.795300000000000000
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo215: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 975.118740000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'Lista de Notas Fiscais Eletr'#244'nicas')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo2: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Width = 975.118740000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line1: TfrxLineView
          AllowVectorExport = True
          Top = 18.897650000000000000
          Width = 990.236860000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
      end
      object PageFooter3: TfrxPageFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118110240000000000
        Top = 623.622450000000000000
        Width = 990.236860000000000000
        object Memo120: TfrxMemoView
          AllowVectorExport = True
          Width = 1046.929810000000000000
          Height = 15.118110240000000000
          DataSet = DModFin.frxDsExtratos
          DataSetName = 'frxDsExtratos'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
      end
      object GroupHeader1: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 35.905526460000000000
        Top = 200.315090000000000000
        Width = 990.236860000000000000
        Condition = 'frxDsNFe_100."Ordem"'
        object Memo1: TfrxMemoView
          AllowVectorExport = True
          Top = 22.677180000000000000
          Width = 64.252010000000000000
          Height = 13.228346460000000000
          DataSetName = 'frxDsNFeXMLI_'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'N'#186' NF-e')
          ParentFont = False
          WordWrap = False
        end
        object Memo3: TfrxMemoView
          AllowVectorExport = True
          Left = 64.252010000000000000
          Top = 22.677180000000000000
          Width = 22.677180000000000000
          Height = 13.228346460000000000
          DataSetName = 'frxDsNFeXMLI_'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'S'#233'rie')
          ParentFont = False
          WordWrap = False
        end
        object Memo4: TfrxMemoView
          AllowVectorExport = True
          Left = 86.929190000000000000
          Top = 22.677180000000000000
          Width = 45.354330710000000000
          Height = 13.228346460000000000
          DataSetName = 'frxDsNFeXMLI_'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Emiss'#227'o')
          ParentFont = False
          WordWrap = False
        end
        object Memo7: TfrxMemoView
          AllowVectorExport = True
          Left = 132.283550000000000000
          Top = 22.677180000000000000
          Width = 68.031496062992130000
          Height = 13.228346460000000000
          DataSetName = 'frxDsNFeXMLI_'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Produtos')
          ParentFont = False
          WordWrap = False
        end
        object Memo10: TfrxMemoView
          AllowVectorExport = True
          Left = 566.929500000000000000
          Top = 22.677180000000000000
          Width = 68.031496060000000000
          Height = 13.228346460000000000
          DataSetName = 'frxDsNFeXMLI_'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Total NFe')
          ParentFont = False
          WordWrap = False
        end
        object Memo12: TfrxMemoView
          AllowVectorExport = True
          Left = 200.315090000000000000
          Top = 22.677180000000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          DataSetName = 'frxDsNFeXMLI_'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'ICMS ST')
          ParentFont = False
          WordWrap = False
        end
        object Memo16: TfrxMemoView
          AllowVectorExport = True
          Left = 260.787570000000000000
          Top = 22.677180000000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          DataSetName = 'frxDsNFeXMLI_'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Frete')
          ParentFont = False
          WordWrap = False
        end
        object Memo18: TfrxMemoView
          AllowVectorExport = True
          Left = 321.260050000000000000
          Top = 22.677180000000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          DataSetName = 'frxDsNFeXMLI_'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Seguro')
          ParentFont = False
          WordWrap = False
        end
        object Memo20: TfrxMemoView
          AllowVectorExport = True
          Left = 381.732530000000000000
          Top = 22.677180000000000000
          Width = 64.251968503937010000
          Height = 13.228346460000000000
          DataSetName = 'frxDsNFeXMLI_'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'IPI')
          ParentFont = False
          WordWrap = False
        end
        object Memo21: TfrxMemoView
          AllowVectorExport = True
          Left = 445.984540000000000000
          Top = 22.677180000000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          DataSetName = 'frxDsNFeXMLI_'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Outras desp.')
          ParentFont = False
          WordWrap = False
        end
        object Memo22: TfrxMemoView
          AllowVectorExport = True
          Left = 506.457020000000000000
          Top = 22.677180000000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          DataSetName = 'frxDsNFeXMLI_'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Desconto')
          ParentFont = False
          WordWrap = False
        end
        object Memo23: TfrxMemoView
          AllowVectorExport = True
          Left = 634.961040000000000000
          Top = 22.677180000000000000
          Width = 68.031496060000000000
          Height = 13.228346460000000000
          DataSetName = 'frxDsNFeXMLI_'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'BC ICMS')
          ParentFont = False
          WordWrap = False
        end
        object Memo28: TfrxMemoView
          AllowVectorExport = True
          Left = 702.992580000000000000
          Top = 22.677180000000000000
          Width = 64.251968500000000000
          Height = 13.228346460000000000
          DataSetName = 'frxDsNFeXMLI_'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'ICMS')
          ParentFont = False
          WordWrap = False
        end
        object Memo34: TfrxMemoView
          AllowVectorExport = True
          Top = 3.779530000000000000
          Width = 94.488250000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            'AUTORIZADA')
          ParentFont = False
        end
        object Memo61: TfrxMemoView
          AllowVectorExport = True
          Left = 888.189550000000000000
          Top = 22.677180000000000000
          Width = 86.929141180000000000
          Height = 13.228346460000000000
          DataSetName = 'frxDsNFeXMLI_'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Tipo')
          ParentFont = False
          WordWrap = False
        end
        object Memo62: TfrxMemoView
          AllowVectorExport = True
          Left = 975.118740000000000000
          Top = 22.677180000000000000
          Width = 15.118071180000000000
          Height = 13.228346460000000000
          DataSetName = 'frxDsNFeXMLI_'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'E/S')
          ParentFont = False
          WordWrap = False
        end
        object Memo94: TfrxMemoView
          AllowVectorExport = True
          Left = 767.244590000000000000
          Top = 22.677180000000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          DataSetName = 'frxDsNFeXMLI_'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'PIS')
          ParentFont = False
          WordWrap = False
        end
        object Memo97: TfrxMemoView
          AllowVectorExport = True
          Left = 827.717070000000000000
          Top = 22.677180000000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          DataSetName = 'frxDsNFeXMLI_'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'COFINS')
          ParentFont = False
          WordWrap = False
        end
      end
      object GroupFooter1: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 22.677177560000000000
        Top = 298.582870000000000000
        Width = 990.236860000000000000
        object Memo54: TfrxMemoView
          AllowVectorExport = True
          Left = 566.929500000000000000
          Width = 68.031496060000000000
          Height = 13.228346460000000000
          DataSet = frxDsNFe_100
          DataSetName = 'frxDsNFe_100'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsNFe_100."ICMSTot_vNF">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo55: TfrxMemoView
          AllowVectorExport = True
          Left = 321.260050000000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          DataSet = frxDsNFe_100
          DataSetName = 'frxDsNFe_100'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsNFe_100."ICMSTot_vSeg">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo56: TfrxMemoView
          AllowVectorExport = True
          Left = 381.732530000000000000
          Width = 64.251968503937010000
          Height = 13.228346460000000000
          DataSet = frxDsNFe_100
          DataSetName = 'frxDsNFe_100'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsNFe_100."ICMSTot_vIPI">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo57: TfrxMemoView
          AllowVectorExport = True
          Left = 445.984540000000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          DataSet = frxDsNFe_100
          DataSetName = 'frxDsNFe_100'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsNFe_100."ICMSTot_vOutro">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo58: TfrxMemoView
          AllowVectorExport = True
          Left = 506.457020000000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          DataSet = frxDsNFe_100
          DataSetName = 'frxDsNFe_100'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsNFe_100."ICMSTot_vDesc">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo59: TfrxMemoView
          AllowVectorExport = True
          Left = 634.961040000000000000
          Width = 68.031496060000000000
          Height = 13.228346460000000000
          DataSet = frxDsNFe_100
          DataSetName = 'frxDsNFe_100'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsNFe_100."ICMSTot_vBC">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo60: TfrxMemoView
          AllowVectorExport = True
          Left = 702.992580000000000000
          Width = 64.251968500000000000
          Height = 13.228346460000000000
          DataSet = frxDsNFe_100
          DataSetName = 'frxDsNFe_100'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsNFe_100."ICMSTot_vICMS">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo65: TfrxMemoView
          AllowVectorExport = True
          Width = 132.283501180000000000
          Height = 13.228346460000000000
          DataSetName = 'frxDsNFeXMLI_'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'TOTAIS:  ')
          ParentFont = False
          WordWrap = False
        end
        object Memo66: TfrxMemoView
          AllowVectorExport = True
          Left = 888.189550000000000000
          Width = 102.047261180000000000
          Height = 13.228346460000000000
          DataSetName = 'frxDsNFeXMLI_'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
          WordWrap = False
        end
        object Memo67: TfrxMemoView
          AllowVectorExport = True
          Left = 132.283550000000000000
          Width = 68.031496060000000000
          Height = 13.228346460000000000
          DataSet = frxDsNFe_100
          DataSetName = 'frxDsNFe_100'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsNFe_100."ICMSTot_vProd">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo68: TfrxMemoView
          AllowVectorExport = True
          Left = 200.315090000000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          DataSet = frxDsNFe_100
          DataSetName = 'frxDsNFe_100'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsNFe_100."ICMSTot_vST">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo69: TfrxMemoView
          AllowVectorExport = True
          Left = 260.787570000000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          DataSet = frxDsNFe_100
          DataSetName = 'frxDsNFe_100'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsNFe_100."ICMSTot_vFrete">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo95: TfrxMemoView
          AllowVectorExport = True
          Left = 767.244590000000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          DataSet = frxDsNFe_100
          DataSetName = 'frxDsNFe_100'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsNFe_100."ICMSTot_vPIS">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo98: TfrxMemoView
          AllowVectorExport = True
          Left = 827.717070000000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          DataSet = frxDsNFe_100
          DataSetName = 'frxDsNFe_100'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsNFe_100."ICMSTot_vCOFINS">)]')
          ParentFont = False
          WordWrap = False
        end
      end
      object GroupHeader2: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 35.905526460000000000
        Top = 343.937230000000000000
        Width = 990.236860000000000000
        Condition = 'frxDsNFe_101."Ordem"'
        object Memo31: TfrxMemoView
          AllowVectorExport = True
          Left = 177.637910000000000000
          Top = 22.677180000000000000
          Width = 68.031491180000000000
          Height = 13.228346460000000000
          DataSetName = 'frxDsNFeXMLI_'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Tipo')
          ParentFont = False
          WordWrap = False
        end
        object Memo32: TfrxMemoView
          AllowVectorExport = True
          Left = 245.669450000000000000
          Top = 22.677180000000000000
          Width = 15.118071180000000000
          Height = 13.228346460000000000
          DataSetName = 'frxDsNFeXMLI_'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'E/S')
          ParentFont = False
          WordWrap = False
        end
        object Memo35: TfrxMemoView
          AllowVectorExport = True
          Top = 3.779530000000000000
          Width = 990.236860000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            'CANCELADA')
          ParentFont = False
        end
        object Memo39: TfrxMemoView
          AllowVectorExport = True
          Top = 22.677180000000000000
          Width = 64.252010000000000000
          Height = 13.228346460000000000
          DataSetName = 'frxDsNFeXMLI_'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'N'#186' NF-e')
          ParentFont = False
          WordWrap = False
        end
        object Memo40: TfrxMemoView
          AllowVectorExport = True
          Left = 64.252010000000000000
          Top = 22.677180000000000000
          Width = 22.677180000000000000
          Height = 13.228346460000000000
          DataSetName = 'frxDsNFeXMLI_'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'S'#233'rie')
          ParentFont = False
          WordWrap = False
        end
        object Memo41: TfrxMemoView
          AllowVectorExport = True
          Left = 132.283550000000000000
          Top = 22.677180000000000000
          Width = 45.354330710000000000
          Height = 13.228346460000000000
          DataSetName = 'frxDsNFeXMLI_'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Emiss'#227'o')
          ParentFont = False
          WordWrap = False
        end
        object Memo46: TfrxMemoView
          AllowVectorExport = True
          Left = 86.929190000000000000
          Top = 22.677180000000000000
          Width = 22.677180000000000000
          Height = 13.228346460000000000
          DataSetName = 'frxDsNFeXMLI_'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Ano')
          ParentFont = False
          WordWrap = False
        end
        object Memo47: TfrxMemoView
          AllowVectorExport = True
          Left = 109.606370000000000000
          Top = 22.677180000000000000
          Width = 22.677180000000000000
          Height = 13.228346460000000000
          DataSetName = 'frxDsNFeXMLI_'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'M'#234's')
          ParentFont = False
          WordWrap = False
        end
        object Memo50: TfrxMemoView
          AllowVectorExport = True
          Left = 260.787570000000000000
          Top = 22.677180000000000000
          Width = 226.771653540000000000
          Height = 13.228346460000000000
          DataSetName = 'frxDsNFeXMLI_'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Chave NF-e')
          ParentFont = False
          WordWrap = False
        end
        object Memo52: TfrxMemoView
          AllowVectorExport = True
          Left = 638.740570000000000000
          Top = 22.677180000000000000
          Width = 351.496260710000000000
          Height = 13.228346460000000000
          DataSetName = 'frxDsNFeXMLI_'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Motivo ou descri'#231#227'o do status')
          ParentFont = False
          WordWrap = False
        end
        object Memo89: TfrxMemoView
          AllowVectorExport = True
          Left = 487.559370000000000000
          Top = 22.677180000000000000
          Width = 79.370081180000000000
          Height = 13.228346460000000000
          DataSetName = 'frxDsNFeXMLI_'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'dh Recibo')
          ParentFont = False
          WordWrap = False
        end
        object Memo91: TfrxMemoView
          AllowVectorExport = True
          Left = 566.929500000000000000
          Top = 22.677180000000000000
          Width = 71.811021180000000000
          Height = 13.228346460000000000
          DataSetName = 'frxDsNFeXMLI_'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'N'#186' protocolo canc.')
          ParentFont = False
          WordWrap = False
        end
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 13.228346460000000000
        Top = 404.409710000000000000
        Width = 990.236860000000000000
        DataSet = frxDsNFe_101
        DataSetName = 'frxDsNFe_101'
        RowCount = 0
        object Memo30: TfrxMemoView
          AllowVectorExport = True
          Left = 177.637910000000000000
          Width = 68.031491180000000000
          Height = 13.228346460000000000
          DataSet = frxDsNFe_100
          DataSetName = 'frxDsNFe_100'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsNFe_101."NOME_tpEmis"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo33: TfrxMemoView
          AllowVectorExport = True
          Left = 245.669450000000000000
          Width = 15.118071180000000000
          Height = 13.228346460000000000
          DataSet = frxDsNFe_100
          DataSetName = 'frxDsNFe_100'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsNFe_101."NOME_tpNF"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo36: TfrxMemoView
          AllowVectorExport = True
          Width = 64.252010000000000000
          Height = 13.228346460000000000
          DataSet = frxDsNFe_100
          DataSetName = 'frxDsNFe_100'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[FormatFloat('#39'000,000,000'#39', <frxDsNFe_101."ide_nNF">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo37: TfrxMemoView
          AllowVectorExport = True
          Left = 64.252010000000000000
          Width = 22.677180000000000000
          Height = 13.228346460000000000
          DataSet = frxDsNFe_XXX
          DataSetName = 'frxDsNFe_XXX'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsNFe_101."ide_serie"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo38: TfrxMemoView
          AllowVectorExport = True
          Left = 132.283550000000000000
          Width = 45.354330710000000000
          Height = 13.228346460000000000
          DataSet = frxDsNFe_XXX
          DataSetName = 'frxDsNFe_XXX'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsNFe_101."ide_dEmi"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo48: TfrxMemoView
          AllowVectorExport = True
          Left = 86.929190000000000000
          Width = 22.677180000000000000
          Height = 13.228346460000000000
          DataSet = frxDsNFe_XXX
          DataSetName = 'frxDsNFe_XXX'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsNFe_101."ide_AAMM_AA"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo49: TfrxMemoView
          AllowVectorExport = True
          Left = 109.606370000000000000
          Width = 22.677180000000000000
          Height = 13.228346460000000000
          DataSet = frxDsNFe_XXX
          DataSetName = 'frxDsNFe_XXX'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsNFe_101."ide_AAMM_MM"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo51: TfrxMemoView
          AllowVectorExport = True
          Left = 260.787570000000000000
          Width = 226.771653540000000000
          Height = 13.228346460000000000
          DataField = 'Id_TXT'
          DataSet = frxDsNFe_101
          DataSetName = 'frxDsNFe_101'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsNFe_101."Id_TXT"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo53: TfrxMemoView
          AllowVectorExport = True
          Left = 638.740570000000000000
          Width = 351.496260710000000000
          Height = 13.228346460000000000
          DataSet = frxDsNFe_XXX
          DataSetName = 'frxDsNFe_XXX'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsNFe_101."Motivo"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo90: TfrxMemoView
          AllowVectorExport = True
          Left = 487.559370000000000000
          Width = 79.370081180000000000
          Height = 13.228346460000000000
          DataField = 'infCanc_dhRecbto'
          DataSet = frxDsNFe_101
          DataSetName = 'frxDsNFe_101'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsNFe_101."infCanc_dhRecbto"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo92: TfrxMemoView
          AllowVectorExport = True
          Left = 566.929500000000000000
          Width = 71.811021180000000000
          Height = 13.228346460000000000
          DataSet = frxDsNFe_101
          DataSetName = 'frxDsNFe_101'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsNFe_101."infCanc_nProt"]')
          ParentFont = False
          WordWrap = False
        end
      end
      object GroupHeader3: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 35.905526460000000000
        Top = 464.882190000000000000
        Width = 990.236860000000000000
        Condition = 'frxDsNFe_XXX."Ordem"'
        object Memo70: TfrxMemoView
          AllowVectorExport = True
          Left = 177.637910000000000000
          Top = 22.677180000000000000
          Width = 68.031491180000000000
          Height = 13.228346460000000000
          DataSetName = 'frxDsNFeXMLI_'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Tipo')
          ParentFont = False
          WordWrap = False
        end
        object Memo71: TfrxMemoView
          AllowVectorExport = True
          Left = 245.669450000000000000
          Top = 22.677180000000000000
          Width = 15.118071180000000000
          Height = 13.228346460000000000
          DataSetName = 'frxDsNFeXMLI_'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'E/S')
          ParentFont = False
          WordWrap = False
        end
        object Memo72: TfrxMemoView
          AllowVectorExport = True
          Top = 3.779530000000000000
          Width = 990.236860000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsNFe_XXX."NOME_ORDEM"]')
          ParentFont = False
        end
        object Memo73: TfrxMemoView
          AllowVectorExport = True
          Top = 22.677180000000000000
          Width = 64.252010000000000000
          Height = 13.228346460000000000
          DataSetName = 'frxDsNFeXMLI_'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'N'#186' NF-e')
          ParentFont = False
          WordWrap = False
        end
        object Memo74: TfrxMemoView
          AllowVectorExport = True
          Left = 64.252010000000000000
          Top = 22.677180000000000000
          Width = 22.677180000000000000
          Height = 13.228346460000000000
          DataSetName = 'frxDsNFeXMLI_'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'S'#233'rie')
          ParentFont = False
          WordWrap = False
        end
        object Memo75: TfrxMemoView
          AllowVectorExport = True
          Left = 132.283550000000000000
          Top = 22.677180000000000000
          Width = 45.354330710000000000
          Height = 13.228346460000000000
          DataSetName = 'frxDsNFeXMLI_'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Emiss'#227'o')
          ParentFont = False
          WordWrap = False
        end
        object Memo76: TfrxMemoView
          AllowVectorExport = True
          Left = 86.929190000000000000
          Top = 22.677180000000000000
          Width = 22.677180000000000000
          Height = 13.228346460000000000
          DataSetName = 'frxDsNFeXMLI_'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Ano')
          ParentFont = False
          WordWrap = False
        end
        object Memo77: TfrxMemoView
          AllowVectorExport = True
          Left = 109.606370000000000000
          Top = 22.677180000000000000
          Width = 22.677180000000000000
          Height = 13.228346460000000000
          DataSetName = 'frxDsNFeXMLI_'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'M'#234's')
          ParentFont = False
          WordWrap = False
        end
        object Memo78: TfrxMemoView
          AllowVectorExport = True
          Left = 260.787570000000000000
          Top = 22.677180000000000000
          Width = 30.236240000000000000
          Height = 13.228346460000000000
          DataSetName = 'frxDsNFeXMLI_'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Status')
          ParentFont = False
          WordWrap = False
        end
        object Memo79: TfrxMemoView
          AllowVectorExport = True
          Left = 291.023810000000000000
          Top = 22.677180000000000000
          Width = 699.213020710000000000
          Height = 13.228346460000000000
          DataSetName = 'frxDsNFeXMLI_'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Motivo ou descri'#231#227'o do status')
          ParentFont = False
          WordWrap = False
        end
      end
      object MasterData2: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 13.228346460000000000
        Top = 525.354670000000000000
        Width = 990.236860000000000000
        DataSet = frxDsNFe_XXX
        DataSetName = 'frxDsNFe_XXX'
        RowCount = 0
        object Memo80: TfrxMemoView
          AllowVectorExport = True
          Left = 177.637910000000000000
          Width = 68.031491180000000000
          Height = 13.228346460000000000
          DataSet = frxDsNFe_100
          DataSetName = 'frxDsNFe_100'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsNFe_XXX."NOME_tpEmis"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo81: TfrxMemoView
          AllowVectorExport = True
          Left = 245.669450000000000000
          Width = 15.118071180000000000
          Height = 13.228346460000000000
          DataSet = frxDsNFe_100
          DataSetName = 'frxDsNFe_100'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsNFe_XXX."NOME_tpNF"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo82: TfrxMemoView
          AllowVectorExport = True
          Width = 64.252010000000000000
          Height = 13.228346460000000000
          DataSet = frxDsNFe_100
          DataSetName = 'frxDsNFe_100'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[FormatFloat('#39'000,000,000'#39', <frxDsNFe_XXX."ide_nNF">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo83: TfrxMemoView
          AllowVectorExport = True
          Left = 64.252010000000000000
          Width = 22.677180000000000000
          Height = 13.228346460000000000
          DataField = 'ide_serie'
          DataSet = frxDsNFe_XXX
          DataSetName = 'frxDsNFe_XXX'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsNFe_XXX."ide_serie"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo84: TfrxMemoView
          AllowVectorExport = True
          Left = 132.283550000000000000
          Width = 45.354330710000000000
          Height = 13.228346460000000000
          DataField = 'ide_dEmi'
          DataSet = frxDsNFe_XXX
          DataSetName = 'frxDsNFe_XXX'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsNFe_XXX."ide_dEmi"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo85: TfrxMemoView
          AllowVectorExport = True
          Left = 86.929190000000000000
          Width = 22.677180000000000000
          Height = 13.228346460000000000
          DataSet = frxDsNFe_XXX
          DataSetName = 'frxDsNFe_XXX'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsNFe_XXX."ide_AAMM_AA"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo86: TfrxMemoView
          AllowVectorExport = True
          Left = 109.606370000000000000
          Width = 22.677180000000000000
          Height = 13.228346460000000000
          DataField = 'ide_AAMM_MM'
          DataSet = frxDsNFe_XXX
          DataSetName = 'frxDsNFe_XXX'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsNFe_XXX."ide_AAMM_MM"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo87: TfrxMemoView
          AllowVectorExport = True
          Left = 260.787570000000000000
          Width = 30.236240000000000000
          Height = 13.228346460000000000
          DataSet = frxDsNFe_XXX
          DataSetName = 'frxDsNFe_XXX'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[FormatFloat('#39'000'#39', <frxDsNFe_XXX."ide_serie">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo88: TfrxMemoView
          AllowVectorExport = True
          Left = 291.023810000000000000
          Width = 699.213020710000000000
          Height = 13.228346460000000000
          DataField = 'Motivo'
          DataSet = frxDsNFe_XXX
          DataSetName = 'frxDsNFe_XXX'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsNFe_XXX."Motivo"]')
          ParentFont = False
          WordWrap = False
        end
      end
      object Footer1: TfrxFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Top = 442.205010000000000000
        Width = 990.236860000000000000
      end
      object GroupFooter2: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Top = 563.149970000000000000
        Width = 990.236860000000000000
      end
    end
  end
  object QrNFe_100: TMySQLQuery
    Database = Dmod.ZZDB
    SQL.Strings = (
      'SELECT * FROM nfe_100'
      'ORDER BY ide_nNF')
    Left = 64
    Top = 344
    object QrNFe_100Ordem: TIntegerField
      FieldName = 'Ordem'
    end
    object QrNFe_100ide_nNF: TIntegerField
      FieldName = 'ide_nNF'
    end
    object QrNFe_100ide_serie: TIntegerField
      FieldName = 'ide_serie'
    end
    object QrNFe_100ide_dEmi: TDateField
      FieldName = 'ide_dEmi'
    end
    object QrNFe_100ICMSTot_vProd: TFloatField
      FieldName = 'ICMSTot_vProd'
    end
    object QrNFe_100ICMSTot_vST: TFloatField
      FieldName = 'ICMSTot_vST'
    end
    object QrNFe_100ICMSTot_vFrete: TFloatField
      FieldName = 'ICMSTot_vFrete'
    end
    object QrNFe_100ICMSTot_vSeg: TFloatField
      FieldName = 'ICMSTot_vSeg'
    end
    object QrNFe_100ICMSTot_vIPI: TFloatField
      FieldName = 'ICMSTot_vIPI'
    end
    object QrNFe_100ICMSTot_vOutro: TFloatField
      FieldName = 'ICMSTot_vOutro'
    end
    object QrNFe_100ICMSTot_vDesc: TFloatField
      FieldName = 'ICMSTot_vDesc'
    end
    object QrNFe_100ICMSTot_vNF: TFloatField
      FieldName = 'ICMSTot_vNF'
    end
    object QrNFe_100ICMSTot_vBC: TFloatField
      FieldName = 'ICMSTot_vBC'
    end
    object QrNFe_100ICMSTot_vICMS: TFloatField
      FieldName = 'ICMSTot_vICMS'
    end
    object QrNFe_100ICMSTot_vPIS: TFloatField
      FieldName = 'ICMSTot_vPIS'
    end
    object QrNFe_100ICMSTot_vCOFINS: TFloatField
      FieldName = 'ICMSTot_vCOFINS'
    end
    object QrNFe_100NOME_tpEmis: TWideStringField
      FieldName = 'NOME_tpEmis'
      Size = 30
    end
    object QrNFe_100NOME_tpNF: TWideStringField
      FieldName = 'NOME_tpNF'
      Size = 1
    end
  end
  object frxDsNFe_100: TfrxDBDataset
    UserName = 'frxDsNFe_100'
    CloseDataSource = False
    DataSet = QrNFe_100
    BCDToCurrency = False
    DataSetOptions = []
    Left = 64
    Top = 396
  end
  object QrNFe_XXX: TMySQLQuery
    Database = Dmod.ZZDB
    OnCalcFields = QrNFe_XXXCalcFields
    SQL.Strings = (
      'SELECT * FROM nfe_xxx'
      'ORDER BY Ordem, ide_nNF;')
    Left = 216
    Top = 340
    object QrNFe_XXXOrdem: TIntegerField
      FieldName = 'Ordem'
    end
    object QrNFe_XXXcStat: TIntegerField
      FieldName = 'cStat'
    end
    object QrNFe_XXXide_nNF: TIntegerField
      FieldName = 'ide_nNF'
    end
    object QrNFe_XXXide_serie: TIntegerField
      FieldName = 'ide_serie'
    end
    object QrNFe_XXXide_AAMM_AA: TWideStringField
      FieldName = 'ide_AAMM_AA'
      Size = 2
    end
    object QrNFe_XXXide_AAMM_MM: TWideStringField
      FieldName = 'ide_AAMM_MM'
      Size = 2
    end
    object QrNFe_XXXide_dEmi: TDateField
      FieldName = 'ide_dEmi'
    end
    object QrNFe_XXXNOME_tpEmis: TWideStringField
      FieldName = 'NOME_tpEmis'
      Size = 30
    end
    object QrNFe_XXXNOME_tpNF: TWideStringField
      FieldName = 'NOME_tpNF'
      Size = 1
    end
    object QrNFe_XXXStatus: TIntegerField
      FieldName = 'Status'
    end
    object QrNFe_XXXMotivo: TWideStringField
      FieldName = 'Motivo'
      Size = 255
    end
    object QrNFe_XXXNOME_ORDEM: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOME_ORDEM'
      Size = 50
      Calculated = True
    end
  end
  object frxDsNFe_XXX: TfrxDBDataset
    UserName = 'frxDsNFe_XXX'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Ordem=Ordem'
      'cStat=cStat'
      'ide_nNF=ide_nNF'
      'ide_serie=ide_serie'
      'ide_AAMM_AA=ide_AAMM_AA'
      'ide_AAMM_MM=ide_AAMM_MM'
      'ide_dEmi=ide_dEmi'
      'NOME_tpEmis=NOME_tpEmis'
      'NOME_tpNF=NOME_tpNF'
      'Status=Status'
      'Motivo=Motivo'
      'NOME_ORDEM=NOME_ORDEM')
    DataSet = QrNFe_XXX
    BCDToCurrency = False
    DataSetOptions = []
    Left = 216
    Top = 392
  end
  object QrNFe_101: TMySQLQuery
    Database = Dmod.ZZDB
    SQL.Strings = (
      'SELECT * FROM nfe_101'
      'ORDER BY ide_nNF')
    Left = 140
    Top = 344
    object QrNFe_101Ordem: TIntegerField
      FieldName = 'Ordem'
    end
    object QrNFe_101ide_nNF: TIntegerField
      FieldName = 'ide_nNF'
    end
    object QrNFe_101ide_serie: TIntegerField
      FieldName = 'ide_serie'
    end
    object QrNFe_101ide_AAMM_AA: TWideStringField
      FieldName = 'ide_AAMM_AA'
      Size = 2
    end
    object QrNFe_101ide_AAMM_MM: TWideStringField
      FieldName = 'ide_AAMM_MM'
      Size = 2
    end
    object QrNFe_101ide_dEmi: TDateField
      FieldName = 'ide_dEmi'
    end
    object QrNFe_101NOME_tpEmis: TWideStringField
      FieldName = 'NOME_tpEmis'
      Size = 30
    end
    object QrNFe_101NOME_tpNF: TWideStringField
      FieldName = 'NOME_tpNF'
      Size = 1
    end
    object QrNFe_101Id: TWideStringField
      FieldName = 'Id'
      Size = 44
    end
    object QrNFe_101infCanc_dhRecbto: TDateTimeField
      FieldName = 'infCanc_dhRecbto'
    end
    object QrNFe_101infCanc_nProt: TWideStringField
      FieldName = 'infCanc_nProt'
      Size = 15
    end
    object QrNFe_101Motivo: TWideStringField
      FieldName = 'Motivo'
      Size = 255
    end
    object QrNFe_101Id_TXT: TWideStringField
      DisplayWidth = 100
      FieldKind = fkCalculated
      FieldName = 'Id_TXT'
      Size = 100
      Calculated = True
    end
  end
  object frxDsNFe_101: TfrxDBDataset
    UserName = 'frxDsNFe_101'
    CloseDataSource = False
    DataSet = QrNFe_101
    BCDToCurrency = False
    DataSetOptions = []
    Left = 140
    Top = 392
  end
  object QrNFSe: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Numero'
      'FROM nfsenfscab'
      'WHERE Empresa=:P0'
      'AND Numero=:P1'
      'AND Ambiente=:P2')
    Left = 532
    Top = 424
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrNFSeNumero: TLargeintField
      FieldName = 'Numero'
    end
  end
  object PMConsulta: TPopupMenu
    Left = 660
    Top = 496
    object RPSselecionado1: TMenuItem
      Caption = 'RPS selecionado'
      OnClick = RPSselecionado1Click
    end
    object PorfaixadeNFSe1: TMenuItem
      Caption = 'Por faixa de NFSe'
      Enabled = False
      OnClick = PorfaixadeNFSe1Click
    end
    object Servicosprestados1: TMenuItem
      Caption = 'Servicos prestados'
      Enabled = False
      OnClick = Servicosprestados1Click
    end
    object Serviostomados1: TMenuItem
      Caption = 'Servi'#231'os tomados'
      Enabled = False
      OnClick = Serviostomados1Click
    end
  end
  object QrPsqNfse: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT NfsNumero, Empresa, Ambiente'
      'FROM nfsenfscab'
      'WHERE Empresa=-11'
      'AND DpsRpsIdNumero=1'
      'AND DpsRpsIdSerie="1"'
      'AND DpsRpsIdTipo=1'
      'AND Ambiente=2'
      '')
    Left = 776
    Top = 432
    object QrPsqNfseNfsNumero: TLargeintField
      FieldName = 'NfsNumero'
    end
    object QrPsqNfseEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrPsqNfseAmbiente: TSmallintField
      FieldName = 'Ambiente'
    end
  end
  object PMCancela: TPopupMenu
    Left = 308
    Top = 484
    object Erronaemisso1: TMenuItem
      Caption = '1. Erro na emiss'#227'o'
      OnClick = Erronaemisso1Click
    end
    object N2Servionoprestado1: TMenuItem
      Caption = '2. Servi'#231'o n'#227'o prestado'
      OnClick = N2Servionoprestado1Click
    end
    object N3Errodeassinatura1: TMenuItem
      Caption = '3. Erro de assinatura'
      Enabled = False
      OnClick = N3Errodeassinatura1Click
    end
    object N4Duplicidadedanota1: TMenuItem
      Caption = '4. Duplicidade da nota'
      OnClick = N4Duplicidadedanota1Click
    end
    object N5Errodeprocessamento1: TMenuItem
      Caption = '5. Erro de processamento'
      Enabled = False
      OnClick = N5Errodeprocessamento1Click
    end
  end
  object QrIntermediario: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Tipo, CNPJ, CPF, NIRE,'
      'IF(Tipo=0, RazaoSocial, Nome) NO_Enti'
      'FROM Entidades'
      'WHERE Cliente1="V"'
      'OR Cliente2="V"'
      'OR Cliente3="V"'
      'OR Cliente4="V"'
      'ORDER BY NO_Enti')
    Left = 688
    Top = 12
    object QrIntermediarioCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrIntermediarioNO_Enti: TWideStringField
      FieldName = 'NO_Enti'
      Required = True
      Size = 100
    end
    object QrIntermediarioCNPJ: TWideStringField
      FieldName = 'CNPJ'
      Size = 18
    end
    object QrIntermediarioCPF: TWideStringField
      FieldName = 'CPF'
      Size = 18
    end
    object QrIntermediarioTipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrIntermediarioNIRE: TWideStringField
      FieldName = 'NIRE'
      Size = 15
    end
  end
  object DsIntermediario: TDataSource
    DataSet = QrIntermediario
    Left = 692
    Top = 60
  end
  object QrLoc: TMySQLQuery
    Database = Dmod.MyDB
    Left = 568
    Top = 296
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 404
    Top = 312
  end
  object CSTabSheetChamou: TdmkCompoStore
    Left = 432
    Top = 312
  end
end
