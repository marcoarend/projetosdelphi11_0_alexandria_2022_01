unit NFSe_TbTerc;
{ Colocar no MyListas:

Uses NFSe_TbTerc;


//
function TMyListas.CriaListaTabelas(:
      UnNFSe_TbTerc.CarregaListaTabelas(Lista);
//
function TMyListas.CriaListaSQL:
    UnNFSe_TbTerc.CarregaListaSQL(Tabela, FListaSQL);
//
function TMyListas.CompletaListaSQL:
    UnNFSe_TbTerc.ComplementaListaSQL(Tabela, FListaSQL);
//
function TMyListas.CriaListaIndices:
      UnNFSe_TbTerc.CarregaListaFRIndices(TabelaBase, FRIndices, FLIndices);

//
function TMyListas.CriaListaCampos:
      UnNFSe_TbTerc.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
    UnNFSe_TbTerc.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
//
function TMyListas.CriaListaJanelas:
  UnNFSe_TbTerc.CompletaListaFRJanelas(FRJanelas, FLJanelas);

}

interface

uses
  System.Generics.Collections,
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Dialogs, DB,
  (*DBTables,*) UnMyLinguas, Forms,
  UnInternalConsts, dmkGeral, UnDmkEnums;

type
  TNFSe_TbTerc = class(TObject)
  private
    { Private declarations }
  public
    { Public declarations }
    function CarregaListaTabelas(Lista: TList<TTabelas>): Boolean;
    function CarregaListaFRCampos(Tabela: String; FRCampos:
             TCampos; FLCampos: TList<TCampos>; var TemControle: TTemControle): Boolean;
    function CompletaListaFRJanelas(FRJanelas:
             TJanelas; FLJanelas: TList<TJanelas>): Boolean;
    function CarregaListaFRIndices(Tabela: String; FRIndices:
             TIndices; FLIndices: TList<TIndices>): Boolean;
    function CarregaListaSQL(Tabela: String; FListaSQL: TStringList):
             Boolean;
    function CarregaListaFRQeiLnk(TabelaCria: String; FRQeiLnk: TQeiLnk;
             FLQeiLnk: TList<TQeiLnk>): Boolean;
    function ComplementaListaSQL(Tabela: String; FListaSQL: TStringList):
             Boolean;
    function CompletaListaFRCampos(Tabela: String; FRCampos:
             TCampos; FLCampos: TList<TCampos>): Boolean;
  end;

const
  NFE_CO_PRESTADOR = 0;
  NFE_CO_TOMADOR = 1;
  NFE_CO_INTERMEDIARIO = 2;
  NFE_CO_ADEFINIR = -1;

var
  UnNFSe_TbTerc: TNFSe_TbTerc;

implementation

uses UMySQLModule;

function TNFSe_TbTerc.CarregaListaTabelas(Lista: TList<TTabelas>): Boolean;
begin
  try
    MyLinguas.AdTbLst(Lista, False, Lowercase('ListServ'), '');
    //
    MyLinguas.AdTbLst(Lista, False, Lowercase('tsExigibilidadeIss'), '');
    MyLinguas.AdTbLst(Lista, False, Lowercase('tsStatusRps'), '');
    MyLinguas.AdTbLst(Lista, False, Lowercase('tsStatusNfse'), '');
    MyLinguas.AdTbLst(Lista, False, Lowercase('tsRegimeEspecialTributacao'), '');
    MyLinguas.AdTbLst(Lista, False, Lowercase('tsSimNao'), '');
    MyLinguas.AdTbLst(Lista, False, Lowercase('tsResponsavelRetencao'), '');
    MyLinguas.AdTbLst(Lista, False, Lowercase('tsTipoRps'), '');
    MyLinguas.AdTbLst(Lista, False, Lowercase('tsSituacaoLoteRps'), '');
    MyLinguas.AdTbLst(Lista, False, Lowercase('tsCodigoCancelamentoNfse'), '');
    MyLinguas.AdTbLst(Lista, False, Lowercase('tsErrosAlertasCab'), '');
    MyLinguas.AdTbLst(Lista, False, Lowercase('tsErrosAlertasIts'), '');
    MyLinguas.AdTbLst(Lista, False, Lowercase('tsErrosAlertasMun'), '');
    //
    MyLinguas.AdTbLst(Lista, False, Lowercase('myMunicipioIncidencia'), '');
    MyLinguas.AdTbLst(Lista, False, Lowercase('myNFSeMetodos'), '');
    //
    Result := True;
  except
    raise;
    Result := False;
  end;
end;

function TNFSe_TbTerc.CarregaListaSQL(Tabela: String; FListaSQL:
TStringList): Boolean;
begin
  Result := True;
  if Uppercase(Tabela) = Uppercase('myMunicipioIncidencia') then
  begin
    FListaSQL.Add('Codigo|Nome');
    FListaSQL.Add(Geral.FF0(NFE_CO_ADEFINIR)      + '|"A definir"');
    FListaSQL.Add(Geral.FF0(NFE_CO_PRESTADOR)     + '|"Prestador"');
    FListaSQL.Add(Geral.FF0(NFE_CO_TOMADOR)       + '|"Tomador"');
    FListaSQL.Add(Geral.FF0(NFE_CO_INTERMEDIARIO) + '|"Intermedi�rio"');
  end else
  if Uppercase(Tabela) = Uppercase('myNFSeMetodos') then
  begin
    FListaSQL.Add('Codigo|Nome');
    FListaSQL.Add('0|""');
    FListaSQL.Add('1|"ABRASF"');
  end else
  if Uppercase(Tabela) = Uppercase('tsExigibilidadeIss') then
  begin
    FListaSQL.Add('Codigo|Nome');
    FListaSQL.Add('0|""');
    FListaSQL.Add('1|"Exig�vel"');
    FListaSQL.Add('2|"N�o incid�ncia"');
    FListaSQL.Add('3|"Isen��o"');
    FListaSQL.Add('4|"Exporta��o"');
    FListaSQL.Add('5|"Imunidade"');
    FListaSQL.Add('6|"Exigibilidade Suspensa por Decis�o Judicial"');
    FListaSQL.Add('7|"Exigibilidade Suspensa por Processo Administrativo"');
  end else
  if Uppercase(Tabela) = Uppercase('tsStatusRps') then
  begin
    FListaSQL.Add('Codigo|Nome');
    FListaSQL.Add('0|""');
    FListaSQL.Add('1|"Normal"');
    FListaSQL.Add('2|"Cancelado"');
  end else
  if Uppercase(Tabela) = Uppercase('tsStatusNfse') then
  begin
    FListaSQL.Add('Codigo|Nome');
    FListaSQL.Add('0|""');
    FListaSQL.Add('1|"Normal"');
    FListaSQL.Add('2|"Cancelado"');
  end else
  if Uppercase(Tabela) = Uppercase('tsRegimeEspecialTributacao') then
  begin
    FListaSQL.Add('Codigo|Nome');
    FListaSQL.Add('0|""');
    FListaSQL.Add('1|"Microempresa municipal"');
    FListaSQL.Add('2|"Estimativa"');
    FListaSQL.Add('3|"Sociedade de profissionais"');
    FListaSQL.Add('4|"Cooperativa"');
    FListaSQL.Add('5|"Microempres�rio Individual (MEI)"');
    FListaSQL.Add('6|"Microempres�rio e Empresa de Pequeno Porte (ME EPP)"');
  end else
  if Uppercase(Tabela) = Uppercase('tsSimNao') then
  begin
    FListaSQL.Add('Codigo|Nome');
    FListaSQL.Add('0|""');
    FListaSQL.Add('1|"Sim"');
    FListaSQL.Add('2|"N�o"');
  end else
  if Uppercase(Tabela) = Uppercase('tsResponsavelRetencao') then
  begin
    FListaSQL.Add('Codigo|Nome');
    FListaSQL.Add('-1|"Sem reten��o"'); 
    FListaSQL.Add('0|"Prestador"'); // ACBr
    FListaSQL.Add('1|"Tomador"');
    FListaSQL.Add('2|"Intermedi�rio"');
  end else
  if Uppercase(Tabela) = Uppercase('tsTipoRps') then
  begin
    FListaSQL.Add('Codigo|Nome');
    FListaSQL.Add('0|""');
    FListaSQL.Add('1|"RPS"');
    FListaSQL.Add('2|"Nota Fiscal Conjugada (Mista)"');
    FListaSQL.Add('3|"Cupom"');
  end else
  if Uppercase(Tabela) = Uppercase('tsSituacaoLoteRps') then
  begin
    FListaSQL.Add('Codigo|Nome');
    FListaSQL.Add('0|""');
    FListaSQL.Add('1|"N�o Recebido"');
    FListaSQL.Add('2|"N�o Processado"');
    FListaSQL.Add('3|"Processado com Erro"');
    FListaSQL.Add('4|"Processado com Sucesso"');
  end else
  if Uppercase(Tabela) = Uppercase('tsCodigoCancelamentoNfse') then
  begin
    FListaSQL.Add('Codigo|Aplicacao|Nome');
    FListaSQL.Add('0|0|""');
    FListaSQL.Add('1|1|"Erro na emiss�o"');
    FListaSQL.Add('2|1|"Servi�o n�o prestado"');
    FListaSQL.Add('3|0|"Erro de assinatura"');
    FListaSQL.Add('4|1|"Duplicidade da nota"');
    FListaSQL.Add('5|0|"Erro de processamento "');
  { D� erro StrToInt
  end else
  if Uppercase(Tabela) = Uppercase('tsErrosAlertas') then
  begin
    FListaSQL.Add('CodTxt|Nome|Solucao');
    FListaSQL.Add('""|""|""');
  }
  end else
  ;
end;

function TNFSe_TbTerc.ComplementaListaSQL(Tabela: String; FListaSQL:
TStringList): Boolean;
begin
  Result := True;
  if Uppercase(Tabela) = Uppercase('') then
  begin
  end else if Uppercase(Tabela) = Uppercase('PerfisIts') then
  begin
    //FListaSQL.Add('"Entidades","Cadastro de pessoas f�sicas e jur�dicas (clientes, fornecedores, etc.)"');
    //FListaSQL.Add('"",""');
  end;
end;


function TNFSe_TbTerc.CarregaListaFRIndices(Tabela: String; FRIndices:
 TIndices; FLIndices: TList<TIndices>): Boolean;
begin
  Result := True;
  if Uppercase(Tabela) = Uppercase('ListServ') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE1';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'CodTxt';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'UNIQUE2';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'CodAlf';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('tsExigibilidadeIss') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
  end else
  if Uppercase(Tabela) = Uppercase('tsStatusRps') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
  end else
  if Uppercase(Tabela) = Uppercase('tsStatusNfse') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
  end else
  if Uppercase(Tabela) = Uppercase('tsRegimeEspecialTributacao') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
  end else
  if Uppercase(Tabela) = Uppercase('tsSimNao') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
  end else
  if Uppercase(Tabela) = Uppercase('tsResponsavelRetencao') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
  end else
  if Uppercase(Tabela) = Uppercase('tsTipoRps') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
  end else
  if Uppercase(Tabela) = Uppercase('tsSituacaoLoteRps') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
  end else
  if Uppercase(Tabela) = Uppercase('tsCodigoCancelamentoNfse') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
  end else
  if Uppercase(Tabela) = Uppercase('tsErrosAlertasCab') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('tsErrosAlertasIts') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
    //
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 2;
    FRIndices.Column_name   := 'CodTxt';
    FLIndices.Add(FRIndices);
  end else
  if Uppercase(Tabela) = Uppercase('tsErrosAlertasMun') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Cidade';
    FLIndices.Add(FRIndices);
    //
  end else
  if Uppercase(Tabela) = Uppercase('myMunicipioIncidencia') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
  end else
  if Uppercase(Tabela) = Uppercase('myNFSeMetodos') then
  begin
    New(FRIndices);
    FRIndices.Non_unique    := 0;
    FRIndices.Key_name      := 'PRIMARY';
    FRIndices.Seq_in_index  := 1;
    FRIndices.Column_name   := 'Codigo';
    FLIndices.Add(FRIndices);
  end else
  ;
end;

function TNFSe_TbTerc.CarregaListaFRQeiLnk(TabelaCria: String;
  FRQeiLnk: TQeiLnk; FLQeiLnk: TList<TQeiLnk>): Boolean;
begin
// n�o implementado!
  Result := False;
end;

function TNFSe_TbTerc.CarregaListaFRCampos(Tabela: String; FRCampos:
 TCampos; FLCampos: TList<TCampos>; var TemControle: TTemControle): Boolean;
begin
  Result := True;
  TemControle := TemControle + cTemControleNao;
  try
    if Uppercase(Tabela) = Uppercase('ListServ') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CodTxt';
      FRCampos.Tipo       := 'varchar(20)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := 'UNI';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CodAlf';
      FRCampos.Tipo       := 'varchar(20)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := 'UNI';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Descricao';
      FRCampos.Tipo       := 'text';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('tsExigibilidadeIss') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(120)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('tsStatusRps') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('tsStatusNfse') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('tsRegimeEspecialTributacao') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(120)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('tsSimNao') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(6)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('tsResponsavelRetencao') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('tsTipoRps') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('tsSituacaoLoteRps') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('tsCodigoCancelamentoNfse') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'varchar(12)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Aplicacao';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('tsErrosAlertasCab') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(100)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('tsErrosAlertasIts') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CodTxt';
      FRCampos.Tipo       := 'varchar(12)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Solucao';
      FRCampos.Tipo       := 'text';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('tsErrosAlertasMun') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Cidade';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('myMunicipioIncidencia') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(60)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('myNFSeMetodos') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := 'PRI';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(100)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '?';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    ;
  except
    raise;
    Result := False;
  end;
end;

function TNFSe_TbTerc.CompletaListaFRCampos(Tabela: String; FRCampos: TCampos;
  FLCampos: TList<TCampos>): Boolean;
begin
  Result := True;
  try
    if Uppercase(Tabela) = Uppercase('Controle') then
    begin
      {
      New(FRCampos);
      FRCampos.Field      := '???';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      }
    end else
    ;
  except
    raise;
    Result := False;
  end;
end;

function TNFSe_TbTerc.CompletaListaFRJanelas(FRJanelas:
 TJanelas; FLJanelas: TList<TJanelas>): Boolean;
begin
  //
  // NFS-TBTER-001 :: Tabelas de Erros e Altertas da NFS-e
  New(FRJanelas);
  FRJanelas.ID        := 'NFS-TBTER-001';
  FRJanelas.Nome      := 'FmTsErrosAlertasCab';
  FRJanelas.Descricao := 'Tabelas de Erros e Altertas da NFS-e';
  FLJanelas.Add(FRJanelas);
  //
  // NFS-TBTER-002 :: Itens de Erros e Altertas da NFS-e
  New(FRJanelas);
  FRJanelas.ID        := 'NFS-TBTER-002';
  FRJanelas.Nome      := 'FmTsErrosAlertasIts';
  FRJanelas.Descricao := 'Itens de Erros e Altertas da NFS-e';
  FLJanelas.Add(FRJanelas);
  //
  // NFS-TBTER-003 :: Adi��o de Cidade
  New(FRJanelas);
  FRJanelas.ID        := 'NFS-TBTER-003';
  FRJanelas.Nome      := 'FmTsErrosAlertasMun';
  FRJanelas.Descricao := 'Adi��o de Cidade';
  FLJanelas.Add(FRJanelas);
  //
  // NFS-TBTER-004 :: Lista de Servi�os - LC 116/03
  New(FRJanelas);
  FRJanelas.ID        := 'NFS-TBTER-004';
  FRJanelas.Nome      := 'FmListServ';
  FRJanelas.Descricao := 'Lista de Servi�os - LC 116/03';
  FLJanelas.Add(FRJanelas);
  //
  //////////////////////////////////////////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////////
  Result := True;
  //////////////////////////////////////////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////////
end;

end.
