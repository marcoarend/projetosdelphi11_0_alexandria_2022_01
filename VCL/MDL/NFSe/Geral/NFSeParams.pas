unit NFSeParams;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkPermissoes,
  dmkDBLookupComboBox, dmkEditCB, mySQLDbTables, dmkValUsu;

type
  TFmNFSeParams = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    QrEntiTipCtoWA: TmySQLQuery;
    IntegerField3: TIntegerField;
    IntegerField4: TIntegerField;
    StringField2: TWideStringField;
    DsEntiTipCtoWA: TDataSource;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    VUWhatsApp_EntiTipCto: TdmkValUsu;
    Panel5: TPanel;
    GroupBox3: TGroupBox;
    Label6: TLabel;
    SBWhatsApp_EntiTipCto: TSpeedButton;
    EdWhatsApp_EntiTipCto: TdmkEditCB;
    CBWhatsApp_EntiTipCto: TdmkDBLookupComboBox;
    GroupBox4: TGroupBox;
    MeWhatsApp_Msg: TMemo;
    LVItens: TListView;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SBWhatsApp_EntiTipCtoClick(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure LVItensDblClick(Sender: TObject);
  private
    { Private declarations }
    procedure MostraEdicao();
  public
    { Public declarations }
    FEmpresa: Integer;
  end;

  var
  FmNFSeParams: TFmNFSeParams;

implementation

uses UnMyObjects, UMySQLModule, Module, ModuleGeral, UnEntities, DmkDAC_PF,
  UnitDmkTags;

{$R *.DFM}

procedure TFmNFSeParams.MostraEdicao();
begin
  EdWhatsApp_EntiTipCto.ValueVariant := DmodG.QrParamsEmp.FieldByName('Nfse_WhatsApp_EntiTipCto').AsInteger;
  CBWhatsApp_EntiTipCto.KeyValue     := DmodG.QrParamsEmp.FieldByName('Nfse_WhatsApp_EntiTipCto').AsInteger;
  MeWhatsApp_Msg.Text                := DmodG.QrParamsEmp.FieldByName('Nfse_WhatsApp_Msg').AsString;
end;

procedure TFmNFSeParams.SBWhatsApp_EntiTipCtoClick(Sender: TObject);
begin
  Entities.MostraFormMostraEntiTipCto(VUWhatsApp_EntiTipCto.ValueVariant,
    EdWhatsApp_EntiTipCto, CBWhatsApp_EntiTipCto, QrEntiTipCtoWA);
end;

procedure TFmNFSeParams.BtOKClick(Sender: TObject);
var
  WhatsApp_EntiTipCto: Integer;
  WhatsApp_Msg: String;
begin
  if MyObjects.FIC(FEmpresa = 0, nil, 'Empresa n�o informada!') then Exit;
  //
  WhatsApp_EntiTipCto := VUWhatsApp_EntiTipCto.ValueVariant;
  WhatsApp_Msg        := MeWhatsApp_Msg.Text;
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'paramsemp', False,
    ['Nfse_WhatsApp_EntiTipCto', 'Nfse_WhatsApp_Msg'], ['Codigo'],
    [WhatsApp_EntiTipCto, WhatsApp_Msg], [FEmpresa], True) then
  begin
    Close;
  end;
end;

procedure TFmNFSeParams.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmNFSeParams.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmNFSeParams.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  FEmpresa        := 0;
  //
  UMyMod.AbreQuery(QrEntiTipCtoWA, DMod.MyDB);
  //
  MostraEdicao();
  //
  UnDmkTags.CarregaWhatsAppNFSeTags(LVItens);
end;

procedure TFmNFSeParams.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
    [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmNFSeParams.LVItensDblClick(Sender: TObject);
begin
  UnDmkTags.AdicionaTag(LVItens, MeWhatsApp_Msg);
end;

end.
