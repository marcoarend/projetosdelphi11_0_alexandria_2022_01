unit NFSe_NFSPesq;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, dmkLabel, dmkEdit, dmkEditCB,
  DBCtrls, dmkDBLookupComboBox, DB, mySQLDbTables, ComCtrls,
  dmkEditDateTimePicker, Grids, DBGrids, dmkDBGrid, Variants, Menus, dmkMemo,
  frxClass, frxDBSet, dmkGeral, frxBarcode, UnDmkProcFunc, dmkImage,
  frxExportPDF, dmkCompoStore, UnDmkEnums, dmkPermissoes;

type
  THackDBGrid = class(TdmkDBGrid);
  TFmNFSe_NFSPesq = class(TForm)
    PnGeral: TPanel;
    QrClientes: TmySQLQuery;
    QrClientesCodigo: TIntegerField;
    QrClientesNO_Enti: TWideStringField;
    DsClientes: TDataSource;
    QrClientesTipo: TSmallintField;
    QrClientesCNPJ: TWideStringField;
    QrClientesCPF: TWideStringField;
    PMImprime: TPopupMenu;
    QrNFSeNfsCab: TmySQLQuery;
    DsNFSeNfsCab: TDataSource;
    QrNFeXMLI: TmySQLQuery;
    QrNFeXMLICodigo: TWideStringField;
    QrNFeXMLIID: TWideStringField;
    QrNFeXMLIValor: TWideStringField;
    QrNFeXMLIPai: TWideStringField;
    QrNFeXMLIDescricao: TWideStringField;
    QrNFeXMLICampo: TWideStringField;
    frxDsNFeXMLI: TfrxDBDataset;
    frxCampos: TfrxReport;
    QrArq: TmySQLQuery;
    QrArqFatID: TIntegerField;
    QrArqFatNum: TIntegerField;
    QrArqEmpresa: TIntegerField;
    QrArqIDCtrl: TIntegerField;
    QrArqXML_NFe: TWideMemoField;
    QrArqXML_Aut: TWideMemoField;
    QrArqXML_Can: TWideMemoField;
    QrArqLk: TIntegerField;
    QrArqDataCad: TDateField;
    QrArqDataAlt: TDateField;
    QrArqUserCad: TIntegerField;
    QrArqUserAlt: TIntegerField;
    QrArqAlterWeb: TSmallintField;
    QrArqAtivo: TSmallintField;
    PMArq: TPopupMenu;
    NFe1: TMenuItem;
    Autorizao1: TMenuItem;
    Cancelamento2: TMenuItem;
    N1: TMenuItem;
    DiretriodoarquivoXML1: TMenuItem;
    frxListaNFes: TfrxReport;
    QrNFe_100: TmySQLQuery;
    frxDsNFe_100: TfrxDBDataset;
    QrNFe_XXX: TmySQLQuery;
    frxDsNFe_XXX: TfrxDBDataset;
    QrNFe_XXXOrdem: TIntegerField;
    QrNFe_XXXcStat: TIntegerField;
    QrNFe_XXXide_nNF: TIntegerField;
    QrNFe_XXXide_serie: TIntegerField;
    QrNFe_XXXide_AAMM_AA: TWideStringField;
    QrNFe_XXXide_AAMM_MM: TWideStringField;
    QrNFe_XXXide_dEmi: TDateField;
    QrNFe_XXXNOME_tpEmis: TWideStringField;
    QrNFe_XXXNOME_tpNF: TWideStringField;
    QrNFe_XXXStatus: TIntegerField;
    QrNFe_XXXMotivo: TWideStringField;
    QrNFe_100ide_nNF: TIntegerField;
    QrNFe_100ide_serie: TIntegerField;
    QrNFe_100ide_dEmi: TDateField;
    QrNFe_100ICMSTot_vProd: TFloatField;
    QrNFe_100ICMSTot_vST: TFloatField;
    QrNFe_100ICMSTot_vFrete: TFloatField;
    QrNFe_100ICMSTot_vSeg: TFloatField;
    QrNFe_100ICMSTot_vIPI: TFloatField;
    QrNFe_100ICMSTot_vOutro: TFloatField;
    QrNFe_100ICMSTot_vDesc: TFloatField;
    QrNFe_100ICMSTot_vNF: TFloatField;
    QrNFe_100ICMSTot_vBC: TFloatField;
    QrNFe_100ICMSTot_vICMS: TFloatField;
    QrNFe_100NOME_tpEmis: TWideStringField;
    QrNFe_100NOME_tpNF: TWideStringField;
    QrNFe_100Ordem: TIntegerField;
    QrNFe_XXXNOME_ORDEM: TWideStringField;
    QrNFe_101: TmySQLQuery;
    frxDsNFe_101: TfrxDBDataset;
    QrNFe_101Ordem: TIntegerField;
    QrNFe_101ide_nNF: TIntegerField;
    QrNFe_101ide_serie: TIntegerField;
    QrNFe_101ide_AAMM_AA: TWideStringField;
    QrNFe_101ide_AAMM_MM: TWideStringField;
    QrNFe_101ide_dEmi: TDateField;
    QrNFe_101NOME_tpEmis: TWideStringField;
    QrNFe_101NOME_tpNF: TWideStringField;
    QrNFe_101infCanc_dhRecbto: TDateTimeField;
    QrNFe_101infCanc_nProt: TWideStringField;
    QrNFe_101Motivo: TWideStringField;
    QrNFe_101Id: TWideStringField;
    QrNFe_101Id_TXT: TWideStringField;
    QrNFe_100ICMSTot_vPIS: TFloatField;
    QrNFe_100ICMSTot_vCOFINS: TFloatField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel1: TPanel;
    BtImprime: TBitBtn;
    BtEnvia: TBitBtn;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    Panel5: TPanel;
    Panel6: TPanel;
    Panel7: TPanel;
    dmkLabel1: TdmkLabel;
    Label14: TLabel;
    Label3: TLabel;
    EdFilial: TdmkEditCB;
    CBFilial: TdmkDBLookupComboBox;
    EdCliente: TdmkEditCB;
    CBCliente: TdmkDBLookupComboBox;
    EdIntermediario: TdmkEditCB;
    CBIntermediario: TdmkDBLookupComboBox;
    GroupBox1: TGroupBox;
    Label2: TLabel;
    Label1: TLabel;
    TPDataF: TdmkEditDateTimePicker;
    TPDataI: TdmkEditDateTimePicker;
    GroupBox2: TGroupBox;
    Label4: TLabel;
    Label5: TLabel;
    dmkEditDateTimePicker1: TdmkEditDateTimePicker;
    dmkEditDateTimePicker2: TdmkEditDateTimePicker;
    Panel8: TPanel;
    BtReabre: TBitBtn;
    Ck100e101: TCheckBox;
    RGOrdem2: TRadioGroup;
    RGAmbiente: TRadioGroup;
    RGQuemEmit: TRadioGroup;
    RGOrdem1: TRadioGroup;
    PnDados: TPanel;
    dmkDBGrid1: TdmkDBGrid;
    Splitter1: TSplitter;
    DBGrid1: TDBGrid;
    QrNFSeNfsCabEmpresa: TIntegerField;
    QrNFSeNfsCabAmbiente: TSmallintField;
    QrNFSeNfsCabNfsNumero: TLargeintField;
    QrNFSeNfsCabNfsRpsIDSerie: TWideStringField;
    QrNFSeNfsCabNfsRpsIDTipo: TSmallintField;
    QrNFSeNfsCabNfsRpsIDNumero: TIntegerField;
    QrNFSeNfsCabNfsVersao: TFloatField;
    QrNFSeNfsCabNfsCodigoVerificacao: TWideStringField;
    QrNFSeNfsCabNfsDataEmissao: TDateTimeField;
    QrNFSeNfsCabNfsNfseSubstituida: TLargeintField;
    QrNFSeNfsCabNfsOutrasInformacoes: TWideStringField;
    QrNFSeNfsCabNfsBaseCalculo: TFloatField;
    QrNFSeNfsCabNfsAliquota: TFloatField;
    QrNFSeNfsCabNfsValorIss: TFloatField;
    QrNFSeNfsCabNfsValorLiquidoNfse: TFloatField;
    QrNFSeNfsCabNfsValorCredito: TFloatField;
    QrNFSeNfsCabNfsPrestaCnpj: TWideStringField;
    QrNFSeNfsCabNfsPrestaCpf: TWideStringField;
    QrNFSeNfsCabNfsPrestaInscricaoMunicipal: TWideStringField;
    QrNFSeNfsCabNfsPrestaRazaoSocial: TWideStringField;
    QrNFSeNfsCabNfsPrestaNomeFantasia: TWideStringField;
    QrNFSeNfsCabNfsPrestaEndereco: TWideStringField;
    QrNFSeNfsCabNfsPrestaNumero: TWideStringField;
    QrNFSeNfsCabNfsPrestaComplemento: TWideStringField;
    QrNFSeNfsCabNfsPrestaBairro: TWideStringField;
    QrNFSeNfsCabNfsPrestaCodigoMunicipio: TIntegerField;
    QrNFSeNfsCabNfsPrestaUf: TWideStringField;
    QrNFSeNfsCabNfsPrestaCodigoPais: TWideStringField;
    QrNFSeNfsCabNfsPrestaCep: TWideStringField;
    QrNFSeNfsCabNfsPrestaContatoTelefone: TWideStringField;
    QrNFSeNfsCabNfsPrestaContatoEmail: TWideStringField;
    QrNFSeNfsCabNfsOrgaoGeradorCodigoMunicipio: TIntegerField;
    QrNFSeNfsCabNfsOrgaoGeradorUf: TWideStringField;
    QrNFSeNfsCabDpsRpsIDNumero: TIntegerField;
    QrNFSeNfsCabDpsRpsIDSerie: TWideStringField;
    QrNFSeNfsCabDpsRpsIDTipo: TSmallintField;
    QrNFSeNfsCabDpsRpsDataEmissao: TDateTimeField;
    QrNFSeNfsCabDpsRpsStatus: TIntegerField;
    QrNFSeNfsCabDpsSubstNumero: TIntegerField;
    QrNFSeNfsCabDpsSubstSerie: TWideStringField;
    QrNFSeNfsCabDpsSubstTipo: TSmallintField;
    QrNFSeNfsCabDpsCompetencia: TDateField;
    QrNFSeNfsCabDpsValorServicos: TFloatField;
    QrNFSeNfsCabDpsValorDeducoes: TFloatField;
    QrNFSeNfsCabDpsValorPis: TFloatField;
    QrNFSeNfsCabDpsValorCofins: TFloatField;
    QrNFSeNfsCabDpsValorInss: TFloatField;
    QrNFSeNfsCabDpsValorIr: TFloatField;
    QrNFSeNfsCabDpsValorCsll: TFloatField;
    QrNFSeNfsCabDpsOutrasRetencoes: TFloatField;
    QrNFSeNfsCabDpsValorIss: TFloatField;
    QrNFSeNfsCabDpsAliquota: TFloatField;
    QrNFSeNfsCabDpsDescontoIncondicionado: TFloatField;
    QrNFSeNfsCabDpsDescontoCondicionado: TFloatField;
    QrNFSeNfsCabDpsIssRetido: TSmallintField;
    QrNFSeNfsCabDpsResponsavelRetencao: TSmallintField;
    QrNFSeNfsCabDpsItemListaServico: TWideStringField;
    QrNFSeNfsCabDpsCodigoCnae: TWideStringField;
    QrNFSeNfsCabDpsCodigoTributacaoMunicipio: TWideStringField;
    QrNFSeNfsCabDpsDiscriminacao: TWideMemoField;
    QrNFSeNfsCabDpsCodigoMunicipio: TIntegerField;
    QrNFSeNfsCabDpsCodigoPais: TIntegerField;
    QrNFSeNfsCabDpsExigibilidadeIss: TSmallintField;
    QrNFSeNfsCabDpsNaturezaOperacao: TIntegerField;
    QrNFSeNfsCabDpsMunicipioIncidencia: TIntegerField;
    QrNFSeNfsCabDpsNumeroProcesso: TWideStringField;
    QrNFSeNfsCabDpsTomaCpf: TWideStringField;
    QrNFSeNfsCabDpsTomaCnpj: TWideStringField;
    QrNFSeNfsCabDpsTomaInscricaoMunicipal: TWideStringField;
    QrNFSeNfsCabDpsPrestaCpf: TWideStringField;
    QrNFSeNfsCabDpsPrestaCnpj: TWideStringField;
    QrNFSeNfsCabDpsPrestaInscricaoMunicipal: TWideStringField;
    QrNFSeNfsCabDpsTomaRazaoSocial: TWideStringField;
    QrNFSeNfsCabDpsTomaNomeFantasia: TWideStringField;
    QrNFSeNfsCabDpsTomaEndereco: TWideStringField;
    QrNFSeNfsCabDpsTomaNumero: TWideStringField;
    QrNFSeNfsCabDpsTomaComplemento: TWideStringField;
    QrNFSeNfsCabDpsTomaBairro: TWideStringField;
    QrNFSeNfsCabDpsTomaCodigoMunicipio: TIntegerField;
    QrNFSeNfsCabDpsTomaUf: TWideStringField;
    QrNFSeNfsCabDpsTomaCodigoPais: TIntegerField;
    QrNFSeNfsCabDpsTomaCep: TIntegerField;
    QrNFSeNfsCabDpsTomaContatoTelefone: TWideStringField;
    QrNFSeNfsCabDpsTomaContatoEmail: TWideStringField;
    QrNFSeNfsCabDpsIntermeCpf: TWideStringField;
    QrNFSeNfsCabDpsIntermeCnpj: TWideStringField;
    QrNFSeNfsCabDpsIntermeInscricaoMunicipal: TWideStringField;
    QrNFSeNfsCabDpsIntermeRazaoSocial: TWideStringField;
    QrNFSeNfsCabDpsConstrucaoCivilCodigoObra: TWideStringField;
    QrNFSeNfsCabDpsConstrucaoCivilArt: TWideStringField;
    QrNFSeNfsCabDpsRegimeEspecialTributacao: TSmallintField;
    QrNFSeNfsCabDpsOptanteSimplesNacional: TSmallintField;
    QrNFSeNfsCabDpsIncentivoFiscal: TSmallintField;
    QrNFSeNfsCabDpsInfID_ID: TWideStringField;
    QrNFSeNfsCabCanCodigoCancelamento: TSmallintField;
    QrNFSeNfsCabCanCanc_DataHora: TDateTimeField;
    QrNFSeNfsCabCanCanc_ID: TWideStringField;
    QrNFSeNfsCabCanCanc_Versao: TFloatField;
    QrNFSeNfsCabSubNfseSubstituidora: TLargeintField;
    QrNFSeNfsCabSubSubst_ID: TWideStringField;
    QrNFSeNfsCabSubSubst_Versao: TFloatField;
    QrNFSeNfsCabLk: TIntegerField;
    QrNFSeNfsCabDataCad: TDateField;
    QrNFSeNfsCabDataAlt: TDateField;
    QrNFSeNfsCabUserCad: TIntegerField;
    QrNFSeNfsCabUserAlt: TIntegerField;
    QrNFSeNfsCabAlterWeb: TSmallintField;
    QrNFSeNfsCabAtivo: TSmallintField;
    NFSe1: TMenuItem;
    QrNFSeNfsCabSTATUS: TFloatField;
    QrNFSeNfsCabCanNumero: TLargeintField;
    QrNFSeNfsCabCanCpf: TWideStringField;
    QrNFSeNfsCabCanInscricaomunicipal: TWideStringField;
    QrNFSeNfsCabCanCodigoMunicipio: TIntegerField;
    QrIntermediario: TmySQLQuery;
    DsIntermediario: TDataSource;
    QrIntermediarioCodigo: TIntegerField;
    QrIntermediarioNO_Enti: TWideStringField;
    QrIntermediarioCNPJ: TWideStringField;
    QrIntermediarioCPF: TWideStringField;
    QrIntermediarioTipo: TSmallintField;
    QrNFSeNfsCabNOMEMUNIINCI: TWideStringField;
    N2: TMenuItem;
    NFSeselecionada1: TMenuItem;
    CSTabSheetChamou: TdmkCompoStore;
    CkTeste: TCheckBox;
    dmkPermissoes1: TdmkPermissoes;
    BtSoma: TBitBtn;
    QrNFSeNfsCabDpsTomaCnpjCpf: TWideStringField;
    QrNFSeNfsCabDpsTomaCnpjCpf_TXT: TWideStringField;
    QrNFSeNfsCabCliente: TIntegerField;
    PMEnvia: TPopupMenu;
    EnviarNFSe1: TMenuItem;
    N3: TMenuItem;
    EnviarNFSeporemailEnvioprotocolado1: TMenuItem;
    QrNFSeNfsCabCodigo: TIntegerField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure EdFilialExit(Sender: TObject);
    procedure EdFilialChange(Sender: TObject);
    procedure EdClienteChange(Sender: TObject);
    procedure EdClienteExit(Sender: TObject);
    procedure TPDataIChange(Sender: TObject);
    procedure TPDataIClick(Sender: TObject);
    procedure dmkDBGrid1DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure SbImprimeClick(Sender: TObject);
    procedure QrNFSeNfsCabAfterScroll(DataSet: TDataSet);
    procedure QrNFSeNfsCabBeforeClose(DataSet: TDataSet);
    procedure PMArqPopup(Sender: TObject);
    procedure BtReabreClick(Sender: TObject);
    procedure CkEmitClick(Sender: TObject);
    procedure RGAmbienteClick(Sender: TObject);
    procedure RGOrdem1Click(Sender: TObject);
    procedure RGOrdem2Click(Sender: TObject);
    procedure QrNFe_XXXCalcFields(DataSet: TDataSet);
    procedure frxListaNFesGetValue(const VarName: string; var Value: Variant);
    procedure NFSe1Click(Sender: TObject);
    procedure BtImprimeClick(Sender: TObject);
    procedure QrIntermediarioBeforeClose(DataSet: TDataSet);
    procedure FormShow(Sender: TObject);
    procedure NFSeselecionada1Click(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure BtSomaClick(Sender: TObject);
    procedure QrNFSeNfsCabCalcFields(DataSet: TDataSet);
    procedure dmkDBGrid1DblClick(Sender: TObject);
    procedure PMEnviaPopup(Sender: TObject);
    procedure EnviarNFSe1Click(Sender: TObject);
    procedure BtEnviaClick(Sender: TObject);
    procedure EnviarNFSeporemailEnvioprotocolado1Click(Sender: TObject);
  private
    { Private declarations }
    //FLote, FEmpresa: Integer;
    // Impress�o de lista de NFes
    //F_NFe_100, F_NFe_101, F_NFe_XXX: String;
    //
    procedure ReopenNFSeNfsCab(Numero: Integer; So_O_ID: Boolean);
    procedure FechaNFSeNfsCab();
  public
    { Public declarations }
  end;

  var
  FmNFSe_NFSPesq: TFmNFSe_NFSPesq;

implementation

uses UnMyObjects, ModuleGeral, UnInternalConsts, MyDBCheck, UCreate,
UMySQLModule, Module, DmkDAC_PF, NFSe_0000_Module, UnMailEnv,
MyGlyfs, Principal, NFSe_PF_0000, UnGrl_Geral;

{$R *.DFM}

procedure TFmNFSe_NFSPesq.BtReabreClick(Sender: TObject);
var
  Numero: Integer;
begin
  Numero := 0;
  //
  if (QrNFSeNfsCab.State <> dsInactive) then
    Numero := QrNFSeNfsCabNfsNumero.Value;
  //
  ReopenNFSeNfsCab(Numero, False);
end;

procedure TFmNFSe_NFSPesq.BtEnviaClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMEnvia, BtEnvia);
end;

procedure TFmNFSe_NFSPesq.BtImprimeClick(Sender: TObject);
var
  Num: TArrInt;
begin
  SetLength(Num, 1);
  Num[0] := QrNFSeNfsCabNfsNumero.Value;
  DmNFSe_0000.ImpressaoNFSe_PorNFS(QrNFSeNfsCabEmpresa.Value,
    QrNFSeNfsCabAmbiente.Value, Num);
end;

procedure TFmNFSe_NFSPesq.BtSaidaClick(Sender: TObject);
begin
  if TFmNFSe_NFSPesq(Self).Owner is TApplication then
    Close
  else
    MyObjects.FormTDIFecha(TFmNFSe_NFSPesq(Self), TTabSheet(CSTabSheetChamou.Component));
end;

procedure TFmNFSe_NFSPesq.BtSomaClick(Sender: TObject);
var
  BC: Double;
  i: Integer;
begin
  if (QrNFSeNfsCab.State <> dsInactive) and (QrNFSeNfsCab.RecordCount > 0) then
  begin
    BC := 0;
    //
    with dmkDBGrid1.DataSource.DataSet do
    begin
      for i:= 0 to dmkDBGrid1.SelectedRows.Count - 1 do
      begin
        GotoBookmark(dmkDBGrid1.SelectedRows.Items[i]);
        //
        BC := BC + QrNFSeNfsCabNfsBaseCalculo.Value;
      end;
    end;
    Geral.MB_Aviso('Resultado:' + sLineBreak +
      'Total de itens selecionados: ' + Geral.FF0(dmkDBGrid1.SelectedRows.Count) + sLineBreak +
      'Soma da Base de C�lcul: ' + Geral.TFT(FloatToStr(BC), 2, siNegativo));
  end;
end;

procedure TFmNFSe_NFSPesq.CkEmitClick(Sender: TObject);
begin
  FechaNFSeNfsCab;
end;

procedure TFmNFSe_NFSPesq.dmkDBGrid1DblClick(Sender: TObject);
var
  Campo: String;
begin
  Campo := Uppercase(dmkDBGrid1.Columns[THackDBGrid(dmkDBGrid1).Col -1].FieldName);
  //
  if Campo = Uppercase('Cliente') then
  begin
    if (QrNFSeNfsCab.State <> dsInactive) and (QrNFSeNfsCab.RecordCount > 0) then
      DModG.CadastroDeEntidade(QrNFSeNfsCabCliente.Value, fmcadEntidade2, fmcadEntidade2, False);
  end;
end;

procedure TFmNFSe_NFSPesq.dmkDBGrid1DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
  DmNFSe_0000.ColoreStatusNFeEmDBGrid(TDBGrid(dmkDBGrid1), Rect, DataCol, Column,
  State, Trunc(QrNFSeNfsCabSTATUS.Value));
end;

procedure TFmNFSe_NFSPesq.EdClienteChange(Sender: TObject);
begin
  if not EdFilial.Focused then
    FechaNFSeNfsCab;
end;

procedure TFmNFSe_NFSPesq.EdClienteExit(Sender: TObject);
begin
  FechaNFSeNfsCab;
end;

procedure TFmNFSe_NFSPesq.EdFilialChange(Sender: TObject);
begin
  if not EdFilial.Focused then
    FechaNFSeNfsCab;
end;

procedure TFmNFSe_NFSPesq.EdFilialExit(Sender: TObject);
begin
  FechaNFSeNfsCab;
end;

procedure TFmNFSe_NFSPesq.EnviarNFSe1Click(Sender: TObject);
var
  DANFE, XML: String;
begin
  if QrNFSeNfsCabNfsRpsIDNumero.Value = 0 then Exit;
  //
  if QrNFSeNfsCabSTATUS.Value = 100 then //Autorizada
  begin
    // DANFE
    DANFE := DmNFSe_0000.ExportaPDFNFSe_PorNFS(QrNFSeNfsCabEmpresa.Value,
      QrNFSeNfsCabAmbiente.Value, QrNFSeNfsCabDpsRpsIDNumero.Value);
    if DANFE = '' then
    begin
      Geral.MensagemBox('Falha ao exportar DANFSE!', 'Aviso', MB_OK+MB_ICONWARNING);
      Exit;
    end;
  end;
  // XML
  {
  XML := DmNFSe_0000.ExportaXMLNFSe_PorNFS(QrNFSeNfsCabNfsPrestaCnpj.Value,
    QrNFSeNfsCabNfsPrestaCpf.Value, QrNFSeNfsCabNfsPrestaInscricaoMunicipal.Value,
    QrNFSeNfsCabDpsTomaCnpj.Value, QrNFSeNfsCabDpsTomaCpf.Value,
    QrNFSeNfsCabDpsTomaInscricaoMunicipal.Value, QrNFSeNfsCabDpsIntermeCnpj.Value,
    QrNFSeNfsCabDpsIntermeCpf.Value, QrNFSeNfsCabDpsIntermeInscricaoMunicipal.Value,
    0, TPDataI.Date, TPDataF.Date);
  }
  XML := DmNFSe_0000.ExportaXMLNFSe_PorRps(QrNFSeNfsCabNfsPrestaCnpj.Value,
    QrNFSeNfsCabNfsPrestaCpf.Value, QrNFSeNfsCabNfsPrestaInscricaoMunicipal.Value,
    QrNFSeNfsCabNfsRpsIDNumero.Value, QrNFSeNfsCabNfsRpsIDTipo.Value,
    QrNFSeNfsCabNfsRpsIDSerie.Value, False);
  if XML = '' then
  begin
    Geral.MensagemBox('Falha ao exportar XML NFS-e!', 'Aviso', MB_OK+MB_ICONWARNING);
    Exit;
  end;
  if UMailEnv.Monta_e_Envia_Mail([QrNFSeNfsCabDpsRpsIDNumero.Value,
    QrNFSeNfsCabEmpresa.Value, QrNFSeNfsCabAmbiente.Value,
    Trunc(QrNFSeNfsCabSTATUS.Value)], meNFSe, [DANFE, XML],
    [QrNFSeNfsCabDpsTomaRazaoSocial.Value,
    QrNFSeNfsCabNfsCodigoVerificacao.Value, '', '', ''], True)
  then
    Geral.MensagemBox('E-mail enviado com sucesso!', 'Aviso',
      MB_OK+MB_ICONWARNING);
end;

procedure TFmNFSe_NFSPesq.EnviarNFSeporemailEnvioprotocolado1Click(
  Sender: TObject);
{$IfNDef SemProtocolo}
{$IfDef TEM_DBWEB}
var
  Aba: Boolean;
begin
  if TFmNFSe_NFSPesq(Self).Owner is TApplication then
    Aba := False
  else
    Aba := True;

  UnNFSe_PF_0000.MostraFormNFSeMail(Aba, FmPrincipal.PageControl1,
    FmPrincipal.AdvToolBarPagerNovo, QrNFSeNfsCabEmpresa.Value, 0,
    QrNFSeNfsCabCodigo.Value, True);
{$Else}
begin
  Geral.MB_Aviso('Para utilizar o envio de NFS-es protocoloadas o m�dulo WEB deve est� habilitado!');
{$EndIf}
{$Else}
begin
  Grl_Geral.InfoSemModulo(mdlappProtocolos);
{$EndIf}
end;

procedure TFmNFSe_NFSPesq.FormActivate(Sender: TObject);
begin
  if TFmNFSe_NFSPesq(Self).Owner is TApplication then
  begin
    MyObjects.CorIniComponente();
  end;
end;

procedure TFmNFSe_NFSPesq.FormCreate(Sender: TObject);
begin
  Screen.Cursor := crHourGlass;
  ImgTipo.SQLType := stPsq;
  CBFilial.ListSource := DModG.DsEmpresas;
  DModG.ReopenEmpresas(VAR_USUARIO, 0, EdFilial, CBFilial);
  QrClientes.Open;
  QrIntermediario.Open;
  TPDataI.Date := Date - 10; // m�x permitido pelo fisco
  TPDataF.Date := Date;
  //
  ReopenNFSeNfsCab(0, False);
  //
  CBFilial.ListSource := DModG.DsEmpresas;
  Screen.Cursor := crDefault;
end;

procedure TFmNFSe_NFSPesq.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmNFSe_NFSPesq.FormShow(Sender: TObject);
begin
{$IfNDef cSkinRank} //Berlin
{$IfNDef cAlphaSkin} //Berlin
  FmMyGlyfs.DefineGlyfs(TForm(Sender));
{$EndIf}
{$EndIf}
{$IfDef cSkinRank} //Berlin
  if FmPrincipal.Sd1.Active then
    FmMyGlyfs.DefineGlyfsTDI(FmPrincipal.sd1, Sender);
{$EndIf}
{$IfDef cAlphaSkin} //Berlin
  if FmPrincipal.sSkinManager1.Active then
    FmMyGlyfs.DefineGlyfsTDI2(FmPrincipal.sSkinManager1, Sender);
{$EndIf}
end;

procedure TFmNFSe_NFSPesq.frxListaNFesGetValue(const VarName: string;
  var Value: Variant);
begin
  if VarName = 'VARF_EMPRESA' then
    Value := CBFilial.Text
  else
  if VarName = 'VARF_TERCEIRO' then
    Value := dmkPF.ParValueCodTxt('', CBCliente.Text, EdCliente.ValueVariant)
  else
  if VarName = 'VARF_PERIODO' then
    Value := dmkPF.PeriodoImp2(
    TPDataI.Date, TPDataF.Date, True, True, '', '', '')
  else
  if VarName = 'VARF_STATUS' then
  begin
    if Ck100e101.Checked then
      Value := 'AUTORIZADAS E CANCELADAS'
    else
      Value := 'QUALQUER';
  end
  else
  if VarName = 'VARF_APENAS_EMP' then
  begin
    case RGQuemEmit.ItemIndex of
      0: Value := 'Notas Fiscais Emitidas';
      1: Value := 'Notas Fiscais Recebidas';
      2: Value := 'Notas Fiscais Emitidas e Recebidas';
      else Value := 'Notas Fiscais ????';
    end;
  end
  else
  if VarName = 'VARF_AMBIENTE' then
  begin
    case RGAmbiente.ItemIndex of
      0: Value := 'AMBOS';
      1: Value := 'PRODU��O';
      2: Value := 'HOMOLOGA��O';
    end;
  end
end;

procedure TFmNFSe_NFSPesq.NFSe1Click(Sender: TObject);
var
  Num: TArrInt;
begin
  SetLength(Num, QrNFSeNfsCab.RecordCount);
  QrNFSeNfsCab.DisableControls;
  try
    QrNFSeNfsCab.First;
    while not QrNFSeNfsCab.Eof do
    begin
      Num[QrNFSeNfsCab.RecNo -1] := QrNFSeNfsCabNfsNumero.Value;
      //
      QrNFSeNfsCab.Next;
    end;
    DmNFSe_0000.ImpressaoNFSe_PorNFS(QrNFSeNfsCabEmpresa.Value,
      QrNFSeNfsCabAmbiente.Value, Num);
  finally
    QrNFSeNfsCab.EnableControls;
  end;
end;

procedure TFmNFSe_NFSPesq.NFSeselecionada1Click(Sender: TObject);
var
  Num: TArrInt;
begin
  SetLength(Num, 1);
  Num[0] := QrNFSeNfsCabNfsNumero.Value;
  DmNFSe_0000.ImpressaoNFSe_PorNFS(QrNFSeNfsCabEmpresa.Value,
    QrNFSeNfsCabAmbiente.Value, Num);
end;

procedure TFmNFSe_NFSPesq.PMArqPopup(Sender: TObject);
begin
  NFe1         .Enabled := QrArqXML_NFe.Value <> '';
  Autorizao1   .Enabled := QrArqXML_Aut.Value <> '';
  Cancelamento2.Enabled := QrArqXML_Can.Value <> '';
end;

procedure TFmNFSe_NFSPesq.PMEnviaPopup(Sender: TObject);
var
  Enab: Boolean;
begin
  Enab := (QrNFSeNfsCab.State <> dsInactive) and (QrNFSeNfsCab.RecordCount > 0);
  //
  EnviarNFSe1.Enabled                         := Enab;
  EnviarNFSeporemailEnvioprotocolado1.Enabled := Enab;
end;

procedure TFmNFSe_NFSPesq.QrNFSeNfsCabAfterScroll(DataSet: TDataSet);
var
  Enab: Boolean;
begin
{
  BtLeArq.Enabled     := True;
  BtImprime.Enabled   := False;
  BtEnvia.Enabled     := False;
  BtEvento.Enabled    := False;
  BtCancela.Enabled   := False;
  BtInutiliza.Enabled := False;
  BtExclui.Enabled := False;
  case Trunc(QrNFSeNfsCabcStat.Value) of
    0..99,201..999:
    begin
      BtExclui.Enabled := True;
    end;
    100:
    begin
      BtImprime.Enabled := True;
      BtEvento.Enabled  := True;
      BtCancela.Enabled := True;
      BtEnvia.Enabled   := True;
    end;
    101:
    begin
      BtEnvia.Enabled   := True;
    end;
    //100: BtImprime.Enabled := True;
  end;
  ReopenNFSeNfsCabMsg();
}
  Enab := QrNFSeNfsCabDpsPrestaCnpj.Value = DModG.QrEmpresasCNPJ_CPF.Value;
  //
  PnDados.Visible   := True;
  SbImprime.Enabled := (QrNFSeNfsCab.RecordCount > 0) and Enab;
  BtImprime.Enabled := (QrNFSeNfsCab.RecordCount > 0) and Enab;
  BtEnvia.Enabled   := (QrNFSeNfsCab.RecordCount > 0) and Enab;
  SbNovo.Enabled    := QrNFSeNfsCab.RecordCount > 0;
end;

procedure TFmNFSe_NFSPesq.QrNFSeNfsCabBeforeClose(DataSet: TDataSet);
begin
  PnDados.Visible    := False;
  BtImprime.Enabled  := False;
  SbImprime.Enabled  := False;
  BtEnvia.Enabled    := False;
  SbNovo.Enabled     := False;
end;

procedure TFmNFSe_NFSPesq.QrNFSeNfsCabCalcFields(DataSet: TDataSet);
var
  Doc: String;
begin
  Doc := Geral.FormataCNPJ_TT(QrNFSeNfsCabDpsTomaCnpjCpf.Value);
  //
  QrNFSeNfsCabDpsTomaCnpjCpf_TXT.Value := Doc;
end;

procedure TFmNFSe_NFSPesq.QrIntermediarioBeforeClose(DataSet: TDataSet);
begin
  BtImprime.Enabled   := False;
  BtEnvia.Enabled     := False;
end;

procedure TFmNFSe_NFSPesq.QrNFe_XXXCalcFields(DataSet: TDataSet);
begin
  case QrNFe_XXXOrdem.Value of
    0: QrNFe_XXXNOME_ORDEM.Value := 'CANCELADA';
    1: QrNFe_XXXNOME_ORDEM.Value := 'REJEITADA';
    2: QrNFe_XXXNOME_ORDEM.Value := 'N�O ENVIADA';
    else QrNFe_XXXNOME_ORDEM.Value := '? ? ? ? ?';
  end;
end;

procedure TFmNFSe_NFSPesq.FechaNFSeNfsCab();
begin
  QrNFSeNfsCab.Close;
end;

procedure TFmNFSe_NFSPesq.RGOrdem1Click(Sender: TObject);
begin
  FechaNFSeNfsCab;
end;

procedure TFmNFSe_NFSPesq.RGOrdem2Click(Sender: TObject);
begin
  FechaNFSeNfsCab;
end;

procedure TFmNFSe_NFSPesq.ReopenNFSeNfsCab(Numero: Integer; So_O_ID: Boolean);
var
  //DOC1,
  Empresa, Ord2: String;
  SQL_GERAL, SQL_WHERE, SQL_EMPRESA, SQL_AMBIENTE, SQL_CLIENTE, SQL_INTERMED,
  SQL_QUEMEMIT, SQL_ORDEM: String;
begin

  Screen.Cursor := crHourGlass;
  if (EdFilial.ValueVariant <> Null) and (EdFilial.ValueVariant <> 0) then
    Empresa := FormatFloat('0', DModG.QrEmpresasCodigo.Value)
  else Empresa := '';
  QrNFSeNfsCab.Close;
  if Empresa <> '' then
  begin
    SQL_GERAL := Geral.ATS([
    'SELECT IF(nfs.CanCodigoCancelamento > 0, 101, 100) + 0.000 STATUS, ',
    'IF(nfs.DpsTomaCnpj <> "", nfs.DpsTomaCnpj, nfs.DpsTomaCpf) DpsTomaCnpjCpf, ',
    'mun.Nome NOMEMUNIINCI, dps.Cliente, dps.Codigo, nfs.* ',
    'FROM nfsenfscab nfs ',
    'LEFT JOIN nfsedpscab dps ON ',
    '( ',
    'dps.Empresa = nfs.Empresa AND ',
    'dps.RpsIDNumero = nfs.DpsRpsIdNumero AND ',
    'dps.RpsIDSerie = nfs.DpsRpsIdSerie AND ',
    'dps.RpsIDTipo = nfs.DpsRpsIdTipo AND ',
    'dps.Ambiente = nfs.Ambiente ',
    ') ',
    'LEFT JOIN ' + VAR_AllID_DB_NOME + '.dtb_munici mun ON mun.Codigo=nfs.DpsMunicipioIncidencia ',
    '']);
    if So_O_ID then
    begin
      SQL_WHERE := 'WHERE nfs.NfsNumero=' + Geral.FF0(Numero);
    end else begin
      SQL_WHERE := dmkPF.SQL_Periodo('WHERE nfs.NfsDataEmissao ',
        TPDataI.Date, TPDataF.Date, True, True);
      SQL_EMPRESA := 'AND nfs.Empresa = ' + Empresa;
      case RGAmbiente.ItemIndex of
        0: SQL_AMBIENTE := ''; // Tudo
        1: SQL_AMBIENTE := 'AND nfs.Ambiente=1';
        2: SQL_AMBIENTE := 'AND nfs.Ambiente=2';
      end;
      {
      if EdCliente.ValueVariant <> 0 then
        SQL_CLIENTE := 'AND nfs.Cliente=' + Geral.FF0(EdCliente.ValueVariant);
      }
      if EdCliente.ValueVariant > 0 then
      begin
        if QrClientesTipo.Value = 0 then
          SQL_CLIENTE := 'AND nfs.DpsTomaCnpj=' + Geral.TFD(QrClientesCNPJ.Value, 14, siPositivo)
        else
          SQL_CLIENTE := 'AND nfs.DpsTomaCpf=' + Geral.TFD(QrClientesCPF.Value, 11, siPositivo);
      end;
      if EdIntermediario.ValueVariant <> 0 then
      begin
        if QrIntermediarioTipo.Value = 0 then
          SQL_INTERMED := 'AND nfs.DpsIntermeCnpj=' + Geral.TFD(QrIntermediarioCNPJ.Value, 14, siPositivo)
        else
          SQL_INTERMED := 'AND nfs.DpsIntermeCpf=' + Geral.TFD(QrIntermediarioCPF.Value, 11, siPositivo);
      end;
      case RGQuemEmit.ItemIndex of
        0: SQL_QUEMEMIT := 'AND nfs.DpsPrestaCnpj="' + DModG.QrEmpresasCNPJ_CPF.Value + '"';
        1: SQL_QUEMEMIT := 'AND (nfs.DpsPrestaCnpj<>"' + DModG.QrEmpresasCNPJ_CPF.Value + '" OR nfs.DpsPrestaCnpj IS NULL)';
      end;
      //
      case RGOrdem2.ItemIndex of
        0: Ord2 := '';
        1: Ord2 := 'DESC';
      end;
      case RGOrdem1.ItemIndex of
        0: SQL_ORDEM := 'ORDER BY nfs.NfsDataEmissao ' + Ord2 + ', nfs.NfsNumero ' + Ord2;
        1: SQL_ORDEM := 'ORDER BY nfs.NfsNumero ' + Ord2 + ', nfs.NfsDataEmissao ' + Ord2;
        2: SQL_ORDEM := 'ORDER BY nfs.Status ' + Ord2 + ', nfs.NfsNumero ' + Ord2;
        3: SQL_ORDEM := 'ORDER BY nfs.Status ' + Ord2 + ', nfs.NfsDataEmissao ' + Ord2;
      end;
      //
      UnDmkDAC_PF.AbreMySQLQuery0(QrNFSeNfsCab, Dmod.MyDB, [SQL_GERAL,
        SQL_WHERE, SQL_EMPRESA, SQL_AMBIENTE, SQL_CLIENTE, SQL_INTERMED,
        SQL_QUEMEMIT, SQL_ORDEM, '']);
      QrNFSeNfsCab.Locate('NfsNumero', Numero, []);
    end;
  end;
  Screen.Cursor := crDefault;
end;

procedure TFmNFSe_NFSPesq.RGAmbienteClick(Sender: TObject);
begin
  FechaNFSeNfsCab;
end;

procedure TFmNFSe_NFSPesq.SbImprimeClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMImprime, SbImprime);
end;

procedure TFmNFSe_NFSPesq.SbNomeClick(Sender: TObject);
begin
  //DefParams; //<- evitar erro em form tabbed ou paneled! Colocar no in�cio da procedure!!!
end;

procedure TFmNFSe_NFSPesq.SbNumeroClick(Sender: TObject);
begin
  //DefParams; //<- evitar erro em form tabbed ou paneled! Colocar no in�cio da procedure!!!
end;

procedure TFmNFSe_NFSPesq.SbQueryClick(Sender: TObject);
begin
  //DefParams; //<- evitar erro em form tabbed ou paneled! Colocar no in�cio da procedure!!!
end;

procedure TFmNFSe_NFSPesq.TPDataIChange(Sender: TObject);
begin
  FechaNFSeNfsCab;
end;

procedure TFmNFSe_NFSPesq.TPDataIClick(Sender: TObject);
begin
  FechaNFSeNfsCab;
end;

end.
