object FmNFSe_NFSPesq: TFmNFSe_NFSPesq
  Left = 339
  Top = 185
  Caption = 'NFS-NFS-e-001 :: Gerenciamento de NFS-e'
  ClientHeight = 571
  ClientWidth = 1105
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object PnGeral: TPanel
    Left = 0
    Top = 85
    Width = 1105
    Height = 417
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    ExplicitTop = 94
    ExplicitHeight = 408
    object Panel5: TPanel
      Left = 0
      Top = 0
      Width = 1105
      Height = 161
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object Panel6: TPanel
        Left = 0
        Top = 0
        Width = 1105
        Height = 121
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 0
        object Panel7: TPanel
          Left = 0
          Top = 0
          Width = 441
          Height = 121
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alLeft
          BevelOuter = bvNone
          TabOrder = 1
          ExplicitHeight = 153
          object dmkLabel1: TdmkLabel
            Left = 10
            Top = 0
            Width = 102
            Height = 13
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Empresa (obrigat'#243'rio):'
            UpdType = utYes
            SQLType = stNil
          end
          object Label14: TLabel
            Left = 10
            Top = 41
            Width = 45
            Height = 13
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Tomador:'
          end
          object Label3: TLabel
            Left = 10
            Top = 82
            Width = 63
            Height = 13
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Intermedi'#225'rio:'
          end
          object EdFilial: TdmkEditCB
            Left = 10
            Top = 16
            Width = 56
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            OnChange = EdFilialChange
            OnExit = EdFilialExit
            DBLookupComboBox = CBFilial
            IgnoraDBLookupComboBox = False
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
          object CBFilial: TdmkDBLookupComboBox
            Left = 67
            Top = 16
            Width = 369
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            KeyField = 'Filial'
            ListField = 'NOMEFILIAL'
            ListSource = DModG.DsEmpresas
            TabOrder = 1
            dmkEditCB = EdFilial
            UpdType = utYes
            LocF7SQLMasc = '$#'
            LocF7PreDefProc = f7pNone
          end
          object EdCliente: TdmkEditCB
            Left = 10
            Top = 57
            Width = 56
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Alignment = taRightJustify
            TabOrder = 2
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'Cliente'
            UpdCampo = 'Cliente'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            OnChange = EdClienteChange
            OnExit = EdClienteExit
            DBLookupComboBox = CBCliente
            IgnoraDBLookupComboBox = False
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
          object CBCliente: TdmkDBLookupComboBox
            Left = 67
            Top = 57
            Width = 369
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            KeyField = 'Codigo'
            ListField = 'NO_Enti'
            ListSource = DsClientes
            TabOrder = 3
            dmkEditCB = EdCliente
            QryCampo = 'Cliente'
            UpdType = utNil
            LocF7SQLMasc = '$#'
            LocF7PreDefProc = f7pNone
          end
          object EdIntermediario: TdmkEditCB
            Left = 10
            Top = 98
            Width = 56
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Alignment = taRightJustify
            TabOrder = 4
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'Cliente'
            UpdCampo = 'Cliente'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            OnChange = EdClienteChange
            OnExit = EdClienteExit
            DBLookupComboBox = CBIntermediario
            IgnoraDBLookupComboBox = False
            AutoSetIfOnlyOneReg = setregOnlyManual
          end
          object CBIntermediario: TdmkDBLookupComboBox
            Left = 67
            Top = 98
            Width = 369
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            KeyField = 'Codigo'
            ListField = 'NO_Enti'
            ListSource = DsIntermediario
            TabOrder = 5
            dmkEditCB = EdIntermediario
            QryCampo = 'Cliente'
            UpdType = utNil
            LocF7SQLMasc = '$#'
            LocF7PreDefProc = f7pNone
          end
        end
        object GroupBox1: TGroupBox
          Left = 441
          Top = 0
          Width = 128
          Height = 121
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alLeft
          Caption = ' Emiss'#227'o: '
          TabOrder = 2
          ExplicitHeight = 153
          object Label2: TLabel
            Left = 10
            Top = 62
            Width = 48
            Height = 13
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Data final:'
          end
          object Label1: TLabel
            Left = 10
            Top = 21
            Width = 55
            Height = 13
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Data inicial:'
          end
          object TPDataF: TdmkEditDateTimePicker
            Left = 10
            Top = 78
            Width = 112
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Date = 40017.000000000000000000
            Time = 0.908141423620691100
            TabOrder = 1
            ReadOnly = False
            DefaultEditMask = '!99/99/99;1;_'
            AutoApplyEditMask = True
            UpdType = utYes
            DatePurpose = dmkdpNone
          end
          object TPDataI: TdmkEditDateTimePicker
            Left = 10
            Top = 36
            Width = 112
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Date = 40017.000000000000000000
            Time = 0.908086238428950300
            TabOrder = 0
            OnClick = TPDataIClick
            OnChange = TPDataIChange
            ReadOnly = False
            DefaultEditMask = '!99/99/99;1;_'
            AutoApplyEditMask = True
            UpdType = utYes
            DatePurpose = dmkdpNone
          end
        end
        object GroupBox2: TGroupBox
          Left = 569
          Top = 0
          Width = 128
          Height = 121
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alLeft
          Caption = ' Compet'#234'ncia: '
          TabOrder = 3
          ExplicitHeight = 153
          object Label4: TLabel
            Left = 10
            Top = 62
            Width = 48
            Height = 13
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Data final:'
          end
          object Label5: TLabel
            Left = 10
            Top = 21
            Width = 55
            Height = 13
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Data inicial:'
          end
          object dmkEditDateTimePicker1: TdmkEditDateTimePicker
            Left = 10
            Top = 78
            Width = 112
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Date = 40017.000000000000000000
            Time = 0.908141423620691100
            TabOrder = 1
            ReadOnly = False
            DefaultEditMask = '!99/99/99;1;_'
            AutoApplyEditMask = True
            UpdType = utYes
            DatePurpose = dmkdpNone
          end
          object dmkEditDateTimePicker2: TdmkEditDateTimePicker
            Left = 10
            Top = 36
            Width = 112
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Date = 40017.000000000000000000
            Time = 0.908086238428950300
            TabOrder = 0
            OnClick = TPDataIClick
            OnChange = TPDataIChange
            ReadOnly = False
            DefaultEditMask = '!99/99/99;1;_'
            AutoApplyEditMask = True
            UpdType = utYes
            DatePurpose = dmkdpNone
          end
        end
        object Panel8: TPanel
          Left = 697
          Top = 0
          Width = 408
          Height = 121
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          ExplicitLeft = 901
          ExplicitWidth = 204
          ExplicitHeight = 153
          object BtReabre: TBitBtn
            Tag = 18
            Left = 128
            Top = 10
            Width = 90
            Height = 40
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = '&Reabre'
            NumGlyphs = 2
            TabOrder = 0
            OnClick = BtReabreClick
          end
          object Ck100e101: TCheckBox
            Left = 5
            Top = 62
            Width = 233
            Height = 20
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Somente autorizadas e canceladas.'
            TabOrder = 1
            Visible = False
            OnClick = CkEmitClick
          end
          object RGOrdem2: TRadioGroup
            Left = 5
            Top = 7
            Width = 114
            Height = 48
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = ' Sentido: '
            Columns = 2
            ItemIndex = 1
            Items.Strings = (
              'ASC'
              'DES')
            TabOrder = 2
            OnClick = RGOrdem2Click
          end
        end
      end
      object RGAmbiente: TRadioGroup
        Left = 0
        Top = 121
        Width = 175
        Height = 40
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alLeft
        Caption = ' Ambiente: '
        Columns = 3
        ItemIndex = 1
        Items.Strings = (
          'Ambos'
          'Produ.'
          'Homol.')
        TabOrder = 1
        OnClick = RGAmbienteClick
        ExplicitTop = 153
        ExplicitHeight = 50
      end
      object RGQuemEmit: TRadioGroup
        Left = 175
        Top = 121
        Width = 249
        Height = 40
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alLeft
        Caption = ' Servi'#231'os:  '
        Columns = 3
        ItemIndex = 0
        Items.Strings = (
          'Prestados'
          'Tomados '
          'Ambos')
        TabOrder = 2
        OnClick = RGAmbienteClick
        ExplicitTop = 153
        ExplicitHeight = 50
      end
      object RGOrdem1: TRadioGroup
        Left = 424
        Top = 121
        Width = 681
        Height = 40
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        Caption = ' Ordem: '
        Columns = 4
        ItemIndex = 0
        Items.Strings = (
          'Data, RPS'
          'RPS, Data'
          'Status, RPS'
          'Status, Data')
        TabOrder = 3
        OnClick = RGOrdem1Click
        ExplicitTop = 153
        ExplicitWidth = 511
        ExplicitHeight = 50
      end
    end
    object PnDados: TPanel
      Left = 0
      Top = 161
      Width = 1105
      Height = 256
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      TabOrder = 1
      Visible = False
      ExplicitTop = 203
      ExplicitHeight = 205
      object Splitter1: TSplitter
        Left = 1
        Top = 101
        Width = 1103
        Height = 7
        Cursor = crVSplit
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alBottom
        ExplicitTop = 151
        ExplicitWidth = 1238
      end
      object dmkDBGrid1: TdmkDBGrid
        Left = 1
        Top = 1
        Width = 1103
        Height = 100
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        Columns = <
          item
            Expanded = False
            FieldName = 'STATUS'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            Title.Caption = 'Status'
            Width = 36
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Ambiente'
            Title.Caption = 'Amb'
            Width = 28
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NfsNumero'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            Title.Caption = 'N'#186' NFS-e'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NfsDataEmissao'
            Title.Caption = 'Emiss'#227'o NFS-e'
            Width = 120
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NfsCodigoVerificacao'
            Title.Caption = 'C'#243'digo de verifica'#231#227'o'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NfsBaseCalculo'
            Title.Caption = '$ BC'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NfsAliquota'
            Title.Caption = '% Aliq.'
            Width = 40
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NfsValorIss'
            Title.Caption = '$ ISSQN'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DpsCompetencia'
            Title.Caption = 'Compet.'
            Width = 56
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DpsItemListaServico'
            Title.Caption = 'LC 116'
            Width = 40
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DpsRpsDataEmissao'
            Title.Caption = 'Emiss'#227'o RPS'
            Width = 56
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DpsRpsIDNumero'
            Title.Caption = 'N'#186' RPS'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DpsRpsIDTipo'
            Title.Caption = 'Tipo'
            Width = 28
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DpsRpsIDSerie'
            Title.Caption = 'S'#233'rie'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Cliente'
            Title.Caption = 'Tomador'
            Width = 65
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DpsTomaCnpjCpf_TXT'
            Title.Caption = 'Tomador CNPJ / CPF'
            Width = 130
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DpsTomaRazaoSocial'
            Title.Caption = 'Tomador do Servi'#231'o'
            Width = 200
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DpsValorPis'
            Title.Caption = '$ PIS'
            Width = 56
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DpsValorCofins'
            Title.Caption = '$ COFINS'
            Width = 56
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DpsValorInss'
            Title.Caption = '$ INSS'
            Width = 56
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DpsValorIr'
            Title.Caption = '$ IR'
            Width = 56
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DpsValorCsll'
            Title.Caption = '$ CSLL'
            Width = 56
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DpsValorDeducoes'
            Title.Caption = '$ Dedu'#231#245'es'
            Width = 56
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NOMEMUNIINCI'
            Title.Caption = 'Munic'#237'pio incid'#234'ncia'
            Width = 200
            Visible = True
          end>
        Color = clWindow
        DataSource = DsNFSeNfsCab
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        OnDrawColumnCell = dmkDBGrid1DrawColumnCell
        OnDblClick = dmkDBGrid1DblClick
        FieldsCalcToOrder.Strings = (
          'DpsTomaCnpjCpf_TXT=DpsTomaCnpjCpf')
        Columns = <
          item
            Expanded = False
            FieldName = 'STATUS'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            Title.Caption = 'Status'
            Width = 36
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Ambiente'
            Title.Caption = 'Amb'
            Width = 28
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NfsNumero'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            Title.Caption = 'N'#186' NFS-e'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NfsDataEmissao'
            Title.Caption = 'Emiss'#227'o NFS-e'
            Width = 120
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NfsCodigoVerificacao'
            Title.Caption = 'C'#243'digo de verifica'#231#227'o'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NfsBaseCalculo'
            Title.Caption = '$ BC'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NfsAliquota'
            Title.Caption = '% Aliq.'
            Width = 40
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NfsValorIss'
            Title.Caption = '$ ISSQN'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DpsCompetencia'
            Title.Caption = 'Compet.'
            Width = 56
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DpsItemListaServico'
            Title.Caption = 'LC 116'
            Width = 40
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DpsRpsDataEmissao'
            Title.Caption = 'Emiss'#227'o RPS'
            Width = 56
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DpsRpsIDNumero'
            Title.Caption = 'N'#186' RPS'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DpsRpsIDTipo'
            Title.Caption = 'Tipo'
            Width = 28
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DpsRpsIDSerie'
            Title.Caption = 'S'#233'rie'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Cliente'
            Title.Caption = 'Tomador'
            Width = 65
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DpsTomaCnpjCpf_TXT'
            Title.Caption = 'Tomador CNPJ / CPF'
            Width = 130
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DpsTomaRazaoSocial'
            Title.Caption = 'Tomador do Servi'#231'o'
            Width = 200
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DpsValorPis'
            Title.Caption = '$ PIS'
            Width = 56
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DpsValorCofins'
            Title.Caption = '$ COFINS'
            Width = 56
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DpsValorInss'
            Title.Caption = '$ INSS'
            Width = 56
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DpsValorIr'
            Title.Caption = '$ IR'
            Width = 56
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DpsValorCsll'
            Title.Caption = '$ CSLL'
            Width = 56
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DpsValorDeducoes'
            Title.Caption = '$ Dedu'#231#245'es'
            Width = 56
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NOMEMUNIINCI'
            Title.Caption = 'Munic'#237'pio incid'#234'ncia'
            Width = 200
            Visible = True
          end>
      end
      object DBGrid1: TDBGrid
        Left = 1
        Top = 108
        Width = 1103
        Height = 147
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alBottom
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1105
    Height = 51
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 1052
      Top = 0
      Width = 53
      Height = 51
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 10
        Top = 10
        Width = 32
        Height = 32
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 209
      Height = 51
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 5
        Top = 6
        Width = 40
        Height = 40
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 0
        OnClick = SbImprimeClick
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 45
        Top = 6
        Width = 40
        Height = 40
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Enabled = False
        NumGlyphs = 2
        TabOrder = 1
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 84
        Top = 6
        Width = 40
        Height = 40
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Enabled = False
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 124
        Top = 6
        Width = 40
        Height = 40
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Enabled = False
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 164
        Top = 6
        Width = 40
        Height = 40
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Enabled = False
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 209
      Top = 0
      Width = 843
      Height = 51
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      TabOrder = 2
      ExplicitLeft = 267
      ExplicitWidth = 779
      object LaTitulo1A: TLabel
        Left = 9
        Top = 11
        Width = 317
        Height = 32
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Gerenciamento de NFS-e'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -28
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 11
        Top = 14
        Width = 317
        Height = 32
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Gerenciamento de NFS-e'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -28
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 10
        Top = 12
        Width = 317
        Height = 32
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Gerenciamento de NFS-e'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -28
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 51
    Width = 1105
    Height = 34
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1101
      Height = 17
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitHeight = 26
      object LaAviso1: TLabel
        Left = 16
        Top = 2
        Width = 90
        Height = 14
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 15
        Top = 1
        Width = 90
        Height = 14
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 502
    Width = 1105
    Height = 69
    Align = alBottom
    TabOrder = 3
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 1101
      Height = 52
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtImprime: TBitBtn
        Tag = 5
        Left = 5
        Top = 4
        Width = 90
        Height = 40
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Imprime'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtImprimeClick
      end
      object BtEnvia: TBitBtn
        Tag = 244
        Left = 96
        Top = 4
        Width = 90
        Height = 40
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Envia'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtEnviaClick
      end
      object Panel2: TPanel
        Left = 908
        Top = 0
        Width = 193
        Height = 52
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 2
        object BtSaida: TBitBtn
          Tag = 13
          Left = 95
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Hint = 'Sai da janela atual'
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Sa'#237'da'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtSaidaClick
        end
        object BtSoma: TBitBtn
          Tag = 254
          Left = 3
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Hint = 'Altera banco atual'
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Soma'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSomaClick
        end
      end
      object CkTeste: TCheckBox
        Left = 271
        Top = 25
        Width = 380
        Height = 25
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'N'#227'o enviar Anexos (Apenas texto para teste de envio)'
        TabOrder = 3
      end
    end
  end
  object QrClientes: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Tipo, CNPJ, CPF,'
      'IF(Tipo=0, RazaoSocial, Nome) NO_Enti'
      'FROM Entidades'
      'WHERE Cliente1="V"'
      'OR Cliente2="V"'
      'OR Cliente3="V"'
      'OR Cliente4="V"'
      'ORDER BY NO_Enti')
    Left = 620
    Top = 12
    object QrClientesCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrClientesNO_Enti: TWideStringField
      FieldName = 'NO_Enti'
      Required = True
      Size = 100
    end
    object QrClientesTipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrClientesCNPJ: TWideStringField
      FieldName = 'CNPJ'
      Size = 18
    end
    object QrClientesCPF: TWideStringField
      FieldName = 'CPF'
      Size = 18
    end
  end
  object DsClientes: TDataSource
    DataSet = QrClientes
    Left = 620
    Top = 60
  end
  object PMImprime: TPopupMenu
    Left = 252
    Top = 80
    object NFSeselecionada1: TMenuItem
      Caption = 'NFS-e selecionada'
      OnClick = NFSeselecionada1Click
    end
    object N2: TMenuItem
      Caption = '-'
    end
    object NFSe1: TMenuItem
      Caption = 'NFS-es pesquisadas'
      OnClick = NFSe1Click
    end
  end
  object QrNFSeNfsCab: TMySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrNFSeNfsCabBeforeClose
    AfterScroll = QrNFSeNfsCabAfterScroll
    OnCalcFields = QrNFSeNfsCabCalcFields
    SQL.Strings = (
      
        'SELECT IF(nfs.CanCodigoCancelamento > 0, 101, 100) + 0.000 STATU' +
        'S, '
      'mun.Nome NOMEMUNIINCI, nfs.* '
      'FROM nfsenfscab nfs'
      
        'LEFT JOIN '#39' + VAR_AllID_DB_NOME + '#39'.dtb_munici mun ON mun.Codigo' +
        '=nfs.DpsMunicipioIncidencia')
    Left = 828
    Top = 16
    object QrNFSeNfsCabSTATUS: TFloatField
      FieldName = 'STATUS'
      Required = True
    end
    object QrNFSeNfsCabEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrNFSeNfsCabAmbiente: TSmallintField
      FieldName = 'Ambiente'
    end
    object QrNFSeNfsCabNfsNumero: TLargeintField
      FieldName = 'NfsNumero'
    end
    object QrNFSeNfsCabNfsRpsIDSerie: TWideStringField
      FieldName = 'NfsRpsIDSerie'
      Size = 5
    end
    object QrNFSeNfsCabNfsRpsIDTipo: TSmallintField
      FieldName = 'NfsRpsIDTipo'
    end
    object QrNFSeNfsCabNfsRpsIDNumero: TIntegerField
      FieldName = 'NfsRpsIDNumero'
    end
    object QrNFSeNfsCabNfsVersao: TFloatField
      FieldName = 'NfsVersao'
    end
    object QrNFSeNfsCabNfsCodigoVerificacao: TWideStringField
      FieldName = 'NfsCodigoVerificacao'
      Size = 9
    end
    object QrNFSeNfsCabNfsDataEmissao: TDateTimeField
      FieldName = 'NfsDataEmissao'
    end
    object QrNFSeNfsCabNfsNfseSubstituida: TLargeintField
      FieldName = 'NfsNfseSubstituida'
    end
    object QrNFSeNfsCabNfsOutrasInformacoes: TWideStringField
      FieldName = 'NfsOutrasInformacoes'
      Size = 255
    end
    object QrNFSeNfsCabNfsBaseCalculo: TFloatField
      FieldName = 'NfsBaseCalculo'
      DisplayFormat = '#,###,##0.00'
    end
    object QrNFSeNfsCabNfsAliquota: TFloatField
      FieldName = 'NfsAliquota'
      DisplayFormat = '#,###,##0.00'
    end
    object QrNFSeNfsCabNfsValorIss: TFloatField
      FieldName = 'NfsValorIss'
      DisplayFormat = '#,###,##0.00'
    end
    object QrNFSeNfsCabNfsValorLiquidoNfse: TFloatField
      FieldName = 'NfsValorLiquidoNfse'
    end
    object QrNFSeNfsCabNfsValorCredito: TFloatField
      FieldName = 'NfsValorCredito'
    end
    object QrNFSeNfsCabNfsPrestaCnpj: TWideStringField
      FieldName = 'NfsPrestaCnpj'
      Size = 18
    end
    object QrNFSeNfsCabNfsPrestaCpf: TWideStringField
      FieldName = 'NfsPrestaCpf'
      Size = 18
    end
    object QrNFSeNfsCabNfsPrestaInscricaoMunicipal: TWideStringField
      FieldName = 'NfsPrestaInscricaoMunicipal'
      Size = 18
    end
    object QrNFSeNfsCabNfsPrestaRazaoSocial: TWideStringField
      FieldName = 'NfsPrestaRazaoSocial'
      Size = 150
    end
    object QrNFSeNfsCabNfsPrestaNomeFantasia: TWideStringField
      FieldName = 'NfsPrestaNomeFantasia'
      Size = 60
    end
    object QrNFSeNfsCabNfsPrestaEndereco: TWideStringField
      FieldName = 'NfsPrestaEndereco'
      Size = 125
    end
    object QrNFSeNfsCabNfsPrestaNumero: TWideStringField
      FieldName = 'NfsPrestaNumero'
      Size = 10
    end
    object QrNFSeNfsCabNfsPrestaComplemento: TWideStringField
      FieldName = 'NfsPrestaComplemento'
      Size = 60
    end
    object QrNFSeNfsCabNfsPrestaBairro: TWideStringField
      FieldName = 'NfsPrestaBairro'
      Size = 60
    end
    object QrNFSeNfsCabNfsPrestaCodigoMunicipio: TIntegerField
      FieldName = 'NfsPrestaCodigoMunicipio'
    end
    object QrNFSeNfsCabNfsPrestaUf: TWideStringField
      FieldName = 'NfsPrestaUf'
      Size = 2
    end
    object QrNFSeNfsCabNfsPrestaCodigoPais: TWideStringField
      FieldName = 'NfsPrestaCodigoPais'
      Size = 4
    end
    object QrNFSeNfsCabNfsPrestaCep: TWideStringField
      FieldName = 'NfsPrestaCep'
      Size = 10
    end
    object QrNFSeNfsCabNfsPrestaContatoTelefone: TWideStringField
      FieldName = 'NfsPrestaContatoTelefone'
      Size = 30
    end
    object QrNFSeNfsCabNfsPrestaContatoEmail: TWideStringField
      FieldName = 'NfsPrestaContatoEmail'
      Size = 80
    end
    object QrNFSeNfsCabNfsOrgaoGeradorCodigoMunicipio: TIntegerField
      FieldName = 'NfsOrgaoGeradorCodigoMunicipio'
    end
    object QrNFSeNfsCabNfsOrgaoGeradorUf: TWideStringField
      FieldName = 'NfsOrgaoGeradorUf'
      Size = 2
    end
    object QrNFSeNfsCabDpsRpsIDNumero: TIntegerField
      FieldName = 'DpsRpsIDNumero'
    end
    object QrNFSeNfsCabDpsRpsIDSerie: TWideStringField
      FieldName = 'DpsRpsIDSerie'
      Size = 5
    end
    object QrNFSeNfsCabDpsRpsIDTipo: TSmallintField
      FieldName = 'DpsRpsIDTipo'
    end
    object QrNFSeNfsCabDpsRpsDataEmissao: TDateTimeField
      FieldName = 'DpsRpsDataEmissao'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrNFSeNfsCabDpsRpsStatus: TIntegerField
      FieldName = 'DpsRpsStatus'
    end
    object QrNFSeNfsCabDpsSubstNumero: TIntegerField
      FieldName = 'DpsSubstNumero'
    end
    object QrNFSeNfsCabDpsSubstSerie: TWideStringField
      FieldName = 'DpsSubstSerie'
      Size = 5
    end
    object QrNFSeNfsCabDpsSubstTipo: TSmallintField
      FieldName = 'DpsSubstTipo'
    end
    object QrNFSeNfsCabDpsCompetencia: TDateField
      FieldName = 'DpsCompetencia'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrNFSeNfsCabDpsValorServicos: TFloatField
      FieldName = 'DpsValorServicos'
      DisplayFormat = '#,###,##0.00'
    end
    object QrNFSeNfsCabDpsValorDeducoes: TFloatField
      FieldName = 'DpsValorDeducoes'
      DisplayFormat = '#,###,##0.00'
    end
    object QrNFSeNfsCabDpsValorPis: TFloatField
      FieldName = 'DpsValorPis'
      DisplayFormat = '#,###,##0.00'
    end
    object QrNFSeNfsCabDpsValorCofins: TFloatField
      FieldName = 'DpsValorCofins'
      DisplayFormat = '#,###,##0.00'
    end
    object QrNFSeNfsCabDpsValorInss: TFloatField
      FieldName = 'DpsValorInss'
      DisplayFormat = '#,###,##0.00'
    end
    object QrNFSeNfsCabDpsValorIr: TFloatField
      FieldName = 'DpsValorIr'
      DisplayFormat = '#,###,##0.00'
    end
    object QrNFSeNfsCabDpsValorCsll: TFloatField
      FieldName = 'DpsValorCsll'
      DisplayFormat = '#,###,##0.00'
    end
    object QrNFSeNfsCabDpsOutrasRetencoes: TFloatField
      FieldName = 'DpsOutrasRetencoes'
      DisplayFormat = '#,###,##0.00'
    end
    object QrNFSeNfsCabDpsValorIss: TFloatField
      FieldName = 'DpsValorIss'
      DisplayFormat = '#,###,##0.00'
    end
    object QrNFSeNfsCabDpsAliquota: TFloatField
      FieldName = 'DpsAliquota'
      DisplayFormat = '#,###,##0.00'
    end
    object QrNFSeNfsCabDpsDescontoIncondicionado: TFloatField
      FieldName = 'DpsDescontoIncondicionado'
      DisplayFormat = '#,###,##0.00'
    end
    object QrNFSeNfsCabDpsDescontoCondicionado: TFloatField
      FieldName = 'DpsDescontoCondicionado'
      DisplayFormat = '#,###,##0.00'
    end
    object QrNFSeNfsCabDpsIssRetido: TSmallintField
      FieldName = 'DpsIssRetido'
    end
    object QrNFSeNfsCabDpsResponsavelRetencao: TSmallintField
      FieldName = 'DpsResponsavelRetencao'
    end
    object QrNFSeNfsCabDpsItemListaServico: TWideStringField
      FieldName = 'DpsItemListaServico'
      Size = 5
    end
    object QrNFSeNfsCabDpsCodigoCnae: TWideStringField
      FieldName = 'DpsCodigoCnae'
    end
    object QrNFSeNfsCabDpsCodigoTributacaoMunicipio: TWideStringField
      FieldName = 'DpsCodigoTributacaoMunicipio'
    end
    object QrNFSeNfsCabDpsDiscriminacao: TWideMemoField
      FieldName = 'DpsDiscriminacao'
      Required = True
      BlobType = ftWideMemo
      Size = 4
    end
    object QrNFSeNfsCabDpsCodigoMunicipio: TIntegerField
      FieldName = 'DpsCodigoMunicipio'
    end
    object QrNFSeNfsCabDpsCodigoPais: TIntegerField
      FieldName = 'DpsCodigoPais'
    end
    object QrNFSeNfsCabDpsExigibilidadeIss: TSmallintField
      FieldName = 'DpsExigibilidadeIss'
    end
    object QrNFSeNfsCabDpsNaturezaOperacao: TIntegerField
      FieldName = 'DpsNaturezaOperacao'
    end
    object QrNFSeNfsCabDpsMunicipioIncidencia: TIntegerField
      FieldName = 'DpsMunicipioIncidencia'
    end
    object QrNFSeNfsCabDpsNumeroProcesso: TWideStringField
      FieldName = 'DpsNumeroProcesso'
      Size = 30
    end
    object QrNFSeNfsCabDpsTomaCpf: TWideStringField
      FieldName = 'DpsTomaCpf'
      Size = 18
    end
    object QrNFSeNfsCabDpsTomaCnpj: TWideStringField
      FieldName = 'DpsTomaCnpj'
      Size = 18
    end
    object QrNFSeNfsCabDpsTomaInscricaoMunicipal: TWideStringField
      FieldName = 'DpsTomaInscricaoMunicipal'
      Size = 18
    end
    object QrNFSeNfsCabDpsPrestaCpf: TWideStringField
      FieldName = 'DpsPrestaCpf'
      Size = 18
    end
    object QrNFSeNfsCabDpsPrestaCnpj: TWideStringField
      FieldName = 'DpsPrestaCnpj'
      Size = 18
    end
    object QrNFSeNfsCabDpsPrestaInscricaoMunicipal: TWideStringField
      FieldName = 'DpsPrestaInscricaoMunicipal'
      Size = 18
    end
    object QrNFSeNfsCabDpsTomaRazaoSocial: TWideStringField
      FieldName = 'DpsTomaRazaoSocial'
      Size = 150
    end
    object QrNFSeNfsCabDpsTomaNomeFantasia: TWideStringField
      FieldName = 'DpsTomaNomeFantasia'
      Size = 60
    end
    object QrNFSeNfsCabDpsTomaEndereco: TWideStringField
      FieldName = 'DpsTomaEndereco'
      Size = 125
    end
    object QrNFSeNfsCabDpsTomaNumero: TWideStringField
      FieldName = 'DpsTomaNumero'
      Size = 10
    end
    object QrNFSeNfsCabDpsTomaComplemento: TWideStringField
      FieldName = 'DpsTomaComplemento'
      Size = 60
    end
    object QrNFSeNfsCabDpsTomaBairro: TWideStringField
      FieldName = 'DpsTomaBairro'
      Size = 60
    end
    object QrNFSeNfsCabDpsTomaCodigoMunicipio: TIntegerField
      FieldName = 'DpsTomaCodigoMunicipio'
    end
    object QrNFSeNfsCabDpsTomaUf: TWideStringField
      FieldName = 'DpsTomaUf'
      Size = 125
    end
    object QrNFSeNfsCabDpsTomaCodigoPais: TIntegerField
      FieldName = 'DpsTomaCodigoPais'
    end
    object QrNFSeNfsCabDpsTomaCep: TIntegerField
      FieldName = 'DpsTomaCep'
    end
    object QrNFSeNfsCabDpsTomaContatoTelefone: TWideStringField
      FieldName = 'DpsTomaContatoTelefone'
    end
    object QrNFSeNfsCabDpsTomaContatoEmail: TWideStringField
      FieldName = 'DpsTomaContatoEmail'
      Size = 80
    end
    object QrNFSeNfsCabDpsIntermeCpf: TWideStringField
      FieldName = 'DpsIntermeCpf'
      Size = 18
    end
    object QrNFSeNfsCabDpsIntermeCnpj: TWideStringField
      FieldName = 'DpsIntermeCnpj'
      Size = 18
    end
    object QrNFSeNfsCabDpsIntermeInscricaoMunicipal: TWideStringField
      FieldName = 'DpsIntermeInscricaoMunicipal'
      Size = 18
    end
    object QrNFSeNfsCabDpsIntermeRazaoSocial: TWideStringField
      FieldName = 'DpsIntermeRazaoSocial'
      Size = 150
    end
    object QrNFSeNfsCabDpsConstrucaoCivilCodigoObra: TWideStringField
      FieldName = 'DpsConstrucaoCivilCodigoObra'
      Size = 15
    end
    object QrNFSeNfsCabDpsConstrucaoCivilArt: TWideStringField
      FieldName = 'DpsConstrucaoCivilArt'
      Size = 15
    end
    object QrNFSeNfsCabDpsRegimeEspecialTributacao: TSmallintField
      FieldName = 'DpsRegimeEspecialTributacao'
    end
    object QrNFSeNfsCabDpsOptanteSimplesNacional: TSmallintField
      FieldName = 'DpsOptanteSimplesNacional'
    end
    object QrNFSeNfsCabDpsIncentivoFiscal: TSmallintField
      FieldName = 'DpsIncentivoFiscal'
    end
    object QrNFSeNfsCabDpsInfID_ID: TWideStringField
      FieldName = 'DpsInfID_ID'
      Size = 255
    end
    object QrNFSeNfsCabCanCodigoCancelamento: TSmallintField
      FieldName = 'CanCodigoCancelamento'
    end
    object QrNFSeNfsCabCanCanc_DataHora: TDateTimeField
      FieldName = 'CanCanc_DataHora'
    end
    object QrNFSeNfsCabCanCanc_ID: TWideStringField
      FieldName = 'CanCanc_ID'
      Size = 255
    end
    object QrNFSeNfsCabCanCanc_Versao: TFloatField
      FieldName = 'CanCanc_Versao'
    end
    object QrNFSeNfsCabSubNfseSubstituidora: TLargeintField
      FieldName = 'SubNfseSubstituidora'
    end
    object QrNFSeNfsCabSubSubst_ID: TWideStringField
      FieldName = 'SubSubst_ID'
      Size = 255
    end
    object QrNFSeNfsCabSubSubst_Versao: TFloatField
      FieldName = 'SubSubst_Versao'
    end
    object QrNFSeNfsCabLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrNFSeNfsCabDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrNFSeNfsCabDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrNFSeNfsCabUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrNFSeNfsCabUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrNFSeNfsCabAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrNFSeNfsCabAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrNFSeNfsCabCanNumero: TLargeintField
      FieldName = 'CanNumero'
    end
    object QrNFSeNfsCabCanCpf: TWideStringField
      FieldName = 'CanCpf'
      Size = 18
    end
    object QrNFSeNfsCabCanInscricaomunicipal: TWideStringField
      FieldName = 'CanInscricaomunicipal'
      Size = 18
    end
    object QrNFSeNfsCabCanCodigoMunicipio: TIntegerField
      FieldName = 'CanCodigoMunicipio'
    end
    object QrNFSeNfsCabNOMEMUNIINCI: TWideStringField
      FieldName = 'NOMEMUNIINCI'
      Size = 100
    end
    object QrNFSeNfsCabDpsTomaCnpjCpf: TWideStringField
      FieldName = 'DpsTomaCnpjCpf'
      Size = 18
    end
    object QrNFSeNfsCabDpsTomaCnpjCpf_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DpsTomaCnpjCpf_TXT'
      Size = 30
      Calculated = True
    end
    object QrNFSeNfsCabCliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrNFSeNfsCabCodigo: TIntegerField
      FieldName = 'Codigo'
    end
  end
  object DsNFSeNfsCab: TDataSource
    DataSet = QrNFSeNfsCab
    Left = 828
    Top = 64
  end
  object QrNFeXMLI: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      
        'SELECT xml.Codigo, xml.ID, xml.Valor, lay.Pai, lay.Descricao, la' +
        'y.Campo'
      'FROM nfexmli xml'
      'LEFT JOIN nfelayi lay ON lay.ID=xml.ID AND lay.Codigo=xml.Codigo'
      'WHERE xml.FatID=:P0'
      'AND xml.FatNum=:P1'
      'AND xml.Empresa=:P2')
    Left = 284
    Top = 424
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrNFeXMLICodigo: TWideStringField
      FieldName = 'Codigo'
      Size = 5
    end
    object QrNFeXMLIID: TWideStringField
      FieldName = 'ID'
      Size = 6
    end
    object QrNFeXMLIValor: TWideStringField
      FieldName = 'Valor'
      Size = 60
    end
    object QrNFeXMLIPai: TWideStringField
      FieldName = 'Pai'
      Size = 10
    end
    object QrNFeXMLIDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 100
    end
    object QrNFeXMLICampo: TWideStringField
      FieldName = 'Campo'
      Size = 30
    end
  end
  object frxDsNFeXMLI: TfrxDBDataset
    UserName = 'frxDsNFeXMLI'
    CloseDataSource = False
    DataSet = QrNFeXMLI
    BCDToCurrency = False
    DataSetOptions = []
    Left = 312
    Top = 424
  end
  object frxCampos: TfrxReport
    Version = '2022.1'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 40025.074573773150000000
    ReportOptions.LastChange = 40025.074573773150000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    Left = 340
    Top = 424
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsNFeXMLI
        DataSetName = 'frxDsNFeXMLI'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 15.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 15.000000000000000000
      Frame.Typ = []
      MirrorMode = []
      object PageHeader9: TfrxPageHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 58.582701570000000000
        Top = 79.370130000000000000
        Width = 699.213050000000000000
        object Shape7: TfrxShapeView
          AllowVectorExport = True
          Width = 699.213050000000000000
          Height = 37.795300000000000000
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Line24: TfrxLineView
          AllowVectorExport = True
          Top = 18.897650000000000000
          Width = 699.213050000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo215: TfrxMemoView
          AllowVectorExport = True
          Left = 151.181200000000000000
          Width = 396.850650000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'Campos preenchidos no XML')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo293: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 684.094930000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            
              'NF N'#186' [frxDsNFeCabA."ide_nNF"]   -   Chave NF-e: [frxDsNFeCabA."' +
              'Id"]')
          ParentFont = False
        end
        object Memo1: TfrxMemoView
          AllowVectorExport = True
          Top = 37.795300000000000000
          Width = 18.897650000000000000
          Height = 20.787401570000000000
          DataSet = frxDsNFeXMLI
          DataSetName = 'frxDsNFeXMLI'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'C'#243'd.'
            'ID')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          AllowVectorExport = True
          Left = 18.897650000000000000
          Top = 37.795300000000000000
          Width = 52.913420000000000000
          Height = 20.787401570000000000
          DataSet = frxDsNFeXMLI
          DataSetName = 'frxDsNFeXMLI'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Pai'
            'Campo')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          AllowVectorExport = True
          Left = 71.811070000000000000
          Top = 37.795300000000000000
          Width = 627.401980000000000000
          Height = 20.787401570000000000
          DataSet = frxDsNFeXMLI
          DataSetName = 'frxDsNFeXMLI'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Descri'#231#227'o do campo'
            'Valor da campo no XML')
          ParentFont = False
        end
        object Memo216: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[Date]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo5: TfrxMemoView
          AllowVectorExport = True
          Left = 548.031850000000000000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'p'#225'g. [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object MasterData12: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 20.787401574803150000
        Top = 200.315090000000000000
        Width = 699.213050000000000000
        DataSet = frxDsNFeXMLI
        DataSetName = 'frxDsNFeXMLI'
        RowCount = 0
        object Memo301: TfrxMemoView
          AllowVectorExport = True
          Width = 18.897650000000000000
          Height = 20.787401574803150000
          DataSet = frxDsNFeXMLI
          DataSetName = 'frxDsNFeXMLI'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsNFeXMLI."Codigo"]'
            '[frxDsNFeXMLI."ID"]')
          ParentFont = False
        end
        object Memo14: TfrxMemoView
          AllowVectorExport = True
          Left = 18.897650000000000000
          Width = 52.913420000000000000
          Height = 20.787401570000000000
          DataSet = frxDsNFeXMLI
          DataSetName = 'frxDsNFeXMLI'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsNFeXMLI."Pai"]'
            '[frxDsNFeXMLI."Campo"]')
          ParentFont = False
        end
        object Memo15: TfrxMemoView
          AllowVectorExport = True
          Left = 71.811070000000000000
          Width = 627.401980000000000000
          Height = 20.787401574803150000
          DataSet = frxDsNFeXMLI
          DataSetName = 'frxDsNFeXMLI'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsNFeXMLI."Descricao"]'
            '[frxDsNFeXMLI."Valor"]')
          ParentFont = False
        end
      end
      object ReportTitle1: TfrxReportTitle
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 37.795300000000000000
        Top = 18.897650000000000000
        Width = 699.213050000000000000
        object Shape1: TfrxShapeView
          AllowVectorExport = True
          Width = 699.213050000000000000
          Height = 37.795300000000000000
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo8: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 684.094930000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsNFeCabA."NO_Cli"]')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Width = 684.094930000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line1: TfrxLineView
          AllowVectorExport = True
          Top = 18.897650000000000000
          Width = 699.213050000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
      end
    end
  end
  object QrArq: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM nfearq'
      'WHERE IDCtrl=:P0'
      ''
      '')
    Left = 288
    Top = 368
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrArqFatID: TIntegerField
      FieldName = 'FatID'
      Origin = 'nfearq.FatID'
    end
    object QrArqFatNum: TIntegerField
      FieldName = 'FatNum'
      Origin = 'nfearq.FatNum'
    end
    object QrArqEmpresa: TIntegerField
      FieldName = 'Empresa'
      Origin = 'nfearq.Empresa'
    end
    object QrArqIDCtrl: TIntegerField
      FieldName = 'IDCtrl'
      Origin = 'nfearq.IDCtrl'
    end
    object QrArqXML_NFe: TWideMemoField
      FieldName = 'XML_NFe'
      Origin = 'nfearq.XML_NFe'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrArqXML_Aut: TWideMemoField
      FieldName = 'XML_Aut'
      Origin = 'nfearq.XML_Aut'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrArqXML_Can: TWideMemoField
      FieldName = 'XML_Can'
      Origin = 'nfearq.XML_Can'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrArqLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'nfearq.Lk'
    end
    object QrArqDataCad: TDateField
      FieldName = 'DataCad'
      Origin = 'nfearq.DataCad'
    end
    object QrArqDataAlt: TDateField
      FieldName = 'DataAlt'
      Origin = 'nfearq.DataAlt'
    end
    object QrArqUserCad: TIntegerField
      FieldName = 'UserCad'
      Origin = 'nfearq.UserCad'
    end
    object QrArqUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Origin = 'nfearq.UserAlt'
    end
    object QrArqAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Origin = 'nfearq.AlterWeb'
    end
    object QrArqAtivo: TSmallintField
      FieldName = 'Ativo'
      Origin = 'nfearq.Ativo'
    end
  end
  object PMArq: TPopupMenu
    OnPopup = PMArqPopup
    Left = 456
    Top = 432
    object NFe1: TMenuItem
      Caption = '&NFe'
      Enabled = False
    end
    object Autorizao1: TMenuItem
      Caption = '&Autoriza'#231#227'o'
      Enabled = False
    end
    object Cancelamento2: TMenuItem
      Caption = '&Cancelamento'
      Enabled = False
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object DiretriodoarquivoXML1: TMenuItem
      Caption = '&Diret'#243'rio do arquivo XML'
    end
  end
  object frxListaNFes: TfrxReport
    Version = '2022.1'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 40025.074573773150000000
    ReportOptions.LastChange = 40025.074573773150000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    OnGetValue = frxListaNFesGetValue
    Left = 208
    Top = 340
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsNFe_100
        DataSetName = 'frxDsNFe_100'
      end
      item
        DataSet = frxDsNFe_101
        DataSetName = 'frxDsNFe_101'
      end
      item
        DataSet = frxDsNFe_XXX
        DataSetName = 'frxDsNFe_XXX'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      Orientation = poLandscape
      PaperWidth = 297.000000000000000000
      PaperHeight = 210.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 15.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Frame.Typ = []
      MirrorMode = []
      object PageHeader9: TfrxPageHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 60.472480000000000000
        Top = 79.370130000000000000
        Width = 990.236860000000000000
        object Shape7: TfrxShapeView
          AllowVectorExport = True
          Width = 990.236860000000000000
          Height = 37.795300000000000000
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Line24: TfrxLineView
          AllowVectorExport = True
          Top = 18.897650000000000000
          Width = 990.236860000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo293: TfrxMemoView
          AllowVectorExport = True
          Left = 151.181200000000000000
          Width = 687.874460000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'Per'#237'odo da pesquisa: [VARF_PERIODO]')
          ParentFont = False
        end
        object Memo216: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            '[Date]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo5: TfrxMemoView
          AllowVectorExport = True
          Left = 839.055660000000000000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'p'#225'g. [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo8: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 264.566929130000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            'Status: [VARF_STATUS]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo42: TfrxMemoView
          AllowVectorExport = True
          Left = 272.126160000000000000
          Top = 18.897650000000000000
          Width = 445.984540000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_APENAS_EMP]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo43: TfrxMemoView
          AllowVectorExport = True
          Left = 718.110700000000000000
          Top = 18.897650000000000000
          Width = 264.567100000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'Ambiente: [VARF_AMBIENTE]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo44: TfrxMemoView
          AllowVectorExport = True
          Top = 41.574830000000010000
          Width = 495.118430000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            'Empresa: [VARF_EMPRESA]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo45: TfrxMemoView
          AllowVectorExport = True
          Left = 495.118430000000000000
          Top = 41.574830000000010000
          Width = 495.118430000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haRight
          Memo.UTF8W = (
            'Terceiro: [VARF_TERCEIRO]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object MasterData12: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 13.228346460000000000
        Top = 260.787570000000000000
        Width = 990.236860000000000000
        DataSet = frxDsNFe_100
        DataSetName = 'frxDsNFe_100'
        RowCount = 0
        object Memo14: TfrxMemoView
          AllowVectorExport = True
          Width = 64.252010000000000000
          Height = 13.228346460000000000
          DataSet = frxDsNFe_100
          DataSetName = 'frxDsNFe_100'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[FormatFloat('#39'000,000,000'#39', <frxDsNFe_100."ide_nNF">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo15: TfrxMemoView
          AllowVectorExport = True
          Left = 64.252010000000000000
          Width = 22.677180000000000000
          Height = 13.228346460000000000
          DataSet = frxDsNFe_100
          DataSetName = 'frxDsNFe_100'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[FormatFloat('#39'000'#39', <frxDsNFe_100."ide_serie">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo6: TfrxMemoView
          AllowVectorExport = True
          Left = 86.929190000000000000
          Width = 45.354330710000000000
          Height = 13.228346460000000000
          DataField = 'ide_dEmi'
          DataSet = frxDsNFe_100
          DataSetName = 'frxDsNFe_100'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsNFe_100."ide_dEmi"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo9: TfrxMemoView
          AllowVectorExport = True
          Left = 132.283550000000000000
          Width = 68.031496062992130000
          Height = 13.228346460000000000
          DataField = 'ICMSTot_vProd'
          DataSet = frxDsNFe_100
          DataSetName = 'frxDsNFe_100'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsNFe_100."ICMSTot_vProd"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo11: TfrxMemoView
          AllowVectorExport = True
          Left = 566.929499999999900000
          Width = 68.031496060000000000
          Height = 13.228346460000000000
          DataField = 'ICMSTot_vNF'
          DataSet = frxDsNFe_100
          DataSetName = 'frxDsNFe_100'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsNFe_100."ICMSTot_vNF"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo13: TfrxMemoView
          AllowVectorExport = True
          Left = 200.315090000000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          DataField = 'ICMSTot_vST'
          DataSet = frxDsNFe_100
          DataSetName = 'frxDsNFe_100'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsNFe_100."ICMSTot_vST"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo17: TfrxMemoView
          AllowVectorExport = True
          Left = 260.787570000000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          DataField = 'ICMSTot_vFrete'
          DataSet = frxDsNFe_100
          DataSetName = 'frxDsNFe_100'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsNFe_100."ICMSTot_vFrete"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo19: TfrxMemoView
          AllowVectorExport = True
          Left = 321.260050000000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          DataField = 'ICMSTot_vSeg'
          DataSet = frxDsNFe_100
          DataSetName = 'frxDsNFe_100'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsNFe_100."ICMSTot_vSeg"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo24: TfrxMemoView
          AllowVectorExport = True
          Left = 381.732530000000000000
          Width = 64.251968503937010000
          Height = 13.228346460000000000
          DataField = 'ICMSTot_vIPI'
          DataSet = frxDsNFe_100
          DataSetName = 'frxDsNFe_100'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsNFe_100."ICMSTot_vIPI"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo25: TfrxMemoView
          AllowVectorExport = True
          Left = 445.984540000000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          DataField = 'ICMSTot_vOutro'
          DataSet = frxDsNFe_100
          DataSetName = 'frxDsNFe_100'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsNFe_100."ICMSTot_vOutro"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo26: TfrxMemoView
          AllowVectorExport = True
          Left = 506.457020000000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          DataField = 'ICMSTot_vDesc'
          DataSet = frxDsNFe_100
          DataSetName = 'frxDsNFe_100'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsNFe_100."ICMSTot_vDesc"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo27: TfrxMemoView
          AllowVectorExport = True
          Left = 634.961040000000000000
          Width = 68.031500940000000000
          Height = 13.228346460000000000
          DataField = 'ICMSTot_vBC'
          DataSet = frxDsNFe_100
          DataSetName = 'frxDsNFe_100'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsNFe_100."ICMSTot_vBC"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo29: TfrxMemoView
          AllowVectorExport = True
          Left = 702.992580000000000000
          Width = 64.251968500000000000
          Height = 13.228346460000000000
          DataField = 'ICMSTot_vICMS'
          DataSet = frxDsNFe_100
          DataSetName = 'frxDsNFe_100'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsNFe_100."ICMSTot_vICMS"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo63: TfrxMemoView
          AllowVectorExport = True
          Left = 888.189550000000000000
          Width = 86.929141180000000000
          Height = 13.228346460000000000
          DataField = 'NOME_tpEmis'
          DataSet = frxDsNFe_100
          DataSetName = 'frxDsNFe_100'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsNFe_100."NOME_tpEmis"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo64: TfrxMemoView
          AllowVectorExport = True
          Left = 975.118740000000000000
          Width = 15.118071180000000000
          Height = 13.228346460000000000
          DataField = 'NOME_tpNF'
          DataSet = frxDsNFe_100
          DataSetName = 'frxDsNFe_100'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsNFe_100."NOME_tpNF"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo93: TfrxMemoView
          AllowVectorExport = True
          Left = 767.244590000000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          DataField = 'ICMSTot_vPIS'
          DataSet = frxDsNFe_100
          DataSetName = 'frxDsNFe_100'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsNFe_100."ICMSTot_vPIS"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo96: TfrxMemoView
          AllowVectorExport = True
          Left = 827.717070000000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          DataField = 'ICMSTot_vCOFINS'
          DataSet = frxDsNFe_100
          DataSetName = 'frxDsNFe_100'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsNFe_100."ICMSTot_vCOFINS"]')
          ParentFont = False
          WordWrap = False
        end
      end
      object ReportTitle1: TfrxReportTitle
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 37.795300000000000000
        Top = 18.897650000000000000
        Width = 990.236860000000000000
        object Shape1: TfrxShapeView
          AllowVectorExport = True
          Width = 990.236860000000000000
          Height = 37.795300000000000000
          Frame.Typ = []
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo215: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 975.118740000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            'Lista de Notas Fiscais Eletr'#244'nicas')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo2: TfrxMemoView
          AllowVectorExport = True
          Left = 7.559060000000000000
          Width = 975.118740000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line1: TfrxLineView
          AllowVectorExport = True
          Top = 18.897650000000000000
          Width = 990.236860000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
      end
      object PageFooter3: TfrxPageFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 15.118110240000000000
        Top = 623.622450000000000000
        Width = 990.236860000000000000
        object Memo120: TfrxMemoView
          AllowVectorExport = True
          Width = 1046.929810000000000000
          Height = 15.118110240000000000
          DataSet = DModFin.frxDsExtratos
          DataSetName = 'frxDsExtratos'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
      end
      object GroupHeader1: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 35.905526460000000000
        Top = 200.315090000000000000
        Width = 990.236860000000000000
        Condition = 'frxDsNFe_100."Ordem"'
        object Memo1: TfrxMemoView
          AllowVectorExport = True
          Top = 22.677179999999990000
          Width = 64.252010000000000000
          Height = 13.228346460000000000
          DataSetName = 'frxDsNFeXMLI_'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'N'#186' NF-e')
          ParentFont = False
          WordWrap = False
        end
        object Memo3: TfrxMemoView
          AllowVectorExport = True
          Left = 64.252010000000000000
          Top = 22.677179999999990000
          Width = 22.677180000000000000
          Height = 13.228346460000000000
          DataSetName = 'frxDsNFeXMLI_'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'S'#233'rie')
          ParentFont = False
          WordWrap = False
        end
        object Memo4: TfrxMemoView
          AllowVectorExport = True
          Left = 86.929190000000000000
          Top = 22.677179999999990000
          Width = 45.354330710000000000
          Height = 13.228346460000000000
          DataSetName = 'frxDsNFeXMLI_'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Emiss'#227'o')
          ParentFont = False
          WordWrap = False
        end
        object Memo7: TfrxMemoView
          AllowVectorExport = True
          Left = 132.283550000000000000
          Top = 22.677179999999990000
          Width = 68.031496062992130000
          Height = 13.228346460000000000
          DataSetName = 'frxDsNFeXMLI_'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Produtos')
          ParentFont = False
          WordWrap = False
        end
        object Memo10: TfrxMemoView
          AllowVectorExport = True
          Left = 566.929500000000000000
          Top = 22.677179999999990000
          Width = 68.031496060000000000
          Height = 13.228346460000000000
          DataSetName = 'frxDsNFeXMLI_'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Total NFe')
          ParentFont = False
          WordWrap = False
        end
        object Memo12: TfrxMemoView
          AllowVectorExport = True
          Left = 200.315090000000000000
          Top = 22.677179999999990000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          DataSetName = 'frxDsNFeXMLI_'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'ICMS ST')
          ParentFont = False
          WordWrap = False
        end
        object Memo16: TfrxMemoView
          AllowVectorExport = True
          Left = 260.787570000000000000
          Top = 22.677179999999990000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          DataSetName = 'frxDsNFeXMLI_'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Frete')
          ParentFont = False
          WordWrap = False
        end
        object Memo18: TfrxMemoView
          AllowVectorExport = True
          Left = 321.260050000000000000
          Top = 22.677179999999990000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          DataSetName = 'frxDsNFeXMLI_'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Seguro')
          ParentFont = False
          WordWrap = False
        end
        object Memo20: TfrxMemoView
          AllowVectorExport = True
          Left = 381.732530000000000000
          Top = 22.677179999999990000
          Width = 64.251968503937010000
          Height = 13.228346460000000000
          DataSetName = 'frxDsNFeXMLI_'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'IPI')
          ParentFont = False
          WordWrap = False
        end
        object Memo21: TfrxMemoView
          AllowVectorExport = True
          Left = 445.984540000000000000
          Top = 22.677179999999990000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          DataSetName = 'frxDsNFeXMLI_'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Outras desp.')
          ParentFont = False
          WordWrap = False
        end
        object Memo22: TfrxMemoView
          AllowVectorExport = True
          Left = 506.457020000000000000
          Top = 22.677179999999990000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          DataSetName = 'frxDsNFeXMLI_'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Desconto')
          ParentFont = False
          WordWrap = False
        end
        object Memo23: TfrxMemoView
          AllowVectorExport = True
          Left = 634.961040000000000000
          Top = 22.677179999999990000
          Width = 68.031496060000000000
          Height = 13.228346460000000000
          DataSetName = 'frxDsNFeXMLI_'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'BC ICMS')
          ParentFont = False
          WordWrap = False
        end
        object Memo28: TfrxMemoView
          AllowVectorExport = True
          Left = 702.992580000000000000
          Top = 22.677179999999990000
          Width = 64.251968500000000000
          Height = 13.228346460000000000
          DataSetName = 'frxDsNFeXMLI_'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'ICMS')
          ParentFont = False
          WordWrap = False
        end
        object Memo34: TfrxMemoView
          AllowVectorExport = True
          Top = 3.779529999999994000
          Width = 94.488250000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            'AUTORIZADA')
          ParentFont = False
        end
        object Memo61: TfrxMemoView
          AllowVectorExport = True
          Left = 888.189550000000000000
          Top = 22.677179999999990000
          Width = 86.929141180000000000
          Height = 13.228346460000000000
          DataSetName = 'frxDsNFeXMLI_'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Tipo')
          ParentFont = False
          WordWrap = False
        end
        object Memo62: TfrxMemoView
          AllowVectorExport = True
          Left = 975.118740000000000000
          Top = 22.677179999999990000
          Width = 15.118071180000000000
          Height = 13.228346460000000000
          DataSetName = 'frxDsNFeXMLI_'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'E/S')
          ParentFont = False
          WordWrap = False
        end
        object Memo94: TfrxMemoView
          AllowVectorExport = True
          Left = 767.244590000000000000
          Top = 22.677179999999990000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          DataSetName = 'frxDsNFeXMLI_'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'PIS')
          ParentFont = False
          WordWrap = False
        end
        object Memo97: TfrxMemoView
          AllowVectorExport = True
          Left = 827.717070000000000000
          Top = 22.677179999999990000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          DataSetName = 'frxDsNFeXMLI_'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'COFINS')
          ParentFont = False
          WordWrap = False
        end
      end
      object GroupFooter1: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 22.677177560000000000
        Top = 298.582870000000000000
        Width = 990.236860000000000000
        object Memo54: TfrxMemoView
          AllowVectorExport = True
          Left = 566.929499999999900000
          Width = 68.031496060000000000
          Height = 13.228346460000000000
          DataSet = frxDsNFe_100
          DataSetName = 'frxDsNFe_100'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsNFe_100."ICMSTot_vNF">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo55: TfrxMemoView
          AllowVectorExport = True
          Left = 321.260050000000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          DataSet = frxDsNFe_100
          DataSetName = 'frxDsNFe_100'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsNFe_100."ICMSTot_vSeg">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo56: TfrxMemoView
          AllowVectorExport = True
          Left = 381.732530000000000000
          Width = 64.251968503937010000
          Height = 13.228346460000000000
          DataSet = frxDsNFe_100
          DataSetName = 'frxDsNFe_100'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsNFe_100."ICMSTot_vIPI">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo57: TfrxMemoView
          AllowVectorExport = True
          Left = 445.984540000000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          DataSet = frxDsNFe_100
          DataSetName = 'frxDsNFe_100'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsNFe_100."ICMSTot_vOutro">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo58: TfrxMemoView
          AllowVectorExport = True
          Left = 506.457020000000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          DataSet = frxDsNFe_100
          DataSetName = 'frxDsNFe_100'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsNFe_100."ICMSTot_vDesc">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo59: TfrxMemoView
          AllowVectorExport = True
          Left = 634.961040000000000000
          Width = 68.031496060000000000
          Height = 13.228346460000000000
          DataSet = frxDsNFe_100
          DataSetName = 'frxDsNFe_100'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsNFe_100."ICMSTot_vBC">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo60: TfrxMemoView
          AllowVectorExport = True
          Left = 702.992580000000000000
          Width = 64.251968500000000000
          Height = 13.228346460000000000
          DataSet = frxDsNFe_100
          DataSetName = 'frxDsNFe_100'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsNFe_100."ICMSTot_vICMS">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo65: TfrxMemoView
          AllowVectorExport = True
          Width = 132.283501180000000000
          Height = 13.228346460000000000
          DataSetName = 'frxDsNFeXMLI_'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'TOTAIS:  ')
          ParentFont = False
          WordWrap = False
        end
        object Memo66: TfrxMemoView
          AllowVectorExport = True
          Left = 888.189550000000000000
          Width = 102.047261180000000000
          Height = 13.228346460000000000
          DataSetName = 'frxDsNFeXMLI_'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          ParentFont = False
          WordWrap = False
        end
        object Memo67: TfrxMemoView
          AllowVectorExport = True
          Left = 132.283550000000000000
          Width = 68.031496060000000000
          Height = 13.228346460000000000
          DataSet = frxDsNFe_100
          DataSetName = 'frxDsNFe_100'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsNFe_100."ICMSTot_vProd">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo68: TfrxMemoView
          AllowVectorExport = True
          Left = 200.315090000000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          DataSet = frxDsNFe_100
          DataSetName = 'frxDsNFe_100'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsNFe_100."ICMSTot_vST">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo69: TfrxMemoView
          AllowVectorExport = True
          Left = 260.787570000000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          DataSet = frxDsNFe_100
          DataSetName = 'frxDsNFe_100'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsNFe_100."ICMSTot_vFrete">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo95: TfrxMemoView
          AllowVectorExport = True
          Left = 767.244590000000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          DataSet = frxDsNFe_100
          DataSetName = 'frxDsNFe_100'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsNFe_100."ICMSTot_vPIS">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo98: TfrxMemoView
          AllowVectorExport = True
          Left = 827.717070000000000000
          Width = 60.472440940000000000
          Height = 13.228346460000000000
          DataSet = frxDsNFe_100
          DataSetName = 'frxDsNFe_100'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsNFe_100."ICMSTot_vCOFINS">)]')
          ParentFont = False
          WordWrap = False
        end
      end
      object GroupHeader2: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 35.905526460000000000
        Top = 343.937230000000000000
        Width = 990.236860000000000000
        Condition = 'frxDsNFe_101."Ordem"'
        object Memo31: TfrxMemoView
          AllowVectorExport = True
          Left = 177.637910000000000000
          Top = 22.677180000000020000
          Width = 68.031491180000000000
          Height = 13.228346460000000000
          DataSetName = 'frxDsNFeXMLI_'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Tipo')
          ParentFont = False
          WordWrap = False
        end
        object Memo32: TfrxMemoView
          AllowVectorExport = True
          Left = 245.669450000000000000
          Top = 22.677180000000020000
          Width = 15.118071180000000000
          Height = 13.228346460000000000
          DataSetName = 'frxDsNFeXMLI_'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'E/S')
          ParentFont = False
          WordWrap = False
        end
        object Memo35: TfrxMemoView
          AllowVectorExport = True
          Top = 3.779530000000022000
          Width = 990.236860000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            'CANCELADA')
          ParentFont = False
        end
        object Memo39: TfrxMemoView
          AllowVectorExport = True
          Top = 22.677180000000020000
          Width = 64.252010000000000000
          Height = 13.228346460000000000
          DataSetName = 'frxDsNFeXMLI_'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'N'#186' NF-e')
          ParentFont = False
          WordWrap = False
        end
        object Memo40: TfrxMemoView
          AllowVectorExport = True
          Left = 64.252010000000000000
          Top = 22.677180000000020000
          Width = 22.677180000000000000
          Height = 13.228346460000000000
          DataSetName = 'frxDsNFeXMLI_'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'S'#233'rie')
          ParentFont = False
          WordWrap = False
        end
        object Memo41: TfrxMemoView
          AllowVectorExport = True
          Left = 132.283550000000000000
          Top = 22.677180000000020000
          Width = 45.354330710000000000
          Height = 13.228346460000000000
          DataSetName = 'frxDsNFeXMLI_'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Emiss'#227'o')
          ParentFont = False
          WordWrap = False
        end
        object Memo46: TfrxMemoView
          AllowVectorExport = True
          Left = 86.929190000000000000
          Top = 22.677180000000020000
          Width = 22.677180000000000000
          Height = 13.228346460000000000
          DataSetName = 'frxDsNFeXMLI_'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Ano')
          ParentFont = False
          WordWrap = False
        end
        object Memo47: TfrxMemoView
          AllowVectorExport = True
          Left = 109.606370000000000000
          Top = 22.677180000000020000
          Width = 22.677180000000000000
          Height = 13.228346460000000000
          DataSetName = 'frxDsNFeXMLI_'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'M'#234's')
          ParentFont = False
          WordWrap = False
        end
        object Memo50: TfrxMemoView
          AllowVectorExport = True
          Left = 260.787570000000000000
          Top = 22.677180000000020000
          Width = 226.771653540000000000
          Height = 13.228346460000000000
          DataSetName = 'frxDsNFeXMLI_'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Chave NF-e')
          ParentFont = False
          WordWrap = False
        end
        object Memo52: TfrxMemoView
          AllowVectorExport = True
          Left = 638.740570000000000000
          Top = 22.677180000000020000
          Width = 351.496260710000000000
          Height = 13.228346460000000000
          DataSetName = 'frxDsNFeXMLI_'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Motivo ou descri'#231#227'o do status')
          ParentFont = False
          WordWrap = False
        end
        object Memo89: TfrxMemoView
          AllowVectorExport = True
          Left = 487.559370000000000000
          Top = 22.677180000000020000
          Width = 79.370081180000000000
          Height = 13.228346460000000000
          DataSetName = 'frxDsNFeXMLI_'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'dh Recibo')
          ParentFont = False
          WordWrap = False
        end
        object Memo91: TfrxMemoView
          AllowVectorExport = True
          Left = 566.929500000000000000
          Top = 22.677180000000020000
          Width = 71.811021180000000000
          Height = 13.228346460000000000
          DataSetName = 'frxDsNFeXMLI_'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'N'#186' protocolo canc.')
          ParentFont = False
          WordWrap = False
        end
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 13.228346460000000000
        Top = 404.409710000000000000
        Width = 990.236860000000000000
        DataSet = frxDsNFe_101
        DataSetName = 'frxDsNFe_101'
        RowCount = 0
        object Memo30: TfrxMemoView
          AllowVectorExport = True
          Left = 177.637910000000000000
          Width = 68.031491180000000000
          Height = 13.228346460000000000
          DataSet = frxDsNFe_100
          DataSetName = 'frxDsNFe_100'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsNFe_101."NOME_tpEmis"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo33: TfrxMemoView
          AllowVectorExport = True
          Left = 245.669450000000000000
          Width = 15.118071180000000000
          Height = 13.228346460000000000
          DataSet = frxDsNFe_100
          DataSetName = 'frxDsNFe_100'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsNFe_101."NOME_tpNF"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo36: TfrxMemoView
          AllowVectorExport = True
          Width = 64.252010000000000000
          Height = 13.228346460000000000
          DataSet = frxDsNFe_100
          DataSetName = 'frxDsNFe_100'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[FormatFloat('#39'000,000,000'#39', <frxDsNFe_101."ide_nNF">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo37: TfrxMemoView
          AllowVectorExport = True
          Left = 64.252010000000000000
          Width = 22.677180000000000000
          Height = 13.228346460000000000
          DataSet = frxDsNFe_XXX
          DataSetName = 'frxDsNFe_XXX'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsNFe_101."ide_serie"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo38: TfrxMemoView
          AllowVectorExport = True
          Left = 132.283550000000000000
          Width = 45.354330710000000000
          Height = 13.228346460000000000
          DataSet = frxDsNFe_XXX
          DataSetName = 'frxDsNFe_XXX'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsNFe_101."ide_dEmi"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo48: TfrxMemoView
          AllowVectorExport = True
          Left = 86.929190000000000000
          Width = 22.677180000000000000
          Height = 13.228346460000000000
          DataSet = frxDsNFe_XXX
          DataSetName = 'frxDsNFe_XXX'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsNFe_101."ide_AAMM_AA"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo49: TfrxMemoView
          AllowVectorExport = True
          Left = 109.606370000000000000
          Width = 22.677180000000000000
          Height = 13.228346460000000000
          DataSet = frxDsNFe_XXX
          DataSetName = 'frxDsNFe_XXX'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsNFe_101."ide_AAMM_MM"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo51: TfrxMemoView
          AllowVectorExport = True
          Left = 260.787570000000000000
          Width = 226.771653540000000000
          Height = 13.228346460000000000
          DataField = 'Id_TXT'
          DataSet = frxDsNFe_101
          DataSetName = 'frxDsNFe_101'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsNFe_101."Id_TXT"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo53: TfrxMemoView
          AllowVectorExport = True
          Left = 638.740570000000000000
          Width = 351.496260710000000000
          Height = 13.228346460000000000
          DataSet = frxDsNFe_XXX
          DataSetName = 'frxDsNFe_XXX'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsNFe_101."Motivo"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo90: TfrxMemoView
          AllowVectorExport = True
          Left = 487.559370000000000000
          Width = 79.370081180000000000
          Height = 13.228346460000000000
          DataField = 'infCanc_dhRecbto'
          DataSet = frxDsNFe_101
          DataSetName = 'frxDsNFe_101'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsNFe_101."infCanc_dhRecbto"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo92: TfrxMemoView
          AllowVectorExport = True
          Left = 566.929500000000000000
          Width = 71.811021180000000000
          Height = 13.228346460000000000
          DataSet = frxDsNFe_101
          DataSetName = 'frxDsNFe_101'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsNFe_101."infCanc_nProt"]')
          ParentFont = False
          WordWrap = False
        end
      end
      object GroupHeader3: TfrxGroupHeader
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 35.905526460000000000
        Top = 464.882190000000000000
        Width = 990.236860000000000000
        Condition = 'frxDsNFe_XXX."Ordem"'
        object Memo70: TfrxMemoView
          AllowVectorExport = True
          Left = 177.637910000000000000
          Top = 22.677180000000020000
          Width = 68.031491180000000000
          Height = 13.228346460000000000
          DataSetName = 'frxDsNFeXMLI_'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Tipo')
          ParentFont = False
          WordWrap = False
        end
        object Memo71: TfrxMemoView
          AllowVectorExport = True
          Left = 245.669450000000000000
          Top = 22.677180000000020000
          Width = 15.118071180000000000
          Height = 13.228346460000000000
          DataSetName = 'frxDsNFeXMLI_'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'E/S')
          ParentFont = False
          WordWrap = False
        end
        object Memo72: TfrxMemoView
          AllowVectorExport = True
          Top = 3.779530000000022000
          Width = 990.236860000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = []
          Memo.UTF8W = (
            '[frxDsNFe_XXX."NOME_ORDEM"]')
          ParentFont = False
        end
        object Memo73: TfrxMemoView
          AllowVectorExport = True
          Top = 22.677180000000020000
          Width = 64.252010000000000000
          Height = 13.228346460000000000
          DataSetName = 'frxDsNFeXMLI_'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'N'#186' NF-e')
          ParentFont = False
          WordWrap = False
        end
        object Memo74: TfrxMemoView
          AllowVectorExport = True
          Left = 64.252010000000000000
          Top = 22.677180000000020000
          Width = 22.677180000000000000
          Height = 13.228346460000000000
          DataSetName = 'frxDsNFeXMLI_'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'S'#233'rie')
          ParentFont = False
          WordWrap = False
        end
        object Memo75: TfrxMemoView
          AllowVectorExport = True
          Left = 132.283550000000000000
          Top = 22.677180000000020000
          Width = 45.354330710000000000
          Height = 13.228346460000000000
          DataSetName = 'frxDsNFeXMLI_'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Emiss'#227'o')
          ParentFont = False
          WordWrap = False
        end
        object Memo76: TfrxMemoView
          AllowVectorExport = True
          Left = 86.929190000000000000
          Top = 22.677180000000020000
          Width = 22.677180000000000000
          Height = 13.228346460000000000
          DataSetName = 'frxDsNFeXMLI_'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Ano')
          ParentFont = False
          WordWrap = False
        end
        object Memo77: TfrxMemoView
          AllowVectorExport = True
          Left = 109.606370000000000000
          Top = 22.677180000000020000
          Width = 22.677180000000000000
          Height = 13.228346460000000000
          DataSetName = 'frxDsNFeXMLI_'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'M'#234's')
          ParentFont = False
          WordWrap = False
        end
        object Memo78: TfrxMemoView
          AllowVectorExport = True
          Left = 260.787570000000000000
          Top = 22.677180000000020000
          Width = 30.236240000000000000
          Height = 13.228346460000000000
          DataSetName = 'frxDsNFeXMLI_'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Status')
          ParentFont = False
          WordWrap = False
        end
        object Memo79: TfrxMemoView
          AllowVectorExport = True
          Left = 291.023810000000000000
          Top = 22.677180000000020000
          Width = 699.213020710000000000
          Height = 13.228346460000000000
          DataSetName = 'frxDsNFeXMLI_'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Motivo ou descri'#231#227'o do status')
          ParentFont = False
          WordWrap = False
        end
      end
      object MasterData2: TfrxMasterData
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Height = 13.228346460000000000
        Top = 525.354670000000100000
        Width = 990.236860000000000000
        DataSet = frxDsNFe_XXX
        DataSetName = 'frxDsNFe_XXX'
        RowCount = 0
        object Memo80: TfrxMemoView
          AllowVectorExport = True
          Left = 177.637910000000000000
          Width = 68.031491180000000000
          Height = 13.228346460000000000
          DataSet = frxDsNFe_100
          DataSetName = 'frxDsNFe_100'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsNFe_XXX."NOME_tpEmis"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo81: TfrxMemoView
          AllowVectorExport = True
          Left = 245.669450000000000000
          Width = 15.118071180000000000
          Height = 13.228346460000000000
          DataSet = frxDsNFe_100
          DataSetName = 'frxDsNFe_100'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsNFe_XXX."NOME_tpNF"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo82: TfrxMemoView
          AllowVectorExport = True
          Width = 64.252010000000000000
          Height = 13.228346460000000000
          DataSet = frxDsNFe_100
          DataSetName = 'frxDsNFe_100'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[FormatFloat('#39'000,000,000'#39', <frxDsNFe_XXX."ide_nNF">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo83: TfrxMemoView
          AllowVectorExport = True
          Left = 64.252010000000000000
          Width = 22.677180000000000000
          Height = 13.228346460000000000
          DataField = 'ide_serie'
          DataSet = frxDsNFe_XXX
          DataSetName = 'frxDsNFe_XXX'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsNFe_XXX."ide_serie"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo84: TfrxMemoView
          AllowVectorExport = True
          Left = 132.283550000000000000
          Width = 45.354330710000000000
          Height = 13.228346460000000000
          DataField = 'ide_dEmi'
          DataSet = frxDsNFe_XXX
          DataSetName = 'frxDsNFe_XXX'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsNFe_XXX."ide_dEmi"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo85: TfrxMemoView
          AllowVectorExport = True
          Left = 86.929190000000000000
          Width = 22.677180000000000000
          Height = 13.228346460000000000
          DataSet = frxDsNFe_XXX
          DataSetName = 'frxDsNFe_XXX'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsNFe_XXX."ide_AAMM_AA"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo86: TfrxMemoView
          AllowVectorExport = True
          Left = 109.606370000000000000
          Width = 22.677180000000000000
          Height = 13.228346460000000000
          DataField = 'ide_AAMM_MM'
          DataSet = frxDsNFe_XXX
          DataSetName = 'frxDsNFe_XXX'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsNFe_XXX."ide_AAMM_MM"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo87: TfrxMemoView
          AllowVectorExport = True
          Left = 260.787570000000000000
          Width = 30.236240000000000000
          Height = 13.228346460000000000
          DataSet = frxDsNFe_XXX
          DataSetName = 'frxDsNFe_XXX'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[FormatFloat('#39'000'#39', <frxDsNFe_XXX."ide_serie">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo88: TfrxMemoView
          AllowVectorExport = True
          Left = 291.023810000000000000
          Width = 699.213020710000000000
          Height = 13.228346460000000000
          DataField = 'Motivo'
          DataSet = frxDsNFe_XXX
          DataSetName = 'frxDsNFe_XXX'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsNFe_XXX."Motivo"]')
          ParentFont = False
          WordWrap = False
        end
      end
      object Footer1: TfrxFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Top = 442.205010000000000000
        Width = 990.236860000000000000
      end
      object GroupFooter2: TfrxGroupFooter
        FillType = ftBrush
        FillGap.Top = 0
        FillGap.Left = 0
        FillGap.Bottom = 0
        FillGap.Right = 0
        Frame.Typ = []
        Top = 563.149970000000100000
        Width = 990.236860000000000000
      end
    end
  end
  object QrNFe_100: TMySQLQuery
    Database = Dmod.ZZDB
    SQL.Strings = (
      'SELECT * FROM nfe_100'
      'ORDER BY ide_nNF')
    Left = 208
    Top = 368
    object QrNFe_100Ordem: TIntegerField
      FieldName = 'Ordem'
    end
    object QrNFe_100ide_nNF: TIntegerField
      FieldName = 'ide_nNF'
    end
    object QrNFe_100ide_serie: TIntegerField
      FieldName = 'ide_serie'
    end
    object QrNFe_100ide_dEmi: TDateField
      FieldName = 'ide_dEmi'
    end
    object QrNFe_100ICMSTot_vProd: TFloatField
      FieldName = 'ICMSTot_vProd'
    end
    object QrNFe_100ICMSTot_vST: TFloatField
      FieldName = 'ICMSTot_vST'
    end
    object QrNFe_100ICMSTot_vFrete: TFloatField
      FieldName = 'ICMSTot_vFrete'
    end
    object QrNFe_100ICMSTot_vSeg: TFloatField
      FieldName = 'ICMSTot_vSeg'
    end
    object QrNFe_100ICMSTot_vIPI: TFloatField
      FieldName = 'ICMSTot_vIPI'
    end
    object QrNFe_100ICMSTot_vOutro: TFloatField
      FieldName = 'ICMSTot_vOutro'
    end
    object QrNFe_100ICMSTot_vDesc: TFloatField
      FieldName = 'ICMSTot_vDesc'
    end
    object QrNFe_100ICMSTot_vNF: TFloatField
      FieldName = 'ICMSTot_vNF'
    end
    object QrNFe_100ICMSTot_vBC: TFloatField
      FieldName = 'ICMSTot_vBC'
    end
    object QrNFe_100ICMSTot_vICMS: TFloatField
      FieldName = 'ICMSTot_vICMS'
    end
    object QrNFe_100ICMSTot_vPIS: TFloatField
      FieldName = 'ICMSTot_vPIS'
    end
    object QrNFe_100ICMSTot_vCOFINS: TFloatField
      FieldName = 'ICMSTot_vCOFINS'
    end
    object QrNFe_100NOME_tpEmis: TWideStringField
      FieldName = 'NOME_tpEmis'
      Size = 30
    end
    object QrNFe_100NOME_tpNF: TWideStringField
      FieldName = 'NOME_tpNF'
      Size = 1
    end
  end
  object frxDsNFe_100: TfrxDBDataset
    UserName = 'frxDsNFe_100'
    CloseDataSource = False
    DataSet = QrNFe_100
    BCDToCurrency = False
    DataSetOptions = []
    Left = 236
    Top = 368
  end
  object QrNFe_XXX: TMySQLQuery
    Database = Dmod.ZZDB
    OnCalcFields = QrNFe_XXXCalcFields
    SQL.Strings = (
      'SELECT * FROM nfe_xxx'
      'ORDER BY Ordem, ide_nNF;')
    Left = 208
    Top = 424
    object QrNFe_XXXOrdem: TIntegerField
      FieldName = 'Ordem'
    end
    object QrNFe_XXXcStat: TIntegerField
      FieldName = 'cStat'
    end
    object QrNFe_XXXide_nNF: TIntegerField
      FieldName = 'ide_nNF'
    end
    object QrNFe_XXXide_serie: TIntegerField
      FieldName = 'ide_serie'
    end
    object QrNFe_XXXide_AAMM_AA: TWideStringField
      FieldName = 'ide_AAMM_AA'
      Size = 2
    end
    object QrNFe_XXXide_AAMM_MM: TWideStringField
      FieldName = 'ide_AAMM_MM'
      Size = 2
    end
    object QrNFe_XXXide_dEmi: TDateField
      FieldName = 'ide_dEmi'
    end
    object QrNFe_XXXNOME_tpEmis: TWideStringField
      FieldName = 'NOME_tpEmis'
      Size = 30
    end
    object QrNFe_XXXNOME_tpNF: TWideStringField
      FieldName = 'NOME_tpNF'
      Size = 1
    end
    object QrNFe_XXXStatus: TIntegerField
      FieldName = 'Status'
    end
    object QrNFe_XXXMotivo: TWideStringField
      FieldName = 'Motivo'
      Size = 255
    end
    object QrNFe_XXXNOME_ORDEM: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOME_ORDEM'
      Size = 50
      Calculated = True
    end
  end
  object frxDsNFe_XXX: TfrxDBDataset
    UserName = 'frxDsNFe_XXX'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Ordem=Ordem'
      'cStat=cStat'
      'ide_nNF=ide_nNF'
      'ide_serie=ide_serie'
      'ide_AAMM_AA=ide_AAMM_AA'
      'ide_AAMM_MM=ide_AAMM_MM'
      'ide_dEmi=ide_dEmi'
      'NOME_tpEmis=NOME_tpEmis'
      'NOME_tpNF=NOME_tpNF'
      'Status=Status'
      'Motivo=Motivo'
      'NOME_ORDEM=NOME_ORDEM')
    DataSet = QrNFe_XXX
    BCDToCurrency = False
    DataSetOptions = []
    Left = 236
    Top = 424
  end
  object QrNFe_101: TMySQLQuery
    Database = Dmod.ZZDB
    SQL.Strings = (
      'SELECT * FROM nfe_101'
      'ORDER BY ide_nNF')
    Left = 208
    Top = 396
    object QrNFe_101Ordem: TIntegerField
      FieldName = 'Ordem'
    end
    object QrNFe_101ide_nNF: TIntegerField
      FieldName = 'ide_nNF'
    end
    object QrNFe_101ide_serie: TIntegerField
      FieldName = 'ide_serie'
    end
    object QrNFe_101ide_AAMM_AA: TWideStringField
      FieldName = 'ide_AAMM_AA'
      Size = 2
    end
    object QrNFe_101ide_AAMM_MM: TWideStringField
      FieldName = 'ide_AAMM_MM'
      Size = 2
    end
    object QrNFe_101ide_dEmi: TDateField
      FieldName = 'ide_dEmi'
    end
    object QrNFe_101NOME_tpEmis: TWideStringField
      FieldName = 'NOME_tpEmis'
      Size = 30
    end
    object QrNFe_101NOME_tpNF: TWideStringField
      FieldName = 'NOME_tpNF'
      Size = 1
    end
    object QrNFe_101Id: TWideStringField
      FieldName = 'Id'
      Size = 44
    end
    object QrNFe_101infCanc_dhRecbto: TDateTimeField
      FieldName = 'infCanc_dhRecbto'
    end
    object QrNFe_101infCanc_nProt: TWideStringField
      FieldName = 'infCanc_nProt'
      Size = 15
    end
    object QrNFe_101Motivo: TWideStringField
      FieldName = 'Motivo'
      Size = 255
    end
    object QrNFe_101Id_TXT: TWideStringField
      DisplayWidth = 100
      FieldKind = fkCalculated
      FieldName = 'Id_TXT'
      Size = 100
      Calculated = True
    end
  end
  object frxDsNFe_101: TfrxDBDataset
    UserName = 'frxDsNFe_101'
    CloseDataSource = False
    DataSet = QrNFe_101
    BCDToCurrency = False
    DataSetOptions = []
    Left = 236
    Top = 396
  end
  object QrIntermediario: TMySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrIntermediarioBeforeClose
    SQL.Strings = (
      'SELECT Codigo, Tipo, CNPJ, CPF,'
      'IF(Tipo=0, RazaoSocial, Nome) NO_Enti'
      'FROM Entidades'
      'WHERE Cliente1="V"'
      'OR Cliente2="V"'
      'OR Cliente3="V"'
      'OR Cliente4="V"'
      'ORDER BY NO_Enti')
    Left = 676
    Top = 12
    object QrIntermediarioCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrIntermediarioNO_Enti: TWideStringField
      FieldName = 'NO_Enti'
      Required = True
      Size = 100
    end
    object QrIntermediarioCNPJ: TWideStringField
      FieldName = 'CNPJ'
      Size = 18
    end
    object QrIntermediarioCPF: TWideStringField
      FieldName = 'CPF'
      Size = 18
    end
    object QrIntermediarioTipo: TSmallintField
      FieldName = 'Tipo'
    end
  end
  object DsIntermediario: TDataSource
    DataSet = QrIntermediario
    Left = 676
    Top = 60
  end
  object CSTabSheetChamou: TdmkCompoStore
    Left = 568
    Top = 312
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 596
    Top = 312
  end
  object PMEnvia: TPopupMenu
    OnPopup = PMEnviaPopup
    Left = 120
    Top = 424
    object EnviarNFSe1: TMenuItem
      Caption = '&Enviar NFS-e por e-mail'
      OnClick = EnviarNFSe1Click
    end
    object N3: TMenuItem
      Caption = '-'
    end
    object EnviarNFSeporemailEnvioprotocolado1: TMenuItem
      Caption = 'Enviar NFS-e por e-mail (&Envio protocolado)'
      OnClick = EnviarNFSeporemailEnvioprotocolado1Click
    end
  end
end
