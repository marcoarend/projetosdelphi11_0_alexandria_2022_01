unit NFSeMail;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkDBGridZTO, mySQLDbTables,
  DmkDAC_PF, dmkPermissoes, dmkCompoStore, Vcl.Menus, UnDmkProcFunc,
  UnGrl_Geral, UnGrl_Vars, UnProjGroup_Consts;

type
  TFmNFSeMail = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    LaTitulo1C: TLabel;
    PnMenu: TPanel;
    BtPEBProtocolo: TBitBtn;
    BtPEBTodos: TBitBtn;
    QrProt1: TmySQLQuery;
    QrProt1Codigo: TIntegerField;
    QrProt1Nome: TWideStringField;
    QrProt1PreEmeio: TIntegerField;
    QrProt1NaoEnvBloq: TIntegerField;
    DsProt1: TDataSource;
    BtProPad: TBitBtn;
    QrProt1Tarefa: TIntegerField;
    QrProtocoNFSe: TmySQLQuery;
    QrProtocoNFSeNOMEENT: TWideStringField;
    QrProtocoNFSePROTOCOLO: TIntegerField;
    QrProtocoNFSeTAREFA: TWideStringField;
    QrProtocoNFSeLOTE: TIntegerField;
    QrProtocoNFSeEmail: TWideStringField;
    QrProtocoNFSeDataE_Txt: TWideStringField;
    QrProtocoNFSeDataD_Txt: TWideStringField;
    DsProtocoNFSe: TDataSource;
    QrProtocoNFSeCodigo: TIntegerField;
    QrProtocoNFSeRpsIDTipo: TSmallintField;
    QrProtocoNFSeRpsIDSerie: TWideStringField;
    QrProtocoNFSeRpsIDNumero: TIntegerField;
    QrProtocoNFSeRpsDataEmissao: TDateField;
    QrProtocoNFSeRpsHoraEmissao: TTimeField;
    QrProtocoNFSeValorServicos: TFloatField;
    CSTabSheetChamou: TdmkCompoStore;
    dmkPermissoes1: TdmkPermissoes;
    BtEnvia: TBitBtn;
    PMProtocolo: TPopupMenu;
    Gera1: TMenuItem;
    Todosabertos1: TMenuItem;
    Abertosseleciondos1: TMenuItem;
    Desfazprotocolo1: TMenuItem;
    MenuItem1: TMenuItem;
    Localizaprotocolo1: TMenuItem;
    QrProtocoNFSeProtocoPak: TIntegerField;
    PB1: TProgressBar;
    QrProtocoNFSeDataD: TDateField;
    QrProtocoNFSeEMeio_ID: TFloatField;
    QrProtocoNFSeAmbiente: TSmallintField;
    QrProtocoNFSePROTOCOD: TIntegerField;
    QrProtocoNFSeTipoProt: TIntegerField;
    QrProtocoNFSeEntidade: TIntegerField;
    QrProtocoNFSeNfsNumero: TLargeintField;
    LaTotal: TLabel;
    BtPEBNenhum: TBitBtn;
    QrNFSeCodVerifi: TmySQLQuery;
    QrNFSeCodVerifiNfsCodigoVerificacao: TWideStringField;
    QrProtocoNFSeStatus: TIntegerField;
    QrProtocoNFSePrestaCnpj: TWideStringField;
    QrProtocoNFSePrestaCpf: TWideStringField;
    QrProtocoNFSePrestaInscricaoMunicipal: TWideStringField;
    QrProtocoNFSeTomaRazaoSocial: TWideStringField;
    BtEntidades: TBitBtn;
    TBProtocolos: TTabControl;
    DBGrid1: TDBGrid;
    DBG_ProtocoNFSe: TdmkDBGridZTO;
    PMEnvia: TPopupMenu;
    Selecionados1: TMenuItem;
    SelecionadoNOenviados1: TMenuItem;
    QrProtocoNFSeNOMEENT_MAIL: TWideStringField;
    N1: TMenuItem;
    SelecionadoporWhatsApp1: TMenuItem;
    N2: TMenuItem;
    Abrircadastrodocliente1: TMenuItem;
    QrProtocoNFSeId_Contato: TIntegerField;
    QrProtocoNFSeTelefone: TWideStringField;
    QrProtocoNFSeContato: TWideStringField;
    QrProtocoNFSeTelefone_TXT: TWideStringField;
    QrProtocoNFSeNfsCodigoVerificacao: TWideStringField;
    N3: TMenuItem;
    Visualizaconfirmaoderecebimentodanotafiscalatual1: TMenuItem;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure BtProPadClick(Sender: TObject);
    procedure QrProt1AfterScroll(DataSet: TDataSet);
    procedure QrProt1BeforeClose(DataSet: TDataSet);
    procedure BtPEBNenhumClick(Sender: TObject);
    procedure BtPEBTodosClick(Sender: TObject);
    procedure BtPEBProtocoloClick(Sender: TObject);
    procedure PMProtocoloPopup(Sender: TObject);
    procedure Localizaprotocolo1Click(Sender: TObject);
    procedure Desfazprotocolo1Click(Sender: TObject);
    procedure Todosabertos1Click(Sender: TObject);
    procedure Abertosseleciondos1Click(Sender: TObject);
    procedure QrProtocoNFSeAfterScroll(DataSet: TDataSet);
    procedure QrProtocoNFSeBeforeClose(DataSet: TDataSet);
    procedure BtEnviaClick(Sender: TObject);
    procedure BtEntidadesClick(Sender: TObject);
    procedure TBProtocolosChange(Sender: TObject);
    procedure SelecionadoNOenviados1Click(Sender: TObject);
    procedure Selecionados1Click(Sender: TObject);
    procedure Abrircadastrodocliente1Click(Sender: TObject);
    procedure SelecionadoporWhatsApp1Click(Sender: TObject);
    procedure QrProtocoNFSeCalcFields(DataSet: TDataSet);
  private
    { Private declarations }
    function  ObtemProtocoloDeEnvio(CliInt: Integer; MostraMsg: Boolean): Integer;
    function  EnviaNFSeEmail(NaoEnviados, TemDBWeb: Boolean): Boolean;
    function  ObtemChaveNFSe(Empresa, Ambiente, NumeroNFSE: Integer; SerieNFSE: String): String;
    procedure EnviaEmails(NaoEnviados: Boolean);
    procedure ReopenProt1(CliInt: Integer);
    procedure ReopenProtocoNFSe;
    procedure SelecinarTodos(Selecionar: Boolean; Query: TmySQLQuery;
              Grid: TdmkDBGridZTO);
  public
    { Public declarations }
    FCliInt, FNFSeFatCab, FNFSeDPSCabCod: Integer;
    FAvulso: Boolean;
    procedure ReopenQuerys(Tab: Integer);
  end;

  var
  FmNFSeMail: TFmNFSeMail;

implementation

uses {$IfNDef SemProtocolo} Protocolo, UnProtocoUnit, {$EndIf}
  UnMyObjects, Module, MyGlyfs, Principal, UMySQLModule, ModuleGeral,
  {$IfDef TEM_DBWEB} UnDmkWeb2, UnDmkWeb2_Jan, {$EndIf}
  NFSe_0000_Module, UnMailEnv, MyListas, UnEntities, UnitDmkTags, UnDmkWeb;

{$R *.DFM}

procedure TFmNFSeMail.Abertosseleciondos1Click(Sender: TObject);
begin
  {$IfNDef SemProtocolo}
    try
      DBGrid1.Enabled         := False;
      DBG_ProtocoNFSe.Enabled := False;
      BtPEBProtocolo.Enabled  := False;
      //
      UnProtocolo.GeraProtocoloNFSe(istSelecionados, QrProtocoNFSe,
        Dmod.MyDB, TDBGrid(DBG_ProtocoNFSe), PB1, FCliInt);
      //
      ReopenProtocoNFSe;
    finally
      DBGrid1.Enabled         := True;
      DBG_ProtocoNFSe.Enabled := True;
      BtPEBProtocolo.Enabled  := True;
    end;
  {$Else}
    Grl_Geral.InfoSemModulo(mdlappProtocolos);
  {$EndIf}
end;

procedure TFmNFSeMail.Abrircadastrodocliente1Click(Sender: TObject);
begin
  if (QrProtocoNFSe.State = dsInactive) or (QrProtocoNFSe.RecordCount = 0) then Exit;
  //
  DModG.CadastroDeEntidade(QrProtocoNFSeEntidade.Value, fmcadEntidade2,
    fmcadEntidade2, False, False, nil, nil, False, uetNenhum, 1,
    Trunc(QrProtocoNFSeId_Contato.Value));
  //
  ReopenProtocoNFSe;
end;

procedure TFmNFSeMail.BtEntidadesClick(Sender: TObject);
var
  Entidade: Integer;
begin
  if (QrProtocoNFSe.State <> dsInactive) and (QrProtocoNFSe.RecordCount > 0) then
    Entidade := QrProtocoNFSeEntidade.Value
  else
    Entidade := 0;
  //
  DModG.CadastroDeEntidade(Entidade, fmcadEntidade2, fmcadEntidade2, True);
  //
  ReopenProtocoNFSe;
end;

procedure TFmNFSeMail.BtEnviaClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMEnvia, BtEnvia);
end;

procedure TFmNFSeMail.BtPEBNenhumClick(Sender: TObject);
begin
  if (QrProtocoNFSe.State <> dsInactive) and (QrProtocoNFSe.RecordCount > 0) then
  begin
    SelecinarTodos(False, QrProtocoNFSe, DBG_ProtocoNFSe);
  end;
end;

procedure TFmNFSeMail.BtPEBProtocoloClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMProtocolo, BtPEBProtocolo);
end;

procedure TFmNFSeMail.BtPEBTodosClick(Sender: TObject);
begin
  if (QrProtocoNFSe.State <> dsInactive) and (QrProtocoNFSe.RecordCount > 0) then
  begin
    SelecinarTodos(True, QrProtocoNFSe, DBG_ProtocoNFSe);
  end;
end;

procedure TFmNFSeMail.BtProPadClick(Sender: TObject);
{$IfNDef SemProtocolo}
var
  Tarefa: Integer;
begin
  if (QrProt1.State <> dsInactive) and (QrProt1.RecordCount > 0) then
    Tarefa := QrProt1Codigo.Value
  else
    Tarefa := 0;
  ProtocoUnit.MostraProEnPr(Tarefa);
{$Else}
begin
    Grl_Geral.InfoSemModulo(mdlappProtocolos);
{$EndIf}
end;

procedure TFmNFSeMail.BtSaidaClick(Sender: TObject);
begin
  if TFmNFSeMail(Self).Owner is TApplication then
    Close
  else
    MyObjects.FormTDIFecha(Self, TTabSheet(CSTabSheetChamou.Component));
end;

procedure TFmNFSeMail.Desfazprotocolo1Click(Sender: TObject);
{$IfNDef SemProtocolo}
  procedure DesfazProtocolo();
  var
    Query: TmySQLQuery;
  begin
    Screen.Cursor  := crHourGlass;
    Query          := TmySQLQuery.Create(TDataModule(Dmod.MyDB.Owner));
    Query.Database := Dmod.MyDB;
    //
    try
      if (QrProtocoNFSeDataD_Txt.Value = '') then //N�o tem retorno
      begin
        UMyMod.ExcluiRegistroInt1('', 'protpakits', 'Conta',
          QrProtocoNFSePROTOCOLO.Value, Dmod.MyDB);
        //
        UnProtocolo.ExcluiLoteProtocoloNFSe(Query, Dmod.QrUpd, Dmod.MyDB,
          FCliInt, QrProtocoNFSeAmbiente.Value, Trunc(QrProtocoNFSeEMeio_ID.Value),
          QrProtocoNFSeProtocoPak.Value, QrProtocoNFSeRpsIDNumero.Value,
          QrProtocoNFSeRpsIDSerie.Value);
      end;
    finally
      Query.Free;
      //
      Screen.Cursor := crDefault;
    end;
  end;
var
  i: Integer;
begin
  if Geral.MB_Pergunta('Deseja desfazer o protocolo dos itens selecionados?') <> ID_YES then Exit;
  try
    DBGrid1.Enabled         := False;
    DBG_ProtocoNFSe.Enabled := False;
    BtPEBProtocolo.Enabled  := False;
    //
    if DBG_ProtocoNFSe.SelectedRows.Count > 1 then
    begin
      PB1.Position := 0;
      PB1.Max      := DBG_ProtocoNFSe.SelectedRows.Count;
      //
      with DBG_ProtocoNFSe.DataSource.DataSet do
      begin
        for i:= 0 to DBG_ProtocoNFSe.SelectedRows.Count-1 do
        begin
          PB1.Position := PB1.Position + 1;
          PB1.Update;
          Application.ProcessMessages;
          //
          GotoBookmark(DBG_ProtocoNFSe.SelectedRows.Items[i]);
          //
          DesfazProtocolo();
        end;
      end;
      PB1.Position := 0;
    end else
      DesfazProtocolo();
    //
    ReopenProtocoNFSe;
  finally
    DBGrid1.Enabled         := True;
    DBG_ProtocoNFSe.Enabled := True;
    BtPEBProtocolo.Enabled  := True;
  end;
{$Else}
begin
  Grl_Geral.InfoSemModulo(mdlappProtocolos);
{$EndIf}
end;

function TFmNFSeMail.EnviaNFSeEmail(NaoEnviados, TemDBWeb: Boolean): Boolean;
var
  DANFE, XML, ProtocolImp, Protocol, Email, Telefone, Web_Id: String;
  Cliente, Protocolo: Integer;
  Finalid: TProtFinalidade;
begin
  Result := False;
  //
  if (QrProtocoNFSePROTOCOLO.Value <> 0) then //Tem protocolo
  begin
    if QrProt1NaoEnvBloq.Value = 0 then //Envia anexo
    begin
      if QrProtocoNFSeStatus.Value = 100 then //Autorizada
      begin
        // DANFE
        DANFE := DmNFSe_0000.ExportaPDFNFSe_PorNFS(FCliInt,
                 QrProtocoNFSeAmbiente.Value, QrProtocoNFSeRpsIDNumero.Value);
        if DANFE = '' then
        begin
          Geral.MensagemBox('Falha ao exportar DANFSe!', 'Aviso', MB_OK+MB_ICONWARNING);
          Exit;
        end;
      end;
      XML := DmNFSe_0000.ExportaXMLNFSe_PorRps(QrProtocoNFSePrestaCnpj.Value,
               QrProtocoNFSePrestaCpf.Value, QrProtocoNFSePrestaInscricaoMunicipal.Value,
               QrProtocoNFSeRpsIDNumero.Value, QrProtocoNFSeRpsIDTipo.Value,
               QrProtocoNFSeRpsIDSerie.Value, False);
      if XML = '' then
      begin
        Geral.MensagemBox('Falha ao exportar XML NFS-e!', 'Aviso', MB_OK+MB_ICONWARNING);
        Exit;
      end;
    end;
    //
    if (NaoEnviados = True) and (QrProtocoNFSeDataE_Txt.Value <> '') then
    begin
      Result := True;
    end else
    begin
      if (QrProtocoNFSeDataE_Txt.Value = '') and (TemDBWeb = True) then
      begin
        {$IfDef TEM_DBWEB}
          if not UnProtocolo.Proto_Email_CriaProtocoloEnvio(Dmod.MyDBn,
            QrProtocoNFSeEntidade.Value, QrProtocoNFSePROTOCOLO.Value,
            QrProtocoNFSeEmail.Value)
          then
            Exit;
        {$Else}
          Exit;
        {$EndIf}
      end;
      Web_Id    := DmodG.QrWebParams.FieldByName('Web_Id').AsString;
      Email     := QrProtocoNFSeEmail.Value;
      Telefone  := QrProtocoNFSeTelefone.Value;
      Cliente   := QrProtocoNFSeEntidade.Value;
      Protocolo := QrProtocoNFSePROTOCOLO.Value;
      //
      if QrProtocoNFSeStatus.Value = 100 then //Autorizada
        Finalid := ptkNFSeAut
      else
        Finalid := ptkNFSeCan;
      //
      UnProtocolo.Proto_Email_GeraLinksConfirmacaoReceb2(Dmod.MyDB, Web_Id,
        Email, Telefone, Cliente, Protocolo, CO_DMKID_APP, Finalid, ProtocolImp,
        Protocol);
      //
      if UMailEnv.Monta_e_Envia_Mail([QrProtocoNFSeRpsIDNumero.Value,
        FCliInt, QrProtocoNFSeAmbiente.Value, QrProtocoNFSeStatus.Value,
        QrProtocoNFSeEmail.Value], meNFSe, [DANFE, XML],
        [QrProtocoNFSeNOMEENT_MAIL.Value, QrProtocoNFSENfsCodigoVerificacao.Value,
        QrProtocoNFSeRpsIDSerie.Value, Geral.FF0(QrProtocoNFseNfsNumero.Value),
        ProtocolImp, Protocol], False) then
      begin
        if UnProtocolo.Proto_Email_AtualizDataE(Dmod.MyDB,
          QrProtocoNFSePROTOCOLO.Value, DModG.ObtemAgora)
        then
          Result := True
        else
          Exit;
      end;
    end;
  end;
end;

procedure TFmNFSeMail.FormActivate(Sender: TObject);
begin
  if TFmNFSeMail(Self).Owner is TApplication then
  begin
    MyObjects.CorIniComponente();
  end;
end;

procedure TFmNFSeMail.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  PB1.Position := 0;
  DBG_ProtocoNFSe.PopupMenu := PMEnvia;
end;

procedure TFmNFSeMail.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
    [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmNFSeMail.FormShow(Sender: TObject);
begin
{$IfNDef cSkinRank} //Berlin
{$IfNDef cAlphaSkin} //Berlin
  FmMyGlyfs.DefineGlyfs(TForm(Sender));
{$EndIf}
{$EndIf}
{$IfDef cSkinRank} //Berlin
  if FmPrincipal.Sd1.Active then
    FmMyGlyfs.DefineGlyfsTDI(FmPrincipal.sd1, Sender);
{$EndIf}
{$IfDef cAlphaSkin} //Berlin
  if FmPrincipal.sSkinManager1.Active then
    FmMyGlyfs.DefineGlyfsTDI2(FmPrincipal.sSkinManager1, Sender);
{$EndIf}
end;

procedure TFmNFSeMail.Localizaprotocolo1Click(Sender: TObject);
{$IfNDef SemProtocolo}
var
  Lote: Integer;
begin
  Lote := QrProtocoNFSeProtocoPak.Value;
  //
  if Lote = 0 then
    Geral.MB_Aviso('Lote n�o localizado!')
  else
    ProtocoUnit.MostraFormProtocolos(Lote, 0);
  //
  ReopenProtocoNFSe;
{$Else}
begin
  dmkPF.InfoSemModulo(mdlappProtocolos);
{$EndIf}
end;

function TFmNFSeMail.ObtemChaveNFSe(Empresa, Ambiente, NumeroNFSE: Integer;
  SerieNFSE: String): String;
begin
  QrNFSeCodVerifi.Close;
  QrNFSeCodVerifi.Params[0].AsInteger := Empresa;
  QrNFSeCodVerifi.Params[1].AsInteger := Ambiente;
  QrNFSeCodVerifi.Params[2].AsInteger := NumeroNFSE;
  QrNFSeCodVerifi.Params[3].AsString  := SerieNFSE;
  UMyMod.AbreQuery(QrNFSeCodVerifi, DMod.MyDB);
  if QrNFSeCodVerifi.RecordCount > 0 then
    Result := QrNFSeCodVerifiNfsCodigoVerificacao.Value
  else
    Result := '';
end;

function TFmNFSeMail.ObtemProtocoloDeEnvio(CliInt: Integer; MostraMsg: Boolean): Integer;
var
  Finalidade: TProtFinalidade;
  Res: Integer;
begin
  if MostraMsg then
    Res := Geral.MB_Pergunta('N�o foi localizado nenhum protocolo padr�o!' +
             sLineBreak + 'Deseja cadastrar agora?')
  else
    Res := ID_YES;
  //
  if Res = ID_YES then
  begin
    if TBProtocolos.TabIndex = 0 then
      Finalidade := ptkNFSeAut
    else
      Finalidade := ptkNFSeCan;
    //
    ProtocoUnit.MostraProtoAddTar(Finalidade,
      Geral.FF0(VAR_TIPO_PROTOCOLO_CE_02), nil, nil, CliInt);
  end;
  Result := Res;
end;

procedure TFmNFSeMail.PMProtocoloPopup(Sender: TObject);
var
  Enab: Boolean;
begin
  Enab := (QrProtocoNFSe.State <> dsInactive) and (QrProtocoNFSe.RecordCount > 0);
  //
  Gera1.Enabled                                             := Enab;
  Desfazprotocolo1.Enabled                                  := Enab;
  Localizaprotocolo1.Enabled                                := Enab;
  Visualizaconfirmaoderecebimentodanotafiscalatual1.Enabled := Enab;
end;

procedure TFmNFSeMail.QrProt1AfterScroll(DataSet: TDataSet);
begin
  ReopenProtocoNFSe;
end;

procedure TFmNFSeMail.QrProt1BeforeClose(DataSet: TDataSet);
begin
  QrProtocoNFSe.Close;
end;

procedure TFmNFSeMail.QrProtocoNFSeAfterScroll(DataSet: TDataSet);
begin
  LaTotal.Caption := 'Total de itens: ' + Geral.FF0(QrProtocoNFSe.RecordCount);
end;

procedure TFmNFSeMail.QrProtocoNFSeBeforeClose(DataSet: TDataSet);
begin
  LaTotal.Caption := '';
end;

procedure TFmNFSeMail.QrProtocoNFSeCalcFields(DataSet: TDataSet);
begin
  QrProtocoNFSeTelefone_TXT.Value := Geral.FormataTelefone_TT_Curto(QrProtocoNFSeTelefone.Value);
end;

procedure TFmNFSeMail.ReopenProt1(CliInt: Integer);
var
  Finalidade: Integer;
begin
  if TBProtocolos.TabIndex = 0 then
    Finalidade := 1
  else
    Finalidade := 2;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrProt1, Dmod.MyDB, [
    'SELECT pep.Codigo, pro.Codigo Tarefa, pro.Nome, pro.PreEmeio, mai.NaoEnvBloq ',
    'FROM proenpr pep ',
    'LEFT JOIN protocolos pro ON pro.Codigo = pep.Protocolo ',
    'LEFT JOIN preemail mai ON mai.Codigo = pro.PreEmeio ',
    'WHERE pep.Finalidade=' + Geral.FF0(Finalidade), //NFSe
    'AND pro.Def_Client=' + Geral.FF0(CliInt),
    'AND pro.Tipo=2 ', //Boletos
    '']);
end;

procedure TFmNFSeMail.ReopenProtocoNFSe;
var
  Tarefa, TipoEmail, TipoTel: Integer;
  SQLStatus, SQLCompl: String;
begin
  DModG.ReopenParamsEmp(FCliInt);
  //
  TipoTel   := DmodG.QrParamsEmp.FieldByName('NFSe_WhatsApp_EntiTipCto').AsInteger;
  TipoEmail := DmodG.QrParamsEmpNFSeTipCtoMail.Value;
  Tarefa    := QrProt1Tarefa.Value;
  //
  if MyObjects.FIC(TipoEmail = 0, nil, 'Tipo de e-mail para envio de NFS-es n�o definido nos par�metros das filiais!') then
    Exit;
  //
  if (FNFSeDPSCabCod <> 0) and (FAvulso = True) then
    SQLCompl := 'AND dps.Codigo=' + Geral.FF0(FNFSeDPSCabCod)
  else
    SQLCompl := 'AND dps.NFSeFatCab=' + Geral.FF0(FNFSeFatCab);
  //
  if TBProtocolos.TabIndex = 0 then
    SQLStatus := 'AND dps.Status = 100'
  else
    SQLStatus := 'AND dps.Status <> 0 AND dps.Status <> 100';
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrProtocoNFSe, Dmod.MyDB, [
    'SELECT dps.Codigo, dps.Status, dps.RpsIDTipo, dps.RpsIDSerie, nfs.NfsNumero, ',
    'dps.RpsIDNumero, dps.RpsDataEmissao, ptc.Tipo TipoProt, ',
    'dps.RpsHoraEmissao, dps.ValorServicos, ent.Codigo Entidade, ',
    'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENT, ',
    'IF(IF(ent.Tipo = 0, ent.Fantasia, ent.Apelido) <> "", ',
    'IF(ent.Tipo = 0, ent.Fantasia, ent.Apelido), ',
    'IF(ent.Tipo = 0, ent.RazaoSocial, ent.Nome)) NOMEENT_MAIL, ',
    'ppi.Controle LOTE, ppi.Conta PROTOCOLO, ',
    'IF(ppi.DataE <= "1899-12-30", "", DATE_FORMAT(ppi.DataE, "%d/%m/%Y")) DataE_Txt, ',
    'IF(ppi.DataD <= "1899-12-30", "", DATE_FORMAT(ppi.DataD, "%d/%m/%Y %H:%i")) DataD_Txt, ',
    'enm.Email, ptc.Nome TAREFA, ptc.Codigo PROTOCOD, ppi.Controle ProtocoPak, ppi.DataD, ',
    'enm.Conta + 0.000 EMeio_ID, dps.Ambiente, dps.PrestaCnpj, dps.PrestaCpf, ',
    'dps.PrestaInscricaoMunicipal, dps.TomaRazaoSocial, ppi.ID_Cod1, ppi.ID_Cod2, ',
    'ppi.ID_Cod3, ppi.ID_Cod4, ppi.Docum, eco.Controle Id_Contato, ',
    'IF(tel.EntiTipCto = 0, "", tel.Telefone) Telefone, eco.Nome Contato, ',
    'eco.Controle + 0.000 Id_Contato, nfs.NfsCodigoVerificacao, ',
    'IF(tel.EntiTipCto = 0, "", tel.Telefone) Telefone, eco.Nome Contato ',
    'FROM entimail enm ',
    'LEFT JOIN enticontat eco ON eco.Controle = enm.Controle ',
    'LEFT JOIN enticonent ece ON ece.Controle = eco.Controle ',
    'LEFT JOIN entitel tel ON (tel.Controle=enm.Controle AND tel.EntiTipCto='+ Geral.FF0(TipoTel) +') ',
    'LEFT JOIN entidades ent ON ent.Codigo = ece.Codigo ',
    'LEFT JOIN nfsedpscab dps ON dps.Cliente = ent.Codigo ',
    'LEFT JOIN nfsenfscab nfs ON nfs.Empresa = dps.Empresa ',
    'AND nfs.Ambiente = dps.Ambiente ',
    'AND nfs.NfsRpsIDSerie = dps.RpsIDSerie ',
    'AND nfs.NfsRpsIDTipo = dps.RpsIDTipo  ',
    'AND nfs.NfsRpsIDNumero = dps.RpsIDNumero ',
    'LEFT JOIN protpakits ppi ON ppi.CliInt = dps.Empresa ',
    'AND ppi.ID_Cod1 = dps.Ambiente ',
    'AND ppi.ID_Cod2 = dps.RpsIDSerie ',
    'AND ppi.ID_Cod3 = dps.RpsIDNumero ',
    'AND ppi.ID_Cod4 = enm.Conta ',
    'LEFT JOIN protocolos ptc ON ptc.Codigo=' + Geral.FF0(Tarefa),
    'WHERE enm.EntiTipCto=' + Geral.FF0(TipoEmail),
    SQLStatus,
    SQLCompl,
    'GROUP BY tel.Controle, dps.RpsIDSerie, dps.RpsIDNumero, ent.Codigo, enm.Email ',
    'ORDER BY NOMEENT, dps.RpsIDSerie, dps.RpsIDNumero, PROTOCOLO, enm.Email ',
    '']);
  if FNFSeDPSCabCod <> 0 then
    QrProtocoNFSe.Locate('Codigo', FNFSeDPSCabCod, []);
end;

procedure TFmNFSeMail.ReopenQuerys(Tab: Integer);
begin
  if Tab = -1 then
    TBProtocolos.TabIndex := 0;
  //
  ReopenProt1(FCliInt);
  //
  if QrProt1.RecordCount = 0 then
  begin
    ObtemProtocoloDeEnvio(FCliInt, True);
    ReopenProt1(FCliInt);
  end;
end;

procedure TFmNFSeMail.SelecinarTodos(Selecionar: Boolean; Query: TmySQLQuery;
  Grid: TdmkDBGridZTO);
begin
  if (Query.State = dsInactive) or (Query.RecordCount = 0) then
    Exit;
  //
  Screen.Cursor := crHourGlass;
  try
    Grid.Enabled := False;
    //
    Query.DisableControls;
    //
    Query.First;
    while not Query.Eof do
    begin
      Grid.SelectedRows.CurrentRowSelected := Selecionar;
      Query.Next;
    end;
  finally
    Query.EnableControls;
    //
    Grid.Enabled  := True;
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmNFSeMail.SelecionadoNOenviados1Click(Sender: TObject);
begin
  EnviaEmails(True);
end;

procedure TFmNFSeMail.SelecionadoporWhatsApp1Click(Sender: TObject);
var
  WhatsApp_EntiTipCto, Protocolo, Cliente, Id_Contato: Integer;
  WhatsApp_Msg, Web_Id, Email, ProtocolImp, Protocol, Telefone: String;
  Finalid: TProtFinalidade;
begin
  if QrProtocoNFSeDataE_Txt.Value <> '' then
  begin
    if Geral.MB_Pergunta('O controle de envio de notas fiscais atrav�s do WhatsApp dever� ser realizado de forma manual!' +
      sLineBreak + 'Caso voc� N�O queira controlar desta forma N�O utilize este recurso!!!' + sLineBreak +
      'Antes de enviar certifique-se que a base de dados WEB esteja sincronizada!' + sLineBreak + sLineBreak +
      'Deseja continuar?' + sLineBreak + 'Ao continuar voc� estar� concordando com os termos descritos acima!') <> ID_YES then Exit;
    //
    DmodG.ReopenParamsEmp(DmodG.QrFiliLogCodigo.Value);
    //
    WhatsApp_EntiTipCto := DmodG.QrParamsEmp.FieldByName('Nfse_WhatsApp_EntiTipCto').AsInteger;
    WhatsApp_Msg        := DmodG.QrParamsEmp.FieldByName('Nfse_WhatsApp_Msg').AsString;
    //
    if MyObjects.FIC(WhatsApp_EntiTipCto = 0, nil, 'Tipo de telefone para envio de boleto n�o definido nas op��es de faturas!') then Exit;
    if MyObjects.FIC(WhatsApp_Msg = '', nil, 'Mensagem para envio de mensagens por WhatsApp n�o definido nas op��es de faturas!') then Exit;
    //
    Email      := QrProtocoNFSeEmail.Value;
    Cliente    := QrProtocoNFSeEntidade.Value;
    Protocolo  := QrProtocoNFSePROTOCOLO.Value;
    Web_Id     := DmodG.QrWebParams.FieldByName('Web_Id').AsString;
    Id_Contato := Trunc(QrProtocoNFSeId_Contato.Value);
    Telefone   := Entities.WhatsApp_ObtemTelefone(Dmod.MyDB, WhatsApp_EntiTipCto, Id_Contato);
    //
    if Telefone = '' then
    begin
      if Geral.MB_Pergunta('Nenhum telefone WhatsApp foi definido!' + sLineBreak + 'Deseja definir agora?') = ID_YES then
      begin
        DModG.CadastroDeEntidade(QrProtocoNFSeEntidade.Value, fmcadEntidade2, fmcadEntidade2, True);
        ReopenProtocoNFSe;
      end;
      Exit;
    end;
    if QrProtocoNFSeStatus.Value = 100 then //Autorizada
      Finalid := ptkNFSeAut
    else
      Finalid := ptkNFSeCan;
    //
    UnProtocolo.Proto_Email_GeraLinksConfirmacaoReceb2(Dmod.MyDB, Web_Id, Email,
      Telefone, Cliente, Protocolo, CO_DMKID_APP, Finalid, ProtocolImp, Protocol);
    //
    UnDmkTags.SubstituiVariaveis_NFSe(DModG.ObtemNomeFantasiaEmpresa(),
      QrProtocoNFSENfsCodigoVerificacao.Value, '', QrProtocoNFSeNOMEENT_MAIL.Value,
      QrProtocoNFSeRpsIDSerie.Value, Geral.FF0(QrProtocoNFseNfsNumero.Value),
      ProtocolImp, '', WhatsApp_Msg);
    //
    dmkWeb.WhatsApp_EnviaMensagem(WhatsApp_Msg, Telefone);
  end else
    Geral.MB_Aviso('Antes de enviar atrav�s do WhatsApp voc� deve enviar por e-mail!');
end;

procedure TFmNFSeMail.Selecionados1Click(Sender: TObject);
begin
  EnviaEmails(False);
end;

procedure TFmNFSeMail.EnviaEmails(NaoEnviados: Boolean);
{$IfNDef SemProtocolo}
{$IfDef TEM_DBWEB}
var
  TemDBWeb, ModulHabil, Res: Boolean;
  I, N: Integer;
  Msg: String;
begin
  Res      := False;
  TemDBWeb := DmkWeb2.ConexaoRemota(Dmod.MyDBn, DmodG.QrWebParams, 1);
  //
  if (QrProtocoNFSe.State = dsInactive) or (QrProtocoNFSe.RecordCount = 0) then Exit;
  //
  if not DmkWeb.RemoteConnection then
  begin
    Geral.MB_Aviso('Seu computador n�o est� conectado na internet!' +
      sLineBreak + 'Estabele�a uma conex�o com a internet e tente novamente!');
    Exit;
  end;
  // necess�rio para iserir no servidor protocolos de emeios enviados
  TemDBWeb   := DmkWeb2.ConexaoRemota(Dmod.MyDBn, DmodG.QrWebParams, 1);
  ModulHabil := DmkWeb2.VerificaSeModuloWebEstaAtivo(CO_DMKID_APP, DModG.QrMasterHabilModulos.Value, Msg);
  //
  Res := False;
  //
  if TemDBWeb and ModulHabil then
  begin
    if not DmkWeb2_Jan.MostraWSincro(CO_DMKID_APP, CO_Versao,
      DModG.QrMasterHabilModulos.Value, DmodG.QrWebParams, Dmod.MyDB, Dmod.MyDBn,
      ['entimail', 'enticontat', 'enticonent', 'entidades', 'nfsedpscab',
      'nfsenfscab', 'cnab_cfg', 'protpakits'],[], VAR_VERIFI_DB_CANCEL) then
    begin
      if Geral.MB_Pergunta('Voc� deve sincronizar os dados antes de enviar as Notas Fiscais!' +
        sLineBreak + 'Tem certeza que deseja continuar?') <> ID_YES then
      Exit;
    end;
  end;
  //
  try
    QrProtocoNFSe.DisableControls;
    //
    DBGrid1.Enabled         := False;
    DBG_ProtocoNFSe.Enabled := False;
    PnMenu.Enabled          := False;
    BtEnvia.Enabled         := False;
    N                       := DBG_ProtocoNFSe.SelectedRows.Count;
    //
    if N < 2 then
    begin
      if QrProtocoNFSeRpsIDNumero.Value = 0 then Exit;
      //
      Res := EnviaNFSeEmail(NaoEnviados, TemDBWeb);
      //
      if not Res then
      begin
        Geral.MB_Aviso('Falha ao enviar e-mail!');
        Exit;
      end;
    end else
    begin
      PB1.Position := 0;
      PB1.Max      := N;
      //
      with DBG_ProtocoNFSe.DataSource.DataSet do
      for I := 0 to N - 1 do
      begin
        GotoBookmark(DBG_ProtocoNFSe.SelectedRows.Items[I]);
        //
        PB1.Position := PB1.Position + 1;
        PB1.Update;
        Application.ProcessMessages;
        //
        if QrProtocoNFSeRpsIDNumero.Value <> 0 then
        begin
          MyObjects.Informa2(LaAviso1, LaAviso2, False, 'Enviando NFS-e n�mero ' +
            Geral.FF0(QrProtocoNFSeNfsNumero.Value) + '!');
          //
          Res := EnviaNFSeEmail(NaoEnviados, TemDBWeb);
          //
          if not Res then
          begin
            Geral.MB_Aviso('Falha ao enviar e-mail!');
            Exit;
          end;
        end;
      end;
    end;
  finally
    DBGrid1.Enabled         := True;
    DBG_ProtocoNFSe.Enabled := True;
    PnMenu.Enabled          := True;
    BtEnvia.Enabled         := True;
    PB1.Position            := 0;
    //
    QrProtocoNFSe.EnableControls;
    //
    ReopenProt1(FCliInt);
    //
    if Res then
    begin
      Geral.MB_Aviso('Envio finalizado!');
      MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
    end;
  end;
{$Else}
begin
  Geral.MB_Aviso('O controle dos protocolos de recebimento dever� ser feito manualmente!'
    + sLineBreak + 'Motivo: O m�dulo WEB n�o est� habilitado!');
{$EndIf}
{$Else}
begin
  dmkPF.InfoSemModulo(mdlappProtocolos);
{$EndIf}
end;

procedure TFmNFSeMail.TBProtocolosChange(Sender: TObject);
begin
  ReopenQuerys(TBProtocolos.TabIndex);
end;

procedure TFmNFSeMail.Todosabertos1Click(Sender: TObject);
begin
{$IfNDef SemProtocolo}
  try
    DBGrid1.Enabled         := False;
    DBG_ProtocoNFSe.Enabled := False;
    BtPEBProtocolo.Enabled  := False;
    //
    UnProtocolo.GeraProtocoloNFSe(istTodos, QrProtocoNFSe, Dmod.MyDB,
      TDBGrid(DBG_ProtocoNFSe), PB1, FCliInt);
    //
    ReopenProtocoNFSe;
  finally
    DBGrid1.Enabled         := True;
    DBG_ProtocoNFSe.Enabled := True;
    BtPEBProtocolo.Enabled  := True;
  end;
{$Else}
  dmkPF.InfoSemModulo(mdlappProtocolos);
{$EndIf}
end;

end.
