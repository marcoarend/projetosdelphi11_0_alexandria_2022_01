unit NFSe_Testes1;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnMLAGeral, UnInternalConsts,
  dmkImage,
  //
  DMKpcnLeitor, DMKpcnConversao;

type
  TFmNFSe_Testes1 = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    Panel3: TPanel;
    Label189: TLabel;
    EdLeitorXML: TdmkEdit;
    SbDirNFSeLogs: TSpeedButton;
    MeLeitor: TMemo;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbDirNFSeLogsClick(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
    function LerXml(): Boolean;
    // ExibeValorDeCampo
    procedure EVC(Espacamento: Integer; Campo, Valor: String);
  public
    { Public declarations }
  end;

  var
  FmNFSe_Testes1: TFmNFSe_Testes1;

implementation

uses UnMyObjects, Module, UnDmkProcFunc;

{$R *.DFM}

var
  Prefixo2, Prefixo3: String;


procedure TFmNFSe_Testes1.BtOKClick(Sender: TObject);
begin
  MeLeitor.Lines.Clear;
  LerXml();
end;

procedure TFmNFSe_Testes1.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmNFSe_Testes1.EVC(Espacamento: Integer; Campo, Valor: String);
var
  Espaco: String;
  I: Integer;
begin
  Espaco := '';
  for I := 1 to Espacamento do
    Espaco := Espaco + '    ';
  MeLeitor.Lines.Add(Espaco + Campo + ' = ' + Valor);
end;

procedure TFmNFSe_Testes1.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmNFSe_Testes1.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  Prefixo2 := '';
  Prefixo3 := '';
end;

procedure TFmNFSe_Testes1.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

function TFmNFSe_Testes1.LerXml(): Boolean;
var
  i: Integer;
  Leitor: TLeitor;
  Texto: String;
begin
  Leitor := TLeitor.Create;
  dmkPF.LoadArquivoToText(EdLeitorXML.Text, Texto);
  Leitor.Arquivo := Texto;
  //
  Result := False;
  try
    Leitor.Grupo := Leitor.Arquivo;
    if (leitor.rExtrai(1, Prefixo3 + 'EnviarLoteRpsResposta') <> '')
    or (leitor.rExtrai(1, Prefixo3 + 'EnviarLoteRpsSincronoResposta') <> '') then // Maring�
    begin
      EVC(0, 'NumeroLote      ', Leitor.rCampo(tcStr, Prefixo3 + 'NumeroLote'));
      EVC(0, 'DataRecebimento ', Leitor.rCampo(tcDatHor, Prefixo3 + 'DataRecebimento'));
      EVC(0, 'Protocolo       ', Leitor.rCampo(tcStr, Prefixo3 + 'Protocolo'));

      // Ler a Lista de Mensagens
      if leitor.rExtrai(2, 'ListaMensagemRetorno') <> '' then
      begin
        i := 0;
        while Leitor.rExtrai(3, prefixo2 + 'MensagemRetorno', '', i + 1) <> '' do
        begin
          EVC(0, 'MsgRetorno[' + Geral.FF0(i) + '].FCodigo   ', Leitor.rCampo(tcStr, prefixo2 + 'Codigo'));
          EVC(0, 'MsgRetorno[' + Geral.FF0(i) + '].FMensagem ', Leitor.rCampo(tcStr, prefixo2 + 'Mensagem'));
          EVC(0, 'MsgRetorno[' + Geral.FF0(i) + '].FCorrecao ', Leitor.rCampo(tcStr, prefixo2 + 'Correcao'));
          EVC(0, '', '');

          inc(i);
        end;
      end;

      Result := True;
    end;
  except
    result := False;
  end;
end;

procedure TFmNFSe_Testes1.SbDirNFSeLogsClick(Sender: TObject);
begin
  MyObjects.DefineDiretorio(Self, EdLeitorXML);
end;

end.
