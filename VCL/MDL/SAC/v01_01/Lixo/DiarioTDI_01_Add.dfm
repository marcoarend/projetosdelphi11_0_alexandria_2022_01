object FmDiarioTDI_01_add: TFmDiarioTDI_01_add
  Tag = 1
  Left = 258
  Top = 135
  Caption = 'ATD-TELEF-002 :: Interlocu'#231#227'o'
  ClientHeight = 729
  ClientWidth = 1000
  Color = clWindow
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Arial'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 14
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 1000
    Height = 729
    Align = alClient
    BevelOuter = bvNone
    ParentBackground = False
    TabOrder = 0
    object PnTempo: TPanel
      Left = 0
      Top = 0
      Width = 1000
      Height = 61
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 281
        Height = 61
        Align = alLeft
        Caption = ' Atendimento:'
        TabOrder = 0
        object Label2: TLabel
          Left = 12
          Top = 16
          Width = 12
          Height = 14
          Caption = 'ID:'
        end
        object Label5: TLabel
          Left = 96
          Top = 16
          Width = 25
          Height = 14
          Caption = 'Data:'
        end
        object Label6: TLabel
          Left = 216
          Top = 16
          Width = 26
          Height = 14
          Caption = 'Hora:'
        end
        object EdCodigo: TdmkEdit
          Left = 12
          Top = 32
          Width = 80
          Height = 22
          TabStop = False
          Alignment = taRightJustify
          Enabled = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clTeal
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          OnChange = EdCodigoChange
        end
        object TPData: TdmkEditDateTimePicker
          Left = 96
          Top = 32
          Width = 116
          Height = 22
          Date = 40125.721013657410000000
          Time = 40125.721013657410000000
          TabOrder = 1
          TabStop = False
          ReadOnly = False
          DefaultEditMask = '!99/99/99;1;_'
          AutoApplyEditMask = True
          UpdType = utYes
        end
        object EdHora: TdmkEdit
          Left = 216
          Top = 32
          Width = 56
          Height = 22
          TabStop = False
          TabOrder = 2
          FormatType = dmktfTime
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfLong
          HoraFormat = dmkhfLong
          Texto = '00:00:00'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
      end
      object GroupBox2: TGroupBox
        Left = 281
        Top = 0
        Width = 308
        Height = 61
        Align = alLeft
        Caption = ' Detec'#231#227'o por Bina: '
        TabOrder = 1
        object Label111: TLabel
          Left = 4
          Top = 16
          Width = 95
          Height = 14
          Caption = 'Telefone que tocou:'
        end
        object Label3: TLabel
          Left = 124
          Top = 16
          Width = 25
          Height = 14
          Caption = 'Data:'
        end
        object Label13: TLabel
          Left = 244
          Top = 16
          Width = 26
          Height = 14
          Caption = 'Hora:'
        end
        object EdBinaFone: TdmkEdit
          Left = 6
          Top = 32
          Width = 112
          Height = 21
          TabStop = False
          PopupMenu = PMBinaFone
          TabOrder = 0
          FormatType = dmktfString
          MskType = fmtTelLongo
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          QryCampo = 'ETe1'
          UpdCampo = 'ETe1'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
          OnDblClick = EdBinaFoneDblClick
        end
        object TPBinaSoDt: TdmkEditDateTimePicker
          Left = 124
          Top = 32
          Width = 116
          Height = 22
          Date = 0.721013657406729200
          Time = 0.721013657406729200
          TabOrder = 1
          TabStop = False
          ReadOnly = False
          DefaultEditMask = '!99/99/99;1;_'
          AutoApplyEditMask = True
          UpdType = utYes
        end
        object EdBinaSoHr: TdmkEdit
          Left = 244
          Top = 32
          Width = 56
          Height = 22
          TabStop = False
          TabOrder = 2
          FormatType = dmktfTime
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '00:00'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
      end
      object GB_R: TGroupBox
        Left = 952
        Top = 0
        Width = 48
        Height = 61
        Align = alRight
        TabOrder = 2
        object ImgTipo: TdmkImage
          Left = 8
          Top = 19
          Width = 32
          Height = 32
          Transparent = True
          SQLType = stNil
        end
      end
      object GroupBox4: TGroupBox
        Left = 589
        Top = 0
        Width = 363
        Height = 61
        Align = alClient
        Caption = ' Contato: '
        TabOrder = 3
        object LaInterlctr1B: TLabel
          Left = 7
          Top = 18
          Width = 12
          Height = 19
          Caption = '...'
          Color = clBtnFace
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clSilver
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentColor = False
          ParentFont = False
        end
        object Label8: TLabel
          Left = 4
          Top = 40
          Width = 33
          Height = 14
          Caption = 'Forma:'
        end
        object SbFormContat: TSpeedButton
          Left = 336
          Top = 36
          Width = 21
          Height = 21
          Caption = '...'
          Visible = False
          OnClick = SbFormContatClick
        end
        object LaInterlctr1A: TLabel
          Left = 6
          Top = 17
          Width = 12
          Height = 19
          Caption = '...'
          Color = clBtnFace
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clGreen
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentColor = False
          ParentFont = False
        end
        object EdFormContat: TdmkEditCB
          Left = 40
          Top = 36
          Width = 32
          Height = 22
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          DBLookupComboBox = CBFormContat
          IgnoraDBLookupComboBox = False
        end
        object CBFormContat: TdmkDBLookupComboBox
          Left = 72
          Top = 36
          Width = 261
          Height = 22
          KeyField = 'Codigo'
          ListField = 'Nome'
          ListSource = DsFormContat
          TabOrder = 1
          dmkEditCB = EdFormContat
          UpdType = utYes
          LocF7SQLMasc = '$#'
        end
      end
    end
    object PnFiltro: TPanel
      Left = 0
      Top = 263
      Width = 1000
      Height = 466
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 2
      object Panel2: TPanel
        Left = 0
        Top = 0
        Width = 1000
        Height = 466
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object Panel5: TPanel
          Left = 441
          Top = 0
          Width = 559
          Height = 466
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          object PnContatos: TPanel
            Left = 0
            Top = 0
            Width = 559
            Height = 165
            Align = alTop
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 0
            object DBGEntiContat: TDBGrid
              Left = 0
              Top = 0
              Width = 277
              Height = 165
              Align = alLeft
              DataSource = DsEntiContat
              PopupMenu = PMContatos
              TabOrder = 0
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'Arial'
              TitleFont.Style = []
              OnDrawColumnCell = DBGEntiContatDrawColumnCell
              OnDblClick = DBGEntiContatDblClick
              Columns = <
                item
                  Alignment = taCenter
                  Expanded = False
                  FieldName = 'QUE_ENT'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'Arial'
                  Font.Style = [fsBold]
                  Title.Caption = 'QM'
                  Width = 22
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Controle'
                  Title.Caption = 'ID'
                  Width = 44
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Nome'
                  Title.Caption = 'Nome do contato'
                  Width = 123
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NOME_CARGO'
                  Title.Caption = 'Cargo'
                  Width = 90
                  Visible = True
                end>
            end
            object Panel8: TPanel
              Left = 277
              Top = 0
              Width = 282
              Height = 165
              Align = alClient
              BevelOuter = bvNone
              ParentBackground = False
              TabOrder = 1
              object DBGEntiTel: TDBGrid
                Left = 0
                Top = 0
                Width = 282
                Height = 84
                Align = alTop
                DataSource = DsEntiTel
                Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
                PopupMenu = PMTelefones
                TabOrder = 0
                TitleFont.Charset = DEFAULT_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -11
                TitleFont.Name = 'Arial'
                TitleFont.Style = []
                Columns = <
                  item
                    Expanded = False
                    FieldName = 'NOMEETC'
                    Title.Caption = 'Tipo de telefone'
                    Width = 84
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'TEL_TXT'
                    Title.Caption = 'Telefone'
                    Width = 120
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Ramal'
                    Width = 37
                    Visible = True
                  end>
              end
              object DBGEntiMail: TDBGrid
                Left = 0
                Top = 84
                Width = 282
                Height = 81
                Align = alClient
                DataSource = DsEntiMail
                Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
                PopupMenu = PMEMails
                TabOrder = 1
                TitleFont.Charset = DEFAULT_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -11
                TitleFont.Name = 'Arial'
                TitleFont.Style = []
                Columns = <
                  item
                    Expanded = False
                    FieldName = 'NOMEETC'
                    Title.Caption = 'Tipo de e-mail'
                    Width = 84
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'EMail'
                    Title.Caption = 'E-mail'
                    Width = 159
                    Visible = True
                  end>
              end
            end
          end
          object Panel14: TPanel
            Left = 0
            Top = 165
            Width = 559
            Height = 301
            Align = alClient
            BevelOuter = bvNone
            TabOrder = 1
            object GroupBox6: TGroupBox
              Left = 0
              Top = 0
              Width = 559
              Height = 41
              Align = alTop
              Caption = ' Hist'#243'rico do di'#225'rio'
              TabOrder = 0
              object Panel15: TPanel
                Left = 2
                Top = 16
                Width = 555
                Height = 23
                Align = alClient
                TabOrder = 0
                object CkHistEnt: TCheckBox
                  Left = 6
                  Top = 4
                  Width = 180
                  Height = 17
                  Caption = '[Sub]Cliente'
                  TabOrder = 0
                  OnClick = CkHistEntClick
                end
                object CkHistTer: TCheckBox
                  Left = 188
                  Top = 4
                  Width = 180
                  Height = 17
                  Caption = 'Contratante'
                  TabOrder = 1
                  OnClick = CkHistEntClick
                end
                object CkHistInt: TCheckBox
                  Left = 369
                  Top = 4
                  Width = 180
                  Height = 17
                  Caption = 'Interlocutor'
                  TabOrder = 2
                  OnClick = CkHistEntClick
                end
              end
            end
            object DBGrid3: TDBGrid
              Left = 0
              Top = 41
              Width = 281
              Height = 260
              Align = alLeft
              DataSource = DsDiarioHist
              TabOrder = 1
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'Arial'
              TitleFont.Style = []
              Columns = <
                item
                  Expanded = False
                  FieldName = 'Data'
                  Width = 56
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Hora'
                  Width = 56
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NO_GENERO'
                  Title.Caption = 'A'#231#227'o'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'ID'
                  Visible = True
                end>
            end
            object DBMemo1: TDBMemo
              Left = 281
              Top = 41
              Width = 278
              Height = 260
              Align = alClient
              DataField = 'Nome'
              DataSource = DsDiarioHist
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Arial'
              Font.Style = []
              ParentFont = False
              TabOrder = 2
            end
          end
        end
        object Panel6: TPanel
          Left = 0
          Top = 0
          Width = 441
          Height = 466
          Align = alLeft
          BevelOuter = bvNone
          TabOrder = 1
          object Panel9: TPanel
            Left = 0
            Top = 0
            Width = 441
            Height = 209
            Align = alTop
            BevelOuter = bvNone
            TabOrder = 0
            object Panel7: TPanel
              Left = 0
              Top = 144
              Width = 441
              Height = 65
              Align = alClient
              BevelOuter = bvNone
              TabOrder = 1
              object LaDepto: TLabel
                Left = 4
                Top = 44
                Width = 34
                Height = 14
                Caption = 'ID Link:'
              end
              object SbUserAction: TSpeedButton
                Left = 180
                Top = 40
                Width = 21
                Height = 21
                Caption = '...'
                Enabled = False
                OnClick = SbUserActionClick
              end
              object LaLetras: TLabel
                Left = 417
                Top = 37
                Width = 24
                Height = 14
                Align = alRight
                Alignment = taCenter
                Caption = '255  '
              end
              object EdDepto: TdmkEdit
                Left = 100
                Top = 40
                Width = 80
                Height = 21
                Alignment = taRightJustify
                ReadOnly = True
                TabOrder = 1
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
                OnChange = EdDeptoChange
                OnKeyDown = EdDeptoKeyDown
              end
              object RGUserGenero: TRadioGroup
                Left = 0
                Top = 0
                Width = 441
                Height = 37
                Align = alTop
                Caption = ' A'#231#227'o a ser tomada: '
                Columns = 3
                ItemIndex = 0
                Items.Strings = (
                  'Nenhuma'
                  'Ordem de Servi'#231'o')
                TabOrder = 0
                OnClick = RGUserGeneroClick
              end
            end
            object GroupBox5: TGroupBox
              Left = 0
              Top = 0
              Width = 441
              Height = 144
              Align = alTop
              Caption = ' Relacionamentos neste atendimento: '
              TabOrder = 0
              object Panel12: TPanel
                Left = 2
                Top = 16
                Width = 437
                Height = 126
                Align = alClient
                BevelOuter = bvNone
                TabOrder = 0
                object LaCliInt: TLabel
                  Left = 4
                  Top = 8
                  Width = 71
                  Height = 14
                  Caption = 'Cliente interno:'
                end
                object SpeedButton4: TSpeedButton
                  Left = 408
                  Top = 4
                  Width = 21
                  Height = 21
                  Caption = '&R'
                  Visible = False
                  OnClick = SbEntiContat
                end
                object LaInterloctr: TLabel
                  Left = 4
                  Top = 80
                  Width = 68
                  Height = 14
                  Caption = 'Interlocutor:'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clGreen
                  Font.Height = -11
                  Font.Name = 'Arial'
                  Font.Style = [fsBold]
                  ParentFont = False
                end
                object SbInterloctr: TSpeedButton
                  Left = 408
                  Top = 76
                  Width = 21
                  Height = 21
                  Caption = '...'
                  Visible = False
                end
                object LaEntidade: TLabel
                  Left = 4
                  Top = 32
                  Width = 71
                  Height = 14
                  Caption = '[Sub]Cliente:'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clRed
                  Font.Height = -11
                  Font.Name = 'Arial'
                  Font.Style = [fsBold]
                  ParentFont = False
                end
                object SbEntidade: TSpeedButton
                  Left = 408
                  Top = 28
                  Width = 21
                  Height = 21
                  Caption = '...'
                  OnClick = SbEntidadeClick
                end
                object Label1: TLabel
                  Left = 4
                  Top = 104
                  Width = 44
                  Height = 14
                  Caption = 'Assunto:'
                end
                object SpeedButton1: TSpeedButton
                  Left = 408
                  Top = 100
                  Width = 21
                  Height = 21
                  Caption = '...'
                  OnClick = SpeedButton1Click
                end
                object LaTerceiro01: TLabel
                  Left = 4
                  Top = 56
                  Width = 69
                  Height = 14
                  Caption = 'COntratante:'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clBlue
                  Font.Height = -11
                  Font.Name = 'Arial'
                  Font.Style = [fsBold]
                  ParentFont = False
                end
                object SbTerceiro01: TSpeedButton
                  Left = 408
                  Top = 52
                  Width = 21
                  Height = 21
                  Caption = '...'
                  OnClick = SbTerceiro01Click
                end
                object EdCliInt: TdmkEditCB
                  Left = 76
                  Top = 4
                  Width = 48
                  Height = 22
                  Alignment = taRightJustify
                  TabOrder = 0
                  FormatType = dmktfInteger
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ValMin = '-2147483647'
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0
                  ValWarn = False
                  OnChange = EdCliIntChange
                  DBLookupComboBox = CBCliInt
                  IgnoraDBLookupComboBox = False
                end
                object CBCliInt: TdmkDBLookupComboBox
                  Left = 124
                  Top = 4
                  Width = 284
                  Height = 22
                  KeyField = 'Filial'
                  ListField = 'NOMEFILIAL'
                  ListSource = DsCliInt
                  TabOrder = 1
                  dmkEditCB = EdCliInt
                  UpdType = utYes
                  LocF7SQLMasc = '$#'
                end
                object EdInterloctr: TdmkEditCB
                  Left = 76
                  Top = 76
                  Width = 48
                  Height = 22
                  Alignment = taRightJustify
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clGreen
                  Font.Height = -11
                  Font.Name = 'Arial'
                  Font.Style = [fsBold]
                  ParentFont = False
                  TabOrder = 6
                  FormatType = dmktfInteger
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ValMin = '-2147483647'
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0
                  ValWarn = False
                  OnChange = EdInterloctrChange
                  DBLookupComboBox = CBInterloctr
                  IgnoraDBLookupComboBox = False
                end
                object CBInterloctr: TdmkDBLookupComboBox
                  Left = 124
                  Top = 76
                  Width = 284
                  Height = 22
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clGreen
                  Font.Height = -11
                  Font.Name = 'Arial'
                  Font.Style = [fsBold]
                  KeyField = 'Controle'
                  ListField = 'Nome'
                  ListSource = DsInterloctr
                  ParentFont = False
                  TabOrder = 7
                  dmkEditCB = EdInterloctr
                  UpdType = utYes
                  LocF7SQLMasc = '$#'
                end
                object EdEntidade: TdmkEditCB
                  Left = 76
                  Top = 28
                  Width = 48
                  Height = 22
                  Alignment = taRightJustify
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clRed
                  Font.Height = -11
                  Font.Name = 'Arial'
                  Font.Style = [fsBold]
                  ParentFont = False
                  TabOrder = 2
                  FormatType = dmktfInteger
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ValMin = '-2147483647'
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0
                  ValWarn = False
                  OnChange = EdEntidadeChange
                  OnDblClick = EdEntidadeDblClick
                  DBLookupComboBox = CBEntidade
                  IgnoraDBLookupComboBox = False
                end
                object CBEntidade: TdmkDBLookupComboBox
                  Left = 124
                  Top = 28
                  Width = 284
                  Height = 22
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clRed
                  Font.Height = -11
                  Font.Name = 'Arial'
                  Font.Style = [fsBold]
                  KeyField = 'Codigo'
                  ListField = 'NOMEENTIDADE'
                  ListSource = DsClientes
                  ParentFont = False
                  TabOrder = 3
                  dmkEditCB = EdEntidade
                  UpdType = utYes
                  LocF7SQLMasc = '$#'
                end
                object EdDiarioAss: TdmkEditCB
                  Left = 76
                  Top = 100
                  Width = 48
                  Height = 22
                  Alignment = taRightJustify
                  TabOrder = 8
                  FormatType = dmktfInteger
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ValMin = '-2147483647'
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0
                  ValWarn = False
                  OnChange = EdDiarioAssChange
                  DBLookupComboBox = CBDiarioAss
                  IgnoraDBLookupComboBox = False
                end
                object CBDiarioAss: TdmkDBLookupComboBox
                  Left = 124
                  Top = 100
                  Width = 284
                  Height = 22
                  KeyField = 'CodUsu'
                  ListField = 'Nome'
                  ListSource = DsDiarioAss
                  TabOrder = 9
                  dmkEditCB = EdDiarioAss
                  UpdType = utYes
                  LocF7SQLMasc = '$#'
                end
                object EdTerceiro01: TdmkEditCB
                  Left = 76
                  Top = 52
                  Width = 48
                  Height = 22
                  Alignment = taRightJustify
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clBlue
                  Font.Height = -11
                  Font.Name = 'Arial'
                  Font.Style = [fsBold]
                  ParentFont = False
                  TabOrder = 4
                  FormatType = dmktfInteger
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ValMin = '-2147483647'
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0
                  ValWarn = False
                  OnChange = EdTerceiro01Change
                  DBLookupComboBox = CBTerceiro01
                  IgnoraDBLookupComboBox = False
                end
                object CBTerceiro01: TdmkDBLookupComboBox
                  Left = 124
                  Top = 52
                  Width = 284
                  Height = 22
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clBlue
                  Font.Height = -11
                  Font.Name = 'Arial'
                  Font.Style = [fsBold]
                  KeyField = 'Codigo'
                  ListField = 'NOMEENTIDADE'
                  ListSource = DsTerceiro01
                  ParentFont = False
                  TabOrder = 5
                  dmkEditCB = EdTerceiro01
                  UpdType = utYes
                  LocF7SQLMasc = '$#'
                end
              end
            end
          end
          object Panel13: TPanel
            Left = 0
            Top = 209
            Width = 441
            Height = 257
            Align = alClient
            BevelOuter = bvNone
            TabOrder = 1
            object GBConfirma: TGroupBox
              Left = 0
              Top = 194
              Width = 441
              Height = 63
              Align = alBottom
              TabOrder = 0
              object BtConfirma: TBitBtn
                Tag = 10035
                Left = 12
                Top = 17
                Width = 90
                Height = 40
                Cursor = crHandPoint
                Caption = '&Encerra'
                Enabled = False
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                NumGlyphs = 2
                ParentFont = False
                ParentShowHint = False
                ShowHint = True
                TabOrder = 0
                OnClick = BtConfirmaClick
              end
              object Panel4: TPanel
                Left = 331
                Top = 16
                Width = 108
                Height = 45
                Align = alRight
                BevelOuter = bvNone
                TabOrder = 1
                object BtDesiste: TBitBtn
                  Tag = 15
                  Left = 7
                  Top = 2
                  Width = 90
                  Height = 40
                  Cursor = crHandPoint
                  Caption = '&Desiste'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'MS Sans Serif'
                  Font.Style = []
                  NumGlyphs = 2
                  ParentFont = False
                  ParentShowHint = False
                  ShowHint = True
                  TabOrder = 0
                  OnClick = BtDesisteClick
                end
              end
              object BtParcial: TBitBtn
                Tag = 86
                Left = 108
                Top = 17
                Width = 90
                Height = 40
                Cursor = crHandPoint
                Caption = '&Salva'#13#10'Parcial'
                Enabled = False
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                NumGlyphs = 2
                ParentFont = False
                ParentShowHint = False
                ShowHint = True
                TabOrder = 2
                OnClick = BtParcialClick
              end
              object BtNovaFrase: TBitBtn
                Tag = 86
                Left = 204
                Top = 17
                Width = 90
                Height = 40
                Cursor = crHandPoint
                Caption = '&Nova'#13#10'Frase'
                Enabled = False
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                NumGlyphs = 2
                ParentFont = False
                ParentShowHint = False
                ShowHint = True
                TabOrder = 3
                OnClick = BtNovaFraseClick
              end
            end
            object MeNome: TdmkMemo
              Left = 0
              Top = 0
              Width = 441
              Height = 194
              Align = alClient
              MaxLength = 255
              TabOrder = 1
              OnChange = MeNomeChange
              UpdType = utYes
            end
          end
        end
      end
    end
    object GBGerenInterlocutor: TGroupBox
      Left = 0
      Top = 61
      Width = 1000
      Height = 202
      Align = alTop
      Caption = ' Cadastros neste atendimento: '
      TabOrder = 1
      Visible = False
      object GroupBox3: TGroupBox
        Left = 2
        Top = 16
        Width = 276
        Height = 184
        Align = alLeft
        Caption = ' Interlocutor: '
        TabOrder = 0
        object GBContato: TGroupBox
          Left = 2
          Top = 16
          Width = 272
          Height = 166
          Align = alClient
          Caption = ' Cadastro de novo interlocutor: '
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          TabOrder = 0
          object Panel3: TPanel
            Left = 2
            Top = 16
            Width = 268
            Height = 148
            Align = alClient
            BevelOuter = bvNone
            TabOrder = 0
            object PnEC: TPanel
              Left = 0
              Top = 0
              Width = 268
              Height = 48
              Align = alTop
              BevelOuter = bvNone
              TabOrder = 0
              object Label7: TLabel
                Left = 4
                Top = 1
                Width = 30
                Height = 14
                Caption = 'Nome:'
              end
              object SbECCargo: TSpeedButton
                Left = 244
                Top = 1
                Width = 21
                Height = 21
                Caption = '...'
                OnClick = SbECCargoClick
              end
              object SbETNome: TSpeedButton
                Left = 244
                Top = 25
                Width = 21
                Height = 21
                Caption = 'ok'
                OnClick = SbETNomeClick
              end
              object EdECCargo: TdmkEditCB
                Left = 40
                Top = 1
                Width = 32
                Height = 22
                Alignment = taRightJustify
                TabOrder = 0
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
                DBLookupComboBox = CBECCargo
                IgnoraDBLookupComboBox = False
              end
              object CBECCargo: TdmkDBLookupComboBox
                Left = 72
                Top = 1
                Width = 169
                Height = 22
                KeyField = 'Codigo'
                ListField = 'Nome'
                ListSource = DsEntiCargos
                TabOrder = 1
                dmkEditCB = EdECCargo
                UpdType = utYes
                LocF7SQLMasc = '$#'
              end
              object EdETNome: TdmkEdit
                Left = 40
                Top = 25
                Width = 200
                Height = 21
                TabOrder = 2
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
            end
            object PnET: TPanel
              Left = 0
              Top = 48
              Width = 268
              Height = 48
              Align = alTop
              BevelOuter = bvNone
              Enabled = False
              TabOrder = 1
              object SbETTelefone: TSpeedButton
                Left = 244
                Top = 25
                Width = 21
                Height = 21
                Caption = 'ok'
                OnClick = SbETTelefoneClick
              end
              object LaETCRamal: TLabel
                Left = 184
                Top = 28
                Width = 10
                Height = 14
                Caption = 'R:'
              end
              object SbETCFone: TSpeedButton
                Left = 244
                Top = 1
                Width = 21
                Height = 21
                Caption = '...'
                OnClick = SbETCFoneClick
              end
              object LaETCFone: TLabel
                Left = 4
                Top = 4
                Width = 27
                Height = 14
                Caption = 'Fone:'
              end
              object EdETTelefone: TdmkEdit
                Left = 40
                Top = 25
                Width = 141
                Height = 21
                TabOrder = 2
                FormatType = dmktfString
                MskType = fmtTelCurto
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object EdETRamal: TdmkEdit
                Left = 196
                Top = 25
                Width = 45
                Height = 21
                TabOrder = 3
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object CBETCFone: TdmkDBLookupComboBox
                Left = 72
                Top = 1
                Width = 169
                Height = 22
                KeyField = 'Codigo'
                ListField = 'Nome'
                ListSource = DsETCFone
                TabOrder = 1
                dmkEditCB = EdETCFone
                UpdType = utYes
                LocF7SQLMasc = '$#'
              end
              object EdETCFone: TdmkEditCB
                Left = 40
                Top = 1
                Width = 32
                Height = 22
                Alignment = taRightJustify
                TabOrder = 0
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
                DBLookupComboBox = CBETCFone
                IgnoraDBLookupComboBox = False
              end
            end
            object PnEE: TPanel
              Left = 0
              Top = 96
              Width = 268
              Height = 52
              Align = alClient
              BevelOuter = bvNone
              Enabled = False
              TabOrder = 2
              object SbEEEMail: TSpeedButton
                Left = 243
                Top = 25
                Width = 21
                Height = 21
                Caption = 'ok'
                OnClick = SbEEEMailClick
              end
              object SbETCEMail: TSpeedButton
                Left = 244
                Top = 1
                Width = 21
                Height = 21
                Caption = '...'
                OnClick = SbETCEMailClick
              end
              object LaETCMail: TLabel
                Left = 4
                Top = 4
                Width = 27
                Height = 14
                Caption = 'Email:'
              end
              object EdEEEmail: TdmkEdit
                Left = 40
                Top = 25
                Width = 200
                Height = 21
                TabOrder = 2
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object CBETCMail: TdmkDBLookupComboBox
                Left = 72
                Top = 1
                Width = 169
                Height = 22
                KeyField = 'Codigo'
                ListField = 'Nome'
                ListSource = DsETCMail
                TabOrder = 1
                dmkEditCB = EdETCMail
                UpdType = utYes
                LocF7SQLMasc = '$#'
              end
              object EdETCMail: TdmkEditCB
                Left = 40
                Top = 1
                Width = 32
                Height = 22
                Alignment = taRightJustify
                TabOrder = 0
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
                DBLookupComboBox = CBETCMail
                IgnoraDBLookupComboBox = False
              end
            end
          end
        end
      end
      object Panel10: TPanel
        Left = 278
        Top = 16
        Width = 720
        Height = 184
        Align = alClient
        BevelOuter = bvNone
        ParentBackground = False
        TabOrder = 1
        object DBGrid1: TDBGrid
          Left = 0
          Top = 0
          Width = 422
          Height = 184
          Align = alLeft
          DataSource = DsEC
          PopupMenu = PMEC
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'Arial'
          TitleFont.Style = []
          OnDblClick = DBGrid1DblClick
          Columns = <
            item
              Expanded = False
              FieldName = 'Controle'
              Title.Caption = 'ID'
              Width = 44
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Nome'
              Title.Caption = 'Nome do contato'
              Width = 200
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOME_CARGO'
              Title.Caption = 'Cargo'
              Width = 140
              Visible = True
            end>
        end
        object Panel11: TPanel
          Left = 422
          Top = 0
          Width = 298
          Height = 184
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 1
          object DBGrid2: TDBGrid
            Left = 0
            Top = 0
            Width = 298
            Height = 121
            Align = alClient
            DataSource = DsET
            Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
            PopupMenu = PMET
            TabOrder = 0
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'Arial'
            TitleFont.Style = []
            Columns = <
              item
                Expanded = False
                FieldName = 'NOMEETC'
                Title.Caption = 'Tipo de telefone'
                Width = 84
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'TEL_TXT'
                Title.Caption = 'Telefone'
                Width = 120
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Ramal'
                Width = 37
                Visible = True
              end>
          end
          object DBGEE: TDBGrid
            Left = 0
            Top = 121
            Width = 298
            Height = 63
            Align = alBottom
            DataSource = DsEE
            Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
            PopupMenu = PMEE
            TabOrder = 1
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'Arial'
            TitleFont.Style = []
            Columns = <
              item
                Expanded = False
                FieldName = 'NOMEETC'
                Title.Caption = 'Tipo de e-mail'
                Width = 84
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'EMail'
                Title.Caption = 'E-mail'
                Width = 159
                Visible = True
              end>
          end
        end
      end
    end
  end
  object QrCliInt: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT DISTINCT ent.Codigo, ent.CliInt Filial,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEFILIAL,'
      'IF(ent.Tipo=0, ent.CNPJ, ent.CPF) CNPJ_CPF'
      'FROM entidades ent'
      'WHERE ent.CliInt <> 0')
    Left = 100
    Top = 65524
    object QrCliIntCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCliIntFilial: TIntegerField
      FieldName = 'Filial'
    end
    object QrCliIntNOMEFILIAL: TWideStringField
      FieldName = 'NOMEFILIAL'
      Required = True
      Size = 100
    end
    object QrCliIntCNPJ_CPF: TWideStringField
      FieldName = 'CNPJ_CPF'
      Size = 18
    end
  end
  object DsCliInt: TDataSource
    DataSet = QrCliInt
    Left = 128
    Top = 65524
  end
  object QrClientes: TmySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrClientesBeforeClose
    AfterScroll = QrClientesAfterScroll
    SQL.Strings = (
      'SELECT Codigo, CodUsu, CliInt,'
      'IF(Tipo=0, RazaoSocial, Nome) NOMEENTIDADE'
      'FROM entidades'
      'ORDER BY NOMEENTIDADE')
    Left = 156
    Top = 65524
    object QrClientesCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrClientesCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrClientesCliInt: TIntegerField
      FieldName = 'CliInt'
    end
    object QrClientesNOMEENTIDADE: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object DsClientes: TDataSource
    DataSet = QrClientes
    Left = 184
    Top = 65524
  end
  object QrTerceiro01: TmySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrTerceiro01BeforeClose
    AfterScroll = QrTerceiro01AfterScroll
    SQL.Strings = (
      'SELECT Codigo, CodUsu, CliInt,'
      'IF(Tipo=0, RazaoSocial, Nome) NOMEENTIDADE'
      'FROM entidades'
      'ORDER BY NOMEENTIDADE')
    Left = 212
    Top = 65524
    object QrTerceiro01Codigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'entidades.Codigo'
    end
    object QrTerceiro01CliInt: TIntegerField
      FieldName = 'CliInt'
      Origin = 'entidades.CliInt'
    end
    object QrTerceiro01CodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrTerceiro01NOMEENTIDADE: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object DsTerceiro01: TDataSource
    DataSet = QrTerceiro01
    Left = 240
    Top = 65524
  end
  object QrDiarioAss: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, CodUsu, Nome, Aplicacao'
      'FROM diarioass'
      'ORDER BY Nome')
    Left = 4
    Top = 32
    object QrDiarioAssCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrDiarioAssCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrDiarioAssNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
    object QrDiarioAssAplicacao: TIntegerField
      FieldName = 'Aplicacao'
    end
  end
  object DsDiarioAss: TDataSource
    DataSet = QrDiarioAss
    Left = 32
    Top = 32
  end
  object DsInterloctr: TDataSource
    DataSet = QrInterloctr
    Left = 420
    Top = 65524
  end
  object QrInterloctr: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      '    SELECT Controle, Nome '
      '    FROM enticontat '
      '    ORDER BY Nome')
    Left = 392
    Top = 65524
    object QrInterloctrControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrInterloctrNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
  end
  object VaChamou: TdmkVariable
    ValueVariant = 0
    Left = 452
    Top = 65524
  end
  object VaTipo: TdmkVariable
    ValueVariant = 0
    Left = 480
    Top = 65524
  end
  object VaGenero: TdmkVariable
    ValueVariant = 3
    Left = 508
    Top = 65524
  end
  object VaPreAtend: TdmkVariable
    ValueVariant = 0
    Left = 536
    Top = 65524
  end
  object QrEntiCargos: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM enticargos'
      'ORDER BY Nome')
    Left = 284
    Top = 132
    object QrEntiCargosCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEntiCargosNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
  end
  object DsEntiCargos: TDataSource
    DataSet = QrEntiCargos
    Left = 312
    Top = 132
  end
  object QrETCFone: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM entitipcto'
      'ORDER BY Nome')
    Left = 284
    Top = 160
    object QrETCFoneCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrETCFoneNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
  end
  object DsETCFone: TDataSource
    DataSet = QrETCFone
    Left = 312
    Top = 160
  end
  object QrETCMail: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM entitipcto'
      'ORDER BY Nome')
    Left = 284
    Top = 188
    object QrETCMailCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrETCMailNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
  end
  object DsETCMail: TDataSource
    DataSet = QrETCMail
    Left = 312
    Top = 188
  end
  object QrEC: TmySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrECBeforeClose
    AfterScroll = QrECAfterScroll
    SQL.Strings = (
      'SELECT eco.Codigo, eco.Sexo, '
      'eco.Controle, eco.Nome, eco.Cargo,'
      'eco.DiarioAdd, eco.DtaNatal,'
      'eca.Nome NOME_CARGO '
      'FROM enticontat eco'
      'LEFT JOIN enticargos eca ON eca.Codigo=eco.Cargo'
      'WHERE eco.DiarioAdd=:P0'#10
      ''
      ''
      ''
      ''
      '')
    Left = 352
    Top = 132
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrECControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrECNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 30
    end
    object QrECCargo: TIntegerField
      FieldName = 'Cargo'
      Required = True
    end
    object QrECNOME_CARGO: TWideStringField
      FieldName = 'NOME_CARGO'
      Size = 30
    end
    object QrECDiarioAdd: TIntegerField
      FieldName = 'DiarioAdd'
    end
    object QrECDtaNatal: TDateField
      FieldName = 'DtaNatal'
    end
    object QrECCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrECSexo: TSmallintField
      FieldName = 'Sexo'
    end
  end
  object DsEC: TDataSource
    DataSet = QrEC
    Left = 380
    Top = 132
  end
  object QrEE: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT etc.Nome NOMEETC, ema.Conta, '
      'ema.EMail, ema.EntiTipCto, ema.Ordem'
      'FROM entimail ema'
      'LEFT JOIN entitipcto etc ON etc.Codigo=ema.EntiTipCto'#13
      'WHERE ema.Controle=:P0'
      'ORDER BY ema.Ordem, ema.Conta'#10
      '')
    Left = 352
    Top = 188
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrEENOMEETC: TWideStringField
      FieldName = 'NOMEETC'
      Size = 30
    end
    object QrEEConta: TIntegerField
      FieldName = 'Conta'
      Required = True
    end
    object QrEEEMail: TWideStringField
      FieldName = 'EMail'
      Required = True
      Size = 255
    end
    object QrEEEntiTipCto: TIntegerField
      FieldName = 'EntiTipCto'
      Required = True
    end
    object QrEEOrdem: TIntegerField
      FieldName = 'Ordem'
    end
  end
  object DsEE: TDataSource
    DataSet = QrEE
    Left = 380
    Top = 188
  end
  object QrET: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrETCalcFields
    SQL.Strings = (
      'SELECT etc.Nome NOMEETC, emt.Conta, '
      'emt.Telefone, emt.EntiTipCto, emt.Ramal'
      'FROM entitel emt'
      'LEFT JOIN entitipcto etc ON etc.Codigo=emt.EntiTipCto'#13
      'WHERE emt.Controle=:P0')
    Left = 352
    Top = 160
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrETNOMEETC: TWideStringField
      FieldName = 'NOMEETC'
      Origin = 'entitipcto.Nome'
      Size = 30
    end
    object QrETConta: TIntegerField
      FieldName = 'Conta'
      Origin = 'entitel.Conta'
      Required = True
    end
    object QrETTelefone: TWideStringField
      FieldName = 'Telefone'
      Origin = 'entitel.Telefone'
    end
    object QrETEntiTipCto: TIntegerField
      FieldName = 'EntiTipCto'
      Origin = 'entitel.EntiTipCto'
      Required = True
    end
    object QrETTEL_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TEL_TXT'
      Size = 30
      Calculated = True
    end
    object QrETRamal: TWideStringField
      FieldName = 'Ramal'
      Origin = 'entitel.Ramal'
    end
  end
  object DsET: TDataSource
    DataSet = QrET
    Left = 380
    Top = 160
  end
  object PMEE: TPopupMenu
    OnPopup = PMEEPopup
    Left = 672
    Top = 12
    object ExcluiEE1: TMenuItem
      Caption = '&Exclui email selecionado'
      OnClick = ExcluiEE1Click
    end
    object Alteraemailselecionado2: TMenuItem
      Caption = '&Altera email selecionado'
      OnClick = Alteraemailselecionado2Click
    end
  end
  object PMEC: TPopupMenu
    OnPopup = PMECPopup
    Left = 608
    Top = 156
    object Alteracontatoatual2: TMenuItem
      Caption = '&Altera contato atual'
      OnClick = Alteracontatoatual2Click
    end
    object ExcluiEC1: TMenuItem
      Caption = 'E&xclui contato selecionado'
      OnClick = ExcluiEC1Click
    end
    object Definecomocontatodo1: TMenuItem
      Caption = '&Define como contato do...'
      object Cliente1: TMenuItem
        Caption = 'Cli&ente'
        OnClick = Cliente1Click
      end
      object Contratante1: TMenuItem
        Caption = 'C&ontratante'
        OnClick = Contratante1Click
      end
    end
  end
  object PMET: TPopupMenu
    OnPopup = PMETPopup
    Left = 640
    Top = 64
    object ExcluiET1: TMenuItem
      Caption = '&Exclui telefone selecionado'
      OnClick = ExcluiET1Click
    end
    object Alteratelefoneselecionado1: TMenuItem
      Caption = '&Altera telefone selecionado'
      OnClick = Alteratelefoneselecionado1Click
    end
  end
  object QrEntiContat: TmySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrEntiContatBeforeClose
    AfterScroll = QrEntiContatAfterScroll
    SQL.Strings = (
      'SELECT eco.Codigo, eco.Sexo,'
      'ece.Controle, COUNT(ece.Controle) QTD, eco.Nome, eco.Cargo, '
      'eco.DiarioAdd, eco.DtaNatal, eca.Nome NOME_CARGO , '
      'IF(ece.Codigo=1105, "SC", "CO") QUE_ENT '
      'FROM enticonent ece'
      'LEFT JOIN enticontat eco ON eco.Controle=ece.Controle'
      'LEFT JOIN enticargos eca ON eca.Codigo=eco.Cargo '
      'WHERE ece.Codigo=1105'
      'OR ece.Codigo=1341'
      'GROUP BY ece.Controle')
    Left = 564
    Top = 65524
    object QrEntiContatControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrEntiContatNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 30
    end
    object QrEntiContatCargo: TIntegerField
      FieldName = 'Cargo'
      Required = True
    end
    object QrEntiContatNOME_CARGO: TWideStringField
      FieldName = 'NOME_CARGO'
      Size = 30
    end
    object QrEntiContatQUE_ENT: TWideStringField
      FieldName = 'QUE_ENT'
      Size = 2
    end
    object QrEntiContatDiarioAdd: TIntegerField
      FieldName = 'DiarioAdd'
    end
    object QrEntiContatDtaNatal: TDateField
      FieldName = 'DtaNatal'
    end
    object QrEntiContatQTD: TLargeintField
      FieldName = 'QTD'
      Required = True
    end
    object QrEntiContatCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEntiContatSexo: TSmallintField
      FieldName = 'Sexo'
    end
  end
  object DsEntiContat: TDataSource
    DataSet = QrEntiContat
    Left = 592
    Top = 65524
  end
  object QrEntiMail: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT etc.Nome NOMEETC, ema.Conta, '
      'ema.EMail, ema.EntiTipCto, ema.Ordem'
      'FROM entimail ema'
      'LEFT JOIN entitipcto etc ON etc.Codigo=ema.EntiTipCto'#13
      'WHERE ema.Controle=:P0'
      'ORDER BY ema.Ordem, ema.Conta'#10)
    Left = 620
    Top = 65524
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrEntiMailNOMEETC: TWideStringField
      FieldName = 'NOMEETC'
      Size = 30
    end
    object QrEntiMailConta: TIntegerField
      FieldName = 'Conta'
      Required = True
    end
    object QrEntiMailEMail: TWideStringField
      FieldName = 'EMail'
      Required = True
      Size = 255
    end
    object QrEntiMailEntiTipCto: TIntegerField
      FieldName = 'EntiTipCto'
      Required = True
    end
    object QrEntiMailOrdem: TIntegerField
      FieldName = 'Ordem'
    end
  end
  object DsEntiMail: TDataSource
    DataSet = QrEntiMail
    Left = 648
    Top = 65524
  end
  object QrEntiTel: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrEntiTelCalcFields
    SQL.Strings = (
      'SELECT etc.Nome NOMEETC, emt.Conta, '
      'emt.Telefone, emt.EntiTipCto, emt.Ramal'
      'FROM entitel emt'
      'LEFT JOIN entitipcto etc ON etc.Codigo=emt.EntiTipCto'#13
      'WHERE emt.Controle=:P0')
    Left = 676
    Top = 65524
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrEntiTelNOMEETC: TWideStringField
      FieldName = 'NOMEETC'
      Origin = 'entitipcto.Nome'
      Size = 30
    end
    object QrEntiTelConta: TIntegerField
      FieldName = 'Conta'
      Origin = 'entitel.Conta'
      Required = True
    end
    object QrEntiTelTelefone: TWideStringField
      FieldName = 'Telefone'
      Origin = 'entitel.Telefone'
    end
    object QrEntiTelEntiTipCto: TIntegerField
      FieldName = 'EntiTipCto'
      Origin = 'entitel.EntiTipCto'
      Required = True
    end
    object QrEntiTelTEL_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TEL_TXT'
      Size = 30
      Calculated = True
    end
    object QrEntiTelRamal: TWideStringField
      FieldName = 'Ramal'
      Origin = 'entitel.Ramal'
    end
  end
  object DsEntiTel: TDataSource
    DataSet = QrEntiTel
    Left = 704
    Top = 65524
  end
  object PMEntidades: TPopupMenu
    Left = 356
    Top = 600
    object Rpido1: TMenuItem
      Caption = '&Guia'
      OnClick = Rpido1Click
    end
    object CompletoJanela1: TMenuItem
      Caption = '&Janela'
      OnClick = CompletoJanela1Click
    end
  end
  object PMTerceiro01: TPopupMenu
    Left = 356
    Top = 624
    object Rpido2: TMenuItem
      Caption = '&Guia'
      OnClick = Rpido2Click
    end
    object CompletoJanela2: TMenuItem
      Caption = '&Janela'
      OnClick = CompletoJanela2Click
    end
  end
  object PMContatos: TPopupMenu
    OnPopup = PMContatosPopup
    Left = 456
    Top = 572
    object IncluiContato1: TMenuItem
      Caption = '&Inclui novo contato'
      object SubClienteA1: TMenuItem
        Caption = '[Sub]Cliente'
        OnClick = SubClienteA1Click
      end
      object ContratanteA1: TMenuItem
        Caption = 'Contratante'
        OnClick = ContratanteA1Click
      end
    end
    object AlteraContato1: TMenuItem
      Caption = '&Altera Contato atual'
      OnClick = AlteraContato1Click
    end
    object ExcluiContato1: TMenuItem
      Caption = '&Exclui contato(s)'
      OnClick = ExcluiContato1Click
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object AtrelaraoSubcliente1: TMenuItem
      Caption = 'Atrelar ao [&Sub]Cliente'
      Visible = False
      OnClick = AtrelaraoSubcliente1Click
    end
    object AtrelaraoContratante1: TMenuItem
      Caption = 'Atrelar ao &COntratante'
      Visible = False
      OnClick = AtrelaraoContratante1Click
    end
  end
  object PMEMails: TPopupMenu
    OnPopup = PMEMailsPopup
    Left = 512
    Top = 572
    object NovoEMail1: TMenuItem
      Caption = '&Novo e-mail'
      OnClick = NovoEMail1Click
    end
    object Alteraemailselecionado1: TMenuItem
      Caption = '&Altera e-mail selecionado'
      OnClick = Alteraemailselecionado1Click
    end
    object Excluiemailselecionado1: TMenuItem
      Caption = '&Exclui e-mail selecionado'
      OnClick = Excluiemailselecionado1Click
    end
  end
  object PMTelefones: TPopupMenu
    OnPopup = PMTelefonesPopup
    Left = 540
    Top = 572
    object Incluinovotelefone1: TMenuItem
      Caption = '&Inclui novo telefone'
      OnClick = Incluinovotelefone1Click
    end
    object Alteratelefoneatual1: TMenuItem
      Caption = '&Altera telefone atual'
      OnClick = Alteratelefoneatual1Click
    end
    object Excluitelefones1: TMenuItem
      Caption = '&Exclui telefone(s)'
      OnClick = Excluitelefones1Click
    end
  end
  object QrFormContat: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM formcontat'
      'ORDER BY Nome')
    Left = 732
    Top = 65524
    object QrFormContatCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrFormContatNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
  end
  object DsFormContat: TDataSource
    DataSet = QrFormContat
    Left = 760
    Top = 65524
  end
  object QrDiarioHist: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Data, Hora, '
      'Genero, Depto, Nome '
      'FROM diarioadd'
      'WHERE CliInt=1'
      'AND Entidade=20'
      'ORDER BY Data DESC, Hora DESC')
    Left = 476
    Top = 524
    object QrDiarioHistCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrDiarioHistData: TDateField
      FieldName = 'Data'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrDiarioHistHora: TTimeField
      FieldName = 'Hora'
      DisplayFormat = 'hh:nn'
    end
    object QrDiarioHistNome: TWideStringField
      FieldName = 'Nome'
      Size = 255
    end
    object QrDiarioHistGenero: TSmallintField
      FieldName = 'Genero'
    end
    object QrDiarioHistDepto: TIntegerField
      FieldName = 'Depto'
      DisplayFormat = '000000;-000000; '
    end
    object QrDiarioHistNO_GENERO: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NO_GENERO'
      Size = 10
      Calculated = True
    end
  end
  object DsDiarioHist: TDataSource
    DataSet = QrDiarioHist
    Left = 504
    Top = 524
  end
  object QrEnt0: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, CodUsu, CliInt,'
      'IF(Tipo=0, RazaoSocial, Nome) NOMEENTIDADE'
      'FROM entidades'
      'WHERE Codigo=0')
    Left = 352
    Top = 216
    object QrEnt0Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEnt0CodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrEnt0CliInt: TIntegerField
      FieldName = 'CliInt'
    end
    object QrEnt0NOMEENTIDADE: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
  end
  object DsEnt0: TDataSource
    DataSet = QrEnt0
    Left = 380
    Top = 216
  end
  object PMBinaFone: TPopupMenu
    OnPopup = PMBinaFonePopup
    Left = 312
    Top = 36
    object Adicionaraointerlocutor1: TMenuItem
      Caption = '&Adicionar ao interlocutor'
      OnClick = Adicionaraointerlocutor1Click
    end
  end
end
