object FmDiarioTDI_01_: TFmDiarioTDI_01_
  Tag = 1
  Left = 258
  Top = 135
  Caption = 'Cadastro de clientes'
  ClientHeight = 438
  ClientWidth = 676
  Color = clWindow
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 8
    Top = 8
    Width = 238
    Height = 29
    Caption = 'Cadastro de Clientes'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -24
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object GBConfirma: TGroupBox
    Left = 0
    Top = 375
    Width = 676
    Height = 63
    Align = alBottom
    TabOrder = 0
    object BtConfirma: TBitBtn
      Tag = 14
      Left = 12
      Top = 17
      Width = 90
      Height = 40
      Cursor = crHandPoint
      Caption = '&Confirma'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      OnClick = BtConfirmaClick
      NumGlyphs = 2
    end
    object Panel4: TPanel
      Left = 566
      Top = 15
      Width = 108
      Height = 46
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtDesiste: TBitBtn
        Tag = 15
        Left = 7
        Top = 2
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtDesisteClick
        NumGlyphs = 2
      end
    end
  end
end
