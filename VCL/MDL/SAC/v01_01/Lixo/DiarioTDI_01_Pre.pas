unit DiarioTDI_01_Pre;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ComCtrls, dmkEditDateTimePicker, DBCtrls, DmkDAC_PF, DB,
  dmkDBLookupComboBox, dmkEditCB, dmkEdit, Grids, DBGrids, ExtCtrls, Buttons,
  mySQLDbTables, dmkGeral, dmkCompoStore, Menus, UnInternalConsts, dmkImage,
  dmkRadioGroup, dmkCheckGroup, UnAppListas, UnDmkEnums, dmkMemo, UnProjGroup_Consts,
  dmkLabelRotate;

type
  TFmDiarioTDI_01_Pre = class(TForm)
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel4: TPanel;
    BtDesiste: TBitBtn;
    QrEntidades: TmySQLQuery;
    QrEntidadesCodigo: TIntegerField;
    QrEntidadesNO_ENT: TWideStringField;
    DsEntidades: TDataSource;
    QrSiapTerCad: TmySQLQuery;
    QrSiapTerCadCodigo: TIntegerField;
    QrSiapTerCadNome: TWideStringField;
    DsSiapterCad: TDataSource;
    QrEstatusOSs: TmySQLQuery;
    QrEstatusOSsCodigo: TIntegerField;
    QrEstatusOSsNome: TWideStringField;
    DsEstatusOSs: TDataSource;
    QrFatoGeradr: TmySQLQuery;
    QrFatoGeradrCodigo: TIntegerField;
    QrFatoGeradrNome: TWideStringField;
    QrFatoGeradrNeedOS_Ori: TSmallintField;
    DsFatoGeradr: TDataSource;
    QrEntiContat: TmySQLQuery;
    DsEntiContat: TDataSource;
    QrEntContrat: TmySQLQuery;
    QrEntContratCodigo: TIntegerField;
    QrEntContratNO_ENT: TWideStringField;
    DsEntContrat: TDataSource;
    CsEdDepto: TdmkCompoStore;
    CsAbaOrigem: TdmkCompoStore;
    QrEntiContatControle: TIntegerField;
    QrEntiContatNome: TWideStringField;
    QrEntiContatCargo2: TIntegerField;
    QrEntiContatNOME_CARGO2: TWideStringField;
    QrEntiContatQUE_ENT2: TWideStringField;
    PMOSCabAlv: TPopupMenu;
    IncluiOSCabAlv1: TMenuItem;
    AlteraOSCabAlv1: TMenuItem;
    ExcluiOSCabAlv1: TMenuItem;
    QrOSCabAlv: TmySQLQuery;
    DsOSCabAlv: TDataSource;
    Panel1: TPanel;
    PnTempo: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GBEdita: TGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    Label6: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    Label34: TLabel;
    Label35: TLabel;
    Label36: TLabel;
    EdCodigo: TdmkEdit;
    EdEntidade: TdmkEditCB;
    CBEntidade: TdmkDBLookupComboBox;
    EdSiapTerCad: TdmkEditCB;
    CBSiapTerCad: TdmkDBLookupComboBox;
    EdEstatus: TdmkEditCB;
    CBEstatus: TdmkDBLookupComboBox;
    EdFatoGeradr: TdmkEditCB;
    CBFatoGeradr: TdmkDBLookupComboBox;
    EdEmpresa: TdmkEdit;
    EdEntiContat: TdmkEditCB;
    CBEntiContat: TdmkDBLookupComboBox;
    EdNumContrat: TdmkEdit;
    EdEntContrat: TdmkEditCB;
    CBEntContrat: TdmkDBLookupComboBox;
    SbSiapTerCad: TSpeedButton;
    PMSiapTerCad: TPopupMenu;
    Inclui1: TMenuItem;
    Altera1: TMenuItem;
    Janela1: TMenuItem;
    Guia1: TMenuItem;
    QrOSCabAlvCodigo: TIntegerField;
    QrOSCabAlvControle: TIntegerField;
    QrOSCabAlvOrdem: TSmallintField;
    QrOSCabAlvAtivo: TSmallintField;
    QrOSCabAlvPraga_A: TIntegerField;
    QrOSCabAlvPraga_Z: TIntegerField;
    QrOSCabAlvNO_NIVEL: TWideStringField;
    QrOSCabAlvNO_PRAGA: TWideStringField;
    CsSbUserAction: TdmkCompoStore;
    QrOSCab: TmySQLQuery;
    QrOSCabCodigo: TIntegerField;
    QrOSCabEntidade: TIntegerField;
    QrOSCabSiapTerCad: TIntegerField;
    QrOSCabEstatus: TIntegerField;
    QrOSCabFatoGeradr: TIntegerField;
    QrOSCabDtaContat: TDateTimeField;
    QrOSCabDtaVisPrv: TDateTimeField;
    QrOSCabDtaVisExe: TDateTimeField;
    QrOSCabDtaExePrv: TDateTimeField;
    QrOSCabDtaExeIni: TDateTimeField;
    QrOSCabDtaExeFim: TDateTimeField;
    QrOSCabValorTotal: TFloatField;
    QrOSCabLk: TIntegerField;
    QrOSCabDataCad: TDateField;
    QrOSCabDataAlt: TDateField;
    QrOSCabUserCad: TIntegerField;
    QrOSCabUserAlt: TIntegerField;
    QrOSCabAlterWeb: TSmallintField;
    QrOSCabAtivo: TSmallintField;
    QrOSCabDdsPosVda: TIntegerField;
    QrOSCabEntiContat: TIntegerField;
    QrOSCabNumContrat: TIntegerField;
    QrOSCabEntPagante: TIntegerField;
    QrOSCabEntContrat: TIntegerField;
    QrOSCabOperacao: TIntegerField;
    QrOSPrz: TmySQLQuery;
    QrOSPrzCodigo: TIntegerField;
    QrOSPrzControle: TIntegerField;
    QrOSPrzCondicao: TIntegerField;
    QrOSPrzNO_CONDICAO: TWideStringField;
    QrOSPrzDescoPer: TFloatField;
    QrOSPrzEscolhido: TSmallintField;
    DsOSPrz: TDataSource;
    DBGOSPrz: TDBGrid;
    PMOSPrz: TPopupMenu;
    OSPrz1_Inclui: TMenuItem;
    OSPrz1_Altera: TMenuItem;
    OSPrz1_Exclui: TMenuItem;
    Label3: TLabel;
    EdValorPre: TdmkEdit;
    QrOSPrzVAL_COM_DESCO: TFloatField;
    QrOSPrzVAL_PARCELA: TFloatField;
    QrOSPrzParcelas: TIntegerField;
    N1: TMenuItem;
    OSPrz1_AVista: TMenuItem;
    CGOperacao: TdmkCheckGroup;
    GroupBox3: TGroupBox;
    Label17: TLabel;
    Label18: TLabel;
    Label52: TLabel;
    Label53: TLabel;
    TPDtaVisPrv: TdmkEditDateTimePicker;
    EdDtaVisPrv: TdmkEdit;
    TPFimVisPrv: TdmkEditDateTimePicker;
    EdFimVisPrv: TdmkEdit;
    TPFimExePrv: TdmkEditDateTimePicker;
    EdFimExePrv: TdmkEdit;
    EdDtaExePrv: TdmkEdit;
    TPDtaExePrv: TdmkEditDateTimePicker;
    Label4: TLabel;
    QrSiapTerCadCliente: TIntegerField;
    QrSiapTerCadSLograd: TSmallintField;
    QrSiapTerCadSRua: TWideStringField;
    QrSiapTerCadSNumero: TIntegerField;
    QrSiapTerCadSCompl: TWideStringField;
    QrSiapTerCadSBairro: TWideStringField;
    QrSiapTerCadSCidade: TWideStringField;
    QrSiapTerCadSUF: TWideStringField;
    QrSiapTerCadSCEP: TIntegerField;
    QrSiapTerCadSPais: TWideStringField;
    QrSiapTerCadSEndeRef: TWideStringField;
    QrSiapTerCadSCodMunici: TIntegerField;
    QrSiapTerCadSCodiPais: TIntegerField;
    QrSiapTerCadSTe1: TWideStringField;
    QrSiapTerCadM2Constru: TFloatField;
    QrSiapTerCadM2NaoBuild: TFloatField;
    QrSiapTerCadM2Terreno: TFloatField;
    QrSiapTerCadM2Total: TFloatField;
    Panel2: TPanel;
    GroupBox10: TGroupBox;
    DBGOSCabAlv: TDBGrid;
    Splitter4: TSplitter;
    QrOSAge: TmySQLQuery;
    QrOSAgeNO_AGENTE: TWideStringField;
    QrOSAgeCodigo: TIntegerField;
    QrOSAgeControle: TIntegerField;
    QrOSAgeAgente: TIntegerField;
    QrOSAgeResponsa: TSmallintField;
    DsOSAge: TDataSource;
    PMOSAge: TPopupMenu;
    IncluiOSAge1: TMenuItem;
    RemoveOSAge1: TMenuItem;
    QrEntiContatCodigo: TIntegerField;
    DBGOSPrv: TDBGrid;
    QrOSPrv: TmySQLQuery;
    QrOSPrvCodigo: TIntegerField;
    QrOSPrvControle: TIntegerField;
    QrOSPrvDtHrContat: TDateTimeField;
    QrOSPrvFormContat: TIntegerField;
    QrOSPrvCliente: TIntegerField;
    QrOSPrvContato: TIntegerField;
    QrOSPrvAgente: TIntegerField;
    QrOSPrvNome: TWideStringField;
    QrOSPrvNO_Cliente: TWideStringField;
    QrOSPrvNO_Agente: TWideStringField;
    QrOSPrvNO_Contato: TWideStringField;
    QrOSPrvNO_FormContat: TWideStringField;
    DsOSPrv: TDataSource;
    PMOSPrv: TPopupMenu;
    Adicionasolicitaesdeprovidncias1: TMenuItem;
    Removeasolicitaesdeprovidnciaselecionada1: TMenuItem;
    QrAgeEqiIts: TmySQLQuery;
    QrAgeEqiItsControle: TIntegerField;
    QrAgeEqiItsEntidade: TIntegerField;
    QrAgeEqiItsEhLider: TSmallintField;
    QrAgeEqiCab: TmySQLQuery;
    IntegerField2: TIntegerField;
    QrAgeEqiCabNome: TWideStringField;
    DsAgeEqiCab: TDataSource;
    Label55: TLabel;
    EdAgeEqiCab: TdmkEditCB;
    CBAgeEqiCab: TdmkDBLookupComboBox;
    Label5: TLabel;
    EdEntPagante: TdmkEditCB;
    CBEntPagante: TdmkDBLookupComboBox;
    QrEntPagante: TmySQLQuery;
    QrEntPaganteCodigo: TIntegerField;
    QrEntPaganteNO_ENT: TWideStringField;
    QrOSMonPipDd: TmySQLQuery;
    QrOSMonPipDdCodigo: TIntegerField;
    QrOSMonPipDdControle: TIntegerField;
    QrOSMonPipDdConta: TIntegerField;
    QrOSMonPipDdIDIts: TIntegerField;
    QrOSMonPipDdOrdem: TIntegerField;
    QrOSMonPipDdDias: TIntegerField;
    QrOSMonPipDdLk: TIntegerField;
    QrOSMonPipDdDataCad: TDateField;
    QrOSMonPipDdDataAlt: TDateField;
    QrOSMonPipDdUserCad: TIntegerField;
    QrOSMonPipDdUserAlt: TIntegerField;
    QrOSMonPipDdAlterWeb: TSmallintField;
    QrOSMonPipDdAtivo: TSmallintField;
    DsEntPagante: TDataSource;
    DBGOSAge: TDBGrid;
    GBObservacoes: TGroupBox;
    Splitter11: TSplitter;
    PnObsExecuta: TPanel;
    dmkLabelRotate2: TdmkLabelRotate;
    MeObsExecuta: TdmkMemo;
    PnObsGaranti: TPanel;
    dmkLabelRotate3: TdmkLabelRotate;
    MeObsGaranti: TdmkMemo;
    procedure QrEntidadesBeforeClose(DataSet: TDataSet);
    procedure EdEntidadeChange(Sender: TObject);
    procedure EdFatoGeradrChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure EdEntContratChange(Sender: TObject);
    procedure DBGOSCabAlvMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure IncluiOSCabAlv1Click(Sender: TObject);
    procedure AlteraOSCabAlv1Click(Sender: TObject);
    procedure ExcluiOSCabAlv1Click(Sender: TObject);
    procedure PMOSCabAlvPopup(Sender: TObject);
    procedure EdCodigoChange(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure Guia1Click(Sender: TObject);
    procedure Janela1Click(Sender: TObject);
    procedure Altera1Click(Sender: TObject);
    procedure SbSiapTerCadClick(Sender: TObject);
    procedure PMSiapTerCadPopup(Sender: TObject);
    procedure DBGOSPrzDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure DBGOSPrzMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure OSPrz1_IncluiClick(Sender: TObject);
    procedure OSPrz1_AlteraClick(Sender: TObject);
    procedure OSPrz1_ExcluiClick(Sender: TObject);
    procedure PMOSPrzPopup(Sender: TObject);
    procedure EdValorPreChange(Sender: TObject);
    procedure QrOSPrzCalcFields(DataSet: TDataSet);
    procedure OSPrz1_AVistaClick(Sender: TObject);
    procedure TPDtaVisPrvExit(Sender: TObject);
    procedure EdDtaVisPrvExit(Sender: TObject);
    procedure TPDtaExePrvExit(Sender: TObject);
    procedure EdDtaExePrvExit(Sender: TObject);
    procedure DBGOSAgeMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure IncluiOSAge1Click(Sender: TObject);
    procedure RemoveOSAge1Click(Sender: TObject);
    procedure DBGOSPrvMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure Adicionasolicitaesdeprovidncias1Click(Sender: TObject);
    procedure Removeasolicitaesdeprovidnciaselecionada1Click(Sender: TObject);
    procedure EdEmpresaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdNumContratKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    procedure HandleWndProc(var Msg: TMessage); message WM_SYSCOMMAND;
    procedure FechaOForm();
    procedure ReopenSiapTerCad(Codigo: Integer);
    procedure ReopenEntiContat(Controle: Integer);
    procedure ReopenOSCabAlv(Controle: Integer);
    procedure ReopenOSPrz(Controle: Integer);
    procedure MostraFormSiapterCad(SQLType: TSQLType);
    procedure InsAltOSCabAlv(SQLType: TSQLType);
  public
    { Public declarations }
    FFmCriou: TForm;
  end;

var
  FmDiarioTDI_01_Pre: TFmDiarioTDI_01_Pre;

implementation

uses Principal, DiarioTDI_01, Module, UMySQLModule, UnMyObjects, CfgCadLista,
  UnGOTOy, SiapTerCad, MyDBCheck, ModOS, MyGlyfs, MyVCLSkin, UnDmkProcFunc,
  ModAgenda, ContratUnit, OSUnit, ModuleGeral, OSOriPsq;

{$R *.dfm}

{ TFmDiarioTDI_01_Pre }

procedure TFmDiarioTDI_01_Pre.Adicionasolicitaesdeprovidncias1Click(
  Sender: TObject);
begin
  UnOSUnit.MostraFormOSPrvSel(stIns, QrOSCabCodigo.Value, QrOSPrv, QrOSCabEntidade.Value);
end;

procedure TFmDiarioTDI_01_Pre.Altera1Click(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  //
  MostraFormSiapterCad(stUpd);
  //
  UMyMod.SetaCodigoPesquisado(
    EdSiapTerCad, CBSiapTerCad, QrSiapTerCad, VAR_CADASTRO);
end;

procedure TFmDiarioTDI_01_Pre.AlteraOSCabAlv1Click(Sender: TObject);
begin
  InsAltOSCabAlv(stUpd);
end;

procedure TFmDiarioTDI_01_Pre.BtConfirmaClick(Sender: TObject);
var
  //DtaContat, DtaVisExe, DtaExeIni, DtaExeFim,
  DtaVisPrv, DtaExePrv, FimVisPrv, FimExePrv: String;
  //GarantiaDd, HrEvacuar, HrExecutar, CondicaoPG, DdsPosVda, EntPagante,
  Codigo, Empresa, Entidade, SiapTerCad, Estatus, FatoGeradr, (*OSOrigem,*)
  EntiContat, NumContrat,
  EntContrat, EntPagante, Operacao, AgeEqiCab: Integer;
  //ValorTotal,
  ValorPre: Double;
  Inicio, Termino: TDateTime;
  Nome, Notas, ObsGaranti, ObsExecuta: String;
  Cor, Status: Integer;
  Cption: Byte;
begin
  Codigo         := EdCodigo.ValueVariant;
  Empresa        := EdEmpresa.ValueVariant;
  Entidade       := EdEntidade.ValueVariant;
  SiapTerCad     := EdSiapTerCad.ValueVariant;
  Estatus        := EdEstatus.ValueVariant;
  FatoGeradr     := EdFatoGeradr.ValueVariant;
  //OSOrigem       := EdOSOrigem.ValueVariant;
  //DtaContat      := Geral.FDT(TPDtaVisPrv.Date, 1);
  DtaVisPrv      := Geral.FDT_TP_Ed(TPDtaVisPrv.Date, EdDtaVisPrv.Text);
  //DtaVisExe      :=
  DtaExePrv      := Geral.FDT_TP_Ed(TPDtaExePrv.Date, EdDtaExePrv.Text);
  FimVisPrv      := Geral.FDT_TP_Ed(TPFimVisPrv.Date, EdFimVisPrv.Text);
  FimExePrv      := Geral.FDT_TP_ED(TPFimExePrv.Date, EdFimExePrv.Text);
  //DtaExeIni      := '0000-00-00';
  //DtaExeFim      := '0000-00-00';
  //GarantiaDd     := Ed.ValueVariant;;
  //HrEvacuar      := 0;
  //HrExecutar     := 0;
  //ValorTotal     := 0;
  //CondicaoPG     := 0;
  //DdsPosVda      := 0;
  EntiContat     := EdEntiContat.ValueVariant;
  NumContrat     := EdNumContrat.ValueVariant;;
  //EntPagante     := 0;
  EntContrat     := EdEntContrat.ValueVariant;
  EntPagante     := EdEntPagante.ValueVariant;
  Operacao       := CGOperacao.Value;
  ValorPre       := EdValorPre.ValueVariant;
  ObsGaranti     := MeObsGaranti.Text;
  ObsExecuta     := MeObsExecuta.Text;
  //
  AgeEqiCab      := EdAgeEqiCab.ValueVariant;
  if MyObjects.FIC(AgeEqiCab = 0, nil, 'Informe a equipe de agentes!') then
    Exit;
  //

  Inicio  := Geral.STD_109(DtaVisPrv);
  Termino := Geral.STD_109(FimVisPrv);
  if DmModAgenda.DesistePorqueNaoVaiAgendar(Inicio, Termino, 'Vistoria') then
    Exit;
  //
  Inicio  := Geral.STD_109(DtaExePrv);
  Termino := Geral.STD_109(FimExePrv);
  if DmModAgenda.DesistePorqueNaoVaiAgendar(Inicio, Termino, 'Execu��o') then
    Exit;
  //
  if MyObjects.FIC(Operacao = 0, CGOperacao, 'Informe a(s) opera��o(�es)!') then
    Exit;
  //
  Codigo := UMyMod.BPGS1I32('oscab', 'Codigo', '', '', tsDef,
    ImgTipo.SQLType, Codigo);
  UnOSUnit.CriaItensDeAgentes(Codigo, AgeEqiCab);
  UnOSUnit.ReopenOSAge(QrOSAge, EdCodigo.ValueVariant, 0);
  if MyObjects.FIC(QrOSAge.RecordCount = 0, nil, 'Informe pelo menos um agente operacional!') then
    Exit;
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'oscab', False, [
  'Entidade', 'SiapTerCad', 'Estatus',
  'FatoGeradr', (*'OSOrigem',*) (*'DtaContat',*)
  'DtaVisPrv', (*'DtaVisExe',*) 'DtaExePrv',
  (*'DtaExeIni', 'DtaExeFim', 'GarantiaDd',
  'HrEvacuar', 'HrExecutar', 'ValorTotal',
  'CondicaoPG', 'DdsPosVda',*) 'EntiContat',
  'NumContrat', (*'EntPagante',*) 'EntContrat',
  'Operacao', 'ValorPre', 'FimVisPrv',
  'FimExePrv', 'AgeEqiCab', 'ObsGaranti',
  'ObsExecuta', 'EntPagante'], [
  'Codigo'], [
  Entidade, SiapTerCad, Estatus,
  FatoGeradr, (*OSOrigem,*) (*DtaContat,*)
  DtaVisPrv, (*DtaVisExe,*) DtaExePrv,
  (*DtaExeIni, DtaExeFim, GarantiaDd,
  HrEvacuar, HrExecutar, ValorTotal,
  CondicaoPG, DdsPosVda,*) EntiContat,
  NumContrat, (*EntPagante,*) EntContrat,
  Operacao, ValorPre, FimVisPrv,
  FimExePrv, AgeEqiCab, ObsGaranti,
  ObsExecuta, EntPagante], [
  Codigo], True) then
  begin
    // Vistoria
    Inicio  := Geral.STD_109(DtaVisPrv);
    Termino := Geral.STD_109(FimVisPrv);
    Nome    := '';
    Notas   := '';
    Cor     := 0;
    Cption  := 0;
    Status  := Estatus;
    DmModAgenda.AtualizaCompromissoNaAgenda(qagOSBgstrl, aagVistoria, Codigo,
    Inicio, Termino, Empresa, Entidade, SiapTerCad, FatoGeradr, Nome, Notas,
    Cor, Cption, Status);
    // Execu��o
    Inicio  := Geral.STD_109(DtaExePrv);
    Termino := Geral.STD_109(FimExePrv);
    Nome    := '';
    Notas   := '';
    Cor     := 0;
    Cption  := 0;
    DmModAgenda.AtualizaCompromissoNaAgenda(qagOSBgstrl, aagExecucao, Codigo,
    Inicio, Termino, Empresa, Entidade, SiapTerCad, FatoGeradr, Nome, Notas,
    Cor, Cption, Status);
    //
    FechaOForm();
  end;
end;

procedure TFmDiarioTDI_01_Pre.BtDesisteClick(Sender: TObject);
var
  Res, Exclusao, I, K: Integer;
  CodTxt, Flds: String;
begin
  CodTxt := Geral.FF0(EdCodigo.ValueVariant);
  Res := Geral.MB_Pergunta('Deseja excluir a O.S. atual?');
  case Res of
    ID_YES:
    begin
      Exclusao := MyObjects.SelRadioGroup('Confirma��o de exclus�o',
      'Selecione o detino desta OS', [
      'Enviar para arquivo morto', 'Excluir permanentemente',
      'Quero desistir de excluir!'], 0);
      case Exclusao of
        0:
        begin
          //enviar para arquivo morto
          Flds := UMyMod.ObtemCamposDeTabelaIdentica(Dmod.MyDB, 'osdel', '');
          UMyMod.ExecutaMySQLQuery1(Dmod.QrUpd, [
          'INSERT INTO osdel ',
          'SELECT ' + Flds,
          'FROM oscab ',
          'WHERE Codigo=' + CodTxt,
          '; ',
          'DELETE FROM oscab ',
          'WHERE Codigo=' + CodTxt,
          '; ',
          '']);
          //
          FechaOForm();
        end;
        1:
        begin
          if (QrOSCabAlv.State <> dsInactive) and (QrOSCabAlv.RecordCount > 0) then
          begin
            Geral.MB_Aviso('Exclua primeiro as pragas!');
            Exit;
          end;
          if (QrOSAge.State <> dsInactive) and (QrOSAge.RecordCount > 0) then
          begin
            UMyMod.ExecutaMySQLQuery1(Dmod.QrUpd, [
            'DELETE FROM osage ',
            'WHERE Codigo=' + CodTxt,
            '; ',
            '']);
            UnOSUnit.ReopenOSAge(QrOSAge, EdCodigo.ValueVariant, 0);
            if (QrOSAge.State <> dsInactive) and (QrOSAge.RecordCount > 0) then
            begin
              Geral.MB_Aviso('Retire primeiro os agentes!');
              Exit;
            end;
          end;
          if (QrOSPrv.State <> dsInactive) and (QrOSPrv.RecordCount > 0) then
          begin
            Geral.MB_Aviso('Retire primeiro os pedidos de provid�ncias!');
            Exit;
          end;
          if UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
          'DELETE FROM oscab ',
          'WHERE Codigo=' + CodTxt,
          '']) then
          begin
            MyObjects.DefineValDeCompoEmbeded(CsEdDepto, 0);
            FechaOForm();
          end;
        end;
        2: ; // nada!
      end;
    end;
    ID_CANCEL: ; // nada
    ID_NO: FechaOForm();
  end;
end;

procedure TFmDiarioTDI_01_Pre.DBGOSAgeMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  if Button = mbRight then
    MyObjects.MostraPopOnControlXY(PMOSAge, DBGOSAge, X,Y);
end;

procedure TFmDiarioTDI_01_Pre.DBGOSCabAlvMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  if Button = mbRight then
    MyObjects.MostraPopOnControlXY(PMOSCabAlv, DBGOSCabAlv, X,Y);
end;

procedure TFmDiarioTDI_01_Pre.DBGOSPrvMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  if Button = mbRight then
    MyObjects.MostraPopOnControlXY(PMOSPrv, DBGOSPrv, X,Y);
end;

procedure TFmDiarioTDI_01_Pre.DBGOSPrzDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
  if Column.FieldName = 'Escolhido' then
    MeuVCLSkin.DrawGrid(DBGOSPrz, Rect, 1, QrOSPrzEscolhido.Value);
end;

procedure TFmDiarioTDI_01_Pre.DBGOSPrzMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  if Button = mbRight then
    MyObjects.MostraPopOnControlXY(PMOSPrz, DBGOSPrz, X,Y);
end;

procedure TFmDiarioTDI_01_Pre.EdCodigoChange(Sender: TObject);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrOSCab, Dmod.MyDB, [
  'SELECT * ',
  'FROM oscab ',
  'WHERE Codigo=' + Geral.FF0(EdCodigo.ValueVariant),
  '']);
  //
  ReopenOSCabAlv(0);
  ReopenOSPrz(0);
  UnOSUnit.ReopenOSPrv(QrOSPrv, EdCodigo.ValueVariant, 0);
  UnOSUnit.ReopenOSAge(QrOSAge, EdCodigo.ValueVariant, 0);
end;

procedure TFmDiarioTDI_01_Pre.EdDtaExePrvExit(Sender: TObject);
begin
  if (*(ImgTipo.SQLType = stIns) and*) (EdFimExePrv.Text = '00:00') then
    EdFimExePrv.Text := EdDtaExePrv.Text;
end;

procedure TFmDiarioTDI_01_Pre.EdDtaVisPrvExit(Sender: TObject);
begin
  if (*(ImgTipo.SQLType = stIns) and*) (EdFimVisPrv.Text = '00:00') then
    EdFimVisPrv.Text := EdDtaVisPrv.Text;
end;

procedure TFmDiarioTDI_01_Pre.EdEntContratChange(Sender: TObject);
begin
  ReopenEntiContat(0);
end;

procedure TFmDiarioTDI_01_Pre.EdEntidadeChange(Sender: TObject);
begin
  ReopenSiapTerCad(0);
  ReopenEntiContat(0);
end;

procedure TFmDiarioTDI_01_Pre.EdFatoGeradrChange(Sender: TObject);
(*
var
  Habilita: Boolean;
*)
begin
(*
  Habilita := (EdFatoGeradr.ValueVariant <> 0) and (QrFatoGeradrNeedOS_Ori.Value = 1);
  LaOSOrigem.Enabled := Habilita;
  EdOSOrigem.Enabled := Habilita;
  if not Habilita then
    EdOSOrigem.ValueVariant := 0;
*)
end;

procedure TFmDiarioTDI_01_Pre.EdNumContratKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
  Empresa, Contratante, Contrato: Integer;
begin
  if Key = VK_F4 then
  begin
    Empresa     := DModG.ObtemEntidadeDeFilial(EdEmpresa.ValueVariant);
    Contratante := EdEntidade.ValueVariant;
    //
    if UnContratUnit.PesquisaNumeroDoContrato(Empresa, Contratante, Contrato) then
      EdNumContrat.ValueVariant := Contrato;
  end;
end;

procedure TFmDiarioTDI_01_Pre.EdEmpresaKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
(*
var
  OSOrigem, Empresa, Cliente, Lugar, FatoGeradr: Integer;
*)
begin
(*
  if Key = VK_F4 then
  begin
    FatoGeradr := EdFatoGeradr.ValueVariant;
    Empresa    := EdEmpresa.ValueVariant;
    Cliente    := EdEntidade.ValueVariant;
    Lugar      := EdSiapterCad.ValueVariant;
    FatoGeradr := EdFatoGeradr.ValueVariant;
    //
    if UnOSUnit.PesquisaOSOrigem(Empresa, Cliente, Lugar, FatoGeradr, OSOrigem) then
      EdOSOrigem.ValueVariant := OSOrigem;
  end;
*)
end;

procedure TFmDiarioTDI_01_Pre.EdValorPreChange(Sender: TObject);
begin
  QrOSPrz.Refresh;
end;

procedure TFmDiarioTDI_01_Pre.ExcluiOSCabAlv1Click(Sender: TObject);
var
  Controle: Integer;
begin
  if UMyMod.ExcluiRegistroInt1(
  'Confirma a retirada da praga alvo selecionada?',
  'oscabalv', 'Controle', QrOSCabAlvControle.Value, Dmod.MyDB) = ID_YES then
  begin
    Controle := GOTOy.LocalizaPriorNextIntQr(QrOSCabAlv,
    QrOSCabAlvControle, QrOSCabAlvControle.Value);
    ReopenOSCabAlv(Controle);
  end;
end;

procedure TFmDiarioTDI_01_Pre.FechaOForm;
begin
{
  if TTabSheet(CsAbaOrigem.Component) <> nil then
  begin
    // se ainda existir
    if TMySQLQuery(CsQry.Component) <> nil then
    begin
      TMySQLQuery(CsQry.Component).Close;
      TMySQLQuery(CsQry.Component).Open;
    end;
    TdmkEditCB(CsEd.Component).ValueVariant := VaCodigo.ValueVariant;
    TdmkDBLookupComboBox(CsCB.Component).KeyValue := VaCodigo.ValueVariant;
  end;
}
  if CsSbUserAction.Component <> nil then
    TSpeedButton(CsSbUserAction.Component).Enabled := True;
  Close;
  TFmDiarioTDI_01(FFmCriou).FecharAba(TTabSheet(Self.Owner));
end;

procedure TFmDiarioTDI_01_Pre.FormCreate(Sender: TObject);
begin
  UMyMod.AbreQuery(QrEntidades, DMod.MyDB);
  UMyMod.AbreQuery(QrEstatusOSs, DMod.MyDB);
  UMyMod.AbreQuery(QrFatoGeradr, DMod.MyDB);
  UMyMod.AbreQuery(QrEntContrat, DMod.MyDB);
  UMyMod.AbreQuery(QrEntPagante, Dmod.MyDB);
  UMyMod.AbreQuery(QrAgeEqiCab, DMod.MyDB);
  MyObjects.ConfiguraCheckGroup(CGOperacao, sListaOperacoesBugs, 3, 0, False);
end;

procedure TFmDiarioTDI_01_Pre.FormShow(Sender: TObject);
begin
  if FmPrincipal.sd1.active then
    FmPrincipal.sd1.SkinForm(Handle);
  // ERRO! Oculta dmkEdits!
  //MyObjects.CorIniComponente(THandle(Sender));
  FmMyGlyfs.DefineGlyfs(TForm(Sender));
end;

procedure TFmDiarioTDI_01_Pre.Guia1Click(Sender: TObject);
begin
  TFmDiarioTDI_01(FFmCriou).NovaAba2(naaSiapTerCad, TTabSheet(Self.Owner),
    [QrSiapTerCad, EdSiapTerCad, CBSiapTerCad, EdEntidade]);
end;

procedure TFmDiarioTDI_01_Pre.HandleWndProc(var Msg: TMessage);
begin
  if Word(Msg.wParam) = 255 then
    TFmDiarioTDI_01(FFmCriou).AcoplarFormulario(Owner as TTabSheet)
  else
    inherited;
end;

procedure TFmDiarioTDI_01_Pre.IncluiOSAge1Click(Sender: TObject);
const
  Aviso  = '...';
  Titulo = 'Sele��o de Agentes';
  Prompt = 'Seleciones os agentes operacionais:';
  //Campo  = 'Descricao';
  //
  DestTab = 'osage';
  DestMaster = 'Codigo';
  DestDetail = 'Controle';
  DestSelec1 = 'Agente';
  SorcField  = 'Agente';
  ExcluiAnteriores = True;
var
  ValrMaster: Integer;
begin
  ValrMaster := QrOSCabCodigo.Value;
  if DBCheck.EscolheCodigosMultiplos_A(
  Aviso, Titulo, Prompt, nil, 'Ativo', 'Nivel1', 'Nome', [
  'DELETE FROM _selcods_; ',
  'INSERT INTO _selcods_ ',
  'SELECT Codigo Nivel1, 0 Nivel2, ',
  '0 Nivel3, 0 Nivel4, 0 Nivel5, Nome, 0 Ativo ',
  'FROM ' + TMeuDB + '.entidades',
  'WHERE Nome <> "" ',
  'AND Fornece2="V" ',
  ''],[
  'SELECT * FROM _selcods_ ',
  'ORDER BY Nome;',
  ''], DestTab, DestMaster, DestDetail, DestSelec1, ValrMaster,
  QrOSAge, SorcField, ExcluiAnteriores, Dmod.QrUpd) then
    UnOSUnit.ReopenOSAge(QrOSAge, QrOSCabCodigo.Value, 0);
end;

procedure TFmDiarioTDI_01_Pre.IncluiOSCabAlv1Click(Sender: TObject);
begin
//UnOSUnit.InsAltOSCabAlv(stIns, QrOSCabAlv, Dmod.QrUpd, QrOSCabCodigo.Value);
  UnOSUnit.InsAltOSCabAlv(
    stIns, QrOSCabAlv, Dmod.QrUpd, EdCodigo.ValueVariant);
  //
  ReopenOSCabAlv(0);
  //InsAltOSCabAlv(stIns);
end;

procedure TFmDiarioTDI_01_Pre.InsAltOSCabAlv(SQLType: TSQLType);
var
  _WHERE_: String;
  DefaultItem: Integer;
  DefaultValr: Double;
  Tamanho1, Tamanho2: Integer;
begin
  _WHERE_ := '';
  Tamanho1 := 0;
  Tamanho2 := 2; ; //casas decimais
  //
  if SQLType = stUpd then
  begin
    DefaultItem := QrOSCabAlvPraga_A.Value;
    DefaultValr := QrOSCabAlvOrdem.Value;
  end else
  begin
    DefaultItem := 0;
    DefaultValr := 0;
  end;
  UnCfgCadLista.InsAltItemLstEmRegTab('oscabalv', 'Praga_A', 'Controle', 'Ordem',
  'praga_a', CO_CODIGO, CO_NOME, _WHERE_, DefaultItem,
  DefaultValr, dmktfInteger, dmktfInteger, Tamanho1, Tamanho2,
  SQLType, 'Adi��o de Praga Alvo', 'Praga:', 'Ordem:', ncGerlSeq1,
  ncGerlSeq1, ['Codigo', 'Controle'], [EdCodigo.ValueVariant,
  QrOSCabAlvControle.Value], QrOSCabAlv);
end;

procedure TFmDiarioTDI_01_Pre.Janela1Click(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  //
  MostraFormSiapterCad(stIns);
  //
  UMyMod.SetaCodigoPesquisado(
    EdSiapTerCad, CBSiapTerCad, QrSiapTerCad, VAR_CADASTRO);
end;

procedure TFmDiarioTDI_01_Pre.MostraFormSiapterCad(SQLType: TSQLType);
var
  Entidade: Integer;
begin
  Entidade := EdEntidade.ValueVariant;
  if DBCheck.CriaFm(TFmSiapTerCad, FmSiapTerCad, afmoNegarComAviso) then
  begin
    FmSiapTerCad.ImgTipo.SQLType := SQLType;
    FmSiapTerCad.FQrSiapterCad := QrSiapTerCad;
    if SQLType = stUpd then
    begin
      FmSiapTerCad.EdCodigo.ValueVariant     := QrSiapTerCadCodigo.Value;
      FmSiapTerCad.EdCliente.ValueVariant    := QrSiapTerCadCliente.Value;
      FmSiapTerCad.CBCliente.KeyValue        := QrSiapTerCadCliente.Value;
      //
      FmSiapTerCad.EdNome.ValueVariant       := QrSiapTerCadNome.Value;
      FmSiapTerCad.EdSCEP.ValueVariant       := QrSiapTerCadSCEP.Value;
      FmSiapTerCad.EdSLograd.ValueVariant    := QrSiapTerCadSLograd.Value;
      FmSiapTerCad.CBSLograd.KeyValue        := QrSiapTerCadSLograd.Value;
      FmSiapTerCad.EdSRua.ValueVariant       := QrSiapTerCadSRua.Value;
      FmSiapTerCad.EdSNumero.ValueVariant    := QrSiapTerCadSNumero.Value;
      FmSiapTerCad.EdSCompl.ValueVariant     := QrSiapTerCadSCompl.Value;
      FmSiapTerCad.EdSBairro.ValueVariant    := QrSiapTerCadSBairro.Value;
      FmSiapTerCad.EdSCidade.ValueVariant    := QrSiapTerCadSCidade.Value;
      FmSiapTerCad.EdSUF.ValueVariant        := QrSiapTerCadSUF.Value;
      FmSiapTerCad.EdSPais.ValueVariant      := QrSiapTerCadSPais.Value;
      FmSiapTerCad.EdSEndeRef.ValueVariant   := QrSiapTerCadSEndeRef.Value;
      //FmSiapTerCad.EdSTe1.ValueVariant     := QrSiapTerCadSTe1.Value;
      FmSiapTerCad.EdSCodMunici.ValueVariant := QrSiapTerCadSCodMunici.Value;
      FmSiapTerCad.EdSCodiPais.ValueVariant  := QrSiapTerCadSCodiPais.Value;
      FmSiapTerCad.EdM2Constru.ValueVariant  := QrSiapTerCadM2Constru.Value;
      FmSiapTerCad.EdM2NaoBuild.ValueVariant := QrSiapTerCadM2NaoBuild.Value;
      FmSiapTerCad.EdM2Terreno.ValueVariant  := QrSiapTerCadM2Terreno.Value;
      FmSiapTerCad.EdM2Total.ValueVariant    := QrSiapTerCadM2Total.Value;
    end else
    begin
      FmSiapTerCad.EdCliente.ValueVariant := Entidade;
      FmSiapTerCad.CBCliente.KeyValue     := Entidade;
    end;
    //
    FmSiapTerCad.ShowModal;
    FmSiapTerCad.Destroy;
  end;
end;

procedure TFmDiarioTDI_01_Pre.OSPrz1_AlteraClick(Sender: TObject);
begin
  UnOSUnit.MostraFormOSPrz(stUpd, QrOSCab, QrOSPrz, 0, 0, 0);
end;

procedure TFmDiarioTDI_01_Pre.OSPrz1_AVistaClick(Sender: TObject);
var
  Valor, Fator: Double;
begin
  if dmkPF.ObtemValorDouble(Valor, 2) then
  begin
    Fator := 1 - (QrOSPrzDescoPer.Value /100);
    if Fator <> 0 then
      EdValorPre.ValueVariant := Valor / Fator;
  end;
end;

procedure TFmDiarioTDI_01_Pre.OSPrz1_ExcluiClick(Sender: TObject);
begin    
  if Geral.MensagemBox('Confirma a exclus�o da condi��o de pagamento "' +
  QrOSPrzNO_CONDICAO.Value + '"?', 'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) =
  ID_YES then DBCheck.ExcluiRegistro(Dmod.QrUpd, QrOSPrz, 'OSPrz',
  ['Controle'], ['Controle'], True);
end;

procedure TFmDiarioTDI_01_Pre.OSPrz1_IncluiClick(Sender: TObject);
begin
  UnOSUnit.MostraFormOSPrz(stIns, QrOSCab, QrOSPrz, 0, 0, 0);
end;

procedure TFmDiarioTDI_01_Pre.PMOSCabAlvPopup(Sender: TObject);
begin
  //MyObjects.HabilitaMenuItemItsIns(IncluiOSCabAlv1, QrOSCab);
  IncluiOSCabAlv1.Enabled := EdCodigo.ValueVariant <> 0;
  //
  MyObjects.HabilitaMenuItemItsUpd(AlteraOSCabAlv1, QrOSCabAlv);
  MyObjects.HabilitaMenuItemItsDel(ExcluiOSCabAlv1, QrOSCabAlv);
end;

procedure TFmDiarioTDI_01_Pre.PMOSPrzPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(OSPrz1_Inclui, QrOSCab);
  MyObjects.HabilitaMenuItemItsUpd(OSPrz1_Altera, QrOSPrz);
  MyObjects.HabilitaMenuItemItsDel(OSPrz1_Exclui, QrOSPrz);
  //
  MyObjects.HabilitaMenuItemItsDel(OSPrz1_AVista, QrOSPrz);
  if OSPrz1_AVista.Enabled then
     OSPrz1_AVista.Enabled := QrOSPrzDescoPer.Value <> 0;
end;

procedure TFmDiarioTDI_01_Pre.PMSiapTerCadPopup(Sender: TObject);
begin
  Guia1.Enabled := EdEntidade.ValueVariant <> 0;
end;

procedure TFmDiarioTDI_01_Pre.QrEntidadesBeforeClose(DataSet: TDataSet);
begin
  QrSiapTerCad.Close;
  QrEntiContat.Close;
end;

procedure TFmDiarioTDI_01_Pre.QrOSPrzCalcFields(DataSet: TDataSet);
var
  Valor, Fator: Double;
  Parcelas: Integer;
begin
  Valor := EdValorPre.ValueVariant;
  Fator := 1 - (QrOSPrzDescoPer.Value / 100);
  QrOSPrzVAL_COM_DESCO.Value := Valor * Fator;
  Parcelas := QrOSPrzParcelas.Value;
  if Parcelas > 0 then
    QrOSPrzVAL_PARCELA.Value := QrOSPrzVAL_COM_DESCO.Value / Parcelas
  else
    QrOSPrzVAL_PARCELA.Value := 0;
end;

procedure TFmDiarioTDI_01_Pre.Removeasolicitaesdeprovidnciaselecionada1Click(
  Sender: TObject);
const
  Codigo = 0;
var
  Controle: Integer;
begin
  if Geral.MB_Pergunta(
  'Confirma a retirada da solicita��o de provid�ncia selecionada?') =
  ID_YES then
  begin
    Controle := GOTOy.LocalizaPriorNextIntQr(QrOSPrv,
      QrOSPrvControle, QrOSPrvControle.Value);
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'osprv', False, [
    'Codigo'], [
    'Controle'], [
    Codigo], [
    Controle], True) then
      UnOSUnit.ReopenOSPrv(QrOSPrv, EdCodigo.ValueVariant, Controle);
  end;
end;

procedure TFmDiarioTDI_01_Pre.RemoveOSAge1Click(Sender: TObject);
var
  Controle: Integer;
begin
  if UMyMod.ExcluiRegistroInt1(
  'Confirma a retirada do agente selecionado?',
  'osage', 'Controle', QrOSAgeControle.Value, Dmod.MyDB) = ID_YES then
  begin
    Controle := GOTOy.LocalizaPriorNextIntQr(QrOSAge,
    QrOSAgeControle, QrOSAgeControle.Value);
    UnOSUnit.ReopenOSAge(QrOSAge, QrOSCabCodigo.Value, Controle);
  end;
end;

procedure TFmDiarioTDI_01_Pre.ReopenEntiContat(Controle: Integer);
var
  Cliente, EntContrat, Localiza: Integer;
begin
  Localiza := 0;
  if Controle <> 0 then
    Localiza := Controle
  else
  if QrEntiContat.State <> dsInactive then
    Localiza := QrEntiContatControle.Value;
  //
  Cliente := EdEntidade.ValueVariant;
  if Cliente = 0 then Cliente := -999999999;
  EntContrat := EdEntContrat.ValueVariant;
  if EntContrat = 0 then EntContrat := -999999999;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrEntiContat, DMod.MyDB, [
  'SELECT eco.Codigo, eco.Controle, eco.Nome, eco.Cargo, ',
  'eca.Nome NOME_CARGO, eco.DtaNatal, eco.Sexo, ',
  'ELT(eco.Sexo+1, "NI", "M", "F", "?") SEXO_TXT, ',
  'IF(eco.Codigo=' + Geral.FF0(Cliente) + ', "SC", "CO") QUE_ENT, ',
  'IF(eco.DtaNatal = 0, "", ' +
  'DATE_FORMAT(eco.DtaNatal, "%d/%m/%Y")) DTANATAL_TXT ',
  'FROM enticontat eco ',
  'LEFT JOIN enticonent ece ON ece.Controle=eco.Controle ',
  'LEFT JOIN enticargos eca ON eca.Codigo=eco.Cargo ',
  'WHERE ece.Codigo=' + Geral.FF0(Cliente),
  'OR ece.Codigo=' + Geral.FF0(EntContrat),
  '']);
  //
  if Localiza <> 0 then
    QrEntiContat.Locate('Controle', Localiza, []);
  //
end;

procedure TFmDiarioTDI_01_Pre.ReopenOSCabAlv(Controle: Integer);
begin
  QrOSCabAlv.Close;
  QrOSCabAlv.Params[0].AsInteger := EdCodigo.ValueVariant;
  UMyMod.AbreQuery(QrOSCabAlv, Dmod.MyDB);
  //
  if Controle <> 0 then
    QrOSCabAlv.Locate('Controle', Controle, []);
end;

procedure TFmDiarioTDI_01_Pre.ReopenOSPrz(Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrOSPrz, Dmod.MyDB, [
  'SELECT opz.Parcelas, ',
  'opz.Codigo, opz.Controle, opz.Condicao, ',
  'opz.DescoPer, opz.Escolhido, ppc.Nome NO_CONDICAO ',
  'FROM osprz opz ',
  'LEFT JOIN pediprzcab ppc ON ppc.Codigo=opz.Condicao ',
  'WHERE opz.Codigo=' + Geral.FF0(QrOSCabCodigo.Value),
  ' ']);
  //
  if Controle <> 0 then
    QrOSPrz.Locate('Codigo', Controle, []);
end;

procedure TFmDiarioTDI_01_Pre.ReopenSiapTerCad(Codigo: Integer);
var
  Cliente: Integer;
begin
  EdSiapTerCad.ValueVariant := 0;
  CBSiapTerCad.KeyValue := 0;
  //
  Cliente := EdEntidade.ValueVariant;
  if Cliente <> 0 then
    UnDmkDAC_PF.AbreMySQLQuery0(QrSiapTerCad, Dmod.MyDB, [
    //'SELECT Codigo, Nome ',
    'SELECT * ',
    'FROM siaptercad ',
    'WHERE Cliente=' + Geral.FF0(Cliente),
    ''])
  else
    QrSiapTerCad.Close;
  //
  if Codigo <> 0 then
  begin
    if QrSiapTerCad.Locate('Codigo', Codigo, []) then
    begin
      EdSiapTerCad.ValueVariant := Codigo;
      CBSiapTerCad.KeyValue := Codigo;
    end;
  end;
end;

procedure TFmDiarioTDI_01_Pre.SbSiapTerCadClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMSiapTerCad, SbSiapTerCad);
end;

procedure TFmDiarioTDI_01_Pre.TPDtaExePrvExit(Sender: TObject);
begin
  if (*(ImgTipo.SQLType = stIns) and*) (TPFimExePrv.Date < 2) then
    TPFimExePrv.Date := TPDtaExePrv.Date;
end;

procedure TFmDiarioTDI_01_Pre.TPDtaVisPrvExit(Sender: TObject);
begin
  if (*(ImgTipo.SQLType = stIns) and*) (TPFimVisPrv.Date < 2) then
    TPFimVisPrv.Date := TPDtaVisPrv.Date;
end;

(*
object LaOSOrigem: TLabel
  Left = 488
  Top = 212
  Width = 75
  Height = 13
  Caption = 'OS Origem [F4]:'
  Color = clBtnFace
  ParentColor = False
end
object EdEmpresa: TdmkEdit
  Left = 488
  Top = 228
  Width = 80
  Height = 21
  Alignment = taRightJustify
  Enabled = False
  TabOrder = 17
  FormatType = dmktfInteger
  MskType = fmtNone
  DecimalSize = 0
  LeftZeros = 0
  NoEnterToTab = False
  NoForceUppercase = False
  ForceNextYear = False
  DataFormat = dmkdfShort
  HoraFormat = dmkhfShort
  Texto = '0'
  QryCampo = 'Empresa'
  UpdCampo = 'Empresa'
  UpdType = utYes
  Obrigatorio = False
  PermiteNulo = False
  ValueVariant = 0
end

*)

end.
