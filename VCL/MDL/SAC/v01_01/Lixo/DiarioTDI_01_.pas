unit DiarioTDI_01_;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ComCtrls, ExtCtrls, Buttons;

type
  TFmDiarioTDI_01_ = class(TForm)
    Label1: TLabel;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel4: TPanel;
    BtDesiste: TBitBtn;
    procedure FormShow(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
  private
    procedure HandleWndProc(var Msg: TMessage); message WM_SYSCOMMAND;
    procedure FechaOForm();
  public
    { Public declarations }
  end;

var
  FmDiarioTDI_01_: TFmDiarioTDI_01_;

implementation

uses Principal, DiarioTDI_01;

{$R *.dfm}

{ TFmDiarioTDI_01_ }

procedure TFmDiarioTDI_01_.BtConfirmaClick(Sender: TObject);
begin
  //....
  //
  FechaOForm();
end;

procedure TFmDiarioTDI_01_.BtDesisteClick(Sender: TObject);
begin
  FechaOForm();
end;

procedure TFmDiarioTDI_01_.FechaOForm();
begin
  Close;
  FmDiarioTDI_01.FecharAba(TTabSheet(Self.Owner));
end;

procedure TFmDiarioTDI_01_.FormShow(Sender: TObject);
begin
  if FmPrincipal.sd1.active then
    FmPrincipal.sd1.SkinForm(Handle);
end;

procedure TFmDiarioTDI_01_.HandleWndProc(var Msg: TMessage);
begin
  if Word(Msg.wParam) = 255 then
    FmDiarioTDI_01.AcoplarFormulario(Owner as TTabSheet)
  else
    inherited;
end;

end.
