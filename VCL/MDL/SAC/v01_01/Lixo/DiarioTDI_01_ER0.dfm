object FmDiarioTDI_01_ER0: TFmDiarioTDI_01_ER0
  Left = 339
  Top = 185
  Caption = 'ATD-TELEF-003 :: Entidade'
  ClientHeight = 609
  ClientWidth = 1000
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1000
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 952
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 1
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_M: TGroupBox
      Left = 84
      Top = 0
      Width = 868
      Height = 48
      Align = alClient
      TabOrder = 2
      object PnDados1: TPanel
        Left = 2
        Top = 15
        Width = 864
        Height = 31
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        Visible = False
        object Label25: TLabel
          Left = 4
          Top = 8
          Width = 31
          Height = 13
          Caption = 'Nome:'
        end
        object Label30: TLabel
          Left = 568
          Top = 8
          Width = 46
          Height = 13
          Caption = 'I.E. / RG:'
        end
        object Label29: TLabel
          Left = 368
          Top = 8
          Width = 61
          Height = 13
          Caption = 'CNPJ / CPF:'
        end
        object Label12: TLabel
          Left = 732
          Top = 8
          Width = 24
          Height = 13
          Caption = 'CEP:'
        end
        object Label141: TLabel
          Left = 244
          Top = 7
          Width = 35
          Height = 13
          Caption = 'UF RE:'
        end
        object EdCEP: TdmkEdit
          Left = 760
          Top = 4
          Width = 72
          Height = 21
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 4
          FormatType = dmktfString
          MskType = fmtCEP
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          QryCampo = 'PCEP'
          UpdCampo = 'PCEP'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
          OnEnter = EdCEPEnter
          OnExit = EdCEPExit
        end
        object BtCEP: TBitBtn
          Left = 836
          Top = 4
          Width = 21
          Height = 21
          Caption = '?'
          TabOrder = 5
          TabStop = False
          OnClick = BtCEPClick
        end
        object EdRazaoNome: TdmkEdit
          Left = 37
          Top = 4
          Width = 196
          Height = 21
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 0
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          QryCampo = 'Nome'
          UpdCampo = 'Nome'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
          OnChange = EdRazaoNomeChange
        end
        object EdCNPJCPF: TdmkEdit
          Left = 433
          Top = 4
          Width = 112
          Height = 21
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 2
          FormatType = dmktfString
          MskType = fmtCPFJ
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          QryCampo = 'CPF'
          UpdCampo = 'CPF'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
          OnExit = EdCNPJCPFExit
        end
        object EdIERG: TdmkEdit
          Left = 616
          Top = 4
          Width = 112
          Height = 21
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 3
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          QryCampo = 'RG'
          UpdCampo = 'RG'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object BtRF: TBitBtn
          Left = 544
          Top = 4
          Width = 21
          Height = 21
          Caption = '?'
          TabOrder = 6
          TabStop = False
          OnClick = BtRFClick
        end
        object CBRE: TComboBox
          Left = 284
          Top = 2
          Width = 41
          Height = 21
          AutoComplete = False
          CharCase = ecUpperCase
          TabOrder = 1
          OnChange = CBREChange
          OnClick = CBREClick
          Items.Strings = (
            'AC'
            'AL'
            'AM'
            'AP'
            'BA'
            'CE'
            'DF'
            'ES'
            'GO'
            'MA'
            'MG'
            'MS'
            'MT'
            'PA'
            'PB'
            'PE'
            'PI'
            'PR'
            'RJ'
            'RN'
            'RO'
            'RR'
            'RS'
            'SC'
            'SE'
            'SP'
            'TO'
            'SU')
        end
      end
    end
    object RGTipo: TRadioGroup
      Left = 0
      Top = 0
      Width = 84
      Height = 48
      Align = alLeft
      Caption = ' Cadastro: '
      Items.Strings = (
        'Empresa'
        'Pessoal')
      TabOrder = 0
      OnClick = RGTipoClick
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 1000
    Height = 447
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 1000
      Height = 447
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 1000
        Height = 447
        Align = alClient
        TabOrder = 0
        object PnDados2: TPanel
          Left = 2
          Top = 15
          Width = 996
          Height = 86
          Align = alTop
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          Visible = False
          object Label97: TLabel
            Left = 8
            Top = 4
            Width = 77
            Height = 13
            Caption = 'Tipo logradouro:'
          end
          object Label31: TLabel
            Left = 172
            Top = 4
            Width = 99
            Height = 13
            Caption = 'Nome do logradouro:'
            FocusControl = EdRua
          end
          object Label32: TLabel
            Left = 388
            Top = 4
            Width = 40
            Height = 13
            Caption = 'N'#250'mero:'
            FocusControl = EdNumero
          end
          object Label34: TLabel
            Left = 548
            Top = 4
            Width = 30
            Height = 13
            Caption = 'Bairro:'
            FocusControl = EdBairro
          end
          object Label35: TLabel
            Left = 704
            Top = 44
            Width = 98
            Height = 13
            Caption = 'Cidade (Texto Livre):'
            FocusControl = EdCidade
          end
          object Label38: TLabel
            Left = 8
            Top = 44
            Width = 17
            Height = 13
            Caption = 'UF:'
          end
          object Label15: TLabel
            Left = 848
            Top = 44
            Width = 87
            Height = 13
            Caption = 'Pa'#237's (Texto Livre):'
          end
          object Label16: TLabel
            Left = 440
            Top = 4
            Width = 67
            Height = 13
            Caption = 'Complemento:'
            FocusControl = EdCompl
          end
          object Label17: TLabel
            Left = 360
            Top = 44
            Width = 165
            Height = 13
            Caption = 'Ponto de refer'#234'ncia (do endere'#231'o):'
            FocusControl = EdEndeRef
          end
          object Label106: TLabel
            Left = 712
            Top = 4
            Width = 36
            Height = 13
            Caption = 'Cidade:'
          end
          object Label109: TLabel
            Left = 40
            Top = 44
            Width = 25
            Height = 13
            Caption = 'Pa'#237's:'
          end
          object Label112: TLabel
            Left = 244
            Top = 44
            Width = 83
            Height = 13
            Caption = 'Telefone 1 (NFe):'
          end
          object CBLograd: TdmkDBLookupComboBox
            Left = 48
            Top = 20
            Width = 120
            Height = 21
            KeyField = 'Codigo'
            ListField = 'Nome'
            ListSource = DsListaLograd
            TabOrder = 1
            dmkEditCB = EdLograd
            QryCampo = 'PLograd'
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
          object EdRua: TdmkEdit
            Left = 172
            Top = 20
            Width = 213
            Height = 21
            TabOrder = 2
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            QryCampo = 'PRua'
            UpdCampo = 'PRua'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
            OnChange = EdRuaChange
          end
          object EdNumero: TdmkEdit
            Left = 388
            Top = 20
            Width = 49
            Height = 21
            Alignment = taRightJustify
            TabOrder = 3
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            QryCampo = 'PNumero'
            UpdCampo = 'PNumero'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
            OnChange = EdNumeroChange
          end
          object EdBairro: TdmkEdit
            Left = 548
            Top = 20
            Width = 160
            Height = 21
            TabOrder = 5
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            QryCampo = 'PBairro'
            UpdCampo = 'PBairro'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdCidade: TdmkEdit
            Left = 704
            Top = 60
            Width = 140
            Height = 21
            TabOrder = 13
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            QryCampo = 'PCidade'
            UpdCampo = 'PCidade'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdUF: TdmkEdit
            Left = 8
            Top = 60
            Width = 28
            Height = 21
            CharCase = ecUpperCase
            TabOrder = 8
            FormatType = dmktfString
            MskType = fmtUF
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            QryCampo = 'PUF'
            UpdCampo = 'PUF'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdPais: TdmkEdit
            Left = 848
            Top = 60
            Width = 140
            Height = 21
            TabOrder = 14
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            QryCampo = 'PPais'
            UpdCampo = 'PPais'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdLograd: TdmkEditCB
            Left = 8
            Top = 20
            Width = 40
            Height = 21
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'PLograd'
            UpdCampo = 'PLograd'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            DBLookupComboBox = CBLograd
            IgnoraDBLookupComboBox = False
          end
          object EdCompl: TdmkEdit
            Left = 440
            Top = 20
            Width = 105
            Height = 21
            TabOrder = 4
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            QryCampo = 'PCompl'
            UpdCampo = 'PCompl'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdEndeRef: TdmkEdit
            Left = 360
            Top = 60
            Width = 333
            Height = 21
            TabOrder = 12
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            QryCampo = 'PEndeRef'
            UpdCampo = 'PEndeRef'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdCodMunici: TdmkEditCB
            Left = 712
            Top = 20
            Width = 52
            Height = 21
            Alignment = taRightJustify
            TabOrder = 6
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'PCodMunici'
            UpdCampo = 'PCodMunici'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            OnChange = EdCodMuniciChange
            DBLookupComboBox = CBCodMunici
            IgnoraDBLookupComboBox = False
          end
          object CBCodMunici: TdmkDBLookupComboBox
            Left = 764
            Top = 20
            Width = 225
            Height = 21
            KeyField = 'Codigo'
            ListField = 'Nome'
            ListSource = DsMunici
            TabOrder = 7
            OnClick = CBCodMuniciClick
            dmkEditCB = EdCodMunici
            QryCampo = 'PCodMunici'
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
          object EdCodiPais: TdmkEditCB
            Left = 40
            Top = 60
            Width = 36
            Height = 21
            Alignment = taRightJustify
            TabOrder = 9
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'PCodiPais'
            UpdCampo = 'PCodiPais'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            DBLookupComboBox = CBCodiPais
            IgnoraDBLookupComboBox = False
          end
          object CBCodiPais: TdmkDBLookupComboBox
            Left = 80
            Top = 60
            Width = 161
            Height = 21
            KeyField = 'Codigo'
            ListField = 'Nome'
            ListSource = DsBacen_Pais
            TabOrder = 10
            dmkEditCB = EdCodiPais
            QryCampo = 'PCodiPais'
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
          object EdTe1: TdmkEdit
            Left = 244
            Top = 60
            Width = 112
            Height = 21
            TabOrder = 11
            FormatType = dmktfString
            MskType = fmtTelLongo
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            QryCampo = 'PTe1'
            UpdCampo = 'PTe1'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
        end
        object PnDados3: TPanel
          Left = 2
          Top = 101
          Width = 996
          Height = 140
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 1
          Visible = False
          object Panel6: TPanel
            Left = 0
            Top = 0
            Width = 504
            Height = 140
            Align = alLeft
            BevelOuter = bvNone
            TabOrder = 0
            object GBCunsCad: TGroupBox
              Left = 0
              Top = 0
              Width = 504
              Height = 140
              Align = alClient
              Color = clBtnFace
              ParentBackground = False
              ParentColor = False
              TabOrder = 0
              Visible = False
              object Label24: TLabel
                Left = 8
                Top = 8
                Width = 89
                Height = 13
                Caption = 'Atividade principal:'
              end
              object Label1: TLabel
                Left = 8
                Top = 48
                Width = 65
                Height = 13
                Caption = 'Respons'#225'vel:'
              end
              object Label26: TLabel
                Left = 8
                Top = 88
                Width = 232
                Height = 13
                Caption = 'Como conheceu a empresa (forma de marketing):'
              end
              object Label27: TLabel
                Left = 384
                Top = 88
                Width = 65
                Height = 13
                Caption = 'Data contato:'
              end
              object SpeedButton5: TSpeedButton
                Left = 472
                Top = 24
                Width = 21
                Height = 21
                Caption = '...'
                OnClick = SpeedButton5Click
              end
              object SpeedButton6: TSpeedButton
                Left = 472
                Top = 64
                Width = 21
                Height = 21
                Caption = '...'
                OnClick = SpeedButton6Click
              end
              object SpeedButton7: TSpeedButton
                Left = 360
                Top = 104
                Width = 21
                Height = 21
                Caption = '...'
                OnClick = SpeedButton7Click
              end
              object EdAtivPrinc: TdmkEditCB
                Left = 8
                Top = 24
                Width = 56
                Height = 21
                Alignment = taRightJustify
                TabOrder = 0
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                QryCampo = 'AtivPrinc'
                UpdCampo = 'AtivPrinc'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
                DBLookupComboBox = CBAtivPrinc
                IgnoraDBLookupComboBox = False
              end
              object CBAtivPrinc: TdmkDBLookupComboBox
                Left = 64
                Top = 24
                Width = 405
                Height = 21
                KeyField = 'Codigo'
                ListField = 'Nome'
                ListSource = DsAtividades
                TabOrder = 1
                dmkEditCB = EdAtivPrinc
                QryCampo = 'AtivPrinc'
                UpdType = utYes
                LocF7SQLMasc = '$#'
              end
              object EdAccount: TdmkEditCB
                Left = 8
                Top = 64
                Width = 56
                Height = 21
                Alignment = taRightJustify
                TabOrder = 2
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                QryCampo = 'Account'
                UpdCampo = 'Account'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
                DBLookupComboBox = CBAccount
                IgnoraDBLookupComboBox = False
              end
              object CBAccount: TdmkDBLookupComboBox
                Left = 64
                Top = 64
                Width = 405
                Height = 21
                KeyField = 'Codigo'
                ListField = 'Nome'
                ListSource = DsAccounts
                TabOrder = 3
                dmkEditCB = EdAccount
                QryCampo = 'Account'
                UpdType = utYes
                LocF7SQLMasc = '$#'
              end
              object EdHowFind: TdmkEditCB
                Left = 8
                Top = 104
                Width = 36
                Height = 21
                Alignment = taRightJustify
                TabOrder = 4
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                QryCampo = 'HowFind'
                UpdCampo = 'HowFind'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
                DBLookupComboBox = CBHowFind
                IgnoraDBLookupComboBox = False
              end
              object CBHowFind: TdmkDBLookupComboBox
                Left = 48
                Top = 104
                Width = 309
                Height = 21
                KeyField = 'Codigo'
                ListField = 'Nome'
                ListSource = DsHowFounded
                TabOrder = 5
                dmkEditCB = EdHowFind
                QryCampo = 'HowFind'
                UpdType = utYes
                LocF7SQLMasc = '$#'
              end
              object TPDataCon: TdmkEditDateTimePicker
                Left = 384
                Top = 104
                Width = 108
                Height = 21
                Date = 41058.487372523140000000
                Time = 41058.487372523140000000
                TabOrder = 6
                ReadOnly = False
                DefaultEditMask = '!99/99/99;1;_'
                AutoApplyEditMask = True
                QryCampo = 'DataCon'
                UpdCampo = 'DataCon'
                UpdType = utYes
              end
            end
          end
          object GroupBox4: TGroupBox
            Left = 504
            Top = 0
            Width = 492
            Height = 140
            Align = alClient
            Caption = ' Tipo de cadastro: '
            TabOrder = 1
            object CkCliente1_0: TdmkCheckBox
              Left = 8
              Top = 16
              Width = 152
              Height = 17
              Caption = 'Cliente1'
              TabOrder = 0
              QryCampo = 'Cliente1'
              UpdCampo = 'Cliente1'
              UpdType = utYes
              ValCheck = 'V'
              ValUncheck = 'F'
              OldValor = #0
            end
            object CkFornece1_0: TdmkCheckBox
              Left = 164
              Top = 16
              Width = 152
              Height = 17
              Caption = 'Fornece 1'
              TabOrder = 4
              QryCampo = 'Fornece1'
              UpdCampo = 'Fornece1'
              UpdType = utYes
              ValCheck = 'V'
              ValUncheck = 'F'
              OldValor = #0
            end
            object CkFornece2_0: TdmkCheckBox
              Left = 164
              Top = 32
              Width = 152
              Height = 17
              Caption = 'Fornece 2'
              TabOrder = 5
              Visible = False
              QryCampo = 'Fornece2'
              UpdCampo = 'Fornece2'
              UpdType = utYes
              ValCheck = 'V'
              ValUncheck = 'F'
              OldValor = #0
            end
            object CkFornece3_0: TdmkCheckBox
              Left = 164
              Top = 48
              Width = 152
              Height = 17
              Caption = 'Fornece 3'
              TabOrder = 6
              Visible = False
              QryCampo = 'Fornece3'
              UpdCampo = 'Fornece3'
              UpdType = utYes
              ValCheck = 'V'
              ValUncheck = 'F'
              OldValor = #0
            end
            object CkFornece4_0: TdmkCheckBox
              Left = 164
              Top = 64
              Width = 152
              Height = 17
              Caption = 'Fornece 4'
              TabOrder = 7
              Visible = False
              QryCampo = 'Fornece4'
              UpdCampo = 'Fornece4'
              UpdType = utYes
              ValCheck = 'V'
              ValUncheck = 'F'
              OldValor = #0
            end
            object CkTerceiro_0: TdmkCheckBox
              Left = 320
              Top = 80
              Width = 152
              Height = 17
              Caption = 'Terceiro'
              TabOrder = 12
              QryCampo = 'Terceiro'
              UpdCampo = 'Terceiro'
              UpdType = utYes
              ValCheck = 'V'
              ValUncheck = 'F'
              OldValor = #0
            end
            object CkCliente2_0: TdmkCheckBox
              Left = 8
              Top = 32
              Width = 152
              Height = 17
              Caption = 'Cliente 2'
              TabOrder = 1
              Visible = False
              QryCampo = 'Cliente2'
              UpdCampo = 'Cliente2'
              UpdType = utYes
              ValCheck = 'V'
              ValUncheck = 'F'
              OldValor = #0
            end
            object CkFornece5_0: TdmkCheckBox
              Left = 320
              Top = 16
              Width = 152
              Height = 17
              Caption = 'Fornece 5'
              TabOrder = 8
              Visible = False
              QryCampo = 'Fornece5'
              UpdCampo = 'Fornece5'
              UpdType = utYes
              ValCheck = 'V'
              ValUncheck = 'F'
              OldValor = #0
            end
            object CkFornece6_0: TdmkCheckBox
              Left = 320
              Top = 32
              Width = 152
              Height = 17
              Caption = 'Fornece 6'
              TabOrder = 9
              Visible = False
              QryCampo = 'Fornece6'
              UpdCampo = 'Fornece6'
              UpdType = utYes
              ValCheck = 'V'
              ValUncheck = 'F'
              OldValor = #0
            end
            object CkCliente4_0: TdmkCheckBox
              Left = 8
              Top = 64
              Width = 152
              Height = 17
              Caption = 'Cliente 4'
              TabOrder = 3
              Visible = False
              QryCampo = 'Cliente4'
              UpdCampo = 'Cliente4'
              UpdType = utYes
              ValCheck = 'V'
              ValUncheck = 'F'
              OldValor = #0
            end
            object CkCliente3_0: TdmkCheckBox
              Left = 8
              Top = 48
              Width = 152
              Height = 17
              Caption = 'Cliente 3'
              TabOrder = 2
              Visible = False
              QryCampo = 'Cliente3'
              UpdCampo = 'Cliente3'
              UpdType = utYes
              ValCheck = 'V'
              ValUncheck = 'F'
              OldValor = #0
            end
            object CkFornece8_0: TdmkCheckBox
              Left = 320
              Top = 64
              Width = 152
              Height = 17
              Caption = 'Fornece 8'
              TabOrder = 11
              Visible = False
              QryCampo = 'Fornece8'
              UpdCampo = 'Fornece8'
              UpdType = utYes
              ValCheck = 'V'
              ValUncheck = 'F'
              OldValor = #0
            end
            object CkFornece7_0: TdmkCheckBox
              Left = 320
              Top = 48
              Width = 152
              Height = 17
              Caption = 'Fornece 7'
              TabOrder = 10
              Visible = False
              QryCampo = 'Fornece7'
              UpdCampo = 'Fornece7'
              UpdType = utYes
              ValCheck = 'V'
              ValUncheck = 'F'
              OldValor = #0
            end
          end
        end
        object Panel5: TPanel
          Left = 2
          Top = 241
          Width = 996
          Height = 204
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 2
          object Panel7: TPanel
            Left = 0
            Top = 0
            Width = 996
            Height = 49
            Align = alTop
            BevelOuter = bvNone
            TabOrder = 0
            object Label2: TLabel
              Left = 192
              Top = 8
              Width = 99
              Height = 13
              Caption = 'Nome do logradouro:'
              FocusControl = EdPsqRua
            end
            object Label3: TLabel
              Left = 408
              Top = 8
              Width = 40
              Height = 13
              Caption = 'N'#250'mero:'
              FocusControl = EdPsqNumero
            end
            object Label4: TLabel
              Left = 460
              Top = 8
              Width = 36
              Height = 13
              Caption = 'Cidade:'
            end
            object Label5: TLabel
              Left = 740
              Top = 8
              Width = 17
              Height = 13
              Caption = 'UF:'
            end
            object RGPesquisa: TRadioGroup
              Left = 0
              Top = 0
              Width = 185
              Height = 49
              Align = alLeft
              Caption = ' Pesquisa de endere'#231'os pr'#243'ximos:'
              Columns = 2
              ItemIndex = 0
              Items.Strings = (
                'Autom'#225'tico'
                'Manual')
              TabOrder = 0
            end
            object EdPsqRua: TdmkEdit
              Left = 192
              Top = 24
              Width = 213
              Height = 21
              TabOrder = 1
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              QryCampo = 'PRua'
              UpdCampo = 'PRua'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
              OnChange = EdPsqRuaChange
            end
            object EdPsqNumero: TdmkEdit
              Left = 408
              Top = 24
              Width = 49
              Height = 21
              Alignment = taRightJustify
              TabOrder = 2
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              QryCampo = 'PNumero'
              UpdCampo = 'PNumero'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
              OnChange = EdPsqNumeroChange
            end
            object EdPsqCidade: TdmkEditCB
              Left = 460
              Top = 24
              Width = 52
              Height = 21
              Alignment = taRightJustify
              TabOrder = 3
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              QryCampo = 'PCodMunici'
              UpdCampo = 'PCodMunici'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              OnChange = EdPsqCidadeChange
              DBLookupComboBox = CBPsqCidade
              IgnoraDBLookupComboBox = False
            end
            object CBPsqCidade: TdmkDBLookupComboBox
              Left = 512
              Top = 24
              Width = 225
              Height = 21
              KeyField = 'Codigo'
              ListField = 'Nome'
              ListSource = DsMunici
              TabOrder = 4
              dmkEditCB = EdPsqCidade
              QryCampo = 'PCodMunici'
              UpdType = utYes
              LocF7SQLMasc = '$#'
            end
            object EdPsqUF: TdmkEdit
              Left = 740
              Top = 24
              Width = 28
              Height = 21
              CharCase = ecUpperCase
              TabOrder = 5
              FormatType = dmktfString
              MskType = fmtUF
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              QryCampo = 'PUF'
              UpdCampo = 'PUF'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
              OnChange = EdPsqUFChange
            end
            object BtPesquisa: TBitBtn
              Tag = 20
              Left = 780
              Top = 7
              Width = 120
              Height = 40
              Cursor = crHandPoint
              Caption = '&Pesquisa'
              NumGlyphs = 2
              ParentShowHint = False
              ShowHint = True
              TabOrder = 6
              OnClick = BtPesquisaClick
            end
          end
          object dmkDBGridZTO1: TdmkDBGridZTO
            Left = 0
            Top = 49
            Width = 996
            Height = 155
            Align = alClient
            DataSource = DsPsq
            Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
            TabOrder = 1
            TitleFont.Charset = ANSI_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            RowColors = <>
            Columns = <
              item
                Expanded = False
                FieldName = 'Cliente'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NO_ENT'
                Title.Caption = 'Nome do cliente'
                Width = 180
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Codigo'
                Title.Caption = 'ID Lugar'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Nome'
                Title.Caption = 'Descri'#231#227'o do lugar'
                Width = 140
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'SLograd'
                Title.Caption = 'Tp Logr.'
                Width = 62
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'SRua'
                Title.Caption = 'Nome do logradouro'
                Width = 140
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'SNumero'
                Title.Caption = 'N'#250'mero'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'SBairro'
                Title.Caption = 'Bairro'
                Width = 100
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'SCidade'
                Title.Caption = 'Cidade'
                Width = 100
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'SUF'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'STe1'
                Title.Caption = 'Telefone 1 (NFe)'
                Width = 112
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'SEndeRef'
                Title.Caption = 'Ponto de refer'#234'ncia do endere'#231'o'
                Width = 300
                Visible = True
              end>
          end
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 495
    Width = 1000
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 996
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 539
    Width = 1000
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 854
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 852
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object QrLoc: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT CNPJ_CPF'
      'FROM cadastro_com_itens_its'
      'WHERE CNPJ_CPF=:P0'
      'AND Codigo=:P1')
    Left = 201
    Top = 57
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrLocCNPJ_CPF: TWideStringField
      FieldName = 'CNPJ_CPF'
      Size = 15
    end
  end
  object PMCEP: TPopupMenu
    Left = 168
    Top = 152
    object Descobrir1: TMenuItem
      Caption = 'BD - &Descobrir'
      OnClick = Descobrir1Click
    end
    object Verificar1: TMenuItem
      Caption = 'BD - &Verificar'
      OnClick = Verificar1Click
    end
    object Internet1: TMenuItem
      Caption = 'Internet - Verificar'
      OnClick = Internet1Click
    end
  end
  object QrDuplic2: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo FROM entidades'
      'WHERE CNPJ=:P0'
      'AND Codigo <> :P1')
    Left = 196
    Top = 152
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrDuplic2Codigo: TIntegerField
      FieldName = 'Codigo'
    end
  end
  object QrListaLograd: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome '
      'FROM listalograd'
      'ORDER BY Nome')
    Left = 224
    Top = 152
    object QrListaLogradCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrListaLogradNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 10
    end
  end
  object DsListaLograd: TDataSource
    DataSet = QrListaLograd
    Left = 252
    Top = 152
  end
  object QrMunici: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM dtb_munici'
      'ORDER BY Nome')
    Left = 280
    Top = 152
    object QrMuniciCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrMuniciNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
  object DsMunici: TDataSource
    DataSet = QrMunici
    Left = 308
    Top = 152
  end
  object QrBacen_Pais: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM bacen_pais'
      'ORDER BY Nome'
      '')
    Left = 336
    Top = 152
    object QrBacen_PaisCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrBacen_PaisNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
  object DsBacen_Pais: TDataSource
    DataSet = QrBacen_Pais
    Left = 364
    Top = 152
  end
  object QrHowFounded: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM howfounded'
      'ORDER BY Nome')
    Left = 280
    Top = 180
    object QrHowFoundedCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrHowFoundedNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
  end
  object DsHowFounded: TDataSource
    DataSet = QrHowFounded
    Left = 308
    Top = 180
  end
  object QrAccounts: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM entidades'
      'WHERE Nome <> '#39#39' '
      'AND Fornece2="V"'
      'ORDER BY Nome')
    Left = 336
    Top = 180
    object QrAccountsCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrAccountsNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
  object DsAccounts: TDataSource
    DataSet = QrAccounts
    Left = 364
    Top = 180
  end
  object QrAtividades: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM atividades'
      'ORDER BY Nome')
    Left = 224
    Top = 180
    object QrAtividadesCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrAtividadesNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
  end
  object DsAtividades: TDataSource
    DataSet = QrAtividades
    Left = 252
    Top = 180
  end
  object CsAbaOrigem: TdmkCompoStore
    Left = 212
    Top = 65524
  end
  object VaCodigo: TdmkVariable
    ValueVariant = 0
    Left = 180
    Top = 65524
  end
  object CsQry: TdmkCompoStore
    Left = 240
    Top = 65524
  end
  object CsEd: TdmkCompoStore
    Left = 268
    Top = 65524
  end
  object CsCB: TdmkCompoStore
    Left = 296
    Top = 65524
  end
  object QrPesq: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_ENT, '
      'stc.*'
      'FROM siaptercad stc '
      'LEFT JOIN entidades ent ON ent.Codigo=stc.Cliente'
      'WHERE stc.SRua LIKE "%neo al%"'
      'AND stc.SCidade LIKE "%maringa%"'
      'AND stc.SUF = "PR"'
      'ORDER BY stc.SNumero')
    Left = 248
    Top = 388
    object QrPesqCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPesqNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QrPesqCliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrPesqSLograd: TSmallintField
      FieldName = 'SLograd'
    end
    object QrPesqSRua: TWideStringField
      FieldName = 'SRua'
      Size = 60
    end
    object QrPesqSNumero: TIntegerField
      FieldName = 'SNumero'
    end
    object QrPesqSCompl: TWideStringField
      FieldName = 'SCompl'
      Size = 60
    end
    object QrPesqSBairro: TWideStringField
      FieldName = 'SBairro'
      Size = 60
    end
    object QrPesqSCidade: TWideStringField
      FieldName = 'SCidade'
      Size = 60
    end
    object QrPesqSUF: TWideStringField
      FieldName = 'SUF'
      Size = 3
    end
    object QrPesqSCEP: TIntegerField
      FieldName = 'SCEP'
    end
    object QrPesqSPais: TWideStringField
      FieldName = 'SPais'
      Size = 60
    end
    object QrPesqSEndeRef: TWideStringField
      FieldName = 'SEndeRef'
      Size = 100
    end
    object QrPesqSCodMunici: TIntegerField
      FieldName = 'SCodMunici'
    end
    object QrPesqSCodiPais: TIntegerField
      FieldName = 'SCodiPais'
    end
    object QrPesqSTe1: TWideStringField
      FieldName = 'STe1'
    end
    object QrPesqM2Constru: TFloatField
      FieldName = 'M2Constru'
    end
    object QrPesqM2NaoBuild: TFloatField
      FieldName = 'M2NaoBuild'
    end
    object QrPesqM2Terreno: TFloatField
      FieldName = 'M2Terreno'
    end
    object QrPesqM2Total: TFloatField
      FieldName = 'M2Total'
    end
    object QrPesqLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrPesqDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrPesqDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrPesqUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrPesqUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrPesqAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrPesqAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrPesqLstCusPrd: TIntegerField
      FieldName = 'LstCusPrd'
    end
    object QrPesqPdrMntsMon: TIntegerField
      FieldName = 'PdrMntsMon'
    end
    object QrPesqLatitude: TFloatField
      FieldName = 'Latitude'
    end
    object QrPesqLongitude: TFloatField
      FieldName = 'Longitude'
    end
    object QrPesqRotaLatLon: TIntegerField
      FieldName = 'RotaLatLon'
    end
    object QrPesqNO_ENT: TWideStringField
      FieldName = 'NO_ENT'
      Size = 100
    end
  end
  object DsPsq: TDataSource
    DataSet = QrPesq
    Left = 244
    Top = 436
  end
end
