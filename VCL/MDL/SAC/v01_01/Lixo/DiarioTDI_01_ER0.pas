unit DiarioTDI_01_ER0;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnMLAGeral, UnInternalConsts,
  dmkImage, Mask, Menus, dmkCheckBox, dmkVariable, dmkCompoStore, Variants,
  UnDmkEnums, dmkDBGridZTO;

type
  TFmDiarioTDI_01_ER0 = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_M: TGroupBox;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    QrLoc: TmySQLQuery;
    QrLocCNPJ_CPF: TWideStringField;
    PnDados2: TPanel;
    Label97: TLabel;
    Label31: TLabel;
    Label32: TLabel;
    Label34: TLabel;
    Label35: TLabel;
    Label38: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    Label106: TLabel;
    Label109: TLabel;
    Label112: TLabel;
    CBLograd: TdmkDBLookupComboBox;
    EdRua: TdmkEdit;
    EdNumero: TdmkEdit;
    EdBairro: TdmkEdit;
    EdCidade: TdmkEdit;
    EdUF: TdmkEdit;
    EdPais: TdmkEdit;
    EdLograd: TdmkEditCB;
    EdCompl: TdmkEdit;
    EdEndeRef: TdmkEdit;
    EdCodMunici: TdmkEditCB;
    CBCodMunici: TdmkDBLookupComboBox;
    EdCodiPais: TdmkEditCB;
    CBCodiPais: TdmkDBLookupComboBox;
    EdTe1: TdmkEdit;
    PMCEP: TPopupMenu;
    Descobrir1: TMenuItem;
    Verificar1: TMenuItem;
    Internet1: TMenuItem;
    QrDuplic2: TmySQLQuery;
    QrDuplic2Codigo: TIntegerField;
    QrListaLograd: TmySQLQuery;
    QrListaLogradCodigo: TIntegerField;
    QrListaLogradNome: TWideStringField;
    DsListaLograd: TDataSource;
    QrMunici: TmySQLQuery;
    QrMuniciCodigo: TIntegerField;
    QrMuniciNome: TWideStringField;
    DsMunici: TDataSource;
    QrBacen_Pais: TmySQLQuery;
    QrBacen_PaisCodigo: TIntegerField;
    QrBacen_PaisNome: TWideStringField;
    DsBacen_Pais: TDataSource;
    QrHowFounded: TmySQLQuery;
    QrHowFoundedCodigo: TIntegerField;
    QrHowFoundedNome: TWideStringField;
    DsHowFounded: TDataSource;
    QrAccounts: TmySQLQuery;
    QrAccountsCodigo: TIntegerField;
    QrAccountsNome: TWideStringField;
    DsAccounts: TDataSource;
    QrAtividades: TmySQLQuery;
    QrAtividadesCodigo: TIntegerField;
    QrAtividadesNome: TWideStringField;
    DsAtividades: TDataSource;
    PnDados1: TPanel;
    Label25: TLabel;
    Label30: TLabel;
    Label29: TLabel;
    Label12: TLabel;
    EdCEP: TdmkEdit;
    BtCEP: TBitBtn;
    EdRazaoNome: TdmkEdit;
    EdCNPJCPF: TdmkEdit;
    EdIERG: TdmkEdit;
    RGTipo: TRadioGroup;
    PnDados3: TPanel;
    Panel6: TPanel;
    GBCunsCad: TGroupBox;
    Label24: TLabel;
    Label1: TLabel;
    Label26: TLabel;
    Label27: TLabel;
    SpeedButton5: TSpeedButton;
    SpeedButton6: TSpeedButton;
    SpeedButton7: TSpeedButton;
    EdAtivPrinc: TdmkEditCB;
    CBAtivPrinc: TdmkDBLookupComboBox;
    EdAccount: TdmkEditCB;
    CBAccount: TdmkDBLookupComboBox;
    EdHowFind: TdmkEditCB;
    CBHowFind: TdmkDBLookupComboBox;
    TPDataCon: TdmkEditDateTimePicker;
    GroupBox4: TGroupBox;
    CkCliente1_0: TdmkCheckBox;
    CkFornece1_0: TdmkCheckBox;
    CkFornece2_0: TdmkCheckBox;
    CkFornece3_0: TdmkCheckBox;
    CkFornece4_0: TdmkCheckBox;
    CkTerceiro_0: TdmkCheckBox;
    CkCliente2_0: TdmkCheckBox;
    CkFornece5_0: TdmkCheckBox;
    CkFornece6_0: TdmkCheckBox;
    CkCliente4_0: TdmkCheckBox;
    CkCliente3_0: TdmkCheckBox;
    CkFornece8_0: TdmkCheckBox;
    CkFornece7_0: TdmkCheckBox;
    CsAbaOrigem: TdmkCompoStore;
    VaCodigo: TdmkVariable;
    CsQry: TdmkCompoStore;
    CsEd: TdmkCompoStore;
    CsCB: TdmkCompoStore;
    BtRF: TBitBtn;
    CBRE: TComboBox;
    Label141: TLabel;
    Panel5: TPanel;
    Panel7: TPanel;
    RGPesquisa: TRadioGroup;
    Label2: TLabel;
    EdPsqRua: TdmkEdit;
    Label3: TLabel;
    EdPsqNumero: TdmkEdit;
    Label4: TLabel;
    EdPsqCidade: TdmkEditCB;
    CBPsqCidade: TdmkDBLookupComboBox;
    EdPsqUF: TdmkEdit;
    Label5: TLabel;
    dmkDBGridZTO1: TdmkDBGridZTO;
    QrPesq: TmySQLQuery;
    DsPsq: TDataSource;
    QrPesqCodigo: TIntegerField;
    QrPesqNome: TWideStringField;
    QrPesqCliente: TIntegerField;
    QrPesqSLograd: TSmallintField;
    QrPesqSRua: TWideStringField;
    QrPesqSNumero: TIntegerField;
    QrPesqSCompl: TWideStringField;
    QrPesqSBairro: TWideStringField;
    QrPesqSCidade: TWideStringField;
    QrPesqSUF: TWideStringField;
    QrPesqSCEP: TIntegerField;
    QrPesqSPais: TWideStringField;
    QrPesqSEndeRef: TWideStringField;
    QrPesqSCodMunici: TIntegerField;
    QrPesqSCodiPais: TIntegerField;
    QrPesqSTe1: TWideStringField;
    QrPesqM2Constru: TFloatField;
    QrPesqM2NaoBuild: TFloatField;
    QrPesqM2Terreno: TFloatField;
    QrPesqM2Total: TFloatField;
    QrPesqLk: TIntegerField;
    QrPesqDataCad: TDateField;
    QrPesqDataAlt: TDateField;
    QrPesqUserCad: TIntegerField;
    QrPesqUserAlt: TIntegerField;
    QrPesqAlterWeb: TSmallintField;
    QrPesqAtivo: TSmallintField;
    QrPesqLstCusPrd: TIntegerField;
    QrPesqPdrMntsMon: TIntegerField;
    QrPesqLatitude: TFloatField;
    QrPesqLongitude: TFloatField;
    QrPesqRotaLatLon: TIntegerField;
    BtPesquisa: TBitBtn;
    QrPesqNO_ENT: TWideStringField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure EdCEPExit(Sender: TObject);
    procedure EdCEPEnter(Sender: TObject);
    procedure EdCNPJCPFExit(Sender: TObject);
    procedure BtCEPClick(Sender: TObject);
    procedure Descobrir1Click(Sender: TObject);
    procedure Verificar1Click(Sender: TObject);
    procedure Internet1Click(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure RGTipoClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure EdRazaoNomeChange(Sender: TObject);
    procedure SpeedButton7Click(Sender: TObject);
    procedure SpeedButton5Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure BtRFClick(Sender: TObject);
    procedure SpeedButton6Click(Sender: TObject);
    procedure CBREClick(Sender: TObject);
    procedure CBREChange(Sender: TObject);
    procedure BtPesquisaClick(Sender: TObject);
    procedure EdPsqRuaChange(Sender: TObject);
    procedure EdPsqNumeroChange(Sender: TObject);
    procedure EdPsqCidadeChange(Sender: TObject);
    procedure EdPsqUFChange(Sender: TObject);
    procedure EdRuaChange(Sender: TObject);
    procedure EdNumeroChange(Sender: TObject);
    procedure EdCodMuniciChange(Sender: TObject);
    procedure CBCodMuniciClick(Sender: TObject);
  private
    { Private declarations }
    FCEP: String;
    //
    procedure HandleWndProc(var Msg: TMessage); message WM_SYSCOMMAND;
    //
    function  CNPJCPFDuplicado(): Boolean;
    function  UpdateCunsCad(Codigo: Integer): Boolean;
    procedure FecharOForm(Confirma: Boolean; Codigo: Integer);
    procedure PesquisaSINTEGRA();
    procedure ReopenPesqEndereco(Manual: Boolean = False);
  public
    { Public declarations }
    FFmCriou: TForm;
  end;

  var
  FmDiarioTDI_01_ER0: TFmDiarioTDI_01_ER0;

implementation

uses DiarioTDI_01, UnMyObjects, Module, UMySQLModule, EntiCEP, MyDBCheck, UnCEP,
  UnEntities, CfgCadLista, Principal, ModuleGeral, UnConsultasWeb, MyGlyfs,
  DmkDAC_PF;

{$R *.DFM}

procedure TFmDiarioTDI_01_ER0.BtCEPClick(Sender: TObject);
begin
  MyObjects.MostraPMCEP(PMCEP, BtCEP);
  try
    EdNumero.SetFocus;
  except
    ;
  end;
end;

procedure TFmDiarioTDI_01_ER0.BtOKClick(Sender: TObject);
var
  Codigo, CodUsu, Tipo, ECEP, ELograd, ENumero, EUF, ECodMunici, ECodiPais,
  PCEP, PLograd, PNumero, PUF, PCodMunici, PCodiPais, MaiorCodigo: Integer;
  //
  Cliente1, Cliente2, Cliente3, Cliente4, Fornece1, Fornece2, Fornece3, Fornece4, Fornece5, Fornece6, Fornece7, Fornece8, Terceiro,
  RazaoSocial, CNPJ, IE, ERua, ECompl, EBairro, ECidade, EPais, ETe1, EEndeRef,
  Nome, CPF, RG, PRua, PCompl, PBairro, PCidade, PPais, PTe1, PEndeRef: String;
begin
  if CNPJCPFDuplicado() then
    Exit;
  Tipo := RGTipo.ItemIndex;
  //
  if Tipo = 0 then
    if Geral.Valida_IE(EdIERG.Text, EdUF.Text, '??', False) = False then
    begin
      if Geral.MensagemBox(
      'Deseja continuar mesmo com d�vidas na valida��o da Inscri��o Estadual?',
      'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) <> ID_YES then
    Exit;
  end;
  //
  if not Entities.TipoDeCadastroDefinido(CkCliente1_0, CkCliente2_0,
  CkCliente3_0, CkCliente4_0, CkFornece1_0, CkFornece2_0, CkFornece3_0,
  CkFornece4_0, CkFornece5_0, CkFornece6_0, CkFornece7_0, CkFornece8_0,
  CkTerceiro_0, nil) then
    Exit;
  RazaoSocial  := '';
  CNPJ         := '';
  IE           := '';
  ECEP         := 0;
  ELograd      := 0;
  ERua         := '';
  ENumero      := 0;
  ECompl       := '';
  EBairro      := '';
  ECidade      := '';
  EUF          := 0;
  EPais        := '';
  ETe1         := '';
  EEndeRef     := '';
  ECodMunici   := 0;
  ECodiPais    := 0;
  //
  Nome         := '';
  CPF          := '';
  RG           := '';
  PCEP         := 0;
  PLograd      := 0;
  PRua         := '';
  PNumero      := 0;
  PCompl       := '';
  PBairro      := '';
  PCidade      := '';
  PUF          := 0;
  PPais        := '';
  PTe1         := '';
  PEndeRef     := '';
  PCodMunici   := 0;
  PCodiPais    := 0;
  //
  case Tipo of
    0:
    begin
      RazaoSocial  := EdRazaoNome.Text;
      CNPJ         := Geral.SoNumero_TT(EdCNPJCPF.Text);
      IE           := Geral.SoNumero_TT(EdIERG.Text);
      //
      ECEP         := Geral.IMV(Geral.SoNumero_TT(EdCEP.ValueVariant));
      ELograd      := EdLograd.ValueVariant;
      ERua         := EdRua.Text;
      ENumero      := Geral.IMV(Geral.SoNumero_TT(EdNumero.Text));
      ECompl       := EdCompl.Text;
      EBairro      := EdBairro.Text;
      ECidade      := EdCidade.Text;
      EUF          := Geral.GetCodigoUF_da_SiglaUF(EdUF.Text);
      EPais        := EdPais.Text;
      ETe1         := Geral.SoNumero_TT(EdTe1.Text);
      EEndeRef     := EdEndeRef.Text;
      ECodMunici   := EdCodMunici.ValueVariant;
      ECodiPais    := EdCodiPais.ValueVariant;
    end;
    1:
    begin
      Nome         := EdRazaoNome.Text;
      CPF          := Geral.SoNumero_TT(EdCNPJCPF.Text);
      RG           := Geral.SoNumero_TT(EdIERG.Text);
      //
      PCEP         := Geral.IMV(Geral.SoNumero_TT(EdCEP.Text));
      PLograd      := EdLograd.ValueVariant;
      PRua         := EdRua.Text;
      PNumero      := Geral.IMV(Geral.SoNumero_TT(EdNumero.Text));
      PCompl       := EdCompl.Text;
      PBairro      := EdBairro.Text;
      PCidade      := EdCidade.Text;
      PUF          := Geral.GetCodigoUF_da_SiglaUF(EdUF.Text);
      PPais        := EdPais.Text;
      PTe1         := Geral.SoNumero_TT(EdTe1.Text);
      PEndeRef     := EdEndeRef.Text;
      PCodMunici   := EdCodMunici.ValueVariant;
      PCodiPais    := EdCodiPais.ValueVariant;
    end;
    else
      Exit;
  end;
  if CkCliente1_0.Checked then Cliente1 := 'V' else Cliente1 := 'F';
  if CkCliente2_0.Checked then Cliente2 := 'V' else Cliente2 := 'F';
  if CkCliente3_0.Checked then Cliente3 := 'V' else Cliente3 := 'F';
  if CkCliente4_0.Checked then Cliente4 := 'V' else Cliente4 := 'F';
  if CkFornece1_0.Checked then Fornece1 := 'V' else Fornece1 := 'F';
  if CkFornece2_0.Checked then Fornece2 := 'V' else Fornece2 := 'F';
  if CkFornece3_0.Checked then Fornece3 := 'V' else Fornece3 := 'F';
  if CkFornece4_0.Checked then Fornece4 := 'V' else Fornece4 := 'F';
  if CkFornece5_0.Checked then Fornece5 := 'V' else Fornece5 := 'F';
  if CkFornece6_0.Checked then Fornece6 := 'V' else Fornece6 := 'F';
  if CkFornece7_0.Checked then Fornece7 := 'V' else Fornece7 := 'F';
  if CkFornece8_0.Checked then Fornece8 := 'V' else Fornece8 := 'F';
  if CkTerceiro_0.Checked then Terceiro := 'V' else Terceiro := 'F';
  //
  Codigo := UMyMod.BuscaEmLivreY_Def('Entidades', 'Codigo', ImgTipo.SQLType,
    VaCodigo.ValueVariant);
  CodUsu := Codigo;
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'entidades', False, [
  'Cliente1', 'Cliente2', 'Cliente3', 'Cliente4',
  'Fornece1', 'Fornece2', 'Fornece3', 'Fornece4',
  'Fornece5', 'Fornece6', 'Fornece7', 'Fornece8',
  'Terceiro', 'Tipo',
  'RazaoSocial', 'CNPJ', 'IE', 'ECEP', 'ELograd', 'ERua',
  'ENumero', 'ECompl', 'EBairro', 'ECidade', 'EUF',
  'EPais', 'ETe1', 'EEndeRef', 'ECodMunici', 'ECodiPais',
  'Nome', 'CPF', 'RG', 'PCEP', 'PLograd', 'PRua',
  'PNumero', 'PCompl', 'PBairro', 'PCidade', 'PUF',
  'PPais', 'PTe1', 'PEndeRef', 'PCodMunici', 'PCodiPais'
  ], ['Codigo', 'CodUsu'
  ], [
  Cliente1, Cliente2, Cliente3, Cliente4,
  Fornece1, Fornece2, Fornece3, Fornece4,
  Fornece5, Fornece6, Fornece7, Fornece8,
  Terceiro, Tipo,
  RazaoSocial, CNPJ, IE, ECEP, ELograd, ERua,
  ENumero, ECompl, EBairro, ECidade, EUF,
  EPais, ETe1, EEndeRef, ECodMunici, ECodiPais,
  Nome, CPF, RG, PCEP, PLograd, PRua,
  PNumero, PCompl, PBairro, PCidade, PUF,
  PPais, PTe1, PEndeRef, PCodMunici, PCodiPais
  ], [Codigo, CodUsu
  ], True) then
  begin
    VaCodigo.ValueVariant := Codigo;
    Dmod.AchouNovosClientes(MaiorCodigo);
    UpdateCunsCad(Codigo);
    FecharOForm(True, Codigo);
  end;
end;

procedure TFmDiarioTDI_01_ER0.BtPesquisaClick(Sender: TObject);
begin
  ReopenPesqEndereco(True);
end;

procedure TFmDiarioTDI_01_ER0.BtRFClick(Sender: TObject);
const
  CBCodCNAE = nil;
  EdCodCNAE = nil;
begin
  case RGTipo.ItemIndex of
    0:
      UConsultasWeb.ConsultaCNPJ(EdCNPJCPF, EdIERG, EdRazaoNome, EdCep, EdRua,
        EdLograd, EdCompl, EdNumero, EdBairro, EdCidade, EdUF, EdPais,
        CBLograd, CBCodMunici, CBCodiPais, CBCodCNAE, EdCodMunici,
        EdCodiPais, EdCodCNAE);
    1:
      UConsultasWeb.ConsultaCPF(EdRazaoNome, EdCNPJCPF);
    else
      Geral.MensagemBox('Defina o tipo de pessoa e tente novamente!', 'Aviso',
        MB_OK+MB_ICONWARNING);
  end;
end;

procedure TFmDiarioTDI_01_ER0.BtSaidaClick(Sender: TObject);
begin
  FecharOForm(False, 0);
end;

procedure TFmDiarioTDI_01_ER0.CBCodMuniciClick(Sender: TObject);
begin
  if CBCodMunici.Focused then
    EdPsqCidade.Text := EdCodMunici.Text;
end;

procedure TFmDiarioTDI_01_ER0.CBREChange(Sender: TObject);
begin
  PesquisaSINTEGRA();
end;

procedure TFmDiarioTDI_01_ER0.CBREClick(Sender: TObject);
begin
  PesquisaSINTEGRA();
end;

function TFmDiarioTDI_01_ER0.CNPJCPFDuplicado(): Boolean;
var
  CNPJCPF: String;
  //Tipo: Integer;
begin
  Result := False;
  CNPJCPF := Geral.SoNumero_TT(EdCNPJCPF.Text);
  if Length(CNPJCPF) > 0 then
  begin
    QrDuplic2.Close;
    QrDuplic2.SQL.Clear;
    {
    if Length(CNPJCPF) < 14 then
      Tipo := 1
    else
      Tipo := 0;
    //
    if Tipo = 0 then
    begin
    }
      QrDuplic2.SQL.Add('SELECT Codigo FROM entidades');
      QrDuplic2.SQL.Add('WHERE CPF="' + CNPJCPF + '"');
      QrDuplic2.SQL.Add('OR CNPJ="' + CNPJCPF + '"');
      //QrDuplic2.SQL.Add('AND Codigo <> '+EdCodigo.Text);
    {
    end;
    if Tipo = 1 then
    begin
      QrDuplic2.SQL.Add('SELECT Codigo FROM entidades');
      QrDuplic2.SQL.Add('WHERE CPF="' + CNPJCPF + '"');
      //QrDuplic2.SQL.Add('AND Codigo <> '+EdCodigo.Text);
    end;
    }
    QrDuplic2.Open;
    if QrDuplic2.RecordCount > 0 then
    begin
      Result := True;
      Geral.MensagemBox('Esta entidade j� foi cadastrada com o c�digo n� '+
      IntToStr(QrDuplic2Codigo.Value)+'.', 'Entidade duplicada', MB_OK+MB_ICONWARNING);
      QrDuplic2.Close;
      Screen.Cursor := crDefault;
      Exit;
    end;
    QrDuplic2.Close;
  end;
end;

procedure TFmDiarioTDI_01_ER0.Descobrir1Click(Sender: TObject);
begin
  U_CEP.DescobrirCEP(EdCEP, EdLograd, EdRua, EdBairro, EdCidade, EdUF,
  EdPais, EdCodMunici, EdCodiPais, CBLograd, CBCodMunici,
  CBCodiPais);
end;

procedure TFmDiarioTDI_01_ER0.EdCNPJCPFExit(Sender: TObject);
begin
  CNPJCPFDuplicado();
end;

procedure TFmDiarioTDI_01_ER0.EdCodMuniciChange(Sender: TObject);
begin
  if EdCodMunici.Focused then
    EdPsqCidade.Text := EdCodMunici.Text;
end;

procedure TFmDiarioTDI_01_ER0.EdNumeroChange(Sender: TObject);
begin
  if EdNumero.Focused then
    EdPsqNumero.Text := EdNumero.Text;
end;

procedure TFmDiarioTDI_01_ER0.EdPsqCidadeChange(Sender: TObject);
begin
  ReopenPesqEndereco();
end;

procedure TFmDiarioTDI_01_ER0.EdPsqNumeroChange(Sender: TObject);
begin
  ReopenPesqEndereco();
end;

procedure TFmDiarioTDI_01_ER0.EdPsqRuaChange(Sender: TObject);
begin
  ReopenPesqEndereco();
end;

procedure TFmDiarioTDI_01_ER0.EdPsqUFChange(Sender: TObject);
begin
  ReopenPesqEndereco();
end;

procedure TFmDiarioTDI_01_ER0.EdRazaoNomeChange(Sender: TObject);
begin
  RGTipoClick(Self);
end;

procedure TFmDiarioTDI_01_ER0.EdRuaChange(Sender: TObject);
begin
  if EdRua.Focused then
    EdPsqRua.Text := EdRua.Text;
end;

procedure TFmDiarioTDI_01_ER0.EdCEPEnter(Sender: TObject);
begin
  FCEP := Geral.SoNumero_TT(EdCEP.Text);
end;

procedure TFmDiarioTDI_01_ER0.EdCEPExit(Sender: TObject);
var
  CEP: String;
begin
  CEP := Geral.SoNumero_TT(EdCEP.Text);
  if CEP <> FCEP then
  begin
    if Trim(CEP) <> '' then
      U_CEP.ConsultaCEP(EdCEP, EdLograd, EdRua, EdNumero, EdBairro,
        EdCidade, EdUF, EdPais, EdNumero, EdCompl, CBLograd,
        EdCodMunici, CBCodMunici, EdCodiPais, CBCodiPais);
  end;
end;

procedure TFmDiarioTDI_01_ER0.FecharOForm(Confirma: Boolean; Codigo: Integer);
begin
  if TTabSheet(CsAbaOrigem.Component) <> nil then
  begin
    // se ainda existir
    if TMySQLQuery(CsQry.Component) <> nil then
    begin
      TMySQLQuery(CsQry.Component).Close;
      TMySQLQuery(CsQry.Component).Open;
    end;
    if Confirma and (Codigo <> 0) then
    begin
      TdmkEditCB(CsEd.Component).ValueVariant := VaCodigo.ValueVariant;
      TdmkDBLookupComboBox(CsCB.Component).KeyValue := VaCodigo.ValueVariant;
    end;
  end;
  Close;
  TFmDiarioTDI_01(FFmCriou).FecharAba(TTabSheet(Self.Owner));
end;

procedure TFmDiarioTDI_01_ER0.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stIns;
  GBCunsCad.Visible := ImgTipo.SQLType = stIns;
  //
  FCEP := '';
  VaCodigo.ValueVariant := 0;
  //
  UnDmkDAC_PF.AbreQuery(QrListaLograd, DMod.MyDB);
  UMyMod.AbreQuery(QrMunici, DModG.AllID_DB);
  UMyMod.AbreQuery(QrBacen_Pais, DModG.AllID_DB);
  UnDmkDAC_PF.AbreQuery(QrAccounts, DMod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrAtividades, DMod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrHowFounded, DMod.MyDB);
  //
  CkCliente1_0.Caption := VAR_CLIENTE1;
  CkCliente2_0.Caption := VAR_CLIENTE2;
  CkCliente3_0.Caption := VAR_CLIENTE3;
  CkCliente4_0.Caption := VAR_CLIENTE4;
  CkFornece1_0.Caption := VAR_FORNECE1;
  CkFornece2_0.Caption := VAR_FORNECE2;
  CkFornece3_0.Caption := VAR_FORNECE3;
  CkFornece4_0.Caption := VAR_FORNECE4;
  CkFornece5_0.Caption := VAR_FORNECE5;
  CkFornece6_0.Caption := VAR_FORNECE6;
  CkFornece7_0.Caption := VAR_FORNECE7;
  CkFornece8_0.Caption := VAR_FORNECE8;
  CkTerceiro_0.Caption := VAR_TERCEIR2;
  //
end;

procedure TFmDiarioTDI_01_ER0.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [nil, nil, nil],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmDiarioTDI_01_ER0.FormShow(Sender: TObject);
begin
  if FmPrincipal.sd1.active then
    FmPrincipal.sd1.SkinForm(Handle);
  // ERRO! Oculta dmkEdits!
  //MyObjects.CorIniComponente(THandle(Sender));
  FmMyGlyfs.DefineGlyfs(TForm(Sender));
end;

procedure TFmDiarioTDI_01_ER0.HandleWndProc(var Msg: TMessage);
begin
  if Word(Msg.wParam) = 255 then
    TFmDiarioTDI_01(FFmCriou).AcoplarFormulario(Owner as TTabSheet)
  else
    inherited;
end;

procedure TFmDiarioTDI_01_ER0.Internet1Click(Sender: TObject);
begin
  U_CEP.ConsultaCEP2(EdCEP, EdLograd, EdRua, EdNumero, EdBairro,
    EdCidade, EdUF, EdPais, EdNumero, EdCompl, CBLograd, CBCodMunici,
    CBCodiPais, EdCodMunici, EdCodiPais);
end;

procedure TFmDiarioTDI_01_ER0.PesquisaSINTEGRA;
const
  CBCodCNAE = nil;
  EdCodCNAE = nil;
  EdSUFRAMA = nil;
var
  UF: String;
begin
  if Length(CBRE.Text) = 2 then
  begin
    if CBRE.Items.IndexOf(CBRE.Text) > -1 then
    begin
      UF := CBRE.Text;
      CBRE.Text := '';
      RGTipo.Itemindex := 0;
      UConsultasWeb.ConsultaIE(UF, EdCNPJCPF, EdIERG, EdRazaoNome, EdCep, EdRua,
        EdLograd, EdCompl, EdNumero, EdBairro, EdCidade, EdUF, EdPais,
        CBLograd, CBCodMunici, CBCodiPais, CBCodCNAE, EdCodMunici,
        EdCodiPais, EdCodCNAE, EdTe1, nil, EdSUFRAMA);
    end else
    begin
      Geral.MB_Aviso('UF desconhecida ou n�o implementada: ' + CBRE.Text);
    end;
  end;
end;

procedure TFmDiarioTDI_01_ER0.ReopenPesqEndereco(Manual: Boolean);
var
  Rua, Numero, Cidade, UF: String;
  Num: Integer;
begin
  if Manual or (RGPesquisa.ItemIndex = 0) then
  begin
    Rua := EdPsqRua.Text;
    if Trim(Rua) <> '' then
    begin
      Numero := EdPsqNumero.Text;
      if (Trim(Numero) <> '') and (Geral.SoNumero_TT(Numero) = Numero) then
      begin
        Num := Geral.IMV(Numero);
        if Numero <> '' then
          Numero := 'AND SNumero BETWEEN ' + Geral.FF0(Num - 100) + ' AND ' +
          Geral.FF0(Num + 100);
      end else
        Numero := '';
      Cidade := Trim(CBPsqCidade.Text);
      if EdCidade.ValueVariant <> '' then
        Cidade := 'AND SCidade LIKE "%' + Cidade + '%" '
      else
        Cidade := '';
      UF := Trim(EdUF.Text);
      if UF <> '' then
        UF := 'AND SUF = "' + UF + '" ';
      UnDmkDAC_PF.AbreMySQLQuery0(QrPesq, Dmod.MyDB, [
      'SELECT IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_ENT, ',
      'stc.* ',
      'FROM siaptercad stc ',
      'LEFT JOIN entidades ent ON ent.Codigo=stc.Cliente ',
      'WHERE SRua LIKE "%' + Rua + '%" ',
      Numero,
      Cidade,
      UF,
      'ORDER BY SNumero ',
      '']);
    end;
  end;
end;

procedure TFmDiarioTDI_01_ER0.RGTipoClick(Sender: TObject);
var
  OK: Boolean;
begin
  OK := RGTipo.ItemIndex >= 0;
  BtOK.Enabled := OK and (Trim(EdRazaoNome.Text) <> '');
  PnDados1.Visible := OK;
  PnDados2.Visible := OK;
  PnDados3.Visible := OK;
  //
  EdRazaoNome.SetFocus;
end;

procedure TFmDiarioTDI_01_ER0.SpeedButton5Click(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  //
  UnCfgCadLista.MostraCadLista(QrAtividades.Database, 'atividades',
  QrAtividadesNome.Size, ncGerlSeq1, 'Cadastro de Atividades',
  [], False, Null, [], [], False);
  //
  UMyMod.SetaCodigoPesquisado(EdAtivPrinc, CBAtivPrinc, QrAtividades, VAR_CADASTRO);
end;

procedure TFmDiarioTDI_01_ER0.SpeedButton6Click(Sender: TObject);
begin
  VAR_CADASTRO := EdAccount.ValueVariant;
  DModG.CadastroDeEntidade(VAR_CADASTRO, fmcadSelecionar, fmcadEntidade2);
  if VAR_CADASTRO <> 0 then
    UMyMod.SetaCodigoPesquisado(EdAccount, CBAccount, QrAccounts, VAR_CADASTRO);
end;

procedure TFmDiarioTDI_01_ER0.SpeedButton7Click(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  //
  UnCfgCadLista.MostraCadLista(QrHowFounded.Database, 'howfounded',
  QrHowFoundedNome.Size, ncGerlSeq1, 'Cadastro de Tipos de Marketing',
  [], False, Null, [], [], False);
  //
  UMyMod.SetaCodigoPesquisado(EdHowFind, CBHowFind, QrHowFounded, VAR_CADASTRO);
end;

function TFmDiarioTDI_01_ER0.UpdateCunsCad(Codigo: Integer): Boolean;
var
  AtivPrinc, HowFind, Account: Integer;
  DataCon: String;
begin
  if GBCunsCad.Visible then
  begin
    AtivPrinc := EdAtivPrinc.ValueVariant;
    HowFind := EdHowFind.ValueVariant;
    Account := EdAccount.ValueVariant;
    //
    DataCon := Geral.FDT(TPDataCon.Date, 1);
    //
    Result := UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'cunscad', False, [
    'AtivPrinc', 'HowFind', 'Account', 'DataCon'], [
    'Codigo'], [
    AtivPrinc, HowFind, Account, DataCon], [
    Codigo], True);
  end else
    Result := False;
end;

procedure TFmDiarioTDI_01_ER0.Verificar1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmEntiCEP, FmEntiCEP, afmoNegarComAviso) then
  begin
    with FmEntiCEP do
    begin
      DBGrid1.Visible := False;
      Painel1.Visible := False;
      case VAR_ACTIVEPAGE of
        1: EdCEPLoc.Text := EdCEP.Text;
        {  Parei Aqui! falta fazer
        2: EdCEPLoc.Text := EdECEP.Text;
        5: EdCEPLoc.Text := EdCCEP.Text;
        6: EdCEPLoc.Text := EdLCEP.Text;
        }
      end;
      ShowModal;
      Destroy;
    end;
  end;
end;

end.
