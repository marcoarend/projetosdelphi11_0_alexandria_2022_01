unit DiarioTDI_01;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Menus, ComCtrls, StdCtrls, ExtCtrls, ImgList, dmkGeral,
  dmkVariable, DB, mySQLDbTables, DmkDAC_PF, dmkEditCB, dmkDBLookupComboBox,
  dmkCheckGroup, dmkRadioGroup, dmkEdit, dmkImage, Buttons, UnInternalConsts,
  dmkCompoStore, dmkPageControl, UnDmkEnums, dmkMemo;

type
  TNovaAbaAtendimento = (naaEntiRapido, naaPreAtend, naaSiapTerCad);
  TFmDiarioTDI_01 = class(TForm)
    PageControl: TdmkPageControl;
    Bevel1: TBevel;
    ImageList: TImageList;
    PopupMenu: TPopupMenu;
    mnuFechar: TMenuItem;
    mnuFecharTodasExcetoEssa: TMenuItem;
    mnuDesacoplarFormulario: TMenuItem;
    N2: TMenuItem;
    Timer1: TTimer;
    QrDiarioAdd: TmySQLQuery;
    QrDiarioAddCodigo: TIntegerField;
    QrDiarioAddNome: TWideStringField;
    QrDiarioAddDiarioAss: TIntegerField;
    QrDiarioAddEntidade: TIntegerField;
    QrDiarioAddCliInt: TIntegerField;
    QrDiarioAddDepto: TIntegerField;
    QrDiarioAddData: TDateField;
    QrDiarioAddHora: TTimeField;
    QrDiarioAddTipo: TSmallintField;
    QrDiarioAddGenero: TSmallintField;
    QrDiarioAddBinaFone: TWideStringField;
    QrDiarioAddBinaDtHr: TDateTimeField;
    QrDiarioAddInterloctr: TIntegerField;
    QrDiarioAddTerceiro01: TIntegerField;
    QrDiarioAddPreAtend: TSmallintField;
    QrDiarioAddFormContat: TIntegerField;
    QrOSCab: TmySQLQuery;
    QrOSCabCodigo: TIntegerField;
    QrOSCabEntidade: TIntegerField;
    QrOSCabSiapTerCad: TIntegerField;
    QrOSCabEstatus: TIntegerField;
    QrOSCabFatoGeradr: TIntegerField;
    QrOSCabDtaContat: TDateTimeField;
    QrOSCabDtaVisPrv: TDateTimeField;
    QrOSCabDtaVisExe: TDateTimeField;
    QrOSCabDtaExePrv: TDateTimeField;
    QrOSCabDtaExeIni: TDateTimeField;
    QrOSCabDtaExeFim: TDateTimeField;
    QrOSCabValorTotal: TFloatField;
    QrOSCabLk: TIntegerField;
    QrOSCabDataCad: TDateField;
    QrOSCabDataAlt: TDateField;
    QrOSCabUserCad: TIntegerField;
    QrOSCabUserAlt: TIntegerField;
    QrOSCabAlterWeb: TSmallintField;
    QrOSCabAtivo: TSmallintField;
    QrOSCabDdsPosVda: TIntegerField;
    QrOSCabEntiContat: TIntegerField;
    QrOSCabNumContrat: TIntegerField;
    QrOSCabEntPagante: TIntegerField;
    QrOSCabEntContrat: TIntegerField;
    QrOSCabOperacao: TIntegerField;
    QrOSCabValorPre: TFloatField;
    QrOSCabFimVisPrv: TDateTimeField;
    QrOSCabFimExePrv: TDateTimeField;
    QrOSCabEmpresa: TIntegerField;
    Panel1: TPanel;
    BtInterlocucao: TBitBtn;
    BtEntidade: TBitBtn;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    CSTabSheetChamou: TdmkCompoStore;
    BtAgendaEve: TBitBtn;
    QrOSCabGrupo: TIntegerField;
    QrOSCabNumero: TIntegerField;
    QrOSCabOpcao: TIntegerField;
    QrOSCabAgeEqiCab: TIntegerField;
    QrOSCabDtaLibFat: TDateTimeField;
    QrOSCabDtaFimFat: TDateTimeField;
    QrOSCabValorServi: TFloatField;
    QrOSCabValorDesco: TFloatField;
    QrOSCabValorOutrs: TFloatField;
    QrOSCabCondicaoPg: TIntegerField;
    QrOSCabCartEmis: TIntegerField;
    QrOSCabSerNF: TWideStringField;
    QrOSCabNumNF: TIntegerField;
    QrOSCabInvalServi: TFloatField;
    QrOSCabInvalDesco: TFloatField;
    QrOSCabInvalOutrs: TFloatField;
    QrOSCabInvalTotal: TFloatField;
    QrOSCabOrcamServi: TFloatField;
    QrOSCabOrcamDesco: TFloatField;
    QrOSCabOrcamOutrs: TFloatField;
    QrOSCabOrcamTotal: TFloatField;
    QrOSCabValiDdOrca: TIntegerField;
    QrOSCabExeTxtCli1: TIntegerField;
    QrOSCabObsExecuta: TWideMemoField;
    QrOSCabExeTxtCli2: TIntegerField;
    QrOSCabFimVisExe: TDateTimeField;
    QrOSCabOptado: TSmallintField;
    QrOSCabHowGerou: TSmallintField;
    QrOSCabPosGerou: TSmallintField;
    QrOSCabOSFlhUltGe: TIntegerField;
    QrOSCabMulServico: TLargeintField;
    QrOSCabOSFlhGrCab: TIntegerField;
    QrOSCabOSFlhGrIts: TIntegerField;
    QrOSCabSohInicial: TIntegerField;
    QrOSCabStPipAdPrg: TSmallintField;
    QrOSCabLstUplWeb: TDateTimeField;
    QrOSCabObsGaranti: TWideMemoField;
    procedure Entidade1Click(Sender: TObject);
    procedure PageControlDrawTab(Control: TCustomTabControl;
      TabIndex: Integer; const Rect: TRect; Active: Boolean);
    procedure FormCreate(Sender: TObject);
    procedure PageControlChange(Sender: TObject);
    procedure mnuFecharClick(Sender: TObject);
    procedure PageControlMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure mnuFecharTodasExcetoEssaClick(Sender: TObject);
    procedure mnuDesacoplarFormularioClick(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure BtInterlocucaoClick(Sender: TObject);
    procedure BtEntidadeClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure BtAgendaEveClick(Sender: TObject);
  private
    FCaptionAplicacao: string;

    procedure SetCaptionAplicacao(const Value: string);
    {
    function PodeAbrirFormulario(ClasseForm: TFormClass;
      var TabSheet: TTabSheet): Boolean;
    }
    function TotalFormsAbertos(ClasseForm: TFormClass): Integer;
    procedure AjustarCaptionAbas(ClasseForm: TFormClass);
    procedure AjustarMenuSistema(Formulario: TForm; IdMenu: Word);
    //
    procedure MostraTDI_DiarioTDI_01_Add();
  public
    function  NovaAba(ClasseForm: TFormClass; IndiceImagem: Integer): TForm;
    procedure NovaAba2(Aba: TNovaAbaAtendimento; AbaOrigem: TTabSheet;
              //Qry: TMySQLQuery; EdCad: TdmkEditCB; CBCad: TdmkDBLookupComboBox);
              Componentes: array of TComponent);
    procedure FecharAba2(AbaFechar, AbaOrigem: TTabSheet; Qry: TMySQLQuery;
              Codigo: Integer; EdCad: TdmkEditCB; CBCad: TdmkDBLookupComboBox);
    procedure FecharAba(Aba: TTabSheet); overload;
    procedure FecharAba(Aba: TTabSheet; TodasExcetoEssa: Boolean); overload;
    procedure AcoplarFormulario(Aba: TTabSheet);
    procedure DesacoplarFormulario(Aba: TTabSheet);
    property  CaptionAplicacao: string read FCaptionAplicacao write SetCaptionAplicacao;
    //
    procedure MostraAdd(QuemChamou: Integer; SQLType: TSQLType;
              Codigo: Integer; BinaSoDt: TDateTime; BinaSoHr, BinaFone: String);
  end;

var
  FmDiarioTDI_01: TFmDiarioTDI_01;

implementation

uses DiarioTDI_01_Add, DiarioTDI_01_ER0, DiarioTDI_01_Pre, DiarioTDI_01_STC,
  UnMyVCLRef, Module, UnMyObjects, MyGlyfs, Principal, UnAgendaGerAll;


{$R *.dfm}

{ TForm1 }

const
  IMG_INDEX_0 = 0;

function TFmDiarioTDI_01.NovaAba(ClasseForm: TFormClass;
  IndiceImagem: Integer): TForm;
var
  TabSheet: TTabSheet;
  //Form: TForm;
begin
  {
  if not PodeAbrirFormulario(ClasseForm, TabSheet) then
  begin
    PageControl.ActivePage := TabSheet;
    Exit;
  end;
  }
  TabSheet := TTabSheet.Create(Self);
  //
  TabSheet.PageControl := PageControl;
  //
  Result := ClasseForm.Create(TabSheet);
  with Result do
  begin
    Align       := alClient;
    BorderStyle := bsNone;
    Parent      := TabSheet;
  end;

  with TabSheet do
  begin
    Caption     := Copy(Result.Caption, 17);
    ImageIndex  := IndiceImagem;
  end;

  AjustarCaptionAbas(ClasseForm);

  TForm(Result).Show;
  PageControl.ActivePage := TabSheet;

  PageControlChange(PageControl);
end;

procedure TFmDiarioTDI_01.NovaAba2(Aba: TNovaAbaAtendimento; AbaOrigem:
  TTabSheet; Componentes: array of TComponent);
  //Qry: TMySQLQuery; EdCad: TdmkEditCB; CBCad: TdmkDBLookupComboBox);
var
  Form: TForm;
  Cliente: Integer;
begin
  case Aba of
    naaEntiRapido:
    begin
      Form := NovaAba(TFmDiarioTDI_01_ER0, 1);
      TFmDiarioTDI_01_ER0(Form).FFmCriou := Self;
      MyVCLref.SET_Component(TForm(Form), 'ImgTipo', 'SQLType', stIns, TdmkImage);
      MyVCLref.SET_dmkVariable(TForm(Form), 'VaCodigo', 'ValueVariant', 0, True);
      MyVCLref.SET_dmkCompoStore(TForm(Form), 'CsAbaOrigem',  AbaOrigem, True);
      //MyVCLref.SET_dmkCompoStore(TForm(Form), 'CsQry',  Qry, True);
      MyVCLref.SET_dmkCompoStore(TForm(Form), 'CsQry',  TmySQLQuery(Componentes[0]), True);
      //MyVCLref.SET_dmkCompoStore(TForm(Form), 'CsEd',  EdCad, True);
      MyVCLref.SET_dmkCompoStore(TForm(Form), 'CsEd',  TdmkEditCB(Componentes[1]), True);
      //MyVCLref.SET_dmkCompoStore(TForm(Form), 'CsCB',  CBCad, True);
      MyVCLref.SET_dmkCompoStore(TForm(Form), 'CsCB',  TdmkDBLookupComboBox(Componentes[2]), True);
    end;
    naaPreAtend:
    begin
      Form := NovaAba(TFmDiarioTDI_01_Pre, 2);
      TFmDiarioTDI_01_Pre(Form).FFmCriou := Self;
      MyVCLref.SET_dmkCompoStore(TForm(Form), 'CsAbaOrigem',  AbaOrigem, True);
      MyVCLref.SET_dmkCompoStore(TForm(Form), 'CsEdDepto',  TdmkEdit(Componentes[0]), True);
      MyVCLref.SET_dmkCompoStore(TForm(Form), 'CsSbUserAction',  TSpeedButton(Componentes[1]), True);
      //
      UnDmkDAC_PF.AbreMySQLQuery0(QrOSCab, Dmod.MyDB, [
      'SELECT * ',
      'FROM oscab ',
      'WHERE Codigo=' + Geral.FF0(TdmkEdit(Componentes[0]).ValueVariant),
      '']);
      //
      MyVCLref.SET_Component(TForm(Form), 'ImgTipo', 'SQLType', stUpd, TdmkImage);
      MyVCLref.SET_dmkEdit(TForm(Form), 'EdGrupo',  'ValueVariant', QrOSCabCodigo.Value, True);
      MyVCLref.SET_dmkEdit(TForm(Form), 'EdNumero',  'ValueVariant', QrOSCabCodigo.Value, True);
      MyVCLref.SET_dmkEdit(TForm(Form), 'EdOpcao',  'ValueVariant', QrOSCabCodigo.Value, True);
      MyVCLref.SET_dmkEdit(TForm(Form), 'EdCodigo',  'ValueVariant', QrOSCabCodigo.Value, True);
      MyVCLref.SET_dmkEdit(TForm(Form), 'EdEmpresa',  'ValueVariant',  QrOSCabEmpresa.Value, True);
      MyVCLref.SET_dmkEdit(TForm(Form), 'EdEntidade',  'ValueVariant',  QrOSCabEntidade.Value, True);
      MyVCLref.SET_dmkDBLookupComboBox(TForm(Form), 'CBEntidade', 'KeyValue',  QrOSCabEntidade.Value, True);
      MyVCLref.SET_dmkEdit(TForm(Form), 'EdNumContrat',  'ValueVariant',  QrOSCabNumContrat.Value, True);
      MyVCLref.SET_dmkEdit(TForm(Form), 'EdSiapTerCad',  'ValueVariant',  QrOSCabSiapTerCad.Value, True);
      MyVCLref.SET_dmkDBLookupComboBox(TForm(Form), 'CBSiapTerCad', 'KeyValue',  QrOSCabSiapTerCad.Value, True);
      //MyVCLref.SET_dmkEdit(TForm(Form), 'EdOSOrigem',  'ValueVariant',  QrOSCabOSOrigem.Value, True);
      MyVCLref.SET_dmkEdit(TForm(Form), 'EdEstatus',  'ValueVariant',  QrOSCabEstatus.Value, True);
      MyVCLref.SET_dmkDBLookupComboBox(TForm(Form), 'CBEstatus', 'KeyValue',  QrOSCabEstatus.Value, True);
      MyVCLref.SET_dmkEdit(TForm(Form), 'EdEntiContat',  'ValueVariant',  QrOSCabEntiContat.Value, True);
      MyVCLref.SET_dmkDBLookupComboBox(TForm(Form), 'CBEntiContat', 'KeyValue',  QrOSCabEntiContat.Value, True);
      MyVCLref.SET_dmkEdit(TForm(Form), 'EdFatoGeradr',  'ValueVariant',  QrOSCabFatoGeradr.Value, True);
      MyVCLref.SET_dmkDBLookupComboBox(TForm(Form), 'CBFatoGeradr', 'KeyValue',  QrOSCabFatoGeradr.Value, True);
      MyVCLref.SET_dmkEditDateTimePicker(TForm(Form), 'TPDtaVisPrv', 'Date',  QrOSCabDtaVisPrv.Value, True);
      MyVCLref.SET_dmkEdit(TForm(Form), 'EdDtaVisPrv',  'ValueVariant',  QrOSCabDtaVisPrv.Value, True);
      MyVCLref.SET_dmkEditDateTimePicker(TForm(Form), 'TPDtaExePrv', 'Date',  QrOSCabDtaExePrv.Value, True);
      MyVCLref.SET_dmkEdit(TForm(Form), 'EdDtaExePrv',  'ValueVariant',  QrOSCabDtaExePrv.Value, True);
      MyVCLref.SET_dmkEdit(TForm(Form), 'EdEntContrat',  'ValueVariant',  QrOSCabEntContrat.Value, True);
      MyVCLref.SET_dmkDBLookupComboBox(TForm(Form), 'CBEntContrat', 'KeyValue',  QrOSCabEntContrat.Value, True);
      MyVCLref.SET_dmkEdit(TForm(Form), 'EdEntPagante',  'ValueVariant',  QrOSCabEntPagante.Value, True);
      MyVCLref.SET_dmkDBLookupComboBox(TForm(Form), 'CBEntPagante', 'KeyValue',  QrOSCabEntPagante.Value, True);
      MyVCLref.SET_dmkEdit(TForm(Form), 'EdValorPre',  'ValueVariant',  QrOSCabValorPre.Value, True);
      MyVCLref.SET_Component(TForm(Form), 'CGOperacao', 'Value',  QrOSCabOperacao.Value, TdmkCheckGroup);
      MyVCLref.SET_dmkEditDateTimePicker(TForm(Form), 'TPFimVisPrv', 'Date',  QrOSCabFimVisPrv.Value, True);
      MyVCLref.SET_dmkEdit(TForm(Form), 'EdFimVisPrv',  'ValueVariant',  QrOSCabFimVisPrv.Value, True);
      MyVCLref.SET_dmkEditDateTimePicker(TForm(Form), 'TPFimExePrv', 'Date',  QrOSCabFimExePrv.Value, True);
      MyVCLref.SET_dmkEdit(TForm(Form), 'EdFimExePrv',  'ValueVariant',  QrOSCabFimExePrv.Value, True);
      MyVCLref.SET_dmkEdit(TForm(Form), 'EdAgeEqiCab',  'ValueVariant',  QrOSCabAgeEqiCab.Value, True);
      MyVCLref.SET_dmkDBLookupComboBox(TForm(Form), 'CBAgeEqiCab', 'KeyValue',  QrOSCabAgeEqiCab.Value, True);
      MyVCLref.SET_dmkMemo(TForm(Form), 'MeObsGaranti',  'Lines.Text',  QrOSCabObsGaranti.Value, True);
      MyVCLref.SET_dmkMemo(TForm(Form), 'MeObsExecuta',  'Lines.Text',  QrOSCabObsExecuta.Value, True);
      //
    end;
    naaSiapTerCad:
    begin
      Form := NovaAba(TFmDiarioTDI_01_STC, 5);
      TFmDiarioTDI_01_STC(Form).FFmCriou := Self;
      MyVCLref.SET_Component(TForm(Form), 'ImgTipo', 'SQLType', stIns, TdmkImage);
      //
      MyVCLref.SET_dmkCompoStore(TForm(Form), 'CsQry',  TmySQLQuery(Componentes[0]), True);
      MyVCLref.SET_dmkCompoStore(TForm(Form), 'CsEd',  TdmkEditCB(Componentes[1]), True);
      MyVCLref.SET_dmkCompoStore(TForm(Form), 'CsCB',  TdmkDBLookupComboBox(Componentes[2]), True);
      //
      Cliente := TdmkEditCB(Componentes[3]).ValueVariant;
      MyVCLref.SET_dmkEdit(TForm(Form), 'EdCliente',  'ValueVariant', Cliente, True);
      MyVCLref.SET_dmkDBLookupComboBox(TForm(Form), 'CBCliente', 'KeyValue',  Cliente, True);
    end;
  end;
end;

procedure TFmDiarioTDI_01.Entidade1Click(Sender: TObject);
begin
end;

{
function TFmDiarioTDI_01.PodeAbrirFormulario(ClasseForm: TFormClass;
  var TabSheet: TTabSheet): Boolean;
var
  I: Integer;
begin
  Result := True;
  for I := 0 to PageControl.PageCount - 1 do
    if PageControl.Pages[I].Components[0].ClassType = ClasseForm then
    begin
      TabSheet := PageControl.Pages[I];
      Result := (TabSheet.Components[0] as TForm).Tag = 0;
      Break;
    end;
end;
}

procedure TFmDiarioTDI_01.Timer1Timer(Sender: TObject);
begin
  Timer1.Enabled := False;
  MostraTDI_DiarioTDI_01_Add();
end;

function TFmDiarioTDI_01.TotalFormsAbertos(ClasseForm: TFormClass): Integer;
var
  I: Integer;
begin
  Result := 0;
  for I := 0 to PageControl.PageCount - 1 do
    if PageControl.Pages[I].Components[0].ClassType = ClasseForm then
      Inc(Result);
end;

procedure TFmDiarioTDI_01.AjustarCaptionAbas(ClasseForm: TFormClass);
var
  I, Indice, TotalForms: Integer;
begin
  TotalForms := TotalFormsAbertos(ClasseForm);

  if TotalForms > 1 then
  begin
    Indice := 1;
    for I := 0 to PageControl.PageCount - 1 do
      with PageControl do
        if Pages[I].Components[0].ClassType = ClasseForm then
        begin
          Pages[I].Caption := Copy((Pages[I].Components[0] as TForm).Caption, 17) + ' (' +
            IntToStr(Indice) + ')';
          Inc(Indice);
        end;
  end;
end;

procedure TFmDiarioTDI_01.PageControlDrawTab(Control: TCustomTabControl;
  TabIndex: Integer; const Rect: TRect; Active: Boolean);
var
  CaptionAba, CaptionForm, CaptionContador: string;
  I: Integer;
begin
  with PageControl do
  begin
    // Separa o caption da aba e o contador de forms
    CaptionAba := Pages[TabIndex].Caption;

    CaptionForm := Trim(Copy(CaptionAba, 1, Pos('(', CaptionAba))) + ' ';

    CaptionContador := Copy(CaptionAba, Pos('(', CaptionAba), Length(CaptionAba));
    CaptionContador := Copy(CaptionContador, 1, Pos(')', CaptionContador));

    Canvas.FillRect(Rect);

    Canvas.TextOut(Rect.Left + 3, Rect.Top + 3, CaptionForm);
    I := Canvas.TextWidth(CaptionForm);

    Canvas.Font.Style := [fsBold];
    Canvas.TextOut(Rect.Left + 3 + I, Rect.Top + 3, CaptionContador);
  end;
end;

procedure TFmDiarioTDI_01.SetCaptionAplicacao(const Value: string);
begin
{
  if Value <> FCaptionAplicacao then
  begin
    Caption := StringReplace(Caption, FCaptionAplicacao, Value, []);
    FCaptionAplicacao := Value;
    Application.Title := Caption;
  end;
}
end;

procedure TFmDiarioTDI_01.FormCreate(Sender: TObject);
begin
//  CaptionAplicacao := 'Aplicações TDI';
end;

procedure TFmDiarioTDI_01.FormDestroy(Sender: TObject);
begin
  VAR_CaptionFormOrigem := '';
  VAR_NomeCompoF7 := '';
end;

procedure TFmDiarioTDI_01.FormShow(Sender: TObject);
begin
  FmMyGlyfs.DefineGlyfsTDI(FmPrincipal.sd1, Sender);
end;

procedure TFmDiarioTDI_01.PageControlChange(Sender: TObject);
begin

{  Caption := CaptionAplicacao + ' - ' + PageControl.ActivePage.Caption;
  Application.Title := Caption;
}
  with (PageControl.ActivePage.Components[0] as TForm) do
  begin
    VAR_CaptionFormOrigem := Caption;
    VAR_NomeCompoF7 := '';
    if not Assigned(Parent) then
      Show;
  end;
end;

procedure TFmDiarioTDI_01.mnuFecharClick(Sender: TObject);
begin
  FecharAba(PageControl.ActivePage);
end;

procedure TFmDiarioTDI_01.FecharAba(Aba: TTabSheet);
var
  Form: TForm;
  AbaEsquerda: TTabSheet;
begin
  AbaEsquerda := nil;
  Form := Aba.Components[0] as TForm;

  if Form.CloseQuery then
  begin
    if Aba.TabIndex > 0 then
      AbaEsquerda := PageControl.Pages[Aba.TabIndex - 1];

    Form.Close;
    if Aba <> nil then
    begin
      try
        // Gera erro as vezes!
        //Aba.Free;
        DesacoplarFormulario(Aba);
      except
        ;// nada
      end;
    end;

    PageControl.ActivePage := AbaEsquerda;
  end;
end;

procedure TFmDiarioTDI_01.PageControlMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  if Button = mbRight then
    PageControl.TabIndex := PageControl.IndexOfTabAt(X, Y);
end;

procedure TFmDiarioTDI_01.FecharAba(Aba: TTabSheet; TodasExcetoEssa: Boolean);
var
  I: Integer;
begin
  for I := PageControl.PageCount - 1 downto 0 do
    if PageControl.Pages[I] <> Aba then
      FecharAba(PageControl.Pages[I]);
end;

procedure TFmDiarioTDI_01.FecharAba2(AbaFechar, AbaOrigem: TTabSheet;
  Qry: TMySQLQuery; Codigo: Integer; EdCad: TdmkEditCB;
  CBCad: TdmkDBLookupComboBox);
{
var
  I, J: Integer;
}
begin
  FecharAba(AbaFechar);
end;

procedure TFmDiarioTDI_01.mnuFecharTodasExcetoEssaClick(Sender: TObject);
begin
  FecharAba(PageControl.ActivePage, True);
end;

procedure TFmDiarioTDI_01.MostraAdd(QuemChamou: Integer; SQLType: TSQLType;
  Codigo: Integer; BinaSoDt: TDateTime; BinaSoHr, BinaFone: String);
var
  Form: TForm;
begin
  Form := NovaAba(TFmDiarioTDI_01_Add, IMG_INDEX_0);
  TFmDiarioTDI_01_Add(Form).FFmCriou := Self;
  //
  MyVCLref.SET_dmkVariable(TForm(Form), 'VaChamou', 'ValueVariant', QuemChamou);
  FmMyGlyfs.ConfiguraFormDock(Form);
  MyVCLref.SET_Component(TForm(Form), 'ImgTipo', 'SQLType', SQLType, TdmkImage);
  if SQLType = stIns then
  begin
    MyVCLref.SET_dmkEditDateTimePicker(TForm(Form), 'TPBinaSoDt', 'Date', BinaSoDt, True);
    MyVCLref.SET_dmkEdit(TForm(Form), 'EdBinaSoHr', 'ValueVariant', BinaSoHr, True);
    MyVCLref.SET_dmkEdit(TForm(Form), 'EdBinaFone', 'ValueVariant', BinaFone, True);
  end else
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrDiarioAdd, Dmod.MyDB, [
    'SELECT * FROM diarioadd ',
    'WHERE Codigo=' + Geral.FF0(Codigo),
    '']);
    //
    MyVCLref.SET_dmkEdit(TForm(Form), 'EdCodigo', 'ValueVariant', Codigo, True);
    MyVCLref.SET_dmkEdit(TForm(Form), 'EdNome', 'Text', QrDiarioAddNome.Value, True);

    MyVCLref.SET_dmkEditCB(TForm(Form), 'EdFormContat', 'ValueVariant', QrDiarioAddFormContat.Value, True);
    MyVCLref.SET_dmkDBLookupComboBox(TForm(Form), 'CBFormContat', 'KeyValue', QrDiarioAddFormContat.Value, True);

    MyVCLref.SET_dmkEditCB(TForm(Form), 'EdDiarioAss', 'ValueVariant', QrDiarioAddDiarioAss.Value, True);
    MyVCLref.SET_dmkDBLookupComboBox(TForm(Form), 'CBDiarioAss', 'KeyValue', QrDiarioAddDiarioAss.Value, True);

    MyVCLref.SET_dmkEditCB(TForm(Form), 'EdEntidade', 'ValueVariant', QrDiarioAddEntidade.Value, True);
    MyVCLref.SET_dmkDBLookupComboBox(TForm(Form), 'CBEntidade', 'KeyValue', QrDiarioAddEntidade.Value, True);

    // Permitir setar por DModG.SelecionaEmpresaSeUnica() caso CliInt = 0!
    if QrDiarioAddCliInt.Value <> 0 then
    begin
      MyVCLref.SET_dmkEditCB(TForm(Form), 'EdCliInt', 'ValueVariant', QrDiarioAddCliInt.Value, True);
      MyVCLref.SET_dmkDBLookupComboBox(TForm(Form), 'CBCliInt', 'KeyValue', QrDiarioAddCliInt.Value, True);
    end;

    MyVCLref.SET_dmkEditCB(TForm(Form), 'EdInterloctr', 'ValueVariant', QrDiarioAddInterloctr.Value, True);
    MyVCLref.SET_dmkDBLookupComboBox(TForm(Form), 'CBInterloctr', 'KeyValue', QrDiarioAddInterloctr.Value, True);

    MyVCLref.SET_dmkEditCB(TForm(Form), 'EdTerceiro01', 'ValueVariant', QrDiarioAddTerceiro01.Value, True);
    MyVCLref.SET_dmkDBLookupComboBox(TForm(Form), 'CBTerceiro01', 'KeyValue', QrDiarioAddTerceiro01.Value, True);

    MyVCLref.SET_dmkEdit(TForm(Form), 'EdDepto', 'ValueVariant', QrDiarioAddDepto.Value, True);

    MyVCLref.SET_dmkEditDateTimePicker(TForm(Form), 'TPData', 'Date', QrDiarioAddData.Value, True);
    MyVCLref.SET_dmkEdit(TForm(Form), 'EdHora', 'ValueVariant', QrDiarioAddHora.Value, True);

    MyVCLref.SET_dmkVariable(TForm(Form), 'VaTipo', 'ValueVariant', QrDiarioAddTipo.Value, True);

    MyVCLref.SET_dmkVariable(TForm(Form), 'VaGenero', 'ValueVariant', QrDiarioAddGenero.Value, True);

    MyVCLref.SET_dmkEdit(TForm(Form), 'EdBinaFone', 'ValueVariant', QrDiarioAddBinaFone.Value, True);
    MyVCLref.SET_dmkEdit(TForm(Form), 'EdBinaSoHr', 'ValueVariant', Geral.ObtemHoraDeDateTime(QrDiarioAddBinaDtHr.Value), True);
    MyVCLref.SET_dmkEditDateTimePicker(TForm(Form), 'TPBinaSoDt', 'Date', QrDiarioAddBinaDtHr.Value);

    MyVCLref.SET_dmkVariable(TForm(Form), 'VaPreAtend', 'ValueVariant', QrDiarioAddPreAtend.Value, True);
    //
    MyObjects.WinControlSetFocus('EdETNome');
  end;
end;

procedure TFmDiarioTDI_01.MostraTDI_DiarioTDI_01_Add();
var
  Form: TForm;
begin
  Form := NovaAba(TFmDiarioTDI_01_Add, IMG_INDEX_0);
  TFmDiarioTDI_01_Add(Form).FFmCriou := Self;
end;

procedure TFmDiarioTDI_01.AcoplarFormulario(Aba: TTabSheet);
begin
  with Aba.Components[0] as TForm do
  begin
    // Parent deve vir antes de Align para evitar o flickering da tela
    Parent      := Aba;
    Align       := alClient;
    BorderStyle := bsNone;
  end;

  Aba.TabVisible := True;
  PageControl.ActivePage := Aba;
end;

procedure TFmDiarioTDI_01.DesacoplarFormulario(Aba: TTabSheet);
begin
  with Aba.Components[0] as TForm do
  begin
    Align       := alNone;
    BorderStyle := bsSizeable;
    Parent      := nil;
  end;

  Aba.TabVisible := False;

  AjustarMenuSistema(Aba.Components[0] as TForm, 255);
end;

procedure TFmDiarioTDI_01.AjustarMenuSistema(Formulario: TForm; IdMenu: Word);
const
  CAPTION_MENU = 'Acoplar formulário';
var
  Menu: HMENU;
  MenuItemInfo: TMenuItemInfo;
begin
  Menu := GetSystemMenu(Formulario.Handle, False);

  if Lo(GetVersion) >= 4 then
  begin
    { Add a seperator }
    FillChar(MenuItemInfo, SizeOf(MenuItemInfo), #0);
    with MenuItemInfo do
    begin
      cbSize := 44; //SizeOf(MenuItemInfo);
      fType  := MFT_SEPARATOR;
      wID    := 0;
      fMask  := MIIM_CHECKMARKS or MIIM_DATA or
        MIIM_ID or MIIM_STATE or MIIM_SUBMENU or MIIM_TYPE;
    end;
    InsertMenuItem(Menu, 0, True, MenuItemInfo);

    with MenuItemInfo do
    begin
      fType         := MFT_STRING;
      fState        := MFS_DEFAULT;
      wID           := IdMenu;
      hSubMenu      := 0;
      hbmpChecked   := 0;
      hbmpUnchecked := 0;
      dwTypeData    := CAPTION_MENU;
      fMask         := MIIM_CHECKMARKS or MIIM_DATA or MIIM_ID or MIIM_STATE
        or MIIM_SUBMENU or MIIM_TYPE;      
    end;
    InsertMenuItem(Menu, 0, True, MenuItemInfo);
  end
  else
    InsertMenu(Menu, 0, MF_BYPOSITION, IdMenu, CAPTION_MENU);
end;

procedure TFmDiarioTDI_01.BtAgendaEveClick(Sender: TObject);
begin
  AgendaGerAll.MostraFormAgendaEve(0, 0, False, 0, 0, 0, 0, []);
end;

procedure TFmDiarioTDI_01.BtEntidadeClick(Sender: TObject);
var
  Form: TForm;
begin
  Form := NovaAba(TFmDiarioTDI_01_ER0, (Sender as TBitBtn).Taborder);
  TFmDiarioTDI_01_ER0(Form).FFmCriou := Self;
end;

procedure TFmDiarioTDI_01.BtInterlocucaoClick(Sender: TObject);
begin
  MostraTDI_DiarioTDI_01_Add();
end;

procedure TFmDiarioTDI_01.BtSaidaClick(Sender: TObject);
begin
  //Close;
  //
  MyObjects.FormTDIFecha(Self, TTabSheet(CSTabSheetChamou.Component));
end;

procedure TFmDiarioTDI_01.mnuDesacoplarFormularioClick(Sender: TObject);
begin
  DesacoplarFormulario(PageControl.ActivePage);
end;

end.
