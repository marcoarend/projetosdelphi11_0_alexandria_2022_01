object FmSAC_01_Pre: TFmSAC_01_Pre
  Tag = 1
  Left = 258
  Top = 135
  Caption = 'ATD-TELEF-004 :: Pr'#233'-Atendimento'
  ClientHeight = 696
  ClientWidth = 1000
  Color = clMenu
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label8: TLabel
    Left = 0
    Top = 540
    Width = 1000
    Height = 13
    Align = alBottom
    Caption = 'Clique com o bot'#227'o direito do mouse para abrir a lista de op'#231#245'es'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clGreen
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
    ExplicitTop = 476
    ExplicitWidth = 361
  end
  object GBConfirma: TGroupBox
    Left = 0
    Top = 633
    Width = 1000
    Height = 63
    Align = alBottom
    TabOrder = 0
    ExplicitTop = 569
    object BtConfirma: TBitBtn
      Tag = 14
      Left = 12
      Top = 17
      Width = 90
      Height = 40
      Cursor = crHandPoint
      Caption = '&Confirma'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      NumGlyphs = 2
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      OnClick = BtConfirmaClick
    end
    object Panel4: TPanel
      Left = 890
      Top = 15
      Width = 108
      Height = 46
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtDesiste: TBitBtn
        Tag = 15
        Left = 7
        Top = 2
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtDesisteClick
      end
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 585
    Height = 540
    Align = alLeft
    BevelOuter = bvNone
    TabOrder = 1
    ExplicitHeight = 476
    object Label7: TLabel
      Left = 0
      Top = 475
      Width = 585
      Height = 13
      Align = alTop
      Caption = 'Clique com o bot'#227'o direito do mouse para abrir a lista de op'#231#245'es'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clGreen
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      ExplicitTop = 429
      ExplicitWidth = 361
    end
    object PnTempo: TPanel
      Left = 0
      Top = 0
      Width = 585
      Height = 45
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object GB_R: TGroupBox
        Left = 537
        Top = 0
        Width = 48
        Height = 45
        Align = alRight
        TabOrder = 0
        object ImgTipo: TdmkImage
          Left = 8
          Top = 7
          Width = 32
          Height = 32
          Transparent = True
          SQLType = stNil
        end
      end
    end
    object GBEdita: TGroupBox
      Left = 0
      Top = 45
      Width = 585
      Height = 430
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 1
      object Label1: TLabel
        Left = 16
        Top = 8
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label2: TLabel
        Left = 76
        Top = 8
        Width = 35
        Height = 13
        Caption = 'Cliente:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label6: TLabel
        Left = 16
        Top = 48
        Width = 243
        Height = 13
        Caption = 'Lugar do im'#243'vel ou onde est'#225' o m'#243'vel / autom'#243'vel:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label14: TLabel
        Left = 16
        Top = 88
        Width = 33
        Height = 13
        Caption = 'Status:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label15: TLabel
        Left = 16
        Top = 132
        Width = 104
        Height = 13
        Caption = 'Fato gerador (Motivo):'
        Color = clBtnFace
        ParentColor = False
      end
      object Label34: TLabel
        Left = 344
        Top = 88
        Width = 40
        Height = 13
        Caption = 'Contato:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label35: TLabel
        Left = 488
        Top = 8
        Width = 64
        Height = 13
        Caption = 'Contrato [F4]:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label36: TLabel
        Left = 16
        Top = 212
        Width = 79
        Height = 13
        Caption = 'Contratante: [F4]'
        Color = clBtnFace
        ParentColor = False
      end
      object SbSiapTerCad: TSpeedButton
        Left = 464
        Top = 64
        Width = 21
        Height = 21
        Caption = '...'
        OnClick = SbSiapTerCadClick
      end
      object Label3: TLabel
        Left = 16
        Top = 172
        Width = 134
        Height = 13
        Caption = 'Valor aprox. info. ao cliente: '
      end
      object Label4: TLabel
        Left = 488
        Top = 48
        Width = 44
        Height = 13
        Caption = 'Empresa:'
        Color = clBtnFace
        Enabled = False
        ParentColor = False
      end
      object Label55: TLabel
        Left = 16
        Top = 383
        Width = 92
        Height = 13
        Caption = 'Equipe de agentes:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label5: TLabel
        Left = 16
        Top = 252
        Width = 64
        Height = 13
        Caption = 'Pagante: [F4]'
        Color = clBtnFace
        ParentColor = False
      end
      object Label10: TLabel
        Left = 16
        Top = 292
        Width = 52
        Height = 13
        Caption = 'Vendedor: '
        Color = clBtnFace
        ParentColor = False
      end
      object EdCodigo: TdmkEdit
        Left = 16
        Top = 24
        Width = 56
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Codigo'
        UpdCampo = 'Codigo'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = EdCodigoChange
      end
      object EdEntidade: TdmkEditCB
        Left = 76
        Top = 24
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 1
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Entidade'
        UpdCampo = 'Entidade'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = EdEntidadeChange
        OnExit = EdEntidadeExit
        DBLookupComboBox = CBEntidade
        IgnoraDBLookupComboBox = False
      end
      object CBEntidade: TdmkDBLookupComboBox
        Left = 132
        Top = 24
        Width = 353
        Height = 21
        KeyField = 'Codigo'
        ListField = 'NO_ENT'
        ListSource = DsEntidades
        TabOrder = 2
        OnExit = CBEntidadeExit
        dmkEditCB = EdEntidade
        QryCampo = 'Entidade'
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdSiapTerCad: TdmkEditCB
        Left = 16
        Top = 64
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 4
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'SiapTerCad'
        UpdCampo = 'SiapTerCad'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBSiapTerCad
        IgnoraDBLookupComboBox = False
      end
      object CBSiapTerCad: TdmkDBLookupComboBox
        Left = 72
        Top = 64
        Width = 389
        Height = 21
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsSiapterCad
        TabOrder = 5
        dmkEditCB = EdSiapTerCad
        QryCampo = 'SiapTerCad'
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdEstatus: TdmkEditCB
        Left = 16
        Top = 104
        Width = 44
        Height = 21
        Alignment = taRightJustify
        TabOrder = 7
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Estatus'
        UpdCampo = 'Estatus'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBEstatus
        IgnoraDBLookupComboBox = False
      end
      object CBEstatus: TdmkDBLookupComboBox
        Left = 60
        Top = 104
        Width = 281
        Height = 21
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsEstatusOSs
        TabOrder = 8
        dmkEditCB = EdEstatus
        QryCampo = 'Estatus'
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdFatoGeradr: TdmkEditCB
        Left = 16
        Top = 148
        Width = 44
        Height = 21
        Alignment = taRightJustify
        TabOrder = 11
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'FatoGeradr'
        UpdCampo = 'FatoGeradr'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = EdFatoGeradrChange
        DBLookupComboBox = CBFatoGeradr
        IgnoraDBLookupComboBox = False
      end
      object CBFatoGeradr: TdmkDBLookupComboBox
        Left = 60
        Top = 148
        Width = 133
        Height = 21
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsFatoGeradr
        TabOrder = 12
        dmkEditCB = EdFatoGeradr
        QryCampo = 'FatoGeradr'
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdEmpresa: TdmkEdit
        Left = 488
        Top = 64
        Width = 80
        Height = 21
        Alignment = taRightJustify
        ReadOnly = True
        TabOrder = 6
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Empresa'
        UpdCampo = 'Empresa'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnKeyDown = EdEmpresaKeyDown
      end
      object EdEntiContat: TdmkEditCB
        Left = 344
        Top = 104
        Width = 44
        Height = 21
        Alignment = taRightJustify
        TabOrder = 9
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'EntiContat'
        UpdCampo = 'EntiContat'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBEntiContat
        IgnoraDBLookupComboBox = False
      end
      object CBEntiContat: TdmkDBLookupComboBox
        Left = 388
        Top = 104
        Width = 180
        Height = 21
        KeyField = 'Controle'
        ListField = 'Nome'
        ListSource = DsEntiContat
        TabOrder = 10
        dmkEditCB = EdEntiContat
        QryCampo = 'EntiContat'
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdNumContrat: TdmkEdit
        Left = 488
        Top = 24
        Width = 80
        Height = 21
        Alignment = taRightJustify
        ReadOnly = True
        TabOrder = 3
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'NumContrat'
        UpdCampo = 'NumContrat'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnKeyDown = EdNumContratKeyDown
      end
      object EdEntContrat: TdmkEditCB
        Left = 16
        Top = 228
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 15
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'EntContrat'
        UpdCampo = 'EntContrat'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = EdEntContratChange
        OnKeyDown = EdEntContratKeyDown
        DBLookupComboBox = CBEntContrat
        IgnoraDBLookupComboBox = False
      end
      object CBEntContrat: TdmkDBLookupComboBox
        Left = 76
        Top = 228
        Width = 485
        Height = 21
        KeyField = 'Codigo'
        ListField = 'NO_ENT'
        ListSource = DsEntContrat
        TabOrder = 16
        OnKeyDown = CBEntContratKeyDown
        dmkEditCB = EdEntContrat
        QryCampo = 'EntContrat'
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdValorPre: TdmkEdit
        Left = 16
        Top = 188
        Width = 173
        Height = 21
        Alignment = taRightJustify
        TabOrder = 13
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        QryCampo = 'ValorPre'
        UpdCampo = 'ValorPre'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
        OnChange = EdValorPreChange
      end
      object CGOperacao: TdmkCheckGroup
        Left = 16
        Top = 332
        Width = 553
        Height = 45
        Caption = ' Opera'#231#227'o: '
        Columns = 2
        Items.Strings = (
          'Dedetiza'#231#227'o'
          'Caixa d'#39#225'gua')
        TabOrder = 21
        UpdType = utYes
        Value = 0
        OldValor = 0
      end
      object GroupBox3: TGroupBox
        Left = 198
        Top = 128
        Width = 371
        Height = 81
        Caption = ' Data prevista: '
        TabOrder = 14
        object Label17: TLabel
          Left = 12
          Top = 32
          Width = 37
          Height = 13
          Caption = 'Vistoria:'
          Color = clBtnFace
          ParentColor = False
        end
        object Label18: TLabel
          Left = 12
          Top = 60
          Width = 51
          Height = 13
          Caption = 'Execu'#231#227'o:'
          Color = clBtnFace
          ParentColor = False
        end
        object Label52: TLabel
          Left = 64
          Top = 16
          Width = 51
          Height = 13
          Caption = 'In'#237'cio: [F4]'
          Color = clBtnFace
          ParentColor = False
        end
        object Label53: TLabel
          Left = 216
          Top = 16
          Width = 62
          Height = 13
          Caption = 'T'#233'rmino: [F4]'
          Color = clBtnFace
          ParentColor = False
        end
        object TPDtaVisPrv: TdmkEditDateTimePicker
          Left = 64
          Top = 32
          Width = 108
          Height = 21
          Date = 0.639644131944805900
          Time = 0.639644131944805900
          TabOrder = 0
          OnExit = TPDtaVisPrvExit
          ReadOnly = False
          DefaultEditMask = '!99/99/99;1;_'
          AutoApplyEditMask = True
          QryCampo = 'DtaVisPrv'
          UpdCampo = 'DtaVisPrv'
          UpdType = utYes
        end
        object EdDtaVisPrv: TdmkEdit
          Left = 172
          Top = 32
          Width = 40
          Height = 21
          TabOrder = 1
          FormatType = dmktfTime
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '00:00'
          QryCampo = 'DtaVisPrv'
          UpdCampo = 'DtaVisPrv'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
          OnExit = EdDtaVisPrvExit
        end
        object TPFimVisPrv: TdmkEditDateTimePicker
          Left = 216
          Top = 32
          Width = 108
          Height = 21
          Date = 0.639644131944805900
          Time = 0.639644131944805900
          TabOrder = 2
          ReadOnly = False
          DefaultEditMask = '!99/99/99;1;_'
          AutoApplyEditMask = True
          QryCampo = 'FimVisPrv'
          UpdCampo = 'FimVisPrv'
          UpdType = utYes
        end
        object EdFimVisPrv: TdmkEdit
          Left = 324
          Top = 32
          Width = 40
          Height = 21
          TabOrder = 3
          FormatType = dmktfTime
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '00:00'
          QryCampo = 'FimVisPrv'
          UpdCampo = 'FimVisPrv'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object TPFimExePrv: TdmkEditDateTimePicker
          Left = 216
          Top = 56
          Width = 108
          Height = 21
          Date = 0.639644131944805900
          Time = 0.639644131944805900
          TabOrder = 6
          ReadOnly = False
          DefaultEditMask = '!99/99/99;1;_'
          AutoApplyEditMask = True
          QryCampo = 'FimExePrv'
          UpdCampo = 'FimExePrv'
          UpdType = utYes
        end
        object EdFimExePrv: TdmkEdit
          Left = 324
          Top = 56
          Width = 40
          Height = 21
          TabOrder = 7
          FormatType = dmktfTime
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '00:00'
          QryCampo = 'FimExePrv'
          UpdCampo = 'FimExePrv'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object EdDtaExePrv: TdmkEdit
          Left = 172
          Top = 56
          Width = 40
          Height = 21
          TabOrder = 5
          FormatType = dmktfTime
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '00:00'
          QryCampo = 'DtaExePrv'
          UpdCampo = 'DtaExePrv'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
          OnExit = EdDtaExePrvExit
        end
        object TPDtaExePrv: TdmkEditDateTimePicker
          Left = 64
          Top = 56
          Width = 108
          Height = 21
          Date = 0.639644131944805900
          Time = 0.639644131944805900
          TabOrder = 4
          OnExit = TPDtaExePrvExit
          ReadOnly = False
          DefaultEditMask = '!99/99/99;1;_'
          AutoApplyEditMask = True
          QryCampo = 'DtaExePrv'
          UpdCampo = 'DtaExePrv'
          UpdType = utYes
        end
      end
      object EdAgeEqiCab: TdmkEditCB
        Left = 16
        Top = 399
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 22
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'AgeEqiCab'
        UpdCampo = 'AgeEqiCab'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBAgeEqiCab
        IgnoraDBLookupComboBox = False
      end
      object CBAgeEqiCab: TdmkDBLookupComboBox
        Left = 72
        Top = 399
        Width = 496
        Height = 21
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsAgeEqiCab
        TabOrder = 23
        dmkEditCB = EdAgeEqiCab
        QryCampo = 'AgeEqiCab'
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdEntPagante: TdmkEditCB
        Left = 16
        Top = 268
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 17
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'EntContrat'
        UpdCampo = 'EntContrat'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = EdEntContratChange
        OnKeyDown = EdEntPaganteKeyDown
        DBLookupComboBox = CBEntPagante
        IgnoraDBLookupComboBox = False
      end
      object CBEntPagante: TdmkDBLookupComboBox
        Left = 76
        Top = 268
        Width = 485
        Height = 21
        KeyField = 'Codigo'
        ListField = 'NO_ENT'
        ListSource = DsEntPagante
        TabOrder = 18
        OnKeyDown = CBEntPaganteKeyDown
        dmkEditCB = EdEntPagante
        QryCampo = 'EntContrat'
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdVendedor: TdmkEditCB
        Left = 16
        Top = 308
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 19
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Vendedor'
        UpdCampo = 'Vendedor'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBVendedor
        IgnoraDBLookupComboBox = False
      end
      object CBVendedor: TdmkDBLookupComboBox
        Left = 76
        Top = 308
        Width = 485
        Height = 21
        KeyField = 'Codigo'
        ListField = 'NO_ENT'
        ListSource = DsEntVendedor
        TabOrder = 20
        dmkEditCB = EdVendedor
        QryCampo = 'Vendedor'
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
    end
    object DBGOSPrz: TDBGrid
      Left = 0
      Top = 488
      Width = 585
      Height = 52
      Align = alClient
      DataSource = DsOSPrz
      PopupMenu = PMOSPrz
      TabOrder = 2
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      OnDrawColumnCell = DBGOSPrzDrawColumnCell
      OnMouseUp = DBGOSPrzMouseUp
      Columns = <
        item
          Expanded = False
          FieldName = 'Escolhido'
          Title.Caption = 'E'
          Width = 17
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_CONDICAO'
          Title.Caption = 'Descri'#231#227'o da condi'#231#227'o de pagamento'
          Width = 292
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DescoPer'
          Title.Caption = '% Desconto'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'VAL_COM_DESCO'
          Title.Caption = '$ TOTAL'
          Width = 72
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Parcelas'
          Title.Caption = 'Parc.'
          Width = 32
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'VAL_PARCELA'
          Title.Caption = '$ Parcela'
          Visible = True
        end>
    end
  end
  object Panel2: TPanel
    Left = 585
    Top = 0
    Width = 415
    Height = 540
    Align = alClient
    TabOrder = 2
    ExplicitHeight = 476
    object Splitter4: TSplitter
      Left = 1
      Top = 145
      Width = 413
      Height = 5
      Cursor = crVSplit
      Align = alTop
      ExplicitTop = 258
    end
    object GroupBox10: TGroupBox
      Left = 1
      Top = 1
      Width = 413
      Height = 144
      Align = alTop
      Caption = ' Pragas alvo: '
      TabOrder = 0
      object Label9: TLabel
        Left = 2
        Top = 15
        Width = 409
        Height = 13
        Align = alTop
        Caption = 'Clique com o bot'#227'o direito do mouse para abrir a lista de op'#231#245'es'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clGreen
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        ExplicitWidth = 361
      end
      object DBGOSCabAlv: TDBGrid
        Left = 2
        Top = 28
        Width = 409
        Height = 114
        Align = alClient
        DataSource = DsOSCabAlv
        PopupMenu = PMOSCabAlv
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        OnMouseUp = DBGOSCabAlvMouseUp
        Columns = <
          item
            Expanded = False
            FieldName = 'NO_NIVEL'
            Title.Caption = 'N'
            Width = 18
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NO_PRAGA'
            Title.Caption = 'Praga'
            Width = 224
            Visible = True
          end>
      end
    end
    object DBGOSAge: TDBGrid
      Left = 1
      Top = 309
      Width = 413
      Height = 230
      Align = alClient
      DataSource = DsOSAge
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
      PopupMenu = PMOSAge
      TabOrder = 1
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Visible = False
      OnMouseUp = DBGOSAgeMouseUp
      Columns = <
        item
          Expanded = False
          FieldName = 'Responsa'
          Title.Caption = 'R'
          Width = 17
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_AGENTE'
          Title.Caption = 'Agente'
          Width = 160
          Visible = True
        end>
    end
    object GBObservacoes: TGroupBox
      Left = 1
      Top = 150
      Width = 413
      Height = 159
      Align = alTop
      Caption = ' Obseva'#231#245'es: '
      TabOrder = 2
      object Splitter11: TSplitter
        Left = 2
        Top = 85
        Width = 409
        Height = 5
        Cursor = crVSplit
        Align = alTop
        ExplicitTop = 79
        ExplicitWidth = 1146
      end
      object PnObsExecuta: TPanel
        Left = 2
        Top = 15
        Width = 409
        Height = 70
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 0
        object dmkLabelRotate2: TdmkLabelRotate
          Left = 0
          Top = 0
          Width = 25
          Height = 70
          Angle = ag90
          Caption = 'Execu'#231#227'o'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Align = alLeft
          ExplicitHeight = 64
        end
        object MeObsExecuta: TdmkMemo
          Left = 25
          Top = 0
          Width = 384
          Height = 70
          Align = alClient
          TabOrder = 0
          QryCampo = 'ObsExecuta'
          UpdCampo = 'ObsExecuta'
          UpdType = utYes
        end
      end
      object PnObsGaranti: TPanel
        Left = 2
        Top = 90
        Width = 409
        Height = 67
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 1
        object dmkLabelRotate3: TdmkLabelRotate
          Left = 0
          Top = 0
          Width = 25
          Height = 67
          Angle = ag90
          Caption = 'Garantia'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Align = alLeft
          ExplicitHeight = 64
        end
        object MeObsGaranti: TdmkMemo
          Left = 25
          Top = 0
          Width = 384
          Height = 67
          Align = alClient
          TabOrder = 0
          QryCampo = 'ObsGaranti'
          UpdCampo = 'ObsGaranti'
          UpdType = utYes
        end
      end
    end
  end
  object DBGOSPrv: TDBGrid
    Left = 0
    Top = 553
    Width = 1000
    Height = 80
    Align = alBottom
    DataSource = DsOSPrv
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
    PopupMenu = PMOSPrv
    TabOrder = 3
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    OnMouseUp = DBGOSPrvMouseUp
    Columns = <
      item
        Expanded = False
        FieldName = 'DtHrContat'
        Title.Caption = 'Data / hora contato'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NO_FormContat'
        Title.Caption = 'Forma de contato'
        Width = 100
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NO_Contato'
        Title.Caption = 'Contato'
        Width = 100
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NO_Agente'
        Title.Caption = 'Agente'
        Width = 100
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Nome'
        Title.Caption = 'Solicita'#231#227'o de provid'#234'ncia'
        Width = 598
        Visible = True
      end>
  end
  object QrEntidades: TmySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrEntidadesBeforeClose
    SQL.Strings = (
      'SELECT Codigo, '
      'IF(Tipo=0, RazaoSocial, Nome) NO_ENT'
      'FROM entidades '
      'WHERE Codigo IN ('
      '     SELECT Codigo'
      '     FROM cunscad'
      ')'
      'ORDER BY NO_ENT')
    Left = 680
    object QrEntidadesCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEntidadesNO_ENT: TWideStringField
      FieldName = 'NO_ENT'
      Size = 100
    end
  end
  object DsEntidades: TDataSource
    DataSet = QrEntidades
    Left = 708
  end
  object QrSiapTerCad: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *'
      'FROM siaptercad'
      'WHERE Cliente=100')
    Left = 680
    Top = 28
    object QrSiapTerCadCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrSiapTerCadNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QrSiapTerCadCliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrSiapTerCadSLograd: TSmallintField
      FieldName = 'SLograd'
    end
    object QrSiapTerCadSRua: TWideStringField
      FieldName = 'SRua'
      Size = 60
    end
    object QrSiapTerCadSNumero: TIntegerField
      FieldName = 'SNumero'
    end
    object QrSiapTerCadSCompl: TWideStringField
      FieldName = 'SCompl'
      Size = 60
    end
    object QrSiapTerCadSBairro: TWideStringField
      FieldName = 'SBairro'
      Size = 60
    end
    object QrSiapTerCadSCidade: TWideStringField
      FieldName = 'SCidade'
      Size = 60
    end
    object QrSiapTerCadSUF: TWideStringField
      FieldName = 'SUF'
      Size = 3
    end
    object QrSiapTerCadSCEP: TIntegerField
      FieldName = 'SCEP'
    end
    object QrSiapTerCadSPais: TWideStringField
      FieldName = 'SPais'
      Size = 60
    end
    object QrSiapTerCadSEndeRef: TWideStringField
      FieldName = 'SEndeRef'
      Size = 100
    end
    object QrSiapTerCadSCodMunici: TIntegerField
      FieldName = 'SCodMunici'
    end
    object QrSiapTerCadSCodiPais: TIntegerField
      FieldName = 'SCodiPais'
    end
    object QrSiapTerCadSTe1: TWideStringField
      FieldName = 'STe1'
    end
    object QrSiapTerCadM2Constru: TFloatField
      FieldName = 'M2Constru'
    end
    object QrSiapTerCadM2NaoBuild: TFloatField
      FieldName = 'M2NaoBuild'
    end
    object QrSiapTerCadM2Terreno: TFloatField
      FieldName = 'M2Terreno'
    end
    object QrSiapTerCadM2Total: TFloatField
      FieldName = 'M2Total'
    end
  end
  object DsSiapterCad: TDataSource
    DataSet = QrSiapTerCad
    Left = 708
    Top = 28
  end
  object QrEstatusOSs: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM estatusoss'
      'ORDER BY Codigo')
    Left = 680
    Top = 56
    object QrEstatusOSsCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEstatusOSsNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
  end
  object DsEstatusOSs: TDataSource
    DataSet = QrEstatusOSs
    Left = 708
    Top = 56
  end
  object QrFatoGeradr: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome, NeedOS_Ori'
      'FROM fatogeradr'
      'ORDER BY Nome')
    Left = 680
    Top = 84
    object QrFatoGeradrCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrFatoGeradrNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
    object QrFatoGeradrNeedOS_Ori: TSmallintField
      FieldName = 'NeedOS_Ori'
    end
  end
  object DsFatoGeradr: TDataSource
    DataSet = QrFatoGeradr
    Left = 708
    Top = 84
  end
  object QrEntiContat: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT eco.Codigo, eco.Controle, eco.Nome, eco.Cargo,'
      'eca.Nome NOME_CARGO '
      'FROM enticontat eco'
      'LEFT JOIN enticargos eca ON eca.Codigo=eco.Cargo'
      'WHERE eco.Codigo=:P0'
      ''
      ''
      '')
    Left = 680
    Top = 112
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrEntiContatControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrEntiContatNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 30
    end
    object QrEntiContatCargo2: TIntegerField
      FieldName = 'Cargo'
      Required = True
    end
    object QrEntiContatNOME_CARGO2: TWideStringField
      FieldName = 'NOME_CARGO'
      Size = 30
    end
    object QrEntiContatQUE_ENT2: TWideStringField
      FieldName = 'QUE_ENT'
      Size = 2
    end
    object QrEntiContatCodigo: TIntegerField
      FieldName = 'Codigo'
    end
  end
  object DsEntiContat: TDataSource
    DataSet = QrEntiContat
    Left = 708
    Top = 112
  end
  object QrEntContrat: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, '
      'IF(Tipo=0, RazaoSocial, Nome) NO_ENT'
      'FROM entidades '
      'WHERE Codigo IN ('
      '     SELECT Codigo'
      '     FROM cunscad'
      ')'
      'ORDER BY NO_ENT')
    Left = 680
    Top = 140
    object QrEntContratCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEntContratNO_ENT: TWideStringField
      FieldName = 'NO_ENT'
      Size = 100
    end
  end
  object DsEntContrat: TDataSource
    DataSet = QrEntContrat
    Left = 708
    Top = 140
  end
  object CsEdDepto: TdmkCompoStore
    Left = 240
    Top = 65524
  end
  object CsAbaOrigem: TdmkCompoStore
    Left = 212
    Top = 65524
  end
  object PMOSCabAlv: TPopupMenu
    OnPopup = PMOSCabAlvPopup
    Left = 736
    Top = 172
    object IncluiOSCabAlv1: TMenuItem
      Caption = '&Adiciona praga alvo'
      OnClick = IncluiOSCabAlv1Click
    end
    object AlteraOSCabAlv1: TMenuItem
      Caption = '&Muda a praga alvo selecionada'
      Visible = False
      OnClick = AlteraOSCabAlv1Click
    end
    object ExcluiOSCabAlv1: TMenuItem
      Caption = '&Retira praga alvo'
      OnClick = ExcluiOSCabAlv1Click
    end
  end
  object QrOSCabAlv: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT oca.*, IF(oca.Praga_Z <> 0, '#39'E'#39', '#39'G'#39') NO_NIVEL, '
      'IF(oca.Praga_Z <> 0, pcz.Nome, pca.Nome) NO_PRAGA'
      'FROM oscabalv oca'
      'LEFT JOIN praga_a pca ON pca.Codigo=oca.Praga_A'
      'LEFT JOIN praga_z pcz ON pcz.Codigo=oca.Praga_Z'
      'WHERE oca.Codigo=:P0'
      'ORDER BY oca.Ordem')
    Left = 680
    Top = 168
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrOSCabAlvCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrOSCabAlvControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrOSCabAlvOrdem: TSmallintField
      FieldName = 'Ordem'
    end
    object QrOSCabAlvAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrOSCabAlvPraga_A: TIntegerField
      FieldName = 'Praga_A'
    end
    object QrOSCabAlvPraga_Z: TIntegerField
      FieldName = 'Praga_Z'
    end
    object QrOSCabAlvNO_NIVEL: TWideStringField
      FieldName = 'NO_NIVEL'
      Required = True
      Size = 1
    end
    object QrOSCabAlvNO_PRAGA: TWideStringField
      FieldName = 'NO_PRAGA'
      Size = 60
    end
  end
  object DsOSCabAlv: TDataSource
    DataSet = QrOSCabAlv
    Left = 709
    Top = 168
  end
  object PMSiapTerCad: TPopupMenu
    OnPopup = PMSiapTerCadPopup
    Left = 424
    Top = 100
    object Inclui1: TMenuItem
      Caption = '&Inclui'
      object Guia1: TMenuItem
        Caption = '&Aba'
        OnClick = Guia1Click
      end
      object Janela1: TMenuItem
        Caption = '&Janela'
        OnClick = Janela1Click
      end
    end
    object Altera1: TMenuItem
      Caption = '&Altera'
      OnClick = Altera1Click
    end
  end
  object CsSbUserAction: TdmkCompoStore
    Left = 296
    Top = 65524
  end
  object QrOSCab: TmySQLQuery
    Database = Dmod.MyDB
    Left = 12
    Top = 12
    object QrOSCabCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrOSCabEntidade: TIntegerField
      FieldName = 'Entidade'
    end
    object QrOSCabSiapTerCad: TIntegerField
      FieldName = 'SiapTerCad'
    end
    object QrOSCabEstatus: TIntegerField
      FieldName = 'Estatus'
    end
    object QrOSCabFatoGeradr: TIntegerField
      FieldName = 'FatoGeradr'
    end
    object QrOSCabDtaContat: TDateTimeField
      FieldName = 'DtaContat'
    end
    object QrOSCabDtaVisPrv: TDateTimeField
      FieldName = 'DtaVisPrv'
    end
    object QrOSCabDtaVisExe: TDateTimeField
      FieldName = 'DtaVisExe'
    end
    object QrOSCabDtaExePrv: TDateTimeField
      FieldName = 'DtaExePrv'
    end
    object QrOSCabDtaExeIni: TDateTimeField
      FieldName = 'DtaExeIni'
    end
    object QrOSCabDtaExeFim: TDateTimeField
      FieldName = 'DtaExeFim'
    end
    object QrOSCabValorTotal: TFloatField
      FieldName = 'ValorTotal'
    end
    object QrOSCabLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrOSCabDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrOSCabDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrOSCabUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrOSCabUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrOSCabAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrOSCabAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrOSCabDdsPosVda: TIntegerField
      FieldName = 'DdsPosVda'
    end
    object QrOSCabEntiContat: TIntegerField
      FieldName = 'EntiContat'
    end
    object QrOSCabNumContrat: TIntegerField
      FieldName = 'NumContrat'
    end
    object QrOSCabEntPagante: TIntegerField
      FieldName = 'EntPagante'
    end
    object QrOSCabEntContrat: TIntegerField
      FieldName = 'EntContrat'
    end
    object QrOSCabOperacao: TIntegerField
      FieldName = 'Operacao'
    end
  end
  object QrOSPrz: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrOSPrzCalcFields
    SQL.Strings = (
      'SELECT opz.Parcelas, '
      'opz.Codigo, opz.Controle, opz.Condicao, '
      'opz.DescoPer, opz.Escolhido, ppc.Nome NO_CONDICAO'
      'FROM osprz opz'
      'LEFT JOIN pediprzcab ppc ON ppc.Codigo=opz.Condicao'
      'WHERE opz.Codigo=:P0')
    Left = 680
    Top = 196
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrOSPrzCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'osprz.Codigo'
    end
    object QrOSPrzControle: TIntegerField
      FieldName = 'Controle'
      Origin = 'osprz.Controle'
    end
    object QrOSPrzCondicao: TIntegerField
      FieldName = 'Condicao'
      Origin = 'osprz.Condicao'
    end
    object QrOSPrzNO_CONDICAO: TWideStringField
      FieldName = 'NO_CONDICAO'
      Origin = 'pediprzcab.Nome'
      Size = 50
    end
    object QrOSPrzDescoPer: TFloatField
      FieldName = 'DescoPer'
      Origin = 'osprz.DescoPer'
      DisplayFormat = '0.00;-0.00; '
    end
    object QrOSPrzEscolhido: TSmallintField
      FieldName = 'Escolhido'
      Origin = 'osprz.Escolhido'
      MaxValue = 1
    end
    object QrOSPrzVAL_COM_DESCO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'VAL_COM_DESCO'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
      Calculated = True
    end
    object QrOSPrzVAL_PARCELA: TFloatField
      FieldKind = fkCalculated
      FieldName = 'VAL_PARCELA'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
      Calculated = True
    end
    object QrOSPrzParcelas: TIntegerField
      FieldName = 'Parcelas'
      DisplayFormat = '00;-00; '
    end
  end
  object DsOSPrz: TDataSource
    DataSet = QrOSPrz
    Left = 708
    Top = 196
  end
  object PMOSPrz: TPopupMenu
    OnPopup = PMOSPrzPopup
    Left = 736
    Top = 200
    object OSPrz1_Inclui: TMenuItem
      Caption = '&Adiciona nova condi'#231#227'o de pagamento'
      Enabled = False
      OnClick = OSPrz1_IncluiClick
    end
    object OSPrz1_Altera: TMenuItem
      Caption = '&Edita a condi'#231#227'o de pagamento atual'
      Enabled = False
      OnClick = OSPrz1_AlteraClick
    end
    object OSPrz1_Exclui: TMenuItem
      Caption = '&Remove condi'#231'ao de pagamento atual'
      Enabled = False
      OnClick = OSPrz1_ExcluiClick
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object OSPrz1_AVista: TMenuItem
      Caption = 'Calcular valor '#224' vista'
      OnClick = OSPrz1_AVistaClick
    end
  end
  object QrOSAge: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ent.Nome NO_AGENTE, age.* '
      'FROM osage age'
      'LEFT JOIN entidades ent on ent.Codigo=age.Agente'
      'WHERE age.Codigo=:P0')
    Left = 680
    Top = 224
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrOSAgeNO_AGENTE: TWideStringField
      FieldName = 'NO_AGENTE'
      Size = 100
    end
    object QrOSAgeCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrOSAgeControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrOSAgeAgente: TIntegerField
      FieldName = 'Agente'
    end
    object QrOSAgeResponsa: TSmallintField
      FieldName = 'Responsa'
    end
  end
  object DsOSAge: TDataSource
    DataSet = QrOSAge
    Left = 708
    Top = 224
  end
  object PMOSAge: TPopupMenu
    Left = 736
    Top = 224
    object IncluiOSAge1: TMenuItem
      Caption = '&Adiciona agente(s)'
      OnClick = IncluiOSAge1Click
    end
    object RemoveOSAge1: TMenuItem
      Caption = '&Retira agente'
      OnClick = RemoveOSAge1Click
    end
  end
  object QrOSPrv: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT prv.*, '
      'IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NO_Cliente, '
      'IF(age.Tipo=0, age.RazaoSocial, age.Nome) NO_Agente, '
      'eco.Nome NO_Contato, fct.Nome NO_FormContat  '
      'FROM osprv prv '
      'LEFT JOIN entidades cli ON cli.Codigo=prv.Cliente '
      'LEFT JOIN entidades age ON age.Codigo=prv.Agente '
      'LEFT JOIN enticontat eco ON eco.Controle=prv.Contato '
      'LEFT JOIN formcontat fct ON fct.Codigo=prv.FormContat '
      ' '
      ' ')
    Left = 680
    Top = 252
    object QrOSPrvCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrOSPrvControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrOSPrvDtHrContat: TDateTimeField
      FieldName = 'DtHrContat'
    end
    object QrOSPrvFormContat: TIntegerField
      FieldName = 'FormContat'
    end
    object QrOSPrvCliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrOSPrvContato: TIntegerField
      FieldName = 'Contato'
    end
    object QrOSPrvAgente: TIntegerField
      FieldName = 'Agente'
    end
    object QrOSPrvNome: TWideStringField
      FieldName = 'Nome'
      Size = 255
    end
    object QrOSPrvNO_Cliente: TWideStringField
      FieldName = 'NO_Cliente'
      Size = 100
    end
    object QrOSPrvNO_Agente: TWideStringField
      FieldName = 'NO_Agente'
      Size = 100
    end
    object QrOSPrvNO_Contato: TWideStringField
      FieldName = 'NO_Contato'
      Size = 30
    end
    object QrOSPrvNO_FormContat: TWideStringField
      FieldName = 'NO_FormContat'
      Size = 60
    end
  end
  object DsOSPrv: TDataSource
    DataSet = QrOSPrv
    Left = 708
    Top = 252
  end
  object PMOSPrv: TPopupMenu
    Left = 736
    Top = 252
    object Adicionasolicitaesdeprovidncias1: TMenuItem
      Caption = '&Adiciona solicita'#231#245'es de provid'#234'ncias'
      OnClick = Adicionasolicitaesdeprovidncias1Click
    end
    object Removeasolicitaesdeprovidnciaselecionada1: TMenuItem
      Caption = '&Remove a solicita'#231#245'es de provid'#234'ncia selecionada'
      OnClick = Removeasolicitaesdeprovidnciaselecionada1Click
    end
  end
  object QrAgeEqiIts: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT aei.Controle, aei.Entidade'
      'FROM ageeqiits aei'
      'WHERE aei.Codigo=1')
    Left = 744
    Top = 333
    object QrAgeEqiItsControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrAgeEqiItsEntidade: TIntegerField
      FieldName = 'Entidade'
    end
    object QrAgeEqiItsEhLider: TSmallintField
      FieldName = 'EhLider'
    end
  end
  object QrAgeEqiCab: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM ageeqicab '
      'ORDER BY Nome'
      ' ')
    Left = 684
    Top = 332
    object IntegerField2: TIntegerField
      FieldName = 'Codigo'
    end
    object QrAgeEqiCabNome: TWideStringField
      FieldName = 'Nome'
      Size = 255
    end
  end
  object DsAgeEqiCab: TDataSource
    DataSet = QrAgeEqiCab
    Left = 712
    Top = 332
  end
  object QrEntPagante: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, '
      'IF(Tipo=0, RazaoSocial, Nome) NO_ENT'
      'FROM entidades '
      'WHERE Codigo IN ('
      '     SELECT Codigo'
      '     FROM cunscad'
      ')'
      'ORDER BY NO_ENT')
    Left = 680
    Top = 384
    object QrEntPaganteCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEntPaganteNO_ENT: TWideStringField
      FieldName = 'NO_ENT'
      Size = 100
    end
  end
  object QrOSMonPipDd: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM osmonpipdd'
      'WHERE Conta>0'
      'ORDER BY Ordem, IDIts')
    Left = 1432
    Top = 504
    object QrOSMonPipDdCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrOSMonPipDdControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrOSMonPipDdConta: TIntegerField
      FieldName = 'Conta'
    end
    object QrOSMonPipDdIDIts: TIntegerField
      FieldName = 'IDIts'
    end
    object QrOSMonPipDdOrdem: TIntegerField
      FieldName = 'Ordem'
    end
    object QrOSMonPipDdDias: TIntegerField
      FieldName = 'Dias'
    end
    object QrOSMonPipDdLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrOSMonPipDdDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrOSMonPipDdDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrOSMonPipDdUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrOSMonPipDdUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrOSMonPipDdAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrOSMonPipDdAtivo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object DsEntPagante: TDataSource
    DataSet = QrEntPagante
    Left = 708
    Top = 384
  end
  object QrEntVendedor: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, '
      'IF(Tipo=0, RazaoSocial, Nome) NO_ENT'
      'FROM entidades '
      'WHERE Fornece3 = "V"'
      'ORDER BY NO_ENT')
    Left = 664
    Top = 452
    object IntegerField1: TIntegerField
      FieldName = 'Codigo'
    end
    object StringField1: TWideStringField
      FieldName = 'NO_ENT'
      Size = 100
    end
  end
  object DsEntVendedor: TDataSource
    DataSet = QrEntVendedor
    Left = 692
    Top = 452
  end
end
