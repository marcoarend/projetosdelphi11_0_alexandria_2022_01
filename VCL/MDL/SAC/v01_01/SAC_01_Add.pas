unit SAC_01_Add;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ComCtrls, dmkEditDateTimePicker, dmkEdit, ExtCtrls, Menus,
  DBCtrls, dmkDBLookupComboBox, dmkEditCB, Buttons, DB, mySQLDbTables, dmkGeral,
  dmkImage, dmkVariable, dmkMemo, Grids, DBGrids, DmkDAC_PF, UnAppListas,
  //UnBugs_Tabs,
  UnProjGroup_Consts, UnDmkEnums, MyListas;

type
  TFmSAC_01_add = class(TForm)
    Panel1: TPanel;
    PnTempo: TPanel;
    GroupBox1: TGroupBox;
    Label2: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    EdCodigo: TdmkEdit;
    TPData: TdmkEditDateTimePicker;
    EdHora: TdmkEdit;
    GroupBox2: TGroupBox;
    Label111: TLabel;
    Label3: TLabel;
    Label13: TLabel;
    EdBinaFone: TdmkEdit;
    TPBinaSoDt: TdmkEditDateTimePicker;
    EdBinaSoHr: TdmkEdit;
    QrCliInt: TmySQLQuery;
    QrCliIntCodigo: TIntegerField;
    QrCliIntFilial: TIntegerField;
    QrCliIntNOMEFILIAL: TWideStringField;
    QrCliIntCNPJ_CPF: TWideStringField;
    DsCliInt: TDataSource;
    QrClientes: TmySQLQuery;
    QrClientesCodigo: TIntegerField;
    QrClientesCodUsu: TIntegerField;
    QrClientesCliInt: TIntegerField;
    QrClientesNOMEENTIDADE: TWideStringField;
    DsClientes: TDataSource;
    QrTerceiro01: TmySQLQuery;
    QrTerceiro01Codigo: TIntegerField;
    QrTerceiro01CliInt: TIntegerField;
    QrTerceiro01CodUsu: TIntegerField;
    QrTerceiro01NOMEENTIDADE: TWideStringField;
    DsTerceiro01: TDataSource;
    PnFiltro: TPanel;
    QrDiarioAss: TmySQLQuery;
    QrDiarioAssCodigo: TIntegerField;
    QrDiarioAssCodUsu: TIntegerField;
    QrDiarioAssNome: TWideStringField;
    QrDiarioAssAplicacao: TIntegerField;
    DsDiarioAss: TDataSource;
    DsInterloctr: TDataSource;
    QrInterloctr: TmySQLQuery;
    Panel2: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    VaChamou: TdmkVariable;
    VaTipo: TdmkVariable;
    VaGenero: TdmkVariable;
    VaPreAtend: TdmkVariable;
    QrEntiCargos: TmySQLQuery;
    DsEntiCargos: TDataSource;
    QrETCFone: TmySQLQuery;
    DsETCFone: TDataSource;
    QrETCMail: TmySQLQuery;
    DsETCMail: TDataSource;
    GBGerenInterlocutor: TGroupBox;
    GroupBox3: TGroupBox;
    GBContato: TGroupBox;
    Panel3: TPanel;
    Panel10: TPanel;
    DBGrid1: TDBGrid;
    Panel11: TPanel;
    DBGrid2: TDBGrid;
    DBGEE: TDBGrid;
    PnEC: TPanel;
    Label7: TLabel;
    EdECCargo: TdmkEditCB;
    CBECCargo: TdmkDBLookupComboBox;
    SbECCargo: TSpeedButton;
    EdETNome: TdmkEdit;
    SbETNome: TSpeedButton;
    PnET: TPanel;
    EdETTelefone: TdmkEdit;
    SbETTelefone: TSpeedButton;
    EdETRamal: TdmkEdit;
    LaETCRamal: TLabel;
    SbETCFone: TSpeedButton;
    CBETCFone: TdmkDBLookupComboBox;
    EdETCFone: TdmkEditCB;
    LaETCFone: TLabel;
    PnEE: TPanel;
    EdEEEmail: TdmkEdit;
    SbEEEMail: TSpeedButton;
    SbETCEMail: TSpeedButton;
    CBETCMail: TdmkDBLookupComboBox;
    EdETCMail: TdmkEditCB;
    LaETCMail: TLabel;
    QrEntiCargosCodigo: TIntegerField;
    QrEntiCargosNome: TWideStringField;
    QrETCFoneCodigo: TIntegerField;
    QrETCFoneNome: TWideStringField;
    QrETCMailCodigo: TIntegerField;
    QrETCMailNome: TWideStringField;
    QrEC: TmySQLQuery;
    QrECControle: TIntegerField;
    QrECNome: TWideStringField;
    QrECCargo: TIntegerField;
    QrECNOME_CARGO: TWideStringField;
    DsEC: TDataSource;
    QrEE: TmySQLQuery;
    QrEENOMEETC: TWideStringField;
    QrEEConta: TIntegerField;
    QrEEEMail: TWideStringField;
    QrEEEntiTipCto: TIntegerField;
    QrEEOrdem: TIntegerField;
    DsEE: TDataSource;
    QrET: TmySQLQuery;
    QrETNOMEETC: TWideStringField;
    QrETConta: TIntegerField;
    QrETTelefone: TWideStringField;
    QrETEntiTipCto: TIntegerField;
    QrETTEL_TXT: TWideStringField;
    QrETRamal: TWideStringField;
    DsET: TDataSource;
    PMEE: TPopupMenu;
    ExcluiEE1: TMenuItem;
    PMEC: TPopupMenu;
    ExcluiEC1: TMenuItem;
    PMET: TPopupMenu;
    ExcluiET1: TMenuItem;
    QrInterloctrControle: TIntegerField;
    QrInterloctrNome: TWideStringField;
    QrEntiContat: TmySQLQuery;
    QrEntiContatControle: TIntegerField;
    QrEntiContatNome: TWideStringField;
    QrEntiContatCargo: TIntegerField;
    QrEntiContatNOME_CARGO: TWideStringField;
    DsEntiContat: TDataSource;
    QrEntiMail: TmySQLQuery;
    QrEntiMailNOMEETC: TWideStringField;
    QrEntiMailConta: TIntegerField;
    QrEntiMailEMail: TWideStringField;
    QrEntiMailEntiTipCto: TIntegerField;
    QrEntiMailOrdem: TIntegerField;
    DsEntiMail: TDataSource;
    QrEntiTel: TmySQLQuery;
    QrEntiTelNOMEETC: TWideStringField;
    QrEntiTelConta: TIntegerField;
    QrEntiTelTelefone: TWideStringField;
    QrEntiTelEntiTipCto: TIntegerField;
    QrEntiTelTEL_TXT: TWideStringField;
    QrEntiTelRamal: TWideStringField;
    DsEntiTel: TDataSource;
    Definecomocontatodo1: TMenuItem;
    Cliente1: TMenuItem;
    Contratante1: TMenuItem;
    PMEntidades: TPopupMenu;
    Rpido1: TMenuItem;
    CompletoJanela1: TMenuItem;
    PMTerceiro01: TPopupMenu;
    Rpido2: TMenuItem;
    CompletoJanela2: TMenuItem;
    QrEntiContatQUE_ENT: TWideStringField;
    PMContatos: TPopupMenu;
    IncluiContato1: TMenuItem;
    AlteraContato1: TMenuItem;
    ExcluiContato1: TMenuItem;
    PMEMails: TPopupMenu;
    NovoEMail1: TMenuItem;
    Alteraemailselecionado1: TMenuItem;
    Excluiemailselecionado1: TMenuItem;
    PMTelefones: TPopupMenu;
    Incluinovotelefone1: TMenuItem;
    Alteratelefoneatual1: TMenuItem;
    Excluitelefones1: TMenuItem;
    SubClienteA1: TMenuItem;
    ContratanteA1: TMenuItem;
    GroupBox4: TGroupBox;
    Label8: TLabel;
    EdFormContat: TdmkEditCB;
    CBFormContat: TdmkDBLookupComboBox;
    SbFormContat: TSpeedButton;
    LaInterlctr1A: TLabel;
    LaInterlctr1B: TLabel;
    QrFormContat: TmySQLQuery;
    DsFormContat: TDataSource;
    QrFormContatCodigo: TIntegerField;
    QrFormContatNome: TWideStringField;
    Panel5: TPanel;
    PnContatos: TPanel;
    DBGEntiContat: TDBGrid;
    Panel8: TPanel;
    DBGEntiTel: TDBGrid;
    DBGEntiMail: TDBGrid;
    Panel6: TPanel;
    Panel9: TPanel;
    Panel7: TPanel;
    LaDepto: TLabel;
    SbUserAction: TSpeedButton;
    EdDepto: TdmkEdit;
    RGUserGenero: TRadioGroup;
    GroupBox5: TGroupBox;
    Panel12: TPanel;
    LaCliInt: TLabel;
    SpeedButton4: TSpeedButton;
    LaInterloctr: TLabel;
    SbInterloctr: TSpeedButton;
    LaEntidade: TLabel;
    SbEntidade: TSpeedButton;
    Label1: TLabel;
    SpeedButton1: TSpeedButton;
    LaTerceiro01: TLabel;
    SbTerceiro01: TSpeedButton;
    EdCliInt: TdmkEditCB;
    CBCliInt: TdmkDBLookupComboBox;
    EdInterloctr: TdmkEditCB;
    CBInterloctr: TdmkDBLookupComboBox;
    EdEntidade: TdmkEditCB;
    CBEntidade: TdmkDBLookupComboBox;
    EdDiarioAss: TdmkEditCB;
    CBDiarioAss: TdmkDBLookupComboBox;
    EdTerceiro01: TdmkEditCB;
    CBTerceiro01: TdmkDBLookupComboBox;
    Panel13: TPanel;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel4: TPanel;
    BtDesiste: TBitBtn;
    MeNome: TdmkMemo;
    Panel14: TPanel;
    GroupBox6: TGroupBox;
    Panel15: TPanel;
    CkHistEnt: TCheckBox;
    CkHistTer: TCheckBox;
    CkHistInt: TCheckBox;
    DBGrid3: TDBGrid;
    QrDiarioHist: TmySQLQuery;
    DsDiarioHist: TDataSource;
    QrDiarioHistCodigo: TIntegerField;
    QrDiarioHistData: TDateField;
    QrDiarioHistHora: TTimeField;
    DBMemo1: TDBMemo;
    QrDiarioHistNome: TWideStringField;
    QrDiarioHistGenero: TSmallintField;
    QrDiarioHistDepto: TIntegerField;
    QrDiarioHistNO_GENERO: TWideStringField;
    Alteracontatoatual2: TMenuItem;
    QrEnt0: TmySQLQuery;
    DsEnt0: TDataSource;
    Alteratelefoneselecionado1: TMenuItem;
    Alteraemailselecionado2: TMenuItem;
    BtParcial: TBitBtn;
    QrECDiarioAdd: TIntegerField;
    QrECDtaNatal: TDateField;
    QrEntiContatDiarioAdd: TIntegerField;
    QrEntiContatDtaNatal: TDateField;
    LaLetras: TLabel;
    BtNovaFrase: TBitBtn;
    PMBinaFone: TPopupMenu;
    Adicionaraointerlocutor1: TMenuItem;
    N1: TMenuItem;
    AtrelaraoSubcliente1: TMenuItem;
    AtrelaraoContratante1: TMenuItem;
    QrEntiContatQTD: TLargeintField;
    QrEntiContatCodigo: TIntegerField;
    QrEntiContatSexo: TSmallintField;
    QrECCodigo: TIntegerField;
    QrECSexo: TSmallintField;
    QrEnt0Codigo: TIntegerField;
    QrEnt0CodUsu: TIntegerField;
    QrEnt0CliInt: TIntegerField;
    QrEnt0NOMEENTIDADE: TWideStringField;
    QrECTipo: TIntegerField;
    QrECCNPJ: TWideStringField;
    QrECCPF: TWideStringField;
    QrECAplicacao: TIntegerField;
    QrECCNPJ_CPF: TWideStringField;
    QrEntiContatTipo: TIntegerField;
    QrEntiContatCNPJ: TWideStringField;
    QrEntiContatCPF: TWideStringField;
    QrEntiContatAplicacao: TIntegerField;
    QrEntiContatCNPJ_CPF: TWideStringField;
    QrDiarioOpc: TmySQLQuery;
    LaContratanteAviso: TLabel;
    Label4: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure SbEntiContat(Sender: TObject);
    procedure SbEntidadeClick(Sender: TObject);
    procedure SbECCargoClick(Sender: TObject);
    procedure SbETCFoneClick(Sender: TObject);
    procedure SbETCEMailClick(Sender: TObject);
    procedure SbETNomeClick(Sender: TObject);
    procedure QrECBeforeClose(DataSet: TDataSet);
    procedure QrECAfterScroll(DataSet: TDataSet);
    procedure EdBinaFoneDblClick(Sender: TObject);
    procedure SbETTelefoneClick(Sender: TObject);
    procedure QrETCalcFields(DataSet: TDataSet);
    procedure SbEEEMailClick(Sender: TObject);
    procedure PMEEPopup(Sender: TObject);
    procedure ExcluiEE1Click(Sender: TObject);
    procedure ExcluiET1Click(Sender: TObject);
    procedure PMETPopup(Sender: TObject);
    procedure PMECPopup(Sender: TObject);
    procedure ExcluiEC1Click(Sender: TObject);
    procedure EdCodigoChange(Sender: TObject);
    procedure EdEntidadeChange(Sender: TObject);
    procedure EdTerceiro01Change(Sender: TObject);
    procedure DBGrid1DblClick(Sender: TObject);
    procedure DBGEntiContatDblClick(Sender: TObject);
    procedure QrEntiContatBeforeClose(DataSet: TDataSet);
    procedure QrEntiContatAfterScroll(DataSet: TDataSet);
    procedure QrEntiTelCalcFields(DataSet: TDataSet);
    procedure Cliente1Click(Sender: TObject);
    procedure Contratante1Click(Sender: TObject);
    procedure EdInterloctrChange(Sender: TObject);
    procedure QrClientesBeforeClose(DataSet: TDataSet);
    procedure CompletoJanela1Click(Sender: TObject);
    procedure Rpido1Click(Sender: TObject);
    procedure Rpido2Click(Sender: TObject);
    procedure CompletoJanela2Click(Sender: TObject);
    procedure SbTerceiro01Click(Sender: TObject);
    procedure DBGEntiContatDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure AlteraContato1Click(Sender: TObject);
    procedure ExcluiContato1Click(Sender: TObject);
    procedure NovoEMail1Click(Sender: TObject);
    procedure Alteraemailselecionado1Click(Sender: TObject);
    procedure Excluiemailselecionado1Click(Sender: TObject);
    procedure Incluinovotelefone1Click(Sender: TObject);
    procedure Alteratelefoneatual1Click(Sender: TObject);
    procedure Excluitelefones1Click(Sender: TObject);
    procedure PMContatosPopup(Sender: TObject);
    procedure PMEMailsPopup(Sender: TObject);
    procedure PMTelefonesPopup(Sender: TObject);
    procedure SubClienteA1Click(Sender: TObject);
    procedure ContratanteA1Click(Sender: TObject);
    procedure SbFormContatClick(Sender: TObject);
    procedure RGUserGeneroClick(Sender: TObject);
    procedure EdDeptoChange(Sender: TObject);
    procedure SbUserActionClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure CkHistEntClick(Sender: TObject);
    procedure EdCliIntChange(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure EdDeptoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure Alteracontatoatual2Click(Sender: TObject);
    procedure Alteratelefoneselecionado1Click(Sender: TObject);
    procedure Alteraemailselecionado2Click(Sender: TObject);
    procedure EdEntidadeDblClick(Sender: TObject);
    procedure BtParcialClick(Sender: TObject);
    procedure MeNomeChange(Sender: TObject);
    procedure BtNovaFraseClick(Sender: TObject);
    procedure EdDiarioAssChange(Sender: TObject);
    procedure PMBinaFonePopup(Sender: TObject);
    procedure Adicionaraointerlocutor1Click(Sender: TObject);
    procedure QrClientesAfterScroll(DataSet: TDataSet);
    procedure QrTerceiro01BeforeClose(DataSet: TDataSet);
    procedure QrTerceiro01AfterScroll(DataSet: TDataSet);
    procedure AtrelaraoSubcliente1Click(Sender: TObject);
    procedure AtrelaraoContratante1Click(Sender: TObject);
  private
    FQrEntidades: TmySQLQuery;
    FDsEntidades: TDataSource;
    FSalvou: Boolean;
    FItensSemOS: array of Integer;
    //
    procedure HandleWndProc(var Msg: TMessage); message WM_SYSCOMMAND;
    //
    procedure AtrelaContatoAtual(Entidade: Integer);
    procedure EditaEntidade(Ed: TdmkEditCB; CB: TdmkDBLookupComboBox);
    procedure MostraEntiTipCto();
    procedure MostraEntiContat(SQLType: TSQLType; Que_Ent: String);
    procedure MostraEntiMail(SQLType: TSQLType;
              QrContat, QrMail: TmySQLQuery; DsContat: TDataSource;
              QrEnti: TmySQLQuery; DsEnti: TDataSource);
    procedure MostraEntiTel(SQLType: TSQLType;
              QrContat, QrTel: TmySQLQuery; DsContat: TDataSource;
              QrEnti: TmySQLQuery; DsEnti: TDataSource; Telefone: String);
    //
    procedure ReopenInterloctr(Controle: Integer);
    procedure ReopenEC(Controle: Integer);
    procedure ReopenEE(Conta: Integer);
    procedure ReopenET(Conta: Integer);
    procedure ReopenEntiContat(Controle: Integer);
    procedure ReopenEntiMail(Conta: Integer);
    procedure ReopenEntiTel(Conta: Integer);
    procedure ReopenDiarioHist(Codigo: Integer);
    //
    procedure DefineCodigoDeContato(Codigo: Integer);
    procedure SetaInterloctr(Controle: Integer);
    procedure HabilitaEdicaoDeGeneroEDepto();
    procedure FechaOForm();
    //
    procedure InsereConversa(NovaFrase, Encerra: Boolean);
    //
    procedure MostraTabPreAtendimento(SQLType: TSQLType);
    procedure HabilitaBotoes();
  public
    { Public declarations }
    FChamou: TComponent;
    FFmCriou: TForm;
  end;

var
  FmSAC_01_add: TFmSAC_01_add;

implementation

uses
{$IfDef CO_DMKID_APP_24} {$EndIf} UnOSApp_PF,
{$IfNDef NAO_DIAR} DiarioAss, UnDiario_Tabs, {$EndIf}
  Module, ModuleGeral, Principal, UnInternalConsts,
  MyDBCheck, UMySQLModule, (*EntiRapido0,*) UnMyObjects, Entidade2, EntiCargos,
  UnGOTOy, EntiTipCto, EntiContat, EntiMail, EntiTel, CfgCadLista,
  MyGlyfs, SAC_01, UnDmkProcFunc, EntiConEntUnit, UnOSAll_PF, UnDiario_PF;

{$R *.dfm}

const
  FSim = True;
  FNao = False;

procedure TFmSAC_01_add.SbETTelefoneClick(Sender: TObject);
const
  Codigo = 0;
var
  Conta, Controle, EntiTipCto, DiarioAdd: Integer;
  Telefone, Ramal: String;
begin
  Controle := QrECControle.Value;
  Telefone := Geral.SoNumero_TT(EdETTelefone.Text);
  Ramal := EdETRamal.Text;
  EntiTipCto := EdETCFone.ValueVariant;
  DiarioAdd := EdCodigo.ValueVariant;
  //
  Conta := UMyMod.BuscaEmLivreY_Def('entitel', 'Conta', stIns, 0);
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'entitel', False, [
  'Codigo', 'Controle', 'Telefone', 'EntiTipCto', 'Ramal', 'DiarioAdd'], [
  'Conta'], [Codigo, Controle, Telefone, EntiTipCto, Ramal, DiarioAdd], [
  Conta], True) then
  begin
    EdETCFone.ValueVariant := 0;
    CBETCFone.KeyValue := Null;
    EdETTelefone.Text := '';
    EdETRamal.Text := '';
    //
    ReopenEC(Conta);
  end;
end;

procedure TFmSAC_01_add.SbFormContatClick(Sender: TObject);
begin
  Diario_PF.MostraFormContat(QrFormContat.Database, QrFormContatNome.Size,
    EdFormContat, CBFormContat, QrFormContat);
end;

procedure TFmSAC_01_add.SbTerceiro01Click(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMTerceiro01, SbTerceiro01);
end;

procedure TFmSAC_01_add.SbUserActionClick(Sender: TObject);
begin
  if EdDepto.ValueVariant = 0 then
    MostraTabPreAtendimento(stIns)
  else
    MostraTabPreAtendimento(stUpd);
end;

procedure TFmSAC_01_add.SetaInterloctr(Controle: Integer);
begin
  EdInterloctr.ValueVariant := Controle;
  CBInterloctr.KeyValue := Controle;
end;

procedure TFmSAC_01_add.ExcluiContato1Click(Sender: TObject);
begin
  UnEntiConEntUnit.ExcluiContato(QrEntiContat);
end;

procedure TFmSAC_01_add.ExcluiEC1Click(Sender: TObject);
var
  Controle: Integer;
begin
  if UMyMod.ExcluiRegistroInt1('Confirma a exclus�o do contato selecionado?',
  'enticontat', 'Controle', QrECControle.Value, DMod.MyDB) = ID_YES then
  begin
    Controle := GOTOy.LocalizaPriorNextIntQr(QrEC, QrECControle, QrECControle.Value);
    ReopenEC(Controle);
  end;
end;

procedure TFmSAC_01_add.Adicionaraointerlocutor1Click(Sender: TObject);
begin
  if not QrEntiContat.Locate('Controle', EdInterloctr.ValueVariant, []) then
    Geral.MB_Aviso('N�o foi poss�vel localizar o interlocutor!');
  MostraEntiTel(stIns, QrEntiContat, QrEntiTel, DsEntiContat,
    FQrEntidades, FDsEntidades, EdBinaFone.Text);
end;

procedure TFmSAC_01_add.AlteraContato1Click(Sender: TObject);
begin
  MostraEntiContat(stUpd, QrEntiContatQUE_ENT.Value);
end;

procedure TFmSAC_01_add.Alteracontatoatual2Click(Sender: TObject);
begin
  MostraEntiContat(stUpd, 'NI'); // Novo interlocutor
end;

procedure TFmSAC_01_add.Alteraemailselecionado1Click(Sender: TObject);
begin
  MostraEntiMail(stUpd, QrEntiContat, QrEntiMail, DsEntiContat,
    FQrEntidades, FDsEntidades);
end;

procedure TFmSAC_01_add.Alteraemailselecionado2Click(Sender: TObject);
begin
  MostraEntiMail(stUpd, QrEC, QrEE, DsEC, QrEnt0, DsEnt0);
end;

procedure TFmSAC_01_add.Alteratelefoneatual1Click(Sender: TObject);
begin
  MostraEntiTel(stUpd, QrEntiContat, QrEntiTel, DsEntiContat,
    FQrEntidades, FDsEntidades, '');
end;

procedure TFmSAC_01_add.Alteratelefoneselecionado1Click(Sender: TObject);
begin
  MostraEntiTel(stUpd, QrEC, QrET, DsEC, QrEnt0, DsEnt0, '');
end;

procedure TFmSAC_01_add.AtrelaContatoAtual(Entidade: Integer);
var
  Codigo, Controle: Integer;
begin
  Codigo := Entidade;
  Controle := QrEntiContatControle.Value;
  //
  if DModG.ContatoJaAtrelado(Codigo, Controle, True) then
    Exit;
  if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'enticonent', False, [], [
  'Codigo', 'Controle'], [], [
  Codigo, Controle], True) then
    ReopenEntiContat(Controle);
end;

procedure TFmSAC_01_add.AtrelaraoContratante1Click(Sender: TObject);
begin
  AtrelaContatoAtual(EdTerceiro01.ValueVariant);
end;

procedure TFmSAC_01_add.AtrelaraoSubcliente1Click(Sender: TObject);
begin
  AtrelaContatoAtual(EdEntidade.ValueVariant);
end;

procedure TFmSAC_01_add.BtConfirmaClick(Sender: TObject);
var
  CunsGru, CunsSub: Integer;
begin
  CunsGru := EdTerceiro01.ValueVariant;
  CunsSub := EdEntidade.ValueVariant;
  if MyObjects.FIC((QrEC.State <> dsInactive) and (QrEC.RecordCount > 0), nil,
  'Defina a(s) entidade(s) do(s) novo(s) interlocutor(es)') then
     Exit;
  //
  InsereConversa(False, FSim);
  //
  OSAll_PF.AtualizaCadastroCunsIts(CunsGru, CunsSub);
end;

procedure TFmSAC_01_add.BtDesisteClick(Sender: TObject);
begin
  FechaOForm();
end;

procedure TFmSAC_01_add.BtNovaFraseClick(Sender: TObject);
begin
  InsereConversa(True, FNao);
end;

procedure TFmSAC_01_add.BtParcialClick(Sender: TObject);
begin
  InsereConversa(False, FNao);
end;

procedure TFmSAC_01_add.CkHistEntClick(Sender: TObject);
begin
  ReopenDiarioHist(0);
end;

procedure TFmSAC_01_add.Cliente1Click(Sender: TObject);
begin
  DefineCodigoDeContato(EdEntidade.ValueVariant);
end;

procedure TFmSAC_01_add.CompletoJanela1Click(Sender: TObject);
begin
  EditaEntidade(EdEntidade, CBEntidade);
end;

procedure TFmSAC_01_add.CompletoJanela2Click(Sender: TObject);
begin
  EditaEntidade(EdTerceiro01, CBTerceiro01);
end;

procedure TFmSAC_01_add.Contratante1Click(Sender: TObject);
begin
  DefineCodigoDeContato(EdTerceiro01.ValueVariant);
end;

procedure TFmSAC_01_add.ContratanteA1Click(Sender: TObject);
begin
  MostraEntiContat(stIns, 'CO');
end;

procedure TFmSAC_01_add.DBGEntiContatDblClick(Sender: TObject);
begin
  SetaInterloctr(QrEntiContatControle.Value);
end;

procedure TFmSAC_01_add.DBGEntiContatDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
var
  Cor: TColor;
begin
  if (Column.FieldName = 'QUE_ENT') then
  begin
    if      QrEntiContatQUE_ENT.Value = 'SC' then Cor := clRed   // Sub-cliente
    else if QrEntiContatQUE_ENT.Value = 'CO' then Cor := clBlue  // Contratante
    else if QrEntiContatQUE_ENT.Value = Char(169) then Cor := clFuchsia  // Ambos
    else Cor := clGray;
    {if QrEntiContatQUE_ENT.Value = Char(169) then
      Column.Font.Size := 10
    else
      Column.Font.Size := 8;
    }
    MyObjects.DesenhaTextoEmDBGrid(
      TDbGrid(DBGEntiContat), Rect, Cor, clWhite,
      Column.Alignment, Column.Field.DisplayText);
  end;
end;

procedure TFmSAC_01_add.DBGrid1DblClick(Sender: TObject);
begin
  SetaInterloctr(QrECControle.Value);
end;

procedure TFmSAC_01_add.DefineCodigoDeContato(Codigo: Integer);
var
  Controle: Integer;
begin
  if Codigo = 0 then
    Geral.MB_Aviso('A entidade de c�digo "zero" n�o pode receber contatos!')
  else
  begin
    Screen.Cursor := crHourGlass;
    try
      Controle := QrECControle.Value;
      //if
      UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'enticontat', False, [
      'Codigo'], ['Controle'], [Codigo], [Controle], True);// then
      //if
      UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'entimail', False, [
      'Codigo'], ['Controle'], [Codigo], [Controle], True);// then
      //if
      UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'entitel', False, [
      'Codigo'], ['Controle'], [Codigo], [Controle], True);// then
      begin
        DModG.AtualizaEntiConEnt();
        ReopenEC(0);
        ReopenInterloctr(EdInterloctr.ValueVariant);
        ReopenEntiContat(0);
      end;
    finally
      Screen.Cursor := crDefault;
    end;
  end;
end;

procedure TFmSAC_01_add.EdBinaFoneDblClick(Sender: TObject);
begin
  EdETTelefone.Text := EdBinaFone.Text;
end;

procedure TFmSAC_01_add.EdCliIntChange(Sender: TObject);
begin
  ReopenDiarioHist(0);
  HabilitaBotoes();
end;

procedure TFmSAC_01_add.EdCodigoChange(Sender: TObject);
begin
  //GBGerenInterlocutor.Visible := EdCodigo.ValueVariant <> 0;
  GBGerenInterlocutor.Visible := True;
  ReopenInterloctr(0);
end;

procedure TFmSAC_01_add.EdDeptoChange(Sender: TObject);
begin
  HabilitaEdicaoDeGeneroEDepto();
end;

procedure TFmSAC_01_add.EdDeptoKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key = VK_RETURN) and (SbUserAction.Visible)
  and (SbUserAction.Enabled) then
    SbUserActionClick(Self);
end;

procedure TFmSAC_01_add.EdDiarioAssChange(Sender: TObject);
begin
  HabilitaBotoes();
end;

procedure TFmSAC_01_add.EdEntidadeChange(Sender: TObject);
begin
  ReopenInterloctr(0);
  ReopenEntiContat(0);
  ReopenDiarioHist(0);
  HabilitaBotoes();
end;

procedure TFmSAC_01_add.EdEntidadeDblClick(Sender: TObject);
begin
  if EdEntidade.ValueVariant <> 0 then
  begin
    EdTerceiro01.ValueVariant := EdEntidade.ValueVariant;
    CBTerceiro01.KeyValue := EdEntidade.ValueVariant;
  end;
end;

procedure TFmSAC_01_add.EdInterloctrChange(Sender: TObject);
begin
  MyObjects.Informa2(LaInterlctr1A, LaInterlctr1B, False, CBInterloctr.Text);
  ReopenDiarioHist(0);
  HabilitaBotoes();
end;

procedure TFmSAC_01_add.EditaEntidade(Ed: TdmkEditCB;
  CB: TdmkDBLookupComboBox);
const
  Tit = 'FmSAC_01_add.EditaEntidade()';
var
  Ent: Integer;
  Continua: Boolean;
begin
  Continua     := False;
  VAR_CADASTRO := 0;
  Ent := Ed.ValueVariant;
  if DBCheck.CriaFm(TFmEntidade2, FmEntidade2, afmoNegarComAviso) then
  begin
    FmEntidade2.LocCod(Ent, Ent);
    FmEntidade2.ShowModal;
    FmEntidade2.Destroy;
    //
    Continua := True;
  end;
  if Continua then
  begin
    UMyMod.ReabreQuery(QrClientes, Dmod.MyDB, [], Tit);
    UMyMod.ReabreQuery(QrTerceiro01, Dmod.MyDB, [], Tit);
    //
{   Desmarcado a pedido do Eslauco
    if VAR_CADASTRO <> 0 then
    begin
      Ed.ValueVariant := VAR_CADASTRO;
      CB.KeyValue     := VAR_CADASTRO;
    end;
}
  end;
end;

procedure TFmSAC_01_add.EdTerceiro01Change(Sender: TObject);
begin
  ReopenInterloctr(0);
  ReopenEntiContat(0);
  ReopenDiarioHist(0);
  HabilitaBotoes();
end;

procedure TFmSAC_01_add.ExcluiEE1Click(Sender: TObject);
var
  Conta: Integer;
begin
  if UMyMod.ExcluiRegistroInt1('Confirma a exclus�o do e-mail selecionado?',
  'entimail', 'Conta', QrEEConta.Value, Dmod.MyDB) = ID_YES then
  begin
    Conta := GOTOy.LocalizaPriorNextIntQr(QrEE, QrEEConta, QrEEConta.Value);
    ReopenEE(Conta);
  end;
end;

procedure TFmSAC_01_add.Excluiemailselecionado1Click(Sender: TObject);
begin
  DBCheck.QuaisItens_Exclui(Dmod.QrUpd, QrEntimail, DBGEntiMail,
    'entimail', ['Conta'], ['Conta'], istPergunta, '');
end;

procedure TFmSAC_01_add.ExcluiET1Click(Sender: TObject);
var
  Conta: Integer;
begin
  if UMyMod.ExcluiRegistroInt1('Confirma a exclus�o do telefone selecionado?',
  'entitel', 'Conta', QrETConta.Value, Dmod.MyDB) = ID_YES then
  begin
    Conta := GOTOy.LocalizaPriorNextIntQr(QrET, QrETConta, QrETConta.Value);
    ReopenET(Conta);
  end;
end;

procedure TFmSAC_01_add.Excluitelefones1Click(Sender: TObject);
begin
  DBCheck.QuaisItens_Exclui(Dmod.QrUpd, QrEntitel, DBGEntiTel,
    'entitel', ['Conta'], ['Conta'], istPergunta, '');
end;

procedure TFmSAC_01_add.FechaOForm;
begin
{
  if TTabSheet(CsAbaOrigem.Component) <> nil then
  begin
    // se ainda existir
    if TMySQLQuery(CsQry.Component) <> nil then
    begin
      TMySQLQuery(CsQry.Component).Close;
      TMySQLQuery(CsQry.Component).Open;
    end;
    TdmkEditCB(CsEd.Component).ValueVariant := VaCodigo.ValueVariant;
    TdmkDBLookupComboBox(CsCB.Component).KeyValue := VaCodigo.ValueVariant;
  end;
}
  //Close;
  TFmSAC_01(FFmCriou).FecharAba(TTabSheet(Self.Owner));
end;

procedure TFmSAC_01_add.FormCreate(Sender: TObject);
var
  Hoje: TDateTime;
  Agora: TTime;
begin
  FSalvou := False;
  MyObjects.ConfiguraRadioGroup(RGUserGenero, sListaUserGenero, 3, 0);
  ImgTipo.SQLType := stIns;
  //
  DModG.ObtemDataHora(Hoje, Agora);
  TPData.Date := Hoje;
  EdHora.ValueVariant := Agora;
  //
  UMyMod.AbreQuery(QrEnt0, DMod.MyDB);
  //
  UMyMod.AbreQuery(QrClientes, DMod.MyDB);
  UMyMod.AbreQuery(QrTerceiro01, DMod.MyDB);
{$IfNDef NAO_DIAR}
  UMyMod.AbreQuery(QrDiarioAss, DMod.MyDB);
{$EndIf}
  //
  UMyMod.AbreQuery(QrEntiCargos, DMod.MyDB);
  UMyMod.AbreQuery(QrETCFone, DMod.MyDB);
  UMyMod.AbreQuery(QrETCMail, DMod.MyDB);
  UMyMod.AbreQuery(QrFormContat, DMod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrDiarioOpc, Dmod.MyDB);
  // Deixar por �ltimo!
  QrCliInt.Close;
  QrCliInt.Database := Dmod.MyDB;
  DModG.ReopenEmpresas(VAR_USUARIO, 0, nil, nil, '', False, QrCliInt);
  DModG.SelecionaEmpresaSeUnica(EdCliInt, CBCliInt, QrCliInt);
  //
  EdFormContat.ValueVariant := QrDiarioOpc.FieldByName('FrmCPreVda').AsInteger;
  CBFormContat.KeyValue     := QrDiarioOpc.FieldByName('FrmCPreVda').AsInteger;
end;

procedure TFmSAC_01_add.FormShow(Sender: TObject);
begin
  if FmPrincipal.sd1.active then
    FmPrincipal.sd1.SkinForm(Handle);
  //
  // ERRO! Oculta dmkEdits!
  //MyObjects.CorIniComponente(THandle(Sender));
  FmMyGlyfs.DefineGlyfs(TForm(Sender));
  EdCliInt.SetFocus;
end;

procedure TFmSAC_01_add.HabilitaBotoes();
var
  Habilita: Boolean;
begin
  Habilita := (EdCliInt.ValueVariant <> 0) and
              (EdEntidade.ValueVariant <> 0) and
              (*
              (EdTerceiro01.ValueVariant <> 0) and
              (EdInterloctr.ValueVariant <> 0) and
              *)
              (EdDiarioAss.ValueVariant <> 0);
  //
  BtConfirma.Enabled := Habilita;
  BtParcial.Enabled := Habilita;
  BtNovaFrase.Enabled := Habilita and FSalvou;
end;

procedure TFmSAC_01_add.HabilitaEdicaoDeGeneroEDepto();
var
  Genero, Depto: Integer;
begin
  Genero := RGUserGenero.ItemIndex;
  Depto := EdDepto.ValueVariant;
  //
  SbUserAction.Enabled := (Genero > 0) and (Depto = 0); // S� inclus�o por enquanto!
  RGUserGenero.Enabled := Depto = 0;
  EdDepto.ReadOnly := True;  // Sempre! Deve ser setado via aplicativo!
end;

procedure TFmSAC_01_add.HandleWndProc(var Msg: TMessage);
begin
  if Word(Msg.wParam) = 255 then
    TFmSAC_01(FFmCriou).AcoplarFormulario(Owner as TTabSheet)
  else
    inherited;
end;

procedure TFmSAC_01_add.Incluinovotelefone1Click(Sender: TObject);
begin
  MostraEntiTel(stIns, QrEntiContat, QrEntiTel, DsEntiContat,
    FQrEntidades, FDsEntidades, '');
end;

procedure TFmSAC_01_add.InsereConversa(NovaFrase, Encerra: Boolean);
var
  Nome, Data, Hora, BinaFone, BinaDtHr: String;
  N, Codigo, DiarioAss, Entidade, CliInt, Depto, Tipo, Genero, Interloctr,
  Terceiro01, PreAtend, FormContat, Len: Integer;
  Hoje: TDateTime;
  Agora: TTime;
begin
  Codigo         := EdCodigo.ValueVariant;
  Nome           := MeNome.Text;
  DiarioAss      := EdDiarioAss.ValueVariant;
  Entidade       := EdEntidade.ValueVariant;
  CliInt         := EdCliInt.ValueVariant;
  Depto          := EdDepto.ValueVariant;
  Data           := Geral.FDT(TPData.Date, 1);
  Hora           := EdHora.Text;
  Tipo           := VaTipo.ValueVariant;
  Genero         := AppListas.ObtemGeneroDeUserGenero(RGUserGenero.ItemIndex);
  BinaFone       := Geral.SoNumero_TT(EdBinaFone.Text);
  BinaDtHr       := Geral.FDT(TPBinaSoDt.Date, 1) + ' ' + EdBinaSoHr.Text + ':00';
  Interloctr     := EdInterloctr.ValueVariant;
  Terceiro01     := EdTerceiro01.ValueVariant;
  PreAtend       := 1; // Confima pr�-atendimento
  FormContat     := EdFormContat.ValueVariant;
  //
  if MyObjects.FIC(DiarioAss = 0, EdDiarioAss, 'Informe o assunto!') then
    Exit;
  ///
  N := QrDiarioAssAplicacao.Value;
  //if N > 0 then
  //begin
    if Geral.IntInConjunto(CO_DIARIO_ASSUNTO_FLD_NEED_ClienteInterno (*1*), N) then
      if MyObjects.FIC(CliInt = 0, EdCliInt, 'Informe "' + LaCliInt.Caption) then
        Exit;
    if Geral.IntInConjunto(CO_DIARIO_ASSUNTO_FLD_NEED_Entidade       (*2*), N) then
      if MyObjects.FIC(Entidade = 0, EdEntidade, 'Informe "' + LaEntidade.Caption) then
        Exit;
    if Geral.IntInConjunto(CO_DIARIO_ASSUNTO_FLD_NEED_Departamento   (*4*), N) then
      if MyObjects.FIC(Depto = 0, EdDepto, 'Informe "' + LaDepto.Caption) then
        Exit;
    if Geral.IntInConjunto(CO_DIARIO_ASSUNTO_FLD_NEED_Terceiro01     (*8*), N) then
      if MyObjects.FIC(Terceiro01 = 0, EdTerceiro01, 'Informe "' + LaTerceiro01.Caption) then
        Exit;
    if Geral.IntInConjunto(CO_DIARIO_ASSUNTO_FLD_NEED_Interlocutor   (*16*), N) then
      if MyObjects.FIC(Interloctr = 0, EdInterloctr, 'Informe "' + LaInterloctr.Caption) then
        Exit;
  //end;
  //
  if CliInt <> 0 then
    CliInt := QrCliIntCodigo.Value;
  //
  Codigo := UMyMod.BuscaEmLivreY_Def('diarioadd', 'Codigo', ImgTipo.SQLType, Codigo);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'diarioadd', False, [
  'Nome', 'DiarioAss', 'Entidade',
  'CliInt', 'Depto', 'Data',
  'Hora', 'Tipo', 'Genero',
  'BinaFone', 'BinaDtHr', 'Interloctr',
  'Terceiro01', 'PreAtend', 'FormContat'], [
  'Codigo'], [
  Nome, DiarioAss, Entidade,
  CliInt, Depto, Data,
  Hora, Tipo, Genero,
  BinaFone, BinaDtHr, Interloctr,
  Terceiro01, PreAtend, FormContat], [
  Codigo], True) then
  begin
    if Depto = 0 then
    begin
      Len := Length(FItensSemOS);
      SetLength(FItensSemOS, Len + 1);
      FItensSemOS[Len] := Codigo;
    end;
    //
    if Encerra then
      FechaOForm()
    else
    begin
      FSalvou := True;
      ReopenDiarioHist(Codigo);
      if NovaFrase then
      begin
        MeNome.Text := '';
        EdCodigo.ValueVariant := 0;
        ImgTipo.SQLType := stIns;
        //
        DModG.ObtemDataHora(Hoje, Agora);
        TPData.Date := Hoje;
        EdHora.ValueVariant := Agora;
        //
      end else
      begin
        EdCodigo.ValueVariant := Codigo;
        ImgTipo.SQLType       := stUpd;
      end;
      HabilitaBotoes();
    end;
  end;
end;

procedure TFmSAC_01_add.MeNomeChange(Sender: TObject);
begin
  LaLetras.Caption := IntToStr(255 - Length(MeNome.Text)) + '  ';
end;

procedure TFmSAC_01_add.MostraEntiContat(SQLType: TSQLType; Que_Ent: String);
var
  Qry: TmySQLQuery;
begin
  if Que_Ent = 'NI' then
    Qry := QrEC
  else
    Qry := QrEntiContat;
  //
  if UmyMod.FormInsUpd_Cria(TFmEntiContat, FmEntiContat, afmoNegarComAviso,
  Qry, SQLType) then
  begin
    FmEntiContat.FQrEntiContat := Qry;
    if Que_Ent = 'SC' then // Sub-cliente
    begin
      //FmEntiContat.FQrEntidades := QrClientes;
      //FmEntiContat.FDsEntidades := DsClientes;
      //
      FmEntiContat.FEntidadeOri  := QrClientesCodigo.Value;
      FmEntiContat.FEntidadeAtu  := QrClientesCodigo.Value;
    end else
    if Que_Ent = 'CO' then // Contratante
    begin
      //FmEntiContat.FQrEntidades := QrTerceiro01;
      //FmEntiContat.FDsEntidades := DsTerceiro01;
      //
      FmEntiContat.FEntidadeOri  := QrTerceiro01Codigo.Value;
      FmEntiContat.FEntidadeAtu  := QrTerceiro01Codigo.Value;
    end else
    if Que_Ent = 'NI' then  // Novo interlocutor
    begin
      //FmEntiContat.FQrEntidades := QrEnt0;
      //FmEntiContat.FDsEntidades := DsEnt0;
      //
      FmEntiContat.FEntidadeOri  := QrEnt0Codigo.Value;
      FmEntiContat.FEntidadeAtu  := QrEnt0Codigo.Value;
    end;
    //
    //
    FmEntiContat.ShowModal;
    FmEntiContat.Destroy;
    //
    ReopenInterloctr(EdInterloctr.ValueVariant);
  end;
end;

procedure TFmSAC_01_add.MostraEntiMail(SQLType: TSQLType;
QrContat, QrMail: TmySQLQuery; DsContat: TDataSource;
QrEnti: TmySQLQuery; DsEnti: TDataSource);
begin
  if UmyMod.FormInsUpd_Cria(TFmEntiMail, FmEntiMail, afmoNegarComAviso,
  (*QrEntiMail*)QrMail, SQLType) then
  begin
    FmEntiMail.FQrEntiMail := QrMail; //QrEntiMail;
    FmEntiMail.FQrEntidades := QrEnti;//FQrEntidades;
    FmEntiMail.FDsEntidades := DsEnti; //FDsEntidades;
    FmEntiMail.FQrEntiContat  := QrContat; //QrEntiContat;
    FmEntiMail.FDsEntiContat  := DsContat; //DsEntiContat;
    //
    FmEntiMail.DBEdCodigo.DataSource := DsEnti; //FDsEntidades;
    FmEntiMail.DBEdCodUsu.DataSource := DsEnti; //FDsEntidades;
    FmEntiMail.DBEdNome.DataSource := DsEnti; //FDsEntidades;
    //
    FmEntiMail.DBEdContatoCod.DataSource := DsContat; //DsEntiContat;
    FmEntiMail.DBEdContatoNom.DataSource := DsContat; //DsEntiContat;
    FmEntiMail.DBEdContatoCrg.DataSource := DsContat; //DsEntiContat;
    //
    FmEntiMail.ShowModal;
    FmEntiMail.Destroy;
  end;
end;

procedure TFmSAC_01_add.MostraEntiTel(SQLType: TSQLType;
  QrContat, QrTel: TmySQLQuery; DsContat: TDataSource;
  QrEnti: TmySQLQuery; DsEnti: TDataSource; Telefone: String);
begin
  if UmyMod.FormInsUpd_Cria(TFmEntiTel, FmEntiTel, afmoNegarComAviso,
  (*QrEntiTel*)QrTel, SQLType) then
  begin
    FmEntiTel.FQrEntiTel := QrTel; //QrEntiTel;
    FmEntiTel.FQrEntidades := QrEnti; //FQrEntidades;
    FmEntiTel.FDsEntidades := DsEnti; //FDsEntidades;
    FmEntiTel.FQrEntiContat  := QrContat; //QrEntiContat;
    FmEntiTel.FDsEntiContat  := DsContat; //DsEntiContat;
    //
    FmEntiTel.DBEdCodigo.DataSource := DsEnti; //FDsEntidades;
    FmEntiTel.DBEdCodUsu.DataSource := DsEnti; //FDsEntidades;
    FmEntiTel.DBEdNome.DataSource := DsEnti; //FDsEntidades;
    //
    FmEntiTel.DBEdContatoCod.DataSource := DsContat; //DsEntiContat;
    FmEntiTel.DBEdContatoNom.DataSource := DsContat; //DsEntiContat;
    FmEntiTel.DBEdContatoCrg.DataSource := DsContat; //DsEntiContat;
    //
    if SQLType = stIns then
      FmEntiTel.EdTelefone.Text := Geral.SoNumero_TT(Telefone)
    else
    begin
      FmEntiTel.EdConta.ValueVariant := QrTel.FieldByName('Conta').AsInteger;
      FmEntiTel.EdTelefone.Text := Geral.SoNumero_TT(QrTel.FieldByName('Telefone').AsString);
      FmEntiTel.EdRamal.Text := QrTel.FieldByName('Ramal').AsString;
      FmEntiTel.EdEntiTipCto.ValueVariant := QrTel.FieldByName('EntiTipCto').AsInteger;
      FmEntiTel.CBEntiTipCto.KeyValue := QrTel.FieldByName('EntiTipCto').AsInteger;
    end;
    //
    FmEntiTel.ShowModal;
    FmEntiTel.Destroy;
  end;
end;

procedure TFmSAC_01_add.MostraEntiTipCto;
begin
  if DBCheck.CriaFm(TFmEntiTipCto, FmEntiTipCto, afmoNegarComAviso) then
  begin
    FmEntiTipCto.ShowModal;
    FmEntiTipCto.Destroy;
  end;
end;

procedure TFmSAC_01_add.MostraTabPreAtendimento(SQLType: TSQLType);
var
  DtaContat, DtaVisPrv, DtaVisExe, DtaExePrv, DtaExeIni, DtaExeFim: String;
  Codigo, Entidade, SiapTerCad, Estatus, FatoGeradr, (*OSOrigem,*) (*GarantiaDd,
  HrEvacuar, HrExecutar, CondicaoPG,*) DdsPosVda, EntiContat, NumContrat,
  EntPagante, EntContrat, Empresa, Operacao, Grupo, Numero, Opcao, Len, I: Integer;
  ValorTotal: Double;
begin
  if SQLType = stIns then
  begin
    Codigo         := 0;
    Empresa        := EdCliInt.ValueVariant;
    Entidade       := EdEntidade.ValueVariant;
    SiapTerCad     := 0;
    case TdmkAppID(CO_DMKID_APP) of
      dmkappB_U_G_S_T_R_O_L: Estatus := 100(*CO_BUG_STATUS_0100_PRE_ATEN*);
      else
        Estatus        := 0;
    end;
    FatoGeradr     := 0;
    //OSOrigem       := 0;
    DtaContat      := Geral.FDT_TP_Ed(TPData.Date, EdHora.Text);
    DtaVisPrv      := '0000-00-00';
    DtaVisExe      := '0000-00-00';
    DtaExePrv      := '0000-00-00';
    DtaExeIni      := '0000-00-00';
    DtaExeFim      := '0000-00-00';
    (*GarantiaDd     := 0;
    HrEvacuar      := 0;
    HrExecutar     := 0;
    CondicaoPG     := 0;*)
    ValorTotal     := 0;
    DdsPosVda      := 0;
    EntiContat     := EdInterloctr.ValueVariant;
    NumContrat     := 0;
    EntPagante     := 0;
    EntContrat     := EdTerceiro01.ValueVariant;
    Operacao       := 0;
    //
    Grupo := UMyMod.BPGS1I32('oscab', 'Grupo', '', '', tsDef, stIns, 0);
    Numero := Grupo;
    Opcao := 1;
    Codigo := UMyMod.BPGS1I32('oscab', 'Codigo', '', '', tsDef,
      stIns, Codigo);
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'oscab', False, [
    'Empresa',
    'Entidade', 'SiapTerCad', 'Estatus',
    'FatoGeradr', (*'OSOrigem',*) 'DtaContat',
    'DtaVisPrv', 'DtaVisExe', 'DtaExePrv',
    'DtaExeIni', 'DtaExeFim', 'ValorTotal',
    'DdsPosVda', 'EntiContat', 'NumContrat',
    'EntPagante', 'EntContrat', 'Operacao',
    'Grupo', 'Numero', 'Opcao'], [
    'Codigo'], [
    Empresa,
    Entidade, SiapTerCad, Estatus,
    FatoGeradr, (*OSOrigem,*) DtaContat,
    DtaVisPrv, DtaVisExe, DtaExePrv,
    DtaExeIni, DtaExeFim, ValorTotal,
    DdsPosVda, EntiContat, NumContrat,
    EntPagante, EntContrat, Operacao,
    Grupo, Numero, Opcao], [
    Codigo], True) then
    begin
      EdDepto.ValueVariant := Codigo;
      Len := Length(FItensSemOS);
      if Len > 0 then
      begin
        for I := 0 to Len - 1 do
        begin
          UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'diarioadd', False, [
          'Depto'], ['Codigo'], [
          Codigo], [FItensSemOS[I]], True);
        end;
      end;
      if OSApp_PF.TemProvidenciaOrfa(Entidade) then
        OSApp_PF.MostraFormOSPrvSel(stIns, Codigo, nil, Entidade);
      TFmSAC_01(FFmCriou).NovaAba2(naaPreAtend, TTabSheet(Self.Owner), [EdDepto, SbUserAction]);
    end;
  end else
  begin
    TFmSAC_01(FFmCriou).NovaAba2(naaPreAtend, TTabSheet(Self.Owner), [EdDepto, SbUserAction]);
  end;
end;

procedure TFmSAC_01_add.NovoEMail1Click(Sender: TObject);
begin
  MostraEntiMail(stIns, QrEntiContat, QrEntiMail, DsEntiContat,
    FQrEntidades, FDsEntidades);
end;

procedure TFmSAC_01_add.PMBinaFonePopup(Sender: TObject);
begin
  Adicionaraointerlocutor1.Enabled := EdInterloctr.ValueVariant <> 0;
end;

procedure TFmSAC_01_add.PMContatosPopup(Sender: TObject);
{
var
  Habilita: Boolean;
}
begin
  //MyObjects.HabilitaMenuItemItsIns(IncluiContato1, QrCunsImgCab);
  MyObjects.HabilitaMenuItemItsUpd(AlteraContato1, QrEntiContat);
  MyObjects.HabilitaMenuItemCabDelC1I2(ExcluiContato1, QrEntiContat,
    QrEntiTel, QrEntiMail);
  SubClienteA1.Enabled  := EdEntidade.ValueVariant <> 0;
  ContratanteA1.Enabled := EdTerceiro01.ValueVariant <> 0;
  //
  AtrelaraoSubcliente1.Visible  := QrEntiContatQUE_ENT.Value = 'CO';
  AtrelaraoSubcliente1.Enabled  := EdEntidade.ValueVariant <> 0;
  //
  AtrelaraoContratante1.Visible := QrEntiContatQUE_ENT.Value = 'SC';
  AtrelaraoContratante1.Enabled := EdTerceiro01.ValueVariant <> 0;
end;

procedure TFmSAC_01_add.PMECPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemCabDelC1I2(ExcluiEC1, QrEC, QrET, QrEE);
end;

procedure TFmSAC_01_add.PMEEPopup(Sender: TObject);
begin
  //MyObjects.HabilitaMenuItemItsIns(IncluiEE, QrCunsCad);
  MyObjects.HabilitaMenuItemItsDel(ExcluiEE1, QrEE);
end;

procedure TFmSAC_01_add.PMEMailsPopup(Sender: TObject);
var
  Habilita: Boolean;
begin
  Habilita := (QrEntiContat.State <> dsInactive) and (QrEntiContat.RecordCount > 0);
  NovoEMail1.Enabled := Habilita;
  Habilita := (QrEntiMail.State <> dsInactive) and (QrEntiMail.RecordCount > 0);
  Alteraemailselecionado1.Enabled := Habilita;
  Excluiemailselecionado1.Enabled := Habilita;
end;

procedure TFmSAC_01_add.PMETPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsDel(ExcluiET1, QrET);
end;

procedure TFmSAC_01_add.PMTelefonesPopup(Sender: TObject);
var
  Habilita: Boolean;
begin
  Habilita := (QrEntiContat.State <> dsInactive) and (QrEntiContat.RecordCount > 0);
  Incluinovotelefone1.Enabled := Habilita;
  Habilita := (QrEntiTel.State <> dsInactive) and (QrEntiTel.RecordCount > 0);
  Alteratelefoneatual1.Enabled := Habilita;
  Excluitelefones1.Enabled := Habilita;
end;

procedure TFmSAC_01_add.QrClientesAfterScroll(DataSet: TDataSet);
begin
  ReopenEntiContat(QrEntiContatControle.Value);
end;

procedure TFmSAC_01_add.QrClientesBeforeClose(DataSet: TDataSet);
begin
  QrEntiContat.Close;
end;

procedure TFmSAC_01_add.QrECAfterScroll(DataSet: TDataSet);
begin
  ReopenET(0);
  ReopenEE(0);
  //
  PnET.Enabled := True;
  PnEE.Enabled := True;
end;

procedure TFmSAC_01_add.QrECBeforeClose(DataSet: TDataSet);
begin
  QrEE.Close;
  QrET.Close;
  //
  PnET.Enabled := False;
  PnEE.Enabled := False;
end;

procedure TFmSAC_01_add.QrEntiContatAfterScroll(DataSet: TDataSet);
begin
  ReopenEntiMail(0);
  ReopenEntiTel(0);
  if QrEntiContatQUE_ENT.Value = 'SC' then  // Sub-cliente
  begin
    FQrEntidades := QrClientes;
    FDsEntidades := DsClientes;
  end else begin
    FQrEntidades := QrTerceiro01;
    FDsEntidades := DsTerceiro01;
  end;
end;

procedure TFmSAC_01_add.QrEntiContatBeforeClose(DataSet: TDataSet);
begin
  QrEntiMail.Close;
  QrEntiTel.Close;
  FQrEntidades := nil;
  FDsEntidades := nil;
end;

procedure TFmSAC_01_add.QrEntiTelCalcFields(DataSet: TDataSet);
begin
  QrEntiTelTEL_TXT.Value := Geral.FormataTelefone_TT(QrEntiTelTelefone.Value);
end;

procedure TFmSAC_01_add.QrETCalcFields(DataSet: TDataSet);
begin
  QrETTEL_TXT.Value := Geral.FormataTelefone_TT(QrETTelefone.Value);
end;

procedure TFmSAC_01_add.QrTerceiro01AfterScroll(DataSet: TDataSet);
begin
  ReopenEntiContat(QrEntiContatControle.Value);
end;

procedure TFmSAC_01_add.QrTerceiro01BeforeClose(DataSet: TDataSet);
begin
  QrEntiContat.Close;
end;

procedure TFmSAC_01_add.ReopenDiarioHist(Codigo: Integer);
var
  CliInt, Entidade, Terceiro01, Interloctr: Integer;
  TxtE, TxtT, TxtI: String;
begin
  CliInt := EdCliInt.ValueVariant;
  if CliInt <> 0 then
    CliInt := QrCliIntCodigo.Value;
  //
  QrDiarioHist.Close;
  if CliInt <> 0 then
  begin
    Entidade := EdEntidade.ValueVariant;
    Terceiro01 := EdTerceiro01.ValueVariant;
    Interloctr := EdInterloctr.ValueVariant;
    if ((Entidade <> 0) and (CkHistEnt.Checked))
    or ((Terceiro01 <> 0) and (CkHistTer.Checked))
    or ((Interloctr <> 0) and (CkHistInt.Checked)) then
    begin
      if ((Entidade <> 0) and (CkHistEnt.Checked)) then
        TxtE := 'AND Entidade=' + Geral.FF0(Entidade)
      else
        TxtE := '';
      if ((Terceiro01 <> 0) and (CkHistTer.Checked)) then
        TxtT := 'AND Terceiro01=' + Geral.FF0(Terceiro01)
      else
        TxtT := '';
      if ((Interloctr <> 0) and (CkHistInt.Checked)) then
        TxtI := 'AND Interloctr=' + Geral.FF0(Interloctr)
      else
        TxtI := '';
      //
      UnDmkDAC_PF.AbreMySQLQuery0(QrDiarioHist, DMod.MyDB, [
      'SELECT Codigo, Data, Hora, ',
      'Genero, Depto, Nome ',
      'FROM diarioadd ',
      'WHERE CliInt=' + Geral.FF0(CliInt),
      TxtE,
      TxtT,
      TxtI,
      'ORDER BY Data DESC, Hora DESC ',
      '']);
      //
      if Codigo <> 0 then
        QrDiarioHist.Locate('Codigo', Codigo, []);
    end;
  end;
end;

procedure TFmSAC_01_add.ReopenEC(Controle: Integer);
begin
  QrEC.Close;
  if EdCodigo.ValueVariant <> 0 then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrEC, DMod.MyDB, [
    'SELECT eco.Codigo, eco.Sexo, ',
    'eco.Controle, eco.Nome, eco.Cargo, ',
    'eco.DiarioAdd, eco.DtaNatal,',
    'eca.Nome NOME_CARGO, ',
    // NFe 3.10
    'IF(Tipo=0, CNPJ, CPF) CNPJ_CPF, ',
    'eco.Tipo, eco.CNPJ, eco.CPF, eco.Aplicacao ',
    //
    'FROM enticontat eco ',
    'LEFT JOIN enticargos eca ON eca.Codigo=eco.Cargo ',
    'WHERE eco.Codigo=0 ',
    'AND eco.DiarioAdd=' + Geral.FF0(EdCodigo.ValueVariant)]);
    QrEC.Locate('Controle', Controle, []);
  end;
end;

procedure TFmSAC_01_add.ReopenEE(Conta: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrEE, DMod.MyDB, [
  'SELECT etc.Nome NOMEETC, ema.Conta, ',
  'ema.EMail, ema.EntiTipCto, ema.Ordem  ',
  'FROM entimail ema ',
  'LEFT JOIN entitipcto etc ON etc.Codigo=ema.EntiTipCto ',
  'WHERE ema.Controle=' + Geral.FF0(QrECControle.Value),
  'ORDER BY ema.Ordem, ema.Conta ',
  '']);
  QrEE.Locate('Conta', Conta, []);
end;

procedure TFmSAC_01_add.ReopenEntiContat(Controle: Integer);
var
  Cliente, Terceiro01, Localiza: Integer;
begin
  Localiza := 0;
  if Controle <> 0 then
    Localiza := Controle
  else
  if QrEntiContat.State <> dsInactive then
    Localiza := QrEntiContatControle.Value;
  //
  Cliente := EdEntidade.ValueVariant;
  if Cliente = 0 then Cliente := -999999999;
  Terceiro01 := EdTerceiro01.ValueVariant;
  if Terceiro01 = 0 then Terceiro01 := -999999999;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrEntiContat, DMod.MyDB, [
  'SELECT eco.Codigo, eco.Sexo, ',
  'ece.Controle, COUNT(ece.Controle) QTD, eco.Nome, eco.Cargo, ',
  'eco.DiarioAdd, eco.DtaNatal, eca.Nome NOME_CARGO , ',
  'IF(COUNT(ece.Controle)>1, CHAR(169), IF(ece.Codigo=' +
  Geral.FF0(Cliente) + ', "SC", "CO")) QUE_ENT, ',
  // NFe 3.10
  'IF(Tipo=0, CNPJ, CPF) CNPJ_CPF, ',
  'eco.Tipo, eco.CNPJ, eco.CPF, eco.Aplicacao ',
  //
  'FROM enticonent ece',
  'LEFT JOIN enticontat eco ON eco.Controle=ece.Controle',
  'LEFT JOIN enticargos eca ON eca.Codigo=eco.Cargo ',
  'WHERE ece.Codigo=' + Geral.FF0(Cliente),
  'OR ece.Codigo=' + Geral.FF0(Terceiro01),
  'GROUP BY ece.Controle',
{
  'SELECT eco.Controle, COUNT(eco.Controle) QTD, eco.Nome, eco.Cargo, ',
  'eco.DiarioAdd, eco.DtaNatal, eca.Nome NOME_CARGO , ',
  'IF(COUNT(eco.Controle)>1, CHAR(169), IF(eco.Codigo=' +
  Geral.FF0(Cliente) + ', "SC", "CO")) QUE_ENT ',
  'FROM enticonent ece',
  'LEFT JOIN enticontat eco ON eco.Controle=ece.Controle',
  'LEFT JOIN enticargos eca ON eca.Codigo=eco.Cargo ',
  'WHERE eco.Codigo=' + Geral.FF0(Cliente),
  'OR eco.Codigo=' + Geral.FF0(Terceiro01),
  'GROUP BY ece.Controle',
}
  '']);
  //
  if Localiza <> 0 then
    QrEntiContat.Locate('Controle', Localiza, []);
  //  
end;

procedure TFmSAC_01_add.ReopenEntiMail(Conta: Integer);
begin
  QrEntiMail.Close;
  QrEntiMail.Params[0].AsInteger := QrEntiContatControle.Value;
  UMyMod.AbreQuery(QrEntiMail, DMod.MyDB);
  //
  if Conta <> 0 then
    QrEntiMail.Locate('Conta', Conta, []);
end;

procedure TFmSAC_01_add.ReopenEntiTel(Conta: Integer);
begin
  QrEntiTel.Close;
  QrEntiTel.Params[0].AsInteger := QrEntiContatControle.Value;
  UMyMod.AbreQuery(QrEntiTel, DMod.MyDB);
  //
  if Conta <> 0 then
    QrEntiTel.Locate('Conta', Conta, []);
end;

procedure TFmSAC_01_add.ReopenET(Conta: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrET, DMod.MyDB, [
  'SELECT etc.Nome NOMEETC, emt.Conta, ',
  'emt.Telefone, emt.EntiTipCto, emt.Ramal ',
  'FROM entitel emt ',
  'LEFT JOIN entitipcto etc ON etc.Codigo=emt.EntiTipCto ',
  'WHERE emt.Controle=' + Geral.FF0(QrECControle.Value),
  '']);
  QrET.Locate('Conta', Conta, []);
end;

procedure TFmSAC_01_add.ReopenInterloctr(Controle: Integer);
var
  DiarioAdd, Entidade, Terceiro01, Localize: Integer;
  SQL1: String;
begin
  Localize := 0;
  if Controle <> 0 then
    Localize := Controle
  else
  if QrInterloctr.State <> dsInactive then
    Localize := QrInterloctrControle.Value;
  //  
  QrInterloctr.Close;
  DiarioAdd := EdCodigo.ValueVariant;
  Entidade := EdEntidade.ValueVariant;
  Terceiro01 := EdTerceiro01.ValueVariant;
  if (DiarioAdd <> 0) or (Entidade <> 0) or (Terceiro01 <> 0) then
  begin
    SQL1 := '';
// 2013-06-29
    if Entidade <> 0 then
      SQL1 := SQL1 + ' OR ece.Codigo=' + Geral.FF0(Entidade) + sLineBreak;
    if Terceiro01 <> 0 then
      SQL1 := SQL1 + ' OR ece.Codigo=' + Geral.FF0(Terceiro01) + sLineBreak;
    if DiarioAdd <> 0 then
    // 2013-08-26
      //SQL1 := SQL1 + ' OR eco.DiarioAdd=' + Geral.FF0(DiarioAdd) + sLineBreak;
      SQL1 := SQL1 +
      'OR ece.Controle IN (SELECT Controle FROM enticontat WHERE DiarioAdd=' +
      Geral.FF0(DiarioAdd) +')' + sLineBreak;
    // FIM 2013-08-26
    UnDmkDAC_PF.AbreMySQLQuery0(QrInterloctr, Dmod.MyDB, [
(*
    'SELECT Controle, Nome ',
    'FROM enticontat ',
*)
    'SELECT eco.Controle, eco.Nome ',
    'FROM enticontat eco ',
    'LEFT JOIN enticonent ece ON ece.Controle=eco.Controle ',
    'WHERE ece.Codigo=-999999999 ',
    SQL1 +
    'ORDER BY eco.Nome ',
    '']);
// FIM 2013-06-29
    QrInterloctr.Locate('Controle', Localize, []);
  end;
end;

procedure TFmSAC_01_add.RGUserGeneroClick(Sender: TObject);
begin
  HabilitaEdicaoDeGeneroEDepto();
{
  if RGUserGenero.ItemIndex > 0
  begin
    /
  end;
}
end;

procedure TFmSAC_01_add.Rpido1Click(Sender: TObject);
begin
  TFmSAC_01(FFmCriou).NovaAba2(naaEntiRapido, TTabSheet(Self.Owner), [QrClientes,
    EdEntidade, CBEntidade]);
end;

procedure TFmSAC_01_add.Rpido2Click(Sender: TObject);
begin
  TFmSAC_01(FFmCriou).NovaAba2(naaEntiRapido, TTabSheet(Self.Owner), [QrTerceiro01,
    EdTerceiro01, CBTerceiro01]);
end;

procedure TFmSAC_01_add.SbETCEMailClick(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  MostraEntiTipCto();
  if VAR_CADASTRO > 0 then
  begin
    QrETCMail.Close;
    UMyMod.AbreQuery(QrETCMail, DMod.MyDB);
    //
    EdETCMail.ValueVariant := VAR_CADASTRO;
    CBETCMail.KeyValue     := VAR_CADASTRO;
  end;
end;

procedure TFmSAC_01_add.SpeedButton1Click(Sender: TObject);
begin
{$IfNDef NAO_DIAR}
  VAR_CADASTRO := 0;
  if DBCheck.CriaFm(TFmDiarioAss, FmDiarioAss, afmoNegarComAviso) then
  begin
    FmDiarioAss.ShowModal;
    FmDiarioAss.Destroy;
    //
    if VAR_CADASTRO <> 0 then
      UMyMod.SetaCodUsuDeCodigo(EdDiarioAss, CBDiarioAss, QrDiarioAss,
      VAR_CADASTRO, 'Codigo', 'CodUsu');
  end;
{$Else}
  dmkPF.InfoSemModulo(mdlappDiario);
 {$EndIf}
end;

procedure TFmSAC_01_add.SubClienteA1Click(Sender: TObject);
begin
  MostraEntiContat(stIns, 'SC');
end;

procedure TFmSAC_01_add.SbEntidadeClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMEntidades, SbEntidade);
end;

procedure TFmSAC_01_add.SbEntiContat(Sender: TObject);
{
var
  Entidade, Destino: Integer;
}
begin
{ N�o precisa por enquanto!
  if DBCheck.CriaFm(TFmEntiRapido0, FmEntiRapido0, afmoNegarComAviso) then
  begin
    FmEntiRapido0.ShowModal;
    Entidade := FmEntiRapido0.FCodigo;
    FmEntiRapido0.Destroy;
    //
    if Entidade <> 0 then
    begin
      QrClientes.Close;
      UMyMod.AbreQuery(QrClientes, DMod.MyDB);
      //
      QrTerceiro01.Close;
      UMyMod.AbreQuery(QrTerceiro01, DMod.MyDB);
      //
      Destino := MyObjects.SelRadioGroup('Uso do cadastro rec�m feito',
      'Defina como deseja usar o cadastro rec�m feito',
      ['N�o quero usar', LaEntidade1.Caption, LaEntidade2.Caption], 1);
      case Destino of
        0: ; // nada
        1:
        begin
          EdEntidade.ValueVariant := Entidade;
          CBEntidade.KeyValue     := Entidade;
        end;
        2:
        begin
          EdTerceiro01.ValueVariant := Entidade;
          CBTerceiro01.KeyValue     := Entidade;
        end;
        else
          Geral.MB_Aviso('Uso n�o implementado. Solicite � DERMATEK!');
      end;
    end;
  end;
}
end;

procedure TFmSAC_01_add.SbECCargoClick(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  if DBCheck.CriaFm(TFmEntiCargos, FmEntiCargos, afmoNegarComAviso) then
  begin
    FmEntiCargos.ShowModal;
    FmEntiCargos.Destroy;
  end;
  if VAR_CADASTRO <> 0 then
  begin
    QrEntiCargos.Close;
    UMyMod.AbreQuery(QrEntiCargos, DMod.MyDB);
    EdECCargo.ValueVariant := VAR_CADASTRO;
    CBECCargo.KeyValue     := VAR_CADASTRO;
  end;
end;

procedure TFmSAC_01_add.SbEEEMailClick(Sender: TObject);
const
  Codigo = 0;
var
  Conta, Controle, EntiTipCto, DiarioAdd: Integer;
  EMail: String;
begin
  Controle := QrECControle.Value;
  EMail := EdEEEmail.Text;
  EntiTipCto := EdETCMail.ValueVariant;
  DiarioAdd := EdCodigo.ValueVariant;
  //
  Conta := UMyMod.BuscaEmLivreY_Def('entimail', 'Conta', stIns, 0);
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'entimail', False, [
  'Codigo', 'Controle', 'EMail', 'EntiTipCto', 'DiarioAdd'], [
  'Conta'], [
  Codigo, Controle, EMail, EntiTipCto, DiarioAdd], [
  Conta], True) then
  begin
    EdETCMail.ValueVariant := 0;
    CBETCMail.KeyValue := Null;
    EdEEEmail.Text := '';
    //
    ReopenEE(Conta);
  end;
end;

procedure TFmSAC_01_add.SbETCFoneClick(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  MostraEntiTipCto();
  if VAR_CADASTRO > 0 then
  begin
    QrETCFone.Close;
    UMyMod.AbreQuery(QrETCFone, DMod.MyDB);
    //
    EdETCFone.ValueVariant := VAR_CADASTRO;
    CBETCFone.KeyValue     := VAR_CADASTRO;
  end;
end;

procedure TFmSAC_01_add.SbETNomeClick(Sender: TObject);
const
  Codigo = 0;
var
  Nome: String;
  Controle, Cargo, DiarioAdd: Integer;
begin
  Nome := EdETNome.Text;
  if MyObjects.FIC(Trim(Nome) = '', EdETNome, 'Informe o nome do contato!') then
   Exit;
  //
  Cargo := EdECCargo.ValueVariant;
  DiarioAdd := EdCodigo.ValueVariant;
  //
  Controle := UMyMod.BuscaEmLivreY_Def('enticontat', 'Controle', stIns, 0);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'enticontat', False, [
  'Codigo', 'Nome', 'Cargo', 'DiarioAdd'], ['Controle'], [
  Codigo, Nome, Cargo, DiarioAdd], [Controle], True) then
  begin
    DModG.AtualizaEntiConEnt();
    EdECCargo.ValueVariant := 0;
    CBECCargo.KeyValue := Null;
    EdETNome.Text := '';
    //
    ReopenEC(Controle);
    //
    ReopenInterloctr(EdInterloctr.ValueVariant);
  end;
end;

end.
