unit SAC_01_ER0;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnInternalConsts,
  dmkImage, Mask, Menus, dmkCheckBox, dmkVariable, dmkCompoStore, Variants,
  UnDmkEnums, dmkDBGridZTO;

type
  TFmSAC_01_ER0 = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_M: TGroupBox;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    QrLoc: TmySQLQuery;
    QrLocCNPJ_CPF: TWideStringField;
    PnDados2: TPanel;
    Label97: TLabel;
    Label31: TLabel;
    Label32: TLabel;
    Label34: TLabel;
    Label35: TLabel;
    Label38: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    Label106: TLabel;
    Label109: TLabel;
    Label112: TLabel;
    CBLograd: TdmkDBLookupComboBox;
    EdRua: TdmkEdit;
    EdNumero: TdmkEdit;
    EdBairro: TdmkEdit;
    EdCidade: TdmkEdit;
    EdUF: TdmkEdit;
    EdPais: TdmkEdit;
    EdLograd: TdmkEditCB;
    EdCompl: TdmkEdit;
    EdEndeRef: TdmkEdit;
    EdCodMunici: TdmkEditCB;
    CBCodMunici: TdmkDBLookupComboBox;
    EdCodiPais: TdmkEditCB;
    CBCodiPais: TdmkDBLookupComboBox;
    EdTe1: TdmkEdit;
    PMCEP: TPopupMenu;
    Descobrir1: TMenuItem;
    Verificar1: TMenuItem;
    Internet1: TMenuItem;
    QrDuplic2: TmySQLQuery;
    QrDuplic2Codigo: TIntegerField;
    QrListaLograd: TmySQLQuery;
    QrListaLogradCodigo: TIntegerField;
    QrListaLogradNome: TWideStringField;
    DsListaLograd: TDataSource;
    QrMunici: TmySQLQuery;
    QrMuniciCodigo: TIntegerField;
    QrMuniciNome: TWideStringField;
    DsMunici: TDataSource;
    QrBacen_Pais: TmySQLQuery;
    QrBacen_PaisCodigo: TIntegerField;
    QrBacen_PaisNome: TWideStringField;
    DsBacen_Pais: TDataSource;
    QrHowFounded: TmySQLQuery;
    QrHowFoundedCodigo: TIntegerField;
    QrHowFoundedNome: TWideStringField;
    DsHowFounded: TDataSource;
    QrAccounts: TmySQLQuery;
    QrAccountsCodigo: TIntegerField;
    QrAccountsNome: TWideStringField;
    DsAccounts: TDataSource;
    QrAtividades: TmySQLQuery;
    QrAtividadesCodigo: TIntegerField;
    QrAtividadesNome: TWideStringField;
    DsAtividades: TDataSource;
    PnDados1: TPanel;
    Label25: TLabel;
    Label30: TLabel;
    Label29: TLabel;
    Label12: TLabel;
    EdCEP: TdmkEdit;
    EdRazaoNome: TdmkEdit;
    EdCNPJCPF: TdmkEdit;
    EdIERG: TdmkEdit;
    RGTipo: TRadioGroup;
    PnDados3: TPanel;
    Panel6: TPanel;
    GBCunsCad: TGroupBox;
    Label24: TLabel;
    Label1: TLabel;
    Label26: TLabel;
    Label27: TLabel;
    SpeedButton5: TSpeedButton;
    SpeedButton6: TSpeedButton;
    SpeedButton7: TSpeedButton;
    EdAtivPrinc: TdmkEditCB;
    CBAtivPrinc: TdmkDBLookupComboBox;
    EdAccount: TdmkEditCB;
    CBAccount: TdmkDBLookupComboBox;
    EdHowFind: TdmkEditCB;
    CBHowFind: TdmkDBLookupComboBox;
    TPDataCon: TdmkEditDateTimePicker;
    GroupBox4: TGroupBox;
    CkCliente1_0: TdmkCheckBox;
    CkFornece1_0: TdmkCheckBox;
    CkFornece2_0: TdmkCheckBox;
    CkFornece3_0: TdmkCheckBox;
    CkFornece4_0: TdmkCheckBox;
    CkTerceiro_0: TdmkCheckBox;
    CkCliente2_0: TdmkCheckBox;
    CkFornece5_0: TdmkCheckBox;
    CkFornece6_0: TdmkCheckBox;
    CkCliente4_0: TdmkCheckBox;
    CkCliente3_0: TdmkCheckBox;
    CkFornece8_0: TdmkCheckBox;
    CkFornece7_0: TdmkCheckBox;
    CsAbaOrigem: TdmkCompoStore;
    VaCodigo: TdmkVariable;
    CsQry: TdmkCompoStore;
    CsEd: TdmkCompoStore;
    CsCB: TdmkCompoStore;
    CBRE: TComboBox;
    Label141: TLabel;
    Panel5: TPanel;
    Panel7: TPanel;
    RGPesquisa: TRadioGroup;
    Label2: TLabel;
    EdPsqRua: TdmkEdit;
    Label3: TLabel;
    EdPsqNumero: TdmkEdit;
    Label4: TLabel;
    EdPsqCidade: TdmkEditCB;
    CBPsqCidade: TdmkDBLookupComboBox;
    EdPsqUF: TdmkEdit;
    Label5: TLabel;
    dmkDBGridZTO1: TdmkDBGridZTO;
    QrPesq: TmySQLQuery;
    DsPsq: TDataSource;
    QrPesqCodigo: TIntegerField;
    QrPesqNome: TWideStringField;
    QrPesqCliente: TIntegerField;
    QrPesqSLograd: TSmallintField;
    QrPesqSRua: TWideStringField;
    QrPesqSNumero: TIntegerField;
    QrPesqSCompl: TWideStringField;
    QrPesqSBairro: TWideStringField;
    QrPesqSCidade: TWideStringField;
    QrPesqSUF: TWideStringField;
    QrPesqSCEP: TIntegerField;
    QrPesqSPais: TWideStringField;
    QrPesqSEndeRef: TWideStringField;
    QrPesqSCodMunici: TIntegerField;
    QrPesqSCodiPais: TIntegerField;
    QrPesqSTe1: TWideStringField;
    QrPesqM2Constru: TFloatField;
    QrPesqM2NaoBuild: TFloatField;
    QrPesqM2Terreno: TFloatField;
    QrPesqM2Total: TFloatField;
    QrPesqLk: TIntegerField;
    QrPesqDataCad: TDateField;
    QrPesqDataAlt: TDateField;
    QrPesqUserCad: TIntegerField;
    QrPesqUserAlt: TIntegerField;
    QrPesqAlterWeb: TSmallintField;
    QrPesqAtivo: TSmallintField;
    QrPesqLstCusPrd: TIntegerField;
    QrPesqPdrMntsMon: TIntegerField;
    QrPesqLatitude: TFloatField;
    QrPesqLongitude: TFloatField;
    QrPesqRotaLatLon: TIntegerField;
    BtPesquisa: TBitBtn;
    QrPesqNO_ENT: TWideStringField;
    StaticText1: TStaticText;
    QrEmpresa: TmySQLQuery;
    SbRF: TSpeedButton;
    SbCEP: TSpeedButton;
    BtSintregra: TBitBtn;
    PnConfirma: TPanel;
    BtOK: TBitBtn;
    Label6: TLabel;
    EdFantasiaApelido: TdmkEdit;
    Label148: TLabel;
    EdTe1Tip: TdmkEditCB;
    CBTe1Tip: TdmkDBLookupComboBox;
    Label111: TLabel;
    EdEmail: TdmkEdit;
    QrTe1Tip: TmySQLQuery;
    IntegerField4: TIntegerField;
    IntegerField5: TIntegerField;
    StringField3: TWideStringField;
    DsTe1Tip: TDataSource;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure EdCEPExit(Sender: TObject);
    procedure EdCEPEnter(Sender: TObject);
    procedure EdCNPJCPFExit(Sender: TObject);
    procedure Descobrir1Click(Sender: TObject);
    procedure Verificar1Click(Sender: TObject);
    procedure Internet1Click(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure RGTipoClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure EdRazaoNomeChange(Sender: TObject);
    procedure SpeedButton7Click(Sender: TObject);
    procedure SpeedButton5Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure SpeedButton6Click(Sender: TObject);
    procedure BtPesquisaClick(Sender: TObject);
    procedure EdPsqRuaChange(Sender: TObject);
    procedure EdPsqNumeroChange(Sender: TObject);
    procedure EdPsqCidadeChange(Sender: TObject);
    procedure EdPsqUFChange(Sender: TObject);
    procedure EdRuaChange(Sender: TObject);
    procedure EdNumeroChange(Sender: TObject);
    procedure EdCodMuniciChange(Sender: TObject);
    procedure CBCodMuniciClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure BtSintregraClick(Sender: TObject);
    procedure SbRFClick(Sender: TObject);
    procedure SbCEPClick(Sender: TObject);
    procedure dmkDBGridZTO1DblClick(Sender: TObject);
  private
    { Private declarations }
    FNaoPesquisa: Boolean;
    FCEP: String;
    //
    procedure HandleWndProc(var Msg: TMessage); message WM_SYSCOMMAND;
    //
    function  CNPJCPFDuplicado(): Boolean;
    function  UpdateCunsCad(Codigo: Integer): Boolean;
    procedure FecharOForm(Confirma: Boolean; Codigo: Integer);
    procedure PesquisaSINTEGRA();
    procedure ReopenPesqEndereco(Manual: Boolean = False);
    procedure ConfiguraDadosPadrao(Configura: Boolean);
  public
    { Public declarations }
    FFmCriou: TForm;
  end;

  var
  FmSAC_01_ER0: TFmSAC_01_ER0;

implementation

uses SAC_01, UnMyObjects, Module, UMySQLModule, EntiCEP, MyDBCheck, UnCEP,
  UnEntities, CfgCadLista, Principal, ModuleGeral, UnConsultasWeb, MyGlyfs,
  DmkDAC_PF, UnOSAll_PF, UnDmkProcFunc;

{$R *.DFM}

procedure TFmSAC_01_ER0.BtSintregraClick(Sender: TObject);
begin
  PesquisaSINTEGRA();
end;

procedure TFmSAC_01_ER0.BtOKClick(Sender: TObject);
var
  Codigo, CodUsu, Tipo, ECEP, ELograd, ENumero, EUF, ECodMunici, ECodiPais,
  PCEP, PLograd, PNumero, PUF, PCodMunici, PCodiPais, MaiorCodigo,
  indIEDest, ETe1Tip, PTe1Tip: Integer;
  //
  Cliente1, Cliente2, Cliente3, Cliente4, Fornece1, Fornece2, Fornece3, Fornece4,
  Fornece5, Fornece6, Fornece7, Fornece8, Terceiro, RazaoSocial, CNPJ, IE, ERua,
  ECompl, EBairro, ECidade, EPais, ETe1, EEndeRef, Nome, CPF, RG, PRua, PCompl,
  PBairro, PCidade, PPais, PTe1, PEndeRef, Fantasia, Apelido, EEmail,
  PEmail: String;
begin
  if CNPJCPFDuplicado() then
    Exit;
  Tipo := RGTipo.ItemIndex;
  //
  if Tipo = 0 then
    if Geral.Valida_IE(EdIERG.Text, EdUF.Text, '??', False) = False then
    begin
      if Geral.MB_Pergunta(
      'Deseja continuar mesmo com d�vidas na valida��o da Inscri��o Estadual?'
      ) <> ID_YES then
    Exit;
  end;
  //
  if not Entities.TipoDeCadastroDefinido(CkCliente1_0, CkCliente2_0,
  CkCliente3_0, CkCliente4_0, CkFornece1_0, CkFornece2_0, CkFornece3_0,
  CkFornece4_0, CkFornece5_0, CkFornece6_0, CkFornece7_0, CkFornece8_0,
  CkTerceiro_0, nil) then
    Exit;
  RazaoSocial  := '';
  Fantasia     := '';
  CNPJ         := '';
  IE           := '';
  ECEP         := 0;
  ELograd      := 0;
  ERua         := '';
  ENumero      := 0;
  ECompl       := '';
  EBairro      := '';
  ECidade      := '';
  EUF          := 0;
  EPais        := '';
  ETe1Tip      := 0;
  ETe1         := '';
  EEmail       := '';
  EEndeRef     := '';
  ECodMunici   := 0;
  ECodiPais    := 0;
  //
  Nome         := '';
  Apelido      := '';
  CPF          := '';
  RG           := '';
  PCEP         := 0;
  PLograd      := 0;
  PRua         := '';
  PNumero      := 0;
  PCompl       := '';
  PBairro      := '';
  PCidade      := '';
  PUF          := 0;
  PPais        := '';
  PTe1Tip      := 0;
  PTe1         := '';
  PEmail       := '';
  PEndeRef     := '';
  PCodMunici   := 0;
  PCodiPais    := 0;
  //
  case Tipo of
    0:
    begin
      RazaoSocial  := EdRazaoNome.Text;
      Fantasia     := EdFantasiaApelido.Text;
      CNPJ         := Geral.SoNumero_TT(EdCNPJCPF.Text);
      IE           := Geral.SoNumero_TT(EdIERG.Text);
      //
      ECEP         := Geral.IMV(Geral.SoNumero_TT(EdCEP.ValueVariant));
      ELograd      := EdLograd.ValueVariant;
      ERua         := EdRua.Text;
      ENumero      := Geral.IMV(Geral.SoNumero_TT(EdNumero.Text));
      ECompl       := EdCompl.Text;
      EBairro      := EdBairro.Text;
      ECidade      := EdCidade.Text;
      EUF          := Geral.GetCodigoUF_da_SiglaUF(EdUF.Text);
      EPais        := EdPais.Text;
      ETe1Tip      := EdTe1Tip.ValueVariant;
      ETe1         := Geral.SoNumero_TT(EdTe1.Text);
      EEmail       := EdEmail.ValueVariant;
      EEndeRef     := EdEndeRef.Text;
      ECodMunici   := EdCodMunici.ValueVariant;
      ECodiPais    := EdCodiPais.ValueVariant;
    end;
    1:
    begin
      Nome         := EdRazaoNome.Text;
      Apelido      := EdFantasiaApelido.Text;
      CPF          := Geral.SoNumero_TT(EdCNPJCPF.Text);
      RG           := Geral.SoNumero_TT(EdIERG.Text);
      //
      PCEP         := Geral.IMV(Geral.SoNumero_TT(EdCEP.Text));
      PLograd      := EdLograd.ValueVariant;
      PRua         := EdRua.Text;
      PNumero      := Geral.IMV(Geral.SoNumero_TT(EdNumero.Text));
      PCompl       := EdCompl.Text;
      PBairro      := EdBairro.Text;
      PCidade      := EdCidade.Text;
      PUF          := Geral.GetCodigoUF_da_SiglaUF(EdUF.Text);
      PPais        := EdPais.Text;
      PTe1Tip      := EdTe1Tip.ValueVariant;
      PTe1         := Geral.SoNumero_TT(EdTe1.Text);
      PEmail       := EdEmail.ValueVariant;
      PEndeRef     := EdEndeRef.Text;
      PCodMunici   := EdCodMunici.ValueVariant;
      PCodiPais    := EdCodiPais.ValueVariant;
    end;
    else
      Exit;
  end;
  if CkCliente1_0.Checked then Cliente1 := 'V' else Cliente1 := 'F';
  if CkCliente2_0.Checked then Cliente2 := 'V' else Cliente2 := 'F';
  if CkCliente3_0.Checked then Cliente3 := 'V' else Cliente3 := 'F';
  if CkCliente4_0.Checked then Cliente4 := 'V' else Cliente4 := 'F';
  if CkFornece1_0.Checked then Fornece1 := 'V' else Fornece1 := 'F';
  if CkFornece2_0.Checked then Fornece2 := 'V' else Fornece2 := 'F';
  if CkFornece3_0.Checked then Fornece3 := 'V' else Fornece3 := 'F';
  if CkFornece4_0.Checked then Fornece4 := 'V' else Fornece4 := 'F';
  if CkFornece5_0.Checked then Fornece5 := 'V' else Fornece5 := 'F';
  if CkFornece6_0.Checked then Fornece6 := 'V' else Fornece6 := 'F';
  if CkFornece7_0.Checked then Fornece7 := 'V' else Fornece7 := 'F';
  if CkFornece8_0.Checked then Fornece8 := 'V' else Fornece8 := 'F';
  if CkTerceiro_0.Checked then Terceiro := 'V' else Terceiro := 'F';
  //
  Codigo    := UMyMod.BuscaEmLivreY_Def('Entidades', 'Codigo', ImgTipo.SQLType,
                 VaCodigo.ValueVariant);
  CodUsu    := Codigo;
  //
  if Tipo = 0 then //Juridica
    indIEDest := Entities.ObtemindIEDestAuto(IE, EUF, Tipo)
  else
    indIEDest := Entities.ObtemindIEDestAuto('', PUF, Tipo);
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'entidades', False, [
  'Cliente1', 'Cliente2', 'Cliente3', 'Cliente4',
  'Fornece1', 'Fornece2', 'Fornece3', 'Fornece4',
  'Fornece5', 'Fornece6', 'Fornece7', 'Fornece8',
  'Terceiro', 'Tipo', 'indIEDest',
  'RazaoSocial', 'Fantasia', 'CNPJ', 'IE', 'ECEP', 'ELograd', 'ERua',
  'ENumero', 'ECompl', 'EBairro', 'ECidade', 'EUF',
  'EPais', 'ETe1Tip', 'ETe1', 'EEmail', 'EEndeRef', 'ECodMunici', 'ECodiPais',
  'Nome', 'Apelido', 'CPF', 'RG', 'PCEP', 'PLograd', 'PRua',
  'PNumero', 'PCompl', 'PBairro', 'PCidade', 'PUF',
  'PPais', 'PTe1Tip', 'PTe1', 'PEmail', 'PEndeRef', 'PCodMunici', 'PCodiPais'
  ], ['Codigo', 'CodUsu'
  ], [
  Cliente1, Cliente2, Cliente3, Cliente4,
  Fornece1, Fornece2, Fornece3, Fornece4,
  Fornece5, Fornece6, Fornece7, Fornece8,
  Terceiro, Tipo, indIEDest,
  RazaoSocial, Fantasia, CNPJ, IE, ECEP, ELograd, ERua,
  ENumero, ECompl, EBairro, ECidade, EUF,
  EPais, ETe1Tip, ETe1, EEmail, EEndeRef, ECodMunici, ECodiPais,
  Nome, Apelido, CPF, RG, PCEP, PLograd, PRua,
  PNumero, PCompl, PBairro, PCidade, PUF,
  PPais, PTe1Tip, PTe1, PEmail, PEndeRef, PCodMunici, PCodiPais
  ], [Codigo, CodUsu
  ], True) then
  begin
    VaCodigo.ValueVariant := Codigo;
    OSAll_PF.AchouNovosClientes(MaiorCodigo);
    UpdateCunsCad(Codigo);
    FecharOForm(True, Codigo);
  end;
end;

procedure TFmSAC_01_ER0.BtPesquisaClick(Sender: TObject);
begin
  ReopenPesqEndereco(True);
end;

procedure TFmSAC_01_ER0.BtSaidaClick(Sender: TObject);
begin
  FecharOForm(False, 0);
end;

procedure TFmSAC_01_ER0.CBCodMuniciClick(Sender: TObject);
begin
  if CBCodMunici.Focused then
    EdPsqCidade.Text := EdCodMunici.Text;
end;

function TFmSAC_01_ER0.CNPJCPFDuplicado(): Boolean;
var
  CNPJCPF: String;
  //Tipo: Integer;
begin
  Result := False;
  CNPJCPF := Geral.SoNumero_TT(EdCNPJCPF.Text);
  if Length(CNPJCPF) > 0 then
  begin
    QrDuplic2.Close;
    QrDuplic2.SQL.Clear;
    {
    if Length(CNPJCPF) < 14 then
      Tipo := 1
    else
      Tipo := 0;
    //
    if Tipo = 0 then
    begin
    }
      QrDuplic2.SQL.Add('SELECT Codigo FROM entidades');
      QrDuplic2.SQL.Add('WHERE CPF="' + CNPJCPF + '"');
      QrDuplic2.SQL.Add('OR CNPJ="' + CNPJCPF + '"');
      //QrDuplic2.SQL.Add('AND Codigo <> '+EdCodigo.Text);
    {
    end;
    if Tipo = 1 then
    begin
      QrDuplic2.SQL.Add('SELECT Codigo FROM entidades');
      QrDuplic2.SQL.Add('WHERE CPF="' + CNPJCPF + '"');
      //QrDuplic2.SQL.Add('AND Codigo <> '+EdCodigo.Text);
    end;
    }
    QrDuplic2.Open;
    if QrDuplic2.RecordCount > 0 then
    begin
      Result := True;
      Geral.MB_Aviso('Esta entidade j� foi cadastrada com o c�digo n� '+
      IntToStr(QrDuplic2Codigo.Value)+'.');
      QrDuplic2.Close;
      Screen.Cursor := crDefault;
      Exit;
    end;
    QrDuplic2.Close;
  end;
end;

procedure TFmSAC_01_ER0.ConfiguraDadosPadrao(Configura: Boolean);
begin
  if Configura then
  begin
    DModG.ReopenEmpresas(VAR_USUARIO, VAR_LIB_EMPRESA_SEL, nil, nil, '', False, QrEmpresa);
    //
    CBRE.Text := Geral.GetSiglaUF_do_CodigoUF(QrEmpresa.FieldByName('UF').AsInteger);
    //
    EdCodMunici.ValueVariant := QrEmpresa.FieldByName('CodMunici').AsInteger;
    CBCodMunici.KeyValue     := QrEmpresa.FieldByName('CodMunici').AsInteger;
    EdUF.ValueVariant        := QrEmpresa.FieldByName('UF').AsInteger;
    EdCodiPais.ValueVariant  := QrEmpresa.FieldByName('CodiPais').AsInteger;
    CBCodiPais.KeyValue      := QrEmpresa.FieldByName('CodiPais').AsInteger;
    //
    EdPsqCidade.ValueVariant := QrEmpresa.FieldByName('CodMunici').AsInteger;
    CBPsqCidade.KeyValue     := QrEmpresa.FieldByName('CodMunici').AsInteger;
    EdPsqUF.ValueVariant     := QrEmpresa.FieldByName('UF').AsInteger;
  end else
  begin
    CBRE.Text := '';
    //
    EdCodMunici.ValueVariant := 0;
    CBCodMunici.KeyValue     := Null;
    EdUF.ValueVariant        := 0;
    EdCodiPais.ValueVariant  := 0;
    CBCodiPais.KeyValue      := Null;
    //
    EdPsqCidade.ValueVariant := 0;
    CBPsqCidade.KeyValue     := Null;
    EdPsqUF.ValueVariant     := 0;
  end;
  FNaoPesquisa := False;
end;

procedure TFmSAC_01_ER0.Descobrir1Click(Sender: TObject);
begin
  U_CEP.DescobrirCEP(EdCEP, EdLograd, EdRua, EdBairro, EdCidade, EdUF,
  EdPais, EdCodMunici, EdCodiPais, CBLograd, CBCodMunici,
  CBCodiPais);
end;

procedure TFmSAC_01_ER0.dmkDBGridZTO1DblClick(Sender: TObject);
var
  Entidade: Integer;
begin
  if (QrPesq.State <> dsInactive) and (QrPesq.RecordCount > 0) then
  begin
    Entidade              := QrPesqCliente.Value;
    VaCodigo.ValueVariant := Entidade;
    //
    FecharOForm(True, Entidade);
  end;
end;

procedure TFmSAC_01_ER0.EdCNPJCPFExit(Sender: TObject);
begin
  CNPJCPFDuplicado();
end;

procedure TFmSAC_01_ER0.EdCodMuniciChange(Sender: TObject);
begin
  if EdCodMunici.Focused then
    EdPsqCidade.Text := EdCodMunici.Text;
end;

procedure TFmSAC_01_ER0.EdNumeroChange(Sender: TObject);
begin
  if EdNumero.Focused then
    EdPsqNumero.Text := EdNumero.Text;
end;

procedure TFmSAC_01_ER0.EdPsqCidadeChange(Sender: TObject);
begin
  ReopenPesqEndereco();
end;

procedure TFmSAC_01_ER0.EdPsqNumeroChange(Sender: TObject);
begin
  ReopenPesqEndereco();
end;

procedure TFmSAC_01_ER0.EdPsqRuaChange(Sender: TObject);
begin
  if Length(Trim(EdPsqRua.ValueVariant)) > 0 then
    ReopenPesqEndereco();
end;

procedure TFmSAC_01_ER0.EdPsqUFChange(Sender: TObject);
begin
  ReopenPesqEndereco();
end;

procedure TFmSAC_01_ER0.EdRazaoNomeChange(Sender: TObject);
begin
  RGTipoClick(Self);
end;

procedure TFmSAC_01_ER0.EdRuaChange(Sender: TObject);
begin
  if EdRua.Focused then
    EdPsqRua.Text := EdRua.Text;
end;

procedure TFmSAC_01_ER0.EdCEPEnter(Sender: TObject);
begin
  FCEP := Geral.SoNumero_TT(EdCEP.Text);
end;

procedure TFmSAC_01_ER0.EdCEPExit(Sender: TObject);
var
  CEP: String;
begin
  CEP := Geral.SoNumero_TT(EdCEP.Text);
  if CEP <> FCEP then
  begin
    if Trim(CEP) <> '' then
      U_CEP.ConsultaCEP(EdCEP, EdLograd, EdRua, EdNumero, EdBairro,
        EdCidade, EdUF, EdPais, EdNumero, EdCompl, CBLograd,
        EdCodMunici, CBCodMunici, EdCodiPais, CBCodiPais);
  end;
end;

procedure TFmSAC_01_ER0.FecharOForm(Confirma: Boolean; Codigo: Integer);
var
  i: Integer;
  Res: Boolean;
begin
  Res := False;

  for i:= 0 to (TFmSAC_01(FFmCriou).ComponentCount - 1) do
  begin
    if TFmSAC_01(FFmCriou).Components[i] = CsAbaOrigem.Component then
      Res := True;
  end;
  //
  if (TTabSheet(CsAbaOrigem.Component) <> nil) and (Res) then
  begin
    // se ainda existir
    if TMySQLQuery(CsQry.Component) <> nil then
    begin
      TMySQLQuery(CsQry.Component).Close;
      TMySQLQuery(CsQry.Component).Open;
    end;
    if Confirma and (Codigo <> 0) then
    begin
      TdmkEditCB(CsEd.Component).ValueVariant       := VaCodigo.ValueVariant;
      TdmkDBLookupComboBox(CsCB.Component).KeyValue := VaCodigo.ValueVariant;
    end;
  end;
  //Close;
  TFmSAC_01(FFmCriou).FecharAba(TTabSheet(Self.Owner));
end;

procedure TFmSAC_01_ER0.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stIns;
  GBCunsCad.Visible := ImgTipo.SQLType = stIns;
  //
  FCEP := '';
  VaCodigo.ValueVariant := 0;
  //
  CBTe1Tip.ListSource    := DsTe1Tip;
  CBHowFind.ListSource   := DsHowFounded;
  CBCodMunici.ListSource := DsMunici;
  CBCodiPais.ListSource  := DsBacen_Pais;
  CBPsqCidade.ListSource := DsMunici;
  //
  UnDmkDAC_PF.AbreQuery(QrListaLograd, DMod.MyDB);
  UMyMod.AbreQuery(QrMunici, DModG.AllID_DB);
  UMyMod.AbreQuery(QrBacen_Pais, DModG.AllID_DB);
  UnDmkDAC_PF.AbreQuery(QrAccounts, DMod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrAtividades, DMod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrHowFounded, DMod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrTe1Tip, DMod.MyDB);
  //
  Entities.ConfiguraTipoDeCadastro(CkCliente1_0, CkCliente2_0, CkCliente3_0,
    CkCliente4_0, CkFornece1_0, CkFornece2_0, CkFornece3_0, CkFornece4_0,
    CkFornece5_0, CkFornece6_0, CkFornece7_0, CkFornece8_0, CkTerceiro_0);
  //
  FNaoPesquisa := True;
  ConfiguraDadosPadrao(True);
  //
  PnConfirma.Visible := False;
end;

procedure TFmSAC_01_ER0.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_DELETE then
  begin
    ConfiguraDadosPadrao(False);
  end;
end;

procedure TFmSAC_01_ER0.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [nil, nil, nil],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmSAC_01_ER0.FormShow(Sender: TObject);
begin
  if FmPrincipal.sd1.active then
    FmPrincipal.sd1.SkinForm(Handle);
  // ERRO! Oculta dmkEdits!
  //MyObjects.CorIniComponente(THandle(Sender));
  FmMyGlyfs.DefineGlyfs(TForm(Sender));
  EdPsqRua.SetFocus;
end;

procedure TFmSAC_01_ER0.HandleWndProc(var Msg: TMessage);
begin
  if Word(Msg.wParam) = 255 then
    TFmSAC_01(FFmCriou).AcoplarFormulario(Owner as TTabSheet)
  else
    inherited;
end;

procedure TFmSAC_01_ER0.Internet1Click(Sender: TObject);
begin
  U_CEP.ConsultaCEP2(EdCEP, EdLograd, EdRua, EdNumero, EdBairro,
    EdCidade, EdUF, EdPais, EdNumero, EdCompl, CBLograd, CBCodMunici,
    CBCodiPais, EdCodMunici, EdCodiPais);
end;

procedure TFmSAC_01_ER0.PesquisaSINTEGRA;
const
  CBCodCNAE = nil;
  EdCodCNAE = nil;
  EdSUFRAMA = nil;
var
  UF: String;
begin
  if Length(CBRE.Text) = 2 then
  begin
    if CBRE.Items.IndexOf(CBRE.Text) > -1 then
    begin
      UF := CBRE.Text;
      CBRE.Text := '';
      RGTipo.Itemindex := 0;
      UConsultasWeb.ConsultaIE(UF, EdCNPJCPF, EdIERG, EdRazaoNome, EdCep, EdRua,
        EdLograd, EdCompl, EdNumero, EdBairro, EdCidade, EdUF, EdPais,
        CBLograd, CBCodMunici, CBCodiPais, CBCodCNAE, EdCodMunici,
        EdCodiPais, EdCodCNAE, EdTe1, nil, EdSUFRAMA);
    end else
    begin
      Geral.MB_Aviso('UF desconhecida ou n�o implementada: ' + CBRE.Text);
    end;
  end;
end;

procedure TFmSAC_01_ER0.ReopenPesqEndereco(Manual: Boolean);
var
  Rua, Numero, Cidade, UF: String;
  Num: Integer;
begin
  if FNaoPesquisa = True then
    Exit;
  //
  if Manual or (RGPesquisa.ItemIndex = 0) then
  begin
    Rua := EdPsqRua.Text;
    //
    if MyObjects.FIC(Trim(Rua) = '', EdPsqRua, 'Informe o "Nome do logradouro"!') then Exit;
    //
    if Trim(Rua) <> '' then
    begin
      Numero := EdPsqNumero.Text;
      if (Trim(Numero) <> '') and (Geral.SoNumero_TT(Numero) = Numero) then
      begin
        Num := Geral.IMV(Numero);
        if Numero <> '' then
          Numero := 'AND SNumero BETWEEN ' + Geral.FF0(Num - 100) + ' AND ' +
          Geral.FF0(Num + 100);
      end else
        Numero := '';
      Cidade := Trim(CBPsqCidade.Text);
      if EdCidade.ValueVariant <> '' then
        Cidade := 'AND SCidade LIKE "%' + Cidade + '%" '
      else
        Cidade := '';
      UF := Trim(EdUF.Text);
      if UF <> '' then
        UF := 'AND SUF = "' + UF + '" ';
      UnDmkDAC_PF.AbreMySQLQuery0(QrPesq, Dmod.MyDB, [
      'SELECT IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_ENT, ',
      'stc.* ',
      'FROM siaptercad stc ',
      'LEFT JOIN entidades ent ON ent.Codigo=stc.Cliente ',
      'WHERE SRua LIKE "%' + Rua + '%" ',
      Numero,
      Cidade,
      UF,
      'ORDER BY SNumero ',
      '']);
    end;
  end;
end;

procedure TFmSAC_01_ER0.RGTipoClick(Sender: TObject);
var
  OK: Boolean;
begin
  OK                  := RGTipo.ItemIndex >= 0;
  PnConfirma.Visible  := OK and (Trim(EdRazaoNome.Text) <> '');
  PnDados1.Visible    := OK;
  PnDados2.Visible    := OK;
  PnDados3.Visible    := OK;
  BtSintregra.Visible := RGTipo.ItemIndex = 0;
  //
  if PnDados1.Visible then
    EdRazaoNome.SetFocus;
end;

procedure TFmSAC_01_ER0.SbCEPClick(Sender: TObject);
begin
  MyObjects.MostraPMCEP(PMCEP, SbCEP);
  try
    EdNumero.SetFocus;
  except
    ;
  end;
end;

procedure TFmSAC_01_ER0.SbRFClick(Sender: TObject);
const
  CBCodCNAE = nil;
  EdCodCNAE = nil;
begin
  case RGTipo.ItemIndex of
    0:
      UConsultasWeb.ConsultaCNPJ(EdCNPJCPF, EdIERG, EdRazaoNome, EdCep, EdRua,
        EdLograd, EdCompl, EdNumero, EdBairro, EdCidade, EdUF, EdPais,
        CBLograd, CBCodMunici, CBCodiPais, CBCodCNAE, EdCodMunici,
        EdCodiPais, EdCodCNAE);
    1:
      UConsultasWeb.ConsultaCPF(EdRazaoNome, EdCNPJCPF);
    else
      Geral.MB_Aviso('Defina o tipo de pessoa e tente novamente!');
  end;
end;

procedure TFmSAC_01_ER0.SpeedButton5Click(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  //
  UnCfgCadLista.MostraCadLista(QrAtividades.Database, 'atividades',
  QrAtividadesNome.Size, ncGerlSeq1, 'Cadastro de Atividades',
  [], False, Null, [], [], False);
  //
  UMyMod.SetaCodigoPesquisado(EdAtivPrinc, CBAtivPrinc, QrAtividades, VAR_CADASTRO);
end;

procedure TFmSAC_01_ER0.SpeedButton6Click(Sender: TObject);
begin
  VAR_CADASTRO := EdAccount.ValueVariant;
  DModG.CadastroDeEntidade(VAR_CADASTRO, fmcadSelecionar, fmcadEntidade2);
  if VAR_CADASTRO <> 0 then
    UMyMod.SetaCodigoPesquisado(EdAccount, CBAccount, QrAccounts, VAR_CADASTRO);
end;

procedure TFmSAC_01_ER0.SpeedButton7Click(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  //
  UnCfgCadLista.MostraCadLista(QrHowFounded.Database, 'howfounded',
  QrHowFoundedNome.Size, ncGerlSeq1, 'Cadastro de Tipos de Marketing',
  [], False, Null, [], [], False);
  //
  UMyMod.SetaCodigoPesquisado(EdHowFind, CBHowFind, QrHowFounded, VAR_CADASTRO);
end;

function TFmSAC_01_ER0.UpdateCunsCad(Codigo: Integer): Boolean;
var
  AtivPrinc, HowFind, Account: Integer;
  DataCon: String;
begin
  Result := False;
{$IfNDef NAO_CUNS}
  if GBCunsCad.Visible then
  begin
    AtivPrinc := EdAtivPrinc.ValueVariant;
    HowFind := EdHowFind.ValueVariant;
    Account := EdAccount.ValueVariant;
    //
    DataCon := Geral.FDT(TPDataCon.Date, 1);
    //
    Result := UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'cunscad', False, [
    'AtivPrinc', 'HowFind', 'Account', 'DataCon'], [
    'Codigo'], [
    AtivPrinc, HowFind, Account, DataCon], [
    Codigo], True);
  end;
{$Else}
  //dmkPF.InfoSemModulo(mdlappCuns);
{$EndIf}
end;

procedure TFmSAC_01_ER0.Verificar1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmEntiCEP, FmEntiCEP, afmoNegarComAviso) then
  begin
    with FmEntiCEP do
    begin
      DBGrid1.Visible := False;
      Painel1.Visible := False;
      case VAR_ACTIVEPAGE of
        1: EdCEPLoc.Text := EdCEP.Text;
        {  Parei Aqui! falta fazer
        2: EdCEPLoc.Text := EdECEP.Text;
        5: EdCEPLoc.Text := EdCCEP.Text;
        6: EdCEPLoc.Text := EdLCEP.Text;
        }
      end;
      ShowModal;
      Destroy;
    end;
  end;
end;

end.
