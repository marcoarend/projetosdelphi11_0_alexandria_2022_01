unit UnSACAll_PF;

interface

uses
  StdCtrls, ExtCtrls, Windows, Messages, SysUtils, Classes, Graphics, Controls,
  ComCtrls, dmkGeral, Forms, Dialogs, Menus, UnMsgInt, UnInternalConsts, Db,
  DbCtrls, UnInternalConsts2, Variants, dmkEdit;

type
  TUnSACAll_PF = class(TObject)
  private
    { Private declarations }
  public
    { Public declarations }
    procedure CalculaM2Total(EdM2Constru, EdM2NaoBuild, EdM2Total: TdmkEdit);
  end;

var
  SACAll_PF: TUnSACAll_PF;

implementation

//uses ;

{ TUnSACAll_PF }

{ TUnSACAll_PF }

procedure TUnSACAll_PF.CalculaM2Total(EdM2Constru, EdM2NaoBuild,
  EdM2Total: TdmkEdit);
var
  Sim, Nao: Double;
begin
  Sim := 0;
  Nao := 0;
  if EdM2Constru.ValueVariant <> Null then
    Sim := EdM2Constru.ValueVariant;
  if EdM2NaoBuild.ValueVariant <> Null then
    Nao := EdM2NaoBuild.ValueVariant;
  EdM2Total.ValueVariant := Sim + Nao;
end;

end.
