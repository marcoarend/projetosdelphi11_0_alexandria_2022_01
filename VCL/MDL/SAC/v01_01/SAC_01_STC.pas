unit SAC_01_STC;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ComCtrls, ExtCtrls, Buttons, DB, mySQLDbTables, Menus,
  dmkGeral, dmkImage, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB,
  dmkCompoStore, UnInternalConsts, UnDmkEnums;

type
  TFmSAC_01_STC = class(TForm)
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel4: TPanel;
    BtDesiste: TBitBtn;
    PMCEP: TPopupMenu;
    Descobrir1: TMenuItem;
    Verificar1: TMenuItem;
    QrBacen_Pais: TmySQLQuery;
    IntegerField1: TIntegerField;
    StringField1: TWideStringField;
    DsBacen_Pais: TDataSource;
    QrMunici: TmySQLQuery;
    QrMuniciCodigo: TIntegerField;
    QrMuniciNome: TWideStringField;
    DsMunici: TDataSource;
    QrCunsCad: TmySQLQuery;
    QrCunsCadCodigo: TIntegerField;
    QrCunsCadNO_ENT: TWideStringField;
    DsCunsCad: TDataSource;
    QrListaLograd: TmySQLQuery;
    QrListaLogradCodigo: TIntegerField;
    QrListaLogradNome: TWideStringField;
    DsListaLograd: TDataSource;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GroupBox2: TGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    EdCliente: TdmkEditCB;
    CBCliente: TdmkDBLookupComboBox;
    EdCodigo: TdmkEdit;
    EdNome: TdmkEdit;
    GroupBox3: TGroupBox;
    Panel8: TPanel;
    Label12: TLabel;
    Label97: TLabel;
    Label31: TLabel;
    Label32: TLabel;
    Label34: TLabel;
    Label35: TLabel;
    Label38: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    Label106: TLabel;
    Label109: TLabel;
    Label112: TLabel;
    Label4: TLabel;
    Label6: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    BtCEP_S: TBitBtn;
    EdSCEP: TdmkEdit;
    CBSLograd: TdmkDBLookupComboBox;
    EdSRua: TdmkEdit;
    EdSNumero: TdmkEdit;
    EdSBairro: TdmkEdit;
    EdSCidade: TdmkEdit;
    EdSUF: TdmkEdit;
    EdSPais: TdmkEdit;
    EdSLograd: TdmkEditCB;
    EdSCompl: TdmkEdit;
    EdSEndeRef: TdmkEdit;
    EdSCodMunici: TdmkEditCB;
    CBSCodMunici: TdmkDBLookupComboBox;
    EdSCodiPais: TdmkEditCB;
    CBSCodiPais: TdmkDBLookupComboBox;
    EdSTe1: TdmkEdit;
    EdM2Constru: TdmkEdit;
    EdM2NaoBuild: TdmkEdit;
    EdM2Terreno: TdmkEdit;
    EdM2Total: TdmkEdit;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    CsQry: TdmkCompoStore;
    CsEd: TdmkCompoStore;
    CsCB: TdmkCompoStore;
    BrCopia: TBitBtn;
    VerificarInternet1: TMenuItem;
    procedure FormShow(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure Descobrir1Click(Sender: TObject);
    procedure Verificar1Click(Sender: TObject);
    procedure BtCEP_SClick(Sender: TObject);
    procedure EdSCEPEnter(Sender: TObject);
    procedure EdSCEPExit(Sender: TObject);
    procedure EdM2ConstruChange(Sender: TObject);
    procedure EdM2NaoBuildChange(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BrCopiaClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure VerificarInternet1Click(Sender: TObject);
  private
    FSCEP: String;
    //
    procedure HandleWndProc(var Msg: TMessage); message WM_SYSCOMMAND;
    procedure FechaOForm();
  public
    { Public declarations }
    FFmCriou: TForm;
  end;

var
  FmSAC_01_STC: TFmSAC_01_STC;

implementation

uses Principal, SAC_01, UnMyObjects, EntiCEP, Module, UMySQLModule, UnCEP,
  MyDBCheck, ModuleGeral, MyGlyfs, DmkDAC_PF, UnSACAll_PF;

{$R *.dfm}

{ TFmSAC_01_STC }

procedure TFmSAC_01_STC.BrCopiaClick(Sender: TObject);
begin
  DmodG.ReopenEndereco(EdCliente.ValueVariant);
  //
  EdSCEP.ValueVariant       := DModG.QrEnderecoCEP.Value;
  EdSLograd.ValueVariant    := DModG.QrEnderecoLOGRAD.Value;
  CBSLograd.KeyValue        := DModG.QrEnderecoLOGRAD.Value;
  EdSRua.ValueVariant       := DModG.QrEnderecoRUA.Value;
  EdSNumero.ValueVariant    := DModG.QrEnderecoNUMERO.Value;
  EdSCompl.ValueVariant     := DModG.QrEnderecoCOMPL.Value;
  EdSBairro.ValueVariant    := DModG.QrEnderecoBAIRRO.Value;
  EdSCidade.ValueVariant    := DModG.QrEnderecoCIDADE.Value;
  EdSUF.ValueVariant        := DModG.QrEnderecoNOMEUF.Value;
  EdSPais.ValueVariant      := DModG.QrEnderecoPais.Value;
  EdSEndeRef.ValueVariant   := DModG.QrEnderecoENDEREF.Value;
  EdSTe1.ValueVariant       := DModG.QrEnderecoTE1.Value;
  EdSCodMunici.ValueVariant := DModG.QrEnderecoCODMUNICI.Value;
  CBSCodMunici.KeyValue     := DModG.QrEnderecoCODMUNICI.Value;
  EdSCodiPais.ValueVariant  := DModG.QrEnderecoCODPAIS.Value;
  CBSCodiPais.KeyValue      := DModG.QrEnderecoCODPAIS.Value;
end;

procedure TFmSAC_01_STC.BtCEP_SClick(Sender: TObject);
begin
  MyObjects.MostraPMCEP(PMCEP, BtCEP_S);
  try
    EdSNumero.SetFocus;
  except
    ;
  end;
end;

procedure TFmSAC_01_STC.BtConfirmaClick(Sender: TObject);
var
  Nome, SRua, SCompl, SBairro, SCidade, SUF, SPais, SEndeRef, STe1: String;
  Codigo, Cliente, SLograd, SNumero, SCEP, SCodMunici, SCodiPais: Integer;
  M2Constru, M2NaoBuild, M2Terreno, M2Total: Double;
  //
  QrSiapterCad: TmySQLQuery;
begin
  Codigo         := EdCodigo.ValueVariant;
  Nome           := EdNome.Text;
  Cliente        := EdCliente.ValueVariant;
  SLograd        := EdSLograd.ValueVariant;
  SRua           := EdSRua.Text;
  SNumero        := Geral.IMV(Geral.SoNumero_TT(EdSNumero.ValueVariant));
  SCompl         := EdSCompl.Text;
  SBairro        := EdSBairro.Text;
  SCidade        := EdSCidade.Text;
  SUF            := EdSUF.Text;
  SCEP           := Geral.IMV(Geral.SoNumero_TT(EdSCEP.ValueVariant));
  SPais          := EdSPais.Text;
  SEndeRef       := EdSEndeRef.Text;
  SCodMunici     := EdSCodMunici.ValueVariant;
  SCodiPais      := EdSCodiPais.ValueVariant;
  STe1           := Geral.SoNumero_TT(EdSTe1.Text);
  M2Constru      := EdM2Constru.ValueVariant;
  M2NaoBuild     := EdM2NaoBuild.ValueVariant;
  M2Terreno      := EdM2Terreno.ValueVariant;
  M2Total        := EdM2Total.ValueVariant;
  //
  Codigo := UMyMod.BPGS1I32(
    'siaptercad', 'Codigo', '', '', tsPos, ImgTipo.SQLType, Codigo);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'siaptercad', False, [
  'Nome', 'Cliente', 'SLograd',
  'SRua', 'SNumero', 'SCompl',
  'SBairro', 'SCidade', 'SUF',
  'SCEP', 'SPais', 'SEndeRef',
  'SCodMunici', 'SCodiPais', 'STe1',
  'M2Constru', 'M2NaoBuild',
  'M2Terreno', 'M2Total'], [
  'Codigo'], [
  Nome, Cliente, SLograd,
  SRua, SNumero, SCompl,
  SBairro, SCidade, SUF,
  SCEP, SPais, SEndeRef,
  SCodMunici, SCodiPais, STe1,
  M2Constru, M2NaoBuild,
  M2Terreno, M2Total], [
  Codigo], True) then
  begin
    if CsQry.Component <> nil then
    begin
      QrSiapterCad := TmySQLQuery(CsQry.Component);
      QrSiapterCad.Close;
      QrSiapterCad.Open;
      QrSiapterCad.Locate('Codigo', Codigo, []);
      //
    end;
    if CsEd.Component <> nil then
      TdmkEdit(CsEd.Component).ValueVariant := Codigo;
    if CsCB.Component <> nil then
      TdmkDBLookupComboBox(CsCB.Component).KeyValue := Codigo;
    //
    FechaOForm();
  end;
end;

procedure TFmSAC_01_STC.BtDesisteClick(Sender: TObject);
begin
  FechaOForm();
end;

procedure TFmSAC_01_STC.Descobrir1Click(Sender: TObject);
begin
  U_CEP.DescobrirCEP(EdSCEP, EdSLograd, EdSRua, EdSBairro, EdSCidade, EdSUF,
  EdSPais, EdSCodMunici, EdSCodiPais, CBSLograd, CBSCodMunici, CBSCodiPais);
end;

procedure TFmSAC_01_STC.EdM2ConstruChange(Sender: TObject);
begin
  SACAll_PF.CalculaM2Total(EdM2Constru, EdM2NaoBuild, EdM2Total);
end;

procedure TFmSAC_01_STC.EdM2NaoBuildChange(Sender: TObject);
begin
  SACAll_PF.CalculaM2Total(EdM2Constru, EdM2NaoBuild, EdM2Total);
end;

procedure TFmSAC_01_STC.EdSCEPEnter(Sender: TObject);
begin
  FSCEP := Geral.SoNumero_TT(EdSCEP.Text);
end;

procedure TFmSAC_01_STC.EdSCEPExit(Sender: TObject);
var
  CEP: String;
begin
  CEP := Geral.SoNumero_TT(EdSCEP.Text);
  if CEP <> FSCEP then
  begin
    if Trim(CEP) <> '' then
      U_CEP.ConsultaCEP(EdSCEP, EdSLograd, EdSRua, EdSNumero, EdSBairro,
        EdSCidade, EdSUF, EdSPais, EdSNumero, EdSCompl, CBSLograd,
        EdSCodMunici, CBSCodMunici, EdSCodiPais, CBSCodiPais);
  end;
end;

procedure TFmSAC_01_STC.FechaOForm();
begin
{
  if TTabSheet(CsAbaOrigem.Component) <> nil then
  begin
    // se ainda existir
    if TMySQLQuery(CsQry.Component) <> nil then
    begin
      TMySQLQuery(CsQry.Component).Close;
      TMySQLQuery(CsQry.Component).Open;
    end;
    TdmkEditCB(CsEd.Component).ValueVariant := VaCodigo.ValueVariant;
    TdmkDBLookupComboBox(CsCB.Component).KeyValue := VaCodigo.ValueVariant;
  end;
}
  Close;
  TFmSAC_01(FFmCriou).FecharAba(TTabSheet(Self.Owner));
end;

procedure TFmSAC_01_STC.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stIns;
  UnDmkDAC_PF.AbreQuery(QrListaLograd, DMod.MyDB);
  UMyMod.AbreQuery(QrMunici, DModG.AllID_DB);
  UMyMod.AbreQuery(QrBacen_Pais, DModG.AllID_DB);
  UnDmkDAC_PF.AbreQuery(QrCunsCad, DMod.MyDB);
end;

procedure TFmSAC_01_STC.FormShow(Sender: TObject);
begin
  if FmPrincipal.sd1.active then
    FmPrincipal.sd1.SkinForm(Handle);
  // ERRO! Oculta dmkEdits!
  //MyObjects.CorIniComponente(THandle(Sender));
  FmMyGlyfs.DefineGlyfs(TForm(Sender));
end;

procedure TFmSAC_01_STC.HandleWndProc(var Msg: TMessage);
begin
  if Word(Msg.wParam) = 255 then
    TFmSAC_01(FFmCriou).AcoplarFormulario(Owner as TTabSheet)
  else
    inherited;
end;

procedure TFmSAC_01_STC.Verificar1Click(Sender: TObject);
begin
  VAR_ACTIVEPAGE := 7;
  if DBCheck.CriaFm(TFmEntiCEP, FmEntiCEP, afmoNegarComAviso) then
  begin
    with FmEntiCEP do
    begin
      DBGrid1.Visible := False;
      Painel1.Visible := False;
      //
      FmEntiCEP.EdCEPLoc.Text := EdSCEP.Text;
      //
      FmEntiCEP.FEdCEP       :=  EdSCEP;
      FmEntiCEP.FEdLograd    :=  EdSLograd;
      FmEntiCEP.FCBLograd    :=  CBSLograd;
      FmEntiCEP.FEdRua       :=  EdSRua;
      FmEntiCEP.FEdBairro    :=  EdSBairro;
      FmEntiCEP.FEdCidade    :=  EdSCidade;
      FmEntiCEP.FEdUF        :=  EdSUF;
      FmEntiCEP.FEdPais      :=  EdSPais;
      FmEntiCEP.FEdCodMunici :=  EdSCodMunici;
      FmEntiCEP.FCBCodMunici :=  CBSCodMunici;
      FmEntiCEP.FEdCodiPais  :=  EdSCodiPais;
      FmEntiCEP.FCBCodiPais  :=  CBSCodiPais;
      //
      ShowModal;
      Destroy;
    end;
  end;
end;

procedure TFmSAC_01_STC.VerificarInternet1Click(Sender: TObject);
begin
  U_CEP.ConsultaCEP2(EdSCEP, EdSLograd, EdSRua, EdSNumero, EdSBairro,
    EdSCidade, EdSUF, EdSPais, EdSNumero, EdSCompl, CBSLograd, CBSCodMunici,
    CBSCodiPais, EdSCodMunici, EdSCodiPais);
end;

end.
